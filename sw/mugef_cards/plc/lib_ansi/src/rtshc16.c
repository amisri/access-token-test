#ifdef __Z_BASED__
/*****************************************************
  FILE        : RTSHC16.c
  PURPOSE     : Runtime support for M68HC16.
  COMMENT     : 
    _Hxxx routines implement DSP arithmetic.
    _Fxxx routines implement IEEE 32bit arithmetic
    _Dxxx routines implement IEEE 64bit arithmetic, including conversions
          to/from the other two formats.
    DSP format makes use of the M68HC16's fast DSP instructions. The hi word
    is a fix point mantissa in 2's complement. Bit #15 can be used as sign bit,
    bits #14 to #0 are the mantissa (bit #14 is 0.5, #13 is 0.25). There's no
    hidden bit.
      The exponent is in the low word in 2's complement and ranges from -32768
    to 32767.
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

int errno;           /* implemented here because Modula-2 uses 'Math.c'.  */

/******************************************************************************
   DSP FLOATING POINT ROUTINES
 ******************************************************************************/

#pragma NO_FRAME

void _HMUL (void)  /*   parameters are passed in registers:  */
                   /*   D:E operand 1                        */
                   /*    X  @operand 2    D:E * @X           */
                   /*   D:E result                           */
{
   asm {
           PSHM  X
           ADDD  2,X      ; add exponents
           LDX   0,X      ; load mantissa of op2
           XGDX           ; prepare D:E for multiplication, save new exponent
           FMULS          ; E = D * E -> new mantissa
           XGDX           ; restore new exponent
           BVC   L1       ; was there an overflow
           ADDD  #1       ; yes, adjust exponent and
           LSRE           ; normalize mantissa
     L1:   ADCE  #0       ; round mantissa
           BEQ   Zero     ; is the result zero
           LSLE           ; no, normalize mantissa
           BVC   L3       ; if was normalized already
           RORE           ; undo shift again
           BRA   End
     L3:   ADDD  #-1      ; adjust exponent
           BRA   End
     Zero: CLRD           ; result is zero
     End:  PULM  X
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HDIV (void)  /*   parameters are passed in registers:  */
                   /*   D:E dividend                         */
                   /*    X  divisor      D:E / @X            */
                   /*   D:E result                           */
{
   asm {
           PSHM  X,Y,K
           TXY   
           TSTW  0,Y      ; is divisor zero
           BEQ   Z_DIV    ; yes, do a zero division
           TSTE           ; is dividend zero
           BEQ   ZERO     ; yes, return zero
           SUBD  2,Y      ; E.result = E.dividend - E.divisor
           PSHM  E,Y      ; save M.dividend(E), temp(Y)
           LDE   0,Y      ; M.divisor
           TSY            ; points to saved values
           STE   0,Y      ; temp (M.divisor) for sign of result
           BPL   L1       ; is neg -> invert
           NEGE           ; M.divisor
           BVC   L1       ; if overflow
           LSRE           ; normalize
           ADDD  #-1      ; adjust E.result
    L1:    TSTW  2,Y      ; M.dividend > 0
           BPL   L3       ; is neg -> invert
           COM   0,Y      ; invert sign of result
           NEGW  2,Y      ; M.dividend
           BVC   L3       ; if overflow
           LSRW  2,Y      ; normalize
           ADDD  #1       ; adjust E.result
           BRA   L3
    LOOP:  LSRW  2,Y      ; else M.dividend >> 1     
           ADDD  #1       ; adjust E.result    
    L3:    CPE   2,Y      ; M.divisor > M.dividend
           BLE   LOOP     ; yes, continue
           XGDX           ; save E.result to X
           LDD   2,Y      ; M.dividend
           LSLD           ; left adjust M.dividend
           LSLE           ; left adjust M.divisor
           XGEX           ; prepare operands for fdiv, save X to E
           FDIV           ;
           XGEX           ; restore X from E, M.result > E
           LSRE           ; adjust M.result (DSP format)
           LSLD           ; round (based on remainder)
           ADCE  #0       ;
           XGDX           ; reload E.result from X
           TSTW  0,Y      ; sign of result
           BPL   L4
           NEGE           ; result is negative, negate
           CPE   #0xC000  ; special case, do not normalize
           BGE   L6
    L4:    LSLE           ; normalize mantissa
           BVS   L5       ;
           ADDD  #-1      ; adjust E.result
           BRA   L4
    L5:    RORE
    L6:    AIS   #4
           BRA   End
           
    ZERO:  CLRD           ; return zero
           CLRE
           BRA   End 
           
    Z_DIV: CLRD           ; force zero divide
           XGDX
           IDIV
    
    End:   PULM  X,Y,K 
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HADD2 (void) /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    X               D:E + @X            */
                   /*   D:E result                           */
{
   asm {
           PSHM  X,Y,K
           TXY   
           TSTE           ; is operand 1 zero ?
           BEQ   R_OP2    ; yes, return operand 2
           TSTW  0,Y      ; is operand 2 zero ?
           BEQ   R_OP1    ; yes, return operand 1
           CPD   2,Y      ; same exponents
           BEQ   DO_ADD   ; yes
           BMI   L1       ; exchange operands
           LDX   0,Y
           XGEX
           STX   0,Y
           LDX   2,Y
           XGDX
           STX   2,Y      ; now, operand (E:D) < operand (@Y)
   L1:     SUBD  2,Y      ; exp (E:D) - exp @Y
           CPD   #-15     ; if operands too different
           BMI   R_OP2    ; then return bigger operand
           CPD   #-7      ; if more 7 bits to shift
           BPL   L2       ; no, shift only
           XGDE
           TAB            ; yes, simulate shift right by 8
           SXT            ; 
           XGDE
           ADDD  #8       ; adjust shift count
           BEQ   DO_ADD
   L2:     ASRE           ; shift right remainding bits
           ADDD  #1
           BNE   L2       ;
   DO_ADD: LDD   2,Y      ; load bigger exponent
           ADDE  0,Y      ; add mantissas
           BVS   L4       ; if overflow
           BEQ   R_ZERO   ; if result is zero, return zero
   L3:     LSLE           ; normalize
           BVS   L5
           ADDD  #-1
           BRA   L3
   L4:     RORE           ; shift right,
           ADDD  #1       ; is normalized now
           BRA   End
   L5:     RORE           
           BRA   End 
           
   R_ZERO: TED
           BRA   End  
               
   R_OP2:  LDE   0,Y      ; return operand 2
           LDD   2,Y
   R_OP1:
   End:    PULM  X,Y,K
   }
}        

/******************************************************************************/

#pragma NO_FRAME

void _HSUB (void)  /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    X               D:E - @X            */
                   /*   D:E result                           */
{
   asm {   
           PSHM  X,K
           PSHM  D,E      ; save subtrahend
           LDE   0,X
           LDD   2,X      ; load subtractor
           NEGE           ; negate subtractor
           BVC   L2
           LSRE  
           ADDD  #1
     L2:
           TSX            ; setup pointer to subtrahend
           JSR   _HADD2   ; call common add routine
           AIS   #4
           PULM  X,K    
   }
}


/******************************************************************************/

#pragma NO_FRAME

void _HADD (void)  /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    X               D:E + @X            */
                   /*   D:E result                           */
{
   asm {
           PSHM  X,K   
           PSHM  D,E
           LDE   0,X
           LDD   2,X
           TSX
           JSR   _HADD2   ; call common add routine
           AIS   #4
           PULM  X,K    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HCMP (void)  /*   parameters are passed in registers:  */
                   /*   D:E operand 1                        */
                   /*    X  operand 2    D:E - @X            */
                   /*   CCR result                           */
{
    asm {
           EORE  0,X      ; XOR mantissas: XOR sign bit
           BMI   L2       ; same sign ?
           EORE  0,X      ; yes, same sign, undo XOR of mantissas
           BEQ   L3       ; is operand 1 zero
           TSTW  0,X      ; is operand 2 zero
           BEQ   L4       ;
           CPD   2,X      ; compare exponents
           BEQ   L3       ; exponents differ
           TPA            ; save CCR
           TSTE           ; check sign bit
           BPL   L1       ; if positive, CCR already ok
           EORA  #8       ; no, invert N bit in saved CCR
   L1:     TAP            ; restore CCR
           RTS
           
   L4:     TSTE   
           RTS
      
   L2:     EORE  0,X      ; undo XOR of mantissas
   L3:     CPE   0,X      ; set flags in CCR
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HNEG(void)   /* negate DSP float:         */
                   /*   D:E operand and result  */
{
   asm {   
           NEGE     
           BVC   L1
           LSRE  
           ADDD  #1
       L1:
   }
}

/* this operation can be done inline and/or in memory: 
           NEGW   (ea)
           BVC    L1
           LSRW   (ea)
           INCW   (ea+2)
       L1:
*/
  
/******************************************************************************/

#pragma NO_FRAME         

void _HABS(void)   /* absolute value of DSP float:  */
                   /*   D:E operand and result      */
{
   asm {   
           TSTE
           BPL   L1
           NEGE     
           BVC   L1
           LSRE  
           ADDD  #1
       L1:
   }
}

/* this operation can be done inline and/or in memory:
           TSTW   (ea)
           BPL    L1 
           NEGW   (ea)
           BVC    L1
           LSRW   (ea)
           INCW   (ea+2)
       L1:
*/ 

/******************************************************************************/

#pragma NO_FRAME

void _HSFLOAT(void) /* signed long to DSP float transformation */
                    /* parameter E:D                           */
                    /* result    E:D                           */
{
   asm {
           PSHM  X
           TSTE           ; check high word
           BNE   L1
           TSTD           ; check low word
           BEQ   END      ; restult is zero
           BMI   L2
     L3:   TDE            ; E was zero, swap words
           CLRD
           LDX   #16      ; initialize exponent
           BRA   Norm     ; and normalize
     L1:   TSTD           ; is E sign extended D
           BPL   L2       ; no if N flag is set
           CPE   #-1      ; 
           BEQ   L3           
     L2:   LDX   #32      ; initial exponent
     Norm: AIX   #-1      ; normalize mantissa
           LSLD
           ROLE
           BVC   Norm     ;
           RORE
           XGDX           ; load exponent into D
     END:  PULM  X  
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HUFLOAT(void) /* unsigned long to DSP float transformation */
                    /* parameter E:D                             */
                    /* result    E:D                             */
{
   asm {
           PSHM  X
           LDX   #32      ; initial exponent
           TSTE           ; check high word
           BMI   Done     ; msb set; data in D not needed
           BNE   Norm
           TSTD           ; check low word
           BEQ   END      ; restult is zero
           BMI   Norm
           TDE            ; E was zero, swap words
           CLRD
           AIX   #-16     ; adjust exponent
     Norm: AIX   #-1      ; normalize mantissa
           LSLD
           ROLE
           BVC   Norm     ;
     Done: RORE
           XGDX           ; load exponent into D
     END:  PULM  X  
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HSTRUNC (void)
  /**** IN: D: Exponent, E: Signed mantissa. Corrected version by TW, original
        just shifted, regardless of the mantissa's sign. But if the value was
        -5.7..., i.e. mantissa was 0xA..., the result was 0xFFFA (-6). Correct
        is to shift 5.7 (0x5...) giving 0x0005 and then negating: -5 (0xFFFB). */
{
  asm {
          PSHM   E,X,K          ; Save mantissa for sign!
          TSTE
          BEQ    ZeroD          ; Result is zero
          TSTD
          BLT    Zero           ; exp < 0 --> result is zero
          TSTE
          BPL    NotNeg
          NEGE                  ; Make mantissa positive
     NotNeg:
          XGDE                  ; E: counter, D: mantissa
          SUBE   #15
          BEQ    PosNeg         ; No shifts needed: E == 0, D == mantissa
          BPL    DoLong         ; More than 15 shifts needed
     ShWord:
          LSRD
          ADDE   #1
          BNE    ShWord
          BRA    PosNeg         ; E == 0, D == mantissa
     DoLong:
          XGDE                  ; E: mantissa, D: counter
          SUBD   #16
          BEQ    PosNeg         ; No shifts needed
          BPL    TooLarge       ; Counter still too large
          NEGD
          XGDX                  ; E|D : Mantissa, X: counter
          CLRD
     ShLong:
          LSRE
          RORD
          AIX    #-1
          BNE    ShLong
     PosNeg:
          TSX
          BRCLR  4,X, #0x80, End
          NEGE                  ; Negate the long result!
          NEGD
          SBCE   #0
          BRA    End
     TooLarge:
          TSX
          BRSET  4,X, #0x80, TooSmall
          LDD    #0xFFFF        ; Overflow value: MAX(longint)
          LDE    #0x7FFF
          BRA    End
     TooSmall:
          LDE    #0x8000        ; Negative overflow
          BRA    ZeroD
     Zero:
          CLRE
     ZeroD:
          CLRD
     End:
          PULM   X,K            ; Clean up stack
          AIS    #2
  }
}

#if 0
               
void _HSTRUNC(void)
{
   asm {
           PSHM  X
           TSTE
           BEQ   ZroD     ; value is zero
           TSTD
           BLT   Zero
           SUBD  #15
           BPL   L2 
           NEGD
           XGDX           ; value fits in lower 16 bits
           TED            ; move bit to D and
           BMI   L3       ; sign extend D into E
           CLRE
           BRA   L1
     L3:   LDE   #-1
     L1:   ASRD           ; shift 16 bits to the correct position
           AIX   #-1
           BNE   L1
           BRA   END
           
     L2:   SUBD  #16
           BEQ   ZroD     ; if no shifts to do
           BGE   Zero     ; result does not fit in a unsigned long
           NEGD
           XGDX           ; result will have up to 32 bits
           CLRD
     LOOP: ASRE           ; shift 32 bits to the correct position
           RORD
           AIX   #-1
           BNE   LOOP
           BRA   END    
                 
     Zero: CLRE      
     ZroD: CLRD
     
     END:  PULM  X
     
   }
}   

#endif

/******************************************************************************/

#pragma NO_FRAME

void _HUTRUNC(void)
{
   asm {
           PSHM  X
           TSTE
           BLE   Zero     ; value is less or equal zero => return 0
           TSTD
           BLE   Zero     ; exponent less or equal zero => return 0
           SUBD  #15
           BPL   L2
           NEGD
           XGDX           ; value fits in lower 16 bits
           TED            
           CLRE
     L1:   LSRD           ; shift 16 bits to the correct position
           AIX   #-1
           BNE   L1
           BRA   END 
           
     L2:   SUBD  #16
           BEQ   L3       ; value > 2^30; no shifts needed
           CPD   #1       
           BEQ   L4       ; value > 2^31;
           BGE   Zero     ; result does not fit in a unsigned long
           NEGD
           XGDX           ; result will have up to 32 bits
           CLRD
     LOOP: LSRE           ; shift 32 bits to the correct position
           RORD
           AIX   #-1
           BNE   LOOP
           BRA   END
                
     Zero: CLRE           ; return zero
     L4:   LSLE      
     L3:   CLRD 
     END:  PULM  X
   }

}

/******************************************************************************
                      CALLING PROCEDURE VARIABLES
                      
  For all routines except _CALLS, the procedure variable is in registers. We
  always have to increment the low word by 2 because the RTS will subtract 2!
    The method is quite tricky: we set up a stack layout as follows:
    
                  RETURN ADDRESS LOW
                  RETURN ADDRESS HIGH
                  Parameter Low + 2
                  Parameter High
    
    The return address is the runtime routine's return address, i.e. it points
  behind the JSR that brought us here. The parameter is the address of the
  function we want to call.
    When we have set up above stack frame, we execute a RTS. This pops the
  parameter from the stack and instead of returning jumps to the beginning of
  the procedure to be called. When this procedure finally executes its own RTS,
  it'll use the original return address and thereby finally return to the point
  where the function pointer call was.
  
  Function _CALLE is used in SMALL memory model only (if both X and Y are used
  by parameters). It uses 0 as the high word of the parameter. This implies that
  in SMALL memory model, the code page be page zero.
    This however is also required by the fact that the vector table has only
  16bit entries and also because the compiler doesn't patch the code page in
  a JSR 0,X!!
 ******************************************************************************/

#pragma NO_FRAME

void _CALLYX (void)         /* parameters are passed in Y:X (4:16)  */
{
   asm {
          AIX   #2        ; adjust address 
          PSHM  X,Y       ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLED(void)         /* parameters are passed in E:D (4:16)  */
{
   asm {
           ADDD  #2       ; adjust address 
           PSHM  D,E      ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLE (void)
{
  asm {
          ADDE  #2
          PSHM  E
          CLRE            ; zero as code page!!
          PSHM  E
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLXE (void)
{
  asm {
          ADDE  #2
          PSHM  E,X
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLYE (void)
{
  asm {
          ADDE #2
          PSHM E,Y
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLS (void)
  /**** Here, the destination address is on the stack,
        above the return address! Therefore, we gotta swap them! */
{
  asm {
          PSHM  D,X,Z,K   ; Save all registers used!
          TSZ             ; Set up access pointer
          LDD   14,Z      ; Load dest addr (LO)
          LDX   10,Z      ; Load return addr (LO)
          ADDD  #2        ; Correct dest addr
          STD   10,Z      ; Swap the two!
          STX   14,Z
          LDD   12,Z      ; Load dest addr (HI)
          LDX   8,Z       ; Load return addr (HI)
          STD   8,Z       ; Swap the two!
          STX   12,Z
          PULM  D,X,Z,K   ; Restore all registers!
  }                       /* this return is the procedure call!  */
} /* end _CALLS */

#pragma NO_FRAME

void _CALLSS (void)
{
  asm {
          AIS   #-2       ; Create space for duplication of code page
          PSHM  D,X,K     ; Save all registers used
          TSX             ; Set up access pointer
          LDD   8,X       ; Load code page...
          STD   6,X       ; ... and duplicate
          LDD   12,X      ; Load dest addr...
          ADDD  #2        ; ... correct it...
          STD   8,X       ; ... and set up as our return address
          LDD   10,X      ; Load former return address...
          STD   12,X      ; ... and set up as return address of called function
          LDD   6,X       ; Now set code page for the latter
          STD   10,X
          PULM  D,X,K     ; Restore all registers used!
  }                       /* this return is the procedure call! */
} /* end _CALLSS */
    
/******************************************************************************
                                BLOCK MOVES
  
  If we may use only one address register, block moves must be done using
  runtime routines. There are four routines, mirroring all the possible moves
  between I/O space and normal memory. To keep the number of routines that
  small, all pointers (not constant I/O pointers) are considered far pointers.
  If necessary, a near pointer is extended by the default data page.
 ******************************************************************************/

#pragma NO_FRAME

static void _block_move (void)
  /****
    Arguments:
          YK:Y  source address
          XK:X  dest. address
          E     counter, initially block size in bytes - 2
    Precondition:
          E >= 0
   */
{
  asm {
    Loop: LDD   E,Y      ;   Copy words
          STD   E,X
          SUBE  #2       ;   counter -= 2
          BPL   Loop     ; WHILE (counter >= 0)
          PULM  Y,K      ; Restore registers
  } /* end asm */;
} /* end _block_move */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_MEM (void)
  /****
    Arguments:
          Source address : 16 bit I/O address on stack
          Dest. address  : 20 bit far pointer in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TBXK           ; Load page register
          TSY            ; Set up access pointer
          LDY   8,Y      ; Load source address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBYK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _IO_TO_MEM */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_IO (void)
  /****
    Arguments:
          Source address : 20 bit far pointer in B/X
          Dest. address  : 16 bit I/O address on stack
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TXY            ; Move source pointer to Y 
          TBYK           ; Load page register
          TSX            ; Set up access pointer
          LDX   8,X      ; Load dest address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBXK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _MEM_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_IO (void)
  /****
    Arguments:
          Source address : 16 bit I/O address in D
          Dest. address  : 16 bit I/O address in X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K
          XGDY
          TSKB
          DECB
          TBXK
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _IO_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_MEM (void)
  /****
    Arguments:
          Source address : 20 bit address on stack
          Dest. address  : 20 bit address in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K 
          TBXK
          TSY
          LDAB 9,Y
          LDY  10,Y
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _MEM_TO_MEM */

/******************************************************************************
                          MISCELLANEOUS PROCEDURES
 ******************************************************************************/
 
#pragma NO_FRAME

void _YK (void)
  /**** Used to reset YK after runtime call. */
{
  asm {
           PSHM   D, CCR
           TSKB
           TBYK
           PULM   D, CCR
  };        
} /* end _YK */

#pragma NO_FRAME

void _XK (void)
{
  asm {
           PSHM   D, CCR
           TSKB
           TBXK
           PULM   D, CCR
  };
} /* end _XK */

#pragma NO_FRAME

void _IDIV(void)          /* parameters are passed in D / X       */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
           XGDX           
   }
}

#pragma NO_FRAME

void _IMOD(void)          /* parameters are passed in D MOD X     */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
   }
}

/******************************************************************************
  32 bit integral arithmetic
 ******************************************************************************/

/*--------------------------------------------------------------- _LMULU ----*
 * UNSIGNED LONG MULTIPLICATION
 *------------------------------
 * Multiplies the unsigned value in registers E:D with the unsigned at the
 * address 0,X:2,X. The result is stored back in registers E:D.
 *
 * The general results of this operation is
 *
 *        +-----------+
 *        |  E  * 0,Y |                 High part          
 *        +-----+-----+-----+
 *    +         |  E  * 2,Y |           Middle 1 part
 *              +-----------+
 *    +         |  D  * 0,Y |           Middle 2 part
 *              +-----+-----+-----+
 *    +               |  D  * 2,Y |     Low part
 *                    +-----------+
 *
 *                    \_ result __/
 *  
 * The result of the multiplication is only le lowest longword of the 64 bits
 * results. In order to have a correct multiplication (without overflow) the
 * highest longword of the result must be 0.
 * The highest longword equal to 0 means that either E = 0 or 0,Y = 0. We can
 * use this fact to make the algorithm faster :
 *
 * if (e == 0) {
 *     return ((D * 0,Y) >> 8) + (D * 2,Y);
 * } else {
 *     return ((E * 2,Y) >> 8) + (D * 2,y);
 * }
 *
 * This algorithm is quite fast because we have only 2 multiplications.
 * 
 * WRONG: for ANSI-C we have to use 32 bit even in case of an                                                                    
 * overflow! So we have the same code as for the signed multiplication.
 * (bh, vc; 1-Mar-93) 
 *
 *---------------------------------------------------------------------------*/
                     
long _LMULU (void)
{
    asm {
                PSHM D,E,Y,K  ; reserve temporary space, save D and save used registers
                TSY           ; Set up access pointer
                
                LDD  2,X      ; Compute mid1 part
                EMUL          ; E * 2,X
                STD  4,Y
                
                LDD  6,Y      ; Compute mid2 part
                LDE  0,X
                EMUL          ; D * 0,X
                ADDD 4,Y
                STD  4,Y
                
                LDD  6,Y      ; Compute low part
                LDE  2,X
                EMUL          ; D * 2,X
                ADDE 4,Y
                PULM Y,K      ; Restore access pointer
                AIS  #4       ; Remove temp space
    }
}


/**************************************************************** _LMULS *****
 * SIGNED LONG MULTIPLICATION
 *----------------------------
 * Multiplies the signed value in registers E:D with the signed at the
 * address 0,Y:2,Y. The result is stored back in registers E:D.
 *
 * Here we can't use the fact that either E = 0 or 0,Y = 0, so we have to
 * implement the standard algorithm with 3 multiplictions.
 *
 *---------------------------------------------------------------------------*/

long _LMULS (void)

{
    asm JMP _LMULU;
}

/************************************************************** _LDIVG ******
 * GENERIC LONG DIVISION
 *-----------------------
 * See D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

#pragma NO_FRAME
void _LDIVG ()

{
#define dd      2,Z 
#define rl      4,Z 
#define tl      6,Z 
#define th      8,Z 
#define tx     10,Z 
#define bl     12,Z 
#define bh     14,Z 
#define al     16,Z 
#define ah     18,Z 
#define ax     20,Z 
#define t1     22,Z 
#define t0     24,Z

    asm {
                AIS  #-24   ; Reserve space for temporaries
                PSHM  Z
                TSZ         ; Set up access pointer
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1
                
        Alg0:               ; 0,Y and E are = 0. Then we can use
                            ; the (fast) IDIV instruction.
                LDX 2,Y   
                IDIV
                XGDX
                CLRE
                BRA end
                
        Alg1:               ; only 0,Y is 0, we can use the algorithm
                            ; explained at exercice 16 of chapter 4.3.1 in
                            ; D. KNUTH

                STD t0      ; Store D in t0
                TED         
                LDX 2,Y
                IDIV        ; E / 2,Y
                STX t1
                
                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; rest : D / 2,X
                XGDX        ; result in E:D
                LDE t1
                BRA end

        Alg2:               ; 0,Y is not equal to 0, so we have to use here
                            ; the complete long division.

                STD al      ; Firste store E:D in ah:al
                LDD 0,Y     ; and 0,Y:2,Y in bh:bl
                STD bh
                LDD 2,Y
                STD bl
                STE ah
                CLRW ax
                BMI calcQ   ; if ah:al >= 2^16, no need to normalize
                
        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register     
                BNE Divide
                LDX #1
                BRA Approx    
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1
                
                LDE al      ; Compute ax:ah:al = ah:al * dd
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax
                
                LDE bl      ; Compute bh:bl = bh:bl * d
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh
                
        calcQ:  LDX #0xFE   ; First estimation of the quotient
                LDE bh
                CPE ax
                BEQ l1      ; if bl = ax then register X = 0xFE
                LDE ax      ; else compute register X = ax:ah / bh
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl      ; store X in rl (q)
        
                            ; Check if the result is correct
                LDE bl      ; Compute tx:th:tl = rl * bh:bl
                LDD rl
                EMUL
                STD tl
                STE th
                
                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx
                
                LDD al      ; Compute tx:th:tl = ax:ah:al - tx:th:tl
                SUBD tl
                STD tl
                
                LDD ah
                SBCD th
                STD th
                
                LDD ax
                SBCD tx
                STD tx
                            ; Here tx:th:tl represents the rest of the
                            ; division (T = A - R * B)
                
                BMI loop    ; if T < 0 then result is too big, goto loop
                
                CLRE        ; Else, result is already correct, clear E and
                LDD rl      ; load the result in D
                
                BRA end

        loop:   DECW rl     ; Result is too big, so decrement it (R = R-1)
        
                            ; If the result is of 1 smaller, then the rest
                            ; (in T) is of B bigger
                            ; T0 = A - R*B
                            ; T1 = A - (R-1)*B
                            ; T1 = A - R*B + B = T0 + B
                            ; --                 ------

                LDD tl
                ADDD bl
                STD tl
                
                LDD th
                ADCD bh
                STD th
                
                LDD tx
                ADCD #0
                STD tx
                
                BMI loop    ; if T is still < 0, loop again
                            ; this branch is very very unlikely to be taken
                
                CLRE        ; The Result is now correct, clear E and
                LDD rl      ; load the result in D
                
        end:    LDZ 0,Z
                AIS #26     ; Clean up stack
    }
#undef dd
#undef rl  
#undef tl  
#undef th  
#undef tx  
#undef bl  
#undef bh  
#undef al  
#undef ah  
#undef ax  
#undef t1  
#undef t0  
}

/************************************************************** _LMODG ******
 * GENERIC LONG MODULO
 *---------------------
 * See generic long division and D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LMODG ()

{
#define dd       2,Z
#define rl       4,Z
#define rh       6,Z
#define tl       8,Z
#define th      10,Z
#define tx      12,Z
#define bl      14,Z
#define bh      16,Z
#define al      18,Z
#define ah      20,Z
#define ax      22,Z
#define t0      24,Z
    
    asm {
                AIS   #-24
                PSHM  Z
                TSZ
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1
                
        Alg0:   LDX 2,Y     ; See Generic Long Division
                IDIV
                CLRE        ; The result is already in D, just clear E
                BRA exit
                
        Alg1:   STD t0      ; See Generic Long Division
                TED         
                LDX 2,Y
                IDIV
                
                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; The result is in D, clear E
                CLRE
                BRA exit

        Alg2:   STD al
                LDD 0,Y
                STD bh
                LDD 2,Y
                STD bl
                STE ah

                CLR dd      ; dd = 0 means has not been normalized
                CLRW ax
                BMI calcQ
                
        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register     
                BNE Divide
                LDX #1
                BRA Approx    
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1
                
                LDE al
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax
                
                LDE bl
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh
                
        calcQ:  LDX #0xFE
                LDE bh
                CPE ax
                BEQ l1
                LDE ax
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl
        
                LDE bl
                LDD rl
                EMUL
                STD tl
                STE th
                
                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx
                
                LDD al
                SUBD tl
                STD tl
                
                LDD ah
                SBCD th
                STD th
                
                LDD ax
                SBCD tx
                STD tx
                
                BMI loop
                BRA end
                
        loop:   DECW rl
        
                LDD tl
                ADDD bl
                STD tl
                
                LDD th
                ADCD bh
                STD th
                
                LDD tx
                ADCD #0
                STD tx
                
                BMI loop

        end:    TST dd      ; Check if the operation has been normalized
                BNE deno    ; if yes, denormalize

                LDE th      ; If the operatinon has not been normalized
                            ; the result is in th:tl
                LDD tl
                BRA exit

        deno:   LDE tx      ; Else, we have to divide tx:th:tl by dd
                LDD th
                LDX dd
                EDIV
                STX rh
                TDE
                LDD tl
                LDX dd
                EDIV
                XGDX
                LDE rh
        exit:   LDZ 0,Z
                AIS #26
    }
#undef dd
#undef rl
#undef rh
#undef tl
#undef th
#undef tx
#undef bl
#undef bh
#undef al
#undef ah
#undef ax
#undef t0
}
                    
/************************************************************** _LDIVS ******/

#pragma NO_FRAME

void _LDIVS ()

{   
    asm {
                PSHM E,X,Y,Z,K
                TXY
                TSZ           ; 8,Z is 'sign', 9,Z is 'ySign'
                
                TSTE          ; test E
                BPL   ePos    ; if e > 0 then ok
                COME          ; else negate E:D
                NEGD
                SBCE  #-1
                
        ePos:   CLR   9,Z     ; Clear ySign
                TST   0,Y     ; test 0,Y
                BPL   yPos    ; if 0,Y > 0 then ok
                COMW  8,Z     ; Complement signs
                XGDX          ; else negate 0,Y:2,Y
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
                
        yPos:   JSR   _LDIVG  
                
        end:    TST   8,Z
                BPL   corrY   ; is sign is +, then keep E:D and check if a
                              ; correction of Y is nessessary.
                COME          ; else negate E:D
                NEGD
                SBCE  #-1
                
        corrY:  TST   9,Z
                BEQ   exit
                XGDX          ; y has been negated, reverse operation.
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
        exit:   PULM  X,Y,Z,K
                AIS   #2
    }
}
                    
/************************************************************** _LDIVU ******/

#pragma NO_FRAME

void _LDIVU ()

{
    asm {
                PSHM X,Y,Z,K
                TXY
                JSR _LDIVG  
                PULM X,Y,Z,K
    }
}

/************************************************************** _LMODS ******/

#pragma NO_FRAME

void _LMODS ()
{
    asm {
                PSHM  E,X,Y,Z,K  ; See _LDIVS for comments
                TXY
                TSZ
                                 ; the difference with _LDIVS is that here the
                                 ; sign of the result is the same as the sign
                                 ; of the first argument.                
                TSTE
                BPL   ePos
                COME
                NEGD
                SBCE  #-1
                
        ePos:   CLR   9,Z
                TST   0,Y
                BPL   yPos
                COM   9,Z
                XGDX
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
                
        yPos:   JSR   _LMODG
                
        end:    TST   8,Z
                BPL   corrY
                COME
                NEGD
                SBCE  #-1
                
        corrY:  TST   9,Z
                BEQ   exit
                XGDX
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
        exit:   PULM  X,Y,Z,K
                AIS   #2
    }
}

/************************************************************** _LMODU ******/


#pragma NO_FRAME

void _LMODU (void)
{
    asm {
                PSHM X,Y,Z,K
                TXY
                JSR _LMODG
                PULM X,Y,Z,K
    }
}

/****************************************************************************/

#pragma NO_FRAME

void _LINCPRE (void)
  /**** ++long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                INCW  2,X
                BNE   LoadIt
                INCW  0,X
    LoadIt:     LDD   2,X
                LDE   0,X
  }
}

#pragma NO_FRAME

void _LINCPST (void)
  /**** long++. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD   2,X
                LDE   0,X
                INCW  2,X
                BNE   End
                INCW  0,X
    End:
  }
}

#pragma NO_FRAME

void _LDECPRE (void)
  /**** --long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD  2,X       
                BNE  DecLo     
                DECW 0,X       
    DecLo:      ADDD #-1       
                STD  2,X       
                LDE  0,X       
  }              
}

#pragma NO_FRAME

void _LDECPST (void)
{
  asm {
                LDE   0,X
                LDD   2,X
                BNE   DecLo
                DECW  0,X
    DecLo:      DECW  2,X
  }
}      
    
/****************************************************************************/

#pragma NO_FRAME

void _FPINCPRE1 (void)
  /**** Far pointer preincrement. Argument is a far address in B/X. Increment
        is by 1. Returns in B/X the value of the pointer. */
{
  asm {
                PSHM  K
                TBXK
                INCW  1,X
                LDAB  0,X
                LDX   1,X
                PULM  K
  }
}

#pragma NO_FRAME

void _FPINCPREN (void)
  /**** Additional argument in E: size of base type */
{
  asm {
                PSHM  K
                TBXK
                ADDE  1,X
                STE   1,X
                LDAB  0,X
                XGEX
                PULM  K
  }
}

#pragma NO_FRAME

void _FPINCPST1 (void)
{
  asm {
                PSHM  K
                TBXK
                LDE   1,X
                LDAB  0,X
                INCW  1,X
                XGEX
                PULM  K
  }
}

#pragma NO_FRAME

void _FPINCPSTN (void)
{
  asm {
                PSHM  K
                TBXK
                LDD   1,X
                ADE
                XGDE
                STD   1,X
                LDAB  0,X
                XGEX
                PULM  K
  }
}

#pragma NO_FRAME

void _FPDECPRE1 (void)
{
  asm {
                PSHM  K
                TBXK
                DECW  1,X
                LDAB  0,X
                LDX   1,X
                PULM  K
  }
}

#pragma NO_FRAME

void _FPDECPST1 (void)
{
  asm {
                PSHM  K
                TBXK
                LDE   1,X
                DECW  1,X
                LDAB  0,X
                XGEX
                PULM  K
  }
}

/****************************************************************************/

#pragma NO_FRAME

void _LCMP (void)
  /**** Signed or unsigned comparison. Called in SMALL and MEDIUM memory models
        if one of the operands is far. (Generating the normal inline comparison
        gives troubles with the PUSH/PULL optimization!
          Arguments: E/D    a
                     Stack  b
          Result:    CCR according to a - b
   */
{
  asm {
            PSHM    X,K
            TSX
            SUBD    10,X
            SBCE    8,X
            BNE     End
            TSTD
    End:    PULM    X,K
  } /* end asm */;
} /* end _LCMP */

#pragma NO_FRAME

void _PCMP (void)
  /**** Far pointer comparison. Stack: b, B/E: a, CCR set according to a - b. */
{
  asm {
            PSHM    X,K
            TSX
            SUBE   10,X
            SBCB    9,X
            BNE     End
            TSTE
    End:    PULM    X,K
  } /* end asm */;
} /* end _PCMP */

#pragma NO_FRAME

void  _LSHL (void)
  /**** Long <<. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASLD
            ROLE
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHL */

#pragma NO_FRAME

void  _LSHR (void)
  /**** Long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHR */

#pragma NO_FRAME

void  _LSHRU (void)
  /**** Unsigned long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   LSRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHRU */

/******************************************************************************
  CASE PROCESSOR ROUTINES CALLED IMPLICITLY BY THE COMPILER
 ******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_CHECKED (void)
{
   asm {     
             TDE
             PSHM   Y,K     ; Save old XK and YK registers
             TSY
             LDX    6,Y     ; load table address
             LDAB   5,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             CPE   -2,X     ; compare with table size bound
             BCC    Default ; too big
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD    2,X     ; load offset
    Return:  ADDD   6,Y     ; add return address
             STD    6,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    5,Y
    ThatsIt: PULM   Y,K     ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
    Default: LDD    0,X
             BRA    Return;        
   }                      
}

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_DIRECT (void)
{
   asm {      
             TDE
             PSHM   Y,K     ; Save old XK,YK registers
             TSY
             LDX    6,Y     ; load table address
             LDAB   5,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD   -2,X     ; load offset
    Return:  ADDD   6,Y     ; add return address
             STD    6,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    5,Y
    ThatsIt: PULM   Y,K     ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
   }                     
}

 
/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_SEARCH(void)
{
   asm {
             TDE             ; Move value to look for into E
             PSHM   D,Y,K    ; Save old XK register and reserve space for temp
             TSY
             LDX    8,Y      ; load table address
             LDAB   7,Y      ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0  ; Disable interrupts
#endif
             TBXK
             LDD    -2,X     ; load table size -> D
             XGDE            ; E = table size, D = value to look for
             AIX    #2
    Loop:    ASRE            ; E = (E / 2) & 0xFFFC
             STE    4,Y      ; Save into temp: we'll need it below!!
             ANDE   #0xFFFC  ;
             BEQ    Check    ; if last possible entry -> Check
             CPD    E,X      ; is entry found
             BEQ    Found    ; yes, -> Found
             BLS    Loop     ; no and lower -> Loop
             AEX             ; X = X + E + 4
             AIX    #4       ; +4 because this entry already has been tested
             BRSET  5,Y, #0x02, Loop
             SUBE   #4       ; Correct E, but only if we generated
                             ; an odd size sub-table
             BRA    Loop    
                      
    Check:   CPD    E,X      ; entry found?
             BNE    Default  ; yes, 
    Found:   AEX             ; point to entry in table
             LDD    2,X      ; load offset
    Return:  ADDD   8,Y      ; add return address
             STD    8,Y      ; overwrite return address by destination address
             BCC    ThatsIt
             INC    7,Y
    ThatsIt: PULM   K,Y,D    ; Restore XK and YK registers and remove temp space
             RTI             ; jumps to case label and restores interrupt mask
             
    Default: LDX    8,Y      ; load table address
             LDD    0,X      ; load default offset
             BRA    Return         
   }
}

/******************************************************************************
  IEEE 32 bit arithmetic
 ******************************************************************************/

/**** IEEE 32 bit multiplication. Arguments:

        x   in E:D
        &y  in X

        result x * y (E:D * @X) is returned in E:D
 */

#pragma NO_FRAME

void _FMUL (void)
{
#define par1_hi      0,Z
#define par1_lo      2,Z
#define newExp       4,Z
#define carry        6,Z
#define result_hh    8,Z
#define result_hl   10,Z
#define result_lh   12,Z
#define result_ll   14,Z

    asm {
    
                PSHM  Z,K
                AIS   #-16
                TSZ
                STE par1_hi          ; Save x
                STD par1_lo
        
        ;----- Compute the exponent of the result
        
                ANDE #0x7F80        ; Extract first exponent
                BEQ zero
        
                LDD 0,X           ; Extract second exponent
                ANDD #0x7F80
                BEQ zero
        
                ADE                 ; Add the 2 exponents
                SUBE #0x3F80        ; Substract Bias
                        
        ;----- Compute the sign of the result
        
                LDD par1_hi
                EORD 0,X
                ANDD #0x8000        ; Mask the sign bit
        
                ADE
                STE newExp          ; newExp is now the exponent of the result
        
        ;----- Multiply the 2 mantissa and store the result in <result>
        
                LDE par1_lo         ; par1:2 * 2,X
                LDD 2,X
                EMUL
                STD result_ll
                STE carry
        
                LDE par1_lo          ; par1:2 * 0,X
                LDD 0,X
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                STD result_lh
                STE result_hl
        
                LDE par1_hi          ; par1:0 * 2,X
                ANDE #0x007F
                ORE  #0x0080
                LDD 2,X
                EMUL
                ADDD result_lh
                ADCE #0
                STD result_lh
                STE carry
                
                LDE par1_hi          ; par1:0 * 0,X
                ANDE #0x007F
                ORE  #0x0080
                LDD 0,X
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                ADDD result_hl
                ADCE #0
                STD result_hl
                STE result_hh
        
        ;----- Normalise the mantissa
        
                BRSET result_hl, #0x80, noDen
                LSLW result_lh
                LSLW result_hl
                LSLW result_hh
                LDD 11,Z
                LDE 9,Z
                ANDE #0x007F
                ADDE newExp
                BRA exit
                
        noDen:  LDD 11,Z
                LDE 9,Z
                ANDE #0x007F
                ADDE newExp
                ADDE #0x0080
                BRA exit
        
        
        zero:   CLRD
                CLRE
                
        exit:   AIS   #16
                PULM  Z,K
    }
#undef par1_hi    
#undef par1_lo    
#undef newExp     
#undef carry      
#undef result_HH  
#undef result_HL  
#undef result_LH  
#undef result_LL  
}  
   
/******************************************************************************
    IEEE 32 bit division. Arguments:
   
      x   in E:D
      &y  in X

    result x / y (E:D / @X) is returned in E:D
 */
 
void _FDIV (void)
{

#define par1_hi      0,Z
#define par1_lo      2,Z
#define u0           4,Z
#define u1           6,Z
#define u2           8,Z
#define u3          10,Z
#define u4          12,Z
#define v0          14,Z
#define v1          16,Z
#define r0          18,Z
#define r1          20,Z
#define r2          22,Z
#define q           24,Z
#define i           26,Z
#define j           28,Z
#define borrow      30,Z
#define rest        32,Z
#define newExp      34,Z
    
    asm {
                PSHM  Z,Y,K
                AIS   #-36
                TSZ
                STE par1_hi
                STD par1_lo
                
        ; Compute the new exponent                                
                                    
                LDD 0,X             ; load first word of second parameter
                ANDD #0x7F80        ; mask exponent
                BEQ div0            ; if zero, then division by zero error

                                    ; E is first word of first parameter
                ANDE #0x7F80        ; mask exponent
                BEQ zero            ; if zero, then result is zero
                
                SDE                 ; substract exponents
                ADDE #0x3F80        ; add bias

                LDD par1_hi         ; load first word of first parameter
                EORD 0,X            ; xor with the 1 st word of the 
                                    ; 2 nd param.
                ANDD #0x8000        ; mask the sign bit
                
                ADE                 ; add the sign to the new exponent
                STE newExp          ; newExp is now the exponent of 
                                    ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                CLRW u4
                CLRW u3
            
                LDAA 3,Z
                CLRB
                STD u2
                
                LDE 1,Z
                ORE #0x8000
                STE u1
                
                CLRW u0
                    
        
        ; Compute the normalized fractional part of the second parameter        
        ; and store it in v
                    
                LDAA 3,X
                CLRB                   
                STD v1
                 
                LDE 1,X
                ORE #0x8000
                STE v0

        ; Compute the quotient of the mantissa.
                
                CLRW j              ; counter = 0
                TZY
                AIY #4              ; Y points to u[j = 0]
                BRA loop

        mainLoop:
                TZY
                AIY #4              ; Y points to u[0]
                LDD j
                ASLD
                ADY                 ; Y points to u[j]

        ; Calculate the first estimation of q

        loop:   LDE 0,Y             ; E = u[j]
                CPE v0              ; IF u[j] == v[1] THEN
                BNE l1              ;
                LDX #0xFFFF         ;   X := b-1
                STX q
                    
                LDD 0,Y             ;   rest = u[j] + u[j+1]          
                ADDD 2,Y
                    
                BCS substr
                STD rest
                BRA fastCheck
                    
        l1:     LDD 2,Y             ; ELSE  D = u[j+1]
                LDX v0
                EDIV                ;   X = u[j]:u[j+1] / v[1]
             
        l2:     STX q
                STD rest            ;   rest

        ; Fast Check to see if q is too big

        fastCheck:
                LDE v1              ; E = v[2]
                LDD q               ; D = q
                EMUL
                    
                CPE rest
                BHI decq
                BNE substr
                    
                CPD 4,Y
                BLS substr
                    
        decq:   DECW q              ; Decrement q
                LDD rest            ; Adjust rest
                ADDD v0
                BCS substr
                STD rest
                BRA fastCheck

        ; substract q * v from u

        substr: CLRW borrow         ; Start with no borrow

                TZX
                AIX #16            ; X points to v[n]
                TZY
                AIY #12
                LDD j
                ASLD
                ADY                 ; Y points to u[j+n]

                LDE #1
                STE i               ; i = 1..0

        sl1:    LDE 0,X
                LDD q
                EMUL                ; E:D = q * v[n]

                ADDD borrow
                ADCE #0             ; E:D = q * v[n] + borrow

                STE borrow
                LDE 0,Y             ; E = u[j+n]
                SDE                 ; E = E - D
                STE 0,Y             ; Store new u[j+n]
                    
                BCC noB
                INCW borrow         ; correct borrow if necessary

        noB:    AIX #-2
                AIY #-2
                DECW i
                BPL sl1

                LDE 0,Y
                SUBE borrow
                STE 0,Y
                    
                BCC ok

        ; q was too big, we have to add v back to u

                TZX
                AIX #14              ; X points to v[1]

                TZY
                AIY #4
                LDD j
                ASLD
                ADY                 ; Y points to u[j]
                    
                LDD 2,X
                ADDD 2,Y
                STD 2,Y
                  
                LDD 0,X
                ADCD 0,Y
                STD 0,Y
                  
                DECW q

        ok:     TZY
                AIY #18
                LDD j
                ASLD
                ADY                 ; Y points to r[j]

                LDE q
                STE 0,Y             ; store q in r[j]

                INCW j
                LDE j
                CPE #2
                BLE mainLoop
                
        ; denormalize and move the result in par1. if necessary, correct
        ; the exponent of the result.
        
                TSTW r0
                BNE dnl1
                LDE 19,Z
                ANDE #0x007F
                ADDE newExp
                SUBE #0x0080
                LDD 21,Z
                BRA end
                    
        dnl1:   
                LDE 19,Z
                LDD 21,Z
                LSRE
                RORD
                ANDE #0x007F
                ADDE newExp
                BRA end
                 
        div0:   LDX #0             ; Simulate a division by 0
                EDIV
                        
        zero:   CLRE               ; result is zero
                CLRD
                
        end:    AIS   #36
                PULM  Y,Z,K

    }
#undef par1_hi   
#undef par1_lo   
#undef u0        
#undef u1        
#undef u2        
#undef u3        
#undef u4        
#undef v0        
#undef v1        
#undef r0        
#undef r1        
#undef r2        
#undef q         
#undef i         
#undef j         
#undef borrow    
#undef rest
#undef newExp      
}

/******************************************************************************
  IEEE 32 bit addition. Arguments:

    x   in E:D
    &y  in X

    result x + y (E:D + @X) is returned in E:D
 */
 
void _FADD (void)
{
#define par1_hi      0,Z
#define par1_lo      2,Z
#define par2_hi      4,Z
#define par2_lo      6,Z
#define newExp       8,Z
#define sign1       10,Z
#define sign2       12,Z
#define resSign     14,Z
    
    asm {
                PSHM  Y,Z,K
                AIS   #-16
                TSZ
                CLR   resSign
                STE   par1_hi        ; Save par1
                STD   par1_lo
                ANDE  #0x7F80       ; Mask Exponent
                BEQ   uvfl          ; x == 0, return par2 (y)
                LDD   2,X
                STD   par2_lo
                LDD   0,X
                STD   par2_hi
                ANDD  #0x7F80
                BNE   not0          ; y == 0, return par1 (x)
                LDE   par1_hi
                LDD   par1_lo
                BRA   end
        not0:   SDE                 ; compute exp1 - exp2
                ASLE                ; carry set means D negativ
                TED
                XGAB
                TDE                 ; E = # of bits to shift
                TZX                 ; initialise X and Y pointers
                TZY
                BCC   shP2             
        shP1:   AIY   #4         ; and compute par2 + par1
                NEGE
                ANDE  #0x00FF
                BRA   cont
        shP2:   AIX   #4         ; exp1 > exp2 => shift par2
        cont:   CPE   #24
                BLT   noUVFL
        uvfl:   LDE   0,Y           ; We'd shift @X completely away
                LDD   2,Y
                BRA   end                
        noUVFL: LDAA  0,X
                STAA  sign2
                LDD   0,Y
                STAA  sign1
                ANDD  #0x7F80
                STD   newExp
                CLR   par1_hi
                BSET  1,Z,#0x80  ; Hidden bit
                CLR   par2_hi
                BSET  5,Z,#0x80  ; Hidden bit  

        ; shift the mantissa pointed to by X right by E bits

        chk16:  CPE   #16           ; Try to shift a bloc of 16 bits
                BLT   chk8
        sh16:   LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #16
        chk8:   CPE   #8            ; Try to shift a bloc of 8 bits
                BLT   chk1           
        sh8:    LDD   1,X
                STD   2,X
                CLR   1,X
                SUBE  #8
                BRA   chk1
        sh1:    LSR   1,X        
                RORW  2,X
        chk1:   SUBE  #1
                BGE   sh1
        madd:   BRCLR sign2,#0x80,xPos
                LDD   #0             ; if @X is negative...
                SUBD  2,X            ; ...negate the mantissa
                STD   2,X
                LDD   #0
                SBCD  0,X
                STD   0,X
        xPos:   BRCLR sign1,#0x80,yPos
                LDD   #0             ; if @Y is negative...
                SUBD  2,Y            ; ...negate the mantissa
                STD   2,Y
                LDD   #0
                SBCD  0,Y
                STD   0,Y
        yPos:   LDD   2,X            ; Add them!
                ADDD  2,Y
                STD   par1_lo
                LDD   0,X
                ADCD  0,Y
                STD   par1_hi
                BPL   normal
                LDD   #0             ; Resulting mantissa is negative
                SUBD  par1_lo
                STD   par1_lo
                LDD   #0
                SBCD  par1_hi
                STD   par1_hi
                COM   resSign        ; Result is negative!!
        normal: LDE   newExp         
                BRCLR par1_hi,#0x01,subN
                LSRW  par1_hi        ; Overflow: shift right by 1...
                RORW  par1_lo
                ADDE  #0x80          ; ... and increment exponent
        subN:   BRSET 1,Z, #0x80, pack
                LDD   par1_hi           ; De-normalized: test for zero
                BNE   try1
                LDD   par1_lo
                BNE   mov16
                TDE                  ; Result is zero
                BRA   end
        mov16:  LDAA  par1_lo 
                BNE   mov8
                LDD   par1_lo         ; hi 3 bytes are 0
                STD   par1_hi
                CLRW  par1_lo                
                SUBE  #0x800
                BRA   try1
        mov8:   LDD   par1_lo         ; only hi 2 bytes are 0
                STD   1,Z
                CLR   3,Z
                SUBE  #0x400
                BRA   try1                        
        mov1:   LSLW  par1_lo         ; Shift until normalized
                ROL   1,Z
                SUBE  #0x80
        try1:   BRCLR 1,Z,#0x80,mov1
        pack:   BCLR  1,Z,#0x80
                LDD   par1_lo
                ORE   par1_hi
                BRCLR resSign,#0x80, end
                ORE   #0x8000
        end:    AIS   #16
                PULM  Y,Z,K
    }
#undef par1_hi   
#undef par1_lo   
#undef par2_hi   
#undef par2_lo   
#undef newExp    
#undef sign1     
#undef sign2     
#undef resSign   
}


/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in X

    result x - y (E:D - @X) is returned in E:D

    Note: @X mustn't be changed, therefore y is copied (and the sign bit
          inverted).
 */

#pragma NO_FRAME
 
void _FSUB (void)
{
    asm {
                PSHM D
                LDD  2,X
                PSHM D
                LDD  0,X
                EORA #0x80
                PSHM D
                TSX
                LDD  4,X
                JSR _FADD
                AIS  #6
    }
}

/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in X

    result x - y (E:D - @X) is returned in E:D

    Note: @X may be changed, therefore the sign bit is inverted in place.
 */

#pragma NO_ENTRY
#pragma NO_EXIT
 
void _FSUB0 (void)
{
  asm {
                PSHM D
                LDD  0,X
                EORA #0x80
                STD  0,X
                PULM D
                JMP  _FADD
  }
}

/******************************************************************************
  Auxiliary routine: right shift (B, E, #0:8) by 31-A bits and return result
  in E:D
 */

#define FLT_BIAS      127

#pragma NO_FRAME

void _frshift (void)
{
  asm {
                AIS    #-4
                TSX
                NEGA                    ;   A := 31 - A
                ADDA   #31
                CMPA   #24      
                BCS    Smaller24        ;   IF (A >= 24) THEN
                STAB   3,X              ;     l := (mantissa >> 24);
                CLRW   0,X
                CLR    2,X
                SUBA   #24              ;     DEC (A, 24);
                BRA    Smaller8         ;   ELSE
    Smaller24:  STAB   0,X              ;     l := left adjusted mantissa
                STE    1,X
                CLR    3,X
                CMPA   #16
                BCS    Smaller16        ;     IF (A >= 16) THEN
                LDE    0,X              ;       l.lo := l.hi
                STE    2,X
                CLRW   0,X              ;       l.hi := 0;
                SUBA   #16              ;       DEC (A, 16);
                BRA    Smaller8
    Smaller16:  CMPA   #8
                BCS    Smaller8         ;     ELSIF (A >= 8) THEN
                LDAB   2,X              ;       Move l right 8 bits by swapping bytes
                STAB   3,X
                LDE    0,X
                STE    1,X
                CLR    0,X
                SUBA   #8               ;       DEC (A, 8);
                                        ;     END;
    Smaller8:   LDE    0,X              ;   END;
                STAA   0,X
                LDD    2,X   
                TST    0,X
                BEQ    EndLoop          ;   IF (count > 0) THEN
                                        ;     /* Note: {count < 8} */
    Loop:       LSRE                    ;     REPEAT
                RORD                    ;       result := result >> 1
                DEC    0,X              ;       DEC (count);
                BNE    Loop             ;     UNTIL (count = 0);
                                        ;   END;
    EndLoop:    AIS    #4
  } /* end asm */
} /* end _frshift */

/******************************************************************************
  IEEE-32 to LONG conversion                                   TW, 03/01/93
    E:D    float                                
    E:D    result
 */

#pragma NO_FRAME

void _FSTRUNC (void)
{
  asm {
                PSHM   D,X,K
                TSX
                XGDE                    ; Move hi word to D (easier to unpack exp)
                STAA   4,X              ; Save sign bit
                LSLD                    ; Exp in A
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
                CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #31              ; ELSIF (exp >= 31) THEN
                BCS    Ok
                LDE    #0x8000          ;   IF (f < 0.0) THEN
                CLRD                    ;     RETURN MIN (LONGINT);
                BRSET  4,X, #0x80, End  ;   ELSE
                COME                    ;     RETURN MAX (LONGINT);
                COMD                    ;   END;
                BRA    End              ; ELSE
    Ok:         JSR    _frshift
                TSX
                BRCLR  4,X, #0x80, End  ;   IF (f < 0.0) THEN
                NEGE                    ;     result := -result;
                NEGD
                SBCE   #0               ;   END;
    End:        PULM   X,K              ; END
                AIS    #2            
  } /* end asm */;
} /* end FSTRUNC */ 

/******************************************************************************
  IEEE-32 to UNSIGNED LONG conversion                          TW, 03/01/93
    E:D    float                                
    E:D    result
 */

void _FUTRUNC (void)
{
  asm {
                PSHM   X,K
                XGDE                    ; Move hi word to D (easier to unpack exp)
                LSLD                    ; Exp in A
                BCS    RetZero          ; IF (f < 0.0) THEN RETURN 0
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
    RetZero:    CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #32              ; ELSIF (exp >= 32) THEN
                BCS    Ok
                LDD    #0xFFFF          ;   RETURN MAX (LONGCARD);
                TDE
                BRA    End              ; END
    Ok:         JSR    _frshift 
    End:        PULM   X,K
  } /* end asm */;
} /* end FUTRUNC */ 


/******************************************************************************
  Auxiliary routine: shift (E, D) until bit #15 of E is 1. Also decrement X
  for each bit shifted. Then, pack it together and return result in D/E!
 */

#define F_EXP_MAX     158

#pragma NO_FRAME

void _fnormant (void)
{
  asm {
                LDX   #F_EXP_MAX
                TSTE
                BMI   Pack
                BNE   Shift
                AIX   #-16              ; Optimize for shift counts > 16
                TDE
                CLRD
                TSTE
                BMI   Pack
    Shift:      AIX   #-1               ; Shift until negative (normalized)
                LSLD
                ROLE
                BPL   Shift
    Pack:       ANDE  #0x7FFF            ; Clear hidden bit
                PSHM  D, E
                XGDX                    ; A = undef,   B = exp
                XGAB                    ; A = exp,     B = undef
                CLRB                    ; A = exp,     B = 0
                LSRD                    ; A = 0/exp:7, B = exp:1/0:7
                PSHM  K
                TSX
                ORAB  2,X               ; A = 0/exp:7, B = exp:1/mant:7 ==> D = res.hi
                LDE   3,X               ; E = res.lo
                PULM  K
                AIS   #4
  } /* end asm */;
} /* end _fnormant */

/******************************************************************************
  LONG to IEEE-32                                              TW, 03/01/93
    E:D    long                                
    E:D    result
 */

#pragma NO_FRAME

void _FSFLOAT (void)
{
  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    PSHM  E                 ; Set negative flag
                BPL   Pos
                NEGE                    ; Negate mantissa
                NEGD
                SBCE  #0
    Pos:        JSR   _fnormant
                PSHM  X,K
                TSX
                BRCLR 4,X, #0x80, Swap  ; IF (long < 0) THEN
                ORAA  #0x80             ;   Set sign bit
    Swap:       XGDE                    ; END
                PULM  X,K
                PULM  E   /* << ES 12/14/96: was missing! */
    End:        /* << ES 12/14/96: was with PULM X,K above! */
  } /* end asm */;
} /* end _FSFLOAT */


/******************************************************************************
  UNSIGNED LONG to IEEE-32                                     TW, 03/01/93
    E:D    long                                
    E:D    result
 */

#pragma NO_FRAME

void _FUFLOAT (void)
{
  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    JSR   _fnormant
                XGDE
    End:
  } /* end asm */;
} /* end _FUFLOAT */

/******************************************************************************
    E:D   x
    @X    y

    Set flags according to x-y
 */

#pragma NO_FRAME

void _FCMP (void)
{
  asm {
                    PSHM  Y
                    XGDY
                    TED
                    EORD  0,X
                    BMI   InverseComp     /*  different signs */
    SameSign:       TSTE
                    BPL   Pos
                    /* Both are negative: compare the other way 'round */
                    /* Or numbers have different signs */
    InverseComp:    ;
                    LDD   0,X
                    XGDE
                    SDE
                    BNE   End
                    XGDY
                    LDE   2,X
                    SDE
                    BRA   End
                    /* Both are positive */
    Pos:            CPE   0,X
                    BNE   End
                    CPY   2,X
    End:            PULM  Y
  } /* end asm */;
} /* end _FCMP */


/******************************************************************************
  IEEE 64 bit arithmetic
 ******************************************************************************/

typedef struct {
  unsigned int hh, hl, lh, ll;
} DOUBLE;

/******************************************************************************
  IEEE 64 bit addition. Arguments are on the stack; the result (par1 + par2) is
  returned in par1 (i.e. the stack is overwritten).
 */

#pragma NO_FRAME

void _DADD (void)
{

#define resSign     1,Z
#define sign2       2,Z
#define sign1       3,Z
#define newExp      4,Z
  /* Return address occupies 4 bytes at 12,Z */
#define par2_hh    16,Z
#define par2_hl    18,Z
#define par2_lh    20,Z
#define par2_ll    22,Z
#define par1_hh    24,Z
#define par1_hl    26,Z
#define par1_lh    28,Z
#define par1_ll    30,Z

    asm {     
                PSHM  Y,Z,K
                AIS   #-6
                TSZ
                CLR   resSign             ; result is assumed to be positive
                LDE   par1_hh             ; load first word of first parmeter
                ANDE  #0x7FF0             ; mask exponent
                BNE   not0
                TZY                       ; par1 = 0, so copy par2 in par1
                AIY   #16
                BRA   uvfl
        not0:   LDD   par2_hh             ; load first word of second parameter
                ANDD  #0x7FF0             ; mask exponent
                BEQ   end                 ; if zero, return par1 as result
                SDE                       ; compute exp1 - exp2
                TZX                       ; prepare X and Y
                TZY
                BPL   shP2             
        shP1:   AIX   #24                 ; exp1 < exp2 => shift par1
                AIY   #16                 ; and compute par2 + par1
                NEGE     
                BRA   cont
                
        shP2:   AIX   #16                 ; exp1 > exp2 => shift par2
                AIY   #24                 ; and compute par1 + par2
                
        cont:   CPE   #0x350
                BLT   noUVFL
        uvfl:   LDD   0,Y                 ; We'd shift @X completely away...
                STD   par1_hh             ; ... return @Y
                LDD   2,Y
                STD   par1_hl
                LDD   4,Y
                STD   par1_lh
                LDD   6,Y
                STD   par1_ll
                BRA   end
        noUVFL: LDAA  0,X
                STAA  sign2               ; Store sign bit
                LDD   0,Y
                STAA  sign1               ; Store sign bit
                ANDD  #0x7FF0
                STD   newExp              ; Store new exponent
                BCLRW par1_hh,#0xFFF0
                BSET  25,Z,#0x10        ; Hidden bit
                BCLRW par2_hh,#0xFFF0
                BSET  17,Z,#0x10        ; Hidden bit
                
        ; shift the mantissa pointed to by X right by E bits
        
                BRA   chk16
        sh16:   LDD   4,X
                STD   6,X
                LDD   2,X
                STD   4,X
                LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #0x100              ; E -= 16
        chk16:  CPE   #0x100              ; E >= 16 ??
                BGE   sh16
        chk8:   CPE   #0x80               ; E >= 8
                BLT   chk1
        sh8:    LDD   5,X
                STD   6,X
                LDD   3,X
                STD   4,X
                LDD   1,X
                STD   2,X
                CLRW  0,X
                SUBE  #0x80               ; E -= 8
                BRA   chk1
        sh1:    LSRW  0,X        
                RORW  2,X
                RORW  4,X
                RORW  6,X
        chk1:   SUBE  #0x10               ; E -= 1
                BGE   sh1
                
        madd:   BRCLR sign2,#0x80,xPos
                CLRD                      ; Negate @X
                CLRE
                SUBD  6,X
                STD   6,X
                TED
                SBCD  4,X
                STD   4,X
                TED
                SBCD  2,X
                STD   2,X
                TED
                SBCD  0,X
                STD   0,X
                
        xPos:   BRCLR sign1,#0x80,yPos
                CLRD                      ; Negate @Y
                CLRE
                SUBD  6,Y
                STD   6,Y
                TED
                SBCD  4,Y
                STD   4,Y
                TED
                SBCD  2,Y
                STD   2,Y
                TED
                SBCD  0,Y
                STD   0,Y
                
        yPos:   LDD   6,X                 ; Add them
                ADDD  6,Y
                STD   par1_ll
                LDD   4,X
                ADCD  4,Y
                STD   par1_lh
                LDD   2,X
                ADCD  2,Y
                STD   par1_hl
                LDD   0,X
                ADCD  0,Y
                STD   par1_hh
                BPL   normal
                CLRD                      ; Result is negative: negate and set flag
                CLRE
                SUBD  par1_ll
                STD   par1_ll
                TED
                SBCD  par1_lh
                STD   par1_lh
                TED
                SBCD  par1_hl
                STD   par1_hl
                TED
                SBCD  par1_hh
                STD   par1_hh
                COM   resSign             ; Result is negative!!
                
        normal: LDE   newExp               
                BRCLR 25,Z,#0x20,subN
                LSRW  par1_hh             ; Overflow: shift right by 1
                RORW  par1_hl
                RORW  par1_lh
                RORW  par1_ll
                ADDE  #0x10               ; and correct exponent
        subN:   BRSET 25,Z, #0x10, pack ; De-normalized: normalize
                LDD   par1_hh             ; Test for zero
                BNE   shN
                LDD   par1_hl
                BNE   shN
                LDD   par1_lh
                BNE   mov16
                LDD   par1_ll
                BEQ   end                 ; Return zero
        mov32:  LDD   par1_ll             ; HH, HL and LH words are zero
                STD   par1_hl
                CLRW  par1_lh
                CLRW  par1_ll
                SUBE  #0x200              ; exp -= 32
                BRA   shN
        mov16:  LDD   par1_lh             ; HH and HL words are zero
                STD   par1_hl
                LDD   par1_ll
                STD   par1_lh
                CLRW  par1_ll
                SUBE  #0x100              ; exp -= 16
        shN:    BRSET 25,Z,#0x10,pack
                LSLW  par1_ll
                ROLW  par1_lh
                ROLW  par1_hl
                ROL   25,Z
                SUBE  #0x10               ; exp -= 1
                BRA   shN      
        pack:   BCLR  25,Z,#0x10        ; Hidden bit
                ORE   par1_hh
                BRCLR resSign,#0x80, pos
                ORE   #0x8000 
        pos:    STE   par1_hh
        end:    AIS   #6
                PULM  Y,Z,K
    } /* end asm */;

#undef resSign
#undef sign2
#undef sign1
#undef newExp
#undef par2_hh
#undef par2_hl
#undef par2_lh
#undef par2_ll
#undef par1_hh
#undef par1_hl
#undef par1_lh
#undef par1_ll

} /* end _DADD */

#pragma NO_EXIT
#pragma NO_FRAME

void _DSUB (void)
  /**** Calculate par2 - par1 and return the result in par1 */
{
  asm {
                PSHM  X,K
                TSX   
                LDAA  16,X                ; Negate sign
                EORA  #0x80
                STAA  16,X
                PULM  X,K                 ; Correct stack...
                JMP   _DADD               ; ... and add!
  } /* end asm */;
} /* end _DSUB */

/******************************************************************************
  IEEE 64 bit multiplication. Arguments are on the stack; the result (par1 * par2)
  is returned in par1 (i.e. the stack is overwritten).
 */

#pragma NO_FRAME

void _DMUL (void)
{

#define counter1    6,Z
#define counter2    8,Z
#define carry      10,Z
#define temp       12,Z
#define result0    14,Z
#define result1    16,Z
#define result2    18,Z
#define result3    20,Z
#define result4    22,Z
#define result5    24,Z
#define result6    26,Z
#define result7    28,Z
#define newExp     30,Z
  /**** Return address occupies 4 bytes at address 32,Z */
#define par2_hh    36,Z
#define par2_hl    38,Z
#define par2_lh    40,Z
#define par2_ll    42,Z
#define par1_hh    44,Z
#define par1_hl    46,Z
#define par1_lh    48,Z
#define par1_ll    50,Z

    asm {
                AIS  #-26 
                PSHM Y,Z,K
                TSZ
                
        ; Compute the new exponent
                
                LDD par1_hh        ; load first word of first parameter
                ANDD #0x7FF0       ; mask exponent
                BEQ zero           ; if zero, then result is zero
                
                LDE par2_hh        ; load first word of second parameter
                ANDE #0x7FF0       ; mask exponent
                BEQ zero           ; if zero, then result is zero
                
                ADE                ; add the 2 exponents
                SUBE #0x3FF0       ; substract bias
                
                LDD par1_hh        ; load first word of first parameter
                EORD par2_hh       ; xor with the 1 st word of the 2 nd param.
                ANDD #0x8000       ; mask the sign bit
                
                ADE                ; add the sign to the new exponent
                STE newExp         ; newExp is now the exponent of the result

        ; Extract mantissa of par1
               
                LDE par1_hh
                ANDE #0x000F
                ORE  #0x0010
                STE par1_hh
                
        ; Extract mantissa of par2
        
                LDE par2_hh
                ANDE #0x000F
                ORE  #0x0010
                STE par2_hh
                
        ; First Multiply par2 with the last word of par1 and store the
        ; result in the array result.
        
                CLRW carry         ; carry is initialise to 0
                
                LDD par1_ll
                STD temp           ; temp := last word of the mantissa of par1
                
                TZY
                AIY #28            ; Y points to where the result must be written

                TZX
                AIX #42            ; X points to the last word of the mantissa of par2
                LDD #3             ; iterate 4 times (3..0)
                STD counter2

        lab0:   LDE temp           ; begin of the loop
                LDD 0,X
                EMUL               ; Multiply temp with a word of par2
                ADDD carry         ; Add carry to low word of result
                ADCE #0            ; Adjust high word of result if the last
                                   ; operation produces a carry

                STD 0,Y            ; store low word of result
                STE carry          ; store high word in the carry
                AIX #-2            ; decrement pointer X by 2
                AIY #-2            ; decrement pointer Y by 2
                DECW counter2      ; DEC(counter2)
                BPL lab0           ; end of loop

                LDD carry
                STD 0,Y            ; Store carry

        ; Then multiply par2 with the other 3 words of par1 and adjust the
        ; result
                
                LDD #4             ; iterate 3 times (3,2,0)
                STD counter1

        lab1:   CLRW carry         ; begin the outer loop with carry = 0
                
                TZX
                AIX #44            ; X points to the begin of par1
                                   ; XK already correct!
                LDE counter1
                LDD E,X
                STD temp           ; temp := word of par1 with which we 
                                   ; will multiply par2
                TZY                   
                AIY #22
                AEY                ; Y points where the result must be written
                
                AIX #-2            ; X point to the last word of par2
                                   ; XK already correct!
                LDD #3             ; iterate 4 times (3..0)
                STD counter2

        lab2:   LDE temp           ; begin of the loop
                LDD 0,X
                EMUL               ; multiply temp with a word of par2
                ADDD carry         ; add carry to low word of result
                ADCE #0            ; Adjust high word of result if the last
                                   ; operation produces a carry

                ADDD 0,Y           ; add the low word with the last result
                STD 0,Y            ; store the noew word of the result
                ADCE #0            ; adjust high word if carry
                STE carry          ; store high word in carry

                AIX #-2            ; decrement pointer X by 2
                AIY #-2            ; decrement pointer Y by 2
                DECW counter2      ; DEC(counter2)
                BPL lab2           ; end of inner loop

                LDD carry
                STD 0,Y            ; store carry
                
                DECW counter1
                DECW counter1      ; decrement counter1 by 2
                BPL lab1           ; end of outer loop

        ; align the mantissa of the result
                
                BRCLR result1,#0x02,normal
                
                LDD newExp         ; If the bit 106 of the result if set
                ADDD #0x0010       ; then increment the exponent
                STD newExp
                
                LSRW result1      ; and shift the result
                RORW result2
                RORW result3
                RORW result4

        normal: LDD #3             ; shift the result 4 times (3..0)
        adjust: LSRW result1
                RORW result2
                RORW result3
                RORW result4
                ADDD #-1
                BPL adjust
                
                BCLRW result1,#0xFFF0  ; clear implicit 1

        ; copy result in par1
                
                LDD result1
                ADDD newExp         ; add sign and exponent
                STD par1_hh
                LDD result2
                STD par1_hl
                LDD result3
                STD par1_lh
                LDD result4
                STD par1_ll
                BRA end

        ; copy zero in par1
                
        zero:   CLRD
                STD par1_hh
                STD par1_hl
                STD par1_lh
                STD par1_ll

        end:    PULM Y,Z,K 
                AIS  #26
    }
#undef counter1 
#undef counter2 
#undef carry    
#undef temp     
#undef result0  
#undef result1  
#undef result2  
#undef result3  
#undef result4  
#undef result5  
#undef result6  
#undef result7  
#undef newExp   
#undef par2_hh  
#undef par2_hl  
#undef par2_lh  
#undef par2_ll  
#undef par1_hh  
#undef par1_hl  
#undef par1_lh  
#undef par1_ll  
}

/******************************************************************************
  IEEE 64 bit divison. Arguments are on the stack; the result (par1 / par2)
  is returned in par2 (i.e. the stack is overwritten).
 */

#pragma NO_FRAME

void _DDIV (void)
{

#define rest         6,Z
#define borrow       8,Z
#define j           10,Z
#define i           12,Z
#define q           14,Z
#define r0          16,Z
#define r1          18,Z
#define r2          20,Z
#define r3          22,Z
#define r4          24,Z
#define v0          26,Z
#define v1          28,Z
#define v2          30,Z
#define v3          32,Z
#define u0          34,Z
#define u1          36,Z
#define u2          38,Z
#define u3          40,Z
#define u4          42,Z
#define u5          44,Z
#define u6          46,Z
#define u7          48,Z
#define u8          50,Z
#define newExp      52,Z
    /**** Return address occupies 4 bytes at offset 54 */
#define par1_hh     58,Z
#define par1_hl     60,Z
#define par1_lh     62,Z
#define par1_ll     64,Z
#define par2_hh     66,Z
#define par2_hl     68,Z
#define par2_lh     70,Z
#define par2_ll     72,Z
 
    asm {
                    AIS  #-48
                    PSHM Y,Z,K
                    TSZ
                    
        ; Compute the new exponent                                

                    LDD par2_hh        ; load first word of second parameter
                    ANDD #0x7FF0       ; mask exponent
                    BEQ div0           ; if zero, then division by zero error

                    LDE par1_hh        ; load first word of first parameter
                    ANDE #0x7FF0       ; mask exponent
                    BEQ zero           ; if zero, then result is zero
                
                    SDE                ; substract exponents
                    ADDE #0x3FF0       ; add bias

                    LDD par1_hh        ; load first word of first parameter
                    EORD par2_hh       ; xor with the 1 st word of the
                                       ; 2 nd param.
                    ANDD #0x8000       ; mask the sign bit
                
                    ADE                ; add the sign to the new exponent
                    STE newExp         ; newExp is now the exponent of 
                                       ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                    CLRD
                    STD u8
                    STD u7
                    STD u6
                    STD u5
                    STAA 43,Z
                
                    LDAA 65,Z
                    LSLA
                    STAA u4
                
                    LDE 63,Z
                    ROLE
                    STE u3
                
                    LDE 61,Z
                    ROLE
                    STE u2
                    
                    LDE 59,Z
                    ANDE #0x0FFF       ; remove the sign and the exponent
                    ORE #0x1000        ; add the implicit 1
                    ROLE
                    STE u1               
                
                    CLRW u0            ; was CLR u:0 !!!!!
                
                    LSLW u4
                    ROLW u3
                    ROLW u2
                    ROLW u1
                
                    LSLW u4
                    ROLW u3
                    ROLW u2
                    ROLW u1
        
        ; Compute the normalized fractional part of the second parameter        
        ; and store it in v
        
                    CLR 33,Z
                    
                    LDAA 73,Z
                    LSLA
                    STAA v3
                 
                    LDE 71,Z
                    ROLE
                    STE v2
                
                    LDE 69,Z
                    ROLE
                    STE v1
                
                    LDE 67,Z
                    ANDE #0x0FFF       ; remove sign and exponent 
                    ORE #0x1000        ; add implicit 1
                    ROLE
                    STE v0
                
                    LSLW v3
                    ROLW v2
                    ROLW v1
                    ROLW v0
                
                    LSLW v3
                    ROLW v2
                    ROLW v1
                    ROLW v0


        ; Compute the quotient of the mantissa.
                
                    CLRW j              ; counter = 0
                    
                    TZY
                    AIY #34             ; Y points to u[j = 0]
                    BRA loop

        mainLoop:   TZY
                    AIY #34             ; Y points to u[0]
                    LDD j
                    ASLD
                    ADY

        ; Calculate the first estimation of q

        loop:       LDE 0,Y             ; E = u[j]
                    CPE v0              ; IF u[j] == v[1] THEN
                    BNE l1              ;
                    LDX #0xFFFF         ;   X := b-1
                    STX q
                    
                    LDD 0,Y             ;   rest = u[j] + u[j+1]          
                    ADDD 2,Y
                    
                    BCS substr
                    STD rest
                    BRA fastCheck
                    
        l1:         LDD 2,Y             ; ELSE  D = u[j+1]
                    LDX v0
                    EDIV                ;   X = u[j]:u[j+1] / v[1]

        l2:         STX q
                    STD rest

        ; Fast Check to see if q is too big

        fastCheck:  LDE v1              ; E = v[2]
                    LDD q               ; D = q
                    EMUL
                    
                    CPE rest
                    BHI decq
                    BNE substr
                    
                    CPD 4,Y
                    BLS substr
                    
        decq:       DECW q              ; Decrement q
                    LDD rest            ; Adjust rest
                    ADDD v0
                    BCS substr
                    STD rest
                    BRA fastCheck

        ; substract q * v from u

        substr:     CLRW borrow         ; Start with no borrow

                    TZX
                    AIX #32            ; X points to v[n]

                    TZY
                    AIY #42
                    LDD j
                    ASLD
                    ADY                 ; Y points to u[j+n]

                    LDE #3
                    STE i               ; i = 3..0

        sl1:        LDE 0,X
                    LDD q
                    EMUL                ; E:D = q * v[n]

                    ADDD borrow
                    ADCE #0             ; E:D = q * v[n] + borrow

                    STE borrow
                    LDE 0,Y             ; E = u[j+n]
                    SDE                 ; E = E - D
                    STE 0,Y             ; Store new u[j+n]
                    
                    BCC noB
                    INCW borrow         ; correct borrow if necessary

        noB:        AIX #-2
                    AIY #-2
                    DECW i
                    BPL sl1

                    LDE 0,Y
                    SUBE borrow
                    STE 0,Y
                    
                    BCC ok

        ; q was too big, we have to add v back to u

                    TZX
                    AIX #26             ; X points to v[1]

                    TZY
                    AIY #34
                    LDD j
                    ASLD
                    ADY                 ; Y points to u[j]

                    LDD 6,X
                    ADDD 6,Y
                    STD 6,Y
                    
                    LDD 4,X
                    ADCD 4,Y
                    STD 4,Y
                    
                    LDD 2,X
                    ADCD 2,Y
                    STD 2,Y
                    
                    LDD 0,X
                    ADCD 0,Y
                    STD 0,Y
                    
                    DECW q

        ok:         TZY
                    AIY #16
                    LDD j
                    ASLD
                    ADY                ; Y points to r[j]

                    LDE q
                    STE 0,Y             ; store q in r[j]

                    INCW j
                    LDE j
                    CPE #4
                    BLE mainLoop
                
                
        ; denormalize and move the result in par2. if necessary, correct
        ; the exponent of the result.
        
        

        exit:       TSTW r0
                    BNE dnl1
                    LDD newExp
                    SUBD #0x0010
                    STD newExp
                    BRA dnl2
                    
        dnl1:       LSRW r0
                    RORW r1
                    RORW r2
                    RORW r3
                    RORW r4
                    
        dnl2:       LSRW r1
                    RORW r2
                    RORW r3
                    RORW r4
                    
                    LSRW r1
                    RORW r2
                    RORW r3
                    RORW r4
                    
                    LDD r1
                    LSRD
                    ANDD #0x0FFF;
                    STD 67,Z
                    
                    LDD r2
                    RORD
                    STD 69,Z
                    
                    LDD r3
                    RORD
                    STD 71,Z
                    
                    LDD r4
                    RORD
                    STAA 73,Z
                    
                    LDE par2_hh
                    ANDE #0x000F
                    ADDE newExp
                    STE par2_hh
                                    
                    BRA end
                    
        div0:       LDX #0             ; Simulate a division by 0
                    EDIV
                        
        zero:       CLRD
                    STD par2_hh        ; result is zero
                    STD par2_hl
                    STD par2_lh
                    STD par2_ll
                
        end:        PULM Y,Z,K
                    AIS #48
                    
    }
    
#undef rest      
#undef borrow    
#undef j         
#undef i         
#undef q         
#undef r0        
#undef r1        
#undef r2        
#undef r3        
#undef r4        
#undef v0        
#undef v1        
#undef v2        
#undef v3        
#undef u0        
#undef u1        
#undef u2        
#undef u3        
#undef u4        
#undef u5        
#undef u6        
#undef u7        
#undef u8        
#undef newExp    
#undef par1_hh   
#undef par1_hl   
#undef par1_lh   
#undef par1_ll   
#undef par2_hh   
#undef par2_hl   
#undef par2_lh   
#undef par2_ll   

}

/******************************************************************************
  IEEE 64bit comparison.

  Arguments:   p at S + 12
               q at S + 4

  Result:      Set CCR according to q - p
 */

#pragma NO_FRAME

void _DCMP (void)
{
#define q_hh     10,Z
#define q_hl     12,Z
#define q_lh     14,Z
#define q_ll     16,Z
#define p_hh     18,Z
#define p_hl     20,Z
#define p_lh     22,Z
#define p_ll     24,Z

  asm {
                    PSHM  Y,Z,K
                    TSZ
                    LDD   p_hh
                    EORD  q_hh
                    BPL   SameSign
                    /* The two values have different signs */
                    LDD   p_hh
                    CPD   q_hh
                    BRA   End
    SameSign:       TZX             ; Prepare pointers
                    TZY 
                    BRCLR q_hh, #0x80, Pos
                    /* Both are negative: compare the other way 'round! */
                    AIY   #18
                    AIX   #10
                    BRA   Comp
                    /* Both are positive */
    Pos:            AIY   #10
                    AIX   #18
                    /* Now set flags according to @Y - @X */
    Comp:           LDD   0,Y
                    CPD   0,X
                    BNE   End
                    LDD   2,Y
                    CPD   2,X
                    BNE   End
                    LDD   4,Y
                    CPD   4,X
                    BNE   End
                    LDD   6,Y
                    CPD   6,X            
    End:            PULM  Y,Z,K
  } /* end asm */;

#undef q_hh
#undef q_hl
#undef q_lh
#undef q_ll
#undef p_hh
#undef p_hl
#undef p_lh
#undef p_ll

} /* _DCMP */

/******************************************************************************
  Conversion HC16 DSP format to IEEE 64bit format.

  Arguments:   E/D: DSP-float (E = Mantissa in fix point format,
                               D = Exponent in 2's complement)

  Result:      At S + 4. Space reserved by compiler. Note: we can access that
               memory location using "parameter" res.
 */

#define DBL_BIAS          1023
#define DBL_MAX           2047
#define DBL_INF_EXP     0x7FF0

#pragma NO_FRAME

void _DSPTOD (void)
{
#define res_hh     8,Z
#define res_hl    10,Z
#define res_lh    12,Z
#define res_ll    14,Z

  asm {
                    PSHM  Z,K
                    TSZ
                    TSTE
                    BNE   NotZero
                    CLRW  res_hh
                    CLRW  res_hl
                    CLRW  res_lh
                    CLRW  res_ll
                    BRA   End
    NotZero:        XGDE                   ; D = mant; E = exp
                    CLRW  res_lh
                    STAA  res_ll           ; Save sign bit
                    BPL   Pos
                    NEGD                   ; Negate mantissa
    Pos:            LSLD                   ; Hidden bit of IEEE format
                    LSLD
                    STD   res_hl           ; Store it
                    CLRW  res_hh
                    LSLW  res_hl           ; Shift left 4 times
                    ROL   9,Z
                    LSLW  res_hl
                    ROL   9,Z
                    LSLW  res_hl
                    ROL   9,Z
                    LSLW  res_hl
                    ROL   9,Z              ; Now it's aligned correctly
                    XGDE                   ; D is exp again
                    ADDD  #(DBL_BIAS - 1)  ; -1 because of hidden bit...
                    BVS   Infinity         ; Overflow, return infinity
                    BPL   ChkMax
                    CLRD                   ; Exponent too small, return zero
                    CLRW  res_hl
                    BRA   WasPos           ; Always return +0.0
    ChkMax:         CPD   #DBL_MAX
                    BLE   Ok
    Infinity:       CLRW  res_hl           ; Exp > 2047, return infinity
                    LDD   #DBL_INF_EXP     ; Load infinity's exponent
                    BRA   ChkSign          ; Set sign correctly
    Ok:             LSLD
                    LSLD
                    LSLD
                    LSLD
                    ORD   res_hh
    ChkSign:        BRCLR res_ll, #0x80, WasPos
                    ORAA  #0x80
    WasPos:         CLRW  res_ll
                    STD   res_hh 
    End:            PULM  Z,K
  } /* end asm */;

#undef res_hh
#undef res_hl
#undef res_lh
#undef res_ll

} /* _DSPTOD */

/******************************************************************************
  Conversion IEEE 32bit format to IEEE 64bit format.

  Arguments:   E/D: float

  Result:      At S + 4. Space reserved by compiler. Note: we can access that
               memory location using "parameter" res.
 */

#define FLT_BIAS    127

#pragma NO_FRAME

void _DLONG (void)
{
#define res_hh     8,Z
#define res_hl    10,Z
#define res_lh    12,Z
#define res_ll    14,Z

  asm {
                    PSHM   Z,K
                    TSZ
                    TSTD
                    BNE    NotZero
                    TSTE
                    BNE    NotZero
                    CLRW   res_hh
    ClrMant:        CLRW   res_hl
                    CLRW   res_lh
                    CLRW   res_ll
                    BRA    End
    NotZero:        XGDE
                    STD    res_ll           ; Save D
                    LSLD                    ; Exp in A
                    CLRB
                    XGAB
                    CMPB   #0xFF
                    BNE    NotInf
                    LDD    #0x7FF0
                    TST    res_ll
                    BPL    PosInf
                    ORAA   #0x80
    PosInf:         STD    res_hh
                    BRA    ClrMant
    NotInf:         ADDD   #(DBL_BIAS - FLT_BIAS)     ; New exp in D
                    TST    res_ll
                    BPL    Pos
                    ORAA   #8               ; Set sign bit
    Pos:            LSLD
                    LSLD
                    LSLD
                    LSLD                    ; Left align
                    STD    res_hh
                    LDD    res_ll
                    ANDD   #0x007F          ; Mantissa in D/E
                    LSRB                    ; Align correctly
                    RORE
                    RORA
                    LSRB
                    RORE
                    RORA
                    LSRB
                    RORE
                    RORA
                    STE    res_hl 
                    STAA   res_lh
                    CLR    13,Z
                    CLRW   res_ll
                    CLRA
                    ORD    res_hh
                    STD    res_hh
    End:            PULM   Z,K
  } /* end asm */;

#undef res_hh
#undef res_hl
#undef res_lh
#undef res_ll

} /* end _DLONG */

/******************************************************************************
  Conversion IEEE 64bit format to HC16 DSP format.

  Arguments:   X is &double

  Result:      E:D is DSP float (E = mant, D = exp)
 */

#pragma NO_FRAME

void _DTODSP (void)
{
  asm {
                    TSTW  0,X
                    BNE   NotZero
                    TSTW  2,X
                    BNE   NotZero
                    CLRD
                    TDE
                    BRA   End
    NotZero:        LDAB  1,X
                    LDE   2,X
                    ANDB  #0xF
                    ORAB  #0x10            ; Hidden bit (B:E = Mantissa)
                    LDAA  #6               ; Shift count
    Shift:          LSRB
                    RORE
                    DECA
                    BNE   Shift
                    TST   0,X
                    BPL   IsPos
                    NEGE                   ; E is mantissa
    IsPos:          LDD   0,X              ; Load exponent
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #(DBL_BIAS - 1)  ; -1 because of hidden bit...
    End:
  } /* end asm */;
} /* end _DTODSP */

/******************************************************************************
  Conversion IEEE 64bit format to IEEE 32bit format.

  Arguments:   X is &double

  Result:      E:D is IEEE float (E = hi, D = lo)
 */

#define FLT_INF_EXP   0x7F80

#pragma NO_FRAME

void _DSHORT (void)
{
  asm {
                    PSHM  D,E,Z,K
                    TSZ
                    LDD   0,X
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #(DBL_BIAS - FLT_BIAS)
                    BPL   ChkInf
                    CLRD                    ; Return zero
                    TDE
                    BRA   End
    ChkInf:         TSTA
                    BEQ   Ok
                    CLRD                    ; Return +/- infinity
                    LDE   #FLT_INF_EXP
                    TST   0,X
                    BPL   End
                    ORE   #0x8000
                    BRA   End;
    Ok:             LSRB                    ; Shift left by 7 implemented
                    RORA                    ; As rotate right by 1 and
                    XGAB                    ; swap
                    TST   0,X
                    BPL   IsPos             ; Set sign bit correctly
                    ORAA  #0x80
    IsPos:          STD   4,Z
                    LDD   1,X
                    LDE   3,X
                    LSLE
                    ROLD
                    LSLE
                    ROLD
                    LSLE
                    ROLD
                    ANDA  #0x7F
                    STD   6,Z
                    TED
                    TAB
                    LDAA  7,Z
                    TDE
                    LDD   4,Z
                    ORAB  6,Z
                    XGDE
     End:           PULM  Z,K
                    AIS   #4
  } /* end asm */;
} /* end _DSHORT */

/******************************************************************************
  Auxiliary routine to align mantissa correctly. X is &double; shifts right by
  31 - D bits.
 */

#pragma NO_FRAME

void _drshift (void)
{
  asm {
                    PSHM  D,E,Z,K
                    TSZ
                    NEGD
                    ADDD  #31
                    TDE
                    LDD   3,X
                    STD   4,Z
                    LDD   1,X
                    ANDA  #0xF
                    ORAA  #0x10           ; Hidden bit!!!
                    SUBE  #3
                    BEQ   LoadIt
    DoLeft:         BGT   DoRight
                    LDX   5,X
                    STX   6,Z
    ShLeft:         LSLW  6,Z
                    ROLW  4,Z
                    ROLD
                    ADDE  #1
                    BNE   ShLeft
                    BRA   LoadIt
    DoRight:        CPE   #24
                    BCS   Smaller24
                    STAA  5,Z
                    CLR   4,Z
                    CLRD
                    SUBE  #24
                    BRA   Smaller8
    Smaller24:      CPE   #16
                    BCS   Smaller16
                    STD   4,Z
                    CLRD
                    SUBE  #16
                    BRA   Smaller8
    Smaller16:      CPE   #8
                    BCS   Smaller8
                    XGDX
                    LDAB  4,Z
                    STAB  5,Z
                    XGDX
                    STAB  4,Z
                    CLRB
                    XGAB
                    SUBE  #8
    Smaller8:       BEQ   LoadIt
    Loop:           LSRD
                    RORW  4,Z
                    SUBE  #1
                    BNE   Loop
    LoadIt:         LDE   4,Z
                    XGDE
                    PULM  Z,K
                    AIS   #4
  } /* end asm */;
} /* end _drshift */

/******************************************************************************
  Conversion IEEE 64bit format to UNSIGNED LONG.

  Arguments:   X is &double

  Result:      E:D is LONGCARD
 */

#pragma NO_FRAME

void _DUTRUNC (void)
{
  asm {
                    LDD   0,X
                    BMI   IsNeg              ; IF (d < 0.0) THEN RETURN 0
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #DBL_BIAS
                    BGE   NonNeg             ; IF (exp < 0) THEN
    IsNeg:          CLRD
                    TDE                      ;   RETURN 0
                    BRA   End
    NonNeg:         CPD   #32                ; ELSIF (exp >= 32) THEN
                    BCS   Ok
                    LDD   #0xFFFF            ;   RETURN MAX (LONGCARD)
                    TDE
                    BRA   End                ; END
    Ok:             JSR   _drshift
    End:
  } /* end asm */;
} /* end _DUTRUNC */

/******************************************************************************
  Conversion IEEE 64bit format to LONG.

  Arguments:   X is &double

  Result:      E:D is LONGINT
 */

#pragma NO_FRAME

void _DSTRUNC (void)
{
  asm {
                    LDD   0,X
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #DBL_BIAS
                    BGE   NonNeg             ; IF (exp < 0) THEN
    IsNeg:          CLRD
                    TDE                      ;   RETURN 0
                    BRA   End
    NonNeg:         CPD   #31                ; ELSIF (exp >= 31) THEN
                    BCS   ShiftIt
                    LDE   #0x8000            ;   IF (f < 0.0) THEN
                    CLRD                     ;     RETURN MIN (LONGINT);
                    BRSET 0,X, #0x80, End    ;   ELSE
                    COME                     ;     RETURN MAX (LONGINT);
                    COMD                     ;   END;
                    BRA   End                ; END
    ShiftIt:        JSR   _drshift
                    BRCLR 0,X, #0x80, End    ; IF (d < 0.0) THEN
                    NEGE                     ;   result := result;
                    NEGD
                    SBCE  #0
    End:                    
  } /* end asm */;
} /* end _DSTRUNC */

/******************************************************************************
  Auxiliary routine: shift E, D until bit #15 of E is 1. Also decrement X for
  each bit shifted. Than pack it together and store at @Y!
 */

#define DBL_EXP_MAX     (DBL_BIAS + 31)

#pragma NO_FRAME

void _dnormant (void)
{
  asm {
                    LDX   #DBL_EXP_MAX
                    TSTE
                    BMI   Pack
                    BNE   Shift
                    AIX   #-16
                    TDE
                    CLRD
                    TSTE
                    BMI   Pack
    Shift:          AIX   #-1
                    LSLD
                    ROLE
                    BPL   Shift
    Pack:           LSLD                      ; Hidden bit
                    ROLE
                    STD   4,Y
                    XGDX
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    STD   0,Y
                    STE   2,Y
                    CLRW  6,Y
  } /* end asm */;
} /* end _dnormant */

/******************************************************************************
  Conversion UNSIGNED LONG to IEEE 64bit format.

  Arguments:   E:D is LONGCARD

  Result:      S+4 is double
 */

#pragma NO_FRAME

void _DUFLOAT (void)
{
  asm {
                    PSHM  Y,Z,K
                    TSZ
                    TSTE
                    BNE   NotZero
                    TSTD
                    BNE   NotZero
                    STD   10,Z
                    STD   12,Z
                    STD   14,Z
                    STD   16,Z
                    BRA   End
    NotZero:        TZY
                    AIY   #10
                    JSR   _dnormant                     
    End:            PULM  Y,Z,K
  } /* end asm */;
} /* end _DUFLOAT */


/******************************************************************************
  Conversion LONG to IEEE 64bit format.

  Arguments:   E:D is LONGINT

  Result:      S+4 is double
 */

#pragma NO_FRAME

void _DSFLOAT (void)
{

  asm {
                    PSHM  D,Y,Z,K
                    TSZ 
                    TSTE
                    BNE   NotZero
                    TSTD
                    BNE   NotZero
                    STD   12,Z
                    STD   14,Z
                    STD   16,Z
                    STD   18,Z
                    BRA   End
    NotZero:        STE   6,Z                 ; Set negative flag
                    BPL   Pos
                    NEGE
                    NEGD
                    SBCE  #0
    Pos:            TZY
                    AIY   #12
                    JSR   _dnormant
                    BRCLR 6,Z, #0x80, End
                    BSET  12,Z, #0x80
    End:            PULM  Y,Z,K
                    AIS   #2          
  } /* end asm */;
} /* end _DSFLOAT */

/******************************************************************************/
/* end rtshc16.c */

#elif defined(__Y_BASED__)
/*****************************************************
  FILE        : RTSHC16.c
  PURPOSE     : Runtime support for M68HC16.
    _Hxxx routines implement DSP arithmetic.
    _Fxxx routines implement IEEE 32bit arithmetic
    _Dxxx routines implement IEEE 64bit arithmetic, including conversions
          to/from the other two formats.
    DSP format makes use of the M68HC16's fast DSP instructions. The hi word
    is a fix point mantissa in 2's complement. Bit #15 can be used as sign bit,
    bits #14 to #0 are the mantissa (bit #14 is 0.5, #13 is 0.25). There's no
    hidden bit.
      The exponent is in the low word in 2's complement and ranges from -32768
    to 32767.
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

int errno;           /* implemented here because Modula-2 uses 'Math.c'.  */

/******************************************************************************
   DSP FLOATING POINT ROUTINES
 ******************************************************************************/

#pragma NO_FRAME

void _HMUL (void)  /*   parameters are passed in registers:  */
                   /*   D:E operand 1                        */
                   /*    X  @operand 2    D:E * @X           */
                   /*   D:E result                           */
{
   asm {
           ADDD  2,X      ; add exponents
           LDX   0,X      ; load mantissa of op2
           XGDX           ; prepare D:E for multiplication, save new exponent
           FMULS          ; E = D * E -> new mantissa
           XGDX           ; restore new exponent
           BVC   L1       ; was there an overflow
           ADDD  #1       ; yes, adjust exponent and
           LSRE           ; normalize mantissa
     L1:   ADCE  #0       ; round mantissa
           BEQ   Zero     ; is the result zero
           LSLE           ; no, normalize mantissa
           BVC   L3       ; if was normalized already
           RORE           ; undo shift again
           BRA   End
     L3:   ADDD  #-1      ; adjust exponent
           BRA   End
     Zero: CLRD           ; result is zero
     End:  
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HDIV (void)  /*   parameters are passed in registers:  */
                   /*   D:E dividend                         */
                   /*    X  divisor      D:E / @X            */
                   /*   D:E result                           */
{
   asm {
           PSHM  Y,K
           TXY   
           TSTW  0,Y      ; is divisor zero
           BEQ   Z_DIV    ; yes, do a zero division
           TSTE           ; is dividend zero
           BEQ   ZERO     ; yes, return zero
           SUBD  2,Y      ; E.result = E.dividend - E.divisor
           PSHM  E,Y      ; save M.dividend(E), temp(Y)
           LDE   0,Y      ; M.divisor
           TSY            ; points to saved values
           STE   0,Y      ; temp (M.divisor) for sign of result
           BPL   L1       ; is neg -> invert
           NEGE           ; M.divisor
           BVC   L1       ; if overflow
           LSRE           ; normalize
           ADDD  #-1      ; adjust E.result
    L1:    TSTW  2,Y      ; M.dividend > 0
           BPL   L3       ; is neg -> invert
           COM   0,Y      ; invert sign of result
           NEGW  2,Y      ; M.dividend
           BVC   L3       ; if overflow
           LSRW  2,Y      ; normalize
           ADDD  #1       ; adjust E.result
           BRA   L3
    LOOP:  LSRW  2,Y      ; else M.dividend >> 1     
           ADDD  #1       ; adjust E.result    
    L3:    CPE   2,Y      ; M.divisor > M.dividend
           BLE   LOOP     ; yes, continue
           XGDX           ; save E.result to X
           LDD   2,Y      ; M.dividend
           LSLD           ; left adjust M.dividend
           LSLE           ; left adjust M.divisor
           XGEX           ; prepare operands for fdiv, save X to E
           FDIV           ;
           XGEX           ; restore X from E, M.result > E
           LSRE           ; adjust M.result (DSP format)
           LSLD           ; round (based on remainder)
           ADCE  #0       ;
           XGDX           ; reload E.result from X
           TSTW  0,Y      ; sign of result
           BPL   L4
           NEGE           ; result is negative, negate
           CPE   #0xC000  ; special case, do not normalize
           BGE   L6
    L4:    LSLE           ; normalize mantissa
           BVS   L5       ;
           ADDD  #-1      ; adjust E.result
           BRA   L4
    L5:    RORE
    L6:    AIS   #4
           BRA   End
           
    ZERO:  CLRD           ; return zero
           CLRE
           BRA   End 
           
    Z_DIV: CLRD           ; force zero divide
           XGDX
           IDIV
    
    End:   PULM  Y,K 
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HADD2 (void) /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    X               D:E + @X            */
                   /*   D:E result                           */
{
   asm {
           PSHM  Y,K
           TXY   
           TSTE           ; is operand 1 zero ?
           BEQ   R_OP2    ; yes, return operand 2
           TSTW  0,Y      ; is operand 2 zero ?
           BEQ   R_OP1    ; yes, return operand 1
           CPD   2,Y      ; same exponents
           BEQ   DO_ADD   ; yes
           BMI   L1       ; exchange operands
           LDX   0,Y
           XGEX
           STX   0,Y
           LDX   2,Y
           XGDX
           STX   2,Y      ; now, operand (E:D) < operand (@Y)
   L1:     SUBD  2,Y      ; exp (E:D) - exp @Y
           CPD   #-15     ; if operands too different
           BMI   R_OP2    ; then return bigger operand
           CPD   #-7      ; if more 7 bits to shift
           BPL   L2       ; no, shift only
           XGDE
           TAB            ; yes, simulate shift right by 8
           SXT            ; 
           XGDE
           ADDD  #8       ; adjust shift count
           BEQ   DO_ADD
   L2:     ASRE           ; shift right remainding bits
           ADDD  #1
           BNE   L2       ;
   DO_ADD: LDD   2,Y      ; load bigger exponent
           ADDE  0,Y      ; add mantissas
           BVS   L4       ; if overflow
           BEQ   R_ZERO   ; if result is zero, return zero
   L3:     LSLE           ; normalize
           BVS   L5
           ADDD  #-1
           BRA   L3
   L4:     RORE           ; shift right,
           ADDD  #1       ; is normalized now
           BRA   End
   L5:     RORE           
           BRA   End 
           
   R_ZERO: TED
           BRA   End  
               
   R_OP2:  LDE   0,Y      ; return operand 2
           LDD   2,Y
   R_OP1:
   End:    PULM  Y,K
   }
}        

/******************************************************************************/

#pragma NO_FRAME

void _HSUB (void)  /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    X               D:E - @X            */
                   /*   D:E result                           */
{
   asm {   
           PSHM  D,E      ; save subtrahend
           LDE   0,X
           LDD   2,X      ; load subtractor
           NEGE           ; negate subtractor
           BVC   L2
           LSRE  
           ADDD  #1
     L2:
           TSX            ; setup pointer to subtrahend
           JSR   _HADD2   ; call common add routine
           AIS   #4    
   }
}


/******************************************************************************/

#pragma NO_FRAME

void _HADD (void)  /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    X               D:E + @X            */
                   /*   D:E result                           */
{
   asm {   
           PSHM  D,E
           LDE   0,X
           LDD   2,X
           TSX
           JSR   _HADD2   ; call common add routine
           AIS   #4    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HCMP (void)  /*   parameters are passed in registers:  */
                   /*   D:E operand 1                        */
                   /*    X  operand 2    D:E - @X            */
                   /*   CCR result                           */
{
    asm {
           EORE  0,X      ; XOR mantissas: XOR sign bit
           BMI   L2       ; same sign ?
           EORE  0,X      ; yes, same sign, undo XOR of mantissas
           BEQ   L3       ; is operand 1 zero
           TSTW  0,X      ; is operand 2 zero
           BEQ   L4       ;
           CPD   2,X      ; compare exponents
           BEQ   L3       ; exponents differ
           TPA            ; save CCR
           TSTE           ; check sign bit
           BPL   L1       ; if positive, CCR already ok
           EORA  #8       ; no, invert N bit in saved CCR
   L1:     TAP            ; restore CCR
           RTS
           
   L4:     TSTE   
           RTS
      
   L2:     EORE  0,X      ; undo XOR of mantissas
   L3:     CPE   0,X      ; set flags in CCR
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HNEG(void)   /* negate DSP float:         */
                   /*   D:E operand and result  */
{
   asm {   
           NEGE     
           BVC   L1
           LSRE  
           ADDD  #1
       L1:
   }
}

/* this operation can be done inline and/or in memory: 
           NEGW   (ea)
           BVC    L1
           LSRW   (ea)
           INCW   (ea+2)
       L1:
*/
  
/******************************************************************************/

#pragma NO_FRAME         

void _HABS(void)   /* absolute value of DSP float:  */
                   /*   D:E operand and result      */
{
   asm {   
           TSTE
           BPL   L1
           NEGE     
           BVC   L1
           LSRE  
           ADDD  #1
       L1:
   }
}

/* this operation can be done inline and/or in memory:
           TSTW   (ea)
           BPL    L1 
           NEGW   (ea)
           BVC    L1
           LSRW   (ea)
           INCW   (ea+2)
       L1:
*/ 

/******************************************************************************/

#pragma NO_FRAME

void _HSFLOAT(void) /* signed long to DSP float transformation */
                    /* parameter E:D                           */
                    /* result    E:D                           */
{
   asm {
           TSTE           ; check high word
           BNE   L1
           TSTD           ; check low word
           BEQ   END      ; restult is zero
           BMI   L2
     L3:   TDE            ; E was zero, swap words
           CLRD
           LDX   #16      ; initialize exponent
           BRA   Norm     ; and normalize
     L1:   TSTD           ; is E sign extended D
           BPL   L2       ; no if N flag is set
           CPE   #-1      ; 
           BEQ   L3           
     L2:   LDX   #32      ; initial exponent
     Norm: AIX   #-1      ; normalize mantissa
           LSLD
           ROLE
           BVC   Norm     ;
           RORE
           XGDX           ; load exponent into D
     END:    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HUFLOAT(void) /* unsigned long to DSP float transformation */
                    /* parameter E:D                             */
                    /* result    E:D                             */
{
   asm {
           LDX   #32      ; initial exponent
           TSTE           ; check high word
           BMI   Done     ; msb set; data in D not needed
           BNE   Norm
           TSTD           ; check low word
           BEQ   END      ; restult is zero
           BMI   Norm
           TDE            ; E was zero, swap words
           CLRD
           AIX   #-16     ; adjust exponent
     Norm: AIX   #-1      ; normalize mantissa
           LSLD
           ROLE
           BVC   Norm     ;
     Done: RORE
           XGDX           ; load exponent into D
     END:    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HSTRUNC (void)
  /**** IN: D: Exponent, E: Signed mantissa. Corrected version by TW, original
        just shifted, regardless of the mantissa's sign. But if the value was
        -5.7..., i.e. mantissa was 0xA..., the result was 0xFFFA (-6). Correct
        is to shift 5.7 (0x5...) giving 0x0005 and then negating: -5 (0xFFFB). */
{
  asm {
          PSHM   E,K            ; Save mantissa for sign!
          TSTE
          BEQ    ZeroD          ; Result is zero
          TSTD
          BLT    Zero           ; exp < 0 --> result is zero
          TSTE
          BPL    NotNeg
          NEGE                  ; Make mantissa positive
     NotNeg:
          XGDE                  ; E: counter, D: mantissa
          SUBE   #15
          BEQ    PosNeg         ; No shifts needed: E == 0, D == mantissa
          BPL    DoLong         ; More than 15 shifts needed
     ShWord:
          LSRD
          ADDE   #1
          BNE    ShWord
          BRA    PosNeg         ; E == 0, D == mantissa
     DoLong:
          XGDE                  ; E: mantissa, D: counter
          SUBD   #16
          BEQ    PosNeg         ; No shifts needed
          BPL    TooLarge       ; Counter still too large
          NEGD
          XGDX                  ; E|D : Mantissa, X: counter
          CLRD
     ShLong:
          LSRE
          RORD
          AIX    #-1
          BNE    ShLong
     PosNeg:
          TSX
          BRCLR  2,X, #0x80, End
          NEGE                  ; Negate the long result!
          NEGD
          SBCE   #0
          BRA    End
     TooLarge:
          TSX
          BRSET  2,X, #0x80, TooSmall
          LDD    #0xFFFF        ; Overflow value: MAX(longint)
          LDE    #0x7FFF
          BRA    End
     TooSmall:
          LDE    #0x8000        ; Negative overflow
          BRA    ZeroD
     Zero:
          CLRE
     ZeroD:
          CLRD
     End:
          PULM   X,K            ; Clean up stack
  }
}

#if 0
               
void _HSTRUNC(void)
{
   asm {
           TSTE
           BEQ   ZroD     ; value is zero
           TSTD
           BLT   Zero
           SUBD  #15
           BPL   L2 
           NEGD
           XGDX           ; value fits in lower 16 bits
           TED            ; move bit to D and
           BMI   L3       ; sign extend D into E
           CLRE
           BRA   L1
     L3:   LDE   #-1
     L1:   ASRD           ; shift 16 bits to the correct position
           AIX   #-1
           BNE   L1
           RTS
           
     L2:   SUBD  #16
           BEQ   ZroD     ; if no shifts to do
           BGE   Zero     ; result does not fit in a unsigned long
           NEGD
           XGDX           ; result will have up to 32 bits
           CLRD
     LOOP: ASRE           ; shift 32 bits to the correct position
           RORD
           AIX   #-1
           BNE   LOOP
           RTS    
                 
     Zero: CLRE      
     ZroD: CLRD
   }
}   

#endif

/******************************************************************************/

#pragma NO_FRAME

void _HUTRUNC(void)
{
   asm {
           TSTE
           BLE   Zero     ; value is less or equal zero => return 0
           TSTD
           BLE   Zero     ; exponent less or equal zero => return 0
           SUBD  #15
           BPL   L2
           NEGD
           XGDX           ; value fits in lower 16 bits
           TED            
           CLRE
     L1:   LSRD           ; shift 16 bits to the correct position
           AIX   #-1
           BNE   L1
           RTS 
           
     L2:   SUBD  #16
           BEQ   L3       ; value > 2^30; no shifts needed
           CPD   #1       
           BEQ   L4       ; value > 2^31;
           BGE   Zero     ; result does not fit in a unsigned long
           NEGD
           XGDX           ; result will have up to 32 bits
           CLRD
     LOOP: LSRE           ; shift 32 bits to the correct position
           RORD
           AIX   #-1
           BNE   LOOP
           RTS
     
     L4:   LSLE      
     L3:   CLRD
           RTS
           
     Zero: CLRE           ; return zero
           CLRD
   }

}

/******************************************************************************
                      CALLING PROCEDURE VARIABLES
                      
  For all routines except _CALLS, the procedure variable is in registers. We
  always have to increment the low word by 2 because the RTS will subtract 2!
    The method is quite tricky: we set up a stack layout as follows:
    
                  RETURN ADDRESS LOW
                  RETURN ADDRESS HIGH
                  Parameter Low + 2
                  Parameter High
    
    The return address is the runtime routine's return address, i.e. it points
  behind the JSR that brought us here. The parameter is the address of the
  function we want to call.
    When we have set up above stack frame, we execute a RTS. This pops the
  parameter from the stack and instead of returning jumps to the beginning of
  the procedure to be called. When this procedure finally executes its own RTS,
  it'll use the original return address and thereby finally return to the point
  where the function pointer call was.
  
  Function _CALLE is used in SMALL memory model only (if both X and Y are used
  by parameters). It uses 0 as the high word of the parameter. This implies that
  in SMALL memory model, the code page be page zero.
    This however is also required by the fact that the vector table has only
  16bit entries and also because the compiler doesn't patch the code page in
  a JSR 0,X!!
 ******************************************************************************/

#pragma NO_FRAME

void _CALLYX (void)         /* parameters are passed in Y:X (4:16)  */
{
   asm {
          AIX   #2        ; adjust address 
          PSHM  X,Y       ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLED(void)         /* parameters are passed in E:D (4:16)  */
{
   asm {
           ADDD  #2       ; adjust address 
           PSHM  D,E      ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLE (void)
{
  asm {
          ADDE  #2
          PSHM  E
          CLRE            ; zero as code page!!
          PSHM  E
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLXE (void)
{
  asm {
          ADDE  #2
          PSHM  E,X
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLYE (void)
{
  asm {
          ADDE #2
          PSHM E,Y
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLS (void)
  /**** Here, the destination address is on the stack,
        above the return address! Therefore, we gotta swap them! */
{
  asm {
          PSHM  D,X,Z,K   ; Save all registers used!
          TSZ             ; Set up access pointer
          LDD   14,Z      ; Load dest addr (LO)
          LDX   10,Z      ; Load return addr (LO)
          ADDD  #2        ; Correct dest addr
          STD   10,Z      ; Swap the two!
          STX   14,Z
          LDD   12,Z      ; Load dest addr (HI)
          LDX   8,Z       ; Load return addr (HI)
          STD   8,Z       ; Swap the two!
          STX   12,Z
          PULM  D,X,Z,K   ; Restore all registers!
  }                       /* this return is the procedure call!  */
} /* end _CALLS */

#pragma NO_FRAME

void _CALLSS (void)
{
  asm {
          AIS   #-2       ; Create space for duplication of code page
          PSHM  D,X,K     ; Save all registers used
          TSX             ; Set up access pointer
          LDD   8,X       ; Load code page...
          STD   6,X       ; ... and duplicate
          LDD   12,X      ; Load dest addr...
          ADDD  #2        ; ... correct it...
          STD   8,X       ; ... and set up as our return address
          LDD   10,X      ; Load former return address...
          STD   12,X      ; ... and set up as return address of called function
          LDD   6,X       ; Now set code page for the latter
          STD   10,X
          PULM  D,X,K     ; Restore all registers used!
  }                       /* this return is the procedure call! */
} /* end _CALLSS */
    
/******************************************************************************
                                BLOCK MOVES
  
  If we may use only one address register, block moves must be done using
  runtime routines. There are four routines, mirroring all the possible moves
  between I/O space and normal memory. To keep the number of routines that
  small, all pointers (not constant I/O pointers) are considered far pointers.
  If necessary, a near pointer is extended by the default data page.
 ******************************************************************************/

#pragma NO_FRAME

static void _block_move (void)
  /****
    Arguments:
          YK:Y  source address
          XK:X  dest. address
          E     counter, initially block size in bytes - 2
    Precondition:
          E >= 0
   */
{
  asm {
    Loop: LDD   E,Y      ;   Copy words
          STD   E,X
          SUBE  #2       ;   counter -= 2
          BPL   Loop     ; WHILE (counter >= 0)
          PULM  Y,K      ; Restore registers
  } /* end asm */;
} /* end _block_move */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_MEM (void)
  /****
    Arguments:
          Source address : 16 bit I/O address on stack
          Dest. address  : 20 bit far pointer in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TBXK           ; Load page register
          TSY            ; Set up access pointer
          LDY   8,Y      ; Load source address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBYK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _IO_TO_MEM */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_IO (void)
  /****
    Arguments:
          Source address : 20 bit far pointer in B/X
          Dest. address  : 16 bit I/O address on stack
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TXY            ; Move source pointer to Y 
          TBYK           ; Load page register
          TSX            ; Set up access pointer
          LDX   8,X      ; Load dest address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBXK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _MEM_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_IO (void)
  /****
    Arguments:
          Source address : 16 bit I/O address in D
          Dest. address  : 16 bit I/O address in X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K
          XGDY
          TSKB
          DECB
          TBXK
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _IO_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_MEM (void)
  /****
    Arguments:
          Source address : 20 bit address on stack
          Dest. address  : 20 bit address in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K 
          TBXK
          TSY
          LDAB 9,Y
          LDY  10,Y
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _MEM_TO_MEM */

/******************************************************************************
                          MISCELLANEOUS PROCEDURES
 ******************************************************************************/
 
#pragma NO_FRAME

void _YK (void)
  /**** Used to reset YK after runtime call. */
{
  asm {
           PSHM   D, CCR
           TSKB
           TBYK
           PULM   D, CCR
  };        
} /* end _YK */

#pragma NO_FRAME

void _XK (void)
{
  asm {
           PSHM   D, CCR
           TSKB
           TBXK
           PULM   D, CCR
  };
} /* end _XK */

#pragma NO_FRAME

void _IDIV(void)          /* parameters are passed in D / X       */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
           XGDX           
   }
}

#pragma NO_FRAME

void _IMOD(void)          /* parameters are passed in D MOD X     */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
   }
}

/******************************************************************************
  32 bit integral arithmetic
 ******************************************************************************/

/*--------------------------------------------------------------- _LMULU ----*
 * UNSIGNED LONG MULTIPLICATION
 *------------------------------
 * Multiplies the unsigned value in registers E:D with the unsigned at the
 * address 0,X:2,X. The result is stored back in registers E:D.
 *
 * The general results of this operation is
 *
 *        +-----------+
 *        |  E  * 0,Y |                 High part          
 *        +-----+-----+-----+
 *    +         |  E  * 2,Y |           Middle 1 part
 *              +-----------+
 *    +         |  D  * 0,Y |           Middle 2 part
 *              +-----+-----+-----+
 *    +               |  D  * 2,Y |     Low part
 *                    +-----------+
 *
 *                    \_ result __/
 *  
 * The result of the multiplication is only le lowest longword of the 64 bits
 * results. In order to have a correct multiplication (without overflow) the
 * highest longword of the result must be 0.
 * The highest longword equal to 0 means that either E = 0 or 0,Y = 0. We can
 * use this fact to make the algorithm faster :
 *
 * if (e == 0) {
 *     return ((D * 0,Y) >> 8) + (D * 2,Y);
 * } else {
 *     return ((E * 2,Y) >> 8) + (D * 2,y);
 * }
 *
 * This algorithm is quite fast because we have only 2 multiplications.
 * 
 * WRONG: for ANSI-C we have to use 32 bit even in case of an                                                                    
 * overflow! So we have the same code as for the signed multiplication.
 * (bh, vc; 1-Mar-93) 
 *
 *---------------------------------------------------------------------------*/
                     
long _LMULU (void)
{
    int dd, t0;
    
    asm {
                STD dd     ; D will be changed after the EMUL, and we need
                           ; it later for calculation the low part. So we
                           ; have to store it.

                LDD 2,X    ; Compute mid1 part
                EMUL       ; E * 2,X
                STD t0
                
                LDD dd     ; Compute mid2 part
                LDE 0,X
                EMUL       ; D * 0,X
                ADDD t0
                STD t0
                
                LDD dd     ; Compute low part
                LDE 2,X
                EMUL       ; D * 2,X
                ADDE t0
    }
}


/**************************************************************** _LMULS *****
 * SIGNED LONG MULTIPLICATION
 *----------------------------
 * Multiplies the signed value in registers E:D with the signed at the
 * address 0,Y:2,Y. The result is stored back in registers E:D.
 *
 * Here we can't use the fact that either E = 0 or 0,Y = 0, so we have to
 * implement the standard algorithm with 3 multiplictions.
 *
 *---------------------------------------------------------------------------*/

long _LMULS (void)

{
    int dd, t0;
    
    asm {
                STD dd     ; D will be changed after the EMUL, and we need
                           ; it later for calculation the low part. So we
                           ; have to store it.

                LDD 2,X    ; Compute mid1 part
                EMUL       ; E * 2,X
                STD t0
                
                LDD dd     ; Compute mid2 part
                LDE 0,X
                EMUL       ; D * 0,X
                ADDD t0
                STD t0
                
                LDD dd     ; Compute low part
                LDE 2,X
                EMUL       ; D * 2,X
                ADDE t0
    }
}

/************************************************************** _LDIVG ******
 * GENERIC LONG DIVISION
 *-----------------------
 * See D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LDIVG ()

{
    unsigned int t0,t1;
    unsigned int ax, ah, al;
    unsigned int bh, bl;
    unsigned int tx, th, tl;
    unsigned int rl;
    unsigned int dd;
    
    asm {
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1
                
        Alg0:               ; 0,Y and E are = 0. Then we can use
                            ; the (fast) IDIV instruction.
                LDX 2,Y   
                IDIV
                XGDX
                CLRE
                BRA end
                
        Alg1:               ; only 0,Y is 0, we can use the algorithm
                            ; explained at exercice 16 of chapter 4.3.1 in
                            ; D. KNUTH

                STD t0      ; Store D in t0
                TED         
                LDX 2,Y
                IDIV        ; E / 2,Y
                STX t1
                
                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; rest : D / 2,X
                XGDX        ; result in E:D
                LDE t1
                BRA end

        Alg2:               ; 0,Y is not equal to 0, so we have to use here
                            ; the complete long division.

                STD al      ; Firste store E:D in ah:al
                LDD 0,Y     ; and 0,Y:2,Y in bh:bl
                STD bh
                LDD 2,Y
                STD bl
                STE ah
                CLRW ax
                BMI calcQ   ; if ah:al >= 2^16, no need to normalize
                
        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register     
                BNE Divide
                LDX #1
                BRA Approx    
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1
                
                LDE al      ; Compute ax:ah:al = ah:al * dd
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax
                
                LDE bl      ; Compute bh:bl = bh:bl * d
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh
                
        calcQ:  LDX #0xFE   ; First estimation of the quotient
                LDE bh
                CPE ax
                BEQ l1      ; if bl = ax then register X = 0xFE
                LDE ax      ; else compute register X = ax:ah / bh
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl      ; store X in rl (q)
        
                            ; Check if the result is correct
                LDE bl      ; Compute tx:th:tl = rl * bh:bl
                LDD rl
                EMUL
                STD tl
                STE th
                
                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx
                
                LDD al      ; Compute tx:th:tl = ax:ah:al - tx:th:tl
                SUBD tl
                STD tl
                
                LDD ah
                SBCD th
                STD th
                
                LDD ax
                SBCD tx
                STD tx
                            ; Here tx:th:tl represents the rest of the
                            ; division (T = A - R * B)
                
                BMI loop    ; if T < 0 then result is too big, goto loop
                
                CLRE        ; Else, result is already correct, clear E and
                LDD rl      ; load the result in D
                
                BRA end

        loop:   DECW rl     ; Result is too big, so decrement it (R = R-1)
        
                            ; If the result is of 1 smaller, then the rest
                            ; (in T) is of B bigger
                            ; T0 = A - R*B
                            ; T1 = A - (R-1)*B
                            ; T1 = A - R*B + B = T0 + B
                            ; --                 ------

                LDD tl
                ADDD bl
                STD tl
                
                LDD th
                ADCD bh
                STD th
                
                LDD tx
                ADCD #0
                STD tx
                
                BMI loop    ; if T is still < 0, loop again
                            ; this branch is very very unlikely to be taken
                
                CLRE        ; The Result is now correct, clear E and
                LDD rl      ; load the result in D
                
        end:
    }
}

/************************************************************** _LMODG ******
 * GENERIC LONG MODULO
 *---------------------
 * See generic long division and D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LMODG ()

{
    unsigned int t0;
    unsigned int ax, ah, al;
    unsigned int bh, bl;
    unsigned int tx, th, tl;
    unsigned int rh, rl;
    unsigned int dd;
    
    asm {
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1
                
        Alg0:   LDX 2,Y     ; See Generic Long Division
                IDIV
                CLRE        ; The result is already in D, just clear E
                BRA exit
                
        Alg1:   STD t0      ; See Generic Long Division
                TED         
                LDX 2,Y
                IDIV
                
                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; The result is in D, clear E
                CLRE
                BRA exit

        Alg2:   STD al
                LDD 0,Y
                STD bh
                LDD 2,Y
                STD bl
                STE ah

                CLR dd      ; dd = 0 means has not been normalized
                CLRW ax
                BMI calcQ
                
        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register     
                BNE Divide
                LDX #1
                BRA Approx    
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1
                
                LDE al
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax
                
                LDE bl
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh
                
        calcQ:  LDX #0xFE
                LDE bh
                CPE ax
                BEQ l1
                LDE ax
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl
        
                LDE bl
                LDD rl
                EMUL
                STD tl
                STE th
                
                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx
                
                LDD al
                SUBD tl
                STD tl
                
                LDD ah
                SBCD th
                STD th
                
                LDD ax
                SBCD tx
                STD tx
                
                BMI loop
                BRA end
                
        loop:   DECW rl
        
                LDD tl
                ADDD bl
                STD tl
                
                LDD th
                ADCD bh
                STD th
                
                LDD tx
                ADCD #0
                STD tx
                
                BMI loop

        end:    TST dd      ; Check if the operation has been normalized
                BNE deno    ; if yes, denormalize

                LDE th      ; If the operatinon has not been normalized
                            ; the result is in th:tl
                LDD tl
                BRA exit

        deno:   LDE tx      ; Else, we have to divide tx:th:tl by dd
                LDD th
                LDX dd
                EDIV
                STX rh
                TDE
                LDD tl
                LDX dd
                EDIV
                XGDX
                LDE rh
        exit:
    }
}
                    
/************************************************************** _LDIVS ******/

#pragma NO_FRAME

void _LDIVS ()

{   
    asm {
                PSHM E,X,Y,Z,K
                TXY
                TSZ           ; 8,Z is 'sign', 9,Z is 'ySign'
                
                TSTE          ; test E
                BPL   ePos    ; if e > 0 then ok
                COME          ; else negate E:D
                NEGD
                SBCE  #-1
                
        ePos:   CLR   9,Z     ; Clear ySign
                TST   0,Y     ; test 0,Y
                BPL   yPos    ; if 0,Y > 0 then ok
                COMW  8,Z     ; Complement signs
                XGDX          ; else negate 0,Y:2,Y
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
                
        yPos:   JSR   _LDIVG  
                
        end:    TST   8,Z
                BPL   corrY   ; is sign is +, then keep E:D and check if a
                              ; correction of Y is nessessary.
                COME          ; else negate E:D
                NEGD
                SBCE  #-1
                
        corrY:  TST   9,Z
                BEQ   exit
                XGDX          ; y has been negated, reverse operation.
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
        exit:   PULM  X,Y,Z,K
                AIS   #2
    }
}
                    
/************************************************************** _LDIVU ******/

#pragma NO_FRAME

void _LDIVU ()

{
    asm {
                PSHM X,Y,K
                TXY
                JSR _LDIVG  
                PULM X,Y,K
    }
}

/************************************************************** _LMODS ******/

#pragma NO_FRAME

void _LMODS ()
{
    asm {
                PSHM  E,X,Y,Z,K  ; See _LDIVS for comments
                TXY
                TSZ
                                 ; the difference with _LDIVS is that here the
                                 ; sign of the result is the same as the sign
                                 ; of the first argument.                
                TSTE
                BPL   ePos
                COME
                NEGD
                SBCE  #-1
                
        ePos:   CLR   9,Z
                TST   0,Y
                BPL   yPos
                COM   9,Z
                XGDX
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
                
        yPos:   JSR   _LMODG
                
        end:    TST   8,Z
                BPL   corrY
                COME
                NEGD
                SBCE  #-1
                
        corrY:  TST   9,Z
                BEQ   exit
                XGDX
                LDD   0,Y
                COMD
                NEGW  2,Y
                SBCD  #-1
                STD   0,Y
                XGDX
        exit:   PULM  X,Y,Z,K
                AIS   #2
    }
}

/************************************************************** _LMODU ******/


#pragma NO_FRAME

void _LMODU (void)
{
    asm {
                PSHM X,Y,K
                TXY
                JSR _LMODG
                PULM X,Y,K
    }
}

/****************************************************************************/

#pragma NO_FRAME

void _LINCPRE (void)
  /**** ++long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                INCW  2,X
                BNE   LoadIt
                INCW  0,X
    LoadIt:     LDD   2,X
                LDE   0,X
  }
}

#pragma NO_FRAME

void _LINCPST (void)
  /**** long++. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD   2,X
                LDE   0,X
                INCW  2,X
                BNE   End
                INCW  0,X
    End:
  }
}

#pragma NO_FRAME

void _LDECPRE (void)
  /**** --long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD  2,X       
                BNE  DecLo     
                DECW 0,X       
    DecLo:      ADDD #-1       
                STD  2,X       
                LDE  0,X       
  }              
}

#pragma NO_FRAME

void _LDECPST (void)
{
  asm {
                LDE   0,X
                LDD   2,X
                BNE   DecLo
                DECW  0,X
    DecLo:      DECW  2,X
  }
}      
    
/****************************************************************************/

#pragma NO_FRAME

void _LCMP (void)
  /**** Signed or unsigned comparison. Called in SMALL and MEDIUM memory models
        if one of the operands is far. (Generating the normal inline comparison
        gives troubles with the PUSH/PULL optimization!
          Arguments: E/D    a
                     Stack  b
          Result:    CCR according to a - b
   */
{
  asm {
            TSX
            SUBD    6,X
            SBCE    4,X
            BNE     End
            TSTD
    End:
  } /* end asm */;
} /* end _LCMP */

#pragma NO_FRAME

void _PCMP (void)
  /**** Far pointer comparison. Stack: b, B/E: a, CCR set according to a - b. */
{
  asm {
            TSX
            SUBE    6,X
            SBCB    5,X
            BNE     End
            TSTE
    End:
  } /* end asm */;
} /* end _PCMP */

/****************************************************************************/

#pragma NO_FRAME

void  _LSHL (void)
  /**** Long <<. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASLD
            ROLE
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHL */

#pragma NO_FRAME

void  _LSHR (void)
  /**** Long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHR */

#pragma NO_FRAME

void  _LSHRU (void)
  /**** Unsigned long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   LSRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHRU */

/******************************************************************************
  CASE PROCESSOR ROUTINES CALLED IMPLICITLY BY THE COMPILER
 ******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_CHECKED (void)
{
   asm {     
             TDE
             PSHM   Y,K     ; Save old XK and YK registers
             TSY
             LDX    6,Y     ; load table address
             LDAB   5,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             CPE   -2,X     ; compare with table size bound
             BCC    Default ; too big
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD    2,X     ; load offset
    Return:  ADDD   6,Y     ; add return address
             STD    6,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    5,Y
    ThatsIt: PULM   Y,K     ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
    Default: LDD    0,X
             BRA    Return;        
   }                      
}

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_DIRECT (void)
{
   asm {      
             TDE
             PSHM   Y,K     ; Save old XK,YK registers
             TSY
             LDX    6,Y     ; load table address
             LDAB   5,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD   -2,X     ; load offset
    Return:  ADDD   6,Y     ; add return address
             STD    6,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    5,Y
    ThatsIt: PULM   Y,K     ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
   }                     
}

 
/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_SEARCH(void)
{
   asm {
             TDE             ; Move value to look for into E
             PSHM   D,Y,K    ; Save old XK register and reserve space for temp
             TSY
             LDX    8,Y      ; load table address
             LDAB   7,Y      ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0  ; Disable interrupts
#endif
             TBXK
             LDD    -2,X     ; load table size -> D
             XGDE            ; E = table size, D = value to look for
             AIX    #2
    Loop:    ASRE            ; E = (E / 2) & 0xFFFC
             STE    4,Y      ; Save into temp: we'll need it below!!
             ANDE   #0xFFFC  ;
             BEQ    Check    ; if last possible entry -> Check
             CPD    E,X      ; is entry found
             BEQ    Found    ; yes, -> Found
             BLS    Loop     ; no and lower -> Loop
             AEX             ; X = X + E + 4
             AIX    #4       ; +4 because this entry already has been tested
             BRSET  5,Y, #0x02, Loop
             SUBE   #4       ; Correct E, but only if we generated
                             ; an odd size sub-table
             BRA    Loop    
                      
    Check:   CPD    E,X      ; entry found?
             BNE    Default  ; yes, 
    Found:   AEX             ; point to entry in table
             LDD    2,X      ; load offset
    Return:  ADDD   8,Y      ; add return address
             STD    8,Y      ; overwrite return address by destination address
             BCC    ThatsIt
             INC    7,Y
    ThatsIt: PULM   K,Y,D    ; Restore XK and YK registers and remove temp space
             RTI             ; jumps to case label and restores interrupt mask
             
    Default: LDX    8,Y      ; load table address
             LDD    0,X      ; load default offset
             BRA    Return         
   }
}

/******************************************************************************
  IEEE 32 bit arithmetic
 ******************************************************************************/

/**** IEEE 32 bit multiplication. Arguments:

        x   in E:D
        &y  in X

        result x * y (E:D * @X) is returned in E:D
 */

void _FMUL (void)
{
    unsigned long par1;
    int           newExp, carry;
    int           result[4];
    
    asm {
    
                STE par1:0          ; Save x
                STD par1:2
        
        ;----- Compute the exponent of the result
        
                ANDE #0x7F80        ; Extract first exponent
                BEQ zero
        
                LDD 0,X           ; Extract second exponent
                ANDD #0x7F80
                BEQ zero
        
                ADE                 ; Add the 2 exponents
                SUBE #0x3F80        ; Substract Bias
                        
        ;----- Compute the sign of the result
        
                LDD par1
                EORD 0,X
                ANDD #0x8000        ; Mask the sign bit
        
                ADE
                STE newExp          ; newExp is now the exponent of the result
        
        ;----- Multiply the 2 mantissa and store the result in <result>
        
                LDE par1:2          ; par1:2 * 2,X
                LDD 2,X
                EMUL
                STD result:6
                STE carry
        
                LDE par1:2          ; par1:2 * 0,X
                LDD 0,X
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                STD result:4
                STE result:2
        
                LDE par1:0          ; par1:0 * 2,X
                ANDE #0x007F
                ORE  #0x0080
                LDD 2,X
                EMUL
                ADDD result:4
                ADCE #0
                STD result:4
                STE carry
                
                LDE par1:0          ; par1:0 * 0,X
                ANDE #0x007F
                ORE  #0x0080
                LDD 0,X
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                ADDD result:2
                ADCE #0
                STD result:2
                STE result:0
        
        ;----- Normalise the mantissa
        
                BRSET result:2, #0x80, noDen
                LSLW result:4
                LSLW result:2
                LSLW result:0
                LDD result:3
                LDE result:1
                ANDE #0x007F
                ADDE newExp
                BRA exit
                
        noDen:  LDD result:3
                LDE result:1
                ANDE #0x007F
                ADDE newExp
                ADDE #0x0080
                BRA exit
        
        
        zero:   CLRD
                CLRE
                
        exit:
    }
}

/******************************************************************************
  IEEE 32 bit division. Arguments:

    x   in E:D
    &y  in X

    result x / y (E:D / @X) is returned in E:D
 */
 
void _FDIV (void)
{

    unsigned long par1;
    int           newExp;
    
    unsigned int  u[5];
    unsigned int  v[2];
    unsigned int  r[3];
    int           q, i, j;
    int           borrow, rest;    
    
    asm {
                STE par1:0
                STD par1:2
                PSHM  Y,K
                
        ; Compute the new exponent                                
                                    
                LDD 0,X             ; load first word of second parameter
                ANDD #0x7F80        ; mask exponent
                BEQ div0            ; if zero, then division by zero error

                                    ; E is first word of first parameter
                ANDE #0x7F80        ; mask exponent
                BEQ zero            ; if zero, then result is zero
                
                SDE                 ; substract exponents
                ADDE #0x3F80        ; add bias

                LDD par1            ; load first word of first parameter
                EORD 0,X            ; xor with the 1 st word of the 
                                    ; 2 nd param.
                ANDD #0x8000        ; mask the sign bit
                
                ADE                 ; add the sign to the new exponent
                STE newExp          ; newExp is now the exponent of 
                                    ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                CLRW u:8
                CLRW u:6
            
                LDAA par1:3
                CLRB
                STD u:4
                
                LDE par1:1
                ORE #0x8000
                STE u:2
                
                CLRW u:0
                    
        
        ; Compute the normalized fractional part of the second parameter        
        ; and store it in v
                    
                LDAA 3,X
                CLRB                   
                STD v:2
                 
                LDE 1,X
                ORE #0x8000
                STE v:0

        ; Compute the quotient of the mantissa.
                
                CLRW j              ; counter = 0
                TZY
                AIY @u              ; Y points to u[j = 0]
                BRA loop

        mainLoop:
                TZY
                AIY @u              ; Y points to u[0]
                LDD j
                ASLD
                ADY                 ; Y points to u[j]

        ; Calculate the first estimation of q

        loop:   LDE 0,Y             ; E = u[j]
                CPE v:0             ; IF u[j] == v[1] THEN
                BNE l1              ;
                LDX #0xFFFF         ;   X := b-1
                STX q
                    
                LDD 0,Y             ;   rest = u[j] + u[j+1]          
                ADDD 2,Y
                    
                BCS substr
                STD rest
                BRA fastCheck
                    
        l1:     LDD 2,Y             ; ELSE  D = u[j+1]
                LDX v:0
                EDIV                ;   X = u[j]:u[j+1] / v[1]
             
        l2:     STX q
                STD rest            ;   rest

        ; Fast Check to see if q is too big

        fastCheck:
                LDE v:2             ; E = v[2]
                LDD q               ; D = q
                EMUL
                    
                CPE rest
                BHI decq
                BNE substr
                    
                CPD 4,Y
                BLS substr
                    
        decq:   DECW q              ; Decrement q
                LDD rest            ; Adjust rest
                ADDD v:0
                BCS substr
                STD rest
                BRA fastCheck

        ; substract q * v from u

        substr: CLRW borrow         ; Start with no borrow

                TZX
                AIX @v:2            ; X points to v[n]
                TZY
                AIY @u:4
                LDD j
                ASLD
                ADY                 ; Y points to u[j+n]

                LDE #1
                STE i               ; i = 1..0

        sl1:    LDE 0,X
                LDD q
                EMUL                ; E:D = q * v[n]

                ADDD borrow
                ADCE #0             ; E:D = q * v[n] + borrow

                STE borrow
                LDE 0,Y             ; E = u[j+n]
                SDE                 ; E = E - D
                STE 0,Y             ; Store new u[j+n]
                    
                BCC noB
                INCW borrow         ; correct borrow if necessary

        noB:    AIX #-2
                AIY #-2
                DECW i
                BPL sl1

                LDE 0,Y
                SUBE borrow
                STE 0,Y
                    
                BCC ok

        ; q was too big, we have to add v back to u

                TZX
                AIX @v              ; X points to v[1]

                TZY
                AIY @u
                LDD j
                ASLD
                ADY                 ; Y points to u[j]
                    
                LDD 2,X
                ADDD 2,Y
                STD 2,Y
                  
                LDD 0,X
                ADCD 0,Y
                STD 0,Y
                  
                DECW q

        ok:     TZY
                AIY @r
                LDD j
                ASLD
                ADY                 ; Y points to r[j]

                LDE q
                STE 0,Y             ; store q in r[j]

                INCW j
                LDE j
                CPE #2
                BLE mainLoop
                
        ; denormalize and move the result in par1. if necessary, correct
        ; the exponent of the result.
        
                TSTW r
                BNE dnl1
                LDE r:1
                ANDE #0x007F
                ADDE newExp
                SUBE #0x0080
                LDD r:3
                BRA end
                    
        dnl1:   
                LDE r:1
                LDD r:3
                LSRE
                RORD
                ANDE #0x007F
                ADDE newExp
                BRA end
                 
        div0:   LDX #0             ; Simulate a division by 0
                EDIV
                        
        zero:   CLRE               ; result is zero
                CLRD
                
        end:    PULM  Y,K

    }
}

/******************************************************************************
  IEEE 32 bit addition. Arguments:

    x   in E:D
    &y  in X

    result x + y (E:D + @X) is returned in E:D
 */
 
void _FADD (void)
{
    unsigned long par1,  par2;
    int           newExp;    
    char          sign1, sign2, resSign;
    
    asm {
                PSHM  Y,K
                CLR   resSign
                STE   par1:0        ; Save par1
                STD   par1:2
                ANDE  #0x7F80       ; Mask Exponent
                BEQ   uvfl          ; x == 0, return par2 (y)
                LDD   2,X
                STD   par2:2
                LDD   0,X
                STD   par2:0
                ANDD  #0x7F80
                BNE   not0          ; y == 0, return par1 (x)
                LDE   par1:0
                LDD   par1:2
                BRA   end
        not0:   SDE                 ; compute exp1 - exp2
                ASLE                ; carry set means D negativ
                TED
                XGAB
                TDE                 ; E = # of bits to shift
                TZX                 ; initialise X and Y pointers
                TZY
                BCC   shP2             
        shP1:   AIX   @par1         ; exp1 < exp2 => shift par1
                AIY   @par2         ; and compute par2 + par1
                NEGE
                ANDE  #0x00FF
                BRA   cont
        shP2:   AIX   @par2         ; exp1 > exp2 => shift par2
                AIY   @par1         ; and compute par1 + par2
        cont:   CPE   #24
                BLT   noUVFL
        uvfl:   LDE   0,Y           ; We'd shift @X completely away
                LDD   2,Y
                BRA   end                
        noUVFL: LDAA  0,X
                STAA  sign2
                LDD   0,Y
                STAA  sign1
                ANDD  #0x7F80
                STD   newExp
                CLR   par1
                BSET  par1:1,#0x80  ; Hidden bit
                CLR   par2
                BSET  par2:1,#0x80  ; Hidden bit  

        ; shift the mantissa pointed to by X right by E bits

        chk16:  CPE   #16           ; Try to shift a bloc of 16 bits
                BLT   chk8
        sh16:   LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #16
        chk8:   CPE   #8            ; Try to shift a bloc of 8 bits
                BLT   chk1           
        sh8:    LDD   1,X
                STD   2,X
                CLR   1,X
                SUBE  #8
                BRA   chk1
        sh1:    LSR   1,X        
                RORW  2,X
        chk1:   SUBE  #1
                BGE   sh1
        madd:   BRCLR sign2,#0x80,xPos
                LDD   #0             ; if @X is negative...
                SUBD  2,X            ; ...negate the mantissa
                STD   2,X
                LDD   #0
                SBCD  0,X
                STD   0,X
        xPos:   BRCLR sign1,#0x80,yPos
                LDD   #0             ; if @Y is negative...
                SUBD  2,Y            ; ...negate the mantissa
                STD   2,Y
                LDD   #0
                SBCD  0,Y
                STD   0,Y
        yPos:   LDD   2,X            ; Add them!
                ADDD  2,Y
                STD   par1:2
                LDD   0,X
                ADCD  0,Y
                STD   par1:0
                BPL   normal
                LDD   #0             ; Resulting mantissa is negative
                SUBD  par1:2
                STD   par1:2
                LDD   #0
                SBCD  par1:0
                STD   par1:0
                COM   resSign        ; Result is negative!!
        normal: LDE   newExp         
                BRCLR par1,#0x01,subN
                LSRW  par1:0         ; Overflow: shift right by 1...
                RORW  par1:2
                ADDE  #0x80          ; ... and increment exponent
        subN:   BRSET par1:1, #0x80, pack
                LDD   par1           ; De-normalized: test for zero
                BNE   try1
                LDD   par1:2
                BNE   mov16
                TDE                  ; Result is zero
                BRA   end
        mov16:  LDAA  par1:2 
                BNE   mov8
                LDD   par1:2         ; hi 3 bytes are 0
                STD   par1:0
                CLRW  par1:2
                SUBE  #0x800
                BRA   try1
        mov8:   LDD   par1:2         ; only hi 2 bytes are 0
                STD   par1:1
                CLR   par1:3
                SUBE  #0x400
                BRA   try1                        
        mov1:   LSLW  par1:2         ; Shift until normalized
                ROL   par1:1
                SUBE  #0x80
        try1:   BRCLR par1:1,#0x80,mov1
        pack:   BCLR  par1:1,#0x80
                LDD   par1:2
                ORE   par1
                BRCLR resSign,#0x80, end
                ORE   #0x8000
        end:    PULM  Y,K
    }
}


/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in X

    result x - y (E:D - @X) is returned in E:D

    Note: @X mustn't be changed, therefore y is copied (and the sign bit
          inverted).
 */
 
void _FSUB (void)
{
    unsigned long par1;
    
    asm {
                PSHM D
                LDD  0,X
                EORD #0x8000
                STD  par1
                LDD  2,X
                STD  par1:2
                TZX
                AIX  @par1
                PULM D
                JSR _FADD
    }
}

/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in X

    result x - y (E:D - @X) is returned in E:D

    Note: @X may be changed, therefore the sign bit is inverted in place.
 */

#pragma NO_EXIT
 
void _FSUB0 (void)
{
  asm {
                PSHM D
                LDD  0,X
                EORA #0x80
                STD  0,X
                PULM D
                PULM Z
                JMP  _FADD
  }
}

/******************************************************************************
  Auxiliary routine: right shift (B, E, #0:8) by 31-A bits and return result
  in E:D
 */

#define FLT_BIAS      127

void _frshift (void)
{
  unsigned long l;

  asm {
                NEGA                    ;   A := 31 - A
                ADDA   #31
                CMPA   #24      
                BCS    Smaller24        ;   IF (A >= 24) THEN
                STAB   l:3              ;     l := (mantissa >> 24);
                CLRW   l
                CLR    l:2
                SUBA   #24              ;     DEC (A, 24);
                BRA    Smaller8         ;   ELSE
    Smaller24:  STAB   l                ;     l := left adjusted mantissa
                STE    l:1
                CLR    l:3
                CMPA   #16
                BCS    Smaller16        ;     IF (A >= 16) THEN
                LDE    l                ;       l.lo := l.hi
                STE    l:2
                CLRW   l                ;       l.hi := 0;
                SUBA   #16              ;       DEC (A, 16);
                BRA    Smaller8
    Smaller16:  CMPA   #8
                BCS    Smaller8         ;     ELSIF (A >= 8) THEN
                LDAB   l:2              ;       Move l right 8 bits by swapping bytes
                STAB   l:3
                LDE    l
                STE    l:1
                CLR    l
                SUBA   #8               ;       DEC (A, 8);
                                        ;     END;
    Smaller8:   LDE    l                ;   END;
                STAA   l
                LDD    l:2   
                TST    l
                BEQ    EndLoop          ;   IF (count > 0) THEN
                                        ;     /* Note: {count < 8} */
    Loop:       LSRE                    ;     REPEAT
                RORD                    ;       result := result >> 1
                DEC    l                ;       DEC (count);
                BNE    Loop             ;     UNTIL (count = 0);
                                        ;   END;
    EndLoop:
  } /* end asm */
} /* end _frshift */

/******************************************************************************
  IEEE-32 to LONG conversion                                   TW, 03/01/93
    E:D    float                                
    E:D    result
 */

void _FSTRUNC (void)
{
  unsigned char  i;

  asm {
                XGDE                    ; Move hi word to D (easier to unpack exp)
                STAA   i                ; Save sign bit
                LSLD                    ; Exp in A
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
                CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #31              ; ELSIF (exp >= 31) THEN
                BCS    Ok
                LDE    #0x8000          ;   IF (f < 0.0) THEN
                CLRD                    ;     RETURN MIN (LONGINT);
                BRSET  i, #0x80, End    ;   ELSE
                COME                    ;     RETURN MAX (LONGINT);
                COMD                    ;   END;
                BRA    End              ; ELSE
    Ok:         JSR    _frshift
                BRCLR  i, #0x80, End    ;   IF (f < 0.0) THEN
                NEGE                    ;     result := -result;
                NEGD
                SBCE   #0               ;   END;
    End:                                ; END            
  } /* end asm */;
} /* end FSTRUNC */ 

/******************************************************************************
  IEEE-32 to UNSIGNED LONG conversion                          TW, 03/01/93
    E:D    float                                
    E:D    result
 */

void _FUTRUNC (void)
{
  asm {
                XGDE                    ; Move hi word to D (easier to unpack exp)
                LSLD                    ; Exp in A
                BCS    RetZero          ; IF (f < 0.0) THEN RETURN 0
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
    RetZero:    CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #32              ; ELSIF (exp >= 32) THEN
                BCS    Ok
                LDD    #0xFFFF          ;   RETURN MAX (LONGCARD);
                TDE
                BRA    End              ; END
    Ok:         JSR    _frshift 
    End:
  } /* end asm */;
} /* end FUTRUNC */ 


/******************************************************************************
  Auxiliary routine: shift (E, D) until bit #15 of E is 1. Also decrement X
  for each bit shifted. Then, pack it together and return result in D/E!
 */

#define F_EXP_MAX     158

void _fnormant (void)
{
  unsigned long l;

  asm {
                LDX   #F_EXP_MAX
                TSTE
                BMI   Pack
                BNE   Shift
                AIX   #-16              ; Optimize for shift counts > 16
                TDE
                CLRD
                TSTE
                BMI   Pack
    Shift:      AIX   #-1               ; Shift until negative (normalized)
                LSLD
                ROLE
                BPL   Shift
    Pack:       STE   l
                STAA  l:2
                BCLR  l, #0x80          ; Clear hidden bit
                XGDX                    ; A = undef,   B = exp
                XGAB                    ; A = exp,     B = undef
                CLRB                    ; A = exp,     B = 0
                LSRD                    ; A = 0/exp:7, B = exp:1/0:7
                ORAB  l                 ; A = 0/exp:7, B = exp:1/mant:7 ==> D = res.hi
                LDE   l:1               ; E = res.lo
  } /* end asm */;
} /* end _fnormant */

/******************************************************************************
  LONG to IEEE-32                                              TW, 03/01/93
    E:D    long                                
    E:D    result
 */

void _FSFLOAT (void)
{
  unsigned int  i;

  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    STE   i                 ; Set negative flag
                BPL   Pos
                NEGE                    ; Negate mantissa
                NEGD
                SBCE  #0
    Pos:        JSR   _fnormant
                BRCLR i, #0x80, Swap    ; IF (long < 0) THEN
                ORAA  #0x80             ;   Set sign bit
    Swap:       XGDE                    ; END
    End:
  } /* end asm */;
} /* end _FSFLOAT */


/******************************************************************************
  UNSIGNED LONG to IEEE-32                                     TW, 03/01/93
    E:D    long                                
    E:D    result
 */

void _FUFLOAT (void)
{
  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    JSR   _fnormant
                XGDE
    End:
  } /* end asm */;
} /* end _FUFLOAT */

/******************************************************************************
    E:D   x
    @X    y

    Set flags according to x-y
 */

void _FCMP (void)
{
  asm {
                    PSHM  Y
                    XGDY
                    TED
                    EORD  0,X
                    BMI   InverseComp     /*  different signs */
    SameSign:       TSTE
                    BPL   Pos
                    /* Both are negative: compare the other way 'round */
                    /* Or numbers have different signs */
    InverseComp:    ;
                    LDD   0,X
                    XGDE
                    SDE
                    BNE   End
                    XGDY
                    LDE   2,X
                    SDE
                    BRA   End
                    /* Both are positive */
    Pos:            CPE   0,X
                    BNE   End
                    CPY   2,X
    End:            PULM  Y
  } /* end asm */;
} /* end _FCMP */


/******************************************************************************
  IEEE 64 bit arithmetic
 ******************************************************************************/

typedef struct {
  unsigned int hh, hl, lh, ll;
} DOUBLE;

/******************************************************************************
  IEEE 64 bit addition. Arguments are on the stack; the result (par1 + par2) is
  returned in par1 (i.e. the stack is overwritten).
 */

void _DADD (DOUBLE par1, DOUBLE par2)

{
    int  newExp;
    char sign1, sign2, resSign;
    
    asm {     
                PSHM  Y,K
                CLR   resSign             ; result is assumed to be positive
                LDE   par1                ; load first word of first parmeter
                ANDE  #0x7FF0             ; mask exponent
                BNE   not0
                TZY                       ; par1 = 0, so copy par2 in par1
                AIY   @par2
                BRA   uvfl
        not0:   LDD   par2.hh             ; load first word of second parameter
                ANDD  #0x7FF0             ; mask exponent
                BEQ   end                 ; if zero, return par1 as result
                SDE                       ; compute exp1 - exp2
                TZX                       ; prepare X and Y
                TZY
                BPL   shP2             
        shP1:   AIX   @par1               ; exp1 < exp2 => shift par1
                AIY   @par2               ; and compute par2 + par1
                NEGE     
                BRA   cont
                
        shP2:   AIX   @par2               ; exp1 > exp2 => shift par2
                AIY   @par1               ; and compute par1 + par2
                
        cont:   CPE   #0x350
                BLT   noUVFL
        uvfl:   LDD   0,Y                 ; We'd shift @X completely away...
                STD   par1.hh             ; ... return @Y
                LDD   2,Y
                STD   par1.hl
                LDD   4,Y
                STD   par1.lh
                LDD   6,Y
                STD   par1.ll
                BRA   end
        noUVFL: LDAA  0,X
                STAA  sign2               ; Store sign bit
                LDD   0,Y
                STAA  sign1               ; Store sign bit
                ANDD  #0x7FF0
                STD   newExp              ; Store new exponent
                BCLRW par1,#0xFFF0
                BSET  par1:1,#0x10        ; Hidden bit
                BCLRW par2,#0xFFF0
                BSET  par2:1,#0x10        ; Hidden bit
                
        ; shift the mantissa pointed to by X right by E bits
        
                BRA   chk16
        sh16:   LDD   4,X
                STD   6,X
                LDD   2,X
                STD   4,X
                LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #0x100              ; E -= 16
        chk16:  CPE   #0x100              ; E >= 16 ??
                BGE   sh16
        chk8:   CPE   #0x80               ; E >= 8
                BLT   chk1
        sh8:    LDD   5,X
                STD   6,X
                LDD   3,X
                STD   4,X
                LDD   1,X
                STD   2,X
                CLRW  0,X
                SUBE  #0x80               ; E -= 8
                BRA   chk1
        sh1:    LSRW  0,X        
                RORW  2,X
                RORW  4,X
                RORW  6,X
        chk1:   SUBE  #0x10               ; E -= 1
                BGE   sh1
                
        madd:   BRCLR sign2,#0x80,xPos
                CLRD                      ; Negate @X
                CLRE
                SUBD  6,X
                STD   6,X
                TED
                SBCD  4,X
                STD   4,X
                TED
                SBCD  2,X
                STD   2,X
                TED
                SBCD  0,X
                STD   0,X
                
        xPos:   BRCLR sign1,#0x80,yPos
                CLRD                      ; Negate @Y
                CLRE
                SUBD  6,Y
                STD   6,Y
                TED
                SBCD  4,Y
                STD   4,Y
                TED
                SBCD  2,Y
                STD   2,Y
                TED
                SBCD  0,Y
                STD   0,Y
                
        yPos:   LDD   6,X                 ; Add them
                ADDD  6,Y
                STD   par1.ll
                LDD   4,X
                ADCD  4,Y
                STD   par1.lh
                LDD   2,X
                ADCD  2,Y
                STD   par1.hl
                LDD   0,X
                ADCD  0,Y
                STD   par1.hh
                BPL   normal
                CLRD                      ; Result is negative: negate and set flag
                CLRE
                SUBD  par1.ll
                STD   par1.ll
                TED
                SBCD  par1.lh
                STD   par1.lh
                TED
                SBCD  par1.hl
                STD   par1.hl
                TED
                SBCD  par1.hh
                STD   par1.hh
                COM   resSign             ; Result is negative!!
                
        normal: LDE   newExp               
                BRCLR par1:1,#0x20,subN
                LSRW  par1.hh             ; Overflow: shift right by 1
                RORW  par1.hl
                RORW  par1.lh
                RORW  par1.ll
                ADDE  #0x10               ; and correct exponent
        subN:   BRSET par1:1, #0x10, pack ; De-normalized: normalize
                LDD   par1.hh             ; Test for zero
                BNE   shN
                LDD   par1.hl
                BNE   shN
                LDD   par1.lh
                BNE   mov16
                LDD   par1.ll
                BEQ   end                 ; Return zero
        mov32:  LDD   par1.ll             ; HH, HL and LH words are zero
                STD   par1.hl
                CLRW  par1.lh
                CLRW  par1.ll
                SUBE  #0x200              ; exp -= 32
                BRA   shN
        mov16:  LDD   par1.lh             ; HH and HL words are zero
                STD   par1.hl
                LDD   par1.ll
                STD   par1.lh
                CLRW  par1.ll
                SUBE  #0x100              ; exp -= 16
        shN:    BRSET par1:1,#0x10,pack
                LSLW  par1.ll
                ROLW  par1.lh
                ROLW  par1.hl
                ROL   par1:1
                SUBE  #0x10               ; exp -= 1
                BRA   shN      
        pack:   BCLR  par1:1,#0x10        ; Hidden bit
                ORE   par1.hh
                BRCLR resSign,#0x80, pos
                ORE   #0x8000 
        pos:    STE   par1.hh
        end:    PULM  Y,K
    } /* end asm */;
} /* end _DADD */

#pragma NO_EXIT

void _DSUB (DOUBLE par1, DOUBLE par2)
  /**** Calculate par2 - par1 and return the result in par1 */
{
  asm { 
                LDAA  par1                ; Negate sign
                EORA  #0x80
                STAA  par1
                PULM  Z                   ; Correct stack...
                JMP   _DADD               ; ... and add!
  } /* end asm */;
} /* end _DSUB */

/******************************************************************************
  IEEE 64 bit multiplication. Arguments are on the stack; the result (par1 * par2)
  is returned in par1 (i.e. the stack is overwritten).
 */

void _DMUL (DOUBLE par1, DOUBLE par2)

{
    int          counter1, counter2;
    int          carry, temp;
    unsigned int result[8];
    int          newExp;
    
    asm { 
                PSHM Y,K
                
        ; Compute the new exponent
                
                LDD par1           ; load first word of first parameter
                ANDD #0x7FF0       ; mask exponent
                BEQ zero           ; if zero, then result is zero
                
                LDE par2           ; load first word of second parameter
                ANDE #0x7FF0       ; mask exponent
                BEQ zero           ; if zero, then result is zero
                
                ADE                ; add the 2 exponents
                SUBE #0x3FF0       ; substract bias
                
                LDD par1           ; load first word of first parameter
                EORD par2          ; xor with the 1 st word of the 2 nd param.
                ANDD #0x8000       ; mask the sign bit
                
                ADE                ; add the sign to the new exponent
                STE newExp         ; newExp is now the exponent of the result

        ; Extract mantissa of par1
               
                LDE par1
                ANDE #0x000F
                ORE  #0x0010
                STE par1
                
        ; Extract mantissa of par2
        
                LDE par2
                ANDE #0x000F
                ORE  #0x0010
                STE par2
                
        ; First Multiply par2 with the last word of par1 and store the
        ; result in the array result.
        
                CLRW carry         ; carry is initialise to 0
                
                LDD par1:6
                STD temp           ; temp := last word of the mantissa of par1
                
                TZY
                AIY @result:14     ; Y points to where the result must be written

                TZX
                AIX @par2:6        ; X points to the last word of the mantissa of par2
                LDD #3             ; iterate 4 times (3..0)
                STD counter2

        lab0:   LDE temp           ; begin of the loop
                LDD 0,X
                EMUL               ; Multiply temp with a word of par2
                ADDD carry         ; Add carry to low word of result
                ADCE #0            ; Adjust high word of result if the last
                                   ; operation produces a carry

                STD 0,Y            ; store low word of result
                STE carry          ; store high word in the carry
                AIX #-2            ; decrement pointer X by 2
                AIY #-2            ; decrement pointer Y by 2
                DECW counter2      ; DEC(counter2)
                BPL lab0           ; end of loop

                LDD carry
                STD 0,Y            ; Store carry

        ; Then multiply par2 with the other 3 words of par1 and adjust the
        ; result
                
                LDD #4             ; iterate 3 times (3,2,0)
                STD counter1

        lab1:   CLRW carry         ; begin the outer loop with carry = 0
                
                TZX
                AIX @par1          ; X points to the begin of par1
                                   ; XK already correct!
                LDE counter1
                LDD E,X
                STD temp           ; temp := word of par1 with which we 
                                   ; will multiply par2
                TZY                   
                AIY @result:8
                LDD counter1
                ADY                ; Y points where the result must be written
                
                TZX
                AIX @par2:6        ; X point to the last word of par2
                                   ; XK already correct!
                LDD #3             ; iterate 4 times (3..0)
                STD counter2

        lab2:   LDE temp           ; begin of the loop
                LDD 0,X
                EMUL               ; multiply temp with a word of par2
                ADDD carry         ; add carry to low word of result
                ADCE #0            ; Adjust high word of result if the last
                                   ; operation produces a carry

                ADDD 0,Y           ; add the low word with the last result
                STD 0,Y            ; store the noew word of the result
                ADCE #0            ; adjust high word if carry
                STE carry          ; store high word in carry

                AIX #-2            ; decrement pointer X by 2
                AIY #-2            ; decrement pointer Y by 2
                DECW counter2      ; DEC(counter2)
                BPL lab2           ; end of inner loop

                LDD carry
                STD 0,Y            ; store carry
                
                DECW counter1
                DECW counter1      ; decrement counter1 by 2
                BPL lab1           ; end of outer loop

        ; align the mantissa of the result
                
                BRCLR result:2,#0x02,normal
                
                LDD newExp         ; If the bit 106 of the result if set
                ADDD #0x0010       ; then increment the exponent
                STD newExp
                
                LSRW result:2      ; and shift the result
                RORW result:4
                RORW result:6
                RORW result:8

        normal: LDD #3             ; shift the result 4 times (3..0)
        adjust: LSRW result:2
                RORW result:4
                RORW result:6
                RORW result:8
                ADDD #-1
                BPL adjust
                
                BCLRW result:2,#0xFFF0  ; clear implicit 1

        ; copy result in par1
                
                LDD result:2
                ADDD newExp         ; add sign and exponent
                STD par1:0
                LDD result:4
                STD par1:2
                LDD result:6
                STD par1:4
                LDD result:8
                STD par1:6
                BRA end

        ; copy zero in par1
                
        zero:   CLRD
                STD par1:0
                STD par1:2
                STD par1:4
                STD par1:6

        end:    PULM Y,K
    }
}

/******************************************************************************
  IEEE 64 bit divison. Arguments are on the stack; the result (par1 / par2)
  is returned in par2 (i.e. the stack is overwritten).
 */

void _DDIV (DOUBLE par2, DOUBLE par1)

{ 
    int newExp;
    
    unsigned int u[9];
    unsigned int v[4];
    unsigned int r[5];
    int q, i, j;
    int borrow, rest;    
    
    asm {
                    PSHM Y,K
                    
        ; Compute the new exponent                                

                    LDD par2           ; load first word of second parameter
                    ANDD #0x7FF0       ; mask exponent
                    BEQ div0           ; if zero, then division by zero error

                    LDE par1           ; load first word of first parameter
                    ANDE #0x7FF0       ; mask exponent
                    BEQ zero           ; if zero, then result is zero
                
                    SDE                ; substract exponents
                    ADDE #0x3FF0       ; add bias

                    LDD par1           ; load first word of first parameter
                    EORD par2          ; xor with the 1 st word of the
                                       ; 2 nd param.
                    ANDD #0x8000       ; mask the sign bit
                
                    ADE                ; add the sign to the new exponent
                    STE newExp         ; newExp is now the exponent of 
                                       ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                    CLRD
                    STD u:16
                    STD u:14
                    STD u:12
                    STD u:10
                    STAA u:9
                
                    LDAA par1:7
                    LSLA
                    STAA u:8
                
                    LDE par1:5
                    ROLE
                    STE u:6
                
                    LDE par1:3
                    ROLE
                    STE u:4
                    
                    LDE par1:1
                    ANDE #0x0FFF       ; remove the sign and the exponent
                    ORE #0x1000        ; add the implicit 1
                    ROLE
                    STE u:2               
                
                    CLRW u:0           ; was CLR u:0 !!!!!
                
                    LSLW u:8
                    ROLW u:6
                    ROLW u:4
                    ROLW u:2
                
                    LSLW u:8
                    ROLW u:6
                    ROLW u:4
                    ROLW u:2
        
        ; Compute the normalized fractional part of the second parameter        
        ; and store it in v
        
                    CLR v:7
                    
                    LDAA par2:7
                    LSLA
                    STAA v:6
                 
                    LDE par2:5
                    ROLE
                    STE v:4
                
                    LDE par2:3
                    ROLE
                    STE v:2
                
                    LDE par2:1
                    ANDE #0x0FFF       ; remove sign and exponent 
                    ORE #0x1000        ; add implicit 1
                    ROLE
                    STE v:0
                
                    LSLW v:6
                    ROLW v:4
                    ROLW v:2
                    ROLW v:0
                
                    LSLW v:6
                    ROLW v:4
                    ROLW v:2
                    ROLW v:0


        ; Compute the quotient of the mantissa.
                
                    CLRW j              ; counter = 0
                    
                    TZY
                    AIY @u              ; Y points to u[j = 0]
                    BRA loop

        mainLoop:   TZY
                    AIY @u              ; Y points to u[0]
                    LDD j
                    ASLD
                    ADY

        ; Calculate the first estimation of q

        loop:       LDE 0,Y             ; E = u[j]
                    CPE v:0             ; IF u[j] == v[1] THEN
                    BNE l1              ;
                    LDX #0xFFFF         ;   X := b-1
                    STX q
                    
                    LDD 0,Y             ;   rest = u[j] + u[j+1]          
                    ADDD 2,Y
                    
                    BCS substr
                    STD rest
                    BRA fastCheck
                    
        l1:         LDD 2,Y             ; ELSE  D = u[j+1]
                    LDX v:0
                    EDIV                ;   X = u[j]:u[j+1] / v[1]

        l2:         STX q
                    STD rest

        ; Fast Check to see if q is too big

        fastCheck:  LDE v:2             ; E = v[2]
                    LDD q               ; D = q
                    EMUL
                    
                    CPE rest
                    BHI decq
                    BNE substr
                    
                    CPD 4,Y
                    BLS substr
                    
        decq:       DECW q              ; Decrement q
                    LDD rest            ; Adjust rest
                    ADDD v:0
                    BCS substr
                    STD rest
                    BRA fastCheck

        ; substract q * v from u

        substr:     CLRW borrow         ; Start with no borrow

                    TZX
                    AIX @v:6            ; X points to v[n]

                    TZY
                    AIY @u:8
                    LDD j
                    ASLD
                    ADY                 ; Y points to u[j+n]

                    LDE #3
                    STE i               ; i = 3..0

        sl1:        LDE 0,X
                    LDD q
                    EMUL                ; E:D = q * v[n]

                    ADDD borrow
                    ADCE #0             ; E:D = q * v[n] + borrow

                    STE borrow
                    LDE 0,Y             ; E = u[j+n]
                    SDE                 ; E = E - D
                    STE 0,Y             ; Store new u[j+n]
                    
                    BCC noB
                    INCW borrow         ; correct borrow if necessary

        noB:        AIX #-2
                    AIY #-2
                    DECW i
                    BPL sl1

                    LDE 0,Y
                    SUBE borrow
                    STE 0,Y
                    
                    BCC ok

        ; q was too big, we have to add v back to u

                    TZX
                    AIX @v              ; X points to v[1]

                    TZY
                    AIY @u
                    LDD j
                    ASLD
                    ADY                 ; Y points to u[j]

                    LDD 6,X
                    ADDD 6,Y
                    STD 6,Y
                    
                    LDD 4,X
                    ADCD 4,Y
                    STD 4,Y
                    
                    LDD 2,X
                    ADCD 2,Y
                    STD 2,Y
                    
                    LDD 0,X
                    ADCD 0,Y
                    STD 0,Y
                    
                    DECW q

        ok:         TZY
                    AIY @r
                    LDD j
                    ASLD
                    ADY                ; Y points to r[j]

                    LDE q
                    STE 0,Y             ; store q in r[j]

                    INCW j
                    LDE j
                    CPE #4
                    BLE mainLoop
                
                
        ; denormalize and move the result in par2. if necessary, correct
        ; the exponent of the result.
        
        

        exit:       TSTW r
                    BNE dnl1
                    LDD newExp
                    SUBD #0x0010
                    STD newExp
                    BRA dnl2
                    
        dnl1:       LSRW r:0
                    RORW r:2
                    RORW r:4
                    RORW r:6
                    RORW r:8
                    
        dnl2:       LSRW r:2
                    RORW r:4
                    RORW r:6
                    RORW r:8
                    
                    LSRW r:2
                    RORW r:4
                    RORW r:6
                    RORW r:8
                    
                    LDD r:2
                    LSRD
                    ANDD #0x0FFF;
                    STD par2:1
                    
                    LDD r:4
                    RORD
                    STD par2:3
                    
                    LDD r:6
                    RORD
                    STD par2:5
                    
                    LDD r:8
                    RORD
                    STAA par2:7
                    
                    LDE par2:0
                    ANDE #0x000F
                    ADDE newExp
                    STE par2:0
                                    
                    BRA end
                    
        div0:       LDX #0             ; Simulate a division by 0
                    EDIV
                        
        zero:       CLRD
                    STD par2:0        ; result is zero
                    STD par2:2
                    STD par2:4
                    STD par2:6
                
        end:        PULM Y,K

    }
}

/******************************************************************************
  IEEE 64bit comparison.

  Arguments:   p at S + 12
               q at S + 4

  Result:      Set CCR according to q - p
 */

void _DCMP (DOUBLE p, DOUBLE q)
{
  asm {
                    PSHM  Y,K
                    LDD   p.hh
                    EORD  q.hh
                    BPL   SameSign
                    /* The two values have different signs */
                    LDD   p.hh
                    CPD   q.hh
                    BRA   End
    SameSign:       TZX             ; Prepare pointers
                    TZY 
                    BRCLR q.hh, #0x80, Pos
                    /* Both are negative: compare the other way 'round! */
                    AIY   @p
                    AIX   @q
                    BRA   Comp
                    /* Both are positive */
    Pos:            AIY   @q
                    AIX   @p
                    /* Now set flags according to @Y - @X */
    Comp:           LDD   0,Y
                    CPD   0,X
                    BNE   End
                    LDD   2,Y
                    CPD   2,X
                    BNE   End
                    LDD   4,Y
                    CPD   4,X
                    BNE   End
                    LDD   6,Y
                    CPD   6,X            
    End:            PULM  Y,K
  } /* end asm */;
} /* _DCMP */

/******************************************************************************
  Conversion HC16 DSP format to IEEE 64bit format.

  Arguments:   E/D: DSP-float (E = Mantissa in fix point format,
                               D = Exponent in 2's complement)

  Result:      At S + 4. Space reserved by compiler. Note: we can access that
               memory location using "parameter" res.
 */

#define DBL_BIAS          1023
#define DBL_MAX           2047
#define DBL_INF_EXP     0x7FF0

void _DSPTOD (DOUBLE res)
{
  asm {
                    TSTE
                    BNE   NotZero
                    CLRW  res.hh
                    CLRW  res.hl
                    CLRW  res.lh
                    CLRW  res.ll
                    BRA   End
    NotZero:        XGDE                   ; D = mant; E = exp
                    CLRW  res.lh
                    STAA  res.ll           ; Save sign bit
                    BPL   Pos
                    NEGD                   ; Negate mantissa
    Pos:            LSLD                   ; Hidden bit of IEEE format
                    LSLD
                    STD   res.hl           ; Store it
                    CLRW  res.hh
                    LSLW  res.hl           ; Shift left 4 times
                    ROL   res.hh:1
                    LSLW  res.hl
                    ROL   res.hh:1
                    LSLW  res.hl
                    ROL   res.hh:1
                    LSLW  res.hl
                    ROL   res.hh:1         ; Now it's aligned correctly
                    XGDE                   ; D is exp again
                    ADDD  #(DBL_BIAS - 1)  ; -1 because of hidden bit...
                    BVS   Infinity         ; Overflow, return infinity
                    BPL   ChkMax
                    CLRD                   ; Exponent too small, return zero
                    CLRW  res.hl
                    BRA   WasPos           ; Always return +0.0
    ChkMax:         CPD   #DBL_MAX
                    BLE   Ok
    Infinity:       CLRW  res.hl           ; Exp > 2047, return infinity
                    LDD   #DBL_INF_EXP     ; Load infinity's exponent
                    BRA   ChkSign          ; Set sign correctly
    Ok:             LSLD
                    LSLD
                    LSLD
                    LSLD
                    ORD   res.hh
    ChkSign:        BRCLR res.ll, #0x80, WasPos
                    ORAA  #0x80
    WasPos:         CLRW  res.ll
                    STD   res.hh 
    End:
  } /* end asm */;
} /* _DSPTOD */

/******************************************************************************
  Conversion IEEE 32bit format to IEEE 64bit format.

  Arguments:   E/D: float

  Result:      At S + 4. Space reserved by compiler. Note: we can access that
               memory location using "parameter" res.
 */

#define FLT_BIAS    127

void _DLONG (DOUBLE res)
{
  asm {
                    TSTD
                    BNE    NotZero
                    TSTE
                    BNE    NotZero
                    CLRW   res.hh
    ClrMant:        CLRW   res.hl
                    CLRW   res.lh
                    CLRW   res.ll
                    BRA    End
    NotZero:        XGDE
                    STD    res.ll           ; Save D
                    LSLD                    ; Exp in A
                    CLRB
                    XGAB
                    CMPB   #0xFF
                    BNE    NotInf
                    LDD    #0x7FF0
                    TST    res.ll
                    BPL    PosInf
                    ORAA   #0x80
    PosInf:         STD    res.hh
                    BRA    ClrMant
    NotInf:         ADDD   #(DBL_BIAS - FLT_BIAS)     ; New exp in D
                    TST    res.ll
                    BPL    Pos
                    ORAA   #8               ; Set sign bit
    Pos:            LSLD
                    LSLD
                    LSLD
                    LSLD                    ; Left align
                    STD    res.hh
                    LDD    res.ll
                    ANDD   #0x007F          ; Mantissa in D/E
                    LSRB                    ; Align correctly
                    RORE
                    RORA
                    LSRB
                    RORE
                    RORA
                    LSRB
                    RORE
                    RORA
                    STE    res.hl 
                    STAA   res.lh
                    CLR    res.lh:1
                    CLRW   res.ll
                    CLRA
                    ORD    res.hh
                    STD    res.hh
    End:
  } /* end asm */;
} /* end _DLONG */

/******************************************************************************
  Conversion IEEE 64bit format to HC16 DSP format.

  Arguments:   X is &double

  Result:      E:D is DSP float (E = mant, D = exp)
 */

void _DTODSP (void)
{
  asm {
                    TSTW  0,X
                    BNE   NotZero
                    TSTW  2,X
                    BNE   NotZero
                    CLRD
                    TDE
                    BRA   End
    NotZero:        LDAB  1,X
                    LDE   2,X
                    ANDB  #0xF
                    ORAB  #0x10            ; Hidden bit (B:E = Mantissa)
                    LDAA  #6               ; Shift count
    Shift:          LSRB
                    RORE
                    DECA
                    BNE   Shift
                    TST   0,X
                    BPL   IsPos
                    NEGE                   ; E is mantissa
    IsPos:          LDD   0,X              ; Load exponent
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #(DBL_BIAS - 1)  ; -1 because of hidden bit...
    End:
  } /* end asm */;
} /* end _DTODSP */

/******************************************************************************
  Conversion IEEE 64bit format to IEEE 32bit format.

  Arguments:   X is &double

  Result:      E:D is IEEE float (E = hi, D = lo)
 */

#define FLT_INF_EXP   0x7F80

void _DSHORT (void)
{
  unsigned long l;

  asm {
                    LDD   0,X
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #(DBL_BIAS - FLT_BIAS)
                    BPL   ChkInf
                    CLRD                    ; Return zero
                    TDE
                    BRA   End
    ChkInf:         TSTA
                    BEQ   Ok
                    CLRD                    ; Return +/- infinity
                    LDE   #FLT_INF_EXP
                    TST   0,X
                    BPL   End
                    ORE   #0x8000
                    BRA   End;
    Ok:             LSRB                    ; Shift left by 7 implemented
                    RORA                    ; As rotate right by 1 and
                    XGAB                    ; swap
                    TST   0,X
                    BPL   IsPos             ; Set sign bit correctly
                    ORAA  #0x80
    IsPos:          STD   l
                    LDD   1,X
                    LDE   3,X
                    LSLE
                    ROLD
                    LSLE
                    ROLD
                    LSLE
                    ROLD
                    ANDA  #0x7F
                    STD   l:2
                    TED
                    TAB
                    LDAA  l:3
                    TDE
                    LDD   l
                    ORAB  l:2
                    XGDE
     End:
  } /* end asm */;
} /* end _DSHORT */

/******************************************************************************
  Auxiliary routine to align mantissa correctly. X is &double; shifts right by
  31 - D bits.
 */

void _drshift (void)
{
  unsigned long a;

  asm {

                    NEGD
                    ADDD  #31
                    TDE
                    LDD   3,X
                    STD   a
                    LDD   1,X
                    ANDA  #0xF
                    ORAA  #0x10           ; Hidden bit!!!
                    SUBE  #3
                    BEQ   LoadIt
    DoLeft:         BGT   DoRight
                    LDX   5,X
                    STX   a:2
    ShLeft:         LSLW  a:2
                    ROLW  a
                    ROLD
                    ADDE  #1
                    BNE   ShLeft
                    BRA   LoadIt
    DoRight:        CPE   #24
                    BCS   Smaller24
                    STAA  a:1
                    CLR   a
                    CLRD
                    SUBE  #24
                    BRA   Smaller8
    Smaller24:      CPE   #16
                    BCS   Smaller16
                    STD   a
                    CLRD
                    SUBE  #16
                    BRA   Smaller8
    Smaller16:      CPE   #8
                    BCS   Smaller8
                    XGDX
                    LDAB  a
                    STAB  a:1
                    XGDX
                    STAB  a
                    CLRB
                    XGAB
                    SUBE  #8
    Smaller8:       BEQ   LoadIt
    Loop:           LSRD
                    RORW  a
                    SUBE  #1
                    BNE   Loop
    LoadIt:         LDE   a
                    XGDE

  } /* end asm */;
} /* end _drshift */

/******************************************************************************
  Conversion IEEE 64bit format to UNSIGNED LONG.

  Arguments:   X is &double

  Result:      E:D is LONGCARD
 */

void _DUTRUNC (void)
{
  asm {
                    LDD   0,X
                    BMI   IsNeg              ; IF (d < 0.0) THEN RETURN 0
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #DBL_BIAS
                    BGE   NonNeg             ; IF (exp < 0) THEN
    IsNeg:          CLRD
                    TDE                      ;   RETURN 0
                    BRA   End
    NonNeg:         CPD   #32                ; ELSIF (exp >= 32) THEN
                    BCS   Ok
                    LDD   #0xFFFF            ;   RETURN MAX (LONGCARD)
                    TDE
                    BRA   End                ; END
    Ok:             JSR   _drshift
    End:
  } /* end asm */;
} /* end _DUTRUNC */

/******************************************************************************
  Conversion IEEE 64bit format to LONG.

  Arguments:   X is &double

  Result:      E:D is LONGINT
 */

void _DSTRUNC (void)
{
  asm {
                    LDD   0,X
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #DBL_BIAS
                    BGE   NonNeg             ; IF (exp < 0) THEN
    IsNeg:          CLRD
                    TDE                      ;   RETURN 0
                    BRA   End
    NonNeg:         CPD   #31                ; ELSIF (exp >= 31) THEN
                    BCS   ShiftIt
                    LDE   #0x8000            ;   IF (f < 0.0) THEN
                    CLRD                     ;     RETURN MIN (LONGINT);
                    BRSET 0,X, #0x80, End    ;   ELSE
                    COME                     ;     RETURN MAX (LONGINT);
                    COMD                     ;   END;
                    BRA   End                ; END
    ShiftIt:        JSR   _drshift
                    BRCLR 0,X, #0x80, End    ; IF (d < 0.0) THEN
                    NEGE                     ;   result := result;
                    NEGD
                    SBCE  #0
    End:                    
  } /* end asm */;
} /* end _DSTRUNC */

/******************************************************************************
  Auxiliary routine: shift E, D until bit #15 of E is 1. Also decrement X for
  each bit shifted. Than pack it together and store at @Y!
 */

#define DBL_EXP_MAX     (DBL_BIAS + 31)

void _dnormant (void)
{
  asm {
                    LDX   #DBL_EXP_MAX
                    TSTE
                    BMI   Pack
                    BNE   Shift
                    AIX   #-16
                    TDE
                    CLRD
                    TSTE
                    BMI   Pack
    Shift:          AIX   #-1
                    LSLD
                    ROLE
                    BPL   Shift
    Pack:           LSLD                      ; Hidden bit
                    ROLE
                    STD   4,Y
                    XGDX
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    STD   0,Y
                    STE   2,Y
                    CLRW  6,Y
  } /* end asm */;
} /* end _dnormant */

/******************************************************************************
  Conversion UNSIGNED LONG to IEEE 64bit format.

  Arguments:   E:D is LONGCARD

  Result:      S+4 is double
 */

void _DUFLOAT (DOUBLE res)
{
  asm {
                    TSTE
                    BNE   NotZero
                    TSTD
                    BNE   NotZero
                    STD   res.hh
                    STD   res.hl
                    STD   res.lh
                    STD   res.ll
                    BRA   End
    NotZero:        PSHM  Y,K
                    TZY
                    AIY   @res
                    JSR   _dnormant 
                    PULM  Y,K
    End:
  } /* end asm */;
} /* end _DUFLOAT */


/******************************************************************************
  Conversion LONG to IEEE 64bit format.

  Arguments:   E:D is LONGINT

  Result:      S+4 is double
 */

void _DSFLOAT (DOUBLE res)
{
  unsigned int i;

  asm {
                    TSTE
                    BNE   NotZero
                    TSTD
                    BNE   NotZero
                    STD   res.hh
                    STD   res.hl
                    STD   res.lh
                    STD   res.ll
                    BRA   End
    NotZero:        STE   i                 ; Set negative flag
                    BPL   Pos
                    NEGE
                    NEGD
                    SBCE  #0
    Pos:            PSHM  Y,K
                    TZY
                    AIY   @res
                    JSR   _dnormant
                    PULM  Y,K
                    BRCLR i, #0x80, End
                    BSET  res.hh, #0x80
    End:                      
  } /* end asm */;
} /* end _DSFLOAT */

/******************************************************************************/
/* end rtshc16.c */

#else
/*****************************************************
  FILE        : RTSHC16.c - Runtime support for M68HC16.
  COMMENT     : 
    _Hxxx routines implement DSP arithmetic.
    _Fxxx routines implement IEEE 32bit arithmetic
    _Dxxx routines implement IEEE 64bit arithmetic, including conversions
          to/from the other two formats.
    DSP format makes use of the M68HC16's fast DSP instructions. The hi word
    is a fix point mantissa in 2's complement. Bit #15 can be used as sign bit,
    bits #14 to #0 are the mantissa (bit #14 is 0.5, #13 is 0.25). There's no
    hidden bit.
      The exponent is in the low word in 2's complement and ranges from -32768
    to 32767.
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

int errno;           /* implemented here because Modula-2 uses 'Math.c'.  */

/******************************************************************************
   DSP FLOATING POINT ROUTINES
 ******************************************************************************/

#pragma NO_FRAME

void _HMUL (void)  /*   parameters are passed in registers:  */
                   /*   D:E operand 1                        */
                   /*    Y  @operand 2    D:E * @Y           */
                   /*   D:E result                           */
{
   asm {
           ADDD  2,Y      ; add exponents
           LDX   0,Y      ; load mantissa of op2
           XGDX           ; prepare D:E for multiplication, save new exponent
           FMULS          ; E = D * E -> new mantissa
           XGDX           ; restore new exponent
           BVC   L1       ; was there an overflow
           ADDD  #1       ; yes, adjust exponent and
           LSRE           ; normalize mantissa
     L1:   ADCE  #0       ; round mantissa
           BEQ   Zero     ; is the result zero
           LSLE           ; no, normalize mantissa
           BVC   L3       ; if was normalized already
           RORE           ; undo shift again
           RTS
     L3:   ADDD  #-1      ; adjust exponent
           RTS
     Zero: CLRD           ; result is zero
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HDIV (void)  /*   parameters are passed in registers:  */
                   /*   D:E dividend                         */
                   /*    Y  divisor      D:E / @Y            */
                   /*   D:E result                           */
{
   asm {   
           TSTW  0,Y      ; is divisor zero
           BEQ   Z_DIV    ; yes, do a zero division
           TSTE           ; is dividend zero
           BEQ   ZERO     ; yes, return zero
           SUBD  2,Y      ; E.result = E.dividend - E.divisor
           PSHM  E,Y      ; save M.dividend(E), temp(Y)
           LDE   0,Y      ; M.divisor
           TSY            ; points to saved values
           STE   0,Y      ; temp (M.divisor) for sign of result
           BPL   L1       ; is neg -> invert
           NEGE           ; M.divisor
           BVC   L1       ; if overflow
           LSRE           ; normalize
           ADDD  #-1      ; adjust E.result
    L1:    TSTW  2,Y      ; M.dividend > 0
           BPL   L3       ; is neg -> invert
           COM   0,Y      ; invert sign of result
           NEGW  2,Y      ; M.dividend
           BVC   L3       ; if overflow
           LSRW  2,Y      ; normalize
           ADDD  #1       ; adjust E.result
           BRA   L3
    LOOP:  LSRW  2,Y      ; else M.dividend >> 1     
           ADDD  #1       ; adjust E.result    
    L3:    CPE   2,Y      ; M.divisor > M.dividend
           BLE   LOOP     ; yes, continue
           XGDX           ; save E.result to X
           LDD   2,Y      ; M.dividend
           LSLD           ; left adjust M.dividend
           LSLE           ; left adjust M.divisor
           XGEX           ; prepare operands for fdiv, save X to E
           FDIV           ;
           XGEX           ; restore X from E, M.result > E
           LSRE           ; adjust M.result (DSP format)
           LSLD           ; round (based on remainder)
           ADCE  #0       ;
           XGDX           ; reload E.result from X
           TSTW  0,Y      ; sign of result
           BPL   L4
           NEGE           ; result is negative, negate
           CPE   #0xC000  ; special case, do not normalize
           BGE   L6
    L4:    LSLE           ; normalize mantissa
           BVS   L5       ;
           ADDD  #-1      ; adjust E.result
           BRA   L4
    L5:    RORE
    L6:    AIS   #4
           RTS
           
    ZERO:  CLRD           ; return zero
           CLRE
           RTS 
           
    Z_DIV: CLRD           ; force zero divide
           XGDX
           IDIV  
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HADD2 (void) /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    Y               D:E + @Y            */
                   /*   D:E result                           */
{
   asm {   
           TSTE           ; is operand 1 zero ?
           BEQ   R_OP2    ; yes, return operand 2
           TSTW  0,Y      ; is operand 2 zero ?
           BEQ   R_OP1    ; yes, return operand 1
           CPD   2,Y      ; same exponents
           BEQ   DO_ADD   ; yes
           BMI   L1       ; exchange operands
           LDX   0,Y
           XGEX
           STX   0,Y
           LDX   2,Y
           XGDX
           STX   2,Y      ; now, operand (E:D) < operand (@Y)
   L1:     SUBD  2,Y      ; exp (E:D) - exp @Y
           CPD   #-15     ; if operands too different
           BMI   R_OP2    ; then return bigger operand
           CPD   #-7      ; if more 7 bits to shift
           BPL   L2       ; no, shift only
           XGDE
           TAB            ; yes, simulate shift right by 8
           SXT            ; 
           XGDE
           ADDD  #8       ; adjust shift count
           BEQ   DO_ADD
   L2:     ASRE           ; shift right remainding bits
           ADDD  #1
           BNE   L2       ;
   DO_ADD: LDD   2,Y      ; load bigger exponent
           ADDE  0,Y      ; add mantissas
           BVS   L4       ; if overflow
           BEQ   R_ZERO   ; if result is zero, return zero
   L3:     LSLE           ; normalize
           BVS   L5
           ADDD  #-1
           BRA   L3
   L4:     RORE
           ADDD  #1       ; shift right,
           RTS 
   L5:     RORE           ; is normalized now
           RTS
           
   R_ZERO: TED
           RTS  
               
   R_OP2:  LDE   0,Y      ; return operand 2
           LDD   2,Y
   R_OP1:
   }
}        

/******************************************************************************/

#pragma NO_FRAME

void _HSUB (void)  /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    Y               D:E - @Y            */
                   /*   D:E result                           */
{
   asm {   
           PSHM  D,E      ; save subtrahend
           LDE   0,Y
           LDD   2,Y      ; load subtractor
           NEGE           ; negate subtractor
           BVC   L2
           LSRE  
           ADDD  #1
     L2:
           TSY            ; setup pointer to subtrahend
           JSR   _HADD2   ; call common add routine
           AIS   #4    
   }
}


/******************************************************************************/

#pragma NO_FRAME

void _HADD (void)  /*   parameters are passed in registers:  */
                   /*   D:E                                  */
                   /*    Y               D:E + @Y            */
                   /*   D:E result                           */
{
   asm {   
           PSHM  D,E
           LDE   0,Y
           LDD   2,Y
           TSY
           JSR   _HADD2   ; call common add routine
           AIS   #4    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HCMP (void)  /*   parameters are passed in registers:  */
                   /*   D:E operand 1                        */
                   /*    Y  operand 2    D:E - @Y            */
                   /*   CCR result                           */
{
    asm {
           EORE  0,Y      ; XOR mantissas: XOR sign bit
           BMI   L2       ; same sign ?
           EORE  0,Y      ; yes, same sign, undo XOR of mantissas
           BEQ   L3       ; is operand 1 zero
           TSTW  0,Y      ; is operand 2 zero
           BEQ   L4       ;
           CPD   2,Y      ; compare exponents
           BEQ   L3       ; exponents differ
           TPA            ; save CCR
           TSTE           ; check sign bit
           BPL   L1       ; if positive, CCR already ok
           EORA  #8       ; no, invert N bit in saved CCR
   L1:     TAP            ; restore CCR
           RTS
           
   L4:     TSTE   
           RTS
      
   L2:     EORE  0,Y      ; undo XOR of mantissas
   L3:     CPE   0,Y      ; set flags in CCR
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HNEG(void)   /* negate DSP float:         */
                   /*   D:E operand and result  */
{
   asm {   
           NEGE     
           BVC   L1
           LSRE  
           ADDD  #1
       L1:
   }
}

/* this operation can be done inline and/or in memory: 
           NEGW   (ea)
           BVC    L1
           LSRW   (ea)
           INCW   (ea+2)
       L1:
*/
  
/******************************************************************************/

#pragma NO_FRAME         

void _HABS(void)   /* absolute value of DSP float:  */
                   /*   D:E operand and result      */
{
   asm {   
           TSTE
           BPL   L1
           NEGE     
           BVC   L1
           LSRE  
           ADDD  #1
       L1:
   }
}

/* this operation can be done inline and/or in memory:
           TSTW   (ea)
           BPL    L1 
           NEGW   (ea)
           BVC    L1
           LSRW   (ea)
           INCW   (ea+2)
       L1:
*/ 

/******************************************************************************/

#pragma NO_FRAME

void _HSFLOAT(void) /* signed long to DSP float transformation */
                    /* parameter E:D                           */
                    /* result    E:D                           */
{
   asm {
           TSTE           ; check high word
           BNE   L1
           TSTD           ; check low word
           BEQ   END      ; restult is zero
           BMI   L2
     L3:   TDE            ; E was zero, swap words
           CLRD
           LDX   #16      ; initialize exponent
           BRA   Norm     ; and normalize
     L1:   TSTD           ; is E sign extended D
           BPL   L2       ; no if N flag is set
           CPE   #-1      ; 
           BEQ   L3           
     L2:   LDX   #32      ; initial exponent
     Norm: AIX   #-1      ; normalize mantissa
           LSLD
           ROLE
           BVC   Norm     ;
           RORE
           XGDX           ; load exponent into D
     END:    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HUFLOAT(void) /* unsigned long to DSP float transformation */
                    /* parameter E:D                             */
                    /* result    E:D                             */
{
   asm {
           LDX   #32      ; initial exponent
           TSTE           ; check high word
           BMI   Done     ; msb set; data in D not needed
           BNE   Norm
           TSTD           ; check low word
           BEQ   END      ; restult is zero
           BMI   Norm
           TDE            ; E was zero, swap words
           CLRD
           AIX   #-16     ; adjust exponent
     Norm: AIX   #-1      ; normalize mantissa
           LSLD
           ROLE
           BVC   Norm     ;
     Done: RORE
           XGDX           ; load exponent into D
     END:    
   }
}

/******************************************************************************/

#pragma NO_FRAME

void _HSTRUNC (void)
  /**** IN: D: Exponent, E: Signed mantissa. Corrected version by TW, original
        just shifted, regardless of the mantissa's sign. But if the value was
        -5.7..., i.e. mantissa was 0xA..., the result was 0xFFFA (-6). Correct
        is to shift 5.7 (0x5...) giving 0x0005 and then negating: -5 (0xFFFB). */
{
  asm {
          PSHM   E              ; Save mantissa for sign!
          TSY
          TSTE
          BEQ    ZeroD          ; Result is zero
          TSTD
          BLT    Zero           ; exp < 0 --> result is zero
          TSTE
          BPL    NotNeg
          NEGE                  ; Make mantissa positive
     NotNeg:
          XGDE                  ; E: counter, D: mantissa
          SUBE   #15
          BEQ    PosNeg         ; No shifts needed: E == 0, D == mantissa
          BPL    DoLong         ; More than 15 shifts needed
     ShWord:
          LSRD
          ADDE   #1
          BNE    ShWord
          BRA    PosNeg         ; E == 0, D == mantissa
     DoLong:
          XGDE                  ; E: mantissa, D: counter
          SUBD   #16
          BEQ    PosNeg         ; No shifts needed
          BPL    TooLarge       ; Counter still too large
          NEGD
          XGDX                  ; E|D : Mantissa, X: counter
          CLRD
     ShLong:
          LSRE
          RORD
          AIX    #-1
          BNE    ShLong
     PosNeg:
          BRCLR  0,Y, #0x80, End
          NEGE                  ; Negate the long result!
          NEGD
          SBCE   #0
          BRA    End
     TooLarge:
          BRSET  0,Y, #0x80, TooSmall
          LDD    #0xFFFF        ; Overflow value: MAX(longint)
          LDE    #0x7FFF
          BRA    End
     TooSmall:
          LDE    #0x8000        ; Negative overflow
          BRA    ZeroD
     Zero:
          CLRE
     ZeroD:
          CLRD
     End:
          PULM   X              ; Clean up stack
  }
}

#if 0
               
void _HSTRUNC(void)
{
   asm {
           TSTE
           BEQ   ZroD     ; value is zero
           TSTD
           BLT   Zero
           SUBD  #15
           BPL   L2 
           NEGD
           XGDX           ; value fits in lower 16 bits
           TED            ; move bit to D and
           BMI   L3       ; sign extend D into E
           CLRE
           BRA   L1
     L3:   LDE   #-1
     L1:   ASRD           ; shift 16 bits to the correct position
           AIX   #-1
           BNE   L1
           RTS
           
     L2:   SUBD  #16
           BEQ   ZroD     ; if no shifts to do
           BGE   Zero     ; result does not fit in a unsigned long
           NEGD
           XGDX           ; result will have up to 32 bits
           CLRD
     LOOP: ASRE           ; shift 32 bits to the correct position
           RORD
           AIX   #-1
           BNE   LOOP
           RTS    
                 
     Zero: CLRE      
     ZroD: CLRD
   }
}   

#endif

/******************************************************************************/

#pragma NO_FRAME

void _HUTRUNC(void)
{
   asm {
           TSTE
           BLE   Zero     ; value is less or equal zero => return 0
           TSTD
           BLE   Zero     ; exponent less or equal zero => return 0
           SUBD  #15
           BPL   L2
           NEGD
           XGDX           ; value fits in lower 16 bits
           TED            
           CLRE
     L1:   LSRD           ; shift 16 bits to the correct position
           AIX   #-1
           BNE   L1
           RTS 
           
     L2:   SUBD  #16
           BEQ   L3       ; value > 2^30; no shifts needed
           CPD   #1       
           BEQ   L4       ; value > 2^31;
           BGE   Zero     ; result does not fit in a unsigned long
           NEGD
           XGDX           ; result will have up to 32 bits
           CLRD
     LOOP: LSRE           ; shift 32 bits to the correct position
           RORD
           AIX   #-1
           BNE   LOOP
           RTS
     
     L4:   LSLE      
     L3:   CLRD
           RTS
           
     Zero: CLRE           ; return zero
           CLRD
   }

}

/******************************************************************************
                      CALLING PROCEDURE VARIABLES
                      
  For all routines except _CALLS, the procedure variable is in registers. We
  always have to increment the low word by 2 because the RTS will subtract 2!
    The method is quite tricky: we set up a stack layout as follows:
    
                  RETURN ADDRESS LOW
                  RETURN ADDRESS HIGH
                  Parameter Low + 2
                  Parameter High
    
    The return address is the runtime routine's return address, i.e. it points
  behind the JSR that brought us here. The parameter is the address of the
  function we want to call.
    When we have set up above stack frame, we execute a RTS. This pops the
  parameter from the stack and instead of returning jumps to the beginning of
  the procedure to be called. When this procedure finally executes its own RTS,
  it'll use the original return address and thereby finally return to the point
  where the function pointer call was.
  
  Function _CALLE is used in SMALL memory model only (if both X and Y are used
  by parameters). It uses 0 as the high word of the parameter. This implies that
  in SMALL memory model, the code page be page zero.
    This however is also required by the fact that the vector table has only
  16bit entries and also because the compiler doesn't patch the code page in
  a JSR 0,X!!
 ******************************************************************************/

#pragma NO_FRAME

void _CALLYX (void)         /* parameters are passed in Y:X (4:16)  */
{
   asm {
          AIX   #2        ; adjust address 
          PSHM  X,Y       ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLED(void)         /* parameters are passed in E:D (4:16)  */
{
   asm {
           ADDD  #2       ; adjust address 
           PSHM  D,E      ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLE (void)
{
  asm {
          ADDE  #2
          PSHM  E
          CLRE            ; zero as code page!!
          PSHM  E
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLXE (void)
{
  asm {
          ADDE  #2
          PSHM  E,X
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLYE (void)
{
  asm {
          ADDE #2
          PSHM E,Y
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLS (void)
  /**** Here, the destination address is on the stack,
        above the return address! Therefore, we gotta swap them! */
{
  asm {
          PSHM  D,X,Z     ; Save all registers used!
          TSZ             ; Set up access pointer
          LDD   12,Z      ; Load dest addr (LO)
          LDX   8,Z       ; Load return addr (LO)
          ADDD  #2        ; Correct dest addr
          STD   8,Z       ; Swap the two!
          STX   12,Z
          LDD   10,Z      ; Load dest addr (HI)
          LDX   6,Z       ; Load return addr (HI)
          STD   6,Z       ; Swap the two!
          STX   10,Z
          PULM  D,X,Z     ; Restore all registers!
  }                       /* this return is the procedure call !  */
} /* end _CALLS */
    
#pragma NO_FRAME

void _CALLSS (void)
{
  asm {
          AIS   #-2       ; Create space for duplication of code page
          PSHM  D,X,K     ; Save all registers used
          TSX             ; Set up access pointer
          LDD   8,X       ; Load code page...
          STD   6,X       ; ... and duplicate
          LDD   12,X      ; Load dest addr...
          ADDD  #2        ; ... correct it...
          STD   8,X       ; ... and set up as our return address
          LDD   10,X      ; Load former return address...
          STD   12,X      ; ... and set up as return address of called function
          LDD   6,X       ; Now set code page for the latter
          STD   10,X
          PULM  D,X,K     ; Restore all registers used!
  }                       /* this return is the procedure call! */
} /* end _CALLSS */
    
/******************************************************************************
                                BLOCK MOVES
  
  If we may use only one address register, block moves must be done using
  runtime routines. There are four routines, mirroring all the possible moves
  between I/O space and normal memory. To keep the number of routines that
  small, all pointers (not constant I/O pointers) are considered far pointers.
  If necessary, a near pointer is extended by the default data page.
 ******************************************************************************/

#pragma NO_FRAME

static void _block_move (void)
  /****
    Arguments:
          YK:Y  source address
          XK:X  dest. address
          E     counter, initially block size in bytes - 2
    Precondition:
          E >= 0
   */
{
  asm {
    Loop: LDD   E,Y      ;   Copy words
          STD   E,X
          SUBE  #2       ;   counter -= 2
          BPL   Loop     ; WHILE (counter >= 0)
          PULM  Y,K      ; Restore registers
  } /* end asm */;
} /* end _block_move */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_MEM (void)
  /****
    Arguments:
          Source address : 16 bit I/O address on stack
          Dest. address  : 20 bit far pointer in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TBXK           ; Load page register
          TSY            ; Set up access pointer
          LDY   8,Y      ; Load source address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBYK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _IO_TO_MEM */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_IO (void)
  /****
    Arguments:
          Source address : 20 bit far pointer in B/X
          Dest. address  : 16 bit I/O address on stack
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TXY            ; Move source pointer to Y 
          TBYK           ; Load page register
          TSX            ; Set up access pointer
          LDX   8,X      ; Load dest address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBXK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _MEM_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_IO (void)
  /****
    Arguments:
          Source address : 16 bit I/O address in D
          Dest. address  : 16 bit I/O address in X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K
          XGDY
          TSKB
          DECB
          TBXK
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _IO_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_MEM (void)
  /****
    Arguments:
          Source address : 20 bit address on stack
          Dest. address  : 20 bit address in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K 
          TBXK
          TSY
          LDAB 9,Y
          LDY  10,Y
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _MEM_TO_MEM */

/******************************************************************************
                          MISCELLANEOUS PROCEDURES
 ******************************************************************************/
 
#pragma NO_FRAME

void _YK (void)
  /**** Used to reset YK after runtime call. */
{
  asm {
           PSHM   D, CCR
           TSKB
           TBYK
           PULM   D, CCR
  };        
} /* end _YK */

#pragma NO_FRAME

void _XK (void)
{
  asm {
           PSHM   D, CCR
           TSKB
           TBXK
           PULM   D, CCR
  };
} /* end _XK */

#pragma NO_FRAME

void _IDIV(void)          /* parameters are passed in D / X       */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
           XGDX           
   }
}

#pragma NO_FRAME

void _IMOD(void)          /* parameters are passed in D MOD X     */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
   }
}

/******************************************************************************
  32 bit integral arithmetic
 ******************************************************************************/

/*--------------------------------------------------------------- _LMULU ----*
 * UNSIGNED LONG MULTIPLICATION
 *------------------------------
 * Multiplies the unsigned value in registers E:D with the unsigned at the
 * address 0,Y:2,Y. The result is stored back in registers E:D.
 *
 * The general results of this operation is
 *
 *        +-----------+
 *        |  E  * 0,Y |                 High part          
 *        +-----+-----+-----+
 *    +         |  E  * 2,Y |           Middle 1 part
 *              +-----------+
 *    +         |  D  * 0,Y |           Middle 2 part
 *              +-----+-----+-----+
 *    +               |  D  * 2,Y |     Low part
 *                    +-----------+
 *
 *                    \_ result __/
 *  
 * The result of the multiplication is only le lowest longword of the 64 bits
 * results. In order to have a correct multiplication (without overflow) the
 * highest longword of the result must be 0.
 * The highest longword equal to 0 means that either E = 0 or 0,Y = 0. We can
 * use this fact to make the algorithm faster :
 *
 * if (e == 0) {
 *     return ((D * 0,Y) >> 8) + (D * 2,Y);
 * } else {
 *     return ((E * 2,Y) >> 8) + (D * 2,y);
 * }
 *
 * This algorithm is quite fast because we have only 2 multiplications.
 * 
 * WRONG: for ANSI-C we have to use 32 bit even in case of an                                                                    
 * overflow! So we have the same code as for the signed multiplication.
 * (bh, vc; 1-Mar-93) 
 *
 *---------------------------------------------------------------------------*/
                     
long _LMULU (void)
{
    int dd, t0;
    
    asm {
                STD dd     ; D will be changed after the EMUL, and we need
                           ; it later for calculation the low part. So we
                           ; have to store it.

                LDD 2,Y    ; Compute mid1 part
                EMUL       ; E * 2,Y
                STD t0
                
                LDD dd     ; Compute mid2 part
                LDE 0,Y
                EMUL       ; D * 0,Y
                ADDD t0
                STD t0
                
                LDD dd     ; Compute low part
                LDE 2,Y
                EMUL       ; D * 2,Y
                ADDE t0
    }
}


/**************************************************************** _LMULS *****
 * SIGNED LONG MULTIPLICATION
 *----------------------------
 * Multiplies the signed value in registers E:D with the signed at the
 * address 0,Y:2,Y. The result is stored back in registers E:D.
 *
 * Here we can't use the fact that either E = 0 or 0,Y = 0, so we have to
 * implement the standard algorithm with 3 multiplictions.
 *
 *---------------------------------------------------------------------------*/

long _LMULS (void)

{
    int dd, t0;
    
    asm {
                STD dd     ; D will be changed after the EMUL, and we need
                           ; it later for calculation the low part. So we
                           ; have to store it.

                LDD 2,Y    ; Compute mid1 part
                EMUL       ; E * 2,Y
                STD t0
                
                LDD dd     ; Compute mid2 part
                LDE 0,Y
                EMUL       ; D * 0,Y
                ADDD t0
                STD t0
                
                LDD dd     ; Compute low part
                LDE 2,Y
                EMUL       ; D * 2,Y
                ADDE t0
    }
}

/************************************************************** _LDIVG ******
 * GENERIC LONG DIVISION
 *-----------------------
 * See D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LDIVG ()

{
    unsigned int t0,t1;
    unsigned int ax, ah, al;
    unsigned int bh, bl;
    unsigned int tx, th, tl;
    unsigned int rl;
    unsigned int dd;
    
    asm {
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1
                
        Alg0:               ; 0,Y and E are = 0. Then we can use
                            ; the (fast) IDIV instruction.
                LDX 2,Y   
                IDIV
                XGDX
                CLRE
                BRA end
                
        Alg1:               ; only 0,Y is 0, we can use the algorithm
                            ; explained at exercice 16 of chapter 4.3.1 in
                            ; D. KNUTH

                STD t0      ; Store D in t0
                TED         
                LDX 2,Y
                IDIV        ; E / 2,Y
                STX t1
                
                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; rest : D / 2,X
                XGDX        ; result in E:D
                LDE t1
                BRA end

        Alg2:               ; 0,Y is not equal to 0, so we have to use here
                            ; the complete long division.

                STD al      ; Firste store E:D in ah:al
                LDD 0,Y     ; and 0,Y:2,Y in bh:bl
                STD bh
                LDD 2,Y
                STD bl
                STE ah
                CLRW ax
                BMI calcQ   ; if ah:al >= 2^16, no need to normalize
                
        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register     
                BNE Divide
                LDX #1
                BRA Approx    
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1
                
                LDE al      ; Compute ax:ah:al = ah:al * dd
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax
                
                LDE bl      ; Compute bh:bl = bh:bl * d
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh
                
        calcQ:  LDX #0xFE   ; First estimation of the quotient
                LDE bh
                CPE ax
                BEQ l1      ; if bl = ax then register X = 0xFE
                LDE ax      ; else compute register X = ax:ah / bh
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl      ; store X in rl (q)
        
                            ; Check if the result is correct
                LDE bl      ; Compute tx:th:tl = rl * bh:bl
                LDD rl
                EMUL
                STD tl
                STE th
                
                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx
                
                LDD al      ; Compute tx:th:tl = ax:ah:al - tx:th:tl
                SUBD tl
                STD tl
                
                LDD ah
                SBCD th
                STD th
                
                LDD ax
                SBCD tx
                STD tx
                            ; Here tx:th:tl represents the rest of the
                            ; division (T = A - R * B)
                
                BMI loop    ; if T < 0 then result is too big, goto loop
                
                CLRE        ; Else, result is already correct, clear E and
                LDD rl      ; load the result in D
                
                BRA end

        loop:   DECW rl     ; Result is too big, so decrement it (R = R-1)
        
                            ; If the result is of 1 smaller, then the rest
                            ; (in T) is of B bigger
                            ; T0 = A - R*B
                            ; T1 = A - (R-1)*B
                            ; T1 = A - R*B + B = T0 + B
                            ; --                 ------

                LDD tl
                ADDD bl
                STD tl
                
                LDD th
                ADCD bh
                STD th
                
                LDD tx
                ADCD #0
                STD tx
                
                BMI loop    ; if T is still < 0, loop again
                            ; this branch is very very unlikely to be taken
                
                CLRE        ; The Result is now correct, clear E and
                LDD rl      ; load the result in D
                
        end:    
    }
}

/************************************************************** _LMODG ******
 * GENERIC LONG MODULO
 *---------------------
 * See generic long division and D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LDIVU(void); /* << ES 12/09/96 */

void _LMODG (void) {
    unsigned int t0;
#if 0 /* << ES 12/09/96 */
    unsigned int ax, ah, al;
    unsigned int bh, bl;
    unsigned int tx, th, tl;
    unsigned int rh, rl;
    unsigned int dd;
#else
    unsigned long tmp;
#endif    
    asm {
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1
                
        Alg0:   LDX 2,Y     ; See Generic Long Division
                IDIV
                CLRE        ; The result is already in D, just clear E
                BRA exit
  
        Alg1:   STD t0      ; See Generic Long Division
                TED         
                LDX 2,Y
                IDIV
                
                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; The result is in D, clear E
                CLRE
                BRA exit
#if 1   /* << ES 12/09/96: somehow, the other way it does not work! */
        /* use a%b == 'return (a-((a/b)*b));' */
        Alg2:
                PSHM D,E       ; spill a
                JSR _LDIVU     ; Y points to b, a/b => result in E:D, Y is NOT destroyed!!!!!
                JSR _LMULU     ; Y points to b, (a/b) is in E:D, result of ((a/b)*a in E:D
                STD tmp
                STE tmp:2
                PULM D,E
                SUBD tmp
                SBCE tmp:2
                /* result in E:D, return */
#else    
        Alg2:   STD al
                LDD 0,Y
                STD bh
                LDD 2,Y
                STD bl
                STE ah

                CLR dd      ; dd = 0 means has not been normalized
                CLRW ax
                BMI calcQ
                
        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register     
                BNE Divide
                LDX #1
                BRA Approx    
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1
                
                LDE al
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax
                
                LDE bl
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh
                
        calcQ:  LDX #0xFE
                LDE bh
                CPE ax
                BEQ l1
                LDE ax
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl
        
                LDE bl
                LDD rl
                EMUL
                STD tl
                STE th
                
                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx
                
                LDD al
                SUBD tl
                STD tl
                
                LDD ah
                SBCD th
                STD th
                
                LDD ax
                SBCD tx
                STD tx
                
                BMI loop
                BRA end
                
        loop:   DECW rl
        
                LDD tl
                ADDD bl
                STD tl
                
                LDD th
                ADCD bh
                STD th
                
                LDD tx
                ADCD #0
                STD tx
                
                BMI loop

        end:    TST dd      ; Check if the operation has been normalized
                BNE deno    ; if yes, denormalize

                LDE th      ; If the operatinon has not been normalized
                            ; the result is in th:tl
                LDD tl
                BRA exit

        deno:   LDE tx      ; Else, we have to divide tx:th:tl by dd
                LDD th
                LDX dd
                EDIV
                STX rh
                TDE
                LDD tl
                LDX dd
                EDIV
                XGDX
                LDE rh
#endif
        exit:
    }
}

                    
/************************************************************** _LDIVS ******/

void _LDIVS ()

{
    int sign, ySign;
    
    asm {
                PSHM X
                
                STE sign
                
                TSTE        ; test E
                BPL ePos    ; if e > 0 then ok
                COME        ; else negate E:D
                NEGD
                SBCE #-1
                
        ePos:   CLR ySign   ; Clear ySign
                TST 0,Y     ; test 0,Y
                BPL yPos    ; if 0,Y > 0 then ok
                COM sign    ; Complement sign
                COM ySign   ; Complement sign of y
                XGDX        ; else negate 0,Y:2,Y
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX
                
        yPos:   JSR _LDIVG  
                
        end:    TST sign
                BPL corrY   ; is sign is +, then keep E:D and check if a
                            ; correction of Y is nessessary.
                COME        ; else negate E:D
                NEGD
                SBCE #-1
                
        corrY:  TST ySign
                BEQ exit
                XGDX        ; y has been negated, reverse operation.
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX
        exit:   PULM X
    }
}
                    
/************************************************************** _LDIVU ******/


#pragma NO_FRAME

void _LDIVU ()

{
    asm {
                PSHM X
                JSR _LDIVG  
                PULM X
    }
}

/************************************************************** _LMODS ******/

void _LMODS ()
{
    int sign, ySign;
    
    asm {
                PSHM X      ; See _LDIVS for comments
                
                STE sign    ; the difference with _LDIVS is that here the
                            ; sign of the result is the same as the sign
                            ; of the first argument.
                
                TSTE
                BPL ePos
                COME
                NEGD
                SBCE #-1
                
        ePos:   CLR ySign
                TST 0,Y
                BPL yPos
                COM ySign
                XGDX
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX
                
        yPos:   JSR _LMODG
                
        end:    TST sign
                BPL corrY
                COME
                NEGD
                SBCE #-1
                
        corrY:  TST ySign
                BEQ exit
                XGDX
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX
        exit:   PULM X
    }
}

/************************************************************** _LMODU ******/

#pragma NO_FRAME

void _LMODU (void)
{
    asm {
                PSHM X
                JSR _LMODG
                PULM X
    }
}

/****************************************************************************/

#pragma NO_FRAME

void _LINCPRE (void)
  /**** ++long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                INCW  2,X
                BNE   LoadIt
                INCW  0,X
    LoadIt:     LDD   2,X
                LDE   0,X
  }
}

#pragma NO_FRAME

void _LINCPST (void)
  /**** long++. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD   2,X
                LDE   0,X
                INCW  2,X
                BNE   End
                INCW  0,X
    End:
  }
}

#pragma NO_FRAME

void _LDECPRE (void)
  /**** --long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD  2,X       
                BNE  DecLo     
                DECW 0,X       
    DecLo:      ADDD #-1       
                STD  2,X       
                LDE  0,X       
  }              
}

#pragma NO_FRAME

void _LDECPST (void)
{
  asm {
                LDE   0,X
                LDD   2,X
                BNE   DecLo
                DECW  0,X
    DecLo:      DECW  2,X
  }
}      
    
/****************************************************************************/

#pragma NO_FRAME

void _LCMP (void)
  /**** Signed or unsigned comparison. Called in SMALL and MEDIUM memory models
        if one of the operands is far. (Generating the normal inline comparison
        gives troubles with the PUSH/PULL optimization!
          Arguments: E/D    a
                     Stack  b
          Result:    CCR according to a - b
   */
{
  asm {
            TSX
            SUBD    6,X  /* low part */
            SBCE    4,X  /* high part */
            BNE     End
            /* here, E == 0 */
            TSTD
            BEQ     End /* << ES 10/05/95: if low part is zero, all is ok */
            CLRA        /* but if there is the N flag set, we must clear it! */
            INCA        /* here, N=0, Z=0, V=0, C=0 */
    End:
  } /* end asm */;
} /* end _LCMP */

#pragma NO_FRAME

void _PCMP (void)
  /**** Far pointer comparison. Stack: b, B/E: a, CCR set according to a - b. */
{
  asm {
            TSX
            SUBE    6,X
            SBCB    5,X
            BNE     End
            TSTE
    End:
  } /* end asm */;
} /* end _PCMP */

#pragma NO_FRAME

void  _LSHL (void)
  /**** Long <<. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASLD
            ROLE
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHL */

#pragma NO_FRAME

void  _LSHR (void)
  /**** Long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHR */

#pragma NO_FRAME

void  _LSHRU (void)
  /**** Unsigned long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   LSRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHRU */

/******************************************************************************
  CASE PROCESSOR ROUTINES CALLED IMPLICITLY BY THE COMPILER
 ******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_CHECKED (void)
{
   asm {     
             TDE
             PSHM   K       ; Save old XK and YK registers
             TSY
             LDX    4,Y     ; load table address
             LDAB   3,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             CPE   -2,X     ; compare with table size bound
             BCC    Default ; too big
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD    2,X     ; load offset
    Return:  ADDD   4,Y     ; add return address
             STD    4,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    3,Y
    ThatsIt: PULM   K       ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
    Default: LDD    0,X
             BRA    Return;        
   }                      
}

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_DIRECT (void)
{
   asm {      
             TDE
             PSHM   K       ; Save old XK,YK registers
             TSY
             LDX    4,Y     ; load table address
             LDAB   3,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD   -2,X     ; load offset
    Return:  ADDD   4,Y     ; add return address
             STD    4,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    3,Y
    ThatsIt: PULM   K       ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
   }                     
}

 
/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_SEARCH(void)
{
   asm {
             TDE             ; Move value to look for into E
             PSHM   D,K      ; Save old XK register and reserve space for temp
             TSY
             LDX    6,Y      ; load table address
             LDAB   5,Y      ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0  ; Disable interrupts
#endif
             TBXK
             LDD    -2,X     ; load table size -> D
             XGDE            ; E = table size, D = value to look for
             AIX    #2
    Loop:    ASRE            ; E = (E / 2) & 0xFFFC
             STE    2,Y      ; Save into temp: we'll need it below!!
             ANDE   #0xFFFC  ;
             BEQ    Check    ; if last possible entry -> Check
             CPD    E,X      ; is entry found
             BEQ    Found    ; yes, -> Found
             BLS    Loop     ; no and lower -> Loop
             AEX             ; X = X + E + 4
             AIX    #4       ; +4 because this entry already has been tested
             BRSET  3,Y, #0x02, Loop
             SUBE   #4       ; Correct E, but only if we generated
                             ; an odd size sub-table
             BRA    Loop    
                      
    Check:   CPD    E,X      ; entry found?
             BNE    Default  ; yes, 
    Found:   AEX             ; point to entry in table
             LDD    2,X      ; load offset
    Return:  ADDD   6,Y      ; add return address
             STD    6,Y      ; overwrite return address by destination address
             BCC    ThatsIt
             INC    5,Y
    ThatsIt: PULM   K,D      ; Restore XK and YK registers and remove temp space
             RTI             ; jumps to case label and restores interrupt mask
             
    Default: LDX    6,Y      ; load table address
             LDD    0,X      ; load default offset
             BRA    Return         
   }
}

/******************************************************************************
  IEEE 32 bit arithmetic
 ******************************************************************************/

/**** IEEE 32 bit multiplication. Arguments:

        x   in E:D
        &y  in Y

        result x * y (E:D * @Y) is returned in E:D
 */

void _FMUL (void)
{
    unsigned long par1;
    int           newExp, carry;
    int           result[4];
    
    asm {
    
                STE par1:0          ; Save x
                STD par1:2
        
        ;----- Compute the exponent of the result
        
                ANDE #0x7F80        ; Extract first exponent
                BEQ zero
        
                LDD 0,Y             ; Extract second exponent
                ANDD #0x7F80
                BEQ zero
        
                ADE                 ; Add the 2 exponents
                SUBE #0x3F80        ; Substract Bias
                        
        ;----- Compute the sign of the result
        
                LDD par1
                EORD 0,Y
                ANDD #0x8000        ; Mask the sign bit
        
                ADE
                STE newExp          ; newExp is now the exponent of the result
        
        ;----- Multiply the 2 mantissa and store the result in <result>
        
                LDE par1:2          ; par1:2 * 2,Y
                LDD 2,Y
                EMUL
                STD result:6
                STE carry
        
                LDE par1:2          ; par1:2 * 0,Y
                LDD 0,Y
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                STD result:4
                STE result:2
        
                LDE par1:0          ; par1:0 * 2,Y
                ANDE #0x007F
                ORE  #0x0080
                LDD 2,Y
                EMUL
                ADDD result:4
                ADCE #0
                STD result:4
                STE carry
                
                LDE par1:0          ; par1:0 * 0,Y
                ANDE #0x007F
                ORE  #0x0080
                LDD 0,Y
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                ADDD result:2
                ADCE #0
                STD result:2
                STE result:0
        
        ;----- Normalise the mantissa
        
                BRSET result:2, #0x80, noDen
                LSLW result:4
               ; LSLW result:2     bug 1177  md 22.5.95
               ; LSLW result:0
                ROLW result:2   ;md 22.5.95
                ROLW result:0   ;md 22.5.95
                LDD result:3
                LDE result:1
                ANDE #0x007F
                ADDE newExp
                BRA exit
                
        noDen:  LDD result:3
                LDE result:1
                ANDE #0x007F
                ADDE newExp
                ADDE #0x0080
                BRA exit
        
        
        zero:   CLRD
                CLRE
                
        exit:
    }
}

/******************************************************************************
  IEEE 32 bit division. Arguments:

    x   in E:D
    &y  in Y

    result x / y (E:D / @Y) is returned in E:D
 */
 
void _FDIV (void)
{

    unsigned long par1;
    int           newExp;
    
    unsigned int  u[5];
    unsigned int  v[2];
    unsigned int  r[3];
    int           q, i, j;
    int           borrow, rest;    
    
    asm {
                STE par1:0
                STD par1:2

        ; Compute the new exponent                                
                                    
                LDD 0,Y             ; load first word of second parameter
                ANDD #0x7F80        ; mask exponent
                BEQ div0            ; if zero, then division by zero error

                                    ; E is first word of first parameter
                ANDE #0x7F80        ; mask exponent
                BEQ zero            ; if zero, then result is zero
                
                SDE                 ; substract exponents
                ADDE #0x3F80        ; add bias

                LDD par1            ; load first word of first parameter
                EORD 0,Y            ; xor with the 1 st word of the 
                                    ; 2 nd param.
                ANDD #0x8000        ; mask the sign bit
                
                ADE                 ; add the sign to the new exponent
                STE newExp          ; newExp is now the exponent of 
                                    ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                CLRW u:8
                CLRW u:6
            
                LDAA par1:3
                CLRB
                STD u:4
                
                LDE par1:1
                ORE #0x8000
                STE u:2
                
                CLRW u:0
                    
        
        ; Compute the normalized fractional part of the second parameter        
        ; and store it in v
                    
                LDAA 3,Y
                CLRB                   
                STD v:2
                 
                LDE 1,Y
                ORE #0x8000
                STE v:0

        ; Compute the quotient of the mantissa.
                
                CLRW j              ; counter = 0
                TZY
                AIY @u              ; Y points to u[j = 0]
                BRA loop

        mainLoop:
                TZY
                AIY @u              ; Y points to u[0]
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to u[j]

        ; Calculate the first estimation of q

        loop:   LDE 0,Y             ; E = u[j]
                CPE v:0             ; IF u[j] == v[1] THEN
                BNE l1              ;
                LDX #0xFFFF         ;   X := b-1
                STX q
                    
                LDD 0,Y             ;   rest = u[j] + u[j+1]          
                ADDD 2,Y
                    
                BCS substr
                STD rest
                BRA fastCheck
                    
        l1:     LDD 2,Y             ; ELSE  D = u[j+1]
                LDX v:0
                EDIV                ;   X = u[j]:u[j+1] / v[1]
             
        l2:     STX q
                STD rest            ;   rest

        ; Fast Check to see if q is too big

        fastCheck:
                LDE v:2             ; E = v[2]
                LDD q               ; D = q
                EMUL
                    
                CPE rest
                BHI decq
                BNE substr
                    
                CPD 4,Y
                BLS substr
                    
        decq:   DECW q              ; Decrement q
                LDD rest            ; Adjust rest
                ADDD v:0
                BCS substr
                STD rest
                BRA fastCheck

        ; substract q * v from u

        substr: CLRW borrow         ; Start with no borrow

                TZX
                AIX @v:2            ; X points to v[n]
                TZY
                AIY @u:4
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to u[j+n]

                LDE #1
                STE i               ; i = 1..0

        sl1:    LDE 0,X
                LDD q
                EMUL                ; E:D = q * v[n]

                ADDD borrow
                ADCE #0             ; E:D = q * v[n] + borrow

                STE borrow
                LDE 0,Y             ; E = u[j+n]
                SDE                 ; E = E - D
                STE 0,Y             ; Store new u[j+n]
                    
                BCC noB
                INCW borrow         ; correct borrow if necessary

        noB:    AIX #-2
                AIY #-2
                DECW i
                BPL sl1

                LDE 0,Y
                SUBE borrow
                STE 0,Y
                    
                BCC ok

        ; q was too big, we have to add v back to u

                TZX
                AIX @v              ; X points to v[1]

                TZY
                AIY @u
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to u[j]
                    
                LDD 2,X
                ADDD 2,Y
                STD 2,Y
                  
                LDD 0,X
                ADCD 0,Y
                STD 0,Y
                  
                DECW q

        ok:     TZY
                AIY @r
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to r[j]

                LDE q
                STE 0,Y             ; store q in r[j]

                INCW j
                LDE j
                CPE #2
                BLE mainLoop
                
        ; denormalize and move the result in par1. if necessary, correct
        ; the exponent of the result.
        
                TSTW r
                BNE dnl1
                LDE r:1
                ANDE #0x007F
                ADDE newExp
                SUBE #0x0080
                LDD r:3
                BRA end
                    
        dnl1:   
                LDE r:1
                LDD r:3
                LSRE
                RORD
                ANDE #0x007F
                ADDE newExp
                BRA end
                 
        div0:   LDX #0             ; Simulate a division by 0
                EDIV
                        
        zero:   CLRE               ; result is zero
                CLRD
                
        end:

    }
}

/******************************************************************************
  IEEE 32 bit addition. Arguments:

    x   in E:D
    &y  in Y

    result x + y (E:D + @Y) is returned in E:D
 */
 
void _FADD (void)
{
    unsigned long par1,  par2;
    int           newExp;    
    char          sign1, sign2, resSign;
    
    asm {
                CLR   resSign
                STE   par1:0        ; Save par1
                STD   par1:2
                ANDE  #0x7F80       ; Mask Exponent
                BEQ   uvfl          ; x == 0, return par2 (y)
                LDD   2,Y
                STD   par2:2
                LDD   0,Y
                STD   par2:0
                ANDD  #0x7F80
                BNE   not0          ; y == 0, return par1 (x)
                LDE   par1:0
                LDD   par1:2
                BRA   end
        not0:   SDE                 ; compute exp1 - exp2
                ASLE                ; carry set means D negativ
                TED
                XGAB
                TDE                 ; E = # of bits to shift
                TZX                 ; initialise X and Y pointers
                TZY
                BCC   shP2             
        shP1:   AIX   @par1         ; exp1 < exp2 => shift par1
                AIY   @par2         ; and compute par2 + par1
                NEGE
                ANDE  #0x00FF
                BRA   cont
        shP2:   AIX   @par2         ; exp1 > exp2 => shift par2
                AIY   @par1         ; and compute par1 + par2
        cont:   CPE   #24
                BLT   noUVFL
        uvfl:   LDE   0,Y           ; We'd shift @X completely away
                LDD   2,Y
                BRA   end                
        noUVFL: LDAA  0,X
                STAA  sign2
                LDD   0,Y
                STAA  sign1
                ANDD  #0x7F80
                STD   newExp
                CLR   par1
                BSET  par1:1,#0x80  ; Hidden bit
                CLR   par2
                BSET  par2:1,#0x80  ; Hidden bit  

        ; shift the mantissa pointed to by X right by E bits

        chk16:  CPE   #16           ; Try to shift a bloc of 16 bits
                BLT   chk8
        sh16:   LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #16
        chk8:   CPE   #8            ; Try to shift a bloc of 8 bits
                BLT   chk1           
        sh8:    LDD   1,X
                STD   2,X
                CLR   1,X
                SUBE  #8
                BRA   chk1
        sh1:    LSR   1,X        
                RORW  2,X
        chk1:   SUBE  #1
                BGE   sh1
        madd:   BRCLR sign2,#0x80,xPos
                LDD   #0             ; if @X is negative...
                SUBD  2,X            ; ...negate the mantissa
                STD   2,X
                LDD   #0
                SBCD  0,X
                STD   0,X
        xPos:   BRCLR sign1,#0x80,yPos
                LDD   #0             ; if @Y is negative...
                SUBD  2,Y            ; ...negate the mantissa
                STD   2,Y
                LDD   #0
                SBCD  0,Y
                STD   0,Y
        yPos:   LDD   2,X            ; Add them!
                ADDD  2,Y
                STD   par1:2
                LDD   0,X
                ADCD  0,Y
                STD   par1:0
                BPL   normal
                LDD   #0             ; Resulting mantissa is negative
                SUBD  par1:2
                STD   par1:2
                LDD   #0
                SBCD  par1:0
                STD   par1:0
                COM   resSign        ; Result is negative!!
        normal: LDE   newExp         
                BRCLR par1,#0x01,subN
                LSRW  par1:0         ; Overflow: shift right by 1...
                RORW  par1:2
                ADDE  #0x80          ; ... and increment exponent
        subN:   BRSET par1:1, #0x80, pack
                LDD   par1           ; De-normalized: test for zero
                BNE   try1
                LDD   par1:2
                BNE   mov16
                TDE                  ; Result is zero
                BRA   end
        mov16:  LDAA  par1:2 
                BNE   mov8
                LDD   par1:2         ; hi 3 bytes are 0
                STD   par1:0
                CLRW  par1:2
                SUBE  #0x800
                BRA   try1
        mov8:   LDD   par1:2         ; only hi 2 bytes are 0
                STD   par1:1
                CLR   par1:3
                SUBE  #0x400
                BRA   try1                        
        mov1:   LSLW  par1:2         ; Shift until normalized
                ROL   par1:1
                SUBE  #0x80
        try1:   BRCLR par1:1,#0x80,mov1
        pack:   BCLR  par1:1,#0x80
                LDD   par1:2
                ORE   par1
                BRCLR resSign,#0x80, end
                ORE   #0x8000
        end:
    }
}


/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in Y

    result x - y (E:D - @Y) is returned in E:D

    Note: @Y mustn't be changed, therefore y is copied (and the sign bit
          inverted).
 */
 
void _FSUB (void)
{
    unsigned long par1;
    
    asm {
                XGDX
                LDD 0,Y
                EORD #0x8000
                STD par1
                LDD 2,Y
                STD par1:2
                TZY
                AIY @par1
                XGDX
                JSR _FADD
    }
}

/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in Y

    result x - y (E:D - @Y) is returned in E:D

    Note: @Y may be changed, therefore the sign bit is inverted in place.
 */

#pragma NO_EXIT
 
void _FSUB0 (void)
{
  asm {
                XGDX
                LDD  0,Y
                EORA #0x80
                STD  0,Y
                XGDX
                PULM Z
                JMP  _FADD
  }
}

/******************************************************************************
  Auxiliary routine: right shift (B, E, #0:8) by 31-A bits and return result
  in E:D
 */

#define FLT_BIAS      127

void _frshift (void)
{
  unsigned long l;

  asm {
                NEGA                    ;   A := 31 - A
                ADDA   #31
                CMPA   #24      
                BCS    Smaller24        ;   IF (A >= 24) THEN
                STAB   l:3              ;     l := (mantissa >> 24);
                CLRW   l
                CLR    l:2
                SUBA   #24              ;     DEC (A, 24);
                BRA    Smaller8         ;   ELSE
    Smaller24:  STAB   l                ;     l := left adjusted mantissa
                STE    l:1
                CLR    l:3
                CMPA   #16
                BCS    Smaller16        ;     IF (A >= 16) THEN
                LDE    l                ;       l.lo := l.hi
                STE    l:2
                CLRW   l                ;       l.hi := 0;
                SUBA   #16              ;       DEC (A, 16);
                BRA    Smaller8
    Smaller16:  CMPA   #8
                BCS    Smaller8         ;     ELSIF (A >= 8) THEN
                LDAB   l:2              ;       Move l right 8 bits by swapping bytes
                STAB   l:3
                LDE    l
                STE    l:1
                CLR    l
                SUBA   #8               ;       DEC (A, 8);
                                        ;     END;
    Smaller8:   LDE    l                ;   END;
                STAA   l
                LDD    l:2   
                TST    l
                BEQ    EndLoop          ;   IF (count > 0) THEN
                                        ;     /* Note: {count < 8} */
    Loop:       LSRE                    ;     REPEAT
                RORD                    ;       result := result >> 1
                DEC    l                ;       DEC (count);
                BNE    Loop             ;     UNTIL (count = 0);
                                        ;   END;
    EndLoop:
  } /* end asm */
} /* end _frshift */

/******************************************************************************
  IEEE-32 to LONG conversion                                   TW, 03/01/93
    E:D    float                                
    E:D    result
 */

void _FSTRUNC (void)
{
  unsigned char  i;

  asm {
                XGDE                    ; Move hi word to D (easier to unpack exp)
                STAA   i                ; Save sign bit
                LSLD                    ; Exp in A
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
                CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #31              ; ELSIF (exp >= 31) THEN
                BCS    Ok
                LDE    #0x8000          ;   IF (f < 0.0) THEN
                CLRD                    ;     RETURN MIN (LONGINT);
                BRSET  i, #0x80, End    ;   ELSE
                COME                    ;     RETURN MAX (LONGINT);
                COMD                    ;   END;
                BRA    End              ; ELSE
    Ok:         JSR    _frshift
                BRCLR  i, #0x80, End    ;   IF (f < 0.0) THEN
                NEGE                    ;     result := -result;
                NEGD
                SBCE   #0               ;   END;
    End:                                ; END            
  } /* end asm */;
} /* end FSTRUNC */ 

/******************************************************************************
  IEEE-32 to UNSIGNED LONG conversion                          TW, 03/01/93
    E:D    float                                
    E:D    result
 */

void _FUTRUNC (void)
{
  asm {
                XGDE                    ; Move hi word to D (easier to unpack exp)
                LSLD                    ; Exp in A
                BCS    RetZero          ; IF (f < 0.0) THEN RETURN 0
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
    RetZero:    CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #32              ; ELSIF (exp >= 32) THEN
                BCS    Ok
                LDD    #0xFFFF          ;   RETURN MAX (LONGCARD);
                TDE
                BRA    End              ; END
    Ok:         JSR    _frshift 
    End:
  } /* end asm */;
} /* end FUTRUNC */ 


/******************************************************************************
  Auxiliary routine: shift (E, D) until bit #15 of E is 1. Also decrement X
  for each bit shifted. Then, pack it together and return result in D/E!
 */

#define F_EXP_MAX     158

void _fnormant (void)
{
  unsigned long l;

  asm {
                LDX   #F_EXP_MAX
                TSTE
                BMI   Pack
                BNE   Shift
                AIX   #-16              ; Optimize for shift counts > 16
                TDE
                CLRD
                TSTE
                BMI   Pack
    Shift:      AIX   #-1               ; Shift until negative (normalized)
                LSLD
                ROLE
                BPL   Shift
    Pack:       STE   l
                STAA  l:2
                BCLR  l, #0x80          ; Clear hidden bit
                XGDX                    ; A = undef,   B = exp
                XGAB                    ; A = exp,     B = undef
                CLRB                    ; A = exp,     B = 0
                LSRD                    ; A = 0/exp:7, B = exp:1/0:7
                ORAB  l                 ; A = 0/exp:7, B = exp:1/mant:7 ==> D = res.hi
                LDE   l:1               ; E = res.lo
  } /* end asm */;
} /* end _fnormant */

/******************************************************************************
  LONG to IEEE-32                                              TW, 03/01/93
    E:D    long                                
    E:D    result
 */

void _FSFLOAT (void)
{
  unsigned int  i;

  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    STE   i                 ; Set negative flag
                BPL   Pos
                NEGE                    ; Negate mantissa
                NEGD
                SBCE  #0
    Pos:        JSR   _fnormant
                BRCLR i, #0x80, Swap    ; IF (long < 0) THEN
                ORAA  #0x80             ;   Set sign bit
    Swap:       XGDE                    ; END
    End:
  } /* end asm */;
} /* end _FSFLOAT */


/******************************************************************************
  UNSIGNED LONG to IEEE-32                                     TW, 03/01/93
    E:D    long                                
    E:D    result
 */

void _FUFLOAT (void)
{
  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    JSR   _fnormant
                XGDE
    End:
  } /* end asm */;
} /* end _FUFLOAT */

/******************************************************************************
    E:D   x
    @Y    y

    Set flags according to x-y
 */

void _FCMP (void)
{
  asm {
                    XGDX
                    TED
                    EORD  0, Y
                    BMI   InverseComp     /*  different signs */
    SameSign:       TSTE
                    BPL   Pos
                    /* Both are negative: compare the other way 'round */
                    /* Or numbers have different signs */
    InverseComp:    ;
                    LDD   0, Y
                    XGDE
                    SDE
                    BNE   End
                    XGDX
                    LDE   2, Y
                    SDE
                    BRA   End
                    /* Both are positive */
    Pos:            CPE   0, Y
                    BNE   End
                    CPX   2, Y
    End:            
  } /* end asm */;
} /* end _FCMP */


/******************************************************************************
  IEEE 64 bit arithmetic
 ******************************************************************************/

typedef struct {
  unsigned int hh, hl, lh, ll;
} DOUBLE;

/******************************************************************************
  IEEE 64 bit addition. Arguments are on the stack; the result (par1 + par2) is
  returned in par1 (i.e. the stack is overwritten).
 */

void _DADD (DOUBLE par1, DOUBLE par2)

{
    int  newExp;
    char sign1, sign2, resSign;
    
    asm {     
                CLR   resSign             ; result is assumed to be positive
                LDE   par1                ; load first word of first parmeter
                ANDE  #0x7FF0             ; mask exponent
                BNE   not0
                TZY                       ; par1 = 0, so copy par2 in par1
                AIY   @par2
                BRA   uvfl
        not0:   LDD   par2.hh             ; load first word of second parameter
                ANDD  #0x7FF0             ; mask exponent
                BEQ   end                 ; if zero, return par1 as result
                SDE                       ; compute exp1 - exp2
                TZX                       ; prepare X and Y
                TZY
                BPL   shP2             
        shP1:   AIX   @par1               ; exp1 < exp2 => shift par1
                AIY   @par2               ; and compute par2 + par1
                NEGE     
                BRA   cont
                
        shP2:   AIX   @par2               ; exp1 > exp2 => shift par2
                AIY   @par1               ; and compute par1 + par2
                
        cont:   CPE   #0x350
                BLT   noUVFL
        uvfl:   LDD   0,Y                 ; We'd shift @X completely away...
                STD   par1.hh             ; ... return @Y
                LDD   2,Y
                STD   par1.hl
                LDD   4,Y
                STD   par1.lh
                LDD   6,Y
                STD   par1.ll
                BRA   end
        noUVFL: LDAA  0,X
                STAA  sign2               ; Store sign bit
                LDD   0,Y
                STAA  sign1               ; Store sign bit
                ANDD  #0x7FF0
                STD   newExp              ; Store new exponent
                BCLRW par1,#0xFFF0
                BSET  par1:1,#0x10        ; Hidden bit
                BCLRW par2,#0xFFF0
                BSET  par2:1,#0x10        ; Hidden bit
                
        ; shift the mantissa pointed to by X right by E bits
        
                BRA   chk16
        sh16:   LDD   4,X
                STD   6,X
                LDD   2,X
                STD   4,X
                LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #0x100              ; E -= 16
        chk16:  CPE   #0x100              ; E >= 16 ??
                BGE   sh16
        chk8:   CPE   #0x80               ; E >= 8
                BLT   chk1
        sh8:    LDD   5,X
                STD   6,X
                LDD   3,X
                STD   4,X
                LDD   1,X
                STD   2,X
                CLRW  0,X
                SUBE  #0x80               ; E -= 8
                BRA   chk1
        sh1:    LSRW  0,X        
                RORW  2,X
                RORW  4,X
                RORW  6,X
        chk1:   SUBE  #0x10               ; E -= 1
                BGE   sh1
                
        madd:   BRCLR sign2,#0x80,xPos
                CLRD                      ; Negate @X
                CLRE
                SUBD  6,X
                STD   6,X
                TED
                SBCD  4,X
                STD   4,X
                TED
                SBCD  2,X
                STD   2,X
                TED
                SBCD  0,X
                STD   0,X
                
        xPos:   BRCLR sign1,#0x80,yPos
                CLRD                      ; Negate @Y
                CLRE
                SUBD  6,Y
                STD   6,Y
                TED
                SBCD  4,Y
                STD   4,Y
                TED
                SBCD  2,Y
                STD   2,Y
                TED
                SBCD  0,Y
                STD   0,Y
                
        yPos:   LDD   6,X                 ; Add them
                ADDD  6,Y
                STD   par1.ll
                LDD   4,X
                ADCD  4,Y
                STD   par1.lh
                LDD   2,X
                ADCD  2,Y
                STD   par1.hl
                LDD   0,X
                ADCD  0,Y
                STD   par1.hh
                BPL   normal
                CLRD                      ; Result is negative: negate and set flag
                CLRE
                SUBD  par1.ll
                STD   par1.ll
                TED
                SBCD  par1.lh
                STD   par1.lh
                TED
                SBCD  par1.hl
                STD   par1.hl
                TED
                SBCD  par1.hh
                STD   par1.hh
                COM   resSign             ; Result is negative!!
                
        normal: LDE   newExp               
                BRCLR par1:1,#0x20,subN
                LSRW  par1.hh             ; Overflow: shift right by 1
                RORW  par1.hl
                RORW  par1.lh
                RORW  par1.ll
                ADDE  #0x10               ; and correct exponent
        subN:   BRSET par1:1, #0x10, pack ; De-normalized: normalize
                LDD   par1.hh             ; Test for zero
                BNE   shN
                LDD   par1.hl
                BNE   shN
                LDD   par1.lh
                BNE   mov16
                LDD   par1.ll
                BEQ   end                 ; Return zero
        mov32:  LDD   par1.ll             ; HH, HL and LH words are zero
                STD   par1.hl
                CLRW  par1.lh
                CLRW  par1.ll
                SUBE  #0x200              ; exp -= 32
                BRA   shN
        mov16:  LDD   par1.lh             ; HH and HL words are zero
                STD   par1.hl
                LDD   par1.ll
                STD   par1.lh
                CLRW  par1.ll
                SUBE  #0x100              ; exp -= 16
        shN:    BRSET par1:1,#0x10,pack
                LSLW  par1.ll
                ROLW  par1.lh
                ROLW  par1.hl
                ROL   par1:1
                SUBE  #0x10               ; exp -= 1
                BRA   shN      
        pack:   BCLR  par1:1,#0x10        ; Hidden bit
                ORE   par1.hh
                BRCLR resSign,#0x80, pos
                ORE   #0x8000 
        pos:    STE   par1.hh
        end:
    } /* end asm */;
} /* end _DADD */

#pragma NO_EXIT

void _DSUB (DOUBLE par1, DOUBLE par2)
  /**** Calculate par2 - par1 and return the result in par1 */
{
  asm { 
                LDAA  par1                ; Negate sign
                EORA  #0x80
                STAA  par1
                PULM  Z                   ; Correct stack...
                JMP   _DADD               ; ... and add!
  } /* end asm */;
} /* end _DSUB */

/******************************************************************************
  IEEE 64 bit multiplication. Arguments are on the stack; the result (par1 * par2)
  is returned in par1 (i.e. the stack is overwritten).
 */

void _DMUL (DOUBLE par1, DOUBLE par2)

{
    int          counter1, counter2;
    int          carry, temp;
    unsigned int result[8];
    int          newExp;
    
    asm { 

        ; Compute the new exponent
                
                LDD par1           ; load first word of first parameter
                ANDD #0x7FF0       ; mask exponent
                BEQ zero           ; if zero, then result is zero
                
                LDE par2           ; load first word of second parameter
                ANDE #0x7FF0       ; mask exponent
                BEQ zero           ; if zero, then result is zero
                
                ADE                ; add the 2 exponents
                SUBE #0x3FF0       ; substract bias
                
                LDD par1           ; load first word of first parameter
                EORD par2          ; xor with the 1 st word of the 2 nd param.
                ANDD #0x8000       ; mask the sign bit
                
                ADE                ; add the sign to the new exponent
                STE newExp         ; newExp is now the exponent of the result

        ; Extract mantissa of par1
               
                LDE par1
                ANDE #0x000F
                ORE  #0x0010
                STE par1
                
        ; Extract mantissa of par2
        
                LDE par2
                ANDE #0x000F
                ORE  #0x0010
                STE par2
                
        ; First Multiply par2 with the last word of par1 and store the
        ; result in the array result.
        
                CLRW carry         ; carry is initialise to 0
                
                LDD par1:6
                STD temp           ; temp := last word of the mantissa of par1
                
                TZY
                AIY @result:14     ; Y points to where the result must be written

                TZX
                AIX @par2:6        ; X points to the last word of the mantissa of par2
                LDD #3             ; iterate 4 times (3..0)
                STD counter2

        lab0:   LDE temp           ; begin of the loop
                LDD 0,X
                EMUL               ; Multiply temp with a word of par2
                ADDD carry         ; Add carry to low word of result
                ADCE #0            ; Adjust high word of result if the last
                                   ; operation produces a carry

                STD 0,Y            ; store low word of result
                STE carry          ; store high word in the carry
                AIX #-2            ; decrement pointer X by 2
                AIY #-2            ; decrement pointer Y by 2
                DECW counter2      ; DEC(counter2)
                BPL lab0           ; end of loop

                LDD carry
                STD 0,Y            ; Store carry

        ; Then multiply par2 with the other 3 words of par1 and adjust the
        ; result
                
                LDD #4             ; iterate 3 times (3,2,0)
                STD counter1

        lab1:   CLRW carry         ; begin the outer loop with carry = 0
                
                TZX
                AIX @par1          ; X points to the begin of par1
                                   ; XK already correct!
                LDE counter1
                LDD E,X
                STD temp           ; temp := word of par1 with which we 
                                   ; will multiply par2
                TZY                   
                AIY @result:8
                LDD counter1
                ADY                ; Y points where the result must be written
                
                TZX
                AIX @par2:6        ; X point to the last word of par2
                                   ; XK already correct!
                LDD #3             ; iterate 4 times (3..0)
                STD counter2

        lab2:   LDE temp           ; begin of the loop
                LDD 0,X
                EMUL               ; multiply temp with a word of par2
                ADDD carry         ; add carry to low word of result
                ADCE #0            ; Adjust high word of result if the last
                                   ; operation produces a carry

                ADDD 0,Y           ; add the low word with the last result
                STD 0,Y            ; store the noew word of the result
                ADCE #0            ; adjust high word if carry
                STE carry          ; store high word in carry

                AIX #-2            ; decrement pointer X by 2
                AIY #-2            ; decrement pointer Y by 2
                DECW counter2      ; DEC(counter2)
                BPL lab2           ; end of inner loop

                LDD carry
                STD 0,Y            ; store carry
                
                DECW counter1
                DECW counter1      ; decrement counter1 by 2
                BPL lab1           ; end of outer loop

        ; align the mantissa of the result
                
                BRCLR result:2,#0x02,normal
                
                LDD newExp         ; If the bit 106 of the result if set
                ADDD #0x0010       ; then increment the exponent
                STD newExp
                
                LSRW result:2      ; and shift the result
                RORW result:4
                RORW result:6
                RORW result:8

        normal: LDD #3             ; shift the result 4 times (3..0)
        adjust: LSRW result:2
                RORW result:4
                RORW result:6
                RORW result:8
                ADDD #-1
                BPL adjust
                
                BCLRW result:2,#0xFFF0  ; clear implicit 1

        ; copy result in par1
                
                LDD result:2
                ADDD newExp         ; add sign and exponent
                STD par1:0
                LDD result:4
                STD par1:2
                LDD result:6
                STD par1:4
                LDD result:8
                STD par1:6
                BRA end

        ; copy zero in par1
                
        zero:   CLRD
                STD par1:0
                STD par1:2
                STD par1:4
                STD par1:6

        end:
    }
}

/******************************************************************************
  IEEE 64 bit divison. Arguments are on the stack; the result (par1 / par2)
  is returned in par2 (i.e. the stack is overwritten).
 */

void _DDIV (DOUBLE par2, DOUBLE par1)

{ 
    int newExp;
    
    unsigned int u[9];
    unsigned int v[4];
    unsigned int r[5];
    int q, i, j;
    int borrow, rest;    
    
    asm {
    
        ; Compute the new exponent                                

                    LDD par2           ; load first word of second parameter
                    ANDD #0x7FF0       ; mask exponent
                    BEQ div0           ; if zero, then division by zero error

                    LDE par1           ; load first word of first parameter
                    ANDE #0x7FF0       ; mask exponent
                    BEQ zero           ; if zero, then result is zero
                
                    SDE                ; substract exponents
                    ADDE #0x3FF0       ; add bias

                    LDD par1           ; load first word of first parameter
                    EORD par2          ; xor with the 1 st word of the
                                       ; 2 nd param.
                    ANDD #0x8000       ; mask the sign bit
                
                    ADE                ; add the sign to the new exponent
                    STE newExp         ; newExp is now the exponent of 
                                       ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                    CLRD
                    STD u:16
                    STD u:14
                    STD u:12
                    STD u:10
                    STAA u:9
                
                    LDAA par1:7
                    LSLA
                    STAA u:8
                
                    LDE par1:5
                    ROLE
                    STE u:6
                
                    LDE par1:3
                    ROLE
                    STE u:4
                    
                    LDE par1:1
                    ANDE #0x0FFF       ; remove the sign and the exponent
                    ORE #0x1000        ; add the implicit 1
                    ROLE
                    STE u:2               
                
                    CLRW u:0           ; was CLR u:0 !!!!!
                
                    LSLW u:8
                    ROLW u:6
                    ROLW u:4
                    ROLW u:2
                
                    LSLW u:8
                    ROLW u:6
                    ROLW u:4
                    ROLW u:2
        
        ; Compute the normalized fractional part of the second parameter        
        ; and store it in v
        
                    CLR v:7
                    
                    LDAA par2:7
                    LSLA
                    STAA v:6
                 
                    LDE par2:5
                    ROLE
                    STE v:4
                
                    LDE par2:3
                    ROLE
                    STE v:2
                
                    LDE par2:1
                    ANDE #0x0FFF       ; remove sign and exponent 
                    ORE #0x1000        ; add implicit 1
                    ROLE
                    STE v:0
                
                    LSLW v:6
                    ROLW v:4
                    ROLW v:2
                    ROLW v:0
                
                    LSLW v:6
                    ROLW v:4
                    ROLW v:2
                    ROLW v:0


        ; Compute the quotient of the mantissa.
                
                    CLRW j              ; counter = 0
                    
                    TZY
                    AIY @u              ; Y points to u[j = 0]
                    BRA loop

        mainLoop:   TZY
                    AIY @u              ; Y points to u[0]
                    XGEY
                    LDD j
                    ASLD
                    ADE
                    XGEY                ; Y points to u[j]

        ; Calculate the first estimation of q

        loop:       LDE 0,Y             ; E = u[j]
                    CPE v:0             ; IF u[j] == v[1] THEN
                    BNE l1              ;
                    LDX #0xFFFF         ;   X := b-1
                    STX q
                    
                    LDD 0,Y             ;   rest = u[j] + u[j+1]          
                    ADDD 2,Y
                    
                    BCS substr
                    STD rest
                    BRA fastCheck
                    
        l1:         LDD 2,Y             ; ELSE  D = u[j+1]
                    LDX v:0
                    EDIV                ;   X = u[j]:u[j+1] / v[1]

        l2:         STX q
                    STD rest

        ; Fast Check to see if q is too big

        fastCheck:  LDE v:2             ; E = v[2]
                    LDD q               ; D = q
                    EMUL
                    
                    CPE rest
                    BHI decq
                    BNE substr
                    
                    CPD 4,Y
                    BLS substr
                    
        decq:       DECW q              ; Decrement q
                    LDD rest            ; Adjust rest
                    ADDD v:0
                    BCS substr
                    STD rest
                    BRA fastCheck

        ; substract q * v from u

        substr:     CLRW borrow         ; Start with no borrow

                    TZX
                    AIX @v:6            ; X points to v[n]

                    TZY
                    AIY @u:8
                    XGEY
                    LDD j
                    ASLD
                    ADE
                    XGEY                ; Y points to u[j+n]

                    LDE #3
                    STE i               ; i = 3..0

        sl1:        LDE 0,X
                    LDD q
                    EMUL                ; E:D = q * v[n]

                    ADDD borrow
                    ADCE #0             ; E:D = q * v[n] + borrow

                    STE borrow
                    LDE 0,Y             ; E = u[j+n]
                    SDE                 ; E = E - D
                    STE 0,Y             ; Store new u[j+n]
                    
                    BCC noB
                    INCW borrow         ; correct borrow if necessary

        noB:        AIX #-2
                    AIY #-2
                    DECW i
                    BPL sl1

                    LDE 0,Y
                    SUBE borrow
                    STE 0,Y
                    
                    BCC ok

        ; q was too big, we have to add v back to u

                    TZX
                    AIX @v              ; X points to v[1]

                    TZY
                    AIY @u
                    XGEY
                    LDD j
                    ASLD
                    ADE
                    XGEY                ; Y points to u[j]

                    LDD 6,X
                    ADDD 6,Y
                    STD 6,Y
                    
                    LDD 4,X
                    ADCD 4,Y
                    STD 4,Y
                    
                    LDD 2,X
                    ADCD 2,Y
                    STD 2,Y
                    
                    LDD 0,X
                    ADCD 0,Y
                    STD 0,Y
                    
                    DECW q

        ok:         TZY
                    AIY @r
                    XGEY
                    LDD j
                    ASLD
                    ADE
                    XGEY                ; Y points to r[j]

                    LDE q
                    STE 0,Y             ; store q in r[j]

                    INCW j
                    LDE j
                    CPE #4
                    BLE mainLoop
                
                
        ; denormalize and move the result in par2. if necessary, correct
        ; the exponent of the result.
        
        

        exit:       TSTW r
                    BNE dnl1
                    LDD newExp
                    SUBD #0x0010
                    STD newExp
                    BRA dnl2
                    
        dnl1:       LSRW r:0
                    RORW r:2
                    RORW r:4
                    RORW r:6
                    RORW r:8
                    
        dnl2:       LSRW r:2
                    RORW r:4
                    RORW r:6
                    RORW r:8
                    
                    LSRW r:2
                    RORW r:4
                    RORW r:6
                    RORW r:8
                    
                    LDD r:2
                    LSRD
                    ANDD #0x0FFF;
                    STD par2:1
                    
                    LDD r:4
                    RORD
                    STD par2:3
                    
                    LDD r:6
                    RORD
                    STD par2:5
                    
                    LDD r:8
                    RORD
                    STAA par2:7
                    
                    LDE par2:0
                    ANDE #0x000F
                    ADDE newExp
                    STE par2:0
                                    
                    BRA end
                    
        div0:       LDX #0             ; Simulate a division by 0
                    EDIV
                        
        zero:       CLRD
                    STD par2:0        ; result is zero
                    STD par2:2
                    STD par2:4
                    STD par2:6
                
        end:

    }
}

/******************************************************************************
  IEEE 64bit comparison.

  Arguments:   p at S + 12
               q at S + 4

  Result:      Set CCR according to q - p
 */

void _DCMP (DOUBLE p, DOUBLE q)
{
  asm {
                    LDD   p.hh
                    EORD  q.hh
                    BPL   SameSign
                    /* The two values have different signs */
                    LDD   p.hh
                    CPD   q.hh
                    BRA   End
    SameSign:       TZX             ; Prepare pointers
                    TZY 
                    BRCLR q.hh, #0x80, Pos
                    /* Both are negative: compare the other way 'round! */
                    AIY   @p
                    AIX   @q
                    BRA   Comp
                    /* Both are positive */
    Pos:            AIY   @q
                    AIX   @p
                    /* Now set flags according to @Y - @X */
    Comp:           LDD   0,Y
                    CPD   0,X
                    BNE   End
                    LDD   2,Y
                    CPD   2,X
                    BNE   End
                    LDD   4,Y
                    CPD   4,X
                    BNE   End
                    LDD   6,Y
                    CPD   6,X            
    End:
  } /* end asm */;
} /* _DCMP */

/******************************************************************************
  Conversion HC16 DSP format to IEEE 64bit format.

  Arguments:   E/D: DSP-float (E = Mantissa in fix point format,
                               D = Exponent in 2's complement)

  Result:      At S + 4. Space reserved by compiler. Note: we can access that
               memory location using "parameter" res.
 */

#define DBL_BIAS          1023
#define DBL_MAX           2047
#define DBL_INF_EXP     0x7FF0

void _DSPTOD (DOUBLE res)
{
  asm {
                    TSTE
                    BNE   NotZero
                    CLRW  res.hh
                    CLRW  res.hl
                    CLRW  res.lh
                    CLRW  res.ll
                    BRA   End
    NotZero:        XGDE                   ; D = mant; E = exp
                    CLRW  res.lh
                    STAA  res.ll           ; Save sign bit
                    BPL   Pos
                    NEGD                   ; Negate mantissa
    Pos:            LSLD                   ; Hidden bit of IEEE format
                    LSLD
                    STD   res.hl           ; Store it
                    CLRW  res.hh
                    LSLW  res.hl           ; Shift left 4 times
                    ROL   res.hh:1
                    LSLW  res.hl
                    ROL   res.hh:1
                    LSLW  res.hl
                    ROL   res.hh:1
                    LSLW  res.hl
                    ROL   res.hh:1         ; Now it's aligned correctly
                    XGDE                   ; D is exp again
                    ADDD  #(DBL_BIAS - 1)  ; -1 because of hidden bit...
                    BVS   Infinity         ; Overflow, return infinity
                    BPL   ChkMax
                    CLRD                   ; Exponent too small, return zero
                    CLRW  res.hl
                    BRA   WasPos           ; Always return +0.0
    ChkMax:         CPD   #DBL_MAX
                    BLE   Ok
    Infinity:       CLRW  res.hl           ; Exp > 2047, return infinity
                    LDD   #DBL_INF_EXP     ; Load infinity's exponent
                    BRA   ChkSign          ; Set sign correctly
    Ok:             LSLD
                    LSLD
                    LSLD
                    LSLD
                    ORD   res.hh
    ChkSign:        BRCLR res.ll, #0x80, WasPos
                    ORAA  #0x80
    WasPos:         CLRW  res.ll
                    STD   res.hh 
    End:
  } /* end asm */;
} /* _DSPTOD */

/******************************************************************************
  Conversion IEEE 32bit format to IEEE 64bit format.

  Arguments:   E/D: float

  Result:      At S + 4. Space reserved by compiler. Note: we can access that
               memory location using "parameter" res.
 */

#define FLT_BIAS    127

void _DLONG (DOUBLE res)
{
  asm {
                    TSTD
                    BNE    NotZero
                    TSTE
                    BNE    NotZero
                    CLRW   res.hh
    ClrMant:        CLRW   res.hl
                    CLRW   res.lh
                    CLRW   res.ll
                    BRA    End
    NotZero:        XGDE
                    STD    res.ll           ; Save D
                    LSLD                    ; Exp in A
                    CLRB
                    XGAB
                    CMPB   #0xFF
                    BNE    NotInf
                    LDD    #0x7FF0
                    TST    res.ll
                    BPL    PosInf
                    ORAA   #0x80
    PosInf:         STD    res.hh
                    BRA    ClrMant
    NotInf:         ADDD   #(DBL_BIAS - FLT_BIAS)     ; New exp in D
                    TST    res.ll
                    BPL    Pos
                    ORAA   #8               ; Set sign bit
    Pos:            LSLD
                    LSLD
                    LSLD
                    LSLD                    ; Left align
                    STD    res.hh
                    LDD    res.ll
                    ANDD   #0x007F          ; Mantissa in D/E
                    LSRB                    ; Align correctly
                    RORE
                    RORA
                    LSRB
                    RORE
                    RORA
                    LSRB
                    RORE
                    RORA
                    STE    res.hl 
                    STAA   res.lh
                    CLR    res.lh:1
                    CLRW   res.ll
                    CLRA
                    ORD    res.hh
                    STD    res.hh
    End:
  } /* end asm */;
} /* end _DLONG */

/******************************************************************************
  Conversion IEEE 64bit format to HC16 DSP format.

  Arguments:   Y is &double

  Result:      E:D is DSP float (E = mant, D = exp)
 */

void _DTODSP (void)
{
  asm {
                    TSTW  0,Y
                    BNE   NotZero
                    TSTW  2,Y
                    BNE   NotZero
                    CLRD
                    TDE
                    BRA   End
    NotZero:        LDAB  1,Y
                    LDE   2,Y
                    ANDB  #0xF
                    ORAB  #0x10            ; Hidden bit (B:E = Mantissa)
                    LDAA  #6               ; Shift count
    Shift:          LSRB
                    RORE
                    DECA
                    BNE   Shift
                    TST   0,Y
                    BPL   IsPos
                    NEGE                   ; E is mantissa
    IsPos:          LDD   0,Y              ; Load exponent
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #(DBL_BIAS - 1)  ; -1 because of hidden bit...
    End:
  } /* end asm */;
} /* end _DTODSP */

/******************************************************************************
  Conversion IEEE 64bit format to IEEE 32bit format.

  Arguments:   Y is &double

  Result:      E:D is IEEE float (E = hi, D = lo)
 */

#define FLT_INF_EXP   0x7F80

void _DSHORT (void)
{
  unsigned long l;

  asm {
                    LDD   0,Y
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #(DBL_BIAS - FLT_BIAS)
                    BPL   ChkInf
                    CLRD                    ; Return zero
                    TDE
                    BRA   End
    ChkInf:         TSTA
                    BEQ   Ok
                    CLRD                    ; Return +/- infinity
                    LDE   #FLT_INF_EXP
                    TST   0,Y
                    BPL   End
                    ORE   #0x8000
                    BRA   End;
    Ok:             LSRB                    ; Shift left by 7 implemented
                    RORA                    ; As rotate right by 1 and
                    XGAB                    ; swap
                    TST   0,Y
                    BPL   IsPos             ; Set sign bit correctly
                    ORAA  #0x80
    IsPos:          STD   l
                    LDD   1,Y
                    LDE   3,Y
                    LSLE
                    ROLD
                    LSLE
                    ROLD
                    LSLE
                    ROLD
                    ANDA  #0x7F
                    STD   l:2
                    TED
                    TAB
                    LDAA  l:3
                    TDE
                    LDD   l
                    ORAB  l:2
                    XGDE
     End:
  } /* end asm */;
} /* end _DSHORT */

/******************************************************************************
  Auxiliary routine to align mantissa correctly. Y is &double; shifts right by
  31 - D bits.
 */

void _drshift (void)
{
  unsigned long a;

  asm {
                    NEGD
                    ADDD  #31
                    TDE
                    LDD   3,Y
                    STD   a
                    LDD   1,Y
                    ANDA  #0xF
                    ORAA  #0x10           ; Hidden bit!!!
                    SUBE  #3
                    BEQ   LoadIt
    DoLeft:         BGT   DoRight
                    LDX   5,Y
                    STX   a:2
    ShLeft:         LSLW  a:2
                    ROLW  a
                    ROLD
                    ADDE  #1
                    BNE   ShLeft
                    BRA   LoadIt
    DoRight:        CPE   #24
                    BCS   Smaller24
                    STAA  a:1
                    CLR   a
                    CLRD
                    SUBE  #24
                    BRA   Smaller8
    Smaller24:      CPE   #16
                    BCS   Smaller16
                    STD   a
                    CLRD
                    SUBE  #16
                    BRA   Smaller8
    Smaller16:      CPE   #8
                    BCS   Smaller8
                    XGDX
                    LDAB  a
                    STAB  a:1
                    XGDX
                    STAB  a
                    CLRB
                    XGAB
                    SUBE  #8
    Smaller8:       BEQ   LoadIt
    Loop:           LSRD
                    RORW  a
                    SUBE  #1
                    BNE   Loop
    LoadIt:         LDE   a
                    XGDE
  } /* end asm */;
} /* end _drshift */

/******************************************************************************
  Conversion IEEE 64bit format to UNSIGNED LONG.

  Arguments:   Y is &double

  Result:      E:D is LONGCARD
 */

void _DUTRUNC (void)
{
  asm {
                    LDD   0,Y
                    BMI   IsNeg              ; IF (d < 0.0) THEN RETURN 0
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #DBL_BIAS
                    BGE   NonNeg             ; IF (exp < 0) THEN
    IsNeg:          CLRD
                    TDE                      ;   RETURN 0
                    BRA   End
    NonNeg:         CPD   #32                ; ELSIF (exp >= 32) THEN
                    BCS   Ok
                    LDD   #0xFFFF            ;   RETURN MAX (LONGCARD)
                    TDE
                    BRA   End                ; END
    Ok:             JSR   _drshift
    End:
  } /* end asm */;
} /* end _DUTRUNC */

/******************************************************************************
  Conversion IEEE 64bit format to LONG.

  Arguments:   Y is &double

  Result:      E:D is LONGINT
 */

void _DSTRUNC (void)
{
  asm {
                    LDD   0,Y
                    ANDD  #DBL_INF_EXP
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    SUBD  #DBL_BIAS
                    BGE   NonNeg             ; IF (exp < 0) THEN
    IsNeg:          CLRD
                    TDE                      ;   RETURN 0
                    BRA   End
    NonNeg:         CPD   #31                ; ELSIF (exp >= 31) THEN
                    BCS   ShiftIt
                    LDE   #0x8000            ;   IF (f < 0.0) THEN
                    CLRD                     ;     RETURN MIN (LONGINT);
                    BRSET 0,Y, #0x80, End    ;   ELSE
                    COME                     ;     RETURN MAX (LONGINT);
                    COMD                     ;   END;
                    BRA   End                ; END
    ShiftIt:        JSR   _drshift
                    BRCLR 0,Y, #0x80, End    ; IF (d < 0.0) THEN
                    NEGE                     ;   result := result;
                    NEGD
                    SBCE  #0
    End:                    
  } /* end asm */;
} /* end _DSTRUNC */

/******************************************************************************
  Auxiliary routine: shift E, D until bit #15 of E is 1. Also decrement X for
  each bit shifted. Than pack it together and store at @Y!
 */

#define DBL_EXP_MAX     (DBL_BIAS + 31)

void _dnormant (void)
{
  asm {
                    LDX   #DBL_EXP_MAX
                    TSTE
                    BMI   Pack
                    BNE   Shift
                    AIX   #-16
                    TDE
                    CLRD
                    TSTE
                    BMI   Pack
    Shift:          AIX   #-1
                    LSLD
                    ROLE
                    BPL   Shift
    Pack:           LSLD                      ; Hidden bit
                    ROLE
                    STD   4,Y
                    XGDX
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    LSLW  4,Y
                    ROLE
                    ROLD
                    STD   0,Y
                    STE   2,Y
                    CLRW  6,Y
  } /* end asm */;
} /* end _dnormant */

/******************************************************************************
  Conversion UNSIGNED LONG to IEEE 64bit format.

  Arguments:   E:D is LONGCARD

  Result:      S+4 is double
 */

void _DUFLOAT (DOUBLE res)
{
  asm {
                    TSTE
                    BNE   NotZero
                    TSTD
                    BNE   NotZero
                    STD   res.hh
                    STD   res.hl
                    STD   res.lh
                    STD   res.ll
                    BRA   End
    NotZero:        TZY
                    AIY   @res
                    JSR   _dnormant
    End:
  } /* end asm */;
} /* end _DUFLOAT */


/******************************************************************************
  Conversion LONG to IEEE 64bit format.

  Arguments:   E:D is LONGINT

  Result:      S+4 is double
 */

void _DSFLOAT (DOUBLE res)
{
  unsigned int i;

  asm {
                    TSTE
                    BNE   NotZero
                    TSTD
                    BNE   NotZero
                    STD   res.hh
                    STD   res.hl
                    STD   res.lh
                    STD   res.ll
                    BRA   End
    NotZero:        STE   i                 ; Set negative flag
                    BPL   Pos
                    NEGE
                    NEGD
                    SBCE  #0
    Pos:            TZY
                    AIY   @res
                    JSR   _dnormant
                    BRCLR i, #0x80, End
                    BSET  res.hh, #0x80
    End:                      
  } /* end asm */;
} /* end _DSFLOAT */
#endif /* __Z_BASED__, __Y_BASED__ */
/******************************************************************************/
/* end rtshc16.c */
