/*****************************************************
    stdlib.c - ANSI-C library: various functions
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <ctype.h>
#include <libdefs.h>

#undef toupper
#undef isspace
#undef isdigit

#define UNKNOWN   4
#define _100TEN     1.0e100
#define _10TEN      1.0e010
#define _1TEN       1.0e001
#define _TEN1       1.0e-001
#define _TEN10      1.0e-010
#define _TEN100     1.0e-100

#ifdef __HC12__
#pragma MESSAGE DISABLE  C12056 /* WARNING C12056: SP debug info incorrect because of optimization or inline assembler */
#endif

#if LIBDEF_ENABLE_OVERFLOW_CHECK
  #define CHECK_OVERFLOW(val, rel, lim, error) if((val) rel (lim)) {_errno = (error); }
#else
  #define CHECK_OVERFLOW(val, rel, lim, error)
#endif

#if LIBDEF_REENTRANT_PRINTF == 0
/*****************************************************/
FILE channels[1];
/*****************************************************/
#endif

#pragma MESSAGE DISABLE C5703 /* func: parameter declared in function atexit but not referenced */

/*****************************************************/

static int CheckCharWithBase(char ch, int base, int *val) { /* << ES 11/11/94 */
/* returns true, if character is compatible to base and sets val, returns false otherwise */
  if (ch >= '0' && ch <= '9')
    *val = ch - '0';
  else {
    if ((ch = toupper (ch)) >= 'A' && ch <= 'Z')
      *val = ch - 'A' + 10;
    else
      return 0; /* fail */
  }
  if (*val < base)
    return 1; /* ok */
  else
    return 0; /* fail */
} /* CheckCharWithBase */
/*****************************************************/
unsigned long int _strtoul(LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, int base, unsigned int len){
  /* len == 0 -> now length specified */
  /* len > 0 -> length of digits == len */
  /* spaces, sign are not counted !! */
  unsigned long int lsum;
  LIBDEF_ConstStringPtr nptr0 = nptr;
  int val;

#if LIBDEF_ENABLE_OVERFLOW_CHECK
  char _errno= ERESET;
#endif
  lsum = 0;
  while (isspace(*nptr)) {
    ++nptr;   /* Eat any white spaces    */
    if (--len == 0) {
      goto done;
    }
  }
  if(*nptr == '+') {
    ++nptr;
    if (--len == 0) {
      goto done;
    }
  }
  if(base == 16) {                  /* Remove 0x or 0X, if any */
    if(*nptr == '0') {
      if (toupper(*++nptr) == 'X') {
        if (--len == 0) {
          goto done;
        }
        ++nptr;
        if (--len == 0) {
          goto done;
        }
      }
    } /* end if */;
  } else if (!base) {               /* Need to find base prefix */
    if(*nptr == '0') {              /* Hex or octal */
       if (toupper(*++nptr) == 'X') {
         if (--len == 0) {
           goto done;
         }
         base = 16; ++nptr;         /* Hex */
         if (--len == 0) {
           goto done;
         }
       } else {
         base = 8;                  /* Octal */
       }
    } else {
      base = 10;                    /* Decimal */
    }
  } /* end if */;
  if(!CheckCharWithBase(*nptr, base, &val)) {
    nptr = nptr0;
    goto done;
  }
  if (base < 37) {
    /* DF 22.10.96 new parameter len for strtol, ... */
    while (CheckCharWithBase(*nptr, base, &val)) { /* << ES 11/11/94: old: 'ishexdigit(*nptr, &val)' */
      CHECK_OVERFLOW(lsum, >, (ULONG_MAX - val) / base, ERANGE);
      lsum = lsum * base + val;
      ++nptr;
      if (--len == 0) {
        goto done;
      }
    } /* end while */;
  } /* end if */;
  done:
  if (endptr) {
    *endptr = nptr;
  }
#if LIBDEF_ENABLE_OVERFLOW_CHECK
  errno= _errno;
  if(_errno == ERANGE) {
    return ULONG_MAX;
  }
#endif
  return lsum;
} /* end strtoul */
/*****************************************************/
unsigned long int strtoul(LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, int base) {
  return _strtoul(nptr, endptr, base, 0);
}
/*****************************************************/
long int _strtol(LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, int base, unsigned int len) {
  /* len == 0 -> now length specified */
  /* len > 0 -> length of digits == len */
  /* spaces, sign are not counted !! */
   long int lsum;
   char nsgn;
   LIBDEF_ConstStringPtr nptr0 = nptr;

   lsum = 0; nsgn = 0;
   while (isspace(*nptr)) {
     ++nptr;   /* Eat any white spaces */
     if (--len == 0) {
       if (endptr != NULL) {
         *endptr= nptr;
       }
       return 0;
     }
   }
   switch(*nptr) {
      case '-': nsgn = 1;            /* Set negative sign flag */
      case '+': ++nptr;
                if (--len == 0) {
                  if (endptr != NULL) {
                    *endptr= nptr;
                  }
                  return 0;
                }
      default:
        if(!(isdigit(*nptr))) {
          nptr = nptr0;
        }
   } /* end switch */;
   lsum = _strtoul(nptr, endptr, base, len);
#if LIBDEF_ENABLE_OVERFLOW_CHECK
   if(errno == ERANGE) {
     return nsgn ? LONG_MIN : LONG_MAX;
   }
#endif
   if(nsgn) {
     return -lsum;
   }
   return lsum;
} /* end strtol */
/*****************************************************/
long int strtol(LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, int base) {
   return _strtol(nptr, endptr, base, 0);
}
/*****************************************************/
double _strtod (LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, unsigned int len) {
  /* len == 0 -> now length specified */
  double dsum, fplc;
  char nsgn, ensgn;
  LIBDEF_ConstStringPtr nptr0 = nptr;
#if LIBDEF_ENABLE_OVERFLOW_CHECK
  char _errno=ERESET;
#endif

  dsum = 0.0; fplc = 10.0;
  nsgn = 0.0; ensgn = 0.0;

  while(isspace(*nptr)) {
    ++nptr;   /* Eat any white spaces */
  }
  if(!(isdigit(*nptr))) {         /* Check for sign */
    switch(*nptr) {
       case '-': nsgn=1;           /* Set negative sign flag */
                 /* fall through */
       case '+': ++nptr;
                 if (--len == 0) {
                   _errno= EOVERFLOW;
                   goto rt;
                 }
       case '.': break;
       default :
         nptr = nptr0;
         _errno = EOVERFLOW;
         goto rt;          /* Neither digit, '.' or sign */
    } /* end switch */;
  } /* end if */;
  while(isdigit(*nptr)) {         /* Set integer portion */
    CHECK_OVERFLOW(dsum, >, DBL_MAX/10.0, EOVERFLOW);
    dsum *= 10.0;
    CHECK_OVERFLOW(dsum, >, DBL_MAX - *nptr + '0', EOVERFLOW);
    dsum += (*nptr++ - '0');
    if (--len == 0) {
      goto rt;
    }
    if (--len == 0) {
      goto rt;
    }
  } /* end while */;
  switch(*nptr) {                  /* Check char */
    default: goto rt;              /* End of 'double' | string */
    case 'e' :
    case 'E' : goto xp;            /* Goto set exponent */
    case '.' : ++nptr;             /* Check locale for decimal . */
               if (--len == 0) {
                 goto rt;
               }
  } /* end switch */;
  while(isdigit(*nptr)) {         /* Set fractional portion */
#if LIBDEF_STRTOD_EXTENDED_PREC
    dsum*=10;
    dsum += (*nptr++ - '0');
#else
    dsum += (*nptr++ - '0') / fplc;
#endif

#if LIBDEF_STRTOD_EXTENDED_PREC
    if (--len == 0) {
      dsum/=fplc;
      goto rt;
    }
    fplc *= 10;
#else
    fplc *= 10;
    if (--len == 0) {
      goto rt;
    }
#endif
  } /* end while */;
#if LIBDEF_STRTOD_EXTENDED_PREC
  dsum/=(fplc/10);
#endif

xp:
  if (toupper(*nptr) == 'E') {    /* Is there an exponent */
    int xp;

    xp = 0;
    switch (*++nptr) {
      case '-' : ensgn = 1;
      case '+' : ++nptr;
      if (--len == 0) {
        goto rt;
      }
    } /* end switch */;
    while (isdigit(*nptr)) {      /* Get exponent value */
      CHECK_OVERFLOW(xp, >, INT_MAX/10, ensgn ? EUNDERFLOW : EOVERFLOW);
#if LIBDEF_ENABLE_OVERFLOW_CHECK
      if(_errno == 0) {
#endif
        xp *= 10;
#if LIBDEF_ENABLE_OVERFLOW_CHECK
      }
#endif
      CHECK_OVERFLOW(xp, >, INT_MAX - *nptr + '0', ensgn ? EUNDERFLOW : EOVERFLOW);
#if LIBDEF_ENABLE_OVERFLOW_CHECK
      if(_errno == 0) {
#endif
        xp += (*nptr++ - '0');
#if LIBDEF_ENABLE_OVERFLOW_CHECK
      } else {
        nptr++;
      }
#endif
      if (--len == 0) {
        break;
      }
    } /* end while */;
/*
    if (ensgn) fplc = 0.001; else fplc = 1000.0;
    while (xp >= 100) {
      dsum *= fplc; xp -= 100;
    };
    if (ensgn) fplc = 0.01; else fplc = 100.0;
    while ( xp >= 10) {
      dsum *= fplc; xp -= 10;
    };
    if (ensgn) fplc = 0.1; else fplc = 10.0;
    while (xp) {
      dsum *= fplc; --xp;
    };
*/
#if LIBDEF_ENABLE_OVERFLOW_CHECK
  if (_errno != ERESET) { /* over or under flow */
    goto rt;
  }
#endif
    if (ensgn) fplc = _TEN100; else fplc = _100TEN;
    /* Set to exponent */
    while (xp >= 100) {
      if(ensgn) {
        CHECK_OVERFLOW(dsum, ==, 0.0, EUNDERFLOW);
      } else {
        CHECK_OVERFLOW(dsum, >, DBL_MAX / fplc, EOVERFLOW);
      }
      dsum *= fplc; xp -= 100;
    } /* end while */;
    if (ensgn) fplc = _TEN10; else fplc = _10TEN;
    while ( xp >= 10) {
      if(ensgn) {
        CHECK_OVERFLOW(dsum, ==, 0.0, EUNDERFLOW);
      } else {
        CHECK_OVERFLOW(dsum, >, DBL_MAX / fplc, EOVERFLOW);
      }
      dsum *= fplc; xp -= 10;
    } /* end while */;
    if (ensgn) fplc = _TEN1; else fplc = _1TEN;
    while (xp) {
      if(ensgn) {
        CHECK_OVERFLOW(dsum, ==, 0.0, EUNDERFLOW);
      } else {
        CHECK_OVERFLOW(dsum, >, DBL_MAX / fplc, EOVERFLOW);
      }
      dsum *= fplc; --xp;
    } /* end while */;
  } /* end if */;

rt:
#if LIBDEF_ENABLE_OVERFLOW_CHECK
  if (nptr0 != nptr) { /* something read */
    if (_errno == EOVERFLOW)  {
      _errno=ERANGE; dsum = HUGE_VAL;
    } else if (_errno == EUNDERFLOW) {
      _errno=ERANGE; dsum = 0;
    }
  }
#endif
  if (endptr) *endptr = nptr;
  if (_errno != 0) {
    errno= _errno;
  }
  if (nsgn) return -dsum; else return dsum;
} /* end strtod */
/*****************************************************/
double strtod (LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr) {
  return _strtod (nptr, endptr, 0);
}
/*****************************************************/
LIBDEF_MemPtr bsearch(LIBDEF_ConstMemPtr key, LIBDEF_ConstMemPtr base,
              size_t n, size_t size,
              int (*cmp) (LIBDEF_ConstMemPtr, LIBDEF_ConstMemPtr))
{
#if defined(__HC05__) || defined(__ST7__)
  HALTX (UNKNOWN);         /* only 2 bytes are allowed for function pointer call */
  return NULL;
#else
  size_t l = 0, x;
  int    res;
  LIBDEF_StringPtr thiS;

  while (l < n) {
    thiS = (LIBDEF_StringPtr) base + (x = ((l + n - 1) / 2)) * size;
    if (!(res = cmp (key, thiS)))
      return thiS;
    if (res < 0)
      n = x;
    else
      l = x + 1;
  } /* end while */;
  return NULL;
#endif
}
/*****************************************************/
static void swap (LIBDEF_MemBytePtr left, LIBDEF_MemBytePtr right, size_t size)
  /**** Attention: assumption is sizeof (sWord) == 2!!! */
{
  if (size & 1) {
    char a;

    /**** Odd size: do byte moves */
    do {
      a = *left; *left++ = *right; *right++ = a;
    } while (--size);
  } else {
    sWord    b, *LIBDEF_VOID_DPTRQ p, *LIBDEF_VOID_DPTRQ q;

    /**** Even size: do word moves */
    p = (sWord *LIBDEF_VOID_DPTRQ) left; q = (sWord *LIBDEF_VOID_DPTRQ) right;
    size >>= 1;
    do {
      b = *p; *p++ = *q; *q++ = b;
    } while (--size);
  } /* end if-else */;
} /* end swap */

/*****************************************************/

#pragma MESSAGE DISABLE C1855 /* WARNING C1855: Recursive function call */

/* Generic quicksort routine, optimized for minimum stack
   usage and speed. */
void qsort (LIBDEF_ConstMemPtr base,
            size_t n, size_t size,
            int (*cmp) (LIBDEF_ConstMemPtr, LIBDEF_ConstMemPtr))
{
#if defined(__HC05__) || defined(__ST7__)
  HALTX (UNKNOWN);         /* only 2 bytes are allowed for function pointer call */
#else
  size_t  i, j;
  LIBDEF_MemBytePtr left, right;      /* always point to base[i] and base[j], except
                                when i == n or j == 0, in this case, they point
                                to base[i-1] and base[j+1]. */
  if (size) {
    /**** It's no use doing anything if size == 0... */
    while (n) {
      /**** We use the very first element of the array for the comparison. */
      i = 0; right = (left = (LIBDEF_MemBytePtr) base) + (j = n) * size;
      for (;;) {
        while (++i < n && cmp (left += size, base) <= 0);
        while (--j     && cmp (right -= size, base) >= 0);
        if (i > j) break;
        swap (left, right, size); /* i < j is asserted */
      } /* end for */;
      if (j) {
        swap (base, right, size);
        /**** Smaller part sorted recursively, bigger part using the while loop. This
              guarantees an upper limit for the stack depth of log (n). */
        if (2 * j < n - 1) {
          /* Left part is smaller */
          qsort (base, j, size, cmp); base = right + size; n -= (j + 1);
        } else {
          /* Right part is smaller */
          qsort (right + size, n - j - 1, size, cmp); n = j;
        } /* end if-else */;
      } else {
        /**** Bad luck, leftmost element was the smallest... Note: since j == 0,
              right points to base[1], not to base[0]! */
        base = right; n--;
      } /* end if-else */;
    } /* end while */;
  } /* end if */;
#endif
} /* end qsort */
/*****************************************************/

div_t  div(int numer, int denom)
{
  div_t res;

  res.quot = numer / denom;
  res.rem  = numer % denom;
  return res;
}

ldiv_t  ldiv(long int numer, long int denom)
{
  ldiv_t res;

  res.quot = numer / denom;
  res.rem  = numer % denom;
  return res;
}

/*****************************************************/
int abs (int j) {
  return M_ABS(j);
}

long int labs (long int j) {
  return M_ABS(j);
}
/*****************************************************/
/* Minimum standard Lehmer random number generator,
   scaled down to integer range... is not re-entrant
   for light-weight processes (uses a global variable)
*/

#define   A        16807  /* Multiplier */
#define   M   2147483647
#define   Q       (M / A)
#define   R       (M % A)

static long seed = 1;

int rand (void)
{
  long  hi, lo;

  hi = seed / Q; lo = seed % Q; seed = A * lo - R * hi;
  if (seed <= 0) seed += M;
  return (int) ((unsigned int) seed & 0x7FFF);
} /* end rand */

void srand (unsigned int init)
{
  seed = init;
} /* end srand */

/*****************************************************/

/* end stdlib.c */
