/*****************************************************
   modf.c - Low-level ANSI library routines
             For DSP Format MC68HC16
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#ifdef __Z_BASED__

#ifdef __FLOAT_IS_IEEE32__

#define FLT_BIAS      127

/******************************************************************************/

typedef struct {
  unsigned int hi, lo;
} FLOAT;

/******************************************************************************/


void modff (FLOAT val)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed.
       Note: register X contains the address of the integral part, the fractional
     part is returned in E:D. */
{
  unsigned long mask;

  asm {
                    PSHM  Y,Z,K
  };
  asm {
#ifdef __LARGE__
                    TBXK
#endif
                    TSZ
                    TXY
                    LDD   val:0,Z
                    LSLD
                    SUBA  #FLT_BIAS
                    BPL   Large
                    CLRW  0,Y           ; Exp < 0
                    CLRW  2,Y           ; int_p := 0.0
                    LDE   val,Z        ; fract := val
                    LDD   val:2,Z
                    BRA   End
    Large:          CMPA  #22
                    BLE   Ok            ; Exp >= 23
                    LDD   val,Z        ; int_p := val
                    STD   0,Y
                    LDD   val:2,Z
                    STD   2,Y
    FractZero:      CLRD                ; fract := 0.0
                    TDE
                    BRA   End
    Ok:             CLRB
                    XGAB
                    XGDX                ; X is exponent
                    LDD   #0xFF80       ; Initial mask
                    CLRE
                    CPX   #0
                    BEQ   EndLoop
    Loop:           ASRD                ; Build mask
                    RORE
                    AIX   #-1
                    BNE   Loop
    EndLoop:        STD   mask,Z     ; Save mask
                    STE   mask:2,Z
                    ANDD  val,Z        ; Compute integral part
                    ANDE  val:2,Z
                    STD   0,Y           ; Store integral part
                    STE   2,Y
                    LDD   mask,Z       ; Compute mask for fraction
                    LDE   mask:2,Z
                    COMD
                    ORD   #0xFF80
                    COME
                    ANDD  val,Z        ; Compute fraction
                    ANDE  val:2,Z
                    LSLD
                    LSRB
                    BNE   NotZero
                    TSTE
                    BEQ   FractZero
    NotZero:        DECA
                    LSLE
                    ROLB
                    BCC   NotZero
                    INCA
                    LSRD
                    RORE
                    BRCLR val,Z, #0x80, Pos
                    ORAA  #0x80
    Pos:            XGDE
    End:            PULM  Z,Y,K
  } /* end asm */;
} /* end modff */

/******************************************************************************/

void frexpf (FLOAT val)
  /* Split a REAL number into exponent and mantissa such that val = 2^exp * mant,
     0.5 > ABS (mant) <= 1.0. Returns both exp = 0 and mantissa = 0.0 if val = 0.0,
     if val = +/- infinity, exp = 128 and mantissa = +/- 1.0  Mantissa returned as
     float in E:D, exponent stored at @X. */
{
  asm {
                    PSHM  Z,K
  };
  asm {
                    TSZ
#ifdef __LARGE__
                    TBXK
#endif
                    LDD   val,Z
                    BNE   NotZero
                    LDE   val:2,Z
                    BNE   NotZero
                    STD   0,X
                    BRA   End
    NotZero:        LSLD
                    XGAB
                    CMPB  #0xFF
                    BNE   NotInf
                    LDD   #128
                    STD   0,X
                    LDE   #0x3F80
                    CLRD
                    BRCLR val,Z, #0x80, PosInf
                    ORE   #0x8000
    PosInf:         BRA   End
    NotInf:         SUBB  #(FLT_BIAS - 1)
                    SXT
                    STD   0,X
                    LDE   val,Z
                    ANDE  #0x807F
                    ORE   #0x3F00
                    LDD   val:2,Z 
    End:            PULM  Z,K
  } /* end asm */;
} /* end frexpf */

/******************************************************************************/

void ldexpf (FLOAT val, int exp)
  /* Returns val * 2^exp. If an underflow occures, 0.0 is returned, in case of an
     overflow, +/- infinity is returned. The result is returned in E:D. */
{
  asm {
                    PSHM  Z,K
  }
  asm {
                    TSZ
                    LDD   val.hi,Z
                    BNE   NotZero
                    TSTW  val.lo,Z
                    BEQ   RetZero
    NotZero:        LSLD
                    CLRB
                    XGAB
                    ADDD  exp,Z
                    BVS   RetInf
                    BPL   NotSmall
    RetZero:        CLRD                      ; Return 0
                    TDE
                    BRA   End
    NotSmall:       CPD   #0x00FF
                    BLS   NotLarge
    RetInf:         CLRD                      ; Return infinity
                    LDE   #0x7F80
                    BRCLR val.hi,Z, #0x80, End
                    ORE   #0x8000
                    BRA   End
    NotLarge:       TBA
                    LDAB  val.hi:1,Z
                    LSLB
                    LSRD
                    BRCLR val.hi,Z, #0x80, Pos
                    ORAA  #0x80
    Pos:            LDE   val.lo,Z
                    XGDE
    End:            PULM  Z,K
  } /* end asm */;
}

/******************************************************************************/

#else                                               /**** FLOAT IS DSP FORMAT */

/******************************************************************************/

typedef struct {
  unsigned int m, e;
} FLOAT;

/******************************************************************************/

void modff (FLOAT val)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed. */

{
   asm {
              PSHM  Y,Z,K
   };
   asm {
              TSZ
#ifdef __LARGE__
              TBXK
#endif
              TXY
              LDE   val.m,Z     ; load mantissa
              BEQ   Zero        ; val == 0.0
              LDD   val.e,Z     ; load exponent
              BLE   SmallVal    ; ABS (val) < 1.0
              CPD   #16
              BGE   LargeVal    ; out of precision, fraction == 0.0
              /**** Exponent of int_part in D, 0 <= D < 16 */
              STD   2,Y         ; Store exponent of int_part
              LDE   #0x8000     ; Build mask
     Loop:    ASRE
              DECB
              BGT   Loop
              TED               ; Mask mantissa
              ANDD  val.m,Z
              STD   0,Y         ; Ok, that was the integral part.
              /**** Now complement mask to get fraction bits. Then normalize,
                    at the end, put in sign bit. */
              LDD   2,Y         ; Load exponent
              COME
              ANDE  val.m,Z     ; Build mantissa bits of fraction
              BEQ   End         ; All bits zero
     Norm:    SUBD  #1          ; Normalize fraction
              ASLE
              BVC   Norm        ; V is set if bit #15 becomes 1
              ADDD  #1          ; We shifted once too much
              LSRE              ; Now the fraction is OK, too
              BRCLR val.m,Z, #0x80, End
              ORE   #0x8000     ; Sign bit
              BRA   End 
    Zero:     CLRD
    SmallVal: /**** int_part = 0.0, fraction = val */
              CLRW  0,Y
              CLRW  2,Y
              BRA   End
    LargeVal: STD   2,Y
              STE   0,Y
              CLRE
              CLRD
    End:      PULM  Y,Z,K
   }
}                   

/******************************************************************************/

void frexpf (FLOAT val)
  /* Split a REAL number into exponent and mantissa such that
     val = 2^exp * mant, 0.5 > ABS (mant) <= 1.0.
     Returns both exp = 0 and mantissa = 0.0 if val = 0.0      */
{
  asm {
           PSHM Z,K
  };
  asm {
           TSZ
#ifdef __LARGE__
           TBXK
#endif
           LDD  val.e,Z   ; load exponent
           LDE  val.m,Z   ; load mantissa
           BNE  L1
           CLRD           ; set exponent to 0
           STD  0,X       ; save exponent to *exp
           BRA  END
     L1:   STD  0,X       ; save exponent to *exp
           CLRD           ; set exponent of result to 0
           CPE  #0x4000   ; special case mant == 1.000000
           BNE  END
           ADDD #1        ; adjust exponent of result and
           DECW 0,X       ; of *exp
     END:  PULM Z,K
  }
} 

/******************************************************************************/

void ldexpf(FLOAT val)
{
  asm {
           TSX
           LDE  val.m,X   ; load mantissa
           ADDD val.e,X   ; add exponents
           BVC  END
           LDD  #0x7FFF   ; overflow=huge val
           TDE
     END:
  }   
}

#endif

/******************************************************************************

             >>>>>>>>>>>> Now the double routines! <<<<<<<<<<<<<<<<

 ******************************************************************************/

#ifdef __DOUBLE_IS_IEEE64__

#define DBL_BIAS          1023

typedef struct {
  unsigned int hh, hl, lh, ll;
} DOUBLE;

typedef struct {
  DOUBLE *p;
} PTR;

/******************************************************************************/

void modf (DOUBLE val, PTR int_part)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed.
       Note: register Y contains the address of the integral part, the fractional
     part is returned in @X */
{
  DOUBLE mask;

  asm {
                    PSHM  Y,Z,K
  };
  asm {
                    TSZ
#ifdef __LARGE__
                    LDAB  int_part.p,Z
                    LDY   int_part.p:1,Z
                    TBYK
#else
                    TSY
                    LDY   int_part.p,Z
#endif
                    LDD   val.hh,Z
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ANDA  #0x07
                    SUBD  #DBL_BIAS
                    BPL   Large         ; Exp < 0
    DontShift:      LDD   val.hh,Z      ; fract := val;
                    STD   0,X
                    LDD   val.hl,Z
                    STD   2,X
                    LDD   val.lh,Z
                    STD   4,X
                    LDD   val.ll,Z
                    STD   6,X
    SetZero:        CLRW  0,Y           ; int_p := 0.0
                    CLRW  2,Y
                    CLRW  4,Y
                    CLRW  6,Y
                    BRA   End
    Large:          CPD   #51
                    BLE   Ok            ; Exp >= 52
                    XGDY                ; Exchange X and Y ==>
                    TXY                 ; int_p := val;
                    XGDX                ; fract := 0;
                    BRA   DontShift
    Ok:             LDE   #0xFFF0       ; Initial mask
                    CLRW  mask.hl,Z
                    CLRW  mask.lh,Z
                    CLRW  mask.ll,Z
                    TSTB
                    BEQ   EndLoop
    Loop:           ASRE                ; Build mask
                    RORW  mask.hl,Z
                    RORW  mask.lh,Z
                    RORW  mask.ll,Z
                    DECB
                    BNE   Loop
    EndLoop:        TED
                    STD   mask.hh,Z
                    ANDD  val.hh,Z      ; Compute & store integral part
                    STD   0,Y
                    LDD   mask.hl,Z
                    ANDD  val.hl,Z
                    STD   2,Y
                    LDD   mask.lh,Z
                    ANDD  val.lh,Z
                    STD   4,Y
                    LDD   mask.ll,Z
                    ANDD  val.ll,Z
                    STD   6,Y
                    LDD   mask.hh,Z
                    COMD                ; Compute mask for fractional part
                    ORD   #0xFFF0
                    COMW  mask.hl,Z
                    COMW  mask.lh,Z
                    COMW  mask.ll,Z
                    ANDD  val.hh,Z      ; Compute fraction and store in mask
                    STD   mask.hh,Z
                    LDD   mask.hl,Z
                    ANDD  val.hl,Z
                    STD   mask.hl,Z
                    LDD   mask.lh,Z
                    ANDD  val.lh,Z
                    STD   mask.lh,Z
                    LDD   mask.ll,Z
                    ANDD  val.ll,Z
                    STD   mask.ll,Z
                    LDD   mask.hh,Z
                    ANDD  #0x000F       ; D is hi nibble of mantissa
                    BNE   NotZero
                    TSTW  mask.hl,Z
                    BNE   NotZero
                    TSTW  mask.lh,Z
                    BNE   NotZero
                    TSTW  mask.ll,Z
                    BNE   NotZero
                    TXY                 ; Fraction is zero
                    BRA   SetZero
    NotZero:        LSRW  mask.hh,Z     ; Get Exponent
                    LSRW  mask.hh,Z
                    LSRW  mask.hh,Z
                    LSRW  mask.hh,Z
                    BCLR  mask.hh,Z, #0xF0
    NormLoop:       DECW  mask.hh,Z     ; Normalize mantissa
                    LSLW  mask.ll,Z
                    ROLW  mask.lh,Z
                    ROLW  mask.hl,Z
                    ROLD
                    BITB  #0x10
                    BEQ   NormLoop
                    LSLW  mask.hh,Z     ; Pack result
                    LSLW  mask.hh,Z
                    LSLW  mask.hh,Z
                    LSLW  mask.hh,Z
                    ANDB  #0x0F
                    ORD   mask.hh,Z
                    BRCLR val.hh,Z, #0x80, Pos
                    ORAA  #0x80
    Pos:            STD   0,X           ; Store result
                    LDD   mask.hl,Z
                    STD   2,X
                    LDD   mask.lh,Z
                    STD   4,X
                    LDD   mask.ll,Z
                    STD   6,X
    End:            PULM  Y,Z,K
  } /* end asm */;
} /* end modf */
                    
/******************************************************************************/

void frexp (DOUBLE val, PTR exp)
  /* Split a REAL number into exponent and mantissa such that val = 2^exp * mant,
     0.5 > ABS (mant) <= 1.0. Returns both exp = 0 and mantissa = 0.0 if val = 0.0,
     if val = +/- infinity, exp = 1024 and mantissa = +/- 1.0  Mantissa returned as
     double in @X, exponent stored at @Y. */
{
  asm {
                    PSHM  Y,Z,K
  };
  asm {
                    TSZ
#ifdef __LARGE__
                    LDAB  exp.p,Z
                    LDY   exp.p:1,Z
                    TBYK
#else 
                    TSY
                    LDY   exp.p,Z
#endif
                    LDD   val.hh,Z
                    BNE   NotZero
                    TSTW  val.hl,Z
                    BNE   NotZero
                    TSTW  val.lh,Z
                    BNE   NotZero
                    TSTW  val.ll,Z
                    BNE   NotZero
                    CLRW  0,Y
                    CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    CPD   #0x07FF
                    BNE   NotInf
                    LDD   #1024
                    STD   0,Y
                    LDD   #0x3FF0
                    BRCLR val.hh,Z, #0x80, PosInf
                    ORAA  #0x80
                    STD   0,X
    PosInf:         BRA   ClrMant
    NotInf:         SUBD  #(DBL_BIAS - 1)
                    STD   0,Y
                    LDD   val.hh,Z
                    ANDD  #0x800F
                    ORD   #0x3FE0
                    STD   0,X
                    LDD   val.hl,Z
                    STD   2,X
                    LDD   val.lh,Z
                    STD   4,X
                    LDD   val.ll,Z
                    STD   6,X
    End:            PULM  Y,Z,K
  } /* end asm */;
} /* end frexp */

/******************************************************************************/

void ldexp (DOUBLE val, int exp)
  /* Returns val * 2^exp. If an underflow occures, 0.0 is returned, in case of an
     overflow, +/- infinity is returned. The result is returned in E:D. */
{
  asm {
                    PSHM  Z,K
  };
  asm {
                    TSZ
                    LDD   val.hh,Z
                    BNE   NotZero
                    TSTW  val.hl,Z
                    BNE   NotZero
                    TSTW  val.lh,Z
                    BNE   NotZero
                    TSTW  val.ll,Z
                    BNE   NotZero
    RetZero:        CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ADDD  exp,Z
                    BVS   RetInf
                    BMI   RetZero
                    CPD   #0x07FF
                    BLS   NotLarge
    RetInf:         LDD   #0x7FF0
                    BRCLR val.hh,Z, #0x80, PosInf
                    ORAA  #0x80
    PosInf:         STD   0,X
                    BRA   ClrMant
    NotLarge:       LSLD
                    LSLD
                    LSLD
                    LSLD
                    TDE
                    BCLRW val.hh,Z, #0x7FF0
                    ORD   val.hh,Z
                    STD   0,X
                    LDD   val.hl,Z
                    STD   2,X
                    LDD   val.lh,Z
                    STD   4,X
                    LDD   val.ll,Z
                    STD   6,X
    End:            PULM  Z,K
  } /* end asm */;
} /* end ldexp */

/******************************************************************************/

#else

  /**** DOUBLE IS THE SAME AS FLOAT, EITHER DSP OR IEEE 32BIT FORMAT */

/******************************************************************************/

#pragma NO_ENTRY
#pragma NO_EXIT

void modf (void)
{
  asm   JMP   modff;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void frexp (void)
{
  asm   JMP   frexpf;
}    

#pragma NO_ENTRY
#pragma NO_EXIT

void ldexp (void)
{
  asm   JMP   ldexpf;
}

#endif

/******************************************************************************
    FAR routines, do not depend on the float format chosen!!
 ******************************************************************************/

void far_modff (FLOAT val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  modff (val);
  asm {
       PULM  K
  } /* end asm */;
} /* end far_modff */

void far_frexpf (FLOAT val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  frexpf (val);
  asm {
       PULM  K
  } /* end asm */;
} /* end far_frexpf */

#ifdef __DOUBLE_IS_IEEE64__

typedef struct {
  DOUBLE *far p;
} FARPTR;

void far_modf (DOUBLE val, FARPTR int_part)
{
  DOUBLE mask;

  asm {
                    PSHM  Y,Z,K
  };
  asm {
                    TSZ
                    LDAB  int_part.p,Z
                    LDY   int_part.p:1,Z
                    TBYK
                    LDD   val.hh,Z
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ANDA  #0x07
                    SUBD  #DBL_BIAS
                    BPL   Large         ; Exp < 0
    DontShift:      LDD   val.hh,Z      ; fract := val;
                    STD   0,X
                    LDD   val.hl,Z
                    STD   2,X
                    LDD   val.lh,Z
                    STD   4,X
                    LDD   val.ll,Z
                    STD   6,X
    SetZero:        CLRW  0,Y           ; int_p := 0.0
                    CLRW  2,Y
                    CLRW  4,Y
                    CLRW  6,Y
                    BRA   End
    Large:          CPD   #51
                    BLE   Ok            ; Exp >= 52
                    XGDY                ; Exchange X and Y ==>
                    TXY                 ; int_p := val;
                    XGDX                ; fract := 0;
                    BRA   DontShift
    Ok:             LDE   #0xFFF0       ; Initial mask
                    CLRW  mask.hl,Z
                    CLRW  mask.lh,Z
                    CLRW  mask.ll,Z
                    TSTB
                    BEQ   EndLoop
    Loop:           ASRE                ; Build mask
                    RORW  mask.hl,Z
                    RORW  mask.lh,Z
                    RORW  mask.ll,Z
                    DECB
                    BNE   Loop
    EndLoop:        TED
                    STD   mask.hh,Z
                    ANDD  val.hh,Z      ; Compute & store integral part
                    STD   0,Y
                    LDD   mask.hl,Z
                    ANDD  val.hl,Z
                    STD   2,Y
                    LDD   mask.lh,Z
                    ANDD  val.lh,Z
                    STD   4,Y
                    LDD   mask.ll,Z
                    ANDD  val.ll,Z
                    STD   6,Y
                    LDD   mask.hh,Z
                    COMD                ; Compute mask for fractional part
                    ORD   #0xFFF0
                    COMW  mask.hl,Z
                    COMW  mask.lh,Z
                    COMW  mask.ll,Z
                    ANDD  val.hh,Z      ; Compute fraction and store in mask
                    STD   mask.hh,Z
                    LDD   mask.hl,Z
                    ANDD  val.hl,Z
                    STD   mask.hl,Z
                    LDD   mask.lh,Z
                    ANDD  val.lh,Z
                    STD   mask.lh,Z
                    LDD   mask.ll,Z
                    ANDD  val.ll,Z
                    STD   mask.ll,Z
                    LDD   mask.hh,Z
                    ANDD  #0x000F       ; D is hi nibble of mantissa
                    BNE   NotZero
                    TSTW  mask.hl,Z
                    BNE   NotZero
                    TSTW  mask.lh,Z
                    BNE   NotZero
                    TSTW  mask.ll,Z
                    BNE   NotZero
                    TXY                 ; Fraction is zero
                    BRA   SetZero
    NotZero:        LSRW  mask.hh,Z    ; Get Exponent
                    LSRW  mask.hh,Z
                    LSRW  mask.hh,Z
                    LSRW  mask.hh,Z
                    BCLR  mask.hh,Z, #0xF0
    NormLoop:       DECW  mask.hh,Z       ; Normalize mantissa
                    LSLW  mask.ll,Z
                    ROLW  mask.lh,Z
                    ROLW  mask.hl,Z
                    ROLD
                    BITB  #0x10
                    BEQ   NormLoop
                    LSLW  mask.hh,Z     ; Pack result
                    LSLW  mask.hh,Z
                    LSLW  mask.hh,Z
                    LSLW  mask.hh,Z
                    ANDB  #0x0F
                    ORD   mask.hh,Z
                    BRCLR val.hh,Z, #0x80, Pos
                    ORAA  #0x80
    Pos:            STD   0,X           ; Store result
                    LDD   mask.hl,Z
                    STD   2,X
                    LDD   mask.lh,Z
                    STD   4,X
                    LDD   mask.ll,Z
                    STD   6,X
    End:            PULM  Y,Z, K
  } /* end asm */;
}

void far_frexp (DOUBLE val, FARPTR exp)
{
  asm {
                    PSHM  Y,Z,K
                    TSZ
                    LDAB  exp.p,Z
                    LDY   exp.p:1,Z
                    TBYK
                    LDD   val.hh,Z
                    BNE   NotZero
                    TSTW  val.hl,Z
                    BNE   NotZero
                    TSTW  val.lh,Z
                    BNE   NotZero
                    TSTW  val.ll,Z
                    BNE   NotZero
                    CLRW  0,Y
                    CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    CPD   #0x07FF
                    BNE   NotInf
                    LDD   #1024
                    STD   0,Y
                    LDD   #0x3FF0
                    BRCLR val.hh,Z, #0x80, PosInf
                    ORAA  #0x80
                    STD   0,X
    PosInf:         BRA   ClrMant
    NotInf:         SUBD  #(DBL_BIAS - 1)
                    STD   0,Y
                    LDD   val.hh,Z
                    ANDD  #0x800F
                    ORD   #0x3FE0
                    STD   0,X
                    LDD   val.hl,Z
                    STD   2,X
                    LDD   val.lh,Z
                    STD   4,X
                    LDD   val.ll,Z
                    STD   6,X
    End:            PULM  Y,Z,K
  } /* end asm */;
}

#else

#pragma NO_ENTRY
#pragma NO_EXIT

void far_modf (void)
{
  asm JMP far_modff;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void far_frexp (void)
{
  asm JMP far_frexpf;
}

#endif


#else

#ifdef __FLOAT_IS_IEEE32__

#define FLT_BIAS      127

/******************************************************************************/

typedef struct {
  unsigned int hi, lo;
} FLOAT;

/******************************************************************************/


void modff (FLOAT val)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed.
       Note: register Y contains the address of the integral part, the fractional
     part is returned in E:D. */
{
  unsigned long mask;

  asm {
#ifdef __LARGE__
  #ifdef __Y_BASED__
                    TBXK
  #else
                    TBYK
  #endif
#endif
#ifdef __Y_BASED__
                    TXY
#endif
                    LDD   val.hi
                    LSLD
                    SUBA  #FLT_BIAS
                    BPL   Large
                    CLRW  0,Y           ; Exp < 0
                    CLRW  2,Y           ; int_p := 0.0
                    LDE   val.hi        ; fract := val
                    LDD   val.lo
                    BRA   End
    Large:          CMPA  #22
                    BLE   Ok            ; Exp >= 23
                    LDD   val.hi        ; int_p := val
                    STD   0,Y
                    LDD   val.lo
                    STD   2,Y
    FractZero:      CLRD                ; fract := 0.0
                    TDE
                    BRA   End
    Ok:             CLRB
                    XGAB
                    XGDX                ; X is exponent
                    LDD   #0xFF80       ; Initial mask
                    CLRE
                    CPX   #0
                    BEQ   EndLoop
    Loop:           ASRD                ; Build mask
                    RORE
                    AIX   #-1
                    BNE   Loop
    EndLoop:        STD   mask          ; Save mask
                    STE   mask:2
                    ANDD  val.hi        ; Compute integral part
                    ANDE  val.lo
                    STD   0,Y           ; Store integral part
                    STE   2,Y
                    LDD   mask          ; Compute mask for fraction
                    LDE   mask:2
                    COMD
                    ORD   #0xFF80
                    COME
                    ANDD  val.hi        ; Compute fraction
                    ANDE  val.lo
                    LSLD
                    LSRB
                    BNE   NotZero
                    TSTE
                    BEQ   FractZero
    NotZero:        DECA
                    LSLE
                    ROLB
                    BCC   NotZero
                    INCA
                    LSRD
                    RORE
                    BRCLR val.hi, #0x80, Pos
                    ORAA  #0x80
    Pos:            XGDE
    End:
#ifdef __Y_BASED__
                    PULM Y,K
#endif
  } /* end asm */;
} /* end modff */

/******************************************************************************/

void frexpf (FLOAT val)
  /* Split a REAL number into exponent and mantissa such that val = 2^exp * mant,
     0.5 > ABS (mant) <= 1.0. Returns both exp = 0 and mantissa = 0.0 if val = 0.0,
     if val = +/- infinity, exp = 128 and mantissa = +/- 1.0  Mantissa returned as
     float in E:D.
     __Y_BASED__: exponent stored at @X
     else       : exponent stored at @Y. */
{
  asm {
#ifdef __LARGE__
  #ifdef __Y_BASED__
                    TBXK
  #else
                    TBYK
  #endif
#endif
                    LDD   val.hi
                    BNE   NotZero
                    LDE   val.lo
                    BNE   NotZero
#ifdef __Y_BASED__
                    STD   0,X
#else
                    STD   0,Y
#endif
                    BRA   End
    NotZero:        LSLD
                    XGAB
                    CMPB  #0xFF
                    BNE   NotInf
                    LDD   #128
#ifdef __Y_BASED__
                    STD   0,X
#else
                    STD   0,Y
#endif
                    LDE   #0x3F80
                    CLRD
                    BRCLR val.hi, #0x80, PosInf
                    ORE   #0x8000
    PosInf:         BRA   End
    NotInf:         SUBB  #(FLT_BIAS - 1)
                    SXT
#ifdef __Y_BASED__
                    STD   0,X
#else
                    STD   0,Y
#endif
                    LDE   val.hi
                    ANDE  #0x807F
                    ORE   #0x3F00
                    LDD   val.lo 
    End:
  } /* end asm */;
} /* end frexpf */

/******************************************************************************/

void ldexpf (FLOAT val, int exp)
  /* Returns val * 2^exp. If an underflow occures, 0.0 is returned, in case of an
     overflow, +/- infinity is returned. The result is returned in E:D. */
{
  asm {
                    LDD   val.hi
                    BNE   NotZero
                    TSTW  val.lo
                    BEQ   RetZero
    NotZero:        LSLD
                    CLRB
                    XGAB
                    ADDD  exp
                    BVS   RetInf
                    BPL   NotSmall
    RetZero:        CLRD                      ; Return 0
                    TDE
                    BRA   End
    NotSmall:       CPD   #0x00FF
                    BLS   NotLarge
    RetInf:         CLRD                      ; Return infinity
                    LDE   #0x7F80
                    BRCLR val.hi, #0x80, End
                    ORE   #0x8000
                    BRA   End
    NotLarge:       TBA
                    LDAB  val.hi:1
                    LSLB
                    LSRD
                    BRCLR val.hi, #0x80, Pos
                    ORAA  #0x80
    Pos:            LDE   val.lo
                    XGDE
    End:
  } /* end asm */;
}

/******************************************************************************/

#elif defined(__FLOAT_IS_DSP__)                     /**** FLOAT IS DSP FORMAT */

/******************************************************************************/

typedef struct {
  unsigned int m, e;
} FLOAT;

/******************************************************************************/

void modff (FLOAT val)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed. */

{
   asm {
#ifdef __Y_BASED__
              PSHM  Y,K
#endif
#ifdef __LARGE__
  #ifdef __Y_BASED__
              TBXK
  #else
              TBYK
  #endif
#endif
#ifdef __Y_BASED__
              TXY
#endif
              LDE   val.m       ; load mantissa
              BEQ   Zero        ; val == 0.0
              LDD   val.e       ; load exponent
              BLE   SmallVal    ; ABS (val) < 1.0
              CPD   #16
              BGE   LargeVal    ; out of precision, fraction == 0.0
              /**** Exponent of int_part in D, 0 <= D < 16 */
              STD   2,Y         ; Store exponent of int_part
              LDE   #0x8000     ; Build mask
     Loop:    ASRE
              DECB
              BGT   Loop
              TED               ; Mask mantissa
              ANDD  val.m 
              STD   0,Y         ; Ok, that was the integral part.
              /**** Now complement mask to get fraction bits. Then normalize,
                    at the end, put in sign bit. */
              LDD   2,Y         ; Load exponent
              COME
              ANDE  val.m       ; Build mantissa bits of fraction
              BEQ   End         ; All bits zero
     Norm:    SUBD  #1          ; Normalize fraction
              ASLE
              BVC   Norm        ; V is set if bit #15 becomes 1
              ADDD  #1          ; We shifted once too much
              LSRE              ; Now the fraction is OK, too
              BRCLR val.m, #0x80, End
              ORE   #0x8000     ; Sign bit
              BRA   End 
    Zero:     CLRD
    SmallVal: /**** int_part = 0.0, fraction = val */
              CLRW  0,Y
              CLRW  2,Y
              BRA   End
    LargeVal: STD   2,Y
              STE   0,Y
              CLRE
              CLRD
    End:
#ifdef __Y_BASED__
              PULM Y,K
#endif
   }
}                   

/******************************************************************************/

void frexpf (FLOAT val)
  /* Split a REAL number into exponent and mantissa such that
     val = 2^exp * mant, 0.5 > ABS (mant) <= 1.0.
     Returns both exp = 0 and mantissa = 0.0 if val = 0.0      */
{
  asm {
#ifdef __LARGE__
#ifdef __Y_BASED__
           TBXK
#else
           TBYK
#endif
#endif
           LDD  val.e     ; load exponent
           LDE  val.m     ; load mantissa
           BNE  L1
           CLRD           ; set exponent to 0
#ifdef __Y_BASED__
           STD  0,X       ; save exponent to *exp
#else
           STD  0,Y       ; save exponent to *exp
#endif
           BRA  END
     L1:   
#ifdef __Y_BASED__
           STD  0,X       ; save exponent to *exp
#else
           STD  0,Y       ; save exponent to *exp
#endif
           CLRD           ; set exponent of result to 0
           CPE  #0x4000   ; special case mant == 1.000000
           BNE  END
           ADDD #1        ; adjust exponent of result and
#ifdef __Y_BASED__
           DECW 0,X       ; of *exp
#else
           DECW 0,Y       ; of *exp
#endif
     END:
  }
} 

/******************************************************************************/

void ldexpf(FLOAT val)
{
  asm {
           LDE  val.m     ; load mantissa
           ADDD val.e     ; add exponents
           BVC  END
           LDD  #0x7FFF   ; overflow=huge val
           TDE
     END:
  }   
}

#endif

/******************************************************************************

             >>>>>>>>>>>> Now the double routines! <<<<<<<<<<<<<<<<

 ******************************************************************************/

#ifdef __DOUBLE_IS_IEEE64__

#define DBL_BIAS          1023

typedef struct {
  unsigned int hh, hl, lh, ll;
} DOUBLE;

/******************************************************************************/
#ifdef __Y_BASED__
  typedef struct {
    DOUBLE *p;
  } PTR;
#endif

#ifdef __Y_BASED__
void modf (DOUBLE val, PTR int_part)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed.
       Note: register Y contains the address of the integral part, the fractional
     part is returned in @X */
{
  DOUBLE mask;

  asm {
                    PSHM  Y,K
#ifdef __LARGE__
                    LDAB  int_part.p
                    LDY   int_part.p:1
                    TBYK
#else
                    TSY
                    LDY   int_part.p
#endif
                    LDD   val.hh
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ANDA  #0x07
                    SUBD  #DBL_BIAS
                    BPL   Large         ; Exp < 0
    DontShift:      LDD   val.hh        ; fract := val;
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    SetZero:        CLRW  0,Y           ; int_p := 0.0
                    CLRW  2,Y
                    CLRW  4,Y
                    CLRW  6,Y
                    BRA   End
    Large:          CPD   #51
                    BLE   Ok            ; Exp >= 52
                    XGDY                ; Exchange X and Y ==>
                    TXY                 ; int_p := val;
                    XGDX                ; fract := 0;
                    BRA   DontShift
    Ok:             LDE   #0xFFF0       ; Initial mask
                    CLRW  mask.hl
                    CLRW  mask.lh
                    CLRW  mask.ll
                    TSTB
                    BEQ   EndLoop
    Loop:           ASRE                ; Build mask
                    RORW  mask.hl
                    RORW  mask.lh
                    RORW  mask.ll
                    DECB
                    BNE   Loop
    EndLoop:        TED
                    STD   mask.hh
                    ANDD  val.hh        ; Compute & store integral part
                    STD   0,Y
                    LDD   mask.hl
                    ANDD  val.hl
                    STD   2,Y
                    LDD   mask.lh
                    ANDD  val.lh
                    STD   4,Y
                    LDD   mask.ll
                    ANDD  val.ll
                    STD   6,Y
                    LDD   mask.hh
                    COMD                ; Compute mask for fractional part
                    ORD   #0xFFF0
                    COMW  mask.hl
                    COMW  mask.lh
                    COMW  mask.ll
                    ANDD  val.hh        ; Compute fraction and store in mask
                    STD   mask.hh
                    LDD   mask.hl
                    ANDD  val.hl
                    STD   mask.hl
                    LDD   mask.lh
                    ANDD  val.lh
                    STD   mask.lh
                    LDD   mask.ll
                    ANDD  val.ll
                    STD   mask.ll
                    LDD   mask.hh
                    ANDD  #0x000F       ; D is hi nibble of mantissa
                    BNE   NotZero
                    TSTW  mask.hl
                    BNE   NotZero
                    TSTW  mask.lh
                    BNE   NotZero
                    TSTW  mask.ll
                    BNE   NotZero
                    TXY                 ; Fraction is zero
                    BRA   SetZero
    NotZero:        LSRW  mask.hh       ; Get Exponent
                    LSRW  mask.hh
                    LSRW  mask.hh
                    LSRW  mask.hh
                    BCLR  mask.hh, #0xF0
    NormLoop:       DECW  mask.hh       ; Normalize mantissa
                    LSLW  mask.ll
                    ROLW  mask.lh
                    ROLW  mask.hl
                    ROLD
                    BITB  #0x10
                    BEQ   NormLoop
                    LSLW  mask.hh       ; Pack result
                    LSLW  mask.hh
                    LSLW  mask.hh
                    LSLW  mask.hh
                    ANDB  #0x0F
                    ORD   mask.hh
                    BRCLR val.hh, #0x80, Pos
                    ORAA  #0x80
    Pos:            STD   0,X           ; Store result
                    LDD   mask.hl
                    STD   2,X
                    LDD   mask.lh
                    STD   4,X
                    LDD   mask.ll
                    STD   6,X
    End:            PULM  Y, K
  } /* end asm */;
} /* end modf */

#else
void modf (DOUBLE val)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed.
       Note: register Y contains the address of the integral part, the fractional
     part is returned in @X */
{
  DOUBLE mask;

  asm {
#ifdef __LARGE__
                    TBYK
#endif
                    LDD   val.hh
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ANDA  #0x07
                    SUBD  #DBL_BIAS
                    BPL   Large         ; Exp < 0
    DontShift:      LDD   val.hh        ; fract := val;
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    SetZero:        CLRW  0,Y           ; int_p := 0.0
                    CLRW  2,Y
                    CLRW  4,Y
                    CLRW  6,Y
                    BRA   End
    Large:          CPD   #51
                    BLE   Ok            ; Exp >= 52
                    XGDY                ; Exchange X and Y ==>
                    TXY                 ; int_p := val;
                    XGDX                ; fract := 0;
                    BRA   DontShift
    Ok:             LDE   #0xFFF0       ; Initial mask
                    CLRW  mask.hl
                    CLRW  mask.lh
                    CLRW  mask.ll
                    TSTB
                    BEQ   EndLoop
    Loop:           ASRE                ; Build mask
                    RORW  mask.hl
                    RORW  mask.lh
                    RORW  mask.ll
                    DECB
                    BNE   Loop
    EndLoop:        TED
                    STD   mask.hh
                    ANDD  val.hh        ; Compute & store integral part
                    STD   0,Y
                    LDD   mask.hl
                    ANDD  val.hl
                    STD   2,Y
                    LDD   mask.lh
                    ANDD  val.lh
                    STD   4,Y
                    LDD   mask.ll
                    ANDD  val.ll
                    STD   6,Y
                    LDD   mask.hh
                    COMD                ; Compute mask for fractional part
                    ORD   #0xFFF0
                    COMW  mask.hl
                    COMW  mask.lh
                    COMW  mask.ll
                    ANDD  val.hh        ; Compute fraction and store in mask
                    STD   mask.hh
                    LDD   mask.hl
                    ANDD  val.hl
                    STD   mask.hl
                    LDD   mask.lh
                    ANDD  val.lh
                    STD   mask.lh
                    LDD   mask.ll
                    ANDD  val.ll
                    STD   mask.ll
                    LDD   mask.hh
                    ANDD  #0x000F       ; D is hi nibble of mantissa
                    BNE   NotZero
                    TSTW  mask.hl
                    BNE   NotZero
                    TSTW  mask.lh
                    BNE   NotZero
                    TSTW  mask.ll
                    BNE   NotZero
                    TXY                 ; Fraction is zero
                    BRA   SetZero
    NotZero:        LSRW  mask.hh       ; Get Exponent
                    LSRW  mask.hh
                    LSRW  mask.hh
                    LSRW  mask.hh
                    BCLR  mask.hh, #0xF0
    NormLoop:       DECW  mask.hh       ; Normalize mantissa
                    LSLW  mask.ll
                    ROLW  mask.lh
                    ROLW  mask.hl
                    ROLD
                    BITB  #0x10
                    BEQ   NormLoop
                    LSLW  mask.hh       ; Pack result
                    LSLW  mask.hh
                    LSLW  mask.hh
                    LSLW  mask.hh
                    ANDB  #0x0F
                    ORD   mask.hh
                    BRCLR val.hh, #0x80, Pos
                    ORAA  #0x80
    Pos:            STD   0,X           ; Store result
                    LDD   mask.hl
                    STD   2,X
                    LDD   mask.lh
                    STD   4,X
                    LDD   mask.ll
                    STD   6,X
    End:
  } /* end asm */;
} /* end modf */
#endif
                    
/******************************************************************************/

#ifdef __Y_BASED__
void frexp (DOUBLE val, PTR exp)
  /* Split a REAL number into exponent and mantissa such that val = 2^exp * mant,
     0.5 > ABS (mant) <= 1.0. Returns both exp = 0 and mantissa = 0.0 if val = 0.0,
     if val = +/- infinity, exp = 1024 and mantissa = +/- 1.0  Mantissa returned as
     double in @X, exponent stored at @Y. */
{
  asm {
                    PSHM  Y,K
#ifdef __LARGE__
                    LDAB  exp.p
                    LDY   exp.p:1
                    TBYK
#else 
                    TSY
                    LDY   exp.p
#endif
                    LDD   val.hh
                    BNE   NotZero
                    TSTW  val.hl
                    BNE   NotZero
                    TSTW  val.lh
                    BNE   NotZero
                    TSTW  val.ll
                    BNE   NotZero
                    CLRW  0,Y
                    CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    CPD   #0x07FF
                    BNE   NotInf
                    LDD   #1024
                    STD   0,Y
                    LDD   #0x3FF0
                    BRCLR val.hh, #0x80, PosInf
                    ORAA  #0x80
                    STD   0,X
    PosInf:         BRA   ClrMant
    NotInf:         SUBD  #(DBL_BIAS - 1)
                    STD   0,Y
                    LDD   val.hh
                    ANDD  #0x800F
                    ORD   #0x3FE0
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    End:            PULM  Y,K
  } /* end asm */;
} /* end frexp */
#else
void frexp (DOUBLE val)
  /* Split a REAL number into exponent and mantissa such that val = 2^exp * mant,
     0.5 > ABS (mant) <= 1.0. Returns both exp = 0 and mantissa = 0.0 if val = 0.0,
     if val = +/- infinity, exp = 1024 and mantissa = +/- 1.0  Mantissa returned as
     double in @X, exponent stored at @Y. */
{
  asm {
#ifdef __LARGE__
                    TBYK
#endif
                    LDD   val.hh
                    BNE   NotZero
                    TSTW  val.hl
                    BNE   NotZero
                    TSTW  val.lh
                    BNE   NotZero
                    TSTW  val.ll
                    BNE   NotZero
                    CLRW  0,Y
                    CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    CPD   #0x07FF
                    BNE   NotInf
                    LDD   #1024
                    STD   0,Y
                    LDD   #0x3FF0
                    BRCLR val.hh, #0x80, PosInf
                    ORAA  #0x80
                    STD   0,X
    PosInf:         BRA   ClrMant
    NotInf:         SUBD  #(DBL_BIAS - 1)
                    STD   0,Y
                    LDD   val.hh
                    ANDD  #0x800F
                    ORD   #0x3FE0
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    End:
  } /* end asm */;
} /* end frexp */
#endif
/******************************************************************************/

void ldexp (DOUBLE val, int exp)
  /* Returns val * 2^exp. If an underflow occures, 0.0 is returned, in case of an
     overflow, +/- infinity is returned. The result is returned in E:D. */
{
  asm {
                    LDD   val.hh
                    BNE   NotZero
                    TSTW  val.hl
                    BNE   NotZero
                    TSTW  val.lh
                    BNE   NotZero
                    TSTW  val.ll
                    BNE   NotZero
    RetZero:        CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ADDD  exp
                    BVS   RetInf
                    BMI   RetZero
                    CPD   #0x07FF
                    BLS   NotLarge
    RetInf:         LDD   #0x7FF0
                    BRCLR val.hh, #0x80, PosInf
                    ORAA  #0x80
    PosInf:         STD   0,X
                    BRA   ClrMant
    NotLarge:       LSLD
                    LSLD
                    LSLD
                    LSLD
                    TDE
                    BCLRW val.hh, #0x7FF0
                    ORD   val.hh
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    End:
  } /* end asm */;
} /* end ldexp */

/******************************************************************************/

#else

  /**** DOUBLE IS THE SAME AS FLOAT, EITHER DSP OR IEEE 32BIT FORMAT */

/******************************************************************************/

#pragma NO_ENTRY
#pragma NO_EXIT

void modf (void)
{
  asm   JMP   modff;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void frexp (void)
{
  asm   JMP   frexpf;
}    

#pragma NO_ENTRY
#pragma NO_EXIT

void ldexp (void)
{
  asm   JMP   ldexpf;
}

#endif

/******************************************************************************
    FAR routines, do not depend on the float format chosen!!
 ******************************************************************************/

void far_modff (FLOAT val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  modff (val);
  asm {
       PULM  K
  } /* end asm */;
} /* end far_modff */

void far_frexpf (FLOAT val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  frexpf (val);
  asm {
       PULM  K
  } /* end asm */;
} /* end far_frexpf */

#ifdef __DOUBLE_IS_IEEE64__

#ifdef __Y_BASED__  

typedef struct {
  DOUBLE *far p;
} FARPTR;

void far_modf (DOUBLE val, FARPTR int_part)
{    DOUBLE mask;


  asm {
                    PSHM  Y,K
                    LDAB  int_part.p
                    LDY   int_part.p:1
                    TBYK
                    LDD   val.hh
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    ANDA  #0x07
                    SUBD  #DBL_BIAS
                    BPL   Large         ; Exp < 0
    DontShift:      LDD   val.hh        ; fract := val;
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    SetZero:        CLRW  0,Y           ; int_p := 0.0
                    CLRW  2,Y
                    CLRW  4,Y
                    CLRW  6,Y
                    BRA   End
    Large:          CPD   #51
                    BLE   Ok            ; Exp >= 52
                    XGDY                ; Exchange X and Y ==>
                    TXY                 ; int_p := val;
                    XGDX                ; fract := 0;
                    BRA   DontShift
    Ok:             LDE   #0xFFF0       ; Initial mask
                    CLRW  mask.hl
                    CLRW  mask.lh
                    CLRW  mask.ll
                    TSTB
                    BEQ   EndLoop
    Loop:           ASRE                ; Build mask
                    RORW  mask.hl
                    RORW  mask.lh
                    RORW  mask.ll
                    DECB
                    BNE   Loop
    EndLoop:        TED
                    STD   mask.hh
                    ANDD  val.hh        ; Compute & store integral part
                    STD   0,Y
                    LDD   mask.hl
                    ANDD  val.hl
                    STD   2,Y
                    LDD   mask.lh
                    ANDD  val.lh
                    STD   4,Y
                    LDD   mask.ll
                    ANDD  val.ll
                    STD   6,Y
                    LDD   mask.hh
                    COMD                ; Compute mask for fractional part
                    ORD   #0xFFF0
                    COMW  mask.hl
                    COMW  mask.lh
                    COMW  mask.ll
                    ANDD  val.hh        ; Compute fraction and store in mask
                    STD   mask.hh
                    LDD   mask.hl
                    ANDD  val.hl
                    STD   mask.hl
                    LDD   mask.lh
                    ANDD  val.lh
                    STD   mask.lh
                    LDD   mask.ll
                    ANDD  val.ll
                    STD   mask.ll
                    LDD   mask.hh
                    ANDD  #0x000F       ; D is hi nibble of mantissa
                    BNE   NotZero
                    TSTW  mask.hl
                    BNE   NotZero
                    TSTW  mask.lh
                    BNE   NotZero
                    TSTW  mask.ll
                    BNE   NotZero
                    TXY                 ; Fraction is zero
                    BRA   SetZero
    NotZero:        LSRW  mask.hh       ; Get Exponent
                    LSRW  mask.hh
                    LSRW  mask.hh
                    LSRW  mask.hh
                    BCLR  mask.hh, #0xF0
    NormLoop:       DECW  mask.hh       ; Normalize mantissa
                    LSLW  mask.ll
                    ROLW  mask.lh
                    ROLW  mask.hl
                    ROLD
                    BITB  #0x10
                    BEQ   NormLoop
                    LSLW  mask.hh       ; Pack result
                    LSLW  mask.hh
                    LSLW  mask.hh
                    LSLW  mask.hh
                    ANDB  #0x0F
                    ORD   mask.hh
                    BRCLR val.hh, #0x80, Pos
                    ORAA  #0x80
    Pos:            STD   0,X           ; Store result
                    LDD   mask.hl
                    STD   2,X
                    LDD   mask.lh
                    STD   4,X
                    LDD   mask.ll
                    STD   6,X
    End:            PULM  Y, K
  } /* end asm */;
}
#else
void far_modf (DOUBLE val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  modf (val);
  asm {
       PULM  K
  } /* end asm */;
}
#endif

#ifdef __Y_BASED__
void far_frexp (DOUBLE val, FARPTR exp)
{
  asm {
                    PSHM  Y,K
                    LDAB  exp.p
                    LDY   exp.p:1
                    TBYK
                    LDD   val.hh
                    BNE   NotZero
                    TSTW  val.hl
                    BNE   NotZero
                    TSTW  val.lh
                    BNE   NotZero
                    TSTW  val.ll
                    BNE   NotZero
                    CLRW  0,Y
                    CLRW  0,X
    ClrMant:        CLRW  2,X
                    CLRW  4,X
                    CLRW  6,X
                    BRA   End
    NotZero:        ANDA  #0x7F
                    LSRD
                    LSRD
                    LSRD
                    LSRD
                    CPD   #0x07FF
                    BNE   NotInf
                    LDD   #1024
                    STD   0,Y
                    LDD   #0x3FF0
                    BRCLR val.hh, #0x80, PosInf
                    ORAA  #0x80
                    STD   0,X
    PosInf:         BRA   ClrMant
    NotInf:         SUBD  #(DBL_BIAS - 1)
                    STD   0,Y
                    LDD   val.hh
                    ANDD  #0x800F
                    ORD   #0x3FE0
                    STD   0,X
                    LDD   val.hl
                    STD   2,X
                    LDD   val.lh
                    STD   4,X
                    LDD   val.ll
                    STD   6,X
    End:            PULM  Y,K
  } /* end asm */;
}
#else
void far_frexp (DOUBLE val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  frexp (val);
  asm {
       PULM  K
  } /* end asm */;
}
#endif

#else

#pragma NO_ENTRY
#pragma NO_EXIT

void far_modf (void)
{
  asm JMP far_modff;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void far_frexp (void)
{
  asm JMP far_frexpf;
}

#endif

#endif  /* __Z_BASED__ */
/******************************************************************************/
/* end ieee64.c */
