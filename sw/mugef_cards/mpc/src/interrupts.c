/*---------------------------------------------------------------------------------------------------------*\
 File:      interrupts.c

 Purpose:   MPC program


 Author:    Alexandre FRassier

 History:   12/05/08  af  Created
\*---------------------------------------------------------------------------------------------------------*/

#define INTERRUPTS_GLOBALS

#include <interrupts.h>
#include <ucos_ii.h>
#include <command.h>
#include <daisy.h>
#include <terminal.h>
#include <analog.h>

void Int1(void)
{

    // Limitation : Do not declare variable here. It won't work with OS_START_ISR

    asm("FCLR B");      // clear bank bit flag (force bank 0)
    OS_START_ISR(Int1);    // save all registers and executes OSIntEnter()

    CmdVmeIrq();

    OS_END_ISR();    // restore previously saved registers and call OSIntExit()
}

void Int2(void)
{

    asm("FCLR B");       // clear bank bit flag (force bank 0)
    OS_START_ISR(Int2);  // save all registers and executes OSIntEnter()

    DaisyRxIrq();

    OS_END_ISR();    // restore previously saved registers and call OSIntExit()
}

void Uart0rxInt(void)
{
    asm("FCLR B");                // clear bank bit flag (force bank 0)
    OS_START_ISR(Uart0rxInt);     // save all registers and executes OSIntEnter()

    TermRxIrq();

    OS_END_ISR();                 // restore previously saved registers and call OSIntExit()
}

void Uart0txInt(void)
{
    asm("FCLR B");             // clear bank bit flag (force bank 0)
    OS_START_ISR(Uart0txInt);  // save all registers and executes OSIntEnter()

    OS_END_ISR();              // restore previously saved registers and call OSIntExit()
}

void Uart4txInt(void)
{
    asm("FCLR B");       // clear bank bit flag (force bank 0)
    OS_START_ISR(Uart4txInt);    // save all registers and executes OSIntEnter()

    OS_END_ISR();        // restore previously saved registers and call OSIntExit()

}

void Uart4rxInt(void)
{
    asm("FCLR B");               // clear bank bit flag (force bank 0)
    OS_START_ISR(Uart4rxInt);    // save all registers and executes OSIntEnter()

    OS_END_ISR();                // restore previously saved registers and call OSIntExit()
}

void AdConvert(void)
{
    asm("FCLR B");              // clear bank bit flag (force bank 0)
    OS_START_ISR(AdConvert);    // save all registers and executes OSIntEnter()

    AnaConvertEndIrq();

    OS_END_ISR();               // restore previously saved registers and call OSIntExit()
}

// EOF

