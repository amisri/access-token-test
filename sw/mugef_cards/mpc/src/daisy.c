/*---------------------------------------------------------------------------------------------------------*\
 File:      Daisy.c

 Purpose:   MPC program - This file will

 Author:    Alexandre FRassier

 History:    12/05/08  af  Created
\*---------------------------------------------------------------------------------------------------------*/
#define DAISY_GLOBALS

#include <daisy.h>
#include <main.h>
#include <sfr87_144.h>
#include <memory_map.h>
#include <terminal.h>


/*! Local Daisy Variables */
struct
{
    // ToDo why not on the stack ??
    struct channel_data * msg;
    struct channel_data msg_buffer[MAX_DAISY_MSG];   /*!< Table to store incoming daisy data */
    INT8U msg_idx;                              /*!< Index within msg_buffer */

} local_daisy;

#if DEBUG_DAISY>1
#define DAISY_TERM_OUT_MAX 5
INT8U daisy_term_out_idx;
INT8U daisy_term_out[DAISY_TERM_OUT_MAX][45];
#endif

/*!
 * This function set up the external interrupt Int2
 * the Int2 is triggered by the MPC_LINK_DAISY peripheral
 */
inline void Setup_extInt2(void)
{
    int2ic = 0x05;    // INT2 interrupt level 5 is selected
}

void DaisyScdTsk(void * data)
{
    INT8U err = OS_NO_ERR;
    struct channel_data * msgq;

#if DEBUG_DAISY>1
    INT8U * cout;
    daisy_term_out_idx = 0;
#endif

    Setup_extInt2();   // irq Daisy. Start Here, problem if done before OSStart

    for (;;)
    {

        msgq = OSQPend(daisy.msgq, 0, &err);     // The messageq will give the channel

#if DEBUG_DAISY>1
        cout = daisy_term_out[daisy_term_out_idx];
        daisy_term_out_idx = (daisy_term_out_idx + 1) % DAISY_TERM_OUT_MAX;
        sprintf(cout, "[Tsk Daisy] Rx word 0x%X on chan %d\n\r", ~msgq->data, msgq->idx);
        OSQPost(MsgQ_Tx.msgq, cout);         // print message
#endif

        if (channel[local_daisy.msg->idx].link == MPC_LINK_DAISY)
        {
            *(CURRENT_STATUS + msgq->idx) = msgq->data ;           // copy it to the current channel status
        }
        else
        {
            FlashLedFlt();
#if DEBUG_DAISY>0
            OSQPost(MsgQ_Tx.msgq,
                    "[Tsk Daisy] Rx daisy but no in daisy mode\r\n");         // print message     // print message
#endif
        }

        debug.task_cnt.daisy_scd ++;
    }
}

void DaisyRxIrq(void)
{

    local_daisy.msg = &local_daisy.msg_buffer[local_daisy.msg_idx];

    local_daisy.msg->idx  = *PLD_DAISY_CH_IDX;   // read the channel
    local_daisy.msg->data = *(PLD_DAISY + local_daisy.msg->idx);  // read the data

    OSQPost(daisy.msgq, local_daisy.msg);   // send data through a message queue (daisy Scheduler task dest.)
    local_daisy.msg_idx = (local_daisy.msg_idx + 1) % MAX_DAISY_MSG;

    FlashLedEvt();

    debug.irq_cnt.daisy ++;

}

// EOF

