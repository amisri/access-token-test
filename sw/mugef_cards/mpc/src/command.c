/*---------------------------------------------------------------------------------------------------------*\
 File:      Cmd.c

 Purpose:   MPC program -
            The CmdScdTsk Task will process the commands and will launch the
            appropriate task in function of the input channel.

 Author:    Alexandre FRassier

 History:
      12/05/08  af  Created
\*---------------------------------------------------------------------------------------------------------*/

#define COMMAND_GLOBALS

#include <command.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <main.h>
#include <channel.h>
#include <terminal.h>
#include <memory_map.h>
#include <sfr87_144.h>
#include <task.h>
#include <leds.h>
#include "mugef/mpc_cmd.h"
#include "mugef/cmd.h"

#define MAX_CVS_PARAM   (MPC_NUM_PARMS-1)

#define  MPC_CMD_NO_CMD              10     // internal used constants


struct Cmd_Scd_vars
{
    command_t  Cmd[MAX_CMD_MSG];
};
struct Cmd_Scd_vars Cmd_Scd;


/* Irq VME Variables */
struct
{
    INT8U   msg_idx;
    command_t cmd_from_vme[MAX_VME_MSG];
    command_t * vme_cmd;
    INT16U idx;
    INT16U offset;
    INT8U loop;
} Irq_Vme;


/*!
 * This function set up the external interrupt Int1
 * the Int1 is triggered by the VME peripheral
 */
inline void Setup_extInt1(void)
{
    INT16U idx;

    // Variables initialisation
    Irq_Vme.msg_idx = 0;

    for (idx = 0; idx < MPC_NUM_CHANNELS; idx++)
    {
        // Ack Initialisation
        *(VME_CMD_ACK + idx) = 0x1;
    }

    /* Initialise ISR variables */
    for (idx = 0; idx < MAX_VME_MSG ; idx++)
    {
        Irq_Vme.cmd_from_vme[idx].sender = VME;
    }

    int1ic = 0x06;    // INT1 interrupt level 6 is selected
}

void CmdScdTsk(void * data)
{

    INT8U cmd_err = OS_NO_ERR;
    command_t * msgqc;
    INT8U ch_idx, loop;
    OS_TCB   task_data;
    INT16U * ws_sram;
    INT8U Cmd_idx = 0;
#if DEBUG_CMD>0
    INT8U * cout;
#endif

    Setup_extInt1();   // irq VME. Start Here, problem if done before OSStart

    for (;;)
    {
        cmd_err = OS_NO_ERR;
        // Waiting for a messageQ from Ascii_cmd,Hex_cm or xx_scd_tsk
        msgqc = OSQPend(command_task.msgq, 0, &cmd_err);

        ch_idx = msgqc->chan_idx;                               // store the channel index

#if DEBUG_CMD>0
        // This make freeze the program!?
        //      cout = term_out[term_out_idx];
        //      term_out_idx = (term_out_idx+1)%TERM_OUT_MAX;
        //      sprintf(cout, "[Tsk Cmd] Rx msg 0x%X/0x%X/0x%X/0x%X/0x%X/0x%X on channel %d\r\n"\
        //               , msgqc->Cmd_V, msgqc->param[0], msgqc->param[1], msgqc->param[2], msgqc->param[3], msgqc->param[4], msgqc->chan_idx);
        //      OSQPost (term.tx.msgq, cout );
        //      OSQPost (term.tx.msgq, "[Tsk Cmd] New cmd\n\r" );        // print message
#endif

        // Ack command
        // cmd_err = MPC_CMD_STATE_PROC; ??? TO BE REMOVED
        if ((msgqc->cmd & MUGEF_CMD_PRIO) == MUGEF_CMD_LOW_PRIO)
        {
            *(CMD_STATE + ch_idx) = MPC_CMD_STATE_PROC;
        }

        if (msgqc->sender == VME)
        {
            *(VME_CMD_ACK + ch_idx) = 0x1;         // set the ack
        }

        if (ch_idx > MPC_NUM_CHANNELS)
        {
            cmd_err = INVALID_CH;
        }
        else if (ch_idx < MUGEF_MAX_CHANNELS) // Physical channel
        {
            if (channel[ch_idx].link == MPC_LINK_UNUSED)
            {
                cmd_err = LINK_NOT_USED;
            }
            else
            {
                Cmd_Scd.Cmd[Cmd_idx].cmd = msgqc->cmd;

                if (msgqc->param[0] > MAX_CVS_PARAM)       // check number of parameters for a write
                {
                    cmd_err = INVALID_ARG;
                }
                else
                {
                    for (loop = 0; loop < MPC_NUM_PARMS; loop++)
                    {
                        Cmd_Scd.Cmd[Cmd_idx].param[loop] = msgqc->param[loop];
                    }

                    if (PMSQPost(&channel[ch_idx].pmsgq, &Cmd_Scd.Cmd[Cmd_idx],
                                 (Cmd_Scd.Cmd[Cmd_idx].cmd & MUGEF_CMD_PRIO) ? 0 : 1) != OS_NO_ERR)
                    {
                        cmd_err =  IPC_FAIL;
                    }

                    Cmd_idx = (Cmd_idx + 1) % MAX_CMD_MSG;
                }
            }

        }
        else if (ch_idx == MPC_GLOBAL_CHANNEL)            // global channel
        {
            ch_global.cmd       = msgqc->cmd;

            for (loop = 0; loop < MPC_NUM_PARMS; loop++)
            {
                ch_global.param[loop] = msgqc->param[loop];
            }

            OSTaskResume(CH_GLOBAL_TSK_PRI);
        }
        else if (ch_idx == MPC_SYSTEM_CHANNEL)                             // system channel
        {
            ch_system.cmd  = msgqc->cmd;

            for (loop = 0; loop < MPC_NUM_PARMS; loop++)
            {
                ch_system.param[loop] = msgqc->param[loop];
            }

            OSTaskResume(CH_SYSTEM_TSK_PRI);
        }


        if (cmd_err != OS_NO_ERR)
        {
            FlashLedWrn();
            *(CMD_STATE + ch_idx) = MPC_CMD_STATE_ERROR;
            ws_sram = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)ch_idx * (INT32U)MPC_DATA_SIZE_B);
            ErrCpy(ws_sram, error_list[cmd_err]);
#ifdef ERROR_REPORT
            OSQPost(term.tx.msgq, "[Tsk Cmd] ");
            OSQPost(term.tx.msgq, error_list[cmd_err]);
            OSQPost(term.tx.msgq, "\r\n");
#endif
        }

        debug.task_cnt.cmd_scd ++;
    }
}

void CmdTermTsk(void * data)
{
    INT16U loop;
    INT16U offset, number_of_read;
    INT16U * pbuff;
    INT8U c, str2[7];
    static command_t cmd_from_term;

    static INT8U std_cmd[510];
    static INT8U std_data[40];
#ifdef TASK_CHECK
    OS_STK_DATA stk_data;
#endif
    INT8U prio;

    cmd_from_term.sender = TERMINAL;

    for (;;)
    {

        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task

        cmd_from_term.chan_idx = 67;
        cmd_from_term.cmd  = 0;

        for (loop = 0; loop < MPC_NUM_PARMS ; loop++)
        {
            cmd_from_term.param[loop] = 0;
        }

        sscanf(std_in, "%c %d %X %d %d %d %d %d", &c, &cmd_from_term.chan_idx, &cmd_from_term.cmd,
               &cmd_from_term.param[0], &cmd_from_term.param[1], &cmd_from_term.param[2], &cmd_from_term.param[3],
               &cmd_from_term.param[4]);

        c = toupper(c);

        for (loop = 0; loop < strlen(str2); loop++)
        {
            str2[loop] = toupper(str2[loop]);
        }

        pbuff = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)cmd_from_term.chan_idx * (INT32U)MPC_DATA_SIZE_B);

        switch (c)
        {
            case 'R': // Read buffer
                number_of_read = *pbuff++;

                if (number_of_read > MPC_DATA_SIZE)
                {
                    sprintf(std_data, "\r\n oversized buffer \r\n");
                    OSQPost(term.tx.msgq, std_data);          // print message
                }
                else
                {
                    sprintf(std_data, "$.%X", *pbuff++);
                    OSQPost(term.tx.msgq, std_data);          // print message

                    for (loop = 1; loop < number_of_read; loop++)
                    {
                        sprintf(std_data, ",%X", *pbuff++);
                        OSQPost(term.tx.msgq, std_data);       // print message
                    }

                    sprintf(std_data, ".");                   // end of datas
                    OSQPost(term.tx.msgq, std_data);          // print message
                }

                break;

            case 'G':
            case 'S':
                OSQPost(command_task.msgq, &cmd_from_term);
                break;

            case 'T': // Test
                for (prio = CH_SND_TSK_PRI; prio <= TST_TSK_PRI; prio++)
                {
#ifdef TASK_CHECK

                    if (OSTaskStkChk(prio, &stk_data) != OS_NO_ERR)
                    {
                        OSQPost(term.tx.msgq, "Stack checking error\r\n");
                    }
                    else
                    {
                        sprintf(std_data, "Task %d, %d free, %d used\r\n", prio, stk_data.OSFree, stk_data.OSUsed);
                        OSQPost(term.tx.msgq, std_data);       // print message
                    }

#endif
                }

                break;

            case 'P':
                cmd_from_term.chan_idx = 67;
                OSTaskResume(PRINT_TSK_PRI);
                *(CMD_STATE + cmd_from_term.chan_idx) = MPC_CMD_NO_CMD;
                break;

            case 'H': // Help
                cmd_from_term.chan_idx = 67;
                OSQPost(term.tx.msgq, help_list);
                *(CMD_STATE + cmd_from_term.chan_idx) = MPC_CMD_NO_CMD;
                break;

            default :
                cmd_from_term.chan_idx = 67;
                *(CMD_STATE + cmd_from_term.chan_idx) = MPC_CMD_NO_CMD;
                break;
        }

        OSTimeDly(10 * TICK_FREQ_KHZ);  // 10ms delay, ToDo what for ?

        switch (*(CMD_STATE + cmd_from_term.chan_idx))
        {
            case  MPC_CMD_NO_CMD :
                break;

            case  MPC_CMD_STATE_PROC :    sprintf(std_cmd, cmd_progress[MPC_CMD_STATE_PROC]);
                break;

            case  MPC_CMD_STATE_READY :   sprintf(std_cmd, cmd_progress[MPC_CMD_STATE_READY]);
                break;

            case  MPC_CMD_STATE_ERROR :   sprintf(std_cmd, cmd_progress[MPC_CMD_STATE_PROC]);
                ErrCpyInv(std_cmd + sizeof("\r\nCommand Error: ") - 1,
                          (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)cmd_from_term.chan_idx * (INT32U)MPC_DATA_SIZE_B));
                break;

            case  MPC_CMD_STATE_TIMEOUT : sprintf(std_cmd, cmd_progress[MPC_CMD_STATE_TIMEOUT]);
                break;

            default :   sprintf(std_cmd, cmd_progress[MPC_CMD_STATE_TIMEOUT + 1]);
                ErrCpyInv(std_cmd, (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)cmd_from_term.chan_idx *
                                              (INT32U)MPC_DATA_SIZE_B));
                break;
        }


        OSQPost(term.tx.msgq, std_cmd);         // print message

        std_cmd[0] = std_data[0] = c = 0x0;

        OSQPost(term.tx.msgq, "\r\n>");
        debug.task_cnt.terminal_cmd ++;

    }
}

void CmdVmeIrq(void)
{
    Irq_Vme.idx =  *VME_ACK_IDX;
    Irq_Vme.vme_cmd = &Irq_Vme.cmd_from_vme[Irq_Vme.msg_idx];

    Irq_Vme.vme_cmd->chan_idx     = Irq_Vme.idx;                       // read the channel
    Irq_Vme.vme_cmd->cmd      = *(VME_CMD_ACK + Irq_Vme.idx);        // get the command
    Irq_Vme.offset = Irq_Vme.idx * MPC_NUM_PARMS;

    for (Irq_Vme.loop = 0; Irq_Vme.loop < MPC_NUM_PARMS; Irq_Vme.loop++)
    {
        Irq_Vme.vme_cmd->param[Irq_Vme.loop]  = *(VME_PARAM + Irq_Vme.offset + Irq_Vme.loop);    // parameters
    }

    // send to the command task

    if (OSQPost(command_task.msgq, Irq_Vme.vme_cmd) != OS_NO_ERR)
    {
        FlashLedWrn();
#ifdef ERROR_REPORT
        OSQPost(term.tx.msgq, "[Irq Vme] Fail posting message\r\n");         // print message
#endif
        debug.error_cnt.irq_vme_msg_err ++;
    }

    Irq_Vme.msg_idx = (Irq_Vme.msg_idx + 1) % MAX_VME_MSG;

    debug.irq_cnt.vme ++;
}

// EOF

