/*---------------------------------------------------------------------------------------------------------*\
 File:    main.c

 Purpose:  MPC program  - Main function

   1. set processer mode (expanded mode1) -- see start.a30 file
         CS0 : VME
         CS1 : MPC_LINK_DAISY
         CS2 : UARTs
   2. Initialize periphirals (see Setup.c)
   3. Variables Initialisation
         Configuration from VME
         Local variable
   4. Startup : reset FPGAs, intialise term, cleam memory
   5. start ucos-ii

 Author:  Alexandre FRassier

 History:
    12/05/08  af  Created
\*---------------------------------------------------------------------------------------------------------*/
#define OS_GLOBALS
#define MPC_GLOBALS
#define TASK_GLOBALS
#define DEBUG_GLOBALS
#define ANALOG_GLOBALS

#include <main.h>
#include <ucos_ii.h>
#include <memory_map.h>
#include <sfr87_144.h>    // M32C/87 PQ144 register declarations
#include <task.h>
#include <stdio.h>        // ANSI C header files
#include <string.h>
#include <ctype.h>
#include <pmsgq.h>
#include <channel.h>
#include <leds.h>
#include <terminal.h>
#include <daisy.h>
#include <test.h>
#include <command.h>
#include <analog.h>
#include <debug.h>


#define Enable_interrupts  asm( "\tFSET I")   // Enable maskable interrupt
#define Disable_interrupts asm( "\tFCLR I")   // Disable maskable interrupt

void  InitStk(OS_STK * stk, INT16U n_words);


/*---------------------------------------------------------------------------------------------------------*/
inline void Setup_Uart0(void)
/*---------------------------------------------------------------------------------------------------------*\
   This function set up the Uart0 .

   the Uart0 is used for controlling a remote terminal

   Baurate setup :
   U0brg = 0x81 : 9600 Bps
   U0brg = 0xA  : 115200 bps
   U0brg = 0x5  : 230400 bps
\*---------------------------------------------------------------------------------------------------------*/
{
    ps0_3 = 1;                   // TxD0 ouput (Table 16.8)
    /* Baudrate setup */
    u0brg = 0xA;                 // Fj / (16 (m+1) ) = Transfert Clock = 115200 Bps  m : u2rb setting value
    /* control Register Setup */
    smd0_u0mr = 1;               // Serial I/O mode select bit
    smd1_u0mr = 0;               // Serial I/O mode select bit
    smd2_u0mr = 1;               // Serial I/O mode select bit
    ckdir_u0mr = 0;              // Internal clock select bit
    stps_u0mr = 0;               // 1 Stop bit length select bit
    prye_u0mr = 0;               // Disables a parity
    u0irs_u0c1 = 0;              // Transmission is completed don't interrupt cause
    clk0_u0c0 = 0;               // F1 selected
    clk1_u0c0 = 0;
    crd_u0c0 = 1;
    /* Interrupt Setup */
    ilvl0_s0ric = 0;             // Interrupt UART0 receive enable (leve 1)
    ilvl1_s0ric = 1;
    ilvl2_s0ric = 0;
    ilvl0_s0tic = 0;             // Interrupt UART0 transmit disable  (level 1)
    ilvl1_s0tic = 0;
    ilvl2_s0tic = 0;
    te_u0c1 = 1;                 // Transmit enable
    //  ti_u2c1 transmit buffer empty flag
    re_u0c1 = 1;                 // Receive enable
    // ri_u2c1  Receive complete flag
}
/*---------------------------------------------------------------------------------------------------------*/
inline void Setup_Uart4(void)
/*---------------------------------------------------------------------------------------------------------*\
   This function set up the Uart4 .

   the Uart4 is used for the RS422 communication . NOT YET USED

   Baurate setup :
   U0brg = 0x81 : 9600 Bps
   U0brg = 0x7  : 230400 bps
\*---------------------------------------------------------------------------------------------------------*/
{
    /* UART4 Config - Pin1 (P96 - TxD4) - Pin144 (P97 - RxD4) 144pin */
    prc2 = 1;                     // Enables writing to pd9,ps3 registers
    ps3_6 = 1;                    //  JUST DO  IT !
    prc2 = 0;                     // disables writing to pd9,ps3 registers
    /* Baudrate setup */
    u4brg = 0x81;                 // Fj / (16 (m+1) ) = Transfert Clock = 9600 Bps  m : u4rb setting value
    /* control Register Setup */
    smd0_u4mr = 1;                // Serial I/O mode select bit
    smd1_u4mr = 0;                // Serial I/O mode select bit
    smd2_u4mr = 1;                // Serial I/O mode select bit
    ckdir_u4mr = 0;               // Internal clock select bit
    stps_u4mr = 0;                // 1 Stop bit length select bit
    prye_u4mr = 0;                // Disables a parity
    u4irs_u4c1 = 1;               // Transmission is completed interrupt cause
    clk0_u4c0 = 0;                // F1 selected
    clk1_u4c0 = 0;
    crd_u4c0 = 1;                 // Disables CTS~/RTS~ functiont
    /* Interrupt Setup */
    ilvl0_s4ric = 0;              // Interrupts disable
    ilvl1_s4ric = 0;
    ilvl2_s4ric = 0;
    ilvl0_s4tic = 0;
    ilvl1_s4tic = 0;
    ilvl2_s4tic = 0;
    te_u4c1 = 0;                  // Transmit Disable
    //  ti_u2c1 transmit buffer empty flag
    re_u4c1 = 0;                  // Receive Disable
    // ri_u4c1  Receive complete flag
}
/*---------------------------------------------------------------------------------------------------------*/
inline void Setup_TimerA0(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function set up the Timer A0 .

  the Timer A0 is used for _OSTickISR
\*---------------------------------------------------------------------------------------------------------*/
{
    //F8 clock selected
    tck0_ta0mr = 1;              //  Timer mode, f8 clock source selected
    tck1_ta0mr = 0;
    ta0ic = 5;                   // Timer A0 interrupt controller level 2
    //  ta0   = 25000;                 // ~10ms
    //  ta0   = 750;                   // ~0.3ms
    ta0   = 2500;                // ~1ms
    //  ta0   = 250;                   // ~0.1ms

    ta0s  = 1;                    // start/stop count
}
/*---------------------------------------------------------------------------------------------------------*/
inline void Setup_IO(void)
/*---------------------------------------------------------------------------------------------------------*\
   This function set up output P92,P93,P94 and P92 .

   P92 : Led_Failed
   P93 : Led_Warning
   P94 : Led_Evt
   P95 : Led_Alive
\*---------------------------------------------------------------------------------------------------------*/
{
    prc2 = 1;     // enable writing to pd9
    pd9_2 = 1;    // out
    prc2 = 1;     // enable writing to pd9
    pd9_3 = 1;    // out
    prc2 = 1;     // enable writing to pd9
    pd9_4 = 1;    // out
    prc2 = 1;     // enable writing to pd9
    pd9_5 = 1;    // out
}
inline void Setup_AD(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function set up the A/D Convwerter .
  The A/D Converter will be used in single sweep mode, 10 bits of resolution
\*---------------------------------------------------------------------------------------------------------*/
{
    md0_ad0con0 = 0;           // Single sweep mode (one shot for multiple pins)
    md1_ad0con0 = 1;

    stop_ad_conv;              // no A/D conversion

    scan0_ad0con1 = 1;         // Ani_0 to Ani_7
    scan1_ad0con1 = 1;

    vcut_ad0con1 = 1;          // vref connected

    bits_ad0con1 = 1;          // 10-bits mode

    cks0_ad0con0 = 0;          // fAD didvided by 4
    cks1_ad0con1 = 0;
    cks2_ad0con3 = 0;

    smp_ad0con2 = 0;           // With Sample and hold

    ad_lsb;                    // Daisy low significant byte selected

    /* Interrupt Setup */
    ilvl0_ad0ic = 1;           // Interrupts level 1
    ilvl1_ad0ic = 0;
    ilvl2_ad0ic = 0;

}
/*---------------------------------------------------------------------------------------------------------*/
inline void Setup_Main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function set up all peripherals
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Set up Timer */
    cst   = 0;                 // set to "0" before rewirting CNTx
    tcspr = 0xF;               // main clock is divided by 2n
    cst   = 1;                 // divider start

    Setup_Uart0();
    Setup_AD();

    Setup_IO();


    pd11_0 = 1;                // Test point
    pd11_1 = 1;
    pd11_2 = 1;
    pd11_3 = 1;

    PT17 = PT18 = PT19 = PT20 = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
inline INT32U CheckSRAM(void)
/*---------------------------------------------------------------------------------------------------------*\
   This function check SRAM memory :
      1. A counter fill all memory space available  (0xC20000 to 0xCFFFFF ~ 450kx16 ) ,
      2. this counter is read back and checked,
      3. The memory is erasing.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U k, tmp;
    INT32U error = 0;
    far INT32U * Test_mram;

    Test_mram = (INT32U *)MPC_RW_SRAM___BASE_ADDRESS;

    for (k = 0; k < 0x37F7F; k++)          /* 0x6FFFF in 32 bits 0x37FFF*/
    {
        *(Test_mram + k) = k;               /* Load counter */
    }

    for (k = 0; k < 0x37F7F; k++)
    {

        tmp = *(Test_mram + k);

        if (tmp != k)            /* Read back and check */
        {
            error ++;
        }
    }

    for (k = 0; k < 0x37F7F; k++)          /* Erase MRAM */
    {
        *(Test_mram + k) = 0;
    }

    return (error);
}

inline INT32U CheckDaisyPLD(void)
{
    INT32U error = 0;
    INT32U idx;

    for (idx = 0; idx < MUGEF_MAX_CHANNELS; idx++)
    {
        *(PLD_CH_LINK + idx) = MPC_LINK_SERIAL;
    }

    for (idx = 0; idx < MUGEF_MAX_CHANNELS; idx++)
    {
        if (*(PLD_CH_LINK + idx) !=  MPC_LINK_SERIAL)
        {
            return 1;
        }
    }

    for (idx = 0; idx < MUGEF_MAX_CHANNELS; idx++)
    {
        *(PLD_CH_LINK + idx) = MPC_LINK_UNUSED;
    }

    for (idx = 0; idx < MUGEF_MAX_CHANNELS; idx++)
    {
        if (*(PLD_CH_LINK + idx) !=  MPC_LINK_UNUSED)
        {
            return 2;
        }
    }

    return error;
}

/*---------------------------------------------------------------------------------------------------------*/
inline INT8S Startup_Main(void)
/*---------------------------------------------------------------------------------------------------------*\
  MPc startup function
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U error, loop;
    INT16U i, j;
    FP32 os_ver;
    union word  ch_init[4];

    LedInit();

    /* Reset FPGAs */
    /* Read FPGA Program Version Number */
    *VME_VERSION = *UARTS_VERSION = *DAISY_VERSION = 0xFFFF;

    /* Set MPC_LINK_UNUSED all channels */
    for (loop = 0; loop < MUGEF_MAX_CHANNELS; loop++)
    {
        *(PLD_CH_LINK + loop) = channel[loop].link =  MPC_LINK_UNUSED;
    }



    /* Clear VT100 Terminal */
    sprintf(std_out, "\33c\33[?7h\a");  /* clear VT100 terminal */
    TermPrint();

    for (loop = 0; loop < 1000000; loop++); // wait ~0.5s

    LED_ALIVE   = LED_OFF;
    LED_FAILED  = LED_ON;
    LED_EVT     = LED_ON;
    LED_WARNING = LED_OFF;

    // print version prog message

    sprintf(std_out, "MPC Firmware Version = %d\r\n", MPC_VER);
    TermPrint();

    sprintf(std_out, "FPGA PROGRAM VERSION : \r\nUARTS = %2i\r\nVME   = %2i\r\nDAISY = %2i\r\n", *UARTS_VERSION,
            *VME_VERSION, *DAISY_VERSION);
    TermPrint();

    error = CheckSRAM();        // not compatible with the status acknowledge

    if (error == 0)
    {
        sprintf(std_out, "SRAM OK \r\n");
        TermPrint();
    }
    else
    {
        sprintf(std_out, "SRAM failed , %lu Error found\r\n", error);
        TermPrint();
        LED_ALIVE   = LED_OFF;
        return (-1);
    }

    error = CheckDaisyPLD();

    if (error == 0)
    {
        sprintf(std_out, "Daisy PLD OK \r\n");
        TermPrint();
    }
    else
    {
        sprintf(std_out, "Daisy PLD failed , Error %lu\r\n", error);
        TermPrint();
    }

    if (error != 0)
    {
        LED_ALIVE   = LED_OFF;
        return (-1);
    }

    for (i = 0; i < MUGEF_MAX_CHANNELS; i++)            // Read register
    {
        if (channel[i].link == MPC_LINK_DAISY)
        {
            *(CURRENT_STATUS + i) = *(PLD_DAISY + i);
        }
        else
        {
            j = *(PLD_DAISY + i);
        }
    }

    os_ver = (FP32)((FP32)OS_VERSION) / 100;
    sprintf(std_out, "uCOS_II VER_%1.2f IS STARTING\r\n", os_ver);
    TermPrint();
    return (0);


}
void panic(INT8U led_code)
{
    LED_ALIVE = (~led_code) & 0x01;
    LED_FAILED = (~led_code) & 0x02;
    LED_EVT = (~led_code) & 0x04;
    LED_WARNING = (~led_code) & 0x08;
    sprintf(std_out, "Panic 0x%X!\r\n", led_code);
    TermPrint();

    while (1)
    {
        asm("NOP");
    }
}

#ifdef TASK_CHECK
#define TASK_CREATE(CAMELCASE, LOWER_NAME, UPPER_NAME) \
    os_err = OSTaskCreateExt( CAMELCASE ## Tsk ,0 ,&tsk_stk.LOWER_NAME[ UPPER_NAME ## _STK_SIZE-1 ], UPPER_NAME ## _TSK_PRI \
                              ,UPPER_NAME ## _TSK_PRI , &tsk_stk.LOWER_NAME[0], UPPER_NAME ## _STK_SIZE, & LOWER_NAME ## _pext, OS_TASK_OPT_STK_CHK ); \
    if (os_err != OS_NO_ERR) \
    { \
        panic(0x0A); \
    }
#else
#define TASK_CREATE(CAMELCASE, LOWER_NAME, UPPER_NAME) \
    os_err = OSTaskCreate( CAMELCASE ## Tsk ,0 ,&tsk_stk.LOWER_NAME[ UPPER_NAME ## _STK_SIZE-1 ], UPPER_NAME ## _TSK_PRI); \
    if (os_err != OS_NO_ERR) \
    { \
        panic(0x0A); \
    }
#endif

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    INT16U i = 0;
    INT8U os_err, err;

    Disable_interrupts;

    /* setup microcontroller mode */
    prc1 = 1;   // Enables writing to PM0
    pm01 = 0;
    pm00 = 1;   // Memory expansion mode selected
    prc1 = 0;   // Disable writing to PM0

    prc1 = 1;   // Enables writing to PM1
    pm10 = 1;   // Mode 1
    prc1 = 0;   // Disable writing to PM0

    ds0 = 1;    // External space 0, CS1 (Daisy) : 16 bits selected
    ds1 = 1;    // External space 1, CS2 (Uarts_Irq) : 16 bits selected
    ds3 = 1;    // External space 3, CS0 (Vme  ) : 16 bits selected

    ewcr000 = 0;  // 1 wait state for external Space 0
    ewcr001 = 1;

    ewcr100 = 0;  // 1 wait state for external Space 1
    ewcr101 = 1;

    ewcr300 = 0;  // 1 wait state for external Space 3
    ewcr301 = 1;

    Setup_Main();

    Disable_interrupts;

    err = Startup_Main();

    if (err == 0)
    {

        Disable_interrupts;

        /* Prepare OS tasks and resources */

        //#if 0
        /* ---- Write detectable pattern into stacks ---- */
        InitStk(&tsk_stk.led_evt[0], LED_EVT_STK_SIZE);
        InitStk(&tsk_stk.led_wrn[0], LED_WRN_STK_SIZE);
        InitStk(&tsk_stk.led_flt[0], LED_FLT_TSK_PRI);
        InitStk(&tsk_stk.led_cpuok[0], LED_CPUOK_STK_SIZE);
        InitStk(&tsk_stk.term_rx[0], TERMINAL_RX_STK_SIZE);
        InitStk(&tsk_stk.term_tx[0], TERMINAL_TX_STK_SIZE);
        InitStk(&tsk_stk.term_cmd[0], TERMINAL_CMD_STK_SIZE);
        InitStk(&tsk_stk.command[0], CMD_SCD_STK_SIZE);
        InitStk(&tsk_stk.daisy[0], DAISY_SCD_STK_SIZE);
        InitStk(&tsk_stk.global_ch[0], CH_GLOBAL_STK_SIZE);
        InitStk(&tsk_stk.system_ch[0], CH_SYSTEM_STK_SIZE);
        InitStk(&tsk_stk.print[0], PRINT_STK_SIZE);

        for (i = 0; i < MUGEF_MAX_CHANNELS; i++)
        {
            InitStk(&tsk_stk.channel[i][0], CH_SND_STK_SIZE);
        }

        InitStk(&tsk_stk.test[0], TST_STK_SIZE);
        //#endif

        OSInit(); // uCOS II internal variables initializations


        TASK_CREATE(LedEvt, led_evt, LED_EVT);
        TASK_CREATE(LedWrn, led_wrn, LED_WRN);
        TASK_CREATE(LedFlt, led_flt, LED_FLT);
        TASK_CREATE(LedCpuOk, led_cpuok, LED_CPUOK);
        TASK_CREATE(TermRx, term_rx, TERMINAL_RX);
        TASK_CREATE(TermTx, term_tx, TERMINAL_TX);
        TASK_CREATE(CmdTerm, term_cmd, TERMINAL_CMD);
        TASK_CREATE(CmdScd, command, CMD_SCD);
        TASK_CREATE(DaisyScd, daisy, DAISY_SCD);
        TASK_CREATE(ChanSystem, system_ch, CH_SYSTEM);
        TASK_CREATE(ChanGlobal, global_ch, CH_GLOBAL);
        TASK_CREATE(Print, print, PRINT);
        TASK_CREATE(Test, test, TST);


        for (i = 0; i < MUGEF_MAX_CHANNELS; i++)
        {
#ifdef TASK_CHECK
            os_err = OSTaskCreateExt(ChanPhysicalTsk, (void *)i, &channel[i].stk[CH_SND_STK_SIZE - 1], CH_SND_TSK_PRI + i
                                     , CH_SND_TSK_PRI + i, &channel[i].stk[0], CH_SND_STK_SIZE, &Chs_Snd_Tsk_pext[i], OS_TASK_OPT_STK_CHK);

#else
            os_err = OSTaskCreate(ChanPhysicalTsk, (void *)i, &tsk_stk.channel[i][CH_SND_STK_SIZE - 1],
                                  CH_SND_TSK_PRI + i);
#endif

            if (os_err != OS_NO_ERR)
            {
                panic(0x0A);
            }
        }

        /* ---- Creating Semaphore ---- */
        analog.sem_adc  = OSSemCreate(0);

        if (analog.sem_adc == (void *)0)
        {
            panic(0x0D);
        }

        /* ---- Creating message queue ---- */
        term.rx.msgq = OSQCreate(&term.rx.msg[0], TERM_IN_MAX);
        daisy.msgq = OSQCreate(&daisy.msg[0], MAX_DAISY_MSG);
        command_task.msgq = OSQCreate(&command_task.msg[0], MAX_VME_MSG);
        term.tx.msgq = OSQCreate(&term.tx.msg[0], TERM_OUT_MAX);

        if (daisy.msgq == (void *)0 || command_task.msgq == (void *)0 || term.tx.msgq == (void *)0)
        {
            panic(0x0E);
        }

        /* ---- Creating Mailbox ---- */
        for (i = 0; i < MUGEF_MAX_CHANNELS; i++)
        {
            channel[i].mbox_rx_data = OSMboxCreate((void *)0);

            if (channel[i].mbox_rx_data == (OS_EVENT *)0)
            {
                panic(0x0D);
            }
        }

        /* ---- Creating Prio MesgQ ---- */
        for (i = 0; i < MUGEF_MAX_CHANNELS; i++)
        {
            if (PMSQInit(&channel[i].pmsgq) == (void *)0)
            {
                panic(0x0D);
            }
        }

        Setup_TimerA0();   // start _OSTickISR

        *MPC_VERSION =  MPC_VER;

        Enable_interrupts;
        PT18 = 1;

        OSStart();         // Never returns
    }

    panic(0x07);
}

void  ErrCpy(INT16U * dst, INT8U * src) { while (*src != 0x00) {*dst++ = (INT16U) * src++;}*dst = 0x00;}
void  ErrCpyInv(INT8U * dst, INT16U * src) {while (*src != 0x00) {*dst++ = (INT8U) * src++;}*dst = 0x00;}

/*---------------------------------------------------------------------------------------------------------*/
void InitStk(OS_STK * stk, INT16U n_words)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write the hex word 0xCAFE into every location of the stack to aid stack overflow
  detection using the debugger
\*---------------------------------------------------------------------------------------------------------*/
{
    while (n_words--)
    {
        *(stk++) = 0xCAFE;
    }

}

// EOF

