/*---------------------------------------------------------------------------------------------------------*\
 File:      leds.c

 Purpose:   MPC program - This file  will manage the LEDs on the front panel


 Author:    Alexandre FRassier

 History:   12/05/08  af  Created
\*---------------------------------------------------------------------------------------------------------*/
#define LEDS_GLOBALS


#include <leds.h>
#include <ucos_ii.h>

#define DAISY_FLASHING 100


/*---------------------------------------------------------------------------------------------------------*/
void LedEvtTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
  This task will flash one time the front panel led evt
\*---------------------------------------------------------------------------------------------------------*/
{

    INT8U err = OS_NO_ERR;

    /* leds are unlighted here*/
    LED_WARNING = LED_OFF;
    LED_FAILED  = LED_OFF;
    LED_ALIVE   = LED_OFF;
    LED_EVT     = LED_OFF;

    for (;;)
    {
        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task
        LED_EVT = LED_ON;
        OSTimeDly(LED_EVT_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_EVT = LED_OFF;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void LedWrnTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
  This task will flash one times the front panel led evt
\*---------------------------------------------------------------------------------------------------------*/
{
    BOOLEAN  store_led_state;

    for (;;)
    {
        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task
        store_led_state = LED_WARNING;
        LED_WARNING = LED_OFF;
        OSTimeDly(LED_WRN_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_WARNING = LED_ON;
        OSTimeDly(LED_WRN_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_WARNING = LED_OFF;
        OSTimeDly(LED_WRN_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_WARNING = store_led_state;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void LedFltTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
  This task will flash one times the front panel led evt
\*---------------------------------------------------------------------------------------------------------*/
{
    BOOLEAN  store_led_state;

    for (;;)
    {
        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task
        store_led_state = LED_FAILED;
        LED_FAILED = LED_OFF;
        OSTimeDly(LED_FLT_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_FAILED = LED_ON;
        OSTimeDly(LED_FLT_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_FAILED = LED_OFF;
        OSTimeDly(LED_FLT_FLASH_TIME_MS * TICK_FREQ_KHZ);
        LED_FAILED = store_led_state;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void LedCpuOkTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
  This task will flash the front panel led CPU OK
\*---------------------------------------------------------------------------------------------------------*/
{

    for (;;)                                     // no end loop
    {
        LED_ALIVE = ~LED_ALIVE;
        OSTimeDly(LED_CPUOK_TOGGLE_TIME_MS * TICK_FREQ_KHZ);
    }
}

void LedInit(void)
{
    /* leds are ligthed */
    LED_WARNING = LED_ON;
    LED_FAILED  = LED_ON;
    LED_ALIVE   = LED_ON;
    LED_EVT     = LED_ON;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: Leds.c
\*---------------------------------------------------------------------------------------------------------*/
