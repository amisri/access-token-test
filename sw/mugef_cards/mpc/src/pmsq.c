/*---------------------------------------------------------------------------------------------------------*\
 File:      pmsgq.c

 Purpose:   Message Queue with priority.

 Author:    Hugo Lebreton

 History:
      07/2011  hl  Created
\*---------------------------------------------------------------------------------------------------------*/

#include <pmsgq.h>

OS_EVENT * PMSQInit(PMSGQ * pq)
{

    INT8U i;

    for (i = 0; i < PMSQ_MAX_PRI; i++)
    {
        pq->start[i] = pq->end[i] = 0;
        pq->isEmpty[i] = 1;
    }

    pq->sem = OSSemCreate(0);
    return pq->sem;
}

void * PMSQPend(PMSGQ * pq, INT16U timeout, INT8U * err)
{
    INT8U i;
    void * msg;

    OSSemPend(pq->sem, timeout, err);

    if (*err != OS_NO_ERR)
    {
        return ((void *) 0);
    }

    OS_ENTER_CRITICAL();

    for (i = 0; i < PMSQ_MAX_PRI; i++)
    {
        if (!pq->isEmpty[i])
        {

            msg = pq->msg[i][pq->start[i]];
            pq->start[i] = (pq->start[i] + 1) % PMSQ_MAX_MSG;

            if (pq->start[i] == pq->end[i])
            {
                pq->isEmpty[i] = 1;
            }

            OS_EXIT_CRITICAL();
            return msg;
        }
    }

    OS_EXIT_CRITICAL();
    *err = OS_Q_EMPTY;
    return ((void *) 0);
}


INT8U PMSQPost(PMSGQ * pq, void * msg, INT8U prio)
{
    if (prio >= PMSQ_MAX_PRI)
    {
        return OS_PRIO_INVALID;
    }

    OS_ENTER_CRITICAL();

    if ((pq->start[prio] == pq->end[prio]) && (!pq->isEmpty[prio]))
    {
        OS_EXIT_CRITICAL();
        return OS_Q_FULL;
    }

    pq->msg[prio][pq->end[prio]] = msg;
    pq->end[prio] = (pq->end[prio] + 1) % PMSQ_MAX_MSG;
    pq->isEmpty[prio] = 0;
    OSSemPost(pq->sem);
    OS_EXIT_CRITICAL();
    return OS_NO_ERR;
}


INT8U PMSQIsEmpty(PMSGQ * pq, INT8U prio)
{
    return (pq->isEmpty[prio]);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: pmsgq.c
\*---------------------------------------------------------------------------------------------------------*/
