/*!
 *  @file     channel.c
 *  @defgroup MUGEF MPC
 *  @brief
 *
 *  Description    :
 */

#define CHANNEL_GLOBALS

#include <channel.h>
#include <leds.h>
#include "mugef/mpc_cmd.h"
#include "mugef/cmd.h"
#include <sfr87_144.h>
#include <memory_map.h>
#include <terminal.h>
#include <command.h>
#include <task.h>
#include <analog.h>
#include <debug.h>
#include <main.h>

#define CHAN_TERM_OUT_MAX   20

#define MAX_VME_BUFFER      0xFFFF      // vme buffer max size

#define DISABLE             0
#define ENABLE              1

void ChanPhysicalTsk(void * pdata)
{
    // Begin of Ch_Snd_vars
    INT8U cmd_err = NO_ERR;
    INT8U cmd_state = MPC_CMD_STATE_READY;
    INT8U err = 0;
    INT16U cmd = 0;
    INT16U * w_sram;
    INT16U resp;
    INT16U * const ws_sram = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U) pdata * (INT32U) MPC_DATA_SIZE_B);
    INT16U msg;
    INT8U idx;
    INT16U loop;
    INT16U current_status = 0;
    command_t * Ch_Cmd;
#if DEBUG_CHAN>0
    INT8U * cout;
#endif
    // end of Ch_Snd_vars

    idx = (INT8U) pdata; // = channel number

    *ws_sram = 0;      //Ch_Snd_ini
    *(CMD_STATE + idx) = cmd_state;
#if DEBUG_CHAN>0
    term_out_idx = 0;
#endif

    for (;;)
    {
        // begin of Ch_Snd_core
        cmd_err = NO_ERR;
        w_sram = ws_sram;
        Ch_Cmd = PMSQPend(&channel[idx].pmsgq, 0, &err);

        if (err != OS_NO_ERR)
        {
            OSQPost(term.tx.msgq, "[Tsk Chan] Error waiting msg\r\n");
        }
        else
        {
#if DEBUG_CHAN>0
            cout = term_out[term_out_idx];
            term_out_idx = (term_out_idx + 1) % CHAN_TERM_OUT_MAX;
            sprintf(cout, "[Tsk Chan %d] Rx msg 0x%X/0x%X/0x%X/0x%X/0x%X/0x%X\r\n", \
                    idx, Ch_Cmd->cmd , Ch_Cmd->param[0], Ch_Cmd->param[1], Ch_Cmd->param[2], Ch_Cmd->param[3], Ch_Cmd->param[4]);
            OSQPost(term.tx.msgq, cout);

#endif
            cmd = Ch_Cmd->cmd;

            switch (channel[idx].link)
            {
                    // end of Ch_Snd_core_header
                    // begin of Ch_snd_core_DAISY
                case (MPC_LINK_DAISY) :

                    cmd = cmd & MUGEF_CMD_MASK; // remove read/write bits and prio bits

                    if (cmd & ~MUGEF_CMD_DAISY)
                    {
                        cmd_err = UNKNOWN_CMD;
                    }
                    else
                    {
                        *w_sram++ = 1; // return 1 data
                        *(PLD_DAISY + idx) = cmd;
                        // We want to be sure the status will be updated when we send a command,
                        // before we say CMD_STATE_READY. Worst case is 2 daisy chain time period
                        OSTimeDly(2 * TMO_DAISY_MS * TICK_FREQ_KHZ);
                        *w_sram = ~*(PLD_DAISY + idx);
                    }

                    break;

                default:
                    break;
            }
        }

        if (cmd_err == NO_ERR)
        {
            cmd_state = MPC_CMD_STATE_READY;
        }
        else
        {
            FlashLedWrn();
            debug.error_cnt.ch_err[idx]++;
            cmd_state = MPC_CMD_STATE_ERROR;

            if ((cmd & MUGEF_CMD_PRIO) == MUGEF_CMD_LOW_PRIO)
            {
                ErrCpy(ws_sram, error_list[cmd_err]);
            }

#ifdef ERROR_REPORT
            OSQPost(term.tx.msgq, "[Tsk Chan] ");
            OSQPost(term.tx.msgq, error_list[cmd_err]);
            OSQPost(term.tx.msgq, "\r\n");
#endif
            // In case of error, wait for TMO_NETWORK_CMD to reset the communication with slave.
            OSTimeDly(MPC_NETWORK_TMO_MS * TICK_FREQ_KHZ);
        }

        if ((cmd & MUGEF_CMD_PRIO) == MUGEF_CMD_LOW_PRIO)
        {
            *(CMD_STATE + idx) = cmd_state;
        }
        else
        {
            // Do nothing
        }

        debug.task_cnt.ch_snd[idx]++;
        // end of Ch_snd_core_end
    }
}

void ChanGlobalTsk(void * pdata)
{
    INT8U   cmd_err, cmd_state, i;
    INT16U * w_sram, * r_sram;
    static INT16U * ws_sram;
    INT16U  offset, bitwise, loop;
    const INT8U   idx = MPC_GLOBAL_CHANNEL;
#if DEBUG_CHAN>0
    INT8U  *  cout;
#endif

    ws_sram = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)MPC_GLOBAL_CHANNEL * (INT32U)MPC_DATA_SIZE_B);
    *ws_sram = 0;    // no response expected by default
    *(CMD_STATE + idx) = MPC_CMD_STATE_READY;

    for (;;)
    {

        cmd_err   = NO_ERR;
        r_sram = w_sram = ws_sram;

        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task
        *w_sram = 0;    // no response expected by default

#if DEBUG_CHAN>1
        cout = term_out[term_out_idx];
        term_out_idx = (term_out_idx + 1) % CHAN_TERM_OUT_MAX;
        sprintf(cout, "[Global Cmd] Rx msg 0x%X/0x%X/0x%X/0x%X/0x%X/0x%X\r\n"
                , ch_global.cmd, ch_global.param[0], ch_global.param[1], ch_global.param[2], ch_global.param[3],
                ch_global.param[4]);
        OSQPost(term.tx.msgq, cout);
#endif

        switch (ch_global.cmd)
        {
            case MPC_CMD_SET_1_LINK : // set the position for one channel  param0=nb of pm param1=ch, load pos from vme_mem(0:MPC_LINK_UNUSED,1:uart,2:daisy)
                if (ch_global.param[0] == 1 && ch_global.param[1] < MUGEF_MAX_CHANNELS)
                {
                    switch (*(CHAN_LINK + ch_global.param[1]))
                    {
                        case MPC_LINK_SERIAL :
                            *(PLD_CH_LINK + ch_global.param[1]) = channel[ch_global.param[1]].link = MPC_LINK_SERIAL ;
                            break;

                        case MPC_LINK_DAISY :
                            *(PLD_CH_LINK + ch_global.param[1]) = channel[ch_global.param[1]].link = MPC_LINK_DAISY ;
                            break;

                        case MPC_LINK_UNUSED :
                        default :
                            *(PLD_CH_LINK + ch_global.param[1]) = channel[ch_global.param[1]].link =  MPC_LINK_UNUSED;
                            break;
                    }

                    OSTimeDly(RELAY_DELAY_MS * TICK_FREQ_KHZ);       // relays are low devices

                    //*UARTS_VERSION = 0xFFFF;            // do an Uarts reset - prevent spurious interrupt
                    //OSTimeDly (1);
                    if (channel[ch_global.param[1]].link == MPC_LINK_SERIAL)
                    {
                        // Polling of status will start in dedicated task.
                    }
                    else if (channel[ch_global.param[1]].link == MPC_LINK_DAISY)
                    {
                        *(CURRENT_STATUS + ch_global.param[1]) = *(PLD_DAISY + ch_global.param[1]);
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_SET_ALL_LINK :

                if (ch_global.param[0] == 0)
                {
                    for (loop = 0; loop < MUGEF_MAX_CHANNELS; loop++)
                    {
                        switch (*(CHAN_LINK + loop))
                        {
                            case MPC_LINK_SERIAL :
                                *(PLD_CH_LINK + loop) = channel[loop].link = MPC_LINK_SERIAL ;
                                break;

                            case MPC_LINK_DAISY :
                                *(PLD_CH_LINK + loop) = channel[loop].link = MPC_LINK_DAISY ;
                                break;

                            case MPC_LINK_UNUSED :
                            default :
                                *(PLD_CH_LINK + loop) = channel[loop].link =  MPC_LINK_UNUSED;
                                break;
                        }
                    }

                    OSTimeDly(640 * TICK_FREQ_KHZ);         // relays are low devices  & wait daisy cycle
                    *UARTS_VERSION = 0xFFFF;                // do an Uarts reset - prevent spurious interrupt
                    OSTimeDly(640 * TICK_FREQ_KHZ);

                    for (loop = 0; loop < MUGEF_MAX_CHANNELS; loop++)
                    {
                        if (channel[loop].link == MPC_LINK_SERIAL)
                        {
                            // Status polling done in dedicated task
                        }
                        else if (channel[ch_global.param[1]].link == MPC_LINK_DAISY)
                        {
                            *(CURRENT_STATUS + loop) = *(PLD_DAISY + loop);
                        }
                    }

                    OSTimeDly(50 * TICK_FREQ_KHZ);  // 50ms delay ToDo what for?
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_SET_1_LINK_T   : // set the position for one channel  param1=ch, param2=pos(0:MPC_LINK_UNUSED,1:uart,2:daisy)
                if (ch_global.param[0] == 2 && ch_global.param[1] < MUGEF_MAX_CHANNELS)
                {
                    *(PLD_CH_LINK + ch_global.param[1]) = channel[ch_global.param[1]].link = ch_global.param[2] ;
                    OSTimeDly(RELAY_DELAY_MS * TICK_FREQ_KHZ);       // relays are low devices

                    //*UARTS_VERSION = 0xFFFF;            // do an Uarts reset - prevent spurious interrupt
                    //OSTimeDly (1);
                    if (channel[ch_global.param[1]].link == MPC_LINK_SERIAL)
                    {
                        // Status polling done in dedicated task
                    }
                    else if (channel[ch_global.param[1]].link == MPC_LINK_DAISY)
                    {
                        *(CURRENT_STATUS + ch_global.param[1]) = *(PLD_DAISY + ch_global.param[1]);
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_GET_1_LINK       : // get the position for one channel  param1=ch )
                *w_sram++ = 1;

                if (ch_global.param[0] == 1 && ch_global.param[1] < MUGEF_MAX_CHANNELS)
                {
                    *w_sram = *(PLD_CH_LINK + ch_global.param[1]) ;
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case GLB_CMD_SET_1_SHD_CH   : // set the shutdown mode for one channel  param1=ch, param2=mode(0:DISABLE,1:DISABLE)
                if (ch_global.param[0] == 2 && ch_global.param[1] < MUGEF_MAX_CHANNELS && ch_global.param[2] <= ENABLE)
                {
                    offset = ch_global.param[1] / 16;                       // calculated the register number index
                    ch_global.param[1] = ch_global.param[1] - offset * 16 ;   // param1 become the bit index

                    if (ch_global.param[2] == DISABLE)
                    {
                        bitwise = ~(0x1 <<  ch_global.param[1]);                   // shift and complemente
                        *(UARTS_SHD + offset) = *(UARTS_SHD + offset) & bitwise ; // bit selection
                    }
                    else
                    {
                        bitwise = 0x1 <<  ch_global.param[1];                      // only shift
                        *(UARTS_SHD + offset) = *(UARTS_SHD + offset) | bitwise ; // bit selection
                    }

                    OSTimeDly(1 * TICK_FREQ_KHZ);       // wait 1ms for driver goes to shutdown mode
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case GLB_CMD_GET_ALL_SHD_CH      : // get the driver mode
                if (ch_global.param[0] == 0)
                {
                    *w_sram++ = 4;

                    for (loop = 0; loop < 4; loop++)
                    {
                        *w_sram++ = *(UARTS_SHD + loop) ;
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_GET_ALL_LINK      : // get the position for alle channels
                if (ch_global.param[0] == 0)
                {
                    *w_sram++ = MUGEF_MAX_CHANNELS;

                    for (loop = 0; loop < MUGEF_MAX_CHANNELS; loop++)
                    {
                        *w_sram++ = *(PLD_CH_LINK + loop) ;
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_GET_ALL_DAISY : // read all daisy channel
                if (ch_global.param[0] == 0)
                {
                    *w_sram++ = MUGEF_MAX_CHANNELS;

                    for (i = 0; i < MUGEF_MAX_CHANNELS; i++)
                    {
                        *w_sram++ = *(PLD_DAISY + i);
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_GET_ONE_DAISY : // read one daisy channel
                if (ch_global.param[0] == 1)
                {
                    *w_sram++ = 1;
                    *w_sram++ = *(PLD_DAISY + ch_global.param[1]);
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_CLEAR_RESP_BUF :  // Clear channel buffer
                if (ch_global.param[0] == 1 && ch_global.param[1] < MPC_NUM_CHANNELS)
                {
                    w_sram = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)ch_global.param[1] * (INT32U)MPC_DATA_SIZE_B);

                    for (loop = 0; loop < MPC_DATA_SIZE_B; loop++)
                    {
                        *w_sram++ = 0;
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case GLB_CMD_READ_BUFF : // Read channel buffer
                if (ch_global.param[0] == 2 && ch_global.param[1] < MPC_NUM_CHANNELS && ch_global.param[2] < MPC_DATA_SIZE)
                {
                    r_sram = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)ch_global.param[1] * (INT32U)MPC_DATA_SIZE_B);
                    *w_sram++ = ch_global.param[2];

                    for (loop = 0; loop < ch_global.param[2]; loop++)
                    {
                        *w_sram++ = *r_sram++;
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_SEND_VME_BUF       : // Send the VME buff (for prog). Do not expect any answer.
                if (ch_global.param[0] == 3 && ch_global.param[1] < MUGEF_MAX_CHANNELS &&
                    ch_global.param[2] < MAX_VME_BUFFER)
                {
                    // Use the processing state
                    *(CMD_STATE + idx) = MPC_CMD_STATE_PROC;

                    channel[ch_global.param[1]].link = MPC_LINK_SERIAL_PROG;
                    *w_sram++ = ch_global.param[3];

                    for (loop = 0; loop < ch_global.param[2]; loop++)
                    {
                        *(UARTS + ch_global.param[1]) = *(VME_BUFF + loop); // send cmd
#if DEBUG_CHAN>0
                        cout = term_out[term_out_idx];
                        term_out_idx = (term_out_idx + 1) % CHAN_TERM_OUT_MAX;
                        sprintf(cout, "[Global Cmd] Tx 0x%X\r\n", *(VME_BUFF + loop));
                        OSQPost(term.tx.msgq, cout);
#endif
                    }

                    for (loop = 0; loop < ch_global.param[3]; loop++)
                    {
                        // wait response with 20ms TMO
                        *w_sram++ = (INT16U)OSMboxPend(channel[ch_global.param[1]].mbox_rx_data, TMO_VME_BUF_MS * TICK_FREQ_KHZ,
                                                       &cmd_err);

                        if (cmd_err != OS_NO_ERR)
                        {
                            cmd_err = CMD_TMO;    // do nothing
#ifdef ERROR_REPORT

                            OSQPost(term.tx.msgq, "[Global Cmd] Ack Timeout during programming\r\n");
#endif
                        }
                    }

                    channel[ch_global.param[1]].link = MPC_LINK_UNUSED;
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            default: cmd_err   = UNKNOWN_CMD;
                break;
        }

        if (cmd_err == NO_ERR)
        {
            cmd_state = MPC_CMD_STATE_READY;
        }
        else
        {
            FlashLedWrn();
            ErrCpy(ws_sram, error_list[cmd_err]);
            cmd_state = MPC_CMD_STATE_ERROR;
#ifdef ERROR_REPORT
            OSQPost(term.tx.msgq, "[Global Cmd] ");
            OSQPost(term.tx.msgq, error_list[cmd_err]);
            OSQPost(term.tx.msgq, "\r\n");
#endif
        }

        *(CMD_STATE + idx) = cmd_state;
        debug.task_cnt.ch_global ++;
    }
}

void ChanSystemTsk(void * pdata)
{
    INT8U cmd_err, cmd_state, err;
    INT16U * w_sram;
    static INT16U * ws_sram;
    INT16U * r_adc;
    static INT8U idx;
    INT8U i;
    union longword data;
#if DEBUG_CHAN>0
    INT8U    lterm_out[100];
#endif

    idx = MPC_SYSTEM_CHANNEL;
    // avoid to have complex operation inside the task
    ws_sram = (INT16U *)(DATAS_DEST____BASE_ADDRESS + (INT32U)idx * (INT32U) MPC_DATA_SIZE_B);
    *ws_sram = 0;    // no response expected by default
    *(CMD_STATE + idx) = MPC_CMD_STATE_READY;

    for (;;)
    {
        cmd_err   = NO_ERR;
        w_sram = ws_sram;

        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current taskk

        *w_sram = 0;     // no response expected by default
#if DEBUG_CHAN>0
        sprintf(lterm_out, "[System Cmd] Rx msg 0x%X/0x%X/0x%X/0x%X/0x%X/0x%X\r\n"
                , ch_system.cmd, ch_system.param[0], ch_system.param[1], ch_system.param[2], ch_system.param[3],
                ch_system.param[4]);
        OSQPost(term.tx.msgq, lterm_out);
#endif

        switch (ch_system.cmd)
        {
            case MPC_CMD_FPGA_VERSION  :     // get fpga version
                if (ch_system.param[0] == 1 && ch_system.param[1] <= 2)
                {
                    *w_sram++ = 1;

                    switch (ch_system.param[1])
                    {
                        case 0 : *w_sram = *UARTS_VERSION ;           // Read Uarts fpga version
                            break;

                        case 1 : *w_sram = *DAISY_VERSION ;           // Read Daisy fpga version
                            break;

                        case 2 : *w_sram = *VME_VERSION ;             // Read Vme fpga version
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_FPGA_RESET       :     // reset fpga
                if (ch_system.param[0] == 1 && ch_system.param[1] <= MPC_LINK_DAISY)
                {
                    switch (ch_system.param[1])
                    {
                        case 0 : *UARTS_VERSION = 0xFFFF;          // Reset Uarts_Irq interface
                            break;

                        case 1 : *DAISY_VERSION = 0xFFFF;          // Reset Daisy interface
                            break;

                        case 2 : *VME_VERSION = 0xFFFF;            // Reset Daisy interface
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_GET_ANA_DAISY    :     // Read the daisy analog values
                if (ch_system.param[0] == 1 && ch_system.param[1] < 160)
                {
                    *w_sram++ = 16 * ch_system.param[1];

                    while (ch_system.param[1]-- != 0)
                    {
                        ad_lsb;
                        start_ad_conv;
                        OSSemPend(analog.sem_adc, 5 * TICK_FREQ_KHZ, &err); // 5ms timeout
                        r_adc  = (INT16U *)0x380;

                        for (i = 0; i < 8; i++) { *w_sram++ = *r_adc++; }

                        ad_msb;
                        start_ad_conv;
                        OSSemPend(analog.sem_adc, 5 * TICK_FREQ_KHZ, &err); // 5ms timeout
                        r_adc  = (INT16U *)0x380;

                        for (i = 0; i < 8; i++) { *w_sram++ = *r_adc++; }

                        OSTimeDly(1 * TICK_FREQ_KHZ);       // wait 1ms ToDo what for?
                    }
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_HELP_LIST  :     // return the help list
                if (ch_system.param[0] == 0)
                {
                    ErrCpy(ws_sram, help_list);
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            case MPC_CMD_GET_STATE  :        // command state
                if (ch_system.param[0] == 1 && ch_system.param[1] <= MPC_NUM_CHANNELS)
                {
                    *w_sram++ = 1;
                    *w_sram++ = *(CMD_STATE + ch_system.param[1]);
                }
                else
                {
                    cmd_err   = ARG_OUT_OF_RANGE;
                }

                break;

            default :
                cmd_err   = UNKNOWN_CMD;
                break;
        }

        if (cmd_err == NO_ERR)
        {
            cmd_state = MPC_CMD_STATE_READY;
        }
        else
        {
            FlashLedWrn();
            ErrCpy(ws_sram, error_list[cmd_err]);
            cmd_state = MPC_CMD_STATE_ERROR;
#ifdef ERROR_REPORT
            OSQPost(term.tx.msgq, "[System Cmd] ");
            OSQPost(term.tx.msgq, error_list[cmd_err]);
            OSQPost(term.tx.msgq, "\r\n");
#endif
        }

        *(CMD_STATE + idx) = cmd_state;

        debug.task_cnt.ch_system ++;

    }
}

// EOF
