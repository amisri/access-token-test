/*---------------------------------------------------------------------------------------------------------*\
 File:    Test.c

 Purpose:  MPC program -

 Author:  Alexandre FRassier

 History:
    12/05/08  af  Created
\*---------------------------------------------------------------------------------------------------------*/

#define TEST_GLOBALS


#include <test.h>
#include <task.h>


void TestTsk(void * data)
{
    for (;;)
    {

        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task
        debug.task_cnt.test ++;
    }
}

void PrintTsk(void * data)
{

    for (;;)
    {
        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task
        debug.task_cnt.print ++;
    }
}
