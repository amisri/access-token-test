/*---------------------------------------------------------------------------------------------------------*\
 File:      terminal.c

 Purpose:   MPC program - Emulate Terminal VT100


 Author:    Alexandre FRassier

 History:
    12/05/08   af  Created
    16/06/08   add C83 lib VT100/ANSI terminal support functions (qak)
\*---------------------------------------------------------------------------------------------------------*/

#define TERMINAL_GLOBALS


#include <terminal.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "mugef/mpc_cmd.h"
#include <command.h>
#include <channel.h>
#include <memory_map.h>
#include <sfr87_144.h>
#include <task.h>
#include <debug.h>
#include <main.h>

void     TermInitLineEditor(void);
INT16U   TermLE0(INT8U ch);
INT16U   TermLE1(INT8U ch);
INT16U   TermLE2(INT16U ch);
INT16U   TermLE3(INT16U ch);
INT16U   TermLE4(INT16U ch);
INT16U   TermLE5(INT16U ch);
void     TermCursorLeft(void);
void     TermCursorRight(void);
void     TermInsertChar(INT16U ch);
void     TermSendCmd(void);
void     TermDeleteLeft(void);

static INT16U(* const edit_func[])(INT16U) =       // Line editor functions
{
    TermLE0,            // State 0 function (Idle)
    TermLE1,            // State 1 function (Line editor)
    TermLE2,            // State 2 function (ESC pressed)
    TermLE3,            // State 3 function (Cursor key or function key)
    TermLE4,            // State 4 function (Function key)
    TermLE5,            // State 5 function (PF1-PF4)
};

struct term_vars
{
    INT16U line_idx;        // Line editor buffer index
    INT16U line_end;        // Line editor end of buffer contents index
    INT16U edit_state;      // Line editor state machine
};

struct term_vars  local_term;      // Term variables structure


void TermRxTsk(void * data)
{
    INT8U ch, err;

    for (;;)
    {
        ch = (INT8U) OSQPend(term.rx.msgq, 0, &err);    // waiting for a message from Irq uart0_rx

        local_term.edit_state = edit_func[local_term.edit_state](ch);        // state machine to interpret character

        OSQPost(term.tx.msgq, std_out);

        debug.task_cnt.terminal_rx ++;
    }
}

void TermTxTsk(void * data)
{
    INT8U err;
    INT8U * msgq ;

    for (;;)
    {
        msgq = OSQPend(term.tx.msgq, 0, &err);

        while (*msgq != 0x00)
        {
            while (ti_u0c1 == 0) {}

            u0tbl = *msgq++;
        }

        debug.task_cnt.terminal_tx ++;
    }
}

void TermPrint(void)
{
    INT16U i = 0;

    while (std_out[i] != 0x00)
    {
        while (ti_u0c1 == 0) {}

        u0tbl = std_out [i];
        std_out[i++] = 0x00;
    }

    std_out[0] = 0x00;

}

/*---------------------------------------------------------------------------------------------------------*/
void TermInitLineEditor(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE0() if ESC is pressed once, or from TermLE2() if ECS is pressed for
  a second time.  The function initialises the ANSI/VT100 terminal connected to UART0.

  Set up string uses the following ANSI/VT100 escape codes (ESC='\33'):

     ESC c    Reset terminal to default settings
     ESC [?7h    Enable line wrap
\*---------------------------------------------------------------------------------------------------------*/
{

    local_term.edit_state = 1;        // Enable line editor

    local_term.line_idx = 0;          // Reset line buffer cursor index
    local_term.line_end = 0;          // Reset line buffer end index

    sprintf(std_out, "\33c\33[?7h\a MPC Program Test\r\n>");
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE0(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 0. In this state, output to the terminal is
  suppressed and the only character that is recognised is ESC, which starts the line editor.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ch == 0x1B)               // If character is ESC
    {
        TermInitLineEditor();      // Reset line editor and display the prompt
        return (1);                // Start line editor
    }

    return (0);
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE1(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 1. In this state, the character is analysed
  directly.  If the ESC code (0x1B) is received, the state changes to 1, otherwise the character is
  processed and the state remains 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)               // Switch according to the key pressed
    {
        case 0x08:             // [Backspace]  Delete left

        case 0x7F:  TermDeleteLeft();  break; // [Delete]  Delete left

        case 0x0D:  TermSendCmd();    break;  // [Return]  Send command

        case 0x1A:  return (0);    break;     // [CTRL-Z]  Line editor off

        case 0x1B:  return (2);    break;     // [ESC]   Start escape sequence

        default:  TermInsertChar(ch);  break; // [others]  Insert character
    }

    return (1);         // Continue with edit state 1
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE2(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 2. The previous character was [ESC].
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)         // Switch according to next code
    {
        case 0x1B: TermInitLineEditor();  return (1); // [ESC]  Restart editor

        case 0x5B:        return (3); // [Cursor/Fxx]  Change state to analyse

        case 0x4F:        return (5); // [PF1-4]  Change state to ignore
    }

    return (1);         // All other characters - return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE3(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 3. The key was either a cursor key or a function
  key.  Cursors keys have the code sequence "ESC[A" to "ESC[D", while function keys have the code
  sequence "ESC[???~" where ??? is a variable number of alternative codes.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)         // Switch according to next code
    {
        case 0x41:        return (1); // [Up]    Previous history line

        case 0x42:        return (1); // [Down]  Next history line

        case 0x43:  TermCursorRight();  return (1); // [Right]  Move cursor right

        case 0x44:  TermCursorLeft();  return (1); // [Left]  Move cursor left
    }

    return (4);         // Function key - change to edit state 4
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE4(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 4.  Orignial Key was a Function with sequence
  terminated by 0x7E (~).  However, the function will also accept a new [ESC] to allow an escape route
  in case of corrupted reception.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)         // Switch according to next code
    {
        case 0x1B:        return (1); // [ESC]  Escape to edit state 1

        case 0x7E:        return (1); // [~]    Sequence complete - return to state 1
    }

    return (4);          // No change to edit state
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE5(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 5.  The original key was PF1-4 and one more
  character must be ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (1);         // Return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor left key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!local_term.line_idx)     // If cursor is already at the start of the line
    {
        sprintf(std_out, "\a");        // Just beep
    }
    else        // else
    {
        sprintf(std_out, "\b");        // Move cursor left one character
        local_term.line_idx--;        // Decrement cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor right key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (local_term.line_idx == local_term.line_end)   // If cursor is already at the end of the line
    {
        sprintf(std_out, "\a");        // Just beep
    }
    else          // else
    {
        sprintf(std_out, "\33[C");    // Move cursor right one character
        local_term.line_idx++;      // Increment cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermInsertChar(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if a standard character has been received.  It will try to enter
  the character into the current line under the current cursor position.  The rest of the line will be
  moved to the right to may space for the new character.  If the line is full, the bell will ring.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    if (ch >= 0x20 && ch < 0x80)    // If character is printable 7-bit ASCII symbol
    {
        if (local_term.line_end >= (SRMAX - 2)) // If line buffer is full...
        {
            sprintf(std_out, "\a");        // Just beep
        }
        else          // else
        {

            for (i = local_term.line_end++; i > local_term.line_idx; i--) // For the rest of the line (backwards)
            {
                std_in[i] = std_in[i - 1];  // Shift characters one column in buffer
            }

            std_in[local_term.line_idx ++] = ch;     // Save new character in line buffer

            std_in[local_term.line_end] = '\0';    // Nul terminate line buffer

            if ((i = local_term.line_end - local_term.line_idx) != 0) // If cursor is now offset from true position
            {
                sprintf(std_out, "%s\33[%uD", &std_in[local_term.line_idx - 1],
                        i);    // Move cursor the required number of columns left
            }

            sprintf(std_out, "%c", ch);   // Output chars to screen

        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermSendCmd(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if Enter or Return been received.  It will send the command to
  be executed by the communication task.
\*---------------------------------------------------------------------------------------------------------*/
{
    std_in[local_term.line_end] = '\0';    // Nul terminate line buffer

    sprintf(std_out, "\r\n");

    OSTaskResume(TERMINAL_CMD_TSK_PRI);       // Send command to com task

    local_term.line_idx = local_term.line_end = 0;    // Reset cursor and end of line indexes

    //  strcat (std_out,"\r\n");

    //  strcat (std_out,">");

}

/*---------------------------------------------------------------------------------------------------------*/
void TermDeleteLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the backspace or delete keys have been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    if (!local_term.line_idx)       // If cursor is already at the start of the line
    {
        sprintf(std_out, "\a");      // Just beep
    }
    else          // else
    {
        local_term.line_idx--;      // Adjust cursor position index

        for (i = local_term.line_idx; i < local_term.line_end; i++) // For the remainder of the line
        {
            std_in[i] = std_in[i + 1];  // Shift character in buffer
        }

        std_in[local_term.line_end--] = '\0';    // Nul terminate line buffer

        sprintf(std_out, "\b%s \33[%uD", &std_in[local_term.line_idx],
                (1 + local_term.line_end - local_term.line_idx)); // Move cursor the required number of columns left
    }
}

// EOF

