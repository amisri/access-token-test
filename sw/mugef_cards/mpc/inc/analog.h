/*!
 *  @file       analog.h
 *  @defgroup   MUGEF MPC
 *  @brief      Analog is here only one ADC, used for daisy chain only
 *
 *  Description    : ToDo
 */

#ifndef ANALOG_H
#define ANALOG_H

#ifdef ANALOG_GLOBALS
#define ANALOG_VARS_EXT
#else
#define ANALOG_VARS_EXT extern
#endif

// Includes

#include <ucos_ii.h>
#include <mugef/mpc_cmd.h>
#include <debug.h>

// Constants

// ToDo use inline instead of macros
#define  ad_lsb         aps0_ad0con2=0;
#define  ad_msb         aps0_ad0con2=1;
#define  stop_ad_conv   adst_ad0con0=0;
#define  start_ad_conv  adst_ad0con0=1;
#define  ad_conv_state  adst_ad0con0

// External structures, unions and enumerations

/*! Analogue global variables */
struct analog
{
    OS_EVENT * sem_adc;         /*!< Synchronise end of conversion read out of ADC values */
};

// External function declarations

// External variable definitions

/*! Analogue global variables */
ANALOG_VARS_EXT struct analog analog;

// Inline function declarations

/*!
* Interrupt used at end of ADC conversion
*/
inline void AnaConvertEndIrq(void)
{
    OSSemPost(analog.sem_adc);
    debug.irq_cnt.ad_conv ++;
}

#endif  // ANALOG_H end of header encapsulation

// EOF

