/*!
 *  @file       interrupts.h
 *  @defgroup   MUGEF MPC
 *  @brief      All interrupts with corresponding pragma
 *
 *  Description    : see start.a30 to see to which vector the functions below correspond to
 */

#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#ifdef INTERRUPTS_GLOBALS
#define INTERRUPTS_VARS_EXT
#else
#define INTERRUPTS_VARS_EXT extern
#endif

// Includes

// Constants

// External structures, unions and enumerations

// External function declarations

/*!
 * This interrupt function is triggered when the transmission Txd0 is completed
 * Not used.
 */
void    Uart0txInt(void);
#pragma INTERRUPT /B Uart0txInt

/*!
 * This interrupt function is triggered when the reception Rxd0 is completed
 * It is used for terminal RX
 */
void    Uart0rxInt(void);
#pragma INTERRUPT /B Uart0rxInt

/*!
 * This interrupt function is triggered when a reception from Rx is completed.
 * Not used.
 */
void    Uart4rxInt(void);
#pragma INTERRUPT /B Uart4rxInt

/*!
 * This interrupt function is triggered when the transmission Txd4 is completed
 * Not used.
 */
void    Uart4txInt(void);
#pragma INTERRUPT /B Uart4txInt

/*!
 * This interrupt function is triggered when a falling edge appears on Int1 external pin
 * It is triggered by a VME event
 */
void    Int1(void);
#pragma INTERRUPT /B Int1

/*!
 * This interrupt function is triggered when a falling edge appears on Int2 external pin
 * It deals with daisy chain communication.
 */
void    Int2(void);
#pragma INTERRUPT /B Int2

/*!
 * This interrupt function is triggered when a sequence of A/D conversion is completed.
 */
void    AdConvert(void);
#pragma INTERRUPT /B AdConvert

// External variable definitions


#endif  // INTERRUPTS_H end of header encapsulation

// EOF

