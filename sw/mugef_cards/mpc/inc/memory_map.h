/*!
 *  @file     memory_map.h
 *  @defgroup MUGEF MPC
 *  @brief
 *
 *  Description    :
 */

#ifndef MEMORY_MAP_H
#define MEMORY_MAP_H


/* MPC peripherals  Base Address */
#define UARTS_________BASE_ADDRESS     0x200002
#define UARTS_IDX_____BASE_ADDRESS     0x200082
#define UARTS_IRQ_____BASE_ADDRESS     0x20008A
#define UARTS_SHD_____BASE_ADDRESS     0x20008C

#define DAISY_LINES___BASE_ADDRESS     0x010002
#define DAISY_IDX_____BASE_ADDRESS     0x010082
#define DAISY_IRQ_____BASE_ADDRESS     0x01008A

#define DAISY_SWITCHS_BASE_ADDRESS     0x01008C

#define VME___________BASE_ADDRESS     0xC00000
#define VME_MPC_VER_BASE_ADDRESS       0xC00002
#define VME_CMD_______BASE_ADDRESS     0xC00004
#define VME_ACK_IDX___BASE_ADDRESS     0xC00088
#define VME_UTC_______BASE_ADDRESS     0xC00092
#define VME_US________BASE_ADDRESS     0xC00096
#define VME_CH_POS____BASE_ADDRESS     0xC00100
#define VME_PARAM_____BASE_ADDRESS     0xC00200
#define VME_BUFFER____BASE_ADDRESS     0xC10000
#define CURRENTSTATUS_BASE_ADDRESS     0xC20000
#define MPC_RW_SRAM___BASE_ADDRESS     0XC20000
#define CMD_STATE_____BASE_ADDRESS     0XC20200
#define DATAS_DEST____BASE_ADDRESS     0xC20400    // read and write -> CFFFFF   (458k x 16)


/* FPGA Version Number */
#define UARTS_VER   0x200000
//#define VME___VER   0xC00086
#define VME___VER   0xC00000
#define DAISY_VER   0x010000

/*----- PLD MAPPING -----*/
/*----- Uarts_Irq / external interrupts 0 -----*/
#define UARTS           ((INT16U *) UARTS_________BASE_ADDRESS)
#define UARTS_VERSION   ((INT16U *) UARTS_VER)
#define UARTS_SHD       ((INT16U *) UARTS_SHD_____BASE_ADDRESS)

/*----- Vme / external interrupts 1 -----*/
#define VME_BUFF        ((INT16U *) VME_BUFFER____BASE_ADDRESS)
#define VME_VERSION     ((INT16U *) VME___VER)
#define CHAN_LINK       ((INT16U *) VME_CH_POS____BASE_ADDRESS) // just used for vme link set
#define MPC_VERSION     ((INT16U *) VME_MPC_VER_BASE_ADDRESS)
#define CMD_STATE       ((INT16U *) CMD_STATE_____BASE_ADDRESS)
#define CURRENT_STATUS  ((INT16U *) CURRENTSTATUS_BASE_ADDRESS)
#define VME_ACK_IDX     ((INT16U *) VME_ACK_IDX___BASE_ADDRESS)
#define VME_CMD_ACK     ((INT16U *) VME_CMD_______BASE_ADDRESS)
#define VME_PARAM       ((INT16U *) VME_PARAM_____BASE_ADDRESS)


/*----- Daisy / external interrupts 2 -----*/
#define PLD_DAISY           ((INT16U *) DAISY_LINES___BASE_ADDRESS)
#define PLD_DAISY_CH_IDX    ((INT16U *) DAISY_IRQ_____BASE_ADDRESS)
#define PLD_CH_LINK         ((INT16U *) DAISY_SWITCHS_BASE_ADDRESS) // actually set chan link type
#define DAISY_VERSION       ((INT16U *) DAISY_VER)

#endif  // MEMORY_MAP_H end of header encapsulation

// EOF
