/*!
 *  @file       channel.h
 *  @defgroup   MUGEF MPC
 *  @brief      Process the commands for all different channels.
 *              Process here for channel 64 and 65
 *              Forward to PLC (over serial link or daisy chain) for physical channel
 *
 *  Description    :
 */

#ifndef CHANNEL_H
#define CHANNEL_H

#ifdef CHANNEL_GLOBALS
#define CHANNEL_VARS_EXT
#else
#define CHANNEL_VARS_EXT extern
#endif

// Includes

#include <mugef/mpc_cmd.h>
#include <pmsgq.h>

// Constants

#define MPC_DATA_SIZE_B     (MPC_DATA_SIZE * 2) /*!< Channel buffer size available for each channel in bytes */

#define TMO_DAISY_MS        1300        /*!< Timeout waiting for slave, with daisy chain. Status is read each 1,3s */
#define TMO_VME_BUF_MS      20          /*!< Shorter timeout waiting for slave, when programming with serial link */
#define RELAY_DELAY_MS      50          /*!< Delay after moving the relays*/

// Not in mpc.h!!! ToDo put in mpc.h ?
#define GLB_CMD_SET_1_SHD_CH        0x0006      /*!< Shutdown one particular channel*/
#define GLB_CMD_GET_ALL_SHD_CH      0x0007      /*!< Get shutdown state for all channel */
#define GLB_CMD_READ_BUFF           0x001F      /*!< Read channel buffer */


/*----- Channel constants -----*/
// ToDo review if we remove MPC_LINK_SERIAL_PROG (not needed)
#define MPC_LINK_SERIAL_PROG   3     // serial prog mode

/*! List of communication errors ToDo enum*/
#define  NO_ERR            0
#define  INVALID_ARG       1
#define  INVALID_CH        2
#define  UNKNOWN_CMD       3
#define  ARG_OUT_OF_RANGE  4
#define  CMD_TMO           5
#define  TASK_BUSY         6
#define  CMD_NACK          7
#define  IPC_FAIL          8
#define  LINK_NOT_USED     9
//#define  PROCESSING_TEST  20

// External structures, unions and enumerations

/* !
 * channel data
 */
struct channel_data
{
    INT16U   idx;
    INT16U   data;
};

/* !
 * Command Variables for channels from 0 to 63
 */
struct physical_channel
{
    OS_EVENT * mbox_rx_data;    /*!< Message box for data coming from network (serial or daisy)*/
    INT8U link;                 /*!< Link type */
    PMSGQ pmsgq;                /*!< Priority message queue for incoming commands*/
} ;

/* !
 * Command Variables for virtual channels (64 and 65)
 */
struct virtual_channel
{
    INT16U cmd;                     /*!< Command number */
    INT16U param[MPC_NUM_PARMS];    /*!< List of parameters */
};

// External function declarations

/* !
 * Task managing for one channel over serial link or daisy chain
 * This task is spawned for each physical channel\
 *
 * @param pdata channel number
 */
void ChanPhysicalTsk(void * pdata);

/* !
 * Task processing the commands related to the configuration of the channels (switches...)
 * This task is seen as the channel number 64
 *
 * @param pdata unused
 */
void ChanGlobalTsk(void * pdata);

/* !
 * Task processing the commands related to the MPC card state (version, reset...)
 * This task is seen as the channel number 65
 *
 * @param pdata unused
 */
void ChanSystemTsk(void * pdata);

// External variable definitions

/*! Global channel global variables */
CHANNEL_VARS_EXT struct virtual_channel ch_global;

/*! System channel global variables */
CHANNEL_VARS_EXT struct virtual_channel ch_system;

/*! Physical channel global variables */
CHANNEL_VARS_EXT struct physical_channel channel[MUGEF_MAX_CHANNELS];

/*! Constants string error handling */
CHANNEL_VARS_EXT INT8U * const error_list[]
#ifdef CHANNEL_GLOBALS
=
{
    "No Error", "Invalid number of arguments", "Channel out of range", "Unknown command",
    "Argument out of range", "Serial link time out", "Task busy", "Serial link No Ack",
    "Inter Process Communication error", "Link not used"
}
#endif
;

#endif  // CHANNEL_H end of header encapsulation

// EOF

