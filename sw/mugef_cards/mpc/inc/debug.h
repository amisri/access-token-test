/*!
 *  @file     debug.h
 *  @defgroup MUGEF MPC
 *  @brief
 *
 *  Description    : everything related to debug, which might be essential...
 */

#ifndef DEBUG_H
#define DEBUG_H

#ifdef DEBUG_GLOBALS
#define DEBUG_VARS_EXT
#else
#define DEBUG_VARS_EXT extern
#endif

// Includes

#include <ucos_ii.h>
#include <mugef/consts.h>

// Constants

// Be careful using debug, check RAM size is less than 0xC3FF
// ToDo check RAM overflow in linker!
#define DEBUG_CHAN   0      /*! Debug level for channel task */
#define DEBUG_CMD    0      /*! Debug level for command task */
#define DEBUG_DAISY  0      /*! Debug level for daisy task */

// #define TASK_CHECK  // need OS_TASK_CREATE_EXT_EN
#define ERROR_REPORT        /*! Toggle error reporting on terminal */

// Test points
#define  PT17        p11_0
#define  PT18        p11_1
#define  PT19        p11_2
#define  PT20        p11_3

// External structures, unions and enumerations

/* !
 * Container for all data related to debug and statistics
 */
struct debug
{
    struct
    {
        // ToDo increase debug info here
        INT32U irq_vme_msg_err;
        INT32U ch_err[MUGEF_MAX_CHANNELS];
    } error_cnt;                                /*!< # of errors */
    struct
    {
        INT32U terminal_rx;
        INT32U terminal_tx;
        INT32U terminal_cmd;
        INT32U cmd_scd;
        INT32U daisy_scd;
        INT32U test;
        INT32U print;
        INT32U ch_global;
        INT32U ch_system;
        INT32U ch_snd[MUGEF_MAX_CHANNELS];
    } task_cnt;                                 /*!< # of time the task has been executed */
    struct
    {
        INT32U daisy;
        INT32U vme;
        INT32U term_rx;
        INT32U ad_conv;
    } irq_cnt;                                  /*!< # of time the interrupt has been executed */
};

// External function declarations

// External variable definitions

/* ! Container for all data related to debug and statistics */
DEBUG_VARS_EXT struct debug debug;

#endif  // DEBUG_H end of header encapsulation

// EOF

