/*!
 *  @file     interrupts.h
 *  @defgroup MUGEF MPC
 *  @brief
 *
 *  Description    :
 */

#ifndef TEST_H
#define TEST_H

#ifdef TEST_GLOBALS
#define TEST_VARS_EXT
#else
#define TEST_VARS_EXT extern
#endif

// Includes

// Constants

// External structures, unions and enumerations

// External function declarations

/*!
 * This task does nothing. ToDo to be removed
 *
 * @param pdata unused
 */
void  TestTsk(void * pdata);

/*!
 * This task does nothing. ToDo to be removed
 *
 * @param data unused
 */
void  PrintTsk(void * data);

// External variable definitions


#endif  // TEST_H end of header encapsulation

// EOF

