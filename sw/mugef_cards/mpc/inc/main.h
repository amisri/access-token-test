/*---------------------------------------------------------------------------------------------------------*\
  File:     main.h

  Contents: Multi PowerConverters Controller (MPC) program  -
            this file declares/defines all includes files used by MPC program
  History:

    18/08/05  af  Created
\*---------------------------------------------------------------------------------------------------------*/
#ifndef MAIN_H
#define MAIN_H

#ifdef MPC_GLOBALS
#define MPC_EXT
#else
#define MPC_EXT extern
#endif

// Includes

#include <ucos_ii.h>

// Constants

//#define MPC_VER         4              /* 11 november 2009 */
#define MPC_VER         6           /*5 Increase the number of parameters (5->255) and delete tsk_tst */

#define TICK_FREQ_KHZ   1           /*!< Tick frequency. See timer A0 settings */

#define FALSE           0
#define TRUE            1

// External structures, unions and enumerations

// ToDo use big/little endian FGC headers
union word
{
    struct
    {
        BOOLEAN    b0: 1;
        BOOLEAN    b1: 1;
        BOOLEAN    b2: 1;
        BOOLEAN    b3: 1;
        BOOLEAN    b4: 1;
        BOOLEAN    b5: 1;
        BOOLEAN    b6: 1;
        BOOLEAN    b7: 1;
        BOOLEAN    b8: 1;
        BOOLEAN    b9: 1;
        BOOLEAN    b10: 1;
        BOOLEAN    b11: 1;
        BOOLEAN    b12: 1;
        BOOLEAN    b13: 1;
        BOOLEAN    b14: 1;
        BOOLEAN    b15: 1;
    } bit;
    struct
    {
        INT8U    low;                /* Low  8 bit */
        INT8U    high;               /* High 8 bit */
    } byte;
    INT16U  word;
};

union longword
{
    INT8U    bytes [4];
    INT16U   word  [2];
    FP32     fv;
    INT32U   longword;
};


// External function declarations

void  ErrCpy(INT16U * dst, INT8U * src);
void  ErrCpyInv(INT8U * dst, INT16U * src);

// External variable definitions

#endif // MAIN_H

/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
