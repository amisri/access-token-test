/*!
 *  @file     task.h
 *  @defgroup MUGEF MPC
 *  @brief
 *
 *  Description    :
 */

#ifndef TASK_H
#define TASK_H

#ifdef TASK_GLOBALS
#define TASK_VARS_EXT
#else
#define TASK_VARS_EXT extern
#endif

// Includes

#include <mugef/mpc_cmd.h>
#include <debug.h>


// Constants

/*! Task Priorities - used as Task Identifiers */
#define  CH_SND_TSK_PRI            1     // task reserved until MUGEF_MAX_CHANNELS
#define  CH_GLOBAL_TSK_PRI         65    // must following CH_SND_TSK_PRI + MUGEF_MUGEF_MAX_CHANNELS
#define  CH_SYSTEM_TSK_PRI         66    // must following CH_GLOBAL_TSK_PRI
#define  DAISY_SCD_TSK_PRI         67    // must be lower ch_snd task
#define  CMD_SCD_TSK_PRI           68
#define  STS_POLL_TSK_PRI          69
#define  TERMINAL_TX_TSK_PRI       70
#define  LED_EVT_TSK_PRI           71
#define  LED_CPUOK_TSK_PRI         72
#define  TERMINAL_RX_TSK_PRI       73
#define  PRINT_TSK_PRI             74
#define  TERMINAL_CMD_TSK_PRI      75
#define  LED_WRN_TSK_PRI           76
#define  LED_FLT_TSK_PRI           77
#define  TST_TSK_PRI               78

/*! Task Stack sizes (words) */
#if DEBUG_CHAN>0 || DEBUG_CMD>0 || DEBUG_DAISY>0
#define  CH_SND_STK_SIZE         192
#define  CH_GLOBAL_STK_SIZE      192
#define  CH_SYSTEM_STK_SIZE      192
#define  DAISY_SCD_STK_SIZE      128
#define  CMD_SCD_STK_SIZE        160
#define  TERMINAL_TX_STK_SIZE    112
#define  LED_EVT_STK_SIZE        112
#define  LED_CPUOK_STK_SIZE       80
#define  TERMINAL_RX_STK_SIZE    160
#define  PRINT_STK_SIZE           64
#define  TERMINAL_CMD_STK_SIZE   192
#define  LED_WRN_STK_SIZE         96
#define  LED_FLT_STK_SIZE         96
#define  TST_STK_SIZE             64
#else
#define  CH_SND_STK_SIZE         176
#define  CH_GLOBAL_STK_SIZE      100
#define  CH_SYSTEM_STK_SIZE      100
#define  DAISY_SCD_STK_SIZE      128
#define  CMD_SCD_STK_SIZE        160
#define  TERMINAL_TX_STK_SIZE    100
#define  LED_EVT_STK_SIZE        112
#define  LED_CPUOK_STK_SIZE       80
#define  TERMINAL_RX_STK_SIZE    160
#define  PRINT_STK_SIZE           64
#define  TERMINAL_CMD_STK_SIZE   192
#define  LED_WRN_STK_SIZE         96
#define  LED_FLT_STK_SIZE         96
#define  TST_STK_SIZE             64
#endif

#ifdef TASK_CHECK
TASK_VARS_EXT OS_STK_DATA Chs_Snd_Tsk_pext[CH_SND_STK_SIZE];
TASK_VARS_EXT OS_STK_DATA Led_Evt_pext;
TASK_VARS_EXT OS_STK_DATA Led_Wrn_pext;
TASK_VARS_EXT OS_STK_DATA Led_Fld_pext;
TASK_VARS_EXT OS_STK_DATA Led_CpuOk_pext;
TASK_VARS_EXT OS_STK_DATA Terminal_Rx_pext;
TASK_VARS_EXT OS_STK_DATA Terminal_Tx_pext;
TASK_VARS_EXT OS_STK_DATA Terminal_Cmd_pext;
TASK_VARS_EXT OS_STK_DATA Cmd_Scd_pext;
TASK_VARS_EXT OS_STK_DATA Daisy_Scd_pext;
TASK_VARS_EXT OS_STK_DATA Ch_Global_pext;
TASK_VARS_EXT OS_STK_DATA Ch_System_pext;
TASK_VARS_EXT OS_STK_DATA Print_pext;
TASK_VARS_EXT OS_STK_DATA Tst_pext;
#endif

// External structures, unions and enumerations

/*! Task stacks */
struct tsk_stk
{
    OS_STK      led_evt[LED_EVT_STK_SIZE];
    OS_STK      led_wrn[LED_WRN_STK_SIZE];
    OS_STK      led_flt[LED_FLT_STK_SIZE];
    OS_STK      led_cpuok[LED_CPUOK_STK_SIZE];
    OS_STK      channel[MUGEF_MAX_CHANNELS][CH_SND_STK_SIZE];
    OS_STK      global_ch[CH_GLOBAL_STK_SIZE];
    OS_STK      system_ch[CH_GLOBAL_STK_SIZE];
    OS_STK      command[CMD_SCD_STK_SIZE];
    OS_STK      daisy[DAISY_SCD_STK_SIZE];
    OS_STK      term_cmd[TERMINAL_CMD_STK_SIZE];
    OS_STK      term_rx[TERMINAL_RX_STK_SIZE];
    OS_STK      term_tx[TERMINAL_TX_STK_SIZE];
    OS_STK      test[TST_STK_SIZE];
    OS_STK      print[PRINT_STK_SIZE];
};

// External variable definitions

/*! Task stacks */
TASK_VARS_EXT struct tsk_stk tsk_stk;

#endif  // TASK_H end of header encapsulation

// EOF

