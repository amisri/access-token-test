/*!
 *  @file       terminal.h
 *  @defgroup   MUGEF MPC
 *  @brief      Emulate Terminal VT100
 *
 *  Description    :
 */

#ifndef TERMINAL_H
#define TERMINAL_H

#ifdef TERMINAL_GLOBALS
#define TERMINAL_VARS_EXT
#else
#define TERMINAL_VARS_EXT extern
#endif

// Includes

#include <ucos_ii.h>
#include <debug.h>
#include <sfr87_144.h>

// Constants

/* Terminal in/out Array size */
#define SRMAX           50          /*!< stdin buffer size*/
#define STMAX           100         /*!< stdout buffer size*/
#define TERM_OUT_MAX    32          /*!< terminal outcoming message queue size*/
#define TERM_IN_MAX     20          /*!< terminal incoming message queue size*/


// External structures, unions and enumerations

/*!
 * terminal global variables
 */
struct terminal
{
    struct
    {
        OS_EVENT * msgq;            /*!< Message queue for outcoming communication */
        void * msg[TERM_OUT_MAX];   /*!< Message queue buffer */
    } tx;

    struct
    {
        OS_EVENT * msgq;            /*!< Message queue for incoming communication */
        void * msg[TERM_IN_MAX];   /*!< Message queue buffer */
    } rx;
};



// External function declarations

/*!
 * This task receives the characters from the terminal, process it according to the terminal state
 * and echo the character on the terminal
 *
 * @param data unused
 */
void TermRxTsk(void * data);

/*!
 * This task is waiting for a message queue. When a string queue appear, the string
 * char is transmitting to the uart0.
 *
 * @param data unused
 */
void TermTxTsk(void * data);

/*!
 * This function is used at the beginning of the program, before uCOS II is started.
 */
void TermPrint(void);

// External variable definitions

/*! Help string */
TERMINAL_VARS_EXT INT8U * const help_list
#ifdef TERMINAL_GLOBALS
    = "\r\ns g r t h\r\n\ts ch_idx cmd number_of_parameters p1 p2 p3 p4\r\n\tg ch_idx cmd\r\n\tr ch_idx\r\n\tt ch_idx tst_number pl_ni nb_of_loop\r\n\th\r\n\tCH0..63: Cvs Channels\r\n\tCH64: GLOBAL\r\n\tCH65: SYSTEM"
#endif
      ;

/*! Command state string */
TERMINAL_VARS_EXT INT8U * const cmd_progress[]
#ifdef TERMINAL_GLOBALS
=
{   "\r\nCommand in Progress.", "\r\nCommand Completed.", "\r\nCommand Error: ", "\r\nCommand tmo.", "\r\nUnknown."}
#endif
;

/*! standard input buffer */
TERMINAL_VARS_EXT INT8U std_in[SRMAX];

/*! standard output buffer */
TERMINAL_VARS_EXT INT8U std_out[STMAX];

/*! Terminal global variables */
TERMINAL_VARS_EXT struct terminal term;

#if DEBUG_CHAN>0
TERMINAL_VARS_EXT    INT8U term_out_idx;
TERMINAL_VARS_EXT    INT8U term_out[CHAN_TERM_OUT_MAX][70];
#endif

// Inline Function declarations

/*!
 * Interrupt called when a new character is received
 */
inline void TermRxIrq(void)
{
    OSQPost(term.rx.msgq, (void *)u0rb);   // send data to Terminal_rx task
    debug.irq_cnt.term_rx ++;
}
#endif  // TERMINAL_H end of header encapsulation

// EOF

