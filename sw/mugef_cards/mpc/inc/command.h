/*!
 *  @file       command.h
 *  @defgroup   MUGEF MPC
 *  @brief      Parse commands and forward them to appropriate "channel" task
 *
 *  Description    :
 */

#ifndef COMMAND_H
#define COMMAND_H

#ifdef COMMAND_GLOBALS
#define COMMAND_VARS_EXT
#else
#define COMMAND_VARS_EXT extern
#endif

// Includes

#include <mugef/mpc_cmd.h>
#include <ucos_ii.h>

// Constants

/*! List of sources for commands ToDo enum */
#define TERMINAL         0
#define VME              1
#define INTERNAL         2

#define MAX_CMD_MSG     96      /*!< Size for command message queue */
#define MAX_VME_MSG     60      /*!< Size for VME message queue */

// External structures, unions and enumerations

/*! Type definition of a command */
typedef struct
{
    INT16U cmd;                     /*!< command */
    INT16U param[MPC_NUM_PARMS];    /*!< list of parameters */
    INT8U chan_idx;                 /*!< Channel index */
    INT8U sender;                   /*!< Source of the command */
} command_t;


/*!
 *  Command global variables
 */
struct command_task
{
    OS_EVENT * msgq;            /*!< Send a command (from VME or Terminal) to command task */
    void * msg[MAX_VME_MSG];    /*!< Table for msgq */
};

// External function declarations

/*!
 * This tasks receives a message and forward it to the right task
 * Manage the channel state, and handshake with VME
 * Forward command to the right task, awake the given task
 * ToDo get rid of this task?
 *
 * @param data unused
 */
void CmdScdTsk(void * data);

/*!
 * This task will parse the commands received from the terminal.
 *
 * @param data unused
 */
void CmdTermTsk(void * data);

/*!
 * Pile up incoming commands from VME and forward them to command_task.
 *
 */
void CmdVmeIrq(void);

// External variable definitions

/*! Command global variables */
COMMAND_VARS_EXT struct command_task command_task;

// Inline Function declarations

#endif  // COMMAND_H end of header encapsulation

// EOF

