/*!
 *  @file     test.h
 *  @defgroup MUGEF NiAuxPs
 *  @brief
 *
 *  Description    : Quick implementation of Message Queue with priority, from 0 (high) to PMSQ_MAX_PRI-1
 */

#ifndef PMSGQ_H
#define PMSGQ_H

// Includes

#include <ucos_ii.h>

// Constants

#define  PMSQ_MAX_PRI 2
#define  PMSQ_MAX_MSG 3

// External structures, unions and enumerations

typedef struct pmsgq
{
    void * msg[PMSQ_MAX_PRI][PMSQ_MAX_MSG];
    INT8U start[PMSQ_MAX_PRI];
    INT8U end[PMSQ_MAX_PRI];
    INT8U isEmpty[PMSQ_MAX_PRI];
    OS_EVENT * sem;
} PMSGQ;

// External function declarations

// void *      PMSQAccept       (PMSGQ pq, INT8U *err);

/*!
 * Initialise the PMSQ
 *
 * @param pq pointer to priority message queue
 *
 * @Return a pointer to the priority message queue, NULL if failed
 */
OS_EVENT * PMSQInit(PMSGQ * pq);

// OS_EVENT *  PMSQDel          (PMSGQ pq, INT8U opt, INT8U *err);

// INT8U       PMSQFlush        (PMSGQ pq);

/*!
 * Pend on a PMSGQ with timeout (0 pends forever)
 *
 * @param pq pointer to priority message queue
 * @param timeout timeout in ticks
 * @param err pointer to error
 *
 * @Return a pointer to the message with highest priority
 */
void * PMSQPend(PMSGQ * pq, INT16U timeout, INT8U * err);

/*!
 * Post a message in the Q
 *
 * @param pq pointer to priority message queue
 * @param msg pointer to the message to post
 * @param priority for this message
 *
 * @Return OS error
 */
INT8U PMSQPost(PMSGQ * pq, void * msg, INT8U prio);

// INT8U       PMSQPostFront    (PMSGQ pq, void * msg);

// INT8U       PMSQPostOpt      (PMSGQ pq, void * msg, INT8U opt);

/*!
 * Check if the queue of given priority is empty
 *
 * @param pq pointer to priority message queue
 * @param priority to check
 *
 * @Return TRUE if empty
 */
INT8U PMSQIsEmpty(PMSGQ * pq, INT8U prio);

// External variable definitions

#endif //  PMSGQ_H

// EOF

