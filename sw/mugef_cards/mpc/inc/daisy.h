/*!
 *  @file       daisy.h
 *  @defgroup   MUGEF MPC
 *  @brief      Manage communication over the daisy chain
 *
 *  Description    :
 */

#ifndef DAISY_H
#define DAISY_H

#ifdef DAISY_GLOBALS
#define DAISY_VARS_EXT
#else
#define DAISY_VARS_EXT extern
#endif

// Includes

#include <ucos_ii.h>
#include <channel.h>
#include <leds.h>

// Constants

#define MAX_DAISY_MSG   30      /*!< Daisy message queue size */

// External structures, unions and enumerations

/*!
 *  daisy global variables
 */
struct daisy
{
    OS_EVENT * msgq;                            /*!< Send a command (from VME or Terminal) to command task */
    void * msg[MAX_DAISY_MSG];                  /*!< Table for msgq */
};

// External function declarations

/*!
 * This task receives the data (word + channel) and process it
 *
 * @param data unused
 */
void DaisyScdTsk(void * data);

/*!
 * Read incoming data from daisy channels. Send data to daisy task.
 */
void DaisyRxIrq(void);

// External variable definitions

/*! Daisy global variables declaration*/
DAISY_VARS_EXT struct daisy daisy;

// Inline Function declarations

#endif  // DAISY_H end of header encapsulation

// EOF

