/*!
 *  @file     leds.h
 *  @defgroup MUGEF
 *  @brief
 *
 *  Description    :
 */

#ifndef LEDS_H
#define LEDS_H

#ifdef LEDS_GLOBALS
#define LEDS_VARS_EXT
#else
#define LEDS_VARS_EXT extern
#endif

// Includes

#include <ucos_ii.h>
#include <sfr87_144.h>
#include <task.h>
#include <main.h>

// Constants

#define LED_ON      0
#define LED_OFF     1

#define LED_FAILED  p9_2
#define LED_WARNING p9_3
#define LED_EVT     p9_4
#define LED_ALIVE   p9_5

#define LED_EVT_FLASH_TIME_MS       30
#define LED_WRN_FLASH_TIME_MS       10      // UARTs activity?
#define LED_FLT_FLASH_TIME_MS       100     // Daisy activity?
#define LED_CPUOK_TOGGLE_TIME_MS    500

// External structures, unions and enumerations

// External function declarations

/*!
 * Task to manage the Event (green) Led
 */
void LedEvtTsk(void * data);

/*!
 * Task to manage the Warning (orange) Led
 */
void LedWrnTsk(void * data);

/*!
 * Task to manage the Failed (red) Led
 */
void LedFltTsk(void * data);

/*!
 * Task to manage the CPU Ok (green) Led
 */
void LedCpuOkTsk(void * data);

/*!
 * Task to initialise the leds
 */
void LedInit(void);

// External variable definitions

// External inline functions definitions

/*!
 * Macro to flash 1 time led Event
 */
inline void FlashLedEvt(void)
{
    OSTaskResume(LED_EVT_TSK_PRI);
}

/*!
 * Macro to flash 1 time led Warning
 */
inline void FlashLedWrn(void)
{
    OSTaskResume(LED_WRN_TSK_PRI);
}

/*!
 * Macro to flash 1 time led Failed
 */
inline void FlashLedFlt(void)
{
    OSTaskResume(LED_FLT_TSK_PRI);
}


#endif  // LEDS_H end of header encapsulation

// EOF

