/*
*********************************************************************************************************
*                                               uC/OS-II
*                                         The Real-Time Kernel
*
*
*                             (c) Copyright 1992-2007, Micrium, Weston, FL
*                                          All Rights Reserved
*
*                                           Renesas M32C Port
*
* File      : os_cpu.h
* Version   : V1.00
* By        : Jean J. Labrosse
*
* For       : Renesas M32C
* Toolchain : IAR's EW for M32C
*********************************************************************************************************
*/

#ifdef  OS_CPU_GLOBALS
#define OS_CPU_EXT
#else
#define OS_CPU_EXT  extern
#endif

/*
*********************************************************************************************************
*                                             DATA TYPES
*                                         (Compiler Specific)
*********************************************************************************************************
*/

typedef unsigned char  BOOLEAN;
typedef unsigned char  INT8U;                                   /* Unsigned  8 bit quantity            */
typedef signed   char  INT8S;                                   /* Signed    8 bit quantity            */
typedef unsigned int   INT16U;                                  /* Unsigned 16 bit quantity            */
typedef signed   int   INT16S;                                  /* Signed   16 bit quantity            */
typedef unsigned long  INT32U;                                  /* Unsigned 32 bit quantity            */
typedef signed   long  INT32S;                                  /* Signed   32 bit quantity            */
typedef float          FP32;                                    /* Single precision floating point     */
typedef double         FP64;                                    /* Double precision floating point     */

typedef unsigned int   OS_STK;                                  /* Each stack entry is 16-bit wide     */
typedef INT16U         OS_CPU_SR;                               /* Type of CPU status register         */

/*
*********************************************************************************************************
*                                        RENESAS M32C FAMILY
*
* Method #1:  Disable/Enable interrupts using simple instructions.  After critical section, interrupts
*             will be enabled even if they were disabled before entering the critical section.
*
* Method #2:  Disable/Enable interrupts by preserving the state of interrupts.  In other words, if
*             interrupts were disabled before entering the critical section, they will be disabled when
*             leaving the critical section.
*
* Method #3:  Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking you
*             would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
*             disable interrupts.  'cpu_sr' is allocated in all of uC/OS-II's functions that need to
*             disable interrupts.  You would restore the interrupt disable state by copying back 'cpu_sr'
*             into the CPU's status register.
*********************************************************************************************************
*/

#define  OS_CRITICAL_METHOD    1

#if      OS_CRITICAL_METHOD == 1
#define  OS_ENTER_CRITICAL()  asm("FCLR I")                     /* Disable interrupts                  */
#define  OS_EXIT_CRITICAL()   asm("FSET I")                     /* Enable  interrupts                  */
#endif


#if      OS_CRITICAL_METHOD == 2
#define  OS_ENTER_CRITICAL()  asm("PUSHC FLG"); asm("FCLR I")   /* Disable interrupts                  */
#define  OS_EXIT_CRITICAL()   asm("POPC FLG")                   /* Enable  interrupts                  */
#endif

#if      OS_CRITICAL_METHOD == 3
#define  OS_ENTER_CRITICAL()  asm("STC FLG, $@", cpu_sr);asm("FCLR I")   /* Disable interrupts         */
#define  OS_EXIT_CRITICAL()   asm("LDC $@, FLG", cpu_sr)                 /* Enable  interrupts         */
#endif
/*
*********************************************************************************************************
*                                  RENESAS M32C FAMILY MISCELLANEOUS
*********************************************************************************************************
*/

#define  OS_STK_GROWTH        1                                 /* Stack grows from HIGH to LOW memory */
#define  OS_TASK_SW()         asm("INT #1")                     /* Mapped to the software interrupt 1  */

/*
*********************************************************************************************************
*                                              PROTOTYPES
*********************************************************************************************************
*/

void     OSCtxSw(void);
void     OSIntCtxSw(void);
void     OSStartHighRdy(void);
void     OSTickISR(void);


/* save all register and do the same as if OSIntEnter() is called  */
#define OS_START_ISR(lbl) \
    asm("PUSHM R0,R1,R2,R3,A0,A1,SB,FB");\
    asm("INC.B _OSIntNesting");\
    asm("CMP.B #1,_OSIntNesting");\
    asm("JNE _L" ## #lbl );\
    asm("MOV.L _OSTCBCur,A0");\
    asm("STC ISP,[A0]");\
    asm("_L" ## #lbl ":")

/* restore all saved register and call OSIntExit */
#define OS_END_ISR() \
    asm("JSR _OSIntExit");\
    asm("POPM R0,R1,R2,R3,A0,A1,SB,FB")

/* End of os_cpu.h */

