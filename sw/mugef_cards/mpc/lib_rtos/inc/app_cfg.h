/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2003-2006; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                       APPLICATION CONFIGURATION
*
*                                             Renesas M32C
*                                                on the
*                                      SKP32C84 Evaluation Board
*
* Filename      : app_cfg.h
* Version       : V1.00
* Programmer(s) : Brian Nagel
*********************************************************************************************************
*/

#ifndef  __APP_CFG_H__
#define  __APP_CFG_H__

/*
*********************************************************************************************************
*                                       ADDITIONAL uC/MODULE ENABLES
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                            TASK PRIORITIES
*********************************************************************************************************
*/

#define  OS_TASK_TMR_PRIO             0
#define  APP_TASK_USER_IF_PRIO        5
#define  APP_TASK_KBD_PRIO            6

#define  OS_VIEW_TASK_PRIO            7
#define  OS_VIEW_TASK_ID              7

#define  APP_TASK_START_PRIO         10


/*
*********************************************************************************************************
*                                            TASK STACK SIZES
*********************************************************************************************************
*/

#define  APP_TASK_START_STK_SIZE    200
#define  APP_TASK_USER_IF_STK_SIZE  200
#define  APP_TASK_KBD_STK_SIZE      200

#define  OS_VIEW_TASK_STK_SIZE      200


/*
*********************************************************************************************************
*                                          uC/LCD CONFIGURATION
*********************************************************************************************************
*/

#define  DISP_BUS_WIDTH               4        /* LCD controller is accessed with a 4 bit bus          */




#endif
