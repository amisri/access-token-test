              PAGE

;****************************************************************************
;** BOOTWARE PROGRAM                          
;****************************************************************************
STARTBOOT:    CLR.L    D0
              CLR.L    D1
              CLR.L    D2
              CLR.L    D3
              CLR.L    D4
              CLR.L    D5
              CLR.L    D6
              CLR.L    D7
              SETDIAGBOOT   31              ;/diagnostic to 31 (pas valide)
              STACMDRUN     30              ; SET STATUS TO RUN, ALL CHECKS BAD
              MAGICBOOTEST  29              ; test bootware magic number

              BOOTCRCTEST   28              ; TEST BOOTWARE CRC
              BOOTCSTEST    27              ; TEST BOOTWARE CHECK SUM

              READSIZEMEM   26              ; lecture des tailles memoires
              RAMPGMTEST    25              ; TEST RAM PROGRAM
              RAMCOMTEST    24              ; TEST RAM COMMON
              STACMDTEST    23              ; TEST COMMAND/STATUS REGISTER
              LEDIAGTEST    22              ; TEST LED DIAGNOSTIC REGISTER
              REGMBATEST    21              ; TEST BASE ADDRESS REGISTER
              REGMDPWTEST   20              ; TEST DUAL PORTED WINDOW REGISTER
              REGMMIDTEST   19              ; TEST MID POINTERS REGISTERS
              REGMACTTEST   18              ; TEST ACTION REGISTER
              REGMADDTEST   17              ; TEST ADD-ON REGISTER
              REGIRQVTEST   16              ; TEST IRQ VME REGISTER (NO IRQ)
              REGVECVTEST   15              ; TEST IRQ VME VECTORS REGISTERS
              REGIMENTEST   14              ; TEST IRQ MUP REGISTER (NO IRQ)
              COPROCTEST    13              ; TEST COPROCESSOR REGISTERS
              REGADONTEST   12              ; TEST ADD-ON SELECT REGISTER
              BOOTWAREMID   11              ; GENERATE BOOTWARE MID
              CMPI.L   #$55555555,FIRMNOTPRES ; TEST PAS DE FIRMWARE
              BNE.S    NEXTFIRMTST
              BRA.S    NOFIRMWARE
NEXTFIRMTST:  CMPI.L   #$AAAAAAAA,FIRMNOTPRES ; TEST PRESENCE FIRMWARE
              BNE.S    NOFIRMWARE
              MOVE.W   #$D03F,REGMSTACMD    ; set firm test not valid, not ok
                                            ; but all result ok except MID
              JMP      FIRMLINK             ; link to start firmware check

NOFIRMWARE:   MOVE.W   #$D81F,REGMSTACMD    ; set firm test not valid, ok
                                            ; but no firmware
              SETDIAGBOOT    1              ;/diag to boot ok without firmware
              ANDI.W   #$7FFE,REGMSTACMD    ; set firm test valid
              STOP     #$0700

              PAGE

;*****************************************************************************
;*** GESTION DES EXCEPTIONS
;*****************************************************************************

PGMMIV02      EQU      *       ; 002  02  BUS ERROR                       
              MOVE.W   #$02,REGLEDIAG
              BRA      TRAPERROR
PGMMIV03      EQU      *       ; 003  03  ADDRESS ERROR                       
              MOVE.W   #$03,REGLEDIAG
              BRA      TRAPERROR
PGMMIV04      EQU      *       ; 004  04  ILLEGAL INSTRUCTION                 
              MOVE.W   #$04,REGLEDIAG
              BRA      TRAPERROR
PGMMIV05      EQU      *       ; 005  05  ZERO DIVIDE                         
              MOVE.W   #$05,REGLEDIAG
              BRA      TRAPERROR
PGMMIV06      EQU      *       ; 006  06  CHK, CHK2 INSTRUCTION               
              MOVE.W   #$06,REGLEDIAG
              BRA      TRAPERROR
PGMMIV07      EQU      *       ; 007  07  cpTRAPcc, TRAPcc, TRAPV INSTRUCTIONS
              MOVE.W   #$07,REGLEDIAG
              BRA      TRAPERROR
PGMMIV08      EQU      *       ; 008  08  PRIVILIEGE VIOLATION                
              MOVE.W   #$08,REGLEDIAG
              BRA      TRAPERROR
PGMMIV09      EQU      *       ; 009  09  TRACE                               
              MOVE.W   #$09,REGLEDIAG
              BRA      TRAPERROR
PGMMIV0A      EQU      *       ; 010  0A  LINE 1010 EMULATOR                  
              MOVE.W   #$0A,REGLEDIAG
              BRA      TRAPERROR
PGMMIV0B      EQU      *       ; 011  0B  LINE 1111 EMULATOR                  
              MOVE.W   #$0B,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMMIV0C      EQU      *       ; 012  0C  (UNASSIGNED,RESERVED)               
              MOVE.W   #$0C,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMMIV0D      EQU      *       ; 013  0D  COPROCESSOR PROTOCOL VIOLATION      
              MOVE.W   #$0D,REGLEDIAG
              BRA      TRAPERROR
PGMMIV0E      EQU      *       ; 014  0E  FORMAT ERROR                        
              MOVE.W   #$0E,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMMIV0F      EQU      *       ; 015  0F  UNINITIALIZED INTERUPT VECTOR       
              MOVE.W   #$0F,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMUNR10      EQU      *       ; 016  10  (UNASSIGNED,RESERVED)               
              MOVE.W   #$10,REGLEDIAG
              BRA      TRAPERROR
PGMUNR11      EQU      *       ; 017  11  (UNASSIGNED,RESERVED)               
              MOVE.W   #$11,REGLEDIAG
              BRA      TRAPERROR
PGMUNR12      EQU      *       ; 018  12  (UNASSIGNED,RESERVED)               
              MOVE.W   #$12,REGLEDIAG
              BRA      TRAPERROR
PGMUNR13      EQU      *       ; 019  13  (UNASSIGNED,RESERVED)               
              MOVE.W   #$13,REGLEDIAG
              BRA      TRAPERROR
PGMUNR14      EQU      *       ; 020  14  (UNASSIGNED,RESERVED)               
              MOVE.W   #$14,REGLEDIAG
              BRA      TRAPERROR
PGMUNR15      EQU      *       ; 021  15  (UNASSIGNED,RESERVED)               
              MOVE.W   #$15,REGLEDIAG
              BRA      TRAPERROR
PGMUNR16      EQU      *       ; 022  16  (UNASSIGNED,RESERVED)               
              MOVE.W   #$16,REGLEDIAG
              BRA      TRAPERROR
PGMUNR17      EQU      *       ; 023  17  (UNASSIGNED,RESERVED)               
              MOVE.W   #$17,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMUNR18      EQU      *       ; 024  18  SPURIOUS INTERUPT                   
              MOVE.W   #$18,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMAUV19      EQU      *       ; 025  19  LEVEL 1 INTERUPT AUTOVECTOR         
              MOVE.W   #$19,REGLEDIAG
              BRA      TRAPERROR
PGMAUV1A      EQU      *       ; 026  1A  LEVEL 2 INTERUPT AUTOVECTOR         
              MOVE.W   #$1A,REGLEDIAG
              BRA      TRAPERROR
PGMAUV1B      EQU      *       ; 027  1B  LEVEL 3 INTERUPT AUTOVECTOR         
              MOVE.W   #$1B,REGLEDIAG
              BRA      TRAPERROR
PGMAUV1C      EQU      *       ; 028  1C  LEVEL 4 INTERUPT AUTOVECTOR         
              MOVE.W   #$1C,REGLEDIAG
              BRA      TRAPERROR
PGMAUV1D      EQU      *       ; 029  1D  LEVEL 5 INTERUPT AUTOVECTOR         
              MOVE.W   #$1D,REGLEDIAG
              BRA      TRAPERROR
PGMAUV1E      EQU      *       ; 030  1E  LEVEL 6 INTERUPT AUTOVECTOR         
              MOVE.W   #$1E,REGLEDIAG
              BRA      TRAPERROR
PGMAUV1F      EQU      *       ; 031  1F  LEVEL 7 INTERUPT AUTOVECTOR         
              MOVE.W   #$1F,REGLEDIAG
              BRA      TRAPERROR
                                                                                                                           
PGMTIV20      EQU      *       ; 032  20  TRAP #00 INSTRUCTION VECTOR         
              MOVE.W   #$20,REGLEDIAG
              BRA      TRAPERROR
PGMTIV21      EQU      *       ; 033  21  TRAP #01 INSTRUCTION VECTOR         
              MOVE.W   #$21,REGLEDIAG
              BRA      TRAPERROR
PGMTIV22      EQU      *       ; 034  22  TRAP #02 INSTRUCTION VECTOR         
              MOVE.W   #$22,REGLEDIAG
              BRA      TRAPERROR
PGMTIV23      EQU      *       ; 035  23  TRAP #03 INSTRUCTION VECTOR         
              MOVE.W   #$23,REGLEDIAG
              BRA      TRAPERROR
PGMTIV24      EQU      *       ; 036  24  TRAP #04 INSTRUCTION VECTOR         
              MOVE.W   #$24,REGLEDIAG
              BRA      TRAPERROR
PGMTIV25      EQU      *       ; 037  25  TRAP #05 INSTRUCTION VECTOR         
              MOVE.W   #$25,REGLEDIAG
              BRA      TRAPERROR
PGMTIV26      EQU      *       ; 038  26  TRAP #06 INSTRUCTION VECTOR         
              MOVE.W   #$26,REGLEDIAG
              BRA      TRAPERROR
PGMTIV27      EQU      *       ; 039  27  TRAP #07 INSTRUCTION VECTOR         
              MOVE.W   #$27,REGLEDIAG
              BRA      TRAPERROR
PGMTIV28      EQU      *       ; 040  28  TRAP #08 INSTRUCTION VECTOR         
              MOVE.W   #$28,REGLEDIAG
              BRA      TRAPERROR
PGMTIV29      EQU      *       ; 041  29  TRAP #09 INSTRUCTION VECTOR         
              MOVE.W   #$29,REGLEDIAG
              BRA      TRAPERROR
PGMTIV2A      EQU      *       ; 042  2A  TRAP #10 INSTRUCTION VECTOR         
              MOVE.W   #$2A,REGLEDIAG
              BRA      TRAPERROR
PGMTIV2B      EQU      *       ; 043  2B  TRAP #11 INSTRUCTION VECTOR         
              MOVE.W   #$2B,REGLEDIAG
              BRA      TRAPERROR
PGMTIV2C      EQU      *       ; 044  2C  TRAP #12 INSTRUCTION VECTOR         
              MOVE.W   #$2C,REGLEDIAG
              BRA      TRAPERROR
PGMTIV2D      EQU      *       ; 045  2D  TRAP #13 INSTRUCTION VECTOR         
              MOVE.W   #$2D,REGLEDIAG
              BRA      TRAPERROR
PGMTIV2E      EQU      *       ; 046  2E  TRAP #14 INSTRUCTION VECTOR         
              MOVE.W   #$2E,REGLEDIAG
              BRA      TRAPERROR
PGMTIV2F      EQU      *       ; 047  2F  TRAP #15 INSTRUCTION VECTOR         
              MOVE.W   #$2F,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMCOP30      EQU      *       ; 048  30  FPCP BRANCH OR SET UNORDERED CONDITION    
              MOVE.W   #$30,REGLEDIAG
              BRA      TRAPERROR
PGMCOP31      EQU      *       ; 049  31  FPCP INEXACT RESULT                 
              MOVE.W   #$31,REGLEDIAG
              BRA      TRAPERROR
PGMCOP32      EQU      *       ; 050  32  FPCP DIVIDE BY ZERO                 
              MOVE.W   #$32,REGLEDIAG
              BRA      TRAPERROR
PGMCOP33      EQU      *       ; 051  33  FPCP UNDERFLOW                      
              MOVE.W   #$33,REGLEDIAG
              BRA      TRAPERROR
PGMCOP34      EQU      *       ; 052  34  FPCP OPERAND ERROR                  
              MOVE.W   #$34,REGLEDIAG
              BRA      TRAPERROR
PGMCOP35      EQU      *       ; 053  35  FPCP OVERFLOW                       
              MOVE.W   #$35,REGLEDIAG
              BRA      TRAPERROR
PGMCOP36      EQU      *       ; 054  36  FPCP SIGNALING NAN                  
              MOVE.W   #$36,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMUNR37      EQU      *       ; 055  37  (UNASSIGNED,RESERVED)               
              MOVE.W   #$37,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMMMU38      EQU      *       ; 056  38  PMMU CONFIGURATION                  
              MOVE.W   #$38,REGLEDIAG
              BRA      TRAPERROR
PGMMMU39      EQU      *       ; 057  39  PMMU ILLEGAL OPERATION              
              MOVE.W   #$39,REGLEDIAG
              BRA      TRAPERROR
PGMMMU3A      EQU      *       ; 058  3A  PMMU ACCESS LEVEL VIOLATION         
              MOVE.W   #$3A,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMUNR3B      EQU      *       ; 059  3B  (UNASSIGNED,RESERVED)               
              MOVE.W   #$3B,REGLEDIAG
              BRA      TRAPERROR
PGMUNR3C      EQU      *       ; 060  3C  (UNASSIGNED,RESERVED)               
              MOVE.W   #$3C,REGLEDIAG
              BRA      TRAPERROR
PGMUNR3D      EQU      *       ; 061  3D  (UNASSIGNED,RESERVED)               
              MOVE.W   #$3D,REGLEDIAG
              BRA      TRAPERROR
PGMUNR3E      EQU      *       ; 062  3E  (UNASSIGNED,RESERVED)               
              MOVE.W   #$3E,REGLEDIAG
              BRA      TRAPERROR
PGMUNR3F      EQU      *       ; 063  3F  (UNASSIGNED,RESERVED)               
              MOVE.W   #$3F,REGLEDIAG
              BRA      TRAPERROR
                                                                               
PGMUIV40      EQU      *       ; 064  40  USER INTERUPT VECTORS               
              MOVE.W   #$40,REGLEDIAG
              BRA      TRAPERROR
PGMUIV41      EQU      *       ; 065  41  USER INTERUPT VECTORS               
              MOVE.W   #$41,REGLEDIAG
              BRA      TRAPERROR
PGMUIV42      EQU      *       ; 066  42  USER INTERUPT VECTORS               
              MOVE.W   #$42,REGLEDIAG
              BRA      TRAPERROR
PGMUIV43      EQU      *       ; 067  43  USER INTERUPT VECTORS               
              MOVE.W   #$43,REGLEDIAG
              BRA      TRAPERROR
PGMUIV44      EQU      *       ; 068  44  USER INTERUPT VECTORS               
              MOVE.W   #$44,REGLEDIAG
              BRA      TRAPERROR
PGMUIV45      EQU      *       ; 069  45  USER INTERUPT VECTORS               
              MOVE.W   #$45,REGLEDIAG
              BRA      TRAPERROR
PGMUIV46      EQU      *       ; 070  46  USER INTERUPT VECTORS               
              MOVE.W   #$46,REGLEDIAG
              BRA      TRAPERROR
PGMUIV47      EQU      *       ; 071  47  USER INTERUPT VECTORS               
              MOVE.W   #$47,REGLEDIAG
              BRA      TRAPERROR
PGMUIV48      EQU      *       ; 072  48  USER INTERUPT VECTORS               
              MOVE.W   #$48,REGLEDIAG
              BRA      TRAPERROR
PGMUIV49      EQU      *       ; 073  49  USER INTERUPT VECTORS               
              MOVE.W   #$49,REGLEDIAG
              BRA      TRAPERROR
PGMUIV4A      EQU      *       ; 074  4A  USER INTERUPT VECTORS               
              MOVE.W   #$4A,REGLEDIAG
              BRA      TRAPERROR
PGMUIV4B      EQU      *       ; 075  4B  USER INTERUPT VECTORS               
              MOVE.W   #$4B,REGLEDIAG
              BRA      TRAPERROR
PGMUIV4C      EQU      *       ; 076  4C  USER INTERUPT VECTORS               
              MOVE.W   #$4C,REGLEDIAG
              BRA      TRAPERROR
PGMUIV4D      EQU      *       ; 077  4D  USER INTERUPT VECTORS               
              MOVE.W   #$4D,REGLEDIAG
              BRA      TRAPERROR
PGMUIV4E      EQU      *       ; 078  4E  USER INTERUPT VECTORS               
              MOVE.W   #$4E,REGLEDIAG
              BRA      TRAPERROR
PGMUIV4F      EQU      *       ; 079  4F  USER INTERUPT VECTORS               
              MOVE.W   #$4F,REGLEDIAG
              BRA      TRAPERROR
PGMUIV50      EQU      *       ; 080  50  USER INTERUPT VECTORS               
              MOVE.W   #$50,REGLEDIAG
              BRA      TRAPERROR
PGMUIV51      EQU      *       ; 081  51  USER INTERUPT VECTORS               
              MOVE.W   #$51,REGLEDIAG
              BRA      TRAPERROR
PGMUIV52      EQU      *       ; 082  52  USER INTERUPT VECTORS               
              MOVE.W   #$52,REGLEDIAG
              BRA      TRAPERROR
PGMUIV53      EQU      *       ; 083  53  USER INTERUPT VECTORS               
              MOVE.W   #$53,REGLEDIAG
              BRA      TRAPERROR
PGMUIV54      EQU      *       ; 084  54  USER INTERUPT VECTORS               
              MOVE.W   #$54,REGLEDIAG
              BRA      TRAPERROR
PGMUIV55      EQU      *       ; 085  55  USER INTERUPT VECTORS               
              MOVE.W   #$55,REGLEDIAG
              BRA      TRAPERROR
PGMUIV56      EQU      *       ; 086  56  USER INTERUPT VECTORS               
              MOVE.W   #$56,REGLEDIAG
              BRA      TRAPERROR
PGMUIV57      EQU      *       ; 087  57  USER INTERUPT VECTORS               
              MOVE.W   #$57,REGLEDIAG
              BRA      TRAPERROR
PGMUIV58      EQU      *       ; 088  58  USER INTERUPT VECTORS               
              MOVE.W   #$58,REGLEDIAG
              BRA      TRAPERROR
PGMUIV59      EQU      *       ; 089  59  USER INTERUPT VECTORS               
              MOVE.W   #$59,REGLEDIAG
              BRA      TRAPERROR
PGMUIV5A      EQU      *       ; 090  5A  USER INTERUPT VECTORS               
              MOVE.W   #$5A,REGLEDIAG
              BRA      TRAPERROR
PGMUIV5B      EQU      *       ; 091  5B  USER INTERUPT VECTORS               
              MOVE.W   #$5B,REGLEDIAG
              BRA      TRAPERROR
PGMUIV5C      EQU      *       ; 092  5C  USER INTERUPT VECTORS               
              MOVE.W   #$5C,REGLEDIAG
              BRA      TRAPERROR
PGMUIV5D      EQU      *       ; 093  5D  USER INTERUPT VECTORS               
              MOVE.W   #$5D,REGLEDIAG
              BRA      TRAPERROR
PGMUIV5E      EQU      *       ; 094  5E  USER INTERUPT VECTORS               
              MOVE.W   #$5E,REGLEDIAG
              BRA      TRAPERROR
PGMUIV5F      EQU      *       ; 095  5F  USER INTERUPT VECTORS               
              MOVE.W   #$5F,REGLEDIAG
              BRA      TRAPERROR
PGMUIV60      EQU      *       ; 096  60  USER INTERUPT VECTORS               
              MOVE.W   #$60,REGLEDIAG
              BRA      TRAPERROR
PGMUIV61      EQU      *       ; 097  61  USER INTERUPT VECTORS               
              MOVE.W   #$61,REGLEDIAG
              BRA      TRAPERROR
PGMUIV62      EQU      *       ; 098  62  USER INTERUPT VECTORS               
              MOVE.W   #$62,REGLEDIAG
              BRA      TRAPERROR
PGMUIV63      EQU      *       ; 099  63  USER INTERUPT VECTORS               
              MOVE.W   #$63,REGLEDIAG
              BRA      TRAPERROR
PGMUIV64      EQU      *       ; 100  64  USER INTERUPT VECTORS               
              MOVE.W   #$64,REGLEDIAG
              BRA      TRAPERROR
PGMUIV65      EQU      *       ; 101  65  USER INTERUPT VECTORS               
              MOVE.W   #$65,REGLEDIAG
              BRA      TRAPERROR
PGMUIV66      EQU      *       ; 102  66  USER INTERUPT VECTORS               
              MOVE.W   #$66,REGLEDIAG
              BRA      TRAPERROR
PGMUIV67      EQU      *       ; 103  67  USER INTERUPT VECTORS               
              MOVE.W   #$67,REGLEDIAG
              BRA      TRAPERROR
PGMUIV68      EQU      *       ; 104  68  USER INTERUPT VECTORS               
              MOVE.W   #$68,REGLEDIAG
              BRA      TRAPERROR
PGMUIV69      EQU      *       ; 105  69  USER INTERUPT VECTORS               
              MOVE.W   #$69,REGLEDIAG
              BRA      TRAPERROR
PGMUIV6A      EQU      *       ; 106  6A  USER INTERUPT VECTORS               
              MOVE.W   #$6A,REGLEDIAG
              BRA      TRAPERROR
PGMUIV6B      EQU      *       ; 107  6B  USER INTERUPT VECTORS               
              MOVE.W   #$6B,REGLEDIAG
              BRA      TRAPERROR
PGMUIV6C      EQU      *       ; 108  6C  USER INTERUPT VECTORS               
              MOVE.W   #$6C,REGLEDIAG
              BRA      TRAPERROR
PGMUIV6D      EQU      *       ; 109  6D  USER INTERUPT VECTORS               
              MOVE.W   #$6D,REGLEDIAG
              BRA      TRAPERROR
PGMUIV6E      EQU      *       ; 110  6E  USER INTERUPT VECTORS               
              MOVE.W   #$6E,REGLEDIAG
              BRA      TRAPERROR
PGMUIV6F      EQU      *       ; 111  6F  USER INTERUPT VECTORS               
              MOVE.W   #$6F,REGLEDIAG
              BRA      TRAPERROR
PGMUIV70      EQU      *       ; 112  70  USER INTERUPT VECTORS               
              MOVE.W   #$70,REGLEDIAG
              BRA      TRAPERROR
PGMUIV71      EQU      *       ; 113  71  USER INTERUPT VECTORS               
              MOVE.W   #$71,REGLEDIAG
              BRA      TRAPERROR
PGMUIV72      EQU      *       ; 114  72  USER INTERUPT VECTORS               
              MOVE.W   #$72,REGLEDIAG
              BRA      TRAPERROR
PGMUIV73      EQU      *       ; 115  73  USER INTERUPT VECTORS               
              MOVE.W   #$73,REGLEDIAG
              BRA      TRAPERROR
PGMUIV74      EQU      *       ; 116  74  USER INTERUPT VECTORS               
              MOVE.W   #$74,REGLEDIAG
              BRA      TRAPERROR
PGMUIV75      EQU      *       ; 117  75  USER INTERUPT VECTORS               
              MOVE.W   #$75,REGLEDIAG
              BRA      TRAPERROR
PGMUIV76      EQU      *       ; 118  76  USER INTERUPT VECTORS               
              MOVE.W   #$76,REGLEDIAG
              BRA      TRAPERROR
PGMUIV77      EQU      *       ; 119  77  USER INTERUPT VECTORS               
              MOVE.W   #$77,REGLEDIAG
              BRA      TRAPERROR
PGMUIV78      EQU      *       ; 120  78  USER INTERUPT VECTORS               
              MOVE.W   #$78,REGLEDIAG
              BRA      TRAPERROR
PGMUIV79      EQU      *       ; 121  79  USER INTERUPT VECTORS               
              MOVE.W   #$79,REGLEDIAG
              BRA      TRAPERROR
PGMUIV7A      EQU      *       ; 122  7A  USER INTERUPT VECTORS               
              MOVE.W   #$7A,REGLEDIAG
              BRA      TRAPERROR
PGMUIV7B      EQU      *       ; 123  7B  USER INTERUPT VECTORS               
              MOVE.W   #$7B,REGLEDIAG
              BRA      TRAPERROR
PGMUIV7C      EQU      *       ; 124  7C  USER INTERUPT VECTORS               
              MOVE.W   #$7C,REGLEDIAG
              BRA      TRAPERROR
PGMUIV7D      EQU      *       ; 125  7D  USER INTERUPT VECTORS               
              MOVE.W   #$7D,REGLEDIAG
              BRA      TRAPERROR
PGMUIV7E      EQU      *       ; 126  7E  USER INTERUPT VECTORS               
              MOVE.W   #$7E,REGLEDIAG
              BRA      TRAPERROR
PGMUIV7F      EQU      *       ; 127  7F  USER INTERUPT VECTORS               
              MOVE.W   #$7F,REGLEDIAG
              BRA      TRAPERROR
PGMUIV80      EQU      *       ; 128  80  USER INTERUPT VECTORS               
              MOVE.W   #$80,REGLEDIAG
              BRA      TRAPERROR
PGMUIV81      EQU      *       ; 129  81  USER INTERUPT VECTORS               
              MOVE.W   #$81,REGLEDIAG
              BRA      TRAPERROR
PGMUIV82      EQU      *       ; 130  82  USER INTERUPT VECTORS               
              MOVE.W   #$82,REGLEDIAG
              BRA      TRAPERROR
PGMUIV83      EQU      *       ; 131  83  USER INTERUPT VECTORS               
              MOVE.W   #$83,REGLEDIAG
              BRA      TRAPERROR
PGMUIV84      EQU      *       ; 132  84  USER INTERUPT VECTORS               
              MOVE.W   #$84,REGLEDIAG
              BRA      TRAPERROR
PGMUIV85      EQU      *       ; 133  85  USER INTERUPT VECTORS               
              MOVE.W   #$85,REGLEDIAG
              BRA      TRAPERROR
PGMUIV86      EQU      *       ; 134  86  USER INTERUPT VECTORS               
              MOVE.W   #$86,REGLEDIAG
              BRA      TRAPERROR
PGMUIV87      EQU      *       ; 135  87  USER INTERUPT VECTORS               
              MOVE.W   #$87,REGLEDIAG
              BRA      TRAPERROR
PGMUIV88      EQU      *       ; 136  88  USER INTERUPT VECTORS               
              MOVE.W   #$88,REGLEDIAG
              BRA      TRAPERROR
PGMUIV89      EQU      *       ; 137  89  USER INTERUPT VECTORS               
              MOVE.W   #$89,REGLEDIAG
              BRA      TRAPERROR
PGMUIV8A      EQU      *       ; 138  8A  USER INTERUPT VECTORS               
              MOVE.W   #$8A,REGLEDIAG
              BRA      TRAPERROR
PGMUIV8B      EQU      *       ; 139  8B  USER INTERUPT VECTORS               
              MOVE.W   #$8B,REGLEDIAG
              BRA      TRAPERROR
PGMUIV8C      EQU      *       ; 140  8C  USER INTERUPT VECTORS               
              MOVE.W   #$8C,REGLEDIAG
              BRA      TRAPERROR
PGMUIV8D      EQU      *       ; 141  8D  USER INTERUPT VECTORS               
              MOVE.W   #$8D,REGLEDIAG
              BRA      TRAPERROR
PGMUIV8E      EQU      *       ; 142  8E  USER INTERUPT VECTORS               
              MOVE.W   #$8E,REGLEDIAG
              BRA      TRAPERROR
PGMUIV8F      EQU      *       ; 143  8F  USER INTERUPT VECTORS               
              MOVE.W   #$8F,REGLEDIAG
              BRA      TRAPERROR
PGMUIV90      EQU      *       ; 144  90  USER INTERUPT VECTORS               
              MOVE.W   #$90,REGLEDIAG
              BRA      TRAPERROR
PGMUIV91      EQU      *       ; 145  91  USER INTERUPT VECTORS               
              MOVE.W   #$91,REGLEDIAG
              BRA      TRAPERROR
PGMUIV92      EQU      *       ; 146  92  USER INTERUPT VECTORS               
              MOVE.W   #$92,REGLEDIAG
              BRA      TRAPERROR
PGMUIV93      EQU      *       ; 147  93  USER INTERUPT VECTORS               
              MOVE.W   #$93,REGLEDIAG
              BRA      TRAPERROR
PGMUIV94      EQU      *       ; 148  94  USER INTERUPT VECTORS               
              MOVE.W   #$94,REGLEDIAG
              BRA      TRAPERROR
PGMUIV95      EQU      *       ; 149  95  USER INTERUPT VECTORS               
              MOVE.W   #$95,REGLEDIAG
              BRA      TRAPERROR
PGMUIV96      EQU      *       ; 150  96  USER INTERUPT VECTORS               
              MOVE.W   #$96,REGLEDIAG
              BRA      TRAPERROR
PGMUIV97      EQU      *       ; 151  97  USER INTERUPT VECTORS               
              MOVE.W   #$97,REGLEDIAG
              BRA      TRAPERROR
PGMUIV98      EQU      *       ; 152  98  USER INTERUPT VECTORS               
              MOVE.W   #$98,REGLEDIAG
              BRA      TRAPERROR
PGMUIV99      EQU      *       ; 153  99  USER INTERUPT VECTORS               
              MOVE.W   #$99,REGLEDIAG
              BRA      TRAPERROR
PGMUIV9A      EQU      *       ; 154  9A  USER INTERUPT VECTORS               
              MOVE.W   #$9A,REGLEDIAG
              BRA      TRAPERROR
PGMUIV9B      EQU      *       ; 155  9B  USER INTERUPT VECTORS               
              MOVE.W   #$9B,REGLEDIAG
              BRA      TRAPERROR
PGMUIV9C      EQU      *       ; 156  9C  USER INTERUPT VECTORS               
              MOVE.W   #$9C,REGLEDIAG
              BRA      TRAPERROR
PGMUIV9D      EQU      *       ; 157  9D  USER INTERUPT VECTORS               
              MOVE.W   #$9D,REGLEDIAG
              BRA      TRAPERROR
PGMUIV9E      EQU      *       ; 158  9E  USER INTERUPT VECTORS               
              MOVE.W   #$9E,REGLEDIAG
              BRA      TRAPERROR
PGMUIV9F      EQU      *       ; 159  9F  USER INTERUPT VECTORS               
              MOVE.W   #$9F,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA0      EQU      *       ; 160  A0  USER INTERUPT VECTORS               
              MOVE.W   #$A0,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA1      EQU      *       ; 161  A1  USER INTERUPT VECTORS               
              MOVE.W   #$A1,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA2      EQU      *       ; 162  A2  USER INTERUPT VECTORS               
              MOVE.W   #$A2,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA3      EQU      *       ; 163  A3  USER INTERUPT VECTORS               
              MOVE.W   #$A3,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA4      EQU      *       ; 164  A4  USER INTERUPT VECTORS               
              MOVE.W   #$A4,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA5      EQU      *       ; 165  A5  USER INTERUPT VECTORS               
              MOVE.W   #$A5,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA6      EQU      *       ; 166  A6  USER INTERUPT VECTORS               
              MOVE.W   #$A6,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA7      EQU      *       ; 167  A7  USER INTERUPT VECTORS               
              MOVE.W   #$A7,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA8      EQU      *       ; 168  A8  USER INTERUPT VECTORS               
              MOVE.W   #$A8,REGLEDIAG
              BRA      TRAPERROR
PGMUIVA9      EQU      *       ; 169  A9  USER INTERUPT VECTORS               
              MOVE.W   #$A9,REGLEDIAG
              BRA      TRAPERROR
PGMUIVAA      EQU      *       ; 170  AA  USER INTERUPT VECTORS               
              MOVE.W   #$AA,REGLEDIAG
              BRA      TRAPERROR
PGMUIVAB      EQU      *       ; 171  AB  USER INTERUPT VECTORS               
              MOVE.W   #$AB,REGLEDIAG
              BRA      TRAPERROR
PGMUIVAC      EQU      *       ; 172  AC  USER INTERUPT VECTORS               
              MOVE.W   #$AC,REGLEDIAG
              BRA      TRAPERROR
PGMUIVAD      EQU      *       ; 173  AD  USER INTERUPT VECTORS               
              MOVE.W   #$AD,REGLEDIAG
              BRA      TRAPERROR
PGMUIVAE      EQU      *       ; 174  AE  USER INTERUPT VECTORS               
              MOVE.W   #$AE,REGLEDIAG
              BRA      TRAPERROR
PGMUIVAF      EQU      *       ; 175  AF  USER INTERUPT VECTORS               
              MOVE.W   #$AF,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB0      EQU      *       ; 176  B0  USER INTERUPT VECTORS               
              MOVE.W   #$B0,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB1      EQU      *       ; 177  B1  USER INTERUPT VECTORS               
              MOVE.W   #$B1,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB2      EQU      *       ; 178  B2  USER INTERUPT VECTORS               
              MOVE.W   #$B2,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB3      EQU      *       ; 179  B3  USER INTERUPT VECTORS               
              MOVE.W   #$B3,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB4      EQU      *       ; 180  B4  USER INTERUPT VECTORS               
              MOVE.W   #$B4,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB5      EQU      *       ; 181  B5  USER INTERUPT VECTORS               
              MOVE.W   #$B5,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB6      EQU      *       ; 182  B6  USER INTERUPT VECTORS               
              MOVE.W   #$B6,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB7      EQU      *       ; 183  B7  USER INTERUPT VECTORS               
              MOVE.W   #$B7,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB8      EQU      *       ; 184  B8  USER INTERUPT VECTORS               
              MOVE.W   #$B8,REGLEDIAG
              BRA      TRAPERROR
PGMUIVB9      EQU      *       ; 185  B9  USER INTERUPT VECTORS               
              MOVE.W   #$B9,REGLEDIAG
              BRA      TRAPERROR
PGMUIVBA      EQU      *       ; 186  BA  USER INTERUPT VECTORS               
              MOVE.W   #$BA,REGLEDIAG
              BRA      TRAPERROR
PGMUIVBB      EQU      *       ; 187  BB  USER INTERUPT VECTORS               
              MOVE.W   #$BB,REGLEDIAG
              BRA      TRAPERROR
PGMUIVBC      EQU      *       ; 188  BC  USER INTERUPT VECTORS               
              MOVE.W   #$BC,REGLEDIAG
              BRA      TRAPERROR
PGMUIVBD      EQU      *       ; 189  BD  USER INTERUPT VECTORS               
              MOVE.W   #$BD,REGLEDIAG
              BRA      TRAPERROR
PGMUIVBE      EQU      *       ; 190  BE  USER INTERUPT VECTORS               
              MOVE.W   #$BE,REGLEDIAG
              BRA      TRAPERROR
PGMUIVBF      EQU      *       ; 191  BF  USER INTERUPT VECTORS               
              MOVE.W   #$BF,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC0      EQU      *       ; 192  C0  USER INTERUPT VECTORS               
              MOVE.W   #$C0,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC1      EQU      *       ; 193  C1  USER INTERUPT VECTORS               
              MOVE.W   #$C1,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC2      EQU      *       ; 194  C2  USER INTERUPT VECTORS               
              MOVE.W   #$C2,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC3      EQU      *       ; 195  C3  USER INTERUPT VECTORS               
              MOVE.W   #$C3,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC4      EQU      *       ; 196  C4  USER INTERUPT VECTORS               
              MOVE.W   #$C4,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC5      EQU      *       ; 197  C5  USER INTERUPT VECTORS               
              MOVE.W   #$C5,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC6      EQU      *       ; 198  C6  USER INTERUPT VECTORS               
              MOVE.W   #$C6,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC7      EQU      *       ; 199  C7  USER INTERUPT VECTORS               
              MOVE.W   #$C7,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC8      EQU      *       ; 200  C8  USER INTERUPT VECTORS               
              MOVE.W   #$C8,REGLEDIAG
              BRA      TRAPERROR
PGMUIVC9      EQU      *       ; 201  C9  USER INTERUPT VECTORS               
              MOVE.W   #$C9,REGLEDIAG
              BRA      TRAPERROR
PGMUIVCA      EQU      *       ; 202  CA  USER INTERUPT VECTORS               
              MOVE.W   #$CA,REGLEDIAG
              BRA      TRAPERROR
PGMUIVCB      EQU      *       ; 203  CB  USER INTERUPT VECTORS               
              MOVE.W   #$CB,REGLEDIAG
              BRA      TRAPERROR
PGMUIVCC      EQU      *       ; 204  CC  USER INTERUPT VECTORS               
              MOVE.W   #$CC,REGLEDIAG
              BRA      TRAPERROR
PGMUIVCD      EQU      *       ; 205  CD  USER INTERUPT VECTORS               
              MOVE.W   #$CD,REGLEDIAG
              BRA      TRAPERROR
PGMUIVCE      EQU      *       ; 206  CE  USER INTERUPT VECTORS               
              MOVE.W   #$CE,REGLEDIAG
              BRA      TRAPERROR
PGMUIVCF      EQU      *       ; 207  CF  USER INTERUPT VECTORS               
              MOVE.W   #$CF,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD0      EQU      *       ; 208  D0  USER INTERUPT VECTORS               
              MOVE.W   #$D0,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD1      EQU      *       ; 209  D1  USER INTERUPT VECTORS               
              MOVE.W   #$D1,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD2      EQU      *       ; 210  D2  USER INTERUPT VECTORS               
              MOVE.W   #$D2,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD3      EQU      *       ; 211  D3  USER INTERUPT VECTORS               
              MOVE.W   #$D3,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD4      EQU      *       ; 212  D4  USER INTERUPT VECTORS               
              MOVE.W   #$D4,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD5      EQU      *       ; 213  D5  USER INTERUPT VECTORS               
              MOVE.W   #$D5,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD6      EQU      *       ; 214  D6  USER INTERUPT VECTORS               
              MOVE.W   #$D6,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD7      EQU      *       ; 215  D7  USER INTERUPT VECTORS               
              MOVE.W   #$D7,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD8      EQU      *       ; 216  D8  USER INTERUPT VECTORS               
              MOVE.W   #$D8,REGLEDIAG
              BRA      TRAPERROR
PGMUIVD9      EQU      *       ; 217  D9  USER INTERUPT VECTORS               
              MOVE.W   #$D9,REGLEDIAG
              BRA      TRAPERROR
PGMUIVDA      EQU      *       ; 218  DA  USER INTERUPT VECTORS               
              MOVE.W   #$DA,REGLEDIAG
              BRA      TRAPERROR
PGMUIVDB      EQU      *       ; 219  DB  USER INTERUPT VECTORS               
              MOVE.W   #$DB,REGLEDIAG
              BRA      TRAPERROR
PGMUIVDC      EQU      *       ; 220  DC  USER INTERUPT VECTORS               
              MOVE.W   #$DC,REGLEDIAG
              BRA      TRAPERROR
PGMUIVDD      EQU      *       ; 221  DD  USER INTERUPT VECTORS               
              MOVE.W   #$DD,REGLEDIAG
              BRA      TRAPERROR
PGMUIVDE      EQU      *       ; 222  DE  USER INTERUPT VECTORS               
              MOVE.W   #$DE,REGLEDIAG
              BRA      TRAPERROR
PGMUIVDF      EQU      *       ; 223  DF  USER INTERUPT VECTORS               
              MOVE.W   #$DF,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE0      EQU      *       ; 224  E0  USER INTERUPT VECTORS               
              MOVE.W   #$E0,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE1      EQU      *       ; 225  E1  USER INTERUPT VECTORS               
              MOVE.W   #$E1,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE2      EQU      *       ; 226  E2  USER INTERUPT VECTORS               
              MOVE.W   #$E2,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE3      EQU      *       ; 227  E3  USER INTERUPT VECTORS               
              MOVE.W   #$E3,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE4      EQU      *       ; 228  E4  USER INTERUPT VECTORS               
              MOVE.W   #$E4,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE5      EQU      *       ; 229  E5  USER INTERUPT VECTORS               
              MOVE.W   #$E5,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE6      EQU      *       ; 230  E6  USER INTERUPT VECTORS               
              MOVE.W   #$E6,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE7      EQU      *       ; 231  E7  USER INTERUPT VECTORS               
              MOVE.W   #$E7,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE8      EQU      *       ; 232  E8  USER INTERUPT VECTORS               
              MOVE.W   #$E8,REGLEDIAG
              BRA      TRAPERROR
PGMUIVE9      EQU      *       ; 233  E9  USER INTERUPT VECTORS               
              MOVE.W   #$E9,REGLEDIAG
              BRA      TRAPERROR
PGMUIVEA      EQU      *       ; 234  EA  USER INTERUPT VECTORS               
              MOVE.W   #$EA,REGLEDIAG
              BRA      TRAPERROR
PGMUIVEB      EQU      *       ; 235  EB  USER INTERUPT VECTORS               
              MOVE.W   #$EB,REGLEDIAG
              BRA      TRAPERROR
PGMUIVEC      EQU      *       ; 236  EC  USER INTERUPT VECTORS               
              MOVE.W   #$EC,REGLEDIAG
              BRA      TRAPERROR
PGMUIVED      EQU      *       ; 237  ED  USER INTERUPT VECTORS               
              MOVE.W   #$ED,REGLEDIAG
              BRA      TRAPERROR
PGMUIVEE      EQU      *       ; 238  EE  USER INTERUPT VECTORS               
              MOVE.W   #$EE,REGLEDIAG
              BRA      TRAPERROR
PGMUIVEF      EQU      *       ; 239  EF  USER INTERUPT VECTORS               
              MOVE.W   #$EF,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF0      EQU      *       ; 240  F0  USER INTERUPT VECTORS               
              MOVE.W   #$F0,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF1      EQU      *       ; 241  F1  USER INTERUPT VECTORS               
              MOVE.W   #$F1,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF2      EQU      *       ; 242  F2  USER INTERUPT VECTORS               
              MOVE.W   #$F2,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF3      EQU      *       ; 243  F3  USER INTERUPT VECTORS               
              MOVE.W   #$F3,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF4      EQU      *       ; 244  F4  USER INTERUPT VECTORS               
              MOVE.W   #$F4,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF5      EQU      *       ; 245  F5  USER INTERUPT VECTORS               
              MOVE.W   #$F5,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF6      EQU      *       ; 246  F6  USER INTERUPT VECTORS               
              MOVE.W   #$F6,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF7      EQU      *       ; 247  F7  USER INTERUPT VECTORS               
              MOVE.W   #$F7,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF8      EQU      *       ; 248  F8  USER INTERUPT VECTORS               
              MOVE.W   #$F8,REGLEDIAG
              BRA      TRAPERROR
PGMUIVF9      EQU      *       ; 249  F9  USER INTERUPT VECTORS               
              MOVE.W   #$F9,REGLEDIAG
              BRA      TRAPERROR
PGMUIVFA      EQU      *       ; 250  FA  USER INTERUPT VECTORS               
              MOVE.W   #$FA,REGLEDIAG
              BRA      TRAPERROR
PGMUIVFB      EQU      *       ; 251  FB  USER INTERUPT VECTORS               
              MOVE.W   #$FB,REGLEDIAG
              BRA      TRAPERROR
PGMUIVFC      EQU      *       ; 252  FC  USER INTERUPT VECTORS               
              MOVE.W   #$FC,REGLEDIAG
              BRA      TRAPERROR
PGMUIVFD      EQU      *       ; 253  FD  USER INTERUPT VECTORS               
              MOVE.W   #$FD,REGLEDIAG
              BRA      TRAPERROR
PGMUIVFE      EQU      *       ; 254  FE  USER INTERUPT VECTORS               
              MOVE.W   #$FE,REGLEDIAG
              BRA      TRAPERROR
PGMUIVFF      EQU      *       ; 255  FF  USER INTERUPT VECTORS               
              MOVE.W   #$FF,REGLEDIAG
              BRA      TRAPERROR

              NOP

TRAPERROR:    MOVE.L   #$A5A55A5A,-(SP)
              ORI.W    #$4200,REGMSTACMD
              ANDI.W   #$7FFA,REGMSTACMD
              STOP     #$0700

BOOTEND:      EQU      *

              ORG      $10000
FIRMNOTPRES:  DC.L     $55555555            ; string to no firmware
FIRMLINK:     NOP
              NOP
              STOP     #$2700

              END

