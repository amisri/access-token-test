              PAGE
      
;****************************************************************************
;** FIRMWARE PROGRAM                          
;****************************************************************************
STARTFIRM:    CLR.L    D0
              CLR.L    D1
              CLR.L    D2
              CLR.L    D3
              CLR.L    D4
              CLR.L    D5
              CLR.L    D6
              CLR.L    D7
              SETDIAGFIRM    31             ; diagnostic to 31
              MAGICFIRMTEST  30             ; test firmware magic number

              FIRMCRCTEST    29             ; TEST FIRMWARE CRC
              FIRMCSTEST     28             ; TEST FIRMWARE CHECK SUM

              CALFUNCPTR     27
              CALFUNCCMD     26
              CALFUNCSTA     25
              READADDONTY    24
              FIRMWAREMID    23             ; GENERATE FIRMWARE MID
              CHKFILLETYPE   22             ; check type de fille existante

              BRA      APPLIWARE

              END

