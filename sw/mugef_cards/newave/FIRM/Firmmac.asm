;**************************************************************************** 
;** SET CURRENT DIAGNOSTIC NUMBER TO LEDIAG AND FIRMDIAG 
;**************************************************************************** 

SETDIAGFIRM:  MACRO
              MOVE.L   #\1,D7
              ANDI.L   #$0000001F,D7
              ORI.L    #$20,D7
              MOVE.B   D7,REGLEDIAG
              MOVE.W   D7,REGMADDON
              ENDM

              PAGE

;****************************************************************************
;** TEST MAGIC FIRMWARE NUMBER
;****************************************************************************

MAGICFIRMTEST:MACRO
              SETDIAGFIRM  \1                   ;/diag test bootware size magic number
              MOVE.L   #MAGFIRMSTART,A0
              MOVE.L   #MAGFIRMEND,A1
              MOVE.L   #MAGICSTARTF,A2
              MOVE.L   #MAGICENDF,A3
              MOVE.L   A1,D0
              SUB.L    A0,D0
              MOVE.L   A3,D1
              SUB.L    A2,D1
              CMP.L    D0,D1
              BNE      MERR\@

M2\@:         MOVE.L   #MAGFIRMSTART,A0
              MOVE.L   #MAGFIRMEND,A1
              MOVE.L   #MAGICSTARTF,A2
              MOVE.L   #MAGICENDF,A3
M21\@:        MOVE.B   (A0)+,D0
              MOVE.B   (A2)+,D1
              CMP.B    D0,D1
              BNE      MERR\@
              CMP.L    A0,A1
              BNE      M21\@

MOK\@:        BRA      MEND\@

MERR\@:       MOVE.W   #$0700,SR            ; set MUP in user mode
              ORI.W    #4800,REGMSTACMD     ; set test result bad
              ANDI.W   #$7FFE,REGMSTACMD    ; set MUP in halt state
              STOP     #$0700

MAGFIRMSTART: EQU      *
              DC.B     'Ceci est le nombre magique du FIRMWARE NEWAVE 3.1 :'
              DC.B     '  Une personne bien equilibree est une personne qui'
              DC.B     '  peut faire deux fois la meme erreur sans devenir'
              DC.B     '  nerveuse'
              DC.B     'Espoir   :'
              DC.B     '  SEMANAZ Yael Galaad Francois'
              DC.B     '  3 Decembre 1994 - ...'
              DC.B     'SEMANAZ Philippe B&S / CERN SL/PC-CS le 1er Octobre 1996'
MAGFIRMEND:   EQU      *

              LONGEVEN

MEND\@:       EQU      *
              ENDM

              PAGE

;*****************************************************************************
;*** CHECK FIRMWARE CRC
;*****************************************************************************

FIRMCRCTEST:  MACRO
              SETDIAGFIRM  \1
              ENDM
 
              PAGE

;*****************************************************************************
;*** CHECK FIRMWARE CHECKSUM
;*****************************************************************************

FIRMCSTEST:   MACRO
              SETDIAGFIRM  \1
              ENDM

              PAGE

;*****************************************************************************
;*** CALCUL VALEUR POINTEUR DES NOUVELLES VALEURS DEPUIS VME
;*****************************************************************************
CALFUNCPTR:   MACRO
              SETDIAGFIRM  \1
              MOVE.L   GLOBALPTR,A2
              MOVE.L   A2,PTRFUNCPTR
              CLR.L    D0
              MOVE.L   D0,(A2)+             ; pointer channel 1
              MOVE.L   D0,(A2)+             ; pointer channel 2
              MOVE.L   D0,(A2)+             ; pointer channel 3
              MOVE.L   D0,(A2)+             ; pointer channel 4
              MOVE.L   D0,(A2)+             ; pointer channel 5
              MOVE.L   D0,(A2)+             ; pointer channel 6
              MOVE.L   D0,(A2)+             ; pointer channel 7
              MOVE.L   D0,(A2)+             ; pointer channel 8
              MOVE.L   A2,GLOBALPTR
              ENDM

              PAGE

;*****************************************************************************
;*** CALCUL VALEUR POINTEUR COMMAND
;*****************************************************************************
CALFUNCCMD:   MACRO
              SETDIAGFIRM  \1
              MOVE.L   GLOBALPTR,A2
              MOVE.L   A2,PTRFUNCCMD
              CLR.L    D0
              MOVE.L   D0,(A2)+             ; semaphore

              MOVE.L   A2,GLOBALPTR
              ENDM

              PAGE

;*****************************************************************************
;*** CALCUL VALEUR POINTEUR STATUS
;*****************************************************************************
CALFUNCSTA:   MACRO
              SETDIAGFIRM  \1
              MOVE.L   GLOBALPTR,A2
              MOVE.L   A2,PTRFUNCSTA
              CLR.L    D0
              MOVE.L   D0,(A2)+             ; status
              MOVE.L   A2,GLOBALPTR
              ENDM

              PAGE

;*****************************************************************************
;*** LECTURE MODULE IDENTIFIER DE LA CARTE FILLE
;*****************************************************************************
READADDONTY:  MACRO
              SETDIAGFIRM  \1
              MOVE.W   #2,REGSELCHAN

;***    LECTURE DES PARAMETRES GENERAUX DE LA CARTE FILLE
;*****************************************************************************
              CLR.L    D0
              MOVE.W   REGADDONTYPE,D0      ; lecture du type de carte fille
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONTYPE
              CLR.L    D0
              MOVE.W   REGADDONSUB,D0       ; lecture du sous type de carte
              ANDI.W   #$FF,D0              ;  fille dans le type concerne
              MOVE.L   D0,ADDONSUB
              CLR.L    D0
              MOVE.W   REGADDONTRIG,D0      ; lecture des triggers utilisables
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONTRIG
              CLR.L    D0
              MOVE.W   REGADDONHARD,D0      ; lecture des canaux installes
              ANDI.W   #$FF,D0              ;  ou installables
              MOVE.L   D0,ADDONHARDPAT
              CLR.L    D0
              MOVE.W   REGADDONUSEA,D0      ; lecture des canaux utilisables
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONUSEAPAT
              CLR.L    D0
              MOVE.W   REGADDONRESER,D0     ; rerserved
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONRESER 
              CLR.L    D0
              MOVE.W   REGADDONTSTAA,D0     ; test byte to 0xAA
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONTSTAA 
              CLR.L    D0
              MOVE.W   REGADDONTST55,D0     ; test byte to 0x55
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONTST55 

;***    LECTURE DES PARAMETRES CANAUX DE LA CARTE FILLE
;*****************************************************************************
              CLR.L    D0
              MOVE.W   REGADDONCHAN1,D0     ; lecture caracteristiques canal 1
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN1
              CLR.L    D0
              MOVE.W   REGADDONCHAN2,D0     ; lecture caracteristiques canal 2
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN2
              CLR.L    D0
              MOVE.W   REGADDONCHAN3,D0     ; lecture caracteristiques canal 3
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN3
              CLR.L    D0
              MOVE.W   REGADDONCHAN4,D0     ; lecture caracteristiques canal 4
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN4
              CLR.L    D0
              MOVE.W   REGADDONCHAN5,D0     ; lecture caracteristiques canal 5
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN5
              CLR.L    D0
              MOVE.W   REGADDONCHAN6,D0     ; lecture caracteristiques canal 6
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN6
              CLR.L    D0
              MOVE.W   REGADDONCHAN7,D0     ; lecture caracteristiques canal 7
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN7
              CLR.L    D0
              MOVE.W   REGADDONCHAN8,D0     ; lecture caracteristiques canal 8
              ANDI.W   #$FF,D0
              MOVE.L   D0,ADDONCHAN8

              MOVE.W   #0,REGSELCHAN

;***    CALCUL DU NOMBRE DE CANAL INSTALLE OU INSTALLABLE ET UTILISABLE
;*****************************************************************************
              MOVE.L   ADDONHARDPAT,D0
              CLR.L    D1
              CLR.L    D2
              LSR.L    #1,D0                ; set canal 1 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 2 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 3 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 4 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 5 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 6 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 7 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 8 bit in X
              ADDX.L   D2,D1
              MOVE.L   D1,ADDONHARDNUM

              MOVE.L   ADDONUSEAPAT,D0
              CLR.L    D1
              CLR.L    D2
              LSR.L    #1,D0                ; set canal 1 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 2 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 3 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 4 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 5 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 6 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 7 bit in X
              ADDX.L   D2,D1
              LSR.L    #1,D0                ; set canal 8 bit in X
              ADDX.L   D2,D1
              MOVE.L   D1,ADDONUSEANUM
              ENDM

              PAGE

;*****************************************************************************
;*** GENERATE HEX AND ASCII FIRMWARE MODULE IDENTIFIER
;*****************************************************************************
FIRMWAREMID:  MACRO
              SETDIAGFIRM  \1
              MOVE.L   MIDADDPTR,A0
              MOVE.L   GLOBALPTR,A1
              MOVE.L   A1,(A0)+
              MOVE.L   A0,MIDADDPTR

              MOVE.L   #FIRMMIDHEXST,A3
              MOVE.L   #FIRMMIDHEXEN,A4
M1\@:         MOVE.L   (A3)+,(A1)+
              CMP.L    A3,A4
              BNE.S    M1\@
              MOVE.L   A1,GLOBALPTR

              MOVE.L   MIDADDPTR,A0
              MOVE.L   GLOBALPTR,A1
              MOVE.L   A1,(A0)+
              MOVE.L   A0,MIDADDPTR

              MOVE.L   #FIRMMIDASCST,A3
              MOVE.L   #FIRMMIDASCEN,A4
M2\@:         MOVE.L   (A3)+,(A1)+
              CMP.L    A3,A4
              BNE.S    M2\@
              MOVE.L   A1,GLOBALPTR
              ENDM

              PAGE

;*****************************************************************************
;*** CHECK TYPE DE CARTE FILLE EXISTANTE
;*****************************************************************************
CHKFILLETYPE: MACRO
              SETDIAGFIRM  \1
              MOVE.L   ADDONTYPE,D0
              CMPI.B   #0,D0      ; carte de type : DUMMYDAC
              BEQ.S    MEND\@
              CMPI.B   #3,D0      ; carte de type : AFTER8
              BEQ.S    MEND\@
              CMPI.B   #4,D0      ; carte de type : MAINPS
              BEQ.S    MEND\@
;              CMPI.B   #5,D0      ; carte de type : RFDIG
;              BEQ.S    MEND\@
              CMPI.B   #6,D0      ; carte de type : DACHIRES
              BEQ.S    MEND\@
;              CMPI.B   #7,D0      ; carte de type : DOC32BIT
;              BEQ.S    MEND\@
              CMPI.B   #8,D0      ; carte de type : 8 DOC32BIT
              BEQ.S    MEND\@


MERR\@:       MOVE.W   #$0700,SR            ; set MUP in user mode
              ORI.W    #4080,REGMSTACMD     ; set test result bad
              ANDI.W   #$7FDE,REGMSTACMD    ; set MUP in halt state
              STOP     #$0700

MEND\@:       EQU      *
              ANDI.W   #$3FDF,REGMSTACMD    ; set test result ok
              ENDM

              END

