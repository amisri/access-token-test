;****************************************************************************
;** APPLIWARE PROGRAM
;****************************************************************************
APPLCMDMSK    DS.L    1                  ; internal command mask
APPLSTATUS    DS.L    1                  ; internal status

APPLIWARE:    EQU     *

;** SET STATUS AND SEMAPHORE TO OK
;*********************************
              MOVE.B  #0,REGLEDIAG
              CLR.L   D0
              CLR.L   D1
              CLR.L   D2
              MOVE.L  PTRFUNCPTR,A0       ; A0 pointer to new VME value
              MOVE.L  PTRFUNCPTR,A1       ; A1 pointer to new VME value
              MOVE.L  PTRFUNCCMD,A2       ; A2 pointer to command mask
              MOVE.L  PTRFUNCSTA,A3       ; A3 pointer to status
              MOVE.L  #APPLSTATUS,A4      ; A4 pointer to internal status
              MOVE.L  #APPLCMDMSK,A5      ; A5 pointer to internal command mask
              MOVE.W  #$0000,(A2)         ; set command mask to no update
              MOVE.W  #$0000,(A3)         ; set status to ok 
              MOVE.W  #$0000,(A4)         ; set internal status to ok 
              MOVE.W  #$0000,REGMADDON    ; set addon status to ok
              MOVE.W  #$403F,REGMSTACMD   ; set status valid, ok, appli                 

                        
;** SET MS INTERRUPT TO POINT THE RIGHT INTERFACE
;************************************************
APPLADDON:    EQU     *
              MOVE.L  ADDONTYPE,D0

              CMPI.B  #0,D0               ; carte de type : DUMMYDAC (0)
              BNE.S   APPLADDON1
              MOVE.L  #DUMMYDAC,AUV1D
              BRA.L   APPLADDONED

APPLADDON1:   EQU     *
              CMPI.B  #3,D0               ; carte de type : AFTER8 (3)
              BNE.S   APPLADDON2
              MOVE.L  #AFTER8,AUV1D
              BRA.L   APPLADDONED

APPLADDON2:   EQU     *
              CMPI.B  #4,D0               ; carte de type : MAINPS (4)
              BNE.S   APPLADDON3
              MOVE.L  #MAINPS,AUV1D
              BRA.L   APPLADDONED

APPLADDON3:   EQU     *
              CMPI.B  #5,D0               ; carte de type : RFDIG (5)
              BNE.S   APPLADDON4
              MOVE.L  #RFDIG,AUV1D
              BRA.L   APPLADDONED

APPLADDON4:   EQU     *
              CMPI.B  #6,D0               ; carte de type : DACHIRES (6)
              BNE.S   APPLADDON5
              MOVE.L  #DACHIRES,AUV1D
              BRA.L   APPLADDONED

APPLADDON5:   EQU     *
              CMPI.B  #7,D0               ; carte de type : DOC32BIT (7)
              BNE.S   APPLADDON6
              MOVE.L  #DOC32BIT,AUV1D
              BRA.L   APPLADDONED

APPLADDON6:   EQU     *
              CMPI.B  #8,D0               ; carte de type : 8 DOC32BIT (8)
              BNE.S   APPLADDONER
              MOVE.L  #DOC32BIT8,AUV1D
              BRA.L   APPLADDONED


APPLADDONER:  MOVE.W  #$0700,SR           ; set MUP in user mode
              ORI.W   #4080,REGMSTACMD    ; set test result bad
              ANDI.W  #$7FDE,REGMSTACMD   ; set MUP in halt state
              STOP    #$0700

APPLADDONED:  EQU     *
              ORI.B   #$10,D0
              MOVE.B  D0,REGLEDIAG

;** ENABLE THE MS INTERRUPT AND WAIT FOR INTERRUPT
;************************************************
              MOVE.W  #$0000,REGIMENINT   ; disable all interrupt sources
              MOVE.W  #$0000,REGSELDIVMS  ; disable sub millisecond
              MOVE.W  #$FFFF,REGIMININT   ; clear all interrupts pending
              MOVE.W  #$0400,REGIMENINT   ; enable interrupt ms

WAITINTMS:    MOVE.L  #STACKSTART,A7
              MOVE.W  #$FFFF,REGIMININT   ; clear all interrupts pending
              STOP    #$2000

;** DUMMY DAC (TYPE 0)
;*********************
DUMMYDAC:    EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2  
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0)+,D0            ; read update value DAC1
             MOVE.L   (A0)+,D1            ; read update value DAC2
             MOVE.L   (A0)+,D2            ; read update value DAC3
             MOVE.L   (A0)+,D3            ; read update value DAC4
             MOVE.L   (A0)+,D4            ; read update value DAC5
             MOVE.L   (A0)+,D5            ; read update value DAC6
             MOVE.L   (A0)+,D6            ; read update value DAC7
             MOVE.L   (A0),D7             ; read update value DAC8
             MOVE.L   A1,A0               ; init pointer to new value

             MOVE.W   #$0007,(A4)         ; set write to dac flag

             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000
                 
;** AFTER8 (TYPE 3)
;******************
AFTER8:      EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2  
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0)+,D0            ; read update value DAC1
             MOVE.L   (A0)+,D1            ; read update value DAC2
             MOVE.L   (A0)+,D2            ; read update value DAC3
             MOVE.L   (A0)+,D3            ; read update value DAC4
             MOVE.L   (A0)+,D4            ; read update value DAC5
             MOVE.L   (A0)+,D5            ; read update value DAC6
             MOVE.L   (A0)+,D6            ; read update value DAC7
             MOVE.L   (A0),D7             ; read update value DAC8
             MOVE.L   A1,A0               ; init pointer to new value

             MOVE.W   #$0007,(A4)         ; set write to dac flag
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac

AFTER81:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER82             ; branch if no update requested for this channel
             SWAP     D0                  ; normalize value for DAC1
             MOVE.W   D0,REGCHAN1         ; write value to DAC1
AFTER82:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER83             ; branch if no update requested for this channel
             SWAP     D1                  ; normalize value for DAC2
             MOVE.W   D1,REGCHAN2         ; write value to DAC2
AFTER83:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER84             ; branch if no update requested for this channel
             SWAP     D2                  ; normalize value for DAC3
             MOVE.W   D2,REGCHAN3         ; write value to DAC3
AFTER84:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER85             ; branch if no update requested for this channel
             SWAP     D3                  ; normalize value for DAC4
             MOVE.W   D3,REGCHAN4         ; write value to DAC4
AFTER85:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER86             ; branch if no update requested for this channel
             SWAP     D4                  ; normalize value for DAC5
             MOVE.W   D4,REGCHAN5         ; write value to DAC5
AFTER86:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER87             ; branch if no update requested for this channel
             SWAP     D5                  ; normalize value for DAC6
             MOVE.W   D5,REGCHAN6         ; write value to DAC6
AFTER87:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER88             ; branch if no update requested for this channel
             SWAP     D6                  ; normalize value for DAC7
             MOVE.W   D6,REGCHAN7         ; write value to DAC7
AFTER88:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      AFTER8E             ; branch if no update requested for this channel
             SWAP     D7                  ; normalize value for DAC8
             MOVE.W   D7,REGCHAN8         ; write value to DAC8

AFTER8E:     MOVE.W   #$0000,REGSELCHAN   ; disable access to dac

             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             NOP                          ; in case of ms interrupt pending
             NOP                          ; in case of ms interrupt pending
             NOP                          ; in case of ms interrupt pending
             NOP                          ; in case of ms interrupt pending
             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000              ; wait for next ms interrupt
             NOP
             NOP

;** MAINPS (TYPE 4)
;******************
MAINPS:      EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0),D0             ; read update value DAC1

             MOVE.W   #$0007,(A4)         ; set write to dac flag
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac

MAINPS1:     LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      MAINPSE             ; branch if no update requested for this channel
             LSL.L    #1,D0               ; suppression bit de signe
             SWAP     D0                  ; extraction data CANAL principal
             MOVE.W   D0,REGCHAN1         ; set data on CANAL principal
             SWAP     D0
             LSR.W    #7,D0               ; alignement a droite des bits 
             NEG.W    D0                  ; normalisation correction
             MOVE.W   D0,REGCHAN2         ; set data on CANAL correction

MAINPSE:     MOVE.W   #$0000,REGSELCHAN   ; disable access to dac

             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000

;** RFDIG (TYPE 5)
;*****************
RFDIG:       EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0)+,D0            ; read update value DAC1
             MOVE.L   (A0),D1             ; read update value DAC2
             MOVE.L   A1,A0               ; init pointer to new value

             MOVE.W   #$0007,(A4)         ; set write to dac flag
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac

RFDIG1:      LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      RFDIG2              ; branch if no update requested for this channel
             SWAP     D0                  ; normalize value for DAC1
             MOVE.W   D0,REGCHAN1         ; write value to DAC1
RFDIG2:      LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      RFDIGE              ; branch if no update requested for this channel
             SWAP     D1                  ; normalize value for DAC2
             MOVE.W   D1,REGCHAN2         ; write value to DAC2

RFDIGE:      MOVE.W   #$0000,REGSELCHAN   ; disable access to dac

             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000

;** DACHIRES (TYPE 6)
;********************
DACHIRES:    EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0),D0             ; read update value DAC1

             MOVE.W   #$0007,(A4)         ; set write to dac flag
             MOVE.W   #$0002,REGSELCHAN  ; set access to dac

DACHIRES1:   LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DACHIRESE           ; branch if no update requested for this channel
             SWAP     D0                  ; extraction data CANAL msb
             MOVE.W   D0,REGCHAN1         ; set data on CANAL principal
             SWAP     D0
             MOVE.W   D0,REGCHAN2         ; set data on CANAL lsb

DACHIRESE:   MOVE.W   #$0000,REGSELCHAN   ; disable access to dac

             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000

;** DOC32BIT (TYPE 7)
;********************
DOC32BIT:    EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0),D0             ; read update value DAC1

             MOVE.W   #$0007,(A4)         ; set write to dac flag
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac

DOC32BIT1:   LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BITE           ; branch if no update requested for this channel
             SWAP     D0                  ; extraction data CANAL msb
             MOVE.W   D0,REGCHAN1         ; set data on CANAL principal
             SWAP     D0
             MOVE.W   D0,REGCHAN2         ; set data on CANAL lsb

DOC32BITE:   MOVE.W   #$0000,REGSELCHAN   ; disable access to dac

             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000

;** 8 DOC32BIT (TYPE 8)
;**********************
DOC32BIT8:   EQU      *
             MOVE.W   REGIMININT,D1       ; read interrupt received register
             BTST.L   #10,D1              ; ms interrupt received
             BEQ.L    WAITINTMS           ; branch if spurious interrupt

             MOVE.W   (A2),(A5)           ; read command mask
             BEQ.L    WAITINTMS           ; branch if no update required
             MOVE.W   #$0000,(A2)         ; clear command mask for next update

             MOVE.W   (A4),D2
             OR.W     D2,(A3)             ; keep trace of status (NRZ)
             MOVE.W   (A3),D2
             LSL.W    #5,D2
             OR.B     D2,REGLEDIAG        ; set status on led
             MOVE.W   #$FFFF,REGIMININT   ; clear all interrupts pending
             MOVE.L   #STACKSTART,A7
             ANDI.W   #$F8FF,SR           ; set priority level to 0
             MOVE.W   #$0003,(A4)         ; set transfert dac value flag

;     TRANSFERT SUR DAC
;     =================
             MOVE.L   (A0)+,D0            ; read update value DAC1
             MOVE.L   (A0)+,D1            ; read update value DAC2
             MOVE.L   (A0)+,D2            ; read update value DAC3
             MOVE.L   (A0)+,D3            ; read update value DAC4
             MOVE.L   (A0)+,D4            ; read update value DAC5
             MOVE.L   (A0)+,D5            ; read update value DAC6
             MOVE.L   (A0)+,D6            ; read update value DAC7
             MOVE.L   (A0),D7             ; read update value DAC8
             MOVE.L   A1,A0               ; init pointer to new value

             MOVE.W   #$0007,(A4)         ; set write to dac flag

DOC32BIT81:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT82          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D0                  ; normalize value for DAC1
             MOVE.W   D0,REGCHAN1         ; write value to DAC1
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D0                  ; normalize value for DAC1
             MOVE.W   D0,REGCHAN1         ; write value to DAC1

DOC32BIT82:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT83          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D1                  ; normalize value for DAC2
             MOVE.W   D1,REGCHAN2         ; write value to DAC2
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D1                  ; normalize value for DAC2
             MOVE.W   D1,REGCHAN2         ; write value to DAC2

DOC32BIT83:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT84          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D2                  ; normalize value for DAC3
             MOVE.W   D2,REGCHAN3         ; write value to DAC3
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D2                  ; normalize value for DAC3
             MOVE.W   D2,REGCHAN3         ; write value to DAC3

DOC32BIT84:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT85          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D3                  ; normalize value for DAC4
             MOVE.W   D3,REGCHAN4         ; write value to DAC4
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D3                  ; normalize value for DAC4
             MOVE.W   D3,REGCHAN4         ; write value to DAC4

DOC32BIT85:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT86          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D4                  ; normalize value for DAC5
             MOVE.W   D4,REGCHAN5         ; write value to DAC5
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D4                  ; normalize value for DAC5
             MOVE.W   D4,REGCHAN5         ; write value to DAC5

DOC32BIT86:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT87          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D5                  ; normalize value for DAC6
             MOVE.W   D5,REGCHAN6         ; write value to DAC6
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D5                  ; normalize value for DAC6
             MOVE.W   D5,REGCHAN6         ; write value to DAC6

DOC32BIT87:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT88          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D6                  ; normalize value for DAC7
             MOVE.W   D6,REGCHAN7         ; write value to DAC7
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D6                  ; normalize value for DAC7
             MOVE.W   D6,REGCHAN7         ; write value to DAC7

DOC32BIT88:  LSR.W    (A5)                ; shift right one bit command mask to test the lsb bit and check the update request
             BCC      DOC32BIT8E          ; branch if no update requested for this channel
             MOVE.W   #$0002,REGSELCHAN   ; set access to dac
             SWAP     D7                  ; normalize value for DAC8
             MOVE.W   D7,REGCHAN8         ; write value to DAC8
             MOVE.W   #$0008,REGSELCHAN   ; set access to dac
             SWAP     D7                  ; normalize value for DAC8
             MOVE.W   D7,REGCHAN8         ; write value to DAC8

DOC32BIT8E:  MOVE.W   #$0000,REGSELCHAN   ; disable access to dac

             MOVE.W   #$0000,(A4)         ; clear update, transfert value & write to dac flag
             STOP     #$2000


              ALIGN    4 
                
FIRMEND:      EQU      *

              PAGE

              END

