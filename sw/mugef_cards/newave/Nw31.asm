

              TITLE  "NEWAVE3.1_V0100"
;             Ver: 0.1.0.0              LE 26/11/2009   P. SEMANAZ
              PAGE


              XDEF      BOOTSTART
              
BOOTVERSEXT   EQU      $30303031
FIRMVERSEXT   EQU      $30313030

              INCLUDE   "board\hardmap.asm"
              INCLUDE   "mup\vectable.asm"
              INCLUDE   "mup\stacksup.asm"            
              INCLUDE   "boot\bootdata.asm"
              INCLUDE   "boot\bootmac.asm"
              INCLUDE   "boot\bootpgm.asm"

              INCLUDE   "firm\firmdata.asm"
              INCLUDE   "firm\firmmac.asm"
              INCLUDE   "firm\firmpgm.asm"

              INCLUDE   "appl\applpgm.asm"

              
              END

