/*---------------------------------------------------------------------------------------------------------*\
  File:         cc_types.h

  Purpose:      Typedef for the TMS320C32 compiler

 From TMS320C3x/C4x Optimizing C Compiler User�s Guide - Literature Number: SPRU034H June1998

Table 3�1. TMS320C3x/C4x C Data Types
Type                    Size    Representation          Range
                                                Minimum         Maximum
char, signed char       32 bits ASCII           �2147483648     2147483647
unsigned char           32 bits ASCII           0               4294967295
short                   32 bits 2s complement   �2147483648     2147483647
unsigned char           32 bits binary          0               4294967295
int, signed int         32 bits 2s complement   �2147483648     2147483647
unsigned int            32 bits binary          0               4294967295
long, signed long       32 bits 2s complement   �2147483648     2147483647
unsigned long           32 bits binary          0               4294967295
enum                    32 bits 2s complement   �2147483648     2147483647
float                   32 bits TMS320C3x/C4x   5.877472e�39    3.4028235e38
double                  32 bits TMS320C3x/C4x   5.877472e�39    3.4028235e38
long double             40 bits TMS320C3x/C4x   5.87747175e�39  3.4028236684e38
pointers                32 bits binary          0               0xFFFFFFFF

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CC_TYPES_H	// header encapsulation
#define CC_TYPES_H

// ----- Typedefs -----


// TMS320C32 only support 32 bits

// FAKE
typedef unsigned char	        INT8U;		// Unsigned  8-bit quantity [0:255]
typedef signed char		INT8S;		// Signed    8-bit quantity [-128:127]

// FAKE
typedef unsigned int		INT16U;		// Unsigned 16-bit quantity [0:65535]
typedef signed int		INT16S;		// Signed   16-bit quantity [-32768:32767]

// typedef unsigned 		INT32U;		// Unsigned 32-bit quantity [0:4294967295]
// typedef signed   		INT32S;		// Signed   32-bit quantity [-2147483648:2147483647]
typedef unsigned long           INT32U;         // Unsigned 32-bit quantity [0:4294967295]
typedef signed long             INT32S;         // Signed   32-bit quantity [-2147483648:2147483647]

// FAKE
typedef unsigned long 		INT64U;
typedef signed long 		INT64S;

// Note: Q.K. uses in the __HC16__ code BOOLEAN as INT16U
typedef unsigned char		BOOLEAN;	// Flag value (TRUE/FALSE)
#define FALSE			0
#define TRUE			1

#ifndef _STDINT_H_
typedef INT32U                  uintptr_t;      // used to cast a pointer in an integer container
#endif

// only used to communicate IEEE FP32 to the TMS320C32 via the Dual Port RAM
// because TMS320C32 not support IEEE format we place here then we convert to internal format
typedef unsigned		DP_IEEE_FP32;

typedef float                   FP32;           // Single precision floating point
typedef long double		FP40;
// FAKE
typedef FP40			FP64;		// Double precision floating point

typedef volatile unsigned char	REG8U;		// Unsigned  8-bit register
typedef volatile signed   int   REG16S;		// Signed   16-bit register
typedef volatile unsigned int   REG16U;		// Unsigned 16-bit register
typedef volatile unsigned long	REG32U;		// Unsigned 32-bit register


// FAR and NEAR are not defined for us
#define FAR
#define NEAR

typedef char * 			PCHARFAR;	// Far pointer to character (string)

#define BIT_FIELD       int     // The Texas TMS320C6x compiler doesn't support unsigned char as a bit field

#define INLINE inline

#endif	// CC_TYPES_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cc_types.h
\*---------------------------------------------------------------------------------------------------------*/
