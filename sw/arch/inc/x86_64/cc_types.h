/*---------------------------------------------------------------------------------------------------------*\
  File:         cc_types.h

  Purpose:      Typedef for the GNU GCC compiler
                under Linux OS
                for L866 target

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CC_TYPES_H	// header encapsulation
#define CC_TYPES_H


#include <stdint.h> 
#include <stdbool.h>

// char is char, nor signed nor unsigned
typedef uint8_t 		INT8U;	// Unsigned  8 bit quantity [0:255]
typedef int8_t  		INT8S;	// Signed    8 bit quantity [-128:127]

typedef uint16_t		INT16U;	// Unsigned 16 bit quantity [0:65535]           64Kb
typedef int16_t 		INT16S;	// Signed   16 bit quantity [-32768:32767]      32Kb

// int = signed long int = INT32S  for this compiler

typedef uint32_t		INT32U;	// Unsigned 32 bit quantity [0:4294967295]              4Gb
typedef int32_t 		INT32S;	// Signed   32 bit quantity [-2147483648:2147483647]    2Gb

typedef uint64_t        	INT64U;	// Unsigned 64 bit quantity
typedef int64_t         	INT64S;	// Signed   64 bit quantity

typedef bool                    BOOLEAN;

typedef float                   DP_IEEE_FP32;

typedef float			FP32;	// Single precision floating point
typedef double			FP64;	// Double precision floating point
typedef long double		FP128;	// 12 bytes floating point


typedef volatile unsigned char	REG8U;		// Unsigned  8-bit register
typedef volatile signed   short	REG16S;		// Signed   16-bit register
typedef volatile unsigned short	REG16U;		// Unsigned 16-bit register
typedef volatile unsigned int	REG32U;		// Unsigned 32-bit register

// to share code while compiling for other processors that can have different address pointers sizes
// for example for GNU GCC C compiler for M32C target the far is not defined
#define FAR
#define NEAR

typedef char * 		        PCHARFAR;	// Far pointer to character (string)

#define  FALSE			0
#define  TRUE			1

#define BIT_FIELD       INT16U

//SLC5 uses GCC 4.1.2 (<4.6)
#ifndef __BYTE_ORDER__
#define __ORDER_LITTLE_ENDIAN__ 1111
#define __ORDER_BIG_ENDIAN__ 2222
#define __ORDER_PDP_ENDIAN__ 3333

//this line will be arch dependent
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__

#endif //__BYTE_ORDER__

#define INLINE inline

#endif	// CC_TYPES_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cc_types.h
\*---------------------------------------------------------------------------------------------------------*/
