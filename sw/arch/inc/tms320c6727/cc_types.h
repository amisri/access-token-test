/*---------------------------------------------------------------------------------------------------------*\
  File:         cc_types.h

  Purpose:      Typedef for the TMS320C6x compiler

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CC_TYPES_H  // header encapsulation
#define CC_TYPES_H

// ----- Typedefs -----


typedef unsigned char           INT8U;      // Unsigned  8-bit quantity [0:255]
typedef char                    INT8S;      // Signed    8-bit quantity [-128:127]

typedef unsigned short          INT16U;     // Unsigned 16-bit quantity [0:65535]
typedef short                   INT16S;     // Signed   16-bit quantity [-32768:32767]

typedef unsigned int            INT32U;     // Unsigned 32-bit quantity [0:4294967295]
typedef int                     INT32S;     // Signed   32-bit quantity [-2147483648:2147483647]

typedef unsigned char           BOOLEAN;    // Flag value (TRUE/FALSE)
// Note: Q.K. uses in the __HC16__ code BOOLEAN as INT16U

typedef unsigned long           INT40U;     // Unsigned 40-bit quantity
typedef signed long             INT40S;     // Signed   40-bit quantity

typedef unsigned long long      INT64U;
typedef signed long long        INT64S;

// only used to communicate IEEE FP32 to the TMS320C32 via the Dual Port RAM
typedef float                   DP_IEEE_FP32;

typedef float                   FP32;       // Single precision floating point
typedef double                  FP64;       // Double precision floating point

typedef volatile unsigned char      REG8U;      // Unsigned  8-bit register
typedef volatile signed   short     REG16S;     // Signed   16-bit register
typedef volatile unsigned short     REG16U;     // Unsigned 16-bit register
typedef volatile unsigned int       REG32U;     // Unsigned 32-bit register
typedef volatile signed int         REG32S;     // Signed 32-bit register
typedef volatile unsigned long long REG64U;
typedef volatile signed long long   REG64S;

// FAR and NEAR are not defined for us
#define FAR
#define NEAR

typedef char *          PCHARFAR;   // Far pointer to character (string)

#define  FALSE          0
#define  TRUE           1

#define BIT_FIELD       int     // The Texas TMS320C6x compiler doesn't support unsigned char as a bit field

//GCC endianess macros
#ifndef __BYTE_ORDER__
#define __ORDER_LITTLE_ENDIAN__ 1111
#define __ORDER_BIG_ENDIAN__ 2222
#define __ORDER_PDP_ENDIAN__ 3333

//this line will be arch dependent
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__

#endif  //__BYTE_ORDER__

#define INLINE inline

#endif  // CC_TYPES_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cc_types.h
\*---------------------------------------------------------------------------------------------------------*/
