/*---------------------------------------------------------------------------------------------------------*\
  File:         structs_bits_big.h

  Purpose:      Global structures and types declarations
                This files describes how we access words, dwords from the BIG ENDIAN
		point of view

		so this is for BIG ENDIAN processors

		there is a similar file prepared for the LITTLE ENDIAN

 I tested this file for
 	PPC4, Power PC on the RIOS 806x VME cards under Lynx OS


\*---------------------------------------------------------------------------------------------------------*/

#ifndef _STRUCTS_BITS_BIG_H_	// header encapsulation
#define _STRUCTS_BITS_BIG_H_

#include <cc_types.h>

/*

    LITTLE ENDIAN format so LO goes to the lowest address
	INTEL
	M32C87
	M32C83
	M16C62

   BIG ENDIAN format so HI goes to the lowest address
	MOTOROLA
	PPC4, Power PC on the RIOS 806x VME cards under Lynx OS


                     start address     +1  --->     ......          top address
                       0000           0001			      FFFF
big endian             HI part        LO part
little endian          LO part        HI part

*/


// --- Bits, Bytes, Words, DWords ---

//-----------------------------------------------------------------------------------------------------------

// this is the natural order for this processor
struct T8Bits
{
    BIT_FIELD b7 : 1; // b7
    BIT_FIELD b6 : 1; // b6
    BIT_FIELD b5 : 1; // b5
    BIT_FIELD b4 : 1; // b4
    BIT_FIELD b3 : 1; // b3
    BIT_FIELD b2 : 1; // b2
    BIT_FIELD b1 : 1; // b1
    BIT_FIELD b0 : 1; // b0
};

union TUnion8Bits
{
    INT8U		byte;
    struct T8Bits	bit;
};

//-----------------------------------------------------------------------------------------------------------


// when this processor acces a memory written by a LITTLE ENDIAN processor
// the bits are like this
struct TLittleEndian_16Bits
{
    BIT_FIELD b7  : 1;
    BIT_FIELD b6  : 1;
    BIT_FIELD b5  : 1;
    BIT_FIELD b4  : 1;
    BIT_FIELD b3  : 1;
    BIT_FIELD b2  : 1;
    BIT_FIELD b1  : 1;
    BIT_FIELD b0  : 1;

    BIT_FIELD b15 : 1;
    BIT_FIELD b14 : 1;
    BIT_FIELD b13 : 1;
    BIT_FIELD b12 : 1;
    BIT_FIELD b11 : 1;
    BIT_FIELD b10 : 1;
    BIT_FIELD b9  : 1;
    BIT_FIELD b8  : 1;
};

// this is the natural order for this processor
struct TBigEndian_16Bits
{
    BIT_FIELD b15 : 1; // b15   Addr 1(Hi) b7
    BIT_FIELD b14 : 1; // b14   Addr 1(Hi) b6
    BIT_FIELD b13 : 1; // b13   Addr 1(Hi) b5
    BIT_FIELD b12 : 1; // b12   Addr 1(Hi) b4
    BIT_FIELD b11 : 1; // b11   Addr 1(Hi) b3
    BIT_FIELD b10 : 1; // b10   Addr 1(Hi) b2
    BIT_FIELD b9  : 1; // b9    Addr 1(Hi) b1
    BIT_FIELD b8  : 1; // b8    Addr 1(Hi) b0 --- Hi

    BIT_FIELD b7  : 1; // b7    Addr 0(Lo) b7
    BIT_FIELD b6  : 1; // b6    Addr 0(Lo) b6
    BIT_FIELD b5  : 1; // b5    Addr 0(Lo) b5
    BIT_FIELD b4  : 1; // b4    Addr 0(Lo) b4
    BIT_FIELD b3  : 1; // b3    Addr 0(Lo) b3
    BIT_FIELD b2  : 1; // b2    Addr 0(Lo) b2
    BIT_FIELD b1  : 1; // b1    Addr 0(Lo) b1
    BIT_FIELD b0  : 1; // b0	Addr 0(Lo) b0 --- Lo
};



// when this processor acces a memory written by a LITTLE ENDIAN processor
// the bits are like this
struct TLittleEndian_16bits_lo_hi
{
    INT8U	lo;	// Addr 0(Lo)
    INT8U	hi;	// Addr 1(Hi)
};

// this is the natural order for this processor
struct TBigEndian_16bits_lo_hi
{
    INT8U	hi; 	// Addr 0(Hi)
    INT8U	lo;     // Addr 1(Lo)
};

// this is the natural order for this processor
union TUnion16Bits
{
    INT16U				word;		// natural, big endian value
    INT16S				int16s;
    INT8U				byte[2];
    struct TBigEndian_16Bits		bit;		// natural
    struct TLittleEndian_16Bits		bit_le;		// little endian
    struct TBigEndian_16Bits     	bit_be;         // big endian, natural
    struct TBigEndian_16bits_lo_hi	part;          	// natural
    struct TLittleEndian_16bits_lo_hi	le;		// little endian
    struct TBigEndian_16bits_lo_hi	be;             // big endian, natural
};

//-----------------------------------------------------------------------------------------------------------

// when this processor acces a memory written by a LITTLE ENDIAN processor
// the bits are like this
struct TLittleEndian_32Bits
{
    BIT_FIELD b7  : 1;
    BIT_FIELD b6  : 1;
    BIT_FIELD b5  : 1;
    BIT_FIELD b4  : 1;
    BIT_FIELD b3  : 1;
    BIT_FIELD b2  : 1;
    BIT_FIELD b1  : 1;
    BIT_FIELD b0  : 1;

    BIT_FIELD b15 : 1;
    BIT_FIELD b14 : 1;
    BIT_FIELD b13 : 1;
    BIT_FIELD b12 : 1;
    BIT_FIELD b11 : 1;
    BIT_FIELD b10 : 1;
    BIT_FIELD b9  : 1;
    BIT_FIELD b8  : 1;

    BIT_FIELD b23 : 1;
    BIT_FIELD b22 : 1;
    BIT_FIELD b21 : 1;
    BIT_FIELD b20 : 1;
    BIT_FIELD b19 : 1;
    BIT_FIELD b18 : 1;
    BIT_FIELD b17 : 1;
    BIT_FIELD b16 : 1;

    BIT_FIELD b31 : 1;
    BIT_FIELD b30 : 1;
    BIT_FIELD b29 : 1;
    BIT_FIELD b28 : 1;
    BIT_FIELD b27 : 1;
    BIT_FIELD b26 : 1;
    BIT_FIELD b25 : 1;
    BIT_FIELD b24 : 1;
};

// this is the natural order for this processor
struct TBigEndian_32Bits
{
    BIT_FIELD b31 : 1; // b31   Addr 3(HiHi) b7
    BIT_FIELD b30 : 1; // b30   Addr 3(HiHi) b6
    BIT_FIELD b29 : 1; // b29   Addr 3(HiHi) b5
    BIT_FIELD b28 : 1; // b28   Addr 3(HiHi) b4
    BIT_FIELD b27 : 1; // b27   Addr 3(HiHi) b3
    BIT_FIELD b26 : 1; // b26   Addr 3(HiHi) b2
    BIT_FIELD b25 : 1; // b25   Addr 3(HiHi) b1
    BIT_FIELD b24 : 1; // b24   Addr 3(HiHi) b0 --- HiHi

    BIT_FIELD b23 : 1; // b23   Addr 2(HiLo) b7
    BIT_FIELD b22 : 1; // b22   Addr 2(HiLo) b6
    BIT_FIELD b21 : 1; // b21   Addr 2(HiLo) b5
    BIT_FIELD b20 : 1; // b20   Addr 2(HiLo) b4
    BIT_FIELD b19 : 1; // b19   Addr 2(HiLo) b3
    BIT_FIELD b18 : 1; // b18   Addr 2(HiLo) b2
    BIT_FIELD b17 : 1; // b17   Addr 2(HiLo) b1
    BIT_FIELD b16 : 1; // b16   Addr 2(HiLo) b0 --- HiLo

    BIT_FIELD b15 : 1; // b15   Addr 1(LoHi) b7
    BIT_FIELD b14 : 1; // b14   Addr 1(LoHi) b6
    BIT_FIELD b13 : 1; // b13   Addr 1(LoHi) b5
    BIT_FIELD b12 : 1; // b12   Addr 1(LoHi) b4
    BIT_FIELD b11 : 1; // b11   Addr 1(LoHi) b3
    BIT_FIELD b10 : 1; // b10   Addr 1(LoHi) b2
    BIT_FIELD b9  : 1; // b9    Addr 1(LoHi) b1
    BIT_FIELD b8  : 1; // b8    Addr 1(LoHi) b0 --- LoHi

    BIT_FIELD b7  : 1; // b7    Addr 0(LoLo) b7
    BIT_FIELD b6  : 1; // b6    Addr 0(LoLo) b6
    BIT_FIELD b5  : 1; // b5    Addr 0(LoLo) b5
    BIT_FIELD b4  : 1; // b4    Addr 0(LoLo) b4
    BIT_FIELD b3  : 1; // b3    Addr 0(LoLo) b3
    BIT_FIELD b2  : 1; // b2    Addr 0(LoLo) b2
    BIT_FIELD b1  : 1; // b1    Addr 0(LoLo) b1
    BIT_FIELD b0  : 1; // b0	Addr 0(LoLo) b0 --- LoLo
};

struct TLittleEndian_32bits_lo_hi
{
    INT16U	lo;	// Addr 0,1 Lo word
    INT16U	hi;	// Addr 2,3 Hi word
};

struct TBigEndian_32bits_lo_hi
{
    INT16U	hi; 	// Addr 0,1 Hi word
    INT16U	lo;     // Addr 2,3 Lo word
};

// when this processor acces a memory written by a LITTLE ENDIAN processor
// the bits are like this
struct TLittleEndian_32bits_in4bytes
{
    INT8U	lo_w_lo_b;	// Addr 00 Low word Lo byte
    INT8U	lo_w_hi_b;	// Addr 01 Low word Hi byte
    INT8U	hi_w_lo_b;	// Addr 10 Hi word Lo byte
    INT8U	hi_w_hi_b;	// Addr 11 Hi word Hi byte
};

struct TBigEndian_32bits_in4bytes
{
    INT8U	hi_w_hi_b;	// Addr 00 Hi word Hi byte
    INT8U	hi_w_lo_b;	// Addr 01 Hi word Lo byte
    INT8U	lo_w_hi_b;	// Addr 10 Low word Hi byte
    INT8U	lo_w_lo_b;	// Addr 11 Low word Lo byte
};


union TUnion32Bits
{
    INT32U					int32u;		// natural, big endian value
    INT32S					int32s;
    INT8U					byte[4];
    INT16U					word[2];
    struct TBigEndian_32Bits			bit;		// natural
    struct TLittleEndian_32Bits			bit_le;		// little endian
    struct TBigEndian_32Bits			bit_be;         // big endian, natural
    struct TBigEndian_32bits_lo_hi		part;           // natural
    struct TBigEndian_32bits_in4bytes		in4Bytes;	// big endian, natural
    struct TLittleEndian_32bits_in4bytes	le;		// little endian
    struct TBigEndian_32bits_in4bytes		be;             // big endian, natural
    FP32					fp32;		// natural
};

//-----------------------------------------------------------------------------------------------------------

struct TLittleEndian_64Bits
{
    BIT_FIELD b7  : 1;
    BIT_FIELD b6  : 1;
    BIT_FIELD b5  : 1;
    BIT_FIELD b4  : 1;
    BIT_FIELD b3  : 1;
    BIT_FIELD b2  : 1;
    BIT_FIELD b1  : 1;
    BIT_FIELD b0  : 1;

    BIT_FIELD b15 : 1;
    BIT_FIELD b14 : 1;
    BIT_FIELD b13 : 1;
    BIT_FIELD b12 : 1;
    BIT_FIELD b11 : 1;
    BIT_FIELD b10 : 1;
    BIT_FIELD b9  : 1;
    BIT_FIELD b8  : 1;

    BIT_FIELD b23 : 1;
    BIT_FIELD b22 : 1;
    BIT_FIELD b21 : 1;
    BIT_FIELD b20 : 1;
    BIT_FIELD b19 : 1;
    BIT_FIELD b18 : 1;
    BIT_FIELD b17 : 1;
    BIT_FIELD b16 : 1;

    BIT_FIELD b31 : 1;
    BIT_FIELD b30 : 1;
    BIT_FIELD b29 : 1;
    BIT_FIELD b28 : 1;
    BIT_FIELD b27 : 1;
    BIT_FIELD b26 : 1;
    BIT_FIELD b25 : 1;
    BIT_FIELD b24 : 1;


    BIT_FIELD b39 : 1;
    BIT_FIELD b38 : 1;
    BIT_FIELD b37 : 1;
    BIT_FIELD b36 : 1;
    BIT_FIELD b35 : 1;
    BIT_FIELD b34 : 1;
    BIT_FIELD b33 : 1;
    BIT_FIELD b32 : 1;

    BIT_FIELD b47 : 1;
    BIT_FIELD b46 : 1;
    BIT_FIELD b45 : 1;
    BIT_FIELD b44 : 1;
    BIT_FIELD b43 : 1;
    BIT_FIELD b42 : 1;
    BIT_FIELD b41 : 1;
    BIT_FIELD b40 : 1;

    BIT_FIELD b55 : 1;
    BIT_FIELD b54 : 1;
    BIT_FIELD b53 : 1;
    BIT_FIELD b52 : 1;
    BIT_FIELD b51 : 1;
    BIT_FIELD b50 : 1;
    BIT_FIELD b49 : 1;
    BIT_FIELD b48 : 1;

    BIT_FIELD b63 : 1;
    BIT_FIELD b62 : 1;
    BIT_FIELD b61 : 1;
    BIT_FIELD b60 : 1;
    BIT_FIELD b59 : 1;
    BIT_FIELD b58 : 1;
    BIT_FIELD b57 : 1;
    BIT_FIELD b56 : 1;
};

struct TBigEndian_64Bits
{
    BIT_FIELD b63 : 1; // b63       Addr 0(HiHi) b7
    BIT_FIELD b62 : 1; // b62       Addr 0(HiHi) b6
    BIT_FIELD b61 : 1; // b61       Addr 0(HiHi) b5
    BIT_FIELD b60 : 1; // b60       Addr 0(HiHi) b4
    BIT_FIELD b59 : 1; // b59       Addr 0(HiHi) b3
    BIT_FIELD b58 : 1; // b58       Addr 0(HiHi) b2
    BIT_FIELD b57 : 1; // b57       Addr 0(HiHi) b1
    BIT_FIELD b56 : 1; // b56       Addr 0(HiHi) b0 --- dwhi.HiHi
    BIT_FIELD b55 : 1; // b55       Addr 1(HiLo) b7
    BIT_FIELD b54 : 1; // b54       Addr 1(HiLo) b6
    BIT_FIELD b53 : 1; // b53       Addr 1(HiLo) b5
    BIT_FIELD b52 : 1; // b52       Addr 1(HiLo) b4
    BIT_FIELD b51 : 1; // b51       Addr 1(HiLo) b3
    BIT_FIELD b50 : 1; // b50       Addr 1(HiLo) b2
    BIT_FIELD b49 : 1; // b49       Addr 1(HiLo) b1
    BIT_FIELD b48 : 1; // b48       Addr 1(HiLo) b0 --- dwhi.HiLo
    BIT_FIELD b47 : 1; // b47       Addr 2(LoHi) b7
    BIT_FIELD b46 : 1; // b46       Addr 2(LoHi) b6
    BIT_FIELD b45 : 1; // b45       Addr 2(LoHi) b5
    BIT_FIELD b44 : 1; // b44       Addr 2(LoHi) b4
    BIT_FIELD b43 : 1; // b43       Addr 2(LoHi) b3
    BIT_FIELD b42 : 1; // b42       Addr 2(LoHi) b2
    BIT_FIELD b41 : 1; // b41       Addr 2(LoHi) b1
    BIT_FIELD b40 : 1; // b40       Addr 2(LoHi) b0 --- dwhi.LoHi
    BIT_FIELD b39 : 1; // b39       Addr 3(LoLo) b7
    BIT_FIELD b38 : 1; // b38       Addr 3(LoLo) b6
    BIT_FIELD b37 : 1; // b37       Addr 3(LoLo) b5
    BIT_FIELD b36 : 1; // b36       Addr 3(LoLo) b4
    BIT_FIELD b35 : 1; // b35       Addr 3(LoLo) b3
    BIT_FIELD b34 : 1; // b34       Addr 3(LoLo) b2
    BIT_FIELD b33 : 1; // b33       Addr 3(LoLo) b1
    BIT_FIELD b32 : 1; // b32	Addr 3(LoLo) b0 --- dwhi.LoLo
    BIT_FIELD b31 : 1; // b31       Addr 4(HiHi) b7
    BIT_FIELD b30 : 1; // b30       Addr 4(HiHi) b6
    BIT_FIELD b29 : 1; // b29       Addr 4(HiHi) b5
    BIT_FIELD b28 : 1; // b28       Addr 4(HiHi) b4
    BIT_FIELD b27 : 1; // b27       Addr 4(HiHi) b3
    BIT_FIELD b26 : 1; // b26       Addr 4(HiHi) b2
    BIT_FIELD b25 : 1; // b25       Addr 4(HiHi) b1
    BIT_FIELD b24 : 1; // b24       Addr 4(HiHi) b0 --- dwlo.HiHi
    BIT_FIELD b23 : 1; // b23       Addr 5(HiLo) b7
    BIT_FIELD b22 : 1; // b22       Addr 5(HiLo) b6
    BIT_FIELD b21 : 1; // b21       Addr 5(HiLo) b5
    BIT_FIELD b20 : 1; // b20       Addr 5(HiLo) b4
    BIT_FIELD b19 : 1; // b19       Addr 5(HiLo) b3
    BIT_FIELD b18 : 1; // b18       Addr 5(HiLo) b2
    BIT_FIELD b17 : 1; // b17       Addr 5(HiLo) b1
    BIT_FIELD b16 : 1; // b16       Addr 5(HiLo) b0 --- dwlo.HiLo
    BIT_FIELD b15 : 1; // b15       Addr 6(LoHi) b7
    BIT_FIELD b14 : 1; // b14       Addr 6(LoHi) b6
    BIT_FIELD b13 : 1; // b13       Addr 6(LoHi) b5
    BIT_FIELD b12 : 1; // b12       Addr 6(LoHi) b4
    BIT_FIELD b11 : 1; // b11       Addr 6(LoHi) b3
    BIT_FIELD b10 : 1; // b10       Addr 6(LoHi) b2
    BIT_FIELD b9  : 1; // b9        Addr 6(LoHi) b1
    BIT_FIELD b8  : 1; // b8        Addr 6(LoHi) b0 --- dwlo.LoHi
    BIT_FIELD b7  : 1; // b7        Addr 7(LoLo) b7
    BIT_FIELD b6  : 1; // b6        Addr 7(LoLo) b6
    BIT_FIELD b5  : 1; // b5        Addr 7(LoLo) b5
    BIT_FIELD b4  : 1; // b4        Addr 7(LoLo) b4
    BIT_FIELD b3  : 1; // b3        Addr 7(LoLo) b3
    BIT_FIELD b2  : 1; // b2        Addr 7(LoLo) b2
    BIT_FIELD b1  : 1; // b1        Addr 7(LoLo) b1
    BIT_FIELD b0  : 1; // b0	Addr 7(LoLo) b0 --- dwlo.LoLo
};

// this is the natural order for this processor
struct TLittleEndian_64bits_lo_hi
{
    INT32U	lo;	// Addr 0, 1, 2, 3 Lo double word
    INT32U	hi;	// Addr 4, 5, 6, 7 Hi double word
};

// when this processor acces a memory written by a BIG ENDIAN processor
// the bits are like this
struct TBigEndian_64bits_lo_hi
{
    INT32U	hi; 	// Addr 0, 1, 2, 3 Hi double word
    INT32U	lo;     // Addr 4, 5, 6, 7 Lo double word
};


struct TLittleEndian_64bits_in8bytes
{
    INT8U	lo_dw_lo_w_lo_b;	// Addr 000 Low double word Low word Lo byte
    INT8U	lo_dw_lo_w_hi_b;	// Addr 001 Low double word Low word Hi byte
    INT8U	lo_dw_hi_w_lo_b;	// Addr 010 Low double word Hi word Lo byte
    INT8U	lo_dw_hi_w_hi_b;	// Addr 011 Low double word Hi word Hi byte
    INT8U	hi_dw_lo_w_lo_b;	// Addr 100 Hi double word Low word Lo byte
    INT8U	hi_dw_lo_w_hi_b;	// Addr 101 Hi double word Low word Hi byte
    INT8U	hi_dw_hi_w_lo_b;	// Addr 110 Hi double word Hi word Lo byte
    INT8U	hi_dw_hi_w_hi_b;	// Addr 111 Hi double word Hi word Hi byte
};

struct TBigEndian_64bits_in8bytes
{
    INT8U	hi_dw_hi_w_hi_b;	// Addr 111 Hi double word Hi word Hi byte
    INT8U	hi_dw_hi_w_lo_b;	// Addr 110 Hi double word Hi word Lo byte
    INT8U	hi_dw_lo_w_hi_b;	// Addr 101 Hi double word Low word Hi byte
    INT8U	hi_dw_lo_w_lo_b;	// Addr 100 Hi double word Low word Lo byte
    INT8U	lo_dw_hi_w_hi_b;	// Addr 011 Low double word Hi word Hi byte
    INT8U	lo_dw_hi_w_lo_b;	// Addr 010 Low double word Hi word Lo byte
    INT8U	lo_dw_lo_w_hi_b;	// Addr 001 Low double word Low word Hi byte
    INT8U	lo_dw_lo_w_lo_b;	// Addr 000 Low double word Low word Lo byte
};

union TUnion64Bits
{
    INT64U					int64u;		// natural, big endian value
    INT64S					int64s;
    INT32U					int32u[2];
    INT8U					byte[8];
    INT16U					word[4];
    struct TBigEndian_64Bits			bit;		// natural
    struct TLittleEndian_64Bits			bit_le;		// little endian
    struct TBigEndian_64Bits			bit_be;		// big endian, natural
    struct TBigEndian_64bits_lo_hi		part;           // natural
    struct TBigEndian_64bits_in8bytes		in8Bytes;	// big endian, natural
    struct TLittleEndian_64bits_in8bytes	le;		// little endian
    struct TBigEndian_64bits_in8bytes		be;		// big endian, natural
    FP64					fp64;		// natural
};

#endif	// _STRUCTS_BITS_BIG_H_ end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: structs_bits_big.h
\*---------------------------------------------------------------------------------------------------------*/
