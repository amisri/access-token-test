/*---------------------------------------------------------------------------------------------------------*\
  File:         cc_types.h

  Purpose:      Typedef for the Hiware compiler for m68hc16 MCU

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CC_TYPES_H
#define CC_TYPES_H

// ----- Typedefs -----

#ifndef NULL
#define NULL            ((void*)0)              // Same definition as in stdio.h and stddef.h
#endif

// char is char, nor signed nor unsigned

typedef unsigned char		INT8U;		// Unsigned  8-bit quantity [0:255]
typedef signed   char		INT8S;		// Signed    8-bit quantity [-128:127]

// int = signed short int = INT16S  for this compiler

typedef unsigned short		INT16U;		// Unsigned 16-bit quantity [0:65535]           64Kb
typedef signed short		INT16S;		// Signed   16-bit quantity [-32768:32767]      32Kb

typedef unsigned long		INT32U;		// Unsigned 32-bit quantity [0:4294967295]              4Gb
typedef signed   long		INT32S;		// Signed   32-bit quantity [-2147483648:2147483647]    2Gb

typedef unsigned long		INT64U;		// Not available
typedef signed   long		INT64S;		// Not available

// only used to communicate IEEE FP32 to the TMS320C32 via the Dual Port RAM
typedef float                   DP_IEEE_FP32;

typedef float			FP32;		// Single precision floating point

//    float is 4 bytes (32 bits) [1.17549435e-38:3.40282347e+38]

typedef double			FP64;		// Double precision floating point

//    double is 8 bytes (64 bits) [2.2250738585072014e-308:1.7976931348623157e+308]


typedef volatile unsigned char	REG8U;		// Unsigned  8-bit register
typedef volatile signed   short	REG16S;		// Signed   16-bit register
typedef volatile unsigned short	REG16U;		// Unsigned 16-bit register
typedef volatile unsigned long	REG32U;		// Unsigned 32-bit register

typedef INT16U                  uintptr_t;	// used to cast a pointer in an integer container

#ifdef __CDT_PARSER__   // fake definitions to avoid eclipse syntax errors
    #define FAR
    #define NEAR
#else
    // to share code while compiling for other processors that can have different address pointers sizes
    // for example for GNU GCC C compiler for M32C target the far is not defined
    #define FAR  far		// pointer is 32bits [0:0xFFFFFFFF]
    #define NEAR 
#endif

//typedef char far * 		PCHARFAR;	// Far pointer to character (string)

#define BIT_FIELD       int     // The HiWare compiler doesn't support unsigned char as a bit field

#ifndef __BYTE_ORDER__
#define __ORDER_LITTLE_ENDIAN__ 1111
#define __ORDER_BIG_ENDIAN__ 2222
#define __ORDER_PDP_ENDIAN__ 3333

//this line will be arch dependent
#define __BYTE_ORDER__ __ORDER_BIG_ENDIAN__

#endif //__BYTE_ORDER__

#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cc_types.h
\*---------------------------------------------------------------------------------------------------------*/
