/*---------------------------------------------------------------------------------------------------------*\
  File:         cc_types.h

  Purpose:      Typedef for the GNU GCC compiler for RX6xx target


 Note:
	Significant initial characters in an identifier or macro name.
	    The preprocessor treats all characters as significant.
	    The C standard requires only that the first 63 be significant

	For external names, the number of significant characters are defined by the linker;
	for almost all targets, all characters are significant.

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CC_TYPES_H	// header encapsulation
#define CC_TYPES_H

// ----- Typedefs -----
/*
	             Size
    char		1  	0				[2^8]-1 (255)
    signed char		1  	-[2^7] (-128)			[2^7]-1 (127)
    unsigned char	1  	0				[2^8]-1 (255)
    short		2  	-[2^15] (-32768)		[2^15]-1 (32767)
    signed short	2  	-[2^15] (-32768) 		[2^15]-1 (32767)
    unsigned short	2  	0				[2^16]-1 (65535)
    int			4  	-[2^31] (-2147483648) 		[2^31]-1 (2147483647)
    signed int		4  	-[2^31] (-2147483648) 		[2^31]-1 (2147483647)
    unsigned int	4  	0				[2^32]-1 (4294967295)
    long		4  	-[2^31] (-2147483648) 		[2^31]-1 (2147483647)
    signed long		4  	-[2^31] (-2147483648) 		[2^31]-1 (2147483647)
    unsigned long	4  	0				[2^32]-1 (4294967295)
    long long		8  	-[2^63] (-9223372036854775808)	[2^63]-1 (9223372036854775807)
    signed long long	8  	-[2^63] (-9223372036854775808) 	[2^63]-1 (9223372036854775807)
    unsigned long long	8  	0				[2^64]-1 (18446744073709551615)
*/

// char is char, nor signed nor unsigned

typedef unsigned char		INT8U;	// Unsigned  8-bit quantity [0:255]
typedef signed   char		INT8S;	// Signed    8-bit quantity [-128:127]

typedef unsigned short		INT16U;	// Unsigned 16-bit quantity [0:65535]		64Kb
typedef signed   short		INT16S;	// Signed   16-bit quantity [-32768:32767]	32Kb

// int = signed long int = INT32S  for this compiler

typedef unsigned long		INT32U;	// Unsigned 32-bit quantity [0:4294967295] UL           4Gb
typedef signed   long		INT32S;	// Signed   32-bit quantity [-2147483648:2147483647] L	2Gb

typedef unsigned long long	INT64U;	// Unsigned 64-bit quantity [0:18446744073709551615] ULL
typedef signed   long long	INT64S;	// Signed   64-bit quantity [-9223372036854775808:9223372036854775807] LL

typedef INT8U			BOOLEAN;// Flag value (TRUE/FALSE)
// Note: Q.K. uses in the __HC16__ code BOOLEAN as INT16U
typedef INT16U                  BOOLEAN16; // Flag value (TRUE/FALSE)

// only used to communicate IEEE FP32 to the TMS320C32 via the Dual Port RAM
typedef float                   DP_IEEE_FP32;

/*
    the RX610 FPU is 32 bits (float) so by default the compiler has -m32bit_doubles
    making double=float=32 bits so all the floating point arithmetics work with the FPU

    when changing to -m64bit_doubles so float:32bits and double:64 bits the FPU is not used
*/
typedef float			FP32;		// Single precision floating point
//   float is 4 bytes (32 bits)

// this is only valid when -m64bit_doubles
typedef double			FP64;		// Double precision floating point
//    double can be 8 bytes (64 bits)

// this is not valid!!!, long double = double, can be FP64 or FP32
typedef long double		FP96;	// 12 bytes floating point


typedef volatile unsigned char      REG8U;		// Unsigned  8-bit register
typedef volatile signed   short     REG16S;		// Signed   16-bit register
typedef volatile unsigned short     REG16U;		// Unsigned 16-bit register
typedef volatile unsigned long      REG32U;     // Unsigned 32-bit register
typedef volatile signed long        REG32S;     // Unsigned 32-bit register
typedef volatile unsigned long long	REG64U;
typedef volatile signed long long	REG64S;

typedef INT32U                          uintptr_t;		// used to cast a pointer in an integer container

// Stephen Page and Quenting King are compiling for other processors, they have different address
// pointers sizes
// FAR and NEAR are not defined for this compiler
#define FAR
#define NEAR

typedef char * 			PCHARFAR;	// Far pointer to character (string)

#define  FALSE			0
#define  TRUE			1

#define BIT_FIELD       INT16U

#define INLINE inline

#endif	// CC_TYPES_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cc_types.h
\*---------------------------------------------------------------------------------------------------------*/
