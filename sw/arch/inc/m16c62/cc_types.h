/*---------------------------------------------------------------------------------------------------------*\
  File:         cc_types.h

  Purpose:      Typedef for the Renesas NC30 compiler (M16C target)

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CC_TYPES_H	// header encapsulation
#define CC_TYPES_H

// ----- Typedefs -----

// char is char, nor signed nor unsigned

typedef unsigned char		INT8U;		// Unsigned  8-bit quantity [0:255]
typedef signed   char		INT8S;		// Signed    8-bit quantity [-128:127]

// int = signed short int = INT16S  for this compiler

typedef unsigned short		INT16U;		// Unsigned 16-bit quantity [0:65535]           64Kb
//typedef signed short		INT16S;		// Signed   16-bit quantity [-32768:32767]      32Kb
typedef signed   int	    	INT16S;		// why I can't use previous???
// problem of imcompatible assignment in the renesas library ??
// when compiling the M16C62 IdProg

typedef unsigned long		INT32U;		// Unsigned 32-bit quantity [0:4294967295]              4Gb
typedef signed   long		INT32S;		// Signed   32-bit quantity [-2147483648:2147483647]    2Gb

typedef unsigned long long	INT64U;		// Unsigned 64-bit quantity [0:18446744073709551615]
typedef signed   long long	INT64S;		// Signed   64-bit quantity [-9223372036854775808:9223372036854775807]

// typedef INT8U		_Bool;	// NC30 _Bool is 8bits unsigned [0,1]

typedef INT8U			BOOLEAN;	// Flag value (TRUE/FALSE)
// Note: Q.K. uses in the __HC16__ code BOOLEAN as INT16U

typedef float			FP32;		// Single precision floating point
//    float is 4 bytes (32 bits) [1.17549435e-38:3.40282347e+38]

typedef double			FP64;		// Double precision floating point
//    double is 8 bytes (64 bits) [2.2250738585072014e-308:1.7976931348623157e+308]

// this is not valid!!!, long double = double, it is FP64
typedef long double		FP96;	// 12 bytes floating point


typedef volatile unsigned char	REG8U;		// Unsigned  8-bit register
typedef volatile signed   short	REG16S;		// Signed   16-bit register
typedef volatile unsigned short	REG16U;		// Unsigned 16-bit register
typedef volatile unsigned long	REG32U;		// Unsigned 32-bit register

typedef INT16U                  uintptr_t;	// used to cast a pointer in an integer container
//-----------------------------------------------------------------------------------------------------------

// to share code while compiling for other processors that can have different address pointers sizes
// for example for GNU GCC C compiler for M32C target the far is not defined

//-----------------------------------------------------------------------------------------------------------

// defaults for Renesas NC38 compiler
// Variables                  near attribute, resides in the first 64Kb of memory, our RAM
// const-qualified constants  far attribute
// Functions                  far attribute

#ifdef __CDT_PARSER__   // fake definitions to avoid eclipse syntax errors
    #define FAR
    #define NEAR
#else
    #define FAR	    _far		// or just 'far'  pointer is 32bits [0:0xFFFFFFFF]
    #define NEAR    _near       // or just 'near' pointer is 16bits [0:0xFFFF]
#endif

/*
#define POINTER_ANY_MEM far *   // this pointer can access all the memory, is the default
#define POINTER_PAGE0   near *  // this pointer can access only the first 64Kb of memory

#define VAR_ANY_MEM     far     // this variable goes in any place of the memory, not restricted to the first 64Kb
#define VAR_PAGE0       near    // this variable goes only in the first 64Kb of memory, is the default
*/

//-----------------------------------------------------------------------------------------------------------

typedef char far * 		PCHARFAR;	// Far pointer to character (string)

#define  FALSE			0
#define  TRUE			1

#define BIT_FIELD       INT8U

//GCC endianess macros
#ifndef __BYTE_ORDER__
#define __ORDER_LITTLE_ENDIAN__ 1111
#define __ORDER_BIG_ENDIAN__ 2222
#define __ORDER_PDP_ENDIAN__ 3333

//this line will be arch dependent
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__

#endif  //__BYTE_ORDER__

#define INLINE inline

#endif	// CC_TYPES_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cc_types.h
\*---------------------------------------------------------------------------------------------------------*/
