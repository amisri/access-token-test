"""
setup.py for fgc_logger.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages

########################
# Change version here! #
########################
VERSION = "1.3.0"

HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()

GEN_EXAMPLES_SCRIPT = "generate_examples.py"
GEN_GW_LIST_SCRIPT = "generate_gw_list.py"

CONFIG_TEMPLATE = "config_example.cfg.template"
LAUNCHER_TEMPLATE = "launcher_example.sh.template"
SYSTEMD_FILE_TEMPLATE = "fgc_logger.service.template"
SECRET = "secret_example.json"
LIST_GROUPS = "list_groups_example.txt"
LIST_GATEWAYS = "list_gateways_example.txt"
LIST_DEVICES = "list_devices_example.txt"
DIR_DESC = "example_directory.txt"

REQUIREMENTS: dict = {
    'core': [
        'pyfgc>=1.3.2',
        'pyfgc_statussrv>=1.0',
        'pyfgc_log>=1.4.0',
        'docopt>=0.6',
        'colorlog>=4.1',
        'aiohttp>=3.6.2',
        'yappi>=1.2.5',
        'sentry_sdk>=1.7.0',
    ],
    'test': [
        'pytest',
        'pytest-asyncio'
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
    ],
}


setup(
    name='fgc-logger',
    version=VERSION,

    author='Joao Afonso',
    author_email='joao.afonso@cern.ch',
    description='FGC Logger - logging server',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',

    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/fgc_logger/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/fgc_logger',
    },

    packages=find_packages(),
    python_requires='>=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    # Files to copy to specific directories
    data_files=[
        ("fgc_logger/templates", [
            "fgc_logger/templates/"+CONFIG_TEMPLATE,
            "fgc_logger/templates/"+LAUNCHER_TEMPLATE,
            "fgc_logger/templates/"+SYSTEMD_FILE_TEMPLATE,
            "fgc_logger/templates/"+SECRET,
            "fgc_logger/templates/"+LIST_GROUPS,
            "fgc_logger/templates/"+LIST_GATEWAYS,
            "fgc_logger/templates/"+LIST_DEVICES,
        ]),
        ("fgc_logger/keys", [
             "fgc_logger/templates/"+SECRET,
             "fgc_logger/templates/"+DIR_DESC,
        ]),
        ("fgc_logger/log", [
            "fgc_logger/templates/"+DIR_DESC,
        ]),
        ("fgc_logger", [
            "fgc_logger/"+GEN_EXAMPLES_SCRIPT,
            "fgc_logger/"+GEN_GW_LIST_SCRIPT,
        ])
    ]
)



# EOF
