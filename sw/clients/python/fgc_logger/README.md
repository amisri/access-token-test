# FGC Logger

FGC Logger is a python package responsible for monitoring the FGCs,
detecting fault events, reading the log buffers and storing them into Fortlogs.

This package should be executed as a daemon, monitoring the FGCs in the background.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the FGC Logger.
It is recomended the usage of an virtual environment.

To create and source the virtual environment:
```bash
pip -m virtualenv new_venv
source new_venv/bin/activate
```

To install the FGC Logger in the virtual environment, from CERN (repo acc-py):
```bash
pip install fgc_logger
```

To install outside of CERN, one must have access to a wheel or tar distribution.
If FGC Logger and all dependencies are available in the directory *offline_packages_dir*:
```bash
pip install fgc_logger --no-index -f ./offline_packages_dir
```

## Usage

To launch the FGC Logger, source the virtual environment, and prepare a configuration
file *config.cfg*.

Then, you can execute the FGC Logger with:

```bash
python3 -m fgc_logger config.cfg
```

For help run:

```bash
python3 -m fgc_logger -h
```

The configuration file is required to setup various parameters. Check the example file
for more information.

Other example files are also provided, for example to setup the FGC Logger with
[systemd](https://www.freedesktop.org/wiki/Software/systemd/).

### Generate example files

After installing the FGC Logger it is possible to generate a set of example files,
to show how to integrate it with various systems.

The following files are generated:

* **config_example.cfg** - Example configuration file.
* **launcher_example.sh** - Script for launching the FGC Logger. Can be invoqued from systemd.
* **fgc_logger.service** - Systemd service file. Must be moved into systemd directory.
* **secret_example.json** - Example of the secret file, with the fields required to access Fortlogs.
* **list_gateways_example.txt** - Can be used to filter which gateways to monitor.
* **list_devices_example.txt** - Can be used to filter which devices to monitor.

*All of these files are just examples, meaning they must be edited during the setup.*

To generate the example files, run:
```bash
cd new_venv/fgc_logger
python ./generate_examples.py
```

### Generate list of gateways from group file

If a group file exists, it is possible to generate the list of gateways from this file.
A script is provided to auxiliate this process:

```bash
cd new_venv/fgc_logger
python ./generate_gw_list.py
```

This tool provides a menu displaying all groups. The user can easily select the important
ones and export the result as a file that can be imported into *config.cfg*.

## Licence

Check LICENCE file.