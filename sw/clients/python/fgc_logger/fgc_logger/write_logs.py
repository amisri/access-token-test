"""Log write module

This module is used to store the logs in fortlogs.

"""
import logging
import asyncio
import aiohttp
from typing import Dict, Tuple

import pyfgc_const

from fgc_logger.log_read_request import EventType
from fgc_logger.fortlogs_handler import FortLogsSessionContext, FortLogsSession

import fgc_logger.utils as utils

module_logger = logging.getLogger(__name__)

# Event values to be sent to fortlogs

EVENT_TRANSLATE = {
    EventType.POST_MORTEM: pyfgc_const.Cause.PostMortem.value,
    EventType.SLOW_ABORT: pyfgc_const.Cause.SlowAbort.value,
    EventType.DIM: pyfgc_const.Cause.DIM.value,
}

RETRY_FORTLOGS_PERIOD_S = 15.0


class FortlogsError(Exception):
    """When there is a connection problem with fortlogs.
    """

class FgcLogWriter:
    """Log writer class.

    This object can be used to store the logs of a device into FortLogs
    """

    def __init__(self, request):
        self.logger = utils.DeviceLogAdapter(module_logger, {"device": request.device_name})
        self.request = request
        self.acq_id = None

        self.log_queue = asyncio.Queue()

        self.running = True

        asyncio.ensure_future(self._run())


    async def save_logs(self, decoded_logs: Dict[str, tuple]):
        await self.log_queue.put(decoded_logs)

    async def flush(self):
        self.running = False
        await self.log_queue.put(None)
        self.logger.debug("Flushing log writer")

    async def _run(self):
        while self.running or not self.log_queue.empty():

            logs = await self.log_queue.get()
            if logs is None:
                continue

            save_logs_later = await self._save_logs(logs)
            if save_logs_later:
                await self.log_queue.put(save_logs_later)
                # Wait for some seconds
                self.logger.warning("Retry to write %s in %s s.",
                                    ', '.join(save_logs_later.keys()), RETRY_FORTLOGS_PERIOD_S)
                await asyncio.sleep(RETRY_FORTLOGS_PERIOD_S)
        self.logger.info("All logs have been stored into FortLogs.")

    async def _save_logs(self, decoded_logs: Dict[str, tuple]) -> dict:
        """Try to save logs in fortlogs.

        If argument acq_id is not given (or None), a new acquisition will be created and the
        ID returned.

        Args:
            decoded_logs (Dict[str, tuple]): Decoded log tuple, as returned from '_get_decoded_log'.

        Returns:
            Tuple[int, dict]: (1) Acquisition ID (equal to argument 'acq_id',
                or a new one if it is None).
                (2) Dictionary containing 'decoded_logs' that were not stored
                successfully. These will need to be stored in the future again.
        """

        save_log_later = dict()

        async with FortLogsSessionContext(self.request.device_name) as ft_session:

            # Save in fortlogs - Add buffers to acquisition
            if self.acq_id is None:
                try:
                    self.acq_id = await self._create_acquisition(ft_session)
                except FortlogsError as err:
                    self.logger.error(
                        str(err),
                        exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))

            # Save in fortlogs - Add buffers to acquisition
            if self.acq_id is None:
                for log_name, (decoded_data, log_type) in decoded_logs.items():
                    save_log_later[log_name] = decoded_logs[log_name]
            else:
                for log_name, (decoded_data, log_type) in decoded_logs.items():
                    try:
                        await self._save_buffer(ft_session, log_name, log_type, decoded_data)
                    except FortlogsError as err:
                        # self.logger.exception(str(err))
                        self.logger.error(
                            str(err),
                            exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))
                        save_log_later[log_name] = decoded_logs[log_name]
                    else:
                        self.logger.debug("%s saved successfully.", log_name)

            if save_log_later:
                self.logger.warning(
                    "Failed to write %s to fortlogs.",
                    ", ".join(save_log_later.keys())
                )

        return save_log_later

    async def _create_acquisition(self, ft_session: FortLogsSession) -> int:
        """Create new fortlogs acquisition.

        Args:
            ft_session (FortLogsSession): Fortlogs session object.

        Raises:
            FortlogsError: When there was a communication problem with fortlogs.

        Returns:
            int: New acquisition ID.
        """
        event = EVENT_TRANSLATE[self.request.event_type]
        timestamp = self.request.event_timestamp
        self.logger.info("Creating fortlogs acquisition for event %s at %f s.",
                         event, timestamp)
        try:
            self.acq_id = await ft_session.create_acquisition(event, timestamp)
        except aiohttp.ClientResponseError as err:
            msg = "Failed to create fortlogs acquisition."
            raise FortlogsError(msg) from err
        except aiohttp.ClientConnectionError as err:
            msg = "Failed to connect to fortlogs server. Acquisition not created."
            raise FortlogsError(msg) from err
        else:
            self.logger.debug("Fortlogs acquisition created.")
            return self.acq_id

    async def _save_buffer(self, ft_session: FortLogsSession, log_name: str, log_type: str, decoded_data: str):
        """Write a log buffer to fortlogs.

        Args:
            ft_session (FortLogsSession): Fortlogs session object.
            log_name (str): Log name.
            log_type (str): Log type.
            decoded_data (str): Decoded log data, as json string.

        Raises:
            FortlogsError: When there was a communication problem with fortlogs.
        """
        self.logger.info("Saving %s.", log_name)
        try:
            await ft_session.add_buffer(self.acq_id, log_type, decoded_data)
        except aiohttp.ClientResponseError as err:
            msg = f"Failed to save {log_name} in fortlogs."
            raise FortlogsError(msg) from err
        except aiohttp.ClientConnectionError as err:
            msg = f"Failed to connect to fortlogs server. {log_name} not saved."
            raise FortlogsError(msg) from err
        except Exception as err:
            msg = f"Failed to save {log_name} in fortlogs."
            raise FortlogsError(msg) from err
        else:
            self.logger.debug("%s saved.", log_name)
