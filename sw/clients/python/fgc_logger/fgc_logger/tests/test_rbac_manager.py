
import pytest
import asyncio
import logging

import fgc_logger
from fgc_logger.rbac_manager import RbacManager

logging.getLogger("fgc_logger").setLevel(logging.DEBUG)

@pytest.fixture(autouse=True)
def clean_singleton():
    fgc_logger.rbac_manager.Singleton.instances.clear()

@pytest.mark.asyncio
async def test_create():
    obj = RbacManager()
    assert obj

@pytest.mark.asyncio
async def test_singleton_behaviour():
    obj_1 = RbacManager()
    obj_2 = RbacManager()
    assert obj_1 and obj_2
    assert obj_1 is obj_2

@pytest.mark.asyncio
async def test_start_stop():
    obj = RbacManager()
    await obj.start()
    auto_task = obj.auto_task
    assert not auto_task.done()
    await obj.stop()
    assert obj.auto_task is None
    assert auto_task.done()
    assert auto_task.cancelled()

@pytest.mark.asyncio
async def test_get_token():
    rbac_man = RbacManager()
    token = await rbac_man.get_token()
    assert isinstance(token, bytes)

@pytest.mark.asyncio
async def test_get_token_auto():
    rbac_man = RbacManager()
    await rbac_man.start()

    await asyncio.sleep(0.1)
    token = await rbac_man.get_token()
    assert token is rbac_man.token

    assert isinstance(token, bytes)
    await rbac_man.stop()

@pytest.mark.asyncio
async def test_token_caching():
    rbac_man = RbacManager()
    token_1 = await rbac_man.get_token()
    token_2 = await rbac_man.get_token()
    assert token_1 and token_2
    assert token_1 is token_2

@pytest.mark.asyncio
async def test_token_caching_auto():
    rbac_man = RbacManager()

    await rbac_man.start()
    await asyncio.sleep(0.1)
    token_0 = rbac_man.token

    token_1 = await rbac_man.get_token()
    await asyncio.sleep(0.1)
    token_2 = await rbac_man.get_token()

    assert token_0 and token_1 and token_2
    assert token_0 is token_1 and token_0 is token_2

    await rbac_man.stop()

@pytest.mark.asyncio
async def test_token_refreshing(monkeypatch):

    # Refresh after 0.1 seconds (token last usually 8 hours)
    monkeypatch.setattr(fgc_logger.rbac_manager, "REFRESH_TRIGGER_MARGIN", 1 / (3600 * 8 * 10)) 

    rbac_man = RbacManager()
    token_1 = await rbac_man.get_token()
    await asyncio.sleep(0.2)
    token_2 = await rbac_man.get_token()
    assert token_1 and token_2
    assert token_1 is not token_2

@pytest.mark.asyncio
async def test_auto_token_refreshing(monkeypatch):

    # Refresh after 0.1 seconds (token last usually 8 hours)
    monkeypatch.setattr(fgc_logger.rbac_manager, "REFRESH_TRIGGER_MARGIN", 1 / (3600 * 8 * 10)) 

    rbac_man = RbacManager()
    await rbac_man.start()
    await asyncio.sleep(0.2)
    token_1 = rbac_man.token
    await asyncio.sleep(0.4)
    token_2 = rbac_man.token
    assert token_1 and token_2
    assert token_1 is not token_2
    await rbac_man.stop()

# EOF
