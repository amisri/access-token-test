import time
import asyncio
import pytest

import fgc_logger.settings
from fgc_logger.fgc_logger_server import FgcLoggerServer


def test_logger_server_create():
    server = FgcLoggerServer()


@pytest.mark.asyncio
async def test_logger_server_start_stop(monkeypatch):

    monkeypatch.setattr(fgc_logger.settings.basic, "FILTER_GATEWAYS", {"GW_1", "GW_2"})

    server = FgcLoggerServer()
    logger_server_task = asyncio.ensure_future(server.run())
    await asyncio.sleep(0.5)
    assert not logger_server_task.done()

    server.stop()
    await asyncio.wait_for(logger_server_task, 3.0)
    assert logger_server_task.done()



# EOF