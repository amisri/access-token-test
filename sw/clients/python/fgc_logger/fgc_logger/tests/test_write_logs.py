import pytest
import logging
import asyncio
import contextlib
import importlib
import time
import aiohttp
import unittest
import unittest.mock
import json

import concurrent.futures

import pyfgc
import pyfgc_rbac

import fgc_logger
import fgc_logger.fetch_logs as fetch
import fgc_logger.utils
import fgc_logger.settings as settings

from fgc_logger.write_logs import FgcLogWriter
from fgc_logger.log_read_request import EventType
from fgc_logger.fortlogs_handler import FortLogsSessionContext
from fgc_logger.log_read_request import LogReadRequest
from fgc_logger.event_analyser import LogReadAborted

DEVICE_1 = "RFNA.866.04.ETH1"

@pytest.mark.asyncio
async def test_create_acquisition():

    class MockRequest:
        event_timestamp = time.time()
        event_type = EventType.SLOW_ABORT
        device_name = DEVICE_1

    class MockFtSession:
        called = False

        async def create_acquisition(self, *args):
            self.called = True
            return 123

    writer = FgcLogWriter(MockRequest())
    session = MockFtSession()
    acq_id = await writer._create_acquisition(session)
    assert session.called
    assert acq_id == 123

@pytest.mark.asyncio
async def test_save_buffer():

    class MockRequest:
        event_timestamp = time.time()
        event_type = EventType.SLOW_ABORT
        device_name = DEVICE_1

    class MockFtSession:
        called = False

        async def add_buffer(self, acq_id, log_type, decoded_data):
            self.called = True
            assert acq_id == 123
            assert log_type == "log_type"
            assert decoded_data == "data"

    writer = FgcLogWriter(MockRequest())
    writer.acq_id = 123
    session = MockFtSession()
    await writer._save_buffer(session, "LOG_NAME", "log_type", "data")
    assert session.called


@pytest.mark.asyncio
async def test_create_acquisition_slow_abort():

    class MockRequest:
        event_timestamp = time.time()
        event_type = EventType.SLOW_ABORT
        device_name = DEVICE_1

    class MockFtSession:
        event = None
        async def create_acquisition(self, event, timestamp):
            self.event = event

    writer = FgcLogWriter(MockRequest())
    session = MockFtSession()
    await writer._create_acquisition(session)
    assert session.event == "SlowAbort"


@pytest.mark.asyncio
async def test_create_acquisition_post_mortem():

    class MockRequest:
        event_timestamp = time.time()
        event_type = EventType.POST_MORTEM
        device_name = DEVICE_1

    class MockFtSession:
        event = None
        async def create_acquisition(self, event, timestamp):
            self.event = event

    writer = FgcLogWriter(MockRequest())
    session = MockFtSession()
    await writer._create_acquisition(session)
    assert session.event == "PostMortem"


@pytest.mark.asyncio
async def test_create_acquisition_dim():

    class MockRequest:
        event_timestamp = time.time()
        event_type = EventType.DIM
        device_name = DEVICE_1

    class MockFtSession:
        event = None
        async def create_acquisition(self, event, timestamp):
            self.event = event

    writer = FgcLogWriter(MockRequest())
    session = MockFtSession()
    await writer._create_acquisition(session)
    assert session.event == "DIM"
