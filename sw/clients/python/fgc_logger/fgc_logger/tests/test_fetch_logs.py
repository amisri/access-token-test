import pytest
import logging
import asyncio
import contextlib
import importlib
import time
import aiohttp
import unittest
import unittest.mock
import json

import concurrent.futures

import pyfgc
import pyfgc_rbac

import fgc_logger
import fgc_logger.fetch_logs as fetch
import fgc_logger.utils
import fgc_logger.settings as settings

from fgc_logger.fetch_logs import FgcLogFetcher
from fgc_logger.log_read_request import EventType
from fgc_logger.fortlogs_handler import FortLogsSessionContext
from fgc_logger.log_read_request import LogReadRequest
from fgc_logger.event_analyser import LogReadAborted

DEVICE_1 = "RFNA.866.04.ETH1"


# TODO: Create mock of IO only (aiohttp, check if correct data was sent)


@pytest.fixture
def reload_fgclogger():
    importlib.reload(fgc_logger)
    importlib.reload(fgc_logger.fortlogs_handler)

@pytest.mark.asyncio
async def test_single_log_fetching(reload_fgclogger):

    done = False

    async def err_callback(*args, **kwargs):
        pass

    async def done_callback(*args, **kwargs):
        nonlocal done
        done = True

    timestamp = time.time()

    log = "ACQ"
    prop = "LOG.SPY.DATA[0]"
    get_option = f"DATA|{timestamp:.6f},500,500,0"

    request = LogReadRequest(
        DEVICE_1,
        EventType.SLOW_ABORT,
        timestamp,
        err_callback,
        done_callback,
        pyfgc_rbac.get_token_location()
    )

    request.add_log(log, {"prop": prop, "get_option": get_option})
    request.set_log_ready(log)

    # Fetch log and check if it is stored in fortlogs and acknowledged

    got_token = False
    got_acq_id = False
    stored_buf = False

    async def async_post(self, *args, **kwargs):
        response = None
        nonlocal got_token, got_acq_id, stored_buf

        if settings.secret.AUTH_URL in args[0]:
            response = {"access_token": "token_string", "refresh_token": "resfresh_string"}
            got_token = True
        elif settings.fortlogs.FORTLOGS_URL+"/fgc-logger" in args[0]:
            response = {"acquisition_id": 123}
            got_acq_id = True
        elif settings.fortlogs.FORTLOGS_URL in args[0]:
            stored_buf = True

        class RtObj:
            async def json(self):
                return response

        return RtObj()

    with unittest.mock.patch("aiohttp.ClientSession.post", new=async_post):
        log_fetcher = FgcLogFetcher(request)
        await asyncio.wait_for(log_fetcher.execute(), 5.0)
        assert done
        assert got_token
        assert got_acq_id
        assert stored_buf


@pytest.mark.asyncio
async def test_multiple_log_fetching(reload_fgclogger):

    done = False

    async def err_callback(*args, **kwargs):
        pass

    async def done_callback(*args, **kwargs):
        nonlocal done
        done = True

    timestamp = time.time()

    logs = ["ACQ", "I_MEAS", "TEMP"]
    props = ["LOG.SPY.DATA[0]", "LOG.SPY.DATA[5]", "LOG.SPY.DATA[16]"]
    get_option = f"DATA|{timestamp:.6f},500,500,0"

    request = LogReadRequest(
        DEVICE_1,
        EventType.SLOW_ABORT,
        timestamp,
        err_callback,
        done_callback,
        pyfgc_rbac.get_token_location()
    )

    for idx, log in enumerate(logs):
        request.add_log(log, {"prop": props[idx], "get_option": get_option})

    # Fetch log and check if it is stored in fortlogs and acknowledged

    got_token = False
    got_acq_id = False
    stored_buf = set()

    async def async_post(self, *args, **kwargs):
        response = None
        nonlocal got_token, got_acq_id, stored_buf

        if settings.secret.AUTH_URL in args[0]:
            response = {"access_token": "token_string", "refresh_token": "resfresh_string"}
            got_token = True
        elif settings.fortlogs.FORTLOGS_URL+"/fgc-logger" in args[0]:
            response = {"acquisition_id": 123}
            got_acq_id = True
        elif settings.fortlogs.FORTLOGS_URL in args[0]:
            # Register the name of all logs stored
            stored_buf.add(json.loads(kwargs["data"])["name"])

        class RtObj:
            async def json(self):
                return response

        return RtObj()

    with unittest.mock.patch("aiohttp.ClientSession.post", new=async_post):
        log_fetcher = FgcLogFetcher(request)
        fetch_task = asyncio.ensure_future(log_fetcher.execute())

        for log in logs:
            request.set_log_ready(log)

        await asyncio.wait_for(fetch_task, 5.0)

        assert done
        assert got_token
        assert got_acq_id
        assert len(logs) == len(stored_buf)
        for log in logs:
            assert log in stored_buf
        assert fgc_logger.fetch_logs.ERROR_TABLE_NAME not in stored_buf


@pytest.mark.asyncio
async def test_logging_fgc_error(reload_fgclogger):

    done = False

    async def err_callback(*args, **kwargs):
        raise LogReadAborted("Nope", err_code=123, err_msg="Ups") 

    async def done_callback(*args, **kwargs):
        nonlocal done
        done = True

    timestamp = time.time()

    log = "ACQ"
    prop = "PROP.WILL_FAIL"
    get_option = f"DATA|{timestamp:.6f},500,500,0"

    request = LogReadRequest(
        DEVICE_1,
        EventType.SLOW_ABORT,
        timestamp,
        err_callback,
        done_callback,
        pyfgc_rbac.get_token_location()
    )

    request.add_log(log, {"prop": prop, "get_option": get_option})
    request.set_log_ready(log)

    # Fetch log and check if it is stored in fortlogs and acknowledged

    stored_buf = set()

    async def async_post(self, *args, **kwargs):
        response = None

        if settings.secret.AUTH_URL in args[0]:
            response = {"access_token": "token_string", "refresh_token": "resfresh_string"}
        elif settings.fortlogs.FORTLOGS_URL+"/fgc-logger" in args[0]:
            response = {"acquisition_id": 123}
        elif settings.fortlogs.FORTLOGS_URL in args[0]:
            # Log only for ERROR_TABLE_NAME
            stored_buf.add(json.loads(kwargs["data"])["name"])

        class RtObj:
            async def json(self):
                return response

        return RtObj()

    with unittest.mock.patch("aiohttp.ClientSession.post", new=async_post):
        log_fetcher = FgcLogFetcher(request)
        await asyncio.wait_for(log_fetcher.execute(), 5.0)
        assert done
        assert len(stored_buf) == 1
        assert fgc_logger.fetch_logs.ERROR_TABLE_NAME in stored_buf


@pytest.mark.asyncio
async def test_logging_errors_and_ok_reads(reload_fgclogger):

    done = False

    async def err_callback(*args, **kwargs):
        raise LogReadAborted("Nope", err_code=123, err_msg="Ups") 

    async def done_callback(*args, **kwargs):
        nonlocal done
        done = True

    timestamp = time.time()

    logs = ["ACQ", "I_MEAS", "TEMP"]
    props = ["LOG.SPY.DATA[0]", "WILL.FAIL", "WILL.FAIL"]
    get_option = f"DATA|{timestamp:.6f},500,500,0"

    request = LogReadRequest(
        DEVICE_1,
        EventType.SLOW_ABORT,
        timestamp,
        err_callback,
        done_callback,
        pyfgc_rbac.get_token_location()
    )

    for idx, log in enumerate(logs):
        request.add_log(log, {"prop": props[idx], "get_option": get_option})
        request.set_log_ready(log)

    # Fetch log and check if it is stored in fortlogs and acknowledged

    stored_buf = set()

    async def async_post(self, *args, **kwargs):
        response = None

        if settings.secret.AUTH_URL in args[0]:
            response = {"access_token": "token_string", "refresh_token": "resfresh_string"}
        elif settings.fortlogs.FORTLOGS_URL+"/fgc-logger" in args[0]:
            response = {"acquisition_id": 123}
        elif settings.fortlogs.FORTLOGS_URL in args[0]:
            # Log only for ERROR_TABLE_NAME
            stored_buf.add(json.loads(kwargs["data"])["name"])

        class RtObj:
            async def json(self):
                return response

        return RtObj()

    with unittest.mock.patch("aiohttp.ClientSession.post", new=async_post):
        log_fetcher = FgcLogFetcher(request)
        await asyncio.wait_for(log_fetcher.execute(), 5.0)
        assert done
        assert len(stored_buf) == 2
        assert "ACQ" in stored_buf
        assert "I_MEAS" not in stored_buf
        assert "TEMP" not in stored_buf
        assert fgc_logger.fetch_logs.ERROR_TABLE_NAME in stored_buf


# EOF