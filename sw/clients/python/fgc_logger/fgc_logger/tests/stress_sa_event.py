"""Script to search for LOG_PLEASE problems
"""
import queue
import time
import datetime

import pyfgc

DEVICE = "RFNA.866.07.ETH1"
STATE_TIMEOUT = 20.0
CURRENT_TIMEOUT = 60.0

pub_q = queue.Queue()

# Create function to put new pub data in queue, so that it can be easily analysed
def monitor_cb(data_dict, _data_list, _gateway, timestamp):
    pub_q.put(data_dict[DEVICE])

def monitor_cb_err(exc, _gateway, timestamp):
    formatted_time = datetime.datetime.fromtimestamp(timestamp)
    print(f"Monitor error at {formatted_time}.")
    print(exc)

# Wait for queued data to fullfill a certain condition
def wait_for_cond(func, timeout):
    raise_timeout_time = time.time() + timeout
    while True:
        new_timeout = raise_timeout_time - time.time()
        if new_timeout <= 0:
            raise TimeoutError()
        try:
            data = pub_q.get(timeout=new_timeout)
        except queue.Empty:
            continue
        if func(data):
            return data

# Set a current reference, and set state DIRECT
def set_pc_dt(curr_val=None):
    if curr_val is not None:
        rsp = pyfgc.set(DEVICE, "REF.DIRECT.I.VALUE", curr_val)
        rsp.value
    rsp = pyfgc.set(DEVICE, "MODE.PC", "DIRECT")
    rsp.value
    wait_for_cond(lambda x: x["STATE_PC"] == "DIRECT", STATE_TIMEOUT)

# Set state OFF
def set_pc_of():
    rsp = pyfgc.set(DEVICE, "MODE.PC", "OFF")
    rsp.value
    wait_for_cond(lambda x: x["STATE_PC"] == "OFF", STATE_TIMEOUT)

# Wait until current value, and trigger fault
def trigger_at_current(value, delay=None):
    wait_for_cond(lambda x: x["I_MEAS"] >= value, CURRENT_TIMEOUT)
    if delay:
        time.sleep(delay)
    rsp = pyfgc.set(DEVICE, "FGC.FAULTS", "VS_FAULT")
    rsp.value

def reset():
    rsp = pyfgc.set(DEVICE, "LOG.SPY", "RESET")
    rsp.value
    set_pc_of()

# Set up monitor
monitor = pyfgc.monitor_session(monitor_cb, DEVICE, 5, callback_err=monitor_cb_err)
monitor.start()

#### Script ####

# Disable slow-abort
pyfgc.set(DEVICE, "MODE.SLOW_ABORT_FAULTS", "").value
pyfgc.set(DEVICE, "LIMITS.I.RMS_FAULT", 60)
reset()

# Try for different current values
for curr_val in range(4, 20+1, 4):

    # Trigger fault at a current value, before finishing ramping up
    for trigger_val in range(4, curr_val+1, 2):

        print(f"Ref: {curr_val:2} A; Trigger PM when reaching {trigger_val} A.")

        set_pc_dt(curr_val)
        trigger_at_current(trigger_val-0.1)
        try:
            wait_for_cond(lambda x: "LOG_PLEASE" in x["ST_UNLATCHED"], 10.0)
        except TimeoutError:
            print("ERROR: LOG_PLEASE not detected!")
        else:
            print("OK!")
        try:
            wait_for_cond(lambda x: x["STATE_PC"] == "FLT_OFF", 10.0)
        except TimeoutError:
            pass
        reset()

    # Trigger after finishing ramping up and waiting some time
    for wait_time in range(0, 6+1, 2):

        print(f"Ref: {curr_val:2} A; Trigger PM {wait_time} s after stabilizing.")

        set_pc_dt(curr_val)
        trigger_at_current(curr_val-0.1, wait_time)
        try:
            wait_for_cond(lambda x: "LOG_PLEASE" in x["ST_UNLATCHED"], 10.0)
        except TimeoutError:
            print("ERROR: LOG_PLEASE not detected!")
            exit(1)
        else:
            print("OK!")
        try:
            wait_for_cond(lambda x: x["STATE_PC"] == "FLT_OFF", 10.0)
        except TimeoutError:
            pass
        reset()

# Cleanup
monitor.stop()



# EOF