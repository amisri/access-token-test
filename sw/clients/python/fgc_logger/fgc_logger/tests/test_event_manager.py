import os
import pytest
import logging
import asyncio
import copy
import time

import pyfgc
import pyfgc_log

import fgc_logger
from fgc_logger.event_analyser import EvtAnalyser
from fgc_logger.event_manager import EventManager, FgcDataAnalyser


logger = logging.getLogger(__name__)
logging.getLogger("fgc_logger").setLevel(logging.DEBUG)

GATEWAY_1 = "cfc-866-reth1"
DEVICE_1_1 = "RFNA.866.04.ETH1"
DEVICE_1_2 = "RFNA.866.07.ETH1"
DEVICE_1_3 = "RFNA.866.11.ETH1"

class MockLogFetcher:

    def __init__(self, request):
        pass
    
    async def execute(self):
        pass

class MockEvtAnalyser(EvtAnalyser):
    
    def __init__(self, device, loop):
        self.device = device
        self.active = False
        self.timestamp = 0

    def __str__(self):
        return f"{self.__class__.__name__}"

    async def ack_log_get(self, device, prop, get_option):
        pass

    async def get_log_menu_data(self, timestamp = 0.0):
        pass

    async def ingest_new_data(self, data_dict, timestamp):
        # Just for testing
        if "activate" in data_dict:
            self.active = True
            self.timestamp = timestamp
        else:
            self.active = False
        return self.active

    @property
    def event_timestamp(self):
        return self.timestamp

    @property
    def event_requests(self):
        return {self} if self.active else None # Just return something

    @property
    def event_active(self):
        return self.active

    async def reset(self):
        self.active = False


class MockEvtAnalyser1(MockEvtAnalyser):

    async def ingest_new_data(self, data_dict, timestamp):
        # Just for testing
        if "activate_1" in data_dict:
            await super().ingest_new_data({"activate": True}, timestamp)
        else:
            await super().ingest_new_data({}, timestamp)


class MockEvtAnalyser2(MockEvtAnalyser):

    async def ingest_new_data(self, data_dict, timestamp):
        # Just for testing
        if "activate_2" in data_dict:
            await super().ingest_new_data({"activate": True}, timestamp)
        else:
            await super().ingest_new_data({}, timestamp)


@pytest.mark.asyncio
async def test_fgc_data_analyser_creation(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    pub_data = {

    }

    data_analyser = FgcDataAnalyser("DEV_NAME", 1, event_loop)
    assert data_analyser

    await data_analyser.ingest_data(pub_data, time.time())
    assert not data_analyser.evt_active


@pytest.mark.asyncio
async def test_fgc_data_analyser_event_detected(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate' will trigger and event on the mock class MockEvtAnalyser
    pub_data_0 = {
    }
    pub_data_1 = {
        "activate": True,
    }

    data_analyser = FgcDataAnalyser("DEV_NAME", 1, event_loop)

    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active

    await data_analyser.ingest_data(pub_data_1, time.time())
    assert data_analyser.evt_active
    assert isinstance(data_analyser.active_analyser, MockEvtAnalyser)

    await data_analyser.reset()


@pytest.mark.asyncio
async def test_fgc_data_analyser_multiple_analysers(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser1, MockEvtAnalyser2,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate_2' will trigger and event on the mock class MockEvtAnalyser2 only
    pub_data = {
        "activate_2": True,
    }

    data_analyser = FgcDataAnalyser("DEV_NAME", 1, event_loop)

    await data_analyser.ingest_data(pub_data, time.time())
    assert data_analyser.evt_active
    assert not isinstance(data_analyser.active_analyser, MockEvtAnalyser1)
    assert isinstance(data_analyser.active_analyser, MockEvtAnalyser2)

    await data_analyser.reset()


@pytest.mark.asyncio
async def test_fgc_data_analyser_event_started_and_finished_1(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate' will trigger and event on the mock class MockEvtAnalyser
    pub_data_0 = {
    }
    pub_data_1 = {
        "activate": True,
    }

    data_analyser = FgcDataAnalyser("DEV_NAME", 1, event_loop)

    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active
    await data_analyser.ingest_data(pub_data_1, time.time())
    assert data_analyser.evt_active
    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active
    assert data_analyser.active_analyser is None

    await data_analyser.reset()


@pytest.mark.asyncio
async def test_fgc_data_analyser_event_started_and_finished_2(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate' will trigger and event on the mock class MockEvtAnalyser
    pub_data_0 = {
    }
    pub_data_1 = {
        "activate": True,
    }

    data_analyser = FgcDataAnalyser("DEV_NAME", 1, event_loop)

    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active
    await asyncio.sleep(0.1)
    await data_analyser.ingest_data(pub_data_1, time.time())
    assert data_analyser.evt_active
    await asyncio.sleep(0.1)
    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active
    assert data_analyser.active_analyser is None

    await data_analyser.reset()


@pytest.mark.asyncio
async def test_fgc_data_analyser_two_different_events(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser1, MockEvtAnalyser2,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate' will trigger and event on the mock class MockEvtAnalyser
    pub_data_0 = {
    }
    pub_data_1 = {
        "activate_1": True,
    }
    pub_data_2 = {
        "activate_2": True,
    }

    data_analyser = FgcDataAnalyser("DEV_NAME", 1, event_loop)

    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active

    await data_analyser.ingest_data(pub_data_1, time.time())
    assert data_analyser.evt_active
    assert isinstance(data_analyser.active_analyser, MockEvtAnalyser1)

    await data_analyser.ingest_data(pub_data_2, time.time()) # The first time will reset the previous event, but not trigger a new one yet.
    await data_analyser.ingest_data(pub_data_2, time.time())
    assert data_analyser.evt_active
    assert isinstance(data_analyser.active_analyser, MockEvtAnalyser2)

    await data_analyser.ingest_data(pub_data_0, time.time())
    assert not data_analyser.evt_active

    await data_analyser.reset()


@pytest.mark.asyncio
async def test_event_manager_creation(event_loop):

    event_manager = EventManager(event_loop, asyncio.Queue())
    assert event_manager


@pytest.mark.asyncio
async def test_event_manager_run_and_stop(event_loop):

    event_manager = EventManager(event_loop, asyncio.Queue())
    event_manager_task = asyncio.ensure_future(event_manager.run())

    await asyncio.sleep(0.05)

    assert event_manager.running
    await event_manager.stop()

    await asyncio.wait_for(event_manager_task, 0.05)
    assert not event_manager.running


@pytest.mark.asyncio
async def test_class_supported(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
        11: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)

    event_manager = EventManager(event_loop, asyncio.Queue())

    assert event_manager._device_class_supported(1)
    assert event_manager._device_class_supported(11)

    assert not event_manager._device_class_supported(0)
    assert not event_manager._device_class_supported(12)

    assert not event_manager._device_class_supported(2)
    assert not event_manager._device_class_supported(21)


@pytest.mark.asyncio
async def test_class_family_supported(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
        11: [MockEvtAnalyser,],
        10: [MockEvtAnalyser,], # All classes 1X will be supported
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)

    event_manager = EventManager(event_loop, asyncio.Queue())

    assert event_manager._device_class_supported(1)
    assert event_manager._device_class_supported(11)

    assert event_manager._device_class_supported(12)
    assert event_manager._device_class_supported(13)
    
    assert not event_manager._device_class_supported(2)
    assert not event_manager._device_class_supported(21)


@pytest.mark.asyncio
async def test_identify_new_device(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    pub_data = {
        "time" : time.time(),
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
            }
        }
    }

    input_queue = asyncio.Queue()
    event_manager = EventManager(event_loop, input_queue)
    event_manager_task = asyncio.ensure_future(event_manager.run())

    # Send data
    await input_queue.put(pub_data)
    await asyncio.sleep(0.01)

    # Just to confirm that it is still running
    with pytest.raises(asyncio.InvalidStateError):
        event_manager_task.result()

    # Check if device has been identified
    assert "GW_1" in event_manager.gw_analysers
    assert "DEV_1" in event_manager.gw_analysers["GW_1"]
    assert isinstance(event_manager.gw_analysers["GW_1"]["DEV_1"], FgcDataAnalyser)
    assert not event_manager.gw_analysers["GW_1"]["DEV_1"].evt_active

    await event_manager.stop()
    await asyncio.wait_for(event_manager_task, 0.05)


@pytest.mark.asyncio
async def test_new_device_goes_online(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    timestamp = time.time()
    pub_data_0 = {
        "time" : timestamp,
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"CLASS_VALID"},
            },
            "DEV_2" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"CLASS_VALID"},
            }
        }
    }
    pub_data_1 = {
        "time" : timestamp + 1.0,
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
            },
            "DEV_2" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"CLASS_VALID"},
            }
        }
    }
    pub_data_2 = {
        "time" : timestamp + 2.0,
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
            },
            "DEV_2" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
            }
        }
    }

    input_queue = asyncio.Queue()
    event_manager = EventManager(event_loop, input_queue)
    event_manager_task = asyncio.ensure_future(event_manager.run())

    # Send new data, gateway should be identified but no devices
    await input_queue.put(pub_data_0)
    await asyncio.sleep(0.01)
    assert "GW_1" in event_manager.gw_analysers
    assert not "DEV_1" in event_manager.gw_analysers["GW_1"]
    assert not "DEV_2" in event_manager.gw_analysers["GW_1"]

    # Send new data, gateway and device 1 should be identified, but not device 2
    await input_queue.put(pub_data_1)
    await asyncio.sleep(0.01)
    assert "GW_1" in event_manager.gw_analysers
    assert "DEV_1" in event_manager.gw_analysers["GW_1"]
    assert not "DEV_2" in event_manager.gw_analysers["GW_1"]

    # Send new data, gateway and both devices should be identified
    await input_queue.put(pub_data_2)
    await asyncio.sleep(0.01)
    assert "GW_1" in event_manager.gw_analysers
    assert "DEV_1" in event_manager.gw_analysers["GW_1"]
    assert "DEV_2" in event_manager.gw_analysers["GW_1"]

    await event_manager.stop()
    await asyncio.wait_for(event_manager_task, 0.05)


@pytest.mark.asyncio
async def test_event_manager_receives_an_event(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate' will trigger and event on the mock class MockEvtAnalyser
    pub_data_0 = {
        "time" : None, # Must be filled
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
            }
        }
    }
    pub_data_1 = {
        "time" : None, # Must be filled
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
                "activate": True,
            }
        }
    }

    input_queue = asyncio.Queue()
    event_manager = EventManager(event_loop, input_queue)
    event_manager_task = asyncio.ensure_future(event_manager.run())

    timestamp = time.time()

    pub_data_0["time"] = timestamp
    await input_queue.put(pub_data_0)
    await asyncio.sleep(0.01)
    assert not event_manager.gw_analysers["GW_1"]["DEV_1"].evt_active

    pub_data_1["time"] = timestamp + 1.0
    await input_queue.put(pub_data_1)
    await asyncio.sleep(0.01)
    assert event_manager.gw_analysers["GW_1"]["DEV_1"].evt_active

    pub_data_0["time"] = timestamp + 2.0
    await input_queue.put(pub_data_0)
    await asyncio.sleep(0.01)
    assert not event_manager.gw_analysers["GW_1"]["DEV_1"].evt_active

    await event_manager.stop()
    await asyncio.wait_for(event_manager_task, 0.05)


@pytest.mark.asyncio
async def test_clean_up_after_unresponsive_gateway(monkeypatch, event_loop):

    analysers = {
        1: [MockEvtAnalyser,],
    }
    monkeypatch.setattr(fgc_logger.event_manager, "EVT_ANALYSERS_PER_CLASS", analysers)
    monkeypatch.setattr(fgc_logger.event_manager, "FgcLogFetcher", MockLogFetcher)

    # 'activate' will trigger and event on the mock class MockEvtAnalyser
    pub_data = {
        "time" : time.time(),
        "gateway": "GW_1",
        "devices": {
            "DEV_1" : {
                "CLASS_ID" : 1,
                "DATA_STATUS": {"DATA_VALID", "CLASS_VALID"},
                "activate": True,
            }
        }
    }

    input_queue = asyncio.Queue()
    event_manager = EventManager(event_loop, input_queue)
    event_manager_task = asyncio.ensure_future(event_manager.run())

    await input_queue.put(pub_data)
    await asyncio.sleep(0.01)
    assert event_manager.gw_analysers["GW_1"]["DEV_1"].evt_active

    monkeypatch.setattr(fgc_logger.event_manager, "CLEANUP_TIMEOUT_S", 0.01)

    await input_queue.put(None) # Will not do anything, but cleanup will be triggered, stoppong event analyser
    await asyncio.sleep(0.01)
    assert not event_manager.gw_analysers["GW_1"]["DEV_1"].evt_active

    await event_manager.stop()
    await asyncio.wait_for(event_manager_task, 0.05)



# EOF