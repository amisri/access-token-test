import os
import pytest
import logging
import asyncio
import time

import pyfgc
import pyfgc_log

import fgc_logger
from fgc_logger.fetch_logs import FgcLogFetcher
from fgc_logger.log_read_request import LogReadRequest, EventType, RequestAborted, RequestNotCompleted


logger = logging.getLogger(__name__)
logging.getLogger("fgc_logger").setLevel(logging.DEBUG)

DEVICE_1 = "RFNA.866.04.ETH1"
properties_to_check_1 = {
    # Property      : duration,
    "ACQ"           : 1.0,
    "TEMP"          : 300.0,
    "EVENTS"        : None,
    "CONFIGURATION" : None,
}

def get_time():
    return float("{:.6f}".format(time.time()))

@pytest.mark.asyncio
async def test_request_create():

    async def done_ack(*args, **kwargs):
        pass

    async def fgc_err_ack(*args, **kwargs):
        raise RuntimeError()

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        "token_data"
    )

    assert request.device_name == DEVICE_1
    assert request.event_type == EventType.DIM
    assert request.event_timestamp == timestamp
    assert request.token == "token_data"
    assert len(request.log_read_done) == 0


@pytest.mark.asyncio
async def test_request_add_logs():
    
    async def done_ack(*args, **kwargs):
        pass

    async def fgc_err_ack(*args, **kwargs):
        raise RuntimeError()

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        None
    )

    log_defs = [
        ("LOG_1", "PROP.1", "DATA|1,2,3,0"),
        ("LOG_2", "PROP.2", "DATA|1,2,3,0"),
        ("LOG_3", "PROP.3", "DATA|1,2,3,0"),
    ]
    
    for log, prop, get_option in log_defs:
        request.add_log(log, {"prop": prop, "get_option": get_option})

    assert len(request.log_read_done) == len(log_defs)
    assert not any({done for done in request.log_read_done.values()})


@pytest.mark.asyncio
async def test_request_set_log_ready():

    async def done_ack(*args, **kwargs):
        pass

    async def fgc_err_ack(*args, **kwargs):
        raise RuntimeError()

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        None
    )

    log_defs = [
        ("LOG_1", "PROP.1", "DATA|1,2,3,0"),
        ("LOG_2", "PROP.2", "DATA|1,2,3,0"),
        ("LOG_3", "PROP.3", "DATA|1,2,3,0"),
    ]
    
    for log, prop, get_option in log_defs:
        request.add_log(log, {"prop": prop, "get_option": get_option})

    # Should raise Timeout
    with pytest.raises(asyncio.TimeoutError):
        await request.wait_for_log_ready("LOG_1", 0.1)

    # Should return without raising timeout
    request.set_log_ready("LOG_1")
    await request.wait_for_log_ready("LOG_1", 0.1)

    # Should again not be ready
    request.set_log_ready("LOG_1", is_ready=False)
    with pytest.raises(asyncio.TimeoutError):
        await request.wait_for_log_ready("LOG_1", 0.1)


@pytest.mark.asyncio
async def test_request_aborted():

    async def done_ack(*args, **kwargs):
        pass

    async def fgc_err_ack(*args, **kwargs):
        raise RuntimeError()

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        None
    )

    log_defs = [
        ("LOG_1", "PROP.1", "DATA|1,2,3,0"),
        ("LOG_2", "PROP.2", "DATA|1,2,3,0"),
        ("LOG_3", "PROP.3", "DATA|1,2,3,0"),
    ]
    
    for log, prop, get_option in log_defs:
        request.add_log(log, {"prop": prop, "get_option": get_option})

    await request.abort_request()

    # Should raise Timeout
    with pytest.raises(RequestAborted):
        await request.wait_for_log_ready("LOG_1", 0.1)


@pytest.mark.asyncio
async def test_mark_log_read():

    async def done_ack(*args, **kwargs):
        pass

    async def fgc_err_ack(*args, **kwargs):
        raise RuntimeError()

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        None
    )

    log_defs = [
        ("LOG_1", "PROP.1", "DATA|1,2,3,0"),
        ("LOG_2", "PROP.2", "DATA|1,2,3,0"),
        ("LOG_3", "PROP.3", "DATA|1,2,3,0"),
    ]
    
    for log, prop, get_option in log_defs:
        request.add_log(log, {"prop": prop, "get_option": get_option})
        request.set_log_ready(log)


    await request.mark_log_read("LOG_1")

    # Check if it has been marked
    for log, _, _ in log_defs:
        if log == "LOG_1":
            assert request.log_read_done[log]
        else:
            assert not request.log_read_done[log]


@pytest.mark.asyncio
async def test_request_done():

    done = False

    async def done_ack(*args, **kwargs):
        nonlocal done
        done = True

    async def fgc_err_ack(*args, **kwargs):
        raise RuntimeError()

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        None
    )

    log_defs = [
        ("LOG_1", "PROP.1", "DATA|1,2,3,0"),
        ("LOG_2", "PROP.2", "DATA|1,2,3,0"),
        ("LOG_3", "PROP.3", "DATA|1,2,3,0"),
    ]
    
    for log, prop, get_option in log_defs:
        request.add_log(log, {"prop": prop, "get_option": get_option})
        request.set_log_ready(log)

    # Should raise an exception
    with pytest.raises(RequestNotCompleted):
        await request.ack_request_done()
    assert not done

    # Should still raise an exception
    await request.mark_log_read("LOG_1")
    await request.mark_log_read("LOG_2")

    with pytest.raises(RequestNotCompleted):
        await request.ack_request_done()
    assert not done

    # Should be done
    await request.mark_log_read("LOG_3")
    await request.ack_request_done()
    assert done


@pytest.mark.asyncio
async def test_log_read_error():

    executed = False

    async def done_ack(*args, **kwargs):
        pass

    async def fgc_err_ack(log_name, err_nr, err_msg):
        nonlocal executed
        executed = True

        assert log_name == "LOG_1"
        assert err_nr == 777
        assert err_msg == "test_error"

    timestamp = get_time()

    request = LogReadRequest(
        DEVICE_1,
        EventType.DIM,
        timestamp,
        fgc_err_ack,
        done_ack,
        None
    )

    log_defs = [
        ("LOG_1", "PROP.1", "DATA|1,2,3,0"),
        ("LOG_2", "PROP.2", "DATA|1,2,3,0"),
        ("LOG_3", "PROP.3", "DATA|1,2,3,0"),
    ]
    
    for log, prop, get_option in log_defs:
        request.add_log(log, {"prop": prop, "get_option": get_option})
        request.set_log_ready(log)

    await request.check_log_read_error("LOG_1", 777, "test_error")
    assert executed



# EOF