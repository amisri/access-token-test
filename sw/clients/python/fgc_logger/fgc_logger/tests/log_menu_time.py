import time
import pyfgc

ITERS = 10

with pyfgc.fgc("RFNA.866.07.ETH1") as fgc:
    ta_0 = time.time()
    for _ in range(ITERS):
        rsp = fgc.get("LOG.MENU")
    ta_1 = time.time()

ta_avg = (ta_1 - ta_0) / ITERS

with pyfgc.fgc("RFNA.866.07.ETH1") as fgc:
    tb_0 = time.time()
    for _ in range(ITERS):
        rsp = fgc.get("LOG.MENU.STATUS")
    tb_1 = time.time()

tb_avg = (tb_1 - tb_0) / ITERS

print(f"Time LOG.MENU:        {ta_avg} s")
print(f"Time LOG.MENU.STATUS: {tb_avg} s")
