import os
import pytest
import logging
import asyncio
import copy
import time

import pyfgc
import pyfgc_log

import fgc_logger
from fgc_logger.event_analyser import EvtAnalyser, PostMortemAnalyser, LogReadAborted
from fgc_logger.event_manager import EventManager, FgcDataAnalyser
from fgc_logger.log_read_request import EventType, RequestAborted


@pytest.mark.asyncio
async def test_creation(event_loop):

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    assert analyser

@pytest.mark.asyncio
async def test_dont_trigger_event(event_loop):

    pub_data = {
        "ST_UNLATCHED" : {""},
    }

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert not analyser.event_active
    assert analyser.event_requests is None


@pytest.mark.asyncio
async def test_trigger_event(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
                "LOG_2" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.2"
                }
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request_set = analyser.event_requests
    assert len(request_set) == 1

    request = request_set.pop()
    assert request.event_timestamp == event_time
    assert request.event_type is EventType.POST_MORTEM

    assert len(request.logs) == 2
    assert "LOG_1" in request.logs
    assert "LOG_2" in request.logs


@pytest.mark.asyncio
async def test_unfreezable_log_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request = analyser.event_requests.pop()
    assert request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_freezable_log_not_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "FZ", "RUN"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request = analyser.event_requests.pop()
    assert not request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_freezable_log_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "FZ"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request = analyser.event_requests.pop()
    assert request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_freezable_log_turns_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    time_0 = time.time()
    time_1 = time_0 + 1.0

    def get_log_menu_gen():

        menu_data = {
            time_0 : {
                "PM_TIME" : time_0,
                "LOG_DATA" : {
                    "LOG_1" : {
                        "STATUS" : {"SAVE", "FZ", "RUN"},
                        "PROP" : "LOG.PROP.1"
                    },
                }
            },
            time_1 : {
                "PM_TIME" : time_0,
                "LOG_DATA" : {
                    "LOG_1" : {
                        "STATUS" : {"SAVE", "FZ"},
                        "PROP" : "LOG.PROP.1"
                    },
                }
            },
        } 

        last_cached = None

        async def get_log_menu(self, timestamp = 0):

            nonlocal last_cached
            if timestamp == 0:
                assert last_cached
                return last_cached

            data = menu_data[timestamp]
            last_cached = data
            return data

        return get_log_menu

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu_gen())

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time_0)

    request = analyser.event_requests.pop()
    assert not request._log_ready["LOG_1"]

    await analyser.ingest_new_data(pub_data, time_1) # At time_1, the flag "RUN" will be not active
    assert request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_event_cancelled(monkeypatch, event_loop):

    pub_data_0 = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }
    pub_data_1 = {
        "ST_UNLATCHED" : {},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data_0, event_time)

    request = analyser.event_requests.pop()
    await request.wait_for_log_ready("LOG_1", 1.0)

    # Not having "LOG_PLEASE" will reset the request
    await analyser.ingest_new_data(pub_data_1, event_time + 1.0)
    with pytest.raises(RequestAborted):
        await request.wait_for_log_ready("LOG_1", 1.0)


@pytest.mark.asyncio
async def test_fgc_error_log_cancelled(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, event_time)

    request = analyser.event_requests.pop()
    with pytest.raises(LogReadAborted):
        await request.check_log_read_error("LOG_1", 123, "random error")



@pytest.mark.asyncio
async def test_fgc_error_pm_in_progress(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, event_time)

    request = analyser.event_requests.pop()

    # Initially it will be ready
    await request.wait_for_log_ready("LOG_1", 0.1)

    # Finding 'pm in progress' will make it not ready (while not raising LogReadAborted)
    await request.check_log_read_error("LOG_1", 55, "pm in progress")
    with pytest.raises(asyncio.TimeoutError):
        await request.wait_for_log_ready("LOG_1", 0.1)


@pytest.mark.asyncio
async def test_logs_done_callback(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    logs_acknowledged = False

    async def ack_log_get(self, device, prop, get_option):

        nonlocal logs_acknowledged

        assert prop == "LOG.MENU.PM_TIME"
        assert get_option == "ZERO"
        logs_acknowledged = True

    monkeypatch.setattr(PostMortemAnalyser, "ack_log_get", ack_log_get)


    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, event_time)
    request = analyser.event_requests.pop()

    await request.wait_for_log_ready("LOG_1", 0.1)
    await request.mark_log_read("LOG_1")
    await request.ack_request_done()
    assert logs_acknowledged


@pytest.mark.asyncio
async def test_logs_done_callback_with_dims(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
                "LOG_2" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.2"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    dim_acknowledged = False
    logs_acknowledged = False

    async def ack_log_get(self, device, prop, get_option):

        nonlocal dim_acknowledged, logs_acknowledged

        if prop == "LOG.PROP.2": # DIMs will be acknowledged individually
            assert get_option == "SYNCHED"
            dim_acknowledged = True
        else:
            assert prop == "LOG.MENU.PM_TIME"
            assert get_option == "ZERO"
            logs_acknowledged = True

    monkeypatch.setattr(PostMortemAnalyser, "ack_log_get", ack_log_get)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, event_time)
    request = analyser.event_requests.pop()

    await request.wait_for_log_ready("LOG_1", 0.1)
    await request.wait_for_log_ready("LOG_2", 0.1)
    await request.mark_log_read("LOG_1")
    await request.mark_log_read("LOG_2")
    await request.ack_request_done()
    assert dim_acknowledged
    assert logs_acknowledged


@pytest.mark.asyncio
async def test_log_menu_not_needed_after_logs_read(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()
    log_menu_called = False

    async def get_log_menu(*args):
        nonlocal log_menu_called
        log_menu_called = True

        return {
            "PM_TIME" : event_time,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.1"
                },
                "LOG_2" : {
                    "STATUS" : {"SAVE"},
                    "PROP" : "LOG.PROP.2"
                },
            }
        }

    monkeypatch.setattr(PostMortemAnalyser, "get_log_menu_data", get_log_menu)

    analyser = PostMortemAnalyser("DEV_1", event_loop)
    
    # New event causes log menu to be read
    log_menu_called = False
    await analyser.ingest_new_data(pub_data, event_time)
    assert log_menu_called

    request = analyser.event_requests.pop()

    # LOG_2 not read yet, this causes log menu to be read
    await request.mark_log_read("LOG_1")
    log_menu_called = False
    await analyser.ingest_new_data(pub_data, event_time)
    assert log_menu_called

    # Both logs have been read, so log menu does not need to be read
    await request.mark_log_read("LOG_2")
    log_menu_called = False
    await analyser.ingest_new_data(pub_data, event_time)
    assert not log_menu_called



# EOF