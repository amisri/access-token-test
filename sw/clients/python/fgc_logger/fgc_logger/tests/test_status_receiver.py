import pytest
import logging
import asyncio
import contextlib

import pyfgc

from fgc_logger.status_receiver import StatusSrvReader, StatusMonitorPort


logger = logging.getLogger(__name__)
logging.getLogger("fgc_logger").setLevel(logging.DEBUG)

GATEWAY_1 = "CFC-866-RETH1"
MONITOR_PORT = 12345

@contextlib.contextmanager
def new_udp_stream(gateway, sub_period, sub_port, sub_id, token=None):
    __tracebackhide__ = True
    with pyfgc.fgc(gateway) as fgc_session:
        if token:
            res_0 = fgc_session.set("CLIENT.TOKEN", token)
            res_0.value

        res_1 = fgc_session.set("CLIENT.UDP.SUB.ID", sub_id)
        res_2 = fgc_session.set("CLIENT.UDP.SUB.PERIOD", sub_period)
        res_3 = fgc_session.set("CLIENT.UDP.SUB.PORT", sub_port)

        # Will raise exception if any command failed

        res_1.value
        res_2.value
        res_3.value

        yield
        return

@pytest.mark.asyncio
async def test_status_server_reader_create(event_loop):
    queue = asyncio.Queue()
    server_reader = StatusSrvReader(queue, event_loop)
    assert server_reader

@pytest.mark.asyncio
async def test_status_server_reader_start_stop(event_loop):
    queue = asyncio.Queue()
    server_reader = StatusSrvReader(queue, event_loop)
    server_task = asyncio.ensure_future(server_reader.run())

    await asyncio.sleep(0.1)

    await server_reader.stop()
    await asyncio.wait_for(server_task, 5.0)

@pytest.mark.asyncio
async def test_status_server_read_one(event_loop):
    queue = asyncio.Queue()
    server_reader = StatusSrvReader(queue, event_loop, period=0.1)
    server_task = asyncio.ensure_future(server_reader.run())

    data = await asyncio.wait_for(queue.get(), 5.0)
    assert isinstance(data, dict)

    await server_reader.stop()
    await server_task

@pytest.mark.asyncio
async def test_status_server_read_many_gateways(event_loop):
    queue = asyncio.Queue()
    server_reader = StatusSrvReader(queue, event_loop, period=0.1)
    server_task = asyncio.ensure_future(server_reader.run())

    set_gws = set()
    data = await asyncio.wait_for(queue.get(), 5.0)
    set_gws.add(data["gateway"])
    while not queue.empty():
        data = queue.get_nowait()
        set_gws.add(data["gateway"])

    assert GATEWAY_1.lower() in set_gws
    assert len(set_gws) > 1

    await server_reader.stop()
    await server_task

@pytest.mark.asyncio
async def test_status_monitor_create(event_loop):
    queue = asyncio.Queue()
    server_reader = StatusMonitorPort(queue, event_loop, MONITOR_PORT)

@pytest.mark.asyncio
async def test_status_monitor_start_stop(event_loop):
    queue = asyncio.Queue()
    server_reader = StatusMonitorPort(queue, event_loop, MONITOR_PORT)
    server_task = asyncio.ensure_future(server_reader.run())
    await asyncio.sleep(0.1)

    await server_reader.stop()
    await server_task

@pytest.mark.asyncio
async def test_status_monitor_read_one(event_loop):
    queue = asyncio.Queue()
    with new_udp_stream(GATEWAY_1, 5, MONITOR_PORT, 0):
        server_reader = StatusMonitorPort(queue, event_loop, MONITOR_PORT)
        server_task = asyncio.ensure_future(server_reader.run())
        data = await asyncio.wait_for(queue.get(), 5.0)
        assert isinstance(data, dict)

    await server_reader.stop()
    await server_task

@pytest.mark.asyncio
async def test_status_monitor_read_several_packets(event_loop):
    queue = asyncio.Queue()
    data_recv_list = []
    with new_udp_stream(GATEWAY_1, 5, MONITOR_PORT, 0):
        server_reader = StatusMonitorPort(queue, event_loop, MONITOR_PORT)
        server_task = asyncio.ensure_future(server_reader.run())
        await asyncio.sleep(0.5)
        while not queue.empty():
            data = queue.get_nowait()
            data_recv_list.append(data["time"])
            assert GATEWAY_1.lower() == data["gateway"]

    assert len(data_recv_list) > 1
    await server_reader.stop()
    await server_task


# EOF
