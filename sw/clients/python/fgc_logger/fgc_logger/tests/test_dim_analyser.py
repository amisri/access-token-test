import os
import pytest
import logging
import asyncio
import copy
import time

import pyfgc
import pyfgc_log

import fgc_logger
from fgc_logger.event_analyser import EvtAnalyser, DIMAnalyser, LogReadAborted
from fgc_logger.event_manager import EventManager, FgcDataAnalyser
from fgc_logger.log_read_request import EventType, RequestAborted


@pytest.mark.asyncio
async def test_creation(event_loop):

    analyser = DIMAnalyser("DEV_1", event_loop)
    assert analyser

@pytest.mark.asyncio
async def test_dont_trigger_event(event_loop):

    pub_data = {
        "ST_UNLATCHED" : {""},
    }

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert not analyser.event_active
    assert analyser.event_requests is None


@pytest.mark.asyncio
async def test_trigger_dim_event(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : 0,
            "SA_TIME" : 0,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, event_time)
    assert analyser.event_active

    request_set = analyser.event_requests
    assert len(request_set) == 1

    request = analyser.event_requests.pop()
    assert request.event_timestamp == event_time
    assert request.event_type is EventType.DIM
    assert len(request.logs) == 1
    assert "LOG_1" in request.logs


@pytest.mark.asyncio
async def test_triggering_two_dim_events_creates_two_requests(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "PM_TIME" : 0,
            "SA_TIME" : 0,
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
                "LOG_2" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.2"
                }
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, event_time)
    assert analyser.event_active

    request_set = analyser.event_requests
    assert len(request_set) == 2

    for request in request_set:
        assert request.event_timestamp == event_time
        assert request.event_type is EventType.DIM
        assert len(request.logs) == 1
        assert (
            ("LOG_1" in request.logs and "LOG_2" not in request.logs) or
            ("LOG_1" not in request.logs and "LOG_2" in request.logs)
        )


@pytest.mark.asyncio
async def test_unfreezable_log_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request = analyser.event_requests.pop()
    assert request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_freezable_log_not_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "FZ", "RUN", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request = analyser.event_requests.pop()
    assert not request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_freezable_log_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "FZ", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    assert analyser.event_active

    request = analyser.event_requests.pop()
    assert request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_freezable_log_turns_ready(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    time_0 = time.time()
    time_1 = time_0 + 1.0

    def get_log_menu_gen():

        menu_data = {
            time_0 : {
                "LOG_DATA" : {
                    "LOG_1" : {
                        "STATUS" : {"SAVE", "FZ", "RUN", "DIM"},
                        "PROP" : "LOG.PROP.1"
                    },
                }
            },
            time_1 : {
                "LOG_DATA" : {
                    "LOG_1" : {
                        "STATUS" : {"SAVE", "FZ", "DIM"},
                        "PROP" : "LOG.PROP.1"
                    },
                }
            },
        } 

        last_cached = None

        async def get_log_menu(self, timestamp = 0):

            nonlocal last_cached
            if timestamp == 0:
                assert last_cached
                return last_cached

            data = menu_data[timestamp]
            last_cached = data
            return data

        return get_log_menu

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu_gen())

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time_0)

    request = analyser.event_requests.pop()
    assert not request._log_ready["LOG_1"]

    await analyser.ingest_new_data(pub_data, time_1) # At time_1, the flag "RUN" will be not active
    assert request._log_ready["LOG_1"]


@pytest.mark.asyncio
async def test_event_cancelled(monkeypatch, event_loop):

    pub_data_0 = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }
    pub_data_1 = {
        "ST_UNLATCHED" : {},
    }

    event_time = time.time()

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data_0, event_time)

    request = analyser.event_requests.pop()
    await request.wait_for_log_ready("LOG_1", 1.0)

    # Not having "LOG_PLEASE" will reset the request
    await analyser.ingest_new_data(pub_data_1, event_time + 1.0)
    with pytest.raises(RequestAborted):
        await request.wait_for_log_ready("LOG_1", 1.0)


@pytest.mark.asyncio
async def test_fgc_error_log_cancelled(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())

    request = analyser.event_requests.pop()
    with pytest.raises(LogReadAborted):
        await request.check_log_read_error("LOG_1", 123, "random error")



@pytest.mark.asyncio
async def test_fgc_error_pm_in_progress(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())

    request = analyser.event_requests.pop()

    # Initially it will be ready
    await request.wait_for_log_ready("LOG_1", 0.1)

    # Finding 'pm in progress' will make it not ready (while not raising LogReadAborted)
    await request.check_log_read_error("LOG_1", 55, "pm in progress")
    with pytest.raises(asyncio.TimeoutError):
        await request.wait_for_log_ready("LOG_1", 0.1)


@pytest.mark.asyncio
async def test_logs_done_callback(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    async def get_log_menu(*args):
        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    dim_log_acknowledged = False

    async def ack_log_get(self, device, prop, get_option):

        nonlocal dim_log_acknowledged

        assert prop == "LOG.PROP.1"
        assert get_option == "SYNCHED"
        dim_log_acknowledged = True

    monkeypatch.setattr(DIMAnalyser, "ack_log_get", ack_log_get)

    analyser = DIMAnalyser("DEV_1", event_loop)
    await analyser.ingest_new_data(pub_data, time.time())
    request = analyser.event_requests.pop()

    await request.wait_for_log_ready("LOG_1", 0.1)
    await request.mark_log_read("LOG_1")
    await request.ack_request_done()
    assert dim_log_acknowledged


@pytest.mark.asyncio
async def test_log_menu_not_needed_after_logs_read(monkeypatch, event_loop):

    pub_data = {
        "ST_UNLATCHED" : {"LOG_PLEASE"},
    }

    event_time = time.time()
    log_menu_called = False

    async def get_log_menu(*args):
        nonlocal log_menu_called
        log_menu_called = True

        return {
            "LOG_DATA" : {
                "LOG_1" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.1"
                },
                "LOG_2" : {
                    "STATUS" : {"SAVE", "DIM"},
                    "PROP" : "LOG.PROP.2"
                },
            }
        }

    monkeypatch.setattr(DIMAnalyser, "get_log_menu_data", get_log_menu)

    analyser = DIMAnalyser("DEV_1", event_loop)
    
    # New event causes log menu to be read
    log_menu_called = False
    await analyser.ingest_new_data(pub_data, event_time)
    assert log_menu_called

    for request in analyser.event_requests:
        if "LOG_1" in request.logs:
            await request.mark_log_read("LOG_1")

    # LOG_2 not read yet, this causes log menu to be read
    log_menu_called = False
    await analyser.ingest_new_data(pub_data, event_time)
    assert log_menu_called

    for request in analyser.event_requests:
        if "LOG_2" in request.logs:
            await request.mark_log_read("LOG_2")

    # Both logs have been read, so log menu does not need to be read
    log_menu_called = False
    await analyser.ingest_new_data(pub_data, event_time)
    assert not log_menu_called



# EOF