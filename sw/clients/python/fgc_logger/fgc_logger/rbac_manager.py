"""Module for handling automatic refresh or RBAC tokens.
"""

import time
import asyncio
import logging
import concurrent
import concurrent.futures
from typing import Optional

import fgc_logger.settings as settings
import pyfgc_rbac

logger = logging.getLogger(__name__)

# Refresh after 90% of the token life has passed.
REFRESH_TRIGGER_MARGIN = 0.9
RETRY_TIME_S = 60


class Singleton(type):
    """Singleton class implementation.
    """
    instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.instances:
            cls.instances[cls] = super().__call__(*args, **kwargs)
        return cls.instances[cls]


class RbacManager(metaclass=Singleton):
    """RBAC token manager module for FGC logger.

    Handles fetching and refreshing of the RBAC token.
    Due to 'Singleton' parent class, only one instance of
    this class will exist.
    """
    def __init__(self):

        self.token = None
        self.auth_time = None
        self.expiration_time = None
        self.refresh_time = None
        self.lock = asyncio.Lock()
        self.auto_task = None

    @staticmethod
    async def _get_new_token() -> Optional[bytes]:
        """Get a new RBAC token.

        Returns:
            Optional[bytes]: RBAC token, encoded. If failed to obtain, return None.
        """
        with concurrent.futures.ThreadPoolExecutor() as pool:
            try:
                return await asyncio.get_event_loop().run_in_executor(
                    pool, pyfgc_rbac.get_token_location)
            except pyfgc_rbac.RbacServerError:
                logger.exception("RBAC server error.")
                return None

    async def get_token(self, new_token_required=False):
        """Get a the RBAC token.

        If necessary, refresh it.
        In case of failure to refresh, the old token is returned.
        """
        async with self.lock:

            # new_token_required = False

            if self.token is None:
                logger.info("Acquiring new RBAC token.")
                new_token_required = True

            elif self.expiration_time < time.time():
                logger.info("Current RBAC token has expired. Acquiring new token.")
                new_token_required = True

            elif self.refresh_time < time.time():
                expiring_in = self.expiration_time - time.time()
                logger.info(f"Current RBAC token is expiring soon, in {int(expiring_in)} s. Acquiring new token.")
                new_token_required = True

            if new_token_required:
                new_token = await self._get_new_token()

                if new_token is None:
                    logger.warning("Failed to get RBAC token.")

                else:
                    token_decoded = pyfgc_rbac.token_to_dict(new_token)
                    auth_time = token_decoded["AuthenticationTime"]
                    expire_time = token_decoded["ExpirationTime"]
                    refresh_in = (expire_time - auth_time) * REFRESH_TRIGGER_MARGIN
                    refresh_time = auth_time + refresh_in
                    self.auth_time = auth_time
                    self.token = new_token
                    self.expiration_time = expire_time
                    self.refresh_time = refresh_time
                    logger.info("New RBAC token obtained.")

            else:
                logger.debug("Current RBAC token is valid.")

            return self.token

    async def _run_auto_refresh(self):

        while True:

            prev_token = self.token
            prev_refresh_time = self.refresh_time
            curr_token = await self.get_token()

            if self.token is None:
                logger.warning(f"Retry to get RBAC token in {RETRY_TIME_S} s.")
                await asyncio.sleep(RETRY_TIME_S)
            if prev_token is curr_token and prev_refresh_time < time.time():
                logger.warning(f"Retry to refresh RBAC token in {RETRY_TIME_S} s.")
                await asyncio.sleep(RETRY_TIME_S)
            elif prev_token is not curr_token:
                next_refresh = self.refresh_time - time.time()
                logger.info(f"Next RBAC token refresh in {int(next_refresh)} s.")
                await asyncio.sleep(next_refresh)
            else:
                next_refresh = self.refresh_time - time.time()
                logger.info(f"Next RBAC token refresh in {int(next_refresh)} s.")
                await asyncio.sleep(next_refresh)

    async def start(self):
        """Start task fetching RBAC tokens.
        """
        if self.auto_task:
            raise RuntimeError("RBAC token auto-refreshing is already running.")
        self.auto_task = asyncio.ensure_future(self._run_auto_refresh())
        logger.info("RBAC token auto-refreshing enabled.")

    async def stop(self):
        """Stop task fetching RBAC tokens.
        """
        if not self.auto_task:
            raise RuntimeError("RBAC token manager not running.")

        self.auto_task.cancel()
        try:
            await self.auto_task
        except asyncio.CancelledError:
            logger.info("RBAC token auto-refreshing disabled.")
        finally:
            self.auto_task = None


async def get_token(refresh_token=False) -> Optional[bytes]:
    """Get cached RBAC token.

    If RBAC is not used (USE_RBAC==False), returns None.

    Returns:
        Optional[bytes]: RBAC token. If not necessary or not available, None
    """
    if settings.secret.USE_RBAC:
        rbac_manager = RbacManager()
        return await rbac_manager.get_token(new_token_required=refresh_token)
    else:
        return None



# EOF