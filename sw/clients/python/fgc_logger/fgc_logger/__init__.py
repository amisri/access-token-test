"""
FGC Logger __init__.

"""
import pkg_resources
__version__ = pkg_resources.require("fgc_logger")[0].version

from fgc_logger.fgc_logger_main import main



# EOF