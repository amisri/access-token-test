"""Log read request module.

Used to communicate a request between the event analyser and
a log fetcher.
"""
import enum
import asyncio
import logging
from typing import Coroutine, Mapping, Dict, Optional, Any

import fgc_logger.utils as utils

# Logger

module_logger = logging.getLogger(__name__)


class RequestAborted(Exception):
    """Request was aborted.
    """


class RequestNotCompleted(Exception):
    """Request not completed yet.
    Should acknowledge all log reads, event failed ones.
    """


class EventType(enum.Enum):
    """Event types
    """
    POST_MORTEM = enum.auto()
    SLOW_ABORT = enum.auto()
    DIM = enum.auto()
    VSREG = enum.auto()


class LogReadRequest:
    """Log read request.

    Instances of this class can be sent to the fetch manager.
    Used to request the reading of logs, after a certain event.

    Can be used to define which logs are ready to be read, vs those which require some time.
    """
    def __init__(self, device: str, event: EventType, timestamp: float, fgc_err_callback: Coroutine,
                 logs_done_callback: Coroutine, token: Optional[bytes]):
        """Init method.

        Args:
            device (str): Device name.
            event (EventType): Event type.
            timestamp (float): Event timestamp.
            fgc_err_callback (Coroutine): Courotine callback, to handle log reading errors.
            logs_done_callback (Coroutine): Courotine callback, to be run when all logs have been read.
            token (bytes): RBAC token. Can be None.
        """
        self.logger = utils.DeviceLogAdapter(module_logger, {"device": device})

        self._device = device
        self._event = event
        self._timestamp = timestamp
        self._request_aborted = asyncio.Event()
        self._fgc_err_callback = fgc_err_callback

        self._log_ready = dict()

        self._log_read_done = dict()
        self._logs_done_callback = logs_done_callback

        self._log_attributes = dict()
        self._log_ready_events = dict()

        self._token = token

    def __str__(self):
        return f"LogReadRequest({self._device}, {self._event.name}, {self._timestamp})"

    def add_log(self, log_name: str, attributes: Mapping,
                is_ready: bool = False):
        """Add a log to the request.

        This should be used by the event analyser, to define which logs must be
        obtained by the fetcher.

        Args:
            log_name (str): Log name.
            attributes (Mapping): Log attributes ('prop', 'get_option').
            is_ready (bool, optional): Log is ready to be read. Defaults to False.

        TODO: Replace 'attributes' field by literal arguments ('prop' and 'get_option').
        """
        self._log_ready[log_name] = is_ready
        self._log_read_done[log_name] = False

        self._log_attributes[log_name] = attributes
        self._log_ready_events[log_name] = asyncio.Event()

        self.set_log_ready(log_name, is_ready)

    def set_log_ready(self, log_name: str, is_ready: bool = True):
        """Set when log is ready to be read.

        Args:
            log_name (str): Log name.
            is_ready (bool, optional): Log is ready to be read. Defaults to True.
        """
        self._log_ready[log_name] = is_ready
        event = self._log_ready_events[log_name]
        if is_ready:
            event.set()
        else:
            event.clear()

    async def abort_request(self):
        """Abort current request to read logs.
        """
        self._request_aborted.set()
        for evt in self._log_ready_events.values():
            evt.set()

    @property
    def log_read_done(self) -> dict:
        """Check if log read has been acknowledged.

        Returns:
            dict: For each log, True if log has been read from FGC.
        """
        return self._log_read_done

    @property
    def device_name(self) -> str:
        """Get the name of the device associated with the request.

        Returns:
            str: Device name.
        """
        return self._device

    @property
    def token(self) -> bytes:
        """Get RBAC token.

        Returns:
            bytes: RBAC token, can be None.
        """
        return self._token

    @property
    def event_type(self) -> EventType:
        """Get the type of the event associated with the request.

        Returns:
            EventType: Type of the event.
        """
        return self._event

    @property
    def event_timestamp(self) -> float:
        """Get the time of the event.

        Returns:
            float: Time of the event. Format is unix time stamp.
        """
        return self._timestamp

    @property
    def logs(self) -> set:
        """Get the name of all requested logs.

        Returns:
            set: Set containing all the requested log names.
        """
        return set(self._log_ready.keys())

    def get_log_attributes(self, log_name: str) -> Dict[str, Any]:
        """Get log attributes.

        Args:
            log_name (str): Log name.

        Returns:
            Dict[str, Any]: Dictionary containing attribute values.

        TODO: Handle attribute values in dedicated methods, since only two are required:
            prop and get_option.
        """
        return self._log_attributes[log_name]

    async def wait_for_log_ready(self, log_name: str, timeout: Optional[float] = None):
        """Wait for log to be ready.

        Args:
            log_name (str): Log name.
            timeout (float, optional): Wait timeout. Defaults to None.

        Raises:
            RequestAborted: If request has been aborted.
            asyncio.TimeoutError: Wait timed out.
        """
        try:
            await asyncio.wait_for(self._log_ready_events[log_name].wait(), timeout)
        except asyncio.TimeoutError as err:
            raise asyncio.TimeoutError("Timed out while waiting for log ready.") from err

        # Check if it was aborted
        if self._request_aborted.is_set():
            raise RequestAborted("Request is no longer valid. Abort reading logs.")

    async def check_log_read_error(self, log_name: str, err_nr: int, err_msg: str):
        """This coroutine should be called by the log fetcher when a log read has failed.

        Will trigger the execution of the 'fgc_err_callback' coroutine set by the event analyser
        during the request initialization. This allows the event analyser to handle the log error,
        and take proper action (such as canceling the log read, or resqueduling it for later).

        Args:
            log_name (str): Log name.
            err_nr (int): Device response error number.
            err_msg (str): Device response error message.

        Raises:
            LogReadAborted: Raise when the log should be aborted, and not read again.
        """
        await self._fgc_err_callback(log_name, err_nr, err_msg)

    async def mark_log_read(self, log_name: str):
        """This coroutine should be called by the log fetcher when a log has been read.

        Args:
            log_name (str): Log name.
        """
        self._log_read_done[log_name] = True

    async def ack_request_done(self):
        """This coroutine should be called all logs have been read and stored in fortlogs.
        This required all logs to have been acknowledged with 'mark_log_read'.

        Raises:
            RequestNotCompleted: When some logs have not been acknowledged with 'mark_log_read'.
        """

        if not all(self._log_read_done.values()):
            missing_logs = {name for name, done in self._log_read_done.items() if not done}
            raise RequestNotCompleted(f"Did not acknowledge logs {missing_logs}.")

        await self._logs_done_callback()



# EOF