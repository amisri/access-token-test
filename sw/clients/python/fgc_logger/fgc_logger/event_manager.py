"""Log event manager module

This module receives the status data from the status receiver module
and manages its analysis, in search for log events.
It does it for every device, concurrently.

The following events are analysed, with this order:
    1) Slow-abort event
    2) Post-mortem event
    3) DIM event

When an event is confirmed, it will be locked for that device until all logs
have been recorded.
"""

import time
import logging
import asyncio
from typing import Iterable, Optional

import fgc_logger.utils as utils
from fgc_logger.event_analyser import EvtAnalyser, PostMortemAnalyser, SlowAbortAnalyser, DIMAnalyser
from fgc_logger.fetch_logs import FgcLogFetcher

# Define all the event analyser types here, in the order they should be evaluated.
# 1) Slow-abort  - 1st, since it checks for non-frozen logs
#                      that may be overriten in a short timespan.
# 2) Post-mortem - 2nd, since logs are frozen, so its safe to wait.
# 3) DIM         - 3rd, DIM log event checking is left for last,
#                      if they were not already read by post-mortem event.

EVT_ANALYSERS_PER_CLASS = {
    51: [PostMortemAnalyser, DIMAnalyser],
    53: [PostMortemAnalyser, DIMAnalyser],
    59: [PostMortemAnalyser, DIMAnalyser],
    62: [SlowAbortAnalyser, PostMortemAnalyser, DIMAnalyser],
    63: [SlowAbortAnalyser, PostMortemAnalyser, DIMAnalyser],
    # 91: [SlowAbortAnalyser, PostMortemAnalyser, DIMAnalyser],
    # 94: [SlowAbortAnalyser, PostMortemAnalyser, DIMAnalyser],

    # Defaults for class families
    50: [PostMortemAnalyser, DIMAnalyser],
    60: [SlowAbortAnalyser, PostMortemAnalyser, DIMAnalyser],
    90: [SlowAbortAnalyser, PostMortemAnalyser, DIMAnalyser],
}

# Timeout values

CLEANUP_TIMEOUT_S = 120.0  # Cleanup analysers if new gateway data hasn't arrived for 2 minutes.
ANALYSER_TIMEOUT_S = 60.0  # Stop analyser if it cannot evaluate a new event in 20 seconds.
CLOSE_ANALYSER_TIMEOUT_S = 5.0  # Time to wait while closing event analyser.
REQUEST_WARN_PERIOD = 120.0  # Warn if this time has passed without the request being completed.

# Logger definition

logger = logging.getLogger(__name__)


class FgcDataAnalyser:
    """Agregator of all event analysers, for a single device.
    """

    def __init__(self, device: str, class_id: int, loop: asyncio.AbstractEventLoop):
        """Init method.

        Args:
            device (str): Device name.
            class_id (int): Device class number.
            loop (asyncio.AbstractEventLoop): Running event loop.
        """

        self.logger = utils.DeviceLogAdapter(logger, {"device": device})
        self.loop = loop
        self.device = device
        self.class_id = class_id

        # NOTE: Analysers order is important. Higher priority events should come first.
        try:
            analyser_types = EVT_ANALYSERS_PER_CLASS[class_id]
        except KeyError:
            class_family = class_id - (class_id % 10)
            analyser_types = EVT_ANALYSERS_PER_CLASS[class_family]
            self.logger.warning("Did not find analysers for class %s. "
                                "Using default analysers for class family %s.",
                                class_id, class_family)

        self.analysers = [analyser_t(device, loop) for analyser_t in analyser_types]

        self.request_fetchers = dict()
        self.wait_task = None
        self.active_analyser = None
        self.last_data = None
        self.last_data_time = None

    async def _update_analyser(self) -> Optional[EvtAnalyser]:
        """Update currently active event analyser.
        """
        analyser = self.active_analyser
        pub_data = self.last_data
        timestamp = self.last_data_time

        if analyser.event_active:
            self.logger.debug("%s: Sending updated status data to analyser.", analyser)
            await analyser.ingest_new_data(pub_data, timestamp)

        if not analyser.event_active:
            self.logger.debug("%s: No longer active.", analyser)
            return None

        return analyser

    async def _check_new_analyser(self) -> Optional[tuple]:
        """Checks if all event analysers, by order,
        to find if one makes a request.
        """
        pub_data = self.last_data
        timestamp = self.last_data_time

        for analyser in self.analysers:

            # Check previous event time
            prev_evt_timestamp = analyser.event_timestamp

            # New data
            await analyser.ingest_new_data(pub_data, timestamp)

            # Check if new event occured
            if analyser.event_active and analyser.event_timestamp > prev_evt_timestamp:
                self.logger.info("%s: Detected new event.", analyser)
                return analyser, analyser.event_requests

        return None

    async def _wait_for_log_fetching(self):
        """Wait for log fetching task, when it is active.
        """
        pending = True
        while pending:
            done, pending = await asyncio.wait(self.request_fetchers.keys(),
                                               timeout=REQUEST_WARN_PERIOD)
            for task in pending:
                self.logger.warning("Log read request %s running for too long.",
                                    self.request_fetchers[task])

        for task in done:
            if task.cancelled():
                self.logger.warning("Log read request %s was cancelled.",
                                    self.request_fetchers[task])
            else:
                try:
                    task.result()
                    self.logger.info("Finished task serving request %s.", self.request_fetchers[task])
                except Exception:
                    self.logger.exception("Log read request failed unexpectedly.")

    async def ingest_data(self, pub_data: dict, timestamp: float) -> bool:
        """Ingest new published data.
        FgcDataAnalyser will handle it properly, and trigger the reading of
        the logs when necessary.

        Args:
            pub_data (dict): Device published data.
            timestamp (float): Device published data timestamp.

        Returns:
            bool: True if logs are being analysed.
        """
        self.last_data = pub_data
        self.last_data_time = timestamp

        # First, if there is an active analyser it should be updated.
        if self.active_analyser:
            self.active_analyser = await self._update_analyser()

            if not self.active_analyser:
                try:
                    await asyncio.wait_for(self.wait_task, CLOSE_ANALYSER_TIMEOUT_S)
                except asyncio.TimeoutError:
                    self.logger.warning(
                        "Log fetching task not terminating by itself... "
                        "Terminate by force.")
                    await self.reset()
                self._clean_fields()

        else:
            new_event_data = await self._check_new_analyser()

            if new_event_data:
                self.active_analyser, new_requests = new_event_data

                for request in new_requests:
                    new_log_fetcher = FgcLogFetcher(request)
                    log_fetcher_fut = asyncio.ensure_future(new_log_fetcher.execute())
                    self.request_fetchers[log_fetcher_fut] = request
                    self.logger.info("New task serving request %s.", request)

                self.wait_task = asyncio.ensure_future(self._wait_for_log_fetching())

    @property
    def evt_active(self) -> bool:
        """Check if log event is ongoing.
        """
        return self.active_analyser is not None

    async def reset(self):
        """Reset the object.
        """
        if self.request_fetchers:
            self.logger.warning("Cancelling log reading tasks.")
            for fut in self.request_fetchers:
                fut.cancel()
            try:
                await asyncio.wait_for(self.wait_task, CLOSE_ANALYSER_TIMEOUT_S)
            except asyncio.CancelledError:
                pass
            except asyncio.TimeoutError:
                self.logger.warning("Timeout while waiting for log reading tasks to complete.")
        self._clean_fields()

    def _clean_fields(self):
        self.request_fetchers = dict()
        self.wait_task = None
        self.active_analyser = None
        self.last_data = None
        self.last_data_time = None


class EventManager:
    """Event manager module.
    """
    def __init__(self, loop: asyncio.AbstractEventLoop, input_queue: asyncio.Queue,
                 filter_devices: Iterable[str] = None):
        """Init method.

        Args:
            loop (asyncio.AbstractEventLoop): Running event loop.
            input_queue (asyncio.Queue): Input queue for incoming decoded status data.
            filter_devices (Iterable[str], optional): Read only these devices. Defaults to None.
        """
        logger.debug("Setting up event manager.")

        self.loop = loop
        self.input_queue = input_queue

        self.running = False
        self.last_gw_data_time = dict()
        self.last_gw_data = dict()
        self.last_recv_time = dict()
        self.gw_analysers = dict()
        self.gw_active = dict()

        self.filter_devices = {dev.upper() for dev in filter_devices} if filter_devices else None

        self.active_analysis = dict()

    def _device_class_supported(self, class_id: int) -> bool:
        """Check if device class should be logged.

        Args:
            class_id (int): Device class number.

        Returns:
            bool: True if class should be logged.
        """
        class_family = class_id - (class_id % 10)
        if class_id == class_family:
            return False  # Do not support boot classes

        return class_id in EVT_ANALYSERS_PER_CLASS or class_family in EVT_ANALYSERS_PER_CLASS

    async def run(self):
        """Run event manager.
        """
        try:
            logger.info("Running event manager task.")
            await self._recv_loop()
        except Exception:
            self.running = False
            logger.exception("Event manager task failed unexpectedly.")
            raise

    async def stop(self):
        """Stop running event manager.
        """
        logger.info("Stopping event manager task.")
        self.running = False
        await self.input_queue.put(None)  # Poison pill

    async def _cleanup_old(self):
        """Clean up device handling objects, after a period of time without new data.

        Checks if CLEANUP_TIMEOUT_S has passed since the last data was received.
        If so, remove device analysers.
        """
        # Abort analysers starved of data for the last CLEANUP_TIMEOUT_S seconds
        current_time = time.time()
        for gateway, last_time in self.last_recv_time.items():
            if self.gw_active[gateway] and last_time + CLEANUP_TIMEOUT_S < current_time:
                logger.warning("Gateway %s status data timed out (%f s). "
                               "Last data obtained on %f s. Aborting current requests.",
                               gateway, CLEANUP_TIMEOUT_S, last_time)
                for dev_analyser in self.gw_analysers[gateway].values():
                    await dev_analyser.reset()
                self.gw_active[gateway] = False

    async def _cleanup_all(self):
        """Clean up all devices handling objects.
        """
        logger.debug("Cleaning up all requests.")
        for gw_devs_analysers in self.gw_analysers.values():
            for dev_analyser in gw_devs_analysers.values():
                await dev_analyser.reset()

        self.last_gw_data_time.clear()
        self.last_gw_data.clear()
        self.gw_analysers.clear()
        self.last_recv_time.clear()
        self.gw_active.clear()

    async def _wait_for_analysers(self, new_analysis_evt: asyncio.Event):
        """Wait for eventa analyser tasks.
        """
        while self.running:

            analysing_set = set()
            tasks = {task: dev for dev, (_, task) in self.active_analysis.items()}
            analysing_set.update(tasks.keys())
            evt_wait = asyncio.ensure_future(new_analysis_evt.wait())
            analysing_set.add(evt_wait)

            if self.active_analysis:
                next_abs_timeout = min(
                    {entry_time + ANALYSER_TIMEOUT_S for entry_time, _ in self.active_analysis.values()})
                timeout = max(next_abs_timeout - time.time(), 0.02)
            else:
                timeout = 1.0

            done, pending = await asyncio.wait(
                analysing_set, timeout=timeout, return_when=asyncio.FIRST_COMPLETED)

            for task in done:
                if task is evt_wait:
                    new_analysis_evt.clear()
                else:
                    device = tasks[task]
                    try:
                        task.result()
                    except Exception:
                        logger.exception(
                            "Task analysing %s published data failed unexpectedly.", device)
                    del self.active_analysis[device]

            # Check timeouts
            current_time = time.time()
            for task in pending:
                if task is not evt_wait:
                    device = tasks[task]
                    entry_time, _ = self.active_analysis[device]
                    if current_time > (entry_time + ANALYSER_TIMEOUT_S):
                        logger.error("Task to update analysers of %s timedout (%s s).",
                                     device, ANALYSER_TIMEOUT_S)
                        task.cancel()
                        del self.active_analysis[device]

        # Terminate safely
        evt_wait.cancel()
        await evt_wait

        remaining_tasks = {task for _, task in self.active_analysis.values()}
        if remaining_tasks:
            for task in remaining_tasks:
                task.cancel()
            await asyncio.wait(remaining_tasks, timeout=CLOSE_ANALYSER_TIMEOUT_S)

        self.active_analysis.clear()

    async def _recv_loop(self):
        """Analyse status data, in search for new log events.
        """
        logger.info("Event manager ready to receive new status data.")
        new_analysis_evt = asyncio.Event()
        self.running = True

        analyser_wait_task = asyncio.ensure_future(self._wait_for_analysers(new_analysis_evt))

        while self.running:

            # Cleanup analysers starved of data
            await self._cleanup_old()

            # Wait for new data, on input queue
            gw_data = await self.input_queue.get()

            # If poison pill was received, check self.running again
            if gw_data is None:
                continue

            timestamp = gw_data["time"]
            gateway = gw_data["gateway"]

            # Check if timestamp is valid, else ignore
            if not timestamp:
                continue

            # Check if packet contains new data, else ignore
            try:
                if timestamp <= self.last_gw_data_time[gateway]:
                    continue
            except KeyError:
                pass

            # New data is valid
            # Update object with received data
            self.last_gw_data[gateway] = gw_data
            self.last_gw_data_time[gateway] = timestamp
            self.last_recv_time[gateway] = time.time()
            self.gw_active[gateway] = True

            try:
                self.gw_analysers[gateway]
            except KeyError:
                logger.debug("Identified new gateway %s.", gateway)
                self.gw_analysers[gateway] = dict()

            for device, dev_data in gw_data["devices"].items():

                # Check if device must be filtered out
                if self.filter_devices and device not in self.filter_devices:
                    continue

                # Check if device class should be monitored
                if not self._device_class_supported(dev_data["CLASS_ID"]):
                    continue

                # Check if device is offline or its class is valid
                if ("DATA_VALID" not in dev_data["DATA_STATUS"] or
                        "CLASS_VALID" not in dev_data["DATA_STATUS"]):
                    continue

                # Create device handlers if they do not exist
                try:
                    dev_analyser = self.gw_analysers[gateway][device]
                except KeyError:
                    logger.debug("Identified new device %s. Creating event analyser.", device)
                    dev_analyser = FgcDataAnalyser(device, dev_data["CLASS_ID"], self.loop)
                    self.gw_analysers[gateway][device] = dev_analyser
                else:
                    # If device class was modified, a new analyser needs to be created.
                    if dev_analyser.class_id != dev_data["CLASS_ID"]:
                        logger.warning("Device %s class changed from %s to %s.",
                                       device, dev_analyser.class_id, dev_data["CLASS_ID"])
                        await dev_analyser.reset()
                        dev_analyser = FgcDataAnalyser(device, dev_data["CLASS_ID"], self.loop)
                        self.gw_analysers[gateway][device] = dev_analyser

                # Analyse device, if not done already
                if device not in self.active_analysis:
                    dev_analyser.logger.debug("Launching task to process pub data.")
                    task = asyncio.ensure_future(
                        dev_analyser.ingest_data(dev_data, timestamp))
                    self.active_analysis[device] = (time.time(), task)
                    new_analysis_evt.set()
                else:
                    dev_analyser.logger.warning(
                        "Previous published data is still being processed. "
                        "Ignoring newly received data.")

        new_analysis_evt.set()
        try:
            await analyser_wait_task
        except asyncio.CancelledError:
            pass
        await self._cleanup_all()



# EOF