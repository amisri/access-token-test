"""FGC status receiver module

This module is used to get the status data published by the FGCs.
Two modes of operation are provided:

    1) Polling status data from status server.

       This makes use of the pyfgc_statusrv module.
       It requires a status server to exist.

    2) Receiving status data on open UDP port.

       This makes use of the pyfgc monitor module.
       It requires all gateways to forward the status data
       to this UDP port.
"""
import asyncio
import logging
import time
import concurrent
import concurrent.futures
import functools
import queue
from typing import Iterable, Mapping

import pyfgc
import pyfgc_statussrv

import fgc_logger.rbac_manager
import fgc_logger.settings as settings


logger = logging.getLogger(__name__)


class StatusReceiver:
    """Status receiver interface.
    """
    async def run(self):
        """Start running status receiver.
        """
        raise NotImplementedError()

    async def stop(self):
        """Stop running status receiver.
        """
        raise NotImplementedError()


class StatusSrvReader(StatusReceiver):
    """Read status data from status server.

    This class connects to a status server, and fetches new status data periodically.
    The new data will be forwarded to an asyncio queue.
    """
    def __init__(self, async_queue: asyncio.Queue, loop: asyncio.AbstractEventLoop,
                 period: float = None, filter_gateways: Iterable[str] = None):
        """Init method.

        Args:
            async_queue (asyncio.Queue): Queue to fill with new status data.
            loop (asyncio.AbstractEventLoop): Current event loop.
            period (float, optional): Status data reading period. Defaults to None.
            filter_gateways (Iterable[str], optional): Read only these gateways. Defaults to None.
        """
        logger.debug("Setting up status server reader.")

        self.async_queue = async_queue
        self.loop = loop
        self.period = period if period is not None else settings.basic.STATUS_SERVER_PERIOD
        self.thread_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.running = False
        self.running_fut = None
        self.filter_gateways = {gw.lower() for gw in filter_gateways} if filter_gateways else None

    async def _add_to_queue(self, status_data):
        """Extract and add gateway data to queue.
        """
        for gateway, status in status_data.items():

            gw_status = dict()
            gw_status["gateway"] = gateway
            gw_status["hostname"] = status["hostname"]
            gw_status["time"] = status["time_sec"] + (status["time_usec"] / 1000000.0)
            gw_status["devices"] = status["devices"]

            if self.filter_gateways is None or gateway in self.filter_gateways:
                await self.async_queue.put(gw_status)

    async def _get_new_session(self):
        """Create new pyfgc session with the status server.

        Sync session needs to be created using a thread pool executor,
        to avoid blocking the asyncio event loop.
        """
        logger.info("Opening new connection to status server %s.",
                    settings.basic.STATUS_SERVER_NAME)

        token = await fgc_logger.rbac_manager.get_token()
        return await self.loop.run_in_executor(
            self.thread_executor,
            functools.partial(
                pyfgc.connect,
                settings.basic.STATUS_SERVER_NAME,
                rbac_token=token,
                name_file=settings.basic.NAME_FILE)
        )

    async def _close_session(self, session):
        """Close pyfgc session with the status server.

        Sync session needs to be destroyed using a thread pool executor,
        to avoid blocking the asyncio event loop.
        """
        logger.info("Closing connection to status server %s.", settings.basic.STATUS_SERVER_NAME)
        await self.loop.run_in_executor(self.thread_executor,
                                        pyfgc.disconnect, session)

    async def _get_status_data(self, session):
        """Get latest data from status server.

        Request needs to use a thread pool executor,
        to avoid blocking the asyncio event loop.
        """
        return await self.loop.run_in_executor(
            self.thread_executor,
            functools.partial(
                pyfgc_statussrv.get_status_all,
                fgc_session=session
            )
        )

    async def _status_srv_polling(self):
        """Poll the status server, periodically, for new data.
        """
        session = await self._get_new_session()
        prev_time = time.time()
        self.running = True

        logger.info("Polling status server %s at %f Hz.",
                    settings.basic.STATUS_SERVER_NAME, (1.0/settings.basic.STATUS_SERVER_PERIOD))

        while self.running:
            next_time = prev_time + self.period
            wait_time = next_time - time.time()

            # Wait for next loop
            if wait_time > 0:
                await asyncio.sleep(wait_time)

            try:
                logger.debug("Polling latest status data from status server.")
                status_data = await self._get_status_data(session)
            except (OSError, RuntimeError, pyfgc.FgcResponseError):
                logger.error(
                    "Failed to get status data from status server. Will try to reconnect.",
                    exc_info=(logger.getEffectiveLevel() == logging.DEBUG))
                await self._close_session(session)
                session = await self._get_new_session()
                continue

            await self._add_to_queue(status_data)
            prev_time = next_time

        await self._close_session(session)

    async def run(self):
        """Run status receiver.
        """
        try:
            logger.info("Running status server reader task.")
            await self._status_srv_polling()
        except Exception:
            self.running = False
            logger.exception("Status server reader task failed unexpectadly.")
            raise

    async def stop(self):
        """Stop running status receiver.
        """
        logger.info("Stopping status server reader task.")
        self.running = False


class StatusMonitorPort(StatusReceiver):
    """Receive status data on UDP port.

    This class opens a UDP port and waits for status data sent by the gateways.
    The new data will be forwarded to an asyncio queue.
    """
    def __init__(self, async_queue: asyncio.Queue, loop: asyncio.AbstractEventLoop,
                 port: int, filter_gateways: Iterable[str] = None):
        """Init method.

        Args:
            async_queue (asyncio.Queue): Queue to fill with new status data.
            loop (asyncio.AbstractEventLoop): Current event loop.
            port (int): Status data receiving UDP port.
            filter_gateways (Iterable[str], optional): Read only these gateways. Defaults to None.
        """
        self.async_queue = async_queue
        self.loop = loop
        self.port = port
        self.thread_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.sync_queue = queue.Queue()
        self.monitor = None
        self.running = False
        self.running_fut = None
        self.filter_gateways = {gw.lower() for gw in filter_gateways} if filter_gateways else None

    def monitor_callback(self, data_dict: Mapping[str, dict], _data_list: Iterable[dict],
                         gateway: str, timestamp: float):
        """Callback for new status data.

        When new UDP published data is received from a gateway.
        Since the callback is a function, not a coroutine, this will send the data to a sync queue.

        Args:
            data_dict (Mapping[str, dict]): Received status data in dictionary format.
            _data_list (Iterable[dict]): Received status data in list format [0..64]. Ignored.
            gateway (str): Gateway name.
            timestamp (float): Received data timestamp.
        """
        gw_status = dict()
        gw_status["gateway"] = gateway
        gw_status["hostname"] = gateway
        gw_status["time"] = timestamp
        gw_status["devices"] = data_dict

        if self.filter_gateways is None or gateway in self.filter_gateways:
            self.sync_queue.put(gw_status)

    def monitor_callback_err(self, err: Exception, gateway: str, timestamp: float):
        """Callback for monitor errors.

        When the monitor callback catches an exception.
        This allows the user log those exceptions.

        Args:
            err (Exception): Exception raised by the monitor.
            gateway (str): Gateway name.
            timestamp (float): Exception timestamp.
        """
        logger.error("Monitor error caused by %s at %f s: %s", gateway, timestamp, err)

    async def _wait_for_status_data(self):
        """Get new data from the sync queue.

        This allows the data to be safely passed from the callback to the asyncio event loop.
        """
        return await self.loop.run_in_executor(self.thread_executor, self.sync_queue.get)

    async def _pub_data_polling(self):
        """Loop while waiting for new data.
        """
        self.running = True
        self.monitor = pyfgc.monitor_port(self.monitor_callback, self.port,
                                          callback_err=self.monitor_callback_err,
                                          name_file=settings.basic.NAME_FILE)
        self.monitor.start()

        logger.info("Waiting for status data on port %d.", self.port)

        while self.running:
            status_data = await self._wait_for_status_data()
            if status_data:
                logger.debug("Received new status data for %s", status_data['gateway'])
                await self.async_queue.put(status_data)

    async def run(self):
        """Start running status receiver.
        """
        try:
            logger.info("Running status monitor port task.")
            await self._pub_data_polling()
        except Exception:
            self.running = False
            logger.exception("Status monitor port task failed unexpectadly.")
            raise

    async def stop(self):
        """Stop running status receiver.
        """
        logger.info("Stopping status monitor port task.")
        self.running = False
        self.monitor.stop()
        self.sync_queue.put(None)  # To unlock queue



# EOF