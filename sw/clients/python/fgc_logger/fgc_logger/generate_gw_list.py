"""
Script for generating list of gateways from groups.
"""

import os

# Group file path
GROUP_FILE_PATH = "/user/pclhc/etc/fgcd/group"

# New file name
NEW_FILE_NAME = "list_gateways_new.txt"


# Command to clear terminal
def cls(): os.system('cls' if os.name == 'nt' else 'clear')


# Read groups
with open(GROUP_FILE_PATH) as group_file:
    gateways = group_file.readlines()

groups = dict()

for gateway_info in gateways:

    info = gateway_info.replace("/", ":").split(":")
    gw_name = info[0].strip()
    gw_group = info[1].strip()

    try:
        groups[gw_group].add(gw_name)
    except KeyError:
        groups[gw_group] = set()
        groups[gw_group].add(gw_name)

selections = [
    {'group': group, 'selected': False} for group in sorted(groups.keys())
]

# Allow user to select groups
generate_now = False
while not generate_now:

    cls()
    for index, data in enumerate(selections):
        print(
            f"[{'X' if data['selected'] else ' '}] {index:>2}: {data['group']}"
        )

    print()
    print("Insert command and press ENTER:")
    print("- To select, write group numbers (comma-separated).")
    print("- To reset, write 'r|reset'.")
    print("- To quit, write 'q|quit'.")
    print("- To generate file, write 'g|generate'.")

    command = input("> ")
    if command.strip().lower() in ("g", "generate"):
        generate_now = True
    elif command.strip().lower() in ("r", "reset"):
        for entry in selections:
            entry["selected"] = False
    elif command.strip().lower() in ("q", "quit"):
        exit(0)
    else:
        try:
            numbers = {int(num) for num in command.split(",")}
        except ValueError:
            print("Error: Invalid input.")
            exit(1)
        for num in numbers:
            selections[num]["selected"] = True

# Finally, generate

with open(NEW_FILE_NAME, "w") as new_file:

    for entry in selections:
        if entry["selected"]:
            for gw in sorted(groups[entry["group"]]):
                new_file.write(gw + "\n")

print(f"Generated new '{NEW_FILE_NAME}'.")



# EOF