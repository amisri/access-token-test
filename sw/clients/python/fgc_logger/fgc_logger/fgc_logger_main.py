#!/usr/bin/python3

"""FGC Logger.

Usage:
    fgc_logger <config_file>
    fgc_logger [-v] [-p <prof_dir>] <config_file>
    fgc_logger -g <file_dir> | --generate=<file_dir>
    fgc_logger -h | --help

Options:
    -h,--help                            Show this help.
    -v,--verbosity                       Increase output verbosity [default: INFO].
    -p <prof_dir>,--profile=<prof_dir>   Run profiling and output to directory.
    -g <file_dir>,--generate=<file_dir>  Generate configuration file template on this directory.       
"""

import signal
import os
import asyncio
import shutil
import pathlib
import syslog
import logging
import logging.handlers
import sentry_sdk

import docopt
import colorlog

import yappi

import fgc_logger.utils as utils
import fgc_logger.settings as settings
from fgc_logger.fgc_logger_server import FgcLoggerServer

ROOT_NAME = __name__.split(".")[0] #fgc_logger
logger = logging.getLogger(__name__)

# Maximum processes on process pool
MAX_DECODE_PROCS = 4

# Config file path
CONFIG_EXAMPLE_FILE = "config_example.cfg"
CONFIG_EXAMPLE_PATH = pathlib.Path(os.path.dirname(__file__)) / "templates" / CONFIG_EXAMPLE_FILE
CONFIG_TEMPLATE_FILE = "config_example.cfg.template"
CONFIG_TEMPLATE_PATH = pathlib.Path(os.path.dirname(__file__)) / "templates" / CONFIG_TEMPLATE_FILE

# Format for all project message logs
FILELOG_FORMAT = "[%(asctime)s] %(levelname)s (%(module)s) %(message)s"
TERMLOG_FORMAT = "[%(asctime)s] %(log_color)s%(levelname)s%(reset)s %(blue)s(%(module)s)%(reset)s %(message)s"
SYSLOG_FORMAT = "%(levelname)s (%(module)s) %(message)s"

def configure_logging(verbosity: bool):
    """Configure python message logging.

    Logging messages are printed both to the terminal and to a rotating file.

    Args:
        verbosity (bool): Verbose logging enabled (file and terminal only).
    """
    default_severity = logging.DEBUG if verbosity else logging.INFO

    root_logger = logging.getLogger(ROOT_NAME)
    root_logger.setLevel(default_severity)

    logging_handlers = settings.logging.LOGGING_HANDLERS

    # Setup syslog

    class NewSysLogHandler(logging.Handler):

        DEFAULT_PRIO = [
            logging.CRITICAL,
            logging.ERROR,
            logging.WARNING,
            logging.INFO,
            logging.DEBUG,
        ]

        PRIO_TABLE = {
            logging.CRITICAL : syslog.LOG_CRIT,
            logging.ERROR    : syslog.LOG_ERR,
            logging.WARNING  : syslog.LOG_WARNING,
            logging.INFO     : syslog.LOG_INFO,
            logging.DEBUG    : syslog.LOG_DEBUG,
        }

        def _get_syslog_level(self, levelno):

            try:
                return self.PRIO_TABLE[levelno]
            except KeyError:
                for ref_priority in self.DEFAULT_PRIO:
                    if levelno >= ref_priority:
                        # Cache new logging level and return
                        self.PRIO_TABLE[levelno] = self.PRIO_TABLE[ref_priority]
                        return self.PRIO_TABLE[levelno]
            raise ValueError(f"Logging levelno invalid {levelno}.")

        def emit(self, record):
            log_entry = self.format(record)
            log_level = self._get_syslog_level(record.levelno)
            syslog.syslog(log_level, log_entry)

    if logging_handlers & settings.LoggingHandlers.SYSLOG:
        syslog.openlog(ident=ROOT_NAME, logoption=syslog.LOG_PID)
        sh = NewSysLogHandler()
        sh.setLevel(settings.logging.LOGGING_LEVEL_SYSLOG)
        sh_formatter = logging.Formatter(SYSLOG_FORMAT)
        sh.setFormatter(sh_formatter)
        root_logger.addHandler(sh)

    # Setup rotating file for logs.

    if logging_handlers & settings.LoggingHandlers.FILE:
        fh = logging.handlers.RotatingFileHandler(settings.logging.LOGGING_FILE_PATH,
                                                  maxBytes=settings.logging.LOGGING_FILE_SIZE,
                                                  backupCount=settings.logging.LOGGING_FILE_BACKUP_COUNT)
        fh.setLevel(logging.DEBUG if verbosity else settings.logging.LOGGING_LEVEL_FILE)
        fh_formatter = logging.Formatter(FILELOG_FORMAT)
        fh.setFormatter(fh_formatter)
        root_logger.addHandler(fh)

    # Setup colored logs to print to the terminal.

    if logging_handlers & settings.LoggingHandlers.TERMINAL:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG if verbosity else settings.logging.LOGGING_LEVEL_TERMINAL)
        ch_formatter = colorlog.ColoredFormatter(TERMLOG_FORMAT)
        ch.setFormatter(ch_formatter)
        root_logger.addHandler(ch)


def copy_conf_template(log_file_dir: str):
    """Copy template of configuration file.
    """
    received_path = pathlib.Path(log_file_dir).resolve()
    if not received_path.is_dir():
        print(f"ERROR: Argument is not a directory: {received_path}")
        return

    new_file_path = received_path / CONFIG_EXAMPLE_FILE
    if new_file_path.is_file():
        print(f"ERROR: File already exists: {new_file_path}")
        return

    try:
        shutil.copy(CONFIG_EXAMPLE_PATH, new_file_path)
    except FileNotFoundError:
        print("WARNING: Unable to find auto-generated config file. Returning template instead.")
        shutil.copy(CONFIG_TEMPLATE_PATH, new_file_path)

    print(f"SUCCESS: File created: {new_file_path}")

def main():
    """Main function executing FGC logger.
    """
    args = docopt.docopt(__doc__)

    # Check if template file should be generated
    generate_template = args["--generate"]
    if generate_template:
        copy_conf_template(generate_template)
        exit(0)

    # Check if profile file should be printed
    profile_dir = args["--profile"]
    if profile_dir:
        profile_path = pathlib.Path(profile_dir).resolve()
        if not profile_path.is_dir():
            print(f"ERROR: Argument is not a directory: {profile_path}")
            exit(1)
        do_profile = True
    else:
        do_profile = False

    # Else, read config file and execute FGC logger
    config_file = args["<config_file>"]
    if not pathlib.Path(config_file).is_file():
        print(f"ERROR: File does not exist: {config_file}")
        exit(1)

    settings.update_settings(args["<config_file>"])
    configure_logging(args["--verbosity"])

    if settings.basic.SENTRY_URL:
        sentry_sdk.init(settings.basic.SENTRY_URL, environment=settings.basic.SENTRY_ENV)

    logger.info("---- FGC Logger main program ----")
    logger.debug("Debug message logging enabled.") # Will only run in debug.

    main_pid = os.getpid()
    running = False

    # Setup process pool, to be used to decode the data without blocking the current process

    utils.setup_process_pool_exec(max_workers=MAX_DECODE_PROCS)

    # Signal handler for ordered termination

    def signal_handler(_sig, _frame):
        nonlocal running
        if main_pid == os.getpid() and running:
            logger.warning("Received signal to close server.")
            server.stop()
            running = False

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # Start FGC Logger server object

    running = True
    server = FgcLoggerServer()
    loop = asyncio.get_event_loop()

    if do_profile:
        # Profiling executed with yappi. Performante may be affected.
        logger.warning("Running profiling. Performance may be affected.")
        profile_file = str(profile_path / 'fgc_logger_profile.prof')
        with yappi.run():
            loop.run_until_complete(server.run())
        yappi.get_func_stats().save(profile_file, type='pstat')
        logger.warning("Profiling output file (pstat) saved as %s.", profile_file)
    else:
        loop.run_until_complete(server.run())

    # Cleanup shared pool

    utils.clear_process_pool_exec()
    logger.info("---- FGC Logger closed ----")

if __name__ == "__main__":
    main()



# EOF