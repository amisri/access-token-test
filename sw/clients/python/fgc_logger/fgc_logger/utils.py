"""Utilities for the fgc logger.
"""
import logging
import concurrent
import concurrent.futures


class DeviceLogAdapter(logging.LoggerAdapter):
    """Will prepend the device name to the log message.
    This gives a very usefull field to message logging.
    """
    def process(self, msg, kwargs):
        device = self.extra["device"]
        return f"[{device}] {msg}", kwargs


global_proc_pool_exec = None

# TODO: In python 3.7, use ProcessPoolExecutor 'initializer' argument to setup posix signal handling.


def setup_process_pool_exec(**kwargs):
    """Setup the process pool executor.

    Raises:
        RuntimeError: If global process pool already exists.
    """
    global global_proc_pool_exec
    if global_proc_pool_exec:
        raise RuntimeError("ProcessPoolExecutor already inicialized.")
    global_proc_pool_exec = concurrent.futures.ProcessPoolExecutor(**kwargs)


def get_process_pool_exec() -> concurrent.futures.ProcessPoolExecutor:
    """Get process pool executor.

    Raises:
        RuntimeError: If process pool executor has not been initialized.

    Returns:
        concurrent.futures.ProcessPoolExecutor: Global process pool executor.
    """
    if not global_proc_pool_exec:
        raise RuntimeError("ProcessPoolExecutor not initialized.")
    return global_proc_pool_exec


def clear_process_pool_exec(wait: bool = True):
    """Clear process pool executor.

    Args:
        wait (bool, optional): Wait for process pool to shutdown. Defaults to True.

    Raises:
        RuntimeError: If process pool executor has not been initialized.
    """
    global global_proc_pool_exec
    if not global_proc_pool_exec:
        raise RuntimeError("ProcessPoolExecutor not initialized.")
    global_proc_pool_exec.shutdown(wait=wait)
    global_proc_pool_exec = None



# EOF