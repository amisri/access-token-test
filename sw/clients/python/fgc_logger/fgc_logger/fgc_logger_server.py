"""FGC Logger server

This module handles the starting/stopping of the FGC Logger.
"""
import logging
import asyncio
import pyfgc_name

from fgc_logger.status_receiver import StatusSrvReader, StatusMonitorPort
from fgc_logger.event_manager import EventManager
import fgc_logger.settings as settings
import fgc_logger.rbac_manager

logger = logging.getLogger(__name__)

VALID_MODES = {"status_server", "status_monitor"}


class FgcLoggerServer:
    """FGC Logger server module.

    Handles the starting/stopping of the FGC Logger.
    Two modes of execution are supported:

        1) Running in the current asyncio event loop.
        2) Launching in a separate thread.

    The asyncio mode is preferred, to reduce thread overhead.
    """
    def __init__(self):
        """Init method.
        """
        self.loop = None
        self.async_mode = False

        self.term_signal = None

        self.filter_groups = settings.basic.FILTER_GROUPS
        self.filter_gateways = settings.basic.FILTER_GATEWAYS
        self.filter_devices = settings.basic.FILTER_DEVICES

        self.recv_mode = settings.basic.STATUS_READ_MODE
        self.period = settings.basic.STATUS_SERVER_PERIOD
        self.port = settings.basic.STATUS_MONITOR_PORT

    @staticmethod
    def _get_gateways_from_groups(group_list: list):
        """Given a list of groups, get the corresponding gateways.

        Args:
            group_list (list): List of groups.

        Return:
            dict: Gateways per group.
        """
        pyfgc_name.read_name_file()
        pyfgc_name.read_group_file()

        groups_ref = pyfgc_name.groups
        results_dict = {g.upper(): set() for g in group_list}

        def get_all_gateways(node: dict, results: set):
            for key, val in node.items():
                if key == "gateways":
                    results.update(val)
                elif isinstance(val, dict):
                    get_all_gateways(val, results)

        def search_groups(node: dict, in_out_data: dict):
            for key, val in node.items():
                if key.upper() in in_out_data:
                    gateways_set = set()
                    get_all_gateways(val, gateways_set)
                    in_out_data[key.upper()].update(gateways_set)
                elif isinstance(val, dict):
                    search_groups(val, in_out_data)

        # Search for gateways on each group
        search_groups(groups_ref, results_dict)
        return {g: results_dict[g.upper()] for g in group_list}

    def _get_gateways_devices(self):
        """Get a list of gateways and devices to filter.

        Raises:
            RuntimeError: If no listings are found.
        """
        if self.filter_groups:
            groups_dict = self._get_gateways_from_groups(self.filter_groups)
            results = set()
            for group, gateways in groups_dict.items():
                if gateways:
                    results.update(gateways)
                    logger.info(
                        "Monitoring group %s: %s.",
                        group,
                        ", ".join(gateways)
                    )
            list_gateways = results
            list_devices = None
        elif self.filter_gateways:
            logger.info(
                "Monitoring gateways: %s.",
                ", ".join(self.filter_gateways)
            )
            list_gateways = self.filter_gateways
            list_devices = None
        elif self.filter_devices:
            logger.info(
                "Monitoring devices: %s.",
                ", ".join(self.filter_devices)
            )
            list_gateways = None
            list_devices = self.filter_devices
        else:
            raise RuntimeError("No groups, gateways or devices defined.")

        return list_gateways, list_devices

    async def _run(self):
        """Run the FGC logger server.

        Raises:
            RuntimeError: Failed to start status receiver object.
        """
        logger.info("Starting FGC logger...")

        self.term_signal = asyncio.Event()
        queue_status = asyncio.Queue()

        # Check gateways and devices
        list_gateways, list_devices = self._get_gateways_devices()

        # Start RBAC manager
        if settings.secret.USE_RBAC:
            logger.info("Use RBAC token.")
            rbac_manager = fgc_logger.rbac_manager.RbacManager()
            await rbac_manager.start()
        else:
            rbac_manager = None

        # Create modules
        if self.recv_mode is settings.ReadMode.STATUS_SERVER:
            status_recv = StatusSrvReader(
                queue_status, self.loop, self.period,
                filter_gateways=list_gateways)
        elif self.recv_mode is settings.ReadMode.STATUS_MONITOR:
            status_recv = StatusMonitorPort(
                queue_status, self.loop, self.port,
                filter_gateways=list_gateways)
        else:
            raise RuntimeError("Unable to start status receiver.")

        evt_manager = EventManager(self.loop, queue_status,
                                   filter_devices=list_devices)

        # Run modules
        evt_manager_fut = asyncio.ensure_future(evt_manager.run())
        status_recv_fut = asyncio.ensure_future(status_recv.run())

        logger.info("FGC logger is running.")

        # Wait for termination signal
        term_fut = asyncio.ensure_future(self.term_signal.wait())
        done, _ = await asyncio.wait({term_fut, evt_manager_fut, status_recv_fut},
                                     return_when=asyncio.FIRST_COMPLETED)

        if term_fut not in done:
            logger.error("Important task failed. Aborting execution.")
            for fut in done:
                # Try to re-raise the exception that triggered the abortion
                fut.result()

        logger.info("Stopping running tasks...")

        if rbac_manager:
            await rbac_manager.stop()

        await status_recv.stop()
        await evt_manager.stop()

        logger.info("Waiting for tasks to finish...")

        await status_recv_fut
        await evt_manager_fut

        logger.info("FGC logger is closed.")

    async def run(self):
        """Run the FGC Logger server as a asyncio task.
        """
        logger.info("Run FGC logger in event loop.")
        self.async_mode = True
        self.loop = asyncio.get_event_loop()
        await self._run()

    def stop(self):
        """Stop the FGC Logger.

        This works the same way for both modes of operation.
        Thread-safe.
        """
        logger.info("Closing FGC logger...")
        try:
            self.loop.call_soon_threadsafe(self.term_signal.set)
        except AttributeError as err:
            raise AttributeError("Event loop has not been started yet.") from err



# EOF