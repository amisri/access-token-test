"""Log fetch module

This module is used to fetch the logs from a device, decode them, and
store in fortlogs.

After this is done, it acknowledges the completion.
"""
import asyncio
import enum
import time
import logging
import concurrent.futures
import threading
import collections
from typing import Tuple, Union

import pyfgc
import pyfgc_log

from fgc_logger.event_analyser import LogReadAborted, AckError
from fgc_logger.log_read_request import RequestAborted, RequestNotCompleted
from fgc_logger.fortlogs_handler import FortLogsTable
from fgc_logger.write_logs import FgcLogWriter

import fgc_logger.utils as utils
import fgc_logger.settings as settings

# Logger

module_logger = logging.getLogger(__name__)

# Timeouts for IO-dependant coroutines

MAX_LOG_READY_WAIT_S = 120  # Wait for a maximum of 2 minutes for the logs to be ready.
MAX_FGC_WAIT_S = 60  # Wait for a maximum of 2 minutes for a FGC response.
MAX_DECODE_WAIT_S = 300  # Wait for a maximum of 5 minutes for a decoded log.
CANCEL_TASKS_TIMEOUT = 2.0 # Time to wait for cancelled tasks

# Retry times

RETRY_DELAY = 10.0

# Failed logs table

# NOTE: Pass strings, integers or floats only
ErrorTableEntry = collections.namedtuple(
    "ErrorTableEntry",
    ["timestamp", "failed_log", "error_type", "error_code", "error_message", "error_info"]
)


class LogDecodingError(Exception):
    """Log decoding failed
    """


class ErrorType(enum.Enum):
    """Types of log read errors.
    """
    FGC_ERROR = enum.auto()
    EXCEPTION = enum.auto()


ERROR_TABLE_NAME = "LOG_READ_ERRORS"
ERROR_TABLE_TYPE = "error"
RETRY_FORTLOGS_PERIOD_S = 15.0

# Process pool executor

shared_executor = None
shared_executor_lock = threading.Lock()


def get_shared_executor() -> concurrent.futures.ProcessPoolExecutor:
    """Returns a process pool executor.

    To be used for log decoding, in a separate process,
    without overloading the current process.

    Returns:
        concurrent.futures.ProcessPoolExecutor: Process pool executor.
    """
    global shared_executor
    with shared_executor_lock:
        if not shared_executor:
            try:
                shared_executor = utils.get_process_pool_exec()
            except RuntimeError:
                shared_executor = concurrent.futures.ProcessPoolExecutor()
        return shared_executor


# Log fetcher

class FortlogsError(Exception):
    """When there is a connection problem with fortlogs.
    """


class FgcLogFetcher:
    """Log fetcher class.

    This object can be used to serve a request for device logs.
    Three steps are followed:
        1) Read logs from device
        2) Decode logs
        3) Store logs in fortlogs

    The algorithm iterates through each one of the steps, always going back to
    the beggining until all logs have been stored.
    """
    def __init__(self, request):

        self.proc_executor = get_shared_executor()

        self.logger = utils.DeviceLogAdapter(module_logger, {"device": request.device_name})

        self.request = request
        self.log_read_lock = asyncio.Lock()
        self.futures = dict()

        self.log_writer = FgcLogWriter(request)

    async def _get_decoded_log(self, log_name: str) -> Tuple[str, str]:
        """Fetch a log from a device, and decode it.

        Args:
            log_name (str): Log name.
            session (pyfgc.FgcAsyncSession): Pyfgc asyn session.

        Raises:
            asyncio.TimeoutError: Log took too long to read or decode.
            RequestAborted: Request is no longer necessary.

        Returns:
            Tuple[str, str]: Tuple containing (decoded log as json string, log type).
        """
        log_get_done = False
        while not log_get_done:

            # Wait for log to be ready
            try:
                self.logger.debug("Waiting for %s.", log_name)
                await self.request.wait_for_log_ready(log_name, timeout=MAX_LOG_READY_WAIT_S)
            except asyncio.TimeoutError as err:
                raise asyncio.TimeoutError(
                    f"Timeout waiting for log {log_name} to be ready.") from err
            except RequestAborted as err:
                raise RequestAborted(
                    f"Log {log_name} reading request aborted.") from err

            # Get log
            attributes = self.request.get_log_attributes(log_name)
            prop = attributes["prop"]
            get_option = attributes.get("get_option", None)
            self.logger.debug("Command: %s %s", prop, get_option)

            try:
                async with self.log_read_lock:
                    # Lock important: Only request one single log at a time.
                    #  - This will avoid starving other requests to the device.
                    #  - Will prevent the gateway from running out of memory.
                    #  - Does not impact performance, because only 1 request can be
                    #    done for a device at a time.
                    self.logger.info("Reading log %s.", log_name)
                    try:
                        response = await self.fgc_session.get(prop, get_option=get_option)
                    except (pyfgc.PyFgcSessionError, asyncio.CancelledError, concurrent.futures.CancelledError):
                        self.logger.error(f"There is a problem with the fgc session or with the gateway connection " +
                                          f"while getting log: {log_name}")
                        await self._reset_fgc_session()
                        continue
            except asyncio.TimeoutError as err:
                raise asyncio.TimeoutError(f"Timeout getting log {log_name}.") from err

            # Get log - analyse response
            try:
                raw_log = response.value
            except pyfgc.FgcResponseError:
                err_code = int(response.err_code)
                err_msg = response.err_msg
                get_option_str = get_option if get_option else ""
                msg = (
                    f"Failed to get {log_name} with error "
                    f"'{err_code}: {err_msg}' (G {prop} {get_option_str})."
                )
                try:
                    await self.request.check_log_read_error(log_name, err_code, err_msg)
                except LogReadAborted:
                    self.logger.warning(msg)
                    raise
                else:
                    self.logger.warning(msg + " Retry again later.")
            else:
                log_get_done = True

        # Decode log
        self.logger.info("Decoding log %s.", log_name)

        # Decode log - Use a process poll executor, to remove decoding overhead
        decoded_log_fut = asyncio.get_event_loop().run_in_executor(
            self.proc_executor,
            self._decode_to_json,
            log_name,
            prop,
            raw_log,
            self.request.device_name,
            self.request.event_timestamp
        )

        try:
            decoded_log, log_type, logging_messages = await asyncio.wait_for(decoded_log_fut, MAX_DECODE_WAIT_S)
        except asyncio.TimeoutError as err:
            raise asyncio.TimeoutError(f"Timeout decoding log {log_name}.") from err

        # Log the messages returned by the other process
        for log_lvl, log_msg in logging_messages:
            self.logger.log(log_lvl, log_msg)

        # Return log and type
        return decoded_log, log_type

    async def execute(self):
        """Run the log fetcher logic.
        """
        await self._get_fgc_session()
        try:
            await self._execute()
        except asyncio.CancelledError:
            for fut in self.futures:
                fut.cancel()
            _, pending = await asyncio.wait(self.futures.keys(), timeout=CANCEL_TASKS_TIMEOUT)
            if pending:
                self.logger.warning(
                    "Unable to cancel tasks fetching %s.",
                    ", ".join({self.futures[task] for task in pending})
                )
        await self._close_fgc_session()

    async def _get_fgc_session(self):
        success = False
        while not success:
            try:
                self.logger.debug("Getting a pyfgc session")
                self.fgc_session = await pyfgc.async_connect(self.request.device_name, timeout_s=MAX_FGC_WAIT_S,
                                                             rbac_token=self.request.token,
                                                             name_file=settings.basic.NAME_FILE)
                success = True
            except (pyfgc.PyFgcSessionError, asyncio.CancelledError, concurrent.futures.CancelledError):
                self.logger.error(f"Failed to get a pyfgc session")
            await asyncio.sleep(3)
        self.logger.debug("Got a pyfgc session successfully")

    async def _close_fgc_session(self):
        self.logger.debug("Closing the pyfgc session")
        if self.fgc_session:
            await self.fgc_session.disconnect()

    async def _reset_fgc_session(self):
        self.logger.debug("Resetting the pyfgc session")
        await self._close_fgc_session()
        await self._get_fgc_session()

    async def _execute(self):
        """Run the log fetcher logic.
        """
        required_logs = self.request.logs
        failed_log_entries = list()
        save_logs_later = dict()
        request_aborted = False

        # Get all logs in parallel
        for log_name in required_logs:
            fut = asyncio.ensure_future(self._get_decoded_log(log_name))
            self.futures[fut] = log_name

        remaining_futures = set()
        remaining_futures.update(self.futures.keys())

        while remaining_futures:

            try:
                done, _ = await asyncio.wait(remaining_futures, return_when=asyncio.FIRST_COMPLETED)
            except asyncio.CancelledError:
                # If current task is cancelled, wait for children tasks
                for fut in remaining_futures:
                    fut.cancel()
                await asyncio.wait(remaining_futures, return_when=asyncio.ALL_COMPLETED, timeout=1.0)
                raise

            remaining_futures.difference_update(done)
            timestamp = time.time()

            # Get decoded logs
            new_logs_decoded = dict()

            for task in done:

                log_name = self.futures[task]

                try:
                    new_logs_decoded[log_name] = task.result()
                except LogReadAborted as err:
                    self.logger.error(str(err))
                    err_entry = ErrorTableEntry(timestamp, log_name, ErrorType.FGC_ERROR.name,
                                                err.err_code, err.err_msg,
                                                f"{type(err).__name__}: {str(err)}")
                    failed_log_entries.append((log_name, err_entry))
                    await self.request.mark_log_read(log_name)
                    continue
                except RequestAborted as err:
                    request_aborted = True
                    self.logger.warning(str(err))
                    err_entry = ErrorTableEntry(timestamp, log_name, ErrorType.EXCEPTION.name,
                                                "", "",
                                                f"{type(err).__name__}: {str(err)}")
                    failed_log_entries.append((log_name, err_entry))
                    await self.request.mark_log_read(log_name)
                    continue
                except asyncio.TimeoutError as err:
                    self.logger.error(str(err),
                                      exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))
                    err_entry = ErrorTableEntry(timestamp, log_name, ErrorType.EXCEPTION.name,
                                                "", "",
                                                f"{type(err).__name__}: {str(err)}")
                    failed_log_entries.append((log_name, err_entry))
                    await self.request.mark_log_read(log_name)
                    continue
                except LogDecodingError as err:
                    self.logger.error(str(err))
                    err_entry = ErrorTableEntry(timestamp, log_name, ErrorType.EXCEPTION.name,
                                                "", "",
                                                f"{type(err).__name__}: {str(err)}")
                    failed_log_entries.append((log_name, err_entry))
                    await self.request.mark_log_read(log_name)
                    continue
                else:
                    await self.request.mark_log_read(log_name)

            # Save logs in fortlogs
            await self.log_writer.save_logs(new_logs_decoded)

        # Try to acknowledge remaining logs
        if not request_aborted:

            try:
                self.logger.info("All logs have been read. Acknowledge device.")
                await self.request.ack_request_done()
            except RequestNotCompleted:
                self.logger.error(
                    "Can't acknowledge because logs have not been read. Will not retry.",
                    exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))
            except AckError:
                self.logger.error(
                    "Failed to acknowledge to device.",
                    exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))

        else:
            self.logger.warning("Request has been aborted. Not acknowledging.")

        # Save log read errors in fortlogs
        if failed_log_entries:

            self.logger.info("There are log reading errors on %s.",
                             ', '.join([name for name, _ in failed_log_entries]))

            table = FortLogsTable(
                self.request.device_name, ERROR_TABLE_NAME, list(ErrorTableEntry._fields))
            for _, entry in failed_log_entries:
                table.append_row(list(entry))

            # Save in fortlogs
            log_dict = dict()
            log_dict[ERROR_TABLE_NAME] = (table.json(), ERROR_TABLE_TYPE)
            await self.log_writer.save_logs(log_dict)

        await self.log_writer.flush()

    @staticmethod
    def _decode_to_json_method(log_name: str,
                               log_property: str,
                               raw_buffer: Union[str, bytes],
                               device: str,
                               timestamp: float
                               ) -> Tuple[str, str, list]:
        """Decode the log.

        The decoding algorithm is very CPU demanding, thereforee should be
        executed by a separate process.

        Args:
            log_name ([type]): Log name.
            log_property (str): Log property name.
            raw_buffer (Union[str, bytes]): Raw log buffer.
            device (str): Device name.

        Returns:
            Tuple[str, str, list]: Tuple containing decoded log. First element is the decoded log,
                in json format, as a string. Second element is the log data type. Third is a list
                of logging messages, these will only be visible if no exception is raised.
        """
        logging_messages = list()

        payload, version = pyfgc_log.extract_payload_and_version(log_property, raw_buffer)

        log_decoded = pyfgc_log.decode(payload, version)
        log_type = pyfgc_log.get_log_type(log_decoded, version)

        t_s, t_us = divmod(timestamp, 1)
        t_seconds = int(t_s)
        t_microseconds = int(round(t_us * 1E6))
        t_nanoseconds = int(round(t_us * 1E9))

        # TODO: Check if time_origin_seconds/time_origin_microseconds should be done elsewhere.

        if isinstance(log_decoded, dict):
            if (
                'time_origin_seconds' in log_decoded and
                'time_origin_microseconds' in log_decoded
            ):
                log_decoded['time_origin_seconds'] = t_seconds
                log_decoded['time_origin_microseconds'] = t_microseconds
            elif (
                    'time_origin_seconds' in log_decoded and
                    'time_origin_nanoseconds' in log_decoded
            ):
                log_decoded['time_origin_seconds'] = t_seconds
                log_decoded['time_origin_nanoseconds'] = t_nanoseconds
            else:
                logging_messages.append((
                    logging.WARNING,
                    f"Unable to modify 'time_origin' of {log_name}."
                ))

        log_json = pyfgc_log.encode(log_decoded,
                                   version,
                                   device=device,
                                   log_name=log_name)
        return log_json, log_type, logging_messages

    @staticmethod
    def _decode_to_json(log_name: str,
                        log_property: str,
                        raw_buffer: Union[str, bytes],
                        device: str,
                        timestamp: float
                        ) -> Tuple[str, str, list]:
        """Wrapper function, for log decoding in a separate process.

        This allows all exceptions to be handled by a separate process,
        without crashing it.

        Args:
            log_name ([type]): Log name.
            log_property (str): Log property name.
            raw_buffer (Union[str, bytes]): Raw log buffer.
            device (str): Device name.

        Returns:
            Tuple[str, str, list]: Tuple containing decoded log.
        """
        try:
            return FgcLogFetcher._decode_to_json_method(log_name,
                                                        log_property,
                                                        raw_buffer,
                                                        device,
                                                        timestamp)
        except pyfgc_log.LogException as err:
            raise LogDecodingError(err.message)



# EOF