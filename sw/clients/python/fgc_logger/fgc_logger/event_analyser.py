"""Log event analyser module

This module is used to anaylse the status data provided by each FGC.
For each data packet received, events are searched in the following order:

    1) Slow-abort
    2) Post-mortem
    3) DIM

NOTE: The support for new events and rules can be added, by implementing a new subclass
      of EvtAnalyser.
"""
import asyncio

import time
import logging
from typing import Coroutine, Mapping, Dict, Set, Optional

import pyfgc
import pyfgc_log

from fgc_logger.log_read_request import LogReadRequest, EventType
import fgc_logger.rbac_manager
import fgc_logger.utils as utils
import fgc_logger.settings as settings

# FGC cycle
CYCLE_DURATION = 0.02

# FGC read timeout
LOG_MENU_TIMEOUT_S = 10
ACK_TIMEOUT_S = 30

# Current FGC fieldbus bitrate: 400KB/s
FGC_BITRATE = 400000

# How log should it take to receive slow-abort data: 1 second
SLOW_ABORT_COMM_TIME = 1.0

# Maximum time to wait for a slow abort log to be completed: 2 seconds
SLOW_ABORT_MAX_WAIT_TIME = 5.0

# Minimum number of samples in a slow-abort reading
SLOW_ABORT_MIN_SAMPLES = 250

# Max offset/duration variable value
MAX_TIME_VAR_VALUE = 999999

# Logger

module_logger = logging.getLogger(__name__)

# Some important FGC errors
PM_IN_PROGRESS = "pm in progress"
DEV_NOT_READY = "dev not ready"


class LogReadAborted(Exception):
    """This log has been aborted.
    Logger should not try to read it again.
    """

    def __init__(self, message: str, err_code: Optional[int] = None, err_msg: Optional[str] = None):
        self.__message = message
        self.__err_code = err_code
        self.__err_msg = err_msg
        Exception.__init__(self, message)

    @property
    def err_code(self):
        """Device error code.
        """
        return self.__err_code

    @property
    def err_msg(self):
        """Device error message.
        """
        return self.__err_msg

    @property
    def message(self):
        """Exception message.
        """
        return self.__message


class AckError(Exception):
    """To be used when the acknowledgment to the device failed.
    """


class EvtAnalyser:
    """Event analyser abstract class.

    Gives a reference of which methods should be exposed.
    Implements some shared logic.
    """

    def __init__(self, device: str, loop: asyncio.AbstractEventLoop):
        """Init method.

        Args:
            device (str): Device name.
            loop (asyncio.AbstractEventLoop): Event loop currently in use.
        """
        self.logger = utils.DeviceLogAdapter(module_logger, {"device": device})
        self.device = device
        self.loop = loop
        self.cached_menu = (0.0, None)
        self.cached_data = (0.0, None)

    def __str__(self):
        return f"{self.__class__.__name__}"

    async def ack_log_get(self, device: str, prop: str, get_option: str):
        """acknowledge log reading.

        Args:
            device (str): Device name.
            prop (str): Acknowledge property name.
            get_option (str): Acknowledge get option.
        """
        token = await fgc_logger.rbac_manager.get_token()

        # retry 5 times to acknowledge log
        success = False
        for attempt in range(1, 6):
            try:
                rsp = await pyfgc.async_get(device, prop, get_option=get_option, timeout_s=ACK_TIMEOUT_S,
                                            rbac_token=token, name_file=settings.basic.NAME_FILE)
                rsp.value
                success = True
                break
            except (asyncio.TimeoutError, pyfgc.PyFgcError, pyfgc.FgcResponseError) as err:
                self.logger.warning(f"Log read acknowledgment attempt {attempt} failed (G {prop} {get_option}): {err}.")
                await asyncio.sleep(3)
                continue
        if not success:
            raise AckError(f"Log read acknowledgment failed (G {prop} {get_option}).")

    async def _get_log_menu(self):
        """Get the LOG.MENU from the FGC and parse it with pyfgc_log"""

        # Read log menu data
        token = await fgc_logger.rbac_manager.get_token()
        try:
            rsp = await pyfgc.async_get(self.device, "LOG.MENU", timeout_s=LOG_MENU_TIMEOUT_S, rbac_token=token,
                                        name_file=settings.basic.NAME_FILE)
        except (asyncio.TimeoutError, pyfgc.PyFgcError):
            level = logging.WARNING if self.event_active else logging.ERROR
            self.logger.log(level,
                            "Failed to get log menu.",
                            exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))
            return None

        # Check device response
        try:
            rsp_value = rsp.value
        except pyfgc.FgcResponseError as err:
            self.logger.error("Failed to get log menu with FGC error: %s", err)
            return None

        try:
            parsed_log_menu = pyfgc_log.parse_log_menu(rsp_value)
        except Exception:
            self.logger.error("Cannot parse log menu. Releasing device with S LOG RESET.")
            await pyfgc.async_set(self.device, "LOG", "RESET", timeout_s=LOG_MENU_TIMEOUT_S, rbac_token=token,
                                  name_file=settings.basic.NAME_FILE)
            await self.reset()
            raise
        return parsed_log_menu

    async def _refresh_log_menu_status(self):
        """Get the LOG.MENU.STATUS from the FGC and update the log_menu data"""

        token = await fgc_logger.rbac_manager.get_token()
        try:
            rsp = await pyfgc.async_get(self.device, "LOG.MENU.STATUS", timeout_s=LOG_MENU_TIMEOUT_S, rbac_token=token,
                                        name_file=settings.basic.NAME_FILE)
        except (asyncio.TimeoutError, pyfgc.PyFgcError):
            level = logging.WARNING if self.event_active else logging.ERROR
            self.logger.log(level, "Failed to get log menu status.",
                            exc_info=(self.logger.getEffectiveLevel() == logging.DEBUG))
            return None

        # Check device response
        try:
            # The response is a comma-delimited list of space-separated flags,
            # e.g. "ANA PM CONT FZ SAVE,ANA CONT FZ MPX,ANA PM PM_BUF SAVE"
            #
            # We split it along the commas to obtain a list in the same order as LOG.MENU.NAMES
            status_by_log_menu_index = rsp.value.split(",")
        except pyfgc.FgcResponseError as err:
            self.logger.error("Failed to get log menu status with FGC error: %s", err)
            return None

        log_menu = self.cached_menu[1]
        for index, name in enumerate(log_menu['NAMES']):
            # Split each list of flags such as "ANA PM PM_BUF SAVE" into a set of strings
            # (mimicking the behavior of pyfgc_log.parse_log_menu)
            log_menu['LOG_DATA'][name]['STATUS'] = set(status_by_log_menu_index[index].split())
        return log_menu

    async def get_log_menu_data(self, timestamp: float = 0.0) -> Optional[Dict]:
        """Read, parse and cache the latest log menu data.

        Caching, using the timestamp as reference, avoids overloading the
        device with multiple requests for the same log menu data.
        New log menu status is fetched when timestamp is higher than previous request.

        Args:
            timestamp (float): Timestamp of current log menu request. Defaults to 0.0,
                which will use the cached value.

        Returns:
            Optional[Dict]: Log menu data of current device. If failed returns None.
        """
        if timestamp <= self.cached_menu[0]:
            return self.cached_menu[1]

        if not self.event_active:
            log_menu = await self._get_log_menu()
        else:
            log_menu = await self._refresh_log_menu_status()

        if log_menu is None:
            return None

        self.cached_menu = (timestamp, log_menu)
        return self.cached_menu[1]

    async def ingest_new_data(self, data_dict: Mapping, timestamp: float):
        """Ingest new device published data.

        Args:
            data_dict (Mapping): Device published data. Should be already decoded.
            timestamp (float): Device published data timestamp.
        """
        self.cached_data = (timestamp, data_dict)

    @property
    def event_timestamp(self) -> float:
        """Get latest event timestamp.

        Returns:
            float: Event timestamp.
        """
        raise NotImplementedError()

    @property
    def event_requests(self) -> LogReadRequest:
        """Get latest event read request.

        Returns:
            LogReadRequest: Read request object.
        """
        raise NotImplementedError()

    @property
    def event_active(self) -> bool:
        """Is latest event active.

        Returns:
            bool: True if event is currently active.
        """
        raise NotImplementedError()

    async def reset(self):
        """Request event reset.
        """
        raise NotImplementedError()


class DIMAnalyser(EvtAnalyser):
    """Event analyser for DIM events.

    Checks for DIM events.
    An independent request will be made for each triggered DIM log.
    This will cause each DIM log to belong to a different acquisition.
    """

    def __init__(self, device: str, loop: asyncio.AbstractEventLoop):
        """See EvtAnalyser docstring.
        """
        super().__init__(device, loop)
        self._event_active = False
        self._ignore_next_data = False  # Required to avoid false positives
        self._event_time = 0
        self._current_requests = None
        self._required_logs = set()
        self._logs_retry_countdown = dict()
        self._event_active_last = False
        self._surpress_ign_error = False

    async def _filter_required_logs(self) -> Set:
        """Find which logs should be read and recorded.

        Returns:
            Set: Name of logs.
        """
        log_menu_data = await self.get_log_menu_data()
        selected_logs = set()
        for log, data in log_menu_data["LOG_DATA"].items():
            status = data["STATUS"]
            if "SAVE" in status and "DIM" in status:
                self.logger.debug("SAVE/DIM flags on log %s.", log)
                selected_logs.add(log)
        return selected_logs

    async def _refresh_ready_logs(self):
        """Check if DIM logs are ready to be read.
        """
        last_pub_time, last_pub_data = self.cached_data
        log_menu_data = await self.get_log_menu_data(last_pub_time)
        if log_menu_data is None:
            self.logger.warning("Failed to analyse log menu status.")
            return

        log_data = log_menu_data["LOG_DATA"]

        # Check logs one-by-one
        for name in self._required_logs:
            status = log_data[name]["STATUS"]
            is_ready = False if "FZ" in status and "RUN" in status else True
            self._current_requests[name].set_log_ready(name, is_ready)
            if is_ready:
                self.logger.debug("%s is ready.", name)

    def logs_done_callback_gen(self, log_name: str, log_prop: str) -> Coroutine:
        """Generates a coroutine to be called to acknowledge all logs have been read.

        This coroutine implements the logic to send the acknowledgment to the device:
        - GET LOG SYNCHED, for each DIM log.

        Args:
            log_name (str): Log name.
            log_prop (str): Log property.

        Returns:
            Coroutine: Callback coroutine.
        """

        async def _callback():
            self.logger.debug("Sent SYNCHED get option for log %s.", log_name)

            # DIM logs need to be set with SYNCHED flag
            if self.event_active:
                await self.ack_log_get(self.device, log_prop, "SYNCHED")

            self._ignore_next_data = True
            await self.reset()

        return _callback

    async def _configure_new_requests(self):
        """Create a request for each log that needs to be read.

        Each DIM log should be handled in a separate acquisition.
        """
        self._current_requests = dict()
        log_menu_data = await self.get_log_menu_data()
        log_data = log_menu_data["LOG_DATA"]
        token = await fgc_logger.rbac_manager.get_token()

        for log_name in self._required_logs:

            prop = log_data[log_name]["PROP"]
            new_request = LogReadRequest(self.device,
                                         EventType.DIM,
                                         self._event_time,
                                         self._fgc_err_callback,
                                         self.logs_done_callback_gen(log_name, prop),
                                         token)

            log_attributes = dict()
            log_attributes["prop"] = prop

            get_options = []
            for option in pyfgc_log.get_log_read_options(prop):
                if option.upper() == "DATA":
                    get_options.append(f"DATA|0,0,0,0,0,0")
                else:
                    get_options.append(option.upper())
            if get_options:
                log_attributes["get_option"] = ' '.join(get_options)

            new_request.add_log(log_name, log_attributes)

            self._current_requests[log_name] = new_request

    async def ingest_new_data(self, data_dict: Mapping, timestamp: float):
        """See EvtAnalyser docstring.
        """
        await super().ingest_new_data(data_dict, timestamp)
        try:
            st_unlatched = data_dict["ST_UNLATCHED"]
        except KeyError:
            if not self._surpress_ign_error:
                self.logger.error("Unable to obtain ST_UNLATCHED pub field. Ignoring data.")
                self._surpress_ign_error = True
            return self._event_active
        else:
            self._surpress_ign_error = False

        if self._ignore_next_data:
            self._ignore_next_data = False
            return self._event_active

        if "LOG_PLEASE" in st_unlatched and not self._event_active:
            log_menu_data = await self.get_log_menu_data(timestamp)
            if (log_menu_data is not None
                    and log_menu_data.get("PM_TIME", 0) == 0
                    and log_menu_data.get("SA_TIME", 0) == 0):
                self._required_logs = await self._filter_required_logs()
                if self._required_logs:
                    self.logger.info("DIM event detected.")
                    self._event_active = True
                    self._event_time = timestamp  # log_menu_data["PM_TIME"]
                    await self._configure_new_requests()
                    await self._refresh_ready_logs()
                else:
                    self.logger.error("DIM event detected, but no log readings requested.")

        elif "LOG_PLEASE" in st_unlatched:
            requests = self._current_requests.values()
            req_logs_read = [
                all(req.log_read_done.values()) for req in requests
            ]
            if not all(req_logs_read):
                await self._refresh_ready_logs()

        elif "LOG_PLEASE" not in st_unlatched and self._event_active:
            self.logger.info("DIM event is over.")
            for request in self._current_requests.values():
                await request.abort_request()
            self._event_active = False
            await self.reset()

        elif "LOG_PLEASE" not in st_unlatched:
            self._event_active = False

        return self._event_active

    async def _fgc_err_callback(self, log_name: str, err_nr: int, err_msg: str):
        """Callback to handle FGC errors received by the log fetcher.

        Raises:
            LogReadAborted: Raise when the log should be aborted, and not read again.
        """
        if PM_IN_PROGRESS.lower() in err_msg.lower() or DEV_NOT_READY.lower() in err_msg.lower():
            self.logger.warning(f"{err_nr}:{err_msg} detected while reading log {log_name}. "
                                f"Reading will be delayed.")
            self._current_requests[log_name].set_log_ready(log_name, False)
        else:
            raise LogReadAborted("Unable to recover. Log reading aborted.",
                                 err_code=err_nr, err_msg=err_msg)

    @property
    def event_timestamp(self) -> float:
        """See EvtAnalyser docstring.
        """
        return self._event_time

    @property
    def event_requests(self) -> LogReadRequest:
        """See EvtAnalyser docstring.
        """
        return set(self._current_requests.values()) if self._event_active else None

    @property
    def event_active(self) -> bool:
        """See EvtAnalyser docstring.
        """
        return self._event_active

    async def reset(self):
        """See EvtAnalyser docstring.
        """
        self._event_active = False
        self._event_time = 0
        if self._current_requests:
            for request in self._current_requests.values():
                await request.abort_request()
            self._current_requests = None
        self._current_requests = None
        self._required_logs = set()
        self._logs_retry_countdown = dict()


class PostMortemAnalyser(EvtAnalyser):
    """Event analyser for post-mortem events.
    """

    def __init__(self, device: str, loop: asyncio.AbstractEventLoop):
        """See EvtAnalyser docstring.
        """
        super().__init__(device, loop)
        self._event_active = False
        self._event_time = 0
        self._current_request = None
        self._required_logs = set()
        self._log_properties = dict()
        self._logs_retry_countdown = dict()
        self._dim_logs = set()
        self._surpress_ign_error = False

    async def _filter_required_logs(self):
        """Find which logs should be read and recorded.

        Returns:
            Set: Name of logs.
        """
        log_menu_data = await self.get_log_menu_data()
        selected_logs = set()
        self._dim_logs = set()
        for log, data in log_menu_data["LOG_DATA"].items():
            status = data["STATUS"]
            if "SAVE" in status:
                selected_logs.add(log)
                if "DIM" in status:
                    self.logger.debug("SAVE/DIM flag on log %s.", log)
                    self._dim_logs.add(log)
                else:
                    self.logger.debug("SAVE flag on log %s.", log)
        return selected_logs

    async def _refresh_ready_logs(self):
        """Check if post-mortem logs are ready to be read.
        """
        last_pub_time, last_pub_data = self.cached_data
        log_menu_data = await self.get_log_menu_data(last_pub_time)
        if log_menu_data is None:
            self.logger.warning("Failed to analyse log menu status.")
            return

        log_data = log_menu_data["LOG_DATA"]

        # Check logs one-by-one
        for name in self._required_logs:
            status = log_data[name]["STATUS"]
            is_ready = False if ("CONT" not in status or "FZ" in status) and "RUN" in status else True
            self._current_request.set_log_ready(name, is_ready)
            if is_ready:
                self.logger.debug("%s is ready.", name)

    async def _logs_done_callback(self):
        """Coroutine to be called when all logs have been saved.

        This coroutine implements the logic to send the acknowledgment to the device:
        - GET LOG SYNCHED, for each DIM log.
        - GET LOG.MENU.PM_TIME ZERO, once.
        """
        # DIM logs need to be set with SYNCHED flag
        for log_name in self._required_logs:
            if log_name in self._dim_logs and self.event_active:
                log_prop = self._log_properties[log_name]
                await self.ack_log_get(self.device, log_prop, "SYNCHED")
                self.logger.debug("Sent SYNCHED get option for log %s.", log_name)

        # When all logs have been read, slow-abort needs to be reset
        if self.event_active:
            await self.ack_log_get(self.device, "LOG.MENU.PM_TIME", "ZERO")
            self.logger.debug("Sent ZERO get option for post mortem event.")

        await self.reset()

    async def _configure_new_request(self):
        """Create a request and add list of logs to be read.
        """
        token = await fgc_logger.rbac_manager.get_token()
        self._current_request = LogReadRequest(self.device,
                                               EventType.POST_MORTEM,
                                               self._event_time,
                                               self._fgc_err_callback,
                                               self._logs_done_callback,
                                               token)
        log_menu_data = await self.get_log_menu_data()
        log_data = log_menu_data["LOG_DATA"]

        for log_name in self._required_logs:
            log_attributes = dict()

            prop = log_data[log_name]["PROP"]
            log_attributes["prop"] = prop

            get_options = []
            for option in pyfgc_log.get_log_read_options(prop):
                if option.upper() == "DATA":
                    get_options.append(f"DATA|0,0,0,0,0,0")
                else:
                    get_options.append(option.upper())
            if get_options:
                log_attributes["get_option"] = ' '.join(get_options)

            self._log_properties[log_name] = prop
            self._current_request.add_log(log_name, log_attributes)

    async def ingest_new_data(self, data_dict: Mapping, timestamp: float):
        """See EvtAnalyser docstring.
        """
        await super().ingest_new_data(data_dict, timestamp)
        try:
            st_unlatched = data_dict["ST_UNLATCHED"]
        except KeyError:
            if not self._surpress_ign_error:
                self.logger.error("Unable to obtain ST_UNLATCHED pub field. Ignoring data.")
                self._surpress_ign_error = True
            return self._event_active
        else:
            self._surpress_ign_error = False

        if "LOG_PLEASE" in st_unlatched and not self._event_active:
            log_menu_data = await self.get_log_menu_data(timestamp)
            if (log_menu_data is not None
                    and log_menu_data.get("PM_TIME", 0) > 0):
                self._required_logs = await self._filter_required_logs()
                if self._required_logs:
                    self.logger.info("Post-Mortem event detected.")
                    self._event_active = True
                    self._event_time = log_menu_data["PM_TIME"]
                    await self._configure_new_request()
                    await self._refresh_ready_logs()
                else:
                    self.logger.error("Post-Mortem event detected, but no log readings requested.")

        elif "LOG_PLEASE" in st_unlatched:
            if not all(self._current_request.log_read_done.values()):
                await self._refresh_ready_logs()

        elif "LOG_PLEASE" not in st_unlatched and self._event_active:
            self.logger.info("Post-Mortem event is over.")
            await self._current_request.abort_request()
            self._event_active = False
            await self.reset()

        elif "LOG_PLEASE" not in st_unlatched:
            self._event_active = False

        return self._event_active

    async def _fgc_err_callback(self, log_name: str, err_nr: int, err_msg: str):
        """Callback to handle FGC errors received by the log fetcher.

        Raises:
            LogReadAborted: Raise when the log should be aborted, and not read again.
        """
        if PM_IN_PROGRESS.lower() in err_msg.lower() or DEV_NOT_READY.lower() in err_msg.lower():
            self.logger.warning(f"{err_nr}:{err_msg} detected while reading log {log_name}. "
                                f"Reading will be delayed.")
            self._current_request.set_log_ready(log_name, False)
        else:
            raise LogReadAborted("Unable to recover. Log reading aborted.",
                                 err_code=err_nr, err_msg=err_msg)

    @property
    def event_timestamp(self) -> float:
        """See EvtAnalyser docstring.
        """
        return self._event_time

    @property
    def event_requests(self) -> LogReadRequest:
        """See EvtAnalyser docstring.
        """
        return {self._current_request, } if self._event_active else None

    @property
    def event_active(self) -> bool:
        """See EvtAnalyser docstring.
        """
        return self._event_active

    async def reset(self):
        """See EvtAnalyser docstring.
        """
        self._event_active = False
        self._event_time = 0
        if self._current_request:
            await self._current_request.abort_request()
            self._current_request = None
        self._current_request = None
        self._required_logs = set()
        self._log_properties = dict()
        self._logs_retry_countdown = dict()
        self._dim_logs = set()


class SlowAbortAnalyser(EvtAnalyser):
    """Event analyser for slow-abort events.
    """

    def __init__(self, device: str, loop: asyncio.AbstractEventLoop):
        """See EvtAnalyser docstring.
        """
        super().__init__(device, loop)
        self._event_active = False
        self._event_time = 0
        self._current_request = None
        self._log_ready_times = dict()
        self._log_ready = dict()
        self._log_properties = dict()
        self._required_logs = set()
        self._pm_in_progress = False
        self._wait_for_ready_fut = None
        self._logs_retry_countdown = dict()
        self._dim_logs = set()
        self._surpress_ign_error = False

    async def _wait_for_ready(self):
        """Wait for slow-abort logs to be ready to be read.

        Signal request object when they become ready.
        """
        waiting_list = [
            (time_ready, log_name) for log_name, time_ready in self._log_ready_times.items()]
        waiting_list.sort()

        for time_ready, log_name in waiting_list:
            wait_time = time_ready - time.time()
            if wait_time > 0:
                self.logger.debug("Waiting %f s for %s.", wait_time, log_name)
                await asyncio.sleep(wait_time)
            self._log_ready[log_name] = True
            if not self._pm_in_progress:
                self.logger.debug("%s is ready.", log_name)
                self._current_request.set_log_ready(log_name)

    async def _filter_required_logs(self):
        """Find which logs should be read and recorded.

        Returns:
            Set: Name of logs.
        """
        log_menu_data = await self.get_log_menu_data()
        log_data = log_menu_data["LOG_DATA"]
        selected_logs = set()
        self._dim_logs = set()
        for log, data in log_data.items():
            status = data["STATUS"]
            if "SAVE" in status:
                selected_logs.add(log)
                if "DIM" in status:
                    self.logger.debug("SAVE/DIM flag on log %s.", log)
                    self._dim_logs.add(log)
                else:
                    self.logger.debug("SAVE flag on log %s.", log)
        return selected_logs

    async def _calculate_reading_times(self):
        """
                    FGC_BITRATE * MAX_COMM_TIME
        read_time = ----------------------------------------------------------
                    SUM <for each log> ( SAMPLES_PER_S * SIGNALS_PER_LOG * 4 )

        """
        log_menu_data = await self.get_log_menu_data()
        log_data = log_menu_data["LOG_DATA"]
        read_duration = dict()
        log_bitrate = dict()

        for log, data in log_data.items():
            if data["PERIOD"]:
                log_bitrate[log] = len(data["SIGNALS"]) * (1.0 / data["PERIOD"])

        if log_bitrate:
            total_bitrate = sum(log_bitrate.values())
            logs_time = (FGC_BITRATE * SLOW_ABORT_COMM_TIME) / total_bitrate

        for log in log_data:
            if log in log_bitrate:
                samples = logs_time * (1.0 / log_data[log]["PERIOD"])
                l_time = logs_time if samples > SLOW_ABORT_MIN_SAMPLES else (
                             log_data[log]["PERIOD"] * SLOW_ABORT_MIN_SAMPLES
                         ) / 1.0
                read_duration[log] = l_time
            else:
                read_duration[log] = 0

        return read_duration

    async def _refresh_ready_logs(self):
        """Check if slow-abort logs are ready to be read.
        """
        _, last_pub_data = self.cached_data

        if "POST_MORTEM" in last_pub_data["ST_UNLATCHED"]:
            self.logger.debug("POST_MORTEM flag is on. Log reading should wait.")
            self._pm_in_progress = True
            for name in self._required_logs:
                self._current_request.set_log_ready(name, False)
        else:
            self._pm_in_progress = False
            for name in self._required_logs:
                if self._log_ready[name]:
                    self._current_request.set_log_ready(name)
                    self.logger.debug("%s is ready.", name)

    async def _logs_done_callback(self):
        """Coroutine to be called when all logs have been saved.

        This coroutine implements the logic to send the acknowledgment to the device:
        - GET LOG SYNCHED, for each DIM log.
        - GET LOG.MENU.SA_TIME ZERO, once.
        """
        # DIM logs need to be set with SYNCHED flag
        for log_name in self._required_logs:
            if log_name in self._dim_logs and self.event_active:
                log_prop = self._log_properties[log_name]
                await self.ack_log_get(self.device, log_prop, "SYNCHED")
                self.logger.debug("Sent SYNCHED get option for log %s.", log_name)

        # When all logs have been read, slow-abort needs to be reset
        if self.event_active:
            await self.ack_log_get(self.device, "LOG.MENU.SA_TIME", "ZERO")
            self.logger.debug("Sent ZERO get option for slow abort event.")

        await self.reset()

    async def _configure_new_request(self):
        """Create a request and add list of logs to be read.
        """
        token = await fgc_logger.rbac_manager.get_token()
        self._current_request = LogReadRequest(self.device,
                                               EventType.SLOW_ABORT,
                                               self._event_time,
                                               self._fgc_err_callback,
                                               self._logs_done_callback,
                                               token)
        log_menu_data = await self.get_log_menu_data()
        log_data = log_menu_data["LOG_DATA"]

        reading_times = await self._calculate_reading_times()

        for log_name in self._required_logs:
            log_attributes = dict()

            prop = log_data[log_name]["PROP"]
            log_attributes["prop"] = prop

            get_options = []
            self._log_ready_times[log_name] = self._event_time
            for option in pyfgc_log.get_log_read_options(prop):
                if option.upper() == "DATA":
                    time_origin_s, time_origin_fraction = str(float(self._event_time)).split(".")
                    time_origin_ns = int(float(f"0.{time_origin_fraction}") * 10 ** 9)
                    wait_time_s = min(SLOW_ABORT_MAX_WAIT_TIME, reading_times[log_name] / 2)
                    time_offset_ms = min(int(wait_time_s * 1000), MAX_TIME_VAR_VALUE)
                    time_duration_ms = min(int(reading_times[log_name] * 1000), MAX_TIME_VAR_VALUE)
                    get_options.append(
                        f"DATA|{time_origin_s},{time_origin_ns},{time_offset_ms},{time_duration_ms},0,0")
                    self._log_ready_times[log_name] += wait_time_s  # Add wait time
                else:
                    get_options.append(option.upper())
            if get_options:
                log_attributes["get_option"] = ' '.join(get_options)

            self._log_properties[log_name] = prop
            self._current_request.add_log(log_name, log_attributes)

        self._log_ready = {log: False for log in self._required_logs}
        self._wait_for_ready_fut = asyncio.ensure_future(self._wait_for_ready())

    async def ingest_new_data(self, data_dict: Mapping, timestamp: float):
        """See EvtAnalyser docstring.
        """
        await super().ingest_new_data(data_dict, timestamp)
        try:
            st_unlatched = data_dict["ST_UNLATCHED"]
        except KeyError:
            if not self._surpress_ign_error:
                self.logger.error("Unable to obtain ST_UNLATCHED pub field. Ignoring data.")
                self._surpress_ign_error = True
            return self._event_active
        else:
            self._surpress_ign_error = False

        if "LOG_PLEASE" in st_unlatched and not self._event_active:
            log_menu_data = await self.get_log_menu_data(timestamp)
            if (log_menu_data is not None
                    and log_menu_data.get("SA_TIME", 0) > 0):
                self._required_logs = await self._filter_required_logs()
                if self._required_logs:
                    self.logger.info("Slow-Abort event detected.")
                    self._event_active = True
                    self._event_time = log_menu_data["SA_TIME"]
                    await self._configure_new_request()
                    await self._refresh_ready_logs()
                else:
                    self.logger.error("Slow-abort event detected, but no log readings requested.")

        elif "LOG_PLEASE" in st_unlatched:
            if not all(self._current_request.log_read_done.values()):
                await self._refresh_ready_logs()

        elif "LOG_PLEASE" not in st_unlatched and self._event_active:
            self.logger.info("Slow-Abort event is over.")
            await self._current_request.abort_request()
            self._wait_for_ready_fut.cancel()
            self._event_active = False
            await self.reset()

        elif "LOG_PLEASE" not in st_unlatched:
            self._event_active = False

        return self._event_active

    @property
    def event_timestamp(self) -> float:
        """See EvtAnalyser docstring.
        """
        return self._event_time

    @property
    def event_requests(self) -> LogReadRequest:
        """See EvtAnalyser docstring.
        """
        return {self._current_request, } if self._event_active else None

    @property
    def event_active(self) -> bool:
        """See EvtAnalyser docstring.
        """
        return self._event_active

    async def _fgc_err_callback(self, log_name: str, err_nr: int, err_msg: str):
        """Callback to handle FGC errors received by the log fetcher.

        Raises:
            LogReadAborted: Raise when the log should be aborted, and not read again.
        """
        if PM_IN_PROGRESS.lower() in err_msg.lower():
            self.logger.warning(f"{err_nr}:{err_msg} detected while reading log {log_name}. "
                                f"Reading will be delayed.")
            self._current_request.set_log_ready(log_name, False)
        else:
            raise LogReadAborted(f"Unable to recover {log_name}. Log reading aborted.",
                                 err_code=err_nr, err_msg=err_msg)

    async def reset(self):
        """See EvtAnalyser docstring.
        """
        self._event_active = False
        self._event_time = 0
        if self._current_request:
            await self._current_request.abort_request()
            self._current_request = None
        self._current_request = None
        self._log_ready_times = dict()
        self._log_ready = dict()
        self._log_properties = dict()
        self._required_logs = set()
        self._pm_in_progress = False
        self._wait_for_ready_fut = None
        self._logs_retry_countdown = dict()
        self._dim_logs = set()

# EOF
