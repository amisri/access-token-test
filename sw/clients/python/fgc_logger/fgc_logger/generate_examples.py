"""
Generate example files for the FGC Logger.
"""

import os
import sys
import getpass
import shutil
from pathlib import Path
from string import Template

HOME = Path.home()
HERE = Path(__file__).parent.absolute()

CONFIG = "config_example.cfg"  # To be generated
CONFIG_TEMPLATE = CONFIG + ".template"

LAUNCHER = "launcher_example.sh"  # To be generated
LAUNCHER_TEMPLATE = LAUNCHER + ".template"

SYSTEMD_FILE = "fgc_logger.service"  # To be generated
SYSTEMD_FILE_TEMPLATE = "fgc_logger.service.template"

SECRET = "secret_example.json"
LIST_GROUPS = "list_groups_example.txt"
LIST_GATEWAYS = "list_gateways_example.txt"
LIST_DEVICES = "list_devices_example.txt"


def generate():

    subs_dict = {
        "tag_home": str(HOME),
        "tag_venv": sys.prefix,
        "tag_user": getpass.getuser(),
        "tag_dest": str(HERE)
    }

    # Fill config template
    config_template_path = HERE / "templates" / CONFIG_TEMPLATE
    config_path = HERE / CONFIG

    print(f"{CONFIG}...", end='')

    with open(config_template_path) as config_template_file:
        config_template = Template(config_template_file.read())
    with open(config_path, "w") as config_file:
        config_file.write(config_template.safe_substitute(subs_dict))

    print("done")

    # Fill launcher template
    launcher_template_path = HERE / "templates" / LAUNCHER_TEMPLATE
    launcher_path = HERE / LAUNCHER

    print(f"{LAUNCHER}...", end='')

    with open(launcher_template_path) as launcher_template_file:
        launcher_template = Template(launcher_template_file.read())
    with open(launcher_path, "w") as launcher_file:
        launcher_file.write(launcher_template.safe_substitute(subs_dict))
    os.chmod(launcher_path, 0o755)  # Needs to be executable

    print("done")

    # Fill systemd service template
    systemd_file_template_path = HERE / "templates" / SYSTEMD_FILE_TEMPLATE
    systemd_file_path = HERE / SYSTEMD_FILE

    print(f"{SYSTEMD_FILE}...", end='')

    with open(systemd_file_template_path) as systemd_template_file:
        systemd_template = Template(systemd_template_file.read())
    with open(systemd_file_path, "w") as systemd_file:
        systemd_file.write(systemd_template.safe_substitute(subs_dict))

    print("done")

    # Secret permissions
    secret_orig_path = HERE / "templates" / SECRET
    secret_dest_path = HERE / SECRET

    print(f"{SECRET}...", end='')

    shutil.copyfile(secret_orig_path, secret_dest_path)
    os.chmod(secret_dest_path, 0o600)  # Only user can read

    print("done")

    # List of groups/gateways/devices permissions
    list_gr_orig_path = HERE / "templates" / LIST_GROUPS
    list_gr_dest_path = HERE / LIST_GROUPS
    print(f"{LIST_GROUPS}...", end='')
    shutil.copyfile(list_gr_orig_path, list_gr_dest_path)
    print("done")

    list_gw_orig_path = HERE / "templates" / LIST_GATEWAYS
    list_gw_dest_path = HERE / LIST_GATEWAYS
    print(f"{LIST_GATEWAYS}...", end='')
    shutil.copyfile(list_gw_orig_path, list_gw_dest_path)
    print("done")

    list_dv_orig_path = HERE / "templates" / LIST_DEVICES
    list_dv_dest_path = HERE / LIST_DEVICES
    print(f"{LIST_DEVICES}...", end='')
    shutil.copyfile(list_dv_orig_path, list_dv_dest_path)
    print("done")


if __name__ == "__main__":
    print("Generating files...")
    generate()
    print("All done.")



# EOF