"""Fortlogs handling module.

Handles the communication with fortlogs REST api.

TODO: External labs may not use Oauth2.
      - Provide an implementation that supports other authentication systems; OR
      - Provide an implementation that does not require fortlogs authentication.
"""
import asyncio
import base64
import json
import logging
from typing import List

import aiohttp

import fgc_logger.rbac_manager
import fgc_logger.settings as settings
import fgc_logger.utils as utils

module_logger = logging.getLogger(__name__)

ADD_ACQ_URL_PATH = "/fgc-logger"

# Endpoints for different types of logs
BUF_TYPE_TO_URL_PATH = {
    "analog": "/analog-logs",
    "digital": "/digital-logs",
    "table": "/table-logs",
    "event": "/table-logs/event-logs",
    "error": "/table-logs/error-logs",
    "timing": "/table-logs/timing-logs",
    "cycle": "/table-logs/cycle-logs",
}

KEEP_ALIVE = 1.0
MAX_CONNECTIONS = 30
JSON_CURRENT_VERSION = "2.0"


class FortLogsTable:
    """Class that handles the creation of a fortlogs log of type table.
    """
    def __init__(self, device: str, name: str, headings: List[str]):
        """Init method.

        Args:
            device (str): Device name.
            name (str): Table log name, as it will appear to the user.
            headings (List[str]): Table log headers.
        """
        self.device = device
        self.headings = headings
        self.rows = list()
        self.table_name = name

    @property
    def name(self) -> str:
        """Table log name.

        Returns:
            str: Table log name.
        """
        return self.table_name

    def append_row(self, row: List[str]):
        """Add new row of data.

        Args:
            row (Iterable[str]): Row of data.

        Raises:
            ValueError: When the number of elements does not match the table headers.
        """
        if len(row) != len(self.headings):
            raise ValueError(
                "Number of elements in row does not match number of elements in header.")
        for cell in row:
            if not isinstance(cell, (str, int, float)):
                raise ValueError(
                    f"Cell value {cell} is of invalid type ({type(cell).__name__}).")
        self.rows.append(row)

    def json(self) -> str:
        """Convert the table to a json string.

        Returns:
            str: Json string containing table data.
        """
        return encode_error_table(self.headings, self.rows, self.device, self.name)


def encode_error_table(headings: List[str], rows: List[List[str]], device: str, name: str) -> str:
    """Build a JSON string for the error table log.

    Args:
        headings (List[str]): Table headers.
        rows (List[List[str]]): List of rows of data.
        device (str): Device name.
        name (str): Log name, as it will appear to the user.

    Returns:
        str: Json string containing table data.
    """

    device = device if device is not None else "unknown"
    name = name if name is not None else "unknown"

    table_dict = dict()

    # Table headers
    headings_dict = dict()
    headings_dict["cells"] = headings
    table_dict["headings"] = headings_dict

    # Table rows
    rows_list = list()
    for row in rows:
        row_dict = dict()
        row_dict["cells"] = row
        rows_list.append(row_dict)
    table_dict["rows"] = rows_list

    # Other data
    data_dict = dict()
    data_dict["source"] = "FGC"
    data_dict["device"] = device
    data_dict["name"] = name
    data_dict["version"] = JSON_CURRENT_VERSION
    data_dict["table"] = table_dict

    return json.dumps(data_dict)


class FortLogsSessionContext:
    """Context manager for FortLogsSession
    """

    def __init__(self, device_name):
        self.device_name = device_name
        self.session = None

    async def __aenter__(self):
        self.session = await FortLogsSession.create(self.device_name)
        await self.session.start()
        return self.session

    async def __aexit__(self, exc_type, exc, tb):
        await self.session.close()


class FortLogsSession:
    """Class that handles a session with fortlogs.
    """

    loop = None
    secret = dict()

    token_is_rbac = True
    token_fortlogs = None
    token_fut = None

    sem = asyncio.Semaphore(MAX_CONNECTIONS)

    def __init__(self, device_name):
        self.logger = utils.DeviceLogAdapter(module_logger, {"device": device_name})

    @classmethod
    async def create(cls, device_name):
        """Async init method.

        Use this class method as an object factory,
        instead of __init__().
        """
        fl_session = FortLogsSession(device_name)

        # Setup class
        if not cls.loop:
            if settings.secret.USE_FORTLOGS_TOKEN:
                with open(settings.secret.PATH) as secret_file:
                    cls.secret = json.loads(secret_file.read())

            cls.loop = asyncio.get_event_loop()

        # Get tokens
        if settings.secret.USE_FORTLOGS_TOKEN and not cls.token_fortlogs:
            await cls._refresh_tokens()

        # There must not be more than a single event loop
        assert cls.loop is asyncio.get_event_loop()

        # Create object
        return fl_session

    @classmethod
    async def _refresh_tokens(cls):
        """Refresh fortlogs access token.

        This will get a new FGC Logger token, and then exchange it
        for the fortlogs token, using the keycloak endpoint.
        """
        async def _refresh():
            module_logger.info("Acquiring new token for Fortlogs.")
            cls.token_fortlogs = await get_token(cls.secret, token_is_rbac=cls.token_is_rbac)

        # Guarantees that we only ask 1 time for the token
        if not cls.token_fut or cls.token_fut.done():
            cls.token_fut = asyncio.ensure_future(_refresh())
            await cls.token_fut
            cls.token_fut = None
        else:
            await cls.token_fut

    async def start(self):
        """Start forlogs session.
        """
        pass

    async def close(self):
        """Close fortlogs session.
        """
        pass

    async def create_acquisition(self, event: str, event_time: float) -> int:
        """Create a new fortlogs aquisition, for a certain event.

        Args:
            event (str): Log event type.
            event_time (float): Log event timestamp.

        Returns:
            int: New acquisition ID.
        """
        for attempt_inv in reversed(range(2)):
            try:
                async with FortLogsSession.sem:
                    async with aiohttp.ClientSession(raise_for_status=True) as session:
                        kwargs = dict()
                        kwargs["data"] = json.dumps({"cause": event, "time_event": event_time})
                        if settings.secret.USE_FORTLOGS_TOKEN:
                            kwargs["headers"] = {
                                "Content-Type": "application/json",
                                "Authorization": f"Bearer {self.token_fortlogs}"
                            }
                        response = await session.post(
                            settings.fortlogs.FORTLOGS_URL + ADD_ACQ_URL_PATH,
                            **kwargs
                        )

            except aiohttp.ClientResponseError as err:
                self.logger.warning("Create acquisition error %s %s.", err.status, err.message)
                if settings.secret.USE_FORTLOGS_TOKEN and int(err.status / 100) == 4:
                    await self._refresh_tokens()
                    type(self).token_is_rbac = not type(self).token_is_rbac
                else:
                    raise

            else:
                rsp_json = await response.json()
                return rsp_json["acquisition_id"]

    async def add_buffer(self, acquisition_id: int, buffer_type: str, buffer: str):
        """Add a new buffer to an existing fortlogs acquisition.

        Args:
            acquisition_id (int): Acquisition ID.
            buffer_type (str): Log buffer data type.
            buffer (str): Log buffer data, in json string format.
        """
        url = settings.fortlogs.FORTLOGS_URL + BUF_TYPE_TO_URL_PATH[buffer_type]

        for attempt_inv in reversed(range(2)):
            try:
                async with FortLogsSession.sem:
                    async with aiohttp.ClientSession(raise_for_status=True) as session:
                        kwargs = dict()
                        kwargs["data"] = buffer
                        kwargs["params"] = {"acquisition_id": acquisition_id}
                        if settings.secret.USE_FORTLOGS_TOKEN:
                            kwargs["headers"] = {
                                "Content-Type": "application/json",
                                "Authorization": f"Bearer {self.token_fortlogs}"
                            }
                        await session.post(
                            url,
                            **kwargs
                        )

            except aiohttp.ClientResponseError as err:
                self.logger.warning("Add buffer error %s %s.", err.status, err.message)
                if settings.secret.USE_FORTLOGS_TOKEN and int(err.status / 100) == 4:
                    await self._refresh_tokens()
                    type(self).token_is_rbac = not type(self).token_is_rbac
                else:
                    raise

            else:
                break


# Get token for fortlogs
async def get_token(config: dict, token_is_rbac: bool) -> str:
    """Obtain token for Fortlogs access,
    using FGC Logger client ID and secret.

    Args:
        config (dict): Dictionary with ''client_id' and 'secret'.
        token_is_rbac (bool): Flag to use rbac or oauth token

    Returns:

    """
    if token_is_rbac:
        module_logger.info("Acquiring an RBAC token for FortLogs")
        return base64.b64encode(await fgc_logger.rbac_manager.get_token()).decode('ascii')
    else:
        module_logger.info("Acquiring an OAuth token for FortLogs")
        timeout = aiohttp.ClientTimeout(total=60)
        async with aiohttp.ClientSession(raise_for_status=True, timeout=timeout) as session:
            token = await session.post(
                settings.secret.AUTH_URL,
                auth=aiohttp.BasicAuth(config["client_id"], config["secret"]),
                data={
                    "grant_type": "client_credentials",
                    "audience": settings.secret.AUDIENCE,
                }
            )
        return (await token.json())['access_token']



# EOF