
# Regulation Toolbox

This tool is used to compute the RST and ILC coefficients needed for power converter commissioning. 
For the thyristor converters, this tool can also be used to compute the coeffecients for the damping-loop (ki, ku, kd) and voltage-loop (kint, kp, kff). The coefficients can be computed using the converter and magnet model, 
or by providing a frequency response function (i.e., data-driven optimization). 
The user has three options for specifying the type of desired performance; the user 
can obtain H_1, H_2, or H_inf performance. The UI parameters with the user's desired specifications are sent
to this package, and the RST and ILC controllers are automatically calculated for them.

## Usage


## Installing
1. `pip install cvxpy`

2. 
    1. `import cvxpy` will create error (/lib/libstdc++.so.6: version `CXXABI_1.3.8' not found)

    ## New fix:
    2. Put the following in the .bashrc file in anicolet main directory: 
    `export LD_LIBRARY_PATH=/acc/local/share/python/acc-py/pro/gcc/lib64/:$LD_LIBRARY_PATH`

    ## Old fix:
    2. `LD_LIBRARY_PATH=/afs/cern.ch/user/a/anicolet/regulation_tools/venv/lib64/:$LD_LIBRARY_PATH`
    (Note: the usual location of LD_LIBRARY_PATH is /usr/java/jdk/jre/amd64/). Try echo $LD_LIBRARY_PATH to confirm.

    On workspace:
    `LD_LIBRARY_PATH=/afs/cern.ch/work/a/anicolet/private/regulation_tools/venv/lib64/:$LD_LIBRARY_PATH`
    `LD_LIBRARY_PATH=/afs/cern.ch/work/a/anicolet/private/fgc/sw/clients/python/pyfresco/venv/lib64:$LD_LIBRARY_PATH`

    3. `export LD_LIBRARY_PATH`

    4. Search for location of distribution 
    
    `ldd /afs/cern.ch/user/a/anicolet/regulation_tools/venv/lib/python3.6/site-packages/_cvxcore.cpython-36m-x86_64-linux-gnu.so`

    Output will be something like:         
    
    ```bash
    
    linux-vdso.so.1 =>  (0x00007fff7bdb4000)
            libpython3.6m.so.1.0 => /acc/local/share/python/acc-py/pro/lib/libpython3.6m.so.1.0 (0x00007f94b9dae000)
            libstdc++.so.6 => /acc/local/share/python/acc-py/pro/gcc/lib64/libstdc++.so.6 (0x00007f94b9a2c000)
            libm.so.6 => /lib64/libm.so.6 (0x00007f94b972a000)
            libgcc_s.so.1 => /acc/local/share/python/acc-py/pro/gcc/lib64/libgcc_s.so.1 (0x00007f94b9513000)
            libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f94b92f7000)
            libc.so.6 => /lib64/libc.so.6 (0x00007f94b8f29000)
            libdl.so.2 => /lib64/libdl.so.2 (0x00007f94b8d25000)
            libutil.so.1 => /lib64/libutil.so.1 (0x00007f94b8b22000)
            /lib64/ld-linux-x86-64.so.2 (0x00007f94ba58e000)
    ```
		
3. Do the following:
`cp /acc/local/share/python/acc-py/pro/gcc/lib64/libstdc++.so.6 /afs/cern.ch/work/a/anicolet/private/fgc/sw/clients/python/pyfresco/venv/lib`


## Documentation

Documentation is generated by sphinx and put on the docs directory.


## Testing

