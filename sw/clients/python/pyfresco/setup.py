import os

from setuptools import setup, find_packages
from codecs import open


here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'pyfresco', '__version__.py'), 'r', 'utf-8') as f:
    exec(f.read(), about)


with open("README.md", "r") as fh:
    description = fh.read()


requirements = {
    'core': [
        'pyfgc>=1.4.1',
        'cvxpy>=1.0.31',
        'cvxopt>=1.2.5',
        'numpy>=1.18.4',
        'pandas>=1.0.5',
        'control>=0.8.3',
        'tabulate>=0.8.7',
        'scipy>=1.5.2'
    ],
    'test': [
        'pytest',
    ],
    'doc': [
        'sphinx',
        'sphinx_rtd_theme',
    ],
}


setup(
    name="pyfresco",
    version=about['__version__'],
    author="Achille Nicoletti",
    author_email="achille.nicoletti@cern.ch",
    description="Regulation Toolbox for Power Converters",
    long_description=description,
    long_description_content_type='text/markdown',

    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/pyfresco/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/pyfresco',
    },
    packages=find_packages(),
    package_data={"pyfresco.tests": ['test_data/*.json', 'test_data/*.pickle']},

    python_requires=">=3.7",
    install_requires=requirements['core'],
    extras_require={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test'],
)
