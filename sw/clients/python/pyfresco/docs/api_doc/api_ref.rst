API Reference
=============

ObCD Objects/Methods
--------------------

.. autofunction:: pyfresco.obcd.get_default_b

.. autofunction:: pyfresco.obcd.get_default_i

.. autofunction:: pyfresco.obcd.get_default_v

.. autoclass:: pyfresco.obcd.FgcProperties
    :members:

    .. automethod:: __init__

.. autoclass:: pyfresco.obcd.OptimizeIB
    :members:

    .. automethod:: __init__

.. autoclass:: pyfresco.obcd.OptimizeV
    :members:

    .. automethod:: __init__

.. autofunction:: pyfresco.obcd.nyquist

FRM Objects/Methods
-------------------

.. autofunction:: pyfresco.frm.get_default_prbs

.. autofunction:: pyfresco.frm.get_default_sine

.. autoclass:: pyfresco.frm.FgcProperties
    :members:

    .. automethod:: __init__

.. autoclass:: pyfresco.frm.Frm_methods
    :members:

    .. automethod:: __init__