from .opt_select import OptimizeIB
from .opt_select import OptimizeV
from .props import UiParams, FgcProperties
from .common_funcs import Funcs

get_default_b = UiParams().get_default_b
get_default_i = UiParams().get_default_i
get_default_v = UiParams().get_default_v
nyquist = Funcs.nyquist