import numpy as np
import control as cs
from .common_funcs import Funcs


def MA_filter(w, control_mode, prop):
    Ts_fund = round(prop['FGC.ITER_PERIOD'], 6)

    meas_select = prop['REG.{}.EXTERNAL.MEAS_SELECT'.format(control_mode)]
    if meas_select == 'UNFILTERED' or meas_select == 'EXTRAPOLATED':
        filter_tot = 1
    elif meas_select == 'FILTERED':
        z = lambda x: np.exp(x * 1j * w * Ts_fund)
        fir_len1 = prop['MEAS.{}.FIR_LENGTHS'.format(control_mode)][0]
        fir_len2 = prop['MEAS.{}.FIR_LENGTHS'.format(control_mode)][1]

        if fir_len1 == 0:
            fir_filter_1 = 1
        elif fir_len1 > 0:
            fir_filter_1 = 0
            for i in range(fir_len1):
                fir_filter_1 += z(-i)
            fir_filter_1 = fir_filter_1 / fir_len1
        else:
            raise Exception(f'Invalid value for MEAS.{control_mode}.FIR_LENGTHS.')

        if fir_len2 == 0:
            fir_filter_2 = 1
        elif fir_len2 > 0:
            fir_filter_2 = 0
            for i in range(fir_len2):
                fir_filter_2 += z(-i)
            fir_filter_2 = fir_filter_2 / fir_len2
        else:
            raise Exception(f'Invalid value for MEAS.{control_mode}.FIR_LENGTHS.')

        filter_tot = fir_filter_1 * fir_filter_2

    return filter_tot


def thiran(delay, Ts):

    D = delay / Ts
    N = int(np.ceil(D))
    d_vec = np.arange(1, N + 1)
    ak = []
    for k in d_vec:
        prod_term = 1
        for i in range(N + 1):
            prod_term = prod_term * (D - N + i) / (D - N + k + i)
        comb_term = np.math.factorial(N) / (np.math.factorial(k) * np.math.factorial(N - k))
        ak_term = (-1)**k * comb_term * prod_term
        ak.append(ak_term)

    akf = np.insert(ak, 0, 1)
    del_thiran = cs.tf(akf[::-1], akf, Ts)

    return del_thiran


class Model:
    def __init__(self, user_pars, prop):
        self.user_pars = user_pars
        self.control_mode = user_pars.control_mode
        self.prop = prop
        self.Ts_fund = round(self.prop['FGC.ITER_PERIOD'], 6)
    
    def model_frf(self, n, epsilon, beta): # this model is for current or field control
        self.Ts = self.Ts_fund * int(self.prop[f'REG.{self.control_mode}.PERIOD_ITERS'])
        nu = n
        Nw = (1 / epsilon) * (np.log(1 / beta) + nu - 1 + np.sqrt(2 *
            (nu - 1) * np.log(1 / beta)))
        Nw = int(np.ceil(Nw))

        Rs = self.prop['LOAD.OHMS_SER']
        Rm = self.prop['LOAD.OHMS_MAG']
        Lm = self.prop['LOAD.HENRYS']
        Rp = self.prop['LOAD.OHMS_PAR']

        if self.prop['VS.ACTUATION'] == 'VOLTAGE_REF':
            clbw_vs = self.prop['VS.SIM.BANDWIDTH']
            zeta_vs = self.prop['VS.SIM.Z']
        else:
            clbw_vs = self.user_pars.volt_bw
            zeta_vs = self.user_pars.volt_z

        # Get initial frequency grid point w_init
        g0 = 1 / (Rs + Rp)
        g1 = 1 / (Rs + (Rp * Rm) / (Rp + Rm)) - g0
        tau = Lm / (Rm + (Rp * Rs) / (Rp + Rs))
        M_dc = g0 + g1
        minus_3db = M_dc * 10 ** (-3 / 20)
        w_init = 1 / tau * np.sqrt((minus_3db ** 2 - M_dc ** 2) /
            (g0 ** 2 - minus_3db ** 2)) / 100

        # Use w_init as the starting value for the frequency grid
        w = np.logspace(np.log10(w_init), np.log10(np.pi / self.Ts), Nw) # array of frequencies in rad/sec
        if self.user_pars.noise_rej:
            for pair in self.user_pars.noise_rej:
                if pair[0] > 1 / (2 * self.Ts) or 2 * np.pi * pair[0] < w[0]:
                    raise Exception(f'ERROR: Cannot have a noise rejection frequency outside of '
                            f'the range [{w[0] / (2 * np.pi)}, {1 / (2 * self.Ts)}] Hz')
                w = np.concatenate((w, [2 * np.pi * pair[0]]))
        w = np.sort(w)

        wn = (2 * np.pi * clbw_vs) / np.sqrt(1 - 2 * np.power(zeta_vs, 2) +
            np.sqrt(2 - 4 * np.power(zeta_vs, 2) + 4 * np.power(zeta_vs, 4)))
        
        # Achille's code
        # Vs = np.power(wn, 2) / (-np.power(w, 2) + 2 * zeta_vs * wn * 1j * w + np.power(wn, 2))
        # Z1 = Rm + 1j * w * Lm
        # M = 1 / (Rs + 1 / (1 / Rp + 1 / Z1))
        
        # Michele's attemp
        M_cont = cs.tf([Lm,(Rm+Rp)],[Lm*(Rp+Rs),(Rm*Rp + Rm*Rs + Rp*Rs)])
        Vs_cont = cs.tf(wn**2, [1, 2*zeta_vs*wn, wn**2])
        G_no_delay_cont = M_cont * Vs_cont
        
        # Achille's unused code
        # Vss = cs.tf(wn**2, [1, 2*zeta_vs*wn, wn**2])
        # numv = [wn**2, 2*wn**2, wn**2]
        # denv = [4/(self.Ts**2) + 4/self.Ts * zeta_vs*wn + wn**2,
        #    2*wn**2 - 8/(self.Ts**2), 4/(self.Ts**2) - 4/self.Ts * zeta_vs * wn + wn**2]
        # Vsz = cs.tf(numv, denv, self.Ts)

        # t1 = Rs*Rm+Rs*Rp+Rm*Rp
        # Ms = cs.tf([Lm, Rm+Rp], [Rs*Lm+Rp*Lm, t1])

        Delay_total = Funcs.delay_from_model(self.Ts_fund, self.control_mode, self.prop)
        # Achille's unused code
        # Gz = cs.c2d(Vss * Ms, self.Ts) * thiran(Delay_total, self.Ts)
        # Gz = cs.c2d(Ms, self.Ts) * Vsz * thiran(Delay_total, self.Ts)
        # mag, phase, w2 = cs.freqresp(Gz, w)
        # G = mag[0][0] * np.exp(1j*np.unwrap(phase[0][0]))

        # Formulate open-loop model
        if self.control_mode == 'I':
            Delay_fgc_periods = self.prop['REG.I.INTERNAL.PURE_DELAY_PERIODS']
            T_CTRL = self.prop['REG.I.PERIOD_ITERS'] * self.Ts_fund

            # Michele's attempt
            G_no_delay_discrete = cs.c2d(G_no_delay_cont, T_CTRL)

            # Michele's attempt
            mag, phase, w2 = cs.freqresp(G_no_delay_discrete, w)
            G = mag * np.exp(1j*np.unwrap(phase))

            Delay_fgc = Delay_fgc_periods * T_CTRL
            # Approximate correction for "missing" ZOH - not accurate
            # Correction_ZOH = (1 - np.exp(-1j * w * T_CTRL))/(1j * w * T_CTRL)
            if Delay_fgc == 0:
                # Achille's code + Michele ZOH correction
                # G = M * Vs * np.exp(-1j * w * Delay_total) * Correction_ZOH
                
                # Michele's attemp
                G *= np.exp(-1j * w * Delay_total)
            elif Delay_fgc > 0:
                # Achille's code + Michele ZOH correction
                # G = M * Vs * np.exp(-1j * w * Delay_fgc) * Correction_ZOH
                
                # Michele's attemp
                G *= np.exp(-1j * w * Delay_fgc) 
            else:
                raise Exception('ERROR:REG.I.INTERNAL.PURE_DELAY_PERIODS must be positive.')
        elif self.control_mode == 'B':
            Delay_fgc_periods = self.prop['REG.B.INTERNAL.PURE_DELAY_PERIODS']
            T_CTRL = self.prop['REG.B.PERIOD_ITERS'] * self.Ts_fund
            Delay_fgc = Delay_fgc_periods * T_CTRL
            
            # Michele's attemp
            G_no_delay_discrete = cs.c2d(G_no_delay_cont, T_CTRL)
            mag, phase, w2 = cs.freqresp(G_no_delay_discrete, w)
            G = mag * np.exp(1j*np.unwrap(phase))

            # Approximate correction for "missing" ZOH - not accurate
            # Correction_ZOH = (1 - np.exp(-1j * w * T_CTRL))/(1j * w * T_CTRL)
            if Delay_fgc == 0:
                # Achille's code + Michele ZOH correction
                # G = self.prop['LOAD.GAUSS_PER_AMP'] * M * Vs * np.exp(-1j * w * Delay_total) * Correction_ZOH
                
                # Michele's attemp
                G *= self.prop['LOAD.GAUSS_PER_AMP'] * np.exp(-1j * w * Delay_total)
            elif Delay_fgc > 0:
                # Achille's code + Michele ZOH correction
                #G = self.prop['LOAD.GAUSS_PER_AMP'] * M * Vs * np.exp(-1j * w * Delay_fgc) * Correction_ZOH
                
                # Michele's attemp
                G *= self.prop['LOAD.GAUSS_PER_AMP'] * np.exp(-1j * w * Delay_fgc)
            else:
                raise Exception('ERROR:REG.B.INTERNAL.PURE_DELAY_PERIODS must be positive.')

        return G, w

    def model_frf_v(self, w_init, f_points): # this model is for voltage control

        # Initialize frequency grid
        w = np.logspace(np.log10(w_init), np.log10(np.pi / self.Ts_fund), f_points)

        # Calculate delays
        if self.prop['VS.ACTUATION'] == 'FIRING_REF':
            Delay_firing = self.prop['VS.FIRING.DELAY']
        else:
            Delay_firing = 0

        del_act = np.exp(-1j * w * (Delay_firing +
                self.prop['VS.ACT_DELAY_ITERS'] * self.Ts_fund))
        del_ADC_v = np.exp(-1j * w * self.prop['MEAS.V.DELAY_ITERS'] * self.Ts_fund)
        del_ADC_i = np.exp(-1j * w * self.prop['MEAS.I.DELAY_ITERS'] * self.Ts_fund)

        # Formulate models
        # ref_to_vmeas = F_REF_LIMITED --> V_MEAS_REG,
        # GHi = F_REF_LIMITED --> I_CAPA,
        # GMag = F_REF_LIMITED --> I_MEAS
        
        # Achille's code (upon which Michele's attempt is calculated)
        # Zd = self.prop['LOAD.OHMS_MAG'] + 1j * w * self.prop['LOAD.HENRYS']
        # M = 1 / (self.prop['LOAD.OHMS_SER'] + 1 / (1 / self.prop['LOAD.OHMS_PAR'] + 1 / Zd))
        # DC_mag = 1 / (self.prop['LOAD.OHMS_SER'] + self.prop['LOAD.OHMS_MAG'])

        # Zmt = 1 / M
        # Z1 = self.user_pars.rc + 1 / (1j * w * self.user_pars.c1)
        # Z2 = 1 / (1j * w * self.user_pars.c2)
        # Z3 = (Z1 * Z2) / (Z1 + Z2)
        # ZT = (Z3 * Zmt) / (Z3 + Zmt)

        # H_icappa = (1j * w) / (self.user_pars.rc * 1j * w + 1 / self.user_pars.c1)

        # G = (ZT / (self.user_pars.lf * 1j * w + ZT)) * del_act
        
        T = self.Ts_fund
        
        # Michele's attempt
        
        Rm = self.prop['LOAD.OHMS_MAG']
        Rs = self.prop['LOAD.OHMS_SER']
        Rp = self.prop['LOAD.OHMS_PAR']
        Rc = self.user_pars.rc
        Lm = self.prop['LOAD.HENRYS']
        Lf = self.user_pars.lf
        Rf = self.user_pars.rf
        C1 = self.user_pars.c1
        C2 = self.user_pars.c2

        DC_mag = 1/(Rs + ((Rp * Rm)/(Rp + Rm)))
 
        b = C1/(C1+C2)
        a = np.exp(-T/(b*C2*Rc))
 
        if a < 0.05:
            Rc = 0
 
        # G model

        # Numerator
        n0 = (Rm*Rp + Rm*Rs + Rp*Rs)
        n1 = (Lm*Rp + Lm*Rs + C1*Rc*Rm*Rp + C1*Rc*Rm*Rs + C1*Rc*Rp*Rs)
        n2 = (C1*Lm*Rc*Rp + C1*Lm*Rc*Rs)
        # Denominator
        d0 = (Rf*Rm + Rf*Rp + Rm*Rp + Rm*Rs + Rp*Rs)
        d1 = (Lf*Rm + Lm*Rf + Lf*Rp + Lm*Rp + Lm*Rs + C1*Rc*Rf*Rm + C1*Rc*Rf*Rp + C1*Rc*Rm*Rp + C1*Rc*Rm*Rs + C1*Rf*Rm*Rp + C2*Rf*Rm*Rp +
              C1*Rc*Rp*Rs + C1*Rf*Rm*Rs + C2*Rf*Rm*Rs + C1*Rf*Rp*Rs + C2*Rf*Rp*Rs)
        d2 = (Lf*Lm + C1*Lf*Rc*Rm + C1*Lm*Rc*Rf + C1*Lf*Rc*Rp + C1*Lm*Rc*Rp + C1*Lf*Rm*Rp + C1*Lm*Rc*Rs + C1*Lm*Rf*Rp  + C2*Lf*Rm*Rp +
              C2*Lm*Rf*Rp + C1*Lf*Rm*Rs + C1*Lm*Rf*Rs + C2*Lf*Rm*Rs + C2*Lm*Rf*Rs + C1*Lf*Rp*Rs + C2*Lf*Rp*Rs + C1*C2*Rc*Rf*Rm*Rp +
              C1*C2*Rc*Rf*Rm*Rs + C1*C2*Rc*Rf*Rp*Rs)
        d3 = (C1*Lf*Lm*Rc + C1*Lf*Lm*Rp + C2*Lf*Lm*Rp + C1*Lf*Lm*Rs + C2*Lf*Lm*Rs + C1*C2*Lf*Rc*Rm*Rp + C1*C2*Lm*Rc*Rf*Rp +
              C1*C2*Lf*Rc*Rm*Rs + C1*C2*Lm*Rc*Rf*Rs + C1*C2*Lf*Rc*Rp*Rs)
        d4 = (C1*C2*Lf*Lm*Rc*Rp + C1*C2*Lf*Lm*Rc*Rs)
       
        # Continuous time with no "pure" delay
        G_cont = cs.tf([n2,n1,n0],[d4,d3,d2,d1,d0])
        
        # Discretization (zoh by default)
        G_no_delay_discrete = cs.c2d(G_cont,T)
        
        # Approximate correction for the "missing" ZOH  <- Achille's code + ZOH not very accurate correction
        # Correction_ZOH = (1 - np.exp(-1j * w * T))/(1j * w * T)    
        # G = G * Correction_ZOH
        
        # Michele's attempt
        mag, phase, w2 = cs.freqresp(G_no_delay_discrete, w)
        G = mag * np.exp(1j*np.unwrap(phase))
        # The approximation is to calculate the FRF of the part with zeros and poles but no delay and then multiply by the delay part
        G *= del_act
        
          
        
        # GHi calculation
        # Numerator - it has the same denominator of G
        n0_GHi = 0 
        n1_GHi = (C1*Rm*Rp + C1*Rm*Rs + C1*Rp*Rs)
        n2_GHi = (C1*Lm*Rp + C1*Lm*Rs)

        GHi_cont = cs.tf([n2_GHi,n1_GHi,n0_GHi],[d4,d3,d2,d1,d0])
        GHi_no_delay_discrete = cs.c2d(GHi_cont,T)
        
        # Achille's code
        # GHi = G * H_icapa *del_act *del_ADC_i
       
        # Michele's attempt
        mag, phase, w2 = cs.freqresp(GHi_no_delay_discrete, w)
        GHi = mag * np.exp(1j*np.unwrap(phase))
        # The approximation is to calculate the FRF of the part with zeros and poles but no delay and then multiply by the delay part
        GHi *= del_act * del_ADC_i

        # Gmag calculation
        # Numerator - it has the same denominator of G
        n0_Gmag = (Rm + Rp)
        n1_Gmag = (Lm + C1*Rc*Rm + C1*Rc*Rp)
        n2_Gmag = (C1*Lm*Rc)
       
        # Achille's code
        # GMag = G * M * del_ADC_i
        
        # Michele's attempt
        GMag_cont = cs.tf([n2_Gmag,n1_Gmag,n0_Gmag],[d4,d3,d2,d1,d0]) 
        GMag_no_delay_discrete = cs.c2d(GMag_cont,T)
        
        mag, phase, w2 = cs.freqresp(GMag_no_delay_discrete, w)
        GMag = mag * np.exp(1j*np.unwrap(phase))
        # The approximation is to calculate the FRF of the part with zeros and poles but no delay and then multiply by the delay part
        GMag *= del_act * del_ADC_i
        
        # Gdel calculation
        
        # Achille's code
        # Gdel = G * del_ADC_v
        
        # Michele's attempt (in this case it is the same as Achille's above)
        Gdel = G * del_ADC_v
        
        return {
            'ref_to_icapa': GHi, 
            'ref_to_imeas': GMag, 
            'ref_to_vmeas': Gdel,
            'w': w, 
            'DC_mag': DC_mag
        }
