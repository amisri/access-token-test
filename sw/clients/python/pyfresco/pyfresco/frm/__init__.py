from .meas_avg import Frm_methods
from .props import UiParams, FgcProperties


from_fgc = FgcProperties().from_fgc
# prbs = Frm_methods().prbs
# sine_fit = Frm_methods().sine_fit
# sine_freq_array = Frm_methods().sine_freq_arrays

get_default_prbs = UiParams().get_default_prbs
get_default_sine = UiParams().get_default_sine