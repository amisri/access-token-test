
dl = 1e-5
limits_prbs = {'REF.PRBS.NUM_SEQUENCES': [6, 50], 'REF.PRBS.AMPLITUDE_PP': [dl, 10**6],
        'REF.PRBS.PERIOD_ITERS': [1, 100], 'REF.PRBS.K': [8, 20]}

limits_sine = {'reg_mode': 'V', 'ref_mode': 'V_REF_(V)', 'meas_mode': 'I_MEAS_(A)',
        'num_freq': [2, 10**4]}