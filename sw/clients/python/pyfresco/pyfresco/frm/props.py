import pyfgc


class UiParams:
    """
    A class which contains the default values to be used for the FRM UI.
    """
    def __init__(self):
        self.reg_mode = None
        self.ref_mode = None
        self.meas_mode = None
        self.amplitude_pp = None
        self.num_sequences = None
        self.num_freq = None


    @staticmethod
    def get_default_prbs(device: str, rbac_token: str = None, control_mode='V'):
        """
        :param device: FGC device name.
        :type device: str
        :param rbac_token: RBAC token needed to access the FGC properties.
        :type rbac_token: str
        :param control_mode: Select FGC properties based on loop that is controlled. For current
                             control, ``I``; For field control, ``B``.
        :type control_mode: str
        :return: Object which contains the UI paramater default values
                 (for PRBS measurememt).
        :rtype: class
        """

        fgc = vars(FgcProperties.from_fgc(device, rbac_token, control_mode=control_mode))
        if control_mode == 'V':
            Ts = fgc['FGC.ITER_PERIOD']
            input, output = 'V_REF', 'I_MEAS'
        elif control_mode in ['I', 'B']:
            Ts = fgc['FGC.ITER_PERIOD'] * fgc[f'REG.{control_mode}.PERIOD_ITERS']
            input, output = control_mode + '_REF_ADV', control_mode + '_MEAS_REG'
        k, period_iters = 12, 1
        fmin = 1 / (Ts * (2 ** k - 1))
        fmax = 1 / (2 * Ts * period_iters)

        if fgc[f'LIMITS.{control_mode}.RATE'] > 0:
            amp_pp = round(0.5 * fgc[f'LIMITS.{control_mode}.RATE'] * Ts, 3)
        elif fgc[f'LIMITS.{control_mode}.POS'] > 0:
            amp_pp = round(0.2 * fgc[f'LIMITS.{control_mode}.POS'], 3)
        else:
            amp_pp = 1

        default = UiParams()
        props_prbs = {'reg_mode': control_mode, 'ref_mode': input, 'meas_mode': output,
        'f_min': round(fmin, 3) + 1e-3, 'f_max': int(fmax), 'amplitude_pp': amp_pp,  'num_sequences': int(12)}
        for prop in props_prbs:
            setattr(default, prop, props_prbs[prop])
        return default

    @staticmethod
    def get_default_sine(device: str, rbac_token: str = None, control_mode='V'):
        """
        :param device: FGC device name.
        :type device: str
        :param rbac_token: RBAC token needed to access the FGC properties.
        :type rbac_token: str
        :param control_mode: Select FGC properties based on loop that is controlled. For current
                             control, ``I``; For field control, ``B``.
        :type control_mode: str
        :return: Object which contains the UI paramater default values
                 (for sine measurememt).
        :rtype: class
        """
        fgc = vars(FgcProperties.from_fgc(device, rbac_token, control_mode=control_mode))

        if control_mode == 'V':
            Ts = fgc['FGC.ITER_PERIOD']
            input, output = 'V_REF', 'I_MEAS'
        elif control_mode in ['I', 'B']:
            Ts = fgc['FGC.ITER_PERIOD'] * int(fgc[f'REG.{control_mode}.PERIOD_ITERS'])
            input, output = control_mode + '_REF_ADV', control_mode + '_MEAS_REG'
        else:
            raise Exception('Invalid character set for reg_mode.')

        fmax = int(0.1 / Ts)
        if fgc[f'LIMITS.{control_mode}.RATE'] > 0:
            amp_limit = round(fgc[f'LIMITS.{control_mode}.RATE'] / (2 * 3.14159 * fmax), 3)
            if fgc[f'LIMITS.{control_mode}.POS'] > 0 and amp_limit < fgc[f'LIMITS.{control_mode}.POS']:
                amp = amp_limit
            elif fgc[f'LIMITS.{control_mode}.POS'] > 0 and amp_limit >= fgc[f'LIMITS.{control_mode}.POS']:
                amp = round(0.2 * fgc[f'LIMITS.{control_mode}.POS'], 3)
        elif fgc[f'LIMITS.{control_mode}.POS'] > 0:
            amp = round(0.2 * fgc[f'LIMITS.{control_mode}.POS'], 3)
        else:
            amp = 1

        default = UiParams()
        props_sine = {'reg_mode': control_mode, 'ref_mode': input, 'meas_mode': output,
                    'num_freq': int(200), 'f_min': 0.025, 'f_max': fmax, 'amplitude_pp': amp}
        for prop in props_sine:
            setattr(default, prop, props_sine[prop])
        return default


class FgcProperties:
    """
    A class to send and extract the FGC device property values needed for the FRM package.

    """

    prop_name_v = ['FGC.ITER_PERIOD', 'LIMITS.{control_mode}.RATE', 'LIMITS.{control_mode}.POS',
                   'LIMITS.{control_mode}.NEG', 'REF.PRBS.PERIOD_ITERS',
                    'REF.PRBS.K', 'REF.PRBS.AMPLITUDE_PP', 'REF.PRBS.NUM_SEQUENCES',
                    'LOAD.SELECT']


    def __init__(self):
        pass

    @staticmethod
    def from_fgc(device: str, rbac_token: str = None, control_mode='V'):
        """
        :param device: FGC device name.
        :type device: str
        :param rbac_token: RBAC token needed to access the FGC properties.
        :type rbac_token: str
        :param control_mode: Select FGC properties based on loop that is controlled. For current
                             control, ``I``; For field control, ``B``.
        :type control_mode: str
        :return: Object which contains the FGC properties and values needed for
                 the FRM measurements.
        """
        def hasNumbers(inputString):
            return any(char.isdigit() for char in inputString)

        fgc_props = FgcProperties()
        ind = int(pyfgc.get(device, 'LOAD.SELECT', rbac_token=rbac_token).value)

        if control_mode == 'V':
            prop_name = fgc_props.prop_name_v
        elif control_mode in ['I', 'B']:
            prop_name = fgc_props.prop_name_v + ['REG.{control_mode}.PERIOD_ITERS']
        else:
            raise Exception('Control mode must be V, I, or B string.')

        for prop_pattern in prop_name:
            prop = prop_pattern.format(control_mode=control_mode)
            r = pyfgc.get(device, prop, rbac_token=rbac_token)
            p_val_all = r.value.split(',')

            if len(p_val_all) > 1 and hasNumbers(p_val_all[0]):
                setattr(fgc_props, prop, float(p_val_all[ind]))
            elif len(p_val_all) > 1 and not hasNumbers(p_val_all[0]):
                setattr(fgc_props, prop, p_val_all[ind])

            if len(p_val_all) == 1 and hasNumbers(p_val_all[0]):
                setattr(fgc_props, prop, float(p_val_all[0]))
            elif len(p_val_all) == 1 and not hasNumbers(p_val_all[0]):
                setattr(fgc_props, prop, p_val_all[0])


        return fgc_props