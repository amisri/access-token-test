VERSION 1.7.2; 29.03.2023
    - Properly keep track of 'last_time_read_keep'. Also add support for empty keep file (would previously fail as 'keepfile not read').
VERSION 1.7.1; 27.03.2023
    - Add keep file path to args of standalone entrypoint
VERSION 1.7.0; 16.01.2023
    - [EPCCCS-9412] Fix subdevice name values to parse subdev_idx to str when concatenating.

VERSION 1.6.0; 01.12.2022
    - [EPCCCS-8820] Force use of pyfgc-const>=v0.0.5 which comes with a FGC_MAX_DEV_LEN=31 instead of 23. This is needed because of sanity name length checks when parsing the name file.

VERSION 1.5.3; 29.07.2022
    - [EPCCCS-9412] Add subdevice["name"] field whose value is equivalent to "parent-device-name:sub-device-index"

VERSION 1.5.2; 10.03.2022
    - [EPCCCS-9157] Fixed: the effects of `read_group_file` must not be negated when the namefile is re-read

VERSION 1.5.1; 06.01.2022
    - Added functions `get_fgcs_from_class`, `count_fgcs_from_classes`

VERSION 1.5.0; 01.12.2021
    - Add support for optional gw_comment in groups file to pyfgc_name python module

VERSION 1.4.4; 02.11.2021
    - Fix unit tests, to re-include it in the release pipeline. Using fix mockup of the namefile, to avoid breaking assertion tests when live namefile changes.

VERSION 1.4.1; 02.11.2021
    - Include the package in gitlab release pipeline again

VERSION 1.4.0; 02.11.2021
    - [EPCCCS-8803] Implement support for fgc keep file

VERSION 1.3.2; 01.06.2020
    - [EPCCCS-8070] Include filterfunction as argument for 'get_fgcs_for_group'

VERSION 1.3.1; 28.05.2020
    - [EPCCCS-xxxx] Fixed FGC name regex bug

VERSION 1.3.0; 22.05.2020
    - [EPCCCS-xxxx] Adapt package to acc-py. Include utils module

VERSION 1.2.0
    - [EPCCCS-7425] Include build_device_tset function
    - [EPCCCS-7455] Rename package to pyfgc_name
