pyfgc\_name package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyfgc_name.tests

Submodules
----------

pyfgc\_name.pyfgc\_name module
------------------------------

.. automodule:: pyfgc_name.pyfgc_name
   :members:
   :undoc-members:
   :show-inheritance:

pyfgc\_name.utils module
------------------------

.. automodule:: pyfgc_name.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyfgc_name
   :members:
   :undoc-members:
   :show-inheritance:
