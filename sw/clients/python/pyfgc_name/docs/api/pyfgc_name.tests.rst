pyfgc\_name.tests package
=========================

Submodules
----------

pyfgc\_name.tests.test\_pyfgc\_name module
------------------------------------------

.. automodule:: pyfgc_name.tests.test_pyfgc_name
   :members:
   :undoc-members:
   :show-inheritance:

pyfgc\_name.tests.test\_pyfgc\_name\_basic module
-------------------------------------------------

.. automodule:: pyfgc_name.tests.test_pyfgc_name_basic
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyfgc_name.tests
   :members:
   :undoc-members:
   :show-inheritance:
