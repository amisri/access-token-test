"""
Documentation for the pyfgc_name package

"""
from .pyfgc_name import devices, gateways, groups, keep_devices, keep_gateways
from .pyfgc_name import reset, read_name_file, read_subdevice_file, read_group_file, read_keep_file, read_subkeep_file, build_device_set, build_device_tset

from .utils import get_gws_for_group, get_fgcs_for_group, get_fgcs_from_class, count_fgcs_from_classes

from .__version__ import __version__, __emails__, __authors__
