import pathlib
import pytest
import re

import pyfgc_name

SCRIPT_DIR = str(pathlib.Path(__file__).parent.resolve())
MOCK_NAME_FILE = SCRIPT_DIR + "/mock_name_data"
MOCK_GROUP_FILE = SCRIPT_DIR + "/mock_group_data"


@pytest.fixture(autouse=True)
def reset_name_file():
    pyfgc_name.reset()


def extract_gws_from_groups(groups):
    """ Helper function to recursively iterate through the given groups dictionary, collecting all values hiding behind a 'gatways' dict key. """
    gws = set()
    if(not isinstance(groups, dict)):
        return gws
    for key, value in groups.items():
        if(key == "gateways"):
            gws.update(value)
        else:
            gws.update(extract_gws_from_groups(value))
    return gws

def test_data_structures_have_minimum_content():
    pyfgc_name.read_name_file()
    pyfgc_name.read_subdevice_file()
    pyfgc_name.read_group_file()
    assert len(pyfgc_name.gateways) > 200
    assert len(pyfgc_name.devices) > 4000
    assert len(pyfgc_name.groups) > 20

    gw_set_from_groups = extract_gws_from_groups(pyfgc_name.groups)
    assert len(pyfgc_name.gateways) == len(gw_set_from_groups)

def test_devices_can_be_read_from_default_name_file_parameter():
    pyfgc_name.read_name_file()
    assert len(pyfgc_name.devices) != 0

def test_devices_can_be_read_by_passing_name_file_absolute_path():
    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    assert len(pyfgc_name.devices) != 0

def test_devices_can_be_read_by_passing_name_file_url():
    pyfgc_name.read_name_file("file://" + MOCK_NAME_FILE)
    assert len(pyfgc_name.devices) != 0

def test_devices_can_be_read_by_passing_http_url():
    pyfgc_name.read_name_file("http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/name")
    assert len(pyfgc_name.devices) != 0

def test_non_existing_file():
    with pytest.raises(FileNotFoundError):
        pyfgc_name.read_name_file("/a/b/c/ddddsdf/s/321423/5/6c")

def test_build_device_set_from_simple_regex():
    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    fgcs = pyfgc_name.build_device_set("rpzes.866.15.eth1")
    assert len(fgcs) == 1

def test_build_device_set_from_regex():
    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    fgcs = pyfgc_name.build_device_set("rpz.*400.*rch.*")
    assert len(fgcs) == 31

def test_build_device_with_suffix():
    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    fgcs_0 = pyfgc_name.build_device_set("RPCEK.361.BT.RBHZ10")
    fgcs_1 = pyfgc_name.build_device_set("RPCEK.361.BT.RBHZ10.SP")
    assert len(fgcs_0) == 1
    assert len(fgcs_1) == 1
    assert fgcs_0.pop() != fgcs_1.pop()

def test_build_device_set_from_device_list():
    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    fgcs = pyfgc_name.build_device_set(['RFNA.866.01.ETH1', 'RFNA.866.02.ETH1'])
    assert len(fgcs) == 2

def test_build_device_set_from_filter_func():
    def my_filter(key, value):
            return value["channel"] == 10 and value["class_id"] == 91

    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    fgcs = pyfgc_name.build_device_set(my_filter)
    assert len(fgcs) ==  8

def test_build_device_set_from_filter_func_with_regex():
    def my_filter_regex(key, value):
        ba1s = re.compile("^.*BA1.*")
        return value["class_id"] == 91 and ba1s.match(key)

    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    fgcs = pyfgc_name.build_device_set(my_filter_regex)
    assert len(fgcs) == 29

# TODO: [Test] build_device_tset()
# TODO: [Test] invalid arguments for build_device_set() and build_device_tset()
# TODO: [Test] upper case conversion in build_device_set() and build_device_tset()
# TODO: [Test] KeyError in build_device_tset()
# TODO: [Fix] tests using callback functions

def test_devices_gateways_consistency():
    pyfgc_name.read_name_file()

    # Filter function
    def filter_fgcs(dev_dict):
        return dev_dict["gateway"] == "cfc-866-reth7"

    # Get devices controlled by certain gateway
    target_devices = list(filter(filter_fgcs, pyfgc_name.devices.values()))

    # Add one to the gateways list because the gateway is not included
    assert len(pyfgc_name.gateways["cfc-866-reth7"]["devices"]) + 1 == len(target_devices)


def test_namefile_reload_doesnt_drop_groups():
    """
    Regression test for https://issues.cern.ch/browse/EPCCCS-9157
    """

    import os

    pyfgc_name.read_name_file(MOCK_NAME_FILE)
    pyfgc_name.read_group_file(MOCK_GROUP_FILE)

    device_obj = pyfgc_name.devices["RFNA.866.17.ETH1"]
    pyfgc_name.gateways[device_obj["gateway"]]["groups"]

    # Pretend that file has been changed since we last read it
    pyfgc_name.pyfgc_name.last_time_read_name = os.path.getmtime(MOCK_NAME_FILE) - 3600
    pyfgc_name.read_name_file(MOCK_NAME_FILE)

    device_obj = pyfgc_name.devices["RFNA.866.17.ETH1"]
    pyfgc_name.gateways[device_obj["gateway"]]["groups"]  # must not fail
