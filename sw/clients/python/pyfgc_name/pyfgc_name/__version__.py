__version__ = "1.7.2"

__authors__ = """
    Carlos Ghabrous Larrea,
    Marc Magrans de Abril,
    Joao Afonso,
    Kevin Kessler
"""

__emails__ = """
    kevin.timo.kessler@cern.ch
"""