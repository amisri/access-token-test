from pathlib import Path
from setuptools import setup, find_packages

HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()


about = {}
with (HERE / 'pyfgc_log' / '__version__.py').open() as f:
    exec(f.read(), about)


requirements = {
    'core': [],
    'test': [
        'pytest',
    ],
    'doc': [
        'sphinx',
        'sphinx_rtd_theme',
    ],
}

setup(
	name = "pyfgc_log",
	version = about['__version__'],
	author = about['__authors__'],
	author_email = about['__emails__'],
	description = LONG_DESCRIPTION,
	project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/pyfgc_log/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/pyfgc_log',
    },
	python_requires=">=3.7",
	packages = find_packages(),
	install_requires=requirements['core'],
    extras_require={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test']
)
