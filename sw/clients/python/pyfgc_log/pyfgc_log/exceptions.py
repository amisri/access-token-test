class LogException(Exception):

    def __init__(self, message):
        Exception.__init__(self)
        self.__message = message

    @property
    def message(self):
        return self.__message


class UnsuccessfulAction(LogException):

    def __init__(self, error_message):
        message = 'Action was unsuccessful: %s' % error_message
        LogException.__init__(self, message)


class BufferLengthMismatch(LogException):

    def __init__(self, expected_length, actual_length):
        message = 'Buffer length mismatch (expected: %d, got:%d)' % (expected_length,
                                                                     actual_length)
        LogException.__init__(self, message)


class UnknownBufferVersion(LogException):

    def __init__(self, version):
        message = 'Unknown buffer version : %s' % str(version)
        LogException.__init__(self, message)


class InvalidProperty(LogException):

    def __init__(self, prop):
        message = 'Unknown property : %s' % str(prop)
        LogException.__init__(self, message)


class InvalidSignalType(LogException):

    def __init__(self, signal_type, signal_name):
        message = 'Unknown signal type %s for signal %s' % (signal_type, signal_name)
        LogException.__init__(self, message)
