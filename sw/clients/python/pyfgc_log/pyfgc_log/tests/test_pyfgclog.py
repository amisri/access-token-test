import json
import pytest

from pathlib import Path


# Class 92.
V2_BUFFER_PATH = Path(__file__).parent.joinpath('mocks', 'buffer-v2.bin')

# Class 94.
V3_BUFFER_PATH = Path(__file__).parent.joinpath('mocks', 'buffer-v3.bin')

# Class 63.
LOG_MENU_PATH = Path(__file__).parent.joinpath('mocks', 'log_menu.txt')


# If install from wheel does not succeed the tests will be skiped.
if not V2_BUFFER_PATH.exists() or not V3_BUFFER_PATH.exists():
    pytest.skip('Skiping because cannot find the mock files', allow_module_level=True)


def test_decode_v2_header():
    from pyfgc_log import decode

    with open(V2_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 2)

        assert(decoded['version'] == 2)
        assert(decoded['meta']['little_endian'] == True)
        assert(decoded['meta']['analog_signals'] == True)
        assert(decoded['meta']['analog_signals_in_columns'] == False)
        assert(decoded['meta']['timestamp_is_lst'] == False)


def test_decode_v2():
    from pyfgc_log import decode

    with open(V2_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 2)

        assert('time_origin_seconds' in decoded)
        assert('time_origin_microseconds' in decoded)
        assert('first_sample_time_seconds' in decoded)
        assert('first_sample_time_microseconds' in decoded)
        assert('period_microseconds' in decoded)
        assert('epoch' not in decoded)

        signals = decoded['signals']
        first_signal = signals[0]

        assert(len(signals) == 3)
        assert('type' in first_signal)
        assert('name' in first_signal)
        assert('size' in first_signal)
        assert('offset' in first_signal)
        assert('samples' in first_signal)
        assert('digital_bit_index' in first_signal)
        assert('time_offset_microseconds' in first_signal)


def test_decode_v3_header():
    from pyfgc_log import decode

    with open(V3_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 3)

        assert(decoded['version'] == 3)
        assert(decoded['meta']['little_endian'] == True)
        assert(decoded['meta']['analog_signals'] == True)
        assert(decoded['meta']['analog_signals_in_columns'] == False)
        assert(decoded['meta']['timestamp_is_lst'] == False)

def test_decode_v3():
    from pyfgc_log import decode

    with open(V3_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 3)

        assert('time_origin_seconds' in decoded)
        assert('time_origin_nanoseconds' in decoded)
        assert('timestamp_seconds' in decoded)
        assert('timestamp_nanoseconds' in decoded)
        assert('period_seconds' in decoded)
        assert('period_nanoseconds' in decoded)

        signals = decoded['signals']
        first_signal = signals[0]

        assert(len(signals) == 3)
        assert('meta' in first_signal)
        assert('type' in first_signal)
        assert('name' in first_signal)
        assert('size' in first_signal)
        assert('offset' in first_signal)
        assert('samples' in first_signal)
        assert('digital_bit_index' in first_signal)
        assert('time_offset_nanoseconds' in first_signal)


def test_encode_v3():
    from pyfgc_log import decode, encode

    with open(V3_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 3)
        encoded_json_string = encode(decoded_buffer=decoded, version=3)
        encoded_json = json.loads(encoded_json_string)

        # Original TIME ORIGIN: 1669303400.473999977
        # Original FIRST SAMPLE TIME: 1669303370.475000000

        # At this level epoch is the min between time origin and first sample time i.e. 1669303370

        # time origin and first sample time should substract the integer part with epoch and the nanoseconds should remain the same.
        # time origin: 1669303400 - 1669303370 = 30.473999977
        # first sample time: 1669303370 - 1669303370 = 0.475000000

        assert(encoded_json.get('period') == 0.001)
        assert(encoded_json.get('epoch') == 1669303370)
        assert(float(format(encoded_json.get('timeOrigin'), '.9f')) == 30.473999977)
        assert(float(format(encoded_json.get('firstSampleTime'), '.9f')) == 0.475000000)

        signals = encoded_json.get('signals', [])
        first_signal = signals[0]

        assert(len(signals) == 3)
        assert('name' in first_signal)
        assert('timeOffset' in first_signal)
        assert('samples' in first_signal)
        assert(isinstance(first_signal['samples'], list))

def test_encode_v3_with_missing_time_origin():
    from pyfgc_log import decode, encode

    with open(V3_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 3)

        # Original TIMESTAMP: 1669303370.475000000 (first or last sample time)
        # If time origin is not privided, it should take either
        # the first sample time or the last sample time  (depending on cycling/non-cycling mode).

        decoded['time_origin_seconds'] = None
        decoded['time_origin_nanoseconds'] = None
        decoded['cycleSelector'] = 2 # Changin to anything which is not 0 or ALL

        encoded_json_string = encode(decoded_buffer=decoded, version=3, cycle_selector=2)
        encoded_json = json.loads(encoded_json_string)

        # If there is no time origin and no cycle, timeOrigin should be the same as first sample time.
        assert(float(format(encoded_json.get('timeOrigin'), '.9f')) == 0.475000000)

        # Now decoding without time_origin, but with cycle
        decoded['cycleSelector'] = '0'

        encoded_json_string = encode(decoded_buffer=decoded, version=3)
        encoded_json = json.loads(encoded_json_string)

        # If there is no time origin and but there is cycle, timeOrigin should be the same as first sample time + buffer duration.
        # buffer duration = (num samples - 1) * period => 29999 * 0.001 = 29.999
        # first_sample_time + buffer duration = 0.475000000 + 29.999 = 30.474000000
        assert(float(format(encoded_json.get('timeOrigin'), '.9f')) == 30.474000000)


def test_encode_v3_with_timestamp_is_lst_meta():
    from pyfgc_log import decode, encode

    with open(V3_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 3)

        # Original TIMESTAMP: 1669303370.475000000 (first or last sample time)
        # If buffer['meta']['timestamp_is_lst'] flag is True, then it means that this is the last sample time
        # And we have to do a calculation in order to know which is the first sample time
        # first_sample_time = original_first_simple_time - buffer_duration = 1669303370.475000000 - 29.999 = 1669303340.476000000
        # epoch = 1669303340, so first_sample_time -= epoch = 0.476000000

        decoded['meta']['timestamp_is_lst'] = True

        encoded_json_string = encode(decoded_buffer=decoded, version=3, cycle_selector=2)
        encoded_json = json.loads(encoded_json_string)

        assert(float(format(encoded_json.get('firstSampleTime'), '.9f')) == 0.476000000)


def test_encode_v3_with_zero_leading_timestamp_ns():
    from pyfgc_log import decode, encode

    with open(V3_BUFFER_PATH, mode='rb') as file:
        buffer = file.read()

        decoded = decode(buffer, 3)

        # If the ns are not 9 digits, they should be prefixed with 0 since the format at the end should be <%d.%09d>
        decoded["timestamp_seconds"] = 0
        decoded["timestamp_nanoseconds"] = 12000000
        decoded["time_origin_seconds"] = 0
        decoded["time_origin_nanoseconds"] = 412000

        encoded_json_string = encode(decoded_buffer=decoded, version=3, cycle_selector=0)
        encoded_json = json.loads(encoded_json_string)

        assert(float(format(encoded_json.get('firstSampleTime'), '.9f')) == 0.012000000)
        assert(float(format(encoded_json.get('timeOrigin'), '.9f')) == 0.000412000)


def test_parse_log_menu():
    from pyfgc_log import parse_log_menu

    with open(LOG_MENU_PATH, mode='r') as file:
        log_menu_str = file.read()

    parsed_log_menu = parse_log_menu(log_menu_str)

    assert('PERIOD' in parsed_log_menu['LOG_DATA']['I_REG'])
    assert(type(parsed_log_menu['LOG_DATA']['I_REG']['PERIOD']) == float)
    assert('PERIOD' in parsed_log_menu['LOG_DATA']['I_MEAS'])
    assert(type(parsed_log_menu['LOG_DATA']['I_MEAS']['PERIOD']) == float)
    assert('PERIOD' in parsed_log_menu['LOG_DATA']['CYCLES'])
    assert(type(parsed_log_menu['LOG_DATA']['CYCLES']['PERIOD']) == float)
