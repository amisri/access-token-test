# Centralized place for version tracking
__version__ = '1.4.2'


__authors__ = """
    Benjamin Raymond,
    Nuno Laurentino Mendes,
    Vasile Sambor,
"""


__emails__ = """
    benjamin.raymond@cern.ch,
    nuno.laurentino.mendes@cern.ch,
    vasile.sambor@cern.ch,
"""
