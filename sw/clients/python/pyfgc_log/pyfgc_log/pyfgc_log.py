from enum import Enum
import struct
import re
import json
from . import exceptions
from decimal import Decimal


JSON_CURRENT_VERSION = '3.0'

BUFFER_LITTLE_ENDIAN = 0x01
BUFFER_CYCLIC = 0x02
BUFFER_ANALOG = 0x04
BUFFER_POSTMORTEM = 0x08
BUFFER_CONTINUOUS = 0x10
BUFFER_FREEZABLE = 0x20
BUFFER_SUB_SAMPLED = 0x40

# V2 meta masks
BUFFER_LITTLE_ENDIAN_V2 = 0x01
BUFFER_ANA_SIGNALS_V2 = 0x02
BUFFER_ANA_SIGS_IN_COLUMNS_V2 = 0x04
BUFFER_TIMESTAMP_IS_LST = 0x08

SIGNAL_NAME_LEN = 24
SIGNAL_STEPS = 0x01
SIGNAL_SUB_SAMPLED = 0x08
SIGNAL_USE_SCALING = 0x02

EVENT_LOG_TOTAL_LENGTH = 76
EVENT_LOG_PROPERTY_LENGTH = 24
EVENT_LOG_VALUE_LENGTH = 36
EVENT_LOG_ACTION_LENGTH = 7
EVENT_LOG_STATUS_LENGTH = 1

# Legacy support for Spy v1
SIGNAL_NAME_LEN_V1 = 15
SIGNAL_UNIT_LEN_V1 = 7
SIGNAL_TYPE_FLOAT_V1 = 0
SIGNAL_TYPE_INT16S_V1 = 1
SIGNAL_TYPE_INT16U_V1 = 2

# Timing format
REGEX_FGCD_TIMING = re.compile(r'(\w+)\|(\d+)\|(\w+)$')

regex_escape_json = re.compile(r'(\/|\\|\")')
regex_config_set = re.compile(r'^(.*):(.*)')
regex_cycle_log = re.compile(
      r'^\s*(?P<supercycle_index>\d+)' \
    + r'\s+(?P<timestamp>\d+\.\d+)' \
    + r'\s+((?P<subdevice_index>\d+)\|)?(\s*(?P<cycle_index>\d+))' \
    + r'\s+(?P<status_code>\w+)' \
    + r'\s+(?P<function_type>\w+)' \
    + r'(\s+(?P<status_string>[\w ]+))?$'
)
regex_timing_log = re.compile(
      r'\s*(?P<timestamp>\d+\.\d+)' \
    + r'\s+((?P<subdevice_index>\d+)\|)?(\s*(?P<cycle_index>\d+))' \
    + r'\s+(?P<delay>\d+)' \
    + r'\s+(?P<type>[\w ]+)$'
)
regex_to_null_byte = re.compile(b'[^\x00]*')


class BufferVersion(Enum):
    V1 = 1
    V2 = 2
    V3 = 3
    EVENT_LOG = 101
    CONFIG_SET = 102
    CYCLE_LOG = 103
    TIMING_LOG = 104


def extract_buffer(response):
    """
    Extract the buffer from a FGC get response. Throws an error if an error occurred.

    :response: response from the gateway (from pyfgc)
    """

    # If response.value is accessed but a FGC error occurred, `response.value` will throw an exception
    if not response or not response.value:
        # If response.value is none, response is empty
        raise Exception('Empty response')
    
    # Response is a string -> Return the string directly
    if not isinstance(response.value, bytes):
        return response.value
    
    # Response is a byte sequence: Get the binary length (first 4 bytes) & extract buffer
    expected_length = struct.unpack('>L', response.value[:4])[0]
    buffer = response.value[4:]

    # Check that the buffer is of the correct length
    if expected_length != len(buffer):
        raise Exception('Buffer length mismatch: expected %d, got %d' % (expected_length, len(buffer)))

    return buffer


def extract_buffer_version(buffer):
    """
    Extract the log version from the binary buffer. Returns None if the version can not be inferred.

    :buffer: Extracted buffer
    """
    try:
        # struct.unpack will return an error if the byte can not be extracted
        # otherwise it returns a tuple with only one value, the first byte, which contains the buffer version
        return struct.unpack('>B', buffer[:1])[0]
    except struct.error:
        return None


def parse_timing(timing_raw):
    """
    Parses FGCD.TIMING.USERS for a given gateway.

    :timing_raw: Raw response from the gateway
    """
    domain = re.search(r'^(\w+)\.', timing_raw).group(1)
    cycles = []
    for element in timing_raw.split(','):
        result = REGEX_FGCD_TIMING.search(element)
        cycle_name = result.group(1)
        cycle = int(result.group(2))
        status = result.group(3) == 'ENABLED'
        cycles.insert(cycle, {'name': cycle_name, 'enabled': status})

    return {
        'domain': domain,
        'cycles': cycles
    }


def decode(raw_buffer, version):
    """
    Decode buffer acquired with function get.

    :raw_buffer: a dictionary (from get())
    :returns: a dictionary with decoded buffer and other buffer related data
    """

    if version is BufferVersion.CONFIG_SET.value:
        return raw_buffer
    elif version is BufferVersion.CYCLE_LOG.value:
        return raw_buffer
    elif version is BufferVersion.TIMING_LOG.value:
        return raw_buffer
    elif version is BufferVersion.EVENT_LOG.value:
        return _decode_event_log(raw_buffer)
    elif version is BufferVersion.V1.value:
        return _decode_buffer_v1(raw_buffer)
    elif version is BufferVersion.V2.value:
        return _decode_buffer_v2(raw_buffer)
    elif version is BufferVersion.V3.value:
        return _decode_buffer_v3(raw_buffer)
    else:
        raise exceptions.UnknownBufferVersion(version)


def is_cycle_selector_default(cycle_selector):
    return cycle_selector == '0' or cycle_selector == 'ALL'


def _decode_event_log(raw_buffer):
    """
    Decodes event log from binary buffer.

    :raw_buffer: the buffer (from get())
    :returns: a dictionary with decoded buffer
    """

    buffer_length = len(raw_buffer)
    buffer_offset = 0
    number_events = int(buffer_length / EVENT_LOG_TOTAL_LENGTH)

    events = []
    event_unpack_string = 'LL%ds%ds%ds%ds' % (EVENT_LOG_PROPERTY_LENGTH, EVENT_LOG_VALUE_LENGTH,
                                              EVENT_LOG_ACTION_LENGTH, EVENT_LOG_STATUS_LENGTH)

    for _ in range(number_events):
        event = {}
        right_pad = buffer_length - EVENT_LOG_TOTAL_LENGTH - buffer_offset

        unpack_string = '>%dx%s%dx' % (buffer_offset, event_unpack_string, right_pad)
        (
            event['time_seconds'],
            event['time_microseconds'],
            event['property'],
            event['value'],
            event['action'],
            event['status'],
        ) = struct.unpack(unpack_string, raw_buffer)  # yapf:disable

        event['property'] = regex_to_null_byte.search(event['property']).group(0).decode()
        event['value'] = regex_to_null_byte.search(event['value']).group(0).decode('utf-8', 'ignore')
        event['action'] = regex_to_null_byte.search(event['action']).group(0).decode()
        event['status'] = regex_to_null_byte.search(event['status']).group(0).decode()

        events.append(event)
        buffer_offset += EVENT_LOG_TOTAL_LENGTH

    return events


def _decode_buffer_v1(raw_buffer):
    """
    Decode device buffer (SPY header version 1).

    :raw_buffer: the buffer (from get())
    :returns: a dictionary with decoded buffer
    """

    [buffer, buffer_offset] = _decode_header_v1(raw_buffer)

    endian_character = '<' if buffer['meta']['little_endian'] else '>'
    sum_sample_size = 0

    for signal_index in range(buffer['number_signals']):
        sum_sample_size += buffer['signals'][signal_index]['sample_size']

    left_pad = 0

    format_string = (buffer['number_samples'] * '%f,')[:-1]

    for signal_index in range(buffer['number_signals']):
        signal = buffer['signals'][signal_index]

        if signal['type'] == SIGNAL_TYPE_FLOAT_V1:
            unpack_character = 'f'
        elif signal['type'] == SIGNAL_TYPE_INT16S_V1:
            unpack_character = 'h'
        elif signal['type'] == SIGNAL_TYPE_INT16U_V1:
            unpack_character = 'H'
        else:
            raise exceptions.InvalidSignalType(signal['type'], signal['name'])

        sample_offset = sum_sample_size - signal['sample_size']

        right_pad = sample_offset - left_pad

        unpack_string = '%s%dx' % (endian_character, buffer_offset) \
            + buffer['number_samples'] * ('%dx%s%dx' % (left_pad, unpack_character, right_pad))

        samples = struct.unpack(unpack_string, raw_buffer)

        if signal['gain'] != 1 or signal['offset'] != 0:
            samples = tuple(map(lambda x, s=signal: x * s['gain'] + s['offset'], samples))

        signal['samples'] = format_string % samples

        left_pad += signal['sample_size']

    return buffer


def _decode_header_v1(buffer):
    """
    Decode device buffer's header (SPY header version 1).

    :buffer: the buffer (from get())
    :returns: a dictionary with decoded buffer and other buffer related data
    """

    buffer_length = len(buffer)
    header = {}

    (header['version'], meta_data) = struct.unpack('>2B%dx' % (buffer_length - 2), buffer)

    # 2: 2 char (1B)
    buffer_offset = 2

    header['meta'] = {'little_endian': bool(meta_data & BUFFER_LITTLE_ENDIAN)}

    endian_character = '<' if header['meta']['little_endian'] else '>'

    (
        header['number_signals'],
        header['number_samples'],
        header['period_microseconds'],
        header['first_sample_time_seconds'],
        header['first_sample_time_microseconds'],
    ) = struct.unpack('%s2xH4L%dx'
                      % (endian_character,
                         buffer_length - buffer_offset - 18),
                      buffer)  # yapf: disable

    # 18: 1 short (2B) + 4 long (4B)
    buffer_offset += 18

    header['signals'] = []

    for _ in range(header['number_signals']):
        signal_header = {}
        (
            signal_header['type'],
            signal_meta_data,
            signal_header['time_offset_microseconds'],
            signal_header['gain'],
            signal_header['offset'],
            length_signal_name,
            signal_name,
            length_signal_unit,
            signal_unit,
        ) = struct.unpack('%s%dxHHlffB%dsB%ds%dx'
                          % (endian_character,
                             buffer_offset,
                             SIGNAL_NAME_LEN_V1,
                             SIGNAL_UNIT_LEN_V1,
                             buffer_length - buffer_offset - SIGNAL_NAME_LEN_V1 - SIGNAL_UNIT_LEN_V1 - 18),
                          buffer)  # yapf: disable

        signal_header['name'] = regex_to_null_byte.search(signal_name).group(
            0)[:length_signal_name].decode()
        signal_header['unit'] = regex_to_null_byte.search(signal_unit).group(
            0)[:length_signal_unit].decode()
        signal_header['meta'] = {'steps': bool(signal_meta_data & SIGNAL_STEPS)}

        if signal_header['type'] == SIGNAL_TYPE_FLOAT_V1:
            signal_header['sample_size'] = 4
        elif signal_header['type'] in (SIGNAL_TYPE_INT16U_V1, SIGNAL_TYPE_INT16S_V1):
            signal_header['sample_size'] = 2
        else:
            raise exceptions.InvalidSignalType(signal_header['type'], signal_header['name'])

        header['signals'].append(signal_header)

        # 40: 2 short (2B) + 1 long (4B) + 2 float (4B) + 1 byte (1B) + 16 char (1B) + 1 byte (1B) + 8 char (1B)
        buffer_offset += 40

    return [header, buffer_offset]


def _decode_buffer_v2(raw_buffer):
    """
    Decode device buffer (SPY header version 2).

    :raw_buffer: the buffer (from get())
    :returns: a dictionary with decoded buffer
    """

    (buffer, buffer_offset) = _decode_header_v2(raw_buffer)

    return _v2_and_v3_decoder(buffer, raw_buffer, buffer_offset)


def _decode_header_v2(buffer):
    """
    Decode device buffer's header (SPY header version 1).

    :buffer_data: a dictionary (from get())
    :returns: a dictionary with decoded buffer and other buffer related data
    """

    buffer_length = len(buffer)

    header = {}

    (header['version'], meta_data) = struct.unpack('>2B%dx' % (buffer_length - 2), buffer)

    # 2: 2 char (1B)
    buffer_offset = 2

    header['meta'] = {
        'little_endian': bool(meta_data & BUFFER_LITTLE_ENDIAN_V2),
        'analog_signals': bool(meta_data & BUFFER_ANA_SIGNALS_V2),
        'analog_signals_in_columns': bool(meta_data & BUFFER_ANA_SIGS_IN_COLUMNS_V2),
        'timestamp_is_lst': bool(meta_data & BUFFER_TIMESTAMP_IS_LST),
    }

    endian_character = '<' if header['meta']['little_endian'] else '>'

    (
        header['number_signals'],
        header['number_samples'],
        header['time_origin_seconds'],
        header['time_origin_microseconds'],
        header['first_sample_time_seconds'],
        header['first_sample_time_microseconds'],
        header['period_microseconds']
    ) = struct.unpack('%s2xH6L%dx'
                      % (endian_character,
                         buffer_length - buffer_offset - 26),
                      buffer)  # yapf: disable

    # 26: 1 short (2B) + 6 long (4B)
    buffer_offset += 26

    header['signals'] = []

    for _ in range(header['number_signals']):
        signal_header = {}
        unpack_str = '%s%dxBBBBffl%ds%dx' % (endian_character, buffer_offset, SIGNAL_NAME_LEN, buffer_length - buffer_offset - 40)
        (
            signal_meta_data,
            signal_header['digital_bit_index'],
            signal_header['type'],
            signal_header['size'],
            signal_header['gain'],
            signal_header['offset'],
            signal_header['time_offset_microseconds'],
            signal_name
        ) = struct.unpack(unpack_str, buffer)  # yapf: disable

        signal_header['name'] = regex_to_null_byte.search(signal_name).group(0).decode()

        signal_header['meta'] = {
            'sub_sampled': bool(signal_meta_data & SIGNAL_SUB_SAMPLED),
            'steps': bool(signal_meta_data & SIGNAL_STEPS),
            'use_scaling': bool(signal_meta_data & SIGNAL_USE_SCALING)
        }

        header['signals'].append(signal_header)

        # 40: 2 char (1B) + 10 empty (1B) + 1 long (4B) + 24 char (1B)
        buffer_offset += 40

    return [header, buffer_offset]


def _decode_buffer_v3(raw_buffer):
    """
    Decode device buffer (SPY header version 3).

    :raw_buffer: the buffer (from get())
    :returns: a dictionary with decoded buffer
    """

    (buffer, buffer_offset) = _decode_header_v3(raw_buffer)

    return _v2_and_v3_decoder(buffer, raw_buffer, buffer_offset)


def _v2_and_v3_decoder(buffer, raw_buffer, buffer_offset):
    endian_character = '<' if buffer['meta']['little_endian'] else '>'

    if buffer['meta']['analog_signals']:

        for signal_index in range(buffer['number_signals']):
            signal = buffer['signals'][signal_index]
            signal_sample_size = signal['size']
            sample_offset = signal_sample_size * (buffer['number_signals'] - 1)

            # find out unpack character
            if signal['type'] == SIGNAL_TYPE_FLOAT_V1:
                unpack_character = 'f'
            elif signal['type'] == SIGNAL_TYPE_INT16S_V1:
                unpack_character = 'h'
            elif signal['type'] == SIGNAL_TYPE_INT16U_V1:
                unpack_character = 'H'
            else:
                raise exceptions.InvalidSignalType(signal['type'], signal['name'])

            # Skip header
            unpack_string = '%s%dx' % (endian_character, buffer_offset)

            if buffer['meta']['analog_signals_in_columns']:
                # Build unpack string when data are stored in columns
                left_pad = signal_index * signal_sample_size
                right_pad = sample_offset - left_pad
                unpack_string += ('%dx%s%dx' % (left_pad, unpack_character, right_pad)) * buffer['number_samples']
                samples = struct.unpack(unpack_string, raw_buffer)
            else:
                # Build unpack string when data are stored in rows
                unpack_string += 'x' * buffer['number_samples'] * signal_sample_size * signal_index
                unpack_string += 'f' * buffer['number_samples']
                unpack_string += 'x' * buffer['number_samples'] * signal_sample_size * (buffer['number_signals'] - signal_index - 1)
                samples = struct.unpack(unpack_string, raw_buffer)

            if signal['meta']['use_scaling']:
                samples = tuple(map(lambda x, s=signal: x * s['gain'] + s['offset'], samples))

            format_string = ('%.7E,' * buffer['number_samples'])[:-1]
            signal['samples'] = format_string % samples
    else:

        # Skip header
        unpack_string = '%s%dx' % (endian_character, buffer_offset)

        # Unpack the binary data (N longs)
        unpack_string += '%dl' % buffer['number_samples']
        samples = struct.unpack(unpack_string, raw_buffer)

        for signal_index in range(buffer['number_signals']):
            signal = buffer['signals'][signal_index]
            digital_bit_mask = 1 << signal['digital_bit_index']
            signal['samples'] = ','.join(list(map(lambda x, y=digital_bit_mask: '1' if x & y else '0', samples)))

    return buffer


def _decode_header_v3(buffer):
    """
    Decode device buffer's header (SPY header version 3).

    :buffer_data: a dictionary (from get())
    :returns: a dictionary with decoded buffer and other buffer related data
    """

    buffer_length = len(buffer)

    header = {}

    (header['version'], meta_data) = struct.unpack('>2B%dx' % (buffer_length - 2), buffer)

    # 2: 2 char (1B)
    buffer_offset = 2

    header['meta'] = {
        'little_endian': bool(meta_data & BUFFER_LITTLE_ENDIAN_V2),
        'analog_signals': bool(meta_data & BUFFER_ANA_SIGNALS_V2),
        'analog_signals_in_columns': bool(meta_data & BUFFER_ANA_SIGS_IN_COLUMNS_V2),
        'timestamp_is_lst': bool(meta_data & BUFFER_TIMESTAMP_IS_LST),
    }

    endian_character = '<' if header['meta']['little_endian'] else '>'

    (
        header['number_signals'],
        header['number_samples'],
        header['time_origin_seconds'],
        header['time_origin_nanoseconds'],
        header['timestamp_seconds'],
        header['timestamp_nanoseconds'],
        header['period_seconds'],
        header['period_nanoseconds']
    ) = struct.unpack('%s2xH7L%dx'
                      % (endian_character,
                         buffer_length - buffer_offset - 30),
                      buffer)  # yapf: disable

    # 30: 1 short (2B) + 7 long (4B)
    buffer_offset += 30

    header['signals'] = []

    for _ in range(header['number_signals']):
        signal_header = {}
        unpack_str = '%s%dxBBBBffl%ds%dx' % (endian_character, buffer_offset, SIGNAL_NAME_LEN, buffer_length - buffer_offset - 40)
        (
            signal_meta_data,
            signal_header['digital_bit_index'],
            signal_header['type'],
            signal_header['size'],
            signal_header['gain'],
            signal_header['offset'],
            signal_header['time_offset_nanoseconds'],
            signal_name
        ) = struct.unpack(unpack_str, buffer)  # yapf: disable

        signal_header['name'] = regex_to_null_byte.search(signal_name).group(0).decode()

        signal_header['meta'] = {
            'sub_sampled': bool(signal_meta_data & SIGNAL_SUB_SAMPLED),
            'steps': bool(signal_meta_data & SIGNAL_STEPS),
            'use_scaling': bool(signal_meta_data & SIGNAL_USE_SCALING)
        }

        header['signals'].append(signal_header)

        # 40: 2 char (1B) + 10 empty (1B) + 1 long (4B) + 24 char (1B)
        buffer_offset += 40

    return [header, buffer_offset]


def extract_payload_and_version(log_property, response):
    """
    Analyses the response, to extract the payload and version.

    :log_property: device property as a string
    :response: response from the gateway (from pyfgc.fgctcp get() or set())
    :returns: (
        :the buffer as a string
        :the version
        )
    """

    version = None

    # CONFIG.SET
    if re.match(r'^CONFIG\.SET$', log_property):
        buffer = response
        version = BufferVersion.CONFIG_SET.value
        return (buffer, version)

    # LOG.CYCLES
    if re.match(r'^LOG\.CYCLES$', log_property):
        buffer = response
        version = BufferVersion.CYCLE_LOG.value
        return (buffer, version)

    # LOG.TIMING
    if re.match(r'^LOG\.TIMING$', log_property):
        buffer = response
        version = BufferVersion.TIMING_LOG.value
        return (buffer, version)

    # Other properties
    if re.match(r'^LOG\.CAPTURE.+', log_property):
        pass
    elif re.match(r'^LOG\.SPY.+', log_property):
        pass
    elif re.match(r'^LOG\.(PM|DIM).+', log_property):
        # LOG.PM, LOG.DIM
        pass
    elif re.match(r'^LOG\.(MUGEF|ISEG).+', log_property):
        # LOG.MUGEF, LOG.ISEG
        pass
    elif re.match(r'^LOG.EVT$', log_property):
        # LOG.EVT
        version = BufferVersion.EVENT_LOG.value
    else:
        raise exceptions.InvalidProperty(log_property)

    buffer = get_payload_from_binary_response(response)
    if version is None:
        version = struct.unpack('>B%dx' % (len(buffer) - 1), buffer)[0]

    return (buffer, version)


def get_payload_from_binary_response(response):
    """
    Parses the payload out of the response.

    :response: response from the gateway (from pyfgc.fgctcp get() or set())
    :returns: payload as bytes or string depending on the response
    """

    if not isinstance(response, bytes):
        raise exceptions.UnsuccessfulAction(response)

    expected_payload_length = struct.unpack('>L', response[:4])[0]
    payload = response[4:]
    if expected_payload_length != len(payload):
        raise exceptions.BufferLengthMismatch(expected_payload_length, len(payload))

    return payload


def get_log_read_options(log_property):
    """
    Retrieves the list of log options required to read a log, based on its name.

    :log_property: device property as a string
    :returns: list of of get options, as a list of strings
    """
    get_options = []

    if re.match("LOG\.", log_property):
        get_options.append("BIN")

    if re.match("LOG.SPY\.", log_property):
        get_options.append("DATA")

    return get_options


def get_log_type(decoded_buffer, version=None):
    """
    Checks the log type.

    :decoded_buffer: decoded buffer data
    :version: version
    :returns: string, describing type of log
    """

    # Infer table types based on version
    if version == BufferVersion.EVENT_LOG.value:
        return "event"
    if version == BufferVersion.CYCLE_LOG.value:
        return "cycle"
    if version == BufferVersion.TIMING_LOG.value:
        return "timing"
    if version == BufferVersion.CONFIG_SET.value:
        return "table"
    
    # If string -> table
    if isinstance(decoded_buffer, str):
        return "table"
    
    # If list -> event logs
    if isinstance(decoded_buffer, list):
        return "event"
    
    # Digital
    if version != BufferVersion.V1.value and ("analog_signals" not in decoded_buffer["meta"] or not decoded_buffer["meta"]["analog_signals"]):
        return "digital"
    
    # Analog
    return "analog"


def parse_log_menu(log_menu_str):
    """
    Parses the log menu info.
    Returns a dictionary with decoded info.
    Logs are accessible by key, under the 'LOG_DATA' key.

    :log_menu_str: received log menu data, as string
    :returns: dictionary containing decoded data
    """

    log_menu_parsed = dict()

    for line in log_menu_str.split('\n'):
        key, value = line.split(':')
        log_menu_parsed[key] = value.split(',')

    # Log menu properties that only have 1 item - Represented as string
    for entry in {'MODE_PC_ON',}:
        try:
            log_menu_parsed[entry] = log_menu_parsed[entry][0]
        except KeyError:
            pass

    # Log menu properties that only have 1 item - Represented as float
    for entry in {'PM_TIME', 'SA_TIME'}:
        try:
            log_menu_parsed[entry] = float(log_menu_parsed[entry][0])
        except KeyError:
            pass

    log_data = dict()
    parsing_funcs = {
        'PROP'          : lambda log_menu, index: log_menu['PROPS'][index],
        'PERIOD'        : lambda log_menu, index: float(log_menu['PERIOD'][index]),
        'STATUS'        : lambda log_menu, index: list(log_menu['STATUS'][index].split()),
        'SIGNALS'       : lambda log_menu, index: list(log_menu['SIGNALS'][index].split()),
        'MPX_SIG_NAMES' : lambda log_menu, index: list(log_menu['MPX_SIG_NAMES'][index].split()),
    }

    for index, name in enumerate(log_menu_parsed['NAMES']):
        data = dict()
        for field, func in parsing_funcs.items():
            try:
                data[field] = func(log_menu_parsed, index)
            except (KeyError, IndexError):
                pass
        log_data[name] = data

    log_menu_parsed['LOG_DATA'] = log_data
    return log_menu_parsed


def encode(
    decoded_buffer,
    version,
    device=None,
    subdevices=None,
    log_name=None,
    cycle_selector='0',
    timing=None
):
    """
    Encodes the decoded buffer to specified format.

    :decoded_buffer: decoded buffer as dict (from decode())
    :version: the version of the buffer
    :device: the name of the device
    :subdevices: list of subdevices as defined in pyfgc_name (Only needed for timing logs)
    :log_name: the log property of the buffer
    :cycle_selector: the cycle selector
    :timing: timing data as parsed by parse_timing (Only needed for timing logs)
    :returns: string containing buffer data in specified format
    """

    if version is BufferVersion.CONFIG_SET.value:
        return _encode_config_set_json(decoded_buffer, device, log_name)
    elif version is BufferVersion.CYCLE_LOG.value:
        return _encode_cycle_log_json(decoded_buffer, device, log_name, subdevices, timing)
    elif version is BufferVersion.TIMING_LOG.value:
        return _encode_timing_log_json(decoded_buffer, device, log_name, subdevices, timing)
    elif version is BufferVersion.EVENT_LOG.value:
        return _encode_event_log_json(decoded_buffer, device, log_name)
    elif version in (BufferVersion.V1.value, BufferVersion.V2.value):
        return _encode_signal_buffer_json(decoded_buffer, version, device, log_name, cycle_selector)
    elif version == BufferVersion.V3.value:
        return _encode_signal_buffer_json_v3(decoded_buffer, device, log_name, cycle_selector)
    else:
        raise exceptions.UnknownBufferVersion(version)


def _encode_config_set_json(buffer, device, log_name):
    """
    Compose JSON string for CONFIG.SET property.

    :buffer: decoded buffer as dict (from decode())
    :device: the name of the device
    :log_name: the log property of the buffer
    :returns: string containing buffer data in PowerSpy JSON format.
    """

    device = device if device is not None else "unknown"
    log_name = log_name if log_name is not None else "unknown"

    output = ''

    output += '{"source":"FGC",'
    output += '"device":"%s",' % device
    output += '"name": "%s",' % log_name
    output += '"type":"table",'
    output += '"subtype":"default",'
    output += '"version":"%s",' % JSON_CURRENT_VERSION
    output += '"table":{'
    output += '"headings":{"cells":["Property","Value"]},'
    output += '"rows":['

    for line in buffer.split('\n'):
        if not line:
            continue

        line = _escape_json(line)
        regex_result = regex_config_set.search(line)
        output += '{"cells":["%s","%s"]},' % (regex_result.group(1), regex_result.group(2))

    output = '%s]}' % output[:-1]

    output += '}'

    return output


def _encode_cycle_log_json(buffer, device, log_name, subdevices=None, timing=None):
    """
    Compose JSON string for CYCLE.LOG property.

    :buffer: decoded buffer as dict (from decode())
    :device: the name of the device
    :log_name: the log property of the buffer
    :subdevices: device subdevices (used to map the subdevice index to its name)
    :timing: the timing data for the gateway domain, as decoded by parse_timing
    :returns: string containing buffer data in PowerSpy JSON format.
    """

    device = device if device is not None else "unknown"
    log_name = log_name if log_name is not None else "unknown"

    output = ''

    output += '{"source":"FGC",'
    output += '"device":"%s",' % device
    output += '"name": "%s",' % log_name
    output += '"type":"table",'
    output += '"subtype":"cycle-log",'
    output += '"version":"%s",' % JSON_CURRENT_VERSION
    output += '"table":{'
    output += '"rows":['

    for line in buffer.split(','):

        # Skip empty lines
        if not line:
            continue

        # Parse line
        line = _escape_json(line)
        regex_result = regex_cycle_log.search(line)
        if not regex_result:
            continue
        values = regex_result.groupdict()

        # Get subdevice string
        subdevice_index = int(values['subdevice_index'] or '0')
        subdevice_name = subdevices[subdevice_index]['alias'] if (subdevices and subdevice_index >= 0 and subdevice_index < len(subdevices)) else None
        subdevice = subdevice_name or (device + ':' + str(subdevice_index)) if subdevice_index > 0 else device

        # Build cycle label
        cycle_index = int(values['cycle_index'])
        cycle_name = timing['cycles'][cycle_index]['name'] if (timing and timing.get('cycles') and 0 <= cycle_index < len(timing['cycles'])) else None
        cycle_label = ('%2s.%s' % (cycle_index, cycle_name)) if cycle_name else str(cycle_index)

        # Build row
        output += '{"timestamp":%f,"cells":["%s","%d","%s","%s","%s","%s"]},' % (
            # Row timestamp
            float(values['timestamp']),
            # Cells
            values['status_code'],
            int(values['supercycle_index']),
            subdevice,
            cycle_label,
            values['function_type'],
            values['status_string'] or '',
        )

    output = (output[:-1] if output[-1] == ',' else output) + ']}'
    output += '}'

    return output


def _encode_timing_log_json(buffer, device, log_name, subdevices=None, timing=None):
    """
    Compose JSON string for LOG.TIMING property.

    :buffer: decoded buffer as dict (from decode())
    :device: the name of the device
    :log_name: the log property of the buffer
    :subdevices: device subdevices (used to map the subdevice index to its name)
    :timing: the timing data for the gateway domain, as decoded by parse_timing
    :returns: string containing buffer data in PowerSpy JSON format.
    """

    device = device if device is not None else "unknown"
    log_name = log_name if log_name is not None else "unknown"

    output = ''

    output += '{"source":"FGC",'
    output += '"device":"%s",' % device
    output += '"name": "%s",' % log_name
    output += '"type":"table",'
    output += '"subtype":"timing-log",'
    output += '"version":"%s",' % JSON_CURRENT_VERSION
    output += '"table":{'
    output += '"rows":['

    for line in buffer.split(','):

        # Skip empty lines
        if not line:
            continue

        # Parse line
        line = _escape_json(line)
        regex_result = regex_timing_log.search(line)
        if not regex_result:
            continue
        values = regex_result.groupdict()
        
        # Get subdevice string
        subdevice_index = int(values['subdevice_index'] or '0')
        subdevice_name = subdevices[subdevice_index]['alias'] if (subdevices and subdevice_index >= 0 and subdevice_index < len(subdevices)) else None
        subdevice = subdevice_name or (device + ':' + str(subdevice_index)) if subdevice_index > 0 else device

        # Build cycle label
        cycle_index = int(values['cycle_index'])
        cycle_name = timing['cycles'][cycle_index]['name'] if (timing and timing.get('cycles') and 0 <= cycle_index < len(timing['cycles'])) else None
        cycle_label = ('%2s.%s' % (cycle_index, cycle_name)) if cycle_name else str(cycle_index)

        # Build row
        output += '{"timestamp":%f,"cells":["%s","%s","%d","%s"]},' % (
            # Row timestamp
            float(values['timestamp']),
            # Cells
            subdevice,
            cycle_label,
            int(values['delay']),
            values['type'],
        )

    output = (output[:-1] if output[-1] == ',' else output) + ']}'
    output += '}'

    return output


def _encode_event_log_json(buffer, device, log_name):
    """
    Compose JSON string for event log.

    :buffer: decoded buffer as dict (from decode())
    :device: the name of the device
    :log_name: the log property of the buffer
    :returns: string containing buffer data in PowerSpy JSON format.
    """

    device = device if device is not None else "unknown"
    log_name = log_name if log_name is not None else "unknown"

    output = ''
    output += '{"source":"FGC",'
    output += '"device":"%s",' % device
    output += '"name":"%s",' % log_name
    output += '"type":"table",'
    output += '"subtype":"event-log",'
    output += '"version":"%s",' % JSON_CURRENT_VERSION
    output += '"table":{'
    output += '"rows":['

    for event in buffer:
        timestamp = event['time_seconds'] + event['time_microseconds'] / 1e6

        if timestamp == 0:
            continue

        output += '{"timestamp":%.6f,' % timestamp

        output += '"cells":["%s","%s","%s","%s"]},' %\
            (_escape_json(event['property']),
             _escape_json(event['value']),
             _escape_json(event['action']),
             event['status'])

    output = '%s]}' % output[:-1]

    output += '}'

    return output


def _encode_signal_buffer_json(buffer, version, device, log_name, cycle_selector="0"):
    """
    Compose JSON string for signal buffer.

    :buffer: decoded buffer as dict (from decode())
    :version: the version of the buffer (from get())
    :device: the name of the device
    :log_name: the log property of the buffer
    :cycle_selector: cycle selector that applied to the acquisition of the data. can be the number of the cycle or the cycle name.
    :returns: string containing buffer data in PowerSpy JSON format.
    """

    device = device if device is not None else "unknown"
    log_name = log_name if log_name is not None else "unknown"

    if version == BufferVersion.V1.value:
        period = buffer.get('period_seconds', 0) + buffer['period_microseconds'] / 1e6
        first_sample_time = buffer['first_sample_time_seconds'] + buffer['first_sample_time_microseconds'] / 1e6
        time_origin = first_sample_time + ((buffer['number_samples'] - 1) * period if is_cycle_selector_default(cycle_selector) else 0)
        is_analog = True
    elif version == BufferVersion.V2.value:
        period = buffer['period_microseconds'] / 1e6
        # Compute first sample time
        first_sample_time = 0.
        if buffer['meta']['timestamp_is_lst']:
            first_sample_time = buffer['first_sample_time_seconds'] + buffer['first_sample_time_microseconds'] / 1e6 - (buffer['number_samples'] - 1) * period
        else:
            first_sample_time = buffer['first_sample_time_seconds'] + buffer['first_sample_time_microseconds'] / 1e6
        # If time origin is given in the data, use it
        time_origin = 0.
        if buffer.get('time_origin_seconds', None) and buffer.get('time_origin_microseconds', None):
            time_origin = buffer['time_origin_seconds'] + buffer['time_origin_microseconds'] / 1e6
        else:
            # Otherwise infer the value: Set it as the first or last sample time (depending on cycling/non-cycling mode)
            time_origin = first_sample_time + ((buffer['number_samples'] - 1) * period if is_cycle_selector_default(cycle_selector) else 0)
        # Other flags
        is_analog = buffer['meta'].get('analog_signals', True)
    else:
        raise Exception('Encoder does not support buffer version ' + str(version))

    output = ''
    output += '{"source":"FGC",'
    output += '"device":"%s",' % device
    output += '"name":"%s",' % log_name
    output += '"cycleSelector":"%s",' % cycle_selector
    output += '"timeOrigin":%.6f,' % time_origin
    output += '"firstSampleTime":%.6f,' % first_sample_time
    output += '"period":%.6f,' % period
    output += '"version":"%s",' % JSON_CURRENT_VERSION
    output += '"type":"%s",' % ('analog' if is_analog else 'digital')
    output += '"signals":['

    for signal in buffer['signals']:
        output += '{"name":"%s",' % signal['name']

        if signal['meta']['steps']:
            output += '"step":true,'

        output += '"timeOffset":%.6f,' % (signal.get('time_offset_microseconds', 0) / 1e6)
        output += '"samples":[%s]},' % signal['samples']

    output = "%s]" % output[:-1]

    output += '}'
    return output


def _encode_signal_buffer_json_v3(
    buffer,
    device="unknown",
    log_name="unknown",
    cycle_selector="0"
):
    """
    Creates the JSON object for SPY_V3 signal buffer.

    :buffer: decoded buffer as dict (from decode())
    :device: the name of the device
    :log_name: the log property of the buffer
    :cycle_selector: cycle selector that applied to the acquisition of the data. Cycle number or name.
    
    :returns: buffer PowerSpy JSON object.
    """

    period = buffer['period_seconds'] + buffer['period_nanoseconds'] * 1e-9
    first_sample_time = _get_first_sample_time_decimal_v3(buffer, period)
    time_origin = _get_time_origin_decimal_v3(buffer, first_sample_time, period, cycle_selector)

    epoch = min(int(first_sample_time), int(time_origin))
    first_sample_time -= epoch
    time_origin -= epoch

    is_analog = buffer['meta'].get('analog_signals', True)

    result = {
        "source": "FGC",
        "device": device,
        "name": log_name,
        "cycleSelector": cycle_selector,
        "epoch": epoch,
        "timeOrigin": float(time_origin),
        "firstSampleTime": float(first_sample_time),
        "period": period,
        "version": JSON_CURRENT_VERSION,
        "type": "analog" if is_analog else "digital",
        "signals": _get_buffer_signals_v3(buffer)
    }

    return json.dumps(result)


def _get_first_sample_time_decimal_v3(buffer, period):
    first_sample_time = Decimal('%d.%09d' % (buffer['timestamp_seconds'], buffer['timestamp_nanoseconds']))

    if buffer['meta']['timestamp_is_lst']:
        first_sample_time -= Decimal(str(_get_buffer_duration(buffer, period)))

    return first_sample_time


def _get_time_origin_decimal_v3(buffer, first_sample_time, period, cycle_selector):
    if buffer.get('time_origin_seconds') is not None and buffer.get('time_origin_nanoseconds') is not None:
        return Decimal('%d.%09d' % (buffer['time_origin_seconds'], buffer['time_origin_nanoseconds']))

    # Sets time_origin as the first or last sample time (depending on cycling/non-cycling mode).
    time_origin = first_sample_time

    if is_cycle_selector_default(cycle_selector):
        time_origin += Decimal(str(_get_buffer_duration(buffer, period)))

    return time_origin


def _get_buffer_signals_v3(buffer): 
    signals = []

    for buffer_signal in buffer['signals']:
        signal = {
            "name": buffer_signal['name'],
            "timeOffset": buffer_signal.get('time_offset_nanoseconds', 0) * 1e-9,
            "samples": [float(i) for i in buffer_signal['samples'].split(',')],
        }

        if buffer_signal['meta']['steps']:
            signal["step"] = True

        signals.append(signal)
    
    return signals


def _get_buffer_duration(buffer, period):
    return float(format((buffer['number_samples'] - 1) * period, '.9f'))


def _escape_json(string):
    """
    Escapes characters needed to be escaped in JSON.

    :string: a string to be escaped
    :returns: escaped string
    """

    return regex_escape_json.sub(r'\\\g<1>', string)
