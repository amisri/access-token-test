"""
replicate_super_cycle

Usage:
    ./replicate_super_cycle -h | --help
    ./replicate_super_cycle device_name

Options:
    -h --help Shows this help
"""

import sys
import re

import pyfgc

def get_super_cycle(device):
    print(f"Retrieving super cycle from {device}")
    try:
        cycles = pyfgc.get(device, "LOG.CYCLES")
    except pyfgc.api.PyFgcError as e:
        print(f"{e}")
        return -1
    
    start_super_cycle = False
    end_super_cycle   = False
    super_cycle       = []
    count             = 0

    for cycle in cycles.value.split(","):
        # Format of cycle information
        #   19 1650889143.666000  0| 1 OK  TABLE
        #   20 1650889144.866000  0|18 OK  NONE

        if (count >= 64):
            print(f"Machine super cycle longer than 64 cycles. The simulated super-cycle will be trimmed.")
            break

        [cyc_idx, utc_s, utc_us, sub_sel, cyc_sel, *_] = re.findall(r"\d+", cycle)

        timestamp = utc_s+"."+utc_us

        # Wait until the beginning of the super-cycle

        if cyc_idx == "1":
            if start_super_cycle == False:
                start_super_cycle = True
                cyc_idx_prev, timestamp_prev, cyc_sel_prev = cyc_idx, timestamp, cyc_sel
                continue
            else:
                end_super_cycle = True

        if start_super_cycle == False:
            continue

        super_cycle.append(str(int(cyc_sel) * 100 + int(round((float(timestamp) - float(timestamp_prev)) / 1.2))))
        count = count + 1

        if end_super_cycle:
            break

        cyc_idx_prev, timestamp_prev, cyc_sel_prev = cyc_idx, timestamp, cyc_sel

    fgc_super_cycle = ",".join(super_cycle)

    print(f"Super cycle ({len(super_cycle)}): {fgc_super_cycle}")

    return fgc_super_cycle



def set_super_cycle(device, super_cycle):
    print(f"Setting the super cycle to {device}")

    try:
        cycles = pyfgc.set(device, "FGC.CYC.SIM", super_cycle)
    except pyfgc.api.PyFgcError as e:
        print(f"{e}")
        return -1
    

if __name__ == "__main__":

    super_cycle = get_super_cycle(sys.argv[1])

    for device in sys.argv[2:]:
        set_super_cycle(device, super_cycle)

