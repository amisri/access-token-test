''' Utility to rename systems in the FGC DB'''
import argparse
import sys
from collections import Counter
from getpass     import getpass

import cx_Oracle

import pyfgc_name
pyfgc_name.read_name_file()


DSN = """(DESCRIPTION=
            (ADDRESS=(PROTOCOL=TCP)(HOST=acccon-s.cern.ch)(PORT=10121))
            (ENABLE=BROKEN)
            (CONNECT_DATA = (SERVICE_NAME = acccon_s.cern.ch))) """
            
RENAME_SYSTEM_QUERY = """
    UPDATE 
      POCONTROLS.FGC_SYSTEMS fs
    SET 
      fs.SYS_NAME=:new_fgc_name
    WHERE 
      fs.SYS_NAME=:old_fgc_name
"""
            
def find_duplicates(device_list):
    return [item for item, count in Counter(device_list).items() if count > 1]

def run_checks_on_systems(old_to_new_map: dict) -> None:
    old_devices = list(old_to_new_map.keys())
    new_devices = list(old_to_new_map.values())

    duplicated_new = find_duplicates(new_devices)
    if duplicated_new:
        print(f'ERROR: duplicated device(s) found in new devices: {duplicated_new}')
        print('Please correct the input arguments before proceeding')
        sys.exit(2)

    for old in old_devices:
        if old in pyfgc_name.devices:
            print(f'ERROR: old device {old} found in name file. Change name to new device before proceeding')
            sys.exit(2)

    for new in new_devices:
        if new not in pyfgc_name.devices:
            print(f'ERROR: new device {new} not found in name file. Add name to new device before proceeding')
            sys.exit(2)

def get_old_to_new_map(renaming_file: str) -> dict:
    old_to_new_map = dict()

    with open(renaming_file, 'r') as fh:
        for line_number,line in enumerate(fh):
            old, new = line.strip().split()

            try:
                old_to_new_map[old]

            except KeyError:
                old_to_new_map[old] = new
            
            else:
                print(f'ERROR: old device {old} in line {line_number + 1} is duplicated. Correct file content before proceeding')
                sys.exit(2)

    run_checks_on_systems(old_to_new_map)
    return old_to_new_map

def rename_in_fgc_db(mapping):
    user_name = input('Username: ')
    pwd = getpass(prompt='Password: ')

    with cx_Oracle.connect(user_name, pwd, DSN) as dbc:
        with dbc.cursor() as cursor:
            for old, new in mapping.items():
                cursor.execute(RENAME_SYSTEM_QUERY, {"old_fgc_name": old, "new_fgc_name": new})

        dbc.commit()

def rename_systems(renaming_file: str = '') -> None:
    old_to_new_map = get_old_to_new_map(renaming_file.pop())
    rename_in_fgc_db(old_to_new_map)

def configure_parser(parser: 'argparser.ArgumentParser') -> None:
    parser.description = __doc__
    parser.set_defaults(handler=rename_systems)

    parser.add_argument('renaming_file', type=str, nargs=1, help='File with old and new names. Each line contains an old-new name pair separated by a space')

def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)
