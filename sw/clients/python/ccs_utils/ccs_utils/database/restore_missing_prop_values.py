import argparse
import sys
from pathlib import Path

import cx_Oracle

import pyfgc_name

PWD_DIR = '/user/pclhc/etc/program_manager/config/'
DB_USER = 'pocontrols_mod'

DEV_DSN = '''(DESCRIPTION=
            (ADDRESS=(PROTOCOL=TCP)(HOST=devdb18-s.cern.ch)(PORT=10121))
            (ENABLE=BROKEN)
            (CONNECT_DATA = (SERVICE_NAME = devdb18_s.cern.ch)))
            '''
            
PRO_DSN = '''(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=acccon-s.cern.ch)(PORT=10121))
           (ENABLE=BROKEN)
           (CONNECT_DATA = (SERVICE_NAME = acccon_s.cern.ch)))
           '''

GET_MISSING_PROPS = '''
    SELECT
      fs.SYS_NAME,
      fp.PRO_NAME,
      fsp.SPR_VALUE 
    FROM FGC_SYSTEMS fs
    INNER JOIN FGC_SYSTEM_PROPERTIES fsp
    ON (fs.SYS_ID = fsp.SPR_SYS_ID)
    INNER JOIN FGC_PROPERTIES fp 
    ON (fp.PRO_ID = fsp.SPR_PRO_ID)
    WHERE 
      fs.SYS_IS_OBSOLETE = 0 AND
      (fsp.SPR_VALUE IS NULL OR 
      fsp.SPR_VALUE = '')
        '''

SET_MISSING_PROPS = '''
    UPDATE FGC_SYSTEM_PROPERTIES fsp
    SET fsp.SPR_VALUE =:value
    WHERE
        (fsp.SPR_VALUE is NULL OR fsp.SPR_VALUE='') AND
        fsp.SPR_ID = (
            SELECT
            fsp.SPR_ID
            FROM
            FGC_SYSTEM_PROPERTIES fsp
            INNER JOIN FGC_SYSTEMS fs
            ON(fs.SYS_ID=fsp.SPR_SYS_ID)
            INNER JOIN FGC_PROPERTIES fp
            ON(fp.PRO_ID=fsp.SPR_PRO_ID)
            WHERE
            fs.SYS_NAME=:sys AND
            fp.PRO_NAME=:prop)
        '''

DELETE_OLD_SYSTEM_PROPS = '''
    DELETE 
    FROM 
    (
      SELECT 
        * 
      FROM fgc_system_properties fsp
      INNER JOIN fgc_properties fp 
      ON fp.PRO_ID = fsp.SPR_PRO_ID
      WHERE 
        fp.PRO_NAME = :prop)
    '''

DELETE_OLD_CLAS_PROPS = '''
    DELETE
    FROM (
        SELECT
          *
        FROM
          FGC_PROPERTY_CLASSES fpc
        INNER JOIN FGC_PROPERTIES fp
        ON fpc.PCL_PRO_ID = fp.PRO_ID
        WHERE
          fp.PRO_NAME = :prop)
      '''

DELETE_OLD_PROPS = '''
    DELETE 
    FROM 
    FGC_PROPERTIES fp 
    WHERE 
      fp.PRO_NAME = :prop
        '''


_class_to_device = dict()
_restore_data = dict()
_missing_data = dict()
_db_conn_strings = {'dev': DEV_DSN, 'pro': PRO_DSN}

_migration_map_common_new_to_old = {
    'INTER_FGC.MASTER':'INTER_FGC.MASTERS',
    'LIMITS.I.STANDBY':'LIMITS.I.MIN',
    'REG.B.EXTERNAL.TRACK_DELAY_PERIODS':'REG.B.EXTERNAL.TRACK_DELAY',
    'REG.B.INTERNAL.PURE_DELAY_PERIODS': 'REG.B.INTERNAL.PURE_DELAY',
    'REG.I.EXTERNAL.OP.R':'REG.I.MANUAL.R',
    'REG.I.EXTERNAL.OP.S':'REG.I.MANUAL.S',
    'REG.I.EXTERNAL.OP.T': 'REG.I.MANUAL.T',
    'REG.I.EXTERNAL.TRACK_DELAY_PERIODS':'REG.I.EXTERNAL.TRACK_DELAY',
    'REG.I.INTERNAL.PURE_DELAY_PERIODS':'REG.I.INTERNAL.PURE_DELAY',
    'REG.I.INTERNAL.AUXPOLE1_HZ':'REG.I.CLBW',
    'REG.I.INTERNAL.AUXPOLES2_HZ': 'REG.I.CLBW2',
    'REG.I.INTERNAL.AUXPOLES2_Z':'REG.I.Z',
}

_migration_map = {51:_migration_map_common_new_to_old,
                  53:_migration_map_common_new_to_old,
                  62:_migration_map_common_new_to_old,
                  63:_migration_map_common_new_to_old,
                  64:_migration_map_common_new_to_old,
                  92:_migration_map_common_new_to_old}
                  
_reverse_migration_map = dict.fromkeys(_migration_map.keys(), None)
for k in _reverse_migration_map:
    _reverse_migration_map[k] = {v: k for k, v in _migration_map[k].items()}
    
_new_properties = {
    'ADC.EXTERNAL.CONTROL': 'DISABLED',
    'CAL.CALSYS.CDC_NAME':'UNUSED',
    'CAL.CALSYS.SM_NAME':'UNUSED',
    'CAL.CALSYS.SM_CHANNEL':'0',
    'DEVICE.MULTI_PPM': 'DISABLED',
    'INTER_FGC.BROADCAST': 'DISABLED',
    'INTER_FGC.CONSUMERS': 'NONE',
    'INTER_FGC.DECO.D': '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
    'INTER_FGC.DECO.INDEX':'0',
    'INTER_FGC.DECO.K': '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
    'INTER_FGC.DECO.PARTNERS':'NONE',
    'INTER_FGC.DECO.PHASE':'0',
    'INTER_FGC.ISR_OFFSET_US':'400',
    'INTER_FGC.MASTER':'NONE',
    'INTER_FGC.PRODUCED_SIGS':'NONE',
    'INTER_FGC.SIG_SOURCES':'NONE',
    'INTER_FGC.SLAVES':'NONE',
    'INTER_FGC.TOPOLOGY':'NONE',
    'INTER_FGC.VS_PRESENT': 'ENABLED',
    'LOG.OASIS.SUBSAMPLE': '1',
    'REG.B.EXTERNAL.TRACK_DELAY_PERIODS':'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
    'REG.B.INTERNAL.PURE_DELAY_PERIODS': '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
    'REG.I.EXTERNAL.TRACK_DELAY_PERIODS':'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
    'REG.I.INTERNAL.PURE_DELAY_PERIODS': '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
    'REF.V_FEED_FWD.GAIN':'1',
    'VS.DAC_CLAMP':'0.0000000E+00',
}

def _get_data_from_config_file(cf, devices_filter, props_filter):
    fgc_name = cf.name.rsplit('.', 1)[0]
    if devices_filter and fgc_name not in devices_filter:
        return

    with open(cf, 'r') as cfh:
        for line in cfh.readlines():
            if not line.startswith('!'):
                continue
        
            _, _, prop, value = line.split()
            
            if props_filter and prop not in props_filter:
                continue

            try:
                _restore_data[fgc_name]
            
            except KeyError:
                _restore_data[fgc_name] = dict()
            
            _restore_data[fgc_name].update({prop:value})

def _populate_class_to_device():
    pyfgc_name.read_name_file()
    for dev in pyfgc_name.devices.keys():
        class_id = pyfgc_name.devices[dev]['class_id']
        
        try:
            _class_to_device[class_id]
        
        except KeyError:
            _class_to_device[class_id] = list()
        
        _class_to_device[class_id].append(dev)

def _is_property_in_backup_file(dev, prop):
    try:
        _restore_data[dev][prop]

    except KeyError:
        return False

    return True

def _has_device_backup(dev):
    try:
        _restore_data[dev]

    except KeyError:
        return False

    return True

def _restore_props(db_instance):
    """Performs the query to update the missing property values
    
    Arguments:
        missing {[type]} -- [description]
        bkp {[type]} -- [description]
        db {[type]} -- [description]
        fgcs {[type]} -- [description]
        classes {[type]} -- [description]
        props {[type]} -- [description]
    """
    expected_updates = {device: len(inner) for device, inner in _missing_data.items()}
    successful_updates = dict.fromkeys(expected_updates.keys(), 0)
    error_updates = dict.fromkeys(expected_updates.keys(), dict())

    devices_missing = dict()
    props_missing = list()
    props_to_delete = list()
    data_to_update = list()

    for dev, innerd in _missing_data.items():
        for prop, _ in innerd.items():

            if not _has_device_backup(dev):
                devices_missing[dev] = _missing_data[dev]

            device_class = pyfgc_name.devices[dev]['class_id']

            if _is_property_in_backup_file(dev, prop):
                # Property is missing in DB, and value found in backup file. Update DB
                data_to_update.append((dev, prop, _restore_data[dev][prop]))

            else:
                # Property is missing in DB, but value not in backup file
                if prop in _migration_map[device_class].keys():
                    # Property has been renamed in DB. Find old property value and update DB
                    old_prop_name = _migration_map[device_class][prop]
                    try:
                        value = _restore_data[dev][old_prop_name]

                    except KeyError:
                        pass
                
                    else:
                        data_to_update.append((dev, prop, value))

                elif prop in _new_properties.keys():
                    # New property. Update DB
                    data_to_update.append((dev, prop, _new_properties[prop]))

                else:
                    props_missing.append((dev, prop))
            # SHOULD BE IN THE if/else??
            if prop in _reverse_migration_map[device_class]:
                props_to_delete.append((dev, prop, _reverse_migration_map[device_class][prop]))

    print('WARNING: the following issues have been found...')
    print('----- Devices missing from backup files -----')
    print(f'{"Device":>23}{"Issue":>60}')
    for dev in devices_missing:
        print(f'{dev:>23}{"Device not found in backup files. Properties not set in DB:":>60}')
        for prop in devices_missing[dev].keys():
            print(f'{prop:>83}')
        
        print('\n')

    print('\n')
    print('----- Properties missing from backup files or migration maps-----')
    print(f'{"Device":>23}{"Property":>35}{"Issue":>60}')
    for missed in props_missing:
        dev, prop = missed
        print(f'{dev:>23}{prop:>35}{"Property not found in backup file or migration maps":>60}')

    print('\n')
    print('----- Properties to delete-----')
    print(f'{"Device":>23}{"Property":>35}{"Issue":>60}')
    for to_delete in props_to_delete:
        dev, prop, new_prop = to_delete
        msg = "Property replaced by " + new_prop
        print(f'{dev:>23}{prop:>35}{msg:>60}')


    print('\n')
    p = Path(PWD_DIR)
    with open(p / DB_USER) as f:
        password = f.read().rstrip()

    print('Populating database...')
    data_dict_keys = ('sys', 'prop', 'value')
    with cx_Oracle.connect(DB_USER, password, _db_conn_strings[db_instance]) as db_conn:
        with db_conn.cursor() as cursor:
            for data in data_to_update:
                data_dict = {meta: data for (meta, data) in zip(data_dict_keys, data)}
                # print(f'data_dict is {data_dict}')
                cursor.execute(SET_MISSING_PROPS, data_dict)
            # cursor.executemany(SET_MISSING_PROPS, data_to_update, batcherrors=True, arraydmlrowcounts=True)
            
            # try: 
            #     row_counts = cursor.getarraydmlrowcounts()
            #     for device, count in zip(data_to_update, row_counts):
            #         print(f'INFO: device {device} updated {count} properties')
            #         successful_updates[device] = count

            # except cx_Oracle.DatabaseError as e:
            #     print(f'{e}')

            # try:
            #     errors = cursor.getbatcherrors()
            #     for error in errors:
            #         print(f'ERROR: {error.message} at row offset {error.offset}')
            #         err_dev, err_prop, _ = data_to_update[error.offset]
            #         error_updates[err_dev] = {err_prop: error.message}

            #     db_conn.rollback()
            
            # except cx_Oracle.DatabaseError as e:
            #     print(f'{e}')
            
            db_conn.commit()

            for to_delete in props_to_delete:
                _, prop, _ = to_delete
                cursor.execute(DELETE_OLD_SYSTEM_PROPS, {'prop': prop})
                cursor.execute(DELETE_OLD_CLAS_PROPS, {'prop': prop})
                cursor.execute(DELETE_OLD_PROPS, {'prop': prop})
            
            db_conn.commit()

            
    print('INFO: update finished. Summary...')
    print(f'{"Expected updates":>50}{"Successful updates":>20}{"Error updates":>20}')
    for device in expected_updates.keys():
        print(f'{device:>23}{str(expected_updates[device]):>27}{str(successful_updates[device]):>20}{str(len(error_updates[device])):>20}')

    print('\n')
    print('WARNING: update error details...')
    print(f'{"Property":>50}{"Error message":>20}')
    for device, inner in error_updates.items():
        for prop, msg in inner.items():
            print(f'{device:>23}{prop:>27}{msg:>20}')

def _get_data_to_restore(bkp_dir, devices_filter, props_filter):
    print('Getting data from backup files...')

    p = Path(bkp_dir)
    if not p.exists():
        print(f'ERROR: backup directory {bkp_dir} does not exist. Exiting...')
        sys.exit(2)

    if not p.is_dir():
        print(f'ERROR: backup directory {bkp_dir} is not a directory. Exiting...')
        sys.exit(2)

    bkp_files_list = list(p.glob('**/*.txt'))
    for f in bkp_files_list:
        _get_data_from_config_file(f, devices_filter, props_filter)              
    
def _get_missing_data_from_db(db_instance, devices_filter, props_filter):
    print('Getting systems and properties with empty values...')
    p = Path(PWD_DIR)
    with open(p / DB_USER) as f:
        password = f.read().rstrip()

    rows = list()
    with cx_Oracle.connect(DB_USER, password, _db_conn_strings[db_instance], encoding='UTF-8') as db_conn:
        with db_conn.cursor() as cursor:
            cursor.execute(GET_MISSING_PROPS)
            rows = cursor.fetchall()

    for row in rows:
        device, prop, value = row
        if devices_filter and device not in devices_filter:
            continue

        if props_filter and prop not in props_filter:
            continue

        try:
            _missing_data[device].update({prop:value})
            
        except KeyError:
            _missing_data[device] = {prop: value}
    
def _get_filters_from_cli(fgcs, classes, props, filter_union):
    _populate_class_to_device()

    # Build filters from CLI classes and fgcs before parsing files
    ## Filter by class
    devices_from_class = set()
    for c in classes:
        try:
            _class_to_device[c]

        except KeyError:
            pass

        else:
            devices_from_class |= set(_class_to_device[c])

    ## Filter by fgc in CLI arg
    devices_from_fgcs = set(fgcs)
    device_filter_list = [devices_from_fgcs, devices_from_class]
    non_empties = [s for s in device_filter_list if s]

    devices_filter_set = set()
    if non_empties:
        if filter_union:
            devices_filter_set = set.union(*non_empties)

        else:
            devices_filter_set = set.intersection(*non_empties)

    props_filter = set(props)

    return devices_filter_set, props_filter

def restore_db_props_from_files(bkp_dir=Path(), db='', fgcs=None, classes=None, props=None, filter_union=False):
    devices_filter, props_filter = _get_filters_from_cli(fgcs, classes, props, filter_union)
    _get_missing_data_from_db(db, devices_filter, props_filter)
    _get_data_to_restore(bkp_dir, devices_filter, props_filter)
    _restore_props(db)

def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=restore_db_props_from_files)

    parser.add_argument('bkp_dir',        type=str,              default=Path.cwd(),                         help='Directory where configuration files are stored')
    parser.add_argument('--db'     ,      type=str,              default='dev', choices=['dev', 'pro'],      help='DB to populate missing values to [dev , pro]. Defaults to pro.')
    parser.add_argument('--fgcs'   ,      type=str,              default=list(), nargs='+',                  help='Space separated FGC list to populate values to')
    parser.add_argument('--classes',      type=int,              default=list(), nargs='+',                  help='Space separated classes list to populate values to')
    parser.add_argument('--props'  ,      type=str,              default=list(), nargs='+',                  help='Space separated property list to populate missing values to')
    parser.add_argument('--filter_union', type=bool, const=True, default=False, nargs='?',                   help='Total of devices will be filtered by the union of fgcs, classes and props sets')


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)
