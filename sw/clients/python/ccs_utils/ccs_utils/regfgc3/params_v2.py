import sys

from functools import partial
from itertools import chain

from xmlrpc.client import boolean

import params_common as pc 


class ParamsV2:
    def __init__(self, blob: list = None):

        self._blob = self._ensure_blob_format(blob)
    
        # Make sure the blob version is 2
        if self._blob[0] >> 16 != 2:
            raise pc.ParamsWrongVersion(self._blob[0] >> 16)

        magic_word_idx = [i for i in range(len(self._blob)) if self._blob[i] & 0x0000FFFF == pc.BLOB_V2_MAGIC_WORD][0]
        self._metadata = list(map(lambda x: ((x >> 16) & 0xFFFF, x & 0xFFFF), self._blob[0 : magic_word_idx + 1]))
        self._metadata = list(chain.from_iterable(self._metadata))
        self._values   = self._blob[magic_word_idx + 1 :]


    def _ensure_blob_format(self, blob: list = None) -> list:
        if type(blob[0]) == str:
            base = blob[0].startswith("0x") and 16 or 10
            partial_int = partial(int, base=base)
            blob = list(map(partial_int, blob))

        return blob


    def get_metadata(self) -> list:
        return self._metadata


    def get_values(self) -> list:
        return self._values


    def convert_to_props(self, pad: boolean = False) -> None:
        metadata_idx    = 1
        props           = list()
        push_access     = True

        for slot_idx in range(0, pc.SCIVS_N_SLOTS):
            push_slot = True
        
            for device_idx in range(0, pc.SCIVS_N_DEVICES):
                for block_idx in range(0, pc.SCIVS_N_BLOCKS):
                    size, index = (self._metadata[metadata_idx] >> 11), self._metadata[metadata_idx] & 0x7FF
                    if size > 0:
                        if push_access == True:
                            props.append(("REGFGC3.RAW.ACCESS", "RW"))
                            push_access = False
                        if push_slot == True:
                            props.append(("REGFGC3.RAW.SLOT", str(slot_idx)))
                            push_slot = False
                        
                        props.append(("REGFGC3.RAW.BLOCK", str(block_idx)))
                        params = ",".join("0x{:08X}".format(param) for param in self._values[index : index + size])
                        if pad and size < pc.SCIVS_MAX_BLOCK_SIZE:
                            params += ",0x00000000" * (pc.SCIVS_MAX_BLOCK_SIZE - size)
                        props.append(("REGFGC3.RAW.PARAM" if device_idx == 0 else "REGFGC3.RAW.DSP_PARAM", params))
                    metadata_idx += 1

        return props


    def return_blob(self) -> list:
        return [",".join("0x{:08X}".format(param) for param in self._blob)]


    def print_blob(self) -> None:
        print(",".join("0x{:08X}".format(param) for param in self._blob))


    def print_nice(self) -> None:
        print("Not implemented for version 2")
        None
