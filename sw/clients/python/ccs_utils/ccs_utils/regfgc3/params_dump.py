import sys
import argparse
import getpass
from pathlib import Path

import cx_Oracle
import pyfgc

from params_v4 import ParamsV4
from params_v2 import ParamsV2


DB_USER_RO = "pocontrols_pub"
DB_PW_RO = "p0published!!"

DB_USER = "pocontrols_mod"
DSN = """(DESCRIPTION=
            (ADDRESS=(PROTOCOL=TCP)(HOST=acccon-s.cern.ch)(PORT=10121))
            (ENABLE=BROKEN)
            (CONNECT_DATA = (SERVICE_NAME=acccon_s.cern.ch)))"""
GET_BLOB_QUERY = """
                SELECT
                  fsp.SPR_VALUE
                FROM
                  FGC_SYSTEM_PROPERTIES fsp
                INNER JOIN FGC_SYSTEMS fs
                ON fs.SYS_ID = fsp.SPR_SYS_ID
                INNER JOIN FGC_PROPERTIES fp
                ON fp.PRO_ID = fsp.SPR_PRO_ID
                WHERE
                  fp.PRO_NAME =:prop_name AND
                  fs.SYS_NAME =:fgc_name"""

UPDATE_BLOB_QUERY = """update
                         POCONTROLS.FGC_SYSTEM_PROPERTIES
                        set SPR_VALUE=:property_value
                        where
                          SPR_SYS_ID=(select SYS_ID from POCONTROLS.FGC_SYSTEMS where SYS_NAME=:fgc_name) and
                          SPR_PRO_ID=(select PRO_ID from POCONTROLS.FGC_PROPERTIES where PRO_NAME=:property_name)"""


def _get_blob_from_db(db_connection, source: str):
    with db_connection.cursor() as cursor:
        cursor.execute(GET_BLOB_QUERY, {"fgc_name": source, "prop_name": "REGFGC3.PARAMS"})
        clob = cursor.fetchone()[0]
    try:
        blob = ",".join("0x{:08X}".format(int(param)) for param in clob.read().split(','))
        blob = blob.split(',')

    except AttributeError as e:
        print(f"Access REGFGC3.PARAMS from DB failed: {e}")
        blob = []

    return blob


def _get_blob_from_fgc(source):
    return pyfgc.get(source, "REGFGC3.PARAMS HEX").value.split(",")


def _get_blob_from_file(file_name):
    blob = []
    with open(file_name) as f:
        lines = f.readlines()

    for line in lines:
        index = line.find('REGFGC3.PARAMS')
        if index != -1:
            blob = line[index + len('REGFGC3.PARAMS'):].strip().split(',')
            break

    return blob



def process_dump(sources: str, type: str='', verbose: str='') -> None:
    for source in sources.split(','):
        try:
            if type == 'db':
                with cx_Oracle.connect(DB_USER_RO, DB_PW_RO, DSN) as connection:
                    blob = _get_blob_from_db(connection, source.upper())

            elif type == 'fgc':
                blob = _get_blob_from_fgc(source.upper())

            elif type == 'file':
                blob = _get_blob_from_file(source)

            version = int(blob[0], 16) >> 16
            params = ParamsV2(blob=blob) if version == 2 else ParamsV4(blob=blob)

            print(f"SOURCE: {source:<25} ", end=('' if verbose == None else '\n'))
            print(f"-" * (len(source) + 8))

            if verbose == 'blob':
                params.print_blob()
            elif verbose == 'nice':
                params.print_nice()
            elif verbose == 'props':
                props = params.convert_to_props()
                for prop in props:
                    # FGC response format used by FGCRun+
                    print(f"{prop[0]}:{prop[1]}")
            elif verbose == 'conf':
                props = params.convert_to_props()
                for prop in props:
                    # FGC config file format
                    print(f"! S {prop[0]} {prop[1]}")

            print("")

        except Exception as e:
            print(f"Blob for {source} could not be retrieved: {e}")


def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=process_dump)
    parser.add_argument('sources',                                            help='Comma separated list of fgc names or file paths')
    parser.add_argument('-t', '--type'   , choices=['db', 'file', 'fgc'],     help='Source type')
    parser.add_argument('-v', '--verbose', choices=['blob', 'nice', 'props', 'conf'], help='Prints the RegFGC3 v4 blob as raw values, nice format, properties, or config file format')


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)

if __name__ == "__main__":
    cli()

