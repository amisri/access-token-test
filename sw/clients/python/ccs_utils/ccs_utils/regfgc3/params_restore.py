import sys
import argparse

import pyfgc



def process_params_restore(devices: str, file: str='', commit: int=0) -> None:

    with open(file) as f:
        for line in f:
            device, params = line.split(":")

            # Strip the suffix .txt
            device = device.replace('.txt','')
            
            if device in devices:
                # The paramters are the last string in the line
                params  = params.split(" ")[-1]
                print("Device {}: {}".format(device, params))
                if commit:
                    try:
                        response = pyfgc.set(device, "REGFGC3.PARAMS", params)
                        if not response.err_code:
                            print(f"Parameters for device {device} have been restored successfully\n")
                        else:
                            print(f"Parameters for device {device} could not be restored: {response.err_msg}")
                    except Exception as e:
                        print(f"Parameters for device {device} could not be restored: {e}")                    



def configure_parser(parser: "argparse.ArgumentParser") -> None:
    """ Restores REGFGC3.PARAMS based on the values backed up in a file with the command 
    grep REGFGC3.PARAM /user/pclhc/etc/config/devices/* > /user/rmurillo/projects/config/REGFGC3_PARAMS_ALL.txt """
    
    parser.description = __doc__
    parser.set_defaults(handler=process_params_restore)
    parser.add_argument('devices',          help='Comma separated list of fgc names or file paths')
    parser.add_argument('-f', '--file'   ,  help='File with parameters backed up')
    parser.add_argument('-c', '--commit' ,  action='store_true', help='Commits the migration')


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)

if __name__ == "__main__":
    cli()

