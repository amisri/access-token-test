import sys
import argparse
import getpass
from pathlib import Path
from xmlrpc.client import boolean

import cx_Oracle
from functools import partial

import pyfgc

from params_v4 import ParamsV4
from params_v2 import ParamsV2
import params_common as pc 



DB_USER = "pocontrols_mod"
DSN = """(DESCRIPTION=
            (ADDRESS=(PROTOCOL=TCP)(HOST=acccon-s.cern.ch)(PORT=10121))
            (ENABLE=BROKEN)
            (CONNECT_DATA = (SERVICE_NAME=acccon_s.cern.ch)))"""
GET_BLOB_QUERY = """
                SELECT
                  fsp.SPR_VALUE 
                FROM 
                  FGC_SYSTEM_PROPERTIES fsp
                INNER JOIN FGC_SYSTEMS fs 
                ON fs.SYS_ID = fsp.SPR_SYS_ID 
                INNER JOIN FGC_PROPERTIES fp 
                ON fp.PRO_ID = fsp.SPR_PRO_ID 
                WHERE 
                  fp.PRO_NAME =:prop_name AND 
                  fs.SYS_NAME =:fgc_name"""
                      
UPDATE_BLOB_QUERY = """update 
                         POCONTROLS.FGC_SYSTEM_PROPERTIES
                        set SPR_VALUE=:property_value
                        where 
                          SPR_SYS_ID=(select SYS_ID from POCONTROLS.FGC_SYSTEMS where SYS_NAME=:fgc_name) and 
                          SPR_PRO_ID=(select PRO_ID from POCONTROLS.FGC_PROPERTIES where PRO_NAME=:property_name)"""
                


def _get_blob_from_db(db_connection, source: str):
    
    print(f"\nGetting blob for device {source} from the DB")

    with db_connection.cursor() as cursor:
        cursor.execute(GET_BLOB_QUERY, {"fgc_name": source, "prop_name": "REGFGC3.PARAMS"})
        clob = cursor.fetchone()[0]

    try:
        blob = list(map(int, clob.read().split(',')))

    except AttributeError as e:
        print(f"Access REGFGC3.PARAMS from DB failed: {e}")
        blob = []

    return blob



def _set_blob_to_db(db_connection, source: str, params_v4: ParamsV4):
    
    print(f"\nSetting blob for device {source} to the DB")

    with db_connection.cursor() as cursor:
        blob = ",".join("{:d}".format(param) for param in params_v4.get_blob())
        cursor.execute(UPDATE_BLOB_QUERY, {"property_value": blob, "fgc_name": source, "property_name": "REGFGC3.PARAMS"})
    
    db_connection.commit()



def _get_blob_from_fgc(source):

    print(f"\nGetting blob from device {source}")
    
    return pyfgc.get(source, "REGFGC3.PARAMS HEX").value.split(",")



def _set_blob_to_fgc(source: str, params_v4: ParamsV4):

    print(f"\nSetting blob to device {source}")

    pyfgc.set(source, "REGFGC3.PARAMS", params_v4.get_blob())



def _get_blob_from_file(source: str):

    print(f"\nGetting blob from file: {source}")    

    blob = ''
    with open(source) as f:
        lines = f.readlines()
    
    for line in lines:
        index = line.find('REGFGC3.PARAMS')
        if index != -1:
            blob = line[index + len('REGFGC3.PARAMS'):].strip().split(',')
            break

    return blob



def _set_blob_to_file(source: str, params_v4: ParamsV4):
  
    print(f"\nSetting blob to file: {source}")    

    None



def _migrate_v2_to_v4(params_v2: ParamsV2) -> ParamsV4:

    metadata_v2 = params_v2.get_metadata()
    value_v2    = params_v2.get_values()
    metadata_v4 = [0] * pc.SCIVS_N_SLOTS
    values_v4   = [0] * (16 * pc.SCIVS_N_BLOCKS * pc.SCIVS_MAX_BLOCK_SIZE)
    dsp_slot    = 0
   
    metadata_v2_idx = 1

    for slot_idx in range(0, pc.SCIVS_N_SLOTS_PARAMS):
        for device_idx in range(0, pc.SCIVS_N_DEVICES):
            block_bitmask = 0
            for block_idx in range(0, pc.SCIVS_N_BLOCKS):
                size, index = (metadata_v2[metadata_v2_idx] >> 11), metadata_v2[metadata_v2_idx] & 0x7FF
                if size > 0:
                    block_bitmask = block_bitmask | 1 << block_idx
                    values_v4_idx = (slot_idx if device_idx == 0 else 0) * pc.SCIVS_N_BLOCKS * pc.SCIVS_MAX_BLOCK_SIZE + block_idx * pc.SCIVS_MAX_BLOCK_SIZE
                    values_v4[values_v4_idx : values_v4_idx + size] = value_v2[index : index + size]
                metadata_v2_idx += 1

            if block_bitmask != 0:
                if device_idx > 0:
                    if metadata_v4[0] != 0: 
                        print(f"Original blob includes parameters for two DSP boards. This is not supported in version 4.")
                        sys.exit(1)
                    metadata_v4[0] = block_bitmask
                    dsp_slot = slot_idx
                else: 
                    metadata_v4[slot_idx] = block_bitmask
    
    return ParamsV4(dsp_slot=dsp_slot, metadata=metadata_v4, values=values_v4)



def _process_verbose(source:str, verbose: str, params_v4: ParamsV4):

    if verbose == None:
        return

    print(f"\n{source:<25} ", end=('' if verbose == None else '\n'))

    if verbose == 'blob':
        params_v4.print_blob()
   
    elif verbose == 'nice':
        params_v4.print_nice()

    elif verbose == 'props':
        for prop in params_v4.convert_to_props():
            print(f"!S {prop[0]}: {prop[1]}")



def _validate_migration(params_v2: ParamsV2, params_v4: ParamsV4):
    
    props_v4 = params_v4.convert_to_props()
    props_v2 = params_v2.convert_to_props(True)

    if props_v2 != props_v4:
        print(f" *** Migration check: FAILED ***")
        # Should take into account props_v4 length as well
        for i in range (len(props_v2)):
            if props_v2[i] != props_v4[i]:
                print(f"             v2: {props_v2[i][0]}: {props_v2[i][1]}")
                print(f"             v4: {props_v4[i][0]}: {props_v4[i][1]}") 
            # else:
            #     print(f" ok {props_v2[i][0]}: {props_v2[i][1]}")
        raise pc.MigrationFailed()
    else:
        print(f" *** Migration check: OK ***")


def _process_backup(source: str, backup: int, params_v2: ParamsV2):
    if backup:
        with open(source + ".txt", "w") as f:
            f.write("!S REGFGC3.PARAMS ")
            params_v2_hex = params_v2.return_blob()
            for param in params_v2_hex:
                f.write(param)
            f.write("\n")


def process_migrate(sources: str, type: str='', commit: int=0, backup: int=0, verbose: str='') -> None:
    if type == 'db': 
        # Get DB password
        password = getpass.getpass(prompt='DB password: ')
        # with cx_Oracle.connect(DB_USER, "p0modherlode!22", DSN) as connection:
        with cx_Oracle.connect(DB_USER, password, DSN) as connection:
            for source in sources.split(','):
                try:
                    params_v2 = ParamsV2(_get_blob_from_db(connection, source))
                    params_v4 = _migrate_v2_to_v4(params_v2)

                    _process_verbose(source, verbose, params_v4)
                    _validate_migration(params_v2, params_v4)
                    _process_backup(source, backup, params_v2)               

                    if commit:
                        _set_blob_to_db(connection, source, params_v4)

                except Exception as e:
                    print(f"Device {source} could not be migrated: {e}")

    elif type == 'fgc':
        for source in sources.split(','):
            # try:
                params_v2 = ParamsV2(_get_blob_from_fgc(source))
                params_v4 = _migrate_v2_to_v4(params_v2)

                _process_verbose(source, verbose, params_v4)
                _validate_migration(params_v2, params_v4)
                _process_backup(source, backup, params_v2)

                if commit:
                    _set_blob_to_fgc(source, params_v4)

            # except Exception as e:
            #     print(f"Device {source} could not be migrated: {e}")

    elif type == 'file':
        for source in sources.split(','):
            try:
                params_v2 = ParamsV2(_get_blob_from_file(source))
                params_v4 = _migrate_v2_to_v4(params_v2)

                _process_verbose(source, verbose, params_v4)
                _validate_migration(params_v2, params_v4)
                
                if commit:
                    _set_blob_to_file(source, params_v4)

            except Exception as e:
                print(f"\nFile {source} could not be migrated: {e}")

    print(f"\n")



def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=process_migrate)
    parser.add_argument('sources',                                            help='Comma separated list of fgc names or file paths')
    parser.add_argument('-t', '--type'   , choices=['db', 'fgc'],             help='Source type')
    parser.add_argument('-v', '--verbose', choices=['blob', 'nice', 'props'], help='Prints the RegFGC3 v4 blob as raw values, nice format or properties')
    parser.add_argument('-c', '--commit' , action='store_true',               help='Commits the migration')
    parser.add_argument('-b', '--backup' , action='store_true',               help='Backs up RegFGC3 v2 parameters')


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)

if __name__ == "__main__":
    cli()

