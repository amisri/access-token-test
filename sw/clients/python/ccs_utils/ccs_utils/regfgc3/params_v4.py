import sys
import math
import struct

from functools import partial
from itertools import chain
from itertools import zip_longest
from xmlrpc.client import boolean

import params_common as pc 


class ParamsV4:
    def __init__(self, blob: list = None, dsp_slot: int = 0, metadata: list = None, values: list = None):
        if blob != None:
            self._version    = 4
            self._dsp_slot   = 0
            self._metadata   = [] 
            self._values     = []
            self._values_dsp = None

            # Make sure the blob is a list of integers
            self._blob = self._ensure_blob_format(blob)
            self._decompose_blob()
        else:
            meatadat_word = []
            for a,b,c,d in self._grouper(metadata, 4):
                meatadat_word.append(a << 24 | b << 16 | c << 8 | d)

            self._version    = 4
            self._dsp_slot   = dsp_slot
            self._metadata   = metadata
            self._values     = values
            self._values_dsp = None
            self._blob       = [0x00040000 + dsp_slot] + meatadat_word + values


    def _ensure_blob_format(self, blob: list = None) -> list:
        if type(blob[0]) == str:
            base = blob[0].startswith("0x") and 16 or 10
            partial_int = partial(int, base=base)
            blob = list(map(partial_int, blob))

        return blob
        

    def _decompose_blob(self) -> None:
        index = 0
        self._version  = self._blob[index] >> 16
        self._dsp_slot = self._blob[index] & 0xFF

        index += 1
        
        # End of metadata section
        index_end = index + int(pc.SCIVS_N_SLOTS/4)

        # Metadata: type integer. Need arithmetic manipulation of the data.
        for word in self._blob[index:index_end]:
            for byte in word.to_bytes(4, byteorder='big'):
                self._metadata.append(byte)

        index = index_end

        # Params: type int. Only used for printing
        self._values = self._blob[index:]

    def get_blob(self) -> None:
        return [",".join("0x{:08X}".format(param) for param in self._blob)]


    def _convert_props_params(self, slot_idx: int, block_bitmask: int, props: list) -> None:       
       
        while block_bitmask:
            block_idx = block_bitmask & (~block_bitmask+1)
            block_bitmask ^= block_idx
            block_idx = int(math.log2(block_idx))
            param_idx = slot_idx * pc.SCIVS_N_BLOCKS * pc.SCIVS_MAX_BLOCK_SIZE + block_idx * pc.SCIVS_MAX_BLOCK_SIZE

            props.append(("REGFGC3.RAW.BLOCK", str(block_idx)))
            params = ",".join("0x{:08X}".format(param) for param in self._values[param_idx : (param_idx + pc.SCIVS_MAX_BLOCK_SIZE)])
            props.append(("REGFGC3.RAW.PARAM" if slot_idx != 0 else "REGFGC3.RAW.DSP_PARAM", params))


    def convert_to_props(self) -> None:
        access   = True
        slot_idx = 1
        props    = list()

        for slot_idx in range(1, pc.SCIVS_N_SLOTS_PARAMS):
            # FPGA parameters
            slot_num_output = False
            block_bitmask = self._metadata[slot_idx]

            if block_bitmask != 0:
                if access == True:
                    props.append(("REGFGC3.RAW.ACCESS", "RW"))
                    access = False

                slot_num_output = True
                props.append(("REGFGC3.RAW.SLOT", str(slot_idx)))
                self._convert_props_params(slot_idx, block_bitmask, props)
          
            # DSP parameters
            if slot_idx == self._dsp_slot:
                block_bitmask = self._metadata[0]
                if block_bitmask != 0:
                    if not slot_num_output:
                        if access == True:
                            props.append(("REGFGC3.RAW.ACCESS", "RW"))
                            access = False
                        props.append(("REGFGC3.RAW.SLOT", str(slot_idx)))
                    self._convert_props_params(0, self._metadata[0], props)
        
        return props


    def _grouper(self, iterable, group_by, fillvalue=None):
        args_for_zip_l = [iter(iterable)] * group_by
        return zip_longest(*args_for_zip_l, fillvalue=fillvalue)


    def _copy_to_private_blob(self) -> None:
        index = 0
        self._blob[index] = self._version << 16 | self._dsp_slot
        index += 1

        # Metadata
        for a,b,c,d in self._grouper(self._metadata, 4):
            self._blob[index] = a << 24 | b << 16 | c << 8 | d
            index += 1

        # Params
        self._blob[index:] = self._values[:]


    def _print_nice_blocks(self,  slot_idx:int):
        if slot_idx != 0:
            block_bitmask = self._metadata[slot_idx]
            print(f" {slot_idx:02}    FPGA  ", end='')
        else:
            block_bitmask = self._metadata[0]
            print(f" {self._dsp_slot:02}    DSP   ", end='')

        for block_idx in range(pc.SCIVS_N_BLOCKS - 1, -1, -1):
            if (1 << block_idx) & block_bitmask:
                print(f" {block_idx} ", end='')
            else:
                print(f" _ ", end='')


    def _print_nice_params(self, slot_idx:int) -> None:
        first_block   = True
        block_bitmask = self._metadata[slot_idx if slot_idx != 0 else 0]

        if self._values_dsp == None:
            # Only used for printing
            packed_long = [struct.pack('>L', param) for param in self._values[0:pc.SCIVS_N_BLOCKS * pc.SCIVS_MAX_BLOCK_SIZE]]
            self._values_dsp = [struct.unpack('>f', param)[0] for param in packed_long] 

        for block_idx in range(0, pc.SCIVS_N_BLOCKS - 1, 1):
            if (1 << block_idx) & block_bitmask:
                if first_block == True:
                    print(f"   {block_idx}: ", end='')
                    first_block = False
                else:
                    print(f"\n                                     {block_idx}: ", end='')

                if slot_idx == 0:
                    param_idx = slot_idx * pc.SCIVS_N_BLOCKS * pc.SCIVS_MAX_BLOCK_SIZE + block_idx * pc.SCIVS_MAX_BLOCK_SIZE

                    print(",".join("{:.4E}".format(param) for param in self._values_dsp[param_idx:param_idx+8]), end = '')
                    param_idx += 8
                    print("\n                                        ", end = '')
                    print(",".join("{:.4E}".format(param) for param in self._values_dsp[param_idx:param_idx+8]), end = '')
                    param_idx += 8
                    print("\n                                        ", end = '')
                    print(",".join("{:.4E}".format(param) for param in self._values_dsp[param_idx:param_idx+8]), end = '')
                    param_idx += 8
                    print("\n                                        ", end = '')
                    print(",".join("{:.4E}".format(param) for param in self._values_dsp[param_idx:param_idx+8]), end = '')
                    continue

                param_idx = slot_idx * pc.SCIVS_N_BLOCKS * pc.SCIVS_MAX_BLOCK_SIZE + block_idx * pc.SCIVS_MAX_BLOCK_SIZE                
                print(",".join("0x{:08X}".format(param) for param in self._values[param_idx:param_idx+8]), end = '')
                param_idx += 8
                print("\n                                        ", end = '')
                print(",".join("0x{:08X}".format(param) for param in self._values[param_idx:param_idx+8]), end = '')
                param_idx += 8
                print("\n                                        ", end = '')
                print(",".join("0x{:08X}".format(param) for param in self._values[param_idx:param_idx+8]), end = '')
                param_idx += 8
                print("\n                                        ", end = '')
                print(",".join("0x{:08X}".format(param) for param in self._values[param_idx:param_idx+8]), end = '')


    def print_nice(self) -> None:

        print(f"VERSION : {self._version}")
        print(f"DSP SLOT: {self._dsp_slot}")
        print(f"SLOT  DEVICE       BLOCK                PARAMS")
        
        for slot_idx in range (1, pc.SCIVS_N_SLOTS_PARAMS):
            self._print_nice_blocks(slot_idx)
            self._print_nice_params(slot_idx)

            if slot_idx == self._dsp_slot:
                print(f"")
                self._print_nice_blocks(0)
                self._print_nice_params(0)

            print(f"  ")


    def print_blob(self) -> None:
        print(",".join("0x{:08X}".format(param) for param in self._blob))


    def get_blob(self) -> list:
        return self._blob
