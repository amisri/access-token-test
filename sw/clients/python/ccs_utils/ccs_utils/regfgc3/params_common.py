
# Constants
SCIVS_N_SLOTS = 32
SCIVS_N_SLOTS_PARAMS = 16
SCIVS_N_DEVICES = 2
SCIVS_N_BLOCKS = 7
SCIVS_MAX_BLOCK_SIZE = 31
BLOB_V2_MAGIC_WORD = 0xB10B


# Exception

class ParamsWrongVersion(Exception):
    """Exception raised for errors in the blob version.

    Attributes:
        version -- blob version
    """

    def __init__(self, version):
        self.message = "unexpected blob version: " + str(version)
        super().__init__(self.message)


class MigrationFailed(Exception):
    """Exception raised for errors in the blob version.

    Attributes:
        version -- blob version
    """

    def __init__(self):
        self.message = "resulting v4 properties does not match the original v2 properties"
        super().__init__(self.message)

