__version__ = "0.0.12"

__authors__ = """
    Carlos Ghabrous Larrea
    Martin Cejp
"""

__emails__ = """
    martin.cejp@cern.ch
"""