import argparse
import sys
from datetime import datetime
from typing import Dict, Iterable, List, Optional, Tuple

import pyfgc
import pyfgc_const
import pyfgc_name


# Note: FGCD version could also be inspected through the DEVICE.VERSION.COMPILE.TIME property
CODE_TYPE_TO_PROPERTY = {
    "boot": "BOOTPROG",
    "compdb": "COMPDB",
    "dimdb": "DIMDB",
    "iddb": "IDDB",
    "main": "MAINPROG",
    "pld": "PLD",
    "ptdb": "PTDB",
    "sysdb": "SYSDB",
}


def parse_code_type_list(code_types_str: str) -> List[str]:
    code_types = code_types_str.lower().split(",")

    for type in code_types:
        if type not in CODE_TYPE_TO_PROPERTY:
            raise ValueError(type)

    return code_types


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    # note that we want to propagate the return code
    return args.pop("handler")(**args)


def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=code_version_by_device)

    parser.add_argument(
        "-c",
        "--code-types",
        type=parse_code_type_list,
        help="comma-separated list of code types from the following set: "
        + " ".join(CODE_TYPE_TO_PROPERTY.keys())
        + " (defaults to all codes)",
    )
    parser.add_argument("devices", nargs="*", help="device names")


def code_version_by_device(code_types: Optional[List[str]], devices: List[str]) -> int:
    if not len(devices):
        devices = [line.strip() for line in sys.stdin]

    # Collect the devices to be interrogated
    # conceptually this is a set, but we use a dict to preserve order
    resolved_devices: Dict[str, dict] = dict()

    pyfgc_name.read_name_file()

    for device_pattern in devices:
        matching = {
            name: device_dict
            for name, device_dict in pyfgc_name.devices.items()
            if name.upper().startswith(device_pattern.upper())
        }

        if not len(matching):
            raise ValueError(f"No device matching name/prefix '{device_pattern}'")

        resolved_devices.update(matching)

    # Iterate over devices and fetch the code versions

    any_error = False

    for device_name, device_dict in resolved_devices.items():
        try:
            with pyfgc.fgc(device_name) as fgc:
                matching_codes: List[Tuple[str, str]]

                # FIXME: it is really stupid that the gateway name is uppercase in namefile but lowercase in pyfgc_name.gateways,
                #        and that we have to hard-code this knowledge here
                if device_name.lower() in pyfgc_name.gateways:
                    # device is a gateway

                    # parse CODE.INFO parent property
                    resp = fgc.get("CODE.INFO")
                    codes = _code_info_to_list_of_dict(resp)

                    # remove empty & filtered ones
                    codes = [code_dict for code_dict in codes if code_dict["NAME"] != "" and _matches_filter(code_dict["NAME"], code_types)]

                    matching_codes = [(code_dict["NAME"], code_dict["VERSION"]) for code_dict in codes]
                else:
                    # assume an FGC device (only FGC3 has been tested)

                    matching_codes = []
                    resp_dict = _parent_to_dict(fgc.get("CODE.VERSION"))

                    for prop, version_str in resp_dict.items():
                        if _matches_filter(prop, code_types):
                            matching_codes.append((prop, version_str))

                if not matching_codes:
                    print(f"{device_name}: no matching code", file=sys.stderr)

                device_name_padded = device_name.ljust(pyfgc_const.FGC_MAX_DEV_LEN)

                for name, version_str in matching_codes:
                    decoded: object
                    try:
                        decoded = datetime.fromtimestamp(int(version_str))
                    except Exception:
                        decoded = ""

                    # codes on the gateway are up to 16 characters in length
                    print(f"{device_name_padded}\t{name:16}\t{version_str:10}\t{decoded}")
        except Exception as ex:
            print(f"{device_name}: {type(ex).__name__}: {ex}", file=sys.stderr)
            any_error = True

    # We only return success if there were no errors
    return 1 if any_error else 0


def _code_info_to_list_of_dict(resp: pyfgc.FgcResponse) -> List[dict]:
    # TODO: really this code doesn't already exist?

    # A bit of functional magic ahead
    # "NAME:foo,bar,baz" + \n + "CLASS:10,20,30" + \n + ... -> {"NAME": "foo,bar,baz", "CLASS": "10,20,30", ...}
    resp_dict = _parent_to_dict(resp)
    # {"NAME": "foo,bar,baz", "CLASS": "10,20,30", ...} -> [("foo", "bar", "baz"), ("10", "20", "30"), ...]
    value_lists = [value.split(",") for value in resp_dict.values()]
    # [("foo", "bar", "baz"), ("10", "20", "30"), ...] -> [("foo", "10", ...), ("bar", "20", ...), ("baz", "30", ...)]
    transposed = zip(*value_lists)
    # -> [{"NAME": "foo", "CLASS": "10", ...}, {"NAME": "bar", "CLASS": "20", ...}, {"NAME": "baz", "CLASS": "30", ...}]
    codes = [dict(zip(resp_dict.keys(), row)) for row in transposed]

    return codes


def _matches_filter(code_name: str, filter: Optional[Iterable[str]]) -> bool:
    """
    Check if the name of a code matches passes the filter, if one was provided.
    Since the names are different between gateways and FGC devices, this is a bit more complicated than looking for an exact match.

    Examples:
    - if the filter includes "pld", we want to match "PLD", but also "C012_60_PLD_7" and "C014_60_PLD_14"
    - if the filter includes "main", we want to match "MAINPROG", as well as "C008_51_MainProg", "C024_63_MainProg" etc.
    """

    if filter is None:
        return True

    return any(CODE_TYPE_TO_PROPERTY[generic_name] in code_name.upper() for generic_name in filter)


def _parent_to_dict(resp: pyfgc.FgcResponse) -> Dict[str, str]:
    """Convert a parent property response into a dict of child properties"""
    return {prop: value for prop, value in (row.split(":") for row in resp.value.splitlines())}


if __name__ == "__main__":
    sys.exit(cli())
