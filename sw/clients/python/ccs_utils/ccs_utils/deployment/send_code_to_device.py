#!/usr/bin/env python3
"""
Send code to a device. For the moment, only updating of FGC3 MainProg is supported.
"""

import argparse
import logging
from pathlib import Path
import time
import re
from typing import Optional

import pyfgc
import pyfgc_test_framework.fgc


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def assert_ok(resp):
    if resp.err_code != "":
        logger.info("Failing response: %s", resp)
        raise pyfgc.FgcResponseError(f"{resp.err_code}: {resp.err_msg}")


def await_value(gw, prop, value, timeout=60):
    # Poll device status from gateway. It needs some time to power cycle, and yet more time to flash the new code
    for i in range(timeout):
        response = gw.get(prop)
        logger.debug(f'({i}) {response.value}')
        time.sleep(1)

        if response.value == value:
            break
    else:
        raise Exception


# https://gitlab.cern.ch/ccs/fgc/-/blob/master/sw/clients/perl/fgcrun+/fgcrun+.pl#L5778
def _send_code(gateway: str, device_name: str, blob: bytes, version_string: str, allow_downgrade: bool):
    # Connect to gateway
    with pyfgc.fgc(gateway) as gw:
        # Resolve GW channel for the device
        device_name_by_channel = gw.get("GW.FGC.NAME").value.split(",")
        channel = 1 + device_name_by_channel.index(device_name)

        # Check state
        state = gw.get("GW.FGC.STATE[%d]" % channel).value

        if state == "INBOOT":
            in_boot = True
        elif state == "IDLE":
            in_boot = False
        else:
            raise Exception("Unexpected device state: " + state)

    # Check current code version
    if in_boot:
        logger.warning("Device %s is in BOOT mode -- skipping code version check and uploading code %s", device_name, version_string)
    else:
        response = pyfgc.get(device_name, property_name="CODE.VERSION.MAINPROG")

        if response.value == version_string:
            logger.info("Device %s already up-to-date with code %s", device_name, version_string)
            return
        elif int(response.value) < int(version_string):
            logger.info("Will upgrade device %s from %s to %s", device_name, response.value, version_string)
        elif int(response.value) > int(version_string):
            if allow_downgrade:
                logger.info("Will **downgrade** device %s from %s to %s", device_name, response.value, version_string)
            else:
                raise Exception(f"Device {device_name} already contains more recent code version {response.value}. Re-run program with '-f' to downgrade.")

    code_name = "CODE.SLOT31"

    with pyfgc.fgc(gateway) as gw:
        # Clear ALL SLOTS except 0 (NameDB)
        # This needs to be done to ensure that the MainProg isn't in any other slot,
        # because if the FGC boot sees it, it will not look at the rest (and will miss our newer build)
        #
        # Reference: clear_codes https://gitlab.cern.ch/ccs/fgc/-/blob/master/sw/clients/perl/fgcrun+/fgcrun+.pl#L5750
        logger.info("Clearing code slots")
        for i in range(1, 32):
            assert_ok(gw.set("CODE.SLOT" + str(i), ""))

        # Upload code
        start = 0
        block_size = 30000

        logger.info("Uploading code")

        while start < len(blob):
            block = blob[start:start+block_size]

            setvar = code_name + "[" + str(start) + ",]"
            block_ascii = ",".join("0x%02X" % byte for byte in block)

            assert_ok(gw.set(setvar, block_ascii))

            start += block_size

        if not in_boot:
            # Restart device
            logger.info("Power cycle")

            # TODO: can we re-use the gateway session?
            assert_ok(pyfgc.set(device_name, property_name="DEVICE.RESET", value=""))

            # Repeatedly poll device status from gateway. It needs some time to power cycle, and yet more time to flash the new code
            await_value(gw, 'GW.FGC.STATE[%s]' % channel, "INBOOT")
            await_value(gw, 'GW.FGC.STATE[%s]' % channel, "IDLE")
        else:
            # Wait for a bit to ensure that the FGC has had enough time to see an advertisement of the new code by the gateway
            time.sleep(2)

            result, rterm_response = pyfgc_test_framework.boot_send_command(device_name, "UPDATE_CODES_FROM_FIELDBUS", timeout=30)

            if result == "ERROR":
                logger.warning("UPDATE_CODES_FROM_FIELDBUS returned ERROR, this might be however triggered by a bug in pyfgc")
                time.sleep(20) # give some time to complete the programming & hope for the best
            else:
                logger.info("UPDATE_CODES_FROM_FIELDBUS result: %s", result)

            logger.info("Power cycle")
            if pyfgc_test_framework.boot_power_cycle(device_name) is False:
                logger.warning("Power cycle timed out, but we will give it a few more seconds")
                time.sleep(5)

        # Clear slot again
        logger.debug("Clearing code slot & re-reading codes")
        assert_ok(gw.set(code_name, ""))
        assert_ok(gw.set("GW.READCODES", ""))

        logger.debug("Validating new code version")
        response = pyfgc.get(device_name, property_name="CODE.VERSION.MAINPROG")

        if response.value != version_string:
            logger.info(f'{response}, expect {version_string}')
        assert response.value == version_string

    logger.info("Done")


def send_code_to_device(codefile_or_directory: Path, device_name: str, allow_downgrade: bool, gateway_name: Optional[str]):
    if codefile_or_directory.is_dir():
        # expect a directory like /sw/fgc/codes/C024_63_MainProg/fieldbus/
        best_candidate = None
        best_candidate_timestamp = None

        for candidate in codefile_or_directory.iterdir():
            # candidate should be a directory named like 1618216435
            if not re.fullmatch(r"\d{10}", candidate.name):
                continue

            timestamp = int(candidate.name)

            if best_candidate_timestamp is None or timestamp > best_candidate_timestamp:
                # directory should contain a single file prefixed Cddd_dd_...
                for candidate_code_file in candidate.iterdir():
                    if re.match(r"^C\d{3}_\d{2}_", candidate_code_file.name):
                        best_candidate = candidate_code_file
                        best_candidate_timestamp = timestamp
                        break

        if best_candidate is None:
            raise Exception("No candidate codefiles in directory")

        codefile = best_candidate
        version_string = str(best_candidate_timestamp)
    elif codefile_or_directory.is_file():
        # extract version_string from path
        match = re.findall(r"\d{10}", str(codefile_or_directory))

        # expect exactly 1 match
        if len(match) != 1:
            # TODO: allow specifying version string explicitly
            raise Exception("Codefile path must contain version string")

        codefile = codefile_or_directory
        version_string, = match
    else:
        raise FileNotFoundError(str(codefile_or_directory))

    if gateway_name is None:
        import pyfgc_name

        pyfgc_name.read_name_file()
        gateway_name = pyfgc_name.devices[device_name]["gateway"]

    # Attempt to parse code file name & warn if type not supported
    match = re.match(r"^(C\d{3})_\d{2}_", codefile.name)
    if match:
        code_type = match[1]

        if code_type not in {
                "C023",     # 62_MainProg
                "C024",     # 63_MainProg
                "C030",     # 64_MainProg
                "C031",     # 65_MainProg
                }:
            logger.warning("Deployment of code type %s has not been tested -- please report any encountered issues", code_type)

    code_blob = codefile.read_bytes()

    _send_code(gateway_name, device_name, code_blob, version_string, allow_downgrade)


def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=send_code_to_device)

    parser.add_argument("codefile_or_directory", type=Path,
                        help="Path to code binary, or directory containing different versions of the same code (the latest will then be used)")
    parser.add_argument("device_name",
                        help="Name of device to update, e.g. RFNA.866.06.ETH2")
    parser.add_argument("-f", "--allow-downgrade", dest="allow_downgrade", action="store_true", default=False,
                        help="Allow sending a code version older than currently running (by default this is not allowed)")
    parser.add_argument("-g", "--gateway", dest="gateway_name",
                        help="Name of gateway, e.g. cfc-866-reth2. If omitted, will be deduced from namefile.")

    args = parser.parse_args()


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop("handler")(**args)


# Usage:
#   send_code_to_device.py sw/fgc/codes/C024_63_MainProg/fieldbus RFNA.866.06.ETH2
#   send_code_to_device.py sw/fgc/codes/C024_63_MainProg/fieldbus/1618216435/C024_63_MainProg RFNA.866.06.ETH2
if __name__ == "__main__":
    cli()
