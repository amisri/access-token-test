"""
Returns a string containing gateways and devices of each gateway of a certain class
"""
import pyfgc_name

import argparse

def gws_for_fgc_class(class_id: int) -> None:
    pyfgc_name.read_name_file()
    pyfgc_name.read_group_file()
    
    gws = dict()
    for dev, dev_dict in pyfgc_name.devices.items(): 
        try:
            dev_dict["class_id"]

        except KeyError:
            continue

        if dev_dict["class_id"] == class_id:
            gateway = dev_dict["gateway"]

            try:
                gws[gateway]

            except KeyError:
                gws[gateway] = list()
                print(gateway)

            gws[gateway].append(dev)
            print(f'\t{dev}')

def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=gws_for_fgc_class)

    parser.add_argument('class_id', type=int, help='Class ID')

def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)

