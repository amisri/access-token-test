"""
Returns a string containing the gateways managing FGCs of a certain class. 

The script uses the internal structures of pyfgc_name to find out all gateways
that manage devices of a certain class.
"""
import pyfgc_name

import argparse

def targets_for_class(class_id: int) -> None:    
    targets = set()
    pyfgc_name.read_name_file()
    pyfgc_name.read_group_file()

    for gw, gw_obj in pyfgc_name.gateways.items():
        for channel in gw_obj["channels"]:
            try:
                if channel["class_id"] == class_id:
                   targets.add(pyfgc_name.gateways[gw]["groups"][0])

            except TypeError:
                pass
    
    print("\n".join(sorted(targets)))  

def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=targets_for_class)

    parser.add_argument('class_id', type=int, help='FGC/FGCD class number')

def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)
