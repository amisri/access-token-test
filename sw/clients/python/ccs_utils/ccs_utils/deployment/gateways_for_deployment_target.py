import pyfgc_name

import argparse
import sys
from typing import Dict, List

def gws_for_target(group_dict: Dict, gws: List) -> List:
    for v in group_dict.values():
        if isinstance(v, dict):
            gws_for_target(v, gws)

        else:
            gws += group_dict["gateways"]

def gateways_for_deployment_target(dep_target: str) -> None:
    pyfgc_name.read_name_file()
    pyfgc_name.read_group_file()

    try:
        pyfgc_name.groups[dep_target]

    except KeyError:
        print(f"Deployment target {dep_target} not found!")
        sys.exit(2)

    
    gws = list()
    gws_for_target(pyfgc_name.groups[dep_target], gws)
    print('\n'.join(gws))

def configure_parser(parser: "argparse.ArgumentParser"):
    parser.description = __doc__
    parser.set_defaults(handler=gateways_for_deployment_target)

    parser.add_argument('dep_target', type=str, help='Deployment target (area)')

def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)
