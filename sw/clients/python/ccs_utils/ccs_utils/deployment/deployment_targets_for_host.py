"""
Returns the deployment group to which a hostname belongs.

The script uses the 'name' file and the 'group' file to get all the data
related to devices and groups. Given a hostname, it returns the group to which 
it is attached. 
"""
import pyfgc_name

import argparse
import sys

def targets_for_host(host_name: str) -> None:
    pyfgc_name.read_name_file()
    pyfgc_name.read_group_file()

    try:
        group = pyfgc_name.gateways[host_name]["groups"][0]

    except KeyError:
        print(f"No group defined for gateway: {host_name}! Exiting...")
        sys.exit(2)
    
    else:
        print(group)


def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=targets_for_host)

    parser.add_argument('host_name', help='Name of the host')


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)

