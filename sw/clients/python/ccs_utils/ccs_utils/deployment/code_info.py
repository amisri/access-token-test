# Name:     code_info.py
# Purpose:  Display info block for FGC code files
#
# Notes: Migrated to python from /user/pclhc/bin/perl/code_info.pl
#
# [TESTING]
# args for ADE:
# C002_31_IDProg C011_60_BootProg C012_60_PLD_7_ANA101 C013_60_PLD_7_ANA103 C014_60_PLD_14_ANA101 C015_60_PLD_14_ANA103 C018_60_IDDB C019_60_PTDB C020_60_SysDB C021_60_CompDB C022_60_DIMDB C024_63_MainProg


import argparse
import os
import sys
from stat import *
import struct
from datetime import datetime


def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)


def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=code_info)

    parser.add_argument('filenames', metavar='N', type=str, nargs='+', help='codenames to retrieve code version')


def code_info(filenames: list) -> None:

    #get the lenght of the longest filename
    max_len = 0
    for codename in filenames:
        max_len = max(max_len, len(codename))

    for codename in filenames:

        pack_template = ">LBBHH"
        info_size = len(struct.pack(pack_template, 1, 2, 3, 4, 5))

        mode = os.stat(codename).st_mode
        size = os.stat(codename).st_size

        # Check that file is a valid code file
        if not S_ISREG(mode):
            print(codename, 'is not a file')
            return
        if size < info_size:
            print('File ' + codename + ' is smaller than info block')
            return

        # Open file and seek to code info
        with open(codename, mode='rb') as file:
            file.seek(size - info_size)

            # Read info block
            bin_info = file.read(info_size)

            # Decode the info block
            version, class_id, code_id, old_version, crc = struct.unpack(pack_template, bin_info)
            date = datetime.fromtimestamp(version).ctime()

            print(f'{codename.ljust(max_len)}: '
                  f'class_id={"{:3d}".format(class_id)}, '
                  f'code_id={"{:3d}".format(code_id)}, '
                  f'crc={hex(crc)}, '
                  f'old_version={"{:5d}".format(old_version)}, '
                  f'version={version} {date}')


if __name__ == "__main__":
    cli()

# EOF