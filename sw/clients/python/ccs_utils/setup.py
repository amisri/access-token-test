"""
setup.py for ccs_utils.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
import os.path
from pathlib import Path
from setuptools import setup, find_packages


HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()

about = {}
with (HERE / 'ccs_utils' / '__version__.py').open() as f:
    exec(f.read(), about)

REQUIREMENTS: dict = {
    'core': [
        'cx_Oracle',
        'pyfgc>=1.3.2',
        'pyfgc_const>=0.0.5',
        'pyfgc_name>=1.6.0',
        'pyfgc-test-framework>=1.1.0',
    ],
    'test': [
        'pytest',
        'hypothesis',
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
        'acc_py_sphinx',
        'sphinx_rtd_theme',
    ],
}


def compute_console_scripts(directory: str, console_script_prefix):
    console_scripts = []

    directory = Path(directory)
    for script in directory.glob('**/*.py'):
        if str(script).startswith(str(directory / 'tests/')):
            continue

        if script.name.startswith('_'):
            continue

        with script.open('rt') as fh:
            contents = fh.read()

        # Only support modules that have a "cli" function.
        if not 'def cli():' in contents:
            continue

        # Transform file path to module path, e.g. deployment/send_code.py -> deployment.send_code
        name = script.relative_to(directory).with_suffix('')
        module_path = '.'.join(name.parts)

        # CLI name is prefixed with "ccs.", while the full module path starts with "ccs_utils."
        cli_name = console_script_prefix + module_path
        full_module_path = f'{directory.name}.{module_path}'
        console_scripts.append(f'{cli_name}={full_module_path}:cli')

    return console_scripts


setup(
    name='ccs_utils',
    version=about['__version__'],
    author=about['__authors__'],
    author_email=about['__emails__'],
    description='SHORT DESCRIPTION OF ccs_utils',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/ccs_utils/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/ccs_utils',
    },

    packages=find_packages(),
    python_requires='>=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    entry_points={
        'console_scripts': compute_console_scripts('ccs_utils', 'ccs.')},
)

