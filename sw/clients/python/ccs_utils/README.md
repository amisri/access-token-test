# ccs-utils

An assortment of utilities for SY-EPC-CSS, mostly related to FGC/RegFGC3.

Each of the sub-packages of ccs_utils which define a cli function, is exposed as a command-line-interface. 
It can also be used directly from Python for modular re-use and testing. For example, since the module
ccs_utils.database.list_all has a cli function, it also has a corresponding command-line-interface:
$ ccs.database.list_all --help
...

## Installation

This module is pip installable. Once released, you could:
`pip install ccs_utils`
This will make all of the command line scripts available from the Python
environment's bin directory.

## How to include a new utility

1. Use one of the already existing packages in the ccs_utils directory. If your new utility requires a new one, create another folder. 
```bash
mkdir ccs_utils/my_new_utility
touch ccs_utils/my_new_utility/__init__.py
```
2. Write your new utility in the new package
```bash
touch ccs_utils_my_new_utility/new_script.py
```
3. Every utility should have the following components:
  1. A 'cli' function with the following body:
```python
def cli():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args().__dict__.copy()
    args.pop('handler')(**args)
```

  2. A 'configure_parser' function with at least the following body:
```python
def configure_parser(parser: "argparse.ArgumentParser") -> None:
    parser.description = __doc__
    parser.set_defaults(handler=my_new_utility_function)

    parser.add_argument('host_name', help='Name of the host')
```

TODO: What is the motivation for separating `configure_parser`?

  3. The utility function entry point:
```python
def my_new_utility_function():
    pass
```

## How to call a new utility

After the package is installed again with the new utility, type `ccs` in the command line and all existing utilities will be available
for the command autocompletion. 
```bash
[env] cghabrou@MACTE25303:~/Work/projects/fgc/sw/utilities/python (master)
$ ccs.
ccs.database.list_all                       ccs.deployment.deployment_targets_for_host  ccs.regfgc3.blob_migrator
```

