# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.0.12 - 2023-04-28
### Added
- ccs.regfgc3.params_dump: Change output format of '-v props' to match FGC response format.
- ccs.regfgc3.params_dump: Add output format '-v conf' to print params in FGC config file format.

## 0.0.11 - 2023-04-27
### Added
- ccs.deployment.code_versions: Added support for inspecting codes loaded on gateways
- ccs.regfgc3.params_dump: Use public fgcdb user for get query to avoid password request
### Fixed
- ccs.deployment.code_versions: Device names/patterns are now case-insensitive
- ccs.regfgc3.params_dump: convert device names to capital letters. Needed for DB match.

## 0.0.10 - 2023-03-09
### Added
- New script ccs.deployment.code_versions

### Fixed
- Compatibility with latest pyfgc-test-framework

## 0.0.9 - 2021-11-17
### Fixed
- Compatibility with latest pyfgc-test-framework

## 0.0.8 - 2021-09-30
### Added
- deployment.send_code_to_device is now able to program devices in BOOT mode

### Fixed
- setup.py would fail on Windows due to assuming `/` as the directory separator character

## 0.0.7.dev1 - 2021-09-20
### Fixed
- Fixed imports in blame test files

## 0.0.7.dev0 - 2021-09-20
### Changed
- Make package structure consistent to our other Python packages and compatible with automatic releasing via GitLab CI

## 0.0.6 - 2021-09-20
### Added
- deployment.send_code_to_device: A tool to install a given MainProg build on a specific FGC

### Changed
- Adjust db host of r3blame.py to dynamic name acccon-s.cern.ch, which fixes resolving issues with the previous host name

## 0.0.5 - 2020-08-06

## 0.0.4 - 2020-07-20

## 0.0.3 - 2020-05-25
### Added
- deployment.code_info: migrated from code_info.pl, displays info block for FGC code files

## 0.0.2 - 2020-05-15
### Changed
- Adapt blame to new location of credential files
