"""
setup.py for program_manager.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages

HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()

about = {}
with (HERE / 'program_manager' / '__version__.py').open() as f:
    exec(f.read(), about)

REQUIREMENTS: dict = {
    'core': [
        'cx_Oracle>=7.2',
        'docopt>=0.6',
        'pyfgc~=2.0.1',
        'pyfgc_statussrv>=1.0',
        'ccs_sso_utils>=1.1.2',
        'sentry-sdk',
        'termcolor>=1.1',
    ],
    'test': [
        'pytest',
        'hypothesis',
    ],
    'dev': [
        # type skeletons for dependent packages
        "types-docopt",
        "types-requests",
        "types-termcolor",
    ],
    'doc': [
        'sphinx',
    ],
}

setup(
    name='program-manager',
    version=about['__version__'],

    author=about['__authors__'],
    author_email=about['__emails__'],
    description='RegFgc3 program manager',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/program_manager/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/program_manager',
    },

    packages=find_packages(),
    python_requires='>=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    # TODO: data_files is deprecated in favor of package_data
    data_files=[("program_manager", ["program_manager_launcher.sh", "data/pm_config.cfg"])],
    package_data={"program_manager.tests": ["data/*"]},
    entry_points=dict(console_scripts=[
        "program-manager=program_manager.pm_main:main",
        "regfgc3_programmer=program_manager.regfgc3_programmer:main",
    ])
)
