# RegFGC3 Program Manager

Documentation:
- https://wikis.cern.ch/display/TEEPCCCS/RegFGC3+program+manager
- [EDMS 1607937: RegFGC3 Remote Download - Note](https://edms.cern.ch/document/1607937)

The project follows [keep a changelog](https://keepachangelog.com/en/1.0.0/) & [The Twelve-Factor App](https://12factor.net/) guidelines.

Caveat: testing requires the Oracle Client native libraries

## Running the test suite + type check (Mypy)

    make test

## Check formatting of source and to reformat (via Black)

    make format
    make format-apply

## Check test coverage

coverage.xml is generated automatically. You can use the VS Code extension
[Coverage Gutters](https://marketplace.visualstudio.com/items?itemName=ryanluker.vscode-coverage-gutters)
to visualize it while editing.

## Module dependency graph

    make pydeps && open program_manager.svg

## Deploy program_manager_test on cs-ccr-abpo1 from current source

    make deploy-test-env

## Building container image locally

Don't even think about it -- use Kaniko in CI, both for `:latest` and tagged releases.
