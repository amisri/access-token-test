'''
Usage send_incremental_bins <fgc> <min_size_bytes> <max_size_bytes> <bin_increment_bytes>
'''
import binascii
import logging
import os
import string
import sys

import pyfgc

BYTES_IN_WORD             = 4
LIMIT_GW_CMD_WORDS        = 66100
MEM_DSP_START_ADD         = 0x80100000
LIMIT_DEBUG_PROP_RSP_SIZE = 0x80
DSP_MEM_DEBUG_PROP        = "FGC.DEBUG.MEM.DSP"

logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
logger = logging.getLogger()

def _compare_sent_received(sent, received, bin_size):
    up_to = bin_size // 4
    received_cut = received[:up_to]
    if sent == received_cut:
        logger.info('Sent == received')

    else:
        logger.warning('Sent != received')
        print(f'{"BIN SIZE":>20s}{bin_size:20}')
        print(f'{"WORD":>20s}{"MEMORY ADDRESS":>20s}{"SENT":>20s}{"RECEIVED":>20s}')

        for i, value in enumerate(sent):
            if value != received_cut[i]:
                print(
                f'{i:>20d}{MEM_DSP_START_ADD + i*4:>20X}{value:>20}{received_cut[i]:>20}')

def _get_mem_content(fgc, bin_size):
    mem_dump = list()
    
    for mem_pos in range(MEM_DSP_START_ADD, MEM_DSP_START_ADD + bin_size, LIMIT_DEBUG_PROP_RSP_SIZE):
        fgc.set(DSP_MEM_DEBUG_PROP, mem_pos)
        partial_bin = fgc.get(DSP_MEM_DEBUG_PROP)

        # We receive something in this format: exp_value:hex_value, exp_value:hex_value, ...
        mem_dump += [value.split(":")[1].upper()
                    for value in partial_bin.value.split(",")]
    
    return mem_dump

def _send_bin_file(fgc, bin_file):
    logger.info(f'Sending binary file...')

    for i in range(0, len(bin_file), LIMIT_GW_CMD_WORDS):
        prop = f'REGFGC3.PROG.BIN[{i},]'
        value = ','.join(bin_file[i:i+LIMIT_GW_CMD_WORDS])
        r = fgc.set(prop, value)
        try:
            r.value

        except pyfgc.FgcResponseError:
            logger.error(f'{r.err_code}: {r.err_msg}')
            print(f'{r.err_code}: {r.err_msg}')

def _generate_random_binary(size):
    size_words = size // 4
    return ['0X'+binascii.hexlify(os.urandom(BYTES_IN_WORD)).decode().upper() for i in range(size_words)]

def compare_incremental_bins(fgc_name, min_size, max_size, inc_size):
    for bin_size in range(min_size, max_size, inc_size):
        logger.info(f'Generate bin file of size {bin_size} bytes')

        bin_file = _generate_random_binary(bin_size)

        with pyfgc.fgc(fgc_name) as fgc:
            _send_bin_file(fgc, bin_file)
            mem_dump = _get_mem_content(fgc, bin_size)

        _compare_sent_received(bin_file, mem_dump, bin_size)

if __name__ == '__main__':
    try:
        fgc = sys.argv[1]

    except (IndexError):
        print(__doc__)
        sys.exit(2)
    
    try:
        min_size = int(sys.argv[2])
        max_size = int(sys.argv[3])
        inc_size = int(sys.argv[4])
    
    except (IndexError, ValueError):
        print(__doc__)
        sys.exit(2)
    
    compare_incremental_bins(fgc, min_size, max_size, inc_size)
