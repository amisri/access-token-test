"""
This module implements the RegFGC3 programming state machine.
It is somewhat tricky, since the modes and states of the FSM do not match 1:1.

To be understood if there is pre-existing documentation of the protocol between the PM and FGC3.
"""

import logging

import time
from typing import Dict, Optional

from binascii     import hexlify

import program_manager.fw_file_utils as ff_utils
import pyfgc

from program_manager.model import BlobRepository, ProgramInfo
from program_manager.pyfgc_wrapper import pyfgc_connect
from program_manager.structured_logging.model import DeviceReport

CHARS_PER_WORD       = 8
FW_FILE_LIMIT_BYTES  = 4194304
LIMIT_GW_CMD_WORDS   = 250


# TODO: this should be in pyfgc, not here
def _assert_ok(resp: pyfgc.FgcResponse):
    # For protocol="rda", only err_msg is set
    if resp.err_code != "" or resp.err_msg != "":
        raise pyfgc.FgcResponseError(f"{resp.err_code}: {resp.err_msg}")


class UnkownVariantError(Exception):
    pass


# Note that the state-specific classes below do not implement any transition logic -- this is managed
# completely by the STATE_TO_MODE_TO_INTERIM_STATES table at the end.

class PmState:
    def __init__(self, logger, name=""):
        self.name = name
        self._logger = logger
        self._debug_props_list = list()

    def run(self, fgc_session, **kwargs):
        self._logger.info(f"Entering state {self.name}")
        fgc_state  = fgc_session.get("REGFGC3.PROG.FSM.STATE")
        last_state = fgc_session.get("REGFGC3.PROG.FSM.LAST_STATE")
        try:
            _ = fgc_state.value
            _ = last_state.value

        except pyfgc.FgcResponseError as fe:
            self._logger.warning(f"{fe}")

        else:
            if fgc_state.value != self.name:
                err_msg_list = [f"{fgc_session.fgc} expected state {self.name} but got {fgc_state.value} (last state: {last_state.value})"]
                
                for p in self._debug_props_list:
                    r = fgc_session.get(p)
                    try:
                        r.value

                    except pyfgc.PyFgcError as pe:
                        err_msg_list.append(f"Could not get value for debug property {p}: {pe}")

                    else:
                        err_msg_list.append(f"{p}: {r.value}")

                raise RuntimeError('\n'.join(err_msg_list))

            self._logger.info(f"FGC PM FSM state {self.name} processed successfully")

    def __repr__(self):
        return f"PmState('{self.name}')"


class PmStateUninitialized(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="UNINITIALIZED")


class PmStateWaiting(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="WAITING")

    def run(self, fgc_session, **kwargs):
        super().run(fgc_session, **kwargs)


class PmStateTransferring(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="TRANSFERRING")

    def run(self, fgc_session: pyfgc.FgcSession, **kwargs):
        super().run(fgc_session, **kwargs)

        blob_repo: BlobRepository
        slot, board, device, variant, var_revision, api_revision, bin_crc, blob_repo, fw_file_loc, loose = (
            kwargs["slot"],
            kwargs["board"],
            kwargs["device"],
            kwargs["variant"],
            kwargs["var_revision"],
            kwargs["api_revision"],
            kwargs["bin_crc"],
            kwargs["blob_repo"],
            kwargs["fw_file_loc"],
            kwargs["loose"],)

        response_data = blob_repo.get_blob_by_path(fw_file_loc)
        fw_file_size = len(response_data)

        if fw_file_size == 0:
            raise RuntimeError(f"File's {fw_file_loc} is empty. Nothing to do...")

        if fw_file_size > FW_FILE_LIMIT_BYTES:
            raise RuntimeError(f"File's {fw_file_loc} size {fw_file_size} over limit {FW_FILE_LIMIT_BYTES}")

        # Check against header
        header_info, bin_crc  = ff_utils.extract_data_from_file_header_trailer(response_data)
        adapter_info          = ProgramInfo(board, device, variant, var_revision, api_revision)

        try:
            ff_utils.check_header_data_consistency(adapter_info, header_info)

        except AssertionError as ae:
            if not loose:
                raise AssertionError(f"Adapter data != FW file header data: {ae}") from ae

        packet = list()
        for word in range(0, fw_file_size, 4):
            # Bin into hex and decode
            ascii_word = hexlify(response_data[word:word + 4]).decode()

            # Add padding
            ascii_word += "".join(["0"] * (CHARS_PER_WORD - len(ascii_word)))
            packet.append(hex(int(ascii_word, 16)))

        _assert_ok(fgc_session.set("REGFGC3.PROG.SLOT"             ,slot))
        _assert_ok(fgc_session.set("REGFGC3.PROG.DEVICE"           ,device))

        resp = fgc_session.set("REGFGC3.PROG.VARIANT", variant)
        if "unknown sym" in resp.err_msg:
            raise UnkownVariantError(f"Variant rejected by FGC: '{variant}'")
        elif resp.err_code != "" or resp.err_msg != "":
            raise pyfgc.FgcResponseError(f"{resp.err_code}: {resp.err_msg}")

        _assert_ok(fgc_session.set("REGFGC3.PROG.VARIANT_REVISION" ,var_revision))
        _assert_ok(fgc_session.set("REGFGC3.PROG.API_REVISION"     ,api_revision))
        _assert_ok(fgc_session.set("REGFGC3.PROG.BIN_SIZE_BYTES"   ,fw_file_size))
        _assert_ok(fgc_session.set("REGFGC3.PROG.BIN_CRC"          ,bin_crc))

        for i in range(0, len(packet), LIMIT_GW_CMD_WORDS):
            _assert_ok(fgc_session.set(f"REGFGC3.PROG.BIN[{i},]", ",".join(packet[i:i + LIMIT_GW_CMD_WORDS])))

        super().run(fgc_session, **kwargs)


class PmStateTransferred(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="TRANSFERRED")

    def run(self, fgc_session, **kwargs):
        super().run(fgc_session, **kwargs)


class PmStateGetProgInfo(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="GET_PROG_INFO")
        self._debug_props_list = ["REGFGC3.PROG.DEBUG.BOARD_ERROR"]

    def run(self, fgc_session, **kwargs):
        kwargs.update({"debug_prop_list":self._debug_props_list})
        super().run(fgc_session, **kwargs)


class PmStateProgramming(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="PROGRAMMING")

    def run(self, fgc_session, **kwargs):
        super().run(fgc_session, **kwargs)


class PmStateProgramCheck(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="PROG_CHK")
        self._debug_props_list = ["REGFGC3.PROG.BIN_CRC"]

    def run(self, fgc_session, **kwargs):
        kwargs.update({"debug_prop_list":self._debug_props_list})
        super().run(fgc_session, **kwargs)


class PmStateProgrammed(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="PROGRAMMED")
        # Arguably the CRC validation would fail when the FGC3 transitions from PROG_CHK to PROGRAMMED,
        # so we will have already advanced to PmStateProgrammed by then.
        #
        # See regfgc3ProgFsm.c:
        # stateProgProgramCheck calls regFgc3ProgVerifyProgram and sets regfgc3_prog_fsm.state_error = true
        # if the card reports a verification failure.
        # However, this will only be picked up by XXtoER the next time regFgc3ProgFsmProcess is called,
        # which will only happen when the MODE property is set again.
        self._debug_props_list = ["REGFGC3.PROG.BIN_CRC", "REGFGC3.PROG.DEBUG.BOARD_ERROR"]

    def run(self, fgc_session, **kwargs):
        super().run(fgc_session, **kwargs)


class PmStateCleanUp(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="CLEAN_UP")

    def run(self, fgc_session, **kwargs):
        super().run(fgc_session, **kwargs)


class PmStateError(PmState):
    def __init__(self, logger):
        super().__init__(logger, name="ERROR")

    def run(self, fgc_session, **kwargs):
        super().run(fgc_session, **kwargs)


class ProgramManagerFsm:
    # This table maps from source state to the mode to use + the sequence of interim states to be expected.
    # For example, to get from TRANSFERRED to PROGRAMMED, we pass through 4 different states while always setting the same MODE
    # (the setting of MODE is what actually triggers a FSM update on the FGC3 side)
    #
    # It is not clear why the values in the outer dictionary are further dictionaries.
    STATE_TO_MODE_TO_INTERIM_STATES: Dict[str, dict] = {
        "UNINITIALIZED"       : {"WAITING"             : [PmStateWaiting]},
        "WAITING"             : {"TRANSFERRED"         : [PmStateTransferring, PmStateTransferred]},
        "TRANSFERRED"         : {"PROGRAMMED"          : [PmStateGetProgInfo, PmStateProgramming, PmStateProgramCheck, PmStateProgrammed]},
        "PROGRAMMED"          : {"CLEAN_UP"            : [PmStateCleanUp]},
        "CLEAN_UP"            : {"WAITING"             : [PmStateWaiting]},
        "ERROR"               : {"CLEAN_UP"            : [PmStateCleanUp]}
    }

    # this looks like a clumsy way to extract the 2nd column from the table above?
    VALID_MODES = {k for inner in STATE_TO_MODE_TO_INTERIM_STATES.values() for k in inner.keys()}

    # FIXME: must refactor prog_data from being an opaque tuple
    def __init__(self, prog_data, fgc_session, init_state=PmStateUninitialized, loose=False, logger=None, device_report: Optional[DeviceReport] = None):
        self.prog_data_dict = dict(zip(("converter",
                                "slot",
                                "board",
                                "device",
                                "variant",
                                "var_revision",
                                "api_revision",
                                "bin_crc",
                                "blob_repo",
                                "fw_file_loc"), prog_data))

        self.prog_data_dict["loose"] = loose
        self._fgc_session         = fgc_session
        self._fgc_session_created = False
        self._mode                = "UNINITIALIZED"
        self._logger              = logger or logging.getLogger("pm_main." + __name__)
        self._device_report       = device_report
        self._current_state       = init_state(self._logger)

        self._set_valid_fgc_connection()

    def process(self):
        try:
            assert isinstance(self._current_state, PmStateUninitialized)

        except AssertionError:
            self._logger.exception(f"Initial FSM state '{self.state}' should be 'UNINITIALIZED'")
            return

        mode_sequence          = list(ProgramManagerFsm.VALID_MODES)
        error_during_reprogram = False

        # We loop while the mode sequence is not empty, but we never actually look at its elements!
        # Basically we let the state transition table guide us.
        while mode_sequence:
            mode = list(ProgramManagerFsm.STATE_TO_MODE_TO_INTERIM_STATES[self.state].keys())[0]
            self._logger.debug(f"{self.prog_data_dict['converter']}: processing mode {mode} in state {self.state}")

            try:
                self._process_mode(mode, self._fgc_session)

            except (RuntimeError, KeyError) as e:
                # TODO: is there any value in this message?
                self._logger.exception("Error during FSM processing")

                # Save the error message into the device report if we are building one
                if self._device_report is not None:
                    self._device_report.errors.append(type(e).__name__ + ": " + str(e))

                # TODO: is there any point in trying a second time? how often does it actually help?
                #       will the answer differ for PM vs regfgc3_programmer?

                # Exit if an error was already registered
                if error_during_reprogram:
                    mode_sequence = []

                # Try to recover if first time error happens
                else:
                    error_during_reprogram = True
                    self._current_state = PmStateError(self._logger)
                    mode_sequence = ["CLEAN_UP"]

                    # TODO: back off for a few seconds

            else:
                _ = mode_sequence.pop(0)

        # Try to leave the FGC FSM in its initial state
        try:
            self._process_mode("WAITING", self._fgc_session)

        except (RuntimeError, KeyError) as e:
            raise RuntimeError(e) from e

        if error_during_reprogram:
            raise RuntimeError("Error during reprogramming after recovery attempt")

    def _set_valid_fgc_connection(self):
        # Create connection if the client did not do it
        # TODO: document why/when this would be the case

        if self._fgc_session:
            return

        try:
            self._fgc_session = pyfgc_connect(self.prog_data_dict["converter"])

        except pyfgc.PyFgcError as pe:
            # TODO: why?
            raise RuntimeError(pe) from pe

        else:
            self._fgc_session_created = True

    def reset(self):
        self._mode          = "UNINITIALIZED"
        self._current_state = PmStateUninitialized(self._logger)

        # Only close the connection if it was created here (otherwise it is the client's connection)
        if self._fgc_session_created:
            try:
                self._fgc_session.disconnect()

            except pyfgc.PyFgcError as pe:
                self._logger.exception(f"Could not close connection to the FGC: {pe}")

    def _process_mode(self, target_mode, fgc_session):
        self._mode = target_mode
        interim_states = ProgramManagerFsm.STATE_TO_MODE_TO_INTERIM_STATES[self.state][target_mode]

        for interim_state in interim_states:
            r = fgc_session.set("REGFGC3.PROG.FSM.MODE", target_mode)
            try:
                r.value

            except pyfgc.FgcResponseError:
                self._logger.warning(f"Could not set mode {target_mode}: {fgc_session.get('REGFGC3.PROG')}. FGC response: {r.err_code}, {r.err_msg}")

            next_state = interim_state(self._logger)

            if self._current_state.name == next_state.name:
                pass

            else:
                start = time.time()
                next_state.run(fgc_session, **self.prog_data_dict)
                self._logger.debug(f'Processing state {next_state.name} took {time.time() - start:.3f} seconds')
                self._current_state = next_state

    @property
    def state(self):
        return self._current_state.name

    @property
    def mode(self):
        return self._mode

    #TODO: the setter is only valid for commissioning/testing.
    @mode.setter
    def mode(self, new_mode):
        mode_for_current_state = list(ProgramManagerFsm.STATE_TO_MODE_TO_INTERIM_STATES[self.state].keys())[0]
        try:
            assert new_mode == mode_for_current_state

        except AssertionError as ae:
            raise AssertionError(f"Mode {new_mode} not allowed for current state {self.state}") from ae

        else:
            self._process_mode(new_mode, self._fgc_session)

    def __str__(self):
        return f"<ProgramManagerFsm: {self.mode}, {self.state}>"
