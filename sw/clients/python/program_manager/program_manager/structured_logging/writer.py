from datetime import datetime
import json
import logging
import logging.handlers
import os
from pathlib import Path
import re
from typing import List, Union

from .model import ServerRestart, TaskReport


logger = logging.getLogger("pm_main." + __name__)


class ModifiedTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    """
    Based on logging.handlers.TimedRotatingFileHandler, but even the current log file contains a datestamp.
    This avoids the need for renaming files, which seems to interfere with HTTPD file handle caching on NFS.
    """

    _dir: Path
    _prefix: str
    _suffix: str

    _date_regex = re.compile(r"^\d{4}-\d{2}-\d{2}$", re.ASCII)

    def __init__(self, filename: Path, backup_count: int, get_time_function=None):
        self._dir = filename.parent
        self._prefix = filename.stem + "."
        self._suffix = filename.suffix
        self._backup_count = backup_count
        self._now_function = get_time_function or datetime.now

        super().__init__(filename=self._make_filename(self._now_function()))

    def _make_filename(self, t: datetime) -> str:
        return os.path.join(self._dir, self._prefix + t.strftime("%Y-%m-%d") + self._suffix)

    def emit(self, record):
        try:
            expected_filename = self._make_filename(self._now_function())

            if self.baseFilename != expected_filename:
                if self.stream:
                    self.stream.close()
                    self.stream = None

                if self._backup_count > 0:
                    for s in self._get_files_to_delete():
                        s.unlink()

                self.baseFilename = expected_filename

            logging.FileHandler.emit(self, record)
        except Exception:
            self.handleError(record)

    def _get_files_to_delete(self) -> List[Path]:
        """
        Determine the files to delete when rolling over.

        More specific than the earlier method, which just used glob.glob().
        """
        matches = []

        plen = len(self._prefix)
        slen = len(self._suffix)

        for path in self._dir.iterdir():
            fileName = path.name
            logger.info("Test %s vs prefix '%s'='%s' & suffix '%s'='%s'", path, fileName[:plen], self._prefix, fileName[len(fileName)-slen:], self._suffix)

            if fileName[:plen] == self._prefix and fileName[len(fileName)-slen:] == self._suffix:
                date = fileName[plen:len(fileName)-slen]

                if self._date_regex.match(date):
                    matches.append(path)
                    logger.debug("Match %s", fileName)
                else:
                    logger.debug("Ignore %s(%s)", fileName, date)

        # Sort results from oldest, return all but N most recent
        if len(matches) < self._backup_count:
            result = []
        else:
            matches.sort()
            result = matches[:len(matches) - self._backup_count]

        return result


class Writer:
    _handler: ModifiedTimedRotatingFileHandler

    def __init__(self, log_file_path: Path, retention_days: int, get_time_function=None):
        # Notes:
        # - we use the standard logging module to handle thread synchronization + log rotation for us
        # - we do not guarantee sequential timestamps in log
        self._handler = ModifiedTimedRotatingFileHandler(log_file_path,
                                                         backup_count=retention_days,
                                                         get_time_function=get_time_function)

    def close(self):
        self._handler.close()

    def write(self, event: Union[ServerRestart, TaskReport]):
        try:
            self._write(event)
        except RuntimeError:
            logger.error("Error in writing to structured log", exc_info=True)
            pass

    def _write(self, event: Union[ServerRestart, TaskReport]):
        model = event.to_dict()
        model_json = json.dumps(model)
        record = logging.makeLogRecord(dict(levelno=logging.INFO, msg=model_json))
        self._handler.handle(record)
