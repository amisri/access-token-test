from dataclasses import dataclass, field
import datetime
from enum import auto, Enum
from typing import List, Optional, Tuple


class DevResult(Enum):
    UNKNOWN = auto()
    UP_TO_DATE = auto()
    NO_ACTION = auto()
    TO_BE_PROGRAMMED = auto()
    PROGRAMMED = auto()
    PROGRAMMING_FAILED = auto()
    ABANDONED = auto()
    CANNOT_REPROGRAM = auto()

    NOT_IN_CONVERTER = auto()
    NOT_IN_DB = auto()


@dataclass
class DevFwVersion:
    variant: str
    var_rev: str
    api_rev: str


@dataclass
class DeviceReport:
    device: str

    fw_before: Optional[DevFwVersion]
    fw_after: Optional[DevFwVersion]
    fw_db: Optional[DevFwVersion]

    res: DevResult

    errors: List[str]
    warnings: List[str]

    WARNING_VARIANT_REVISIONS_MISMATCH = "VARIANT_REVISIONS_MISMATCH"

    @staticmethod
    def from_dict(model: dict) -> "DeviceReport":
        return DeviceReport(
            device=model["device"],
            fw_before=DevFwVersion(**model["fw_before"]) if model["fw_before"] is not None else None,
            fw_after=DevFwVersion(**model["fw_after"]) if model["fw_after"] is not None else None,
            fw_db=DevFwVersion(**model["fw_db"]) if model["fw_db"] is not None else None,

            res=DevResult[model["res"]],

            errors=model.get("errors", []),
            warnings=model.get("warnings", []),
        )

    def to_dict(self) -> dict:
        return dict(
            device=self.device,

            fw_before=self.fw_before.__dict__ if self.fw_before else None,
            fw_after=self.fw_after.__dict__ if self.fw_after else None,
            fw_db=self.fw_db.__dict__ if self.fw_db else None,

            res=self.res.name,

            errors=self.errors,
            warnings=self.warnings,
        )


@dataclass
class SlotReport:
    slot: str
    board: str
    devices: List[DeviceReport]

    state_before: Optional[str]
    state_after: Optional[str]

    errors: List[str]
    warnings: List[str]

    WARNING_DID_NOT_SWITCH_TO_PRODUCTION_BOOT = "DID_NOT_SWITCH_TO_PRODUCTION_BOOT"
    WARNING_EXPECTED_BOARD_NOT_FOUND = "EXPECTED_BOARD_NOT_FOUND"
    WARNING_EXPECTED_DIFFERENT_BOARD = "EXPECTED_DIFFERENT_BOARD"
    WARNING_UNEXPECTED_BOARD_FOUND = "UNEXPECTED_BOARD_FOUND"

    @staticmethod
    def from_dict(model: dict) -> "SlotReport":
        return SlotReport(
            slot=model["slot"],
            board=model["board"],
            devices=[DeviceReport.from_dict(device_model) for device_model in model["devices"]],

            state_before=model.get("state_before", None),
            state_after=model.get("state_after", None),

            errors=model.get("errors", []),
            warnings=model.get("warnings", []),
        )

    def get_device_by_name(self, name: str) -> DeviceReport:
        for dev in self.devices:
            if dev.device == name:
                return dev
        else:
            raise KeyError(name)

    def get_or_add_device(self, name: str, default_res: DevResult) -> Tuple[DeviceReport, bool]:
        # Find device if it exists
        for device_report in self.devices:
            if device_report.device == name:
                return (device_report, False)
        else:
            device_report = DeviceReport(device=name, fw_before=None, fw_after=None, fw_db=None, res=default_res,
                                         errors=[], warnings=[])
            self.devices.append(device_report)
            return (device_report, True)

    def to_dict(self) -> dict:
        return dict(
            slot=self.slot,
            board=self.board,
            devices=[device.to_dict() for device in self.devices],

            state_before=self.state_before if self.state_before is not None else None,
            state_after=self.state_after if self.state_after is not None else None,

            errors=self.errors,
            warnings=self.warnings,
        )


@dataclass
class EventBase:
    timestamp: datetime.datetime


@dataclass
class TaskReport(EventBase):
    fgc_name: str
    abandoned: bool
    errors: List[str]
    warnings: List[str]

    slots: List[SlotReport]

    duration_sec: Optional[float] = None

    @staticmethod
    def from_dict(model: dict) -> "TaskReport":
        return TaskReport(
            timestamp=datetime.datetime.fromisoformat(model["timestamp"]),

            fgc_name=model["fgc_name"],
            duration_sec=model["duration_sec"] if "duration_sec" in model else None,  # 1.2.1 compat
            abandoned=model["abandoned"],
            errors=model.get("errors", []),
            warnings=model.get("warnings", []),

            slots=[SlotReport.from_dict(slot_model) for slot_model in model["slots"]],
        )

    def get_or_add_slot(self, slot: str, board_name: str) -> SlotReport:
        for slot_report in self.slots:
            # TODO: if for some slot number the board names disagree, we should report an error
            if slot_report.slot == slot and slot_report.board == board_name:
                return slot_report
        else:
            slot_report = SlotReport(slot=slot, board=board_name, devices=[], state_before=None, state_after=None, errors=[], warnings=[])
            self.slots.append(slot_report)
            return slot_report

    def to_dict(self) -> dict:
        return dict(
            type="TaskReport",
            timestamp=self.timestamp.isoformat(),

            fgc_name=self.fgc_name,
            duration_sec=self.duration_sec,
            abandoned=self.abandoned,
            errors=self.errors,
            warnings=self.warnings,

            slots=[slot.to_dict() for slot in self.slots],
        )

@dataclass
class ServerRestart(EventBase):
    @staticmethod
    def from_dict(model: dict) -> "ServerRestart":
        return ServerRestart(
            timestamp=datetime.datetime.fromisoformat(model["timestamp"]),
        )

    def to_dict(self) -> dict:
        return dict(
            type="ServerRestart",
            timestamp=self.timestamp.isoformat(),
        )
