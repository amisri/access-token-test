"""regfgc3_programmer

Usage:
    regfgc3_programmer.py -h | --help
    regfgc3_programmer.py [-v | --verbosity] [-l | --loose] [-s | --stay-db] [-y | --assumeyes] <converter> <slot> <board> <device> <variant> <var_revision> <api_revision> <fw_repo> <fw_file_name>

Options:
    -h --help       Show this help. 
    -v --verbosity  Increase output verbosity [default: INFO].
    -l --loose      Upgrade FW even if variant in board differs from input argument.   
    -s --stay-db    Do not switch the board to production after reprogramming.  
    -y --assumeyes  Do not ask for confirmation (assume YES)
"""

import logging
import re
import sys
import time
from collections import namedtuple
from logging import handlers
from pathlib import Path
from typing import Optional

import docopt
import termcolor

import program_manager.fw_file_utils as ff_utils
from program_manager.model import BlobRepository, Board, BoardState, Device
import program_manager.pm_fsm        as fsm
import pyfgc

from program_manager.structured_logging.model import DevResult, DeviceReport

DEVICES_LIST  = ["DB", "MF"] + ["DEVICE_" + str(i) for i in range(2, 6)]
LOG_FILE_NAME = "program_manager.log"
MAX_REPROGRAM_ATTEMPTS = 1

_module_logger = logging.getLogger("pm_main." + __name__)

def _configure_logger(verbosity):
    default_severity = verbosity and logging.DEBUG or logging.INFO
    logger = logging.getLogger(__name__)
    logger.setLevel(default_severity)

    LOG_FORMAT = "[%(asctime)s] - [%(levelname)8s](%(module)10s): %(message)s"
    formatter = logging.Formatter(LOG_FORMAT)
    fh = handlers.RotatingFileHandler(LOG_FILE_NAME, maxBytes=1000000, backupCount=10)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)

    ch = logging.StreamHandler()
    ch.setLevel(default_severity)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)
    global _module_logger
    _module_logger = logger
    
def _args_dict_to_tuple(args):
    return (args["<converter>"],
            args["<slot>"],
            args["<board>"],
            args["<device>"],
            args["<variant>"],
            args["<var_revision>"],
            args["<api_revision>"],
            args["<fw_repo>"],
            args["<fw_file_name>"])

def _run_security_checks(cmd_info, fgc_info, hed_info, loose_option):
    # Device in possible values
    global _module_logger

    try:
        assert cmd_info.device in DEVICES_LIST

    except AssertionError:
        _module_logger.critical(f"Command line device {cmd_info.device} is not a valid device. Possible values are {','.join(DEVICES_LIST)}. Exiting...")
        sys.exit(2)

    # board
    try:
        assert fgc_info.board == cmd_info.board

    except AssertionError:
        _module_logger.critical(f"Command line board {cmd_info.board} is different than fgc board {fgc_info.board}. Board programming NOT ALLOWED! Exiting...")
        sys.exit(2)

    try:
        assert fgc_info.device == cmd_info.device

    except AssertionError:
        _module_logger.critical(f"Command line device {cmd_info.device} is different than fgc device {fgc_info.device}. Board programming NOT ALLOWED! Exiting...")
        sys.exit(2)

    # Variant
    try:
        assert fgc_info.variant == cmd_info.variant

    except AssertionError:
        variant_msg = f"Command line variant {cmd_info.variant} is different than fgc variant {fgc_info.variant}."
        if loose_option:
            _module_logger.warning(variant_msg)
        
        else:
            _module_logger.critical(variant_msg + "board programming NOT ALLOWED!. Exiting...")
            sys.exit(2)

    _module_logger.info("Input argumets successfully validated!")

    # File header 
    try:
        ff_utils.check_header_data_consistency(cmd_info, hed_info)

    except AssertionError as ae:
        if not loose_option:
            _module_logger.critical(f"{ae}. Exiting...")
            sys.exit(2)
    
    else:
        _module_logger.info("File header consistency successfully validated!")

    # All OK
    if not loose_option:
        if cmd_info.var_rev == fgc_info.var_rev:
            nothing_todo_msg = f"Nothing to do: cmd line variant {cmd_info.variant} = fgc variant {fgc_info.variant};"
            nothing_todo_msg += f"cmd line var_revision {cmd_info.var_rev} = fgc var_revision {fgc_info.var_rev}. Exiting..."
            _module_logger.info(nothing_todo_msg)
            sys.exit(0)

def _get_fgc_detected(converter, slot, device):
    slot_info = ""
    try:
        _ = pyfgc.set(converter, "REGFGC3.SLOT_INFO", "")
        r = pyfgc.get(converter, "REGFGC3.SLOT_INFO")
        slot_info = r.value

    except pyfgc.FgcResponseError:
        _module_logger.error(f"{r.err_code}: {r.err_msg}")
        sys.exit(2)

    except RuntimeError as re:
        _module_logger.error(re)
        sys.exit(2)

    boards = parse_slot_info(slot_info)
    variant, var_rev, api_rev = [""] * 3

    try:
        boards[slot]

    except KeyError:
        _module_logger.error(f"slot {slot} not found in REGFGC3.SLOT_INFO")
        sys.exit(1)

    if not is_board_in_download_boot(boards[slot]):
        _module_logger.error(f"Board {boards[slot]} is not running in DownloadBoot!")
        sys.exit(1)

    try:
        d = boards[slot]["devices"][device]

    except KeyError as ke:
        _module_logger.error(f"{ke} not found for slot {slot}")
        sys.exit(1)
    
    else:
        dev, variant, var_rev, api_rev = d.Device, d.Variant, d.Var_Rev, d.API_Rev
        return boards[slot]["board"], dev, variant, var_rev, api_rev


def _parse_single_slot(single_slot):
    device_fields_exact = {field.casefold(): field for field in Device._fields}     # this comes in handy a bit later

    single_slot.pop()
    devices          = dict()
    board_info_dict  = dict()

    dev_pos = [i for i, element in enumerate(single_slot) if element.casefold().startswith("device")]
    for i in range(len(dev_pos)):
        idx_tuple = dev_pos[i:i+2]
        if len(idx_tuple) == 1:
            single_device_info = single_slot[idx_tuple[0]:]
        else:
            single_device_info = single_slot[idx_tuple[0]:idx_tuple[1]]

        # Parse device attribute lines formatted like "Var_Rev    303"
        device_dict = {}
        for el in single_device_info:
            # skip empty lines
            if not el.strip():
                continue

            # skip known ignored attributes
            if el.casefold().startswith("git_sha ") or el.casefold().startswith("utc "):
                continue

            tokens = el.split()
            if len(tokens) == 2:
                attribute, value = tokens
                attribute_casefold = attribute.casefold()

                # To get case-insensitive attribute names, we match them by the normalized (case-folded) name,
                # but to be able to instantiate the Device, we need to restore the exact spelling 
                if attribute_casefold in device_fields_exact:
                    attribute_exact = device_fields_exact[attribute_casefold]
                    device_dict[attribute_exact] = value
                else:
                    _module_logger.warning("REGFGC3.SLOT_INFO: Unknown Device attribute `%s` in `%s`", attribute, el)
            else:
                _module_logger.warning("REGFGC3.SLOT_INFO: Invalid syntax: `%s`", el)

        single_device = Device(**device_dict)
        devices.update({single_device.Device: single_device})

    board_info_dict = dict([el.split() for el in single_slot[0:3]])
    board_info_dict["devices"] = devices
    return Board(**board_info_dict)

def is_board_in_download_boot(board: dict) -> bool:
    # Check that board state is "DownloadBoot", that a device named 'DB' exists and is the expected variant

    if board["state"] != BoardState.DOWNLOAD_BOOT:
        return False

    try:
        db_device = board["devices"]["DB"]

    except KeyError:
        return False

    if db_device.Variant != "DOWNLDBOOT_3":
        return False
    
    return True

def parse_slot_info(slot_info_reply):
    si_list        = slot_info_reply.split(",")
    slot_start_pos = [idx for idx, element in enumerate(si_list) if element.startswith("SLOT")]
    boards         = dict()
    
    for i in range(len(slot_start_pos)):
        idx_tuple = slot_start_pos[i:i + 2]
        
        if len(idx_tuple) == 1:
            single_slot_info = si_list[idx_tuple[0]:]
        else:
            single_slot_info = si_list[idx_tuple[0]:idx_tuple[1]]

        single_board = _parse_single_slot(single_slot_info)
        try:
            boards[single_board.SLOT]

        except KeyError:
            boards[single_board.SLOT] = {"board": single_board.BOARD, "state": single_board.STATE, "devices": single_board.devices}
    
    return boards

def program(converter, slot, board, device, variant, var_revision, api_revision, blob_repo: BlobRepository,
            fw_file_name, bin_crc=None, fgc_session=None, loose=False,
        device_report: Optional[DeviceReport] = None):
    """
    Reprogram a board. Retry multiple times if necessary.
    `device_report`, if provided, will be updated with the correct result, if the function returns normally.
    If the function raises, or lets an exception pass through, it is up to the caller to log the error message and set the result.

    This function adds very little over the ProgramManagerFsm but it is called from two different locations (service x stand-alone tool)
    However, the needs of these two might be different enough that it would justify diverging the function.
    (As usual, poor test coverage complicates things)
    """

    global _module_logger
    attempts_count = 0
    
    for n in range(MAX_REPROGRAM_ATTEMPTS):
        # TODO: in service, we need to pas task-specific logger rather than module-level logger
        pm_fsm = fsm.ProgramManagerFsm((converter, slot, board, device, variant, var_revision, api_revision, bin_crc, blob_repo, fw_file_name),
                                        fgc_session,
                                        loose=loose,
                                        logger=_module_logger,
                                        device_report=device_report)
        try:
            pm_fsm.process()

        except fsm.UnkownVariantError:
            raise       # propagate out
        # TODO: understand of logging of exception should really happen on this level and not above/below
        #       (keep in mind that this codepath is used both in service & stand-alone)
        #       For example, EDMS exceptions are logged as "Error during FSM processing", but also trigger a latent
        #       RuntimeError("Error during reprogramming after recovery attempt")
        except (RuntimeError, pyfgc.FgcResponseError) as ex:
            _module_logger.exception(f"Error in %s while reprogramming %s in board %s (attempt %d)", converter, device, board, 1 + n)
            if device_report is not None:
                device_report.errors.append(type(ex).__name__ + ": " + str(ex))
            pm_fsm.reset()
            del pm_fsm
        
        else:
            # TODO: we need to check that the new revision really matches expectation; it seems that downgrading (e.g. 304->303) fails silently
            _module_logger.info(f"{converter}: device {device} on board {board} successfully reprogrammed")
            if device_report is not None:
                device_report.res = DevResult.PROGRAMMED
            attempts_count = n
            break
    
    else:
        _module_logger.critical(f"{converter}: reached maximum programming attempts. Device {device} on {board} was NOT successfully reprogrammed")
        if device_report is not None:
            device_report.res = DevResult.PROGRAMMING_FAILED
        attempts_count = 3

    return attempts_count


# Named ProgramInfo2 to distinguish from model.ProgramInfo (until they are reunited)
ProgramInfo2 = namedtuple("ProgramInfo2", "converter, board, device, variant, var_rev, api_rev")


class FilesystemBlobRepository(BlobRepository):
    _directory: Path

    def __init__(self, directory: Path) -> None:
        super().__init__()

        self._directory = directory

    def get_blob_by_path(self, path: str) -> bytes:
        return (self._directory / path).read_bytes()


def main():
    args = docopt.docopt(__doc__)
    _configure_logger(args["--verbosity"])
    _module_logger.debug(f"Cmd line arguments: {args}")

    ask_confirmation = not args["--assumeyes"]

    fgc_info = ProgramInfo2(args["<converter>"], *_get_fgc_detected(args["<converter>"], args["<slot>"], args["<device>"]))
    cmd_info = ProgramInfo2(args["<converter>"], args["<board>"], args["<device>"], args["<variant>"], args["<var_revision>"], args["<api_revision>"])

    # Run security checks
    _module_logger.info("Running security checks...")
    _module_logger.debug(f"cmd_info: {cmd_info}, fgc_info: {fgc_info}")

    # patch up some args... this is to be done in a better way!
    args["<fw_repo>"] = FilesystemBlobRepository(args["<fw_repo>"])
    args["<fw_file_name>"] = Path(cmd_info.board) / cmd_info.device / cmd_info.variant / args["<fw_file_name>"]

    try:
        fw_file_content = args["<fw_repo>"].get_blob_by_path(args["<fw_file_name>"])
    
    except FileNotFoundError as fe:
        _module_logger.critical(f"{fe}. Exiting...")
        sys.exit(2)

    header_data, bin_crc = ff_utils.extract_data_from_file_header_trailer(fw_file_content)
    hed_info             = ProgramInfo2(args["<converter>"], **header_data._asdict())
    _run_security_checks(cmd_info, fgc_info, hed_info, args["--loose"])

    msg_reprog_basic_info = termcolor.colored(
        f"DEVICE: {fgc_info.device} from BOARD: {fgc_info.board} (slot{args['<slot>']}) in CONVERTER {cmd_info.converter} will be programmed.",
        "red",
        attrs=["bold"])

    msg_reprog_variants = termcolor.colored(
        f"{'VARIANT(old)':<13}: {fgc_info.variant:<13} ---> {'VARIANT(new)':<13}: {cmd_info.variant}",
        "red",
        attrs=["bold"])

    msg_reprog_revisions = termcolor.colored(
        f"{'REVISION(old)':<13}: {fgc_info.var_rev:<13} ---> {'REVISION(new)':<13}: {cmd_info.var_rev}",
        "red",
        attrs=["bold"])

    msg_confirmation = termcolor.colored("PROCEED? [Y/n]", "green", attrs=["bold"])

    _module_logger.warning(msg_reprog_basic_info)
    _module_logger.warning(msg_reprog_variants)
    _module_logger.warning(msg_reprog_revisions)
    _module_logger.warning(termcolor.colored(f"{'Binary file':<13}: {args['<fw_file_name>']}", "red", attrs=["bold"]))

    if ask_confirmation:
        _module_logger.warning(msg_confirmation)

        try:
            confirmation = str(input())

        except KeyboardInterrupt:
            _module_logger.info("Action cancelled by user. Exiting...")
            sys.exit(0)
    
        else:
            if confirmation != "Y" and confirmation.lower() != "n":
                _module_logger.warning("Unknown option. Exiting...")
                sys.exit(2)

            if confirmation.lower() == "n":
                _module_logger.info("Action cancelled by user. Exiting...")
                sys.exit(0)
    
    # Start reprogramming
    ## Unlock FGC3    
    _ = pyfgc.set(cmd_info.converter, "REGFGC3.PROG.LOCK", "DISABLED")

    ## Program
    try:
        program(*_args_dict_to_tuple(args), bin_crc=bin_crc, loose=args["--loose"])

    except RuntimeError as re:  
        _module_logger.error(f"Error while reprogramming {cmd_info.converter}, board {cmd_info.board}, device {cmd_info.device}: {re}")
        sys.exit(2)

    stay_in_db = args["--stay-db"]

    ## Lock FGC3
    with pyfgc.fgc(cmd_info.converter) as fgc:
        fgc.set("REGFGC3.PROG.LOCK", "ENABLED")

        # Switch card to production boot if user wants
        if not stay_in_db:
            fgc.set("REGFGC3.PROG.SLOT", args["<slot>"])
            fgc.set("REGFGC3.PROG.DEBUG.ACTION", "SWITCH")
            
            _module_logger.info("Switching to production boot...")
            time.sleep(5)

        # Rescan crate to get updated data on G REGFGC3.SLOT_INFO
        fgc.set("REGFGC3.SLOT_INFO", "")

if __name__ == "__main__":
    main()
