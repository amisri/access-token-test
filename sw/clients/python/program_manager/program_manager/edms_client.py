import logging
from pathlib import Path
import tempfile
import time
from typing import Optional

import ccs_sso_utils
import requests

from program_manager.model import BlobRepository


logger = logging.getLogger("pm_main." + __name__)


def download(url: str, expected_content_type: str, max_attempts: int = 3) -> bytes:
    for attempt in range(max_attempts):
        try:
            t1 = time.time()
            cookies = ccs_sso_utils.save_sso_cookie("https://edms.cern.ch/ws/api/users/current")
        except Exception:
            logger.warning(f"ccs_sso_utils library failed signing on EDMS (attempt %d/%d)", 1 + attempt, max_attempts, exc_info=True)
            time.sleep(5)
            continue

        try:
            t2 = time.time()
            resp = requests.get(url, cookies=cookies, timeout=60)
            t3 = time.time()

            content_type = resp.headers.get("content-type", None)
            logger.debug("GET %s: %.2f sec for SSO, %.2f sec for request, status %d, content-type %s",
                                url, (t3 - t2), (t2 - t1), resp.status_code, content_type)
            resp.raise_for_status()

            if resp.status_code == 200 and resp.content.find(b"Not found") != -1:
                raise FileNotFoundError(f"Could not GET {url}: File not found")

            if content_type != expected_content_type:
                logger.debug("GET %s: headers=%s", url, repr(resp.headers))
                raise RuntimeError(f"Unexpected content type {content_type}; expected {expected_content_type}")

            return resp.content
        except (requests.HTTPError, requests.exceptions.RequestException) as re:
            logger.warning(f"Could not GET {url} (attempt %d/%d)", 1 + attempt, max_attempts, exc_info=True)

            if 1 + attempt == max_attempts:
                raise
            else:
                time.sleep(5)
                continue

    raise AssertionError("Unreachable code")


class EdmsBlobRepository(BlobRepository):
    _edms_url: str
    _cache_dir: Optional[Path]

    def __init__(self, edms_url: str, cache_dir: Optional[Path]) -> None:
        super().__init__()

        self._edms_url = edms_url
        self._cache_dir = cache_dir

    def get_blob_by_path(self, path: str) -> bytes:
        # first check cache, if applicable
        if self._cache_dir is not None:
            try:
                blob = (self._cache_dir / path).read_bytes()
                logger.info("%s: loaded from local cache (%d bytes)", path, len(blob))
                return blob
            except FileNotFoundError:
                pass

        # download file from EDMS
        blob = download(self._edms_url + "/" + path, expected_content_type="application/octet-stream")

        # store in cache if newly downloaded
        if self._cache_dir is not None:
            cached_path = self._cache_dir / path
            try:
                cached_path.parent.mkdir(parents=True, exist_ok=True)

                with tempfile.NamedTemporaryFile(dir=cached_path.parent, prefix=cached_path.stem, delete=False) as f:
                    f.write(blob)

                Path(f.name).rename(cached_path)
                logger.info("%s: saved to local cache (%s)", path, cached_path)
            except Exception:
                logger.warning("%s: failed to save to cache", path, exc_info=True)

        return blob
