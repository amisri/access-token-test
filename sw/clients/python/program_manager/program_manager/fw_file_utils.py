import binascii
from typing import Tuple

from program_manager.model import ProgramInfo


_PREAMBLE_LENGTH = 20
_HEADER_LENGTH = 128


def check_header_data_consistency(cmd_info: ProgramInfo, hed_info: ProgramInfo) -> None:
    try:
        assert cmd_info.board.upper() == hed_info.board.upper()
        
    except AssertionError:
        raise AssertionError(f"input data and header file boards are different ({cmd_info.board} != {hed_info.board})!")

    try:
        assert cmd_info.device.upper() == hed_info.device.upper()
        
    except AssertionError:
        raise AssertionError(f"input data and header file devices are different ({cmd_info.device} != {hed_info.device})!")

    try:
        assert cmd_info.variant.upper() == hed_info.variant.upper()
    
    except AssertionError:
        raise AssertionError(f"input data and header file variants are different ({cmd_info.variant} != {hed_info.variant})")

    try:
        assert cmd_info.var_rev == hed_info.var_rev

    except AssertionError:
        raise AssertionError(f"input data and header file revisions are different ({cmd_info.var_rev} != {hed_info.var_rev})")

    # try:
    #     assert cmd_info.api_rev == hed_info.api_rev

    # except AssertionError:
    #     raise AssertionError(f"input data and header file API revisions are different ({cmd_info.api_rev} != {hed_info.api_rev})")


def extract_data_from_file_header_trailer(content: bytes) -> Tuple[ProgramInfo, int]:
    """
    Extract metadata from firmware file.

    As per EDMS 2305763, this can be in one of two formats depending on the FPGA targeted.
    Format A prepends the full 128-byte header.
    Format B prepends just a short preamble and the full header is found at the end of the file.

    (The technical reason is that certain newer FPGAs cannot cope with Format A.)
    """

    if content[0:1] == b"H":
        # Format A

        header_start = 0
    elif content[0:1] == b"P":
        # Format B

        # Preamble example: "P1;L1012554;;0000000"
        preamble_dec = content[0:_PREAMBLE_LENGTH].decode(encoding="ascii")
        preamble_list = preamble_dec.split(";")

        # Skip preamble version, and ignore any additional fields at the end
        _, program_length_tag, *_ = preamble_list
        header_start = _PREAMBLE_LENGTH + int(program_length_tag[1:])
    else:
        raise Exception(f"Unsupported firmware header format -- first byte is {content[0:1]!r}, expected b'H'")

    # Header example: "H1;L341692;V3;R303;A200;VS_STATE_CTRL;db;DOWNLDBOOT_3;1590427655;c9da049e;;000..."
    header_dec   = content[header_start:header_start + _HEADER_LENGTH].decode(encoding="ascii")
    crc_dec      = int(binascii.hexlify(content[-2:]), 16)  # TODO: better to use bytes.hex() ?
    header_list  = header_dec.split(";")

    # Skip header version and file length, and ignore any additional fields at the end
    # Header version is not validated on @jgomezco request, since any changes will be forward-compatible
    _, bin_size, _, var_rev, api_rev, board, device, variant, *_ = header_list

    bin_size  = bin_size[1:]
    var_rev   = var_rev[1:]
    api_rev   = api_rev[1:]

    return (ProgramInfo(board=board, device=device, variant=variant, var_rev=var_rev, api_rev=api_rev), crc_dec)
