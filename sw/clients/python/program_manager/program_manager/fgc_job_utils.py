import logging

from program_manager import structured_logging
from program_manager.adapters import Adapter
from program_manager.model import BlobRepository, Device
import program_manager.regfgc3_programmer as programmer
from program_manager.structured_logging.model import DevResult, DeviceReport, SlotReport
import pyfgc

def _set_programming_fault(fgc_session):
    fgc_session.set("REGFGC3.PROG.MODE", "PROG_FAILED")

def _switch_boards_to_pb_and_reset(fgc_session, warning=False):
    # Boards to production boot
    fgc_session.set("REGFGC3.PROG", "RESET")

    # Flags down and build cache if no warning set
    if not warning:
        fgc_session.set("REGFGC3.PROG.MODE", "PROG_NONE")

def _revisions_are_the_same(expected, detected):
    if expected == detected:
        return True

    for slot in expected.keys():
        for device in expected[slot]["devices"].keys():
            expected_rev = expected[slot]["devices"][device].Var_Rev
            detected_rev = detected[slot]["devices"][device].Var_Rev

            if expected_rev != detected_rev:
                return False

    return True

def _boards_variants_are_the_same(expected, detected):
    detected_without_state = detected
    for slot in detected:
        try:
            del detected_without_state[slot]['state']

        except KeyError:
            pass

    if expected == detected_without_state:
        return True

    # Slots are the same
    if expected.keys() != detected.keys():
        return False

    for slot in expected.keys():
        if expected[slot]["board"] != detected[slot]["board"]:
            return False

    for slot, slot_dict in expected.items():
        for device, device_dict in slot_dict["devices"].items():
            try:
                if detected[slot]["devices"][device].Variant != device_dict.Variant:
                    return False

            except KeyError:
                return False

    else:
        return True

def pretty_print_data(data):
    if not isinstance(data, dict):
        print(data)
        return
    
    result_list = list()

    # Seen while testing: slot in database missing and exception thrown while sorting keys
    try:
        sorted_keys_numeric = list(map(str, sorted(map(int, data.keys()))))

    except ValueError:
        return f'Missing slot number in {data}'

    for k in sorted_keys_numeric:
        board = data[k]['board']
        devices = data[k]['devices']
        result_list.append(f'{"slot":>14}: {k:2}')
        result_list.append(f'{"board":>14}: {board}')
        result_list.append(f'{"devices":>14}:')

        for dev in sorted(devices.keys()):
            dev_info = devices[dev]
            result_list.append(f'{" ":>15} {dev:>10} -> {dev_info}')
    
    return '\n'.join(result_list)


def _add_board_warning(rep: structured_logging.TaskReport, slot: str, slot_info: dict, message: str, logger):
    try:
        slot_report = rep.get_or_add_slot(slot, slot_info["board"])
        slot_report.warnings.append(message)
    except RuntimeError:
        logger.exception("Error in _add_board_warning")


def _add_device_warning(rep: structured_logging.TaskReport, slot: str, slot_info: dict, device_name: str, message: str, logger):
    try:
        slot_report = rep.get_or_add_slot(slot, slot_info["board"])
        dev = slot_report.get_device_by_name(device_name)
        dev.warnings.append(message)
    except RuntimeError:
        logger.exception("Error in _add_device_warning")


def apply_faults_policy(job_name, fgc_session, expected, detected, attempts: int, logger, rep: structured_logging.TaskReport):
    # Converter with no boards
    if not expected and not detected:
        _switch_boards_to_pb_and_reset(fgc_session)
        return

    MAX_REPROGRAM_ATTEMPTS = 2

    # Reprogramming attempts exceeded maximum
    # FIXME: counting of attempts is inconsistent. we get 0 for immediate success, but 3 after 3 failures
    if attempts == MAX_REPROGRAM_ATTEMPTS:
        logger.error("Reprogramming failed after %d attempts. Converter not ready to operate", attempts)
        rep.errors.append(f"Reprogramming failed after {attempts} attempts. Converter not ready to operate")
        _set_programming_fault(fgc_session)
        return

    # If something went wrong or there are differences...
    # Check if there are boards missing
    missing_slots_msg_list = list()
    missing_slots          = expected.keys() - detected.keys()
    if missing_slots:
        for m in missing_slots:
            missing_slots_msg_list.append(f"{m}: {expected[m]['board']}")
            _add_board_warning(rep, m, expected[m], SlotReport.WARNING_EXPECTED_BOARD_NOT_FOUND, logger=logger)

        logger.warning(f"Expected boards not found; {';'.join(missing_slots_msg_list)}")
        logger.warning("Converter will be set in FAULT")
        _set_programming_fault(fgc_session)
        return

    # Check if there are unexpected boards
    unexpected_slots_msg_list = list()
    unexpected_slots          = detected.keys() - expected.keys()
    if unexpected_slots:
        for u in unexpected_slots:
            unexpected_slots_msg_list.append(f"slot {u} -> {detected[u]['board']}")

            # Generate per-board warnings only if database is not empty
            if len(expected) > 0:
                _add_board_warning(rep, u, detected[u], SlotReport.WARNING_UNEXPECTED_BOARD_FOUND, logger=logger)

        logger.warning(f"Unexpected boards found; {'; '.join(unexpected_slots_msg_list)}")
        logger.warning("Converter will be set in FAULT")
        _set_programming_fault(fgc_session)
        return

    # Check if boards are swapped
    swapped_boards_msg_list = list()
    for slot in expected.keys():
        if expected[slot]["board"] != detected[slot]["board"]:
            swapped_boards_msg_list.append(f"slot {slot} -> expecting board {expected[slot]['board']}; found {detected[slot]['board']}!")
            _add_board_warning(rep, slot, detected[slot], SlotReport.WARNING_EXPECTED_DIFFERENT_BOARD, logger=logger)
        
    if swapped_boards_msg_list:
        logger.warning(f"Swapped board found; {'; '.join(swapped_boards_msg_list)}")
        logger.warning("Converter will be set in FAULT")
        _set_programming_fault(fgc_session)
        return
        
    # Boards are the same. Check if variants are not
    different_variants = list()
    for slot in expected.keys():
        for device in expected[slot]["devices"].keys():
            try:
                if expected[slot]["devices"][device].Variant != detected[slot]["devices"][device].Variant:
                    different_variants.append((slot, device))
                    _add_device_warning(rep, slot, detected[slot], device, SlotReport.WARNING_EXPECTED_DIFFERENT_BOARD, logger=logger)
            except KeyError:
                pass

    unexpected_vars_msg_list = list()
    if different_variants:
        for diff in different_variants:
            slot, device = diff
            unexpected_vars_msg_list.append(
                f"slot: {slot}; board: {expected[slot]['board']}; device: {expected[slot]['devices'][device].Device}")
            
            unexpected_vars_msg_list.append(
                f"expected: {expected[slot]['devices'][device].Variant}, detected {detected[slot]['devices'][device].Variant}")

            # <converter> <slot> <board> <device> <variant> <var_revision> <api_revision> <fw_repo> <fw_file_name>
            logger.info("To force reprogramming: /user/pclhc/bin/python/ccs_venv/bin/regfgc3_programmer --loose %s %s %s %s %s %s %s <fw_repo> <fw_file_name>",
                        job_name, slot, expected[slot]['board'], expected[slot]['devices'][device].Device, expected[slot]['devices'][device].Variant,
                        expected[slot]['devices'][device].Var_Rev, expected[slot]['devices'][device].API_Rev)

        logger.warning(f"Unexpected variants found in the following boards; {'; '.join(unexpected_vars_msg_list)}")
        logger.warning("Converter will be set in FAULT")
        _set_programming_fault(fgc_session)
        return

    # Reprogramming successful

    if expected and detected and _boards_variants_are_the_same(expected, detected):
        logger.info("Boards and variants OK; all boards will be switched to production boot")

        warning_set = False
        if not _revisions_are_the_same(expected, detected):
            logger.info("Variant revisions are different. Setting PROG_WARNING.")

            for slot in expected.keys():
                for device in expected[slot]["devices"].keys():
                    expected_rev = expected[slot]["devices"][device].Var_Rev
                    try:
                        detected_rev = detected[slot]["devices"][device].Var_Rev
                    except KeyError:
                        detected_rev = None

                    if expected_rev != detected_rev:
                        _add_device_warning(rep, slot, expected[slot], device, DeviceReport.WARNING_VARIANT_REVISIONS_MISMATCH, logger)

            fgc_session.set("REGFGC3.PROG.MODE", "PROG_WARNING")
            warning_set = True
    else:
        # This seems to happen when no board is in DownloadBoot
        # Is this because DownloadBoot is not handled the same in _boards_variants_are_the_same as in get_diffs_for_common_cards?
        logger.warning("Reached end of apply_faults_policy, but board variants are not the same. Bug?")

        fgc_session.set("REGFGC3.PROG.MODE", "PROG_WARNING")
        warning_set = True

    # Reprogramming successful, but not at the first attempt
    # FIXME: counting of attempts is inconsistent. we get 0 for immediate success, but 3 after 3 failures
    if attempts > 0:
        logger.warning("Reprogramming task took %d attempts. Leaving program warning", 1 + attempts)
        rep.warnings.append(f"Reprogramming task took {1 + attempts} attempts. Leaving program warning")
        warning_set = True

    _switch_boards_to_pb_and_reset(fgc_session, warning=warning_set)


def reprogram_cards(job_name, fgc_session, tasks, expected_files,
                    blob_repo: BlobRepository,
                    logger,
                    rep: structured_logging.TaskReport):
    attempts, max_attempts_so_far = 0, 0

    # Unlock FGC3
    r = fgc_session.set("REGFGC3.PROG.LOCK", "DISABLED")

    try:
        _ = r.value

    except pyfgc.FgcResponseError:
        logger.error(f"{job_name}: Could not disable REGFGC3.PROG.LOCK value. Cards will not be reprogrammed, {r.err_code}: {r.err_msg}")
        return max_attempts_so_far

    sorted_slots = list(map(str, sorted(list(map(int, tasks.keys())))))
    for slot in sorted_slots:
        board   = tasks[slot]["board"]
        devices = tasks[slot]["devices"]

        slot_report = rep.get_or_add_slot(slot, board)

        for dev in devices:
            device_obj = devices[dev]
            fw_file_name = expected_files[slot]["devices"][dev]

            device_report = slot_report.get_device_by_name(dev)

            logger.info("Start reprogramming converter, slot %s, board %s, device %s", slot, board, dev)
            try:
                attempts = programmer.program(job_name,
                                          slot,
                                          board,
                                          dev,
                                          device_obj.Variant,
                                          device_obj.Var_Rev,
                                          device_obj.API_Rev,
                                          blob_repo,
                                          fw_file_name,
                                          fgc_session=fgc_session,
                                          device_report=device_report)
                                          
                if attempts > max_attempts_so_far:
                    max_attempts_so_far = attempts

                if device_report.res == DevResult.PROGRAMMED and attempts > 0:
                    device_report.warnings.append(f"Reprogramming took {1 + attempts} attempts")

            except Exception as ex:
                # Errors during programming attempts will be normally logged to the device report by program()
                # However, if the function raises an exception (such as an UnkownVariantError), the clean-up is up to us

                logger.exception("Could not reprogram board %s in slot %s", board, slot)
                device_report.res = DevResult.PROGRAMMING_FAILED
                device_report.errors.append(type(ex).__name__ + ": " + str(ex))

    # Lock FGC3
    fgc_session.set("REGFGC3.PROG.LOCK", "ENABLED")

    return max_attempts_so_far

def get_diffs_for_common_cards(expected, detected, logger):
    def _add_diff(slot, board_name, devices_dict, diff_dict):
        try:
            diff_dict[slot]

        except KeyError:
            diff_dict[slot] = {"board": board_name, "devices": dict()}

        diff_dict[slot]["devices"].update(devices_dict)

    diff = dict()

    common_slots = expected.keys() & detected.keys()
    for slot in common_slots:
        if not programmer.is_board_in_download_boot(detected[slot]):
            logger.info(f"Ignoring board {detected[slot]['board']} in slot {slot} NOT in download boot")
            continue
        
        board_name_expected = expected[slot]["board"]
        board_name_detected = detected[slot]["board"]

        if board_name_expected != board_name_detected:
            # TODO: We don't care to see this error in Sentry; how to stop it from propagating?
            logger.error(f"Board {board_name_detected} found in slot {slot}! Expecting {board_name_expected}")
            continue

        dev_expected = expected[slot]["devices"]
        dev_detected = detected[slot]["devices"]

        common_devices_diff = {dev: dev_expected[dev] for dev in (dev_expected.keys() & dev_detected.keys())
                               if (dev_expected[dev].Variant == dev_detected[dev].Variant
                                        and dev_expected[dev].Var_Rev != dev_detected[dev].Var_Rev
                                    )
                                    or (dev_expected[dev].Variant != dev_detected[dev].Variant
                                        and dev_detected[dev].Variant == "EMPTY")}

        if common_devices_diff:
            _add_diff(slot, board_name_expected, common_devices_diff, diff)

    return diff

def get_expected(job_name, adapter: Adapter, logger):
    expected_data, expected_files = None, None

    try:
        expected_data, expected_files = adapter.get_expected(job_name)

    except FileNotFoundError:
        raise FileNotFoundError(f"{job_name}: expected data (file) not found")

    logger.debug(f"expected_data:\n {pretty_print_data(expected_data)}")
    return expected_data, expected_files

def get_detected(job_name, fgc_session, logger):
    detected_data = None

    try:
        r = fgc_session.get("REGFGC3.SLOT_INFO")
        slot_info = r.value

    except pyfgc.FgcResponseError:
        raise pyfgc.PyFgcError(f"{job_name}: did not get REGFGC3.SLOT_INFO: {r.err_code} {r.err_msg}")

    try:
        detected_data = programmer.parse_slot_info(slot_info)

    except TypeError:
        raise TypeError(f"{job_name}: Could not parse detected data {detected_data}")

    logger.debug(f"detected data:\n {pretty_print_data(detected_data)}")
    return detected_data


def is_up_to_date(device_report: DeviceReport) -> bool:
    assert device_report.fw_db is not None
    assert device_report.fw_before is not None

    return (device_report.fw_db.variant == device_report.fw_before.variant and
            device_report.fw_db.var_rev == device_report.fw_before.var_rev)


def able_to_reprogram(slot_info, device_report: DeviceReport) -> bool:
    if not programmer.is_board_in_download_boot(slot_info):
        # Already logged elsewhere
        # logger.info(f"Ignoring board {slot_info['board']} in slot {slot} NOT in download boot")
        return False

    VARIANT_EMPTY = "EMPTY"

    if device_report.fw_db is None or device_report.fw_before is None:
        return False

    if (device_report.fw_db.variant == device_report.fw_before.variant and
        device_report.fw_db.var_rev != device_report.fw_before.var_rev) or (
        device_report.fw_db.variant != device_report.fw_before.variant and
        device_report.fw_before.variant == VARIANT_EMPTY):
        return True
    else:
        return False
