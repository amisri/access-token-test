from datetime import datetime
import logging
import queue
import threading
import time

import pyfgc

from program_manager.adapters import Adapter, get_adapter
import program_manager.fgc_job_utils      as utils
from program_manager.model import BlobRepository, BoardState, Device
from program_manager.pyfgc_wrapper import pyfgc_connect
from program_manager import structured_logging
from program_manager.structured_logging.model import DevFwVersion, DevResult, DeviceReport, SlotReport


ITERATION_TIME_SEC = 5

# TODO: these should be handled by the FGC state machine
PRE_RESCAN_DELAY_SEC = 0.5
POST_RESCAN_DELAY_SEC = 0.5


def fgc_job(job_name, adapter: Adapter, blob_repo: BlobRepository, logger, slog: structured_logging.Writer, session_factory=pyfgc_connect):
    # Wrap _fgc_job_inner so that no matter what, the Task Report gets saved
    rep = structured_logging.TaskReport(timestamp=datetime.now(),
                                        fgc_name=job_name,
                                        abandoned=False,
                                        errors=[],
                                        warnings=[],
                                        slots=[]
                                        )
    try:
        _fgc_job_inner(job_name, adapter, blob_repo, logger, rep, session_factory)
    except Exception as ex:
        rep.errors.append(type(ex).__name__ + ": " + str(ex))
        raise
    finally:
        # Iterate devices and change any TO_BE_PROGRAMMED results to ABANDONED
        # (as they're clearly not getting programmed this time around)
        for slot_report in rep.slots:
            for device_report in slot_report.devices:
                if device_report.res in {DevResult.UNKNOWN, DevResult.TO_BE_PROGRAMMED}:
                    device_report.res = DevResult.ABANDONED
                    rep.abandoned = True

        # Update task duration
        if rep.timestamp is not None:
            rep.duration_sec = (datetime.now() - rep.timestamp).total_seconds()

        slog.write(rep)


def _fgc_job_inner(job_name, adapter: Adapter, blob_repo: BlobRepository, logger, rep: structured_logging.TaskReport, session_factory=pyfgc_connect):
    expected = dict()
    detected = dict()
    attempts = 0

    expected, expected_files = utils.get_expected(job_name, adapter, logger)
    if not expected:
        logger.warning(f"Expected data for fgc empty, will not reprogram any card")

    device_info: Device

    for slot, slot_info in expected.items():
        devices = []

        for device_info in slot_info["devices"].values():
            fw_version = DevFwVersion(variant=device_info.Variant, var_rev=device_info.Var_Rev, api_rev=device_info.API_Rev)
            devices.append(DeviceReport(device=device_info.Device, fw_before=None, fw_after=None, fw_db=fw_version,
                                        res=DevResult.UNKNOWN,
                                        errors=[], warnings=[]))
        rep.slots.append(SlotReport(slot=slot, board=slot_info["board"], devices=devices, state_before=None, state_after=None, errors=[], warnings=[]))

    fgc_session = session_factory(job_name)

    try:
        detected = utils.get_detected(job_name, fgc_session, logger)

        for slot, slot_info in detected.items():
            slot_report = rep.get_or_add_slot(slot=slot, board_name=slot_info["board"])
            slot_report.state_before = slot_info["state"]

            for device_info in slot_info["devices"].values():
                device_report, was_newly_added = slot_report.get_or_add_device(device_info.Device, default_res=DevResult.NOT_IN_DB)
                device_report.fw_before = DevFwVersion(variant=device_info.Variant, var_rev=device_info.Var_Rev, api_rev=device_info.API_Rev)

                # If we were already aware of the device (== it exists in the DB and has a target revision),
                # see if we should reprogram it
                # In any case we have to change the state from the default 'NOT_IN_CONVERTER'
                if not was_newly_added:
                    if utils.is_up_to_date(device_report):
                        device_report.res = DevResult.UP_TO_DATE
                    elif utils.able_to_reprogram(slot_info, device_report):
                        device_report.res = DevResult.TO_BE_PROGRAMMED
                    else:
                        device_report.res = DevResult.CANNOT_REPROGRAM

        # anything not detected is now positively NOT_IN_CONVERTER
        for slot_report in rep.slots:
            for device_report in slot_report.devices:
                if device_report.res is DevResult.UNKNOWN:
                    device_report.res = DevResult.NOT_IN_CONVERTER

        todo_tasks = utils.get_diffs_for_common_cards(expected, detected, logger)

        if not todo_tasks:
            logger.info(f"Nothing to program; expected data == detected data")

        else:
            logger.info(f"The following will be programmed:\n {utils.pretty_print_data(todo_tasks)}")
            attempts = utils.reprogram_cards(job_name, fgc_session, todo_tasks, expected_files,
                                             blob_repo=blob_repo,
                                             logger=logger,
                                             rep=rep)

            time.sleep(PRE_RESCAN_DELAY_SEC)
            fgc_session.set("REGFGC3.SLOT_INFO", "")
            time.sleep(POST_RESCAN_DELAY_SEC)
            logger.info(f"Reprogramming done")

    finally:
        detected = utils.get_detected(job_name, fgc_session, logger)

        for slot, slot_info in detected.items():
            slot_report = rep.get_or_add_slot(slot=slot, board_name=slot_info["board"])

            for device_info in slot_info["devices"].values():
                device_report, was_newly_added = slot_report.get_or_add_device(device_info.Device, default_res=DevResult.NOT_IN_DB)
                device_report.fw_after = DevFwVersion(variant=device_info.Variant, var_rev=device_info.Var_Rev, api_rev=device_info.API_Rev)

                if not was_newly_added:
                    # If the device is in the DB, but was not detected earlier, we should still change the 'NOT_IN_CONVERTER' state
                    if device_report.res == DevResult.NOT_IN_CONVERTER:
                        device_report.res = DevResult.NO_ACTION
                        device_report.errors.append("Device not detected during initial scan")

        utils.apply_faults_policy(job_name, fgc_session, expected, detected, attempts, logger, rep)

        # Wait for 3 seconds and do one more scan to capture final board state (DownloadBoot/ProductionBoot)
        time.sleep(3)
        detected = utils.get_detected(job_name, fgc_session, logger)

        for slot, slot_info in detected.items():
            slot_report = rep.get_or_add_slot(slot=slot, board_name=slot_info["board"])
            slot_report.state_after = slot_info["state"]

            if slot_info["state"] == BoardState.DOWNLOAD_BOOT:
                slot_report.warnings.append(SlotReport.WARNING_DID_NOT_SWITCH_TO_PRODUCTION_BOOT)

        pyfgc.disconnect(fgc_session)


# Prepend device name to log messages
# (if needed, it could be just added to kwargs here and printed out at a higher level)
class PrefixLoggerAdapter(logging.LoggerAdapter):
    def __init__(self, logger, device_name):
        super().__init__(logger, dict())

        self._device_name = device_name

    def process(self, msg, kwargs):
        return f"({self._device_name}) {msg}", kwargs


class FgcWorker(threading.Thread):
    FGC_WORKER_TIME_SLEEP_SEC = 1

    def __init__(self, tasks, jobs, job_lock, name="Unknown_FGC"):
        super().__init__()
        self._tasks      = tasks
        self._jobs       = jobs
        self._lock       = job_lock
        self.name        = name
        self._stop_event = threading.Event()
        self._logger     = logging.getLogger("pm_main." + __name__ + ".FgcWorker")

        self.start()

    def run(self):
        # Note: an uncaught exception here (e.g. from the logger) will kill the worker without replacement

        while not self._stop_event.is_set():
            try:
                func, job_name, adapter, blob_repo, slog = self._tasks.get(timeout=2)

            except queue.Empty:
                time.sleep(FgcWorker.FGC_WORKER_TIME_SLEEP_SEC)
                continue

            logger_adapter = PrefixLoggerAdapter(self._logger, device_name=job_name)

            try:
                func(job_name, adapter, blob_repo, logger_adapter, slog)

            except FileNotFoundError as fe:
                logger_adapter.error(f"Job failed due to missing file: {fe}")
            except Exception:
                logger_adapter.exception("Job failed due to unexpected error")

            finally:
                self._tasks.task_done()
                with self._lock:
                    self._jobs.discard(job_name)

                logger_adapter.info("Job removed from queue")

    def stop(self):
        self._stop_event.set()
    

class AreaProgramManager:
    MAX_NUM_TASKS   = 200
    MAX_NUM_WORKERS = 1

    name: str
    _tasks: queue.Queue
    _workers: list
    _jobs: set
    _job_set_lock: threading.Lock
    _adapter: Adapter
    _logger: logging.LoggerAdapter

    def __init__(self, name, expected_data, adapter_data: dict,
                 blob_repo: BlobRepository,
                 slog: structured_logging.Writer,
                 num_workers=MAX_NUM_WORKERS):
        self.name          = name
        self._tasks        = queue.Queue(maxsize=AreaProgramManager.MAX_NUM_TASKS)
        self._workers      = list()
        self._jobs         = set()
        self._job_set_lock = threading.Lock()
        self._slog = slog
        
        # FIXME: expected_data is "db" or "fs". to be renamed & retyped (Enum)
        self._adapter = get_adapter(expected_data, adapter_data)

        self._blob_repo = blob_repo

        for i in range(num_workers):
            self._workers.append(FgcWorker(self._tasks, self._jobs, self._job_set_lock, name=self.name+str(i)))
            
        logger = logging.getLogger("pm_main." + __name__)
        self._logger = PrefixLoggerAdapter(logger, name)

    def add_job(self, func, job_name):
        if not job_name in self._jobs:
            with self._job_set_lock:
                self._jobs.add(job_name)

            # TODO: nonsense that 'func' is a variable, but the set of arguments is hardcoded
            self._tasks.put((func, job_name, self._adapter, self._blob_repo, self._slog))
            self._logger.info(f"Job added to queue: %s", job_name)

    def wait_completion(self):
        self._logger.info("Waiting for pending tasks to be completed")
        self._tasks.join()
        self._logger.info("Pending tasks are done")

        for worker in self._workers:
            worker.stop()
        
        self._logger.info("Workers stopped")
        
