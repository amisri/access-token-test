__version__ = "1.4.3"

__authors__ = """
    Carlos Ghabrous Larrea,
    Martin Cejp
"""

__emails__ = """
    martin.cejp@cern.ch
"""
