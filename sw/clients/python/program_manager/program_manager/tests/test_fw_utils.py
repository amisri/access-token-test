from pathlib import Path

import pytest

from program_manager.fw_file_utils import extract_data_from_file_header_trailer
from program_manager.model import ProgramInfo
from program_manager.regfgc3_programmer import ProgramInfo2


DATA_DIR = Path(__file__).parent / "data"

PROGRAM_INFOS = [
    ("toplevel_SIRAMATRIX_mf_HMINUS_40_1667386044.prg",
     ProgramInfo(board="SIRAMATRIX", device="MF",variant="HMINUS_40", var_rev="650", api_rev="403"),
     0x5384),
    ("toplevel_VS_STATE_CTRL_db_DOWNLDBOOT_3_1590427655.prg",
     ProgramInfo(board="VS_STATE_CTRL", device="db", variant="DOWNLDBOOT_3", var_rev="303", api_rev="200"),
     0x241B),
]


@pytest.mark.parametrize("filename, program_info_expected, crc_expected", PROGRAM_INFOS)
def test_extract_data_from_file_header_trailer(filename, program_info_expected, crc_expected):
    program_info, crc = extract_data_from_file_header_trailer((DATA_DIR / filename).read_bytes())

    assert program_info == program_info_expected
    assert crc == crc_expected


def test_ProgramInfo_to_ProgramInfo2():
    header_data = ProgramInfo(board="VS_STATE_CTRL", device="db", variant="DOWNLDBOOT_3", var_rev="303",
                              api_rev="200")
    hed_info = ProgramInfo2("<converter>", **header_data._asdict())

    assert hed_info == ProgramInfo2(converter="<converter>", board="VS_STATE_CTRL", device="db",
                                    variant="DOWNLDBOOT_3", var_rev="303", api_rev="200")
