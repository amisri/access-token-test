import logging

from program_manager.fgc_job_utils import get_diffs_for_common_cards
from program_manager.fw_file_utils import check_header_data_consistency
from program_manager.model import Device, ProgramInfo


logger = logging.getLogger(__name__)


expected_fake_sample = {
    "5": {
        "board": "VS_STATE_CTRL",
        "state": "DownloadBoot",
        "devices": {"MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="211", API_Rev="200")},
    },
    "6": {
        "board": "VS_REG_DSP",
        "state": "DownloadBoot",
        "devices": {
            "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="201", API_Rev="200"),
            "DEVICE_2": Device(Device="DEVICE_2", Variant="IGBT_33", Var_Rev="140", API_Rev="1"),
        },
    },
}

detected_fake_sample = {
    "5": {
        "board": "VS_STATE_CTRL",
        "state": "DownloadBoot",
        "devices": {"MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="211", API_Rev="200")},
    },
    "6": {
        "board": "VS_REG_DSP",
        "state": "DownloadBoot",
        "devices": {
            "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="201", API_Rev="200"),
            "DEVICE_2": Device(Device="DEVICE_2", Variant="IGBT_33", Var_Rev="140", API_Rev="1"),
        },
    },
    "14": {
        "board": "VS_BIS_INTK",
        "state": "DownloadBoot",
        "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="202", API_Rev="200")},
    },
}

expected_fake_sample_different_variants = {
    "5": {
        "board": "VS_STATE_CTRL",
        "devices": {"MF": Device(Device="MF", Variant="IGBT_35", Var_Rev="211", API_Rev="200")},
    },
    "6": {
        "board": "VS_REG_DSP",
        "devices": {
            "MF": Device(Device="MF", Variant="IGBT_35", Var_Rev="201", API_Rev="200"),
            "DEVICE_2": Device(Device="DEVICE_2", Variant="IGBT_36", Var_Rev="140", API_Rev="1"),
        },
    },
    "14": {
        "board": "VS_BIS_INTK",
        "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="202", API_Rev="200")},
    },
}


expected_real_sample = {
    "5": {
        "board": "VS_STATE_CTRL",
        "devices": {"MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="211", API_Rev="200")},
    },
    "6": {
        "board": "VS_REG_DSP",
        "devices": {
            "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="201", API_Rev="200"),
            "DEVICE_2": Device(Device="DEVICE_2", Variant="IGBT_33", Var_Rev="140", API_Rev="1"),
        },
    },
    "14": {
        "board": "VS_BIS_INTK",
        "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="202", API_Rev="200")},
    },
}

detected_real_sample = {
    "5": {
        "board": "VS_STATE_CTRL",
        "state": "DownloadBoot",
        "devices": {
            "DB": Device(Device="DB", Variant="DOWNLDBOOT_3", Var_Rev="240", API_Rev="200"),
            "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="207", API_Rev="201"),
        },
    },
    "6": {
        "board": "VS_REG_DSP",
        "state": "DownloadBoot",
        "devices": {
            "DB": Device(Device="DB", Variant="DOWNLDBOOT_3", Var_Rev="240", API_Rev="200"),
            "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="205", API_Rev="200"),
            "DEVICE_2": Device(Device="DEVICE_2", Variant="IGBT_33", Var_Rev="141", API_Rev="1"),
        },
    },
    "9": {
        "board": "VS_ANA_INTK_2",
        "state": "ProductionBoot",
        "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="21", API_Rev="1")},
    },
    "10": {
        "board": "VS_ANA_INTK_2",
        "state": "ProductionBoot",
        "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="21", API_Rev="1")},
    },
    "12": {
        "board": "VS_DIG_INTK",
        "state": "DownloadBoot",
        "devices": {
            "DB": Device(Device="DB", Variant="DOWNLDBOOT_3", Var_Rev="240", API_Rev="200"),
            "MF": Device(Device="MF", Variant="UNKNOWN", Var_Rev="2803", API_Rev="2904"),
        },
    },
    "14": {
        "board": "VS_BIS_INTK",
        "state": "DownloadBoot",
        "devices": {
            "DB": Device(Device="DB", Variant="DOWNLDBOOT_3", Var_Rev="202", API_Rev="200"),
            "MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="203", API_Rev="200"),
        },
    },
}


def test_empty_expected_and_detected_return_empty_dict():
    diff = get_diffs_for_common_cards(dict(), dict(), logger)
    assert isinstance(diff, dict)
    assert len(diff) == 0


def test_equal_entries_return_empty_dict():
    diff = get_diffs_for_common_cards(expected_fake_sample, expected_fake_sample, logger)
    assert isinstance(diff, dict)
    assert len(diff) == 0


def test_entries_with_same_data_for_common_slots_return_empty_dict():
    diff = get_diffs_for_common_cards(expected_fake_sample, detected_fake_sample, logger)
    assert isinstance(diff, dict)
    assert len(diff) == 0


def test_entries_with_different_data_for_common_slots_return_non_empty_dict():
    diff = get_diffs_for_common_cards(expected_real_sample, detected_real_sample, logger)
    assert isinstance(diff, dict)
    assert len(diff) != 0


def test_entries_with_different_data_for_common_slots_return_data_that_makes_sense():
    diff = get_diffs_for_common_cards(expected_real_sample, detected_real_sample, logger)
    assert isinstance(diff, dict)
    assert len(diff) != 0

    # Slots are correct
    assert str(5) in list(diff.keys())
    assert str(6) in list(diff.keys())
    assert diff["5"]["board"] == "VS_STATE_CTRL"
    assert diff["6"]["board"] == "VS_REG_DSP"

    # Devices are correct
    assert "MF" in diff["5"]["devices"]
    assert "MF" in diff["6"]["devices"]
    assert "DEVICE_2" in diff["6"]["devices"]
    assert diff["6"]["devices"]["DEVICE_2"].Var_Rev == "140"


def test_entries_with_different_variants_are_not_listed_in_diff():
    diff = get_diffs_for_common_cards(expected_fake_sample_different_variants, detected_fake_sample, logger)
    assert len(diff) == 0


def test_detected_card_in_db_is_in_diff_dict():
    diff = get_diffs_for_common_cards(expected_real_sample, detected_real_sample, logger)
    assert isinstance(diff, dict)
    assert len(diff) != 0

    assert "14" in diff.keys()
    assert "MF" in diff["14"]["devices"]
    assert diff["14"]["devices"]["MF"].Variant == "STANDARD_4"
    assert diff["14"]["devices"]["MF"].Var_Rev == "202"


def test_check_header_data_consistency():
    header_info = ProgramInfo(board="VS_STATE_CTRL", device="db", variant="DOWNLDBOOT_3", var_rev="303", api_rev="200")
    adapter_info = ProgramInfo(board="VS_STATE_CTRL", device="DB", variant="DOWNLDBOOT_3", var_rev="303", api_rev="303")

    # Does not return anything, but must not raise
    check_header_data_consistency(header_info, adapter_info)
