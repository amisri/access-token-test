import pytest
from hypothesis.strategies import text

from program_manager.pm_fsm import (
    PmState,
    PmStateWaiting,
    PmStateTransferred,
    PmStateProgrammed,
    PmStateCleanUp,
    PmStateError,
)
from program_manager.pm_fsm import ProgramManagerFsm

ALLOWED_FSM_MODES = [
    "TRANSFERRED",
    "PROGRAMMED",
    "SET_PROD_BOOT_PARS",
    "TO_PROD_BOOT",
    "CLEAN_UP",
    "WAITING",
]
NOT_ALLOWED_FSM_MODES = [text() for _ in range(len(ALLOWED_FSM_MODES))]
PROG_DUMMY_DATA = ("RPAGM.866.21.ETH1", 4, "card", "variant", 103, "my_file.bin")

# Helpers
# valid_modes_for_state = {
#     PmStateWaiting: ("TRANSFERRED",),
#     PmStateTransferred: ("PROGRAMMED",),
#     PmStateProgrammed: ("SET_PROD_BOOT_PARS",),
#     PmStateSetProdBootPars: ("TO_PROD_BOOT",),
#     PmStateToProdBoot: ("CLEAN_UP",),
#     PmStateCleanUp: ("WAITING",)}
# return valid_modes_for_state[state]

# invalid_modes_for_state = {
#     PmStateWaiting: (mode for mode in ALLOWED_FSM_MODES if mode != "TRANSFERRED"),
#     PmStateTransferred: (mode for mode in ALLOWED_FSM_MODES if mode != "PROGRAMMED"),
#     PmStateProgrammed: (mode for mode in ALLOWED_FSM_MODES if mode != "SET_PROD_BOOT_PARS"),
#     PmStateSetProdBootPars: (mode for mode in ALLOWED_FSM_MODES if mode != "TO_PROD_BOOT"),
#     PmStateToProdBoot: (mode for mode in ALLOWED_FSM_MODES if mode != "CLEAN_UP"),
#     PmStateCleanUp: (mode for mode in ALLOWED_FSM_MODES if mode != "WAITING")}
# return invalid_modes_for_state[state]


@pytest.mark.skip  # test broken
@pytest.mark.parametrize("mode", [m for m in ALLOWED_FSM_MODES])
def test_fsm_mode_is_settable_and_state_is_not(mode, state="state"):
    fsm = ProgramManagerFsm(PROG_DUMMY_DATA)
    fsm.mode = mode
    assert fsm.mode == mode

    with pytest.raises(AttributeError):
        fsm.state = state


@pytest.mark.skip  # test broken
@pytest.mark.parametrize(
    "good_mode, bad_mode",
    [(g, b) for g, b in zip(ALLOWED_FSM_MODES, NOT_ALLOWED_FSM_MODES)],
)
def test_fsm_only_accepts_valid_modes(good_mode, bad_mode):
    fsm = ProgramManagerFsm(PROG_DUMMY_DATA)
    fsm.mode = good_mode
    assert fsm.mode == good_mode

    with pytest.raises(ValueError):
        fsm.mode = bad_mode


# @pytest.mark.parametrize("current_state, new_modes",
#                          (
#                             )
#                         )
#     fsm = ProgramManagerFsm(PROG_DUMMY_DATA, init_state=current_state)
#     for mode in new_modes:
#         fsm.mode = mode
#         assert fsm.mode == mode

# @pytest.mark.parametrize("current_state, not_permitted_modes",
#                          (
#                             )
#                          )
#     fsm = ProgramManagerFsm(PROG_DUMMY_DATA, init_state=current_state)
#     for mode in not_permitted_modes:
#         with pytest.raises(ValueError):
#             fsm.mode = mode


@pytest.mark.skip  # test broken
def test_fsm_moves_as_it_should():
    fsm = ProgramManagerFsm(PROG_DUMMY_DATA)
    modes = ALLOWED_FSM_MODES
    for mode in modes:
        fsm.mode = mode
        assert fsm.state == mode
