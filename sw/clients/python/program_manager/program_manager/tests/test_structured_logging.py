from datetime import datetime, timedelta
import logging
from pathlib import Path
import tempfile

import pytest

from program_manager import structured_logging
from program_manager.structured_logging.model import DevFwVersion, DeviceReport, DevResult, ServerRestart, SlotReport, TaskReport


def test_it():
    with tempfile.TemporaryDirectory() as dir:
        path = Path(dir) / "log.jsonl"
        writer = structured_logging.Writer(log_file_path=path, retention_days=14)

        entry = TaskReport(
            timestamp=datetime(2022, 1, 1, 0, 0, 0),
            abandoned=False,
            errors=["Error"],
            fgc_name="RFNA.866.DIAG.1",
            slots=[
                SlotReport(
                    slot="9",
                    board="VS_ANA_INTK",
                    devices=[
                        DeviceReport(
                            "MF",
                            fw_before=DevFwVersion(variant="MODULATORL4_5", var_rev="305", api_rev="200"),
                            fw_after=DevFwVersion(variant="MODULATORL4_5", var_rev="305", api_rev="200"),
                            fw_db=DevFwVersion(variant="MODULATORL4_5", var_rev="305", api_rev="200"),
                            res=DevResult.NO_ACTION,
                            errors=["DevError"],
                            warnings=["DevWarning"],
                        ),
                    ],
                    state_before="DownloadBoot",
                    state_after="ProductionBoot",
                    errors=["SlotError"],
                    warnings=["SlotWarning"],
                )
            ],
            warnings=["Warning"],
        )
        writer.write(entry)
        writer.close()

        row = Path(writer._handler.baseFilename).read_text().rstrip()
        assert row == (
            '{"type": "TaskReport", "timestamp": "2022-01-01T00:00:00", "fgc_name": "RFNA.866.DIAG.1", "duration_sec": null, "abandoned": false, "errors": '
            '["Error"], "warnings": ["Warning"], "slots": [{"slot": "9", "board": "VS_ANA_INTK", "devices": '
            '[{"device": "MF", "fw_before": {"variant": "MODULATORL4_5", "var_rev": "305", "api_rev": "200"}, '
            '"fw_after": {"variant": "MODULATORL4_5", "var_rev": "305", "api_rev": "200"}, "fw_db": {"variant": '
            '"MODULATORL4_5", "var_rev": "305", "api_rev": "200"}, "res": "NO_ACTION", '
            '"errors": ["DevError"], "warnings": ["DevWarning"]}], "state_before": "DownloadBoot", '
            '"state_after": "ProductionBoot", "errors": ["SlotError"], "warnings": ["SlotWarning"]}]}'
        )


@pytest.mark.parametrize("suffix", ["", ".jsonl"])
def test_rollover(caplog, suffix):
    caplog.set_level(logging.DEBUG)

    # pick a fake date
    now = [datetime(2022, 2, 1)]  # boxed to pass as reference to closure

    def now_func():
        return now[0]

    with tempfile.TemporaryDirectory() as dir:
        path_prefix = (Path(dir) / "log").with_suffix(suffix)

        # generate some log files
        for date in ["2022-01-28", "2022-01-29", "2022-01-30", "2022-01-31"]:
            path_prefix.with_name(f"{path_prefix.stem}.{date}{suffix}").write_text(date)

        # instantiate the writer
        writer = structured_logging.Writer(log_file_path=path_prefix,
                                           retention_days=3,
                                           get_time_function=now_func)

        writer.write(ServerRestart(now[0]))

        # trigger a roll-over
        now[0] += timedelta(days=1)
        writer.write(ServerRestart(now[0]))

        # close writer & check files in directory
        writer.close()

        files_in_dir = sorted([p.name for p in Path(dir).iterdir()])
        assert files_in_dir == [f"{path_prefix.stem}.{date}{suffix}" for date in ["2022-01-30", "2022-01-31", "2022-02-01", "2022-02-02"]]

        assert "ServerRestart" in path_prefix.with_name(f"{path_prefix.stem}.2022-02-01{suffix}").read_text()
        assert "ServerRestart" in path_prefix.with_name(f"{path_prefix.stem}.2022-02-02{suffix}").read_text()
