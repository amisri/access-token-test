import os

import cx_Oracle


USERNAME = "pocontrols_pub"
PASSWORD = "p0published!!"  # per https://wikis.cern.ch/display/TEEPCCCS/CCS+Databases

ORACLE_DSN = """(DESCRIPTION=
                  (ADDRESS=(PROTOCOL=TCP)(HOST=pdb-s.cern.ch)(PORT=10121))
                  (ENABLE=BROKEN)
                  (CONNECT_DATA=(SERVICE_NAME=pdb_cerndb1.cern.ch))
                )"""


def test_retrieve_data_from_pm_table_with_class():
    sql_query = """
        SELECT logical_name
        FROM ALIM.FIRMWARE_PARAMETERS_VIEW
        WHERE logical_name = :logical_name
    """
    logical_name = "RPADF.363.LTB.RBHZ40"

    con = cx_Oracle.connect(USERNAME, PASSWORD, ORACLE_DSN)
    cursor = con.cursor()
    cursor.execute(sql_query, dict(logical_name=logical_name))
    rows = cursor.fetchall()

    assert len(rows) > 0
    assert all(row[0] == logical_name for row in rows)


def test_retrieve_data_using_cx_Oracle():
    sql_query = """select *
                from Alim.FIRMWARE_PARAMETERS_VIEW
                where LOGICAL_NAME = : 1 """

    con = cx_Oracle.connect(USERNAME, PASSWORD, ORACLE_DSN)
    cursor = con.cursor()
    cursor.execute(sql_query, ("RPADF.363.LTB.RBHZ40",))
    row = cursor.fetchone()
    print(row)
