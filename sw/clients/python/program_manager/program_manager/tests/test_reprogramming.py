from dataclasses import dataclass, field
import logging
from pathlib import Path
import tempfile
from unittest.mock import patch

from program_manager.adapters import Adapter
from program_manager.area_worker import fgc_job
from program_manager.edms_client import EdmsBlobRepository
from program_manager.model import Device
from program_manager import structured_logging
from program_manager.tests.test_db import ORACLE_DSN, PASSWORD, USERNAME


SLOT_INFO_STRING = (
    "------------------------------,"
    "SLOT       5,BOARD       VS_STATE_CTRL,STATE      DownloadBoot,"
    "Device     DB,Variant    DOWNLDBOOT_3,Var_Rev    208,API_Rev    200,,"
    "Device     MF,Variant    0,Var_Rev    0,API_Rev    0,,"
    "------------------------------,"
    "SLOT       6,BOARD       VS_REG_DSP,STATE      DownloadBoot,"
    "Device     DB,Variant    3,Var_Rev    205,API_Rev    200,,"
    "Device     MF,Variant    0,Var_Rev    0,API_Rev    0,,"
    "Device     DEVICE_2,Variant    0,Var_Rev    0,API_Rev    0,,"
    "------------------------------,SLOT       9,BOARD       VS_ANA_INTK_2,STATE      ProductionBoot,"
    "Device     MF,Variant    4,Var_Rev    21,API_Rev    1,,"
    "------------------------------,SLOT       12,BOARD       VS_DIG_INTK,STATE      DownloadBoot,"
    "Device     DB,Variant    3,Var_Rev    231,API_Rev    200,,"
    "Device     MF,Variant    0,Var_Rev    0,API_Rev    0,,"
)


FW_FILE = Path(__file__).parent / "data" / "toplevel_VS_STATE_CTRL_db_DOWNLDBOOT_3_1590427655.prg"


class MockAdapter(Adapter):
    @property
    def fw_file_loc(self):
        return "/user/pclhc/etc/program_manager"

    def get_expected(self, fgc_name):
        assert fgc_name == "RPAGM.866.21.ETH1"

        expected_data = {
            "6": {
                "board": "VS_REG_DSP",
                "devices": {
                    "DEVICE_2": Device(Device="DEVICE_2", Variant="IGBT_33", Var_Rev="302", API_Rev="302"),
                    "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="305", API_Rev="305"),
                },
            },
            "10": {
                "board": "VS_ANA_INTK_2",
                "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="23", API_Rev="23")},
            },
            "9": {
                "board": "VS_ANA_INTK_2",
                "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="23", API_Rev="23")},
            },
            "12": {
                "board": "VS_DIG_INTK",
                "devices": {
                    "DB": Device(Device="DB", Variant="DOWNLDBOOT_3", Var_Rev="303", API_Rev="303"),
                    "MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="304", API_Rev="304"),
                },
            },
            "5": {
                "board": "VS_STATE_CTRL",
                "devices": {
                    "MF": Device(Device="MF", Variant="IGBT_34", Var_Rev="307", API_Rev="307"),
                    "DB": Device(Device="DB", Variant="DOWNLDBOOT_3", Var_Rev="303", API_Rev="303"),
                },
            },
            "14": {
                "board": "VS_BIS_INTK_2",
                "devices": {"MF": Device(Device="MF", Variant="STANDARD_4", Var_Rev="306", API_Rev="306")},
            },
        }
        expected_files = {
            "6": {
                "board": "VS_REG_DSP",
                "devices": {
                    "DEVICE_2": "https://edms.cern.ch/ws/api/files/2342441/302/toplevel_VS_REG_DSP_device_2_IGBT_33_1589898288.prg",
                    "MF": "https://edms.cern.ch/ws/api/files/2342439/305/toplevel_VS_REG_DSP_mf_IGBT_34_1639751721.prg",
                },
            },
            "10": {
                "board": "VS_ANA_INTK_2",
                "devices": {
                    "MF": "https://edms.cern.ch/ws/api/files/2340052/23/toplevel_VS_ANA_INTK_2_mf_STANDARD_4_1639751763.prg"
                },
            },
            "9": {
                "board": "VS_ANA_INTK_2",
                "devices": {
                    "MF": "https://edms.cern.ch/ws/api/files/2340052/23/toplevel_VS_ANA_INTK_2_mf_STANDARD_4_1639751763.prg"
                },
            },
            "12": {
                "board": "VS_DIG_INTK",
                "devices": {
                    "DB": "https://edms.cern.ch/ws/api/files/2679019/303/toplevel_VS_DIG_INTK_db_DOWNLDBOOT_3_1639751674.prg",
                    "MF": "https://edms.cern.ch/ws/api/files/2340057/304/toplevel_VS_DIG_INTK_mf_STANDARD_4_1639751611.prg",
                },
            },
            "5": {
                "board": "VS_STATE_CTRL",
                "devices": {
                    "MF": "https://edms.cern.ch/ws/api/files/2342434/307/toplevel_VS_STATE_CTRL_mf_IGBT_34_1639751693.prg",
                    "DB": "https://edms.cern.ch/ws/api/files/2349228/303/toplevel_VS_STATE_CTRL_db_DOWNLDBOOT_3_1590427655.prg",
                },
            },
            "14": {
                "board": "VS_BIS_INTK_2",
                "devices": {
                    "MF": "https://edms.cern.ch/ws/api/files/2340061/306/toplevel_VS_BIS_INTK_2_mf_STANDARD_4_1600679250.prg"
                },
            },
        }

        return expected_data, expected_files


# TODO: can we just use real class from requests?
@dataclass
class MockHttpResponse:
    content: bytes

    status_code: int = 200
    headers: dict = field(default_factory=lambda: {})

    def raise_for_status(self):
        pass


@dataclass
class MockFgcResponse:
    value: str = ""
    err_code: str = ""
    err_msg: str = ""


class MockSession:
    def __init__(self, fgc_name):
        self.fgc = fgc_name

        self._scenario = [
            ("get", ("REGFGC3.SLOT_INFO",), MockFgcResponse(SLOT_INFO_STRING)),
            ("set", ("REGFGC3.PROG.LOCK", "DISABLED"), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.FSM.MODE", "WAITING"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("WAITING")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("WAITING")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "TRANSFERRED"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("TRANSFERRING")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("WAITING")),
            ("set", ("REGFGC3.PROG.SLOT", "5"), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.DEVICE", "DB"), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.VARIANT", "DOWNLDBOOT_3"), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.VARIANT_REVISION", "303"), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.API_REVISION", "303"), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.BIN_SIZE_BYTES", 341822), MockFgcResponse()),
            ("set", ("REGFGC3.PROG.BIN_CRC", 9243), MockFgcResponse()),
            # TODO: REGFGC3.PROG.BIN[...]
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("TRANSFERRING")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("WAITING")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "TRANSFERRED"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("TRANSFERRED")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("TRANSFERRING")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "PROGRAMMED"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("GET_PROG_INFO")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("TRANSFERRED")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "PROGRAMMED"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("PROGRAMMING")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("GET_PROG_INFO")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "PROGRAMMED"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("PROG_CHK")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("PROGRAMMING")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "PROGRAMMED"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("PROGRAMMED")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("PROG_CHK")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "CLEAN_UP"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("CLEAN_UP")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("PROGRAMMED")),
            ("set", ("REGFGC3.PROG.FSM.MODE", "WAITING"), MockFgcResponse()),
            ("get", ("REGFGC3.PROG.FSM.STATE",), MockFgcResponse("WAITING")),
            ("get", ("REGFGC3.PROG.FSM.LAST_STATE",), MockFgcResponse("CLEAN_UP")),
            ("set", ("REGFGC3.PROG.LOCK", "ENABLED"), MockFgcResponse()),
            ("set", ("REGFGC3.SLOT_INFO", ""), MockFgcResponse()),
            ("get", ("REGFGC3.SLOT_INFO",), MockFgcResponse(SLOT_INFO_STRING)),
            ("set", ("REGFGC3.PROG.MODE", "PROG_FAILED"), MockFgcResponse()),
            ("get", ("REGFGC3.SLOT_INFO",), MockFgcResponse(SLOT_INFO_STRING)),
            ("disconnect",),
        ]

    def disconnect(self):
        (operation,) = self._scenario.pop(0)
        assert operation == "disconnect"

    def get(self, *args):
        if len(self._scenario) == 0:
            raise Exception(f"Unexpected MockSession.get{args}")

        operation, args_exp, result = self._scenario.pop(0)
        assert operation == "get" and args_exp == args
        return result

    def set(self, *args):
        if args[0].startswith("REGFGC3.PROG.BIN["):
            return MockFgcResponse()

        if len(self._scenario) == 0:
            logging.warning(f"Unexpected MockSession.set{args}")
            raise Exception(f"Unexpected MockSession.set{args}")

        operation, args_exp, result = self._scenario.pop(0)
        assert operation == "set" and args_exp == args
        return result


@patch("ccs_sso_utils.save_sso_cookie")
@patch("requests.get")
def test_fgc_job(requests_get, save_sso_cookie, caplog):
    caplog.set_level(logging.DEBUG)

    save_sso_cookie.return_value = {"dummy": "cookie"}
    requests_get.return_value = MockHttpResponse(
        FW_FILE.read_bytes(), headers={"content-type": "application/octet-stream"}
    )

    job_name = "RPAGM.866.21.ETH1"

    adapter = MockAdapter()
    blob_repo = EdmsBlobRepository("https://edms.cern.ch/ws/api/files", Path("/tmp/program_manager_cache"))

    logger = logging.getLogger("test")

    with tempfile.TemporaryDirectory() as dir:
        path = Path(dir) / "log.jsonl"
        slog = structured_logging.Writer(log_file_path=path, retention_days=14)

        fgc_job(job_name=job_name, adapter=adapter, blob_repo=blob_repo, logger=logger, slog=slog, session_factory=MockSession)

        slog.close()
        assert Path(slog._handler.baseFilename).stat().st_size > 0  # check that a structured log entry has been emitted
