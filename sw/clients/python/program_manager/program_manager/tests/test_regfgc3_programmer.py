from contextlib import contextmanager
from unittest.mock import patch

from program_manager import regfgc3_programmer

from .test_fw_utils import DATA_DIR
from .test_reprogramming import MockSession, MockFgcResponse


@patch("docopt.docopt")
@patch("pyfgc.connect")
@patch("pyfgc.fgc")
@patch("pyfgc.get")#, wraps=_mock_pyfgc_get)
@patch("pyfgc.set")
# Watch out: mocks are passed positionally, _in reverse order_
def test_regfgc3_programmer(pyfgc_set, pyfgc_get, pyfgc_fgc, pyfgc_connect, docopt):
    CONVERTER = "RFNA.000.MOCK.1"

    session = MockSession(CONVERTER)
    session._scenario = [
        ("set", ("REGFGC3.SLOT_INFO", ""), MockFgcResponse()),
    ] + session._scenario[:-5] + [
        ("set", ("REGFGC3.PROG.SLOT", "5"), MockFgcResponse()),
        ("set", ("REGFGC3.PROG.DEBUG.ACTION", "SWITCH"), MockFgcResponse()),
        ("set", ("REGFGC3.SLOT_INFO", ""), MockFgcResponse()),
        ("disconnect",),
    ]

    @contextmanager
    def mock_pyfgc_fgc(target):
        assert target == CONVERTER
        yield session

    def mock_pyfgc_get(converter, key):
        assert converter == CONVERTER
        return session.get(key)

    def mock_pyfgc_set(converter, key, value):
        assert converter == CONVERTER
        return session.set(key, value)

    docopt.return_value = {
        "--assumeyes":      True,
        "--loose":          False,
        "--stay-db":        False,
        "--verbosity":      "INFO",
        "<converter>":      CONVERTER,
        "<slot>":           "5",
        "<board>":          "VS_STATE_CTRL",
        "<device>":         "DB",
        "<variant>":        "DOWNLDBOOT_3",
        "<var_revision>":   "303",
        "<api_revision>":   "303",
        "<fw_repo>":        "dummy",        # TODO why can't we do better
        "<fw_file_name>":   str(DATA_DIR / "toplevel_VS_STATE_CTRL_db_DOWNLDBOOT_3_1590427655.prg"),
    }

    pyfgc_fgc._mock_wraps = mock_pyfgc_fgc
    pyfgc_get._mock_wraps = mock_pyfgc_get
    pyfgc_set._mock_wraps = mock_pyfgc_set
    pyfgc_connect.return_value = session

    regfgc3_programmer.main()

    session.disconnect()
