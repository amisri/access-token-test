import pyfgc


"""
Flashing the firmware for larger FPGAs can take longer than the default FGC command timeout.
The FGC protocol requires that the operation completes within one command execution.
It is not feasible to re-do the protocol now, so we must increase the command timeout.

Time calculations (courtesy of @jgomezco):
- Transfer a Slice (4Kwords) to the card: 64 msec, if 2msec per exchange via SCIVS (worst case)
- Program a Slice: 255 msec
- Maximum number of Slices to transfer: 390
    - In fact = ceiling (File_length / 4096)
- Worst case: 390 * (64 + 255) / 1000 = 102 sec
- Computing of CRC: 3.2 sec

---

See also discussion in https://issues.cern.ch/browse/EPCCCS-9498

"""
_PROGRAMMING_TIMEOUT = 150


# This must be a real function rather than functools.partial as long as there are tests
# that need to @patch("pyfgc.connect")
def pyfgc_connect(*args, **kwargs):
    return pyfgc.connect(*args, **kwargs, timeout_s=_PROGRAMMING_TIMEOUT)
