from abc import ABC, abstractmethod
from collections import namedtuple
from dataclasses import dataclass


class BoardState:
    DOWNLOAD_BOOT = "DownloadBoot"
    PRODUCTION_BOOT = "ProductionBoot"


Board = namedtuple("Board", "SLOT, BOARD, STATE, devices")
# Device = namedtuple("Device", "Device, Variant, Var_Rev, API_Rev")

@dataclass
class Device:
    _fields = ["Device", "Variant", "Var_Rev", "API_Rev"]

    Device: str
    Variant: str
    Var_Rev: str
    API_Rev: str

ProgramInfo = namedtuple("ProgramInfo", "board, device, variant, var_rev, api_rev")


class BlobRepository(ABC):
    # TODO: this is not a good abstraction, because the format of `path` differs between EDMS
    #       (2339832/311/toplevel_VS_REG_DSP_device_2_LPS4Q_130_1641813961.prg)
    #       and filesystem repository
    #       (VS_STATE_CTRL/MF/MAXIDISCAP_23/toplevel_VS_STATE_CTRL_mf_MAXIDISCAP_23_1583506281.prg)
    #
    #       Perhaps the correct solution is to have distinct types for the 2 kinds of path,
    #       and no base class for the blob repository implementation.
    #
    #       Further refactoring hinges upon higher test coverage of regfgc3_programmer, though.
    @abstractmethod
    def get_blob_by_path(self, path: str) -> bytes:
        ...
