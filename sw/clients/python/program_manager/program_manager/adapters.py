from abc import ABC, abstractmethod
import logging
import os
from collections import namedtuple

import cx_Oracle

from program_manager.model import Device


logger = logging.getLogger("pm_main." + __name__)


# TODO: some amount of type checking for adapter_data
def get_adapter(adapter, adapter_data: dict):
    if adapter == "db":
        return DbAdapter(**adapter_data)

    if adapter == "fs":
        return FileSystemAdapter(**adapter_data)

    raise ValueError(f"Invalid adapter type: '{adapter}'")


class Adapter(ABC):
    @abstractmethod
    def get_expected(self, fgc_name):
        ...


class DbAdapter(Adapter):
    DbRow = namedtuple("DbRow",
                    """id_location, 
                        logical_name, 
                        eda_board, 
                        slot_number,
                        board_name_fgc, 
                        component_type, 
                        variant,
                        edms_version,
                        edms_number,
                        edms_file_name"""
                    )

    _fw_file_loc: str

    def __init__(self, connection_string, username, password, fw_file_loc):
        self._fw_file_loc = fw_file_loc

        # Create singleton SessionPool. This could be probably handled in a cleaner way.
        if not hasattr(DbAdapter, "pool"):
            logger.info("Creating database session pool")
            DbAdapter.pool = cx_Oracle.SessionPool(username,
                                                   password,
                                                   connection_string,
                                                   min=2,
                                                   max=20,
                                                   encoding="UTF-8",
                                                   threaded=True)

        self.pool = DbAdapter.pool

    @property
    def fw_file_loc(self):
        return self._fw_file_loc

    def get_expected(self, fgc_name):
        expected_data  = dict()
        expected_files = dict()

        db_connection = self.pool.acquire()
        cursor = db_connection.cursor()

        QUERY = """select ID_LOCATION, 
                        LOGICAL_NAME,
                        EDA_BOARD,
                        SLOT_NUMBER,
                        BOARD_NAME_FGC,
                        COMPONENT_TYPE,
                        VARIANT,
                        EDMS_VERSION,
                        EDMS_NUMBER,
                        EDMS_FILE_NAME   
                from ALIM.FIRMWARE_PARAMETERS_VIEW
                where LOGICAL_NAME = :logical_name"""

        for row in cursor.execute(QUERY, {"logical_name": fgc_name}):
            self._format_expected_data(expected_data, expected_files, DbAdapter.DbRow(*row))
        
        self.pool.release(db_connection)
        return expected_data, expected_files

    def _format_expected_data(self, expected_data, expected_files, row):
        slot     = str(row.slot_number)
        revision = row.edms_version
        dev      = Device(row.component_type, row.variant, revision, revision)
        
        try:
            expected_data[slot]

        except KeyError:
            expected_data[slot]  = {"board": row.board_name_fgc, "devices": dict()}
            expected_files[slot] = {"board": row.board_name_fgc, "devices": dict()}

        # This doesn't belong here at all! The problem is that we need 'row.edms_file_name'
        fw_file_path = "/".join([row.edms_number, revision, row.edms_file_name])
        expected_data[slot]["devices"].update({dev.Device: dev})
        expected_files[slot]["devices"].update({dev.Device: fw_file_path})
        

class FileSystemAdapter(Adapter):
    def __init__(self, fw_subfolder, db_subfolder, fw_file_loc):
        self._db_files = os.path.join(fw_file_loc, db_subfolder)
        self._fw_files = os.path.join(fw_file_loc, fw_subfolder)
        
        self._converter_last_time_updated = dict()

        self._logger = logging.getLogger("pm_main." + __name__)
        self._logger.debug(f"Adapter {type(self).__name__} created")

    def get_expected(self, fgc_name):
        expected_data  = dict()
        expected_files = dict()
        expected_converters = os.listdir(self._db_files)

        if fgc_name not in expected_converters:
            raise FileNotFoundError
                
        expected_converter_file       = os.path.join(self._db_files, fgc_name)
        expected_data, expected_files = self._parse_expected_file(expected_converter_file)
        # last_time_updated       = int(os.path.getmtime(expected_converter_file))
        
        # try:
        #     last_time = self._converter_last_time_updated[fgc_name]
        
        # except KeyError:
        #     self._converter_last_time_updated[fgc_name] = last_time_updated
        #     expected_data, expected_files = self._parse_expected_file(expected_converter_file)
        
        # else:
            # if last_time_updated > last_time:
            #     expected_data, expected_files = self._parse_expected_file(expected_converter_file)

            # else:
            #     self._logger.info(f"File {expected_converter_file} has not been updated since last time checked")
        
        return expected_data, expected_files

    def _parse_expected_file(self, file_name):
        #TODO: protect access to file? One thread should only access one file, so might not be needed
        lines         = list()
        expected_data = dict()
        expected_files = dict()

        with open(file_name, "r") as f:
            lines = f.readlines()
        
        for l in lines:
            if l.startswith("#"):
                continue

            l = l.strip()
            slot, board, *dev_info, fw_file_name = l.split(",")
            dev = Device(*dev_info)

            try:
                expected_data[slot]
                expected_files[slot]

            except KeyError:
                expected_data[slot]  = {"board": board, "devices": dict()}
                expected_files[slot] = {"board": board, "devices": dict()}

            expected_data[slot]["devices"].update({dev.Device: dev})
            fw_file_canonical_path = os.path.join(self._fw_files, board, dev.Device, dev.Variant, fw_file_name)
            expected_files[slot]["devices"].update({dev.Device: fw_file_canonical_path})

        return expected_data, expected_files

