from dataclasses import dataclass
from datetime import datetime, timedelta
import logging
import os
import subprocess
import threading
import time
from typing import Dict, Iterable, Optional
from pathlib import Path

import pyfgc
import pyfgc_name
import pyfgc_statussrv

from program_manager.area_worker import AreaProgramManager
from program_manager.area_worker import fgc_job
from program_manager.edms_client import EdmsBlobRepository
from program_manager import structured_logging


ITERATION_STATUS_SRV_SEC = 5
REFRESH_KERBEROS_TICKET_SEC = 11 * 3600
STATUS_SRV_REFRESH_SEC = 5


logger = logging.getLogger("pm_main." + __name__)


def filter_pending_devices(status_rsp, verbose):
    gateways_alive = 0
    fgcs_selected = 0
    fgcs_total = 0

    now = time.time()

    for gw in status_rsp.keys():
        recv_time_sec = status_rsp[gw]["recv_time_sec"]

        if recv_time_sec >= (now - (STATUS_SRV_REFRESH_SEC * 2 + 1)):
            gateways_alive += 1

            for dev in status_rsp[gw]["devices"].keys():
                dev_dict = status_rsp[gw]["devices"][dev]

                # Completely skip FGCs that don't even have ST_UNLATCHED, to prevent spurious warnings:
                # 98 FGCD_EQ_STATUS
                # 150 CALSYS_CDC
                # 151 CALSYS_SM
                if dev_dict["CLASS_ID"] in {98, 150, 151}:
                    continue

                try:
                    if "SYNC_PROG_MGR" in dev_dict["ST_UNLATCHED"]:
                        fgcs_selected += 1
                        device_obj = pyfgc_name.devices[dev]
                        yield dev, pyfgc_name.gateways[device_obj["gateway"]]["groups"][0]
                
                except KeyError as ex:
                    if verbose:
                        logger.warning("%s: %s raised in filter_pending_devices", dev, repr(ex))

            fgcs_total += len(status_rsp[gw]["devices"])
        elif verbose:
            logger.warning("Gateway %s unresponsive for %d minutes (recv_time_sec=%d)",
                    gw, (now - recv_time_sec) / 60, recv_time_sec)

    if verbose:
        logger.info("%d/%d gateways in good shape; %d/%d FGCs requiring attention",
                gateways_alive, len(status_rsp), fgcs_selected, fgcs_total)


@dataclass
class ServerOptions:
    secrets_location: Path
    crash_on_status_error: bool

    firmware_blobs_edms_url: Optional[str]
    firmware_blobs_cache_dir: Optional[Path]


class ProgramManagerServer:
    _area_pms: Dict[str, AreaProgramManager]

    def __init__(self, *, pm_running_account, name_file, expected_data, adapter_data, filter_groups, exclude_groups,
            options: ServerOptions, slog: structured_logging.Writer):
        self.pm_running_account = pm_running_account
        self.name_file          = name_file
        self.expected_data      = expected_data
        self.adapter_data       = adapter_data
        self.filter_groups      = filter_groups
        self.exclude_groups     = exclude_groups
        self._slog = slog
                
        self._run           = threading.Event()
        self._status_dump_requested = threading.Event()
        self._area_pms      = dict()
        
        self._last_kerberos_ticket_time = None
        self._status_srv_conn = None

        self._keytab_file_location = options.secrets_location

        self._should_crash_on_status_error = options.crash_on_status_error

        assert options.firmware_blobs_edms_url is not None  # TODO: need to do better
        self._blob_repo = EdmsBlobRepository(edms_url=options.firmware_blobs_edms_url,
                                             cache_dir=options.firmware_blobs_cache_dir)

        self._logger = logging.getLogger("pm_main." + __name__ + type(self).__name__)

    def start(self):
        self._logger.info("Starting Program Manager Server")
        self._slog.write(structured_logging.ServerRestart(timestamp=datetime.now()))
        pyfgc_name.read_name_file()
        pyfgc_name.read_group_file()

        # If an allow-list of zones has been provided, consider only those
        areas: Iterable[str]
        if self.filter_groups is not None:
            areas = self.filter_groups
        else:
            areas = pyfgc_name.groups.keys()

        for area in areas:
            # Skip any excluded zones
            if self.exclude_groups is not None and area in self.exclude_groups:
                self._logger.info("Skipping excluded area %s", area)
                continue

            self._logger.info("Monitoring area %s", area)
            self._area_pms[area] = AreaProgramManager(area, self.expected_data, self.adapter_data,
                                                      blob_repo=self._blob_repo,
                                                      slog=self._slog)

        VERBOSE_PERIOD = timedelta(minutes=15)

        last_verbose = None

        # Continue running until stopped
        while not self._run.is_set():
            self._obtain_kerberos_ticket()

            if not self._status_srv_conn:
                self._get_status_srv_connection()

            # Get status of all FGCs
            try:
                fgcds = pyfgc_statussrv.get_status_all(fgc_session=self._status_srv_conn)
            except RuntimeError:
                if self._should_crash_on_status_error:
                    raise

                self._logger.warning("Status server error, dropping session and backing off for 5 seconds", exc_info=True)
                self._clean_status_srv_connection()
                time.sleep(5)

                # TODO: Use the opportunity to also reload name & group files
                # pyfgc_name.read_name_file()
                # pyfgc_name.read_group_file()
                continue

            if self._status_dump_requested.is_set():
                self._status_dump_requested.clear()

                path = f"/tmp/program_manager_{os.getpid()}.log"

                self._logger.info("Dumping status data to %s on user request", path)
                with open(path, "wt") as f:
                    resp = self._status_srv_conn.get("ALL")
                    f.write(str(resp))
                    f.write("\n")
                    f.write(repr(fgcds))
                    f.write("\n")

                last_verbose = None

            now = datetime.now()

            if last_verbose is None or now - last_verbose > VERBOSE_PERIOD:
                verbose = True
                last_verbose = now
            else:
                verbose = False

            # Select devices with "SYNC_PROG_MGR" in ST_UNLATCHED
            filtered = list(filter_pending_devices(fgcds, verbose=verbose))

            # Generate tasks for pending FGCs
            for device, area in filtered:
                if area in self._area_pms:  # area that we care about?
                    self._area_pms[area].add_job(fgc_job, device)

            # Sleep for a few seconds and poll again
            time.sleep(ITERATION_STATUS_SRV_SEC)

            
    def _get_status_srv_connection(self):
        try:
            self._status_srv_conn = pyfgc.connect("FGC_STATUS")
            self._logger.info("Opened connection to status server")
        except pyfgc.PyFgcError as pe:
            self._logger.exception("Could not establish connection with status server")
            self._clean_status_srv_connection()
            
    def _clean_status_srv_connection(self):
        if self._status_srv_conn:
            self._status_srv_conn.disconnect()
            self._status_srv_conn = None

    def _obtain_kerberos_ticket(self):
        if  (not self._last_kerberos_ticket_time
            or (time.time() - self._last_kerberos_ticket_time) > REFRESH_KERBEROS_TICKET_SEC):

            keytab_file_name = self.pm_running_account + '.kt'
            s = subprocess.run(['kinit', '-kt', str(self._keytab_file_location / keytab_file_name), self.pm_running_account])

            if s.returncode == 0:
                self._last_kerberos_ticket_time = time.time()
                self._logger.info(f"Kerberos token acquired successfully")
            
            else:
                self._logger.error(f"Could not get kerberos token! Access to EDMS not available!")

    def request_status_dump(self) -> None:
        self._status_dump_requested.set()

    def stop(self):
        self._logger.info("Stopping Program Manager Server")
        self._run.set()
        for area in self._area_pms.keys():
            self._area_pms[area].wait_completion()
        
        if self._status_srv_conn:
            self._status_srv_conn.disconnect()
            self._status_srv_conn = None
        
        self._logger.info("Program Manager Server stopped")
