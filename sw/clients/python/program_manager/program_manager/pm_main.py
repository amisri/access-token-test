#!/usr/bin/python3

"""program_manager

Usage:
    program_manager [-v] [-c --config-file=<C>]
    program_manager -h | --help

Options:
    -h --help           Show this help.
    -v --verbosity      Increase output verbosity [default: INFO].
    -c --config-file C  Program Manager configuration file location [default: ../data/pm_config.cfg].
"""

# Imports 
import docopt
import logging
import signal
import sys
from typing import Optional, Tuple
from pathlib import Path

from configparser import SafeConfigParser
from logging import handlers
import sentry_sdk

from program_manager.pm_server import ProgramManagerServer, ServerOptions
from program_manager import structured_logging
from program_manager.__version__ import __version__


LOG_RETENTION_DAYS = 14


# PEP 8: Direct inheritance from BaseException is reserved for exceptions where catching them is almost always the wrong thing to do.
#  -> as is exactly the case here
class ProgramManagerTermError(BaseException):
    pass


def _get_db_pwd(secrets_location: Path, username: str):
    with open(secrets_location / username) as fh:
        return fh.readline().rstrip()

def _parse_list(input_str: str) -> Optional[list]:
    """Receives a list of items, either directly or through a file.
    Parses them into a list.

    Args:
        input (str): List of items, comma-separated, or path to file.

    Returns:
        list: List of items
    """
    if Path(input_str).is_file():
        with open(input_str) as filter_file:
            lines = filter(lambda x: not x.strip().startswith("#"), filter_file.readlines())
            contents = [
                s.strip() for line in lines for s in line.split(",")
            ]
    else:
        contents = [s.strip() for s in input_str.split(",")]

    filtered_contents = list(filter(None, contents))
    return filtered_contents if filtered_contents else None

def shutdown_pm(signum, frame):
    logger = logging.getLogger("pm_main")
    logger.info(f"Received signal {signum}, shutting down")

    raise ProgramManagerTermError(f"Signal {signum} received")

# TODO: no point in passing 'log' by argument
def start_pm(log, config_info, options: ServerOptions, slog: structured_logging.Writer):
    pms = None

    def usr1_handler(signum, frame):
        if pms:
            pms.request_status_dump()

    signal.signal(signal.SIGTERM, shutdown_pm)
    signal.signal(signal.SIGINT, shutdown_pm)
    signal.signal(signal.SIGUSR1, usr1_handler)

    log.info("Signal handlers configured")

    # If an exception occurs post-startup, we try to finish all pending jobs and exit with an non-zero error code.
    try:
        pms = ProgramManagerServer(pm_running_account = config_info["pm_running_account"],
                                        name_file     = config_info["name_file"],
                                        expected_data = config_info["expected_data"],
                                        adapter_data  = config_info["adapter_data"],
                                        filter_groups = config_info["filter_groups"],
                                        exclude_groups = config_info["exclude_groups"],
                                        options=options,
                                        slog=slog)
        pms.start()
    
    except ProgramManagerTermError:
        if pms:
            pms.stop()
        log.info("ProgramManagerServer terminated. Exiting...")
        sys.exit(0)

    except Exception:
        log.exception("Unhandled exception in PM server")
        if pms:
            pms.stop()
        sys.exit(1)


# TODO: migrate to logging.config.fileConfig: https://docs.python.org/3/library/logging.config.html
#       (then also the -v argument becomes obsolete)
def configure_logger(verbosity: bool, log_file_path: str) -> logging.Logger:
    root_logger = logging.getLogger()

    # Standard error output; timestamp will be prepended by systemd, so don't bother
    ch = logging.StreamHandler()
    # ch.setLevel(severity_threshold)
    ch.setFormatter(logging.Formatter("%(levelname)-7s %(message)s"))
    root_logger.addHandler(ch)

    # File handler: include timestamp for every entry.
    # fh = handlers.RotatingFileHandler(log_file_path, maxBytes=1000000, backupCount=LOG_FILES_NUMBER)
    fh = handlers.TimedRotatingFileHandler(log_file_path,
                                           when='midnight',
                                           interval=1,
                                           backupCount=LOG_RETENTION_DAYS)
    # fh.setLevel(severity_threshold)
    fh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-7s %(message)s"))
    root_logger.addHandler(fh)

    # Variable verbosity for own messages
    severity_threshold = logging.DEBUG if verbosity else logging.INFO
    logger = logging.getLogger("pm_main")
    logger.setLevel(severity_threshold)

    # Logging for 3rd-party modules should be less sensitive
    # We cannot go to INFO for now, since there is some spam from charset_normalizer
    root_logger.setLevel(logging.WARNING)

    return logger

def read_config_file(config_file_name: str) -> Tuple[dict, ServerOptions]:
    config = SafeConfigParser()
    config.read(config_file_name)

    name_file          = config.get("BASIC", "name_file_location")
    fw_repo_loc        = config.get("BASIC", "pm_config_location")
    expected_data      = config.get("BASIC", "expected_data_location")
    log_file_path      = config.get("BASIC", "pm_log_file_path")
    pm_running_account = config.get("BASIC", "pm_running_account")
    secrets_location   = Path(config["BASIC"].get("secrets_location", "/user/pclhc/etc/private/program_manager"))

    filter_groups_str = config["BASIC"].get("filter_groups", "")
    filter_groups = _parse_list(filter_groups_str)

    exclude_groups_str = config["BASIC"].get("exclude_groups", "")
    exclude_groups = _parse_list(exclude_groups_str)

    if filter_groups is not None and exclude_groups is not None:
        raise Exception("Only one of {filter_groups, exclude_groups} should be specified")

    conn_string, username, password = [""] * 3
    fw_subfolder, db_subfolder      = [""] * 2

    if expected_data == "db":
        connection_string = config.get("db", "connection_string")
        username     = config.get("db", "username")
        password     = _get_db_pwd(secrets_location, username)
        adapter_data = dict(connection_string=connection_string, username=username, password=password)

    if expected_data == "fs":
        fw_subfolder = config.get("fs", "fw_subfolder")
        db_subfolder = config.get("fs", "db_subfolder")
        adapter_data = dict(fw_subfolder=fw_subfolder, db_subfolder=db_subfolder)

    adapter_data["fw_file_loc"] = fw_repo_loc

    config_file_dict: dict = dict(zip(
                                ("pm_running_account", "name_file", "log_file_path", "expected_data"),
                                (pm_running_account,   name_file,    log_file_path,   expected_data)
                                )
                            )

    config_file_dict["adapter_data"] = adapter_data
    config_file_dict["filter_groups"] = filter_groups
    config_file_dict["exclude_groups"] = exclude_groups
    config_file_dict["sentry_url"] = config["BASIC"].get("sentry_url", None)
    config_file_dict["sentry_environment"] = config["BASIC"].get("sentry_environment", None)

    def pathify(s: Optional[str]) -> Optional[Path]:
        return Path(s) if s is not None else None

    # All newly-added options should go here to benefit from type-checking
    options = ServerOptions(
        secrets_location=secrets_location,
        crash_on_status_error=config["BASIC"].getboolean("crash_on_status_error", False),

        firmware_blobs_edms_url=config["firmware_blobs"].get("edms_url", None),
        firmware_blobs_cache_dir=pathify(config["firmware_blobs"].get("cache_dir", None)),
    )

    return config_file_dict, options
    
def main():
    # TODO: migrate away from docopt, since it doesn't have a stable (1.x) release
    args        = docopt.docopt(__doc__)
    config_info, options = read_config_file(args["--config-file"])

    # Start sentry monitoring
    if config_info["sentry_url"] is not None:
        # If the environment is literally 'production', use just the package version as release version
        # Otherwise append +<environment> to distinguish pre-release versions
        #
        # Release naming convention follows https://docs.sentry.io/platforms/python/configuration/releases/
        if config_info["sentry_environment"] == "production":
            release = f"program_manager@{__version__ }"
        else:
            release = f"program_manager@{__version__ }+{config_info['sentry_environment']}"

        sentry_sdk.init(dsn=config_info["sentry_url"],
                        environment=config_info["sentry_environment"],
                        release=release)

    log         = configure_logger(args["--verbosity"], config_info["log_file_path"])
    slog = structured_logging.Writer(log_file_path=Path(config_info["log_file_path"]).with_suffix(".jsonl"),
                                     retention_days=LOG_RETENTION_DAYS)
    start_pm(log, config_info, options, slog=slog)

# Program Manager entry point
if __name__ == "__main__":
    main()
