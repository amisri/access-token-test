#!/bin/sh
USER_HOME=/user/pclhc

if [ -f ${USER_HOME}/.profile ]; then
    . /user/pclhc/.profile
else
    . /user/pclhc/.bashrc
fi

# Executable
PM_HOME=${USER_HOME}/bin/python/program_manager
PM_VENV_DIR_NAME=pm_env
PM_DIR_IN_VENV_NAME=program_manager

# Config file
PM_CONFIG_HOME=${USER_HOME}/etc/program_manager
PM_CONFIG_FILE=pm_config.cfg

PYTHON_VENV_ACTIVE=0

is_python_venv_active()
{
    PYTHON_COMMAND="import sys; sys.stdout.write('1')\
    if hasattr(sys, 'sys.real_prefix') or (hasattr(sys, 'base_prefix') and sys.base_prefix != sys.prefix)\
    else sys.stdout.write('0')"

    PYTHON_VENV_ACTIVE=$(python3 -c "$PYTHON_COMMAND")
}

is_python_venv_active

if [ "${PYTHON_VENV_ACTIVE}" != 1 ]; then
    echo "Activating program manager virtual environment..."
    source ${PM_HOME}/${PM_VENV_DIR_NAME}/bin/activate
fi

python3 -m program_manager.pm_main -v -c ${PM_CONFIG_HOME}/${PM_CONFIG_FILE} &
pm_pid=$!

trap "kill $pm_pid; wait $pm_pid" SIGHUP SIGINT SIGTERM
#EOF