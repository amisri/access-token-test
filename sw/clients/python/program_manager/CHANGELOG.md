## 1.4.3 - 2023-05-19

### Fixed
- Compatibility with pyfgc 2.0


## 1.4.2 - 2023-01-13

### Fixed
- Yellow color in regfgc3_programmer output was unreadable against a light background (EPCCCE-1978)


## 1.4.1 - 2023-01-06

### Changed
- Reduced number of programming attempts: 2 for hosted service, 1 for standalone tool (EPCCCS-9498)
- Minor improvements to web UI and command-line tool UX


## 1.4.0 - 2022-12-06

### Added
- Support for firmware files in Format B (EDMS 2305763) (EPCCCS-9498)
- Support for programming large devices by increasing command timeout to 150 seconds (EPCCCS-9498)

### Fixed
- Invalid variant name (one not recognized by the FGC) now triggers a more sensible error message


## 1.3.2 - 2022-03-02

### Fixed
- regfgc3_programmer crash if `-y` option specified


## 1.3.1 - 2022-03-01

### Fixed
- regfgc3_programmer hotfix


## 1.3.0 - 2022-03-01

### Added
- regfgc3_programmer command-line entry point
- regfgc3_programmer option `-y`/`--assumeyes`
- Configuration option `secrets_location`
- Dockerfile & image build jobs

### Fixed
- Updated dependency ccs_sso_utils due to incompatibile change in SSO page
- Small fixes in web log viewer


## 1.2.5 - 2022-02-09

### Added
- Caching of firmware downloads, improving operational reliability

### Fixed
- Logging improvements
- Reliability improvements


## 1.2.4 - 2022-02-04

### Added
- Web-based operations log viewer

### Fixed
- Logging improvements
- Reliability improvements


## 1.2.3 - 2022-01-31

### Fixed
- Logging improvements


## 1.2.2 - 2022-01-26

### Fixed
- An infinite logic loop if no boards are in DownloadBoot
- Added a delay before re-scanning cards after programming


## 1.2.1 - 2022-01-26

### Fixed
- Logging improvements


## 1.2.0 - 2022-01-26

### Added
- Structured logging for more precised insight into the converter state and actions taken

### Changed
- Plaintext logs have been made more straightforward and consistent

### Fixed
- Errors in communication with the status server do not trigger a restart of the service anymore
- Transient errors in communication with SSO and EDMS are now handled transparently


## 1.1.10 - 2022-01-21

### Added
- A configuration option to set the Sentry DSN and environment name

### Changed
- The number of workers per area has been set to 1, pending investigation of programming issues

### Fixed
- A crash when trying to re-program a converter outside of monitored areas
- Error responses from FGCs are being checked as they should
- Fixed slow service start-up due to constant re-creation of the database session pool


## 1.1.9 - 2022-01-18

### Added
- It is now possible to limit the monitored areas (groups), making it feasible to run multiple independent instances
  of the Program Manager


## 1.1.8 - 2022-01-06

### Added
- When programming is blocked due to variant mismatch, log the syntax of a command that can be used to override this check (EPCCCS-9084)

### Changed
- Firmware attributes Git_sha and UTC are now recognized and no warning is emitted for them (EPCCCS-9084)


## 1.1.7 - 2021-11-25

### Changed
- Parsing of attribute names in REGFGC3.SLOT_INFO is now case-insensitive (EPCCCS-9037)


## 1.1.6 - 2021-11-23

### Changed
- More flexible and robust parsing of REGFGC3.SLOT_INFO (EPCCCS-8944)
- The change log should now conform to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).


## Older versions

v 1.1.4 - Release date: 27.07.2021
    - [EPCCCS-8834] Replace old SSO with new one for EDMS

v 1.1.3 - Release date: 29.09.2020
    - [EPCCCS-XXXX] Property initialize fgc_session variable in area_workers, not use it if invalid session
   
v 1.1.2 - Release date: 25.09.2020
    - [EPCCCS-XXXX] Trasfer binary in smaller chunks to prevent pkt buffer full error from FGC
  
v 1.1.1 - Release date: 19.08.2020
    - [EPCCCS-XXXX] Fix bug in _boards_variants_are_the_same by which devices in expected and detected were (almost) always different
  
v 1.1.0 - Release date: 07.07.2020
    - [EPCCCS-8172] Fix bug that made a converter be added twice to the reprogramming jobs list
    - [EPCCCS-8172] Make after programming checks more robust: swapped boards, different variants
    - [EPCCCS-8172] Add program manager running account as parameter in config file
    - [EPCCCS-8172] Acquire kerberos token every 11 hours to get files from EDMS
    - [EPCCCS-8172] Improve logging by including FGC name whenever possible
    - [EPCCCS-8172] Pass FW repository location as argument instead of hard-coding it
    - [EPCCCS-8172] Increase program manager log files to twenty
    - [EPCCCS-7913] Get file from NFS if not be able to get it from EDMS
    - [EPCCCS-7913] Replace materialized view by older view 
    - [EPCCCS-XXXX] Increment timeout when requesting document to EDMS
  
v 1.0.11 - Release date: 09.06.2020
    - [EPCCCS-XXXX] Update view to query for the FW data

v 1.0.10 - Release date: 25.05.2020
    - [EPCCCS-XXXX] Replace SYNC_REGFGC3 flag to SYNC_PROG_MGR
   
v 1.0.9 - Release date: 15.05.2020
    - [EPCCCS-XXXX] Scan crate before attempting reprogram boards. 
  
v 1.0.8 - Release date: 15.05.2020
    - [EPCCCS-XXXX] Add optional input argument to programmer so that boards can stay in DB after reprogramming. 
  
v 1.0.7 - Release date: 11.05.2020
    - [EPCCCS-7913] Adapt config file and code to parse it to get stuff from pclhc account. 
   
v 1.0.6 - Release date: 11.05.2020
    - [EPCCCS-7913] Adapt config file and code to parse it to get stuff from pclhc account. 
    - [EPCCCS-7913] Compare board's revisions, also for non-reprogrammable boards.

v 1.0.5 
    - [EPCCCS-7913] Implement changes after second hackathon.
  
v 1.0.4
    - [EPCCCS-7913] Implement changes after first hackathon.
  
v 1.0.3
    - [EPCCCS-6678] Add database connection pool. Reduce logging. 
    - [EPCCCS-XXXX] Remove logger stream handler.
    - [EPCCCS-7716] Add variables to launcher script.
    - [EPCCCS-7716] Add launcher to package distribution.
    - [EPCCCS-6678] Initial code for the program manager package.