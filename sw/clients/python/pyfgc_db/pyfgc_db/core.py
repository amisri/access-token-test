from contextlib import contextmanager
import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.exc
import sqlalchemy.engine
import sqlalchemy.engine.url
from sqlalchemy.pool import NullPool
import cx_Oracle

from .models import Base, ViewBase
from .constants import Driver


class SingletonMeta(type):
    _instance = None

    def __call__(self):
        if self._instance is None:
            self._instance = super().__call__()
        return self._instance


class FgcDb(metaclass=SingletonMeta):
    _engine = None
    _SessionMaker = None

    @staticmethod
    def config_db(drivername, username=None, password=None, host=None, port=None, database=None, create=False):
        try:
            if FgcDb._engine is None:
                if drivername == Driver.ORACLE:
                    dsn_tns = cx_Oracle.makedsn(host, port, service_name=database)
                    db_url = sqlalchemy.engine.url.URL.create(drivername, username, password, dsn_tns)
                else:
                    db_url = sqlalchemy.engine.url.URL.create(drivername, username, password, host, port, database)

                FgcDb._engine = sqlalchemy.create_engine(db_url, poolclass=NullPool)

            if FgcDb._SessionMaker is None:
                FgcDb._SessionMaker = sqlalchemy.orm.sessionmaker(bind=FgcDb._engine)

            if create:
                FgcDb._create_all()

        except sqlalchemy.exc.NoSuchModuleError:
            raise FgcDbConfigError(f"driver {drivername} does not exist")
        except sqlalchemy.exc.OperationalError as err:
            raise FgcDbConfigError(err.orig)
        except Exception as err:
            raise FgcDbConfigError("DB ERROR: " + str(err))

    @staticmethod
    def _drop_all():
        sqlalchemy.orm.session.close_all_sessions()

        # manually drop views via custom queries
        if FgcDb._engine.name in [Driver.POSTGRESQL, Driver.SQLITE]:
            drop_view_command = "DROP VIEW IF EXISTS"
        else:
            drop_view_command = "DROP VIEW"
        with FgcDb._engine.connect() as con:
            for view_mapper in ViewBase.registry.mappers:
                viewname = view_mapper.class_.__tablename__
                con.exec_driver_sql(f"{drop_view_command} {viewname};")

        # automatically drop tables via defined models
        Base.metadata.drop_all(FgcDb._engine)

    @staticmethod
    def _create_all():
        # automatically create tables via defined models
        Base.metadata.create_all(FgcDb._engine)

        # manually create views via custom DDL queries
        with FgcDb._engine.connect() as con:
            for view_mapper in ViewBase.registry.mappers:
                try:
                    ddl = view_mapper.class_._ddl
                    con.exec_driver_sql(ddl[FgcDb._engine.name])
                except AttributeError as err:
                    raise FgcDbConfigError(f"CREATE VIEW ERROR: '_ddl' dictionary not defined in {view_mapper.class_.__name__}.")
                except KeyError as err:
                    raise FgcDbConfigError(f"CREATE VIEW ERROR: No query for engine {FgcDb._engine.name} defined in {view_mapper.class_.__name__}._ddl")
                except Exception as err:
                    raise FgcDbConfigError(f"CREATE VIEW ERROR: {str(err)}")

    @staticmethod
    def create(*table_names: str):
        """Creates specified tables.

        If no or all tables are specified, the `_create_all` method
        is called.

        Args:
            *table_names: Strings representing table names.

        Raises:
            ValueError: Passed table names do not match existing tables.
        """
        table_names_set = set(table_names)
        all_table_names_set = set(Base.metadata.tables.keys())

        if not table_names_set or table_names_set == all_table_names_set:
            FgcDb._create_all()
        elif all_table_names_set.issuperset(table_names_set):
            tables_to_create = [
                Base.metadata.tables[table_name]
                for table_name in table_names_set
            ]
            Base.metadata.create_all(
                FgcDb._engine,
                tables=tables_to_create
            )
        else:
            raise ValueError("Passed table names do not match existing tables.")

    @staticmethod
    def create_history_triggers(*table_names: str):
        """Creates history triggers.

        Args:
            *table_names: Strings representing table names. If no table
                names are specified, triggers for all non-history tables
                are instantiated.

        Raises:
            ValueError: Passed table names do not match existing tables.
        """
        SUPPORTED_DRIVERS = [Driver.POSTGRESQL, Driver.ORACLE]
        if FgcDb._engine.name not in SUPPORTED_DRIVERS:
            raise NotImplementedError(f"Creation of history triggers not implemented for {FgcDb._engine.name}")

        # table_names = [table_mapper.class_.__tablename__ for table_mapper in Base.registry.mappers]
        # All table names except for the names of the history tables.
        non_hist_table_names = [
            table_name
            for table_name in Base.metadata.tables.keys()
            if not table_name.endswith('_hist')
        ]

        if table_names:
            table_names_set = set(table_names)
            non_hist_table_names_set = set(non_hist_table_names)
            if not non_hist_table_names_set.issuperset(table_names_set):
                raise ValueError("Passed table names do not match existing tables.")
        else:
            table_names = non_hist_table_names

        if FgcDb._engine.name == Driver.POSTGRESQL:
            from postgres_templates.hist_triggers import TRIGGER_FUNCTION_QUERY_TEMPLATE, TRIGGER_CREATION_QUERY_TEMPLATE

            with FgcDb._engine.connect() as con:
                for table_name in table_names:
                    # prepare column names to be used with the query templates
                    column_names = Base.metadata.tables[table_name].columns.keys()
                    old_column_names = [f"OLD.{col}" for col in column_names]
                    new_column_names = [f"NEW.{col}" for col in column_names]
                    column_names = ', '.join(column_names)
                    old_column_names = ', '.join(old_column_names)
                    new_column_names = ', '.join(new_column_names)

                    # create a trigger function and the trigger itself
                    trigger_function = TRIGGER_FUNCTION_QUERY_TEMPLATE % (table_name, table_name, column_names, old_column_names, table_name, column_names, new_column_names)
                    trigger_creation = TRIGGER_CREATION_QUERY_TEMPLATE % (table_name, table_name, table_name)
                    con.exec_driver_sql(trigger_function)
                    con.exec_driver_sql(trigger_creation)
        # In case of Oracle, we make use of the CCDB's history framework.
        # See https://wikis.cern.ch/display/TEEPCCCS/FGC+DB+History+Framework
        elif FgcDb._engine.name == Driver.ORACLE:
            session = FgcDb.get_session()
            for table_name in table_names:
                # Convert table case to uppercase, otherwise it fails.
                table_name = table_name.upper()
                trigger_history_framework_oracle = sqlalchemy.text(
                    f"""
                    begin
                        com_history_mgr.enable_history('{table_name}');
                    end;
                    """
                )
                session.execute(trigger_history_framework_oracle)
            session.commit()

    @staticmethod
    def get_session():
        return FgcDb._SessionMaker()

    @staticmethod
    @contextmanager
    def session():
        session = FgcDb._SessionMaker()
        try:
            yield session
        finally:
            session.close()


class FgcDbConfigError(Exception):
    pass

# EOF
