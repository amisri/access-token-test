from datetime import datetime, timezone

import pytest
from sqlalchemy.exc import IntegrityError

from pyfgc_db.models import *
from .context import configure
from ..constants import Driver


FgcDb = configure()
session = FgcDb.get_session()

CURRENT_DRIVER = session.bind.dialect.name


@pytest.fixture(autouse=True)
def setup_tests(request):
    """Drop and create the database before and after each test.
    TODO: maybe replace this with mocking instead of this"""

    # Setup
    FgcDb._drop_all()
    FgcDb._create_all()

    # Perform Test
    yield

    # Tear Down
    FgcDb._drop_all()
    FgcDb._create_all()


@pytest.fixture()
def insert_operational_spare_spare_combination():
    """Inserts operational, spare, and spare combination systems."""
    # Insert operational, spare, and spare combination.
    operational = System(name="OPERATIONAL", is_obsolete=0, is_spare_combination=0)
    spare = System(name="SPARE", is_obsolete=0, is_spare_combination=0)
    spare_combination = System(name="SPARE_COMBINATION", is_obsolete=0, is_spare_combination=1)

    session.add(operational)
    session.add(spare)
    session.add(spare_combination)
    session.flush()

    return (operational, spare, spare_combination)


def test_system():
    typ = Type(name='SYS_TYP', component_code='12345', description='SYS_TYP DESC', multi_sys=1)
    platform = Platform(name='SYS_PLA', description="SYS_DESC")
    clazz = Class(name='SYS_CLASS', description='SYS_CLASS_DESC')
    clazz.platform = platform

    system = System(name='SYS_NAME', is_obsolete=0, is_spare_combination=0, class_sys=clazz, type=typ)
    session.add(system)
    session.commit()

    # assert
    assert session.query(System).filter(System.name == 'SYS_NAME').count() == 1
    assert session.query(Platform).count() == 1
    assert session.query(Class).first().systems[0] == system
    assert session.query(Type).first().systems[0] == system


def test_platform():
    platform = Platform(name='PLATFORM', description="DESC")
    session.add(platform)
    session.commit()

    # assert
    assert session.query(Platform).filter(Platform.name == 'PLATFORM').count() == 1
    assert session.query(Platform).filter(Platform.description == 'DESC').count() == 1


def test_class():
    platform = Platform(name='PLATFORM', description="DESC")

    clazz = Class(name='CLASS', description='CLASS_DESC')
    clazz.platform = platform
    session.add(clazz)
    session.commit()

    # assert
    assert session.query(Class).filter(Class.name == 'CLASS').count() == 1
    assert session.query(Class).filter(Class.description == 'CLASS_DESC').count() == 1
    assert session.query(Class).filter(Class.name == 'CLASS').first().platform == platform


def test_property():
    prop = Property(name='PROP')
    session.add(prop)
    session.commit()

    # assert
    assert session.query(Property).filter(Property.name == 'PROP').count() == 1


def test_type():
    typ = Type(name='TYP', component_code='12345', description='TYP DESC', multi_sys=1)
    session.add(typ)
    session.commit()

    # assert
    assert session.query(Type).filter(Type.name == 'TYP').count() == 1


def test_scope():
    scope = Scope(name='SCO')
    session.add(scope)
    session.commit()

    # assert
    assert session.query(Scope).filter(Scope.name == 'SCO').count() == 1


def test_category():
    category = Category(name='CAT')
    session.add(category)
    session.commit()

    # assert
    assert session.query(Category).filter(Category.name == 'CAT').count() == 1


def test_system_property():
    prop = Property(name='SYS_PROP')
    system = System(name='SYS_PROP', is_obsolete=0, is_spare_combination=0)

    system_property = SystemProperty()
    system_property.system = system
    system_property.property = prop
    system_property.value = "123213"
    session.add(system_property)
    session.commit()

    # assert
    assert session.query(SystemProperty).filter(SystemProperty.value == "123213").count() == 1
    assert system_property in session.query(Property).filter(Property.name == "SYS_PROP").first().system_properties
    assert system_property in session.query(System).filter(System.name == "SYS_PROP").first().system_properties


def test_type_property():
    prop = Property(name='TYP_PROP')
    typ = Type(name='STP', component_code='12345', description='TYP DESC', multi_sys=1)

    type_property = TypeProperty()
    type_property.type = typ
    type_property.property = prop
    type_property.value = "123213"
    session.add(type_property)
    session.commit()

    # assert
    assert session.query(TypeProperty).filter(TypeProperty.value == "123213").count() == 1
    assert type_property in session.query(Property).filter(Property.name == "TYP_PROP").first().type_properties
    assert type_property in session.query(Type).filter(Type.name == "STP").first().type_properties


def test_property_type_defaults():
    prop = Property(name='DEF_PROP')
    typ = Type(name='DTP', component_code='12345', description='TYP DESC', multi_sys=1)

    type_defaults = PropertyTypeDefault()
    type_defaults.type = typ
    type_defaults.property = prop
    type_defaults.default_value = "123213"
    session.add(type_defaults)
    session.commit()

    # assert
    assert session.query(PropertyTypeDefault).filter(PropertyTypeDefault.default_value == "123213").count() == 1
    assert type_defaults in session.query(Property).filter(Property.name == "DEF_PROP").first().type_defaults
    assert type_defaults in session.query(Type).filter(Type.name == "DTP").first().type_defaults


def test_property_class():
    prop = Property(name='CLS_PROP')
    clazz = Class(name='CLASS_PROP', description='CLASS_DESC')
    clazz.platform = Platform(name='PLACLSPR', description="SYS_DESC")

    property_class = PropertyClass()
    property_class.property = prop
    property_class.class_pro = clazz

    session.add(property_class)
    session.commit()

    # assert
    assert session.query(PropertyClass).count() == 1
    assert property_class in session.query(Property).filter(Property.name == "CLS_PROP").first().class_properties
    assert property_class in session.query(Class).filter(Class.name == "CLASS_PROP").first().properties


def test_property_type_scope():
    prop = Property(name='SCP_PROP')
    typ = Type(name='PTS', component_code='12345', description='TYP DESC', multi_sys=1)
    scope = Scope(name='TSC')

    type_scope = PropertyTypeScope()
    type_scope.type = typ
    type_scope.property = prop
    type_scope.scope = scope
    session.add(type_scope)
    session.commit()

    # assert
    assert session.query(PropertyTypeScope).count() == 1
    assert type_scope in session.query(Property).filter(Property.name == "SCP_PROP").first().type_scopes
    assert type_scope in session.query(Type).filter(Type.name == "PTS").first().type_scopes
    assert type_scope in session.query(Scope).filter(Scope.name == "TSC").first().type_scopes


def test_property_category():
    prop = Property(name='CA_PROP')
    scope = Scope(name='SCA')
    category = Category(name="PCAT")

    property_category = PropertyCategory()
    property_category.property = prop
    property_category.category = category
    property_category.scope = scope
    session.add(property_category)
    session.commit()

    # assert
    assert session.query(PropertyCategory).count() == 1
    assert property_category in session.query(Property).filter(Property.name == "CA_PROP").first().category_properties
    assert property_category in session.query(Category).filter(Category.name == "PCAT").first().category_properties
    assert property_category in session.query(Scope).filter(Scope.name == "SCA").first().category_properties


def test_platform_types():
    platform = Platform(name='PLATP', description="SYS_DESC")
    typ = Type(name='PLTT', component_code='12345', description='TYP DESC', multi_sys=1)

    platform_types = PlatformType()
    platform_types.platform = platform
    platform_types.type = typ

    session.add(platform_types)
    session.commit()

    # assert
    assert session.query(PlatformType).count() == 1
    assert platform_types in session.query(Platform).filter(Platform.name == "PLATP").first().platform_types
    assert platform_types in session.query(Type).filter(Type.name == "PLTT").first().type_platforms


def test_component():
    typ = Type(name='CMP', component_code='12345', description='TYP DESC', multi_sys=1)

    parent = Component()
    parent.mtf_id = "555"
    parent.type = typ

    component = Component()
    component.mtf_id = "123321"
    component.type = typ
    component.parent = parent

    session.add(component)
    session.commit()

    # assert
    assert session.query(Component).filter(Component.mtf_id == "123321").count() == 1
    assert component in session.query(Type).filter(Type.name == "CMP").first().components
    assert session.query(Component).filter(Component.mtf_id == "123321").first().parent == parent


def test_component_property():
    prop = Property(name='CMP_PROP')
    component = Component()
    component.mtf_id = "666"
    component.type = Type(name='CMP', component_code='12345', description='TYP DESC', multi_sys=1)

    component_property = ComponentProperty()
    component_property.property = prop
    component_property.component = component
    component_property.value = "asdf"

    session.add(component_property)
    session.commit()

    # assert
    assert session.query(ComponentProperty).filter(ComponentProperty.value == "asdf").count() == 1
    assert component_property in session.query(Property).filter(Property.name == "CMP_PROP").first().component_properties
    assert component_property in session.query(Component).filter(Component.mtf_id == "666").first().component_properties


def test_component_system():
    system = System(name='SYSC_PROP', is_obsolete=0, is_spare_combination=0)

    component = Component()
    component.mtf_id = "667"
    component.type = Type(name='CMS', component_code='12345', description='TYP DESC', multi_sys=1)

    component_system = ComponentSystem()
    component_system.component = component
    component_system.system = system

    session.add(component_system)
    session.commit()

    # assert
    assert session.query(ComponentSystem).count() == 1
    assert component_system in session.query(Component).filter(Component.mtf_id == "667").first().component_systems
    assert component_system in session.query(System).filter(System.name == "SYSC_PROP").first().component_systems


def test_spare_combination_mapping(insert_operational_spare_spare_combination):
    """Tests normal behaviour of `fgc_spare_combination_mappings`."""
    operational, spare, spare_combination = insert_operational_spare_spare_combination

    spare_mapping = SpareCombinationMapping(
        operational_id=operational.id,
        spare_id=spare.id,
        combination_id=spare_combination.id
    )

    session.add(spare_mapping)
    session.commit()

    # Assert the mapping refers to appropriate system IDs.
    assert spare_mapping.operational_id == operational.id
    assert spare_mapping.spare_id == spare.id
    assert spare_mapping.combination_id == spare_combination.id


def test_spare_combination_mapping_last_update(insert_operational_spare_spare_combination):
    """Tests `scm_last_update_utc` column in `fgc_spare_combination_mappings`."""
    operational, spare, spare_combination = insert_operational_spare_spare_combination

    spare_mapping = SpareCombinationMapping(
        operational_id=operational.id,
        spare_id=spare.id,
        combination_id=spare_combination.id
    )
    session.add(spare_mapping)
    last_update = datetime.now(timezone.utc)
    spare_mapping.last_update = last_update
    session.commit()

    # Assert the mapping contains the correct last activation change time.
    last_update_naive = last_update.replace(tzinfo=None)
    assert spare_mapping.last_update == last_update_naive


def test_spare_combination_mapping_spare_combination_uniqueness(insert_operational_spare_spare_combination):
    """Asserts that inserting two rows with the same `scm_comb_id` fails.

    Asserts that sqlalchemy.exc.IntegrityError is raised when inserting
    a row with `scm_comb_id` that already exists in `fgc_spare_combination_mappings`.
    """
    operational1, spare1, spare_combination = insert_operational_spare_spare_combination

    operational2 = System(name="OPERATIONAL2", is_obsolete=0, is_spare_combination=0)
    spare2 = System(name="SPARE2", is_obsolete=0, is_spare_combination=0)

    session.add(operational2)
    session.add(spare2)
    session.flush()

    spare_mapping1 = SpareCombinationMapping(
        operational_id=operational1.id,
        spare_id=spare1.id,
        combination_id=spare_combination.id
    )
    session.add(spare_mapping1)

    # Try to insert mapping with a spare combination ID that already
    # appears in the table.
    with pytest.raises(IntegrityError) as exception_info:
        spare_mapping2 = SpareCombinationMapping(
            operational_id=operational2.id,
            spare_id=spare2.id,
            combination_id=spare_combination.id
        )
        session.add(spare_mapping2)
        session.commit()
    if CURRENT_DRIVER == Driver.SQLITE:
        # If an SQLite database is used, we won't get the constraint name
        # in the error message.
        #
        # Assert a unique constraint has failed.
        assert "unique constraint failed" in str(exception_info.value).lower()
    else:
        # We can be more specific for other dialects.
        #
        # Assert the name of the constraint appears in the error message.
        assert "unique_spare_combination_id" in str(exception_info.value).lower()


def test_spare_combination_mapping_uniqueness(insert_operational_spare_spare_combination):
    """Asserts that inserting the same mapping more than once fails.

    Asserts that sqlalchemy.exc.IntegrityError is raised when inserting
    a mapping that already exists in `fgc_spare_combination_mappings`.
    """
    operational, spare, spare_combination = insert_operational_spare_spare_combination

    spare_mapping1 = SpareCombinationMapping(
        operational_id=operational.id,
        spare_id=spare.id,
        combination_id=spare_combination.id
    )
    session.add(spare_mapping1)

    # Try to insert mapping that already exists.
    with pytest.raises(IntegrityError) as exception_info:
        spare_mapping2 = SpareCombinationMapping(
            operational_id=operational.id,
            spare_id=spare.id,
            combination_id=spare_combination.id
        )
        session.add(spare_mapping2)
        session.commit()
    if CURRENT_DRIVER == Driver.SQLITE:
        # If an SQLite database is used, we won't get the constraint name
        # in the error message.
        #
        # Assert a unique constraint has failed.
        assert "unique constraint failed" in str(exception_info.value).lower()
    else:
        # We can be more specific for other dialects.
        #
        # Assert the name of the constraint appears in the error message.
        assert "unique_mapping" in str(exception_info.value).lower()


def test_spare_combination_mapping_op_and_sp_uniqueness(insert_operational_spare_spare_combination):
    """Asserts inserting the same op and sp twice with different comb fails.

    Asserts that sqlalchemy.exc.IntegrityError is raised when inserting
    a combination of operational and spare that already exists in
    `fgc_spare_combination_mappings`, but with a different combination system.
    """
    operational, spare, spare_combination = insert_operational_spare_spare_combination

    spare_combination1 = System(name="SPARE_COMBINATION1", is_obsolete=0, is_spare_combination=1)
    session.add(spare_combination1)
    session.flush()

    spare_mapping1 = SpareCombinationMapping(
        operational_id=operational.id,
        spare_id=spare.id,
        combination_id=spare_combination.id
    )
    session.add(spare_mapping1)

    # Try to insert mapping that already exists.
    with pytest.raises(IntegrityError) as exception_info:
        spare_mapping2 = SpareCombinationMapping(
            operational_id=operational.id,
            spare_id=spare.id,
            combination_id=spare_combination1.id
        )
        session.add(spare_mapping2)
        session.commit()
    if CURRENT_DRIVER == Driver.SQLITE:
        # If an SQLite database is used, we won't get the constraint name
        # in the error message.
        #
        # Assert a unique constraint has failed.
        assert "unique constraint failed" in str(exception_info.value).lower()
    else:
        # We can be more specific for other dialects.
        #
        # Assert the name of the constraint appears in the error message.
        assert "unique_mapping" in str(exception_info.value).lower()


def test_spare_combination_mapping_relationships(insert_operational_spare_spare_combination):
    """Asserts that relationships in `fgc_spare_combination_mappings` behave correctly."""
    operational, spare1, spare_combination1 = insert_operational_spare_spare_combination

    spare2 = System(name="SPARE1", is_obsolete=0, is_spare_combination=0)
    spare_combination2 = System(name="SPARE_COMBINATION2", is_obsolete=0, is_spare_combination=1)

    session.add(spare2)
    session.add(spare_combination2)
    session.flush()

    spare_mapping1 = SpareCombinationMapping(
        operational_id=operational.id,
        spare_id=spare1.id,
        combination_id=spare_combination1.id
    )
    session.add(spare_mapping1)

    spare_mapping2 = SpareCombinationMapping(
        operational_id=operational.id,
        spare_id=spare2.id,
        combination_id=spare_combination2.id
    )
    session.add(spare_mapping2)

    session.commit()

    # Assert System.spare_combination_mappings_operationals refers to
    # appropriate mappings.
    assert set([spare_mapping1, spare_mapping2]) == {
        mapping
        for mapping in operational.spare_combination_mappings_operationals
    }
    # Assert System.spare_combination_mappings_spares refers to
    # appropriate mappings.
    assert set([spare_mapping1]) == {
        mapping
        for mapping in spare1.spare_combination_mappings_spares
    }
    # Assert System.spare_combination_mappings_combination refers to
    # appropriate mapping.
    assert spare_mapping1 is spare_combination1.spare_combination_mappings_combination
    # Assert SpareCombinationMapping.operational refers to appropriate system.
    assert spare_mapping1.operational is operational
    # Assert SpareCombinationMapping.spare refers to appropriate system.
    assert spare_mapping1.spare is spare1
    assert spare_mapping2.spare is spare2
    # Assert SpareCombinationMapping.combination refers to appropriate system.
    assert spare_mapping1.combination is spare_combination1
    assert spare_mapping2.combination is spare_combination2


def test_spare_combination_mapping_op_id_foreign_key(insert_operational_spare_spare_combination):
    """Asserts that foreign key error is raised for invalid `scm_op_id`."""
    _, spare, spare_combination = insert_operational_spare_spare_combination

    # Try to insert a mapping with non-existing operational ID.
    with pytest.raises(IntegrityError) as exception_info:
        spare_mapping = SpareCombinationMapping(
            operational_id=5,
            spare_id=spare.id,
            combination_id=spare_combination.id
        )
        session.add(spare_mapping)
        session.commit()
    assert "foreign key constraint" in str(exception_info.value).lower()


def test_spare_combination_mapping_sp_id_foreign_key(insert_operational_spare_spare_combination):
    """Asserts that foreign key error is raised for invalid `scm_sp_id`."""
    operational, _, spare_combination = insert_operational_spare_spare_combination

    # Try to insert a mapping with non-existing spare ID.
    with pytest.raises(IntegrityError) as exception_info:
        spare_mapping = SpareCombinationMapping(
            operational_id=operational.id,
            spare_id=6,
            combination_id=spare_combination.id
        )
        session.add(spare_mapping)
        session.commit()
    assert "foreign key constraint" in str(exception_info.value).lower()


def test_spare_combination_mapping_comb_id_foreign_key(insert_operational_spare_spare_combination):
    """Asserts that foreign key error is raised for invalid `scm_comb_id`."""
    operational, spare, _ = insert_operational_spare_spare_combination

    # Try to insert a mapping with non-existing spare combination ID.
    with pytest.raises(IntegrityError) as exception_info:
        spare_mapping = SpareCombinationMapping(
            operational_id=operational.id,
            spare_id=spare.id,
            combination_id=7
        )
        session.add(spare_mapping)
        session.commit()
    assert "foreign key constraint" in str(exception_info.value).lower()
