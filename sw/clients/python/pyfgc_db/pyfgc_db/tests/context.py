import os
import sys
from sqlite3 import Connection as SQLite3Connection
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from sqlalchemy.engine import Engine
from sqlalchemy import event

from pyfgc_db.core import FgcDb
from pyfgc_db.constants import Driver


dbconfig_local = {
    "drivername": Driver.POSTGRESQL.value,
    "username": "fgc_user",
    "password": "fgc_pass",
    "host": "localhost",
    "port": "5432",
    "database": "fgc_db"
}

dbconfig_dbod_fgc_dev = {
    "drivername": Driver.POSTGRESQL.value,
    "username": "admin",
    "password": "",
    "host": "dbod-fgc-dev.cern.ch",
    "port": "6602",
    "database": "fgc_db"
}

dbconfig_devdb19 = {
    "drivername": Driver.ORACLE.value,
    "username": "pocontrols",
    "password": "",
    "host": "devdb19-s.cern.ch",
    "port": "10121",
    "database": "devdb19_s.cern.ch"
}

dbconfig_sqlite = {
    "drivername": Driver.SQLITE.value,
    "database": "test.db"
}

DB_CONFIG = dbconfig_local  # Specify your desired config here.

# If run in a GitLab CI job, change the test database to SQLite.
if os.environ.get('CI') == "true":
    DB_CONFIG = dbconfig_sqlite


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    """Enables foreign key constraints if SQLite is used."""
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        # print(inspect.getmodule(dbapi_connection))
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()


def create_tables(dbconfig=DB_CONFIG):
    # Creates tables in your @config specified database
    FgcDb.config_db(**dbconfig, create=True)
    return FgcDb


def configure(dbconfig=DB_CONFIG):
    # Prepares the wrapper for tests
    FgcDb.config_db(**dbconfig)
    return FgcDb


def create_history_triggers(dbconfig=DB_CONFIG):
    FgcDb.config_db(**dbconfig)
    FgcDb.create_history_triggers()
    return FgcDb
