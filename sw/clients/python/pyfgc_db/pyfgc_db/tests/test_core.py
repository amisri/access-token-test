from email import contentmanager
from pyfgc_db.core import FgcDb
from .context import configure

def test_engine():
    FgcDb = configure()

    assert FgcDb._engine is not None
    assert FgcDb._SessionMaker is not None
