import sys

from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Text,
    Boolean,
    UniqueConstraint,
    DateTime,
    PrimaryKeyConstraint,
    event,
    DDL
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.oracle import TIMESTAMP as ORACLE_TIMESTAMP
from sqlalchemy.schema import Sequence

from .constants import Driver
from .utils import utcnow


class HistoryMeta(DeclarativeMeta):
    def __new__(mcs, name, bases, dct):
        if name != "Base" and not name.endswith("Hist"):
            hst_dct = {}
            hst_dct["__tablename__"] = dct["__tablename__"] + "_hist"
            hst_dct["__module__"] = dct["__module__"]
            hst_dct["__qualname__"] = dct["__qualname__"] + "Hist"

            for key, value in dct.items():
                if isinstance(value, Column):
                    if key == "id":
                        hst_dct[key] = Column(value.name, value.type, primary_key=True)
                    else:
                        hst_dct[key] = Column(value.name, value.type)
            hst_dct["op_type"] = Column("op_type", String(1))
            hst_dct["create_time"] = Column("create_time_utc", DateTime, primary_key=True, index=True)
            hst_dct["transaction_id"] = Column("transaction_id", String(48), index=True)
            hst_dct["module"] = Column("module", String(48))
            hst_dct["action"] = Column("action", String(32))
            hst_dct["client_info"] = Column("client_info", String(64))

            history_class = type(name + "Hist", bases, hst_dct)
            setattr(sys.modules[__name__], name + "Hist", history_class)

        return super().__new__(mcs, name, bases, dct)


Base = declarative_base(metaclass=HistoryMeta)
ViewBase = declarative_base()


class Category(Base):
    __tablename__ = "fgc_categories"

    id = Column("cat_id", Integer, primary_key=True)
    name = Column("cat_name", String(30), unique=True, nullable=False, index=True)
    description = Column("cat_description", String(100))

    category_properties = relationship("PropertyCategory", back_populates="category")


class Class(Base):
    __tablename__ = "fgc_classes"

    id = Column("class_id", Integer, primary_key=True)
    name = Column("class_name", String(10), unique=True, index=True)
    description = Column("class_description", String(100))

    platform_id = Column("class_pla_id", Integer, ForeignKey("fgc_platforms.pla_id"), nullable=False)
    platform = relationship("Platform", back_populates="classes")

    systems = relationship("System", back_populates="class_sys")

    properties = relationship("PropertyClass", back_populates="class_pro")


class Component(Base):
    __tablename__ = "fgc_components"

    id = Column("cmp_id", Integer, primary_key=True)
    mtf_id = Column("cmp_mtf_id", String(20), unique=True, index=True)
    channel = Column("cmp_channel", String(10))
    is_obsolete = Column("cmp_is_obsolete", Boolean, nullable=False, default=False)

    type_id = Column("cmp_tp_id", Integer, ForeignKey("fgc_types.tp_id"), nullable=False)
    type = relationship("Type", back_populates="components")

    parent_id = Column("cmp_parent_id", Integer, ForeignKey("fgc_components.cmp_id"), index=True)
    parent = relationship("Component", back_populates="child", remote_side="Component.id")

    child = relationship("Component")

    component_properties = relationship("ComponentProperty", back_populates="component")

    component_systems = relationship("ComponentSystem", back_populates="component")

    component_mapping = relationship("ComponentMapping", back_populates="component")


class ComponentMapping(Base):
    __tablename__ = "fgc_component_mapping"
    __table_args__ = (PrimaryKeyConstraint("cma_cmp_id", "cma_chip_id"),)

    component_mtf_id = Column("cma_cmp_id", String(20), ForeignKey("fgc_components.cmp_mtf_id"), nullable=False,
                              index=True)
    chip_id = Column("cma_chip_id", String(16), nullable=False, index=True)

    component = relationship("Component", back_populates="component_mapping")


class ComponentProperty(Base):
    __tablename__ = "fgc_component_properties"
    __table_args__ = (UniqueConstraint("cpr_cmp_id", "cpr_pro_id"),)

    id = Column("cpr_id", Integer, primary_key=True)
    value = Column("cpr_value", Text)

    component_id = Column("cpr_cmp_id", Integer, ForeignKey("fgc_components.cmp_id"), nullable=False, index=True)
    component = relationship("Component", back_populates="component_properties")

    property_id = Column("cpr_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False, index=True)
    property = relationship("Property", back_populates="component_properties")


class ComponentSystem(Base):
    __tablename__ = "fgc_component_systems"
    __table_args__ = (UniqueConstraint("cs_sys_id", "cs_cmp_id"),)

    id = Column("cs_id", Integer, primary_key=True)

    component_id = Column("cs_cmp_id", Integer, ForeignKey("fgc_components.cmp_id"), nullable=False, index=True)
    component = relationship("Component", back_populates="component_systems")

    system_id = Column("cs_sys_id", Integer, ForeignKey("fgc_systems.sys_id"), nullable=False, index=True)
    system = relationship("System", back_populates="component_systems")


class Platform(Base):
    __tablename__ = "fgc_platforms"

    id = Column("pla_id", Integer, primary_key=True)
    name = Column("pla_name", String(10), unique=True, nullable=False, index=True)
    description = Column("pla_description", String(100))

    classes = relationship("Class", back_populates="platform")

    platform_types = relationship("PlatformType", back_populates="platform")


class PlatformType(Base):
    __tablename__ = "fgc_platform_types"
    __table_args__ = (UniqueConstraint("plt_pla_id", "plt_tp_id"),)

    id = Column("plt_id", Integer, primary_key=True)

    platform_id = Column("plt_pla_id", Integer, ForeignKey("fgc_platforms.pla_id"), nullable=False)
    platform = relationship("Platform", back_populates="platform_types")

    type_id = Column("plt_tp_id", Integer, ForeignKey("fgc_types.tp_id"), nullable=False)
    type = relationship("Type", back_populates="type_platforms")


class Property(Base):
    __tablename__ = "fgc_properties"

    id = Column("pro_id", Integer, primary_key=True)
    name = Column("pro_name", String(50), unique=True, nullable=False, index=True)
    description = Column("pro_description", Text)
    global_default_value = Column("pro_global_default_value", Text)

    system_properties = relationship("SystemProperty", back_populates="property")

    type_properties = relationship("TypeProperty", back_populates="property")

    type_defaults = relationship("PropertyTypeDefault", back_populates="property")

    type_scopes = relationship("PropertyTypeScope", back_populates="property")

    class_properties = relationship("PropertyClass", back_populates="property")

    category_properties = relationship("PropertyCategory", back_populates="property")

    component_properties = relationship("ComponentProperty", back_populates="property")


class PropertyCategory(Base):
    __tablename__ = "fgc_property_categories"
    __table_args__ = (UniqueConstraint("pca_cat_id", "pca_pro_id", "pca_scope_id"),)

    id = Column("pca_id", Integer, primary_key=True)

    property_id = Column("pca_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False)
    property = relationship("Property", back_populates="category_properties")

    category_id = Column("pca_cat_id", Integer, ForeignKey("fgc_categories.cat_id"), nullable=False)
    category = relationship("Category", back_populates="category_properties")

    scope_id = Column("pca_scope_id", Integer, ForeignKey("fgc_scopes.scope_id"), nullable=False)
    scope = relationship("Scope", back_populates="category_properties")


class PropertyClass(Base):
    __tablename__ = "fgc_property_classes"
    __table_args__ = (UniqueConstraint("pcl_pro_id", "pcl_class_id"),)

    id = Column("pcl_id", Integer, primary_key=True)

    property_id = Column("pcl_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False)
    property = relationship("Property", back_populates="class_properties")

    class_id = Column("pcl_class_id", Integer, ForeignKey("fgc_classes.class_id"), nullable=False)
    class_pro = relationship("Class", back_populates="properties")


class PropertyTypeDefault(Base):
    __tablename__ = "fgc_property_type_defaults"
    __table_args__ = (UniqueConstraint("ptd_pro_id", "ptd_tp_id"),)

    id = Column("ptd_id", Integer, primary_key=True)
    default_value = Column("ptd_default_value", Text)

    property_id = Column("ptd_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False)
    property = relationship("Property", back_populates="type_defaults")

    type_id = Column("ptd_tp_id", Integer, ForeignKey("fgc_types.tp_id"), nullable=False)
    type = relationship("Type", back_populates="type_defaults")


class PropertyTypeScope(Base):
    __tablename__ = "fgc_property_type_scopes"
    __table_args__ = (
        UniqueConstraint("pts_tp_id", "pts_pro_id", "pts_scope_id"),)

    id = Column("pts_id", Integer, primary_key=True)

    property_id = Column("pts_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False)
    property = relationship("Property", back_populates="type_scopes")

    type_id = Column("pts_tp_id", Integer, ForeignKey("fgc_types.tp_id"), nullable=False)
    type = relationship("Type", back_populates="type_scopes")

    scope_id = Column("pts_scope_id", Integer, ForeignKey("fgc_scopes.scope_id"), nullable=False)
    scope = relationship("Scope", back_populates="type_scopes")


class Scope(Base):
    __tablename__ = "fgc_scopes"

    id = Column("scope_id", Integer, primary_key=True)
    name = Column("scope_name", String(4), unique=True, nullable=False, index=True)

    type_scopes = relationship("PropertyTypeScope", back_populates="scope")

    category_properties = relationship("PropertyCategory", back_populates="scope")


class System(Base):
    __tablename__ = "fgc_systems"

    id = Column("sys_id", Integer, primary_key=True)
    name = Column("sys_name", String(50), nullable=False, index=True)
    is_obsolete = Column("sys_is_obsolete", Boolean, nullable=False)
    is_spare_combination = Column("sys_is_spare_combination", Boolean, nullable=False, default=False)

    type_id = Column("sys_tp_id", Integer, ForeignKey("fgc_types.tp_id"), index=True)
    type = relationship("Type", back_populates="systems")

    class_id = Column("sys_class_id", Integer, ForeignKey("fgc_classes.class_id"))
    class_sys = relationship("Class", back_populates="systems")

    system_properties = relationship("SystemProperty", back_populates="system")

    component_systems = relationship("ComponentSystem", back_populates="system")

    spare_combination_mappings_operationals = relationship(
        "SpareCombinationMapping",
        foreign_keys="SpareCombinationMapping.operational_id",
        back_populates="operational"
    )

    spare_combination_mappings_spares = relationship(
        "SpareCombinationMapping",
        foreign_keys="SpareCombinationMapping.spare_id",
        back_populates="spare"
    )

    spare_combination_mappings_combination = relationship(
        "SpareCombinationMapping",
        foreign_keys="SpareCombinationMapping.combination_id",
        back_populates="combination",
        uselist=False
    )


class SystemProperty(Base):
    __tablename__ = "fgc_system_properties"
    __table_args__ = (UniqueConstraint("spr_sys_id", "spr_pro_id"),)

    id = Column("spr_id", Integer, primary_key=True)
    value = Column("spr_value", Text)

    system_id = Column("spr_sys_id", Integer, ForeignKey("fgc_systems.sys_id"), nullable=False, index=True)
    system = relationship("System", back_populates="system_properties")

    property_id = Column("spr_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False, index=True)
    property = relationship("Property", back_populates="system_properties")


class SpareCombinationMapping(Base):
    __tablename__ = "fgc_spare_combination_mappings"
    __table_args__ = (
        UniqueConstraint("scm_op_id", "scm_sp_id", name="unique_mapping"),
        UniqueConstraint("scm_comb_id", name="unique_spare_combination_id"),
    )

    id = Column("scm_id", Integer, primary_key=True)

    operational_id = Column("scm_op_id", Integer, ForeignKey("fgc_systems.sys_id"), nullable=False)
    operational = relationship(
        "System",
        foreign_keys=operational_id,
        back_populates="spare_combination_mappings_operationals"
    )

    spare_id = Column("scm_sp_id", Integer, ForeignKey("fgc_systems.sys_id"), nullable=False)
    spare = relationship(
        "System",
        foreign_keys=spare_id,
        back_populates="spare_combination_mappings_spares"
    )

    combination_id = Column("scm_comb_id", Integer, ForeignKey("fgc_systems.sys_id"), nullable=False)
    combination = relationship(
        "System",
        foreign_keys=combination_id,
        back_populates="spare_combination_mappings_combination"
    )

    # Specify explicit data type (i.e., `TIMESTAMP`) for the
    # `scm_last_update_utc` column if an Oracle db is used to enforce
    # its usage.
    last_update = Column(
        "scm_last_update_utc",
        DateTime().with_variant(ORACLE_TIMESTAMP(), Driver.ORACLE),
        server_default=utcnow(),
        nullable=False
    )


class Type(Base):
    __tablename__ = "fgc_types"

    id = Column("tp_id", Integer, primary_key=True)
    name = Column("tp_name", String(10), unique=True, nullable=False, index=True)
    description = Column("tp_description", String(100), nullable=False)
    component_code = Column("tp_component_code", String(5), nullable=False)
    seq_number = Column("tp_seq_number", String(3))
    multi_sys = Column("tp_multi_sys", Boolean, nullable=False)
    dallas_id = Column("tp_dallas_id", Boolean, default=False)
    multiple_ids = Column("tp_multiple_ids", Integer)
    temp_prop = Column("tp_temp_prop", String(30))
    barcode_prop = Column("tp_barcode_prop", String(30))

    systems = relationship("System", back_populates="type")

    type_properties = relationship("TypeProperty", back_populates="type")

    type_defaults = relationship("PropertyTypeDefault", back_populates="type")

    type_scopes = relationship("PropertyTypeScope", back_populates="type")

    type_platforms = relationship("PlatformType", back_populates="type")

    components = relationship("Component", back_populates="type")


class TypeProperty(Base):
    __tablename__ = "fgc_type_properties"
    __table_args__ = (UniqueConstraint("tpr_tp_id", "tpr_pro_id"),)

    id = Column("tpr_id", Integer, primary_key=True)
    value = Column("tpr_value", Text)

    type_id = Column("tpr_tp_id", Integer, ForeignKey("fgc_types.tp_id"), nullable=False, index=True)
    type = relationship("Type", back_populates="type_properties")

    property_id = Column("tpr_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False, index=True)
    property = relationship("Property", back_populates="type_properties")


#########################################################################################
# Sequences                                                                             #
#########################################################################################

# Explicitly create the `FGC_SPARE_COMBINATION_MAPPINGS_SEQ` sequence
# for those SQL dialects that do not have primary key generation
# mechanisms other than an explicit sequence (used for Oracle in our case).
# PostgreSQL automatically creates a sequence and uses the SERIAL
# data type for the primary key column.
Sequence(
    name='FGC_SPARE_COMBINATION_MAPPINGS_SEQ',
    metadata=Base.metadata,
    increment=1,
    minvalue=1,
    cache=20,
    optional=True  # Use the `SERIAL` data type on PostgreSQL instead.
)


#########################################################################################
# Custom DDL statements                                                                 #
#########################################################################################

# If an Oracle db is used, use the `FGC_SPARE_COMBINATION_MAPPINGS_SEQ`
# sequence for the values of the `SCM_ID` column of the
# `FGC_SPARE_COMBINATION_MAPPINGS` table.
oracle_connect_sequence_to_column = DDL(
    """
    CREATE OR REPLACE EDITIONABLE TRIGGER "POCONTROLS"."FGC_SPARE_COMBINATION_MAPPINGS"
    BEFORE INSERT ON FGC_SPARE_COMBINATION_MAPPINGS
    FOR EACH ROW

    BEGIN
        SELECT FGC_SPARE_COMBINATION_MAPPINGS_SEQ.NEXTVAL
        INTO   :new.scm_id
        FROM   dual;
    END;
    """
)

# Execute the statement above only if an Oracle db is used and
# only after creating all the objects from the metadata.
event.listen(
    Base.metadata,
    'after_create',
    oracle_connect_sequence_to_column.execute_if(dialect=Driver.ORACLE)
)


#########################################################################################
# Table Views                                                                           #
# Generated via raw SQL as SQLAlchemy doesn't support generation of views at the moment #
#########################################################################################

class AllPropertiesView(ViewBase):
    __tablename__ = "fgc_all_properties_v"

    fgc_name = Column("sys_name", String(50), primary_key=True)
    property_name = Column("pro_name", String(50), primary_key=True)
    value = Column("pro_value", Text)

    _ddl = {}
    _ddl[Driver.POSTGRESQL.value] = \
        f"""
        CREATE OR REPLACE VIEW {__tablename__.lower()} (sys_name, pro_name, pro_value) AS
        SELECT fs2.sys_name AS sys_name, fp.pro_name AS pro_name, prop.value AS pro_value
        FROM (
            SELECT fsp.spr_sys_id AS SYS_ID, fsp.spr_pro_id AS PROP_ID, fsp.spr_value AS value
            FROM fgc_system_properties fsp
            UNION
            SELECT fs.sys_id, ftp.tpr_pro_id, ftp.tpr_value
            FROM fgc_systems fs JOIN fgc_type_properties ftp ON fs.sys_tp_id = ftp.tpr_tp_id
            UNION
            SELECT fcs.cs_sys_id, fcp.cpr_pro_id, fcp.cpr_value
            FROM fgc_component_systems fcs JOIN fgc_component_properties fcp ON fcs.cs_cmp_id = fcp.cpr_cmp_id
            UNION
            SELECT fcs.cs_sys_id, ftp.tpr_pro_id, ftp.tpr_value
            FROM fgc_component_systems fcs JOIN fgc_components fc ON fcs.cs_cmp_id = fc.cmp_id
                JOIN fgc_type_properties ftp ON fc.cmp_tp_id = ftp.tpr_tp_id
        ) AS prop
        JOIN fgc_systems AS fs2 ON prop.SYS_ID = fs2.sys_id
        JOIN fgc_properties fp ON prop.PROP_ID = fp.pro_id;
        """

    _ddl[Driver.SQLITE.value] = \
        f"""
        CREATE VIEW IF NOT EXISTS {__tablename__.lower()} (sys_name, pro_name, pro_value) AS
        SELECT fs2.sys_name AS sys_name, fp.pro_name AS pro_name, prop.value AS pro_value
        FROM (
            SELECT fsp.spr_sys_id AS SYS_ID, fsp.spr_pro_id AS PROP_ID, fsp.spr_value AS value
            FROM fgc_system_properties fsp
            UNION
            SELECT fs.sys_id, ftp.tpr_pro_id, ftp.tpr_value
            FROM fgc_systems fs JOIN fgc_type_properties ftp ON fs.sys_tp_id = ftp.tpr_tp_id
            UNION
            SELECT fcs.cs_sys_id, fcp.cpr_pro_id, fcp.cpr_value
            FROM fgc_component_systems fcs JOIN fgc_component_properties fcp ON fcs.cs_cmp_id = fcp.cpr_cmp_id
            UNION
            SELECT fcs.cs_sys_id, ftp.tpr_pro_id, ftp.tpr_value
            FROM fgc_component_systems fcs JOIN fgc_components fc ON fcs.cs_cmp_id = fc.cmp_id
                JOIN fgc_type_properties ftp ON fc.cmp_tp_id = ftp.tpr_tp_id
        ) AS prop
        JOIN fgc_systems AS fs2 ON prop.SYS_ID = fs2.sys_id
        JOIN fgc_properties fp ON prop.PROP_ID = fp.pro_id;
        """

    _ddl[Driver.ORACLE.value] = \
        f"""
        CREATE OR REPLACE FORCE EDITIONABLE VIEW "POCONTROLS"."{__tablename__.upper()}" ("SYS_NAME", "PRO_NAME", "PRO_VALUE") AS
        SELECT fs2.SYS_NAME AS sys_name, fp.PRO_NAME AS pro_name, prop.value AS pro_value
        FROM (
            SELECT fsp.SPR_SYS_ID AS sys_id, fsp.SPR_PRO_ID AS prop_id, fsp.SPR_VALUE AS value
            FROM FGC_SYSTEM_PROPERTIES fsp
            UNION ALL
            SELECT fs.SYS_ID, ftp.TPR_PRO_ID, ftp.TPR_VALUE
            FROM FGC_SYSTEMS fs JOIN FGC_TYPE_PROPERTIES ftp ON fs.SYS_TP_ID = ftp.TPR_TP_ID
            UNION ALL
            SELECT fcs.CS_SYS_ID, fcp.CPR_PRO_ID, fcp.CPR_VALUE
            FROM FGC_COMPONENT_SYSTEMS fcs JOIN FGC_COMPONENT_PROPERTIES fcp ON fcs.CS_CMP_ID = fcp.CPR_CMP_ID
            UNION ALL
            SELECT fcs.CS_SYS_ID, ftp.TPR_PRO_ID, ftp.TPR_VALUE
            FROM FGC_COMPONENT_SYSTEMS fcs JOIN FGC_COMPONENTS fc ON fcs.CS_CMP_ID = fc.CMP_ID
                JOIN FGC_TYPE_PROPERTIES ftp ON fc.CMP_TP_ID = ftp.TPR_TP_ID
        ) prop
        JOIN FGC_SYSTEMS fs2 ON prop.sys_id = fs2.SYS_ID
        JOIN FGC_PROPERTIES fp ON prop.prop_id = fp.PRO_ID;
        """


class PropertyScopesView(ViewBase):
    __tablename__ = "fgc_property_scopes_v"

    id = Column("ps_id", Integer, primary_key=True)
    scope_id = Column("ps_scope_id", Integer, ForeignKey("fgc_scopes.scope_id"), nullable=False, index=True)
    scope_name = Column("ps_scope_name", String(10), unique=True, nullable=False, index=True)

    property_id = Column("ps_pro_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False, index=True)
    property_name = Column("ps_pro_name", String(30), unique=True, nullable=False, index=True)

    # View generation is not properly supported by SQLAlchemy, so we use raw queries for needed views
    _ddl = {}
    _ddl[Driver.POSTGRESQL.value] = \
        f"""
        CREATE OR REPLACE VIEW {__tablename__.lower()} (ps_id, ps_scope_id, ps_scope_name, ps_pro_id, ps_pro_name) AS
        SELECT row_number() OVER(Order by scope_id, pro_id) as PS_ID,
            scope_id AS PS_SCOPE_ID,
            scope_name AS PS_SCOPE_NAME,
            pro_id AS PS_PRO_ID,
            pro_name AS PS_PRO_NAME
        FROM(
            SELECT scope_id, scope_name, pro_id, pro_name
            FROM fgc_properties, fgc_scopes, fgc_property_type_scopes
            WHERE pro_id = pts_pro_id
            AND scope_id = pts_scope_id
            GROUP BY scope_id, scope_name, pro_id, pro_name
        UNION
            SELECT scope_id, scope_name, pro_id, pro_name
            FROM fgc_properties, fgc_scopes, fgc_property_classes
            WHERE pro_id = pcl_pro_id
            AND scope_name = 'SYS'
            GROUP BY scope_id, scope_name, pro_id, pro_name
        ) AS V;
        """

    _ddl[Driver.SQLITE.value] = \
        f"""
        CREATE VIEW IF NOT EXISTS {__tablename__.lower()} (ps_id, ps_scope_id, ps_scope_name, ps_pro_id, ps_pro_name) AS
        SELECT row_number() OVER(Order by scope_id, pro_id) as PS_ID,
            scope_id AS PS_SCOPE_ID,
            scope_name AS PS_SCOPE_NAME,
            pro_id AS PS_PRO_ID,
            pro_name AS PS_PRO_NAME
        FROM(
            SELECT scope_id, scope_name, pro_id, pro_name
            FROM fgc_properties, fgc_scopes, fgc_property_type_scopes
            WHERE pro_id = pts_pro_id
            AND scope_id = pts_scope_id
            GROUP BY scope_id, scope_name, pro_id, pro_name
        UNION
            SELECT scope_id, scope_name, pro_id, pro_name
            FROM fgc_properties, fgc_scopes, fgc_property_classes
            WHERE pro_id = pcl_pro_id
            AND scope_name = 'SYS'
            GROUP BY scope_id, scope_name, pro_id, pro_name
        ) AS V;
        """

    _ddl[Driver.ORACLE.value] = \
        f"""
        CREATE OR REPLACE FORCE EDITIONABLE VIEW "POCONTROLS"."{__tablename__.upper()}" ("PS_ID", "PS_SCOPE_ID", "PS_SCOPE_NAME", "PS_PRO_ID", "PS_PRO_NAME") AS
        SELECT row_number() OVER(Order by scope_id, pro_id) as PS_ID,
            scope_id AS PS_SCOPE_ID,
            scope_name AS PS_SCOPE_NAME,
            pro_id AS PS_PRO_ID,
            pro_name AS PS_PRO_NAME
        FROM(
            SELECT scope_id, scope_name, pro_id, pro_name
            FROM fgc_properties, fgc_scopes, fgc_property_type_scopes
            WHERE pro_id = pts_pro_id
            AND scope_id = pts_scope_id
            GROUP BY scope_id, scope_name, pro_id, pro_name
        UNION
            SELECT scope_id, scope_name, pro_id, pro_name
            FROM fgc_properties, fgc_scopes, fgc_property_classes
            WHERE pro_id = pcl_pro_id
            AND scope_name = 'SYS'
            GROUP BY scope_id, scope_name, pro_id, pro_name
        );
        """


class SystemPropertiesView(ViewBase):
    __tablename__ = "fgc_system_properties_v"

    id = Column("system_property_id", Integer, primary_key=True)
    value = Column("system_property_value", Text)

    property_id = Column("property_id", Integer, ForeignKey("fgc_properties.pro_id"), nullable=False, index=True)
    property_name = Column("property_name", String(30), unique=True, nullable=False, index=True)

    system_id = Column("system_id", Integer, ForeignKey("fgc_systems.sys_id"), nullable=False, index=True)
    system_name = Column("system_name", String(30), unique=True, nullable=False, index=True)

    _ddl = {}
    _ddl[Driver.POSTGRESQL.value] = \
        f"""
        CREATE OR REPLACE VIEW {__tablename__.lower()} (property_id, property_name, system_id, system_name, system_property_id, system_property_value) AS
        SELECT
            pro_id          as property_id,
            pro_name        as property_name,
            spr_sys_id      as system_id,
            sys_name        as system_name,
            spr_id          as system_property_id,
            spr_value       as system_property_value
        FROM fgc_properties, fgc_system_properties, fgc_systems
        WHERE spr_pro_id = pro_id
        AND spr_sys_id = sys_id;
        """

    _ddl[Driver.SQLITE.value] = \
        f"""
        CREATE VIEW IF NOT EXISTS {__tablename__.lower()} (property_id, property_name, system_id, system_name, system_property_id, system_property_value) AS
        SELECT
            pro_id          as property_id,
            pro_name        as property_name,
            spr_sys_id      as system_id,
            sys_name        as system_name,
            spr_id          as system_property_id,
            spr_value       as system_property_value
        FROM fgc_properties, fgc_system_properties, fgc_systems
        WHERE spr_pro_id = pro_id
        AND spr_sys_id = sys_id;
        """

    _ddl[Driver.ORACLE.value] = \
        f"""
        CREATE OR REPLACE FORCE EDITIONABLE VIEW "POCONTROLS"."{__tablename__.upper()}" ("PROPERTY_ID", "PROPERTY_NAME", "SYSTEM_ID", "SYSTEM_NAME", "SYSTEM_PROPERTY_ID", "SYSTEM_PROPERTY_VALUE") AS
        SELECT
            pro_id          as property_id,
            pro_name        as property_name,
            spr_sys_id      as system_id,
            sys_name        as system_name,
            spr_id          as system_property_id,
            spr_value       as system_property_value
        FROM fgc_properties, fgc_system_properties, fgc_systems
        WHERE spr_pro_id = pro_id
        AND spr_sys_id = sys_id;
        """

# EOF
