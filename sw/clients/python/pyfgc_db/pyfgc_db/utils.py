"""This module defines miscellaneous utilities for pyfgc_db."""
from sqlalchemy.sql import expression
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.types import DateTime

from .constants import Driver


class utcnow(expression.FunctionElement):
    """SQL function for generating timezone naive timestamps in UTC.

    SQLAlchemy determines the implementation of `utcnow` depending on
    the dialect used. The implementations for specific dialects are
    given below and are registered as compilers for `utcnow` using
    the `compiles` decorator.
    """
    type = DateTime()
    inherit_cache = True


@compiles(utcnow, Driver.POSTGRESQL)
def pg_utcnow(element, compiler, **kw):
    """Outputs timezone naive timestamp in UTC time in PostgreSQL."""
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"


@compiles(utcnow, Driver.ORACLE)
def ora_utcnow(element, compiler, **kw):
    """Outputs timezone naive timestamp in UTC time in Oracle."""
    return "CAST(CURRENT_TIMESTAMP at time zone 'UTC' AS TIMESTAMP)"


@compiles(utcnow, Driver.SQLITE)
def sqlite_utcnow(element, compiler, **kw):
    """Outputs timezone naive timestamp in UTC time in SQLite."""
    return "DATETIME()"
