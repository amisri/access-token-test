"""Constants for pyfgc_db."""
from enum import Enum


class Driver(str, Enum):
    """Defines enum values for different database drivers."""
    ORACLE = "oracle"
    POSTGRESQL = "postgresql"
    SQLITE = "sqlite"
