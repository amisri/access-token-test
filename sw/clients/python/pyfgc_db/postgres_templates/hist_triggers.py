TRIGGER_FUNCTION_QUERY_TEMPLATE = """
CREATE FUNCTION public.%s_hist() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO %s_hist(%s, op_type, create_time_utc, transaction_id, module, action, client_info) VALUES(%s, 'D', now() at time zone 'utc', txid_current(), '', '', user || '@' || inet_client_addr());
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
            INSERT INTO %s_hist(%s, op_type, create_time_utc, transaction_id, module, action, client_info) VALUES(%s, substring(TG_OP, 1, 1), now() at time zone 'utc', txid_current(), '', '', user || '@' || inet_client_addr());
            RETURN NEW;
        END IF;
        RETURN NULL;
    END;
$$;
"""

TRIGGER_CREATION_QUERY_TEMPLATE = "CREATE TRIGGER %s_hist_trigger AFTER INSERT OR DELETE OR UPDATE ON public.%s FOR EACH ROW EXECUTE FUNCTION public.%s_hist();"