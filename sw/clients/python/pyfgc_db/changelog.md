VERSION 0.1.0; 23.11.2022
    - Set the version to `0.1.0` marking the strict following of the semantic versioning

VERSION 0.0.13; 26.10.2022
    - Add the `utils.py` module defining the `utcnow` function for generating
      timezone naive timestamps in UTC time
    - Add the `create` method to the `FgcDb` class to create only specific tables
    - Update the `create_history_triggers` method of the `FgcDb` class to
      allow for creating only specific history triggers & add support for Oracle

VERSION 0.0.12; 31.08.2022
    - Rename `scm_last_activation_change_utc` to `scm_last_update_utc` in the `fgc_spare_combination_mappings` table
    - Add the `constants.py` module defining the `Driver` enum for driver names

VERSION 0.0.11; 24.08.2022
    - Add SQLite `CREATE VIEW` statements
    - Replace `DROP VIEW` with `DROP VIEW IF EXISTS` for all Postgres and SQLite in `FgcDb._drop_all`
    - Remove testing the `fgc_categories` table from `FgcDb.config_db`

VERSION 0.0.10; 23.08.2022
    - Add `scm_last_activation_change_utc` column to the `fgc_spare_combination_mappings` table

VERSION 0.0.9; 23.08.2022
    - Add support for history trigger creation (postgres only for now)
    - Add support for SystemPropertiesView
    - Add the `SpareCombinationMapping` model for the new `fgc_spare_combination_mappings` table
    - Add relationships related to `SpareCombinationMapping` to the `System` model 