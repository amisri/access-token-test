from typing import List
from pydantic import BaseModel


class Cell(BaseModel):
    cells: List[str]


class TimestampedCell(Cell):
    timestamp: float


class Table(BaseModel):
    headings: Cell
    rows: List[Cell]


class EventLogTable(BaseModel):
    rows: List[TimestampedCell]


class CycleLogTable(BaseModel):
    rows: List[TimestampedCell]


class TimingLogTable(BaseModel):
    rows: List[TimestampedCell]
