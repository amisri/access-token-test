import itertools
import pandas as pd

from typing import List, Union
from pydantic import BaseModel, Field

from .signals import AnalogSignal, DigitalSignal, BodeSignal, GroupDelaySignal, ParametricSignal
from .tables import Table, EventLogTable, CycleLogTable, TimingLogTable
from .others import ApplicationSource, PropertyPair


class LogType:
    analog: str = "analog"
    digital: str = "digital"
    frequency: str = "frequency"
    parametric: str = "parametric"
    table: str = "table"
    properties: str = "properties"


class LogSubType:
    default: str = "default"
    event: str = "event-log"
    cycle: str = "cycle-log"
    timing: str = "timing-log"
    error: str = "error-log"
    bode: str = "bode"
    group_delay: str = "group-delay"
    comparison: str = "comparison"


class TimeSeries(BaseModel):
    cycleSelector: str = "0"
    firstSampleTime: float = 0
    epoch: Union[int, None] = None
    timeOrigin: float = firstSampleTime
    period: float = 1
    timestamps: List[float] = None


class LogName(BaseModel):
    name: str
    sources: List[ApplicationSource]

    class Config:
        orm_mode = True


class LogBase(BaseModel):
    name: str
    version: str
    device: str
    source: str = ""

    def to_dataframe(self):
        raise NotImplementedError('Dataframe was not designed for this type at this time.')


class ComparisonBase(BaseModel):
    test_reference_log_id: int
    test_matched: bool
    test_details: str
    test_case: str


class AnalogLogInput(LogBase, TimeSeries):
    signals: List[AnalogSignal]


class AnalogLogComparisonInput(AnalogLogInput, ComparisonBase): pass


class DigitalLogInput(LogBase, TimeSeries):
    signals: List[DigitalSignal]


class DigitalLogComparisonInput(DigitalLogInput, ComparisonBase): pass


class FrequencyLogInput(LogBase):
    frequencies: List[float] = None


class BodeLogInput(FrequencyLogInput):
    signals: List[BodeSignal]


class GroupDelayLogInput(FrequencyLogInput):
    signals: List[GroupDelaySignal]


class ParametricLogInput(LogBase):
    signals: List[ParametricSignal]


class AnalogLog(AnalogLogInput):
    type_: str = Field(LogType.analog, alias="type")
    subtype: str = LogSubType.default

    def __len__(self):
        if not self.signals:
            return 0
        else:
            return len(self.signals[0].samples)

    def to_dataframe(self):
        signal_names = [s.name for s in self.signals]
        header = list(itertools.product(signal_names, ['t_local', 'sample']))
        header.insert(0, ('t_global', '-'))
        mux = pd.MultiIndex.from_tuples(header)
        if not self.timestamps:
            global_time = [self.firstSampleTime + i * self.period for i in range(len(self))]
        else:
            global_time = self.timestamps
        data = list()
        for i, t in enumerate(global_time):
            row = [t]
            for s in self.signals:
                ts_loc = t + s.timeOffset
                row.extend([ts_loc, s.samples[i]])
            data.append(row)
        return pd.DataFrame(data, columns=mux)

    def to_csv(self, filename=None):
        """ Export log to file in csv format.

        :param filename: name of the output file
        :return:
        """
        filename = self.name if not filename else filename
        first_column = (
            f"source:{self.source}",
            "type:analog",
            "subtype: default",
            f"device:{self.device}",
            f"name:{self.name}",
            f"timeOrigin:{self.timeOrigin}",
            f"firstSampleTime:{self.firstSampleTime}",
            f"period:{self.period}"
        )

        first_column = ' '.join(first_column)
        signal_header = list()
        for s in self.signals:
            step = " STEP" if s.step else ''
            timeOffset = f" {s.timeOffset}" if s.timeOffset != 0 else ''
            signal_header.append(f"{s.name}{step}{timeOffset}")

        header = ','.join([first_column, *signal_header])
        sample_nr = 0 if not len(self.signals) else len(self.signals[0].samples)

        with open(f"{filename}.csv", "w") as f:
            f.write(header + "\n")
            for i in range(sample_nr):
                if self.timestamps:
                    t = self.timestamps[i]
                else:
                    t = self.firstSampleTime + i * self.period
                aux = ",".join([str(t), *[str(s.samples[i]) for s in self.signals]]) + "\n"
                f.write(aux)
    
    def to_json(self, filename=None):
        filename = self.name if not filename else filename
        with open(f"{filename}.json", "w") as f:
            f.write(self.json())


class AnalogLogComparison(AnalogLog, ComparisonBase):
    type_: str = Field(LogType.analog, alias="type")
    subtype: str = LogSubType.comparison


class DigitalLog(DigitalLogInput):
    type_: str = Field(LogType.digital, alias="type")
    subtype: str = LogSubType.default

    def to_dataframe(self):
        signal_names = [s.name for s in self.signals]
        header = list(itertools.product(signal_names, ['t_local', 'sample']))
        header.insert(0, ('t_global', '-'))
        mux = pd.MultiIndex.from_tuples(header)
        if not self.timestamps:
            global_time = [self.firstSampleTime + i * self.period for i in range(len(self))]
        else:
            global_time = self.timestamps
        data = list()
        for i, t in enumerate(global_time):
            row = [t]
            for s in self.signals:
                ts_loc = t + s.timeOffset
                row.extend([ts_loc, s.samples[i]])
            data.append(row)
        return pd.DataFrame(data, columns=mux)


class DigitalLogComparison(DigitalLog, ComparisonBase):
    type_: str = Field(LogType.digital, alias="type")
    subtype: str = LogSubType.comparison


class BodeLog(BodeLogInput):
    type_: str = Field(LogType.frequency, alias="type")
    subtype: str = LogSubType.bode

    def to_dataframe(self):
        signal_names = [s.name for s in self.signals]
        header = list(itertools.product(signal_names, ['gain', 'phase']))
        header.insert(0, ('freq',))
        mux = pd.MultiIndex.from_tuples(header)
        data = list()
        for i, f in enumerate(self.frequencies):
            row = [f]
            for s in self.signals:
                row.extend([s.gain[i], s.phase[i]])
            data.append(row)

        return pd.DataFrame(data, columns=mux)


class GroupDelayLog(GroupDelayLogInput):
    type_: str = Field(LogType.frequency, alias="type")
    subtype: str = LogSubType.group_delay


class ParametricLog(ParametricLogInput):
    type_: str = Field(LogType.parametric, alias="type")
    subtype: str = LogSubType.default


# Table schemas
class TableLogBase(LogBase):
    type_: str = Field(LogType.table, alias="type")
    subtype: str = None


class TableLog(TableLogBase):
    subtype: str = LogSubType.default
    table: Table


class PropertiesLog(LogBase):
    type_: str = Field(LogType.properties, alias="type")
    subtype: str = LogSubType.default
    properties: List[PropertyPair]


class ErrorLog(TableLogBase):
    subtype: str = LogSubType.error
    table: Table


class EventLog(TableLogBase):
    subtype: str = LogSubType.event
    table: EventLogTable


class CycleLog(TableLogBase):
    subtype: str = LogSubType.cycle
    table: CycleLogTable


class TimingLog(TableLogBase):
    subtype: str = LogSubType.timing
    table: TimingLogTable


class TableLogInput(LogBase):
    table: Table


class PropertiesLogInput(LogBase):
    properties: List[PropertyPair]


class ErrorLogInput(TableLogInput):
    pass


class EventLogInput(LogBase):
    table: EventLogTable


class CycleLogInput(LogBase):
    table: CycleLogTable


class TimingLogInput(LogBase):
    table: TimingLogTable


class LogMeta(BaseModel):
    name: str
    log_id: int
    device: str
    type_: str = Field(None, alias="type")
    subtype: str
    cycleSelector: str = None
    signals: List[str]
    live: bool

    class Config:
        orm_mode = True


class TestLogMeta(ComparisonBase, LogMeta): pass


AnyMetaLog = Union[LogMeta, TestLogMeta]


AnyLog = Union[
    AnalogLog,
    AnalogLogComparison,
    DigitalLog,
    DigitalLogComparison,
    BodeLog,
    GroupDelayLog,
    TableLog,
    PropertiesLog,
    ErrorLog,
    EventLog,
    CycleLog,
    TimingLog,
]


class LogFull(BaseModel):
    log_id: int
    live: bool
    data: AnyLog
    acquisition_id: int
    acquisition_source: ApplicationSource

    class Config:
        orm_mode = True



MAPPING_TABLE = {
    AnalogLogInput: (LogType.analog, LogSubType.default),
    AnalogLogComparisonInput: (LogType.analog, LogSubType.comparison),
    DigitalLogInput: (LogType.digital, LogSubType.default),
    DigitalLogComparisonInput: (LogType.digital, LogSubType.comparison),
    BodeLogInput: (LogType.frequency, LogSubType.bode),
    GroupDelayLogInput: (LogType.frequency, LogSubType.group_delay),
    ParametricLogInput: (LogType.parametric, LogSubType.default),
    PropertiesLogInput: (LogType.properties, LogSubType.default),
    TableLogInput: (LogType.table, LogSubType.default),
    ErrorLogInput: (LogType.table, LogSubType.error),
    EventLogInput: (LogType.table, LogSubType.event),
    CycleLogInput: (LogType.table, LogSubType.cycle),
    TimingLogInput: (LogType.table, LogSubType.timing),
}
