from typing import List
from pydantic import BaseModel


class SignalBase(BaseModel):
    name: str


class DigitalSignal(SignalBase):
    samples: List[bool]
    timeOffset: float = 0


class AnalogSignal(SignalBase):
    samples: List[float]
    step: bool = False
    timeOffset: float = 0


class ParametricSignal(SignalBase):
    x: List[float]
    y: List[float]


class BodeSignal(SignalBase):
    firstSampleOffset: int = 0
    gain: List[float]
    phase: List[float]


class GroupDelaySignal(SignalBase):
    firstSampleOffset: int = 0
    samples: List[float]

