import fortlogs_schemas as sc
import json


def test_midas_no_barcode():
    midas_meta = {
        'script_name': 'hello',
    }

    m = sc.MidasCreate(**midas_meta)
    converted_dict = m.dict()
    assert len(converted_dict['barcodes']) == 0

def test_midas_barcodes():
    midas_meta = {
        'script_name': 'scripty',
        'barcodes': [
            {
                'barcode': 'HCRMIC____-TT000342'
            },
            {
                'barcode': 'HCRMIC____-TT000345'
            }
        ]
    }
    m = sc.MidasCreate(**midas_meta)
    converted_dict = m.dict()
    assert len(converted_dict['barcodes']) == 2

def test_barcodes_list():
    x = sc.Barcodes(barcodes=[sc.Barcode(barcode='HCRMIC____-TT000342'), sc.Barcode(barcode='HCRMIC____-TT000343')])
    d = x.dict()
    print(d)
    assert len(d['barcodes']) == 2 