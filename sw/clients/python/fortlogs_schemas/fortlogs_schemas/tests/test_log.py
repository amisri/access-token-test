import fortlogs_schemas as sc
import random


def test_analog_log():
    log_dict: dict = {
        'type': 'analog',
        'name': 'IMEAS',
        'version': '2',
        'device': 'SOME_FGC',
        'source': 'FGC',
        'subtype': 'default',
        'signals': list()
    }

    an_lg = sc.AnalogLog(**log_dict)

    converted_dict = an_lg.dict()

    converted_dict['type'] = converted_dict.pop('type_')

    for k, v in log_dict.items():
        assert v == converted_dict[k]

def test_analog_datafram():
    samples = 10000
    log = {
        'name': 'a n a l o g  l o g',
        'version': '2.0',
        'device': 'TEST-DEVICE',
        'type': 'analog',
        'subtype': 'default',
        'timestamps': [i for i in range(samples)],
        'signals': [
            {
                'name': 'foo',
                'samples': [random.random() * 30 for _ in range(samples)],
            },
            {
                'name': 'bar',
                'samples': [random.random() * 50 for _ in range(samples)],
            },
            {
                'name': 'zee',
                'samples': [random.random() * 70 for _ in range(samples)],
            },
        ]
    }

    parsed_log = sc.AnalogLog(**log)
    df = parsed_log.to_dataframe()

    assert df.shape == (samples, len(log['signals']) * 2 + 1)


def test_digital_dataframe():
    samples = 10000
    log = {
        'name': 'd i g i  l o g',
        'version': '2.0',
        'device': 'TEST-DEVICE',
        'type': 'digital',
        'subtype': 'default',
        'timestamps': [i for i in range(samples)],
        'signals': [
            {
                'name': 'foo',
                'samples': [bool(random.getrandbits(1)) for _ in range(samples)],
            },
            {
                'name': 'bar',
                'samples': [bool(random.getrandbits(1)) for _ in range(samples)],
            },
            {
                'name': 'zee',
                'samples': [bool(random.getrandbits(1)) for _ in range(samples)],
            },
        ]
    }

    parsed_log = sc.DigitalLog(**log)
    df = parsed_log.to_dataframe()

    assert df.shape == (samples, len(log['signals']) * 2 + 1)


def test_freq_dataframe():
    samples = 10000
    log = {
        'name': 'f r e q  l o g',
        'version': '2.0',
        'device': 'TEST-DEVICE',
        'type': 'frequency',
        'subtype': 'bode',
        'frequencies': [i for i in range(samples)],
        'signals': [
            {
                'name': 'foo',
                'gain': [random.random() * 10 for _ in range(samples)],
                'phase': [random.random() * (-90) for _ in range(samples)]
            },
            {
                'name': 'bar',
                'gain': [random.random() * 3 for _ in range(samples)],
                'phase': [random.random() * 90 for _ in range(samples)]
            }
        ]
    }

    parsed_log = sc.BodeLog(**log)
    df = parsed_log.to_dataframe()

    assert df.shape == (samples, len(log['signals']) * 2 + 1)


def test_properties_log_type_and_structure():
    log = {
        'name': 'CONTROLLER_DRIVE_FILE',
        'version': '2.0',
        'device': 'TEST-DEVICE',
        'properties': [
            {  
                'key': 'prop1',
                'value': 'val1',
            },
            {  
                'key': 'prop2',
                'value': 'val2',
            },
        ]
    }

    properties_log = sc.PropertiesLog(**log)

    assert properties_log.type_ == 'properties'
    assert properties_log.subtype == 'default'
    assert properties_log.name == 'CONTROLLER_DRIVE_FILE'
    assert properties_log.properties[0].key == 'prop1'
    assert properties_log.properties[0].value == 'val1'
    assert properties_log.properties[1].key == 'prop2'
    assert properties_log.properties[1].value == 'val2'
