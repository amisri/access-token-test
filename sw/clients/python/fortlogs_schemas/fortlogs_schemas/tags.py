from pydantic import BaseModel, Field


class TagBase(BaseModel):
    tag_name: str = Field(..., min_length=2, max_length=13, regex="^[A-Z\d_\-]*$")


class Tag(TagBase):
    tag_id: int

    class Config:
        orm_mode = True
