from pydantic import BaseModel

from .others import User, ApplicationSource


class LogStats(BaseModel):
    used_space: float
    log_count: int
    acquisition_count: int


class UserStorageStats(LogStats):
    user: User
    

class AppStorageStats(LogStats):
    acquisition_source: ApplicationSource


# EOF
