import datetime
import orjson

from pydantic import BaseModel
from typing import List, Union


STOP_WORD = "!!!"


def orjson_dumps(v, *, default):
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v, default=default).decode()


class Stop:
    pass


class FrameResponse(BaseModel):
    timeout: float = None


class StreamBase(BaseModel):
    reason: str


class Stream(BaseModel):
    # User defined data
    name: str
    device: str
    time_origin: datetime.datetime
    signal_names: List[str]
    step_signals: List[str] = None
    period: float = 0
    ignore: bool = False


class StreamRegister(StreamBase):
    reason = 'register'
    acquisition_id: int
    streams: List[Stream]


class StreamsSubmitted(StreamBase): 
    reason = 'registered'
    stream_ids: List[int]


class SignalData(BaseModel):
    name: str
    samples: List[float]

    def reset(self):
        self.samples *= 0 


class BufferFrame(BaseModel):
    stream_id: int
    data: List[SignalData] 
    local_id: int
    time_start: datetime.datetime
    time_end: datetime.datetime
    timestamps: List[float]

    class Config:
        json_loads = orjson.loads
        json_dumps = orjson_dumps

    def reset(self):
        self.timestamps.clear()
        for signal_data in self.data:
            signal_data.reset()

    def add_sample(self, values):
        for i, s in enumerate(self.data):
            s.samples.append(values[i+1])


class StopFrame(BaseModel):
    stream_id: int
    data: str = STOP_WORD


#  --- Browser Client Messages --- 
class SubscribeRequest(StreamBase):
    reason = "subscribe"
    stream_ids: List[int]


class UnsubscribeRequest(StreamBase):
    reason = "unsubscribe"
    stream_ids: List[int]


class LoadFramesTimeRequest(StreamBase):
    reason = "load_frames_by_time"
    stream_id: int
    start: datetime.datetime
    end: datetime.datetime
    

class LoadFramesIdRequest(StreamBase):
    reason = "load_frames_by_id"
    stream_id: int
    start: int
    end: int


class ErrorMessage(StreamBase):
    reason = "error"
    exception: str
    message: str
