from datetime import datetime
from typing import List

from pydantic import BaseModel, Field
from pyfgc_const import Cause

from .others import User, Barcode
from .acquisitions import AcquisitionBase


class MidasBase(BaseModel):
    script_name: str = Field(..., min_length=4, max_length=35, regex="^[a-zA-Z\d_\-]*$")
    barcodes: List[Barcode] = []


class MidasCreate(MidasBase):
    pass


class Midas(MidasBase, AcquisitionBase):
    user: User
    devices: List[str]
    live: bool

    class Config:
        orm_mode = True


class FgcTestBase(BaseModel):
    script_name: str = Field(..., min_length=4, max_length=35, regex="^[a-zA-Z\d_\-]*$")
    script_description: str = Field('', max_length=35, regex="^[a-zA-Z\d_\-\s]*$")


class FgcTestCreate(FgcTestBase):
    pass


class FgcTest(FgcTestBase, AcquisitionBase):
    user: User
    devices: List[str]
    is_reference: bool
    has_unmatched: bool

    class Config:
        orm_mode = True


class FgcLoggerBase(BaseModel):
    cause: Cause
    time_event: datetime


class FgcLoggerCreate(FgcLoggerBase):
    pass


class FgcLogger(FgcLoggerBase, AcquisitionBase):
    devices: List[str]

    class Config:
        orm_mode = True


class PowerSpyBase(BaseModel):
    pass


class PowerSpyCreate(PowerSpyBase):
    pass


class PowerSpy(PowerSpyBase, AcquisitionBase):
    user: User
    devices: List[str]

    class Config:
        orm_mode = True
