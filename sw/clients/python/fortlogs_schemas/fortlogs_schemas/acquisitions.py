from datetime import datetime
from typing import List

from pydantic import BaseModel, Field

from .tags import Tag
from .others import User
from .logs import LogFull


class AcquisitionBase(BaseModel):
    acquisition_id: int
    time_started: datetime
    comment: str = Field('', max_length=200)
    trash: bool
    tags: List[Tag]
    favorite_users: List[User]
    log_count: int
    last_access: datetime

    class Config:
        orm_mode = True


class Acquisition(AcquisitionBase):
    logs: List[LogFull]

    class Config:
        orm_mode = True
