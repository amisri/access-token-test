import enum
import threading

from pydantic import BaseModel, Field
from typing import List


class ApplicationSource(str, enum.Enum):
    powerspy: str = "PowerSpy"
    fgc_logger: str = "FGC-Logger"
    midas: str = "Midas"
    fgc_tests: str = "FGC-Tests"


class Script(BaseModel):
    script_name: str

    class Config:
        orm_mode = True


class Comment(BaseModel):
    comment: str = Field('', max_length=200)


class CycleSelector(BaseModel):
    cycleSelector: str
    

class UserBase(BaseModel):
    username: str
    name: str


class User(UserBase):
    user_id: int

    class Config:
        orm_mode = True


class Device(BaseModel):
    device: str
    sources: List[ApplicationSource]

    class Config:
        orm_mode = True


class Barcode(BaseModel):
    barcode: str = Field(..., min_length=19, max_length=19)

    class Config:
        orm_mode = True


class Barcodes(BaseModel):
    barcodes: List[Barcode] = []


class Sort(str, enum.Enum):
    desc: str = "desc"
    asc: str = "asc"


class SafeCount:

    def __init__(self):
        self._value = 0
        self._lock = threading.Lock()

    @property
    def value(self):
        return self._value

    def __add__(self, other):
        with self._lock:
            self._value += other
        return self

    def __sub__(self, other):
        with self._lock:
            self._value -= other
        return self

    def __iadd__(self, other):
        with self._lock:
            self._value += other
        return self

    def __isub__(self, other):
        with self._lock:
            self._value -= other
        return self

    def __eq__(self, other):
        return self._value == other

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f"SafeCount({self.value})"


class PropertyPair(BaseModel):
    key: str
    value: str
