import asyncio
import collections
import datetime
import logging
import midas
import midas.fortlogs_client
import queue
import sys
import ssl
import threading
import websockets


from midas.utils import open_on_powerspy
from websockets.exceptions import ConnectionClosed
from fortlogs_schemas.streams import (
    Stop,
    BufferFrame,
    Stream,
    StreamRegister,
    StreamsSubmitted,
    SignalData
)
from pathlib import PurePath, Path
from typing import List


logger = logging.getLogger(__name__)


class FrameBuilder:

    def __init__(self, buffers, websocket_channel):
        self.frame_id = 0
        self.frames = {}
        self.periodic_frames = {}
        self.timestamps = collections.defaultdict(list)
        self.has_data = False
        self._to_be_removed = list()
        self._websocket_channel = websocket_channel

        for b in buffers:
            self.frames[b.id] = BufferFrame(
                stream_id=b.remote_id,
                data=[SignalData(name=signal_name, samples=[]) for signal_name in b.signals],
                timestamps=[],
                time_start=datetime.datetime.now(),
                time_end=datetime.datetime.now(),
                local_id=self.frame_id
            )
            self.periodic_frames[b.id] = True if b.period is not None else False

    @property
    def is_finished(self):
        return len(self.frames) == 0 and self.has_data is False

    def reset_frames(self):
        for local_id, frame in self.frames.items():
            frame.reset()
        self.timestamps.clear()
        self.has_data = False

    def add_samples(self, stream_id, values):
        # For periodic signals only push value data
        self.frames[stream_id].add_sample(values)
        self.timestamps[stream_id].append(values[0])
        self.has_data = True

    async def dispatch_frames(self):
        for local_id, frame in self.frames.items():
            if not self.timestamps[local_id]:
                continue
            frame.time_start = self.timestamps[local_id][0]
            frame.time_end = self.timestamps[local_id][-1]
            frame.local_id = self.frame_id
            if not self.periodic_frames[local_id]:
                frame.timestamps = self.timestamps[local_id]
            await self._websocket_channel.send(frame.json())
        self.frame_id += 1

    def schedule_removal(self, stream_id):
        self._to_be_removed.append(stream_id)

    def execute_scheduled_removal(self):
        for stream_id in self._to_be_removed:
            self.frames.pop(stream_id, None)
            self.timestamps.pop(stream_id, None)


class Aggregator(threading.Thread):
    """This object aggregates messages from all publishing buffers and sends
    them through a websocket.
    Buffers samples are accumulated until ACCUMULATE_TIMEOUT is reached, then
    a Stream frame is built and sent through a websocket.
    """
    # time between each FortLogs message
    ACCUMULATE_TIMEOUT: datetime = datetime.timedelta(seconds=0.1)
    CONNECT_ATTEMPTS: int = 3
    CONNECT_TIMEOUT: float = 1
    STOP_STREAM: str = '!!!'
    EVENT_LOOP_WAIT: float = 0.001

    def __init__(self, open_on_powerspy: bool, acquisition_name: str, barcodes: List[str]):
        super().__init__()
        # Single queue or multiple queue
        # Let's start with a single one to avoid having to deal with timeouts
        self._aggregator_queue = queue.Queue()
        self._publishers = list()
        self._event_loop = asyncio.new_event_loop()
        self._frame_builder = None
        self._open_on_powerspy = open_on_powerspy
        self._acquisition_id = None
        self.success = False

        if acquisition_name is None:
            try:
                script_path = sys.argv[0]
                path = PurePath(script_path)
                self._acquisition_name = path.stem

            except IndexError:
                self._acquisition_name = "Untitled"
        else:
            self._acquisition_name = acquisition_name

        self._acquisition_name = self._acquisition_name.upper()
        self._barcodes = barcodes if barcodes else []

    def new_acquisition(self, token):
        """This method fetches the acquisition ID from FortLogs and caches it during
        the script session.


        """
        try:
            acquisition_request = midas.fortlogs_client.create_acquisition(
                self._acquisition_name,
                self._barcodes,
                token
            )
        except Exception as exc:
            raise midas.FortLogsPushFailed(f"{exc}")

        self._acquisition_id = acquisition_request["acquisition_id"]

    async def register_buffers(self, channel):
        """This function should register the buffer into aggregator object

        """
        streams = list()

        for s in self._publishers:
            # Wait for acquisition to start
            s.ready.wait()
            stream_obj = Stream(
                name=s.buffer_name,
                device=s.device,
                time_origin=datetime.datetime.utcfromtimestamp(s.start_time),
                signal_names=s.signals,
                step_signals=s.step_signals,
                ignore=s.ignore
            )
            if s.period is not None:
                stream_obj.period = s.period
            streams.append(stream_obj)

        registration_msg = StreamRegister(
            streams=streams,
            acquisition_id=self._acquisition_id
        )

        # Send negotiation message
        await channel.send(registration_msg.json())

        # return remote id
        remote_ids_raw = await channel.recv()
        remote_ids = StreamsSubmitted.parse_raw(remote_ids_raw)

        for i, s in enumerate(self._publishers):
            s.remote_id = remote_ids.stream_ids[i]

        self._frame_builder = FrameBuilder(self._publishers, channel)
        return remote_ids.stream_ids

    def listen(self, buffer: midas.Buffer):
        """

        """
        buffer.observer = self
        self._publishers.append(buffer)

    def publish(self, l_id: int, values):
        self._aggregator_queue.put((l_id, values))

    async def push_loop(self):
        result: bool = True
        channel = None
        header = []
        access_token = ""

        try:
            # If SSO is activated send access token
            if not midas.constants.Config.STANDALONE:
                access_token = midas.fortlogs_client.fetch_rbac_token()
            header.append(("Authorization", f"Bearer {access_token}"))
        except Exception as exc:
            logger.warning("Authentication server unavailable!")
            self._event_loop.call_soon_threadsafe(self._event_loop.stop)
            return exc

        try:
            self.new_acquisition(access_token)
        except Exception as exc:
            logger.warning("Could not create acquisition!")
            logger.warning(exc)
            self._event_loop.call_soon_threadsafe(self._event_loop.stop)
            return exc

        # Retry loop
        for i in range(self.CONNECT_ATTEMPTS):
            try:
                ssl_context = ssl.SSLContext()
                ssl_context.check_hostname = False
                ssl_context.verify_mode = ssl.CERT_NONE
                channel = await websockets.connect(
                    midas.constants.Config.STREAMING_ENDPOINT,
                    extra_headers=header,
                    ssl=ssl_context,
                )
                break
            except (ConnectionRefusedError, OSError, websockets.exceptions.InvalidStatusCode) as exc:
                if i < self.CONNECT_ATTEMPTS - 1:
                    logger.warning("Connection refused... Trying again!")
                    logger.warning(exc.args)
                    await asyncio.sleep(self.CONNECT_TIMEOUT)
                else:
                    logger.error("Remote server Unavailable... Try again at a later time!")
                    self._event_loop.call_soon_threadsafe(self._event_loop.stop)
                    return exc

        # Register buffers to get their ID's
        try:
            remote_ids = await self.register_buffers(channel)
            # open browser with streams open
            if self._open_on_powerspy:
                open_on_powerspy(remote_ids)

            last_update = datetime.datetime.now()

            while not self._frame_builder.is_finished:
                current_time = datetime.datetime.now()
                try:
                    stream_id, values = self._aggregator_queue.get(timeout=0.001)
                    if isinstance(values, Stop):
                        self._frame_builder.schedule_removal(stream_id)
                        # FIXME stop each stream early instead of stoping them at the same time
                    else:
                        self._frame_builder.add_samples(stream_id, values)
                except queue.Empty:
                    pass

                await asyncio.sleep(self.EVENT_LOOP_WAIT)

                if current_time - last_update > self.ACCUMULATE_TIMEOUT:
                    if self._frame_builder.has_data:
                        await self._frame_builder.dispatch_frames()
                        self._frame_builder.reset_frames()
                    if self._frame_builder._to_be_removed:
                        self._frame_builder.execute_scheduled_removal()
                    last_update = current_time

            await channel.send(self.STOP_STREAM)
            self.success = True

        except Exception as exc:
            logger.error(exc)
            result = exc
        finally:
            await channel.close()

        self._event_loop.call_soon_threadsafe(self._event_loop.stop)
        return result

    def run(self):
        """Send prepared frame to FortLogs server.

        """
        # Each aggregator thread should have its own event loop
        future = asyncio.run_coroutine_threadsafe(self.push_loop(), self._event_loop)
        self._event_loop.run_forever()

        if isinstance(future.result(), (ConnectionRefusedError, midas.FortLogsPushFailed, ConnectionClosed, OSError)):
            # This should make all the buffers ready if the server is down
            logger.info("The Remote server is not online, will start acquisition anyway.")


class Job(threading.Thread):
    job_id = 0

    def __init__(self, open_on_powerspy, acquisition_name, barcodes, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.job_id += 1
        self.observer = Aggregator(open_on_powerspy, acquisition_name, barcodes)
        self.job_time = datetime.datetime.now()

        self._buffer_list = list()
        self._buffer_names = list()
        self._local_exception = None

        # Can have access to all the errors thrown by buffers.
        self.errors = list()

    def add_buffer(self, buffer):
        if buffer.buffer_name in self._buffer_names:
            raise midas.InvalidBufferName(f"Buffer already exists with name {buffer.buffer_name}")
        if not buffer._callback_function:
            raise midas.UnregisteredCallback(f"Buffer {buffer.buffer_name} has no registered callback.")
        self._buffer_names.append(buffer.buffer_name)
        self._buffer_list.append(buffer)
        self.observer.listen(buffer)

    def resolve_all_upstreams(self):
        for b in self._buffer_list:
            if isinstance(b, (midas.SubscribedBuffer,)):
                b.resolve_subscription(self._buffer_list)

    def join(self):
        """Override join to propagate exceptions to the main thread.

        """
        super().join()
        if self._local_exception:
            raise self._local_exception

    def run(self):
        try:
            self.observer.start()
            for b in self._buffer_list:
                b.start()

            self.observer.join()
            for b in self._buffer_list:
                b.join()
        except Exception as e:
            self._local_exception = e

        if not self.observer.success:
            current_path = Path(".") / f"failed_logs_{self.job_time.strftime('%Y-%m-%d_%H-%M')}"
            current_path.mkdir(exist_ok=True)
            for b in self._buffer_list:
                new_path = current_path / f"{b.buffer_name}"
                b.to_json(str(new_path))


def start(*buffers, acquisition_name: str = None, wait_to_complete: bool = False, 
        open_powerspy=True, barcodes: List[str] = None, **kwargs
    ):
    """Start all the declared unstarted acquisitions.

    :param acquisition_name: explicitly give a name to an acquisition instead of using script name
    :param wait_to_complete: Wait for the job to complete. Useful syntax for not having to check if
        the buffer is ready, during tests.
    :param barcodes: add list of barcode strings with exactly 19 characters each. Example: `HCRMIC____-TT000342`
    :param open_powerspy: flag to enable/disable the opening of a new browser tab with the current streams.
    """
    j = Job(open_powerspy, acquisition_name, barcodes)
    for b in buffers:
        j.add_buffer(b)
    j.resolve_all_upstreams()
    j.start()

    if wait_to_complete:
        j.join()

    return j

# EOF
