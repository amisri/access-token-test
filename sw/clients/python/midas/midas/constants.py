from midas.__version__ import __version__ 


ENDPOINT_MAP = {
    "analog": "analog-logs",
    "table": "table-logs",
    "digital": "digital-logs"
}


class Profile:

    def __init__(self, fortlogs_host: str, mount_point: str = '/fortlogs', powerspy_host: str = None):
        self.fortlogs_host = fortlogs_host
        self.mount_point = mount_point
        self.powerspy_host = fortlogs_host if powerspy_host is None else powerspy_host


# Add further profiles here
PROFILES = {
    'preprod': Profile('ccs-webtools-preprod',),
    'dev': Profile('nulauren-dev', powerspy_host='vsambor-dev'),
    'prod': Profile('accwww',),
    'local': Profile('localhost'),
}


SSO_PROFILES = {
    'qa': ('keycloak-qa', '39123264-a46c-4c4f-a242-91d46585c103'),
    'prod': ('auth', '7fc7ef48-cda1-4a39-b5ce-4ac22b24354d'),
}


def set_endpoint_env(profile='prod'):
    if profile == 'local':
        Config.API_URL = f"https://{PROFILES[profile].fortlogs_host}{PROFILES[profile].mount_point}"
        Config.STREAMING_ENDPOINT = f"wss://{PROFILES[profile].fortlogs_host}{PROFILES[profile].mount_point}/publish"
        Config.POWERSPY_URL = f"https://{PROFILES[profile].powerspy_host}/powerspy/"
    else:
        Config.API_URL = f"https://{PROFILES[profile].fortlogs_host}.cern.ch{PROFILES[profile].mount_point}"
        Config.STREAMING_ENDPOINT = f"wss://{PROFILES[profile].fortlogs_host}.cern.ch{PROFILES[profile].mount_point}/publish"
        Config.POWERSPY_URL = f"https://{PROFILES[profile].powerspy_host}.cern.ch/powerspy/"


def set_standalone():
    # Disable OAuth2
    Config.STANDALONE = True


class Config:
    STANDALONE: str = False

    API_URL: str = None
    STREAMING_ENDPOINT: str = None
    POWERSPY_URL: str = None
    FORTLOGS_CLIENT_ID: str = "fort-logs"

    @staticmethod
    def show_settings():
        return str(vars(Config))


if "dev" in __version__:
    set_endpoint_env(profile='dev')
else:
    set_endpoint_env()


class Colors:
    RED = '\033[91m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    END = '\033[0m'

# EOF
