import collections
import itertools
import queue
import threading
import time

import pandas as pd

import midas
import midas.stats

from fortlogs_schemas.streams import Stop
from fortlogs_schemas import AnalogLog
from fortlogs_schemas.signals import AnalogSignal
from typing import Tuple


TIMEOUT_RETRIES = 3
TIMEOUT = 3


class BaseBuffer(threading.Thread):
    id_lock = threading.Lock()
    id_tracker = 0

    def __init__(self, buffer_name: str, signal_names: Tuple[str], ignore: bool = False,
            device: str = None, step_signals: Tuple[str] = None, **kwargs
        ):
        super().__init__(**kwargs)

        with Buffer.id_lock:
            self.id = Buffer.id_tracker
            Buffer.id_tracker += 1

        self.remote_id: int

        # Configurable parameters
        self._callback_function = None
        self._callback_args = list()
        self._callback_kwargs = dict()

        self._start_time = None
        self.period = None

        self._stop_function = None
        self._stop_quantity = None

        self._length = 0

        self._time_origin = 0
        self._timestamp_override = False
        self._first_sample_timestamp = None
        self._subscribers = set()

        self.ignore = ignore

        self.observer = None
        self.ready: threading.Event = threading.Event()

        self._validate_and_set_names(device, buffer_name, signal_names, step_signals)

        # Field used to store acquisition data
        self._header = list(itertools.chain('t', signal_names))
        self._data = collections.OrderedDict()
        for column in self._header:
            self._data[column] = list()

        # Response class for the callback response
        self.Response = collections.namedtuple('Response', self._header)

    def _validate_and_set_names(self, device, buffer_name, signals, step_signals):
        if not isinstance(signals, (tuple, list)):
            raise midas.InvalidSignalName("Signals should be defined in a tuple")
        for signal in signals:
            if not isinstance(signal, str):
                raise midas.InvalidSignalName("Name must be a string")

            if signal == 't':
                raise midas.InvalidSignalName("Name must not be equal to 't'")

            if len(signal) > 20 or len(signal) < 2:
                raise midas.InvalidSignalName("Name should have between 3 to 20 characters.")

        if buffer_name and not isinstance(buffer_name, str):
            raise midas.InvalidBufferName("Name must be a string")

        if buffer_name and (len(buffer_name) > 25 or len(buffer_name) < 4):
            raise midas.InvalidBufferName("Buffer name should have between 3 to 25 characters.")

        if device and not isinstance(device, str):
            raise midas.InvalidDeviceName("Device must be a string")

        if device and (len(device) > 25 or len(device) < 5):
            raise midas.InvalidDeviceName("Device should have between 5 to 25 characters.")

        # check if all stepped signals are included in the original signal list
        if step_signals and not all(signal_name in signals for signal_name in step_signals):
            raise midas.InvalidSignalName("Stepped signal not included in the given signal names")

        self.signals = signals
        self.step_signals = step_signals
        self.device = device.upper() if device else 'NO_DEVICE'
        self.buffer_name = buffer_name.upper()

    def _build_schema(self):
        signal_list = list()
        for s_name, samples in self._data.items():
            if s_name != 't':
                analog_signal = AnalogSignal(name=s_name, samples=samples)
                if self.step_signals and s_name in self.step_signals:
                    analog_signal.step = True
                signal_list.append(analog_signal)

        return AnalogLog(
            name=self.buffer_name,
            device=self.device,
            timestamps=self._data['t'],
            source="MIDAS",
            version="2.0",
            period=0,
            timeOrigin=self.start_time,
            firstSampleTime=self.start_time,
            signals=signal_list,
        )

    @property
    def start_time(self):
        if self._start_time:
            return self._start_time
        else:
            raise Exception("Acquisition not started yet.")

    @property
    def finish_time(self):
        # FIXME finish time is not giving the correct field yet.
        if len(self):
            return self._data["t"][-1]
        else:
            raise Exception("Acquisition not finished yet.")

    @property
    def duration(self):
        if len(self):
            return self.finish_time - self.start_time
        else:
            return 0

    @property
    def data(self):
        """Get buffer content as a pandas dataframe.

        """
        # FIXME: this should be using the format from fortlogs_schemas instead
        return pd.DataFrame(self._data, columns=self._header)

    def stop(self):
        """Stop the current acquisition and sends a stop message to all
        subscribed buffers.
        """
        terminator = Stop()
        self._publish(terminator)

    def _publish(self, new_data):
        """Publish data to all buffers that are subscribed to this buffer.
        It also publishes data to observer node.

        """
        for sub in self._subscribers:
            sub._receive_queue.put(new_data)
        self.observer.publish(self.id, new_data)

    def join(self):
        super().join()
        if self.exc:
            raise self.exc

    def _validate_and_send(self, t, new_data):
        if not isinstance(new_data, (list, tuple)):
            raise midas.InvalidBufferReturnType("Received data must be of type list")

        if len(new_data) != len(self.signals):
            raise midas.WrongSampleCount(f"Data {new_data} has wrong number of samples")

        to_send = (t, *new_data)
        self._append_values(to_send)

    def _append_values(self, new_data):
        # Add new data to buffer
        for i, s in enumerate(self._header):
            self._data[s].append(new_data[i])
        self._length += 1

        to_publish = self.Response(*new_data)
        self._publish(to_publish)

    def _engine(self):
        raise NotImplementedError("Engine not defined for this class")

    def run(self):
        try:
            self.exc = None
            self._engine()
        except Exception as e:
            self.exc = e
            self.stop()
            return

    def _check_samples(self):
        return self._length == self._stop_quantity

    def _check_duration(self):
        return self._current_time - self._time_origin > self._stop_quantity

    def _check_duration_with_timestamps(self):
        if self._first_sample_timestamp is None:
            return False
        else:
            return self._current_time - self._first_sample_timestamp > self._stop_quantity

    def _common_acquire(self, callback_function, args=None, kwargs=None, start_time: float = None):
        if self._callback_function:
            raise midas.AcquireTwiceError('Acquisition function was already set.')

        self._callback_function = callback_function
        
        if args:
            if isinstance(args, (list, tuple)):
                self._callback_args.extend(args)
            else:
                raise midas.InvalidCallbackArg(f"Expected iterable and got {type(kwargs)}")
        
        if kwargs:
            if isinstance(kwargs, (collections.abc.Mapping,)):
                self._callback_kwargs = kwargs
            else:
                raise midas.InvalidCallbackArg(f"Expected dictionary and got {type(kwargs)}")
        
        self._start_time = start_time

    def acquire_by_duration(self, duration: float, callback_function, args=None, kwargs=None, start_time: float = None):
        """This method sets the acquisition configuration. It does not start the acquisition.

        :param duration: duration of the test
        :param callback_function: registered function that will optionally modify incoming data and publish new data
        :param args: arguments to give to the callback function
        :param kwargs: named arguments to give to the callback function
        :param start_time: override the starting time of this acquisition with a new timestamp float
        :return:
        """
        self._common_acquire(callback_function, args=args, kwargs=kwargs, start_time=start_time)
        if self._timestamp_override:
            self._stop_function = self._check_duration_with_timestamps
        else:
            self._stop_function = self._check_duration
        self._stop_quantity = duration

    def acquire_by_samples(self, samples: int, callback_function, args=None, kwargs=None, start_time: float = None):
        """This method sets the acquisition configuration. It does not start the acquisition.

        :param samples: number of samples to be acquired
        :param callback_function: registered function that will optionally modify incoming data and publish new data
        :param args: arguments to give to the callback function
        :param kwargs: named arguments to give to the callback function
        :param start_time: override the starting time of this acquisition with a new timestamp float
        :return:
        """
        self._common_acquire(callback_function, args=args, kwargs=kwargs, start_time=start_time)
        self._stop_function = self._check_samples
        self._stop_quantity = samples

    def to_csv(self, filename=None):
        s = self._build_schema()
        s.to_csv(filename=filename)

    def to_json(self, filename=None):
        s = self._build_schema()
        s.to_json(filename=filename)

    def __len__(self):
        return self._length

    def add_max(self, window: int = None, hold: bool = False, window_type: str = 'samples', **kwargs):
        """Create a moving maximum buffer.

        :param window: number of samples within this buffer window, otherwise uses the full window width
        :param ignore: if set to true, it will not save this data to fortlogs
        :param hold: only display values in the boundary of each window
        :param window_type: If window should be based on number of 'samples' or 'duration'
        :return: buffer object
        """
        return midas.stats.max_buffer_util(self, window=window, hold=hold, window_type=window_type, **kwargs)

    def add_min(self, window: int = None, hold: bool = False, window_type: str = 'samples', **kwargs):
        """Create a moving minimum buffer.

        :param window: number of samples within this buffer window, otherwise uses the full window width
        :param ignore: if set to true, it will not save this data to fortlogs
        :param hold: only display values in the boundary of each window
        :param window_type: If window should be based on number of 'samples' or 'duration'
        :return: buffer object
        """
        return midas.stats.min_buffer_util(self, window=window, hold=hold, window_type=window_type, **kwargs)

    def add_p2p(self, window: int = None, hold: bool = False, window_type: str = 'samples', **kwargs):
        """Create a moving peak to peak buffer.

        :param window: number of samples within this buffer window, otherwise uses the full window width
        :param ignore: if set to true, it will not save this data to fortlogs
        :param hold: only display values in the boundary of each window
        :param window_type: If window should be based on number of 'samples' or 'duration'
        :return: buffer object
        """
        return midas.stats.p2p_buffer_util(self, window=window, hold=hold, window_type=window_type, **kwargs)

    def add_mean(self, window: int = None, hold: bool = False, window_type: str = 'samples', **kwargs):
        """Create a moving average buffer.

        :param window: number of samples within this buffer window, otherwise uses the full window width
        :param ignore: if set to true, it will not save this data to fortlogs
        :param hold: only display values in the boundary of each window
        :param window_type: If window should be based on number of 'samples' or 'duration'
        :return: buffer object
        """
        return midas.stats.mean_buffer_util(self, window=window, hold=hold, window_type=window_type, **kwargs)

    def add_std(self, window: int = None, hold: bool = False, window_type: str = 'samples', **kwargs):
        """Create a moving standard deviation buffer.

        :param window: number of samples within this buffer window, otherwise uses the full window
            width
        :param ignore: if set to true, it will not save this data to fortlogs
        :param hold: only display values in the boundary of each window
        :param window_type: If window should be based on number of 'samples' or 'duration'
        :return: buffer object
        """
        return midas.stats.std_buffer_util(self, window=window, hold=hold, window_type=window_type, **kwargs)


class PeriodicBuffer(BaseBuffer):

    def __init__(self, buffer_name: str, signal_names: Tuple[str], period: float, **kwargs):
        """Aggregates a list of signals in the buffer structure from PowerSpy.

        :param buffer_name: the log name that will be seen on PowerSpy
        :param signal_names: list of signal names
        :param period: sampling period of signals within the buffer
        :param step_signals: list of signals which are step signals
        :param ignore: if set to true, it will not save this data to fortlogs
        :param device: which device do associate with this buffer
        :return:
        """
        super().__init__(buffer_name, signal_names, **kwargs)
        self._step = 0
        self.period = period

    def _build_schema(self):
        s = super()._build_schema()
        s.timestamps = None
        s.period = self.period
        return s

    def _engine(self):
        self._time_origin = time.time()
        if not self._start_time:
            self._start_time = self._time_origin

        # the buffer has a starting time
        self.ready.set()

        while True:
            self._current_time = self._start_time + self._step * self.period
            self._step += 1

            if self._stop_function():
                self.stop()
                break

            new_data = self._callback_function(*self._callback_args, **self._callback_kwargs)

            # Check if the stop condition was returned by the function
            if isinstance(new_data, Stop):
                self.stop()
                break

            self._validate_and_send(self._current_time, new_data)

    def __repr__(self):
        return f"PeriodicBuffer(buffer_name={self.buffer_name}, signals=({', '.join(self.signals)}), device={self.device}, period={self.period})"


class SubscribedBuffer(BaseBuffer):

    def __init__(self, buffer_name: str, signal_names: Tuple[str], upstream_buffer: str, **kwargs):
        """Aggregates a list of signals in the buffer structure from PowerSpy.

        :param buffer_name: the log name that will be seen on PowerSpy
        :param signal_names: list of signal names
        :param upstream_buffer: name of target parent buffer
        :param step_signals: list of signals which are step signals
        :param ignore: if set to true, it will not save this data to fortlogs
        :param device: which device do associate with this buffer
        :return:
        """
        self._receive_queue = queue.Queue()
        self._upstream_name = upstream_buffer.upper() if upstream_buffer else None
        super().__init__(buffer_name, signal_names, **kwargs)
        self._callback_args = [None, ]

    def stop(self):
        super().stop()
        self._receive_queue.task_done()

    def resolve_subscription(self, buffer_list):
        """Subscribe to another another buffer stream.

        :param buffer_name: name of the target buffer to subscribe to.
        """
        for b in buffer_list:
            if b.buffer_name == self._upstream_name:
                upstream_buffer = b
                break
        if upstream_buffer is None:
            raise Exception(f"No upstream buffer named {self._upstream_name}")
        upstream_buffer._subscribers.add(self)

    def _fetch_upstream(self):
        for i in range(TIMEOUT_RETRIES):
            try:
                return self._receive_queue.get(timeout=TIMEOUT)
            except queue.Empty:
                pass

        raise midas.EmptyReceiveQueue('No sample received from upstream buffer.')

    def _never_stop(self):
        return False

    def acquire(self, callback_function, args=None, kwargs=None, start_time: float = None):
        """This method sets the acquisition configuration. It does not start the acquisition.

        :param callback_function: registered function that will optionally modify incoming data and publish new data
        :param args: arguments to give to the callback function
        :param kwargs: named arguments to give to the callback function
        :param start_time: override the starting time of this acquisition with a new timestamp float
        :return:
        """
        self._common_acquire(callback_function, args=args, kwargs=None, start_time=start_time)
        self._stop_function = self._never_stop

    def _engine(self):
        item = self._fetch_upstream()
        self._time_origin = item.t
        if not self._start_time:
            self._start_time = self._time_origin

        # the buffer has a starting time
        self.ready.set()

        while True:
            # Stop acquisition if the item received is a stop object
            if isinstance(item, Stop):
                self.stop()
                break

            self._current_time = item.t

            if self._stop_function():
                self.stop()
                break

            self._callback_args[0] = item
            new_data = self._callback_function(*self._callback_args, **self._callback_kwargs)

            # Check if the stop condition was returned by the function
            if isinstance(new_data, Stop):
                self.stop()
                break
            elif new_data is None:
                pass
            else:
                self._validate_and_send(self._current_time, new_data)

            self._receive_queue.task_done()
            # Try to fetch item from upstream
            item = self._fetch_upstream()

    def __repr__(self):
        return f"SubscribedBuffer(buffer_name={self.buffer_name}, signals=({', '.join(self.signals)}), device={self.device}, upstream={self.upstream_buffer})"


class Buffer(BaseBuffer):

    def __init__(self, buffer_name: str, signal_names: Tuple[str], timestamp_override: bool = False, **kwargs):
        """Aggregates a list of signals in the buffer structure from PowerSpy.

        :param buffer_name: the log name that will be seen on PowerSpy
        :param signal_names: list of signal names
        :param timestamp_override: if set to true, the timestamp used will be given by the callback function
        :param step_signals: list of signals which are step signals
        :param ignore: if set to true, it will not save this data to fortlogs
        :param device: which device do associate with this buffer
        :return:
        """
        super().__init__(buffer_name, signal_names, **kwargs)
        self._timestamp_override = timestamp_override

    def _engine_with_timestamp(self):
        self._current_time = float('-inf')
        # problem with sample number
        while True:
            if self._stop_function():
                self.stop()
                break

            received_data = self._callback_function(*self._callback_args, **self._callback_kwargs)

            # Check if the stop condition was returned by the function
            if isinstance(received_data, Stop):
                self.stop()
                break

            self._current_time = received_data[0]
            if self._first_sample_timestamp is None:
                self._first_sample_timestamp = self._current_time
            new_data = received_data[1:]
            self._validate_and_send(self._current_time, new_data)

    def _default_engine(self):
        while True:
            self._current_time = time.time()

            if self._stop_function():
                self.stop()
                break

            new_data = self._callback_function(*self._callback_args, **self._callback_kwargs)

            # Check if the stop condition was returned by the function
            if isinstance(new_data, Stop):
                self.stop()
                break

            self._validate_and_send(self._current_time, new_data)

    def _engine(self):
        self._time_origin = time.time()
        if not self._start_time:
            self._start_time = self._time_origin

        # the buffer has a starting time
        self.ready.set()

        if self._timestamp_override:
            self._engine_with_timestamp()
        else:
            self._default_engine()

    def __repr__(self):
        return f"Buffer(buffer_name={self.buffer_name}, signals=({', '.join(self.signals)}), device={self.device})"


# EOF
