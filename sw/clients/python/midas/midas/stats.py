import collections
import math
import midas
import statistics


def make_memory(buffer, stat_list, window):
    callback_memory = {
        'nr_signals': len(buffer.signals),
        'step': 0,
    }
    if window:
        callback_memory['last_update'] = -1
        callback_memory['current_time'] = None

        if 'mean' in stat_list:
            callback_memory['values'] = list()

        if 'min' in stat_list:
            callback_memory['dq_min'] = [collections.deque() for s in buffer.signals]

        if 'max' in stat_list:
            callback_memory['dq_max'] = [collections.deque() for s in buffer.signals]
    else:
        if 'mean' in stat_list:
            callback_memory['mean'] = [0 for s in buffer.signals]

        if 'max' in stat_list:
            callback_memory['max'] = [-math.inf for s in buffer.signals]

        if 'min' in stat_list:
            callback_memory['min'] = [math.inf for s in buffer.signals]
    return callback_memory


def should_output_factory(memory, hold, window, window_type):
    if window_type == 'samples':
        if hold:
            def should_output():
                return memory['step'] % window == 0
        else:
            def should_output():
                return memory['step'] >= window
    elif window_type == 'duration':
        if hold:
            def should_output():
                if memory['current_time'] - memory['last_update'] >= window:
                    memory['last_update'] = memory['current_time']
                    return True
                else:
                    return False
        else:
            def should_output():
                return memory['current_time'] - memory['last_update'] >= window

    return should_output


def max_min_callback_factory(buffer, stat_list: tuple, window_type: str, window: float, hold: bool):
    callback_memory = make_memory(buffer, stat_list, window)
    should_output = should_output_factory(callback_memory, hold, window, window_type)

    if window:
        if 'max' in stat_list:
            def get_stored(i):
                return callback_memory['dq_max'][i]

            def should_remove(stored, current):
                return stored and stored[-1][1] < current
        
        if 'min' in stat_list:
            def get_stored(i):
                return callback_memory['dq_min'][i]

            def should_remove(stored, current):
                return stored and stored[-1][1] > current

        if window_type == 'samples':
            def is_outside_window(point):
                return point and point[0][0] == callback_memory['step'] - window
        
        elif window_type == 'duration':
            def is_outside_window(point):
                return point and callback_memory['current_time'] - point[0][2] > window

        def default_callback(received_data):
            new_data = []
            callback_memory['current_time'] = received_data[0]
            if callback_memory['step'] == 0:
                callback_memory['last_update'] = callback_memory['current_time']

            for i in range(callback_memory['nr_signals']):
                sample_val = received_data[i+1]

                stored = get_stored(i)

                # check if the top element in the deque has moved out of the window.
                while is_outside_window(stored):
                    stored.popleft()

                # check if the new element is greater than the elements in the deque. If so, pop elements in the deque.
                while should_remove(stored, sample_val):
                    stored.pop()

                # add the current index to the deque
                stored.append((callback_memory['step'], sample_val, callback_memory['current_time']))

                # add the current window maximum to the result.
                new_data.append(stored[0][1])

            callback_memory['step'] += 1

            if should_output():
                return new_data

    # No window              
    else:
        if 'max' in stat_list:
            def find_best(old, new):
                return max(old, new)

            def get_memory():
                return callback_memory['max']
        
        if 'min' in stat_list:
            callback_memory['min'] = [math.inf for s in buffer.signals]

            def find_best(old, new):
                return min(old, new)

            def get_memory():
                return callback_memory['min']

        def default_callback(received_data):
            new_data = list()
            for i, old in enumerate(get_memory()):
                new_data.append(find_best(old, received_data[i+1]))
                get_memory()[i] = new_data[i]
            return new_data
    
    return default_callback


def max_buffer_util(parent_buffer, window: float = None, hold: bool = False, window_type: str = 'samples', **kwargs):
    max_buffer = midas.SubscribedBuffer(
        f"{parent_buffer.buffer_name}_max",
        [f"{signal}_max" for signal in parent_buffer.signals],
        parent_buffer.buffer_name,
        device=parent_buffer.device,
        **kwargs
    )

    callback = max_min_callback_factory(max_buffer, ('max',), window_type, window, hold)
    max_buffer.acquire(callback)
    return max_buffer


def min_buffer_util(parent_buffer, window: float = None, hold: bool = False, window_type: str = 'samples', **kwargs):
    min_buffer = midas.SubscribedBuffer(
        f"{parent_buffer.buffer_name}_min",
        [f"{signal}_min" for signal in parent_buffer.signals],
        parent_buffer.buffer_name,
        device=parent_buffer.device,
        **kwargs
    )

    callback = max_min_callback_factory(min_buffer, ('min',), window_type, window, hold)
    min_buffer.acquire(callback)
    return min_buffer


def p2p_buffer_util(parent_buffer, window: float = None, hold: bool = False, window_type: str = 'samples', **kwargs):
    p2p_buffer = midas.SubscribedBuffer(
        f"{parent_buffer.buffer_name}_p2p",
        [f"{signal}_p2p" for signal in parent_buffer.signals],
        parent_buffer.buffer_name,
        device=parent_buffer.device,
        **kwargs
    )

    callback_memory = make_memory(p2p_buffer, ('max', 'min'), window)
    should_output = should_output_factory(callback_memory, hold, window, window_type)

    if window_type == 'samples':
        def is_outside_window(point):
            return point and point[0][0] == callback_memory['step'] - window
    
    elif window_type == 'duration':
        def is_outside_window(point):
            return point and callback_memory['current_time'] - point[0][2] > window

    def p2p_window_callback(received_data):
        new_data = []
        callback_memory['current_time'] = received_data[0]
        if callback_memory['step'] == 0:
            callback_memory['last_update'] = callback_memory['current_time']
        
        for i in range(callback_memory['nr_signals']):
            sample_val = received_data[i+1]
            s_max = callback_memory['dq_max'][i]
            s_min = callback_memory['dq_min'][i]

            # check if the top element in the deque has moved out of the window.
            while is_outside_window(s_max):
                s_max.popleft()
            while is_outside_window(s_min):
                s_min.popleft()

            # check if the new element is lower than the elements in the deque. If so, pop elements in the deque.
            while s_max and s_max[-1][1] < sample_val:
                s_max.pop()
            while s_min and s_min[-1][1] > sample_val:
                s_min.pop()

            # add the current index to the deque
            s_max.append((callback_memory['step'], sample_val, callback_memory['current_time']))
            s_min.append((callback_memory['step'], sample_val, callback_memory['current_time']))

            # add the current window maximum to the result.
            new_data.append(s_max[0][1] - s_min[0][1])

        callback_memory['step'] += 1

        if should_output():
            return new_data

    def p2p_callback(received_data):
        new_data = list()
        for i, old_max in enumerate(callback_memory['max']):
            new_data.append(max(old_max, received_data[i+1]))
            callback_memory['max'][i] = new_data[i]
        for i, old_min in enumerate(callback_memory['min']):
            callback_memory['min'][i] = min(old_min, received_data[i+1])
            new_data[i] -= callback_memory['min'][i]
        return new_data

    p2p_buffer.acquire(p2p_window_callback) if window else p2p_buffer.acquire(p2p_callback)
    return p2p_buffer


def mean_buffer_util(parent_buffer, window: float = None, hold: bool = False, window_type: str = 'samples', **kwargs):
    mean_buffer = midas.SubscribedBuffer(
        f"{parent_buffer.buffer_name}_mean",
        [f"{signal}_mean" for signal in parent_buffer.signals],
        parent_buffer.buffer_name,
        device=parent_buffer.device,
        **kwargs
    )
    callback_memory = make_memory(mean_buffer, ('mean',), window)
    should_output = should_output_factory(callback_memory, hold, window, window_type)

    if window_type == 'samples':
        def is_outside_window(point):
            return point and point[0][0] == callback_memory['step'] - window

    elif window_type == 'duration':
        def is_outside_window(point):
            return point and callback_memory['current_time'] - point[0][2] > window

    def mean_window_callback(received_data):
        callback_memory['current_time'] = received_data[0]
        if callback_memory['step'] == 0:
            callback_memory['last_update'] = callback_memory['current_time']

        callback_memory['values'].append((
            callback_memory['step'],
            received_data[1:],
            callback_memory['current_time']
        ))

        while is_outside_window(callback_memory['values']):
            callback_memory['values'].pop(0)

        average = list()
        for signal in range(callback_memory['nr_signals']):
            average.append(statistics.mean([sample[1][signal] for sample in callback_memory['values']]))
        
        callback_memory['step'] += 1
        if should_output():
            return average

    def mean_callback(received_data):
        callback_memory['step'] += 1
        for i in range(callback_memory['nr_signals']):
            new_val = received_data[i+1] / callback_memory['step']
            callback_memory['mean'][i] = callback_memory['mean'][i] - callback_memory['mean'][i] / callback_memory['step'] + new_val
        return callback_memory['mean']

    mean_buffer.acquire(mean_window_callback) if window else mean_buffer.acquire(mean_callback)
    return mean_buffer


def std_buffer_util(parent_buffer, window: float = None, hold: bool = False, window_type: str = 'samples', **kwargs):
    std_buffer = midas.SubscribedBuffer(
        f"{parent_buffer.buffer_name}_std",
        [f"{signal}_std" for signal in parent_buffer.signals],
        parent_buffer.buffer_name,
        device=parent_buffer.device,
        **kwargs
    )

    callback_memory = {
        'average': [0 for s in std_buffer.signals],
        'step': 1
    }

    if window:
        callback_memory['variance'] = [0 for s in std_buffer.signals]
        callback_memory['samples'] = [list() for s in std_buffer.signals]
    else:
        callback_memory['m2'] = [0 for s in std_buffer.signals]

    def std_window_callback(received_data, memory, window_size: int, hold: bool):
        new_data = []
        for i, s in enumerate(memory["samples"]):
            s.append(received_data[i+1])
            if memory['step'] > window_size:
                old_average = memory['average'][i]
                old_sample = s.pop(0)
                memory['average'][i] = old_average + (s[-1] - old_sample) / window_size
                memory['variance'][i] += (s[-1] - old_sample) \
                    * (s[-1] + old_sample - (memory['average'][i] + old_average)) / (window_size - 1)
            elif memory['step'] == window_size:
                memory['variance'][i] = statistics.variance(s)
                memory['average'][i] = statistics.mean(s)
            new_data.append(math.sqrt(memory['variance'][i]))
        memory['step'] += 1

        if hold:
            if memory['step'] % (window_size + 1) == 0:
                return new_data
        else:
            # Only return data when the window has enough samples already
            if memory['step'] >= window_size + 1:
                return new_data

    def std_callback(received_data, memory):
        new_data = []
        for i, _ in enumerate(memory['average']):
            delta = received_data[i+1] - memory['average'][i]
            memory['average'][i] += delta / memory['step']
            delta2 = received_data[i+1] - memory['average'][i]
            memory['m2'][i] += delta * delta2
            if memory['step'] < 2:
                std_dev = 0
            else:
                std_dev = (memory['m2'][i] / (memory['step'] - 1)) ** .5
            new_data.append(std_dev)
        memory['step'] += 1
        return new_data

    if window:
        std_buffer.acquire(std_window_callback, args=(callback_memory, window, hold))
    else:
        std_buffer.acquire(std_callback, args=(callback_memory,))
    return std_buffer
