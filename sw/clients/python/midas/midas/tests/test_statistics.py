import midas
import statistics
import pytest
import midas.tests as tests
import random

midas.set_endpoint_env('dev')

@pytest.fixture
def setup():
    tests.reset()
    yield


def test_stats_range(setup):
    b = midas.Buffer("stats_range", ("m1", "m2", "m3"))
    b.acquire_by_samples(300, tests.sample_gen, args=(3,))
    m = b.add_mean()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    assert len(b.signals) == len(m.signals) == 3


def test_single_signal_mean(setup):
    b = midas.Buffer("signal4mean", ('m1',))
    b.acquire_by_samples(300, tests.sample_gen, args=(1,))
    m = b.add_mean()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    assert m.data['m1_mean'].iloc[-1] == pytest.approx(statistics.mean(tests.x[0][:len(m)]))


# def test_single_signal_mean_duration(setup):
#     class Counter:
#         i = 0

#     def generate_samples(nr_signals):
#         result = Counter.i
#         Counter.i += 1
#         return [result for _ in range(nr_signals + 1)]
    
#     b = midas.Buffer("signal4mean", ('m1',), timestamp_override=True)
#     b.acquire_by_samples(101, generate_samples, args=(1,))
#     m = b.add_mean(window=10, window_type='duration', hold=True)
#     midas.start(b, m, wait_to_complete=True, open_powerspy=False)
#     assert m.data['m1_mean'].iloc[-1] == pytest.approx(statistics.mean(tests.x[0][:len(m)]))


def test_1_signal_mean_window():
    FACTOR = 1000
    SAMPLES = 500
    example_input = [FACTOR * random.random() for _ in range(SAMPLES)]
    # TODO: test with bigger lists and random numbers
    window = 4

    class Counter:
        i = 0

    def wander(inputs, index):
        result = inputs[index.i]
        index.i += 1
        return [result]

    c = Counter()
    b = midas.Buffer("single_mean", ('roy',))
    b.acquire_by_samples(len(example_input), wander, args=(example_input, c))
    mean_b = b.add_mean(window=window)
    midas.start(b, mean_b, wait_to_complete=True, open_powerspy=False)
    assert list(mean_b.data['roy_mean']) ==\
        pytest.approx([statistics.mean(example_input[i-window:i]) for i, _ in enumerate(example_input[window-1:], window)])


def test_multi_signal_mean(setup):
    b = midas.Buffer("multi_mean", ('m1', 'm2', 'm3'))
    b.acquire_by_samples(300, tests.sample_gen, args=(3,))
    m = b.add_mean()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    for i, s in enumerate(m.signals):
        assert m.data[s].iloc[-1] == pytest.approx(statistics.mean(tests.x[i][:len(m)]))


def test_single_signal_max(setup):
    b = midas.Buffer("single_max_source", ('m1',))
    b.acquire_by_samples(300, tests.sample_gen, args=(1,))
    m = b.add_max()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    assert m.data['m1_max'].iloc[-1] == pytest.approx(max(tests.x[0][:len(m)]))


def test_1_signal_max_window():
    example_input = [1, 3, -1, 100, 4, 5, 2, 34, -3, 5, 3, 6, 7]
    window = 4

    class Counter:
        i = 0

    def wander(inputs, index):
        result = inputs[index.i]
        index.i += 1
        return [result]

    c = Counter()
    b = midas.Buffer("single_max_source", ('roy',))
    b.acquire_by_samples(len(example_input), wander, args=(example_input, c))
    max_b = b.add_max(window=window)
    midas.start(b, max_b, wait_to_complete=True, open_powerspy=False)
    assert list(max_b.data['roy_max']) == [100, 100, 100, 100, 34, 34, 34, 34, 6, 7]


def test_multi_signal_max(setup):
    b = midas.Buffer("multi_max_source", ('m1', 'm2', 'm3'))
    b.acquire_by_samples(300, tests.sample_gen, args=(3,))
    m = b.add_max()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    for i, s in enumerate(m.signals):
        assert m.data[s].iloc[-1] == pytest.approx(max(tests.x[i][:len(m)]))


def test_single_signal_min(setup):
    b = midas.Buffer('single_min_source', ('m1',))
    b.acquire_by_samples(300, tests.sample_gen, args=(1,))
    m = b.add_min()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    assert m.data['m1_min'].iloc[-1] == pytest.approx(min(tests.x[0][:len(m)]))


def test_1_signal_min_window():
    example_input = [1, 3, -1, 100, 4, 5, 2, 34, -3, 5, 3, 6, 7]
    window = 4

    class Counter:
        i = 0

    def wander(inputs, index):
        result = inputs[index.i]
        index.i += 1
        return [result]

    c = Counter()
    b = midas.Buffer('single_min_source', ('roy',))
    b.acquire_by_samples(len(example_input), wander, args=(example_input, c))
    min_b = b.add_min(window=window)
    midas.start(b, min_b, wait_to_complete=True, open_powerspy=False)
    assert list(min_b.data['roy_min']) == [-1, -1, -1, 2, 2, -3, -3, -3, -3, 3]


def test_multi_signal_min(setup):
    b = midas.Buffer('multi_min_source', ('m1', 'm2', 'm3'))
    b.acquire_by_samples(300, tests.sample_gen, args=(3,))
    m = b.add_min()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    for i, s in enumerate(m.signals):
        assert m.data[s].iloc[-1] == pytest.approx(min(tests.x[i][:len(m)]))


def test_single_signal_p2p(setup):
    b = midas.Buffer('single_p2p_source', ('m1',))
    b.acquire_by_samples(300, tests.sample_gen, args=(1,))
    m = b.add_p2p()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    calculated_p2p = max(tests.x[0][:len(m)]) - min(tests.x[0][:len(m)])
    assert m.data['m1_p2p'].iloc[-1] == pytest.approx(calculated_p2p)


def test_1_signal_p2p_window():
    example_input = [1, 3, -1, 100, 4, 5, 2, 34, -3, 5, 3, 6, 7]
    window = 4

    class Counter:
        i = 0

    def wander(inputs, index):
        result = inputs[index.i]
        index.i += 1
        return [result]

    c = Counter()
    b = midas.Buffer('single_p2p_source', ('roy',))
    b.acquire_by_samples(len(example_input), wander, args=(example_input, c))
    p2p_b = b.add_p2p(window=window)
    midas.start(b, p2p_b, wait_to_complete=True, open_powerspy=False)

    assert list(p2p_b.data['roy_p2p']) == [101, 101, 101, 98, 32, 37, 37, 37, 9, 4]


def test_multi_signal_p2p(setup):
    b = midas.Buffer('single_p2p_source', ('m1',))
    b.acquire_by_samples(300, tests.sample_gen, args=(1,))
    m = b.add_p2p()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    for i, s in enumerate(m.signals):
        calculated_p2p = max(tests.x[i][:len(m)]) - min(tests.x[i][:len(m)])
        assert m.data[s].iloc[-1] == pytest.approx(calculated_p2p)


def test_single_signal_std(setup):
    b = midas.Buffer('single_std_source', ('m1',))
    b.acquire_by_samples(300, tests.sample_gen, args=(1,))
    m = b.add_std()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    assert m.data['m1_std'].iloc[-1] == pytest.approx(statistics.stdev(tests.x[0][:len(m)]))


def test_multi_signal_std(setup):
    b = midas.Buffer("multi_std_source", ('m1', 'm2', 'm3'))
    b.acquire_by_samples(300, tests.sample_gen, args=(3,))
    m = b.add_std()
    midas.start(b, m, wait_to_complete=True, open_powerspy=False)
    for i, s in enumerate(m.signals):
        assert m.data[s].iloc[-1] == pytest.approx(statistics.stdev(tests.x[i][:len(m)]))


def test_1_signal_std_window():
    example_input = [random.random() for i in range(300)]
    window = 4
    calculated_std = [statistics.stdev(example_input[i-window:i]) for i, _ in enumerate(example_input[window-1:], window)]

    class Counter:
        i = 0

    def wander(inputs, index):
        result = inputs[index.i]
        index.i += 1
        return [result]

    c = Counter()
    b = midas.Buffer('single_std_source', ('roy',))
    b.acquire_by_samples(len(example_input), wander, args=(example_input, c))
    std_b = b.add_std(window=window)
    midas.start(b, std_b, wait_to_complete=True, open_powerspy=False)
    assert list(std_b.data['roy_std']) == pytest.approx(calculated_std)

# EOF
