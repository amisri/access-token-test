import pytest
import midas
import os

from midas.fortlogs_client import create_acquisition, add_log, fetch_rbac_token

try:
    host = os.uname().nodename

except Exception:
    host = ""

if host != 'cwe-513-vol984.cern.ch':
    pytest.skip("No grid root certificate inside CI", allow_module_level=True)

midas.set_endpoint_env('dev')

def get_log_meta(acquisition_id: int) -> list:
    """

    :param acquisition_id: target acquisition's ID
    :return: list of log meta data
    """
    get_meta_request = requests.get(
        f"{midas.constants.Config.API_URL}/acquisitions/{acquisition_id}",
        verify=False,
    )

    if not get_meta_request.ok:
        raise midas.exceptions.FortLogsPullFailed(get_meta_request.text)

    return get_meta_request.json()


@pytest.fixture
def setup():
    midas.tests.reset()
    yield


def test_create_acquisition(setup):
    token = fetch_rbac_token()
    acquisition = create_acquisition("test_script", ['HCRMIC____-TT000342', 'HCRMIC____-TT000343'], token)

    assert acquisition["script_name"] == "test_script"


def test_add_analog_log(setup):
    token = fetch_rbac_token()

    acquisition = create_acquisition("test_script", [], token)

    acquisition_id = acquisition["acquisition_id"]

    log = {
        "name": "DCV",
        "version": "1.2",
        "device": "test_script",
        "source": "MIDAS",
        "signals": [
            {
                "name": "5",
                "samples": [
                    0
                ],
            },
            {
                "name": "-5",
                "samples": [
                    0
                ],
            }
        ]
    }

    log_id = add_log(acquisition_id, log, token)

    logs_meta = [lg["log_id"] for lg in get_log_meta(acquisition_id)]

    assert log_id in logs_meta


def test_multiple_signal_open(setup):
    b = midas.Buffer("BIG BUFFER NAME", ("foo", "car", "bar"))
    b.acquire_by_duration(0.3, midas.tests.sample_gen, args=(3,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)


# EOF
