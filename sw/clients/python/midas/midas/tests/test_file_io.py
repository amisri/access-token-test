import pytest
import midas
import midas.tests as tests
import random

midas.set_endpoint_env('dev')


@pytest.fixture
def setup():
    tests.reset()
    yield


def test_single_s_json(setup):
    s = midas.Buffer("file_single", ("foo",))
    s.acquire_by_duration(0.5, tests.sample_gen, args=(1,))
    midas.start(s, wait_to_complete=True, open_powerspy=False)
    s.to_json()


def test_multi_s_json(setup):
    b = midas.Buffer("file_multi", ("foo", "bar", "glee"))
    b.acquire_by_duration(0.5, tests.sample_gen, args=(3,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)
    b.to_json("multi")


def test_multi_buffer_json(setup):
    s = midas.Buffer("medusa_all", ("foo", "bar", "glee"))
    s.acquire_by_duration(0.5, tests.sample_gen, args=(3,))
    midas.start(s, wait_to_complete=True, open_powerspy=False)
    s.to_json()


def test_single_csv(setup):
    s = midas.Buffer("single_csv", ("foo",))
    s.acquire_by_duration(0.5, tests.sample_gen, args=(1,))
    midas.start(s, wait_to_complete=True, open_powerspy=False)
    s.to_csv()


def test_multi_csv(setup):
    s = midas.Buffer("big_capture", ("foo", "bar", "glee"))
    s.acquire_by_duration(0.5, tests.sample_gen, args=(3,))
    midas.start(s, wait_to_complete=True, open_powerspy=False)
    s.to_csv()


def test_simultaneous():
    base = midas.Buffer("basic", ('r_0',))
    transform = midas.SubscribedBuffer('transformation', ('t_one',), base.buffer_name)

    def foobar(x):
        return [x.r_0 + 1]

    base.acquire_by_duration(0.3, lambda: [random.random()])
    transform.acquire(foobar)
    midas.start(base, transform, wait_to_complete=True, open_powerspy=False)

    base.to_json("sync_base")
    transform.to_json("sync_transform")


# EOF
