import pytest
import midas


@pytest.mark.skip()
def test_fetch_powerspy_url():
    URL = "https://accwww.cern.ch/powerspy/chart/?history=1021175:2051292?history=1021175:2051296"
    log = midas.fetch(URL)
    assert len(log) == 2


@pytest.mark.skip()
def test_fetch_fortlogs_url():
    URL = "https://accwww.cern.ch/fortlogs/logs?log_id=1021175?log_id=1443333"
    log = midas.fetch(URL)
    assert len(log) == 2


@pytest.mark.skip()
def test_fetch_by_id():
    log = midas.fetch(4333)
    assert len(log) == 1


@pytest.mark.skip()
def test_fetch_by_other_id():
    log = midas.fetch((4333, 32344))
    assert len(log) == 2
