import midas
import midas.tests as tests
import random
import pytest

midas.set_endpoint_env('dev')


@pytest.fixture
def setup():
    tests.reset()
    yield


def test_periodic_buffer():
    b = midas.PeriodicBuffer('PERIOD_B', ('one', 'two', 'three', 'four'), 0.1)
    b.acquire_by_duration(0.3, tests.sample_gen, args=(4,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)

    assert len(b) != 0

    for i, n in enumerate(b.data.columns[1:]):
        assert list(b.data[n]) == pytest.approx(tests.x[i][:len(b)])


def test_bad_callback_count(setup):
    with pytest.raises(midas.WrongSampleCount):
        b = midas.Buffer("bad_samples", ("foo", "bar"))
        b.acquire_by_samples(20, tests.sample_gen, args=(1,))
        midas.start(b, wait_to_complete=True, open_powerspy=False)


def test_bad_callback_type(setup):
    with pytest.raises(midas.InvalidBufferReturnType):
        b = midas.Buffer("invalid_rt", ("foo",))

        def generate_str():
            return "hey"
        b.acquire_by_samples(20, generate_str)
        midas.start(b, wait_to_complete=True, open_powerspy=False)


def test_bad_arg_type(setup):
    with pytest.raises(midas.InvalidCallbackArg):
        b = midas.Buffer("invalid_rt", ("foo",))

        def generate_str():
            return "hey"
        b.acquire_by_samples(20, generate_str, args=2)


def test_bad_arg_type(setup):
    with pytest.raises(midas.InvalidCallbackArg):
        b = midas.Buffer("invalid_rt", ("foo",))
        def generate_str():
            return "hey"
        b.acquire_by_samples(20, generate_str, kwargs=2)


def test_single_signal_name():
    s = midas.Buffer("single_s", ("bar",))
    assert "bar" in s.data.columns


def test_t_named_signal():
    with pytest.raises(midas.InvalidSignalName):
        midas.Buffer('sfdfs', ('t,'))


def test_bad_buffer_name(setup):
    with pytest.raises(midas.InvalidBufferName):
        midas.Buffer("sdfisdofjsdfsjdjsdfsffdsffsfddjifdso", ('fsdsa',))


def test_bad_signal_name(setup):
    with pytest.raises(midas.InvalidSignalName):
        midas.Buffer("boo", ("sdfisdofjsdfsjdjsdfsffdsffsfddjifdso",))
    with pytest.raises(midas.InvalidSignalName):
        midas.Buffer("boo", 1)


def test_multiple_signals():
    s = midas.Buffer("multi", ("foo", "car", "bar"))
    assert ["foo", "car", "bar"] == list(s.data.columns[1:])


def test_buffer_name():
    s = midas.Buffer("BUFFER_NAME", ('foo',))
    assert s.buffer_name == "BUFFER_NAME"


def test_no_callback_start():
    with pytest.raises(midas.UnregisteredCallback):
        b = midas.Buffer("NO_CBCK", ('fus', 'roh', 'dah'))
        midas.start(b, wait_to_complete=True, open_powerspy=False)


def test_by_max_samples():
    x = midas.Buffer("max_samples", ('foo',))
    x.acquire_by_samples(50, lambda: [random.random()])
    midas.start(x, wait_to_complete=True, open_powerspy=False)

    assert len(x) == 50


def test_kwargs():
    x = midas.Buffer("max_samples", ('foo',))
    
    class Counter:
        i = 0

    c = Counter()

    outputs = {
        'a': 1,
        'b': 5,
        'c': 6,
        'd': 9,
        'e': 15,
    }

    def call_me_back(*inputs, **outputs):
        v = inputs[c.i]
        
        if c.i < len(inputs):
            c.i += 1
            return [outputs[v]]

    x.acquire_by_samples(len(outputs), call_me_back, args=list(outputs.keys()), kwargs=outputs)
    midas.start(x, wait_to_complete=True, open_powerspy=False)
    assert list(x.data["foo"]) == [1, 5, 6, 9, 15]


def test_by_max_duration():
    x = midas.Buffer("max_dur", ('com',))
    x.acquire_by_duration(0.5, tests.sample_gen, args=(1,))
    midas.start(x, wait_to_complete=True, open_powerspy=False)

    assert pytest.approx(x.duration, rel=1e-1) == 0.5


def test_buffer_len(setup):
    b = midas.Buffer("zero_len", ("foo", "bar"))
    assert len(b) == 0

    b2 = midas.Buffer("test_len", ('foo',))
    b2.acquire_by_samples(50, lambda: [random.random()])
    midas.start(b2, wait_to_complete=True, open_powerspy=False)

    assert len(b2) == 50


def test_acquire_one_signal(setup):
    s = midas.Buffer("one_s", ("foo",))
    s.acquire_by_duration(0.3, tests.sample_gen, args=(1,))
    midas.start(s, wait_to_complete=True, open_powerspy=False)

    assert list(s.data["foo"]) == pytest.approx(tests.x[0][:len(s)]) and len(s) != 0


def test_multi_acquire(setup):
    b = midas.Buffer("OTHER_NAME", ("foo", "car", "bar"))
    b.acquire_by_duration(0.3, tests.sample_gen, args=(3,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)

    for i, n in enumerate(b.data.columns[1:]):
        assert list(b.data[n]) == pytest.approx(tests.x[i][:len(b)]) and len(b) != 0


def test_max_samples_by_defined_list(setup):
    b = midas.Buffer("max_samp", ("foo",))
    b.acquire_by_samples(100, tests.sample_gen, args=(1,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)

    assert list(b.data.foo) == pytest.approx(tests.x[0][:len(b)]) and len(b) == 100


def test_bad_upstream(setup):
    with pytest.raises(midas.UnregisteredCallback):
        b = midas.Buffer("parent", ("foo",))
        m = midas.SubscribedBuffer("child", ("child",), b.buffer_name)
        m.acquire(lambda: [random.random()])
        midas.start(b, m, wait_to_complete=True, open_powerspy=False)


def test_acquire_twice():
    with pytest.raises(midas.AcquireTwiceError):
        zero = midas.Buffer("twice", ('basic',))
        zero.acquire_by_samples(100, lambda: [random.random()])
        zero.acquire_by_duration(0.5, lambda: [random.random()])


def test_has_upstream_buffer():
    base = midas.Buffer('Base', ('base',))
    transform = midas.SubscribedBuffer('transform', ('t_one',), base.buffer_name)
    base.acquire_by_samples(50, lambda: [random.random()])
    transform.acquire(lambda x: [x.base + 1])

    midas.start(base, transform, wait_to_complete=True, open_powerspy=False)
    assert transform.buffer_name in [b.buffer_name for b in base._subscribers]


def test_chain_buffer():
    base = midas.Buffer('base', ('r_0',))
    transform = midas.SubscribedBuffer('transform', ('t_one',), base.buffer_name)

    def foobar(x):
        return [x.r_0 + 1]

    base.acquire_by_duration(0.3, tests.sample_gen, args=(1,))
    transform.acquire(foobar)
    midas.start(base, transform, wait_to_complete=True, open_powerspy=False)

    assert len(base) == len(transform)
    assert list(base.data.r_0) == pytest.approx([x - 1 for x in transform.data.t_one])


def test_chain_finish_time():
    base = midas.Buffer('base', ('r_0',))
    transform = midas.SubscribedBuffer('transform', ('t_one',), base.buffer_name)

    def foobar(x):
        return [x.r_0 + 1]

    base.acquire_by_duration(0.3, tests.sample_gen, args=(1,))
    transform.acquire(foobar)
    midas.start(base, transform, wait_to_complete=True, open_powerspy=False)

    assert list(base.data.t) == list(transform.data.t)
    assert base.finish_time == transform.finish_time


def test_multi_level_chain():
    zero = midas.Buffer('basic', ('basic',))
    first = midas.SubscribedBuffer('trasform', ('add_one',), zero.buffer_name)
    second = midas.SubscribedBuffer('transform2', ('take_one',), first.buffer_name)

    def add(x, y):
        return [x[1] + y]

    zero.acquire_by_samples(100, lambda: [random.random()])
    first.acquire(add, args=(1,))
    second.acquire(add, args=(-1,))

    midas.start(zero, first, second, wait_to_complete=True, open_powerspy=False)

    assert len(zero) == len(first) == len(second)
    assert list(zero.data.basic) == pytest.approx(list(second.data.take_one))


def test_multi_consumer():
    zero = midas.Buffer('basic', ('basic',))
    first = midas.SubscribedBuffer('trasform', ('add_one',), zero.buffer_name)
    second = midas.SubscribedBuffer('transform2', ('take_one',), zero.buffer_name)

    def add(x, y):
        return [x[1] + y]

    zero.acquire_by_samples(100, lambda: [random.random()])
    first.acquire(add, args=(1,))
    second.acquire(add, args=(-1,))

    midas.start(zero, first, second, wait_to_complete=True, open_powerspy=False)

    assert len(zero) == len(first) == len(second)
    assert list(zero.data.basic) == pytest.approx([x - 1 for x in first.data.add_one])
    assert list(zero.data.basic) == pytest.approx([x + 1 for x in second.data.take_one])


def test_stop_early():
    x = midas.Buffer('stop_b', ('foo',))
    EARLY_STOP = 30

    class Counter:
        i = 0

    start = Counter()

    def counter(c):
        c.i += 1

        print(c.i)
        if c.i > EARLY_STOP:
            return midas.Stop()

        return [random.random()]

    x.acquire_by_samples(100, counter, args=(start,))
    midas.start(x, wait_to_complete=True, open_powerspy=False)

    assert len(x) == EARLY_STOP


def test_step_signals():
    b = midas.Buffer('TEST_NAME', ('foo', 'bar'), step_signals=('foo',))
    b.acquire_by_samples(50, lambda: [random.random(), random.random()])
    midas.start(b, wait_to_complete=True, open_powerspy=False)


def test_timestamp_override():
    b = midas.Buffer('TEST_NAME', ('foo',), timestamp_override=True)
    
    class Counter:
        i = 0

    def generate_samples(nr_signals):
        result = Counter.i
        Counter.i += 1
        return [result for _ in range(nr_signals + 1)]

    b.acquire_by_samples(100, generate_samples, args=(1,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)
    assert list(b.data['t']) == pytest.approx([i for i in range(100)])


def test_timestamp_override_acquire_duration():
    b = midas.Buffer('TEST_NAME', ('foo',), timestamp_override=True)
    
    class Counter:
        i = 0

    def generate_samples(nr_signals):
        result = Counter.i
        Counter.i += 1
        return [result for _ in range(nr_signals + 1)]

    b.acquire_by_duration(100, generate_samples, args=(1,))
    midas.start(b, wait_to_complete=True, open_powerspy=False)
    assert len(b.data['foo']) == 102

# EOF
