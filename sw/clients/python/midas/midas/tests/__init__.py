import math
import random
import time


SAMPLES = 1000

def new_sample(i):
    return random.uniform(-10, 10) * math.sin(time.time() * i)

x = [[new_sample(i) for i in range(SAMPLES)] for j in range(4)]


iter_list = -1


def sample_gen(dimension):
    global iter_list
    time.sleep(0.001)
    if iter_list == SAMPLES:
        iter_list = -1
    else:
        iter_list += 1

    return tuple(x[v][iter_list] for v in range(dimension))

def reset():
    global iter_list
    iter_list = -1

# EOF
