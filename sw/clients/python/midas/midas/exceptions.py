class InvalidBufferName(Exception):
    pass


class InvalidCallbackArg(Exception):
    pass


class InvalidDeviceName(Exception):
    pass


class InvalidSignalName(Exception):
    pass


class WrongSampleCount(Exception):
    pass


class FortLogsPushFailed(Exception):
    pass


class FortLogsPullFailed(Exception):
    pass


class InvalidOutputFormat(Exception):
    pass


class EmptyBuffer(Exception):
    pass


class InvalidBufferReturnType(Exception):
    pass


class EmptyReceiveQueue(Exception):
    pass


class AcquireTwiceError(Exception):
    pass


class UnregisteredCallback(Exception):
    pass


class InvalidRBACToken(Exception):
    pass

# EOF
