import midas
import webbrowser

from typing import List


def open_on_powerspy(logs: List[int]):
    """This function opens a browser tab on PowerSpy with the given logs

    :params logs: list of tuples of format (acquisition_id, log_id)
    """
    query_args = []
    query_arg_template = "?stream={}"
    for log_id in logs:
        arg = query_arg_template.format(log_id)
        query_args.append(arg)

    powerspy_chart_url = "".join([midas.constants.Config.POWERSPY_URL, *query_args])
    webbrowser.open(powerspy_chart_url)
