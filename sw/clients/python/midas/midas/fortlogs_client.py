import midas
import base64
import logging
import os
import requests
import pyfgc_rbac

from midas.constants import ENDPOINT_MAP
from fortlogs_schemas import Barcode, Barcodes
from typing import List



def fetch_rbac_token():
    """
    Try to acquire RBAC token in the following order:
    1. By Kerberos ticket exchange
    2. By Authorized Host Location declared in CCDE
    3. Throw exception if none of the above works
    """
    # Finally, try Kerberos if available
    try:
        decoded_token = pyfgc_rbac.get_token_kerberos()
        token = base64.b64encode(decoded_token).decode()
        return token
    except (pyfgc_rbac.KerberosUnavailableError, pyfgc_rbac.RbacServerError, pyfgc_rbac.GSSError) as e:
        logging.debug(f"Unable to get new RBAC token by krb5: {e}")

    raise midas.InvalidRBACToken("Unable to acquire new RBAC token.")


def create_acquisition(script_name: str, barcodes: List[str], access_token: dict) -> dict:
    """

    :param script_name: name of the script
    :param access_token: fortlogs access token
    :return: acquisition object created
    """
    body = {
        "script_name": script_name,
    }
    barcodes_to_send = Barcodes(barcodes=[Barcode(barcode=b) for b in barcodes])
    body.update(barcodes_to_send.dict())

    new_acquisition = requests.post(
        f"{midas.constants.Config.API_URL}/midas",
        json=body,
        headers={"Authorization": f"Bearer {access_token}"},
        verify=False,
    )

    if not new_acquisition.ok:
        raise midas.exceptions.FortLogsPushFailed(new_acquisition.text)

    return new_acquisition.json()


def add_log(acquisition_id: int, log: dict, access_token: dict, log_type: str = "analog") -> int:
    """

    :param acquisition_id: target acquisition's ID
    :param log: log to insert
    :param access_token: fortlogs access token
    :param log_type: type of the log object
    :return: log's ID
    """
    if log_type not in ENDPOINT_MAP:
        message = f"Wrong log type. Expected any of {list(ENDPOINT_MAP.keys())}"
        raise Exception("Wrong log type. Expected any of {}")

    add_log_request = requests.post(
        f"{midas.constants.Config.API_URL}/{ENDPOINT_MAP[log_type]}",
        params={"acquisition_id": acquisition_id},
        json=log,
        headers={"Authorization": f"Bearer {access_token}"},
        verify=False,
    )

    if not add_log_request.ok:
        raise midas.exceptions.FortLogsPushFailed(add_log_request.text)

    return add_log_request.json()
