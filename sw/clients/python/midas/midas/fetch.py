import midas
import re
import requests

from fortlogs_schemas import (
    LogType, LogSubType, AnalogLog, DigitalLog,
    BodeLog, EventLog, ErrorLog, TableLog
)
from typing import List, Union


TYPE_MAP = {
    LogType.analog: AnalogLog,
    LogType.digital: DigitalLog,
    LogType.frequency: BodeLog
}

SUBTYPE_MAP = {
    LogSubType.default: TableLog,
    LogSubType.error: ErrorLog,
    LogSubType.event: EventLog
}


def _logs_from_ids(log_id: List[int], api_endpoint: str):
    """Fetch Log from fortlog

    :param log_id: receive an integer log identifier
    :return: log object
    """
    endpoint = f"{api_endpoint}/logs"
    fetched = requests.get(endpoint, params={'log_id': log_id}, verify=False)

    if not fetched.ok:
        raise midas.exceptions.FortLogsPullFailed(fetched.text)

    result = fetched.json()
    logs = list()
    for log_obj in result:
        data = log_obj['data']
        if data['type'] in TYPE_MAP:
            logs.append(TYPE_MAP[data['type']](**data))
        elif data['type'] == LogType.table and data['subtype'] in SUBTYPE_MAP:
            logs.append(SUBTYPE_MAP[data['subtype']](**data))
        else:
            raise NotImplementedError(f"Log of type {data['type']} and subtype {data['subtype']}\
                not defined.")
    return logs


def _resolve_host_powerspy(url):
    for env, profile in midas.constants.PROFILES.items():
        if profile.powerspy_host in url:
            return f"https://{profile.fortlogs_host}.cern.ch{profile.mount_point}"


def _resolve_host_fortlogs(url):
    for env, profile in midas.constants.PROFILES.items():
        if profile.fortlogs_host in url:
            return f"https://{profile.fortlogs_host}.cern.ch{profile.mount_point}"


def fetch(log_source: Union[str, List[int], int]) -> List:
    """This method fetches Log objects of any of the existing types:
        - AnalogLog
        - DigitalLog
        - BodeLog
        - TableLog
        - EventLog
        - ErrorLog

    It can accept an URL string from either PowerSpy or FortLogs with historical Logs;
    or an integer representing a log id; or even a list of integer log ID's.
    Example:
    
    .. code-block:: python

        ls = midas.fetch("https://accwww.cern.ch/powerspy/chart/?history=1029185:2087184?history=1029185:2087181?history=1029185:2087182")

    :param log_source: It can be a single integer, URL string or list of ID's
    :return: a list of Log objects
    """
    ids_to_fetch = list()
    api_endpoint = midas.constants.Config.API_URL

    if isinstance(log_source, int):
        ids_to_fetch.append(log_source)

    elif all(isinstance(log_id, int) for log_id in log_source):
        for log_id in log_source:
            ids_to_fetch.append(log_id)

    elif '.cern.ch/powerspy' in log_source:
        api_endpoint = _resolve_host_powerspy(log_source)
        pattern = re.compile(r'history=(\d+):(\d+)')
        for _, log_id in pattern.findall(log_source):
            ids_to_fetch.append(log_id)

    elif '/logs?log_id=' in log_source:
        api_endpoint = _resolve_host_fortlogs(log_source)
        pattern = re.compile(r'log_id=(\d+)')
        for log_id in pattern.findall(log_source):
            ids_to_fetch.append(log_id)

    else:
        raise Exception("URL <{log_source}> has a bad format!")

    return _logs_from_ids(ids_to_fetch, api_endpoint)
