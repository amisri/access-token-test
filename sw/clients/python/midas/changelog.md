Version 2.0.2
-------------

- **TODO**


Version 2.0.0dev
-------------
_breaks compatibility with older scripts_

- Add Buffer concept;
- Remove Signal class
- Export buffer to csv/json;
- Stream;
- New acquire function


Version 1.3.1:
--------------

- Fix export to csv for signals that share the same time frame


Version 1.3.0:
--------------

- Add window filtering for stats
- Add support for numpy arrays
- Add offline mode
- Export to csv functionality