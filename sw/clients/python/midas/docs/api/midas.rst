API
===

General
-------


.. autofunction:: midas.fetch

.. autofunction:: midas.start


midas.Buffer
------------

.. autoclass:: midas.PeriodicBuffer
    :members:
    :undoc-members:

    .. automethod:: __init__


.. autoclass:: midas.Buffer
    :members:
    :undoc-members:

    .. automethod:: __init__


.. autoclass:: midas.SubscribedBuffer
    :members:
    :undoc-members:
    :exclude-members: resolve_subscription, stop

    .. automethod:: __init__


.. autoclass:: midas.buffer.BaseBuffer
    :members:
    :undoc-members:
    :exclude-members: run, stop, join, id_lock, id_tracker

    .. automethod:: __init__
