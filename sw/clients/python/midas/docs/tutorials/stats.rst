Statistical Measures
====================

There are statistical measures available as properties.
When not specified the following will be applied over the entire window of the acquisition.

* Mean: `mean`
* Maximum: `max`
* Minimum: `min`
* Standard Deviation: `std`
* Peak-to-peak value: `p2p`

If one wants to restrict the statistical measure to a specific window 
(either the most recent X samples or during the latest X seconds) of the acquisition:

.. code:: python

   import midas
   import random
   
   buffer = midas.Buffer('TEST_DEV', ('foo',))

   # Acquire measurements for 30 seconds
   buffer.acquire_by_duration(30, lambda x,y: [random.uniform(x,y)], args=(-10, 10))

   # Add a rolling average of the latest 30 samples
   mean_b = buffer.add_mean(window=30, window_type='samples')
   
   # Could also make a duration based window like shown below:
   # mean_b = buffer.add_mean(window=30, window_type='duration')

   midas.start(buffer, mean_b, wait_to_complete=True)


