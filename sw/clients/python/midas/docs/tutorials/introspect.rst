Introspecting Buffers
=====================

The implemented Buffer objects have several properties which map 
to useful analysis measures or acquisition attributes.


First Sample Timestamp
######################

The start time of the acquisition can be acessed as a property of the buffer.
It will only be anything other than `None` when the acquisition has started.

.. code:: python

   import midas
   import random
   
   buffer = midas.Buffer('TEST_DEV', ('foo',))

   # Acquire measurements for 3 seconds
   buffer.acquire_by_duration(3, lambda x,y: [random.uniform(x,y)], args=(-10, 10))
   
   midas.start(buffer, wait_to_complete=True)

   print(buffer.start_time)


Get Buffer Data as a Dataframe
##############################

`Pandas <https://pandas.pydata.org/docs/getting_started/index.html#getting-started>`_ 
is a popular library in the python ecossystem to handle tabular data.
It is supported by other libraries in the python world:

* `Scipy <https://docs.scipy.org/doc/scipy/reference/index.html>`_: advanced algebra & machine learning algorithms library
* `TensorFlow <https://www.tensorflow.org/tutorials/load_data/pandas_dataframe>`_: deep learning library
* `Sympy <https://www.sympy.org/en/index.html>`_: library for symbolic maths 
* `MatPlotLib <https://pandas.pydata.org/pandas-docs/version/0.13.1/visualization.html>`_: data visualization library

.. code:: python

   import midas
   import random
   
   buffer = midas.Buffer('TEST_DEV', ('foo',))

   # Acquire measurements for 3 seconds
   buffer.acquire_by_duration(3, lambda x,y: [random.uniform(x,y)], args=(-10, 10))

   midas.start(buffer, wait_to_complete=True)

   # Get buffer as panda table
   x = buffer.data
