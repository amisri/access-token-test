Making Acquisitions
===================


Acquisitions based on duration
##############################

.. code:: python

   import midas
   import random
   
   def measure(factor: float):
       return [factor * random.uniform(-10, 10)]


   buffer = midas.Buffer('TEST_DEV', ('foo',))
   
   # Acquire measurements for 3 seconds
   buffer.acquire_by_duration(3, measure, args=(5,))

   # Does not wait for the job to complete before the script exits
   midas.start(buffer)

   # Blocks the program waiting for the job to finish
   midas.start(buffer, barcodes=["HCRMIC____-TT000343"], wait_to_complete=True)


Acquisitions based on number of samples
#######################################

.. code:: python

   import midas
   import random
   
   def measure(factor: float):
       return [random.uniform(-10, 10) - factor]

   buffer = midas.Buffer('TEST_DEV', ('foo',))

   # Acquire 300 samples
   buffer.acquire_by_samples(300, measure, args=(5,))

   # Blocks the program waiting for the job to finish
   midas.start(buffer, wait_to_complete=True)


Acquisition with multiple signals
#################################

.. code:: python

   import midas
   import random

   # buffer with 3 signals
   buffer = midas.PeriodicBuffer('TUTORIAL', ("foo", "car", "bar"), 0.1)

   # callback function
   def measure_dvms(factor: float):
      return factor * [random.random() for _ in range(3)]

   # acquire data for 1.2 seconds
   buffer.acquire_by_duration(1.2, measure_dvms, args=(2,))

   # Blocks the program waiting for the job to finish
   midas.start(buffer, wait_to_complete=True)


Acquisitions based on precomputed lists
#######################################

.. code:: python

   import midas
   import random
   import time

   trigger_time = time.time()
   sample_period = 0.02

   measurements = [random.random() for _ in range(300)]

   def list_iterator(precomputed_list):
      return precomputed_list.pop(0)

   buffer = midas.PeriodicBuffer('TUT_LISTS', ('my_list',), sample_period)

   buffer.acquire_by_samples(len(measurements), list_iterator, args=(measurements,), start_time=trigger_time)

   # Blocks the program waiting for the job to finish
   midas.start(buffer, wait_to_complete=True)


Stoping acquisitions manually
#############################

.. code:: python

   import midas
   import random
   
   def measure(factor: float):
      sample = factor * random.uniform(-10, 10)
      if sample > 5:
         print('Early exit!')
         return midas.Stop()
      return [sample]

   buffer = midas.Buffer('TEST_DEV', ('foo',))

   # Acquire 300 samples
   buffer.acquire_by_samples(300, measure, args=(1,))

   # Blocks the program waiting for the job to finish
   midas.start(buffer, wait_to_complete=True)


Chaining Buffer Acquisitions
****************************

.. code:: python

   import midas
   import random

   parent_buffer = midas.Buffer("Water", ("first",))
   child_buffer = midas.SubscribedBuffer("Lemonade", ("modified",), parent_buffer.buffer_name)

   # The first argument used on the child function callback is the data received 
   # Data received in this case is a tuple of size 2: (time, data)
   def add(received_data, offset):
       return [received_data[1] + offset]
   
   def measure(factor: float):
       return [factor * random.uniform(-10, 10)]

   parent_buffer.acquire_by_samples(300, measure, args=(3,))
   child_buffer.acquire(add, args=(3,))

   midas.start(parent_buffer, child_buffer, wait_to_complete=True)
