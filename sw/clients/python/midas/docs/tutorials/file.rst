Exporting Data into Files
=========================

Using midas it is possible to export data to files that can be then added manually into PowerSpy.

.. code:: python

   import midas
   import random
   

   buffer = midas.Buffer('TEST_DEV', ('foo',))

   # Acquire measurements for 3 seconds
   buffer.acquire_by_duration(3, lambda x,y: [random.uniform(x, y)], args=(-10, 10))

   midas.start(buffer, wait_to_complete=True)

   # Export to file, you can choose between csv or json (this one is 
   # preferable for integration with PowerSpy). The default is json format.
   buffer.to_csv()

   # Another option if you need to set a different name
   buffer.to_csv(filename="TESTNAME")

   # It is also possible to export the file to a json format
   buffer.to_json()
