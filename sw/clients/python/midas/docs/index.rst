.. midas documentation master file, created by
   sphinx-quickstart on Fri Mar 29 09:29:59 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to midas's documentation!
=================================

Contact converter-controls-support@cern.ch for any question or bug report.

Other Libraries Developed By Us
###############################

* `pyfgc <https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/pyfgc/stable/>`_: library to communicate with FGC's via USB/TCP-IP
* `hpmlab <https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/hpmlab/stable/>`_: library to control hpm lab equipment.


Contents:

.. toctree::
   :maxdepth: 1

   tutorials/install.rst
   tutorials/acquire.rst
   tutorials/stats.rst
   tutorials/introspect.rst
   tutorials/file.rst
   api/midas.rst
