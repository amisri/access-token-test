import os

from setuptools import setup, find_packages
from codecs import open


here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'ccs_sso_utils', '__version__.py'), 'r', 'utf-8') as f:
    exec(f.read(), about)


with open("README.md", "r") as fh:
    description = fh.read()


requirements = {
    'core': [
        'requests',
        'beautifulsoup4',
        'requests-kerberos',
    ],
    'test': [
        'pytest',
    ],
    'doc': [
        'sphinx',
        'sphinx_rtd_theme',
    ],
}


setup(
    name="ccs_sso_utils",
    version=about['__version__'],
    author=about['__authors__'],
    author_email=about['__emails__'],
    description="OAuth2 Utilities",
    long_description=description,
    long_description_content_type='text/markdown',
    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/ccs_sso_utils/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/ccs_sso_utils',
    },
    packages=find_packages(),

    python_requires=">=3.7",
    install_requires=requirements['core'],
    extras_require={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test'],
)
