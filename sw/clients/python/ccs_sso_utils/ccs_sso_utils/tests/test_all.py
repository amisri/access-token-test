import os
import pytest

from ccs_sso_utils import get_sso_token, save_sso_cookie


try:
    host = os.uname().nodename

except Exception:
    host = ""


@pytest.mark.skipif(host != 'cwe-513-vol984.cern.ch' and 'dev' not in host, reason="only run on dev machine")
def test_kerberos_token():
    url: str = "https://nulauren-dev.cern.ch/docs/oauth2-redirect"
    client_id: str = "fort-logs-dev"
    token = get_sso_token(url, client_id, verify=False)


@pytest.mark.skipif(host != 'cwe-513-vol984.cern.ch' and 'dev' not in host, reason="only run on dev machine")
def test_kerberos_cookie():
    url: str = "https://edms.cern.ch/ws/api/users/current"
    cookie = save_sso_cookie(url)

def test_me():
    assert True
