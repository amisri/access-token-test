class LoginFailed(Exception):
    pass


class UnknownLandingPage(Exception):
    pass


class NoConsent(Exception):
    pass


class ConfigNotFound(Exception):
    pass


class TokenNotFound(Exception):
    pass


class AuthenticationFailed(Exception):
    pass


class TokenExchangeFailed(Exception):
    pass


class RefreshTokenFailed(Exception):
    pass
