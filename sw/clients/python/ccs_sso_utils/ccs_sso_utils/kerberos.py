# Adapted from https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie

import logging
import time
import uuid

from typing import List
from urllib.parse import parse_qs
from http.cookiejar import MozillaCookieJar, Cookie
from ccs_sso_utils import exceptions

# External dependencies
import bs4
import requests
import requests_kerberos


AUTH_HOST = "auth.cern.ch"
OIDC_URL = f"https://{AUTH_HOST}/auth/realms/cern/protocol/openid-connect"
AUTHZ_URL = f"{OIDC_URL}/auth"
TOKEN_URL = f"{OIDC_URL}/token"


def post_session_saml(session, response):
    """Performs the SAML POST request given a session and a successful Keycloak authentication response in SAML"""

    soup_saml = bs4.BeautifulSoup(response.text, features="html.parser")
    action = soup_saml.form.get("action")
    post_key = soup_saml.form.input.get("name")
    post_value = soup_saml.form.input.get("value")
    session.post(action, data={post_key: post_value})


def save_cookies_lwp(cookiejar, filename: str = None):
    """Saves cookies from a requests.Session cookies member into a file in the Netscape format"""

    lwp_cookiejar = MozillaCookieJar()
    for c in cookiejar:
        args = dict(vars(c).items())
        args["rest"] = args["_rest"]
        del args["_rest"]
        if args["expires"] == None:
            args["expires"] = int(time.time()) + 86400
        c = Cookie(**args)
        lwp_cookiejar.set_cookie(c)
    if filename:
        lwp_cookiejar.save(filename, ignore_discard=True)
    return lwp_cookiejar


def save_sso_cookie(url: str, filename: str = None, verify: bool = False):
    """Log in into a URL that redirects to the SSO and save the session cookies"""
    
    session, response = login_with_kerberos(
        url, 
        verify
    )

    if response.status_code == 302:
        redirect_uri = response.headers["Location"]
        session.get(redirect_uri, verify=verify)
    
    return save_cookies_lwp(session.cookies, filename)


def login_with_kerberos(url: str, params: dict = None, verify: bool = False) -> List[str]:
    """Simulates a browser session to log in using SPNEGO protocol"""

    session = requests.Session()
    r_login_page = session.get(url, params=params, verify=verify)
   
    soup = bs4.BeautifulSoup(r_login_page.text, features="html.parser")

    # Try new button
    kerberos_button = soup.find(id="social-kerberos")

    # Try old button
    if not kerberos_button:
        kerberos_button = soup.find(id="zocial-kerberos")

    # No button found
    if not kerberos_button:
        error_message = get_error_message(r_login_page.text)
        if error_message:
            raise exceptions.LoginFailed("Login failed: {}".format(error_message))
        else:
            raise exceptions.UnknownLandingPage("Login failed: Landing page not recognized as the CERN SSO login page.")
    kerberos_path = kerberos_button.get("href")

    r_kerberos_redirect = session.get(f"https://{AUTH_HOST}{kerberos_path}")
    r_kerberos_auth = session.get(
        r_kerberos_redirect.url,
        auth=requests_kerberos.HTTPKerberosAuth(mutual_authentication=requests_kerberos.OPTIONAL),
        allow_redirects=False,
    )
    while (
        r_kerberos_auth.status_code == 302
        and AUTH_HOST in r_kerberos_auth.headers["Location"]
    ):
        r_kerberos_auth = session.get(
            r_kerberos_auth.headers["Location"], allow_redirects=False
        )
    if r_kerberos_auth.status_code != 302:
        if "login-actions/consent" in r_kerberos_auth.text:
            raise exceptions.NoConsent(
                "Login failed: This application requires consent. Please accept it manually before using this tool.")
        error_message = get_error_message(r_kerberos_auth.text)
        if not error_message:
            logging.debug(
                "Not automatically redirected: trying SAML authentication")
            post_session_saml(session, r_kerberos_auth)
        else:
            raise exceptions.LoginFailed("Login failed: {}".format(error_message))
    return session, r_kerberos_auth


def get_error_message(response_html):
    soup_err_page = bs4.BeautifulSoup(response_html, features="html.parser")
    error_message = soup_err_page.find(id="kc-error-message")
    if not error_message:
        return None
    else:
        return error_message.find("p").text


def generate_state() -> str:
    return str(uuid.uuid4()).split("-")[0]


def get_sso_token(redirect_url: str, client_id: str, verify: bool = False) -> str:
    """Get an OIDC token by logging in in the Auhtorization URL using Kerberos

    :param redirect_url: Application or Redirect URL. Required for the OAuth request.
    :param client_id: Client ID of a client with implicit flow enabled.
    :param verify: Verify certificate.
    """
    random_state = generate_state()

    params = {
        'client_id': client_id,
        'response_type': 'code',
        'state': random_state,
        'redirect_uri': redirect_url
    }

    login_response = login_with_kerberos(AUTHZ_URL, params, verify)[1]
    authz_response = parse_qs(login_response.headers["Location"].split("?")[1])

    if authz_response["state"][0] != random_state:
        raise Exception(
            "The authorization response doesn't contain the expected state value.")

    r = requests.post(
        TOKEN_URL,
        data={
            "client_id": client_id,
            "grant_type": "authorization_code",
            "code": authz_response["code"][0],
            "redirect_uri": redirect_url,
        },
    )

    token_response = r.json()
    return token_response["access_token"]
