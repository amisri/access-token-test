from pathlib import Path


CONFIG_PATH = Path.home() / ".oauth2_oidc.json"

AUTH_HOST = "auth.cern.ch"

OIDC_URL = f"https://{AUTH_HOST}/auth/realms/cern/protocol/openid-connect"

AUTHZ_URL = f"{OIDC_URL}/auth"
TOKEN_URL = f"{OIDC_URL}/token"
