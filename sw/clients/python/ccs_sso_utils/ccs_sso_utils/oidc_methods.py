import json
import getpass
import requests

from pathlib import Path
from ccs_sso_utils.constants import CONFIG_PATH, TOKEN_URL
from ccs_sso_utils.exceptions import ConfigNotFound, TokenNotFound, TokenExchangeFailed, RefreshTokenFailed


def get_offline_token(client_id: str, username: str = None):
    """Fetch offline token from the user's ~/.oauth2_oidc.json

    :param client_id: current application client_id 
    :param username: this is fetched from the logged in user, but can
    also be used if the user wants to override this mechanism, for example
    on a service computer.
    :return: OAuth2's offline token as a JWT in a dictionary
    """
    if not username:
        username = getpass.getuser()

    if not CONFIG_PATH.exists():
        raise ConfigNotFound()

    with CONFIG_PATH.open() as f:
        config_dict = json.load(f)

        application_config = config_dict.get(client_id)
        if not application_config:
            raise TokenNotFound(f"No token available for aplication with client id '{client_id}'")

        user_config = application_config.get(username)
        if not user_config:
            raise TokenNotFound(f"No token available for user '{username}'")

        token = user_config.get("token")
        if not token:
            raise TokenNotFound(f"No token available for '{username}'")

        return token


def save_token(client_id: str, client_secret: str, password: str, username: str = None):
    """Create an offline token for the user received in the arguments.
    _Passwords are not stored anywhere in your system_.

    :param client_id: current application client_id 
    :param client_secret: current application client secret
    :param password: user's CERN password
    :param username: this is fetched from the logged in user, but can
    also be used if the user wants to override this mechanism, for example
    on a service computer.
    :return:
    """
    if not username:
        username = getpass.getuser()

    offline_token_request = requests.post(
        TOKEN_URL,
        auth=(client_id, client_secret),
        data={
            "grant_type": "password",
            "username": username,
            "password": password,
            "scope": "openid offline_access"
        }
    )

    if not offline_token_request.ok:
        raise AuthenticationFailed(offline_token_request.text)

    offline_token: dict = offline_token_request.json()
    config = dict()

    if CONFIG_PATH.exists():
        with CONFIG_PATH.open() as f:
            config = json.load(f)

    if client_id not in config:
        config[client_id] = dict()

    config[client_id][username] = {
        "token": offline_token
    }

    with CONFIG_PATH.open("w") as f:
        json.dump(config, f)


def exchange_token(old_token: dict, client_id: str, client_secret: str, target_client_id: str):
    """Exchange an application OAuth2 JWT into a new application OAuth2 JWT.

    :param old_token: old OAuth2 JWT received from CERN auth endpoint
    :param client_id: current application client_id 
    :param client_secret: current application client secret
    :param target_client_id: target application client_id
    :return: new OAuth2 JWT for application with _client_id_
    """
    exchange_request = requests.post(
        TOKEN_URL,
        auth=(client_id, client_secret),
        data={
            "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
            "audience": target_client_id,
            "subject_token": token["access_token"]
        }
    )

    if not exchange_request.ok:
        raise TokenExchangeFailed(exchange_request.text)

    return exchange_request.json()


def refresh_token(old_token: dict, client_id: str, client_secret: str):
    """Refresh a JWT

    :param old_token: old OAuth2 JWT received from CERN auth endpoint
    :param client_id: current application client_id 
    :param client_secret: current application client secret
    :return: refreshed OAuth2 JWT for a new extended period of time
    """
    # Fetch access token based on offline token
    token_request = requests.post(
        TOKEN_URL,
        auth=(client_id, client_secret),
        data={
            "grant_type": "refresh_token",
            "refresh_token": old_token["refresh_token"],
        }
    )

    if not token_request.ok:
        raise RefreshTokenFailed(token_request.text)

    return token_request.json()


def request_token(client_id: str, client_secret: str):
    """Request token by application credentials.

    :param client_id: current application client_id 
    :param client_secret: current application client secret
    :return: refreshed OAuth2 JWT for a new extended period of time
    """
    # Fetch access token based on offline token
    token_request = requests.post(
        TOKEN_URL,
        auth=(client_id, client_secret),
        data={
            "grant_type": "client_credentials",
            "scope": "openid info"
        }
    )

    if not token_request.ok:
        raise AuthenticationFailed(token_request.text)

    return token_request.json()


def access_from_offline_token(client_id: str, client_secret: str, target_client_id: str = None, username: str = None):
    """Fetch access token from offline token.

    :param client_id: current application client_id 
    :param client_secret: current application client secret
    :param target_client_id: target application client_id
    :param username: username of the user
    :return: oauth2 access token to use in your target application
    """
    offline_token = get_offline_token(client_id, username=username)
    token = refresh_token(offline_token, client_id, client_secret)
    if target_client_id:
        return exchange_token(token, client_id, client_secret, target_client_id)['access_token']
    else:
        return token['access_token']


def authentication_helper():
    """ Authentication helper for your application. This function saves an application
    offline token in the current host.

    :return:
    """
    username = input("CERN username: ")
    client_id = input("Current Application Client ID: ")
    client_secret = getpass.getpass("Current Client Secret: ")
    password = getpass.getpass("CERN password: ")
    try:
        save_token(client_id, client_secret, password, username=username)
        print("Your token was saved succesfully!")

    except AuthenticationFailed:
        print("Authentication failed!")
