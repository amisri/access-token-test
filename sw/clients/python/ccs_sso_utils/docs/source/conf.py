import datetime
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))
 
import ccs_sso_utils
 
 
project = "ccs_sso_utils"
 
author = ccs_sso_utils.__authors__
 
version = ccs_sso_utils.__version__
release = ccs_sso_utils.__version__
 
copyright = "{0}, CERN".format(datetime.datetime.now().year)
 
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    'sphinx.ext.napoleon',
]
 
templates_path = ["_templates"]
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
 
 
# -- Options for HTML output -------------------------------------------------
html_theme = "sphinx_rtd_theme"
html_show_sphinx = False
html_show_sourcelink = True
autosummary_generate = True
autosummary_imported_members = True