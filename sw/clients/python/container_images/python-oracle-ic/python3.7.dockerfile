FROM registry.cern.ch/acc/acc-py_cc7:2020.11

RUN yum update && \
    yum install -y oracle-instantclient19.3-basiclite && \
    yum clean all