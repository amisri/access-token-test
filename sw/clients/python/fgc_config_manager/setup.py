from setuptools import setup, find_packages

VERSION = "0.2.12"

with open("README.md", "r") as fh:
        description = fh.read()

requirements = {
    'core': [
        'docopt>=0.6',
        'pyfgc_db>=0.0.1',
        'pyfgc>=1.3.2',
        'pyfgc_rbac>=1.1',
        'pyfgc_statussrv>=1.0'
        'colorlog>=4.1',
        'aiohttp>=3.6.2',
        'sentry_sdk>=1.7.0',
        'colorlog>=6.6.0',
    ],
    'test': [
        'pytest',
        'pytest-asyncio',
    ],
    'doc': [
    ],
}


setup(
    name='fgc_config_manager',
    version=VERSION,
    author='Tsampikos Livisianos',
    author_email='tsampikos.livisianos@cern.ch',
    description='FGC Config Manager',
    long_description=description,
    long_description_content_type='text/markdown',

    python_requires='>=3.7',

    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/fgc_config_manager/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/fgc_config_manager',
    },
    packages=find_packages(),

    install_requires=requirements['core'],
    extras_require={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test'],
    setup_requires=['wheel']

)


# EOF
