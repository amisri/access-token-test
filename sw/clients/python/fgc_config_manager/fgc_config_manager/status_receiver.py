import logging
import asyncio
from typing import Iterable, Mapping

import time
import functools
import concurrent.futures
import queue

import pyfgc
import pyfgc_statussrv

import fgc_config_manager.rbac_manager
import fgc_config_manager.settings as settings

logger = logging.getLogger(__name__)

MIN_ITERATION_TIME = 5
FGC_STATSRV_UPDATE_SECS = 5


class StatusReceiver:
    """Status receiver interface.
    """

    async def run(self):
        """Start running status receiver.
        """
        raise NotImplementedError()

    async def stop(self):
        """Stop running status receiver.
        """
        raise NotImplementedError()


class StatusServerReceiver(StatusReceiver):
    """Read status data from status server.

    This class connects to a status server, and fetches new status data periodically.
    The new data will be forwarded to an asyncio queue.
    """

    def __init__(self, loop: asyncio.AbstractEventLoop, async_queue: asyncio.Queue, gateways: Iterable[str] = None,
                 period=None):
        logger.debug("Setting up status server receiver.")

        self.loop = loop
        self.thread_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.async_queue = async_queue
        self.running = False
        self.gateways = {gw.lower() for gw in gateways} if gateways else None
        self.period = period if period is not None else MIN_ITERATION_TIME

    async def run(self):
        """Run status receiver.
        """
        self.running = True
        fgc_session = await self._get_new_session()
        logger.info("Running status server reader task.")

        while self.running:
            logger.info("Checking status data for devices that need sync")
            try:
                status_data = await self._get_status_data(fgc_session)
            except (OSError, RuntimeError, pyfgc.FgcResponseError):
                logger.error("Failed to get status data from status server. Will try to reconnect.",
                             exc_info=(logger.getEffectiveLevel() == logging.DEBUG))
                await self._close_session(fgc_session)
                fgc_session = await self._get_new_session()
                continue

            await self._check_target_gateways(status_data)

            await asyncio.sleep(self.period)

    async def _get_new_session(self):
        """Create new pyfgc session with the status server.

        Sync session needs to be created using a thread pool executor,
        to avoid blocking the asyncio event loop.
        """
        logger.info("Opening new connection to status server %s.",
                    settings.basic.STATUS_SERVER_NAME)

        token = await fgc_config_manager.rbac_manager.get_token()
        return await self.loop.run_in_executor(self.thread_executor,
                                               functools.partial(pyfgc.connect, settings.basic.STATUS_SERVER_NAME,
                                                                 rbac_token=token, name_file=settings.basic.NAME_FILE))

    async def _close_session(self, session):
        """Close pyfgc session with the status server.

        Sync session needs to be destroyed using a thread pool executor,
        to avoid blocking the asyncio event loop.
        """
        logger.info("Closing connection to status server %s.", settings.basic.STATUS_SERVER_NAME)
        await self.loop.run_in_executor(self.thread_executor, pyfgc.disconnect, session)

    async def _get_status_data(self, fgc_session=None):
        """Get latest data from status server.

        Request needs to use a thread pool executor,
        to avoid blocking the asyncio event loop.
        """
        status_data = await self.loop.run_in_executor(self.thread_executor,
                                                      functools.partial(pyfgc_statussrv.get_status_all,
                                                                        fgc_session=fgc_session))
        return status_data

    async def _check_target_gateways(self, gw_status):
        """Check if the status data of the gateway are still valid/fresh"""

        for gateway in self.gateways:
            if gateway not in gw_status:
                logger.warning(f"Error getting status data from status server for gateway {gateway}")
                continue
            status_data = gw_status[gateway]
            if status_data['recv_time_sec'] >= (time.time() - ((FGC_STATSRV_UPDATE_SECS * 2) + 1)):
                logger.debug(f"Status data fresh for {gateway}")
                await self._add_to_queue(gateway, status_data)

    async def _add_to_queue(self, gateway, status_data):
        """Extract and add gateway data to queue.
        """
        gw_status = dict()
        gw_status["gateway"] = gateway
        gw_status['recv_time_sec'] = status_data['recv_time_sec']
        gw_status["channels"] = status_data["channels"]
        await self.async_queue.put(gw_status)

    async def stop(self):
        """Stop running status receiver.
        """
        logger.info("Stopping status server task.")
        self.running = False


class StatusMonitorPort(StatusReceiver):
    """Receive status data on UDP port.

    This class opens a UDP port and waits for status data sent by the gateways.
    The new data will be forwarded to an asyncio queue.
    """

    def __init__(self, async_queue: asyncio.Queue, loop: asyncio.AbstractEventLoop,
                 port: int, gateways: Iterable[str] = None):
        """Init method.

        Args:
            async_queue (asyncio.Queue): Queue to fill with new status data.
            loop (asyncio.AbstractEventLoop): Current event loop.
            port (int): Status data receiving UDP port.
            gateways (Iterable[str], optional): Read only these gateways. Defaults to None.
        """
        self.async_queue = async_queue
        self.loop = loop
        self.port = port
        self.thread_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.sync_queue = queue.Queue()
        self.monitor = None
        self.running = False
        self.running_fut = None
        self.gateways = {gw.lower() for gw in gateways} if gateways else None

    def monitor_callback(self, data_dict: Mapping[str, dict], _data_list: Iterable[dict],
                         gateway: str, timestamp: float):
        """Callback for new status data.

        When new UDP published data is received from a gateway.
        Since the callback is a function, not a coroutine, this will send the data to a sync queue.

        Args:
            _data_dict (Mapping[str, dict]): Received status data in dictionary format.
            data_list (Iterable[dict]): Received status data in list format [0..64]. Ignored.
            gateway (str): Gateway name.
            timestamp (float): Received data timestamp.
        """
        if self.gateways is None or gateway in self.gateways:
            if timestamp >= (time.time() - ((FGC_STATSRV_UPDATE_SECS * 2) + 1)):
                gw_status = dict()
                gw_status["gateway"] = gateway
                gw_status['recv_time_sec'] = timestamp
                gw_status["channels"] = {v['CHANNEL']: v for k, v in data_dict.items()}
                self.sync_queue.put(gw_status)

    def monitor_callback_err(self, err: Exception, gateway: str, timestamp: float):
        """Callback for monitor errors.

        When the monitor callback catches an exception.
        This allows the user log those exceptions.

        Args:
            err (Exception): Exception raised by the monitor.
            gateway (str): Gateway name.
            timestamp (float): Exception timestamp.
        """
        logger.error("Monitor error caused by %s at %f s: %s", gateway, timestamp, err)

    async def _wait_for_status_data(self):
        """Get new data from the sync queue.

        This allows the data to be safely passed from the callback to the asyncio event loop.
        """
        return await self.loop.run_in_executor(self.thread_executor, self.sync_queue.get)

    async def _pub_data_polling(self):
        """Loop while waiting for new data.
        """
        self.running = True
        self.monitor = pyfgc.monitor_port(self.monitor_callback, self.port,
                                          callback_err=self.monitor_callback_err, name_file=settings.basic.NAME_FILE)
        self.monitor.start()

        logger.info("Waiting for status data on port %d.", self.port)

        while self.running:
            status_data = await self._wait_for_status_data()
            if status_data:
                logger.debug("Received new status data for %s", status_data['gateway'])
                await self.async_queue.put(status_data)

    async def run(self):
        """Start running status receiver.
        """
        try:
            logger.info("Running status monitor port task.")
            await self._pub_data_polling()
        except Exception:
            self.running = False
            logger.exception("Status monitor port task failed unexpectedly.")
            raise

    async def stop(self):
        """Stop running status receiver.
        """
        logger.info("Stopping status monitor port task.")
        self.running = False
        self.monitor.stop()
        self.sync_queue.put(None)  # To unlock queue

# EOF
