import asyncio
import logging
import time

from fgc_config_manager.sync_gateway import SyncGateway

logger = logging.getLogger(__name__)

FGC_STATSRV_UPDATE_SECS = 5


class SyncManager:
    def __init__(self, loop: asyncio.AbstractEventLoop, input_queue: asyncio.Queue):
        self.loop = loop
        self.input_queue = input_queue
        self.running = False
        self.active_gateway = dict()

    async def run(self):
        """Run sync manager."""

        try:
            logger.info("Running sync manager task.")
            self.running = True

            while self.running:

                self._clean_done()
                gw_data = await self.input_queue.get()

                # If poison pill was received, check self.running again
                if gw_data is None:
                    continue

                if self._is_valid_gateway(gw_data):
                    self.active_gateway[gw_data['gateway']] = self.loop.create_task(
                        SyncGateway(self.loop, gw_data).check_devices())

        except Exception:
            self.running = False
            logger.exception("Sync manager task failed unexpectedly.")
            raise
        logger.info("Exiting sync manager task")

    def _is_valid_gateway(self, gw_data):
        """Check if the gateway status data are fresh"""
        if gw_data['recv_time_sec'] >= (time.time() - ((FGC_STATSRV_UPDATE_SECS * 2) + 1)) \
                and gw_data['gateway'] not in self.active_gateway:
            return True
        return False

    def _clean_done(self):
        """Remove the task that have finished"""

        remove = [k for k, v in self.active_gateway.items() if v.done()]
        for k in remove:
            del self.active_gateway[k]

    async def stop(self):
        """Stop running sync manager."""

        logger.info("Stopping sync manager task.")
        self.running = False
        await self.input_queue.put(None)  # Poison pill


# EOF
