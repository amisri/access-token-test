import asyncio
import logging

import fgc_config_manager.settings as settings
from fgc_config_manager.status_receiver import StatusServerReceiver, StatusMonitorPort
from fgc_config_manager.sync_manager import SyncManager
import fgc_config_manager.rbac_manager
import fgc_config_manager.lsa_handler

logger = logging.getLogger(__name__)


class FgcConfigManager:

    def __init__(self, gateways=None, recv_mode=None):
        self.loop = asyncio.get_event_loop()
        self.gateways = gateways
        self.term_signal = None
        self.recv_mode = recv_mode

    async def run(self):
        self.term_signal = asyncio.Event()
        queue_status = asyncio.Queue()

        # Log Name file
        if settings.basic.NAME_FILE:
            logger.info(f"Using name file from url/path: {settings.basic.NAME_FILE}")

        # Start RBAC manager
        if settings.basic.USE_RBAC:
            logger.info("Use RBAC token.")
            rbac_manager = fgc_config_manager.rbac_manager.RbacManager()
            await rbac_manager.start()
        else:
            rbac_manager = None

        if settings.basic.INCA_ENDPOINT:
            logger.info("Use LSA service.")
            lsa_handler = fgc_config_manager.lsa_handler.LsaHandler()
            lsa_handler_fut = asyncio.ensure_future(lsa_handler.run())
        else:
            lsa_handler = None
            lsa_handler_fut = None

        # Create modules
        if self.recv_mode is settings.ReadMode.STATUS_SERVER:
            status_recv = StatusServerReceiver(self.loop, queue_status, self.gateways)
        elif self.recv_mode is settings.ReadMode.STATUS_MONITOR:
            status_recv = StatusMonitorPort(queue_status, self.loop, settings.basic.STATUS_MONITOR_PORT,
                                            gateways=self.gateways)
        else:
            raise RuntimeError("Unable to start status receiver.")

        sync_manager = SyncManager(self.loop, queue_status)

        # Run modules
        sync_manager_fut = asyncio.ensure_future(sync_manager.run())
        status_recv_fut = asyncio.ensure_future(status_recv.run())

        term_fut = asyncio.ensure_future(self.term_signal.wait())
        wait_tasks = {term_fut, sync_manager_fut, status_recv_fut}
        if lsa_handler:
            wait_tasks.add(lsa_handler_fut)
        done, _ = await asyncio.wait(wait_tasks, return_when=asyncio.FIRST_COMPLETED)

        if term_fut not in done:
            logger.error("Important task failed. Aborting execution.")
            for fut in done:
                # Try to re-raise the exception that triggered the abortion
                fut.result()

        logger.info("Stopping running tasks...")

        if lsa_handler:
            await lsa_handler.stop()
            await lsa_handler_fut

        if rbac_manager:
            await rbac_manager.stop()

        await status_recv.stop()
        await sync_manager.stop()

        logger.info("Waiting for tasks to finish...")

        await status_recv_fut
        await sync_manager_fut

        logger.info("FGC Config Manager is closed.")

    def stop(self):
        """Stop the FGC Config Manager.
        """
        logger.info("Closing FGC Config Manager...")
        try:
            self.loop.call_soon_threadsafe(self.term_signal.set)
        except AttributeError as err:
            raise AttributeError("Event loop has not been started yet.") from err

# EOF
