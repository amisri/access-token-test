import logging
import asyncio

import pyfgc

import fgc_config_manager.rbac_manager
import fgc_config_manager.settings as settings
from fgc_config_manager.synchroniser import DbSynchroniser
from fgc_config_manager.synchroniser import DbCalSynchroniser
from fgc_config_manager.synchroniser import FgcSynchroniser
from fgc_config_manager.synchroniser import BarcodeSynchroniser
from fgc_config_manager.utils import DeviceLogAdapter


class SyncGateway:

    def __init__(self, loop: asyncio.AbstractEventLoop, status_data: dict):
        self.loop = loop
        self.gateway = status_data['gateway']
        self.channels = status_data['channels']
        self.rbac_token = None

        self.logger = DeviceLogAdapter(logging.getLogger(__name__ + "." + self.gateway.upper()), {"device": self.gateway})

    async def check_devices(self):
        """Check all the devices of the gateway to find if they need sync"""

        try:
            self.logger.debug(f"Checking gateway for FGCs that need sync")
            self.rbac_token = await fgc_config_manager.rbac_manager.get_token()
            devices_for_sync, sync_barcode = self._get_devices_for_sync()
            if not devices_for_sync and not sync_barcode:
                return

            sync_db, sync_db_cal, sync_fgc = await self._get_devices_per_mode(devices_for_sync)

            fgc_sync_tasks = []
            if sync_db or sync_db_cal or sync_fgc or sync_barcode:
                self.logger.info(f"{len(sync_db)} SYNC_DB device(s), {len(sync_db_cal)} SYNC_DB_CAL device(s), "
                                 f"{len(sync_fgc)} SYNC_FGC device(s), {len(sync_barcode)} SYNC_BARCODE devices(s)...")
                for device in sync_db:
                    fgc_sync_tasks.append(self.loop.create_task(DbSynchroniser(self.rbac_token, device['NAME']).sync()))
                for device in sync_db_cal:
                    fgc_sync_tasks.append(
                        self.loop.create_task(DbCalSynchroniser(self.rbac_token, device['NAME']).sync()))
                for device in sync_fgc:
                    fgc_sync_tasks.append(
                        self.loop.create_task(FgcSynchroniser(self.rbac_token, device['NAME']).sync()))
                for device in sync_barcode:
                    fgc_sync_tasks.append(
                        self.loop.create_task(BarcodeSynchroniser(self.rbac_token, device['NAME']).sync()))

                await asyncio.gather(*fgc_sync_tasks, return_exceptions=True)
                self.logger.info(f"Sync Gateway done {len(fgc_sync_tasks)} tasks")
        except Exception:
            self.logger.exception("Sync Gateway task failed unexpectedly.")

    def _get_devices_for_sync(self):
        """Get all the devices with the SYNC_PLEASE flag"""

        devices_for_sync = []
        devices_for_barcode = []
        for channel, device in self.channels.items():
            if channel == 0:
                continue
            if 'DATA_VALID' not in device['DATA_STATUS'] or 'NAME' not in device or 'ST_UNLATCHED' not in device:
                self.logger.debug(f"channel {channel} is not valid")
                continue
            self.logger.debug(f"channel {channel} with name {device['NAME']} is valid")
            if 'SYNC_BARCODE' in device['ST_UNLATCHED']:
                self.logger.debug(f"channel {channel} with name {device['NAME']} needs barcode")
                devices_for_barcode.append(device)
            elif 'SYNC_PLEASE' in device['ST_UNLATCHED']:
                self.logger.debug(f"channel {channel} with name {device['NAME']} needs sync")
                devices_for_sync.append(device)
        return devices_for_sync, devices_for_barcode

    async def _get_devices_per_mode(self, devices_for_sync: list):
        """Query the CONFIG.MODE of the devices to find what kind of sync they need"""

        sync_db = []
        sync_db_cal = []
        sync_fgc = []
        for device in devices_for_sync:
            try:
                response = await pyfgc.async_get(device['NAME'], "CONFIG.MODE", rbac_token=self.rbac_token,
                                                 name_file=settings.basic.NAME_FILE)
                if response.err_code != "" and response.err_code in ['64', '65']:
                    self.logger.warning(
                        f"FGC {device['NAME']} thinks the RBAC token has expired before its time. Refreshing token...")
                    self.rbac_token = await fgc_config_manager.rbac_manager.get_token(refresh_token=True)
                    response = await pyfgc.async_get(device['NAME'], "CONFIG.MODE", rbac_token=self.rbac_token,
                                                     name_file=settings.basic.NAME_FILE)
            except asyncio.TimeoutError:
                self.logger.warning(f"Request to read CONFIG.MODE from FGC {device['NAME']} failed with timeout")
                continue
            if response.err_code != "":
                self.logger.error(f"Failed to read CONFIG.MODE from FGC {device['NAME']}: {response.err_msg}")
                continue
            if response.value == "SYNC_DB":
                sync_db.append(device)
            elif response.value == "SYNC_DB_CAL":
                sync_db_cal.append(device)
            elif response.value == "SYNC_FGC":
                sync_fgc.append(device)
        return sync_db, sync_db_cal, sync_fgc

# EOF
