"""FGC Config Manager settings module.

It parses the config.ini file and sets up the global settings.
Default values are provided, when file is not available.
"""
import json

import sys
import enum
import configparser
import types
import pathlib
import logging as logging_m

HOME = str(pathlib.Path.home())


# Basic settings

class ReadMode(enum.Enum):
    """Status read mode.
    """
    STATUS_SERVER = enum.auto()
    STATUS_MONITOR = enum.auto()


BASIC_DICT = {
    "STATUS_READ_MODE": ReadMode.STATUS_SERVER,
    "STATUS_SERVER_PERIOD": 1.0,
    "STATUS_SERVER_NAME": "FGC_STATUS",
    "STATUS_MONITOR_PORT": 0,
    "FILTER_GROUPS": None,
    "FILTER_GATEWAYS": None,
    "NAME_FILE": None,
    "USE_RBAC": False,
    "INCA_ENDPOINT": None,
    "SENTRY_URL": None,
    "SENTRY_ENV": None,
}


# Logging settings

class LoggingHandlers(enum.Flag):
    """Active logging handlers.
    """
    NONE = 0
    SYSLOG = enum.auto()
    FILE = enum.auto()
    TERMINAL = enum.auto()
    ALL = SYSLOG | FILE | TERMINAL


LOGGING_DICT = {
    "LOGGING_HANDLERS": LoggingHandlers.TERMINAL,
    "LOGGING_LEVEL_SYSLOG": None,
    "LOGGING_LEVEL_FILE": None,
    "LOGGING_LEVEL_TERMINAL": None,
    "LOGGING_FOLDER_PATH": HOME + "/projects/fgc/sw/clients/python/fgc_config_manager/logs/",
    "LOGGING_FILE_PATH": HOME + "/projects/fgc/sw/clients/python/fgc_config_manager/logs/fgc_config_manager.log",
    "LOGGING_FILE_SIZE": 10 * 1024 * 1024,
    "LOGGING_FILE_BACKUP_COUNT": 5
}

# Database settings

DATABASE_DICT = {
    "DRIVER": None,
    "USERNAME": None,
    "PASSWORD": None,
    "HOST": None,
    "PORT": None,
    "DATABASE": None
}

# Namespaces, to be overriten by Module object

basic = None
logging = None
database = None

# Namespaces

BASIC_NS = types.SimpleNamespace(**BASIC_DICT)
LOGGING_NS = types.SimpleNamespace(**LOGGING_DICT)
DATABASE_NS = types.SimpleNamespace(**DATABASE_DICT)


# Parse list

def _parse_list(input_str: str) -> list:
    """Receives a list of items, either directly or through a file.
    Parses them into a list.

    Args:
        input (str): List of items, comma-separated, or path to file.

    Returns:
        list: List of items
    """
    if pathlib.Path(input_str).is_file():
        with open(input_str) as filter_file:
            lines = filter(lambda x: not x.strip().startswith("#"), filter_file.readlines())
            contents = [
                s.strip() for line in lines for s in line.split(",")
            ]
    else:
        contents = [s.strip() for s in input_str.split(",")]

    filtered_contents = list(filter(None, contents))
    return filtered_contents if filtered_contents else None


# Update settings

def update_settings(config_file_path: str):
    """Update settings from config file.

    Args:
        config_file_path (str): Path to config file.
    """

    global BASIC_NS
    global LOGGING_NS
    global DATABASE_NS

    config = configparser.SafeConfigParser()
    config.read(config_file_path)

    # Basic data
    basic_data = config["BASIC"]

    # Basic data - status read mode
    status_read_mode_str = basic_data.get("status_read_mode")
    try:
        status_read_mode = ReadMode[status_read_mode_str.upper()]
    except KeyError:
        raise ValueError("status_read_mode value not valid.")

    # Basic data - status server parameters
    status_server_period_str = basic_data.get("status_server_period", "")
    try:
        status_server_period = float(status_server_period_str)
    except ValueError:
        status_server_period = None
    status_server_name = basic_data.get("status_server_name", "")

    # Basic data - status monitor parameters
    monitoring_port_str = basic_data.get("status_monitor_port", "")
    try:
        status_monitor_port = int(monitoring_port_str)
    except ValueError:
        status_monitor_port = None

    # Basic data - group, gateway and device listing
    filter_groups_str = basic_data.get("filter_groups", "")
    filter_groups = _parse_list(filter_groups_str)

    filter_gateways_str = basic_data.get("filter_gateways", "")
    filter_gateways = _parse_list(filter_gateways_str)

    valid_lists_count = sum(
        bool(x) for x in [filter_groups, filter_gateways]
    )

    if valid_lists_count == 0:
        raise ValueError("No groups or gateways are listed for monitoring.")
    elif valid_lists_count > 1:
        raise ValueError("List only one of these: groups or gateways.")

    # Basic data - Name file
    name_file = basic_data.get("name_file", None)

    # Basic data - rbac token
    use_rbac_str = basic_data.get("use_rbac", "")
    if use_rbac_str.upper() in ("YES", "TRUE", "1"):
        use_rbac = True
    elif use_rbac_str.upper() in ("NO", "FALSE", "0", ""):
        use_rbac = False
    else:
        raise ValueError(f"use_rbac value {use_rbac_str} not valid.")

    # Basic data - InCA endpoint
    inca_endpoint = basic_data.get("inca_endpoint", None)

    # Basic data - Sentry
    sentry_url = basic_data.get("sentry_url", None)
    sentry_env = basic_data.get("sentry_env", None)

    BASIC_DICT["STATUS_READ_MODE"] = status_read_mode
    BASIC_DICT["STATUS_SERVER_PERIOD"] = status_server_period
    BASIC_DICT["STATUS_SERV)ER_NAME"] = status_server_name
    BASIC_DICT["STATUS_MONITOR_PORT"] = status_monitor_port
    BASIC_DICT["FILTER_GROUPS"] = filter_groups
    BASIC_DICT["FILTER_GATEWAYS"] = filter_gateways
    BASIC_DICT["NAME_FILE"] = name_file
    BASIC_DICT["USE_RBAC"] = use_rbac
    BASIC_DICT["INCA_ENDPOINT"] = inca_endpoint
    BASIC_DICT["SENTRY_URL"] = sentry_url
    BASIC_DICT["SENTRY_ENV"] = sentry_env

    # Logging data
    logging_data = config["LOGGING"]

    # Logging data - message logging outputs
    logging_handlers_str = logging_data.get("logging_handlers")
    logging_handlers_list = [
        s.strip() for s in logging_handlers_str.split(",")
    ] if logging_handlers_str else []

    logging_handlers = LoggingHandlers.NONE
    for handler in logging_handlers_list:
        try:
            logging_handlers |= LoggingHandlers[handler.upper()]
        except KeyError:
            raise ValueError(f"logging_handlers value {handler} not valid.")

    # Logging data - message logging levels
    logging_level_syslog_str = logging_data.get("logging_level_syslog", "WARNING")
    try:
        logging_level_syslog = getattr(logging_m, logging_level_syslog_str.upper())
    except AttributeError:
        raise ValueError(f"logging_level_syslog value {logging_level_syslog_str} not valid.")

    logging_level_file_str = logging_data.get("logging_level_file", "INFO")
    try:
        logging_level_file = getattr(logging_m, logging_level_file_str.upper())
    except AttributeError:
        raise ValueError(f"logging_level_file value {logging_level_file_str} not valid.")

    logging_level_terminal_str = logging_data.get("logging_level_terminal", "INFO")
    try:
        logging_level_terminal = getattr(logging_m, logging_level_terminal_str.upper())
    except AttributeError:
        raise ValueError(f"logging_level_terminal value {logging_level_terminal_str} not valid.")

    # Logging data - logging file path
    logging_folder_path = logging_data.get("logging_folder_path", "")
    logging_file_path = logging_data.get("logging_file_path", "")

    logging_file_size = int(logging_data.get("logging_file_size", "10")) * 1024 * 1024
    logging_file_backup_count = int(logging_data.get("logging_file_backup_count", "5"))

    LOGGING_DICT["LOGGING_HANDLERS"] = logging_handlers
    LOGGING_DICT["LOGGING_LEVEL_SYSLOG"] = logging_level_syslog
    LOGGING_DICT["LOGGING_LEVEL_FILE"] = logging_level_file
    LOGGING_DICT["LOGGING_LEVEL_TERMINAL"] = logging_level_terminal
    LOGGING_DICT["LOGGING_FOLDER_PATH"] = logging_folder_path
    LOGGING_DICT["LOGGING_FILE_PATH"] = logging_file_path
    LOGGING_DICT["LOGGING_FILE_SIZE"] = logging_file_size
    LOGGING_DICT["LOGGING_FILE_BACKUP_COUNT"] = logging_file_backup_count

    # Database data
    database_data = config["DATABASE"]
    try:
        with open(database_data["config_file"]) as json_file:
            db_config = json.load(json_file)
            DATABASE_DICT["DRIVER"] = db_config.get("driver")
            DATABASE_DICT["USERNAME"] = db_config.get("username")
            DATABASE_DICT["PASSWORD"] = db_config.get("password")
            DATABASE_DICT["HOST"] = db_config.get("host")
            DATABASE_DICT["PORT"] = db_config.get("port")
            DATABASE_DICT["DATABASE"] = db_config.get("database")
    except AttributeError:
        raise ValueError(f"logging_level_terminal value {logging_level_terminal_str} not valid.")

    # Update namespaces

    BASIC_NS = types.SimpleNamespace(**BASIC_DICT)
    LOGGING_NS = types.SimpleNamespace(**LOGGING_DICT)
    DATABASE_NS = types.SimpleNamespace(**DATABASE_DICT)


# Wrap reading of constants, guaranteeing that value is obtained by reference

class Module:
    """Module will be used as the new namespace.
    """

    def __init__(self, module):
        self.__module = module

    def __getattr__(self, name):
        return getattr(self.__module, name)

    @property
    def basic(self):
        """Get BASIC settings.
        """
        return BASIC_NS

    @property
    def logging(self):
        """Get message logging settings.
        """
        return LOGGING_NS

    @property
    def database(self):
        """Get database settings.
        """
        return DATABASE_NS


# Add references to constants to module

sys.modules[__name__] = Module(sys.modules[__name__])

# EOF
