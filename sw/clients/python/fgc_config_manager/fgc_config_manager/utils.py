import logging


class Singleton(type):
    """Singleton class implementation.
    """
    instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.instances:
            cls.instances[cls] = super().__call__(*args, **kwargs)
        return cls.instances[cls]


class DeviceLogAdapter(logging.LoggerAdapter):
    """Will prepend the device name to the log message.
    This gives a very usefull field to message logging.
    """
    def process(self, msg, kwargs):
        device = self.extra["device"]
        return f"[{device}] {msg}", kwargs


# EOF
