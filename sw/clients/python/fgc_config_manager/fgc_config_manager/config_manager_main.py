#!/usr/bin/python3

"""FGC Config Manager

Usage:
    fgc_config_manager [-v] <config_file>
    fgc_config_manager -h | --help

Options:
    -h,--help                            Show this help.
    -v,--verbosity                       Increase output verbosity [default: INFO].
"""

import signal
import os
import docopt
import asyncio
import pathlib
import logging.handlers
import syslog
import colorlog
import sys
import sentry_sdk

import pyfgc_name
from pyfgc_db.core import FgcDb, FgcDbConfigError

import fgc_config_manager.settings as settings
from fgc_config_manager.config_manager import FgcConfigManager

ROOT_NAME = __name__.split(".")[0]
logger = logging.getLogger(__name__)

# Format for all project message logs
FILELOG_FORMAT = "[%(asctime)s] %(levelname)s (%(module)s) %(message)s"
TERMLOG_FORMAT = "[%(asctime)s] %(log_color)s%(levelname)s%(reset)s %(blue)s(%(module)s)%(reset)s %(message)s"
SYSLOG_FORMAT = "%(levelname)s (%(module)s) %(message)s"


def configure_logging(verbosity: bool):
    """Configure python message logging.

    Logging messages are printed both to the terminal and to a rotating file.

    Args:
        verbosity (bool): Verbose logging enabled (file and terminal only).
    """
    default_severity = logging.DEBUG if verbosity else logging.INFO

    root_logger = logging.getLogger(ROOT_NAME)
    root_logger.setLevel(default_severity)

    logging_handlers = settings.logging.LOGGING_HANDLERS

    # Setup syslog

    class NewSysLogHandler(logging.Handler):

        DEFAULT_PRIO = [
            logging.CRITICAL,
            logging.ERROR,
            logging.WARNING,
            logging.INFO,
            logging.DEBUG,
        ]

        PRIO_TABLE = {
            logging.CRITICAL: syslog.LOG_CRIT,
            logging.ERROR: syslog.LOG_ERR,
            logging.WARNING: syslog.LOG_WARNING,
            logging.INFO: syslog.LOG_INFO,
            logging.DEBUG: syslog.LOG_DEBUG,
        }

        def _get_syslog_level(self, levelno):

            try:
                return self.PRIO_TABLE[levelno]
            except KeyError:
                for ref_priority in self.DEFAULT_PRIO:
                    if levelno >= ref_priority:
                        # Cache new logging level and return
                        self.PRIO_TABLE[levelno] = self.PRIO_TABLE[ref_priority]
                        return self.PRIO_TABLE[levelno]
            raise ValueError(f"Logging levelno invalid {levelno}.")

        def emit(self, record):
            log_entry = self.format(record)
            log_level = self._get_syslog_level(record.levelno)
            syslog.syslog(log_level, log_entry)

    if logging_handlers & settings.LoggingHandlers.SYSLOG:
        syslog.openlog(ident=ROOT_NAME, logoption=syslog.LOG_PID)
        sh = NewSysLogHandler()
        sh.setLevel(settings.logging.LOGGING_LEVEL_SYSLOG)
        sh_formatter = logging.Formatter(SYSLOG_FORMAT)
        sh.setFormatter(sh_formatter)
        root_logger.addHandler(sh)

    # Setup rotating file for logs.

    if logging_handlers & settings.LoggingHandlers.FILE:
        fh = logging.handlers.RotatingFileHandler(settings.logging.LOGGING_FILE_PATH,
                                                  maxBytes=settings.logging.LOGGING_FILE_SIZE,
                                                  backupCount=settings.logging.LOGGING_FILE_BACKUP_COUNT)
        fh.setLevel(logging.DEBUG if verbosity else settings.logging.LOGGING_LEVEL_FILE)
        fh_formatter = logging.Formatter(FILELOG_FORMAT)
        fh.setFormatter(fh_formatter)
        root_logger.addHandler(fh)

    # Setup colored logs to print to the terminal.

    if logging_handlers & settings.LoggingHandlers.TERMINAL:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG if verbosity else settings.logging.LOGGING_LEVEL_TERMINAL)
        ch_formatter = colorlog.ColoredFormatter(TERMLOG_FORMAT)
        ch.setFormatter(ch_formatter)
        root_logger.addHandler(ch)


def get_gateways_from_groups(group_list: list):
    """Given a list of groups, get the corresponding gateways.

    Args:
        group_list (list): List of groups.

    Return:
        dict: Gateways per group.
    """
    pyfgc_name.read_name_file()
    pyfgc_name.read_group_file()
    pyfgc_name.read_subdevice_file()

    groups_ref = pyfgc_name.groups
    results_dict = {g.upper(): set() for g in group_list}

    def get_all_gateways(node: dict, results: set):
        for key, val in node.items():
            if key == "gateways":
                results.update(val)
            elif isinstance(val, dict):
                get_all_gateways(val, results)

    def search_groups(node: dict, in_out_data: dict):
        for key, val in node.items():
            if key.upper() in in_out_data:
                gateways_set = set()
                get_all_gateways(val, gateways_set)
                in_out_data[key.upper()].update(gateways_set)
            elif isinstance(val, dict):
                search_groups(val, in_out_data)

    # Search for gateways on each group
    search_groups(groups_ref, results_dict)
    return {g: results_dict[g.upper()] for g in group_list}


def setup_device_logging():
    for device in pyfgc_name.devices:
        if pyfgc_name.devices[device]['channel'] == 0:
            l = logging.getLogger(ROOT_NAME + ".sync_gateway." + device)
        else:
            l = logging.getLogger(ROOT_NAME + ".synchroniser.synchroniser." + device)
            # l.setLevel(settings.logging.LOGGING_LEVEL_FILE)

        # create gateway folder if not exists
        gw_folder = f"{settings.logging.LOGGING_FOLDER_PATH}/{pyfgc_name.devices[device]['gateway']}"
        if not os.path.exists(gw_folder):
            os.mkdir(gw_folder)

        fh = logging.handlers.RotatingFileHandler(f"{gw_folder}/{device}.log",
                                                  maxBytes=settings.logging.LOGGING_FILE_SIZE,
                                                  backupCount=settings.logging.LOGGING_FILE_BACKUP_COUNT)
        fh.setLevel(settings.logging.LOGGING_LEVEL_FILE)
        fh_formatter = logging.Formatter(FILELOG_FORMAT)
        fh.setFormatter(fh_formatter)

        l.addHandler(fh)


def main():
    """Main function executing the FGC Config Manager"""
    args = docopt.docopt(__doc__)

    config_file = args["<config_file>"]
    if not pathlib.Path(config_file).is_file():
        print(f"ERROR: File does not exist: {config_file}")
        exit(1)

    settings.update_settings(config_file)
    configure_logging(args["--verbosity"])

    if settings.basic.SENTRY_URL:
        sentry_sdk.init(settings.basic.SENTRY_URL, environment=settings.basic.SENTRY_ENV)

    logger.info("---- FGC Config Manager main program ----")
    logger.debug("Debug message logging enabled.")  # Will only run in debug.

    main_pid = os.getpid()
    running = False

    # Signal handler for ordered termination

    def signal_handler(_sig, _frame):
        nonlocal running
        if main_pid == os.getpid() and running:
            # logger.warning("Received signal to close server.")
            config_manager.stop()
            running = False

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # get gateways
    if settings.basic.FILTER_GATEWAYS:
        gateways = settings.basic.FILTER_GATEWAYS
    else:
        groups_dict = get_gateways_from_groups(settings.basic.FILTER_GROUPS)
        results = set()
        for group, gateways in groups_dict.items():
            if gateways:
                results.update(gateways)
                logger.info("Monitoring group %s: %s.", group, ", ".join(gateways))
        gateways = results

    setup_device_logging()

    # setup database
    try:
        logger.debug(
            f"Trying established connection with database {settings.database.DRIVER}://{settings.database.USERNAME}"
            "@{settings.database.HOST}:{settings.database.PORT}/{settings.database.DATABASE}")
        FgcDb.config_db(settings.database.DRIVER, settings.database.USERNAME, settings.database.PASSWORD,
                        settings.database.HOST, settings.database.PORT, settings.database.DATABASE)
        logger.debug(f"Connected to database successfully")
    except FgcDbConfigError as err:
        logger.critical(f"Database configuration failed: {err}")
        sys.exit(f"Database configuration failed: {err}")

    running = True
    config_manager = FgcConfigManager(gateways, settings.basic.STATUS_READ_MODE)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(config_manager.run())
    except Exception as err:
        logger.exception(f"Something failed: {err}")
        raise

    logger.info("---- FGC Config Manager closed ----")


if __name__ == "__main__":
    main()

# EOF
