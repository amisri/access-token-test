import logging

from collections import OrderedDict

import pyfgc_db.models as db_models
from pyfgc_db.core import FgcDb

from fgc_config_manager.synchroniser.synchroniser import Synchroniser

module_logger = logging.getLogger(__name__)


class BarcodeSynchroniser(Synchroniser):

    async def _sync(self):
        self.logger.info(f"\tSynchronising barcodes of device {self.device}")

        dallas_ids = await self._get_dallas_ids()
        await self._set_barcodes(dallas_ids)

        self.logger.info(f"\tSynchronisation of barcodes of device {self.device} completed")

    async def _get_dallas_ids(self):
        self.logger.info("\tFetching dallas_ids from the FGC")
        dallas_branches = ["VS.A", "VS.B", "MEAS.A", "MEAS.B", "LOCAL", "CRATE"]
        dallas_ids = OrderedDict()

        for branch in dallas_branches:
            branch_dallas_ids = []
            response = await self.fgc_session.get("FGC.ID." + branch)
            if response.err_code == "" and response.value != "":
                for barcode in response.value.split(","):
                    branch_dallas_ids.append(barcode.split(":")[0])
            if branch_dallas_ids:
                self.logger.info(f"\t\tFGC.ID.{branch} = {','.join(branch_dallas_ids)}")
                dallas_ids[branch] = branch_dallas_ids
        return dallas_ids

    async def _set_barcodes(self, dallas_ids):
        self.logger.info("\tSetting barcodes on the FGC")
        with FgcDb.session() as db_session:
            for branch, branch_dallas_ids in dallas_ids.items():
                branch_barcodes = []
                for dallas_id in branch_dallas_ids:
                    mapping = db_session.query(db_models.ComponentMapping).filter(
                        db_models.ComponentMapping.chip_id == dallas_id).one_or_none()
                    if mapping:
                        branch_barcodes.append(mapping.component_mtf_id)
                    else:
                        branch_barcodes.append("")
                if branch_barcodes and (len(set(branch_barcodes)) > 1 or branch_barcodes[0] != ""):
                    while branch_barcodes[-1] == "":
                        branch_barcodes.pop()
                    self.logger.info(f"\t\tFGC.ID.{branch} = {','.join(branch_barcodes)}")
                    response = await self.fgc_session.set("FGC.ID." + branch, ",".join(branch_barcodes))
                    if response.err_code != "":
                        self.logger.error(
                            f"\t\tFailed to set FGC.ID.{branch} to {','.join(branch_barcodes)}: {response.err_msg}")
        response = await self.fgc_session.set("FGC.ID", "RESET")
        if response.err_code != "":
            self.logger.error( f"\t\tFailed to set FGC.ID to RESET: {response.err_msg}")

# EOF
