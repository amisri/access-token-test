import logging
import re
import math

import pyfgc_db.models as db_models
from pyfgc_db.core import FgcDb

from fgc_config_manager.synchroniser.synchroniser import Synchroniser

module_logger = logging.getLogger(__name__)


class DbSynchroniser(Synchroniser):

    async def _sync(self):
        """Sync all the properties from the device to the database"""

        self.logger.info(f"\tSynchronising from device {self.device} to database")

        with FgcDb.session() as db_session:
            failed = self._check_spare(db_session)
            if failed:
                self.logger.error(f"\tFailed to replace device with spare")
                await self._set_config_state(failed=True)
                return

            self.logger.info("\t\tReading system details from database...")
            system = db_session.query(db_models.System).filter(db_models.System.name == self.device_db).one_or_none()
            if system is None:
                self.logger.error(f"\tFailed to read details for system {self.device_db} from database")
                await self._set_config_state(failed=True)
                return
            failed = await self._sync_system_to_db(db_session, system)

            components = [comp_sys.component for comp_sys in system.component_systems]
            failed |= await self._sync_components_to_db(db_session, components)

            failed |= await self._sync_type_properties(db_session, system)

        if not failed:
            await self._clear_unsynced()

        await self._set_config_state(failed)

    async def _sync_system_to_db(self, db_session, system: db_models.System):
        """Sync the system properties"""

        system_properties = {prop.property.name: prop for prop in system.system_properties}
        self.logger.debug(f"Found {len(system_properties)} system properties")
        self.logger.info("\t\tRetrieving list of configuration properties from FGC...")
        fgc_config_properties = await self._get_fgc_config_properties()
        failed = False
        if not fgc_config_properties:
            self.logger.error(f"Failed to read config properties list from {self.device}")
            failed = True
        for prop_name in list(system_properties.keys()):
            if prop_name not in fgc_config_properties:
                self.logger.info(f"\t\t\tSkipping {prop_name}, it does not exist in {self.device}")
                del system_properties[prop_name]
        self.logger.debug(f"Left with {len(system_properties)} system properties after removing the not CONFIG ones")
        for prop_name in list(system_properties.keys()):
            if prop_name not in fgc_config_properties or fgc_config_properties[prop_name] == '' or \
                    system_properties[prop_name].value == fgc_config_properties[prop_name]:
                del system_properties[prop_name]
                continue
            # logger.debug(f"{prop_name} : {system_properties[prop_name].value} - {fgc_properties[prop_name]}")
            system_properties[prop_name].value = fgc_config_properties[prop_name]
        if system_properties:
            self.logger.info(f"\t\tUpdating database with new property values...")
            for prop_name, prop in system_properties.items():
                self.logger.info(f"\t\t\t{prop_name}=\"{prop.value}\"")
            db_session.add_all(system_properties.values())
            db_session.commit()
        else:
            self.logger.info("\t\tNo system property values have changed.")
        if not failed and system_properties:
            await self._clear_cfg_changed_flag(system_properties.keys())
        return failed

    async def _sync_components_to_db(self, db_session, components: list, calibration: bool = False):
        """Sync the component properties"""

        failed = False
        for component in components:
            component_properties = {}
            for component_property in component.component_properties:
                prop_name = component_property.property.name
                if component.channel:
                    prop_name = prop_name.replace("?", component.channel)
                if calibration:
                    if not re.match(r"CAL\..\.ADC\.INTERNAL\.ERR", prop_name):
                        continue
                # Skip properties related to barcodes so that they are not overwritten
                if prop_name.startswith("BARCODE"):
                    continue
                component_properties[prop_name] = component_property

            self.logger.info(f"\t\tGetting properties for component {component.mtf_id} from FGC...")
            fgc_component_properties = await self._read_properties_from_fgc(component_properties.keys())
            if fgc_component_properties is None:
                self.logger.error(f"\t\tFailed to read all component properties for {component.mtf_id} from FGC")
                failed = True
                continue
            for prop_name in list(component_properties.keys()):
                if prop_name not in fgc_component_properties or fgc_component_properties[prop_name] == '' or \
                        component_properties[prop_name].value == fgc_component_properties[prop_name]:
                    del component_properties[prop_name]
                    continue
                # logger.debug(f"{prop_name} : {component_properties[prop_name].value} - {fgc_component_properties[prop_name]}")
                component_properties[prop_name].value = fgc_component_properties[prop_name]

            if component_properties:
                self.logger.info(f"\t\tUpdating database with new property values for component {component.mtf_id}...")
                for prop_name, prop in component_properties.items():
                    self.logger.info(f"\t\t\t{prop_name}=\"{prop.value}\"")
                db_session.add_all(component_properties.values())
                db_session.commit()
            else:
                self.logger.info(f"\t\t\tNo property values have changed for component {component.mtf_id}.")
            if not failed and component_properties:
                await self._clear_cfg_changed_flag(component_properties.keys())
        return failed

    async def _sync_type_properties(self, db_session, system: db_models.System):
        """Sync type properties"""

        type_properties = self._get_all_type_properties(system)
        self.logger.debug(f"FOUND {len(type_properties)} type properties: {type_properties}")

        self.logger.info("\t\tGetting type properties from FGC...")
        fgc_type_properties = await self._read_properties_from_fgc(type_properties.keys())
        if fgc_type_properties is None:
            return True
        elif fgc_type_properties == {}:
            return False

        self.logger.info("\t\tComparing type properties between FGC and DB...")
        failed = self._compare_type_properties(db_session, fgc_type_properties, type_properties)
        if not failed:
            await self._clear_cfg_changed_flag(type_properties)
        return failed

    def _compare_type_properties(self, db_session, fgc_type_properties: dict, type_properties: dict):
        """Check if type properties haven't change"""

        failed = False
        for prop_name in list(type_properties.keys()):
            if prop_name not in fgc_type_properties or \
                    type_properties[prop_name].value == fgc_type_properties[prop_name]:
                del type_properties[prop_name]
                continue

        for prop_name, prop in type_properties.items():
            db_values = []
            if prop.value:
                db_values = prop.value.split(",")
            fgc_values = fgc_type_properties[prop_name].split(",")

            if len(fgc_values) == len(db_values):
                equals = True
                for fgc_value, db_value in zip(fgc_values, db_values):
                    try:
                        float(fgc_value)
                        float(db_value)
                    except ValueError:
                        equals = False
                        break

                    if not math.isclose(float(fgc_value), float(db_value), rel_tol=0.000005):
                        equals = False
                        break

                if equals:
                    if len(fgc_type_properties[prop_name]) >= len(prop.value):
                        self.logger.info(f"\t\t\tUpdating {prop.type.name}: "
                                         f"{prop.property.name}=\"{fgc_type_properties[prop_name]}\" (from: "
                                         f"\"{prop.value}\")")
                        prop.value = fgc_type_properties[prop_name]
                        db_session.add(type_properties[prop_name])
                        db_session.commit()
                    continue

            self.logger.error(f"\t\t\tType {prop.type.name} with property {prop_name} in FGC differs from"
                              f" DB ({fgc_type_properties[prop_name]} != {prop.value})")
            failed = True
        return failed

    async def _read_properties_from_fgc(self, property_names: list):
        """Fetch properties from the FGC"""

        properties = {}
        failed = False
        for property_name in property_names:
            response = await self.fgc_session.get(property_name + " TYPE")
            if response.err_code != "":
                self.logger.error(f"\t\tFailed to read property {property_name} from FGC: {response.err_msg}")
                failed = True
                break
            # logger.debug(f"{property_name}: {response.value}")
            typ, value = response.value.split(":")
            if typ.lower() == "char":
                value = value.replace(",", "")
            properties[property_name] = value
        if failed:
            return None
        return properties

    def _get_all_type_properties(self, system: db_models.System):
        """Get all the type properties"""
        type_properties = {}

        if system.type:
            for type_property in system.type.type_properties:
                type_properties[type_property.property.name] = type_property

        for component_system in system.component_systems:
            component = component_system.component
            if component.type:
                for type_property in component.type.type_properties:
                    prop_name = type_property.property.name
                    if component.channel:
                        prop_name = prop_name.replace("?", component.channel)
                    type_properties[prop_name] = type_property

        return type_properties

    async def _set_config_state(self, failed: bool):
        """Set the config state of the FGC after synchronization"""

        await super()._set_config_state(failed)
        if failed:
            self.logger.error(f"\tFailed to complete synchronisation from {self.device} to database")
        else:
            self.logger.info(f"\tSynchronisation from {self.device} to database completed")

# EOF
