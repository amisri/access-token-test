import logging

import pyfgc
import pyfgc_db.models as db_models

from fgc_config_manager.utils import DeviceLogAdapter
import fgc_config_manager.settings as settings


class Synchroniser:

    def __init__(self, rbac_token, device):
        self.rbac_token = rbac_token
        self.device = device
        self.logger = DeviceLogAdapter(logging.getLogger(__name__ + "." + device), {"device": device})
        self.fgc_session = None
        self.device_db = device
        self.spare_id = 0

    def _check_spare(self, db_session):
        """Check if the device should be replaced by a spare one"""

        self.logger.info("\tChecking if device should be spare...")
        failed = False
        spare_id = db_session.query(db_models.SystemProperty).filter(
            db_models.SystemProperty.system.has(name=self.device),
            db_models.SystemProperty.property.has(name="DEVICE.SPARE_ID")).one_or_none()
        if spare_id is not None and spare_id.value is not None and spare_id.value != "0":
            self.spare_id = int(spare_id.value)
            self.device_db = self.device + "_" + spare_id.value.zfill(2)
            self.logger.info(
                f"\tDevice will be replaced by spare in channel {self.spare_id} with name in db {self.device_db}")
        return failed

    async def _get_fgc_config_properties(self):
        """Get all the config properties from the FGC"""

        response = await self.fgc_session.get("CONFIG.SET TYPE")
        if response.err_code != "":
            self.logger.error(f"\tFailed to retrieve CONFIG.SET from {self.device}: {response.err_msg}")
            return None
        config_properties = {}
        self._parse_config_properties(config_properties, response)
        response = await self.fgc_session.get("CONFIG.UNSET TYPE")
        if response.err_code != "":
            self.logger.error(f"\tFailed to retrieve CONFIG.UNSET from {self.device}: {response.err_msg}")
            return None
        self._parse_config_properties(config_properties, response)
        return config_properties

    def _parse_config_properties(self, config_properties: dict, response):
        """Parse the properties into key:value dict"""

        response_value = response.value
        if response_value.startswith("PARENT:"):
            response_value = response_value[7:]
        if response_value.strip() != "":
            for prop in response_value.split("\n"):
                name, typ, value = prop.split(":")
                if typ.lower() == "char":
                    value = value.replace(",", "")
                config_properties[name] = value

    async def _get_fgc_session(self):
        """Open an FGC async session"""

        self.fgc_session = await pyfgc.async_connect(self.device, rbac_token=self.rbac_token,
                                                     name_file=settings.basic.NAME_FILE)

    async def _close_fgc_session(self):
        """Close an FGC async session"""

        await self.fgc_session.disconnect()

    async def sync(self):
        """Sync function to be called by modules that want to sync FGCs"""

        try:
            await self._get_fgc_session()
            await self._sync()
        except Exception:
            await self._set_config_state(failed=True)
            self.logger.exception("Sync task failed")
        finally:
            await self._close_fgc_session()

    async def _sync(self):
        raise NotImplementedError()

    async def _set_config_state(self, failed: bool):
        """Set the config state of the FGC after synchronization"""

        await self.fgc_session.set("CONFIG.STATE", "RESET")
        if failed:
            await self.fgc_session.set("CONFIG.MODE", "SYNC_FAILED")

    async def _clear_cfg_changed_flag(self, properties):
        """Acknowledge all the properties that have been updated"""

        success = True
        for prop_name in properties:
            response = await self.fgc_session.get(prop_name + " SYNCHED")
            if response.err_code != "":
                success = False
        return success

    async def _clear_unsynced(self):
        """Acknowledge all the properties that show as changed on the FGC but were already updated in the database"""

        response = await self.fgc_session.get("CONFIG.CHANGED")
        properties = []
        if response.err_code == "":
            if response.value.strip() != "":
                for prop in response.value.split("\n"):
                    name, _ = prop.split(":")
                    properties.append(name)
        self.logger.debug(f"UNSYNCHED: {properties}")
        if properties:
            await self._clear_cfg_changed_flag(properties)

# EOF
