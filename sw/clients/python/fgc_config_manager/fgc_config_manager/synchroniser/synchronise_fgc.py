import logging
import re

import pyfgc_db.models as db_models
from pyfgc_db.core import FgcDb

from fgc_config_manager.synchroniser.synchroniser import Synchroniser
from fgc_config_manager.lsa_handler import lsa_handler

module_logger = logging.getLogger(__name__)


class FgcSynchroniser(Synchroniser):

    async def _sync(self):
        """Sync all the properties from the database to the device. Also sync any component changes(removal/addition)"""

        self.logger.info(f"\tSynchronising from database to device {self.device}")
        failed = False

        buses = await self._read_barcodes_from_fgc()
        if not buses:
            self.logger.error("\tFailed to read barcodes from FGC")
            # await self._set_config_state(failed=True)
            failed = True
        self.logger.debug(buses)

        with FgcDb.session() as db_session:
            f, reset = await self._check_spare(db_session)
            if f:
                self.logger.error(f"\tFailed to replace device with spare")
                await self._set_config_state(failed=True)
                return
            if reset:
                self.logger.info(f"\tDevice {self.device} is replaced by spare device {self.device_db}. Resetting...")
                # await self._set_config_state(failed=False)
                return

            component_ids = [comp for comps in buses.values() for comp in comps]
            fgc_components = self._get_components_from_db(db_session, component_ids)
            self.logger.debug(fgc_components)

            self.logger.info("\t\tReading system details from database...")
            system = db_session.query(db_models.System).filter(db_models.System.name == self.device_db).one_or_none()
            if system is None:
                self.logger.error(f"\tFailed to read details for system {self.device_db} from database")
                await self._set_config_state(failed=True)
                return

            self._remove_components_from_db(db_session, system, component_ids)

            db_components = {comp_sys.component.mtf_id: comp_sys.component for comp_sys in system.component_systems}
            self.logger.debug(db_components)
            self._add_components_to_db(db_session, system, fgc_components, db_components)

            self._update_component_channels(db_session, buses, fgc_components)

            self.logger.info("\t\tRetrieving list of configuration properties from FGC...")
            config_properties = await self._get_fgc_config_properties()
            if not config_properties:
                self.logger.error(f"Failed to read config properties list from {self.device}")
                failed = True
            else:
                # logger.debug(config_properties)

                failed |= await self._set_system_properties(system, config_properties)

                failed |= await self._set_system_type_properties(system, config_properties)

                failed |= await self._set_component_properties(system, config_properties)

                failed |= await self._set_spare()

        if not failed:
            await self._clear_unsynced()

        await self._set_config_state(failed)

    async def _set_spare(self):
        failed = False
        if self.spare_id != 0:
            self.logger.info(f"\t\tSetting DEVICE.SPARE_ID to {self.spare_id}")
            response = await self.fgc_session.set("DEVICE.SPARE_ID", self.spare_id)
            if response.err_code != "":
                self.logger.error(f"\t\t\tFailed to set DEVICE.SPARE_ID to {self.spare_id}: {response.err_msg}")
                failed = True
        return failed

    async def _check_spare(self, db_session):
        """Check if the device should be replaced by a spare one"""

        failed = super()._check_spare(db_session)
        reset = False
        if self.device != self.device_db:
            response = await self.fgc_session.get("DEVICE.SPARE_ID")
            if response.err_code != "":
                self.logger.error(f"\tFailed to retrieve DEVICE.SPARE_ID from {self.device}: {response.err_msg}")
                failed = True
            elif int(response.value) != self.spare_id:
                self.logger.info(f"\t\tSetting DEVICE.SPARE_ID to {self.spare_id}")
                set_response = await self.fgc_session.set("DEVICE.SPARE_ID", self.spare_id)
                if set_response.err_code != "":
                    self.logger(f"\tFailed to set DEVICE.SPARE_ID to {self.spare_id}: {set_response.err_msg}")
                    failed = True
                else:
                    set_response = await self.fgc_session.set("DEVICE.RESET", "")
                    if set_response.err_code != "":
                        self.logger(f"\tFailed to reset device: {set_response.err_msg}")
                        failed = True
                    else:
                        reset = True
        return failed, reset

    async def _set_component_properties(self, system: db_models.System, config_properties: dict):
        """Set the component property for all components of the FGC"""

        components = [comp_sys.component for comp_sys in system.component_systems]
        failed = False
        for component in components:
            component_properties = {}
            for component_property in component.component_properties:
                prop_name = component_property.property.name
                if component.channel:
                    prop_name = prop_name.replace("?", component.channel)
                if prop_name in ["CAL.A.ADC16.ERR", "CAL.B.ADC16.ERR"]:
                    continue
                component_properties[prop_name] = component_property

            if component.type:
                for type_property in component.type.type_properties:
                    prop_name = type_property.property.name
                    if component.channel:
                        prop_name = prop_name.replace("?", component.channel)
                    component_properties[prop_name] = type_property

            self.logger.info(f"\t\tSetting properties for component {component.mtf_id} on FGC...")
            for prop_name, prop in component_properties.items():
                if not prop.value:
                    continue

                if prop_name != 'BARCODE.FGC.CASSETTE' and not re.match(r"BARCODE\.[AB]\.EXADC\.CASSETTE", prop_name) and \
                        not re.match(r"BARCODE\.[AB]\.ADC22\.CASSETTE", prop_name) and \
                        not re.match(r"BARCODE\.[AB]\.DCCT\.ELEC", prop_name) and \
                        not re.match(r"BARCODE\.[AB]\.DCCT\.HEAD", prop_name) and prop_name not in config_properties:
                    self.logger.info(f"\t\t\tSkipping {prop_name}, it does not exist in {self.device}")
                    continue
                self.logger.info(f"\t\t\t{prop_name}=\"{prop.value}\"")
                response = await self.fgc_session.set(prop_name, prop.value)
                if response.err_code != "":
                    self.logger.error(f"\t\t\tFailed to set {prop_name} to {prop.value}: {response.err_msg}")
                    failed = True
        return failed

    async def _set_system_type_properties(self, system: db_models.System, config_properties: dict):
        """Set the type properties associated with the FGC and its components"""

        failed = False
        if system.type:
            self.logger.info("\t\tSetting system type properties on FGC...")
            for typ_prop in system.type.type_properties:
                if not typ_prop.value:
                    continue
                if config_properties and typ_prop.property.name not in config_properties:
                    self.logger.info(f"\t\t\tSkipping {typ_prop.property.name}, it does not exist in {self.device}")
                    continue
                self.logger.info(f"\t\t\t{typ_prop.property.name}=\"{typ_prop.value}\"")
                response = await self.fgc_session.set(typ_prop.property.name, typ_prop.value)
                if response.err_code != "":
                    self.logger.error(f"\t\t\tFailed to set {typ_prop.property.name} to {typ_prop.value}: {response.err_msg}")
                    failed = True
        return failed

    async def _set_system_properties(self, system: db_models.System, config_properties: dict):
        """Set the system properties of the FGC"""

        failed = False
        self.logger.info("\t\tSetting system properties on FGC...")
        for sys_prop in system.system_properties:
            if not sys_prop.value:
                continue
            if config_properties and sys_prop.property.name not in config_properties:
                self.logger.info(f"\t\t\tSkipping {sys_prop.property.name}, it does not exist in {self.device}")
                continue
            self.logger.info(f"\t\t\t{sys_prop.property.name}=\"{sys_prop.value}\"")
            response = await self.fgc_session.set(sys_prop.property.name, sys_prop.value)
            if response.err_code != "":
                self.logger.error(f"\t\t\tFailed to set {sys_prop.property.name} to {sys_prop.value}: {response.err_msg}")
                failed = True
        return failed

    def _update_component_channels(self, db_session , buses: dict, fgc_components: dict):
        """Update the component channel in case of moved components"""

        for bus in ['A', 'B']:
            if bus not in buses:
                continue
            new_channel_components = []
            for comp_id in buses[bus]:
                comp = fgc_components[comp_id]
                if comp.channel != bus:
                    self.logger.info(f"\t\t\tComponent {comp_id} has moved to channel {bus}")
                    comp.channel = bus
                    new_channel_components.append(comp)
            if new_channel_components:
                self.logger.info(f"\t\tUpdating channel in database for new devices on channel {bus}")
                db_session.add_all(new_channel_components)
                db_session.commit()

    def _add_components_to_db(self, db_session, system: db_models.System, fgc_components: dict, db_components: dict):
        """Connect newly added components to the system in database"""

        self.logger.info("\t\tChecking for added components...")
        added_components = []
        for comp_id, comp in fgc_components.items():
            if comp.mtf_id not in db_components:
                self.logger.info(f"\t\t\t{comp.mtf_id} has been added")
                added_components.append(comp)
        for comp in added_components:
            for comp_child in comp.child:
                if comp_child.mtf_id not in db_components:
                    self.logger.info(f"\t\t\t{comp_child.mtf_id} child component has been added")
                    added_components.append(comp_child)
        for comp_sys in system.component_systems:
            comp = comp_sys.component
            if comp not in added_components:
                for comp_child in comp.child:
                    if comp_child.mtf_id not in db_components:
                        self.logger.info(f"\t\t\t{comp_child.mtf_id} child component has been added")
                        added_components.append(comp_child)
        if added_components:
            self.logger.info("\t\tRegistering added components with system in database")
            for comp in added_components:
                if not comp.type.multi_sys:
                    for comp_sys in comp.component_systems:
                        db_session.delete(comp_sys)
                comp_sys = db_models.ComponentSystem(system=system, component=comp)
                db_session.add(comp_sys)
            db_session.commit()

    def _remove_components_from_db(self, db_session, system: db_models.System, component_ids: list):
        """Remove the connection with a component and a system from the database"""

        self.logger.info("\t\tChecking for removed components...")
        remove_list = []

        db_components_id = []
        remove_ids = []
        for comp_sys in system.component_systems:
            comp = comp_sys.component
            if comp.type.dallas_id:
                db_components_id.append(comp.mtf_id)
                if comp.mtf_id not in component_ids:
                    self.logger.info(f"\t\t\t{comp.mtf_id} has been removed")
                    remove_list.append(comp_sys)
                    remove_ids.append(comp.mtf_id)

        for comp_sys in system.component_systems:
            comp = comp_sys.component
            if not comp.type.dallas_id:
                if not comp.parent or comp.parent.mtf_id not in db_components_id or comp.parent.mtf_id in remove_ids:
                    self.logger.info(f"\t\t\tchild barcode {comp.mtf_id} has been removed")
                    remove_list.append(comp_sys)

        # for comp_sys in system.component_systems:
        #     comp = comp_sys.component
        #     if comp.type.dallas_id:
        #         if comp.mtf_id not in component_ids:
        #             self.logger.info(f"\t\t\t{comp.mtf_id} has been removed")
        #             remove_list.append(comp_sys)
        #     else:
        #         if not comp.parent or comp.parent.mtf_id not in component_ids:
        #             self.logger.info(f"\t\t\tchild barcode {comp.mtf_id} has been removed")
        #             remove_list.append(comp_sys)

        self.logger.debug(remove_list)
        if remove_list:
            self.logger.info("\t\tUnregistering removed components from system in database")
            for comp_sys in remove_list:
                db_session.delete(comp_sys)
            db_session.commit()

    async def _read_barcodes_from_fgc(self):
        """Read the barcodes from the FGC"""

        self.logger.info("\t\tGetting barcodes from FGC...")
        response = await self.fgc_session.get("BARCODE.BUS")
        if response.err_code != "":
            self.logger.error(f"\tFailed to retrieve BARCODE.BUS from {self.device}: {response.err_msg}")
            return {}
        # logger.debug(response.value)
        buses = {}
        for line in response.value.split("\n"):
            bus, barcodes = line.split(":")
            # logger.debug(f"{bus} - {barcodes.split(',')}")
            if barcodes == '':
                continue
            components = []
            for comp in barcodes.split(","):
                if comp != "000000" and not comp.startswith("DS18B20___-JT"):
                    components.append(comp)
            if components:
                buses[bus] = components
        return buses

    def _get_components_from_db(self, db_session, component_ids: list):
        """Get the components from the database"""

        components = {}
        for comp_id in component_ids:
            component = db_session.query(db_models.Component).filter(db_models.Component.mtf_id == comp_id).one_or_none()
            if not component:
                self.logger.error(f"Failed to read component {comp_id} from database")
                continue
            components[comp_id] = component

        return components

    async def _set_config_state(self, failed: bool):
        """Set the config state of the FGC after synchronization"""

        await super()._set_config_state(failed)
        if failed:
            self.logger.error(f"\tFailed to complete synchronisation from database to device {self.device}")
        else:
            if lsa_handler.enabled():
                await lsa_handler.add_device(self.device)
            self.logger.info(f"\tSynchronisation from database to device {self.device} completed")


# EOF
