import logging

import pyfgc_db.models as db_models
from pyfgc_db.core import FgcDb

from fgc_config_manager.synchroniser.synchronise_db import DbSynchroniser

module_logger = logging.getLogger(__name__)


class DbCalSynchroniser(DbSynchroniser):

    async def _sync(self):
        """Sync all the calibration properties from the database to the device."""

        self.logger.info(f"\tSynchronising calibration from device {self.device} to database")

        with FgcDb.session() as db_session:
            failed = self._check_spare(db_session)
            if failed:
                self.logger.error(f"\tFailed to replace device with spare")
                await self._set_config_state(failed=True)
                return

            self.logger.info("\t\tReading system details from database...")
            system = db_session.query(db_models.System).filter(db_models.System.name == self.device_db).one_or_none()
            if system is None:
                self.logger.error(f"\tFailed to read details for system {self.device_db} from database")
                await self._set_config_state(failed=True)
                return

            components = [comp_sys.component for comp_sys in system.component_systems]
            failed = await self._sync_components_to_db(db_session, components, calibration=True)

        await self._set_config_state(failed)

    async def _set_config_state(self, failed: bool):
        """Set the config state of the FGC after synchronization"""

        await super()._set_config_state(failed)
        if failed:
            self.logger.error(f"\tFailed to complete the synchronisation of calibration from {self.device}")
        else:
            self.logger.info(f"\tSynchronisation of calibration from {self.device} to database completed")


# EOF
