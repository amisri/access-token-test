"""Module for handling the LSA re-drive requests
"""

import asyncio
import logging
import aiohttp
import base64

import pyfgc_name

from fgc_config_manager.utils import Singleton
import fgc_config_manager.rbac_manager
import fgc_config_manager.settings as settings

logger = logging.getLogger(__name__)

SLEEP_TIME = 10  # in seconds


class LsaHandler(metaclass=Singleton):

    def __init__(self):
        self.lock = asyncio.Lock()
        self.devices = set()
        self.running = False

    async def add_device(self, device):
        async with self.lock:
            if 'sub_devices' in pyfgc_name.devices[device]:
                logger.info(f"{device} is scheduled for LSA re-drive.")
                self.devices.add(device)

    def enabled(self):
        return self.running

    async def run(self):
        try:
            logger.info("Running LSA handler task.")
            self.running = True

            while self.running:
                await asyncio.sleep(SLEEP_TIME)

                logger.info("Checking if devices need re-drive from LSA.")

                async with self.lock:
                    if self.devices:
                        logger.info(f"Preparing LSA request for {len(self.devices)} FGCs: {self.devices}")
                        aliases = []
                        for device in self.devices:
                            dev_aliases = [sub_dev['alias'] for sub_dev in pyfgc_name.devices[device]['sub_devices'] if
                                           sub_dev is not None]
                            aliases.extend(dev_aliases)
                        if aliases:
                            await self._send_request(aliases)
                        self.devices.clear()

        except Exception:
            self.running = False
            logger.exception("LSA handler failed unexpectedly.")
            raise
        logger.info("Exiting LSA handler task.")

    async def stop(self):
        logger.info("Stopping LSA handler task.")
        self.running = False

    async def _send_request(self, aliases):
        logger.info(f"\tSending LSA request for {len(aliases)} aliases.")
        logger.debug(f"\tSending LSA request for aliases: {aliases}")
        rbac_token = await fgc_config_manager.rbac_manager.get_token()
        try:
            async with aiohttp.ClientSession(raise_for_status=True) as session:
                headers = {"Authorization": f"{base64.b64encode(rbac_token).decode('utf-8')}"}
                json_data = {"@type": "redrive_request", "deviceNamesToReDrive": aliases}
                response = await session.post(settings.basic.INCA_ENDPOINT, json=json_data, headers=headers)
                rsp_json = await response.json()
                logger.info(f"\tLSA request response: OK for {len(rsp_json['deviceReDriveResponses'])} aliases")
                resp_aliases = [dev['deviceName'] for dev in rsp_json['deviceReDriveResponses']]
                logger.debug(f"\tLSA request response: OK for: {resp_aliases}")
        except aiohttp.ClientResponseError as err:
            logger.error(f"\tLSA request failed: {err.status} {err.message}.")
        except Exception as err:
            logger.error(f"\tLSA request failed: {err}.")


lsa_handler = LsaHandler()

# EOF
