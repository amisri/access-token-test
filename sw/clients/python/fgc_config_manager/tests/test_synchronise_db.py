from fgc_config_manager.synchroniser import synchronise_db as sync_db

import pyfgc

import pyfgc_db.models as db_models

import pytest
import unittest.mock as mock


class MockFgcResponse:
    def __init__(self, err_code="", err_msg="", value=""):
        self.err_code = err_code
        self.err_msg = err_msg
        self.value = value


class MockFgcAsyncSession:

    async def get(self, prop):
        if prop.startswith("BAD"):
            return MockFgcResponse(err_code=1, err_msg="BAD PROP")
        elif prop.startswith("CHAR"):
            return MockFgcResponse(value="CHAR:1,0")
        elif prop.endswith("TYPE"):
            return MockFgcResponse(value="INT:10")
        else:
            return MockFgcResponse(value="10")

    async def disconnect(self):
        pass


class MockDbSession:

    def __init__(self):
        self.items = []
        self.committed = False

    def add(self, item):
        self.items.append(item)

    def add_all(self, items):
        self.items.extend(items)

    def commit(self):
        self.committed = True


@pytest.mark.asyncio
async def test_clear_cfg_changed_flag():
    db_sync = sync_db.DbSynchroniser(None, "DEV")
    db_sync.fgc_session = MockFgcAsyncSession()

    assert await db_sync._clear_cfg_changed_flag(["GOOD", "VGOOD"])
    assert not await db_sync._clear_cfg_changed_flag(["GOOD", "BAD"])


def test_get_all_type_properties():
    typ = db_models.Type()
    prop = db_models.Property(name="TYP_PROP")
    type_prop = db_models.TypeProperty(type=typ, property=prop)
    typ.type_properties = [type_prop]

    comp_typ = db_models.Type()
    comp_prop = db_models.Property(name="CMP_?_PROP")
    comp_type_prop = db_models.TypeProperty(type=comp_typ, property=comp_prop)
    comp_typ.type_properties = [comp_type_prop]

    comp = db_models.Component()
    comp.type = comp_typ
    comp.channel = "1"

    system = db_models.System()
    system.type = typ
    comp_sys = db_models.ComponentSystem(component=comp, system=system)
    system.component_systems = [comp_sys]

    type_properties = {"TYP_PROP": type_prop, "CMP_1_PROP": comp_type_prop}

    db_sync = sync_db.DbSynchroniser(None, "DEV")

    assert db_sync._get_all_type_properties(system) == type_properties


@pytest.mark.asyncio
async def test_read_properties_from_fgc():
    props = {"PROP": '10', "OTHER": '10', "CHAR": '10'}
    db_sync = sync_db.DbSynchroniser(None, "DEV")
    db_sync.fgc_session = MockFgcAsyncSession()

    assert await db_sync._read_properties_from_fgc(props.keys()) == props
    assert await db_sync._read_properties_from_fgc(list(props.keys()) + ["BAD"]) is None


def test_compare_type_properties():
    typ = db_models.Type(name="TYP")
    prop = db_models.Property(name="PROP")
    type_prop = db_models.TypeProperty(type=typ, property=prop, value="1")
    type_prop2 = db_models.TypeProperty(type=typ, property=prop, value="1,a,1")
    type_prop3 = db_models.TypeProperty(type=typ, property=prop, value="1,1.000005")

    type_properties = {"ZERO": None, "ONE": type_prop, "TWO": type_prop2, "THREE": type_prop3}
    fgc_type_properties = {"ONE": "1", "TWO": "1,a,1", "THREE": "1,1.000004"}
    db_session = MockDbSession()
    db_sync = sync_db.DbSynchroniser(None, "DEV")

    assert not db_sync._compare_type_properties(db_session, fgc_type_properties, type_properties)
    assert type_prop3.value == fgc_type_properties["THREE"]
    assert db_session.items == [type_prop3]
    assert db_session.committed

    type_properties = {"TWO": type_prop2}
    fgc_type_properties = {"TWO": "1,b,1"}
    db_session = MockDbSession()

    assert db_sync._compare_type_properties(None, fgc_type_properties, type_properties)
    assert not db_session.committed

    type_properties = {"THREE": type_prop3}
    fgc_type_properties = {"THREE": "1,1.000011"}
    db_session = MockDbSession()

    assert db_sync._compare_type_properties(None, fgc_type_properties, type_properties)
    assert not db_session.committed


@pytest.mark.asyncio
async def test_sync_components_to_db(monkeypatch):
    comp = db_models.Component(mtf_id="123")
    comp.channel = "1"
    prop = db_models.Property(name="PROP.?.ABC")
    comp_prop = db_models.ComponentProperty(component=comp, property=prop, value="123")
    prop2 = db_models.Property(name="PROP.?.ABCD")
    comp_prop2 = db_models.ComponentProperty(component=comp, property=prop2, value="123")
    prop3 = db_models.Property(name="PROP.?.MIS")
    comp_prop3 = db_models.ComponentProperty(component=comp, property=prop3, value="123")
    comp.component_properties = [comp_prop, comp_prop2, comp_prop3]

    async def mock_read_properties(*args, **kwargs):
        return {"PROP.1.ABC": "1234", "PROP.1.ABCD": "123"}

    monkeypatch.setattr(sync_db.DbSynchroniser, "_read_properties_from_fgc", mock_read_properties)

    db_session = MockDbSession()
    db_sync = sync_db.DbSynchroniser(None, "DEV")

    with mock.patch("fgc_config_manager.synchroniser.DbSynchroniser._clear_cfg_changed_flag") as mk:
        await db_sync._sync_components_to_db(db_session, [comp])

        assert db_session.items == [comp_prop]
        assert db_session.committed
        assert comp_prop.value == "1234"
        mk.assert_called_once_with(dict({"PROP.1.ABC": 1}).keys())


@pytest.mark.asyncio
async def test_sync_components_to_db_skip_barcode(monkeypatch):
    comp = db_models.Component(mtf_id="123")
    comp.channel = "1"
    prop = db_models.Property(name="BARCODE.?.ABC")
    comp_prop = db_models.ComponentProperty(component=comp, property=prop, value="123")
    comp.component_properties = [comp_prop]

    async def mock_read_properties(*args, **kwargs):
        return {"BARCODE.1.ABC": "1234"}

    monkeypatch.setattr(sync_db.DbSynchroniser, "_read_properties_from_fgc", mock_read_properties)

    db_session = MockDbSession()
    db_sync = sync_db.DbSynchroniser(None, "DEV")

    await db_sync._sync_components_to_db(db_session, [comp])
    assert db_session.items == []
    assert not db_session.committed


@pytest.mark.asyncio
async def test_sync_components_to_db_calibration(monkeypatch):
    comp = db_models.Component(mtf_id="123")
    comp.channel = "1"
    prop = db_models.Property(name="PROP.?.ABC")
    comp_prop = db_models.ComponentProperty(component=comp, property=prop, value="123")
    prop_cal = db_models.Property(name="CAL.A.ADC.INTERNAL.ERR")
    comp_prop2 = db_models.ComponentProperty(component=comp, property=prop_cal, value="123")
    comp.component_properties = [comp_prop, comp_prop2]

    async def mock_read_properties(*args, **kwargs):
        return {"PROP.1.ABC": "1234", "CAL.A.ADC.INTERNAL.ERR": "1234"}

    monkeypatch.setattr(sync_db.DbSynchroniser, "_read_properties_from_fgc", mock_read_properties)

    db_session = MockDbSession()
    db_sync = sync_db.DbSynchroniser(None, "DEV")

    with mock.patch("fgc_config_manager.synchroniser.DbSynchroniser._clear_cfg_changed_flag") as mk:
        await db_sync._sync_components_to_db(db_session, [comp], calibration=True)
        assert db_session.items == [comp_prop2]
        assert db_session.committed
        assert comp_prop2.value == "1234"
        mk.assert_called_once_with(dict({"CAL.A.ADC.INTERNAL.ERR": 1}).keys())


@pytest.mark.asyncio
async def test_sync_system_to_db(monkeypatch):
    system = db_models.System()
    prop = db_models.Property(name="CONF_PROP")
    sys_prop = db_models.SystemProperty(system=system, property=prop, value="123")
    prop2 = db_models.Property(name="CONF_PROP_IGN")
    sys_prop2 = db_models.SystemProperty(system=system, property=prop2, value="123")
    system.system_properties = [sys_prop, sys_prop2]

    async def mock_get_config_properties(*args, **kwargs):
        return {"CONF_PROP": "1234"}

    monkeypatch.setattr(sync_db.DbSynchroniser, "_get_fgc_config_properties", mock_get_config_properties)

    db_sync = sync_db.DbSynchroniser(None, "DEV")

    with mock.patch("fgc_config_manager.synchroniser.DbSynchroniser._clear_cfg_changed_flag") as mk:
        db_session = MockDbSession()
        await db_sync._sync_system_to_db(db_session, system)
        assert db_session.items == [sys_prop]
        assert db_session.committed
        assert sys_prop.value == "1234"
        mk.assert_called_once_with(dict({"CONF_PROP": 1}).keys())


# EOF
