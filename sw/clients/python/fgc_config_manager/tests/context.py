import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import fgc_config_manager

from pyfgc_db.core import FgcDb

FgcDb.config_db(drivername="sqlite", database=":memory:", create=True)


# EOF
