from fgc_config_manager.synchroniser import synchronise_fgc as sync_fgc

import pyfgc

import pyfgc_db.models as db_models

import pytest

class MockFgcResponse:
    def __init__(self, err_code="", err_msg="", value=""):
        self.err_code = err_code
        self.err_msg = err_msg
        self.value = value


class MockFgcAsyncSession:

    def __init__(self):
        self.items = {}
        self.disconnected = False

    async def get(self, prop):
        if prop.startswith("BAD"):
            return MockFgcResponse(err_code=1, err_msg="BAD PROP")
        elif prop.startswith("CHAR"):
            return MockFgcResponse(value="CHAR:1,0")
        elif prop.endswith("TYPE"):
            return MockFgcResponse(value="INT:10")
        else:
            return MockFgcResponse(value="10")

    async def disconnect(self):
        self.disconnected = True

    async def set(self, name, value):
        self.items[name] = value
        return MockFgcResponse(value=value)


class MockDbSession:

    def __init__(self):
        self.items = []
        self.deleted = []
        self.committed = False

    def add(self, item):
        self.items.append(item)

    def add_all(self, items):
        self.items.extend(items)

    def delete(self, item):
        self.deleted.append(item)

    def commit(self):
        self.committed = True


@pytest.mark.asyncio
async def test_read_barcodes_from_fgc():

    class BarcodeGetAsyncFgcSession:
        async def get(self, prop):
            return MockFgcResponse(value="1:ABC\n2:000000\n3:DEF\n4:DS18B20___-JT1522\n5:GHI,JKL")

    buses = {"1": ["ABC"], "3": ["DEF"], "5": ["GHI", "JKL"]}

    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync.fgc_session = BarcodeGetAsyncFgcSession()

    assert await fgc_sync._read_barcodes_from_fgc() == buses


def test_remove_components_from_db():
    system = db_models.System()
    typ1 = db_models.Type(dallas_id=True)
    typ2 = db_models.Type(dallas_id=False)

    comp1 = db_models.Component(mtf_id="MTF1", type=typ1)
    comp_sys1 = db_models.ComponentSystem(component=comp1, system=system)

    comp2 = db_models.Component(mtf_id="MTF2", type=typ2)
    comp_sys2 = db_models.ComponentSystem(component=comp2, system=system)

    comp3 = db_models.Component(mtf_id="MTF3", type=typ2)
    comp3.parent = comp1
    comp_sys3 = db_models.ComponentSystem(component=comp3, system=system)

    comp4 = db_models.Component(mtf_id="MTF4", type=typ2)
    comp4.parent = comp1
    comp_sys4 = db_models.ComponentSystem(component=comp4, system=system)

    comp5 = db_models.Component(mtf_id="MTF5", type=typ1)
    comp_sys5 = db_models.ComponentSystem(component=comp5, system=system)

    system.component_systems = [comp_sys1, comp_sys2, comp_sys3, comp_sys4, comp_sys5]

    db_session = MockDbSession()
    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync._remove_components_from_db(db_session, system, ["MTF1", "MTF3"])

    assert set(db_session.deleted) == {comp_sys2, comp_sys5}
    assert db_session.committed


def test_add_components_to_db():
    system = db_models.System()

    typ = db_models.Type(multi_sys=False)
    comp1 = db_models.Component(mtf_id="MTF1", type=typ)

    comp2 = db_models.Component(mtf_id="MTF2", type=typ)
    comp3 = db_models.Component(mtf_id="MTF3", type=typ)
    comp33 = db_models.Component(mtf_id="MTF33", type=typ)
    comp2.child = [comp3, comp33]

    comp4 = db_models.Component(mtf_id="MTF4", type=typ)
    comp_sys = db_models.ComponentSystem(component=comp4, system=db_models.System())
    comp4.component_systems = [comp_sys]

    fgc_components = {"MTF1": comp1, "MTF2": comp2, "MTF4": comp4}

    db_components = {"MTF1": comp1, "MTF33": comp33}

    db_session = MockDbSession()
    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync._add_components_to_db(db_session, system, fgc_components, db_components)

    assert len(db_session.items) == 3
    assert db_session.items[0].system == system
    assert db_session.items[0].component == comp2
    assert db_session.items[1].system == system
    assert db_session.items[1].component == comp4
    assert db_session.items[2].system == system
    assert db_session.items[2].component == comp3
    assert db_session.deleted == [comp_sys]
    assert db_session.committed


def test_update_component_channels():
    buses = {"A": ["ABC"], "B": ["GHI", "JKL"], "C": ["IGN"]}

    comp1 = db_models.Component(mtf_id="ABC", channel='B')
    comp2 = db_models.Component(mtf_id="GHI", channel='B')
    comp3 = db_models.Component(mtf_id="JKL", channel='A')
    comp4 = db_models.Component(mtf_id="IGN", channel='C')

    fgc_components = {"ABC": comp1, "GHI": comp2, "JKL": comp3, "IGN": comp4}

    db_session = MockDbSession()
    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync._update_component_channels(db_session, buses, fgc_components)

    assert db_session.items == [comp1, comp3]
    assert db_session.committed


@pytest.mark.asyncio
async def test_set_system_properties():
    fgc_session = MockFgcAsyncSession()

    system = db_models.System()
    prop1 = db_models.Property(name="PROP1")
    sys_prop1 = db_models.SystemProperty(system=system, property=prop1, value="123")
    prop2 = db_models.Property(name="PROP2")
    sys_prop2 = db_models.SystemProperty(system=system, property=prop2, value="123")
    system.system_properties = [sys_prop1, sys_prop2]

    config_properties = ['PROP1']

    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync.fgc_session = fgc_session
    assert not await fgc_sync._set_system_properties(system, config_properties)
    assert len(fgc_session.items) == 1
    assert fgc_session.items["PROP1"] == '123'


@pytest.mark.asyncio
async def test_set_system_type_properties():
    fgc_session = MockFgcAsyncSession()

    typ = db_models.Type()
    system = db_models.System()
    system.type = typ
    prop1 = db_models.Property(name="PROP1")
    typ_prop1 = db_models.TypeProperty(type=typ, property=prop1, value="123")
    prop2 = db_models.Property(name="PROP2")
    typ_prop2 = db_models.TypeProperty(type=typ, property=prop2, value="123")
    typ.type_properties = [typ_prop1, typ_prop2]

    config_properties = ['PROP1']

    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync.fgc_session = fgc_session
    assert not await fgc_sync._set_system_type_properties(system, config_properties)
    assert len(fgc_session.items) == 1
    assert fgc_session.items["PROP1"] == '123'


@pytest.mark.asyncio
async def test_set_component_properties():
    fgc_session = MockFgcAsyncSession()

    system = db_models.System()

    comp1 = db_models.Component(mtf_id="123", channel='1')
    sys_comp1 = db_models.ComponentSystem(system=system, component=comp1)
    prop1 = db_models.Property(name="PROP1")
    comp_prop1 = db_models.ComponentProperty(property=prop1, component=comp1, value="123")
    prop2 = db_models.Property(name="CAL.A.ADC16.ERR")
    comp_prop2 = db_models.ComponentProperty(property=prop2, component=comp1, value="123")
    prop3 = db_models.Property(name="PROP.?.ABC")
    comp_prop3 = db_models.ComponentProperty(property=prop3, component=comp1, value="123")
    comp1.component_properties = [comp_prop1, comp_prop2, comp_prop3]

    typ = db_models.Type()
    prop4 = db_models.Property(name="TYP.?.DCB")
    typ_prop1 = db_models.TypeProperty(type=typ, property=prop4, value="123")
    prop5 = db_models.Property(name="BARCODE.A.DCCT.ELEC")
    typ_prop2 = db_models.TypeProperty(type=typ, property=prop5, value="123")
    typ.type_properties = [typ_prop1, typ_prop2]
    comp2 = db_models.Component(mtf_id="456", channel="2")
    comp2.type = typ
    sys_comp2 = db_models.ComponentSystem(system=system, component=comp2)

    system.component_systems = [sys_comp1, sys_comp2]
    config_properties = ['PROP1', 'PROP.1.ABC', 'TYP.2.DCB']

    fgc_sync = sync_fgc.FgcSynchroniser(None, "DEV")
    fgc_sync.fgc_session = fgc_session
    assert not await fgc_sync._set_component_properties(system, config_properties)
    assert len(fgc_session.items) == 4
    assert fgc_session.items["PROP1"] == '123'
    assert fgc_session.items["PROP.1.ABC"] == '123'
    assert fgc_session.items["TYP.2.DCB"] == '123'
    assert fgc_session.items["BARCODE.A.DCCT.ELEC"] == '123'


# EOF
