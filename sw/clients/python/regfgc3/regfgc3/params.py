import math
import struct

from itertools import chain, zip_longest
from typing import List, Tuple

from . import constants 
from . import utils

class ParamsBase:
    def __init__(self, blob: List[str]) -> None:
        self.version: int
        self.metadata: List = []
        self.values: List = []
        if isinstance(blob, (list,)):
            self.blob = blob
        elif isinstance(blob, (str,)):
            self.blob = utils.preprocess_fgc_response(blob)
        
    def print_nice(self) -> None:
        None

    def print_blob(self) -> None:
        print(",".join("0x{:08X}".format(param) for param in self.blob))

    def convert_to_props(self, **kwargs) -> List[Tuple[str, str]]:
        pass

class ParamsV2(ParamsBase):
    def __init__(self, blob: List[int]):
        super().__init__(blob)
        self.version = 2
        magic_word_idx = [i for i in range(len(self.blob)) if self.blob[i] & 0x0000FFFF == constants.BLOB_V2_MAGIC_WORD][0]
        self.metadata = list(map(lambda x: ((x >> 16) & 0xFFFF, x & 0xFFFF), self.blob[0:magic_word_idx + 1]))
        self.metadata = list(chain.from_iterable(self.metadata))
        self.values   = self.blob[magic_word_idx + 1:]

    def convert_to_props(self, pad: bool = False) -> None:
        metadata_idx = 1
        props = list()
        push_access = True

        for slot_idx in range(0, constants.SCIVS_N_SLOTS):
            push_slot = True
        
            for device_idx in range(0, constants.SCIVS_N_DEVICES):
                for block_idx in range(0, constants.SCIVS_N_BLOCKS):
                    size, index = (self.metadata[metadata_idx] >> 11), self.metadata[metadata_idx] & 0x7FF
                    if size > 0:
                        if push_access == True:
                            props.append(("REGFGC3.RAW.ACCESS", "RW"))
                            push_access = False
                        if push_slot == True:
                            props.append(("REGFGC3.RAW.SLOT", str(slot_idx)))
                            push_slot = False
                        
                        props.append(("REGFGC3.RAW.BLOCK", str(block_idx)))
                        params = ",".join("0x{:08X}".format(param) for param in self.values[index : index + size])
                        if pad and size < constants.SCIVS_MAX_BLOCK_SIZE:
                            params += ",0x00000000" * (constants.SCIVS_MAX_BLOCK_SIZE - size)
                        props.append(("REGFGC3.RAW.PARAM" if device_idx == 0 else "REGFGC3.RAW.DSP_PARAM", params))
                    metadata_idx += 1
        return props


class ParamsV4(ParamsBase):
    def __init__(self, blob: List[int]):
        super().__init__(blob)
        self.version = 4
        self.values_dsp = None
        self.dsp_slot = 0
        self._decompose_blob()
    
    @staticmethod
    def from_args(dsp_slot: int, metadata: list, values: list) -> None:
        metadata_word = []
        for a, b, c, d in ParamsV4._grouper(metadata, 4):
            metadata_word.append(a << 24 | b << 16 | c << 8 | d)
        return ParamsV4([0x00040000 + dsp_slot] + metadata_word + values)

    def _decompose_blob(self) -> None:
        index = 0
        self.dsp_slot = self.blob[index] & 0xFF
        index += 1
        
        # End of metadata section
        index_end = index + int(constants.SCIVS_N_SLOTS / 4)

        # Metadata: type integer. Need arithmetic manipulation of the data.
        for word in self.blob[index:index_end]:
            for byte in word.to_bytes(4, byteorder='big'):
                self.metadata.append(byte)
        index = index_end

        # Params: type int. Only used for printing
        self.values = self.blob[index:]

    def _convert_props_params(self, slot_idx: int, block_bitmask: int, props: list) -> None:
        while block_bitmask:
            block_idx = block_bitmask & (~block_bitmask+1)
            block_bitmask ^= block_idx
            block_idx = int(math.log2(block_idx))
            param_idx = slot_idx * constants.SCIVS_N_BLOCKS * constants.SCIVS_MAX_BLOCK_SIZE + block_idx * constants.SCIVS_MAX_BLOCK_SIZE

            props.append(("REGFGC3.RAW.BLOCK", str(block_idx)))
            params = ",".join("0x{:08X}".format(param) for param in self.values[param_idx : (param_idx + constants.SCIVS_MAX_BLOCK_SIZE)])
            props.append(("REGFGC3.RAW.PARAM" if slot_idx != 0 else "REGFGC3.RAW.DSP_PARAM", params))

    def convert_to_props(self) -> None:
        access = True
        props = list()

        for slot_idx in range(1, constants.SCIVS_N_SLOTS_PARAMS):
            # DSP parameters
            if slot_idx == self.dsp_slot:
                block_bitmask = self.metadata[0]
                if block_bitmask != 0:
                    props.append(("REGFGC3.RAW.SLOT", str(slot_idx)))
                    self._convert_props_params(0, self.metadata[0], props)

            # FPGA parameters
            block_bitmask = self.metadata[slot_idx]
            if block_bitmask != 0:
                if access == True:
                    props.append(("REGFGC3.RAW.ACCESS", "RW"))
                    access = False

                props.append(("REGFGC3.RAW.SLOT", str(slot_idx)))
                self._convert_props_params(slot_idx, block_bitmask, props)
            
        return props

    @staticmethod
    def _grouper(self, iterable, group_by, fillvalue=None):
        args_for_zip_l = [iter(iterable)] * group_by
        return zip_longest(*args_for_zip_l, fillvalue=fillvalue)

    def _print_nice_blocks(self,  slot_idx:int):
        if slot_idx != 0:
            block_bitmask = self.metadata[slot_idx]
            print(f" {slot_idx:02}    FPGA  ", end='')
        else:
            block_bitmask = self.metadata[0]
            print(f" {self.dsp_slot:02}    DSP   ", end='')

        for block_idx in range(constants.SCIVS_N_BLOCKS - 1, -1, -1):
            if (1 << block_idx) & block_bitmask:
                print(f" {block_idx} ", end='')
            else:
                print(f" _ ", end='')

    def _print_nice_params(self, slot_idx:int) -> None:
        first_block   = True
        block_bitmask = self.metadata[slot_idx if slot_idx != 0 else 0]

        if self.values_dsp == None:
            # Only used for printing
            packed_long = [struct.pack('>l', param) for param in self.values[0:constants.SCIVS_N_BLOCKS * constants.SCIVS_MAX_BLOCK_SIZE]]
            self.values_dsp = [struct.unpack('>f', param)[0] for param in packed_long] 

        for block_idx in range(0, constants.SCIVS_N_BLOCKS - 1, 1):
            if (1 << block_idx) & block_bitmask:
                if first_block == True:
                    print(f"   {block_idx}: ", end='')
                    first_block = False
                else:
                    print(f"\n                                     {block_idx}: ", end='')

                if slot_idx == 0:
                    param_idx = slot_idx * constants.SCIVS_N_BLOCKS * constants.SCIVS_MAX_BLOCK_SIZE + block_idx * constants.SCIVS_MAX_BLOCK_SIZE

                    print(",".join("{:.4E}".format(param) for param in self.values_dsp[param_idx:param_idx+8]), end = '')
                    param_idx += 8
                    print("\n                                        ", end = '')
                    print(",".join("{:.4E}".format(param) for param in self.values_dsp[param_idx:param_idx+8]), end = '')
                    param_idx += 8
                    print("\n                                        ", end = '')
                    print(",".join("{:.4E}".format(param) for param in self.values_dsp[param_idx:param_idx+8]), end = '')
                    param_idx += 8
                    print("\n                                        ", end = '')
                    print(",".join("{:.4E}".format(param) for param in self.values_dsp[param_idx:param_idx+8]), end = '')
                    continue

                param_idx = slot_idx * constants.SCIVS_N_BLOCKS * constants.SCIVS_MAX_BLOCK_SIZE + block_idx * constants.SCIVS_MAX_BLOCK_SIZE                
                print(",".join("0x{:08X}".format(param) for param in self.values[param_idx:param_idx+8]), end = '')
                param_idx += 8
                print("\n                                        ", end = '')
                print(",".join("0x{:08X}".format(param) for param in self.values[param_idx:param_idx+8]), end = '')
                param_idx += 8
                print("\n                                        ", end = '')
                print(",".join("0x{:08X}".format(param) for param in self.values[param_idx:param_idx+8]), end = '')
                param_idx += 8
                print("\n                                        ", end = '')
                print(",".join("0x{:08X}".format(param) for param in self.values[param_idx:param_idx+8]), end = '')

    def print_nice(self) -> None:
        print(f"VERSION : {self.version}")
        print(f"DSP SLOT: {self.dsp_slot}")
        print(f"SLOT  DEVICE       BLOCK                PARAMS")
        
        for slot_idx in range (1, constants.SCIVS_N_SLOTS_PARAMS):
            self._print_nice_blocks(slot_idx)
            self._print_nice_params(slot_idx)

            if slot_idx == self.dsp_slot:
                print(f"")
                self._print_nice_blocks(0)
                self._print_nice_params(0)

            print(f"  ")
