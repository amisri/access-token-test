from functools import partial
from typing import List


def preprocess_fgc_response(fgc_string_response: str) -> List[int]:
    blob = fgc_string_response.split(",")
    if type(blob[0]) == str:
        if blob[0].startswith("0x"):
            base = 16
        else:
            base = 10
        partial_int = partial(int, base=base)
        return list(map(partial_int, blob))
    else:
        raise TypeError("blob must be a string of a list of integers")
