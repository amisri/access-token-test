from typing import List

from regfgc3.params import ParamsV2, ParamsV4
from . import utils


def check_version(blob: list) -> int:
    return blob[0] >> 16


def decode_blob(fgc_string_response: str) -> List:
    """
    Decode the blob.
    """
    blob = utils.preprocess_fgc_response(fgc_string_response)
    version = check_version(blob)
    if version == 2:
        p = ParamsV2(blob)
        return p.convert_to_props()
    elif version == 4:
        p = ParamsV4(blob)
        return p.convert_to_props()
    else:
        raise NotImplementedError(f"blob version {version} not implemented")
