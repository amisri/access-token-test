import datetime
import os
import sys

sys.path.insert(0, os.path.abspath('../..'))

import fgc_parser_schemas

project = "fgc_parser_schemas"

author = fgc_parser_schemas.__authors__

version = fgc_parser_schemas.__version__
release = fgc_parser_schemas.__version__

copyright = "{0}, CERN".format(datetime.datetime.now().year)

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    'sphinx.ext.napoleon',
]

templates_path = ["_templates"]
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
html_theme = "sphinx_rtd_theme"
html_show_sphinx = False
html_show_sourcelink = True
autosummary_generate = True
autosummary_imported_members = True