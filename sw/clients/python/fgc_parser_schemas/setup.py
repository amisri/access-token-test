import os

from setuptools import setup, find_packages
from codecs import open


here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'fgc_parser_schemas', '__version__.py'), 'r', 'utf-8') as f:
    exec(f.read(), about)


requirements = {
    'core': [
        'pydantic>=1,<2',
        'ruamel.yaml'
    ],
    'test': [
        'pytest'
    ],
    'doc': [
        'sphinx',
        'acc_py_sphinx',
        'sphinx_rtd_theme',
    ],
}


setup(
    name="fgc_parser_schemas",
    version=about['__version__'],
    author=about['__authors__'],
    author_email=about['__emails__'],
    description="pydantic schemas for FGC pyparser",

    url='https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/fgc_parser_schemas/stable/index.html',
    packages=find_packages(),

    python_requires=">=3.6",
    install_requires=requirements['core'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    extras_require={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test'],
)
