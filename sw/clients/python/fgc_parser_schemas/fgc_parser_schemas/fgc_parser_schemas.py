from __future__ import annotations
from typing import List, Dict, Optional, Union
from datetime import datetime
from enum import Enum
from pydantic import BaseModel as PydanticBaseModel, validator, constr, conint, Extra, PrivateAttr
from ruamel.yaml.comments import CommentedMap

from .__version__ import __version__


class DictWrapper(dict):
    pass


class ListWrapper(list):
    pass


class BaseModel(PydanticBaseModel):
    class Config:
        # default behaviour - forbid unknown attributes
        extra = Extra.forbid
        # don't copy on validation
        copy_on_model_validation = False

    _seen_by_test: bool = PrivateAttr(default=False)
    _seen_by_extra_parser: bool = PrivateAttr(default=False)
    yaml_node: Optional[Union[DictWrapper, CommentedMap]]


class Code(BaseModel):
    id: int
    name: str
    title: str
    size_kb: int
    class_id: Optional[int]
    doc: Optional[str]
    processed_docs_: Optional[str]

    @property
    def full_name(self):
        if self.class_id:
            return f"{self.class_id}_{self.name}"
        return self.name

    @property
    def code_dir_name(self):
        return f"C{self.id:03d}_{self.full_name}"


class DimName(BaseModel):
    """Dim name"""
    name: str
    index: int  # needed?


class Error(BaseModel):
    """Error"""
    message: str
    number: int
    doc: str
    processed_docs_: Optional[str]


class Flag(BaseModel):
    """Flag"""
    name: str
    title: str
    value: int
    doc: str
    processed_docs_: Optional[str]

    @property
    def mask(self):
        return f"0x{self.value:08X}"


class Type(BaseModel):
    """Type"""
    name: str
    title: str
    size: str
    doc: str
    processed_docs_: Optional[str]

    limits: Optional[str]
    min: Optional[str]
    max: Optional[str]
    cmw_type: Optional[str]


class GetOption(BaseModel):
    """GetOption"""
    symbol: str
    title: str
    doc: str
    bitmask: int
    processed_docs_: Optional[str]

    @property
    def mask(self):
        return f"0x{self.bitmask:04X}"


class GetFunction(BaseModel):
    name: str
    title: str
    doc: str
    processed_docs_: Optional[str]

    types: List[Type]
    getopts: List[GetOption]


class SetFunction(BaseModel):
    name: str
    title: str
    doc: str
    processed_docs_: Optional[str]

    types: List[Type]

    getopts: Optional[List[GetOption]]


class SetIfFunction(BaseModel):
    name: str
    title: str
    doc: str
    processed_docs_: Optional[str]


class PropertyGroup(BaseModel):
    name: str
    title: str
    doc: str
    processed_docs_: Optional[str]

    java: bool = False


class Runlog(BaseModel):
    id: str  # TODO: check if prefix of 'RL_' is needed
    label: str
    length: int
    doc: str
    processed_docs_: Optional[str]


class Link(BaseModel):
    name: str
    url: str


class Area(BaseModel):
    name: str
    links: List[Link]
    areas: List['Area']


class Const(BaseModel):
    name: str
    val: str


class SymlistConst(BaseModel):

    class Meaning(str, Enum):
        NONE = "NONE"
        OFF = "OFF"
        ON = "ON"
        ERROR = "ERROR"
        WARNING = "WARNING"

    name: str
    title: str

    settable: bool = True
    value: Optional[str]
    doc: Optional[str]
    meaning: Optional[SymlistConst.Meaning] = None
    assertion: Optional[str]

    # for consts used in class
    action: Optional[str]
    cause: Optional[str]
    consequence: Optional[str]

    # not sure if needed, to be seen
    priority: Optional[int]
    # 63/symlists/spy:
    source: Optional[str]

    # symbol
    symbol: Optional[str]

    processed_docs_: Optional[str]


class Symlist(BaseModel):
    name: str
    title: str
    consts: List[SymlistConst]
    doc: Optional[str]
    meaning: Optional[SymlistConst.Meaning]

    processed_docs_: Optional[str]


class Property(BaseModel):
    name: constr(regex=r'^[A-Z]\w*$')
    full_name: str
    title: str
    type: Type
    doc: str

    parent: 'Property' = None

    children: List['Property'] = []

    flags: Dict[str, Flag] = dict()

    # extra types class/platform -> type
    specific_types: Dict[str, Type] = dict()

    limits: Optional[str]
    min: Optional[str]
    max: Optional[str]

    # class/platform specific limits/min/max

    specific_limits: Dict[str, str] = dict()
    specific_min: Dict[str, str] = dict()
    specific_max: Dict[str, str] = dict()

    # get and set
    get: Optional[GetFunction]
    set: Optional[SetFunction]
    setif: Optional[SetIfFunction]

    specific_get: Dict[str, GetFunction] = dict()
    specific_set: Dict[str, SetFunction] = dict()

    symlist: Optional[Symlist]
    class_symlist: Optional[str]
    class_symlists: Dict[str, Symlist] = dict()

    group: PropertyGroup

    operational: bool = False

    dynamic: bool = False

    from_spare_converter: bool = False

    kt: Optional[bool]

    kt_title: str = ""
    kt_units: str = ""
    kt_units_x: str = ""
    kt_units_y: str = ""
    kt_precision: str = ""
    kt_precision_x: str = ""
    kt_precision_y: str = ""

    # numels and maxels
    numels: Optional[str]
    specific_numels: Dict[str, str] = dict()

    maxels: Optional[str]
    specific_maxels: Dict[str, str] = dict()

    # DSP - platform/class -> value
    specific_dsp: Dict[str, str] = dict()

    # value
    value: Optional[str]
    specific_value: Dict[str, str] = dict()

    # operational
    specific_operational: Dict[str, bool] = dict()

    # pars
    pars: Optional[str]
    specific_pars: Dict[str, str] = dict()

    # step
    specific_step: Dict[str, str] = dict()

    # flags
    specific_flags: Dict[str, Dict[str, Flag]] = dict()

    # class flags - same as specific flags, but might contain additional ones (SYM_LST, LIMITS)
    # added when parsing the class properties
    class_flags: Dict[str, Dict[str, Flag]] = dict()


    # classes that use it
    classes: Dict[str, 'Class'] = dict()

    # property level (top level - 0 etc)
    @property
    def level(self):
        if self.parent is None:
            return 0
        return self.parent.level + 1

    # used for internal calculation
    limit_: Dict[str, str] = dict()
    class_index_: Optional[int] = None

    processed_docs_: Optional[str]


class Component(BaseModel):
    barcode: str
    label: str
    dallas_id: bool = False

    platforms: Optional[List[str]]
    multiple_ids: Optional[int]
    temp_prop: Optional[str]
    barcode_prop: Optional[str]

    sys_groups: Dict[str, int] = dict()

    regfgc3_def: Optional['RegFgc3BoardListBoardType']


class ComponentLabel(BaseModel):
    barcode: str
    label: str


class AnaChannel(BaseModel):
    index: int
    label: constr(max_length=13, regex=r'^\S+$')
    min_0V: float
    max_5V: float
    units: str

    @property
    def gain(self):
        return (self.max_5V - self.min_0V) / 4096

    @property
    def offset(self):
        return self.min_0V


class DigInput(BaseModel):
    index: int
    label: str
    fault: str
    one: str
    zero: str


class DigBank(BaseModel):
    index: int
    inputs: Dict[int, DigInput]


class DimType(BaseModel):
    comp: str
    position: str
    variant: str
    ana_channels: Dict[int, AnaChannel]
    dig_banks: Dict[int, DigBank]
    purpose: Optional[str]
    platform: Optional[str]
    index: int

    # used by FGC Codes
    platform_dim_type_idx: int = -1

    @property
    def type(self):
        return f"{self.comp}-{self.position}{self.variant}"


class LogMenu(BaseModel):
    name: str
    prop: str

    log_menu_name_idx: Optional[int]
    log_menu_prop_idx: Optional[int]


class Dim(BaseModel):
    bus_addr: int
    dim_idx: int
    name: str
    position: str
    type: str
    variant: int
    name_idx: int

    # link to DimType
    dim_type: Optional[DimType]

    @property
    def full_type(self):
        return f"{self.type}-{self.position}{self.variant}"


class SystemComponentGroup(BaseModel):
    tag: str
    required: Optional[int]
    index: int
    barcodes: Dict[str, int] = dict()


class SystemComponent(BaseModel):
    barcode: str
    required: Optional[int]
    group: Optional[SystemComponentGroup]


class System(BaseModel):
    comp: str
    type: str
    components: Dict[str, SystemComponent]
    groups: Dict[str, SystemComponentGroup]
    platform: Optional[str]
    label: Optional[str]

    log_menus: List[LogMenu]
    dims: Dict[int, Dim]


class XilinxRevision(BaseModel):
    doc: str

    processed_docs_: Optional[str]


class XilinxProgram(BaseModel):
    title: str
    revision: int
    doc: str
    revisions: List[XilinxRevision]
    id: str

    processed_docs_: Optional[str]


class Xilinx(BaseModel):
    logiware: str
    purpose: str
    type: str
    programs: List[XilinxProgram]
    tester: bool = False
    hardware: 'HW'
    ic_number: int = 0
    usercode: str


class Pin(BaseModel):
    label: str
    number: str
    sig_type: str
    use: str

    IO: Optional[str]
    c83_pin: Optional[str]  # possibly int or 'EXT'

    @validator('sig_type')
    def sig_type_must_be_one_of(cls, v):
        if v not in ('A', 'D', 'G', 'I', 'P', 'S', 'U'):
            raise ValueError('Invalid sig_type')
        return v


class Connector(BaseModel):
    id: str
    name: str
    type: str

    pins: List[Pin]


class HW(BaseModel):
    lhc_id: str
    epc_id: str
    title: str
    label: str
    doc: str

    platform: 'Platform'

    tester_platform: str
    tst_id: int
    tester_xilinxes: int

    xilinxes: List[Xilinx]
    connectors: List[Connector]

    subhw_str: Optional[List[str]]  # make private?
    usercode_dev: Optional[str]
    subhw: List['HW']

    processed_docs_: Optional[str]


class BootMenuReturn(BaseModel):
    type: str
    desc: str


class BootMenuArg(BaseModel):
    desc: str
    type: str

    optional: bool = False


class BootMenu(BaseModel):
    name: str

    platform: 'Platform'

    parent: Optional['BootMenu']

    id: str
    cname: str  # can be gen.

    fatal: bool = False

    confirm: bool
    recurse: bool

    doc: str = ""

    returns: List[BootMenuReturn]
    args: List[BootMenuArg]
    menus: List['BootMenu']
    function: Optional[str]

    processed_docs_: Optional[str]


class MemoryMap(BaseModel):
    class Endian(str, Enum):
        BIG = "big",
        LITTLE = "little"

    class Access(str, Enum):
        R = "R",
        W = "W",
        RW = "RW"
    name: str
    offset: str
    c_offset: int
    size: str
    numbits: int
    comment: str
    doc: Optional[str]

    parent: Optional['MemoryMap']
    children: List['MemoryMap']
    platform: 'Platform'

    endian: MemoryMap.Endian
    access: Optional[MemoryMap.Access]

    consts: str = ""
    const_types: Dict[str, bool]

    type: Optional[str]

    mask: Optional[int]
    shift: Optional[int]
    origin: Optional[bool]
    origin_offset: Optional[int]

    processed_docs_: Optional[str]

    @property
    def full_name(self):
        if self.parent and self.parent.full_name:
            return f"{self.parent.full_name}_{self.name}"
        return self.name

    @property
    def address(self):
        if self.parent:
            return self.parent.address + self.c_offset
        return self.c_offset

    @property
    def byte_address(self):
        # rounded down byte address
        return int(self.address / 8)

    @validator('consts')
    def consts_must_be_one_of(cls, v):
        for const in v.split():
            if const not in ("16bit",
                             "32bit",
                             "array",
                             "far",
                             "mask8",
                             "mask16",
                             "mask32",
                             "near",
                             "origin_near",
                             "origin_array",
                             "origin_far",
                             "page",
                             "shift",
                             "sizebits",
                             "sizebytes",
                             "sizewords"):
                raise ValueError(f'Invalid const: {const}')
        return v


class Platform(BaseModel):
    class Config:
        extra = Extra.allow
    id: int
    title: str
    name: str
    hardware: Dict[str, HW]
    sitemap: Optional[Area]
    boot_menu: Optional[BootMenu]
    memmaps: Dict[str, MemoryMap]

    classes: Dict[str, 'Class'] = dict()


class Assert(BaseModel):
    lhs: str
    rhs: str
    operator: str = "=="

    @validator('operator')
    def operator_must_be_one_of(cls, v):
        allowed_operators = ('==', '!=', '<=', '>=', '<', '>')
        if v not in allowed_operators:
            raise ValueError(f'Invalid operator: {v}. Must be one of: {allowed_operators}')
        return v


class DSP(BaseModel):
    name: str
    properties: Dict[str, Property]
    property_idxs: Dict[str, int]


# for Class' PubData
class Zone(BaseModel):
    @property
    def full_name(self):
        if self.parent and self.parent.full_name:
            return f"{self.parent.full_name}_{self.name}"
        return self.name

    @property
    def address(self):
        if self.parent:
            return self.parent.address + self.c_offset
        return self.c_offset

    @property
    def byte_address(self):
        # rounded down byte address
        return int(self.address / 8)

    name: str
    comment: str
    offset: str
    c_offset: int
    numbits: int
    size: str

    cls: 'Class'

    parent: Optional['Zone']

    children: List['Zone'] = []

    display_ord: Optional[int]
    display_bits: bool = False
    property: Optional[Property]

    # TODO: check if used - present in class 59
    manual_java: Optional[int]

    data_position: int


class Class(BaseModel):
    id: str
    name: str
    title: str
    platform: Platform
    doc: str

    consts: Dict[str, Const]
    asserts: List[Assert]

    symlists: Dict[str, Symlist]

    sitemap: Optional[Area]

    kt: Optional[bool]

    # optional
    dsps: Dict[str, DSP] = dict()

    # properties
    properties: Dict[str, Property] = dict()

    # pars functions
    parsfunctions: List[str] = []

    # public data
    pubdata: Optional[Zone]

    # class symlists
    class_symlists: Dict[str, Symlist] = dict()

    # symtab consts
    symtab_consts: Dict[str, SymlistConst] = dict()

    # property levels
    property_levels: Dict[int, List[Property]] = dict()

    processed_docs_: Optional[str]


class RegFgc3Device(BaseModel):
    name: str
    variant: str

    variant_symbol: Optional[SymlistConst]


class RegFgc3Board(BaseModel):
    slot: conint(gt=0, lt=32)
    name: str
    board_symbol: Optional[SymlistConst]

    devices: List[RegFgc3Device]

    slots: List[int]


class RegFgc3Crate(BaseModel):
    title: str
    barcode: constr(regex=r'^HC([A-Z]{4}[A-Z_])___$')

    boards: Dict[str, RegFgc3Board]

    @property
    def type(self):
        # Extract the crate name from the barcode (HCxxxxx___)
        return self.barcode[2:].rstrip("_")


class RegFgc3VariantReadWriteBlockElement(BaseModel):
    class AllowedTypes(str, Enum):
        FLOAT       = "float"
        INT8_T      = "int8_t"
        INT16_T     = "int16_t"
        INT32_T     = "int32_t"
        UINT8_T     = "uint8_t"
        UINT16_T    = "uint16_t"
        UINT32_T    = "uint32_t"

    name: str
    type: RegFgc3VariantReadWriteBlockElement.AllowedTypes
    size_bits: conint(ge=1, le=32)
    parent: 'RegFgc3VariantReadWriteBlock'
    number: conint(le=30)
    default_value: Optional[str]
    doc: Optional[str]

    symlist: Optional[Symlist]

    scaling_gain: Optional[int]
    scaling_offset: Optional[int]
    unit: Optional[str]

    limits: Optional[str]
    min: Optional[Union[int, float]]
    max: Optional[Union[int, float]]

    processed_docs_: Optional[str]


class RegFgc3VariantReadWriteBlock(BaseModel):
    class Type(str, Enum):
        Read = "Read"
        Write = "Write"
    number: conint(ge=0, le=6)
    title: str
    doc: Optional[str]

    critical: bool = False

    elements: List[RegFgc3VariantReadWriteBlockElement]

    type: RegFgc3VariantReadWriteBlock.Type

    processed_docs_: Optional[str]


class RegFgc3Variant(BaseModel):
    board: Optional[str]
    variant: Optional[str]
    api_version: Optional[str]
    instance: Optional[str]
    doc: Optional[str]

    title: Optional[str]

    read_blocks: List[RegFgc3VariantReadWriteBlock]
    write_blocks: List[RegFgc3VariantReadWriteBlock]

    processed_docs_: Optional[str]


class RegFgc3VariantDict(BaseModel):
    variant_symbol: SymlistConst
    variant: Dict[str, RegFgc3Variant]
    # also here others


class RegFgc3DeviceDict(BaseModel):
    variants: Dict[str, RegFgc3VariantDict] = {}


class RegFgc3BoardDict(BaseModel):
    devices: Dict[str, RegFgc3DeviceDict] = {}

    # copy of RegFgc3BoardListBoardType
    name: Optional[str]
    eda_number: Optional[str]

    barcodes: Optional[List[str]]

    board_symbol: Optional[SymlistConst]


class RegFgc3(BaseModel):
    converters: Dict[str, RegFgc3Crate]
    boards: Dict[str, RegFgc3BoardDict]


class RegFgc3BoardListBoardType(BaseModel):
    name: str
    eda_number: str

    barcodes: List[str]

    board_symbol: Optional[SymlistConst]


class RegFgc3BoardsList(BaseModel):
    title: str
    doc: str

    board_types: Dict[str, RegFgc3BoardListBoardType]

    processed_docs_: Optional[str]


# for recursive annotations/forward decl to work
Property.update_forward_refs()
Component.update_forward_refs()
Area.update_forward_refs()
HW.update_forward_refs()
Xilinx.update_forward_refs()
BootMenu.update_forward_refs()
MemoryMap.update_forward_refs()
Zone.update_forward_refs()
RegFgc3VariantReadWriteBlockElement.update_forward_refs()
SymlistConst.update_forward_refs()
RegFgc3VariantReadWriteBlock.update_forward_refs()


class SuperDict(BaseModel):
    # schema version of fgc_parser_schemas
    schema_version: str = __version__

    # creation date
    creation_date: datetime = datetime.now()

    codes: List[Code] = []
    dim_names: Dict[str, DimName] = dict()

    components: Dict[str, Component] = dict()

    component_labels: Dict[str, ComponentLabel] = dict()

    errors: List[Error] = []
    flags: Dict[str, Flag] = dict()

    getoptions: Dict[str, GetOption] = dict()

    types: Dict[str, Type] = dict()

    gets: Dict[str, GetFunction] = dict()
    sets: Dict[str, SetFunction] = dict()
    setifs: Dict[str, SetIfFunction] = dict()

    property_groups: Dict[str, PropertyGroup] = dict()
    runlog: Dict[str, Runlog] = dict()

    sitemap: List[Area] = []

    consts: Dict[str, Const] = dict()

    symlists: Dict[str, Symlist] = dict()

    # Dict [symlist_name, Dict[class_id, Symlist]]
    class_symlists: Dict[str, Dict[str, Symlist]] = dict()

    properties: Dict[str, Property] = dict()

    dim_types: Dict[str, DimType] = dict()

    log_menu_names: Dict[str, int] = dict()
    log_menu_props: Dict[str, int] = dict()

    platforms: Dict[str, Platform] = dict()

    systems: Dict[str, System] = dict()

    classes: Dict[str, Class] = dict()

    regfgc3: Optional[RegFgc3]
