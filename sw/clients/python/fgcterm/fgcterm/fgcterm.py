import argparse
import functools
import getpass
import signal
import sys
import cernrbac
import pyfgc

parser = argparse.ArgumentParser()
parser.add_argument('-f', dest='infile', type=argparse.FileType('r'), help='Send file instead of interactive terminal',
                    default=sys.stdin)
parser.add_argument('-l', dest='outfile', type=argparse.FileType('w'), help='File to which to log terminal',
                    default=sys.stdout)
parser.add_argument('-p', dest='pwdfile', type=argparse.FileType('r'), help='File containing password')
parser.add_argument('-r', dest='channel', type=int, help='Start a remote terminal to channel', default=1)
parser.add_argument('-t', dest='tokenfile', type=argparse.FileType('r'), help='File containing RBAC token')
parser.add_argument('gateway', type=str, help='Gateway host')
parser.add_argument('username', type=str, help='Username', nargs='?')
args = parser.parse_args()

# if the password AND the token is given
if args.pwdfile is not None and args.tokenfile is not None:
    print("Please do not specify both a password file and a token file.")
    sys.exit(1)

# username
if args.username is None:
    args.username = getpass.getuser()

# password
args.pwd = None
if args.pwdfile is not None:
    args.pwd = '\n'.join(args.pwdfile.readlines()).strip()
    args.pwdfile.close()

# token
args.token = None
if args.tokenfile is not None:
    args.token = '\n'.join(args.tokenfile.readlines())
    args.tokenfile.close()

# if nor the password nor the token has been given, rbac token has to be fetched
if args.token is None:
    rbac = cernrbac.RBAC()
    rbac.authenticate_by_login(getpass.getuser(), getpass.getpass() if args.pwd is None else args.pwd)
    args.token = rbac.token


# connect to the gateway
async def main(loop):
    fgc = await pyfgc.FgcAsync.instance(args.gateway, 1905, loop)

    await pyfgc.do_or_die(functools.partial(fgc.set, 'CLIENT.TOKEN', args.token))

    # output
    def output_callback(byte):
        args.outfile.write(byte.decode("unicode_escape", errors="ignore"))

    # input conversion map
    conversion_map = {'\n': '\n\r'}

    # convert function
    def input_convert(character):
        return conversion_map[character] if character in conversion_map else character

    # input
    async def input_loop():
        while True:
            character = await loop.run_in_executor(None, functools.partial(args.infile.read, 1))
            byte = input_convert(character).encode()
            await fgc.send(byte)

    # enable rterm mode
    await pyfgc.FgcUtil.do_or_die(functools.partial(fgc.enable_rterm, args.channel, output_callback))

    # ask for a terminal reset
    await fgc.send(b'\x1b\x1b')

    # input loop
    await input_loop()


def signal_handler(signal, frame):
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
pyfgc.FgcUtil.run_sync(main)
