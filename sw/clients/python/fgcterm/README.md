# FGC remote terminal

This module is the new python equivalent of the old fgcterm.cpp script

## Run

```python
python3 -m fgcterm.fgcterm [options]
```

## Man

```text
usage: fgcterm.py [-h] [-f INFILE] [-l OUTFILE] [-p PWDFILE] [-r CHANNEL]
                  [-t TOKENFILE]
                  gateway [username]

positional arguments:
  gateway       Gateway host
  username      Username

optional arguments:
  -h, --help    show this help message and exit
  -f INFILE     Send file instead of interactive terminal
  -l OUTFILE    File to which to log terminal
  -p PWDFILE    File containing password
  -r CHANNEL    Start a remote terminal to channel
  -t TOKENFILE  File containing RBAC token
```