# Pyfgc Test Framework
## 2nd iteration

![png](docs/images/intro.png)

![png](docs/images/structure.png)

# Framework Glossary

#### Pyfgc wrappers

| Function  | Description |
| ------------- | :- |
| __get__ | PyFgc get wrapper - get property value |
| __set__ | PyFgc set wrapper - set property value |
| __subscribe__ | Wrapper of pyfg.subscribe — context manager to subscribe to one or more properties |
| __create_manual_subscription_session__ | Wrapper of pyfgc.create_manual_subscription_session - requires the user to manually stop the subscription. Useful to have several subscriptions in parallel, and stop them as desired. |

#### Auxiliary functions

| Function  | Description |
| ------------- | :- |
| __test_set__ | Quick test to set an FGC property, optionally compare the result against an expected exception |
| __test_get__ | Quick test to get a property from an FGC and compare it against an expected value/exception |
| __wait_until__ | Wait until condition in passed function returns true. It will reexecute at a pooling_period interval, until a timeout is reached. |
| __sleep_except__ | Wait given time, but confirm that during whole sleep passed except_function is always false, checking with a frequency of polling_period (seconds) |
| __parse_opts__ | Parse command line options |
| __load_config__ | Load properties into FGC from a configuration file |
| __set_protocol__ | Set protocol to use (sync, serial, or rda) |
| __gateways__ | dictionary of available gateways |
| __devices__ | dictionary of available FGCs |
| __zip_table__ | Serialize a list of time and a list of references into a table function |
| __unzip_table__ | Deserialise table function into tuple (time_vector, ref_vector) |
| __get_rbac_token__ | Get rbac token using kerberos and SSO or user/password |

#### Main program

| Function  | Description |
| ------------- | :- |
| __get_gateway__ | Get all the info on a specific gateway |
| __get_devices_from_gateway__ | Get list of FGC devices in a gateway |
| __get_property_info__ | Get info about a property |
| __assure_main__ | Make sure FGC iss running the Main program |
| __is_in_main__ | Check if FGC is running the Main program |
| __set_pc__ | Set FGC STATE.PC |
| __sync_fgc__ | Synchronize FGC |
| __get_available_timing_users__ | Retrieve available timing users for a given fgc or gateway |
| __get_available_logs__ | Retrieve available log names for a device |
| __get_phase_aligned_timestamp__ | Get the next phase aligned timestamp after $delay seconds. Useful to play a function aligned with the FGC regulation period, which means if we retrieve the log, the firstSampleTime will be equal to the timeOrigin of the log. |


#### Boot program

| Function  | Description |
| ------------- | :- |
| __is_in_boot__ | Check if FGC is in boot program |
| __assure_boot__ | Make sure the FGC is in the boot program |
| __boot_send_command__ | Send a command to the FGC boot |
| __boot_global_reset__ | Do a global reset to the FGCs in the gateway |
| __boot_power_cycle__ | Power cycle an FGC |
| __boot_slow_watchdog__ | Send TOGGLE_SLOW_WD_TRIG_ENABLE boot command |
| __Boot_menu_options__ | Enum with available boot commands |
| __boot_assure_dsp__ | Make sure DSP is alive by reseting the FGC up to 10 times and checking the result of the DSP_IS_ALIVE test |


#### Log utilities

| Function  | Description |
| ------------- | :- |
| __create_spy_acquisition__ | Creates a Fortlogs acquisition for us to store log comparisons and later access in Powerspy |
| __compare_cyclic_logs__ | Compares 2 cycle bounded logs point by point, uploads result to Fortlogs |
| __compare_non_cyclic_logs__ | Compares 2 IDLE or DIRECT logs point by point, uploads result to Fortlogs |
| __import_log_from_csv__ | Imports log from a CSV into a log object |
| __get_reference_logs__ | Retrieves logs referenced in PowerSpy for a given script_name and script_description |

## Fortlogs
![gif](docs/images/fortlogs-swagger.gif)

## PowerSpy
![png](docs/images/powerspy.png)

# FGC State Machine
![png](docs/images/fgc-state-machine.png)

The FGC has 3 ways of managing the reference based on the operating states:

- CYCLING - Pre-armed functions are played synchronously with timing events
- IDLE - Function plays at a specific time (non-cyclic)
- DIRECT - Asynchronous setting of an arbitrary final reference (non-cyclic)

#### Compare logs - function signatures


```python
def compare_cyclic_logs(fgc_name: str,         # name of the fgc device
                        log_name: str,         # name of the log to capture
                        test_name: str,        # name we want to give to this comparison
                        ref_log = None,        # reference log object, if None the framework will search fortlogs for a reference
                        user=None,             # timing user 
                        duration=0,            # duration of the log to retrieve
                        time_offset=0,         # retrieve the log witha slight time offset
                        threshold=1.0E-4,      # threshold to use for the point by point comparison
                        only_signals = []      # compare only a subset of signals inside the log
                       ):
    ...
    
    return result, log, ref_log, diff_log
```


```python
def compare_non_cyclic_logs(fgc_name: str,        # name of the fgc device
                            log_name: str,        # name of the log to capture
                            test_name: str,       # name we want to give to this comparison
                            ref_log = None,       # reference log object, if None the framework will search fortlogs for a reference
                            start_time=0,         # timestamp of the beginning of the log, if 0 the value in REF.RUN is used
                            duration=0,           # duration of the log to retrieve
                            threshold=1.0E-4,     # threshold to use for the point by point comparison
                            only_signals= []      # compare only a subset of signals inside the log
                           ):
    ...
    
    return result, log, ref_log, diff_log
```

# Let's run some code

## 1. Authentication  




### The framework automatically tries to authenticate on the first request made to an FGC  

###  
1. Check if the environment variable RBAC_TOKEN_SERIALIZED is present, it can be set running __$ eval(rbac-authenticate -e)__ in the same terminal session  
2. Check for an existing kerberos token, if found, exchange it for an rbac token using pyfgc_rbac 

### We can also authenticate manually


```python
import pyfgc_test_framework as test_fw
import getpass
import time
```


```python

username = getpass.getuser()
password = getpass.getpass()
```


```python
test_fw.get_rbac_token(username, password)
```

## 2. Let's get and set some properties


```python
TESTFGC_A = "RFNA.866.04.ETH2"
TESTFGC_B = "RFNA.866.05.ETH2"

SCRIPT_NAME = "jupyter_notebook"
SCRIPT_DESCRIPTON = "test_framework_demo"
```

#### Let's get the device.name


```python
test_fw.get(TESTFGC_B, "device.name").value
```
    'RFNA.866.05.ETH2'

#### Now some other properties


```python
test_fw.set(TESTFGC_A, "TEST.INT8U", "123")
test_fw.get(TESTFGC_A, "TEST.INT8U").value
```

    '123'

```python
test_fw.test_set(TESTFGC_A, "TEST.CHAR", "ABC")
```
    True
```python
test_fw.test_get(TESTFGC_A, "TEST.CHAR", "ABC")
```
    True
```python
test_fw.get(TESTFGC_A, "TEST.CHAR").value
```
    'ABC'

#### Get info about a specific property


```python
test_fw.get_property_info(TESTFGC_A, "TEST.INT32S")
```

    {'struct prop *': '0x06003B80',
     'parent *': '0x060035E0',
     'sym_idx': '0x00ED',
     'flags': '0x0064',
     'dynflags': '0x00',
     'type': '11 INT32S',
     'setif_func_idx': '0 0x00000',
     'set_func_idx': '15 0xFFE7E4BA',
     'get_func_idx': '16 0xFFE74FAC',
     'n_elements': '0',
     'max_elements': '16',
     'range': '0xFFE8F368',
     'value': '0x3C40',
     'dsp_idx': '0',
     'nvs_idx': '0xFFFF',
     'is_setting': '1'}

#### Load a config file


```python
properties, fails = test_fw.load_config(TESTFGC_A, "./configs/RPAAO.866.02.ETH8.MOD.SHORT.txt")
```

    RFNA.866.04.ETH2 Error setting DEVICE.PPM -> bad state
    RFNA.866.04.ETH2 Error setting DEVICE.MULTI_PPM -> bad state
    RFNA.866.04.ETH2 Error setting INTER_FGC.MASTER -> bad state
    RFNA.866.04.ETH2 Error setting INTER_FGC.SLAVES -> bad state
    RFNA.866.04.ETH2 Error setting INTER_FGC.DECO.PARTNERS -> bad state
    RFNA.866.04.ETH2 Error setting LIMITS.V.POS -> bad state
    RFNA.866.04.ETH2 Error setting LOAD.SELECT -> bad state
    RFNA.866.04.ETH2 Error setting MEAS.B.FIR_LENGTHS -> bad state
    RFNA.866.04.ETH2 Error setting MEAS.B.DELAY_ITERS -> bad state
    RFNA.866.04.ETH2 Error setting MEAS.I.FIR_LENGTHS -> bad state
    RFNA.866.04.ETH2 Error setting MEAS.I.DELAY_ITERS -> bad state
    RFNA.866.04.ETH2 Error setting REG.MODE_CYC -> bad state
    RFNA.866.04.ETH2 Error setting REG.B.PERIOD_ITERS -> bad state
    RFNA.866.04.ETH2 Error setting VS.ACTUATION -> bad state
    RFNA.866.04.ETH2 Error setting VS.ACT_DELAY_ITERS -> bad state
    RFNA.866.04.ETH2 Error setting VS.BLOCKABLE -> bad state

```python
fails
```
    16
#### We are in the wrong state to set some of the properties, let's set STATE.PC to OFF and try again

```python
test_fw.set_pc(TESTFGC_A, "OF")
```
    True
```python
properties, fails = test_fw.load_config(TESTFGC_A, "./configs/RPAAO.866.02.ETH8.MOD.SHORT.txt")
```


```python
fails
```
    0
```python
properties
```

    {'DEVICE.PPM': 'ENABLED',
     'DEVICE.MULTI_PPM': 'DISABLED',
     'DEVICE.SPARE_ID': '0',
     'INTER_FGC.BROADCAST': 'DISABLED',
     'INTER_FGC.MASTER': 'NONE',
     'INTER_FGC.SLAVES': 'NONE',
     'INTER_FGC.SIG_SOURCES': 'NONE',
     'INTER_FGC.DECO.D': '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
     'INTER_FGC.DECO.PARTNERS': 'NONE',
     'I_PROBE.CAPA.GAIN': '0.0000000E+00',
     'LIMITS.V.POS': '3.9000001E+00,7.5000000E+01,7.5000000E+01,7.5000000E+01',
     'LOAD.SELECT': '0',
     'LOAD.GAUSS_PER_AMP': '1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
     'LOG.OASIS.SUBSAMPLE': '1',
     'MEAS.B.FIR_LENGTHS': '0,0',
     'MEAS.B.DELAY_ITERS': '1.1000000E+00',
     'MEAS.I.FIR_LENGTHS': '2,0',
     'MEAS.I.DELAY_ITERS': '5.0999999E-01',
     'MODE.PC_ON': 'CYCLING',
     'MODE.RT_NUM_STEPS': '0',
     'MODE.PRE_FUNC': 'OPENLP_MINRMS',
     'REF.ILC.FUNCTION': '9.6711755E-01,-1.8891206E+00,1.7405366E+00,-1.2210138E+00,2.9925280E+00,-6.2532797E+00,3.3476253E+00,2.7382412E+00,-3.0008800E+00,3.0603766E+00,-1.5313487E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
     'REF.DEFAULTS.V.MIN_RMS': '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
     'REF.V_FEED_FWD.SIGNAL': 'DISABLED',
     'REG.MODE_CYC': 'I',
     'REG.B.PERIOD_ITERS': '10,100,100,100',
     'REG.I.EXTERNAL.OP.T': '3.7096541E+00,-1.8269564E+00,-3.2536355E-01,-8.1391025E-01,-5.1656473E-01,1.2956418E-01,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
     'SWITCH.POLARITY.AUTO': 'DISABLED',
     'VS.ACTUATION': 'VOLTAGE_REF',
     'VS.ACT_DELAY_ITERS': '1.0000000E+00',
     'VS.BLOCKABLE': 'ENABLED',
     'V_PROBE.CAPA.GAIN': '7.5000000E+00'}

#### We can also change the protocol used for the FGC requests


```python
test_fw.set_protocol('rda')
```
    True
```python
test_fw.test_get(TESTFGC_A, "device.ppm", "enabled")
```
    True
## 3. Let's subscribe to properties


```python
test_fw.set_pc(TESTFGC_A, "CY")
```

#### We can use the subscribe context manager, that allows us to subscribe to one or more properties.  The callback is optional, the listener keeps a dictionary with the data of each of the properties.


```python
def callback_fun(data):
    print(f'Heeeey {data}')

with test_fw.subscribe(TESTFGC_A, ["MEAS.V.REF"], callback=callback_fun) as listener:
    time.sleep(3)
    
```

    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>
    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>
    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>
    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>
    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>
    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>
    Heeeey <FgcSingleResponse: 'tag=' 'value=0.0000000E+00' 'err_code=' 'err_msg='>


#### Or we can use the standalone create_manual_subscription_session() function to subscribe to one property only. We are in charge of stoping the subscription and deleting the connection.


```python
collected_data = list()

def callback_fun(data):
    collected_data.append(data)
    print(f'Received:\n{data.value}')

subscription = test_fw.create_manual_subscription_session(TESTFGC_A, "MEAS.V", callback=callback_fun)
time.sleep(1.5)

subscription.unsubscribe()
del subscription
```

    Received:
    DELAY_ITERS:5.1999998E-01
    REF:0.0000000E+00
    VALUE:0.0000000E+00
    
    Received:
    DELAY_ITERS:5.1999998E-01
    REF:0.0000000E+00
    VALUE:0.0000000E+00
    
    Received:
    DELAY_ITERS:5.1999998E-01
    REF:0.0000000E+00
    VALUE:0.0000000E+00

## 4. Testing Boot and Main programs

### Use the "assure" functions to make sure the device is in that state


```python
test_fw.set_protocol("rda")
test_fw.assure_boot(TESTFGC_B, from_status_data=True)
```
    True
```python
test_fw.is_in_boot(TESTFGC_B, from_status_data=False)
```
    True
### To send commands to the Boot:


```python
# !pip install nest_asyncio
import nest_asyncio
nest_asyncio.apply()

command = test_fw.Boot_menu_options.BUILD_INFO.value

result, response = test_fw.boot_send_command(TESTFGC_B, command)

print(f"Result is {result}\n")
print(response.decode("utf-8"))
```

    Result is OKAY
    
    $0,8,Build info
    --------------MCU--------------
    02/12/2021 17:20:44
    cs-ccr-teepc2.cern.ch
    Linux 3.10.0-1160.2.2.el7.x86_64 x86_64
    rx-elf-gcc.exe (GCC) 4.7.4 20140125 (prerelease) 
    rmurillo:sa
    /afs/cern.ch/work/r/rmurillo/projects/fgc/sw/fgc/fgc3/rx610/60
    --------------DSP--------------
    02/12/2021 17:08:36
    cs-ccr-teepc2.cern.ch
    Linux 3.10.0-1160.2.2.el7.x86_64 x86_64
    TMS320C6x C/C++ Compiler               
    rmurillo:sa
    /afs/cern.ch/work/r/rmurillo/projects/f
    $3
    


#### If we don't want to wait for the result we can pass the flag no_wait =True


```python
command = test_fw.Boot_menu_options.RUNMAIN.value

test_fw.boot_send_command(TESTFGC_B, command, no_wait=True)
```
    ('', '')
#### We can also assure we are running the main program


```python
test_fw.assure_main(TESTFGC_B, wait_to_sync=True) 
```
    True
```python
test_fw.sync_fgc(TESTFGC_B)
```


```python
test_fw.set_pc(TESTFGC_A, "OFF")
```
    True
## 5. Log utilities and comparisons

### We can check the available logs and timing users for a given FGC


```python
test_fw.get_available_logs(TESTFGC_A)
```

    {'ACQ': 'LOG.SPY.DATA[0]',
     'ACQ_MPX': 'LOG.SPY.DATA[1]',
     'I_MEAS': 'LOG.SPY.DATA[4]',
     'I_MEAS_MPX': 'LOG.SPY.DATA[5]',
     'I_REG': 'LOG.SPY.DATA[8]',
     'I_REG_MPX': 'LOG.SPY.DATA[9]',
     'V_REF': 'LOG.SPY.DATA[10]',
     'V_REF_MPX': 'LOG.SPY.DATA[11]',
     'I_1KHZ': 'LOG.SPY.DATA[14]',
     'TEMP': 'LOG.SPY.DATA[16]',
     'V_AC_HZ': 'LOG.SPY.DATA[17]',
     'FLAGS': 'LOG.SPY.DATA[18]',
     'A0_DIM1': 'LOG.SPY.DATA[47]',
     'EVENTS': 'LOG.EVT',
     'CYCLES': 'LOG.CYCLES',
     'TIMING': 'LOG.TIMING',
     'CONFIGURATION': 'CONFIG.SET'}

```python
test_fw.set_protocol('tcp')
test_fw.get_available_timing_users(TESTFGC_A)
```

    {'ALL': 0,
     'ZERO': 1,
     'LHC1A': 4,
     'LHCIND1': 10,
     'MD4': 15,
     'NORMGPS': 18,
     'TOF': 23,
     'AD': 24}



### Comparing CYCLIC and NON-CYCLIC logs

#### Let's simulate a cycle and put the FGC in clycling mode


```python
test_fw.set(TESTFGC_A, "fgc.cyc.sim", "1201, 2201")
test_fw.set(TESTFGC_A, "REF.TABLE.FUNC.VALUE(12)", "0.00|0.00,0.1|0.00,0.20|2.50,0.30|5.00,0.70|2.50,0.80|0.00")
test_fw.set(TESTFGC_A, "ref.func.play(12)", "enabled")
```
    <pyfgc.fgc_response.FgcResponse at 0x7fc299bc3e90>
```python
test_fw.set_pc(TESTFGC_A, 'CY')
```
    True
#### Before we can run comparisons we need to create a fortlogs acquision to store the comparisons


```python
test_fw.create_spy_acquisition(SCRIPT_NAME, SCRIPT_DESCRIPTON)
```

    {'acquisition_id': 5172,
     'time_started': '2022-03-02T10:07:48.690470+01:00',
     'comment': '',
     'trash': False,
     'tags': [],
     'favorite_users': [],
     'log_count': 0,
     'last_access': '2022-03-02T10:07:48.690470+01:00',
     'script_name': 'jupyter_notebook',
     'script_description': 'test_framework_demo',
     'user': {'username': 'fmendesr', 'name': 'Filipe Mendes Rosa', 'user_id': 14},
     'devices': [],
     'is_reference': False}

#### Now we can compare the I_MEAS log from user 12 with its reference log


```python
result, log, ref_log, difference = test_fw.compare_cyclic_logs(TESTFGC_A, 
                                                               "I_MEAS", 
                                                               "compare_cycling", 
                                                               user=12)
```


```python
[sig.name for sig in log.signals]
```
    ['I_MEAS_(A)', 'I_REF_DELAYED_(A)', 'I_ERR_(A)']
```python
%matplotlib inline
import matplotlib.pyplot as plt
plt.plot(log.signals[0].samples)
```
    [<matplotlib.lines.Line2D at 0x7fc2948e0150>]
    
![png](docs/images/output_81_1.png)
    

```python
print(result)
test_fw.set_pc(TESTFGC_A, "OFF")
```
    True
    True
#### It should fail, as we have no reference log stored for this device and this test name

#### We can also compare a log that started at a specific timestamp, like 10 seconds ago


```python
test_fw.set_pc(TESTFGC_A, 'IDLE')
```
    True
```python
timestamp = time.time() - 10
duration = 3

result, log, ref_log , diff = test_fw.compare_non_cyclic_logs(TESTFGC_A,
                                                    "I_MEAS",
                                                    test_name="compare_idle_1",
                                                    ref_log=None,  # Since it is None, it will search for a reference in fortlogs
                                                    start_time=timestamp,
                                                    duration=duration)
print(result)
```
    False
```python
print(f"log with {len(log.signals[0].samples)} samples, "
      f"duration of {(log.timeOrigin - log.firstSampleTime):.1f} seconds, "
      f"{(time.time() - log.firstSampleTime):.2f} seconds ago, "
      f"average value of signals {[ (sum(sig.samples)/len(sig.samples)) for sig in log.signals ]}")
```

    log with 30001 samples, duration of 3.0 seconds, 15.35 seconds ago, average value of signals [-4.868278899997726e-36, -4.868278899997726e-36, 0.0]

#### So let's play a function and try again


```python
time_vector = [0.0, 0.1, 0.2, 0.3, 0.7, 0.8]
references = [0.0, 0.0, 2.5, -5.0, 2.5, 0.0]
table_function = test_fw.zip_table(time_vector, references)
table_function
```
    '0.0|0.0,0.1|0.0,0.2|2.5,0.3|-5.0,0.7|2.5,0.8|0.0'
```python
%matplotlib inline
import matplotlib.pyplot as plt
plt.plot(time_vector, references)
```
    [<matplotlib.lines.Line2D at 0x7fc297e5a550>]
    
![png](docs/images/output_90_1.png)
    



```python
test_fw.play_table_function(TESTFGC_A, table_function)
time.sleep(3)

result, log, ref_log, diff = test_fw.compare_non_cyclic_logs(TESTFGC_A,
                                                    "I_MEAS",
                                                    test_name="compare_idle_2",
                                                    ref_log=None,  # Since it is None, it will search for a reference in fortlogs
                                                    duration=1.2)
print(result)
```
    True
#### Let's see a preview of the log we got


```python
%matplotlib inline
import matplotlib.pyplot as plt

sample_time = [log.period*i for i in range(len(log.signals[0].samples))]
plt.plot(sample_time, log.signals[0].samples)

#print(log.)
```
    [<matplotlib.lines.Line2D at 0x7fc26d503390>]
    
![png](docs/images/output_93_1.png)


### And finally we can observe the result of our log comparisons in PowerSpy  
  
  
For now only available in pre-production:  
https://ccs-webtools-preprod.cern.ch/powerspy

## Test Manager

We need an application to manage all our tests. To trigguer test execution on demand or nightly, with what configurations and on what devices.
![Screenshot%202022-02-28%20at%2014.47.26.png](attachment:Screenshot%202022-02-28%20at%2014.47.26.png)

# Thank you!
