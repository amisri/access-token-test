.. pyfgc_test_framework documentation master file, created by
   sphinx-quickstart on Wed Jan 20 11:39:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyfgc_test_framework's documentation!
================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   api.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
