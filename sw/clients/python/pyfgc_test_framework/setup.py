import os

from setuptools import setup, find_packages
from codecs import open


here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'pyfgc_test_framework', '__version__.py'), 'r', 'utf-8') as f:
    exec(f.read(), about)


with open("README.md", "r") as fh:
    description = fh.read()

# add my dependencies here
requirements = {
    'core': [
        'pyfgc[kerberos]>=2.0.2',
        'pyfgc_log>=1.3.4',
        'fortlogs-schemas>=1.8.0',
        'requests>=2.25.1'
    ],
    ':sys_platform!="win32"': [
        'pyrda3',
    ],
    'test': [
        'pytest',
    ],
    'doc': [
        'sphinx',
        'sphinx_rtd_theme',
    ],
}


setup(
    name="pyfgc_test_framework",
    version=about['__version__'],
    author="Filipe Rosa",
    author_email="filipe.rosa@cern.ch",
    description="Framework to write tests for FGCs",
    long_description=description,  # this is a README file
    long_description_content_type='text/markdown',

    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/pyfgc_test_framework/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/pyfgc_test_framework',
    },
    packages=find_packages(),

    python_requires=">=3.7",
    install_requires=requirements['core'],
    extras_require={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test'],
)
