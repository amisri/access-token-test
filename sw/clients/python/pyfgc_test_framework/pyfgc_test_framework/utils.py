from contextlib import contextmanager
import sys
import getopt
import os
import re
import pyfgc
import pyfgc_log
import time
import struct
import logging
from typing import List, Callable, Tuple, ContextManager, Dict
from contextlib import contextmanager
from pyfgc.fgc_session import FgcSession
from pyfgc.responses.fgc_response import FgcResponse
from pyfgc_test_framework.consts import TIMEOUT, POLLING_PERIOD, default_protocol, logger
from pyfgc_test_framework import authenticate

fw_protocol = default_protocol

# PyFgc wrappers


def get(device: str, prop: str, getopt: str = None) -> pyfgc.FgcResponse:
    """Wrapper of pyfgc.get"""
    return pyfgc.get(device, prop, get_option=getopt, protocol=fw_protocol, rbac_token=authenticate.get_rbac_token())


def set(device: str, prop: str, value) -> pyfgc.FgcResponse:
    """Wrapper of pyfgc.set"""
    return pyfgc.set(device, prop, value, protocol=fw_protocol, rbac_token=authenticate.get_rbac_token())


@contextmanager
def subscribe(device: str, properties: List[str], callback: Callable[[FgcResponse], None]) -> ContextManager[FgcSession]:
    """Wrapper of context manager pyfgc.subscribe"""
    with pyfgc.subscribe(device, properties, callback, rbac_token=authenticate.get_rbac_token()) as sub_session:
        yield sub_session


def create_manual_subscription_session(device: str, prop: str, callback: Callable[[FgcResponse], None]) -> FgcSession:
    """Wrapper of pyfgc.create_manual_subscription_session"""
    return pyfgc.create_manual_subscription_session(device, prop, callback, rbac_token=authenticate.get_rbac_token())


def print_help_message_and_exit(default_opts: dict, exit_code: int):
    """
    Prints help message and exit program

    @param default_opts:
    @param exit_code:
    """
    print("Usage: python " + os.path.basename(sys.argv[0]) + " [options]")
    print("Script is written to test behaviour of FGC or Pow devices\n")
    print("Options and default values:")

    for option in sorted(default_opts.keys()):
        print("--" + option + " " + default_opts[option])

    print()
    print("where")
    print("--help is this help message")
    print("--device <DEVICE_NAME>")
    print("--authenticate location|explicit")
    print("--protocol cmw|tcp")

    exit(exit_code)


def parse_opts(default_opts=dict(), args=sys.argv[1:]):
    """
    Parse command line options

    @param default_opts: dict with all the possible options and their default values
                         (excluding "help" and "authenticate" which are always assumed).
    @param args: actual args to parse, uses argv by default (ignoring first argument which is the script name)
    @return:
    """
    # Authentication is by location by default
    if "authenticate" not in default_opts.keys():
        default_opts["authenticate"] = "location"

    # Protocol is CMW by default
    if "protocol" not in default_opts.keys():
        default_opts["protocol"] = "tcp"

    # Add device
    if "device" not in default_opts.keys():
        default_opts['device'] = ''

    # build list with option name appended with "=", which means the option expects a value
    recognised_opts = [arg + "=" for arg in default_opts.keys()]
    # append 'help', which does not expect a value
    recognised_opts.append("help")

    # get command line options
    try:
        options, _ = getopt.gnu_getopt(args, "h", recognised_opts)
    except getopt.GetoptError:
        print_help_message_and_exit(default_opts, 2)

    # transform reveived list of tuples into a dictionary
    params = {}
    for opt, arg in options:
        # check if the user asked for HELP
        if opt == '--help' or opt == '-h':
            print_help_message_and_exit(default_opts, 1)
        # store it in params without the first 2 characters '--'
        params[opt[2:]] = arg

    # exit if device to test wasn't specified
    if 'device' not in params.keys():
        print('Error:\n--device not specified\n')
        print_help_message_and_exit(default_opts, 1)

    # load defaults if they were not provided as command line options
    for opt in default_opts.keys():
        if opt not in params.keys():
            params[opt] = default_opts[opt]

    # return params dict built from the command line options
    return params


def test_get(device_name: str, property_name: str, result='', user='') -> bool:
    """
    Quick test to get a property from an FGC and compare it against an expected value/exception
    Compares as strings, it will fail with floats

    @param device_name: FGC name
    @param property_name: property name
    @param result: expected value or exception
    @param user: optional user/cycle
    @return: True if successful
    """
    if user != '':
        user = f'({user})'
    try:
        res = get(device_name, property_name + user)
        # compare in uppercase
        if result != '' and f"{res.value}".upper() != f"{result}".upper():
            logger.error(f'not ok - {device_name}:{property_name}{user} exp:{result} got:{res.value}')
            return False
        result = res.value

    except pyfgc.FgcResponseError:
        if res.err_msg != result:
            logger.error(f'not ok - {device_name}:{property_name}{user} exp:{result} got:{res.err_msg}')
            return False

    logger.info(f'ok - G {device_name}:{property_name} = {result}')
    return True


def test_set(device_name: str, property_name: str, value, user='', exception='') -> bool:
    """
    Quick test to set an FGC property, optionally compare the result against an expected exception

    @param device_name: FGC name
    @param property_name: property name
    @param value: value to set
    @param user: optional user/cycle
    @param exception: optional expected exception
    @return: True if successful
    """
    try:
        if user != '':
            user = f'({user})'
        res = set(device_name, property_name + user, value)
        if res.err_msg != exception:
            logger.error(f'not ok - S {device_name}:{property_name}{user} {value} - exp:{exception} got:{res.err_msg}')
            return False

    except pyfgc.FgcResponseError:
        if res.err_msg != exception:
            logger.error(f'not ok - S {device_name}:{property_name}{user} {value} - exp:{exception} got:{res.err_msg}')
            return False
    except Exception as e:
        logger.error(f'not ok - S {device_name}:{property_name}{user} {value} - exp:{exception} {e}')
        return False

    logger.info(f'ok - S {device_name}:{property_name}{user} {value} {exception}')
    return True


def load_config(device_name: str, config_path: str) -> Tuple[dict, int]:
    """
    Load properties into FGC from a configuration file

    @param device_name: FGC name
    @param config_path: config file
    @return: loaded properties and number of failures
    """
    properties = dict()
    failures = 0

    with open(config_path) as file:
        for line in file:
            # parse line with regex pattern below
            pattern = "^\! S ([A-Z1-9._]*)\s+(...*)$"
            valid_line = re.match(pattern, line)

            # if we have a match then extract the values and set them with pyfgc
            if valid_line:
                property_name, property_value = valid_line.groups()
                property_name = property_name.strip()
                property_value = property_value.strip()

                properties[property_name] = property_value

                try:
                    res = set(device_name, property_name, property_value)
                    if res.err_code:
                        logger.error(f"{device_name} Error setting {property_name} -> {res.err_msg}")
                        failures += 1
                except pyfgc.FgcResponseError as error:
                    logger.error(error)
    return properties, failures


def wait_until(func, timeout=TIMEOUT, polling_period=POLLING_PERIOD) -> bool:
    """
    Wait until condition in passed func returns true.
    It will reexecute forever or with optional passed $timeout and optional passed $polling_period interval

    @param func: function to run
    @param timeout:
    @param polling_period:
    @return: True if successful, False otherwise
    """
    start_time = time.time()

    while not func():
        if timeout and (time.time() - start_time > timeout):
            logger.error("ERROR: wait_until() timeout exceeded")
            return False
        time.sleep(polling_period)

    return True


def wait_until_pc_state(fgc_name: str, pc_state: str, timeout=TIMEOUT, polling_period=POLLING_PERIOD) -> bool:
    """
    Wait until state PC is pc_state
    @param fgc_name: FGC
    @param pc_state: value
    @param timeout: timeout value
    @param polling_period: polling period
    @return: True if successful
    """

    start_time = time.time()

    while get(fgc_name, "STATE.PC").value != pc_state:
        if timeout and (time.time() - start_time > timeout):
            logger.error("ERROR: wait_until_pc() timeout exceeded")
            return False
        time.sleep(polling_period)

    return True


def wait_until_op_state(fgc_name: str, op_state: str, timeout=TIMEOUT, polling_period=POLLING_PERIOD) -> bool:
    """
    Wait until state OP is op_state
    @param fgc_name: FGC
    @param pc_state: value
    @param timeout: timeout value
    @param polling_period: polling period
    @return: True if successful
    """

    start_time = time.time()

    while get(fgc_name, "STATE.OP").value != op_state:
        if timeout and (time.time() - start_time > timeout):
            logger.error("ERROR: wait_until_pc() timeout exceeded")
            return False
        time.sleep(polling_period)

    return True


def sleep_except(except_function, time_s, polling_period=POLLING_PERIOD) -> bool:
    """
    Wait given time, but confirm that during whole sleep passed except_function is always false,
    checking with a frequency of polling_period (seconds)

    @param except_function: function to run
    @param time_s: total time to sleep
    @param polling_period: time to run the except_function
    @return: True if successful, False otherwise
    """
    start_time = time.time()

    while time.time() - start_time < time_s:
        if except_function():
            logger.error("ERROR: sleep_except() stopped by condition before $time_s seconds. ")
            return False
            # exit(2)
        time.sleep(polling_period)
    return True


def get_devices(wildcards):
    """TODO: implement later, only used by 2 tests: sw/tests/fgcd/test_parallel.pl , sw/tests/fgcd/test_cern_mfip.pl

    Retrieve the device names given a series of space separater series of wildcards
    The wildcards apply to the DB fields ACCELERATOR, DEVICENAME, CLASSNAME, and FECNAME
    Each wildcard implies an AND condition (e.g. "PowM% CPS" means all devices with CLASSNAME like 'PowM%'
    AND ACCELERATOR like 'CPS')

    @param wildcards: wildcards
    """


def set_protocol(prot: str) -> bool:
    """
    Set protocol to use
    @param prot: sync|serial
    @return: successful change
    """
    global fw_protocol

    if prot in ['sync', 'tcp']:
        fw_protocol = 'sync'
        return True
    elif prot in ['serial', 'usb']:
        fw_protocol = 'serial'
        return True
    elif prot in ['cmw', 'rda']:
        try:
            import pyrda3
            fw_protocol = 'rda'
        except ImportError as e:
            logger.error(f"Unable to set RDA protocol as pyrda3 is not installed. "
                         f"Proceeding with '{fw_protocol}' protocol.")
            return False
        return True

    logger.error("Invalid protocol. Accepted: sync|rda|serial")
    return False

# EOF
