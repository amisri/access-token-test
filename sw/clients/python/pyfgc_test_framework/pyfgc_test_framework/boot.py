# Name:     boot.py
# Purpose:  Communication with FGC boot program

import pyfgc
import time
from pyfgc_test_framework import utils, fgc, authenticate
from pyfgc_test_framework.consts import logger, DSP_MAX_RESETS, TIMEOUT
from pyfgc_test_framework.boot_options import Boot_menu_options
from pyfgc_test_framework.remote_terminal import remote_terminal_command, REMOTE_TERMINAL_COMMAND_DEFAULT_TIMEOUT
from typing import Tuple, List


def assure_boot(fgc_name: str, from_status_data=False) -> bool:
    if from_status_data:
        # This method requires subscribing via UDP to the gateway status data until we see BOOT and LOCKED for a givenFGC
        return _assure_boot_from_status_data(fgc_name)
    else:
        return _assure_boot_default(fgc_name)


def _assure_boot_default(fgc_name: str) -> bool:
    """
    Make sure the FGC is in the boot program
    @param fgc_name: FGC
    @return: True if successful, False if timeout
    """
    utils.wait_until(lambda: fgc.is_in_main(fgc_name, query_gateway=True) or is_in_boot(fgc_name))

    if fgc.is_in_main(fgc_name):
        try:
            fgc.set_pc(fgc_name, 'OF')
            res = utils.set(fgc_name, 'DEVICE.BOOT', '')
            if res.err_code:
                logger.error(res.err_msg)
                return False
            utils.wait_until(lambda: is_in_boot(fgc_name))
            time.sleep(5)
            return True
        except pyfgc.FgcResponseError as error:
            logger.error(error)
            return False

    return True


def _assure_boot_from_status_data(fgc_name: str) -> bool:
    """
    Check if FGC is in Boot by subscribing to the gateway's status data
    @param fgc_name: FGC
    @return: True if successful, False if error or timeout
    """
    if _is_in_boot_from_status_data(fgc_name):
        return True

    try:
        utils.set(fgc_name, 'DEVICE.BOOT', '')
        return utils.wait_until(lambda: _is_in_boot_from_status_data(fgc_name), timeout=20)
    except pyfgc.FgcResponseError as error:
        logger.error(error)
    return False


def boot_assure_dsp(fgc_name: str) -> bool:
    """
    Makes sure DSP is alive by reseting the FGC up to 10 times and checking the result of the DSP_IS_ALIVE test
    """
    resets_count = 0
    command = Boot_menu_options.DSP_IS_ALIVE.value
    result, _ = remote_terminal_command(fgc_name, command)

    while result != 'OKAY':
        if resets_count == DSP_MAX_RESETS:
            return False

        boot_global_reset(fgc_name)
        resets_count += 1

        assure_boot(fgc_name)
        remote_terminal_command(fgc_name, '\x1b')
        time.sleep(1)

        result, _ = remote_terminal_command(fgc_name, command)

    return True


def boot_global_reset(fgc_name: str) -> bool:
    """
    Do a global reset to the FGCs in the gateway
    @param fgc_name: FGC
    @return: True if successful
    """
    if 'GLOBAL_RESET' not in Boot_menu_options.keys():
        logger.error('Boot command not defined')
        return False

    command = Boot_menu_options.GLOBAL_RESET.value
    remote_terminal_command(fgc_name, command, no_wait=True)

    time.sleep(5)
    return utils.wait_until(lambda: is_in_boot(fgc_name), timeout=16)


def boot_power_cycle(fgc_name: str) -> bool:
    """
    Power cycle an FGC
    @param fgc_name: FGC
    @return: True if successful
    """
    if 'POWER_CYCLE' not in Boot_menu_options.keys():
        logger.error('Boot command not defined')
        return False

    command = Boot_menu_options.POWER_CYCLE.value
    remote_terminal_command(fgc_name, command, no_wait=True)

    time.sleep(8)
    return utils.wait_until(lambda: fgc.is_in_main(fgc_name, query_gateway=True), timeout=20, polling_period=2)


def boot_send_command(fgc_name: str, command: str, arg="", no_wait=False, timeout=REMOTE_TERMINAL_COMMAND_DEFAULT_TIMEOUT) -> Tuple[str, str]:
    """
    There are tests like the POWER_CONTROL (360) that need an argument (e.g. 0,1,2)
    since the full command (e.g. 3601) is not available as a boot menu option
    """
    if command in Boot_menu_options.keys():
        command = Boot_menu_options[command].value

    result, response = remote_terminal_command(fgc_name, command + arg, no_wait=no_wait, timeout=timeout)
    return result, response


def boot_slow_watchdog(fgc_name: str) -> bool:
    """
    Send TOGGLE_SLOW_WD_TRIG_ENABLE boot command
    @param fgc_name:
    @return: True if successful
    """
    if 'TOGGLE_SLOW_WD_TRIG_ENABLE' not in Boot_menu_options.keys():
        logger.error('Boot command not defined')
        return False

    command = Boot_menu_options.TOGGLE_SLOW_WD_TRIG_ENABLE.value
    remote_terminal_command(fgc_name, command, no_wait=True)

    time.sleep(4)
    res = utils.wait_until(lambda: is_in_boot(fgc_name))
    time.sleep(5)
    res &= utils.wait_until(lambda: fgc.is_in_main(fgc_name, query_gateway=True), timeout=25, polling_period=2)

    return res


def is_in_boot(fgc_name: str, from_status_data=False) -> bool:
    if from_status_data:
        return _is_in_boot_from_status_data(fgc_name)
    else:
        return _is_in_boot_default(fgc_name)


def _is_in_boot_default(fgc_name: str) -> bool:
    """
        Check if in boot by checking the gateway fgc.state
        @param fgc_name: FGC
        @return: True if in Boot
        """
    if fgc_name not in fgc.devices.keys():
        logger.error(f'{fgc_name} not found in devices')
        return False

    gateway = fgc.devices[fgc_name]["gateway"]
    channel = fgc.devices[fgc_name]['channel']

    try:
        response = utils.get(gateway, f'GW.FGC.STATE[{channel}]').value
        logger.info(f'{fgc_name} @ {gateway} GW.FGC.STATE[{channel}]= {response}')
        return response == 'INBOOT'
    except pyfgc.FgcResponseError as error:
        logger.error(error)
    return False


def _is_in_boot_from_status_data(fgc_name: str) -> bool:
    """
        Check if FGC is in boot program by UDP subscribing to the gateway's status data
        @param fgc_name: FGC
        @return: True if in boot
        """
    if fgc_name not in fgc.devices.keys():
        logger.error(f'{fgc_name} not found in devices')
        return False

    gateway = fgc.devices[fgc_name]["gateway"]

    class MonitorCallback:
        def __init__(self):
            self.result = None

        def __call__(self, devices_data: dict, devices_by_position: list, gateway_name: str, timestamp: float):
            if fgc_name in devices_data.keys():
                data = devices_data[fgc_name]
                if data['STATE_PLL'] == 'LOCKED' and data['STATE_OP'] == 'BOOT':
                    self.result = True
                else:
                    self.result = False

    monitor_callback = MonitorCallback()
    monitor = pyfgc.monitor_session(monitor_callback, gateway, TIMEOUT, rbac_token=authenticate.get_rbac_token())
    monitor.start()
    start_time = time.time()

    while monitor_callback.result is None:
        if time.time() - start_time > TIMEOUT:
            return None

    monitor.stop()
    return monitor_callback.result

