import time

import pyfgc
import pyfgc_log
from pydantic import BaseModel
from typing import List, Union, Tuple
from datetime import datetime, timedelta
import numpy as np
from pyfgc_test_framework import utils, fgc, exceptions, fortlogs_client
from pyfgc_test_framework.consts import *
import re
from fortlogs_schemas import AnalogLogComparison, DigitalLogComparison, TableLog, EventLog
import fortlogs_schemas.signals
import os
import copy
import json
import math


def _get_log_menu(fgc_name: str) -> dict:
    try:
        log_menu_str = utils.get(fgc_name, 'LOG.MENU').value
    except pyfgc.FgcResponseError as error:
        logger.error(f"Could not retrieve log menu from {fgc_name}. Error {error}")
        # return empty log menu with empty log data dict
        return {'LOG_DATA': {}}

    return pyfgc_log.parse_log_menu(log_menu_str.strip())


def get_available_logs(fgc_name: str, allow_disabled=False) -> dict:
    """
    Retrieve available log names for a device
    @param fgc_name:
    @param allow_disabled: present also DISabled logs
    @return: dictionary with available logs; (key, value) = (log_name, log_buffer)
    """
    log_data = _get_log_menu(fgc_name)['LOG_DATA']
    available_logs = {}

    for log in log_data.keys():
        # check if the log is not DISabled
        if allow_disabled or 'DIS' not in log_data[log]['STATUS']:
            available_logs[log] = log_data[log]['PROP']

    return available_logs


def get_available_timing_users(device: str, allow_disabled=False) -> dict:
    """
    Retrieve available timing users for a given fgc or gateway

    @param device: fgc or gateway name
    @param allow_disabled: present also DISabled users
    @return: dictionary with timing users
    """

    if device in fgc.devices.keys():
        gateway = fgc.devices[device]['gateway']
    elif device in fgc.gateways.keys():
        gateway = device
    else:
        return {}

    rsp = utils.get(gateway, 'FGCD.TIMING.USERS')
    timig_dict = pyfgc_log.parse_timing(rsp.value)
    available_users = {}
    for id, user in enumerate(timig_dict['cycles']):
        if user['enabled'] or allow_disabled:
            name = user["name"]
            available_users[name] = id

    return available_users


def get_cyclic_log(fgc_name: str, log_name: str, user=None, duration_ms=0, time_offset_ms=0, get_raw_data=False) -> Union[AnalogLogComparison, DigitalLogComparison, EventLog, TableLog, dict, list, str]:
    """
    Retrieve cyclic logs

    @param get_raw_data: Flag to choose getting log as raw data (dict, list, str) instead of fortlogs log object
    @param user: timing user
    @param fgc_name: fgc name
    @param log_name: log name or log buffer
    @return: log
    """
    # RETRIEVE THE USER INDEX
    user_idx = ''
    if user is not None:
        available_users = get_available_timing_users(fgc_name, allow_disabled=True)
        # If user has int type it should correspond to the user index in the log buffer
        if isinstance(user, int):
            if user not in available_users.values():
                logger.error(f'Timing user {user} is not available for {fgc_name}')
                return {}
            user_idx = f'({user})'
        # If string it should be the name of the user, we need to find the respective index
        elif isinstance(user, str):
            if user in available_users.keys():
                user_idx = f'({available_users[user]})'
            else:
                logger.error(f'Timing user {user} is not available for {fgc_name}')
                return {}
        # Wrong type
        else:
            logger.error(f'Unrecognized type for user, string (user name) or int (user index) accepted')
            return {}

    # Retrieve the log buffer
    if log_name.split('[')[0] not in ['LOG.SPY.DATA', 'LOG.EVT', 'CONFIG.SET']:
        logs = get_available_logs(fgc_name, allow_disabled=True)
        if log_name in logs.keys():
            log_buffer = logs[log_name]
        else:
            logger.error(f'Log {log_name} not available for {fgc_name}')
            return {}
    else:
        log_buffer = log_name

    # Separate log index from buffer name, and set get options
    if log_buffer == 'LOG.EVT':
        log_idx = ''
        get_opt = 'BIN'
    elif log_buffer == 'CONFIG.SET':
        log_idx = ''
        get_opt = ''
    elif 'LOG.SPY.DATA' in log_buffer:
        log_idx = log_buffer.split('LOG.SPY.DATA').pop()
        log_buffer = log_buffer.split('[')[0]
        get_opt = f'BIN DATA|0,0,{time_offset_ms},{duration_ms},0,0'
    else:
        # cclibs v1 analogue signals
        log_idx = ''
        get_opt = 'BIN'

    try:
        response = utils.get(fgc_name, f'{log_buffer}{user_idx}{log_idx}', getopt=get_opt)
        binary_buffer = pyfgc_log.extract_buffer(response)
    except pyfgc.FgcResponseError as error:
        logger.error(f"Could not retrieve log from {fgc_name}. Error {error}")
        return {}

    if log_buffer == 'LOG.EVT':
        version = pyfgc_log.BufferVersion.EVENT_LOG.value
    elif log_buffer == 'CONFIG.SET':
        version = pyfgc_log.BufferVersion.CONFIG_SET.value
    else:
        version = pyfgc_log.extract_buffer_version(binary_buffer)

    # decode log to dict format (list for EVENTS, or string for CONFIGURATION)
    log = pyfgc_log.decode(binary_buffer, version)

    if get_raw_data:
        # return log dict/list/str
        return log
    else:
        # return fortlogs log object
        return decoded_log_to_fortlogs_log(log, version, log_name, fgc_name, user_idx)


def get_non_cyclic_log(fgc_name: str, log_name: str, start_time=0.0, duration_ms=0, get_raw_data=False) \
        -> Union[AnalogLogComparison, DigitalLogComparison, EventLog, TableLog, dict, list, str]:
    """
    Retrieve non-cyclic logs
    """

    # Retrieve the log buffer
    if log_name.split('[')[0] not in ['LOG.SPY.DATA', 'LOG.EVT', 'CONFIG.SET']:
        logs = get_available_logs(fgc_name)
        if log_name in logs.keys():
            log_buffer = logs[log_name]
        else:
            logger.error(f'Log {log_name} not available for {fgc_name}')
            return {}
    else:
        log_buffer = log_name

    # Separate log index from buffer name, and set get options
    if log_buffer == 'LOG.EVT':
        log_idx = ''
        get_opt = 'BIN'
    elif log_buffer == 'CONFIG.SET':
        log_idx = ''
        get_opt = ''
    elif 'LOG.SPY.DATA' in log_buffer:
        log_idx        = log_buffer.split('LOG.SPY.DATA').pop()
        log_buffer     = log_buffer.split('[')[0]
        time_origin    = math.modf(start_time)
        time_origin_ns = int(time_origin[0] * 1e9) 
        time_origin_s  = int(time_origin[1])
        get_opt = f'BIN DATA|{time_origin_s},{time_origin_ns},0,{duration_ms},0,0'
    else:
        # cclibs v1 analogue signals
        log_idx = ''
        get_opt = 'BIN'

    try:
        response = utils.get(fgc_name, f'{log_buffer}{log_idx}', getopt=get_opt)
        binary_buffer = pyfgc_log.extract_buffer(response)
    except pyfgc.FgcResponseError as error:
        logger.error(f"Could not retrieve log. Error {error}")
        return {}

    if log_buffer == 'LOG.EVT':
        version = pyfgc_log.BufferVersion.EVENT_LOG.value
    elif log_buffer == 'CONFIG.SET':
        version = pyfgc_log.BufferVersion.CONFIG_SET.value
    else:
        version = pyfgc_log.extract_buffer_version(binary_buffer)

    # decode log to dict format (list for EVENTS, or string for CONFIGURATION)
    log = pyfgc_log.decode(binary_buffer, version)

    if get_raw_data:
        # return log dict/list/str
        return log
    else:
        # return fortlogs log object
        return decoded_log_to_fortlogs_log(log, version, log_name, fgc_name)


def compare_cyclic_logs(fgc_name: str,
                        log_name: str,
                        test_name: str,
                        ref_log: Union[AnalogLogComparison, DigitalLogComparison] = None,
                        user=None,
                        duration_ms=0,
                        time_offset_ms=0,
                        threshold=1.0E-4,
                        only_signals: List[str] = []) -> Tuple[bool, AnalogLogComparison, AnalogLogComparison]:
    """
    Compares 2 cycle bounded logs

    @param fgc_name: fgc name
    @param log_name: log name
    @param test_name: some test name
    @param ref_log: reference log object, or None to use last reference in Fortlogs
    @param user: timing user id
    @param duration: duration of the acquisition
    @param time_offset: time offset in milliseconds
    @param threshold: comparison threshold
    @param only_signals: if set, only provided signals are compared, the rest are deleted
    @return: result, log, diff_log
    """

    log = get_cyclic_log(fgc_name, log_name, user, duration_ms, time_offset_ms)

    if ref_log is None:
        ref_log_id, ref_log = fortlogs_client.get_spy_reference(log_name, test_name)
    else:
        ref_log_id = 0

    result, diff_log = compare_log_objects(log, ref_log, ref_log_id, test_name, threshold, only_signals)
    return result, log, ref_log, diff_log


def compare_non_cyclic_logs(fgc_name: str,
                            log_name: str,
                            test_name: str,
                            ref_log: Union[AnalogLogComparison, DigitalLogComparison] = None,
                            start_time=0.0,
                            duration_ms=0,
                            threshold=1.0E-4,
                            only_signals: List[str] = []) -> Tuple[bool, AnalogLogComparison, AnalogLogComparison]:

    if not start_time:
        start_time = float(utils.get(fgc_name, "REF.RUN").value)

    log = get_non_cyclic_log(fgc_name, log_name, start_time, duration_ms)
    if ref_log is None:
        ref_log_id, ref_log = fortlogs_client.get_spy_reference(log_name, test_name)
    else:
        ref_log_id = 0

    result, diff_log = compare_log_objects(log, ref_log, ref_log_id, test_name, threshold, only_signals)
    return result, log, ref_log, diff_log


def compare_log_objects(log: Union[AnalogLogComparison, DigitalLogComparison],
                        ref_log: Union[AnalogLogComparison, DigitalLogComparison],
                        ref_log_id: int,
                        test_name: str,
                        threshold=1.0E-4,
                        only_signals: List[str] = []) -> Tuple[bool, AnalogLogComparison]:
    """
    Compares 2 synchronized log objects point by point

    @param log: fortlogs log object
    @param ref_log: reference fortlogs log object
    @param ref_log_id: reference log identifier in fortlogs
    @param test_name: test case
    @param threshold: error threshold
    @param only_signals: signal names to ignore
    @return: tuple (True/False, fortlogs log object with the difference between log and ref_log)
    """

    # check if ref_log exists
    if ref_log is None:
        warning_msg = 'FAILED - Reference log not found'
        logger.warning(warning_msg)
        _upload_log(log, test_name, warning_msg, matched=False)
        return True, None

    # Pick which signals to compare
    select_signals = list()
    ref_select_signals = list()
    if len(only_signals):
        for signal in log.signals:
            for name in only_signals:
                if name in signal.name:
                    select_signals.append(signal)
        for signal in ref_log.signals:
            for name in only_signals:
                if name in signal.name:
                    ref_select_signals.append(signal)
    else:
        select_signals = log.signals
        ref_select_signals = ref_log.signals

    if len(select_signals) == 0 or len(ref_select_signals) == 0:
        error_msg = f'FAILED - Logs have no signals, {len(select_signals)} vs {len(ref_select_signals)}'
        logger.error(error_msg)
        _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None
    if len(select_signals[0].samples) != len(ref_select_signals[0].samples):
        error_msg = f'FAILED - Logs have different number of samples, {len(select_signals[0].samples)} vs {len(ref_select_signals[0].samples)}'
        logger.error(error_msg)
        _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None
    if log.period != ref_log.period:
        error_msg = 'FAILED - Logs have different period'
        logger.error(error_msg)
        _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None

    # Check if the logs contain the same signals
    if [signal.name for signal in select_signals] != [signal.name for signal in ref_select_signals]:
        error_msg = 'FAILED - Logs have different signals'
        logger.error(error_msg)
        _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None

    # Check time differences
    # delta_time = log.timeOrigin - log.firstSampleTime
    # ref_delta_time = ref_log.timeOrigin - ref_log.firstSampleTime
    # diff_time = delta_time - ref_delta_time
    #
    # if(abs(diff_time) > time_tolerance_us * 1e-6):
    #     error_msg = f'FAILED - Time difference {diff_time} exceeds tolerance {time_tolerance_us}'
    #     logger.error(error_msg)
    #     _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
    #     return False, None

    # create a log with the differences
    diff_log = copy.deepcopy(log)

    failed_signals = []
    all_failed_samples = []

    # iterate signals to compare samples subtracting the numpy arrays
    for sig_index in range(len(select_signals)):
        curr_samples = np.array(select_signals[sig_index].samples, dtype=float)
        ref_samples = np.array(ref_select_signals[sig_index].samples, dtype=float)

        diff_signal = curr_samples - ref_samples

        max_ref = np.max(ref_samples)
        min_ref = np.min(ref_samples)

        # store diff_signal in the diff_log
        diff_log.signals[sig_index].samples = diff_signal.tolist()

        # Use the range to compute the signal tolerance
        delta = max_ref - min_ref
        if delta < threshold:
            sig_tolerance = threshold
        else:
            sig_tolerance = delta * threshold

        failed_samples = np.argwhere(abs(diff_signal) > sig_tolerance).tolist()
        all_failed_samples += failed_samples

        # If there are failed samples store the signal in the failed_signals
        if len(failed_samples):
            failed_signals.append(select_signals[sig_index].name)

    # If there are failed signals, upload log to fortlogs with FAILED message and return False
    if len(failed_signals):
        _upload_log(log, test_name,
                    f"FAILED comparing signals {','.join(failed_signals)} at sample {min(all_failed_samples).pop()}; "
                    f"threshold={threshold} only_signals={only_signals}",
                    matched=False,
                    ref_id=ref_log_id)
        return False, diff_log
    # Upload with SUCCESS and return True
    else:
        _upload_log(log, test_name,
                    f"threshold={threshold} only_signals={only_signals}",
                    matched=True,
                    ref_id=ref_log_id)
        return True, diff_log


def import_log_from_csv(filename: str) -> Union[AnalogLogComparison, DigitalLogComparison]:
    """
    Read a log from a CSV file and return a log object accepted by fortlogs

    @param filename: path to the file
    @return: log object AnalogLogComparison or DigitalLogComparison
    """
    with open(f"{filename}", "r") as file:
        header, *signals = file.readline().split(',')
        pattern = "source:(\w+) type:(\w+) subtype:(.{0,1}\w+) device:([a-zA-Z.0-9]+) name:([a-zA-Z.0-9_]+) timeOrigin:([0-9.e-]+) firstSampleTime:([0-9.e-]+) period:([0-9.e-]+)"
        match = re.search(pattern, header)
        if not match:
            return None
        source, type, sub_type, device, name, time_origin, first_sample_time, period = match.groups()

        arrays = list()
        for line in file:
            line_elements = line.rstrip('\n').split(',')
            if line_elements != ['']:
                arrays.append(line_elements)
        samples = np.stack(arrays,1)
        samples = samples[1:]

        if type == 'analog':
            log_type = AnalogLogComparison
            sig_type = fortlogs_schemas.signals.AnalogSignal
        elif type == 'digital':
            log_type = DigitalLogComparison
            sig_type = fortlogs_schemas.signals.DigitalSignal

        sigs = list()
        for id in range(len(signals)):
            signal = sig_type(
                name=signals[id].split(' ', 1)[0],
                samples=samples[id].tolist(),
                timeOffset=(float(first_sample_time) - float(time_origin))
            )
            if 'STEP' in signals[id] and type == 'analog':
                signal.step = True
            sigs.append(signal)

        log = log_type(
            name=name,
            device=device,
            version=2,
            source=source,
            firstSampleTime=first_sample_time,
            timeOrigin=time_origin,
            period=period,
            signals=sigs,
            test_reference_log_id=0,
            test_matched=False,
            test_details="",
            test_case=""
        )

        return log


def export_log_to_csv(log: Union[AnalogLogComparison, DigitalLogComparison], filename: str):
    """
    Export log object to a CSV file

    @param log: AnalogLogComparison object
    @param filename: file path
    @return: True|False
    """
    if not isinstance(log, AnalogLogComparison) or not isinstance(log, DigitalLogComparison):
        logger.error(f"Unable to export CSV - Invalid log format, expected AnalodLog, got {type(log)}")
        return False
    try:
        log.to_csv(filename)
        return True
    except Exception as err:
        logger.error(f"Unable to export CSV - {err}")
    return False

    
def decoded_log_to_fortlogs_log(log: Union[dict, list, str], version, log_name="", device="", cycle='0') -> Union[AnalogLogComparison, DigitalLogComparison, TableLog, EventLog]:
    """
    Transform log dict/list/str into a log object accepted by fortlogs
    @param log: log
    @param version: log version
    @param log_name: log name
    @param device: fgc name
    @param cycle: timing user number
    @return: log object AnalogLog, DigitalLog, TableLog, or EventLog]
    """
    comparison_fields = json.loads(
        '{\
          "test_reference_log_id": 0,\
          "test_matched": false,\
          "test_details": "string",\
          "test_case": "string"\
        }')

    if isinstance(log, dict):
        if log["meta"]["analog_signals"]:
            log_type = AnalogLogComparison
        else:
            log_type = DigitalLogComparison
    elif isinstance(log, list):
        log_type = EventLog
        comparison_fields = {}
    elif isinstance(log, str):
        log_type = TableLog
        comparison_fields = {}
    else:
        raise exceptions.UnknownLogType(f"Got log with type {type(log)}, expecting dict/list/str")

    log_dict = json.loads(pyfgc_log.encode(log, version, device=device, log_name=log_name, cycle_selector=cycle))
    log_dict.update(comparison_fields)

    return log_type.parse_obj(log_dict)


def _upload_log(log: Union[AnalogLogComparison, DigitalLogComparison, TableLog, EventLog], test_name: str = None,
                comment: str = "", matched=False, ref_id=0):
    """
    Send log to fortlogs
    @param test_name: name of this test
    @param acquisition_name:
    @param log: log object to store
    @param ref_log: reference log object
    @param comment: optional comment
    @return:
    """

    if test_name:
        log.test_case = test_name

    log.test_details = comment
    log.test_matched = matched
    log.test_reference_log_id = ref_id

    # returns the log_id
    return fortlogs_client.add_log(fortlogs_client.fortlogs_acquisition(), log)

# EOF