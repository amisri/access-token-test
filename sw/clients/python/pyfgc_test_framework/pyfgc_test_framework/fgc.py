import pyfgc
import time
import pyfgc_name
from pyfgc_test_framework import utils, boot, authenticate
from typing import Tuple, List
from pyfgc_test_framework.consts import TIMEOUT, logger, regulation_period


# pyfgc_const.FGC_NAME_FILE
pyfgc_name.read_name_file()
gateways = pyfgc_name.gateways
devices = pyfgc_name.devices

pc_state_aliases = {
    "OF": "OFF",
    "SA": "SLOW_ABORT",
    "SB": "ON_STANDBY",
    "IL": "IDLE",
    "CY": "CYCLING",
    "BK": "BLOCKING",
    "EC": "ECONOMY",
    "DT": "DIRECT"
}


def get_gateway(gateway_name: str) -> dict:
    """
    Get all the info on a specific gateway
    @param gateway_name:
    @return dictionary with gateway info or empty if not found
    """

    if gateway_name in gateways.keys():
        return gateways[gateway_name]

    return {}


def get_devices_from_gateway(gateway_name: str) -> list:
    """
    Get list of FGC devices in a gateway
    @param gateway_name: gateway name
    @return: list of devices
    """
    if gateway_name in gateways.keys():
        return gateways[gateway_name]['devices']
    return []


def get_property_info(device_name: str, property_name: str) -> dict:
    """
    Get info about a property
    @param device_name: FGC name
    @param property_name: property name
    @return: dict with info about the property
    """
    try:
        result = utils.get(device_name, f'{property_name} INFO')
    except pyfgc.FgcResponseError as error:
        print(error)
        return {"error": error}

    info_lines = result.value.split('\n')
    info = {}

    for line in info_lines:
        key, value = line.split(':')
        info[key] = value

    return info


def zip_table(time_vector: list, ref_vector: list) -> str:
    """
    Serialize a list of time and a list of references into a table function

    @param time_vector: duration of each reference
    @param ref_vector: reference values
    @return: table function
    """
    serialized = f"{time_vector[0]}|{ref_vector[0]}"
    i = 1
    while i < len(time_vector):
        serialized += f",{time_vector[i]}|{ref_vector[i]}"
        i += 1

    return serialized


def unzip_table(serialized: str) -> Tuple[List[float], List[float]]:
    """
    Deserialize table function into tuple (time_vector, ref_vector)

    @param serialized: table function
    @return: (time_vector, ref_vector)
    """
    lines = serialized.split(',')
    time_vector = list()
    ref_vector = list()

    for line in lines:
        time_val, ref_val = line.split('|')
        time_vector.append(float(time_val))
        ref_vector.append(float(ref_val))

    return time_vector, ref_vector


def is_in_main(fgc_name: str, query_gateway=False) -> bool:
    if query_gateway:
        return _is_in_main_query_gateway(fgc_name)
    else:
        return _is_in_main_query_fgc(fgc_name)


def _is_in_main_query_fgc(fgc_name: str) -> bool:
    """
    Check if FGC is running the Main program
    @param fgc_name: FGC
    @return: True if in Main
    """
    try:
        if utils.get(fgc_name, 'BARCODE').err_code != '':
            return False
    except pyfgc.FgcResponseError as error:
        logger.warning(error)
        return False
    return True


def assure_main(fgc_name: str, mode_op: str = None, wait_to_sync=False, query_gateway=False) -> bool:
    if query_gateway:
        return _assure_main_query_gateway(fgc_name)
    else:
        return _assure_main_query_fgc(fgc_name, mode_op, wait_to_sync)


def _assure_main_query_fgc(fgc_name: str, mode_op: str = None, wait_to_sync=False) -> bool:
    """
    Make sure FGC iss running the Main program
    @param wait_to_sync: wait for FGC to synchronize in order to accept commands
    @param fgc_name: FGC
    @param mode_op: optional mode to put the FGC in
    @return: True if successful, False if timeout
    """
    # There's a short time after a reset or a power cycle, when the PLL is unlocked and
    # the device and it's neither in boot, nor in main

    utils.wait_until(lambda: boot.is_in_boot(fgc_name) or is_in_main(fgc_name))

    if not is_in_main(fgc_name):
        # Event that PLL is now locked, the boot may be not fully initialised, so we need to sleep for a while,
        # otherwise remote terminal may hang
        time.sleep(2)

        # Keep sending 'go to main' request until we are in main
        def job():
            boot.remote_terminal_command(fgc_name, '0Y', no_wait=True)
            return is_in_main(fgc_name)

        if not utils.wait_until(job, timeout=80, polling_period=2):
            return False

    # Put Main in OFF if mode_op was provided
    # Class 59 doesn't have PC state machine, but each sub-device have a simplified version.
    #
    # Class 53 has PC state machine, but it's driven by an external controller. The controller
    # can be simulated on the FGC by setting PAL.LINKS.STATUS property. However, it's impossible
    # to reach OFF state in this class the state goes from FLT_OFF to STARTING.
    if mode_op:
        class_num = devices[fgc_name]['class_id']

        # Wait for 'soft OFF' state - OFF or FLT_OFF
        if class_num != '59':
            # If the FGC isn't OFF/FLT_OFF
            try:
                current_state = utils.get(fgc_name, 'STATE.PC').value
                if current_state not in {'OFF', 'FLT_OFF'}:
                    if class_num == '53':
                        res = utils.set(fgc_name, 'PAL.LINKS.STATUS', 'FAULT')
                        if res.err_code:
                            logger.error(f'Error {res.err_code}: {res.err_msg}')
                            return False
                        if not utils.wait_until(lambda: utils.get(fgc_name, 'STATE.PC').value == 'FLT_OFF'):
                            return False
                    else:
                        res = utils.set(fgc_name, 'PC', 'OF')
                        if res.err_code:
                            logger.error(f'Error {res.err_code}: {res.err_msg}')
                            return False
                        if not utils.wait_until(
                                lambda: 'OFF' in utils.get(fgc_name, 'STATE.PC').value):
                            return False
            except pyfgc.FgcResponseError as error:
                logger.error(error)
                return False

        # Synchronise the FGC with the database
        if not sync_fgc(fgc_name):
            return False

        # Enter simulation mode
        try:
            res = utils.set(fgc_name, 'MODE.OP', mode_op)
            if res.err_code:
                logger.error(f'Error {res.err_code}: {res.err_msg}')
                return False
            if not utils.wait_until(lambda: utils.get(fgc_name, 'STATE.OP').value == mode_op):
                return False
        except pyfgc.FgcResponseError as error:
            print(error)
            logger.error(error)

        # Class 59 doesn't have PC state machine nor MEAS properties
        if class_num != '59':
            # In simulation, enable measurement simulation
            if mode_op == 'SIMULATION':
                try:
                    res = utils.set(fgc_name, 'MEAS.SIM', 'ENABLED')
                    if res.err_code:
                        logger.error(f'Error {res.err_code}: {res.err_msg}')
                        return False
                except pyfgc.FgcResponseError as error:
                    logger.error(error)

            if class_num != '53':
                if not set_pc(fgc_name, 'OFF'):
                    return False
                
    if wait_to_sync:
        return utils.wait_until(lambda: utils.test_get(fgc_name, "CONFIG.STATE", "SYNCHRONISED"), timeout=30)
    
    return True


def _is_in_main_query_gateway(fgc_name: str) -> bool:
    """
    Check if FGC is in Main by querying the gateway and not the FGC
    @param fgc_name: FGC
    @return: True if successful, False if error or timeout
    """
    if fgc_name not in devices.keys():
        logger.error(f'{fgc_name} not found in devices')
        return False

    gateway = devices[fgc_name]["gateway"]
    channel = devices[fgc_name]['channel']

    try:
        response = utils.get(gateway, 'GW.FGC.STATE[%s]' % channel).value
        logger.info(f'{fgc_name} @ {gateway} GW.FGC.STATE[{channel}]= {response}')

        return response not in {'INBOOT', 'OFFLINE', 'NOLOCK'}

    except pyfgc.FgcResponseError as error:
        print(error)
        exit(2)


def _assure_main_query_gateway(fgc_name: str) -> bool:
    """
    Make sure FGC is in Main by querying the gateway and not the FGC
    @param fgc_name: FGC
    @return: True if successful, False if error or timeout
    """
    if _is_in_main_query_gateway(fgc_name):
        return True

    try:
        boot.boot_send_command(fgc_name, 'RUNMAIN', 'Y')
        return utils.wait_until(lambda: _is_in_main_query_gateway(fgc_name), timeout=80, polling_period=2)

    except pyfgc.FgcResponseError as error:
        print(error)
        logger.error(error)
        return False

    return False


def set_pc(fgc_name: str, pc_state: str, timeout=TIMEOUT) -> bool:
    """
    Set FGC STATE.PC
    @param fgc_name: FGC
    @param pc_state: value
    @param timeout: timeout value
    @return: True if successful
    """
    if pc_state in pc_state_aliases.keys():
        pc_state = pc_state_aliases[pc_state]
    elif pc_state not in pc_state_aliases.values():
        logger.error(f"{pc_state} is not a valid pc state. Accepted values are: {pc_state_aliases.keys()}")
        return False

    try:
        res = utils.set(fgc_name, 'PC', pc_state)
        if res.err_code:
            logger.error(f'set_pc({fgc_name}) -> Error {res.err_code}: {res.err_msg}')
            return False
        return utils.wait_until(lambda: pc_state in utils.get(fgc_name, 'STATE.PC').value, timeout)

    except pyfgc.FgcResponseError as error:
        logger.error(error)
    return False


def sync_fgc(fgc_name: str) -> bool:
    """
    Synchronize FGC
    @param fgc_name: FGC
    @return: True if successful
    """
    if fgc_name not in devices.keys():
        logger.error(f'{fgc_name} not found in devices')
        return False

    class_num = devices[fgc_name]['class_id']
    timeout = 30

    # It takes more time to synchronise class 59
    if class_num == '59':
        timeout = timeout * 2

    try:
        if utils.get(fgc_name, 'CONFIG.STATE').value == 'UNSYNCED':
            if utils.get(fgc_name, 'CONFIG.MODE').value == 'SYNC_FAILED':
                utils.set(fgc_name, 'CONFIG.STATE', 'RESET')

            # Try to get barcodes. If that fails it means that the main program didn't start yet, e.g.: after a reset.
            # It also means SYNC_FGC request will be prepared as part of startup sequence, so we don't have to do it.
            try:
                utils.get(fgc_name, 'BARCODE')
                res = utils.set(fgc_name, 'CONFIG.MODE', 'SYNC_FGC')
                if res.err_code:
                    logger.error(res.err_msg)
                    return False
            except pyfgc.FgcResponseError as err2:
                logger.error(err2)
                return False

        return utils.wait_until(lambda: utils.get(fgc_name, 'CONFIG.STATE').value == 'SYNCHRONISED' and
                                        utils.get(fgc_name, 'CONFIG.MODE').value == 'SYNC_NONE', timeout)

    except pyfgc.FgcResponseError as err1:
        print(err1)
        logger.error(err1)

    return False


def reset_fgc(fgc_name: str) -> bool:
    """
    Restart an FGC
    @param fgc_name: FGC
    @return: True if successful
    """
    if _is_in_main_query_gateway(fgc_name):
        utils.set(fgc_name, 'DEVICE.RESET', '')
        time.sleep(5)
    else:
        return boot.boot_global_reset(fgc_name)

    return assure_main(fgc_name, wait_to_sync=True)


def get_phase_aligned_timestamp(fgc_name: str, delay=2) -> str:
    """
    Get the next phase aligned timestamp after $delay seconds.
    Useful to play a function aligned with the FGC regulation period, which means if we retrieve the log,
    the firstSampleTime will be equal to the timeOrigin of the log.

    @param fgc_name: device name
    @param delay: timestamp delay
    @return: timestamp
    """
    if utils.get(fgc_name, "state.pc").value != "CYCLING":
        try:
            # get the index of the selected load
            load_select = int(utils.get(fgc_name, "load.select").value)

            # get the list of period/phase ids
            period_iters = utils.get(fgc_name, "reg.i.period_iters").value.split(',')

            # get the iter_id for the present load
            iter_id = int(period_iters[load_select])

            # iter_id translates to a period in microseconds stored in regulation_period
            # regulation_period - list of periods FGCs can use to be phase aligned and regulate at the exact same time
            if iter_id in regulation_period.keys():
                period_us = regulation_period[iter_id]

                time_seconds, time_micros = utils.get(fgc_name, "time.now").value.split('.')
                delay_seconds, delay_micros = f"{delay:.6f}".split(".")

                time_seconds = int(time_seconds) + int(delay_seconds)
                time_micros = int(time_micros) + int(delay_micros)

                logger.info(f"load_select:{load_select} iter_id:{iter_id} period:{period_us} us\n")
                logger.info(f"before crop timestamp: {time_seconds}.{time_micros}\n")

                time_micros -= time_micros % period_us
                logger.info(f" after crop timestamp: {time_seconds}.{time_micros}\n")

                # subtract rest of the division of the decimal part of the timestamp with the period
                # timestamp -= (timestamp-int(timestamp)) % (period * 1e-6)

                return f"{time_seconds}.{time_micros}"

        except pyfgc.FgcResponseError as err:
            logger.error(err)

    return "0.0"


def play_table_function(fgc_name: str, function_table: str, user: int = 0, start_time=None) -> bool:
    """
    Set a reference table function and play it at a specific time

    @param fgc_name: fgc name
    @param function_table: table function to play
    @param user: timing user number
    @param start_time: timestamp when to play, if zero it plays after 1 second
    @return: True|False
    """

    rsp = utils.set(fgc_name, f"ref.table.func.value({user})", function_table)
    if rsp.err_code:
        logger.error(rsp.err_msg)
        return False

    rsp = utils.set(fgc_name, f"ref.func.type({user})", "table")
    if rsp.err_code:
        logger.error(rsp.err_msg)
        return False

    if start_time is None:
        start_time = get_phase_aligned_timestamp(fgc_name, 1.005670)

    # if start_time is not of string type, convert it to str with 6 decimals
    if not isinstance(start_time, str):
        start_time = f"{start_time:.6f}"

    rsp = utils.set(fgc_name, "ref.run", start_time)
    if rsp.err_code:
        logger.error(rsp.err_msg)
        return False

    return True


