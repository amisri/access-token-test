import pyfgc_rbac
from pyfgc_test_framework import fgc, utils
from pyfgc_test_framework.oldpyfgc import FgcAsync
import asyncio
import logging
import time
import socket
from typing import Tuple
from pyfgc_test_framework.consts import PORT, logger, REMOTE_TERMINAL_COMMAND_DEFAULT_TIMEOUT
from pyfgc_test_framework import authenticate


class RTermCallback:
    """
    Remote terminal callback
    """
    def __init__(self):
        self.response = b""
        self.result = None

    def __call__(self, payload: bytes):
        # ignore if it starts with '$'
        #if payload.startswith(b'$'):
        #  return

        # append to response
        self.response += payload

        # check if we're finished
        # FIXME: since this only checks the end of the line, it can mis-interpret output of commands
        #        which display a table of codes, such as SHOW_INSTALLED_CODES or UPDATE_CODES_FROM_FIELDBUS
        if payload.endswith(b'3\r\n'):
            self.result = 'OKAY'
        elif payload.endswith(b'4\r\n'):
            self.result = 'ERROR'
        elif payload.endswith(b'R\r\n'):
            self.result = 'FGC REBOOTED'


def remote_terminal_command(device, command, no_wait=False, timeout=REMOTE_TERMINAL_COMMAND_DEFAULT_TIMEOUT) -> Tuple[str, str]:
    """
    Send a command to the FGC boot program

    @param device: fgc name
    @param command: command to send
    @param no_wait: If True, do not wait for answer to arrive
    @param timeout: timeout
    @return: (result True|False, raw response)
    """

    gateway = fgc.devices[device]['gateway']
    dongle = fgc.devices[device]['channel']

    # if utils.fw_rbac_token is not None:
    #     rbac_token = utils.fw_rbac_token
    # else:
    #     rbac_token = pyfgc_rbac.get_token_location()
    # logger.info(f'TOKEN: {rbac_token}')

    loop = asyncio.new_event_loop()

    async def main():

        rterm_cb = RTermCallback()

        connection = await FgcAsync.instance(gateway, PORT, loop)
        await connection.set('CLIENT.TOKEN', authenticate.get_rbac_token())

        res = await connection.enable_rterm(dongle, rterm_cb, True)
        logger.info(f"enable_rterm result: {res}")

        # put the remote terminal in DIRECT MODE
        res = await connection.send(b"\032")
        bytes_to_send = bytes(f"\${command}\n", 'utf-8')
        await connection.send(bytes_to_send)

        if no_wait:
            await connection.send(b'\x1b\x1b')
            connection.disconnect()
            return '', ''

        # wait up to 6 secs to get the full response
        # (UPDATE_CODES_* commands can take much longer, so the timeout is overridable)
        deadline = time.time() + timeout
        while rterm_cb.result is None and time.time() < deadline:
            await asyncio.sleep(0.05, True)

        if rterm_cb.result is None:
            if rterm_cb.response == b'':
                logger.error('Remote terminal NO RESPONSE')
                result = 'NO RESPONSE'
            else:
                logger.error('Remote terminal BAD RESPONSE')
                logger.error('Remote terminal response= ' + rterm_cb.response.decode('utf-8'))
                result = 'BAD RESPONSE'

        else:
            logger.info(f"Remote terminal response:\n {rterm_cb.response.decode('utf-8')}")
            logger.info(f'Remote terminal result: {rterm_cb.result}')
            result = rterm_cb.result

        # put the remote terminal back to EDIT MODE (send double ESCAPE)
        await connection.send(b'\x1b\x1b')
        connection.disconnect()
        return result, rterm_cb.response

    return loop.run_until_complete(main())

# EOF