import getpass
import socket
import pytest

# Skip tests if we are not in a dev machine in the TN
# this avois running in Gitlab
if 'cs-ccr-dev' not in socket.gethostname():
    pytest.skip("Not in dev machine in the TN", allow_module_level=True)

import pyfgc_test_framework as test_fw
from pyfgc_test_framework.boot_options import Boot_menu_options
import time
from .test_consts import *


def test_assure_boot():
    test_fw.set_protocol("rda")
    assert test_fw.assure_boot(TESTFGC_B, from_status_data=False)


# @pytest.mark.skip()
def test_send_command():
    device = TESTFGC_B   #"RPACL.866.1"

    assert test_fw.assure_boot(device, True)

    command = Boot_menu_options.BUILD_INFO.value
    result, response = test_fw.boot_send_command(device, command)

    assert 'OKAY' == result


# @pytest.mark.skip()
def test_send_command_no_wait():
    device = TESTFGC_B

    test_fw.assure_boot(device)

    command = Boot_menu_options.BUILD_INFO.value
    res, rsp = test_fw.boot_send_command(device, command, no_wait=True)
    assert res == ''
    assert rsp == ''


# @pytest.mark.skip()
def test_is_in_boot():
    assert test_fw.is_in_boot(TESTFGC_B, from_status_data=True)
    LOG.info("###########################################")
    assert test_fw.is_in_boot(TESTFGC_B, from_status_data=False)


# @pytest.mark.skip()
def test_is_in_boot_from_status_data():
    assert test_fw.is_in_boot(TESTFGC_B, from_status_data=True)


#@pytest.mark.skip()
def test_assure_dsp():
    assert test_fw.boot_assure_dsp(TESTFGC_B)


# @pytest.mark.skip()
def test_send_powercycle():
    test_fw.assure_boot(TESTFGC_A)
    assert test_fw.boot_power_cycle(TESTFGC_A)


# @pytest.mark.skip()
def test_send_watchdog():
    test_fw.assure_boot(TESTFGC_B)
    assert test_fw.boot_slow_watchdog(TESTFGC_B)


# @pytest.mark.skip()
def test_send_boot_test():
    test_fw.assure_boot(TESTFGC_B)
    result, response = test_fw.boot_send_command(TESTFGC_B, 'RUNMAIN', 'Y')
    assert result == 'OKAY'
    assert test_fw.assure_main(TESTFGC_B, wait_to_sync=True)


# @pytest.mark.skip()
def test_send_global_reset():
    assert test_fw.assure_boot(TESTFGC_B)
    time.sleep(2)
    assert test_fw.boot_global_reset(TESTFGC_B)
    time.sleep(5)

# EOF