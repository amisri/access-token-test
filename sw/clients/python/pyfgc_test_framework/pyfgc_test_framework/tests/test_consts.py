import logging

TESTFGC_A = "RFNA.866.04.ETH2"
TESTFGC_B = "RFNA.866.05.ETH2"
TESTFGC_C = "RPAAO.866.02.ETH8"

LOG = logging.getLogger('pytest')
LOG.setLevel(logging.INFO)