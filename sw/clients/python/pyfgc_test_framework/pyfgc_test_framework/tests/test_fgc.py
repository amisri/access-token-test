import socket
import pytest
from datetime import datetime
# Skip tests if we are not in a dev machine in the TN
# this avoids running in Gitlab
if 'cs-ccr-dev' not in socket.gethostname():
    pytest.skip("Not in dev machine in the TN", allow_module_level=True)


import pyfgc
import pyfgc_test_framework as test_fw
import time
from .test_consts import *


def test_rterm_main():
    assert test_fw.boot_send_command(TESTFGC_A, "G DEVICE.NAME")

# @pytest.mark.skip()
def test_reset():
    assert test_fw.reset_fgc(TESTFGC_A)
    assert test_fw.reset_fgc(TESTFGC_B)
    # time.sleep(15)


# @pytest.mark.skip()
def test_get_gateway():
    gateway = test_fw.get_gateway("cfc-866-reth1")
    print(gateway)

    assert 'RPZET.866.20.ETH1' in gateway['devices']


# @pytest.mark.skip()
def test_get_property_info():
    device_name = TESTFGC_B
    property_name = 'TEST.INT32S'

    info = test_fw.get_property_info(device_name, property_name)
    LOG.info(info)

    assert "n_elements" in info.keys()


# @pytest.mark.skip()
def test_is_in_main():
    assert test_fw.is_in_main(TESTFGC_A)


# @pytest.mark.skip()
def test_is_in_main_query_gateway():
    assert test_fw.is_in_main(TESTFGC_A, query_gateway=True)


# @pytest.mark.skip()
def test_pyfgc_name():
    LOG.info(test_fw.devices[TESTFGC_A])
    assert test_fw.devices[TESTFGC_A]


# @pytest.mark.skip()
def test_set_pc():
    device = TESTFGC_A
    # first set a different PC state
    pyfgc.set(device, 'PC', 'IDLE')

    assert test_fw.set_pc(device, 'ON_STANDBY')


# @pytest.mark.skip()
def test_sync_fgc():
    assert test_fw.sync_fgc(TESTFGC_A)


# @pytest.mark.skip()
def test_assure_main_in_off():
    # assert fgc.fgc_assure_main_in_off(TESTFGC_B, 'SIMULATION')
    assert test_fw.assure_main(TESTFGC_B, 'SIMULATION')


# @pytest.mark.skip()
def test_assure_main():
    device = TESTFGC_A

    assert test_fw.assure_main(device)
    test_fw.assure_boot(device)
    time.sleep(2)
    assert test_fw.assure_main(device)


# @pytest.mark.skip()
def test_assure_main_query_gateway():
    device = TESTFGC_A

    assert test_fw.assure_main(device)
    test_fw.assure_boot(device)
    time.sleep(2)
    assert test_fw.assure_main(device, query_gateway=True)


def test_phase():
    device = TESTFGC_A

    test_fw.get(device, "device.name")

    start_time = time.time()
    ts = test_fw.fgc.get_phase_aligned_timestamp(device, 1.0005)
    LOG.info(f"Task duration: {time.time() - start_time:.3f} seconds")

    LOG.info(f"Next phase aligned timestamp for {device} is {ts}")
    assert ts != '0.0'
