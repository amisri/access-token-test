import time
import pytest
import socket

# Skip tests if we are not in a dev machine in the TN
# this avoids running in Gitlab
if 'cs-ccr-dev' not in socket.gethostname():
   pytest.skip("Not in dev machine in the TN", allow_module_level=True)


import fortlogs_schemas
import copy
import pyfgc_test_framework as test_fw
from pyfgc_test_framework.tests.test_consts import *
import matplotlib.pyplot as plt
import pickle

test_fw.create_spy_acquisition("test_log", "pytest from pyfgc_test_framework")
test_fw.set_protocol("tcp")

def test_fgc_get_available_logs():
    device = TESTFGC_A
    available_logs = test_fw.get_available_logs(device, allow_disabled=True)
    LOG.info(available_logs)
    assert len(available_logs)


def test_fgc_get_available_users():
    device = TESTFGC_A
    users = test_fw.get_available_timing_users(device)
    LOG.info(users)
    assert len(users)


aqc_id = test_fw.fortlogs_client.create_spy_acquisition('TEST_LOG')["acquisition_id"]

@pytest.mark.skip()
@pytest.mark.parametrize("device,log_name,user",
                         [(TESTFGC_A, 'I_MEAS', 'ALL'),
                          (TESTFGC_A, 'I_MEAS', 'ZERO'),
                          (TESTFGC_A, 'EVENTS', None),
                          (TESTFGC_A, 'CONFIGURATION', None),
                          (TESTFGC_A, 'LOG.SPY.DATA[0]', 0),
                          # ('RFNA.866.01.ETH2', 'LOG.SPY.DATA[7]', 0) # failing with Error 138: no data
                          # ('RPAAO.866.02.ETH8', 'FLAGS', None)
                          ]
                         )
def test_fgc_get_log(device, log_name, user):

    log = test_fw.log_utils._get_cyclic_log(device, log_name, user)
    if isinstance(log, fortlogs_schemas.AnalogLog) or isinstance(log, fortlogs_schemas.DigitalLog):
        LOG.info(f"Analog {log.device} {log.name} : {len(log.signals)} signals with {len(log.signals[0].samples)} samples")
        assert len(log.signals[0].samples)
    elif isinstance(log, fortlogs_schemas.EventLog) or isinstance(log, fortlogs_schemas.TableLog):
        LOG.info(log)
    assert log != {}
    test_fw.fortlogs_client.add_log(aqc_id, log)


@pytest.mark.parametrize("device,log_name,user", [(TESTFGC_A, 'I_MEAS', 'ALL')])
def test_compare_cycling_logs(device, log_name, user):
    ref_log = test_fw.log_utils._get_cyclic_log(device, log_name, user)
    ref_log.signals[0].samples[99] = 500.0
    ref_log.signals[0].samples[150] = 100.0
    # result, diff_log = log_utils._compare_sync_logs(log, ref_log, test_name="test_compare_cycling_logs")

    result, log, ref_log, diff_log = test_fw.compare_cyclic_logs(device, log_name, "test_compare_cycling_logs",
                                                          ref_log, user, only_signals=['I_MEAS_(A)'])

    assert diff_log is not None
    assert len(diff_log.signals)
    assert not result


@pytest.mark.parametrize("device,log_name,user", [(TESTFGC_A, 'I_MEAS', 'ALL')])
def test_compare_cycling_log_with_ref(device, log_name, user):
    # log = log_utils._get_cycling_log(device, log_name, user)
    # aqc_id, ref_log = log_utils.get_reference_logs(log_name, device).pop()
    # result, diff_log = log_utils._compare_sync_logs(log, ref_log, 'test_compare_cycling_log_with_ref', only_signals=['I_MEAS_(A)'])  # only_signals=['I_MEAS_(A)'])

    result, log, ref_log, diff_log = test_fw.compare_cyclic_logs(device, log_name, test_name='test_compare_cycling_log_with_ref', user=user)

    assert diff_log is not None
    assert len(diff_log.signals)
    assert result


@pytest.mark.parametrize("device,log_name,user", [(TESTFGC_A, 'I_MEAS', 'ALL')])
def test_log_to_csv(device, log_name, user):
    log = test_fw.log_utils._get_cyclic_log(device, log_name, user)

    log.to_csv(filename='output')


@pytest.mark.parametrize("filename", ["output.csv", "flags.csv"])
def test_import_csv(filename):
    log = test_fw.import_log_from_csv(filename)
    LOG.info(f"{log.device} {log.name} : {len(log.signals)} signals with {len(log.signals[0].samples)} samples")

    for i in range(len(log.signals)):
        assert len(log.signals[i].samples)


@pytest.mark.parametrize("log_name, fgc_name, test_name", [#('I_MEAS', TESTFGC_A, None),
                                                         ("I_MEAS", TESTFGC_A, "test_compare_cycling_log_with_ref")])
def test_get_reference_logs(log_name, fgc_name, test_name):
    logs = test_fw.fortlogs_client._get_list_of_ref_logs(log_name, test_name)
    LOG.info(f"Found {len(logs)} reference logs")
    for log_id, log in logs:
        LOG.info(f"(log_id={log_id}, {log.name} with {len(log.signals[0].samples)} samples)")
    assert len(logs)


@pytest.mark.parametrize("fgc_name,log_name,start_time,duration",
                         [(TESTFGC_A, 'I_MEAS', time.time()-4, 2),
                          (TESTFGC_A, 'I_MEAS', time.time()-10, 5)
                         ]
                         )
def test_get_idle_log(fgc_name, log_name, start_time, duration):
    log = test_fw.log_utils._get_non_cyclic_log(fgc_name, log_name, start_time, duration)
    if isinstance(log, dict):
        LOG.error(log)
        assert False
    assert len(log.signals[0].samples) > 0
    assert log.firstSampleTime < log.timeOrigin
    LOG.info(f"{log.name} with {len(log.signals[0].samples)} samples, "
             f"duration of {(log.timeOrigin - log.firstSampleTime):.4f} seconds, "
             f"{(time.time() - log.firstSampleTime):.2f} seconds ago)")


def test_zip_unzip():
    table_in = "0.00|0.00,0.1|0.00,0.20|2.50,0.30|-5.00,0.70|2.50,0.80|0.00"
    duration, refs = test_fw.unzip_table(table_in)
    LOG.info(f"duration={duration}, refs={refs}")
    assert len(duration)
    assert isinstance(duration[0], float)
    assert len(refs)
    assert isinstance(refs[0], float)

    table_out = test_fw.zip_table(duration, refs)
    LOG.info(table_out)



@pytest.mark.parametrize("fgc_A, fgc_B, log_name, table_func, duration",
                         [(TESTFGC_A, TESTFGC_B, 'I_MEAS', "0.00|0.00,0.1|0.00,0.20|2.50,0.30|-5.00,0.70|2.50,0.80|0.00", 1.2)
                          # (TESTFGC_B, 'I_MEAS', "", 10)
                         ]
                         )
def test_set_idle_function_and_compare_logs(fgc_A, fgc_B, log_name, table_func, duration):
    test_fw.set_pc(fgc_A, 'OF')
    test_fw.set_pc(fgc_B, 'OF')
    assert test_fw.set_pc(fgc_A, 'IL')
    assert test_fw.set_pc(fgc_B, 'IL')
    start_time=time.time()+2
    assert test_fw.play_table_function(fgc_A, table_func, start_time=start_time)
    assert test_fw.play_table_function(fgc_B, table_func, start_time=start_time)
    time.sleep(3+duration)
    
    log_A = test_fw.log_utils._get_non_cyclic_log(fgc_A, log_name, start_time, duration)
    log_B = test_fw.log_utils._get_non_cyclic_log(fgc_B, log_name, start_time, duration)
    
    if isinstance(log_A, dict):
        LOG.error(log_A)
        assert False
    if isinstance(log_B, dict):
        LOG.error(log_B)
        assert False

    assert len(log_A.signals[0].samples) > 0
    assert len(log_B.signals[0].samples) > 0

    assert log_A.firstSampleTime < log_A.timeOrigin
    assert log_B.firstSampleTime < log_B.timeOrigin

    assert test_fw.log_utils.compare_log_objects(log_A, log_B, 0, test_name="comparing_idle_functions")


@pytest.mark.parametrize("fgc_name, log_name, table_func, duration",
                         [(TESTFGC_A, 'I_MEAS', "0.00|0.00,0.1|0.00,0.20|2.50,0.30|-5.00,0.70|2.50,0.80|0.00", 1.2),
                          # (TESTFGC_B, 'I_MEAS', "", 10)
                          ]
                         )
def test_compare_idle_log_with_fortlogs_ref(fgc_name, log_name, table_func, duration):
    assert test_fw.set_pc(fgc_name, 'OF')
    assert test_fw.set_pc(fgc_name, 'IL')
    # start_time = time.time() + 2
    assert test_fw.play_table_function(fgc_name, table_func)
    time.sleep(1 + duration)

    # assert log_utils._compare_sync_logs(log_A, log_b, test_name="comparing_idle_functions")
    result, log, ref_log, diff_log = test_fw.compare_non_cyclic_logs(fgc_name,
                                                                     log_name,
                                                                     test_name="comparing_idle_functions_new",
                                                                     ref_log=None,  # will search for a ref in fortlogs
                                                                     duration=duration,
                                                                     threshold=0.009
                                                                     )

    if isinstance(log, dict):
        LOG.error(log)
        assert False
    assert len(log.signals[0].samples) > 0
    assert log.firstSampleTime < log.timeOrigin

    plot_logs(log, ref_log, diff_log, "comparing_idle_functions_new")

    assert result


@pytest.mark.parametrize("fgc_name, log_name, ref_property_name, ref_property_value, duration", [(TESTFGC_B, "I_MEAS", "ref.direct.i.value", 100, 0.7),
                                                                                                 (TESTFGC_C, "I_MEAS", "ref.direct.i.value", 1.789, 2.0)])
def test_compare_direct_log_with_ref(fgc_name, log_name, ref_property_name, ref_property_value, duration):
    assert test_fw.set_pc(fgc_name, 'DT')
    test_fw.set(fgc_name, ref_property_name, 0)
    time.sleep(2)
    test_fw.set(fgc_name, ref_property_name, ref_property_value)

    time.sleep(2)

    result, log, ref_log, diff_log = test_fw.compare_non_cyclic_logs(fgc_name,
                                                                     log_name,
                                                                     test_name=f"test_compare_direct_log_with_ref_{fgc_name}",
                                                                     duration=duration,
                                                                     threshold=0.001,
                                                                     only_signals=["I_MEAS"])

    if isinstance(log, dict):
        LOG.error(log)
        assert False
    assert len(log.signals[0].samples) > 0

    plot_logs(log, ref_log, diff_log, f"test_compare_direct_log_with_ref_{fgc_name}")

    # storing to file
    # with open("log_samples.txt", 'wb') as f:
    #     pickle.dump((log.signals[0].samples, ref_log.signals[0].samples, diff_log.signals[0].samples, sample_time), f)

    assert result


def plot_logs(log, ref_log, diff_log, title: str = None):
    sample_time = [log.period * i for i in range(len(log.signals[0].samples))]
    figure, (line1, line2) = plt.subplots(2, 1)
    figure.set_figheight(20)
    figure.set_figwidth(15)

    # check if logs don't have the same size
    if ref_log and len(log.signals[0].samples) != len(ref_log.signals[0].samples):
        for signal in log.signals:
            line1.plot(sample_time, signal.samples, label=signal.name)
        line1.set_title("log")

        sample_time_ref = [ref_log.period * i for i in range(len(ref_log.signals[0].samples))]
        for ref_signal in ref_log.signals:
            line2.plot(sample_time_ref, ref_signal.samples, label=ref_signal.name)
        line2.set_title("reference log")

    else:
        for signal in log.signals:
            line1.plot(sample_time, signal.samples, label=signal.name)
        if ref_log:
            for ref_signal in ref_log.signals:
                line1.plot(sample_time, ref_signal.samples, label=ref_signal.name, linestyle=":")
        line1.set_title("signals")

        if diff_log:
            LOG.info(
                f"Max signal difference: {max(abs(min(diff_log.signals[0].samples)), max(diff_log.signals[0].samples))}")
            line2.plot(sample_time, diff_log.signals[0].samples)
        line2.set_title("error")

    line1.legend()
    line2.legend()

    if title:
        figure.suptitle(title, fontsize=16)

    plt.show()
# EOF