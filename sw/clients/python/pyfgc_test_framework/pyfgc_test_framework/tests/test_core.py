import socket
import pytest

# Skip tests if we are not in a dev machine in the TN
# this avois running in Gitlab
if 'cs-ccr-dev' not in socket.gethostname():
    pytest.skip("Not in dev machine in the TN", allow_module_level=True)

import pyfgc
import pyfgc_test_framework as test_fw
import time
import threading
import pyfgc_rbac
from random import randrange
from .test_consts import *


# @pytest.mark.skip()
def test_pyfgc_get_set():
    device = TESTFGC_B
    property_name = "LIMITS.B.POS"
    property_value = "1.0000000E+00,2.0000000E+00,3.0000000E+00,4.0000000E+00"

    test_fw.assure_main(device, wait_to_sync=True)
    time.sleep(5)

    # set/get test property
    try:
        prop = "TEST.INT8U"
        val = '9'
        target = TESTFGC_A
        test_fw.set(target, prop, val)
        assert val == test_fw.get(target, prop).value
    except pyfgc.FgcResponseError as error:
        LOG.error(error)

    # set something
    try:
        res = test_fw.set(device, property_name, property_value)
        if res.err_code:
            print(f"{device} Error setting {property_name} -> {res.err_msg}")
    except pyfgc.FgcResponseError as error:
        print(error)

    # read what we set
    try:
        r = test_fw.get(device, property_name)
        # print('GET LIMITS.B.POS= ', r.value)
    except pyfgc.FgcResponseError as error:
        print(error)

    # compare what we set and what we set
    assert r.value == property_value


def test_get():
    device = TESTFGC_A
    prop = 'LIMITS.B.POS'

    assert test_fw.test_get(device, prop, result='bad cycle selector', user='1')
    assert test_fw.test_get(device, prop, result='0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00')
    assert test_fw.test_get(device, prop)


def test_set():
    device = TESTFGC_B
    prop = 'LIMITS.B.POS'
    value_1 = '1,2,3,4'
    value_2 = '0,0,0,0'

    assert test_fw.test_set(device, prop, value_1, user='1', exception='bad cycle selector')
    assert test_fw.test_set(device, prop, value_1)
    assert not test_fw.test_set(device, prop, value_1, exception='bad cycle selector')
    assert test_fw.test_set(device, prop, value_2)


# @pytest.mark.skip()
def test_cli_arg_parsing():
    args = ["--device", "AAA", "--gateway", "cfc-866-eth1", "--class", "63", "--area", "LHC"]
    default_opts = {
        "gateway": "",
        "class": "",
        "area": ""
    }

    params = test_fw.parse_opts(default_opts, args)
    LOG.info(f'Loaded params: {params}')

    # assert that the args were loaded by camparing each one with an empty string
    for opt in default_opts.keys():
        assert params[opt] != ''


# @pytest.mark.skip(reason="takes too long")
def test_load_config():
    config_file = "../configs/RPAAO.866.02.ETH8.MOD.SHORT.txt"
    device = TESTFGC_B
    properties, fails = test_fw.load_config(device, config_file)

    succeeded = len(properties.keys()) - fails

    assert succeeded == len(properties.keys())

    # for property_name in properties.keys():
    #     try:
    #         print(property_name)
    #         assert properties[property_name] == utils.get(device, property_name).value
    #     except pyfgc.FgcResponseError as error:
    #         print(error)


# @pytest.mark.skip()
def test_wait_until():
    # failing with many properties due to rbac permissions
    # CAL.A.ADC.EXTERNAL.GAIN

    device = TESTFGC_B
    property = "LIMITS.B.POS"
    value_1 = "1.0000000E+00,2.0000000E+00,3.0000000E+00,4.0000000E+00"
    value_2 = "5.0000000E+00,6.0000000E+00,7.0000000E+00,8.0000000E+00"

    # set a value to a property
    try:
        res = test_fw.set(device, property, value_1)
        if res.err_code:
            print(device, f"Error setting {property}:", res.err_msg)
            LOG.error(f"Error setting {property} -> {res.err_msg}")
    except pyfgc.FgcResponseError as error:
        print(f'1 ERROR @ SET {property} -', error)

    # create a thread to set a different value to the same property after 2 seconds
    def thread_set_value():
        start_time = time.time()
        while time.time() < (start_time + 1.5):
            time.sleep(0.5)

        try:
            res = test_fw.set(device, property, value_2)
            if res.err_code:
                print(device, f"Error setting {property}:", res.err_msg)
                LOG.error(f"Error setting {property} -> {res.err_msg}")
        except pyfgc.FgcResponseError as error:
            print(f'2 ERROR @ SET {property} -', error)

    # start the thread
    threading.Thread(target=thread_set_value).start()

    # wait until the property has expected value
    wait_time = time.time()

    assert test_fw.wait_until(lambda: test_fw.get(device, property).value == value_2, polling_period=1, timeout=3)
    LOG.info('Waited for %s until condition was met' % str(time.time() - wait_time))


# @pytest.mark.skip()
def test_sleep_except():
    device = TESTFGC_B
    property_name = 'MODE.RT_NUM_STEPS'
    property_value_1 = '1'
    property_value_2 = '2'

    assert not test_fw.set(device, property_name, property_value_1).err_code

    # create a thread to set a different value to the same property after 2 seconds
    def thread_set_value():
        start_time = time.time()
        while time.time() < (start_time + 1.5):
            time.sleep(0.5)
        assert not test_fw.set(device, property_name, property_value_2).err_code

    # start the thread
    threading.Thread(target=thread_set_value).start()

    # Sleep except the property has given value
    wait_time = time.time()
    assert test_fw.sleep_except(lambda: test_fw.get(device, property_name).value == property_value_2,
                              time_s=2, polling_period=1)
    LOG.info('Slept for %s until condition was met' % str(time.time() - wait_time))


@pytest.mark.skip()
def test_rbac():
    token = pyfgc_rbac.get_token_login("USER", "PASSWORD")
    value = f'TEST_{randrange(100)}'
    pyfgc.set(TESTFGC_A, "TEST.CHAR", value, protocol="sync", rbac_token=token)
    res = pyfgc.get(TESTFGC_A, "TEST.CHAR", protocol="sync", rbac_token=token)
    assert res.value == value
    res = pyfgc.get(TESTFGC_A, "TEST.CHAR", protocol="sync", rbac_token=None)
    assert res.value == value
    logging.info(f'Set TEST.CHAR = {res.value}')


@pytest.mark.parametrize("property_names", [["MEAS.V.VALUE"], ["MEAS.V.REF", "MEAS.I.REF"]])
def test_subscription_context(property_names):
    responses_per_prop = dict()
    for property_name in property_names:
        responses_per_prop[property_name] = list()

    def callback(fgc_response):
        assert fgc_response.property_name in property_names
        responses_per_prop[fgc_response.property_name].append(fgc_response)

    with test_fw.subscribe(TESTFGC_A, property_names, callback):
        time.sleep(3)

    for responses in responses_per_prop.values():
        assert len(responses) > 1


# @pytest.mark.skip()
def test_create_manual_subscription_session():
    responses = list()

    def callback(fgc_response):
        responses.append(fgc_response)
        print(f'Received:\n{fgc_response.value}')

    sub_session = test_fw.create_manual_subscription_session(TESTFGC_A, "MEAS.V", callback)
    time.sleep(3)

    sub_session.unsubscribe()
    sub_session.disconnect()

    assert len(responses) > 1


@pytest.mark.skip(reason="Only succeeds on Windows, where pyrda3 is not available.")
def test_no_rda():
    assert test_fw.test_get(TESTFGC_A, "device.name", TESTFGC_A)
    assert test_fw.utils.fw_protocol == "sync"

    assert not test_fw.set_protocol("rda")
    assert test_fw.utils.fw_protocol == "sync"


# EOF
