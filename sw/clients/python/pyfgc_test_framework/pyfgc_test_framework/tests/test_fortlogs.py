import pytest
import socket

# Skip tests if we are not in a dev machine in the TN
# this avoids running in Gitlab
if 'cs-ccr-dev' not in socket.gethostname():
    pytest.skip("Not in dev machine in the TN", allow_module_level=True)

import time

# from pyfgc_test_framework import log_utils, fortlogs_client, authenticate
import pyfgc_test_framework as test_fw
from .test_consts import *
from fortlogs_schemas import AnalogLog, AnalogLogComparison, DigitalLog, DigitalLogComparison
import json

acq_id = 0



def test_create_aquisition():
    # token = authenticate._kerb_authenticate()
    # assert token != ''
    acq = test_fw.fortlogs_client.create_spy_acquisition('FGC_TESTS_ACQ10')
    LOG.info(acq)

    assert 'acquisition_id' in acq.keys()
    global acq_id
    acq_id = acq['acquisition_id']


def test_add_fortlogs_log():
    log = test_fw.log_utils._get_cyclic_log(TESTFGC_A, 'I_MEAS')
    # token = authenticate._kerb_authenticate()
    res = test_fw.fortlogs_client.add_log(acq_id, log)
    print(res)
    assert res > 0


def test_get_log_meta():
    rsp = test_fw.fortlogs_client.get_log_meta(acq_id)
    LOG.info(rsp)
    assert len(rsp)


def test_get_acquisition():
    rsp = test_fw.fortlogs_client.get_acquisition(acq_id)
    LOG.info(rsp)
    assert len(rsp.keys())


def test_get_logs_from_acquisition():
    rsp = test_fw.fortlogs_client.get_logs_from_acquisition(acq_id)
    for log_id, log in rsp:
        LOG.info(LOG.info(f"{type(log)} {log.device} {log.name} : {len(log.signals)} signals with {len(log.signals[0].samples)} samples"))
    assert len(rsp)


@pytest.mark.parametrize("name,device", [('FGC_TESTS_ACQ10', None)])
def test_search_acquisitions(name, device):
    res = test_fw.fortlogs_client.search_acquisitions(name, device=device)
    LOG.info(res)
    assert res != {}


def test_trash_acquisition():
    # token = authenticate._kerb_authenticate()
    # assert token != ''
    assert test_fw.fortlogs_client.trash_acquisition(acq_id)
    assert test_fw.fortlogs_client.untrash_acquisition(acq_id)


def test_comment_acquisition():
    # token = authenticate._kerb_authenticate()
    # assert token != ''
    assert test_fw.fortlogs_client.comment_acquisition(acq_id, 'my comment 8')


def test_star_acquisition():
    # token = authenticate._kerb_authenticate()
    # assert token != ''
    assert test_fw.fortlogs_client.star_acquisition(acq_id)
    assert test_fw.fortlogs_client.unstar_acquisition(acq_id)
    assert test_fw.fortlogs_client.star_acquisition(acq_id)


def test_tag_acquisition():
    # token = authenticate._kerb_authenticate()
    # assert token != ''
    assert test_fw.fortlogs_client.tag_acquisition(acq_id, 'TEST-FW')
    assert test_fw.fortlogs_client.tag_acquisition(acq_id, 'TEST99')
    assert test_fw.fortlogs_client.untag_acquisition(acq_id, 'TEST99')

    # assert fortlogs_client.tag_acquisition(3756, 'TESTNO', token)


def test_types():
    """
    Try different constructors for the forlogs objects
    :return:
    """
    log_json = json.loads('{\
      "cycleSelector": "0",\
      "firstSampleTime": 0,\
      "timeOrigin": 0,\
      "period": 1,\
      "timestamps": [\
        0\
      ],\
      "name": "string",\
      "version": "string",\
      "device": "string",\
      "source": "",\
      "signals": [\
        {\
          "name": "string",\
          "samples": [\
            0\
          ],\
          "step": false,\
          "timeOffset": 0\
        }\
      ]\
    }')

    comp_json = json.loads('{\
      "test_reference_log_id": 0,\
      "test_matched": true,\
      "test_details": "string",\
      "test_case": "string"\
    }')

    comp_json.update(log_json)

    assert log_json["name"] == 'string'
    assert comp_json["name"] == 'string'

    analog: AnalogLog = AnalogLog.parse_obj(log_json)
    analog_comp: AnalogLogComparison = AnalogLogComparison.parse_obj(comp_json)

    assert analog.name == 'string'
    assert analog_comp.name == 'string'

    analog_2 = AnalogLog(
        name="name",
        device="device",
        version=2,
        source="",
        timeOrigin=0,
        period=100,
        signals=analog_comp.signals
    )

    analog_comp_2 = AnalogLogComparison(
        name="name",
        device="device",
        version=2,
        source="",
        firstSampleTime=0,
        timeOrigin=0,
        period=100,
        signals=analog_comp.signals,
        test_reference_log_id=0,
        test_matched=False,
        test_details="string",
        test_case=""
    )



