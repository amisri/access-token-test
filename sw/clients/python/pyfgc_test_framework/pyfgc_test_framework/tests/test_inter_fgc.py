import socket
import pytest

# Skip tests if we are not in a dev machine in the TN
# this avois running in Gitlab
if 'cs-ccr-dev' not in socket.gethostname():
    pytest.skip("Not in dev machine in the TN", allow_module_level=True)

import pyfgc
from pyfgc_test_framework import utils, fgc, inter_fgc
from .test_consts import *


def test_prepare():
    assert fgc.assure_main(TESTFGC_A)
    assert fgc.assure_main(TESTFGC_B)


def test_set_master():
    assert inter_fgc.fgc_set_master(TESTFGC_A, TESTFGC_B)


def test_set_slaves():
    assert inter_fgc.fgc_set_slaves(TESTFGC_B, TESTFGC_A)


def test_set_role():
    assert inter_fgc.fgc_set_role(TESTFGC_A, 'SLAVE', [TESTFGC_B])
    assert inter_fgc.fgc_set_role(TESTFGC_B, 'MASTER', [TESTFGC_A])
    assert inter_fgc.fgc_set_role(TESTFGC_A, 'CONSUMER', [TESTFGC_B])
    assert inter_fgc.fgc_set_role(TESTFGC_B, 'PRODUCER', [TESTFGC_A])


def test_is_master():
    assert inter_fgc.fgc_is_master(TESTFGC_B)


def test_is_slave():
    assert inter_fgc.fgc_is_slave(TESTFGC_A)


def test_prepare_test():
    assert inter_fgc.prepare_inter_fgc_test([TESTFGC_A, TESTFGC_B])


@pytest.mark.skip()
def test_set_broadcast():
    # TODO: Implement test of the broadcast feature
    pass