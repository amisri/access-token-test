import os
import re
import socket
import urllib
from   .fgc_consts    import FGC_NAME_FILE, FGC_NAME_FILE_URL, FGC_SUB_NAME_FILE_BASE, FGC_SUB_NAME_URL_BASE, FGC_MAX_DEV_LEN, FGC_MAX_FGCDS

# Initialize just once

try:
    gateways
except NameError:
    gateways = dict()

try:
    devices
except NameError:
    devices  = dict()

def readNameFile(filename = FGC_NAME_FILE):

    if filename is None:
        # Use the CERN name file if the file name is not provided

        if os.path.isfile(FGC_NAME_FILE):
            filename = FGC_NAME_FILE
        else:
            # If the default name file is not visible, try to use the default name file url
            
            filename = FGC_NAME_FILE_URL
    else:
        if not re.match("(file|http|https)://.*",filename):
            # If the filename does not contain the protocol, assume it is a file
            
            filename = "file://" + filename

    with urllib.request.urlopen(filename) as f:
        __readNameFile(filename,f)

def __readNameFile(filename,fhandler):

    rdevname  = re.compile("[A-Z][A-Z0-9._\\-]+")
    rhostname = re.compile("[a-z][a-z0-9\\-]+")
    romode    = re.compile("0x[0-9A-Fa-f]{4}")
    global devices
    devices.clear()
    global gateways
    gateways.clear()

    for linenum,line in enumerate(fhandler):

        line = line.decode('ascii')
        line.strip()



        hostname, channel, class_id, devname, omodemask = line.split(":")

        # Validate channel number 

        try:
            channel = int(channel)
        except:
            raise ValueError("{}:{}: Channel number '{}' is not an integer".format(filename,linenum,channel))

        if channel < 0:
            raise ValueError("{}:{}: Channel number '{}' is not an integer >= 0".format(filename,linenum,channel))

        # Validate class id

        try:
            class_id = int(class_id)
        except:
            raise ValueError("{}:{}: Class id '{}' is not an integer".format(filename,linenum,class_id))

        if class_id < 0 or class_id > 255:
            raise ValueError("{}:{}: Class id '{}' is not an integer >= 0 and <= 255".format(filename,linenum,class_id))

        # Validate device name

        if len(devname) > FGC_MAX_DEV_LEN:
            raise ValueError("{}:{}: Length of the device name '{}' is > {}".format(filename,linenum,devname,FGC_MAX_DEV_LEN))

        if channel > 0:
            if not rdevname.match(devname):
                raise ValueError("{}:{}: Device name '{}' contains one or more characters different than 'A-Z0-9.-_'".format(filename,linenum,devname))
        else:
            expected_devname = hostname.upper()
            if devname != expected_devname:
                raise ValueError("{}:{}: Gateway device name '{}' must be '{}'".format(filename,linenum,devname,expected_devname))

        if devname in devices:
            raise ValueError("{}:{}: Duplicate device name '{}'".format(filename,linenum,devname))

        if channel > 0:
            if hostname not in gateways:
                raise ValueError("{}:{}: Device '{}' defined before the gateway definition".format(filename,linenum,devname))

            if channel < len(gateways[hostname]["channels"]):
                raise ValueError("{}:{}: Out of order device channel {}. It is smaller than expected ({})".format(filename,linenum,channel,len(gateways[hostname]["channels"]))) 

            if channel in gateways[hostname]["channels"]:
                raise ValueError("{}:{}: Duplicate channel number {} in gateway '{}'".format(filename,linenum,channel,hostname)) 

        # Validate omode mask

        if not romode.match(omodemask):
            raise ValueError("{}:{}: omode mask '{}' must be a 4-byte hexadecimal number".format(filename,linenum,omodemask))

        omodemask = int(omodemask,16)

        # Validate hostname

        try:
            socket.gethostbyname(hostname)
        except:
            raise ValueError("{}:{}: Hostname '{}' can not be resolved".format(filename,linenum,hostname))

        # Validate number of gateways

        if channel == 0 and len(gateways) == FGC_MAX_FGCDS:
            raise ValueError("{}:{}: Maximum number of gateways exceeded (< {}}".format(filename,linenum,FGC_MAX_FGCDS))

        # Insert items in gateways and devices 

        if channel == 0:
            gateways[hostname]             = dict()
            gateways[hostname]["channels"] = []

        devices[devname]             = dict()
        devices[devname]["channel"]  = channel
        devices[devname]["class_id"] = class_id
        devices[devname]["omodemask"]= omodemask

        for i in range(len(gateways[hostname]["channels"]),channel+1):
            gateways[hostname]["channels"].append(None)

        gateways[hostname]["channels"][channel] = devices[devname]

def readSubdeviceFile(basepath = FGC_SUB_NAME_FILE_BASE):
    # It is mandatory to read firs the name file
    assert(len(gateways) != 0 and len(devices) != 0)

    if basepath is None:
        # Use the CERN subdevice base path 

        if os.path.isdir(FGC_SUB_NAME_FILE_BASE):
            basepath = "file://" + FGC_SUB_NAME_FILE_BASE
        else:
            # If the default name file is not visible, try to use the default name file url
            
            basepath = FGC_NAME_URL_BASE
    else:
        if not re.match("(file|http|https)://.*",basepath):
            # If the filename does not contain the protocol, assume it is a file
            
            basepath = "file://" + basepath

    # Iterate over all gateways 

    for gateway in gateways:
        filename = os.path.join(basepath,gateway)
        try:
            with urllib.request.urlopen(filename) as f:
               return __readSubdeviceFile(filename,f)
        except urllib.error.URLError:
            # sub-device file does not exist
            pass

def __readSubdeviceFile(filename,fhandler):
    global devices
    global gateways
    rdevname  = re.compile("[A-Z][A-Z0-9._\\-]+")

    aliases = set()

    for linenum,line in enumerate(fhandler):
        line = line.decode('ascii')
        line.strip()

        device, channel, alias, *other = line.split(":")

        # Check channel

        try:
            channel = int(channel)
        except:
            raise ValueError("{}:{}: Channel number '{}' is not an integer".format(filename,linenum,channel))

        if channel < 0 or channel > 255:
            raise ValueError("{}:{}: Channel number '{}' is not an integer >= 0 and <= 255".format(filename,linenum,channel))

        # Device name already exist

        if device not in devices:
            raise ValueError("{}:{}: Device '{}' not defined".format(filename,linenum,device))

        # Alias is not an existing device

        if alias in devices:
            raise ValueError("{}:{}: Alias '{}' is not unique. Another device exist with the same name".format(filename,linenum,alias))

        # Alias is not unique

        if alias in aliases:
            raise ValueError("{}:{}: Alias '{}' is not unique. Another alias exist with the same name".format(filename,linenum,alias))

        # Device does not exist for the given gateway

        # Device format is correct

        if not rdevname.match(device):
            raise ValueError("{}:{}: Device '{}' contains one or more characters different than 'A-Z0-9.-_'".format(filename,linenum,device))

        if len(device) > FGC_MAX_DEV_LEN:
            raise ValueError("{}:{}: Device '{}' length is too long (> {})".format(filename,linenum,device,FGC_MAX_DEV_LEN))

        # Alias format is correct        

        if not rdevname.match(alias):
            raise ValueError("{}:{}: Alias '{}' contains one or more characters different than 'A-Z0-9.-_'".format(filename,linenum,alias))

        if len(alias) > FGC_MAX_DEV_LEN:
            raise ValueError("{}:{}: Alias '{}' length is too long (> {})".format(filename,linenum,alias,FGC_MAX_DEV_LEN))

        # Add alias to the list of alias

        aliases.add(alias)

        # Append sub_device

        if "sub_devices" not in devices[device]:
            devices[device]["sub_devices"] = []

        sub_device = dict()
        sub_device["alias"]   = alias
        sub_device["device"]  = device
        sub_device["channel"] = channel
        
        for i in range(len(devices[device]["sub_devices"]),channel+1):
            devices[device]["sub_devices"].append(None)      

        devices[device]["sub_devices"][channel] = sub_device
