import socket
from   threading import Thread

class FgcUdpListen:

    def __init__(self):
        self.__udp_socket       = None
        self.__thread           = None
        self.__running          = False

    def connect(self, port) -> bool:
        """
        Connects to the gateway and creates a UDP listener socket.

        return: Boolean representing if the connection and the UDP binding were successful.
        """

        # Start thread loop

        self.__running = True

        try:
            # Create UDP listener
            self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.__udp_socket.bind(('', port))

        except Exception:
            if self.__udp_socket is not None:
                self.__udp_socket.close()

            raise

        return True

    def disconnect(self) -> None:
        """
        Frees all the resources. It never throws"
        """

        # Stop thread loop

        self.__running = False

        # Join thread

        try:
            if self.__thread is not None:
                self.__thread.join()
        except:
            raise

        # Close UDP socket

        try:
            if self.__udp_socket is not None:
                self.__udp_socket.close()
        except:
            raise


    def __enter__(self, port):
        self.connect(port)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def subscribe(self, callback, subscription_period_ms = 1000):
        """
        Subscribe to a given set of FGC devices.
        :param callback               Callback function receiving a list of dictionaries with the status information.
        :param subscription_period_ms period of time between successive subscriptions in milliseconds
        """
        if self.__udp_socket is None:
            raise AssertionError("subscribe called without opening the socket")

        self.__udp_socket.settimeout(subscription_period_ms/1000.0*2)

        # Create subscription thread and callback

        def fgcsub_callback(sock, callback):
            while self.__running:
                try:
                    data, address = sock.recvfrom(4096)
                except socket.timeout:
                    # Ignore timeouts after the disconnection
                    if self.__running:
                        raise
                    else:
                        pass
                callback(data, address)

        self.__thread = Thread(target = fgcsub_callback, args = (self.__udp_socket,callback))
        self.__thread.start()


if __name__ == "__main__":

    import sys
    import time
    import oldpyfgc

    # Subscription time

    port = int(sys.argv[1])
    sec = float(sys.argv[2])

    print("Subscribing {} for {} seconds...".format(port, sec))

    udp = FgcUdpListen()
    udp.connect(port)

    def callback_func(data, _):
        print(oldpyfgc.decode(data)[20])

    udp.subscribe(callback_func)

    time.sleep(sec)

    udp.disconnect()
