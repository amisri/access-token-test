from   .fgc_consts   import FGC_STATUS_SIZE
import importlib
import os
import struct
import re

# Represents the size of the fgc_udp_header plus time_sec and time_usec
UDP_OFFSET          = 24

# Import all fgc_xy packages under classes and add the decode dunction to the dictionary

classes_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),"classes")

decoders = dict()

# For all the files matching classes/fgc_xyz.py

re_fn = re.compile("fgc_(\d+)[.]py")
for fn in os.listdir(classes_path):
    m = re_fn.search(fn)
    if (m):
        package = importlib.import_module("pyfgc_test_framework.oldpyfgc.classes.fgc_" + m.group(1))
        decoders[int(m.group(1))] = package.decode

decoder_common = importlib.import_module("pyfgc_test_framework.oldpyfgc.classes.fgc_common").decode

def decode_header(data):

    # Decodes the fgc_udp_header plus time_sec and time_usec

    d = dict()

    head_data = data[0:UDP_OFFSET]

    unpack_string = ">IIiiII"

    id, seq, send_time_sec, send_time_usec, time_sec, time_usec = struct.unpack_from(unpack_string, head_data)

    send_time = send_time_sec + (0.000001 * send_time_usec)
    time      = time_sec      + (0.000001 * time_usec)

    d['id']  = id
    d['seq'] = seq
    d['send_time'] = send_time
    d['time'] = time

    return d

def decode(data):

    packet_length       = len(data)
    num_devs            = int((packet_length - UDP_OFFSET) / FGC_STATUS_SIZE);

    status = []
    for i in range(num_devs):

        offset   = i*FGC_STATUS_SIZE + UDP_OFFSET
        dev_data = data[offset:offset+FGC_STATUS_SIZE]

        # Decode class data (class id is just after the data status field)

        class_id = dev_data[1]

        if (class_id in decoders):
            status.append(decoders[class_id](dev_data))
        else:
            status.append(decoder_common(dev_data))

    return status
