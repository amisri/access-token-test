import atexit
import socket
import time
from   threading import Thread
from   typing    import Callable, List
from   .         import fgc_base, FgcTcp

class FgcSub(object):

    def __init__(self, gateway):
        self.__gateway         = gateway
        self.__udp_socket      = None
        self.__channel         = None
        self.__thread          = None
        self.__running         = True

    def connect(self) -> bool:
        """
        Connects to the gateway and creates a UDP listener socket.
        
        return: Boolean representing if the connection and the UDP binding were successful. 
        """

        try:
            # Connect to gateway
            self.__channel = FgcTcp(self.__gateway)
            self.__channel.connect()
        
            # Create UDP listener
            self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.__udp_socket.bind(('',0))
        except:
            if self.__channel is not None:
                self.__channel.disconnect()
            
            if self.__udp_socket is not None:
                self.__udp_socket.close()

            raise

        return True


    def disconnect(self) -> None:
        """
        Frees all the resources. It never throws"
        """

        # Stop thread loop

        self.__running = False

        # Stop gateway subscription and disconnect socket

        try:
            if self.__channel is not None:
                self.__channel.set("CLIENT.UDP.SUB.PERIOD", "")
                self.__channel.set("CLIENT.UDP.SUB.PORT", "")
                self.__channel.disconnect()
        except:
            pass

        # Join thread

        try:
            if self.__thread is not None:
                self.__thread.join()
        except:
            pass

        # Close UDP socket

        try:
            if self.__udp_socket is not None:
                self.__udp_socket.close()
        except:
            pass

    
    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def subscribe(self, callback, rbac_token, subscription_period_ms = 1000):
        """
        Subscribe to a given set of FGC devices.
        :param callback            Callback function receiving a list of dictionaries with the status information.
        :param rbac_token          RBAC token for authentication and authorization
        :param subscription_period period of time between successive subscriptions in milliseconds
        """        
        if self.__channel is None or self.__udp_socket is None:
            raise AssertionError("subscribe called without connecting to the gateway")

        # Authenitcate and subscribe to gateway

        self.__channel.set("CLIENT.TOKEN", rbac_token)
        self.__channel.set("CLIENT.UDP.SUB.PORT", self.__udp_socket.getsockname()[1])

        # Subscription period is set in multiples of 20 ms (gateway cycle)

        self.__channel.set("CLIENT.UDP.SUB.PERIOD", subscription_period_ms/20);
    
        # Set UDP socket timeout

        self.__udp_socket.settimeout(subscription_period_ms/1000.0*2)

        # Create subscritpion thread and callback

        def fgcsub_callback(sock,callback):
            while self.__running:
                try:
                    data, address = sock.recvfrom(4096)
                except socket.timeout:
                    # Ignore timeouts after the disconnection
                    if self.__running:
                        raise
                    else:
                        pass
                callback(data)

        self.__thread = Thread(target = fgcsub_callback, args = (self.__udp_socket,callback))
        self.__thread.start()
