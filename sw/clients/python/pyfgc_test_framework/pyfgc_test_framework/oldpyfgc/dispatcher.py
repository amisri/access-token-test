import time

class CommandTypes(object):
    GET = 'G'
    SET = 'S'


class FgcCommand(object):

    def __init__(self, command_type: CommandTypes, property: str, value=None):
        self.__command_type = command_type
        self.__property = property
        self.__value = value

    def in_bytes(self):
        pass

class ScrpCommand(object):
    pass


class NcrpCommand(object):

    def __init__(self, command_type: CommandTypes, property: str, gateway: str, device: str, value=None, tag=None):
        super().__init__(command_type, property, value)


    def in_bytes(self):
        return '!{} {} {}:{}{}'.format().encode()


class Sender(object):
    pass


class Receiver(object):
    pass


def simple_receiver(self) -> bytes:
    """
    Receives the response from the gateway, byte by byte (synchronous reading)

    :return: response from the gateway as a list of bytes.
    """
    response_byte = None
    response = list()

    while response_byte not in [NcrCodes.RESPONSE_EOL, NcrCodes.VOID]:
        response_byte = self.__socket.recv(1)
        response.append(response_byte)

    if len(response) == 0 or response[-1] != NcrCodes.RESPONSE_EOL:
        raise fgc_exceptions.IncompleteResponseError()

    response = b''.join(response)

    return response


class Dispatcher(object):

    def __init__(self, receiver, sender):
        self.__receiver = receiver
        self.__sender = sender

    @property
    def receiver(self):
        return self.__receiver

    @receiver.setter
    def receiver(self, receiver_behavior):
        self.__receiver = receiver_behavior



class SynchronousDispatcher(Dispatcher):
    pass


class AsynchronousDispatcher(Dispatcher):
    pass