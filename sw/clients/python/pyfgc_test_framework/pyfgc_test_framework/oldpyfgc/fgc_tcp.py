import re
import socket
import pyfgc_test_framework.cernrbac as cernrbac
import threading
import struct

from typing import Union
from . import fgc_exceptions
from . import fgc_base


class NcrCodes(object):
    RESPONSE_EOL = b';'
    HANDSHAKE_CHAR = b'+'
    VOID = b''
    BINARY_FLAG = b'\xFF'


class FgcTcp(fgc_base.FgcBase):
    """
    Connect to a gateway which hosts FGC's on its ethernet ports. So, one can communicate with the gateway in order \
    to gather status data or change behavior of the FGC's.

    It implements the Network Command Response Protocol from: \
    https://wikis.cern.ch/pages/viewpage.action?pageId=70682032
    """

    def __init__(self, gateway_address: str, timeout: float = 30, tcp_port: int = 1905,
                 auth_method: str = 'LOCATION', auth_args: tuple = ()):
        """

        :param gateway_address: Gateway alias or ip address, given as a string.
        :param tcp_port: remote port for gateway. Inside CERN is 1905. For other
        """
        super().__init__()
        self.__gateway_address = gateway_address
        self.__socket = None
        self.__timeout = timeout
        self.__tcp_port = tcp_port
        self.__lock = threading.Lock()

        self.__auth_method = auth_method
        self.__auth_arguments = auth_args
        self.__rbac_session = cernrbac.RBAC()

        self.__auth_converter = {
            'SAML': self.__rbac_session.authenticate_by_saml,
            'LOCATION': self.__rbac_session.authenticate,
            'LOGIN': self.__rbac_session.authenticate_by_login,
            'NONE': None
        }

        self.success_regex = re.compile(b'(\$([\w\W]*) .\n([\w\W]*)\n;)')
        self.error_regex = re.compile(b'\$([\w\W]*) !\n([\w\W]*)\n;')
        self.bin_regex = re.compile(b'(\$([\w\W]*) .\n\xFF([\x00-\xFF]*);)')

    def __enter__(self):
        self.connect()

        if self.__auth_method in self.__auth_converter:
            if self.__auth_converter[self.__auth_method]:
                self.authorize()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def __repr__(self):
        return 'FgcTcp({}, {})'.format(self.__gateway_address, self.__socket)

    def receive_binary(self, chunk: int = 4096) -> bytes:
        """Reads bytes from socket, interpreting the length
        flag and the bytes that will be read.

        :param chunk: Byte chunk size that is read in one socket.recv.
        :return:
        """
        # Receive bytes from the header
        response_length_bytes = self.__socket.recv(4)
        response = response_length_bytes

        response_length = struct.unpack('!L', response_length_bytes)[0] + 2
        received = 0

        while received != response_length:
            # Divide in chunks the bytes expected to be read in order to give time to fill the buffer.
            if chunk <= response_length - received:
                incoming_bytes = self.__socket.recv(chunk)
            else:
                incoming_bytes = self.__socket.recv(response_length - received)

            received += len(incoming_bytes)
            response = b''.join([response, incoming_bytes])

        return response

    def receive_loop(self) -> bytes:
        """
        Synchronously receives the response from the gateway.

        :return: response from the gateway as a byte string.
        """
        response_byte = None
        response = list()

        while response_byte not in [NcrCodes.RESPONSE_EOL, NcrCodes.VOID, NcrCodes.BINARY_FLAG]:
            response_byte = self.__socket.recv(1)
            response.append(response_byte)

        response = b''.join(response)

        if response_byte == NcrCodes.BINARY_FLAG:
            response += self.receive_binary()

        if len(response) == 0 or chr(response[-1]).encode() != NcrCodes.RESPONSE_EOL:
            raise fgc_exceptions.IncompleteResponseError()

        return response

    def connect(self) -> None:
        """
        Connect to the gateway using the given gateway address and port on object instantiation.
        It can also raise a UnavailableGatewayError in case the channel initialization was unsuccessful.
        This happens if the handshake protocol was not successful or if there was a communication error with the socket.

        :return:
        """
        with self.__lock:

            if self.__socket:
                return

            try:
                self.__socket = socket.socket()
                self.__socket.settimeout(self.__timeout)
                self.__socket.connect((self.__gateway_address, self.__tcp_port))

                self.__handshake()

            except fgc_exceptions.UnavailableGatewayError:
                raise

            except Exception as e:
                raise fgc_exceptions.CommunicationError(e)

    def __handshake(self) -> None:
        """
        Handshake between the client and the gateway server.

        :return:
        """
        if self.__socket.recv(1) == NcrCodes.HANDSHAKE_CHAR:

            self.__socket.sendall(NcrCodes.HANDSHAKE_CHAR)

        else:
            raise fgc_exceptions.UnavailableGatewayError()

    def disconnect(self) -> None:
        with self.__lock:
            self.__socket.shutdown(socket.SHUT_WR)
            self.__socket.close()

    def get(self, property_to_get: str, device: str='') -> dict:
        """Retrieve a property through the gateway.

        :param property_to_get: FGC/Gateway property that is requested.
        :param device: Target FGC device. If field is not set, it will send the get command to the gateway.
        :return: A dictionary with the format: {'status': [*'FGC_REPLY_OK'* | *'FGC_REPLY_ERROR'*], 'raw': *byte response*,
         'content': *PARSED_CONTENT*}
        """
        to_send = '! g {}:{}{}'.format(device, property_to_get, fgc_base.FgcCode.COMMAND_EOL).encode()

        response = None

        with self.__lock:
            try:
                self.__socket.sendall(to_send)
                response = self.receive_loop()

            except (OSError, AttributeError):
                raise fgc_exceptions.NoConnectionError()

        return fgc_base.FgcBase.parse_response(self, response, property_to_get)

    def set(self, property_to_set: str, property_value: Union[str, bytes, list], device: str='') -> dict:
        """Set a property to some value through the gateway.

        :param property_to_set: FGC/Gateway property to be set.
        :param property_value: Values wanted for the previous variable.
        :param device:  Target FGC device. If field is not set, it will send the get command to the gateway.
        :return: A dictionary with the format: {'status': [*'FGC_REPLY_OK'* | *'FGC_REPLY_ERROR'*], 'raw': *byte response*,
         'content': *PARSED_CONTENT*}
        """
        to_send = '! s {}:{} '.format(device, property_to_set).encode()
        to_send = b''.join([to_send, fgc_base.FgcBase.format_send(property_value)])

        response = None

        with self.__lock:
            try:
                self.__socket.sendall(to_send)
                response = self.receive_loop()

            except (OSError, AttributeError):
                raise fgc_exceptions.NoConnectionError()

        return fgc_base.FgcBase.parse_response(self, response, property_to_set)

    def authorize(self, auth_method: str=None, auth_args: tuple=None) -> None:
        """Try to authenticate & authorize client using constructor authentication scheme. The following authentication
        strings are available: [*'LOCATION'*, *'SAML'*, *'LOGIN'*, *None*]

        It sets the gateway property *CLIENT.TOKEN* in the gateway with the token obtained before.

        :param auth_method: Optional auth_method string to manually override authentication method.
        :param auth_args: Respective arguments for optional auth_method.
        :return:
        """
        try:
            if auth_method:
                self.__auth_converter[auth_method](*auth_args)

            elif self.__auth_converter[self.__auth_method] is None:
                return

            else:
                self.__auth_converter[self.__auth_method](*self.__auth_arguments)

        except (cernrbac.RbacAuthenticationFailure, cernrbac.AuthenticationTrialsExceeded):
            raise fgc_exceptions.AuthorizationError('RBAC authentication Failure.')

        # Will try to authorize the client on the gateway
        r = self.set('CLIENT.TOKEN', self.__rbac_session.token)

        if r['status'] != fgc_base.FgcReplies.OK:
            raise fgc_exceptions.AuthorizationError(r['raw'])
