import serial
import re
import threading
import struct

from . import fgc_base
from . import fgc_exceptions
from typing import Union


class ScrCodes(object):
    DIRECT_MODE_CHAR = b'\x1A'
    EDITOR_MODE_CHAR = b'\x1B'
    SUCCESS_EOL = b';'
    FAILED_EOL = b'!'
    VOID = b''
    BINARY_FLAG = b'\xFF'


class FgcSerial(fgc_base.FgcBase):
    """
    The FGC is configured with the following settings: 9600 baud-rate, 8 bits, no parity, 1 stop bit.
    Also, it implements the Serial Command Response Protocol from: \
    https://wikis.cern.ch/pages/viewpage.action?pageId=70682032
    """

    def __init__(self, port: str, timeout: float = 0.2):
        """

        :param port: is a parent_property name: depending on operating system. e.g. /dev/ttyUSB0 on GNU/Linux or COM# \
        on Windows, where # is a number.
        """
        super().__init__()
        self.success_regex = re.compile(b'((\$)([\w\W]*)\n;)')
        self.error_regex = re.compile(b'\$([\w\W]*)\$([\w\W]+ [\w\W]+)\n!')
        self.bin_regex = re.compile(b'(\$(xFF)([\x00-\xFF]*);)')
        self.__read_timeout = timeout

        self.__serial_channel = None
        self.__port = port
        self.__lock = threading.Lock()

    def __enter__(self):
        """
        Both this and the __exit__ method allow one to use the with statement for the instantiation of this class.

        :return:
        """
        if self.__serial_channel is None:
            self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def __repr__(self):
        """
        String representation of an object this class.

        :return:
        """
        return 'FgcSerial({})'.format(self.__serial_channel)

    def connect(self) -> None:
        """pyserial automatically opens the port whenever the serial object is instantiated

        :return: None
        """
        with self.__lock:

            if self.__serial_channel:
                return

            try:
                self.__serial_channel = serial.Serial(port=self.__port, timeout=self.__read_timeout)

            except Exception as e:
                raise fgc_exceptions.CommunicationError(e)

    def disconnect(self) -> None:
        """ Close the serial channel.

        :return: None
        """
        with self.__lock:
            self.__serial_channel.close()

    def editor_mode(self) -> None:
        """
        Switches the FGC state to editor mode.

        :return:
        """
        with self.__lock:
            self.__serial_channel.write(ScrCodes.EDITOR_MODE_CHAR)

    def direct_mode(self) -> None:
        """
        Switches the FGC state to direct mode.

        :return:
        """
        with self.__lock:
            self.__serial_channel.write(ScrCodes.DIRECT_MODE_CHAR)

    def receive_binary(self, chunk: int = 4096) -> bytes:
        """Reads bytes from socket, interpreting the length
        flag and the bytes that will be read.

        :param chunk: Byte chunk size that is read in one socket.recv.
        :return:
        """
        response_length_bytes = self.__serial_channel.read(4)
        response = response_length_bytes

        response_length = struct.unpack('!L', response_length_bytes)[0] + 2
        received = 0

        while received != response_length:
            # Divide in chunks the bytes expected to be read in order to give time to fill the buffer.
            if chunk <= response_length - received:
                incoming_bytes = self.__serial_channel.read(chunk)
            else:
                incoming_bytes = self.__serial_channel.read(response_length - received)

            received += len(incoming_bytes)
            response = b''.join([response, incoming_bytes])

        return response

    def receive_loop(self) -> bytes:
        """
        Synchronously receives the response from the gateway.

        :return: response from the gateway as a byte string.
        """
        response_byte = None
        response = list()

        # Repeat until termination character is found.
        while response_byte not in [ScrCodes.SUCCESS_EOL, ScrCodes.FAILED_EOL, ScrCodes.VOID, ScrCodes.BINARY_FLAG]:
            response_byte = self.__serial_channel.read(size=1)
            response.append(response_byte)

        response = b''.join(response)

        if response_byte == ScrCodes.BINARY_FLAG:
            response += self.receive_binary()

        if len(response) == 0 or chr(response[-1]).encode() not in [ScrCodes.SUCCESS_EOL, ScrCodes.FAILED_EOL]:
            raise fgc_exceptions.IncompleteResponseError()

        return response

    def get(self, property_to_get: str) -> dict:
        """Retrieve a property from the FGC.

        :param property_to_get: FGC property that is requested.
        :return: A dictionary with the format: {'status': [*'FGC_REPLY_OK'* | *'FGC_REPLY_ERROR'*],
        'raw': *byte response*, 'content': *PARSED_CONTENT*}
        """
        request = '! g {}{}'.format(property_to_get, fgc_base.FgcCode.COMMAND_EOL).encode()

        response = None

        with self.__lock:
            try:
                self.__serial_channel.write(request)
                response = self.receive_loop()

            except (OSError, AttributeError):
                raise fgc_exceptions.NoConnectionError()

        return fgc_base.FgcBase.parse_response(self, response, property_to_get)

    def set(self, property_to_set: str,  property_value: Union[str, bytes, list]) -> dict:
        """Set a FGC property to some value.

        :param property_to_set: FGC property to be set.
        :param property_value: Values wanted for the previous variable.
        :return: A dictionary with the format: {'status': [*'FGC_REPLY_OK'* | *'FGC_REPLY_ERROR'*],
        'raw': *byte response*, 'content': *PARSED_CONTENT*}
        """
        to_send = '! s {} '.format(property_to_set).encode()
        to_send = b''.join([to_send, fgc_base.FgcBase.format_send(property_value)])

        response = None

        with self.__lock:
            try:
                self.__serial_channel.write(to_send)
                response = self.receive_loop()

            except (OSError, AttributeError):
                raise fgc_exceptions.NoConnectionError()

        return fgc_base.FgcBase.parse_response(self, response, property_to_set)
