import asyncio
import re
from typing import Union, Callable, Any

from pyfgc_test_framework import oldpyfgc


class FgcTagProvider:
    """
    This class allow mapping between commands and their associated responses. Each command is linked to a unique tag.
    When the response is retrieved, the tag allows to do a reverse search of the previously made command.
    """
    TAG_REGEXP = re.compile(r'^([0-9]+)$')

    def __init__(self):
        self.current_index = 0
        self.tags = {}

    def provide(self, prop: str) -> str:
        """
        Creates a new tag associated to the property 'prop'

        :param prop:
        :return:
        """
        self.current_index = self.current_index + 1
        self.tags[self.current_index] = prop
        return '{}'.format(self.current_index)

    def get_tag(self, tag: str) -> Union[dict, None]:
        """
        Get the tag 'tag'

        :param tag:
        :return:
        """
        matches = FgcTagProvider.TAG_REGEXP.search(tag)
        index = int(matches.group(1))
        return {
            'tag': tag,
            'number': index,
            'property': self.tags[index]
        } if matches else None

    def pop_tag(self, tag: str) -> Union[dict, None]:
        """
        Pop the tag 'tag' from the stored ones and return its associated property.

        :param tag:
        :return:
        """
        tag = self.get_tag(tag)
        if tag is not None:
            self.tags.pop(tag['number'])
        return tag


class FgcUtil:
    NCRP_RESPONSE_REGEXP = re.compile(b'^\$([^ ]*) ([.!])\n(\xFF(.{4})|[\w\W\n]*?\n;)', flags=re.DOTALL)

    @staticmethod
    def run_sync(job: Callable[[asyncio.AbstractEventLoop], Any]):
        """
        Run an asynchronous function in a blocking way

        :param job:
        :return:
        """
        loop = asyncio.get_event_loop()
        loop.run_until_complete(job(loop))
        loop.close()

    @staticmethod
    async def do_or_die(func):
        result = await func()
        if result['status'] == oldpyfgc.FgcReplies.ERROR:
            raise IOError(result['result'])
