try:
    from collections.abc import MutableMapping  # noqa
except ImportError:
    # Python <3.3 compatibility; see also: https://stackoverflow.com/q/53978542
    from collections import MutableMapping  # noqa


class CaseInsensitiveDict(MutableMapping):

    def __init__(self, *args, **kwargs):
        self.__insensitive_dict = dict()
        self.update(dict(*args, **kwargs))

    def __getitem__(self, key: str):
        return self.__insensitive_dict[self.__transform_key(key)]

    def __setitem__(self, key: str, value):
        self.__insensitive_dict[self.__transform_key(key)] = value

    def __delitem__(self, key: str):
        del self.__insensitive_dict[self.__transform_key(key)]

    def __iter__(self):
        return iter(self.__insensitive_dict)

    def __len__(self):
        return len(self.__insensitive_dict)

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, self.__insensitive_dict.__repr__())

    def __transform_key(self, key: str):
        return key.upper()