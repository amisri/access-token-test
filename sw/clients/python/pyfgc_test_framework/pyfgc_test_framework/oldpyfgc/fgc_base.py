import re
import struct

from typing import Union
from . import utils


class FgcCode(object):
    COMMAND_EOL = '\n'


class FgcReplies(object):
    OK = 'FGC_REPLY_OK'
    ERROR = 'FGC_REPLY_ERROR'


class FgcBase(object):
    """
    Interface to communicate with FGC's.
    """

    def connect(self) -> bool:
        pass

    def disconnect(self) -> None:
        pass

    def set(self, **kwargs) -> dict:
        pass

    def get(self, **kwargs) -> dict:
        pass

    def parse_response(self, raw_response: bytes, command: str) -> dict:
        bin_regex = self.bin_regex.search(raw_response)
        if bin_regex:
            return {'status': FgcReplies.OK, 'raw': raw_response, 'content': None}

        error_regex = self.error_regex.search(raw_response)
        if error_regex:
            return {'status': FgcReplies.ERROR, 'raw': error_regex.group(2), 'content': None}

        success_regex = self.success_regex.search(raw_response)
        if success_regex:
            response = success_regex.group(3).decode('utf-8')
            response = FgcBase.format_response(response, command)
            return {'status': FgcReplies.OK, 'raw': success_regex.group(1), 'content': response}

    @staticmethod
    def format_send(property_value: Union[str, bytes, list]) -> bytes:
        new_value = property_value

        if type(property_value) is list:
            new_value = [str(a) for a in property_value]
            new_value = ','.join(new_value)

        if type(new_value) == bytes:
            return struct.pack('!BL', 255, len(new_value)) + new_value + FgcCode.COMMAND_EOL.encode()

        else:
            return '{}{}'.format(new_value, FgcCode.COMMAND_EOL).encode()

    @staticmethod
    def format_response(raw_data, command: str) -> dict:
        r = utils.CaseInsensitiveDict()

        # Case where there is only one line
        if raw_data.count('\n') == 0:

            # Find the property key if it exists
            sub_property = re.match(r'(\D+):([\w\W]*)', raw_data)

            if sub_property:
                r[sub_property.group(1)] = sub_property.group(2).split(',')

            else:
                r[command] = raw_data.split(',')

        else:
            sub_property = raw_data.split('\n')
            for line in sub_property:
                if line:
                    key, value = line.split(':', 1)
                    r[key] = value.split(',')

        return r
