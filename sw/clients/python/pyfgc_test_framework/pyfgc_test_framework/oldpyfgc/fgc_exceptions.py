class FgcError(Exception):
    """Generic error for FGC exceptions.
    """
    pass


class NCRPError(FgcError):
    """Generic error for NCRP exceptions.
    """
    pass


class SCRPError(FgcError):
    """Generic error for SCRP exceptions.
    """
    pass


class UnavailableGatewayError(NCRPError):
    """Error thrown when the gateway is incapable of establishing the handshake.
    """
    pass


class NoConnectionError(SCRPError, NCRPError):
    """Error thrown when one tries to get/set without properly initialization of the connection.
    """
    pass


class AuthorizationError(NCRPError):
    """"Exception raised when there is a error with both RBAC authentication or gateway authorization.
    """
    pass


class IncompleteResponseError(SCRPError, NCRPError):
    """Exception raised whenever there is a error during the receive loop and an incomplete
    response is read.
    """
    pass


class CommunicationError(SCRPError, NCRPError):
    """"This error is a placeholder for errors during the connection establishment phase.
    """
    pass
