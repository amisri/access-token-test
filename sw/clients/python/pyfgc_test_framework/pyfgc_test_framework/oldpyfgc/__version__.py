
__title__ = 'oldpyfgc'
__description__ = 'Python client to communicate with FGC\'s over serial/tcp-ip'
__url__ = 'https://gitlab.cern.ch/ccs/fgc'
__version__ = '0.4.0'
__author__ = 'Nuno Mendes'
__author_email__ = 'little.brat@cern.ch'
__license__ = 'GNU General Public License v3.0'
