# ../../sw/clients/python/oldpyfgc/oldpyfgc/classes/fgc_94.py - This file is automatically generated by 'parser/Output/Python/Class/Fgc_xyz.pm' - DO NOT EDIT
from   pyfgc_test_framework.oldpyfgc.fgc_consts import FGC_STAT_PRECLASS_PAD
import struct

# unpack string

# DATA_STATUS, CLASS_ID, reserved, and RUNLOG_BYTE

unpack_string = ">BBBB"

# reserved

unpack_string += str(FGC_STAT_PRECLASS_PAD) + "s"

# Other fields

unpack_string += "H"
unpack_string += "H"
unpack_string += "H"
unpack_string += "B"
unpack_string += "B"
unpack_string += "B"
unpack_string += "f"
unpack_string += "f"
unpack_string += "f"
unpack_string += "f"

# DATA_STATUS bit mask

DATA_STATUS_bitmask = dict()

DATA_STATUS_bitmask['DEVICE_IN_DB']  = 0x00000001
DATA_STATUS_bitmask['DATA_VALID']    = 0x00000002
DATA_STATUS_bitmask['CLASS_VALID']   = 0x00000004

DATA_STATUS_bitmask['ACK_PLL']       = 0x00000020
DATA_STATUS_bitmask['SELF_PM_REQ']   = 0x00000040
DATA_STATUS_bitmask['EXT_PM_REQ']    = 0x00000080

# Decode ST_FAULTS bitmask

ST_FAULTS_bitmask = dict()
ST_FAULTS_bitmask['380V'] = 16
ST_FAULTS_bitmask['AUX_SUPPLY'] = 256
ST_FAULTS_bitmask['COMMS'] = 8192
ST_FAULTS_bitmask['EARTH'] = 128
ST_FAULTS_bitmask['FGC_STATE'] = 32768
ST_FAULTS_bitmask['FILTER'] = 8
ST_FAULTS_bitmask['LIMITS'] = 4096
ST_FAULTS_bitmask['LOCAL_CTRL'] = 32
ST_FAULTS_bitmask['MAGNET'] = 4
ST_FAULTS_bitmask['MEAS'] = 16384
ST_FAULTS_bitmask['OVERLOAD'] = 64
ST_FAULTS_bitmask['REG'] = 512
ST_FAULTS_bitmask['VS'] = 1024
ST_FAULTS_bitmask['VS_EXTINTLOCK'] = 1
ST_FAULTS_bitmask['VS_INTINTLOCK'] = 2
ST_FAULTS_bitmask['VS_RUN_TO'] = 2048

# Decode ST_WARNINGS bitmask

ST_WARNINGS_bitmask = dict()
ST_WARNINGS_bitmask['CONFIG'] = 8192
ST_WARNINGS_bitmask['NON_VOLATILE'] = 4096
ST_WARNINGS_bitmask['REF_LIM'] = 1
ST_WARNINGS_bitmask['REF_RATE_LIM'] = 2
ST_WARNINGS_bitmask['SIMULATION'] = 4

# Decode ST_UNLATCHED bitmask

ST_UNLATCHED_bitmask = dict()
ST_UNLATCHED_bitmask['LOAD_1'] = 1
ST_UNLATCHED_bitmask['LOAD_2'] = 2
ST_UNLATCHED_bitmask['POL_SWI_NEG'] = 8
ST_UNLATCHED_bitmask['POL_SWI_POS'] = 16

# Decode STATE_OP enumeration

STATE_OP_enumeration = dict()
STATE_OP_enumeration[5] = 'BOOT'
STATE_OP_enumeration[3] = 'CALIBRATING'
STATE_OP_enumeration[1] = 'NORMAL'
STATE_OP_enumeration[6] = 'PROGRAMMING'
STATE_OP_enumeration[2] = 'SIMULATION'
STATE_OP_enumeration[4] = 'TEST'
STATE_OP_enumeration[0] = 'UNCONFIGURED'

# Decode STATE_VS enumeration

STATE_VS_enumeration = dict()
STATE_VS_enumeration[8] = 'BLOCKED'
STATE_VS_enumeration[2] = 'FASTPA_OFF'
STATE_VS_enumeration[4] = 'FAST_STOP'
STATE_VS_enumeration[1] = 'FLT_OFF'
STATE_VS_enumeration[0] = 'INVALID'
STATE_VS_enumeration[9] = 'NONE'
STATE_VS_enumeration[3] = 'OFF'
STATE_VS_enumeration[7] = 'READY'
STATE_VS_enumeration[6] = 'STARTING'
STATE_VS_enumeration[5] = 'STOPPING'

# Decode STATE_PC enumeration

STATE_PC_enumeration = dict()
STATE_PC_enumeration[12] = 'ABORTING'
STATE_PC_enumeration[10] = 'ARMED'
STATE_PC_enumeration[15] = 'BLOCKING'
STATE_PC_enumeration[13] = 'CYCLING'
STATE_PC_enumeration[17] = 'DIRECT'
STATE_PC_enumeration[16] = 'ECONOMY'
STATE_PC_enumeration[0] = 'FLT_OFF'
STATE_PC_enumeration[2] = 'FLT_STOPPING'
STATE_PC_enumeration[8] = 'IDLE'
STATE_PC_enumeration[1] = 'OFF'
STATE_PC_enumeration[7] = 'ON_STANDBY'
STATE_PC_enumeration[18] = 'PAUSED'
STATE_PC_enumeration[14] = 'POL_SWITCHING'
STATE_PC_enumeration[11] = 'RUNNING'
STATE_PC_enumeration[5] = 'SLOW_ABORT'
STATE_PC_enumeration[4] = 'STARTING'
STATE_PC_enumeration[3] = 'STOPPING'
STATE_PC_enumeration[9] = 'TO_CYCLING'
STATE_PC_enumeration[6] = 'TO_STANDBY'

def decode_bitmask(result, field, bitmask):

    value = result[field]
    items = []
    for k,v in bitmask.items():
        if v & value:
            items.append(k)

    result[field] = ' '.join(items)

def decode_enum(result,field, enumeration):

    result[field] = enumeration[result[field]]

def decode(data):
    result        = dict()

    result['raw'] = data

    # Unpack data

    global unpack_string
    result['DATA_STATUS'], result['CLASS_ID'], _, result['RUNLOG_BYTE'], _, result['ST_FAULTS'], result['ST_WARNINGS'], result['ST_UNLATCHED'], result['STATE_OP'], result['STATE_VS'], result['STATE_PC'], result['REF_MEAS'], result['I_REF'], result['I_MEAS'], result['V_REF'] = struct.unpack_from(unpack_string,data)

    # Decode DATA_STATUS bit mask

    global DATA_STATUS_bitmask
    decode_bitmask(result, 'DATA_STATUS', DATA_STATUS_bitmask)

    # Decode ST_FAULTS bitmask

    global ST_FAULTS_bitmask
    decode_bitmask(result,'ST_FAULTS', ST_FAULTS_bitmask)

    # Decode ST_WARNINGS bitmask

    global ST_WARNINGS_bitmask
    decode_bitmask(result,'ST_WARNINGS', ST_WARNINGS_bitmask)

    # Decode ST_UNLATCHED bitmask

    global ST_UNLATCHED_bitmask
    decode_bitmask(result,'ST_UNLATCHED', ST_UNLATCHED_bitmask)

    # Decode STATE_OP enumeration

    global STATE_OP_enumeration
    decode_enum(result,'STATE_OP', STATE_OP_enumeration)

    # Decode STATE_VS enumeration

    global STATE_VS_enumeration
    decode_enum(result,'STATE_VS', STATE_VS_enumeration)

    # Decode STATE_PC enumeration

    global STATE_PC_enumeration
    decode_enum(result,'STATE_PC', STATE_PC_enumeration)

    return result

