import asyncio
import struct

from pyfgc_test_framework import oldpyfgc
from typing import Union, Callable

from .fgc_util import FgcUtil
from .fgc_base import FgcReplies
from .fgc_util import FgcTagProvider


class FgcAsyncProtocol(asyncio.Protocol):
    """
    Asynchronous TCP interaction with a Gateway/FGC
    """

    def __init__(self, loop: asyncio.AbstractEventLoop):
        """
        This constructor is called only by the Loop.create_connection method

        :param loop:
        """
        self.loop = loop
        self.transport = None
        self.handshake_made = False
        self.buffer = b''
        self.commands = {}
        self.receive_callback = None
        self.rterm_enabled = False
        self.tag_provider = FgcTagProvider()

    def connection_made(self, transport: asyncio.Transport):
        """
        When connection has been made

        :param transport:
        :return:
        """
        self.transport = transport

    def data_received(self, payload: bytes):
        """
        When something is received from the socket

        :param payload:
        :return:
        """

        if not self.handshake_made:
            # handshake
            self.__data_handshake(payload)

        elif self.rterm_enabled:
            # rterm
            self.__data_rterm(payload)

        else:
            # command/response protocol
            self.__data_command_response(payload)

    def __data_handshake(self, payload: bytes) -> None:
        """
        Handle character when in 'waiting for handshake' mode

        :param payload:
        :return:
        """
        if payload != oldpyfgc.fgc_tcp.NcrCodes.HANDSHAKE_CHAR:
            raise oldpyfgc.UnavailableGatewayError()
        self.handshake_made = True

    def __data_rterm(self, payload: bytes) -> None:
        """
        Handle character when in rterm mode

        :param payload:
        :return:
        """
        self.receive_callback(payload)

    def __data_command_response(self, payload: bytes) -> None:
        """
        Handle receiving bytes when in command/response mode.

        :param payload:
        :return:
        """
        self.buffer += payload

        if payload != b'' and b'\n;' not in payload:
            return

        # if the buffer looks like a complete response, we handle it
        parsed = self.__parse_response(self.buffer)
        while parsed:
            self.__resolve_pending_request(parsed)

            # remove this response from the buffer + check if there is another
            self.buffer = self.buffer[len(parsed['raw']):]
            parsed = self.__parse_response(self.buffer)

    def __parse_response(self, raw_response: bytes):
        match = FgcUtil.NCRP_RESPONSE_REGEXP.search(raw_response)

        if not match:
            return None

        tag = match.group(1).decode('utf-8')
        tag_info = self.tag_provider.get_tag(tag)
        status = FgcReplies.ERROR if match.group(2) == b'!' else FgcReplies.OK

        parsed = {'tag': tag, 'tag_info': tag_info, 'status': status}

        result = match.group(3)
        if result[0] == 255:
            # binary response
            length = struct.unpack('>I', match.group(4))[0] + len(match.group(0)) + 2
            if len(self.buffer) < length:
                return None
            parsed['raw'] = raw_response[:length]
            parsed['content'] = None
            self.tag_provider.pop_tag(tag)
            return parsed

        # normal response
        content = result[:-2].decode('utf-8')  # removes '\n;' from the result
        content = oldpyfgc.FgcBase.format_response(content, tag_info['property'])

        parsed['raw'] = match.group(0)
        parsed['content'] = content

        self.tag_provider.pop_tag(tag)
        return parsed

    def __register_pending_request(self, tag: str) -> asyncio.Future:
        """
        Register a pending command. The future will be resolved by the 'data_received' callback

        :param tag:
        :return:
        """
        self.commands[tag] = self.loop.create_future()
        return self.commands[tag]

    def __resolve_pending_request(self, result: dict) -> None:
        """
        Resolves a previously pending request
        :param result:
        :return:
        """
        future = self.commands.pop(result['tag'], None)
        future.set_result(result)

        was_rterm = result['tag_info']['property'] in ['CLIENT.RTERM', 'CLIENT.RTERMLOCK']
        was_ok = result['status'] == oldpyfgc.FgcReplies.OK
        if was_rterm and was_ok:
            self.rterm_enabled = True

    def disconnect(self):
        """
        Disconnects from the FGC
        """
        self.transport.close()

    async def handshake(self):
        """
        Handshake with the gateway
        """
        await self.send(oldpyfgc.fgc_tcp.NcrCodes.HANDSHAKE_CHAR)

    async def set(self, prop: str, value: Union[str, bytes, list], device: str = ''):
        """
        Asynchronous set. Will resolve when the response arrives.

        :param prop: property to set
        :param value: value
        :param device: device that should handle the command
        :return: future that will be resolved when the response arrived
        """
        tag = self.tag_provider.provide(prop)
        to_send = '!{} s {}:{} '.format(tag, device, prop).encode()
        to_send += oldpyfgc.FgcBase.format_send(value)
        await self.send(to_send)
        return await self.__register_pending_request(tag)

    async def get(self, prop: str, device: str = ''):
        """
        Asynchronous get. Will resolve when the response arrives.

        :param prop: property to get
        :param device: device that should handle the command
        :return: future that will be resolved when the response arrived
        """
        tag = self.tag_provider.provide(prop)
        to_send = '!{} g {}:{}{}'.format(tag, device, prop, oldpyfgc.fgc_base.FgcCode.COMMAND_EOL).encode()
        await self.send(to_send)
        return await self.__register_pending_request(tag)

    async def enable_rterm(self, channel: int, callback: Callable[[bytes], None], lock: bool = False):
        """
        Start a rterm communication with the FGC.

        :param callback: will be called everytime bytes are received from the FGC. This callback should take a single
        parameter that will be filled with received bytes.
        :param channel: channel on which to start the rterm communication
        :return: the result of the 'set rterm' command
        """
        self.receive_callback = callback
        prop_name = 'CLIENT.RTERMLOCK' if lock else 'CLIENT.RTERM'
        return await self.set(prop_name, str(channel))

    async def send(self, payload: bytes):
        """
        Send given payload to the gateway

        :param payload: Bytes to send
        """
        self.transport.write(payload)


class FgcAsync:
    """
    Asynchronous context manager for FgcAsyncProtocol
    """

    def __init__(self, device: str, port: int, loop: asyncio.AbstractEventLoop = None):
        self.device = device
        self.port = port
        self.loop = loop if loop is not None else asyncio.get_event_loop()
        self.fgc = None

    async def __aenter__(self) -> FgcAsyncProtocol:
        self.fgc = await FgcAsync.instance(self.device, self.port, self.loop)
        return self.fgc

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.fgc.disconnect()

    @staticmethod
    async def instance(host: str, port: int, loop: asyncio.AbstractEventLoop) -> FgcAsyncProtocol:
        """
        Get a new instance of a connected FgcAsyncProtocol

        :param loop: loop on which the instance will be instantiated
        :param host: gateway host
        :param port: gateway port
        :return: FgcAsyncProtocol instance
        """
        _, fgc = await loop.create_connection(lambda: FgcAsyncProtocol(loop), host, port)
        await fgc.handshake()
        return fgc

