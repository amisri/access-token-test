import os

# Bytes of padding before class data in fgc_stat structure
FGC_STAT_PRECLASS_PAD = 12

# Size of status block for device
FGC_STATUS_SIZE = 56

# MAximum device name length
FGC_MAX_DEV_LEN = 23

# Maximum number of FGCDs
FGC_MAX_FGCDS            = 400

# Name file location
if os.name == "nt":
	FGC_NAME_FILE = "//cs-ccr-samba1/pclhc/etc/fgcd/name"
else:
	FGC_NAME_FILE = "/user/pclhc/etc/fgcd/name"

FGC_NAME_FILE_URL = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/name"

# Subdevice base path location
if os.name == "nt":
    FGC_SUB_NAME_FILE_BASE = "//cs-ccr-samba1/pclhc/etc/fgcd/sub_devices/"
else:
    FGC_SUB_NAME_FILE_BASE = "/user/pclhc/etc/fgcd/sub_devices/"

FGC_SUB_NAME_URL_BASE = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/sub_devices/"

# Group file location
if os.name == "nt":
    FGC_GROUP_FILE = "//cs-ccr-samba1/pclhc/etc/fgcd/group"
else:
    FGC_GROUP_FILE = "/user/pclhc/etc/fgcd/group"

FGC_GROUP_FILE_URL = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/group"
