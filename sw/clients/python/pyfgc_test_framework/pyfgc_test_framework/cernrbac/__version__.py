
__title__ = 'cernrbac'
__description__ = 'Python client for authentication on CERN RBAC servers'
__url__ = 'https://gitlab.cern.ch/ccs/fgc'
__version__ = '1.1.2'
__author__ = 'Nuno Mendes'
__author_email__ = 'little.brat@cern.ch'
__license__ = 'GNU General Public License v3.0'
