import requests
import random
import getpass
import base64

from typing import Union
from . import exceptions


class RBAC(object):

    rbac_servers = ['rbac-pro-lb.cern.ch']
    rbac_port = 8443
    trials = 3

    def __init__(self) -> None:
        self.__token = None

        # Sets up the lifetime of a token in minutes:
        #     - Application and Local-master-token can't be bigger than 720 minutes
        #     - Master token maximum is 512640 minutes
        self.__lifetime = None

        # 0 for application token, 1 for master token and 2 for local master token.
        self.__token_type = None

    @property
    def token(self) -> Union[bytes, None]:
        if self.__token:
            return base64.b64decode(self.__token)
        return None

    @token.setter
    def token(self, value: bytes):
        self.__token = base64.b64encode(value)

    @property
    def lifetime(self):
        return self.__lifetime

    @lifetime.setter
    def lifetime(self, value: int):
        self.__lifetime = value

    @property
    def token_type(self):
        return self.__token_type

    @token_type.setter
    def token_type(self, value: int):
        self.__token_type = value

    def authenticate_by_login(self, username: str=None, password: str=None) -> bool:
        """

        :param username:    CERN given username
        :param password:    Respective password
        :return:    Boolean value that determines if the operation was successful or not.
        """
        request_body = dict()

        request_body['UserName'] = username
        request_body['Password'] = password

        return self.authenticate(request_body)

    def authenticate_by_saml(self, saml_response: base64) -> bool:
        """
        Useful for capable Single Sign-On (SSO) web services.

        :param saml_response:   Response from the SAML server
        :return:                Boolean value that determines if the operation was successful or not.
        """
        request_body = dict()
        request_body['SamlResponse'] = base64.b64decode(saml_response)
        return self.authenticate(request_body)

    def authenticate(self, request_body: dict=None) -> bool:
        """
        Authenticate the client. If no dictionary is given, it will try to authenticate by **location**. If a dictionary
        with 'UserName' and 'Password' keys is given, it will try to authenticate by credentials. This method is also
        used to renew the token.

        :param request_body:    Method is used for other authentication routines with different values in its \
        dictionary. Though, if it is empty, it will try to authenticate the client by location.

        :return:    Boolean value that determines if the operation was successful or not.
        """
        r = dict()

        if request_body:
            r.update(request_body)
        else:
            r['AccountName'] = getpass.getuser()

        if self.token_type and self.token_type in [0, 1, 2]:
            r['TokenType'] = self.token_type

        if self.lifetime and type(self.lifetime) is int:
            r['Lifetime'] = self.lifetime

        r['TokenFormat'] = 'TEXT'

        # Make a POST HTTP request to a random RBAC server over self.trials
        for server in [random.choice(self.rbac_servers) for _ in range(self.trials)]:

            # Must use verify=False due to self signed certificate on the RBAC server - Potentially Insecure
            r = requests.post('https://{}:{}/rba/'.format(server, self.rbac_port), data=r, verify=False)

            if r.status_code == requests.codes.ok:
                self.__token = r.text
                return True

            elif r.status_code == 401:
                raise exceptions.RbacAuthenticationFailure(r.text)

        raise exceptions.AuthenticationTrialsExceeded(r.text)

    def renew_token(self) -> bool:
        """
        The RBAC server can renew a token without requiring credentials of a client. This feature can be used to
        generate a new token based on a renewable application token, master token or a local master token. For the
        latter two types of tokens, renew means obtaining a new token based on an original one with the set of picked
        roles.

        :return: Boolean value that determines if the operation was successful or not.
        """
        if self.__token:
            request_body = dict()
            request_body['Origin'] = self.__token
            return self.authenticate(request_body)
        else:
            raise exceptions.UnexistentToken()

    def decipher_token(self) -> dict:
        """
        Even though this is not the correct token format to be sent to a server, it is useful for showing the token in
        a structured way.

        :return: Dictionary of every field in the token with its respective values.
        """
        if self.token:
            rbac_dict = dict()

            rbac_dict['Signature'] = self.token[-68:-4]
            rbac_dict['Signature_length'] = self.token[-4:]

            lines = self.token.split(b'\n')

            number_of_fields = int(lines[0])

            i = 0
            line_number = 1

            while i < number_of_fields:
                field = lines[line_number].decode("utf-8")

                line_number += 1

                field_type = lines[line_number].decode("utf-8")
                line_number += 1

                field_type_base = field_type

                is_array = False
                num_elements = 1

                if field_type == 'byte_array':
                    field_type_base = 'byte'
                    line_number += 1

                elif field_type.endswith('_array'):
                    field_type_base = field_type[:len('_array')]
                    is_array = True
                    num_elements = int(lines[line_number].decode("utf-8"))
                    line_number += 1

                elif field_type == 'string':
                    line_number += 1

                if is_array:
                    values = []
                    for j in range(num_elements):
                        values.append(RBAC.primitive_value(field_type_base, lines[line_number]))
                        line_number += 1

                    rbac_dict[field] = values

                else:
                    rbac_dict[field] = RBAC.primitive_value(field_type_base, lines[line_number])
                    line_number += 1

                i += 1

            return rbac_dict

        else:
            raise exceptions.UnexistentToken()

    @staticmethod
    def primitive_value(field_base_type: str, value: Union[bytes, bytearray]) -> Union[bool, float, str, bytes,
                                                                                       bytearray]:
        """
        Auxiliary method for decipher_token method.

        :param field_base_type: Given type of _value_.
        :param value:           Value in bytes/bytearray.
        :return:                Python native value.
        """

        if field_base_type == 'byte':
            return value

        decoded_value = value.decode('utf-8')

        if field_base_type == 'bool':

            if decoded_value == 'true':
                return True

            elif decoded_value == 'false':
                return False

        elif field_base_type in ['short', 'int', 'long', 'float', 'double']:
            return float(decoded_value)

        elif field_base_type == 'string':
            return decoded_value.replace('%2E', '.').replace('%2D', '-').replace('%20', ' ')

        else:
            raise exceptions.UnknownType()
