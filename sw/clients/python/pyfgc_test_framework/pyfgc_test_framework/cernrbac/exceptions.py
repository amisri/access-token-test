class RbacError(Exception):
    pass


class RbacAuthenticationFailure(RbacError):
    pass


class UnknownType(RbacError):
    pass


class UnexistentToken(RbacError):
    pass


class AuthenticationTrialsExceeded(RbacError):
    pass
