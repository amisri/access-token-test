import logging

from dataclasses import dataclass
from enum import Enum, auto
from pyfgc_test_framework.__version__ import __version__
from fortlogs_schemas import AnalogLog, DigitalLog, TableLog, EventLog, AnalogLogComparison, DigitalLogComparison

logger = logging.getLogger('pyfgc_test_framework')

TIMEOUT = 12  # original: 60
POLLING_PERIOD = 2  # original 2
DSP_MAX_RESETS = 10  # Max number of resets to perform in order to wake up DSP
PORT = 1905
REMOTE_TERMINAL_COMMAND_DEFAULT_TIMEOUT = 6
default_protocol = 'sync'


# Based on https://gitlab.cern.ch/ccs/fgc/-/blob/master/sw/lib/cclibs/libreg/src/regMgrReg.c line 104
# awk 'BEGIN{for(us=100;us<=100000;us+=100){g=12E6/us;if(int(g)==g)printf "%4u,%6u us,%10.4f Hz,%6g per 12s\n",us/100,us,1E6/us,12E6/us}}'
regulation_period = dict()
for us in range(100, 100000+100, 100):
    g = 12e6/us
    if int(g) == g:
        regulation_period[int(us/100)] = us

# Available fortlogs instances
class FortlogsInstance(Enum):
    DEV = auto()
    PROD = auto()
    PREPROD = auto()
    QA = auto()


@dataclass
class FortlogsConfig:
    """Class with fortlogs configuration"""
    API_URL: str


fortlogs_instance_mapping = {
    FortlogsInstance.DEV: FortlogsConfig("https://nulauren-dev.cern.ch/fortlogs"),
    FortlogsInstance.PROD: FortlogsConfig("https://accwww.cern.ch/fortlogs"),
    FortlogsInstance.PREPROD: FortlogsConfig("https://ccs-webtools-preprod.cern.ch/fortlogs"),
    FortlogsInstance.QA: FortlogsConfig("https://nulauren-dev.cern.ch/fortlogs")
}


# fortlogs instance to be used across the framework
fortlogs_config: FortlogsConfig = None


def set_fortlogs_config(config: FortlogsInstance = FortlogsInstance.PROD):
    global fortlogs_config
    fortlogs_config = fortlogs_instance_mapping[config]


if "dev" in __version__:
    set_fortlogs_config(FortlogsInstance.DEV)
else:
    set_fortlogs_config()


fortlogs_endpoints = {
    AnalogLog: "analog-logs",
    DigitalLog: "digital-logs",
    TableLog: "table-logs",
    EventLog: "table-logs/event-logs",
    AnalogLogComparison: "analog-logs/comparison",
    DigitalLogComparison: "digital-logs/comparison"
}

# EOF
