import base64
import logging
import os
import typing

import pyfgc_rbac


logger = logging.getLogger('pyfgc_test_framework')
_rbac_token: typing.Optional[bytes] = None


def set_rbac_token(token: bytes):
    """
    Set arbitrary rbac token to be used for authentication.
    Use the package pyfgc_rbac to generate a token.
    it is None by default and the framework uses authentication by location

    @param token: rbac token in pyfgc_rbac format
    """
    global _rbac_token
    _rbac_token = token

def get_rbac_token(user: str = None, password: str = None):
    """
    Get rbac token using kerberos , env variable or user/password
    """
    global _rbac_token

    encoded_token = base64.b64encode(_rbac_token).decode() if _rbac_token else None

    if encoded_token and not pyfgc_rbac.is_expired(encoded_token):
        return _rbac_token

    if 'RBAC_TOKEN_SERIALIZED' in os.environ:
        logger.debug('Getting a RBAC token from environment variable RBAC_TOKEN_SERIALIZED')
        encoded = os.environ["RBAC_TOKEN_SERIALIZED"]
        if not pyfgc_rbac.is_expired(encoded):
            _rbac_token = base64.b64decode(encoded)
            return _rbac_token

    if user and password:
        # If user and password provided use it to authenticate
        try:
            logger.debug('Getting a RBAC token using username and password')
            _rbac_token = pyfgc_rbac.get_token_login(user, password)
            return _rbac_token
        except Exception as err:
            logger.error('Failed to get a RBAC token using the credentials provided. Problem: %s', err)
            raise

    # Use Kerberos
    try:
        logger.debug('Getting a RBAC token using Kerberos')
        _rbac_token = pyfgc_rbac.get_token_kerberos()
        return _rbac_token
    except Exception as err:
        logger.error('Failed to get a RBAC token using Kerberos. Problem: %s', err)
        raise


# EOF