#
# Name:     inter_fgc.py
# Purpose:  functions to set FGC roles (MASTER/SLAVE)

import pyfgc
from pyfgc_test_framework import utils
import logging
from pyfgc_test_framework.consts import logger

# TODO: To be reviewed, some functions might be obsolete


def fgc_set_master(slave: str, master: str) -> bool:
    try:
        utils.set(slave, 'INTER_FGC.MASTER', master)
        res = utils.get(slave, 'INTER_FGC.MASTER').value

        if res == master:
            logger.info(f"{slave}: Master specified ({master})")
            return True
    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)
    return False


def fgc_set_slaves(master: str, slaves: str) -> bool:
    try:
        utils.set(master, 'INTER_FGC.SLAVES', slaves)
        res = utils.get(master, 'INTER_FGC.SLAVES').value

        if res == slaves:
            logger.info(f"{master}: Slaves specified ({slaves})")
            return True
    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)
    return False


def fgc_set_sig_sources(consumer: str, producers: str) -> bool:
    parsed_producers = list()

    for producer in producers.split(','):
        if 'NONE' not in producer and '@' not in producer:
            producer = "I_MEAS@" + producer

        parsed_producers.append(producer)

    parsed_producers = ','.join(parsed_producers)

    try:
        utils.set(consumer, 'INTER_FGC.SIG_SOURCES', parsed_producers)
        res = utils.get(consumer, 'INTER_FGC.SIG_SOURCES').value
        if res == parsed_producers:
            logger.info(f"{consumer}: Producers specified ({producers})")
            return True

    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)

    return False


def fgc_set_consumers(producer: str, consumers: str) -> True:
    try:
        utils.set(producer, 'INTER_FGC.CONSUMERS', consumers)
        res = utils.get(producer, 'INTER_FGC.CONSUMERS').value
        if res == consumers:
            logger.info(f'{producer}: Consumers specified ({consumers})')
            return True
    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)
    return False


def fgc_set_role(device: str, role: str, targets=list()) -> bool:

    tx_role_to_property = {
        'SLAVE':    fgc_set_master,
        'MASTER':   fgc_set_slaves,
        'PRODUCER': fgc_set_consumers,
        'CONSUMER': fgc_set_sig_sources
    }

    merged_targets = ','.join(targets)
    task = tx_role_to_property[role]

    return task(device, merged_targets)


def fgc_is_master(device: str) -> bool:
    try:
        return utils.get(device, 'INTER_FGC.SLAVES').value != 'NONE'
    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)

    return False


def fgc_is_slave(device: str) -> bool:
    try:
        return utils.get(device, 'INTER_FGC.MASTER').value != 'NONE'
    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)

    return False


def fgc_set_broadcast(devices: list, value: str):
    try:
        for device in devices:
            utils.set(device, 'INTER_FGC.BROADCAST', value)

    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)


def fgc_set_signals(device: str, signals: str) -> bool:
    try:
        utils.set(device, 'INTER_FGC.PRODUCED_SIGS', signals)
        res = utils.get(device, 'INTER_FGC.PRODUCED_SIGS').value

        if res == signals:
            logger.info(f'{device}: Signals specified ({signals})')
            return True

    except pyfgc.FgcResponseError as error:
        # print(error)
        logger.error(error)

    return False


def prepare_inter_fgc_test(devices: list):

    try:
        # Find all masters and turn them off, the slaves should follow
        for device in devices:
            if fgc_is_master(device):
                utils.set(device, 'PC', 'OF')
                if not utils.wait_until(lambda: 'OFF' in utils.get(device, 'STATE.PC').value):
                    logger.error(f'not ok - {device}: Master FLT_OFF/OFF')
                    # ok(get($device, "STATE.PC") =~ "OFF", "$device: Master FLT_OFF/OFF");
                    return False

        # Make sure that all slaves are off
        for device in devices:
            if fgc_is_slave(device):
                if not utils.wait_until(lambda: 'OFF' in utils.get(device, 'STATE.PC').value):
                    logger.error(f'not ok - {device}: Slave FLT_OFF/OFF')
                    # ok(get($device, "STATE.PC") =~ "OFF", "$device: Slave FLT_OFF/OFF");
                    return False

        # Turn off everything else
        for device in devices:
            if 'OFF' not in utils.get(device, 'STATE.PC').value:
                utils.set(device, 'PC', 'OF')
                if not utils.wait_until(lambda: 'OFF' in utils.get(device, 'STATE.PC').value):
                    logger.error(f'not ok - {device}: Master FLT_OFF/OFF')
                    # ok(get($device, "STATE.PC") =~ "OFF", "$device: Master FLT_OFF/OFF");
                    return False

        # Clear role properties
        result = True
        for device in devices:
            if fgc_is_master(device):
                result &= fgc_set_role(device, 'MASTER', ["NONE"])
                result &= fgc_set_role(device, 'SLAVE', ['NONE'])
            else:
                result &= fgc_set_role(device, 'SLAVE', ['NONE'])
                result &= fgc_set_role(device, 'MASTER', ['NONE'])

            result &= fgc_set_role(device, 'CONSUMER', ['NONE'])
            result &= fgc_set_role(device, 'PRODUCER', ['NONE'])

            result &= fgc_set_signals(device, "NONE")

    except pyfgc.FgcResponseError as error:
        logger.error(error)
        return False

    return result

# EOF