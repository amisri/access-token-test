import pydantic
import requests
import base64

from typing import List, Union, Tuple, Optional

from pyfgc_test_framework import exceptions, consts, authenticate
from fortlogs_schemas import AnalogLog, DigitalLog, TableLog, AnalogLogComparison, DigitalLogComparison


spy_acq: Optional[dict]


def fortlogs_acquisition():
    global spy_acq
    if spy_acq is None:
        consts.logger.error("No spy acquisition has been created. Please run: create_spy_acquisition(scripts_name, description)")
    return spy_acq["acquisition_id"]


def create_spy_acquisition(script_name: str, description="", access_token: str = None) -> dict:
    """

    :param script_name: name of the test
    :param description: description of the test
    :param access_token: access token to use for authentication can be both RBAC or OAUTH2 token
    :return: acquisition object created
    """
    global spy_acq
    if spy_acq:
        consts.logger.warning("Fortlogs acquisition already created, returning the existing one.")
        return spy_acq

    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    new_acquisition = requests.post(
        f"{consts.fortlogs_config.API_URL}/fgc-tests",
        json={"script_name": script_name.replace(".", "_"), "script_description": description.replace(" ", "_")},
        headers={"Authorization": f"Bearer {encoded_token}"},
        verify=False,
    )

    if not new_acquisition.ok:
        raise exceptions.FortlogsError(f'Failed to create a new fortlogs acquisition\n'
                                       f'ERROR - status code {new_acquisition.status_code} - {new_acquisition.text}\n'
                                       f'acquisition_name={script_name}')
    spy_acq = new_acquisition.json()
    return spy_acq


def add_log(acquisition_id: int, log: Union[AnalogLogComparison, DigitalLogComparison, AnalogLog, DigitalLog, TableLog], access_token: str = None) -> int:
    """

    :param acquisition_id: target acquisition's ID
    :param log: log to insert
    :param log_type: type of the log object
    :param access_token: access token to use for authentication can be both RBAC or OAUTH2 token
    :return: log's ID
    """
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    # retrieve the endpoint
    if type(log) in consts.fortlogs_endpoints.keys():
        endpoint = consts.fortlogs_endpoints[type(log)]
    else:
        raise exceptions.UnknownLogType(
            f"log has type {type(log)}, expecting AnalogLogComparison, DigitalLogComparison, TableLog or EventLog\n"
            f"log={log}"
        )

    add_log_request = requests.post(
        f"{consts.fortlogs_config.API_URL}/{endpoint}",
        params={"acquisition_id": acquisition_id},
        json=log.dict(),
        headers={"Authorization": f"Bearer {encoded_token}"},
        verify=False,
    )

    if not add_log_request.ok:
        consts.logger.error('Failed to add log to acquisition')
        consts.logger.error(f'ERROR - status code {add_log_request.status_code} - {add_log_request.text}')
        return -1

    return add_log_request.json()


def get_log_meta(acquisition_id: int) -> list:
    """

    :param acquisition_id: target acquisition's ID
    :return: list of log meta data
    @param acquisition_id:
    """
    get_meta_request = requests.get(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acquisition_id}/logs-meta",
        verify=False,
    )

    if not get_meta_request.ok:
        consts.logger.error('Failed to get log meta')
        consts.logger.error(f'ERROR - status code {get_meta_request.status_code} - {get_meta_request.text}')
        return []

    return get_meta_request.json()


def get_acquisition(acquisition_id: int) -> dict:
    """

    :param acquisition_id: target acquisition's ID
    :return: list of log meta data
    @param acquisition_id:
    """
    get_meta_request = requests.get(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acquisition_id}",
        verify=False,
    )

    if not get_meta_request.ok:
        consts.logger.error('Failed to get acquisition')
        consts.logger.error(f'ERROR - status code {get_meta_request.status_code} - {get_meta_request.text}')
        return {}

    return get_meta_request.json()


def get_logs_from_acquisition(acquisition_id: int) -> List[Union[AnalogLogComparison, DigitalLogComparison, dict]]:
    """

    :param acquisition_id: target acquisition's ID
    :return: list of log objects
    """
    get_request = requests.get(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acquisition_id}/logs",
        verify=False,
    )

    if not get_request.ok:
        consts.logger.error('Failed to get log meta')
        consts.logger.error(f'ERROR - status code {get_request.status_code} - {get_request.text}')
        return {}

    logs = list()
    for log in get_request.json():
        try:
            if log['data']['type'] == 'analog':
                tup = (log['log_id'], AnalogLogComparison(**log['data']))
            elif log['data']['type'] == 'digital':
                tup = (log['log_id'], DigitalLogComparison(**log['data']))
            else:
                tup = (log['log_id'], log)
            logs.append(tup)
        except pydantic.error_wrappers.ValidationError:
            continue

    return logs


def search_acquisitions(script_name: str = None, script_description: str = None, device: str = None, user_id: int = None,
                        live: bool = None, comment:str = None, log_name: str = None, tags: List[int] = None,
                        favorite_user: int = None, commented_only=False, from_datetime=None, to_datetime=None,
                        is_reference=False, trashed=False, limit=100,
                        test_case=None, offset=0,  order='desc', source='fgc-tests') -> dict:
    params = dict()

    if script_name:
        params['script_name'] = script_name
    if script_description:
        params['script_description'] = script_description
    if user_id:
        params['user'] = f'{user_id}'
    if device:
        params['device'] = f'{device}'
    if live is not None:
        params['live'] = live
    if comment:
        params['comment'] = f'{comment}'
    if log_name:
        params['log_name'] = f'{log_name}'
    if tags:
        params['tags'] = tags
    if favorite_user:
        params['favorite_user'] = favorite_user
    if from_datetime:
        params['from'] = f'{from_datetime}'
    if to_datetime:
        params['to'] = f'{to_datetime}'
    if test_case:
        params["test_case"] = test_case

    params['only_reference'] = is_reference
    params['commented_only'] = commented_only
    params['trashed'] = trashed
    params['limit'] = limit
    params['offset'] = offset
    params['order'] = order

    res = requests.get(
        f"{consts.fortlogs_config.API_URL}/{source}",
        params=params,
        verify=False
    )

    return res.json()


def trash_acquisition(acq_id: int, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.put(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/trash",
        headers={"Authorization": f"Bearer {encoded_token}"},
        verify=False,
    )

    if not res.ok:
        consts.logger.error('Failed to trash acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def untrash_acquisition(acq_id: int, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.put(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/untrash",
        headers={"Authorization": f"Bearer {encoded_token}"},
        verify=False,
    )

    if not res.ok:
        consts.logger.error('Failed to untrash acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def comment_acquisition(acq_id: int, comment: str, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.put(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/comment",
        headers={"Authorization": f"Bearer {encoded_token}"},
        json={'comment': comment},
        verify=False,
    )
    if not res.ok:
        consts.logger.error('Failed to comment acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def star_acquisition(acq_id: int, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.put(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/star",
        headers={"Authorization": f"Bearer {encoded_token}"},
        verify=False,
    )
    if not res.ok:
        consts.logger.error('Failed to star acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def unstar_acquisition(acq_id: int, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.delete(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/star",
        headers={"Authorization": f"Bearer {encoded_token}"},
        verify=False
    )
    if not res.ok:
        consts.logger.error('Failed to unstar acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def tag_acquisition(acq_id: int, tag: str, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.post(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/tags",
        headers={"Authorization": f"Bearer {encoded_token}"},
        json=[tag],
        verify=False
    )
    if not res.ok:
        consts.logger.error('Failed to tag acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def untag_acquisition(acq_id: int, tag: str, access_token: str = None) -> bool:
    if access_token:
        encoded_token = access_token
    else:
        rbac_token = authenticate.get_rbac_token()
        encoded_token = base64.b64encode(rbac_token).decode()

    res = requests.delete(
        f"{consts.fortlogs_config.API_URL}/acquisitions/{acq_id}/tags",
        headers={"Authorization": f"Bearer {encoded_token}"},
        json=[tag],
        verify=False
    )
    if not res.ok:
        consts.logger.error('Failed to untag acquisition')
        consts.logger.error(f'ERROR - status code {res.status_code} - {res.text}')

    return res.ok


def _get_list_of_ref_logs(log_name: str, test_case: str) -> List[Tuple[int, Union[AnalogLog, DigitalLog]]]:
    """
    Retrieves list of starred logs in fortlogs/powerspy ordered from oldest to newest.
    One can get the most recent reference with:
    _, ref_log = get_reference_logs(...).pop()

    @param log_name:
    @param test_case:
    @return: List of tuples (acquisition_id, Log object)
    """
    # search acquisitions from fgc-tests endpoint only
    acquisitions = search_acquisitions(spy_acq["script_name"],
                                       spy_acq["script_description"],
                                       log_name=log_name,
                                       test_case=test_case,
                                       is_reference=True)

    ref_logs = []
    for acq in acquisitions:
        acquisition_logs = get_logs_from_acquisition(acq["acquisition_id"])
        ref_logs += [(log_id, log) for log_id, log in acquisition_logs if
                     (not isinstance(log, dict) and log_name in log.name and test_case in log.test_case)]

    return ref_logs


def get_spy_reference(log_name: str, test_case: str = None) -> Tuple[int, Union[AnalogLog, DigitalLog]]:
    """
    Retrieve the laste reference log from Fortlogs

    @param log_name:
    @param test_case:
    @return: Tuple (log id, log object)
    """
    log_id, log = None, None

    ref_logs = _get_list_of_ref_logs(log_name, test_case)
    if len(ref_logs):
        log_id, log = ref_logs[0]

    return log_id, log

# EOF
