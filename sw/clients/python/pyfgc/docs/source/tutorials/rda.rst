RDA/CMW
#######

Works by connecting to an FGC using CERN's (R)emote (D)evice (A)ccess library based on ZeroMQ 
peer-to-peer networking library, which is the standard controls middleware used at the CERN 
accelerator complex to control equipment devices.

++++++++++++++++++++++
Let's do a GET and SET
++++++++++++++++++++++

.. code:: python

    import pyfgc

    res = pyfgc.get("device", "property", protocol="rda")
    print(res.value)

    res = pyfgc.set("device", "property", "set_value", protocol="rda")
    res = pyfgc.get("device", "property", protocol="rda")
    print(res.value)

+++++++++
Subscribe
+++++++++

RDA, unlike the other protocols, allows us to subscribe to properties.
There are two ways to do it, using the ``subscribe`` context manager or using 
the ``create_manual_subscription_session`` function.


-------------------------
subscribe context manager
-------------------------

Handles opening and closing subscriptions for you and allows subscribing to one or more properties.
For that we pass it a callback function. Bear in mind that the callback will be shared by all 
subscriptions, if more than one property was passed as argument. Thus you might be unable to tell
which property the data corresponds to.

.. code:: python

    import pyfgc

    collected_data = list()

    def callback_fun(data):
        collected_data.append(data)
        print(f'Received data:\n{data.value}')

    with pyfgc.subscribe("rfna.866.04.eth2", ["MEAS.V.REF"], callback_fun) as listener:
        time.sleep(3)

    print(collected_data)

----------------------------------
create_manual_subscription_session
----------------------------------

It is also possible to create a single subscription, without the ``with`` statement, and decide when to close it.

.. code:: python

    import pyfgc

    def callback_fun(data):
        print(f'Received data:\n{data.value}')

    subscription = pyfgc.create_manual_subscription_session("rfna.866.04.eth2", "MEAS.V", callback_fun)
    time.sleep(3)

    subscription.disconnect()


---------------------------------
Filter subscription by user/cycle
---------------------------------

For this purpose there is an additional optional argument named ``cycle_selector``:

.. code:: python

    import pyfgc

    def callback_fun(data):
        print(f'Received data:\n{data.value}')

    subscription = pyfgc.create_manual_subscription_session("rfna.866.04.eth2", "MEAS.V", callback_fun, cycle_selector='PSB.USER.AD')
    time.sleep(3)

    subscription.disconnect()