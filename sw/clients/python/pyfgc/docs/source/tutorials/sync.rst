TCP/IP
######

A client can send commands to FGC's using TCP/IP, but in order to do so, the commands have to be sent through an 
intermediary, its front-end computer. This intermediary is called the FGC server. The FGC server is a program that
runs on the front-end computer and that listens for incoming TCP/IP connections. When a connection is established,
a RBAC token can be sent to the FGC server to be verified. If the sent token is valid, for every command the FGC server
receives, the server will forward and collect responses from the commands to the FGC's and returns the results to the client.
There is an application layer protocol that is used to communicate between the FGC server and the client. The protocol
is called FGC Network Command/Response Protocol (NCRP).

++++++++++++
Get Commands
++++++++++++

.. code:: python
   
   # Let's try to get the current time from the FGC
   current_time = pyfgc.get("RFNA.866.01.ETH1", "time.now")
   print(current_time.value)

++++++++++++
Set Commands
++++++++++++

.. code:: python

   # Let's try to set one of the test properties 
   pyfgc.set("RFNA.866.01.ETH1", "test.char", 'hello')
   response = pyfgc.get("RFNA.866.01.ETH1", "test.char")
   print(response.value)

+++++++++++++++
Handling Errors
+++++++++++++++

.. code:: python

   # Let's set an unknown property
   response = pyfgc.get("RFNA.866.01.ETH1", "bad.property.name")
   
   # The response will contain an error and this will be raised when accessing its value
   # We will have to catch the exception and handle it
   try:
       print(response.value)
   except pyfgc.FgcResponseError as e:
       print(f"ERROR SENDING COMMAND: {response.err_code} {response.err_msg}")

++++++++++++++++++++
Handling RBAC Tokens
++++++++++++++++++++

The pyfgc library default behavior is to automatically acquire an RBAC token in
the following order:

1. It will try to use one from the current shell environment given to the 
variable named ``RBAC_TOKEN_SERIALIZED``, if it exists;


2. It will try to acquire a token by location, if possible;
3. It will try to acquire by a kerberos (GSSAPI) ticket, if possible;
4. Fail and raise an exception ``pyfgc.PyFgcError``.

Otherwise, you can always manually pass an RBAC token to your pyfgc commands:

.. code:: python

    RBAC_TOKEN = "" # Your RBAC token
    response = pyfgc.get("RFNA.866.01.ETH1", "time.now", rbac_token=RBAC_TOKEN)
    print(response.value)

If this library is running on an environment that has no access to RBAC servers 
(i.e. External Laboratory Facilities), the RBAC token has to be handled on each API call 
by passing ``None`` to the ``rbac_token`` parameter.

.. code:: python

    response = pyfgc.get("RFNA.866.01.ETH1", "time.now", rbac_token=None)
    print(response.value)

This library will always try to renew the RBAC token if necessary.

+++++++++++++++
Keeping Session
+++++++++++++++

One can keep session alive by either using the available context manager or by manually 
controlling the connection.
This is useful when you want to send a sequence commands to the same FGC
without having to wait for the connection to be established each time.
Remember that the RBAC token has to fetched & set on each connection.
**This method is not recommended because it might cause a denial of service
of its FGC gateway.**

* Using the context manager:

.. code:: python

    with pyfgc.fgc("RFNA.866.01.ETH1") as fgc:
        for i in range(10):
            try:
                r = fgc.get("time.now")
                print(r.value)
            except pyfgc.FgcResponseError:
                pass

* Manually controlling the connection:

.. code:: python
   
    try:
        session = pyfgc.connect("RFNA.866.01.ETH1")
        r = session.get("time.now")
        print(r.value)

    except pyfgc.FgcResponseError:
        # Do something here: try again? exit?
        pass

    finally:
        session.close()
