USB (Serial Port)
#################

Works by connecting a FGC using a double male ended USB cord to your computer.
There should be two connections available, one for the serial comand response protocol and another
for the SPY interface to collect data about the internal SPY buffers that have a relatively high 
bandwidth in comparison with the first. Usually, the serial port that appears first when you connect the FGC
is the one that is used for the command response protocol and the other one is used for the SPY interface.

The serial connection can be in one of the following modes:
   - Direct: for a control application (this should be the desired mode for pyfgc)
   - Editor (Remote Terminal): for a human using an ANSI/VT100 terminal
   - Diagnostic: for a diagnostic application - DEPRECIATED - NOT SUPPORTED BY FGC3

**Most of the documentation written above is valid also for this use case, just by adding the protocol argument.
Just remember that there is no RBAC server involved in this case, and if an RBAC token is passed it will be ignored.**

.. code:: python

   import pyfgc

   # While using USB (MUST FIND PORT)
   r = pyfgc.get("COM3", "device", protocol="serial")
   print(r.value)

   pyfgc.set("COM3", "test.char", 1, protocol="serial")
   y = pyfgc.get("COM3", "test.char", protocol="serial")
   print(y.value)
