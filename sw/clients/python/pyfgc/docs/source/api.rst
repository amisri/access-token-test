API
===

.. autofunction:: pyfgc.get

.. autofunction:: pyfgc.set

.. autofunction:: pyfgc.fgc

.. autofunction:: pyfgc.connect

.. autofunction:: pyfgc.disconnect

.. autofunction:: pyfgc.subscribe


Async API
---------

.. autofunction:: pyfgc.async_connect

.. autofunction:: pyfgc.async_disconnect

.. autofunction:: pyfgc.async_get

.. autofunction:: pyfgc.async_set


Extra Utilities
---------------

.. autofunction:: pyfgc.monitor_port

.. autofunction:: pyfgc.monitor_session

.. autofunction:: pyfgc.connect_rt

.. autofunction:: pyfgc.create_manual_subscription_session


Session Classes
---------------

.. autoclass:: pyfgc.FgcSession

.. autoclass:: pyfgc.FgcAsyncSession

.. autoclass:: pyfgc.async_fgc


Response Classes
----------------

.. autoclass:: pyfgc.FgcResponse
   :members:
   :undoc-members:

.. autoclass:: pyfgc.responses.async_sync_response.AsyncSyncResponse
   :members:
   :undoc-members:

.. autoclass:: pyfgc.responses.serial_response.SerialResponse
   :members:
   :undoc-members:

.. autoclass:: pyfgc.responses.rda_response.RdaResponse
   :members:
   :undoc-members:


Protocol Classes
----------------

.. autoclass:: pyfgc.protocols.sync_protocol.SyncProtocol
   :members:
   :undoc-members:

.. autoclass:: pyfgc.protocols.serial_protocol.SerialProtocol
   :members:
   :undoc-members:

.. autoclass:: pyfgc.protocols.async_protocol.AsyncProtocol
   :members:
   :undoc-members:

.. autoclass:: pyfgc.protocols.rda_protocol.RdaProtocol
   :members:
   :undoc-members:

.. autoclass:: pyfgc.protocols.rda_protocol.RdaSubscriptionListener
   :members:
   :undoc-members:


Channel Classes
---------------

.. autoclass:: pyfgc.channels.serial_channel.SerialChannel
   :members:
   :undoc-members:

.. autoclass:: pyfgc.channels.sync_channel.SyncChannel
   :members:
   :undoc-members:

.. autoclass:: pyfgc.channels.async_channel.AsyncChannel
   :members:
   :undoc-members:

.. autoclass:: pyfgc.channels.rda_channel.RdaChannel
   :members:
   :undoc-members:

.. autoclass:: pyfgc.channels.channel_manager.ChannelManager
   :members:
   :undoc-members:




Miscellaneous Classes
---------------------

.. autoclass:: pyfgc.constants.ProtocolTypes

.. autoclass:: pyfgc.constants.CommandResponseProtocolSymbols


Exceptions
----------

.. autoclass:: pyfgc.AmbiguousTarget

.. autoclass:: pyfgc.ChannelError

.. autoclass:: pyfgc.FgcResponseError

.. autoclass:: pyfgc.InvalidRbacTokenError

.. autoclass:: pyfgc.ProtocolNotSupportedError

.. autoclass:: pyfgc.PyFgcError

.. autoclass:: pyfgc.PyFgcSessionError

.. autoclass:: pyfgc.UnknownDevice

