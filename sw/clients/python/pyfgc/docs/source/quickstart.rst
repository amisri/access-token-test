About
#####

pyfgc is a library that allows clients to communicate with Function Generation Controllers (FGCs) via
different communication protocols. 

We currently support the following protocols:
    - TCP/IP through a Front-End Computer (FEC) / Gateway;
    - USB Serial interface (RS232);
    - RDA (Remote Device Access);

Support for the following protocols is planned but not implemented yet:
    - HTTP through FGC-API;
    - WebSocket through FGC-API;


The FGC system accepts only two commands: SET and GET.  These can be directed at Properties belonging to Devices within the FGC system.  
The list of Properties included within a Device depends upon the Device's Class.


Installation
############

The easiest way is to activate the ACC-PY environment by following this `guide <https://wikis.cern.ch/display/ACCPY/Getting+started+with+Acc-Py>`_:

Then ``pyfgc`` can be installed with pip::
   
   pip install pyfgc


`You'll need the correct RBAC role and to be in the same network than the device you want to access.`

If you want to use kerberos authentication for RBAC you'll need to install ``pyfgc`` like this::

    pip install pyfgc[kerberos]
    

Quick Start
###########


.. code:: python
   
    pyfgc.set("RFNA.866.01.ETH1", "test.char", "hello")
    print(pyfgc.get("RFNA.866.01.ETH1", "test.char"))


Contact
#######

Please send your bug/improvement reports to converter-controls-support@cern.ch