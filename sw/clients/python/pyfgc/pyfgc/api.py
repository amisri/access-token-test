"""Pyfgc API

This module provides several utilities to communicate with CERN's FGCs.
It abstracts the usage of several protocols such as SCRP, NCRP and UDP status
publication.

Pyfgc is compatible with python asyncio module.

Todo:
    * After upgrading from python 3.6 to 3.7/3.8, the asyncio implementation
    should be revisited, to adopt the new (better) features provided by these
    releases.
"""
import logging

from contextlib import contextmanager
from typing import Callable, Dict, List

from pyfgc.constants import ProtocolTypes, ASYNC_TIMEOUT_S, TIMEOUT_S
from pyfgc.exceptions import PyFgcSessionError
from pyfgc.fgc_session import FgcSession, FgcAsyncSession
from pyfgc.fgc_monitor import MonitorSession, MonitorPort
from pyfgc.fgc_rt import FgcRt
from pyfgc.responses.fgc_response import FgcResponse

logger = logging.getLogger(__name__)


# Synchronous functions

@contextmanager
def fgc(target_device_name: str, protocol: str = ProtocolTypes.SYNC.value, rbac_token: bytes = None,
        timeout_s: int = TIMEOUT_S, name_file: str = None):
    """Synchronous context manager

    Context manager that handles the creation/destruction of a synchronous fgc
    session.

    Depending on the protocol, the connection channel may be shared between
    different sessions.

    Args:
        target_device_name (str): Target fgc name.
        protocol (str, optional): Communication protocol (``sync``/``serial``). Defaults to ``sync``.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.
        name_file (str): Path/url to name file.

    Yields:
        FgcSession: fgc session object that can be used to send ``get``/``set`` commands

    Raises:
        PyFgcSessionError: If an error occurs during the creation of the session
    """
    fgc_session = None

    try:
        fgc_session = FgcSession(target_device_name, protocol, rbac_token, timeout_s, name_file)
        yield fgc_session
    except Exception as err:
        raise PyFgcSessionError(f"Exception in pyfgc context manager: {err}") from err
    finally:
        if fgc_session:
            disconnect(fgc_session)


def connect(target_device_name: str, protocol: str = ProtocolTypes.SYNC.value, rbac_token: bytes = None,
            timeout_s: int = TIMEOUT_S, name_file: str = None):
    """Open connection to a device

    Args:
        target_device_name (str): Target fgc name.
        protocol (str, optional): Communication protocol (``sync``/``serial``). Defaults to ``sync``.
        name_file (str): Path/url to name file.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.
    
    Returns:
        FgcSession: fgc session object
    
    Raises:
        PyFgcSessionError: If an error occurs during the creation of the session
    """
    fgc_session = None

    try:
        fgc_session = FgcSession(target_device_name, protocol, rbac_token, timeout_s, name_file)
    except Exception as err:
        raise PyFgcSessionError(f"Error building FgcSession for target {target_device_name}, "
                                f"protocol {protocol}: {err}") from err

    return fgc_session


def disconnect(fgc_session: FgcSession) -> None:
    """Close connection to a device

    Args:
        fgc_session (FgcSession): Fgc session to close.
    """
    fgc_session.disconnect()


def get(target_device_name: str, property_name: str, get_option: str = None, protocol: str = ProtocolTypes.SYNC.value,
        rbac_token: bytes = None, timeout_s: int = TIMEOUT_S, name_file: str = None) -> FgcResponse:
    """Returns a ``FgcResponse`` containing the value of the specified property from the specified target device

    Does not require the explicit creation of a session.
    However, it has a bigger overhead (session is created and destroyed internally).

    Args:
        target_device_name (str): Target fgc name.
        property_name (str): Property to get.
        get_option (str, optional): Get option.
        protocol (str, optional): Communication protocol (``sync``/``serial``). Defaults to ``sync``.
        name_file (str): Path/url to name file.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.

    Returns:
        FgcResponse: FGC response object
    """
    with fgc(target_device_name, protocol, rbac_token, timeout_s, name_file) as fgc_session:
        return fgc_session.get(property_name, get_option)


# This is overriding the keyword set for lists with immutable elements, 
# but it is the only way to keep a consistent API according to the original protocol definition
def set(target_device_name: str, property_name: str, value: str, protocol: str = ProtocolTypes.SYNC.value,
        rbac_token: bytes = None, timeout_s: int = TIMEOUT_S, name_file: str = None) -> FgcResponse:
    """Sets the provided value to the provided ``property_name`` on the provided device.
    Returns a ``FgcResponse`` from the device.

    Does not require the explicit creation of a session.
    However, it has a bigger overhead (session is created and destroyed internally).

    Args:
        target_device_name (str): Target fgc name.
        property_name (str): Property to set.
        value (str): New value.
        protocol (str, optional): Communication protocol (``sync``/``serial``). Defaults
            to ``sync``.
        name_file (str): Path/url to name file.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.

    Returns:
        FgcResponse: FGC response object
    """
    with fgc(target_device_name, protocol, rbac_token, timeout_s, name_file) as fgc_session:
        return fgc_session.set(property_name, value)


@contextmanager
def subscribe(target_device_name: str, property_names: List[str], on_receive_callback: Callable[[FgcResponse], None],
              cycle_selector: str = None, **kwargs):
    """
    Subscription context manager.
    Subscribes to one or more FGC properties of the given target device.

    Beware:
    The provided callback will be invoked for all of the subscribed properties.
    The handed ``FgcResponse`` however tells you to which property it belongs to via the ``FgcResponse.property_name``.

    Usage: ``with subscribe(...) as subscribe_session``
    Connections are automatically closed as we exit the ``with`` statement

    Args:
        target_device_name (str): Target fgc name
        property_names (list): List of properties to subscribe to
        on_receive_callback (Callable): mandatory callback function that gets invoked whenever a ``FgcResponse`` is received
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        cycle_selector (str): Name of the cycle selector (default is ``None``) to filter the subscription
    
    Yields:
        FgcSession: object with active subscription(s)
    """
    with fgc(target_device_name, protocol=ProtocolTypes.RDA.value, **kwargs) as fgc_session:
        for property_name in property_names:
            fgc_session.subscribe(property_name, on_receive_callback, cycle_selector=cycle_selector)

        yield fgc_session

        for property_name in property_names:
            fgc_session.unsubscribe(property_name)


def create_manual_subscription_session(target_device_name: str, property_name: str,
                                       on_receive_callback: Callable[[FgcResponse], None], cycle_selector: str = None,
                                       **kwargs):
    """
    Subscribes to the given property of the given target device and returns the ``FGCSession`` object that handles the subscription.
    The subscription must be stopped manually via the ``session.unsubscribe()`` or ``session.disconnect()`` methods.

    Args:
        target_device_name (str): Target fgc name
        property_name (str): Property to subscribe
        on_receive_callback (Callable): mandatory callback function that gets invoked whenever a ``FgcResponse`` is received
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        cycle_selector (str): Name of the cycle selector (default is ``None``) to filter the subscription

    Returns:
        FgcSession: object with active subscription
    """
    fgc_session = FgcSession(target_device_name, protocol=ProtocolTypes.RDA.value, **kwargs)
    fgc_session.subscribe(property_name, on_receive_callback, cycle_selector=cycle_selector)
    return fgc_session


# Asynchronous functions (asyncio coroutines)
# TODO: Rename this to have a more consistent naming scheme
class async_fgc:
    """Asynchonous context manager

    Async context manager that handles the creation/destruction of an asynchrounous fgc
    session.

    The connection channel may be shared between different sessions.

    Args:
        target_device_name (str): Target fgc name.
        protocol (str, optional): Communication protocol (async/...). Defaults to async.
        name_file (str): Path/url to name file.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.

    Returns:
        FgcAsyncSession: fgc session object
    """

    # TODO: Replace this class by a function with @asynccontextmanager decorator, in python 3.7.
    def __init__(self, target_device_name: str, protocol: str = ProtocolTypes.ASYNC.value, rbac_token: bytes = None,
                 timeout_s: int = ASYNC_TIMEOUT_S, name_file: str = None):
        self._target_device_name = target_device_name
        self._protocol = protocol
        self._name_file = name_file
        self._rbac_token = rbac_token
        self._timeout_s = timeout_s
        self._async_session = None

    async def __aenter__(self):
        self._async_session = await async_connect(self._target_device_name, self._protocol, self._rbac_token,
                                                  self._timeout_s, self._name_file)
        return self._async_session

    async def __aexit__(self, exc_type, exc, tb):
        if self._async_session:
            await async_disconnect(self._async_session)


async def async_connect(target_device_name: str, protocol: str = ProtocolTypes.ASYNC.value, rbac_token: bytes = None,
                        timeout_s: int = ASYNC_TIMEOUT_S, name_file: str = None):
    """Open async connection to a device

    Args:
        target_device_name (str): Target fgc name.
        protocol (str, optional): Communication protocol (async/...). Defaults to async.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.
        name_file (str): Path/url to name file.

    Returns:
        FgcAsyncSession: fgc session object
    """
    fgc_session = None

    try:
        fgc_session = await FgcAsyncSession.instance(target_device_name, protocol, rbac_token, timeout_s,
                                                     name_file)
    except Exception as err:
        raise PyFgcSessionError(f"Error building FgcSession for target {target_device_name}, "
                                f"protocol {protocol}: {err}") from err

    return fgc_session


async def async_disconnect(fgc_session: FgcAsyncSession) -> None:
    """Close async connection to a device

    Args:
        fgc_session (FgcAsyncSession): Fgc session to close.
    """
    await fgc_session.disconnect()


async def async_get(target_device_name: str, property_name: str, get_option: str = None,
                    protocol: str = ProtocolTypes.ASYNC.value, rbac_token: bytes = None,
                    timeout_s: int = ASYNC_TIMEOUT_S, name_file: str = None):
    """Get a device property asynchronously

    Does not require the explicit creation of a session.
    However, it has a bigger overhead (session is created and destroyed internally).

    Args:
        target_device_name (str): Target fgc name.
        property_name (str): Property to get.
        get_option (str, optional): Get option.
        protocol (str, optional): Communication protocol (async/...). Defaults to async.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.
        name_file (str): Path/url to name file.

    Returns:
        FgcResponse: FGC response object
    """
    async with async_fgc(target_device_name, protocol, rbac_token, timeout_s, name_file) as fgc_session:
        return await fgc_session.get(property_name, get_option)


async def async_set(target_device_name: str, property_name: str, value: str, protocol: str = ProtocolTypes.ASYNC.value,
                    rbac_token: bytes = None, timeout_s: int = ASYNC_TIMEOUT_S, name_file: str = None):
    """Set a device property asynchronously

    Does not require the explicit creation of a session.
    However, it has a bigger overhead (session is created and destroyed internally).

    Args:
        target_device_name (str): Target fgc name.
        property_name (str): Property to set.
        value (str): New value.
        protocol (str, optional): Communication protocol (async/...). Defaults to async.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.
        timeout_s (int): Timeout in seconds.
        name_file (str): Path/url to name file.

    Returns:
        FgcResponse: FGC response object
    """
    async with async_fgc(target_device_name, protocol, rbac_token, timeout_s, name_file) as fgc_session:
        return await fgc_session.set(property_name, value)


# Monitor

def monitor_session(callback: Callable[[Dict, List, str, float], None],
                    targets, period, timeout: float = None, callback_err=None,
                    **kwargs):
    """Subscribe gateway to get FGC published properties (status data)
    https://accwww.cern.ch/proj-fgc/proj-fgc/gendoc/def/Class63_pubdata.htm

    Opens a session to one (or more) FGC gateway, and subscribes to its published
    properties.
    Callback will be invoked everytime a new UDP packet is received.

    Can be used as a context manager.
    Alternativelly, can be started with ``self.start()`` and stopped with ``self.stop()``.

    Args:
        callback (Callable): Callback that will be invoked when new data is
            received.
        targets (List): List of targets to monitor.
        period (int): Subcribed data refresh period.
        timeout (float, optional): Timeout for subcribed data (in seconds).
        callback_err (Callable, optional): Extra callback that will be invoked
            when there was an exception during the original callback execution.
        name_file (str): Path/url to name file.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.

    ``callback`` arguments::

        Dict: Dictionary mapping device names to dictionaries of published
            properties.
        {
            <device_name (str)>: {
                <property_name (str)>: <property_value>,
                ...
            },
            ...
        }
        List: Array of 64 iterm, mapping device positions [0..64] to dictionaries
            of published properties.
        [
            {
                <property_name (str)>: <property_value>,
                ...

            }, # Device 0
            {
                ...
            }, # Device 1
            ...
            {
                ...
            }  # Device 64
        ]
        str: Name (address) of gateway that sent current data fields.
        float: Timestamp of current data, as reported by the gateway.

    ``callback_err`` arguments::

        Exception: Excpetion that triggered the callback invocation.
        str: Name (address) of gateway associated with the exception. Or None.
        float: Current timestamp.

    Returns:
        MonitorSession: FGC monitor session object
    """
    return MonitorSession(callback,
                          targets,
                          period,
                          timeout=timeout,
                          callback_err=callback_err,
                          **kwargs)


def monitor_port(callback: Callable[[Dict, List, str, float], None],
                 sub_port: int, filter_address=None, filter_id=None,
                 timeout: float = None, callback_err=None, **kwargs):
    """Listen to FGC published properties (status data) at a port
    https://accwww.cern.ch/proj-fgc/proj-fgc/gendoc/def/Class63_pubdata.htm

    Callback will be invoked everytime a new UDP packet is received.
    This should be used when the gateway are sending the published data
    to a fixed IP (of the current machine), without requiring a session.

    Can be used as a context manager.
    Alternativelly, can be started with ``self.start()`` and stopped with ``self.stop()``.

    Args:
        callback (Callable): Callback that will be invoked when new data is
            received.
        sub_port (int): Port to listen for UDP data.
        filter_address (List[str], optional): only accept data from these
            addresses.
        filter_id (List[int], optional): only accept data with this ID token.
        timeout (float, optional): Timeout for subcribed data (in seconds).
        callback_err (Callable, optional): Extra callback that will be invoked
            when there was an exception during the original callback execution.
        name_file (str): Path/url to name file.
        rbac_token (bytes): CERN rbac token. Pass ``None`` to disable rbac.

    ``callback`` arguments::

        Dict: Dictionary mapping device names to dictionaries of published
            properties.
        {
            <device_name (str)>: {
                <property_name (str)>: <property_value>,
                ...
            },
            ...
        }
        List: Array of 64 iterm, mapping device positions [0..64] to dictionaries
            of published properties.
        [
            {
                <property_name (str)>: <property_value>,
                ...

            }, # Device 0
            {
                ...
            }, # Device 1
            ...
            {
                ...
            }  # Device 64
        ]
        str: Name (address) of gateway that sent current data fields.
        float: Timestamp of current data, as reported by the gateway.

    ``callback_err`` arguments::

        Exception: Excpetion that triggered the callback invocation.
        str: Name (address) of gateway associated with the exception. Or None.
        float: Current timestamp.

    Returns:
        MonitorPort: FGC monitor port object
    """
    return MonitorPort(callback,
                       sub_port,
                       filter_address=filter_address,
                       filter_id=filter_id,
                       timeout=timeout,
                       callback_err=callback_err,
                       **kwargs)


# RT channel


def connect_rt(name_file: str):
    """Open a connection to the RT channel

    The same connection object can be used for multiple gateways

    Args:
        name_file (str): Path/url to name file.

    Returns:
        FgcRt: RT channel object
    """
    rt_channel = FgcRt(name_file=name_file)
    return rt_channel

# EOF
