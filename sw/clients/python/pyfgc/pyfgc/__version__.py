__version__ = "2.0.2"

__authors__ = """
    Carlos Ghabrous Larrea,
    Nuno Laurentino Mendes,
    Joao Afonso,
    Benjamin Raymond,
    Filipe Rosa,
    Tsampikos Livisianos,
    Kevin Kessler
"""

__emails__ = """
    carlos.ghabrous@cern.ch,
    nuno.laurentino.mendes@cern.ch,
    joao.afonso@cern.ch,
    benjamin.raymond@cern.ch,
    filipe.rosa@cern.ch,
    kevin.timo.kessler@cern.ch
"""
