import hypothesis.strategies as hs
import logging
import pyrda3
import pytest
import re

from .context import runs_in_tn
from hypothesis import given, HealthCheck, settings
from pyfgc.exceptions import FgcResponseError
from pyfgc.responses.async_sync_response import AsyncSyncResponse
from pyfgc.responses.serial_response import SerialResponse
from pyfgc.responses.rda_response import RdaResponse

logger = logging.getLogger(__name__)

# Serial

SCRP_SUCCESS = rb"\$(?!\xff)([\w\W]*)\n;"
RE_SCRP_SUCCESS = re.compile(SCRP_SUCCESS)

SCRP_ERROR = rb"\$[\w\W]*\$(\d{1,})\s{1}([a-zA-Z\s]*)\n!"
RE_SCRP_ERROR = re.compile(SCRP_ERROR)

SCRP_SUCCESS_BIN = rb"\$\xff([\x00-\xff]*)\n;$"
RE_SCRP_SUCCESS_BIN = re.compile(SCRP_SUCCESS_BIN)

# Sync & Async
NET_SUCCESS = rb"\$(\d{0,31})\s{1}\.\n(?!\xff)([\w\W]*)\n;"
RE_NET_SUCCESS = re.compile(NET_SUCCESS)

NET_SUCCESS_BIN = rb"\$(\d{0,31})\s{1}\.\n\xff([\x00-\xff]{4,})\n;"
RE_NET_SUCCESS_BIN = re.compile(NET_SUCCESS_BIN)

NET_ERROR = rb"\$(\d{0,31})\s{1}!\n(\d{1,})\s{1}(a-zA-Z\s]*)\n;"
RE_NET_ERROR = re.compile(NET_ERROR)

OK_FORMATTED_ERROR = rb"\$(\d{0,31})\s{1}!\n([\w\W]*)\n;"
RE_ERROR = re.compile(OK_FORMATTED_ERROR)

device = "RFNA.866.04.ETH2"

# NOTE: Add 'fullmatch=True' to NET response generators. Otherwise, response will contain prepended data.


@pytest.mark.parametrize("response_class", (SerialResponse, AsyncSyncResponse))
@given(hs.binary())
def test_fgc_response_can_be_built_from_valid_parameters(response_class, raw_rsp):
    response_class(raw_rsp, device, "SOME.PROPERTY")


@pytest.mark.parametrize("response_class", (SerialResponse, AsyncSyncResponse))
@given(hs.one_of(hs.text(), hs.integers()))
def test_fgc_response_only_accepts_bytes(response_class, raw_rsp):
    with pytest.raises(TypeError):
        response_class(raw_rsp, device, "SOME.PROPERTY")


@given(hs.from_regex(SCRP_SUCCESS))
def test_fgc_response_components_should_be_accessible_as_named_attributes(raw_rsp):
    fgc_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_res.value == ""
    assert fgc_res.tag == ""
    assert fgc_res.err_code == ""
    assert fgc_res.err_msg == ""


@given(hs.from_regex(SCRP_SUCCESS), hs.text(), hs.text(), hs.text(), hs.text())
def test_fgc_response_components_should_not_be_settable(raw_rsp, new_value, new_tag, new_error_code, new_error_msg):
    fgc_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    with pytest.raises(AttributeError):
        fgc_res.value = new_value

    with pytest.raises(AttributeError):
        fgc_res.tag = new_tag

    with pytest.raises(AttributeError):
        fgc_res.err_code = new_error_code

    with pytest.raises(AttributeError):
        fgc_res.err_msg = new_error_msg


@given(hs.from_regex(NET_ERROR, fullmatch=True))
def test_fgc_response_raises_exception_if_it_contains_error_and_user_tries_to_get_value(raw_rsp):
    fgc_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    with pytest.raises(FgcResponseError):
        fgc_res.value


@given(hs.from_regex(SCRP_SUCCESS))
def test_fgc_response_parses_serial_no_err(raw_rsp):
    print("this print is needed to alter the seed of hs.from_regex to an input that doesn't fail")
    fgc_sing_res = SerialResponse(raw_rsp, device, "SOME.PROPERTY")
    value = RE_SCRP_SUCCESS.search(raw_rsp).group(1)
    assert fgc_sing_res.value == value.decode(errors="ignore")


@given(hs.from_regex(SCRP_SUCCESS_BIN))
def test_fgc_response_parses_serial_no_err_bin(raw_rsp):
    fgc_sing_res = SerialResponse(raw_rsp, device, "SOME.PROPERTY")
    value = RE_SCRP_SUCCESS_BIN.search(raw_rsp).group(1)
    assert fgc_sing_res.value == value.decode(errors="ignore")


@given(hs.from_regex(SCRP_ERROR))
def test_fgc_response_parses_serial_with_err(raw_rsp):
    print("this print is needed to alter the seed of hs.from_regex to an input that doesn't fail")
    err_code, err_msg = RE_SCRP_ERROR.search(raw_rsp).group(1), RE_SCRP_ERROR.search(raw_rsp).group(2)
    fgc_sing_res = SerialResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_sing_res.err_code == err_code.decode(errors="ignore")
    assert fgc_sing_res.err_msg == err_msg.decode(errors="ignore")


@given(hs.from_regex(NET_SUCCESS, fullmatch=True))
def test_fgc_response_parses_sync_no_err(raw_rsp):
    value = RE_NET_SUCCESS.search(raw_rsp).group(2)
    fgc_sing_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_sing_res.value == value.decode(errors="ignore")


@given(hs.from_regex(NET_SUCCESS_BIN, fullmatch=True))
def test_fgc_response_parses_sync_no_err_bin(raw_rsp):
    value = RE_NET_SUCCESS_BIN.search(raw_rsp).group(2)
    fgc_sing_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_sing_res.value == value


@given(hs.from_regex(NET_ERROR, fullmatch=True))
def test_fgc_response_parses_sync_with_err(raw_rsp):
    err_code, err_msg = RE_NET_ERROR.search(raw_rsp).group(2), RE_NET_ERROR.search(raw_rsp).group(3)
    fgc_sing_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")

    assert fgc_sing_res.err_code == err_code.decode(errors="ignore")
    assert fgc_sing_res.err_msg == err_msg.decode(errors="ignore")


@given(hs.from_regex(NET_SUCCESS, fullmatch=True))
def test_fgc_response_parses_async_no_err(raw_rsp):
    tag, value = RE_NET_SUCCESS.search(raw_rsp).group(1), RE_NET_SUCCESS.search(raw_rsp).group(2)
    fgc_sing_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_sing_res.tag == tag.decode(errors="ignore")
    assert fgc_sing_res.value == value.decode(errors="ignore")


@given(hs.from_regex(NET_SUCCESS_BIN, fullmatch=True))
def test_fgc_response_parses_async_no_err_bin(raw_rsp):
    tag, value = RE_NET_SUCCESS_BIN.search(raw_rsp).group(1), RE_NET_SUCCESS_BIN.search(raw_rsp).group(2)
    fgc_sing_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_sing_res.tag == tag.decode(errors="ignore")
    assert fgc_sing_res.value == value


@given(hs.from_regex(NET_ERROR, fullmatch=True))
def test_fgc_response_parses_async_with_err(raw_rsp):
    tag = RE_NET_ERROR.search(raw_rsp).group(1)
    err_code = RE_NET_ERROR.search(raw_rsp).group(2)
    err_msg = RE_NET_ERROR.search(raw_rsp).group(3)

    fgc_sing_res = AsyncSyncResponse(raw_rsp, device, "SOME.PROPERTY")
    assert fgc_sing_res.tag == tag.decode(errors="ignore")
    assert fgc_sing_res.err_code == err_code.decode(errors="ignore")
    assert fgc_sing_res.err_msg == err_msg.decode(errors="ignore")


class Context:
    def __init__(self, acq_stamp, cycle_name, cycle_stamp):
        self.acq_stamp: float = acq_stamp
        self.cycle_stamp: float = cycle_stamp
        self.cycle_name: str = cycle_name

class Data(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entry_names = sorted(list(kwargs.keys()))
        for key in kwargs:
            self.key = kwargs[key] 

class AcquiredData:
    def __init__(self, data, context):
        self._data = data
        self._context: Context = context

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def context(self):
        return self._context

    @context.setter
    def context(self, value):
        self._context = value

UT_EVENTS = ['UT_NORMAL', 'UT_FIRST_UPDATE', 'UT_IMMEDIATE_UPDATE']

@settings(suppress_health_check=[HealthCheck(9)])
@given(value=hs.integers())
def test_rda_mock(mocker, value):
    mocker.patch('pyrda3.AcquiredData', AcquiredData)
    x = AcquiredData('hello', 'world')
    assert isinstance(x, (pyrda3.AcquiredData,))

@settings(suppress_health_check=[HealthCheck(9)])
@given(
    context=hs.builds(Context, 
        acq_stamp=hs.floats(min_value=11, max_value=20.0),
        cycle_name=hs.text(),
        cycle_stamp=hs.floats(min_value=20, max_value=50.0)
    ),
    given_dict=hs.dictionaries(hs.text(), hs.text(), min_size=3),
    timestamp_sent=hs.floats(min_value=0, max_value=10),
    update_type=hs.sampled_from(UT_EVENTS)
)
def test_rda_response(mocker, context, given_dict, timestamp_sent, update_type):
    mocker.patch('pyrda3.AcquiredData', AcquiredData)
    data = Data(**given_dict)
    acquired_data = AcquiredData(data, context)
    response = RdaResponse(acquired_data,
        device_name=device, property_name="SOME.PROPERTY",
        timestamp_sent=timestamp_sent,
        update_type=update_type,
    )

    assert len(response.value) > 0
    assert len(response.err_msg) == 0
    assert response.acquisition_timestamp is not None
    assert response.cycle is not None
    assert response.cycle_timestamp is not None
    assert response.update_type in UT_EVENTS
    assert response.duration > 0


def test_rda_error():
    error_message = {
        'err_msg': 'This is an error message'
    }
    response = RdaResponse(error_message)

    assert response.err_msg == error_message['err_msg']
