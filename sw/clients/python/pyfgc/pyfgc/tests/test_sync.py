import logging
import pyfgc_name
import pytest
import struct
import threading

from .context import runs_in_tn
from pyfgc import api, exceptions

logger = logging.getLogger(__name__)

if not runs_in_tn():
    pytest.skip("Not running in the TN", allow_module_level=True)

gateway_1 = "CFC-866-RETH2"
device_1 = "RFNA.866.04.ETH2"

pyfgc_name.read_name_file()


def test_sync_get_single():
    """
    Gets property from device.

    """
    prop = "DEVICE.NAME"

    r = api.get(device_1, prop)
    try:
        assert r.value == device_1
    except exceptions.FgcResponseError:
        # Just in case the device was not operational
        logger.error(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")


def test_sync_get_single_option():
    """
    Gets a property from a single device, specifying an option.

    """

    prop = "LIMITS.I.EARTH"

    r = api.get(device_1, prop, get_option="HEX")
    try:
        int(r.value, 16)
    except ValueError:
        logger.error(f"Property {prop} from device {device_1} is not a valid hex value: {r.value}!")
        assert False
    except exceptions.FgcResponseError:
        logger.error(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")


def test_sync_get_single_binary():
    """
    Gets a property in binary format.
    """
    r = api.get(device_1, "LOG.PM.SPY.IEARTH", get_option="bin")
    try:
        length_header_decoded = struct.unpack("!L", r.value[0:4])[0]
        actual_length = len(r.value[4:])
        assert len(length_header_decoded) == actual_length
    except exceptions.FgcResponseError:
        logger.error(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")


def test_sync_get_single_parent():
    """
    Gets a parent property from a single device.
    """

    prop = "BARCODE"
    r = api.get(device_1, prop)
    try:
        assert isinstance(r.value, str)
    except exceptions.FgcResponseError:
        logger.error(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")


def test_sync_set_single():
    """
    Sends a set command to a single device.

    Runs a get command to verify that the set command took place.
    """

    prop = "LIMITS.I.EARTH"
    value_one = 1e-2
    value_two = 2e-2

    with api.fgc(device_1) as fgc:
        fgc.set(prop, value_one)
        r_one = fgc.get(prop)

        fgc.set(prop, value_two)
        r_two = fgc.get(prop)

        try:
            assert pytest.approx(float(r_one.value)) == value_one
            assert pytest.approx(float(r_two.value)) == value_two
        except exceptions.FgcResponseError:
            logger.error(f"Error, device {device_1}, err_code {r_one.err_code}, err_msg {r_one.err_msg}")
            logger.error(f"Error, device {device_1}, err_code {r_two.err_code}, err_msg {r_two.err_msg}")


def test_two_threads():
    def get_device_name(device_name, results):
        r = api.get(device_name, "DEVICE.NAME")
        results[device_name] = r.value

    thread_list = list()
    results = dict()

    devices_names = pyfgc_name.build_device_set(".*866.*ETH2")
    for dev in devices_names:
        t = threading.Thread(target=get_device_name, args=(dev, results))
        thread_list.append(t)
        t.start()

    for t in thread_list:
        t.join()

    for k, v in results.items():
        assert k == v
