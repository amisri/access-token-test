import functools
import logging
import multiprocessing.pool
import multiprocessing.context
import pytest
import pyfgc
import random
import struct

from pyfgc.constants import ProtocolTypes

logger = logging.getLogger(__name__)

def timeout(max_timeout):
    """Timeout decorator, parameter in seconds."""
    def timeout_decorator(item):
        """Wrap the original function."""
        @functools.wraps(item)
        def func_wrapper(*args, **kwargs):
            """Closure for function."""
            pool = multiprocessing.pool.ThreadPool(processes=1)
            async_result = pool.apply_async(item, args, kwargs)
            # raises a TimeoutError if execution exceeds max_timeout
            try:
                return async_result.get(max_timeout)
            except multiprocessing.context.TimeoutError:
                pass
        return func_wrapper
    return timeout_decorator

try:
    import serial.tools.list_ports
except ImportError:
    pytest.skip("pyserial not installed, skipping serial tests...", allow_module_level=True)

@timeout(2)
def ping_port(port_path):
    try:
        r = pyfgc.get(port_path, "device", protocol=ProtocolTypes.SERIAL.value)
        if r.value:
            return True
    except Exception:
        logging.warning(f"Failed to connect to {port_path}")
        return False
	
def find_fgc_port():
    fgc_ports = [p for p in serial.tools.list_ports.comports() if "FGC" in p.hwid]
    for p in fgc_ports:
        if ping_port(p.device):
            return p.device

SERIAL_PORT = find_fgc_port()
if SERIAL_PORT is None:
    pytest.skip("No FGC found, skipping serial tests...", allow_module_level=True)


def test_serial_connect_disconnect():
	with pyfgc.fgc(SERIAL_PORT, protocol=ProtocolTypes.SERIAL.value):
		logger.info("Connected to FGC using context manager")

def test_serial_get():
	with pyfgc.fgc(SERIAL_PORT, protocol=ProtocolTypes.SERIAL.value) as fgc_session:
		r = fgc_session.get("DEVICE.CLASS_ID")

	assert 0 <= int(r.value) <= 255


def test_serial_get_array():
	limits_i_rate = pyfgc.get(SERIAL_PORT, "LIMITS.I.RATE", protocol=ProtocolTypes.SERIAL.value)
	assert len(limits_i_rate.value.split(",")) != 0


def test_serial_set():
    test_str = f"HELLO{random.randint(0, 100)}"
    pyfgc.set(SERIAL_PORT, 'TEST.CHAR', test_str, protocol=ProtocolTypes.SERIAL.value)    
    r = pyfgc.get(SERIAL_PORT, 'TEST.CHAR', protocol=ProtocolTypes.SERIAL.value)
    assert r.value == test_str
