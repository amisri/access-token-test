import asyncio
import logging
import os
import pytest
import time
import struct


import pyfgc_name
import pyfgc_rbac
from .context import runs_in_tn
from pyfgc import api, exceptions
from pyfgc.channels.async_channel import AsyncChannel
from pyfgc.responses.fgc_response import FgcResponse, FgcResponseError
from pyfgc.responses.async_sync_response import AsyncSyncResponse

logger = logging.getLogger(__name__)


gateway_1 = "CFC-866-RETH2"
device_1 = "RFNA.866.01.ETH2"
port = 1905

if not runs_in_tn():
    pytest.skip("Not running in the TN", allow_module_level=True)

pyfgc_name.read_name_file()

@pytest.fixture(scope='session')
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()

# Test protocol object
def truncate_float(value, precision):
    string_def = "{{:.{}f}}".format(precision)
    return float(string_def.format(value))


@pytest.fixture
def send_rbac():
    async def _send_rbac(protocol):
        token = pyfgc_rbac.get_token_kerberos()
        response_fut = asyncio.ensure_future(protocol.set("CLIENT.TOKEN", token))
        raw = await asyncio.wait_for(response_fut, 1.0)
        response = AsyncSyncResponse(raw, device_1, "CLIENT.TOKEN")
        response.value
    return _send_rbac


@pytest.mark.asyncio
async def test_protocol_connection(event_loop):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    assert isinstance(protocol, AsyncChannel)


@pytest.mark.asyncio
async def test_protocol_set(event_loop):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    response_fut = asyncio.ensure_future(protocol.set("TEST.FLOAT", 123.456, device=device_1))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST.FLOAT")
    assert isinstance(response, FgcResponse)


@pytest.mark.asyncio
async def test_protocol_get(event_loop):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    response_fut = asyncio.ensure_future(protocol.get("TEST.FLOAT", device=device_1))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST_FLOAT")
    assert isinstance(response, FgcResponse)


@pytest.mark.asyncio
async def test_protocol_set_value(event_loop, send_rbac):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    response_fut = asyncio.ensure_future(protocol.set("TEST.FLOAT", 111, device=device_1))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST_FLOAT")
    response.value


@pytest.mark.asyncio
async def test_protocol_set_value_error(event_loop, send_rbac):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    response_fut = asyncio.ensure_future(protocol.set("TEST.FLOAT", "will fail", device=device_1))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST_FLOAT")
    with pytest.raises(FgcResponseError, match=r'bad float'):  # No rbac token was set
        response.value


@pytest.mark.asyncio
async def test_protocol_set_get_value(event_loop, send_rbac):
    float_value = 777.777
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    # Set value
    response_fut = asyncio.ensure_future(protocol.set("TEST.FLOAT", float_value, device=device_1))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST_FLOAT")
    # Get value and confirm result
    response_fut = asyncio.ensure_future(protocol.get("TEST.FLOAT", device=device_1))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST_FLOAT")
    assert pytest.approx(float(response.value)) == float_value


property_list = [
    ("TEST.FLOAT", 0, 777.777),  # bytes [0:3]   - pos 0/4 = 0
    ("TEST.INT8S", 4, -16),  # bytes [4]     - pos 4/1 = 4
    ("TEST.INT8U", 5, 61),  # bytes [5]     - pos 5/1 = 5
    ("TEST.INT16S", 3, -1221),  # bytes [6:7]   - pos 6/2 = 3
    ("TEST.INT16U", 4, 2112),  # bytes [8:9]   - pos 8/2 = 4
    ("TEST.INT32S", 3, -123454321),  # bytes [12:15] - pos 12/4 = 3
    ("TEST.INT32U", 4, 543212345),  # bytes [16:19] - pos 16/4 = 4
]


@pytest.mark.asyncio
async def test_protocol_multiple_set_value(event_loop, send_rbac):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    futures = set()
    for prop, idx, val in property_list:
        new_future = asyncio.ensure_future(protocol.set(prop + f"[{idx}]", val, device=device_1))
        futures.add(new_future)
    done, not_done = await asyncio.wait(futures, timeout=1.0)
    assert not not_done
    for fut_done in done:
        raw = fut_done.result()
        response = AsyncSyncResponse(raw, device_1, "SOME.PROPERTY")
        response.value


@pytest.mark.asyncio
async def test_protocol_multiple_set_get_value(event_loop, send_rbac):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    # Set values
    futures = set()
    for prop, idx, val in property_list:
        new_future = asyncio.ensure_future(protocol.set(prop + f"[{idx}]", val, device=device_1))
        futures.add(new_future)
    done, not_done = await asyncio.wait(futures, timeout=1.0)
    assert not not_done
    for fut_done in done:
        raw = fut_done.result()
        response = AsyncSyncResponse(raw, device_1, "SOME.PROPERTY")
        response.value
    # Get values and confirm result
    futures = dict()
    for prop, idx, val in property_list:
        new_future = asyncio.ensure_future(protocol.get(prop + f"[{idx}]", device=device_1))
        futures[new_future] = val
    done, not_done = await asyncio.wait(futures.keys(), timeout=1.0)
    assert not not_done
    for fut_done in done:
        raw = fut_done.result()
        response = AsyncSyncResponse(raw, device_1, "SOME.PROPERTY")
        assert pytest.approx(futures[fut_done]) == float(response.value)


binary_data_streams = [
    b"1234567890",
    b"abcdefghij",
    b"0000000000",
    b"a1b2c3d4e5",
    b"abcdef\n;",
    b"$$$$$$$$",
    b";;;;;;;;",
    b"\n\n\n\n",
    b"\n;\n;\n;\n;",
    b"$ .\n\n;",
    b"$ !\n\n;",
    b"$ .\n\n;$ .\n\n;$ .\n\n;$ .\n\n;",
    b"\x00\x00\x00\x00",
    b"\xff\xff\xff\xff",
    os.urandom(1),
    os.urandom(64),
]


@pytest.mark.parametrize("input_data", binary_data_streams)
@pytest.mark.asyncio
async def test_protocol_set_get_binary(event_loop, send_rbac, input_data):
    str_value = input_data
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    # Set value
    response_fut = asyncio.ensure_future(protocol.set("TEST.BIN", str_value))
    raw = await asyncio.wait_for(response_fut, 1.0)
    # Get value and confirm result
    response_fut = asyncio.ensure_future(protocol.get("TEST.BIN BIN"))
    raw = await asyncio.wait_for(response_fut, 1.0)
    response = AsyncSyncResponse(raw, device_1, "TEST.BIN BIN")
    bin_value = response.value
    length = struct.unpack('>I', bin_value[:4])[0]
    assert length == len(str_value)
    assert length + 4 == len(bin_value)


log_properties = [
    "LOG.EVT",
    "LOG.SPY.DATA[0]",
    "LOG.SPY.DATA[1]",
    # FIXME: Device responds '42 not impl' error for index 2 and 3
    # "LOG.SPY.DATA[2]",
    # "LOG.SPY.DATA[3]",
]


@pytest.mark.parametrize("input_prop", log_properties)
@pytest.mark.asyncio
async def test_protocol_get_long_binary(event_loop, send_rbac, input_prop):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    await send_rbac(protocol)
    # BIN DATA|{time_origin_s},{time_offset_ms},{time_duration_ms},0
    get_opt = f"BIN DATA|{int(time.time())},0,2000,2000,0,0"
    response_fut = asyncio.ensure_future(protocol.get(input_prop, get_option=get_opt, device=device_1))
    raw = await asyncio.wait_for(response_fut, 2.0)
    response = AsyncSyncResponse(raw, device_1, input_prop)
    bin_value = response.value
    length = struct.unpack('>I', bin_value[:4])[0]
    assert length + 4 == len(bin_value)


@pytest.mark.asyncio
async def test_protocol_stream(event_loop, send_rbac):
    _, protocol = await event_loop.create_connection(lambda: AsyncChannel(), gateway_1, port)
    stream_reader = protocol.get_stream_reader()
    await send_rbac(protocol)
    # Check before opening raw connection - streamreader should stay empty
    await asyncio.sleep(0.1)
    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(stream_reader.read(4), 0.5)
    # Check after opening raw connection - streamreader should contain bytes
    await protocol.enable_rterm_mode(4)
    raw_data = await asyncio.wait_for(stream_reader.read(4), 0.5)
    assert len(raw_data)


# Test top level API
@pytest.mark.asyncio
async def test_async_get_single(event_loop):
    """Gets property from device."""
    prop = "DEVICE.NAME"
    r = await api.async_get(device_1, prop)
    try:
        assert r.value == device_1
    except exceptions.FgcResponseError:
        # Just in case the device was not operational
        print(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")


@pytest.mark.asyncio
async def test_async_get_single_option(event_loop):
    """Gets a property from a single device, specifying an option."""
    prop = "LIMITS.I.EARTH"
    r = await api.async_get(device_1, prop, get_option="HEX")
    try:
        int(r.value, 16)
    except ValueError:
        print(f"Property {prop} from device {device_1} is not a valid hex value: {r.value}!")
        raise
    except exceptions.FgcResponseError:
        print(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")
        raise


@pytest.mark.asyncio
async def test_async_get_single_binary(event_loop):
    """Gets a property in binary format."""
    r = await api.async_set(gateway_1, "TEST.BIN", b"binary_data")
    r.value
    r = await api.async_get(gateway_1, "TEST.BIN", get_option="bin")
    try:
        length_header_decoded = struct.unpack("!L", r.value[0:4])[0]
        actual_length = len(r.value[4:])
        assert length_header_decoded == actual_length
    except exceptions.FgcResponseError:
        print(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")
        raise


@pytest.mark.asyncio
async def test_async_get_single_parent(event_loop):
    """Gets a parent property from a single device."""
    prop = "BARCODE"
    r = await api.async_get(device_1, prop)
    try:
        assert isinstance(r.value, str)
    except exceptions.FgcResponseError:
        print(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")
        raise


@pytest.mark.asyncio
async def test_async_set_single(event_loop):
    """Sends a set command to a single device.
    Runs a get command to verify that the set command took place.
    """
    prop = "LIMITS.I.EARTH"
    value = 1e-2
    async with api.async_fgc(device_1) as fgc:
        r = await fgc.set(prop, value)
        r = await fgc.get(prop)
        try:
            assert pytest.approx(float(r.value)) == value
        except exceptions.FgcResponseError:
            print(f"Error, device {device_1}, err_code {r.err_code}, err_msg {r.err_msg}")
            raise


@pytest.mark.asyncio
async def test_multiple_devices(event_loop):
    """Tests sending a command to multiple FGCs in parallel"""
    device_names = pyfgc_name.build_device_set(".*866.*ETH2")
    fut_results = dict()
    for dev in device_names:
        fut_results[dev] = asyncio.ensure_future(api.async_get(dev, "DEVICE.NAME"))
    await asyncio.wait(fut_results.values(), timeout=1.0)
    for k, v in fut_results.items():
        rsp = v.result()
        try:
            assert k == rsp.value
        except FgcResponseError:
            # This is also a valid response for this test
            with pytest.raises(FgcResponseError, match=r'dev not ready'):
                rsp.value


@pytest.mark.asyncio
async def test_multiple_devices_plus(event_loop):
    """Tests sending a command to multiple FGCs in parallel (more commands!)"""
    device_names = pyfgc_name.build_device_set(".*866.*ETH2")
    # Set multiple values for many devices
    fut_results = list()
    for dev in device_names:
        for prop, idx, val in property_list:
            fut = asyncio.ensure_future(api.async_set(dev, prop + f"[{idx}]", val))
            fut_results.append(fut)
    done, not_done = await asyncio.wait(fut_results, timeout=10.0)
    assert not not_done
    # Get multiple values from many devices
    fut_results = list()
    for dev in device_names:
        for prop, idx, val in property_list:
            fut = asyncio.ensure_future(api.async_get(dev, prop + f"[{idx}]"))
            fut_results.append((fut, val))
    done, not_done = await asyncio.wait({fut for fut, _ in fut_results}, timeout=10.0)
    assert not not_done
    for fut, val in fut_results:
        rsp = fut.result()
        try:
            assert pytest.approx(val) == float(rsp.value)
        except FgcResponseError:
            # This is also a valid response for this test
            with pytest.raises(FgcResponseError, match=r'dev not ready'):
                rsp.value


@pytest.mark.asyncio
async def test_socket_failure_before_coroutine(event_loop):
    async with api.async_fgc(device_1) as fgc:
        fgc._protocol_object.channel.transport.close()
        with pytest.raises(exceptions.ChannelError):
            await fgc.get("TEST.FLOAT")


@pytest.mark.asyncio
async def test_socket_failure_middle_coroutine(monkeypatch):
    async with api.async_fgc(device_1) as fgc:
        prev_send = fgc._protocol_object.channel.send

        async def send_and_close(data):
            await prev_send(data)
            fgc._protocol_object.channel.transport.close()
            # fgc._protocol_object.channel.disconnect()

        monkeypatch.setattr(fgc._protocol_object.channel, "send", send_and_close)
        fut_coroutine = asyncio.create_task(fgc.get("TEST.FLOAT"))
        with pytest.raises(asyncio.CancelledError):
            await asyncio.wait_for(fut_coroutine, 5.0)
# EOF
