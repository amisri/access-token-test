import os
from socket import gethostname
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


TN_MACHINES = {
    "cs-ccr-dev1.cern.ch",
    "cs-ccr-dev2.cern.ch",
    "cs-ccr-dev3.cern.ch",
    "cwe-513-vpl165.cern.ch",  # kekessle-dev
    "cwe-513-vol984.cern.ch" # nulauren-dev
}


def runs_in_tn():
    # Used to skip tests if we are not in a dev machine in the TN (gitlab)
    return gethostname() in TN_MACHINES
