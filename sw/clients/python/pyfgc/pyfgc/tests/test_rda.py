from .context import runs_in_tn
from pyfgc import exceptions
from pyfgc.constants import ProtocolTypes
from pyfgc.responses.fgc_response import FgcResponse
from pyfgc.responses.rda_response import RdaResponse
from pyfgc.fgc_session import FgcSession

import logging
import pyfgc_rbac
import pyrda3
import pyfgc
import pytest
import random
import string
import time

logger = logging.getLogger(__name__)

if not runs_in_tn():
    pytest.skip("Not running in the TN", allow_module_level=True)


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length)).upper()


device = "RFNA.866.04.ETH2"
device2 = "RFNA.866.05.ETH2"
prop = "TEST.CHAR"

token = pyfgc_rbac.get_token_kerberos()


def test_pyrda_get():
    with pyrda3.ClientServiceBuilder.new_instance().build() as client:
        client.set_authentication_token(token)
        get_data = client.get_access_point(device, prop).get()
        assert get_data.data

        data: pyrda3.Data = get_data.data

        # To understand what comes inside a RDA response
        logger.info(f"Dictionary Keys: {data.entry_names}")
        logger.info(f"Dictionary Size: {data.all_entries_size}")
        keys = list(data.entry_names)
        keys.sort(reverse=True)
        raw_rsp = "".join([f'{key}:{data[key]}\n' for key in keys])

        logger.info(f"Value for each one of the keys received: {raw_rsp}")

        fgc_response: FgcResponse = pyfgc.get(device, prop, rbac_token=token)
        sync_data = fgc_response.value
        
        # Filipe made this just to visualize the difference between the two responses
        logger.info(f'{type(sync_data)} sync_data =\n{sync_data}')


def test_set_get_pyrda():
    with pyrda3.ClientServiceBuilder.new_instance().build() as client:
        client.set_authentication_token(token)

        set_data = pyrda3.DataFactory.create_data()
        set_data["value"] = randomword(10)

        client.get_access_point(device, prop).set(set_data)
        get_data = client.get_access_point(device, prop).get()

        assert set_data["value"] == get_data.data["value"]


def test_create_fgc_response_from_pyrda():
    with pyrda3.ClientServiceBuilder.new_instance().build() as client:
        client.set_authentication_token(token)

        set_data = pyrda3.DataFactory.create_data()
        set_data["value"] = randomword(20)

        client.get_access_point(device, prop).set(set_data)

        rda_rsp = None
        try:
            rda_rsp = client.get_access_point(device, prop).get()

        except pyrda3.RdaException as error:
            logger.error(error)
            rda_rsp = {'err_msg': error}

        fgc_response = RdaResponse(rda_rsp, device, prop)

        assert fgc_response.err_code == ''
        assert fgc_response.err_msg == ''

        assert set_data["value"] == fgc_response.value


# NOTE: RDA responses for non-leaf properties might come in a different order
@pytest.mark.parametrize("prop_name", ['TEST.FLOAT', 'TEST.CHAR', 'LOG.MENU.STATUS'])
def test_rda_vs_sync(prop_name):
    device = "CFC-866-RETH2"

    rda_fgc_response = pyfgc.get(device, prop_name, protocol=ProtocolTypes.RDA.value, rbac_token=token)
    logger.info(f'rda received:\n{rda_fgc_response.value}\n')

    sync_fgc_response = pyfgc.get(device, prop_name, rbac_token=token)
    logger.info(f'\nsync received:\n{sync_fgc_response.value}\n')

    assert rda_fgc_response.value == sync_fgc_response.value


def test_response_error():
    with pyrda3.ClientServiceBuilder.new_instance().build() as client:
        client.set_authentication_token(token)
        with pytest.raises(pyrda3.RdaException):
            client.get_access_point(device, 'sfdjash').get()


def test_pyfgc_rda_get():
    res1 = pyfgc.get(device, "TEST.INT32S[11] INFO", protocol=ProtocolTypes.RDA.value, rbac_token=token)
    logger.info("RDA response:\n {res1.value}")
    res2 = pyfgc.get(device, "TEST.INT32S[11] INFO", protocol=ProtocolTypes.SYNC.value, rbac_token=token)
    logger.info("TCP response:\n {res2.value}")
    assert len(res1.value) != len(res2.value)


def test_pyfgc_rda_set():
    set_val = randomword(10)
    pyfgc.set(device, prop, set_val, protocol=ProtocolTypes.RDA.value, rbac_token=token)

    res = pyfgc.get(device, prop, protocol=ProtocolTypes.RDA.value, rbac_token=token)
    assert res.value == set_val
    res = pyfgc.get(device, prop, protocol=ProtocolTypes.SYNC.value, rbac_token=token)
    assert res.value == set_val


class NotificationListener(pyrda3.NotificationListener):
    def __init__(self):
        self.data = []
        pyrda3.NotificationListener.__init__(self)

    def data_received(self, subscription, value, update_type):
        data = value.data
        self.data.append(data)

    def error_received(self, subscription, exception, update_type):
        logger.error("update type: " + str(update_type))
        logger.error(exception)



def test_rda_subscribe():
    listener = NotificationListener()

    prop_2 = "MEAS.V.REF"

    with pyrda3.ClientServiceBuilder.new_instance().build() as client:
        client.set_authentication_token(token)

        ap = client.get_access_point(device, prop_2)
        sub = ap.subscribe(listener=listener)

        timeout = time.time() + 10
        while sub.state != pyrda3.State.SS_SUBSCRIBED and time.time() < timeout:
            logger.info(sub.state)
            time.sleep(0.5)
        logger.info(sub.state)

        count = 0
        while sub.state == pyrda3.State.SS_SUBSCRIBED and count < 5:
            time.sleep(1)
            count += 1

        ap.unsubscribe(sub)

        assert len(listener.data) > 1


def test_multiple_subscribers():
    prop_2 = "MEAS.V.REF"
    with pyrda3.ClientServiceBuilder.new_instance().build() as client:
        client.set_authentication_token(token)

        listener_1 = NotificationListener()
        ap_1 = client.get_access_point(device, prop_2)
        sub_1 = ap_1.subscribe(listener=listener_1)

        listener_2 = NotificationListener()
        ap_2 = client.get_access_point(device2, prop_2)
        sub_2 = ap_2.subscribe(listener=listener_2)

        time.sleep(2)

        ap_1.unsubscribe(sub_1)
        ap_2.unsubscribe(sub_2)

        assert len(listener_1.data) > 1
        assert len(listener_2.data) > 1


def test_subscription():
    responses = list()

    def on_receive_callback(fgc_response):
        responses.append(fgc_response)

    sub_session = FgcSession(device, protocol=ProtocolTypes.RDA.value, rbac_token=token)
    sub_session.subscribe("MEAS.V.REF", on_receive_callback)

    time.sleep(2)

    assert len(responses) > 1
    for response in responses:
        assert response.property_name == "MEAS.V.REF"

    sub_session.disconnect()


@pytest.mark.parametrize("property_names", [["MEAS.V.VALUE"], ["MEAS.V.REF", "MEAS.I.REF"]])
def test_subscription_context(property_names):
    responses_per_prop = dict()
    for property_name in property_names:
        responses_per_prop[property_name] = list()

    def on_receive_callback(fgc_response):
        assert fgc_response.property_name in property_names
        responses_per_prop[fgc_response.property_name].append(fgc_response)

    with pyfgc.subscribe(device, property_names, on_receive_callback, rbac_token=token):
        time.sleep(2)

    for responses in responses_per_prop.values():
        assert len(responses) > 1


def test_create_manual_subscription_session():
    responses = list()

    def on_receive_callback(fgc_response):
        responses.append(fgc_response)

    sub_session = pyfgc.create_manual_subscription_session(device, "MEAS.V.REF", on_receive_callback, rbac_token=token)
    time.sleep(2)
    sub_session.disconnect()

    assert len(responses) > 1


def test_set_test_char_overflow():
    value = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F," \
            "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F," \
            "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F," \
            "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,"

    with pytest.raises(pyfgc.FgcResponseError):
        pyfgc.set(device, "TEST.CHAR", value, protocol=ProtocolTypes.RDA.value, rbac_token=token).value

def test_cycle_selector():
    cycle = 'PSB.USER.AD'
    responses = list()
    
    def on_receive_callback(fgc_response):
        responses.append(fgc_response)

    sub_session = pyfgc.create_manual_subscription_session(device, "MEAS.PULSE", on_receive_callback, cycle_selector=cycle, rbac_token=token)
    time.sleep(2)
    sub_session.disconnect()

    assert responses and all([cycle == r.cycle for r in responses])

def test_response_update():
    update_types = ['UpdateType.UT_NORMAL', 'UpdateType.UT_FIRST_UPDATE', 'UpdateType.UT_IMMEDIATE_UPDATE']

    responses = list()
    def on_receive_callback(fgc_response):
        logger.debug(fgc_response.update_type)
        responses.append(fgc_response)

    sub_session = pyfgc.create_manual_subscription_session(device, "MEAS.PULSE", on_receive_callback, rbac_token=token)
    time.sleep(2)
    sub_session.disconnect()

    assert responses and any([str(r.update_type) in update_types for r in responses])
