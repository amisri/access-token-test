import logging
import time
import pytest
from datetime import datetime


from .context import runs_in_tn
from pyfgc.constants import ProtocolTypes
from pyfgc.fgc_session import FgcSession

logger = logging.getLogger(__name__)

if not runs_in_tn():
    pytest.skip("Not running in the TN", allow_module_level=True)

TARGET_1 = "RFNA.866.04.ETH2"


def test_session_rbac_internal_set():
    session = FgcSession(TARGET_1, ProtocolTypes.SYNC.value)
    assert session._rbac_token is not None
    assert session._rbac_is_external is False
    assert session._rbac_expiration_time > datetime.now()
    session.disconnect()


def test_session_rbac_external_set():
    session = FgcSession(TARGET_1, ProtocolTypes.SYNC.value)
    first_rbac_expiration = session._rbac_expiration_time

    time.sleep(1)

    other_session = FgcSession(TARGET_1, ProtocolTypes.RDA.value)
    session.set_external_rbac_token(other_session._rbac_token)

    assert session._rbac_token is not None
    assert session._rbac_expiration_time > first_rbac_expiration
    assert session._rbac_is_external is True

    session.disconnect()
    other_session.disconnect()


def test_session_rbac_internal_renewal():
    session = FgcSession(TARGET_1, ProtocolTypes.SYNC.value)
    first_rbac_expiration = session._rbac_expiration_time

    time.sleep(1)

    session.renew_rbac_token(force=True)
    assert session._rbac_expiration_time > first_rbac_expiration
    session.disconnect()


def test_session_rbac_external_renewal():
    session = FgcSession(TARGET_1, ProtocolTypes.SYNC.value)
    first_rbac_expiration = session._rbac_expiration_time
    external_token = session._rbac_token

    time.sleep(1)

    session.set_external_rbac_token(external_token)
    session.renew_rbac_token(force=True)
    assert session._rbac_expiration_time > first_rbac_expiration
    session.disconnect()
