import asyncio
import logging
import pytest
import random

from .context import runs_in_tn
from pyfgc import api, exceptions
from pyfgc.constants import ProtocolTypes
from pyfgc.responses.fgc_response import FgcResponse
from pyfgc.fgc_monitor import MonitorSession, MonitorPort
from pyfgc.fgc_session import FgcAsyncSession, FgcSession

logger = logging.getLogger(__name__)

if not runs_in_tn():
    pytest.skip("Not running in the TN", allow_module_level=True)

TARGET_1 = "RFNA.866.04.ETH2"


@pytest.fixture(scope='session')
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


def test_sync_pyfgc_context_manager_returns_session_default_protocol():
    with api.fgc(TARGET_1) as device:
        assert isinstance(device, FgcSession)


def test_unsupported_protocol_raises_exception():
    with pytest.raises(exceptions.PyFgcError):
        with api.fgc(TARGET_1, 'unknown_protocol') as device:
            assert isinstance(device, FgcSession)


@pytest.mark.asyncio
async def test_async_pyfgc_context_manager_returns_session_default_protocol():
    async with api.async_fgc(TARGET_1) as device:
        assert isinstance(device, FgcAsyncSession)


def test_sync_pyfgc_context_manager_returns_session_specific_protocol():
    with api.fgc(TARGET_1, protocol=ProtocolTypes.SYNC.value) as device:
        assert isinstance(device, FgcSession)


def test_unknown_device_raises_exception():
    with pytest.raises(exceptions.PyFgcSessionError):
        api.connect('fdssfddsfj')


def test_case_sensitivity():
    # conver target name to random case sensitivity
    new_name = ''.join(random.choice([str.upper, str.lower])(c) for c in TARGET_1)
    fgc_res = api.get(new_name, "non_existing_prop")
    assert isinstance(fgc_res, FgcResponse)


def test_sync_pyfgc_connect_returns_session_default_protocol():
    try:
        device = api.connect(TARGET_1)
        assert isinstance(device, FgcSession)
        device.disconnect()
    except exceptions.PyFgcError:
        logger.error(f"Error, device {device} not found")
        raise


@pytest.mark.asyncio
async def test_async_pyfgc_connect_returns_session_default_protocol():
    device = await api.async_connect(TARGET_1)
    assert isinstance(device, FgcAsyncSession)


def test_sync_pyfgc_connect_returns_session_specific_protocol():
    device = api.connect(TARGET_1, protocol=ProtocolTypes.SYNC.value)
    assert isinstance(device, FgcSession)


def test_sync_pyfgc_connect_throws_correct_exception_type():
    with pytest.raises(exceptions.PyFgcError):
        _ = api.connect(TARGET_1, protocol=ProtocolTypes.SERIAL.value)


def test_sync_pyfgc_get_returns_response_default_protocol():
    fgc_res = api.get(TARGET_1, "non_existing_prop")
    assert isinstance(fgc_res, FgcResponse)


@pytest.mark.asyncio
async def test_async_pyfgc_get_returns_response_default_protocol():
    fgc_res = await api.async_get(TARGET_1, "non_existing_prop")
    assert isinstance(fgc_res, FgcResponse)


def test_sync_pyfgc_get_returns_response_specific_protocol():
    fgc_res = api.get(TARGET_1, "property", protocol=ProtocolTypes.SYNC.value)
    assert isinstance(fgc_res, FgcResponse)


def test_sync_pyfgc_set_returns_response():
    fgc_res = api.set(TARGET_1, "property", "value")
    assert isinstance(fgc_res, FgcResponse)


@pytest.mark.asyncio
async def test_async_pyfgc_set_returns_response():
    fgc_res = await api.async_set(TARGET_1, "property", "value")
    assert isinstance(fgc_res, FgcResponse)


def test_pyfgc_monitor_returns_monitor_session_object():
    def my_callback(*args, **kwargs):
        pass

    fgc_mon = api.monitor_session(my_callback, TARGET_1, 50)
    assert isinstance(fgc_mon, MonitorSession)


def test_pyfgc_monitor_returns_monitor_port_nobject():
    def my_callback(*args, **kwargs):
        pass

    fgc_mon = api.monitor_port(my_callback, 12345)
    assert isinstance(fgc_mon, MonitorPort)
