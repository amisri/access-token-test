import asyncio
import logging
import pyfgc_name
import pytest
import threading

from .context import runs_in_tn
from pyfgc.channels.async_channel import AsyncChannel
from pyfgc.channels.rda_channel import RdaChannel
from pyfgc.channels.sync_channel import SyncChannel
from pyfgc.channels.channel_manager import ChannelManager, AsyncChannelManager

logger = logging.getLogger(__name__)

if not runs_in_tn():
    pytest.skip("Not running in the TN", allow_module_level=True)

pyfgc_name.read_name_file()

FGC_A = "RPAGM.866.21.ETH1"
FGC_B = "RPZES.866.15.ETH1"
RETH1 = "cfc-866-reth1"

FGC_C = "RFNA.866.04.ETH2"
FGC_D = "RFNA.866.05.ETH2"
RETH2 = "cfc-866-reth2"

# Execute the tests in turns with the following channel types
SYNC_CHANNEL_CLASSES = [RdaChannel, SyncChannel]
ASYNC_CHANNEL_CLASSES = [AsyncChannel]

RBAC_NAME = None
RBAC_NAME_2 = "User"
TIMEOUT_S = 60


def test_get_channel_from_unknown_type_raises_exception():
    with pytest.raises(TypeError):
        _ = ChannelManager.allocate(object, FGC_C, RETH2, RBAC_NAME, TIMEOUT_S)


@pytest.mark.parametrize("channel_class", SYNC_CHANNEL_CLASSES)
def test_sync_get_channel_can_be_called_using_channel_class_and_fgc(channel_class):
    channel = ChannelManager.allocate(channel_class, FGC_C, RETH2, RBAC_NAME, TIMEOUT_S)
    assert channel is not None
    ChannelManager.free(channel_class, FGC_C, RETH2, RBAC_NAME)


@pytest.mark.parametrize("channel_class", ASYNC_CHANNEL_CLASSES)
@pytest.mark.asyncio
async def test_async_get_channel_can_be_called_using_channel_class_and_fgc(channel_class):
    channel = await AsyncChannelManager.allocate(channel_class, RETH2, RBAC_NAME, TIMEOUT_S)
    assert channel is not None
    await AsyncChannelManager.free(channel_class, RETH2, RBAC_NAME)


@pytest.mark.parametrize("channel_class", SYNC_CHANNEL_CLASSES)
def test_sync_channel_is_acquired_and_freed(channel_class):
    numOfChannels = len(ChannelManager._channel_cache)
    channel = ChannelManager.allocate(channel_class, FGC_C, RETH2, RBAC_NAME, TIMEOUT_S)
    assert channel is not None
    ChannelManager.free(channel_class, FGC_C, RETH2, RBAC_NAME)
    assert len(ChannelManager._channel_cache) == numOfChannels


@pytest.mark.parametrize("channel_class", ASYNC_CHANNEL_CLASSES)
@pytest.mark.asyncio
async def test_async_channel_is_acquired_and_freed(channel_class):
    numOfChannels = len(AsyncChannelManager._channel_cache)
    channel = await AsyncChannelManager.allocate(channel_class, RETH2, RBAC_NAME, TIMEOUT_S)
    assert channel is not None
    await AsyncChannelManager.free(channel_class, RETH2, RBAC_NAME)
    assert len(AsyncChannelManager._channel_cache) == numOfChannels


@pytest.mark.parametrize("channel_class", SYNC_CHANNEL_CLASSES)
def test_sync_get_channel_data_makes_sense(channel_class):
    channel = ChannelManager.allocate(channel_class, FGC_C, RETH2, RBAC_NAME, TIMEOUT_S)
    channel_data = ChannelManager._channel_cache[RBAC_NAME, FGC_C, channel_class, RETH2]
    assert channel_data["count"] == 1
    assert channel_data["channel"] == channel
    ChannelManager.free(channel_class, FGC_C, RETH2, RBAC_NAME)


@pytest.mark.parametrize("channel_class", ASYNC_CHANNEL_CLASSES)
@pytest.mark.asyncio
async def test_async_get_channel_data_makes_sense(channel_class):
    channel = await AsyncChannelManager.allocate(channel_class, RETH2, RBAC_NAME, TIMEOUT_S)
    channel_data = AsyncChannelManager._channel_cache[RBAC_NAME, channel_class, RETH2]
    assert channel_data["count"] == 1
    assert channel_data["channel"] == channel
    await AsyncChannelManager.free(channel_class, RETH2, RBAC_NAME)


def test_sync_channel_is_shared_different_threads():
    def get_channel_from_manager(fgc, gw, res):
        channel = ChannelManager.allocate(SyncChannel, fgc, gw, RBAC_NAME_2, TIMEOUT_S)
        res[fgc] = id(channel)

    results = dict()
    t1 = threading.Thread(target=get_channel_from_manager, args=(FGC_C, RETH2, results))
    t2 = threading.Thread(target=get_channel_from_manager, args=(FGC_C, RETH2, results))

    t1.start()
    t2.start()
    t1.join()
    t2.join()
    assert len(set(results.values())) == 1
