from .__version__ import __version__, __emails__, __authors__

from . import constants
from .api import (
    fgc,
    connect,
    disconnect,
    get,
    set,
    subscribe,
    create_manual_subscription_session,
    async_fgc,
    async_connect,
    async_disconnect,
    async_get,
    async_set,
    monitor_session,
    monitor_port,
    connect_rt,
)
from .exceptions import (
    InvalidRbacTokenError,
    PyFgcError,
    PyFgcSessionError,
    UnknownDevice,
    AmbiguousTarget,
    ChannelError,
    FgcResponseError,
    ProtocolNotSupportedError,
)
from .fgc_monitor import MonitorSession, MonitorPort
from .fgc_session import FgcSession, FgcAsyncSession
from .responses.fgc_response import FgcResponse, FgcResponseError

import logging
logging.getLogger(__name__).addHandler(logging.NullHandler())

# fh = logging.FileHandler("/user/pclhc/var/log/fgc_logger/dev/pyfgc.log")
# fh.setLevel(logging.DEBUG)

# FILELOG_FORMAT = "[%(asctime)s] %(levelname)s (%(module)s) %(message)s"
# fh_formatter = logging.Formatter(FILELOG_FORMAT)
# fh.setFormatter(fh_formatter)

# logging.getLogger(__name__).addHandler(fh)
