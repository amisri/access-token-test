import logging
import struct
import time
from typing import Callable

from pyfgc.constants import CommandResponseProtocolSymbols
from pyfgc.channels.serial_channel import SerialChannel
from pyfgc.responses.serial_response import SerialResponse
from pyfgc.command_encoders.serial_command_encoder import SerialCommandEncoder
from pyfgc.protocols.fgc_protocol import FgcProtocol

logger = logging.getLogger(__name__)

class SerialProtocol(FgcProtocol):
    def __init__(self) -> None:
        super().__init__(channel_class=SerialChannel)

    def connect(self, target_serial_port: str, target_gw: str, timeout_s: int, rbac_token: bytes) -> None:
        target_gw = target_serial_port
        super().connect(target_serial_port, target_gw, timeout_s, rbac_token)

    def disconnect(self) -> None:
        super().disconnect()

    def get(self, property_name: str, get_option: str = None) -> SerialResponse:
        encoded_command = SerialCommandEncoder.encode_get(self.target_fgc, property_name, get_option)
        sent_command_time = time.time()
        raw_response = self._send_command_to_target(encoded_command)
        return SerialResponse(raw_response, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    def set(self, property_name: str, value: str) -> SerialResponse:
        encoded_command = SerialCommandEncoder.encode_set(self.target_fgc, property_name, value)
        sent_command_time = time.time()
        raw_response = self._send_command_to_target(encoded_command)
        return SerialResponse(raw_response, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    def subscribe(self, property_name: str, on_receive_callback: Callable[[SerialResponse], None] = None):
        raise NotImplementedError(f"Subscribe method is not implemented for {type(self)}.")

    def unsubscribe(self, property_name: str = None) -> None:
        pass

    def _send_command_to_target(self, encoded_command: bytes) -> bytes:
        raw_response = None
        with self.channel.lock:
            try:
                self.channel.write(encoded_command)
            except Exception as e:
                raise RuntimeError(f"Exception while sending command {encoded_command.decode()} to port {self.target_fgc}: {e}") from e

            try:
                raw_response = self._receive_data_from_target()
            except Exception as e:
                raise RuntimeError(f"Exception while receiving response to command {encoded_command.decode()} to port {self.target_fgc}: {e}") from e

        return raw_response

    def _receive_data_from_target(self) -> bytes:
        response = list()

        received = self.channel.read(1)
        while received not in [
            CommandResponseProtocolSymbols.VOID,
            CommandResponseProtocolSymbols.END,
            CommandResponseProtocolSymbols.BIN_FLAG,
            CommandResponseProtocolSymbols.ERROR
        ]:
            response.append(received)
            received = self.channel.read(1)

        response.append(received)

        # Binary response
        if received == CommandResponseProtocolSymbols.BIN_FLAG:
            response_length_bytes = self.channel.read(4)
            response.append(response_length_bytes)

            # Format string !L -> ! (network = big endian), L (unsigned long)
            response_length_int = struct.unpack("!L", response_length_bytes)[0]

            byte_counter = 0
            while byte_counter < response_length_int:
                incoming_bytes = self.channel.read(response_length_int)
                byte_counter += len(incoming_bytes)
                response.append(incoming_bytes)

        byte_rsp = b"".join(response)
        return byte_rsp

    def set_rbac_token_on_target(self, rbac_token: bytes) -> None:
        """ No RBAC token needed when being connected directly to an FGC via USB """
        pass

    def _send_rbac_token_to_target(self, rbac_token: bytes) -> None:
        """ No RBAC token needed when being connected directly to an FGC via USB """
        pass
