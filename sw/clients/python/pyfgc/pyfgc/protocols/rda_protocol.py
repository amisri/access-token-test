import logging
from typing import Callable

import pyrda3
import time

from pyfgc.channels.rda_channel import RdaChannel
from pyfgc.responses.rda_response import RdaResponse
from pyfgc.protocols.fgc_protocol import FgcProtocol

logger = logging.getLogger(__name__)

class RdaSubscriptionListener(pyrda3.NotificationListener):
    """
    Manage subscription connection and data/callbacks
    """

    def __init__(self, target_fgc: str, property_name: str, on_receive_callback: Callable[[RdaResponse], None]) -> None:
        self._sub = None
        self._access_point = None
        self._target_fgc = target_fgc
        self._property_name = property_name
        self._on_receive_callback = on_receive_callback

        # super()__init__(self) doesn't work here because the pyrda3 python package is just a wrapper around C code, packaged with Cython.
        pyrda3.NotificationListener.__init__(self)

    def subscribe(self, rda_channel: RdaChannel, cycle_selector: str = None) -> None:
        try:
            logger.info(f"Subscribing {self._target_fgc}:{self._property_name}")
            self._access_point = rda_channel.client.get_access_point(self._target_fgc, self._property_name)

            if cycle_selector:
                context = pyrda3.RequestContextFactory.create(selector=cycle_selector)
                self._sub = self._access_point.subscribe(listener=self, context=context)
            else:
                self._sub = self._access_point.subscribe(listener=self)
        except Exception as e:
            logger.error(e)
            raise e

    def unsubscribe(self) -> None:
        logger.info(f"Unsubscribing {self._target_fgc}:{self._property_name}")
        self._access_point.unsubscribe(self._sub)
        self._sub = None
        self._access_point = None

    def data_received(self, subscription, value, updateType) -> None:
        response = RdaResponse(value, self._target_fgc, self._property_name, update_type=updateType)
        self._on_receive_callback(response)

    def error_received(self, subscription, exception, updateType) -> None:
        rsp_data = {'err_msg': f'{exception}'}
        response = RdaResponse(rsp_data, self._target_fgc, self._property_name, update_type=updateType)
        self._on_receive_callback(response)


class RdaProtocol(FgcProtocol):

    def __init__(self) -> None:
        super().__init__(channel_class=RdaChannel)

        # Keeps track of ongoing subscriptions. 'Property' -> RdaSubscriptionListener
        self.subscription_listeners = dict()

    def connect(self, target_fgc: str, target_gw: str, timeout_s: int, rbac_token: bytes) -> None:
        super().connect(target_fgc, target_gw, timeout_s, rbac_token)

    def disconnect(self) -> None:
        super().disconnect()

    def get(self, property_name: str, get_option: str = None) -> RdaResponse:
        if get_option is not None:
            get_option = ' ' + get_option
        else:
            get_option = ''

        sent_command_time = time.time()
        try:
            rda_rsp = self.channel.client.get_access_point(self.target_fgc, property_name + get_option).get()
        except pyrda3.RdaException as error:
            logger.exception(error)
            rda_rsp = {}
            rda_rsp['err_msg'] = error

        return RdaResponse(rda_rsp, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    def set(self, property_name: str, value: str) -> RdaResponse:
        set_data = pyrda3.DataFactory.create_data()
        set_data["value"] = str(value)

        rsp_data = {}
        sent_command_time = time.time()
        try:
            self.channel.client.get_access_point(self.target_fgc, property_name).set(set_data)
        except Exception as error:
            logger.exception(error)
            rsp_data['err_msg'] = error

        return RdaResponse(rsp_data, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    def subscribe(self, property_name: str, on_receive_callback: Callable[[RdaResponse], None], cycle_selector: str = None) -> None:
        """
        Subscribe to a device property

        Args:
            on_receive_callback (Callable[[RdaResponse], None]): optional callback function which is invoked every time we receive a RdaResponse (data or error) from the subscription
            property_name (str): property to subscribe to
            cycle_selector (str): Name of the cycle selector (default is ``None``) to filter the subscription
        """
        if property_name in self.subscription_listeners:
            logger.warning('Subscription for property {property_name} on {self.target_fgc} already exists. Not creating new subscription.')
            return

        listener = RdaSubscriptionListener(self.target_fgc, property_name, on_receive_callback)
        listener.subscribe(self.channel, cycle_selector=cycle_selector)
        self.subscription_listeners[property_name] = listener

    def unsubscribe(self, property_name: str = None) -> None:
        """
        Unsubscribe from the provided device property_name, or from all if no property_name is provided.
        """
        if property_name is None:
            # Unsubscribe all properties
            for listener in self.subscription_listeners.values():
                if listener is not None:
                    listener.unsubscribe()
                    del listener
            self.subscription_listeners = dict()
            return

        if property_name in self.subscription_listeners:
            # Unsubscribe specific property
            listener = self.subscription_listeners.pop(property_name)
            if listener is not None:
                listener.unsubscribe()
                del listener

    def set_rbac_token_on_target(self, rbac_token: bytes) -> None:
        super().set_rbac_token_on_target(rbac_token)

    def _send_rbac_token_to_target(self, rbac_token: bytes) -> None:
        with self.channel.lock:
            self.channel.client.set_authentication_token(rbac_token)
