import logging
import struct
import time
import traceback
from typing import Callable

from pyfgc.constants import CommandResponseProtocolSymbols
from pyfgc.channels.sync_channel import SyncChannel
from pyfgc.responses.async_sync_response import AsyncSyncResponse
from pyfgc.command_encoders.async_sync_command_encoder import AsyncSyncCommandEncoder
from pyfgc.protocols.fgc_protocol import FgcProtocol
from pyfgc.exceptions import ChannelError

logger = logging.getLogger(__name__)

class SyncProtocol(FgcProtocol):
    def __init__(self) -> None:
        super().__init__(channel_class=SyncChannel)

    def connect(self, target_fgc: str, target_gw: str, timeout_s: int, rbac_token: bytes) -> None:
        super().connect(target_fgc, target_gw, timeout_s, rbac_token)

    def disconnect(self) -> None:
        super().disconnect()

    def get(self, property_name: str, get_option: str = None) -> AsyncSyncResponse:
        encoded_command = AsyncSyncCommandEncoder.encode_get(self.target_fgc, property_name, get_option)
        sent_command_time = time.time()
        try:
            raw_response = self._send_command_to_target(encoded_command)
        except BrokenPipeError as e:
            super().destroy_channel()
            raise ChannelError(e)
        return AsyncSyncResponse(raw_response, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    def set(self, property_name: str, value: str) -> AsyncSyncResponse:
        encoded_command = AsyncSyncCommandEncoder.encode_set(self.target_fgc, property_name, value)
        sent_command_time = time.time()
        try:
            raw_response = self._send_command_to_target(encoded_command)
        except BrokenPipeError as e:
            super().destroy_channel()
            raise ChannelError(e)
        return AsyncSyncResponse(raw_response, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    def subscribe(self, property_name: str, on_receive_callback: Callable[[AsyncSyncResponse], None] = None):
        raise NotImplementedError(f"Subscribe method is not implemented for {type(self)}.")

    def unsubscribe(self, property_name: str = None) -> None:
        pass

    def set_rbac_token_on_target(self, rbac_token: bytes) -> None:
        super().set_rbac_token_on_target(rbac_token)

    def _send_rbac_token_to_target(self, rbac_token: bytes) -> None:
        # Cannot reuse `set()` of this class instance because this command has 
        # to be sent to the gateway of this device.
        property_name = "CLIENT.TOKEN"
        rbac_command = AsyncSyncCommandEncoder.encode_set(self.target_gw, property_name, rbac_token)
        try:
            self._send_command_to_target(rbac_command)
        except BrokenPipeError as e:
            super().destroy_channel()
            raise ChannelError(e)

    def _send_command_to_target(self, encoded_command: bytes) -> bytes:
        raw_response = None
        with self.channel.lock:
            try:
                self.channel.write(encoded_command)
            except Exception as e:
                raise RuntimeError(f"Exception while sending command {encoded_command.decode()}") from e

            try:
                raw_response = self._receive_data_from_target()
            except Exception as e:
                raise RuntimeError(f"Exception while receiving response to command {encoded_command.decode()}: {repr(e)}, {traceback.print_exc()}") from e

        return raw_response

    def _receive_data_from_target(self) -> bytes:
        response = list()

        received = self.channel.read(1)
        while received not in [
            CommandResponseProtocolSymbols.VOID,
            CommandResponseProtocolSymbols.END,
            CommandResponseProtocolSymbols.BIN_FLAG
        ]:
            response.append(received)
            received = self.channel.read(1)

        response.append(received)

        # Binary response
        if received == CommandResponseProtocolSymbols.BIN_FLAG:
            response_length_bytes = self.channel.read(4)
            response.append(response_length_bytes)

            # Format string !L -> ! (network = big endian), L (unsigned long)
            # Length read from the payload + 2 characters for the '\n;' in the end
            response_length_int = struct.unpack('!L', response_length_bytes)[0] + 2

            byte_counter = 0
            while byte_counter < response_length_int:
                incoming_bytes = self.channel.read(response_length_int - byte_counter)
                byte_counter += len(incoming_bytes)
                response.append(incoming_bytes)

        byte_rsp = b"".join(response)
        return byte_rsp
