import logging
import time

from pyfgc.channels.channel_manager import AsyncChannelManager, AsyncChannel
from pyfgc.responses.async_sync_response import AsyncSyncResponse
from pyfgc.exceptions import ChannelError

logger = logging.getLogger(__name__)


class AsyncProtocol:
    def __init__(self) -> None:
        self.channel = None  # AsyncChannel
        self.target_fgc = None
        self.target_gw = None
        self.timeout_s = None
        self.rbac_username = None
        self._channel_class = AsyncChannel

    def is_connected(self):
        return self.channel is not None

    async def connect(self, target_fgc: str, target_gw: str, timeout_s: int, rbac_username: str) -> None:
        self.target_fgc = target_fgc
        self.target_gw = target_gw
        self.timeout_s = timeout_s
        self.rbac_username = rbac_username

        try:
            self.channel = await AsyncChannelManager.allocate(self._channel_class, self.target_gw, self.rbac_username,
                                                              timeout_s=self.timeout_s,
                                                              callback=self.invalidate_channel)
        except Exception as e:
            raise RuntimeError(f"Could not connect to {self.target_gw}: {e}") from e

    async def disconnect(self) -> None:
        if not self.channel:
            return
        try:
            await AsyncChannelManager.free(self._channel_class, self.target_gw, self.rbac_username)
        except Exception:
            logger.exception(
                f"Failed to disconnect from gateway {self.target_gw}, channel_class: {self._channel_class}")
        finally:
            self.channel = None

    def invalidate_channel(self):
        self.channel = None

    async def get(self, property_name: str, get_option: str = None) -> AsyncSyncResponse:
        if not self.channel:
            raise ChannelError("Channel is invalid!")
        sent_command_time = time.time()
        raw_response = await self.channel.get(property_name, get_option, self.target_fgc)
        return AsyncSyncResponse(raw_response, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    async def set(self, property_name: str, value: str) -> AsyncSyncResponse:
        if not self.channel:
            raise ChannelError("Channel is invalid!")
        sent_command_time = time.time()
        raw_response = await self.channel.set(property_name, value, self.target_fgc)
        return AsyncSyncResponse(raw_response, self.target_fgc, property_name, timestamp_sent=sent_command_time)

    async def set_rbac_token_on_target(self, rbac_token: bytes) -> None:
        if rbac_token is None:
            logger.warning(f"Received None token for gateway {self.target_gw}. Did not set token on target.")
            return

        try:
            await self._send_rbac_token_to_target(rbac_token)
        except Exception as e:
            await self.disconnect()
            raise RuntimeError(f"Could not set rbac token on gateway {self.target_gw}: {e}") from e

    async def _send_rbac_token_to_target(self, rbac_token: bytes) -> None:
        await self.channel.set("CLIENT.TOKEN", rbac_token)

# EOF
