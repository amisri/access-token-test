import logging

from abc import ABC, abstractmethod
from typing import TypeVar, Callable

from pyfgc.channels.channel_manager import ChannelManager
from pyfgc.responses.fgc_response import FgcResponse
from pyfgc.exceptions import ChannelError

logger = logging.getLogger(__name__)

class FgcProtocol(ABC):
    """ Abstract class that serves as base class for fgc protocol implementations """
    def __init__(self, channel_class: TypeVar('FgcChannel')) -> None:
        self.channel_id = None
        self.target_fgc = None
        self.target_gw = None
        self.timeout_s = None
        self._channel_class = channel_class

    @property
    def channel(self):
        return ChannelManager.use_channel(self.channel_id)

    def is_connected(self):
        return self.channel is not None

    @abstractmethod
    def connect(self, target_fgc: str, target_gw: str, timeout_s: int, rbac_username: str) -> None:
        self.target_fgc = target_fgc
        self.target_gw = target_gw
        self.timeout_s = timeout_s
        self.rbac_username = rbac_username

        try:
            self.channel_id = ChannelManager.allocate(self._channel_class, self.target_fgc, self.target_gw, self.rbac_username, timeout_s=self.timeout_s)
        except Exception as e:
            raise RuntimeError(f"Could not connect to {self.target_gw} for FGC {self.target_fgc}: {e}") from e

    @abstractmethod
    def disconnect(self) -> None:
        try:
            ChannelManager.free(self.channel_id)
        except Exception:
            logger.exception(f"Failed to disconnect from gateway {self.target_gw}, device {self.target_fgc}, channel_class: {self._channel_class}")
        finally:
            self.channel_id = None

    @abstractmethod
    def get(self, property_name: str, get_option: str = None) -> FgcResponse:
        pass

    @abstractmethod
    def set(self, property_name: str, value: str) -> FgcResponse:
        pass

    @abstractmethod
    def subscribe(self, property_name: str, on_receive_callback: Callable[[FgcResponse], None]) -> None:
        pass

    @abstractmethod
    def unsubscribe(self, property_name: str = None) -> None:
        pass

    @abstractmethod
    def set_rbac_token_on_target(self, rbac_token: bytes) -> None:
        if rbac_token is None:
            logger.warning(f"Received None token for gateway {self.target_gw}. Did not set token on target.")
            return

        try:
            self._send_rbac_token_to_target(rbac_token)
        except ChannelError as e:
            self.channel_id = None
            raise
        except Exception as e:
            self.disconnect()
            raise RuntimeError(f"Could not set rbac token on gateway {self.target_gw}: {e}") from e

    @abstractmethod
    def _send_rbac_token_to_target(self, rbac_token: bytes) -> None:
        """ To be implemented by the respective protocol class """
        pass

    def destroy_channel(self) -> None:
        ChannelManager.destroy(self.channel_id)
