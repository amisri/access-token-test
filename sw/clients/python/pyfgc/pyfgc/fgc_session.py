import asyncio
import concurrent.futures
import logging
import os
import base64

from datetime import datetime, timedelta
from typing import Callable

import pyfgc_name
import pyfgc_rbac

from pyfgc.constants import ProtocolTypes, TIMEOUT_S, ASYNC_TIMEOUT_S, RBAC_EXTENSION_DURATION
from pyfgc.exceptions import InvalidRbacTokenError, PyFgcError, ProtocolNotSupportedError, UnknownDevice, \
    AmbiguousTarget, ProtocolTimeoutError
from pyfgc.protocols.async_protocol import AsyncProtocol
from pyfgc.protocols.serial_protocol import SerialProtocol
from pyfgc.protocols.sync_protocol import SyncProtocol
from pyfgc.responses.fgc_response import FgcResponse

logger = logging.getLogger(__name__)

# Protocol string to class mappings
PROTOCOL_CLASSES = {
    ProtocolTypes.ASYNC.value: AsyncProtocol,
    ProtocolTypes.SERIAL.value: SerialProtocol,
    ProtocolTypes.SYNC.value: SyncProtocol,
}

try:
    from pyfgc.protocols.rda_protocol import RdaProtocol

    PROTOCOL_CLASSES[ProtocolTypes.RDA.value] = RdaProtocol
except ImportError:
    logger.debug(
        "Unable to import RDA protocol. RDA protocol will not be available. To have RDA protocol available, run 'pip install pyrda3'")

# RBAC tokens will be renewed if they have less than 1 hour left until expiration
TOKEN_EXPIRATION_MARGIN = timedelta(hours=1)


class FgcSession(object):
    """
    FgcSession is the main class for interacting with a FGC device.
    It provides methods for getting and setting properties and subscribing to property changes.

    An FgcSession always has a protocol assigned to it. The protocol defines how the FgcSession communicates with the target device.
    Currently supported protocols are:
    - SYNC: Non-async-await protocol over TCP.
    - SERIAL: The FgcSession will communicate with the target device via a serial connection.
    - RDA: The FgcSession will communicate with the target device via CERN's RDA communication protocol.

    An FgcSession is also responsible for acquiring and renewing RBAC tokens.
    """

    def __init__(self, target_device_name: str, protocol: str, rbac_token: bytes = None, timeout_s: int = TIMEOUT_S,
                 name_file_path: str = None) -> None:
        self.protocol = protocol
        self.fgc = target_device_name
        self._protocol_object = None
        self._target_fgc = None
        self._target_gw = None
        self._timeout_s = None
        self._rbac_token = None
        self._rbac_expiration_time = None
        self._rbac_is_external = False
        self._rbac_username = None

        self._process_args(target_device_name, rbac_token, timeout_s, name_file_path)
        self._connect()

    def get(self, property_name: str, get_option: str = None) -> FgcResponse:
        self.renew_rbac_token()
        return self._protocol_object.get(property_name, get_option=get_option)

    def set(self, property_name: str, value) -> FgcResponse:
        self.renew_rbac_token()
        return self._protocol_object.set(property_name, value)

    def subscribe(self, property_name: str, on_receive_callback: Callable[[FgcResponse], None],
                  cycle_selector: str = None) -> None:
        self.renew_rbac_token()
        return self._protocol_object.subscribe(property_name, on_receive_callback, cycle_selector=cycle_selector)

    def unsubscribe(self, property_name: str = None) -> None:
        return self._protocol_object.unsubscribe(property_name)

    def disconnect(self) -> None:
        self._protocol_object.unsubscribe()
        self._protocol_object.disconnect()

    def set_external_rbac_token(self, external_rbac_token: bytes) -> None:
        """
        Assigns the provided external RBAC token to the FgcSession and sets it on the target gateway.
        Will prevent the FgcSession from acquiring new tokens, but will make it try to extend the lifetime of the provided token automatically.
        """
        self._set_rbac_token_on_target(external_rbac_token, is_external=True)

    def renew_rbac_token(self, force: bool = False) -> None:
        """
        Renews the current RBAC token if it is nearly expired (t<TOKEN_EXPIRATION_MARGIN).
        If force is True, the remaining lifetime will be ignored and the renewal will be triggered in any case.
        If the current token was acquired by the session itself, a new token will be acquired.
        If the current token was provided externally, the session tries to extend the lifetime of the external token.
        """
        if self._rbac_token is None:
            logger.warning("Can not renew RBAC token. Currently there is no RBAC token to renew.")
            return

        if self._rbac_expiration_time is None:
            logger.warning("Can not renew RBAC token. Expiration time of current token is unknown.")
            return

        if not force and datetime.now() < self._rbac_expiration_time - TOKEN_EXPIRATION_MARGIN:
            # Not expiring anytime soon. Skip renewal as it is not necessary
            return

        if not self._rbac_is_external:
            # Acquire new token because we don't care about user info if token was acquired by FgcSession
            internal_rbac_token = _acquire_new_rbac_token()
            self._set_rbac_token_on_target(internal_rbac_token, is_external=False)
        else:
            # Try to extend the lifetime of the external provided token because we want it to have the same user info as before
            external_rbac_token = _try_to_extend_rbac_token(self._rbac_token)
            self._set_rbac_token_on_target(external_rbac_token, is_external=True)

    def _connect(self) -> None:
        self._protocol_object.connect(self._target_fgc, self._target_gw, self._timeout_s, self._rbac_username)
        self._set_rbac_token_on_target(self._rbac_token, self._rbac_is_external)

    def _process_args(self, target_device_name: str, rbac_token: bytes = None, timeout_s: int = TIMEOUT_S,
                      name_file_path: str = None) -> None:
        try:
            self._protocol_object = PROTOCOL_CLASSES[self.protocol]()
        except KeyError as e:
            raise ProtocolNotSupportedError(
                f"Protocol {self.protocol} is not supported. Supported protocols are: {PROTOCOL_CLASSES.keys()}") from e
        self._timeout_s = timeout_s

        if isinstance(self._protocol_object, SerialProtocol):
            self._target_fgc = target_device_name
        else:
            self._target_fgc, self._target_gw = _resolve_from_name_file(target_device_name, name_file_path)
            if rbac_token is not None:
                self._set_rbac_token_on_target(rbac_token, is_external=True)
            else:
                rbac_token = _acquire_new_rbac_token()
                self._set_rbac_token_on_target(rbac_token, is_external=False)

    def _set_rbac_token_on_target(self, rbac_token: bytes, is_external: bool) -> None:
        """
        Assigns the provided RBAC token to the FgcSession and sets it on the target gateway.
        If is external: FgcSession will try to extend the lifetime of the provided rbac token before expiration on every get, set, subscribe
        If is not external: FgcSession will automatically acquire new rbac tokens before expiration on every get, set, subscribe
        """
        if rbac_token is None:
            logger.warning("Can not set RBAC token. No RBAC token provided.")
            return

        try:
            token_dict = pyfgc_rbac.token_to_dict(rbac_token)
            self._rbac_expiration_time = datetime.fromtimestamp(int(token_dict['ExpirationTime']))
            self._rbac_username = token_dict['UserName']
        except Exception as e:
            msg = f"Can not set invalid RBAC token: {e}"
            logger.exception(msg)
            raise InvalidRbacTokenError(msg)

        self._rbac_is_external = is_external
        self._rbac_token = rbac_token

        if self._protocol_object is not None and self._protocol_object.is_connected():
            self._protocol_object.set_rbac_token_on_target(rbac_token)
            info = "External" if is_external else "Internal"
            logger.info(f"{info} RBAC token set on target. Expiration time: {self._rbac_expiration_time}")

    def __repr__(self) -> str:
        return f"protocol: {self.protocol},\n \
                 fgc: {self.fgc},\n \
                 _protocol_object: {self._protocol_object},\n \
                 _target_fgc: {self._target_fgc},\n \
                 _target_gw: {self._target_gw},\n \
                 _timeout_s: {self._timeout_s},\n \
                 _token: {self._rbac_token},\n \
                 _token_expiration_time: {self._rbac_expiration_time},\n \
                 _token_is_external: {self._rbac_is_external}\n"


class FgcAsyncSession:

    def __init__(self, target_device_name: str, protocol: str, instance_called: bool = False):
        if not instance_called:
            raise NotImplementedError("FgcAsyncSession must not be called directly. "
                                      "Use FgcAsynSession.instance() instead.")

        self.fgc = target_device_name
        self.protocol = protocol
        self._protocol_object: AsyncProtocol = None
        self._target_fgc = None
        self._target_gw = None
        self._timeout_s = None
        self._rbac_token = None
        self._rbac_expiration_time = None
        self._rbac_is_external = False
        self._rbac_username = None

    @staticmethod
    async def instance(target_device_name: str, protocol: str, rbac_token: bytes = None,
                       timeout_s: int = ASYNC_TIMEOUT_S, name_file_path=None):  # -> FgcAsyncSession
        obj = FgcAsyncSession(target_device_name, protocol, instance_called=True)
        await obj._process_args(target_device_name, rbac_token, timeout_s, name_file_path)
        await obj._connect()
        return obj

    def _process_args_sync(self, target_device_name: str, rbac_token: bytes = None, timeout_s: int = ASYNC_TIMEOUT_S,
                           name_file_path: str = None):
        try:
            self._protocol_object = PROTOCOL_CLASSES[self.protocol]()
        except KeyError as e:
            raise ProtocolNotSupportedError(
                f"Protocol {self.protocol} is not supported. Supported protocols are: {PROTOCOL_CLASSES}") from e

        self._target_fgc, self._target_gw = _resolve_from_name_file(target_device_name, name_file_path)
        self._timeout_s = timeout_s
        self._rbac_token = rbac_token
        if self._rbac_token is None:
            self._rbac_token = _acquire_new_rbac_token()

    async def _connect(self):
        try:
            await self._protocol_object.connect(self._target_fgc, self._target_gw, self._timeout_s, self._rbac_username)
            await asyncio.wait_for(self._set_rbac_token_on_target(self._rbac_token, self._rbac_is_external), TIMEOUT_S)
        except asyncio.TimeoutError:
            raise ProtocolTimeoutError


    async def _process_args(self, target_device_name, rbac_token: bytes = None, timeout_s: int = ASYNC_TIMEOUT_S,
                            name_file_path: str = None):
        with concurrent.futures.ThreadPoolExecutor() as pool:
            await asyncio.get_running_loop().run_in_executor(pool, self._process_args_sync, target_device_name,
                                                             rbac_token, timeout_s, name_file_path)

    async def disconnect(self):
        await self._protocol_object.disconnect()

    async def get(self, property_name: str, get_option: str = None):
        try:
            await asyncio.wait_for(self.renew_rbac_token(), TIMEOUT_S)
            return await asyncio.wait_for(self._protocol_object.get(property_name, get_option=get_option), TIMEOUT_S)
        except asyncio.TimeoutError:
            raise ProtocolTimeoutError

    async def set(self, property_name: str, value: str):
        try:
            await asyncio.wait_for(self.renew_rbac_token(), TIMEOUT_S)
            return await asyncio.wait_for(self._protocol_object.set(property_name, value), TIMEOUT_S)
        except asyncio.TimeoutError:
            raise ProtocolTimeoutError

    async def set_external_rbac_token(self, external_rbac_token: bytes) -> None:
        """
        Assigns the provided external RBAC token to the FgcSession and sets it on the target gateway.
        Will prevent the FgcSession from acquiring new tokens, but will make it try to extend the lifetime of the provided token automatically.
        """
        await self._set_rbac_token_on_target(external_rbac_token, is_external=True)

    async def _set_rbac_token_on_target(self, rbac_token: bytes, is_external: bool) -> None:
        """
        Assigns the provided RBAC token to the FgcSession and sets it on the target gateway.
        If is external: FgcSession will try to extend the lifetime of the provided rbac token before expiration on every get, set, subscribe
        If is not external: FgcSession will automatically acquire new rbac tokens before expiration on every get, set, subscribe
        """
        if rbac_token is None:
            logger.warning("Can not set RBAC token. No RBAC token provided.")
            return

        try:
            token_dict = pyfgc_rbac.token_to_dict(rbac_token)
            self._rbac_expiration_time = datetime.fromtimestamp(int(token_dict['ExpirationTime']))
            self._rbac_username = token_dict['UserName']
        except Exception as e:
            msg = f"Can not set invalid RBAC token: {e}"
            logger.exception(msg)
            raise InvalidRbacTokenError(msg)

        self._rbac_is_external = is_external
        self._rbac_token = rbac_token

        if self._protocol_object is not None and self._protocol_object.is_connected():
            await self._protocol_object.set_rbac_token_on_target(rbac_token)
            info = "External" if is_external else "Internal"
            logger.info(f"{info} RBAC token set on target. Expiration time: {self._rbac_expiration_time}")

    async def renew_rbac_token(self, force: bool = False) -> None:
        """
        Renews the current RBAC token if it is nearly expired (t<TOKEN_EXPIRATION_MARGIN).
        If force is True, the remaining lifetime will be ignored and the renewal will be triggered in any case.
        If the current token was acquired by the session itself, a new token will be acquired.
        If the current token was provided externally, the session tries to extend the lifetime of the external token.
        """
        if self._rbac_token is None:
            logger.warning("Can not renew RBAC token. Currently there is no RBAC token to renew.")
            return

        if self._rbac_expiration_time is None:
            logger.warning("Can not renew RBAC token. Expiration time of current token is unknown.")
            return

        if not force and datetime.now() < self._rbac_expiration_time - TOKEN_EXPIRATION_MARGIN:
            # Not expiring anytime soon. Skip renewal as it is not necessary
            return

        if not self._rbac_is_external:
            # Acquire new token because we don't care about user info if token was acquired by FgcSession
            internal_rbac_token = _acquire_new_rbac_token()
            await self._set_rbac_token_on_target(internal_rbac_token, is_external=False)
        else:
            # Try to extend the lifetime of the external provided token because we want it to have the same user info as before
            external_rbac_token = _try_to_extend_rbac_token(self._rbac_token)
            await self._set_rbac_token_on_target(external_rbac_token, is_external=True)


def _resolve_from_name_file(target_device_name: str, namefile_path: str = None) -> tuple([str, str]):
    """
    Returns a tuple (fgc, gw) resolved via the name file by the given target device name
    """
    try:
        pyfgc_name.read_name_file(namefile_path)
    except FileNotFoundError as e:
        if namefile_path is None:
            raise FileNotFoundError("Unable to find default name file") from e
        else:
            raise FileNotFoundError(f"Unable to find name file {namefile_path}") from e

    resolved_targets = pyfgc_name.build_device_tset(target_device_name)

    if len(resolved_targets) == 0:
        raise UnknownDevice(f"Target {target_device_name} not found in name file {namefile_path}")

    if len(resolved_targets) > 1:
        raise AmbiguousTarget(f"Too many devices found for target {target_device_name}")

    # resolved_targets is a set, so we have to call pop instead of index
    return resolved_targets.pop()


def _acquire_new_rbac_token() -> bytes:
    """
    Try to acquire RBAC token in the following order:
    1. Explicitly provided via environment variable
    2. By Authorized Host Location declared in CCDE
    3. By Kerberos ticket exchange
    4. Throw exception if none of the above works
    """
    err = ''

    # Explicit token via env. variable -- if provided, takes full priority
    if 'RBAC_TOKEN_SERIALIZED' in os.environ:
        encoded = os.environ["RBAC_TOKEN_SERIALIZED"]
        if not pyfgc_rbac.is_expired(encoded):
            return base64.b64decode(encoded)

    else:
        msg = "No RBAC token in os environment detected. Falling back to get RBAC by location.\n"
        logger.debug(msg)
        err += msg

    # Authentication by location. Preferred over Kerberos for better predictability.
    try:
        return pyfgc_rbac.get_token_location()
    except pyfgc_rbac.RbacServerError as e:
        msg = f"Unable to get new RBAC token by location: {e}. Falling back to get RBAC by krb5.\n"
        logger.debug(msg)
        err += msg

    # Finally, try Kerberos if available
    try:
        return pyfgc_rbac.get_token_kerberos()
    except (pyfgc_rbac.KerberosUnavailableError, pyfgc_rbac.RbacServerError, pyfgc_rbac.GSSError) as e:
        msg = f"Unable to get new RBAC token by krb5: {e}\n"
        logger.debug(msg)
        err += msg

    raise PyFgcError(f"Unable to acquire new RBAC token:\n{err}")


def _try_to_extend_rbac_token(token_to_extend) -> bytes:
    try:
        return pyfgc_rbac.renew_token(token_to_extend, RBAC_EXTENSION_DURATION)
    except pyfgc_rbac.RbacServerError as e:
        raise PyFgcError(f"Unable to renew provided RBAC token:\n{e}")

# EOF
