import logging
import re

from pyfgc.responses.fgc_response import FgcResponse, ERROR_CODE_MESSAGE

logger = logging.getLogger(__name__)

class SerialResponsesRegEx():
    SUCCESS = re.compile(rb"\$(?!\xff)([\w\W]*)\n;")
    SUCCESS_BIN = re.compile(rb"\$\xff([\x00-\xff]*)\n;$")
    ERROR = re.compile(rb"\$[\w\W]*\$([\w\W]*)\n!")


class SerialResponse(FgcResponse):
    """A parsed response from a single FGC device retrieved via the SERIAL protocol

    Arguments:
        raw_response (bytes): Byte string received from the FGC.
        device_name (str): Name of the device (defaults to empty string)
        property_name (str): Name of the property (defaults to empty string)
        err_policy (str): Policy for decoding errors (defaults to ignore)
        timestamp_sent (float): Timestamp when the request was sent (defaults to None)

    Returns:
        SerialResponse: The parsed response object.
    """

    def __init__(self, raw_response, device_name: str = "", property_name: str = "", err_policy: str = "ignore", timestamp_sent: float = None):
        if not isinstance(raw_response, bytes):
            raise TypeError(f"SerialResponse can only be built from a byte string: {raw_response}")
        self.err_policy = err_policy
        super().__init__(raw_response, device_name, property_name, timestamp_sent)

    def _parse(self):
        """SERIAL specific response parse implementation. Overwrites the _parse function of the super class."""
        raw_response = self.raw_response
        err_policy = self.err_policy

        match = SerialResponsesRegEx.SUCCESS.search(raw_response)
        if match:
            self._value = match.group(1).decode(errors=err_policy)

        match = SerialResponsesRegEx.SUCCESS_BIN.search(raw_response)
        if match:
            self._value = match.group(1).decode(errors=err_policy)

        match = SerialResponsesRegEx.ERROR.search(raw_response)
        if match:
            error_string = match.group(1).decode(errors=err_policy)
            self._err_code = ERROR_CODE_MESSAGE.search(error_string).group(1)
            self._err_msg = ERROR_CODE_MESSAGE.search(error_string).group(2)
