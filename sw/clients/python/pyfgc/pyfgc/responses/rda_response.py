import logging
import pyrda3

from typing import Union
from pyfgc.responses.fgc_response import FgcResponse

logger = logging.getLogger(__name__)

class RdaResponse(FgcResponse):
    """A parsed response from a single FGC device retrieved via the RDA/CMW protocol

    Arguments:
        rda_acquisition (dict or pyrda3.AcquiredData): response parsed by pyrda3
        device_name (str): Name of the device (defaults to empty string)
        property_name (str): Name of the property (defaults to empty string)
        timestamp_sent (float): Timestamp when the request was sent (defaults to None)
        update_type (str): subscription-only field to know the update type of data received

    Returns:
        RdaResponse: The parsed response object.
    """

    def __init__(self, rda_acquisition: Union[dict, pyrda3.AcquiredData], device_name: str = "", property_name: str = "", 
            timestamp_sent: float = None, update_type: str = None):
        if not isinstance(rda_acquisition, dict) and not isinstance(rda_acquisition, pyrda3.AcquiredData):
            raise TypeError(f"RdaResponse can only be built from pyrda3.AcquiredData: {rda_acquisition}")

        self._cycle = None
        self._update_type = None
        self._cycle_timestamp = None
        self._acquisition_timestamp = None
        self._update_type = update_type

        super().__init__(rda_acquisition, device_name, property_name, timestamp_sent)

    def _parse(self):
        """RDA specific response parse implementation. Overwrites the _parse function of the super class."""
        rda_acquisition = self.raw_response

        if isinstance(rda_acquisition, dict):
            self._err_msg = rda_acquisition.get("err_msg", "")
            return

        try:
            data_keys = sorted(list(rda_acquisition.data.entry_names))
            if len(data_keys) == 1:
                self._value = RdaResponse._parse_rda_value(rda_acquisition.data[data_keys[0]])
            else:
                for key in data_keys:
                    self._value += f'{key}:{RdaResponse._parse_rda_value(rda_acquisition.data[key])}\n'
            self._timestamp_received = rda_acquisition.context.acq_stamp
            self._cycle = rda_acquisition.context.cycle_name
            self._cycle_timestamp = rda_acquisition.context.cycle_stamp
            self._acquisition_timestamp = rda_acquisition.context.acq_stamp
        except Exception as error:
            self._err_msg = str(error)

    @property
    def cycle(self) -> str:
        """
        Name of the cycle/user that the acquisition belongs

        Returns:
            cycle (str): The cycle that the response belongs to.
        """
        return self._cycle

    @property
    def update_type(self) -> str:
        """
        Each message received on a subscription has a type; they can
        be a first update or part of a continuous stream of messages.

        Returns:
            update_type (str): The update type of the response.
        """
        return self._update_type

    @property
    def cycle_timestamp(self) -> str:
        """
        Returns:
            cycle_timestamp (str): The timestamp of the cycle that the response belongs to.
        """
        return self._cycle_timestamp

    @property
    def acquisition_timestamp(self) -> str:
        """
        Returns:
            acquisition_timestamp (str): The timestamp of the acquisition that the response belongs to.
        """
        return self._acquisition_timestamp



    @staticmethod
    def _parse_rda_value(value) -> str:
        """
        Transforms an RDA property value into a string
        @return: RDA value as string
        """
        try:
            import numpy
        except ModuleNotFoundError:
            raise RuntimeError('RDA methods require the full version of pyfgc. To install: pip install pyfgc[full]')

        # check if value is a float
        if isinstance(value, float):
            return f'{value:.7E}'
        # check if value is a numpy array
        elif isinstance(value, numpy.ndarray):
            if value.size == 0:
                return ''
            # array of floats
            if isinstance(value.dtype, float):
                sci_notation_format = [f'{i:.7E}' for i in value]
                return ",".join(sci_notation_format)
            # before joining we need to convert the list of values (could be integers) into a list of strings
            return ",".join(map(str, value))
        # other datatype
        else:
            return str(value)
