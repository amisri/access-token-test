import logging
import re

from pyfgc.responses.fgc_response import FgcResponse, ERROR_CODE_MESSAGE

logger = logging.getLogger(__name__)

class AsyncSyncResponseRegEx(object):
    SUCCESS = re.compile(rb"\$(\w{0,31})\s{1}\.\n(?!\xff)([\w\W]*)\n;")
    SUCCESS_BIN = re.compile(rb"\$(\w{0,31})\s{1}\.\n\xff([\x00-\xff]*)\n;")
    ERROR = re.compile(rb"\$(\w{0,31})\s{1}!\n([\w\W]*)\n;")
    '''
    BEGIN - Optimized Regex

    Looks at beginning of a valid response, without parsing the value.
    This is more efficient when parsing responses (ex. binary) of multiple MB.

    Group 1: Match tag (returns tag)
    Group 2: Match if response is OK
    Group 3: Match if response is OK and binary (returns binary array size - not decoded)
    Group 4: Match if response is ERROR (returns error message and code)
    '''
    BEGIN = re.compile(rb"\$(\w{0,31})\s{1}(?:(?:\.\n((?:\xff([\x00-\xff]{4})|(?!\xff))))|(?:!\n((?:(?!\n    ;).)*)\n;))")


class AsyncSyncResponse(FgcResponse):
    """A parsed response from a single FGC device retrieved via the ASYNC or SYNC protocol

    Arguments:
        raw_response (bytes): Byte string received from the FGC.
        device_name (str): Name of the device (defaults to empty string)
        property_name (str): Name of the property (defaults to empty string)
        err_policy (str): Policy for decoding errors (defaults to ignore)
        timestamp_sent (float): Timestamp when the request was sent (defaults to None)

    Returns:
        AsyncSyncResponse -- The parsed response object.
    """

    def __init__(self, raw_response: bytes, device_name: str = "", property_name: str = "", err_policy: str = "ignore", timestamp_sent: float = None):
        if not isinstance(raw_response, bytes):
            raise TypeError(f"AsyncSyncResponse can only be built from a byte string: {raw_response}")
        self.err_policy = err_policy
        super().__init__(raw_response, device_name, property_name, timestamp_sent)

    def _parse(self):
        """ASYNC/SYNC specific response parse implementation. Overwrites the _parse function of the super class."""
        raw_response = self.raw_response
        err_policy = self.err_policy

        match = AsyncSyncResponseRegEx.SUCCESS.match(raw_response)
        if match:
            self._tag = match.group(1).decode(errors=err_policy)
            self._value = match.group(2).decode(errors=err_policy)

        match = AsyncSyncResponseRegEx.SUCCESS_BIN.match(raw_response)
        if match:
            self._tag = match.group(1).decode(errors=err_policy)
            self._value = match.group(2)

        match = AsyncSyncResponseRegEx.ERROR.match(raw_response)
        if match:
            self._tag = match.group(1).decode(errors=err_policy)
            self._error_string = match.group(2).decode(errors=err_policy)
            self._err_code = ERROR_CODE_MESSAGE.search(self._error_string).group(1)
            self._err_msg = ERROR_CODE_MESSAGE.search(self._error_string).group(2)
