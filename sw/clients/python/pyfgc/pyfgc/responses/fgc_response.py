import logging
import re
import time

from abc import ABC, abstractmethod
from pyfgc.exceptions import FgcResponseError

logger = logging.getLogger(__name__)

ERROR_CODE_MESSAGE = re.compile(r"\s*(\d{1,})\s([\w\W]*)")


class FgcResponse(ABC):
    """
    Abstract class, representing a parsed response from a single FGC device.

    Arguments:
        raw_response (bytes): Byte string received from the FGC.
        device_name (str): Name of the device (defaults to empty string)
        property_name (str): Name of the property (defaults to empty string)
        timestamp_sent (float): Timestamp when the request was sent (defaults to None)

    Raises:
        TypeError: Raised when the passed raw_response's type does not match the raw type needed by the specific response implementation.
        FgcResponseError: When the value of a response that contains an error is accessed

    Returns:
        FgcResponse: The parsed response object.
    """

    def __init__(self, raw_response, device_name: str = "", property_name: str = "", timestamp_sent: float = None):
        self._device_name = device_name
        self._err_code = ""
        self._err_msg = ""
        self._property_name = property_name
        self._raw_response = raw_response
        self._tag = ""
        self._timestamp_sent = timestamp_sent
        self._timestamp_received = time.time()
        self._value = ""

        # calls the protocol specific parse function of the respective subclass (to which the object instance belongs)
        self._parse()

    @abstractmethod
    def _parse(self):
        """ To be implemented by the protocol specific response implementation """
        pass

    @property
    def device_name(self):
        """
        This is the name of the device that sent the response.

        Returns:
            str: Device name
        """
        return self._device_name

    @property
    def property_name(self):
        """
        This is the name of the property concerned by the response.

        Returns:
            str: Property name
        """
        return self._property_name

    @property
    def err_code(self):
        """
        This is the error code received from the device, as seen `here 
        <https://accwww.cern.ch/proj-fgc/gendoc/def/Errors.htm>`_.

        Returns:
            int: Error code
        """
        return self._err_code

    @property
    def err_msg(self):
        """
        This is the error code message from the device, as seen `here 
        <https://accwww.cern.ch/proj-fgc/gendoc/def/Errors.htm>`_.

        Returns:
            str: Error message
        """
        return self._err_msg

    @property
    def raw_response(self):
        """
        This is the raw response received from the device, before any preprocessing 
        takes place.

        Returns:
            bytes: Byte string response from the device
        """
        return self._raw_response

    @property
    def tag(self):
        """
        The NCRP allows for the server responses to be asynchronous.
        A client application may submit many commands before receiving any responses, 
        and the responses may be out of order.  To make it possible for the client to
        relate responses to commands, the server will accept a command tag, and will
        return it at the start of the response.
        
        The tag may be 0 to 31 characters in length and is delimited by a space.
        Note that the space is always required, even for a null tag of zero characters.
        If the command starts "!S device:...", then the "S" will be interpreted as the
        tag and an error will be reported.

        The tag is not interpreted in any way by the server.

        `We provide this tag field as it might be helpful to debug the communication
        between the client and the server while using async API.`

        Returns:
            str: Tag string sent on the original request
        """
        return self._tag

    @property
    def timestamp_received(self):
        """
        This is the timestamp when the response was received from the device.

        Returns:
            float: Timestamp in seconds
        """
        return self._timestamp_received

    @property
    def timestamp_sent(self):
        """
        This is the timestamp when the response was sent to the device.

        Returns:
            float: Timestamp in seconds
        """
        return self._timestamp_sent

    @property
    def duration(self):
        """
        This is the duration that it took for the whole command to be sent 
        and the response to be received from the device.

        Returns:
            float: duration in seconds
        """
        return self.timestamp_received - self.timestamp_sent

    @property
    def value(self):
        """
        This property contains the value of the response in string format.

        Returns:
            str: parsed response string from the device

        Raises:
            FgcResponseError: When the value of a response that contains an error is accessed
        """
        if (not self._value and (self._err_code or self._err_msg)):
            raise FgcResponseError(f"{self._err_code}: {self._err_msg}")
        return self._value

    def __str__(self):
        return f"<FgcResponse: 'type={type(self)}' 'tag={self._tag}' 'device_name={self.device_name}' 'property_name={self.property_name}' 'err_code={self._err_code}' 'err_msg={self._err_msg}' 'value={self._value}'>"
