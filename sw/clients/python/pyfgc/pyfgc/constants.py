import enum

# General connection timeout
TIMEOUT_S = 60
ASYNC_TIMEOUT_S = 2

# Amount of seconds by which an external RBAC token should be extended upon renewal
RBAC_EXTENSION_DURATION = 60 * 60 * 8

GW_PORT = 1905


# Enum to define protocol types
class ProtocolTypes(enum.Enum):
    SYNC = "sync"
    ASYNC = "async"
    SERIAL = "serial"
    RDA = "rda"


class CommandResponseProtocolSymbols():
    VOID = b""
    BIN_FLAG = b"\xFF"
    END = b";"
    ERROR = b"!"
    HANDSHAKE = b"+"
    COMMAND_ENCODE_END = b"\n"
