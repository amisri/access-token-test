import logging

from pyfgc.command_encoders.fgc_command_encoder import FgcCommandEncoder
from pyfgc.constants import CommandResponseProtocolSymbols

logger = logging.getLogger(__name__)

class AsyncSyncCommandEncoder(FgcCommandEncoder):
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def encode_get(device_name: str, property_name: str, get_option: str = None, tag: str = None) -> bytes:
        command_tag = tag if tag is not None else ""

        command_init_bytes = "!{} G {}:{}".format(command_tag, device_name, property_name).encode()
        option_bytes = get_option.encode() if get_option else "".encode()

        return command_init_bytes + " ".encode() + option_bytes + CommandResponseProtocolSymbols.COMMAND_ENCODE_END

    @staticmethod
    def encode_set(device_name: str, property_name: str, value: str = None, tag: str = None) -> bytes:
        command_tag = tag if tag is not None else ""

        command_init_bytes = "!{} S {}:{}".format(command_tag, device_name, property_name).encode()
        value_bytes = FgcCommandEncoder._encode_value(value)

        return command_init_bytes + " ".encode() + value_bytes + CommandResponseProtocolSymbols.COMMAND_ENCODE_END
