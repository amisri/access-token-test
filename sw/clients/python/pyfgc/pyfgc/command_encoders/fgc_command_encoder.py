import logging
import struct

from abc import ABC, abstractstaticmethod

logger = logging.getLogger(__name__)

class FgcCommandEncoder(ABC):
    """ Abstract class that serves as base class for fgc command encoder implementations """
    def __init__(self) -> None:
        pass

    @staticmethod
    @abstractstaticmethod
    def encode_get(device_name: str, property_name: str, get_option: str = None, tag: str = None) -> bytes:
        pass

    @staticmethod
    @abstractstaticmethod
    def encode_set(device_name: str, property_name: str, value: str = None, tag: str = None) -> bytes:
        pass

    @staticmethod
    def _encode_value(value):
        # string
        if isinstance(value, str):
            return value.encode()

        # bytes: case for the CLIENTS.TOKEN property
        if isinstance(value, bytes):
            return struct.pack('!BL', 255, len(value)) + value

        # list or tuple
        if isinstance(value, (list, tuple, set)):
            str_list = [str(item) for item in value]
            return ",".join(str_list).encode()

        # numbers, other data types
        return str(value).encode()
