import logging
import threading
from abc import ABC, abstractmethod
from pyfgc.constants import TIMEOUT_S

logger = logging.getLogger(__name__)

class FgcChannel(ABC):
    """ Abstract class that serves as base class for fgc channel implementations """
    def __init__(self, device_name: str, timeout_s: int = TIMEOUT_S) -> None:
        self.device_name = device_name
        self.timeout_s = timeout_s
        self.lock = threading.Lock()
        self.create()

    @abstractmethod
    def create(self) -> None:
        pass

    @abstractmethod
    def read(self) -> bytes:
        pass

    @abstractmethod
    def write(self) -> None:
        pass

    @abstractmethod
    def destroy(self) -> None:
        pass
