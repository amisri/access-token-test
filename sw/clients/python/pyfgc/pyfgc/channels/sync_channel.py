import logging
import socket

from pyfgc.exceptions import ChannelError
from pyfgc.channels.fgc_channel import FgcChannel
from pyfgc.constants import TIMEOUT_S, GW_PORT, CommandResponseProtocolSymbols

logger = logging.getLogger(__name__)

class SyncChannel(FgcChannel):

    def __init__(self, device_name: str, timeout_s: int = TIMEOUT_S) -> None:
        self.port = GW_PORT
        self.socket = socket.socket()
        self.socket.settimeout(timeout_s)
        super().__init__(device_name, timeout_s)

    def create(self) -> None:
        try:
            self.socket.connect((self.device_name, self.port))
        except socket.timeout as ste:
            logger.warning(f"Could not find device {self.device_name} on port {self.port}: {ste}")
            raise ChannelError(f"Could not connect to device {self.device_name} on port {self.port}: {ste}")

        with self.lock:
            try:
                self._do_handshake()
            except Exception as e:
                if self.socket:
                    self.socket.close()
                    logger.warning(f"Could not complete handshake with device {self.device_name}: {e}")
                    raise ChannelError(f"Could not complete handshake with device {self.device_name}: {e}")

    def read(self, n_bytes: int) -> bytes:
        return self.socket.recv(n_bytes)

    def write(self, message: bytes) -> None:
        self.socket.sendall(message)

    def destroy(self) -> None:
        if self.socket:
            try:
                self.socket.close()
            except Exception as err_message:
                logger.warning(f"Something went wrong while closing the sync channel. See {err_message}")

    def _do_handshake(self) -> None:
        if self.socket.recv(1) != CommandResponseProtocolSymbols.HANDSHAKE:
            raise ChannelError(f"Handshake with device {self.device_name} not successful!")
        else:
            self.socket.sendall(CommandResponseProtocolSymbols.HANDSHAKE)
