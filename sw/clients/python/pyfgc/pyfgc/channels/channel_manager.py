import asyncio
import functools
import logging
import threading

from typing import TypeVar

from pyfgc.channels.fgc_channel import FgcChannel
from pyfgc.channels.async_channel import AsyncChannel
from pyfgc.constants import TIMEOUT_S, ASYNC_TIMEOUT_S

logger = logging.getLogger(__name__)


class ChannelManager(object):
    # Maps a tuple (username, fgc, channel type, gateway) to an opened channel
    _channel_cache = dict()

    # Lock used to change the _channel_cache entries
    __lock = threading.Lock()

    @classmethod
    def allocate(cls, channel_class: TypeVar('FgcChannel'), target_fgc: str, target_gw: str, rbac_username: str,
                 timeout_s: int = TIMEOUT_S) -> int:
        # Why use a hash here, you ask? If we return the channel itself then we cannot easily invalidate a channel because
        # every protocol instance maintains a reference to the channel. If we return a hash then we can invalidate
        # the channel by removing it from the cache and then the next time a protocol instance tries to use it, it will
        # be equal to None 
        channel_id = hash((rbac_username, target_fgc, channel_class, target_gw))

        if not issubclass(channel_class, FgcChannel):
            raise TypeError(
                f"Can not allocate channel of given type {channel_class}. Must be a subclass of FgcChannel.")

        with cls.__lock:
            # Retrieve channel from cache
            cache_entry = cls._channel_cache.get(channel_id, None)

            # Or create a new one if it doesn't exist
            if not cache_entry:
                cache_entry = {
                    'count': 0,
                    'channel': channel_class(target_gw, timeout_s)
                }
                cls._channel_cache[channel_id] = cache_entry

            # Increment the usage count and return channel
            cache_entry['count'] += 1
            return channel_id

    @classmethod
    def use_channel(cls, channel_id: int) -> FgcChannel:
        with cls.__lock:
            # Retrieve channel from cache
            cache_entry = cls._channel_cache.get(channel_id, None)
            if cache_entry:
                return cache_entry['channel']

    @classmethod
    def destroy(cls, channel_id: int) -> None:
        """We need this function to destroy channel in case it is not valid anymore
        """
        with cls.__lock:
            # Retrieve channel from cache
            cache_entry = cls._channel_cache.get(channel_id, None)

            # Soft-fail because the channel as been destroyed already
            if not cache_entry:
                return

            # Forcefully terminate the channel if it exists, ignoring all possible errors
            try:
                cache_entry['channel'].destroy()
                del cls._channel_cache[channel_id]
            except Exception:
                pass

    @classmethod
    def free(cls, channel_id: int) -> None:
        with cls.__lock:
            # Retrieve channel from cache
            cache_entry = cls._channel_cache.get(channel_id, None)

            # If entry does not exist we soft-fail (it should be there, as we just freed a channel)
            if not cache_entry:
                return

            # Decrement the usage count
            cache_entry['count'] -= 1

            # if no more users: Disconnect channel and remove entry from cache
            if cache_entry['count'] <= 0:
                cache_entry['channel'].destroy()
                del cls._channel_cache[channel_id]


class AsyncChannelManager:
    # Maps a tuple (username, fgc, channel type, gateway) to an opened channel
    _channel_cache = dict()

    # Lock used to change the _channel_cache entries
    __lock = asyncio.Lock()

    @classmethod
    async def allocate(cls, channel_class: TypeVar('AsyncChannel'), target_gw: str, rbac_username: str,
                       timeout_s: int = ASYNC_TIMEOUT_S, callback=None) -> AsyncChannel:
        # target_fgc is not needed for async, but is kept to comply with FgcProtocol abstract
        channel_key = (rbac_username, channel_class, target_gw)

        if not issubclass(channel_class, AsyncChannel):
            raise TypeError(
                f"Can not allocate channel of given type {channel_class}. Must be a subclass of FgcChannel.")

        async with cls.__lock:
            # Retrieve channel from cache
            cache_entry = cls._channel_cache.get(channel_key, None)

            # Or create a new one if it doesn't exist
            if not cache_entry:
                channel = await channel_class.create(target_gw, timeout_s)
                cache_entry = {
                    'count': 0,
                    'channel': channel,
                    'callbacks': set()
                }
                channel.add_connection_lost_callback(functools.partial(cls.connection_lost, channel_key))
                cls._channel_cache[channel_key] = cache_entry

            # Increment the usage count and return channel
            cache_entry['count'] += 1
            cache_entry['callbacks'].add(callback)
            return cache_entry['channel']

    @classmethod
    def connection_lost(cls, channel_key):
        cache_entry = cls._channel_cache.get(channel_key, None)

        if cache_entry:
            for cb in cache_entry['callbacks']:
                cb()
            del cls._channel_cache[channel_key]

    @classmethod
    async def free(cls, channel_class: TypeVar('AsyncChannel'), target_gw: str, rbac_username: str) -> None:
        # target_fgc is not needed for async, but is kept to comply with FgcProtocol abstract
        channel_key = (rbac_username, channel_class, target_gw)

        async with cls.__lock:
            # Retrieve channel from cache
            cache_entry = cls._channel_cache.get(channel_key, None)

            # If entry does not exist we soft-fail (it should be there, as we just freed a channel)
            if not cache_entry:
                return

            # Decrement the usage count
            cache_entry['count'] -= 1

            # if no more users: Disconnect channel and remove entry from cache
            if cache_entry['count'] <= 0:
                await cache_entry['channel'].destroy()
                del cls._channel_cache[channel_key]

# EOF
