import serial
import logging

from pyfgc.channels.fgc_channel import FgcChannel
from pyfgc.constants import TIMEOUT_S
from pyfgc.exceptions import ChannelError

logger = logging.getLogger(__name__)

class SerialChannel(FgcChannel):

    class SCRPNonPrintable(object):
        SCRP_ABORT_GET = b"\x03"
        SCRP_EXECUTE_CMD_LF = b"\x0A"
        SCRP_EXECUTE_CMD_CR = b"\x0D"
        SCRP_RESUME_TX = b"\x11"
        SCRP_SUSPEND_TX = b"\x13"
        SCRP_SWITCH_DIRECT = b"\x1A"
        SCRP_SWITCH_EDITOR = b"\x1B"

    def __init__(self, target_serial_port: str, timeout_s: int = TIMEOUT_S) -> None:
        self.target_serial_port = target_serial_port
        self.serial_channel = None
        super().__init__(target_serial_port, timeout_s)

    def create(self) -> None:
        try:
            self.serial_channel = serial.Serial(self.target_serial_port, timeout=self.timeout_s)
        except serial.SerialException as se:
            raise ChannelError(f"Could not connect to serial port {self.target_serial_port}: {se}")
        
        # This is a hack to get the FGC to switch from editor mode to direct mode
        # We just need to send a special character to the FGC and it will switch mode
        self._set_mode(SerialChannel.SCRPNonPrintable.SCRP_SWITCH_DIRECT)

    def read(self, n_bytes: int) -> bytes:
        return self.serial_channel.read(n_bytes)

    def write(self, message: bytes) -> int:
        self.serial_channel.write(message)

    def destroy(self) -> None:
        if self.serial_channel:
            try:
                self.serial_channel.close()
            except (serial.SerialException, AttributeError) as err_message:
                logger.warning(f"Something went wrong while closing the serial channel. See {err_message}")

    def _set_mode(self, mode) -> None:
        try:
            self.serial_channel.write(mode)
        except serial.SerialTimeoutException as err_message:
            logger.warning(f"Something went wrong while setting the mode. See message: {err_message}")
            raise ChannelError(f"Could not set terminal mode to {mode} on fgc {self.target_serial_port}: {err_message}")
