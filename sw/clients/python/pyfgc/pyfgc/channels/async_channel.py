import asyncio
import logging
import socket
import struct
from typing import Tuple, Callable

from pyfgc.command_encoders.async_sync_command_encoder import AsyncSyncCommandEncoder
from pyfgc.constants import ASYNC_TIMEOUT_S, GW_PORT, CommandResponseProtocolSymbols
from pyfgc.exceptions import ChannelError
from pyfgc.responses.async_sync_response import AsyncSyncResponseRegEx

logger = logging.getLogger(__name__)


class AsyncChannel(asyncio.Protocol):

    def __init__(self) -> None:
        self.transport = None
        self.handshake_made = False
        self.gateway = None
        self.raw_mode = False
        self.raw_stream = asyncio.StreamReader()
        self.buffer = bytearray()
        self.buffer_wait_n = 0
        self.commands = dict()
        self.tag_counter = 0
        self.connection_lost_callbacks = set()

    @staticmethod
    async def create(target_gw: str, timeout_s: int = ASYNC_TIMEOUT_S):  # -> AsyncChannel
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setblocking(False)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 5)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 5)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 5)
            await asyncio.wait_for(asyncio.get_running_loop().sock_connect(sock, (target_gw, GW_PORT)), timeout_s)
            _, channel = await asyncio.wait_for(
                asyncio.get_running_loop().create_connection(lambda: AsyncChannel(), sock=sock), timeout=timeout_s)
        except Exception:
            raise
        else:
            return channel

    async def destroy(self) -> None:
        self.disconnect()

    def disconnect(self) -> None:
        """
        Disconnects from the FGC
        """
        self.transport.close()

    def connection_made(self, transport: asyncio.Transport) -> None:
        """
        Callback invoked When connection has been established

        :param transport:
        :return:
        """
        self.transport = transport
        self.gateway = transport.get_extra_info("peername")
        self._send_handshake()

    def _send_handshake(self) -> None:
        """
        Send handshake to the gateway
        """
        self.transport.write(CommandResponseProtocolSymbols.HANDSHAKE)

    def connection_lost(self, exc) -> None:
        """
        Callback invoked When connection has been lost

        :param exc:
        :return:
        """

        for fut in self.commands.values():
            fut.cancel()
        for cb in self.connection_lost_callbacks:
            cb()

    def add_connection_lost_callback(self, callback: Callable) -> None:
        """
        Run (non-coroutine) callback when connection is lost
        """
        self.connection_lost_callbacks.add(callback)

    def data_received(self, payload: bytes) -> None:
        """
        Callback invoked when something is received from the socket

        :param payload:
        :return:
        """
        self.buffer += payload

        if not self.handshake_made:
            # handshake
            self._process_handshake()
        elif self.raw_mode:
            # Provide data to stream
            self._process_raw_stream()
        else:
            # Parse result into a response
            self._process_responses(payload)

    def _process_handshake(self) -> None:
        """
        Handle character when in 'waiting for handshake' mode

        :param payload:
        :return:
        """
        # Remove first character from buffer
        ack_char = chr(self.buffer.pop(0)).encode()
        if ack_char != CommandResponseProtocolSymbols.HANDSHAKE:
            raise ChannelError(
                f"Handshake with gateway {self.gateway} not successful (received {ack_char} vs {CommandResponseProtocolSymbols.HANDSHAKE})!")
        self.handshake_made = True

    def _process_raw_stream(self) -> None:
        # Just parse entire buffer into StreamReader
        self.raw_stream.feed_data(self.buffer)
        self.buffer.clear()

    def _process_responses(self, payload: bytes) -> None:

        self.buffer_wait_n = max(self.buffer_wait_n - len(payload), 0)

        # Check for '\n' only on new data (payload).
        if payload != CommandResponseProtocolSymbols.VOID and \
                (self.buffer_wait_n > 0 or CommandResponseProtocolSymbols.END not in payload):
            return

        while True:
            result = self._extract_response()
            tag, raw_response, wait_n = result
            if tag and raw_response:
                self._resolve_pending_request(tag, raw_response)
            elif wait_n:
                self.buffer_wait_n = wait_n
                break
            else:
                break

    def _extract_response(self) -> Tuple[str, bytes, int]:
        """
        Gets a response from the buffer
        :return: (tag, raw response, wait for N characters)
        """
        # NOTE: 'memoryview' will make the analysis of the buffer much more efficient.
        # For example, for slicing bytearray without copying its contents.

        # Check type of response (binary/non-binary/error)
        match = AsyncSyncResponseRegEx.BEGIN.search(self.buffer)

        if match and match.group(3) is not None:
            # The next response is binary
            span_begin, span_end = match.span()
            tag = match.group(1).decode()
            length = struct.unpack('>I', match.group(3))[0]

            rsp_end = span_end + length
            last_char = rsp_end + 2
            if len(self.buffer) < last_char:
                # BIN response is not ready. Wait for another N characters.
                wait_n = last_char - len(self.buffer)
                return (tag, None, wait_n)  # Wait for another N characters

            # Return response and character \xff at postion #0
            with memoryview(self.buffer) as buffer_view:
                raw_response = bytes(buffer_view[span_begin: last_char])
            del self.buffer[:last_char]

            return (tag, raw_response, None)  # Response is valid

        elif match and match.group(2) is not None:
            # The next response is non-binary
            span_begin, span_end = match.span()
            tag = match.group(1).decode()

            rsp_end = self.buffer.find(b"\n;", span_end)
            if rsp_end < 0:
                return (tag, None, None)  # Wait for another ? characters

            last_char = rsp_end + 2

            with memoryview(self.buffer) as buffer_view:
                raw_response = bytes(buffer_view[span_begin: last_char])
            del self.buffer[:last_char]

            return (tag, raw_response, None)  # Response is valid

        elif match and match.group(4) is not None:
            # The next response is an error
            span_begin, span_end = match.span()
            tag = match.group(1).decode()

            with memoryview(self.buffer) as buffer_view:
                raw_response = bytes(buffer_view[span_begin: span_end])
            del self.buffer[:span_end]

            return (tag, raw_response, None)

        # If no match found
        return (None, None, None)  # Wait for another ? characters

    def _resolve_pending_request(self, tag: str, raw_response: bytes) -> None:
        """
        Resolves a previously pending request
        :param raw_response:
        :return:
        """
        try:
            future = self.commands[tag]
        except KeyError as err:
            raise ChannelError(f"Tag {tag} is unknown.") from err

        del self.commands[tag]

        try:
            future.set_result(raw_response)
        except asyncio.InvalidStateError as err:
            # During termination, futures may be cancelled by their callers
            if not future.cancelled():
                raise RuntimeError("Future was already set before.") from err

    def _new_tag(self) -> str:
        self.tag_counter += 1
        return "{:08X}".format(self.tag_counter)[:8]

    def _register_pending_request(self, tag: str) -> asyncio.Future:
        """
        Register a pending command. The future will be resolved by the 'data_received' callback

        :param tag:
        :return:
        """
        self.commands[tag] = asyncio.get_running_loop().create_future()
        return self.commands[tag]

    async def send(self, payload: bytes):
        """
        Send given payload to the gateway

        :param payload: Bytes to send
        """
        if self.transport and not self.transport.is_closing():
            self.transport.write(payload)
        else:
            raise ChannelError("Tried sending / writing data to a closing or None socket.")

    async def enable_rterm_mode(self, channel, command: str = None) -> None:
        """
        Use raw communication instead of NCRP protocol.
        """
        command = "CLIENT.RTERMLOCK" if command is None else command
        raw = await self.set(command, str(channel))
        # AsyncSyncResponse(raw, '', '').value

        self.raw_mode = True
        self.data_received(b"")  # Flush data to StreamReader

    def get_stream_reader(self) -> asyncio.StreamReader:
        """
        Returns a StreamReader object buffering the received raw data.
        :return: asyncio.StreamReader
        """
        return self.raw_stream

    async def get(self, property_name: str, get_option: str = None, device: str = '') -> bytes:
        """
        Asynchronous get. Will resolve when the response arrives.

        :param property_name: property to get
        :param get_option: get options for the property
        :param device: device that should handle the command
        :return: future that will be resolved when the response arrived
        """
        tag = self._new_tag()
        command = AsyncSyncCommandEncoder.encode_get(device, property_name, get_option, tag=tag)
        future = self._register_pending_request(tag)
        await self.send(command)
        return await future

    async def set(self, property_name: str, value: str, device: str = '') -> bytes:
        """
        Asynchronous set. Will resolve when the response arrives.

        :param property_name: property to set
        :param value: value
        :param device: device that should handle the command
        :return: future that will be resolved when the response arrived
        """
        tag = self._new_tag()
        command = AsyncSyncCommandEncoder.encode_set(device, property_name, value, tag=tag)
        future = self._register_pending_request(tag)
        await self.send(command)
        return await future

# EOF
