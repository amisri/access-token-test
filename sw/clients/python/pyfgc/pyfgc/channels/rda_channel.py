import logging

from pyfgc.channels.fgc_channel import FgcChannel
from pyfgc.constants import TIMEOUT_S

logger = logging.getLogger(__name__)

class RdaChannel(FgcChannel):

    def __init__(self, device_name: str, timeout_s: int = TIMEOUT_S) -> None:
        self.client = None
        super().__init__(device_name, timeout_s)

    def create(self) -> None:
        try:
            import pyrda3
        except ModuleNotFoundError:
            raise RuntimeError('RDA methods require the full version of pyfgc. To install: pip install pyfgc[full]')

        try:
            self.client = pyrda3.ClientServiceBuilder.new_instance().build()
        except Exception as e:
            raise RuntimeError(f"Could not open clients instance for fgc {self.target_fgc}: {e}") from e

    def read(self) -> None:
        pass

    def write(self) -> None:
        pass

    def client(self):
        return self.client()

    def destroy(self) -> None:
        try:
            self.client.close()
        except Exception as e:
            raise e
