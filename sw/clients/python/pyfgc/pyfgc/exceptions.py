class InvalidRbacTokenError(Exception):
    pass


class PyFgcError(Exception):
    pass


class PyFgcSessionError(PyFgcError):
    pass


class UnknownDevice(PyFgcError):
    pass


class AmbiguousTarget(PyFgcError):
    pass


class ChannelError(PyFgcError):
    pass


class FgcResponseError(PyFgcError):
    pass


class ProtocolNotSupportedError(PyFgcError):
    pass


class ProtocolTimeoutError(PyFgcError):
    pass
