import logging
import struct
import socket
import pyfgc_name

from datetime import datetime
from pyfgc.constants import GW_PORT

logger = logging.getLogger(__name__)

FGC_FIELDBUS_CYCLE_PERIOD_MS = 20
FGC_FIELDBUS_CYCLE_FREQ = (1000 / FGC_FIELDBUS_CYCLE_PERIOD_MS)

udp_header_format = "=IIII"
udp_rt_data_format = udp_header_format + "IIQ64L"


class RtData:
    def __init__(self):
        self.sequence = 0
        self.data = [0] * 64
        self.active_channels = 0

    def set(self, channel, value):
        self.data[channel - 1] = value
        self.active_channels |= 1 << (channel - 1)


class FgcRt:
    def __init__(self, name_file=None):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.rt_data = {}

        self.devices = {}
        pyfgc_name.read_name_file(name_file)
        for dev, dev_info in pyfgc_name.devices.items():
            self.devices[dev.lower()] = (dev_info['gateway'], dev_info['channel'])

    def _htonl(self, value, ll=False):
        if ll:
            b = bin(value)[2:].zfill(64)
            high = int(b[0:32], 2)
            low = int(b[32:], 2)

            high_n = socket.htonl(high)
            low_n = socket.htonl(low)
            b_n = bin(low_n)[2:].zfill(32) + bin(high_n)[2:].zfill(32)
            return int(b_n, 2)
        else:
            if value < 0:
                return socket.htonl(value & 0xffffffff)
            return socket.htonl(value)

    def _long_from_float(self, fl):
        return struct.unpack('=l', struct.pack('=f', fl))[0]

    def send_refs(self, values, apply_time: datetime = None):
        for fgc, ref in values.items():
            gateway, channel = self.devices[fgc.lower()]
            if gateway not in self.rt_data:
                self.rt_data[gateway] = RtData()
            self.rt_data[gateway].set(channel, self._htonl(self._long_from_float(ref)))

        send_time = datetime.now()
        send_time_sec = self._htonl(int(send_time.timestamp()))
        send_time_usec = self._htonl(int(send_time.microsecond))

        apply_time_sec = 0
        apply_time_usec = 0
        if apply_time:
            apply_time_sec = self._htonl(int(apply_time.timestamp()))
            apply_time_usec = self._htonl(int(apply_time.microsecond))

        packets = {}

        active_rt = {gw: rt for gw, rt in self.rt_data.items() if rt.active_channels}
        for gateway, rt_data in active_rt.items():
            byte_message = struct.pack(udp_rt_data_format, 0, rt_data.sequence, send_time_sec, send_time_usec,
                                       apply_time_sec, apply_time_usec, self._htonl(rt_data.active_channels, ll=True),
                                       *rt_data.data)
            packets[gateway] = byte_message

            rt_data.sequence += 1
            rt_data.active_channels = 0

        # TODO: this should probably be "parallelized" so the packets are send to all gateways at the same time
        for gateway, packet in packets.items():
            self.socket.sendto(packet, socket.MSG_DONTWAIT, (gateway, GW_PORT))
