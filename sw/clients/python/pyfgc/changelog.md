VERSION 2.0.1; 2022-04-26
- Fix bug with rda protocol - set command, to convert value to string before sending it to the device

VERSION 2.0.0; 2022-04-17
- Major overhaul of documentation and examples
- API changes:
    -  **[BREAKING CHANGE]:** Some keyword arguments in api.py have been renamed:
        - target -> target_device_name
        - prop -> property_name
    - RDA subscriptions:
        - **[BREAKING CHANGE]:** the callback function for RDA subscribe is now mandatory (was optional before). It is invoked whenever the subscription receives data and should be used to process it. See test_rda.py for usage examples: https://gitlab.cern.ch/ccs/fgc/-/blob/master/sw/clients/python/pyfgc/pyfgc/tests/test_rda.py#L200
        - **[BREAKING CHANGE]:** the 'create_subscription' endpoint of api.py has been renamed to 'create_manual_subscription_session' and now returns a FgcSession object instead of a RdaSubscriptionManager object. The FgcSession provides a unsubscribe method that can be used to stop the subscriptions (see below).
        - RdaResponse now provides access to new properties: cycle, update_type, cycle_timestamp, acquisition_timestamp
        - RdaProtocol.subscribe now supports a cycle_selector filter as optional argument
    - FgcSession (sync):
        - **[BREAKING CHANGE]:** Passing an invalid (non-decodable / non-parsable) RBAC token to FgcSession will raise an InvalidRbacTokenError
        - FgcSession now has a unsubscribe method that allows to unsubscribe from RDA subscriptions for a specific property or from all, if no property is specified
        - FgcSession now allows to manually trigger renewal of the RBAC token via FgcSession.renew_rbac_token() with an optional force flag
        - FgcSession now allows to manually set an external RBAC token anytime via FgcSession.set_external_rbac_token(token), even if it was initialized with a different or internal token.
    - FgcResponse:
        - **[BREAKING CHANGE]:** FgcResponse no longer holds a protocol attribute. Instead, the class type now indicates the protocol that was used to acquire the response.
        - FgcResponse now provides access to the property_name and device_name that triggered the response
        - FgcResponse additionally provides the following new attributes: duration, timestamp_sent and timestamp_received
- Transparent changes and refactoring:
    - Refactoring according to deep review https://gitlab.cern.ch/ccs/fgc/-/issues/56
    - Introduction of ChannelManager, Constants, ProtocolTypes(Enum), Exceptions
    - Introduction of abstract base classes and concrete implementations for FgcProtocol, FgcResponse, FgcChannel, FgcCommandParser
    - Improving code quality, getting rid of several bad practices
    - Removed wildcard imports from main __init__.py
    - Removed unused adapters.py
    - Introduce type annotations
    - `logger.warn` renamed to `logger.warning` as it is deprecated
    - log messages in function to acquire RBAC token changed from `logger.warning` to `logger.debug`, in order to not spam the standard output by default
    - Refactoring of tests

VERSION 1.8.5; 2022-05-13
- [EPCCCS-XXXX] Add timeout argument to serial channel

VERSION 1.8.2; 2022-02-10
- [EPCCCS-XXXX] Remove an unintended dependency on Kerberos for Windows

VERSION 1.8.0; 2022-01-14
- [EPCCCS-XXXX] Add support for kerberos ticket exchange

VERSION 1.4.1; 2020-08-21
- [EPCCCS-8303] Use self._token instead of self.token to store the RBAC token

VERSION 1.4.0; 2020-08-21
- [EPCCCS-8303] Acquire token if rbac_token kw argument explicitly set to None

VERSION 1.3.9; 2020-08-20
- [EPCCCS-8285] Lock serial channel for multithreaded operations

VERSION 1.3.4; 2020-06-20
- [EPCCCS-xxxx] Fix pyfgc long timeout on serial response errors.

VERSION 1.3.3; 2020-05-22
- [EPCCCS-xxxx] Renew RBAC token automatically if one hour to expire (FgcSession).

VERSION 1.3.2; 2020-05-06
- [EPCCCS-xxxx] Updated version requirements of pyfgc_rbac and pyfgc_decoders.

VERSION 1.3.1; 2020-04-29
- [EPCCCS-xxxx] Bug fix on pyfgc rbac_token argument. RBAC can now be disabled explicitly by passing None.

VERSION 1.3.0; 2020-04-28
- [EPCCCS-xxxx] Modified pyfgc async interface. Fixed broken tests.
- [EPCCCS-8011] Revision of pyfgc error parsing.

VERSION 1.2.3; 2020-04-17
- [EPCCCS-xxxx] Fixed concurrency issues in pyfgc async.

VERSION 1.2.2; 2020-04-16
- [EPCCCS-7975] Allow multiple serial connections
- [EPCCCS-XXXX] Improve logging in sync_fgc

VERSION 1.2.1; 2020-04-02
- [EPCCCS-XXXX] Bug fixes in async_pyfgc.

VERSION 1.2.0; 2020-02-21
- [EPCCCS-7843] pyfgc async module

VERSION 1.1.6; 2020-02-05
- [EPCCCS-7425] Include placeholders for get/set coroutines.
- [EPCCCS-7425] Rearrange code in fgc_session.
- [EPCCCS-xxxx] Fixed several pyfgc monitor bugs.

VERSION 1.1.5; 2019-12-02
- [EPCCCS-7425] Add 'fgc' as a fgc_session member

VERSION 1.1.4; 2019-11-29
- [EPCCCS-XXXX] Simplify pyfgc api allowing one connection at a time only. Changes in monitor classes
- [EPCCCS-XXXX] Correct bug in fgc_session by which wrong device name is passed to pyfgc_name
- [EPCCCS-XXXX] Avoid calling pyfgc_name and pyfgc_rbac if protocol is serial
- [EPCCCS-7455] Rename protocol modules. Add consistency in function arguments naming
- [EPCCCS-7425] Change library's API
- [EPCCCS-6851] Improve pyfgc responses
