import json
import logging

from typing import Union

import requests
import fastapi.openapi.models as md
import fastapi.security as sc

from fastapi import Depends, HTTPException

from ccs_fastapi_utils.token import RbacToken, OauthToken, LabToken
from ccs_fastapi_utils.constants import OAUTH_URL


rbac_token_header = sc.api_key.APIKeyHeader(
    name="Authorization",
    scheme_name="RBAC Token",
    description="Client's RBAC token header. "
                "Value format: `Bearer <base64_encoded_token>`",
)

class AuthMeta(type):
    # Public attributes
    client_id: Union[str, None] = None
    secret: Union[str, None] = None
    enable_rbac: bool = False
    external_lab_name: Union[str, None] = None
    override_roles: Union[list, None] = None
    oauth_url: str = OAUTH_URL

    # Private attributes
    _oauth_flow = None
    _rbac_flow = None
    _jwk_list = None

    def __init__(cls, name, bases, dct):
        auth_flows = {}

        if cls.client_id:
            auth_flows["implicit"] = md.OAuthFlowImplicit(authorizationUrl=f"{cls.oauth_url}/auth")

        if cls.client_id and cls.secret:
            auth_flows["clientCredentials"] = md.OAuthFlowClientCredentials(tokenUrl=f"{cls.oauth_url}/token")

        if cls.client_id:
            cls._oauth_flow = sc.OAuth2(flows=md.OAuthFlows(**auth_flows))

        if cls.enable_rbac:
            cls._rbac_flow = rbac_token_header

        if cls.external_lab_name:
            def get_token(token: str = "") -> LabToken:
                return LabToken(token, cls.override_roles, cls.external_lab_name)
        else:
            cls.refresh_jwk_list()

            def get_token(
                jwt_token: str = Depends(cls._oauth_flow if cls._oauth_flow else lambda: ''),
                rbac_token: str = Depends(rbac_token_header if cls._rbac_flow else lambda: ''),
            ) -> Union[RbacToken, OauthToken]:
                token = None
                try:
                    token = OauthToken.parse(jwt_token, cls.client_id, cls._jwk_list)
                except Exception as e:
                    logging.warning(f"Failed to parse JWT token: {e}")
                
                if token and not token.has_expired():
                    return token

                try:
                    token = RbacToken.parse(rbac_token)
                except Exception as e:
                    logging.warning(f"Failed to parse RBAC token: {e}")
                    
                if token and not token.has_expired():
                    return token

                if token and token.has_expired():
                    raise HTTPException(
                        status_code=401,
                        detail="The RBAC token has expired."
                    )
                else:
                    raise HTTPException(
                        status_code=403,
                        detail="The access token is invalid."
                    )
        
        cls.get_token = get_token
        super().__init__(name, bases, dct)
    
    def refresh_jwk_list(cls):
        new_endpoint = f"{cls.oauth_url}/certs"
        cls._jwk_list = requests.get(new_endpoint).json()["keys"]
