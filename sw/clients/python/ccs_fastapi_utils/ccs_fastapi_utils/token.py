import base64
import json
import time
import typing

from abc import ABC, abstractmethod

import fastapi

try:
    import jwt
except ImportError:
    jwt = None

try:
    import pyfgc_rbac
except ImportError:
    pyfgc_rbac = None

class TokenBase(ABC):
    def __init__(self, encoded_token: str):
        self.encoded_token = encoded_token
        self._dict = None

    @staticmethod
    def _get_bearer(token):
        try:
            auth_scheme, bearer_token = token.split()
        except ValueError:
            raise fastapi.HTTPException(
                401,
                "Token is not a Bearer token.",
                headers={"WWW-Authenticate": "Bearer"},
            )

        stripped_bearer_token = bearer_token.strip()
        if auth_scheme != "Bearer" or not stripped_bearer_token:
            raise fastapi.HTTPException(
                401,
                "Token is not a Bearer token.",
                headers={"WWW-Authenticate": "Bearer"},
            )
        
        return stripped_bearer_token

    @property
    def to_dict(self):
        return self._dict

    def has_any_roles(self, *role_lists):
        flattened_roles = [role for roles in role_lists for role in roles]
        return any(role in self.roles for role in flattened_roles)

    def has_all_roles(self, *role_lists):
        flattened_roles = [role for roles in role_lists for role in roles]
        return all(role in self.roles for role in flattened_roles)

    def has_expired(self):
        return time.time() > self.expiration_time

    @staticmethod
    @abstractmethod
    def parse(token_str: str):
        pass

    @property
    @abstractmethod
    def username(self):
        pass

    @property
    @abstractmethod
    def roles(self):
        pass

    @property
    @abstractmethod
    def full_name(self):
        pass

    @property
    @abstractmethod
    def expiration_time(self):
        pass


class RbacToken(TokenBase):
    
    def __init__(self, encoded_token: str):
        super().__init__(encoded_token)
        self.decoded_token = base64.b64decode(encoded_token, validate=True)

        try:
            self._dict = pyfgc_rbac.token_to_dict(self.decoded_token)
        except Exception:
            raise fastapi.HTTPException(
                401,
                "Could not get token dict from token.",
                headers={"WWW-Authenticate": "Bearer"},
            )

    @staticmethod
    def parse(token_str: str):
        token_encoded = RbacToken._get_bearer(token_str)

        if not pyfgc_rbac.verify_signature(token_encoded):
            raise fastapi.HTTPException(
                401,
                "Invalid RBAC Token with bad signature.",
                headers={"WWW-Authenticate": "Bearer"},
            )

        return RbacToken(token_encoded)

    @property
    def username(self):
        return self._dict.get("UserName")

    @property
    def roles(self):
        return self._dict.get("Roles", [])

    @property
    def full_name(self):
        full_name = self._dict.get("UserFullName")
        if full_name:
            return full_name.title()

    @property
    def expiration_time(self):
        return float(self._dict.get("ExpirationTime", 0))


class OauthToken(TokenBase):
    def __init__(self, encoded_token: str, client_id: str, token_dict: dict):
        super().__init__(encoded_token)
        self.client_id = client_id
        self._dict = token_dict

    @staticmethod
    def parse(token_str: str, client_id: str, jwk_list: list):
        token_encoded = OauthToken._get_bearer(token_str)

        try:
            header = jwt.get_unverified_header(token_encoded)
            kid = header.get("kid")
            algorithm = header.get("alg", "RS256")
            public_key = OauthToken._fetch_public_key(jwk_list, kid)
        except Exception:
            raise fastapi.HTTPException(
                401,
                "Could not fetch public key from Oauth2 token.",
                headers={"WWW-Authenticate": "Bearer"},
            )

        try:
            token_dict = jwt.decode(token_encoded, public_key, algorithms=algorithm, audience=client_id)
            return OauthToken(token_encoded, client_id, token_dict)
        except Exception:
            raise fastapi.HTTPException(
                401,
                "Could not decode Oauth2 token.",
                headers={"WWW-Authenticate": "Bearer"},
            )

    @staticmethod
    def _fetch_public_key(jwk_list: list, kid: str = None) -> str:
        if kid:
            for jwk in jwk_list:
                if jwk["kid"] == kid:
                    wanted_jwk = jwk
                    break

        else:
            wanted_jwk = jwk_list[0]

        public_config = json.dumps(wanted_jwk)
        return jwt.algorithms.RSAAlgorithm.from_jwk(public_config)

    @property
    def username(self):
        return self._dict.get("sub")

    @property
    def roles(self):
        try:
            return self._dict["resource_access"][self.client_id]["roles"]
        except KeyError:
            return []

    @property
    def full_name(self):
        return self._dict.get("name")

    @property
    def expiration_time(self):
        return float(self._dict.get("exp", 0))


class LabToken(TokenBase):
    
    def __init__(self, encoded_token: str, roles_override: list, lab_name:  str):
        super().__init__(encoded_token)
        self._username = lab_name.replace(" ", "-").lower()
        self._name = lab_name
        self._roles = roles_override
        self._dict = {
            "UserName": self._username,
            "UserFullName": self._name,
            "Roles": self._roles,
            "ExpirationTime": float('inf'),
        }

    @staticmethod
    def parse(token_str: str, roles_override: list, lab_name: str):
        return LabToken("", roles_override, lab_name)

    @property
    def username(self):
        return self._username

    @property
    def roles(self):
        return self._roles

    @property
    def full_name(self):
        return self._name

    @property
    def expiration_time(self):
        return float('inf')
