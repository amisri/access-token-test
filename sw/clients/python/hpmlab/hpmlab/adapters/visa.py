#
# This file is part of the PyMeasure package.
#
# Copyright (c) 2013-2017 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import copy
import pyvisa
import numpy as np

from .adapter import Adapter


class VISAAdapter(Adapter):
    """ Adapter class for the VISA library using PyVISA to communicate
    with instruments.

    :param resource: VISA resource name that identifies the address
    :param kwargs: Any valid key-word arguments for constructing a PyVISA instrument
    """

    def __init__(self, resource_name, **kwargs):

        if isinstance(resource_name, int):
            resource_name = f'GPIB0::{resource_name}::INSTR'

        super().__init__()

        self.resource_name = resource_name
        self.manager = pyvisa.ResourceManager()

        safe_keywords = ['resource_name', 'timeout', 'term_chars',
                         'chunk_size', 'lock', 'delay', 'send_end',
                         'values_format', 'read_termination']

        for key in copy.deepcopy(kwargs):
            if key not in safe_keywords:
                kwargs.pop(key)

        self.connection = self.manager.get_instrument(resource_name, **kwargs)

    def __repr__(self):
        return f'VISAAdapter(resource={self.connection.resource_name})'

    def write(self, command):
        """ Writes a command to the instrument

        :param command: SCPI command string to be sent to the instrument
        """
        self.connection.write(command)

    def read(self):
        """ Reads until the buffer is empty and returns the resulting
        ASCII response

        :returns: String ASCII response of the instrument.
        """
        return self.connection.read()

    def read_bytes(self, size):
        """Reads a number of bytes from the buffer.

        :param size: number of bytes that will be received.
        :return: Bytestring response of the instrument.
        """
        return self.connection.read_bytes(size)

    def query(self, command):
        """ Writes the command to the instrument and returns the resulting
        ASCII response

        :param command: SCPI command string to be sent to the instrument
        :returns: String ASCII response of the instrument
        """
        return self.connection.query(command)

    def ask_values(self, command):
        """ Writes a command to the instrument and returns a list of formatted
        values from the result. The format of the return is configurated by
        self.config().

        :param command: SCPI command to be sent to the instrument
        :returns: Formatted response of the instrument.
        """
        return self.connection.query_values(command)

    def config(self, is_binary=False, datatype='str',
               container=np.array, converter='s',
               separator=',', is_big_endian=False):
        """ Configurate the format of data transfer to and from the instrument.

        :param is_binary: If True, data is in binary format, otherwise ASCII.
        :param datatype: Data type.
        :param container: Return format. Any callable/type that takes an iterable.
        :param converter: String converter, used in dealing with ASCII data.
        :param separator: Delimiter of a series of data in ASCII.
        :param is_big_endian: Endianness.
        """
        self.connection.values_format.is_binary = is_binary
        self.connection.values_format.datatype = datatype
        self.connection.values_format.container = container
        self.connection.values_format.converter = converter
        self.connection.values_format.separator = separator
        self.connection.values_format.is_big_endian = is_big_endian

    def wait_for_srq(self, timeout=25, delay=0.1):
        """ Blocks until a SRQ, and leaves the bit high

        :param timeout: Timeout duration in seconds
        :param delay: Time delay between checking SRQ in seconds
        """
        self.connection.wait_for_srq(timeout * 1000)
