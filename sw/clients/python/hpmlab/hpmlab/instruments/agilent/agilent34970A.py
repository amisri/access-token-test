#
# This file is part of the PyMeasure package.
#
# Copyright (c) 2013-2017 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import re
import enum
import time

from hpmlab.instruments import Instrument
from hpmlab.instruments.constants import toggle


class Agilent34970A(Instrument):
    """
    Wrapper for Agilent 34970 Data Acquisition/Switch Unit.

    Implemented measurements: voltage_dc, voltage_ac, current_dc,
    current_ac, resistance, resistance_4w

    >>> import hpmlab.instruments as insts
    >>> x = insts.agilent.Agilent34970A('GPIB::10')
    >>> x.channels = [202, 203]
    >>> x.mode = x.Mode.voltage_dc
    >>> x.measure()
    or:
    >>> x.measure([201, 202], x.Mode.voltage_dc)

    """

    def __init__(self, adapter, **kwargs):
        super().__init__(adapter, "Agilent 34970 Data Acquisition/Switch Unit", **kwargs)

    class Mode(enum.Enum):

        """
        Modes available to the DAQ
        """
        current_ac = 'CURR:AC'
        current_dc = 'CURR:DC'
        frequency = 'FREQ'
        fourpt_resistance = 'FRES'
        period = 'PER'
        resistance = 'RES'
        temperature = 'TEMP'
        voltage_ac = 'VOLT:AC'
        voltage_dc = 'VOLT:DC'

    class TimeFormat(enum.Enum):
        relative = 'REL'
        absolute = 'ABS'

    class Zero(enum.Enum):

        off = "OFF"
        on = "ON"
        once = "ONCE"

    @property
    def mode(self):
        response = self.query("SENS:FUNC?")
        regex_find = re.search(r'\"(.[^,]+)\"', response, re.M)
        if regex_find.group(1) == 'VOLT':
            return self.Mode.voltage_dc
        elif regex_find.group(1) == 'CURR':
            return self.Mode.current_dc
        else:
            return self.Mode(regex_find.group(1))

    @mode.setter
    def mode(self, mode_daq: Mode):
        """Sets the mode of the channels in the DAQ.
        Every time that this function is called it will replace the previous channel list.

        :param mode_daq: a tuple with the Mode enum and a list of channels to be defined
        :return: None
        """
        self.write(f"SENS:FUNC \"{mode_daq.value}\"")

    @property
    def channels(self) -> list:
        """

        :return:
        """
        response = self.query('ROUT:SCAN?')
        regex_find = re.search(r'@(.*)\)', response, re.M)
        return [int(x) for x in regex_find.group(1).split(',')]

    @channels.setter
    def channels(self, channel_list) -> None:
        """

        :param channel_list:
        :return:
        """
        self.write('ROUT:SCAN (@{})'.format(','.join(str(x) for x in channel_list)))

    @property
    def read_channel(self):
        return self.query('FORMat:READing:CHANnel?')

    @read_channel.setter
    def read_channel(self, toggle_value: bool):
        self.write(f"FORMat:READing:CHANnel {toggle[toggle_value]}")

    @property
    def read_time(self):
        return self.query('FORMat:READing:TIME?')

    @read_time.setter
    def read_time(self, toggle_value: bool):
        self.write(f"FORMat:READing:TIME {toggle[toggle_value]}")

    @property
    def read_unit(self):
        return self.query("FORMat:READ:UNIT?")

    @read_unit.setter
    def read_unit(self, toggle_value: bool):
        self.write(f"FORMat:READ:UNIT {toggle[toggle_value]}")

    @property
    def read_time_format(self):
        return self.query('FORMat:READ:TIME:TYPE?')

    @read_time_format.setter
    def read_time_format(self, time_format: TimeFormat):
        self.write('FORMat:READ:TIME:TYPE {}'.format(time_format.value))

    def clear_all_channels(self):
        self.channels = []

    @property
    def trigger_count(self):
        """
        This command specifies the number of times to sweep through the scan list.

        """
        return self.query('TRIG:COUN?')

    @trigger_count.setter
    def trigger_count(self, count):
        self.write(f"TRIG:COUN {count}")

    @property
    def autozero(self):
        return self.query("SENS:ZERO:AUTO?")

    @autozero.setter
    def autozero(self, azero_value: Zero):
        if isinstance(azero_value, self.Zero):
            self.write(f"SENS:ZERO:AUTO {azero_value.value}")
        else:
            raise ValueError('toggle must be an instance of Zero')

    @property
    def nplc(self):
        return self.query(f'SENS:{self.mode.value}:NPLC?')

    @nplc.setter
    def nplc(self, nplc_value: float):
        self.write(f'SENS:{self.mode.value}:NPLC {nplc_value}')

    @property
    def aperture(self):
        return self.query(f'SENS:{self.mode.value}:APER?')

    @aperture.setter
    def aperture(self, aperture_value: float):
        self.write(f'SENS:{self.mode.value}:APER {aperture_value}')

    @property
    def range(self):
        return self.query(f'{self.mode.value}:RANG?')

    @range.setter
    def range(self, range_value: float):
        self.write(f'{self.mode.value}:RANG {range_value}')

    @property
    def resolution(self):
        return self.query(f'{self.mode.value}:RES?')

    @resolution.setter
    def resolution(self, resolution_value: float):
        self.write(f'{self.mode.value}:RES {resolution_value}')

    def initiate(self):
        """
        Change the state of the triggering system from the "idle" state to the
        "wait-for-trigger" state.
        Scanning will begin when the specified trigger conditions are satisfied
        following the receipt of the INITiate command. Readings are stored in
        the instrument's internal reading memory and using this command will
        clear the previous set of readings from memory.
        """
        self.write('INITiate')

    def fetch(self) -> str:
        """
        This command transfers readings stored in non-volatile memory to the
        instrument's output buffer.
        The readings stored in memory are not erased when you use this method.
        The format of the readings can be changed using FORMat:READing commands.

        :return: Instrument readings in the established format.
        """
        return self.query('FETCh?')

    def read_measurements(self):
        """
        Switch device from "idle" state to "wait-for-trigger" state and begin
        measurements when the specified trigger conditions are satisfied. These
        readings are sent to the instrument's output buffer.
        Though, these readings are not stored on memory.
        """
        return self.query('READ?')

    def measure(self, channels=None, mode=None, to_wait: float = 0):
        """
        Do a measurement from the current mode or with it given as an optional argument.

        :param config: Configuration object for the DAQ.
        :param to_wait: Time in seconds to wait for the execution of the measurements.
        :return:
        """
        if channels and isinstance(channels, list):
            self.channels = channels

        if mode and isinstance(mode, self.Mode):
            self.mode = mode

        self.initiate()
        time.sleep(to_wait)
        results = self.fetch()
        results = [float(x) for x in results[:-1].split(',')]

        return results
