#
# This file is part of the PyMeasure package.
#
# Copyright (c) 2013-2017 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import enum

from hpmlab.instruments import Instrument


class Agilent34410A(Instrument):
    """
    Represent the HP/Agilent/Keysight 34410A and related multimeters.

    Implemented measurements: voltage_dc, voltage_ac, current_dc, current_ac,
    resistance, resistance_4w
    """
    # Only the most simple functions are implemented
    voltage_dc = Instrument.measurement("MEAS:VOLT:DC? DEF,DEF", "DC voltage, in Volts")

    voltage_ac = Instrument.measurement("MEAS:VOLT:AC? DEF,DEF", "AC voltage, in Volts")

    current_dc = Instrument.measurement("MEAS:CURR:DC? DEF,DEF", "DC current, in Amps")

    current_ac = Instrument.measurement("MEAS:CURR:AC? DEF,DEF", "AC current, in Amps")

    resistance = Instrument.measurement("MEAS:RES? DEF,DEF", "Resistance, in Ohms")

    resistance_4w = Instrument.measurement(
        "MEAS:FRES? DEF,DEF",
        "Four-wires (remote sensing) resistance, in Ohms")

    def __init__(self, adapter, **kwargs):
        super().__init__(adapter, "HP/Agilent/Keysight 34410A Multimiter", **kwargs)

    class Mode(enum.Enum):

        continuity = "CONT"
        current_ac = "CURR:AC"
        current_dc = "CURR:DC"
        diode = "DIOD"
        frequency = "FREQ"
        fourpt_resistance = "FRES"
        period = "PER"
        resistance = "RES"
        voltage_ac = "VOLT:AC"
        voltage_dc = "VOLT:DC"
        voltage_dc_ratio = "VOLT:DC:RAT"

    class Aperture(enum.Enum):

        short = "0.01"
        medium = "0.1"
        large = "1"
        min = short
        max = large

    class Range(enum.Enum):

        minimum = "MIN"
        maximum = "MAX"
        default = "DEF"
        automatic = "AUTO"

    class Resolution(enum.Enum):

        minimum = "MIN"
        maximum = "MAX"
        default = "DEF"

    class NPLCycles(enum.Enum):

        very_short = "0.02"
        short = "0.2"
        medium = "1"
        large = "10"
        very_large = "100"
        min = very_short
        max = very_large

    class Zero(enum.Enum):

        off = "OFF"
        on = "ON"
        once = "ONCE"

    @property
    def mode(self):
        response = self._clean_response(self.query('CONF?'))
        mode_value = response.split(" ")[0]
        if mode_value == 'VOLT':
            return self.Mode.voltage_dc
        elif mode_value == 'CURR':
            return self.Mode.current_dc
        else:
            return self.Mode(mode_value)

    @mode.setter
    def mode(self, mode_value: Mode):
        self.write(f'CONF:{mode_value.value}')

    @property
    def range(self):
        response = self._clean_response(self.query('CONF?'))
        return response.split(" ")[1].split(",")[0]

    @range.setter
    def range(self, range_value: [float, Range]):
        if isinstance(range_value, self.Range):
            self.write(f'{self.mode.value}:RANGe {range_value.value}')
        else:
            self.write(f'{self.mode.value}:RANGe {range_value}')

    @property
    def resolution(self):
        response = self._clean_response(self.query('CONF?'))
        return response.split(" ")[1].split(",")[1]

    @resolution.setter
    def resolution(self, resolution_value: [float, Resolution]):
        # TODO There could be a bug here introduced by RES
        if isinstance(resolution_value, self.Resolution):
            self.write(f'SENS:{self.mode.value}:RES {resolution_value.value}')
        else:
            self.write(f'SENS:{self.mode.value}:RES {resolution_value}')

    @property
    def nplc(self):
        return self.query(f'SENS:{self.mode.value}:NPLC?')

    @nplc.setter
    def nplc(self, nplc_value: [float, NPLCycles]):
        if isinstance(nplc_value, self.NPLCycles):
            self.write(f'SENS:{self.mode.value}:NPLC {nplc_value.value}')
        else:
            self.write(f'SENS:{self.mode.value}:NPLC {nplc_value}')

    @property
    def aperture(self):
        return self.query(f'SENS:{self.mode.value}:APER?')

    @aperture.setter
    def aperture(self, aperture_value: [float, Aperture]):
        if isinstance(aperture_value, self.Aperture):
            self.write(f'SENS:{self.mode.value}:APER {aperture_value.value}')
        else:
            self.write(f'SENS:{self.mode.value}:APER {aperture_value}')

    @property
    def autozero(self):
        return self.query("SENS:ZERO:AUTO?")

    @autozero.setter
    def autozero(self, azero_value: Zero):
        if isinstance(azero_value, self.Zero):
            self.write(f"SENS:ZERO:AUTO {azero_value.value}")
        else:
            raise ValueError('toggle must be an instance of Zero')

    @property
    def sample_count(self):
        """
        Number of readings (samples) that the multimeter will
        take per trigger event.

        Note that if the trigger_count property has been changed, the number
        of readings taken total will be a multiplication of sample count and
        trigger count.
        """
        return self.query('SAMP:COUN?')

    @sample_count.setter
    def sample_count(self, samples):
        """

        :param samples: min = 1. max = 50000
        :return: None
        """
        self.write(f"SAMP:COUN {samples}")

    @property
    def trigger_count(self):
        """
        Number of triggers that the multimeter will accept before
        returning to an "idle" trigger state.

        """
        return self.query('TRIG:COUN?')

    @trigger_count.setter
    def trigger_count(self, count):
        self.write(f"TRIG:COUN {count}")

    def delete_nvmem(self):
        """
        Delete data on instrument's non-volatile memory.

        :return:
        """
        self.write('DATA:DEL NVMEM')

    def initiate(self):
        """
        Change the state of the triggering system from the "idle" state to the
        "wait-for-trigger" state.
        Scanning will begin when the specified trigger conditions are satisfied
        following the receipt of the INITiate command.
        Readings are stored in the instrument's internal reading memory and
        using this command will clear the previous set of readings from memory.
        """
        self.write('INITiate')

    def fetch(self) -> str:
        """
        This command transfers readings stored in non-volatile memory to the
        instrument's output buffer.
        The readings stored in memory are not erased when you use this method.
        The format of the readings can be changed using FORMat:READing commands.

        :return: Instrument readings in the established format.
        """
        samples_string = self.query('FETCh?')
        return [float(s) for s in samples_string.split(",")]
        
    def read_measurements(self):
        """
        Switch device from "idle" state to "wait-for-trigger" state and begin
        measurements when the specified trigger conditions are satisfied. These
        readings are sent to the instrument's output buffer.
        Though, these readings are not stored on memory.
        """
        return self.query('READ?')

    def measure(self, mode=None):

        if not mode:
            return float(self.read_measurements())

        if not isinstance(mode, Agilent34410A.Mode):
            raise TypeError("Mode must be specified as a Agilent34410A.Mode value.")
        else:
            return float(self.query('MEAS:{}?'.format(mode.value)))

    @staticmethod
    def _clean_response(response):
        return response.replace('\"', '').replace('\n', '')
