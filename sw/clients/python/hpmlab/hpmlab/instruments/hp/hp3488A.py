import enum

from hpmlab.instruments import Instrument
from hpmlab.instruments.constants import toggle


class HP3488A(Instrument):
    """Wrapper for Agilent 3488A Switch/Control unit.


    >>> import hpmlab.instruments as insts
    >>> x = insts.hp.HP3488A('GPIB::10')
    """
    def __init__(self, adapter, **kwargs):
        super().__init__(adapter, "HP 3488A Switch/Control Unit", **kwargs)

    class ChannelState(enum.Enum):
        open = 'OPEN   1'
        closed = 'CLOSED   0'

    @property
    def id(self):
        """Requests and returns the identification of the instrument."""
        return self.query("ID?").replace('\r\n', '')

    def view_channel(self, channel: int):
        """Shows the state, either open or closed of a particular channel or a DIO
        bit.

        :param channel: int with the format snn - s is the slot number and nn is the
        channel number
        :return: ChannelState enum
        """
        q = self.query(f'VIEW {channel}').replace('\r\n', '').split(' ')
        if q[0] == "OPEN":
            return self.ChannelState.open
        elif q[0] == "CLOSED":
            return self.ChannelState.closed
        else:
            raise Exception("Channel is not in OPEN/CLOSED state")

    def monitor(self, card: int):
        """Monitor the individual card (module) in the specified slot.

        :param card:
        :return:
        """
        self.write(f"CMON {card}")

    def card_type(self, slot):
        """

        :param slot: number from 0 to 9
        :return: string containing the module identification in the specified slot.
        """

        return self.query(f'CTYPE {slot}').replace('\r\n', '')

    @property
    def delay(self):
        """Delay between the time that a channel is closed and the
        time that the next command or Channel Closed pulse is executed.

        :return: float
        """
        return self.query('DELAY')

    @delay.setter
    def delay(self, time: float):
        """Inserts a time delay between the time that a channel is closed and the \
        time that the next command or Channel Closed pulse is executed.
        The delay time may be a value between 0 and 32,767 mS (32 seconds) in \
        1 mS increments.
        The delay time does not become effective until either a CHAN or a STEP is \
        executed.

        :param time: time in ms
        """
        self.write(f'DELAY {time}')

    def step(self):
        """Opens the last channel closed and close the next channel in the Scan
        List. If STEP is executed and no Scan List exists, an error will be generated.
        A pointer is used to keep track of which channel in the Scan
        List is currently closed. When STEP is executed, that channel is opened
        and the next item in the list is checked. If the next item in the list is a
        relay or a digital I/O line, that channel/bit is closed. If the next item in
        the list is a stored channel setup, that channel setup is recalled. If the
        next item in the list is a stop channel 0, the last channel/bit closed will be
        open without closing any channel/bit.
        Channels that are closed by a channel setup will remain closed; they are
        not opened by the next execution of the STEP command.

        :return:
        """
        self.write("STEP")

    def lock(self, state: bool):
        """Locking out the keyboard prevents the keyboard from being scanned
        thus permitting faster operation.

        :return:
        """
        self.write(f"LOCK {toggle[state]}")

    def reset(self):
        """Identifies all plug-in modules and resets them (all relays open)
        Sets all parameters of the instrument to their default conditions.
        *Important:* If the front panel RESET key is pressed, interface
        functions are reset.

        """
        self.write("RESET")

    def scan_list(self, channels: [list, int]):
        """Specifies a sequence of channels to be scanned. The sequence is specified
        as a list of up to 85 channel addresses (relays or digital I/O lines).
        step() is used to sequentially close channels in the list.
        The commands open() or close() will have no effect on
        the scan list even though the states of individual channels change.

        :param channels:
        :return:
        """
        if isinstance(channels, int):
            self.write(f"SLIST {channels}")
        elif isinstance(channels, list):
            self.write(f"SLIST {','.join(str(x) for x in channels)}")
        else:
            raise ValueError('channels must be either a int or a list of int')

    def open(self, channels: [list, int]):
        """Open one or multiple channels/bits on the plug-in modules.
        The channel_address has the form @snn, where s is the slot number and
        nn is the channel number. For all mainframes, slot 0 refers to the 3499A/
        B/C control board.
        Several channels/bits can be opened with one OPEN command by
        separating the channel/bit addresses with a comma. If more than one
        channel/bit is specified, they are opened in the order listed. Use CARD
        RESET command to open all channels/bits on the modules, it is easier
        than listing each channel/bit.
        All the channels in the plug-in modules are opened following a reset. For
        the DIO modules, all bits are operating as input bits. There is no
        handshaking enabled.

        :param channels:
        :return:
        """
        if isinstance(channels, int):
            self.write(f"OPEN {channels}")
        elif isinstance(channels, list):
            self.write(f"OPEN {','.join(str(x) for x in channels)}")
        else:
            raise ValueError('channels must be either a int or a list of int')

    def close(self, channels: [list, int]):
        """Close one or more channels on the non digital I/O (DIO) modules. It can
        also be used to “clear” bits on the digital I/O modules. The CLOSE
        command does not open any channels/bits that were previously closed.

        :param channels:
        :return:
        """
        if isinstance(channels, int):
            self.write(f"CLOSE {channels}")
        elif isinstance(channels, list):
            self.write(f"CLOSE {','.join(str(x) for x in channels)}")
        else:
            raise ValueError('channels must be either a int or a list of int')

    def render_text(self, text: str):
        self.write(f'DISP \"{text}\"')

    def clear_text(self):
        self.render_text('')

    @property
    def display(self):
        raise NotImplementedError

    @display.setter
    def display(self, state: bool):
        if state:
            self.write('DON')
        else:
            self.write('DOFF')

    def status(self):
        status_codes = {
            1: 'End of scan sequence',
            2: 'Output available',
            4: 'Power-on SRQ asserted',
            8: 'Front panel SRQ key pressed',
            16: 'Ready for instructions',
            32: 'Error',
            64: 'RQS',
            128: 'Not used'
        }

        status_message = int(self.query('STATUS'))

        return {x: status_codes[x] for x in status_codes if status_message & x}

    def check_errors(self) -> dict:
        error_codes = {
            1: 'Syntax Error',
            2: 'Execution Error, cause can be either: Parameter out of range / '
               'Card type mismatch / Attempt to access a nonexistent stored '
               'state or scan list',
            4: 'Hardware Trigger too fast',
            8: 'Logical Failure',
            16: 'Power Supply Failure'
        }

        errors = int(self.query('ERROR'))

        return {x: error_codes[x] for x in error_codes if errors & x}

    @property
    def is_powered_on(self):
        raise NotImplementedError

    @property
    def opc(self):
        raise NotImplementedError

    def wait(self):
        raise NotImplementedError

    def clear(self):
        raise NotImplementedError

    @property
    def beeper(self):
        raise NotImplementedError

    def beep(self):
        raise NotImplementedError
