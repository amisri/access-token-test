import enum

from hpmlab.instruments import Instrument


toggle = {
    True: 1,
    False: 0
}


class HP3458A(Instrument):
    """Driver for the HP3458a Digital Voltmeter. Usage:

    >>> import hpmlab.instruments as insts
    >>> d = insts.hp.HP3458A("GPIB0::23")
    >>> # Easy:
    >>> d.current_dc
    >>> # Configurable:
    >>> d.mode = d.Mode.current_dc
    >>> d.nplc = 0
    >>> print(d.measure())

    """
    def __init__(self, resource_name, **kwargs):
        super().__init__(
            resource_name, "HP3458A",
            read_termination="\r",
            **kwargs)
        self._trigger_mode = self.TriggerMode.single

    class Mode(enum.Enum):
        current_ac = 7
        current_dc = 6
        current_acdc = 8

        voltage_ac = 2
        voltage_dc = 1
        voltage_acdc = 3

        frequency = 9
        period = 10

        fourpt_resistance = 5
        resistance = 4

        direct_sampling_ac = 11
        direct_sampling_dc = 12

        sub_sampling_ac = 13
        sub_sampling_dc = 14

    class TriggerMode(enum.Enum):
        # Used with: TARM, TRIG, NRDGS
        # Occurs automatically (whenever required)
        auto = 1

        # Occurs on negative edge transition on the multimeter's external trigger input
        external = 2

        # Occurs when the multimeter's output buffer is empty, reading memory is off or empty,
        # and the controller requests data
        syn = 5

        # Used with: TARM, TRIG
        # Occurs once (upon receipt of TARM SGL or TRIG SGL command, then becomes HOLD)
        single = 3

        # Suspends measurements
        hold = 4

        # Used with: TRIG, NRDGS
        # Occurs when the power line voltage crosses zero volts
        level = 7

        # Occurs when the specified voltage is reached on the specified slope of the input signa
        line = 8

    class FormatMode(enum.Enum):
        #  ASCII-15 bytes per reading (see 1st and 2nd Remarks below)
        ascii = 1

        # Single Integer-16 bits 2's complement (2 bytes per reading)
        sint = 2

        # Double Integer-32 bits 2's complement (4 bytes per reading)
        dint = 3

        # Single Real-(IEEE-754) 32 bits, (4 bytes per reading)
        sreal = 4

        # Double Real-(IEEE-754) 64 bits, (8 bytes per reading)
        dreal = 5

    # The following properties are defined for the default behavior of the
    # multimeter.

    @property
    def voltage_dc(self):
        return self.measure(self.Mode.voltage_dc)

    @property
    def current_dc(self):
        return self.measure(self.Mode.current_dc)

    @property
    def voltage_ac(self):
        return self.measure(self.Mode.voltage_ac)

    @property
    def current_ac(self):
        return self.measure(self.Mode.current_ac)

    @property
    def resistance(self):
        return self.measure(self.Mode.resistance)

    @property
    def resistance_4w(self):
        return self.measure(self.Mode.fourpt_resistance)

    # From here, there are the configurable properties and
    # actuation methods

    @property
    def line(self):
        return self.query("LINE?")

    @property
    def trigger_mode(self):
        return self._trigger_mode

    @trigger_mode.setter
    def trigger_mode(self, mode: TriggerMode):
        self._trigger_mode = mode

    @property
    def range(self):
        return self.query("RANGE?")

    @range.setter
    def range(self, range_value):
        self.write(f"RANGE {range_value}")

    @property
    def mode(self):
        return self.Mode(int(self.query("FUNC?").split(",")[0]))

    @mode.setter
    def mode(self, measurement_mode: Mode):
        self.write(f"FUNC {measurement_mode.value}")

    @property
    def nplc(self):
        """Gets the A/D converter's integration time in
        terms of power line cycles. Integration time is the time
        during which the A/D converter measures the input signal.

        """
        return float(self.query("NPLC?"))

    @nplc.setter
    def nplc(self, nplc_value: float):
        """Since the APER and NPLC commands both set the integration time,
        executing either will cancel the integration time previously
        established by the other.

        :param nplc_value:
        :return: None
        """
        self.write(f"NPLC {nplc_value}")

    @property
    def aperture(self):
        """Gets the A/D converter integration time in seconds."""
        return float(self.query("APER?"))

    @aperture.setter
    def aperture(self, aperture_value: float):
        """Specifies the A/D converter's integration time and
         overrides any previously specified integration time or resolution.
         Since the APER and NPLC commands both set the integration time,
        executing either will cancel the integration time previously
        established by the other.

        :param aperture_value: The valid range for aperture is 0 - 1 s in increments of 100 ns.
        (Specifying a value <500 ns selects minimum aperture which is 500 ns.)
        :return: None
        """
        self.write(f"APER {aperture_value}")

    @property
    def delay(self):
        """The DELAY command allows you to specify a time interval
        that is inserted between the trigger event and the first sample event.

        :return: delay in seconds
        """
        return float(self.query("DELAY?"))

    @delay.setter
    def delay(self, delay_value: float):
        """Specifies the delay time in seconds. Delay time can range from
        1E-7 (100 ns) to 6000 seconds in 10 ns increments for direct- or
        sub-sampling (DSAC, DSDC, SSAC, or SSDC) or 100 ns increments for
        all other measurement functions.
        Specifying 0 for the delay sets the delay to its minimum possible value.

        :param delay_value:
        :return: None
        """
        self.write(f"DELAY {delay_value}")

    @property
    def display(self):
        response = int(self.query("DISP?"))
        return response == 1

    @display.setter
    def display(self, state: bool):
        self.write(f"DISP {toggle[state]}")

    @property
    def arange(self):
        response = int(self.query("ARANGE?"))
        return response == 1

    @arange.setter
    def arange(self, state: bool):
        self.write(f"ARRANGE {toggle[state]}")

    @property
    def trigger_arm(self):
        return self.TriggerMode(int(self.query("TARM?")))

    @trigger_arm.setter
    def trigger_arm(self, trigger_arm_mode: TriggerMode):
        self.write(f"TARM {trigger_arm_mode.value}")

    @property
    def oformat(self):
        return self.FormatMode(int(self.query("OFORMAT?")))

    @oformat.setter
    def oformat(self, oformat_mode: FormatMode):
        self.write(f"OFORMAT {oformat_mode.value}")

    @property
    def mformat(self):
        return self.FormatMode(int(self.query("MFORMAT?")))

    @mformat.setter
    def mformat(self, mformat_mode: FormatMode):
        self.write(f"MFORMAT {mformat_mode.value}")

    @property
    def azero(self):
        """The autozero function applies only to DC voltage, DC current,
         and resistance measurements.

        :return: bool
        """
        response = int(self.query("AZERO?"))
        return response == 1

    @azero.setter
    def azero(self, state: bool):
        """When autozero is ON, the multimeter makes a zero measurement
        (measurement with the input disabled) following every reading and
        algebraically subtracts the zero measurement from the reading. This
        approximately doubles the time required per reading.

        :param state: on/off
        :return: None
        """
        self.write(f"AZERO {toggle[state]}")

    @property
    def nrdgs(self):
        """Designates the number of readings taken per trigger and
        the event (sample event) that initiates each reading. 

        """
        response = self.query("NRDGS?")

        # Get only the number of readings, discard trigger method used
        response = response.split(",")[0]

        # Convert back to an integer
        response = int(response)

        return response

    @nrdgs.setter
    def nrdgs(self, value: int):
        if value < 1 or value > 16777215:
            raise Exception("The valid range for this parameter is 1 to 16777215")
        
        self.write(f"NRDGS {value}")

    @property
    def timer(self):
        """Time interval for the TIMER sample event in the NRDGS command.
        When using the TIMER event, the time interval is inserted
        between readings.

        """
        return float(self.query("TIMER?"))

    @timer.setter
    def timer(self, value: float):
        self.write(f"TIMER {value}")

    @property
    def id(self):
        """Requests and returns the identification of the instrument. """
        return self.query("ID?")

    def trigger(self):
        self.write(f"TRIG {self.trigger_mode.value}")

    def sweep(self, period=None, samples=None):
        """Call DVM's "SWEEP PERIOD, SAMPLES" followed by a trigger.
        Then read all the acquired samples one by one.

        :return: generator 
        """
        if not period:
            period = self.timer
        
        if not samples:
            samples = self.nrdgs

        self.write(f"sweep {period}, {samples}")
        self.trigger()
        for _ in range(samples):
            yield float(self.read())

    def measure(self, mode=None, read_config=None):
        if mode:
            self.mode = mode

        if read_config:
            formatting = FORMAT_CONFIG[read_config]
        else:
            formatting = FORMAT_CONFIG[self.oformat]

        self.trigger()
        value = self.read(size=formatting[0], encoding=formatting[1])
        return float(value)

    def clear(self):
        """Clears the instrument status byte"""
        self.write("CLEAR")

    def reset(self):
        """Resets the instrument."""
        self.write("RESET")


FORMAT_CONFIG = {
    HP3458A.FormatMode.ascii: (-1, "utf-8"),
    HP3458A.FormatMode.sint: (2, "SI/16"),
    HP3458A.FormatMode.dint: (4, "DI/32"),
    HP3458A.FormatMode.sreal: (4, "IEEE-754/32"),
    HP3458A.FormatMode.dreal: (8, "IEEE-754/64"),
}
