#
# This file is part of the PyMeasure package.
#
# Copyright (c) 2013-2017 PyMeasure Developers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import re
import struct

from hpmlab.instruments.constants import toggle
from hpmlab.adapters.visa import VISAAdapter


class Instrument:
    """This provides the base class for all Instruments, which is
    independent of the particular Adapter used to connect for
    communication to the instrument. It provides basic SCPI commands
    by default, but can be toggled with :code:`includeSCPI`.

    :param adapter: An adapter object
    :param name: A string name
    """

    # noinspection PyPep8Naming
    def __init__(self, adapter, name, **kwargs):
        try:
            if isinstance(adapter, (int, str)):
                adapter = VISAAdapter(adapter, **kwargs)
        except ImportError:
            raise Exception("Invalid Adapter provided for Instrument since "
                            "PyVISA is not present")

        self.name = name
        self.adapter = adapter

    @property
    def timeout(self):
        """

        :return: the timeout in milliseconds
        """
        return self.adapter.connection.timeout

    @timeout.setter
    def timeout(self, value):
        self.adapter.connection.timeout = value

    # Wrapper functions for the Adapter object
    def query(self, command):
        """Writes the command to the instrument through the adapter
        and returns the read response.

        :param command: command string to be sent to the instrument
        """
        return self.adapter.query(command)

    def write(self, command):
        """Writes the command to the instrument through the adapter.

        :param command: command string to be sent to the instrument
        """
        self.adapter.write(command)

    def read(self, size=-1, encoding='utf-8'):
        """Reads from the instrument through the adapter and returns the response.

        :param size: number of bytes that will be read from the buffer. If -1,\
        it will read until termination character is found.
        :param encoding: Number encoding of the response.
        :return: UTF-8 response from the instrument.
        """
        if size == -1:
            msg = self.adapter.read()
        else:
            msg = self.adapter.read_bytes(size)

        encoders = {
            'SI/16': '>h',
            'DI/32': '>l',
            'IEEE-754/32': '>f',
            'IEEE-754/64': '>d'
        }

        if encoding in encoders:
            msg = struct.unpack(encoders[encoding], msg)[0]

        elif encoding == 'utf-8':
            pass

        else:
            raise ValueError('Encoding {} is not implemented yet.'.format(encoding))

        return msg

    def values(self, command, **kwargs):
        """Reads a set of values from the instrument through the adapter,
        passing on any key-word arguments.
        """
        return self.adapter.values(command, **kwargs)

    @staticmethod
    def control(get_command, set_command, docs,
                validator=lambda v, vs: v, values=(), map_values=False,
                get_process=lambda v: v, set_process=lambda v: v,
                check_set_errors=False, check_get_errors=False,
                **kwargs):
        """Returns a property for the class based on the supplied
        commands. This property may be set and read from the
        instrument.

        :param get_command: A string command that asks for the value
        :param set_command: A string command that writes the value
        :param docs: A docstring that will be included in the documentation
        :param validator: A function that takes both a value and a group of valid values
                          and returns a valid value, while it otherwise raises an exception
        :param values: A list, tuple, range, or dictionary of valid values, that can be used
                       as to map values if :code:`map_values` is True.
        :param map_values: A boolean flag that determines if the values should be
                          interpreted as a map
        :param get_process: A function that take a value and allows processing
                            before value mapping, returning the processed value
        :param set_process: A function that takes a value and allows processing
                            before value mapping, returning the processed value
        :param check_set_errors: Toggles checking errors after setting
        :param check_get_errors: Toggles checking errors after getting
        """

        if map_values and isinstance(values, dict):
            # Prepare the inverse values for performance
            inverse = {v: k for k, v in values.items()}

        def fget(self):
            vals = self.values(get_command, **kwargs)
            if check_get_errors:
                self.check_errors()
            if len(vals) == 1:
                value = get_process(vals[0])
                if not map_values:
                    return value
                elif isinstance(values, (list, tuple, range)):
                    return values[int(value)]
                elif isinstance(values, dict):
                    return inverse[value]
                else:
                    raise ValueError(
                        f"Values of type `{type(values)}` are not allowed "
                        "for Instrument.control"
                    )
            else:
                vals = get_process(vals)
                return vals

        def fset(self, value):
            value = set_process(validator(value, values))
            if not map_values:
                pass
            elif isinstance(values, (list, tuple, range)):
                value = values.index(value)
            elif isinstance(values, dict):
                value = values[value]
            else:
                raise ValueError(
                    'Values of type `{}` are not allowed '
                    'for Instrument.control'.format(type(values))
                )
            self.write(set_command % value)
            if check_set_errors:
                self.check_errors()

        # Add the specified document string to the getter
        fget.__doc__ = docs

        return property(fget, fset)

    @staticmethod
    def measurement(get_command, docs, values=(), map_values=None,
                    get_process=lambda v: v, command_process=lambda c: c,
                    check_get_errors=False, **kwargs):
        """ Returns a property for the class based on the supplied
        commands. This is a measurement quantity that may only be
        read from the instrument, not set.

        :param get_command: A string command that asks for the value
        :param docs: A docstring that will be included in the documentation
        :param values: A list, tuple, range, or dictionary of valid values, that can be used
                       as to map values if :code:`map_values` is True.
        :param map_values: A boolean flag that determines if the values should be
                          interpreted as a map
        :param get_process: A function that take a value and allows processing
                            before value mapping, returning the processed value
        :param command_process: A function that take a command and allows processing
                            before executing the command, for both getting and setting
        :param check_get_errors: Toggles checking errors after getting
        """

        if map_values and isinstance(values, dict):
            # Prepare the inverse values for performance
            inverse = {v: k for k, v in values.items()}

        def fget(self):
            vals = self.values(command_process(get_command), **kwargs)
            if check_get_errors:
                self.check_errors()
            if len(vals) == 1:
                value = get_process(vals[0])
                if not map_values:
                    return value
                elif isinstance(values, (list, tuple, range)):
                    return values[int(value)]
                elif isinstance(values, dict):
                    return inverse[value]
                else:
                    raise ValueError(
                        'Values of type `{}` are not allowed '
                        'for Instrument.measurement'.format(type(values))
                    )
            else:
                return get_process(vals)

        # Add the specified document string to the getter
        fget.__doc__ = docs

        return property(fget)

    @property
    def id(self):
        """Requests and returns the identification of the instrument."""
        return self.query("*IDN?").strip()

    @property
    def is_powered_on(self):
        """Gets the power status of the instrument.

        :return:
        """
        return self.query("*PSC?")

    @property
    def opc(self):
        """
        This places a 1 in the Output Queue when all pending operations have completed.
        Because it requires your program to read the returned value before executing the next
        program statement, *OPC? can be used to cause the controller to wait for commands
        to complete before proceeding with its program.

        """
        return self.query("*OPC?")

    @opc.setter
    def opc(self, value: bool):
        """
        Pass

        :param value:
        :return:
        """
        if value:
            self.write("*OPC")
        else:
            raise ValueError('There is no defined behavior for {}'.format(value))

    def wait(self):
        """This prevents the isntrument from processing subsequent commands
        until all pending operations are completed.

        :return:
        """
        self.write("*WAI")

    def clear(self):
        """The status registers, the error queue, and all configuration states
        are left unchanged when a device clear message is received. Though, the
        input/output buffers of the instrument are cleared;
        and the instrument is prepared to accept a new command string.
        """
        self.write("*CLS")

    def reset(self):
        """Resets the instrument."""
        self.write("*RST")

    def abort(self):
        """Abort instrument current directive. Behavior can change from
        instrument to instrument.
        """
        self.write("ABOR")

    def check_errors(self) -> dict:
        """Fetch and clear all the errors from the instrument's error queue.

        :return: Dictionary with error codes as keys and error messages as its values.
        """
        error_dict = dict()
        while True:
            result_string = self.query("SYSTem:ERRor?")
            error_code = int(result_string.split(',')[0])
            error_message = result_string.split('\"')[1]
            if error_code == 0:
                break
            else:
                error_dict[error_code] = error_message
        return error_dict

    @property
    def display(self):
        return self.query("DISP?")

    @display.setter
    def display(self, state: bool):
        self.write(f"DISP {toggle[state]}")

    @property
    def beeper(self):
        return self.query("SYST:BEEP?")

    @beeper.setter
    def beeper(self, state: bool):
        self.write(f"SYST:BEEP:STAT {toggle[state]}")

    def render_text(self, text: str):
        self.write(f"DISP:TEXT \"{text}\"")

    def clear_text(self):
        self.write("DISP:TEXT:CLE")

    def beep(self):
        self.write("SYST:BEEP")
