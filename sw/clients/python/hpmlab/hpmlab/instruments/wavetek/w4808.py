import enum

from hpmlab.instruments import Instrument


toggle = {
    True: 1,
    False: 0
}

class Wavetek4808(Instrument):
    """
    Wrapper for Wavetek 4808 multifunction calibrator


    >>> import hpmlab.instruments as instruments
    >>> x = instruments.wavetek.Wavetek4808('GPIB::10')
    >>> x.mode = x.Mode.voltage_dc
    >>> x.range = x.InputRange.auto_range
    >>> x.output = True
    """

    def __init__(self, adapter, **kwargs):
        super().__init__(adapter, "Wavetek 4808 multifunction calibrator", **kwargs)

    class Mode(enum.Enum):
        voltage_dc = 0
        voltage_ac = 1
        current_dc = 2
        current_ac = 3
        resistance = 4

    class InputRange(enum.Enum):
        auto_range = 0
        micro_100 = 1
        milli_1 = 2
        milli_10 = 3
        milli_100 = 4
        base_1 = 5
        base_10 = 6
        base_100 = 7
        base_1000 = 8

    def write(self, command):
        terminator = "="
        super().write(command + terminator)

    @property
    def mode(self):
        raise NotImplementedError('This instrument does not allow to read this property.')

    @mode.setter
    def mode(self, mode_enum: Mode):
        if isinstance(mode_enum, self.Mode):
            self.write(f"F{mode_enum.value}")
        else:
            raise ValueError(f"Argument value must be an instance of {self.Mode}")

    @property
    def range(self):
        raise NotImplementedError('This instrument does not allow to read this property.')

    @range.setter
    def range(self, range_enum: InputRange):
        if isinstance(range_enum, self.InputRange):
            self.write(f"R{range_enum.value}")
        else:
            raise ValueError(f"Argument value must be an instance of {self.InputRange}")

    @property
    def output(self):
        raise NotImplementedError('This instrument does not allow to read this property.')

    @output.setter
    def output(self, state: bool):
        self.write(f"O{toggle[state]}")

    @property
    def numeric_output(self):
        raise NotImplementedError('This instrument does not allow to read this property.')

    @numeric_output.setter
    def numeric_output(self, numeric_value: float):
        if isinstance(numeric_value, float):
            self.write(f"M{numeric_value}")
        else:
            raise ValueError(f"Argument value must be an instance of float")

    @property
    def frequency(self):
        raise NotImplementedError('This instrument does not allow to read this property.')

    @frequency.setter
    def frequency(self, freq_value: float):
        if isinstance(freq_value, float):
            self.write(f"H{freq_value}")
        else:
            raise ValueError(f"Argument value must be an instance of float")

    @property
    def safety_delay(self):
        raise NotImplementedError('This instrument does not allow to read this property.')

    @safety_delay.setter
    def safety_delay(self, state: bool):
        self.write(f"D{toggle[state]}")
