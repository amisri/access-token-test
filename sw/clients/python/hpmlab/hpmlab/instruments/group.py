import pyvisa


class Group:
    
    def __init__(self, *args, name="GPIB0", **kwargs):
        """

        >>> import hpmlab.instruments as insts
        >>> d = insts.Group(dvm1, dvm2)
        >>> d.write("PRESET NORM")
        >>> d.measure()
        """
        rm = pyvisa.ResourceManager()
        try:
            self._bus = rm.open_resource(f"{name}::INTFC")
        except pyvisa.errors.VisaIOError:
            print(f"The following resources are available {rm.list_resources()}")
            raise
        self._devices = args

    def write(self, command):
        """Writes the command to all the instruments in the group.

        :param command: command string to be sent to the instrument
        """
        [x.write(command) for x in self._devices]

    def query(self, command):
        """Writes the command to the instrument through the adapter
        and returns the read response.

        :param command: command string to be sent to the instrument
        """
        return [x.query(command) for x in self._devices]

    def read(self, size=-1, encoding='utf-8'):
        return [x.read(size=size, encoding=encoding) for x in self._devices]

    def measure(self, **kwargs):
        self._bus.group_execute_trigger(*[x.adapter.connection for x in self._devices])
        return self.read(**kwargs)
