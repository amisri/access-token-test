import enum

from hpmlab.instruments import Instrument
from hpmlab.instruments.constants import toggle


class Keithley2182(Instrument):
    """
    Wrapper class for the keithley 2182 nanovoltmeter.
    Based on the wrapper from InstrumentKit library.

    >>> d = Keithley2182('GPIB0::23')
    >>> print(d.voltage_dc)
    >>> # or modify some fields and then do the measurements
    >>> d.mode = d.Mode.voltage
    >>> d.analog_filter = False
    >>> d.digital_filter = False
    >>> d.display = False
    >>> d.nplc = 0.01
    >>> print(d.measure())
    """
    voltage_dc = Instrument.measurement("MEAS:VOLT:DC? DEF,DEF", "DC voltage, in Volts")

    def __init__(self, adapter, **kwargs):
        super().__init__(adapter, "Keithley 2182 Nanovoltmeter", **kwargs)

    class Mode(enum.Enum):
        voltage = 'VOLT:DC'
        temperature = 'TEMP'

    class Channel(enum.Enum):
        internal_temp = 0
        dcv_1 = 1
        dcv_2 = 2

    @property
    def mode(self):
        return self.Mode(self.query('SENS:FUNC?').replace('\n', '').replace('\"', ''))

    @mode.setter
    def mode(self, mode_value: Mode):
        self.write(f"SENS:FUNC {mode_value.value}")

    @property
    def analog_filter(self):
        return self.query(f"{self.mode.value}:CHANnel{self.channel.value}:LPAS:STAT?")

    @analog_filter.setter
    def analog_filter(self, state: bool):
        self.write(f"{self.mode.value}:CHANnel{self.channel.value}:LPAS:STAT {toggle[state]}")

    @property
    def digital_filter(self):
        return self.query(f"{self.mode.value}:CHANnel{self.channel.value}:DFIL:STAT?")

    @digital_filter.setter
    def digital_filter(self, state: bool):
        self.write(f"{self.mode.value}:CHANnel{self.channel.value}:DFIL:STAT {toggle[state]}")

    @property
    def channel(self):
        return self.Channel(int(self.query('SENS:CHAN?')))

    @channel.setter
    def channel(self, channel_value: Channel):
        self.write(f"SENS:CHAN {channel_value.value}")

    @property
    def range(self):
        return self.query(f"{self.mode.value}:CHANnel{self.channel.value}:RANG?")

    @range.setter
    def range(self, range_value: int):
        self.write(f'{self.mode.value}:CHANnel{self.channel.value}:RANGe {range_value}')

    @property
    def autorange(self):
        return self.query(f"{self.mode.value}:CHANnel{self.channel.value}:RANG:AUTO?")

    @autorange.setter
    def autorange(self, state: bool):
        self.write(f"{self.mode.value}:CHANnel{self.channel.value}:RANG:AUTO {toggle[state]}")

    @property
    def nplc(self):
        return self.query(f'SENS:{self.mode.value}:NPLC?')

    @nplc.setter
    def nplc(self, nplc_value: float):
        self.write(f'SENS:{self.mode.value}:NPLC {nplc_value}')

    @property
    def autozero(self):
        return self.query("SYST:AZER:STAT?")

    @autozero.setter
    def autozero(self, state: bool):
        self.write(f"SYST:AZER:STAT {toggle[state]}")

    @property
    def aperture(self):
        return self.query(f'SENS:{self.mode.value}:APER?')

    @aperture.setter
    def aperture(self, aperture_value: float):
        self.write(f'SENS:{self.mode.value}:APER {aperture_value}')

    @property
    def sample_count(self):
        """Number of readings (samples) that the multimeter will
        take per trigger event.

        Note that if the trigger_count property has been changed, the number
        of readings taken total will be a multiplication of sample count and
        trigger count.
        """
        return self.query('SAMP:COUN?')

    @sample_count.setter
    def sample_count(self, samples):
        """

        :param samples: min = 1. max = 50000
        :return: None
        """
        self.write(f"SAMP:COUN {samples}")

    @property
    def trigger_count(self):
        """Number of triggers that the multimeter will accept before
        returning to an "idle" trigger state.

        """
        return self.query('TRIG:COUN?')

    @trigger_count.setter
    def trigger_count(self, count):
        self.write(f"TRIG:COUN {count}")

    def initiate(self):
        """Change the state of the triggering system from the "idle"
        state to the "wait-for-trigger" state.
        Scanning will begin when the specified trigger conditions
        are satisfied following the receipt of the INITiate
        command. Readings are stored in the instrument's internal
        reading memory and using this command will clear the
        previous set of readings from memory.
        """
        self.write('INITiate')

    def fetch(self) -> str:
        """This command transfers readings stored in non-volatile
        memory to the instrument's output buffer.
        The readings stored in memory are not erased when you use
        this method.
        The format of the readings can be changed using
        FORMat:READing commands.

        :return: Instrument readings in the established format.
        """
        return self.query('FETCh?')

    def fetch_fresh_data(self):
        return self.query('SENS:DATA:FRES?')

    def read_measurements(self):
        """Switch device from "idle" state to "wait-for-trigger" state and begin
        measurements when the specified trigger conditions are satisfied. These
        readings are sent to the instrument's output buffer.
        Though, these readings are not stored on memory.

        """
        return self.query('READ?')

    def measure(self, mode=None):
        """Do a measurement from the current mode or with it given as an optional argument.

        """
        if mode and isinstance(mode, self.Mode):
            self.mode = mode

        return self.read_measurements()
