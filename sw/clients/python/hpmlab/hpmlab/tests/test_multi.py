import pyvisa
import time
import pytest
import hpmlab.instruments as inst


pytest.skip("skipping manual tests", allow_module_level=True)

def test_one_meas():
    dvm = inst.hp.HP3458A(24)
    interface = inst.Group(dvm)
    assert interface.measure() != []

def test_nplc():
    dvm = inst.hp.HP3458A(24)
    g = inst.Group(dvm)
    g.write("nplc 2")
    assert dvm.nplc == 2

