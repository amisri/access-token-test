import hpmlab.instruments as instruments
import pytest


pytest.skip("skipping manual tests", allow_module_level=True)

def test_id():
    switch = instruments.hp.HP3488A('GPIB0::3')
    assert switch.id == 'HP3488A'

def test_error_checking():
    switch = instruments.hp.HP3488A('GPIB0::3')
    assert isinstance(switch.check_errors(), dict)

def test_status():
    switch = instruments.hp.HP3488A('GPIB0::3')
    assert isinstance(switch.status(), dict)

def test_view():
    switch = instruments.hp.HP3488A('GPIB0::3')
    assert isinstance(switch.view_channel(101), switch.ChannelState)

def test_display():
    switch = instruments.hp.HP3488A('GPIB0::3')
    switch.render_text('Hello')
    assert True

def test_errors():
    switch = instruments.hp.HP3488A('GPIB0::3')
    switch.card_type(0)
    assert 2 in switch.check_errors()
