import hpmlab.instruments as instruments
import pytest
import time


pytest.skip("skipping manual tests", allow_module_level=True)


# Invoked once per module
@pytest.fixture(scope="module")
def daq():
    return instruments.agilent.Agilent34970A('GPIB0::10')

def test_channels(daq):
    daq.channels = [202, 205, 206]
    assert daq.channels == [202, 205, 206]

def test_mode(daq):
    daq.channels = [221]
    daq.mode = daq.Mode.current_dc
    assert daq.mode == daq.Mode.current_dc

def test_measure(daq):
    daq.channels = [201,202]
    daq.mode = daq.Mode.voltage_dc
    data = daq.measure()
    assert isinstance(data, list) and len(data) == 2

def test_measure_easy(daq):
    data = daq.measure([201, 202], daq.Mode.voltage_dc)
    assert isinstance(data, list) and len(data) == 2