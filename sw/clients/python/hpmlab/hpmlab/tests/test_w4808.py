import pytest
import hpmlab.instruments.wavetek.w4808 as tek


pytest.skip("skipping manual tests", allow_module_level=True)

@pytest.fixture(scope="module")
def calibrator():
    return tek.Wavetek4808('GPIB0::2')

def test_mode(calibrator):
    calibrator.mode = calibrator.Mode.voltage_dc
    assert True
