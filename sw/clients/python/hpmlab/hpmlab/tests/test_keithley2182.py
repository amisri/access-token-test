import pytest
from hpmlab.instruments.keithley import Keithley2182


pytest.skip("skipping manual tests", allow_module_level=True)

# Invoked once per module
@pytest.fixture(scope="module")
def nanovoltmeter():
    return Keithley2182('GPIB0::23')

def test_easy_measurement(nanovoltmeter):
    assert isinstance(nanovoltmeter.voltage_dc, float)

def test_mode(nanovoltmeter):
    assert isinstance(nanovoltmeter.mode, nanovoltmeter.Mode)

def test_measurement(nanovoltmeter):
    x = nanovoltmeter.measure()
    assert True