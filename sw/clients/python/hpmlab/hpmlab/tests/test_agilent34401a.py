import hpmlab.instruments as instruments
import pytest


pytest.skip("skipping manual tests", allow_module_level=True)


# Invoked once per module
@pytest.fixture(scope="module")
def multimeter():
    return instruments.agilent.Agilent34410A('GPIB0::21')


def test_get_mode(multimeter):
    for mode in multimeter.Mode:
        assert isinstance(mode, multimeter.Mode)


def test_set_mode(multimeter):
    for mode in multimeter.Mode:
        multimeter.mode = mode
        assert isinstance(multimeter.mode, multimeter.Mode)


def test_range(multimeter):
    print(multimeter.range)
    assert True


def test_measure_noargs(multimeter):
    data = multimeter.measure()
    assert isinstance(data, float)


def test_measure(multimeter):
    data = multimeter.measure(mode=multimeter.Mode.current_dc)
    assert isinstance(data, float)