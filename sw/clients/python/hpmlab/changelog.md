Release History
===============
    

0.5.4 (2019-09-27)
--------------------- 
    - Add sweep function to HP3458A


0.5.3dev (2019-07-11)
---------------------
    - EPCCCS-7392: Add support for Group Execute Triggers
        - New class instrument.Group
    - Fix bug on adapter repr method


0.5.2 (2019-07-10)
------------------
    - EPCCCS-7375: Remove boilerplate code for HP3458A and set the default trig mode to single


0.5.1 (2019-07-05)
------------------
    - Fix import bug on visa adapter module.
    - Remove deprecated __version__ code from __init__