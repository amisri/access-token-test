.. hpmlab documentation master file, created by
   sphinx-quickstart on Tue Jul 17 16:22:36 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

hpmlab documentation
====================

Contact converter-controls-support@cern.ch for any question or bug report.

Other Libraries Developed By Us
###############################

* `pyfgc <https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/pyfgc/stable/>`_: library to communicate with FGC's via USB/TCP-IP
* `midas <https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/midas/stable/>`_: library to acquire data & visualize in PowerSpy


Contents:


.. toctree::
   :maxdepth: 1

   main/install.rst
   main/tutorial1.rst
   main/tutorial2.rst
   main/tutorial3.rst
   main/tutorial4.rst
   instruments/hpmlab.instruments.rst
