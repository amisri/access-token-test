How to install
==============

Remember to setup the acc-py private index (if you haven't done so already) 
by adding this configuration to pip:

.. code:: bash

    [global]
    index = https://acc-py-repo.cern.ch/repository/vr-py-releases/pypi
    index-url = https://acc-py-repo.cern.ch/repository/vr-py-releases/simple
    trusted-host = acc-py-repo.cern.ch


And then you can can easily install it using pip:

.. code:: bash

   pip install hpmlab