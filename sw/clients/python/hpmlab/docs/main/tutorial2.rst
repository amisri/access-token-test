Using groups of DVM's
=====================

This method uses GPIB "Group Execute Triggers" which synchronizes triggers.
In order to do this, first we instantiate two DVM's:

.. code:: python

    import hpmlab.instruments as inst

    dvm1 = inst.hp.HP3458A(24)
    dvm2 = inst.hp.HP3458A(23)

Then we join them as a group:

.. code:: python

    g = inst.Group(dvm1, dvm2)

This way you can send commands to the group and also read their respective responses:

.. code:: python

    g.write("nplc 2")
    g.write("range 10")

    result = g.measure()
    print(result)
