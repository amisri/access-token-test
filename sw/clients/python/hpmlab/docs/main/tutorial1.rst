Making Measurements with HP3458
===============================

First import the library and give it an alias name so it is easier to instantiate an instrument object:

.. code:: python

    import hpmlab.instruments as inst

    # Replace 24 with your instrument's predefined address
    dvm = inst.hp.HP3458A(24)

Then you can set dvm properties as python properties:

.. code:: python

    dvm.range = 10
    dvm.nplc = 2

Or if you want to read properties:

.. code:: python

    print(dvm.range) # Should print 10
    print(dvm.nplc) # Should print 2


And finally, you can get a single measurement:

.. code:: python

    print(dvm.measure())


If some property or command is not available through the library, you can always use normal GPIB commands:

.. code:: python

    # Can use raw GPIB instrument commands from the manual
    dvm.write("range 10")
    dvm.read()
