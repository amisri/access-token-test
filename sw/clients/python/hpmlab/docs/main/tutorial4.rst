Integrating into Midas
======================

Here it is a simple example with dvm HP3458A:

.. code:: python

    import midas
    import hpmlab.instruments as inst

    dvm = inst.hp.HP3458A(24)

    buffer = midas.Buffer('V_REF')

    # Acquire buffer for 3 seconds
    buffer.acquire(3, dvm.measure)

    # Print statistics of the whole buffer
    print(
        buffer.mean,
        buffer.std,
        buffer.p2p,
        buffer.max,
        buffer.min
    )
