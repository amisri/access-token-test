Fast Measurements with HP3458A
==============================

*Also called sweep measurements*

We start by importing the library as always:

.. code:: python

    import hpmlab.instruments as inst

    # Replace 24 with your instrument's predefined address
    dvm = inst.hp.HP3458A(24)

Then we can configure the number of samples and period, 
using the object's properties *nrdgs* and *timer* respectively:

.. code:: python

    dvm.samples = 1000
    dvm.nplc = 0.05

After that, call the method *sweep* which returns an iterable:

.. code:: python

    for sample in dvm.sweep():
        print(sample)
