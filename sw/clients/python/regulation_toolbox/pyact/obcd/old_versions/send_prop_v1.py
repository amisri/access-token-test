import pyfgc


class To_FGC:
    def __init__(self, device, reg_mode, test_select, **kwargs):
        self.dev = device
        self.ts = test_select
        self.rm = reg_mode
        self.__dict__.update(kwargs)

    def deploy_prop_IB(self):

        if self.ts == 1:
            ind = str(int(pyfgc.get(self.dev, 'LOAD.TEST_SELECT').value))
        elif self.ts == 0:
            ind = str(int(pyfgc.get(self.dev, 'LOAD.SELECT').value))

        pyfgc.set(self.dev, 'REG.{}.EXTERNAL_ALG[{}]'.format(self.rm, ind), 'ENABLED')

        if self.ts == 0:
            pyfgc.set(self.dev, 'REG.{}.EXTERNAL.OP.R'.format(self.rm),
                    ','.join([str(x) for x in self.RST['R']]))
            pyfgc.set(self.dev, 'REG.{}.EXTERNAL.OP.S'.format(self.rm),
                    ','.join([str(x) for x in self.RST['S']]))
            pyfgc.set(self.dev, 'REG.{}.EXTERNAL.OP.T'.format(self.rm),
                    ','.join([str(x) for x in self.RST['T']]))
        else:
            pyfgc.set(self.dev, 'REG.{}.EXTERNAL.TEST.R'.format(self.rm),
                    ','.join([str(x) for x in self.RST['R']]))
            pyfgc.set(self.dev, 'REG.{}.EXTERNAL.TEST.S'.format(self.rm),
                    ','.join([str(x) for x in self.RST['S']]))
            pyfgc.set(self.dev, 'REG.{}.EXTERNAL.TEST.T'.format(self.rm),
                    ','.join([str(x) for x in self.RST['T']]))

        pyfgc.set(self.dev, 'REG.{}.EXTERNAL.CLBW[{}]'.format(self.rm, ind), self.des_bw)
        pyfgc.set(self.dev, 'REG.{}.EXTERNAL.Z[{}]'.format(self.rm, ind), self.des_z)
        pyfgc.set(self.dev, 'REG.{}.EXTERNAL.MOD_MARGIN[{}]'.format(self.rm, ind), self.des_mm)
        pyfgc.set(self.dev, 'REG.{}.EXTERNAL.TRACK_DELAY_PERIODS[{}]'
                .format(self.rm, ind), self.track_delay)

        pyfgc.set(self.dev, 'REF.ILC.ORDER', self.ILC['order'])
        pyfgc.set(self.dev, 'REF.ILC.FUNCTION', ','.join([str(x) for x in self.ILC['filter']]))

    # def deploy_prop_V(self):