import pyfgc


class UI_prop:
    def __init__(self, device, reg_mode, test_select):
        self.device = device
        self.test_select = test_select
        self.reg_mode = reg_mode

    def defaults(self):
        fgc_prop = UI_prop.prop(self)
        Ts = round(self.fgc_prop['FGC.ITER_PERIOD'], 6) * int(self.fgc_prop
            ['REG.{}.PERIOD_ITERS'.format(self.reg_mode)])

        var = {'des_bw': 1 / (15 * Ts),
            'des_z': fgc_prop['REG.{}.EXTERNAL.Z'.format(self.reg_mode)],
            'des_mm': fgc_prop['REG.{}.EXTERNAL.Z'.format(self.reg_mode)],
            'n_integrators': 2, 'ref_delay': 0, 'opt_method': 'Hinf', 'H_scale': 1,
            'nr': 5, 'ns': 5, 'nt': 5, 'n_ilc': 3, 'epsilon': 0.1, 'beta': 0.1,
            'bis_tol': 1e-5, 'LMI_tol': 1e-3}
        return var

    def user_input(self):
        fgc_prop = UI_prop.prop(self)
        Ts = round(fgc_prop['FGC.ITER_PERIOD'], 6) * int(fgc_prop
            ['REG.{}.PERIOD_ITERS'.format(self.reg_mode)])

        var = {'Ts': Ts, 'des_bw': 200, 'des_z': 0.8, 'des_mm': 0.5,
            'n_integrators': 2, 'ref_delay': 0, 'opt_method': 'H1', 'h_scale': 1,
            'nr': 5, 'ns': 5, 'nt': 5, 'n_ilc': 3, 'epsilon': 0.1, 'beta': 0.1,
            'bis_tol': 1e-5, 'LMI_tol': 1e-3}
        return var, fgc_prop

    def user_input_v(self):
        fgc_prop = UI_prop.prop_v(self)
        Ts = round(fgc_prop['FGC.ITER_PERIOD'], 6)

        var = {'Ts': Ts, 'damp_bw': 80, 'damp_z': 0.8, 'volt_mm': 0.5,
            'volt_bw': 30, 'volt_z': 0.8, 'ref_delay': 0, 'epsilon': 0.1, 'beta': 0.1,
            'bis_tol': 1e-5, 'LMI_tol': 1e-3}
        return var, fgc_prop

    def prop(self):
        device = self.device
        test_select = self.test_select

        def hasNumbers(inputString):
            return any(char.isdigit() for char in inputString)

        property_names = ['LOAD.SELECT', 'LOAD.TEST_SELECT', 'LOAD.OHMS_SER', 'LOAD.OHMS_MAG',
                        'LOAD.OHMS_PAR', 'LOAD.HENRYS', 'LOAD.GAUSS_PER_AMP', 'VS.SIM.BANDWIDTH',
                        'VS.SIM.Z', 'VS.ACTUATION', 'VS.FIRING.DELAY', 'VS.ACT_DELAY_ITERS',
                        f'REG.{self.reg_mode}.PERIOD_ITERS', 'FGC.ITER_PERIOD',
                        f'REG.{self.reg_mode}.INTERNAL.PURE_DELAY_PERIODS',
                        f'MEAS.{self.reg_mode}.DELAY_ITERS',
                        f'REG.{self.reg_mode}.EXTERNAL.MEAS_SELECT',
                        f'REG.{self.reg_mode}.EXTERNAL.Z',
                        f'REG.{self.reg_mode}.EXTERNAL.MOD_MARGIN']

        # property_names = ['LOAD.SELECT', 'LOAD.OHMS_SER', 'LOAD.OHMS_MAG', 'LOAD.OHMS_PAR',
        # 'LOAD.HENRYS', 'REG.I.PERIOD_ITERS', 'FGC.ITER_PERIOD','VS.SIM.BANDWIDTH',
        # 'VS.SIM.Z', 'REG.I.EXTERNAL.MEAS_SELECT']

        if test_select == 1:
            ind = int(pyfgc.get(device, 'LOAD.TEST_SELECT').value)
        elif test_select == 0:
            ind = int(pyfgc.get(device, 'LOAD.SELECT').value)

        p_val = {}
        for prop_name in property_names:
            r = pyfgc.get(device, prop_name)
            p_val_all = r.value.split(',')

            if len(p_val_all) > 1 and hasNumbers(p_val_all[0]):
                p_val[prop_name] = float(p_val_all[ind])
            elif len(p_val_all) > 1 and not hasNumbers(p_val_all[0]):
                p_val[prop_name] = p_val_all[ind]

            if len(p_val_all) == 1 and hasNumbers(p_val_all[0]):
                p_val[prop_name] = float(p_val_all[0])
            elif len(p_val_all) == 1 and not hasNumbers(p_val_all[0]):
                p_val[prop_name] = p_val_all[0]

        p_val['MEAS.I.FIR_LENGTHS'] = list(map(int, pyfgc.get(device,
                                        'MEAS.I.FIR_LENGTHS').value.split(',')))
        p_val['MEAS.B.FIR_LENGTHS'] = list(map(int, pyfgc.get(device,
                                        'MEAS.B.FIR_LENGTHS').value.split(',')))

        return p_val

    def prop_v(self):
        device = self.device

        def hasNumbers(inputString):
            return any(char.isdigit() for char in inputString)

        property_names = ['LOAD.SELECT', 'LOAD.TEST_SELECT', 'LOAD.OHMS_SER', 'LOAD.OHMS_MAG',
                        'LOAD.OHMS_PAR', 'LOAD.HENRYS', 'FGC.ITER_PERIOD', 'VS.FILTER.FARADS1',
                        'VS.FILTER.FARADS2', 'VS.FILTER.OHMS', 'VS.FILTER.HENRYS',
                        'VS.SIM.BANDWIDTH', 'MEAS.I.DELAY_ITERS', 'MEAS.V.DELAY_ITERS',
                        'VS.SIM.Z', 'VS.ACTUATION', 'VS.FIRING.DELAY', 'VS.ACT_DELAY_ITERS']

        ind = int(pyfgc.get(device, 'LOAD.SELECT').value)

        p_val = {}
        for prop_name in property_names:
            r = pyfgc.get(device, prop_name)
            p_val_all = r.value.split(',')

            if len(p_val_all) > 1 and hasNumbers(p_val_all[0]):
                p_val[prop_name] = float(p_val_all[ind])
            elif len(p_val_all) > 1 and not hasNumbers(p_val_all[0]):
                p_val[prop_name] = p_val_all[ind]

            if len(p_val_all) == 1 and hasNumbers(p_val_all[0]):
                p_val[prop_name] = float(p_val_all[0])
            elif len(p_val_all) == 1 and not hasNumbers(p_val_all[0]):
                p_val[prop_name] = p_val_all[0]

        return p_val

# device_1 = "RFNA.866.01.ETH2"
# reg_mode = 'I'
# test_select = 0
# A = prop(device_1, test_select)