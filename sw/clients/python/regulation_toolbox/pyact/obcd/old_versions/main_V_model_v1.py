import numpy as np
import matplotlib.pyplot as plt
from optimize.fgc_prop.build_model import Model
from optimize.optimize_V import Optimize
from optimize.fgc_prop.props import UiParams, FgcProperties
import pyfgc

device = "RFNA.866.01.ETH2"
user_pars = vars(UiParams.set_pars_v())
fgc_props = vars(FgcProperties.from_fgc_v(device))

reg_mode = user_pars['reg_mode']
MOD = Model(reg_mode, fgc_props)
w_init = 2*np.pi*30/100; f_points = 400
F = MOD.model_frf_v(w_init, f_points)

gamma_bis_tol = 1e-5; gamma_LMI_tol = 1e-3
# Initialize optimization problem
P = Optimize(F, user_pars)

# Optimize performance for DAMPING-LOOP (ki, ku, kd)

Damping_opt = P.OPT_DAMPING_Hinf()
Kd_opt = Damping_opt['K-dloop']
print('K-damping = ', Kd_opt)



# Optimize performance for VOLTAGE-LOOP (kff, kp, kint)
lag = [(2*np.pi*user_pars['volt_bw'])/3, 2*np.pi*user_pars['volt_bw'],
    (2*np.pi*user_pars['volt_bw'])*3]
gamma_init = []; K_init = []
for a in lag:
    g_init, x0 = P.init_PI(a)
    gamma_init.append(g_init)
    K_init.append(x0)

PI_index_min = np.argmin(gamma_init)

Voltage_opt = P.OPT_VOLTAGE_Hinf(K_init[PI_index_min])
Kv_opt = Voltage_opt['K-vloop']
print('K-voltage = ', Kv_opt)


pyfgc.set(device_1, 'REG.V.FILTER.EXTERNAL.K_I', Kd_opt[0])
pyfgc.set(device_1, 'REG.V.FILTER.EXTERNAL.K_D', Kd_opt[1])
pyfgc.set(device_1, 'REG.V.FILTER.EXTERNAL.K_U', Kd_opt[2])
pyfgc.set(device_1, 'REG.V.EXTERNAL.K_P', Kv_opt[0])
pyfgc.set(device_1, 'REG.V.EXTERNAL.K_INT', Kv_opt[1])
pyfgc.set(device_1, 'REG.V.EXTERNAL.K_FF', Kv_opt[2])

Sens_max = np.amax(np.abs(Voltage_opt['Sens-volt'])) 
print('Modulus Margin = ', 1/Sens_max)

plt.figure()
plt.semilogx(F['angular_freq']/(2*np.pi),20*np.log10(np.abs(Voltage_opt['Sens-volt'])))
plt.savefig('T.png')
plt.show()
