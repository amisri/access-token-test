import numpy as np
import matplotlib.pyplot as plt
import control as cs
#import mosek
from build_model import MA_filter
from optimize.optimization_func_IB import optimize
from fgc_prop.get_prop import prop
import pyfgc
import sys


#device_1 = "RPAAO.866.02.ETH8"
device_1 = "RFNA.866.01.ETH2"
reg_mode = 'I'
test_select = 0

Ts_fund = round(prop(device_1, test_select)['FGC.ITER_PERIOD'],6)
Ts = Ts_fund * int(prop(device_1, test_select)['REG.' + reg_mode + '.PERIOD_ITERS'])
bw = 200; zeta = 0.8; n_high = [7,10,16]; n_integrators = 2; mm = 0.5; H2_scale = 1
reference_delay=0
opt_method = "Hinf"

# This would be the FRF data obtained through Fortlogs
#w1 = np.logspace(-2,np.log10(np.pi/(350e-6)),300); f1 = w1/(2*np.pi)
w1 = np.linspace(0.01,np.pi/(500e-6),300); f1 = w1/(2*np.pi)
R = 1e-2; L = 700e-6; 
G1 = np.exp(-1j*w1*0.0005)/(R + L*1j*w1)
Amp = np.absolute(G1); Phase_deg = np.unwrap(np.angle(G1))*180/(np.pi)

Fs = 1/Ts; 
del_freq = 5
print('Max freq = ' + str(f1[-1]) + ': Sampling freq = ' + str(Fs))

# Here, we check if there is enough frequency data for the requested sampling time in REG.I/B.PERIOD_ITERS
# If the data surpasses Fs/2, then we take the data up to Fs/2 for the synthesis
# If the data is less than Fs/2, then Fs/2 must be set to the highest available frequency point fmax (fmax < Fs/2)
if f1[-1] >= Fs/2 - del_freq:
    k_ind = np.where(2*f1 <= Fs)[0][-1] + 1 
    G1i = Amp[0:k_ind] * np.exp(1j*Phase_deg[0:k_ind]*np.pi/180); w = 2*np.pi*f1[0:k_ind]
elif f1[-1] < Fs/2 - del_freq:
    T_permitted = Ts_fund*np.arange(1,1000,1); F_permitted = 1./T_permitted
    f_max = f1[-1]
    k_fmax = np.where(F_permitted>=2*f_max); f_sample_max = F_permitted[k_fmax[0][-1] + 1]

    Ts_max = 1/f_sample_max; div = int(np.ceil(Ts_max/Ts_fund))
    print(k_fmax)
    print(f_sample_max)
    
    while True:
        try:
            div_user = int(input('Insufficient data for the current regulation frequency. REG.' + reg_mode + '.PERIOD_ITERS must be >= ' + str(div) + '. What value would you like to set REG.' + reg_mode + '.PERIOD_ITERS to?'))
            if div_user >= div:
                Ts = div_user * Ts_fund; Fs = 1/Ts
                k_ind = np.where(2*f1 - 1e-5 <= Fs)[0][-1] + 1
                G1i = Amp[0:k_ind] * np.exp(1j*Phase_deg[0:k_ind]*np.pi/180); w = 2*np.pi*f1[0:k_ind]
                break
            elif div_user < div:
                sys.exit('ERROR: cannot continue with the optimization. REG.' + reg_mode + '.PERIOD_ITERS must be at least ' + str(div))
        except ValueError:
            print('REG.' + reg_mode + '.PERIOD_ITERS must be a positive integer')

print(w[-1]/(2*np.pi), Fs/2)
G = G1i * MA_filter(w, device_1, reg_mode, test_select)

gamma_bis_tol = 1e-4; gamma_LMI_tol = 1e-4
P = optimize(G, w, Ts=Ts, bw=bw, zeta=zeta, n_integrators=n_integrators, 
                mm=mm, ref_delay=reference_delay, bis_tol=gamma_bis_tol, LMI_tol=gamma_LMI_tol)

gamma_tol = 1e-3

for i in n_high:
    P.cont_struct(i)
    BI_out = P.bisection(opt_method)
    gamma_BI = BI_out['gamma_opt']

    if gamma_BI <= 10:
        n_high_f = i
        break

    elif gamma_BI > 10:
        print('Possible bad solution...will try higher order controller...')

    if i == 16 and gamma_BI > 10:
        print('ERROR: No solution found...Change bandwidth or damping')


GAIN = np.sum(BI_out['T_vec'])/np.sum(BI_out['R_vec'])
Rf0 = BI_out['Rf']
Sf0 = BI_out['Sf']
Tf0 = BI_out['Tf']/GAIN
CL0 = (G*Tf0)/(G*Rf0 + Sf0)

# Optimize with LMI's
n_lmi = 5
gamma_LMI = 1e6

while gamma_LMI - gamma_BI > 0:
    #Rf0, Sf0 = P.init_RS(BI_out['R_vec'], BI_out['S_vec'], n_high_f)
    P.cont_struct(n_lmi)
    if opt_method == "Hinf":
        LMI_out = P.LMI_Hinf(Rf0,Sf0)
        gamma_LMI = LMI_out['gamma_opt']
        print(gamma_LMI)
    elif opt_method == "H2":
        LMI_out = P.LMI_H2(Rf0,Sf0,H2_scale)
        gamma_LMI = LMI_out['gamma_opt']
    n_lmi += 1

Rf = LMI_out['Rf']
Sf = LMI_out['Sf']
Tf = LMI_out['Tf']

CL = (G*Tf)/(G*Rf + Sf)

ilc_order = 5
ILC_out = P.ILC(CL,ilc_order)

plt.figure()
plt.semilogx(w/(2*np.pi),20*np.log10(np.abs(CL0)),
    w/(2*np.pi),20*np.log10(np.abs(CL)),
    w/(2*np.pi),20*np.log10(np.abs(P.Td)))
plt.savefig('main_IB_plot.png')
plt.show()


Rv = LMI_out['R_vec']
n_int_av = np.array([1,-1]); n_int_a = np.polynomial.polynomial.polypow(n_int_av,n_integrators)
sv = np.insert(LMI_out['S_vec'],0,1)
Sv = np.convolve(sv,n_int_a)
Tv = LMI_out['T_vec']/LMI_out['Gain']

print(Sv)
print(Tv)
print(ILC_out['L_vec'])
'''
den1 = np.zeros_like(np.arange(n-1)); den = np.insert(den1,0,1);
Rz = cs.tf(LMI_out['R_vec'],den,Ts)

sv = np.insert(LMI_out['S_vec'],0,1)
n_int_av = np.array([1,-1]); n_int_a = np.polynomial.polynomial.polypow(n_int_av,n_integrators)
n_int_bv = np.array([0,1]); n_int_b = np.flip(np.polynomial.polynomial.polypow(n_int_bv,n_integrators))
Sz = cs.tf(sv,den,Ts)*cs.tf(n_int_a,n_int_b,Ts)

GAIN = np.sum(LMI_out['T_vec'])/np.sum(LMI_out['R_vec'])
Tz = cs.tf(LMI_out['T_vec'],den,Ts); Tz = cs.minreal(Tz/GAIN)

psi = Gz*Rz + Sz

TZ = (Gz*Tz)/(psi)

t = np.arange(0,0.1,Ts)
t, yout = cs.step_response(TZ,t,transpose = True);
td, yout_d = cs.step_response(Tdes,t,transpose = True);

plt.figure()
plt.plot(t,yout,td,yout_d)
plt.legend(['Output','Desired'])

T = np.divide(np.multiply(P2.G,LMI_out['T_final_freq']),LMI_out['S_final_freq']+np.multiply(P2.G,LMI_out['R_final_freq']))
T_db = 20*np.log10(np.absolute(T))

phase = np.unwrap(np.angle(T))
Group_Delay = -np.gradient(phase,P2.w)
dtrack = Group_Delay[0]

plt.figure()
plt.semilogx(P2.w/(2*np.pi),Group_Delay)

S = np.divide(S0,S0+np.multiply(P2.G,LMI_out['R_final_freq']))
S_db = 20*np.log10(np.absolute(S));


T_des_db = 20*np.log10(np.absolute(P2.Td))

plt.figure()
plt.semilogx(P2.w/(2*np.pi),T_db,P2.w/(2*np.pi),T_des_db)

L = P2.G*LMI_out['R_final_freq']/LMI_out['S_final_freq']

#plt.figure(4)
#plt.plot(np.real(L),np.imag(L))
plt.show()


Rv = LMI_out['R_vec']
Sv = np.convolve(sv,n_int_a)
Tv = LMI_out['T_vec']/GAIN

out = FGC_output(Rv,Sv,Tv,Ts,dtrack,bw,zeta)
'''
