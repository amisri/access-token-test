import cvxpy as cp
import numpy as np
import sys

class Optimize:
    def __init__(self, G, w, **kwargs):

        self.w = w
        self.G = G
        self.__dict__.update(kwargs)

        wn = (2 * np.pi * self.des_bw) / np.sqrt(1 - 2 * np.power(self.des_z, 2) + np.sqrt(2 - 4 * np.power(self.des_z, 2) + 4 * np.power(self.des_z, 4))) # pylint: disable=no-member
        Td = np.power(wn, 2) / (-np.power(self.w, 2) + 2 * self.des_z * wn * 1j * self.w + np.power(wn, 2)) # pylint: disable=no-member

        self.Td = Td * np.exp(-1j* self.w * self.ref_delay) # pylint: disable=no-member
        #wn = 2*np.pi*self.bw; self.Td = (2*wn*1j*self.w + np.power(wn,2)) / (-np.power(self.w, 2) + 2 * wn * 1j * self.w + np.power(wn, 2)) # pylint: disable=no-member

    def cont_struct(self, n):
        
        rho_r = cp.Variable(n); rho_s = cp.Variable(n-1-self.n_integrators); rho_t = cp.Variable(n) # pylint: disable=no-member

        Ro = 0
        for i in range(n):
            Ri = rho_r[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            Ro += Ri

        So = 0
        for i in range(n-1-self.n_integrators): # pylint: disable=no-member
            Si = rho_s[i]*np.exp(-(i+1)*1j*self.w*self.Ts) # pylint: disable=no-member
            So += Si

        So = 1+So
        #So = cp.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),n_integrators),(1+So));

        To = 0
        for i in range(n):
            Ti = rho_t[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            To += Ti

        self.rho_r = rho_r
        self.rho_s = rho_s
        self.rho_t = rho_t
        self.Ro = Ro
        self.So = So
        self.To = To
        self.n = n
        
        #return Ro,So,To

    def init_RS(self, rho_r, rho_s, n_high):

        R0 = 0
        for i in range(n_high):
            Rii = rho_r[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            R0 += Rii

        S0 = 0
        for i in range(n_high-1-self.n_integrators): # pylint: disable=no-member
            Sii = rho_s[i]*np.exp(-(i+1)*1j*self.w*self.Ts) # pylint: disable=no-member
            S0 += Sii

        S0 = np.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),(1+S0)) # pylint: disable=no-member

        return R0, S0


    def bisection(self,opt_method):

        So_no_int = self.So
        So = cp.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),self.So) # pylint: disable=no-member

        Wd = 1/(1-self.Td)
        g_max = 10; g_min = 1e-8 #gamma = (g_max + g_min)/2;

        gamma = cp.Parameter(nonneg = True)
        PSI = So + cp.multiply(self.G,self.Ro)
        if opt_method == "H2":
            F1 = cp.multiply(cp.inv_pos(gamma),cp.abs(cp.multiply((2*np.pi)/(1j*self.w),cp.multiply(self.G,self.To) - cp.multiply(self.Td,PSI)))) - cp.real(PSI)
            #F1 = cp.multiply(cp.inv_pos(gamma),cp.abs(cp.multiply(Wd,PSI - cp.multiply(self.G,self.To)))) - cp.real(PSI)
        elif opt_method == "Hinf":
            F1 = cp.multiply(cp.inv_pos(gamma),cp.abs(cp.multiply(Wd,PSI - cp.multiply(self.G,self.To)))) - cp.real(PSI)
        elif opt_method == "H1":
            F1 = cp.multiply(cp.inv_pos(gamma),cp.abs(cp.multiply(Wd,PSI - cp.multiply(self.G,self.To)))) - cp.real(PSI)

        F2 = cp.abs(cp.multiply(self.des_mm,So)) - cp.real(PSI) # pylint: disable=no-member
        constraints = [F1 <= -1e-6, F2 <= -1e-6, cp.real(So_no_int) >= 5e-3]
        gamma.value = (g_max + g_min)/2

        while g_max - g_min > 10*self.bis_tol: # pylint: disable=no-member

            prob = cp.Problem(cp.Minimize(gamma),constraints)

            try:
                prob.solve(solver = cp.ECOS,abstol_inacc = 1,max_iters=10000)
            except:
                print('ECOS solver failed. Trying a more robust solver... \n')
                try:
                    prob.solve(solver = cp.CVXOPT,kktsolver = 'robust')
                except:
                    print('Could not find any solution. Please change your desired specifications and/or verify that your input data is correct.')

            stat = prob.status

            if stat == "optimal":
                print("Feasible (gamma = ", gamma.value ,")")
                gamma_opt = gamma.value
                rho_r_OPT = self.rho_r.value
                rho_s_OPT = self.rho_s.value
                rho_t_OPT = self.rho_t.value
                g_max = gamma.value
                gamma.value = np.average([gamma.value,g_min])
                #GAIN = np.sum(rho_t_OPT)/np.sum(rho_r_OPT)

            else:
                print("Infeasible (gamma = ", gamma.value ,")")
                g_min = gamma.value
                gamma.value = np.average([gamma.value,g_max])

        if gamma.value > 9:
            gamma_opt = 100
            rho_r_OPT = np.ones(self.n); rho_s_OPT = np.ones(self.n - 1); rho_t_OPT = np.ones(self.n)
            R0 = 1; S0 = 1; T0 = 1 
            #GAIN = 1

        print("\nThe optimal solution gamma = %f" % gamma_opt)


        R0 = 0 
        for i in range(self.n):
            Rii = rho_r_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            R0 += Rii

        S0 = 0
        for i in range(self.n-1-self.n_integrators): # pylint: disable=no-member
            Sii = rho_s_OPT[i]*np.exp(-(i+1)*1j*self.w*self.Ts) # pylint: disable=no-member
            S0 += Sii

        S0 = np.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),(1+S0)) # pylint: disable=no-member

        T0 = 0
        for i in range(self.n):
            Tii = rho_t_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            T0 += Tii

        return {'gamma_opt':gamma_opt,
                'R_vec':rho_r_OPT,'S_vec':rho_s_OPT,'T_vec':rho_t_OPT,
                'Rf': R0, 'Sf': S0, 'Tf': T0}

    def hinf(self,R0,S0):

        So_no_int = self.So
        So = cp.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),self.So) # pylint: disable=no-member

        Wd = 1/(1-self.Td)
        g_lmi_0 = np.zeros(100); g_lmi = np.insert(g_lmi_0,0,1e6)
        it = 1

        while g_lmi[it-1] - g_lmi[it] > self.LMI_tol: # pylint: disable=no-member
            g_max = 200; g_min = 1e-8; gamma = (g_max + g_min)/2
            while g_max - g_min > self.bis_tol: # pylint: disable=no-member

                PSI2 = So + cp.multiply(self.G,self.Ro)
                PSI2_0 = S0 + np.multiply(self.G,R0)

                t1 = 2*cp.real(cp.multiply(cp.conj(PSI2_0),PSI2)) - cp.power(cp.abs(PSI2_0),2)
                #x1 = cp.multiply(cp.inv_pos(gamma),cp.power(cp.abs(cp.multiply((2*np.pi)/(1j*w),cp.multiply(G,To) - cp.multiply(Td,PSI2))),2))
                x1 = cp.multiply(cp.inv_pos(gamma),cp.power(cp.abs(cp.multiply(Wd,PSI2 - cp.multiply(self.G,self.To))),2))
                x2 = cp.power(cp.abs(cp.multiply(self.des_mm,So)),2) # pylint: disable=no-member
                F1 = x1 - t1
                F2 = x2 - t1

                constraints2 = [F1 <= -1e-6, F2 <= -1e-6, cp.real(So_no_int) >= 5e-3]
                prob = cp.Problem(cp.Minimize(0),constraints2)
                
                try:
                    prob.solve(solver = cp.ECOS,abstol_inacc = 1,max_iters=10000)
                except:
                    print('ECOS solver failed. Trying a more robust solver... \n')
                    try:
                        prob.solve(solver = cp.CVXOPT,kktsolver = 'robust')
                    except:
                        print('Could not find any solution. Please change your desired specifications and/or verify that your input data is correct.')
                stat = prob.status
                #print(stat)

                if stat == "optimal":
                    #print("Feasible (gamma = ", gamma ,")")
                    gamma_opt = gamma
                    rho_r_OPT = self.rho_r.value
                    rho_s_OPT = self.rho_s.value
                    rho_t_OPT = self.rho_t.value
                    g_max = gamma
                    gamma = np.average([gamma,g_min])
                    GAIN = np.sum(rho_t_OPT)/np.sum(rho_r_OPT)

                else:
                    #print("Infeasible (gamma = ", gamma ,")")
                    g_min = gamma
                    gamma = np.average([gamma,g_max])

            if gamma > 100:
                gamma_opt = 1000
                return {'gamma_opt':gamma_opt}


            print("Optimal gamma for iteration", it, " = ", np.sqrt(gamma_opt))
            R0 = 0
            for i in range(self.n):
                Rii = rho_r_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
                R0 += Rii

            S0 = 0
            for i in range(self.n-1-self.n_integrators): # pylint: disable=no-member
                Sii = rho_s_OPT[i]*np.exp(-(i+1)*1j*self.w*self.Ts) # pylint: disable=no-member
                S0 += Sii

            S0 = np.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),(1+S0)) # pylint: disable=no-member
            g_lmi[1] = 1e6

            g_lmi[it+1] = np.sqrt(gamma_opt)
            it = it+1

        T0 = 0
        for i in range(self.n):
            Tii = rho_t_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            T0 += Tii

        T0 = T0/GAIN

        return {'gamma_opt':np.sqrt(gamma_opt),
                'R_vec':rho_r_OPT,'S_vec':rho_s_OPT,'T_vec':rho_t_OPT,'Gain':GAIN,
                'Rf':R0,'Sf':S0,'Tf':T0}

    def h2(self, R0, S0, scale):

        So_no_int = self.So
        So = cp.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),self.So) # pylint: disable=no-member

        #Wd = 1/(1-self.Td)
        g_lmi_0 = np.zeros(100); g_lmi = np.insert(g_lmi_0,0,1e6)
        it = 1

        while g_lmi[it-1] - g_lmi[it] > 10*self.LMI_tol: # pylint: disable=no-member
            gamma = cp.Variable(len(self.w),nonneg = True)

            PSI2 = So + cp.multiply(self.G,self.Ro)
            PSI2_0 = S0 + np.multiply(self.G,R0)

            #states = [];
            con = []
            #MAT = cp.Variable((2*len(w), 2*len(w)))
            for i in range(len(self.w)):

                con1 = 2*cp.real(cp.multiply(cp.conj(PSI2_0[i]),PSI2[i])) - cp.power(cp.abs(PSI2_0[i]),2)
                #con2 = cp.multiply(Wd[i],PSI2[i] - cp.multiply(self.G[i],self.To[i]))
                #con2 = cp.multiply(Wd[i],So[i] + cp.multiply(self.G[i],self.Ro[i] - self.To[i]))
                con2 = cp.multiply((scale*2*np.pi)/(1j*self.w[i]),cp.multiply(self.G[i],self.To[i]) - cp.multiply(self.Td[i],PSI2[i]))
                con3 = cp.conj(con2)
                con4 = gamma[i]

                A1 = cp.hstack([con1,con3])
                A2 = cp.hstack([con2, con4])
                MAT1 = cp.vstack([A1,A2])
                #MAT1 = cp.bmat([[con1,con3],[con2,con4]])


                #if i >= 1 and i <= len(w)-2:
                #   MAT2 = cp.bmat([[MAT2],[cp.hstack([np.zeros((2,2*i)),MAT1,np.zeros((2,2*(len(w)-i-1)))]) ]] )
                #elif i < 1:
                #   MAT2 = cp.hstack([MAT1,np.zeros((2,2*(len(w)-i-1)))])
                #elif i == len(w)-1:
                #   MAT2 = cp.bmat([[MAT2],[cp.hstack([np.zeros((2,2*(i))),MAT1]) ]] )




                con11 = 2*cp.real(cp.multiply(cp.conj(PSI2_0[i]),PSI2[i])) - cp.power(cp.abs(PSI2_0[i]),2)
                con21 = self.des_mm*So[i] # pylint: disable=no-member
                con31 = cp.conj(con21)
                con41 = 1

                A11 = cp.hstack([con11,con31])
                A12 = cp.hstack([con21, con41])
                MAT2 = cp.vstack([A11,A12])
                #MAT2 = cp.bmat([[con11,con31],[con21,con41]])

                con += [MAT1 >> 0, MAT2 >> 0]
                #states.append(cp.Problem(cp.Minimize(0),constr))



                '''
                #Use this for setting constraints 2x2
                MAT = cp.Variable((2, 2))
                con1 = MAT[0,0] == 2*cp.real(cp.multiply(cp.conj(PSI2_0[i]),PSI2[i])) - cp.power(cp.abs(PSI2_0[i]),2)
                con2 = MAT[1,0] == cp.multiply(Wd[i],PSI2[i] - cp.multiply(G[i],To[i]))
                con3 = MAT[0,1] == cp.conj(MAT[1,0])
                con4 = MAT[1,1] == gamma2
    
                MAT2 = cp.Variable((2, 2))
                con11 = MAT2[0,0] == MAT[0,0]
                con22 = MAT2[1,0] == mm*So[i]
                con33 = MAT2[0,1] == cp.conj(MAT2[1,0])
                con44 = MAT2[1,1] == 1
    
                cost = gamma2
                constr = [MAT >> 0,con1,con2,con3,con4,MAT2 >> 0,con11,con22,con33,con44]
                #constr = [MAT >> 0,con1,con2,con3,con4]
                states.append(cp.Problem(cp.Minimize(cost),constr))
                del MAT
                del MAT2
                '''


                '''
                # Use this for setting constraints nxn
                con1 = MAT[2*i,2*i] == cp.conj(PSI2[i])*PSI2_0[i] + cp.conj(PSI2_0[i])*PSI2[i] - cp.conj(PSI2_0[i])*PSI2_0[i]
                con2 = MAT[2*i+1,2*i] == cp.multiply(Wd[i],PSI2[i] - cp.multiply(G[i],To[i]))
                con3 = MAT[2*i,2*i+1] == cp.conj(MAT[2*i+1,2*i])
                con4 = MAT[2*i+1,2*i+1] == gamma2
    
                if i == len(w)-1:
                    cost = gamma2
                    constr = [con1,con2,con3,con4]
                    states.append(cp.Problem(cp.Minimize(cost),constr))
                    break
                else:
                    con0 = MAT[:,2*i+2] == 0
                    con01 = MAT[:,2*i+3] == 0
                    con02 = MAT[2*i+2,:] == 0
                    con03 = MAT[2*i+3,:] == 0
                    cost = gamma2
                    constr = [con1,con2,con3,con4,con0,con01,con02,con03]
                    states.append(cp.Problem(cp.Minimize(cost),constr))
                '''


                '''
                # Use this for setting constraints nxn
                con1 = MAT[2*i,2*i] == cp.conj(PSI2[i])*PSI2_0[i] + cp.conj(PSI2_0[i])*PSI2[i] - cp.conj(PSI2_0[i])*PSI2_0[i]
                con2 = MAT[2*i+1,2*i] == cp.multiply(Wd[i],PSI2[i] - cp.multiply(G[i],To[i]))
                con3 = MAT[2*i,2*i+1] == cp.conj(MAT[2*i+1,2*i])
                con4 = MAT[2*i+1,2*i+1] == gamma2
    
                if i == len(w)-1:
                    cost = gamma2
                    constr = [con1,con2,con3,con4]
                    states.append(cp.Problem(cp.Minimize(cost),constr))
                    break
                else:
                    con0 = MAT[:,2*i+2] == 0
                    con01 = MAT[:,2*i+3] == 0
                    con02 = MAT[2*i+2,:] == 0
                    con03 = MAT[2*i+3,:] == 0
                    cost = gamma2
                    constr = [con1,con2,con3,con4,con0,con01,con02,con03]
                    states.append(cp.Problem(cp.Minimize(cost),constr))
                '''

            #constraints = [MAT1 >> 0]
            #prob = cp.Problem(cp.Minimize(gamma2),constraints)
            #prob = cp.Problem(cp.Minimize(gamma),constraints)


            #prob = cp.sum(states)
            print("Now doing LMI's for H2 performance")
            #print(MAT2.shape)
            #cnst = prob.constraints + [cp.real(So_no_int) >= 5e-3, gamma >= 0]
            #cnst = prob.constraints + [MAT >> 0]
            #prob = cp.Problem(prob.objective, cnst)
            #cnst = [MAT2 >> 0,cp.real(So_no_int) >= 5e-3]
            cnst = con + [cp.real(So_no_int) >= 5e-3]
            prob = cp.Problem(cp.Minimize(cp.sum(gamma)), cnst)
            #prob = cp.Problem(cp.Minimize(gamma),[MAT >> 0,con1,con2,con3,con4])
            

            try:
                prob.solve(solver = cp.ECOS, abstol_inacc = 1, max_iters=10000)
            except:
                print('ECOS solver failed. Trying a more robust solver... \n')
                try:
                    prob.solve(solver = cp.CVXOPT, kktsolver = 'robust')
                except:
                    print('Could not find any solution. Please change your desired specifications and/or verify that your input data is correct.')

            
            stat = prob.status
            print(stat)
            print(np.sum(gamma.value))
            if stat == "optimal" or stat == "optimal_inaccurate":
                print("Feasible (gamma = ", np.sum(gamma.value) ,")")
                gamma_opt = np.sum(gamma.value)
                rho_r_OPT = self.rho_r.value
                rho_s_OPT = self.rho_s.value
                rho_t_OPT = self.rho_t.value
                GAIN = np.sum(rho_t_OPT)/np.sum(rho_r_OPT)

            R0 = 0
            for i in range(self.n):
                Rii = rho_r_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
                R0 += Rii

            S0 = 0
            for i in range(self.n-1-self.n_integrators): # pylint: disable=no-member
                Sii = rho_s_OPT[i]*np.exp(-(i+1)*1j*self.w*self.Ts) # pylint: disable=no-member
                S0 += Sii

            S0 = np.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),(1+S0)) # pylint: disable=no-member

            g_lmi[1] = 1e6
            g_lmi[it+1] = gamma_opt
            it = it+1

        T0 = 0
        for i in range(self.n):
            Tii = rho_t_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            T0 += Tii

        T0 = T0/GAIN

        return {'gamma_opt':gamma_opt,
                'R_vec':rho_r_OPT,'S_vec':rho_s_OPT,'T_vec':rho_t_OPT,'Gain':GAIN,
                'Rf':R0,'Sf':S0,'Tf':T0}

    def h1(self, nt):
        
        rho_t = cp.Variable(nt)
        To = 0
        for i in range(nt):
            Ti = rho_t[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            To += Ti

        So_no_int = self.So
        So = cp.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),self.So) # pylint: disable=no-member

        PSI = So + cp.multiply(self.G,self.Ro)

        F2 = cp.abs(cp.multiply(self.des_mm,So)) - cp.real(PSI) # pylint: disable=no-member
        constraints = [F2 <= -1e-6, cp.real(So_no_int) >= 5e-3]

        prob = cp.Problem(cp.Minimize(0),constraints)

        try:
            prob.solve(solver = cp.ECOS,abstol_inacc = 1,max_iters=10000)
        except:
            print('ECOS solver failed. Trying a more robust solver... \n')
            try:
                prob.solve(solver = cp.CVXOPT,kktsolver = 'robust')
            except:
                print('Could not find any solution. Please change your desired specifications and/or verify that your input data is correct.')

        stat = prob.status

        if stat == "optimal":
            #print("Feasible")
            rho_r_OPT = self.rho_r.value
            rho_s_OPT = self.rho_s.value
            RS_flag = False

        else:
            #print("Infeasible")
            return {'RS-flag':True}

        R0 = 0 
        for i in range(self.n):
            Rii = rho_r_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
            R0 += Rii

        S0 = 0
        for i in range(self.n-1-self.n_integrators): # pylint: disable=no-member
            Sii = rho_s_OPT[i]*np.exp(-(i+1)*1j*self.w*self.Ts) # pylint: disable=no-member
            S0 += Sii

        S0 = np.multiply(np.power((1-np.exp(-1j*self.w*self.Ts)),self.n_integrators),(1+S0)) # pylint: disable=no-member
        
        #Wd = 1/(1-self.Td)
        TCL = cp.multiply(cp.multiply(self.G, To), 1 / (S0 + R0 * self.G)) 
        #OB = cp.multiply(Wd, (1-TCL))
        OB = cp.multiply(cp.multiply(2*np.pi, 1/(1j*self.w)),TCL - self.Td)
        #OB = cp.multiply(self.G, To) - cp.multiply(self.Td, (S0 + R0 * self.G))
        prob2 = cp.Problem(cp.Minimize(cp.pnorm(OB, 1)))

        try:
            prob2.solve(solver = cp.CVXOPT,kktsolver = 'robust')
        except:
            print('All solvers failed')
        stat2 = prob2.status

        if stat2 == "optimal":
            T_flag = False
            #print("H1 optimization Feasible \n")
            rho_t_OPT = rho_t.value
            GAIN = np.sum(rho_t_OPT)/np.sum(rho_r_OPT)
            
            T0 = 0
            for i in range(nt):
                Tii = rho_t_OPT[i]*np.exp(-i*1j*self.w*self.Ts) # pylint: disable=no-member
                T0 += Tii

            T0 = T0/GAIN

        else:
            #print("H1 optimization Infeasible \n")
            #rho_r_OPT = np.ones(self.n); rho_s_OPT = np.ones(self.n - 1); rho_t_OPT = np.ones(self.n)
            #R0 = 1; S0 = 1; T0 = 1; T_flag = True
            T_flag = True
            return {'T-flag':T_flag, 'RS-flag':RS_flag}


        
        return {'R_vec':rho_r_OPT,'S_vec':rho_s_OPT,'T_vec':rho_t_OPT,'Gain':GAIN,
                'Rf':R0,'Sf':S0,'Tf':T0, 'RS-flag': RS_flag, 'T-flag': T_flag}

    def ilc(self,G,n):

        rho = cp.Variable(2*n+1)
        n_vector = np.arange(-n,n+1)
        n_vector = n_vector.astype(int)

        L = 0; ind = 0
        for i in n_vector:
            Li = cp.multiply(rho[ind], np.exp(i*1j*self.w*self.Ts)) # pylint: disable=no-member
            ind += 1
            L += Li

        gamma = cp.Variable(1)
        F1 = cp.abs(1 - cp.multiply(L,G))
        constraints = [F1 <= gamma, gamma >=0]

        prob = cp.Problem(cp.Minimize(gamma),constraints)
        #prob = cp.Problem(cp.Minimize(cp.pnorm(F1,'inf')),constraints)

        try:
            prob.solve(solver = cp.CVXOPT,kktsolver = 'robust')
        except:
            print('ERROR: ILC solver failed. Try to modify your desired specifications.')

        stat = prob.status

        if stat == "optimal" and gamma.value < 1:
            #print("Feasible Problem")
            rho0 = rho.value
            gamma0 = gamma.value
            flag = False
            print('ILC optimal value = ', gamma0)
        else:
            #print("ILC: Infeasible Problem")
            return {'flag': True}

        L0 = 0; ind = 0
        for i in n_vector:
            Li0 = rho0[ind]*np.exp(i*1j*self.w*self.Ts) # pylint: disable=no-member
            ind += 1
            L0 += Li0

        return {'gamma_opt':gamma0, 'flag': flag,
                'Lf':L0,'L_vec':rho0}

