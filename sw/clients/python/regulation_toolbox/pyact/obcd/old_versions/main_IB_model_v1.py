import numpy as np
import matplotlib.pyplot as plt
import control as cs
from build_model import Model
from optimize.optimization_func_IB import Optimize
from fgc_prop.get_prop import prop
from fgc_prop.send_prop import To_FGC
import pyfgc
import time

start = time.time()


#device_1 = "RPAAO.866.02.ETH8"
device_1 = "RFNA.866.01.ETH2"
reg_mode = 'I'
test_select = 0

Ts = round(prop(device_1, test_select)['FGC.ITER_PERIOD'],6) * int(prop(device_1, test_select)['REG.{}.PERIOD_ITERS'.format(reg_mode)])
bw = 200; zeta = 0.8; n_high = [6,8,10,16]; n_integrators = 2; mm = 0.5; reference_delay = 0
epsilon = 0.1; beta = 0.1; H2_scale = 1
opt_method = "Hinf"

mod = Model(device_1, reg_mode, test_select)

#w = np.logspace(-2,np.log10(np.pi/Ts),300)
#R = 1e-2; L = 700e-6; G = np.exp(-1j*w*0.0005)/(R + L*1j*w)

gamma_bis_tol = 1e-5; gamma_LMI_tol = 1e-3


for i in n_high:
    G, w = mod.model_frf(i, epsilon, beta)
    P = Optimize(G, w, Ts=Ts, des_bw=bw, des_z=zeta, n_integrators=n_integrators, 
                    des_mm=mm, ref_delay=reference_delay, bis_tol=gamma_bis_tol, LMI_tol=gamma_LMI_tol)

    P.cont_struct(i)
    BI_out = P.bisection(opt_method)
    gamma_BI = BI_out['gamma_opt']

    if gamma_BI <= 10:
        n_high_f = i
        break

    elif gamma_BI > 10:
        print('Possible bad solution...will try higher order controller...')

    if i == 16 and gamma_BI > 10:
        print('ERROR: No solution found...Change bandwidth or damping')


GAIN = np.sum(BI_out['T_vec'])/np.sum(BI_out['R_vec'])
Rf0 = BI_out['Rf']
Sf0 = BI_out['Sf']
Tf0 = BI_out['Tf']/GAIN
CL0 = (G*Tf0)/(G*Rf0 + Sf0)


# Optimize with LMI's
n_lmi = 6
gamma_LMI = 1e6

if opt_method in ["H2", "Hinf"]:
    while gamma_LMI - gamma_BI > 0:
        #Rf0, Sf0 = P.init_RS(BI_out['R_vec'], BI_out['S_vec'], n_high_f)
        P.cont_struct(n_lmi)
        if opt_method == "Hinf":
            LMI_out = P.hinf(Rf0,Sf0)
            gamma_LMI = LMI_out['gamma_opt']
            print(gamma_LMI)
        elif opt_method == "H2":
            LMI_out = P.h2(Rf0,Sf0,H2_scale)
            gamma_LMI = LMI_out['gamma_opt']
        n_lmi += 1
elif opt_method in ["H1"]:
    H1_flag = True
    nT = n_lmi + 3
    while H1_flag:
        P.cont_struct(n_lmi)
        LMI_out = P.h1(nT)
        H1_flag = LMI_out['RS-flag'] or LMI_out['T-flag']
        if not H1_flag and nT >= 17:
            print('ERROR: Maximum coeffs of T reached. Could not find a solution.')
        if LMI_out['RS-flag']:
            if n_lmi == 16:
                print('ERROR: Maximum coeffs of R or S reached. Could not find a solution.')
            print('Initial RS design failed...increasing order of R and S')
            n_lmi += 1; nT += 1
        elif not LMI_out['RS-flag'] and LMI_out['T-flag']:
            if nT == 16:
                print('ERROR: Maximum coeffs of T reached. Could not find a solution.')
            print('T design failed...increasing order of T')
            nT += 1


Rf = LMI_out['Rf']
Sf = LMI_out['Sf']
Tf = LMI_out['Tf']

CL = (G*Tf)/(G*Rf + Sf)
a = np.asarray([np.real(CL),np.imag(CL), w])
np.savetxt("test_ILC_data.csv", a, delimiter=",")

Wd = 1/(1-P.Td)
print('Hinf-max = ', np.amax(np.abs(Wd * (1-CL))))

print('\n Now optimizing ILC filter.')
ilc_order = 3; ilc_flag = True
while ilc_flag:
    ILC_out = P.ilc(CL,ilc_order)
    ilc_flag = ILC_out['flag']
    if ilc_flag:
        print('ILC problem infeasible...Increasing filter order \n')
        ilc_order += 1
    if ilc_order > 15:
        print('ERROR: Could not find a robust ILC filter.')

plt.figure()
plt.semilogx(w/(2*np.pi),20*np.log10(np.abs(CL0)),
    w/(2*np.pi),20*np.log10(np.abs(CL)),
    w/(2*np.pi),20*np.log10(np.abs(P.Td)))
plt.savefig('T.png')
plt.show()

phase = np.unwrap(np.angle(CL))
Group_Delay = -np.gradient(phase,w)
dtrack = Group_Delay[0]/Ts

Rv = LMI_out['R_vec']
n_int_av = np.array([1,-1]); n_int_a = np.polynomial.polynomial.polypow(n_int_av,n_integrators)
sv = np.insert(LMI_out['S_vec'],0,1)
Sv = np.convolve(sv,n_int_a)
Tv = LMI_out['T_vec']/LMI_out['Gain']
RST = {'R':Rv, 'S':Sv, 'T': Tv}
ILC = {'filter': ILC_out['L_vec'], 'order': ilc_order}

end = time.time()
print('Time elapsed: ',end - start, ' seconds')

FGC = To_FGC(device_1, reg_mode, test_select, RST=RST, ILC=ILC, track_delay=dtrack, des_mm=mm, des_bw=bw, des_z=zeta)
FGC.deploy_prop_IB()
