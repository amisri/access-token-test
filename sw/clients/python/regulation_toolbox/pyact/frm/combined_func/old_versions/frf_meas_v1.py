import json
import numpy as np
import matplotlib.pyplot as plt
from combined_func.frf_props import UiParams


class Frm_methods:

    @staticmethod
    def prbs(path, pars):

        with open(path) as json_file:
            data = json.load(json_file)
        X = data['signals']
        Ts_fund = data['period']
        Ts = Ts_fund * pars.period_iters
        Fs = 1 / Ts

        for i in range(len(X)):
            if X[i]['name'] == pars.ref_mode:
                in_array = X[i]['samples']
            if X[i]['name'] == pars.meas_mode:
                out_array = X[i]['samples']

        amp = pars.pp_amp / 2
        for i in range(len(in_array)):
            diff = np.absolute(in_array[i + 1] - in_array[i])
            if diff >= amp / 5:
                start_index = i + 1
                break

        dat_length = np.power(2, pars.k_order) - 1
        p_start, p_end = 4, pars.num_seq - 1

        y = out_array[start_index::pars.period_iters]
        r = in_array[start_index::pars.period_iters]
        y11 = y[p_start * dat_length:p_end * dat_length]
        r11 = r[p_start * dat_length:p_end * dat_length]

        yfft, rfft = np.fft.rfft(y11), np.fft.rfft(r11)
        L = len(y11)

        RP2n = rfft / L
        RP1n = RP2n[0:int(np.floor((L + 1) / 2 + 1))]
        RP1n[1:] = 2 * RP1n[1:]
        YP2n = yfft / L
        YP1n = YP2n[0:int(np.floor((L + 1) / 2 + 1))]
        YP1n[1:] = 2 * YP1n[1:]

        with np.errstate(divide='ignore'):
            N_FRF = YP1n / RP1n
        G1 = N_FRF[0::int(p_end - p_start)]
        f1 = Fs * np.linspace(0, int((dat_length + 1) / 2 - 1),
            int((dat_length + 1) / 2)) / dat_length

        beg_freq = 50
        if pars.k_order in {7, 8, 9, 10}:
            G, f = G1[1:], f1[1:]
        elif pars.k_order > 10:
            K_ORDER_step = int(str(pars.k_order)[-1])
            f = np.concatenate((f1[1:beg_freq], f1[beg_freq + 1::2**K_ORDER_step], f1[-1]),
                axis=None)
            G = np.concatenate((G1[1:beg_freq], G1[beg_freq + 1::2**K_ORDER_step], G1[-1]),
                axis=None)

        amp_db = 20 * np.log10(np.absolute(G))
        phase_deg = (180 / np.pi) * np.unwrap(np.angle(G))
        UiParams(freq=f, amp=amp_db, phase=phase_deg).to_fortlogs()

        font = {'family': 'monospace',
            'weight': 'bold',
            'size': 15}

        plt.rc('font', **font)
        plt.subplot(2, 1, 1)
        plt.semilogx(f, amp_db)
        plt.ylabel('Magnitude [dB]')
        plt.grid(True, which="both")

        plt.subplot(2, 1, 2)
        plt.semilogx(f, phase_deg)
        plt.ylabel('Phase [deg]')
        plt.xlabel('Frequency [Hz]')
        plt.grid(True, which="both")
        # save_path = Path('frm') / 'Combined_func' / 'PRBS_FRF_plot.png'
        plt.savefig('PRBS_FRF_plot.png')
        plt.show()

    @staticmethod
    def sine_fit_alg(t, f, r, y):
        V = np.transpose(np.array([np.cos(2 * np.pi * f * t),
            np.sin(2 * np.pi * f * t), np.ones(t.shape)]))
        # print(V.shape)
        P = np.linalg.pinv(V)
        x_meas, x_ref = np.matmul(P, y), np.matmul(P, r)
        A_meas = np.linalg.norm(x_meas[0:2], 2)
        ph_meas = np.arctan2(-x_meas[1], x_meas[0])
        A_ref = np.linalg.norm(x_ref[0:2], 2)
        ph_ref = np.arctan2(-x_ref[1], x_ref[0])

        Amplitude, Phase = A_meas / A_ref, ph_meas - ph_ref
        return Amplitude, Phase

    @staticmethod
    def sine_fit(path, pars):
        import re
        import os
        from os import listdir
        from os.path import isfile, join

        os.chdir(path)
        onlyfiles1 = [f for f in listdir(path) if isfile(join(path, f))]

        onlyfiles = [x for x in onlyfiles1 if not 'DC' in x]

        freq = np.empty(len(onlyfiles))
        amp = np.empty(len(onlyfiles))
        phase = np.empty(len(onlyfiles))
        for i in range(len(onlyfiles)):

            with open(onlyfiles[i]) as json_file:
                data = json.load(json_file)
            X = data['signals']
            Ts = data['period']
            for ii in range(len(X)):
                if X[ii]['name'] == pars.ref_mode:
                    in_array = X[ii]['samples']
                if X[ii]['name'] == pars.meas_mode:
                    out_array = X[ii]['samples']

            get_floats = re.findall(r'[-+]?\d*\.\d+|\d+', onlyfiles[i])
            freq[i] = float(get_floats[-1])

            t = np.arange(0, Ts * (len(in_array) - 1), Ts)
            r, y = in_array[0:len(t)], out_array[0:len(t)]
            amp[i], phase[i] = Frm_methods.sine_fit_alg(t, freq[i], r, y)

        phase = np.unwrap(phase)
        # G = amp * np.exp(1j * phase)

        f = freq
        amp_db = 20 * np.log10(np.absolute(amp))
        phase_deg = (180 / np.pi) * phase
        UiParams(freq=f, amp=amp_db, phase=phase_deg).to_fortlogs()

        font = {'family': 'monospace',
                'weight': 'bold',
                'size': 15}

        plt.rc('font', **font)
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.semilogx(freq, amp_db)
        plt.ylabel('Magnitude [dB]')
        plt.grid(True, which="both")

        plt.subplot(2, 1, 2)
        plt.semilogx(freq, phase_deg)
        plt.ylabel('Phase [deg]')
        plt.xlabel('Frequency [Hz]')
        plt.grid(True, which="both")

        dirName = '/afs/cern.ch/work/a/anicolet/private/fgc/sw/clients/python/' \
            + 'regulation_toolbox/Sine_FRF_plot.png'
        # os.chdir(dirName)
        plt.savefig(dirName)
        plt.show()

        # f_max = freq[-1]; f_sample_max = 2*f_max; f_vec1 = np.zeros(len(freq)-1)
        # f_vec = np.insert(f_vec1, 0, f_sample_max)
        # M = np.transpose(np.array([freq,amp,phase,f_vec]))
        # np.savetxt('Sine_FRF_file.csv', M, fmt='%10.10f', delimiter=',',
        # 	header='Frequency [Hz] | Amplitude [-] | Phase [rad] | Max sampling rate [Hz]')


# PRBS(path_json_prbs, 'V_REF', 'I_MEAS')
# sine_fit(path_json_sine, 'I_REF_ADV', 'I_MEAS_REG')

# print('\n' 'End of script')
