__version__ = "0.0.5"

__authors__ = """
    Carlos Ghabrous Larrea,
    Nuno Laurentino Mendes,
    Joao Afonso,
    Benjamin Raymond,
    Tsampikos Livisianos
"""

__emails__ = """
    carlos.ghabrous@cern.ch,
    nuno.laurentino.mendes@cern.ch,
    joao.afonso@cern.ch,
    benjamin.raymond@cern.ch
"""
