import pyfgc_const

def test_cause_enum():
    assert pyfgc_const.Cause.DIM.value == 'DIM'
    assert pyfgc_const.Cause.SlowAbort == 'SlowAbort'
    assert pyfgc_const.Cause.PostMortem == 'PostMortem'
    assert pyfgc_const.Cause.VSRegDsp == 'VSRegDsp'

