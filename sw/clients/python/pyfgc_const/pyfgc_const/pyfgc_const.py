import enum
import os

# Bytes of padding before class data in fgc_stat structure
FGC_STAT_PRECLASS_PAD = 12

# Size of status block for device
FGC_STATUS_SIZE = 56

# Maximum device name length
FGC_MAX_DEV_LEN = 31

# Maximum number of FGCDs
FGC_MAX_FGCDS = 400

# Maximum number of FGC devices in FGC-Ether bus
FGCD_MAX_DEVS = 65

# Name file location
if os.name == "nt":
    FGC_NAME_FILE = "//cs-ccr-samba1/pclhc/etc/fgcd/name"
else:
    FGC_NAME_FILE = "/user/pclhc/etc/fgcd/name"

FGC_NAME_FILE_URL = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/name"

# Subdevice base path location
if os.name == "nt":
    FGC_SUB_NAME_FILE_BASE = "//cs-ccr-samba1/pclhc/etc/fgcd/sub_devices/"
else:
    FGC_SUB_NAME_FILE_BASE = "/user/pclhc/etc/fgcd/sub_devices/"

FGC_SUB_NAME_URL_BASE = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/sub_devices/"

# Group file location
if os.name == "nt":
    FGC_GROUP_FILE = "//cs-ccr-samba1/pclhc/etc/fgcd/group"
else:
    FGC_GROUP_FILE = "/user/pclhc/etc/fgcd/group"

FGC_GROUP_FILE_URL = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/group"

# Keep file location
if os.name == "nt":
    FGC_KEEP_FILE = "//cs-ccr-samba1/pclhc/etc/fgcd/keep"
else:
    FGC_KEEP_FILE = "/user/pclhc/etc/fgcd/keep"

FGC_KEEP_FILE_URL = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/keep"

# Keep subdevice base path location
if os.name == "nt":
    FGC_SUB_KEEP_FILE_BASE = "//cs-ccr-samba1/pclhc/etc/fgcd/sub_devices_keep/"
else:
    FGC_SUB_KEEP_FILE_BASE = "/user/pclhc/etc/fgcd/sub_devices_keep/"

FGC_SUB_KEEP_URL_BASE = "http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/sub_devices_keep/"


# FgcLogger error causes - used by FortLogs & FgcLogger
class Cause(str, enum.Enum):
    DIM: str = "DIM"
    SlowAbort: str = "SlowAbort"
    PostMortem: str = "PostMortem"
    VSRegDsp: str = "VSRegDsp"
