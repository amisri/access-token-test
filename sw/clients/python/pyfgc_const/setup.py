"""
setup.py for pyfgc_const.
 
For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/
 
"""
from pathlib import Path
from setuptools import setup, find_packages
 
 
HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()
 
 
about = {}
with (HERE / 'pyfgc_const' / '__version__.py').open() as f:
    exec(f.read(), about)
 
 
REQUIREMENTS: dict = {
    'core': [
        # 'mandatory-requirement1',
        # 'mandatory-requirement2',
    ],
    'test': [
        'pytest',
        # ' mandatory-requirement1',
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
        'acc_py_sphinx',
        'sphinx_rtd_theme',
    ],
}
 
 
setup(
    name='pyfgc_const',
    version=about['__version__'],
    author=about['__authors__'],
    author_email=about['__emails__'],
    description='SHORT DESCRIPTION OF pyfgc_const',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/pyfgc_const/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/pyfgc_const',
    },
 
    packages=find_packages(),
    python_requires='>=3.7, <4',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
 
    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    tests_require=REQUIREMENTS['test'],
)
