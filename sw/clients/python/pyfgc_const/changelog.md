VERSION 0.0.5; 01.12.2022
    - [EPCCCS-8820]: Change FGC_MAX_DEV_LEN from 23 to 31

VERSION 0.0.4; 02.11.2021
    - [Add constants for fgc keep file]

VERSION 0.0.2; 03.06.2020
    - [Add Cause enum for FgcLogger & FortLogs](https://gitlab.cern.ch/ccs/fgc/-/merge_requests/108)
