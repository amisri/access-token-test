# fgc

PACKAGE CONTAINING AUTO-GENERATED PYTHON MODULES

You can use [Github-flavored Markdown](https://guides.github.com/features/mastering-markdown/)
to write your content.

## Purpose of this project
This package hosts the python autogenerated modules by the XML parser except the ones hosted in pyfgc_decoders.
A test module should be added per autogenerated package. 
