import fgc

def test_fgc_properties_can_be_imported():
    from fgc.properties import fgc_properties
    assert True

def test_fgc_properties_is_dict():
    assert isinstance(fgc.properties.fgc_properties, dict)

def test_fgc_properties_has_minimum_attributes():
    minimum_attributes = ['name', 'group', 'type', 'classes']
    for p, p_dict in fgc.properties.fgc_properties.items():
        for at in minimum_attributes:
            try:
                p_dict[at]
            
            except KeyError:
                assert False
            
            else:
                assert True
