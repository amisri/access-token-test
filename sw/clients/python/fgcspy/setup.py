import setuptools

with open("README.md", "r") as fh:
	description = fh.read()

setuptools.setup(
  name             = "fgcspy",
  version          = "1.0.0",
  author           = "Carlos Ghabrous Larrea, Nuno Laurentino Mendes",
  author_email     = "carlos.ghabrous@cern.ch, nuno.laurentino.mendes@cern.ch",
  description      = description,
  project_urls={
        'Documentation': 'https://acc-py.web.cern.ch/gitlab-mono/ccs/fgc/docs/fgcspy/stable/index.html',
        'Source': 'https://gitlab.cern.ch/ccs/fgc/-/tree/master/sw/clients/python/fgcspy',
    },
  python_requires  = ">=3.6",
  install_requires = ["pyserial>=3.4", "docopt>=0.6", "pyfgc>=1.0", "fortlogs-schemas>=1.3.11"],
  py_modules       = ["fgcspy"],
  packages         = setuptools.find_packages(),
)
