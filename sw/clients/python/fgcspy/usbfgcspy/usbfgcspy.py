import datetime
import errno
import math
import os
import serial
import struct
import time

# Make sure to have the CERN custom package fortlogs_schemas installed / copied to your python virtual environment
import fortlogs_schemas as sc

# Files are saved so that log size do not exceed this duration in seconds
MAX_DURATION_PER_LOG = 20

# Output path for saved files
OUTPUT_FILE_PREFIX = 'spy'
if os.name == "nt":
    # WINDOWS
    OUTPUT_PATH = os.path.dirname(os.path.realpath(__file__)) + '\\'
else:
    # MAC / LINUX
    OUTPUT_PATH = os.path.dirname(os.path.realpath(__file__)) + '/'
    # OUTPUT_PATH = '/nfs/cs-ccr-nfshome/user/poccdev/dropbox/spy_data/usb' # Note: This is a CERN internal unix path.

# Log information
LOG_NAME = 'SPY.MPX'
LOG_VERSION = '2.0'
LOG_PERIOD = 0.001

# Required information to decode the SPY data
CHANNELS = 6
BYTES_TO_READ = 4 * CHANNELS
SYNC_WORD = [b"\xff", b"\xff", b"\xff", b"\xff"]


def reset_connection(connection):
    connection.write(struct.pack("B", 0))
    connection.reset_input_buffer()


def set_desired_sampling_frequency(connection, frequency=1):
    connection.write(struct.pack("B", frequency))


def read_until_header(connection):
    index = 0
    while index < len(SYNC_WORD):
        x = connection.read()
        if x == SYNC_WORD[index]:
            index += 1
        else:
            index = 0


def read_spy(connection, duration: int = 1, frequency: int = 1, relative_origin=None):
    """Read SPY samples for a given duration from serial device and return dictionary.

    :param connection: connection to serial device
    :param duration: duration in seconds of the acquisition
    :param frequency: sampling frequency in kHz
    :param relative_origin: Relative time base
    :return: dictionary with acquisitions samples
    """
    samples = int((frequency * 1000) * duration)
    data = dict()
    absolute_origin = datetime.datetime.now()

    if not relative_origin:
        relative_origin = absolute_origin

    for i in range(samples):
        read_until_header(connection)
        timestamp = relative_origin + i * datetime.timedelta(seconds=(1 / (frequency * 1000)))
        response = connection.read(BYTES_TO_READ)

        if b"".join(SYNC_WORD) in response:
            continue

        try:
            data[timestamp] = [v[0] for v in struct.iter_unpack("!f", response)]

        except struct.error:
            pass

    return data


def read_sample(connection):
    """Read one spy sample from given serial device connection and return it as list of floats.
    :param connection: connection to serial device
    :return: the acquired sample as list of floats
    """
    read_until_header(connection)
    response = connection.read(BYTES_TO_READ)
    sample = []

    # This deals with regfgc3 issue that causes empty frames
    if b"".join(SYNC_WORD) in response:
        return None

    try:
        sample = [v[0] for v in struct.iter_unpack("!f", response)]
    except struct.error:
        pass

    return sample


def get_timestamp(dtime=None):
    """Converts a given datetime object into a float timestamp.
    If no datetime object is provided, it uses the current datetime.
    :param dtime: datetime object to be converted
    :return: float timestamp
    """
    if not dtime:
        dtime = datetime.datetime.now()
    return dtime.timestamp()


def samples_generator(connection):
    """Simple generator function that yields a touple of (timestamp, spysample).
    :param connection: connection to the serial device from which the spysample should be acquired
    :yield: A tuple (timestamp, spysample). The timestamp corresponds to the time when the sample was read and is irrelevant for periodic data.
    """
    while True:
        yield get_timestamp(), read_sample(connection)


def read_log(connection, duration=1):
    """Reads spy samples from given serial device connection for N seconds and stores them in an AnalogLog object.
    :param connection: connection to the serial device from which the spy samples should be acquired
    :param duration: the acquisition duration in seconds
    :return: a fortlogs-schemas AnalogLog object containing the acquired spy samples
    """

    # TODO: The list of signal names is hardcoded here to meet the default SPY.MPX signal list. The signal list of a device can be different though,
    # if the value of SPY.MPX has been changed. Hence, a better approach would be to dynamically read the signal list from the device, or ask the user
    # to provide it in case we can not read the SPY.MPX property value via USB.
    log = {
        'name': LOG_NAME,
        'version': LOG_VERSION,
        'device': connection.portstr,
        'period': LOG_PERIOD,
        'signals': [
            {
                'name': 'V_REF',
                'samples': [],
            },
            {
                'name': 'I_MEAS',
                'samples': [],
            },
            {
                'name': 'B_MEAS',
                'samples': [],
            },
            {
                'name': 'V_MEAS',
                'samples': [],
            },
            {
                'name': 'REF',
                'samples': [],
            },
            {
                'name': 'ERR',
                'samples': [],
            },
        ]
    }

    time_start = time.time()
    raw_samples = samples_generator(connection)
    while time.time() < time_start + duration:
        timestamp, data = next(raw_samples)
        if not data:
            continue
        for sig_idx, sig_value in enumerate(data, start=0):
            log['signals'][sig_idx]['samples'].append(sig_value)

    return sc.AnalogLog(**log)


def mkdir(dir_name):
    try:
        os.makedirs(dir_name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


if __name__ == "__main__":
    import sys

    # Parse command line arguments
    try:
        port_name = sys.argv[1]
    except IndexError:
        print("Usage:   usbfgcspy.py PATH_TO_USB_PORT [DURATION in sec] [json, csv, print]")
        print("Examples:")
        print("- Linux:   usbfgcspy.py /dev/ttyUSB1 5 json")
        print("- Windows: usbfgcspy.py COM4 2 csv")
        sys.exit(2)

    try:
        total_duration = 0
        total_duration = int(sys.argv[2])
    except IndexError:
        print(f"Warning: no duration argument specified. Using default '{total_duration}' for infinite duration.")
    except ValueError:
        print("Unsupported duration detected. Please provide an integer to specify the duration. Exiting...")
        sys.exit(2)

    try:
        format = 'json'
        format = sys.argv[3]
    except IndexError:
        print(f"Warning: no format argument specified. Using default '{format}'.")

    supported_formats = ['print', 'json', 'csv']
    if format not in supported_formats:
        print(f"Unsupported format detected. Supported formats are '{supported_formats}'. Exiting...")
        sys.exit(2)

    # Decide in advance the number of logs and how long all logs should be
    if format == 'print':
        num_of_logs = 1
        duration_per_log = math.inf
    else:
        num_of_logs = sys.maxsize
        if total_duration and total_duration > 0:
            num_of_logs = math.ceil(total_duration / MAX_DURATION_PER_LOG)
        duration_per_log = MAX_DURATION_PER_LOG

        num_of_leading_zeros = len(str(num_of_logs))
        mkdir(OUTPUT_PATH)

        print(f"Log files will be stored at {OUTPUT_PATH}")

    if format != 'print':
        print("Starting acquisition...")

    # Keep track of how much data we still need to read
    remaining_duration = total_duration

    # Connect to the serial port
    with serial.Serial(port_name, timeout=5) as connection:

        # Initialize the connection
        reset_connection(connection)
        set_desired_sampling_frequency(connection)

        # For as many logs as we need
        for i in range(num_of_logs):

            # Decide current log duration
            duration = remaining_duration
            if duration > duration_per_log or duration <= 0:
                duration = duration_per_log
                remaining_duration = remaining_duration - duration_per_log

            # Read the log data
            log = read_log(connection, duration)

            # If printing data, print it and continue the loop. Technically, loop runs once when format is print so this could be a break.
            if format == 'print':
                print(log.json())
                continue

            # At this step, we need to save the log either in json/csv format
            file_name = OUTPUT_FILE_PREFIX + '_' + str(i).zfill(num_of_leading_zeros)
            print(f"Generating log file {i+1} / {num_of_logs} as {file_name}.{format} | [{duration} s]")

            if format == 'csv':
                log.to_csv(filename=(OUTPUT_PATH + file_name))
            else:
                log.to_json(filename=(OUTPUT_PATH + file_name))

        reset_connection(connection)

    if format != 'print':
        print("Done")
