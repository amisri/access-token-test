"""fgcspy

Usage:
    fgcspy.py <port> [-v][-f][-a=<A>][-r=<R>][-n=<N>][-d=<D>][-p=<P>][-o=<O>][<[SIGNALS_LIST...]>]
    fgcspy.py -h | --help

Options: 
    -h --help       Show this help.
    -v --verbosity  Increase output verbosity [default: INFO].
    -f              Generate fake data [default: False].
    -a A            Archived data file base name [default: spydata-].
    -r R            Rolling data file name [default: spydata.csv].
    -n N            Length of the archived data files, in seconds [default: 10].
    -d D            Length of the rolling data file, in seconds [default: 20].
    -p P            Refresh rate of the rolling data file, in seconds [default: 1].
    -o O            Output directory for the log files [default: $HOME]
    SIGNALS_LIST    Signals to override the default signal list, in reversed order [SPY.MPX[5] SPY.MPX[4] ... SPY.MPX[0]].
"""

import datetime
import itertools
import logging
import os
import queue
import random
import shutil
import sys
import threading
import time
from decimal import Decimal
from typing import List, Tuple

import docopt
import serial

from usbfgcspy import usbfgcspy

# Globals
signal_names_list = "I_REF I_MEAS V_REF V_MEAS I_A I_B".split()

# Constants
SCRIPT_DESCRIPTION               = "Acquire SPY module"
FILE_SUFFIX                      = ".csv"
DEFAULT_SEC_TO_ACQUIRE_FROM_FGC  = 1
LIMITS_MAX_TIME_ARGUMENTS        = 60
ARCHIVE_FILE_FIRST_LINE          = "TIME," + ",".join(signal_names_list)
ARCHIVE_FILE_DATE_FORMAT         = "%Y-%m-%dT%H:%M:%S"
if os.name == "nt":
    ARCHIVE_FILE_DATE_FORMAT = "%Y-%m-%dT%H-%M-%S"

# Classes used in the program for the different threads
class CleanThread(threading.Thread):
    
    def __init__(self, name=None, target=None):
        super().__init__(name=name)
        self.target     = target
        self.run_event  = threading.Event()

    def run(self, args):
        logging.info(f"{self.name}: thread started")

        try:
            while not self.run_event.is_set():
                self.target(args)

        finally:
            self.clean_resources()

    def terminate(self):
        logging.debug(f"{self.name}: received terminate event")
        self.run_event.set()

    def init_resources(self):
        pass

    def clean_resources(self):
        pass

class SpyUsbProducer(CleanThread):

    def __init__(self, name="Producer->FGC data", target=None, args=()):
        super().__init__(name=name, target=target)
        self._port_name, self._buffers, self._fake_data = args
        self._usb_port  = None
        
    def run(self):
        if not self._fake_data:
            self.init_resources()

        super().run((self._usb_port, self._buffers))

    def init_resources(self):
        try:
            self._usb_port = serial.Serial(self._port_name, timeout=1)

        except serial.SerialException as se:
            logging.error(se)
            self.clean_resources()
        
        else:
            usbfgcspy.reset(self._usb_port)
            usbfgcspy.sampling_frequency(self._usb_port)

    def clean_resources(self):
        logging.debug(f"{self._name}: cleaning resources.")
        try:
            usbfgcspy.reset(self._usb_port)
            self._usb_port.close()
        
        except AttributeError as ae:
            logging.warning(f"{self.name}: exception while closing serial port {self._port_name}: {ae}")

class ArchiveConsumer(CleanThread):
    
    def __init__(self, name="Consumer->Archiving", args=()):
        super().__init__(name=name)
        self._output_dir, self._file_name_original, self._data_buffer, self._period = args
        self._fh        = None
        self._file_name = None

    def run(self): 
        logging.info(f"{self.name}: thread started")
        next_acq_ts, sec_to_discard = _calculate_first_acquisition_ts_and_seconds_to_discard(self._period)

        logging.info(f"{self.name}: first acquisition will be at ts: {next_acq_ts}, discard(sec) from archived buffer: {sec_to_discard}")

        counter = itertools.count()
        data_for_file = list()

        while not self.run_event.is_set():
            logging.debug(f"Archived data buffer has length: {self._data_buffer.qsize()}")

            # Discard samples if needed
            if sec_to_discard:
                sec_to_discard -= 1
                continue

            try:
                data = self._data_buffer.get(timeout=5)

            except queue.Empty:
                logging.info(f"{self.name} queue is empty. Will stop executing thread now")
                break

            data_for_file.append(data)
            self._data_buffer.task_done()
            logging.debug(f"Data list for archive file has length: {len(data_for_file)}")
            if len(data_for_file) < self._period:
                continue

            my_count = next(counter)
            file_name_date = next_acq_ts + datetime.timedelta(seconds=my_count * self._period)
            self._file_name = (self._file_name_original
                        + file_name_date.strftime(ARCHIVE_FILE_DATE_FORMAT)
                        + FILE_SUFFIX)

            logging.info(f"{self.name}: opening archive file {self._file_name} for writing")

            global ARCHIVE_FILE_FIRST_LINE
            self.init_resources()
            self._fh.write(ARCHIVE_FILE_FIRST_LINE)
            _write_fgc_data_in_file(self._fh, data_for_file)
            self.clean_resources()

            data_for_file.clear()
        
        self.clean_resources()

    def init_resources(self):
        try:
            self._fh = open(os.path.join(self._output_dir, self._file_name), mode="w")

        except OSError as ose:
            logging.error(ose)
            self.clean_resources()
            raise

    def clean_resources(self):
        logging.debug(f"{self.name}: cleaning resources")
        try:
            self._fh.close()

        except AttributeError as ae:
            logging.warning(f"{self.name}: exception while closing file {self._file_name}: {ae}")

class RollingConsumer(CleanThread):
    
    def __init__(self, name="Consumer->Rolling", target=None, args=()):
        super().__init__(name=name)
        self._output_dir, self._file_name, self._data_buffer, self._periods = args
        self._rolling_data_period, self._refresh_rate = self._periods
        self._fh = None
        self._temp_data_file = os.path.join(self._output_dir, self._file_name) + ".temp"

    def run(self):
        logging.info(f"{self.name}: thread started")

        counter = 0
        data_for_file = list()

        while not self.run_event.is_set():
            try:
                data = self._data_buffer.get(timeout=5)

            except queue.Empty:
                logging.info(f"{self.name}: queue is empty. Will stop executing thread now")
                break

            data_for_file.append(data)
            self._data_buffer.task_done()
            if counter < self._refresh_rate:
                counter += 1
                continue

            counter = 0

            # Time to refresh the file
            logging.info(f"{self.name}: refreshing the rolling file ({self._refresh_rate} seconds)")
            global ARCHIVE_FILE_FIRST_LINE
            self.init_resources()
            self._fh.write(ARCHIVE_FILE_FIRST_LINE)
            _write_fgc_data_in_file(self._fh, data_for_file)
            self.clean_resources()
            shutil.move(self._temp_data_file, os.path.join(self._output_dir, self._file_name))

            if len(data_for_file) >= self._rolling_data_period:
                logging.info(f"{self.name}: maximum size of rolling file reached. Let's roll it.")
                for i in range(self._refresh_rate):
                    _ = data_for_file.pop(i)
                
                data_for_file.clear()
        
        self.clean_resources()

    def init_resources(self):
        try:
            self._fh = open(os.path.join(self._output_dir, self._temp_data_file), mode="w")

        except OSError as ose:
            logging.error(ose)
            self.clean_resources()
            raise

    def clean_resources(self):
        logging.debug(f"{self.name}: cleaning resources.")
        try:
            self._fh.close()

        except AttributeError as ae:
            logging.warning(f"{self.name}: exception while closing file {self._file_name}: {ae}")

# Target functions for the different threads
def _generate_fake_data(args: Tuple) -> None:
    """Generates random data and stores the data in the buffers shared with the consumer threads.
    
    Arguments:
        args {[type]} -- Tuple containing the port and the data buffer list.
    """
    _, buffers = args
    continue_running = True

    now = datetime.datetime.now()
    fake_data = dict()
    time.sleep(1)

    for _ in range(1000):
        fake_data[now] = [random.uniform(
            0, random.randint(-100, 100)) for i in range(6)]
        now += datetime.timedelta(milliseconds=1)

    for b in buffers:
        b.put(fake_data)

    return continue_running

def update_buffer_from_usb(args: Tuple):
    """Reads from the USB port and stores the data in the buffers shared with the consumer threads.
    
    Arguments:
        args {[type]} -- Tuple containing the port and the data buffer list.
    """

    port, data_buffers = args
    try:
        first_sample = datetime.datetime.now()
        data = usbfgcspy.read_spy(port, relative_origin=first_sample)
        first_sample += datetime.timedelta(seconds=DEFAULT_SEC_TO_ACQUIRE_FROM_FGC)

    except serial.SerialException:
        logging.warning(f"USB acquisition failed! Data buffers will not be updated.")
        time.sleep(DEFAULT_SEC_TO_ACQUIRE_FROM_FGC)

    except AttributeError as ae:
        logging.error(f"USB port object not correctly initialized: {ae}")

    else:
        for b in data_buffers:
            try:
                b.put(data, False)

            except queue.Full:
                logging.warning(f"Data buffer is full! Data will be lost if not consumed first.")

    finally:
        # Put here to minimize error messages on the output to one per second in case of exceptions.
        time.sleep(DEFAULT_SEC_TO_ACQUIRE_FROM_FGC)


# Helper functions (marked as private to identify them easily)
def _calculate_first_acquisition_ts_and_seconds_to_discard(archived_files_period: int) -> Tuple:
    """Returns first time stamp for the archived files.

    Based on the timestamp when the script was launched, it calculates the closest 
    timestamp for the first archived file generation. It also returns the number 
    of data samples to be skipped in this first archived file.  
    
    Arguments:
        archived_files_period {int} -- Data amount (in seconds) in each archived file. 
    
    Returns:
        [tuple] -- first acquisition timestamp, number of samples to skip
    """
    now               = datetime.datetime.now()
    seconds_next_acq  = (((now.second + 10) % 60) // 10) * 10

    if not seconds_next_acq:
        seconds_to_discard = 60 - now.second
    
    else:
        seconds_to_discard = seconds_next_acq - now.second

    first_acq_ts = now + datetime.timedelta(seconds=seconds_to_discard)

    return first_acq_ts, seconds_to_discard

def _check_time_arguments_within_limits(n, d, p) -> bool:
    """Makes sure time arguments are within limits
    
    Arguments:
        n {[type]} -- [description]
        d {[type]} -- [description]
        p {[type]} -- [description]
    
    Returns:
        bool -- Whether time arguments are within limits. 
    """
    global LIMITS_MAX_TIME_ARGUMENTS
    return ((0 <= n <= LIMITS_MAX_TIME_ARGUMENTS)
        and (0 <= d <= LIMITS_MAX_TIME_ARGUMENTS)
        and (0 <= p <= LIMITS_MAX_TIME_ARGUMENTS))

def _replace_original_signal_list(overriden_signals) -> None:
    """Replaces original signal list by the one given by the user.
    
    Arguments:
        overriden_signals {[type]} -- [description]
    
    Returns:
        [type] -- [description]
    """
    global signal_names_list

    if len(overriden_signals) > len(signal_names_list):
        logging.error(f"Only {len(signal_names_list)} signals are allowed (gave {len(overriden_signals)}).")
        sys.exit(2)

    if overriden_signals == signal_names_list:
        logging.debug("Signals were not provided or are the same as the original list.")
        return signal_names_list

    if len(overriden_signals) == len(signal_names_list):
        logging.debug("Signal list provided replaces original one.")
        overriden_signals.reverse()
        return overriden_signals
    
    # We only want to override a subset of the original signals
    new_list = list(signal_names_list)
    for i, el in enumerate(overriden_signals):
        new_list[-(i + 1)] = el
    
    logging.info(f"Signal list modified: {signal_names_list} -> {new_list}")

    # Modify the first line of the file accordingly
    global ARCHIVE_FILE_FIRST_LINE
    ARCHIVE_FILE_FIRST_LINE = "TIME," + ",".join(new_list)
    logging.debug(f"New first line in acquisition files: {ARCHIVE_FILE_FIRST_LINE}")

def _write_fgc_data_in_file(fh, data_list: List) -> None:
    """Takes data from either USB or random generator and stores them in a file.
    
    Arguments:
        fh {file handler} -- File handler for the data file.
        data_list {List} -- List containing in each position a dictionary of datetime - float entries.
    """
    for entry in data_list:
        for ts, sample_list in entry.items():
            sample_list_str = map(lambda x: "{:.7E}".format(Decimal(x)), sample_list)
            new_line = "\n" + str(round(ts.timestamp(), 3)) + "," + ",".join(sample_list_str)
            fh.write(new_line)

def has_living_threads(thread_list) -> bool:
    """Returns true if any of the started threads is running. 
    
    Arguments:
        thread_list {threading.Thread} -- List of threads that have been started.
    
    Returns:
        boolean -- Whether there are threads running
    """
    return True in [t.isAlive() for t in thread_list]

def spy(kwargs) -> None:
    """Initializes threads according to the data input. 
    
    With all three threads active, it starts the USB data acquisition (or fake data generation),
    the thread producing the archived files, and the thread producing the rolling file. 
    """

    port                      = kwargs["<port>"]
    archived_file_name        = kwargs["-a"]
    rolling_file_name         = kwargs["-r"]
    archived_data_period_sec  = int(kwargs["-n"])
    rolling_data_period_sec   = int(kwargs["-d"])
    refresh_rolling_sec       = int(kwargs["-p"])
    fake_data                 = kwargs["-f"]
    output_file_dir           = kwargs["-o"]
    
    if output_file_dir == "$HOME":
        output_file_dir = os.path.expanduser("~")

    data_generator = (fake_data == 0) and update_buffer_from_usb or _generate_fake_data
    data_generator_string = (fake_data == 0) and "FGC-USB" or "FAKE"
    logging.info(f"Data generator: {data_generator_string}")

    # List of buffers and threads
    buffers     = list()
    spy_threads = list()

    # Consumer #1
    if archived_data_period_sec:
        archived_data_buffer = queue.Queue(maxsize=archived_data_period_sec)
        buffers.append(archived_data_buffer)

        archiving_thread = ArchiveConsumer(args=(output_file_dir, archived_file_name, archived_data_buffer, archived_data_period_sec))
        spy_threads.append(archiving_thread)

    # Consumer #2
    if rolling_data_period_sec and refresh_rolling_sec:
        rolling_data_buffer = queue.Queue(maxsize=rolling_data_period_sec)
        buffers.append(rolling_data_buffer)

        rolling_thread = RollingConsumer(args=(output_file_dir, rolling_file_name, rolling_data_buffer, (rolling_data_period_sec, refresh_rolling_sec)))
        spy_threads.append(rolling_thread)

    # Producer
    acquiring_thread = SpyUsbProducer(target = data_generator,
                                        args=(port, buffers, fake_data))
                                        
    spy_threads.append(acquiring_thread)

    # Start threads
    for t in spy_threads:
        t.start()

    while has_living_threads(spy_threads):
        try:
            [t.join() for t in spy_threads
            if t is not None and t.isAlive()]
        
        except KeyboardInterrupt:
            logging.info("Ctrl-C received: terminating producer thread...")
            acquiring_t = spy_threads.pop()
            acquiring_t.terminate()
            acquiring_t.join()

            logging.info("Waiting consumers to finish pending tasks...")
            for b in buffers:
                b.join()

            logging.info("Pending tasks finished. Joining consumer threads")
            for t in spy_threads:
                t.terminate()
                t.join()

    logging.info("Threads have been killed. Exiting main program...")

def check_input(args):
    default_severity = (args["--verbosity"] == True) and logging.DEBUG or logging.INFO
    FORMAT = "[%(asctime)s] - [%(levelname)8s]: %(message)s"
    logging.basicConfig(format=FORMAT,
                        level=default_severity,
                        datefmt="%H:%M:%S")

    running_condition = (args["-n"] or (args["-d"] and args["-p"]))

    if not running_condition:
        logging.warning(f"Archived/Rolling/Refresh periods combination does not allow the script to run!")
        logging.info(f"The script will exit now.")
        sys.exit(2)

    if not _check_time_arguments_within_limits(int(args["-n"]), int(args["-d"]), int(args["-p"])):
        logging.error("Values for either -n, -d or -p parameters not within limits (min: 0; max: 60)")
        sys.exit(2)

    logging.debug("Arguments are correct")
    _replace_original_signal_list(args["SIGNALS_LIST"])
    return args
    

if __name__ == "__main__":
    docopt_args = docopt.docopt(__doc__)
    spy(check_input(docopt_args))
