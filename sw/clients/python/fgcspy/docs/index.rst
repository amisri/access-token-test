.. pyfresco documentation master file, created by
   sphinx-quickstart on Wed Jun 24 16:42:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FGCSpy
==================================================================

This python tool allows to get data from FGCs using the serial port.

Download
------------

Download and extract `this archive <./fgcspy.zip>`_. The application entry-point is named usbfgcspy.py.

How to use
------------

To use the tool, you need to prepare a python virtual environment with the following libraries installed:

- pyserial>=3.4
- docopt>=0.6
- pydantic>=1.9.0
- pandas>=0.25.2
- orjson>=3.4.0
- fortlogs_schemas (supplied in the archive)
- pyfgc_const (supplied in the archive)

Example:

.. code-block:: console

   $ python3 -m venv venv
   $ source venv/bin/activate    (On windows: .\venv\Scripts\activate)
   $ pip install -U pip
   $ pip install pyserial docopt pydantic pandas orjson

Then copy the supplied dependencies fortlogs_schemas and pyfgc_const to the lib directory of the created virtual environment:

.. code-block:: console

   $ cp -R fortlogs_schemas venv/lib/*/site-packages    (On windows: copy fortlogs_schemas venv\lib\*\site-packages)
   $ cp -R pyfgc_const venv/lib/*/site-packages         (On windows: copy pyfgc_const venv\lib\*\site-packages)

To see command line parameters and usage examples, run:

.. code-block:: console

   $ python3 usbfgcspy.py
   Usage:     usbfgcspy.py PATH_TO_USB_PORT [DURATION in sec] [json, csv, print]
   Examples:
   - Linux:   usbfgcspy.py /dev/ttyUSB1 5 json
   - Windows: usbfgcspy.py COM4 2 csv


Duration
------------

The log acquisition will be automatically stopped after the specified duration.


Formats
------------

The json and csv formats are `documented here <https://ccs-public-html.app.cern.ch/assets/powerspy/data-format.pdf>`_.
When specified format is "print", the json format is printed to the console once all data is acquired, and only one log will be produced.

File output path
------------------

By default, the generated files will be stored in the same directory that the script resides in.

You can change this path by changing the OUTPUT_PATH variable of the script.

This variable holds the location where the files will be stored, when the specified format is either json or csv.

Please note: Make sure to use the correct path delimiters: '/' on Linux/Mac and '\\' on Windows
