# pyfgc

pyfgc is a Python library to interface Function Generation Controllers (FGCs)

Contents

1. Main features
2. Installation 
3. Usage
4. Additional modules and packages