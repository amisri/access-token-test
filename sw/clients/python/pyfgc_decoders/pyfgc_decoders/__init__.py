from .__version__ import __version__, __emails__, __authors__

from .fgc_status_decoder import decoders, decode
