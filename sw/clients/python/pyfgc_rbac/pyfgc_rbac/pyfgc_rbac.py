import base64
import getpass
import requests
import time

from enum import Enum
from requests.packages.urllib3.exceptions import InsecureRequestWarning


# Global constants
SERVER = "rbac-pro-lb.cern.ch"
PORT = 8443
KERBEROS_RBAC_SERVER = f"RBAC/{SERVER}"
RBAC_OAUTH2_AUDIENCE = "rbac-pro"
OAUTH_HOST="auth.cern.ch"

# This is the exponent and modulus of the public key used to encrypt the token request
# The public key is available at https://gitlab.cern.ch/acc-co/cmw/cmw-core/-/tree/develop/cmw-rbac-py/pyrbac/keys
EXPONENT_STR = "10001"
MODULUS_STR = "9a9da786ba030e94c891ed02081b5753f1adb48b9e1aabd40ad7464e2c408777d5fd93b9c92182773f31595ee2589d88732e37e963a9d2e7a2a207d58a03666f"


def set_rbac_server(rbac_server):
    global SERVER
    SERVER = rbac_server


def set_rbac_port(rbac_port):
    global PORT
    PORT = rbac_port 


def set_kerberos_rbac_server(kerberos_rbac_server):
    global KERBEROS_RBAC_SERVER
    KERBEROS_RBAC_SERVER = kerberos_rbac_server 


def set_rbac_audience(rbac_aud):
    global RBAC_OAUTH2_AUDIENCE
    RBAC_OAUTH2_AUDIENCE = rbac_aud


def set_oauth_host(oauth_host):
    global OAUTH_HOST
    OAUTH_HOST = oauth_host


def set_exponent(exponent):
    global EXPONENT_STR
    EXPONENT_STR = exponent


def set_modulus(modulus):
    global MODULUS_STR
    MODULUS_STR = modulus


# If GSSAPI is not available, mock GSSError
try:
    import gssapi
    GSSError = gssapi.raw.misc.GSSError
except (OSError, ImportError):
    import warnings
    warnings.warn("The package `gssapi` is not installed in the current environment, please do so if you need to use authentication via kerberos tickets.")
    class GSSError(Exception):
        pass


class KerberosUnavailableError(Exception):
    pass


class RbacServerError(Exception):
    pass


class TokenType(Enum):
    """[summary]

    [description]

    Extends:
        Enum

    Variables:
        APPLICATION_TOKEN {number} -- [description]
        MASTER_TOKEN {number} -- [description]
        LOCAL_MASTER_TOKEN {number} -- [description]
    """
    APPLICATION_TOKEN = 0
    MASTER_TOKEN = 1
    LOCAL_MASTER_TOKEN = 2


def get_token_location():
    """[summary]

    [description]

    Returns:
        [type] -- [description]
    """
    return _request_token()


def _exchange_oauth2_token(source_aud, target_aud, token, source_secret=None):
    data = {
        'client_id': source_aud,
        'subject_token': token,
        'audience': target_aud,
        'grant_type': 'urn:ietf:params:oauth:grant-type:token-exchange',
        'requested_token_type': 'urn:ietf:params:oauth:token-type:refresh_token'
    }
    
    if source_secret is not None:
        data['client_secret'] = source_secret

    response = requests.post(
        f'https://{OAUTH_HOST}/auth/realms/cern/protocol/openid-connect/token',
        data=data, 
        headers={'Content-Type': 'application/x-www-form-urlencoded'}, 
        verify=False,
    )

    result = response.json()

    return result


def get_token_oauth2(token, token_audience, token_secret=None):
    """
    Get a RBAC token from a valid OAuth2 token.
    The audience (e.g. application_id) of the given OAuth2 token must be allowed for exchange to the RBAC_OAUTH2_AUDIENCE audience (e.g. requested and approved in the CERN application portal).
    If the audience is an application and is configured to store the client_secret on the server side, then it should be provided here.

    :param token: A valid OAuth2 token
    :param token_audience: Audience of the given token. Must be either RBAC_OAUTH2_AUDIENCE or an audience which can be exchanged to RBAC_OAUTH2_AUDIENCE
    :param token_secret: The client secret that was used to obtain the oauth2 token. Only needed if the application is configured to store their unique secret by itself.
    :return: A valid RBAC token
    """
    if token_audience != RBAC_OAUTH2_AUDIENCE:
        token = _exchange_oauth2_token(token_audience, RBAC_OAUTH2_AUDIENCE, token, token_secret)['access_token']
    form = {"access_token": token}
    return _request_token(form)


def get_token_kerberos():
    """
    Get a RBAC token by exchanging a valid kerberos ticket.
    
    :return: A valid RBAC token
    """

    try:
        import gssapi
    except (OSError, ImportError):
        raise KerberosUnavailableError("The package `gssapi` is not installed in the current environment, please do so if you need to use authentication via kerberos tickets.")

    service_name = gssapi.Name(KERBEROS_RBAC_SERVER, name_type=gssapi.NameType.krb5_nt_principal_name)
    security_context = gssapi.SecurityContext(name=service_name, usage="initiate")

    try:
        kerberos_ticket = security_context.step(None)
    except GSSError:
        raise KerberosUnavailableError()

    form = {
        "Krb5Ticket": base64.b64encode(kerberos_ticket),
    }

    return _request_token(form)


def get_token_saml(saml_response, lifetime_s=720, token_type=TokenType.APPLICATION_TOKEN.value):
    """[summary]

    [description]

    Arguments:
        saml_response {[type]} -- [description]

    Keyword Arguments:
        lifetime_s {number} -- [description] (default: {720})
        token_type {number} -- [description] (default: {0})

    Returns:
        [type] -- [description]
    """

    form = {
        "SamlResponse": base64.b64decode(saml_response),
        "TokenType": token_type,
        "Lifetime": lifetime_s
    }

    return _request_token(form)


def get_token_login(username, password):
    """[summary]

    [description]
    """
    form = {"UserName": username, "Password": password}
    return _request_token(form)


def renew_token(token, lifetime_s=720, token_type=TokenType.APPLICATION_TOKEN.value):
    """[summary]

    [description]

    Arguments:
        token {[type]} -- [description]

    Keyword Arguments:
        lifetime_s {number} -- [description] (default: {720})
        token_type {[type]} -- [description] (default: {TokenType.APPLICATION_TOKEN})
    """

    form = {
        "Origin": base64.b64encode(token),
        "TokenType": token_type,
        "Lifetime": lifetime_s
    }

    return _request_token(form)


def token_to_dict(binary_token):
    """[summary]

    [description]

    Arguments:
        token {[type]} -- [description]
    """

    token_fields = binary_token.split(b"\n")
    num_fields = int(token_fields[0].decode())

    token_dict = dict()

    field_ptr = 1
    num_elements = 1

    for _ in range(num_fields):
        is_array = False
        field, field_type = token_fields[field_ptr: field_ptr + 2]
        type_base = field_type.decode()

        field_ptr += 2

        if field_type == b"byte_array":
            type_base = "byte"
            field_ptr += 1

        elif field_type.endswith(b"_array"):
            type_base = field_type[:len("_array")].decode()
            is_array = True
            num_elements = int(token_fields[field_ptr].decode())
            field_ptr += 1

        elif field_type == b"string":
            field_ptr += 1

        value_list = list()
        if is_array:

            for j in range(num_elements):
                value_list.append(_element_to_native(field_type, type_base, token_fields[field_ptr]))
                field_ptr += 1

        else:
            value = _element_to_native(field_type, type_base, token_fields[field_ptr])
            field_ptr += 1

        token_dict[field.decode()] = (value_list) and value_list or value

    return token_dict


def reduce_roles(token, roles):
    """[summary]

    [description]

    Arguments:
        token {[type]} -- [description]
        roles {[type]} -- [description]
    """

    form = {"Origin": base64.b64encode(token), "Role": roles and roles or ["none"]}
    return _request_token(form)


def _request_token(form=None):
    """[summary]

    [description]

    Arguments:
        form {[type]} -- [description]
    """

    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    if form is None:
        form = dict()

    try:
        form["SamlResponse"]

    except KeyError:

        try:
            form["Origin"]

        except KeyError:
            form["AccountName"] = getpass.getuser()

    form["TokenFormat"] = "TEXT"


    try:
        r = requests.post("https://{}:{}/rba/".format(SERVER, PORT), data=form, timeout=10, verify=False)
        r.raise_for_status()

    except requests.exceptions.RequestException as e:
        try:
            message = r.json().get('message', e)
        except:
            message = e
        # Note: invalid username/password results in a 401 Unauthorized error
        raise RbacServerError('Could not get RBAC token: {}'.format(message)) from e

    else:
        return base64.b64decode(r.text)


def _element_to_native(field_type, type_base, element):
    if type_base == "bool":
        return (element == "true") and True or False

    if type_base in ["short", "int", "long"]:
        return int(element.decode())

    if type_base == "byte":
        return element

    if type_base == "double":
        return float(element.decode())

    if type_base == "string":
        return element.decode().replace('%2E', '.').replace('%2D', '-').replace('%20', ' ')

    raise TypeError("Unknown type {} unpacking RBAC token".format(field_type))


def verify_signature(token) -> bool:
    """
    Verify the signature of a given RBAC token using the public key of the RBAC server.
    
    
    :param token: A valid RBAC token
    :return: boolean telling if the signature is valid or not
    """

    try:
        from cryptography.hazmat.backends import default_backend
        from cryptography.hazmat.primitives import serialization
        from cryptography.hazmat.primitives.asymmetric import rsa
        from cryptography.hazmat.primitives import hashes
        from cryptography.hazmat.primitives.asymmetric import padding
        from cryptography.exceptions import InvalidSignature
    except ImportError:
        raise ImportError("cryptography is required to verify RBAC token signature")

    modulus = int(MODULUS_STR, 16)
    exponent = int(EXPONENT_STR, 16)

    public_key = rsa.RSAPublicNumbers(exponent, modulus).public_key(default_backend())

    whole_message = base64.b64decode(token)

    signature_length_size = 4
    signature_length = int.from_bytes(whole_message[-signature_length_size:], byteorder='little')
    message = whole_message[:-(signature_length+signature_length_size)]
    signature = whole_message[-(signature_length+signature_length_size):-signature_length_size]

    try:
        public_key.verify(
            signature,
            message,
            padding.PKCS1v15(),
            hashes.SHA1()
        )
        return True
    except Exception:
        return False


def is_expired(token) -> bool:
    """
    Check if a given RBAC token is expired or not.

    :param token: A valid RBAC token
    :return: boolean telling if the token is expired or not
    """
    decoded_token = base64.b64decode(token)
    token_dict = token_to_dict(decoded_token)
    return float(token_dict['ExpirationTime']) < time.time()


def includes_roles(token, roles) -> bool:
    """
    Check if a given RBAC token includes the given roles.

    :param token: A valid RBAC token
    :param roles: A list of roles to check
    :return: boolean telling if the token includes the roles or not
    """
    decoded_token = base64.b64decode(token)
    token_dict = token_to_dict(decoded_token)
    return set(roles).issubset(token_dict['Roles'])

