__version__ = "1.6.0"

__authors__ = """
    Carlos Ghabrous Larrea
"""

__emails__ = """
    carlos.ghabrous@cern.ch
"""