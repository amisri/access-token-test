import getpass
import time
import pyfgc_rbac
import pytest
import os


RUNNING_IN_CI = os.environ.get("CI_SERVER", "no")
if RUNNING_IN_CI == "yes":
    pytest.skip("Running in CI...", allow_module_level=True)


def test_authenticate_location():
    token = pyfgc_rbac.get_token_location()
    assert token is not None
    
    token_dict = pyfgc_rbac.token_to_dict(token)
    assert token_dict["ExpirationTime"] > time.time()


def test_authenticate_login():
    token = pyfgc_rbac.get_token_login(getpass.getuser(), getpass.getpass("pass: "))
    assert token is not None

    token_dict = pyfgc_rbac.token_to_dict(token)
    assert token_dict["ExpirationTime"] > time.time()


def test_authenticate_kerberos():
    token = pyfgc_rbac.get_token_kerberos()
    assert token is not None

    token_dict = pyfgc_rbac.token_to_dict(token)
    assert token_dict["ExpirationTime"] > time.time()
    

def test_renew_token():
    token = pyfgc_rbac.get_token_kerberos()
    assert token is not None

    new_token = pyfgc_rbac.renew_token(token)
    assert new_token is not None

    token_dict = pyfgc_rbac.token_to_dict(token)
    new_token_dict = pyfgc_rbac.token_to_dict(new_token)
    assert token_dict["ExpirationTime"] > token_dict["ExpirationTime"]
