"""
High-level tests for the  package.

"""
import os
import pyfgc_rbac
import pytest
import base64

RUNNING_IN_CI = os.environ.get("CI_SERVER", "no")


def test_version():
    assert pyfgc_rbac.__version__ is not None

@pytest.mark.skipif(RUNNING_IN_CI == "yes", reason="Running in CI...")
def test_signature():
    RBAC_TOKEN = pyfgc_rbac.get_token_kerberos()
    assert pyfgc_rbac.verify_signature(RBAC_TOKEN)

@pytest.mark.skipif(RUNNING_IN_CI == "yes", reason="Running in CI...")
def test_is_expired():
    RBAC_TOKEN = pyfgc_rbac.get_token_kerberos()
    assert pyfgc_rbac.is_expired(RBAC_TOKEN) is False

@pytest.mark.skipif(RUNNING_IN_CI == "yes", reason="Running in CI...")
def test_includes_roles():
    RBAC_TOKEN = pyfgc_rbac.get_token_kerberos()
    assert pyfgc_rbac.includes_roles(RBAC_TOKEN, ["CCS-USER", "PO-Data-Expert"])
