VERSION 1.6.0; 30.05.2023
    - Add methods to verify the validity of a token, expiration and role-matching.

VERSION 1.5.3; 12.05.2023
    - Make possible to pass rbac_audience and oauth_host as parameters. 
    - Make global parameters settable from outside.

VERSION 1.4.6; 10.11.2022
    - Make `gssapi` dependency optional with possible installation target `kerberos`;
    - Provide warning if `gssapi` dependency is not found in the current environment;

VERSION 1.4.4; 10.11.2022
    - Fix default get-token_saml/renew_token token_type parameter
    - Refactor internal function name

VERSION 1.4.3; 25.02.2022
    - Remove gssapi dependency for windows hosts

VERSION 1.4.2; 23.02.2022
    - Change Rbac OAuth2 audience to rbac-pro

VERSION 1.4.1; 10.02.2022
    - Remove an unintended dependency on Kerberos for Windows

VERSION 1.4.0; 14.01.2022
    - Add support for kerberos ticket exchange.

VERSION 1.2.0; 07.10.2020
    - Add optional client_secret parameter to the token exchange function to support applications that store their client secret themselves.

VERSION 1.1.0; 06.05.2020
    - [EPCCCS-XXXX] Added new exception pyfgc_rbac.RbacServerError.

VERSION 1.0.0
    - [EPCCCS-7455] Rename to pyfgc_rbac
