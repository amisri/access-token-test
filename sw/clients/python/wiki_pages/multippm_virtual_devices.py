# Filename: multippm_virtual_devices.py
#
# Purpose:  Parse /user/pclhc/etc/fgcd/sub_devices/ files and generate an html table
#           with all the multi-ppm devices and respective timing conditions
#
# Location: Run $make install to deploy the script to ~pclhc/lib/python/packages/wiki_pages

import time
from pathlib import Path
from typing import Dict

import data_parser

# List of multi-ppm devices, composed by tuples (FEC, DEVICE, NUMBER, ALIAS, TIMING CONDITIONS)
mppm_devices = list()
max_timing_conditions = 0


def fgcs_in_fec(index=0, fec=''):
    fgcs = set()
    while mppm_devices[index][0] != fec and index < len(mppm_devices):
        index += 1
    while index < len(mppm_devices):
        if mppm_devices[index][0] == fec:
            fgcs.add(mppm_devices[index][1])
        else:
            break
        index += 1
    return fgcs


def timing_conditions_in_fec(index=0, fec=''):
    timing = list()
    while mppm_devices[index][0] != fec and index < len(mppm_devices):
        index += 1
    while index < len(mppm_devices):
        if mppm_devices[index][0] == fec:
            condition = mppm_devices[index][4]
            if condition not in timing:
                timing.append(mppm_devices[index][4])
        else:
            break
        index += 1
    return timing


def print_table():
    global max_timing_conditions

    print('<style>\n'
          '    table { border-collapse: collapse;}\n'
          '    .loc { background-color: #f4f5f7; padding: 15px; border-right: 3px solid white; white-space: nowrap;}\n'
          '    tbody { border-top: 3px solid white; }\n'
          '    tbody>tr:nth-child(odd) {background-color: #e2e4e9;}\n'
          '    tbody>tr:nth-child(even) {background-color: #f4f5f7;}\n'
          '    tbody>tr>td {padding-right: 50px; border-right: 2px solid white;}\n'
          # '    .device.nth-of-type(odd) {background-color: #e6e6e6;}\n'
          '    td { padding: 8px; text-align: left; }\n'
          '    .subt { text-align: left; background-color: #d7dff4; padding: 8px 15px 8px 15px; }\n'
          '    th {  text-align: left; background-color: #0065ff; padding: 15px; color: white;'
          ' border-bottom: 3px solid white; border-right: 3px solid white;  }'
          '</style>')

    print('<p align="right">Table generated', time.ctime(), '</p>')
    print('<div style="overflow-x:auto;">')
    print('<table>') # border="1" OR style="border:1px solid black; OR border="1px" cellspacing="0""

    print('<thead><tr>'
          '<th>Group</th>'
          '<th>Gateway</th>'
          '<th colspan="5">Devices and timing conditions</th><tr></thead>')

    p_fec, p_dev, p_num, p_alias, p_cond = ['', '', '', '', '']
    i = 0
    while i < len(mppm_devices):
        fec, dev, num, alias, cond = mppm_devices[i]

        # Print new FEC
        if fec != p_fec:
            rows = len(fgcs_in_fec(i, fec)) + 1
            timing = timing_conditions_in_fec(i, fec)
            area, _ = data_parser.targets[fec]
            if i > 0:
                print('</tbody>')
            print('<tbody><tr>')
            print('<td class="loc" rowspan=' + str(rows) + '>' + area + '</td>')
            print('<td class="loc" rowspan=' + str(rows) + '>' + fec + '</td>')
            print('<td class="subt" ><b>Physical device</b></td>')
            for condition in timing:
                condition = '<b>' + condition + '</b>'
                print('<td class="subt">' + condition + '</th>')
            virtual_device_collumns = len(timing) - 1
            while virtual_device_collumns < max_timing_conditions:
                print('<td class="subt"></th>')
                virtual_device_collumns += 1
            print('</tr>')

        # Print new DEVICE and ALIAS
        if dev != p_dev:
            print('    <tr>')
            print('       <td>' + dev + '</td>')
            print('       <td>' + alias + '</td>')

        # Print new ALIAS inside a device, and close the row if next alias is not on the same device
        else:
            print('        <td>'+alias+'</td>')
            # no more virtual device in the current FGC
            if i+1 >= len(mppm_devices) or dev != mppm_devices[i+1][1]:
                # add empty collumns up to the max of timing conditions in the table
                virtual_device_collumns = int(num)
                while virtual_device_collumns < max_timing_conditions:
                    print('        <td></td>')
                    virtual_device_collumns += 1
                print('    </tr>')

        p_fec, p_dev, p_num, p_alias, p_cond = mppm_devices[i]
        i += 1

    print('</table>')
    print('</div>')


def get_mppm_conditions(mppm_devices_dir: Path) -> Dict:
    mppm_data = {}

    for mppm_file in mppm_devices_dir.iterdir():
        filename = mppm_file.stem
        mppm_data[filename] = {}
        with open(mppm_file, 'r') as f:
            for line in f.readlines():
                ch, cond = line.split(':')
                cond = cond.strip('"').strip()
                mppm_data[filename][ch] = cond
    return mppm_data


def parse_and_store(subdevices_dir: Path, mppm_devices_dir: Path):
    global max_timing_conditions

    mppm_conditions = get_mppm_conditions(mppm_devices_dir)
    max_timing_conditions = max([len(mppm_conditions[key]) for key in mppm_conditions.keys()])

    # iterate through devices in the folder
    for file in subdevices_dir.iterdir():
        filename = file.stem
        devices = {}

        # look only on FECs with mppm conditions
        if filename in mppm_conditions:
            with open(file, mode='r') as f:
                for line in f.readlines():
                    device, num, alias = line.split(':')
                    alias = alias.strip()

                    if device in devices:
                        # MPPM device, main device already seen
                        timing_cond = mppm_conditions[filename][num]

                        if num == "1":
                            # add the default (num == 0) one too
                            default_alias = devices[device]
                            mppm_devices.append((filename, device, "0", default_alias, 'Default virtual device'))

                        mppm_devices.append((filename, device, num, alias, timing_cond))
                    else:
                        devices[device] = alias

    # sort the data by gateway>device>number
    mppm_devices.sort(key=lambda tupl: (tupl[0], tupl[1], tupl[2]))


def main():
    subdevs_dir = Path('/user/pclhc/etc/fgcd/sub_devices')
    mppm_dir = Path('/user/pclhc/etc/fgcd/multippm_conditions')

    parse_and_store(subdevs_dir, mppm_dir)
    print_table()


if __name__ == "__main__":
    main()


# EOF

