# Filename: timing_by_fgc.py
#
# Purpose:  Parse files and generate an html table with the timing conditions for each fgc
#
# Location: Run $make install to deploy the script to ~pclhc/lib/python/packages/wiki_pages


import os
import re
import subprocess
import time
from collections import defaultdict

from fgc_device import Fgc
import data_parser


# Dictionary of Fgc objects
fgc_devices = defaultdict(Fgc)


def build_table():
    global fgc_devices
    fgc_devices = data_parser.parse_fgc_devices()
    fgc_devices, _ = data_parser.parse_fgc_fieldbus(fgc_devices)

    fec_events = data_parser.get_fec_events()
    fgc_devices = data_parser.parse_fgc_timing(fgc_devices, fec_events)

    fgc_devices = data_parser.parse_fgc_aliases(fgc_devices)
    fgc_devices = data_parser.skip_fgc_devices(fgc_devices)


def print_table():
    """
        write to standard output the html containing the parsed data
        """

    print('<p>Table generated', time.ctime(), '</p>')
    print('<div style="overflow-x:auto;">')
    print('<TABLE class="confluenceTable" id="timeTable">')

    # Print table header
    print('<THEAD>')
    print('<TR>')
    print('<TH class="confluenceTh">Group</TH>')
    print('<TH class="confluenceTh">Timing Domain</TH>')
    print('<TH class="confluenceTh">Timing Source</TH>')
    print('<TH class="confluenceTh">Gateway</TH>')
    print('<TH class="confluenceTh">Device</TH>')
    print('<TH class="confluenceTh">Alias</TH>')
    print('<TH class="confluenceTh">FieldBus Event Type</TH>')
    print('<TH class="confluenceTh">Timing Event</TH>')
    print('<TH class="confluenceTh">Delay</TH>')
    print('</TR>\n')
    print('</THEAD>')

    print('<TBODY>')

    for device in fgc_devices.keys():
        if fgc_devices[device].timing != []:
            for timing_config in fgc_devices[device].timing:
                print('<TR>')
                print(f'<TD class="confluenceTd"> {fgc_devices[device].area} </TD>')
                print(f'<TD class="confluenceTd"> {fgc_devices[device].timing_domain} </TD>')
                print(f'<TD class="confluenceTd"><a href="{data_parser.PCLHC_URL}{fgc_devices[device].timing_source}" target="_blank" rel="noopener"> {fgc_devices[device].timing_source} </a></TD>')
                print(f'<TD class="confluenceTd"> {fgc_devices[device].fec} </TD>')
                print(f'<TD class="confluenceTd"> {fgc_devices[device].device_name} </TD>')
                print(f'<TD class="confluenceTd"> {fgc_devices[device].alias} </TD>')
                print(f'<TD class="confluenceTd"> {timing_config.fieldbus} </TD>')
                if timing_config.event != '':
                    print(f'<TD class="confluenceTd"><a href="{data_parser.get_ccdb_timing_url(timing_config.event)}" target="_blank" rel="noopener"> {timing_config.event} </a></TD>')
                else:
                    print(f'<TD class="confluenceTd"></TD>')
                print(f'<TD class="confluenceTd"> {timing_config.delay} </TD>')
                print('</TR>\n')

    print('</TBODY>')

    print('</TABLE>')
    print('</div>')


def main():
    build_table()
    print_table()

    # TESTING:
    #
    # print(fgc_devices["RFNA.866.03.ETH2"])
    #
    # for target in fecs_in_target.keys():
    #    print(target, fecs_in_target[target])


if __name__ == "__main__":
    main()

# EOF
