import argparse
import json
from pathlib import Path
import sys

from jinja2 import Environment, FileSystemLoader, select_autoescape
from markupsafe import Markup


parser = argparse.ArgumentParser()
parser.add_argument("--output-dir", type=Path)
args = parser.parse_args()


stats = json.load(sys.stdin)

template_dir = Path(__file__).parent / "templates"

env = Environment(
    loader=FileSystemLoader(Path(__file__).parent / "templates"),
    autoescape=select_autoescape()
)

def pad_right(s: str, width: int, symbol: str = ".", highlight_trailing_whitespace=True):
    str_len = len(s)

    if highlight_trailing_whitespace:
        trimmed = s.rstrip()

        if len(trimmed) < str_len:
            s = s[:len(trimmed)] + Markup('<span style="background-color: salmon">' + s[len(trimmed):] + '</span>')

    if str_len >= width:
        return s
    else:
        return s + Markup('<span style="opacity: 0.33">' + symbol * (width - str_len) + '</span>')

env.filters["pad_right"] = pad_right

# Iterate all templates in the directory
for template_path in (template_dir / "dim_statistics").iterdir():
    template = env.get_template(str(template_path.relative_to(template_dir)))

    (args.output_dir / template_path.name).write_text(template.render(**stats))
