# Filename: device_count.py
#
# Purpose:  Extract from CCDB and the file name the number of devices for each FESA and FGC class.
#           Then build a html table with the data collected from each class.
#
# Location: Placed in ~pclhc/lib/python/packages/wiki_pages with $make install.
#
# Usage:    Script called by wiki_pages/gitlab-ci.jobs.yml

import time
import pyccda
import pyfgc_name
from operator import add

name_column_namefile = 'Name  file'
name_column_total    = 'Total'
name_row_total       = 'Total'

result_count_correct    = 0
result_count_wrong      = 1
result_count_obsolete   = 2
result_devices_obsolete = 3
result_devices_wrong    = 4


def print_common():
    """ Print the styles and functions """

    print('<style type="text/css">')
    print('    .popup {')
    print('        position: fixed;/*VERY IMPORTANT*/')
    print('        padding: 20px;')
    print('        color: white;')
    print('        box-sizing: border-box;')
    print('        white-space: nowrap;')
    print('        transform: translate(-50%, 40px);')
    print('        background-color: rgba(0, 0, 0);')
    print('        visibility: hidden;')
    print('    }')
    print('    .device_popup:hover ~ .popup {')
    print('        visibility: visible;')
    print('    }')
    print('    .device_popup:hover {')
    print('        font-weight: bold;')
    print('    }')
    print('</style>')



def print_html_table_cell_count(counts):
    """ Print a cell with the counts of good, wrong and obsolete devices """

    if counts is not None:
        print('      <td class="confluenceTd" style="text-align: center;">')
        if (counts[ result_count_correct] > 0):
            print('        <font color="green">'   + str(counts[result_count_correct]) + '<br></font>')
        if (counts[ result_count_wrong] > 0):
            print('        <span class="device_popup" style="color: red;"' + 'title="' \
                  + '&#10'.join(counts[result_devices_wrong]) + '"' + '>'              \
                  + str(counts[result_count_wrong]) + '<br></span>')
        if (counts[result_count_obsolete] > 0):
            print('        <span class="device_popup" style="color: orange;"' + 'title="' \
                  + '&#10'.join(counts[result_devices_obsolete]) + '"' + '>'              \
                  + str(counts[result_count_obsolete]) + '<br></span>')

        print('      </td>')
    else:
        print('      <td class="confluenceTd" style="text-align: center;"></td>')



def print_html_table_cell_total(counts, mismatch_devices = []):
    """ Print a cell with the total number of devices based on the FGC name file"""

    if mismatch_devices:
        print('      <td class="confluenceTd" style="text-align: center;vertical-align:middle;">')
        print('        <span class="device_popup" style="color: red;"' + 'title="' \
              + '&#10'.join(mismatch_devices) + '"' + '>' + str(sum(counts[0:2])) + '<br></span>')
    else:
        print('      <td class="confluenceTd" style="text-align: center;vertical-align:middle;"><b>' + str(sum(counts[0:2])) + '</b></font></td>')



def print_html_table(accelerator_list, device_count_list, platform_name, mismatch_devices = None):
    """ Print the table """

    # Data format device_count_list
    # {class_name: {accelerator: [count_operational, count_development, count_obsolete, [device_obsolete], [device_fault]] } }

    # Data format mismatch_device_list. Only for FGC classes
    # {class_name: (devices_not_in_ccdb, devices_not_in_name_file)

    header_items = ['Class ID']
    header_items = header_items + accelerator_list
    platform_fgc = platform_name == "FGC"

    """ Print the table """

    print("<br><br>")
    print("<header><h2>" + platform_name + " devices</h2></header>")
    print('<table class="confluenceTable wrapped fixed-table">')
    print('  <colgroup> <col style="width: 105.0px;"/>' + ('<col style="width: 79.0px;"/>' * (len(header_items) - 1)) + '  </colgroup>')

    print('  <tbody>')
    print('    <tr>')
    print('      <th class="confluenceTh" style="text-align:center;vertical-align:middle;">' + '</th>\n      <th class="confluenceTh" style="text-align: center;vertical-align:middle;">'.join(header_items) + '</th>')
    print('    </tr>')

    for class_name, device_count in device_count_list.items():
        print('    <tr>')

        if platform_fgc == True:
            link = 'https://accwww.cern.ch/proj-fgc/gendoc/def/Class' + str(class_name) + '.htm'
            print('      <td class="confluenceTd" style="text-align: center; vertical-align:middle;"><a href="' + link + '">' + str(class_name) + '</a></td>')
        else:
            link = 'https://ccde.cern.ch/devices/search?query=%2522Class%2520Name%2522%2520%253D%2520' + class_name + '%2520and%2520State%2520%253D%2520operational&mode=d'
            print('      <td class="confluenceTd" style="text-align: center; vertical-align:middle;"><a href="' + link + '">' + class_name + '</a></td>')

        if class_name == name_row_total:
            for accelerator in accelerator_list:
                print_html_table_cell_total(device_count.get(accelerator))
        else:
             for accelerator in accelerator_list:
                if accelerator == name_column_total:
                    mismatches = mismatch_devices[class_name][0] if mismatch_devices else []
                    print_html_table_cell_total(device_count.get(accelerator), mismatches)

                    # if platform_fgc

                elif accelerator == name_column_namefile:
                    mismatches = mismatch_devices[class_name][1]
                    print_html_table_cell_total(device_count.get(accelerator), mismatches)
                else:
                    print_html_table_cell_count(device_count.get(accelerator))

        print('    </tr>')

    print('  </tbody>')
    print('</table>')



def get_fgc_class_names(ccda):
    """ Get the list of all device classes """

    class_list = []

    for device_class in ccda.DeviceClass.search('implementation=="FGC"'):
        class_list.append(device_class.device_class_info.name)

    return sorted(class_list)



def get_device_class_count_ccda(ccda, device_class, fesa_class):
    """ Returns the number of devices for a given class and accelerator """

    # Data format: {accelerator: [count_operational, count_development, count_obsolete, [device_obsolete], [device_fault]]; }

    result = dict()
    accelerator_list = list()
    device_name_list = list()
    device_list    = ccda.Device.search('deviceClassInfo.name==' + device_class)

    total_correct  = 0
    total_wrong    = 0
    total_obsolete = 0

    for device in device_list:
        # Ignore FESA devices that start with GD_ since they do not control power converters
        if fesa_class == True and device.name.startswith("GD_") == True:
            continue

        device_name_list.append(device.name)

        accelerator_name = device.accelerator_name if device.accelerator_name is not None else 'NONE'

        if accelerator_name not in result.keys():
            result[accelerator_name] = [0, 0, 0, list(), list()]
            accelerator_list.append(accelerator_name)

        value = result[accelerator_name]

        if device.state == 'operational':
            if accelerator_name == 'NONE':
                value[result_devices_wrong].append(device.name)
                value[result_count_wrong] += 1
                total_wrong += 1
            else:
                value[result_count_correct] += 1
                total_correct += 1

        elif device.state == 'development':
            if accelerator_name != 'NONE':
                value[result_devices_wrong].append(device.name)
                value[result_count_wrong] += 1
                total_wrong += 1
            else:
                value[result_count_correct] += 1
                total_correct += 1

        else:
            # Include obsolete and expert
            total_obsolete += 1
            value[result_count_obsolete] += 1
            value[result_devices_obsolete].append(device.name)

    # Add the Total number of devices
    result[name_column_total] = [total_correct, total_wrong, total_obsolete, [], []]

    return accelerator_list, device_name_list, result



def process_fgc_devices(ccda):
    """ Retrieves all the FGC class device count """

    # Data format
    # {class_name: {accelerator: [count_operational, count_development, count_obsolete, [device_obsolete], [device_fault]] } }

    # Get the FGC class names
    fgc_class_list = get_fgc_class_names(ccda)

    # Convert to class IDs and discard FGC base classes
    fgc_class_id_list = sorted([int(fgc_class[len('FGC_'):]) for fgc_class in fgc_class_list])
    fgc_class_id_list = [fgc_class_id for fgc_class_id in fgc_class_id_list if not ((fgc_class_id % 10) == 0)]

    device_count     = dict()
    mismatch_devices = dict()
    accelerator_list = []

    pyfgc_name.read_name_file()

    for fgc_class_id in fgc_class_id_list:
        fgc_class_accelerator_list, ccda_name_list, result = get_device_class_count_ccda(ccda, "FGC_" + str(fgc_class_id), False)

        device_count[fgc_class_id] = result

        # Get device names from name file
        name_file_list = pyfgc_name.get_fgcs_from_class(fgc_class_id)

        result[name_column_namefile] = [len(name_file_list), 0, 0, [], []]

        # Check for mismatches between CCDB and the file name
        mismatch_devices[fgc_class_id] = (list(set(name_file_list) - set(ccda_name_list)), \
                                          list(set(ccda_name_list) - set(name_file_list)))

        accelerator_list += fgc_class_accelerator_list


    # Get sorted and unique accelerator names and add the column
    # with the total count and the count based on the name file
    accelerator_list = (sorted(set(accelerator_list)))
    accelerator_list.append(name_column_total)
    accelerator_list.append(name_column_namefile)

    # Count devices per accelerator
    accelerator_total_count = dict()
    for class_count in device_count.values():
        for accelerator_name, counts in class_count.items():
            if accelerator_name not in accelerator_total_count.keys():
                accelerator_total_count[accelerator_name] = [0, 0, 0]
            accelerator_total_count[accelerator_name] = list(map(add, accelerator_total_count[accelerator_name], counts[0:3]))
            accelerator_total_count[accelerator_name].append([])
            accelerator_total_count[accelerator_name].append([])

    fgc_class_id_list.append(name_row_total)
    device_count[name_row_total] = accelerator_total_count

    # Print html table
    print_html_table(accelerator_list, device_count, "FGC", mismatch_devices)



def process_fesa_devices(ccda):
    """ Retrieves all the FESA class device count """

    fesa_class_list = ['PowM1553', 'PowJC', 'PowPLC', 'PowRs422', 'PowMainPS']

    accelerator_list = []
    device_count     = dict();

    for fesa_class in fesa_class_list:
        fesa_class_accelerator_list, ccda_name_list, result = get_device_class_count_ccda(ccda, fesa_class, True)
        device_count[fesa_class] = result
        accelerator_list        += fesa_class_accelerator_list

    # Get sorted and unique accelerator names and add the column with the total count
    accelerator_list = (sorted(set(accelerator_list)))
    accelerator_list.append(name_column_total)

    # Count devices per accelerator
    accelerator_total_count = dict()
    for class_count in device_count.values():
        for accelerator_name, counts in class_count.items():
            if accelerator_name not in accelerator_total_count.keys():
                accelerator_total_count[accelerator_name] = [0, 0, 0]

            accelerator_total_count[accelerator_name] = list(map(add, accelerator_total_count[accelerator_name], counts[0:3]))

    fesa_class_list.append(name_row_total)
    device_count[name_row_total] = accelerator_total_count

    # Print html table
    print_html_table(accelerator_list, device_count, "FESA")




def main():
    ccda = pyccda.SyncAPI()

    print_common()

    process_fesa_devices(ccda)
    process_fgc_devices(ccda)

    print('<p><sup>Last update: ', time.ctime(), ' </sup></p>')


if __name__ == "__main__":
    main()

# EOF
