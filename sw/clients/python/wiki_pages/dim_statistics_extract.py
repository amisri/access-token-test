from datetime import datetime
import json
import os
from pathlib import Path
import subprocess
from typing import Any, Dict, Sequence
import sys


env = os.environ
env["xml_files"] = " ".join(str(p) for p in (Path(__file__).parent / ".." / ".." / ".." / ".." / "def" / "src" / "dim_types").glob("*.xml"))


def shell_lines(cmd: str) -> Sequence[str]:
    output = subprocess.check_output(cmd, shell=True, env=env, stdin=subprocess.DEVNULL).decode()
    lines = output.split("\n")

    # strip whitespace and remove empty lines
    return [line for line in lines if len(line.strip())]


def shell_out(cmd: str) -> str:
    return subprocess.check_output(cmd, shell=True, env=env, stdin=subprocess.DEVNULL).decode().strip()


stats: Dict[str, Any] = {}
stats["job_url"] = os.getenv("CI_JOB_URL")
stats["timestamp_str"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

stats["num_analog_signals"] =   shell_out(r"""grep '<ana_chan' $xml_files | wc -l""")
stats["num_digital_signals"] =  shell_out(r"""grep '<input' $xml_files | wc -l""")
stats["num_fault_signals"] =    shell_out(r"""grep 'fault="1"' $xml_files | wc -l""")
stats["num_status_signals"] =   shell_out(r"""grep 'fault="0"' $xml_files | wc -l""")
stats["num_fault_empty_1"] =    shell_out(r"""grep 'fault="1".*one=""' $xml_files |  wc -l""")
stats["num_fault_nonempty_0"] = shell_out(r"""grep 'fault="1"' $xml_files | grep -v 'zero=""' |  wc -l""")
stats["num_status_empty_0"] =   shell_out(r"""grep 'fault="0".*zero=""' $xml_files |  wc -l""")
stats["num_status_empty_1"] =   shell_out(r"""grep 'fault="0".*one=""' $xml_files |  wc -l""")

stats["analog_labels"] =        shell_lines(r"""gawk '/^ *<ana_chan/{print}' $xml_files | sed 's| *<ana_chan.*\label="\(.*\)".*".*".*".*".*".*".*$|\1|g' | sort | uniq""")
stats["digital_labels"] =       shell_lines(r"""gawk '/^ *<input./{print}' $xml_files | sed 's| *<input.*\label="\(.*\)".*".*".*".*".*$|\1|g' | sort | uniq""")
stats["status_0_labels"] =      shell_lines(r"""gawk '/^ *<input.*fault="0"/{print}' $xml_files | sed 's| *<input.*\zero="\(.*\)".*".*".*$|\1|g' | sort | uniq""")
stats["status_1_labels"] =      shell_lines(r"""gawk '/^ *<input.*fault="0"/{print}' $xml_files | sed 's| *<input.*\one="\(.*\)".*|\1|g' | sort | uniq""")
stats["fault_1_labels"] =       shell_lines(r"""gawk '/^ *<input.*fault="1"/{print}' $xml_files | sed 's| *<input.*\one="\(.*\)".*|\1|g' | sort | uniq""")

json.dump(stats, sys.stdout)
