# fgcd_udp_subscriptions.awk
#
# This script will read all the host files in /user/pclhc/etc/fgcd/udp_subscriptions/host and create
# the confluence table html file in /user/pclhc/public_html/wiki_pages/fgcd_udp_subscriptions.html
#
# The associations between front-end computers and CERN groups is currently hard-coded in the BEGIN
# block below. This could become a dedicated file if needed.

BEGIN {
    # Responsible CERN group for each host name - must be maintained by hand

    group["cfp-864-lab02"]    = "BE-ICS"

    group["cfp-864-lab02"]    = "EN-CRG"
    group["cfp-sa6-qlss56"]   = "EN-CRG"
    group["cfp-sa6-qlss67"]   = "EN-CRG"
    group["cfp-sh8-qlss78"]   = "EN-CRG"
    group["cfp-sh8-qlss81"]   = "EN-CRG"
    group["cfp-shb2-qlss23"]  = "EN-CRG"
    group["cfp-shb4-qlss34"]  = "EN-CRG"
    group["cfp-shb4-qlss45"]  = "EN-CRG"
    group["cfp-shc18-qlss12"] = "EN-CRG"

    group["cs-ccr-ofc1"]      = "SY-BI"
    group["cs-ccr-ofc2"]      = "SY-BI"

    group["cwe-513-win157"]  = "TE-MSC"
    group["pctelinac01"]     = "TE-MSC"
    group["pctemm12"]        = "TE-MSC"
    group["pctemm13"]        = "TE-MSC"
    group["pctemm30"]        = "TE-MSC"
    group["pctemm37"]        = "TE-MSC"

    group["plccois33"]       = "BE-ICE"

    # Set the field separator to ':' to parse the udp_subscription host files

    FS = ":"
}

# Read and process each udp_subscription host file

//{
    # Split each upd subscription into host_name, udp_port, period, option_id - we only use host_name from s[1]

    split($1, s, ".")

    # If this is the first instance of the host name then remember it in an array with numeric index
    # and count the number of times each host name is found

    if(++count[s[1]] == 1)
    {
        host_name[++num_hosts] = s[1]
    }
}

# Post processing

END {
    # Write the Confluence table header

    print "<table class=\"confluenceTable wrapped fixed-table\">"
    print "  <colgroup> <col style=\"width: 156.0px;\"/> <col style=\"width: 128.0px;\"/> <col style=\"width: 294.0px;\"/> <col style=\"width: 128.0px;\"/> </colgroup>"
    print "  <tbody>"
    print "    <tr>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Host</th>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Number of<br/>gateways</th>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">gateways:port:period:option_id</th>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Responsible</th>"
    print "    </tr>"

    # Sort the array of host names

    asort(host_name)

    # Generate the table contents

    for(i=1 ; i <= num_hosts ; i++)
    {
        printf "    <tr>\n"
        printf "      <td class=\"confluenceTd\">\n"
        printf "        <a href=\"https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SearchForDisplay&amp;DeviceName=%s\">%s</a>\n", host_name[i], host_name[i]
        printf "      </td>\n"
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%u</td>\n",count[host_name[i]]
        printf "      <td class=\"confluenceTd\">\n"

        while("grep -n " host_name[i] " *" | getline > 0)
        {
             printf "        <code>%s:%s:%s:%s</code>\n        <br/>\n",$1,$4,$5,$6
        }

        printf "      </td>\n"
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%s</td>\n", group[host_name[i]]
        printf "    </tr>\n"
    }

    # Write the table footer

    printf "  </tbody>\n"
    printf "</table>\n"

    # Write data stamp in the footer

    printf "<p><sup>Last update: %s</sup></p>\n", strftime("%Y-%m-%d %H:%M")
}

# EOF
