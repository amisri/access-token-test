import data_parser
import time

PCLHC_URL = 'https://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/timing/timdt/'

table_lines = list()


def build_table():
    fec_events = data_parser.get_fec_events()
    fec_events = data_parser.merge_fec_events(fec_events)

    # Add lines to the table of tuples
    for fec in fec_events.keys():
        area, timing_domain = data_parser.targets[fec]
        for fieldbus, timing, delay, timing_source in fec_events[fec]:
            table_lines.append((area, timing_domain, timing_source, fec, fieldbus, timing, delay))

    table_lines.sort(key=lambda tupl: (tupl[0], tupl[1], tupl[3]))


def print_table():
    """
    write to standard output the html containing the parsed data
    """

    print('<p>Table generated', time.ctime(), '</p>')
    print('<div style="overflow-x:auto;">')
    print('<TABLE class="confluenceTable" id="aTable">')

    # Print table header
    print('<THEAD>')
    print('<TR>')
    print('<TH class="confluenceTh">Group</TH>')
    print('<TH class="confluenceTh">Timing Area</TH>')
    print('<TH class="confluenceTh">Timing Source</TH>')
    print('<TH class="confluenceTh">Gateway</TH>')
    print('<TH class="confluenceTh">FieldBus Event Type</TH>')
    print('<TH class="confluenceTh">Timing Event</TH>')
    print('<TH class="confluenceTh">Delay</TH>')
    print('</TR>\n')
    print('</THEAD>')

    print('<TBODY>')

    prev_area = ''
    for area, timing_domain, source, fec, fieldbus, timing, delay in table_lines:
        separator = ''
        if area != prev_area:
            separator = 'separator'
            prev_area = area

        print('<TR>')
        print(f'<TD class="confluenceTd {separator}"> {area} </TD>')
        print(f'<TD class="confluenceTd {separator}"> {timing_domain} </TD>')
        print(f'<TD class="confluenceTd {separator}"><a href="{PCLHC_URL}{source}" target="_blank" rel="noopener"> {source} </a></TD>')
        print(f'<TD class="confluenceTd {separator}"> {fec} </TD>')
        print(f'<TD class="confluenceTd"> {fieldbus} </TD>')
        print(f'<TD class="confluenceTd"><a href="{data_parser.get_ccdb_timing_url(timing)}" target="_blank" rel="noopener">{timing}</a></TD>')
        print(f'<TD class="confluenceTd"> {delay} </TD>')
        print('</TR>\n')
    print('</TBODY>')

    print('</TABLE>')
    print('</div>')


def main():
    build_table()
    print_table()


if __name__ == "__main__":
    main()

# EOF