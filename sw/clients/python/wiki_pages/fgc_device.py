from dataclasses import dataclass, field
from typing import List

@dataclass
class TimingConfig:
    fieldbus: str = ''
    event: str = ''
    delay: str = ''


@dataclass
class Fgc:
    device_name: str
    alias: str = ''
    class_num: str = ''
    fec: str = ''
    area: str = ''
    timing_domain: str = ''
    timing_source: str = ''
    timing: List[TimingConfig] = field(default_factory=list)

    def to_list(self):
        return self.device_name, self.alias, self.fec, self.area, self.timing_domain, self.timing