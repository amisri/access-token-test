# fgc_fecs_info.awk
#
# This script will read the name file /user/pclhc/etc/fgcd/name to get every fgc fec name.
# It reads the file /user/pclhc/bin/python/wiki_pages/.fgc_fecs_info.csv, which contains the
# data from the last execution of this program. It tries to update the data for the fecs
# by reading the message of the day files and by using ping to see if each fec is online.

BEGIN {

    if(ARGC != 2)
    {
        print "usage: awk -f fgc_fecs_info.awk html_path"
        exit -1
    }

    html_path = ARGV[1]
    html_nl   = "&#10;"

    # Get the date

    date = strftime("%Y-%m-%d %H:%M")

    # Read the FGC name and group files

    readFgcGroupFile("/user/pclhc/etc/fgcd/group")
    readFgcNameFile("/user/pclhc/etc/fgcd/name")
    readFgcTimingHostFiles("/user/pclhc/etc/fgcd/timing/timdt")
    readFgcTimingClassFiles("/user/pclhc/etc/fgcd/timing/timdt/class")
    readFgcSurveyFile("/user/pclhc/etc/fgcd_survey/image")

    # Read the saved data file. This is a dot file to avoid being cleaned when
    # doing a make clean_src or make install.

    readSaveFile(".fgc_fecs_info.csv")

    # Sort fec names alphabetically

    asort(fec_name)

    # Look for the message of the day (motd) files for all the fecs

    getMotdPaths()

    # Look for the eth0_info files for all fecs

    getEth0InfoPaths()

    # Look for the stderr and stdout files for all fecs

    getStdPaths("stderr")
    getStdPaths("stdout")

    # Read data from CCDA

    getCcdaData()

    # Use fping to see which systems are responding

    pingFecs()

    # For all FECs, try to read the motd files, eth0 info files and stderr files

    printf "Reading motd, eth0, stdout and stderr files for %u FECs\n", num_fecs > "/dev/stderr"

    reset_file = ".fecs_to_reset"

    for(i=1 ; i <= num_fecs ; i++)
    {
        n = fec_name[i]

        # Read the motd file to get the cpu_type, num_cores and mem_size

        if(n in motd)
        {
            readMotd(n, motd[n])
        }

        # Read the eth0_info file to get the link speed

        if(n in eth0_info_file)
        {
            readEthInfo(n, eth0_info_file[n])
        }

        # Set fgcd status to dead/warning/running based on the stderr file for the FEC

        readStderr(n)

        # Get free memory, number of commands and CMD_MAX_VAL_LENGTH from the stdout file for the FEC

        readStdout(n)

        # Set timing colour and config for the tooltip

        setTiming(n)

        # Write fec name to reset file if doesn't respond to ping or has a link speed of less than 1000 Mbps

        if(fgcd[n] == "dead" || fgcd[n] == "offline" || link_speed[n] < 1000)
        {
            print n > reset_file
        }

        # Rewrite the saved data file and count the number of fecs per cpu_type and mem_size

        printf "%s,%s,%s,%s,%s,%s\n", n, cpu_type[n], num_cores[n], mem_size[n], link_speed[n], last_seen[n] > savefile

        count_cpu_type[cpu_type[n]]++
        count_mem_size[mem_size[n]]++
    }

    close(savefile)
    close(reset_file)

    # Sort the cpu_types

    for(n in count_cpu_type)
    {
        sorted_cpu_type[++num_cpu_types] = n
    }

    asort(sorted_cpu_type)

    # Sort the mem_sizes

    for(n in count_mem_size)
    {
        sorted_mem_size[++num_mem_sizes] = n
    }

    asort(sorted_mem_size)

    # Write the confluence tables

    writeHtmlTables("fgc_fecs_info.new")

    # Rename file to immediately become the new version of the file

    system("mv " html_path "/fgc_fecs_info.new " html_path "/fgc_fecs_info.html" )
}



function readFgcGroupFile(groupfile)
{
    print "Reading FGC group file" > "/dev/stderr"

    FS = ":"
    RS = "\n"

    while((getline < groupfile) > 0)
    {
        if(fec_namefile[$1] > 0)
        {
            fgcs[$1] = "WARNING:FEC name repeated in group file"
        }
        fec_namefile[$1]++;
        timing[$1] = $2
        group[$1] = $3
        comment[$1] = $4
    }

    close(groupfile)
}



function readFgcNameFile(namefile)
{
    print "Reading FGC name file" > "/dev/stderr"

    FS = ":"
    RS = "\n"

    while((getline < namefile) > 0)
    {
        if($2 == 0)
        {
            if(!($1 in fec_namefile))
            {
                fec_namefile[$1]++;
                timing[$1] = "unknown"
                group[$1] = "unknown"
            }
            class[$1] = $3
        }
        else
        {
            # Accumulate FGC names for each gateway to be a tooltip in the table

            if(fgcs[$1] != "")
            {
                fgcs[$1] = fgcs[$1] html_nl
            }

            fgcs[$1] = fgcs[$1] $2 ". " $4

            num_fgcs[$1]++
        }
    }

    close(namefile)
}



function readFgcTimingHostFiles(timingpath,  i, n, filename, num_timing_host_files, lines, report)
{
    print "Reading timing host files" > "/dev/stderr"

    RS = "#"

    "ls -1 " timingpath "/host" | getline timing_host_files

    num_timing_host_files = split(timing_host_files, lines, "\n")

    for(i = 1 ; i < num_timing_host_files ; i++)
    {
        n = lines[i]

        # Read host timing file for host n

        filename = timingpath "/host/" n

        getline < filename

        close(filename)

        report = $0

        # Add the difference between the host file and the class file for this host to the report

        "diff " filename " " timingpath "/class/" class[n] "/" timing[n] | getline

        report = report "\nDifference with class " class[n] " timing " timing[n] ":\n" $0

        gsub(/\n/,"\\&#10;",report)

        host_timing[n] = report
    }
}



function readFgcTimingClassFiles(timingpath,  i, j, classpath, filename, num_class_dirs, num_class_files, lines, lines2)
{
    print "Reading timing class files" > "/dev/stderr"

    RS = "#"

    "ls -1 " timingpath | getline timing_class_dirs

    num_class_dirs = split(timing_class_dirs, lines, "\n")

    for(i = 1 ; i < num_class_dirs ; i++)
    {
        classpath = timingpath "/" lines[i]

        "ls -1 " classpath | getline timing_class_files

        num_class_files = split(timing_class_files, lines2, "\n")

        for(j = 1 ; j < num_class_files ; j++)
        {
            filename = classpath "/" lines2[j]

            getline < filename

            close(filename)

            gsub(/\n/,"\\&#10;")

            class_timing[lines[i],lines2[j]] = $0
        }
    }
}



function readFgcSurveyFile(surveyfile)
{
    print "Reading FGC survey file" > "/dev/stderr"

    FS = ":"
    RS = "\n"

    while((getline < surveyfile) > 0)
    {
        fgcd[$1] = $2
    }

    close(surveyfile)
}



function getCcdaData(  curl, ccda_data, i, num_lines, lines, num_field, fields, k, name, key_value, state_abbr)
{
    print "Reading CCDA data" > "/dev/stderr"

    state_abbr["operational"] = "Op"
    state_abbr["development"] = "Dev"

    curl = "curl -s -X POST \"https://ccda.cern.ch:8900/api/computers/names\" -H \"accept: */*\" -H  \"Content-Type: application/json\" -d \"[ \\\"" fec_name[1]  "\\\""

    for(i=2 ; i <= num_fecs ; i++)
    {
        curl = curl ", \\\"" fec_name[i] "\\\""
    }

    curl = curl " ]\""

    curl | getline ccda_data

    gsub(/\"/,"",ccda_data)
    gsub(/},{id:/,"\n",ccda_data)
    gsub(/[{}\[\]]+/,"",ccda_data)

    num_lines = split(ccda_data, lines, "\n")

    for(i = 1 ; i <= num_lines ; i++)
    {
        num_fields = split(lines[i], fields, ",")
        name = ""

        for(k = 1 ; k < num_fields ; k++)
        {
            if(split(fields[k], key_value, ":") == 2)
            {
                if(name == "" && key_value[1] == "name")
                {
                    name = key_value[2]
                }
                else
                {
                    if(key_value[1] == "domain")
                    {
                        network[name] = key_value[2]
                    }
                    else if(key_value[1] == "directory")
                    {
                        directory[name] = key_value[2]
                    }
                    else if(key_value[1] == "state")
                    {
                        state[name] = state_abbr[key_value[2]]
                    }
                }
            }
        }
    }
}



function readSaveFile(filename)
{
    # savefile format is fec_name,cpu_type,num_cores,mem_size,link_speed,last_update,num_cmds,avl_mem,cmd_max_val_length

    savefile = filename
    num_fecs = 0

    printf "Reading %s", savefile > "/dev/stderr"

    FS = ","
    RS = "\n"

    while((getline < savefile) > 0)
    {
        # Ignore fec names that are no longer in the name file

        if($1 in fec_namefile)
        {
            fec_name[++num_fecs] = $1

            cpu_type[$1] = $2
            num_cores[$1] = $3
            mem_size[$1] = $4
            link_speed[$1] = $5
            last_seen[$1] = $6
        }
    }

    close(savefile)

    printf ", %u FECs recovered\n", num_fecs > "/dev/stderr"

    # Initialise records for new fec_names

    for(n in fec_namefile)
    {
        if((n in last_seen) == 0)
        {
            fec_name[++num_fecs] = n

            cpu_type[n] = "unknown"
            num_cores[n] = 0
            mem_size[n] = 0
            link_speed[n] = 0
            last_seen[n] = "0000-00-00 00:00"
        }
    }
}



function getMotdPaths(  i, motds, num_motds, lines, num_fields)
{
    print "Automounting directories in /acc/dsc/..." > "/dev/stderr"

    RS = "#"

    "cd /acc/dsc; stat -t $(ls -1 /acc/dsc/src) 2> /dev/null" | getline

    print "Listing motd files" > "/dev/stderr"

    "ls -1 /acc/dsc/*/*-r*/tmp/motd /acc/dsc/*/*-a*/tmp/motd" | getline motds

    num_motds = split(motds, lines, "\n")

    for(i = 1 ; i < num_motds ; i++)
    {
        num_fields = split(lines[i], fields, "/")

        motd[fields[5]] = lines[i]
        nfs[fields[5]] = fields[4]
    }
}



function getEth0InfoPaths(  i, eth0_info_files, num_eth0_info_files, lines, fields)
{
    print "Listing eth0_info files" > "/dev/stderr"

    RS = "#"

    "ls -1 /user/pclhc/var/log/fgcd/*-*-r*/eth0_info /user/pclhc/var/log/fgcd/*-*-a*/eth0_info" | getline eth0_info_files

    num_eth0_info_files = split(eth0_info_files, lines, "\n")

    for(i = 1 ; i < num_eth0_info_files ; i++)
    {
        num_fields = split(lines[i], fields, "/")

        eth0_info_file[fields[7]] = lines[i]
    }
}



function getStdPaths(stream,   i, contents, num_lines, lines, fields)
{
    printf "Listing %s files\n", stream > "/dev/stderr"

    RS = "#"

    "ls -1 /user/pclhc/var/log/fgcd/*-*-r*/" stream " /user/pclhc/var/log/fgcd/*-*-a*/" stream | getline contents

    num_lines = split(contents, lines, "\n")

    for(i = 1 ; i < num_lines ; i++)
    {
        num_fields = split(lines[i], fields, "/")
        path[stream,fields[7]] = lines[i]
    }
}



function pingFecs(  i, fping, ping_info, num_fields, alive_fecs)
{
    printf "Pinging %u FECs\n", num_fecs > "/dev/stderr"

    fping = "/usr/sbin/fping -a"

    for(i=1 ; i <= num_fecs ; i++)
    {
        fping = fping " " fec_name[i]
    }

    fping " 2> /dev/null" | getline ping_info

    num_fields = split(ping_info, alive_fecs, "\n")

    # update last_seen date for FECs that responded to fping

    for(i = 1 ; i < num_fields ; i++)
    {
        last_seen[alive_fecs[i]] = date
    }
}



function readMotd(n, filename)
{
    FS = " "
    RS = "#"

    getline < filename

    close(filename)

    if(NF > 1)
    {
        gsub(/\(R\)/,"")
        gsub(/\(TM\)/,"")

        match($0, /Intel.*GHz/)
        cpu_type[n] = substr($0, RSTART, RLENGTH)

        match($0, /SMP/)
        num_cores[n] = substr($0, RSTART+4, 1)

        match($0, /[0-9]* MB/)
        mem_size[n] = substr($0, RSTART, RLENGTH-3)
    }
    else
    {
        printf "motd file %s is empty\n", n > "/dev/stderr"
    }
}



function readEthInfo(n, filename)
{
    FS = " "
    RS = "#"

    getline < filename

    close(filename)

    if(NF > 1)
    {
        match($0, /Speed: [0-9]+Mb/)
        link_speed[n] = substr($0, RSTART+7, RLENGTH-9)
    }
    else
    {
        printf "eth0_info file for %s is empty\n", n > "/dev/stderr"
    }
}



function readStderr(n,  filename, warnings_present)
{
    # Always Read stderr file if it is available

    if(path["stderr",n] != "")
    {
        FS = " "
        RS = "\n"
        warnings[n] = ""
        warnings_present = 0
        filename = path["stderr",n]

        # Scan stderr for warning messages

        while((getline < filename) > 0)
        {
            if(index($0,"FGCD starting class") > 0)
            {
                warnings[n] = "Started at " $1 " " $2
                version[n] = $8
                version_ascii[n] = $10 " " $11 " " $12 " " $13 " " $14
                warnings_present = 0
            }
            else if($1 == "WARNING:" || $1 == "ERROR:")
            {
                warnings_present = 1

                # If warnings already seen then add two newlines before the new warning

                if(warnings[n] != "")
                {
                    warnings[n] = warnings[n] html_nl html_nl
                }

                # Convert all double quotes to single quotes

                gsub(/\"/,"'")

                # Drop WARNING: from line but keep ERROR:

                if($1 == "WARNING:")
                {
                    $1 = ""
                    warnings[n] = warnings[n] substr($0,2)
                }
                else
                {
                    warnings[n] = warnings[n] $0
                }
            }
        }

        close(filename)
    }

    # fgcd[n] may be "offline", "unreachable" or empty
    # Convert "unreachable" into "dead"

    if(last_seen[n] != date || fgcd[n] == "unreachable")
    {
        fgcd[n] = "dead"
    }
    else if(fgcd[n] == "")
    {
        # Status is not "offline" so set it to "running" or "warning"

        if(warnings_present == 0)
        {
            fgcd[n] = "running"
        }
        else
        {
            fgcd[n] = "warning"
        }
    }
}



function readStdout(n,  value)
{
    # Read stdout for FEC if it responded to ping

    if(path["stdout",n] != "")
    {
        FS = ":"
        RS = "\n"
        filename = path["stdout",n]

        # Scan stdout for information key-value pairs

        while((getline < filename) > 0)
        {
            value[$1] = $2
        }

        close(filename)
    }

    # Set the information items from the last corresponding value in the file

    num_cmds[n] = setStdPar(value["Commands"])
    avl_mem[n] = setStdPar(value["Available RAM (KB)"])
    cmd_max_val_length[n] = setStdPar(value["CMD_MAX_VAL_LENGTH"])
}



function setTiming(n)
{
    if(n in host_timing)
    {
        timing_color[n] = "color: rgb(255,0,255);"
        timing_config[n] = "Host timing:" html_nl host_timing[n]
    }
    else if(class_timing[class[n], timing[n]] != "")
    {
        timing_color[n] = "color: rgb(0,127,0);"
        timing_config[n] = "Class timing:" html_nl class_timing[class[n], timing[n]]
    }
}


function setStdPar(value)
{
    if(value != "") return value

    return "unknown"
}



function writeHtmlTables(filename,  i, n, of, red_text, fgcd_colour, num_cols, col_widths, col_width, avl_percent)
{
    # Prepare variables for creating the table

    of = html_path "/" filename

    col_widths = "150 80 60 120 60 70 80 70 220 220 120 80 90 100 160"

    num_cols = split(col_widths, col_width, " ")

    computer["Intel Core i5-3550S CPU @ 3.00GHz"]   = "PCI-762 2U"
    computer["Intel Core i5-8500 CPU @ 3.00GHz"]    = "IPC647E"
    computer["Intel Core2 Duo CPU E6400 @ 2.13GHz"] = "PCI-760 R14E"
    computer["Intel Core2 Duo CPU E8400 @ 3.00GHz"] = "PCI-760 R16F"
    computer["Intel Pentium CPU D1519 @ 1.50GHz"]   = "MEN G25A"
    computer["Intel Xeon CPU E5-2630 v4 @ 2.20GHz"] = "Server"

    red_text[1]    = "color: rgb(255,0,0);"

    fgcd_colour["running"] = "color: rgb(0,127,0);"
    fgcd_colour["warning"] = "color: rgb(255,127,0);"
    fgcd_colour["offline"] = red_text[1]
    fgcd_colour["dead"]    = red_text[1]
    nfs_warning["1"] = "&#9888;"

    tooltip["FEC"]       = "From FGC group file." html_nl "Link to the FEC's FGC status page."
    tooltip["FGCD"]      = "From FGCD Survey. Warnings come from FEC's stderr file."
    tooltip["Class"]     = "From FGC name file"
    tooltip["Version"]   = "From FEC's stderr file"
    tooltip["FGCs"]      = "From FGC name file"
    tooltip["NFS"]       = "From NFS path to FEC log files, checked against Directory defined in CCDB."
    tooltip["Timing"]    = "From FGC group file." html_nl "Green when FEC uses class timing." html_nl "Orange when FEC uses host-specific timing."
    tooltip["State"]     = "From CCDB"
    tooltip["Group"]     = "From FGC group file"
    tooltip["Usage"]     = "From FGC group file"
    tooltip["Computer"]  = "From FEC's message-of-the-day (motd) file"
    tooltip["RAM"]       = "From FEC's message-of-the-day (motd) file. Available RAM from stdout file."
    tooltip["Cmds"]      = "From FEC's stdout file"
    tooltip["Link"]      = "Link speed from FEC's eth0_info file and network (GPN/TN) from CCDB."
    tooltip["Last seen"] = "From fping"

    # Write the fecs table header

    print  "<table class=\"confluenceTable wrapped fixed-table\" id=\"fecsTable\">" > of
    print  "  <colgroup>" > of

    for(i=1 ; i <= num_cols ; i++)
    {
        printf "    <col style=\"width: %3upx;\"/>\n",col_width[i] > of
    }

    print  "  </colgroup>" > of
    print  "  <thead>" > of
    print  "    <tr>" > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">FEC</th>\n", tooltip["FEC"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">FGCD</th>\n", tooltip["FGCD"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Class</th>\n", tooltip["Class"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Version</th>\n", tooltip["Version"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">FGCs</th>\n", tooltip["FGCs"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">NFS</th>\n", tooltip["NFS"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Timing</th>\n", tooltip["Timing"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">State</th>\n", tooltip["State"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Group</th>\n", tooltip["Group"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Usage</th>\n", tooltip["Usage"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Computer</th>\n", tooltip["Computer"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center;\" title=\"%s\">RAM<br/>(MB)</th>\n", tooltip["RAM"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Cmds</th>\n", tooltip["Cmds"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center;\" title=\"%s\">Link<br/>(Mb/s)</th>\n", tooltip["Link"] > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center; vertical-align: middle;\" title=\"%s\">Last seen</th>\n", tooltip["Last seen"] > of
    print  "    </tr>" > of
    print  "  </thead>" > of
    print  "  <tbody>" > of

    # Write table body

    for(i=1 ; i <= num_fecs ; i++)
    {
        n = fec_name[i]

        if(mem_size[n] > 0)
        {
            avl_percent = 100 * avl_mem[n] / (mem_size[n] * 1024)
        }
        else
        {
            avl_percent = "unknown "
        }
        printf "    <tr>\n" > of
        printf "      <td class=\"confluenceTd\">\n" > of
        printf "        <a href=\"http://cern.ch/fgc-device-status/statusdev.html?%s&%s&status&statustbl_h.html&0&%s\">%s</a>\n", n, toupper(n), toupper(n), n > of
        printf "      </td>\n" > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;%s\" title=\"%s\">%s</td>\n", fgcd_colour[fgcd[n]], warnings[n], fgcd[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%u</td>\n", class[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\" title=\"%s\">%u</td>\n", version_ascii[n], version[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\" title=\"%s\">%u</td>\n", fgcs[n], num_fgcs[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center\" title=\"Directory in CCDB: %s\">\n", directory[n] > of
        printf "        %s<a href=\"https://cs-ccr-www1.cern.ch/~pclhc/logs/fgcd/%s\">%s</a>\n", nfs_warning[nfs[n]!= "" && nfs[n] != directory[n]], n, nfs[n] > of
        printf "      </td>\n" > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;%s\" title=\"%s\">%s</td>\n", timing_color[n], timing_config[n], timing[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%s</td>\n", state[n] > of
        printf "      <td class=\"confluenceTd\">%s</td>\n", group[n] > of
        printf "      <td class=\"confluenceTd\">%s</td>\n", comment[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\" title=\"CPU:%s (%u cores)\">\n", cpu_type[n], num_cores[n] > of
        printf "        <a href=\"https://ccde.cern.ch/api/hardware/computers/name/%s\">%s</a>\n", n, computer[cpu_type[n]] > of
        printf "      </td>\n" > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;%s\" title=\"Available memory (KB): %s (%s%)\">%s</td>\n", red_text[avl_percent < 11], avl_mem[n], avl_percent,  mem_size[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\" title=\"CMD_MAX_VAL_LENGTH: %s\">%s</td>\n", cmd_max_val_length[n], num_cmds[n] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;%s\">\n", red_text[link_speed[n] < 1000] > of
        printf "        <a href=\"https://cosmos-grafana.cern.ch/rrd/bin/index.cgi?hostname=%s.cern.ch&plugin=netlink&plugin=tcpconns&timespan=3600&action=show_selection&ok_button=OK\">%u %s</a>\n", n, link_speed[n], network[n] > of
        printf "      </td>\n" > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;%s\">%s</td>\n", red_text[last_seen[n] != date], last_seen[n] > of
        printf "    </tr>\n" > of
    }

    # Write the table footer

    printf "  </tbody>\n" > of
    printf "</table>\n" > of

    # Write a second table for the counts of mem_sizes

    print "\n<br/>\n" > of

    print "<table class=\"confluenceTable wrapped fixed-table\">" > of
    print "  <colgroup>" > of
    print "    <col style=\"width: 120px;\"/>" > of
    print "    <col style=\"width:  80px;\"/>" > of
    print "  </colgroup>" > of
    print "  <thead>" > of
    print "    <tr>" > of
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">RAM (MB)</th>" > of
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Count</th>" > of
    print "    </tr>" > of
    print "  </thead>" > of
    print "  <tbody>" > of

    # Write the table body

    for(i=1 ; i <= num_mem_sizes ; i++)
    {
        printf "    <tr>\n" > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%s</td>\n", sorted_mem_size[i] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%u</td>\n", count_mem_size[sorted_mem_size[i]]++ > of
        printf "    </tr>\n" > of
    }
    # Write the table footer

    print  "    <tr>" > of
    print  "      <th class=\"confluenceTh\" style=\"text-align: right;\">Total</th>" > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center;\">%u</th>\n", num_fecs > of
    printf "  </tbody>\n" > of
    printf "</table>\n" > of

    # Write a third table for the counts of cpu_types

    print "\n<br/>\n" > of

    print "<table class=\"confluenceTable wrapped fixed-table\">" > of
    print "  <colgroup>" > of
    print "    <col style=\"width: 120px;\"/>" > of
    print "    <col style=\"width: 320px;\"/>" > of
    print "    <col style=\"width:  80px;\"/>" > of
    print "  </colgroup>" > of
    print "  <thead>" > of
    print "    <tr>" > of
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Computer</th>" > of
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">CPU</th>" > of
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Count</th>" > of
    print "    </tr>" > of
    print "  </thead>" > of
    print "  <tbody>" > of

    # Write the table body

    for(i=1 ; i <= num_cpu_types ; i++)
    {
        printf "    <tr>\n" > of
        printf "      <td class=\"confluenceTd\">%s</td>\n", computer[sorted_cpu_type[i]] > of
        printf "      <td class=\"confluenceTd\">%s</td>\n", sorted_cpu_type[i] > of
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%u</td>\n", count_cpu_type[sorted_cpu_type[i]]++ > of
        printf "    </tr>\n" > of
    }

    # Write the table footer

    print  "    <tr>" > of
    print  "      <th colspan=\"2\" class=\"confluenceTh\" style=\"text-align: right;\">Total</th>" > of
    printf "      <th class=\"confluenceTh\" style=\"text-align: center;\">%u</th>\n", num_fecs > of
    print  "    </tr>" > of
    printf "  </tbody>\n" > of
    printf "</table>\n" > of

    # Write time stamp below the table

    printf "<p><sup>Last update: %s</sup></p>\n", date > of

    # Write style block for table sorter

    print "<style type=\"text/css\">" > of
    print "  .tablesorter .filtered { display: none; }" > of
    print "  .tablesorter-filter-row { background: rgb(210,210,210); }" > of

    for(i=1 ; i <= num_cols ; i++)
    {
        printf "    .tablesorter .tablesorter-filter-row td:nth-child(%u) .tablesorter-filter { width: %upx; }\n",i,col_width[i]-1 > of
    }

    print "</style>" > of

    # Write script section for table sorter

    print "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js\"></script>" > of
    print "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.widgets.min.js\"></script>" > of
    print "<script>" > of
    print "    $('#fecsTable').tablesorter({ widgets : ['filter','stickyHeaders'] });" > of
    print "    $(\"#fecsTable th\").addClass(\"sorter-false\");" > of

    for(i=0 ; i < num_cols ; i++)
    {
        printf "    $(\"#fecsTable th:eq(%u)\").removeClass(\"sorter-false\");\n",i > of
    }

    print "</script>" > of

    # Tables completed

    printf "Writing %s completed.\n", of > "/dev/stderr"

    close(of)
}

# EOF
