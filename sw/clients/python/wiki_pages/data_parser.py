import os
import re
import subprocess
import time
from collections import defaultdict
from fgc_device import Fgc, TimingConfig
from typing import List, Dict, DefaultDict, Tuple


TARGETS_PATH = '/user/pclhc/etc/fgcd/group'
NAMEFILE_PATH = '/user/pclhc/etc/fgcd/name'
FGC_DEVICES_PATH = "~pclhc/etc/config/devices/"
SUBDEVICES_FOLDER = '/user/pclhc/etc/fgcd/sub_devices'
TIMING_FOLDER = '/user/pclhc/etc/fgcd/timing/timdt'
PCLHC_URL = 'https://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/timing/timdt/'
CCDB_TIMING_URL = 'https://apex-sso.cern.ch/pls/htmldb_dbabco/f?p=CONFIG_BROWSER:160:110577167256927::NO::'

# Dictionaries of targets
fecs_in_target = defaultdict(list)
fecs_in_timing_domain = defaultdict(list)
targets = dict()

# mapping between FEC fieldbus and FGC fieldbus
fieldbus_mapping = {
    "ABORTREF": "ABORT",
    "STARTREF1WARNING": "START_REF_1",
    "STARTREF2WARNING": "START_REF_2",
    "STARTREF3WARNING": "START_REF_3",
    "STARTREF4WARNING": "START_REF_4",
    "STARTREF5WARNING": "START_REF_5",
    "CYCLEWARNING": "START_CYCLE",
    "INJECTWARNING": "INJECTION",
    "EXTERNAL": "",
    "EXTERNAL_LAB": ""
}


def parse_targets():
    with open(TARGETS_PATH) as file:
        for line in file:
            fec, timing_domain, target, *_  = line.split(':')
            target = target.split('/')[0].strip()

            fecs_in_target[target].append(fec)
            fecs_in_timing_domain[timing_domain].append(fec)
            targets[fec] = (target, timing_domain)


def run_bash_command(command) -> str:
    # Run bash command and capture its output
    process = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, close_fds=True, encoding="utf-8")
    process.wait()
    return process.stdout.read()


def get_timing_domain_events() -> Dict[str, List[str]]:
    # Get the events by area as a map[<AREA>]=(<LIST_OF_EVENTS>)
    domain_events = {}
    for timing_domain in os.listdir(TIMING_FOLDER + '/class/4/'):
        events = list()
        with open(TIMING_FOLDER + '/class/4/' + timing_domain, mode='r') as file:
            for line in file:
                events.append(line)

        domain_events[timing_domain] = events

    return domain_events


def get_fec_events() -> DefaultDict[str, Tuple[str, str, str, str]]:
    """
            Parse data in 3 stages:
            1: parse the files in .../timdt/host/ which contain the timing conditions for specific FECs, store in table_lines
            2: parse the files in .../timdt/class/4/ which contain the timing conditions for specific areas and store it
               in a temporary dictionary (target_events[<TARGET>]=(<LIST_OF_EVENTS>)
            3: Obtain a list of all the FECs, and for each one assign a target and respective timing conditions,
               ignore previously processed FECs (step 1)
    """

    processed_fecs = set()

    # Dictionary of timing events by FEC
    fec_events = defaultdict(list)

    # parse timing conditions for specific FECs
    for fec in os.listdir(TIMING_FOLDER + '/host/'):
        # skip hidden files
        if fec.startswith('.'):
            continue

        processed_fecs.add(fec)
        try:
            with open(TIMING_FOLDER + '/host/' + fec, mode='r') as file:
                for line in file:
                    # fec_events[fec].append(line)
                    fieldbus, timing, delay = line.strip().split(':')
                    fec_events[fec].append((fieldbus, timing, delay, f'host/{fec}'))
        except Exception as error:
            processed_fecs.remove(fec)
            # TODO: log error msg

    domain_events = get_timing_domain_events()

    # Add events to the dictionary ignoring the already processed fecs
    for timing_domain in domain_events.keys():

        fecs = fecs_in_timing_domain[timing_domain]
        # Check if list of fecs is empty
        if not fecs:
            continue

        for f in fecs:
            # ignore previouly processed FECs
            if f in processed_fecs:
                continue

            for event in domain_events[timing_domain]:
                fieldbus, timing, delay = event.strip().split(':')
                fec_events[f].append((fieldbus, timing, delay, f'class/4/{timing_domain}'))

    return fec_events


def merge_fec_events(fec_events: DefaultDict[str, Tuple[str, str, str, str]]) -> DefaultDict[str, Tuple[str, str, str, str]]:
    """
    Merge fecs if they have the same events and target

    FROM:
    fec_events[FEC_1] = [evt_A, evt_B]
    fec_events[FEC_2] = [evt_A, evt_B]
    fec_events[FEC_3] = [evt_A, evt_B]

    TO:
    fec_events[ FEC_1<br/>FEC_2<br/>FEC_3 ] = [evt_A, evt_B]

    @param fec_events: dict with list of events by gateway
    @return: fec_events with merged keys
    """

    # Merge duplicates and re-insert with merged names
    to_remove = set()
    new_entries = defaultdict(list)
    for fec_1 in fec_events.keys():
        duplicates = list()
        # remove if the FEC is not found in any target (maybe it is a backup or a debug file e.g.: FEC_NAME-debug-1)
        if fec_1 not in targets.keys():
            to_remove.add(fec_1)
        if fec_1 in to_remove:
            continue
        for fec_2 in fec_events.keys():
            # remove if the FEC is not found in any target
            if fec_2 not in targets.keys():
                to_remove.add(fec_2)
            if fec_2 in to_remove:
                continue

            # add to duplicates if events and target are equal
            if fec_events[fec_1] == fec_events[fec_2] and targets[fec_1] == targets[fec_2]:
                duplicates.append(fec_2)
                to_remove.add(fec_2)

        # Insert merged duplicates
        merged_fecs = '<br/>'.join(duplicates)
        new_entries[merged_fecs] = fec_events[fec_1]

    # Remove duplicates
    for fec in to_remove:
        del fec_events[fec]
    # Insert new merged entries
    for fecs in new_entries.keys():
        fec_events[fecs] = new_entries[fecs]
        fec_1 = fecs.split('<', 1)[0]
        targets[fecs] = targets[fec_1]

    return fec_events


def parse_fgc_devices() -> DefaultDict[str, Fgc]:
    # Dictionary of Fgc objects
    fgc_devices = defaultdict(Fgc)

    with open(NAMEFILE_PATH, mode='r') as file:
        for line in file:
            fec, _, classnum, device, _ = line.strip().split(':')

            fgc_devices[device] = Fgc(device)
            fgc_devices[device].fec = fec
            fgc_devices[device].class_num = classnum
            fgc_devices[device].area, fgc_devices[device].timing_domain = targets[fec]

    return fgc_devices


def parse_fgc_fieldbus(fgc_devices: DefaultDict[str, Fgc]) -> Tuple[DefaultDict[str, Fgc], List[str]]:
    output = subprocess.check_output(f"grep -r 'REF.EVENT_CYC' {FGC_DEVICES_PATH}", shell=True, encoding="utf-8")
    skipped = list()

    for line in output.splitlines():
        device = os.path.basename(line.split(':')[0]).split('.txt')[0]
        if device in fgc_devices.keys():
            for fieldbus in line.split()[3:]:
                fgc_devices[device].timing.append(TimingConfig(fieldbus=fieldbus))
        else:
            skipped.append(device)
    return fgc_devices, skipped


def parse_fgc_timing(fgc_devices: DefaultDict[str, Fgc],
                     fec_events: DefaultDict[str, Tuple[str, str, str, str]]) -> DefaultDict[str, Fgc]:

    # Assign timing condition to the correct device
    for device in fgc_devices.keys():
        if fgc_devices[device].fec in fec_events.keys():
            for fec_fieldbus, fec_timing, delay, source in fec_events[fgc_devices[device].fec]:
                # print(fec_fieldbus, fec_timing, fieldbus)

                # fieldbuses in the FGC and the FEC are not exactly the same name, so a mapping is needed
                # thus, we check if our mapping contains this fec_fieldbus, if so, we add this fec_timing
                if fec_fieldbus in fieldbus_mapping.keys():
                    for timing_config in fgc_devices[device].timing:
                        if timing_config.fieldbus == fieldbus_mapping[fec_fieldbus]:
                            timing_config.event = fec_timing
                            timing_config.delay = delay
                            fgc_devices[device].timing_source = source
                            break
    return fgc_devices


def parse_fgc_aliases(fgc_devices: DefaultDict[str, Fgc]) -> DefaultDict[str, Fgc]:
    # iterate through devices in the folder
    for filename in os.listdir(SUBDEVICES_FOLDER):
        with open(SUBDEVICES_FOLDER + '/' + filename, mode='r') as file:
            for line in file:
                data = line.strip().split(':')
                if len(data) < 3:
                    continue
                fgc = data[0]
                alias = data[2]

                if fgc in fgc_devices.keys():
                    fgc_devices[fgc].alias = alias

    return fgc_devices


def skip_fgc_devices(fgc_devices: DefaultDict[str, Fgc]) -> DefaultDict[str, Fgc]:
    """
    Remove all devices that have MODE.PC_ON != CYCLING
    """
    output = subprocess.check_output(f"grep -r 'MODE.PC_ON                     CYCLING' {FGC_DEVICES_PATH}",
                                     shell=True, encoding="utf-8")

    valid_devices = set()

    # parse the device name from the grep output and add to the set of valid_devices
    for line in output.splitlines():
        device = os.path.basename(line.split(':')[0]).split('.txt')[0]
        # del fgc_devices[device]
        valid_devices.add(device)

    # iterate the devices in the device collection and remove the invalid ones
    # (except if it has class 62 or 65, these do not have MODE.PC_ON property)
    all_devices = list(fgc_devices.keys())
    for device in all_devices:
        if device not in valid_devices and fgc_devices[device].class_num not in ['62', '65']:
            del fgc_devices[device]

    return fgc_devices


def get_ccdb_timing_url(timing_event: str) -> str:
    return f'{CCDB_TIMING_URL}P160_EVENT_NAME,P160_QUERY:{timing_event},YES'



# Parse the targets as soon as we import this file
parse_targets()
