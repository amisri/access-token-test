from typing import List, Dict, Set
from pathlib import Path
from dataclasses import dataclass
import datetime
import operator
import requests
import json
import subprocess
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


@dataclass
class FrontEnd:
    name: str
    id: int = -1
    device_class: str = ""
    class_version: str = ""
    operational: bool = False
    timing: str = ""
    accelerator: str = ""
    devices: List[str] = None
    usage: str = ""


CLASSES_TO_PROCESS = (
    "PowM1553",
    "PowPLC",
    "PowRs422",
    "PowMainPS",
    "PowJC"
)

PING_FILE = Path(".fesa_fec_info.json")

# datetime format
DT_FORMAT = "%Y-%m-%d %H:%M"


def get_devices_from_ccda(class_name: str) -> List[Dict]:
    # CCDA API
    URL = f"https://ccda.cern.ch:8900/api/devices/search?query=deviceClassInfo.name%3D%3D{class_name}&page=0&size=5000&sort=id%2Cdesc"

    # session object
    session = requests.session()

    # run the request
    devices_request = session.get(URL, verify=False)

    # load data
    parsed_devices = json.loads(devices_request.text)

    return parsed_devices['content']


def process_ccda_data(devices: List[Dict]) -> Dict[str, FrontEnd]:
    frontends: Dict[str, FrontEnd] = {}

    for device in devices:
        frontend_name = device["fecName"]

        # get/add fec
        if frontend_name not in frontends:
            frontends[frontend_name] = FrontEnd(name=frontend_name, devices=[])
            frontend = frontends[frontend_name]

            # get class data
            class_data = device["deviceClassInfo"]
            frontend.device_class = class_data["name"]
            frontend.class_version = class_data["version"]

            frontend.accelerator = device["accelerator"]
            frontend.timing = device["timingDomain"]

        frontend = frontends[frontend_name]

        # add device
        frontend.devices.append(device["name"])

    return frontends


def get_additional_frontend_data(fec_names: List[str]) -> List[Dict]:
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

    # CCDA API
    URL = f"https://ccda.cern.ch:8900/api/computers/names"

    # session object
    session = requests.session()

    # run the request
    request = session.post(URL, data=json.dumps(fec_names), headers=headers, verify=False)

    # load data
    parsed_data = json.loads(request.text)

    return parsed_data


def update_frontends(frontends: Dict[str, FrontEnd]) -> Dict[str, FrontEnd]:
    data = get_additional_frontend_data(list(frontends.keys()))
    for frontend_data in data:
        fec_name = frontend_data["name"]
        frontend = frontends[fec_name]

        frontend.id = frontend_data["id"]
        frontend.operational = frontend_data["state"] == "operational"
        frontend.usage = frontend_data["description"]

    return frontends


def get_all_data() -> Dict[str, Dict[str, FrontEnd]]:
    data = {}
    for class_name in CLASSES_TO_PROCESS:
        ccda_devices = get_devices_from_ccda(class_name)
        frontends = process_ccda_data(ccda_devices)
        frontends = update_frontends(frontends)
        data[class_name] = frontends
    return data


def handle_file(filename: Path, fecs: List[str], alive_fecs: List[str], now: str) -> Dict[str, str]:
    # prepare data to write
    data_to_write = dict()
    for fec in fecs:
        last_ping = datetime.datetime.min.strftime(DT_FORMAT)
        if fec in alive_fecs:
            last_ping = now

        data_to_write[fec] = last_ping

    # read data from file
    try:
        with open(filename, 'r') as f:
            data_from_file = json.load(f)
    except:
        data_from_file = {}

    for fec, last_ping in data_to_write.items():
        if fec in data_from_file:
            previous_ping = data_from_file[fec]

            data_to_write[fec] = max([last_ping, previous_ping])

    # write to file
    with open(filename, 'w') as f:
        json.dump(data_to_write, f)

    return data_to_write


def get_fecs_list(data: Dict[str, Dict[str, FrontEnd]]) -> List[str]:
    unique_fecs = set()
    for class_name, fecs in data.items():
        for fec in fecs.keys():
            unique_fecs.add(fec)
    return list(sorted(unique_fecs))


def get_alive_fecs(fec_names: List[str]) -> List[str]:
    command = ['/usr/sbin/fping', "-a"]
    command.extend(fec_names)
    command.append("2> /dev/null")
    res = subprocess.run(command, capture_output=True)
    alive_fecs = res.stdout.decode("utf-8").split('\n')[:-1]
    return alive_fecs

def get_lumens_data(fec_name: str):
    result = {}
    # get data
    command = ['/usr/local/sbin/lumensctl', "-H", fec_name, "-S Pow*_M" "list"]

    res = subprocess.run(command, capture_output=True)
    data = res.stdout.decode("utf-8")
    
    # extract data
    data = data.splitlines()
    if data:
        # skip first line (header)
        if not data[0].strip():
            data.pop(0)
        if data and data[0].startswith("SINCE"):
            data.pop(0)

        for line in data:
            # before and after ago
            ago_split = line.split("ago")
            ago = ago_split[0].rstrip()
            split_rest = ago_split[1].split(' ')
            split_rest = list(filter(len, split_rest))
            
            state = split_rest[1]
            status = split_rest[2] == "running"
            process = split_rest[3]
            class_name = process.split('_')[0]

            result[class_name] = ago, status
    
    return result

def print_html_data(data: Dict[str, Dict[str, FrontEnd]], fec_timestamps: Dict[str, str], timestamp_now: str, lumens_data):
    col_widths = [160, 80, 100, 120, 100, 70, 80, 70, 350, 160]
    num_cols = len(col_widths)

    red_text = "color: rgb(255,0,0);"
    #
    # fgcd_colour["running"] = "color: rgb(0,127,0);"
    # fgcd_colour["warning"] = "color: rgb(255,127,0);"
    # fgcd_colour["offline"] = red_text[1]
    # fgcd_colour["dead"] = red_text[1]
    # nfs_warning["1"] = "&#9888;"
    tooltip = {}
    tooltip["FEC"] = "From CCDB.<br />Link to the FEC's CCDB page."
    tooltip["FESA"] = "From CCDB"
    tooltip["Class"] = "From CCDB"
    tooltip["Version"] = "From CCDB"
    tooltip["Devices"] = "From CCDB"
    tooltip["NFS"] = "Directory defined in CCDB."
    tooltip["Timing"] = "From CCDB."
    tooltip["State"] = "From CCDB"
    tooltip["Usage"] = "From CCDB"
    # tooltip["Computer"] = "From FEC's message-of-the-day (motd) file"
    # tooltip["RAM"] = "From FEC's message-of-the-day (motd) file. Available RAM from stdout file."
    # tooltip["Cmds"] = "From FEC's stdout file"
    # tooltip["Link"] = "Link speed from FEC's eth0_info file and network (GPN/TN) from CCDB."
    tooltip["Last seen"] = "From fping"

    # Write the fecs table header

    print("""
    <table class="confluenceTable wrapped fixed-table" id="fecsTable">
        <colgroup>
    """)

    for i in range(0, num_cols):
        print(f"<col style=\"width: {col_widths[i]}px;\"/>")
    print(f"""
        </colgroup>
        <thead>
            <tr>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['FEC']}">FEC</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['FESA']}">FESA</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['Class']}">Class</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['Version']}">Version</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['Devices']}">Devices</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['NFS']}">NFS</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['Timing']}">Timing</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['State']}">State</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['Usage']}">Usage</th>
                <th class="confluenceTh" style="text-align: center; vertical-align: middle;" title="{tooltip['Last seen']}">Last seen</th>
            </tr>
        </thead>
        <tbody>
    """)
    # Write table body
    for class_name, fecs in data.items():
        for fec_name, fec in fecs.items():
            last_seen = fec_timestamps[fec_name]

            print(f"""
            <tr>
                <td class="confluenceTd">
                    <a href="https://ccde.cern.ch/hardware/computers/{fec.id}">{fec.name}</a>
                </td>
                <td class="confluenceTd" style="text-align: center;">
                    ???
                </td>
                <td class="confluenceTd" style="text-align: center;">
                    {fec.device_class}
                </td>
                <td class="confluenceTd" style="text-align: center;">
                    {fec.class_version}
                </td>
                <td class="confluenceTd" style="text-align: center;">
                    {len(fec.devices)}
                </td>

                <td class="confluenceTd" style="text-align: center;">
                    {fec.accelerator}
                </td>

                <td class="confluenceTd" style="text-align: center;">
                    {fec.timing}
                </td>

                <td class="confluenceTd" style="text-align: center;">
                    {"Op" if fec.operational else "Dev"}
                </td>

                <td class="confluenceTd" style="text-align: center;">
                    {fec.usage}
                </td>
                
                <td class="confluenceTd" style="text-align: center; {red_text if last_seen < timestamp_now else ""}">
                    {last_seen}
                </td>
            </tr>
            """)
    print("""
        </tbody>
    </table>
    """)
    # Write last update
    print(f"<p><sup>Last update: {timestamp_now}</sup></p>")

    # Sorting

    print("""
        <style type="text/css">
            .tablesorter .filtered { display: none; }
            .tablesorter-filter-row { background: rgb(210,210,210); }
    """)
    for i, width in enumerate(col_widths):
        print(f'.tablesorter .tablesorter-filter-row td:nth-child({i+1}) .tablesorter-filter {{ width: {width}px; }}')
    print("</style>")
    
    print("""

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.widgets.min.js"></script>
        <script>
            $('#fecsTable').tablesorter({ widgets : ['filter','stickyHeaders'] });
            $("#fecsTable th").addClass("sorter-false");
    """)
    for i in range(num_cols):
        print(f'$("#fecsTable th:eq({i})").removeClass("sorter-false");')
    print("""
        </script>
    """)

if __name__ == '__main__':
    data = get_all_data()
    fecs = get_fecs_list(data)
    alive_fecs = get_alive_fecs(fecs)

    lumens_data = {}
    # for alive_fec in alive_fecs:
    #     lumens_data[alive_fec] = get_lumens_data(alive_fec)

    # get current timestamp
    now = datetime.datetime.now().strftime(DT_FORMAT)

    # initialize save file

    # read&write file
    fecs_timestamps = handle_file(PING_FILE, fecs, alive_fecs, now)

    print_html_data(data, fecs_timestamps, now, lumens_data)

