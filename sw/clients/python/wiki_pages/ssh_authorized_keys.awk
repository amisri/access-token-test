# ssh_authorized_keys.awk
#
# This script will read the authorized_keys file for the user running the scripts and write a
# summary Confluence table to stdout.

BEGIN {

    if(ARGC != 1)
    {
        print "usage: awk -f fgc-fecs-info.awk"
        exit -1
    }

    username = ENVIRON["USER"]

    # Get supported ssh key types

    RS = "#"
    FS = " "

    "ssh -Q key" | getline ssh_Q_key

    n = split(ssh_Q_key, key_types, "\n")

    for(i = 1 ; i <= n ; i++)
    {
        supported_key_types[key_types[i]] = 1
    }

    # Public keys consist of the following space-separated fields: options, keytype, base64-encoded key, comment.

    RS = "\n"
    auth_keys_file = "/user/" username "/.ssh/authorized_keys"

    while((getline < auth_keys_file) > 0)
    {
        if(NF > 1 && substr($1,1,1) != "#")
        {
            process()
        }
    }

    close(auth_keys_file)

    # Sort by numbered comment fields

    asort(sort_key)

    # Write confluence tables into file

    # Write the confluence table

    print "<table class=\"confluenceTable wrapped fixed-table\">"
    print "  <colgroup> <col style=\"width: 400.0px;\"/> <col style=\"width: 200.0px;\"/> <col style=\"width: 100.0px;\"/> <col style=\"width: 400.0px;\"/> </colgroup>"
    print "  <tbody>"
    print "    <tr>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Comment</th>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Key type</th>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Key length</th>"
    print "      <th class=\"confluenceTh\" style=\"text-align: center;\">Options</th>"
    print "    </tr>"

    # Generate the table contents

    for(i = 1 ; i <= num_keys ; i++)
    {
        k = sort_key[i]

        printf "      <td class=\"confluenceTd\">%s</td>\n", comment[k]
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%s</td>\n", key_type[k]
        printf "      <td class=\"confluenceTd\" style=\"text-align: center;\">%u</td>\n", key_length[k]
        printf "      <td class=\"confluenceTd\">%s</td>\n", options[k]
        printf "    </tr>\n"
    }

    # Write the table footer

    printf "  </tbody>\n"
    printf "</table>\n"

    # Write time stamp in the footer

    printf "<p><sup>Last update: %s</sup></p>\n", strftime("%Y-%m-%d %H:%M")
}



function process()
{
    line_num++

    for(i = 1 ; i < NF; i++)
    {
        if($i in supported_key_types)
        {
            count[$NF]++
            k = $NF "_" count[$NF]
            sort_key[++num_keys] = k
            comment[k] = $NF
            key_type[k] = $i
            key_length[k] = length($(i+1))

            if(i > 1)
            {
                for(j = 1 ; j < i ; j++)
                {
                    options[k] = options[k] " " $j
                }
            }

            return
        }
    }
    printf "Error: ignoring line %u: no supported key type found\n", line_num > "/dev/stderr"
}

# EOF
