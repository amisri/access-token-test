# Filename: released_fgc_versions.py
#
# Purpose:  Parse codes_inventory.sh script which provides versions of installed fgc codes
#           in all the release areas/targets and show it as 2 html tables
#
# Location: Placed in production with '$ make install'.
#           Then it is accessible under /user/pclhc/lib/python/packages/wiki_pages/
#
# Notes:    Converted from the Perl script released_fgc_versions.pl
#
# Usage:    Script called by wiki_pages/gitlab-ci.jobs.yml

import subprocess
from sys import stdin, stdout
import time
from parse import compile
import parse
from collections import defaultdict
from datetime import datetime
import re


LATEST = "latest"

# Sets of codes that usually have the same version so they should be grouped in the same table row
codeGroups = [{'C003_50_IDDB', 'C004_50_PTDB'},
              {'C005_50_SysDB', 'C006_50_CompDB', 'C007_50_DIMDB'},
              {'C018_60_IDDB', 'C019_60_PTDB'},
              {'C020_60_SysDB', 'C021_60_CompDB', 'C022_60_DIMDB'}]

# Create a few auxiliar sets
fgc2_codes = set()
fgc3_codes = set()
fgc2_targets = set()
fgc3_targets = set()

# Create a map like structure (key,key,value) with a nested dictionary
# to store the data as (code_name, target, code_version)
code_map = defaultdict(lambda: defaultdict(int))

code_history = defaultdict(lambda: defaultdict(list))


def parse_and_store(stream):
    """
    Parses the output of the script code_inventory.sh and stores the data in a
    map(code_name, experiment, code_version) for further use

    @param stream: lines to be parsed
    """

    # print("Beginning stream parsing...")
    processed_codes = 0
    data = stream.readlines()
    pattern = compile('{}: class_id= {:d}, code_id= {:d}, crc={}, old_version= {}, version={:d} {:tc}')
    target = 'none'

    for line in data:
        if line.startswith('code'):
            # Remove \n and possible whitespaces at the end > Split by / > pop last element
            target = line.rstrip().split('/').pop()
            continue
        if line.startswith('C0'):
            # remove \n
            line = line.strip()
            code_name, class_num, _, _, _, code_version, _ = pattern.parse(line).fixed
            code_name = code_name.rstrip()
            processed_codes += 1

            # Add code name to corresponding set
            if 50 <= class_num < 60:
                fgc2_codes.add(code_name)
                fgc2_targets.add(target)

            elif 60 <= class_num < 70:
                fgc3_codes.add(code_name)
                fgc3_targets.add(target)

            elif class_num == 31:
                fgc2_codes.add(code_name)
                fgc3_codes.add(code_name)

            else:
                continue


            # Add code to the map
            code_map[code_name][target] = code_version

            # Update latest version of the code
            if code_version > code_map[code_name][LATEST]:
                code_map[code_name][LATEST] = code_version

    # print("Processed ", processed_codes, "codes")


def get_code_history(code_name, target):
    # $ grep -i -B 1 -A 1 'Test_Validation' /user/pclhc/etc/fgcd/installed_codes/C020_60_SysDB-C021_60_CompDB-C022_60_DIMDB/install_log.txt | tail -n 12

    # grep the install_log
    args = ['grep', '-i', '-B 1', '-A 1', target, f'/user/pclhc/etc/fgcd/installed_codes/{code_name}/install_log.txt']
    process = subprocess.Popen(args, stdout=subprocess.PIPE, universal_newlines=True)
    # grab only the last lines
    process = subprocess.Popen(['tail', '-n 11'], stdout=subprocess.PIPE, stdin=process.stdout, universal_newlines=True)

    history=[]

    data = process.stdout.readlines()
    author = ""
    install_date = ""
    code_version = ""
    for line in data:
        if re.match('^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', line) or 'CET' in line or 'CEST' in line:
            # Remove timezone and multiple spaces
            install_date = line.strip().replace("CET", "").replace("CEST", "")
            # install_date = " ".join(install_date.split()[1:])
        elif line.startswith("New codes"):
            pattern = compile('New codes installed by {} at: {}\n')
            author, target = pattern.parse(line).fixed
        elif line.startswith("C0"):
            pattern = compile('{}, version={} {:tc}\n')
            _, code_version, _ = pattern.parse(line).fixed
            release_date = datetime.fromtimestamp(int(code_version))
            release_date = release_date.strftime("%Y-%m-%d %H:%M:%S")
            history.append(f'{install_date} {author} installed {code_version} ({release_date})')
            author = ""
            install_date = ""
            release_date = ""
            code_version = ""

    return history


def print_table_to_html(set_of_codes, set_of_targets):
    """
    Prints an html table with the code version for each experiment/target

    @param file: file to print to
    @param set_of_codes: code names to be printed
    @param set_of_targets: targets to be printed
    """
    targets = list(set_of_targets)
    targets.sort()

    codes = list(set_of_codes)
    codes.sort()

    # print('<p></p>')

    print('<p align="right">Table generated', time.ctime(), '</p>')
    print('<div style="overflow-x:auto;">')
    print('<TABLE class="confluenceTable">')
    print('<TR>')

    header_items = ['Code class (link to release notes)']
    header_items = header_items + list(targets)

    print('<TH class="confluenceTh">' + '</TH>\n<TH class="confluenceTh">'.join(header_items) + '</TH>')
    print('</TR>')

    for c in codes:
        print('<TR>')
        print('<TD class="confluenceTd"><A href="https://gitlab.cern.ch/ccs/fgc/tree/master/sw/fgc/codes/'
              + str(c) + '/history.txt">' + shorten_code(str(c)) + '</A></TD>')
        for t in targets:
            version = code_map[c][t]
            history = get_code_history(c, t)
            history = '<br/>'.join(history)
            date = datetime.fromtimestamp(version)
            if version == 0:
                print('<TD class="highlight-grey confluenceTd"></TD>')
            elif is_latest_version(c, t):
                print('<TD class="confluenceTd">')
                print('<SPAN class="timestamp" style="color: green;">' + str(version) + '</SPAN><BR>')
                print('<SPAN style="color: grey;">' + date.strftime("%Y-%m-%d<br>%H:%M:%S") + '</SPAN>')
                if len(history):
                    print(f'<DIV class="popup">{history}</DIV>')
                print('</TD>')
            else:
                print('<TD class="confluenceTd">')
                print('<SPAN class="timestamp" style="color: red;">' + str(version) + '</SPAN><BR>')
                print('<SPAN style="color: grey;">' + date.strftime("%Y-%m-%d<br>%H:%M:%S") + '</SPAN>')
                if len(history):
                    print(f'<DIV class="popup">{history}</DIV>')
                print('</TD>')

        print('</TR>')

    print('</table>')
    print('</div>')


def print_common():
    """ Print the styles and functions """

    print('<style type="text/css">')
    print('    .popup {')
    print('        position: fixed;/*VERY IMPORTANT*/')
    print('        padding: 20px;')
    print('        color: white;')
    print('        box-sizing: border-box;')
    print('        white-space: nowrap;')
    print('        transform: translate(-50%, 40px);')
    print('        background-color: rgba(0, 0, 0, 0.7);')
    print('        visibility: hidden;')
    print('    }')
    print('    .timestamp:hover ~ .popup {')
    print('        visibility: visible;')
    print('    }')
    print('    .timestamp:hover {')
    print('        font-weight: bold;')
    print('    }')
    print('</style>')
    # print('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>') # do not print unless for TESTING, it breaks the Confluence index
    print('<script type="text/javascript">')
    print('    $(document).on("mousemove", function(e){')
    print('        var width = document.getElementsByClassName("popup")[0].offsetWidth;')
    print('        let page_width = document.body.clientWidth;')
    print()
    print('         /* grab the mouse position */')
    print('        let x_pos = e.clientX;')
    print('        let y_pos = e.clientY;')
    print()
    print('        /* avoid the popup to be drawn outside the page */')
    print('        if( (x_pos + width/2) > page_width ){')
    print('            let x_overflow = (x_pos + width/2) - page_width;')
    print('            x_pos -= x_overflow;')
    print('        }')
    print('        else if ( (x_pos - width/2) < 0) {')
    print('            let x_overflow = x_pos - width/2;')
    print('            x_pos -= x_overflow;')
    print('        }')
    print()
    print('        $(".popup").css({')
    print('        left:  x_pos + "px" ,')
    print('        top:   y_pos + "px"')
    print('        });')
    print('    });')
    print('</script>')
    print()
    print()


def is_latest_version(code_name, target):
    """
    Tests if provided code installed in the given experiment/target is the newest version

    @param code_name:
    @param target:
    @return: True if latest version , False otherwise
    """
    if code_map[code_name][target] == code_map[code_name][LATEST]:
        return True
    return False


def group_codes():
    """
    Groups sets of codes predefined in @codeGroups if their versions are the same in every experiment.
    Deletes individual codes from sets @fgc2_codes or @fgc3_codes and adds new one with the code names merged.
    Stores new entry in the @code_map with the code names merged.
    """

    global fgc2_codes
    global fgc3_codes

    for group in codeGroups:

        if is_groupable(list(group)):

            # create auxiliar list to order and build string for new merged code name
            aux_list = list(group)
            aux_list.sort()
            new_code_name = '-'.join(aux_list)

            # remove codes from respective set and add the new_code_name
            if group.issubset(fgc2_codes):
                fgc2_codes = fgc2_codes - group
                fgc2_codes.add(new_code_name)
            if group.issubset(fgc3_codes):
                fgc3_codes = fgc3_codes - group
                fgc3_codes.add(new_code_name)

            # add new_code_name to the code map with same content (code versions) as one of the codes in the group
            some_code_in_group = aux_list[0]
            code_map[new_code_name] = code_map[some_code_in_group]


def is_groupable(code_list):
    """
    Tests if code versions of the given codes are the same for EVERY experiment/target

    @param code_list: list of codes to verify, shorten with each function call until there are no items left
    @return: True if versions are equal and therefore the codes are groupable, False otherwise
    """

    first = code_list[0]

    while code_list:
        last = code_list.pop()

        if first == last:
            # list is empty, popped all its items.
            # If we are here all items have the same versions, end of recursion and return True
            return True
        # quickly compare the versions by comparing the 2 full lines of the dictionary for these codes
        if code_map[first] != code_map[last]:
            break

    return False


def shorten_code(full_code_name):
    """
    Trims the beginning of the code name ( C0***_SOME_CODE -> SOME_CODE )

    @param full_code_name:
    @return: codeShort - short version of the code name
    """

    # Check if we're handling a merged code
    if '-' in str(full_code_name):
        res = str(full_code_name).split('-', 2)
        i = 0
        # Trim the beginning
        while i < len(res):
            res[i] = (res[i].split('_', 1)).pop()
            i += 1
        # Merge again
        return '-'.join(res)

    # Just a normal code to trim
    return (str(full_code_name).split('_', 1)).pop()


def main():
    # Run code_inventory.sh and capture its output
    process = subprocess.Popen(['bash','/user/pclhc/bin/python/code_inventory.sh'], stdout=subprocess.PIPE, universal_newlines=True)

    parse_and_store(process.stdout)
    group_codes()

    # Print html
    print_common()

    # Write FGC2 table
    print('<H1>FGC2</H1>')
    print_table_to_html(fgc2_codes, fgc2_targets)

    # Write FGC3 table
    print('<H1>FGC3</H1>')
    print_table_to_html(fgc3_codes, fgc3_targets)

    return


if __name__ == "__main__":
    main()

# EOF
