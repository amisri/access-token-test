# Filename: released_fgcd_versions.py
#
# Purpose:  Parse fgcd_class_check_installed.sh output which provides where and what versions of
#           an fgcd class are installed. Then build a html table with the data collected from each class.
#
# Location: Placed in ~pclhc/lib/python/packages/wiki_pages with $make install.
#
# Notes:    Converted from the Perl script released_fgcd_versions.pl
#
# Usage:    Script called by wiki_pages/gitlab-ci.jobs.yml

import subprocess
from collections import defaultdict
import re
import threading
import time
from datetime import datetime

code_map = defaultdict(lambda: defaultdict(int))

NUMBER_OF_CLASSES = 15


def run_script(class_num):
    process = subprocess.Popen(['/user/pclhc/bin/fgcd_class_check_installed.sh', str(class_num)],
                               stdout=subprocess.PIPE,
                               universal_newlines=True)
    parse_and_store(process.stdout, class_num)


def parse_and_store(stream, class_num):

    for line in stream:
        pattern = '^([^\d][\w\-]+)\s+(\d{10})'
        match = re.match(pattern, line, re.I)

        if match:
            target_name = match.groups()[0]
            version = match.groups()[1]

            code_map[class_num][target_name] = int(version)


def print_table():
    print('<p align="right">Table generated', time.ctime(), '</p>')
    print('<div style="overflow-x:auto;">')
    print('<TABLE class="confluenceTable">')
    print('<TR>')

    keys = set()

    i = 1
    while i <= len(code_map.keys()):
        # Union of all the dictionary keys, this is all the targets
        keys |= set(code_map[i].keys())
        i += 1

    targets = list(keys)
    targets.sort()

    # Print table header
    print('<TH class="confluenceTh">Code_Class</TH>')
    for t in targets:
        print('<TH class="confluenceTh">' + t + '</TH>')
    print('</TR>\n')

    i = 1
    # Print table rows
    while i <= len(code_map.keys()):
        # Print Class number with link to git
        print('<TR>')
        print('<TD class="confluenceTd">')
        print('<A HREF="https://gitlab.cern.ch/ccs/fgc/tree/master/sw/fgcd/' + str(i) + '/history">Class ' + str(i) + '</A>')
        print('</TD>')

        latest_version = max(set(code_map[i].values()) | {0})

        # Print versions installed on each target
        for t in targets:
            version = code_map[i][t]
            date = datetime.fromtimestamp(version)
            if version == 0:
                print('<TD class="highlight-grey confluenceTd"></TD>')
            elif version == latest_version:
                print('<TD class="confluenceTd">')
                print('<SPAN style="color: green;">' + str(version) + '</SPAN><BR>')
                print('<SPAN style="color: grey;">' + date.strftime("%Y-%m-%d<br>%H:%M:%S") + '</SPAN>')
                print('</TD>')
            else:
                print('<TD class="confluenceTd">')
                print('<SPAN style="color: red;">' + str(version) + '</SPAN><BR>')
                print('<SPAN style="color: grey;">' + date.strftime("%Y-%m-%d<br>%H:%M:%S") + '</SPAN>')
                print('</TD>')
        print('</TR>\n')

        i += 1

    print('</TABLE>')
    print('</div>')


def main():

    i = 0
    threads = list()
    while i <= NUMBER_OF_CLASSES:
        x = threading.Thread(target=run_script, args=(i,))
        threads.append(x)
        x.start()
        i += 1

    # Wait for threads to finish
    for x in threads:
        x.join()

    print_table()


if __name__ == "__main__":
    main()

# EOF
