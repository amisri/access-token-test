﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="EpcView.vi" Type="VI" URL="../EpcView.vi"/>
		<Item Name="beecrypt.dll" Type="Document" URL="../builds/EpcView_2010/data/beecrypt.dll"/>
		<Item Name="libcurl.dll" Type="Document" URL="../builds/EpcView_2010/data/libcurl.dll"/>
		<Item Name="CMWWrapper.dll" Type="Document" URL="//cern.ch/dfs/Users/o/ozturk/Documents/EpcView_2010/builds/EpcView_2010/data/CMWWrapper.dll"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="SQL_open2.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL_adapter/SQL_open2.vi"/>
				<Item Name="SQL_command2.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL_adapter/SQL_command2.vi"/>
				<Item Name="SQL.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL.vi"/>
				<Item Name="dbUserInfo.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/DB/dbUserInfo.ctl"/>
				<Item Name="java2lvMethod.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/java2lvMethod.ctl"/>
				<Item Name="UserData.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/RADEBase/controls/UserData.ctl"/>
				<Item Name="ReadDataFromResponce.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/ReadDataFromResponce.vi"/>
				<Item Name="FieldCluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/FieldCluster.ctl"/>
				<Item Name="GetData.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/GetData.vi"/>
				<Item Name="serv_url_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/URL/serv_url_3.vi"/>
				<Item Name="tgmVersion.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/tgmVersion.ctl"/>
				<Item Name="AddData.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/AddData.vi"/>
				<Item Name="AddUserData.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/AddUserData.vi"/>
				<Item Name="httpPost_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/httpPost_3.vi"/>
				<Item Name="urlList2StringArray_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/urlList2StringArray_3.vi"/>
				<Item Name="ConnectToServer.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/ConnectToServer.vi"/>
				<Item Name="tcp_conn_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/tcp_conn_3.vi"/>
				<Item Name="parceurl_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/parceurl_3.vi"/>
				<Item Name="chkstrnn_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/chkstrnn_3.vi"/>
				<Item Name="tcpwrite_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/tcpwrite_3.vi"/>
				<Item Name="tcp_read_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/tcp_read_3.vi"/>
				<Item Name="checkJava2LVError_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/checkJava2LVError_3.vi"/>
				<Item Name="chksresp_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/chksresp_3.vi"/>
				<Item Name="html2txt_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/html2txt_3.vi"/>
				<Item Name="str_prep.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/_etc/str_prep.vi"/>
				<Item Name="SQL_close2.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL_adapter/SQL_close2.vi"/>
				<Item Name="rdaError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaError.vi"/>
				<Item Name="rdaGetProperty.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetProperty.vi"/>
				<Item Name="rdaProperty.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaProperty.vi"/>
				<Item Name="rdaGetShort.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetShort.vi"/>
				<Item Name="rdaShort.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaShort.vi"/>
				<Item Name="rdaGetInteger.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetInteger.vi"/>
				<Item Name="rdaInteger.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaInteger.vi"/>
				<Item Name="rdaGetFloat.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetFloat.vi"/>
				<Item Name="rdaFloat.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaFloat.vi"/>
				<Item Name="rdaGetRelease.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetRelease.vi"/>
				<Item Name="rdaPropertyError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaPropertyError.vi"/>
				<Item Name="rdaGetBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetBoolean.vi"/>
				<Item Name="rdaBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaBoolean.vi"/>
				<Item Name="rdaGetLong.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetLong.vi"/>
				<Item Name="rdaLong.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaLong.vi"/>
				<Item Name="rdaGetFloatArray.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetFloatArray.vi"/>
				<Item Name="rdaFloatArray.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaFloatArray.vi"/>
				<Item Name="rdaGetString.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetString.vi"/>
				<Item Name="rdaString.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaString.vi"/>
				<Item Name="rdaSetBooleanReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetBooleanReq.vi"/>
				<Item Name="rdaSetBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetBoolean.vi"/>
				<Item Name="rdaGetStringArray.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetStringArray.vi"/>
				<Item Name="rdaStringArray.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaStringArray.vi"/>
				<Item Name="rdaSetRequest.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetRequest.vi"/>
				<Item Name="rdaSetPropertyError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetPropertyError.vi"/>
				<Item Name="rdaSetLongReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetLongReq.vi"/>
				<Item Name="rdaSetLong.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetLong.vi"/>
				<Item Name="rdaSetExecute.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetExecute.vi"/>
				<Item Name="rdaSetIntegerReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetIntegerReq.vi"/>
				<Item Name="rdaSetInteger.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetInteger.vi"/>
				<Item Name="rdaSetFloatReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetFloatReq.vi"/>
				<Item Name="rdaSetFloat.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetFloat.vi"/>
				<Item Name="rdaSetStringReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetStringReq.vi"/>
				<Item Name="rdaSetString.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetString.vi"/>
				<Item Name="rdaSetShortReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetShortReq.vi"/>
				<Item Name="rdaSetShort.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetShort.vi"/>
				<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/MTA-lib/string/string.llb/String to 1D Array__ogtk.vi"/>
			</Item>
			<Item Name="Epc_menu.rtm" Type="Document" URL="../Menu/Epc_menu.rtm"/>
			<Item Name="linux.vi" Type="VI" URL="../Modules/Labview/linux.vi"/>
			<Item Name="Epc_About.vi" Type="VI" URL="../Epc_About.vi"/>
			<Item Name="Epc_ListPcAll.vi" Type="VI" URL="../Epc_ListPcAll.vi"/>
			<Item Name="Sql_fesa.vi" Type="VI" URL="../Modules/Database/Sql_fesa.vi"/>
			<Item Name="Erase_Identical.vi" Type="VI" URL="../Modules/Labview/Erase_Identical.vi"/>
			<Item Name="Sql_Fesa_Functions.ctl" Type="VI" URL="../Modules/Database/Sql_Fesa_Functions.ctl"/>
			<Item Name="Epc_ListPcType.vi" Type="VI" URL="../Epc_ListPcType.vi"/>
			<Item Name="Epc_ListBeam.vi" Type="VI" URL="../Epc_ListBeam.vi"/>
			<Item Name="Epc_ListPc.vi" Type="VI" URL="../Epc_ListPc.vi"/>
			<Item Name="Selection_Control.ctl" Type="VI" URL="../Controls/Selection_Control.ctl"/>
			<Item Name="Fesa_GetSetting.vi" Type="VI" URL="../Modules/Setting/Fesa_GetSetting.vi"/>
			<Item Name="FESA Error Converter.vi" Type="VI" URL="../Modules/Fesa/FESA Error Converter.vi"/>
			<Item Name="Fesa_Setting_Cluster.ctl" Type="VI" URL="../Modules/Setting/Fesa_Setting_Cluster.ctl"/>
			<Item Name="Fesa_Setting_mode_Enum.ctl" Type="VI" URL="../Modules/Setting/Fesa_Setting_mode_Enum.ctl"/>
			<Item Name="Fesa_GetAcquisition.vi" Type="VI" URL="../Modules/Acquisitions/Fesa_GetAcquisition.vi"/>
			<Item Name="Fesa_Acquisition_Cluster.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_Cluster.ctl"/>
			<Item Name="Fesa_Acquisition_machProtmode_Enum.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_machProtmode_Enum.ctl"/>
			<Item Name="Fesa_Acquisition_current_status_Enum.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_current_status_Enum.ctl"/>
			<Item Name="Fesa_Acquisition_mode_Enum.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_mode_Enum.ctl"/>
			<Item Name="Fesa_GetExpertStatus.vi" Type="VI" URL="../Modules/ExpertStatus/Fesa_GetExpertStatus.vi"/>
			<Item Name="Fesa_ExpertStatus_Cluster.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_Cluster.ctl"/>
			<Item Name="Fesa_ExpertStatus_converterOperatingFrom_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_converterOperatingFrom_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_polaritySwitchStatus_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_polaritySwitchStatus_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_switchesCommandState_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_switchesCommandState_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_currentCommandState_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_currentCommandState_Enum.ctl"/>
			<Item Name="Fesa_GetExpertSetting.vi" Type="VI" URL="../Modules/ExpertSetting/Fesa_GetExpertSetting.vi"/>
			<Item Name="Fesa_ExpertSetting_Cluster.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_Cluster.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterType_Enum.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterType_Enum.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterDuty_Enum.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterDuty_Enum.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterTopology_Enum.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterTopology_Enum.ctl"/>
			<Item Name="Epc_PcStatus.vi" Type="VI" URL="../Epc_PcStatus.vi"/>
			<Item Name="Fesa_ExpertStatus_faults_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_faults_Enum.ctl"/>
			<Item Name="Epc_ListPcStatus.vi" Type="VI" URL="../Epc_ListPcStatus.vi"/>
			<Item Name="Epc_SetSetting.vi" Type="VI" URL="../Epc_SetSetting.vi"/>
			<Item Name="Label Cluster Element.vi" Type="VI" URL="../Modules/Labview/Label Cluster Element.vi"/>
			<Item Name="Check String Number.vi" Type="VI" URL="../Modules/Labview/Check String Number.vi"/>
			<Item Name="Fesa_SetSetting.vi" Type="VI" URL="../Modules/Setting/Fesa_SetSetting.vi"/>
			<Item Name="FESA Set TF.vi" Type="VI" URL="../Modules/Fesa/FESA Set TF.vi"/>
			<Item Name="Epc_PropertiesManager.vi" Type="VI" URL="../Epc_PropertiesManager.vi"/>
			<Item Name="Epc_GetEaExpertSetting.vi" Type="VI" URL="../Epc_GetEaExpertSetting.vi"/>
			<Item Name="help enum to string.vi" Type="VI" URL="../Modules/Labview/help enum to string.vi"/>
			<Item Name="Fesa_GetEaExpertSetting.vi" Type="VI" URL="../Modules/EaExpertSetting/Fesa_GetEaExpertSetting.vi"/>
			<Item Name="Fesa_EaExpertSetting_Cluster.ctl" Type="VI" URL="../Modules/EaExpertSetting/Fesa_EaExpertSetting_Cluster.ctl"/>
			<Item Name="Fesa_EaExpertSetting_machProtMode_Enum.ctl" Type="VI" URL="../Modules/EaExpertSetting/Fesa_EaExpertSetting_machProtMode_Enum.ctl"/>
			<Item Name="Fesa_EaExpertSetting_magnetExcitation_Enum.ctl" Type="VI" URL="../Modules/EaExpertSetting/Fesa_EaExpertSetting_magnetExcitation_Enum.ctl"/>
			<Item Name="Epc_GetExpertSetting.vi" Type="VI" URL="../Epc_GetExpertSetting.vi"/>
			<Item Name="Epc_GetSetting.vi" Type="VI" URL="../Epc_GetSetting.vi"/>
			<Item Name="Epc_GetExpertStatus.vi" Type="VI" URL="../Epc_GetExpertStatus.vi"/>
			<Item Name="String Array Numeric to string.vi" Type="VI" URL="../Modules/Labview/String Array Numeric to string.vi"/>
			<Item Name="Numeric Array to string.vi" Type="VI" URL="../Modules/Labview/Numeric Array to string.vi"/>
			<Item Name="Epc_GetStatus.vi" Type="VI" URL="../Epc_GetStatus.vi"/>
			<Item Name="Fesa_GetStatus.vi" Type="VI" URL="../Modules/Status/Fesa_GetStatus.vi"/>
			<Item Name="Fesa_Status_Cluster.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_Cluster.ctl"/>
			<Item Name="Fesa_Status_control_num.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_control_num.ctl"/>
			<Item Name="Fesa_Status_mode_Enum.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_mode_Enum.ctl"/>
			<Item Name="Fesa_Status_status_Enum.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_status_Enum.ctl"/>
			<Item Name="String Array to string.vi" Type="VI" URL="../Modules/Labview/String Array to string.vi"/>
			<Item Name="Fesa_Status_detailedStatus_Enum.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_detailedStatus_Enum.ctl"/>
			<Item Name="Epc_GetAcquisition.vi" Type="VI" URL="../Epc_GetAcquisition.vi"/>
			<Item Name="Epc_GetDatabase.vi" Type="VI" URL="../Epc_GetDatabase.vi"/>
			<Item Name="Fesa_GetDatabase.vi" Type="VI" URL="../Modules/Database/Fesa_GetDatabase.vi"/>
			<Item Name="FESA Get SGL.vi" Type="VI" URL="../Modules/Fesa/FESA Get SGL.vi"/>
			<Item Name="FESA Get abc.vi" Type="VI" URL="../Modules/Fesa/FESA Get abc.vi"/>
			<Item Name="FESA Get I16.vi" Type="VI" URL="../Modules/Fesa/FESA Get I16.vi"/>
			<Item Name="FESA Get I32.vi" Type="VI" URL="../Modules/Fesa/FESA Get I32.vi"/>
			<Item Name="FESA Get TF.vi" Type="VI" URL="../Modules/Fesa/FESA Get TF.vi"/>
			<Item Name="Fesa_Database_Cluster.ctl" Type="VI" URL="../Modules/Database/Fesa_Database_Cluster.ctl"/>
			<Item Name="Epc_SetExpertSetting.vi" Type="VI" URL="../Epc_SetExpertSetting.vi"/>
			<Item Name="Epc_SetEaExpertSetting.vi" Type="VI" URL="../Epc_SetEaExpertSetting.vi"/>
			<Item Name="Fesa_SetEaExpertSetting.vi" Type="VI" URL="../Modules/EaExpertSetting/Fesa_SetEaExpertSetting.vi"/>
			<Item Name="Epc_PropertiesManager_Functions.ctl" Type="VI" URL="../Controls/Epc_PropertiesManager_Functions.ctl"/>
			<Item Name="Epc_CompareDatabFesa.vi" Type="VI" URL="../Epc_CompareDatabFesa.vi"/>
			<Item Name="Epc_DatabTableToDatabCluster.vi" Type="VI" URL="../Epc_DatabTableToDatabCluster.vi"/>
			<Item Name="Epc_Functions.ctl" Type="VI" URL="../Controls/Epc_Functions.ctl"/>
			<Item Name="Faults_num2_faults_string.vi" Type="VI" URL="../Tools/Faults_num2_faults_string.vi"/>
			<Item Name="FESA Set I64.vi" Type="VI" URL="../Modules/Fesa/FESA Set I64.vi"/>
			<Item Name="FESA Set I32.vi" Type="VI" URL="../Modules/Fesa/FESA Set I32.vi"/>
			<Item Name="FESA Set SGL.vi" Type="VI" URL="../Modules/Fesa/FESA Set SGL.vi"/>
			<Item Name="Fesa_SetExpertSetting.vi" Type="VI" URL="../Modules/ExpertSetting/Fesa_SetExpertSetting.vi"/>
			<Item Name="FESA Set abc.vi" Type="VI" URL="../Modules/Fesa/FESA Set abc.vi"/>
			<Item Name="FESA Set I16.vi" Type="VI" URL="../Modules/Fesa/FESA Set I16.vi"/>
			<Item Name="CMWWrapper.dll" Type="Document" URL="/U/projects/EpcView_2014/builds/EpcView_2010/data/CMWWrapper.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="EpcView Windows" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{43DA5551-0AAA-4CD9-AD53-AC56E44C63CF}</Property>
				<Property Name="App_INI_GUID" Type="Str">{8BDB0D45-64BF-4B8E-A920-2DFFFC2530F4}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{D90AE9DB-B684-458F-8719-8F8734ADE81A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">EpcView Windows</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Windows</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{D28021B5-C24D-4AED-9C74-F43209C9654E}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">EpcView.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Windows/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Windows/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{72E1C045-C96A-4269-A8E0-436FD02E2B38}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/EpcView.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">EpcView Windows</Property>
				<Property Name="TgtF_internalName" Type="Str">EpcView Windows</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2009 </Property>
				<Property Name="TgtF_productName" Type="Str">EpcView Windows</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{5D9504FC-C944-4227-B80E-7930B770E145}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">EpcView.exe</Property>
			</Item>
			<Item Name="EpcView Linux" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{32D0CFBA-ED13-4D15-817D-B8F23439E5F1}</Property>
				<Property Name="App_INI_GUID" Type="Str">{FC609813-332E-4FE6-80D5-B329467E9EF1}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8E4BA3F3-C42E-4AAF-85CC-16F9D4CD0767}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">EpcView Linux</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Linux</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{3A9FA1A7-2E68-462E-A46C-261202EFBEAD}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">epcview.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Linux/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Linux/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{72E1C045-C96A-4269-A8E0-436FD02E2B38}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/EpcView.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">EpcView Linux</Property>
				<Property Name="TgtF_internalName" Type="Str">EpcView Linux</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2009 </Property>
				<Property Name="TgtF_productName" Type="Str">EpcView Linux</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{DF1660B2-2687-49B9-A942-347876AA0A5B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">epcview.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
