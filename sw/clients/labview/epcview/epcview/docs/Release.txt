EpcView

Version 1.01 (28/07/06): 
	- First release

Version 1.02 (02/08/06): 
	- update pc status after a command with a short delay
	- order list device_name for BAxx sql call

Version 1.03 (04/08/06): 
	- Add STB/SET CURRENT for list
	- Add STB for single power converter
	- Add currentbias and Switch Postion for single converter status

Version 1.04 (11/08/06)
	- Version provisoire -> refonte du programme

Version 2.00 (11/08/06)
	- Rebuild program with expertstatus + cycle selection.

Version 2.01 (11/08/06)
	- correct min/max current

Version 2.02 (05/10/06)
	- add PropertiesManager application

Version 2.03 (11/08/06)
	- add Database comp.

Version 3.00 (11/08/06)
	- move to 8.2

Version 3.01 (03/11/06)
	- Scrollbar and empty cells in table are unselectable
	- current is now EQUAL under/upper to setting current � tolerance

Version 3.02 (23/02/07)
	- ExpertStatus : quantizedMagnetCurrent (float) -> sentToDac (long)
			 quantizedMagnetCurrent_difference deleted 
	- FESA 2.8 -> 2.9

version 3.03 (15/03/07)
	- CMW Wrapper 302 -> 3.03

version 3.04 (16/04/2007)
	- oups, missing cabling pcname for stb.

version 3.05 (02/10/2007)
	- db : FESA_DEVICE_FIELD switch to FESA_DEVICE 

version 3.06 (11/01/2008)
	- CMWWrapper : update for being compatible with the version 1.07

version 3.08 (11/01/2008)
	- delete some tag in expertstatus property (Daniel)

version 4.0 (16/01/2009)
	- move to lv8.6 and CMWWrapper1.14.win

version 4.1 (11/05/2009)
	- used sql_rade for the database