﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Scan_current.vi" Type="VI" URL="../Scan_current.vi"/>
		<Item Name="scancurrent_new.vi" Type="VI" URL="../scancurrent_new.vi"/>
		<Item Name="Fesa_GetExpertSetting.vi" Type="VI" URL="../../Modules/ExpertSetting/Fesa_GetExpertSetting.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="SQL_open2.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL_adapter/SQL_open2.vi"/>
				<Item Name="SQL_command2.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL_adapter/SQL_command2.vi"/>
				<Item Name="SQL.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL.vi"/>
				<Item Name="dbUserInfo.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/DB/dbUserInfo.ctl"/>
				<Item Name="java2lvMethod.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/java2lvMethod.ctl"/>
				<Item Name="UserData.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/RADEBase/controls/UserData.ctl"/>
				<Item Name="ReadDataFromResponce.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/ReadDataFromResponce.vi"/>
				<Item Name="FieldCluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/FieldCluster.ctl"/>
				<Item Name="GetData.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/GetData.vi"/>
				<Item Name="serv_url_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/URL/serv_url_3.vi"/>
				<Item Name="ServerVersionJAPC_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/ServerVersionJAPC_3.ctl"/>
				<Item Name="ServerVersionInCa_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/ServerVersionInCa_3.ctl"/>
				<Item Name="ServerVersionDIP_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/ServerVersionDIP_3.ctl"/>
				<Item Name="ServerVersionDB_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/ServerVersionDB_3.ctl"/>
				<Item Name="ServerPort_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/ServerPort_3.ctl"/>
				<Item Name="serverMachineSelector_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/serverMachineSelector_3.ctl"/>
				<Item Name="ServerName.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/ServerName.ctl"/>
				<Item Name="tgmVersion.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/tgmVersion.ctl"/>
				<Item Name="allURLData.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/allURLData.ctl"/>
				<Item Name="servletNames.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/servletNames.ctl"/>
				<Item Name="buildUrl_3_1.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/URL/buildUrl_3_1.vi"/>
				<Item Name="serverVersionSelectior_3.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/controls/URL/serverVersionSelectior_3.ctl"/>
				<Item Name="AddData.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/AddData.vi"/>
				<Item Name="AddUserData.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/Data coding/AddUserData.vi"/>
				<Item Name="httpPost_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/httpPost_3.vi"/>
				<Item Name="urlList2StringArray_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/urlList2StringArray_3.vi"/>
				<Item Name="ConnectToServer.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/ConnectToServer.vi"/>
				<Item Name="tcp_conn_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/tcp_conn_3.vi"/>
				<Item Name="parceurl_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/parceurl_3.vi"/>
				<Item Name="chkstrnn_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/chkstrnn_3.vi"/>
				<Item Name="tcpwrite_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/tcpwrite_3.vi"/>
				<Item Name="tcp_read_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/tcp_read_3.vi"/>
				<Item Name="checkJava2LVError_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/checkJava2LVError_3.vi"/>
				<Item Name="chksresp_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/chksresp_3.vi"/>
				<Item Name="html2txt_3.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RIO/JAPC/vis/_etc/HTTPPOST/html2txt_3.vi"/>
				<Item Name="str_prep.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/_etc/str_prep.vi"/>
				<Item Name="SQL_close2.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/SQL/SQL_adapter/SQL_close2.vi"/>
				<Item Name="rdaGetProperty.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetProperty.vi"/>
				<Item Name="rdaProperty.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaProperty.vi"/>
				<Item Name="rdaGetShort.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetShort.vi"/>
				<Item Name="rdaShort.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaShort.vi"/>
				<Item Name="rdaGetString.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetString.vi"/>
				<Item Name="rdaString.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaString.vi"/>
				<Item Name="rdaGetInteger.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetInteger.vi"/>
				<Item Name="rdaInteger.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaInteger.vi"/>
				<Item Name="rdaGetFloat.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetFloat.vi"/>
				<Item Name="rdaFloat.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaFloat.vi"/>
				<Item Name="rdaGetLong.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetLong.vi"/>
				<Item Name="rdaLong.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaLong.vi"/>
				<Item Name="rdaGetRelease.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetRelease.vi"/>
				<Item Name="rdaPropertyError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaPropertyError.vi"/>
				<Item Name="rdaError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaError.vi"/>
				<Item Name="rdaGetBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetBoolean.vi"/>
				<Item Name="rdaBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaBoolean.vi"/>
				<Item Name="rdaGetFloatArray.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaGetFloatArray.vi"/>
				<Item Name="rdaFloatArray.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaFloatArray.vi"/>
				<Item Name="rdaSetRequest.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetRequest.vi"/>
				<Item Name="rdaSetPropertyError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetPropertyError.vi"/>
				<Item Name="rdaSetLongReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetLongReq.vi"/>
				<Item Name="rdaSetLong.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetLong.vi"/>
				<Item Name="rdaSetExecute.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetExecute.vi"/>
				<Item Name="rdaSetBooleanReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetBooleanReq.vi"/>
				<Item Name="rdaSetBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetBoolean.vi"/>
				<Item Name="rdaSetIntegerReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetIntegerReq.vi"/>
				<Item Name="rdaSetInteger.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetInteger.vi"/>
				<Item Name="rdaSetFloatReq.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetFloatReq.vi"/>
				<Item Name="rdaSetFloat.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/FESA/rdaSetFloat.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="CMWWrapper.dll" Type="Document" URL="../../builds/Windows/data/CMWWrapper.dll"/>
			<Item Name="Epc_SetSetting.vi" Type="VI" URL="../../Epc_SetSetting.vi"/>
			<Item Name="Label Cluster Element.vi" Type="VI" URL="../../Modules/Labview/Label Cluster Element.vi"/>
			<Item Name="Fesa_GetSetting.vi" Type="VI" URL="../../Modules/Setting/Fesa_GetSetting.vi"/>
			<Item Name="FESA Error Converter.vi" Type="VI" URL="../../Modules/Fesa/FESA Error Converter.vi"/>
			<Item Name="Fesa_Setting_Cluster.ctl" Type="VI" URL="../../Modules/Setting/Fesa_Setting_Cluster.ctl"/>
			<Item Name="Fesa_Setting_mode_Enum.ctl" Type="VI" URL="../../Modules/Setting/Fesa_Setting_mode_Enum.ctl"/>
			<Item Name="Check String Number.vi" Type="VI" URL="../../Modules/Labview/Check String Number.vi"/>
			<Item Name="Fesa_SetSetting.vi" Type="VI" URL="../../Modules/Setting/Fesa_SetSetting.vi"/>
			<Item Name="FESA Set SGL.vi" Type="VI" URL="../../Modules/Fesa/FESA Set SGL.vi"/>
			<Item Name="FESA Set TF.vi" Type="VI" URL="../../Modules/Fesa/FESA Set TF.vi"/>
			<Item Name="FESA Set I32.vi" Type="VI" URL="../../Modules/Fesa/FESA Set I32.vi"/>
			<Item Name="FESA Set I64.vi" Type="VI" URL="../../Modules/Fesa/FESA Set I64.vi"/>
			<Item Name="Fesa_ExpertSetting_Cluster.ctl" Type="VI" URL="../../Modules/ExpertSetting/Fesa_ExpertSetting_Cluster.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterType_Enum.ctl" Type="VI" URL="../../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterType_Enum.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterDuty_Enum.ctl" Type="VI" URL="../../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterDuty_Enum.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterTopology_Enum.ctl" Type="VI" URL="../../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterTopology_Enum.ctl"/>
			<Item Name="Fesa_GetExpertStatus.vi" Type="VI" URL="../../Modules/ExpertStatus/Fesa_GetExpertStatus.vi"/>
			<Item Name="Fesa_ExpertStatus_Cluster.ctl" Type="VI" URL="../../Modules/ExpertStatus/Fesa_ExpertStatus_Cluster.ctl"/>
			<Item Name="Fesa_ExpertStatus_converterOperatingFrom_Enum.ctl" Type="VI" URL="../../Modules/ExpertStatus/Fesa_ExpertStatus_converterOperatingFrom_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_polaritySwitchStatus_Enum.ctl" Type="VI" URL="../../Modules/ExpertStatus/Fesa_ExpertStatus_polaritySwitchStatus_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_switchesCommandState_Enum.ctl" Type="VI" URL="../../Modules/ExpertStatus/Fesa_ExpertStatus_switchesCommandState_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_currentCommandState_Enum.ctl" Type="VI" URL="../../Modules/ExpertStatus/Fesa_ExpertStatus_currentCommandState_Enum.ctl"/>
			<Item Name="Fesa_GetAcquisition.vi" Type="VI" URL="../../Modules/Acquisitions/Fesa_GetAcquisition.vi"/>
			<Item Name="Fesa_Acquisition_Cluster.ctl" Type="VI" URL="../../Modules/Acquisitions/Fesa_Acquisition_Cluster.ctl"/>
			<Item Name="Fesa_Acquisition_machProtmode_Enum.ctl" Type="VI" URL="../../Modules/Acquisitions/Fesa_Acquisition_machProtmode_Enum.ctl"/>
			<Item Name="Fesa_Acquisition_current_status_Enum.ctl" Type="VI" URL="../../Modules/Acquisitions/Fesa_Acquisition_current_status_Enum.ctl"/>
			<Item Name="Fesa_Acquisition_mode_Enum.ctl" Type="VI" URL="../../Modules/Acquisitions/Fesa_Acquisition_mode_Enum.ctl"/>
			<Item Name="Epc_ListPcAll.vi" Type="VI" URL="../../Epc_ListPcAll.vi"/>
			<Item Name="Sql_fesa.vi" Type="VI" URL="../../Modules/Database/Sql_fesa.vi"/>
			<Item Name="Sql_Fesa_Functions.ctl" Type="VI" URL="../../Modules/Database/Sql_Fesa_Functions.ctl"/>
			<Item Name="Erase_Identical.vi" Type="VI" URL="../../Modules/Labview/Erase_Identical.vi"/>
			<Item Name="Fesa_GetGlobalInfo.vi" Type="VI" URL="../../Modules/Global/Fesa_GetGlobalInfo.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Scan_current" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{C4411778-FEC5-4CFD-BA72-3FF6011C4879}</Property>
				<Property Name="App_INI_GUID" Type="Str">{EABD8C2C-8B63-4B2D-9B39-160CECB5927D}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Scan_current</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/Z/projects/JunctionCrate/sw/Tools/Clients/App/Tools</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_targetDestDir" Type="Path"></Property>
				<Property Name="Destination[0].destName" Type="Str">Scan_current.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/Z/projects/JunctionCrate/sw/Tools/Clients/App/Tools/Scan_current.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/Z/projects/JunctionCrate/sw/Tools/Clients/App/Tools/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{1713CC44-8DC4-46A3-9FBE-154CFB9E72CE}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Scan_current.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">CERN</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Scan_current</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">Scan_current</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2009 CERN</Property>
				<Property Name="TgtF_productName" Type="Str">Scan_current</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{CCA88F5F-F3FF-4D36-A138-970D558E6892}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Scan_current.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
