﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Switch.vi" Type="VI" URL="../Switch.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Switch" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{485E3B06-91A5-4CFA-86E2-71F05543D268}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A52FF6C5-BFD0-44C5-A76E-305EA5F1DA71}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Switch</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/Z/projects/JunctionCrate/sw/Tools/Clients/App/Tools</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_targetDestDir" Type="Path"></Property>
				<Property Name="Destination[0].destName" Type="Str">Switch.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/Z/projects/JunctionCrate/sw/Tools/Clients/App/Tools/Switch.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/Z/projects/JunctionCrate/sw/Tools/Clients/App/Tools/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{2084A52E-4F4B-4A7A-8496-0F50EA795C53}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Switch.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">CERN</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Switch</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">Switch</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2009 CERN</Property>
				<Property Name="TgtF_productName" Type="Str">Switch</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{EC9B24BA-D55E-499D-BA7D-0362BF058D0B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Switch.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
