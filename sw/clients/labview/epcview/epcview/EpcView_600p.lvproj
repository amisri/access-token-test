﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="8608001">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="EpcView_600p.vi" Type="VI" URL="../EpcView_600p.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Merge Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Merge Errors.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="SQL_open2.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/SQL_adapter/SQL_open2.vi"/>
				<Item Name="SQL_command2.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/SQL_adapter/SQL_command2.vi"/>
				<Item Name="SQL.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/SQL.vi"/>
				<Item Name="str_prep.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/str_prep.vi"/>
				<Item Name="serv_url.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/serv_url.vi"/>
				<Item Name="httppost.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/httppost.vi"/>
				<Item Name="tcp_conn.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/tcp_conn.vi"/>
				<Item Name="parceurl.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/parceurl.vi"/>
				<Item Name="chkstrnn.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/chkstrnn.vi"/>
				<Item Name="err_comb.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/err_comb.vi"/>
				<Item Name="tcpwrite.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/tcpwrite.vi"/>
				<Item Name="tcp_read.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/tcp_read.vi"/>
				<Item Name="chksresp.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/chksresp.vi"/>
				<Item Name="html2txt.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/_etc/html2txt.vi"/>
				<Item Name="SQL_close2.vi" Type="VI" URL="/&lt;userlib&gt;/SQL/SQL_adapter/SQL_close2.vi"/>
				<Item Name="rdaGetProperty.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetProperty.vi"/>
				<Item Name="rdaProperty.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaProperty.vi"/>
				<Item Name="CMWWrapper.dll" Type="Document" URL="/&lt;userlib&gt;/FESA/binary/CMWWrapper.dll"/>
				<Item Name="rdaGetFloat.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetFloat.vi"/>
				<Item Name="rdaFloat.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaFloat.vi"/>
				<Item Name="rdaGetBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetBoolean.vi"/>
				<Item Name="rdaBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaBoolean.vi"/>
				<Item Name="rdaGetInteger.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetInteger.vi"/>
				<Item Name="rdaInteger.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaInteger.vi"/>
				<Item Name="rdaGetLong.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetLong.vi"/>
				<Item Name="rdaLong.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaLong.vi"/>
				<Item Name="rdaGetRelease.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetRelease.vi"/>
				<Item Name="rdaPropertyError.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaPropertyError.vi"/>
				<Item Name="rdaError.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaError.vi"/>
				<Item Name="rdaGetString.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetString.vi"/>
				<Item Name="rdaString.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaString.vi"/>
				<Item Name="rdaGetShort.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetShort.vi"/>
				<Item Name="rdaShort.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaShort.vi"/>
				<Item Name="rdaGetFloatArray.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetFloatArray.vi"/>
				<Item Name="rdaFloatArray.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaFloatArray.vi"/>
				<Item Name="rdaSetRequest.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetRequest.vi"/>
				<Item Name="rdaSetPropertyError.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetPropertyError.vi"/>
				<Item Name="rdaSetFloatReq.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetFloatReq.vi"/>
				<Item Name="rdaSetFloat.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetFloat.vi"/>
				<Item Name="rdaSetExecute.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetExecute.vi"/>
				<Item Name="rdaSetBooleanReq.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetBooleanReq.vi"/>
				<Item Name="rdaSetBoolean.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetBoolean.vi"/>
				<Item Name="rdaSetIntegerReq.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetIntegerReq.vi"/>
				<Item Name="rdaSetInteger.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetInteger.vi"/>
				<Item Name="rdaSetLongReq.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetLongReq.vi"/>
				<Item Name="rdaSetLong.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetLong.vi"/>
				<Item Name="rdaGetStringArray.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaGetStringArray.vi"/>
				<Item Name="rdaStringArray.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaStringArray.vi"/>
				<Item Name="rdaSetShortReq.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetShortReq.vi"/>
				<Item Name="rdaSetShort.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetShort.vi"/>
				<Item Name="rdaSetStringReq.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetStringReq.vi"/>
				<Item Name="rdaSetString.vi" Type="VI" URL="/&lt;userlib&gt;/FESA/rdaSetString.vi"/>
			</Item>
			<Item Name="Epc_menu.rtm" Type="Document" URL="../Menu/Epc_menu.rtm"/>
			<Item Name="linux.vi" Type="VI" URL="../Modules/Labview/linux.vi"/>
			<Item Name="Epc_About.vi" Type="VI" URL="../Epc_About.vi"/>
			<Item Name="Epc_ListPcAll.vi" Type="VI" URL="../Epc_ListPcAll.vi"/>
			<Item Name="Sql_fesa.vi" Type="VI" URL="../Modules/Database/Sql_fesa.vi"/>
			<Item Name="Erase_Identical.vi" Type="VI" URL="../Modules/Labview/Erase_Identical.vi"/>
			<Item Name="Sql_Fesa_Functions.ctl" Type="VI" URL="../Modules/Database/Sql_Fesa_Functions.ctl"/>
			<Item Name="Epc_ListPcType.vi" Type="VI" URL="../Epc_ListPcType.vi"/>
			<Item Name="Epc_ListBeam.vi" Type="VI" URL="../Epc_ListBeam.vi"/>
			<Item Name="Epc_ListPc.vi" Type="VI" URL="../Epc_ListPc.vi"/>
			<Item Name="Selection_Control.ctl" Type="VI" URL="../Controls/Selection_Control.ctl"/>
			<Item Name="Fesa_GetSetting.vi" Type="VI" URL="../Modules/Setting/Fesa_GetSetting.vi"/>
			<Item Name="FESA Error Converter.vi" Type="VI" URL="../Modules/Fesa/FESA Error Converter.vi"/>
			<Item Name="Fesa_Setting_Cluster.ctl" Type="VI" URL="../Modules/Setting/Fesa_Setting_Cluster.ctl"/>
			<Item Name="Fesa_Setting_mode_Enum.ctl" Type="VI" URL="../Modules/Setting/Fesa_Setting_mode_Enum.ctl"/>
			<Item Name="Fesa_GetAcquisition.vi" Type="VI" URL="../Modules/Acquisitions/Fesa_GetAcquisition.vi"/>
			<Item Name="Fesa_Acquisition_Cluster.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_Cluster.ctl"/>
			<Item Name="Fesa_Acquisition_machProtmode_Enum.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_machProtmode_Enum.ctl"/>
			<Item Name="Fesa_Acquisition_current_status_Enum.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_current_status_Enum.ctl"/>
			<Item Name="Fesa_Acquisition_mode_Enum.ctl" Type="VI" URL="../Modules/Acquisitions/Fesa_Acquisition_mode_Enum.ctl"/>
			<Item Name="Fesa_GetExpertStatus.vi" Type="VI" URL="../Modules/ExpertStatus/Fesa_GetExpertStatus.vi"/>
			<Item Name="Fesa_ExpertStatus_Cluster.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_Cluster.ctl"/>
			<Item Name="Fesa_ExpertStatus_converterOperatingFrom_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_converterOperatingFrom_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_polaritySwitchStatus_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_polaritySwitchStatus_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_switchesCommandState_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_switchesCommandState_Enum.ctl"/>
			<Item Name="Fesa_ExpertStatus_currentCommandState_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_currentCommandState_Enum.ctl"/>
			<Item Name="Fesa_GetExpertSetting.vi" Type="VI" URL="../Modules/ExpertSetting/Fesa_GetExpertSetting.vi"/>
			<Item Name="Fesa_ExpertSetting_Cluster.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_Cluster.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterType_Enum.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterType_Enum.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterDuty_Enum.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterDuty_Enum.ctl"/>
			<Item Name="Fesa_ExpertSetting_ConverterTopology_Enum.ctl" Type="VI" URL="../Modules/ExpertSetting/Fesa_ExpertSetting_ConverterTopology_Enum.ctl"/>
			<Item Name="Epc_PcStatus.vi" Type="VI" URL="../Epc_PcStatus.vi"/>
			<Item Name="Fesa_ExpertStatus_faults_Enum.ctl" Type="VI" URL="../Modules/ExpertStatus/Fesa_ExpertStatus_faults_Enum.ctl"/>
			<Item Name="Epc_ListPcStatus.vi" Type="VI" URL="../Epc_ListPcStatus.vi"/>
			<Item Name="Epc_SetSetting.vi" Type="VI" URL="../Epc_SetSetting.vi"/>
			<Item Name="Label Cluster Element.vi" Type="VI" URL="../Modules/Labview/Label Cluster Element.vi"/>
			<Item Name="Check String Number.vi" Type="VI" URL="../Modules/Labview/Check String Number.vi"/>
			<Item Name="Fesa_SetSetting.vi" Type="VI" URL="../Modules/Setting/Fesa_SetSetting.vi"/>
			<Item Name="FESA Set SGL.vi" Type="VI" URL="../Modules/Fesa/FESA Set SGL.vi"/>
			<Item Name="FESA Set TF.vi" Type="VI" URL="../Modules/Fesa/FESA Set TF.vi"/>
			<Item Name="FESA Set I32.vi" Type="VI" URL="../Modules/Fesa/FESA Set I32.vi"/>
			<Item Name="FESA Set I64.vi" Type="VI" URL="../Modules/Fesa/FESA Set I64.vi"/>
			<Item Name="Epc_PropertiesManager.vi" Type="VI" URL="../Epc_PropertiesManager.vi"/>
			<Item Name="Epc_GetEaExpertSetting.vi" Type="VI" URL="../Epc_GetEaExpertSetting.vi"/>
			<Item Name="help enum to string.vi" Type="VI" URL="../Modules/Labview/help enum to string.vi"/>
			<Item Name="Fesa_GetEaExpertSetting.vi" Type="VI" URL="../Modules/EaExpertSetting/Fesa_GetEaExpertSetting.vi"/>
			<Item Name="Fesa_EaExpertSetting_Cluster.ctl" Type="VI" URL="../Modules/EaExpertSetting/Fesa_EaExpertSetting_Cluster.ctl"/>
			<Item Name="Fesa_EaExpertSetting_machProtMode_Enum.ctl" Type="VI" URL="../Modules/EaExpertSetting/Fesa_EaExpertSetting_machProtMode_Enum.ctl"/>
			<Item Name="Fesa_EaExpertSetting_magnetExcitation_Enum.ctl" Type="VI" URL="../Modules/EaExpertSetting/Fesa_EaExpertSetting_magnetExcitation_Enum.ctl"/>
			<Item Name="Epc_GetExpertSetting.vi" Type="VI" URL="../Epc_GetExpertSetting.vi"/>
			<Item Name="Epc_GetSetting.vi" Type="VI" URL="../Epc_GetSetting.vi"/>
			<Item Name="Epc_GetExpertStatus.vi" Type="VI" URL="../Epc_GetExpertStatus.vi"/>
			<Item Name="String Array Numeric to string.vi" Type="VI" URL="../Modules/Labview/String Array Numeric to string.vi"/>
			<Item Name="Numeric Array to string.vi" Type="VI" URL="../Modules/Labview/Numeric Array to string.vi"/>
			<Item Name="Epc_GetStatus.vi" Type="VI" URL="../Epc_GetStatus.vi"/>
			<Item Name="Fesa_GetStatus.vi" Type="VI" URL="../Modules/Status/Fesa_GetStatus.vi"/>
			<Item Name="Fesa_Status_Cluster.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_Cluster.ctl"/>
			<Item Name="Fesa_Status_control_num.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_control_num.ctl"/>
			<Item Name="Fesa_Status_mode_Enum.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_mode_Enum.ctl"/>
			<Item Name="Fesa_Status_status_Enum.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_status_Enum.ctl"/>
			<Item Name="String Array to string.vi" Type="VI" URL="../Modules/Labview/String Array to string.vi"/>
			<Item Name="Fesa_Status_detailedStatus_Enum.ctl" Type="VI" URL="../Modules/Status/Fesa_Status_detailedStatus_Enum.ctl"/>
			<Item Name="Epc_GetAcquisition.vi" Type="VI" URL="../Epc_GetAcquisition.vi"/>
			<Item Name="Epc_GetDatabase.vi" Type="VI" URL="../Epc_GetDatabase.vi"/>
			<Item Name="Fesa_GetDatabase.vi" Type="VI" URL="../Modules/Database/Fesa_GetDatabase.vi"/>
			<Item Name="FESA Get SGL.vi" Type="VI" URL="../Modules/Fesa/FESA Get SGL.vi"/>
			<Item Name="FESA Get abc.vi" Type="VI" URL="../Modules/Fesa/FESA Get abc.vi"/>
			<Item Name="FESA Get I16.vi" Type="VI" URL="../Modules/Fesa/FESA Get I16.vi"/>
			<Item Name="FESA Get I32.vi" Type="VI" URL="../Modules/Fesa/FESA Get I32.vi"/>
			<Item Name="FESA Get TF.vi" Type="VI" URL="../Modules/Fesa/FESA Get TF.vi"/>
			<Item Name="Fesa_Database_Cluster.ctl" Type="VI" URL="../Modules/Database/Fesa_Database_Cluster.ctl"/>
			<Item Name="Epc_SetExpertSetting.vi" Type="VI" URL="../Epc_SetExpertSetting.vi"/>
			<Item Name="Fesa_SetExpertSetting.vi" Type="VI" URL="../Modules/ExpertSetting/Fesa_SetExpertSetting.vi"/>
			<Item Name="FESA Set I16.vi" Type="VI" URL="../Modules/Fesa/FESA Set I16.vi"/>
			<Item Name="FESA Set abc.vi" Type="VI" URL="../Modules/Fesa/FESA Set abc.vi"/>
			<Item Name="Epc_SetEaExpertSetting.vi" Type="VI" URL="../Epc_SetEaExpertSetting.vi"/>
			<Item Name="Fesa_SetEaExpertSetting.vi" Type="VI" URL="../Modules/EaExpertSetting/Fesa_SetEaExpertSetting.vi"/>
			<Item Name="Epc_PropertiesManager_Functions.ctl" Type="VI" URL="../Controls/Epc_PropertiesManager_Functions.ctl"/>
			<Item Name="Epc_CompareDatabFesa.vi" Type="VI" URL="../Epc_CompareDatabFesa.vi"/>
			<Item Name="Epc_DatabTableToDatabCluster.vi" Type="VI" URL="../Epc_DatabTableToDatabCluster.vi"/>
			<Item Name="Epc_Functions.ctl" Type="VI" URL="../Controls/Epc_Functions.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="EpcView_600p Windows" Type="EXE">
				<Property Name="App_applicationGUID" Type="Str">{5D9504FC-C944-4227-B80E-7930B770E145}</Property>
				<Property Name="App_applicationName" Type="Str">EpcView_600p.exe</Property>
				<Property Name="App_fileDescription" Type="Str">EpcView_600p Windows</Property>
				<Property Name="App_fileVersion.major" Type="Int">1</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{43DA5551-0AAA-4CD9-AD53-AC56E44C63CF}</Property>
				<Property Name="App_INI_GUID" Type="Str">{8BDB0D45-64BF-4B8E-A920-2DFFFC2530F4}</Property>
				<Property Name="App_internalName" Type="Str">EpcView_600p Windows</Property>
				<Property Name="App_legalCopyright" Type="Str">Copyright © 2009 </Property>
				<Property Name="App_productName" Type="Str">EpcView_600p Windows</Property>
				<Property Name="Bld_buildSpecName" Type="Str">EpcView_600p Windows</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">EpcView_600p.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../NI_AB_PROJECTNAME/builds/Windows/internal.llb</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../NI_AB_PROJECTNAME/builds/Windows/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{BC885278-FEB9-42C0-9B0C-3077DB5B1A29}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/EpcView_600p.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
			</Item>
		</Item>
	</Item>
</Project>
