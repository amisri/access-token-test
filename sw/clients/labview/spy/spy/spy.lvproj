﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="lib" Type="Folder">
			<Item Name="RBAC" Type="Folder">
				<Item Name="AnalyzeError.vi" Type="VI" URL="../lib/RBAC/AnalyzeError.vi"/>
				<Item Name="AppTimeout.vi" Type="VI" URL="../lib/RBAC/AppTimeout.vi"/>
				<Item Name="checkForOS.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/checkForOS.vi"/>
				<Item Name="ClearALLTokens.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/ClearALLTokens.vi"/>
				<Item Name="CreateToken.vi" Type="VI" URL="../lib/RBAC/CreateToken.vi"/>
				<Item Name="DestroyToken.vi" Type="VI" URL="../lib/RBAC/DestroyToken.vi"/>
				<Item Name="DisplayStringPrompt.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/DisplayStringPrompt.vi"/>
				<Item Name="displayToken.vi" Type="VI" URL="../lib/RBAC/displayToken.vi"/>
				<Item Name="GetAllTokenFields.vi" Type="VI" URL="../lib/RBAC/GetAllTokenFields.vi"/>
				<Item Name="GetAppName.vi" Type="VI" URL="../lib/RBAC/GetAppName.vi"/>
				<Item Name="GetArraySizes.vi" Type="VI" URL="../lib/RBAC/GetArraySizes.vi"/>
				<Item Name="GetAuthTime.vi" Type="VI" URL="../lib/RBAC/GetAuthTime.vi"/>
				<Item Name="GetBinaryToken.vi" Type="VI" URL="../lib/RBAC/GetBinaryToken.vi"/>
				<Item Name="GetExpTime.vi" Type="VI" URL="../lib/RBAC/GetExpTime.vi"/>
				<Item Name="GetLocationAdress.vi" Type="VI" URL="../lib/RBAC/GetLocationAdress.vi"/>
				<Item Name="GetLocationAuthenticationRequest.vi" Type="VI" URL="../lib/RBAC/GetLocationAuthenticationRequest.vi"/>
				<Item Name="GetLocName.vi" Type="VI" URL="../lib/RBAC/GetLocName.vi"/>
				<Item Name="GetRoles.vi" Type="VI" URL="../lib/RBAC/GetRoles.vi"/>
				<Item Name="GetSerialID.vi" Type="VI" URL="../lib/RBAC/GetSerialID.vi"/>
				<Item Name="GetTokenIDs.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetTokenIDs.vi"/>
				<Item Name="GetUserCredentials.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetUserCredentials.vi"/>
				<Item Name="GetUserName.vi" Type="VI" URL="../lib/RBAC/GetUserName.vi"/>
				<Item Name="IsApplicationCritical.vi" Type="VI" URL="../lib/RBAC/IsApplicationCritical.vi"/>
				<Item Name="SelectPathLib.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/SelectPathLib.vi"/>
				<Item Name="RBAC_login.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC_login.vi"/>
				<Item Name="String2HTMLhex.vi" Type="VI" URL="../lib/RBAC/String2HTMLhex.vi"/>
				<Item Name="UserCredentials.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/UserCredentials.ctl"/>
				<Item Name="beecrypt.dll" Type="Document" URL="../lib/RBAC/beecrypt.dll"/>
				<Item Name="libcurl.dll" Type="Document" URL="../lib/RBAC/libcurl.dll"/>
				<Item Name="RBAC.dll" Type="Document" URL="../lib/RBAC/RBAC.dll"/>
				<Item Name="RBAC.so" Type="Document" URL="../lib/RBAC/RBAC.so"/>
			</Item>
			<Item Name="fgc_51_data_decode.vi" Type="VI" URL="../lib/fgc_51_data_decode.vi"/>
			<Item Name="fgc_51_full_status.ctl" Type="VI" URL="../lib/fgc_51_full_status.ctl"/>
			<Item Name="fgc_51_status.ctl" Type="VI" URL="../lib/fgc_51_status.ctl"/>
			<Item Name="fgc_93_data_decode.vi" Type="VI" URL="../lib/fgc_93_data_decode.vi"/>
			<Item Name="fgc_93_full_status.ctl" Type="VI" URL="../lib/fgc_93_full_status.ctl"/>
			<Item Name="fgc_93_status.ctl" Type="VI" URL="../lib/fgc_93_status.ctl"/>
			<Item Name="fgc_diag_decode.vi" Type="VI" URL="../lib/fgc_diag_decode.vi"/>
			<Item Name="fgc_disconnect.vi" Type="VI" URL="../lib/fgc_disconnect.vi"/>
			<Item Name="fgc_fifo_close.vi" Type="VI" URL="../lib/fgc_fifo_close.vi"/>
			<Item Name="fgc_fifo_init.vi" Type="VI" URL="../lib/fgc_fifo_init.vi"/>
			<Item Name="fgc_fifo_read.vi" Type="VI" URL="../lib/fgc_fifo_read.vi"/>
			<Item Name="fgc_get.vi" Type="VI" URL="../lib/fgc_get.vi"/>
			<Item Name="fgc_get_log_info.vi" Type="VI" URL="../lib/fgc_get_log_info.vi"/>
			<Item Name="fgc_init.vi" Type="VI" URL="../lib/fgc_init.vi"/>
			<Item Name="fgc_log_decode.vi" Type="VI" URL="../lib/fgc_log_decode.vi"/>
			<Item Name="fgc_log_signal.ctl" Type="VI" URL="../lib/fgc_log_signal.ctl"/>
			<Item Name="fgc_name_entry.ctl" Type="VI" URL="../lib/fgc_name_entry.ctl"/>
			<Item Name="fgc_pub_decode.vi" Type="VI" URL="../lib/fgc_pub_decode.vi"/>
			<Item Name="fgc_pub_decode_old.vi" Type="VI" URL="../lib/fgc_pub_decode_old.vi"/>
			<Item Name="fgc_raw_diag_data_decode.vi" Type="VI" URL="../lib/fgc_raw_diag_data_decode.vi"/>
			<Item Name="fgc_read_names.vi" Type="VI" URL="../lib/fgc_read_names.vi"/>
			<Item Name="fgc_resolve_name.vi" Type="VI" URL="../lib/fgc_resolve_name.vi"/>
			<Item Name="fgc_serial_command.vi" Type="VI" URL="../lib/fgc_serial_command.vi"/>
			<Item Name="fgc_serial_diag_decode.vi" Type="VI" URL="../lib/fgc_serial_diag_decode.vi"/>
			<Item Name="fgc_serial_init.vi" Type="VI" URL="../lib/fgc_serial_init.vi"/>
			<Item Name="fgc_set.vi" Type="VI" URL="../lib/fgc_set.vi"/>
			<Item Name="fgc_tcp_auth.vi" Type="VI" URL="../lib/fgc_tcp_auth.vi"/>
			<Item Name="fgc_tcp_command.vi" Type="VI" URL="../lib/fgc_tcp_command.vi"/>
			<Item Name="fgc_tcp_connect.vi" Type="VI" URL="../lib/fgc_tcp_connect.vi"/>
			<Item Name="fgc_tcp_receive_response.vi" Type="VI" URL="../lib/fgc_tcp_receive_response.vi"/>
			<Item Name="fgc_udp_diag_close.vi" Type="VI" URL="../lib/fgc_udp_diag_close.vi"/>
			<Item Name="fgc_udp_diag_decode.vi" Type="VI" URL="../lib/fgc_udp_diag_decode.vi"/>
			<Item Name="fgc_udp_diag_init.vi" Type="VI" URL="../lib/fgc_udp_diag_init.vi"/>
			<Item Name="fgc_udp_pub_close.vi" Type="VI" URL="../lib/fgc_udp_pub_close.vi"/>
			<Item Name="fgc_udp_pub_decode.vi" Type="VI" URL="../lib/fgc_udp_pub_decode.vi"/>
			<Item Name="fgc_udp_pub_decode_old.vi" Type="VI" URL="../lib/fgc_udp_pub_decode_old.vi"/>
			<Item Name="fgc_udp_pub_init.vi" Type="VI" URL="../lib/fgc_udp_pub_init.vi"/>
			<Item Name="gereg_plus_close.vi" Type="VI" URL="../lib/gereg_plus_close.vi"/>
			<Item Name="gereg_plus_init.vi" Type="VI" URL="../lib/gereg_plus_init.vi"/>
			<Item Name="gereg_plus_read.vi" Type="VI" URL="../lib/gereg_plus_read.vi"/>
			<Item Name="read_n_bytes_from_port.vi" Type="VI" URL="../lib/read_n_bytes_from_port.vi"/>
			<Item Name="values_to_array.vi" Type="VI" URL="../lib/values_to_array.vi"/>
		</Item>
		<Item Name="src" Type="Folder">
			<Item Name="fip_acquire.vi" Type="VI" URL="../src/fip_acquire.vi"/>
			<Item Name="gereg_acquire.vi" Type="VI" URL="../src/gereg_acquire.vi"/>
			<Item Name="globals.vi" Type="VI" URL="../src/globals.vi"/>
			<Item Name="scale10.vi" Type="VI" URL="../src/scale10.vi"/>
			<Item Name="spy-http_get_request.vi" Type="VI" URL="../src/spy-http_get_request.vi"/>
			<Item Name="spy.vi" Type="VI" URL="../src/spy.vi"/>
			<Item Name="spy_acquire.vi" Type="VI" URL="../src/spy_acquire.vi"/>
			<Item Name="sub_acquire.vi" Type="VI" URL="../src/sub_acquire.vi"/>
			<Item Name="tcp_receive_response.vi" Type="VI" URL="../src/tcp_receive_response.vi"/>
			<Item Name="update_fit.vi" Type="VI" URL="../src/update_fit.vi"/>
			<Item Name="update_graph_and_fit.vi" Type="VI" URL="../src/update_graph_and_fit.vi"/>
			<Item Name="zoom_points.vi" Type="VI" URL="../src/zoom_points.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatWriteText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatWriteText.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_MAPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MAPro.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="Write Characters To File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Characters To File.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Write File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ (string).vi"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="Write GIF File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/gif.llb/Write GIF File.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="LabVIEWHTTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/httpClient/LabVIEWHTTPClient.lvlib"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="RBAC.dll" Type="Document" URL="/&lt;userlib&gt;/_RADE/libs/RBAC.dll"/>
				<Item Name="CreateToken.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/CreateToken.vi"/>
				<Item Name="AnalyzeError.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/AnalyzeError.vi"/>
				<Item Name="CreateTokenByLocation.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/CreateTokenByLocation.vi"/>
				<Item Name="GetAllTokenFields.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetAllTokenFields.vi"/>
				<Item Name="GetUserName.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetUserName.vi"/>
				<Item Name="GetArraySizes.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetArraySizes.vi"/>
				<Item Name="GetSerialID.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetSerialID.vi"/>
				<Item Name="GetAuthTime.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetAuthTime.vi"/>
				<Item Name="GetExpTime.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetExpTime.vi"/>
				<Item Name="GetAppName.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetAppName.vi"/>
				<Item Name="IsApplicationCritical.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/IsApplicationCritical.vi"/>
				<Item Name="AppTimeout.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/AppTimeout.vi"/>
				<Item Name="GetLocName.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetLocName.vi"/>
				<Item Name="GetLocationAdress.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetLocationAdress.vi"/>
				<Item Name="GetLocationAuthenticationRequest.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetLocationAuthenticationRequest.vi"/>
				<Item Name="GetRoles.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetRoles.vi"/>
				<Item Name="GetBinaryToken.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/RBAC/RBAC2LV/GetBinaryToken.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="fgc_diag_cluster.ctl" Type="VI" URL="../lib/fgc_diag_cluster.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Spy Windows" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{330F3E6D-F467-4775-B9B4-664CF6297026}</Property>
				<Property Name="App_INI_GUID" Type="Str">{0323262E-90D8-4871-93F4-07ECAA568C91}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C2B2D7D5-26D5-11E5-BD49-68B599CAA810}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Spy Windows</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Windows</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C2B2DB6F-26D5-11E5-BD49-68B599CAA810}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguage[1]" Type="Str">French</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">5</Property>
				<Property Name="Bld_version.minor" Type="Int">30</Property>
				<Property Name="Destination[0].destName" Type="Str">spy.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Windows/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Windows</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/Windows</Property>
				<Property Name="Destination[2].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{1998D9CD-9A55-45C4-82FF-087DEC11741A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/src/spy.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
				<Property Name="TgtF_companyName" Type="Str">CERN</Property>
				<Property Name="TgtF_internalName" Type="Str">Spy Windows</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2010 CERN</Property>
				<Property Name="TgtF_productName" Type="Str">FGC Spy</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{72F72600-52BA-45FC-86DE-650AABEF4804}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">spy.exe</Property>
			</Item>
			<Item Name="Spy Windows installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">spy</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">NI_HTML</Property>
				<Property Name="Destination[1].parent" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="Destination[1].tag" Type="Str">{80C09AD9-A3C9-4D2F-87B5-8EF9DC092E02}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="Destination[2].name" Type="Str">NI_report</Property>
				<Property Name="Destination[2].parent" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="Destination[2].tag" Type="Str">{89030FEB-1013-4D01-AD2C-6A6BCDCE1E5F}</Property>
				<Property Name="Destination[2].type" Type="Str">userFolder</Property>
				<Property Name="Destination[3].name" Type="Str">NI_Standard Report</Property>
				<Property Name="Destination[3].parent" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="Destination[3].tag" Type="Str">{F528B59C-DC88-4A96-ABE1-183A95B60C12}</Property>
				<Property Name="Destination[3].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{C13DF63E-9B02-4CA4-B4CA-3E9B56EFB217}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-VISA Runtime 14.0</Property>
				<Property Name="DistPart[0].SoftDep[0].dependencyKey" Type="Str">GKBAAA.0BD020B2_0CDC_4607_B947_25FF5EAB7B8D</Property>
				<Property Name="DistPart[0].SoftDep[1].dependencyKey" Type="Str">HKBAAA.0BD020B2_0CDC_4607_B947_25FF5EAB7B8D</Property>
				<Property Name="DistPart[0].SoftDep[2].dependencyKey" Type="Str">KKBAAA.0BD020B2_0CDC_4607_B947_25FF5EAB7B8D</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{3FF6680C-29B9-48D6-ADC6-08ACA8B99946}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI LabVIEW Run-Time Engine 2014</Property>
				<Property Name="DistPart[1].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2014 Non-English Support.</Property>
				<Property Name="DistPart[1].SoftDep[0].upgradeCode" Type="Str">{CAC8FA79-6D3D-4263-BB7B-1A706EF87C08}</Property>
				<Property Name="DistPart[1].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[1].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[1].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[10].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[1].SoftDep[10].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[1].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[11].productName" Type="Str">NI mDNS Responder 14.0</Property>
				<Property Name="DistPart[1].SoftDep[11].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[1].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[12].productName" Type="Str">NI Deployment Framework 2014</Property>
				<Property Name="DistPart[1].SoftDep[12].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[1].SoftDep[13].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[13].productName" Type="Str">NI Error Reporting 2014</Property>
				<Property Name="DistPart[1].SoftDep[13].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[1].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[2].productName" Type="Str">NI Service Locator 14.0</Property>
				<Property Name="DistPart[1].SoftDep[2].upgradeCode" Type="Str">{B235B862-6A92-4A04-A8B2-6D71F777DE67}</Property>
				<Property Name="DistPart[1].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[3].productName" Type="Str">NI System Web Server 14.5</Property>
				<Property Name="DistPart[1].SoftDep[3].upgradeCode" Type="Str">{FCF64B73-B7D4-4971-8F11-24BAF7CC3E6C}</Property>
				<Property Name="DistPart[1].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[4].productName" Type="Str">Math Kernel Libraries</Property>
				<Property Name="DistPart[1].SoftDep[4].upgradeCode" Type="Str">{3BDD0408-2F90-4B42-9777-5ED1D4BE67A8}</Property>
				<Property Name="DistPart[1].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[5].productName" Type="Str">NI Logos 14.0</Property>
				<Property Name="DistPart[1].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[1].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[6].productName" Type="Str">NI TDM Streaming 14.0</Property>
				<Property Name="DistPart[1].SoftDep[6].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[1].SoftDep[7].exclude" Type="Bool">true</Property>
				<Property Name="DistPart[1].SoftDep[7].productName" Type="Str">NI LabVIEW Web Server 2014</Property>
				<Property Name="DistPart[1].SoftDep[7].upgradeCode" Type="Str">{4A8BDBBB-DA1C-45C9-8563-74C034FBD357}</Property>
				<Property Name="DistPart[1].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[8].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2014</Property>
				<Property Name="DistPart[1].SoftDep[8].upgradeCode" Type="Str">{4372F3E3-5935-4012-93AB-B6710CE24920}</Property>
				<Property Name="DistPart[1].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[1].SoftDep[9].productName" Type="Str">NI VC2008MSMs</Property>
				<Property Name="DistPart[1].SoftDep[9].upgradeCode" Type="Str">{FDA3F8BB-BAA9-45D7-8DC7-22E1F5C76315}</Property>
				<Property Name="DistPart[1].SoftDepCount" Type="Int">14</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{4722F14B-8434-468D-840D-2B0CD8CBD5EA}</Property>
				<Property Name="DistPartCount" Type="Int">2</Property>
				<Property Name="INST_author" Type="Str">CERN</Property>
				<Property Name="INST_buildLocation" Type="Path">../spy/install/Windows</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">Spy Windows installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="INST_productName" Type="Str">spy</Property>
				<Property Name="INST_productVersion" Type="Str">5.30.0</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14008034</Property>
				<Property Name="MSI_arpCompany" Type="Str">CERN</Property>
				<Property Name="MSI_arpURL" Type="Str">http://cern.ch/fgc</Property>
				<Property Name="MSI_distID" Type="Str">{7D0C8A85-05BB-44F3-A762-A555BACBF5B7}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{03E9A210-62E7-4EEF-9F84-A32260FA48EA}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{A4B0E60F-FDC7-4695-8A2D-2FAE4669037A}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">spy.exe</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{72F72600-52BA-45FC-86DE-650AABEF4804}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">Spy Windows</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/Spy Windows</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="Spy Linux" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{FC3A312B-19EF-11DE-A7C6-001A4BEB8E36}</Property>
				<Property Name="App_INI_GUID" Type="Str">{FC3A3045-19EF-11DE-A7C6-001A4BEB8E36}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C2C97877-26D5-11E5-BD49-68B599CAA810}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Spy Linux</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Linux</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C2C980CE-26D5-11E5-BD49-68B599CAA810}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguage[1]" Type="Str">French</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">5</Property>
				<Property Name="Bld_version.minor" Type="Int">20</Property>
				<Property Name="Destination[0].destName" Type="Str">spy.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Linux/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Linux/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{AFDAF252-26D5-11E5-81DC-68B599CAA810}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/src/spy.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">CERN</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Spy Linux</Property>
				<Property Name="TgtF_internalName" Type="Str">Spy Linux</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright 2010 </Property>
				<Property Name="TgtF_productName" Type="Str">FGC Spy</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{FC3A2E33-19EF-11DE-A7C6-001A4BEB8E36}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">spy.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
