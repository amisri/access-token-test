#!/usr/bin/perl
#
# Name:    spy_acquire.pl
# Purpose: Acquire data from the FGC Spy interface
# Author:  Stephen Page

use FGC::Spy;
use strict;
use warnings;

die "Usage: $0 <port (e.g. COM4)> <number of samples>\n" if(@ARGV != 2);
my ($port_name, $num_samples) = @ARGV;

# Connect to Spy interface

my $port = FGC::Spy::connect($port_name)
    or die "Unable to connect to FGC: $!\n";

# Read from Spy interface

my $data = FGC::Spy::read($port, 1, $num_samples);

# Disconnect from Spy interface

FGC::Spy::disconnect($port);

# Print header

print "TIME,";
for(my $i = 0 ; $i < FGC::Spy::FGC_N_SPY_CHANS ; $i++)
{
    print "SPY.MPX[$i],";
}
print "\n";

# Print samples

my $time = 0;
for my $sample (@$data)
{
    printf("%.03f,", $time);
    for my $field (@$sample)
    {
        printf("%.7e,", $field);
    }
    print "\n";

    # Add 1ms to $time

    $time += 0.001;
}

# EOF
