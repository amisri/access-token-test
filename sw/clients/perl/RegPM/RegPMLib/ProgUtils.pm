#!/usr/bin/perl 

package RegPM::RegPMLib::ProgUtils;

require Exporter; 
@ISA = qw(Exporter);

@EXPORT = qw(execute_programming_fsm);


use RegPM::RegPMLib::RegPMConsts;

use FGC::Sync;
use Carp;

use strict; 


my %programming_fsm = (

	NONE => {
		callback   => \&state_none,
		timeout    => 3,
		attempts   => 1,
		next_state => 'INIT',
			},
	INIT => {
		callback   => \&state_init,
		timeout    => 3,
		attempts   => 1,
		next_state => 'TRANSFER',
			},

	TRANSFER => {
		callback   => \&state_transfer_and_check,
		timeout    => 30,
		attempts   => 3,
		next_state => 'PROGRAMMING',
			},

	PROGRAMMING => {
		callback   => \&state_programming,
		timeout    => 30,
		attempts   => 3,
		next_state => 'NONE',
			}
	);



sub set_try_catch($$$$)
{
	my ($socket, $converter, $property, $value) = @_;
	my $response; 

	eval 
	{
		$response = FGC::Sync::set($socket, $converter, $property, $value)		
	};
	if ($@ || $response->{error}) 
	{
		confess "ERROR: Failed in command s $property $value: ".($@ ? $@ : "$response->{error}")."\n";
	}
}


sub state_none($$)
{
	my ($prog_data_ref, $state) = @_;

	print "In state NONE\n";

	# Nothing to do for the RegPM

	# Set FGC to work
	set_try_catch($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, 'REGFGC3.PROG.FSM.MODE', $state); 
}



sub state_init($$)
{
	my ($prog_data_ref, $state) = @_;

	print "In state INIT\n";

	my $response; 

	my %state_init_props_hash = (
		'REGFGC3.PROG.SLOT'        => $prog_data_ref->{SLOT},
		'REGFGC3.PROG.DEVICE'      => $prog_data_ref->{DEVICE},		
		'REGFGC3.PROG.VARIANT'     => $prog_data_ref->{VARIANT},
		'REGFGC3.PROG.API_VERSION' => $prog_data_ref->{VERSION},
		'REGFGC3.PROG.REVISION'    => $prog_data_ref->{REVISION}
	);

	while (my ($p, $v) = each(%state_init_props_hash))
	{
		set_try_catch($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, $p, $v);
	}

	# Set FGC to work
	set_try_catch($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, 'REGFGC3.PROG.FSM.MODE', $state); 
}



sub state_transfer_and_check($$)
{
	my ($prog_data_ref, $state) = @_;

	print "In state TRANSFER\n";	

	my $canonical_file_name = join('/', (PROGRAM_MANAGER_INSTALLED_DIR, 
										$prog_data_ref->{BOARD}, 
										$prog_data_ref->{DEVICE}, 
										$prog_data_ref->{VARIANT}, 
										$prog_data_ref->{FILE})); 

	transfer_binary($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, $canonical_file_name);

	# Set FGC to work
	set_try_catch($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, 'REGFGC3.PROG.FSM.MODE', $state); 
}



sub state_programming($$)
{
	my ($prog_data_ref, $state) = @_;

	print "In state PROGRAMMING\n";	

	# Nothing to do for the RegPM

	# Set FGC to work
	set_try_catch($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, 'REGFGC3.PROG.FSM.MODE', $state); 
}



sub poll_fgc($$$)
{
	my ($socket, $converter, $state) = @_; 
	my $response; 

	eval 
	{
		$response = FGC::Sync::get($socket, $converter, 'REGFGC3.PROG.FSM.STATE');
	};

	if ($@ || $response->{error}) 
	{
		confess "ERROR: Failed in command g REGFGC3.PROG.FSM.STATE.".($@ ? $@ : $response->{error})."\n";
	}

	# return $response->{value} eq $state; 
	return 1;
}


sub send_data($$$$)
{
	my ($socket, $converter, $data_ref, $total_sent) = @_; 
	my $n_words =  scalar @{$data_ref};

	chop(@$data_ref[-1]);

	my $content = join("", @$data_ref);
	
	my $to = $total_sent + $n_words - 1;

	my $response; 
	eval
	{
		$response = FGC::Sync::set($socket, $converter, "REGFGC3.PROG.BIN[$total_sent,$to]", $content)
	};
	if ($@ || $response->{error})
	{
		confess "ERROR: Failed in command s REGFGC3.PROG.BIN[$total_sent,$to]".($@? $@ : $response->{error})."\n";
	}

}



sub format_data($$)
{

	my ($data, $n_bytes) = @_;
	my $chars_in_word = 8;

	my $hex_word = unpack('H8', $data);

	if ($n_bytes * 2 < $chars_in_word)
	{
		$hex_word .= '0' x ($chars_in_word - $n_bytes*2);
	}

	return $hex_word;

}



sub transfer_binary($$$)
{
	my ($socket, $converter, $bin_file) = @_;

	my @prep_data = ();

	confess "Unable to open file $bin_file" if not open(my $fh, '<:raw', $bin_file);

    my $data_file_size = -s $bin_file;
    confess "Attempt to set binary bigger than the maximum avaiable memory (4 MB)!" if ($data_file_size > FGC_DSP_AVAIL_MEM);

    my ($current_batch_words, $total_words_sent) = (0, 0);
    print "INFO: Sending file $bin_file to $converter\n";

    while ( !eof($fh) )
    {
    	my $read_bytes = read($fh, my $word, 4);
    	my $hex_data = format_data($word, $read_bytes);

    	if ( $current_batch_words < LIMIT_GW_CMD_VALUE)
    	{
    		push(@prep_data, '0x'.$hex_data.',');
    		$current_batch_words += 1;
    	}

    	if ($current_batch_words == LIMIT_GW_CMD_VALUE)
    	{
    		send_data($socket, $converter, \@prep_data, $total_words_sent);
    		$total_words_sent += $#prep_data + 1;

    		undef(@prep_data);
    		$current_batch_words = 0;

    	}

    }

    if ( eof($fh) and @prep_data )
    {
    	send_data($socket, $converter, \@prep_data, $total_words_sent);

    	undef(@prep_data);
    	$current_batch_words = 0;
    }

    close($fh);	
}


# API

sub execute_programming_fsm($)
{
	my $prog_data_ref = shift; 
	my $state = 'NONE';

	print "Executing FSM for converter $prog_data_ref->{CONVERTER}\n";
	while(1)
	{		
		my ($state_func_ref, $attempts, $timeout) = (
			$programming_fsm{$state}{callback},
			$programming_fsm{$state}{attempts},
			$programming_fsm{$state}{timeout}	
			);

		# First, execute whatever the RegPM needs to execute	
		do 
		{
			eval { $state_func_ref->($prog_data_ref, $state) };
			$attempts-- if ($@);
		}
		while ($@ && $attempts > 0);
		confess "Could not finish RegPM action for state $state($@)\n" if ($attempts == 0);

		# Second, start polling the FGC
		my $fgc_finished = 0;
		my $start_time   = time; 
		my $countdown    = $timeout;

		do 
		{
			$fgc_finished = poll_fgc($prog_data_ref->{SOCKET}, $prog_data_ref->{CONVERTER}, $state);
			sleep(1);
			$countdown--; 
		}
		while(!$fgc_finished && $countdown > 0);
		confess "FGC transition to state $state did not finished on time($@)\n" if ($countdown == 0);

		last if $programming_fsm{$state}{next_state} eq 'NONE';
		$state = $programming_fsm{$state}{next_state};
	}

}


return 1; 