#!/usr/bin/perl

package RegPM::RegPMLib::FgcUtils;

require      Exporter;
@ISA       = qw(Exporter);

@EXPORT    = qw(get_detected 
				get_expected 
				set_installed_area 
				get_installed_area 
				get_diffs_expected_detected
                );

# @EXPORT_OK = qw(get_detected);

use Data::Dumper;
use List::MoreUtils qw(indexes);
use XML::Simple;

use RegPM::RegPMLib::RegPMConsts;

use strict; 


# Package variables
my $installed_area; 


# API
sub set_installed_area($)
{
	$installed_area = shift; 
}



sub get_installed_area
{
	return $installed_area; 
}



sub get_detected($$$)
{
    my ($socket, $device, $detected_ref) = @_;  
    my $response; 

    eval { $response = FGC::Sync::get($socket, $device, 'REGFGC3.SLOT_INFO') };
    if ($@)
    {
        die "Could not retrieve property REGFGC3.SLOT_INFO from device $device!\n"; 
    }

    my @response_lines = split("\n", $response->{value}); 
    my @slots_in_boot  = indexes {$_ eq BOOT_SLOT_STATUS} (split(',', $response_lines[0]));

    foreach my $slot (@slots_in_boot)
    {
        $detected_ref->{$slot}->{BOARD_TYPE} = (split(',', $response_lines[1]))[$slot];

        my $resp_line_number = 2;
        foreach my $device_number (1..PROG_DEVICES_NUMBER)
        {   
            # Store information in temporary hash
            my $device_name = ($device_number == 1) ? "MF" : "DEVICE_$device_number";
            my %device_hash = (); 

            $device_hash{VARIANT}  = (split(',', $response_lines[$resp_line_number + 1]))[$slot];
            $device_hash{VERSION}  = (split(',', $response_lines[$resp_line_number + 2]))[$slot]; 
            $device_hash{REVISION} = (split(',', $response_lines[$resp_line_number + 3]))[$slot]; 

            # Assign to detected hash
            $detected_ref->{$slot}->{DEVICES}->{$device_name} = \%device_hash; 
            $resp_line_number += PROPERTIES_PER_PROG_DEVICE;
        }
    }
}



# TODO: will this be replaced by queries to the EPC database?
sub get_expected($$)
{
    my ($converter_name, $expected_ref) = @_; 
    my $converter_type = (split('\.', $converter_name))[0];

    my $regfgc3_xml_file = PROGRAM_MANAGER_XML_DIR.'/'.$converter_type.'.xml';

    die "ERROR: Could not find RegFGC3 XML definition file for converter type $converter_type!\n" unless -f $regfgc3_xml_file;

    my $expected_data = XMLin($regfgc3_xml_file, KeyAttr => {board => 'slot'});

    my $boards_hash_ref = $expected_data->{board};
    foreach my $slot_number (sort keys %$boards_hash_ref)
    {
        $expected_ref->{$slot_number}->{BOARD_TYPE} = $boards_hash_ref->{$slot_number}->{name};

        # This can be an array of hashes or a hash if there is only one element
        my $device_xml_object = $boards_hash_ref->{$slot_number}->{device};

        my @device_xml_objects;
        if (ref($device_xml_object) eq 'HASH')
        {
            push(@device_xml_objects, $device_xml_object); 
        }
        elsif (ref($device_xml_object) eq 'ARRAY')
        {
            @device_xml_objects = @$device_xml_object; 
        }

        foreach my $dev (@device_xml_objects)
        {
            my %device_hash       = (); 
            my $device_name       = uc $dev->{name};
            $device_hash{VARIANT} = $dev->{variant};

            if ($dev->{variant} eq "")
            {
            	print "WARN: Expected variant is empty string for device $device_name in board $expected_ref->{$slot_number}->{BOARD_TYPE} of converter $converter_name\n";
            }

            ($device_hash{VERSION}, $device_hash{REVISION}, $device_hash{FILE}) = 
                                                                eval 
                                                                {
                                                                    get_installed_version_revision
                                                                    (
                                                                        $expected_ref->{$slot_number}->{BOARD_TYPE}, 
                                                                        $device_name, 
                                                                        $device_hash{VARIANT}
                                                                    )
                                                                };

            if ($@)
            {
                print "ERROR: $@";
                ($device_hash{VERSION}, $device_hash{REVISION}, $device_hash{FILE}) = (INVALID_VERSION, INVALID_REVISION, "");
            }

            $expected_ref->{$slot_number}->{DEVICES}->{$device_name} = \%device_hash;
        }
    } 
}



sub get_diffs_expected_detected($$$)
{
    my ($device, $detected, $expected) = @_; 
    my %diffs = ();

    # Check expected vs detected, being expected the source
    foreach my $slot_number (sort keys %$expected)
    {
        if (! $detected->{$slot_number})
        {
            # TODO: set the converter as non-operational, missing board
            print "ERROR: Converter $device is missing a board of type $expected->{$slot_number}->{BOARD_TYPE} in slot $slot_number\n";
            next; 
        } 

        if ($detected->{$slot_number}->{BOARD_TYPE} ne $expected->{$slot_number}->{BOARD_TYPE})
        {
            # TODO: set the converter as non-operational, missing board
            print "ERROR: converter $device has board $detected->{$slot_number}->{BOARD_TYPE} in slot $slot_number, but expects $expected->{$slot_number}->{BOARD_TYPE}\n";
            next; 
        }  

        foreach my $device_name (sort keys %{$detected->{$slot_number}->{DEVICES}})
        {
            if (! $expected->{$slot_number}->{DEVICES}->{$device_name})
            {
                # TODO: set the converter as non-operational, missing board
                print "ERROR: Converter $device has a programmable device $device_name in board $detected->{$slot_number}->{BOARD_TYPE} that is not expected!\n";
                next; 
            }

            my $detected_fw_data = $detected->{$slot_number}->{DEVICES}->{$device_name};
            my $expected_fw_data = $expected->{$slot_number}->{DEVICES}->{$device_name};

            my $warn_message = "";

            next if ($detected_fw_data->{VARIANT} eq 'EMPTY' && $expected_fw_data->{VARIANT} eq 'EMPTY');
            next if ($expected_fw_data->{VERSION} == INVALID_VERSION || $expected_fw_data->{REVISION} == INVALID_REVISION);

            if ( ($detected_fw_data->{VARIANT}      ne $expected_fw_data->{VARIANT})
                || ($detected_fw_data->{VERSION}    ne $expected_fw_data->{VERSION})
                || ($detected_fw_data->{REVISION}   ne $expected_fw_data->{REVISION})
                )
            {                
                $warn_message .= "- Board $detected->{$slot_number}->{BOARD_TYPE}\n";
                $warn_message .= "-- Expected: $expected_fw_data->{VARIANT}, $expected_fw_data->{VERSION}, $expected_fw_data->{REVISION};\n";
                $warn_message .= "-- Detected: $detected_fw_data->{VARIANT}, $detected_fw_data->{VERSION}, $detected_fw_data->{REVISION};\n";
                $warn_message .= "--- targeted for reprogramming with $expected_fw_data->{FILE}\n";

                print $warn_message;
                $warn_message = "";
                $diffs{$slot_number}{BOARD_TYPE}            = $expected->{$slot_number}->{BOARD_TYPE};
                $diffs{$slot_number}{DEVICES}{$device_name} = $expected_fw_data; 
            } 
        }        
    }

    # Check detected vs expected, being detected the source
    foreach my $slot_number (sort keys %$detected)
    {
        if(! $expected->{$slot_number})
        {
            print "ERROR: Converter $device: unexpected board of type $expected->{$slot_number}->{BOARD_TYPE} found in slot $slot_number\n";
        }
    }

    return \%diffs; 
}



# Private functions

sub get_installed_version_revision($$$)
{
    my ($board_type, $device, $variant) = @_;
    my ($version, $revision) = (INVALID_VERSION, INVALID_REVISION);

    if ($variant eq "EMPTY")
    {
        return ($version, $revision, "");
    }

    # TODO: include installed_area in directory
    my $err_message = "Binary file for board $board_type, device $device, variant $variant not found in installed area $installed_area\n";
    my $directory_name = join('/', (PROGRAM_MANAGER_INSTALLED_DIR, $board_type, $device, $variant));

    opendir(my $dh, $directory_name) or die $err_message; 
    my @binary_files = grep{/[^.]/} readdir($dh);

    $err_message = "Wrong number of binary files in directory $directory_name (expected just one)";
    die $err_message if (scalar(@binary_files) != 1);
    ($version, $revision) = $binary_files[0] =~ /EDA_\d{5}-\w+-\w+-(\d{1,4})-(\d{1,4}).bin/g; 

    closedir($dh);

    return ($version, $revision, $binary_files[0]);
}


return 1; 