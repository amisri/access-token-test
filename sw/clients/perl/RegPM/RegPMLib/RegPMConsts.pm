#!/usr/bin/perl

package RegPM::RegPMLib::RegPMConsts;

require Exporter;
@ISA = qw(Exporter);

@EXPORT = qw(
		BOOT_SLOT_STATUS
		PROG_DEVICES_NUMBER
		PROPERTIES_PER_PROG_DEVICE
		PROGRAM_MANAGER_XML_DIR
		INVALID_REVISION
		INVALID_VERSION
		PROGRAM_MANAGER_INSTALLED_DIR
		PROG_MAX_ATTEMPTS
		FGC_DSP_AVAIL_MEM
		LIMIT_GW_CMD_VALUE
);

#TODO: Change string from OK to BOOT when
# 1. The proggram manager code runs in the FGC3 and
# 2. the boards start in boot mode
use constant BOOT_SLOT_STATUS           => 'OK'; 

#TODO: Change number to 5, when there is memory in FGC3 (ha ha ha)
use constant PROG_DEVICES_NUMBER        => 2;
use constant PROPERTIES_PER_PROG_DEVICE => 4; 
use constant INVALID_REVISION           => 0;
use constant INVALID_VERSION            => 0;
use constant PROG_MAX_ATTEMPTS => 3;

#TODO: change this location by the definitive codes install directory. Add to the parent directory a suffix based on the 
#argument of this script (the fgc config manager gateway set, which designs a zone)
use constant PROGRAM_MANAGER_INSTALLED_DIR  => '/user/pclhc/etc/program_manager/FW';
use constant PROGRAM_MANAGER_XML_DIR    => '/user/pclhc/etc/program_manager/XML';

use constant FGC_DSP_AVAIL_MEM 	=> 4194304;
use constant LIMIT_GW_CMD_VALUE => 66100;


return 1;
