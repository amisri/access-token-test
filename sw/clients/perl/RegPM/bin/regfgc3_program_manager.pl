#!/usr/bin/perl 

use strict; 
use warnings; 


# Modules

use FGC::Consts;
use FGC::StatSrv;
use FGC::RBAC; 
use RegPM::RegPMLib::FgcUtils;
use RegPM::RegPMLib::ProgUtils;
use RegPM::RegPMLib::RegPMConsts;

use Carp qw(confess);
use Data::Dumper;
use File::Basename qw(basename);



# Globals

my $start_time = time();
my $rbac_token;
my $rbac_last_auth_time = 0;
my %expected = ();
my %detected = (); 
my %targets  = (); 


my @test_devices = (); #(qw(RPAGM.866.21.ETH1));


# Flush stdout 

$| = 1;



# Functions

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate_by_location() or return(1);

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return 0;
}


sub clean_detected_expected()
{
    (%detected, %expected) = ((), ());
}


sub clean_target($$)
{
    my ($gw, $converter) = @_;
    $targets{$gw}{$converter} = ();
}



sub check_devices($)
{
	my $status_data = shift; 

	# Construct array of devices to be synchronised

    my @devices_sync;

    for(my $channel = 1 ; $channel < FGCD_MAX_DEVS ; $channel++)
    {
        my $device = $status_data->{channels}->[$channel];

        # Ignore offline and unnamed devices

        next if(!$device->{DATA_STATUS}->{DATA_VALID}   
                || !defined($device->{NAME})               
                || !defined($device->{ST_UNLATCHED})
                );

        # Check whether a synchronisation is needed

        if($device->{ST_UNLATCHED}->{SYNC_REGFGC3})
        {
        	# ToDo: only push if the device is RegFGC3
            push(@devices_sync, $device->{NAME});
        }
        
    }

    # TODO: remove TEST line
    push @devices_sync, @test_devices; 

    return if(!@devices_sync);

    my @sorted_devices = sort(@devices_sync); 

    # Connect to gateway
    print "Connecting to gateway $status_data->{hostname}...\n";

    my $socket;
    eval { $socket = FGC::Sync::connect( { name => $status_data->{hostname} } ) };
    if($@)
    {
        print "ERROR: Failed to connect to gateway $status_data->{hostname}\n";
        return;
    }


    # Set RBAC token on gateway
    my $response;
    eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$rbac_token->{binary}, 1) };
    if($@ || $response->{error})
    {
        print "ERROR: Failed to set RBAC token on gateway $status_data->{hostname}: " . ($@ ? $@ : "$response->{value}") . "\n";
        return;
    }

    foreach my $device (@sorted_devices)
    {
        
    	# Check whether it is UNSYNCED
        eval { $response = FGC::Sync::get($socket, $device, "REGFGC3.PROG.STATE") };
        if ($@)
        {
            print "ERROR: Failed to get property REGFGC3.PROG.STATE from device $device-> $@\n";
        } 
        # print "INFO: $device REGFGC3.PROG.STATE: $response->{value}\n";

        # TODO: uncomment
        # next if ($response->{data} ne 'UNSYNCED';

        # Clean expected and detected for the next converter
        clean_detected_expected();

    	# Get detected
        print "INFO: Analyzing device: $device\n";  
        eval { get_detected($socket, $device, \%detected) };
        if ($@)
        {
            print "ERROR: $@\n";
            next; 
        }

    	# Get expected
        eval { get_expected($device, \%expected) };
        if ($@)
        {
            print "ERROR: Failed to get expected data from device $device -> $@\n";
            next; 
        } 

    	# Compare expected and detected  
        my $diff_hash;   
        eval { $diff_hash = get_diffs_expected_detected($device, \%detected, \%expected) };
        if ($@)
        {
            print "ERROR: Failed to detect differences between expected and detected data -> $@\n";
        }

        $targets{$status_data->{hostname}}{$device} = $diff_hash if(%{$diff_hash});
    }
    
    print "DEBUG: targeted devices for reprogramming...\n";
    print Dumper(%targets);

    FGC::Sync::disconnect( { socket => $socket } );
}



sub program_devices
{
    foreach my $gateway (sort keys %targets)
    {
        my $socket;
        eval { $socket = FGC::Sync::connect( { name => $gateway } ) };
        if($@)
        {
            print "ERROR: Failed to connect to gateway $gateway\n";
            next;
        }

        foreach my $converter (sort keys %{$targets{$gateway}})
        {
            eval 
            {
                foreach my $slot (sort keys %{$targets{$gateway}{$converter}})
                {
                    my $board_type = $targets{$gateway}{$converter}{$slot}{BOARD_TYPE};

                    foreach my $device (sort keys %{$targets{$gateway}{$converter}{$slot}{DEVICES}})
                    {
                        my $device_data = $targets{$gateway}{$converter}{$slot}{DEVICES}{$device};

                        my %programming_data = 
                        (
                            SOCKET    => $socket, 
                            CONVERTER => $converter, 
                            SLOT      => $slot, 
                            BOARD     => $board_type,  
                            DEVICE    => $device, 
                            VARIANT   => $device_data->{VARIANT},
                            VERSION   => $device_data->{VERSION},
                            REVISION  => $device_data->{REVISION},
                            FILE      => $device_data->{FILE}
                        );

                        execute_programming_fsm(\%programming_data);
                    }
                }
            };

            if ($@) 
            {
                # TODO: mark the converter as non-operational
                print "ERROR: Maximum attempts to reprogram FGC $converter reached -> $@\n";
            }

            clean_target($gateway, $converter);
        }

        FGC::Sync::disconnect( { socket => $socket } );
    }
}




# Main code

confess "\nUsage: $0 <gateway list file>\n\n" if(@ARGV < 1);
my $gateway_file = shift;
set_installed_area(basename($gateway_file));   


# Read from name file 

my ($devices, $gateways) = FGC::Names::read();
confess "Unable to read FGC name file\n" if(!defined($devices));


# Read gateway list file

open(my $gateway_fh, "<", $gateway_file) or confess "Unable to open gateway list file $gateway_file: $!\n";
my @gateway_names = <$gateway_fh>;
close($gateway_fh);


# @target_gateways contains GW names common between the gateway file and the name file
my @target_gateways;
for my $gateway_name (sort(@gateway_names))
{
    chomp($gateway_name);

    my $gateway = $gateways->{$gateway_name};
    confess "Gateway $gateway_name does not exist\n" if(!defined($gateway));

    # Push only FGC-Ether gateways
    push(@target_gateways, $gateway) if ($gateway_name=~ /^cf[cv]-(.*?)-.*reth\d$/i); 
}


# Sleep for a random period to reduce the peak load on the status server
# when multiple instances of the program manager are started

sleep(rand(FGC_STATSRV_UPDATE_SECS));


# # Connect to the status server
my $socket;
eval { $socket = FGC::StatSrv::connect() };
while($@)
{
    sleep(5);
    eval { $socket = FGC::StatSrv::connect() };
}

print "\nRegFGC3 Program Manager started at ", scalar(localtime), "\n\n";

my $min_iteration_time_sec = 5; 


# while (1)
# {
# 	# Check if the name file has changed since the program started
    
    my $mod_time = (stat(FGC::Names::FGC_NAME_FILE))[9];

    if ($mod_time > $start_time)
    {
        confess "WARN: Name file is newer than the currently loaded by the config manager. Stopping...\n";
    }


    # Check if the gw_sets configuration file has changed since the program started
    $mod_time = (stat($gateway_file))[9];

    if ($mod_time > $start_time)
    {
        confess "WARN: $gateway_file file is newer than currently loaded by the config manager. Stopping...\n";
    }


    # Check whether RBAC token is less than one hour from expiry
    if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
    {
        # Check whether it is at least one minute since last authentication

        if($rbac_last_auth_time < (time - 60))
        {
            print "Getting new RBAC token at ", scalar(localtime), "\n\n";

            # RBAC authentication failed
            if(get_rbac_token() != 0) 
            {
                warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }
        }

        # If there is no token or token has expired, wait then retry

        sleep(60), next if(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
    }
    

    # Read status of all gateways

    my $gw_status;
    eval{ $gw_status = FGC::StatSrv::getallstatus($socket) };

    # Handle error

    if($@ || !defined($gw_status))
    {
        warn "Error getting status data from status server, disconnecting\n";

        FGC::StatSrv::disconnect($socket);

        # Periodically attempt to reconnect
        do
        {
            sleep(5);
            eval { $socket = FGC::StatSrv::connect() };
        } while($@);
        warn "Reconnected to status server\n";
    }

    # Check status of each gateway

    for my $gateway (@target_gateways)
    {
        # Read status for gateway

        my $status_data = $gw_status->{$gateway->{name}};

        # Handle error

        if(!defined($status_data)) # An error occurred
        {
            warn "Error getting status data from status server for gateway $gateway->{name}, disconnecting\n";
            next;
        }
        else
        {
            # Check status of devices if data is fresh
            if($status_data->{recv_time_sec} >= (time() - ((FGC_STATSRV_UPDATE_SECS * 2) + 1))) # Data is fresh
            {
                # Check all devices
                check_devices($status_data);

                # Program devices
                program_devices(); 

            }
        }
    }
    
    sleep($min_iteration_time_sec);
# }

# Disconnect from the status server

FGC::StatSrv::disconnect($socket);

# EOF