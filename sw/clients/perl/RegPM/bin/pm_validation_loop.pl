#!/usr/bin/perl 

use warnings; 
use strict; 

my $prog_data_file = "prog_data.csv";
my %prog_data;

open(my $fh, "<", $prog_data_file) or die "Could not open file $prog_data_file";
my $i = 0;
while (my $row = <$fh>)
{
	next if $row =~ /^#/;
	chomp $row;

	my @splitted = split(",", $row);
	$prog_data{$i} = \@splitted;
	$i++;
}
close($fh);

