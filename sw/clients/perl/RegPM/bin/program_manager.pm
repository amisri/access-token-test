 #!/usr/bin/perl -w
#
# Name: 	program_manager.pl
# Purpose:  Programs RegFGC3 boards' DEVICEs
# Author:   Carlos Ghabrous Larrea
# Description:
# Phase #1. For the initial phase, the script will assume that the binary codes have already been released and installed. Therefore, it will expect to find RegFGC3 binary files in 
# /user/pclhc/etc/regfgc3/FW/CARD_NAME/VARIANT_NAME. It will expect as input arguments:
# a. the card name, 
# b. the variant name, 
# c. the card's DEVICE 
# d. one of the following deployment options: area (-a), gateway (-g) or converter (-c)


use strict; 

use Carp;
use Scalar::Util qw (looks_like_number);
use Term::ANSIColor;
use Test::TCP;



##### Constants #####

# Binary files' repository location
# files/dirs needed to resolve areas and gateways to converters
use constant GROUP_FILE        	=> '/user/pclhc/etc/fgcd/group';
use constant NAME_FILE         	=> '/user/pclhc/etc/fgcd/name';
use constant GW_SETS_DIR       	=> '/user/pclhc/etc/fgc_config_manager/gw_sets/';
use constant FW_REPO			=> '/user/pclhc/etc/program_manager/FW';
use constant GATEWAY           	=> 'GW';
use constant CONVERTER         	=> 'CONV';
use constant FGC_DSP_AVAIL_MEM 	=> 4194304;
use constant LIMIT_GW_CMD_VALUE => 66100;



# ToDo: Generate a perl module from the parser?
use constant CARDS => qw (
	NO_BOARD
	VS_STATE_CTRL 
	VS_BIS_INTK	
	VS_ANA_INTK 
	VS_ANA_INTK_2  
	SIRAMATRIX 	
	VS_DIG_INTK  
	VS_MODE_ANODE  
	VS_REG_DSP 	
	);


use constant VARIANTS => qw (
	EMPTY 		
	GENERIC 	
	DEVELOPMENT 
	STANDARD_4 	
	COMPATIBLEV3 
	SIRIUS 		
	MIDIDISCAP10H 
	THYRISTOR	
	MODULATOR_L4  
	MODULATOR_L46  
	MODULATOR_L47  
	MIDIDISCAP  
	CANCUN	 	
	MAXIDISCAP 	
	MARXDISCAP 	
	MAXIDISCAP_22 
	MAXIDISCAP_V3 
	IGBT_33
	IGBT_34 		
	SVC 		
	MINI_POPSB 	
	HMINUS_DISCAP 
	COMHV_PS 	
	CAPA_TEST_BED 
	RF25KV 		
	TEST 		
	MAXIDISCAP_M1 
	MEGADISCAP_30 
	RF25KV_82
	); 


use constant DEVICES => qw (
	DB 
	MF
	DEVICE_2
	DEVICE_3 
	DEVICE_4 
	DEVICE_5
	);

use constant AREAS => qw (
	ADE 			
	CPS 			
	Development     
	ELENA 			
	ISOLDE 			
	LHC_1 			
	LHC_2 			
	LHC_3 			
	LHC_4 			
	LHC_5 			
	LHC_6 			
	LHC_7 			
	LHC_8 			
	LN4 			
	PSB 			
	SPS 			
	TestHalls_and_Labs 
	TestReception 	
	);
 

use constant CARDS_NUMBER 		=> 9;
use constant VARIANTS_NUMBER 	=> 29;
use constant DEVICES_NUMBER 	=> 6;
use constant AREAS_NUMBER 		=> 18;


##### Variables #####


# Turn output buffering off
$|++; 




##### Functions #####


sub __print_error($)
{
	my $message = shift; 

	print color('bold red');
	print "\nERROR: ".$message."\n";
	print color('reset'); 
}



sub __check_area($)
{
	my $area = shift;

	my %areas_hash = map {$_ => 1} (AREAS)[0 .. AREAS_NUMBER-1];

	if (exists $areas_hash{$area})
	{
		return; 
	}

	my $error_message = "Area does not exist! Available deployment areas are: \n";
	foreach my $key (sort keys(%areas_hash))
	{
		$error_message .= $key." ";
	}

	$error_message .= "\n";

	confess $error_message;
}



sub __resolve_area($$)
{
	my ($area, $device_list_ref) = @_;
	print "Resolving area $area...\n";

	__check_area($area);

	my $gw_set_file = GW_SETS_DIR.$area;
	open (my $fh, $gw_set_file) or die "Could not open file $gw_set_file $!"; 

	while (my $row = <$fh>)
	{
		chomp $row;
		__resolve_device($row, $device_list_ref);
	}

	close($fh);
}



sub __resolve_device($$)
{
	my ($device, $device_list_ref) = @_; 
	print "Resolving device $device...\n";

	my $device_type;
	$device =~ m/^CFC-/i ? ($device_type = GATEWAY) : ($device_type = CONVERTER);

	open(my $fh, NAME_FILE) or die "Could not open file NAME_FILE $!";

	while (my $row = <$fh>)
	{
		my @name_file_line = split(/:/, $row);

		if ($device_type eq CONVERTER)
		{
			next if ($name_file_line[3] ne $device);
		}
		else
		{
			next if ($name_file_line[0] ne $device);
		}
		
		next if ($name_file_line[2] !~ m/61|62|63/);
		
		push(@$device_list_ref, $name_file_line[3]);
	}

	confess "Device $device not found in name file or had non RegFGC3 FGC class!\n" if (not @$device_list_ref);

	close($fh);

	return $device_list_ref; 
}



sub __print_converter_list($)
{
	my $list_ref = shift; 

	my $i = 0;
	foreach my $converter (@$list_ref)
	{
		print color('green') if ($i % 2);
		print $converter, "\n";
		print color('reset');
		$i++;
	}
}



sub __get_device_list($$)
{
	my ($dep_option, $dep_object) = @_; 
	my @device_list = ();

	if ($dep_option !~ m/-[a|g|c]/ )
	{
		usage(); 
	}


	if ($dep_option eq '-a')
	{
		# get list of gateways
		__resolve_area($dep_object, \@device_list); 	
	}

	if ($dep_option eq '-g')
	{
		# get list of converters
		confess "$dep_object does not seem to be a gateway!" if ($dep_object !~ m/^CFC-.+ETH\d$/i);
		__resolve_device(lc $dep_object, \@device_list);
	}

	if ($dep_option eq '-c')
	{
		confess "$dep_object does not seem to be a converter!" if ($dep_object !~ m/^R.+ETH\d$/i);
		__resolve_device(uc $dep_object, \@device_list);
	}

	return \@device_list;

}



sub __check_slot_card_consistency($$$)
{
	my ($converter, $slot, $card) = @_; 
	my $detected; 

	eval { $detected = get($converter, 'REGFGC3.SLOT_INFO.DETECTED.BOARD_TYPE') };
	if ($@)
	{
		__print_error("$@");
		return undef;
	}

	my @cards = split(/,/, $detected);
	if ( not $cards[$slot] eq uc($card) )
	{
		__print_error("card $card is not on slot $slot in converter $converter!");
		return undef;
	}

	return 1;
	 
}



sub __check_binary($$$)
{
	my ($binary_dir, $binary, $variant) = @_; 
	my @bin_parts = split(/[-,\.]/, $binary);
	
	if (@bin_parts == 0)
	{
		__print_error("Binary file name does not follow naming convention: EDA_NUMBER-DEVICE-VARIANT-API_VERSION-REVISION");
		confess "\nQuiting program...\n";
	}

	my %devices_hash = map {$_ => 1} (DEVICES)[0 .. DEVICES_NUMBER-1];
	if ( not exists $devices_hash{$bin_parts[1]})
	{
		__print_error("DEVICE => $bin_parts[1] in binary file name is not a known device in file $binary!");
		confess "\nQuiting program...\n";
	}

	my %variants_hash = map {$_ => 1} (VARIANTS)[0 .. VARIANTS_NUMBER-1];
	if (not exists $variants_hash{$bin_parts[2]})
	{
		__print_error("VARIANT_NAME => $bin_parts[2] is not a valid variant in file $binary!");
		confess "\nQuiting program...\n";
	}

	if ($bin_parts[2] ne $variant)
	{
		__print_error("Command line variant $variant does not match file's: $bin_parts[2]!");
		confess "\nQuitting program...\n";
	}

	if ( not (looks_like_number($bin_parts[3]) && looks_like_number($bin_parts[4])) )
	{
		__print_error("APIVERSION => $bin_parts[3] and/or REVISION => $bin_parts[4] don't look like numbers in file $binary!");
		confess "\nQuitting program...\n";
	}

	if ( ($bin_parts[3] < 1 || $bin_parts[3] > 1023) || ($bin_parts[4] < 1 || $bin_parts[4] > 1023) )
	{	
		__print_error("APIVERSION => $bin_parts[3] and/or REVISION => $bin_parts[4] out of limits [1-1023] in file $binary!");
		confess "\nQuitting program...\n";
	}

	my $binary_size_bytes = -s join('/', $binary_dir, $binary); 
	return ($bin_parts[3], $bin_parts[4], $binary_size_bytes);
}



sub __print_set_prop_error($)
{
	my $prop = shift; 
	confess "Could not set property $prop!\n";
}



sub __format_data($$)
{
	my ($data, $n_bytes) = @_;
	my $chars_in_word = 8;

	my $hex_word = unpack('H8', $data);

	if ($n_bytes * 2 < $chars_in_word)
	{
		$hex_word .= '0' x ($chars_in_word - $n_bytes*2);
	}

	return $hex_word;
}



sub __send_data($$$)
{
	my ($converter, $data_ref, $total_sent) = @_; 
	my $n_words =  scalar @{$data_ref};

	chop(@$data_ref[-1]);

	my $content = join("", @$data_ref);
	
	my $to = $total_sent + $n_words - 1;

	print "REGFGC3.PROG.BIN[$total_sent,] = $content\n";
	set($converter, "REGFGC3.PROG.BIN[$total_sent,]", $content);
}



sub __transfer_binary($$$)
{
	my ($converter, $bin_dir, $bin_file) = @_;

	my $canonical_file_name = $bin_dir.'/'.$bin_file; 
	my @prep_data = ();

	confess "Unable to open file $canonical_file_name" if not open(my $fh, '<:raw', $canonical_file_name);

    my $data_file_size = -s $canonical_file_name;
    confess "Attempt to set binary bigger than the maximum avaiable memory (4 MB)!" if ($data_file_size > FGC_DSP_AVAIL_MEM);

    my ($current_batch_words, $total_words_sent) = (0, 0);

    while ( !eof($fh) )
    {
    	my $read_bytes = read($fh, my $word, 4);
    	my $hex_data = __format_data($word, $read_bytes);

    	if ( $current_batch_words < LIMIT_GW_CMD_VALUE)
    	{
    		push(@prep_data, '0x'.$hex_data.',');
    		$current_batch_words += 1;
    	}

    	if ($current_batch_words == LIMIT_GW_CMD_VALUE)
    	{
    		__send_data($converter, \@prep_data, $total_words_sent);
    		$total_words_sent += $#prep_data + 1;

    		undef(@prep_data);
    		$current_batch_words = 0;
    	}

    }

    if ( eof($fh) and @prep_data )
    {
    	__send_data($converter, \@prep_data, $total_words_sent);

    	undef(@prep_data);
    	$current_batch_words = 0;
    }

    close($fh);	
}




sub program_cards_v1($)
{

	my $prog_data_ref 	= shift; 
	my %prog_data 		= %$prog_data_ref;
	my $converters 		= $prog_data{CONVERTERS};

	for my $converter (@$converters)
	{
	 	# next if not __check_slot_card_consistency($converter, $prog_data{SLOT}, $prog_data{CARD});

		eval { set($converter, 'REGFGC3.PROG.FSM.MODE', 'WAITING') };
		if ($@)
		{
			__print_error('REGFGC3.PROG.FSM.MODE, $@');
		}

		eval { set($converter, 'REGFGC3.PROG.FSM.MODE', 'INIT')};
		if ($@)
		{
			__print_error('REGFGC3.PROG.FSM.MODE, $@');
		}

		eval { set($converter, 'FGC.DEBUG.MEM.DSP', '0x80100000')};

		eval{ set($converter, 'REGFGC3.PROG.SLOT', $prog_data{SLOT}) };
		if ($@)
		{
			__print_error('REGFGC3.PROG.SLOT, $@');
		}

		eval{ set($converter, 'REGFGC3.PROG.DEVICE', $prog_data{DEVICE}) };
		if ($@)
		{
			__print_error('REGFGC3.PROG.DEVICE, $@');
		}

		eval{ set($converter, "REGFGC3.PROG.VARIANT", $prog_data{VARIANT}) };
		if ($@)
		{
			__print_error("REGFGC3.PROG.VARIANT, $@");
		}

		eval{ set($converter, "REGFGC3.PROG.API_VERSION", $prog_data{APIVERSION}) };
		if ($@)
		{
			__print_error("REGFGC3.PROG.API_VERSION, $@");
		}

		eval{ set($converter, "REGFGC3.PROG.REVISION", $prog_data{REVISION}) };
		if ($@)
		{
			__print_error("REGFGC3.PROG.REVISION, $@");
		}

		eval{ set($converter, "REGFGC3.PROG.BIN_SIZE", $prog_data{BIN_SIZE}) };
		if ($@)
		{
			__print_error("REGFGC3.PROG.BIN_SIZE, $@");
		}	

		eval { set($converter, 'REGFGC3.PROG.FSM.MODE', 'TRANSFER')};
		if ($@)
		{
			__print_error('REGFGC3.PROG.FSM.MODE, $@');
		}

		eval { __transfer_binary($converter, $prog_data{BIN_DIR}, $prog_data{BINARY}) };
		if ($@)
		{	
			print "ERROR: transfer binary to the FGC's DSP memory $@!\n";
			next; 
		}

		eval { set($converter, 'REGFGC3.PROG.FSM.MODE', 'PROGRAMMING')};
		if ($@)
		{
			__print_error('REGFGC3.PROG.FSM.MODE, $@');
		}
		
	}

}



sub usage
{
	confess "\nUsage: $0 <slot_number> <card_name> <variant_name> <card_DEVICE> <deployment_option> [fw_repo_dir]
	<deployment_option> can be: 
	-a deployment_area
	-g gateway
	-c converter\n"; 
}



# End of functions




# Check input arguments

sub main($$$$$$;$)
{

	my ($slot, $card, $variant, $device, $deployment_option, $deployment_object, $fw_dir) = @ARGV; 

	# Check input arguments make sense

	my %cards_hash      = map {$_ => 1} (CARDS)[0 .. CARDS_NUMBER-1];
	my %variants_hash	= map {$_ => 1} (VARIANTS)[0 .. VARIANTS_NUMBER-1];
	my %devices_hash = map {$_ => 1} (DEVICES)[0 .. DEVICES_NUMBER-1]; 


	confess "Slot number not valid! Valid numbers 1..31\n" 	if ($slot > 31 or $slot < 1);
	confess "Card $card does not exist!\n" 					if (not exists $cards_hash{uc $card});
	confess "Variant $variant does not exist!\n" 			if (not exists $variants_hash{uc $variant});
	confess "DEVICE $device does not exist!\n" 				if (not exists $devices_hash{uc $device});


	# Check directory and file exists

	my $binary_file_dir_prefix = defined($fw_dir) ? $fw_dir : FW_REPO;
	print "fw_repo dir is: $binary_file_dir_prefix\n";
	my $binary_file_dir  = join('/', $binary_file_dir_prefix, uc $card, uc $device, uc $variant);


	opendir(my $dh, $binary_file_dir) or die "Can't open FW repository directory $binary_file_dir $!";

	my @sorted_dir = sort( readdir($dh) );
	my @binary_files  = grep( /.+[\.bin, \.bit,\.mcs]$/, @sorted_dir );
	my $binary_file_name = $binary_files[-1]; 

	closedir($dh);



	# Get list of affected converters
	my $device_list_ref = __get_device_list($deployment_option, $deployment_object);


	my %programming_data = (
		CONVERTERS => $device_list_ref,
		SLOT       => $slot,
		CARD       => uc $card,
		VARIANT    => uc $variant,
		DEVICE     => uc $device,
		BIN_DIR    => $binary_file_dir,
		BINARY     => $binary_file_name, 
		BIN_SIZE   => 0
	);


	($programming_data{APIVERSION}, 
		$programming_data{REVISION}, 
		$programming_data{BIN_SIZE}) = __check_binary($binary_file_dir, $programming_data{BINARY}, $programming_data{VARIANT});

	print "binary file size: $programming_data{BIN_SIZE}\n";
	# Confirm user action

	my $caps_card = uc $card; 

	print color('bold red');
	print "\n*** ATTENTION *** \nFile $binary_file_name (variant $variant) will be programmed in card $caps_card on the following converters: \n\n";
	print color('reset');

	__print_converter_list($device_list_ref);


	print "Confirm action [Y/n]? ";
	my $answer = <STDIN>;
	my $valid_answer = 0;

	while ($valid_answer == 0)
	{
		if ($answer =~ /^Y$/)
		{
			$valid_answer = 1; 
			program_cards_v1(\%programming_data);
			
			exit; 
		}
		elsif ($answer =~ /^n$/)
		{
			$valid_answer = 1; 
			print "Cards will NOT be programmed\n";
			exit; 
		}
		else
		{
			print "Not valid option\n";
		}

		$answer = <STDIN>;
	}
}

my ($slot, $card, $variant, $device, $deployment_option, $deployment_object, $fw_dir) = @ARGV; 

if ( not(defined $slot and defined $card and defined $variant and defined $device and defined $deployment_option and defined $deployment_object) )
{
	usage(); 
}

main($slot, $card, $variant, $device, $deployment_option, $deployment_object, $fw_dir) if not caller();





