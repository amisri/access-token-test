#!/usr/bin/perl -w
#
# Name:     fgc_monitor.pl
# Purpose:  Monitor all FGCs based upon conditions specified in files and send notification mails
# Author:   Stephen Page

use FGC::Async;
use FGC::DeviceList;
use FGC::Names;
use FGC::RBAC;
use File::Basename;
use Mail::Sendmail;
use strict;
use Sys::Hostname;

my $condition_path;
my $device_list_path;

# Read condition from file

sub read_condition($$$$)
{
    my ($filename, $class, $conditions, $properties) = @_;
    my %condition;
    my @comments;

    open(FILE, "<", $filename)
        or warn "\t\tFailed to open file $filename: $!\n" and return(undef);

    while(<FILE>)
    {
        # Remove newline

        s/[\012\015]//g;

        # Ignore empty lines and comments

        next if(/^\s*$/);

        # Add comments

        if (/^\s*#/)
        {
            s/#\s*//g;
            push(@comments,$_);
            next;
        }

        # Read addresses from file

        if(!defined($condition{addresses}))
        {
            my $addresses = $_;
            my @addresses = split(/\s+/, $addresses);

            $condition{addresses}       = \@addresses;
            $condition{class}           = $class;
            $condition{filename}        = $filename;
            $condition{base_filename}   = basename($filename);
            next;
        }

        # Read the post-condition commands (i.e. commands to be executed if the condition is full filled)

        if (/^\s*!\s*[sg]\s+/i)
        {
             s/^\s*!\s*//g;

             if (!defined($condition{post}))
             {
                  my @actions;
                  $condition{post} = \@actions;
             }

             push(@{$condition{post}},$_);

             next;
        }

        # Read condition rules from file

        my %rule;
        $rule{rule}         = $_;
        $rule{condition}    = \%condition;
        (
            $rule{property},
            $rule{type},
            $rule{expression},
        ) = split(/\s+/, $rule{rule});

        # Check that expression was defined

        if(!defined($rule{expression}) || $rule{expression} =~ /^$/)
        {
            warn "\t\tExpression not defined in line \"$_\".  Skipping rule\n";
            next;
        }

        $rule{property} = "\U$rule{property}";

        # Add property to properties hash

        $properties->{$rule{property}}->{name} = $rule{property};

        push(@{$condition{rules}}, \%rule);
    }
    close(FILE);

    $condition{comments} = \@comments;

    if(!defined($condition{addresses}))
    {
        warn "\t\tAddresses not correctly defined in file $filename.  Skipping file.\n";
        return(undef);
    }

    push(@$conditions, \%condition);

}

# Test whether a value is numeric

sub is_numeric($)
{
    my ($value) = @_;

    return($value =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/ ? 1 : 0); # Value is numeric
}

# Find values exactly equaling or not equaling expression

sub equal_expression($$)
{
    my ($rule, $devices) = @_;

    # Check whether expression is numeric

    my $numeric = is_numeric($rule->{expression});

    for my $device (values(%$devices))
    {
        my $value = $device->{properties}->{$rule->{property}};
        delete($devices->{$device->{name}}), next if(!defined($value));

        # Check whether expression is numeric

        if($numeric) # Expression is numeric
        {
            if(!(
                is_numeric($value)                                          &&
                (($rule->{type} eq  "=" && $value == $rule->{expression})   ||
                 ($rule->{type} eq "!=" && $value != $rule->{expression}))
              ))
            {
                delete($devices->{$device->{name}});
            }
        }
        else # Expression is not numeric
        {
            if(!(
                ($rule->{type} eq  "=" && $value eq $rule->{expression}) ||
                ($rule->{type} eq "!=" && $value ne $rule->{expression})
              ))
            {
                delete($devices->{$device->{name}});
            }
        }
    }
}

# Find values matching regular expression

sub regex_expression($$)
{
    my ($rule, $devices) = @_;

    for my $device (values(%$devices))
    {
        my $value = $device->{properties}->{$rule->{property}};
        delete($devices->{$device->{name}}), next if(!defined($value));

        eval
        {
            if(!(
                ($rule->{type} eq "=~" && $value =~ $rule->{expression}) ||
                ($rule->{type} eq "!~" && $value !~ $rule->{expression})
              ))
            {
                delete($devices->{$device->{name}});
            }
        };

        # Check whether an error occurred

        if($@)
        {
            warn "\t\tBad regular expression \"$rule->{expression}\"\n";
            last;
        }
    }
}

# Find values greater than or less than

sub gtlt_expression($$)
{
    my ($rule, $devices) = @_;

    # Check whether expression is numeric

    if(!is_numeric($rule->{expression}))
    {
        warn("\t\tExpression \"$rule->{expression}\" is not numeric\n");
        return;
    }

    for my $device (values(%$devices))
    {
        my $value = $device->{properties}->{$rule->{property}};
        delete($devices->{$device->{name}}), next if(!defined($value));
        delete($devices->{$device->{name}}), next if(!is_numeric($value));

        if(!(
            ($rule->{type} eq ">"  && $value        >  $rule->{expression})         ||
            ($rule->{type} eq ">=" && $value        >= $rule->{expression})         ||
            ($rule->{type} eq "|>" && abs($value)   >  abs($rule->{expression}))    ||
            ($rule->{type} eq "<"  && $value        <  $rule->{expression})         ||
            ($rule->{type} eq "<=" && $value        <= $rule->{expression})         ||
            ($rule->{type} eq "|<" && abs($value)   <  abs($rule->{expression}))
          ))
        {
            delete($devices->{$device->{name}});
        }
    }
}

# Report devices

sub report_devices($$)
{
    my ($condition, $devices) = @_;

    mail_devices($condition, $devices);
}

# Make a device list

sub make_device_list($$)
{
    my ($condition, $devices) = @_;

    my $filename                = "$device_list_path/$condition->{class}/$condition->{base_filename}";
    $condition->{device_list}   = $filename;

    # Execute post-condition action

    for my $action ( @{ $condition->{post} } )
    {
        my ($action_type,$property,@values) = split(/\s+/,$action);

        for my $device (values (%$devices))
        {
            if ($action_type =~ /g/i)
            {
                # Get action

                eval{FGC::Async::get($device, $property . " " . join(" ",@values))};
            }
            elsif ($action_type =~ /s/i)
            {
                # Set action

                eval{FGC::Async::set($device, $property, join(" ",@values))};
            }
            else
            {
                warn("\t\tUnknown action type: $action")
            }
        }
    }

    # Build array of devices to include in device list

    my @list_devices;
    for my $device (sort { $a->{name} cmp $b->{name} } values(%$devices))
    {
        # Set response to that of last property read and replace new lines with '$' characters

        my $response        =  $device->{properties}->{$condition->{rules}->[-1]->{property}};
        $response           =~ s/[\012\015]+/\$/g;
        $device->{response} = $response;

        # Add device to list devices

        push(@list_devices, $device);
    }

    # Write file

    if(!FGC::DeviceList::write(\@list_devices, $filename))
    {
        warn "\t\tFailed to write device list file $filename: $!\n";
        return(undef);
    }

    # Remove responses from device entries

    delete($_->{response}) for(@list_devices);
}

# Mail list of devices

sub mail_devices($$)
{
    my ($condition, $devices) = @_;

    my %mail = (
                    "Content-type"  => "text/html",
                    From            => "fgc.admin\@cern.ch",
                    To              => join(", ", @{$condition->{addresses}}),
                    Subject         => "FGC Monitor report for $condition->{base_filename} (FGC_$condition->{class})",
               );

    # Construct HTML e-mail message

    $mail{Message} =    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n".
                        "<HTML>\n".
                        "<HEAD>\n".
                        "<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n".
                        "<TITLE>FGC Monitor report</TITLE>\n".
                        "</HEAD>\n".
                        "<BODY>\n";

    # Add comments

    $mail{Message} .= "<P>\n";
    for my $comment (@{$condition->{comments}})
    {
        $mail{Message} .= "$comment<BR>\n";
    }
    $mail{Message} .= "</P>\n";

    # Add rules

    $mail{Message} .=   "<P>Rules:</P>\n".
                        "<P>\n".
                        "<CODE>\n";

    # Add rules to message

    for my $rule (@{$condition->{rules}})
    {
        $mail{Message} .= "$rule->{rule}<BR>\n";
    }
    $mail{Message} .=   "</CODE>\n".
                        "</P>\n";

    # Add list of devices to message

    $mail{Message} .=   "<P>The following ".scalar(keys(%$devices))." devices matched the condition:</P>\n".
                        "<P>\n".
                        "<CODE>\n";

    for my $device (sort { $a->{name} cmp $b->{name} } values(%$devices))
    {
        #example: http://cern.ch/fgc-device-status/statusdev.html?cfc-sr1-reth1&CFC-SR1-RETH1&status&statustbl_h.html&0&CFC-SR1-RETH1
        $mail{Message} .= "<A href=\"http://cern.ch/fgc-device-status/statusdev.html".
                          "?$device->{gateway}->{name}".
                          "&$device->{gateway}->{channels}->[0]->{name}".
                          "&status&statustbl_h.html".
                          "&$device->{channel}\">".
                          "$device->{name}".
                          "</A>".
                          " = $device->{properties}->{$condition->{rules}->[-1]->{property}}<BR>\n";
    }
    $mail{Message} .=   "</CODE>\n".
                        "</P>\n";

    # Add name of device list to message

    $mail{Message} .=   "<P>FGCRun+ device list stored in $condition->{device_list}</P>\n".
                        "</BODY>\n".
                        "</HTML>\n";

    # Send mail to supplied addresses

    print   "\t\tCondition from file $condition->{base_filename} matched, sending mail to ",
            join(", ", @{$condition->{addresses}}),
            "...\n";

    if(!sendmail(%mail)) # Mail failed to send
    {
        warn "\t\tFailed to send mail notification\n";
        return;
    }
}

# End of functions


die "\nUsage: $0 <condition dir path> <device list dir path>\n\n" if(@ARGV != 2);
($condition_path, $device_list_path) = @ARGV;

# Read FGC name file

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Initialise FGC asynchronous communication module

FGC::Async::init($devices, $gateways);

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate_by_location()
    or die "RBAC authentication failed\n";

# Connect to all gateways

for my $gateway (values(%$gateways))
{
    eval { FGC::Async::connect($gateway) };
    if($@)
    {
        warn "Unable to connect to gateway $gateway->{name}\n";
        next;
    }

    # Set RBAC token on gateway

    eval { FGC::Async::set($gateway->{channels}->[0], "CLIENT.TOKEN", \$rbac_token, 1) };
    warn "Failed to send command to set RBAC token on gateway $gateway->{name}\n" if($@);
}

# Read responses to RBAC token set commands

my $commands = FGC::Async::read();
for my $command (values(%$commands))
{
    if($command->{response}->{error})
    {
        warn "Error setting RBAC token on gateway $command->{device}->{name}: $command->{response}->{value}\n";
    }
}

# Handle each class

for my $class_path (<$condition_path/*>)
{
    (my $class = $class_path) =~ s/.*\/(.*?)/$1/;

    print "Handling files for class $class...\n";

    # Read each condition file

    my @conditions;
    my %properties;

    for my $filename (<$class_path/*>)
    {
        print "\tReading conditions from file $filename...\n";

        read_condition($filename, $class, \@conditions, \%properties);
    }

    # Send get command for each property to each device of class

    print "\tGetting properties from devices...\n";

    for my $property (values(%properties))
    {
        print "\t\tGetting $property->{name}...\n";
        for my $device (grep { $_->{class} == $class } values(%$devices))
        {
            next if(!defined($device->{gateway}->{socket}));

            my $command = $property->{name};

            # Send command to device

            eval { FGC::Async::get($device, $command) };

            # Check whether an error occurred

            if($@) # An error occurred
            {
                warn "\t\tError getting property $property->{name} from $device->{name}: $@\n";
                next;
            }
        }
    }

    # Read responses

    print "\tReading responses...\n";

    my $commands = FGC::Async::read();

    print "\tRead responses.\n";

    for my $command (values(%$commands))
    {
        # Strip get options from command property

        $command->{property} =~ s/ .*//;

        # Check whether an error occurred

        if($command->{response}->{error})
        {
            if($command->{response}->{value} !~ /^\d+ dev not ready$/)
            {
                warn "\t\tError getting property $command->{property} from $command->{device}->{name}: $command->{response}->{value}\n";
            }
            next;
        }

        # Store response

        $command->{device}->{properties}->{$command->{property}} = $command->{response}->{value};
    }

    # Check conditions

    for my $condition (@conditions)
    {
        print "\tChecking rules from condition file $condition->{filename}...\n";

        my %matching_devices;
        for my $device (grep { $_->{class} == $class } (values(%$devices)))
        {
            $matching_devices{$device->{name}} = $device;
        }

        for my $rule (@{$condition->{rules}})
        {
            print "\t\tChecking rule \"$rule->{rule}\"...\n";

            if($rule->{type} eq "=" ||
               $rule->{type} eq "!=")
            {
                equal_expression($rule, \%matching_devices);
            }
            elsif($rule->{type} eq "=~" ||
                  $rule->{type} eq "!~")
            {
                regex_expression($rule, \%matching_devices);
            }
            elsif($rule->{type} eq ">"  ||
                  $rule->{type} eq ">=" ||
                  $rule->{type} eq "|>" ||
                  $rule->{type} eq "<"  ||
                  $rule->{type} eq "<=" ||
                  $rule->{type} eq "|<")
            {
                gtlt_expression( $rule, \%matching_devices);
            }
            last if(!scalar(keys(%matching_devices)));
        }
        make_device_list($condition, \%matching_devices);
        next if(!scalar(keys(%matching_devices)));

        print "\t\t", scalar(keys(%matching_devices)), " matches found\n";
        report_devices($condition, \%matching_devices);
        print "\n";
    }
}

# Disconnect from all gateways

FGC::Async::disconnect($_) for values(%$gateways);

# EOF
