#!/bin/bash

# truncate files

LOG_PATH=/user/pclhc/var/log/sps_glitch_trigger
touch $LOG_PATH/sps_glitch_trigger_qd.log
touch $LOG_PATH/sps_glitch_trigger_qf.log
touch $LOG_PATH/sps_glitch_trigger_mb.log

# Remove old content from current log file

cp $LOG_PATH/sps_glitch_trigger_qd.log $LOG_PATH/sps_glitch_trigger_qd.log.tmp
cp $LOG_PATH/sps_glitch_trigger_qf.log $LOG_PATH/sps_glitch_trigger_qf.log.tmp
cp $LOG_PATH/sps_glitch_trigger_mb.log $LOG_PATH/sps_glitch_trigger_mb.log.tmp

tail -50000 $LOG_PATH/sps_glitch_trigger_qd.log.tmp > $LOG_PATH/sps_glitch_trigger_qd.log
tail -50000 $LOG_PATH/sps_glitch_trigger_qf.log.old > $LOG_PATH/sps_glitch_trigger_qf.log
tail -50000 $LOG_PATH/sps_glitch_trigger_mb.log.old > $LOG_PATH/sps_glitch_trigger_mb.log


# Remove old content from current log file

cp $LOG_PATH/sps_glitch_trigger_qd.log $LOG_PATH/sps_glitch_trigger_qd.log.tmp
cp $LOG_PATH/sps_glitch_trigger_qf.log $LOG_PATH/sps_glitch_trigger_qf.log.tmp

tail -50000 $LOG_PATH/sps_glitch_trigger_qd.log.tmp > $LOG_PATH/sps_glitch_trigger_qd.log
tail -50000 $LOG_PATH/sps_glitch_trigger_qf.log.old > $LOG_PATH/sps_glitch_trigger_qf.log


export PERL5LIB=/user/pclhc/lib/perl:$PERL5LIB

function test_pid
{
    kill -0 $@ &> /dev/null

    if [ "$?" -eq "0" ]; then
         return 1
    else
        return 0
    fi
}

while true; do

    # Append to both the current file and the history file

    unbuffer ./sps_glitch_trigger.pl RPPAG.BA3.QF 2>&1 | tee -a $LOG_PATH/sps_glitch_trigger_qf.log | tee -a $LOG_PATH/sps_glitch_trigger_qf.log.old &

    cmd_pid1=$!

    # Append to both the current file and the history file

    unbuffer ./sps_glitch_trigger.pl RPPAG.BA3.QD 2>&1 | tee -a $LOG_PATH/sps_glitch_trigger_qd.log | tee -a $LOG_PATH/sps_glitch_trigger_qd.log.old &

    cmd_pid2=$!

    # Append to both the current file and the history file

    unbuffer ./sps_glitch_trigger.pl RPPEL.BA3.MBI 2>&1 | tee -a $LOG_PATH/sps_glitch_trigger_mb.log | tee -a $LOG_PATH/sps_glitch_trigger_mb.log.old &

    cmd_pid3=$!

    # Terminate script on exit

    trap "kill -9 $cmd_pid1; kill -9 $cmd_pid2;" EXIT

    while ! test_pid $cmd_pid1 && ! test_pid $cmd_pid2 ! test_pid $cmd_pid3 ; do
        sleep 10
    done

    echo "Restarting..."

    kill -9 $cmd_pid1 &> /dev/null
    kill -9 $cmd_pid2 &> /dev/null
    kill -9 $cmd_pid3 &> /dev/null
done;

#EOF
