#!/usr/bin/perl
use POSIX qw(strftime);
use Test::TCP;
use FGC::Log;
use Time::HiRes qw(clock_gettime CLOCK_REALTIME);

use strict;

my $MAX_D2IDT = 0.1;
my $MAX_DIDT  = 0;
my $I_MIN     = 1000;

sub max ($$) { $_[ $_[0] < $_[1] ] }

sub min ($$) { $_[ $_[0] > $_[1] ] }

sub gradient($)
{
    my ($x) = @_;

    my @grad;

    for ( my $i = 2 ; $i <= $#$x ; $i++ )
    {
        push( @grad, 1 / 2.0 * ( ( $x->[$i] - $x->[ $i - 2 ] ) ) );
    }

    return \@grad;

}

sub detect($$$$%)
{
    my ( $device, $times, $meas, $user, $supercycle ) = @_;

    my $d        = gradient($meas);
    my $dd       = gradient($d);
    my $avg_didt = 0;

    for ( my $i = 0 ; $i <= $#$dd - 2 ; $i++ )
    {

        # Moving average for 10 samples

        $avg_didt = 1 / 10 * $d->[ $i + 1 ] + ( 1 - 1 / 10 ) * $avg_didt;

        # Reject measurements below I_MIN

        if ( defined($I_MIN) && $meas->[ $i + 2 ] < $I_MIN )
        {
            next;
        }

        # Reject measurements when ramping down

        if ( defined($MAX_DIDT) && $avg_didt < -$MAX_DIDT )
        {
            next;
        }

        # Trigger if the d2I is above MAX_D2IDT

        my $di = $dd->[$i];

        if ( abs($di) > $MAX_D2IDT )
        {

            my $date = strftime( '%Y/%m/%d %H:%M:%S', localtime( time() ) );
            print "$date WARN $device glitch detected (user=$user, i=$i, t=$times->[$i], I=$meas->[$i+2], dI=$d->[$i+1], d2I=$dd->[$i])\n";

            my $deltaT = int( ( clock_gettime(CLOCK_REALTIME) - $times->[$i] ) * 1000 );

            # If user=0, the timestamps are wrong, so we can not calculate the deltaT
            # see also https://issues.cern.ch/browse/EPCCCS-4177

            if (defined($user))
            {
                print "$date INFO Time between the glitch and trigger = $deltaT ms\n";
            }

            if ( $device =~ /QF/ )
            {

                # Software trigger requires USB+FTDI to BNC cable and the FTDI drivers on the remote computer

                #my $cmd = 'ssh lapte23337 "printf \"\\\\0\" > /dev/cu.usbserial-FTVY6SVN"';

                # Send trigger to BA3

                my $cmd = 'ssh root@lapte23337 "printf \"\\\\0\" > /dev/ttyUSB0"';
                system($cmd);
                if ($?)
                {
                    print "$date ERROR USB trigger command failed on lapte23337 '$cmd': $?\n";
                }

            }

            print "$date INFO Supercycle structure (glitch user = $user): ";
            foreach my $key (sort keys %$supercycle)
            {
                print "$supercycle->{$key} "
            }
            print "\n";

            print "$date INFO printing glitch context...\n";
            print "\tindex [ms], I_MEAS [A], diff(I_MEAS) [A], diff(diff(I_MEAS)) [A]\n";
            for (my $j = max( 0, $i - 10 ) ;$j <= min( $#$dd, $i + 10 ) ; $j++)
            {
                print "\t$j, $meas->[$j],$d->[$j],$dd->[$j]\n";
            }
            return;
        }
    }
}

sub log_subscribe($)
{
    my ($device) = @_;

    authenticate('location');
    subscribe( $device, 10 );

    # Parse output

    my $acqStamp;
    my $acqStamp1;
    my $acqStamp2;
    my $value;     # value
    my $value1;    # value[n-1]
    my $value2;    # value[n-2]
    my $didt;
    my $didt1;
    my $didt2;
    my $d2idt;
    my %supercycle;

    while (1)
    {
        my $prev_cycle_num;
        my $current_cycle_num;
        my $current_user;
        my $prev_user;

        # Retrieve Mugef buffer

        my $buffer;

        # Wait for new cycle (if user is specified)

        while (1)
        {
            if (get($device,"STATE.PC") =~ /IDLE/)
            {
                 # COAST receivied from the timing system check for glitch every 10 seconds

                 $prev_user = undef;
                 %supercycle = undef;

                 sleep(5);

                 # Get just the last 10 seconds and exit the loop

                 $buffer = get( $device, "LOG.MUGEF.I[54000,59999] bin");

                 last;
            }
            else
            {
                 # Otherwise wait for change of cycle

                 $prev_cycle_num = $current_cycle_num;
                 $prev_user      = $current_user;

                 # Retrieve publication data

                 my $publication = get_next_publication($device);
                 $publication =~ /CYCLE_USER:(\d+)/;
                 $current_user = $1;
                 $publication =~ /CYCLE_NUM:(\d+)/;
                 $current_cycle_num = $1;

                 # Reconstruct supercycle information

                 $supercycle{$current_cycle_num} = $current_user;

                 next if ( !defined($prev_cycle_num) );

                 # Exit the loop if this is a new cycle

                 if ( $prev_cycle_num != $current_cycle_num )
                 {
                     $buffer = get( $device, "LOG.MUGEF.I bin", $prev_user );
                     last;
                 }
             }
        }

        # Retrieve buffer

        my $log = FGC::Log::decode( \$buffer );

        my $signals = $log->{signals};

        my @times;
        my @meas;

        for (my $sample_index = 0 ; $sample_index < $log->{header}->{n_samples} ;$sample_index++)
        {

            # Write timestamp

            # It ommits offsets calculated individually for each signal and takes offsets from first signal

            my $time = $signals->[0]->{samples}->[$sample_index]->{time};

            my $t = $time->{sec} + $time->{usec} / 1000000.0;
            push( @times, $t );

            my $m = $signals->[1]->{samples}->[$sample_index]->{value} || 0;
            push( @meas, $m );
        }

        # Detect glitch

        #my $date        = strftime('%Y/%m/%d %H:%M:%S',localtime(time()));
        #print "$date INFO Checking new cycle (user = $prev_user, length = (length $log->{header}->{n_samples} samples)...\n";

        if ( $current_user != 19 && $current_user != 20 )
        {
            detect( $device, \@times, \@meas, $prev_user, \%supercycle );
        }
    }

}

my $nargs = $#ARGV + 1;
if ( $nargs != 1 ) {
    print "\nUsage: $0 <device>\n\n";
    print "The program detects a glitch (i.e. d2I/dt2 > $MAX_D2IDT), and triggers the scope using the serial interface\n\n";
    exit;
}

my $device = $ARGV[0];

print "Monitoring $device/LOG.MUGEF.I...\n";

log_subscribe($device);
