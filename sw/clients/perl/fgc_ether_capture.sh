#!/bin/sh
#
# Name:     fgc_ether_capture.sh
# Purpose:  Capture frames on an FGC_Ether gateway
# Author:   Stephen Page

cpu=$(uname -m)
os=$(uname -s)

wireshark="/user/pclhc/bin/$os/$cpu/fgc_ether-wireshark"

# Handle arguments

if [ $# -lt 1 ]; then
    echo -en "\nUsage: $0 <host name> [<filter>]\n\n" 1>&2
    exit 1
fi

host_name=$1
filter=$2

# Create a temporary file

filename=$(mktemp)

# Remove file if killed

trap "rm \"$filename\"; exit" SIGHUP SIGINT SIGTERM

# Prompt for number of frames

read -p "Enter the number of frames to capture [500]: " num_frames

if [ -z "$num_frames" ]; then
    num_frames=500
fi

# Capture frames on the gateway and display them in wireshark

echo -e "\ntcpdump -p -ieth1 -s0 -w- -c\"$num_frames\" \"$filter\""
(ssh -t "$host_name" "stty -onlcr; sudo tcpdump -p -ieth1 -s0 -w- -c\"$num_frames\" \"$filter\" 2>/dev/null" >"$filename" && \
    "$wireshark" "$filename") || sleep 5

rm -f "$filename"

# EOF
