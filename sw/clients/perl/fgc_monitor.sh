#!/bin/sh

# Import environment

. /user/pclhc/.profile

# Run FGC Monitor

/user/pclhc/bin/perl/fgc_monitor.pl "$@"

# EOF
