#!/usr/bin/perl -w
#
# Filename: FGC/LSADB.pm
# Purpose:  Access functions for the LSA Database
# Author:   Stephen Page

package FGC::LSADB;

use DBI;
use Exporter;
use strict;

use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        connect
                        disconnect
                        empty_soc
                        get_converter_parameter
                        get_groups
                        get_group_devices
                        get_hw_groups
                        get_hw_group_devices
                        get_rf_channels
                        get_soc_converters
                        get_socs
                        set_soc_converters
                    );

# Constants

use constant FGC_LSA_DB_ID          => 'dbi:Oracle:acc_settings_db';    # ID of LSA database
use constant FGC_LSA_DB_PASSWORD    => 'l1m1t3dhwcacc355';              # Public password for LSA database
use constant FGC_LSA_DB_USERNAME    => 'lsa_hwc_apps';                  # Public username for LSA database

my $connect_count = 0;

return(1);


# Begin functions

# Connect to database

sub connect(;$$)
{
    my ($username, $password) = @_;

    $username ||= FGC_LSA_DB_USERNAME;
    $password ||= FGC_LSA_DB_PASSWORD;

    # Connect to database

    my $dbh = DBI->connect(FGC_LSA_DB_ID,
                           FGC_LSA_DB_USERNAME,
                           FGC_LSA_DB_PASSWORD,
                           {PrintError => 0, RaiseError => 0}) or return(undef);
    $connect_count++;

    return($dbh);
}

# Disconnect from database

sub disconnect($)
{
    my ($dbh) = @_;

    # Disconnect from database

    $dbh->disconnect;
}

# Get hardware groups

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_hw_groups($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT UNIQUE ACTUAL_HW_GROUP
                                                     FROM LSA.V_ACTUAL_HARDWARE_GRP_DEVICES
                                                     WHERE ACTUAL_HARDWARE LIKE 'RP%'
                                                     ORDER BY ACTUAL_HW_GROUP") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ACTUAL_HW_GROUP => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        my $results     = $query->fetchall_arrayref(\%result_hash);
        my @hw_groups   = map { $_->{ACTUAL_HW_GROUP} } @$results;

        # Return hardware groups

        return(\@hw_groups);
    }
}

# Get devices in hardware group

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_hw_group_devices($$)
    {
        my ($dbh, $hw_group) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT ACTUAL_HARDWARE
                                                     FROM LSA.V_ACTUAL_HARDWARE_GRP_DEVICES
                                                     WHERE ACTUAL_HW_GROUP=? AND ACTUAL_HARDWARE LIKE 'RP%'
                                                     ORDER BY ACTUAL_HARDWARE") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ACTUAL_HARDWARE => 1,
                                        );
        }

        # Execute query

        $query->execute($hw_group) or return(undef);

        # Return result

        my $result          = $query->fetchall_arrayref(\%result_hash);
        my @device_names    = map { $_->{ACTUAL_HARDWARE} } @$result;
        return(\@device_names);
    }
}

# Get sets of circuits

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_socs($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT SOC_NAME, SOC_DESCRIPTION, IS_OPERATIONAL
                                                     FROM LSA.HWC_SOCS") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            SOC_NAME        => 1,
                                            SOC_DESCRIPTION => 1,
                                            IS_OPERATIONAL  => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        my $results = $query->fetchall_arrayref(\%result_hash);
        my %socs    = map { $_->{SOC_NAME} => $_ } @$results;

        # Return SOCS

        return(\%socs);
    }
}

# Get converters in set of circuits

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_soc_converters($$)
    {
        my ($dbh, $soc) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT POWERCONVERTER_NAME
                                                     FROM LSA.V_HWC_SOC_POWERCONVERTERS
                                                     WHERE SOC_NAME=?
                                                     ORDER BY POWERCONVERTER_NAME") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            POWERCONVERTER_NAME    => 1,
                                        );
        }

        # Execute query

        $query->execute($soc) or return(undef);

        # Return result

        my $result          = $query->fetchall_arrayref(\%result_hash);
        my @converter_names = map { $_->{POWERCONVERTER_NAME} } @$result;
        return(\@converter_names);
    }
}

# Set converters in set of circuits

{
    my $query;
    my $query_connect_count;

    sub set_soc_converters($$$)
    {
        my ($dbh, $soc_name, $converters) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO LSA.HWC_SOC_CIRCUITS
                                                     (SOC_NAME, CIRCUIT_ID)
                                                     VALUES (?, (SELECT CIRCUIT_ID FROM LSA.V_CIRCUIT_POWERCONVERTERS WHERE POWERCONVERTER_NAME=?))") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Remove existing devices from set of circuits

        empty_soc($dbh, $soc_name) or return(undef);

        # Add each converter

        for my $converter (@$converters)
        {
            # Execute query

            $query->execute($soc_name, $converter);
        }
    }
}

# Empty set of circuits

{
    my $query;
    my $query_connect_count;

    sub empty_soc($$)
    {
        my ($dbh, $soc_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("DELETE FROM LSA.HWC_SOC_CIRCUITS
                                                     WHERE SOC_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        $query->execute($soc_name) or return(undef);
    }
}

# Get groups

sub get_groups($$)
{
    my ($dbh, $type) = @_;

    my %groups;

    if($type eq "SOC")
    {
        my $socs = get_socs($dbh);

        for my $soc (values(%$socs))
        {
            my %group = (
                            description => $soc->{SOC_DESCRIPTION},
                            name        => $soc->{SOC_NAME},
                            operational => $soc->{IS_OPERATIONAL} eq 'Y' ? 1 : 0,
                        );

            $groups{$group{name}} = \%group;
        }
    }
    elsif($type eq "HW_Group")
    {
        my $hw_groups = get_hw_groups($dbh);

        for my $hw_group (@$hw_groups)
        {
            my %group = (
                            description => undef,
                            name        => $hw_group,
                            operational => 0,
                        );

            $groups{$group{name}} = \%group;
        }
    }
    else # Unknown group type
    {
        return(undef);
    }
    return(\%groups);
}

# Get devices in a group

sub get_group_devices($$$)
{
    my ($dbh, $type, $group) = @_;

    my $devices;

    if($type eq "SOC")
    {
        $devices = get_soc_converters($dbh, $group);
    }
    elsif($type eq "HW_Group")
    {
        $devices = get_hw_group_devices($dbh, $group);
    }
    else # Unknown group type
    {
        return(undef);
    }
    return($devices);
}

# Get RF Function Generator channel definitions

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_rf_channels($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT *
                                                     FROM LHC_RF_FGEN_INFO
                                                     ORDER BY FGC_NAME, CHANNEL") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            CHANNEL         => 1,
                                            CHANNEL_NAME    => 1,
                                            DESCRIPTION     => 1,
                                            FGC_NAME        => 1,
                                            MAX             => 1,
                                            MIN             => 1,
                                            UNITS           => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return result

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get a converter parameter

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_converter_parameter($$$)
    {
        my ($dbh, $device_name, $parameter) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT VALUE
                                                     FROM LSA.POWERCONVERTER_OP_PARAMETERS, LSA.DEVICES
                                                     WHERE PARAMETER_NAME=?
                                                     AND POWERCONVERTER_ID=DEVICE_ID
                                                     AND DEVICE_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            VALUE   => 1,
                                        );
        }

        # Execute query

        $query->execute($parameter, $device_name) or return(undef);

        # Return result

        my $result = $query->fetchall_arrayref(\%result_hash);
        return(defined($result->[0]) ? $result->[0]->{VALUE} : undef);
    }
}

# EOF
