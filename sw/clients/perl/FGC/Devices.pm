# Filename: FGC/Devices.pm
# Purpose:  Utility functions which operates on devices
# Authors:  Stephen Page, Krystian Wojtas

package FGC::Devices;

use List::Util qw(first);

sub build($)
{
    my ($device_names) = @_;

    # Read FGC name data

    my ($all_devices, $all_gateways) = FGC::Names::read();
    die "Unable to read FGC name file\n" if(!defined($all_devices));

    # Validate requested devices

    my $unknown_device =
        first
            { not defined $all_devices->{$_} }
            @$device_names;
    die "Unknown device $unknown_device\n" if $unknown_device;

    # Return list of interesting devices

    map
        { $_ => $all_devices->{$_} }
        @$device_names;
}

1;
