#!/usr/bin/perl -w
#
# Name:     FGC/Serial.pm
# Purpose:  Serial communication with an FGC
# Author:   Stephen Page

package FGC::Serial;

use bytes;
use Carp;
use Config;
use Exporter;
use strict;

# Use appropriate serial port module for platform

if($^O =~ /win/i) # Platform is Microsoft Windows
{
    require Win32::SerialPort;
}
else # Platform is not Microsoft Windows
{
    require Device::SerialPort;
}

use FGC::Class::Common;
use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        boot_cmd
                        connect
                        disconnect
                        get
                        read_pub
                        set
                    );

return(1);


# Begin functions

# Open port for connection to an FGC

sub connect($)
{
    my ($port_name) = @_;

    # Open serial port

    my $port;
    if($^O =~ /Win/) # Platform is Microsoft Windows
    {
        $port = new Win32::SerialPort($port_name)
            or die "Unable to open serial port $port_name\n";
    }
    else # Platform is not Microsoft Windows
    {
        $port = new Device::SerialPort($port_name)
            or die "Unable to open serial port $port_name\n";
    }

    # Configure serial port

    $port->user_msg("ON");
    $port->databits(8);
    $port->baudrate(9600);
    $port->parity("none");
    $port->stopbits(1);
    $port->handshake("rts");
    $port->buffers(4096, 4096);
    $port->read_const_time(5000);

    if($^O =~ /Win/) # Platform is Microsoft Windows
    {
        $port->read_interval(5000);
    }

    # Write settings to port

    if(!$port->write_settings) # Failed to write settings to port
    {
        $port->close();
        die "Unable to configure port\n";
    }

    # Switch to direct command mode and flush receive buffer

    $port->write("\032");
    $port->purge_rx();

    return($port);
}

# Disconnect from an FGC

sub disconnect($)
{
    my ($port) = @_;

    $port->close();
}

# Get a property

sub get($$)
{
    my ($port, $property) = @_;

    # Switch to direct command mode and flush receive buffer

    $port->write("\032");
    $port->purge_rx();

    # Send command to FGC

    defined($port->write("! g $property\r"))
        or die "Write to port failed: $!\n";

    # Wait for response

    my $c;
    my %response;

    $response{value} = "";

    while(defined(($c = $port->read(1))) && $c ne '')
    {
        $response{value} .= $c;

        # Check for end of response

        if($c eq ';' || $c eq '!')
        {
            $response{error}        =  $c eq '!' ? 1 : 0;

            $response{value}        =~ s/\A.*\$(.*)\012[;!]\z/$1/s;
            @{$response{values}}    =  split(",", $response{value});

            return(\%response);
        }
    }
    die "Read from port failed [$response{value}]: $!\n";
}

# Set a property

sub set($$$)
{
    my ($port, $property, $values) = @_;

    # Check whether $values is an array reference

    if(ref($values) eq "ARRAY")
    {
        # Convert values list to comma delimited string

        $values = join(',', @{$values});
    }

    # Switch to direct command mode and flush receive buffer

    $port->write("\032");
    $port->purge_rx();

    # Send command to FGC

    defined($port->write("! s $property $values\n"))
        or die "Write to port failed: $!\n";

    # Wait for response

    my $c;
    my %response;

    $response{value} = "";

    while(defined(($c = $port->read(1))) && $c ne '')
    {
        $response{value} .= $c;

        # Check for end of response

        if($c eq ';' || $c eq '!')
        {
            $response{error}    =  $c eq '!' ? 1 : 0;
            $response{value}    =~ s/\A.*\$(.*)\012[;!]\z/$1/s;

            return(\%response);
        }
    }
    die "Read from port failed: $!\n";
}

# Read published data

sub read_pub($)
{
    my ($port) = @_;

    # Switch to diag mode

    $port->write("\031");

    # Read data block

    my $buffer;
    defined(($buffer = $port->read(FGC_DIAG_N_SYNCH_BYTES + FGC_DIAG_SIZE)))
        or die "Read from port failed: $!\n";

    my $class;
    my $data_status;
    my $time_sec;
    my $time_usec;

    (
        $time_sec,
        $time_usec,
        $data_status,
        $class,
    ) = unpack("x".FGC_DIAG_N_SYNCH_BYTES."NNCC", $buffer);

    my $offset = FGC_DIAG_N_SYNCH_BYTES + 12; # Offset to class data

    # Decode class data

    my $device;
    if(defined($fgc_decode_func[$class]))
    {
        $device = $fgc_decode_func[$class](\$buffer, $offset, FGC_DIAG_PRECLASS_PAD);
    }
    else
    {
        $device = FGC::Class::Common::decode(\$buffer, $offset, FGC_DIAG_PRECLASS_PAD);
    }

    $device->{DATA_STATUS}  = $data_status;
    $device->{TIME_SEC}     = $time_sec;
    $device->{TIME_USEC}    = $time_usec;

    # Move offset past class data

    $offset += FGC_STATUS_SIZE;

    # Decode diagnostic data

    my $num_diag_samples    = FGC_DIAG_N_LISTS * FGC_DIAG_PERIOD;
    $device->{DIAG_CHANS}   = \[unpack("x${offset}C${num_diag_samples}", $buffer)];
    $offset                += $num_diag_samples;
    $device->{DIAG_DATA}    = \[unpack("x${offset}n${num_diag_samples}", $buffer)];
    $offset                += $num_diag_samples * 2;

    return($device);
}

# Read from port until a line-feed

sub read_line($)
{
    my ($port, $command) = @_;

    # Read from port until a line-feed character is received

    my $buffer;
    my $c;
    while(($c = $port->read(1)) ne "\012" && $c ne '')
    {
        # Append character to buffer

        $buffer .= $c;
    }

    if($c eq '')
    {
        die "Read from port failed: $!\n";
    }
    return($buffer);
}

# Send a command to the FGC boot program

sub boot_cmd($$)
{
    my ($port, $command) = @_;

    # Switch to direct command mode

    $port->write("\032");

    # Send command

    $port->write("\$$command\n");

    # Wait for response

    my %response;
    while(my $line = read_line($port))
    {
        # Skip line if it does not contain a "$"

        next if($line !~ /\$/);

        # Remove carriage return and/or line-feed

        $line =~ s/[\012\015]+$//;

        # Remove any characters before "$"

        $line =~ s/^.*\$/\$/;

        # Skip progress lines

        next if($line =~ /\$1/);

        # Push onto response value array

        push(@{$response{value}}, $line);

        # Check whether FGC has rebooted

        if($line =~ /^\$R/) # FGC has rebooted
        {
            # Throw away data until "$S"

            while(read_line($port) !~ /^\$S/) {}
        }

        # Stop if response is a "$3", "$4" or "$R"

        if($line =~ /^\$([34R])/)
        {
            if($1 eq '3')
            {
                $response{status} = 'OKAY';
            }
            elsif($1 eq '4')
            {
                $response{status} = 'ERROR';
            }
            elsif($1 eq 'R')
            {
                $response{status} = 'FGC REBOOTED';
            }
            else # Unexpected response
            {
                $response{status} = 'BAD RESPONSE';
            }
            last;
        }
    }

    return(\%response);
}

# EOF
