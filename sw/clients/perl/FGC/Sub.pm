#!/usr/bin/perl -w
#
# Name:     FGC/Sub.pm
# Purpose:  Receive published data
# Author:   Stephen Page

package FGC::Sub;

use bytes;
use Carp;
use Exporter;
use strict;

use FGC::Class::Common;
use FGC::Consts;
use IO::Select;
use Carp;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        decode
                        read
                    );
our $TIMEOUT = 60;

return(1);


# Begin functions

# Read published data

sub read($;$)
{
    my ($socket, $decode) = @_;
    $decode = 1 if(!defined($decode));

    my $sel = IO::Select->new($socket);

    # Check that connection is okay

    can_read($sel);
    
    my $buffer;
    my $sockaddr_in;
    $sockaddr_in = recv($socket, $buffer, 10240, 0);

    # Decode fgc_udp_header

    my (
        $id,
        $sequence,
        $send_time_sec,
        $send_time_usec,
        $time_sec,
        $time_usec,
       ) = unpack('NNNNNN', $buffer);

    # Decode device data if requested

    if($decode)
    {
        my @status;

        decode(\$buffer, \@status);
        return($sockaddr_in, $sequence, $time_sec, $time_usec, \@status);
    }
    else
    {
        return($sockaddr_in, $sequence, $time_sec, $time_usec, \$buffer);
    }
}

# Decode published data

sub decode($$;$)
{
    my ($buffer, $status, $decode_only_common) = @_;
    $decode_only_common = 0 if(!defined($decode_only_common));

    my $offset          = 24; # Represents the size of the fgc_udp_header plus time_sec and time_usec
    my $packet_length   = length($$buffer);
    my $num_devs        = int(($packet_length - $offset) / FGC_STATUS_SIZE);

    for(my $i = 0 ; $i < $num_devs ; $i++, $offset += FGC_STATUS_SIZE)
    {
        # Decode class data

        my $class = unpack("x${offset}xC", $$buffer);

        if($decode_only_common || $class == 0)
        {
            $status->[$i] = FGC::Class::Common::decode($buffer, $offset, FGC_STAT_PRECLASS_PAD);
        }
        else
        {
            if(!defined($fgc_decode_func[$class]))
            {
                die "Undefined publication decoding function for class $class";
            }

            $status->[$i] = $fgc_decode_func[$class]($buffer, $offset, FGC_STAT_PRECLASS_PAD);
        }
    }
}

# Check if socket is readable with a timeout

sub can_read($)
{
    my ($select) = @_;

    # Check if we can write to the socket

    my @ready = $select->can_read($TIMEOUT);

    # Timeout

    if (! scalar(@ready))
    {
        die "Server side timeout while waiting for subscription reply ($TIMEOUT sec)\n";
    }
    else
    {
        return 1;
    }
}

# EOF
