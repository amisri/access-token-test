#!/usr/bin/perl -w
#
# Name:     FGC/Runlog.pm
# Purpose:  Decode FGC run logs
# Author:   Stephen Page

package FGC::Runlog;

use Exporter;
use FGC::Runlog_entries;
use POSIX qw(strftime);
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        decode
                    );

use constant FGC_RUNLOG_ENTRY_SIZE  => 6;
use constant FGC_RUNLOG_MAX_ENTRYS  => 128;

return(1);

# Begin functions

# Decode run log buffer

sub decode($)
{
    my ($buffer) = @_;

    # Decode each entry in buffer

    my @entries;
    my $entry_bin;
    my $offset      = 0;
    my $prev_seq    = 0;
    my $start_index = 0;
    for(my $i = 0; $offset < length($$buffer) ; $i++, $offset += FGC_RUNLOG_ENTRY_SIZE)
    {
        my ($sequence, $id) = unpack("x${offset}CC", $$buffer);

        # Ignore entry if it is empty

        $i--, next if($id == 0);

        # Check whether this is the first entry

        if($prev_seq - $sequence == FGC_RUNLOG_MAX_ENTRYS - 1 ||
           $sequence - $prev_seq == FGC_RUNLOG_MAX_ENTRYS + 1)
        {
            $start_index = $i;
        }

        # Treat entry based upon its meta data

        my $entry_meta  = $FGC::Runlog_entries::runlog_entries[$id];
        my %entry       = (sequence => $sequence);

        if(defined($entry_meta))
        {
            $entry{label} = $entry_meta->{label};

            if($entry_meta->{data_length} == 1)
            {
                $entry{data} = unpack("x${offset}xxn", $$buffer);
            }
            elsif($entry_meta->{data_length} == 2)
            {
                $entry{data} = unpack("x${offset}xxN", $$buffer);
            }
        }
        else # Meta data for entry is unknown
        {
            $entry{label} = 'Unknown';
        }

        push(@entries, \%entry);
        $prev_seq = $sequence;
    }

    # Sort entries and add timestamps

    my $entries_to_copy = @entries;
    my @sorted_entries;
    my $timestamp_sec;
    my $timestamp_ms;

    for(my $i = $start_index ; $entries_to_copy ; $i = ($i + 1) % @entries)
    {
        my $entry = $entries[$i];

        # Store timestamps

        if($entry->{label} eq 'Unix time' && defined($entry->{data}))
        {
            $timestamp_sec = $entry->{data};
            $entries_to_copy--;
            next;
        }
        elsif($entry->{label} eq 'Ms time' && defined($entry->{data}))
        {
            if(defined($timestamp_sec))
            {
                $timestamp_ms = $entry->{data};
            }
            $entries_to_copy--;
            next;
        }

        # Add timestamp to entry if defined

        if(defined($timestamp_sec))
        {
            $entry->{timestamp} = strftime("%d/%m/%Y %H:%M:%S", localtime($timestamp_sec));
            $timestamp_sec      = undef;

            if(defined($timestamp_ms))
            {
                $entry->{timestamp} .= sprintf(".%03u", $timestamp_ms);
                $timestamp_ms        = undef;
            }
        }

        push(@sorted_entries, $entry);
        $entries_to_copy--;
    }

    return(\@sorted_entries);
}

# EOF
