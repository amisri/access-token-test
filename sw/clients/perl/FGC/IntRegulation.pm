#!/usr/bin/perl -w
#
# Name: FGC::IntRegulation
# Purpose:  Provides functions to calculate and plot the sensitivity graphs for internal regulation algorithms of FGC
# Author: Kevin Kessler

# depending on the fgc class, certain properties and calculations regarding libreg differ
# which is why we need to distinguish between libregone and libreg two
#####################
package LibRegOne;
#####################

use FGC::Utils qw(get_iteration_frequency_from_device read_property_from_device);
use Scalar::Util qw(looks_like_number);
use strict;
use warnings;

sub isLibRegOneClass($)
{
    my ($fgc_class) = @_;
    return $fgc_class =~ /^(51|53|61)$/;
}

sub new($$$$)
{
    my ($class, $control_loop, $is_test_mode, $load_select, $device) = @_;

    # LibRegOne only supports current regulation
    if($control_loop ne 'I') {
        die "ERROR: LibReg version one only supports current regulation as internal regulation.\n";
    }

    my $self = {
        is_test_mode => $is_test_mode,
        load_select => $load_select,
        device => $device,
    };
    bless($self, $class);

    # Make sure external algorithm isn't in use
    if(!$self->isExternalAlgDisabled()) {
        die "ERROR: REG.I.CLBW must not be 0 when using internal regulation\n";
    }

    return $self;
}

# checks whether the value of the device's "CLBW" property is != 0 (which means we are using internal regulation)
sub isExternalAlgDisabled()
{
    my ($self) = @_;

    my $external_alg_property = $self->{device}->{api}->{KEY_REG_INT_CLBW_I};
    my $external_alg = read_property_from_device($self->{device}, $external_alg_property);
    $external_alg = (split(",",$external_alg))[$self->{load_select}];

    return $external_alg != 0;
}

# calculates and returns the regulation sampling time Ts based on the device's PERIOD_ITERS and Iteration Frequency
# Ts = p_iters / freq
sub calcTs()
{
    my ($self) = @_;

    # read period div from device, based on load select value
    my $period_div_property = $self->{is_test_mode} ? $self->{device}->{api}->{KEY_REG_LAST_TEST_DIV_I} : $self->{device}->{api}->{KEY_REG_LAST_OP_DIV_I};
    my $period_div = read_property_from_device($self->{device}, $period_div_property);
    $period_div = (split(",",$period_div))[$self->{load_select}];
    if(!looks_like_number($period_div)){
        die "ERROR: Invalid value for $period_div_property [".$self->{load_select}."]: $period_div\n";
    }

    # calc and return Ts = per_iters/iter_freq
    my $iteration_frequency = get_iteration_frequency_from_device($self->{device});
    return $period_div / $iteration_frequency;
}

sub getCoeff($)
{
    my ($self, $rstab) = @_;

    if($rstab =~ /^(R|S|T)$/)
    {
        my $property = $self->{is_test_mode} ? $self->{device}->{api}->{"KEY_REG_LAST_TEST_$rstab\_I"} : $self->{device}->{api}->{"KEY_REG_LAST_OP_$rstab\_I"};
        my @vec = split(',', read_property_from_device($self->{device}, $property));
        return \@vec;
    }

    #A and B are not exposed by a property for the old classes. Hence we need to calculate it ourselves
    elsif($rstab =~ /^(A|B)$/)
    {
        # read property arrays required for calculation from device
        my $Lm = read_property_from_device($self->{device}, $self->{device}->{api}->{KEY_LOAD_HENRYS});
        my $Rm = read_property_from_device($self->{device}, $self->{device}->{api}->{KEY_LOAD_OHMS_MAG});
        my $Rp = read_property_from_device($self->{device}, $self->{device}->{api}->{KEY_LOAD_OHMS_PAR});
        my $Rs = read_property_from_device($self->{device}, $self->{device}->{api}->{KEY_LOAD_OHMS_SER});
        my $Ts = $self->calcTs();

        # extract values at load_select index
        $Lm = (split(",",$Lm))[$self->{load_select}];
        $Rm = (split(",",$Rm))[$self->{load_select}];
        $Rp = (split(",",$Rp))[$self->{load_select}];
        $Rs = (split(",",$Rs))[$self->{load_select}];

        # calc coefficients
        my $tau = $Lm / ($Rm + ($Rp*$Rs / ($Rp+$Rs)));
        my $g0 = 1 / ($Rs + $Rp);
        my $g1 = (1 / ($Rs + ($Rp*$Rm / ($Rp+$Rm)))) - (1 / ($Rs+$Rp));
        if($rstab eq 'A')
        {
            my $a1 = -exp(-$Ts / $tau);
            return [1,$a1,0];
        }
        elsif($rstab eq 'B')
        {
            my $b0 = $g0 + $g1 * (1-exp(-$Ts/$tau));
            my $b1 = -$g0 * exp(-$Ts/$tau);
            return [$b0, $b1];
        }
    }

    die "ERROR: Only coefficients R,S,T,A,B supported (not '$rstab')\n";
}

sub getAlgIndex($)
{
    my ($self) = @_;

    # The old classes only have one internal algorithm
    return '1';
}

sub getAuxpoleInfo()
{
    my ($self) = @_;

    # prepare property names depending on lib reg version
    my $clbw_property = $self->{device}->{api}->{KEY_REG_INT_CLBW_I};
    my $clbw2_property = $self->{device}->{api}->{KEY_REG_INT_CLBW2_I};
    my $damping_property = $self->{device}->{api}->{KEY_REG_INT_Z_I};

    # read the properties from device
    my $clbw = read_property_from_device($self->{device}, $clbw_property);
    $clbw = (split(",",$clbw))[$self->{load_select}];

    my $clbw2 = read_property_from_device($self->{device}, $clbw2_property);
    $clbw2 = (split(",",$clbw2))[$self->{load_select}];

    my $damping = read_property_from_device($self->{device}, $damping_property);
    $damping = (split(",",$damping))[$self->{load_select}];

    # return hash ref, containing the gathered info
    return {
        "CLBW" => $clbw,
        "CLBW2" => $clbw2,
        "Z" => sprintf("%.3f", $damping),
    }
}

#####################
package LibRegTwo;
#####################

use FGC::Utils qw(get_iteration_frequency_from_device read_property_from_device);
use Scalar::Util qw(looks_like_number);
use strict;
use warnings;

sub isLibRegTwoClass($)
{
    my ($fgc_class) = @_;
    return $fgc_class =~ /^(63|92|94)$/
}

sub new($$$$)
{
    my ($class, $control_loop, $is_test_mode, $load_select, $device) = @_;
    my $self = {
        control_loop => $control_loop,
        is_test_mode => $is_test_mode,
        load_select => $load_select,
        device => $device,
    };
    bless($self, $class);

    # Make sure external algorithm isn't in use
    if(!$self->isExternalAlgDisabled()) {
        die "ERROR: ".$self->{device}->{api}->{"KEY_REG_EXT_ALG_".$self->{control_loop}}." must be DISABLED\n";
    }

    return $self;
}

# checks whether the value of the device's "EXTERNAL_ALG" property is "DISABLED"
sub isExternalAlgDisabled()
{
    my ($self) = @_;

    my $external_alg_property = $self->{device}->{api}->{"KEY_REG_EXT_ALG_".$self->{control_loop}};

    # new API doesn't contain property for V, because external alg is always disabled
    if(!defined($external_alg_property))
    {
        return 1;
    }

    my $external_alg = read_property_from_device($self->{device}, $external_alg_property);
    $external_alg = (split(",",$external_alg))[$self->{load_select}];

    return $external_alg eq "DISABLED";
}

# calculates and returns the regulation sampling time Ts based on the device's PERIOD_ITERS and Iteration Frequency
# Ts = p_iters / freq
sub calcTs()
{
    my ($self) = @_;

    # read period iters from device, based on load select value
    my $period_iters_property = $self->{device}->{api}->{"KEY_REG_PERIOD_ITERS_".$self->{control_loop}};
    my $period_iters = read_property_from_device($self->{device}, $period_iters_property);
    $period_iters = (split(",",$period_iters))[$self->{load_select}];
    if(!looks_like_number($period_iters)){
        die "ERROR: Invalid value for $period_iters_property [".$self->{load_select}."]: $period_iters\n";
    }

    # calc and return Ts = per_iters/iter_freq
    my $iteration_frequency = get_iteration_frequency_from_device($self->{device});
    return $period_iters / $iteration_frequency;
}

sub getCoeff($)
{
    my ($self, $rstab) = @_;

    if($rstab !~ /^(R|S|T|A|B)$/)
    {
        die "ERROR: Only coefficients R,S,T,A,B supported\n";
    }

    my $test_op = $self->{is_test_mode} ? "TEST" : "OP";
    my $property = $self->{device}->{api}->{"KEY_REG_LAST_".$test_op."_".$rstab."_".$self->{control_loop}};
    my @vec = split(',', read_property_from_device($self->{device}, $property));
    return \@vec;
}

sub getAlgIndex($)
{
    my ($self) = @_;

    my $test_op = $self->{is_test_mode} ? "TEST" : "OP";
    my $alg_index_property = $self->{device}->{api}->{"KEY_REG_LAST_".$test_op."_ALGIDX_".$self->{control_loop}};
    my $alg_index = read_property_from_device($self->{device}, $alg_index_property);

    if(!looks_like_number($alg_index)){
        die "ERROR: Invalid value for $alg_index_property: $alg_index\n";
    }

    return $alg_index;
}

sub getAuxpoleInfo()
{
    my ($self) = @_;

    # prepare property names depending on lib reg version
    my $auxpole1_hz_property = $self->{device}->{api}->{"KEY_REG_INT_CLBW_".$self->{control_loop}};
    my $auxpoles2_hz_property = $self->{device}->{api}->{"KEY_REG_INT_CLBW2_".$self->{control_loop}};
    my $test_op = $self->{is_test_mode} ? "TEST" : "OP";
    my $modulus_margin_property = $self->{device}->{api}->{"KEY_REG_LAST_".$test_op."_MM_".$self->{control_loop}};

    # read the properties from device
    my $auxpole1_hz = read_property_from_device($self->{device}, $auxpole1_hz_property);
    $auxpole1_hz = (split(",",$auxpole1_hz))[$self->{load_select}];

    my $auxpoles2_hz = read_property_from_device($self->{device}, $auxpoles2_hz_property);
    $auxpoles2_hz = (split(",",$auxpoles2_hz))[$self->{load_select}];

    my $modulus_margin = read_property_from_device($self->{device}, $modulus_margin_property);

    # return hash ref, containing the gathered info
    return {
        "Auxpole1 [Hz]" => $auxpole1_hz,
        "Auxpoles2 [Hz]" => $auxpoles2_hz,
        "Modulus Margin" => sprintf("%.3f", $modulus_margin),
    }
}

#####################
package FGC::IntRegulation;
#####################

use Data::Dumper;
use FGC::Utils qw(read_property_from_device logspace log_base);
use File::Temp qw(tempfile);
use Math::Complex;
use Scalar::Util qw(looks_like_number);
use strict;
use warnings;

# Supported Control Loops
use constant {
    CONTROL_LOOP_CURRENT => 'I',
    CONTROL_LOOP_FIELD => 'B',
};

# Menu Labels
use constant {
    MENU_LABEL_CURRENT => 'Current',
    MENU_LABEL_FIELD => 'Field',
};

### Begin class functions

sub getLabelByLoop($)
{
    my ($control_loop) = @_;

    return MENU_LABEL_CURRENT if($control_loop eq CONTROL_LOOP_CURRENT);
    return MENU_LABEL_FIELD if($control_loop eq CONTROL_LOOP_FIELD);

    return undef;
}

sub isFGCClassSupported($)
{
    my ($fgc_class) = @_;
    return $fgc_class =~ /^(51|53|61|63|92|94)$/;
}

sub isControlLoopSupportedByClass($$)
{
    my ($fgc_class, $loop) = @_;

    return 0 if(!isFGCClassSupported($fgc_class));

    # loops supported by all supported classes
    return 1 if($loop eq CONTROL_LOOP_CURRENT);

    # loops only supported by specific classes
    return $loop eq CONTROL_LOOP_FIELD if($fgc_class =~ /^(63)$/);

    return 0;
}

sub getSupportedControlLoopsByClass($)
{
    my ($fgc_class) = @_;
    my @control_loops = (CONTROL_LOOP_CURRENT, CONTROL_LOOP_FIELD);
    my @supported_loops = ();
    for my $loop (@control_loops) {
        push(@supported_loops, $loop) if(isControlLoopSupportedByClass($fgc_class, $loop));
    }
    return \@supported_loops;
}

sub getSupportedControlLoopLabelsByClass($)
{
    my ($fgc_class) = @_;
    my @supported_loops = @{getSupportedControlLoopsByClass($fgc_class)};
    my @supported_labels = ();
    for my $loop (@supported_loops) {
        push(@supported_labels, getLabelByLoop($loop));
    }
    return \@supported_labels;
}

sub isTestModeSupportedByClass($)
{
    my ($fgc_class) = @_;
    return $fgc_class =~ /^(53|63|94)$/;
}

sub isInRegFault($$$)
{
    my ($device, $control_loop, $is_test_mode) = @_;
    my $fgc_class = $device->{class};
    my $status = undef;
    my $is_fault = undef;
    if(LibRegOne::isLibRegOneClass($fgc_class))
    {
        $status = read_property_from_device($device, $device->{api}->{KEY_REG_LAST_OP_STATUS_I});
        $is_fault = $status ne "OK" && $status ne "WARNING";
    }
    elsif(LibRegTwo::isLibRegTwoClass($fgc_class))
    {
        my $test_op = $is_test_mode ? "TEST" : "OP";
        my $status_property = $device->{api}->{"KEY_REG_LAST_".$test_op."_STATUS_".$control_loop};
        $status = read_property_from_device($device, $status_property);

        # the only entries of the reg_status symlist that are not faults are "OK" and "LOW_MOD_MARGN"
        $is_fault = $status ne "OK" && $status ne "LOW_MOD_MARGN";
    }
    else
    {
        die "ERROR: Class $fgc_class does not support internal regulation\n";
    }

    return $is_fault;
}

# calculates a sensitivity vector based on a rst vector and returns it
sub calcVector($$$$)
{
    my ($omega_ref, $Ts, $rst_ref, $order) = @_;

    my @vec = ();
    my @w = @{$omega_ref};
    my @rst = @{$rst_ref};

    for (my $i = 0; $i < scalar(@w); $i++) {
        $vec[$i] = 0;
        for (my $o = 0; $o <= $order; $o++) {
            $vec[$i] += $rst[$o] * exp(i * ($order-$o) * $w[$i] * $Ts);
        }
    }

    return \@vec;
}

# Constructor
sub new($$$$$)
{
    my ($class, $device, $control_loop, $is_test_mode, $omega_start) = @_;

    if(!defined($device)){
        die "ERROR: Invalid device\n";
    }

    my $fgc_class = $device->{class};
    if(!isFGCClassSupported($fgc_class)){
        die "ERROR: Unsupported device class: $fgc_class\n";
    }

    if(!isControlLoopSupportedByClass($fgc_class, $control_loop)){
        die "ERROR: '".getLabelByLoop($control_loop)."' is not supported by class $fgc_class\n";
    }

    if($is_test_mode){
        die "ERROR: Test Mode not supported by class $fgc_class\n" if(!isTestModeSupportedByClass($fgc_class));
    }

    my $self = {
        device => $device,
        control_loop => $control_loop,
        is_test_mode => $is_test_mode,
        gnu_id => undef,             # stores the id of the gnuplot process while active
        gnu_stream => undef,         # stores the pipe to the gnuplot process to which we want to write
        omega => [],                 # omega vector of length 300, logarithmically spaced, starting at $omega_start
        R => [],                     # sensitivity vector R
        S => [],                     # sensitivity vector S
        T => [],                     # sensitivity vector T
        A => [],                     # sensitivity vector A
        B => [],                     # sensitivity vector B
        tmp_in_file => undef,        # stream to tmp file, which is used as gnuplot input
        tmp_in_file_name => undef,   # path to the tmp input file
        tmp_out_file => undef,       # stream to tmp file, which is used as gnuplot output
        tmp_out_file_name => undef,  # path to the tmp output file
        lib_reg => undef,            # depending on libreg one or two, certain properties and calculations differ
    };
    bless($self, $class);

    my $load_select = $self->getLoadSelect();

    # determine and instantiate lib_reg object by fgc_class
    if(LibRegOne::isLibRegOneClass($fgc_class))
    {
        $self->{lib_reg} = LibRegOne->new($control_loop, $is_test_mode, $load_select, $device);
    }
    elsif(LibRegTwo::isLibRegTwoClass($fgc_class))
    {
        $self->{lib_reg} = LibRegTwo->new($control_loop, $is_test_mode, $load_select, $device);
    }
    else
    {
        die "ERROR: Class $fgc_class does not support internal regulation\n";
    }

    # calc log spaced omega vector based on $omega_start and $omega_end
    my $base = 10;
    my $length = 300;
    my $omega_end = pi / $self->{lib_reg}->calcTs();
    $self->{omega} = logspace($base, 2*pi*$omega_start, $omega_end, $length);

    # Return created object
    return $self;
}

### Begin object methods

# returns the device's LOAD.SELECT or LOAD.TEST_SELECT value (according to the is_test_mode flag)
sub getLoadSelect()
{
    my ($self) = @_;

    # class 53 test properties are not based on load select. hence there is no load.test_select property for class 53
    if($self->{is_test_mode} && $self->{device}->{class} =~ /^53$/)
    {
        return 0;
    }

    my $load_select_property = $self->{is_test_mode} ? $self->{device}->{api}->{KEY_LOAD_TEST_SELECT} : $self->{device}->{api}->{KEY_LOAD_SELECT};

    # read load select from device
    my $load_select = read_property_from_device($self->{device}, $load_select_property);
    if(!looks_like_number($load_select) || $load_select < 0 || $load_select > 3){
        die "ERROR: Invalid value for $load_select_property: $load_select\n";
    }

    return $load_select;
}

# calculates all sensitivity vectors R S T A B based on the device's RST parameters and it's internal alg_index
sub calcVectors()
{
    my ($self) = @_;

    # get internally calculated RST parameters
    my @r_coeff = @{$self->{lib_reg}->getCoeff("R")};
    my @s_coeff = @{$self->{lib_reg}->getCoeff("S")};
    my @t_coeff = @{$self->{lib_reg}->getCoeff("T")};
    my @a_coeff = @{$self->{lib_reg}->getCoeff("A")};
    my @b_coeff = @{$self->{lib_reg}->getCoeff("B")};

    # Calculate floating point math correction for T coefficients to ensure that Sum(T) == Sum(R)
    my $t0_correction = 0;
    for (my $i = 0; $i < scalar(@r_coeff); $i++)
    {
        $t0_correction += ($r_coeff[$i] - $t_coeff[$i]);
    }
    $t_coeff[0] += $t0_correction;

    # get index of internal algorithm that was used to calculate the parameters
    my $alg_index= $self->{lib_reg}->getAlgIndex();

    my $Ts = $self->{lib_reg}->calcTs();
    my $omega_ref = $self->{omega};

    # calculate the vectors depending on the interal algorithm index
    if($alg_index == 1 || $alg_index == 2){
        $self->{R} = calcVector($omega_ref, $Ts, \@r_coeff, 3);
        $self->{S} = calcVector($omega_ref, $Ts, \@s_coeff, 3);
        $self->{T} = calcVector($omega_ref, $Ts, \@t_coeff, 3);
        $self->{A} = calcVector($omega_ref, $Ts, \@a_coeff, 2);
        $self->{B} = calcVector($omega_ref, $Ts, \@b_coeff, 1);
    }
    elsif($alg_index == 3 || $alg_index == 4){
        $self->{R} = calcVector($omega_ref, $Ts, \@r_coeff, 4);
        $self->{S} = calcVector($omega_ref, $Ts, \@s_coeff, 4);
        $self->{T} = calcVector($omega_ref, $Ts, \@t_coeff, 4);
        $self->{A} = calcVector($omega_ref, $Ts, \@a_coeff, 3);
        $self->{B} = calcVector($omega_ref, $Ts, \@b_coeff, 1);
    }
    elsif($alg_index == 5){
        $self->{R} = calcVector($omega_ref, $Ts, \@r_coeff, 5);
        $self->{S} = calcVector($omega_ref, $Ts, \@s_coeff, 5);
        $self->{T} = calcVector($omega_ref, $Ts, \@t_coeff, 5);
        $self->{A} = calcVector($omega_ref, $Ts, \@a_coeff, 4);
        $self->{B} = calcVector($omega_ref, $Ts, \@b_coeff, 1);
    }
    elsif($alg_index == 10){
        die "ERROR: Plots only available for PII regulators (current regulator is PI)\n";
    }
    elsif($alg_index == 20){
        die "ERROR: Plots only available for PII regulators (current regulator is I)\n";
    }
    else{
        die "ERROR: No algorithm implemented for alg_index == $alg_index\n";
    }
}

# generates the plot data based on the previously calculated sensitivity vectors RSTAB and displays the three resulting plots via gnuplot
sub plotFunctions()
{
    my ($self) = @_;

    my @w = @{$self->{omega}};
    my @R = @{$self->{R}};
    my @S = @{$self->{S}};
    my @T = @{$self->{T}};
    my @A = @{$self->{A}};
    my @B = @{$self->{B}};

    # verify that the sensitivity vectors have been calculated
    if(!@w || !@R || !@S || !@T || !@A || !@B){
        die "ERROR: Can not plot functions. Sensitivity vectors are not initialized.\n";
    }

    # generate plot data
    my @plot_one  = ();
    my @plot_two_a  = ();
    my @plot_two_b  = ();
    my @plot_three  = ();
    for (my $i = 0; $i < scalar(@w); $i++) {
        my $BRAS = $B[$i]*$R[$i] + $A[$i]*$S[$i];
        $plot_one[$i]   = 20 * log_base(10, abs( $B[$i]*$T[$i] / $BRAS ));
        $plot_two_a[$i] = 20 * log_base(10, abs( $B[$i]*$S[$i] / $BRAS ));
        $plot_two_b[$i] = 20 * log_base(10, abs( $B[$i] / $A[$i] ));
        $plot_three[$i] = 20 * log_base(10, abs(-$B[$i]*$R[$i] / $BRAS ));
    }

    # Open a temporary input file, if there isn't already one open
    if(!defined($self->{tmp_in_file}))
    {
        ($self->{tmp_in_file}, $self->{tmp_in_file_name}) = tempfile('fgcrun+_plot_in_XXXXXX', DIR => '/tmp');
        if(!defined($self->{tmp_in_file})){
            die("ERROR: Unable to open temporary input file\n");
        }
        $self->{tmp_in_file}->autoflush(1);
    }
    else # Zero the temporary input file if there is already one open
    {
        truncate($self->{tmp_in_file}, 0);
        seek($self->{tmp_in_file}, 0, 0);
    }

    # Write plot data into temporary input file
    for(my $i = 0 ; $i < scalar(@w) ; $i++)
    {
        # display omega in Hz, not in rad
        my $w_hz = $w[$i] / (2*pi);

        # write plot data to file
        print {$self->{tmp_in_file}} "$w_hz $plot_one[$i] $plot_two_a[$i] $plot_two_b[$i] $plot_three[$i]\n";
    }

    # Open a temporary output file, if there isn't already one open
    if(!defined($self->{tmp_out_file}))
    {
        ($self->{tmp_out_file}, $self->{tmp_out_file_name}) = tempfile('fgcrun+_plot_out_XXXXXX', SUFFIX => '.png', DIR => '/tmp');
        if(!defined($self->{tmp_out_file})){
            die("ERROR: Unable to open temporary output file\n");
        }
    }
    else # Zero the temporary output file if there is already one open
    {
        truncate($self->{tmp_out_file}, 0);
        seek($self->{tmp_out_file}, 0, 0);
    }

    # prepare titles for plots
    my $loop = $self->{control_loop} eq CONTROL_LOOP_CURRENT ? 'current' : $self->{control_loop} eq CONTROL_LOOP_FIELD ? 'field' : '_unknown_';
    my $plot_one_title = "Sensitivity from reference $loop to output $loop";
    my $plot_two_a_title = "Closed loop sensitivity from input voltage disturbance to output $loop";
    my $plot_two_b_title = "Open loop sensitivity from input voltage (disturbance) to output $loop";
    my $plot_three_title = "Sensitivity from $loop noise to output $loop";

    # prepare the info title
    my %info = %{$self->{lib_reg}->getAuxpoleInfo()};
    my $info_title = "";
    for my $key (keys(%info)) {
        my $value = $info{$key};
        if(looks_like_number($value))
        {
            $value = sprintf("%.10f", $value); # convert from scientific to decimal representation
            $value =~ s/\.(?:|.*[^0]\K)0*\z//; # remove trailing zeros
        }
        $info_title .= "$key = $value | ";
    }
    $info_title = substr($info_title, 0, -3); # remove trailing | separator

    # open gnuplot
    $self->{gnu_id} = open($self->{gnu_stream}, "| '/usr/bin/gnuplot'");
    if(!$self->{gnu_id})
    {
        die "ERROR: Unable to open gnuplot\n";
    }
    $self->{gnu_stream}->autoflush(1);

    # plot the functions #NOTE: for development, put GDFONTPATH=~/projects/fgc/doc/fonts to your environment variables
    print {$self->{gnu_stream}}
        "n = 3\n",                                 # the number of data sets to be plotted
        "t = 100.0\n",                             # top margin in pixels
        "m = 30.0\n",                              # margin in between the middle plots
        "b = 150.0\n",                             # key/legend height in pixels (bottom margin)
        "s = 220.0\n",                             # size (heigh) of each plot box
        "h = s*n + (n-1)*m + t + b\n",             # height of output image in pixels
        "font_general = 'freeroad_light,12'\n",    # general font, like used for the key / legend
        "font_axis = 'freeroad,14'\n",             # font used for units on the axis and the top title
        "font_tics = 'lekton,11'\n",               # font used for the numbers at the tics of the axis

        # line styles
        "c1 = 'blue'\n",
        "c2 = 'magenta'\n",
        "c3 = 'dark-turquoise'\n",
        "c4 = 'forest-green'\n",
        "thickness = 2\n",

        # define functions to help set top/bottom margins,
        "top(i,n,h,t,b) = 1.0 - (t+m/2+(h-t-b)*(i-1)/n)/h\n",
        "bot(i,n,h,t,b) = 1.0 - (t-m/2+(h-t-b)*i/n)/h\n",

        # basic plot parameters
        "set term png enhanced size 1440,h font font_general\n",
        "set output '".$self->{tmp_out_file_name}."'\n",
        "set title '$info_title' font 'freeroad,14'\n",             # info string at the top
        "set format y '\%G'\n",                                     # y-axis tics label format
        "set format x ''\n",                                        # at first, lower x-axis tics should not be labeled
        "set format x2 '10^{\%L}'\n",                               # at first, upper x-axis tics should be labeled in exp. notation
        "set logscale x 10\n",                                      # lower x-axis tics should be in logscale (base 10)
        "set logscale x2 10\n",                                     # upper x-axis tics should be in logscale (base 10)
        "set xtics 1e-6, 10, 1000\n",                               # show major tics on lower x-axis (start, incr, end)
        "set x2tics 1e-6, 10, 1000\n",                              # show major tics on upper x-axis (start, incr, end)
        "set mxtics 10\n",                                          # show minor tics on lower x-axis (freq)
        "set mx2tics 10\n",                                         # show minor tics on upper x-axis (freq)
        "set tics font font_tics\n",                                # set font for tic labels
        "set grid xtics mxtics ytics mytics\n",                     # show dashed grid lines in background of plots
        "set lmargin 14\n",                                         # force same width for all plots by aligning at the same ymargin on the left
        "set rmargin 6\n",                                          # y-margin on the right
        "set multiplot layout (n+1),1\n",                           # multiple graphs in one output

            # First plot
            "set ylabel ''\n",
            "currentplot = 1\n",
            "set tmargin at screen top(currentplot,n,h,t,b)\n",
            "set bmargin at screen bot(currentplot,n,h,t,b)\n",
            "unset key\n",
            "plot '".$self->{tmp_in_file_name}."' using 1:2 with lines title '$plot_one_title' lt rgb c1 lw thickness\n",
            "unset title\n",
            "set format x2 ''\n",

            # Middle plot
            "set ylabel '[dB]' font font_axis rotate by 0 offset -2,0\n",
            "currentplot = currentplot + 1\n",
            "set tmargin at screen top(currentplot,n,h,t,b)\n",
            "set bmargin at screen bot(currentplot,n,h,t,b)\n",
            "plot '".$self->{tmp_in_file_name}."' using 1:3 with lines title '$plot_two_a_title' lt rgb c2 lw thickness, '' using 1:4 with lines title '$plot_two_b_title' lt rgb c3 lw thickness\n",

            # Last plot
            "set ylabel ''\n",
            "currentplot = currentplot + 1\n",
            "set tmargin at screen top(currentplot,n,h,t,b)\n",
            "set bmargin at screen bot(currentplot,n,h,t,b)\n",
            "set xlabel 'Frequency [Hz]' font font_axis\n",
            "set format x '10^{\%L}'\n",
            "plot '".$self->{tmp_in_file_name}."' using 1:5 with lines title '$plot_three_title' lt rgb c4 lw thickness\n",

            # plot the legend (key)
            "set tmargin at screen bot(n,n,h,t,b)\n",
            "set bmargin at screen 0\n",
            "set key below right vertical\n",
            "set border 0\n",
            "unset tics\n",
            "unset xlabel\n",
            "unset ylabel\n",
            "unset logscale x\n",
            "unset logscale x2\n",
            "set yrange [1:2]\n",
            "plot 0 t '$plot_one_title' lt rgb c1 lw thickness, 0 t '$plot_two_a_title' lt rgb c2 lw thickness, 0 t '$plot_two_b_title' lt rgb c3 lw thickness, 0 t '$plot_three_title' lt rgb c4 lw thickness\n",

        "unset multiplot\n";

    close($self->{gnu_stream});
}

# Destructor make sure everything is cleaned up when object being destroyed.
sub DESTROY
{
    my ($self) = @_;

    # Close gnuplot stream
    if($self->{gnu_stream})
    {
        close($self->{gnu_stream});
        $self->{gnu_stream} = undef;
    }

    # delete tmp files if exist
    unlink($self->{tmp_in_file_name}) if(defined($self->{tmp_in_file}) && -e $self->{tmp_in_file_name});
    unlink($self->{tmp_out_file_name}) if(defined($self->{tmp_out_file}) && -e $self->{tmp_out_file_name});
}

# toString implementation
use overload fallback => 1,
'""' => sub{
    my ($self) = @_;
    Dumper( $self );
};

return 1;
# EOF
