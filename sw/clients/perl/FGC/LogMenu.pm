#!/usr/bin/perl -w

package FGC::LogMenu;
use lib ("../private/libs/");
use Test::TCP;
use Exporter;

use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(get_log_menu);

my $log_menu_names;
my $log_menu_props;
my $log_menu_signals;
my $log_menu_sig_list;

sub get_log_menu($;$;$)
{
    my ($device, $gateway, $channel) = @_;

    create_struct_for_device($device,$gateway,$channel);

    my $log_menu = eval{get($device, 'log.menu')};

    if ($@){
        my $error = $@;
        if($@ =~ /^ERROR: /)
        {
            $error = substr $error, 7;
        }
        die "Could not get LOG.MENU: $error";
    }
    my $result;

    my @split_menu = (split "\n", $log_menu);

    my $l = @split_menu;

    for my $el (@split_menu)
    {
        my @split_field = (split ':', $el);
        $result->{lc $split_field[0]} = [split ",", $split_field[1]];
    }

    return $result;

}

1;

#EOF
