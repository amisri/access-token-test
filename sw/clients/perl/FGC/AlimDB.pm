#!/usr/bin/perl -w
#
# Filename: FGC/AlimDB.pm
# Purpose:  Access functions for the TE-EPC (Alim) Database
# Author:   Kevin Kessler

package FGC::AlimDB;

use DBI;
use Exporter;
use strict;
use warnings;
use Data::Dumper;
use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT     = qw(
                        ALIM_DB_PASSWORD
                        ALIM_DB_USERNAME
                    );
our @EXPORT_OK  = qw(
                        connect
                        disconnect

                        get_equipment_barcodes
                        get_equipment_barcode
                    );

# Constants

use constant ALIM_DB_ID          => 'dbi:Oracle:cerndb1'; # ID of Alim database
use constant ALIM_DB_USERNAME    => 'pocontrols_pub';     # Public username for Alim database
use constant ALIM_DB_PASSWORD    => 'p0published!!';       # Public password for Alim database


my $connect_count = 0;

return(1);

# Begin functions

# Connect to database

sub connect($$)
{
    my ($username, $password) = @_;

    my $dbh = DBI->connect(ALIM_DB_ID, $username, $password, {AutoCommit => 0, PrintError => 0, RaiseError => 0});
    if(!$dbh) {
    	print $!;
    	print DBI->errstr();
    }
    $connect_count++;

    return($dbh);
}

# Disconnect from database

sub disconnect($)
{
    my ($dbh) = @_;

    $dbh->disconnect;
}

# Get all converter barcodes as map "equipment_name"->"barcode"

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_equipment_barcodes($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT l.logical_name \"EQUIPMENT_NAME\", pt.prefix || pt.eqp_code || pt.variante || '-' || e.cern_sn \"BARCODE\"
                                                     FROM   alim.locations l,
                                                            alim.equipments e,
                                                            alim.part_Types pt
                                                     WHERE  e.id_eqp = l.id_eqp
                                                     AND    e.id_part_type = pt.id_part_type
                                                     AND    l.logical_name is not null") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            = (
                                          EQUIPMENT_NAME => 1,
                                          BARCODE => 1, # The barcode is constructed out of following fields in alim db: pt.prefix + pt.eqp_code + pt.variante + '-' + e.cern_sn
                                      );
        }

        # Execute query

        $query->execute() or return(undef);

        # Get array of result row hashes and convert it to hash mapping equipment name to barcode

        my $results = $query->fetchall_arrayref(\%result_hash);
        my %converter_to_barcode = map { $_->{EQUIPMENT_NAME} => $_->{BARCODE} } (@$results);

        # Return hash

        return(\%converter_to_barcode);
    }
}

# Get barcode of a specific equipment by equipment name

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_equipment_barcode($$)
    {
        my ($dbh, $equipment_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT barcode FROM alim.equipments_view
                                                     WHERE logical_name = ?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            = (
                                          BARCODE => 1,
                                      );
        }

        # Execute query

        $query->execute($equipment_name) or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}
# EOF
