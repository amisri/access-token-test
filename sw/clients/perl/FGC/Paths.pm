=head1 NAME

FGC::Paths

=head1 DESCRIPTION

Sef of functions which will return paths to some directories in repository

=cut

package FGC::Paths;

use Cwd qw(abs_path);
use strict;
use Exporter 'import';
our @EXPORT_OK = qw(get_bundle_path get_current_path get_class_path get_configs_dir get_patterns_dir get_class_properties_path);
our %EXPORT_TAGS = ( all => \@EXPORT_OK );


# Resolve bundle path

sub get_bundle_path
{
    use File::Basename;

    my $script_path = dirname($0);
    my $bundle_dir = `cd '${script_path}'; git rev-parse --show-toplevel`;
    chomp $bundle_dir;

    $bundle_dir.'/';
}

sub get_current_class
{
    my $script_path = abs_path $0;

    my ( $class ) = ( $script_path =~ /(\d\d)_mcu/ );

    $class
}

sub get_class_path
{
    get_bundle_path().
    'sw/fgc/'.
    get_current_class().
    '_mcu/'
}

sub get_configs_dir
{
    get_class_path().
    '/t/configs/'
}

# There is a convention that directory with patterns for this test should be named the same as current script name

sub get_patterns_dir
{
    my $script_name = basename($0);
    my $dirname = $script_name;
    $dirname =~ s/\.t$//;

    get_class_path().
    't/patterns/'.
    $dirname
}

sub join_progname_with_args
{
    join '_', basename $0, @ARGV
}

sub get_acquisition_dir
{
    get_class_path().
    'tmp/'.
    join_progname_with_args();
}

sub get_pattern_dir2
{
    get_class_path().
    'patterns/'
}

sub get_class_properties_path
{
    my ( $class ) = @_;

    get_bundle_path().
    'sw/fgc/'.
    $class.
    '_mcu/t/properties.csv'
}

1;
