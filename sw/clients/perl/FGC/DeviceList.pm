#!/usr/bin/perl -w
#
# Name:     DeviceList.pm
# Purpose:  Handle FGC device list files
# Author:   Stephen Page

package FGC::DeviceList;

use Exporter;
use strict;

use FGC::Consts;

our @ISA            = qw(Exporter);
our @EXPORT_OK      = qw(
                            read
                            write
                        );

return(1);

# Begin functions

# Read device list file

sub read($$)
{
    my ($devices, $filename) = @_;

    # Open device list

    open(FILE, '<', $filename) or return(undef);

    my @list_devices;
    while(<FILE>)
    {
        # Remove new line

        s/[\012\015]+$//g;

        my ($device_name, @response) = split(',', $_);

        my $device = $devices->{$device_name};

        # Check whether device exists

        if(!defined($device))
        {
            warn "Device $device_name in file $filename does not exist\n";
            next;
        }

        # Set response

        $device->{response} = join(',', @response);

        # Add device to set

        push(@list_devices, $device);
    }
    close(FILE);

    return(\@list_devices);
}

# Write device list file

sub write($$)
{
    my ($list_devices, $filename) = @_;

    # Open device list

    open(FILE, '>', $filename) or return(undef);

    for my $device (sort { $a->{name} cmp $b->{name} } (@$list_devices))
    {
        printf FILE ("%s,%s\n",
                     $device->{name},
                     defined($device->{response}) ? $device->{response} : '',
                    );
    }
    close(FILE);
}

# EOF
