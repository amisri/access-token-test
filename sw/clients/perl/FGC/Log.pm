#!/usr/bin/perl -w
#
# Name:     FGC/Log.pm
# Purpose:  Decode FGC logs
# Author:   Stephen Page

package FGC::Log;
use bytes;
use Exporter;
use List::Util qw(reduce min);
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        decode
                        decode_buffer
                        decode_header
                        decode_event_log
                        compose_event_log
                        compose_config_set
                        decode_buffer_v1
                        decode_header_v1
                        compose
                        compose_response
                        compose_response_v1
                        FGC_LOG_VERSION
                        FGC_LOG_SIG_LABEL_LEN_V1
                        FGC_LOG_SIG_LABEL_LEN
                        FGC_LOG_SIG_UNITS_LEN
                        FGC_LOG_TYPE_FLOAT
                        FGC_LOG_TYPE_INT16S
                        FGC_LOG_TYPE_INT16U
                        FGC_LOG_EVT_PROP_LEN
                        FGC_LOG_EVT_VAL_LEN
                        FGC_LOG_EVT_ACT_LEN
                        FGC_LOG_EVT_STAT_LEN
                        FGC_LOG_EVT_TOT_LEN
                        FGC_LOG_BUF_INFO_STOPPED
                        FGC_LOG_BUF_META_LITTLE_ENDIAN
                        FGC_LOG_BUF_META_CYCLIC
                        FGC_LOG_BUF_META_ANALOG
                        FGC_LOG_BUF_META_POSTMORTEM
                        FGC_LOG_BUF_META_CONTINUOUS
                        FGC_LOG_BUF_META_FREEZABLE
                        FGC_LOG_BUF_META_SUB_SAMPLED
                        FGC_LOG_SIG_META_SUB_SAMPLED
                        FGC_LOG_SIG_META_STEPS
                        JSON_CURRENT_VERSION
                    );

use constant FGC_LOG_VERSION                => 3;       # Checked for compatibility
use constant FGC_LOG_SIG_LABEL_LEN_V1       => 15;
use constant FGC_LOG_SIG_LABEL_LEN          => 24;
use constant FGC_LOG_SIG_UNITS_LEN          => 7;
use constant FGC_LOG_TYPE_FLOAT             => 0;
use constant FGC_LOG_TYPE_INT16S            => 1;
use constant FGC_LOG_TYPE_INT16U            => 2;
use constant FGC_LOG_EVT_PROP_LEN           => 24;      # Length of property field in event log
use constant FGC_LOG_EVT_VAL_LEN            => 36;      # Length of value field in event log
use constant FGC_LOG_EVT_ACT_LEN            => 7;       # Length of action field in event log
use constant FGC_LOG_EVT_STAT_LEN           => 1;       # Length of status field in event log
use constant FGC_LOG_EVT_TOT_LEN            => 76;      # Total length of one event log record
use constant FGC_LOG_BUF_INFO_STOPPED       => 0x80;    # Buffer has stopped filling

use constant FGC_LOG_BUF_META_LITTLE_ENDIAN => 0x01;    # Log data is little endian
use constant FGC_LOG_BUF_META_CYCLIC        => 0x02;    # Log is cyclic
use constant FGC_LOG_BUF_META_ANALOG        => 0x04;    # Log is analog (rather than digital)
use constant FGC_LOG_BUF_META_POSTMORTEM    => 0x08;    # Log is part of the postmortem
use constant FGC_LOG_BUF_META_CONTINUOUS    => 0x10;    # Log is continuous (rather than discontinuous)
use constant FGC_LOG_BUF_META_FREEZABLE     => 0x20;    # Log is freezable (only an option for continuous logs)
use constant FGC_LOG_BUF_META_SUB_SAMPLED   => 0x40;    # Data is being sub-sampled (returned in SPY header)

use constant FGC_LOG_SIG_META_SUB_SAMPLED   => 0x08;    # NOT USED YET
use constant FGC_LOG_SIG_META_STEPS         => 0x01;    # Specify step interpolation for signal

use constant JSON_CURRENT_VERSION           => '1.1';   # Current version of PowerSpy JSON format

# Spy v2 constants

use constant SPY_SIG_NAME_LEN                   => 24;      # Length of the name field in the signal header (1 byte is needed for null termination)

use constant SPY_BUF_META_LITTLE_ENDIAN         => 0x01;    # Log header and data are little endian
use constant SPY_BUF_META_ANA_SIGNALS           => 0x02;    # Log contains analog signals instead of digital signals
use constant SPY_BUF_META_ANA_SIGS_IN_COLUMNS   => 0x04;    # Analog signal data is in columns instead of rows
use constant SPY_BUF_META_TIMESTAMP_IS_LST      => 0x08;    # Timestamp is the Last Sample Time (LST) instead of the First Sample Time (FST)

use constant SPY_SIG_META_STEPS                 => 0x01;    # Analog signal should be displayed with trailing step interpolation
use constant SPY_SIG_META_USE_SCALING           => 0x02;    # Analog signal should be transformed with gain and offset

use constant SPY_TYPE_FLOAT                     => 0;       # Analog signal data is floating point
use constant SPY_TYPE_SIGNED                    => 1;       # Analog signal data is signed integer
use constant SPY_TYPE_UNSIGNED                  => 2;       # Analog signal data is unsigned integer

return(1);


# Begin functions

# ##### SPY V1 COMPATIBILITY FUNCTIONS ######################################################

# Decode buffer header

sub decode_header_v1($)
{
    my ($buffer) = @_;

    my %header;
    my $offset = 0;

    # Check whether buffer is empty

    if(length($$buffer) == 0)
    {
        %header = (
                    version     => FGC_LOG_VERSION,
                    info        => 0,
                    n_signals   => 0,
                    n_samples   => 0,
                    signals     => [],
                  );

        return(\%header, $offset);
    }

    # Decode header

    (
        $header{version}    , # Buffer version
        $header{info}       , # FGC_LOG_BUF_INFO_XXXX
    ) = unpack('CC', $$buffer);

    (
        $header{n_signals}  , # Number of signals
        $header{n_samples}  , # Number of samples
        $header{period_us}  , # Sampling period (us)
        $header{unix_time}  , # Time of last sample (s, us)
        $header{us_time}    ,
    ) = unpack($header{info} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
               'xxvVVVV' : 'xxnNNNN', $$buffer);

    $offset += 20;

    # Decode signal headers

    $header{signals} = [];
    for(my $i = 0 ; $i < $header{n_signals} ; $i++)
    {
        my %sig_header;

        (
            $sig_header{type},              # FGC_LOG_TYPE_XXXX
            $sig_header{info},              # FGC_LOG_SIG_INFO_XXXX
            $sig_header{us_time_offset},    # Timescale offset for this signal (us)
            $sig_header{gain},              # This part should match struct fgc_dimdb_ana
            $sig_header{offset},            # Sig = gain * raw_value + offset
            $sig_header{label},             # Label (not null-terminated)
            $sig_header{units},             # Units (not null-terminated)
        ) = unpack(
                    $header{info} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?

                    "x${offset}".
                    'v'.                                    # type
                    'v'.                                    # info
                    'V'.                                    # us_time_offset
                    'V'.                                    # gain
                    'V'.                                    # offset
                    '(c/a @0)xx'.FGC_LOG_SIG_LABEL_LEN_V1.  # label
                    '(c/a @0)xx'.FGC_LOG_SIG_UNITS_LEN      # units

                    :

                    "x${offset}".
                    'n'.                                    # type
                    'n'.                                    # info
                    'N'.                                    # us_time_offset
                    'N'.                                    # gain
                    'N'.                                    # offset
                    '(c/a @0)xx'.FGC_LOG_SIG_LABEL_LEN_V1.  # label
                    '(c/a @0)xx'.FGC_LOG_SIG_UNITS_LEN      # units

                    ,

                    $$buffer
                  );

        $sig_header{us_time_offset} = unpack('l', pack('L', $sig_header{us_time_offset}));
        $sig_header{gain}           = unpack('f', pack('L', $sig_header{gain}));
        $sig_header{offset}         = unpack('f', pack('L', $sig_header{offset}));

        $offset += 40;

        push(@{$header{signals}}, \%sig_header);
    }

    return(\%header, $offset);
}

# Decode buffer

sub decode_buffer_v1($)
{
    my ($buffer) = @_;

    my %data;
    my $offset;

    # Decode header

    ($data{header}, $offset) = decode_header_v1($buffer)
                               or return(undef);

    # Decode signals

    for(my $i = 0 ; $i < $data{header}->{n_samples} ; $i++)
    {
        for(my $sig_idx = 0; $sig_idx < @{$data{header}->{signals}}; $sig_idx++)
        {
            my $signal = $data{header}->{signals}->[$sig_idx];

            my $value;
            if($signal->{type} == FGC_LOG_TYPE_FLOAT)
            {
                $value = unpack($data{header}->{info} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
                                "x${offset}V" : "x${offset}N", $$buffer);
                $value = unpack('f', pack('L', $value));
                $offset += 4;
            }
            elsif($signal->{type} == FGC_LOG_TYPE_INT16U)
            {
                $value = unpack($data{header}->{info} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
                                "x${offset}v" : "x${offset}n", $$buffer);
                $offset += 2;
            }
            elsif($signal->{type} == FGC_LOG_TYPE_INT16S)
            {
                $value = unpack($data{header}->{info} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
                                "x${offset}v" : "x${offset}n", $$buffer);
                $value = unpack('s', pack('S', $value));
                $offset += 2;
            }
            else # Unknown signal type
            {
                warn "Signal type $signal->{type} for signal $signal->{label} is unknown\n";
                return(undef);
            }

            $data{header}->{signals}->[$sig_idx]->{samples}->[$i] = { value => ($signal->{gain} * $value) + $signal->{offset} };
        }
    }

    # Move signals structure one level up for easier access

    $data{signals} = $data{header}->{signals};
    delete $data{header}->{signals};

    return(\%data);
}

# Compose plain text from decoded data

sub compose_response_v1($;$;$;$)
{
    my ($data, $signal_prefix, $device, $name) = @_;
    $signal_prefix ||= '';

    if(!defined($data) || !defined($data->{signals}))
    {
        die("Buffer does not contain any signals");
    }

    my $output          = '';
    my @signals = @{$data->{signals}};
    my $nSignals = @signals;

    my $period = $data->{header}->{period_us} / 1000000;

    my $firstSampleTime = $data->{header}->{unix_time} +
                          $data->{header}->{us_time} / 1000000 -
                          ($data->{header}->{n_samples} - 1) * $period;

    $output .= "{\"source\":\"FGC\",";
    $output .= "\"device\":\"$device\",";
    $output .= "\"name\":\"$name\",";
    $output .= sprintf ("\"firstSampleTime\":%.6f,", $firstSampleTime);
    $output .= sprintf ("\"period\":%.6f,", $period);
    $output .= "\"version\":\"".JSON_CURRENT_VERSION."\",";

    # Add the signals
    $output .= "\"signals\":[";

    for (my $i = 0; $i < $nSignals; $i++)
    {
        my $signal = $signals[$i];

        # Add a comma to separate the signals after the first signal
        $output .= $i ? ',' : '';

        $output .= "{\"name\":\"$signal_prefix$signal->{label}\",";

        # Add needed metadata
        if($signal->{info} & FGC_LOG_SIG_META_STEPS)
        {
            $output .= "\"meta\":{\"step\":true},";
        }

        # Add samples for the signals
        $output .= "\"samples\":[";
        for(my $sample_index = 0; $sample_index < $data->{header}->{n_samples}; $sample_index++)
        {
            # Add a comma to separate the samples
            $output .=  $sample_index ? ',' : '';

            $output .= sprintf("%.7E", $signal->{samples}->[$sample_index]->{value} || 0);
        }
        $output .= "]}";
    }

    $output .= "]}";

    print $output;
}

# ##### SPY V3 ###########################################################################

sub decode_header($)
{
    my ($buffer) = @_;

    my %header;
    my $offset = 0;

    # Decode header
    (
        $header{version}    , # Buffer version
        $header{meta}       , # LOG_META_XXXX
    ) = unpack('CC', $$buffer);

    (
        $header{n_signals}                  , # number of signals
        $header{n_samples}                  , # number of samples
        $header{time_origin}->{sec}         , # time origin seconds and
        $header{time_origin}->{msec}        , # microseconds
        $header{first_sample_time}->{sec}   , # first sample time seconds and
        $header{first_sample_time}->{msec}  , # microseconds
        $header{period}->{sec}              , # period seconds and
        $header{period}->{msec}             , # microseconds
    ) = unpack($header{meta} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
                "xxvVVVVVVV" : "xxnNNNNNNN", $$buffer);

    $offset += 32;

    # Decode signal headers

    $header{signals} = [];
    for(my $i = 0 ; $i < $header{n_signals} ; $i++)
    {
        my %sig_header;

        (
            $sig_header{meta}           , # LOG_SIG_META_XXXX
            $sig_header{dig_bit_index}  , # Digital bit index
            $sig_header{time_offset_us} , # Time offset for signal
            $sig_header{name}           , # Label (name)
        ) = unpack($header{meta} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
                "x${offset}".
                'C'.                                # meta
                'C'.                                # dig_bit_index
                'x2'.                               # 2 bytes to align
                'V'.                                # time_offset_us
                'A'.FGC_LOG_SIG_LABEL_LEN           # name

                :

                "x${offset}".
                'C'.                                # meta
                'C'.                                # dig_bit_index
                'x2'.                               # 2 bytes to align
                'N'.                                # time_offset_us
                'A'.FGC_LOG_SIG_LABEL_LEN           # name
            ,

            $$buffer
        );

        if($@)
        {
            die("Faulty signal header (signal #".$i.")");
        }

        # Change time offset to signed long value
        $sig_header{time_offset_us} =
            unpack('l', pack('L', $sig_header{time_offset_us}));

        push(@{$header{signals}}, \%sig_header);

        $offset += 32;
    }

    return(\%header, $offset);
}

sub decode_buffer($)
{
    my ($buffer) = @_;

    my %data;
    my $offset;

    # Decode header

    ($data{header}, $offset) = decode_header($buffer)
                               or return(undef);


    # Decode signals

    my $nSamples = $data{header}->{n_samples};

    if($data{header}->{meta} & FGC_LOG_BUF_META_ANALOG)
    {
        my $character = $data{header}->{meta} & FGC_LOG_BUF_META_LITTLE_ENDIAN ? 'V' : 'N';
        my $move = 4*(@{$data{header}->{signals}} - 1);

        for(my $sig_idx = 0; $sig_idx < @{$data{header}->{signals}}; $sig_idx++)
        {
            my $sig_offset = $offset + $sig_idx*4;
            my $unpack_str = "x${sig_offset}${character}";

            for(my $i = 1 ; $i < $nSamples; $i++)
            {
                $unpack_str .= "x${move}${character}";
            }

            my @value = unpack($unpack_str, $$buffer);
            my @samples = unpack("f${nSamples}", pack("L*", @value));

            $data{header}->{signals}->[$sig_idx]->{samples} = \@samples;
        }

    }
    else # Signals are digital
    {
        my @values = unpack($data{header}->{meta} & FGC_LOG_BUF_META_LITTLE_ENDIAN ?
            "x${offset}V${nSamples}" : "x${offset}N${nSamples}", $$buffer);

        for(my $sig_idx = 0; $sig_idx < @{$data{header}->{signals}}; $sig_idx++)
        {
            my $bit_index = $data{header}->{signals}->[$sig_idx]->{dig_bit_index};
            my $bit_mask  = 1 << $bit_index;

            # Sample value is the value in digital bit index location (1 | 0)
            my @samples = map{($bit_mask & $_) ? 1 : 0} @values;
            $data{header}->{signals}->[$sig_idx]->{samples} = \@samples;
        }
    }

    # Move signals structure one level up for easier access

    $data{signals} = $data{header}->{signals};
    delete $data{header}->{signals};

    return(\%data);
}

sub compose_response($;$;$;$)
{
    my ($data, $signal_prefix, $device, $name) = @_;
    $signal_prefix ||= '';

    if(!defined($data))
    {
        die("Buffer is empty");
    }
    elsif (!defined($data->{signals}))
    {
        die("Buffer does not contain any signals");
    }
    elsif (!defined($data->{header}))
    {
        die("Buffer does not contain a header");
    }

    my $output          = '';
    my @signals         = @{$data->{signals}};
    my $nSignals        = @signals;
    my $header          = $data->{header};
    my $period          = $header->{period}->{sec} + $header->{period}->{msec} / 1000000;
    my $firstSampleTime = $header->{first_sample_time}->{sec} + $header->{first_sample_time}->{msec} / 1000000;
    my $timeOrigin      = $header->{time_origin}->{sec} + $header->{time_origin}->{msec} / 1000000;
    my $isDigital       = !($header->{meta} & FGC_LOG_BUF_META_ANALOG);

    $output .= "{\"source\":\"FGC\",";
    $output .= "\"device\":\"$device\",";
    $output .= "\"name\":\"$name\",";
    $output .= sprintf ("\"timeOrigin\":%.6f,", $timeOrigin);
    $output .= sprintf ("\"firstSampleTime\":%.6f,", $firstSampleTime);
    $output .= sprintf ("\"period\":%.6f,", $period);
    $output .= "\"version\":\"".JSON_CURRENT_VERSION."\",";

    if($isDigital)
    {
        $output .= "\"type\":\"digital\",";
    }
    else
    {
        $output .= "\"type\":\"analog\",";
    }

    # Add the signals
    $output .= "\n\"signals\":[";

    for (my $i = 0; $i < $nSignals; $i++)
    {
        my $signal = $signals[$i];

        $output .= "{\"name\":\"$signal_prefix$signal->{name}\",";

        # Add needed metadata
        if($signal->{meta} & FGC_LOG_SIG_META_STEPS)
        {
            $output .= "\"meta\":{\"step\":true},";
        }

        # Add time offset (in seconds)
        $output .= sprintf ("\"timeOffset\":%.6f,", $signal->{time_offset_us}/1e6);

        # Add samples for the signals
        $output .= "\"samples\":[";

        if($isDigital)
        {
            $output .= join ',', @{$signal->{samples}};
        }
        else
        {
            $output .= sprintf "%.7E," x @{$signal->{samples}}, @{$signal->{samples}};
            chop($output);
        }

        $output .= "\n]},";
    }

    chop($output);
    $output .= "]}";

    $output;
}

sub decode_event_log($)
{
    my ($buffer) = @_;

    my $num_records = length($$buffer) / FGC_LOG_EVT_TOT_LEN;
    my %data;

    for (my $i = 0, my $offset = 0; $i < $num_records; $i++, $offset+=FGC_LOG_EVT_TOT_LEN) {

        my %record;

        (
            $record{time}->{sec}    ,   # timestamp, seconds
            $record{time}->{msec}   ,   # timestamp, microseconds
            $record{property}       ,   # property
            $record{value}          ,   # value
            $record{action}         ,   # action
            $record{status}             # status
        ) = unpack(

            "x${offset}" .
            'N'.
            'N'.
            'Z'.FGC_LOG_EVT_PROP_LEN.
            'Z'.FGC_LOG_EVT_VAL_LEN.
            'Z'.FGC_LOG_EVT_ACT_LEN.
            'Z'.FGC_LOG_EVT_STAT_LEN
            ,

            $$buffer
        );

        push(@{$data{events}}, \%record);
    }

    return \%data;
}

sub compose_event_log($$$)
{
    my ($data, $device, $name) = @_;

    my @events = @{$data->{events}};
    my $nEvents = @events;

    my $output = '';

    # Create the header
    $output .= "{\"source\":\"FGC\",";
    $output .= "\"device\":\"$device\",";
    $output .= "\"name\":\"$name\",";
    $output .= "\"type\":\"table\",";
    $output .= "\"class\":\"eventlog\",";
    $output .= "\"version\":\"".JSON_CURRENT_VERSION."\",";
    $output .= "\"table\":{";
    $output .= "\"rows\":[";

    # Create the rows
    for (my $i = 0; $i < $nEvents; $i++) {
        my $event       = $events[$i];
        my $timestamp   = $event->{time}->{sec} + $event->{time}->{msec}/1e6;
        my $property    = $event->{property};
        my $value       = $event->{value};
        my $action      = $event->{action};
        my $status      = $event->{status};

        if($timestamp == 0)
        {
            next;
        }

        # Escape double quote " with backslash ( \" )
        $property   =~ s/(\"|\\|\/)/\\$1/g;
        $value      =~ s/(\"|\\|\/)/\\$1/g;
        $action     =~ s/(\"|\\|\/)/\\$1/g;

        $output .= '{';

        # Add timestamp (with 6 decimals)
        $output .= sprintf("\"timestamp\":%.6f,", $timestamp);

        # Add cells
        $output .= "\"cells\":[";
        $output .= "\"$property\",";
        $output .= "\"$value\",";
        $output .= "\"$action\",";
        $output .= "\"$status\"";

        $output .= ']},';
    }

    chop($output);

    $output .= "]}}";

    $output;
}

sub compose_config_set($$$)
{
    my ($data, $device, $name) = @_;

    my @configs = split(' ', $$data);
    my $nConfig = @configs;

    my $output = '';

    # Create the header
    $output .= "{\"source\":\"FGC\",";
    $output .= "\"device\":\"$device\",";
    $output .= "\"name\":\"$name\",";
    $output .= "\"type\":\"table\",";
    $output .= "\"version\":\"".JSON_CURRENT_VERSION."\",";
    $output .= "\"table\":{";
    $output .= "\"headings\":{\"cells\":[\"Property\",\"Value\"]},";
    $output .= "\"rows\":[";

    # Create the rows
    for (my $i = 0; $i < $nConfig; $i++) {
        my $config          = $configs[$i];
        my @split_config    = split(':', $config);
        my $property        = $split_config[0];
        my $value           = $split_config[1];

        # Escape double quote " with backslash ( \" )
        $property   =~ s/\"/\\\"/g;
        $value      =~ s/\"/\\\"/g;

        $output .= '{';

        # Add cells
        $output .= "\"cells\":[";
        $output .= "\"$property\",";
        $output .= "\"$value\"";

        $output .= ']},';
    }

    chop($output);

    $output .= "]}}";

    $output;
}

sub decode($;$)
{
    my ($buffer, $log_property) = @_;

    my %data;
    my $version;
    my %header;

    $log_property //= "";

    # Check whether buffer is empty
    my $data = eval
    {
        if(length($$buffer) == 0)
        {
            die 'Buffer is empty';
        }
        elsif ($log_property =~ /LOG.EVT/)
        {
            return decode_event_log($buffer);
        }
        elsif ($log_property =~ /CONFIG.SET/)
        {
            return $buffer;
        }
        else
        {
            # Decode buffer
            (
            $version    , # Buffer version
            ) = unpack('C', $$buffer);

            if($version == FGC_LOG_VERSION)
            {
                return decode_buffer($buffer);
            }
            elsif ($version == 1)
            {
                return decode_buffer_v1($buffer);
            }
            elsif ($version == 2)
            {
                return decode_buffer_v2($buffer);
            }
            else
            {
                die "Unsupported SPY version: $version\n";
            }
        }
    };

    # Catch the error, add in the buffer for it to be examined on client side
    if($@)
    {
        die $@."/BUFFER/".$$buffer;
    }

    return $data;
}

sub compose($;$;$;$)
{
    my ($data, $signal_prefix, $device, $name) = @_;

    if ($name){
        if ($name =~ /LOG.EVT/)
        {
            return compose_event_log($data, $device, $name);
        }
        elsif ($name =~ /CONFIG.SET/)
        {
            return compose_config_set($data, $device, $name);
        }
    }


    # Only use last part of the buffer name, not the full name
    $name = (split(/\./, $name))[-1];
    if ($data->{header}->{version} == FGC_LOG_VERSION)
    {
        return compose_response($data, $signal_prefix, $device, $name);
    }
    elsif ($data->{header}->{version} == 1)
    {
        return compose_response_v1($data, $signal_prefix, $device, $name);
    }
    elsif ($data->{header}->{version} == 2)
    {
        return compose_response_v2($data, $signal_prefix, $device, $name);
    }
    else
    {
        die "Unsupported SPY version: $data->{header}->{version}\n";
    }
}

# ##### SPY V2 ##############################################################################

sub decode_ana_sample_v2($$$$$)
{
    my ($data, $buffer, $offset, $sig_idx, $sample_idx) = @_;

    my $log_header = $data->{header};
    my $sig_header = $log_header->{signals}->[$sig_idx];

    # Check whether the data is little-endian

    my $little_endian = $log_header->{meta} & SPY_BUF_META_LITTLE_ENDIAN;

    my $value;
    if($sig_header->{type} == SPY_TYPE_FLOAT)
    {
        $value = unpack("x${offset}".($little_endian ? "V" : "N"), $$buffer);
        $value = unpack('f', pack('L', $value));
    }
    else # Integer value
    {
        my $type_code;

        if($sig_header->{size} == 2)
        {
            $type_code = $little_endian ? "v" : "n"
        }
        elsif($sig_header->{size} == 4)
        {
            $type_code = $little_endian ? "V" : "N"
        }
        else # Invalid size
        {
            die("Invalid size for integer: $sig_header->{size}");
        }

        # Unpack the value

        $value = unpack("x${offset}".($little_endian ? "N" : "N"), $$buffer);

        # Convert the value to a signed type, if necessary

        if($sig_header->{type} == SPY_TYPE_SIGNED)
        {
            if($sig_header->{size} == 2)
            {
                $value = unpack('s', pack('S', $value));
            }
            elsif($sig_header->{size} == 4)
            {
                $value = unpack('l', pack('L', $value));
            }
        }
        elsif($sig_header->{type} != SPY_TYPE_UNSIGNED)
        {
            die("Unknown signal type: $sig_header->{type}");
        }
    }

    # Check whether the value is empty

    if($value eq '')
    {
        die("Signal header and buffer mismatch (Samples: $sample_idx, Expected: $log_header->{n_samples})");
    }

    # Apply gain and offset if scaling is enabled

    if($sig_header->{meta} & SPY_SIG_META_USE_SCALING)
    {
        $value = $sig_header->{gain} * $value + $sig_header->{offset};
    }

    # Append the value to the signal's array

    $sig_header->{samples}->[$sample_idx]->{value} = $value;
}

sub decode_buffer_v2($)
{
    my ($buffer) = @_;

    my %data;
    my $offset;

    # Decode header

    ($data{header}, $offset) = decode_header_v2($buffer) or return(undef);
    my $log_header = $data{header};

    # Check whether the data is little-endian

    my $little_endian = $log_header->{meta} & SPY_BUF_META_LITTLE_ENDIAN;

    # Decode signals

    if($log_header->{meta} & SPY_BUF_META_ANA_SIGNALS)
    {
        if($log_header->{meta} & SPY_BUF_META_ANA_SIGS_IN_COLUMNS)
        {
            for(my $i = 0 ; $i < $log_header->{n_samples} ; $i++)
            {
                for(my $sig_idx = 0; $sig_idx < @{$data{header}->{signals}}; $sig_idx++)
                {
                    my $sig_header = $log_header->{signals}->[$sig_idx];

                    decode_ana_sample_v2(\%data, $buffer, $offset, $sig_idx, $i);

                    $offset += $sig_header->{size};
                }
            }
        }
        else # Signals are in rows
        {
            for(my $sig_idx = 0; $sig_idx < @{$data{header}->{signals}}; $sig_idx++)
            {
                my $sig_header = $log_header->{signals}->[$sig_idx];

                for(my $i = 0 ; $i < $log_header->{n_samples} ; $i++)
                {
                    decode_ana_sample_v2(\%data, $buffer, $offset, $sig_idx, $i);

                    $offset += $sig_header->{size};
                }
            }
        }
    }
    else # Signals are digital
    {
        for(my $i = 0 ; $i < $data{header}->{n_samples} ; $i++)
        {
            my $value = unpack($little_endian ? "x${offset}V" : "x${offset}N", $$buffer);

            if ($value eq '')
            {
                die("Signal header and buffer mismatch (Samples: $i, Expected: $data{header}->{n_samples})");
            }

            for(my $sig_idx = 0; $sig_idx < @{$data{header}->{signals}}; $sig_idx++)
            {
                # Sample value is the value in digital bit index location (1 | 0)

                my $bit_index = $data{header}->{signals}->[$sig_idx]->{dig_bit_index};
                my $bit_mask  = 1 << $bit_index;
                my $bit_value = $value & $bit_mask ? 1 : 0;

                $data{header}->{signals}->[$sig_idx]->{samples}->[$i]->{value} = $bit_value;
            }

            $offset += 4;
        }
    }

    # Move signals structure one level up for easier access

    $data{signals} = $data{header}->{signals};
    delete $data{header}->{signals};

    return(\%data);
}

sub compose_response_v2($;$;$;$)
{
    my ($data, $signal_prefix, $device, $name) = @_;
    $signal_prefix ||= '';

    my $output          = '';
    my @signals = @{$data->{signals}};
    my $nSignals = @signals;

    # If there are no signals, return this kind of struct
    if (!$nSignals)
    {
        $output .= "{\"source\":\"FGC\",";
        $output .= "\"device\":\"$device\",";
        $output .= "\"name\":\"$name\",";
        $output .= "\"firstSampleTime\":0.0,";
        $output .= "\"period\":0.0,";
        $output .= "\"version\":\"".JSON_CURRENT_VERSION."\",";
        $output .= "\"signals\":[]}";
        return $output;
    }

    my $header          = $data->{header};
    my $period          = $header->{period_us} / 1000000;
    my $timeOrigin      = $header->{time_origin}->{sec} + $header->{time_origin}->{usec} / 1000000;
    my $isDigital       = !($header->{meta} & SPY_BUF_META_ANA_SIGNALS);

    my $firstSampleTime;
    if($header->{meta} & SPY_BUF_META_TIMESTAMP_IS_LST)
    {
        my $lastSampleTime = $header->{timestamp}->{sec} + $header->{timestamp}->{usec} / 1000000;
        $firstSampleTime = $lastSampleTime - ($period * ($header->{n_samples} - 1));
    }
    else # Timestamp is first sample time
    {
        $firstSampleTime = $header->{timestamp}->{sec} + $header->{timestamp}->{usec} / 1000000;
    }

    $output .= "{\"source\":\"FGC\",";
    $output .= "\"device\":\"$device\",";
    $output .= "\"name\":\"$name\",";
    $output .= sprintf ("\"timeOrigin\":%.6f,", $timeOrigin);
    $output .= sprintf ("\"firstSampleTime\":%.6f,", $firstSampleTime);
    $output .= sprintf ("\"period\":%.6f,", $period);
    $output .= "\"version\":\"".JSON_CURRENT_VERSION."\",";

    if($isDigital)
    {
        $output .= "\"type\":\"digital\",";
    }
    else
    {
        $output .= "\"type\":\"analog\",";
    }


    # Add the signals
    $output .= "\n\"signals\":[";

    for (my $i = 0; $i < $nSignals; $i++)
    {
        my $signal = $signals[$i];

        # Add a comma to separate the signals after the first signal
        $output .= $i ? ',' : '';

        # Remove units from signal name

        my $signal_name = $signal->{name};
        $signal_name =~ s/_\(.*\)$//;

        $output .= "{\"name\":\"$signal_prefix$signal_name\",";

        # Add needed metadata
        if($signal->{meta} & SPY_SIG_META_STEPS)
        {
            $output .= "\"meta\":{\"step\":true},";
        }

        # Add samples for the signals
        $output .= "\"samples\":[";

        for(my $sample_index = 0; $sample_index < $header->{n_samples}; $sample_index++)
        {
            # Add a comma to separate the samples
            $output .=  $sample_index ? ',' : '';

            my $value = $signal->{samples}->[$sample_index]->{value} || 0;

            $output .= $isDigital ? sprintf('%d', $value) : sprintf("%.7E", $value);
        }
        $output .= "\n]}";
    }

    $output .= "]}";

    $output;
}

sub decode_header_v2($)
{
    my ($buffer) = @_;

    my %header;
    my $offset = 0;

    # Decode header
    (
        $header{version},   # Buffer version
        $header{meta},      # Bit mask for SPY_BUF_META_XXXX
    ) = unpack('CC', $$buffer);

    # Check whether the data is little-endian

    my $little_endian = $header{meta} & SPY_BUF_META_LITTLE_ENDIAN;

    # Decode the rest of the header

    (
        $header{n_signals},                 # Number of signals
        $header{n_samples},                 # Number of samples
        $header{time_origin}->{sec},        # Time origin (Unix time)
        $header{time_origin}->{usec},       # Time origin (microseconds)
        $header{timestamp}->{sec},          # Time of the first or last sample (Unix time)
        $header{timestamp}->{usec},         # Time of the first or last sample (microseconds)
        $header{period_us},                 # Sampling period
    ) = unpack($little_endian ? "xxvVVVVVV" : "xxnNNNNNN", $$buffer);

    $offset += 28;

    # Decode signal headers

    $header{signals} = [];
    for(my $i = 0 ; $i < $header{n_signals} ; $i++)
    {
        my %sig_header;

        (
            $sig_header{meta},              # Bit mask for SPY_SIG_META_XXXX
            $sig_header{dig_bit_index},     # Digital bit index (0-31)
            $sig_header{type},              # SPY_TYPE_XXXX (for analog data only)
            $sig_header{size},              # Element size in bytes (2 or 4) (for analog data only)
            $sig_header{gain},              # Displayed signal = gain * signal_value + offset if SPY_SIG_META_USE_SCALING is set in meta
            $sig_header{offset},            # See above
            $sig_header{time_offset_us},    # Time offset for this signal (microseconds)
            $sig_header{name},              # Signal name and (optional) units "NAME_(Units)" (null terminated)
        ) = unpack("x${offset}".
                   'C'.                             # meta
                   'C'.                             # dig_bit_index
                   'C'.                             # type
                   'C'.                             # size
                   ($little_endian ? 'V' : 'N').    # gain
                   ($little_endian ? 'V' : 'N').    # offset
                   ($little_endian ? 'V' : 'N').    # time_offset_us
                   'A'.SPY_SIG_NAME_LEN             # name
                   , $$buffer);

        # Convert floating-point values

        $sig_header{time_offset_us} = unpack('l', pack('L', $sig_header{time_offset_us}));
        $sig_header{gain}           = unpack('f', pack('L', $sig_header{gain}));

        push(@{$header{signals}}, \%sig_header);

        $offset += 40;
    }

    return(\%header, $offset);
}

# EOF
