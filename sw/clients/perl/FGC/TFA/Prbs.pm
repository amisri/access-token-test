package FGC::TFA::Prbs;

use strict;
use warnings;

use Time::HiRes  qw(usleep);

use Test::TCP;
use FGC::TFA::Common qw(getUnits waitForIdle);



use constant EXTRA_ACQ_DELAY_S => 3;



sub new
{
    my ($class, $api) = @_;

    my %object = ();

    $object{api} = $api;

    return bless \%object, $class;
}



sub getHeadings
{
    my @expected_headings =
    (
        "INITIAL_I_REF" ,
        "FINAL_I_REF"   ,
        "AMP_PP"        ,
        "PERIOD_ITERS"  ,
        "NUM_SEQUENCES" ,
        "K_ORDER"       ,
        "PRE_DELAY_S"   ,
    );

    return \@expected_headings;
}



sub composeFilename
{
    my ($self, $device, $output_path, $reg_mode) = @_;

    my $initial_i_ref  = $self->{INITIAL_I_REF};
    my $reg_mode_units = getUnits($reg_mode);
    my $ref_pp         = $self->{AMP_PP};

    my $filename = "$output_path/fgc_tfa_${device}_${initial_i_ref}A_${ref_pp}${reg_mode_units}pp.json";

    return $filename;
}



sub getIterationFrequency
{
    my ($device) = @_;

    my $fgc_class = get($device, "DEVICE.CLASS_ID");

    # For classes that use new cclibs, the property FGC.ITER_PERIOD can be used to determine the iteration frequency

    if($fgc_class =~ /^(63|92|94)$/)
    {
        eval
        {
            my $fgc_iter_period = get($device, "FGC.ITER_PERIOD");
            my $iter_freq = 1 / $fgc_iter_period;

            # Round to nearest integer and return

            $iter_freq = sprintf "%.0f", $iter_freq;
            return $iter_freq;
        };
    }

    # If reading fails and for older classes we return the estimated hardcoded value (there is no such property for older classes)

    return 10000 if($fgc_class =~ /^(61|62|63)$/);
    return 1000  if($fgc_class =~ /^(51|53|94)$/);
    return 100   if($fgc_class =~ /^(92)$/);

    die "ERROR: No iteration_frequency for class $fgc_class\n";
}



sub getRegulationFrequency
{
    my ($device, $desired_reg_mode) = @_;

    if($desired_reg_mode =~ /I|B/)
    {
        my $load_select      = get($device, "LOAD.SELECT");
        my $reg_period_iters = (split(/,/, get($device, "REG.$desired_reg_mode.PERIOD_ITERS")))[$load_select];
        my $reg_period       = get($device, "FGC.ITER_PERIOD") * $reg_period_iters;

        # Round to nearest integer

        my $reg_freq = sprintf("%.0f", (1 / $reg_period));

        return $reg_freq;
    }

    # For voltage regulation mode, the regulation frequency is always the same as iteration frequency

    return getIterationFrequency($device);
}



sub postProcess
{
    my ($self, $device, $output_path, $reg_mode) = @_;

    $self->{filename} = composeFilename($self, $device, $output_path, $reg_mode);

    my $period_s = $self->{PERIOD_ITERS} / getRegulationFrequency($device, $reg_mode);

    $self->{duration} = $period_s                     *
                        ((1 << $self->{K_ORDER}) - 1) *
                        $self->{NUM_SEQUENCES}        +
                        EXTRA_ACQ_DELAY_S             +
                        $self->{PRE_DELAY_S};
}



sub runSequence
{
    my ($self, $device, $log_menu_prop, $reg_mode) = @_;

    # Run the PRBS

    set($device, "REF.RUN", "");

    # Wait for the log buffer to fill

    my $sleep_time = $self->{duration};
    sleep($self->{duration});

    # Acquire the log buffer

    my $time_offset_ms = int($self->{duration} * 1000);
    my $buffer = get($device, $log_menu_prop . " bin data|0,$time_offset_ms,0,0");

    waitForIdle($device);

    return \$buffer;
}



sub teardownSequence
{
    my ($self, $device) = @_;

    # Go to final reference using current

    set($device, $self->{api}->{KEY_REG_MODE}, "I");
    usleep(500000);
    set($device, "REF", "NOW," . $self->{FINAL_I_REF});

    waitForIdle($device);
}



sub prepareSequence
{
    my ($self, $device, $reg_mode) = @_;

    set($device, "MODE.PC", "IDLE");
    waitForIdle($device);

    # Ramp up to initial current ref

    set($device, $self->{api}->{KEY_REG_MODE}, "I");
    usleep(500000);
    set($device, "REF", "NOW," . $self->{INITIAL_I_REF});
    waitForIdle($device);

    # Wait for all oscillations to die out

    sleep($self->{PRE_DELAY_S});

    # Set the desired regulation mode and arm PRBS

    set($device, $self->{api}->{KEY_REG_MODE}, $reg_mode);
    usleep(500000);
    set($device, $self->{api}->{KEY_PRBS_INITIAL},      $self->{INITIAL_I_REF});
    set($device, $self->{api}->{KEY_PRBS_AMP},          $self->{AMP_PP});
    set($device, $self->{api}->{KEY_PRBS_PERIOD_ITERS}, $self->{PERIOD_ITERS});
    set($device, $self->{api}->{KEY_PRBS_NUM_SEQ},      $self->{NUM_SEQUENCES});
    set($device, $self->{api}->{KEY_PRBS_K},            $self->{K_ORDER});
    set($device, "REF.FUNC.TYPE", "PRBS");
}

1;

# EOF
