package FGC::TFA::Sine;

use strict;
use warnings;
use diagnostics;

use Time::HiRes qw(usleep);
use POSIX       qw(ceil floor);

use Test::TCP;
use Test::Utils;
use FGC::TFA::Common qw(getUnits waitForIdle);



use constant EXTRA_ACQ_DELAY_S => 5;



sub new
{
    my ($class, $api) = @_;

    my %object = ();

    $object{api} = $api;

    return bless \%object, $class;
}



sub getHeadings
{
    my @expected_headings =
    (
        "INITIAL_I_REF" ,
        "FINAL_I_REF"   ,
        "AMP_PP"        ,
        "PERIOD_S"      ,
        "PRE_DELAY_S"   ,
        "ACQ_DELAY_S"   ,
        "ACQ_DURATION_S",
        "POST_DELAY_S"  ,
    );

    return \@expected_headings;
}



sub composeFilename
{
    my ($device, $output_path, $self, $reg_mode) = @_;

    my $initial_i_ref  = $self->{INITIAL_I_REF};
    my $reg_mode_units = getUnits($reg_mode);
    my $index          = sprintf("%02d", $self->{index});

    my $filename;

    if($self->{is_dc})
    {
        $filename = "${output_path}/fgc_tfa_${device}_${index}_${initial_i_ref}A_DC.json";
    }
    else
    {
        my $freq   = sprintf("%.6f", 1 / $self->{PERIOD_S});
        my $ref_pp = $self->{AMP_PP};

        $filename = "${output_path}/fgc_tfa_${device}_${index}_${initial_i_ref}A_${ref_pp}${reg_mode_units}pp_${freq}Hz.json";
    }

    return $filename;
}



sub getPropertyRange
{
    my ($device, $property) = @_;

    # Here we assume that the returned value looks like (min, max) value(s)

    my $value = get($device, "$property RANGE");

    # Remove '(' and ')' from the string

    $value =~ s/[\(|\)]//g;

    # Return min and max

    return (split " ", $value)[0, 1];
}



sub checkAgainstPropRange
{
    my ($device, $value, $property, $label, $excitation_index) = @_;

    my ($min, $max) = getPropertyRange($device, $property);

    if($value < $min || $value > $max)
    {
        die "$label must be in range ($min, $max) (found " . $value . ") in excitation No. $excitation_index";
    }
}



sub postProcess
{
    my ($self, $device, $output_path, $reg_mode) = @_;

    # Add excitation sequence index for convenience

    my $excitation_index = $self->{index};

    if($self->{PERIOD_S} != 0)
    {
        checkAgainstPropRange($device,
                              $self->{PERIOD_S},
                              "REF.TEST.PERIOD",
                              "PERIOD_S",
                              $excitation_index);

        # Calculate number of periods based on user input. Round up to nearest integer to ensure
        # that the sine wave will be playing during the entire acquisition time

        $self->{num_periods} = ceil(($self->{ACQ_DELAY_S}    +
                                     $self->{ACQ_DURATION_S} +
                                     EXTRA_ACQ_DELAY_S)      /
                                     $self->{PERIOD_S});

        checkAgainstPropRange($device,
                              $self->{num_periods},
                              "REF.TEST.NUM_PERIODS",
                              "num_periods",
                              $excitation_index);

        # Clip acquisition duration to span over a full number of sine wave periods.
        # Having full number of periods in the acquisitions makes processing easier.
        # Acquisition duration is deliberately altered after calculation of num_periods!

        my $num_periods_in_acq = floor($self->{ACQ_DURATION_S} / $self->{PERIOD_S});

        $self->{ACQ_DURATION_S} = $num_periods_in_acq * $self->{PERIOD_S};
    }

    # Estimate excitation duration. This calculation doesn't take everything into account, but
    # it should give a good duration approximation to the user.

    $self->{duration} = $self->{PRE_DELAY_S}    +
                        $self->{ACQ_DELAY_S}    +
                        $self->{ACQ_DURATION_S} +
                        $self->{POST_DELAY_S}   +
                        EXTRA_ACQ_DELAY_S;

    # Excitation is a DC measurement when either amplitude or period is 0

    $self->{is_dc} = (($self->{AMP_PP} == 0 || $self->{PERIOD_S} == 0) ? 1 : 0);

    # Generate output file name for excitation

    $self->{filename} = composeFilename($device, $output_path, $self, $reg_mode);
}



sub runSequence
{
    my ($self, $device, $log_menu_prop, $reg_mode) = @_;

    # Run the cosine

    if($self->{is_dc})
    {
        set($device, $self->{api}->{KEY_REG_MODE}, $reg_mode)
    }
    else
    {
        set($device, "REF.RUN", "");
    }

    # Wait for the log buffer to fill

    my $sleep_time = $self->{ACQ_DELAY_S} + 1 + $self->{ACQ_DURATION_S};
    sleep($sleep_time);

    # Acquire the log buffer

    my $time_offset_ms = int($self->{ACQ_DURATION_S} * 1000);
    my $buffer = get($device, $log_menu_prop . " bin data|0,$time_offset_ms,0,0");

    # Early abort, should slightly reduce total run time

    set($device, "REF.ABORT", "");
    waitForIdle($device);

    return \$buffer;
}



sub teardownSequence
{
    my ($self, $device) = @_;

    # Go to final reference using current

    set($device, $self->{api}->{KEY_REG_MODE}, "I");
    usleep(500000);
    set($device, "REF", "NOW," . $self->{FINAL_I_REF});

    waitForIdle($device);

    # Wait for the components (converter, load) to cool down

    sleep($self->{POST_DELAY_S});
}



sub prepareSequence
{
    my ($self, $device, $reg_mode) = @_;

    set($device, "MODE.PC", "IDLE");
    waitForIdle($device);

    # Ramp up to initial current ref

    set($device, $self->{api}->{KEY_REG_MODE}, "I");
    usleep(500000);
    set($device, "REF", "NOW," . $self->{INITIAL_I_REF});
    waitForIdle($device);

    # Wait for all oscillations to die out

    sleep($self->{PRE_DELAY_S});

    # Set the desired regulation mode and arm a cosine wave

    if(!$self->{is_dc})
    {
        set($device, $self->{api}->{KEY_REG_MODE}, $reg_mode);
        usleep(500000);
        set($device, "REF", "COSINE," . $self->{AMP_PP} . "," . $self->{num_periods} . "," . $self->{PERIOD_S});
    }
}

1;

# EOF
