package FGC::TFA::Common;

use Array::Utils qw(intersect);

use Test::TCP;
use Test::FGC;

require Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                     getUnits
                     waitForIdle
                );



sub waitForIdle
{
    my ($device) = @_;

    # Wait for 1s to avoid premature success of the waiting loop. This might happen for
    # example after setting REF.ABORT with default delay of 1s or REF NOW, which always
    # has a delay of 1s

    sleep(1);

    fgc_wait_until(sub {
                       my @allowed_states = ("IDLE", "ARMED", "RUNNING", "ABORTING", "ON_STANDBY");

                       my $state_pc = get($device, "STATE.PC");

                       if(not intersect(@allowed_states, @{[$state_pc]}))
                       {
                           die "Invalid STATE.PC - $state_pc, aborting";
                       }

                       return ($state_pc eq "IDLE");
                   }, 600, 1);
}



sub getUnits
{
    my ($reg_mode) = @_;

    my %units =
    (
        B => "G",
        I => "A",
        V => "V",
    );

    return $units{$reg_mode};
}

1;

# EOF
