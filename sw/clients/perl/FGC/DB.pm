#!/usr/bin/perl -w
#
# Filename: FGC/DB.pm
# Purpose:  Access functions for the LHC Powering Database
# Author:   Stephen Page

package FGC::DB;

use DBI;
use Exporter;
use strict;
use Carp;
use Data::Dumper;
use FGC::Consts;
use FGC::Utils;

our @ISA        = qw(Exporter);
our @EXPORT     = qw(
                        FGC_DB_PASSWORD
                        FGC_DB_USERNAME
                    );
our @EXPORT_OK  = qw(
                        connect
                        disconnect

                        get_property_details

                        get_types
                        get_systems
                        get_system_names
                        get_system_components
                        get_component_mappings
                        get_component_ids_of_type
                        get_component_details
                        get_component_properties_with_null_value
                        get_component_children

                        add_system_components
                        remove_system_components
                        remove_components_from_all_systems
                        set_component_channel
                        set_component_parents
                        add_children_to_parents_systems

                        get_component_properties
                        get_system_properties
                        get_type_properties

                        update_component_property_values
                        update_component_barcode_properties
                        update_system_property_values
                        update_type_property_values

                        add_system
                        update_system
                        rename_system
                        add_property

                        add_component_property
                        add_system_property
                        add_type_property

                        remove_system_properties

                        get_system_details
                        get_barcode_types
                        get_instances_by_class_and_property
                    );

# Constants

use constant FGC_DB_ID          => 'dbi:Oracle:dbabco'; # ID of FGC database
use constant FGC_DB_PASSWORD    => 'p0published!!';      # Public password for FGC database
use constant FGC_DB_USERNAME    => 'pocontrols_pub';    # Public username for FGC database


my $connect_count = 0;

return(1);

# Begin functions

# Connect to database

sub connect($$)
{
    my ($username, $password) = @_;

    my $dbh = DBI->connect(FGC_DB_ID,
                            $username,
                            $password,
                            {AutoCommit => 0,
                            PrintError => 1,
                            RaiseError => 0,
                            LongReadLen => FGC::Consts::FGC_MAX_DB_PROP_SIZE_BYTES});
    if(!$dbh) {
    	print $!;
    	print DBI->errstr();
    }
    $connect_count++;

    return($dbh);
}

# Disconnect from database

sub disconnect($)
{
    my ($dbh) = @_;

    $dbh->disconnect;
}

# Get details of properties

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_property_details($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT PRO_NAME
                                                     FROM POCONTROLS.FGC_PROPERTIES
                                                     ORDER BY PRO_NAME") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            PRO_NAME => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get Types

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_types($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT TP_ID, TP_COMPONENT_CODE, TP_SEQ_NUMBER, TP_NAME, TP_DESCRIPTION, TP_CATEGORY, TP_MULTI_SYS
                                                     FROM POCONTROLS.FGC_TYPES
                                                     ORDER BY TP_COMPONENT_CODE") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            TP_ID => 1,
                                            TP_COMPONENT_CODE => 1,
                                            TP_SEQ_NUMBER => 1,
                                            TP_NAME => 1,
                                            TP_DESCRIPTION => 1,
                                            TP_CATEGORY => 1,
                                            TP_MULTI_SYS => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get systems

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_systems($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT SYS_ID, SYS_NAME, SYS_TP_ID
                                                     FROM POCONTROLS.FGC_SYSTEMS
                                                     ORDER BY SYS_NAME") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            SYS_ID => 1,
                                            SYS_NAME => 1,
                                            SYS_TP_ID => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get system names

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_system_names($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT SYS_NAME
                                                     FROM POCONTROLS.FGC_SYSTEMS
                                                     ORDER BY SYS_NAME") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            SYS_NAME => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get components installed in a system

{
    my $parent_query;
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_system_components($$)
    {
        my ($dbh, $system_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT COMPONENT_CHANNEL, COMPONENT_MTF_ID, COMPONENT_PARENT_ID, TP_NAME, TP_MULTI_SYS
                                                     FROM POCONTROLS.FGC_SYSTEMS_COMPOSITION, POCONTROLS.FGC_TYPES
                                                     WHERE COMPONENT_TYPE_ID=TP_ID AND SYSTEM_NAME=?")  or return(undef);
            $parent_query           = $dbh->prepare("SELECT COMPONENT_CHANNEL, COMPONENT_MTF_ID, COMPONENT_PARENT_ID, TP_NAME, TP_MULTI_SYS
                                                     FROM POCONTROLS.FGC_SYSTEMS_COMPOSITION, POCONTROLS.FGC_TYPES
                                                     WHERE COMPONENT_TYPE_ID=TP_ID AND COMPONENT_ID=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            COMPONENT_CHANNEL   => 1,
                                            COMPONENT_MTF_ID    => 1,
                                            COMPONENT_PARENT_ID => 1,
                                            TP_MULTI_SYS        => 1,
                                            TP_NAME             => 1,
                                        );
        }

        # Execute query

        $query->execute($system_name) or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert results into hash mapping component ID to details

        my %components;
        for my $result (@$results)
        {
            $components{$result->{COMPONENT_MTF_ID}} = $result;

            # Get MTF ID for component's parent

            if(defined($result->{COMPONENT_PARENT_ID}))
            {
                $parent_query->execute($result->{COMPONENT_PARENT_ID}) or return(undef);
                my $parent_results = $parent_query->fetchall_arrayref(\%result_hash);

                if(defined($parent_results->[0]))
                {
                    $result->{PARENT_MTF_ID} = $parent_results->[0]->{COMPONENT_MTF_ID};
                }
            }
        }

        # Return components

        return(\%components);
    }

}

# Get values for components installed in a system

{
    my $value_query;
    my $component_query;
    my $query_connect_count;
    my %result_hash;
    my %component_hash;

    sub get_system_components_values($$)
    {
        my ($dbh, $system_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $component_query           = $dbh->prepare("SELECT COMPONENT_MTF_ID
                                                     FROM POCONTROLS.FGC_SYSTEMS_COMPOSITION
                                                     WHERE SYSTEM_NAME=?")  or return(undef);
            $value_query           = $dbh->prepare("SELECT PROPERTY_NAME, COMPONENT_PROPERTY_VALUE, COMPONENT_CHANNEL, COMPONENT_MTF_ID
                                                     FROM POCONTROLS.FGC_COMPONENT_PROPERTIES_V
                                                     WHERE COMPONENT_MTF_ID=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            PROPERTY_NAME   => 1,
                                            COMPONENT_PROPERTY_VALUE   => 1,
                                            COMPONENT_CHANNEL   => 1,
                                            COMPONENT_MTF_ID    => 1,
                                        );
            %component_hash            =   (
                                            COMPONENT_MTF_ID    => 1,
                                        );
        }

        # Execute query

        $component_query->execute($system_name) or return(undef);

        # Get array of result row hashes

        my $results_mtf = $component_query->fetchall_arrayref(\%component_hash);

        # Convert results into hash mapping component ID to details

        my %components;
        for my $result (@$results_mtf)
        {
            # Get value for components

            $value_query->execute($result->{COMPONENT_MTF_ID}) or return(undef);
            my $value_results = $value_query->fetchall_arrayref(\%result_hash);

            for my $value_result (@$value_results)
            {
                $value_result->{PROPERTY_NAME} =~ s/\?/$value_result->{COMPONENT_CHANNEL}/g;
                $components{$value_result->{PROPERTY_NAME}} = $value_result->{COMPONENT_PROPERTY_VALUE};
            }
        }

        # Return components

        return(\%components);
    }
}

# Get HCR / ID associations
# optional: specify hcr and/or dallas_id to filter for
{
	my $query;
	my $query_connect_count;

    sub get_component_mappings($; $$)
    {
        my ($dbh, $hcr, $dallas_id) = @_;

        my @conditions;
        my @filters;

        my $sql = "SELECT CMA_CMP_ID, CMA_CHIP_ID FROM POCONTROLS.FGC_COMPONENT_MAPPING";

        if ($hcr)
        {
            push(@filters, "CMA_CMP_ID = ?");
            push(@conditions, $hcr);
        }

        if($dallas_id)
        {
            push(@filters, "CMA_CHIP_ID = ?");
            push(@conditions, $dallas_id);
        }

        $sql.= ' WHERE '.join(' AND ',@filters) if(@filters);

        $query                  = $dbh->prepare($sql) or return(undef);
        $query_connect_count    = $connect_count;

        # Execute query
        $query->execute(@conditions) or return(undef);
        my $hcr_ids = $query->fetchall_arrayref({});

        # Return array of component IDs
        return($hcr_ids);
    }
}

# Get IDs of all components of type

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_component_ids_of_type($$)
    {
        my ($dbh, $type_id) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CMP_MTF_ID
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_TYPES
                                                     WHERE CMP_TP_ID=TP_ID AND TP_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            CMP_MTF_ID  => 1,
                                        );
        }

        # Execute query

        $query->execute($type_id) or return(undef);
        my @component_ids = map { $_->{CMP_MTF_ID} } @{$query->fetchall_arrayref(\%result_hash)};

        # Return array of component IDs

        return(\@component_ids);
    }
}

# Get all IDs of all components that have a NULL value

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_component_properties_with_null_value($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CMP_MTF_ID, PRO_NAME
                                                     FROM POCONTROLS.FGC_COMPONENTS,
                                                          POCONTROLS.FGC_PROPERTIES,
                                                          POCONTROLS.FGC_COMPONENT_PROPERTIES
                                                     WHERE CMP_ID=CPR_CMP_ID AND
                                                           PRO_ID=CPR_PRO_ID AND
                                                           CPR_VALUE IS NULL") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            CMP_MTF_ID  => 1,
                                            PRO_NAME    => 1
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert results into hash mapping property name to value

        my %components;

        for my $row (@$results)
        {
            if(!exists($components{ $row->{CMP_MTF_ID} }))
            {
                my @properties;
                $components{ $row->{CMP_MTF_ID} } = \@properties;
            }

            # push the property to an array

            push( @{ $components { $row->{CMP_MTF_ID} } }, $row->{PRO_NAME});
        }

        return(\%components);
    }
}
# Get details for components

{
    my $parent_query;
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_component_details($$)
    {
        my ($dbh, $component_ids) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CMP_CHANNEL, CMP_MTF_ID, CMP_PARENT_ID, TP_NAME, TP_MULTI_SYS
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_TYPES
                                                     WHERE CMP_TP_ID=TP_ID AND CMP_MTF_ID=?") or return(undef);
            $parent_query           = $dbh->prepare("SELECT CMP_CHANNEL, CMP_MTF_ID, CMP_PARENT_ID, TP_NAME, TP_MULTI_SYS
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_TYPES
                                                     WHERE CMP_TP_ID=TP_ID AND CMP_ID=?")     or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            CMP_CHANNEL     => 1,
                                            CMP_MTF_ID      => 1,
                                            CMP_PARENT_ID   => 1,
                                            TP_MULTI_SYS    => 1,
                                            TP_NAME         => 1,
                                        );
        }

        my %components;

        for my $component_id (@$component_ids)
        {
            # Execute query

            $query->execute($component_id) or return(undef);

            # Get array of result row hashes

            my $results = $query->fetchall_arrayref(\%result_hash);

            # Convert results into hash mapping component ID to details

            for my $result (@$results)
            {
                # Rename fields to map those obtained from SYSTEMS_COMPOSITION

                my %renamed_result;
                for my $field (keys(%$result))
                {
                    (my $new_field = $field) =~ s/CMP/COMPONENT/;

                    $renamed_result{$new_field} = $result->{$field};
                }
                $result = \%renamed_result;

                $components{$result->{COMPONENT_MTF_ID}} = $result;

                # Get MTF ID for component's parent

                if(defined($result->{COMPONENT_PARENT_ID}))
                {
                    $parent_query->execute($result->{COMPONENT_PARENT_ID}) or return(undef);
                    my $parent_results = $parent_query->fetchall_arrayref(\%result_hash);

                    if(defined($parent_results->[0]))
                    {
                        $result->{PARENT_MTF_ID} = $parent_results->[0]->{CMP_MTF_ID};
                    }
                }
            }
        }

        # Return components

        return(\%components);
    }
}

# Get component children

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_component_children($$)
    {
        my ($dbh, $parent_id) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CMP_CHANNEL, CMP_MTF_ID, CMP_PARENT_ID, TP_NAME, TP_MULTI_SYS
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_TYPES
                                                     WHERE CMP_TP_ID=TP_ID
                                                     AND CMP_PARENT_ID=(SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?)") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            CMP_CHANNEL     => 1,
                                            CMP_MTF_ID      => 1,
                                            CMP_PARENT_ID   => 1,
                                            TP_MULTI_SYS    => 1,
                                            TP_NAME         => 1,
                                        );
        }

        # Execute query

        $query->execute($parent_id) or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert results into hash mapping component ID to details

        my %components;
        for my $result (@$results)
        {
            # Rename fields to map those obtained from SYSTEMS_COMPOSITION

            my %renamed_result;
            for my $field (keys(%$result))
            {
                (my $new_field = $field) =~ s/CMP/COMPONENT/;

                $renamed_result{$new_field} = $result->{$field};
            }
            $result = \%renamed_result;

            $components{$result->{COMPONENT_MTF_ID}} = $result;

            # Set MTF ID for component's parent

            $result->{PARENT_MTF_ID} = $parent_id;
        }

        # Return components

        return(\%components);
    }
}

# Get systems that component is installed in

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_component_systems($$)
    {
        my ($dbh, $component_id) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT SYSTEM_NAME
                                                     FROM POCONTROLS.FGC_SYSTEMS_COMPOSITION
                                                     WHERE COMPONENT_MTF_ID=?
                                                     ORDER BY SYSTEM_NAME") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            SYSTEM_NAME => 1,
                                        );
        }

        # Execute query

        $query->execute($component_id) or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Remove components and their children from all systems

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub remove_components_from_all_systems($$)
    {
        my ($dbh, $component_ids) = @_;
        my $failed            = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("DELETE FROM POCONTROLS.FGC_COMPONENT_SYSTEMS
                                                     WHERE CS_CMP_ID IN (SELECT CMP_ID
                                                                         FROM POCONTROLS.FGC_COMPONENTS
                                                                         WHERE CMP_MTF_ID=?
                                                                         OR CMP_PARENT_ID=(SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?))") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Remove each component

        for my $component_id (@$component_ids)
        {
            # Execute query

            my $num = $query->execute($component_id, $component_id);
            if (!$num)
            {
                print("\tFailed to remove $component_id from POCONTROLS.FGC_COMPONENT_SYSTEMS\n");
                $failed = 1;
            }
        }

        # Check the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }

    }
}

# Get properties for a system

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_system_properties($$)
    {
        my ($dbh, $system_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT PROPERTY_NAME, SYSTEM_PROPERTY_VALUE
                                                     FROM POCONTROLS.FGC_SYSTEM_PROPERTIES_V
                                                     WHERE SYSTEM_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            PROPERTY_NAME           => 1,
                                            SYSTEM_PROPERTY_VALUE   => 1,
                                        );
        }

        # Execute query
        $query->execute($system_name) or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert results into hash mapping property name to value

        my %properties = map { $_->{PROPERTY_NAME} => $_->{SYSTEM_PROPERTY_VALUE} } (@$results);

        # Return properties

        return(\%properties);
    }
}

# Get properties for an component

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_component_properties($$)
    {
        my ($dbh, $component_id) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT PROPERTY_NAME, COMPONENT_PROPERTY_VALUE
                                                     FROM POCONTROLS.FGC_COMPONENT_PROPERTIES_V
                                                     WHERE COMPONENT_MTF_ID=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            COMPONENT_PROPERTY_VALUE    => 1,
                                            PROPERTY_NAME               => 1,
                                        );
        }

        # Execute query

        $query->execute($component_id) or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert results into hash mapping property name to value

        my %properties = map { $_->{PROPERTY_NAME} => $_->{COMPONENT_PROPERTY_VALUE} } (@$results);

        # Return properties

        return(\%properties);
    }
}

# Get properties for an component type

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_type_properties($$)
    {
        my ($dbh, $type_id) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT PROPERTY_NAME, TYPE_PROPERTY_VALUE
                                                     FROM POCONTROLS.FGC_TYPE_PROPERTIES_V
                                                     WHERE TYPE_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            PROPERTY_NAME       => 1,
                                            TYPE_PROPERTY_VALUE => 1,
                                        );
        }

        # Execute query

        $query->execute($type_id) or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert results into hash mapping property name to value

        my %properties = map { $_->{PROPERTY_NAME} => $_->{TYPE_PROPERTY_VALUE} } (@$results);

        # Return properties

        return(\%properties);
    }
}

# Add components to system

{
    my $query;
    my $query_connect_count;

    sub add_system_components($$$)
    {
        my ($dbh, $system_name, $component_ids) = @_;
        my $failed                          = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("BEGIN INSERT INTO POCONTROLS.FGC_COMPONENT_SYSTEMS (CS_CMP_ID, CS_SYS_ID)
                                                     VALUES ((SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?), (SELECT SYS_ID FROM POCONTROLS.FGC_SYSTEMS WHERE SYS_NAME=?));
                                                     EXCEPTION WHEN DUP_VAL_ON_INDEX THEN NULL; END;") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Add each component

        for my $component_id (@$component_ids)
        {
            # Get component's children

            my $children = get_component_children($dbh, $component_id);
            return(undef) if(!defined($children));

            # Add all children

            for my $child (values(%$children))
            {
                my $num = $query->execute($child->{COMPONENT_MTF_ID}, $system_name);
                if (!$num)
                {
                    print("\tFailed to add component $child->{COMPONENT_MTF_ID} to system $system_name\n");
                    $failed = 1;
                }
            }

            # Execute query

            my $num = $query->execute($component_id, $system_name);
            if (!$num)
            {
                print("\tFailed to add component $component_id to system $system_name\n");
                $failed = 1;
             }
        }

        # Check the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Remove components from system

{
    my $query;
    my $query_connect_count;

    sub remove_system_components($$$)
    {
        my ($dbh, $system_name, $component_ids) = @_;
        my $failed                          = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("DELETE FROM POCONTROLS.FGC_COMPONENT_SYSTEMS
                                                     WHERE CS_CMP_ID IN (SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=? OR CMP_PARENT_ID=(SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?))
                                                     AND CS_SYS_ID=(SELECT SYS_ID FROM POCONTROLS.FGC_SYSTEMS WHERE SYS_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Remove each component

        for my $component_id (@$component_ids)
        {
            # Execute query

            my $num = $query->execute($component_id, $component_id, $system_name);
            if (!$num)
            {
                print("\tFailed to remove component $component_id from system $system_name\n");
                $failed = 1;
            }
        }

        # Check the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Set channel for components

{
    my $query;
    my $query_connect_count;

    sub set_component_channel($$$)
    {
        my ($dbh, $channel, $component_ids) = @_;
        my $failed                      = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_COMPONENTS
                                                     SET CMP_CHANNEL=?
                                                     WHERE CMP_MTF_ID=? OR CMP_PARENT_ID=(SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Set channel for each component

        for my $component_id (@$component_ids)
        {
            # Execute query

            my $num = $query->execute($channel, $component_id, $component_id);
            if (!$num)
            {
                print("\tFailed to update channel $channel for component $component_id\n");
                $failed = 1;
            }
        }

        # Check the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Set parents for components

{
    my $query;
    my $query_connect_count;

    sub set_component_parents($$)
    {
        my ($dbh, $child_parent) = @_;
        my $failed               = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_COMPONENTS
                                                     SET
                                                     CMP_PARENT_ID=(SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?),
                                                     CMP_CHANNEL=(SELECT CMP_CHANNEL FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?)
                                                     WHERE CMP_MTF_ID=?") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Set channel for each component

        for my $child_id (keys(%$child_parent))
        {
            # Execute query

            my $num = $query->execute($child_parent->{$child_id}, $child_parent->{$child_id}, $child_id);
            if (!$num)
            {
                print("\tFailed to update component $child_id to a new parent component $child_parent->{$child_id}\n");
                $failed = 1;
            }
        }

        # Check the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Add children to parents' systems

{
    my $query;
    my $query_connect_count;

    sub add_children_to_parents_systems($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POCONTROLS.FGC_COMPONENT_SYSTEMS
                                                     (CS_CMP_ID, CS_SYS_ID)
                                                     (SELECT FGC_COMPONENTS.CMP_ID, FGC_COMPONENT_SYSTEMS.CS_SYS_ID
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_COMPONENT_SYSTEMS
                                                     WHERE FGC_COMPONENTS.CMP_PARENT_ID=FGC_COMPONENT_SYSTEMS.CS_CMP_ID
                                                     AND CMP_PARENT_ID IS NOT null
                                                     AND FGC_COMPONENTS.CMP_ID NOT IN (SELECT CS_CMP_ID FROM POCONTROLS.FGC_COMPONENT_SYSTEMS))") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        if(!$query->execute())
        {
            print("\tFailed to insert children components to parent systems\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Update property values for a system

{
    my $query;
    my $query_connect_count;

    sub update_system_property_values($$$)
    {
        my ($dbh, $system_name, $properties) = @_;
        my $failed = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_SYSTEM_PROPERTIES
                                                     SET SPR_VALUE=?
                                                     WHERE SPR_SYS_ID=(SELECT SYS_ID FROM POCONTROLS.FGC_SYSTEMS WHERE SYS_NAME=?)
                                                     AND SPR_PRO_ID=(SELECT PRO_ID FROM POCONTROLS.FGC_PROPERTIES WHERE PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Update each property value

        for my $property (sort(keys(%$properties)))
        {
            # Print update
            my $property_value_str = defined($properties->{$property}) ? $properties->{$property} : '<null>';
            print("\tUpdating property of $system_name: $property='$property_value_str'\n");

            # Execute query allowing partial failures
            my $num = $query->execute($properties->{$property}, $system_name, $property);
            if (!$num)
            {
                print("\tFailed to update: ", $system_name,",",$properties->{$property},",", $property,"\n");
                $failed = 1;
            }
        }

	    # Check the commit

	    if (!$dbh->commit)
	    {
	        print("\tFailed to commit\n");
	        $failed = 1;
	    }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Update property values for an component

{
    my $query;
    my $query_connect_count;

    sub update_component_property_values($$$)
    {
        my ($dbh, $component_id, $properties) = @_;
	    my $failed = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_COMPONENT_PROPERTIES
                                                     SET CPR_VALUE=?
                                                     WHERE CPR_CMP_ID=(SELECT CMP_ID FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?)
                                                     AND CPR_PRO_ID=(SELECT PRO_ID FROM POCONTROLS.FGC_PROPERTIES WHERE PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Update each property value

        for my $property (keys(%$properties))
        {
            # Execute query allowing partial failures

             my $num = $query->execute($properties->{$property}, $component_id, $property);
             if (!$num)
             {
                 print("\tFailed to update: ", $component_id,",",$properties->{$property},",", $property,"\n");
                 $failed = 1;
             }
        }

	    # Check also the commit

        if (!$dbh->commit)
	    {
	        print("\tFailed to commit\n");
	        $failed = 1;
	    }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Update property values for an component type

{
    my $query;
    my $query_connect_count;

    sub update_type_property_values($$$)
    {
        my ($dbh, $type_id, $properties) = @_;
        my $failed                       = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_TYPE_PROPERTIES
                                                     SET TPR_VALUE=?
                                                     WHERE TPR_TP_ID=(SELECT TP_ID FROM POCONTROLS.FGC_TYPES WHERE TP_NAME=?)
                                                     AND TPR_PRO_ID=(SELECT PRO_ID FROM POCONTROLS.FGC_PROPERTIES WHERE PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Update each property value

        for my $property (keys(%$properties))
        {
            # Execute query

            my $num = $query->execute($properties->{$property}, $type_id, $property);
            if (!$num)
            {
                print("\tFailed to update: ", $type_id,",",$properties->{$property},",",$property,"\n");
                $failed = 1;
            }
        }

        # Check also the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

{
    my $query;
    my $query_connect_count;

    sub update_component_barcode_properties($)
    {
        my ($dbh)  = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            # Update all barcode component properties whose value is null or different from their associated COMPONENT's mtf_id
            $query = $dbh->prepare("MERGE INTO POCONTROLS.FGC_COMPONENT_PROPERTIES cp
                                    USING (
                                        SELECT CPR_ID, CMP_MTF_ID
                                        FROM POCONTROLS.FGC_COMPONENT_PROPERTIES, POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_PROPERTIES
                                        WHERE CPR_CMP_ID = CMP_ID AND PRO_ID = CPR_PRO_ID AND PRO_NAME LIKE 'BARCODE.%'
                                        AND (CPR_VALUE is null OR CPR_VALUE != CMP_MTF_ID)
                                        ) src
                                    ON  (cp.CPR_ID = src.CPR_ID)
                                    WHEN MATCHED THEN UPDATE
                                        SET cp.CPR_VALUE = src.CMP_MTF_ID") or return(undef);

            $query_connect_count    = $connect_count;
        }

        my $num = $query->execute();

        if (!$num)
        {
            print("\tFailed to update value of barcode properties\n");
            return(undef);
        }

        # Check also the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }

        return($num);
    }
}

# Add a system

{
    my $query;
    my $query_connect_count;

    sub add_system($$$)
    {
        my ($dbh, $name, $type) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POCONTROLS.FGC_SYSTEMS
                                                     (SYS_ID, SYS_NAME, SYS_TP_ID)
                                                     SELECT POCONTROLS.FGC_SYSTEMS_SEQ.NEXTVAL, ?, (select TP_ID from POCONTROLS.FGC_TYPES WHERE TP_COMPONENT_CODE = ? and ROWNUM = 1)
                                                     FROM DUAL
                                                     WHERE NOT EXISTS
                                                     (SELECT 1
                                                     FROM POCONTROLS.FGC_SYSTEMS
                                                     WHERE SYS_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        my $result = $query->execute($name, $type, $name);

        if (!defined($result))
        {
            print("\tFailed to insert system $name\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return($result);
        }
    }
}

# Update a system

{
    my $query;
    my $query_connect_count;

    sub update_system($$$)
    {
        my ($dbh, $name, $type) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_SYSTEMS
                                                     SET SYS_TP_ID = (select TP_ID from POCONTROLS.FGC_TYPES WHERE TP_COMPONENT_CODE = ?)
                                                     WHERE SYS_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        my $result = $query->execute($type, $name);

        if (!defined($result))
        {
            print("\tFailed to update system $name\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return($result);
        }
    }
}

# Rename a system

{
    my $query;
    my $query_connect_count;

    sub rename_system($$$)
    {
        my ($dbh, $old_name, $new_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("UPDATE POCONTROLS.FGC_SYSTEMS
                                                     SET SYS_NAME=?
                                                     WHERE SYS_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        if(!$query->execute($new_name, $old_name))
        {
            print("\tFailed to update system name $old_name --> $new_name\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Add a property

{
    my $query;
    my $query_connect_count;

    sub add_property($$)
    {
        my ($dbh, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POCONTROLS.FGC_PROPERTIES
                                                     (PRO_ID, PRO_NAME)
                                                     SELECT POCONTROLS.FGC_PROPERTIES_SEQ.NEXTVAL, ?
                                                     FROM DUAL
                                                     WHERE NOT EXISTS
                                                     (SELECT 1
                                                     FROM POCONTROLS.FGC_PROPERTIES
                                                     WHERE PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        my $result = $query->execute($property, $property);

        if(!defined($result))
        {
            print("\tFailed to insert property name $property\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return($result);
        }
    }
}

# Add a system property

{
    my $query;
    my $query_connect_count;

    sub add_system_property($$$$)
    {
        my ($dbh, $system_name, $property, $value) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POCONTROLS.FGC_SYSTEM_PROPERTIES
                                                     (SPR_ID, SPR_SYS_ID, SPR_PRO_ID, SPR_VALUE)
                                                     SELECT POCONTROLS.FGC_SYSPROP_SEQ.NEXTVAL, SYS_ID, PRO_ID, ?
                                                     FROM POCONTROLS.FGC_SYSTEMS, POCONTROLS.FGC_PROPERTIES
                                                     WHERE SYS_NAME=?
                                                     AND PRO_NAME=?
                                                     AND NOT EXISTS
                                                     (SELECT 1
                                                     FROM POCONTROLS.FGC_SYSTEMS, POCONTROLS.FGC_SYSTEM_PROPERTIES, POCONTROLS.FGC_PROPERTIES
                                                     WHERE SPR_SYS_ID=SYS_ID
                                                     AND SYS_NAME=?
                                                     AND SPR_PRO_ID=PRO_ID
                                                     AND PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        my $result = $query->execute($value, $system_name, $property, $system_name, $property);

        if(!defined($result))
        {
            print("\tFailed to insert property $property for $system_name\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return($result);
        }
    }
}

# Add an component property to all COMPONENT of a given type

{
    my $query;
    my $query_connect_count;

    sub add_component_property($$$$)
    {
        my ($dbh, $type_id, $property, $value) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POCONTROLS.FGC_COMPONENT_PROPERTIES
                                                     (CPR_ID, CPR_CMP_ID, CPR_PRO_ID, CPR_VALUE)
                                                     SELECT POCONTROLS.FGC_CMPPROP_SEQ.NEXTVAL, CMP_ID, PRO_ID, ?
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_TYPES, POCONTROLS.FGC_PROPERTIES
                                                     WHERE PRO_NAME=?
                                                     AND CMP_TP_ID=TP_ID
                                                     AND TP_NAME=?
                                                     AND CMP_ID NOT IN
                                                     (SELECT CMP_ID
                                                     FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_TYPES, POCONTROLS.FGC_COMPONENT_PROPERTIES, POCONTROLS.FGC_PROPERTIES
                                                     WHERE CPR_CMP_ID=CMP_ID
                                                     AND CMP_TP_ID=TP_ID
                                                     AND TP_NAME=?
                                                     AND CPR_PRO_ID=PRO_ID
                                                     AND PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        my $result = $query->execute($value, $property, $type_id, $type_id, $property);

        if(!defined($result))
        {
            print("\tFailed to insert property $property for components of type $type_id\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return($result);
        }
    }
}

# Add an component type property

{
    my $query;
    my $query_connect_count;

    sub add_type_property($$$$)
    {
        my ($dbh, $type_id, $property, $value) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POCONTROLS.FGC_TYPE_PROPERTIES
                                                    (TPR_ID, TPR_TP_ID, TPR_PRO_ID, TPR_VALUE)
                                                    SELECT POCONTROLS.FGC_TPROP_SEQ.NEXTVAL, TP_ID, PRO_ID, ?
                                                    FROM POCONTROLS.FGC_TYPES, POCONTROLS.FGC_PROPERTIES
                                                    WHERE PRO_NAME=?
                                                    AND TP_NAME=?
                                                    AND NOT EXISTS
                                                    (SELECT 1
                                                    FROM POCONTROLS.FGC_TYPES, POCONTROLS.FGC_TYPE_PROPERTIES, POCONTROLS.FGC_PROPERTIES
                                                    WHERE TPR_TP_ID=TP_ID
                                                    AND TP_NAME=?
                                                    AND TPR_PRO_ID=PRO_ID
                                                    AND PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        my $result = $query->execute($value, $property, $type_id, $type_id, $property);

        if(!defined($result))
        {
            print("\tFailed to insert type property $property for type $type_id\n");
            return(undef);
        }

        if(!$dbh->commit)
        {
            print("\tFailed to commit\n");
            return(undef);
        }
        else
        {
            return($result);
        }
    }
}

# Remove specified system properties for a system

{
    my $query;
    my $query_connect_count;

    sub remove_system_properties($$$)
    {
        my ($dbh, $system_name, $properties) = @_;
        my $failed = 0;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("DELETE FROM POCONTROLS.FGC_SYSTEM_PROPERTIES
                                                     WHERE SPR_SYS_ID=(SELECT SYS_ID FROM POCONTROLS.FGC_SYSTEMS WHERE SYS_NAME=?)
                                                     AND SPR_PRO_ID=(SELECT PRO_ID FROM POCONTROLS.FGC_PROPERTIES WHERE PRO_NAME=?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Update each property value

        for my $property (sort(keys(%$properties)))
        {
            # Print the to be removed property together with current value, so that it can be backed up from the log
            my $property_value_str = defined($properties->{$property}) ? $properties->{$property} : '<null>';
            print("\tRemoving property from $system_name: $property='$property_value_str'\n");

            # Execute query allowing partial failures
            my $num = $query->execute($system_name, $property);
            if (!$num)
            {
                print("\tFailed to delete: $property from $system_name\n");
                $failed = 1;
            }
        }

        # Check the commit

        if (!$dbh->commit)
        {
            print("\tFailed to commit\n");
            $failed = 1;
        }

        # Return undef if any query failed

        if ($failed)
        {
            return(undef);
        }
        else
        {
            return(1);
        }
    }
}

# Get all details of system
sub get_system_details($$$)
{
    my ($dbh, $system_name, $fgc_components) = @_;

    my %system;

    # Get properties associated with system from database

    $system{properties}     = FGC::DB::get_system_properties($dbh, $system_name);
    $system{all_properties} = {%{$system{properties}}};

    if(!defined($system{properties}))
    {
        print "\tFailed to retrieve system properties for '$system_name'\n";
        return(undef);
    }

    # Get properties associated with system type from database

    my $system_type = FGC::Utils::get_typename_from_systemname($system_name);
    $system{type_properties} = FGC::DB::get_type_properties($dbh, $system_type);

    if(defined($system{type_properties}))
    {
        for my $property_name (keys(%{$system{type_properties}}))
        {
            $system{all_properties}->{$property_name} = $system{type_properties}->{$property_name};
        }
    }

    # Get components installed in system from database

    $system{components} = FGC::DB::get_system_components($dbh, $system_name);
    if(!defined($system{components}))
    {
        print "\tFailed to retrieve component properties for '$system_name'\n";
        return(undef);
    }

    # Get component properties from database

    for my $cmp_id (keys(%{$system{components}}))
    {
        my $component = $system{components}{$cmp_id};

        if(!defined($fgc_components->{$component->{TP_NAME}}))
        {
            print "\tFailed to get component definition for component type of '$cmp_id'\n";
            return(undef);
        }

        $component->{type} = $fgc_components->{$component->{TP_NAME}};

        # Get properties associated with component from database

        $component->{properties} = FGC::DB::get_component_properties($dbh, $component->{COMPONENT_MTF_ID});
        if(!defined($component->{properties}))
        {
            print "\tFailed to get component properties for '$cmp_id'\n";
            return(undef);
        }

        for my $property_name (keys(%{$component->{properties}}))
        {
            my $bus_property_name                           = $property_name;
            $bus_property_name                              =~ s/\?/$component->{COMPONENT_CHANNEL}/g if(defined($component->{COMPONENT_CHANNEL}));
            $system{all_properties}->{$bus_property_name}   = $component->{properties}->{$property_name};
        }

        # Get properties associated with component type from database

        $component->{type_properties} = FGC::DB::get_type_properties($dbh, $component->{type}->{barcode});
        if(!defined($component->{type_properties}))
        {
            print "\tFailed to get type properties for component '$cmp_id'\n";
            return(undef);
        }

        for my $property_name (keys(%{$component->{type_properties}}))
        {
            my $bus_property_name                           = $property_name;
            $bus_property_name                              =~ s/\?/$component->{COMPONENT_CHANNEL}/g if(defined($component->{COMPONENT_CHANNEL}));
            $system{all_properties}->{$bus_property_name}   = $component->{type_properties}->{$property_name};
        }
    }
    return(\%system);
}

sub get_barcode_types
{
    my ($dbh) = @_;

    my $query = $dbh->prepare("select tp_name from POCONTROLS.FGC_TYPES")
    or return(undef);#or die "Failed to prepare database query\n" .$dbh->errstr;

    $query->execute() or return(undef);#or die "could not execute SQL query";

    my $results = $query->fetchall_arrayref({});

    return map ($_->{'TP_NAME'}, @$results);

    #my $result = $query->execute();
}

# Get all system properties mapped to the system that uses the properties mapped to the class of that system
# $class->$system->$system_property
{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_class_to_system_to_system_properties($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CLASSNAME, SYSTEM_NAME, PROPERTY_NAME FROM POCONTROLS.FGC_SYSTEM_PROPERTIES_V SP
                                                     LEFT JOIN ABC.DEVICES_V D ON SP.SYSTEM_NAME = D.DEVICENAME
                                                     GROUP BY CLASSNAME, SYSTEM_NAME, PROPERTY_NAME
                                                     ORDER BY CLASSNAME, SYSTEM_NAME, PROPERTY_NAME") or return(undef);
            $query_connect_count    = $connect_count;

            %result_hash            =   (
                                            CLASSNAME   => 1,
                                            SYSTEM_NAME => 1,
                                            PROPERTY_NAME => 1,
                                        );
        }

        # Execute query
        $query->execute() or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert result rows into hash mapping FGC class to system to property name

        my %class_to_systems = ();
        for my $row (@$results)
        {
            my $class = $row->{CLASSNAME};
            my $system = $row->{SYSTEM_NAME};
            my $property = $row->{PROPERTY_NAME};

            next if(!defined($class));

            $class =~ s/^FGC_(\d\d)$/$1/g;

            $class_to_systems{$class} = {} if(!defined($class_to_systems{$class}));
            $class_to_systems{$class}->{$system} = {} if(!defined($class_to_systems{$class}->{$system}));
            $class_to_systems{$class}->{$system}->{$property} = 1;
        }

        # Return result
        return \%class_to_systems;
    }
}

# Get all system property names grouped by fgc classes that use the property

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_class_to_system_properties($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CLASSNAME, PROPERTY_NAME FROM POCONTROLS.FGC_SYSTEM_PROPERTIES_V SP
                                                     LEFT JOIN ABC.DEVICES_V D ON SP.SYSTEM_NAME = D.DEVICENAME
                                                     GROUP BY CLASSNAME, PROPERTY_NAME
                                                     ORDER BY CLASSNAME, PROPERTY_NAME") or return(undef);
            $query_connect_count    = $connect_count;

            %result_hash            =   (
                                            CLASSNAME   => 1,
                                            PROPERTY_NAME   => 1,
                                        );
        }

        # Execute query
        $query->execute() or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert result rows into hash mapping FGC class to property name

        return hash_class_to_property_names($results);
    }
}

# Get all component property names grouped by fgc classes that use the property

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_class_to_component_properties($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CLASSNAME, PROPERTY_NAME FROM POCONTROLS.FGC_COMPONENT_PROPERTIES_V CP
                                                     LEFT JOIN POCONTROLS.FGC_COMPONENT_SYSTEMS CS ON CP.COMPONENT_ID = CS.CS_CMP_ID
                                                     LEFT JOIN POCONTROLS.FGC_SYSTEMS S ON CS.CS_SYS_ID = S.SYS_ID
                                                     LEFT JOIN ABC.DEVICES_V D ON S.SYS_NAME = D.DEVICENAME
                                                     GROUP BY CLASSNAME, PROPERTY_NAME
                                                     ORDER BY CLASSNAME, PROPERTY_NAME") or return(undef);
            $query_connect_count    = $connect_count;

            %result_hash            =   (
                                            CLASSNAME   => 1,
                                            PROPERTY_NAME   => 1,
                                        );
        }

        # Execute query
        $query->execute() or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert result rows into hash mapping FGC class to property name

        return hash_class_to_property_names($results);
    }
}

# Get all type property names grouped by fgc classes that use the property

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_class_to_type_properties($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT CLASSNAME, PROPERTY_NAME FROM POCONTROLS.FGC_TYPE_PROPERTIES_V SP
                                                     LEFT JOIN POCONTROLS.FGC_COMPONENTS C ON C.CMP_TP_ID = SP.TYPE_ID
                                                     LEFT JOIN POCONTROLS.FGC_COMPONENT_SYSTEMS CS ON C.CMP_ID = CS.CS_CMP_ID
                                                     LEFT JOIN POCONTROLS.FGC_SYSTEMS S ON CS.CS_SYS_ID = S.SYS_ID
                                                     LEFT JOIN ABC.DEVICES_V D ON S.SYS_NAME = D.DEVICENAME
                                                     GROUP BY CLASSNAME, PROPERTY_NAME
                                                     ORDER BY CLASSNAME, PROPERTY_NAME") or return(undef);
            $query_connect_count    = $connect_count;

            %result_hash            =   (
                                            CLASSNAME   => 1,
                                            PROPERTY_NAME   => 1,
                                        );
        }

        # Execute query
        $query->execute() or return(undef);

        # Get array of result row hashes

        my $results = $query->fetchall_arrayref(\%result_hash);

        # Convert result rows into hash mapping FGC class to property name

        return hash_class_to_property_names($results);
    }
}

# Helper function to create result hash for
# get_class_to_system_properties
# get_class_to_component_properties
# get_class_to_type_properties
sub hash_class_to_property_names($)
{
    my ($results) = @_;

    # Convert result rows into hash mapping FGC class to property name

    my %class_to_props = ();
    my @all_props = ();
    for my $row (@$results)
    {
        my $class = $row->{CLASSNAME};
        my $property = $row->{PROPERTY_NAME};

        my $escaped_property = $property;
        $escaped_property =~ s/\?/\\?/g;

        push(@all_props, $property) if(!grep(/^$escaped_property$/, @all_props));
        next if(!defined($class));

        $class =~ s/^FGC_(\d\d)$/$1/g;

        $class_to_props{$class} = [] if(!defined($class_to_props{$class}));
        push(@{$class_to_props{$class}}, $property) if(!grep(/^$escaped_property$/, @{$class_to_props{$class}}));
    }

    # Return result

    my %return = (
        CLASS_TO_PROPERTY_NAMES => \%class_to_props,
        ALL_PROPERTY_NAMES => \@all_props
    );

    return(\%return);
}

sub get_instances_by_class_and_property($$$$)
{
    my ($dbh, $class, $property, $kind) = @_;

    return get_systems_by_class_and_property($dbh, $class, $property) if($kind eq "SYS");
    return get_components_by_class_and_property($dbh, $class, $property)  if($kind eq "CMP");
    return get_types_by_class_and_property($dbh, $class, $property)   if($kind eq "TYPE");

    return undef;
}

sub get_systems_by_class_and_property($$$)
{
    my ($dbh, $class, $property) = @_;

    $class = 'FGC_'.$class;

    my $query = $dbh->prepare("SELECT SYSTEM_NAME FROM POCONTROLS.FGC_SYSTEM_PROPERTIES_V SP
                               LEFT JOIN ABC.DEVICES_V D ON SP.SYSTEM_NAME = D.DEVICENAME
                               WHERE CLASSNAME = ?
                               AND PROPERTY_NAME = ?") or return(undef);


    my %result_hash = ( SYSTEM_NAME => 1 );

    # Execute query

    $query->execute($class, $property) or return(undef);

    # Get array of result row hashes

    my $results = $query->fetchall_arrayref(\%result_hash);
    my @results_arr = map ($_->{'SYSTEM_NAME'}, @$results);
    return \@results_arr;
}

sub get_components_by_class_and_property($$$)
{
    my ($dbh, $class, $property) = @_;

    $class = 'FGC_'.$class;

    my $query = $dbh->prepare("SELECT COMPONENT_MTF_ID FROM POCONTROLS.FGC_COMPONENT_PROPERTIES_V CP
                               LEFT JOIN POCONTROLS.FGC_COMPONENT_SYSTEMS CS ON CP.COMPONENT_ID = CS.CS_CMP_ID
                               LEFT JOIN POCONTROLS.FGC_SYSTEMS S ON CS.CS_SYS_ID = S.SYS_ID
                               LEFT JOIN ABC.DEVICES_V D ON S.SYS_NAME = D.DEVICENAME
                               WHERE CLASSNAME = ?
                               AND PROPERTY_NAME = ?") or return(undef);


    my %result_hash = ( COMPONENT_MTF_ID => 1 );

    # Execute query

    $query->execute($class, $property) or return(undef);

    # Get array of result row hashes

    my $results = $query->fetchall_arrayref(\%result_hash);
    my @results_arr = map ($_->{'COMPONENT_MTF_ID'}, @$results);
    return \@results_arr;
}

sub get_types_by_class_and_property($$$)
{
    my ($dbh, $class, $property) = @_;

    $class = 'FGC_'.$class;

    my $query = $dbh->prepare("SELECT TYPE_NAME FROM POCONTROLS.FGC_TYPE_PROPERTIES_V SP
                               LEFT JOIN POCONTROLS.FGC_COMPONENTS C ON C.CMP_TP_ID = SP.TYPE_ID
                               LEFT JOIN POCONTROLS.FGC_COMPONENT_SYSTEMS CS ON C.CMP_ID = CS.CS_CMP_ID
                               LEFT JOIN POCONTROLS.FGC_SYSTEMS S ON CS.CS_SYS_ID = S.SYS_ID
                               LEFT JOIN ABC.DEVICES_V D ON S.SYS_NAME = D.DEVICENAME
                               WHERE CLASSNAME = ?
                               AND PROPERTY_NAME = ?") or return(undef);


    my %result_hash = ( TYPE_NAME => 1 );

    # Execute query

    $query->execute($class, $property) or return(undef);

    # Get array of result row hashes

    my $results = $query->fetchall_arrayref(\%result_hash);
    my @results_arr = map ($_->{'TYPE_NAME'}, @$results);
    return \@results_arr;
}
# EOF
