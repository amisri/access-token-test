#!/usr/bin/perl -w
#
# Name:     FGC/Boot.pm
# Purpose:  Communication with FGC boot program

package FGC::Boot;

use strict;
use warnings;

use Carp;
use IO::Socket;

use FGC::Sync qw(can_write can_read);

require Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                     command
                     command_no_wait
                );


# Begin functions

# Send a command

sub command($$)
{
    my ($socket, $command) = @_;

    my $select = IO::Select->new($socket);

    # Switch to direct command mode

    can_write($select);
    send($socket, "\032", 0);

    # Send command

    can_write($select);
    defined(send($socket, "\$$command\n", 0)) or return(undef);

    # Set locally scoped input record seperator to be line-feed

    local($/) = "\012";

    # Wait for response

    my %response;

    # TODO Make this non-blocking!

    while(<$socket>)
    {
        # Skip line if it does not contain a "$"

        # next if(!/\$/);

        # Remove carriage return and/or line-feed

        s/[\012\015]+$//;

        # Remove any characters before "$"

        s/^.*\$/\$/;

        # Skip progress lines

        next if(/\$1/);

        # Push onto response value array

        push(@{$response{value}}, $_);

        # Check whether FGC has rebooted

        if(/^\$R/) # FGC has rebooted
        {
            # Throw away data until "$S"

            while(<$socket> !~ /^\$S/) {}
        }

        # Stop if response is a "$3", "$4" or "$R"

        if(/^\$([34R])/)
        {
            if($1 eq "3")
            {
                $response{status} = "OKAY";
            }
            elsif($1 eq "4")
            {
                $response{status} = "ERROR";
            }
            elsif($1 eq "R")
            {
                $response{status} = "FGC REBOOTED";
            }
            else
            {
                $response{status} = "BAD RESPONSE";
            }
            last;
        }
    }

    return(\%response);
}



sub command_no_wait($$)
{
    my ($socket, $command) = @_;

    my $select = IO::Select->new($socket);

    # Switch to direct command mode

    can_write($select);
    send($socket, "\032", 0);

    # Send command

    can_write($select);
    defined(send($socket, "\$${command}\n", 0)) or return(undef);

    return(1);
}

1;

# EOF
