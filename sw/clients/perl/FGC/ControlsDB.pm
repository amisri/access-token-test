#!/usr/bin/perl -w
#
# Filename: FGC/ControlsDB.pm
# Purpose:  Access functions for the Controls Database
# Author:   Stephen Page

package FGC::ControlsDB;

use DBI;
use Exporter;
use strict;

use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        add_class_alarm
                        add_device
                        add_device_group
                        add_device_to_group
                        add_hw_property
                        add_hw_property_field
                        add_hw_symbol
                        add_property
                        add_property_group
                        add_property_to_group
                        connect
                        duplicate_class
                        get_all_devices
                        get_class
                        get_class_alarms
                        get_device
                        get_device_groups
                        get_device_group_members
                        get_hw_properties
                        get_hw_property_fields
                        get_hw_symbols
                        get_properties
                        get_property_groups
                        get_property_group_members
                        remove_class_alarm
                        remove_device
                        remove_device_from_group
                        rename_class
                        rename_device
                        remove_property
                        remove_property_field
                        remove_property_from_group
                        remove_hw_property_symbols
                        remove_hw_symbol
                        update_class_alarm
                        update_device
                        update_device_alias
                        update_hw_property
                        update_hw_property_field
                        update_hw_symbol
                        try_to_commit
                    );

# Constants

#TODO: we could add an option to choose between one DB or the other (prod/test)
#use constant FGC_CONTROLS_DB_ID => 'dbi:Oracle:devdb19'; # ID of Test Controls Database
#use constant FGC_CONTROLS_DB_ID => 'dbi:Oracle:accint'; # ID of Test Controls Database
use constant FGC_CONTROLS_DB_ID => 'dbi:Oracle:dbabco'; # ID of Controls Database

my $connect_count = 0;

return(1);


# Begin functions

# Connect to database

sub connect($$)
{
    my ($username, $password) = @_;

    # Connect to database

    my $dbh = DBI->connect( FGC_CONTROLS_DB_ID,
                            $username, $password,
                            {PrintError => 1,
                             RaiseError => 1,
                             LongReadLen => FGC::Consts::FGC_MAX_DB_PROP_SIZE_BYTES}
                        ) or return(undef);
    $connect_count++;

    return($dbh);
}

# Disconnect from database

sub disconnect($)
{
    my ($dbh) = @_;

    # Disconnect from database

    $dbh->disconnect;
}

sub try_to_commit($)
{
    my ($dbh) = @_;

    if (!$dbh->commit)
    {
        print("\tFailed to commit\n");
        return 0;
    }
    print("\tCommited DB session\n");
    return 1;
}

# Get class

{
    my $query;
    my $query_connect_count;

    sub get_class($$)
    {
        my ($dbh, $class) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('SELECT *
                                    FROM ABC.DEVICECLASSES
                                    WHERE CLASSNAME = ?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class") or return(undef);

        # Return result

        return($query->fetchrow_hashref());
    }
}

# Get device

{
    my $query;
    my $query_connect_count;

    sub get_device($$)
    {
        # epc_device_name equals ccdb_alias
        my ($dbh, $epc_device_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('SELECT *
                                    FROM ABC.DEVICES
                                    WHERE CLASSV_ID in (SELECT CLASSV_ID FROM ABC.CLASSVERSIONS_V WHERE regexp_like (CLASSNAME,\'^FGC_\d+$\'))
                                    AND ALIAS=?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($epc_device_name) or return(undef);

        # Return result

        return($query->fetchrow_hashref());
    }
}

# Get all FGC devices

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_all_devices($)
    {
        # epc_device_name equals ccdb_alias
        # epc_alias equals ccdb_device_name
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('SELECT ACCELERATOR, ALIAS, c.CLASSNAME, DEVICENAME, FECNAME, FGC_FIELDBUS_ADDRESS, SERVERNAME, SUBSYSTEM
                                    FROM ABC.DEVICES d JOIN ABC.CLASSVERSIONS_V c
                                    ON d.CLASSV_ID=c.CLASSV_ID
                                    WHERE regexp_like (c.CLASSNAME,\'^FGC_\d+$\')
                                    ORDER BY ALIAS') or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ACCELERATOR => 1,
                                            ALIAS       => 1,
                                            CLASSNAME   => 1,
                                            DEVICENAME  => 1,
                                            FECNAME     => 1,
                                            FGC_FIELDBUS_ADDRESS => 1,
                                            SERVERNAME  => 1,
                                            SUBSYSTEM   => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Add an alarm for a class

{
    my $query;
    my $query_connect_count;

    sub add_class_alarm($$$$$$$$$)
    {
        my ($dbh, $class, $fault_code, $problem, $priority, $cause, $consequence, $action, $symbol) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.CLASS_ALARMS
                                    (ID, CLASS_ID, FAULT_CODE, PROBLEM, PRIORITY, CAUSE, CONSEQUENCE, ACTION, HELP_URL)
                                    VALUES (ABC.CLASS_ALARMS_ID_SEQ.NEXTVAL, (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?),
                                    ?, ?, ?, ?, ?, ?, ?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Construct help URL

        my $help_url = "http://proj-fgc.web.cern.ch/proj-fgc/gendoc/def/Class${class}_SymList_";
        $help_url   .= ($fault_code >= 16) && ($fault_code <= 31) ? "FLT.htm" : "WRN.htm";
        $help_url   .= "#".(($fault_code >= 16) && ($fault_code <= 31) ? "FLT" : "WRN").".$symbol" if(defined($symbol));

        # Execute query

        $query->execute("FGC_$class", $fault_code, $problem, $priority, $cause, $consequence, $action, $help_url) or return(undef);
    }
}

# Add device

{
    my $query;
    my $query_connect_count;

    sub add_device($$$$$$$)
    {
        # epc_alias equals ccdb_device_name
        # epc_device_name equals ccdb_alias
        my ($dbh, $epc_device_name, $epc_alias, $class, $gateway, $server_name, $fgc_fieldbus_address) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.DEVICES
                                    (DEVICE_ID, ALIAS, DEVICENAME, SUBSYSTEM, FECNAME, SERVERNAME, FGC_FIELDBUS_ADDRESS, CLASSV_ID)
                                    VALUES (ABC.DEVICE_ID_SEQ.NEXTVAL, ?, ?, 'FGC power converters', ?, ?, ?,
                                    (SELECT CLASSV_ID FROM ABC.CLASSVERSIONS_V WHERE CLASSNAME=?))")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($epc_device_name, $epc_alias, $gateway, $server_name, $fgc_fieldbus_address, "FGC_$class") or return(undef);
    }
}

# Duplicate Class

{
    my $query;
    my $query_connect_count;

    sub duplicate_class($$$)
    {
        my ($dbh, $class_to_duplicate, $new_class_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.deviceclasses (classname, implementation, alarms, description, person_id, domain_id, responsible_id)
                                    SELECT ?, implementation, alarms, description, person_id, domain_id, responsible_id
                                    FROM ABC.deviceclasses
                                    WHERE classname = ?")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$new_class_name", "FGC_$class_to_duplicate") or return(undef);
    }
}

# Rename Class

{
    my $query;
    my $query_connect_count;

    sub rename_class($$$)
    {
        my ($dbh, $old_class_name, $new_class_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('UPDATE ABC.deviceclasses
                                    SET classname=?
                                    WHERE classname=?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$new_class_name", "FGC_$old_class_name") or return(undef);
    }
}

# Rename device

{
    my $query;
    my $query_connect_count;

    sub rename_device($$$)
    {
        # epc_device_name equals ccdb_alias
        my ($dbh, $old_epc_device_name, $new_epc_device_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('UPDATE ABC.DEVICES
                                    SET ALIAS=?
                                    WHERE CLASSV_ID in (SELECT CLASSV_ID FROM ABC.CLASSVERSIONS_V WHERE regexp_like (CLASSNAME,\'^FGC_\d+$\'))
                                    AND ALIAS=?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($old_epc_device_name, $new_epc_device_name) or return(undef);
    }
}

# Update alarm for a class

{
    my $query;
    my $query_connect_count;

    sub update_class_alarm($$$$$$$$$)
    {
        my ($dbh, $class, $fault_code, $problem, $priority, $cause, $consequence, $action, $symbol) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("UPDATE ABC.CLASS_ALARMS
                                    SET PROBLEM=?, PRIORITY=?, CAUSE=?, CONSEQUENCE=?, ACTION=?, HELP_URL=?
                                    WHERE CLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND FAULT_CODE=?") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Construct help URL

        my $help_url = "http://proj-fgc.web.cern.ch/proj-fgc/gendoc/def/Class${class}_SymList_";
        $help_url   .= ($fault_code >= 16) && ($fault_code <= 31) ? "FLT.htm" : "WRN.htm";
        $help_url   .= "#".(($fault_code >= 16) && ($fault_code <= 31) ? "FLT" : "WRN").".$symbol" if(defined($symbol));

        # Execute query

        $query->execute($problem, $priority, $cause, $consequence, $action, $help_url, "FGC_$class", $fault_code) or return(undef);
    }
}

# Update device

{
    my $query;
    my $query_connect_count;

    sub update_device($$$$$$$)
    {
        # epc_alias equals ccdb_device_name
        # epc_device_name equals ccdb_alias
        my ($dbh, $epc_device_name, $epc_alias, $class, $gateway, $server_name, $fgc_fieldbus_address) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('UPDATE ABC.DEVICES
                                    SET DEVICENAME=?, FECNAME=?, SERVERNAME=?, FGC_FIELDBUS_ADDRESS=?, CLASSV_ID=(SELECT CLASSV_ID FROM ABC.CLASSVERSIONS_V WHERE CLASSNAME=?)
                                    WHERE CLASSV_ID in (SELECT CLASSV_ID FROM ABC.CLASSVERSIONS_V WHERE regexp_like (CLASSNAME,\'^FGC_\d+$\'))
                                    AND ALIAS=?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($epc_alias, $gateway, $server_name, $fgc_fieldbus_address, "FGC_$class", $epc_device_name) or return(undef);
    }
}

# Update device alias

{
    my $query;
    my $query_connect_count;

    sub update_device_alias($$$)
    {
        # Updates the given ccdb device entry's alias field
        my ($dbh, $ccdb_device_name, $epc_alias) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('UPDATE ABC.DEVICES
                                    SET ALIAS=?
                                    WHERE DEVICENAME=?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($epc_alias, $ccdb_device_name) or return(undef);
    }
}

# Remove alarm for a class

{
    my $query;
    my $query_connect_count;

    sub remove_class_alarm($$$)
    {
        my ($dbh, $class, $fault_code) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.CLASS_ALARMS
                                    WHERE CLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND FAULT_CODE=?") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $fault_code) or return(undef);
    }
}

# Remove device

{
    my $query;
    my $query_connect_count;

    sub remove_device($$)
    {
        # epc_device_name equals ccdb_alias
        my ($dbh, $epc_device_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare('DELETE FROM ABC.DEVICES
                                    WHERE CLASSV_ID IN (SELECT CLASSV_ID FROM ABC.CLASSVERSIONS_V WHERE regexp_like (CLASSNAME,\'^FGC_\d+(_OLD)*$\'))
                                    AND ALIAS=?') or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($epc_device_name) or return(undef);
    }
}

# Get device groups for class

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_device_groups($$)
    {
        my ($dbh, $class) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT *
                                    FROM ABC.RBA_DEVICEGROUPS JOIN ABC.DEVICECLASSES
                                    ON DEVICECLASS_ID=CLASS_ID
                                    WHERE CLASSNAME=?
                                    ORDER BY DEVICEGROUP_NAME") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            DESCRIPTION         => 1,
                                            DEVICEGROUP_ID      => 1,
                                            DEVICEGROUP_NAME    => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get device group members

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_device_group_members($$$)
    {
        my ($dbh, $class, $group) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT ALIAS
                                    FROM ABC.RBA_DEVICEGROUPS g, ABC.RBA_DEVGRP_MEMBERS m, ABC.DEVICES d
                                    WHERE g.DEVICEGROUP_ID=m.DEVICEGROUP_ID
                                    AND m.DEVICE_ID=d.DEVICE_ID
                                    AND g.DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND DEVICEGROUP_NAME=?
                                    ORDER BY ALIAS") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ALIAS => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class", $group) or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Add device group

{
    my $query;
    my $query_connect_count;

    sub add_device_group($$$;$)
    {
        my ($dbh, $class, $group, $description) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.RBA_DEVICEGROUPS
                                    (DEVICEGROUP_ID, DEVICECLASS_ID, DEVICEGROUP_NAME, DESCRIPTION)
                                    VALUES (ABC.RBA_DEVICEGROUPS_SEQ.NEXTVAL,
                                    (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?), ?, ?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $group, $description) or return(undef);
    }
}

# Add device to group

{
    my $query;
    my $query_connect_count;

    sub add_device_to_group($$$$)
    {
        # epc_device_name equals ccdb_alias
        my ($dbh, $class, $group, $epc_device_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.RBA_DEVGRP_MEMBERS
                                    (DEVICECLASS_ID, DEVICEGROUP_ID, DEVICE_ID)
                                    VALUES ((SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?),
                                    (SELECT DEVICEGROUP_ID FROM ABC.RBA_DEVICEGROUPS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND DEVICEGROUP_NAME=?),
                                    (SELECT DEVICE_ID FROM ABC.DEVICES WHERE ALIAS=?))")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", "FGC_$class", $group, $epc_device_name) or return(undef);
    }
}

# Remove device from group

{
    my $query;
    my $query_connect_count;

    sub remove_device_from_group($$$$)
    {
        # epc_device_name equals ccdb_alias
        my ($dbh, $class, $group, $epc_device_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.RBA_DEVGRP_MEMBERS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND DEVICEGROUP_ID=(SELECT DEVICEGROUP_ID FROM ABC.RBA_DEVICEGROUPS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND DEVICEGROUP_NAME=?)
                                    AND DEVICE_ID=(SELECT DEVICE_ID FROM ABC.DEVICES WHERE ALIAS=?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", "FGC_$class", $group, $epc_device_name) or return(undef);
    }
}

# Get properties for class

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_hw_properties($$)
    {
        my ($dbh, $class) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT *
                                    FROM ABC.HARDWARE_DEVCLS_PROPS
                                    JOIN ABC.DEVICECLASSES
                                    ON DEVICECLASS_ID=CLASS_ID
                                    LEFT OUTER JOIN ABC.LOV_PRIM_DATA_TYPES
                                    ON PROPERTY_TYPE_ID=ID
                                    WHERE CLASSNAME=?
                                    ORDER BY PROPERTY_NAME") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ARRAY_DIM           => 1,
                                            DEVCLS_PROP_ID      => 1,
                                            GETTABLE            => 1,
                                            IS_CYCLE_BOUND      => 1,
                                            IS_TRANSACTIONAL    => 1,
                                            MONITORABLE         => 1,
                                            OPERATIONAL         => 1,
                                            PPM                 => 1,
                                            PRIMITIVE_DATA_TYPE => 1,
                                            PROPERTY_NAME       => 1,
                                            SETTABLE            => 1,
                                            SYMBOLS_TYPE        => 1,
                                            UNITS               => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get fields for a given property and class

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_hw_property_fields($$$)
    {
        my ($dbh, $class, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT *
                                    FROM    ABC.HARDWARE_DEVCLS_PROPS_FIELDS FIELDS,
                                            ABC.LOV_PRIM_DATA_TYPES          TYPES
                                    WHERE
                                    FIELDS.PROPERTY_TYPE_ID = TYPES.ID
                                    AND
                                    FIELDS.PROPERTY_ID =   (SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS
                                                            WHERE
                                                            DEVICECLASS_ID = (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                                            AND
                                                            PROPERTY_NAME  = ?)") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            FIELD_NAME          => 1,
                                            PRIMITIVE_DATA_TYPE => 1,
                                            SYMBOLS_TYPE        => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class",$property) or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get properties for class

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_properties($$)
    {
        my ($dbh, $class) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT *
                                    FROM ABC.RBA_DEVCLS_PROPS JOIN ABC.DEVICECLASSES
                                    ON DEVICECLASS_ID=CLASS_ID
                                    WHERE CLASSNAME=?
                                    ORDER BY PROPERTY_NAME") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            DEVCLS_PROP_ID  => 1,
                                            PROPERTY_NAME   => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Add property

{
    my $query;
    my $query_connect_count;

    sub add_hw_property($$$$$$$$$$$$$$)
    {
        my ($dbh, $class, $property, $type, $maxels, $operational, $gettable, $settable, $monitorable, $ppm, $units, $symbols_type, $is_cycle_bound, $is_transactional) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.HARDWARE_DEVCLS_PROPS
                                    (DEVCLS_PROP_ID, DEVICECLASS_ID, PROPERTY_NAME, PROPERTY_TYPE_ID,
                                    ARRAY_DIM, OPERATIONAL, GETTABLE, SETTABLE, MONITORABLE, PPM, UNITS, SYMBOLS_TYPE, IS_CYCLE_BOUND, IS_TRANSACTIONAL)
                                    VALUES(ABC.HARDWARE_DEVCLS_PROPS_ID_SEQ.NEXTVAL,
                                    (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?), ?,
                                    (SELECT ID FROM ABC.LOV_PRIM_DATA_TYPES WHERE PRIMITIVE_DATA_TYPE=?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class",
                        $property,
                        $type,
                        $maxels,
                        $operational,
                        $gettable,
                        $settable,
                        $monitorable,
                        $ppm,
                        $units,
                        $symbols_type,
                        $is_cycle_bound,
                        $is_transactional) or return(undef);
    }
}

# Add property fields for the SUB and SUB_xy properties

{
    my $query;
    my $query_connect_count;

    sub add_hw_property_field($$$$$$$)
    {
        my ($dbh, $class, $property, $field, $type, $maxels, $symbols_type) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.HARDWARE_DEVCLS_PROPS_FIELDS
                                    (FIELD_ID, PROPERTY_ID, FIELD_NAME, PROPERTY_TYPE_ID, ARRAY_DIM, SYMBOLS_TYPE)
                                    VALUES
                                    (   ABC.HRDW_DEVCLSPROP_FIELD_ID_SEQ.NEXTVAL,
                                        (SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS
                                         WHERE
                                         DEVICECLASS_ID = (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                         AND
                                         PROPERTY_NAME  = ?),
                                        ?,
                                        (SELECT ID FROM ABC.LOV_PRIM_DATA_TYPES WHERE PRIMITIVE_DATA_TYPE=?),
                                        ?,
                                        ?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class",
                        $property,
                        $field,
                        $type,
                        $maxels,
                        $symbols_type) or return(undef);
    }
}

# Add property

{
    my $query;
    my $query_connect_count;

    sub add_property($$$)
    {
        my ($dbh, $class, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.RBA_DEVCLS_PROPS
                                    (DEVCLS_PROP_ID, DEVICECLASS_ID, PROPERTY_NAME)
                                    VALUES (ABC.RBA_DEVCLS_PROPS_SEQ.NEXTVAL,
                                    (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?), ?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $property) or return(undef);
    }
}

# Remove property

{
    my $query;
    my $query_connect_count;

    sub remove_hw_property($$$)
    {
        my ($dbh, $class, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.HARDWARE_DEVCLS_PROPS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND PROPERTY_NAME=?") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $property) or return(undef);
    }
}

# Remove property

{
    my $query;
    my $query_connect_count;

    sub remove_property($$$)
    {
        my ($dbh, $class, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.RBA_DEVCLS_PROPS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND PROPERTY_NAME=?") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $property) or return(undef);
    }
}

{
    my $query;
    my $query_connect_count;

    sub remove_hw_property_field($$$$)
    {
        my ($dbh, $class, $property, $field) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.HARDWARE_DEVCLS_PROPS_FIELDS
                                    WHERE
                                    PROPERTY_ID =  (SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS
                                                    WHERE
                                                    DEVICECLASS_ID = (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                                    AND
                                                    PROPERTY_NAME  = ?)
                                    AND
                                    FIELD_NAME = ?") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $property, $field) or return(undef);
    }
}


# Update property

{
    my $query;
    my $query_connect_count;

    sub update_hw_property($$$$$$$$$$$$$$)
    {
        my ($dbh, $class, $property, $type, $maxels, $operational, $gettable, $settable, $monitorable, $ppm, $units, $symbols_type, $is_cycle_bound, $is_transactional) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("UPDATE ABC.HARDWARE_DEVCLS_PROPS
                                    SET PROPERTY_TYPE_ID=(SELECT ID FROM ABC.LOV_PRIM_DATA_TYPES WHERE PRIMITIVE_DATA_TYPE=?),
                                    ARRAY_DIM=?, OPERATIONAL=?, GETTABLE=?, SETTABLE=?, MONITORABLE=?, PPM=?, UNITS=?, SYMBOLS_TYPE=?, IS_CYCLE_BOUND=?, IS_TRANSACTIONAL=?
                                    WHERE PROPERTY_NAME=?
                                    AND DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($type, $maxels, $operational, $gettable, $settable, $monitorable,
                        $ppm, $units, $symbols_type, $is_cycle_bound, $is_transactional, $property, "FGC_$class") or return(undef);
    }
}

# Update property field

{
    my $query;
    my $query_connect_count;

    sub update_hw_property_field($$$$$$$)
    {
        my ($dbh, $class, $property, $field, $type, $maxels, $symbols_type) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("UPDATE ABC.HARDWARE_DEVCLS_PROPS_FIELDS
                                    SET
                                    PROPERTY_TYPE_ID = (SELECT ID FROM ABC.LOV_PRIM_DATA_TYPES WHERE PRIMITIVE_DATA_TYPE=?),
                                    ARRAY_DIM=?,
                                    SYMBOLS_TYPE=?
                                    WHERE
                                    PROPERTY_ID =   (SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS
                                                    WHERE
                                                    DEVICECLASS_ID = (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                                    AND
                                                    PROPERTY_NAME  = ?)
                                    AND
                                    FIELD_NAME = ?")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($type, $maxels, $symbols_type, "FGC_$class", $property,$field) or return(undef);
    }
}

# Get property groups for class

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_property_groups($$)
    {
        my ($dbh, $class) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT *
                                    FROM ABC.RBA_PROPERTYGROUPS JOIN ABC.DEVICECLASSES
                                    ON DEVICECLASS_ID = CLASS_ID
                                    WHERE CLASSNAME=?
                                    ORDER BY PROPERTYGROUP_NAME") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            DESCRIPTION         => 1,
                                            PROPERTYGROUP_ID    => 1,
                                            PROPERTYGROUP_NAME  => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get property group members

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_property_group_members($$$)
    {
        my ($dbh, $class, $group) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT PROPERTY_NAME
                                    FROM ABC.RBA_PROPERTYGROUPS g, ABC.RBA_PROPGRP_MEMBERS m, ABC.RBA_DEVCLS_PROPS p
                                    WHERE g.PROPERTYGROUP_ID=m.PROPERTYGROUP_ID
                                    AND m.DEVCLS_PROP_ID=p.DEVCLS_PROP_ID
                                    AND g.DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND PROPERTYGROUP_NAME=?
                                    ORDER BY PROPERTY_NAME") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            PROPERTY_NAME => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class", $group) or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Add property group

{
    my $query;
    my $query_connect_count;

    sub add_property_group($$$;$)
    {
        my ($dbh, $class, $group, $description) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.RBA_PROPERTYGROUPS
                                    (PROPERTYGROUP_ID, DEVICECLASS_ID, PROPERTYGROUP_NAME, DESCRIPTION)
                                    VALUES (ABC.RBA_PROPERTYGROUPS_SEQ.NEXTVAL,
                                    (SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?), ?, ?)")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $group, $description) or return(undef);
    }
}

# Add property to group

{
    my $query;
    my $query_connect_count;

    sub add_property_to_group($$$$)
    {
        my ($dbh, $class, $group, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.RBA_PROPGRP_MEMBERS
                                    (DEVICECLASS_ID, PROPERTYGROUP_ID, DEVCLS_PROP_ID)
                                    VALUES ((SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?),
                                            (SELECT PROPERTYGROUP_ID FROM ABC.RBA_PROPERTYGROUPS
                                             WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                             AND PROPERTYGROUP_NAME=?),
                                             (SELECT DEVCLS_PROP_ID
                                              FROM ABC.RBA_DEVCLS_PROPS WHERE PROPERTY_NAME=?
                                              AND DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)))")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", "FGC_$class", $group, $property, "FGC_$class") or return(undef);
    }
}

# Remove property from group

{
    my $query;
    my $query_connect_count;

    sub remove_property_from_group($$$$)
    {
        my ($dbh, $class, $group, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.RBA_PROPGRP_MEMBERS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND PROPERTYGROUP_ID=(SELECT PROPERTYGROUP_ID FROM ABC.RBA_PROPERTYGROUPS
                                    WHERE DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    AND PROPERTYGROUP_NAME=?)
                                    AND DEVCLS_PROP_ID=(SELECT DEVCLS_PROP_ID FROM ABC.RBA_DEVCLS_PROPS
                                    WHERE PROPERTY_NAME=?
                                    AND DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?))")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", "FGC_$class", $group, $property, "FGC_$class") or return(undef);
    }
}

# Add symbol
# CCS-10504: Not exposing numeric value anymore as it is only used internally. Expose unique arbitrary value (SYMBOL_ID) instead.
{
    my $query;
    my $query_connect_count;

    sub add_hw_symbol($$$$$$)
    {
        my ($dbh, $class, $property, $symbol, $settable, $meaning) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("INSERT INTO ABC.HARDWARE_PROPS_SYMBOLS
                                    (SYMBOL_ID, PROP_ID, SYMBOL, NUMERIC_VALUE, SETTABLE, MEANING)
                                    VALUES(
                                        ABC.HARDWARE_PROPS_SYMBOLS_ID_SEQ.NEXTVAL,
                                        (SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS WHERE PROPERTY_NAME=? AND
                                         DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)),
                                        ?,
                                        ABC.HARDWARE_PROPS_SYMBOLS_ID_SEQ.NEXTVAL,
                                        ?,
                                        ?
                                    )")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query
        $query->execute($property,
                        "FGC_$class",
                        $symbol,
                        $settable,
                        $meaning) or return(undef);
    }
}

# Remove symbol

{
    my $query;
    my $query_connect_count;

    sub remove_hw_symbol($$$$)
    {
        my ($dbh, $class, $property, $symbol) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.HARDWARE_PROPS_SYMBOLS
                                    WHERE PROP_ID=(SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS WHERE PROPERTY_NAME=? AND
                                                   DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?))
                                    AND SYMBOL=?") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($property, "FGC_$class", $symbol) or return(undef);
    }
}

# Remove all symbols for a property

{
    my $query;
    my $query_connect_count;

    sub remove_hw_property_symbols($$$)
    {
        my ($dbh, $class, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("DELETE FROM ABC.HARDWARE_PROPS_SYMBOLS
                                    WHERE PROP_ID=(SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS WHERE PROPERTY_NAME=? AND
                                                   DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?))") or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($property, "FGC_$class") or return(undef);
    }
}

# Update symbol
# CCS-10504: Not exposing numeric value anymore as it is only used internally. Expose unique arbitrary value (SYMBOL_ID) instead.
{
    my $query;
    my $query_connect_count;

    sub update_hw_symbol($$$$$$)
    {
        my ($dbh, $class, $property, $symbol, $settable, $meaning) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("UPDATE ABC.HARDWARE_PROPS_SYMBOLS
                                    SET NUMERIC_VALUE=SYMBOL_ID, SETTABLE=?, MEANING=?
                                    WHERE PROP_ID=(SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS WHERE PROPERTY_NAME=? AND
                                                   DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?))
                                    AND SYMBOL=?")
                        or return(undef);

            $query_connect_count = $connect_count;
        }

        # Execute query

        $query->execute($settable, $meaning, $property, "FGC_$class", $symbol) or return(undef);
    }
}

# Get symbols for property

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_hw_symbols($$$)
    {
        my ($dbh, $class, $property) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT *
                                    FROM ABC.HARDWARE_PROPS_SYMBOLS
                                    WHERE PROP_ID=(SELECT DEVCLS_PROP_ID FROM ABC.HARDWARE_DEVCLS_PROPS WHERE PROPERTY_NAME=? AND
                                                   DEVICECLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?))
                                    ORDER BY SYMBOL") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            MEANING         => 1,
                                            NUMERIC_VALUE   => 1,
                                            PROP_ID         => 1,
                                            SETTABLE        => 1,
                                            SYMBOL          => 1,
                                            SYMBOL_ID       => 1,
                                        );
        }

        # Execute query

        $query->execute($property, "FGC_$class") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get alarms for a class

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_class_alarms($$)
    {
        my ($dbh, $class) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query = $dbh->prepare("SELECT ACTION, CAUSE, CONSEQUENCE, FAULT_CODE, HELP_URL, ID, PRIORITY, PROBLEM
                                    FROM ABC.CLASS_ALARMS
                                    WHERE CLASS_ID=(SELECT CLASS_ID FROM ABC.DEVICECLASSES WHERE CLASSNAME=?)
                                    ORDER BY FAULT_CODE") or return(undef);

            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ACTION      => 1,
                                            CAUSE       => 1,
                                            CONSEQUENCE => 1,
                                            FAULT_CODE  => 1,
                                            HELP_URL    => 1,
                                            ID          => 1,
                                            PRIORITY    => 1,
                                            PROBLEM     => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$class") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# EOF
