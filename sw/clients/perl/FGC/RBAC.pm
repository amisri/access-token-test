#!/usr/bin/perl -w
#
# Filename: FGC/RBAC.pm
# Purpose:  Obtain and decode RBAC tokens
# Author:   Stephen Page

package FGC::RBAC;

use Exporter;
use FGC::RBAC::Serialise;
use List::Util qw(shuffle);
use LWP::UserAgent;
use MIME::Base64;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        authenticate
                        authenticate_by_location
                        decode_token
                        authenticate_by_saml
                        print_token
                        reduce_roles
                        serialize_token
                        deserialize_token
                    );

# Constants

use constant RBAC_PORT              => 8443;                        # RBAC server port number
use constant RBAC_PUBLIC_KEY_FILE   => '/user/pclhc/etc/rbac/rbac.pem';  # RBAC public key file
use constant RBAC_SERVERS           => qw(
                                            rbac-pro-lb.cern.ch
                                         );                         # RBAC servers
use constant RBAC_URL               => 'rba/';                      # URL path to RBAC authentication form

return(1);

# Begin functions

# Authenticate to RBAC server

sub authenticate($$)
{
    my ($username, $password) = @_;

    my %form =  (
                    UserName    => $username,
                    Password    => $password,
                );

    return(request_token(\%form));
}

# Authenticate to RBAC server by location

sub authenticate_by_location()
{
    return(request_token({}));
}

# Obtain new token with a subset of roles in original token

sub reduce_roles($$)
{
    my ($original_token, $roles) = @_;

    my %form =  (
                    Origin  => encode_base64($original_token),
                    Role    => @$roles ? $roles : ['none'],
                );

    return(request_token(\%form));
}

# Authenticate to RBAC server by SAML Response (from SSO)
sub authenticate_by_saml($)
{
    my ($saml_response) = @_;

    my $form;

    if(!$saml_response)
    {
        die "Missing SAML Response";
    }

    $form->{SamlResponse} = decode_base64($saml_response);

    return(request_token($form));
}

# Request a token from RBAC servers

sub request_token($)
{
    my ($form) = @_;

    # Set account name and token format

    if(!$form->{SamlResponse})
    {
        $form->{AccountName}    = (getpwuid($<))[0];
    }

    $form->{TokenFormat}    = 'TEXT';

    # Send request to RBAC servers

    my $user_agent = LWP::UserAgent->new(ssl_opts => {
                                                        SSL_ca_file         => RBAC_PUBLIC_KEY_FILE,
                                                        SSL_verifycn_name   => 'RBAC',
                                                        verify_hostname     => 1,
                                                        timeout             => 10
                                                     });

    my $response;
    for my $server (shuffle(RBAC_SERVERS))
    {
        $response = $user_agent->post('https://'.$server.':'.RBAC_PORT.'/'.RBAC_URL, $form);
        last if($response->is_success || $response->code == 401); # Stop if succeeded or bad credentials
    }
    defined($response) && $response->is_success or return(undef);

    return(decode_base64($response->content));
}

# Decode raw token into a hash

sub decode_token($)
{
    my ($raw_token) = @_;

    return(FGC::RBAC::Serialise::decode($raw_token));
}

# Serializes hash token back into original response string

sub serialize_token($)
{
    my ($token) = @_;
    return(encode_base64($token));
}

# Deserializes token from response string into hash token

sub deserialize_token($)
{
    my ($serialized_token) = @_;
    return(decode_base64($serialized_token));
}

# Print a token

sub print_token($)
{
    my ($token) = @_;

    for my $key (sort(keys(%$token)))
    {
        printf("%-25s: ", $key);

        if($key eq "LocationAddress")
        {
            print join('.', @{$token->{$key}});
        }
        elsif($key eq "Roles")
        {
            print join(' ', @{$token->{$key}}) if(defined($token->{$key}));
        }
        elsif($key eq "ExtraFields" || $key eq "Signature")
        {
            printf("%02X", $_) for(@{$token->{$key}});
        }
        elsif($key =~ /Time$/)
        {
            print scalar(localtime($token->{$key}));
        }
        else
        {
            print $token->{$key};
        }
        print "\n";
    }
}

# Read RBAC token file

sub read_rbac_token_file($)
{
    my ($rbac_file_name) = @_;

    open(TOKEN_FILE, '<', $rbac_file_name)
            or die "Failed to open RBAC token file\n";
    binmode(TOKEN_FILE);
    my $binary_token = join('', <TOKEN_FILE>);
    close(TOKEN_FILE);

    # Return parsed and binary token

    {
        decoded => decode_token(\$binary_token),
        binary  => $binary_token
    };
}

{
    my $user;
    my $password;

    sub authenticate_explicit()
    {
        if (!defined($user) and !defined($password))
        {
            # Prompt for user

            print STDERR "User: ";
            chomp($user = <STDIN>);

            # Prompt for password

            print STDERR "Password: ";
            system("stty -echo");
            chomp($password = <STDIN>);
            system("stty echo");
            print STDERR "\n";
        }

        # Password has already been requested

        authenticate($user, $password);
    }
}

# EOF
