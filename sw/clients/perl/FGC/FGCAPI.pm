#!/usr/bin/perl -w
#
# Filename: FGC/FGCAPI.pm
# Purpose:  Perl module to communicate with FGC REST API
# Author:   Kevin Kessler
# WIP

package FGC::FGCAPI;

use REST::Client;
use FGC::RBAC;
#use Data::Dumper;
use JSON;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        get_property
                    );

my $fgc_api = REST::Client->new({
    host    => 'https://ccs-webtools-preprod.cern.ch/fgc-api/api',
    #cert    => '/path/to/ssl.crt',
    #key     => '/path/to/ssl.key',
    #ca      => '/path/to/ca.file',
    #timeout => 10,
});

# HOW TO USE:
#sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
#my $binary_rbac_token = FGC::RBAC::authenticate('pclhc', ']0ntrol9');
#my $base64_rbac_token = trim(FGC::RBAC::serialize_token($binary_rbac_token));
#my $property = 'time.now';
#my $devices = ['cfc-866-reth2', 'cfc-866-reth3', 'fake-device'];
#get_property($base64_rbac_token, $property, $devices);

sub get_property($$$)
{
    my ($rbac_base64, $property, $devices_array_ref) = @_;

    my $endpoint = '/devices/rbac/batch/'.$property;
    my $data = {
        'rbac_token' => $rbac_base64,
        'fgcs' => $devices_array_ref,
    };
    #print(Dumper($data));
    my $response_hash = _send_post_request($endpoint, $data);
    #print(Dumper($response_hash));
    return $response_hash;
}

sub _send_post_request($$)
{
    my ($endpoint, $data) = @_;
    return _send_request('POST', $endpoint, $data);
}

sub _send_get_request($$)
{
    my ($endpoint, $data) = @_;
    return _send_request('GET', $endpoint, $data);
}

sub _send_put_request($$)
{
    my ($endpoint, $data) = @_;
    return _send_request('PUT', $endpoint, $data);
}

sub _send_request($$$)
{
    my ($request_type, $endpoint, $data) = @_;

    my $request_body = encode_json($data);
    $fgc_api->request($request_type, $endpoint, $request_body);

    my $response_hash = {};
    if($fgc_api->responseCode() eq '200'){
        $response_hash = decode_json($fgc_api->responseContent())->{'responses'};
    }
    else{
        print("FGC-API Error: ".$fgc_api->responseCode(). " - ".$fgc_api->responseContent());
        my $error_code = $fgc_api->responseCode();
        my $error_detail = $fgc_api->responseContent()->{'detail'};
        my $error_msg = defined($error_detail) ? decode_json($error_detail) : "Unknown Error";
        $response_hash = {'error' => 'FGC-API Error: '.$error_code.' - '.$error_msg};
    }
    return $response_hash;
}