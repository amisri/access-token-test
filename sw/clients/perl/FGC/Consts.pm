#!/usr/bin/perl -w
#
# Name:     FGC/Consts.pm
# Purpose:  Constants for the FGC system
# Author:   Stephen Page

package FGC::Consts;

use Cwd;
use File::Find;
use File::Basename;
use Exporter;
use strict;

# Include all the FGC::Class::$class::Auto modules
# The code assumes that the FGC::Class modules are under FGC/Class path

our @fgc_decode_func;
our @fgc_cmw_fields_func;

my($filename, $dir, $suffix) = fileparse(__FILE__);

find(
    {
        wanted =>
        sub
        {
            my($filename, $directories, $suffix) = fileparse($_);
            my $cwd = getcwd();

            if($filename eq "Auto.pm")
            {
                if ($cwd =~ /FGC\/Class\/(\d+)/)
                {
                    my $class = $1;
                    my ($path) = ($cwd =~ /(.*)/ );

                    require "$path/Auto.pm";

                    # SUB decoding function

                    my $dec_function = "FGC::Class::" . $class . "::Auto::decode";
                    $fgc_decode_func[$class] = \&$dec_function;

                    # SUB fields metada function

                    my $field_function = "FGC::Class::" . $class . "::Auto::cmw_fields";
                    $fgc_cmw_fields_func[$class] = \&$field_function
                }
            }
        },
        untaint => 1
    }  ,$dir);

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                    @fgc_decode_func
                    @fgc_cmw_fields_func
                    %fgc_responsible_addresses
                    FGC_CODE_NUM_SLOTS
                    FGC_DIAG_N_LISTS
                    FGC_DIAG_N_SYNCH_BYTES
                    FGC_DIAG_PERIOD
                    FGC_DIAG_PRECLASS_PAD
                    FGC_DIAG_SIZE
                    FGC_DROPBOX_PATH
                    FGC_FGCD_LOG_PATH
                    FGC_FGCD_PORT
                    FGC_FIELDBUS_CYCLE_FREQ
                    FGC_FIELDBUS_CYCLE_PERIOD_MS
                    FGC_HOW_TO_PATH
                    FGC_HOW_TO_IMAGE_PATH
                    FGC_MANAGER_CONFIG_PATH
                    FGC_MANAGER_LOG_PATH
                    FGC_MAX_DEV_LEN
                    FGC_MAX_DEVS_PER_GW
                    FGC_MAX_FGCS_PER_GW
                    FGC_MONITOR_DEV_LIST_PATH
                    FGC_POWER_SIG_ALL
                    FGC_POWER_SIG_BOOT_CRASH
                    FGC_RUNLOG_EL_SIZE
                    FGC_RUNLOG_N_ELS
                    FGC_STAT_PRECLASS_PAD
                    FGC_STATUS_SIZE
                    FGCD_MAX_DEVS
                    FGC_MAX_FGCDS
                    SYS_NAME_SIZE
                    FGC_PRODUCTION_ACCESS
                    FGC_PRODUCTION_LIBS
                );

use constant FGC_CODE_NUM_SLOTS             => 32;                                                                                  # Number of slots for FGC code download from gateway
use constant FGC_DIAG_N_LISTS               => 4;                                                                                   # Number of diagnostic lists
use constant FGC_DIAG_N_SYNCH_BYTES         => 6;                                                                                   # Number of synchronisation bytes for serial diag
use constant FGC_DIAG_PERIOD                => 10;                                                                                  # Diagnostic period in 20ms steps
use constant FGC_DIAG_PRECLASS_PAD          => 2;                                                                                   # Bytes of padding before class data in fgc_diag structure
use constant FGC_DIAG_SIZE                  => 172;                                                                                 # Size of diagnostic data block
use constant FGC_DROPBOX_PATH               => $^O =~ /win/i ? q(\\cs-ccr-samba1\poccdev\dropbox)    : '/user/poccdev/dropbox';     # Path to FGC dropbox
use constant FGC_FGCD_LOG_PATH              => $^O =~ /win/i ? q(\\cs-ccr-samba1\pclhc\var\log\fgcd) : '/user/pclhc/var/log/fgcd';  # Path to FGCD log files
use constant FGC_FGCD_PORT                  => 1905;                                                                                # FGCD TCP port number
use constant FGC_FIELDBUS_CYCLE_PERIOD_MS   => 20;                                                                                  # Fieldbus cycle period in ms
use constant FGC_FIELDBUS_CYCLE_FREQ        => 1000 / FGC_FIELDBUS_CYCLE_PERIOD_MS;                                                 # Number of fieldbus cycles per second
use constant FGC_HOW_TO_PATH                => FGC_DROPBOX_PATH.'/howto';                                                           # Path to "how to" documents
use constant FGC_HOW_TO_IMAGE_PATH          => FGC_HOW_TO_PATH.'/images';                                                           # Path to "how to" images
use constant FGC_MANAGER_CONFIG_PATH        => '/user/pclhc/etc/fgc_config_manager';                                                # Path to FGC Configuration Manager configuration
use constant FGC_MANAGER_LOG_PATH           => '/user/pclhc/var/log/fgc_config_manager';                                            # Path to FGC Configuration Manager logs
use constant FGC_MAX_DEV_LEN                => 31;                                                                                  # Maximum device name length
use constant FGC_MAX_DEVS_PER_GW            => 31;                                                                                  # Maximum number of devices on a gateway
use constant FGC_MAX_FGCS_PER_GW            => 30;
use constant FGC_MAX_DB_PROP_SIZE_BYTES     => 1024 * 1024;                                                                                 # Maximum number of FGCs on a gateway
use constant FGC_MONITOR_DEV_LIST_PATH      => '/user/pclhc/etc/fgc_monitor/device_lists';                                          # Path to FGC Monitor device lists
use constant FGC_POWER_SIG_ALL              => 200;                                                                                 # Number of power signals to power-cycle all FGCs
use constant FGC_POWER_SIG_BOOT_CRASH       => 50;                                                                                  # Number of power signals to power-cycle FGCs in the boot program or crashed
use constant FGC_RUNLOG_EL_SIZE             => 6;                                                                                   # Size of a struct fgc_runlog
use constant FGC_RUNLOG_N_ELS               => 128;                                                                                 # Number of elements in FGC runlog
use constant FGC_STAT_PRECLASS_PAD          => 12;                                                                                  # Bytes of padding before class data in fgc_stat structure
use constant FGC_STATUS_SIZE                => 56;                                                                                  # Size of status block for device
use constant FGCD_MAX_DEVS                  => 65;                                                                                  # Maximum number of devices for an FGCD instance
use constant FGC_MAX_FGCDS                  => 400;                                                                                 # Maximum number of FGCDs
use constant SYS_NAME_SIZE                  => 256;                                                                                 # Size of a system hostname
use constant FGC_PRODUCTION_ACCESS          => 'pclhc@cs-ccr-dev1';                                                                 # Access to production user
use constant FGC_PRODUCTION_LIBS            => '/user/pclhc/lib/perl/FGC';                                                          # Path of FGC perl libraries in production

return(1);

# EOF
