#!/usr/bin/perl -w
#
# Filename: FGC/RBAC/Serialise.pm
# Purpose:  Decode serialisation format described by http://wikis/display/MW/Serialization
# Author:   Stephen Page

package FGC::RBAC::Serialise;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        decode
                    );

# Test whether platform is big endian

my $is_big_endian = unpack("h*", pack("s", 1)) =~ /01/;

return(1);

# Begin functions

# Decode encoded data

sub decode($)
{
    my ($data) = @_;

    # Split data into array of lines

    my @line = split(/\012/, $$data);

    # Read number of fields

    my $num_fields = $line[0];

    # Extract fields

    my %decoded;

    for(my $line_num = 1, my $i = 0 ; $i < $num_fields ; $i++)
    {
        my $field       = $line[$line_num++];
        my $type        = $line[$line_num++];
        my $type_base   = $type;

        # Read number of elements if necessary

        my $is_array    = 0;
        my $num_els     = 1;
        if($type eq 'byte_array')       # Byte array
        {
            $type_base  = 'byte';

            # Move past unneeded number of bytes

            $line_num++;
        }
        elsif($type =~ /^(.*)_array$/)  # Any array other than byte array
        {
            $type_base  = $1;
            $is_array   = 1;
            $num_els    = $line[$line_num++];
        }
        elsif($type eq 'string')        # String
        {
            # Move past unneeded string length

            $line_num++;
        }

        # Decode value

        my $value;
        if($is_array)
        {
            for(my $el_idx = 0 ; $el_idx < $num_els ; $el_idx++)
            {
                my $el_value = element_to_native($type, $type_base, $line[$line_num])
                    or warn "Failed to decode value for field $field at line $line_num\n" and return(undef);
                $line_num++;

                push(@$value, $el_value);
            }
        }
        else # Scalar value
        {
            $value = element_to_native($type, $type_base, $line[$line_num])
                or warn "Failed to decode value for field $field at line $line_num\n" and return(undef);
            $line_num++;
        }
        $decoded{$field} = $value;
    }

    return(\%decoded);
}

# Decode a single encoded element to its native value

sub element_to_native($$$)
{
    my ($type, $type_base, $element) = @_;

    # Handle different types of data

    if($type_base eq 'bool')
    {
        return($element);
    }
    elsif($type_base eq 'byte')
    {
        if($type eq 'byte')
        {
            return(hex($element));
        }
        else # ($type eq 'byte_array')
        {
            my $length = length($element);
            my @value;

            for(my $i = 0 ; $i < $length ; $i += 2)
            {
                push(@value, hex(substr($element, $i, 2)));
            }
            return(\@value);
        }
    }
    elsif($type_base eq 'short')
    {
        return($element);
    }
    elsif($type_base eq 'int')
    {
        return($element);
    }
    elsif($type_base eq 'long')
    {
        return($element);
    }
    elsif($type_base eq 'float')
    {
        return(unpack('f', pack('N', $element)));
    }
    elsif($type_base eq 'double')
    {
        if($is_big_endian)
        {
            return(unpack('d', pack('q', $element)));
        }
        else # Little-endian
        {
            my ($high, $low) = unpack('NN', pack('q', $element));
            return(unpack('d', pack('N', $low).pack('N', $high)));
        }
    }
    elsif($type_base eq 'string')
    {
        # Decode hex-encoded characters

        $element =~ s/\%([0-9a-fA-F]{2,2})/sprintf('%c', hex($1))/ge;
        return($element);
    }
    else # Unknown type
    {
        warn "Unknown type: $type\n";
        return(undef);
    }
}

# EOF
