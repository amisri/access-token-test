#!/usr/bin/perl -w
#
# Name:     FGC/Sync.pm
# Purpose:  Synchronous communication with FGC gateway
# Author:   Stephen Page

package FGC::Sync;

use bytes;
use Carp;
use Exporter;
use IO::Socket;
use IO::Select;
use Net::Ping;
use strict;

use FGC::Class::Common;
use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        TIMEOUT
                        connect
                        disconnect
                        get
                        rtermstart
                        rtermstop
                        set
                        can_write
                        can_read
                    );

our $TIMEOUT = 60;

my %_all_sockets;

# Use appropriate socket receive options for platform

my $recvopts;
if($^O =~ /win/i) # Platform is Microsoft Windows
{
    $recvopts = 0;
}
else # Platform is not Microsoft Windows
{
    $recvopts = MSG_WAITALL;
}

return(1);


# Begin functions

# Connect to a gateway

sub connect($)
{
    my ($gateway) = @_;

    # Check that gateway exists

    die "ERROR: Gateway must be passed as an argument to connect function\n" if(!defined($gateway) or !defined($gateway->{name}));

    # Do nothing if already connected

    return($gateway->{socket}) if(defined($gateway->{socket}) and $gateway->{socket}->connected());

    # Do nothing on SIGPIPE

    $SIG{PIPE} = sub {};

    # Get gateway IP address

    my $address = gethostbyname($gateway->{name});

    die "ERROR: Cannot find ip address of passed gateway name $gateway->{name}\n" if(!defined($address));
    $gateway->{ip} = inet_ntoa($address);

    # Attempt to ping gateway
    my $pinger = Net::Ping->new("tcp", 2);

    my ($ip ) = $gateway->{ip} =~ m/^(\d+\.\d+\.\d+\.\d+)$/;
    die "ERROR: Cannot ping gateway '$gateway->{name}: $!\n" if(!$pinger->ping($ip));

    # Open a socket to the gateway

    my $socket = IO::Socket::INET->new("$ip:".FGC_FGCD_PORT) or
    die "ERROR: Cannot create a socket to gateway '$gateway->{name}: $!\n";

    # Check that connection is okay

    my $sel = IO::Select->new($socket);

    eval { can_read($sel); };

    my $buffer;
    if($@ || !defined(recv($socket, $buffer, 1, $recvopts)))
    {
        close($socket);
        die "ERROR: Cannot receive any data from gateway '$gateway->{name}: $!\n";
    }

    if($buffer ne '+')
    {
        close($socket);
        die "ERROR: Received different data then expected '+' from gateway '$gateway->{name}\n";
    }
    print $socket "+";

    my $fh  = select($socket);
    $|      = 1;
    select($fh);

    # Add socket to the list of all sockets

    $_all_sockets{$socket} = 1;

    # Connection was a success

    $gateway->{socket} = $socket;

    return $socket;
}

# Disconnect from a gateway

sub disconnect($)
{
    my ($gateway) = @_;

    # Do nothing if not connected

    return if(!defined($gateway->{socket}));

    # Clean resources

    close($gateway->{socket});

    delete($_all_sockets{$gateway->{socket}}) if (defined($_all_sockets{$gateway->{socket}}));

    delete($gateway->{socket});


}

# Check if socket is readable with a timeout

sub can_read($)
{
    my ($select) = @_;

    # Check if we can write to the socket

    my @ready = $select->can_read($TIMEOUT);

    # Timeout

    if (! scalar(@ready))
    {
        die "Server side timeout while waiting for sync reply ($TIMEOUT sec)\n";
    }
    else
    {
        return 1;
    }
}

# Check if socket is writable with a timeout

sub can_write($)
{
    my ($select) = @_;

    # Check if we can write to the socket

    my @ready = $select->can_write($TIMEOUT);

    # Timeout

    if (! scalar(@ready))
    {
        die "Timeout waiting to sync request ($TIMEOUT sec)\n";
    }
    else
    {
        return 1;
    }

}

# Get a property

sub get($$$)
{
    my ($socket, $device, $property) = @_;

    # Check that the socket is still connected

    if (!$socket->connected())
    {
        die "ERROR: Socket to $device not connected\n";
    }

    # Create selector

    my $sel = IO::Select->new($socket);

    # Read remaining data on the TCP input buffer (in case there was a time-out in the previous request)

    {
       my $buffer;

       while(scalar($sel->can_read(0)) && $socket->read($buffer,1) > 0) {}
    }

    # Send command to gateway

    if (!can_write($sel) || !defined(send($socket, "! g $device:$property\n", 0)))
    {
        die "Write to socket failed: $!\n";
    }

    # Wait for response

    my $buffer;
    my %response;
    my $status;

    # Receive "$ "

    if(!can_read($sel) || !defined(recv($socket, $buffer, 2, $recvopts)) || $buffer eq "")
    {
        die "Read from socket failed (didn't receive '\$ '): $!\n";
    }

    # Receive status ('.' or '!')

    if(!can_read($sel) || !defined(recv($socket, $status, 1, $recvopts)) || $buffer eq "")
    {
        die "Read from socket failed (didn't receive '.' or '!'): $!\n";
    }
    $response{error} = $status eq '.' ? 0 : 1;

    # Receive "\n"

    if(!can_read($sel) || !defined(recv($socket, $buffer, 1, $recvopts)) || $buffer eq "")
    {
        die "Read from socket failed (didn't receive '\\n'): $!\n";
    }

    if(!can_read($sel) || !defined(recv($socket, $buffer, 1, $recvopts)) || $buffer eq "")
    {
        die "Read from socket failed (didn't receive second '\\n'): $!\n"
    }


    if(ord($buffer) == 0xFF) # Response is binary
    {
        $response{binary} = 1;

        # Receive length of binary response

        can_read($sel);

        recv($socket, my $length, 4, 0);
        $length = unpack("N", $length);

        # Receive binary response

        if(!can_read($sel) || !defined(recv($socket, $buffer, $length, $recvopts)) || $buffer eq "")
        {
            die "Read from socket failed (didn't receive buffer of length $length): $!\n";
        }
        $response{value} .= $buffer;

        # Receive "\n;"

        if(!can_read($sel) || !defined(recv($socket, $buffer, 2, $recvopts)) || $buffer eq "")
        {
            die "Read from socket failed (didn't receive '\\n;'): $!\n";
        }

        return(\%response);
    }
    else # Response is ASCII
    {
        $response{binary} = 0;
        $response{value} .= $buffer;

        while(can_read($sel) && defined(recv($socket, $buffer, 1, $recvopts)) && $buffer ne "")
        {
            $response{value} .= $buffer;

            # Check for end of response

            if($buffer eq ';')
            {
                chop($response{value}); # Remove ';'
                chop($response{value}); # Remove '\n'
                @{$response{values}} =  split(",", $response{value});

                return(\%response);
            }
        }
    }

    die "Read from socket failed: $!\n";
}

# Set a property

sub set($$$$;$)
{
    my ($socket, $device, $property, $values, $binary) = @_;

    # Check that the socket is still connected

    if (!$socket->connected())
    {
        die "ERROR: Socket to $device not connected\n";
    }

    # Create selector

    my $sel = IO::Select->new($socket);

    # Read remaining data on the TCP input buffer (in case there was a time-out in the previous request)

    {
       my $buffer;

       while(scalar($sel->can_read(0)) && $socket->read($buffer,1) > 0) {}
    }

    # Check whether $values is an array reference

    if(ref($values) eq "ARRAY")
    {
        # Convert values list to comma delimited string

        $values = join(',', @{$values});
    }

    # Send command to gateway

    $binary = 0 if(!defined($binary));

    if($binary) # Set of a binary value
    {
        can_write($sel);

        defined(send($socket, "! s $device:$property ".pack("CN", (0xFF, length($$values))).$$values, 0))
            or die "Write to socket failed\n";
    }
    else # Set of an ASCII value
    {
        can_write($sel);

        defined(send($socket, "! s $device:$property $values\n", 0))
            or die "Write to socket failed: $!\n";
    }

    # Wait for response

    my $c;
    my %response;

    $response{value} = "";

    while(can_read($sel) && defined(recv($socket, $c, 1, $recvopts)) && $c ne "")
    {
        $response{value} .= $c;

        # Check for end of response

        if($c eq ';')
        {
            $response{error} =  $response{value} =~ /\A\$.* \!\012/ ? 1 : 0;
            $response{value} =~ s/\A\$.* [\.\!]\012(.*)\012;\z/$1/s;

            return(\%response);
        }
    }

    die "Read from socket failed: $!\n";
}

# Start a remote terminal

sub rtermstart($$)
{
    my ($socket, $device) = @_;

    # Send a command to the gateway to start a remote terminal

    return(set($socket, 0, "CLIENT.RTERM", $device));
}

# Stop a remote terminal

sub rtermstop($)
{
    my ($socket) = @_;
    my $select   = IO::Select->new($socket);

    # Stop remote terminal

    can_write($select);
    defined(send($socket, "\030", 0)) or die "Write to socket failed: $!\n";

    my $c;
    my @end_chr = split(//, "Remote terminal disconnected.\n");
    my $end_idx = 0;
    while($end_idx != @end_chr && can_read($select) && defined(recv($socket, $c, 1, $recvopts)))
    {
        if($c eq $end_chr[$end_idx]) # Character matches next expected character
        {
            $end_idx++;
        }
        else # Character does not match next expected character
        {
            $end_idx = 0;
        }
    }
}

END
{
     for my $s (values(%_all_sockets))
     {
         close($s) if(defined(fileno($s)));
     }
}

# EOF
