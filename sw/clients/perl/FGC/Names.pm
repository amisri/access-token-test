#!/usr/bin/perl -w
#
# Name:     Names.pm
# Purpose:  Extract data from the FGC name file
# Author:   Stephen Page

package FGC::Names;

use Exporter;
use HTTP::Request;
use LWP::UserAgent;
use strict;

use FGC::Consts;

our @ISA            = qw(Exporter);
our @EXPORT         = qw(
                            FGC_SUB_NAME_FILE_BASE
                            FGC_SUB_KEEP_FILE_BASE
                        );
our @EXPORT_OK      = qw(
                             get_group_gateways
                             read
                             read_keep
                             read_group
                             read_sub
                             read_sub_keep
                             %sector_masks
                        );

# Constants

use constant FGC_GROUP_FILE             => $^O =~ /win/i ?                                              # FGCD group file
                                           q(\\cs-ccr-samba1\pclhc\etc\fgcd\group) :
                                           '/user/pclhc/etc/fgcd/group';
use constant FGC_GROUP_FILE_URL         => 'http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/group';          # FGCD group file URL
use constant FGC_KEEP_FILE              => $^O =~ /win/i ?                                              # FGCD keep file
                                           q(\\cs-ccr-samba1\pclhc\etc\fgcd\keep) :
                                           '/user/pclhc/etc/fgcd/keep';
use constant FGC_KEEP_FILE_URL         => 'http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/keep';            # FGCD keep file URL
use constant FGC_NAME_FILE              => $^O =~ /win/i ?                                              # FGC name file
                                           q(\\cs-ccr-samba1\pclhc\etc\fgcd\name) :
                                           '/user/pclhc/etc/fgcd/name';
use constant FGC_NAME_FILE_URL          => 'http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/name';           # FGC name file URL
use constant FGC_SUB_NAME_FILE_BASE     => $^O =~ /win/i ?                                              # FGC sub-device name file base
                                           q(\\cs-ccr-samba1\pclhc\etc\fgcd\sub_devices\\) :
                                           '/user/pclhc/etc/fgcd/sub_devices/';
use constant FGC_SUB_NAME_FILE_URL_BASE => 'http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/sub_devices/';   # FGC sub-device keep file URL base
use constant FGC_SUB_KEEP_FILE_BASE     => $^O =~ /win/i ?                                              # FGC sub-device keep file base
                                           q(\\cs-ccr-samba1\pclhc\etc\fgcd\sub_devices_keep\\) :
                                           '/user/pclhc/etc/fgcd/sub_devices_keep/';
use constant FGC_SUB_KEEP_FILE_URL_BASE => 'http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/sub_devices_keep/';   # FGC sub-device keep file URL base

use constant FGC_DATA_TYPE_NAME => 'name';
use constant FGC_DATA_TYPE_KEEP => 'keep';

my %data_type_to_file_map = (
    FGC_DATA_TYPE_NAME() => [FGC_NAME_FILE, FGC_NAME_FILE_URL, FGC_SUB_NAME_FILE_BASE, FGC_SUB_NAME_FILE_URL_BASE],
    FGC_DATA_TYPE_KEEP() => [FGC_KEEP_FILE, FGC_KEEP_FILE_URL, FGC_SUB_KEEP_FILE_BASE, FGC_SUB_KEEP_FILE_URL_BASE],
);

our %sector_masks = (
                        12      => 0x0001,
                        23      => 0x0002,
                        34      => 0x0004,
                        45      => 0x0008,
                        56      => 0x0010,
                        67      => 0x0020,
                        78      => 0x0040,
                        81      => 0x0080,
                        Beam    => 0x0100,
                    );

return(1);

# Begin functions

sub get_consts_by_data_type($){
    my ($data_type) = @_;

    if(!defined($data_type)){
        $data_type = get_default_data_type();
    }
    if(!defined($data_type_to_file_map{$data_type})){
        warn "ERROR Unsupported data type to read from: ".$data_type."\n";
        return(undef);
    }

    return $data_type_to_file_map{$data_type};
}

sub get_file_by_data_type($){
    my ($data_type) = @_;
    return @{get_consts_by_data_type($data_type)}[0];
}

sub get_url_by_data_type($){
    my ($data_type) = @_;
    return @{get_consts_by_data_type($data_type)}[1];
}

sub get_sub_file_by_data_type($){
    my ($data_type) = @_;
    return @{get_consts_by_data_type($data_type)}[2];
}

sub get_sub_url_by_data_type($){
    my ($data_type) = @_;
    return @{get_consts_by_data_type($data_type)}[3];
}

sub get_default_data_type(){
    return FGC_DATA_TYPE_NAME;
}

# Read names

sub read(;$$)
{
    my ($gateway_class, $data_type) = @_;

    my ($devices, $gateways);

    # Read name file from filesystem (if it exists) or from the web otherwise

    if(-e get_file_by_data_type($data_type))
    {
        ($devices, $gateways) = read_file($gateway_class, $data_type);
    }
    else
    {
        ($devices, $gateways) = read_web($gateway_class, $data_type);
    }

    return($devices, $gateways);
}

sub read_keep(;$)
{
    my ($gateway_class) = @_;
    return FGC::Names::read($gateway_class, FGC_DATA_TYPE_KEEP);
}

# Read names from FGC name file

sub read_file(;$$)
{
    my ($gateway_class, $data_type) = @_;

    my $file_path = get_file_by_data_type($data_type);
    my $file;
    if(!open($file, "<", $file_path))
    {
        warn "ERROR Could not read file ".$file_path."\n";
        return(undef);
    }

    my $file_content = join('', <$file>);
    close($file);

    return(read_data(\$file_content, $gateway_class, $data_type));
}

# Read names from web

sub read_web(;$$)
{
    my ($gateway_class, $data_type) = @_;

    my $url = get_url_by_data_type($data_type);
    my $user_agent  = LWP::UserAgent->new;
    my $request     = HTTP::Request->new(GET => $url);
    my $response    = $user_agent->request($request);

    if(!$response->is_success)
    {
        warn "ERROR retrieving $data_type file from web:\n\t".$response->status_line."\n";
        return(undef);
    }

    return(read_data(\$response->content, $gateway_class, $data_type));
}

# Read name or keep data

sub read_data($;$$)
{
    my ($file_content, $gateway_class, $data_type) = @_;

    # data_type can be 'name' or 'keep' and specifies the type of data to be parsed
    if(!defined($data_type)){
        $data_type = get_default_data_type();
    }

    # Read name file content line-by-line

    my %devices;
    my %gateways;

    my $line = 0;
    for(split("\n", $$file_content))
    {
        $line++;

        chomp;

        # Ignore comments

        if(substr($_, 0, 1) eq '#'){
            next;
        }

        my %device;

        # Construct entry from line

        (
            $device{gateway},
            $device{channel},
            $device{class},
            $device{name},
            $device{sector_mask},
        ) = split(':', $_);

        # Check that all fields are defined

        for my $key (keys(%device))
        {
            if(!defined($device{$key}))
            {
                warn "ERROR $key not defined in $data_type file line $line\n";
                return(undef);
            }
        }

        # Check that fields are valid

        if($device{channel} < 0)
        {
            warn "ERROR Device channel number smaller than 0 in $data_type file line $line\n";
            return(undef);
        }
        if($device{class} < 0 || $device{class} > 255)
        {
            warn "ERROR Device class number out of range [0,255] in $data_type file line $line\n";
            return(undef);
        }
        if(length($device{name}) > FGC_MAX_DEV_LEN)
        {
            warn "ERROR Device name $device{name} longer than " . FGC_MAX_DEV_LEN . " in $data_type file line $line\n";
            return(undef);
        }
        if($device{name} ne uc($device{name}))
        {
            warn "ERROR Device name $device{name} is not upper case in $data_type file line $line\n";
            return(undef);
        }
        if(defined($devices{$device{name}}))
        {
            warn "ERROR Duplicate $device{name} in $data_type file line $line\n";
            return(undef);
        }
        if(defined($gateways{$device{gateway}}) && defined($gateways{$device{gateway}}->{channels}->[$device{channel}]))
        {
            warn "ERROR Duplicate address in $device{gateway} in $data_type file line $line\n";
            return(undef);
        }
        if($device{sector_mask} !~ /^0x[0-9A-Fa-f]{4}$/)
        {
            warn "ERROR selector mask '".$device{sector_mask}."' is not a correctly formatted 4-byte hexadecimal number in $data_type file line $line\n";
            return(undef);
        }

        # Check that the gateway can be resolved

        if(!gethostbyname($device{gateway}))
        {
            warn "ERROR $device{gateway} cannot be resolved\n";
            return(undef);
        }

        # Check whether device name conforms to LHC naming convention

        if($device{channel} == 0) # Device is a gateway device
        {
            if($device{name} =~ /[^A-Z0-9\-]/)
            {
                warn "ERROR device name $device{name} contains illegal characters (allowed: A-Z, 0-9 or '-')";
                return(undef);
            }

            if($device{name} =~ /^c(f[cv]|s)-(.*?)-.*/i) # Name conforms to LHC naming convention
            {
                $device{zone} = "\U$2";
            }
        }
        else # Device is not a gateway device
        {
            if($device{name} =~ /[^A-Z0-9\.\_\-]/)
            {
                warn "ERROR device name $device{name} contains illegal characters (allowed: A-Z, 0-9, '_', '.' or '-')";
                return(undef);
            }

            if($device{name} =~ /^\w+\.(\w+)\.\w+/) # Name conforms to LHC naming convention
            {
                $device{zone} = "\U$1";
            }
        }

        $devices{$device{name}} = \%device;

        # Check whether gateway is already defined

        if(!defined($gateways{$device{gateway}})) # Gateway is not yet defined
        {
            $gateways{$device{gateway}}->{name} = $device{gateway};

            # Check whether gateway name conforms to LHC naming convention

            if($device{gateway} =~ /^c(f[cv]|s)-(.*?)-.*/i) # Name conforms to LHC naming convention
            {
                # Set gateway zone

                $gateways{$device{gateway}}->{zone} = "\U$2";
            }
        }

        $gateways{$device{gateway}}->{channels}->[$device{channel}] = \%device;

        $device{gateway}        = $gateways{$device{gateway}};
        $device{platform_id}    = int($device{class} / 10) * 10;
        $device{sector_mask}    = hex($device{sector_mask});
        my @gw_name_fields      = split(/-/,    $device{gateway}->{name});
        my @device_name_fields  = split(/[.-]/, $device{name});
        $device{type}           = @device_name_fields >= 3 ? ($device_name_fields[0] || "UNKNOWN") : "UNKNOWN";

        $device{location}       = @gw_name_fields     >= 3 &&
                                  @device_name_fields >= 3 ?
                                  ($device_name_fields[1] || "UNKNOWN") : "UNKNOWN";

        $device{sort_location}  = $device{location} ne "UNKNOWN"    &&
                                  $device_name_fields[0] eq "RPLA"  ?
                                  substr($device{location}, 2, 2).substr($device{location}, 0, 2) :
                                  $device{location};

        #EPCCCS-8515: introducing fieldbus_address key, as for sub devices the fieldbus_address differs from channel
        $device{fieldbus_address} = $device{channel};
    }

    for my $gateway (values(%gateways))
    {
        # Check that gateway device is defined

        if(!defined($gateway->{channels}->[0]))
        {
            warn "ERROR Missing Gateway device (address 0) for $gateway->{name} in $data_type file\n";
            return(undef);
        }
    }

    # If gateway class was specified remove data for all other classes of gateway

    if(defined($gateway_class))
    {
        for my $gateway (values(%gateways))
        {
            if($gateway->{channels}->[0]->{class} != $gateway_class)
            {
                delete($gateways{$gateway->{name}});

                for my $device (@{$gateway->{channels}})
                {
                    next if(!defined($device));
                    delete($devices{$device->{name}});
                }
            }
        }
    }

    if(scalar(keys(%gateways)) > FGC_MAX_FGCDS)
    {
        warn "ERROR Maximum number of FGCDs reached. You have to modify Consts.xml and FGC::Consts, and re-compile and deploy FGCD Class 2 (status server)\n";
        return(undef);
    }

    return(\%devices, \%gateways);
}

# Read sub-device names

sub read_sub($$;$)
{
    my ($devices, $gateways, $data_type) = @_;

    # Read sub-devices from filesystem (if directory exists) or from the web otherwise

    for my $gateway (keys(%$gateways))
    {
        # Does the subdevice directory exist?
        if(-e get_sub_file_by_data_type($data_type))
        {
            # Does a subdevice file exist for this gateway?
            if(-e get_sub_file_by_data_type($data_type).$gateway)
            {
                read_sub_file($devices, $gateway, $data_type);
            }
        }
        else
        {
            read_sub_web($devices, $gateway, $data_type);
        }
    }

    return(1);
}

# Read sub-device names from FGC keep file

sub read_sub_keep($$)
{
    my ($devices, $gateways) = @_;
    return FGC::Names::read_sub($devices, $gateways, FGC_DATA_TYPE_KEEP);
}

# Read sub-device names from FGC name file

sub read_sub_file($$;$)
{
    my ($devices, $gateway, $data_type) = @_;

    my $sub_file_path = get_sub_file_by_data_type($data_type).$gateway;
    my $sub_file;
    if(!open($sub_file, "<", $sub_file_path))
    {
        warn "ERROR could not open file ".$sub_file_path;
        return(undef);
    }

    my $sub_file_content = join('', <$sub_file>);
    close($sub_file);

    return(read_sub_data($devices, $gateway, \$sub_file_content, $data_type));
}

# Read sub-device names from web

sub read_sub_web($$;$)
{
    my ($devices, $gateway, $data_type) = @_;

    my $sub_file_url = get_sub_url_by_data_type($data_type).$gateway;
    my $user_agent  = LWP::UserAgent->new;
    my $request     = HTTP::Request->new(GET => $sub_file_url);
    my $response    = $user_agent->request($request);

    # If the subdevice file does not exist, then just do not parse the data

    if($response->is_success)
    {
        return(read_sub_data($devices, $gateway, \$response->content, $data_type));
    }
}

# Read sub-device data

sub read_sub_data($$$;$)
{
    my ($devices, $gateway, $sub_file_content, $data_type) = @_;

    # Read name file content line-by-line

    my $line = 0;
    my %sub_devices;
    for(split("\n", $$sub_file_content))
    {
        $line++;

        chomp;

        my %sub_device;

        # Construct entry from line

        (
            $sub_device{device},
            $sub_device{channel},
            $sub_device{alias},
        ) = split(':', $_);

        # Check that all fields are defined

        for my $key (keys(%sub_device))
        {
            if(!defined($sub_device{$key}))
            {
                warn "ERROR reading sub-device: Key not found data:$key in line:$line  (FEC:$gateway:)\n";
                return(undef);
            }
        }

        # Check that fields are valid

        if(!defined($devices->{$sub_device{device}}))
        {
            warn "ERROR sub-device not found in line:$line (FEC:$gateway:)\n";
            return(undef);
        }

        if(length($sub_device{device}) > (FGC_MAX_DEV_LEN))
        {
            warn "ERROR sub device name too long in line:$line  (FEC:$gateway:)\n";
            return(undef);
        }

        if(length($sub_device{alias}) > (FGC_MAX_DEV_LEN))
        {
            warn "ERROR sub-device alias too long in line:$line  (FEC:$gateway:)\n";
            return(undef);
        }

        if($sub_device{alias} =~ /[^A-Z0-9\.\_\-]/)
        {
            warn "ERROR sub-device alias $sub_device{alias} contains illegal characters (allowed: A-Z, 0-9, '_', '.' or '-')";
            return(undef);
        }

        if(defined($devices->{$sub_device{alias}}))
        {
            warn "ERROR sub-device is also a parent device in line:$line  (FEC:$gateway:)\n";
            return(undef);
        }

        if(defined($sub_devices{$sub_device{alias}}))
        {
            warn "ERROR sub device already exists in line:$line  (FEC:$gateway:)\n";
            return(undef);
        }

        if($devices->{$sub_device{device}}->{gateway}->{name} ne $gateway)
        {
            warn "ERROR wrong gateway for sub-device in line:$line  (FEC:$gateway:)\n";
            return(undef);
        }

        # Check whether device name conforms to LHC naming convention

        if($sub_device{alias} =~ /^(\w+)\.(\w+)\.\w+/) # Name conforms to LHC naming convention
        {
            $sub_device{type}   = "\U$1";
            $sub_device{zone}   = "\U$2";
        }

        #EPCCCS-8515: a sub device's fieldbus_address is equal to its parent's fieldbus_address. sub devices don't have physical bus addresses.
        $sub_device{fieldbus_address} = $devices->{$sub_device{device}}->{fieldbus_address};

        $sub_device{name} = $sub_device{device}.":".$sub_device{channel};
        $sub_devices{$sub_device{name}} = \%sub_device;
    }

    # Add sub-devices to devices

    for my $sub_device (values(%sub_devices))
    {
        $devices->{$sub_device->{device}}->{sub_devices}->[$sub_device->{channel}] = $sub_device;

        # Add alias to main device

        if($sub_device->{channel} == 0)
        {
            $devices->{$sub_device->{device}}->{alias} = $sub_device->{alias};
        }
    }

    # Set alias of those devices that do not have an alias defined equal to the device name
    # Reason: name and alias always should be set due to swapped alias logic of CCDB (https://wikis.cern.ch/display/TEEPCCCS/Converter+names%3A+Aliases+vs+full+names)
    for my $device (values(%$devices))
    {
        if(!defined($device->{alias}))
        {
            $device->{alias} = $device->{name};
        }
    }

    return(1);
}


# Read FGCD groups

sub read_group($$)
{
    my ($devices, $gateways) = @_;

    my $groups;

    # Read group file from filesystem (if it exists) or from the web otherwise

    if( -e FGC_GROUP_FILE )
    {
	    $groups = read_group_file($devices, $gateways);
    }
    else
    {
	    $groups = read_group_web($devices, $gateways) if(!defined($groups));
    }

    return($groups);
}

# Read groups from FGCD group file

sub read_group_file($$)
{
    my ($devices, $gateways) = @_;

    my $group_file;
    if(!open($group_file, "<", FGC_GROUP_FILE))
    {
        warn "ERROR Could not read group file ".FGC_GROUP_FILE."\n";
        return(undef);
    }

    my $group_file_content = join('', <$group_file>);
    close($group_file);

    return(read_group_data($devices, $gateways, \$group_file_content));
}

# Read groups from web

sub read_group_web($$)
{
    my ($devices, $gateways) = @_;

    # Read group file from web

    my $user_agent  = LWP::UserAgent->new;
    my $request     = HTTP::Request->new(GET => FGC_GROUP_FILE_URL);
    my $response    = $user_agent->request($request);

    # Return undef if response was an error

    if(!$response->is_success)
    {
        warn "ERROR retrieving group file from web:\n\t".$response->status_line."\n";
        return(undef);
    }

    return(read_group_data($devices, $gateways, \$response->content));
}

# Read group data

sub read_group_data($$$)
{
    my ($devices, $gateways, $group_file_content) = @_;

    my %groups_root = (
                        full_name   => '',
                        gateways    => {},
                        groups      => {},
                        name        => '',
                      );

    # Read group file content line-by-line

    my $line = 0;
    for(split("\n", $$group_file_content))
    {
        $line++;

        chomp;

        my %device;

        # Construct entry from line

        my ($gw_host, $timing_domain, $gw_full_group_name, $gw_comment) = split(':', $_);
        my @gw_groups = split("/", $gw_full_group_name);

        # Ignore entry if no groups are listed

        if(!@gw_groups)
        {
            warn "Group not defined at ".FGC_GROUP_FILE.":$line\n";
            return(undef);
        }

        # Check whether gateway is known

        my $gateway = $gateways->{$gw_host};
        if(!defined($gateway))
        {
            warn "Unknown gateway host $gw_host at ".FGC_GROUP_FILE.":$line\n";
            return(undef);
        }

        # Check whether gateway is already in a group

        if(defined($gateway->{group}))
        {
            warn "Gateway host $gw_host is already in group $gateway->{group}->{full_name} at ".FGC_GROUP_FILE.":$line\n";
            return(undef);
        }

        # Check that the full group name contains only permitted characters

        if($gw_full_group_name =~ /[^A-Za-z0-9\_\-\/ ]/)
        {
            warn "Group \"$gw_full_group_name\" contains illegal characters (allowed: A-Z, 0-9, '_', '-', '/' or ' ') at ".FGC_GROUP_FILE.":$line\n";
            return(undef);
        }

        # Check that the base group does not contain a space

        if($gw_groups[0] =~ / /)
        {
            warn "Base group \"$gw_groups[0]\" contains a space character, which is not permitted at ".FGC_GROUP_FILE.":$line\n";
            return(undef);
        }

        # Add the group to the overall groups tree

        my $group = \%groups_root;

        for(my $i = 0 ; $i < @gw_groups ; $i++)
        {
            # Initialise group to empty hash if it does not exist

            if(!defined($group->{groups}->{$gw_groups[$i]}))
            {
                $group->{groups}->{$gw_groups[$i]} = {
                                                        full_name   => join("/", @gw_groups[0..$i]),
                                                        gateways    => {},
                                                        groups      => {},
                                                        name        => $gw_groups[$i],
                                                     };
            }

            $group = $group->{groups}->{$gw_groups[$i]};
        }

        # Add the gateway to the group

        $group->{gateways}->{$gw_host} = $gateway;

        # Set the gateway's data from the group entry

        $gateway->{base_group} = $groups_root{groups}->{$gw_groups[0]};
        $gateway->{group} = $group;
        $gateway->{timing_domain} = $timing_domain;
        $gateway->{comment} = $gw_comment;
    }

    # Populate root group with gateways that are not in a group

    for my $gateway (values(%$gateways))
    {
        if(!defined($gateway->{group}))
        {
            $groups_root{gateways}->{$gateway->{name}} = $gateway;
            $gateway->{group} = \%groups_root;
        }
    }

    return(\%groups_root);
}

# Get a hash of all gateways within a group and its children

sub get_group_gateways($);
sub get_group_gateways($)
{
    my ($group) = @_;

    my %gateways = %{$group->{gateways}};

    # Add gateways within child groups

    for my $child_group (values(%{$group->{groups}}))
    {
        %gateways = (%gateways, get_group_gateways($child_group));
    }

    return(%gateways);
}

# EOF
