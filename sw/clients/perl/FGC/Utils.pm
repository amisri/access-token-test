#!/usr/bin/perl -w
#
# Name: FGC::Utils
# Purpose:  Collection of commonly used utility functions
# Author: Kevin Kessler

package FGC::Utils;

use FGC::Sync;
use File::Find;
use File::Slurp qw(read_file);
use strict;
use warnings;
use Data::Dumper;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        open_default_browser
                        get_typecode_from_systemname
                        get_typename_from_systemname
                        get_max_period_iters_by_class
                        get_iteration_frequency_from_device
                        read_property_from_device
                        read_property_from_gateway
                        read_property_from_device_safe
                        read_array_property_at_index_safe
                        logspace
                        linspace
                        log_base
                        is_one_or_more_numbers
                        looks_like_number
                        looks_like_integer
                        get_file_content
                        find_files
                        array_contains_element
                        array_contains_element_at
                        get_device_api
                        get_class_api
                        delta_ok
                        delta_within
                    );

###################
=pod

=item open_default_browser

Usage:
      open_default_browser( "http://www.example.com" );

Opens the given URL in the system's default web browser.

=cut
###################
sub open_default_browser($)
{
    my ($url) = @_;
    my $platform = $^O;
    my $cmd;

    if    ($platform eq 'darwin')  { $cmd = "open \"$url\" &";     } # Mac OS X
    elsif ($platform eq 'linux')   { $cmd = "xdg-open \"$url\" &"; } # Linux
    elsif ($platform eq 'MSWin32') { $cmd = "start /b $url";       } # Windows

    if (defined($cmd))
    {
        eval {
            system($cmd);
        };
        if($@) {
            die "Can not open default browser: $@";
        }
    }
    else
    {
        die "Can not open default browser for platform '$platform'. Not supported.";
    }
}

# returns the type_code that belongs to a given device name
# i.e. 'RPAGO.866.1.ETH4' returns 'RPAGO'
sub get_typecode_from_systemname($)
{
    my ($system_name) = @_;

    # capture part up to first non-dot
    $system_name    =~ /^([^.]*)/;
    my $type_code   = $1;
    return $type_code;
}

# returns the type_name that belongs to a given device name
# i.e. 'RPAGO.866.1.ETH4' returns 'HCRPAGO___'
sub get_typename_from_systemname($)
{
    my ($system_name) = @_;
    my $type_code = get_typecode_from_systemname($system_name);
    my $type_name = 'HC'.$type_code.('_' x (8 - length($type_code)));
    return $type_name;
}

# returns the hardcoded value for FGC_MAX_PERIOD_ITERS according to the definitions in class/defconst.h
sub get_max_period_iters_by_class($)
{
    my ($fgc_class) = @_;

    return 10000 if($fgc_class eq "63");
    return 10 if($fgc_class eq "92");
    return 20 if($fgc_class eq "94");

    die "ERROR: FGC_MAX_PERIOD_ITERS is not defined for class $fgc_class\n";
}

sub get_iteration_frequency_from_device($)
{
    my ($device) = @_;

    my $fgc_class = $device->{class};

    # For classes that use new cclibs, the property FGC.ITER_PERIOD can be used to determine the iteration frequency
    if($fgc_class =~ /^(63|92|94)$/)
    {
        eval{
            my $fgc_iter_period = read_property_from_device($device, "FGC.ITER_PERIOD");
            my $iter_freq = 1 / $fgc_iter_period;

            # round to nearest integer and return
            $iter_freq = sprintf "%.0f", $iter_freq;
            return $iter_freq;
        };
    }

    # If reading fails and for older classes we return the estimated hardcoded value (there is no such property for older classes)
    return 10000 if($fgc_class =~ /^(61|62|63)$/);
    return 1000 if($fgc_class =~ /^(51|53|94)$/);
    return 100 if($fgc_class =~ /^(92)$/);

    die "ERROR: No iteration_frequency for class $fgc_class\n";
}

sub get_regulation_frequency
{
    my ($device, $desired_reg_mode) = @_;

    if(!defined($desired_reg_mode))
    {
        die "ERROR: Undefined regulation mode\n";
    }

    if($desired_reg_mode =~ /I|B/)
    {
        my $load_select      = read_property_from_device($device, "LOAD.SELECT");
        my $reg_period_iters = read_array_property_at_index_safe($device, "REG.$desired_reg_mode.PERIOD_ITERS", $load_select, 1);
        my $reg_period       = read_property_from_device($device, "FGC.ITER_PERIOD") * $reg_period_iters;

        # Round to nearest integer

        my $reg_freq = sprintf("%.0f", (1 / $reg_period));

        return $reg_freq;
    }

    # For voltage regulation mode, the regulation frequency is always the same as iteration frequency
    elsif($desired_reg_mode =~ /V/)
    {
        return get_iteration_frequency_from_device($device);
    }

    die "ERROR: Regulation mode '$desired_reg_mode' not supported\n";
}

# Read and return the value of the given property from the given device (as string)
sub read_property_from_device($$)
{
    my ($device, $property) = @_;

    # Get property from device
    my $res;
    eval { $res = FGC::Sync::get($device->{gateway}->{socket}, $device->{channel}, $property) };

    # die on potential errors
    die "ERROR: Failure sending command to gateway $device->{gateway}->{name}: $@" if($@);
    die "ERROR: $res->{value} ($property)" if($res->{error});

    # return value
    return $res->{value};
}

# Read and return the value of the given property from the given gateway (as string)
sub read_property_from_gateway($$)
{
    my ($gateway, $property) = @_;

    # Get property from gw
    my $res;
    eval { $res = FGC::Sync::get($gateway->{socket}, 0, $property) };

    # die on potential errors
    die "ERROR: Failure sending command to gateway $gateway->{name}: $@" if($@);
    die "ERROR: $res->{value} ($property)" if($res->{error});

    # return value
    return $res->{value};
}

sub read_property_from_device_safe($$$)
{
    my ($device, $property, $default_on_error) = @_;

    # Read property from device
    my $val = '';
    eval { $val = read_property_from_device($device, $property); };
    if ($@) {
        warn "WARNING: Error while reading $property from device '$device->{name}': $@\n";
        return $default_on_error;
    }

    return $val;
}

sub read_array_property_at_index_safe($$$$)
{
    my ($device, $property, $index, $default_on_error) = @_;

    # read property from device
    my $res = read_property_from_device_safe($device, $property, $default_on_error);

    # split comma separated response into array
    my @vals = split(",", $res);
    if($index >= scalar(@vals) || $index < 0) {
        warn "WARNING: Index out of bounds for $property value '$res' ($index)";
        return $default_on_error;
    }

    # return value at index
    return $vals[$index];
}

# generate n logarithmically spaced points between $min and $max
sub logspace($$$$)
{
    my ($base, $min, $max, $n) = @_;

    my $log_min = log_base($base, $min);
    my $log_max = log_base($base, $max);
    my @log_vector = @{linspace($log_min, $log_max, $n)};
    for (my $i = 0; $i < scalar(@log_vector); $i++) {
        $log_vector[$i] = $base ** $log_vector[$i]
    }

    return \@log_vector;
}

# generates n linearly-spaced points between $min and $max
sub linspace($$$)
{
    my ($min, $max, $n) = @_;

    if($n <= 0)
    {
        return [];
    }

    if($n == 1)
    {
        return [ $min + ($max-$min)/2 ];
    }

    my @vector = ();
    my $stepsize = ($max - $min) / ($n -1);
    for (my $i = 0; $i < $n; $i++) {
        $vector[$i] = $min + ($stepsize * $i);
    }

    return \@vector;
}

# returns the logarithm of $base to $num
sub log_base($$)
{
    my ($base, $num) = @_;
    return log($num)/log($base);
}

# Checks if the given value is a number or a list of numbers (comma separated).
# They can be of following format: 500,500.0,5.0E+02
sub is_one_or_more_numbers($)
{
    my ($value) = @_;
    my $num_filter = qr /([+-]?\d+(\.\d+)?([eE][+-]?\d+)?)/;
    return ($value =~ /^$num_filter(,$num_filter)*$/);
}

# Checks if the given value is a number.
sub looks_like_number($)
{
    my ($value) = @_;
    my $num_filter = qr /([+-]?\d+(\.\d+)?([eE][+-]?\d+)?)/;
    return ($value =~ /^$num_filter$/);
}

sub looks_like_integer($)
{
    my ($num) = @_;
    return  $num =~ /^[+-]?\d+$/;
}

# Read and return the given file's content as string.
sub get_file_content($)
{
    my ($file) = @_;
    my $content;

    eval{ $content = read_file($file); };
    die "ERROR: $!\n" if($!);

    return $content;
}

# returns list of files and directories that contain the given search_query in their name
# does a recursive search starting from the given parent_dir up to max_depth deep
sub find_files($$$)
{
    my ($search_query, $parent_dir, $max_depth) = @_;

    # dir exists?
    return undef if(!(-d $parent_dir));

    # warn for deep max depths
    warn("WARNING: A max depth of $max_depth might result in a long find duration!\n") if($max_depth > 3);

    # remove trailing slash
    $parent_dir =~ s{/\z}{};

    # escape $search_query for regex
    $search_query = quotemeta($search_query);

    # calculates depth starting from parent_dir based on the remaining slashes
    # and only processes files / dirs that are within specified depth
    my $preprocess = sub() {
        $_ =~ s/$parent_dir//;
        my $depth = $_ =~ tr[/][];
        return @_ if $depth < $max_depth;
        return grep { not -d } @_ if $depth == $max_depth;
        return;
    };

    # adds matches to list
    my @findings = ();
    my $wanted = sub() {
        push(@findings, $_) if($_ =~ m/$search_query/);
    };

    # perform search
    find({preprocess=>$preprocess, wanted=>$wanted, no_chdir=>1}, $parent_dir);

    return \@findings;
}

sub array_contains_element($$)
{
    my ($element, $array_ref) = @_;
    return -1 != array_contains_element_at($element, $array_ref);
}

# returns -1 if element not found, otherwise returns the position / index of element
sub array_contains_element_at($$)
{
    my ($element, $array_ref) = @_;

    if(!defined($element) || !defined($array_ref))
    {
        return -1;
    }

    my $position = -1;

    for (my $i = 0; $i < scalar(@$array_ref); $i++)
    {
        my $curr_element = @$array_ref[$i];

        # delta for floating point value comparison
        my $floating_precision_delta = 0.000001;

        # string comparison
        if(is_string($element)
           && is_string($curr_element)
           && $element eq $curr_element)
        {
            $position = $i;
            last;
        }

        # numeric comparison
        elsif(looks_like_number($element)
              && looks_like_number($curr_element)
              && ($element == $curr_element || abs($element - $curr_element) < $floating_precision_delta))
        {
            $position = $i;
            last;
        }
    }

    return $position;
}

sub is_string($)
{
    my ($var) = @_;
    return ($var & ~$var);
}

# delta comparison for floating numbers with default delta
# compares to given numbers and returns true if there delta is smaller than 0.0000001
sub delta_ok($$)
{
    my ($a, $b) = @_;
    return delta_within($a, $b, 0.0000001);
}

# delta comparison for floating numbers with custom delta
# compares to given numbers and returns true if there delta is smaller than the given custom delta
sub delta_within($$$)
{
    my ($a, $b, $delta) = @_;
    return abs($a - $b) < $delta;
}

sub get_device_api($)
{
    my ($device) = @_;

    my $class_version = 0;

    # gateway classes, consist of 1 digit only
    # and devices of class 9x depend on the FGCD version of their gateway
    if($device->{class} =~ /^9?\d$/)
    {
        # TODO: verify FGCD version property with stephen
        $class_version = read_property_from_gateway($device->{gateway}, "DEVICE.VERSION.COMPILE.TIME");
    }

    # other classes support "CODE.VERSION.MAINPROG" property
    else
    {
        $class_version = read_property_from_device($device, "CODE.VERSION.MAINPROG");
    }

    return get_class_api($device->{class}, $class_version)
}

# EPCCCS-7247: Temporary hardcode class version specific API-Hashs - to be replaced by reading API from CCDB, once API management is in place
# Returns a hashref that represents the the API of the given class and classversion
sub get_class_api($$)
{
    my ($fgc_class, $class_version) = @_;

    # for gateways, classes 0-9
    if($fgc_class =~ /^\d$/)
    {
        #TODO: implement
        return undef;
    }
    # for classes that use cclibs v2
    if($fgc_class =~ /^(63|64|65|92|94)$/)
    {
        return get_cclibs2_api($class_version);
    }
    # classes that use cclibs v1
    else
    {
        return get_cclibs1_api($class_version);
    }
}

sub get_cclibs1_api()
{
    my ($class_version) = @_;

    my %shared_api =
    (
        KEY_LOAD_SELECT => "LOAD.SELECT",
        KEY_LOAD_TEST_SELECT => "LOAD.TEST_SELECT",

        KEY_REG_LAST_OP_DIV_I => "REG.I.PERIOD_DIV",

        KEY_LOAD_HENRYS => "LOAD.HENRYS",
        KEY_LOAD_OHMS_MAG => "LOAD.OHMS_MAG",
        KEY_LOAD_OHMS_PAR => "LOAD.OHMS_PAR",
        KEY_LOAD_OHMS_SER => "LOAD.OHMS_SER",
    );

    # example device: RFM.866.03.FIP5
    my %old_api =
    (
        KEY_REG_INT_CLBW_I => "REG.I.CLBW",
        KEY_REG_INT_CLBW2_I => "REG.I.CLBW2",
        KEY_REG_INT_Z_I => "REG.I.Z",

        KEY_REG_LAST_OP_R_I => "REG.I.OP.R",
        KEY_REG_LAST_OP_S_I => "REG.I.OP.S",
        KEY_REG_LAST_OP_T_I => "REG.I.OP.T",
        KEY_REG_LAST_OP_STATUS_I => "REG.I.OP.STATUS",

        # test support dropped in newer versions
        KEY_REG_LAST_TEST_DIV_I => "REG.TEST.I.PERIOD_DIV",
        KEY_REG_LAST_TEST_R_I => "REG.TEST.I.R",
        KEY_REG_LAST_TEST_S_I => "REG.TEST.I.S",
        KEY_REG_LAST_TEST_T_I => "REG.TEST.I.T",
    );

    # example device:
    my %new_api =
    (
        KEY_REG_INT_CLBW_I => "REG.I.INTERNAL.AUXPOLE1_HZ",
        KEY_REG_INT_CLBW2_I => "REG.I.INTERNAL.AUXPOLES2_HZ",
        KEY_REG_INT_Z_I => "REG.I.INTERNAL.AUXPOLES2_Z",

        KEY_REG_LAST_OP_R_I => "REG.I.LAST.OP.R",
        KEY_REG_LAST_OP_S_I => "REG.I.LAST.OP.S",
        KEY_REG_LAST_OP_T_I => "REG.I.LAST.OP.T",
        KEY_REG_LAST_OP_STATUS_I => "REG.I.LAST.OP.STATUS",
    );

    # return hashref to api depending on old or new version
    # 1522057699 represents the FGC_51 release of 26th March 2018
    my %diff_api = $class_version <= 1522057699 ? %old_api : %new_api;
    return {%shared_api, %diff_api};
}

sub get_cclibs2_api($)
{
    my ($class_version) = @_;

    my $api_changes =
    {
        # api as of before 29.06.2018
        # < 1530230400
        0 =>
        {
            KEY_CONFIG_SET => "CONFIG.SET",

            KEY_LIMITS_POS_I => "LIMITS.OP.I.POS",
            KEY_LIMITS_POS_B => "LIMITS.OP.B.POS",
            KEY_LIMITS_POS_V => "LIMITS.OP.V.POS",

            KEY_LIMITS_NEG_I => "LIMITS.OP.I.NEG",
            KEY_LIMITS_NEG_B => "LIMITS.OP.B.NEG",
            KEY_LIMITS_NEG_V => "LIMITS.OP.V.NEG",

            KEY_LOAD_SELECT => "LOAD.SELECT",
            KEY_LOAD_TEST_SELECT => "LOAD.TEST_SELECT",

            KEY_LOAD_HENRYS => "LOAD.HENRYS",
            KEY_LOAD_OHMS_MAG => "LOAD.OHMS_MAG",
            KEY_LOAD_OHMS_PAR => "LOAD.OHMS_PAR",
            KEY_LOAD_OHMS_SER => "LOAD.OHMS_SER",
            KEY_LOAD_GAUSS_AMP => "LOAD.GAUSS_PER_AMP",

            KEY_MEAS_DELAY_I => "MEAS.I_DELAY_ITERS",
            KEY_MEAS_DELAY_B => "MEAS.B_DELAY_ITERS",
            KEY_MEAS_DELAY_V => "MEAS.V_DELAY_ITERS",
            KEY_MEAS_FIR_I   => "MEAS.I_FIR_LENGTHS",
            KEY_MEAS_FIR_B   => "MEAS.B_FIR_LENGTHS",

            KEY_PRBS_AMP => "REF.PRBS.AMPLITUDE",
            KEY_PRBS_INITIAL => "REF.PRBS.INITIAL",
            KEY_PRBS_PERIOD_ITERS => "REF.PRBS.PERIOD_ITERS",
            KEY_PRBS_NUM_SEQ => "REF.PRBS.NUM_SEQUENCES",
            KEY_PRBS_K => "REF.PRBS.K",

            KEY_REG_EXT_ALG_I => "REG.I.EXTERNAL_ALG",
            KEY_REG_EXT_CLBW_I => "REG.I.EXTERNAL.CLBW",
            KEY_REG_EXT_MS_I => "REG.I.EXTERNAL.MEAS_SELECT",
            KEY_REG_EXT_MM_I => "REG.I.EXTERNAL.MOD_MARGIN",
            KEY_REG_EXT_SMOOTH_I => "REG.I.EXTERNAL.SMOOTH_ACTUATION",
            KEY_REG_EXT_TR_DELAY_I => "REG.I.EXTERNAL.TRACK_DELAY_PERIODS",
            KEY_REG_EXT_Z_I => "REG.I.EXTERNAL.Z",

            KEY_REG_EXT_ALG_B => "REG.B.EXTERNAL_ALG",
            KEY_REG_EXT_CLBW_B => "REG.B.EXTERNAL.CLBW",
            KEY_REG_EXT_MS_B => "REG.B.EXTERNAL.MEAS_SELECT",
            KEY_REG_EXT_MM_B => "REG.B.EXTERNAL.MOD_MARGIN",
            KEY_REG_EXT_SMOOTH_B => "REG.B.EXTERNAL.SMOOTH_ACTUATION",
            KEY_REG_EXT_TR_DELAY_B => "REG.B.EXTERNAL.TRACK_DELAY_PERIODS",
            KEY_REG_EXT_Z_B => "REG.B.EXTERNAL.Z",

            KEY_REG_EXT_OP_R_I => "REG.I.EXTERNAL.OP.R",
            KEY_REG_EXT_OP_S_I => "REG.I.EXTERNAL.OP.S",
            KEY_REG_EXT_OP_T_I => "REG.I.EXTERNAL.OP.T",
            KEY_REG_EXT_TEST_R_I => "REG.I.EXTERNAL.TEST.R",
            KEY_REG_EXT_TEST_S_I => "REG.I.EXTERNAL.TEST.S",
            KEY_REG_EXT_TEST_T_I => "REG.I.EXTERNAL.TEST.T",

            KEY_REG_EXT_OP_R_B => "REG.B.EXTERNAL.OP.R",
            KEY_REG_EXT_OP_S_B => "REG.B.EXTERNAL.OP.S",
            KEY_REG_EXT_OP_T_B => "REG.B.EXTERNAL.OP.T",
            KEY_REG_EXT_TEST_R_B => "REG.B.EXTERNAL.TEST.R",
            KEY_REG_EXT_TEST_S_B => "REG.B.EXTERNAL.TEST.S",
            KEY_REG_EXT_TEST_T_B => "REG.B.EXTERNAL.TEST.T",

            KEY_REG_EXT_ALG_V => "REG.V.EXTERNAL_ALG",
            KEY_REG_OP_V      => "REG.V.OP",
            KEY_REG_OP_VF     => "REG.V.FILTER.OP",

            KEY_REG_EXT_CLBW_V => "REG.V.EXTERNAL.CLBW",
            KEY_REG_EXT_KFF_V  => "REG.V.EXTERNAL.K_FF",
            KEY_REG_EXT_KINT_V => "REG.V.EXTERNAL.K_INT",
            KEY_REG_EXT_KP_V   => "REG.V.EXTERNAL.K_P",
            KEY_REG_EXT_Z_V    => "REG.V.EXTERNAL.Z",

            KEY_REG_EXT_CLBW_VF => "REG.V.FILTER.EXTERNAL.CLBW",
            KEY_REG_EXT_KD_VF   => "REG.V.FILTER.EXTERNAL.K_D",
            KEY_REG_EXT_KI_VF   => "REG.V.FILTER.EXTERNAL.K_I",
            KEY_REG_EXT_KU_VF   => "REG.V.FILTER.EXTERNAL.K_U",
            KEY_REG_EXT_Z_VF    => "REG.V.FILTER.EXTERNAL.Z",

            KEY_REG_INT_CLBW_I => "REG.I.INTERNAL.AUXPOLE1_HZ",
            KEY_REG_INT_CLBW2_I => "REG.I.INTERNAL.AUXPOLES2_HZ",
            KEY_REG_INT_PURE_I => "REG.I.INTERNAL.PURE_DELAY_PERIODS",
            KEY_REG_INT_Z_I => "REG.I.INTERNAL.AUXPOLES2_Z",

            KEY_REG_INT_CLBW_B => "REG.B.INTERNAL.AUXPOLE1_HZ",
            KEY_REG_INT_CLBW2_B => "REG.B.INTERNAL.AUXPOLES2_HZ",
            KEY_REG_INT_PURE_B => "REG.B.INTERNAL.PURE_DELAY_PERIODS",
            KEY_REG_INT_Z_B => "REG.B.INTERNAL.AUXPOLES2_Z",

            KEY_REG_LAST_I => "REG.I.LAST",
            KEY_REG_LAST_OP_I => "REG.I.LAST.OP",
            KEY_REG_LAST_OP_R_I => "REG.I.LAST.OP.R",
            KEY_REG_LAST_OP_S_I => "REG.I.LAST.OP.S",
            KEY_REG_LAST_OP_T_I => "REG.I.LAST.OP.T",
            KEY_REG_LAST_OP_A_I => "REG.I.LAST.OP.A",
            KEY_REG_LAST_OP_B_I => "REG.I.LAST.OP.B",
            KEY_REG_LAST_OP_ALGIDX_I => "REG.I.LAST.OP.ALG_INDEX",
            KEY_REG_LAST_OP_MM_I => "REG.I.LAST.OP.MOD_MARGIN",
            KEY_REG_LAST_OP_STATUS_I => "REG.I.LAST.OP.STATUS",

            KEY_REG_LAST_B => "REG.B.LAST",
            KEY_REG_LAST_OP_B => "REG.B.LAST.OP",
            KEY_REG_LAST_OP_R_B => "REG.B.LAST.OP.R",
            KEY_REG_LAST_OP_S_B => "REG.B.LAST.OP.S",
            KEY_REG_LAST_OP_T_B => "REG.B.LAST.OP.T",
            KEY_REG_LAST_OP_A_B => "REG.B.LAST.OP.A",
            KEY_REG_LAST_OP_B_B => "REG.B.LAST.OP.B",
            KEY_REG_LAST_OP_ALGIDX_B => "REG.B.LAST.OP.ALG_INDEX",
            KEY_REG_LAST_OP_MM_B => "REG.B.LAST.OP.MOD_MARGIN",
            KEY_REG_LAST_OP_STATUS_B => "REG.B.LAST.OP.STATUS",

            KEY_REG_LAST_TEST_R_I => "REG.I.LAST.TEST.R",
            KEY_REG_LAST_TEST_S_I => "REG.I.LAST.TEST.S",
            KEY_REG_LAST_TEST_T_I => "REG.I.LAST.TEST.T",
            KEY_REG_LAST_TEST_A_I => "REG.I.LAST.TEST.A",
            KEY_REG_LAST_TEST_B_I => "REG.I.LAST.TEST.B",
            KEY_REG_LAST_TEST_ALGIDX_I => "REG.I.LAST.TEST.ALG_INDEX",
            KEY_REG_LAST_TEST_MM_I => "REG.I.LAST.TEST.MOD_MARGIN",
            KEY_REG_LAST_TEST_STATUS_I => "REG.I.LAST.TEST.STATUS",

            KEY_REG_LAST_TEST_R_B => "REG.B.LAST.TEST.R",
            KEY_REG_LAST_TEST_S_B => "REG.B.LAST.TEST.S",
            KEY_REG_LAST_TEST_T_B => "REG.B.LAST.TEST.T",
            KEY_REG_LAST_TEST_A_B => "REG.B.LAST.TEST.A",
            KEY_REG_LAST_TEST_B_B => "REG.B.LAST.TEST.B",
            KEY_REG_LAST_TEST_ALGIDX_B => "REG.B.LAST.TEST.ALG_INDEX",
            KEY_REG_LAST_TEST_MM_B => "REG.B.LAST.TEST.MOD_MARGIN",
            KEY_REG_LAST_TEST_STATUS_B => "REG.B.LAST.TEST.STATUS",

            KEY_REG_MODE => 'REF.FUNC.REG_MODE',
            KEY_REG_MODE_CYC => 'REG.MODE_INIT',

            KEY_REG_PERIOD_ITERS_I => "REG.I.PERIOD_ITERS",
            KEY_REG_PERIOD_ITERS_B => "REG.B.PERIOD_ITERS",

            KEY_VS_ACT_DELAY => "VS.ACT_DELAY_ITERS",
            KEY_VS_ACTUATION => "VS.ACTUATION",
            KEY_VS_FIR_DELAY => "VS.FIRING.DELAY",
            KEY_VS_SIM_BW    => "VS.SIM.BANDWIDTH",
            KEY_VS_SIM_Z     => "VS.SIM.Z",

            KEY_VSF_FARADS1 => "VS.FILTER.FARADS1",
            KEY_VSF_FARADS2 => "VS.FILTER.FARADS2",
            KEY_VSF_HENRYS  => "VS.FILTER.HENRYS",
            KEY_VSF_OHMS    => "VS.FILTER.OHMS",
        },

        # changes from 29.06.2018
        # 1530230400
        1530230400 =>
        {
            KEY_PRBS_AMP => "REF.PRBS.AMPLITUDE_PP",
        },

        # changes from 19.12.2018
        # 1545177600
        1545177600 => {
            KEY_REG_EXT_CLBW_I => "REG.I.EXTERNAL.DESIRED_CLBW",
            KEY_REG_EXT_Z_I => "REG.I.EXTERNAL.DESIRED_Z",

            KEY_REG_EXT_CLBW_B => "REG.B.EXTERNAL.DESIRED_CLBW",
            KEY_REG_EXT_Z_B => "REG.B.EXTERNAL.DESIRED_Z",

            KEY_REG_EXT_CLBW_B => "REG.V.DESIRED_CLBW",
            KEY_REG_EXT_Z_B => "REG.V.DESIRED_Z",

            KEY_REG_EXT_CLBW_VF => "REG.V.FILTER.EXTERNAL.DESIRED_CLBW",
            KEY_REG_EXT_Z_VF    => "REG.V.FILTER.EXTERNAL.DESIRED_Z",

            KEY_REG_EXT_KFF_V  => "REG.V.K_FF",
            KEY_REG_EXT_KINT_V => "REG.V.K_INT",
            KEY_REG_EXT_KP_V   => "REG.V.K_P",

            KEY_REG_EXT_KD_VF   => "REG.V.FILTER.K_D",
            KEY_REG_EXT_KI_VF   => "REG.V.FILTER.K_I",
            KEY_REG_EXT_KU_VF   => "REG.V.FILTER.K_U",

            KEY_REG_EXT_ALG_V => undef,
            KEY_REG_OP_V      => "REG.V",
            KEY_REG_OP_VF     => "REG.V.FILTER",
        },

        # changes from 12.04.2019
        # 1555027200
        # MEAS.IBV_XYZ => MEAS.IBV.XYZ
        1555027200 =>
        {
            KEY_MEAS_DELAY_I => "MEAS.I.DELAY_ITERS",
            KEY_MEAS_DELAY_B => "MEAS.B.DELAY_ITERS",
            KEY_MEAS_DELAY_V => "MEAS.V.DELAY_ITERS",
            KEY_MEAS_FIR_I   => "MEAS.I.FIR_LENGTHS",
            KEY_MEAS_FIR_B   => "MEAS.B.FIR_LENGTHS",
        },

        # changes from 19.04.2019
        # 1555632000
        # back from desired to without desired and introduce reg.v.external and reg.v.filter.external again
        1555632000 =>
        {
            KEY_REG_EXT_CLBW_I => "REG.I.EXTERNAL.CLBW",
            KEY_REG_EXT_Z_I => "REG.I.EXTERNAL.Z",

            KEY_REG_EXT_CLBW_B => "REG.B.EXTERNAL.CLBW",
            KEY_REG_EXT_Z_B => "REG.B.EXTERNAL.Z",

            KEY_REG_EXT_CLBW_B => "REG.V.EXTERNAL.CLBW",
            KEY_REG_EXT_Z_B => "REG.V.EXTERNAL.Z",

            KEY_REG_EXT_CLBW_VF => "REG.V.FILTER.EXTERNAL.CLBW",
            KEY_REG_EXT_Z_VF    => "REG.V.FILTER.EXTERNAL.Z",

            KEY_REG_EXT_KFF_V  => "REG.V.EXTERNAL.K_FF",
            KEY_REG_EXT_KINT_V => "REG.V.EXTERNAL.K_INT",
            KEY_REG_EXT_KP_V   => "REG.V.EXTERNAL.K_P",

            KEY_REG_EXT_KD_VF   => "REG.V.FILTER.EXTERNAL.K_D",
            KEY_REG_EXT_KI_VF   => "REG.V.FILTER.EXTERNAL.K_I",
            KEY_REG_EXT_KU_VF   => "REG.V.FILTER.EXTERNAL.K_U",
        },

        # changes from 16.09.2019
        # 1560902400
        # REG MODE changes -> https://wikis.cern.ch/display/TEEPCCCS/Regulation+Mode
        1560902400 =>
        {
            KEY_REG_MODE => 'REG.MODE',
            KEY_REG_MODE_CYC => 'REG.MODE_CYC',
        },

        # changes from 27.07.2019
        1564215924 =>
        {
            # removed in commit dd31c35442b4144c1309f40b2973986e15d5fc12
            KEY_REG_EXT_SMOOTH_I => undef,
            KEY_REG_EXT_SMOOTH_B => undef,
        },
    };

    # create API hash based on class version.
    # take all changes into account of versions, older than the provided one
    my %api;
    for my $changes_version (sort(keys(%$api_changes)))
    {
        if($class_version >= $changes_version)
        {
            %api = (%api, %{$api_changes->{$changes_version}});
        }
    }

    return \%api;
}

return 1;
# EOF