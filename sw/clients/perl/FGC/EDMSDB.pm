#!/usr/bin/perl -w
#
# Filename: FGC/EDMSDB.pm
# Purpose:  Access functions for the EDMS Database
# Author:   Stephen Page

package FGC::EDMSDB;

use DBI;
use Exporter;
use strict;

use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        connect
                        disconnect
                        get_all_worldfip_addresses
                        get_circuit_details
                    );

# Constants

use constant FGC_EDMS_DB_ID         => 'dbi:Oracle:edmsdb'; # ID of EDMS database
use constant FGC_EDMS_DB_PASSWORD   => 'eGzyK6ti';          # Password for EDMS database
use constant FGC_EDMS_DB_USERNAME   => 'layout_app_power_conv';     # Username for EDMS database

my $connect_count = 0;

return(1);


# Begin functions

# Connect to database

sub connect()
{
    # Connect to database

    my $dbh = DBI->connect( FGC_EDMS_DB_ID,
                            FGC_EDMS_DB_USERNAME,
                            FGC_EDMS_DB_PASSWORD,
                            {PrintError => 0, RaiseError => 0}) or return(undef);
    $connect_count++;

    return($dbh);
}

# Disconnect from database

sub disconnect($)
{
    my ($dbh) = @_;

    # Disconnect from database

    $dbh->disconnect;
}

# Get all WorldFIP addresses

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_all_worldfip_addresses($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT GATEWAY, PC_NAME, WORLDFIP_ADDRESS
                                                     FROM ELEC_PC_DATA_VIEW
                                                     WHERE VERSION_LAYOUT='STUDY'
                                                     AND GATEWAY IS NOT NULL
                                                     AND WORLDFIP_ADDRESS IS NOT NULL
                                                     ORDER BY GATEWAY, TO_NUMBER(WORLDFIP_ADDRESS)") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            GATEWAY             => 1,
                                            PC_NAME             => 1,
                                            WORLDFIP_ADDRESS    => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get circuit details

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_circuit_details($$)
    {
        my ($dbh, $converter_name) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT I_MIN_OP, I_NOMINAL, L_TOT, MAX_DI_DT, R_PARALLEL, R_TOTAL, R_TOT_MEASURED
                                                     FROM LHCLAYOUT.ELEC_PC_DATA
                                                     WHERE VERSION_LAYOUT='STUDY' AND PC_NAME=?") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            I_MIN_OP        => 1,
                                            I_NOMINAL       => 1,
                                            L_TOT           => 1,
                                            MAX_DI_DT       => 1,
                                            R_PARALLEL      => 1,
                                            R_TOTAL         => 1,
                                            R_TOT_MEASURED  => 1,
                                        );
        }

        # Execute query

        $query->execute($converter_name) or return(undef);

        # Return result

        return($query->fetchall_arrayref(\%result_hash)->[0]);
    }
}

# EOF
