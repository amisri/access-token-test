#!/usr/bin/perl -w
#
# Name:     FGC/Class/Common.pm
# Purpose:  Decode common status for all classes 
# Author:   Stephen Page

package FGC::Class::Common;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(decode);

return(1);


# Begin functions

sub decode($$$)
{
    my ($buffer, $offset, $preclass_offset) = @_;

    my %status;

    (
        $status{DATA_STATUS}    , # Data status
        $status{CLASS_ID}       , # FGC class
        $status{RUNLOG_BYTE}    , # FGC runlog byte
        $status{ST_FAULTS}      , # Faults
        $status{ST_WARNINGS}    , # Warnings
    ) = unpack("x${offset}CCxCx${preclass_offset}nn", $$buffer);

    # Decode DATA_STATUS bit mask

    my $temp                = $status{DATA_STATUS};
    $status{DATA_STATUS}    = {};

    $status{DATA_STATUS}->{mask}            = $temp;
    $status{DATA_STATUS}->{DEVICE_IN_DB}    = $temp & 0x00000001;
    $status{DATA_STATUS}->{DATA_VALID}      = $temp & 0x00000002;
    $status{DATA_STATUS}->{CLASS_VALID}     = $temp & 0x00000004;

    return(\%status);
}

# EOF
