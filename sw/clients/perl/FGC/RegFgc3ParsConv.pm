#!/usr/bin/perl 

=begin
This script performs the conversion between a RegFGC3 parameter blob 
(typically stored in the REGFGC3.PARAMS property) and converts it into the series of commands
that would set the RegFGC3 parameters into the cards one by one. It also performs the oposite conversion. 

The script takes one argument, which is the file containing either:
1. A one-line blob in CSV format
2. A series of commands separated by new lines

The conversion is displayed in standard output. 
=cut

package FGC::RegFgc3ParsConv;

use warnings; 
use strict;

use Carp;
use Data::Dumper;
use Getopt::Long    qw(GetOptionsFromArray);
use List::MoreUtils qw(first_index);

# Constants
use constant SCIVS_SLOTS  		=> 32;
use constant SCIVS_COMPONENTS	=> 2; 
use constant SCIVS_BLOCKS		=> 7;
use constant BLOB_VERSION_1		=> 1;
use constant BLOB_VERSION_2		=> 2;
use constant BLOB_VERSION_4		=> 4;
use constant BLOB_MAGIC			=> 0xB10B;


# Global variables
my %regfgc3_pars = ();
my $hex_output = 0;
my $is_standalone = 0;


# Helper functions
sub _get_blob_version($)
{
	my $blob_ref = shift;
	return int($$blob_ref[0]) >> 16
}

sub _check_blob_integrity($)
{	
	# Check blob integrity
	my $blob_array_ref = shift;
	my $blob_version = @$blob_array_ref[0] >> 16;
	if (($blob_version != BLOB_VERSION_1) && ($blob_version != BLOB_VERSION_2) && ($blob_version != BLOB_VERSION_4))
	{
		print "Unrecognized blob version ".$blob_version."\n";
		return -1;
	}

	my $magic_word_idx = 0;
	my $found_magic_word = 0;
	foreach my $element (@$blob_array_ref)
	{
		my $shifted_element = $element & 0x0000FFFF;
		if ($shifted_element == BLOB_MAGIC)
		{
			$found_magic_word = 1;
			last;
		}
		$magic_word_idx++;
	}

	print "Magic word not found!" if $found_magic_word == 0;
	return $magic_word_idx;
}

sub _separate_meta_values($$)
{
	my ($blob_ref, $meta_data_limit) = @_; 

	my @words_16bit_array = ();

	for (my $i = 0; $i <= $meta_data_limit; $i++)
	{
		push(@words_16bit_array, (@$blob_ref[$i] >> 16) & 0xFFFF);
		push(@words_16bit_array, @$blob_ref[$i] & 0x0000FFFF);	
	}
	
	# Remove blob version and magic 
	shift @words_16bit_array;
	pop @words_16bit_array;

	my $length_blob = @$blob_ref; 
	my @values_array = @$blob_ref[$meta_data_limit + 1 .. $length_blob - 1];

	return (\@words_16bit_array, \@values_array);	
}

sub _get_size_index_v1($)
{
	my $word = shift;
	return (($word >> 8) & 0x00FF), ($word & 0x00FF);	
}

sub _get_size_index_v2($)
{
	my $word = shift;
	return (($word >> 11) & 0x00FF), ($word & 0b11111111111);	
}

sub _get_size_index($$)
{
	my ($word, $blob_version) = @_; 

	if ($blob_version == 1) 
	{
		return _get_size_index_v1($word);
	}

	if ($blob_version == 2) 
	{
		return _get_size_index_v2($word);
	}
}

sub _build_regfgc3_pars_hash($$$)
{
	my ($meta_ref, $values_ref, $blob_version) = @_;
	my ($pos_blob, $next_block_index) = (0, 0);
	my ($size, $index)                = (0, 0);
	my ($slot, $component, $block)    = (0, 0, 0);

	foreach my $word_16bit (@$meta_ref)
	{
		($size, $index) = _get_size_index($word_16bit, $blob_version);
		$slot = int($pos_blob / (SCIVS_BLOCKS * SCIVS_COMPONENTS));
		($component, $block) = (
							(($pos_blob % (SCIVS_BLOCKS * SCIVS_COMPONENTS) < SCIVS_BLOCKS) ? 'FPGA' : 'DSP'), 
							(($pos_blob % (SCIVS_BLOCKS * SCIVS_COMPONENTS) < SCIVS_BLOCKS) ? ($pos_blob - $slot * SCIVS_BLOCKS * SCIVS_COMPONENTS) : 
																							  ($pos_blob - $slot * SCIVS_BLOCKS * SCIVS_COMPONENTS - SCIVS_BLOCKS))
					);

		my @block_values = ($size > 0) ? @$values_ref[$next_block_index ... $next_block_index + $size - 1] : ();
		$regfgc3_pars{$slot}{$component}{$block} = \@block_values; 

		$next_block_index += $size; 
		$pos_blob++;
	}
}

sub _assemble_commands
{
	my @commands = (); 

	my $set_access 		= "!S REGFGC3.RAW.ACCESS:RW";
	my $set_slot 		= "!S REGFGC3.RAW.SLOT:";
	my $set_param_prop 	= "!S REGFGC3.RAW.PARAM:";
	my $set_block 		= "!S REGFGC3.RAW.BLOCK:";

	my $slot_already_pushed = 0; 
	
	foreach my $s (sort{$a<=>$b} keys %regfgc3_pars)
	{
		foreach my $c (sort{$b cmp $a} keys %{$regfgc3_pars{$s}})
		{
			foreach my $b (sort{$a<=>$b} keys %{$regfgc3_pars{$s}{$c}})
			{
				my $block_ref = $regfgc3_pars{$s}{$c}{$b};
				next if !@$block_ref;

				push @commands, $set_access    if ($slot_already_pushed == 0);
				push @commands, $set_slot."$s" if ($slot_already_pushed == 0);
				$slot_already_pushed = 1; 

				push @commands, $set_block."$b";

				if ($c eq 'DSP')
				{
					$set_param_prop = "!S REGFGC3.RAW.DSP_PARAM:";
				}
				
				foreach my $p (@$block_ref)
				{
					$set_param_prop .= defined($p) ? sprintf("0x%08X", $p) : sprintf("0x%08X", 0);
					$set_param_prop .= ',';
				}

				chop $set_param_prop;
				push @commands, $set_param_prop;

				# Reset params for next iteration
				$set_param_prop = "!S REGFGC3.RAW.PARAM:";
			}
		}

		$slot_already_pushed = 0;
	}

	return \@commands;
}

sub _print_commands($)
{	
	my $commands_ref = shift;

	foreach my $c (@{$commands_ref})
	{
		print "$c\n";
	}	
}

sub _print_blob($)
{
	my $blob_ref = shift;
	my $final_blob_string = join(',', @{$blob_ref});
	print "$final_blob_string\n";
}

sub _build_blob($)
{
	my $blob_version = shift;
	my (@blob_meta, @blob_values)= ((), ());
	my $block_index = 0;

	push(@blob_meta, $blob_version);

	foreach my $slot (0 .. SCIVS_SLOTS - 1)
	{
		foreach my $component ('FPGA', 'DSP')
		{
			for my $block (0 .. SCIVS_BLOCKS - 1)
			{	
				my $block_size 		= 0;
				my $word_32_bits 	= $block_index;

				my $block_array_ref = $regfgc3_pars{$slot}{$component}{$block};

				if (defined($block_array_ref) && scalar(@$block_array_ref) > 0)
				{
					$block_size = scalar @$block_array_ref;
					$word_32_bits = ($block_size << 8) | $word_32_bits;
					$block_index += $block_size; 
					push @blob_values, map(hex, @$block_array_ref);			
				}


				if ( (($component eq 'FPGA') && ($block % 2 == 0))
					|| (($component eq 'DSP') && ($block % 2 == 1)) )
				{
					my $last_value = pop @blob_meta;
					$word_32_bits = ($last_value << 16) | $word_32_bits;
				}

				push @blob_meta, $word_32_bits;
			}
		}
	}

	# push magic word
	my $magic = pop @blob_meta;
	$magic = $magic << 16 | BLOB_MAGIC;
	
	push @blob_meta, $magic;
	
	return (\@blob_meta, \@blob_values);
}

sub _parse_commands_file($)
{
	# Parse file into hash
	my $lines_ref = shift; 
	my ($slot, $block, $component) = (0, -1, 'FPGA');

	#parse file and build %regfgc3_pars
	foreach my $line (@$lines_ref)
	{
		# Skip empty lines
		next if $line =~ m/^$/;

		# Stop if file format does not conform to FGC3 commands
		die "Command file format is not correct!\n" if ($line !~ m/^!/);

		# Properly done with a finite state machine
		if ($line =~ m/^!S REGFGC3\.RAW\.ACCESS.*R[W|O]$/)
		{
			;
		}
		elsif ($line =~ m/^!S REGFGC3\.RAW\.SLOT.*(\d)$/)
		{
			$slot = $1;
		}
		elsif ($line =~ m/^!S REGFGC3\.RAW\.BLOCK.*(\d)$/)
		{
			$block = $1;
		}
		# elsif ($line =~ m/^!S REGFGC3\.RAW\.PARAM[_DSP]*.*(0x[0-9a-fA-F]+(,0x[0-9a-fA-F]+)*)$/)
		elsif ($line =~ m/^!S REGFGC3\.RAW\.PARAM[_DSP]*[:\s]*(.*)$/)
		{

			my @params = ();
			if (defined $1)
			{
				@params = split(/,/, $1);
			}

			if ($line =~ m/^!S.*PARAM_DSP.*$/)
			{
				$component = 'DSP';
			}

			if ( (($slot != 0) && 
				   ($slot > 0) && 
				   ($slot < SCIVS_SLOTS)) && 
				(($block != -1) && 
				  ($block >= 0) && 
				  ($block < SCIVS_BLOCKS)) )
			{
				$regfgc3_pars{$slot}{$component}{$block} = \@params; 
			}
		}
		else
		{
			print "ERROR: line $line does not contain a REGFGC3 relevant parameter\n";
		}

	}
}

sub _convert_to_hex($)
{
	my $decimal_blob = shift; 
	my @hex_blob = map(sprintf("0x%08X", $_), @$decimal_blob);
	return \@hex_blob;
}


# Main functions
sub blob_to_commands($)
{
	my $blob_string = shift;
	my @decimal_blob = split(/,/, $blob_string);
	@decimal_blob = map(hex, @decimal_blob) if($decimal_blob[0] =~ m/0x[0-9A-F]{8}/i);

	my @failed_response;

	my $metadata_limit = _check_blob_integrity(\@decimal_blob);
	if ($metadata_limit == -1)
	{
		push @failed_response, "Blob integrity check failed!";
		return \@failed_response;
	}

	my ($ref_meta, $ref_values) = _separate_meta_values(\@decimal_blob, $metadata_limit);

	_build_regfgc3_pars_hash($ref_meta, $ref_values, _get_blob_version(\@decimal_blob));

	my $commands_ref = _assemble_commands();

	_print_commands($commands_ref) if($is_standalone);

	return $commands_ref;
}

sub blob_to_commands_string($)
{
	my $input_blob = shift;
	my $commands_ref = blob_to_commands($input_blob);
	my $result = "";

	for my $command (@{$commands_ref})
	{
        $result .= $command."\n";
	}
	return $result; 
}

sub commands_to_blob($$)
{
	my ($lines_ref, $version) = @_;

	_parse_commands_file($lines_ref);

	my ($blob_meta_ref, $blob_values_ref) = _build_blob($version);

	my @final_blob = @$blob_meta_ref;
	push @final_blob, @$blob_values_ref;

	if ($hex_output)
	{
		@final_blob = @{_convert_to_hex(\@final_blob)};
	}

	_print_blob(\@final_blob) if($is_standalone);

	return \@final_blob;
}

sub usage
{
	print "\n\n Usage: $0 --file=<input_file> --version=<blob_version>
	Argument: 
	<input_file> from user's home directory. Contains the RegFGC3 parameters in one of two formats: 
	* blob (either in decimal or hexadecimal numbers) 
	* set of REGFGC3.RAW commands spaced by new lines

	Options:
	--file File with the blob in either format
	--help Prints this help\n\n";
}

sub isEmpty
{
    return (!defined($_[0]) || !length($_[0]));
}

sub getCmdLineOptions
{
	my @opts = @ARGV;
	my %options = ();
	
	GetOptionsFromArray(\@opts, \%options, 'file:s', 'version:s');

	if(isEmpty($options{file}))
	{
		usage();
		die "";
	}
	return \%options;
}

sub main
{
	$is_standalone = 1;
	my $options = getCmdLineOptions();
	open my $blob_fh, "<", $ENV{"HOME"}."/".$options->{file} or die "$options->{file}: $!";
	my @lines = <$blob_fh>;
	close $blob_fh;

	my $blob_version = $options->{version};

	chomp(@lines);
	if ((scalar @lines == 1) && $lines[0] =~ m/^[\d|0x]/)
	{
		chomp $lines[0];
		blob_to_commands($lines[0]);
	}
	elsif ($lines[0] =~ m/^![a-zA-Z]/)
	{
		commands_to_blob(\@lines, $blob_version);
	}
	else
	{
		usage();
	}
}

# Use as script and module
main() if not caller();

1;