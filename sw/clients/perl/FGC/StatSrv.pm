#!/usr/bin/perl -w
#
# Name:    StatSrv.pm
# Purpose: Provide access to FGC status data from FGC status server
# Author:  Stephen Page

package FGC::StatSrv;

use Carp;
use Exporter;
use FGC::Sync;
use IO::Socket;
use Net::Ping;
use strict;

use FGC::Class::Common;
use FGC::Consts;
use FGC::Names;
use FGC::Sync;

our @ISA        = qw(Exporter);
our @EXPORT     = qw(
                        FGC_STATSRV_HOST
                        FGC_STATSRV_UPDATE_SECS
                    );
our @EXPORT_OK  = qw(
                        connect
                        disconnect
                        getallstatus
                        getstatus
                    );

# Constants

use constant FGC_STATSRV_HOST           => 'cs-ccr-abpo1.cern.ch';                  # FGC status server hostname
use constant FGC_STATSRV_UPDATE_SECS    => 5;                                       # FGC status server data update period
use constant HOST_NAME_MAX              => 68;                                      # Size of a system hostname (note that this is includes null and padding)
use constant FGC_STATSRV_HEADER_SIZE    =>  ((FGC_MAX_DEV_LEN + 1) * FGCD_MAX_DEVS)  +   # Device names (FGC_MAX_DEV_LEN + nullchar from C)
                                            HOST_NAME_MAX                       +   # Gateway hostname
                                            8                                   +   # Data reception timestamp
                                            24;                                     # 24 bytes for fgc_udp_header, time_sec, time_usec
use constant FGC_STATSRV_FGCD_SIZE      => FGC_STATSRV_HEADER_SIZE +
                                           (FGCD_MAX_DEVS * FGC_STATUS_SIZE);       # Size of status server data from an FGCD instance

return(1);


# Begin functions

# Connect to the status server

sub connect()
{
    return(FGC::Sync::connect( { name => FGC_STATSRV_HOST } ));
}

# Disconnect from the status server

sub disconnect($)
{
    my ($socket) = (@_);

    return(FGC::Sync::disconnect( {socket => $socket } ));
}

# Get a status block from a single FGCD instance corresponding to a given device

sub getstatus($$;$)
{
    my ($socket, $device, $decode_only_common) = (@_);
    $decode_only_common = 0 if(!defined($decode_only_common));

    # Get data from status server

    my $response = FGC::Sync::get($socket, 1, $device);

    # Check whether an error occurred

    if($response->{error})
    {
        return(undef);
    }

    # Decode and return data

    return(decode_status($response->{value}, 0, $decode_only_common));
}

# Get status blocks from all FGCDs

sub getallstatus($;$)
{
    my ($socket, $decode_only_common) = @_;
    $decode_only_common = 0 if(!defined($decode_only_common));

    # Get data from status server

    my $response = FGC::Sync::get($socket, 1, 'ALL');

    # Check whether an error occurred

    if($response->{error})
    {
        return(undef);
    }

    # Decode data from each FGCD

    my %fgcds;
    my $length = length($response->{value});
    for(my $i = 0 , my $offset = 0 ; $i < ($length / FGC_STATSRV_FGCD_SIZE) ; $i++, $offset += FGC_STATSRV_FGCD_SIZE)
    {
        my $status = decode_status($response->{value}, $offset, $decode_only_common);

        if(defined($status))
        {
            $fgcds{$status->{hostname}} = $status;
        }
    }
    return(\%fgcds);
}

# Decode a status block from a single FGCD

sub decode_status($$;$)
{
    my ($buffer, $offset, $decode_only_common) = @_;
    $decode_only_common = 0 if(!defined($decode_only_common));

    # Decode header

    my @name;
    my %status;

    (
        $name[0],
        $name[1],  $name[2],  $name[3],  $name[4],  $name[5],  $name[6],  $name[7],  $name[8],
        $name[9],  $name[10], $name[11], $name[12], $name[13], $name[14], $name[15], $name[16],
        $name[17], $name[18], $name[19], $name[20], $name[21], $name[22], $name[23], $name[24],
        $name[25], $name[26], $name[27], $name[28], $name[29], $name[30], $name[31], $name[32],
        $name[33], $name[34], $name[35], $name[36], $name[37], $name[38], $name[39], $name[40],
        $name[41], $name[42], $name[43], $name[44], $name[45], $name[46], $name[47], $name[48],
        $name[49], $name[50], $name[51], $name[52], $name[53], $name[54], $name[55], $name[56],
        $name[57], $name[58], $name[59], $name[60], $name[61], $name[62], $name[63], $name[64],
        $status{hostname},
        $status{recv_time_sec},
        $status{recv_time_usec},
        $status{id},
        $status{sequence},
        $status{send_time_sec},
        $status{send_time_usec},
        $status{time_sec},
        $status{time_usec},
    ) = unpack(
                "x${offset}".
                (('Z'.(FGC_MAX_DEV_LEN + 1)) x FGCD_MAX_DEVS).    # names
                'Z'.(HOST_NAME_MAX).        # hostname
                'N'.                        # recv_time_sec
                'N'.                        # recv_time_usec
                'N'.                        # id
                'N'.                        # sequence
                'N'.                        # send_time_sec
                'N'.                        # send_time_usec
                'N'.                        # time_sec
                'N',                        # time_usec
                $buffer
              );

    $offset += FGC_STATSRV_HEADER_SIZE;

    # Return undefined if hostname for FGCD is empty

    if($status{hostname} eq '')
    {
        return(undef);
    }

    # Decode data for each device

    for(my $i = 0 ; $i < FGCD_MAX_DEVS ; $i++, $offset += FGC_STATUS_SIZE)
    {
        my $class = unpack("x${offset}xC", $buffer);

        # Decode class data

        my $device;
        if(!$decode_only_common && defined($fgc_decode_func[$class]))
        {
            $device = $fgc_decode_func[$class](\$buffer, $offset, FGC_STAT_PRECLASS_PAD);
        }
        else
        {
            $device = FGC::Class::Common::decode(\$buffer, $offset, FGC_STAT_PRECLASS_PAD);
        }

        $device->{CHANNEL}              = $i;
        $device->{NAME}                 = $name[$i] if($name[$i] ne '');
        $status{channels}->[$i]         = $device;
        $status{devices}->{$name[$i]}   = $device   if($name[$i] ne '');
    }

    return(\%status);
}

# EOF
