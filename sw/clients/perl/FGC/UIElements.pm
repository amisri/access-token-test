#!/usr/bin/perl -w
#
# Name:     FGC::UIElements
# Purpose:  Collection of perl TK functions that can be used to add certain UI Elements / Controls to FGCRun+ windows
# Author:   Kevin Kessler

package FGC::UIElements;

use File::HomeDir qw(home);
use File::Basename;
use strict;
use warnings;
use Scalar::Util qw(blessed looks_like_number);
use Tk;

use constant 
{
    FRAME_INNER_PADDING => 7,
    FRAME_OUTER_PADDING => 2,
    VERTICAL_SPACE => 5,
    FRAME_BORDER_SIZE => 2,
    FRAME_RELIEF => "groove",
    BUTTON_BORDER_SIZE => 2,
    BUTTON_RELIEF => "groove",
};

my $activetext_tag_id = 't000000'; # id counter for active text elements (e.g. hypertexts)

# sets the state of the given Tk ui_element and all of it's children to $state
# exceptions: list of class/package names and ui_elements that should be ignored (e.g. instances of 'Tk::Label')
sub set_state_to_all($$$);
sub set_state_to_all($$$)
{
  my ($ui_element, $exceptions, $state) = @_;

  # recurse for all children
  for my $child ($ui_element->children()) {
    set_state_to_all($child, $exceptions, $state);
  }

  # skip exceptions
  return if(!blessed($ui_element));
  for my $exception (@{$exceptions}) {
      return if($ui_element->isa($exception) || $ui_element eq $exception);
  }

  # set state to $state. surround by eval, in case $state is not supported by element
  eval{
    $ui_element->configure(-state => $state) if(defined($ui_element->cget('-state')));
  }
}

# sets the state of the given Tk ui_element and all of it's children to disabled
# exceptions: list of class/package names and ui_elements that should be ignored (e.g. instances of 'Tk::Label')
sub disable_all($$) 
{
  my ($ui_element, $exceptions) = @_;
  set_state_to_all($ui_element, $exceptions, 'disabled');
};

# sets the state of the given Tk ui_element and all of it's children to enabled
# exceptions: list of class/package names and ui_elements that should be ignored (e.g. instances of 'Tk::Label')
sub enable_all($$) 
{
  my ($ui_element, $exceptions) = @_;
  set_state_to_all($ui_element, $exceptions, 'normal');
};

# sets the state of the given Tk ui_element and all of it's children to readonly
# exceptions: list of class/package names and ui_elements that should be ignored (e.g. instances of 'Tk::Label')
sub protect_all($$) 
{
  my ($ui_element, $exceptions) = @_;
  set_state_to_all($ui_element, $exceptions, 'readonly');
};

sub add_button($$$$)
{
    my ($ui_parent, $text, $width, $callback) = @_;

    my $button = $ui_parent->Button(-border => BUTTON_BORDER_SIZE, -relief => BUTTON_RELIEF, -text => $text, -width => $width)->pack(-side => "top");
    $button->configure(-command => $callback) if(defined($callback));
    return $button;
}

# Vertical space, usually added after a frame to visually separate controls
sub add_vertical_space($)
{
    my ($ui_parent) = @_;
    my $space = $ui_parent->Frame(-height => VERTICAL_SPACE)->pack(-side => 'top', -expand => 0);
    return $space;
}

sub Tk::Separator
{
  my ($self, %rest) = @_;
  # horizontal by default
  my $direction = delete $rest{'-orient'} // 'horizontal';
  $self->Frame( 
    %{{ 
      %rest, 
      $direction eq 'vertical' ? '-width' : '-height' => 2 
    }}
  );
}

sub add_separator($$$)
{
  my ($ui_parent, $is_horizontal, $color) = @_;
  my $orientation = $is_horizontal ? 'horizontal' : 'vertical';
  my $fill = $is_horizontal ? 'x' : 'y';

  FGC::UIElements::add_vertical_space($ui_parent);
  my $separator = $ui_parent->Separator(-orient => $orientation, -relief => "groove")->pack(-side => 'top', -fill => $fill);
  FGC::UIElements::add_vertical_space($ui_parent);
  
  return $separator;
}

sub add_frame($$)
{
  my ($ui_parent, $append_vertical_space) = @_;

  my $frame = $ui_parent->Frame(-border => FRAME_BORDER_SIZE, -relief => FRAME_RELIEF)->pack(-side=> "top", -fill => "x", -padx => FRAME_OUTER_PADDING, -pady => FRAME_OUTER_PADDING); # Bordered frame
  $frame->Frame(-height => FRAME_INNER_PADDING)->pack(-side => 'top', -expand => 0); # Vertical margin inside frame (top)
  $frame->Frame(-height => FRAME_INNER_PADDING)->pack(-side => 'bottom', -expand => 0); # Vertical margin inside frame (bot)
  $frame->Frame(-width=> FRAME_INNER_PADDING)->pack(-side => "left"); #margin between left border and label
  $frame->Frame(-width=> FRAME_INNER_PADDING)->pack(-side => "right"); #margin between control and right border

  add_vertical_space($ui_parent) if($append_vertical_space);

  return $frame;
}

sub add_input_field($$$$$)
{
    my ($ui_parent, $label_text, $input_text, $space_width, $is_read_only) = @_;

    my $box  = $ui_parent->Frame()->pack(-side=> "top", -fill => "x");
    $box->Label(-text => $label_text)->pack(-side => "left");
    $box->Frame(-width=> $space_width)->pack(-side => "left"); #margin between label and control
    my $input_field = $box->Entry(-text => $input_text)->pack(-side => "left", -fill => "x", -expand => "x");
    $input_field->configure(-state => 'readonly') if($is_read_only);
    $input_field->configure(-bg => 'white') if(!$is_read_only);

    return $input_field;
}

sub add_yes_no_controls($$$$$)
{
  my ($ui_parent, $on_change_callback, $label_text, $default_bool, $space_width) = @_;

  my $bool = $default_bool; # 1 or 0

  # label and space
  my $box  = $ui_parent->Frame()->pack(-side=> "top", -fill => "x");
  $box->Label(-text => $label_text)->pack(-side => "left");
  $box->Frame(-width=> $space_width)->pack(-side => "left"); #margin between label and control

  # yes no radio-buttons
  my $control_frame = $box->Frame()->pack(-side => "left", -fill => "x", -expand => "x");
  my $rb_yes = $control_frame->Radiobutton(-text => 'Yes', -variable => \$bool, -value => 1)->pack(-side => "left");
  $control_frame->Frame(-width=>30)->pack(-side => "left"); #margin between yes and no
  my $rb_no = $control_frame->Radiobutton(-text => 'No', -variable => \$bool, -value => 0)->pack(-side => "left");

  # add callback, if defined
  $rb_yes->configure(-command => $on_change_callback) if(defined($on_change_callback));
  $rb_no->configure(-command => $on_change_callback) if(defined($on_change_callback));

  return \$bool;
}

sub add_dropdown_controls($$$$$$$)
{
  my ($ui_parent, $on_change_callback, $label_text, $options_values_ref, $options_texts_ref, $default_index, $space_width) = @_;

  die("ERROR: Can not add dropdown element. Array of options not defined.\n") if(!defined($options_values_ref));

  my @options_values = @{$options_values_ref};
  my @options_texts;

  if(defined($options_texts_ref))
  {
    @options_texts = @{$options_texts_ref};
    die("ERROR: Can not add dropdown element. Number of texts != number of values\n") if(scalar(@options_values) != scalar(@options_texts));
  }

  # label and dropdown control
  my $chosen_value;
  my $chosen_text;
  my $box  = $ui_parent->Frame()->pack(-side=> "top", -fill => "x");
  my $dropdown_label = $box->Label(-text => $label_text)->pack(-side => "left");
  $box->Frame(-width=> $space_width)->pack(-side => "left"); #margin between label and control
  my $dropdown = $box->Optionmenu(-variable => \$chosen_value, -textvariable => \$chosen_text, -anchor => "w")->pack(-side => "left", -fill => "x", -expand => "x");

  # populate dropdown control
  for (my $i = 0; $i < scalar(@options_values); $i++) 
  {
    my $text = defined($options_texts[$i]) ? $options_texts[$i] : $options_values[$i];
    $dropdown->addOptions([$text => $options_values[$i]]);
  }

  # set default selection
  if(defined($default_index) && scalar(@options_values) > $default_index && $default_index >= 0)
  {
    $chosen_value = $options_values[$default_index]; 
    $chosen_text = defined($options_texts[$default_index]) ? $options_texts[$default_index] : $options_values[$default_index];
  }

  # add callback, if defined
  $dropdown->configure(-command => $on_change_callback) if(defined($on_change_callback));

  return \$chosen_value;
}

sub add_slider_controls($$$$$$$$)
{
  my ($ui_parent, $on_change_callback, $label_text, $min, $max, $default, $delta, $space_width) = @_;

  # label and input field
  my $box  = $ui_parent->Frame()->pack(-side=> "top", -fill => "x");
  my $label = $box->Label(-text => $label_text)->pack(-side => "left");
  $box->Frame(-width=> $space_width)->pack(-side => "left"); #margin between label and controls
  my $input_field = $box->Entry(-text => '100', -width=>'7', -bg => 'white')->pack(-side => "left");
  $box->Frame(-width=> '2')->pack(-side => "left"); #margin between input field and slider
  
  # slider control
  my $slider = $box->Scale(
    -from => $min, 
    -to => $max, 
    -variable => $default, 
    -showvalue => 0, 
    -orient => 'horizontal', 
    -sliderlength => '10', 
    -resolution => $delta,
  )->pack(-side => 'left', -fill => "x", -expand => "x");
  
  # decrease slider thickness, to match height of input field
  my $slider_width = $slider->cget('-width') - 1;
  $slider->configure(-width => $slider_width);

  # link slider value to input field value
  $slider->configure(-command => 
    sub{
      my ($value) = @_;

      # update text of input field
      $input_field->configure(-text => $value);

      # call user defined callback
      $on_change_callback->($value) if(defined($on_change_callback));
    }
  );

  # link input field value to slider value
  $input_field->configure(
    -validate => 'all',
    -validatecommand => 
    sub{
      my ($value) = @_;

      my $is_valid = (looks_like_number($value) && $value >= $min && $value <= $max);

      # update slider value
      if($is_valid) {
        $slider->configure(-variable => $value);
      }

      # call user defined callback
      $on_change_callback->($value) if(defined($on_change_callback));

      return 1;
    }
  );

  return $input_field;
}

sub add_browse_disk_controls($$$)
{
  my ($ui_parent, $label_string, $default_value) = @_;

  # add label and input field
  my $box  = $ui_parent->Frame()->pack(-side=> "top", -fill => "x");
  my $left_area  = $box->Frame()->pack(-side=> "left", -expand => 1, -fill => "x");
  $left_area->Label(-text => $label_string, -anchor => "nw")->pack(-side => "top", -expand => 1, -fill => "x");
  my $input_field = $left_area->Entry(-bg   => "white", -text   => $default_value, -width => 35)->pack(-side => "top", -expand => 1, -fill => "x");
  $input_field->xview('end');

  # add browse button
  $box->Frame(-width => 5)->pack(-side => 'left'); # space between input field and button
  my $browse_button = add_button($box, "Browse", 6, undef);
  $browse_button->pack(-side => 'bottom');

  return ($input_field, $browse_button);
}

sub add_file_browser_controls($$$$$)
{
  my ($ui_parent, $label_string, $default_extension, $extensions, $default_file_path) = @_;

  # get directory part of the given file path
  my $dir;
  if(defined($default_file_path))
  {
    $dir = (fileparse($default_file_path))[1] 
  }
  if(!defined($dir) || !(-d $dir))
  {
    $dir = File::HomeDir::home();
  }

  # create UI controls
  my ($input_field, $browse_button) = add_browse_disk_controls($ui_parent, $label_string, $default_file_path);

  # set button logic (ask user to choose filepath)
  my $browse_files = sub 
  {
    my $path = $ui_parent->getOpenFile(
      -defaultextension   => $default_extension,
      -filetypes          => [[$label_string, $extensions]],
      -initialdir         => $dir,
    );

    if (!defined($path) || $path eq '') {
      return;
    }

    # modify input field content based on user selection
    $input_field->delete(0, 'end');
    $input_field->insert(0, $path);
    $input_field->xview('end');
  };
  $browse_button->configure(-command => $browse_files);

  return $input_field;
}

sub add_directory_browser_controls($$$)
{
  my ($ui_parent, $label_string, $default_directory) = @_;

  if(!defined($default_directory))
  {
    $default_directory = File::HomeDir::home();
  }

  $default_directory = (-d $default_directory) ? $default_directory : "";

  # create UI controls
  my ($input_field, $browse_button) = add_browse_disk_controls($ui_parent, $label_string, $default_directory);

  # set button logic (ask user to choose filepath)
  my $browse_dir = sub 
  {
    my $path = $ui_parent->chooseDirectory(-initialdir => $default_directory);

    if (!defined($path) || $path eq '') {
      return;
    }

    $input_field->delete(0, 'end');
    $input_field->insert(0, $path);
    $input_field->xview('end');
  };
  $browse_button->configure(-command => $browse_dir);

  return $input_field;
}

sub add_image($$$$)
{
  my ($ui_parent, $img_path, $max_width, $max_height) = @_;

  # load image
  my $img = $ui_parent->Photo(-file => $img_path);

  # downscale image, if it exceeds given max height/width
  if($max_width > 0 && $max_height > 0 && ($max_width < $img->width || $max_height < $img->height))
  {
    my $yfactor = $img->height / $max_height;
    my $xfactor = $img->width / $max_width;
    my $scaleFactor = $xfactor > $yfactor ? $xfactor : $yfactor;
    if($scaleFactor > 1)
    {
      $scaleFactor = int($scaleFactor) + 1;
      my $scaledimg = $ui_parent->Photo();
      $scaledimg->copy($img, -subsample => $scaleFactor);
      $img->destroy();
      $img = $scaledimg;
    }
  }

  # show image
  $ui_parent->Label(-image => $img)->pack(-side => 'top');

  return $img;
}

sub show_image_popup($$$$$)
{
  my ($ui_parent, $title, $img_path, $max_img_width, $max_img_height) = @_;

  my $popup = $ui_parent->Toplevel(
    -popover => $ui_parent,
    -title => $title,
  );

  add_image($popup, $img_path, $max_img_width, $max_img_height);
  return $popup;
}

sub show_confirmation_popup($$$)
{
  my ($ui_parent, $title, $message) = @_;

  my $confirm = $ui_parent->messageBox(
    -icon       => 'question',
    -message    => $message,
    -title      => $title,
    -type       => 'okcancel',
    -default    => 'cancel',
    -popover    => $ui_parent,
  );

  # returns true if Ok, false if Cancel or Close
  return lc($confirm) eq 'ok';
}

sub show_yes_no_popup($$$)
{
  my ($ui_parent, $title, $message) = @_;

  my $confirm = $ui_parent->messageBox(
    -icon       => 'question',
    -message    => $message,
    -title      => $title,
    -type       => 'yesno',
    -default    => 'no',
    -popover    => $ui_parent,
  );

  # returns true if Yes, false if No or Close
  return lc($confirm) eq 'yes';
}

# allows to attach callbacks to certain events on a part of given TK text element
sub add_active_text
{
    my ($ui_text, $linktext, $event_to_callback_hash) = @_;

    my %callbacks = %{$event_to_callback_hash};

    my $start_index = $ui_text->index('insert');
    $ui_text->insert(insert => $linktext);
    my $end_index = $ui_text->index('insert');

    my $tag_id = ++$activetext_tag_id;
    $ui_text->tagAdd($tag_id, $start_index, $end_index);

    foreach my $key (keys(%callbacks))
    {
      $ui_text->tagBind($tag_id, $key => $callbacks{$key}) ;
    }

    return $tag_id;
}

# attaches hyperlink like behaviour to a part of a given TK text element, invoking the given callback on click
sub add_hyperlink
{
    my ($ui_text, $link_text, $callback) = @_;

    my $tag_id;

    # prepare interaction with link
    my $event_handlers = {
      '<ButtonPress>' => sub {
        $ui_text->tagConfigure($tag_id, -relief => 'sunken'); 
      },
      '<ButtonRelease-1>' => sub {
        $ui_text->tagConfigure($tag_id, -relief => 'raised'); 
        $callback->();
      },
      '<Enter>' => sub { 
        $ui_text->tagConfigure($tag_id, -relief => 'raised'); 
        $ui_text->configure(-cursor => 'hand1'); 
      },
      '<Leave>' => sub { 
        $ui_text->tagConfigure($tag_id, -relief => 'flat'); 
        $ui_text->configure(-cursor => 'xterm'); 
      },
    };

    # make the link interactable
    $tag_id = add_active_text($ui_text, $link_text, $event_handlers);

    # default look of the hyperlink
    $ui_text->tagConfigure($tag_id, -foreground => 'blue', -underline => 1, -borderwidth => 1, -relief => 'flat');
}

return 1;
# EOF