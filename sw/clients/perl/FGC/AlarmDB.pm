#!/usr/bin/perl -w
#
# Filename: FGC/AlarmDB.pm
# Purpose:  Access functions for the LASER Alarm Database
# Author:   Stephen Page

package FGC::AlarmDB;

use DBI;
use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        add_fault_code
                        add_fault_member
                        connect
                        disconnect
                        flush_fault_codes
                        flush_fault_members
                        get_fault_codes
                        get_fault_families
                        get_fault_members
                    );

# Constants

use constant FGC_ALARM_DB_ID        => 'dbi:Oracle:accdb';  # ID of LHC Alarm Database
use constant FGC_ALARM_DB_USERNAME  => 'pofgc_alarm';       # Username for LHC Alarm Database

my $connect_count = 0;

return(1);


# Begin functions

# Connect to database

sub connect($)
{
    my ($password) = @_;

    # Connect to database

    my $dbh = DBI->connect(
                            FGC_ALARM_DB_ID,
                            FGC_ALARM_DB_USERNAME,
                            $password,
                            {PrintError => 1, RaiseError => 1},
                          ) or return(undef);
    $connect_count++;

    return($dbh);
}

# Disconnect from database

sub disconnect($)
{
    my ($dbh) = @_;

    # Disconnect from database

    $dbh->disconnect;
}

# Flush fault code table

{
    my $query;
    my $query_connect_count;

    sub flush_fault_codes($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("DELETE FROM POFGC_ALARM.FAULT_CODES") or return(1);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        $query->execute() or return(1);

        # Okay

        return(0);
    }
}

# Flush fault member table

{
    my $query;
    my $query_connect_count;

    sub flush_fault_members($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("DELETE FROM POFGC_ALARM.FAULT_MEMBERS") or return(1);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        $query->execute() or return(1);

        # Okay

        return(0);
    }
}

# Get fault families

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_fault_families($)
    {
        my ($dbh) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT DISTINCT FF
                                                     FROM POFGC_ALARM.FAULT_CODES
                                                     ORDER BY FF") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            FF  => 1,
                                        );
        }

        # Execute query

        $query->execute() or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get fault codes for a platform (fault family)

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_fault_codes($$)
    {
        my ($dbh, $platform) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT *
                                                     FROM POFGC_ALARM.FAULT_CODES
                                                     WHERE FF=?
                                                     ORDER BY FC") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            ACTION      => 1,
                                            CAUSE       => 1,
                                            CONSEQUENCE => 1,
                                            FF          => 1,
                                            FC          => 1,
                                            HELP_URL    => 1,
                                            PRIORITY    => 1,
                                            PROBLEM     => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$platform") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Get fault members for a platform (Fault Family)

{
    my $query;
    my $query_connect_count;
    my %result_hash;

    sub get_fault_members($$)
    {
        my ($dbh, $platform) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("SELECT *
                                                     FROM POFGC_ALARM.FAULT_MEMBERS
                                                     WHERE FF=?
                                                     ORDER BY SYSTEM") or return(undef);
            $query_connect_count    = $connect_count;
            %result_hash            =   (
                                            BC                      => 1,
                                            BUILDING                => 1,
                                            CA                      => 1,
                                            FF                      => 1,
                                            FLOOR                   => 1,
                                            HELP_URL_PARAM          => 1,
                                            IC                      => 1,
                                            IDENTIFIER              => 1,
                                            MNEM                    => 1,
                                            MULT                    => 1,
                                            PIQUET_EMAIL            => 1,
                                            PIQUET_PORTABLE_PHONE   => 1,
                                            POSITION                => 1,
                                            RESPONSIBLE_IDENT       => 1,
                                            ROOM                    => 1,
                                            RT                      => 1,
                                            SITE                    => 1,
                                            SOURCE                  => 1,
                                            SYSTEM                  => 1,
                                        );
        }

        # Execute query

        $query->execute("FGC_$platform") or return(undef);

        # Return array of result row hashes

        return($query->fetchall_arrayref(\%result_hash));
    }
}

# Add fault code

{
    my $query;
    my $query_connect_count;

    sub add_fault_code($$$$$$$$$)
    {
        my ($dbh, $class, $symbol, $code, $priority, $problem, $cause, $consequence, $action) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POFGC_ALARM.FAULT_CODES
                                                     (FC, FF, PRIORITY, PROBLEM, CAUSE, CONSEQUENCE, ACTION, HELP_URL)
                                                     VALUES (?, ?, ?, ?, ?, ?, ?, ?)") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Construct help URL

        my $help_url = "http://proj-fgc.web.cern.ch/proj-fgc/gendoc/def/Class${class}_SymList_";
        $help_url   .= ($code >= 16) && ($code <= 31) ? "FLT.htm" : "WRN.htm";
        $help_url   .= "#".(($code >= 16) && ($code <= 31) ? "FLT" : "WRN").".$symbol" if(defined($symbol));

        # Execute query

        $query->execute($code, "FGC_$class", $priority, $problem, $cause, $consequence, $action, $help_url) or return(undef);
    }
}

# Add fault member

{
    my $query;
    my $query_connect_count;

    sub add_fault_member($$$$$)
    {
        my ($dbh, $gw_dev, $name, $class, $location) = @_;

        # Construct query only once per connection

        if(!defined($query_connect_count) || $connect_count != $query_connect_count)
        {
            $query                  = $dbh->prepare("INSERT INTO POFGC_ALARM.FAULT_MEMBERS
                                                     (FF, SOURCE, FM, IDENTIFIER, CA, SYSTEM, MULT, RESPONSIBLE_IDENT, SITE, MNEM, PIQUET_PORTABLE_PHONE)
                                                     VALUES (?, ?, ?, ?, 'cern.lhc.power.test', 'FGC power converters', 'N', ?, 'LHC', ?, '163668')") or return(undef);
            $query_connect_count    = $connect_count;
        }

        # Execute query

        $query->execute("FGC_$class", $gw_dev, $name, $name, "52969", $location) or return(undef);
    }
}

# EOF
