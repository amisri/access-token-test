#!/usr/bin/perl -w
#
# Name:     FGC/Spy.pm
# Purpose:  Read data from FGC spy interface
# Author:   Stephen Page

package FGC::Spy;

use bytes;
use Carp;
use Config;
use Exporter;
use strict;

# Use appropriate serial port module for platform

if($^O =~ /Win/) # Platform is Microsoft Windows
{
    require Win32::SerialPort;
}
else # Platform is not Microsoft Windows
{
    require Device::SerialPort;
}

use FGC::Consts;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        FGC_N_SPY_CHANS

                        connect
                        disconnect
                        fiforeset
                        fifostart
                        read
                        readsample
                    );

# Constants

use constant FGC_N_SPY_CHANS    => 6; # Number of channels sent over an FGC's FIFO interface

return(1);


# Begin functions

# Open port for connection to a spy interface

sub connect($)
{
    my ($port_name) = @_;

    # Open serial port

    my $port;
    if($^O =~ /Win/) # Platform is Microsoft Windows
    {
        $port = new Win32::SerialPort('\\\\.\\'.$port_name)
            or die "Unable to open serial port $port_name\n";
    }
    else # Platform is not Microsoft Windows
    {
        $port = new Device::SerialPort($port_name)
            or die "Unable to open serial port $port_name\n";
    }

    # Configure serial port

    $port->user_msg('ON');
    $port->databits(8);
    $port->baudrate(9600);
    $port->parity('none');
    $port->stopbits(1);
    $port->handshake('rts');
    $port->buffers(4096, 4096);
    $port->read_const_time(5000);

    if($^O =~ /Win/) # Platform is Microsoft Windows
    {
        $port->read_interval(0);
    }

    if(!$port->write_settings)
    {
        # Failed to write settings to port

        $port->close();

        die "Unable to configure port\n";
    }

    # Reset FIFO

    fiforeset($port);

    return($port);
}

# Disconnect from spy port

sub disconnect($)
{
    my ($port) = @_;

    fiforeset($port);
    $port->close();
}

# Read spy interface data

sub read($$$)
{
    my ($port, $period, $num_samples) = @_;

    # Reset FIFO

    fiforeset($port);

    # Start FIFO

    fifostart($port, $period);

    # Read the requested number of samples

    my @data;
    for(my $i = 0 ; $i < $num_samples ; $i++)
    {
        my $sample = readsample($port);

        # Append sample to data

        push(@data, $sample);
    }

    # Reset FIFO

    fiforeset($port);

    return(\@data);
}

# Read a sample from spy interface

sub readsample($)
{
    my ($port) = @_;

    # Read data block

    my $buffer;
    defined(($buffer = $port->read(4 + (4 * FGC_N_SPY_CHANS))))
        or die "Read from port failed: $!\n";

    my @sample = unpack("x4N*", $buffer);

    # Convert sample values to floats

    for(my $i = 0 ; $i < FGC_N_SPY_CHANS ; $i++)
    {
        $sample[$i] = unpack("f", pack("L", $sample[$i]));
    }

    return(\@sample);
}

# Reset FIFO

sub fiforeset($)
{
    my ($port) = @_;

    # Reset FIFO

    $port->write(pack("C", 0));
    $port->purge_rx();
}

# Start FIFO

sub fifostart($$)
{
    my ($port, $period) = @_;

    # Start FIFO

    $port->write(pack("C", $period));
}

# EOF
