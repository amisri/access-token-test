#!/usr/bin/perl -w
#
# Filename: FGC/Async.pm
# Purpose:  Asynchronous interface to FGC gateways and devices
# Author:   Stephen Page

package FGC::Async;

require Exporter;

use Carp;
use Exporter;
use FGC::Sync;
use FGC::Consts;
use IO::Select;
use IO::Socket;
use Net::Ping;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(TIMEOUT connect disconnect get init read set);

our $READ_TIMEOUT = 30;
our $CONNECT_TIMEOUT = 30;
our %commands;
our $devices;
our $gateways;
our $selector;
our %sockets;
our $tag        = 0;
our $flush_input= 0;

# Use appropriate socket receive options for platform

my ($recvopts, $recvnbopts);
if($^O =~ /win/i) # Platform is Microsoft Windows
{
    $recvopts   = 0;
    $recvnbopts = 0;
}
else # Platform is not Microsoft Windows
{
    $recvopts   = MSG_WAITALL;
    $recvnbopts = MSG_DONTWAIT;
}

return(1);


# Begin functions

# Initialise module

sub init($$)
{
    ($devices, $gateways) = @_;

    # Create socket selector

    $selector = IO::Select->new();
}

# Check if socket is readable with a timeout

sub can_read($)
{
    my ($select) = @_;

    # Check if we can write to the socket

    my @ready = $select->can_read($CONNECT_TIMEOUT);

    # Timeout

    if (! scalar(@ready))
    {
        die "Server side timeout while waiting for async reply ($CONNECT_TIMEOUT sec)\n";
    }
    else
    {
        return 1;
    }
}

# Check if socket is writable with a timeout

sub can_write($)
{
    my ($select) = @_;

    # Check if we can write to the socket

    my @ready = $select->can_write($CONNECT_TIMEOUT);

    # Timeout

    if (! scalar(@ready))
    {
        die "Timeout waiting to async request ($CONNECT_TIMEOUT sec)\n";
    }
    else
    {
        return 1;
    }

}

# Connect to a gateway

sub connect($)
{
    my ($gateway) = @_;

    my $socket = FGC::Sync::connect($gateway);

    $sockets{$socket} = $gateway;
    
    return $socket;
}

# Disconnect from a gateway

sub disconnect($)
{
    my ($gateway) = @_;

    delete($sockets{$gateway->{socket}}) if defined $gateway->{socket};
    delete($gateway->{buffer});

    FGC::Sync::disconnect($gateway);
}

# Send a get command

sub get($$)
{
    my ($device, $property) = @_;

    # remember name in case the device is a subdevice

    my $device_name = $device->{name};

    # point to parent device if device is a subdevice

    if(defined($device->{device}))
    {
        $device = $devices->{$device->{device}};
    }

    my $gateway = $device->{gateway};

    # Check whether a socket is open to device's gateway

    if(!defined($gateway->{socket}))
    {
        die "ERROR: Not connected to gateway $gateway->{name} for device $device->{name}\n";
    }

    # Check that the socket is still connected

    if (!$gateway->{socket}->connected())
    {
        die "ERROR: Socket to $device not connected\n";
    }

    # Create command hash

    my %command = (
                    device      => $device,
                    property    => $property,
                    tag         => $tag,
                  );

    # Create selector
    
    my $sel = new IO::Select($gateway->{socket});

    # Read remaining data on the TCP input buffer if it is the first rquest
    
    if (!$flush_input)
    {
       my $buffer;
       
       while(scalar($sel->can_read(0)) && $gateway->{socket}->read($buffer,1) > 0) {}
    }
    
    $flush_input = 1;

    # Send command to gateway

    can_write($sel);

    defined(send($gateway->{socket}, "!$tag g $device_name:$property\n", 0))
        or die "Write to socket failed\n";

    $gateway->{commands}->{$tag}    = \%command;
    $commands{$tag}                 = \%command;

    # Add gateway's socket to selector

    $selector->add($gateway->{socket}) if(!$selector->exists($gateway->{socket}));

    # Increment tag

    $tag = ($tag + 1) % 0x80000000;

    return(\%command);
}

# Send a set command

sub set($$$;$)
{
    my ($device, $property, $values, $binary) = @_;
    $binary ||= 0;

    # Check whether $values is an array reference

    if(ref($values) eq "ARRAY")
    {
        # Convert values list to comma delimited string

        $values = join(',', @{$values});
    }

    # remember name in case the device is a subdevice

    my $device_name = $device->{name};

    # point to parent device if device is a subdevice

    if(defined($device->{device}))
    {
        $device = $devices->{$device->{device}};
    }

    my $gateway = $device->{gateway};

    # Check whether a socket is open to device's gateway

    if(!defined($gateway->{socket}))
    {
        die "Not connected to gateway $gateway->{name} for device $device->{name}\n";
    }

    # Check that the socket is still connected

    if (!$gateway->{socket}->connected())
    {
        die "ERROR: Socket to $device not connected\n";
    }

    # Create command hash

    my %command = (
                    device      => $device,
                    property    => $property,
                    values      => $values,
                    tag         => $tag,
                  );

    # Create selector

    my $sel = new IO::Select($gateway->{socket});

    # Read remaining data on the TCP input buffer (in case there was a time-out in the previous request)
    
    if (!$flush_input)
    {
       my $buffer;
       
       while(scalar($sel->can_read(0)) && $gateway->{socket}->read($buffer,1) > 0) {}
    }
    
    $flush_input = 1;
    
    # Send command to gateway 
    
    can_write($sel);

    if($binary) # Set of a binary value
    {
        die "Value is not a reference\n" if(ref($values) ne "SCALAR");

        defined(send($gateway->{socket}, "!$tag s $device_name:$property ".pack("CN", (0xFF, length($$values))).$$values, 0))
            or die "Write to socket failed\n";
    }
    else # Set of an ASCII value
    {
        defined(send($gateway->{socket}, "!$tag s $device_name:$property $values\n", 0))
            or die "Write to socket failed\n";
    }

    $gateway->{commands}->{$tag}    = \%command;
    $commands{$tag}                 = \%command;

    # Add gateway's socket to selector

    $selector->add($gateway->{socket}) if(!$selector->exists($gateway->{socket}));

    # Increment tag

    $tag = ($tag + 1) % 0x80000000;

    return(\%command);
}

# Read responses from gateways

sub read(;$)
{
    my ($user_command) = @_;

    my %responses;
    my $start = time();

    while($selector->count)
    {
        # Handle timeout

        if ( (time() - $start) > $READ_TIMEOUT)
        {
           for my $socket ($selector->handles())
           {
                my $gateway = $sockets{$socket};

                # Return errors to commands

                for my $command (values(%{$gateway->{commands}}))
                {
                    $command->{response}->{binary}  = 0;
                    $command->{response}->{error}   = 1;
                    $command->{response}->{value}   = "Time-out waiting for response";

                    # Add command to responses hash

                    $responses{$command->{tag}} = $command;

                    delete($commands{$command->{tag}});
                    delete($gateway->{commands}->{$command->{tag}});
                }

                # Remove socket from selector set

                $selector->remove($socket);

                # Clear gateway's buffer

                delete($gateway->{buffer});
            }

            # Go out of the selector read loop

            last;
        }

        # Handle readable sockets

        my @readable = $selector->can_read(5);
        for my $socket (@readable)
        {
            my $gateway = $sockets{$socket};

            # Read from socket

            my $buffer;
            if(defined(recv($socket, $buffer, 16384, $recvnbopts)) && length($buffer))
            {
                $gateway->{buffer} .= $buffer;
                process_responses($gateway, \%responses);
            }
            else # An error occurred
            {
                # Return errors to commands

                for my $command (values(%{$gateway->{commands}}))
                {
                    $command->{response}->{binary}  = 0;
                    $command->{response}->{error}   = 1;
                    $command->{response}->{value}   = "Error communicating with gateway";

                    # Add command to responses hash

                    $responses{$command->{tag}} = $command;

                    delete($commands{$command->{tag}});
                    delete($gateway->{commands}->{$command->{tag}});
                }

                # Remove socket from selector set

                $selector->remove($socket);
            }
        }

    }
    
    # Request to flush the next input
    
    $flush_input = 0;
    
    return(\%responses);
}

# Process responses within gateway's buffer

sub process_responses($$)
{
    my ($gateway, $responses) = @_;

    my $buffer = \$gateway->{buffer};

    while(length($$buffer))
    {
        # Extract response from buffer

        my $binary;
        my $command;
        my $error;
        my $response_tag;
        if($$buffer =~ /^\$(\S+) (.)\012(.)/s)
        {
            $response_tag   = $1;
            $error          = $2 eq "." ? 0 : 1;
            $binary         = ord($3) == 0xFF ? 1 : 0;

            $command        = $commands{$response_tag};

            $command->{response}->{binary}  = $binary;
            $command->{response}->{error}   = $error;
        }
        else # No complete response in buffer
        {
            return;
        }

        # Handle response

        if($binary) # Response is binary
        {
            if($$buffer =~ /^\$\S+ .\012.(....)/s) # Buffer includes binary length
            {
                my $length = unpack("N", $1);

                # Do nothing if buffer is less than full length of the response

                return if(length($$buffer) < 9 + length($response_tag) + $length + 2);

                $command->{response}->{value} = substr($$buffer, 9 + length($response_tag), $length);

                # Remove command from buffer

                $$buffer = substr($$buffer, 9 + length($response_tag) + $length + 2);
            }
            else # Buffer does not include complete response
            {
                return;
            }
        }
        else # Response is ASCII
        {
            if($$buffer =~ s/^\$\S+ .\012(.*?)\012;//s) # Buffer includes complete response
            {
                $command->{response}->{value} = $1;
            }
            else # Buffer does not include complete response
            {
                return;
            }
        }

        # Remove gateway socket from selector if no outstanding commands

        delete($commands{$response_tag});
        delete($gateway->{commands}->{$response_tag});
        if(!keys(%{$gateway->{commands}}))
        {
            $selector->remove($gateway->{socket});
        }

        # Add command to responses hash if tag was known

        if(defined($command->{tag}))
        {
            $responses->{$response_tag} = $command;
        }
        else
        {
            warn "Response received for unknown tag $response_tag\n";
        }
    }

    return($responses);
}

# EOF
