#!/usr/bin/perl -w
# 
# Name: RegFGC3.pm
# Purpose: Functions for RegFGC3 systems


package FGC::RegFGC3;

use FGC::RegFGC3_parameters;
use Test::TCP;
use Exporter;
use Carp;
use strict;

our @ISA       = qw(Exporter);
our @EXPORT    = qw(get_card_numbering set_card_numbering get_param_value set_param_value);

my $rbac; 


return(1);

#
# Return system type from device name
#
sub system_type_from_device($)
{
    my ($device) = @_;

    $device =~ /^([^.]*)/; 
    my $system_type = $1;

    return ($system_type);
}

#
# Return the crate of given type from RegFGC3 definition
#
sub get_crate_definition($)
{
    my ($system_type) = @_;

    return ($regfgc3_converters{$system_type});
}

#
# Return the SCIVS slot of the board from RegFGC3 definition
# 
sub get_board_slot($$)
{
    my ($device, $board) = @_;
   
    
    my $system_type = system_type_from_device($device);
    
	if ( !defined($regfgc3_converters{$system_type}) )
	{
		die "ERROR: System type $system_type is not defined in regfgc3_converters\n";
	}
	
	my %system_type_slots = %{ ${$regfgc3_converters{$system_type}}{slots} };
	while ( my ($key, $value) = each(%system_type_slots) )
	{
		if ($value->{board} =~ m/$board/)
		{
			return $key;
		}
	}

	die "ERROR: Board $board not present in system $system_type \n";
}

#
# Return the board and instance (dsp or fpga) where this parameter exists
#
sub locate_param_on_board($)
{
    my ($param_name) = @_;

    # TODO build a hash of {parameter_names => {board => , instance => }}

    for my $board_k (keys (%regfgc3_parameters))
    {
        for my $variant_k (keys (%{$regfgc3_parameters{$board_k}}))
        {
            for my $instance ("dsp", "fpga")
            {
                for my $api_version_k (keys (%{$regfgc3_parameters{$board_k}->{$variant_k}->{$instance}})) 
                {
                    if (exists($regfgc3_parameters{$board_k}->{$variant_k}->{$instance}->{$api_version_k}->{$param_name}))
                    {
                        # Note: Parameter names are unique among RegFGC3 boards
                        
                        return ($board_k, $instance);
                    }
                }
            }
        }
    }
    die "Parameter $param_name does not exist\n";
}

#
# Return the parameter definition 
# Communication with FGC3 is performed to know the detected board variant/version on the device
#
sub get_param_definition($$)
{
    my ($device, $param_name) = @_;

    # Locate the parameter 
    my ($board_name, $instance) = locate_param_on_board($param_name);

    # Get the parameter definition for the version of the board detected
    my ($variant, $version, $revision) = get_card_numbering($device, $board_name, $instance);
    if (!defined($regfgc3_parameters{$board_name}->{$variant}->{$instance}->{$version}))
    {
        die "ERROR: Installed version $version is not defined for the $instance on $board_name variant $variant\n";
    }
    elsif (!defined($regfgc3_parameters{$board_name}->{$variant}->{$instance}->{$version}->{$param_name}))
    {
        die "ERROR: Parameter $param_name is not defined for the $instance on $board_name variant $variant version $version\n";
    }
    my $param_def = $regfgc3_parameters{$board_name}->{$variant}->{$instance}->{$version}->{$param_name};

    return ($param_def);
}

#
# Convert raw value into human readable value
#
sub param_value_to_human_readable($$)
{
    my ($param_def, $raw_param) = @_;

    if (defined($param_def->{symlist}))
    {
        # Symbol list
        
        my ($symbol) = grep ($param_def->{symbols}->{$_}->{value} == $raw_param, keys %{$param_def->{symbols}});

        return $symbol;
    }
    else
    {
        # Apply scaling

        return $raw_param * $param_def->{scaling_gain} + $param_def->{scaling_offset};
    }
}

#
# Convert human readable value into raw value
#
sub param_value_to_raw($$)
{
    my ($param_def, $param_value) = @_;

    if (defined($param_def->{symlist}))
    {
        # Symbol list
        
        # TODO accept scalar?

        if (!defined($param_def->{symbols}->{$param_value}))
        {
            die "ERROR: $param_value is not a valid symbol for $param_def->{symlist}\n";
        }

        return $param_def->{symbols}->{$param_value}->{value};
    }
    else
    {
        # Apply scaling
        # TODO check limits
        # TODO floor floats
        # TODO check $param_value is a scalar

        return ($param_value - $param_def->{scaling_offset}) / $param_def->{scaling_gain};
    }
}

#
# Return detected board numbering on a given device
#
sub get_card_numbering($$$)
{
    my ($device, $board, $instance) = @_;

    my $slot = get_board_slot($device, $board);

    my $variant = get($device, "REGFGC3.SLOT_INFO.DETECTED.".uc($instance)."_VARIANT[$slot]", 0, $rbac);
    my $version = get($device, "REGFGC3.SLOT_INFO.DETECTED.".uc($instance)."_VERSION[$slot]", 0, $rbac);
    my $revision = get($device, "REGFGC3.SLOT_INFO.DETECTED.".uc($instance)."_REV[$slot]", 0, $rbac);

    return ($variant, $version, $revision);
}

#
# Set the version and revision on given device
#
sub set_card_numbering($$$$$;$)
{
    my ($device, $board, $instance, $version, $revision, $rbac_method) = @_;

    my $slot = get_board_slot($device, $board);

    set($device, "REGFGC3.SLOT_INFO.EXPECTED.".uc($instance)."_VERSION[$slot]", $version, 0, $rbac_method);
    set($device, "REGFGC3.SLOT_INFO.EXPECTED.".uc($instance)."_REV[$slot]", $revision, 0, $rbac_method);
}

#
# Get the raw value of a parameter on a given device
#
sub get_param_raw_value($$$)
{
    my ($device, $param_def, $slot, $raw_value) = @_;

    # Get the raw parameter value from the FGC3
    # TODO should use eventually params interface, appending checksum, instead of RAW interface

    set($device, "REGFGC3.RAW.SLOT", $slot, 0, $rbac);
    set($device, "REGFGC3.RAW.ACCESS", $param_def->{access_rights}, 0, $rbac);
    set($device, "REGFGC3.RAW.BLOCK", $param_def->{block}, 0, $rbac);

    my $param_property= $param_def->{instance} eq "fpga" ? 'PARAM' : 'PARAM_DSP'; 
    my $raw_param = get($device, "REGFGC3.RAW.${param_property}\[$param_def->{index}\]",0,  $rbac);

    return ($raw_param);
}

#
# Get the value of a parameter on a given device
#
sub get_param_value($$;$)
{
    my ($device, $param_name, $rbac_method) = @_;

    $rbac = $rbac_method;

    my $param_def = get_param_definition($device, $param_name);
    my $slot = get_board_slot($device, $param_def->{board_name});

    my ($param_raw_value) = get_param_raw_value($device, $param_def, $slot);

    my $param_value = param_value_to_human_readable($param_def, $param_raw_value);

    return ($param_value, $param_raw_value, $param_def->{unit});
}

#
# Set the raw value of a parameter on a given device
#
sub set_param_raw_value($$$$)
{
    my ($device, $param_def, $slot, $raw_value) = @_;

    # Set the raw parameter value from the FGC3
    # TODO should use eventually params interface, appending checksum, instead of RAW interface

    set($device, "REGFGC3.RAW.SLOT", $slot, 0, $rbac);
    set($device, "REGFGC3.RAW.ACCESS", $param_def->{access_rights}, 0, $rbac);
    set($device, "REGFGC3.RAW.BLOCK", $param_def->{block}, 0, $rbac);

    my $param_property= $param_def->{instance} eq "fpga" ? 'PARAM' : 'PARAM_DSP'; 
    set($device, "REGFGC3.RAW.${param_property}\[$param_def->{index}\]", $raw_value,  $rbac);
}

#
# Set the value of a parameter on a given device
#
sub set_param_value($$$;$)
{
    my ($device, $param_name, $value, $rbac_method) = @_;

    $rbac = $rbac_method; 

    my $param_def = get_param_definition($device, $param_name);
    my $slot = get_board_slot($device, $param_def->{board_name});

    my $raw_value = param_value_to_raw($param_def, $value);

    set_param_raw_value($device, $param_def, $slot, $raw_value);
}

# EOF
