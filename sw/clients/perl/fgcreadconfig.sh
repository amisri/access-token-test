#!/bin/sh

# PERL5LIB environment

export PERL5LIB=/user/pclhc/lib/perl:$PERL5LIB

cd /user/pclhc/etc/config

# Maintain two days of old configurations

rm -rf devices.day-2
mv     devices.day-1 devices.day-2 2>/dev/null
cp -r  devices/      devices.day-1 2>/dev/null
mkdir -p devices
cd devices

# Read configurations

/user/pclhc/bin/perl/fgcreadconfig.pl $@

# Create a tarred copy of today's configurations

cd ..
tar czf archive/`date +%d_%m_%Y`.tar.gz ./devices

# Keep all the last month files from the archive, and then keep the one corresponding to the 1st of each month 

cd archive

old_files=`find . -mtime +30`
for fn in $old_files
do
	fn_date=`date +%d -r $fn`
	if [ $fn_date -ne 1 ]; then
	    rm $fn
	fi
done

# EOF
