#!/usr/bin/perl -w
#
# Name:     fgcreadconfig.pl
# Purpose:  Read FGC configurations to files
# Author:   Stephen Page

use FGC::Async;
use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use File::Basename;
use IO::Socket;
use Mail::Send;
use strict;

# Create report to send by e-mail in case of failure

my $email_report = '';

# Get a configuration from selected power converter

sub write_config($$)
{
    my ($device, $response) = @_;

    print "Writing config for device $device->{name}...\n";

    # Open config file

    my $filename = "$device->{name}.txt";

    if(!open(CONFIG_FILE, ">", $filename))
    {
        warn                "Failed to open file $filename: $!";
        $email_report .=    "Failed to open file $filename: $!";
        return;
    }

    # Replace colons with spaces so that lines are suitable for set command

    $response->{value} =~ s/:/ /og;

    # Handle each line of the config

    for(split("\n", $response->{value}))
    {
        next if(/^CAL./); # Exclude calibration properties

        s/,//g if(/^DEVICE.NAME /); # Remove commas from DEVICE.NAME property
        s/,//g if(/^SYSTEM.NAME /); # Remove commas from SYSTEM.NAME property # Only affects FGC1s

        # EPCCCS-8916: split the response into property and value part, allowing the response to have spaces as well.
        my @parts = split(/\s+/, $_);
        my $property = shift(@parts);
        my $value = join(" ", @parts);

        if(defined($value) && length($value))
        {
            printf CONFIG_FILE ("! S %-30s %s\015\012", $property, $value);
        }
    }
    close(CONFIG_FILE);
}

# End of functions


# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Initialise FGC asynchronous communication module

FGC::Async::init($devices, $gateways);

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate_by_location()
    or die "RBAC authentication failed\n";

# Connect to all gateways

for my $gateway (values(%$gateways))
{
    eval { FGC::Async::connect($gateway) };
    if($@)
    {
        warn                "Unable to connect to gateway $gateway->{name}: $@\n";

        # Remote gateway from set to be processed

        FGC::Async::disconnect($gateway);
        delete($gateways->{$gateway->{name}});
        next;
    }

    # Set RBAC token on gateway

    eval { FGC::Async::set($gateway->{channels}->[0], "CLIENT.TOKEN", \$rbac_token, 1) };
    if($@)
    {
        warn                "Failed to send command to set RBAC token on gateway $gateway->{name}\n";
        $email_report .=    "Failed to send command to set RBAC token on gateway $gateway->{name}\n";

        # Remote gateway from set to be processed

        FGC::Async::disconnect($gateway);
        delete($gateways->{$gateway->{name}});
        next;
    }
}

# Read responses to RBAC token set commands

my $commands = FGC::Async::read();
for my $command (values(%$commands))
{
    if($command->{response}->{error})
    {
        warn                "Error setting RBAC token on gateway $command->{device}->{name}: $command->{response}->{value}\n";
        $email_report .=    "Error setting RBAC token on gateway $command->{device}->{name}: $command->{response}->{value}\n";

        # Remote gateway from set to be processed

        my $gateway = $command->{device}->{gateway};
        delete($gateways->{$gateway->{name}});
        FGC::Async::disconnect($gateway);
        next;
    }
}

# Send command to all devices for connected gateways

for my $gateway (values(%$gateways))
{
    # Read configuration from each connected device

    for my $device (@{$gateway->{channels}})
    {
        # Skip unused channels and gateway device

        next if(!defined($device->{name}) || $device->{channel} == 0);

        # Send command to device

        eval { FGC::Async::get($device, 'CONFIG.SET') };

        # Check whether an error occurred

        if($@) # An error occurred
        {
            warn                "Error getting configuration from $device->{name}: $@\n";
            $email_report .=    "Error getting configuration from $device->{name}: $@\n";
            next;
        }
    }
}

# Read responses

$commands = FGC::Async::read();

# Disconnect from all gateways

FGC::Async::disconnect($_) for values(%$gateways);

for my $command (values(%$commands))
{
    # Check whether an error occurred

    if($command->{response}->{error})
    {
        next;
    }
    else
    {
        write_config($command->{device}, $command->{response});
    }
}

# Send report by e-mail if it has content

if(length($email_report)) # Report has content
{
    my $email       = new Mail::Send(To         => 'converter-controls-devices@cern.ch',
                                     Subject    => basename($0)." report");
    my $email_fh    = $email->open;
    print $email_fh $email_report;
    $email_fh->close;
}

# EOF
