#!/usr/bin/perl
#
# Name:    spy_acquire.pl
# Purpose: Acquire data from the FGC Spy interface
# Author:  Stephen Page and Quentin King

use FGC::Spy;
use strict;
use warnings;

# Process command line arguments

die "Usage: $0 <port> <number seconds> [SPY.MPX[5] [SPY.MPX[4] ... [SPY.MPX[0]]]\n" if(@ARGV < 2);

my $port_name   = $ARGV[0];
my $num_seconds = 1;
my @sig_name    = qw/I_REF I_MEAS V_REF V_MEAS I_A I_B/;

if(@ARGV > 1)
{
    $num_seconds = $ARGV[1];

    my $num_sig_names = @ARGV - 2;

    for(my $i = 0 ; $i < $num_sig_names ; $i++)
    {
        $sig_name[5-$i] = $ARGV[$i+2];
    }
}

# Connect to Spy interface

my $port = FGC::Spy::connect($port_name)
    or die "Unable to connect to FGC: $!\n";

# Read from Spy interface

my $num_samples = $num_seconds * 1000;

my $data = FGC::Spy::read($port, 1, $num_samples);

# Disconnect from Spy interface

FGC::Spy::disconnect($port);

# Print header

print "TIME";

for(my $i = 0 ; $i < FGC::Spy::FGC_N_SPY_CHANS ; $i++)
{
    print ",$sig_name[$i]";

    if($sig_name[$i] =~ /REF/i || $sig_name[$i] =~ /ERR/i)
    {
        print "  step";
    }
}

print "\n";

# Print samples

my $time = time;

for my $sample (@$data)
{
    printf("%.03f,", $time);
    for my $field (@$sample)
    {
        printf("%.7e,", $field);
    }
    print "\n";

    # Add 1ms to $time

    $time += 0.001;
}

# EOF
