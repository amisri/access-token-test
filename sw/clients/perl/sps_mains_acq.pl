#!/usr/bin/perl -w

use strict;

sub get_log
{
    my $output;
    my $cmd;
    
    # Build command string

    $cmd = "/usr/bin/unbuffer /acc/local/Linux/bin/rda-get -r location -d \"RPPEL.BA3.MBI,RPPAG.BA3.QD,RPPAG.BA3.QF\" -p \"LOG.I.MEAS,LOG.I.REF\"";
    $output = `$cmd`;
    if ($?)
    {
        # Replace new line characters by space to have ERROR message in one line which is easier to grep

        $output =~ s/\r|\n/ /g;
        warn "ERROR: $cmd: $output";
    }
    
    # Parse output
    
    my $device;
    my $property;
    my $acqStamp;
    my $value;
    
    foreach my $line (split(/\n/,$output))
    {
        chomp($line);

        # Parse property-value pairs

        if ( $line =~ /Sync\sGET\sdata:\s*([^\s\/]+)\/([^\s\/]+)/ )
        {
            $device = $1;
            $property = $2;
        }
        elsif  (defined($device) && defined($property) && $line =~ /acqStamp=(\d+)/)
        {
            $acqStamp = $1/1000000000.0; 
        }
        elsif ( defined($device) && defined($property) && defined($acqStamp) && $line =~ /Value:\s+(.*)$/ )
        {
            if (!$1)
            {
                warn("WARNING: No data available at $device/$property at $acqStamp sec\n");
                next;
            }
            
            for my $value (split(/\s/,$1))
            {
                  
               print "$device,$property,$acqStamp,$value\n";
               
               # Acquisitions separated one second
               
               $acqStamp += 0.001;
            } 
            $device=undef;
            $property=undef;
            $acqStamp = undef;
            
        }
    }
    
    close($output);
}

print "device,property,acqStamp [sec],value [A]\n";

while(1)
{
    get_log();
    sleep(0.5);
}

# Use this line for a specific cycle
#subscribe();
