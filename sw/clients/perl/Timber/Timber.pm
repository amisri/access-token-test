package Timber;

use strict;
use warnings;

use Data::Dumper;
use File::Basename;
#use File::Temp qw(tempfile);
use Carp;

my $timber_jar_src = '/user/pcrops/dist/accsoft/cals/accsoft-cals-extr-client/PRO/build/dist/accsoft-cals-extr-client-nodep.jar';
my $timber_config_file = dirname(__FILE__)."/configuration.properties";

sub load_timber_file($)
{
	my($csv_filename) = @_;
    my @variables;
    my @cycle_timestamps;
    my @cycle_data;

    open(my $fh, $csv_filename) or die('could not open file $csv_filename');

    my $varname = 'unknown';
    
    while( my $line = <$fh>)  
    {
        if( $line =~ /^VARIABLE/ )
        {
            $varname = substr($line, 10,-1);
        }
        elsif ( $line =~ /^[0-9]{4}/ )
        {
            my @timber_data = split(',',$line);
            my $timestamp = $timber_data[0];
            
			my @data;
            for my $i (1 .. $#timber_data)
            {
                push( @data, $timber_data[$i]);
            }
            
            push(@variables, $varname);
            push(@cycle_timestamps, $timestamp);
            push(@cycle_data, \@data);
        }
    }
    close($fh);
    return (\@variables, \@cycle_timestamps, \@cycle_data);
}

sub download_from_timber($$$$)
{
    my ($var_name, $date_init, $date_end, $output_file, $debug) = @_; 
 
    my $cmd = "java -jar $timber_jar_src -vs $var_name -t1 \"$date_init\" -t2 \"$date_end\"  -C $timber_config_file -M DS -N /$output_file -F CSV";
    
    if (defined($debug))
    {
        print "$cmd\n";
    }

    my $output = `$cmd 2>&1` || '';
    
    if($? != 0)
    {
        print STDERR $output;
        return(1);
    }
    else
    {
        return(0);
    }
}

1;

# Example of how to download data from timber
# read 1h worth of data at a time, to not overload the timber server
unless (caller) 
{

    my $init_date = '2015-11-10 00:00:00';
    my $end_date  = '2015-11-10 01:00:00';
    my $tmp_file  = 'timber.tmp.csv';

    (download_from_timber('RPPAG.BA3.SQF:I_MEAS', $init_date, $end_date, $tmp_file)==0) or die "ERROR: Could not download from Timber";
    
    my ($variables, $timestamps, $data) = load_timber_file($tmp_file);
       
    for my $i ( 0 .. scalar(@$variables) -1)
    {
    	my $var = @$variables[$i];
    	my $time = @$timestamps[$i];
    	my $cycle_data = @$data[$i];
    	
    	for my $x ( 0 .. scalar( @$cycle_data )-1)
    	{
    		print ("$var\t$time\t@$cycle_data[$x]\n");
    	}
    }
    
	unlink($tmp_file);
}
