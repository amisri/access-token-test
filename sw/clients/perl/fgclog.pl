#!/usr/bin/perl -w
#
# Name:     fgclog.pl
# Purpose:  Log published data from devices
# Author:   Stephen Page

use FGC::Async;
use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sub;
use FGC::Sync;
use Socket;
use strict;

# Flush stdout immediately

$| = 1;

my $devices;
my $gateways;
my %gateways_by_ip;
my %sub_devices;

# Write a row of data for a timestamp

sub write_time_data($$)
{
    my ($timestamp, $time_data) = @_;

    print "$timestamp,";

    # Write signals for each device

    for my $device (sort { $a->{name} cmp $b->{name} } (values(%sub_devices)))
    {
        my $data = $time_data->{$device->{name}};

        # Write each signal

        for my $key (@{$device->{sorted_signals}})
        {
            my $value = defined($data) ? $data->{$key} : undef;

            # Check whether data is defined

            if(defined($value))
            {
                if(ref($value) ne "HASH" && $value !~ /^[a-zA-Z]/) # Value is not a symlist or an enumeration
                {
                    # Value must be numeric

                    if($value =~ /^[\-0-9]+$/) # Value is an integer
                    {
                        print $value;
                    }
                    else # Value is a float
                    {
                        printf("%.7e", $value);
                    }
                }
            }
            print ",";
        }
    }
    print "\n";

    # Clear time_data

    %$time_data = ();
}

# End of functions


# Print usage if incorrect number of arguments

if(@ARGV < 4)
{
    die "Usage: $0 <RBAC token file> <period (".FGC_FIELDBUS_CYCLE_PERIOD_MS."ms ticks)> <duration in seconds><device 1> ... <device 2>\n";
}
my ($rbac_token_file, $period, $duration, @sub_devices) = @ARGV;

# Read RBAC token

open(FILE, '<', $rbac_token_file) or die "Failed to open RBAC token file: $!\n";
read(FILE, my $rbac_token, 65536) or die "Failed to read RBAC token: $!\n";
close(FILE);

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

for my $name (@sub_devices)
{
    my $device = $devices->{$name};
    die "Unknown device $name\n" if(!defined($device));

    $sub_devices{$name} = $device;
}

# Create UDP socket

my $udpsock;
socket($udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Bind to port

my $port;
for($port = 1024 ; $port <= 65535 ; $port++)
{
    # Create address structure

    bind($udpsock, sockaddr_in($port, INADDR_ANY)) and last;
}
die "Unable to bind to UDP port\n" if($port > 65535);

# Write information to stderr

warn    "Logging data from the following devices:\n\n".
        join("\n", sort(keys(%sub_devices)))."\n\n";

# Get list of gateways to connect

my %gateways_to_connect = map { $_->{gateway}->{name} => $gateways->{$_->{gateway}->{name}} } values(%sub_devices);

# Connect to gateways

for my $gateway (values(%gateways_to_connect))
{
    (my $tcpsock = FGC::Async::connect($gateway)) >= 0
        or die "Unable to connect to gateway $gateway->{name} : $!\n";

    $gateways_by_ip{$gateway->{ip}} = $gateway;

    # Set RBAC token on gateway

    FGC::Sync::set($tcpsock, "", "CLIENT.TOKEN", \$rbac_token, 1);

    # Set up subscription

    FGC::Sync::set($tcpsock, "", "CLIENT.UDP.SUB.PERIOD",   $period);
    FGC::Sync::set($tcpsock, "", "CLIENT.UDP.SUB.PORT",     $port);

    $gateway->{expected_sequence}   = 0;
    $gateway->{sequence_errors}     = 0;
}

# Write headings

print "TIME,";
for my $device (sort { $a->{name} cmp $b->{name} } values(%sub_devices))
{
    $device->{sorted_signals} = eval("FGC::Class::$device->{class}::Auto::numeric_fields")
        or die "Unknown signals for device $device->{name}\n";

    for my $signal (@{$device->{sorted_signals}})
    {
        print "$device->{name}:$signal,";
    }
}
print "\n";

# Calculate end time

my $start_time  = time;
my $end_time    = $start_time + $duration;

warn    "Period = ".(abs($period * FGC_FIELDBUS_CYCLE_PERIOD_MS))."ms\n".
        "Start  = ".scalar(localtime($start_time))."\n".
        "End    = ".scalar(localtime($end_time))."\n\n";

# Receive data

my $current_timestamp   = $start_time + 0.0;
my %time_data;
while(my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
{
    next if(!defined($sockaddr_in)); # Error receiving packet

    my $gateway = $gateways_by_ip{inet_ntoa((sockaddr_in($sockaddr_in))[1])};
    warn "Data received from unexpected host\n", next if(!defined($gateway));

    # Stop if end time passed

    last if($time_sec > $end_time);

    # Write sequence number to indicate activity

    print STDERR "$sequence $current_timestamp\r";

    # Check whether sequence number matches expected

    if($sequence != $gateway->{expected_sequence})
    {
        $gateway->{sequence_errors}++;
    }
    $gateway->{expected_sequence} = ($sequence + 1);

    # Handle each channel

    for(my $i = 0 ; $i < FGC_MAX_DEVS_PER_GW ; $i++)
    {
        my $device  = $gateway->{channels}->[$i];
        my $data    = $status->[$i];

        next if(!defined($device) || !defined($sub_devices{$device->{name}}));
        next if(!$data->{DATA_STATUS}->{DATA_VALID} || !$data->{DATA_STATUS}->{CLASS_VALID});

        $time_data{$device->{name}} = $data;
    }

    # Check whether data for all subscribed devices has been received

    if(scalar(keys(%time_data)) == scalar(keys(%sub_devices)))
    {
        write_time_data($current_timestamp, \%time_data);
    }

    # Fake timestamp to avoid problems with packet re-ordering

    $current_timestamp += (abs($period) * FGC_FIELDBUS_CYCLE_PERIOD_MS / 1000.0);
}

close($udpsock);
FGC::Async::disconnect($_) for values(%gateways_to_connect);

print "\n";

my $number_of_packets = $duration/($period * FGC_FIELDBUS_CYCLE_PERIOD_MS / 1000.0);

for my $gateway (values(%gateways_to_connect))
{
    if(exists($gateway->{sequence_errors}) && $gateway->{sequence_errors} != 0)
    {
        warn "WARNING: $gateway->{sequence_errors}/$number_of_packets sequence errors on $gateway->{name}\n;"
    }
}

print STDERR "\n\nLogging completed\n";

# EOF
