#!/bin/sh

cpu=$(uname -m)
os=$(uname -s)

export PATH="/user/pclhc/bin/$os/$cpu:/user/pclhc/bin/$os:/user/pclhc/bin/perl:$PATH"
export PERL5LIB="/user/pclhc/lib/perl"

r2eapp.pl

# EOF
