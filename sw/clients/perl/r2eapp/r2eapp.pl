#!/usr/bin/perl

# Ctrl + C to quit
# Ctrl + \ to modify values

use Test::Utils;
use Term::ReadLine;
use Switch;
use Time::HiRes qw( gettimeofday tv_interval);
use POSIX;

# ***********************************************************
# For debug
my $skip_init = 0;
# ***********************************************************
#
# ***********************************************************
# Constants
my $DIG_IPDIRECT_ADDR = "0x04080404,1"; 				# Direct Access to STATUS 12 input via the MCU adress
my $DIG_IP1_ADDR = "0x04080402,1";					# Access VSRUN readback via the MCU adress
my $DIG_OP_SET_ADDR = "0x04080401,1";					# Access the FGC operating mode

my $STATUS12_BitMask = 0x1000;						# Input STATUS 12 state
my $VSRUN_rb_BitMask = 0x2000;						# VSRUN State

my $REF_BitMaskSet = 0x20;						# Set The Control Card in the Voltage REFERENCE configuration via the CMD5 of the FGC
my $REG_BitMaskSet = 0x2000;						# Set The Control Card in the Voltage REGULATION configuration via the CMD5 of the FGC

my $OB_BitMaskSet = 0x400;						# Enable the Output
my $OB_BitMaskClear = 0x4;						# Blocking the Output ia the Output Block command

my $RELAY_BitMaskSet = 0x40;						# Set the Relay command ON via the CMD6 of the FGC
my $RELAY_BitMaskClear = 0x4000;					# Set the Relay command OFF via the CMD6 of the FGC

my $VSRUN_BitMaskSet = 0x1;						# Run Output Voltage VS
my $VSRUN_BitMaskClear = 0x100;						# Stop Output Voltage VS

my $VSRESET_BitMaskSet = 0x100;						# Reset signal(20ms pulse) clear the faults

my $FORWARD_POLLING_Delay_ms = 50;					# Duration of the Beam forward (TTL) signal Pulse send on the FGC input Status 12 in order to start the acquisition process
my $FORWARD_POLLING_Delay_s = $FORWARD_POLLING_Delay_ms/1000; 		# Converting the duration from ms to s

my $FORWARD_to_ACQ_Delay_ms = 550;					# Delay time after receive the input Status 12 to start the acquisition
my $FORWARD_to_ACQ_Delay_s = $FORWARD_to_ACQ_Delay_ms/1000;		# Converting the duration from ms to s

my $ACQ_PERIOD_ms = 750;						# Acquisition time duration
my $ACQ_PERIOD_s = $ACQ_PERIOD_ms/1000;					# Converting the duration from ms to s

my $GAIN_VREF_IREG = 5;							# Gain factor for DAC 2 (SPARE_DAC+/-) value

# End Constants
# ***********************************************************




# ***********************************************************
# Program start point

# Clear screen
print"\033[2J";
print"\033[0;0H";

#display Header
print "##########################################################\n";
print "##\t\t\t\t\t\t\t##\n";
print "##\t\t\tR2E Tester\t\t\t##\n";
print "##\t\t\t\t\t\t\t##\n";
print "##########################################################\n";

# Set R2E device
my %params;
%params = parse_opts %params;

# Connection and authentification
authenticate($params{authenticate});
print "RBAC token acquired\n\n";

# Open terminal and requets device connection

my $prompt = new Term::ReadLine "Default Input";
$params{device} = ($_=$prompt->readline("Enter Device ID (default \'RRTOE.157.1\'): ")) eq "" ? "RRTOE.157.1" : $_;


my $this_FGC = $params{device};

# ***********************************************************
# Initializing and Applying all the parameters settled to the FGC selected
set($this_FGC, "FGC.DEBUG.MEM.MCU", "$DIG_IPDIRECT_ADDR"); 	#set the FGC to read the input channel STATUS 12

if($skip_init != 1)						
{
	reset_FGC($this_FGC);
	initialise_FGC($this_FGC);
	my $ref_reg = set_REF_REG($this_FGC);				
	my $head_relay = set_Head_relay($this_FGC);
	enable_DAC_modification($this_FGC);
	my $dac1_value = set_DAC1($this_FGC);
	my $dac2_value = set_DAC2($this_FGC);
	output_block($this_FGC,'UNBLOCKING');
	vs_run($this_FGC,1);
}
else # Defaults values for the parameters
{
	my $ref_reg = "REF";
	my $head_relay = "OFF";
	my $dac1_value = 0;
	my $dac2_value = 0;	
}

# Acquire the operating mode of the FGC
my $op_mode = get_mode($this_FGC);

# Set the Type of Radiation Testing
print "\nSet Radition Testing Type: ";
chomp(my $Radiation_testing_type = <>);

# Create file and header
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime();
my $year = $year + 1900;
$mon = $mon + 1;
my $filename = sprintf("%4d%02d%02d_%02d-%02d-%02d_%s_%s.csv",$year,$mon,$mday,$hour,$min,$sec,$op_mode,$Radiation_testing_type);
open (my $fileHandler, ">", $filename) or die $!; 
print $fileHandler "Beam frame,,Timestamp,,SE_A1,SE_A2,SE_A3,SE_A4,SE_A5,SE_B1,SE_B2,SE_B3,SE_B4,SE_B5,SE_C1,SE_C2,SE_C3,SE_C4,SE_C5,,LV_NOT/HV,N_NOT/P,HEAD_CONNECTED_NOT,FGC_PSU_FAULT,HEAD_PSU_FAULT,AUX_PSU_FAULT,AUX_15V_FAULT,,A1 m1/HEAD_T,A2 m1,A3 m1,A4 m1,A5 m1,AB1 m2,AB2 m2,AB3 m2,AB4 m2,AB5 m2,HEAD_T,,I-,I+,VCC+,VCC-,,REG/REF,Head Relay,DAC1,DAC2\n";
close $fileHandler;

my $acquisition_counter = 0;
my $modify_parameters = 0;
print "\n***\tStart main loop\t***\n";
# ***********************************************************
# Main loop
while(1)
{
	if ( $modify_parameters == 1 ) 					# Flag set by signal reception Ctrl + \ to modify the parameters values
	{
		print "\n\n\n\t\t##  MODIFY ENVIRONMENT  ##\n";
		vs_run($this_FGC,0);					# Stop the Output		
		output_block($this_FGC,'BLOCKING');			# Blocking the Output
		$ref_reg = set_REF_REG($this_FGC);			# Set The Control Card in the Voltage REFERENCE or REGULATION configuration via the CMD5 of the FGC
		$head_relay = set_Head_relay($this_FGC);		# Set the Relay command ON of OFF via the CMD6 of the FGC
		$dac1_value = set_DAC1($this_FGC);			# Set DAC1 value
		$dac2_value = set_DAC2($this_FGC);			# Set DAC2 value
		output_block($this_FGC,'UNBLOCKING');			# Enable the Output
		vs_run($this_FGC,1);					# Run the Output	
		print "\n\n\n\t\t## RESTART ACQUISITIONS  ##\n\n\n";
		$modify_parameters = 0; # Clear flag
	}
	# check rising edge in Beam forward signal on the input STATUS 12
	my $status12_d = 1;						# Rising Edge detected
	my $status12 = 0;						# Rising Edge not detected
	$status12 = get_status12_Value($this_FGC);
	do
	{
		$SIG{INT} = \&int_signal_handler;			# Ctrl + \ to modify values or Ctrl + C to quit
		$SIG{QUIT} = \&quit_signal_handler;			
		
		Time::HiRes::sleep($FORWARD_POLLING_Delay_s);		# Check the duration of the pulse	 

		$SIG{INT} = 'IGNORE';					# impossible to stop or modify parameters from this point.
		$SIG{QUIT} = 'IGNORE';

		$status12_d = $status12;				
		$status12 = get_status12_Value($this_FGC);
		
	}
	while( ($status12 != 1) || ($status12_d != 0) );

	my $FORWARD_to_ACQ_Delay_ms = $FORWARD_to_ACQ_Delay_s * 1000;	# Time delay before starting the acquisitions
	print "Rising edge detected, wait $FORWARD_to_ACQ_Delay_ms ms\n";
	
	Time::HiRes::sleep($FORWARD_to_ACQ_Delay_s);

	# do some resets to clear the faults
	set($this_FGC, "VS", "RESET");

	print "Start acquisitions\n";
	

	my @dig_acquisitions;
	my @ana_acquisitions;
	my @adc_acquisitions;
	my @timestamp_acquisitions;


	Time::HiRes::sleep($ACQ_PERIOD_s);

	# Acquire digital values
	my $dig_acq = get($this_FGC, "DIAG.DIG");
	push(@dig_acquisitions,$dig_acq);

	# Acquire ana values
	my $ana_acq = get($params{device}, "DIAG.ANA");
	push(@ana_acquisitions,$ana_acq);

	# Acquire ADC values
	my $adc_acq = get($params{device}, "ADC.VOLTS_200ms");
	push(@adc_acquisitions,$adc_acq);

	# Get timestamp
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime();
	$year = $year + 1900;
	$mon = $mon + 1;
	($seconds,$microseconds) = gettimeofday;
	#my $timestamp = sprintf("%4d-%02d-%02d_%02d:%02d:%02d.%04d",$year,$mon,$mday,$hour,$min,$sec,$microseconds);
	my $timestamp = sprintf("%4d-%02d-%02d %02d:%02d:%02d",$year,$mon,$mday,$hour,$min,$sec);		
	push(@timestamp_acquisitions,$timestamp);
	
	

	print "End of acquisition, parsing... ";
	my @parsed_dig_acquisitions = parseDig(@dig_acquisitions);
	my @parsed_ana_acquisitions = parseAna(@ana_acquisitions);
	print "\t\t\t\t[Done]\n";

	print "Adding to file... ";
	# Add to the end of the file
	open (my $fileHandler, ">>", $filename) or die $!;
	my $len = @parsed_dig_acquisitions;
	#print "\n\nacq len : [$len]\n\n";
	for (my $i=0; $i<@parsed_dig_acquisitions; $i++)
	{
		#print "$i [dig] $parsed_dig_acquisitions[$i]\n";
		#print "$i [ana] $parsed_ana_acquisitions[0]\n";
		#print "$i [adc] $adc_acquisitions[0]\n";
		print $fileHandler "$acquisition_counter,,$timestamp_acquisitions[$i],,$parsed_dig_acquisitions[$i],$parsed_ana_acquisitions[0],$adc_acquisitions[0],,$ref_reg,$head_relay,$dac1_value,$dac2_value\n";
		#print $fileHandler "$parsed_dig_acquisitions[$i]\n";
	}

	close $fileHandler;
	undef $parsed_dig_acquisitions;
	undef $parsed_ana_acquisitions;
	undef @dig_acquisitions;
	undef @ana_acquisitions;
	undef @adc_acquisitions;

	$acquisition_counter = $acquisition_counter + 1;

	#Every 100 acquisitions / Head relay switches state
	#if (($acquisition_counter % 100)==0)
	#{
	#	$head_relay = $head_relay eq "ON" ? set_Head_relay_state("OFF") : set_Head_relay_state("ON");
	#}

	print "\t\t\t\t\t[Done]\n\n";

}


exit 0;

# Ctrl + \ to modify values or Ctrl + C to quit
sub int_signal_handler 
{
	print "\n\n\n SIG_INT received\n\n\n";
	exit 1;
}

sub quit_signal_handler
{
	print "\n\n SIGNAL CATCHED\n\n";
	$modify_parameters = 1;
}




# Reset the FGC for this application
# Input: FGC device
# Output: none
sub reset_FGC
{
	my ($device) = @_;

	# Reseting
	print "Reseting ... ";
	set($device, "PC", "OF");
	my $iteration_counter = 0;
	do
	{
		Time::HiRes::sleep(0.5);
		$FGC_state = get($this_FGC, "STATE.PC");
		#print "\nSTATE [$FGC_state]";
		$iteration_counter = $iteration_counter + 1;
	}
	while(($FGC_state ne "OFF") && ($iteration_counter < 100));
	 
	if($FGC_state eq "OFF")
	{
		#Time::HiRes::sleep(5);
		set($device, "VS.FABORT_UNSAFE", "RESET");
		set($device, "VS.RESET", "RESET");
		set($device, "VS.RESET", "");
		set($device, "VS.FW_DIODE", "RESET");
		set($device, "STATUS.ST_LATCHED", "RESET");
		set($device, "DIAG", "RESET");
		print "\t\t\t\t\t\t[Done]\n";
	}
	else
	{
		print "\n\n\t##  Impossible to reset the FGC  ##\n\n";
		exit 1;
	}
}

# Initialise the FGC for this application
# Input: FGC device
# Output: none
sub initialise_FGC
{
	my ($device) = @_;

	# Set the FGC to simulation mode
	print "Set to simulation mode ... ";
	set($device, "MODE.OP", "SIMULATION");
	set($device, "MEAS.SIM", "DISABLED");
	print "\t\t\t\t[Done]\n";
}

# Display menu to set REF or REG mode
# Input: FGC device
# Output: Status (str [REF; REG])
sub set_REF_REG
{
	my ($device) = @_;
	
	# Set REF/REG
	print "\nSet REF/REG value:\n";
	print "\t1. REF\n";
	print "\t2. REG\n[Default: REF] > ";
	do
	{
		chomp($ref_reg = <>);
	}
	while((!($ref_reg =~ /^[1-2]+$/))&&($ref_reg ne "")); 
	switch($ref_reg)
	{
		case "1"	
		{
			$ref_reg = 'REF';
			set($device, "DIG.COMMANDS", "$REF_BitMaskSet");
		}
		case "2"  
		{
			$ref_reg = 'REG';
			set($device, "DIG.COMMANDS", "$REG_BitMaskSet");
		}
		else 
		{
			$ref_reg = 'REF';
			set($device, "DIG.COMMANDS", "$REF_BitMaskSet");
		}
	}
	print "REG/REF value: $ref_reg\n";
	return $ref_reg;
}

# Display menu to set or clear head relay
# Input: FGC device
# Output: Status of head relay (str [ON; OFF])
sub set_Head_relay
{
	my ($device) = @_;
	# Set Head Relay
	print "\nChoose Head Relay status:\n";
	print "\t1. ON\n";
	print "\t2. OFF\n[Default: OFF] > ";
	do
	{
		chomp($head_relay = <>);
	}
	while((!($head_relay =~ /^[1-2]+$/))&&($head_relay ne ""));

	switch($head_relay)
	{
		case 1	
		{
			$head_relay = 'ON';
			set($device, "DIG.COMMANDS", "$RELAY_BitMaskSet");
		}
		case 2  
		{
			$head_relay = 'OFF';
			set($device, "DIG.COMMANDS", "$RELAY_BitMaskClear");
		}
		else
		{
			$head_relay = 'OFF';
			set($device, "DIG.COMMANDS", "$RELAY_BitMaskClear");
		}
	}
	print "Head Relay status: $head_relay\n";
	return $head_relay;
}

#######################################################################################################################
#sub set_Head_relay_state
#{
#	my ($desired_state) = @_;
#	switch($head_relay)
#	{
#		case "ON"
#		{
#			$head_relay = 'ON';
#			set($device, "DIG.COMMANDS", "$RELAY_BitMaskSet");
#		}
#		case "OFF" 
#		{
#			$head_relay = 'OFF';
#			set($device, "DIG.COMMANDS", "$RELAY_BitMaskClear");
#		}
#		else
#		{
#			$head_relay = 'OFF';
#			set($device, "DIG.COMMANDS", "$RELAY_BitMaskClear");
#		}
#	}
#	return $head_relay
#}


# Enable to modify the DAC values
# Input: FGC device
# Output: none
sub enable_DAC_modification
{
	my ($device) = @_;
	# Enable the DAC value modification
	print "\nEnabling DAC ... ";
	set($device, "REF.FUNC.REG_MODE", "v");
	set($device, "VS.I_LIMIT.GAIN", "1");
	set($device, "PC", "IDLE");
	my $iteration_counter = 0;
	do
	{
		Time::HiRes::sleep(0.1);
		$FGC_state = get($this_FGC, "STATE.PC");
		#print "\nSTATE [$FGC_state]";
		$iteration_counter = $iteration_counter + 1;
	}
	while(($FGC_state ne "IDLE") && ($iteration_counter < 100));	 
	
	if($FGC_state eq "IDLE")
	{
		#Time::HiRes::sleep(0.5);
		print "\t\t\t\t\t[Done]\n";
	}
	else
	{
		print "\n\n\t##  Impossible to set the FGC to IDLE state  ##\n\n";
		exit 1;
	}
}

# Display menu to set the DAC1 value
# Input: FGC device
# Output: Value of DAC1 (var)
sub set_DAC1
{
	my ($device) = @_;
	# Set DAC1
	print "\nSet DAC1 value:\n[Default: 0] > ";
	
	chomp($dac1_value = <>);
	$dac1_value = $dac1_value =~ /^-?\d+\.?\d*$/ ? $dac1_value : 0.0;

	set($device, "REF", "NOW,$dac1_value");
	print "DAC1 value: $dac1_value\n";
	return $dac1_value;
}

# Display menu to set the DAC2 value
# Input: FGC device
# Output: Value of DAC2 (var)
sub set_DAC2
{
	my ($device) = @_;
	# Set DAC2
	print "\nSet DAC2 value:\n[Default: 0] > ";
	
	chomp($dac2_value = <>);
	$dac2_value = $dac2_value =~ /^-?\d+\.?\d*$/ ? $dac2_value : 0.0;
	$dac2_value = $dac2_value * $GAIN_VREF_IREG;

	set($device, "VS.I_LIMIT.REF", "$dac2_value");
	print "DAC2 value: $dac2_value\n";
	return $dac2_value;
}

# Set the output block
# Input: FGC device, str [UNBLOCK; BLOCK]
# Output: none
sub output_block
{
	my ($device, $value) = @_;
	# Unblock output
	print "\n";
	if($value eq "UNBLOCKING")
	{
		print "Unblocking output ... ";
		set($device, "DIG.COMMANDS", "$OB_BitMaskClear");
		$state = 0;
	}
	else
	{
		print "Blocking output ... ";
		set($device, "DIG.COMMANDS", "$OB_BitMaskSet");
		$state = 1;
	}

	my $iteration_counter = 0;
	do
	{
		Time::HiRes::sleep(0.02);
		$OPBLOCKED = index(get($device, "DIG.STATUS"),"OP_BLOCKED") != -1 ? 1 : 0;
		$iteration_counter = $iteration_counter + 1;
	}
	while(($OPBLOCKED != $state) && ($iteration_counter < 100));

	if($OPBLOCKED == $state)
	{
		print "\t\t\t\t\t[Done]\n";
	}
	else
	{
		print "\n\n\t##  Impossible to set the Output blocking  ##\n\n";
		exit 1;
	}
	
}

# Set the VS RUN
# Input: FGC device, state [0,1]
# Output: none
sub vs_run
{
	my ($device, $state) = @_;
	print "\t\t\t\t\tReady to set the VS_RUN\n";
	# VS RUN
	set($device, "FGC.DEBUG.MEM.MCU", "$DIG_IP1_ADDR"); 	# Access to the Signal VSRUN command Readback
	if($state == 1)
	{
		print "Set VS RUN ... \t";
		set($device, "DIG.COMMANDS", "$VSRUN_BitMaskSet");
	}
	else
	{
		print "Clear VS RUN ... ";
		set($device, "DIG.COMMANDS", "$VSRUN_BitMaskClear");
	}
	
	my $iteration_counter = 0;
	do
	{
		Time::HiRes::sleep(0.02);
		$VSRUN = get_vsrun_readback_Value($device);
		$iteration_counter = $iteration_counter + 1;
	}
	while(($VSRUN != $state) && ($iteration_counter < 100));
	
	print "\t\t\t\t\tReady to change the MCU adress to read the STATUS 12\n";
	if($VSRUN == $state)
	{
		set($device, "FGC.DEBUG.MEM.MCU", "$DIG_IPDIRECT_ADDR"); #Access to the STATUS 12 Input 
		print "\t\t\t\t\t[Done]\n";
	}
	else
	{
		print "\n\n\t##  Impossible to set the VSRUN  ##\n\n";
		exit 1;
	}	
}

# Get the test setup mode
# Input: FGC device
# Output: mode (str [SE_COP; TID; SE_SWITCH])
sub get_mode
{
	my ($device) = @_;
	# Checking configuration using number of DIMS
	my $dim_fault = 1;
	my $mode = "";
	my $iteration_counter = 0;
	do
	{
		Time::HiRes::sleep(0.1);
		my $dims_detected = get($device, "DIAG.DIMS_DETECTED");
		#print "\nDims Detected: $dims_detected\n";
		$iteration_counter = $iteration_counter + 1;
		switch($dims_detected)
		{
			case "1,0"
			{
				$mode = "SE_COP";
				$dim_fault = 0;
			}
			case "4,0"
			{
				$mode = "TID";
				$dim_fault = 0;
			}
			case "5,0"
			{
				$mode = "SE_SWITCH";
				$dim_fault = 0;
			}
			else
			{
				#print "dim expected fault\n";
			}
		}
	}
	while(($dim_fault == 1)&&($iteration_counter < 100));
	
	if($dim_fault == 1)
	{
		print "\n\n\t##  Unknown DIM configuration  ##\n\n";
		exit 1;
	}
	else
	{
		print "\nMode: $mode\n";
		return $mode;
	}
}

# get the VSRUN read back
# *** REQUIRES to set the fgc debug adr to IP1
# Input: FGC device
# Output: value (var [0; 1])
sub get_vsrun_readback_Value()
{
	my ($device) = @_;
	my $VSRUN = (hex(get($device, "FGC.DEBUG.MEM.MCU")) & $VSRUN_rb_BitMask) == $VSRUN_rb_BitMask ? 1 : 0; # Readback the VSRUN signal state
	return $VSRUN;
}


# get the status 12 value
# *** REQUIRES to set the fgc debug adr to IP direct
# Input: FGC device
# Output: value (var [0; 1])
sub get_status12_Value()
{
	my ($device) = @_;
	my $STATUS12 = (hex(get($device, "FGC.DEBUG.MEM.MCU")) & $STATUS12_BitMask) == $STATUS12_BitMask ? 1 : 0; # Read the STATUS 12 input
	return $STATUS12;
}

# Parse raw dig acquisitions
# Input: raw DIG acquistion array
# Output: csv formatted acquisition array ["d0.0, d0.1, ..."; "d1.0, d1.1, ..."];
sub parseDig
{
	my @acq_list = @_;
	my @digital_acquisitions; #array of acquisitions
	
	foreach $dig_acq(@acq_list)
	{
		my $digital_values = "";	# one acquisition [DIG1.0,DIG1.1,..]
		my @lines = split("\012", $dig_acq);	#split by \n to get only SUB1
		my $line = $lines[0];
		#print "Parsing line [$line]\n\n";
		$line =~ /(.*?):(.*)/;		# Get Field SUB1 and value
		my ($field, $value) = ($1, $2);

		my @elements    = split(',', $value);	#split by , to get elements
		foreach $element(@elements)
		{
			#print "Element [$element]";
			my $sub_line = (($_ = chop substr($element,0,-12)) eq " " ? "0":$_);
			#print "\t[$sub_line]\n";
			$digital_values .= $sub_line;
			$digital_values .= ",";
			undef $sub_line;
		}
		#print $digital_values
		push(@digital_acquisitions,$digital_values);
		undef $digital_values;
			
	}
	return @digital_acquisitions;
}

# Parse raw analog acquisitions
# Input: raw ANA acquistion array
# Output: csv formatted acquisition array ["a0.0, a0.1, ..."; "a1.0, a1.1, ..."];
sub parseAna
{
	my @acq_list = @_;
	my @analog_acquisitions; #array of acquisitions
	
	foreach $ana_acq(@acq_list)
	{
		my $analogue_values ="";	# one acquisition [ANA1.0,ANA1.1,ANA1.2,ANA1.3,ANA2.0,...]
		for my $line (split("\012", $ana_acq))
		# Get ANA1, ANA2, ...
		{
			$line =~ /(.*?):(.*)/;
			my ($field, $value) = ($1, $2);
			my @elements    = split(',', $value);
			#print "Field [$field]\n";
			if(length($value)>25)
			{
				my @elements    = split(',', $value);
				foreach $element(@elements)
				{
					#print "Element [$element]";
					my $sub_line = substr($element,18,-2);
					$sub_line =~ s/(?<!\w) //g; #remove space
					#print "\t[$sub_line]\n";
					$analogue_values .= $sub_line;
					$analogue_values .= ",";
					undef $sub_line;
				}
			}
		}
		push(@analog_acquisitions,$analogue_values);
		undef $analogue_values;
	}
	return @analog_acquisitions;
}
