#!/usr/bin/perl
#
# Name:    spy_acquire_pops.pl
# Purpose: Acquire data from the FGC Spy interface, allowing acquisition
#          to be synchronised with a specified user
# Notes:   This program relies on SPY.MPX[2] having a value of "BMEAS"
# Author:  Stephen Page

use FGC::Spy;
use strict;
use warnings;

# Read data from the Spy interface

sub readspy($$$)
{
    my ($port, $num_samples, $user) = @_;
    
    my @data;

    # If a user was specified, wait for a corresponding cycle to start

    if(defined($user))
    {
        my $user_found  = 0;

        while(!$user_found)
        {
            my $sample = FGC::Spy::readsample($port);
            my $b_meas = $sample->[2];

            # Check for matching user number in the B_MEAS signal

            if($b_meas > $user - .1 && $b_meas < $user + .1)
            {
                push(@data, $sample);

                # Check for 2 values in a row with a value of zero

                $sample = FGC::Spy::readsample($port);

                if($sample->[2] != 0)
                {
                    # Discard data and resume search for user

                    @data = ();
                    next;
                }
                push(@data, $sample);

                $sample = FGC::Spy::readsample($port);

                if($sample->[2] != 0)
                {
                    # Discard data and resume search for user

                    @data = ();
                    next;
                }
                push(@data, $sample);

                $user_found = 1;
            }
        }
    }

    # Read the requested number of samples

    for(my $i = @data ; $i < $num_samples ; $i++)
    {
        my $sample = FGC::Spy::readsample($port);

        # Append sample to data

        push(@data, $sample);
    }

    return(\@data);
}

# End of functions


# Handle command-line arguments

die "Usage: $0 <port (e.g. COM4)> <number of samples> [<user> [<num cycles>]]\n" if(@ARGV < 2);
my ($port_name, $num_samples, $user, $num_cycles) = @ARGV;
$num_cycles ||= 1;

# Connect to Spy interface

my $port = FGC::Spy::connect($port_name)
    or die "Unable to connect to FGC: $!\n";

# Reset FIFO

FGC::Spy::fiforeset($port);

# Start FIFO

FGC::Spy::fifostart($port, 1);

# Read from Spy interface

my @data;

for(my $cycle = 0 ; $cycle < $num_cycles ; $cycle++)
{
    my $cycle_data = readspy($port, $num_samples, $user);
    push(@data, @$cycle_data);
}

# Reset FIFO

FGC::Spy::fiforeset($port);

# Disconnect from Spy interface

FGC::Spy::disconnect($port);

# Print header

print "TIME,";
for(my $i = 0 ; $i < FGC::Spy::FGC_N_SPY_CHANS ; $i++)
{
    print "SPY.MPX[$i],";
}
print "\n";

# Print samples

my $time = 0;
for my $sample (@data)
{
    printf("%.03f,", $time);
    for my $field (@$sample)
    {
        printf("%.7e,", $field);
    }
    print "\n";

    # Add 1ms to $time

    $time += 0.001;
}

# EOF
