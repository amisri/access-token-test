#!/usr/bin/perl -w
#
# Name:     fgcreadtemps.pl
# Purpose:  Periodically read temperature data from FGCs
# Author:   Stephen Page

use POSIX qw(strftime);
use strict;

use FGC::Consts;
use FGC::RBAC;
use FGC::Sync;

my @gateway_names;
my $password;
my $username;
my $rbac_token;
my $rbac_last_auth_time = 0;

# Authenticate and obtain RBAC token

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate($username, $password);
    return(1) if(!defined($binary_token));

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return(0);
}

die "Usage $0 <gateway1> ... <gatewayN>\n" if(@ARGV < 1);

@gateway_names = @ARGV;

# Prompt for username

warn "\nUsername:\n";
chomp($username = <STDIN>);

# Prompt for password

warn "Password:\n";
system("stty -echo");
chomp($password = <STDIN>);
system("stty echo");
warn "\n\n";

# Flush stdout immediately

$| = 1;

while(1)
{
    # Check whether RBAC token is less than one hour from expiry

    if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
    {
        # Check whether it is at least one minute since last authentication

        if($rbac_last_auth_time < (time - 60))
        {
            print "Getting new RBAC token at ", scalar(localtime), "\n\n";

            if(get_rbac_token()) # RBAC authentication failed
            {
                warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }
        }

        # If there is no token or token has expired, wait then retry

        sleep(60), next if(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
    }

    foreach my $gateway_name (@gateway_names)
    {
    # Connect to gateway

        my $socket = FGC::Sync::connect( { name => $gateway_name } );

    # Set RBAC token on gateway

    my $response;
    eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", $rbac_token->{binary}, 1); };
    die "Failed to send command to set RBAC token on gateway $gateway_name\n" if($@);
    die "Error setting RBAC token on gateway $gateway_name: $response->{value}\n" if($response->{error});

    # Check each channel for log data

    for my $channel (1..(FGC_MAX_DEVS_PER_GW - 1))
    {
        my $response;

        # Read device name

        $response = FGC::Sync::get($socket, $channel, "DEVICE.NAME");
        next if($response->{error}); # Response was an error
        (my $device_name = $response->{value}) =~ s/,//g;

        # Read time

        $response = FGC::Sync::get($socket, $channel, "TIME.NOW");
        next if($response->{error}); # Response was an error
        my $time = $response->{value};

        $time =~ /^\s*(\d+)\.(\d+)$/;
        my $time_sec    = $1;
        my $time_usec   = $2;

        $time = sprintf("%s.%03d", strftime("%d/%m/%Y %H:%M:%S", localtime($time_sec)), $time_usec);

        # Read temperatures

        $response = FGC::Sync::get($socket, $channel, "TEMP");
        next if($response->{error}); # Response was an error

        (my $temps = $response->{value}) =~ s/[:\n]/,/g;

        print "$device_name,$time_sec.$time_usec,$time,$temps\n";
    }

    FGC::Sync::disconnect( { socket => $socket } );
    }

    sleep(600);
}

# EOF
