#!/bin/bash
#
#   Name:       nanny.sh
#   Purpose:    Nanny is a child (process) minder
#   Author:     Stephen Page

# Read commands from command files

read_commands()
{
    commands=() # Array of commands
    stderrs=()  # Array of stderr file names
    stdouts=()  # Array of stdout file names

    # Read each command file

    for file in $command_files
    do
        # Check whether the file is readable

        if [ ! -r $file ]; then
            echo "Cannot read command file $file"
            exit 1
        fi

        # Read commands from file

        while read stdout stderr command
        do
            # Ignore comments

            if [ ${stdout:0:1} == "#" ]; then
                continue
            fi

            # Append command information to arrays

            commands[${#commands[*]}]=$command
            stdouts[${#stdouts[*]}]=$stdout
            stderrs[${#stderrs[*]}]=$stderr
        done <$file
    done
}

# Run all commands

run_commands()
{
    starts=0
    for((i = 0 ; i < ${#commands[*]}; i++))
    do
        # Run the command in a loop within a sub-shell

        while :
        do
            # Use only the last 200k lines, and forget the rest

            tail -200000 ${stdouts[$1]} > ${stdouts[$1]}.new
            mv ${stdouts[$1]}.new ${stdouts[$1]}
            tail -200000 ${stderrs[$1]} > ${stderrs[$1]}.new
            mv ${stdouts[$1]}.new ${stderrs[$1]}

            # Run the command

            ${commands[$i]} >>"${stdouts[$i]}" 2>>"${stderrs[$i]}" &
            let starts=$starts+1
            echo "$(date) Started \"${commands[$i]}\" ($starts starts) with PID $!"
            wait

            # Sleep to prevent thrashing

            sleep 1
        done &
    done
}

# End of functions


# Extract command file names from arguments

command_files=$@

# Print usage if no command files were specified

if [ -z "$command_files" ]; then
    echo "Usage: $0 <command file 1> ... <command file n>"
    exit 1
fi

read_commands

# Check whether there were no commands in the files

if [ ${#commands[*]} -eq 0 ]; then
    echo "No commands to run" >&2
    exit 1
fi

# Kill child processes on exit

trap "kill 0; wait" EXIT

# Run the commands

run_commands
wait

# EOF
