The $package and its dependencies version $version has been installed.

Tag: $package/$version

You can check the release notes at https://gitlab.cern.ch/ccs/fgc/commits/master/sw/clients/perl/fgcrun+/fgcrun+ and at https://gitlab.cern.ch/ccs/fgc/commits/master/sw/clients/perl/FGC
