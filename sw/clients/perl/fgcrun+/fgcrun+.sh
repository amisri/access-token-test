#!/bin/sh

cpu=$(uname -m)
os=$(uname -s)
cmd="fgcrun+.pl"
#if grep -q "platform:el9" /etc/os-release
#then
#   cmd=fgcrun+.el9.pl
#fi

export PATH="/user/pclhc/bin/$os/$cpu:/user/pclhc/bin/$os:/user/pclhc/bin/perl:/user/pclhc/bin/matlab:/user/pclhc/bin/matlab/frequency_response_identification:$PATH"
export PERL5LIB="/user/pclhc/lib/perl"

# Add gnuplot font path
export GDFONTPATH="/user/pclhc/usr/share/fonts"

# EPCCCS-7057: Migrate user specific options file from pclhc to the users home folder
# move options file from pclhc to the users home directory, if it exists on pclhc but not in the users home
homedir=$(perl -e 'use File::HomeDir qw(home); print home();')
user_options_home="$homedir/.fgcrun+_options.dat"
user_options_pclhc="/user/pclhc/bin/perl/fgcrun+_useroptions/$USER.dat"
if [ ! -f $user_options_home ] && [ -f $user_options_pclhc ]; then
    mv $user_options_pclhc $user_options_home
fi

# run fgcrun+
if [ $# -ne 0 ]; then
    $cmd "$@"
else
    $cmd '' 'courier 9'
fi

# EOF
