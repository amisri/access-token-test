#!/usr/bin/perl
#
# Name:    json_acquire.pl
# Purpose: Acquire data from the FGC Spy interface and format results in Power Navigator JSON format
# Author:  Stephen Page and Quentin King

use FGC::Spy;
use strict;
use warnings;

# Process command line arguments

die "Usage: $0 <port> [<number seconds> [SPY.MPX[5] [SPY.MPX[4] ... [SPY.MPX[0]]]]\n" if(@ARGV < 1);

my $port_name   = $ARGV[0];
my $num_seconds = 1;
my @sig_name    = qw/I_REF I_MEAS V_REF V_MEAS I_A I_B/;

if(@ARGV > 1)
{
    $num_seconds = $ARGV[1];

    my $num_sig_names = @ARGV - 2;

    for(my $i = 0 ; $i < $num_sig_names ; $i++)
    {
        $sig_name[5-$i] = $ARGV[$i+2];
    }
}

# Connect to Spy interface

my $port = FGC::Spy::connect($port_name)
    or die "Unable to connect to FGC: $!\n";

# Read from Spy interface

my $num_samples = $num_seconds * 1000;

my $data = FGC::Spy::read($port, 1, $num_samples);

# Disconnect from Spy interface

FGC::Spy::disconnect($port);

# Print results in JSON for Power Navigator

my $unixtime = time;

print "{\n";
print " \"version\":\"1.1\",\n";
print " \"source\":\"FGC\",\n";
print " \"device\":\"fgc\",\n";
print " \"name\":\"spy\",\n";
print " \"type\":\"analog\",\n";
print " \"timeOrigin\":$unixtime.0,\n";
print " \"firstSampleTime\":$unixtime.0,\n";
print " \"period\":0.001,\n";
print " \"signals\":[\n";

for(my $i = 0 ; $i < FGC::Spy::FGC_N_SPY_CHANS ; $i++)
{
    if($i > 0)
    {
        print ",";
    }

    # Print signal header info

    print "  {\n";

    print "  \"name\":\"$sig_name[$i]\",\n";

    # Use trailing step interpolation for signals containing REF or ERR in their name

    if($sig_name[$i] =~ /REF/i || $sig_name[$i] =~ /ERR/i)
    {
        print "  \"step\":true,\n";
    }

    # print signal data

    print "  \"samples\":[";

    for(my $s = 0 ; $s < $num_samples ; $s++)
    {
        if($s > 0)
        {
            print ","
        }

        my $sample = @$data[$s];

        my $value = @$sample[$i];

        printf("%.7E", $value);
    }

    print "]\n  }\n";
}

print " ]\n}\n";

exit;

# EOF

