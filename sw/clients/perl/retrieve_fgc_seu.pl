#!/usr/bin/perl -w
#
# Name:     retrieve_fgc_seu.pl
# Purpose:  Retrieve FGC.SEU.TYPE arrays from all LHC FGC_92 devices
# Author:   Marc Magrans de Abril

use FGC::Async;
use FGC::DeviceList;
use FGC::Names;
use FGC::RBAC;
use File::Basename;
use strict;

# Read FGC name file

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Initialise FGC asynchronous communication module

FGC::Async::init($devices, $gateways);

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate_by_location()
    or die "RBAC authentication failed\n";

# Connect to all gateways

my %target_gateways;

for my $gateway (values(%$gateways)) {
	if($gateway->{channels}->[0]->{class} != 6) {
		next;
	} if($gateway->{name} =~ /cfc-a7-|cfc-866-|cfc-272-|cfc-sm18-|cfc-157-/) {
		next;
	} else {
		$target_gateways{$gateway->{name}} = $gateway;
	}

    eval { FGC::Async::connect($gateway) };
    if($@)
    {
        warn "Unable to connect to gateway $gateway->{name}\n";
        next;
    }

    # Set RBAC token on gateway

    eval { FGC::Async::set($gateway->{channels}->[0], "CLIENT.TOKEN", \$rbac_token, 1) };
    warn "Failed to send command to set RBAC token on gateway $gateway->{name}\n" if($@);
}

# Read responses to RBAC token set commands

my $commands = FGC::Async::read();
for my $command (values(%$commands))
{
    if($command->{response}->{error})
    {
        warn "Error setting RBAC token on gateway $command->{device}->{name}: $command->{response}->{value}\n";
    }
}

# Retrieve FGC.SEU

for my $device (grep { exists $target_gateways{$_->{gateway}->{name}} } values(%$devices)) {
	next if($device->{channel} == 0);

    eval { FGC::Async::get($device, "FGC.SEU.TYPE") };

    # Check whether an error occurred

    if($@) # An error occurred
    {
        warn "\t\tError getting property FGC.SEU.TYPE from $device->{name}: $@\n";
        next;
    }
}	

# Read responses

$commands = FGC::Async::read();

for my $command (values(%$commands)) {

    # Check whether an error occurred

    if($command->{response}->{error})
    {
        warn "\t\tError getting property $command->{property} from $command->{device}->{name}: $command->{response}->{value}\n";
        next;
    }

    # Ignore results with zeros

    if($command->{response}->{value} =~ /[\s,0]*/)
    {
        next;
    }

    # Store response

    print "$command->{device}->{name} = $command->{response}->{value}\n";
}