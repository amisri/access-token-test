#!/usr/bin/perl -w
#
# Name:     Test::FGC
# Purpose:  FGC-specific functions

use Test::Utils;

use strict;
use warnings;



my %tx_role_to_property =
(
    "SLAVE"    => \&setMaster,
    "MASTER"   => \&setSlaves,
    "PRODUCER" => \&setConsumers,
    "CONSUMER" => \&setSigSources,
);



sub setMaster
{
    my ($slave, $master) = @_;

    set($slave, "INTER_FGC.MASTER", $master);
    is(get($slave, "INTER_FGC.MASTER"), $master, "$slave: Master specified ($master)");
}



sub setSlaves
{
    my ($master, $slaves) = @_;

    set($master, "INTER_FGC.SLAVES", $slaves);
    is(get($master, "INTER_FGC.SLAVES"), $slaves, "$master: Slaves specified ($slaves)");
}



sub setSigSources
{
    my ($consumer, $producers) = @_;

    my @p  = split(/,/, $producers);
    my @sp = ();

    foreach (@p)
    {
        if($_ !~ /.*\@.*/ && $_ !~ /NONE/)
        {
            $_ = "I_MEAS@" . $_;
        }

        push @sp, $_;
    }

    set($consumer, "INTER_FGC.SIG_SOURCES", join(",", @sp));
    is(get($consumer, "INTER_FGC.SIG_SOURCES"), join(",", @sp), "$consumer: Producers specified ($producers)");
}



sub setConsumers
{
    my ($producer, $consumers) = @_;

    set($producer, "INTER_FGC.CONSUMERS", $consumers);
    is(get($producer, "INTER_FGC.CONSUMERS"), $consumers, "$producer: Consumers specified ($consumers)");
}



sub setRole
{
    my ($device, $role, $targets) = @_;

    $targets ||= [];

    $tx_role_to_property{$role}->($device, join(",", @$targets));
}



sub isMaster
{
    my ($device) = @_;

    return (get($device, "INTER_FGC.SLAVES") ne "NONE");
}



sub isSlave
{
    my ($device) = @_;

    return (get($device, "INTER_FGC.MASTER") ne "NONE");
}



sub setBroadcast
{
    my ($devices, $value) = @_;

    foreach (@$devices)
    {
        set($_, "INTER_FGC.BROADCAST", $value);
    }
}



sub setSignals
{
    my ($device, $signals) = @_;

    set($device, "INTER_FGC.PRODUCED_SIGS", $signals);
    is(get($device, "INTER_FGC.PRODUCED_SIGS"), $signals, "$device: Signals specified ($signals)");
}



sub prepareTest
{
    my @devices = @_;

    # Find all masters and turn them off, the slaves should follow

    foreach my $device (@devices)
    {
        if(isMaster($device))
        {
            set($device, "PC", "OF");
            wait_until( sub { get($device, "STATE.PC") =~ "OFF" } );
            ok(get($device, "STATE.PC") =~ "OFF", "$device: Master FLT_OFF/OFF");
        }
    }

    # Make sure that all slaves are off

    foreach my $device (@devices)
    {
        if(isSlave($device))
        {
            wait_until( sub { get($device, "STATE.PC") =~ "OFF" } );
            ok(get($device, "STATE.PC") =~ "OFF", "$device: Slave FLT_OFF/OFF");
        }
    }

    # Turn off everything else

    foreach my $device (@devices)
    {
        if(get($device, "STATE.PC") !~ "OFF")
        {
            set($device, "PC", "OF");
            wait_until( sub { get($device, "STATE.PC") =~ "OFF" } );
            ok(get($device, "STATE.PC") =~ "OFF", "$device: Master FLT_OFF/OFF");
        }
    }

    # Clear role properties

    foreach my $device (@devices)
    {
        if(isMaster($device))
        {
            setRole($device, "MASTER", ["NONE"]);
            setRole($device, "SLAVE",  ["NONE"]);
        }
        else
        {
            setRole($device, "SLAVE",  ["NONE"]);
            setRole($device, "MASTER", ["NONE"]);
        }

        setRole($device, "CONSUMER", ["NONE"]);
        setRole($device, "PRODUCER", ["NONE"]);

        setSignals($device, "NONE");
    }
}

# EOF
