=head1 NAME

TCP

=head1 DESCRIPTION

Module to comunicate with a gateway by TCP protocol

=cut

package Test::TCP;

use strict;
use warnings;

use Carp;
use FGC::Sync;
use FGC::RBAC;
use FGC::Names;
use FGC::Sub;
use IO::Socket;
use List::Util;
use Scalar::Util;

require Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                    authenticate
                    authorize
                    import_token
                    set_token
                    get
                    set
                    subscribe
                    unsubscribe
                    get_next_publication
                    create_struct_for_device
                    disable_authentication
                );


my $_devices_name__devices;
my $_token;
my $_imported_token;
my $_auth_method;


=item authenticate(string auth_method)

If the method is "location" authenticate by location, otherwise authenticate explicitly.

=cut

sub authenticate($)
{
    my ($auth_method) = @_;

    if (defined($auth_method))
    {
        if(defined($_auth_method) && $_auth_method eq 'disabled')
        {
            return 0;
        }
        else
        {
            $_auth_method = $auth_method;
        }
    }

    # Request token if not defined or if the token has less than one hour from expiry

    if (!defined($_token) || time > ($_token->{ExpirationTime} - 3600))
    {
        my $bin_token;

        if(!defined($_auth_method) || index('location',lc $_auth_method) != -1)
        {
            $_auth_method = 'location';
            $bin_token = FGC::RBAC::authenticate_by_location();
        }
        elsif(index('token',lc $_auth_method) != -1)
        {
            $_auth_method = 'token';

            if(!defined($_imported_token->{binary}))
            {
                die "ERROR: Missing RBAC Token";
            }
            elsif(time > ($_imported_token->{ExpirationTime} - 3600))
            {
                die "ERROR: Session Expired";
            }

            $bin_token = $_imported_token->{binary};
        }
        else
        {
            $_auth_method = 'explicit';
            $bin_token = FGC::RBAC::authenticate_explicit();
        }

        if (!defined($bin_token))
        {
            die "ERROR: Authentication failed";
        }

        return $bin_token;
    }
    else
    {
        return $_token->{binary};
    }
}

sub authorize
{
    my ( $device) = @_;

    my $bin_token = authenticate($_auth_method);

    my $gateway = $device->{gateway};

    if(!defined($gateway->{socket}) or !$gateway->{socket}->connected())
    {
        # First time connecting

        # Store new token

        set_token($bin_token);

        # connect

        my $response;
        FGC::Sync::connect($gateway);
        if($_auth_method ne 'disabled'){
            $response = FGC::Sync::set($gateway->{socket}, "", 'CLIENT.TOKEN', \$_token->{binary}, 1);
        }

        if($response->{error})
        {
            FGC::Sync::disconnect($gateway);
            die "ERROR: Failed to set token at '$gateway->{name}': $response->{value}\n";
        }

    }
    elsif ($bin_token ne $_token->{binary})
    {
        # Setting new token to existing connections

        # Store new token

        set_token($bin_token);

        # send new token without reconnecting

        my $response;
        for my $device (values %$_devices_name__devices)
        {
            if ($device->{channel} == 0 and defined($device->{gateway}->{socket}))
            {
                if($_auth_method ne 'disabled'){
                    $response = FGC::Sync::set($gateway->{socket}, "", 'CLIENT.TOKEN', \$_token->{binary}, 1);
                }
                if($response->{error})
                {
                    FGC::Sync::disconnect($gateway);
                    die "ERROR: Failed to set token at '$gateway->{name}': $response->{value}\n";
                }
            }
        }
    }
}

sub set_token($)
{
    my ($bin_token) = @_;

    $_token                 = FGC::RBAC::decode_token(\$bin_token);
    $_token->{binary}       = $bin_token;
}

sub import_token($)
{
    my ($bin_token) = @_;
    $_imported_token = FGC::RBAC::decode_token(\$bin_token);
    $_imported_token->{binary} = $bin_token;
}

=cut

=item device get_device_from_name

Returns structure of interesting device given by it's name

=cut

sub _get_device_from_name
{
    my ( $device_name ) = @_;

    # Get all devices structures from downloaded name file if not yet done

    eval { ( $_devices_name__devices ) = FGC::Names::read() unless $_devices_name__devices };
    if($@)
    {
        die "ERROR: Cannot get devices: $@\n";
    }

    # Get device from the structure with required name

    if (!defined($_devices_name__devices->{uc $device_name}))
    {
        die "ERROR: Device '$device_name' does not exist\n";
    }
    else
    {
        return $_devices_name__devices->{uc $device_name};
    }
}

=cut

=item get(string device_name, string property_name)

Get $property_name from $device_name.

=cut

sub get($$;$;$)
{
    my ( $device_name, $property_name, $cycle) = @_;

    my $device = _get_device_from_name($device_name);

    authorize($device);

    # split flags and property to set the cycle

    if (defined($cycle))
    {
        my ($property,$flags) = split(/\s/,$property_name,2);
        if (defined($flags))
        {
            $property_name =  "$property($cycle) $flags";
        }
        else
        {
            $property_name =  "$property($cycle)";
        }
    }


    my $response;
    $response = FGC::Sync::get($device->{gateway}->{socket}, $device->{channel}, $property_name);

    if($response->{error})
    {
        die "ERROR: TCP GET $device_name/$property_name: $response->{value}\n";
    }

    return $response->{value}
}

=item set($device_name, $property_name, $value)

Set $property_name on $device_name to $values.

$values are comma separated if there are many of them.

Some fields could be empty if should not be changed.

=cut

sub set($$$;$;$)
{
    my ( $device_name, $property_name, $values, $cycle) = ( @_ );

    # split flags and property to set the cycle

    if (defined($cycle))
    {
        my ($property,$flags) = split(/\s/,$property_name,2);
        if (defined($flags))
        {
            $property_name =  "$property($cycle) $flags";
        }
        else
        {
            $property_name =  "$property($cycle)";
        }
    }

    # If values as passed as a reference to array, then join them to one string

    $values = join(',', @$values) if ref $values eq 'ARRAY';

    my $device = _get_device_from_name($device_name);

    authorize($device);

    my $response;
    $response = FGC::Sync::set($device->{gateway}->{socket}, $device->{channel}, $property_name, $values);

    if($response->{error})
    {
        die "ERROR: TCP SET $device_name/$property_name $values: $response->{value}\n";
    }
}

=item subscribe($device_name; $subscription_period)

Subscribe to $device_name using UDP with a period of $subscription_period cycles.

Then you can get_next_publication and finally unsubscribe.

=cut

sub subscribe($;$)
{
    my ( $device_name, $subscription_period) = @_;

    my $device  = _get_device_from_name($device_name);
    my $gateway = $device->{gateway};
    my $subscription_socket;

    if (!defined($subscription_period) or $subscription_period <= 0 or $subscription_period > 10000 )
    {
        $subscription_period = 50;
    }
    authorize($device);

    # Set up subscription on gateway if not yet done

    unless($gateway->{subscription_socket})
    {
        # Create UDP socket

        socket($subscription_socket, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

        # Bind to first free port

        my $port;
        for($port = 1024 ; $port <= 65535 ; $port++)
        {
            # Create address structure

            bind($subscription_socket, sockaddr_in($port, INADDR_ANY)) and last;
        }
        die "ERROR: Unable to bind to UDP port\n" if($port > 65535);

        # Set up subscription on passed gateway

        set($gateway->{name}, "CLIENT.UDP.SUB.PORT", $port);
        set($gateway->{name}, "CLIENT.UDP.SUB.PERIOD", $subscription_period);

        # Set gateway socket

        $gateway->{subscription_socket} = $subscription_socket;
    }

    # Return subscription socket, then it could be checked if it has new data with 'select' syscall

    $gateway->{subscription_socket};
}

=item unsubscribe($device_name)

Unsubscribe device.

=cut

sub unsubscribe($)
{
    my ( $device_name ) = @_;

    my $device  = _get_device_from_name($device_name);
    my $gateway = $device->{gateway};

    # We do not care about this errors

    eval{set($gateway->{name}, "CLIENT.UDP.SUB.PERIOD", '');};
    eval{set($gateway->{name}, "CLIENT.UDP.SUB.PORT", '');};

    close($gateway->{subscription_socket});
    delete $gateway->{subscription_socket};
}

sub _compose_subscription_for_device
{
    my ( $device, $subscription, $time_sec, $time_usec) = @_;
    my $subscription_for_device = $subscription->[$device->{channel}];

    # Add keys CYCLE_NUM and CYCLE_USER to a device taken from status about gateway

    $subscription_for_device->{CYCLE_NUM} = $subscription->[0]->{CYCLE_NUM};
    $subscription_for_device->{CYCLE_USER} = $subscription->[0]->{CYCLE_USER};
    $subscription_for_device->{ACQTIME} = $time_sec + $time_usec/1000000.0;

    # Internal function to walk through hash of hashes and compose dump

    my $dump_hash;
    $dump_hash =
        sub
        {
            my ( $structure, $prefix ) = @_;

            my $flat = '';

            while( my ($key, $value) = each %$structure)
            {
                my $prefix_with_key = ($prefix ? $prefix.'.' : '').$key;

                if(ref($value) eq 'HASH')
                {
                    $flat .= $dump_hash->($value, $prefix_with_key);
                }
                else
                {
                    $flat .= $prefix_with_key.':'.($value || '').' ';
                }
            }

            $flat;
        };

    $dump_hash->($subscription_for_device, '');
}

=item get_next_publication($device_name)

Returns composed subscription message about interesting device.

If interesting cycle is specified during subscribe invocation, then it will wait for it

=cut

sub get_next_publication($)
{
    my ( $device_name ) = @_;

    my $device = _get_device_from_name($device_name);

    # Read subscription

    my ($sockaddr_in, $sequence, $time_sec, $time_usec, $subscription) = FGC::Sub::read($device->{gateway}->{subscription_socket});

    # Compose subscription message about interesting device

    _compose_subscription_for_device($device, $subscription, $time_sec, $time_usec);
}

=item create_struct_for_device($device_name; $gateway; $channel)

Creates a partial device struct for a device without the need to parse the name file.

=cut

sub create_struct_for_device($$$)
{
    my ( $device_name, $gateway, $channel) = @_;

    if (defined($_devices_name__devices) && defined($_devices_name__devices->{uc $device_name}))
    {
        return;
    }
    elsif(!defined($device_name) || !defined($gateway) || !defined($channel))
    {
        return;
    }

    my $device;
    my $gateway_struct;

    $gateway_struct->{name} = $gateway;
    $gateway_struct->{gateway} = $gateway_struct;
    $gateway_struct->{channel} = 0;

    $device->{gateway}  = $gateway_struct;
    $device->{name}     = $device_name;
    $device->{channel}  = $channel;

    $_devices_name__devices->{uc $device_name} = $device;
    $_devices_name__devices->{uc $gateway} = $gateway_struct;
}

=item set_authentication()

Sets $_auth_method

=cut

sub set_authentication($)
{
    my ($auth_method) = @_;

    $_auth_method = $auth_method;
}

=item flush_connections()

Flushes the connections and subsciptions to devices

=cut

sub flush_connections()
{
    if ($_devices_name__devices)
    {
        for my $device (values %$_devices_name__devices)
        {
            if (defined($device->{subscription_socket}))
            {
                unsubscribe($device->{name});
            }

            if (defined($device->{socket}))
            {
                close($device->{gateway}->{socket});
                delete $device->{gateway}->{socket};
            }
        }
    }
}



END
{
    flush_connections();
}

1;

# EOF
