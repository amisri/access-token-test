#!/usr/bin/perl -w
#
# Filename: Test/CMW.pm
# Purpose:  Interface to the CMW command line tools
# Author:   Marc Magrans de Abril

package Test::CMW;

use strict;
use warnings;

use IPC::Open3 qw( open3 );
use IO::Select;
use Carp;
use Switch;

require Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                    authenticate
                    authorize
                    get
                    get_sub_dev
                    set
                    subscribe
                    unsubscribe
                    get_next_publication
                );

our $TIMEOUT = 60;

my %device_name_fh;
my %device_name_pid;

sub authenticate
{
    my ($auth_method) = @_;

    if($auth_method !~ /^location$/)
    {
        # So far support is only for by location

        die "ERROR: CMW protocol implements authentication only by location so far\n";
    }
}

# The get function performs a CMW GET request using rda-get
# Get requires a device name, a property name, optionally the property contains also  and opionally a cycle name
# Get returns a hash with properties as keys, and values as

sub get($$;$)
{
    my ($device,$property,$cycle) = @_;

    my $output;
    my @cmd;
    my $result;

    # Extract property field pair

    # Build command string

    push @cmd, '/acc/local/Linux/bin/rda-get', '--rbac', 'location', '--dev', $device, '--prop', $property;

    if (defined($cycle))
    {
        push @cmd, '--cycle', $cycle;
    }

    # Execute command

    open(my $null_stdin, '<', '/dev/null') or die "ERROR: Cannot run rda-get command: $!\n";
    my $pid = open3(
       $null_stdin,
       \local *RDAGET,
       undef, # 2>&1
       @cmd
    );
    $output = join '', (<RDAGET>);

    waitpid($pid, 0);
    close($null_stdin);

    if ($?)
    {
        # Replace new line characters by space to have ERROR message in one line which is easier to grep

        $output =~ s/\r|\n/ /g;
        die "ERROR: CMW GET $device/$property: $output\n";
    }

    my $name;

    foreach my $line (split(/\n/, $output))
    {
        chomp($line);

        # Parse property-value pairs

        if ($line =~ /Name\s*:\s*([^\s]*)/)
        {
            # If we are getting a leaf property, it should have 'value' as name

            if ($1 eq 'value')
            {
                $name = $property;
            }
            else
            {
                $name = $property . "." . $1;
            }
        }
        elsif (defined $name)
        {
            # Property value starts with 'Value' key on the first line and, for function lists,
            # may span over multiple lines starting with '<'.

            if ($line =~ /Value\s*:\s*(.*?)$/)
            {
                $result .= $name . ':' . join(',', split(/\s+/, $1));
            }
            elsif ($line =~ /\s*(\<\s*.*?)$/)
            {
                $result .= join(',', split(/\s+/, $1));
            }
        }
    }

    # Delete last space

    chomp($result);

    # If there is only one field to return, then don't provide field name

    if ($result =~ /:/g == 1)
    {
        $result =~ /:(.*)/;
        $result = $1;

        # Remove trailing ">" and leading "<"

        $result =~ s/^<//;
        $result =~ s/>$//;
    }

    # Return concatenated string value

    return $result;
}

sub get_sub_dev
{
    # TODO Get rid of this hardcoded array

    my @sub_devices =
    (
        "ADTH.866.1.DEV",
        "ADTH.866.2.DEV",
        "ADTH.866.3.DEV",
        "ADTH.866.4.DEV",
        "ADTH.866.5.DEV",
        "ADTH.866.6.DEV",
        "ADTH.866.7.DEV",
        "ADTH.866.8.DEV",
        "ADTV.866.9.DEV",
        "ADTV.866.10.DEV",
        "ADTV.866.11.DEV",
        "ADTV.866.12.DEV",
        "ADTV.866.13.DEV",
        "ADTV.866.14.DEV",
        "ADTV.866.15.DEV",
        "ADTV.866.16.DEV",
    );

    my ($d, $channel, $property, $method) = @_;

    # TODO Change it to 3 when the 3rd method is fixed

    $method ||= int(rand(2));

    # Create a sub dev property name by inserting a dot (.) after two first characters (FG)

    my $subdev_property = $property;
    substr($subdev_property, 2, 0) = '.';

    my $sd = $sub_devices[$channel];
    my $subdev_index = $channel + 1;

    switch($method)
    {
        case 0 { return get($d,  "${property}[${channel}]")        }
        case 1 { return get($sd, $subdev_property)                 }
        case 2 { return get($d,  "$subdev_index:$subdev_property") }
        else   { return undef                                      }
    }
}

# The get function performs a CMW SET request using rda-set
# Set requires a device name, a property name, and a value. Optionally a cycle name

sub set($$$;$)
{
    my ($device,$property,$values,$cycle) = @_;

    # If values as passed as a reference to array, then join them to one string

    $values = join(',', @$values) if ref $values eq 'ARRAY';

    my $output;
    my %result;
    my $field;
    my @cmd;

    # Extract property field pair

    # ($property,$field) = split(/\.([^\.]+)$/, $property);

    # Build command string

    push @cmd, '/acc/local/Linux/bin/rda-set', '--rbac', 'location', '--dev', $device, '--prop', $property;

    if($property =~ "REF.TABLE.FUNCLIST")
    {
        push @cmd, '-v', "$values";
    }
    elsif($values =~ /,/)
    {
        push @cmd, '-v', "[$values]";
    }
    else
    {
        push @cmd, '-v', "$values";
    }

    if($cycle)
    {
        push @cmd, '--cycle', $cycle;
    }

    if($field)
    {
        push @cmd, '--fields', $field;
    }

    # Execute command

    open(my $null_stdin, '<', '/dev/null') or die "ERROR: Cannot run rda-set command: $!\n";
    my $pid = open3(
        $null_stdin,
        \local *RDASET,
        undef,      # 2>&1
        @cmd
    );
    $output = join '', (<RDASET>);
    waitpid($pid, 0);
    close($null_stdin);

    if($?)
    {
        $output =~ s/\r|\n/ /g;
        die "ERROR: CMW SET $device/$property: $output\n";
    }

    return 1;
}

sub subscribe($;$$)
{
    my ( $device_name, $property, $cycle ) = @_;

    my $cmd = "/acc/local/Linux/bin/rda-subscribe --rbac location --dev '$device_name' --prop '$property'";

    if($cycle)
    {
        $cmd .= " --cycle '$cycle'";
    }

    # Open pipe

    $device_name_pid{$device_name} = open(my $fh, "unbuffer /acc/local/Linux/bin/rda-subscribe --rbac location --dev '$device_name' --prop '$property' |") or die "ERROR: Cannot subscribe: $!\n";

    # Assign $device_name with its file handler and return it

    $device_name_fh{$device_name} = $fh;
}

sub get_next_publication($)
{
    my ( $device_name ) = @_;

    # Get file handler for passed $device_name

    my $fh = $device_name_fh{$device_name};

    if (tell($fh) == -1)
    {
        unsubscribe($device_name);
        die "ERROR: SUBSCRIPTION $device_name: rda-subscribe file handle closed\n";
    }

    my $sel = IO::Select->new($fh);

    my @ready = $sel->can_read($TIMEOUT);

    # Timeout

    if (! scalar(@ready))
    {
        unsubscribe($device_name);
        die "ERROR: SUBSCRIPTION $device_name timeout ($TIMEOUT sec)\n";
    }

    my $lines = '';

    while(my $line = <$fh>)
    {
        $lines .= $line;

        # Whole subscription packet ends with 3 new line characters. Read stream until they are seen.

        if($lines =~ /\n\s?\n\n/m)
        {
            # Parse property name and acquistion timestamp

            if($lines =~ /Notification: [^\/]+\/([\w\.]+).*acqStamp=(\d+)/ms)
            {
                my ( $property, $acqStamp ) = ( $1, $2 );

                # Loop through all property fields and compose output message

                my $output = '';

                while( $lines =~ /Name : ([\w\.]+)\n.*\nValue: ?(.*)/mg)
                {
                    my ( $field, $value ) = ( $1, $2 );

                    # Build whole property name

                    my $property_name = ($field eq 'value') ? $property : $property.'.'.$field;

                    $output .=  "\U$property_name:$value ";
                }

                # Return composed message

                return $output;
            }
            else
            {
                die "ERROR: unexpected rda-subscribe output: \n$lines";
            }
        }
    }
}

sub unsubscribe($)
{
    my ( $device_name ) = @_;

    kill 'KILL', $device_name_pid{$device_name};
    delete $device_name_pid{$device_name};

    my $fh = $device_name_fh{$device_name};
    delete $device_name_fh{$device_name};

    close $fh;
}

END
{
   if (%device_name_fh)
   {
       for my $d (keys %device_name_fh)
       {
           unsubscribe($d);
       }
   }
}
# EOF

return(1);
