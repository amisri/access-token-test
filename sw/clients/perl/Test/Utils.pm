=head1 NAME

Test

=head1 DESCRIPTION

Core module of FGC test framework.

=cut

use strict;
use warnings;

use Getopt::Long;
use Carp;
use Time::HiRes qw(sleep time);
use Test::Most qw(die no_plan !set);
use DBI;
use FGC::DB;

our $TIMEOUT        = 60;
our $POLLING_PERIOD = 2;

=head1 FUNCTIONS

=over

=cut

=item Prints help message and exit program

=cut

sub print_help_message_and_exit
{
    my ( $default_opts, $exit_code) = @_;

    print "Usage: $0 [options]\n\n";
    print "Script is written to test behaviour of FGC or Pow devices\n\n";
    print "Options and default values:\n";

    for my $option (sort keys %$default_opts)
    {
        print "--$option $default_opts->{$option}\n";
    }

    print "\n";
    print "where\n";
    print "--help is this help message\n";
    print "--authenticate location|explicit\n";
    print "--protocol cmw|tcp\n";

    exit $exit_code;
}

=cut

=item The input is a hash with all the possible options and their default values (excluding "help" and "authenticate" which are always assumed). The function parses the command line arguments

=cut
sub parse_opts
{
    my %default_opts = @_;

    # Authentication is by location by default

    $default_opts{authenticate} ||= 'location';

    # Protocol is CMW by default

    $default_opts{protocol}     ||= 'tcp';

    # Options are based on default values

    my %opts = %default_opts;

    # Prepare list of possible optional CLI options to tell about them GetOptions

    my %get_options_args = map( { $_.':s', \$opts{$_} } keys %opts );

    # There is always --help switch

    $get_options_args{help} = \$opts{help};

    # Parse CLI options

    GetOptions( %get_options_args ) or print_help_message_and_exit( \%default_opts, 2);

    # Print help message if --help switch was specified or --device to test was not specified

    print_help_message_and_exit( \%default_opts, 0) if($opts{help});

    # Use choosen protocol

    if($opts{protocol} =~ /CMW/i)
    {
        require Test::CMW;
        import  Test::CMW;
    }
    else
    {
        require Test::TCP;
        import  Test::TCP;
    }

    require Test::Boot;
    import  Test::Boot;

    require Test::FGC;
    import  Test::FGC;

    # Return parsed options

    %opts;
}

sub load_config
{
    my ( $device_name, $config_path ) = @_;

    open(FILE, "<$config_path")
        or die "ERROR: Unable to open file for reading : $!\n";

    my $line = 0;
    while(<FILE>)
    {
        $line++;

        chomp;

        s/#.*//;            # Remove comments

        next if(/^\s*$/); # Ignore blank lines

        if(/^!\s*S /)
        {
            s/^!\s*S //i; # Remove "! S " from beginning of line

            (my $property   = $_) =~ s/^(\S+).*/$1/;
            (my $value      = $_) =~ s/^\S+\s+(.*)/$1/;

            $value =~ s/\s+//g; # Remove spaces from value

            # Add parsed pair of property and value

            set($device_name, $property, $value);
        }
        else
        {
           # If line is not beginning with '! S' chars, then assume its a perl code to evaluate

            eval;

            if ($@)
            {
                die "ERROR: load_config($config_path at $line)"
            }
        }
    }

    close(FILE);
}

=item wait_until($code, $timeout, $polling_period)

Wait until condition in passed $code returns true.

It will reexecute forever or with optional passed $timeout and optional passed $polling_period interval

=cut

sub wait_until(&;$$)
{
    my ( $code, $timeout, $polling_period ) = @_;

    $timeout        ||= $TIMEOUT;
    $polling_period ||= $POLLING_PERIOD;

    my $start_time = time();
    until( $code->() )
    {
        confess "ERROR: wait_until() timeout exceeded" if $timeout and time() - $start_time > $timeout;
        sleep $polling_period;
    }
}

=item sleep_except($except_code, $time_s, $polling_period)

Wait given $time_s, but confirm that during whole sleep passed $except_code is always false with given $polling_period interval

=cut

sub sleep_except($$;$)
{
    my ( $except_code, $time_s, $polling_period ) = @_;

    $polling_period ||= $POLLING_PERIOD;

    my $start_time = time();
    while(time() - $start_time < $time_s)
    {
        die "ERROR: sleep_except() stopped by condition before $time_s seconds. " if $except_code->();
        sleep($polling_period);
    }
}

=item get_logs

Retrieve the log buffer from the device and return a structure with the log data.

Also writes it to a file in case of further investigations.

=cut

sub get_logs
{
    my ( $device_name, $log_buffer_name ) = @_;

    my $buffer = get($device_name, $log_buffer_name.' bin');

    # Decode binary buffer into perl structure

    my $log = FGC::Log::decode(\$buffer);

    # Compose plain text and write it to file in purpose of further investigation

    my $acquisition_dir = FGC::Paths::get_acquisition_dir();

    make_path $acquisition_dir;

    # write_file
    #         $acquisition_dir.'/'.$log_buffer_name.'.log',
    #         FGC::Log::compose
    #             $log;

    # Read previously prepared pattern

    # my $log_pattern =
    #         FGC::Log::parse
    #         @{$fixture->{pattern}};
}

{

    our $dbh;

=item get_devices

Retrieve the device names given a series of space separater series of wildcards

The wildcards apply to the DB fields ACCELERATOR, DEVICENAME, CLASSNAME, and FECNAME

Each wildcard implies an AND condition (e.g. "PowM% CPS" means all devices with CLASSNAME like 'PowM%' AND ACCELERATOR like 'CPS')

=cut


    sub get_devices($)
    {
        my $filter;
        my @wildcards;
        my $query;
        my $device;
        my @result;


        my ($sql_wildcards) = @_;

        # Create DB connection

        if (!defined($dbh))
        {
            $dbh = FGC::DB::connect(FGC_DB_USERNAME,FGC_DB_PASSWORD) or die "ERROR: Can not connect to DB (error=" . DBI->errstr . ")\n";
        }

        # Create query string


        foreach my $wildcard (split(' ',$sql_wildcards))
        {
            chomp($wildcard);

            if ($wildcard ne "")
            {
                if (defined($filter))
                {
                    $filter = $filter . " AND ";
                }

                $filter = $filter . " ( (ACCELERATOR like ?) OR (CLASSNAME like ?) OR (DEVICENAME like ?) OR (FECNAME like ?) ) ";
                push(@wildcards, $wildcard, $wildcard, $wildcard, $wildcard);
            }
        }

        my $query_str = "SELECT DEVICENAME from ABC.DEVICES_V WHERE " . $filter . " ORDER BY DEVICENAME ASC";

        # Prepare query

        $query  = $dbh->prepare($query_str) or die "ERROR: Can not prepare query (" . $dbh->errstr .")\n";

        # Execute query

        $query->execute(@wildcards) or die "ERROR: Can not execute query (" . $query->errstr . ")\n";

        # Return array of result row hashes

        while ( ($device) = $query->fetchrow_array() )
        {
            push(@result,$device);
        }

        return @result;
    }

}

1;

# EOF
