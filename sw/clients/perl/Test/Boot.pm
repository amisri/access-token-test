#!/usr/bin/perl -w
#
# Name:     Test/Boot.pm
# Purpose:  Communication with FGC boot program

package Test::Boot;

use strict;
use warnings;

use FGC::Boot;

require Test::TCP;

require Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                     sendCommand
                     sendCommandNoWait
                );

=item sendCommand($fgc_name, $command)

Sends a command to the FGC. The $menu should be a string that contains a menu (command) node number.

It is a blocking function so do not use it with any kind of resets, power cycle etc,
because it will hang waiting for a response - in such cases use sendCommandNoWait function.

nnIt will die if the FGC is not in boot or a remote terminal can not be started.

=cut

sub sendCommand
{
    my ($fgc_name, $command) = @_;

    my $device = Test::TCP::_get_device_from_name($fgc_name);

    Test::TCP::authorize($device);

    # Start the remote terminal - needed if you want to send a command

    my $response = FGC::Sync::rtermstart($device->{gateway}->{socket}, $device->{channel});

    if($response->{error})
    {
        die "Cannot start remote terminal\n";
    }

    # Send the command (blocking)

    $response = FGC::Boot::command($device->{gateway}->{socket}, $command);

    # Stop the remote terminal

    FGC::Sync::rtermstop($device->{gateway}->{socket});

    return $response;
}



=item sendCommandNoWait($fgc_name, $command)

Sends a command to the FGC. The $menu should be a string that contains a menu (command) node number.

Similar to sendCommand but it doesn't wait for a response.
It will die if the FGC is not in boot or a remote terminal can not be started.

=cut
sub sendCommandNoWait($$)
{
    my ($fgc_name, $command) = @_;

    my $device = Test::TCP::_get_device_from_name($fgc_name);

    Test::TCP::authorize($device);

    # Start the remote terminal - needed if you want to send a command

    my $response = FGC::Sync::rtermstart($device->{gateway}->{socket}, $device->{channel});

    if($response->{error})
    {
        die "Cannot start remote terminal\n";
    }

    # Send the command (non-blocking)

    FGC::Boot::command_no_wait($device->{gateway}->{socket}, $command);

    # Stop the remote terminal

    FGC::Sync::rtermstop($device->{gateway}->{socket});
}

1;

# EOF