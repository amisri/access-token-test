#!/usr/bin/perl -w
#
# Name:     Test::FGC
# Purpose:  FGC-specific functions

package Test::FGC;

use strict;
use warnings;

use Carp;

use Test::Boot;
use Test::TCP;
use FGC::Platform::Fgc3::Auto;

require Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw(
                     zipTable
                     unzipTable
                     getPropertyInfo
                     isInBoot
                     isInMain
                     assureBoot
                     assureMain
                     isInBootNoSub
                     assureBootNoSub
                     isInMainNoSub
                     assureMainNoSub
                     assureMainInOff
                     fgc_wait_until
                     syncFgc
                     setPc
                     assureDSP
                     sendTest
                     sendPowercycle
                     sendGlobalReset
                     sendSlowWatchdog
                     getDevicesFromGateway
                );

our %pc_state_aliases = (
    "OF" => "OFF",
    "SA" => "SLOW_ABORT",
    "SB" => "ON_STANDBY",
    "IL" => "IDLE",
    "CY" => "CYCLING",
    "BK" => "BLOCKING",
    "EC" => "ECONOMY",
    "DT" => "DIRECT",
);

our $TIMEOUT        = 60;
our $POLLING_PERIOD = 2;
our $DSP_MAX_RESETS = 10;    # Max number of resets to perform in order to wake up DSP

my $gateways;



sub _getGateway
{
    my ( $gateway_name ) = @_;

    # Get all devices structures from downloaded name file if not yet done

    eval { ( undef, $gateways ) = FGC::Names::read() unless $gateways };
    if($@)
    {
        die "ERROR: Cannot get gateways: $@\n";
    }

    # Get device from the structure with required name

    if (!defined($gateways->{lc $gateway_name}))
    {
        die "ERROR: Gateway '$gateway_name' does not exist\n";
    }
    else
    {
        return $gateways->{lc $gateway_name};
    }
}



sub getDevicesFromGateway
{
    my ($gateway_name, $filter) = @_;

    $gateway_name = uc $gateway_name;

    my $gateway = _getGateway($gateway_name);
    my @devices = ();

    foreach my $channel (@{$gateway->{channels}})
    {
        if(defined $filter)
        {
            if($filter->($channel))
            {
                push @devices, $channel->{name};
            }
        }
        else
        {
            push @devices, $channel->{name};
        }
    }

    @devices = grep(!/$gateway_name/, @devices);
}



sub getPropertyInfo
{
    my ($d, $property) = @_;

    my $info;

    # Some properties don't have get a function, so get request may fail

    eval
    {
        $info = get($d, "$property INFO");
    };

    if($@)
    {
        return undef;
    }

    my %info_hash;
    my @info_lines = split(/\n/, $info);

    foreach my $line (@info_lines)
    {
        my ($key, $value) = split ":", $line;

        $info_hash{$key} = $value;
    }

    return \%info_hash;
}



sub zipTable
{
    my ($time_vector, $ref_vector) = @_;

    my $function = join "", $time_vector->[0], "|", $ref_vector->[0];

    for my $i (1..$#{$time_vector})
    {
        $function .= join "", ",", $time_vector->[$i], "|", $ref_vector->[$i];
    }

    return $function;
}



sub unzipTable
{
    my $function = shift;

    my @time_vector;
    my @ref_vector;

    my @points = split(/,/, $function);

    for my $point (@points)
    {
        my ($time, $ref) = split(/\|/, $point);

        push @time_vector, $time;
        push @ref_vector,  $ref;
    }

    return (\@time_vector, \@ref_vector);
}



sub isInBoot($)
{
    my $fgc_name = shift;

    subscribe($fgc_name);

    my $publication = get_next_publication($fgc_name);

    unsubscribe($fgc_name);

    return ($publication =~ /STATE_PLL:LOCKED/ && $publication =~ /STATE_OP:BOOT/);
}



sub assureBoot($)
{
    my $fgc_name = shift;

    # There's a short time after a reset or a power cycle, when we can't communicate with
    # the device and it's neither in boot, nor in main

    fgc_wait_until( sub { isInBoot($fgc_name) || isInMain($fgc_name) } );

    if(isInMain($fgc_name))
    {
        set($fgc_name, "DEVICE.BOOT", "");
        fgc_wait_until( sub { isInBoot($fgc_name) } );
    }
}



sub isInBootNoSub($)
{
    my $fgc_name = shift;

    my $device = Test::TCP::_get_device_from_name($fgc_name);
    Test::TCP::authorize($device);

    my $response = FGC::Sync::get($device->{gateway}->{socket}, 0, "GW.FGC.STATE[$device->{channel}]");

    return ($response->{value} eq "INBOOT");
}



sub assureBootNoSub($)
{
    my $fgc_name = shift;

    if(isInBootNoSub($fgc_name))
    {
        return;
    }

    set($fgc_name, "DEVICE.BOOT", "");
    fgc_wait_until(sub { isInBootNoSub($fgc_name)});
}



sub assureMainNoSub($)
{
    my $fgc_name = shift;
    my $timeout  = 80;

    if(isInMainNoSub($fgc_name))
    {
        return;
    }

    my $response = sendTest ($fgc_name, "RUNMAIN");
   fgc_wait_until(sub { isInMainNoSub($fgc_name)},$timeout );
}



sub isInMain($)
{
    my $fgc_name = shift;

    eval { get($fgc_name, "BARCODE") };

    if($@)
    {
        return 0;
    }

    return 1;
}



sub assureMain($)
{
    my $fgc_name = shift;

    # There's a short time after a reset or a power cycle, when the PLL is unlocked and
    # the device and it's neither in boot, nor in main

    fgc_wait_until( sub { isInBoot($fgc_name) || isInMain($fgc_name) } );

    if(!isInMain($fgc_name))
    {
        # Event that PLL is now locked, the boot may be not fully initialised, so we neet to sleep for a while,
        # otherwise remote terminal may hang

        sleep(2);

        # Keep sending 'go to main' request until we are in main

        fgc_wait_until( sub { sendCommandNoWait($fgc_name, "0"); return(isInMain($fgc_name)); } );
    }
}



sub isInMainNoSub($)
{
    my $fgc_name = shift;

    my $device = Test::TCP::_get_device_from_name($fgc_name);
    Test::TCP::authorize($device);

    my $response = FGC::Sync::get($device->{gateway}->{socket}, 0, "GW.FGC.STATE[$device->{channel}]");

    return !($response->{value} eq "INBOOT" || $response->{value} eq "OFFLINE" || $response->{value} eq "NOLOCK");
}



# Wait until condition in passed $code returns true.

sub fgc_wait_until(&;$$)
{
    my ( $code, $timeout, $polling_period ) = @_;

    $timeout        ||= $TIMEOUT;
    $polling_period ||= $POLLING_PERIOD;

    my $start_time = time();
    until( $code->() )
    {
        confess "ERROR: wait_until() timeout exceeded" if $timeout and time() - $start_time > $timeout;
        sleep $polling_period;
    }
}



sub syncFgc($)
{
    my $fgc_name = shift;
    my $class    = get($fgc_name, "DEVICE.CLASS_ID");
    my $timeout  = $TIMEOUT;

    # It takes more time to synchronise class 59

    if($class == 59)
    {
        $timeout *= 2;
    }

    if(get($fgc_name, "CONFIG.STATE") eq "UNSYNCED")
    {
        if(get($fgc_name, "CONFIG.MODE") eq "SYNC_FAILED")
        {
            set($fgc_name, "CONFIG.STATE", "RESET");
        }

        # Try to get barcodes. If that fails it means that the main program didn't start yet, for example after a reset.
        # It also means, that SYNC_FGC request will be prepared as part of startup sequence, so we don't have to do it.

        eval { get($fgc_name, "BARCODE") };

        if(not $@)
        {
            # Send sync request. It may happen that the request fails, because we have sent it when the FGC is synchronising
            # In that case we need to catch the exception

            eval { set($fgc_name, "CONFIG.MODE", "SYNC_FGC") };
        }
    }

    fgc_wait_until( sub { get($fgc_name,"CONFIG.STATE") eq 'SYNCHRONISED' && get($fgc_name, "CONFIG.MODE") eq 'SYNC_NONE' }, $timeout);
}



sub setPc($$;$)
{
    my ($fgc_name, $pc_state, $timeout) = @_;

    # Translate any of the short aliases to full state name - GET STATE.PC always returns a full name

    if($pc_state =~ m/^(OF|SA|SB|IL|CY|BK|EC|DT)$/i)
    {
        $pc_state = $pc_state_aliases{$pc_state};
    }

    set($fgc_name, "PC", $pc_state);
    fgc_wait_until( sub { get($fgc_name, "STATE.PC") eq $pc_state }, $timeout );
}



# Class 59 doesn't have PC state machine, but each sub-device have a simplified version.
#
# Class 53 has PC state machine, but it's driven by an external controller. The controller
# can be simulated on the FGC by setting PAL.LINKS.STATUS property. However, it's impossible
# to reach OFF state in this class the state goes from FLT_OFF to STARTING.

sub assureMainInOff($$)
{
    my $fgc_name = shift;
    my $mode_op  = shift;

    assureMain($fgc_name);

    my $class    = get($fgc_name, "DEVICE.CLASS_ID");
    my $platform = get($fgc_name, "DEVICE.PLATFORM_ID");

    # Wait for 'soft OFF' state - OFF or FLT_OFF

    if($class != 59)
    {
        # If the FGC isn't OFF/FLT_OFF

        if(!(get($fgc_name, "STATE.PC") =~ /OFF/))
        {
            if($class == 53)
            {
                set($fgc_name, "PAL.LINKS.STATUS", "FAULT");
                fgc_wait_until( sub { get($fgc_name, "STATE.PC") eq "FLT_OFF" } );
            }
            else
            {
                set($fgc_name, "PC", "OF");
                fgc_wait_until( sub { get($fgc_name, "STATE.PC") =~ /OFF/ } );
            }
        }
    }

    # Synchronise the FGC with the database

    syncFgc($fgc_name);

    # Enter simulation mode

    set($fgc_name, "MODE.OP", $mode_op);
    fgc_wait_until( sub { get($fgc_name, "STATE.OP") eq $mode_op } );

    # Class 59 doesn't have PC state machine nor MEAS properties

    if($class != 59)
    {
        # In simulation, enable measurement simulation

        if($mode_op eq "SIMULATION")
        {
            set($fgc_name, "MEAS.SIM", "ENABLED");
        }

        if($class != 53)
        {
            setPc($fgc_name, "OFF");
        }
    }
}



# Makes sure DSP is alive by reseting the FGC up to 10 times and checking the result of the DSP_IS_ALIVE test.

sub assureDSP($)
{
    my $fgc_name = shift;
    my $resets_count = 0;
    my $response;


    $response = sendTest($fgc_name, "DSP_IS_ALIVE");

    while ($response->{status} ne "OKAY")
    {
        if ($resets_count == $DSP_MAX_RESETS)
        {
            return -1;
        }

        sendGlobalReset($fgc_name);
        $resets_count++;

        $response = sendTest($fgc_name, "DSP_IS_ALIVE");
    }

    $resets_count = 0;
    sleep(1);

    return 1;
}



sub sendTest($$;$)
{
    my $fgc_name = shift;
    my $test     = shift;
    my $arg      = shift;

    # There are tests like the POWER_CONTROL (360) that need an argument (e.g. 0,1,2)
    # since the full command (e.g. 3601) is not available as a boot menu option

    defined(my $command = $FGC::Platform::Fgc3::Auto::boot_menu_options{$test}) or die "Command not defined";

    if (defined $arg)
    {
        $command .= $arg;
    }

    sleep(3);

    my $response = sendCommand($fgc_name, $command);

    return $response;
}



sub sendPowercycle($)
{
    my $fgc_name = shift;

    defined(my $command = $FGC::Platform::Fgc3::Auto::boot_menu_options{"POWER_CYCLE"}) or die "Command not defined";

    sendCommandNoWait($fgc_name, $command);

    sleep(5);

    fgc_wait_until( sub { isInBootNoSub($fgc_name) });

    sleep(1);

    my $response = fgc_wait_until(sub { isInMainNoSub($fgc_name)});

    return $response;
}



sub sendGlobalReset($)
{
    my $fgc_name = shift;

    defined(my $command = $FGC::Platform::Fgc3::Auto::boot_menu_options{"GLOBAL_RESET"}) or die "Command not defined";

    sendCommandNoWait($fgc_name, $command);

    sleep(5);

    fgc_wait_until( sub { isInBootNoSub($fgc_name) });

    sleep(5);
}



sub sendSlowWatchdog($)
{
    my $fgc_name = shift;

    defined(my $command = $FGC::Platform::Fgc3::Auto::boot_menu_options{"TOGGLE_SLOW_WD_TRIG_ENABLE"}) or die "Command not defined";

    sendCommandNoWait($fgc_name, $command);

    sleep(5);

    fgc_wait_until( sub { isInBootNoSub($fgc_name) });

    sleep(5);
}

1;

# EOF
