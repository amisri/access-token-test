#!/usr/bin/perl -w
#
# Name:     sendcmds.pl
# Purpose:  Reads commands from files or STDIN and submits them to a gateway
# Author:   Stephen Page

use FGC::Consts;
use IO::Socket;
use strict;

die "\nUsage: $0 <gateway> <maximum fieldbus address> [<file1> <file2> ...]\n\n" if(@ARGV < 3);

my $gateway     = shift;
my $max_address = shift;
my $cmdcount;
my $socket;

# Function to process a single line

sub processline($)
{
    $_ = shift;

    # Ignore empty lines

    if(/^\s*$/o)
    {
        return;
    }

    # Check whether command should be applied to all FGCs

    if(/\*:/o)
    {
        my $base_command = $_;

        # Send command to all FGCs

        for(1..$max_address)
        {
            # Substitute channel number for '*' in command

            my $command = $base_command;
            $command    =~ s/\*/$_/g;

            print $socket $command;
            $cmdcount++;
        }
    }
    else
    {
        print $socket $_;
        $cmdcount++;
    }
}

# End of functions


# Open a socket to the gateway

$socket = IO::Socket::INET->new("$gateway:".FGC_FGCD_PORT)
            or die "Unable to connect to $gateway: $!\n";

# Check whether the gateway is busy

read($socket, my $status, 1, 0);
die "Gateway $gateway is busy\n" if($status eq '-');

print $socket '+';

# Get the commands and submit them to the gateway

if(@ARGV)  # Filenames were supplied as arguments, read them
{
    while(<>)
    {
        processline($_);
    }
}
else  # No filenames in args, read from STDIN
{
    while(<STDIN>)
    {
        processline($_);
    }
}

# Print responses

my $byte;
my $error = 0;
while(read($socket, $byte, 1, 0))
{
    print $byte;

    # Count down remaining responses

    if($byte eq ';')
    {
        --$cmdcount or close $socket, exit($error);
    }
    elsif($byte eq '!')
    {
        $error = 1;
    }
}
close $socket;
die "Gateway closed socket unexpectedly\n";

# EOF
