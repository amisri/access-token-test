#!/usr/bin/perl -w
#
# Name:     	Converter-Cycling.pl
# Purpose:  	Cycle a power converter with a trimmable sequence (to be encoded directly in this software)
# Author:   	Stephen Page
# Modificator:   Yves Thurel
#		./prog.pl RPMB_.A7.BAY4.1_1 | tee result.txt > to be opened with xls afterwards (tab > change of colomn)
# 2012-01-20 v1	didt variable added. I-err1a/2a added to measure static I-error (when using high dIdt)

use strict;

use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sync;

my $device_name;
my $devices;
my $gateways;
my $num_cycles;
my $password;
my $rbac_last_auth_time = 0;
my $rbac_token;
my $socket;
my $username;

my $didt = 10;
my $endoframpplateau = 10;
my $plateau = 60;
my $i0 = 10;
my $i1 = 550;
my $i2 = -550;

# Check RBAC token

sub check_rbac_token()
{
    do
    {
        # Check whether RBAC token is less than one hour from expiry

        if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
        {
            # Check whether it is at least one minute since last authentication

            if($rbac_last_auth_time < (time - 60))
            {
                warn "Getting new RBAC token at ", scalar(localtime), "\n\n";

                get_rbac_token() or warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }

            if(defined($rbac_token) && $rbac_token->{ExpirationTime} >= time)
            {
                # Set RBAC token on gateway

                my $response;
                eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$rbac_token->{binary}, 1) };
                die "Failed to send command to set RBAC token on gateway\n"     if($@);
                die "Error setting RBAC token on gateway: $response->{value}\n" if($response->{error});
            }
            else
            {
                sleep(60);
            }
        }
    } while(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
}

# Authenticate and obtain RBAC token

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate($username, $password);
    return(0) if(!defined($binary_token));

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return(1);
}

die "$0 software tests a defined sequence n times\n Sequence: OFF > IL > LHC precycle at ${didt}A/s > OFF \n At each step of the sequence, converter state is checked\n If converter state faulty, scripts stops indicating sequence number\n This sequence can be modified directly in the script\n\n Usage $0 <device name> <num_cycles>\n\n" if(@ARGV != 2);
($device_name, $num_cycles) = @ARGV;

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $device = $devices->{$device_name};
die "Unknown device $device_name\n" if(!defined($device));

# Prompt for username

print STDERR "\nUsername: ";
chomp($username = <STDIN>);

# Prompt for password

print STDERR "Password: ";
system("stty -echo");
chomp($password = <STDIN>);
system("stty echo");
print "\n\n";

# Flush stdout immediately

$| = 1;

# Connect to gateway

$socket = FGC::Sync::connect($device->{gateway});

my @sequence =
(
    {
        action  => 'fastabortunsafereset',
        wait    => 02,
    },
    {
        action  => 'off',
        wait    => 10,
    },
    {
        action  => 'on',
        wait    => 60,
    },

    {
        action  => 'off',
        wait    => 60,
    },
    {
        action  => 'message',
    },
      
);


my $i_ref = 0;
my $cycle_count = 0;

for(my $i = 0 ; ; $i = ($i + 1) % @sequence)
{
    if($i == 0)
    {
        $cycle_count++;
        last if($cycle_count > $num_cycles);
    }

    my $element = $sequence[$i];

    check_rbac_token();

    if($element->{action} eq 'on')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'MODE.PC', 'ON_STANDBY');
        if($response->{error})
        {
            warn "Error setting MODE.PC to ON_STANDBY: $response->{value}\n";
            sleep(60);
            next;
        }

        sleep(15);

        $response = FGC::Sync::set($socket, $device->{channel}, 'MODE.PC', 'IDLE');
        if($response->{error})
        {
            warn "Error setting MODE.PC to IDLE: $response->{value}\n";
            sleep(60);
            next;
        }
    }
    elsif($element->{action} eq 'off')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'MODE.PC', 'OFF');
        if($response->{error})
        {
            warn "Error setting MODE.PC to OFF: $response->{value}\n";
            sleep(60);
            next;
        }

        sleep(10);
    }
    elsif($element->{action} eq 'fastabortunsafereset')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'VS.FABORT_UNSAFE', 'RESET');
    }
    elsif($element->{action} eq 'ref')
    {
        $i_ref = $element->{current};

        my $response = FGC::Sync::set($socket, $device->{channel}, 'REF', "NOW,$i_ref,,$didt");
        if($response->{error})
        {
            warn "Error setting reference to $i_ref: $response->{value}\n";
            sleep(60);
            next;
        }

        sleep(1);

        $response = FGC::Sync::get($socket, $device->{channel}, 'REF.REMAINING');
        if($response->{error})
        {
            warn "Error getting REF.REMAINING: $response->{value}\n";
            sleep(60);
            next;
        }

        sleep(int($response->{value}) + 1);
    }
    elsif($element->{action} eq 'check_dim')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'DIAG.DIG');

        for my $line (split(',', $response))
        {
            if($line =~ /:\S+\s*:[^:]*:[^:]*$/)
            {
                print "$line\n";
            }
        }
        sleep(1);
    }
    elsif($element->{action} eq 'check_dim_9')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'DIAG.DIG[9]',);
        print "DIM-9\t$cycle_count\tPRECHARGE-FAULT\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'check_dim_23')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'DIAG.DIG[23]');
        print "DIM-23\t$cycle_count\tAUX_PSU-FAULT\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'check_state')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'STATE.PC');
        if($response->{error})
        {
            warn "Error getting STATE.PC: $response->{value}\n",;
            sleep(60);
            next;
        }

        if($response->{value} eq 'FLT_OFF')
        {
            FGC::Sync::disconnect($socket);
            die "$device_name : Converter tripped after $cycle_count cycles\n";
        }
    }
    elsif($element->{action} eq 'reset_Ierr')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'meas.max.abs_err zero');
        sleep(1);
    }
    elsif($element->{action} eq 'check_Ierr-0')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'meas.max.abs_err zero');
        print "Ierr-0\t$cycle_count\tMax.Ierror [${i0}A > -${i0}A at ${didt}A/s]\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'check_Ierr-1')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'meas.max.abs_err zero');
        print "Ierr-1\t$cycle_count\tMax.Ierror [${i0}A > ${i1}A at ${didt}A/s]\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'check_Ierr-1a')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'meas.max.abs_err zero');
        print "Ierr-1a\t$cycle_count\tMax.Ierror [${i1}A static during ${plateau}s]\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'check_Ierr-2')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'meas.max.abs_err zero');
       print "Ierr-2\t$cycle_count\tMax.Ierror [-${i0}A > ${i2}A at ${didt}A/s]\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'check_Ierr-2a')
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, 'meas.max.abs_err zero');
        print "Ierr-2a\t$cycle_count\tMax.Ierror [${i2}A static during ${plateau}s]\t$response->{value}\n";
        sleep(1);
    }
    elsif($element->{action} eq 'message')
    {
        print "Info\t$cycle_count\t$device_name : $cycle_count / $num_cycles cycles being performed.\t$cycle_count\n";
    }
    else # Unknown action
    {
        FGC::Sync::disconnect($socket);
        die "Unknown action $element->{action}\n";
    }

    if(defined($element->{wait}))
    {
        sleep($element->{wait});
    }
}
FGC::Sync::disconnect($socket);

# EOF
