#!/usr/bin/perl -w
#
# Name:     log_max_abs_err.pl
# Purpose:  Log maximum current error
# Author:   Stephen Page
# 		modified by ythurel
#		./prog.pl RPMB_.A7.BAY4.1_1 | tee result.cvs

# version  2015-05-22:
# > align the new commands

use POSIX qw(strftime);
use strict;

use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sync;

my $device_name;
my $devices;
my $gateways;
my $password;
my $socket;
my $username;
my $rbac_token;
my $rbac_last_auth_time = 0;

# Check RBAC token

sub check_rbac_token()
{
    do
    {
        # Check whether RBAC token is less than one hour from expiry

        if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
        {
            # Check whether it is at least one minute since last authentication

            if($rbac_last_auth_time < (time - 60))
            {
                warn "Getting new RBAC token at ", scalar(localtime), "\n\n";

                get_rbac_token() or warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }

            if(defined($rbac_token) && $rbac_token->{ExpirationTime} >= time)
            {
                # Set RBAC token on gateway

                my $response;
                eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$rbac_token->{binary}, 1) };
                die "Failed to send command to set RBAC token on gateway\n"     if($@);
                die "Error setting RBAC token on gateway: $response->{value}\n" if($response->{error});
            }
            else
            {
                sleep(60);
            }
        }
    } while(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
}

# Authenticate and obtain RBAC token

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate($username, $password);
    return(0) if(!defined($binary_token));

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return(1);
}

sub read_values($)
{
    my ($device) = @_;

    my $response;
    my $time = strftime("%d/%m/%Y %H:%M:%S", localtime());

    $response           = FGC::Sync::get($socket, $device->{channel}, 'REF.V');
    my $vref   	 	= $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'MEAS.V');
    my $vmeas   	 	= $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'REF.I');
    my $iref   	 	= $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'MEAS.I');
    my $imeas   	 	= $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'MEAS.MAX.ABS_ERR ZERO');
    my $max_abs_error   = $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'MEAS.MAX.I_EARTH ZERO');
    my $max_i_earth   = $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'MEAS.MAX.U_LEADS[0] ZERO');
    my $max_u_lead_pos   = $response->{value};

    $response           = FGC::Sync::get($socket, $device->{channel}, 'MEAS.MAX.U_LEADS[1] ZERO');
    my $max_u_lead_neg   = $response->{value};

    print "$device_name,$time,$vref,$vmeas,$iref,$imeas,$max_abs_error,$max_i_earth,$max_u_lead_pos,$max_u_lead_neg\n";
}

die "Usage1 $0 <device name>\n Usage2 $0 <device_name> | tee result.cvs\n" if(@ARGV != 1);
($device_name) = @ARGV;

# Prompt for username

print STDERR "\nUsername: ";
chomp($username = <STDIN>);

# Prompt for password

print STDERR "Password: ";
system("stty -echo");
chomp($password = <STDIN>);
system("stty echo");
print STDERR "\n\n";

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $device = $devices->{$device_name};
die "Unknown device $device_name\n" if(!defined($device));

# Flush stdout immediately

$| = 1;

# Connect to gateway

$socket = FGC::Sync::connect($device->{gateway});

print "Device,Time,V_REF[V],V_MEAS[V],I_REF[A],I_MEAS[A],MAX_ABS_ERROR[A],MAX_I_EARTH[A],MAX_U_LEAD_POS[V],MAX_U_LEAD_NEG[V]\n";

my @sequence =  (
                    {
                        action  => 'null',
                    },
                );

for(my $i = 0 ; ; $i = ($i + 1) % @sequence)
{
    my $element = $sequence[$i];

    check_rbac_token();
    read_values($device);
    sleep(60);
}
FGC::Sync::disconnect($socket);

# EOF

