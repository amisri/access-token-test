#!/usr/bin/perl -w
#
# Name:     fgcd_survey.pl
# Purpose:  Survey FGCD instances and notify experts if they are down
# Author:   Stephen Page

use FGC::Consts;
use FGC::StatSrv;
use Mail::Sendmail;
use Net::Ping;
use Socket;
use strict;
use Sys::Hostname;

use constant SURVEY_CONF_DIR => "/user/pclhc/etc/fgcd_survey";
use constant SURVEY_IMAGE_FILE => SURVEY_CONF_DIR."/image";
use constant SURVEY_IGNORE_DIR => SURVEY_CONF_DIR."/ignore";
use constant MAIL_ADDRESSES => qw/FGCD-devs@cern.ch/;
use constant GW_DOWNTIME_TOLERANCE => 4; # Minimum number of consecutive unreachable/offline detections for a gateway to be reported

my %gw_down_states; # Contains gateways that have been identified as down (offline or unreachable) and for which a report / alarm will be or has been sent
my %gw_downtime_counters; # Counts number of consecutive times a given gateway has been identified as down

sub check_fgcd_status($$)
{
    my ($gateways, $receive_time) = @_;

    # Check whether data for each gateway is up-to-date

    my $pinger = Net::Ping->new("syn", 10);
    my @recovered_gateways;
    my @unresponsive_gateways;

    for my $gateway (values(%$gateways))
    {
        # Check whether the gateway should be ignored

        if(-e SURVEY_IGNORE_DIR."/$gateway->{hostname}")
        {
            # Ignore the gateway

            delete($gw_down_states{$gateway->{hostname}});
            next;
        }

        # Check whether the gateway's data has been updated

        if($gateway->{recv_time_sec} > ($receive_time - (FGC_STATSRV_UPDATE_SECS * 20))) # Gateway data updated (< 1.30 sec)
        {
            # Gateway is alive.
            $gw_downtime_counters{$gateway->{hostname}} = 0;

            # Check whether the gateway was down in the previous iteration
            if(defined($gw_down_states{$gateway->{hostname}}))
            {
                print("$gateway->{hostname} recovered from down state. A report will be sent.\n");

                delete($gw_down_states{$gateway->{hostname}});
                push(@recovered_gateways, $gateway);
            }
        }
        else # Gateway data not updated. Looks like the gateway is down (offline or unreachable).
        {
            # Increment number of consecutive times the GW has been identified as down
            if(!defined($gw_downtime_counters{$gateway->{hostname}}))
            {
                $gw_downtime_counters{$gateway->{hostname}} = 1;
            }
            else
            {
                $gw_downtime_counters{$gateway->{hostname}} += 1;
            }

            print("$gateway->{hostname} identified as down ($gw_downtime_counters{$gateway->{hostname}} times in a row)\n");

            # Ensure GW has been identified as down for N consecutive times before triggering a report
            if($gw_downtime_counters{$gateway->{hostname}} < GW_DOWNTIME_TOLERANCE)
            {
                next;
            }

            # If no report has been triggered for the gateway yet
            if(!defined($gw_down_states{$gateway->{hostname}}))
            {
                print("$gateway->{hostname} exceeded downtime tolerance counter ($gw_downtime_counters{$gateway->{hostname}} / ".GW_DOWNTIME_TOLERANCE."). A report will be sent.\n");

                # Ping gateway with a TCP SYN packet to find out whether it's offline or unreachable and
                # add it to the list of unresponsive gateways which are going to be reported

                $pinger->ping($gateway->{hostname});
                push(@unresponsive_gateways, $gateway);
            }
        }
    }

    # Wait to allow ping responses to arrive

    if(@unresponsive_gateways)
    {
        sleep(10);
    }

    # Check the result of gateway pings to determine whether the unresponsive gateway is just offline or unreachable

    for my $gateway (@unresponsive_gateways)
    {
        $gw_down_states{$gateway->{hostname}} = $pinger->ack($gateway->{hostname}) ? "offline" : "unreachable";
    }
    $pinger->close();

    # update survey image file if new unresponsive or recovered gateways have been detected
    if(0 < scalar(@unresponsive_gateways) + scalar(@recovered_gateways))
    {
        write_image_file(\%gw_down_states);
    }

    # Construct e-mail report

    my $report = "";

    # Add details of unresponsive gateways

    if(@unresponsive_gateways)
    {
        $report .= "\n".scalar(@unresponsive_gateways)." unresponsive gateway".(@unresponsive_gateways > 1 ? "s" : "").":\n\n";

        for my $gateway (@unresponsive_gateways)
        {
            $report .= "$gateway->{hostname} $gw_down_states{$gateway->{hostname}}\n";
        }
    }

    # Add details of recovered gateways

    if(@recovered_gateways)
    {
        $report .= "\n".scalar(@recovered_gateways)." recovered gateway".(@recovered_gateways > 1 ? "s" : "").":\n\n";

        for my $gateway (@recovered_gateways)
        {
            $report .= "$gateway->{hostname}\n";
        }
    }

    # Send the report if it has content

    if($report ne "")
    {
        print $report;
        print "---------\n";

        my $username = $ENV{LOGNAME} || $ENV{USER};
        my %mail = (
                        "Content-type"  => "text/plain",
                        From            => "$username@"."mail.cern.ch",
                        To              => join(", ", MAIL_ADDRESSES),
                        Subject         => "fgcd_survey report",
                   );

        # Construct HTML e-mail report

        $mail{Message} = $report;

        if(!sendmail(%mail)) # Mail failed to send
        {
            warn "\t\tFailed to send mail notification\n";
        }
    }
}

# Write survey image file. Content equals the down gateways and their state, tracked in the given hash
# %myhash = {'cfv-287-rmug5' => 'offline', 'cfv-ba1-rsps1' => 'unreachable'}
# translates to this file:
# cfv-287-rmug5:offline
# cfv-ba1-rsps1:unreachable
sub write_image_file(;$)
{
    my ($gw_to_state) = @_;
    my @gw_names_and_states = sort(map {"$_:$gw_to_state->{$_}"} keys(%$gw_to_state));

    open(my $fh, '>', SURVEY_IMAGE_FILE) or die 'Could not create survey image file at '.SURVEY_IMAGE_FILE;

    print("\nWriting fgcd_survey image file ...\n");

    for my $gw_and_state (@gw_names_and_states)
    {
        print($gw_and_state."\n");
        print $fh $gw_and_state."\n";
    }

    close $fh;

    print("Finished writing to fgcd_survey image file!\n");
}

# End of functions

# Create SURVEY IMAGE FILE if not exists
unless(-e SURVEY_IMAGE_FILE)
{
    write_image_file();
}

# Survey gateways

while(1)
{
    # Connect to the status server

    my $socket;
    eval { $socket = FGC::StatSrv::connect() };
    unless($@)
    {
        my $gateways;
        eval{$gateways = FGC::StatSrv::getallstatus($socket, 1);};

        if($@) # An error occurred
        {
            warn "Failed to read status\n";
        }
        else
        {
            check_fgcd_status($gateways, scalar(time));
        }

        # Disconnect from the status server

        FGC::StatSrv::disconnect($socket);
    }
    else
    {
        warn "Failed to connect to status server\n";
    }

    sleep(FGC_STATSRV_UPDATE_SECS);
}

# EOF
