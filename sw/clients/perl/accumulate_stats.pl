#!/usr/bin/perl -w
#
# Name:     accumulate_stats.pl
# Purpose:  Accumulate statistics from zeroed FGC Monitor device lists
# Author:   Stephen Page
# Notes:    Currently contains code specific to the MEM property

use FGC::DeviceList;
use FGC::Names;
use POSIX qw(strftime);
use strict;

die "Usage: $0 <device list> <statistics file>\n" if(@ARGV != 2);
my ($device_list_name, $stats_file_name) = @ARGV;

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $max_old_stats = 0; # Maximum number of old statistic values

# Read existing values from statistics file

my %locations;  # Location data
my @old_dates;  # Dates of previous values
my @old_totals; # Totals of previous values

if(-e $stats_file_name)
{
    open(STATS_FILE, '<', $stats_file_name)
        or die "Failed to read statistics file $stats_file_name : $!\n";

    # Read headings

    $_ = <STATS_FILE>;
    chomp;
    (undef, undef, @old_dates) = split(',');

    # Read device lines

    while(<STATS_FILE>)
    {
        chomp;
        last if(/^$/); # Stop if a blank line is reached

        my ($device_name, $location, @stats) = split(',');

        my $device = $devices->{$device_name};
        if(defined($device))
        {
            $device->{old_stats} = \@stats;

            # Check whether this device has the maximum number of statistics values

            if(@stats > $max_old_stats)
            {
                $max_old_stats = @stats;
            }
        }
    }

    # Read location lines

    while(<STATS_FILE>)
    {
        chomp;
        last if(/^$/); # Stop if a blank line is reached

        my (undef, $location, @stats)       = split(',');
        $locations{$location}->{name}       = $location;
        $locations{$location}->{old_stats}  = \@stats;
    }

    # Read totals

    if(($_ = <STATS_FILE>) ne eof)
    {
        chomp;
        (undef, undef, @old_totals) = split(',');
    }

    close(STATS_FILE);
}

# Read device list

my $list_devices = FGC::DeviceList::read($devices, $device_list_name)
    or die "Failed to read device list $device_list_name : $!\n";

# Add new statistics to records

for my $device (@$list_devices)
{
    if($device->{response} !~ s/NUM_ERRORS:(\d+)\$.*/$1/)
    {
        # Response does not match expected format

        next;
    }

    $device->{new_stat} = defined($device->{old_stats}) ? 
                          $device->{old_stats}->[-1] + $device->{response} :
                          $device->{response};
}

# Write new statistics file

open(STATS_FILE, '>', $stats_file_name)
    or die "Failed to write statistics file $stats_file_name : $!\n";

my $date = strftime "%d/%m/%Y", localtime;

# Write headings

print STATS_FILE "Device,Location", @old_dates ? ',' : '',
                 join(',', @old_dates), ",$date\n";

# Write device rows

my $total;

for my $device (sort { $a->{location} cmp $b->{location} || $a->{name} cmp $b->{name} }
                grep { defined($_->{old_stats}) || defined($_->{new_stat}) }
                values(%$devices))
{
    my $stat = defined($device->{new_stat}) ? $device->{new_stat} : $device->{old_stats}->[-1];

    if(defined($device->{old_stats}))
    {
        print STATS_FILE "$device->{name},$device->{location}",
                         ',', join(',', @{$device->{old_stats}}),
                         ',', $stat, "\n";
    }
    else # This is the first statistic for the device
    {
        print STATS_FILE "$device->{name},$device->{location}",
                         ',' x $max_old_stats,
                          ',', $stat, "\n";
    }

    # Add stats to totals

    $locations{$device->{location}}->{name}      = $device->{location};
    $locations{$device->{location}}->{new_stat} += $stat;
    $total += $stat;
}
print STATS_FILE "\n";

# Write location rows

for my $location (sort { $a->{name} cmp $b->{name} } (values(%locations)))
{
    my $stat = defined($location->{new_stat}) ? $location->{new_stat} : $location->{old_stats}->[-1];

    if(defined($location->{old_stats}))
    {
        print STATS_FILE ",$location->{name}",
                         ',', join(',', @{$location->{old_stats}}),
                         ',', $stat, "\n";
    }
    else # This is the first statistic for the location
    {
        print STATS_FILE ",$location->{name}",
                         ',' x $max_old_stats,
                          ',', $stat, "\n";
    }
}
print STATS_FILE "\n";

# Write overall totals

if(@old_totals)
{
    print STATS_FILE ",Total",
                     ',', join(',', @old_totals),
                     ',', $total, "\n";
}
else # The totals are being written for the first time
{
    print STATS_FILE ',Total',
                     ',' x $max_old_stats,
                     ',', $total, "\n";
}
close(STATS_FILE);

# EOF
