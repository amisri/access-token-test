/*
 *  Filename: libtest.c
 *
 *  Purpose:  Functions for the Function Generator Controller client library test (libtest)
 *
 *  Author:   Stephen Page
 */

#include <fgc.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#define ASYNC
#define DEVICE      "PCGW02"
#define NUM_CMDS    10
#define PROPERTY    "time.now"

struct timeval start, end;

void callback(struct cmd* command)
{
    static int cmd_count = 0;

    cmd_count++;

    if(command->error)
    {
        printf("ERROR %"PRIuPTR" %s\n", command->tag, command->value);
    }
    else
    {
        printf("OKAY  %"PRIuPTR" %s\n", command->tag, command->value);
    }

    // Check whether this is the callback for the last command

    if(cmd_count == NUM_CMDS)
    {
        // Print statistics

        gettimeofday(&end, NULL);
        printf("\n%i commands  START %li.%06li  END %li.%06li\n",
               NUM_CMDS,
               (long)start.tv_sec, (long)start.tv_usec,
               (long)end.tv_sec,   (long)end.tv_usec);

        fgcCleanUp();
        exit(0);
    }
}

int main(int argc, char **argv)
{
    struct cmd  command[NUM_CMDS];
    int         i;
    char        password[FGC_MAX_PASS_LEN + 1];
    int         return_val;
    char        username[FGC_MAX_USER_LEN + 1];

    // Check arguments

    if(argc <= 1)
    {
        fprintf(stderr, "\nUsage: %s <username>\n\n", argv[0]);
        return(1);
    }
    strncpy(username, argv[1], FGC_MAX_USER_LEN);
    username[sizeof(username) - 1] = '\0';

    // Prompt for password

    strncpy(password, getpass("Password:"), FGC_MAX_PASS_LEN);
    password[sizeof(password) - 1] = '\0';

    // Initialise library

    if(fgcInit(username, password))
    {
        fprintf(stderr, "Unable to initialise FGC library\n");
        return(1);
    }

    // Initialise commands

    for(i = 0 ; i < NUM_CMDS ; i++)
    {
        strcpy(command[i].property, PROPERTY);
        strcpy(command[i].device,   DEVICE);
        command[i].callback = callback;
    }

    // Get start time

    gettimeofday(&start, NULL);

    // Send commands

#ifdef ASYNC
    // Asynchronous test

    for(i = 0 ; i < NUM_CMDS ; i++)
    {
        if((return_val = fgcGet(&command[i])))
        {
            fprintf(stderr, "Error on submit: %i\n", return_val);
        }
    }
    pause();
#else
    // Synchronous test

    for(i = 0 ; i < NUM_CMDS ; i++)
    {
        if((return_val = fgcGetSync(&command[i])))
        {
            fprintf(stderr, "Error on submit: %i\n", return_val);
        }
        callback(&command[i]);  
    }
#endif

    return(0);
}

// EOF
