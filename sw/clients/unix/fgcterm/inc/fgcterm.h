/*
 *  Filename: fgcterm.h
 *
 *  Purpose:  Declarations for the Function Generator Controller terminal
 *
 *  Author:   Stephen Page
 */

#ifndef FGCTERM_H
#define FGCTERM_H

#include <stdio.h>

#include "fgc_consts_gen.h"

#ifdef FGCTERM
#define FGCTERM_EXT
#else
#define FGCTERM_EXT extern
#endif

#define FGC_MAX_PASS_LEN        15

#define PORT                    FGC_GW_PORT

#define RTERM_DISCONNECT_CHAR   0x18

#define TELNET_ESC              0xFF

#define TELNET_WILL             0xFB
#define TELNET_WONT             0xFD
#define TELNET_DO               0xFC
#define TELNET_DONT             0xFE

#define TELNET_LINE             0x22

// Static functions

#ifdef FGCTERM
static void CleanUp(int sig_num);
static int  ConnectToSvr(char *hostname);
static void HandleEscape(void);
static void Receive(void);
#endif

// External functions

// Struct containing global variables

struct fgcterm
{
    char            *initial_rterm;
    int             die;
    FILE            *input;
    FILE            *logfile;
    int             recv_error;
    int             sock;
    int             receive_thread_running;
    pthread_t       receive_thread;
    struct termios  line_mode;
    struct termios  raw_mode;
} fgcterm;

#endif

// EOF
