/*
 *  Filename: fgcterm.cpp
 *
 *  Purpose:  The Function Generator Controller terminal client
 *
 *  Author:   Stephen Page
 */

#include <arpa/inet.h>
#include <cmw-rbac/LoginContext.h>
#include <cmw-rbac/Exception.h>
#include <cmw-rbac/TokenClass.h>
#include <limits.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

extern char *optarg;

using namespace std;

#define FGCTERM

#include "fgc_consts_gen.h"
#include "fgcterm.h"

int main(int argc, char **argv)
{
    char            c;
    static char     command[FGC_MAX_CMD_LEN + 1 + FGC_MAX_VAL_LEN + 1];
    uint32_t        command_length;
    FILE            *file;
    char            *hostname;
    uint32_t        i;
    char            passwd_buffer[128];
    struct passwd   passwd_entry;
    struct passwd   *passwd_entry_p;
    char            password[FGC_MAX_PASS_LEN + 1] = "";
    uint32_t        rbac_token_length = 0;
    static uint8_t  rbac_token_raw[FGC_MAX_VAL_LEN];
    char            *username;

    fgcterm.die                     = 0;
    fgcterm.initial_rterm           = NULL;
    fgcterm.input                   = stdin;
    fgcterm.recv_error              = 0;
    fgcterm.receive_thread_running  = 0;
    fgcterm.sock                    = -1;

    // Process command-line options

    while((c = getopt(argc, argv, "?f:hl:p:r:t:")) != -1)
    {
        switch(c)
        {
            case '?':
            case 'h':
                fprintf(stderr, "\nUsage: %s [ -f <file>] [-p <password file>] [-r <channel>] [-t <token file>] <gateway> [<username>]\n\n%s\n",
                                argv[0],
                                "    -f <file>              Send file instead of interactive terminal\n"
                                "    -l <log file>          File to which to log terminal\n"
                                "    -p <password file>     File containing password\n"
                                "    -r <channel>           Start a remote terminal to channel\n"
                                "    -t <token file>        File containing RBAC token\n"
                       );
                exit(1);

            case 'f':
                if(!(fgcterm.input = fopen(optarg, "r")))
                {
                    perror("Open file");
                    exit(1);
                }
                break;

            case 'l':
                if(!(fgcterm.logfile = fopen(optarg, "w")))
                {
                    perror("Open log file");
                    exit(1);
                }
                break;

            case 'p':

                // Read password from file

                if(!(file = fopen(optarg, "r")))
                {
                    perror("Open password file");
                    exit(1);
                }
                password[fread(password, 1, sizeof(password) - 1, file)] = '\0';
                fclose(file);
                break;

            case 'r':
                fgcterm.initial_rterm = optarg;
                break;

            case 't':

                // Read RBAC token from file

                if(!(file = fopen(optarg, "r")))
                {
                    perror("Open RBAC token file");
                    exit(1);
                }
                rbac_token_length = fread(rbac_token_raw, 1, sizeof(rbac_token_raw), file);
                fclose(file);
                break;
        }
    }

    // Check that hostname was specified as an argument

    if(!(argc - optind))
    {
        fprintf(stderr, "You must specify a hostname.\n");
        exit(1);
    }
    hostname = argv[optind++];

    // Check whether username was specified as an argument

    if(argc - optind)
    {
        username = argv[optind++];
    }
    else // Username not specified
    {
        // Get current user

        if(getpwuid_r(geteuid(),
                      &passwd_entry,
                      passwd_buffer, sizeof(passwd_buffer),
                      &passwd_entry_p))
        {
            fprintf(stderr, "Failed to get username.\n");
            exit(1);
        }
        username = passwd_entry.pw_name;
    }

    // Check whether a token has been read

    if(!rbac_token_length)
    {
        // Get password if not known

        if(!password[0])
        {
            // Check whether password was specified as an argument

            if(argc - optind)
            {
                strncpy(password, argv[optind], FGC_MAX_PASS_LEN);
                password[FGC_MAX_PASS_LEN] = '\0';

                // Clear password in process name

                for(i = strlen(argv[optind]) ; i ; i--)
                {
                    argv[optind][i - 1] = '\0';
                }
                optind++;
            }
            else // Password not specified
            {
                // Prompt for password

                strncpy(password, getpass("Password:"), FGC_MAX_PASS_LEN);
                password[FGC_MAX_PASS_LEN] = '\0';
            }
        }

        // Perform RBAC authentication and obtain token

        try
        {
            auto_ptr<cmw::rbac::TokenClass> rbac_token = cmw::rbac::LoginContext::login("", username, password);

            // Copy raw token

            const vector<signed char> token_vector = rbac_token.get()->getRbacServerResponse();
            rbac_token_length = token_vector.size();

            if(rbac_token_length > sizeof(rbac_token_raw))
            {
                fprintf(stderr, "RBAC token too large\n");
                exit(1);
            }
            memcpy(rbac_token_raw, token_vector.data(), rbac_token_length);
        }
        catch(const cmw::rbac::AuthenticationFailure &e)
        {
            // Zero password in memory

            memset(password, 0, sizeof(password));

            fprintf(stderr, "RBAC authentication failed\n");
            exit(1);
        }

        // Zero password in memory

        memset(password, 0, sizeof(password));
    }

    // Initialise terminal variables

    tcgetattr(STDIN_FILENO, &fgcterm.line_mode);
    fgcterm.raw_mode = fgcterm.line_mode;

    fgcterm.raw_mode.c_iflag       &= ~(INLCR|ICRNL|IXON);
    fgcterm.raw_mode.c_oflag       &= ~(OPOST);
    fgcterm.raw_mode.c_lflag        = 0;
    fgcterm.raw_mode.c_cc[VMIN]     = 1;
    fgcterm.raw_mode.c_cc[VTIME]    = 0;

    if(ConnectToSvr(hostname) != 0)
    {
        fprintf(stderr, "Unable to connect to %s:%i\n", hostname, PORT);
        exit(1);
    }

    // Catch signals to clean-up

    signal(SIGHUP,  CleanUp);
    signal(SIGINT,  CleanUp);
    signal(SIGPIPE, CleanUp);
    signal(SIGQUIT, CleanUp);
    signal(SIGTERM, CleanUp);

    // Check that gateway is not busy

    recv(fgcterm.sock, &c, 1, 0);
    if(c != '+')
    {
        fprintf(stderr, "Gateway %s is busy\n", hostname);
        CleanUp(0);
    }
    send(fgcterm.sock, "+", 1, 0);

    printf("Connected to gateway %s\n", hostname);

    // Send RBAC token

    command_length = snprintf(command, sizeof(command), "! s :client.token ");
    command[command_length++] = 0xFF;

    // Copy raw RBAC token to command

    i = htonl(rbac_token_length);
    memcpy(&command[command_length], &i, sizeof(rbac_token_length));
    command_length += sizeof(rbac_token_length);

    memcpy(&command[command_length], &rbac_token_raw, rbac_token_length);
    command_length += rbac_token_length;

    send(fgcterm.sock, command, command_length, 0);

    while(recv(fgcterm.sock, &c, 1, 0) && c != ';')
    {
        if(c == '!')
        {
            fprintf(stderr, "Gateway rejected RBAC token\n");
            CleanUp(0);
        }
    }

    // Attempt to connect to remote terminal if command-line option specified

    if(fgcterm.initial_rterm)
    {
        command_length = snprintf(command, sizeof(command),
                                  fgcterm.input == stdin ? "! s :client.rterm %s\n" : "! s :client.rtermlock %s\n",
                                  fgcterm.initial_rterm);
        send(fgcterm.sock, command, command_length, 0);

        while(recv(fgcterm.sock, &c, 1, 0) && c != ';')
        {
            if(c == '!')
            {
                fprintf(stderr, "Remote terminal request denied\n");
                CleanUp(0);
            }
        }
    }

    // Spawn receive thread

    if(pthread_create(&fgcterm.receive_thread,
                      NULL,
                      (void *(*)(void *))Receive,
                      0))
    {
        perror("pthread_create");
        CleanUp(0);
    }

    // Read bytes and send over socket

    while(fread(&c, 1, 1, fgcterm.input) && !fgcterm.die)
    {
        // Check whether reading from stdin

        if(fgcterm.input == stdin)
        {
            switch(c)
            {
                case RTERM_DISCONNECT_CHAR:
                    if(fgcterm.initial_rterm)
                    {
                        CleanUp(SIGQUIT);
                    }
                    break;

                default:
                    send(fgcterm.sock, &c, 1, 0);
                    break;
            }
        }
        else // Reading from a file
        {
            send(fgcterm.sock, &c, 1, 0);
        }
    }

    // Wait for disconnect character if input is a file to ensure that it is fully transmitted

    if(fgcterm.input != stdin)
    {
        while(getchar() != RTERM_DISCONNECT_CHAR);
    }

    CleanUp(SIGQUIT);
}

void Receive(void)
{
    uint8_t c;

    fgcterm.receive_thread_running = 1;

    // Read characters from socket and write to stdout

    while(!fgcterm.recv_error && (recv(fgcterm.sock, &c, 1, 0)))
    {
        // Write character to stdout if it is not a telnet escape

        switch(c)
        {
            case TELNET_ESC:
                HandleEscape();
                break;

            default:
                write(STDOUT_FILENO, &c, 1);

                if(fgcterm.logfile)
                {
                    fputc(c, fgcterm.logfile);
                    fflush(fgcterm.logfile);
                }

                break;
        }
    }
    fprintf(stderr, "\33cGateway closed connection unexpectedly.\r");
    CleanUp(0);
}

void HandleEscape(void)
{
    uint8_t buf[2];

    if(!(recv(fgcterm.sock, &buf[0], 1, 0) &&
         recv(fgcterm.sock, &buf[1], 1, 0)))
    {
        fgcterm.recv_error = 1;
        return;
    }

    switch(buf[0])
    {
        case TELNET_WILL:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // End session if remote terminal option was specified

                    if(fgcterm.initial_rterm)
                    {
                        CleanUp(0);
                    }
                    else
                    {
                        // Enable line-mode

                        tcsetattr(STDIN_FILENO, 0, &fgcterm.line_mode);
                    }

                    break;
            }
            break;

        case TELNET_DO:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // End session if remote terminal option was specified

                    if(fgcterm.initial_rterm)
                    {
                        CleanUp(0);
                    }
                    else
                    {
                        // Enable line-mode

                        tcsetattr(STDIN_FILENO, 0, &fgcterm.line_mode);
                    }

                    break;
            }
            break;

        case TELNET_WONT:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // Disable line-mode

                    tcsetattr(STDIN_FILENO, 0, &fgcterm.raw_mode);

                    break;
            }
            break;

        case TELNET_DONT:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // Disable line-mode

                    tcsetattr(STDIN_FILENO, 0, &fgcterm.raw_mode);

                    break;
            }
            break;
    }
}

int ConnectToSvr(char *hostname)
{
    struct hostent      *host;
    struct sockaddr_in  sin;

    memset((void *)&sin, 0, sizeof(sin));

    if((host = gethostbyname(hostname)) == NULL)
    {
        perror("gethostbyname");
        return(1);
    }

    sin.sin_addr   = *((struct in_addr *)host->h_addr);
    sin.sin_family = AF_INET;
    sin.sin_port   = htons(PORT);

    if((fgcterm.sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        return(2);
    }

    if(connect(fgcterm.sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("connect");
        return(3);
    }

    return(0);
}

void CleanUp(int sig_num)
{
    // Ignore further signals

    signal(SIGHUP,  SIG_IGN);
    signal(SIGINT,  SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    signal(SIGTERM, SIG_IGN);

    if(!fgcterm.die)
    {
        fgcterm.die = 1;

        // Check whether input is a file

        if(fgcterm.input != stdin)
        {
            fclose(fgcterm.input);
        }

        // Stop receive thread

        if(fgcterm.receive_thread_running)
        {
            pthread_cancel(fgcterm.receive_thread);
            pthread_join(fgcterm.receive_thread, NULL);
            pthread_detach(fgcterm.receive_thread);
        }

        // Close socket if open

        if(fgcterm.sock >= 0)
        {
            close(fgcterm.sock);
        }

        // Close log file if open

        if(fgcterm.logfile)
        {
            fclose(fgcterm.logfile);
        }

        // Restore terminal to line-mode

        tcsetattr(STDIN_FILENO, 0, &fgcterm.line_mode);

        exit(sig_num ? 0 : 1);
    }
}

// EOF
