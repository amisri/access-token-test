/*
 *  Filename: receive_thread.h
 *
 *  Purpose:  Declarations for receive thread
 *
 *  Author:   Stephen Page
 */

#ifndef RECEIVE_THREAD_H
#define RECEIVE_THREAD_H

#include <semaphore.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/time.h>

#include "consts.h"
#include "hash.h"

#ifdef RECEIVE_THREAD
#define RECEIVE_THREAD_EXT
#else
#define RECEIVE_THREAD_EXT extern
#endif

// Static functions

#ifdef RECEIVE_THREAD
#endif

// External functions

RECEIVE_THREAD_EXT void receive_threadCleanUp(void);
RECEIVE_THREAD_EXT int  receive_threadInit(void);
RECEIVE_THREAD_EXT void receive_threadRun(void);

// Struct containing global variables

RECEIVE_THREAD_EXT struct
{
    uint32_t    die;
    sem_t       semaphore;
} receive_thread;

#endif

// EOF
