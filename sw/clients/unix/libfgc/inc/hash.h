/*
 *  Filename: hash.h
 *
 *  Purpose:  Declarations for hashes
 *
 *  Author:   Stephen Page
 */

#ifndef HASH_H
#define HASH_H

#include <stdint.h>

#include "consts.h"

#ifdef HASH
#define HASH_EXT
#else
#define HASH_EXT extern
#endif

// Union to hold the hash key

union hash_key
{
    char        c[HASH_MAX_KEY_LEN + 1];
    uint32_t    i[(HASH_MAX_KEY_LEN + 1) / sizeof(uint32_t)];
};

// Struct containing hash key and value

struct hash_entry
{
    uint32_t            index;
    uintptr_t           value;
    union hash_key      key;
    struct hash_entry   *next;
};

// Struct with all data necessary for the table

struct hash_table
{
    size_t              size;       // Size of the hash table
    uint32_t            index;      // Index within the hash entry array
    struct hash_entry   **data;     // Pointer to the array of table data
};

// Static functions

#ifdef HASH
static unsigned int hashHash(union hash_key *key, int size);
static void hashNextPrime(size_t *target);
#endif

// External functions

HASH_EXT struct hash_table  *hashInit(size_t size);
HASH_EXT void               hashFree(struct hash_table *table);
HASH_EXT void               hashInsert(struct hash_table *table, struct hash_entry *entry);
HASH_EXT struct hash_entry  *hashFind(struct hash_table *table, char *key);

#endif

// EOF
