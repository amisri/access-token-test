/*
 *  Filename: connects.h
 *
 *  Purpose:  Declarations for managing gateway connections
 *
 *  Author:   Stephen Page
 */

#ifndef CONNECTS_H
#define CONNECTS_H

#include <pthread.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/time.h>

#include "cmd.h"
#include "cmdq.h"
#include "fgc_consts_gen.h"
#include "hash.h"

#ifdef CONNECTS
#define CONNECTS_EXT
#else
#define CONNECTS_EXT extern
#endif

// Structure for single connection's information

struct connection_info
{
    int             socket;
    struct cmdq     command_queue;
    struct cmd      *current_send_command;
    char            response[FGC_MAX_VAL_LEN + 1];
    uint32_t        response_length;
    uint32_t        error;
};

// Static functions

#ifdef CONNECTS
static int          connectsOpenGatewayConnect(char *hostname);
#endif

// External functions

CONNECTS_EXT void   connectsCleanUp(void);
CONNECTS_EXT int    connectsConnect(char *hostname);
CONNECTS_EXT int    connectsInit(char *username, char *password);

// Struct containing global variables

CONNECTS_EXT struct
{
    struct hash_table       *connection_hash;
    struct hash_entry       connection_entry[MAX_GATEWAYS];
    struct connection_info  connections_info[MAX_GATEWAYS];
    uint32_t                connection_index;
    fd_set                  full_set;
    int                     max_fd;
    pthread_mutex_t         mutex;
    char                    *password;
    char                    *username;
} connects;

#endif

// EOF
