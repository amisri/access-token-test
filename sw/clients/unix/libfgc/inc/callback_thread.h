/*
 *  Filename: callback_thread.h
 *
 *  Purpose:  Declarations for call-back thread
 *
 *  Author:   Stephen Page
 */

#ifndef CALLBACK_THREAD_H
#define CALLBACK_THREAD_H

#include <stdint.h>

#include "cmdq.h"
#include "consts.h"

#ifdef CALLBACK_THREAD
#define CALLBACK_THREAD_EXT
#else
#define CALLBACK_THREAD_EXT extern
#endif

// Static functions

#ifdef CALLBACK_THREAD
#endif

// External functions

CALLBACK_THREAD_EXT void    callback_threadCleanUp(void);
CALLBACK_THREAD_EXT int     callback_threadInit(void);
CALLBACK_THREAD_EXT void    callback_threadRun(void);

// Struct containing global variables

CALLBACK_THREAD_EXT struct
{
    uint32_t        die;
    struct cmdq     response_queue;
} callback_thread;

#endif

// EOF
