/*
 *  Filename: devname.h
 *
 *  Purpose:  Declarations for device name resolution
 *
 *  Author:   Stephen Page
 */

#ifndef DEVNAME_H
#define DEVNAME_H

#include <stdint.h>

#include "consts.h"
#include "hash.h"

#ifdef DEVNAME
#define DEVNAME_EXT
#else
#define DEVNAME_EXT extern
#endif

// Struct for single device's information

struct device_info
{
    uint32_t    channel;
    uint32_t    class;
    char        gateway[SYS_NAME_SIZE];
    char        *name;
    uint16_t    omode_mask;
};

// Static functions

#ifdef DEVNAME
#endif

// External functions

DEVNAME_EXT int                 devnameInit(void);
DEVNAME_EXT void                devnameCleanUp(void);
DEVNAME_EXT struct device_info* devnameGetInfo(uint32_t *num_devices);
DEVNAME_EXT int                 devnameResolve(char *name);

// Struct containing global variables

DEVNAME_EXT struct
{
    struct hash_table   *name_hash;
    struct hash_entry   name_entry[MAX_DEVICES];
    struct device_info  devices_info[MAX_DEVICES];
    uint32_t            num_devices;
} devname;

#endif

// EOF
