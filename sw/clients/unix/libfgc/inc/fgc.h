/*
 *  Filename: fgc.h
 *
 *  Purpose:  Declarations for the Function Generator Controller client library
 *
 *  Author:   Stephen Page
 */

#ifndef FGC_H
#define FGC_H

#include <pthread.h>
#include <stdint.h>

#define FGC_GLOBALS

#include "cmd.h"
#include "devname.h"
#include "fgc_consts_gen.h"

#ifdef FGC
#define FGC_EXT
#else
#define FGC_EXT extern
#endif

// Constants

#define FGC_MAX_PASS_LEN    15
#define FGC_MAX_USER_LEN    15

// Errors

enum
{
    fgc_okay,
    fgc_callback_thread_fail,
    fgc_connection_fail,
    fgc_name_fail,
    fgc_queue_fail,
    fgc_receive_thread_fail,
    fgc_semaphore_fail,
    fgc_send_thread_fail,
};

FGC_EXT char *fgc_errors[]
#ifdef FGC
=
{
    "Okay",
    "Unable to spawn callback thread",
    "Unable to connect to gateway",
    "Unable to resolve device name",
    "Unable to create command queue",
    "Unable to spawn receive thread",
    "Unable to create semaphore",
    "Unable to spawn send thread",
}
#endif
;

// Static functions

#ifdef FGC
static void                 fgcBlockingCallback(struct cmd *command);
#endif

// External functions

FGC_EXT void                fgcCleanUp(void);
FGC_EXT int                 fgcInit(char *username, char *password);
FGC_EXT struct device_info* fgcGetDeviceInfo(uint32_t *num_devices);
FGC_EXT int                 fgcGet(struct cmd *command);
FGC_EXT int                 fgcGetSync(struct cmd *command);
FGC_EXT int                 fgcSet(struct cmd *command);
FGC_EXT int                 fgcSetSync(struct cmd *command);
FGC_EXT int                 fgcSubmit(struct cmd *command);
FGC_EXT int                 fgcSubmitSync(struct cmd *command);

// Struct containing global variables

FGC_EXT struct
{
    pthread_t   callback_thread;
    pthread_t   receive_thread;
    pthread_t   send_thread;
} fgc;

#endif

// EOF
