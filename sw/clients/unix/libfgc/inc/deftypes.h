/*
 *  Filename: deftypes.h
 *
 *  Purpose:  Type declarations for property definitions
 *
 *  Author:   Stephen Page
 */

#ifndef DEFTYPES_H
#define DEFTYPES_H

#include <stdint.h>

// Types

typedef const char *    def_const_string;   // Type to use for string constants

// Size-specific types

typedef int8_t      INT8S;
typedef uint8_t     INT8U;
typedef int16_t     INT16S;
typedef uint16_t    INT16U;
typedef int32_t     INT32S;
typedef uint32_t    INT32U;

#endif

// EOF
