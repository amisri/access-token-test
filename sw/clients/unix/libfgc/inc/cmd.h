/*
 *  Filename: cmd.h
 *
 *  Purpose:  Declarations for commands
 *
 *  Author:   Stephen Page
 */

#ifndef CMD_H
#define CMD_H

#include <semaphore.h>
#include <stdint.h>

#include "cmdq.h"
#include "consts.h"

#ifdef CMD
#define CMD_EXT
#else
#define CMD_EXT extern
#endif

enum cmd_type    {cmd_set, cmd_get};

// Command structure

struct cmd
{
    uintptr_t           tag;                                    // Command tag
    void                (*callback)();                          // Call-back on completion
    enum cmd_type       type;                                   // Type (set or get)
    uint32_t            command_length;                         // Length of command string
    uint32_t            command_index;                          // Index within command string while parsing
    uint32_t            bin_value_length;                       // Length of value if binary
    uint32_t            remaining;                              // Number of remaining characters to send
    uint32_t            tag_length;                             // Length of tag
    uint32_t            response_length;                        // Length of response string
    uint32_t            error;                                  // Error status
    sem_t               semaphore;                              // Semaphore
    char                device[FGC_MAX_DEV_LEN + 1];            // Name of device
    char                property[FGC_MAX_PROP_LEN + 1];         // Property
    char                value[FGC_MAX_VAL_LEN + 1];             // Value(s)
    char                command_string[FGC_MAX_CMD_LEN + 1];    // Command string
};

// Static functions

#ifdef CMD
#endif

// External functions

#endif

// EOF
