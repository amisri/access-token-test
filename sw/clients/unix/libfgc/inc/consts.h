/*
 *  Filename: consts.h
 *
 *  Purpose:  Global definitions
 *
 *  Author:   Stephen Page
 */

#ifndef CONSTS_H
#define CONSTS_H

#include "fgc_consts_gen.h"

//#define ENABLE_TCP_AUTH

#define MAX_COMMANDS_PER_GW         100                     // Maximum number of commands per gateway
#define MAX_DEVICES                 2500                    // Maximum number of devices for name resolution
#define MAX_GATEWAYS                100                     // Maximum number of gateways to connect to

#define SYS_NAME_SIZE               31                      // Maximum length of system hostname

// Hash

#define HASH_MAX_KEY_LEN	    31                      // Maximum length of a hash key

// Check that maximum hash key length is sufficient to contain a maximum length device name and hostname

#if HASH_MAX_KEY_LEN < FGC_MAX_DEV_LEN || HASH_MAX_KEY_LEN < SYS_NAME_SIZE
#error HASH_MAX_KEY_LEN must be greater than or equal to FGC_MAX_DEV_LEN and SYS_NAME_SIZE
#endif

// Receive

#define RECEIVE_BUFFER_SIZE         32768                   // Size of receive buffer

// Select

#define SELECT_TIMEOUT_US           100000                  // Timeout of select() calls in usecs

#endif

// EOF
