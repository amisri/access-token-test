/*
 *  Filename: send_thread.h
 *
 *  Purpose:  Declarations for send thread
 *
 *  Author:   Stephen Page
 */

#ifndef SEND_THREAD_H
#define SEND_THREAD_H

#include <semaphore.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/time.h>

#include "consts.h"

#ifdef SEND_THREAD
#define SEND_THREAD_EXT
#else
#define SEND_THREAD_EXT extern
#endif

// Static functions

#ifdef SEND_THREAD
#endif

// External functions

SEND_THREAD_EXT void    send_threadCleanUp(void);
SEND_THREAD_EXT int     send_threadInit(void);
SEND_THREAD_EXT void    send_threadRun(void);

// Struct containing global variables

SEND_THREAD_EXT struct
{
    uint32_t    die;
    sem_t       semaphore;
} send_thread;

#endif

// EOF
