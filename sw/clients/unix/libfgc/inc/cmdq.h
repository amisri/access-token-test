/*
 *  Filename: cmdq.h
 *
 *  Purpose:  Declarations for command queues (FIFOs)
 *
 *  Author:   Stephen Page
 *
 *  Notes:    Thread safe with the exception of cmdqPop
 */

#ifndef CMD_Q_H
#define CMD_Q_H

#include <pthread.h>
#include <stdint.h>

#include "cmd.h"
#include "consts.h"

#ifdef CMDQ
#define CMDQ_EXT
#else
#define CMDQ_EXT extern
#endif

// Queue structure

struct cmdq
{
    struct cmd          **buffer;       // The buffer
    uint32_t            buffer_size;    // Buffer size
    uint32_t            back;           // Back of queue index
    uint32_t            front;          // Front of queue index
    pthread_mutex_t     mutex;          // Mutex
    pthread_cond_t      not_empty;      // Condition variable indicating that queue has content
};

// Static functions

#ifdef CMDQ
static int cmdqIsEmpty(struct cmdq *q);
#endif

// External functions

CMDQ_EXT int        cmdqInit(struct cmdq *q, uint32_t buffer_size);
CMDQ_EXT void       cmdqFree(struct cmdq *q);
CMDQ_EXT int        cmdqSyncPush(struct cmdq *q, struct cmd *member);
CMDQ_EXT struct cmd *cmdqPop(struct cmdq *q);
CMDQ_EXT struct cmd *cmdqSyncPop(struct cmdq *q);
CMDQ_EXT struct cmd *cmdqSyncBlockPop(struct cmdq *q);
CMDQ_EXT void       cmdqForceUnblock(struct cmdq *q);

#endif

// EOF
