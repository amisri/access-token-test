/*
 *  Filename: cmdq.c
 *
 *  Purpose:  Functions for command queues (FIFO)
 *
 *  Author:   Stephen Page
 *
 *  Notes:    Queue functions are thread safe with the exception of Init and
 *            Pop which may be used when only one thread is taking commands off
 *            the queue (otherwise use SyncPop or SyncBlockPop)
 */

#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>

#define CMDQ

#include "cmd.h"
#include "cmdq.h"

//////////////////////////////////////////////////

// Initialise the queue

int cmdqInit(struct cmdq *q, uint32_t buffer_size)
{
    buffer_size++;

    if((q->buffer = (struct cmd **)calloc(buffer_size, sizeof(struct cmd **))) != NULL)
    {
        // Memory was successfully allocated

        pthread_mutex_init(&q->mutex, NULL);
        pthread_cond_init(&q->not_empty, NULL);

        q->buffer_size = buffer_size;

        q->back = q->front = 0;

        return(0);
    }
    else
    {
        // Memory allocation failed

        q->buffer_size = 0;
        return(1);
    }
}

//////////////////////////////////////////////////

// Free the queue

void cmdqFree(struct cmdq *q)
{
    pthread_mutex_lock(&q->mutex);
    if(q->buffer != NULL)
    {
        free(q->buffer);
        q->buffer = NULL;
    }
    pthread_mutex_unlock(&q->mutex);
}

//////////////////////////////////////////////////

// Insert member into the queue

int cmdqSyncPush(struct cmdq *q, struct cmd *member)
{
    uint32_t next_pos_idx;

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return(2);
    }

    q->buffer[q->front] = member;

    // Check that the queue is not full

    next_pos_idx = (q->front + 1) % q->buffer_size;

    if(q->back != next_pos_idx)  // The queue is not full
    {
        q->front = next_pos_idx;

        // Send signal that queue has content

        pthread_cond_signal(&q->not_empty);
        pthread_mutex_unlock(&q->mutex);
        return(0);
    }
    else  // The queue is full
    {
        pthread_mutex_unlock(&q->mutex);
        return(1);
    }
}

//////////////////////////////////////////////////

// Extract member from the queue (non-blocking not thread safe)

struct cmd *cmdqPop(struct cmdq *q)
{
    struct cmd *member;

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        return(NULL);
    }

    if(cmdqIsEmpty(q))
    {
        // Queue is empty

        return(NULL);
    }
    member = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    return(member);
}

//////////////////////////////////////////////////

// Extract member from the queue (non-blocking and thread safe)

struct cmd *cmdqSyncPop(struct cmdq *q)
{
    struct cmd *member;

    pthread_mutex_lock(&q->mutex);

    member = cmdqPop(q);

    pthread_mutex_unlock(&q->mutex);
    return(member);
}

//////////////////////////////////////////////////

// Extract member from the queue (blocking)

struct cmd *cmdqSyncBlockPop(struct cmdq *q)
{
    struct cmd *member;

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return(NULL);
    }

    if(cmdqIsEmpty(q))
    {
        // Queue is empty
        // Wait for a member to enter the queue

        pthread_cond_wait(&q->not_empty, &q->mutex);
    }

    // Check whether the queue is still empty
    // (in case the block was forcefully removed)

    if(cmdqIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        return(NULL);
    }

    member = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);

    return(member);
}

//////////////////////////////////////////////////

// Forcefully unblock SyncBlockPops

void cmdqForceUnblock(struct cmdq *q)
{
    pthread_mutex_lock(&q->mutex);
    pthread_cond_broadcast(&q->not_empty);
    pthread_mutex_unlock(&q->mutex);
}

//////////////////////////////////////////////////

// Is the queue empty

static int cmdqIsEmpty(struct cmdq *q)
{
    return((q->back == q->front) ? 1 : 0);
}

//////////////////////////////////////////////////

// EOF
