/*
 *  Filename: devname.c
 *
 *  Purpose:  Functions for device name resolution
 *
 *  Author:   Stephen Page
 */

#include <stdio.h>
#include <string.h>

#define DEVNAME

#include "consts.h"
#include "devname.h"

//////////////////////////////////////////////////

// Initialise device naming
//
// Possible return values are:
//
// 0: Success
// 1: Failed to initialise name hash
// 2: Failed to open name file
// 3: Name file too large (more than MAX_DEVICES entries) or corrupt

int devnameInit(void)
{
    int     c;
    char    format[100];
    int     items_assigned;
    FILE    *name_file;

    // Initialise name hash

    if((devname.name_hash = hashInit(MAX_DEVICES)) == NULL)
    {
        return(1);
    }

    // Open name file

    if((name_file = fopen(FGC_NAME_FILE, "r")) == NULL)
    {
        return(2);
    }

    // Read name file entries

    // Create format from constants

    sprintf(format, "%%%d[^:]:%%u:%%u:%%%d[^:]:0x%%04hX",
            SYS_NAME_SIZE, FGC_MAX_DEV_LEN);

    do
    {
        items_assigned = fscanf(name_file,
                                format,
                                devname.devices_info[devname.num_devices].gateway,
                                &devname.devices_info[devname.num_devices].channel,
                                &devname.devices_info[devname.num_devices].class,
                                devname.name_entry[devname.num_devices].key.c,
                                &devname.devices_info[devname.num_devices].omode_mask);

        devname.name_entry[devname.num_devices].index   = devname.num_devices;
        devname.devices_info[devname.num_devices].name  = devname.name_entry[devname.num_devices].key.c;

        if(items_assigned == 5)
        {
            // Check for duplicate entry

            if(hashFind(devname.name_hash, devname.name_entry[devname.num_devices].key.c))
            {
                fprintf(stderr, "Duplicate entry of %s in name file %s at line %d\n",
                                devname.name_entry[devname.num_devices].key.c,
                                FGC_NAME_FILE,
                                devname.num_devices + 1);
                continue;
            }

            // Insert entry into name hash table

            hashInsert(devname.name_hash, &devname.name_entry[devname.num_devices]);
            devname.num_devices++;

            // Read until end of line or file

            while((c = fgetc(name_file)) != EOF && c != '\n');
        }
    } while(items_assigned == 5 && devname.num_devices < MAX_DEVICES);

    // Check that the end of the file has been reached (otherwise error)

    if(items_assigned != EOF)
    {
        if(items_assigned != 5) // Syntax error
        {
            fprintf(stderr, "Syntax error in name file %s at line %d\n",
                    FGC_NAME_FILE, devname.num_devices);
        }
        else // Too many devices
        {
            fprintf(stderr, "Too many entries in name file %s at line %d\n",
                    FGC_NAME_FILE, devname.num_devices);
        }

        fclose(name_file);
        return(3);
    }

    fclose(name_file);
    return(0);
}

//////////////////////////////////////////////////

// Clean-up

void devnameCleanUp(void)
{
    hashFree(devname.name_hash);
}

//////////////////////////////////////////////////

// Get device information array

struct device_info* devnameGetInfo(unsigned int *num_devices)
{
    *num_devices = devname.num_devices;
    return(devname.devices_info);
}

//////////////////////////////////////////////////

// Resolve a device name

int devnameResolve(char *name)
{
    struct hash_entry *entry;

    if(!(entry = hashFind(devname.name_hash, name)))
    {
        // Name could not be resolved

        return(-1);
    }

    // Return device number

    return(entry->index);
}

//////////////////////////////////////////////////

// EOF
