/*
 *  Filename: connects.c
 *
 *  Purpose:  Functions for managing gateway connections
 *
 *  Author:   Stephen Page
 */

#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define CONNECTS

#include "connects.h"
#include "consts.h"
#include "devname.h"
#include "hash.h"

//////////////////////////////////////////////////

// Initialise connection handling

int connectsInit(char *username, char *password)
{
    connects.connection_index = 0;

    // Set username and password pointers

    connects.username = username;
    connects.password = password;

    // Initialise connection hash

    if((connects.connection_hash = hashInit(MAX_GATEWAYS)) == NULL)
    {
        return(1);
    }

    // Initialise mutex

    pthread_mutex_init(&connects.mutex, NULL);

    return(0);
}

//////////////////////////////////////////////////

// Clean-up

void connectsCleanUp(void)
{
    uint32_t i;

    // Close connections

    for(i = 0 ; i < connects.connection_index; i++)
    {
        close(connects.connections_info[i].socket);
    }

    // Free connection hash

    hashFree(connects.connection_hash);
}

//////////////////////////////////////////////////

// Open connection to gateway
//
// Possible return values are:
//
// >=0: Success (socket)
// -2:  Unable to resolve hostname
// -3:  Unable to get socket
// -4:  Unable to connect to gateway
// -5:  Gateway busy

static int connectsOpenGatewayConnect(char *hostname)
{
    char                c;
    struct hostent      *host;
    struct sockaddr_in  sin;
    int                 sock;

#ifdef ENABLE_TCP_AUTH
    char                buffer[128];
    size_t              length;
#endif

    // Resolve hostname to address

    if((host = gethostbyname(hostname)) == NULL)
    {
        // Unable to resolve hostname

        return(-2);
    }

    // Configure addressing

    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_addr    = *((struct in_addr *)host->h_addr);
    sin.sin_family  = AF_INET;
    sin.sin_port    = htons(FGC_GW_PORT);

    // Get a socket

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        // Unable to get socket

        return(-3);
    }

    // Connect to gateway

    if(connect(sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        // Connection failed

        return(-4);
    }

    // Check for '+' to indicate that the gateway accepts the connection

    if(recv(sock, &c, 1, 0) != 1 || c != '+')
    {
        // Gateway is busy

        close(sock);
        return(-5);
    }
    send(sock, "+", 1, 0);

#ifdef ENABLE_TCP_AUTH

    // Receive username prompt "U"

    if(recv(sock, buffer, 1, 0) != 1)
    {
        // Communications error

        return(-6);
    }

    // Send username

    length = strlen(connects.username);
    if(send(sock, connects.username, length, 0) != length)
    {
        // Communications error

        return(-7);
    }
    send(sock, "\n", 1, 0);

    // Check for '+' to indicate that the gateway accepts the username

    if(recv(sock, &c, 1, 0) != 1 || c != '+')
    {
        // Username not accepted

        close(sock);
        return(-8);
    }

    // Receive password prompt "P"

    if(recv(sock, buffer, 1, 0) != 1)
    {
        // Communications error

        return(-9);
    }

    // Send password

    length = strlen(connects.password);
    if(send(sock, connects.password, length, 0) != length)
    {
        // Communications error

        return(-10);
    }
    send(sock, "\n", 1, 0);

    // Check for '+' to indicate that the gateway accepts the password

    if(recv(sock, &c, 1, 0) != 1 || c != '+')
    {
        // Password not accepted

        close(sock);
        return(-8);
    }

#endif

    // Put socket into non-blocking mode

    fcntl(sock, F_SETFL, fcntl(sock, F_GETFL) | O_NONBLOCK);

    // Add socket to selection set

    FD_SET(sock, &connects.full_set);

    return(sock);
}

//////////////////////////////////////////////////

// Connect to a gateway if not already connected
//
// Possible return values are:
//
// >=0: Success (socket)
// -1:  Maximum number of connections reached
// -2:  Unable to resolve hostname
// -3:  Unable to get socket
// -4:  Unable to connect to gateway
// -5:  Gateway busy

int connectsConnect(char *hostname)
{
    struct hash_entry   *entry;
    int                 sock;

    // Obtain mutex

    pthread_mutex_lock(&connects.mutex);

    // Check whether a new connection needs to be made

    if((entry = hashFind(connects.connection_hash, hostname)) == NULL)
    {
        // A connection does not already exist, so connect

        // Check that the maximum number of gateway connections has not yet been reached

        if(connects.connection_index == MAX_GATEWAYS)
        {
            // Maximum number of gateway connections has been reached

            // Free mutex

            pthread_mutex_unlock(&connects.mutex);

            return(-1);
        }

        if((sock = connectsOpenGatewayConnect(hostname)) < 0)
        {
            // Unable to connect to gateway, return error

            // Free mutex

            pthread_mutex_unlock(&connects.mutex);

            return(sock);
        }

        // Fill connection information structure

        connects.connections_info[connects.connection_index].socket = sock;
        cmdqInit(&connects.connections_info[connects.connection_index].command_queue, MAX_COMMANDS_PER_GW);

        // Add connection to connections hash

        connects.connection_entry[connects.connection_index].index = connects.connection_index;
        strncpy(connects.connection_entry[connects.connection_index].key.c, hostname, SYS_NAME_SIZE + 1);
        hashInsert(connects.connection_hash, &connects.connection_entry[connects.connection_index]);

        // Check whether this socket is greater than or equal to max_fd

        if(sock >= connects.max_fd)
        {
            connects.max_fd = sock + 1;
        }

        // Free mutex

        pthread_mutex_unlock(&connects.mutex);

        // Return connection index

        return(connects.connection_index++);
    }
    else if(connects.connections_info[entry->index].error) // Check whether socket has an error
    {
        // Remove socket from selection set

        FD_CLR(connects.connections_info[entry->index].socket, &connects.full_set);

        // Close socket

        close(connects.connections_info[entry->index].socket);

        // Free mutex

        pthread_mutex_unlock(&connects.mutex);

        // Attempt to reconnect to gateway

        return(connectsOpenGatewayConnect(hostname));
    }

    // Free mutex

    pthread_mutex_unlock(&connects.mutex);

    // Return connection index

    return(entry->index);
}

//////////////////////////////////////////////////

// EOF
