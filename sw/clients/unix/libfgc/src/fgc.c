/*
 *  Filename: fgc.c
 *
 *  Purpose:  Functions for the Function Generator Controller client library
 *
 *  Author:   Stephen Page
 */

#ifdef __Lynx__
#include <types.h>
#include <netinet/in.h>

extern int snprintf(char *str, size_t size, const char *format, ...);
#endif

#include <arpa/inet.h>
#include <inttypes.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define FGC

#include "cmd.h"
#include "cmdq.h"
#include "callback_thread.h"
#include "connects.h"
#include "devname.h"
#include "fgc.h"
#include "fgc_errs.h"
#include "send_thread.h"
#include "receive_thread.h"

//////////////////////////////////////////////////

// Initialise the library
//
// Possible return values are:
//
// fgc_okay:                    Success
// fgc_callback_thread_fail:    Unable to start call-back thread
// fgc_connection_fail:         Unable to initialise connection handling
// fgc_name_fail:               Unable to initialise device naming
// fgc_receive_thread_fail:     Unable to start receive thread
// fgc_send_thread_fail:        Unable to start send thread

int fgcInit(char *username, char *password)
{
    // Initialise device naming

    if(devnameInit())
    {
        return(fgc_name_fail);
    }

    // Initialise connection handling

    if(connectsInit(username, password))
    {
        return(fgc_connection_fail);
    }

    // Create call-back thread

    if(callback_threadInit() ||
       pthread_create(&fgc.callback_thread,
                      NULL,
                      (void *(*)(void *))callback_threadRun,
                      0))
    {
        return(fgc_callback_thread_fail);
    }

    // Create receive thread

    if(receive_threadInit() ||
       pthread_create(&fgc.receive_thread,
                      NULL,
                      (void *(*)(void *))receive_threadRun,
                      0))
    {
        return(fgc_receive_thread_fail);
    }

    // Create send thread

    if(send_threadInit() ||
       pthread_create(&fgc.send_thread,
                      NULL,
                      (void *(*)(void *))send_threadRun,
                      0))
    {
        return(fgc_send_thread_fail);
    }

    return(fgc_okay);
}

//////////////////////////////////////////////////

// Clean-Up the library

void fgcCleanUp(void)
{
    // Stop send thread

    sem_post(&send_thread.semaphore);
    send_thread.die = 1;
    pthread_join(fgc.send_thread, NULL);

    // Stop receive thread

    sem_post(&receive_thread.semaphore);
    receive_thread.die = 1;
    pthread_join(fgc.receive_thread, NULL);
    receive_threadCleanUp();

    // Stop call-back thread

    callback_thread.die = 1;
    cmdqForceUnblock(&callback_thread.response_queue);
    pthread_join(fgc.callback_thread, NULL);
    callback_threadCleanUp();

    // Zero password in memory

    memset(connects.password, 0, strlen(connects.password));

    // Clean up device naming

    devnameCleanUp();
}

//////////////////////////////////////////////////

// Get device information array

struct device_info *fgcGetDeviceInfo(uint32_t *num_devices)
{
    return(devnameGetInfo(num_devices));
}

//////////////////////////////////////////////////

// Submit an asynchronous command
//
// Possible return values are:
//
// fgc_okay:            Success
// fgc_connection_fail: Unable to connect to gateway
// fgc_name_fail:       Unable to resolve device name
// fgc_queue_fail:      Unable to queue command

int fgcSubmit(struct cmd *command)
{
    uint32_t    device_info_index;
    uint32_t    gateway_info_index;
    uint32_t    i;

    // Resolve device name

    if((device_info_index = devnameResolve(command->device)) < 0)
    {
        // Unable to resolve device name, return error

        return(fgc_name_fail);
    }

    // Check that a connection to the gateway exists

    if((gateway_info_index = connectsConnect(devname.devices_info[device_info_index].gateway)) < 0)
    {
        // Unable to connect to gateway, return error

        return(fgc_connection_fail);
    }

    // Convert command address to tag

    command->tag = (uintptr_t)command;

    // Construct command string

    if(command->type == cmd_set)
    {
        if(command->bin_value_length) // Binary value
        {
            command->command_length = snprintf(command->command_string,
                                               FGC_MAX_CMD_LEN + 1,
                                               "!%"PRIuPTR" s%d:%s ",
                                               command->tag,
                                               devname.devices_info[device_info_index].channel,
                                               command->property);

            if((command->command_length + 1 + sizeof(unsigned int) + command->bin_value_length) < FGC_MAX_CMD_LEN)
            {
                // Append 0xFF to command to indicate binary value

                command->command_string[command->command_length++] = 0xFF;

                // Append length of binary value to command

                i = htonl(command->bin_value_length);
                memcpy(&command->command_string[command->command_length], &i, sizeof(unsigned int));
                command->command_length += sizeof(unsigned int);

                // Append binary value to command

                memcpy(&command->command_string[command->command_length], command->value, command->bin_value_length);
                command->command_length += command->bin_value_length;
            }
            command->remaining = command->command_length;
        }
        else // String value
        {
            command->command_length = command->remaining = snprintf(command->command_string,
                                                                    FGC_MAX_CMD_LEN + 1,
                                                                    "!%"PRIuPTR" s%d:%s %s\n",
                                                                    command->tag,
                                                                    devname.devices_info[device_info_index].channel,
                                                                    command->property,
                                                                    command->value);
        }
    }
    else // (command->type == cmd_get)
    {
        command->command_length = command->remaining = snprintf(command->command_string,
                                                                FGC_MAX_CMD_LEN + 1,
                                                                "!%"PRIuPTR" g%d:%s\n",
                                                                command->tag,
                                                                devname.devices_info[device_info_index].channel,
                                                                command->property);
    }

    // Initialise command parameters

    command->command_index      = 0;
    command->error              = 0;
    command->response_length    = 0;

    // Queue command for sending

    if(cmdqSyncPush(&connects.connections_info[gateway_info_index].command_queue, command))
    {
        // Unable to queue command

        return(fgc_queue_fail);
    }

    // Awaken send thread

    sem_post(&send_thread.semaphore);

    return(fgc_okay);
}

//////////////////////////////////////////////////

// Submit a synchronous command
//
// Possible return values are:
//
// fgc_okay:            Success
// fgc_connection_fail: Unable to connect to gateway
// fgc_name_fail:       Unable to resolve device name
// fgc_queue_fail:      Unable to queue command
// fgc_semaphore_fail:  Unable to initialise semaphore

int fgcSubmitSync(struct cmd *command)
{
    int return_val;

    command->callback = fgcBlockingCallback;

    if(sem_init(&command->semaphore, 0, 0) < 0)
    {
        return(fgc_semaphore_fail);
    }

    if((return_val = fgcSubmit(command)) == 0)
    {
        sem_wait(&command->semaphore);
        sem_destroy(&command->semaphore);
    }

    return(return_val);
}

//////////////////////////////////////////////////

// Submit an asynchronous get command

int fgcGet(struct cmd *command)
{
    command->type = cmd_get;

    return(fgcSubmit(command));
}

//////////////////////////////////////////////////

// Submit a synchronous get command

int fgcGetSync(struct cmd *command)
{
    command->type = cmd_get;

    return(fgcSubmitSync(command));
}

//////////////////////////////////////////////////

// Submit an asynchronous set command

int fgcSet(struct cmd *command)
{
    command->type = cmd_set;

    return(fgcSubmit(command));
}

//////////////////////////////////////////////////

// Submit a synchronous set command

int fgcSetSync(struct cmd *command)
{
    command->type = cmd_set;

    return(fgcSubmitSync(command));
}

//////////////////////////////////////////////////

// Call-back for blocking commands

static void fgcBlockingCallback(struct cmd *command)
{
    sem_post(&command->semaphore);
}

//////////////////////////////////////////////////

// EOF
