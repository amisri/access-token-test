/*
 *  Filename: receive_thread.c
 *
 *  Purpose:  Functions for receive thread
 *
 *  Author:   Stephen Page
 */

#include <pthread.h>
#include <search.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#define RECEIVE_THREAD

#include "callback_thread.h"
#include "cmd.h"
#include "cmdq.h"
#include "connects.h"
#include "fgc_errs.h"
#include "hash.h"
#include "receive_thread.h"

//////////////////////////////////////////////////

// Initialise

int receive_threadInit(void)
{
    // Set die flag to 0

    receive_thread.die = 0;

    // Initialise semaphore

    sem_init(&receive_thread.semaphore, 0, 0);

    return(0);
}

//////////////////////////////////////////////////

// Run receive_thread thread

void receive_threadRun(void)
{
    char            buffer[RECEIVE_BUFFER_SIZE];
    uint32_t        buffer_len;
    struct cmd      *command;
    char            *end;
    char            *end_tag;
    fd_set          except_set;
    uint32_t        i;
    int             old_cancel_state;
    fd_set          receive_set;
    int             select_ret;
    int             set_except_connection_count;
    int             set_read_connection_count;
    char            *start;
    uintptr_t       tag;
    struct timeval  timeout;

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Set time-out for select

    timeout.tv_sec  = 0;
    timeout.tv_usec = SELECT_TIMEOUT_US;

    // Disable pthread_cancel

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_cancel_state);

    while(!receive_thread.die)
    {
        // Zero count of found selected connections

        set_except_connection_count = 0;
        set_read_connection_count   = 0;

        // Block on semaphore until there is a command to receive

        sem_wait(&receive_thread.semaphore);
        sem_post(&receive_thread.semaphore);

        // Copy full connection set to set for select

        memcpy(&except_set,     &connects.full_set, sizeof(fd_set));
        memcpy(&receive_set,    &connects.full_set, sizeof(fd_set));

        // Select sockets

        if((select_ret = select(connects.max_fd, &receive_set, NULL, &except_set, &timeout)) > 0)
        {
            for(i = 0 ; i < connects.connection_index && (set_except_connection_count + set_read_connection_count) < select_ret ; i++)
            {
                // Flag error and return error to commands if connection has an exception

                if(FD_ISSET(connects.connections_info[i].socket, &except_set))
                {
                    // Increment count of found selected connections

                    set_except_connection_count++;

                    // Set error flag for connection

                    connects.connections_info[i].error = 1;

                    // Remove connection from selection set

                    FD_CLR(connects.connections_info[i].socket, &connects.full_set);
                }
                else if(FD_ISSET(connects.connections_info[i].socket, &receive_set)) // Is connection readable?
                {
                    // Increment count of found selected connections

                    set_read_connection_count++;

                    // Perform non-blocking receive on socket

                    if((buffer_len = recv(connects.connections_info[i].socket,
                                          buffer,
                                          RECEIVE_BUFFER_SIZE - 1,
                                          0)) < 0)
                    {
                        // Move to next connection

                        continue;
                    }

                    // NULL-terminate buffer

                    buffer[buffer_len] = '\0';

                    // Isolate responses in buffer

                    start = end = buffer;

                    while(end != NULL && end < &buffer[buffer_len])
                    {
                        // Check whether a response is in progress

                        if(!connects.connections_info[i].response_length) // No response is in progress
                        {
                            // Find start and end of next response in buffer

                            start   = strchr(start, '$');
                            end     = (start == NULL) ? NULL : strchr(start, ';');
                        }
                        else // Connection is in the middle of a response
                        {
                            // Attempt to find end of response in buffer

                            end     = strchr(start, ';');
                        }

                        // Break if start position is NULL (no valid response in buffer)

                        if(start == NULL)
                        {
                            break;
                        }

                        // Check whether end of response occurred in buffer

                        if(end != NULL) // End of response is in buffer
                        {
                            // Append to response

                            memcpy(&connects.connections_info[i].response[connects.connections_info[i].response_length], start, end - start);

                            // Set response_length

                            connects.connections_info[i].response_length += end - start;

                            // NULL-terminate response (truncate by one to remove new-line)

                            connects.connections_info[i].response[connects.connections_info[i].response_length - 1] = '\0';

                            // Response is complete

                            // Isolate tag in response

                            // Tag should be between second character and first space

                            // Find first space in response after second character

                            end_tag = strchr(&connects.connections_info[i].response[1], ' ');

                            // Check whether a tag was found

                            if(end_tag != NULL && end_tag > &connects.connections_info[i].response[1]) // A tag was found
                            {
                                // NULL terminate tag

                                *end_tag = '\0';

                                // Convert tag to integer

                                tag = strtoul(&connects.connections_info[i].response[1], NULL, 0);

                                // Set command pointer

                                command = (struct cmd *)tag;

                                // Check whether command was an error

                                if(*(end_tag + 1) == '!')
                                {
                                    command->error = 1;
                                }

                                // Copy response to command

                                strncpy(command->value, end_tag + 3, sizeof(command->value));

                                // Null terminate value in case of overrun

                                command->value[sizeof(command->value) - 1] = '\0';

                                // Add command to call-back thread's queue

                                cmdqSyncPush(&callback_thread.response_queue, command);

                                // Decrement semaphore count

                                sem_trywait(&receive_thread.semaphore);

                                // Move search start to after end

                                start = end + 1;
                            }
                            else
                            {
                                // Ignore response as no tag
                            }

                            // Reset response

                            connects.connections_info[i].response_length = 0;
                        }
                        else // End of response is not in buffer
                        {
                            // Append buffer to response

                            strncpy(&connects.connections_info[i].response[connects.connections_info[i].response_length],
                                    start,
                                    buffer_len - (start - buffer));

                            // Set response length

                            connects.connections_info[i].response_length += buffer_len - (start - buffer);
                        }
                    }
                }
            }

            // Return errors to commands on connections with exceptions

            if(set_except_connection_count)
            {
                // Wait to ensure that a selector does not contain connection

                usleep(SELECT_TIMEOUT_US);

                for(i = 0 ; set_except_connection_count && i < connects.connection_index ; i++)
                {
                    if(FD_ISSET(connects.connections_info[i].socket, &except_set))
                    {
                        // Return error to queued commands

                        if(connects.connections_info[i].current_send_command != NULL)
                        {
                            command = connects.connections_info[i].current_send_command;
                        }
                        else
                        {
                            command = cmdqSyncPop(&connects.connections_info[i].command_queue);
                        }

                        if(command != NULL)
                        {
                            do
                            {
                                command->error              = 1;
                                command->response_length    = snprintf(command->value,
                                                                       sizeof(command->value),
                                                                       "%d %s",
                                                                       FGC_BAD_PORT, fgc_errmsg[FGC_BAD_PORT]);

                                // Add command to call-back thread's queue

                                cmdqSyncPush(&callback_thread.response_queue, command);
                            } while((command = cmdqSyncPop(&connects.connections_info[i].command_queue)) != NULL);
                        }

                        set_except_connection_count--;
                    }
                }
            }
        }
        else if(select_ret < 0) // Check for error during select()
        {
            // An error occurred during select, output message and stop

            perror("receive select");
            break;
        }
    }
}

//////////////////////////////////////////////////

// Clean up receive thread

void receive_threadCleanUp(void)
{
    sem_destroy(&receive_thread.semaphore);
}

//////////////////////////////////////////////////

// EOF
