/*
 *  Filename: hash.c
 *
 *  Purpose:  Functions for hashes
 *
 *  Author:   Stephen Page
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define HASH

#include "hash.h"

//////////////////////////////////////////////////

// Initialise a hash table of given size

struct hash_table* hashInit(size_t size)
{
    struct hash_table *table;

    // Adjust size to next prime number

    hashNextPrime(&size);

    // Allocate memory for table structure

    if((table = (struct hash_table *)malloc(sizeof(struct hash_table))) == NULL)
    {
        // Unable to allocate memory

        return(NULL);
    }

    // Allocate memory for table data

    if((table->data = (struct hash_entry **)calloc(size, sizeof(struct hash_entry *))) == NULL)
    {
        // Unable to allocate memory

        return(NULL);
    }

    table->size  = size;
    table->index = 0;

    return(table);
}

//////////////////////////////////////////////////

// Free memory held by the hash table

void hashFree(struct hash_table *table)
{
    free(table->data);
    free(table);
}

//////////////////////////////////////////////////

// Insert an entry into a hash table

void hashInsert(struct hash_table *table, struct hash_entry *entry)
{
    uint32_t            index;
    struct hash_entry   *node;

    entry->next = NULL;

    // Perform the hash

    index = hashHash(&entry->key, table->size);

    // Insert the entry, checking for collisions

    if(table->data[index] == 0)
    {
        // Insert the entry at the start

        table->data[index]  = entry;
    }
    else
    {
        // Follow the chain until the end

        for(node = table->data[index] ; node->next != NULL ; node = node->next);

        // Add the entry onto the end

        node->next  = entry;
    }
}

//////////////////////////////////////////////////

// Find an entry in a hash table

struct hash_entry *hashFind(struct hash_table *table, char *key_string)
{
    uint32_t            i;
    uint32_t            index;
    union hash_key      key;
    struct hash_entry   *node;

    // Zero fill the key;

    bzero(key.c, HASH_MAX_KEY_LEN + 1);

    // Copy the key string into the key

    strcpy(key.c, key_string);

    // Perform the hash

    index = hashHash(&key, table->size);

    // Check whether there are no matches for the hash

    if(table->data[index] == 0)
    {
        return(NULL);
    }

    // Follow the chain of nodes checking each for a match

    for(node = table->data[index] ; node != NULL ; node = node->next)
    {
        // Check whether the values match

        for(i = 0 ; (i < ((HASH_MAX_KEY_LEN + 1) / sizeof(key.i[0]))) &&
                    (node->key.i[i] == key.i[i]) ; i++)
        {
            // Is this a match?

            if(i == (HASH_MAX_KEY_LEN / sizeof(key.i[0])) - 1)
            {
                // Match found, return value

                return(node);
            }
        }
    }

    // No match was found, return NULL

    return(NULL);
}

//////////////////////////////////////////////////

// Hash a key

static unsigned int hashHash(union hash_key *key, int size)
{
    uint32_t hash = 0;
    uint32_t i;

    for(i = 0 ; i < ((HASH_MAX_KEY_LEN + 1) / sizeof(key->i[0])) ; i++)
    {
        hash += key->i[i];
    }
    hash %= size;

    return(hash);
}

//////////////////////////////////////////////////

// Find the next prime number

static void hashNextPrime(size_t *target)
{
    size_t  i;
    int32_t is_prime = 0;

    for( ; !is_prime ; (*target)++)
    {
        // Set is_prime to 1 initially

        is_prime = 1;

        // Check for factors

        for(i = 2 ; is_prime && i < (*target / 2) ; i++)
        {
            if(*target % i == 0)
            {
                is_prime = 0;
            }
        }
    }
    (*target)--;
}

//////////////////////////////////////////////////

// EOF
