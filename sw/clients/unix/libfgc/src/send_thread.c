/*
 *  Filename: send_thread.c
 *
 *  Purpose:  Functions for send thread
 *
 *  Author:   Stephen Page
 */

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>

#define SEND_THREAD

#include "callback_thread.h"
#include "connects.h"
#include "fgc_errs.h"
#include "receive_thread.h"
#include "send_thread.h"

//////////////////////////////////////////////////

// Initialise

int send_threadInit(void)
{
    // Set die flag to 0

    send_thread.die = 0;

    // Initialise semaphore

    sem_init(&send_thread.semaphore, 0, 0);

    return(0);
}

//////////////////////////////////////////////////

// Run send_thread thread

void send_threadRun(void)
{
    ssize_t         bytes_sent;
    struct cmd      *command;
    int             i;
    int             old_cancel_state;
    int             select_ret;
    fd_set          send_set;
    uint32_t        set_connection_count;
    struct timeval  timeout;

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Set time-out for selector

    timeout.tv_sec  = 0;
    timeout.tv_usec = SELECT_TIMEOUT_US;

    // Disable pthread_cancel

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_cancel_state);

    while(!send_thread.die)
    {
        // Zero count of found selected connections

        set_connection_count = 0;

        // Block on semaphore until there is a command to send

        sem_wait(&send_thread.semaphore);
        sem_post(&send_thread.semaphore);

        // Copy full connection set to set for select

        memcpy(&send_set, &connects.full_set, sizeof(fd_set));

        // Select sockets

        if((select_ret = select(connects.max_fd, NULL, &send_set, NULL, &timeout)) > 0)
        {
            for(i = 0 ; i < connects.connection_index && set_connection_count < select_ret ; i++)
            {
                // Perform non-blocking send if connection is ready

                if(FD_ISSET(connects.connections_info[i].socket, &send_set))
                {
                    // Increment count of found selected connections

                    set_connection_count++;

                    // Check whether a command is in the process of being sent

                    if(connects.connections_info[i].current_send_command == NULL)
                    {
                        // Attempt to get next command from connection's queue

                        if(!(connects.connections_info[i].current_send_command = cmdqSyncPop(&connects.connections_info[i].command_queue)))
                        {
                            // No command available, check next connection

                            continue;
                        }
                    }
                    command = connects.connections_info[i].current_send_command;

                    // Send bytes from command

                    bytes_sent = send(connects.connections_info[i].socket,
                                      &command->command_string[command->command_index],
                                      command->remaining,
                                      0);

                    // Check whether an error occurred during send

                    if(bytes_sent < 0)
                    {
                        if(errno != EAGAIN)
                        {
                            // Set error flag for connection

                            connects.connections_info[i].error                  = 1;
                            connects.connections_info[i].current_send_command   = NULL;

                            // Decrement semaphore count

                            sem_trywait(&send_thread.semaphore);

                            // Return error to queued commands

                            do
                            {
                                command->error              = 1;
                                command->response_length    = snprintf(command->value,
                                                                       sizeof(command->value),
                                                                       "%d %s",
                                                                       FGC_BAD_PORT, fgc_errmsg[FGC_BAD_PORT]);

                                // Add command to call-back thread's queue

                                cmdqSyncPush(&callback_thread.response_queue, command);

                                // Decrement semaphore count

                                sem_trywait(&send_thread.semaphore);
                            } while((command = cmdqSyncPop(&connects.connections_info[i].command_queue)) != NULL);
                        }
                    }
                    else // Send okay
                    {
                        command->command_index  += bytes_sent;
                        command->remaining      -= bytes_sent;

                        // Check whether command has been completely sent

                        if(command->remaining == 0)
                        {
                            // Decrement semaphore count

                            sem_trywait(&send_thread.semaphore);

                            // Awaken receive thread

                            sem_post(&receive_thread.semaphore);

                            connects.connections_info[i].current_send_command = NULL;
                        }
                    }
                }
            }
        }
        else if(select_ret < 0) // Check for error during select()
        {
            // An error occurred during select, output message and stop

            perror("send select");
            break;
        }
    }
}

//////////////////////////////////////////////////

// Clean-up send thread

void send_threadCleanUp(void)
{
    sem_destroy(&send_thread.semaphore);
}

//////////////////////////////////////////////////

// EOF
