/*
 *  Filename: callback_thread.c
 *
 *  Purpose:  Functions for call-back thread
 *
 *  Author:   Stephen Page
 */

#include <pthread.h>

#define CALLBACK_THREAD

#include "callback_thread.h"
#include "cmdq.h"

//////////////////////////////////////////////////

// Initialise

int callback_threadInit(void)
{
    // Initialise response queue

    return(cmdqInit(&callback_thread.response_queue, MAX_COMMANDS_PER_GW * MAX_GATEWAYS));
}

//////////////////////////////////////////////////

// Run call-back thread

void callback_threadRun(void)
{
    struct cmd  *command;
    int         old_cancel_state;

    // Disable pthread_cancel

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_cancel_state);

    while(!callback_thread.die &&
          (command = cmdqSyncBlockPop(&callback_thread.response_queue)))
    {
        // Call user-supplied call-back

        command->callback(command);
    }
}

//////////////////////////////////////////////////

// Clean up call-back thread

void callback_threadCleanUp(void)
{
    cmdqFree(&callback_thread.response_queue);
}

//////////////////////////////////////////////////

// EOF
