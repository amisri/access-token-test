/*
 *  Filename:   rt_zero.c
 *
 *  Purpose:    Zero all RT channels on a gateway
 *
 *  Author:     Stephen Page
 */

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/ip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#define RT_ZERO

#include "rt_zero.h"

#include "fgc_consts_gen.h"
#include "fgc_rtdata.h"

// Send value to gateway

int send_value(void)
{
    static struct fgc_udp_rt_data   rt_data;
    struct timeval                  time;

    // Activate all channels

    rt_data.active_channels = 0xFFFFFFFFFFFFFFFFLL;

    // Timestamp data

    gettimeofday(&time, NULL);
    rt_data.header.send_time_sec    = htonl(time.tv_sec);
    rt_data.header.send_time_usec   = htonl(time.tv_usec);

    // Send data to gateway

    sendto(rt_zero.sock,
           &rt_data, sizeof(rt_data),
           MSG_DONTWAIT,
           (struct sockaddr *)&rt_zero.addr, sizeof(rt_zero.addr));

    // Increment sequence number ready for next packet

    rt_data.header.sequence++;

    return(0);
}

int main(int argc, char *argv[])
{
    char            *gateway;
    struct hostent  *hostent;
    int             sock_tos;

    if(argc != 2)
    {
        fprintf(stderr, "\nUsage: %s <gateway>\n\n", argv[0]);
        return(1);
    }

    // Assign arguments

    gateway = argv[1];

    // Configure addressing

    if((hostent = gethostbyname(gateway)) == NULL)
    {
        fprintf(stderr, "Error resolving host %s\n", gateway);
        return(1);
    }

    memcpy(&rt_zero.addr.sin_addr, hostent->h_addr, hostent->h_length);

    rt_zero.addr.sin_family = AF_INET;
    rt_zero.addr.sin_port   = htons(FGC_GW_RT_PORT);

    // Create socket

    if((rt_zero.sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        return(1);
    }

    // Set IP Type-Of-Service field for socket

    sock_tos = IPTOS_LOWDELAY;
    if(setsockopt(rt_zero.sock,
                  IPPROTO_IP,
                  IP_TOS,
                  &sock_tos, sizeof(sock_tos)) < 0)
    {
        perror("setsockopt");
        return(1);
    }

    send_value();

    return(0);
}

// EOF
