/*
 *  Filename: rt_zero.h
 *
 *  Purpose:  Declarations for RT Zero
 *
 *  Author:   Stephen Page
 */

#ifndef RT_ZERO_H
#define RT_ZERO_H

#include <netinet/in.h>

#ifdef RT_ZERO
#define RT_ZERO_EXT
#else
#define RT_ZERO_EXT extern
#endif

// Static functions

#ifdef RT_ZERO
#endif

// External functions

RT_ZERO_EXT int send_value(void);

// Struct containing global variables

struct
{
    struct sockaddr_in  addr;
    int                 sock;
} rt_zero;

#endif

// EOF
