/*
 *  Filename:   rt_sine.c
 *
 *  Purpose:    Send a sine wave to an FGC's RT channel
 *
 *  Author:     Stephen Page
 *
 *  Notes:      This program must be run on a machine with a CTR timing receiver.
 */

#include <arpa/inet.h>
#include <math.h>
#include <netdb.h>
#include <netinet/ip.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <tim/TimLib.h>
#include <unistd.h>

#define RT_SINE

#include "rt_sine.h"

#include "fgc_consts_gen.h"
#include "fgc_fieldbus.h"
#include "fgc_rtdata.h"

// Calculate value of point in sine wave

float sine_point(float amplitude, float frequency, double time)
{
    float angular_frequency = 2.0 * M_PI * frequency;

    return(amplitude * sin(angular_frequency * time));
}

// Send value to gateway

int send_value(float value)
{
    uint64_t                        active_channels = 1LL << (rt_sine.channel - 1);
    uint32_t                        big_endian      = htons(1) == 1 ? 1 : 0;
    static struct fgc_udp_rt_data   rt_data;
    TimLibError                     timlib_error;
    TimLibTime                      timlib_time;

    // Read time from timing receiver

    if((timlib_error = TimLibGetTime(TIMING_MODULE, &timlib_time))) // Failed to read time
    {
        fprintf(stderr, "Error reading time (Timlib error: %s)\n",
                TimLibErrorToString(timlib_error));
        return(1);
    }

    // Set data

    fl.f = value;
    fl.l = htonl(fl.l);

    rt_data.channels[rt_sine.channel - 1] = fl.f;

    // Set active_channels mask in network byte order
    // This is done manually since there is currently no POSIX standard byte-swap for 64 bit values

    if(big_endian)
    {
        rt_data.active_channels = active_channels;
    }
    else // Little-endian
    {
        rt_data.active_channels = ((uint64_t)htonl( active_channels & 0x00000000FFFFFFFFLL) << 32) |
                                             htonl((active_channels & 0xFFFFFFFF00000000LL) >> 32);
    }

    // Timestamp data

    rt_data.header.send_time_sec    = htonl(timlib_time.Second);
    rt_data.header.send_time_usec   = htonl(timlib_time.Nano / 1000);

    // Send data to gateway

    sendto(rt_sine.sock,
           &rt_data, sizeof(rt_data),
           MSG_DONTWAIT,
           (struct sockaddr *)&rt_sine.addr, sizeof(rt_sine.addr));

    // Increment sequence number ready for next packet

    rt_data.header.sequence++;

    return(0);
}

// Clean-up and exit

static void cleanup(int signal)
{
    // Set flag to die when reference is close to zero

    rt_sine.die = 1;
}

int main(int argc, char *argv[])
{
    float           amplitude;
    TimLibTime      evt_time;
    float           frequency;
    char            *gateway;
    struct hostent  *hostent;
    float           ref;
    float           ref_prev    = 0;
    int             sample      = 0;
    int             sock_tos;
    TimLibError     timlib_error;

    if(argc != 5)
    {
        fprintf(stderr, "\nUsage: %s <gateway> <channel> <amplitude> <frequency>\n\n", argv[0]);
        return(1);
    }

    // Assign arguments

    gateway = argv[1];
    rt_sine.channel = atoi(argv[2]);
    sscanf(argv[3], "%f", &amplitude);
    sscanf(argv[4], "%f", &frequency);

    // Check that channel is within valid range

    if(rt_sine.channel < 1 || rt_sine.channel > 64)
    {
        fprintf(stderr, "Requested channel %u is outside of allowed range 1-64\n", rt_sine.channel);
        return(1);
    }

    // Configure addressing

    if((hostent = gethostbyname(gateway)) == NULL)
    {
        fprintf(stderr, "Error resolving host %s\n", gateway);
        return(1);
    }

    memcpy(&rt_sine.addr.sin_addr, hostent->h_addr, hostent->h_length);

    rt_sine.addr.sin_family = AF_INET;
    rt_sine.addr.sin_port   = htons(FGC_GW_RT_PORT);

    // Create socket

    if((rt_sine.sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        return(1);
    }

    // Set IP Type-Of-Service field for socket

    sock_tos = IPTOS_LOWDELAY;
    if(setsockopt(rt_sine.sock,
                  IPPROTO_IP,
                  IP_TOS,
                  &sock_tos, sizeof(sock_tos)) < 0)
    {
        perror("setsockopt");
        return(1);
    }

    // Catch signals to clean-up

    signal(SIGHUP,  cleanup);
    signal(SIGINT,  cleanup);
    signal(SIGQUIT, cleanup);
    signal(SIGTERM, cleanup);

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Initialise timing library

    if((timlib_error = TimLibInitialize(TimLibDevice_CTR))) // Error during timing library initialisation
    {
        fprintf(stderr, "Error initialising timlib: %s\n", TimLibErrorToString(timlib_error));
        return(1);
    }

    // Connect to interrupt

    if((timlib_error = TimLibConnect(TimLibClassHARDWARE, TimLibHardware1KHZ, TIMING_MODULE)))
    {
        fprintf(stderr, "Error connecting to 1KHz timer: %s\n", TimLibErrorToString(timlib_error));
        return(1);
    }

    // Loop triggered by 1kHz interrupt

    while(!(timlib_error = TimLibWait(  NULL,       // Class of interrupt
                                        NULL,       // PTIM, CTIM or hardware mask
                                        NULL,       // Ptim line number 1..n or 0
                                        NULL,       // Hardware source of interrupt
                                        &evt_time,  // Time of interrupt/output
                                        NULL,       // Time of counters load
                                        NULL,       // Time of counters start
                                        NULL,       // CTIM trigger equipment ID
                                        NULL,       // Payload of trigger event
                                        NULL,       // Module that interrupted
                                        NULL,       // Number of missed interrupts
                                        NULL,       // Remaining interrupts on queue
                                        NULL))      // Corresponding TgmMachine
          || timlib_error == TimLibErrorTIMEOUT)     
    {
        // Do nothing if not aligned to FGC WorldFIP cycle

        if((evt_time.Nano / 1000000) % FGC_FIELDBUS_CYCLE_PERIOD_MS) continue;

        ref = sine_point(amplitude, frequency, (double)sample / FGC_FIELDBUS_CYCLE_FREQ);

        // Check whether a request to die has been made
        // If so, stop when reference crosses zero

        if(rt_sine.die &&
           ((ref >= 0 && ref_prev <= 0) ||
            (ref <= 0 && ref_prev >= 0)))
        {
            break;
        }

        send_value(ref);
        sample++;
        ref_prev = ref;
    }
    send_value(0);

    if(timlib_error && timlib_error != TimLibErrorTIMEOUT)
    {
        fprintf(stderr, "Error waiting for timing interrupt: %s\n",
                TimLibErrorToString(timlib_error));
    }

    return(0);
}

// EOF
