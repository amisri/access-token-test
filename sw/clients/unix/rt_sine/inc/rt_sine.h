/*
 *  Filename: rt_sine.h
 *
 *  Purpose:  Declarations for RT Sine
 *
 *  Author:   Stephen Page
 */

#ifndef RT_SINE_H
#define RT_SINE_H

#include <netinet/in.h>

#ifdef RT_SINE
#define RT_SINE_EXT
#else
#define RT_SINE_EXT extern
#endif

// Constants

#define TIMING_MODULE   1

// Types

union
{
    float   f;
    long    l;
} fl;

// Static functions

#ifdef RT_SINE
static void cleanup(int signal);
#endif

// External functions

RT_SINE_EXT int     send_value(float value);
RT_SINE_EXT float   sine_point(float amplitude, float frequency, double time);

// Struct containing global variables

struct
{
    struct sockaddr_in  addr;
    unsigned int        channel;
    unsigned int        die;
    int                 sock;
} rt_sine;

#endif

// EOF
