/*
 *  Filename: pertu_pm.h
 *
 *  Purpose:  Declarations for pertu post-mortem
 *
 *  Author:   Stephen Page
 */

#ifndef PERTU_PM_H
#define PERTU_PM_H

#include <pm/PMClient.h>
#include <stdint.h>

#include "fgc_pubdata.h"

#ifdef PERTU_PM
#define PERTU_PM_EXT
#else
#define PERTU_PM_EXT extern
#endif

// Constants

#define PERTU_PM_PATH                   "/mnt"  // Path for pertu data files
#define PERTU_PM_PORT_NUMBER            2000    // UDP port number to which to bind
#define PERTU_PM_WAIT_SECS              300     // Time in seconds to wait for a pertu file following a trip

// Struct containing global variables

PERTU_PM_EXT struct pertu_pm
{
    char        *device_name;   // Name of the device being surveyed
    PMClient    *pm_client;     // Post-mortem client object
} pertu_pm;

// Static functions

#ifdef PERTU_PM
#endif

// External functions

PERTU_PM_EXT int getFgcAddress(char *name);
PERTU_PM_EXT int pertuToPMData(struct pertu_data &pertu_data, Timescale &timescale, PMData &pm_data);
PERTU_PM_EXT int sendPertuDataToPm(struct fgc_udp_pub_data *pub_data, int32_t fgc_address);

#endif

// EOF
