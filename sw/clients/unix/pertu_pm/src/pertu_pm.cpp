/*
 *  Filename:   pertu_pm.c
 *
 *  Purpose:    Send Converteam pertu data to post-mortem system in the event of a trip
 *
 *  Author:     Stephen Page
 */

#include <arpa/inet.h>
#include <cmw-data/Data.h>
#include <dirent.h>
#include <errno.h>
#include <pm/PMClient.h>
#include <pm/PMError.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#define PERTU_PM

#include "classes/53/defconst.h"
#include "fgc_consts_gen.h"

#define FGCD_MAX_DEVS 31 // formerly defined in fgc_pubdata.h
#include "fgc_pubdata.h"
#include "pertu.h"
#include "pertu_pm.h"

////////////////////////////////////////////////////////////////////////////////

// Get address of an FGC from its name

int getFgcAddress(char *name)
{
    uint32_t    address;
    char        c;
    char        device_name[FGC_MAX_DEV_LEN + 1];
    uint8_t     fgc_class;
    char        format[64];
    char        host_name[HOST_NAME_MAX + 1];
    int         items_assigned;
    FILE        *name_file;

    // Create format from constants

    snprintf(format, sizeof(format),
             "%%%d[^:]:%%u:%%u:%%%d[^:]",
             HOST_NAME_MAX, FGC_MAX_DEV_LEN);

    // Open FGC name file

    if(!(name_file = fopen(FGC_NAME_FILE, "r")))
    {
        perror("Open name file");
        return(-1);
    }

    // Search for entry for device in name file

    do
    {
        // Read entry

        items_assigned = fscanf(name_file, format, host_name,
                                                   &address,
                                                   &fgc_class,
                                                   device_name);

        // Check whether entry is for desired device

        if(!strcmp(device_name, name) && address < FGCD_MAX_DEVS)
        {
            // Device entry was found

            fclose(name_file);
            return(address);
        }

        // Read until end of line or file

        while((c = fgetc(name_file)) != EOF && c != '\n');
    } while(items_assigned == 4);

    // Name file was fully read, but the device was not found

    fclose(name_file);
    return(-1);
}

////////////////////////////////////////////////////////////////////////////////

// Read data from a pertu file and store it in a post-mortem data object

int pertuToPMData(struct pertu_data &pertu_data, Timescale &timescale, PMData &pm_data)
{
    uint32_t i;

    // Add each signal to post-mortem data

    for(i = 0 ; i < pertu_data.num_signals ; i++)
    {
        // Skip FGC time signals

        if(!strcmp(pertu_data.signal_names[i], "Ps.ValCtrl.Time") ||
           !strcmp(pertu_data.signal_names[i], "Ps.ValCtrl.Time_ms"))
        {
            continue;
        }

        // Add signal to post-mortem data

        try
        {
            switch(pertu_data.signal_types[i])
            {
                case PERTU_FLOAT:
                    pm_data.registerSignal(pertu_data.signal_names[i],
                                           (float *)(&pertu_data.signal_data[PERTU_SAMPLE_SIZE * i * pertu_data.num_samples]),
                                           timescale);
                    break;

                case PERTU_BOOL:
                case PERTU_LONG:
                    pm_data.registerSignal(pertu_data.signal_names[i],
                                           (int32_t *)(&pertu_data.signal_data[PERTU_SAMPLE_SIZE * i * pertu_data.num_samples]),
                                           timescale);
                    break;
            }
        }
        catch(PMError e)
        {
            fprintf(stderr, "Failed to add signal %s to PM data: %s\n",
                            pertu_data.signal_names[i], e.getMessage());
            return(1);
        }
        catch(...) // An unexpected type of exception occurred
        {
            fprintf(stderr, "Failed to add signal %s to PM data: Unexpected exception\n",
                            pertu_data.signal_names[i]);
            return(1);
        }
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Extract pertu data and send it to the post-mortem system

int sendPertuDataToPm(struct fgc_udp_pub_data *pub_data, int32_t fgc_address)
{
    PMData              *data               = new PMData("FGC", "pertu", pertu_pm.device_name);
    DIR                 *dir;
    struct dirent       *dir_entry;
    long long           event_time          = ((long long)ntohl(pub_data->time_sec)  * 1000000000) +
                                              ((long long)ntohl(pub_data->time_usec) * 1000);
    char                filename[128]       = "";
    time_t              newest_file_time    = 0;
    struct pertu_data   pertu_data;
    struct stat         stat_buf;
    Timescale           *timescale;         // Timescale for post-mortem data
    long long           timescale_period_ns = 1000000; // 1ms
    long long           timescale_start_ns  = 0;

    // Wait for P80 system to create pertu file

    sleep(PERTU_PM_WAIT_SECS);

    // Open pertu directory

    if(!(dir = opendir(".")))
    {
        perror("opendir");
        return(1);
    }

    // Find newest pertu file

    errno = 0;
    while((dir_entry = readdir(dir)) && !errno)
    {
        // Stat directory entry

        if(stat(dir_entry->d_name, &stat_buf))
        {
            perror("stat");
            return(1);
        }

        // Skip non-regular files

        if(!S_ISREG(stat_buf.st_mode))
        {
            continue;
        }

        // Check whether file is newest seen so far

        if(stat_buf.st_mtime > newest_file_time)
        {
            newest_file_time = stat_buf.st_mtime;

            // Store file name

            strncpy(filename, dir_entry->d_name, sizeof(filename));
            filename[sizeof(filename) - 1] = '\0';
        }
    }
    closedir(dir);

    // Check whether an error occurred while reading the directory

    if(errno)
    {
        perror("readdir");
        return(1);
    }

    // Check whether file was not found

    if(!filename[0])
    {
        delete(data);
        return(1);
    }

    // Read pertu file

    if(pertu_read_file(filename, &pertu_data))
    {
        fprintf(stderr, "Failed to read %s: ", filename);
        perror(NULL);
        delete(data);
        return(1);
    }

    // Create timescale for post-mortem

    timescale_start_ns = (long long)pertu_data.start_time.tv_sec  * 1000000000 +
                         (long long)pertu_data.start_time.tv_usec * 1000;

    timescale = new Timescale("time",
                              timescale_start_ns, timescale_period_ns,
                              pertu_data.num_samples);

    if(pertuToPMData(pertu_data, *timescale, *data))
    {
        pertu_free_data(&pertu_data);
        delete(data);
        delete(timescale);
        return(1);
    }

    // Send pertu data to post-mortem server

    try
    {
        pertu_pm.pm_client->send(*data, event_time, PMData::QUALIF_UNTESTED);
    }
    catch(PMError e)
    {
        fprintf(stderr, "Failed to send post-mortem data: %s\n", e.getMessage());
        pertu_free_data(&pertu_data);
        delete(data);
        delete(timescale);
        return(1);
    }
    catch(...) // An unexpected type of exception occurred
    {
        fprintf(stderr, "Failed to send post-mortem data: Unexpected exception\n");
        pertu_free_data(&pertu_data);
        delete(data);
        delete(timescale);
        return(1);
    }

    pertu_free_data(&pertu_data);
    delete(data);
    delete(timescale);
    return(0);
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    uint32_t                armed = 0;      // Flag to indicate whether post-mortem trigger is armed
    struct fgc_stat         *device_status; // Data for target device within buffer
    int32_t                 fgc_address;    // Address on gateway of FGC for device being surveyed
    struct fgc_udp_pub_data pub_data;       // Buffer for reception of published data
    struct sockaddr_in      sin;            // Socket address configuration
    int                     sock;           // UDP socket for FGC data reception

    // Process arguments

    if(argc != 2)
    {
        fprintf(stderr, "Usage: %s <device>\n", argv[0]);
        exit(1);
    }
    pertu_pm.device_name = argv[1];

    // Change to pertu directory

    if(chdir(PERTU_PM_PATH))
    {
        perror("chdir");
        fprintf(stderr, "Failed to change to pertu PM path: %s\n", PERTU_PM_PATH);
        exit(1);
    }

    // Get FGC address

    if((fgc_address = getFgcAddress(pertu_pm.device_name)) < 0)
    {
        fprintf(stderr, "Failed to get address for device %s\n", pertu_pm.device_name);
        exit(1);
    }
    device_status = &pub_data.status[fgc_address];

    // Create post-mortem client object

    try
    {
        pertu_pm.pm_client = new PMClient();
    }
    catch(PMError e)
    {
        fprintf(stderr, "Failed to create post-mortem client object: %s\n", e.getMessage());
        exit(1);
    }
    catch(...) // An unexpected type of exception occurred
    {
        fprintf(stderr, "Failed to create post-mortem client object: Unexpected exception\n");
        exit(1);
    }

    // Create socket

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        exit(1);
    }

    // Set bind options

    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(PERTU_PM_PORT_NUMBER);
    sin.sin_family      = AF_INET;

    // Bind to socket

    if(bind(sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("bind");
        close(sock);
        return(1);
    }

    // Receive data

    while(recvfrom(sock,
                   &pub_data, sizeof(pub_data),
                   0, NULL, NULL) >= 0)
    {
        // Ignore data if it is not valid

        if((device_status->data_status & (FGC_DATA_VALID | FGC_CLASS_VALID))
                                      != (FGC_DATA_VALID | FGC_CLASS_VALID) ||
            device_status->class_data.c53.state_op != FGC_OP_NORMAL)
        {
            continue;
        }

        // Process data appropriately based upon whether post-mortem trigger is currently armed

        if(armed)
        {
            // Trigger if state is FLT_STOPPING or FLT_OFF

            switch(device_status->class_data.c53.state_pc)
            {
                case FGC_PC_FLT_STOPPING: // Converter has tripped-off
                case FGC_PC_FLT_OFF:
                    if(sendPertuDataToPm(&pub_data, fgc_address))
                    {
                        fprintf(stderr, "Failed to send pertu data to post-mortem system\n");
                    }

                    // Disarm and wait for conditions to re-arm

                    armed = 0;
                    break;

                case FGC_PC_OFF: // Converter was switched-off normally

                    // Disarm and wait for conditions to re-arm

                    armed = 0;
                    break;
            }
        }
        else // Not armed for post-mortem trigger
        {
            // Arm if state is STARTING or above

            if(device_status->class_data.c53.state_pc >= FGC_PC_STARTING)
            {
                armed = 1;
            }
        }
    }

    // Receive failed

    close(sock);
    delete(pertu_pm.pm_client);
    exit(1);
}

////////////////////////////////////////////////////////////////////////////////

// EOF
