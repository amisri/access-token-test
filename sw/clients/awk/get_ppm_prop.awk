#! /usr/bin/awk -f

# get_ppm_prop.awk
#
# This script will read a PPM property from a device for all users and write the
# set commands to stdout that can be sent using FGCrun+ to transfer the values to another device.

BEGIN {

    if(ARGC != 3)
    {
        print "usage: get_ppm_prop.awk device ppm_property"
        print "\nTo get an RBAC token, first run:\n\neval $(rbac-authenticate -e)"
        exit -1
    }

    device = toupper(ARGV[1])
    property = toupper(ARGV[2])

    rda_get = "rda-get -r reuse -d " device " -p '" property

    # Read property for user 0 - 32

    RS = "#"

    for(user = 0 ; user <= 32 ; user++)
    {
        rda_get "(" user ")'" | getline prop_value

        num_prop_value_lines = split(prop_value, lines, "\n")

        for(i = 1 ; i < num_prop_value_lines ; i++)
        {
            if(index(lines[i],"Name : ") != 0)
            {
               ext = substr(lines[i], 8)

               if(ext != "value")
               {
                   prop_name = property "." ext "(" user ")"
               }
               else
               {
                   prop_name = property "(" user ")"
               }
            }
            else if(index(lines[i],"Value: ") != 0)
            {
                value = substr(lines[i], 8)

                if(value != "<>")
                {
                    gsub(/[<>]/,"",value)
                    gsub(/[ ]+$/,"",value)
                    gsub(/ /,",",value)
                    print "! S " prop_name " " value
                }
            }
        }
    }
}

# EOF
