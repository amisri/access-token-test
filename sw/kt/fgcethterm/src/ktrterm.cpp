/*
 *  Filename: ktrterm.cpp
 *
 *  Purpose:  The Function Generator Controller terminal client to be used with KT Gateway
 *
 *  Author:   Joao Afonso (adapted from: fgcterm.cpp, original author: Stephen Page)
 */

#include <arpa/inet.h>
#include <limits.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>

extern char *optarg;

using namespace std;

//#define FGCTERM

#include "fgc_consts_gen.h"
#include "ktrterm.h"
#include "ktrtermVersion.h"

#define BUFFER_SIZE_MAX (FGC_MAX_CMD_LEN + FGC_MAX_VAL_LEN + 6)

int main(int argc, char **argv)
{
    char            c;
    static char     command_cmd[FGC_MAX_CMD_LEN];
    static char     command_val[FGC_MAX_VAL_LEN];
    static char     command_buf[BUFFER_SIZE_MAX];
    static char     getSet;
    uint32_t        command_length;

    fgcterm.check_conn = false;
    fgcterm.verbose    = false;

    fgcterm.die                     = 0;
    fgcterm.initial_rterm           = NULL;
    fgcterm.input                   = stdin;
    fgcterm.recv_error              = 0;
    fgcterm.receive_thread_running  = 0;
    fgcterm.sock                    = -1;
    fgcterm.response                = 0;

    fgcterm.devname                 = NULL;
    fgcterm.num_files               = 0;
    fgcterm.filename                = NULL;
    fgcterm.num_arg_cmd             = 0;
    fgcterm.arg_cmd                 = NULL;

    if(pthread_mutex_init(&fgcterm.mutex, NULL) != 0)
    {
        perror("mutex");
        exit(1);
    }

    if(pthread_cond_init(&fgcterm.cond, NULL) != 0)
    {
        perror("mutex");
        exit(1);
    }

    // Process command-line options

    while((c = getopt(argc, argv, "?hcvs:l:")) != -1)
    {
        switch(c)
        {
            case '?':
            case 'h':
                fprintf(stderr, "Usage: %s [-v] gateway_name [fgc_name] [file_name|command]*\n%s",
                                argv[0],
                                ""
                                //"    -f <file>              Send file instead of interactive terminal\n"
                                //"    -l <log file>          File to which to log terminal\n"
                                //"    -r <channel>           Start a remote terminal to channel\n"
                                //"    -c                     Apply initial handshake protocol"
                       );
                exit(1);

            case 'c':
                fgcterm.check_conn = true;
                break;

            case 'l':
                if(!(fgcterm.logfile = fopen(optarg, "w")))
                {
                    perror("Open log file");
                    exit(1);
                }
                break;

            case 'v': 
                // Print version
                printf("FGC Ether Terminal - Version %s\n", getVersionStr());
                exit(0);
                break; 
        }
    }

    // Check that hostname was specified as an argument

    if(!(argc - optind))
    {
        fprintf(stderr, "You must specify a hostname.\n");
        exit(1);
    }

    fgcterm.hostname = argv[optind++];

    if(argc - optind)
    {
        fgcterm.devname  = argv[optind++];

        if(argc - optind)
        {
            char setget;
            char setget_buf[16];

            // Check if 3rd comand line argument is a command or a file name

            if(sscanf(argv[optind], "%c %15s", &setget, setget_buf) == 2 && (setget == 's' || setget == 'S' || setget == 'g' || setget == 'G'))
            {
                fgcterm.num_arg_cmd = argc - optind;
                fgcterm.arg_cmd     = &argv[optind++];
            }
            else
            {
                fgcterm.num_files   = argc - optind;
                fgcterm.filename    = &argv[optind++];
            }
        }
    }

    // Initialise terminal variables

    tcgetattr(STDIN_FILENO, &fgcterm.line_mode);
    fgcterm.raw_mode = fgcterm.line_mode;

    fgcterm.raw_mode.c_iflag       &= ~(INLCR|ICRNL|IXON);
    fgcterm.raw_mode.c_oflag       &= ~(OPOST);
    fgcterm.raw_mode.c_lflag        = 0;
    fgcterm.raw_mode.c_cc[VMIN]     = 1;
    fgcterm.raw_mode.c_cc[VTIME]    = 0;

    if((fgcterm.check_conn ? ConnectToSvr(fgcterm.hostname, PORT) : ConnectToSvr(fgcterm.hostname, PORT_NO_CHECK)) != 0)
    {
        fprintf(stderr, "Unable to connect to %s:%i\n", fgcterm.hostname, fgcterm.check_conn ? PORT : PORT_NO_CHECK);
        exit(1);
    }

    // Catch signals to clean-up

    signal(SIGHUP,  CleanUp);
    signal(SIGINT,  CleanUp);
    signal(SIGPIPE, CleanUp);
    signal(SIGQUIT, CleanUp);
    signal(SIGTERM, CleanUp);

    // Check that gateway is not busy

    if(fgcterm.check_conn)
    {
        recv(fgcterm.sock, &c, 1, 0);
        if(c != '+')
        {
            fprintf(stderr, "Gateway %s is busy\n", fgcterm.hostname);
            CleanUp(0);
        }
        send(fgcterm.sock, "+", 1, 0);
    }

    if(!fgcterm.arg_cmd)
    {
        printf("Connected to gateway %s\n", fgcterm.hostname);
    }

    // Attempt to connect to remote terminal if command-line option specified

    fgcterm.rterm_open = false;

    if(fgcterm.devname && !fgcterm.filename && !fgcterm.arg_cmd)
    {
        command_length = snprintf(command_buf, sizeof(command_buf),
                                  "! s :client.rterm %s\n",
                                  fgcterm.devname);
        send(fgcterm.sock, command_buf, command_length, 0);

        while(recv(fgcterm.sock, &c, 1, 0) && c != ';')
        {
            if(c == '!')
            {
                fprintf(stderr, "Remote terminal request denied\n");
                CleanUp(0);
            }
        }

        fgcterm.rterm_open = true;
        printf("Connected to device %s\n", fgcterm.devname);
        printf("\n--- Press ESC twice ---\n");
    }

    // Spawn receive thread

    if(pthread_create(&fgcterm.receive_thread, NULL, (void *(*)(void *))Receive, 0))
    {
        perror("pthread_create");
        CleanUp(0);
    }

    // Read bytes and send over socket

    if(!fgcterm.filename && !fgcterm.arg_cmd)
    {
        while(fread(&c, 1, 1, fgcterm.input) && !fgcterm.die)
        {
            switch(c)
            {
                case RTERM_DISCONNECT_CHAR:

                    if(fgcterm.devname)
                    {
                        CleanUp(SIGQUIT);
                        break; //TODO: 'break' has to be moved here to allow opening and closing rterm multiple times...
                    }
                    //break;

                default:
                    
                    send(fgcterm.sock, &c, 1, 0);
                    break;
            }
        }
    }
    else
    {
        if(fgcterm.arg_cmd)
        {
            for(uint32_t i = 0; i < fgcterm.num_arg_cmd && !fgcterm.die; i++)
            {
                snprintf(command_buf, BUFFER_SIZE_MAX, "! %s\n", fgcterm.arg_cmd[i]);
                command_buf[BUFFER_SIZE_MAX - 1] = '\0';
                int ret = parseCmd(command_buf, &getSet, command_cmd, command_val);

                if(ret != 0)
                {
                    if(ret < 0)
                    {
                        printf("[Unable to parse] %s\n", command_buf);
                    }

                    continue;
                }

                // Print and send parsed command, in upper letters

                snprintf(command_buf, BUFFER_SIZE_MAX, "! %c %s:%s %s\n", getSet, fgcterm.devname, command_cmd, command_val);
                for(uint32_t j = 0; command_buf[j] != '\0'; j++) command_buf[j] = toupper(command_buf[j]);

                send(fgcterm.sock, command_buf, strlen(command_buf), 0);

                // Wait for response to be printed bu recv thread

                // pthread_mutex_lock(&fgcterm.mutex);

                // while(!fgcterm.response)
                // {
                //     pthread_cond_wait(&fgcterm.cond,&fgcterm.mutex);
                // }

                // fgcterm.response = 0;

                // pthread_mutex_unlock(&fgcterm.mutex);
            }

            pthread_mutex_lock(&fgcterm.mutex);

            while(fgcterm.response < fgcterm.num_arg_cmd)
            {
                pthread_cond_wait(&fgcterm.cond,&fgcterm.mutex);
            }

            fgcterm.response = 0;

            pthread_mutex_unlock(&fgcterm.mutex);

        }
        else if(fgcterm.filename)
        {
            printf("Sending commands from file(s) to device %s\n\n", fgcterm.devname);

            for(uint32_t i = 0; i < fgcterm.num_files && !fgcterm.die; i++)
            {
                if(!(fgcterm.input = fopen(fgcterm.filename[i], "r")))
                {
                    perror("Open file");
                    exit(1);
                }

                while(fgets(command_buf, sizeof(command_buf), fgcterm.input) && !fgcterm.die)
                {
                    command_buf[BUFFER_SIZE_MAX - 1] = '\0';
                    int ret = parseCmd(command_buf, &getSet, command_cmd, command_val);

                    if(ret != 0)
                    {
                        if(ret < 0)
                        {
                            printf("\n[Unable to parse] %s\n", command_buf);
                        }

                        continue;
                    }

                    // Print and send parsed command, in upper letters

                    snprintf(command_buf, BUFFER_SIZE_MAX, "! %c %s:%s %s\n", getSet, fgcterm.devname, command_cmd, command_val);
                    for(uint32_t j = 0; command_buf[j] != '\0'; j++) command_buf[j] = toupper(command_buf[j]);

                    printf("%s", command_buf);
                    send(fgcterm.sock, command_buf, strlen(command_buf), 0);

                    // Wait for response to be printed bu recv thread

                    pthread_mutex_lock(&fgcterm.mutex);

                    while(!fgcterm.response)
                    {
                        pthread_cond_wait(&fgcterm.cond,&fgcterm.mutex);
                    }

                    fgcterm.response = 0;

                    pthread_mutex_unlock(&fgcterm.mutex);

                }

                fclose(fgcterm.input);
            }
        }
    }

    CleanUp(SIGQUIT);
}

void Receive(void)
{
    uint8_t  c;
    uint32_t i = 0;
    static char     response_buf[FGC_MAX_CMD_LEN + 1 + FGC_MAX_VAL_LEN + 1];
    static char     response[FGC_MAX_CMD_LEN + 1 + FGC_MAX_VAL_LEN + 1];

    fgcterm.receive_thread_running = 1;

    // Read characters from socket and write to stdout

    if(!fgcterm.filename && !fgcterm.arg_cmd)
    {
        while(!fgcterm.recv_error && (recv(fgcterm.sock, &c, 1, 0)))
        {
            // Write character to stdout if it is not a telnet escape
            switch(c)
            {
                case TELNET_ESC:
                    HandleEscape();
                    break;
            
                default:
    
                    if(write(STDOUT_FILENO, &c, 1) != 1)
                    {
                        perror("Writing output");
                    }

                    if(fgcterm.logfile)
                    {
                        fputc(c, fgcterm.logfile);
                        fflush(fgcterm.logfile);
                    }
                    break;
            }
        }
    }
    else 
    {
        char p_rsp_format[128];

        if(fgcterm.arg_cmd)
        {
            snprintf(p_rsp_format, 128, "%%s");
        }
        else
        {
            snprintf(p_rsp_format, 128, "\n%%s\n");
        }

        i = 0;
        
        while(!fgcterm.recv_error && (recv(fgcterm.sock, &c, 1, 0)))
        {
            response_buf[i++] = c;

            if(c == 0xFF)
            {
                // If binary data is found, read all at once.

                uint32_t bin_size;

                if(recv(fgcterm.sock, &bin_size, 4, 0) != 4)
                {
                     perror("Reading binary data size");
                }

                if(write(STDOUT_FILENO, &bin_size, 4) != 4)
                {
                     perror("Writing output");
                }

                bin_size = ntohl(bin_size);

                while(bin_size > 0)
                {
                    uint8_t c_bin;
                    recv(fgcterm.sock, &c_bin, 1, 0);
                    if(write(STDOUT_FILENO, &c_bin, 1) != 1)
                    {
                        perror("Writing output");
                    }
                    bin_size--;
                }

                char term_chars[2];
                recv(fgcterm.sock, term_chars, 2, 0);
                if(term_chars[0] != '\n' || term_chars[1] != ';')
                {
                    perror("Binary data reading not terminated with '\n;'");
                }

                i = 0;

                pthread_mutex_lock(&fgcterm.mutex);
                fgcterm.response++;
                pthread_cond_signal(&fgcterm.cond);
                pthread_mutex_unlock(&fgcterm.mutex);
            }
            else if(c == ';')
            {
                if(sscanf(response_buf, "$ ! %[^;];", response) == 1)
                {
                    printf("[ERROR] %s", response);
                }
                else if(sscanf(response_buf, "$ . %[^;];", response) == 1)
                {
                    printf(p_rsp_format, response);
                }

                i = 0;

                pthread_mutex_lock(&fgcterm.mutex);
                fgcterm.response++;
                pthread_cond_signal(&fgcterm.cond);
                pthread_mutex_unlock(&fgcterm.mutex);
            }
        }
    }

    fgcterm.receive_thread_running = 0; // This line was missing
    fprintf(stderr, "\33cGateway closed connection unexpectedly.\r");
    CleanUp(0);
}

void HandleEscape(void)
{
    uint8_t buf[2];

    if(!(recv(fgcterm.sock, &buf[0], 1, 0) &&
         recv(fgcterm.sock, &buf[1], 1, 0)))
    {
        fgcterm.recv_error = 1;
        return;
    }

    switch(buf[0])
    {
        case TELNET_WILL:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // End session if remote terminal option was specified

                    if(fgcterm.initial_rterm)
                    {
                        CleanUp(0);
                    }
                    else
                    {
                        // Enable line-mode

                        tcsetattr(STDIN_FILENO, 0, &fgcterm.line_mode);
                    }

                    break;
            }
            break;

        case TELNET_DO:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // End session if remote terminal option was specified

                    if(fgcterm.initial_rterm)
                    {
                        CleanUp(0);
                    }
                    else
                    {
                        // Enable line-mode

                        tcsetattr(STDIN_FILENO, 0, &fgcterm.line_mode);
                    }

                    break;
            }
            break;

        case TELNET_WONT:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // Disable line-mode

                    tcsetattr(STDIN_FILENO, 0, &fgcterm.raw_mode);

                    break;
            }
            break;

        case TELNET_DONT:
            switch(buf[1])
            {
                case TELNET_LINE:

                    // Disable line-mode

                    tcsetattr(STDIN_FILENO, 0, &fgcterm.raw_mode);

                    break;
            }
            break;
    }
}

int ConnectToSvr(char *hostname, uint16_t port_nr)
{
    struct hostent      *host;
    struct sockaddr_in  sin;

    memset((void *)&sin, 0, sizeof(sin));

    if((host = gethostbyname(hostname)) == NULL)
    {
        perror("gethostbyname");
        return(1);
    }

    sin.sin_addr   = *((struct in_addr *)host->h_addr);
    sin.sin_family = AF_INET;
    sin.sin_port   = htons(port_nr);

    if((fgcterm.sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        return(2);
    }

    if(connect(fgcterm.sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("connect");
        return(3);
    }

    return(0);
}

int parseCmd(char * input, char * getSet, char * command_cmd, char * command_val)
{
    int pos = 0;
    int posp, i;

    // Check if string is empty, or first char is '#'

    for(pos = 0; input[pos] != '\0' && isspace(input[pos]); pos++);
    if(input[pos] == '\0') return 1;
    if(input[pos] == '#') return 1;

    // Ignore characters before '!'

    for(; input[pos] != '\0' && input[pos] != '!'; pos++);
    if(input[pos] == '\0')
    {
        return -1;
    }

    // Command type (Set or Get)

    if(sscanf(&input[pos], "! %c %n", getSet, &posp) != 1 || (toupper(*getSet) != 'S' && toupper(*getSet) != 'G'))
    {
        return -1;
    }

    pos += posp;

    // Command string

    if(sscanf(&input[pos], "%s %n", command_cmd, &posp) != 1)
    {
        return -1;
    }

    pos += posp;

    for(i = 0; command_cmd[i] != ':' && command_cmd[i] != '\0'; i++); // Check if ':' exists. Ignore it and characters before
    if(command_cmd[i] == ':')
    {
        memmove(command_cmd, &command_cmd[i + 1], strlen(&command_cmd[i + 1]) + 1); // Shift all characters, including NULL character.
    }

    // Value or parameters

    command_val[0] = '\0';
    strcpy(command_val, &input[pos]); // Value may be non-existant both for Set and Get commands.
    for(int i = strlen(command_val) - 1; i >= 0 && isspace(command_val[i]); i--) // Remove trailing whitespaces
    {
        command_val[i] = '\0';
    }

    return 0;
}

void CleanUp(int sig_num)
{
    // Ignore further signals

    signal(SIGHUP,  SIG_IGN);
    signal(SIGINT,  SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    signal(SIGTERM, SIG_IGN);

    if(!fgcterm.die)
    {
        fgcterm.die = 1;

        // Stop receive thread

        if(fgcterm.receive_thread_running)
        {
            if(!pthread_equal(fgcterm.receive_thread, pthread_self()))
            {
                pthread_cancel(fgcterm.receive_thread);
                pthread_join(fgcterm.receive_thread, NULL);
            }
            else
            {
                pthread_detach(fgcterm.receive_thread);
            }
        }

        // Close socket if open

        if(fgcterm.sock >= 0)
        {
            close(fgcterm.sock);
        }

        // Close log file if open

        if(fgcterm.logfile)
        {
            fclose(fgcterm.logfile);
        }

        // Restore terminal to line-mode

        tcsetattr(STDIN_FILENO, 0, &fgcterm.line_mode);

        if(fgcterm.rterm_open)
        {
            printf("\33c");
        }

        exit(sig_num ? 0 : 1);
    }
}

// EOF

