/*
 *  Filename: ktrterm.h
 *
 *  Purpose:  Declarations for the Function Generator Controller terminal (to be used with KT Gateway)
 *
 *  Author:   Joao Afonso (adapted from: fgcterm.cpp, original author: Stephen Page)
 */

#ifndef KTRTERM_H
#define KTRTERM_H

#include <stdio.h>
#include <semaphore.h>

#include "fgc_consts_gen.h"

#define FGC_MAX_PASS_LEN        15

#define PORT                    FGC_GW_PORT
#define PORT_NO_CHECK           (FGC_GW_PORT + 1)

#define RTERM_DISCONNECT_CHAR   0x18

#define TELNET_ESC              0xFF

#define TELNET_WILL             0xFB
#define TELNET_WONT             0xFD
#define TELNET_DO               0xFC
#define TELNET_DONT             0xFE

#define TELNET_LINE             0x22

// Static functions

static void CleanUp(int sig_num);
static int  ConnectToSvr(char *hostname, uint16_t port_nr);
static void HandleEscape(void);
static void Receive(void);
static int parseCmd(char * input, char * getSet, char * command_cmd, char * command_val);

// External functions

// Struct containing global variables

struct fgcterm
{
    char            *initial_rterm;
    int             die;
    FILE            *input;
    FILE            *logfile;
    int             recv_error;
    int             sock;
    int             receive_thread_running;
    pthread_t       receive_thread;
    struct termios  line_mode;
    struct termios  raw_mode;
    pthread_mutex_t mutex;
    pthread_cond_t  cond;
    uint32_t        response;
    char            *hostname;
    char            *devname;
    char            **filename;
    uint32_t        num_files;
    bool            check_conn;
    bool            verbose;
    bool            rterm_open;
    char            **arg_cmd;
    uint32_t        num_arg_cmd;
} fgcterm;

#endif

// EOF

