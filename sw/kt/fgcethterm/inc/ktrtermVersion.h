#ifndef KTRTERM_VERSION_H
#define KTRTERM_VERSION_H

#include <stdio.h>

const char * getVersionStr()
{
#ifdef KTRTERM_VERSION
#define STRINGIFY2(x) #x
#define STRINGIFY(x) STRINGIFY2(x)
    return STRINGIFY(KTRTERM_VERSION);
#else
    return "unknown";
#endif
}

#endif

// EOF
