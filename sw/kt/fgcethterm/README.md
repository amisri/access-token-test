# FGC Ether terminal

FGC Ether terminal, also known as fgcethterm, is a software running on Linux that opens a remote terminal session with any FGC under the control of the FGC Ether gateway. 

It also allows sending batches of commands using NCRP (network command/response protocol).

## Build and install

0) Extract the tarball to the preferred directory.

- Mode to the extracted directory.

1) Configure the FGC software top directory:

- In file ./CONFIG, modify FGCETH_HOME to the desired top directory path.

2) Build the FGC Ether terminal:
```
make
```

3) Install: 

- To copy fgcethterm to the installation directory:
```
make install
```
- Previous executables will be backed up in old/.


## Running

### Prerequisites

- FGC Ether gateway must be running and connected to the FGCs.

### Running

To display fgcethterm options run:
```
<top>/bin/fgcethterm -h
```

Command examples:

- Open remote terminal to a FGC:
```
<top>/bin/fgcethterm <gateway_name> <fgc_name|fgc_id>
```

- Send a command to a FGC (multiple commands can be chained):
```
<top>/bin/fgcethterm <gateway_name> <fgc_name|fgc_id> <command>*
```

- Send all the commands from a file(s):
```
<top>/bin/fgcethterm <gateway_name> <fgc_name/fgc_id> <command_file>*
```

- Open NCRP raw connection to FGC Ether gateway (expert tool):
```
<top>/bin/fgcethterm <gateway_name>
```

Note:
  Commands (in and outside file) follow SCRP protocol:
```
"S PROPERTY VALUE" 
"G PROPERTY GET-OPTS"
```


# EOF
