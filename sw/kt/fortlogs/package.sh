#!/bin/bash

destination=$(pwd)

project_name="fortlogs"
project_base_path="$(pwd)/../../clients/python/"

offline_packages_dir="$(pwd)/offline_packages"

client_modules="pyfgc_const==0.0.3 fortlogs_schemas==1.3.12"

main() {
    get_packages_from_accpy
    package
}

get_packages_from_accpy() {
    source /acc/local/share/python/acc-py/pro/setup.sh
    echo "Getting packages from accpy..."
    if [[ -d $offline_packages_dir ]]; then
        rm $offline_packages_dir/*
    else
        mkdir $offline_packages_dir
    fi

    for mod in $client_modules; do
        echo "    Getting $mod..."
        pip download --no-deps $mod -d $offline_packages_dir
    done
}

package_project() {
    mkdir $pkg/$project_name
    cp $project_base_path/$project_name/scripts/fortlogs.service $pkg/$project_name
    cp $project_base_path/$project_name/scripts/gunicorn.conf.py $pkg/$project_name
    cp $project_base_path/$project_name/requirements.txt $pkg/$project_name
    cp -r $project_base_path/$project_name/$project_name $pkg/$project_name
    cp $project_base_path/$project_name/scripts/configs/config_kt.yml $pkg/$project_name
}

package() {
    echo "Packaging..."
    cd ${destination}
    pkg=${project_name}_$(date +%s)
    mkdir $pkg
    package_project
    cp -r "offline_packages" "install.sh" "README.md" $pkg
    tar czvf ${pkg}.tar.gz $pkg
    rm -r $pkg
    rm -r "offline_packages"
}

main
