#!/bin/bash

project_name="fortlogs"

kt_dependencies="psycopg2"
centos_dependencies="python3 postgresql-libs postgresql-devel python3-psycopg2 python3-devel"
debian_dependencies="python3 python3-pip python3-venv python3-psycopg2 postgresql libpq-dev"


main() {
    install_packages_if_needed
    init_installation_folder
    create_virtual_environment
    source_virtual_environment
    install_dependencies
    configure_db
    configure_kt
    generate_systemd_service
    #generate_apache_conf
    #configure_apache
}

output() {
    END="\e[0m"
    echo -e "$1$2$END"
}

output_in_red() {
    RED="\e[01;31m"
    output $RED "$1"
}

output_in_yellow() {
    YELLOW="\e[01;33m"
    output $YELLOW "$1"
}

confirm() {
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        [nN][oO]|[nN])
            false
            ;;
        *)
            $2
            ;;
    esac
}

install_packages_if_needed() {
    echo "Checking if needed packages are installed..."
    if [ -n "$(command -v yum)" ]; then
        for pkg in $centos_dependencies; do
            if ! yum_isinstalled $pkg; then sudo yum -y -q install $pkg; fi
        done
    else
        for pkg in $debian_dependencies; do
            if ! apt_isinstalled $pkg; then sudo apt -y -qq install $pkg; fi
        done
    fi
}

yum_isinstalled () {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

apt_isinstalled () {
  if apt list "$@" --installed 2>/dev/null | grep "$@"; then
    true
  else
    false
  fi
}

init_installation_folder() {
    read -e -p "Please enter the full installation base path (Default: /opt/fgc): " path
    path=${path:-/opt/fgc}
    path="${path/#\~/$HOME}"
    path=${path%/}
    project_path="${path}/${project_name}"
    if [[ ! -d $project_path ]]; then
        mkdir $project_path
    fi

    output "Copying files to installation path..."
    cp -r $project_name $project_path
}


create_virtual_environment() {
    env_dir="${project_name}_env"
    pythonenv=${project_path}/$env_dir
    if [[ -d "$pythonenv" ]]; then
        output_in_yellow "Virtual environment directory exists already. Will not create a new one! ($pythonenv)"
    else
        echo "Creating virtual environment: $pythonenv"
        python3 -m venv $pythonenv
    fi
}

source_virtual_environment() {
    echo "Switching to virtual enviroment: $pythonenv"
    source $pythonenv/bin/activate
}

install_dependencies() {
    pip3 install -q --upgrade pip
    echo "Installing dependencies from requirements.txt..."
    pip3 install -q -f offline_packages -r "$project_name"/requirements.txt
    for dep in $kt_dependencies; do 
        pip3 install -q $dep
    done
}

configure_kt() {
    read -p "Please enter your lab name: " lab_name
    read -p "Please enter the powerspy hostname: " powerspy_hostname

    settings_file=${project_path}/${project_name}/config.yml
    python -c "import yaml;f=open(\"${settings_file}\");y=yaml.safe_load(f);y[\"x-origins\"] = \"${powerspy_hostname}\";y[\"kt\"][\"username\"] = \"${lab_name}\"; y[\"kt\"][\"name\"] = \"${lab_name}\"; f.close(); f=open(\"${settings_file}\",\"w\"); f.write(yaml.dump(y,default_flow_style=False,sort_keys=False)); f.close()"
}

configure_db() {
    if [[ -f ${project_path}/${project_name}/config.yml ]]; then
        output_in_yellow "Configuration file ${project_path}/${project_name}/config.yml already exists. Skipping configuration..."
    else
        echo "Setting up database..."
        db_read_parameters
        db_test_parameters
        db_update_dbconf
    fi
}

db_read_parameters() {
    read -p "    Hostname: " hostname
    read -p "    Port: " port
    #read -p "    Database: " database
    database="postgres"
    read -p "    Username: " username
    read -s -p "    Password: " password
}

#TODO: prompt to re-read the parameters
db_test_parameters() {
    echo -e "\nTesting database connection..."

    result=$(PGPASSWORD=$password psql -h $hostname -p $port -U $username $database -t -c 'SELECT 5432;')
    if [[ $result -ne "5432" ]]; then
       output_in_red "Database connection failed! Establishing connection with the db is necessary for the installation!" 
       output "Exiting setup..."
       exit
    else
        output "Database connection successful!"
    fi
}

db_update_dbconf() {
    settings_file=${project_path}/${project_name}/config.yml
    cp ${project_path}/${project_name}/config_kt.yml $settings_file

    python -c "import yaml;f=open(\"${settings_file}\");y=yaml.safe_load(f);y[\"pg_database\"][\"user\"] = \"${username}\"; y[\"pg_database\"][\"pass\"] = \"${password}\"; y[\"pg_database\"][\"host\"] = \"${hostname}\"; y[\"pg_database\"][\"port\"] = \"${port}\"; f.close(); f=open(\"${settings_file}\",\"w\"); f.write(yaml.dump(y,default_flow_style=False,sort_keys=False)); f.close()"

    #sed -i "2s/.*/name=\"${database}\"/" $settings_file
    #sed -i "3s/.*/host=\"${hostname}\"/" $settings_file
    #sed -i "4s/.*/port=\"${port}\"/" $settings_file
    #sed -i "5s/.*/username=\"${username}\"/" $settings_file
    #sed -i "6s/.*/password=\"${password}\"/" $settings_file
}

generate_systemd_service() {
    service="${project_name}.service"

    if [[ -f /etc/systemd/system/$service ]]; then
        output_in_yellow "$project_name service /etc/systemd/system/$service already exists. Skipping creation..."
    else
        echo "Generating ${service}..."

        if [[ -f $service ]]; then
            rm $service
        fi

        echo "[Unit]" | tee -a $service > /dev/null
        echo "Description=FortLogs" | tee -a $service > /dev/null
        echo -e "After=network.target\n" | tee -a $service > /dev/null

        echo "[Service]" | tee -a $service > /dev/null
        echo "PermissionsStartOnly = true" | tee -a $service > /dev/null
        #echo "User = root" | tee -a $service > /dev/null
        echo "WorkingDirectory=${project_path}/${project_name}" | tee -a $service > /dev/null
        echo "ExecStart=${project_path}/${project_name}_env/bin/gunicorn -c ${project_path}/${project_name}/gunicorn.conf.py fortlogs.main:app" | tee -a $service > /dev/null
        echo "ExecReload = /bin/kill -s HUP \$MAINPID" | tee -a $service > /dev/null
        echo "ExecStop = /bin/kill -s TERM \$MAINPID" | tee -a $service > /dev/null
        echo -e "PrivateTmp = true\n" | tee -a $service > /dev/null

        echo "[Install]" | tee -a $service > /dev/null
        echo "WantedBy=multi-user.target" | tee -a $service > /dev/null

        install_service=1
        confirm "Do you want to install and enable ${project_name} service to systemd (location: /etc/systemd/system/) ? [Y/n]: " true || install_service=0
        if [[ $install_service -eq 1 ]]; then
            echo "Enabling ${service}..."
            sudo cp $service /etc/systemd/system/
            sudo systemctl enable --now $project_name
        fi
    fi
}

generate_apache_conf() {
    echo "Generating ${project_name}.conf for Apache..."
    apache_conf="${project_name}.conf"

    if [[ -f $apache_conf ]]; then
        rm $apache_conf
    fi

    def_host_name=$(hostname -a)
    read -p "Please enter the server name (Default: $def_host_name): " host_name
    host_name=${host_name:-$def_host_name}

    echo -e "Define SERVER_NAME $host_name" | tee -a $apache_conf > /dev/null
    echo -e "Define SERVER_DOCUMENT_ROOT /var/www/html\n" | tee -a $apache_conf > /dev/null
    #read -p "Please enter the e-mail address of the server admin: " apache_mail
    #apache_mail=${apache_mail:-""}
    #echo -e "Define SERVER_ADMIN $apache_mail" | tee -a $apache_conf > /dev/null

    #ask for ssl
    use_ssl=1
    confirm "Will you use SSL? [Y/n]: " true || use_ssl=0

    if [ $use_ssl -eq "1" ]; then
        echo -e "Define SERVER_SSL_CERTIFICATE /opt/fgc/ssl/localhost.crt " | tee -a $apache_conf > /dev/null
        echo -e "Define SERVER_SSL_KEYFILE /opt/fgc/ssl/localhost.key\n\n" | tee -a $apache_conf > /dev/null
    fi

    if [ $use_ssl -eq "1" ]; then
        # for https redirect
        echo "<VirtualHost *:80>" | tee -a $apache_conf > /dev/null
        echo -e "\tServerName \${SERVER_NAME}" | tee -a $apache_conf > /dev/null
        echo -e "\tServerAdmin \${SERVER_ADMIN}" | tee -a $apache_conf > /dev/null
        echo -e "\tDocumentRoot  \${SERVER_DOCUMENT_ROOT}" | tee -a $apache_conf > /dev/null
        echo -e "\tRedirect permanent / \${SERVER_NAME}:/" | tee -a $apache_conf > /dev/null
        echo -e "</VirtualHost>\n" | tee -a $apache_conf > /dev/null
    fi

    if [ $use_ssl -eq "1" ]; then
        echo "<VirtualHost *:443>" | tee -a $apache_conf > /dev/null
    else
        echo "<VirtualHost *:80>" | tee -a $apache_conf > /dev/null
    fi
    echo -e "\tServerName \${SERVER_NAME}" | tee -a $apache_conf > /dev/null
    echo -e "\tDocumentRoot  \${SERVER_DOCUMENT_ROOT}" | tee -a $apache_conf > /dev/null
    echo -e "\tProtocols h2" | tee -a $apache_conf > /dev/null
    if [ $use_ssl -eq "1" ]; then
        echo -e "\tSSLEngine On" | tee -a $apache_conf > /dev/null
        echo -e "\tSSLCertificateFile \${SERVER_SSL_CERTIFICATE}" | tee -a $apache_conf > /dev/null
        echo -e "\tSSLCertificateKeyFile \${SERVER_SSL_KEYFILE}" | tee -a $apache_conf > /dev/null
    fi
    echo -e "\n\t# FortLogs" | tee -a $apache_conf > /dev/null
    echo -e "\tProxyPass         /fortlogs http://localhost:57012" | tee -a $apache_conf > /dev/null
    echo -e "\tProxyPassReverse  /fortlogs http://localhost:57012" | tee -a $apache_conf > /dev/null

    echo "</VirtualHost>" | tee -a $apache_conf > /dev/null


    output_in_yellow "Please configure the apache manually! Use the ${project_name}.conf generated file as a template!"
    if [ $use_ssl -eq "1" ]; then
        output_in_yellow "And don't forget to setup the SERVER_SSL_*"
    fi
}

configure_apache() {
    sudo setsebool -P httpd_can_network_connect_db 1
    sudo setsebool -P httpd_can_network_connect 1
}

main
