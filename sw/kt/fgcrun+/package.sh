#!/bin/bash

project_name="fgcrun+"
destination=$(pwd)

perl_lib_modules="$(pwd)/../../clients/perl/FGC"

main() {
    run_make
    package
}

run_make() {
    cd ../../../def
    make
    cd $destination
}

package_project() {
    mkdir $pkg/$project_name
    mkdir $pkg/$project_name/lib

    cp fgcrun+.pl $pkg/$project_name
    #cp fgcrun+.sh $pkg/$project_name
    cp -r $perl_lib_modules $pkg/$project_name/lib
}

package() {
    echo "Packaging..."
    cd ${destination}
    pkg=${project_name}_$(date +%s)
    mkdir $pkg
    package_project

    cp -r "install.sh" "README.md" $pkg
    tar czvf ${pkg}.tar.gz $pkg
    rm -r $pkg
}

main
