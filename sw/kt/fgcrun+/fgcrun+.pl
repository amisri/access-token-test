#!/usr/bin/perl -w
#
# Name:     fgcrun+.pl
# Purpose:  Control and administer FGC devices
# Author:   Stephen Page

use FGC::Async;
use FGC::Boot;
use FGC::Components;
use FGC::Consts;
use FGC::DB;
use FGC::DeviceList;
use FGC::Errors;
use FGC::IntRegulation;
use FGC::Names;
use FGC::Properties;
use FGC::RegFgc3ParsConv;
use FGC::Runlog;
use FGC::Sub;
use FGC::Sync;
use FGC::UIElements;
use FGC::Utils;
use File::Basename;
use File::Copy;
use File::HomeDir qw(home);
use File::Temp 'tempfile';
use FileHandle;
use IO::Socket;
use List::Util qw(min max);
use Mail::Sendmail;
use Math::Round qw(nhimult);
# use POSIX qw(mkfifo strftime tmpnam);
use POSIX qw(mkfifo strftime);
use Readonly;
use Scalar::Util qw(looks_like_number);
use Statistics::Descriptive;
use Storable;
use strict;
use Sys::Hostname;
use Tk;
use Tk::Balloon;
use Tk::HList;
use Tk::ItemStyle;
use Tk::Pane;
use Tk::PNG;
use Tk::Table;
use Tk::widgets qw(JPEG);

use constant SUB_PERIOD => FGC_FIELDBUS_CYCLE_FREQ;

# Short strings to describe states

my %state_op_short      =   (
                                BOOT            => 'BT',
                                CALIBRATING     => 'CL',
                                NORMAL          => 'NL',
                                SIMULATION      => 'SM',
                                TEST            => 'TT',
                                UNCONFIGURED    => 'UC',
                            );

my %state_pc_short      =   (
                                ABORTING        => 'AB',
                                ARMED           => 'AR',
                                BLOCKING        => 'BK',
                                CYCLING         => 'CY',
                                DIRECT          => 'DT',
                                ECONOMY         => 'EC',
                                FLT_OFF         => 'FO',
                                FLT_STOPPING    => 'FS',
                                IDLE            => 'IL',
                                OFF             => 'OF',
                                ON_STANDBY      => 'SB',
                                POL_SWITCHING   => 'PL',
                                PPM             => 'PP',
                                RUNNING         => 'RN',
                                SLOW_ABORT      => 'SA',
                                STARTING        => 'ST',
                                STOPPING        => 'SP',
                                TO_CYCLING      => 'TC',
                                TO_STANDBY      => 'TS',
                                TO_PPM          => 'TP',
                                PAUSED          => 'PD',
                            );

my %state_pll_short     =   (
                                CAPTURE         => 'CP',
                                FAILED          => 'FL',
                                FAST_SLEW       => 'FS',
                                LOCKED          => 'LK',
                                NO_SYNC         => 'NS',
                            );

my %state_vs_short      =   (
                                BLOCKED         => 'BK',
                                FAST_STOP       => 'FS',
                                FASTPA_OFF      => 'PA',
                                FLT_OFF         => 'FO',
                                INVALID         => 'IV',
                                OFF             => 'OF',
                                READY           => 'RD',
                                STARTING        => 'ST',
                                STOPPING        => 'SP',
                                NONE            => 'NO',
                            );

# Globals

my $device_sort_func    = sub { $a->{name} cmp $b->{name} };
my $devices;
my $font                = 'courier 8';
my $gateway_groups;
my $gateways;
my %gateways_connected;
my %gateways_by_ip;
my @history;
my %list_editors;
my @list_editor_history;
my %main;
my $prog_name           = basename($0);
my $udp_port_number;
my $udp_socket;

my $cmd_variable_help   = "\$rsp    Response to previous command for device\n".
                          "\$time   Current unixtime + 10 seconds";

my $expression_help     = "=   Equals\n".
                          "!=  Does not equal\n".
                          "\n".
                          "<   Less than\n".
                          "<=  Less than or equal to\n".
                          ">   Greater than\n".
                          ">=  Greater than or equal to\n".
                          "\n".
                          "|<  Absolute value less than\n".
                          "|>  Absolute value greater than\n".
                          "\n".
                          "=~  Contains (regex)\n".
                          "!~  Does not contain (regex)";

my $shortcut_help       = "Ctrl-A:          Select all devices\n".
                          "Ctrl-a:          Select active devices\n".
                          "Ctrl-m:          Select gateways\n".
                          "Ctrl-F:          Select FGCs\n".
                          "Ctrl-f:          Select active FGCs\n".
                          "Ctrl-b:          Select FGCs in the boot program\n".
                          "Ctrl-e:          Select devices with error responses\n".
                          "Ctrl-~:          Invert selection\n".
                          "Shift-Escape:    Clear selection\n".
                          "\n".
                          "Ctrl-w:          Show all devices\n".
                          "Ctrl-o:          Hide devices that are offline now\n".
                          "Ctrl-E:          Hide devices with error responses\n".
                          "Ctrl-y:          Hide selected devices\n".
                          "Ctrl-t:          Hide all except selected devices\n".
                          "Ctrl-z:          Pop shown device stack\n".
                          "Ctrl-L:          Open a list editor window\n".
                          "\n".
                          "Ctrl-g:          Initialise command area for \'get\'\n".
                          "Ctrl-s:          Initialise command area for \'set\'\n".
                          "Return:          Send command\n".
                          "Ctrl-Return:     Send boot command (no response)\n".
                          "Escape:          Clear command area\n".
                          "\n".
                          "Ctrl-R:          Set configuration (filtered) for selected\n".
                          "\n".
                          "Ctrl-n:          Display names for selected\n".
                          "Ctrl-G:          Display groups for selected\n".
                          "\n".
                          "Ctrl-r:          Remote terminals for selected\n".
                          "\n".
                          "Ctrl-p:          E-mail report concerning selected\n".
                          "\n".
                          "Ctrl-u:          Convert unixtimes to localtimes in selected responses\n".
                          "\n".
                          "Ctrl-q:          Open data analysis window for selected responses\n".
                          "Ctrl-Q:          Open response symbol count window for selected responses\n".
                          "Ctrl-X:          Open hexadecimal display for selected responses\n".
                          "\n".
                          "Ctrl-c:          Re-connect to gateways for which status is stale\n".
                          "Ctrl-C:          Re-connect to all connected gateways\n".
                          "Ctrl-h:          Disconnect from gateways with no visible devices\n".
                          "Ctrl-x:          Disconnect from gateways\n".
                          "\n".
                          "Ctrl-,:          Copy names of selected devices to clipboard\n".
                          "Ctrl-.:          Copy responses from selected devices to clipboard\n".
                          "\n".
                          "Down-arrow:      Next item in command history\n".
                          "Up-arrow:        Previous item in command history\n".
                          "\n".
                          "Tab:             Command completion";

my $status_colour_help  = "In order of precedence:\n".
                          "\n".
                          "Grey:    Power to the device was cut\n".
                          "Black:   Device is offline\n".
                          "White:   FGC in boot program\n".
                          "Purple:  FGC is programming\n".
                          "Brown:   FGC is requesting a synchronisation\n".
                          "Red:     Device has a fault\n".
                          "Yellow:  Device has a warning\n".
                          "Green:   Device is okay";

# Begin functions

sub draw()
{
    # Create main window

    $main{window} = new MainWindow;
    $main{window}->optionAdd('*BorderWidth' => 1);
    $main{window}->title("FGCRun+");

    $main{devices}              = [];
    $main{status_list_content}  = [];
    $main{responses}            = [];
    $main{responses_display}    = [];

    $main{history_idx} = 0;

    # Create menubar

    $main{menubar}              = $main{window}->Frame(-relief => 'groove', -borderwidth => 2)->pack(-side => 'top', -fill => 'x');
    $main{menus}->{file}        = $main{menubar}->Menubutton(-text => "File",         -underline => 1, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{connect}     = $main{menubar}->Menubutton(-text => "Connect",      -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{disconnect}  = $main{menubar}->Menubutton(-text => "Disconnect",   -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{gw}          = $main{menubar}->Menubutton(-text => "Gateway",      -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{fgc}         = $main{menubar}->Menubutton(-text => "FGC",          -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{list}        = $main{menubar}->Menubutton(-text => "List",         -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{analysis}    = $main{menubar}->Menubutton(-text => "Analysis",     -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{menus}->{help}        = $main{menubar}->Menubutton(-text => "Help",         -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $main{device_entry}         = $main{menubar}->Entry(-width => FGC_MAX_DEV_LEN, -font => $font, -bg => 'white')->pack(-side => 'right');
    $main{device_entry}->bind('<Key-Return>', \&connect_device);
    $main{device_entry}->bind('<Key-KP_Enter>', \&connect_device);
    $main{menubar}->Label(-font => $font, -text => "Connect devices:")->pack(-side => 'right');

    # Populate file menu

    $main{menus}->{file}->command(-label => "Load device list",         -command => \&load_device_list);
    $main{menus}->{file}->command(-label => "Add device list",          -command => [\&load_device_list, 1]);
    $main{menus}->{file}->command(-label => "Save device list",         -command => \&save_device_list);
    $main{menus}->{file}->separator;
    $main{menus}->{file}->command(-label => "Save response",            -command => \&save_response);
    $main{menus}->{file}->separator;
    $main{menus}->{file}->command(-label => "Quit",                     -command => \&exit);

    # Populate FGC menu

    $main{menus}->{fgc}->command(-label => "Run main program",              -command => \&fgc_run_main);
    $main{menus}->{fgc}->separator;
    $main{menus}->{fgc}->command(-label => "Event log",                     -command => \&get_event_log);
    #$main{menus}->{fgc}->command(-label => "Run log",                       -command => \&get_run_log);
    $main{menus}->{fgc}->separator;
    $main{menus}->{fgc_info}    = $main{menus}->{fgc}->cascade(-label => "Info", -tearoff => 0);
    $main{menus}->{fgc_info}->command(-label => "Names",                    -command => \&display_names, -accelerator => "Ctrl-n");
    $main{menus}->{fgc_info}->command(-label => "Groups",                   -command => \&display_groups, -accelerator => "Ctrl-G");
    $main{menus}->{fgc_info}->command(-label => "Addresses",                -command => \&display_addresses);
    $main{menus}->{fgc_info}->command(-label => "System types",             -command => \&display_system_types);
    $main{menus}->{fgc_config} = $main{menus}->{fgc}->cascade(-label => "Config", -tearoff => 0);
    $main{menus}->{fgc_config}->command(-label => "Get Config",                    -command => [\&get_config, 0]);
    $main{menus}->{fgc_config}->command(-label => "Set Config - Filtered *",         -command => [\&set_config, 0]);
    $main{menus}->{fgc_config}->command(-label => "* List Filtered Config Properties", -command => [\&list_config_properties_to_ignore]);
    $main{menus}->{fgc_config}->separator;
    $main{menus}->{fgc_config}->command(-label => "Get RegFGC3 Params",            -command => [\&get_config, 1]);
    $main{menus}->{fgc_config}->command(-label => "Set RegFGC3 Params",            -command => [\&set_config, 1]);
    $main{menus}->{fgc}->separator;
    #$main{menus}->{fgc_power}   = $main{menus}->{fgc}->cascade(-label => "Power-cycle", -tearoff => 0);
    #$main{menus}->{fgc}->separator;
    #$main{menus}->{fgc}->command(-label => "Log published data",            -command => \&logging_dialogue);
    #$main{menus}->{fgc}->separator;
    #$main{menus}->{fgc}->command(-label => "Logged remote terminal",        -command => [\&rterm, 1]);
    #$main{menus}->{fgc}->separator;
    #$main{menus}->{fgc}->command(-label => "Send file to remote terminal",  -command => [\&rterm, 0, undef, 1]);
    #$main{menus}->{fgc}->separator;
    #$main{menus}->{fgc}->command(-label => "Run FGC3 reception test",       -command => \&fgc3_reception_test);
    #$main{menus}->{fgc}->separator;

    # Populate internal regulation menu

    $main{menus}->{fgc_int_reg} = $main{menus}->{fgc}->command(-label => "Plot Internal Sensitivity Graphs", -command => [\&sensitivity_graph_for_selected_devices]);

    # Populate list menu

    # $main{menus}->{list}->command(-label => "E-mail report concerning selected",    -command => \&report_devices,                                   -accelerator => "Ctrl-p");
    $main{menus}->{list}->command(-label => "Open list editor",                     -command => [\&list_editor, $main{devices}, $main{responses_display}],  -accelerator => "Ctrl-L");
    $main{menus}->{list}->command(-label => "Perform AND of open lists",            -command => \&list_editor_and);
    $main{menus}->{list}->command(-label => "Perform OR of open lists",             -command => \&list_editor_or);
    $main{menus}->{list}->separator;
    $main{menus}->{list}->command(-label => "Show all devices",                     -command => sub { show_all_devices();           }, -accelerator => "Ctrl-w");
    $main{menus}->{list}->command(-label => "Hide devices that are offline now",    -command => sub { hide_offline_devices();       }, -accelerator => "Ctrl-o");
    $main{menus}->{list}->command(-label => "Show only selected devices",           -command => sub { hide_devices_by_selection(0); }, -accelerator => "Ctrl-t");
    $main{menus}->{list}->command(-label => "Hide selected devices",                -command => sub { hide_devices_by_selection(1); }, -accelerator => "Ctrl-y");
    $main{menus}->{list_type} = $main{menus}->{list}->cascade(-label => "Select only type", -tearoff => 0);
    $main{menus}->{list}->separator;
    $main{menus}->{list}->command(-label => "Sort devices by gateway & address",    -command => sub { $device_sort_func = sub { $a->{gateway}->{name} cmp $b->{gateway}->{name} || $a->{channel} <=> $b->{channel} };   populate_device_list(); });
    $main{menus}->{list}->command(-label => "Sort devices by gateway & name",       -command => sub { $device_sort_func = sub { $a->{gateway}->{name} cmp $b->{gateway}->{name} || $a->{name} cmp $b->{name} };         populate_device_list(); });
    $main{menus}->{list}->command(-label => "Sort devices by location & name",      -command => sub { $device_sort_func = sub { $a->{sort_location} cmp $b->{sort_location}     || $a->{name} cmp $b->{name} };         populate_device_list(); });
    $main{menus}->{list}->command(-label => "Sort devices by name",                 -command => sub { $device_sort_func = sub { $a->{name} cmp $b->{name} };                                                            populate_device_list(); });

    # Populate "Select only type" sub-menu

    my %types = map { $_->{type} => 1 } (values(%$devices));
    for my $type_code (sort(keys(%types)))
    {
        my @types = grep { $_->{barcode} =~ /^HC${type_code}_*$/ } values(%fgc_components);

        # Check whether there was more than one type matched

        next if(@types > 1);

        # Add type to menu
        # (Note that the accelerator field is abused to give neat formatting of the type label)

        $main{menus}->{list_type}->command(-label => $type_code, -accelerator => (defined($types[0]) ? $types[0]->{label} : ''), -command => [\&select_only_type, $type_code]);
    }

    # Populate analysis menu

    $main{menus}->{analysis}->command(-label => "Analyse responses",      -command => \&stats,         -accelerator => "Ctrl-q");
    $main{menus}->{analysis}->command(-label => "Response symbol counts", -command => \&symbol_counts, -accelerator => "Ctrl-Q");

    # Populate help menu

    $main{menus}->{help}->command(-label => "Shortcut keys",        -command => sub {
        my $help_window = $main{window}->Toplevel(-title => "Shortcut key help");
        my $scrollbar = $help_window->Scrolled('Pane', -scrollbars => 'osoe', -bg => 'white')->pack(-expand => 1, -fill => 'both');
        my $label = $scrollbar->Label(-font => $font, -justify => 'left', -bg => 'white', -text => $shortcut_help)->pack();
        $scrollbar->configure(-width => $label->reqwidth+20);
        $scrollbar->configure(-height => $label->reqheight+20);
    });
    $main{menus}->{help}->command(-label => "Status colours",        -command => sub {
        my $help_window = $main{window}->Toplevel(-title => "Status colour help");
        my $scrollbar = $help_window->Scrolled('Pane', -scrollbars => 'osoe', -bg => 'white')->pack(-expand => 1, -fill => 'both');
        my $label = $scrollbar->Label(-font => $font, -justify => 'left', -bg => 'white', -text => $status_colour_help)->pack();
        $scrollbar->configure(-width => $label->reqwidth+20);
        $scrollbar->configure(-height => $label->reqheight+20);
    });
    $main{menus}->{help}->command(-label => "Command variables",        -command => sub {
        my $help_window = $main{window}->Toplevel(-title => "Command variable help");
        my $scrollbar = $help_window->Scrolled('Pane', -scrollbars => 'osoe', -bg => 'white')->pack(-expand => 1, -fill => 'both');
        my $label = $scrollbar->Label(-font => $font, -justify => 'left', -bg => 'white', -text => $cmd_variable_help)->pack();
        $scrollbar->configure(-width => $label->reqwidth+20);
        $scrollbar->configure(-height => $label->reqheight+20);
    });

    # read current user option for tooltips. on click toggle value and persist it
    my $USR_OPT_TT = get_user_option_tooltips();
    $main{menus}->{help}->checkbutton(-label => "Enable tooltips", -variable => \$USR_OPT_TT, -onvalue => 'balloon', -offvalue => 'none', -command => sub {
      update_user_option_tooltips($USR_OPT_TT);
      $main{balloon}->configure(-state => $USR_OPT_TT);
    });

    $main{menus}->{help}->separator;
    $main{menus}->{help_howto} = $main{menus}->{help}->cascade(-label => "How to", -tearoff => 0);

    # Populate "How to" sub-menu

    if($^O !~ /win/i) # Platform is not Microsoft Windows
    {
        sub create_how_to_menu_level($$);
        sub create_how_to_menu_level($$)
        {
            my ($menu, $path) = @_;

            for my $filename (<$path/*>)
            {
                (my $name = $filename)  =~ s/.*\/(.*)/$1/;
                $name                   =~ s/.txt//;

                next if($filename =~ /\.xls$/i);
                next if($filename =~ /images$/i);

                if(-d $filename)
                {
                    my $sub_menu = $menu->cascade(-label => "$name", -tearoff => 0);
                    $filename    =~ s/ /\\ /g;
                    create_how_to_menu_level($sub_menu, $filename);
                }
                elsif(-f $filename)
                {
                    $menu->command( -label      => $name,
                                    -command    =>  sub
                                                    {
                                                        system("xterm +sb -T \"$name\" -e \"less \\\"$filename\\\"\" &");
                                                    });
                }
            }
        }

        create_how_to_menu_level($main{menus}->{help_howto}, FGC_HOW_TO_PATH);
    }

    # Populate "Documentation" sub-menu
    $main{menus}->{help}->separator;
    $main{menus}->{help_doc} = $main{menus}->{help}->cascade(-label => "Online Documentation", -tearoff => 0);
    $main{menus}->{help_doc}->command(-label => "FGC Website", -command => [\&FGC::Utils::open_default_browser,"https://www.cern.ch/fgc"]);
    $main{menus}->{help_doc}->command(-label => "PL.OP.VS.PC States", -command => [\&FGC::Utils::open_default_browser,"https://wikis.cern.ch/display/TEEPCCCS/FGC+Real+Time+Display"]);

    # Populate disconnect menu

    $main{menus}->{disconnect}->command(-label          => "All",
                                        -accelerator    => "Ctrl-x",
                                        -command        => sub { disconnect_gateways([values(%gateways_connected)]) },
                                       );
    $main{menus}->{disconnect}->command(-label => "Hidden", -command => \&disconnect_hidden, -accelerator => "Ctrl-h");
    $main{menus}->{disconnect}->separator;
    $main{menus}->{disconnect_gateways} = $main{menus}->{disconnect}->cascade(-label => "Gateways", -tearoff => 0);
    $main{menus}->{disconnect_groups}   = $main{menus}->{disconnect}->cascade(-label => "Groups",   -tearoff => 0);

    # Populate gateway menu;

    $main{menus}->{gw_codes} = $main{menus}->{gw}->cascade(-label => "Codes", -tearoff => 0);

    $main{menus}->{gw_term} = $main{menus}->{gw}->cascade(-label => "Terminal", -tearoff => 0);
    $main{menus}->{gw_term}->command(-label => "Connected", -command => sub { rterm(0, $_->{channels}->[0]) for(values(%gateways_connected)) });
    $main{menus}->{gw_term}->separator;

    $main{menus}->{gw}->separator;

    #$main{menus}->{gw_log}      = $main{menus}->{gw}->cascade(    -label => "Log",  -tearoff => 0);
    $main{menus}->{gw_log}      = $main{menus}->{gw}->cascade(    -label => "Log",  -tearoff => 0, -state => 'disabled');
    $main{menus}->{gw_log_full} = $main{menus}->{gw_log}->cascade(-label => "Full", -tearoff => 0);
    $main{menus}->{gw_log_full}->command(-label => "Connected",                         -command => sub { gw_log($_) for(values(%gateways_connected)) });
    $main{menus}->{gw_log_full}->command(-label => "Selected",                          -command => sub { gw_log_selected() });
    $main{menus}->{gw_log_full}->command(-label => "Selected, filtered for devices",    -command => sub { gw_log_selected(1) });
    $main{menus}->{gw_log_full}->separator;
    $main{menus}->{gw_log_tail} = $main{menus}->{gw_log}->cascade(-label => "Tail", -tearoff => 0);
    $main{menus}->{gw_log_tail}->command(-label => "Connected",                         -command => sub { gw_log_tail($_) for(values(%gateways_connected)) });
    $main{menus}->{gw_log_tail}->command(-label => "Selected",                          -command => sub { gw_log_tail_selected() });
    $main{menus}->{gw_log_tail}->command(-label => "Selected, filtered for devices",    -command => sub { gw_log_tail_selected(1) });
    $main{menus}->{gw_log_tail}->separator;

    $main{menus}->{gw_root} = $main{menus}->{gw}->cascade(-label => "Root shell", -tearoff => 0);
    $main{menus}->{gw_root}->command(-label => "Connected", -command => sub { gw_root($_) for(values(%gateways_connected)) });
    $main{menus}->{gw_root}->separator;

    #$main{menus}->{gw}->separator;

    #$main{menus}->{gw}->command(-label => "FGC_Ether capture", -command => \&fgc_ether_capture);

    # Create gateway group sub-menus

    create_gateway_group_menus($gateway_groups,
                               $main{menus}->{connect},
                               $main{menus}->{disconnect_groups},
                               $main{menus}->{gw_term},
                               $main{menus}->{gw_log_full},
                               $main{menus}->{gw_log_tail},
                               $main{menus}->{gw_root});

    # Populate codes menu

    #$main{menus}->{gw_codes}->command(-label => "Clear", -command => \&clear_codes);
    $main{menus}->{gw_codes}->command(-label => "Read",  -command => [\&gw_set, "GW.READCODES", '']);
    #my $sub_menu_gw_codes_send_code = $main{menus}->{gw_codes}->cascade(-label => "Send", -tearoff => 0);
    #for(1..(FGC_CODE_NUM_SLOTS - 1))
    #{
    #    $sub_menu_gw_codes_send_code->command(-label => "Slot $_", -command => [\&send_code, $_]);
    #}

    # Populate power-cycle menu

    #$main{menus}->{fgc_power}->command(-label   => "All",
    #                                   -command => [\&gw_set, "FIELDBUS.POWER_SIGNAL", FGC_POWER_SIG_ALL,
    #                                                "This will power-cycle all FGCs on the gateways associated to the FGCs selected.  Are you sure?"]);
    #$main{menus}->{fgc_power}->command(-label   => "Boot & crashed",
    #                                   -command => [\&gw_set, "FIELDBUS.POWER_SIGNAL", FGC_POWER_SIG_BOOT_CRASH,
    #                                                "This will power-cycle all boot and crashed FGCs on the gateways associated to the FGCs selected.  Are you sure?"]);

    # Create frame for username and password line

    $main{userpass_frame} = $main{window}->Frame->pack(-fill => 'x');

    $main{state_shorts_frame}   = $main{userpass_frame}->Label(-width => 11, -font => $font.' bold', -text => "PL.OP.VS.PC")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width =>  4, -font => $font.' bold', -text => '')->pack(-side => 'left');
    $main{i_ref_frame}          = $main{userpass_frame}->Label(-width => 5, -font => $font.' bold', -text => "I_REF")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width =>  2, -font => $font.' bold', -text => '')->pack(-side => 'left');
    $main{i_meas_frame}         = $main{userpass_frame}->Label(-width => 6, -font => $font.' bold', -text => "I_MEAS")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width =>  3, -font => $font.' bold', -text => '')->pack(-side => 'left');
    $main{v_ref_frame}          = $main{userpass_frame}->Label(-width => 5, -font => $font.' bold', -text => "V_REF")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width =>  2, -font => $font.' bold', -text => '')->pack(-side => 'left');
    $main{v_meas_frame}         = $main{userpass_frame}->Label(-width => 6, -font => $font.' bold', -text => "V_MEAS")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width =>  3, -font => $font.' bold', -text => '')->pack(-side => 'left');
    $main{selected_devices}     = $main{userpass_frame}->Label(-width => 4, -font => $font.' bold', -text => "0")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width => 1, -font => $font.' bold', -text => "/")->pack(-side => 'left');
    $main{visible_devices}      = $main{userpass_frame}->Label(-width => 4, -font => $font.' bold', -text => "0")->pack(-side => 'left');
    $main{userpass_frame}->Label(-width => 1, -font => $font.' bold', -text => "/")->pack(-side => 'left');
    $main{connected_devices}    = $main{userpass_frame}->Label(-width => 4, -font => $font.' bold', -text => "0")->pack(-side => 'left');

    # Create status bar

    $main{status_bar} = $main{window}->Label(-width => 130, -font => $font.' bold')->pack(-side => 'bottom');

    # Create command area

    $main{command_frame} = $main{window}->Frame(-width => 130)->pack(-side => 'bottom', -fill => 'both');

    $main{send} = $main{command_frame}->Button(
                                                -border     => 2,
                                                -command    => \&process_command,
                                                -relief     => 'groove',
                                                -text       => "Send",
                                                -width      => 4,
                                              )->pack(-side => 'right');

    $main{command_entry} = $main{command_frame}->Entry(
                                                        -bg                 => 'white',
                                                        -font               => $font,
                                                        -highlightthickness => 0,
                                                        -relief             => 'ridge',
                                                        -state              => 'disabled',
                                                        -width              => 120,
                                                      )->pack(-side => 'left', -fill => 'x', -expand => 1);

    $main{command_entry}->bindtags([($main{command_entry}->bindtags)[1, 0, 2, 3]]);
    $main{command_entry}->bind('<Control-Key-a>',           \&select_all_active_devices);
    $main{command_entry}->bind('<Control-Key-A>',           sub { $main{lists}->{devices}->selection('set', 0, 'end'); count_selected_devices(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-b>',           sub { select_all_boot_fgcs(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-c>',           sub { connect_gateways([grep { $_->{status_time_sec} < time - 10 } values(%gateways_connected)], 0); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-C>',           sub { connect_gateways([values(%gateways_connected)], 0); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-d>',           sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "d "); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-e>',           \&select_devices_with_errors);
    $main{command_entry}->bind('<Control-Key-E>',           \&hide_devices_with_errors);
    $main{command_entry}->bind('<Control-Key-f>',           \&select_all_active_fgcs);
    $main{command_entry}->bind('<Control-Key-F>',           \&select_all_fgcs);
    $main{command_entry}->bind('<Control-Key-g>',           sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "g "); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-G>',           sub { display_groups(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-h>',           sub { disconnect_hidden(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-i>',           sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "i "); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-asciitilde>',  \&invert_selection);
    $main{command_entry}->bind('<Control-Key-l>',           \&fgc_acquire);
    $main{command_entry}->bind('<Control-Key-L>',           sub { list_editor($main{devices}, $main{responses_display}) });
    $main{command_entry}->bind('<Control-Key-m>',           \&select_all_gateways);
    $main{command_entry}->bind('<Control-Key-n>',           sub { display_names(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-o>',           sub { hide_offline_devices(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-p>',           \&report_devices);
    $main{command_entry}->bind('<Control-Key-q>',           sub { stats(); $main{command_entry}->break; } );
    $main{command_entry}->bind('<Control-Key-Q>',           sub { symbol_counts() } );
    $main{command_entry}->bind('<Control-Key-r>',           sub { rterm() });
    $main{command_entry}->bind('<Control-Key-R>',           sub { set_config(0); });
    $main{command_entry}->bind('<Control-Key-s>',           sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "s "); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-t>',           sub { hide_devices_by_selection(0); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-u>',           sub { unixtime_to_localtime(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-x>',           sub { disconnect_gateways([values(%gateways_connected)]); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-X>',           \&display_response_hex);
    $main{command_entry}->bind('<Control-Key-w>',           sub { show_all_devices(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-y>',           sub { hide_devices_by_selection(1); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-z>',           sub { pop_shown_devices(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-bar>',         sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "|"); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-comma>',       \&device_names_to_clipboard);
    $main{command_entry}->bind('<Control-Key-equal>',       sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "="); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-exclam>',      sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "!"); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-less>',        sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, "<"); $main{command_entry}->break; });
    $main{command_entry}->bind('<Control-Key-period>',      \&responses_to_clipboard);
    $main{command_entry}->bind('<Control-Key-greater>',     sub { $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, ">"); $main{command_entry}->break; });
    $main{command_entry}->bind('<Down>',                    sub { $main{command_entry}->delete(0, 'end'); if($main{history_idx} < -1 && defined($history[$main{history_idx} + 1])){ $main{command_entry}->insert(0, $history[++$main{history_idx}]); } else { $main{history_idx} = 0 } $main{command_entry}->break; });
    $main{command_entry}->bind('<Up>',                      sub { if(defined($history[$main{history_idx} - 1])){ $main{command_entry}->delete(0, 'end'); $main{command_entry}->insert(0, $history[--$main{history_idx}]); } $main{command_entry}->break; });
    $main{command_entry}->bind('<Key-Escape>',              sub { $main{command_entry}->delete(0, 'end'); $main{status_bar}->configure(-text => ''); $main{command_entry}->break; });
    $main{command_entry}->bind('<Shift-Key-Escape>',        sub { $main{lists}->{devices}->selection('clear', 0, 'end'); count_selected_devices(); $main{command_entry}->break; });
    $main{command_entry}->bind('<Key-Return>',              \&process_command);
    $main{command_entry}->bind('<Control-Key-Return>',      \&send_boot_command);
    $main{command_entry}->bind('<Key-KP_Enter>',            \&process_command);
    $main{command_entry}->bind('<Control-KP_Enter>',        \&send_boot_command);
    $main{command_entry}->bind('<Tab>',                     sub { command_completion(); $main{command_entry}->break; });

    # Create lists

    $main{scrollbar} = $main{window}->Scrollbar(-orient => "vertical", -elementborderwidth => 1)->pack(-side => 'right', -fill => 'y');

    $main{lists}->{status} = $main{window}->Listbox(
                                                        -bg                 => 'white',
                                                        -borderwidth        => 1,
                                                        -exportselection    => 0,
                                                        -font               => $font,
                                                        -height             => FGC_MAX_DEVS_PER_GW,
                                                        -listvariable       => $main{status_list_content},
                                                        -selectmode         => 'single',
                                                        -takefocus          => 0,
                                                        -width              => 50,
                                                    )->pack(-side => 'left', -fill => 'both');
    $main{lists}->{status}->bind('<Button-1>', \&display_status);

    $main{lists}->{devices} = $main{window}->Listbox(
                                                        -bg                 => 'white',
                                                        -borderwidth        => 1,
                                                        -exportselection    => 0,
                                                        -font               => $font,
                                                        -height             => FGC_MAX_DEVS_PER_GW,
                                                        -listvariable       => $main{devices},
                                                        -selectmode         => 'extended',
                                                        -takefocus          => 0,
                                                        -width              => FGC_MAX_DEV_LEN + 1,
                                                    )->pack(-side => 'left', -fill => 'both');
    $main{lists}->{devices}->bind('<Double-Button-1>', sub { rterm() });
    $main{lists}->{devices}->bind('<<ListboxSelect>>', \&count_selected_devices);

    $main{lists}->{responses} = $main{window}->Listbox(
                                                        -bg             => 'white',
                                                        -borderwidth    => 1,
                                                        -height         => FGC_MAX_DEVS_PER_GW,
                                                        -font           => $font,
                                                        -listvariable   => $main{responses_display},
                                                        -selectmode     => 'single',
                                                        -takefocus      => 0,
                                                        -width          => 80 - (FGC_MAX_DEV_LEN + 1),
                                                      )->pack(-side => 'left', -fill => 'both', -expand => 1);
    $main{lists}->{responses}->bind('<Button-1>', \&display_response);

    $main{lists}->{devices}->configure(     -yscrollcommand => sub { $main{scrollbar}->set(@_); for my $list (values(%{$main{lists}})) { $list->yviewMoveto($_[0]) } });
    $main{lists}->{status}->configure(      -yscrollcommand => sub { $main{lists}->{status}->yviewMoveto(($main{lists}->{devices}->yview)[0]) });
    $main{lists}->{responses}->configure(   -yscrollcommand => sub { $main{lists}->{responses}->yviewMoveto(($main{lists}->{devices}->yview)[0]) });
    $main{lists}->{responses}->configure(   -xscrollcommand => sub { $main{lists}->{responses}->xviewMoveto(0) });
    $main{scrollbar}->configure(-command => sub { for my $list (values(%{$main{lists}})) { $list->yview(@_) } });

    # Item styles for use in log dialogs

    $main{window}->ItemStyle('text', -stylename => 'heading',     -font => $font.' bold',   -bg => 'white');
    $main{window}->ItemStyle('text', -stylename => 'grey',        -font => $font,           -bg => 'grey92');
    $main{window}->ItemStyle('text', -stylename => 'white',       -font => $font,           -bg => 'white');
    $main{window}->ItemStyle('text', -stylename => 'set_grey',    -font => $font,           -bg => 'grey92', -fg => 'forest green', -selectforeground => 'forest green');
    $main{window}->ItemStyle('text', -stylename => 'set_white',   -font => $font,           -bg => 'white',  -fg => 'forest green', -selectforeground => 'forest green');
    $main{window}->ItemStyle('text', -stylename => 'clear_grey',  -font => $font,           -bg => 'grey92', -fg => 'firebrick',    -selectforeground => 'firebrick');
    $main{window}->ItemStyle('text', -stylename => 'clear_white', -font => $font,           -bg => 'white',  -fg => 'firebrick',    -selectforeground => 'firebrick');

    # Balloon help

    $main{balloon} = $main{window}->Balloon(-initwait => 1000, -font => $font, -bg => 'light yellow', -state => $USR_OPT_TT);
    $main{balloon}->attach($main{command_entry},        -msg => "Command field\n\nEnter a command or expression to match here then press Enter or click the Send button.");
    $main{balloon}->attach($main{connected_devices},    -msg => "Number of connected devices.");
    $main{balloon}->attach($main{device_entry},         -msg => "Connect device field\n\nEnter a device name here then press Enter to connect to the appropriate gateway and select the device.");
    $main{balloon}->attach($main{lists}->{devices},     -msg => "Device list\n\nAvailable devices are displayed here.\nCommands will be sent to selected devices.\nDouble-click an entry to open a remote terminal.");
    $main{balloon}->attach($main{lists}->{responses},   -msg => "Response list\n\nResponses to commands appear here.\nClick an entry to display the full contents of a long response.");
    $main{balloon}->attach($main{lists}->{status},      -msg => "Status list\n\nA summary of the current status of each device refreshed at 1Hz is displayed here.\nClick an entry to display the full status.");
    $main{balloon}->attach($main{selected_devices},     -msg => "Number of selected devices.");
    $main{balloon}->attach($main{send},                 -msg => "Send button\n\nClick to send the command or expression entered in the Command field.");
    $main{balloon}->attach($main{status_bar},           -msg => "Status bar\n\nImportant messages are displayed here.");
    $main{balloon}->attach($main{visible_devices},      -msg => "Number of visible devices.");
    $main{balloon}->attach($main{state_shorts_frame},   -msg => "PL: The Phase-Locked Loop state\t\t(FGC.PLL.STATE)\n".
                                                                "OP: The operational state of the FGC\t(STATE.OP)\n".
                                                                "VS: The state of the Voltage Source\t(VS.STATE)\n".
                                                                "PC: The state of the Power Converter\t(STATE.PC)\n\n".
                                                                "See 'FGC Real Time Display' in CCS Wiki for more information");

    $main{balloon}->attach($main{i_ref_frame},          -msg => "REF.I: The current reference in amps");
    $main{balloon}->attach($main{i_meas_frame},         -msg => "MEAS.I: The validated current based on the FGCs measurements of channel AB, A or B");
    $main{balloon}->attach($main{v_ref_frame},          -msg => "REF.V: The voltage reference in volts");
    $main{balloon}->attach($main{v_meas_frame},         -msg => "MEAS.V: The output voltage measured by the FGC");

    # Set the minimum window size

    $main{window}->update;
    $main{window}->minsize($main{window}->winfo("width"),
                           $main{window}->winfo("height"));
}

# Return an array of all gateways in a group

sub group_gateways($);
sub group_gateways($)
{
    my ($group) = @_;

    my @gateways = values(%{$group->{gateways}});

    for my $child_group (values(%{$group->{groups}}))
    {
        @gateways = (@gateways, group_gateways($child_group));
    }
    return(@gateways);
}

# Create gateway group sub-menus

sub create_gateway_group_menus($$$$$$$);
sub create_gateway_group_menus($$$$$$$)
{
    my (
        $group,
        $connect_menu,
        $disconnect_menu,
        $term_menu,
        $log_full_menu,
        $log_tail_menu,
        $shell_menu,
       ) = @_;

    # Create "All" menu entries

    $connect_menu->command(     -label => " All", -command => sub { connect_gateways([group_gateways($group)]) });
    $disconnect_menu->command(  -label => " All", -command => sub { disconnect_gateways([group_gateways($group)]) });

    # Create separators

    $connect_menu->separator;

    if(%{$group->{groups}})
    {
        $disconnect_menu->separator;
    }

    # Create sub-menus for child groups

    for my $child_group (sort { $a->{name} cmp $b->{name} } values(%{$group->{groups}}))
    {
        # Note that there is a space prefix for each group menu label as there is a bug in Tk menus that causes
        # commands to be attached to the wrong menu when labels begin with a number

        create_gateway_group_menus($child_group,
                                   $connect_menu->cascade(      -label => " $child_group->{name}", -tearoff => 0),
                                   $disconnect_menu->cascade(   -label => " $child_group->{name}", -tearoff => 0),
                                   $term_menu->cascade(         -label => " $child_group->{name}", -tearoff => 0),
                                   $log_full_menu->cascade(     -label => " $child_group->{name}", -tearoff => 0),
                                   $log_tail_menu->cascade(     -label => " $child_group->{name}", -tearoff => 0),
                                   $shell_menu->cascade(        -label => " $child_group->{name}", -tearoff => 0));
    }

    # Create menu entries for gateways in this group

    for my $gateway (sort { $a->{name} cmp $b->{name} } values(%{$group->{gateways}}))
    {
        $connect_menu->command( -label => " $gateway->{name}", -command => [\&connect_gateway, $gateway]);
        $term_menu->command(    -label => " $gateway->{name}", -command => [\&rterm, 0, $gateway->{channels}->[0]]);
        $log_full_menu->command(-label => " $gateway->{name}", -command => [\&gw_log,          $gateway]);
        $log_tail_menu->command(-label => " $gateway->{name}", -command => [\&gw_log_tail,     $gateway]);
        $shell_menu->command(   -label => " $gateway->{name}", -command => [\&gw_root,         $gateway]);
    }
}

# Get a list of references to selected devices

sub selected_devices()
{
    my @dev_list_indices    = ref($main{lists}->{devices}->curselection) ?
                                @{$main{lists}->{devices}->curselection} :
                                 ($main{lists}->{devices}->curselection);
    my @selected_devices    = map { $devices->{$main{devices}->[$_]} } (@dev_list_indices);

    return(\@selected_devices);
}

# Set status message

sub set_status_message($$)
{
    my ($device, $message) = @_;

    my $status_width = $main{lists}->{status}->cget(-width);

    # Set status list element to centred message

    $main{status_list_content}->[$device->{list_index}] = (' ' x (($status_width / 2) - (length($message) / 2))).$message;
}

# Set response for a device

sub set_response($$;$)
{
    my ($device, $response, $command) = @_;

    return if(!defined($device->{list_index}));

    $main{responses}->[$device->{list_index}] = $response;

    # Replace all newlines with spaces, because Tk Listbox doesn't display newlines correctly

    $response =~ tr{\n}{ };

    $main{responses_display}->[$device->{list_index}] = $response;

    if(defined($command))
    {
        # Check whether command was a set or a get

        if(defined($command->{values})) # Set command
        {
            $device->{previous_command} = "s $command->{property} ";

            # Check whether values were passed as an array or string

            if(ref($command->{values}))
            {
                if(ref($command->{values}) eq 'ARRAY')
                {
                    $device->{previous_command} .= join(',', @{$command->{values}});
                }
            }
            else
            {
                $device->{previous_command} .= $command->{values};
            }
        }
        else # Get command
        {
            $device->{previous_command} = "g $command->{property}";
        }
    }
    else
    {
        delete($device->{previous_command});
    }
}

sub push_response($)
{
    my ($response) = @_;

    push(@{$main{responses}}, $response);

    # Replace all newlines with spaces, because Tk Listbox doesn't display newlines correctly

    $response =~ tr{\n}{ };

    push(@{$main{responses_display}}, $response);
}

sub get_response($)
{
    my ($index) = @_;

    return $main{responses}->[$index];
}

sub get_responses_array()
{
    return @{$main{responses}};
}

sub clear_responses_array()
{
    @{$main{responses}} = ("");
    delete($main{responses}->[0]);

    @{$main{responses_display}} = ("");
    delete($main{responses_display}->[0]);
}

sub clear_response($)
{
    my ($index) = @_;

    $main{responses}->[$index]         = '';
    $main{responses_display}->[$index] = '';
}

sub redraw_responses()
{
    @{$main{responses_display}} = @{$main{responses_display}};
}

# Command completion

sub command_completion()
{
    my $command = $main{command_entry}->get;

    # Do nothing if cursor is not at end of command

    return if($main{command_entry}->index('insert') != length($command));

    # Check type of completion required

    if($command =~ /^s\s+([a-z0-9-_.]+)([\[\(].*[\)\]])?\s+([a-z0-9-_]*[,\s])*([a-z0-9.-_]*)$/i) # Constant completion
    {
        my $property    = $fgc_properties{"\U$1"};
        my $symbol      = $4;

        const_completion($command, $property, $symbol);
    }
    elsif($command =~ /^[sgd]\s+([a-z0-9-_.]+)$/i) # Property completion
    {
        my $symbol = $1;

        property_completion($command, $symbol);
    }
}

# Property completion for commands

sub property_completion($$)
{
    my ($command, $symbol) = @_;

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    my %classes = map { $_->{class} => 1 } @$selected_devices;

    # Get property names for selected classes

    my $cmd_type = substr($command, 0, 1) =~ /s/i ? 'set' : 'get';
    my @property_names;

    for my $property (sort { $a->{name} cmp $b->{name}  } values(%fgc_properties))
    {
        # Check whether property exists for all selected classes

        my $class_count = 0;

        for my $class (keys(%classes))
        {
            last if(!defined($property->{classes}->{$class}));
            last if(!$property->{classes}->{$class}->{$cmd_type});
            $class_count++;
        }

        if($class_count == scalar(keys(%classes)))
        {
            push(@property_names, $property->{name});
        }
    }

    # Get a list of properties starting with symbol

    my @matches = sort(grep(/^\Q$symbol/i, @property_names));
    if(substr($symbol, 0, 1) =~ /^[a-z]/)
    {
        $_ = "\L$_" for @matches;
    }
    $main{status_bar}->configure(-text => "No common property suggestions found for '".$command."'"), return if(!@matches);

    complete_command($command, $symbol, \@matches);
}

# Const completion for commands

sub const_completion($$$)
{
    my ($command, $property, $symbol) = @_;

    return if(!defined($property));

    # Get a list of constants starting with symbol

    my @matches;

    my @classes_with_symbols = keys(%{$property->{class_symbols}});
    if(@classes_with_symbols) # Class-specific symlist
    {
        my $selected_devices = selected_devices();
        return if(!@$selected_devices);

        # Get symbols common to all selected device classes

        my %classes = map { $_->{class} => 1 } @$selected_devices;

        my %common_syms;
        for my $class (keys(%classes))
        {
            my $class_symbols = $property->{class_symbols}->{$class};

            for my $symbol (grep($class_symbols->{$_}->{settable}, keys(%$class_symbols)))
            {
                next if(defined($common_syms{$symbol}));

                my $common = 1;
                for my $other_class (keys(%classes))
                {
                    my $other_class_symbol = $property->{class_symbols}->{$other_class}->{$symbol};

                    $common = 0, last if(!defined($other_class_symbol) ||
                                         !$other_class_symbol->{settable});
                }
                $common_syms{$symbol} = 1 if($common);
            }
        }
        @matches = sort(grep(/^\Q$symbol/i, keys(%common_syms)));
    }
    else # Property has no symbols
    {
        @matches = ();
    }

    if(substr($symbol, 0, 1) =~ /^[a-z]/)
    {
        $_ = "\L$_" for @matches;
    }
    $main{status_bar}->configure(-text => "No common symbol suggestions found for '".$command."'"), return if(!@matches);

    complete_command($command, $symbol, \@matches);
}

# Complete command with matches

sub complete_command($$$)
{
    my ($command, $symbol, $matches) = @_;

    # Complete common start of matches

    my $common = $symbol;
    for(my $i = length($symbol) ; ; $i++)
    {
        last if(length($matches->[0]) < $i);
        my $a = substr($matches->[0], $i, 1);

        my $mismatch = 0;
        for my $match (@$matches)
        {
            $mismatch = 1, last if(length($match) < $i);
            my $b = substr($match, $i, 1);
            $mismatch = 1, last if($a ne $b);
        }
        last if($mismatch);
        $common .= $a;
    }

    # Replace symbol with common start of matches in command

    $command =~ s/$symbol$/$common/i;
    $main{command_entry}->delete(0, 'end');
    $main{command_entry}->insert(0, $command);

    # Display a menu if there are multiple matches

    if($common eq $symbol && @$matches > 1)
    {
        my $menu    = $main{command_entry}->Menu(-tearoff => 0);
        my $cleanup = sub { $main{command_entry}->focusForce; $menu->destroy; };
        $menu->bind('<FocusOut>',       $cleanup );
        $menu->bind('<Key-BackSpace>',  $cleanup );
        $menu->bind('<Key-Escape>',     $cleanup );

        my $i = 0;
        for my $match (@$matches)
        {
            $menu->command( -label          => $match,
                            -columnbreak    => $i++ % 30 ? 0 : 1,
                            -command        => sub
                                               {
                                                    $command =~ s/$common$/$match/i;
                                                    $main{command_entry}->delete(0, 'end');
                                                    $main{command_entry}->insert(0, $command);
                                                    $main{command_entry}->focusForce;
                                                    $menu->destroy;
                                               },
                          );
        }

        # Get coordinates of entry

        my $x = $main{command_entry}->winfo("rootx");
        my $y = $main{command_entry}->winfo("rooty");

        # Get offset of cursor

        my ($x_offset, $y_offset, $width) = $main{command_entry}->bbox("insert");
        $x += $x_offset + $width;
        $y += $y_offset;

        $menu->post($x, $y);
        $menu->activate(0);
        $menu->focusForce;
    }
}

# Populate disconnect gateways menu

sub populate_disconnect_gateways()
{
    # Populate disconnect gateways menu

    $main{menus}->{disconnect_gateways}->menu->delete(0, 'end');
    my $i = 0;
    for(sort { $a->{name} cmp $b->{name} } (values(%gateways_connected)))
    {
        $main{menus}->{disconnect_gateways}->command(   -label          => $_->{name},
                                                        -columnbreak    => $i++ % 30 ? 0 : 1, -command => [\&disconnect_gateway, $_]);
    }
}

# Populate device list

sub populate_device_list(;$)
{
    my ($populate_disconnect_gateways)  = @_;
    $populate_disconnect_gateways       = 0 if(!defined($populate_disconnect_gateways));

    populate_disconnect_gateways() if($populate_disconnect_gateways);

    # Get current selection

    my $selected_devices = selected_devices();

    # Clear current selection

    $main{lists}->{devices}->selection('clear', 0, 'end');

    # Get a list of available devices

    my @available_devices;
    my $connected_device_count = 0;
    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{name}))
            {
                $connected_device_count++;
                push(@available_devices, $device) if(!defined($device->{hidden}));
            }
        }
    }

    # Clear lists and store previous responses

    @{$main{devices}}       = ("");
    delete($main{devices}->[0]);

    my @previous_responses  = get_responses_array();
    clear_responses_array();

    # Clear FGC status list

    @{$main{status_list_content}} = ("");
    delete($main{status_list_content}->[0]);

    # Sort devices and populate lists

    my $list_index = 0;
    for my $device (sort $device_sort_func (@available_devices))
    {
        push_response(defined($device->{list_index}) ? $previous_responses[$device->{list_index}] : '');

        $main{status_list_content}->[$list_index]   = '';
        $device->{list_index}                       = $list_index++;
        push(@{$main{devices}}, $device->{name});
    }

    # Restore selection

    for my $device (@$selected_devices)
    {
        $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index}) if(defined($device->{list_index}));
    }

    count_selected_devices();
    $main{visible_devices}->configure(  -text => scalar(@{$main{devices}}));
    $main{connected_devices}->configure(-text => $connected_device_count);
}

# Count selected devices

sub count_selected_devices()
{
    my $num_selected = scalar(ref($main{lists}->{devices}->curselection) ?
                                @{$main{lists}->{devices}->curselection} :
                                 ($main{lists}->{devices}->curselection)) || 0;

    $main{selected_devices}->configure(-text => $num_selected);
}

# Pop shown device stack

sub pop_shown_devices()
{
    return if(!defined($main{shown_device_stack}) || !@{$main{shown_device_stack}});
    $main{shown_devices} = pop(@{$main{shown_device_stack}}) or return;

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{name}) && defined($main{shown_devices}->{$device->{name}}))
            {
                delete($device->{hidden});
            }
        }
    }
    populate_device_list();
}

# Connect to gateways for devices with names matching expression and select them

sub connect_device()
{
    my $name = $main{device_entry}->get;

    # Do nothing if name is an empty string

    return if($name eq '');

    # Get a list of matching devices

    my %matched_devices;
    eval
    {
        for my $device (values(%$devices))
        {
            # check device name

            if($device->{name} =~ /$name/i)
            {
                $matched_devices{$device->{name}} = $device;
            }

            # Check subdevice names

            for my $sub_device (@{$device->{sub_devices}})
            {
                if(defined($sub_device->{name}) && $sub_device->{name} =~ /$name/i)
                {
                    $matched_devices{$device->{name}} = $device;
                    last;
                }
            }
        }
    };

    $main{status_bar}->configure(-text => '');

    # Check whether no devices where found

    if(!scalar(keys(%matched_devices)))
    {
        $main{status_bar}->configure(-text => "ERROR: No matching devices found");
        $main{lists}->{devices}->selection('clear', 0, 'end');
        return;
    }

    # Construct list of gateways to connect

    my %gateways_to_connect;

    for my $device (values(%matched_devices))
    {
        $gateways_to_connect{$device->{gateway}->{name}} = $device->{gateway};
        delete($device->{hidden});
    }

    # Connect to gateways

    for my $gateway (values(%gateways_to_connect))
    {
        next if(defined($gateways_connected{$gateway->{name}}));

        # Connect to gateway

        connect_gateway($gateway, 0);

        # Hide non-matching devices

        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{name}));

            if(!defined($matched_devices{$device->{name}}))
            {
                $device->{hidden} = 1;
                delete($device->{list_index});
                delete($main{shown_devices}->{$device->{name}});
            }
        }
    }
    populate_device_list(1);

    # Select devices

    $main{lists}->{devices}->selection('clear', 0, 'end');
    for my $device (values(%matched_devices))
    {
        $main{lists}->{devices}->selection('set', $device->{list_index}) if(defined($device->{list_index}));
    }
    count_selected_devices();

    # Show first matched device

    my @matched = values(%matched_devices);
    $main{lists}->{devices}->see($matched[0]->{list_index});

    # Set focus to command entry

    $main{command_entry}->focusForce;
}

# Hide offline devices

sub hide_offline_devices()
{
    # Push previous list of shown devices onto shown device stack

    push(@{$main{shown_device_stack}}, $main{shown_devices}) if(defined($main{shown_devices}));
    $main{shown_devices} = {};

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{name}));

            if(!defined($device->{online}) || !$device->{online})
            {
                $device->{hidden} = 1;
                delete($device->{list_index});
            }
            elsif(!$device->{hidden})
            {
                $main{shown_devices}->{$device->{name}} = $device;
            }
        }
    }
    populate_device_list();
}

# Hide devices with error responses

sub hide_devices_with_errors()
{
    # Push previous list of shown devices onto shown device stack

    push(@{$main{shown_device_stack}}, $main{shown_devices}) if(defined($main{shown_devices}));
    $main{shown_devices} = {};

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{name}));

            if(defined($device->{list_index}) && get_response($device->{list_index}) =~ /^ERROR: /)
            {
                $device->{hidden} = 1;
                delete($device->{list_index});
            }
            elsif(!$device->{hidden})
            {
                $main{shown_devices}->{$device->{name}} = $device;
            }
        }
    }
    populate_device_list();
}

# Hide devices by selection

sub hide_devices_by_selection($)
{
    my ($hide_selected) = @_;

    my $selected_devices = selected_devices();
    my %selected_devices = map { $_->{name} => $_ } @$selected_devices;

    # Push previous list of shown devices onto shown device stack

    push(@{$main{shown_device_stack}}, $main{shown_devices}) if(defined($main{shown_devices}));
    $main{shown_devices} = {};

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{name}));

            if(defined($selected_devices{$device->{name}})) # Device is selected
            {
                if($hide_selected) # Hide selected devices
                {
                    $device->{hidden} = 1;
                    delete($device->{list_index});
                }
                elsif(!$device->{hidden})
                {
                    $main{shown_devices}->{$device->{name}} = $device;
                }
            }
            else # Device is not selected
            {
                if(!$hide_selected) # Hide non-selected devices
                {
                    $device->{hidden} = 1;
                    delete($device->{list_index});
                }
                if(!$device->{hidden})
                {
                    $main{shown_devices}->{$device->{name}} = $device;
                }
            }
        }
    }
    populate_device_list();
}

# Show all devices

sub show_all_devices()
{
    # Empty shown device stack

    $main{shown_device_stack}   = [];
    $main{shown_devices}        = {};

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{name}))
            {
                delete($device->{hidden});
                $main{shown_devices}->{$device->{name}} = $device;
            }
        }
    }
    populate_device_list();
}

# Disconnect from gateways for hidden devices

sub disconnect_hidden()
{
    $main{status_bar}->configure(-text => '');

    for my $gateway (values(%gateways_connected))
    {
        my $device_visible = 0;

        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{list_index}))
            {
                $device_visible = 1;
                last;
            }
        }
        disconnect_gateway($gateway, 0) if(!$device_visible);
    }
    populate_device_list(1);
}

# Invert selection

sub invert_selection()
{
    my $selected_devices = selected_devices();

    # Convert selected devices to a hash

    my %selected_devices = map { $_->{name} => $_ } @$selected_devices;

    # Select only previously deselected devices

    $main{lists}->{devices}->selection('clear', 0, 'end');
    for my $device (values(%$devices))
    {
        if(defined($device->{list_index}) && !defined($selected_devices{$device->{name}}))
        {
            $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
        }
    }
    count_selected_devices();
}

# Select all active devices in device list

sub select_all_active_devices()
{
    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for(my $i = 0 ; $i < @{$gateway->{channels}} ; $i++)
        {
            my $device = $gateway->{channels}->[$i];
            if(defined($device->{list_index}) &&
               (!defined($device->{online}) || ($device->{online}) && $device->{status}->{DATA_STATUS}->{CLASS_VALID}))
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();

    $main{command_entry}->break;
}

# Select all devices with errors

sub select_devices_with_errors()
{
    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for(my $i = 0 ; $i < @{$gateway->{channels}} ; $i++)
        {
            my $device = $gateway->{channels}->[$i];
            if(defined($device->{list_index}) && get_response($device->{list_index}) =~ /^ERROR: /)
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();

    $main{command_entry}->break;
}

# Select all FGCs in device list

sub select_all_fgcs()
{
    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for(my $i = 1 ; $i < @{$gateway->{channels}} ; $i++)
        {
            my $device = $gateway->{channels}->[$i];
            if(defined($device->{list_index}))
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();

    $main{command_entry}->break;
}

# Select all active FGCs in device list

sub select_all_active_fgcs()
{
    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for(my $i = 1 ; $i < @{$gateway->{channels}} ; $i++)
        {
            my $device = $gateway->{channels}->[$i];

            if(defined($device->{list_index}) &&
               (!defined($device->{online}) || ($device->{online}) && $device->{status}->{DATA_STATUS}->{CLASS_VALID}))
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();

    $main{command_entry}->break;
}

# Select all boot FGCs in device list

sub select_all_boot_fgcs()
{
    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for(my $i = 1 ; $i < @{$gateway->{channels}} ; $i++)
        {
            my $device = $gateway->{channels}->[$i];

            if
            (
               defined($device->{list_index})
               &&
               (
                    !defined($device->{online})
                    ||
                    (
                        $device->{online} &&
                        !$device->{status}->{DATA_STATUS}->{CLASS_VALID} &&
                        (
                            (defined($device->{status}->{STATE_OP}) &&
                            $device->{status}->{STATE_OP} eq 'BOOT')
                            ||
                            # recv_sub_data only decodes full status for devices visible in the viewport
                            # only full status contains STATE_OP. Hence if STATE_OP is not available
                            # we check if the class_id is one of the platform numbers 50, 60, ..
                            # because CLASS_ID == platform number while in boot
                            (defined($device->{status}->{CLASS_ID}) &&
                            $device->{status}->{CLASS_ID} % 10 == 0 &&
                            $device->{status}->{CLASS_ID} != 0)
                        )
                    )
               )
            )
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();

    $main{command_entry}->break;
}

# Select all gateways in device list

sub select_all_gateways()
{
    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        my $gateway_dev = $gateway->{channels}->[0];

        if(defined($gateway_dev->{list_index}))
        {
            $main{lists}->{devices}->selection('set', $gateway_dev->{list_index}, $gateway_dev->{list_index});
        }
    }
    count_selected_devices();

    $main{command_entry}->break;
}

# Select only devices of supplied type

sub select_only_type($)
{
    my ($type) = @_;

    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{list_index}) && $device->{type} eq $type)
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();
}

# Copy selected device names to clipboard

sub device_names_to_clipboard()
{
    $main{window}->clipboardClear();
    $main{window}->clipboardAppend('--', join("\n", map { $_->{name} } @{selected_devices()}));
}

# Copy responses for selected devices to clipboard

sub responses_to_clipboard()
{
    $main{window}->clipboardClear();
    $main{window}->clipboardAppend('--', join("\n", map { get_response($_->{list_index}) } @{selected_devices()}));
}

# Save a device's response

sub save_response()
{
    $main{status_bar}->configure(-text => '');

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    # Check that only one device is selected

    if(@$selected_devices > 1)
    {
        $main{status_bar}->configure(-text => "ERROR: Only one device may be selected to save response");
        return;
    }

    my $device = $selected_devices->[0];

    return if(!defined((my $filename = $main{window}->getSaveFile(
                                                                    -initialfile    => "response_$device->{name}",
                                                                    -initialdir     => '.',
                                                                    -filetypes      => [["All files", "*"]],
                                                                 ))));
    chdir(dirname($filename));

    # Allow file to be modified by anyone if it is in the dropbox

    my $umask = umask();
    umask(0000) if($filename =~ /dropbox/);

    # Open file

    if(!open(FILE, ">", $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file $!");
        return;
    }
    umask($umask);

    # Write response to file

    print FILE get_response($device->{list_index});

    close(FILE);
}

# Save a list of devices

sub save_device_list()
{
    $main{status_bar}->configure(-text => '');

    return if(!defined((my $filename = $main{window}->getSaveFile(
                                                                    -defaultextension   => ".txt",
                                                                    -filetypes          => [["Text files", ".txt"], ["All files", "*"]],
                                                                    -initialdir         => '.',
                                                                    -initialfile        => "device_list.txt",
                                                                 ))));
    chdir(dirname($filename));

    # Populate array of devices to include in list

    my @list_devices;

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{list_index}));

            $device->{response} =  get_response($device->{list_index});
            $device->{response} =~ s/[\012\015]+/\$/g;

            push(@list_devices, $device);
        }
    }

    # Allow file to be modified by anyone if it is in the dropbox

    my $umask = umask();
    umask(0000) if($filename =~ /dropbox/);

    # Write file

    if(!FGC::DeviceList::write(\@list_devices, $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }
    umask($umask);

    # Remove responses from device entries

    delete($_->{response}) for(@list_devices);
}

# Load a list of devices

sub load_device_list(;$$)
{
    my ($keep_connections, $directory) = @_;
    $keep_connections   = 0     if(!defined($keep_connections));
    $directory          = "."   if(!defined($directory));

    $main{status_bar}->configure(-text => '');

    return if(!defined((my $filename = $main{window}->getOpenFile(-title => "Select device list file", -initialdir => $directory))));
    chdir(dirname($filename)) if($directory eq ".");

    # Read devices from file

    $main{shown_devices} = {};
    my %gateways_to_connect;

    # Disconnect existing connections

    if($keep_connections)
    {
        for my $gateway (values(%gateways_connected))
        {
            for my $device (@{$gateway->{channels}})
            {
                next if(!defined($device->{name}));
                $main{shown_devices}->{$device->{name}} = $device if(!defined($device->{hidden}));
            }
        }
    }
    else
    {
        disconnect_gateways([values(%gateways_connected)]);
    }

    # Read device list

    my $list_devices = FGC::DeviceList::read($devices, $filename);

    if(!defined($list_devices))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }

    # Add devices from list to shown devices

    for my $device (@$list_devices)
    {
        delete($device->{hidden});
        $main{shown_devices}->{$device->{name}}             = $device;
        $gateways_to_connect{$device->{gateway}->{name}}    = $device->{gateway};
    }

    # Hide devices that were not in file

    for my $gateway (values(%gateways_to_connect))
    {
        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{name}));
            $device->{hidden} = 1 if(!defined($main{shown_devices}->{$device->{name}}));
        }
    }

    # Connect to gateways

    connect_gateways([grep { !defined($gateways_connected{$_->{name}}) } (values(%gateways_to_connect))]);

    # Restore device responses

    for my $gateway (values(%gateways_to_connect))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{list_index}) && defined($device->{response}))
            {
                set_response($device, $device->{response});
            }
            delete($device->{response});
        }
    }
}

# Initialise statistical data

sub init_stats($)
{
    my ($stats) = @_;

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    $main{status_bar}->configure(   -text => '');
    $stats->{status_bar}->configure(-text => '') if(defined($stats->{status_bar}));

    # Check whether a single device or multiple devices are selected

    if(!defined($stats->{data}) && @$selected_devices == 1) # A single device is selected
    {
        my @data = split(",", get_response($selected_devices->[0]->{list_index}));

        # Check that the response contained at least two samples

        if(@data < 2)
        {
            $main{status_bar}->configure( -text => "ERROR: Data must include at least 2 samples");
            return;
        }

        # Check that data is numeric

        for my $sample (@data)
        {
            if(!is_numeric($sample))
            {
                $main{status_bar}->configure(-text => "ERROR: Sample \"$sample\" is not numeric");
                return;
            }
        }
        $stats->{data} = \@data;
    }
    else # Multiple devices are selected
    {
        my @data;

        for my $device (@$selected_devices)
        {
            my $response = get_response($device->{list_index});

            # Check that response is a scalar number

            if(!is_numeric($response))
            {
                $main{status_bar}->configure(   -text => "ERROR: Response for device $device->{name} is not a scalar number");
                $stats->{status_bar}->configure(-text => "ERROR: Could not add responses") if(defined($stats->{status_bar}));
                return;
            }
            push(@data, $response);
        }
        push(@{$stats->{data}}, @data);
    }

    # Calculate statistics

    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@{$stats->{data}});

    $stats->{max}   = $stat->max();
    $stats->{min}   = $stat->min();
    $stats->{mean}  = $stat->mean();
    $stats->{range} = $stat->sample_range();
    $stats->{sd}    = $stat->standard_deviation();
    $stats->{sum}   = 0;
    $stats->{sum}  += $_ for @{$stat->{data}};

    return(1);
}

# Update statistics display

sub update_stats_display($)
{
    my ($stats) = @_;

    # Update statistics display

    $stats->{num_bins_scale}->configure(-from => 10, -to => scalar(@{$stats->{data}}) > 10 ? scalar(@{$stats->{data}}) : 10);
    $stats->{num_bins_scale}->set(10);
    $stats->{bin_size_entry}->configure(-text => nhimult(0.00000001, $stats->{range} / $stats->{num_bins_scale}->get));
    $stats->{stats_label}->configure(-text =>   "Samples: ".scalar(@{$stats->{data}})."\n".
                                                "Max:     $stats->{max}\n".
                                                "Mean:    ".sprintf("%.5e", $stats->{mean})."\n".
                                                "Min:     $stats->{min}\n".
                                                "Range:   ".sprintf("%.5e", $stats->{range})."\n".
                                                "SD:      ".sprintf("%.5e", $stats->{sd})."\n".
                                                "Sum:     ".sprintf("%.5e", $stats->{sum})
                                    );
}

# Show statistical analysis dialog

sub stats()
{
    my %stats;

    $main{status_bar}->configure(-text => '');

    # Initialise statistics

    init_stats(\%stats) or return;

    # Draw data analysis window

    $stats{window} = $main{window}->Toplevel(-title => "Data Analysis");
    $stats{window}->resizable(0, 0);
    $stats{window}->bind('<Destroy>', sub
                                      {
                                            if(defined($stats{tmp_file}))
                                            {
                                                close($stats{tmp_file});
                                                unlink($stats{tmp_file_name});
                                            }
                                            close($stats{gnuplot}) if(defined($stats{gnuplot}));
                                      });
    $stats{title_frame} = $stats{window}->Frame(-width => 50)->pack(-side => 'top', -fill => 'x');
    $stats{title_frame}->Label(-text => "Title: ", -font => $font.' bold')->pack(-side => 'left');
    $stats{title}       = $stats{title_frame}->Entry(
                                                        -bg     => 'white',
                                                        -font   => $font.' bold',
                                                        -text   => $main{command_entry}->get,
                                                        -width  => 30,
                                                    )->pack(-side => 'right', -fill => 'x', -expand => 1);
    $stats{status_bar}  = $stats{window}->Label(-font => $font.' bold')->pack(-side => 'bottom');
    $stats{stats_label} = $stats{window}->Label(
                                                -border     => 2,
                                                -font       => "$font",
                                                -height     => 7,
                                                -justify    => 'left',
                                                -relief     => 'ridge',
                                               )->pack(-side => 'top', -fill => 'both', -expand => 1);

    # Function to perform histogram plot with gnuplot

    sub plot_histogram(%)
    {
        my ($stats) = @_;

        $stats->{status_bar}->configure(-text => '');

        # Check whether there is already a temporary file open

        if(!defined($stats->{tmp_file}))
        {
            # Open a temporary file

            ($stats->{tmp_file}, $stats->{tmp_file_name}) = tempfile('fgcrun+XXXXXX', DIR => '/tmp');

            if(!defined($stats->{tmp_file}))
            {
                $stats->{status_bar}->configure(-text => "ERROR: Unable to open temporary file");
                return;
            }
            $stats->{tmp_file}->autoflush(1);
        }
        else # There is already a temporary file open
        {
            # Zero file

            truncate($stats->{tmp_file}, 0);
            seek($stats->{tmp_file}, 0, 0);
        }

        # Check whether there is already a gnuplot process open

        if(!defined($stats->{gnuplot}))
        {
            # Open a pipe to a new instance of gnuplot

            if(!open($stats->{gnuplot}, "|/usr/bin/gnuplot"))
            {
                $stats->{status_bar}->configure(-text => "ERROR: Unable to open gnuplot");
                return;
            }
            $stats->{gnuplot}->autoflush(1);
        }

        my @bins;
        my @bin_values;
        my $bin_size    = $stats->{bin_size_entry}->get;

        # Check that bin size is numeric

        if(!is_numeric($bin_size))
        {
            $stats->{status_bar}->configure(-text => "ERROR: Specified bin size is not numeric");
            return;
        }

        # Check that there will be more than one bin

        if($bin_size >= $stats->{range})
        {
            $stats->{status_bar}->configure(-text => "ERROR: Specified bin size is too large");
            return;
        }

        # Put data into bins

        for my $sample (@{$stats->{data}})
        {
            $bins[($sample - $stats->{min}) / $bin_size]++;
        }

        # Set bin values

        for(my $i = 0 ; $i < @bins ; $i++)
        {
            $bin_values[$i] = $stats->{min} + ($bin_size * ($i + 0.5));
        }

        # Check whether upper boundary of $bins[-2] is maximum value

        if($bin_values[-2] + ($bin_size / 2) == $stats->{max})
        {
            # Merge last bin into penultimate bin

            $bins[-2] += $bins[-1];
            delete($bins[-1]);
            delete($bin_values[-1]);
        }

        # Write data into temporary file

        my $bin_max = 0;
        for(my $i = 0 ; $i < @bins ; $i++)
        {
            $bins[$i]   = 0 if(!defined($bins[$i]));
            $bin_max    = $bins[$i] if($bins[$i] > $bin_max);

            print {$stats->{tmp_file}} "$bin_values[$i] $bins[$i]\n";
        }

        # Send commands to gnuplot to perform plot

        print {$stats->{gnuplot}}   "set mouse\n",
                                    "set boxwidth $bin_size absolute\n",
                                    "set style fill solid 0.25 border\n",
                                    "set samples ".scalar(@bins)."\n",
                                    "set key off\n",
                                    "set xlabel \"Bin size = $bin_size\"\n",
                                    "set xrange [ ".($bin_values[0] - ($bin_size / 2))." : ".($bin_values[-1] + ($bin_size / 2))." ] noreverse nowriteback\n",
                                    "set yrange [ 0 : $bin_max ] noreverse nowriteback\n",
                                    "set title \"", $stats->{title}->get, "\"\n",
                                    "plot \"$stats->{tmp_file_name}\" title 'values' with boxes\n";
    }

    $stats{button_frame}    = $stats{window}->Frame->pack(-side => 'bottom', -fill => 'x');
    $stats{add_button}      = $stats{button_frame}->Button(
                                                            -border     => 2,
                                                            -command    => sub
                                                                           {
                                                                                $main{status_bar}->configure(-text => '');
                                                                                init_stats(\%stats);
                                                                                update_stats_display(\%stats);
                                                                                plot_histogram(\%stats) if(defined($stats{gnuplot}));
                                                                           },
                                                            -relief     => 'groove',
                                                            -text       => "Add responses",
                                                            -width      => 13,
                                                          )->pack(-side => 'left');

    $stats{histo_button}    = $stats{button_frame}->Button(
                                                            -border     => 2,
                                                            -command    => [\&plot_histogram, \%stats],
                                                            -relief     => 'groove',
                                                            -text       => "Histogram",
                                                            -width      =>  9,
                                                          )->pack(-side => 'right');

    $stats{num_bins_frame}  = $stats{window}->Frame->pack(-side => 'bottom', -fill => 'x');
    $stats{num_bins_frame}->Label(-text => "Bins: ", -font => $font.' bold')->pack(-side => 'left');
    $stats{num_bins_scale}  = $stats{num_bins_frame}->Scale(-orient => "h")->pack(-side => 'right', -fill => 'x', -expand => 1);
    $stats{bin_size_frame}  = $stats{window}->Frame(-width => 50)->pack(-side => 'bottom', -fill => 'x');
    $stats{bin_size_frame}->Label(-text => "Bin size: ", -font => $font.' bold')->pack(-side => 'left');

    $stats{bin_size_entry}  = $stats{bin_size_frame}->Entry(
                                                            -bg     => 'white',
                                                            -font   => $font,
                                                            -width  => 10,
                                                           )->pack(-side => 'right', -fill => 'x', -expand => 1);

    $stats{bin_size_entry}->bind('<Key-Return>',    sub
                                                    {
                                                        if(defined($stats{gnuplot}))
                                                        {
                                                            plot_histogram(\%stats)
                                                        }
                                                        else
                                                        {
                                                            $stats{histo_button}->focusForce
                                                        }
                                                    });

    $stats{num_bins_scale}->configure(-command =>   sub
                                                    {
                                                        $stats{bin_size_entry}->configure(-text => nhimult(0.00000001, $stats{range} / $stats{num_bins_scale}->get))
                                                    });

    $stats{num_bins_scale}->bind('<ButtonRelease>', sub { plot_histogram(\%stats) if(defined($stats{gnuplot})); });

    update_stats_display(\%stats);

    # Balloon help

    $main{balloon}->attach($stats{bin_size_entry},  -msg => "Size of histogram bins.");
    $main{balloon}->attach($stats{num_bins_scale},  -msg => "Number of histogram bins.");
    $main{balloon}->attach($stats{histo_button},    -msg => "Display histogram.");
    $main{balloon}->attach($stats{stats_label},     -msg => "Statistics.");
    $main{balloon}->attach($stats{title},           -msg => "Data title.");

    # Set focus to histogram button

    $stats{histo_button}->focusForce;
}

# Count instances of symbols in responses

sub symbol_counts(;$)
{
    my ($window) = @_;

    $main{status_bar}->configure(-text => '');

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    # Count instances of each symbol

    my %counts;
    for my $device (@$selected_devices)
    {
        my $response = get_response($device->{list_index});

        next if($response =~ /^ERROR:/);

        for my $symbol (split(/[, ]/, $response))
        {
            $counts{$symbol}++;
        }
    }

    if(defined($window))
    {
        # Update an existing window

        update_symbol_counts(\%counts, $window);
    }
    else
    {
        # Draw a new window

        draw_symbol_counts(\%counts);
    }
}

# Update symbol counts window

sub update_symbol_counts($$)
{
    my ($counts, $window) = @_;

    $window->{lists}->{symbols}->selection('clear', 0, 'end');

    $window->{symbol_count} = $counts;

    if($window->{sort_by_symbol})
    {
        @{$window->{symbols}}   = sort(keys(%$counts));
    }
    else
    {
        @{$window->{symbols}}   = sort {
                                        $counts->{$b} <=> $counts->{$a} ||
                                        $a cmp $b
                                       } keys(%$counts);
    }
    @{$window->{counts}} = map { $counts->{$_} } @{$window->{symbols}};
}

# Draw symbol counts window

sub draw_symbol_counts($)
{
    my ($counts) = @_;

    my %symbol_count;
    $symbol_count{symbol_count} = $counts;
    $symbol_count{command}      = $main{command_entry}->get;

    @{$symbol_count{symbols}}   = sort {
                                        $counts->{$b} <=> $counts->{$a} ||
                                        $a cmp $b
                                       } keys(%$counts);
    return if(!@{$symbol_count{symbols}});

    @{$symbol_count{counts}} = map { $counts->{$_} } @{$symbol_count{symbols}};

    # Draw window

    $symbol_count{window} = $main{window}->Toplevel(-title => "Response symbol counts");
    $symbol_count{window}->Label(
                                    -bg         => 'white',
                                    -font       => $font.' bold',
                                    -justify    => 'left',
                                    -text       => $symbol_count{command},
                                )->pack(-side => 'top', -fill => 'both');
    $symbol_count{list_frame}       = $symbol_count{window}->Frame->pack(-side => 'top', -fill => 'both', -expand => 1);

    $symbol_count{lists}->{symbols} = $symbol_count{list_frame}->Listbox(
                                                                            -bg                 => 'white',
                                                                            -borderwidth        => 1,
                                                                            -exportselection    => 0,
                                                                            -font               => $font,
                                                                            -height             => 20,
                                                                            -listvariable       => $symbol_count{symbols},
                                                                            -selectmode         => 'extended',
                                                                            -takefocus          => 0,
                                                                            -width              => 30,
                                                                        )->pack(-side => 'left', -fill => 'both', -expand => 1);

    $symbol_count{lists}->{symbols}->bind('<Double-Button-1>', sub
                                                               {
                                                                    $main{command_entry}->focusForce;
                                                                    $symbol_count{window}->destroy;
                                                               });
    $symbol_count{lists}->{symbols}->bind('<<ListboxSelect>>', sub
                                                               {
                                                                    my $list = shift;

                                                                    $main{lists}->{devices}->selection('clear', 0, 'end');

                                                                    # Get selected symbols

                                                                    my @list_indices        = ref($list->curselection) ?
                                                                                                @{$list->curselection} :
                                                                                                 ($list->curselection);
                                                                    return if(!@list_indices);
                                                                    my @selected_symbols    = map { quotemeta($symbol_count{symbols}[$_]) } (@list_indices);

                                                                    # Perform regex match on selected symbols

                                                                    my @rsp_match_indices;
                                                                    regex_expression('(^|,| )'.join('($|,| )|(^|,| )', @selected_symbols).'($|,| | )', '=~', [get_responses_array()], \@rsp_match_indices);

                                                                    # Set device list selection to matched

                                                                    $main{lists}->{devices}->selection('set', $_) for(@rsp_match_indices);
                                                                    count_selected_devices();

                                                                    # Show first match

                                                                    $main{lists}->{devices}->see($rsp_match_indices[0]) if(@rsp_match_indices);
                                                               });

    $symbol_count{lists}->{counts} = $symbol_count{list_frame}->Listbox(
                                                                            -bg                 => 'white',
                                                                            -borderwidth        => 1,
                                                                            -exportselection    => 0,
                                                                            -font               => $font,
                                                                            -height             => 20,
                                                                            -listvariable       => $symbol_count{counts},
                                                                            -selectmode         => 'extended',
                                                                            -takefocus          => 0,
                                                                            -width              => 5,
                                                                       )->pack(-side => 'left', -fill => 'y');

    $symbol_count{lists}->{counts}->bind('<Button-1>', sub { $symbol_count{lists}->{counts}->selection('clear', 0, 'end') });

    $symbol_count{scrollbar} = $symbol_count{list_frame}->Scrollbar(-orient => "vertical", -elementborderwidth => 1);

    $symbol_count{lists}->{symbols}->configure( -yscrollcommand => sub { $symbol_count{scrollbar}->set(@_); for my $list (values(%{$symbol_count{lists}})) { $list->yviewMoveto($_[0]) } });
    $symbol_count{lists}->{counts}->configure(  -yscrollcommand => sub { $symbol_count{lists}->{counts}->yviewMoveto(($symbol_count{lists}->{symbols}->yview)[0]) });
    $symbol_count{scrollbar}->configure(-command => sub { for my $list (values(%{$symbol_count{lists}})) { $list->yview(@_) } });
    $symbol_count{scrollbar}->pack(-side => 'right', -fill => 'both');

    $symbol_count{refresh} = $symbol_count{window}->Button(
                                                            -border     => 2,
                                                            -command    => sub
                                                                           {
                                                                                $main{command_entry}->delete(0, 'end');
                                                                                $main{command_entry}->insert(0, $symbol_count{command});
                                                                                process_command();
                                                                                symbol_counts(\%symbol_count);
                                                                           },
                                                            -relief     => 'groove',
                                                            -text       => 'Refresh',
                                                            -width      => 8,
                                                          )->pack(-side => 'bottom');

    $symbol_count{sort_by_symbol} = 0;
    $symbol_count{window}->Checkbutton(
                                                -border     => 2,
                                                -command    => sub
                                                               {
                                                                    # Get selected symbols

                                                                    my $list = $symbol_count{lists}->{symbols};

                                                                    my @list_indices = ref($list->curselection) ?
                                                                                         @{$list->curselection} :
                                                                                          ($list->curselection);
                                                                    my %selected_symbols = map { $symbol_count{symbols}->[$_] => 1 } (@list_indices);

                                                                    $list->selection('clear', 0, 'end');

                                                                    # Perform sort

                                                                    if($symbol_count{sort_by_symbol})
                                                                    {
                                                                        @{$symbol_count{symbols}}   = sort(@{$symbol_count{symbols}});
                                                                    }
                                                                    else
                                                                    {
                                                                        @{$symbol_count{symbols}}   = sort {
                                                                                                            $symbol_count{symbol_count}->{$b} <=> $symbol_count{symbol_count}->{$a} ||
                                                                                                            $a cmp $b
                                                                                                           } @{$symbol_count{symbols}};
                                                                    }
                                                                    @{$symbol_count{counts}} = map { $symbol_count{symbol_count}->{$_} } @{$symbol_count{symbols}};

                                                                    # Restore selection

                                                                    for(my $i = 0 ; $i < @{$symbol_count{symbols}} ; $i++)
                                                                    {
                                                                        if(defined($selected_symbols{$symbol_count{symbols}->[$i]}))
                                                                        {
                                                                            $list->selection('set', $i);
                                                                        }
                                                                    }
                                                               },
                                                -relief     => 'groove',
                                                -text       => "Sort by symbol",
                                                -variable   => \$symbol_count{sort_by_symbol},
                                            )->pack(-side => 'bottom', -fill => 'x');

    # Set the minimum window size

    $symbol_count{window}->update;
    $symbol_count{window}->minsize($symbol_count{window}->winfo("width"),
                                   $symbol_count{window}->winfo("height"));

    $symbol_count{refresh}->focusForce;
}

# Show list editor dialog

sub list_editor($$)
{
    my %editor;

    $editor{devices}    = [@{$_[0]}];
    $editor{responses}  = [@{$_[1]}];

    $editor{history_idx} = 0;

    my $list_editor_shortcut_help = "Ctrl-L:        Open a new list editor window\n".
                                    "Ctrl-c:        Close list editor window\n".
                                    "\n".
                                    "Ctrl-A:        Select all\n".
                                    "Shift-Escape:  Clear selection\n".
                                    "Escape:        Clear expression area\n".
                                    "Return:        Apply expression\n".
                                    "\n".
                                    "Ctrl-s:        Set main window device selection to result\n".
                                    "Ctrl-a:        Set main window device selection to and of results\n".
                                    "Ctrl-o:        Set main window device selection to or of results\n".
                                    "\n".
                                    "Down-arrow:    Next item in list editor history\n".
                                    "Up-arrow:      Previous item in list editor history";

    # Draw list editor window

    $editor{window} = $main{window}->Toplevel(-title => "Edit device list");
    $editor{window}->bind('<Destroy>', [\&list_editor_close, \%editor]);

    # Create menubar

    $editor{menubar}        = $editor{window}->Frame(-relief => 'groove', -borderwidth => 2)->pack(-side => 'top', -fill => 'x');
    $editor{menus}->{list}  = $editor{menubar}->Menubutton(-text => "List", -underline => 0, -tearoff => 0)->pack(-side => 'left');
    $editor{menus}->{help}  = $editor{menubar}->Menubutton(-text => "Help", -underline => 0, -tearoff => 0)->pack(-side => 'left');

    # Populate list menu

    $editor{menus}->{list}->command(-label => "Open list editor",           -command => [\&list_editor, $editor{devices}, $editor{responses}], -accelerator => "Ctrl-l");
    $editor{menus}->{list}->command(-label => "Perform AND of open lists",  -command => \&list_editor_and);
    $editor{menus}->{list}->command(-label => "Perform OR of open lists",   -command => \&list_editor_or);

    # Populate help menu

    $editor{menus}->{help}->command(-label      => "Shortcut keys",
                                    -command    =>  sub
                                                    {
                                                        $editor{window}->Toplevel(-title => "Shortcut key help")->Scrolled('Label',
                                                                                                                            -scrollbars => 'osoe',
                                                                                                                            -bg         => 'white',
                                                                                                                            -font       => $font,
                                                                                                                            -justify    => 'left',
                                                                                                                            -text       => $shortcut_help,
                                                                                                                        )->pack});
    $editor{menus}->{help}->command(-label      => "Expression types",
                                    -command    =>  sub
                                                    {
                                                        $editor{window}->Toplevel(-title => "Expression type help")->Label(
                                                                                                                            -bg         => 'white',
                                                                                                                            -font       => $font,
                                                                                                                            -justify    => 'left',
                                                                                                                            -text       => $expression_help,
                                                                                                                          )->pack;
                                                    });

    # Draw device count

    $editor{device_count_frame} = $editor{window}->Frame->pack(-side => 'top', -fill => 'both');
    $editor{device_count_frame}->Label(-font => $font.' bold', -text => '')->pack(-side => 'left', -fill => 'both', -expand => 1);
    $editor{selected_devices}   = $editor{device_count_frame}->Label(-width => 4, -font => $font.' bold', -text => "0")->pack(-side => 'left');
    $editor{device_count_frame}->Label(-width => 1, -font => $font.' bold', -text => "/")->pack(-side => 'left');
    $editor{visible_devices}    = $editor{device_count_frame}->Label(-width => 4, -font => $font.' bold', -text => scalar(@{$editor{devices}}))->pack(-side => 'left');
    $editor{device_count_frame}->Label(-font => $font.' bold', -text => '')->pack(-side => 'left', -fill => 'both', -expand => 1);

    # Draw command area

    $editor{command_frame}  = $editor{window}->Frame->pack(-side => 'bottom', -fill => 'both');
    $editor{status_bar}     = $editor{window}->Label(-width => 80, -font => $font.' bold')->pack(-side => 'bottom');

    $editor{command_entry}  = $editor{window}->Entry(
                                                        -bg                 => 'white',
                                                        -font               => $font,
                                                        -highlightthickness => 0,
                                                        -relief             => 'ridge',
                                                        -width              => 80,
                                                    )->pack(-fill => 'x', -side => 'bottom');

    $editor{command_entry}->bindtags([($editor{command_entry}->bindtags)[1, 0, 2, 3]]);

    $editor{command_entry}->bind('<Control-Key-a>',     sub { list_editor_and(); $editor{command_entry}->break });
    $editor{command_entry}->bind('<Control-Key-A>',     sub
                                                        {
                                                            $editor{lists}->{devices}->selection('set', 0, 'end');
                                                            $editor{lists}->{responses}->selection('set', 0, 'end');
                                                            $editor{selected_devices}->configure(-text => scalar(@{$editor{devices}}));
                                                            $editor{command_entry}->break;
                                                        });

    $editor{command_entry}->bind('<Control-Key-c>',     sub { $editor{window}->destroy });
    $editor{command_entry}->bind('<Control-Key-L>',     sub { list_editor($editor{devices}, $editor{responses}) });
    $editor{command_entry}->bind('<Control-Key-o>',     sub { list_editor_or(); $editor{command_entry}->break });
    $editor{command_entry}->bind('<Control-Key-s>',     sub { list_editor_set(\%editor); $editor{command_entry}->break });
    $editor{command_entry}->bind('<Down>',              sub
                                                        {
                                                            $editor{command_entry}->delete(0, 'end');

                                                            if($editor{history_idx} < -1 && defined($list_editor_history[$editor{history_idx} + 1]))
                                                            {
                                                                $editor{command_entry}->insert(0, $list_editor_history[++$editor{history_idx}]);
                                                            }
                                                            else
                                                            {
                                                                $editor{history_idx} = 0;
                                                            }
                                                            $editor{command_entry}->break;
                                                        });

    $editor{command_entry}->bind('<Up>',                sub
                                                        {
                                                            if(defined($list_editor_history[$editor{history_idx} - 1]))
                                                            {
                                                                $editor{command_entry}->delete(0, 'end');
                                                                $editor{command_entry}->insert(0, $list_editor_history[--$editor{history_idx}]);
                                                            }
                                                            $editor{command_entry}->break;
                                                        });

    $editor{command_entry}->bind('<Key-Escape>',        sub
                                                        {
                                                            $editor{command_entry}->delete(0, 'end');
                                                            $editor{status_bar}->configure(-text => '');
                                                            $editor{command_entry}->break;
                                                        });

    $editor{command_entry}->bind('<Shift-Key-Escape>',  sub
                                                        {
                                                            $editor{lists}->{devices}->selection('clear', 0, 'end');
                                                            $editor{lists}->{responses}->selection('clear', 0, 'end');
                                                            $editor{selected_devices}->configure(-text => "0");
                                                            $editor{command_entry}->break;
                                                        });

    $editor{command_entry}->bind('<Key-Return>',        [\&list_editor_process, \%editor]);
    $editor{command_entry}->bind('<Key-KP_Enter>',      [\&list_editor_process, \%editor]);

    $editor{and_button} = $editor{command_frame}->Button(
                                                            -border     => 2,
                                                            -command    => [\&list_editor_and, \%editor],
                                                            -relief     => 'groove',
                                                            -state      => 'disabled',
                                                            -text       => 'And',
                                                            -width      => 4,
                                                        )->pack(-side => 'left');

    $editor{or_button}  = $editor{command_frame}->Button(
                                                            -border     => 2,
                                                            -command    => [\&list_editor_or,  \%editor],
                                                            -relief     => 'groove',
                                                            -state      => 'disabled',
                                                            -text       => 'Or',
                                                            -width      => 4,
                                                        )->pack(-side => 'left');

    $editor{set_button} = $editor{command_frame}->Button(
                                                            -border     => 2,
                                                            -command    => [\&list_editor_set, \%editor],
                                                            -relief     => 'groove',
                                                            -text       => 'Set',
                                                            -width      => 4,
                                                        )->pack(-side => 'right');

    # Draw lists

    $editor{lists}->{devices} = $editor{window}->Listbox(
                                                            -bg                 => 'white',
                                                            -borderwidth        => 1,
                                                            -exportselection    => 0,
                                                            -font               => $font,
                                                            -height             => FGC_MAX_DEVS_PER_GW,
                                                            -listvariable       => $editor{devices},
                                                            -selectmode         => 'single',
                                                            -takefocus          => 0,
                                                            -width              => FGC_MAX_DEV_LEN + 1,
                                                        )->pack(-side => 'left', -fill => 'both');

    $editor{lists}->{devices}->bindtags([($editor{lists}->{devices}->bindtags)[1, 0, 2, 3]]);
    $editor{lists}->{devices}->bind('<Button-1>', sub { $editor{lists}->{devices}->break });

    $editor{lists}->{responses} = $editor{window}->Listbox(
                                                            -bg                 => 'white',
                                                            -borderwidth        => 1,
                                                            -exportselection    => 0,
                                                            -font               => $font,
                                                            -height             => FGC_MAX_DEVS_PER_GW,
                                                            -listvariable       => $editor{responses},
                                                            -selectmode         => 'single',
                                                            -takefocus          => 0,
                                                            -width              => 80 - (FGC_MAX_DEV_LEN + 1),
                                                          )->pack(-side => 'left', -fill => 'both', -expand => 1);

    $editor{lists}->{responses}->bindtags([($editor{lists}->{responses}->bindtags)[1, 0, 2, 3]]);
    $editor{lists}->{responses}->bind('<Button-1>', sub { $editor{lists}->{responses}->break });

    # Configure scrollbar

    $editor{scrollbar} = $editor{window}->Scrollbar(-orient => "vertical", -elementborderwidth => 1)->pack(-side => 'right', -fill => 'both');

    for my $list (values(%{$editor{lists}}))
    {
        $list->configure(-yscrollcommand => sub { $editor{scrollbar}->set(@_); for(values(%{$editor{lists}})) { $_->yviewMoveto($_[0]) } });
    }
    $editor{scrollbar}->configure(-command => sub { for my $list (values(%{$editor{lists}})) { $list->yview(@_) } });

    # Balloon help

    $main{balloon}->attach($editor{and_button},         -msg => "And button\n\nClick to apply a logical AND between the results of all open list editors.");
    $main{balloon}->attach($editor{command_entry},      -msg => "Expression field\n\nEnter the expression to match here then press Enter.");
    $main{balloon}->attach($editor{lists}->{devices},   -msg => "Device list\n\nAvailable devices are displayed here.\nThe selection indicates with responses that the entered expression matches.");
    $main{balloon}->attach($editor{lists}->{responses}, -msg => "Response list\n\nResponses to commands appear here.\nThe selection indicates those that the entered expression matches");
    $main{balloon}->attach($editor{or_button},          -msg => "Or button\n\nClick to apply a logical OR between the results of all open list editors.");
    $main{balloon}->attach($editor{selected_devices},   -msg => "Number of selected devices.");
    $main{balloon}->attach($editor{set_button},         -msg => "Set button\n\nClick to apply result of this list editor to the main window.");
    $main{balloon}->attach($editor{status_bar},         -msg => "Status bar\n\nMessages about the result of the entered expression are displayed here.");
    $main{balloon}->attach($editor{visible_devices},    -msg => "Number of visible devices.");

    # Add editor to hash of list editors

    $list_editors{\%editor} = \%editor;

    # Enable AND and OR buttons if there are multiple list editors

    my @list_editors = values(%list_editors);

    if(scalar(@list_editors) == 2)
    {
        for(@list_editors)
        {
            $_->{and_button}->configure(-state => 'normal');
            $_->{or_button}->configure(-state => 'normal');
        }
    }
    elsif(scalar(@list_editors) > 2)
    {
        $editor{and_button}->configure(-state => 'normal');
        $editor{or_button}->configure(-state => 'normal');
    }

    # Set the minimum window size

    $editor{window}->update;
    $editor{window}->minsize($editor{window}->winfo("width"),
                             $editor{window}->winfo("height"));

    $editor{command_entry}->focusForce;
    $main{command_entry}->break;
}

# Clean up on closing list editor

sub list_editor_close($)
{
    my ($ev_widget, $editor) = @_;

    return if(ref($ev_widget) !~ 'Tk::Toplevel');

    delete($list_editors{$editor});

    # Disable AND and OR buttons if only one list editor remains

    my @list_editors = values(%list_editors);

    if(scalar(@list_editors) == 1)
    {
        for(@list_editors)
        {
            $_->{and_button}->configure(-state => 'disabled');
            $_->{or_button}->configure( -state => 'disabled');
        }
    }
}

# Process list editor expression

sub list_editor_process($)
{
    shift;
    my ($editor) = @_;

    $editor->{status_bar}->configure(-text => '');
    $editor->{history_idx} = 0;

    my $expression = $editor->{command_entry}->get;

    my @selected_indices;
    my $error = process_expression($expression, $editor->{responses}, \@selected_indices, \@list_editor_history);

    # Display error if defined

    if(defined($error))
    {
        $editor->{status_bar}->configure(-text => "ERROR: $error");
        return;
    }

    # Clear current selection

    $editor->{lists}->{devices}->selection(  'clear', 0, 'end');
    $editor->{lists}->{responses}->selection('clear', 0, 'end');

    # Apply new selection

    for(@selected_indices)
    {
        $editor->{lists}->{devices}->selection(  'set', $_, $_);
        $editor->{lists}->{responses}->selection('set', $_, $_);
    }

    # Display number of matches

    $editor->{selected_devices}->configure(-text => scalar(@selected_indices));
}

# Process expression

sub process_expression($$$;$)
{
    my ($full_expression, $values, $matching_indices, $history) = @_;

    # Extract type and expression

    if($full_expression !~ /^([!=<>|][~=<>]{0,1})\s*(.*?)\s*$/) # Expression was invalid
    {
        return("Invalid expression: '$full_expression'");
    }

    my $type        = $1;
    my $expression  = $2;

    # Process expression type

    my $error;

    if($type eq  "="    or
       $type eq "!=")
    {
        $error = equal_expression($expression, $type, $values, $matching_indices);
    }
    elsif($type eq "=~" or
          $type eq "!~")
    {
        $error = regex_expression($expression, $type, $values, $matching_indices);
    }
    elsif($type eq ">"  or
          $type eq ">=" or
          $type eq "|>" or
          $type eq "<"  or
          $type eq "<=" or
          $type eq "|<")
    {
        $error = gtlt_expression($expression, $type, $values, $matching_indices);
    }
    else
    {
        $error = "Invalid expression type [$type]";
    }

    # Add expression to history

    if(!defined($error) && defined($history))
    {
        push(@$history, $full_expression) if(!defined($history->[-1]) || $full_expression ne $history->[-1]);

        # Remove old items from history

        shift(@$history) if(@$history > 50);
    }

    return($error);
}

# Find values exactly equaling or not equaling expression

sub equal_expression($$$$)
{
    my ($expression, $type, $values, $matching_indices) = @_;

    # Check whether expression is numeric

    my $numeric = is_numeric($expression);

    for(my $i = 0 ; $i < @{$values} ; $i++)
    {
        # Check whether expression is numeric

        if($numeric) # Expression is numeric
        {
            if(
                is_numeric($values->[$i])                           &&
                (($type eq  "=" && $values->[$i] == $expression)    ||
                 ($type eq "!=" && $values->[$i] != $expression))
              )
            {
                push(@$matching_indices, $i);
            }
        }
        else # Expression is not numeric
        {
            if(
                ($type eq  "=" && $values->[$i] eq $expression) ||
                ($type eq "!=" && $values->[$i] ne $expression)
              )
            {
                push(@$matching_indices, $i);
            }
        }
    }
    return(undef);
}

# Find values matching regular expression

sub regex_expression($$$$)
{
    my ($expression, $type, $values, $matching_indices) = @_;

    for(my $i = 0 ; $i < @{$values} ; $i++)
    {
        eval
        {
            if(
                ($type eq "=~" && $values->[$i] =~ $expression) ||
                ($type eq "!~" && $values->[$i] !~ $expression)
              )
            {
                push(@$matching_indices, $i);
            }
        };

        # Check whether an error occurred

        return("Bad regular expression") if($@);
    }
    return(undef);
}

# Find values greater than or less than

sub gtlt_expression($$$$)
{
    my ($expression, $type, $values, $matching_indices) = @_;

    # Check whether expression is numeric

    return("Expression is not numeric") if(!is_numeric($expression));

    for(my $i = 0 ; $i < @{$values} ; $i++)
    {
        next if(!is_numeric($values->[$i])); # Value is not numeric

        if(
            ($type eq ">"  && $values->[$i]         >  $expression)         ||
            ($type eq ">=" && $values->[$i]         >= $expression)         ||
            ($type eq "|>" && abs($values->[$i])    >  abs($expression))    ||
            ($type eq "<"  && $values->[$i]         <  $expression)         ||
            ($type eq "<=" && $values->[$i]         <= $expression)         ||
            ($type eq "|<" && abs($values->[$i])    <  abs($expression))
          )
        {
            push(@$matching_indices, $i);
        }
    }
    return(undef);
}

# Set main window devices to those selected in the list editor

sub list_editor_set($)
{
    my ($editor) = @_;

    my @selected_indices = ref($editor->{lists}->{devices}->curselection) ?
                             @{$editor->{lists}->{devices}->curselection} :
                              ($editor->{lists}->{devices}->curselection);
    my %selected_devices = map { $devices->{$editor->{devices}->[$_]}->{name} => $devices->{$editor->{devices}->[$_]} } @selected_indices;

    # Apply selection

    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{list_index}) && defined($selected_devices{$device->{name}}))
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();
    $editor->{window}->destroy;
}

# Set main window devices to a logical and of those selected in list editors

sub list_editor_and()
{
    my @list_editors = values(%list_editors);

    # Get devices selected in first editor

    my $editor = shift(@list_editors);
    my @selected_indices = ref($editor->{lists}->{devices}->curselection) ?
                             @{$editor->{lists}->{devices}->curselection} :
                              ($editor->{lists}->{devices}->curselection);
    my %selected_devices = map { $devices->{$editor->{devices}->[$_]}->{name} => $devices->{$editor->{devices}->[$_]} } @selected_indices;

    for my $editor (values(%list_editors))
    {
        my @list_selected_indices = ref($editor->{lists}->{devices}->curselection) ?
                                      @{$editor->{lists}->{devices}->curselection} :
                                       ($editor->{lists}->{devices}->curselection);
        my %list_selected_devices = map { $devices->{$editor->{devices}->[$_]}->{name} => $devices->{$editor->{devices}->[$_]} } @list_selected_indices;

        # Delete devices not selected in this list from global selection

        for my $device (keys(%selected_devices))
        {
            if(!defined($list_selected_devices{$device}))
            {
                delete($selected_devices{$device});
            }
        }
    }

    # Apply selection

    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{list_index}) && defined($selected_devices{$device->{name}}))
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index}) if(defined($device->{list_index}));
            }
        }
    }
    count_selected_devices();
}

# Set main window devices to a logical or of those selected in list editors

sub list_editor_or()
{
    my %selected_devices;

    for my $editor (values(%list_editors))
    {
        my @selected_indices = ref($editor->{lists}->{devices}->curselection) ?
                                 @{$editor->{lists}->{devices}->curselection} :
                                  ($editor->{lists}->{devices}->curselection);
        $selected_devices{$devices->{$editor->{devices}->[$_]}->{name}} = $devices->{$editor->{devices}->[$_]} for @selected_indices;
    }

    # Apply selection

    $main{lists}->{devices}->selection('clear', 0, 'end');

    for my $gateway (values(%gateways_connected))
    {
        for my $device (@{$gateway->{channels}})
        {
            if(defined($device->{list_index}) && defined($selected_devices{$device->{name}}))
            {
                $main{lists}->{devices}->selection('set', $device->{list_index}, $device->{list_index});
            }
        }
    }
    count_selected_devices();
}

# Send an e-mail report about devices

sub report_devices()
{
    my %report;

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: Report not possible from this platform");
        return;
    }

    # Retrieve list of responsibles

    my %fgc_responsible_addresses = (
                                    'Converter Controls Support'=> q(converter-controls-support@cern.ch),
                                    'EPC Injectors Piquet'      => q(te-dep-epc-piquet-injectors@cern.ch),
                                    'EPC LHC Piquet'            => q(te-dep-epc-piquet-LHC-LHCexperiments@cern.ch),
                                    'EPC SPS and Exp. Areas Piquet' => q(te-dep-epc-piquet-SPS-experimental-areas@cern.ch),
                                    'EPC Major Event Piquet'    => q(te-dep-epc-piquet-coordination-major-events-support@cern.ch),
                                    'Benoit Favre'              => q(Benoit.Favre@cern.ch),
                                    'David Nisbet'              => q(David.Nisbet@cern.ch),
                                    'Gilles Le Godec'           => q(Gilles.Le.Godec@cern.ch),
                                    'Greg Hudson'               => q(Greg.Hudson@cern.ch),
                                    'Hugues Thiesen'            => q(Hugues.Thiesen@cern.ch),
                                    'Laurent Ceccone'           => q(Laurent.Ceccone@cern.ch),
                                    'Ludovic Charnay'           => q(Ludovic.Charnay@cern.ch),
                                    'Michele Martino'           => q(Michele.Martino@cern.ch),
                                    'Miguel Cerqueira Bastos'   => q(Miguel.Cerqueira.Bastos@cern.ch),
                                    'Olivier Fournier'          => q(Olivier.Fournier@cern.ch),
                                    'Quentin King'              => q(Quentin.King@cern.ch),
                                    'Raul Murillo Garcia'       => q(Raul.Murillo.Garcia@cern.ch),
                                    'Serge Pittet'              => q(Serge.Pittet@cern.ch),
                                    'Stephen Page'              => q(Stephen.Page@cern.ch),
                                    'Valerie Montabonnet'       => q(Valerie.Montabonnet@cern.ch),
                                    'Vincent Barbet'            => q(Vincent.Barbet@cern.ch),
                                    'Yves Thurel'               => q(Yves.Thurel@cern.ch),
                                 );

    if(open(my $handle,"phonebook --department TE --group EPC --terse firstname --terse surname --terse email|"))
    {
        while (<$handle>)
        {
            chomp; # Eat newline at the end
            my ($firstname,$surname,$email) = split(";",$_);
            $fgc_responsible_addresses{$firstname . " " . $surname} = $email;
        }
    }
    else
    {
        warn "Can not open execute phonebook: $!\n";
    }

    # Retrieve list of selected devices

    $report{devices} = selected_devices();

    # Do nothing if no devices are selected

    $main{status_bar}->configure(-text => "ERROR: No devices selected"), return if(!@{$report{devices}});

    # Draw report window

    $report{window} = $main{window}->Toplevel(-title => "Report selected devices");

    $report{button_frame}   = $report{window}->Frame->pack(-side => 'top', -fill => 'both');
    $report{people}         = $report{button_frame}->Menubutton(
                                                                -border     => 2,
                                                                -relief     => 'groove',
                                                                -tearoff    => 0,
                                                                -text       => "Addresses",
                                                                -underline  => 0,
                                                               )->pack(-side => 'left');

    $report{people_menu}   = $report{people}->Menu( -tearoff => 0, -type => 'normal' );
    $report{people}->configure( -menu => $report{people_menu} );

    $report{send_button}    = $report{button_frame}->Button(
                                                            -border     => 2,
                                                            -command    => [\&report_send, \%report],
                                                            -relief     => 'groove',
                                                            -text       => "Send",
                                                            -width      => 4,
                                                           )->pack(-side => 'right');
    $report{send_responses} = 0;
    $report{button_frame}->Checkbutton(
                                        -border     => 2,
                                        -relief     => 'groove',
                                        -text       => "Send responses",
                                        -underline  => 0,
                                        -variable   => \$report{send_responses},
                                      )->pack(-side => 'bottom');

    my $count = 0;
    for my $name (sort keys(%fgc_responsible_addresses))
    {
        $report{people_menu}->command(-label => " " . $name . "  ",
                                      -columnbreak => (( $count % 40) == 0),
                                      -command => [\&report_add_email, \%report, $fgc_responsible_addresses{$name}],);
        $count += 1;
    }

    $report{address_frame}  = $report{window}->Frame->pack(-side => 'top', -fill => 'both');

    $report{list_label}     = $report{address_frame}->Label(
                                                            -font => $font,
                                                            -text => "NB: The list of ".scalar(@{$report{devices}})." selected devices will be sent with this message",
                                                           )->pack(-side => 'bottom');

    $report{device_balloon} = $report{window}->Balloon(-initwait => 1000, -font => $font, -bg => 'light yellow');
    $report{device_balloon}->attach($report{list_label}, -msg => join("\n", map { $_->{name} } @{$report{devices}}));
    $report{address_frame}->Label(-font => $font, -text => "To:")->pack(-side => 'left');

    $report{addresses}      = $report{address_frame}->Entry(
                                                            -bg                 => 'white',
                                                            -font               => $font,
                                                            -highlightthickness => 0,
                                                            -relief             => 'ridge',
                                                            -width              => 75,
                                                           )->pack(-fill => 'x', -side => 'left', -expand => 1);

    $report{status_bar}     = $report{window}->Label(-width => 80, -font => $font.' bold')->pack(-side => 'bottom');

    $report{text}           = $report{window}->Text(
                                                    -bg     => 'white',
                                                    -font   => $font,
                                                    -height => 30,
                                                    -undo   => 'true',
                                                    -width  => 80,
                                                    -wrap   => 'word',
                                                   )->pack(-side => 'left', -fill => 'both', -expand => 1);

    $report{text}->bind('<Control-Key-Return>', sub { report_send(\%report); $report{text}->break; });
    $report{text}->bind('<Control-Key-KP_Enter>', sub { report_send(\%report); $report{text}->break; });

    # Balloon help

    $main{balloon}->attach($report{addresses},      -msg => "Addresses field\n\nEnter e-mail addresses to which to send the message.");
    $main{balloon}->attach($report{people},         -msg => "Addresses\n\nClick to add addresses from a useful list of people.");
    $main{balloon}->attach($report{send_button},    -msg => "Send button\n\nClick to send the message and device list.");
    $main{balloon}->attach($report{status_bar},     -msg => "Status bar\n\nImportant messages are displayed here.");
    $main{balloon}->attach($report{text},           -msg => "Message text\n\nEnter the message concerning the devices here.");

    # Set the minimum window size

    $report{window}->update;
    $report{window}->minsize($report{window}->winfo("width"),
                             $report{window}->winfo("height"));

    $report{text}->focusForce;
    $main{command_entry}->break;
}

# Add an e-mail address to a report

sub report_add_email($$)
{
    my ($report, $address) = @_;

    $report->{addresses}->insert('end', ", ") if($report->{addresses}->get !~ /^\s*$/);
    $report->{addresses}->insert('end',  $address);
    $report->{addresses}->xview('end');
}

# Send a report

sub report_send($)
{
    my ($report) = @_;

    my @addresses = split(/\s*[,;]\s*/, $report->{addresses}->get);

    # Check whether at least one address was specified

    if(!@addresses)
    {
        $report->{status_bar}->configure(-text => "ERROR: No e-mail address specified");
        return;
    }

    # Set responses for devices to be reported

    for my $device (@{$report->{devices}})
    {
        next if(!defined($device->{list_index}));

        $device->{response} =  get_response($device->{list_index});
        $device->{response} =~ s/[\012\015]+/\$/g;
    }

    # Allow file to be modified by anyone

    my $umask = umask();
    umask(0000);

    # Write device list

    my $filename = FGC_DROPBOX_PATH."/device_lists/mailed/".time."_$$.txt";

    if(!FGC::DeviceList::write($report->{devices}, $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }
    umask($umask);

    # Remove responses from device entries

    delete($_->{response}) for(@{$report->{devices}});

    # Obtain the user's name

    my $user = obtain_users_name();

    # Compose mail message

    my %mail = (
                    'Content-type'  => 'text/html',
                    From            => "$user\@mail.cern.ch",
                    To              => join(', ', @addresses),
                    Subject         => 'FGCRun+ report from '.(getpwuid($<))[0].'@'.hostname(),
               );

    (my $text = $report->{text}->get("1.0", 'end')) =~ s/\n/<BR>\n/g;

    $mail{Message} =    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n".
                        "<HTML>\n".
                        "<HEAD>\n".
                        "<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n".
                        "<TITLE>FGCRun+ report</TITLE>\n".
                        "</HEAD>\n".
                        "<BODY>\n".
                        "<CODE>\n".
                        $text.
                        "</CODE>\n".
                        "----------<BR>\n".
                        "<P>\n".
                        "This message relates to the following ".scalar(@{$report->{devices}})." devices:<BR>\n".
                        "<BR>\n".
                        "<CODE>\n";

    for my $device (@{$report->{devices}})
    {
        #example: http://cern.chfgc-device-status/statusdev.html?cfc-400-rl4src&CFC-400-RL4SRC&status&statustbl_h.html&12&RPZES.400.L4L.RQF.123
        $mail{Message} .= "<A href=\"http://cern.ch/fgc-device-status/statusdev.html".
                          "?$device->{gateway}->{name}".
                          "&$device->{gateway}->{channels}->[0]->{name}".
                          "&status&statustbl_h.html".
                          "&$device->{channel}".
                          "&$device->{name}\">".
                          "$device->{name}".
                          "</A>";

        if($report->{send_responses})
        {
            my $response    =  defined($device->{list_index}) ? get_response($device->{list_index}) : '';
            $response       =~ s/[\012\015]+/\$/g;

            if(length($response) > (72 - length($device->{name}) - 3) && # 3 is ' = '
               $response =~ /[,\$]/)
            {
                if($response =~ /\$/)
                {
                    $response       =~ s/\$/<BR>\n/g;
                }
                else
                {
                    $response       =~ s/,/<BR>\n/g;
                }
                $mail{Message} .=  "\n<BR>$response<BR>" if($response !~ /^$/);
            }
            else
            {
                $mail{Message} .= " = $response" if($response !~ /^$/);
            }
        }
        $mail{Message} .= "<BR>\n";
    }

    $mail{Message} .=   "</CODE>\n".
                        "<BR>\n".
                        "FGCRun+ device list is stored in $filename\n".
                        "</P>\n".
                        "</BODY>\n".
                        "</HTML>\n";

    # Send mail to supplied addresses

    if(!sendmail(%mail)) # Mail failed to send
    {
        $report->{status_bar}->configure(-text => "ERROR: Failed to send report");
        return;
    }

    $report->{window}->destroy;
}

# Convert unixtimes in responses to local time strings

sub unixtime_to_localtime()
{
    my $selected_devices = selected_devices();

    # Local subroutine to perform unixtime to localtime conversion

    sub convert_to_local($)
    {
        my ($unixtime) = @_;

        return(strftime("%d/%m/%Y %H:%M:%S", localtime($unixtime)));
    }

    # Convert each response

    for my $device (@$selected_devices)
    {
        my $response = get_response($device->{list_index});

        next if(!defined($response));

        # Replace unixtimes with local time strings

        $response =~ s/(\d{10,10})/convert_to_local($1)/eg;

        set_response($device, $response);

        # Replace unixtimes encoded into two floats with local time strings

        if($response !~ /\012/) # Value does not contain a newline (it is a bottom-level property)
        {
            my @values = split(',', $response);

            if(@values >= 2 && is_numeric($values[-2]) && is_numeric($values[-1]))
            {
                my $unixtime    = ($values[-2] * 86400) + $values[-1];
                my $localtime   = strftime("%d/%m/%Y %H:%M:%S", localtime($unixtime));

                $response =~ s/[^,]*,[^,]*$/$localtime/gm if($unixtime > 1000000000);

                print $response . "\n";

                set_response($device, $response);
            }
        }
    }

    # Re-draw responses

    redraw_responses();
}

# Display a message box containing full status

sub display_status()
{
    return if(!defined($main{lists}->{status}->curselection));

    my @fgc_status_list_indices = ref($main{lists}->{status}->curselection) ?
                                    @{$main{lists}->{status}->curselection} :
                                     ($main{lists}->{status}->curselection);
    $main{lists}->{status}->selection('clear', 0, 'end');
    my $device                  = $devices->{$main{devices}->[$fgc_status_list_indices[0]]};

    return if(!defined($device));

    # Check whether there is already a status window open for the device

    if(defined($device->{status_window})) # There is already a status window open for the device
    {
        # Highlight the device's status window

        $device->{status_window}->{window}->deiconify;
        $device->{status_window}->{window}->focusForce;
        $main{lists}->{status}->selection('clear', 0, 'end');
        return;
    }

    # Create status window

    $device->{status_window}->{fields}              = [];
    $device->{status_window}->{values}              = [];
    $device->{status_window}->{window}              = $main{window}->Toplevel(-title => "$device->{name} status");
    $device->{status_window}->{window}->bind('<Destroy>', sub { my ($ev_widget) = @_; delete($device->{status_window}) if(ref($ev_widget) =~ "Tk::Toplevel"); });
    $device->{status_window}->{window}->resizable(0, 0);
    $device->{status_window}->{lists}->{status_fields} = $device->{status_window}->{window}->Listbox(
                                                                                                        -bg                 => 'white',
                                                                                                        -borderwidth        => 1,
                                                                                                        -exportselection    => 0,
                                                                                                        -font               => $font,
                                                                                                        -listvariable       => $device->{status_window}->{fields},
                                                                                                        -selectmode         => 'single',
                                                                                                        -takefocus          => 0,
                                                                                                    )->pack(-side => 'left', -fill => 'both');
    $device->{status_window}->{lists}->{status_fields}->bind('<Button-1>', sub { $device->{status_window}->{lists}->{status_fields}->selection('clear', 0, 'end') });
    $device->{status_window}->{lists}->{status_values} = $device->{status_window}->{window}->Listbox(
                                                                                                        -bg                 => 'white',
                                                                                                        -borderwidth        => 1,
                                                                                                        -exportselection    => 0,
                                                                                                        -font               => $font,
                                                                                                        -listvariable       => $device->{status_window}->{values},
                                                                                                        -selectmode         => 'single',
                                                                                                        -takefocus          => 0,
                                                                                                    )->pack(-side => 'left', -fill => 'both', -expand => 1);
    $device->{status_window}->{lists}->{status_values}->bind('<Button-1>', sub { $device->{status_window}->{lists}->{status_values}->selection('clear', 0, 'end') });

    # Update the status window

    update_status_window($device, 1);
}

# Display a message box containing a full response

sub display_response()
{
    return if(!defined($main{lists}->{responses}->curselection));

    my $index = ref($main{lists}->{responses}->curselection)    ?
                   ($main{lists}->{responses}->curselection)[0] :
                    $main{lists}->{responses}->curselection;
    $main{lists}->{responses}->selection('clear', 0, 'end');

    return if(get_response($index) eq '');

    my $device = $devices->{$main{devices}->[$index]};

    my $response_dialog = $main{window}->Toplevel(-title => "$device->{name} response");

    my $command;
    my $refresh;
    my $status_bar;
    if(defined($device->{previous_command}))
    {
        $command = $device->{previous_command};

        $response_dialog->Label(
                                -bg         => 'white',
                                -font       => $font.' bold',
                                -justify    => 'left',
                                -text       => $device->{previous_command},
                               )->pack(-side => 'top', -fill => 'both');

        $status_bar = $response_dialog->Label(-font => $font.' bold')->pack(-side => 'top', -fill => 'x');

        $refresh = $response_dialog->Button(
                                            -border     => 2,
                                            -relief     => 'groove',
                                            -text       => 'Refresh',
                                            -width      => 7,
                                           )->pack(-side => 'bottom');
    }

    if(get_response($index) =~ /\012/) # Response to parent property
    {
        my @fields;
        my @values;

        my $max_element_length = 0;
        my $max_field_length   = 16;
        my $max_value_length   = 16;

        my $list_height       = 0;
        my $num_value_columns = 0;

        for my $line (split("\012", get_response($index)))
        {
            $line =~ /(.*?):(.*)/;
            my ($field, $value) = ($1, $2);

            # Check for undefined value

            if(!defined($value))
            {
                $main{status_bar}->configure(-text => "ERROR: Value does not conform to formatting convention");
                $response_dialog->destroy();
                return;
            }

            # Check whether to display value on multiple lines

            my @elements    = split(',', $value);
            my $el_length   = @elements ? length($elements[0]) : 0;
            $value          =~ s/,/\012/g if($el_length > 25);

            # Get the number of elements in $value
            # Note that this is not the same as scalar(@elements) as it also counts empty elements

            my $num_elements =()= $value =~ /,/g; # Count the number of commas
            $num_elements++;

            # Set the number of value columns

            if(!$num_value_columns) # The number of value columns has not been set
            {
                $num_value_columns = $num_elements;
            }
            elsif($num_elements != $num_value_columns) # Fields have different numbers of value elements
            {
                $num_value_columns = 1;
            }

            # Compensate for padding between rows

            $list_height += 0.7;

            # Count lines

            my @lines       = split("\012", $value);
            my $line_count  = @lines ? @lines : 1;
            $list_height   += $line_count;

            # Find maximum line length

            for(@lines)
            {
                $max_value_length = length if(length > $max_value_length);
            }

            # Find maximum element length

            for(@elements)
            {
                $max_element_length = length if(length > $max_element_length);
            }

            push(@fields, $field);
            push(@values, $value);

            $max_field_length = length($field) if(length($field) > $max_field_length);
        }

        $list_height    = 40 if($list_height > 40);
        my $list_width  = $max_field_length + ($max_value_length > 80 ? 80 : $max_value_length);

        # Field list

        my $list = $response_dialog->Scrolled(
                                                'HList',
                                                -bg                 => 'white',
                                                -borderwidth        => 1,
                                                -columns            => 1 + $num_value_columns,
                                                -exportselection    => 0,
                                                -font               => $font,
                                                -height             => 1 + $list_height,
                                                -itemtype           => 'text',
                                                -relief             => 'groove',
                                                -scrollbars         => 'osoe',
                                                -selectbackground   => 'gray',
                                                -selectmode         => 'single',
                                                -takefocus          => 0,
                                                -width              => 3 + $list_width,
                                             )->pack(-side => 'left', -fill => 'both', -expand => 1);

        # Set field name width

        $list->columnWidth(0, -char => 1 + $max_field_length);

        for(my $row = 0, my $toggle = 0 ; $row < @fields ; $row++)
        {
            $list->add($row, -style => $toggle ? 'grey' : 'white', -text => $fields[$row]);

            # Handle single and multi-column values

            if($num_value_columns == 1) # Single-column value
            {
                $list->columnWidth(1, -char => 1 + $max_value_length);
                $list->itemCreate($row, 1, -style => $toggle ? 'grey' : 'white', -text => $values[$row]);
            }
            else # Multi-column value
            {
                my @elements = split(',', $values[$row]);
                for(my $i = 0 ; $i < $num_value_columns ; $i++)
                {
                    my $column = 1 + $i;

                    $list->columnWidth($column, -char => 1 + $max_element_length);
                    $list->itemCreate($row, $column, -style => $toggle ? 'grey' : 'white', -text => $elements[$i]);
                }
            }

            # Toggle to allow row colouring

            $toggle = ~$toggle;
        }

        if(defined($refresh))
        {
            $refresh->configure(-command => sub
                                            {
                                                $status_bar->configure(-text => '');

                                                if(!defined($device->{gateway}->{socket}))
                                                {
                                                    $status_bar->configure(-text => "ERROR: Not connected to $device->{gateway}->{name}");
                                                    return;
                                                }

                                                my $command_type = substr($command, 0, 1) eq 'g' ? 'GET' : 'SET';
                                                my $response;
                                                my $property;

                                                if($command_type eq 'GET')
                                                {
                                                    ($property = $command) =~ s/^.\s+(.*)/$1/;

                                                    eval { $response = FGC::Sync::get($device->{gateway}->{socket}, $device->{channel}, $property) };
                                                }
                                                else # ($command_type eq 'SET')
                                                {
                                                    ($property = $command) =~ s/^.\s+(\S+).*/$1/;

                                                    my $value;
                                                    if(($value = $command) !~ s/^.\s+\S+\s+(.*)/$1/)
                                                    {
                                                        $value = '';
                                                    }

                                                    eval { $response = FGC::Sync::set($device->{gateway}->{socket}, $device->{channel}, $property, $value) };
                                                }

                                                # Check whether an error occurred

                                                if($@) # An error occurred sending the command
                                                {
                                                    $status_bar->configure(-text => "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
                                                    return;
                                                }
                                                elsif($response->{error}) # The device returned an error
                                                {
                                                    $status_bar->configure(-text => "ERROR: $response->{value}");
                                                    return;
                                                }

                                                # Remove commas from character arrays
                                                if ( lc($property) eq 'regfgc3.params')
                                                {
                                                    $response->{value} = FGC::RegFgc3ParsConv::blob_to_commands_string($response->{value});
                                                }
                                                else
                                                {
                                                    $response->{value} = remove_commas_from_character_arrays($property, $response->{value}, $device->{class});
                                                }

                                                $list->delete('all');

                                                @fields = ();
                                                @values = ();

                                                for my $line (split("\012", $response->{value}))
                                                {
                                                    $line =~ /(.*?):(.*)/;
                                                    my ($field, $value) = ($1, $2);

                                                    # Check whether to display value on multiple lines

                                                    my @elements    = split(',', $value);
                                                    my $el_length   = @elements ? length($elements[0]) : 0;
                                                    $value          =~ s/,/\012/g if($el_length > 25);

                                                    # Count lines

                                                    my @lines       = split("\012", $value);
                                                    my $line_count  = @lines ? @lines : 1;

                                                    # Find maximum line length

                                                    for(@lines)
                                                    {
                                                        $max_value_length = length if(length > $max_value_length);
                                                    }

                                                    # Find maximum element length

                                                    for(@elements)
                                                    {
                                                        $max_element_length = length if(length > $max_element_length);
                                                    }

                                                    push(@fields, $field);
                                                    push(@values, $value);

                                                    $max_field_length = length($field) if(length($field) > $max_field_length);
                                                }

                                                # Set field name width

                                                $list->columnWidth(0, -char => 1 + $max_field_length);

                                                for(my $row = 0, my $toggle = 0 ; $row < @fields ; $row++)
                                                {
                                                    $list->add($row, -style => $toggle ? 'grey' : 'white', -text => $fields[$row]);

                                                    # Handle single and multi-column values

                                                    if($num_value_columns == 1) # Single-column value
                                                    {
                                                        $list->columnWidth(1, -char => 1 + $max_value_length);
                                                        $list->itemCreate($row, 1, -style => $toggle ? 'grey' : 'white', -text => $values[$row]);
                                                    }
                                                    else # Multi-column value
                                                    {
                                                        my @elements = split(',', $values[$row]);
                                                        for(my $i = 0 ; $i < $num_value_columns ; $i++)
                                                        {
                                                            my $column = 1 + $i;

                                                            $list->columnWidth($column, -char => 1 + $max_element_length);
                                                            $list->itemCreate($row, $column, -style => $toggle ? 'grey' : 'white', -text => $elements[$i]);
                                                        }
                                                    }

                                                    # Toggle to allow row colouring

                                                    $toggle = ~$toggle;
                                                }
                                            });
        }
    }
    else # Response to bottom-level property
    {
        my @values = split(",", get_response($index));

        my $max_value_length = 32;

        for(@values)
        {
            $max_value_length = length($_) if(length($_) > $max_value_length);
        }

        my $list_height = scalar(@values) > 40 ? 40 : scalar(@values);
        my $value_list  = $response_dialog->Scrolled(
                                                        'Listbox',
                                                        -bg                 => 'white',
                                                        -borderwidth        => 1,
                                                        -exportselection    => 0,
                                                        -font               => $font,
                                                        -height             => $list_height,
                                                        -listvariable       => \@values,
                                                        -scrollbars         => 'osoe',
                                                        -selectmode         => 'single',
                                                        -takefocus          => 0,
                                                        -width              => $max_value_length < 80 ? $max_value_length : 80,
                                                    )->pack(-side => 'left', -fill => 'both', -expand => 1);
        $value_list->bind('<Button-1>', sub { $value_list->selection('clear', 0, 'end') });

        if(defined($refresh))
        {
            $refresh->configure(-command => sub
                                            {
                                                $status_bar->configure(-text => '');

                                                if(!defined($device->{gateway}->{socket}))
                                                {
                                                    $status_bar->configure(-text => "ERROR: Not connected to $device->{gateway}->{name}");
                                                    return;
                                                }

                                                my ($cmd_type, $property, $value) = split(/\s+/, $command);
                                                my $response;

                                                if($cmd_type eq 'g')
                                                {
                                                    my $get_options = defined($value) ? " $value" : '';

                                                    eval { $response = FGC::Sync::get($device->{gateway}->{socket}, $device->{channel}, $property.$get_options) };
                                                }
                                                else # ($cmd_type eq 's')
                                                {
                                                    eval { $response = FGC::Sync::set($device->{gateway}->{socket}, $device->{channel}, $property, $value) };
                                                }

                                                # Check whether an error occurred

                                                if($@) # An error occurred sending the command
                                                {
                                                    $status_bar->configure(-text => "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
                                                    return;
                                                }
                                                elsif($response->{error}) # The device returned an error
                                                {
                                                    $status_bar->configure(-text => "ERROR: $response->{value}");
                                                    return;
                                                }

                                                # Remove commas from character array

                                                $response->{value} = remove_commas_from_character_arrays($property, $response->{value}, $device->{class});

                                                @values = $response->{value} =~ '^$' ?
                                                          ('') : split(',', $response->{value});
                                            });
        }
    }

    # Set the minimum window size

    $response_dialog->update;
    $response_dialog->minsize($response_dialog->winfo("width"),
                              $response_dialog->winfo("height"));
}

# Display response in hexadecimal

sub display_response_hex()
{
    # Check that hexadecimal display is supported on current platform

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: Hex display not possible on this platform");
        return;
    }

    my $selected_devices = selected_devices();

    # Do nothing if no devices are selected

    $main{status_bar}->configure(-text => "ERROR: No devices selected"), return if(!@$selected_devices);

    if(@$selected_devices > 10)
    {
        $main{status_bar}->configure(-text => "ERROR: Too many devices selected for hex display (maximum 10)");
        return;
    }

    # Display response for each device in hexadecimal

    for my $device (@$selected_devices)
    {
        my $index = $device->{list_index};

        # Skip device if it has no response

        my $response = get_response($index);

        if(!defined($response) || $response =~ /^$/)
        {
            next;
        }

        # Write response to a temporary file

        my ($tmp_file, $tmp_file_name) = tempfile('fgcrun+XXXXXX', DIR => '/tmp');

        if(!defined($tmp_file))
        {
            $main{status_bar}->configure(-text => "ERROR: Unable to open temporary file");
            return;
        }
        print $tmp_file get_response($index);
        close($tmp_file);

        # Display response in hex

        system("(xterm -T \"Hex response of $device->{name}: $device->{previous_command}\" -e \"hexdump -C $tmp_file_name | less\"; rm $tmp_file_name) &");
    }

    $main{status_bar}->configure(-text => '');
}

# Connect to a set of gateways

sub connect_gateways($;$)
{
    my ($gateways, $populate_device_list)   = @_;
    $populate_device_list                   = 1 if(!defined($populate_device_list));

    $main{status_bar}->configure(-text => '');

    # Disable status data updates

    $main{window}->fileevent($udp_socket, readable => undef);

    # Connect to gateways

    my $fail_count = 0;
    $fail_count   += connect_gateway($_, 0) for(@$gateways);
    populate_device_list(1) if($populate_device_list);

    # Re-enable status data updates

    $main{window}->fileevent($udp_socket, readable => [\&recv_sub_data]);

    $main{status_bar}->configure(-text => "ERROR: $fail_count connection(s) failed") if($fail_count);
    return($fail_count);
}

# Connect to a gateway

sub connect_gateway($;$)
{
    my ($gateway, $populate_device_list) = @_;
    $populate_device_list                = 1 if(!defined($populate_device_list));

    my $reconnecting = defined($gateways_connected{$gateway->{name}}) ? 1 : 0;

    # Disconnect if already connected to gateway

    disconnect_gateway($gateway, $populate_device_list, 1) if($reconnecting);

    # Connect to selected gateway

    $main{status_bar}->configure(-text => "Connecting to gateway $gateway->{name}...");
    $main{window}->update;
    eval { FGC::Async::connect($gateway) };
    if($@)
    {
        $main{status_bar}->configure(-text => "ERROR: Unable to connect to gateway $gateway->{name}");
        return(1);
    }
    $gateways_connected{$gateway->{name}}   = $gateway;
    $gateways_by_ip{$gateway->{ip}}         = $gateway;
    $main{status_bar}->configure(-text => '');

    # Add devices to shown devices

    if(!$reconnecting)
    {
        for my $device (@{$gateway->{channels}})
        {
            $main{shown_devices}->{$device->{name}} = $device if(defined($device->{name}) && !defined($device->{hidden}));
        }
    }

    # Populate device list

    populate_device_list(1) if($populate_device_list);

    {
        local $FGC::Sync::TIMEOUT = 10;

        # Subscribe to data from gateway

        eval { FGC::Sync::set($gateway->{socket}, 0, "CLIENT.UDP.SUB.PERIOD", SUB_PERIOD)};
        if($@)
        {
            disconnect_gateways([$gateway]) if(!$reconnecting);
            $main{status_bar}->configure(-text => "ERROR: Unable to connect to gateway $gateway->{name}: ".(split("\n", $@))[0]);
            return(1);
        }

        eval { FGC::Sync::set($gateway->{socket}, 0, "CLIENT.UDP.SUB.PORT",   $udp_port_number)};
        if($@)
        {
            disconnect_gateways([$gateway]) if(!$reconnecting);
            $main{status_bar}->configure(-text => "ERROR: Unable to connect to gateway $gateway->{name}: ".(split("\n", $@))[0]);
            return(1);
        }
    }

    # Clear FGC status list

    for(my $i = 0 ; $i < @{$main{devices}} ; $i++)
    {
        $main{status_list_content}->[$i] = '';
    }

    # Activate command entry and set focus

    $main{command_entry}->configure(-state => 'normal', -highlightthickness => 1);
    $main{command_entry}->focusForce;

    return(0);
}

# Disconnect from a set of gateways

sub disconnect_gateways($;$)
{
    my ($gateways, $populate_device_list)   = @_;
    $populate_device_list                   = 1 if(!defined($populate_device_list));

    $main{status_bar}->configure(-text => '');

    # Disable status data updates

    $main{window}->fileevent($udp_socket, readable => undef);

    # Disconnect from gateways

    disconnect_gateway($_, 0) for(@$gateways);
    populate_device_list(1) if($populate_device_list);

    # Re-enable status data updates

    $main{window}->fileevent($udp_socket, readable => [\&recv_sub_data]);
}

# Disconnect from a gateway

sub disconnect_gateway($;$$)
{
    my ($gateway, $populate_device_list, $reconnecting) = @_;
    $populate_device_list   = 1 if(!defined($populate_device_list));
    $reconnecting           = 0 if(!defined($reconnecting));

    return if(!defined($gateways_connected{$gateway->{name}}));

    # Disconnect from selected gateway

    FGC::Async::disconnect($gateway);
    delete($gateways_connected{$gateway->{name}}) if(!$reconnecting);

    for(my $i = 0 ; $i < @{$main{devices}} ; $i++)
    {
        $main{status_list_content}->[$i] = '';
    }

    # Clear list indices for devices on disconnected gateway

    for my $device (@{$gateway->{channels}})
    {
        next if(!defined($device->{name}));

        if(!$reconnecting)
        {
            delete($device->{list_index});
            delete($device->{hidden});
            delete($main{shown_devices}->{$device->{name}});
        }

        # Disable status window lists if open

        if(defined($device->{status_window}))
        {
            $device->{status_window}->{lists}->{status_fields}->configure(-state => 'disabled');
            $device->{status_window}->{lists}->{status_values}->configure(-state => 'disabled');
        }
    }

    # Populate device list

    populate_device_list(1) if($populate_device_list);

    # Activate command entry and set focus

    if(scalar(keys(%gateways_connected))) # There are remaining connections
    {
        $main{command_entry}->focusForce;
    }
    else # No connections remaining
    {
        $main{command_entry}->configure(-state => 'disabled', -highlightthickness => 0);

        # Empty shown device stack

        $main{shown_device_stack} = [] if(!$reconnecting);
    }

    $main{status_bar}->configure(-text => '');
}

# Receive subscribed data from UDP socket and update FGC status list

sub recv_sub_data()
{
    # Get visible status list range

    my ($view_top, $view_bottom) = $main{lists}->{devices}->yview;
    $view_top                   *= @{$main{devices}};
    $view_bottom                *= @{$main{devices}};
    $view_bottom++;

    my ($sockaddr_in, $sequence, $time_sec, $time_usec, $buffer) = FGC::Sub::read($udp_socket, 0);

    # Get gateway from source IP address

    my $gateway = $gateways_by_ip{inet_ntoa((sockaddr_in($sockaddr_in))[1])};
    return if(!defined($gateway));

    # Store status update time

    $gateway->{status_time_sec}     = $time_sec;
    $gateway->{status_time_usec}    = $time_usec;

    # Get a count of visible devices on gateway

    my $num_visible_devs = grep
                           {
                                defined($_->{status_window}) ||
                                (defined($_->{list_index}) && $_->{list_index} <= $view_bottom && $_->{list_index} >= $view_top)
                           } @{$gateway->{channels}};

    # Decode status data

    my $status = [];
    FGC::Sub::decode($buffer, $status, $num_visible_devs ? 0 : 1);

    my $ref_remaining_used = 0;

    for(my $channel = 0 ; $channel < @$status ; $channel++)
    {
        my $device = $gateway->{channels}->[$channel];

        $device->{status} = $status->[$channel];

        # Check whether device is offline

        $device->{online} = $device->{status}->{DATA_STATUS}->{DATA_VALID} ? 1 : 0;

        # Update device status window

        update_status_window($device);

        # Check whether device is within displayed range

        next if(!defined($device->{list_index}));

        if($device->{list_index} <= $view_bottom && $device->{list_index} >= $view_top)
        {
            if(!$device->{online}) # Device is offline
            {
                if($device->{list_index} <= $view_bottom && $device->{list_index} >= $view_top)
                {
                    # Check whether the power to the device was cut

                    if(defined($device->{status}->{ST_LATCHED}->{VDC_FAIL}) && $device->{status}->{ST_LATCHED}->{VDC_FAIL})
                    {
                        set_status_message($device, "Power to the device was cut");
                        $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'dark gray', -fg => 'white');
                    }
                    else
                    {
                        set_status_message($device, "Device is offline");
                        $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'black', -fg => 'white');
                    }
                }

                next;
            }

            # Update REF.REMAINING value if displayed

            if(defined($device->{ref_remaining}))
            {
                $ref_remaining_used = 1;

                if($device->{status}->{STATE_PC} eq 'RUNNING'       ||
                   $device->{status}->{STATE_PC} eq 'SLOW_ABORT'    ||
                   $device->{status}->{STATE_PC} eq 'TO_STANDBY')
                {
                    my $response = get_response($device->{list_index});

                    if($response ne '' && $response > 0)
                    {
                        $device->{ref_remaining} -= SUB_PERIOD / FGC_FIELDBUS_CYCLE_FREQ;

                        if($device->{ref_remaining} < 0)
                        {
                            delete($device->{ref_remaining});

                            set_response($device, '0.0000');
                        }
                        else
                        {
                            set_response($device, sprintf("%.4f", $device->{ref_remaining}));
                        }
                    }
                    else
                    {
                        delete($device->{ref_remaining});
                    }
                }
                elsif($device->{status}->{STATE_PC} ne 'ARMED')
                {
                    set_response($device, '0.0000');

                    delete($device->{ref_remaining});
                }
            }

            # Construct status line for device

            my $status_line = \$main{status_list_content}->[$device->{list_index}];
            $$status_line   = '';

            # STATE_PLL

            if(defined($device->{status}->{STATE_PLL}) &&
               defined($state_pll_short{$device->{status}->{STATE_PLL}}))
            {
                $$status_line .= sprintf("%2s.", $state_pll_short{$device->{status}->{STATE_PLL}});
            }
            else
            {
                $$status_line .= '   ';
            }

            # STATE_OP

            if(defined($device->{status}->{STATE_OP}) &&
               defined($state_op_short{$device->{status}->{STATE_OP}}))
            {
                $$status_line .= sprintf("%2s.", $state_op_short{$device->{status}->{STATE_OP}});
            }
            else
            {
                $$status_line .= '   ';
            }

            # STATE_VS

            if(defined($device->{status}->{STATE_VS}) &&
               defined($state_vs_short{$device->{status}->{STATE_VS}}))
            {
                $$status_line .= sprintf("%2s.", $state_vs_short{$device->{status}->{STATE_VS}});
            }
            else
            {
                $$status_line .= '   ';
            }

            # STATE_PC

            if(defined($device->{status}->{STATE_PC}) &&
               defined($state_pc_short{$device->{status}->{STATE_PC}}))
            {
                $$status_line .= sprintf("%2s ", $state_pc_short{$device->{status}->{STATE_PC}});
            }
            else
            {
                $$status_line .= '   ';
            }

            # I_REF

            if(defined($device->{status}->{I_REF}))
            {
                $$status_line .= sprintf("%9.2f", $device->{status}->{I_REF});
            }
            else
            {
                $$status_line .= '         ';
            }
            $$status_line .= ' ';

            # I_MEAS

            if(defined($device->{status}->{I_MEAS}))
            {
                $$status_line .= sprintf("%9.2f", $device->{status}->{I_MEAS});
            }
            else
            {
                $$status_line .= '         ';
            }
            $$status_line .= ' ';

            # V_REF

            if(defined($device->{status}->{V_REF}))
            {
                $$status_line .= sprintf("%8.2f", $device->{status}->{V_REF});
            }
            else
            {
                $$status_line .= '        ';
            }
            $$status_line .= ' ';

            # I_MEAS

            if(defined($device->{status}->{V_MEAS}))
            {
                $$status_line .= sprintf("%8.2f", $device->{status}->{V_MEAS});
            }
            else
            {
                $$status_line .= '        ';
            }

            # Check whether status line contains no data

            if($$status_line =~ /^\s*$/) # Status line has no data
            {
                my $class_title = eval("FGC::Class::$device->{status}->{CLASS_ID}::Auto::title") ||
                                       "unknown class $device->{status}->{CLASS_ID}";

                set_status_message($device, $class_title);
            }

            # Set status colour

            if(defined($device->{status}->{STATE_OP}) &&
               $device->{status}->{STATE_OP} eq 'BOOT') # FGC in boot program
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'white', -fg => 'black');
            }
            elsif(defined($device->{status}->{STATE_OP}) &&
                  $device->{status}->{STATE_OP} eq 'PROGRAMMING')
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'purple', -fg => 'white');
                set_status_message($device, "Device is reprogramming");
            }
            elsif(defined($device->{status}->{ST_UNLATCHED}) &&
                  defined($device->{status}->{ST_UNLATCHED}->{SYNC_PLEASE}) &&
                          $device->{status}->{ST_UNLATCHED}->{SYNC_PLEASE})
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'chocolate', -fg => 'white');
            }
            elsif(defined($device->{status}->{ST_UNLATCHED})
                && defined($device->{status}->{ST_UNLATCHED}->{SYNC_PROG_MGR})
                && $device->{status}->{ST_UNLATCHED}->{SYNC_PROG_MGR})
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'pink', -fg => 'black');
            }
            elsif(defined($device->{status}->{ST_FAULTS})       &&
                  ref($device->{status}->{ST_FAULTS}) eq 'HASH' &&
                  $device->{status}->{ST_FAULTS}->{mask}) # Device has a fault
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'red', -fg => 'white');
            }
            elsif(defined($device->{status}->{ST_WARNINGS})       &&
                  ref($device->{status}->{ST_WARNINGS}) eq 'HASH' &&
                  $device->{status}->{ST_WARNINGS}->{mask}) # Device has a warning
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'yellow', -fg => 'black');
            }
            else # Device is okay
            {
                $main{lists}->{status}->itemconfigure($device->{list_index}, -bg => 'green', -fg => 'black');
            }
        }
    }

    # Re-draw responses

    redraw_responses() if($ref_remaining_used);

    # Restore list position

    $main{lists}->{status}->yviewMoveto(($main{lists}->{devices}->yview)[0]);
}

# Update status window for device if open

sub update_status_window($;$)
{
    my ($device, $force) = @_;
    $force               = 0 if(!defined($force));

    return if(!defined($device->{status_window}));

    if(($force && $device->{status}->{DATA_STATUS}->{DEVICE_IN_DB}) || (defined($device->{online}) && $device->{online}))
    {
        # Set window title including data timestamp

        if($device->{status}->{DATA_STATUS}->{DATA_VALID})
        {
            $device->{status_window}->{window}->configure(-title => "$device->{name} status - ".(localtime($device->{gateway}->{status_time_sec})));
        }

        # Set status fields

        @{$device->{status_window}->{fields}} = sort(keys(%{$device->{status}}));

        # Enable status lists

        $device->{status_window}->{lists}->{status_fields}->configure(-state => 'normal');
        $device->{status_window}->{lists}->{status_values}->configure(-state => 'normal');

        my $max_field_length    = 16;
        my $max_value_length    = 45;

        my $st_faults_index     = -1;
        my $st_warnings_index   = -1;
        my @values;

        my $i = 0;
        for my $key (@{$device->{status_window}->{fields}})
        {
            my $value;

            $st_faults_index    = $i if($key eq 'ST_FAULTS');
            $st_warnings_index  = $i if($key eq 'ST_WARNINGS');

            # Check whether field is a reference to a hash (ie a symlist)

            if(ref($device->{status}->{$key}) eq 'HASH')
            {
                for my $symbol (sort(keys(%{$device->{status}->{$key}})))
                {
                    next if($symbol eq "mask");

                    $value .= "$symbol " if($device->{status}->{$key}->{$symbol});
                }
            }
            else # Field is not a reference to a hash
            {
                if($device->{status}->{$key} =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)([Ee]([+-]?\d+))?$/) # Value is a float
                {
                    $value = sprintf("%.5e", $device->{status}->{$key});
                }
                else
                {
                    $value = $device->{status}->{$key};
                }
            }

            # Check maximum lengths

            $max_field_length = length($key)    if(length($key) > $max_field_length);
            $max_value_length = length($value)  if(defined($value) && length($value) > $max_value_length);

            push(@values, $value);
            $i++;
        }
        @{$device->{status_window}->{values}} = @values;

        $device->{status_window}->{lists}->{status_fields}->configure(
                                                                        -height => scalar(@{$device->{status_window}->{fields}}),
                                                                        -width  => $max_field_length,
                                                                     ) if($max_field_length                             > $device->{status_window}->{lists}->{status_fields}->cget(-width) ||
                                                                          scalar(@{$device->{status_window}->{fields}}) > $device->{status_window}->{lists}->{status_fields}->cget(-height));
        $device->{status_window}->{lists}->{status_values}->configure(
                                                                        -height => scalar(@{$device->{status_window}->{values}}),
                                                                        -width  => $max_value_length,
                                                                     ) if($max_value_length                             > $device->{status_window}->{lists}->{status_values}->cget(-width) ||
                                                                          scalar(@{$device->{status_window}->{values}}) > $device->{status_window}->{lists}->{status_values}->cget(-height));

        # Set colours for ST_FAULTS and ST_WARNINGS

        if($st_faults_index != -1 && ref($device->{status}->{ST_FAULTS}) eq 'HASH' && $device->{status}->{ST_FAULTS}->{mask})
        {
            $device->{status_window}->{lists}->{status_fields}->itemconfigure($st_faults_index, -bg => 'red', -fg => 'white');
            $device->{status_window}->{lists}->{status_values}->itemconfigure($st_faults_index, -bg => 'red', -fg => 'white');
        }

        if($st_warnings_index != -1 && ref($device->{status}->{ST_WARNINGS}) eq 'HASH' && $device->{status}->{ST_WARNINGS}->{mask})
        {
            $device->{status_window}->{lists}->{status_fields}->itemconfigure($st_warnings_index, -bg => 'yellow', -fg => 'black');
            $device->{status_window}->{lists}->{status_values}->itemconfigure($st_warnings_index, -bg => 'yellow', -fg => 'black');
        }
    }

    # Disable lists if device offline

    if(!defined($device->{online}) || !$device->{online}) # Device is offline
    {
        $device->{status_window}->{lists}->{status_fields}->configure(-state => 'disabled');
        $device->{status_window}->{lists}->{status_values}->configure(-state => 'disabled');
        return;
    }
}

# Read responses from gateways

sub read_responses(;$)
{
    my ($error_flag) = @_;

    # Read responses

    my $entry_state = $main{command_entry}->cget('-state');
    $main{command_entry}->configure(-state => 'disabled',   -highlightthickness => 0);
    my $commands = FGC::Async::read([sub { $main{window}->update }]);
    $main{command_entry}->configure(-state => $entry_state, -highlightthickness => 1);

    my $command_count   = scalar(keys(%$commands));
    my $error_count     = 0;

    # Set responses

    for my $command (values(%$commands))
    {
        # Remove commas from character arrays
        if ( lc($command->{property}) eq 'regfgc3.params')
        {
            $command->{response}->{value} = FGC::RegFgc3ParsConv::blob_to_commands_string($command->{response}->{value});
        }
        else
        {
            $command->{response}->{value} = remove_commas_from_character_arrays($command->{property}, $command->{response}->{value}, $command->{device}->{class});
        }

        # Set response in response list
        if(!defined($command->{device}->{hidden}))
        {
            set_response($command->{device}, ($command->{response}->{error} ? "ERROR: " : defined($command->{values}) ? "OKAY" : '').$command->{response}->{value}, $command);
        }

        if($command->{response}->{error})
        {
            $error_count++;
        }
        elsif($command->{property} =~ /REF\.REMAINING/i && !defined($command->{values}))
        {
            $command->{device}->{ref_remaining} = $command->{response}->{value};
        }
    }

    if($error_count)
    {
        $main{status_bar}->configure(-text => "$error_count error".($error_count > 1 ? 's' : '')." / $command_count response".($command_count > 1 ? 's' : ''));
    }
    elsif(!defined($error_flag) || !$error_flag) # No errors occurred
    {
        $main{status_bar}->configure(-text => '');
    }

    # Re-draw responses

    redraw_responses();
    $main{window}->update;

    return($commands);
}

# EPCCCS-7346: Detect whether a given command is addressed to a subdevice

sub is_subdevice_command($)
{
    my ($command) = @_;

    # command must be set get or db
    if($command !~ /^[sg] /i)
    {
        return 0;
    }

    # extract and validate property part of command
    my $property;
    if(($property = $command) !~ s/^.\s+(.+)/$1/)
    {
        return 0;
    }

    # check if property is of subdevice syntax 'subdev:prop[ val ]'
    # where subdev can be subdevicename or subdeviceaddress
    # regex: ensure exactly one colon, surrounded by non colon characters and optionally followed by value part
    return $property =~ /^[^:\s]+:[^:\s]+(\s+[^:\s]+)?\s*$/;
}

# EPCCCS-7346: Identify subdevice of a given device which matches the given name or address

sub identify_subdevice_by_name_or_address($$)
{
    my ($parentdevice, $name_or_address) = @_;

    my $is_address = $name_or_address =~ /^\d+$/;

    if($is_address)
    {
        for my $sub (@{$parentdevice->{sub_devices}})
        {
            if($name_or_address == $sub->{channel})
            {
                return $sub;
            }
        }
    }
    else
    {
        for my $sub (@{$parentdevice->{sub_devices}})
        {
            if($name_or_address eq $sub->{name})
            {
                return $sub;
            }
        }

        if($name_or_address eq $parentdevice->{name})
        {
            return $parentdevice;
        }
    }

    return undef;
}

# Process a command

sub process_command()
{
    my $command = $main{command_entry}->get;

    if($command =~ /^[sg] /i)
    {
        send_command();
        return;
    }
    elsif($command =~ /^d /i)
    {
        get_properties_from_db(0);
        return;
    }
    elsif($command =~ /^i /i)
    {
        get_parameters_from_lsa();
        return;
    }

    $main{status_bar}->configure(-text => '');
    $main{history_idx} = 0;

    my $expression = $main{command_entry}->get;

    my @selected_indices;
    my $error = process_expression($expression, [get_responses_array()], \@selected_indices, \@history);

    # Display error if defined

    if(defined($error))
    {
        $main{status_bar}->configure(-text => "ERROR: $error");
        return;
    }

    # Clear current selection

    $main{lists}->{devices}->selection('clear', 0, 'end');

    # Apply new selection

    for(@selected_indices)
    {
        $main{lists}->{devices}->selection('set', $_, $_);
    }

    # Show first match

    $main{lists}->{devices}->see($selected_indices[0]) if(@selected_indices);

    # Display number of matches

    $main{selected_devices}->configure(-text => scalar(@selected_indices));
}

# Send a command to selected devices

sub send_command()
{
    my $command = $main{command_entry}->get;

    # Reset the history index

    $main{history_idx} = 0;

    my $selected_devices = selected_devices();

    # Do nothing if no devices are selected

    if(!@$selected_devices) # No devices are selected;
    {
        $main{status_bar}->configure(-text => "ERROR: No devices selected");
        return;
    }

    # Clear responses

    for my $i (map { $_->{list_index} } @$selected_devices)
    {
        my $response = get_response($i);

        # Store first line of previous response if defined

        if(defined($response) && $response ne '')
        {
            ($devices->{$main{devices}->[$i]}->{previous_response}) = split("\n", $response);
        }
        else
        {
            ($devices->{$main{devices}->[$i]}->{previous_response}) = ("");
        }

        clear_response($i);
    }
    redraw_responses();

    # Check that command starts with 'g' or 's'

    if($command !~ /^([GgSs]) /)
    {
        $main{status_bar}->configure(-text => "ERROR: Command must start with \"g \" or \"s \"");
        return;
    }
    $main{status_bar}->configure(-text => '');

    my $command_type = $1 eq 'g' || $1 eq 'G' ? 'GET' : 'SET';

    # extract property part from command

    my $property;
    if(($property = $command) !~ s/^.\s+(\S+).*/$1/)
    {
        $main{status_bar}->configure(-text => "ERROR: Badly formed property");
        return;
    }

    # extract subdevice info from command

    my $subdevice_id = undef;
    if(is_subdevice_command($command))
    {
        ($subdevice_id, $property) = split(":", $property);
    }

    if($command_type eq 'GET')
    {
        # Add command to history

        push(@history, $command) if(!defined($history[-1]) || $command ne $history[-1]);

        # Remove old items from history

        shift(@history) if(@history > 50);

        # Send command to each device

        $main{status_bar}->configure(-text => "Sending command...");
        $main{window}->update;
        for my $device (@$selected_devices)
        {
            # Extract get options from command

            my $get_options;
            if(($get_options = $command) !~ s/^.\s+\S+\s+(.*)/ $1/)
            {
                $get_options = '';
            }

            # Identify subdevice

            my $target_device = $device;
            if(defined($subdevice_id))
            {
                $target_device = identify_subdevice_by_name_or_address($device, $subdevice_id);

                if(!defined($target_device))
                {
                    set_response($device, "ERROR: Subdevice '$subdevice_id' not found for '$device->{name}'");
                    next;
                }
            }

            # Substitute $rsp for current response for device in property

            (my $device_property = $property) =~ s/\$rsp/$device->{previous_response}/gi;

            delete($device->{previous_response});
            delete($device->{ref_remaining}) if(defined($device->{ref_remaining}));

            eval { FGC::Async::get($target_device, $device_property.$get_options) };

            # Check whether an error occurred

            if($@) # An error occurred
            {
                set_response($device, "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
                next;
            }
        }
    }
    else # ($command_type eq 'SET')
    {
        # extract value part from command

        my $value;
        if(($value = $command) !~ s/^.\s+\S+\s+(.*)/$1/)
        {
            $value = '';
        }

        # Add command to history

        push(@history, $command) if(!defined($history[-1]) || $command ne $history[-1]);

        # Substitute $time for unixtime + 10 in value

        $value =~ s/\$time/time + 10/gie;

        # Send command to each device

        $main{status_bar}->configure(-text => "Sending command...");
        $main{window}->update;
        for my $device (@$selected_devices)
        {
            # identify subdevice

            my $target_device = $device;
            if(defined($subdevice_id))
            {
                $target_device = identify_subdevice_by_name_or_address($device, $subdevice_id);

                if(!defined($target_device))
                {
                    set_response($device, "ERROR: Subdevice '$subdevice_id' not found for '$device->{name}'");
                    next;
                }
            }

            # Substitute $rsp for current response for device in property and value

            (my $device_property = $property)   =~ s/\$rsp/$device->{previous_response}/gi;
            (my $device_value    = $value)      =~ s/\$rsp/$device->{previous_response}/gi;
            delete($device->{previous_response});

            eval { FGC::Async::set($target_device, $device_property, $device_value)};

            # Check whether an error occurred

            if($@) # An error occurred
            {
                set_response($device, "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
                next;
            }
        }
    }

    read_responses();
}

# Send a boot command to selected devices

sub send_boot_command()
{
    my $command = $main{command_entry}->get;

    # Reset the history index

    $main{history_idx} = 0;

    my $selected_devices = selected_devices();

    # Do nothing if no devices are selected

    if(!@$selected_devices) # No devices are selected;
    {
        $main{status_bar}->configure(-text => "ERROR: No devices selected");
        return;
    }
    elsif(@$selected_devices > FGC_MAX_FGCS_PER_GW)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will send a boot command to ".scalar(@$selected_devices)." devices.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    # Add the command to the history

    push(@history, $command) if(!defined($history[-1]) || $command ne $history[-1]);

    # Clear responses

    for my $device (@$selected_devices)
    {
        set_response($device, '');
    }
    redraw_responses();

    # Send the command to each device

    $main{status_bar}->configure(-text => "Sending command...");
    $main{window}->update;

    for my $device (@$selected_devices)
    {
        # Check whether the device is a gateway

        if($device->{channel} == 0)
        {
            set_response($device, "ERROR: Cannot send a boot command to a gateway");
            redraw_responses();
            $main{window}->update;
            next;
        }

        my $socket = $device->{gateway}->{socket};

        # Start a remote terminal

        my $response = FGC::Sync::rtermstart($socket, $device->{channel});
        if($response->{error})
        {
            set_response($device, "ERROR: $response->{value}");
            redraw_responses();
            $main{window}->update;
            next;
        }

        # Send the command

        FGC::Boot::command_no_wait($socket, $command);

        # Close the remote terminal

        FGC::Sync::rtermstop($socket);

        set_response($device, "DONE");
        redraw_responses();
        $main{window}->update;
    }
    $main{status_bar}->configure(-text => '');
}

# Run FGC main program

sub fgc_run_main()
{
    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    $main{status_bar}->configure(-text => "Running main program...");
    $main{window}->update;

    # Send commands to run main program

    for my $device (@$selected_devices)
    {
        my $socket = $device->{gateway}->{socket};

        FGC::Sync::rtermstart($socket, $device->{channel});
        FGC::Boot::command_no_wait($socket, '0');
        FGC::Sync::rtermstop($socket);

        set_response($device, "DONE");
        redraw_responses();
        $main{window}->update;
    }
    $main{status_bar}->configure(-text => '');
}

# Set a gateway property

sub gw_set($$;$$)
{
    return if(!scalar(keys(%gateways_connected)));

    my ($property, $value, $confirm_msg, $binary) = @_;
    $binary = 0 if(!defined($binary));

    # Ask for confirmation if necessary

    return if(defined($confirm_msg) && $main{window}->messageBox(
                                                                    -font       => $font,
                                                                    -icon       => 'question',
                                                                    -message    => $confirm_msg,
                                                                    -title      => 'Confirmation',
                                                                    -type       => 'okcancel',
                                                                ) eq 'Cancel');

    my %selected_gateways = map {
                                    $_->{gateway}->{name} => $_->{gateway};
                                } @{selected_devices()};

    for my $gateway (values(%selected_gateways))
    {
        # Send command

        eval { FGC::Async::set($gateway->{channels}->[0], $property, $value, $binary) };

        # Check whether an error occurred

        if($@) # An error occurred sending the command
        {
            $main{status_bar}->configure(-text => "ERROR: Failure sending command to gateway $gateway->{name}: ".(split("\n", $@))[0]);
            read_responses(1);
            return;
        }
    }
    read_responses();
    $main{status_bar}->configure(-text => '');
}

# Clear codes from gateways

sub clear_codes()
{
    return if(!scalar(keys(%gateways_connected)));

    for my $gateway (values(%gateways_connected))
    {
        for(my $i = 1 ; $i < FGC_CODE_NUM_SLOTS ; $i++)
        {
            # Send command to clear code

            eval { FGC::Async::set($gateway->{channels}->[0], "CODE.SLOT$i", '') };

            # Check whether an error occurred

            if($@) # An error occurred sending the command
            {
                $main{status_bar}->configure(-text => "ERROR: Failure clearing code on $gateway->{channels}->[0]->{name}: ".(split("\n", $@))[0]);
                read_responses(1);
                return;
            }
        }
    }
    read_responses();
    $main{status_bar}->configure(-text => '');
}

# Send a code to a gateway

sub send_code($)
{
    my ($slot) = @_;

    return if(!scalar(keys(%gateways_connected)) || !defined((my $filename = $main{window}->getOpenFile(
                                                                                                        -filetypes  => [["All files", "*"]],
                                                                                                        -initialdir => '.',
                                                                                                        -title      => "Select code file for slot $slot",
                                                                                                       ))));
    chdir(dirname($filename));

    # Open code file

    if(!open(CODE_FILE, "<:raw", $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }

    $main{status_bar}->configure(-text => "Sending code to slot $slot");

    # Read contents of input file into $data

    my $buf;
    my $data;
    while(read(CODE_FILE, $buf, 16384))
    {
        $data .= $buf;
    }
    close(CODE_FILE);

    # Extract bytes from data

    my @bytes   = split("", $data);
    $_          = unpack("C", $_) for @bytes;

    # Construct commands

    my $i = 0;
    while($i < @bytes)
    {
        my $start_i = $i;
        my $value   = '';

        my $first = 1;
        for( ; $i < @bytes && $i < ($start_i + 30000) ; $i++)
        {
            $value .= "," if(!$first);
            $value .= sprintf("0x%02X", $bytes[$i]);
            $first = 0;
        }

        # Update command entry

        $main{command_entry}->delete(0, 'end');
        $main{command_entry}->insert(0, "s CODE.SLOT$slot\[$start_i,] $value");
        $main{window}->update;

        for my $gateway (values(%gateways_connected))
        {
            # Send command

            eval { FGC::Async::set($gateway->{channels}->[0], "CODE.SLOT$slot\[$start_i,]", $value) };

            # Check whether an error occurred

            if($@) # An error occurred sending the command
            {
                $main{status_bar}->configure(-text => "ERROR: Failure sending code command to $gateway->{channels}->[0]->{name}: ".(split("\n", $@))[0]);
                read_responses(1);
                close(CODE_FILE);
                return;
            }
        }

        my $commands = read_responses();

        # Check whether an error occurred

        for my $response (values(%$commands))
        {
            if($response->{response}->{error})
            {
                $main{status_bar}->configure(-text => "ERROR: Error setting code on $response->{device}->{name}");
                close(CODE_FILE);
                return;
            }
        }
    }
    $main{command_entry}->delete(0, 'end');

    close(CODE_FILE);
    $main{status_bar}->configure(-text => "Code sent to slot $slot successfully");
}

# Get an event log from selected power converters

sub get_event_log()
{
    $main{status_bar}->configure(-text => '');

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    $main{status_bar}->configure(-text => "Getting event log...");
    $main{window}->update;

    my $error_flag = 0;

    for my $device (@$selected_devices)
    {
        # Check that the device is not a gateway

        if($device->{channel} == 0)
        {
            $main{status_bar}->configure(-text => "ERROR: Cannot get an event log from a gateway");
            return;
        }

        # Send command to read event log

        my $response;
        eval { $response = FGC::Sync::get($device->{gateway}->{socket}, $device->{channel}, "LOG.EVT") };

        # Check whether an error occurred

        if($@) # An error occurred sending the command
        {
            $main{status_bar}->configure(-text => "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
            return;
        }
        elsif($response->{error}) # The device returned an error
        {
            $main{status_bar}->configure(-text => "ERROR: Error getting event log from $device->{name}");
            set_response($device, "ERROR: $response->{value}");
            redraw_responses();
            $error_flag = 1;
            next;
        }

        # Extract log entries from response

        my @entries;
        for my $element (split(/,/, $response->{value}))
        {
            my %entry;

            ($entry{timestamp_raw}, $entry{property}, $entry{value}, $entry{action}) = split(/:/, $element);
            $entry{property}    =~ s/'/,/g;
            $entry{value}       =~ s/'/,/g;

            # Replace error code with error message in action

            if($entry{action} =~ /\.(\d+)$/)
            {
                my $error_code = $1;

                if($error_code == 0) # No error occurred
                {
                    $entry{action} =~ s/\.$error_code/\.okay/;
                }
                else # An error occurred
                {
                    my $error_message = $fgc_errors[$error_code];
                    if(defined($fgc_errors[$error_code]))
                    {
                        $entry{action} =~ s/\.$error_code/\.$error_message/;
                    }
                }
            }

            $entry{timestamp_sec}   = int($entry{timestamp_raw} / 1000000);
            $entry{timestamp_usec}  = $entry{timestamp_raw} - ($entry{timestamp_sec} * 1000000);
            $entry{timestamp}       = sprintf("%s.%06d", strftime("%d/%m/%Y %H:%M:%S", localtime($entry{timestamp_sec})), $entry{timestamp_usec});

            push(@entries, \%entry);
        }

        # Sort log entries and create individual arrays to map into lists

        my @actions;
        my @properties;
        my @timestamps;
        my @values;

        my @lines;
        for my $entry (sort { $a->{timestamp_raw} <=> $b->{timestamp_raw} } @entries)
        {
            push(@actions,      $entry->{action});
            push(@timestamps,   $entry->{timestamp});
            push(@properties,   $entry->{property});
            push(@values,       $entry->{value});

            my %line_info = (
                                index   => @actions - 1,
                                string  => "$entry->{timestamp} $entry->{property} $entry->{value} $entry->{action}",
                            );
            push(@lines, \%line_info);
        }

        # Create event log dialog box

        my %event_log;
        $event_log{window} = $main{window}->Toplevel(-title => "$device->{name} event log "."(".scalar(@timestamps)." entries)");

        $event_log{save} = $event_log{window}->Button(
                                                        -border     => 2,
                                                        -command    => sub { save_event_log($event_log{window}, $device, \@timestamps, \@properties, \@values, \@actions) },
                                                        -relief     => 'groove',
                                                        -text       => "Save",
                                                        -width      => 4,
                                                     )->pack(-side => 'bottom');
        $event_log{search_frame} = $event_log{window}->Frame->pack(-side => 'bottom', -fill => 'x');
        $event_log{search_frame}->Label(-text => "Search: ")->pack(-side => 'left');

        $event_log{match_count_frame}  = $event_log{search_frame}->Frame->pack(-side => 'right');
        $event_log{match_count_frame}->Label(-font => $font.' bold', -text => '')->pack(-side => 'left', -fill => 'both', -expand => 1);
        $event_log{matched_entries}    = $event_log{match_count_frame}->Label(-width => 4, -font => $font.' bold', -text => "0")->pack(-side => 'left');
        $event_log{match_count_frame}->Label(-width => 1, -font => $font.' bold', -text => "/")->pack(-side => 'left');
        $event_log{total_entries}      = $event_log{match_count_frame}->Label(-width => 4, -font => $font.' bold', -text => scalar(@timestamps))->pack(-side => 'left');
        $event_log{match_count_frame}->Label(-font => $font.' bold', -text => '')->pack(-side => 'left', -fill => 'both', -expand => 1);

        $event_log{search} = $event_log{search_frame}->Entry(
                                                                -bg     => 'white',
                                                                -font   => $font,
                                                                -width  => 20,
                                                            )->pack(-side => 'right', -fill => 'x', -expand => 1);

        $event_log{list} = $event_log{window}->Scrolled(
                                                        'HList',
                                                        -bg                 => 'white',
                                                        -borderwidth        => 1,
                                                        -columns            => 4,
                                                        -exportselection    => 0,
                                                        -font               => $font,
                                                        -height             => 40,
                                                        -itemtype           => 'text',
                                                        -relief             => 'groove',
                                                        -scrollbars         => 'oe',
                                                        -selectbackground   => 'gray',
                                                        -selectmode         => 'single',
                                                        -takefocus          => 0,
                                                        -width              => 113,
                                                       )->pack(-side => 'left', -fill => 'both', -expand => 1);

        # Clear search

        $event_log{list}->bind('<Button-1>', sub
                                             {
                                                delete($event_log{last_search});
                                                delete($event_log{matched_indices});
                                                delete($event_log{shown_match});
                                             });
        $event_log{search}->bind('<Key-Escape>', sub
                                                 {
                                                    $event_log{list}->selectionClear;
                                                    $event_log{matched_entries}->configure(-text => "0");
                                                    $event_log{search}->delete(0, 'end');
                                                    delete($event_log{last_search});
                                                    delete($event_log{matched_indices});
                                                    delete($event_log{shown_match});
                                                 });

        # Search for entries matching regular expression

        $event_log{search}->bind('<Key-Return>', sub
                                                 {
                                                    my $regex = $event_log{search}->get;

                                                    if($regex !~ /^$/)
                                                    {
                                                        # If expression has not changed display previous match

                                                        if(defined($event_log{last_search}) &&
                                                           $regex eq $event_log{last_search})
                                                        {
                                                            return if(!defined($event_log{shown_match}));

                                                            --$event_log{shown_match};
                                                            if(abs($event_log{shown_match}) > @{$event_log{matched_indices}})
                                                            {
                                                                $event_log{shown_match} = -1;
                                                            }

                                                            $event_log{list}->see($event_log{matched_indices}->[$event_log{shown_match}]);
                                                            return;
                                                        }

                                                        $event_log{last_search} = $regex;
                                                        $event_log{list}->selectionClear;

                                                        # Apply new expression

                                                        my @matches;
                                                        eval { @matches = grep { $_->{string} =~ /$regex/i } @lines };
                                                        for my $line (@matches)
                                                        {
                                                            $event_log{list}->selectionSet($line->{index});
                                                        }

                                                        if(@matches)
                                                        {
                                                            $event_log{matched_indices} = [map { $_->{index} } @matches];
                                                            $event_log{list}->see($matches[-1]->{index});
                                                            $event_log{shown_match}     = -1;
                                                        }
                                                        else
                                                        {
                                                            delete($event_log{matched_indices});
                                                            delete($event_log{shown_match});
                                                        }

                                                        $event_log{matched_entries}->configure(-text => scalar(@matches));
                                                    }
                                                    else
                                                    {
                                                        $event_log{list}->selectionClear;
                                                        $event_log{matched_entries}->configure(-text => 0);
                                                    }
                                                 });

        # Set column widths

        $event_log{list}->columnWidth(0, -char => 27); # Timestamp
        $event_log{list}->columnWidth(1, -char => 24); # Property
        $event_log{list}->columnWidth(2, -char => 36); # Value
        $event_log{list}->columnWidth(3, -char => 28); # Action

        for(my $i = 0 ; $i < @timestamps ; $i++)
        {
            $event_log{list}->add(       $i,    -text => $timestamps[$i]);
            $event_log{list}->itemCreate($i, 1, -text => $properties[$i]);
            $event_log{list}->itemCreate($i, 2, -text => $values[$i]    );
            $event_log{list}->itemCreate($i, 3, -text => $actions[$i]   );
        }

        # Set list item colours

        my $toggle              = 0;
        my $timestamp_previous  = '';
        for(my $i = 0 ; $i < @timestamps ; $i++)
        {
            # Check whether item has the same timestamp as the previous one

            if($timestamps[$i] ne $timestamp_previous)
            {
                $timestamp_previous = $timestamps[$i];
                $toggle             = ~$toggle;
            }
            $event_log{list}->itemConfigure($i, 0, -style => $toggle ? 'grey' : 'white');
            $event_log{list}->itemConfigure($i, 1, -style => $toggle ? 'grey' : 'white');
            $event_log{list}->itemConfigure($i, 2, -style => $toggle ? 'grey' : 'white');
            $event_log{list}->itemConfigure($i, 3, -style => $toggle ? 'grey' : 'white');

            # Check whether the item's action is a set or clear

            if($actions[$i] =~ /^SET/)
            {
                $event_log{list}->itemConfigure($i, 1, -style => $toggle ? 'set_grey' : 'set_white');
                $event_log{list}->itemConfigure($i, 2, -style => $toggle ? 'set_grey' : 'set_white');
            }
            elsif($actions[$i] =~ /^CLR/)
            {
                $event_log{list}->itemConfigure($i, 1, -style => $toggle ? 'clear_grey' : 'clear_white');
                $event_log{list}->itemConfigure($i, 2, -style => $toggle ? 'clear_grey' : 'clear_white');
            }
            elsif($actions[$i] =~ /^(NET|TRM)\./) # Network or terminal command
            {
                if($actions[$i] =~ /\.okay$/) # Response was okay
                {
                    $event_log{list}->itemConfigure($i, 3, -style => $toggle ? 'set_grey' : 'set_white');
                }
                else # Response was error
                {
                    $event_log{list}->itemConfigure($i, 3, -style => $toggle ? 'clear_grey' : 'clear_white');
                }
            }
        }

        # Scroll to the end of the log

        if(@timestamps)
        {
            $event_log{list}->see(@timestamps - 1);
        }

        # Set focus to search box

        $event_log{search}->focusForce;

        # Balloon help

        $main{balloon}->attach($event_log{list},            -msg => "FGC event log\n\nFGC events in chronological order.");
        $main{balloon}->attach($event_log{matched_entries}, -msg => "Number of matched entries.");
        $main{balloon}->attach($event_log{save},            -msg => "Save event log\n\nClick this to save the contents of the event log to a file.");
        $main{balloon}->attach($event_log{search},          -msg => "Search for and highlight entries matching a regular expression.");
        $main{balloon}->attach($event_log{total_entries},   -msg => "Total number of entries.");

        # Set the minimum window size

        $event_log{window}->update;
        $event_log{window}->minsize($event_log{window}->winfo("width"),
                                    $event_log{window}->winfo("height"));
    }
    $main{status_bar}->configure(-text => '') if(!$error_flag);
}

# Save an event log

sub save_event_log($$$$$$)
{
    my ($event_log_dialog, $device, $timestamps, $properties, $values, $actions) = @_;

    return if(!defined((my $filename = $event_log_dialog->getSaveFile(
                                                                        -defaultextension   => ".txt",
                                                                        -filetypes          => [["Text files", ".txt"], ["All files", "*"]],
                                                                        -initialdir         => '.',
                                                                        -initialfile        => "$device->{name}_event_log.txt",
                                                                     ))));
    chdir(dirname($filename));

    # Allow file to be modified by anyone if it is in the dropbox

    my $umask = umask();
    umask(0000) if($filename =~ /dropbox/);

    # Open file

    if(!open(FILE, ">", $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }
    umask($umask);

    for(my $i = 0 ; $i < @$timestamps ; $i++)
    {
        print FILE "$timestamps->[$i]|$properties->[$i]|$values->[$i]|$actions->[$i]\015\012";
    }
    close(FILE);
}

# Get an FGC run log

sub get_run_log($$)
{
    my ($run_log, $device) = @_;

    $main{status_bar}->configure(-text => '');

    my $selected_devices = defined($device) ? [ $device ] : selected_devices();
    return if(!@$selected_devices);

    $main{status_bar}->configure(-text => "Getting run log...");
    $main{window}->update;

    my $error_flag = 0;

    for my $device (@$selected_devices)
    {
        # Check that the device is not a gateway

        if($device->{channel} == 0 || "$device->{class}" !~ /^[56]\d$/ )
        {
            $main{status_bar}->configure(-text => "ERROR: Run log does not exist for $device->{name}");
            return;
        }

        # Send command to read run log

        my $response;
        eval { $response = FGC::Sync::get($device->{gateway}->{socket}, 0, "GW.FGC.RUNLOG[$device->{channel}] BIN") };

        # Check whether an error occurred

        if($@) # An error occurred sending the command
        {
            $main{status_bar}->configure(-text => "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
            return;
        }
        elsif($response->{error}) # The device returned an error
        {
            $main{status_bar}->configure(-text => "ERROR: Error getting run log for $device->{name}");
            set_response($device, "ERROR: $response->{value}");
            redraw_responses();
            $error_flag = 1;
            next;
        }

        # Decode run log data

        my $entries = FGC::Runlog::decode(\$response->{value});

        if(!defined($entries) || !@$entries)
        {
            set_response($device, "ERROR: No run log data for device");
            redraw_responses();
            next;
        }

        # Display the run log

        display_run_log($run_log, $device, $entries);
    }
    $main{status_bar}->configure(-text => '') if(!$error_flag);
}

# Display a run log

sub display_run_log($$$)
{
    my ($run_log, $device, $entries) = @_;

    # Create run log dialog if it does not already exist

    if(!defined($run_log))
    {
        $run_log->{window} = $main{window}->Toplevel();

        $run_log->{list} = $run_log->{window}->Scrolled(
                                                    "HList",
                                                    -bg                 => "white",
                                                    -borderwidth        => 1,
                                                    -columns            => 4,
                                                    -exportselection    => 0,
                                                    -font               => $font,
                                                    -height             => 40,
                                                    -itemtype           => "text",
                                                    -relief             => "groove",
                                                    -scrollbars         => "oe",
                                                    -selectbackground   => "gray",
                                                    -selectmode         => "single",
                                                    -takefocus          => 0,
                                                    -width              => 77,
                                                   )->pack(-side => "top", -fill => "both", -expand => 1);

        # Set column widths

        $run_log->{list}->columnWidth(0, -char =>  7); # Index
        $run_log->{list}->columnWidth(1, -char => 27); # Timestamp
        $run_log->{list}->columnWidth(2, -char => 32); # Label
        $run_log->{list}->columnWidth(3, -char => 12); # Value

        # Add refresh button

        $run_log->{refresh} = $run_log->{window}->Button(
                                                            -border     => 2,
                                                            -command    => sub { get_run_log($run_log, $device) },
                                                            -relief     => "groove",
                                                            -text       => "Refresh",
                                                            -width      => 8,
                                                        )->pack(-side => "right");

        # Add save button

        $run_log->{save} = $run_log->{window}->Button(
                                                        -border     => 2,
                                                        -command    => sub { save_run_log($run_log, $device) },
                                                        -relief     => "groove",
                                                        -text       => "Save",
                                                        -width      => 4,
                                                     )->pack(-side => "left");

        # Balloon help

        $main{balloon}->attach($run_log->{list},    -msg => "FGC run log\n\nFGC events in chronological order.");
        $main{balloon}->attach($run_log->{refresh}, -msg => "Refresh run log\n\nClick this to update the displayed run log.");
        $main{balloon}->attach($run_log->{save},    -msg => "Save run log\n\nClick this to save the contents of the run log to a file.");

        # Set the minimum window size

        $run_log->{window}->update;
        $run_log->{window}->minsize($run_log->{window}->winfo("width"),
                                    $run_log->{window}->winfo("height"));
    }
    else # There is an existing run log dialog
    {
        # Clear the list

        $run_log->{list}->delete("all");
    }

    # Set window title

    $run_log->{window}->title("$device->{name} run log "."(".scalar(@$entries)." entries)");

    # Add items to list

    for(my $i = 0 ; $i < @$entries ; $i++)
    {
        my $entry = $entries->[$i];

        # Format entry data if defined

        my $data;
        if(defined($entry->{data}))
        {
            $data = sprintf("0x%08X", $entry->{data});
        }

        $run_log->{list}->add(       $i,    -text => sprintf("0x%02X", $entry->{sequence}));
        $run_log->{list}->itemCreate($i, 1, -text => $entry->{timestamp});
        $run_log->{list}->itemCreate($i, 2, -text => $entry->{label});
        $run_log->{list}->itemCreate($i, 3, -text => $data);
    }

    $run_log->{entries} = $entries;

    # Scroll to the end of the log

    $run_log->{list}->see(@$entries - 1);
}

# Save a run log

sub save_run_log($$)
{
    my ($run_log, $device) = @_;

    return if(!defined((my $filename = $run_log->{window}->getSaveFile(
                                                                       -defaultextension   => ".txt",
                                                                       -filetypes          => [["Text files", ".txt"], ["All files", "*"]],
                                                                       -initialdir         => ".",
                                                                       -initialfile        => "$device->{name}_run_log.txt",
                                                                      ))));
    chdir(dirname($filename));

    # Allow file to be modified by anyone if it is in the dropbox

    my $umask = umask();
    umask(0000) if($filename =~ /dropbox/);

    # Open file

    if(!open(FILE, ">", $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }
    umask($umask);

    for my $entry (@{$run_log->{entries}})
    {
        # Format entry data if defined

        my $data;
        if(defined($entry->{data}))
        {
            $data = sprintf("0x%08X", $entry->{data});
        }

        printf FILE ("%s,%s,%s\015\012", $entry->{timestamp} || "", $entry->{label}, $data || "");
    }
    close(FILE);
}

# validates that the selection supports get config.
# returns the selected device, if valid. else throws exception.

sub get_sel_device_for_get_config()
{
    my $selected_devices = selected_devices();

    if(!@$selected_devices)
    {
       die "ERROR: One device must be selected\n";
    }

    # Check that only one converter is selected

    if(@$selected_devices > 1)
    {
        die "ERROR: Feature only supports one device at a time\n";
    }

    my $device = $selected_devices->[0];

    # Check that the device is not a gateway

    if($device->{channel} == 0)
    {
        die "ERROR: Feature not supported for gateways\n";
    }

    return $device;
}

# Get a configuration from selected power converter
# if is_regfgc3_config == true, only REGFGC3.PARAMS are extracted
# else CONFIG.SET without REGFGC3.* is extracted

sub get_config($)
{
    my ($is_regfgc3_config) = @_;

    $main{status_bar}->configure(-text => '');

    # validate selection and get device from selection
    my $device;
    eval { $device = get_sel_device_for_get_config(); };
    if($@)
    {
        $main{status_bar}->configure(-text => $@);
        return;
    }

    # Send command to read config
    my $property = $is_regfgc3_config ? "REGFGC3.PARAMS" : "CONFIG.SET";
    my $response;
    eval { $response = FGC::Utils::read_property_from_device($device, $property); };
    if($@)
    {
        $main{status_bar}->configure(-text => $@);
        set_response($device, $@);
        redraw_responses();
        return;
    }

    # Show file browser dialog
    my $initial_file = $is_regfgc3_config ? "$device->{name}_regfgc3.txt" : "$device->{name}.txt";
    return if(!defined((my $filename = $main{window}->getSaveFile(
                                                                    -defaultextension   => ".txt",
                                                                    -filetypes          => [["Text files", ".txt"], ["All files", "*"]],
                                                                    -initialdir         => '.',
                                                                    -initialfile        => $initial_file,
                                                                 ))));
    chdir(dirname($filename));

    # Allow file to be modified by anyone if it is in the dropbox
    my $umask = umask();
    umask(0000) if($filename =~ /dropbox/);

    # Open file
    if(!open(FH, ">", $filename))
    {
        $main{status_bar}->configure(-text => "ERROR: Failed to open file: $!");
        return;
    }
    umask($umask);

    # Handle REGFGC3.PARAMS
    if($is_regfgc3_config)
    {
        # Write blob in human readable format to file
        my $commands = FGC::RegFgc3ParsConv::blob_to_commands($response);

        for my $command (@{$commands})
        {
            my $command_no_colon = $command;
            $command_no_colon =~ s/:/ /;
            printf FH $command_no_colon."\n";
        }

        $main{status_bar}->configure(-text => "REGFGC3 Params read okay");
    }

    # Handle CONFIG.SET
    else
    {
        # Replace colons with spaces so that line is suitable for set command
        $response =~ s/:/ /og;

        # Handle each line of the config
        for(split("\n", $response))
        {
            s/,//g if(/DEVICE.NAME /); # Remove commas from DEVICE.NAME property

            # EPCCCS-6781: skip regfgc3 params property
            next if($_ =~ /REGFGC3.PARAMS/i);

            # EPCCCS-8916: split the response into property and value part, allowing the response to have spaces as well.
            my @parts = split(/\s+/, $_);
            my $property = shift(@parts);
            my $value = join(" ", @parts);

            printf FH ("! S %-30s %s\015\012", $property, $value) if(defined($value) && length($value));
        }

        $main{status_bar}->configure(-text => "Config read okay");
    }

    close(FH);
}

# Send a configuration to a list of devices

sub send_config($$;$)
{
    my ($target_devices, $filename, $is_regfgc3_config) = @_;

    # Clear the set_config_responses from previous send_config attempts
    for my $device (@{$target_devices})
    {
        $device->{set_config_responses} = [];
        $device->{set_config_error_count} = 0;
    }

    # get config properties to be ignored and escape them to be used as regular expressions
    my @config_props_to_ignore = map(quotemeta, @{get_config_properties_to_ignore()});
    my $ignore_regex = "(".join("|", @config_props_to_ignore).")";
    $ignore_regex = qr/$ignore_regex/;

    # Open config file
    if(!open(CONFIG_FILE, "<", $filename))
    {
        for my $device (@$target_devices)
        {
            set_response($device, "ERROR: Failed to open $filename: $!");
        }
        $main{status_bar}->configure(-text => "ERROR: Failed to open $filename: $!");
        return;
    }

    # Clear the set_config_responses from previous send_config attempts
    for my $device (@{$target_devices})
    {
        $device->{set_config_responses} = [];
        $device->{set_config_error_count} = 0;
    }

    # Read lines from the config file and send commands to devices
    while(<CONFIG_FILE>)
    {
        s/[\012\015]//g;    # Remove newline
        s/#.*//;            # Remove comments

        next if(/^\s*$/);   # Ignore blank lines

        s/^!\s*S //i;       # Remove "! S " from beginning of line

        (my $property = $_) =~ s/^(\S+).*/$1/;

        my $value = "";
        if(/^\S+\s+(.*)/)
        {
            $value = $1;
            $value =~ s/\s+//g; # Remove spaces from value
        }

        # Filters for REGFGC3
        if($is_regfgc3_config)
        {
            # EPCCCS-6781: Skip everything but regfgc3 properties
            next if($property !~ /^REGFGC3./i);
            next if($property =~ /^REGFGC3.PARAMS/i);
        }

        # Filters for CONFIG.SET
        else
        {
            next if($property =~ /$ignore_regex/i);
        }

        # Set the config property
        send_config_property($target_devices, $property, $value);
    }

    close(CONFIG_FILE);

    # Assemble and show response summaries in the UI
    for my $device (@{$target_devices})
    {
        next if(defined($device->{hidden}));

        my @response_summary = @{$device->{set_config_responses}};
        unshift(@response_summary, "Num. of Errors:".$device->{set_config_error_count});
        set_response($device, join("\012", @response_summary));
    }

    # Re-draw responses
    redraw_responses();
    $main{window}->update;
}


# returns a list of config properties that shall be ignored / filtered during 'config set/restore'

sub get_config_properties_to_ignore()
{
    my @filters = ();

    # Ignore ADC.INTERNAL.TAU_TEMP. See EPCCCS-5977
    push(@filters, 'ADC.INTERNAL.TAU_TEMP');

    # Ignore ADC.INTERNAL.VREF_TEMP.PID and ADC.INTERNAL.VREF_TEMP.REF. See EPCCCS-3821
    push(@filters, 'ADC.INTERNAL.VREF_TEMP.REF');
    push(@filters, 'ADC.INTERNAL.VREF_TEMP.PID');

    # Ignore barcode properties
    push(@filters, 'BARCODE.');

    # Ignore cal properties if $ignore_cal defined and not zero. See also EPCCCS-176
    push(@filters, 'CAL.');

    # Ignore LIMTIS.I.MEAS_DIFF. See EPCCCS-4988
    push(@filters, 'LIMITS.I.MEAS_DIFF');

    # Ignore REGFGC3 properties. See EPCCCS-6781
    push(@filters, 'REGFGC3.');

    return \@filters;
}


# displays a dialog showing a list of config properties that shall be ignored / filtered during 'config set/restore'

sub list_config_properties_to_ignore()
{
    # Dialog pop up
    my $dialog = $main{window}->Toplevel(-title => "List Filtered Config Properties");
    $dialog->Label(
                  -bg         => 'white',
                  -font       => $font.' bold',
                  -justify    => 'left',
                  -text       => "Config Properties that will be filtered during 'Config Set/Restore'",
    )->pack(-side => 'top', -fill => 'both');

    # List Data
    my @properties_to_ignore = @{get_config_properties_to_ignore()};

    # Append wildcard character if property string ends with a dot
    @properties_to_ignore = map($_ =~ /\.$/ ? $_.'*' : $_, @properties_to_ignore);
    @properties_to_ignore = sort(@properties_to_ignore);

    # List View
    my $list = $dialog->Scrolled(
                                'HList',
                                -bg                 => 'white',
                                -borderwidth        => 1,
                                -columns            => 1,
                                -exportselection    => 0,
                                -font               => $font,
                                -height             => scalar(@properties_to_ignore) + 4,
                                -itemtype           => 'text',
                                -relief             => 'groove',
                                -scrollbars         => '',
                                -selectmode         => '',
                                -takefocus          => 0,

    )->pack(-side => 'left', -fill => 'both', -expand => 1);
    $list->columnWidth(0, 400);

    # Populate List View
    for(my $row = 0, my $toggle = 0 ; $row < scalar(@properties_to_ignore); $row++)
    {
        $list->add($row, -style => $toggle ? 'grey' : 'white', -text => $properties_to_ignore[$row]);
        $toggle = ~$toggle;
    }


    # Set the minimum window size
    $dialog->update;
    $dialog->minsize($dialog->winfo("width"),
                     $dialog->winfo("height"));
}


# sets the given value to the given property on all given target devices

sub send_config_property($$$)
{
    my ($target_devices, $property, $value) = @_;

    # Display command being sent

    $main{command_entry}->delete(0, 'end');
    $main{command_entry}->insert(0, "s $property $value");
    $main{window}->update;

    for my $device (@{$target_devices})
    {
        next if(!defined($device));

        eval { FGC::Async::set($device, $property, $value) };

        if($@) # An error occurred sending the command
        {
            my $error_msg = "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0];
            push(@{$device->{set_config_responses}}, $property.":".$error_msg);
            $device->{set_config_error_count} += 1;
        }
    }

    # collect responses that will be used to assemble the response summary
    my $commands = read_responses();
    for my $command (values(%$commands))
    {
        my $response = ($command->{response}->{error} ? "ERROR: " : defined($command->{values}) ? "OKAY" : '').$command->{response}->{value};
        push(@{$command->{device}->{set_config_responses}}, $property.":".$response);
        $command->{device}->{set_config_error_count} += 1 if($command->{response}->{error});
    }

    $main{command_entry}->delete(0, 'end');
}

# Set a configuration on selected devices

sub set_config($)
{
    my ($is_regfgc3_config) = @_;

    $main{status_bar}->configure(-text => '');

    return if(!defined($main{lists}->{devices}->curselection) ||
              !defined((my $filename = $main{window}->getOpenFile(
                                                                    -filetypes  => [["Text files", ".txt"], ["All files", "*"]],
                                                                    -initialdir => '.',
                                                                    -title      => "Select config file",
                                                                 ))));
    chdir(dirname($filename));

    send_config(selected_devices(), $filename, $is_regfgc3_config);
}

# Set type configuration on selected devices

sub type_config()
{
    $main{status_bar}->configure(-text => '');

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);
    return if($main{window}->messageBox(
                                        -default    => 'Cancel',
                                        -font       => $font,
                                        -icon       => 'question',
                                        -message    => "This will set the configuration on the selected devices to the defaults for their types.  Are you sure?",
                                        -title      => 'Confirmation',
                                        -type       => 'okcancel',
                                       ) eq 'Cancel');

    # Clear responses

    set_response($_, '') for @$selected_devices;
    redraw_responses();

    # Set directory name based upon platform (Microsoft Windows or other)

    my $dirname = $^O =~ /win/i ? "X:\\etc\\config\\types" : "/user/pclhc/etc/config/types";

    for my $device (@$selected_devices)
    {
        if($device->{type} eq "UNKNOWN") # Device type is unknown
        {
            set_response($device, "ERROR: Unknown device type");

            # Re-draw responses

            redraw_responses();
            $main{window}->update;
            next;
        }

        send_config([$device], "$dirname/$device->{type}.txt", 0);
    }
}

# Get magnet configuration for a device from the database
sub get_fgc_config($$)
{
  my ($device, $subset) = @_;

  my %fgc_values;
  for my $property (keys(%$subset))
  {
    (my $property_no_array = $property) =~ s/(.*)\[.*/$1/;
    #next if(defined($property_subset) && !defined($property_subset->{$property_no_array}));

    my $response;
    eval { $response = FGC::Sync::get($device->{gateway}->{socket}, $device->{channel}, $property) };

    # Check whether an error occurred

    if($@) # An error occurred sending the command
    {
        $main{status_bar}->configure(-text => "ERROR: Failure sending command to gateway $device->{gateway}->{name}: ".(split("\n", $@))[0]);
        #FGC::EDMSDB::disconnect($edms_dbh);
        #FGC::LSADB::disconnect($lsa_dbh);
        return;
    }
    elsif($response->{error}) # The device returned an error
    {
        $main{status_bar}->configure(-text => "ERROR: Error getting $property from $device->{name}");
        set_response($device, "ERROR: $response->{value}");
        redraw_responses();
        #$error_flag++;
        next;
    }

    $fgc_values{$property} = $response->{value};

    # Remove commas if value is character array

    $fgc_values{$property} = remove_commas_from_character_arrays($property, $fgc_values{$property}, $device->{class});
  }
  return (\%fgc_values);
}

sub merged_subset($$)
{
  my ($subset, $modSubset) = @_;
  for my $modifiedProp (@$modSubset)
  {
    next if $modifiedProp !~ /\[0\]/;
    for my $prop (@$subset)
    {
      my $rep = $prop."[0]";
      $prop =~ s/$prop/$rep/g if $rep eq $modifiedProp;
    }
  }
  return @$subset;
}

sub get_subset($)
{
  my ($device) = @_;

  my $device_property;
  my @subset;
  for my $device_property (qw/config.set config.unset/)
  {
    eval { FGC::Async::get($device, $device_property) };

    # Read responses
    my $entry_state = $main{command_entry}->cget('-state');
    $main{command_entry}->configure(-state => 'disabled',   -highlightthickness => 0);
    my $commands = FGC::Async::read([sub { $main{window}->update }]);
    $main{command_entry}->configure(-state => $entry_state, -highlightthickness => 1);
    for my $command (values(%$commands))
    {
      my $response = $command->{response}->{value};
      if (defined($response))
      {
        # Remove commas from character arrays
        $response = remove_commas_from_character_arrays($device_property, $response, $device->{class});
        my @listTxt = split(/[:\n]/,$response);
        my @var = @listTxt[ grep {!($_ % 2)} 0..$#listTxt ];

        @subset = (@subset,@var);
      }
    }
  }
  return sort @subset;
}

# Display names

sub display_names()
{
    for my $device (@{selected_devices()})
    {
        set_response($device, $device->{name});
    }
    redraw_responses();

    $main{status_bar}->configure(-text => '');
}

# Display groups

sub display_groups()
{
    for my $device (@{selected_devices()})
    {
        set_response($device, $device->{gateway}->{group}->{full_name});
    }
    redraw_responses();

    $main{status_bar}->configure(-text => '');
}

# Display addresses

sub display_addresses()
{
    for my $device (@{selected_devices()})
    {
        set_response($device, "$device->{gateway}->{name}:$device->{channel}");
    }
    redraw_responses();

    $main{status_bar}->configure(-text => '');
}

# Display system types

sub display_system_types()
{
    my $error_count = 0;

    my $selected_devices = selected_devices();

    for my $device (@$selected_devices)
    {
        my $type_code;

        if(!(($type_code = "HC$device->{name}") =~ s/^(HCR.*?)\..*/$1/))
        {
            set_response($device, "ERROR: Name does not conform to convention");
            $error_count++;
            next;
        }

        my @types = grep { $_->{barcode} =~ /^${type_code}_*$/ } values(%fgc_components);

        # Check whether there was more than one type matched

        if(@types > 1)
        {
            set_response($device, "ERROR: More than one matching type");
            $error_count++;
            next;
        }

        my $type = $types[0];

        if(defined($type))
        {
            set_response($device, $type->{label});
        }
        else # Type for system is not defined
        {
            set_response($device, "ERROR: Unknown system type");
            $error_count++;
        }
    }
    redraw_responses();

    if($error_count)
    {
        $main{status_bar}->configure(-text => "$error_count error".($error_count > 1 ? 's' : '')." / ".scalar(@$selected_devices)." response".(scalar(@$selected_devices) > 1 ? 's' : ''));
    }
    else # No errors occurred
    {
        $main{status_bar}->configure(-text => '');
    }
}

# Launch gateway root shell

sub gw_root($)
{
    my ($gateway) = @_;
    return if(!defined($gateway));

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: Root shell not possible from this platform");
        return;
    }

    system("xterm -T \"root\@$gateway->{name}\" -e ssh -t $gateway->{name} sudo -i &");
    $main{status_bar}->configure(-text => '');
}

# Display gateway log

sub gw_log($;$)
{
    my ($gateway, $filter_cmd) = @_;
    return if(!defined($gateway));
    $filter_cmd = '' if(!defined($filter_cmd));

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: gateway log not available on this platform");
        return;
    }

    if(!-r FGC_FGCD_LOG_PATH."/$gateway->{name}/log")
    {
        $main{status_bar}->configure(-text => "ERROR: gateway log not accessible ".FGC_FGCD_LOG_PATH."/$gateway->{name}/log");
        return;
    }

    system("xterm +sb -geometry 120x50 -T \"$gateway->{name} log\" -e \"cat ".FGC_FGCD_LOG_PATH."/$gateway->{name}/log{.old,} $filter_cmd | less +G\" &");
    $main{status_bar}->configure(-text => '');
}

# Display gateway logs for selected devices

sub gw_log_selected(;$)
{
    my ($filter_flag) = @_;
    $filter_flag = 0 if(!defined($filter_flag));

    # Get gateways for selected devices

    my %selected_gateways = map {
                                    $_->{gateway}->{name} => $_->{gateway};
                                } @{selected_devices()};

    for my $gateway (values(%selected_gateways))
    {
        # Build filter command if requested

        my $filter_cmd;
        if($filter_flag)
        {
            $filter_cmd = gw_log_build_filter_cmd($gateway);
        }

        # Display gateway logs

        gw_log($gateway, $filter_cmd);
    }
}

# Display gateway log tail

sub gw_log_tail($;$)
{
    my ($gateway, $filter_cmd) = @_;
    return if(!defined($gateway));
    $filter_cmd = '' if(!defined($filter_cmd));

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: gateway log tail not available on this platform");
        return;
    }

    if(!-r FGC_FGCD_LOG_PATH."/$gateway->{name}/log")
    {
        $main{status_bar}->configure(-text => "ERROR: gateway log not accessible ".FGC_FGCD_LOG_PATH."/$gateway->{name}/log");
        return;
    }

    system("xterm +sb -geometry 120x50 -T \"$gateway->{name} log\" -e \"ssh $gateway->{name} tail --follow=name ".FGC_FGCD_LOG_PATH."/$gateway->{name}/log $filter_cmd\" &");
    $main{status_bar}->configure(-text => '');
}

# Display gateway logs for selected devices

sub gw_log_tail_selected(;$)
{
    my ($filter_flag) = @_;
    $filter_flag = 0 if(!defined($filter_flag));

    # Get gateways for selected devices

    my %selected_gateways = map {
                                    $_->{gateway}->{name} => $_->{gateway};
                                } @{selected_devices()};

    for my $gateway (values(%selected_gateways))
    {
        # Build filter command if requested

        my $filter_cmd;
        if($filter_flag)
        {
            $filter_cmd = gw_log_build_filter_cmd($gateway);
        }

        # Display gateway logs

        gw_log_tail($gateway, $filter_cmd);
    }
}

# Build a command to filter gateway logs for only selected devices

sub gw_log_build_filter_cmd($)
{
    my ($gateway) = @_;

    my $first_flag = 1; # Flag to indicate whether the first device is being treated

    my $filter_cmd = '| egrep \"';
    for my $device (@{selected_devices()})
    {
        next if($device->{gateway} != $gateway);

        if(!$first_flag)
        {
            $filter_cmd .= '|';
        }

        $filter_cmd .= "$device->{name}";

        for my $sub_device (@{$device->{sub_devices}})
        {
              if(defined($sub_device->{name}))
              {
                  $filter_cmd .= '|';
                  $filter_cmd .= "$sub_device->{name}";
              }
        }

        $first_flag  = 0;
    }
    $filter_cmd .= '\"';

    return($filter_cmd);
}

# Launch remote terminal

sub rterm(;$$$)
{
    my ($log, $target_device, $read_file) = @_;
    $log        = 0 if(!defined($log));
    $read_file  = 0 if(!defined($read_file));

    $main{status_bar}->configure(-text => '');

    my $target_devices;
    if(!defined($target_device))
    {
        $target_devices = selected_devices();
    }
    else
    {
        $target_devices = [$target_device];
    }
    return if(!@$target_devices);

    # Prompt for input file if needed

    my $input_file;
    if($read_file)
    {
        return if(!defined(($input_file = $main{window}->getOpenFile(
                                                                        -initialdir => '.',
                                                                        -title      => "Select input file",
                                                                    ))));
        chdir(dirname($input_file));
    }

    # Handle large numbers of devices

    if(@$target_devices > FGC_MAX_DEVS_PER_GW)
    {
        $main{status_bar}->configure(-text => "ERROR: Too many devices selected to launch remote terminals");
        return;
    }
    elsif(@$target_devices > 10)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -default    => 'Cancel',
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will start ".scalar(@$target_devices)." remote terminals.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    # Start terminal on each device

    for my $device (@$target_devices)
    {
        if($^O =~ /win/i) # Platform is Microsoft Windows
        {
            $main{status_bar}->configure(-text => "ERROR: fgcterm does not exist for this platform");
            next;
        }

        if ("$device->{class}" >= 90)
        {
            $main{status_bar}->configure(-text => "ERROR: Class $device->{class} does not support a remote terminal");
            next;
        }

        my $term_title = $device->{name};

        # Set options

        my $input_file_option = '';
        if(defined($input_file))
        {
            $input_file_option  = "-f \'$input_file\'";
            $term_title        .= ': '.basename($input_file);
        }

        my $log_option = '';
        if($log)
        {
            next if(!defined((my $log_file = $main{window}->getSaveFile(
                                                                        -defaultextension   => ".txt",
                                                                        -filetypes          => [["Text files", ".txt"], ["All files", "*"]],
                                                                        -initialdir         => '.',
                                                                        -initialfile        => "$device->{name}_rterm_log.txt",
                                                                        -title              => "Log file for $device->{name}",
                                                                       ))));
            chdir(dirname($log_file));

            $log_option = "-l \'$log_file\'";
        }

        #my $rterm_option = $device->{channel} ? "-r $device->{channel}" : '';
        my $rterm_option = $device->{channel} ? "$device->{channel}" : '';

        # Run fgcterm

        #system("xterm +sb -T \'$term_title\' -e \"`which fgcterm` $input_file_option $log_option $rterm_option $device->{gateway}->{name}\" &");
        system("xterm +sb -T \'$term_title\' -e \"`which fgcethterm` $device->{gateway}->{name} $rterm_option\" &");
    }
    $main{command_entry}->break;
}

# Get components for devices from database

sub get_components_from_db()
{
    my $selected_devices = selected_devices();

    if(@$selected_devices > FGC_MAX_DEVS_PER_GW)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will get components for ".scalar(@$selected_devices)." devices from the database.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    $main{status_bar}->configure(-text => "Getting components from database...");
    $main{window}->update;

    # Connect to database

    my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD);

    if(!defined($dbh))
    {
        $main{status_bar}->configure(-text => "Unable to connect to database");
        return;
    }

    my $error_flag = 0;
    $main{command_entry}->configure(-state => 'disabled', -highlightthickness => 0);

    for my $device (@$selected_devices)
    {
        # Get details for device from database

        my $system = FGC::DB::get_system_details($dbh, $device->{name},\%fgc_components);

        if(!defined($system))
        {
            $main{status_bar}->configure(-text => "Failed to get details for device $device->{name} from database");
            $error_flag = 1;
            last;
        }

        set_response($device, '');

        my $response = '';
        for my $component (sort { (defined($a->{COMPONENT_CHANNEL}) ? $a->{COMPONENT_CHANNEL} : '') cmp (defined($b->{COMPONENT_CHANNEL}) ? $b->{COMPONENT_CHANNEL} : '') ||
                                  $a->{COMPONENT_MTF_ID} cmp $b->{COMPONENT_MTF_ID} } values(%{$system->{components}}))
        {
            $response .= "$component->{COMPONENT_CHANNEL}-" if(defined($component->{COMPONENT_CHANNEL}));
            $response .= "$component->{COMPONENT_MTF_ID}:$component->{type}->{label}\n";
        }
        chomp($response);
        set_response($device, $response);
        $main{window}->update;
    }
    $main{command_entry}->configure(-state => 'normal', -highlightthickness => 1);
    redraw_responses();

    FGC::DB::disconnect($dbh);
    $main{status_bar}->configure(-text => '') if(!$error_flag);
}

# Get properties for devices from database

sub get_properties_from_db(;$)
{
    my ($get_all_properties)    = @_;
    $get_all_properties         = 1 if(!defined($get_all_properties));
    my $selected_devices        = selected_devices();

    if(@$selected_devices > FGC_MAX_DEVS_PER_GW)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will get properties for ".scalar(@$selected_devices)." devices from the database.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    $main{status_bar}->configure(-text => "Getting properties from database...");
    $main{window}->update;

    # Connect to database

    my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD);

    if(!defined($dbh))
    {
        $main{status_bar}->configure(-text => "Unable to connect to database");
        return;
    }

    my $error_flag = 0;
    $main{command_entry}->configure(-state => 'disabled', -highlightthickness => 0);

    for my $device (@$selected_devices)
    {
        # Get details for device from database

        my $system = FGC::DB::get_system_details($dbh, $device->{name},\%fgc_components);
        if(!defined($system))
        {
            $main{status_bar}->configure(-text => "Failed to get details for device $device->{name} from database");
            $error_flag = 1;
            last;
        }

        set_response($device, '');

        if($get_all_properties)
        {
            my $response = '';

            for my $property (sort(keys(%{$system->{all_properties}})))
            {
                $response .= "$property:";

                if(defined($system->{all_properties}->{$property}))
                {
                    $response .= $system->{all_properties}->{$property}
                }
                $response .= "\n";
            }
            chomp($response);
            set_response($device, $response);
        }
        else # Get specific property
        {
            my $command                 =  $main{command_entry}->get;
            (my $property = $command)   =~ s/^.\s+([a-zA-Z0-9\._]+)(.*)/\U$1/;
            my $array_index;
            ($array_index = $2)         =~ s/^\[(\d+)\].*/$1/ || ($array_index = undef);

            my @properties = sort(grep { /^$property(\.|$)/ } keys(%{$system->{all_properties}}));

            # Check whether property exists for system

            if(!@properties) # Property does not exist
            {
                set_response($device, "ERROR: unknown sym");
            }
            else
            {
                my $response = '';

                for my $property_name (@properties)
                {
                    my $value   = $system->{all_properties}->{$property_name};
                    $value      = '' if(!defined($value));

                    # Return element of array if index was specified

                    if(defined($array_index)) # Return element of array if index was specified
                    {
                        my @values = split(',', $value);
                        $value = defined($values[$array_index]) ? $values[$array_index] : "ERROR: bad array idx\n";
                    }
                    elsif (lc($property) eq 'regfgc3.params')
                    {
                        $value = FGC::RegFgc3ParsConv::blob_to_commands_string($value);
                    }

                    # Output property name if more than one property matched

                    if($property_name ne $property)
                    {
                        (my $print_name = $property_name) =~ s/$property\.//;
                        $response .= "$print_name:";
                    }
                    $response .= "$value\n";
                }
                chomp($response);
                set_response($device, $response);
            }

            # Add command to history

            push(@history, $command) if(!defined($history[-1]) || $command ne $history[-1]);
        }
        $main{window}->update;
    }
    $main{command_entry}->configure(-state => 'normal', -highlightthickness => 1);
    redraw_responses();

    FGC::DB::disconnect($dbh);
    $main{status_bar}->configure(-text => '') if(!$error_flag);
}

# Get parameters from LSA database

sub get_parameters_from_lsa()
{
    my $selected_devices = selected_devices();

    if(@$selected_devices > FGC_MAX_DEVS_PER_GW)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will get properties for ".scalar(@$selected_devices)." devices from the LSA database.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    $main{status_bar}->configure(-text => "Getting properties from LSA database...");
    $main{window}->update;

    # Connect to database

    my $dbh = FGC::LSADB::connect();

    if(!defined($dbh))
    {
        $main{status_bar}->configure(-text => "Unable to connect to LSA database");
        return;
    }

    $main{command_entry}->configure(-state => 'disabled', -highlightthickness => 0);

    for my $device (@$selected_devices)
    {
        set_response($device, '');

        my $command                 =  $main{command_entry}->get;
        (my $parameter = $command)  =~ s/^.\s+([a-zA-Z0-9\._]+)(.*)/\U$1/;

        # Read parameters from LSA database

        my $value = FGC::LSADB::get_converter_parameter($dbh, $device->{name}, $parameter);

        set_response($device, defined($value) ? $value : "ERROR: unknown sym");

        # Add command to history

        push(@history, $command) if(!defined($history[-1]) || $command ne $history[-1]);
        $main{window}->update;
    }
    $main{command_entry}->configure(-state => 'normal', -highlightthickness => 1);
    redraw_responses();

    FGC::LSADB::disconnect($dbh);
    $main{status_bar}->configure(-text => '');
}

sub display_sync_logs()
{
    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: Display of sync logs not possible on this platform");
        return;
    }

    my $selected_devices = selected_devices();

    if(@$selected_devices > 5)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will display sync logs for ".scalar(@$selected_devices)." devices.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    for my $device (@$selected_devices)
    {
        # Get the name of the FGC Config Manager gateway set for the device

        my $gw_set_path = FGC_MANAGER_CONFIG_PATH."/gw_sets";
        my $gw_set      = `basename \$(grep -m1 -H ^$device->{gateway}->{name}\$ $gw_set_path/* | cut -d: -f1)`;
        chomp($gw_set);

        # Extract the device's synchronisation log

        my $file = FGC_MANAGER_LOG_PATH."/$gw_set/stdout";

        if(!-r $file)
        {
            $main{status_bar}->configure(-text => "ERROR: Sync log for $device->{name} not accessible $file");
            return;
        }
        my $extract_device_log = 'perl -ne \'if(/'.$device->{name}.'/ && /^(\s+)Synchronising/) { my \$indent = \$1; print; while(<>){ print; last if(/^\$indent\w+/); } print \"\n\" }\'';
        system("xterm +sb -geometry 120x50 -T \"$device->{name} sync log\" -e \"cat $file | $extract_device_log | less +G\" &");
    }
}

# Display window to find systems containing components using the database

sub find_components_using_db()
{
    # Check whether a window is already open

    if(defined($main{db_search}))
    {
        # Highlight the find components window

        $main{db_search}->{window}->deiconify;
        $main{db_search}->{entry}->focusForce;
        return;
    }

    # Create dialog box

    my %db_search;
    $main{db_search}->{window}  = $main{window}->Toplevel(-title => "Find component(s)");
    $main{db_search}->{window}->bind('<Destroy>', sub { delete($main{db_search}); });
    $main{db_search}->{window}->resizable(0, 0);
    $main{db_search}->{window}->Label(-text => "Asset ID: ")->pack(-side => 'left');
    $main{db_search}->{entry}   = $main{db_search}->{window}->Entry(
                                                                    -bg     => 'white',
                                                                    -font   => $font,
                                                                    -width  => 20,
                                                                   )->pack(-side => 'left', -fill => 'x', -expand => 1);
    $main{db_search}->{window}->Button(
                                        -border     => 2,
                                        -command    => sub { run_db_component_search($main{db_search}->{entry}->get) },
                                        -relief     => 'groove',
                                        -text       => 'Search',
                                        -width      => 6,
                                      )->pack(-side => 'right');
    $main{db_search}->{entry}->bind('<Key-Return>', sub { run_db_component_search($main{db_search}->{entry}->get) });
    $main{db_search}->{entry}->bind('<Key-KP_Enter>', sub { run_db_component_search($main{db_search}->{entry}->get) });
    $main{db_search}->{entry}->focusForce;
}

# Run search for components in database

sub run_db_component_search($)
{
    my ($component_code) = @_;
    $component_code = "\U$component_code";

    $main{status_bar}->configure(-text => "Searching for components in database...");
    $main{window}->update;

    # Connect to database

    my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD);

    if(!defined($dbh))
    {
        $main{status_bar}->configure(-text => "Unable to connect to database");
        return;
    }

    # Construct query

    my $query;
    if(!($query = $dbh->prepare("SELECT COMPONENT_CHANNEL, COMPONENT_MTF_ID, SYSTEM_NAME
                                 FROM POCONTROLS.SYSTEMS_COMPOSITION
                                 WHERE SYSTEM_NAME IS NOT NULL AND COMPONENT_MTF_ID LIKE '\%$component_code\%'
                                 ORDER BY COMPONENT_CHANNEL, COMPONENT_MTF_ID")))
    {
        $main{status_bar}->configure(-text => "Failed to prepare database query");
        FGC::DB::disconnect($dbh);
        return(undef);
    }

    # Execute query

    if(!$query->execute())
    {
        $main{status_bar}->configure(-text => "Failed to execute database query");
        FGC::DB::disconnect($dbh);
        return(undef);
    }

    # Get array of result row hashes

    my $results = $query->fetchall_arrayref({
                                                COMPONENT_CHANNEL   => 1,
                                                COMPONENT_MTF_ID    => 1,
                                                SYSTEM_NAME         => 1,
                                            });

    # Disconnect from database

    FGC::DB::disconnect($dbh);

    my %matched_devices;

    for my $result (@$results)
    {
        my $device = $devices->{$result->{SYSTEM_NAME}};
        next if(!defined($device));

        $matched_devices{$device->{name}} = $device;

        my $channel     = defined($result->{COMPONENT_CHANNEL}) ? "$result->{COMPONENT_CHANNEL}-" : '';
        my $type        = $fgc_components{substr($result->{COMPONENT_MTF_ID}, 0, 10)};
        my $type_label  = defined($type) ? $type->{label} : '';

        if(defined($device->{response}))
        {
            $device->{response} .= "\n$channel$result->{COMPONENT_MTF_ID}:$type_label";
        }
        else
        {
            $device->{response}  = "$channel$result->{COMPONENT_MTF_ID}:$type_label";
        }
    }

    # Check whether no matches were found

    if(!keys(%matched_devices))
    {
        $main{status_bar}->configure(-text => "No components found");
        $main{lists}->{devices}->selection('clear', 0, 'end');
        count_selected_devices();

        return;
    }
    $main{status_bar}->configure(-text => '');

    # Get a list of gateways to which to connect

    my %gateways_to_connect;

    for my $device (values(%matched_devices))
    {
        $gateways_to_connect{$device->{gateway}->{name}} = $device->{gateway};
        delete($device->{hidden}) if(defined($device->{hidden}));
        $matched_devices{$device->{name}} = $device;
    }

    # Connect to gateways

    for my $gateway (values(%gateways_to_connect))
    {
        next if(defined($gateways_connected{$gateway->{name}}));

        # Connect to gateway

        connect_gateway($gateway, 0);

        # Hide non-matching devices

        for my $device (@{$gateway->{channels}})
        {
            next if(!defined($device->{name}));

            if(!defined($matched_devices{$device->{name}}))
            {
                $device->{hidden} = 1;
                delete($device->{list_index});
                delete($main{shown_devices}->{$device->{name}});
            }
        }
    }
    populate_device_list(1);

    # Select devices

    $main{lists}->{devices}->selection('clear', 0, 'end');

    my $first_match = 1;
    for my $device (sort(values(%matched_devices)))
    {
        if(defined($device->{list_index}))
        {
            # Show first matched device

            $main{lists}->{devices}->see($device->{list_index}) if($first_match);
            $first_match = 0;

            $main{lists}->{devices}->selection('set', $device->{list_index});
            set_response($device, $device->{response});
        }
        delete($device->{response});
    }
    count_selected_devices();

    $main{window}->deiconify;
    $main{command_entry}->focusForce;
}

# Display logging dialogue

sub logging_dialogue()
{
    $main{status_bar}->configure(-text => '');

    # Check that logging is supported on current platform

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: Logging not possible on this platform");
        return;
    }

    my %logging;
    my $selected_devices = selected_devices();

    # Do nothing if no devices are selected

    $main{status_bar}->configure(-text => "ERROR: No devices selected"), return if(!@$selected_devices);

    # Handle large numbers of devices

    if(@$selected_devices > 250)
    {
        $main{status_bar}->configure(-text => "ERROR: Too many devices selected to log");
        return;
    }
    elsif(@$selected_devices > 50)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -default    => 'Cancel',
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will log ".scalar(@$selected_devices)." devices.  Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    # Draw data analysis window

    my $default_duration    = 60;
    my $default_period      = 1000;
    my $max_samples         = FGC_FIELDBUS_CYCLE_FREQ * 7200; # 2 hours
    my @periods             = qw/20 40 100 200 500 1000 2000 3000 4000 5000 6000 7000 8000 9000/;

    $logging{window}            = $main{window}->Toplevel(-title => "Log published data");
    $logging{window}->resizable(0, 0);
    $logging{status_bar}        = $logging{window}->Label(-font => $font.' bold')->pack(-side => 'bottom', -fill => 'x');
    $logging{device_list}       = $logging{window}->Scrolled(
                                                                'Listbox',
                                                                -bg                 => 'white',
                                                                -borderwidth        => 1,
                                                                -exportselection    => 0,
                                                                -font               => $font,
                                                                -height             => @$selected_devices > 20 ? 20 : scalar(@$selected_devices),
                                                                -listvariable       => [map { $_->{name} } @$selected_devices],
                                                                -scrollbars         => 'oe',
                                                                -selectmode         => 'single',
                                                                -takefocus          => 0,
                                                                -width              => FGC_MAX_DEV_LEN + 1,
                                                            )->pack(-side => 'top', -fill => 'both', -expand => 1);
    $logging{device_list}->bind('<Button-1>', sub { $logging{device_list}->selection('clear', 0, 'end') });
    $logging{info_frame}        = $logging{window}->Frame(-relief => 'groove', -borderwidth => 2)->pack(-side => 'top', -fill => 'x');
    $logging{duration_frame}    = $logging{info_frame}->Frame(-borderwidth => 0)->pack(-side => 'top', -fill => 'x');
    $logging{duration_frame}->Label(-width => 20, -font => $font.' bold', -text => "Log duration (s):")->pack(-side => 'left');
    $logging{duration}          = $logging{duration_frame}->Spinbox(
                                                                    -bg                 => 'white',
                                                                    -font               => $font,
                                                                    -from               => 1,
                                                                    -to                 => $max_samples * ($periods[-1] / 1000),
                                                                    -width              => 20,
                                                                   )->pack(-side => 'left', -fill => 'x', -expand => 1);
    $logging{duration}->set($default_duration);
    $logging{period_frame}      = $logging{info_frame}->Frame(-borderwidth => 0)->pack(-side => 'top', -fill => 'x');
    $logging{period_frame}->Label(-width => 20, -font => $font.' bold', -text => "Sample period (ms):")->pack(-side => 'left', -fill => 'x');
    $logging{period}            = $logging{period_frame}->Spinbox(
                                                                    -font               => $font,
                                                                    -readonlybackground => 'white',
                                                                    -state              => 'readonly',
                                                                    -values             => \@periods,
                                                                    -width              => 20,
                                                                 )->pack(-side => 'left', -fill => 'x');
    $logging{period}->set($default_period);
    $logging{frequency_frame}   = $logging{info_frame}->Frame(-borderwidth => 0)->pack(-side => 'top', -fill => 'x');
    $logging{frequency_frame}->Label(-width => 20, -font => $font.' bold', -text => "Frequency (Hz):")->pack(-side => 'left');
    $logging{frequency}         = $logging{frequency_frame}->Label(-font => $font.' bold')->pack(-side => 'left');
    $logging{samples_frame}     = $logging{info_frame}->Frame(-borderwidth => 0)->pack(-side => 'top', -fill => 'x');
    $logging{samples_frame}->Label(-width => 20, -font => $font.' bold', -text => "Samples:")->pack(-side => 'left');
    $logging{samples}           = $logging{samples_frame}->Label(-font => $font.' bold')->pack(-side => 'left');

    my $update = sub {
                        my $duration    = $logging{duration}->get;
                        my $period      = $logging{period}->get;

                        # Block non-numeric characters from duration

                        if($duration !~ /^[0-9]+$/)
                        {
                            $logging{duration}->delete($logging{duration}->index('insert') - 1);
                            return;
                        }

                        $logging{frequency}->configure(-text => sprintf("%.2f", 1000 / $period));

                        $logging{num_samples} = int($duration * (1000 / $period));
                        $logging{samples}->configure(-text => $logging{num_samples});
                     };
    &$update();
    $logging{duration}->configure(-command => $update);
    $logging{duration}->bind('<Key>',         $update);
    $logging{period}->configure(  -command => $update);

    $logging{start}             = $logging{window}->Button(
                                                            -border     => 2,
                                                            -command    => sub
                                                                           {
                                                                                $logging{status_bar}->configure(-text => '');

                                                                                my $duration            = $logging{duration}->get;
                                                                                my $period_in_cycles    = $logging{period}->get / FGC_FIELDBUS_CYCLE_PERIOD_MS;

                                                                                if($logging{num_samples} > $max_samples)
                                                                                {
                                                                                    $logging{status_bar}->configure(-text => "ERROR: Too many samples (max. $max_samples)");
                                                                                    return;
                                                                                }

                                                                                if($logging{num_samples} * @$selected_devices > $max_samples * 10)
                                                                                {
                                                                                    $logging{status_bar}->configure(-text => "ERROR: Log will be too large");
                                                                                    return;
                                                                                }

                                                                                start_logging($period_in_cycles, $duration, $selected_devices);
                                                                                $logging{window}->destroy;
                                                                           },
                                                            -relief     => 'groove',
                                                            -text       => 'Start',
                                                            -width      => 5,
                                                          )->pack(-side => 'bottom');

    # Balloon help

    $main{balloon}->attach($logging{device_list},   -msg => "Devices which will be logged.");
    $main{balloon}->attach($logging{duration},      -msg => "Duration in seconds for which to log.");
    $main{balloon}->attach($logging{period},        -msg => "UTC aligned sampling period.");
    $main{balloon}->attach($logging{start},         -msg => "Start logging published data for selected devices.");
    $main{balloon}->attach($logging{status_bar},    -msg => "Status bar\n\nMessages about logging are displayed here.");

    # Set focus to start button

    $logging{start}->focusForce;
}

# Log published data of selected devices

sub start_logging($$$)
{
    my ($period, $duration, $devices) = @_;

    # Get the log file name

    return if(!defined((my $log_file = $main{window}->getSaveFile(
                                                                    -defaultextension   => ".csv",
                                                                    -filetypes          => [["CSV files", ".csv"], ["All files", "*"]],
                                                                    -initialdir         => '.',
                                                                    -title              => "Log file",
                                                                 ))));
    chdir(dirname($log_file));

    # Run fgclog

    system("xterm -T FGCLog -e \"`which fgclog.pl` -$period $duration ".
           join(' ', map { $_->{name} } @$devices)." > $log_file; sleep 10\" &");

    $main{status_bar}->configure(-text => '');
}

# Start FGC_Ether capture

sub fgc_ether_capture()
{
    # Check that FGC_Ether capture is supported on current platform

    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: FGC_Ether capture not possible on this platform");
        return;
    }

    my $selected_devices = selected_devices();

    # Do nothing if no devices are selected

    $main{status_bar}->configure(-text => "ERROR: No devices selected"), return if(!@$selected_devices);

    # Construct a hash of selected devices by gateway

    my %selected_by_gw;
    for my $device (@$selected_devices)
    {
        $selected_by_gw{$device->{gateway}->{name}}->{$device->{name}} = $device;
    }

    # Perform FGC_Ether capture for each gateway

    for my $gateway_name (keys(%selected_by_gw))
    {
        my $gateway             = $gateways->{$gateway_name};
        my @gw_selected_devices = values(%{$selected_by_gw{$gateway_name}});

        # Check that gateway is an FGC_Ether gateway

        if($gateway->{channels}->[0]->{class} != 4)
        {
            $main{status_bar}->configure(-text => "ERROR: At least one selected device does not use FGC_Ether");

            for my $device (@gw_selected_devices)
            {
                set_response($device, "ERROR: Device does not use FGC_Ether");
            }

            # Re-draw responses

            redraw_responses();
            return;
        }

        # Construct packet filter

        my $filter = 'broadcast';
        for my $device (@gw_selected_devices)
        {
            # Check whether the device is a gateway

            if($device->{channel} == 0)
            {
                # Do not filter packets as the gateway receives them all

                $filter = '';
                last;
            }
            else # The device is not a gateway
            {
                $filter .= sprintf(" or ether host 02:00:00:00:00:%02X", $device->{channel});
            }
        }

        # Run fgc_ether_capture

        system("xterm -T fgc_ether_capture -e \"`which fgc_ether_capture.sh` $gateway->{name} \'$filter\'\" &");
    }

    $main{status_bar}->configure(-text => '');
}

# Check whether a value is numeric

sub is_numeric($)
{
    my ($value) = @_;

    return($value =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/ ? 1 : 0);
}

# Remove commas from character arrays

sub remove_commas_from_character_arrays
{
    my ( $property, $value, $class ) = @_;

    # Check type of property in given command

    my $type_of_property = $fgc_properties{uc $property}->{classes}->{$class}->{type};

    # Property should be known and defined

    if(defined $type_of_property)
    {
        if($type_of_property eq 'CHAR')
        {
            # Property is simple char array, so remove commas

            $value =~ s/,//g;
        }
        elsif($type_of_property eq 'PARENT')
        {
            # Property type is PARENT, so it is a set of many properties, some of them could be array of char which would need removing commas

            # Firstly split set of properties to individual properties to investigate them separatelly

            my @lines = split("\n", $value);
            for my $line (@lines)
            {
                # Property name are first characters before ':', then it is value

                $line =~ /^([^:]+):(.*)$/;
                my ( $subproperty, $value ) = ( $1, $2 );

                # Check the type of individual property

                my $type_of_individual_property = $fgc_properties{uc $property.'.'.$subproperty}->{classes}->{$class}->{type};

                # If property is known and it's type is array of chars, then remove commas

                if(defined $type_of_individual_property and $type_of_individual_property eq 'CHAR')
                {
                    $line =~ s/,//g;
                }
            }

            # Replace value to fixed one

            $value = join("\n", @lines);
        }
    }

    # Return fixed value

    $value;
}

sub fgc3_reception_test()
{
    if($^O =~ /win/i) # Platform is Microsoft Windows
    {
        $main{status_bar}->configure(-text => "ERROR: fgcterm does not exist for this platform");
    }

    # Get selected devices

    my $selected_devices = selected_devices();
    return if(!@$selected_devices);

    # Handle large numbers of devices

    if(@$selected_devices > FGC_MAX_DEVS_PER_GW)
    {
        # Ask for confirmation

        return if($main{window}->messageBox(
                                            -default    => 'ok',
                                            -font       => $font,
                                            -icon       => 'question',
                                            -message    => "This will launch the FGC3 Reception Test on ".scalar(@$selected_devices)." devices.
                                                            Are you sure?",
                                            -title      => 'Confirmation',
                                            -type       => 'okcancel',
                                           ) eq 'Cancel');
    }

    $main{status_bar}->configure(-text => "Running FGC3 Reception Test ...");
    $main{window}->update;

    my $parentpath = '/user/pclhc/bin/perl/';
    my $testpath = $parentpath . 'fgc3_reception_test.sh';

    # Obtain the user's name

    my $user = obtain_users_name();

    # Launch remote terminal on each device

    for my $device (@$selected_devices)
    {
        # Run reception test for device

        if(defined($device->{name}))
        {
            my @args = ("$testpath", "--device=$device->{name}","--parentpath=$parentpath","--user=$user");
            system("xterm -T $device->{name} +sb -e @args&");
        }
        redraw_responses();
    }
    $main{status_bar}->configure(-text => "FGC3 Reception Test");
    $main{window}->update;
}

#########################################
# Begin FGC->Plot Internal Sensitivity Gr
#########################################

sub sensitivity_graph_for_selected_devices()
{
  my $selected_devices = selected_devices();

  # Allow dialog creation for multiple devices but ask for confirmation, if more than 3 devices are selected.
  if(scalar(@$selected_devices) > 3)
  {
    my $confirmed = FGC::UIElements::show_confirmation_popup(
      $main{window},
      "Confirmation",
      "This will plot Sensitivity Graphs of Internal Regulation for each of the ".scalar(@$selected_devices)." selected devices. \n\nDo you want to continue?"
    );
    return if(!$confirmed);
  }

  # Open one dialog per device
  for my $device (@$selected_devices)
  {
      sensitivity_graph_for_device($device);
  }
}

sub sensitivity_graph_for_device($)
{
  my ($device) = @_;

  # Check that the device is not offline
  if(defined($device->{online}) && !$device->{online})
  {
    set_response($device, "ERROR: Device is offline");
    redraw_responses();
    return;
  }

  # Check that the device is not a gateway
  if($device->{channel} == 0)
  {
    set_response($device, "ERROR: Not supported on gateways");
    redraw_responses();
    return;
  }

  # Check that the device's FGC_Class is supported
  if(!FGC::IntRegulation::isFGCClassSupported($device->{class}))
  {
    set_response($device, "ERROR: Sensitivity Graphs not available for class ".$device->{class});
    redraw_responses();
    return;
  }

  # read the device specific property API
  $device->{api} = FGC::Utils::get_device_api($device) if(!defined($device->{api}));

  # Set up dialog
  my %dialog;
  $dialog{window} = $main{window}->Toplevel();
  $dialog{window}->title("Plot Internal Sensitivity Graph for ".$device->{name});
  $dialog{content} = $dialog{window}->Frame()->pack(-fill => 'both');

  # Display REG STATUS warning if reg status is in fault
  my $control_loop_choice;
  my $use_test_cycle = \0;
  my $status_callback;
  my $reg_status_check = sub
  {
    my $is_in_reg_fault = FGC::IntRegulation::isInRegFault($device, ${$control_loop_choice}, ${$use_test_cycle});
    if($is_in_reg_fault){
      my $status_msg = "The initialisation of the ".FGC::IntRegulation::getLabelByLoop(${$control_loop_choice})." RST coefficients failed. The plots will be based on the last known valid values.";
      $status_callback->($status_msg, 'DarkOrange3');
    }
    else {
      $status_callback->("");
    }
  };

  # add device name input field (readonly)
  my $device_frame = FGC::UIElements::add_frame($dialog{content}, 1);
  FGC::UIElements::add_input_field($device_frame, "Device:", $device->{name}, 130, 1);

  # add dropdown of supported control loops
  my @supported_loops = @{FGC::IntRegulation::getSupportedControlLoopsByClass($device->{class})};
  my @supported_loops_texts = @{FGC::IntRegulation::getSupportedControlLoopLabelsByClass($device->{class})};
  my $dropdown_frame = FGC::UIElements::add_frame($dialog{content}, 1);
  $control_loop_choice = FGC::UIElements::add_dropdown_controls($dropdown_frame, $reg_status_check, "Regulation Mode:", \@supported_loops, \@supported_loops_texts, 0, 74);

  # add input field
  my $start_frame = FGC::UIElements::add_frame($dialog{content}, 1);
  my $start_input = FGC::UIElements::add_input_field($start_frame, "Starting Value:", "0.01", 90, 0);

  # draw test cycle controls, if test mode is supported
  if(FGC::IntRegulation::isTestModeSupportedByClass($device->{class})){
    my $test_frame = FGC::UIElements::add_frame($dialog{content}, 1);
    $use_test_cycle = FGC::UIElements::add_yes_no_controls($test_frame, $reg_status_check, "Test Cycle:", 0, 107);
  }

  # Plot button
  FGC::UIElements::add_vertical_space($dialog{content});
  my $button = FGC::UIElements::add_button($dialog{content}, "Plot", 6, undef);
  FGC::UIElements::add_vertical_space($dialog{content});

  # Set the minimum window size
  $dialog{window}->update;
  $dialog{window}->minsize($dialog{window}->winfo("width"), $dialog{window}->winfo("height"));

  # Statusbar and callback
  my $status_bar = $dialog{content}->Label(-font => $font.' bold', -text => "")->pack(-side => 'top', -fill => 'x');
  $status_callback = sub($$) {
    my ($status, $color) = @_;
    chomp($status);
    $color = 'black' if(!defined($color));
    $status_bar->configure(-text => $status, -foreground => $color);
    $dialog{content}->update();
  };

  # initial check
  $reg_status_check->();

  # Set Plot button functionality
  $button->configure(-command => sub {

    # Try to generate plot
    eval
    {
      $status_callback->("Generating plot image..."); # clear status bar

      # attach the gnuplot child process to the dialog. once the dialog closes, the process closes as well
      $dialog{int_reg} = FGC::IntRegulation->new($device, ${$control_loop_choice}, ${$use_test_cycle}, $start_input->get());
      $dialog{int_reg}->calcVectors();
      $dialog{int_reg}->plotFunctions();

      # create plot window and show generated plot image
      my $plot_window_title = $device->{name}." | ".FGC::IntRegulation::getLabelByLoop(${$control_loop_choice})." | ".(${$use_test_cycle} ? "TEST" : "OP");
      $dialog{plot_window} = FGC::UIElements::show_image_popup($dialog{window}, $plot_window_title, $dialog{int_reg}->{tmp_out_file_name}, 0, 0);
      $dialog{plot_window}->resizable(1200,800);

      $status_callback->(""); # clear status bar
    };

    # Error handling
    if($@)
    {
      $status_callback->($@);
      $dialog{int_reg} = undef;
      return;
    }
  });
}

#########################################
# End FGC->Plot Internal Sensitivity Gr
#########################################

# Obtains the current users name via the running process

sub obtain_users_name()
{
    # Use the user name from the running process

    return (getpwuid($<))[0];
}

# Functions for serializing / deserializing fgcrun+ user options
{
  Readonly my $USR_OPT_DIR => File::HomeDir::home()."/";
  Readonly my $USR_OPT_PATH => $USR_OPT_DIR.".fgcrun+_options.dat";

  Readonly my $USR_OPT_TT => 'tooltips';

  # Dezerializes the user_options hash from file and returns hashref to it
  sub load_user_options()
  {
    # Create user options directory if it does not exist already
    if(!(-d $USR_OPT_DIR))
    {
      mkdir($USR_OPT_DIR);
    }

    # Create user options file if it does not exist already
    if(!(-e $USR_OPT_PATH))
    {
      # Create default user options for new user
      my $default_options = get_default_user_options();

      # Serialize
      Storable::lock_store($default_options, $USR_OPT_PATH);

      # Set file permission to rw-r-r for the current user
      chmod(0644, $USR_OPT_PATH);
    }

    # Deserialize user options file into hash and return it
    my $user_options = Storable::lock_retrieve($USR_OPT_PATH);
    return $user_options;
  }

  # returns the value of the given user_option key, specifically for the current user
  sub get_user_option($)
  {
    my ($option_key) = @_;

    # deserialize and return value of requested option
    my $user_options = load_user_options();

    # old user option files might not have new option keys.
    # get default value if key was not persisted before and persist it
    if(!defined($user_options->{$option_key}))
    {
      my $default_value = get_default_user_options()->{$option_key};
      update_user_option($option_key, $default_value);
      $user_options->{$option_key} = $default_value;
    }

    return $user_options->{$option_key};
  }

  sub get_default_user_options()
  {
    my $default_options = {
      $USR_OPT_TT => 'balloon',
    };

    return $default_options;
  }

  # updates the value of the given user_option specifically for the current user
  sub update_user_option($$)
  {
    my ($option_key, $value) = @_;

    print "[FGCRun+ User Options]: Persisting $option_key = $value\n";

    # deserialize, update and serialize options
    my $user_options = load_user_options();
    $user_options->{$option_key} = $value;
    Storable::lock_store($user_options, $USR_OPT_PATH);
  }

  # returns the value of the "tooltips" user option, specifically for the current user
  sub get_user_option_tooltips()
  {
    return get_user_option($USR_OPT_TT);
  }

  # updates the value of the "tooltips" user option, specifically for the current user
  sub update_user_option_tooltips($)
  {
    my ($value) = @_;
    update_user_option($USR_OPT_TT, $value);
  }

  # deserializes and prints the user_options hash
  sub print_user_options()
  {
    use Data::Dumper;
    my $user_options = load_user_options();
    print Dumper($user_options);
  }
}

# End functions

my ($username, $user_font) = @ARGV;

$font = $user_font if(defined($user_font));

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));
($gateway_groups) = FGC::Names::read_group($devices, $gateways);
die "Unable to read FGCD group file\n" if(!defined($gateway_groups));

# Read subdevice files

FGC::Names::read_sub($devices, $gateways);

# Initialise FGC::Async module

FGC::Async::init($devices, $gateways);

# Draw window

draw;

# Create UDP socket

socket($udp_socket, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Bind to port

for($udp_port_number = 1024 ; $udp_port_number <= 65535 ; $udp_port_number++)
{
    bind($udp_socket, sockaddr_in($udp_port_number, INADDR_ANY)) && last;
}

if($udp_port_number == 65536)
{
    close($udp_socket);
    $main{status_bar}->configure(-text => "Unable to find a free UDP port to bind to for subscription");
}

# Register event to receive data

$main{window}->fileevent($udp_socket, readable => [\&recv_sub_data]);

MainLoop;

# Disconnect connections

for my $gateway (values(%gateways_connected))
{
    FGC::Async::disconnect($gateway);
}
close($udp_socket);

# EOF
