#!/bin/bash

project_name="fgcrun+"

centos_dependencies="perl cpan perl-open libX11-devel xterm gnuplot"
perl_dependencies="File::Slurp HTTP::Request LWP::UserAgent Tk Readonly Statistics::Descriptive Math::Round"


main() {
    install_packages_if_needed
    init_installation_folder
    install_dependencies
    configure_kt
}

output() {
    END="\e[0m"
    echo -e "$1$2$END"
}

output_in_red() {
    RED="\e[01;31m"
    output $RED "$1"
}

output_in_yellow() {
    YELLOW="\e[01;33m"
    output $YELLOW "$1"
}

confirm() {
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        [nN][oO]|[nN])
            false
            ;;
        *)
            $2
            ;;
    esac
}

install_packages_if_needed() {
    echo "Checking if needed packages are installed..."
    for pkg in $centos_dependencies; do
        if ! isinstalled $pkg; then sudo yum -y -q install $pkg; fi
    done
}

isinstalled () {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

init_installation_folder() {
    read -p "Please enter the full installation base path (Default: /opt/fgc): " path
    path=${path:-/opt/fgc}
    project_path="${path}/${project_name}"
    if [[ ! -d $project_path ]]; then
        mkdir $project_path
    fi

    output "Copying files to installation path..."
    cp -r $project_name/* $project_path/
}

install_dependencies() {
    echo "Installing perl dependencies..."
    for perl_module in $perl_dependencies; do
        sudo cpan install $perl_module
    done

}

configure_kt() {
    run_script=${project_path}/fgcrun+.sh
    touch $run_script
    chmod +x $run_script
    echo "#!/bin/sh" > $run_script
    echo "export PERL5LIB=\"${project_path}/lib\"" >> $run_script
    echo "$project_path/fgcrun+.pl '' 'courier 12'" >> $run_script

    name_lib=${project_path}/lib/FGC/Names.pm

    read -p "Please enter the location of the name file: " name_file
    sed -i '35,36d' $name_lib
    sed -i "s@^use constant FGC_NAME_FILE              =>.*@use constant FGC_NAME_FILE              => '$name_file';@" $name_lib

    read -p "Please enter the location of the group file: " group_file
    sed -i '31,32d' $name_lib
    sed -i "s@^use constant FGC_GROUP_FILE             =>.*@use constant FGC_GROUP_FILE             => '$group_file';@" $name_lib

    output_in_yellow "Please add \`alias fgcrun+=\"${run_script}\"\` to your bash configuration (without \`) so you can run fgcrun+ more easily"
}


main
