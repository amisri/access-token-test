#!../../bin/linux-x86_64/testfgcepics

< envPaths
cd "${TOP}"


######## Set env variables ########

#----------------------------------------------------------------------------------#
# Notes:                                                                           #
#  - Max array size must be increased, to accomodate large FGC commands/responses. #
#----------------------------------------------------------------------------------#

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/db")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "1000000")


######## Register support components ########

dbLoadDatabase "dbd/testfgcepics.dbd"
testfgcepics_registerRecordDeviceDriver pdbbase


######## Open command/response connection ########

#-------------------------------------------------------------#
# Notes:                                                      #
#  - Open 1 connection per each FGC (for maximum performance).#
#  - Recomend using the 'drvAsynIPPortConfigure' driver.      #
#-------------------------------------------------------------#

# Example:
#
# - 2 hosts / 4 devices
#   * Host #0:
#     ** FGC #1 : cmd_port_0_1
#   * Host #1:
#     ** FGC #1 : cmd_port_1_1
#     ** FGC #2 : cmd_port_1_2
#     ** FGC #3 : cmd_port_1_3

drvAsynIPPortConfigure("cmd_port_0_1" ,"host_0_addr:1906", 0, 0, 0)
drvAsynIPPortConfigure("cmd_port_1_1" ,"host_1_addr:1906", 0, 0, 0)
drvAsynIPPortConfigure("cmd_port_1_2" ,"host_1_addr:1906", 0, 0, 0)
drvAsynIPPortConfigure("cmd_port_1_3" ,"host_1_addr:1906", 0, 0, 0)


######## Start published data driver ########

#--------------------------------------------------------#
# Parameters:                                            #
#  1) Asyn port name                                     #
#  2) UDP IP port                                        #
#  3) Timeout                                            #
#                                                        #
# NOTE: Default UDP port values may have to be modified. #
#       Different gateways can use different UDP ports.  #
#--------------------------------------------------------#       

# Example:
#
# - 2 hosts, sending to two different UDP ports.
# - Alternatively, they could use the same UDP port.
#   * Host #0:
#     ** udp_port_0, ip-port 2906
#   * Host #1:
#     ** udp_port_1, ip-port 2905

devFgcUdpConfig("udp_port_0" , 2906)
devFgcUdpConfig("udp_port_1" , 2905)


######## Configure published data driver - hosts ########

#----------------------------------------------------------------------------#
# Notes:                                                                     #
#  - Call 1 per each Host.                                                   #
#                                                                            #
# Parameters:                                                                #
#  1) Asyn port name                                                         #
#  2) Host name                                                              #
#  3) Host publication ID                                                    #
#  4) Timeout                                                                #
#----------------------------------------------------------------------------#

# Example:
#
# - 2 hosts, with different names and IDs:
#   * Host #0:
#     ** EPICS port: udp_port_0
#     ** Host name:  name_host_0
#     ** ID:         0x00ff
#     ** Timeout:    5.0
#   * Host #1:
#     ** EPICS port: udp_port_1
#     ** Host name:  name_host_1
#     ** ID:         0x00fe
#     ** Timeout:    10.0

devFgcUdpRegisterHost("udp_port_0" , "name_host_0", "0x00ff", 5.0)
devFgcUdpRegisterHost("udp_port_1" , "name_host_1", "0x00fe", 10.0)


######## Configure published data driver - devices ########

#----------------------------------------------------------------------------#
# Notes:                                                                     #
#  - Call 1 per each FGC.                                                    #
#                                                                            #
# Parameters:                                                                #
#  1) Host name                                                              #
#  2) FGC ID (dongle)                                                        #
#  3) FGC class                                                              #
#  4) FGC name (name file notation)                                          #
#----------------------------------------------------------------------------#

# Example:
#
# - 2 hosts / 4 devices
#   * Host #0:
#     ** FGC #1 : dev.name.0
#   * Host #1:
#     ** FGC #1 : dev.name.1
#     ** FGC #2 : dev.name.2
#     ** FGC #3 : dev.name.3

devFgcUdpRegisterDev("host_name_0" , 1, 63, "dev.name.0")
devFgcUdpRegisterDev("host_name_1" , 1, 63, "dev.name.1")
devFgcUdpRegisterDev("host_name_1" , 2, 63, "dev.name.2")
devFgcUdpRegisterDev("host_name_1" , 3, 63, "dev.name.3")


######## Debug #######")

#asynSetTraceMask("udp_port_0",-1,0xff)
#asynSetTraceMask("udp_port_1",-1,0xff)
#asynSetTraceMask("cmd_port_0_1",-1,0xff)
#asynSetTraceMask("cmd_port_1_1",-1,0xff)
#asynSetTraceMask("cmd_port_1_2",-1,0xff)
#asynSetTraceMask("cmd_port_1_3",-1,0xff)

#asynSetTraceIOMask("udp_port_0",-1,0x2)
#asynSetTraceIOMask("udp_port_1",-1,0x2)
#asynSetTraceIOMask("cmd_port_0_1",-1,0x2)
#asynSetTraceIOMask("cmd_port_1_1",-1,0x2)
#asynSetTraceIOMask("cmd_port_1_2",-1,0x2)
#asynSetTraceIOMask("cmd_port_1_3",-1,0x2)


######## Load record instances - FGC udp #######

cd "${TOP}/db"

dbLoadRecords("fgcudp_class_63.db", "PORT=udp_port_1,HOST=,DEV=dev_name_0:,FGC=dev.name.0")
dbLoadRecords("fgcudp_class_63.db", "PORT=udp_port_0,HOST=,DEV=dev_name_1:,FGC=dev.name.1")
dbLoadRecords("fgcudp_class_63.db", "PORT=udp_port_0,HOST=,DEV=dev_name_2:,FGC=dev.name.2")
dbLoadRecords("fgcudp_class_63.db", "PORT=udp_port_0,HOST=,DEV=dev_name_3:,FGC=dev.name.3")


######## Load record instances - FGC cmd #######

cd "${TOP}/db"

dbLoadRecords("fgccmd_class_63.db", "PORT=cmd_port_0_1,HOST=,DEV=dev_name_0:,FGC=dev.name.0")
dbLoadRecords("fgccmd_class_63.db", "PORT=cmd_port_1_1,HOST=,DEV=dev_name_1:,FGC=dev.name.1")
dbLoadRecords("fgccmd_class_63.db", "PORT=cmd_port_1_2,HOST=,DEV=dev_name_2:,FGC=dev.name.2")
dbLoadRecords("fgccmd_class_63.db", "PORT=cmd_port_1_3,HOST=,DEV=dev_name_3:,FGC=dev.name.3")


######## Start IOC #######

cd "${TOP}/iocBoot/${IOC}"
iocInit

# EOF
