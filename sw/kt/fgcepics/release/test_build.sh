#!/bin/bash

set -e
set -o pipefail

running_dir="$(dirname "$0")"
source $running_dir/import_epics_vars

temp_dir=$(mktemp -d)

close_temp() {
    rm -rf $temp_dir
}

trap "close_temp" EXIT

###############################################################
# Build test
###############################################################

echo "Build test:"

if [ ! -d $epics_path ]; then
    echo "ERROR: Build test can't access NFS directory '$epics_path'"
    exit 1
fi

cp -r "$1/." "$temp_dir"

make -C "$temp_dir"

echo "Build test done."


# EOF
