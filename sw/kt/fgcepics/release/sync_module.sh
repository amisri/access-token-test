#!/bin/bash

set -e
set -o pipefail

running_dir="$(dirname "$0")"
source $running_dir/import_epics_vars

fgcepics_path=$(readlink -f "$(dirname "$0")/..")
fgcepics_incl="${fgcepics_path}/release/sync_list"
top_repo_path="$fgcepics_path"
parser_gen_path="${fgcepics_path}/../../../def"

echo "Syncing FGC Epics modules to $1:"


###############################################################
# Clean files
###############################################################

echo "  File auto-generation"

make -C $parser_gen_path

echo "  Redundant file cleaning"

make -C $top_repo_path uninstall
make -C $top_repo_path clean



###############################################################
# Select and copy files
###############################################################

echo "  Copying files to output directory..."

rsync -avhm --copy-links --ignore-errors --delete --delete-excluded --exclude=".venv" --include-from="${fgcepics_incl}" --exclude={".*","*",".*/"} "${top_repo_path}/" "$1"



########

echo "Syncing done."

# EOF
