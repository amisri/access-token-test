import epics
import pyfgc
import pyname
import pytest
import os
import json
import sys
import enum
from tests import tst_root
from tests.utils import Sevr, Stat

with open(os.path.join(tst_root, 'server_config', 'setup.json'), 'r') as json_file:
    data = json.load(json_file)
    GW_NAME = data['gateway']['address']
    FGC_NAME = data['gateway']['fgc']
    FGC_NAME_IOC_OK = data['ioc']['fgc_ok']

properties_test = [
    ('TEST.INT32S', -111),
    ('TEST.INT32S', 0),
    ('TEST.INT32S', 222),
    ('TEST.INT32U', 333),
    ('TEST.INT32U', 0),
    ('TEST.FLOAT', -123.456),
    ('TEST.FLOAT', 0.0),
    ('TEST.FLOAT', 654.321),
    ('TEST.CHAR', ''),
    ('TEST.CHAR', 'ABCDE'),
    ('TEST.CHAR', 'ABCDEFGHIJKL')
]

@pytest.mark.parametrize("property,input_value", properties_test)
def test_set_record_get_record(property, input_value):
    """
    Write to a '*:S' record. 
    Check if value in corresponding '*.G' record is the same.
    """

    record = FGC_NAME_IOC_OK + ':' + property.replace('.',':')
    record_s = record + ':S'
    record_g = record + ':G'
    epics.caput(record_s, input_value, wait=True, timeout=5.0)
    return_value = epics.caget(record_g, timeout=5.0)

    assert input_value == (pytest.approx(return_value) if isinstance(input_value, float) else return_value)

@pytest.mark.parametrize("property,input_value", properties_test)
def test_set_property_get_record(property, input_value):
    """
    Write property value directly to FGC.
    Check if corresponding '*.G' record is able that value from the FGC.
    """

    record_g = FGC_NAME_IOC_OK + ':' + property.replace('.',':') + ':G'
    pyfgc.set(property, input_value, devices=FGC_NAME, protocol="sync")
    epics.caput(record_g + '.PROC', 1, wait=True)
    return_value = epics.caget(record_g, timeout=5.0)
    
    assert input_value == (pytest.approx(return_value) if isinstance(input_value, float) else return_value)

@pytest.mark.parametrize("property,input_value", properties_test)
def test_set_record_get_property(property, input_value):
    """
    Write to a '*:S' record. 
    Check if corresponding property value was updated on the FGC.
    """

    record_s = FGC_NAME_IOC_OK + ':' + property.replace('.',':') + ':S'
    epics.caput(record_s, input_value, wait=True, timeout=5.0)

    return_value_str = pyfgc.get(property, devices=FGC_NAME, protocol="sync").value

    input_type = type(input_value)
    return_value = input_type(return_value_str)
    
    assert input_value == (pytest.approx(return_value) if isinstance(input_value, float) else return_value)


def test_non_failed_write_status():
    """
    Check successful write using EPICS.
    """

    test_char_pv = epics.PV(FGC_NAME_IOC_OK + ':TEST:CHAR:S')
    test_char_pv.put('NOFAIL', wait=True)
    assert test_char_pv.status == Stat.OK
    assert test_char_pv.severity == Sevr.NO_ALARM


def test_failed_write_status():
    """
    Check unsuccessful write using EPICS.
    """

    # Writing to TEST.CHAR with a whitespace will return an error.
    
    test_char_pv = epics.PV(FGC_NAME_IOC_OK + ':TEST:CHAR:S')
    test_char_pv.put('DO FAIL', wait=True)
    assert test_char_pv.status == Stat.CALC
    assert test_char_pv.severity == Sevr.INVALID

def test_value_after_non_failed_write():
    """
    Check value after successful write using EPICS.
    """

    # Original value will not be affected, by failed write.

    test_char_s_pv = epics.PV(FGC_NAME_IOC_OK + ':TEST:CHAR:S')
    test_char_g_pv = epics.PV(FGC_NAME_IOC_OK + ':TEST:CHAR:G')

    test_char_s_pv.put('NOFAIL', wait=True)

    assert test_char_s_pv.get() == 'NOFAIL'
    assert test_char_g_pv.get() == 'NOFAIL'

def test_value_after_failed_write():
    """
    Check value after unsuccessful write using EPICS.
    """

    test_char_s_pv = epics.PV(FGC_NAME_IOC_OK + ':TEST:CHAR:S')
    test_char_g_pv = epics.PV(FGC_NAME_IOC_OK + ':TEST:CHAR:G')

    test_char_s_pv.put('NOFAIL', wait=True)
    test_char_s_pv.put('DO FAIL', wait=True)

    assert test_char_s_pv.get() == 'DO FAIL'
    assert test_char_g_pv.get() == 'NOFAIL'

def test_ref_funtion_read():
    """
    Write reference table function directly to FGC.
    Check if corresponding records are able to read the values.
    """

    # Values to test
    arr_time = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5] 
    arr_ref = [1.0, 1.1, 1.2, 1.3, 1.4, 1.5]

    # Send array to FGC
    input_value = ','.join('|'.join(x) for x in zip(map(str,arr_time), map(str,arr_ref)))
    pyfgc.set('REF.TABLE.FUNCTION', input_value, devices=FGC_NAME, protocol="sync")

    # Trigger reading by EPICS IOC
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:G.PROC', 1, wait=True)
    epics_arr_time = epics.caget(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:TIME')
    epics_arr_ref = epics.caget(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:REF')

    # Check if values are the same
    assert arr_time == pytest.approx(epics_arr_time)
    assert arr_ref == pytest.approx(epics_arr_ref)

def test_ref_function_write():
    """
    Send reference table function to FGC, using EPICS.
    Check if value in the FGC is the correct one.
    """

    # Values to test
    arr_time = [0.0, 0.25, 0.5, 0.75, 1.0, 1.25] 
    arr_ref = [2.0, 2.1, 2.2, 2.3, 2.4, 2.5]

    # Send array to FGC using EPICS IOC
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:TIME', arr_time, wait=True)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:REF', arr_ref, wait=True)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:S.PROC', 1, wait=True)

    # Get value from FGC
    results_raw = pyfgc.get('REF.TABLE.FUNCTION', devices=FGC_NAME, protocol="sync")
    results_zipped = map(lambda x : x.split('|'), results_raw.value.split(','))
    results_time, results_ref = zip(*results_zipped)
    results_time = list(map(float, results_time))
    results_ref = list(map(float, results_ref))

    # Check if values are the same
    assert arr_time == pytest.approx(results_time)
    assert arr_ref == pytest.approx(results_ref)

def test_ref_funtion_read_overwrite():
    """
    Check if reading successive reference table function - using EPICS - is done correctly.
    """

    # Values to test
    arr_time_1 = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5] 
    arr_ref_1 = [1.0, 1.1, 1.2, 1.3, 1.4, 1.5]
    arr_time_2 = [0.0, 0.5, 1.0, 1.5] 
    arr_ref_2 = [2.0, 2.1, 2.2, 2.3]

    # Send array to FGC (1)
    input_value = ','.join('|'.join(x) for x in zip(map(str,arr_time_1), map(str,arr_ref_1)))
    pyfgc.set('REF.TABLE.FUNCTION', input_value, devices=FGC_NAME, protocol="sync")

    # Trigger reading by EPICS IOC (1)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:G.PROC', 1, wait=True)

    # Send array to FGC (2)
    input_value = ','.join('|'.join(x) for x in zip(map(str,arr_time_2), map(str,arr_ref_2)))
    pyfgc.set('REF.TABLE.FUNCTION', input_value, devices=FGC_NAME, protocol="sync")

    # Trigger reading by EPICS IOC (2)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:G.PROC', 1, wait=True)
    epics_arr_time = epics.caget(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:TIME')
    epics_arr_ref = epics.caget(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:REF')

    # Check if values are the same
    assert arr_time_2 == pytest.approx(epics_arr_time)
    assert arr_ref_2 == pytest.approx(epics_arr_ref)

def test_ref_function_write_overwrite():
    """
    Check if writing successive reference table function - using EPICS - is done correctly.
    """

    # Values to test
    arr_time_1 = [0.0, 0.25, 0.5, 0.75, 1.0, 1.25] 
    arr_ref_1 = [2.0, 2.1, 2.2, 2.3, 2.4, 2.5]
    arr_time_2 = [0.0, 0.25, 0.5, 0.75] 
    arr_ref_2 = [3.0, 3.1, 3.2, 3.3]

    # Send array to FGC using EPICS IOC (1)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:TIME', arr_time_1, wait=True)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:REF', arr_ref_1, wait=True)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:S.PROC', 1, wait=True)

    # Send array to FGC using EPICS IOC (2)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:TIME', arr_time_2, wait=True)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:REF', arr_ref_2, wait=True)
    epics.caput(FGC_NAME_IOC_OK + ':REF:TABLE:FUNCTION:S.PROC', 1, wait=True)

    # Get value from FGC
    results_raw = pyfgc.get('REF.TABLE.FUNCTION', devices=FGC_NAME, protocol="sync")
    results_zipped = map(lambda x : x.split('|'), results_raw.value.split(','))
    results_time, results_ref = zip(*results_zipped)
    results_time = list(map(float, results_time))
    results_ref = list(map(float, results_ref))

    # Check if values are the same
    assert arr_time_2 == pytest.approx(results_time)
    assert arr_ref_2 == pytest.approx(results_ref)

faults_test_params = [
    ('FGC_HW', 'VS_FAULT'),
    ('LIMITS', 'VS_FAULT', 'I_MEAS'),
    ('I_MEAS', ),
    ('FGC_HW', 'VS_FAULT')
]

@pytest.mark.parametrize('params', faults_test_params)
def test_fault_reading(params):
    """ 
    Check fault reading, and (char) waveform record reading.
    """

    # Set value on FGC
    fault_input = set(params)
    fault_arr = ' '.join(fault_input)
    pyfgc.set('FGC.FAULTS', fault_arr, devices=FGC_NAME, protocol="sync")

    # Get value from EPICS IOC
    epics.caput(FGC_NAME_IOC_OK + ':STATUS:FAULTS:G.PROC', 1, wait=True)
    result_arr = epics.caget(FGC_NAME_IOC_OK + ':STATUS:FAULTS:G', as_string=True)

    assert fault_input == set(result_arr.split())


# EOF