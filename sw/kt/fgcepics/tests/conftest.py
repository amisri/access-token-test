import sys
import os
import pyname
import pytest
from tests import tst_root


@pytest.fixture(scope='session', autouse=True)
def setup_name_file():
    """
    Open a custom name file.
    """

    name_path = os.path.join(tst_root, 'server_config', 'name')
    pyname.read_name_file(name_path)


# EOF