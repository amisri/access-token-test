
< envPaths
cd "${TOP}"


######## Set env variables ########

#----------------------------------------------------------------------------------#
# Notes:                                                                           #
#  - Max array size must be increased, to accomodate large FGC commands/responses. #
#----------------------------------------------------------------------------------#

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/db")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "1000000")
#epicsEnvSet("PV_PREFIX", "MAG-FGC")


######## Register support components ########

dbLoadDatabase "dbd/testfgcepics.dbd"
testfgcepics_registerRecordDeviceDriver pdbbase


######## Open command/response connection ########

#-------------------------------------------------------------#
# Notes:                                                      #
#  - Open 1 connection per each FGC (for maximum performance).#
#  - Recomend using the 'drvAsynIPPortConfigure' driver.      #
#-------------------------------------------------------------#

drvAsynIPPortConfigure("cmd_port1" ,"pcte23185.cern.ch:1906", 0, 0, 0)
drvAsynIPPortConfigure("cmd_port2" ,"pcte23185.cern.ch:1906", 0, 0, 0)


######## Start UDP published data driver ########

#--------------------------------------------------------#
# Parameters:                                            #
#  1) Asyn port name                                     #
#  2) UDP IP port                                        #
#  3) Timeout                                            #
#                                                        #
# NOTE: Default UDP port values may have to be modified. #
#       Different gateways can use different UDP ports.  #
#--------------------------------------------------------#       

devFgcUdpConfig("fgc_udp", 2906)


######## Configure published data driver - hosts ########

#----------------------------------------------------------------------------#
# Notes:                                                                     #
#  - Call 1 per each Host.                                                   #
#                                                                            #
# Parameters:                                                                #
#  1) Asyn port name                                                         #
#  2) Host name                                                              #
#  3) Host publication ID                                                    #
#  4) Timeout                                                                #
#----------------------------------------------------------------------------#

devFgcUdpRegisterHost("fgc_udp", "host_5s_t", "0x00ff", 5.0)
devFgcUdpRegisterHost("fgc_udp", "host_10ms_t", "0x00fe", 0.01)


######## Configure published data driver - devices ########

#----------------------------------------------------------------------------#
# Notes:                                                                     #
#  - Call 1 per each FGC.                                                    #
#                                                                            #
# Parameters:                                                                #
#  1) Host name                                                              #
#  2) FGC ID (dongle)                                                        #
#  3) FGC class                                                              #
#  4) FGC name (name file notation)                                          #
#----------------------------------------------------------------------------#

# OK records
devFgcUdpRegisterDev("host_5s_t", 1, 63, "FGC.1.OK")
# Invalid records
devFgcUdpRegisterDev("host_5s_t", 2, 63, "FGC.2.INV")
# Timout records
devFgcUdpRegisterDev("host_10ms_t", 1, 63, "FGC.1.TIM")

######## Debug ########

#asynSetTraceMask("fgc_udp",-1,0xff)
#asynSetTraceIOMask("fgc_udp",-1,0x2)

######## Load record instances - FGC udp ########

cd "${TOP}/db"

dbLoadRecords("fgcudp_class_63.db",  "PORT=fgc_udp,HOST=,DEV=FGC_OK:,FGC=FGC.1.OK")
dbLoadRecords("fgcudp_class_63.db",  "PORT=fgc_udp,HOST=,DEV=FGC_INV:,FGC=FGC.2.INV")
dbLoadRecords("fgcudp_class_63.db",  "PORT=fgc_udp,HOST=,DEV=FGC_TIM:,FGC=FGC.1.TIM")

dbLoadRecords("fgccmd_class_63.db",  "PORT=cmd_port1,HOST=,DEV=FGC_OK:,FGC=FGC.1")

dbLoadRecords("fgccmd_test.db",  "PORT=cmd_port1,HOST=,DEV=FGC_OK:,FGC=FGC.1")
dbLoadRecords("fgccmd_test.db",  "PORT=cmd_port2,HOST=,DEV=FGC_INV:,FGC=FGC.2")

######## Start IOC ########

iocInit

# EOF
