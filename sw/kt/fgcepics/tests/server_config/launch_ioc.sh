source launch_config
cd $KT_PROJ/fgcepics/iocBoot/ioctestfgcepics
../../bin/linux-x86_64/testfgcepics $KT_PROJ/fgcepics/tests/server_config/startup.cmd  2>&1 | tee $KT_PROJ/fgcepics/tests/server_logs/fgcepics_$(date +"%Y-%m-%d_%T").log