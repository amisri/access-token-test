import epics
import pyfgc
import pyname
import pytest
import os
import json
import sys
import threading
import enum
import time
from tests import tst_root
from tests.utils import Sevr, Stat, CountDownTimer

with open(os.path.join(tst_root, 'server_config', 'setup.json'), 'r') as json_file:
    data = json.load(json_file)
    GW_NAME = data['gateway']['address']
    FGC_NAME = data['gateway']['fgc']
    FGC_NAME_IOC_OK = data['ioc']['fgc_ok']
    FGC_NAME_IOC_INV = data['ioc']['fgc_inv']
    FGC_NAME_IOC_TIM = data['ioc']['fgc_tim']

def wait_for_pub_update(fgc_name, timeout):
    """
    Wait for the arrival of a new FGC status publication.
    """

    # Wait for time tick
    data_stat_pv = epics.PV(fgc_name + ':DATA_STATUS')

    data_stat_pv.get_timevars()
    time_1 = data_stat_pv.timestamp
    time_2 = time_1

    timer = CountDownTimer(timeout)

    while time_2 == time_1 and not timer.ended:
        time.sleep(.02)
        data_stat_pv.get_timevars()
        time_2 = data_stat_pv.timestamp

    if timer.ended:
        raise TimeoutError("Wait for new publication timed out")

    return True


published_records = [
    'ST_FAULTS:I_MEAS',
    'ST_FAULTS:FGC_HW',
    'STATE_PLL',
    'I_DIFF_MA',
    'EVENT_GROUP',
    'I_REF',
    'I_MEAS'
]

@pytest.mark.parametrize('record', published_records)
def test_udp_published_property(record):
    """
    Check if (valid) published properties are being updated.
    """

    udp_pv = epics.PV(FGC_NAME_IOC_OK + ':' + record)

    udp_pv.get_timevars()
    time_1 = udp_pv.timestamp
    time_2 = time_1

    timer = CountDownTimer(2.0)

    while time_2 == time_1 and not timer.ended:
        time.sleep(.02)
        udp_pv.get_timevars()
        time_2 = udp_pv.timestamp

    assert not timer.ended
    assert udp_pv.severity == Sevr.NO_ALARM
    assert udp_pv.status == Stat.OK
    assert time_2 > time_1

@pytest.mark.parametrize('record', published_records)
def test_udp_published_invalid(record):
    """
    Check if invalid published properties are being identified correctly.
    """

    udp_pv = epics.PV(FGC_NAME_IOC_INV + ':' + record)
    udp_pv.get_timevars()

    assert udp_pv.severity == Sevr.INVALID

@pytest.mark.parametrize('record', published_records)
def test_udp_published_timeout(record):
    """
    Check if timed-out published properties are being identified correctly.
    """

    udp_pv = epics.PV(FGC_NAME_IOC_TIM + ':' + record)

    timer = CountDownTimer(2.0)

    while udp_pv.severity == Sevr.NO_ALARM and not timer.ended:
        time.sleep(.02)

    assert not timer.ended
    assert udp_pv.severity == Sevr.INVALID
    assert udp_pv.status == Stat.TIMEOUT

faults_test_pub = [
    (
        ['LIMITS', 'VS_FAULT', 'I_MEAS'], 
        ['FGC_STATE']
    ),
    (
        ['I_MEAS', ],
        ['FGC_HW', 'VS_FAULT']
    ),
    (
        [],
        ['I_MEAS', 'FGC_STATE', 'LIMITS']
    )
]

@pytest.mark.parametrize('faults_yes,faults_no', faults_test_pub)
def test_udp_bitmask(faults_yes, faults_no):
    """
    Check published bitmask properties (EPICS bi records). 
    """

    # Set value on FGC
    fault_arr = ' '.join(faults_yes)
    pyfgc.set('FGC.FAULTS', fault_arr, devices=FGC_NAME, protocol="sync")

    # Wait for time tick
    wait_for_pub_update(FGC_NAME_IOC_OK, 2.0)

    # Get value from EPICS IOC
    for fault in (faults_yes + faults_no):

        udp_record_pv = epics.PV(FGC_NAME_IOC_OK + ':ST_FAULTS:' + fault)
        udp_record_pv_val = udp_record_pv.get()
        udp_record_pv_str = udp_record_pv.get(as_string=True)

        assert udp_record_pv_val == (1 if fault in faults_yes else 0)
        assert udp_record_pv_str == (fault if fault in faults_yes else "")


state_pc_test_pub = [
    'BLOCKING',
    'ON_STANDBY',
    'IDLE',
    'DIRECT',
]

@pytest.mark.parametrize('state_pc', state_pc_test_pub)
def test_udp_enum_state_pc(state_pc):
    """
    Check published enum properties (EPICS mbbi records). 
    Check state information split accross multiple mbbi records.
    """

    # Set FGC state to OFF
    pyfgc.set('MODE.PC', 'OFF', devices=FGC_NAME, protocol="sync")
    state_pc_pv_0 = epics.PV(FGC_NAME_IOC_OK + ':STATE_PC:0')
    state_pc_pv_1 = epics.PV(FGC_NAME_IOC_OK + ':STATE_PC:1')
    state_pc_str_0 = state_pc_pv_0.get(as_string=True)
    state_pc_str_1 = state_pc_pv_1.get(as_string=True)

    timer = CountDownTimer(5.0)

    while state_pc_str_0 != 'OFF' and state_pc_str_1 != 'OFF' and not timer.ended:
        time.sleep(.02)
        state_pc_str_0 = state_pc_pv_0.get(as_string=True)
        state_pc_str_1 = state_pc_pv_1.get(as_string=True)

    if timer.ended:
        pytest.fail("OFF timout ({}|{})".format(state_pc_str_0, state_pc_str_1))

    # Set FGC to desired state
    pyfgc.set('MODE.PC', state_pc, devices=FGC_NAME, protocol="sync")
    state_pc_pv_0 = epics.PV(FGC_NAME_IOC_OK + ':STATE_PC:0')
    state_pc_pv_1 = epics.PV(FGC_NAME_IOC_OK + ':STATE_PC:1')
    state_pc_str_0 = state_pc_pv_0.get(as_string=True)
    state_pc_str_1 = state_pc_pv_1.get(as_string=True)

    timer = CountDownTimer(5.0)

    while state_pc_str_0 != state_pc and state_pc_str_1 != state_pc and not timer.ended:
        time.sleep(.02)
        state_pc_str_0 = state_pc_pv_0.get(as_string=True)
        state_pc_str_1 = state_pc_pv_1.get(as_string=True)

    if timer.ended:
        pytest.fail("{} timout ({}|{})".format(state_pc, state_pc_str_0, state_pc_str_1))

    assert state_pc_str_0 == state_pc or state_pc_str_1 == state_pc
    assert state_pc_str_0 != state_pc_str_1

    assert state_pc_pv_0.severity == (Sevr.NO_ALARM if state_pc_str_0 == state_pc else Sevr.INVALID)
    assert state_pc_pv_0.status == (Stat.OK if state_pc_str_0 == state_pc else Stat.STATE)

    assert state_pc_pv_1.severity == (Sevr.NO_ALARM if state_pc_str_1 == state_pc else Sevr.INVALID)
    assert state_pc_pv_1.status == (Stat.OK if state_pc_str_1 == state_pc else Stat.STATE)