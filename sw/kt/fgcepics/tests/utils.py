import enum
import threading

class Sevr(enum.IntEnum):
    """
    EPICS records severity values.
    """
    NO_ALARM = 0
    MINOR = 1
    MAJOR = 2
    INVALID = 3

class Stat(enum.IntEnum):
    """
    (Some) EPICS records status values.
    """
    OK = 0
    STATE = 7
    TIMEOUT = 10
    CALC = 12
    UDF = 17

class CountDownTimer():
    """
    Count down N seconds.
    """

    def __init__(self, time_s):

        def stop():
            self.has_ended = 1
            self.t.cancel() 

        self.has_ended = 0
        self.t = threading.Timer(time_s, stop)
        self.t.start()

    @property
    def ended(self):
        return self.has_ended