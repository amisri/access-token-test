#!/bin/sh
SYS_FGCETH_PATH=/user/fgckttst/fgc
SYS_FGCETH_LOG_PATH=$SYS_FGCETH_PATH/var/log
SYS_FGCETH_NIC=enp2s0
SYS_FGCETH_HOST=fgcgate1
$SYS_FGCETH_PATH/bin/fgcethd -e $SYS_FGCETH_NIC -n $SYS_FGCETH_PATH/etc/fgcd/name -c $SYS_FGCETH_PATH/firmware/$SYS_FGCETH_HOST -u $SYS_FGCETH_PATH/etc/fgcd/udp_subscriptions/$SYS_FGCETH_HOST 2>&1 | tee $SYS_FGCETH_LOG_PATH/fgcethd_$(date +"%Y-%m-%d_%T").log &
