# Systemd module

## Modify and copy *run_fgcethd.sh*

1. Set values of SYS_FGCETH_PATH (absolute path to fgc home directory), SYS_FGCETH_NIC (NIC connected to FGCs) and SYS_FGCETH_HOST (hostname).
2. Copy *run_fgcethd.sh* to fgc/bin/, on the same directory as the executable.

## Modify and copy *fgcethd.service*

1. Set absolute path to *run_fgcethd.sh* in ExecStart.
2. Copy *fgcethd.service* (with sudo) to directory */etc/systemd/system/*.

## Enable and use new systemd module

1. Reload systemd with:
*sudo systemctl daemon-reload*.
2. Enable new systemd with:
*sudo systemctl enable fgcethd*.
3. To start FGC gateway:
*sudo systemctl start fgcethd* (should be automatic during startup).
4. To stop FGC gateway:
*sudo systemctl stop fgcethd*.
5. To restart FGC gateway:
*sudo systemctl restart fgcethd*.
