#!/usr/bin/perl -w
#
# Name:     code_info.pl
# Purpose:  Display info block for FGC code files
# Author:   Stephen Page

use Fcntl ':mode';
use strict;

die "Usage: $0 <code file 1> ... <code file n>\n" if(!@ARGV);

# Get length of longest filename

my $max_len = 0;
for(@ARGV)
{
    $max_len = length() if(length() > $max_len);
}

# Display code info for each file

my $pack_template   = "NCCnn";
my $info_size       = length(pack($pack_template));

for my $file (@ARGV)
{
    # Stat file

    my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev,
        $size, $atime, $mtime, $ctime, $blksize, $blocks) = stat($file)
            or die "Failed to stat $file: $!\n";

    # Check that file is a valid code file

    die "$file is not a file"                   if(!S_ISREG($mode));
    die "File $file is smaller than info block" if($size < $info_size);

    # Open file and seek to code info

    open(FILE, "<", $file) or die "Unable to open file $file: $!\n";
    seek(FILE, $size - $info_size, 0);

    # Read info block

    my $bin_info;
    read(FILE, $bin_info, $info_size);
    close(FILE);

    # Decode info block

    my
    (
        $version,
        $class_id,
        $code_id,
        $old_version,
        $crc,
    ) = unpack($pack_template, $bin_info);

    # Append the date string to the version if it is set

    if($version != 0xFFFFFFFF)
    {
        $version = sprintf("%10u %s", $version, scalar(localtime($version)));
    }

    # Print code info

    printf("%-${max_len}s: class_id=%3u, code_id=%3u, crc=0x%04X, old_version=%5u, version=%s\n",
           $file, $class_id, $code_id, $crc, $old_version, $version);
}

# EOF
