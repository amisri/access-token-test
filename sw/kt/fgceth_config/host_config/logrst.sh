#! /bin/bash 

if [ $# -ne 2 ]; then

    echo "usage: $0 <gateway> <fgc_id>"
    exit 1
fi

echo Sending LOG RESET from gateway $1, fgc $2

fgcethterm $1 $2 "S LOG.RESET"

echo Done

# EOF

