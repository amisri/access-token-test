#! /bin/bash 

if [ $# -ne 2 ]; then

    echo "usage: $0 <gateway> <fgc_id>"
    exit 1
fi

echo "Reading CONFIG.SET from gateway $1, fgc $2"

fgcethterm $1 $2 "G CONFIG.SET" | fgc2cfg > config-$1-$2

echo Done - config written to config-$1-$2

# EOF

