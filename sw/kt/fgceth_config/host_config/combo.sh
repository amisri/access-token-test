#! /bin/bash 

if [ $# -lt 3 ]; then

echo "usage: $0 <gateway> <fgc_id> [file1 file2 ... fileN]"
    exit 1
fi

echo Sending operational FGC config to gateway $1, fgc $2

cd $FGCETH_HOME/configuration/$1

# Send configuration files to the FGC via the gateway

for i in $(seq 3 $#); do
    fgcethterm $1 $2 ${!i}
done

# Read back the configuration

./get.sh $1 $2

cd -

# EOF
