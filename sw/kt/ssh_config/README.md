# Notes

## Usage on Linux

Import all these files in your local *.ssh/config file*.
Example:
```
Include ~/projects/fgc/sw/kt/ssh_config/esrf_ssh_config
Include ~/projects/fgc/sw/kt/ssh_config/freia_ssh_config
Include ~/projects/fgc/sw/kt/ssh_config/triumf_ssh_config
```

## Usage on Windows

Configure your *.ssh/config* file in lxplus.cern.ch, like explained on the previous topic.
Configure Putty to connect to lxplus.cern.ch, and execute 'ssh' to external lab.

### Powerspy

Copy *launch_ext_powerspy.sh* from ~/projects/fgc/sw/kt/ssh_config/ to your home directory ~/ in lxplus.
On windows (ie. cerntsepc), launch 'EPC tools'->FGC->'external labs'->PowerSpy.

## Laboratiories

See notes below for each one of the laboratories:

### TRIUMF

### FREIA

### ESRF

Requires a personal visitor account at ESRF (not possible to access using service account).
Add the following code to the local .ssh/config file (replace <esrf_user> by your ESRF visito account username):
```
Host         esrf-* *.esrf.fr
User         <esrf_user>
```
