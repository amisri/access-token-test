#!/bin/bash

ssh -f -N $1-app
sleep 1
firefox -no-remote "http://localhost:1080/powerspy/"
pkill -f "ssh .*$1-app"

# EOF
