#!/bin/bash

set -e
set -o pipefail

branch="master"



########################################################################
# Auxiliary functions
########################################################################

write_help() {
    echo "Help:"
    echo "    $0 <name> <timestamp> <history_file>"
}



#######################################################################
# Validation: Input arguments
#######################################################################

# Parse named arguments

while getopts "h" arg; do
    case $arg in
        h)
            write_help
            exit 0
            ;;
    esac
done

shift "$(($OPTIND - 1))"

# Validate positional arguments

if [[ $# -ne 3 ]]; then
    echo "Invalid argument number!"
    write_help
    exit 1
fi

proj_name=$1
proj_time=$2
hist_file=$3


######################################################################
# Validation: Check if commit will be successful
######################################################################

if [ "$(git rev-parse --abbrev-ref HEAD)" != "${branch}" ]; then
    echo "Please move to branch ${branch}!"
    exit 1
fi

if [ ! -z "$(git status --untracked-files=no --porcelain)" ]; then
    echo "Please resolve all staged/unstaged changes!"
    exit 1
fi

git fetch origin ${branch}

if [ ! -z "$(git diff --quiet)" ]; then
    echo "Please sync with remote repository!"
    exit 1 
fi

######################################################################
# Add tag to Git repo
######################################################################

str_date=$(date +"%d-%m-%Y %H:%M:%S")

mv $hist_file ${hist_file}.tmp
printf "[KT software] %s: %s (version %s)\n" "$proj_name" "$str_date" "$proj_time" > $hist_file
cat ${hist_file}.tmp >> $hist_file
rm ${hist_file}.tmp

git add $hist_file
git commit -m "KT software release: ${proj_name}/${proj_time}"
git tag "${proj_name}/${proj_time}"
#git push origin ${branch} --follow_tags
git push origin ${branch}        # Update the remote repository with commits created during the install process
git push origin ${branch} --tags # Update the remote repository with tags created during the install process
 
echo "KT program ${proj_name}, version ${proj_time}, tagged on Git."



# EOF
