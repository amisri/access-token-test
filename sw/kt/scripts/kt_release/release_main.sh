#!/bin/bash

######################################################################
# Check config.sh file:                                              #
# - Variables '$proj_sync_sh' and '$proj_test_sh' should be defined. #
######################################################################


# Exit if any error is returned

set -e
set -o pipefail



# Parse arguments

tar_location=
do_release=
config_file=

while getopts "t:rc:h" arg; do
    case $arg in
        t)
            tar_location="$(readlink -f "$OPTARG")"
            ;;
        r)
            do_release="YES"
            ;;
        c)
            config_file="$(readlink -f "$OPTARG")"
            ;; 
        h|?)
            echo "./release [-t <path>?] [-d] [-c <config>]"
            echo "    -t <path>   - (Optional) Export tarball to path"
            echo "    -r          - (Optional) Release" 
            echo "    -c <config> - (Optional) Config file path"
            exit 0
            ;;
    esac
done


# Temporary directories

temp_dir=$(mktemp -d)
temp_dir_release="$temp_dir/release"
temp_dir_test="$temp_dir/test"

mkdir $temp_dir_release
mkdir $temp_dir_test

close_temp_dirs() {  
    echo "Closing temporary directories."
    rm -rf $temp_dir
}

trap "close_temp_dirs" EXIT



# Variable definitions

scripts_dir="$(dirname "$(readlink -f "$0")")"



# Import paths to build and test scripts, check if files exist

running_dir="$(dirname "$0")"

if [ ! -z $config_file ]; then
    source "$config_file"
else
    source "$running_dir/config_release"
fi


for file in $proj_sync_sh $proj_test_sh $project_history; do
    if [ ! -f "$running_dir/$file" ]; then
        echo "File $running_dir/$file not found"
        exit 1
    fi
done


if [ -z "$project_name" ]; then
    echo "Project name not defined."
    exit 1
fi


echo "Releasing $project_name..."

timestamp=$(date +%s)
tar_dir="${project_name}_${timestamp}"
tar_name="${tar_dir}.tar.gz"


echo "Releasing $project_name version $timestamp..."


###############################################################
# Select and copy files
###############################################################

echo "Syncing..."

mkdir -p "$temp_dir_release/$tar_dir"

for file in $proj_sync_sh; do
    "$running_dir/$file" "$temp_dir_release/$tar_dir" "$project_name" "$timestamp"
done


###############################################################
# Test files
###############################################################

echo "Testing & validating..."

for file in $proj_test_sh; do

    # Clean & reset directory in every turn 
    rm -rf "$temp_dir_test/*"
    cp -r "$temp_dir_release/$tar_dir" "$temp_dir_test/"

    # Do test
    "$running_dir/$file" "$temp_dir_test/$tar_dir" "$project_name" "$timestamp"

done


###############################################################
# Create tar file 
###############################################################

echo "Creating tarball..."

tar -cvf $temp_dir/$tar_name -C $temp_dir_release .  

echo "Tarball created"

if [ ! -z $tar_location ]; then
    cp $temp_dir/$tar_name $tar_location/
    echo "Tarball copied to $tar_location"
fi


##############################################################
# Release tar file
##############################################################

if [ -z $do_release ]; then
    echo "Tarball not released."
else
    echo "Release..."
    if [ -z "$project_history" ]; then
        echo "Release history file not defined."
        exit 1
    fi

    $scripts_dir/kt_git_release.sh "$project_name" "$timestamp" "$running_dir/$project_history"
    $scripts_dir/kt_tar_release.sh "$project_name" "$timestamp" "$temp_dir/$tar_name"
fi

echo "Done."


# EOF
