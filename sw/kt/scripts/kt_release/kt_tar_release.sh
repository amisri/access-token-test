#!/bin/bash

set -e
set -o pipefail

install_user="poccdev"
install_host="cs-ccr-teepc2"
install_dir="kt"
install_path="/user/${install_user}/${install_dir}"



########################################################################
# Auxiliary functions
########################################################################

# Print script arguments.

write_help() {
    echo "Help:"
    echo "    $0 <name> <timestamp> <file>"
}



#######################################################################
# Validation: Input arguments
#######################################################################

# Parse named arguments

force=

while getopts "hf" arg; do
    case $arg in
        h)
            write_help
            exit 0
            ;;
        f)
            force="YES"
            ;;
    esac
done

shift "$(($OPTIND - 1))"

# Validate positional arguments

if [[ $# -ne 3 ]]; then
    echo "Invalid argument number!"
    write_help
    exit 1
fi

proj_name=$1
proj_time=$2
tar_file=$3



####################################################################
# Validation: File name
####################################################################

if [ -z $force ]; then
    if [ "$(basename $tar_file)" != "${proj_name}_${proj_time}.tar.gz" ]; then
        echo "File should be named \"${proj_name}_${proj_time}.tar.gz\"!"
        exit 1
    fi
fi



####################################################################
# Release tarball
####################################################################

# Create missing directoriesi, if needed

ssh ${install_user}@${install_host} "if [ ! -d ${install_path}/${proj_name} ]; then mkdir ${install_path}/${proj_name}; echo 'WARNING: directory ${install_path}/${proj_name} created, check ACL!'; fi"

scp ${tar_file} ${install_user}@${install_host}:${install_path}/${proj_name}/

echo "KT program ${proj_name}, version ${proj_time}, released to ${install_path}/${proj_name}/$(basename $tar_file)."



# EOF
