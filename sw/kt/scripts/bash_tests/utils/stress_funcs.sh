#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fgcethpath=$(readlink -m "${DIR}/../../..")


# Generate random float

randf()
{
    echo "scale=6; $(printf '%.4f\n' ".$RANDOM") * $1" | bc
}

# Maximum of two real numbers

max()
{
    if [ "$(echo "$1 > $2" | bc)" == "1" ]; then
        echo $1
    else
        echo $2
    fi
}

# Minimum of two real numbers

min()
{
    if [ "$(echo "$1 < $2" | bc)" == "1" ]; then
        echo $1
    else
        echo $2
    fi
}

# Create a random reference table function, with N points
# ONLY CALL ONCE, NOT in parallel

conf_ref_table()
{
    #pos=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G LIMITS.I.POS[0]")
    #min=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G LIMITS.I.MIN[0]")
    #rate=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G LIMITS.I.RATE[0]")
    pos=5.0
    min=0.0
    rate=0.1

    value=$min
    values="0|${value}"

    for i in $(seq 1 $1); do
        value=$(echo "scale=6; ${value} + ( $(randf 0.9) - 0.4 ) * ${rate} * 0.01" | bc)
        value=$(min $value $pos)
        value=$(max $value $min)
        local time=$(echo "$i * .1" | bc)
        values+=",${time}|${value}"
    done
}



# random_ref_table()
# {
#     local values="0|0.000000"

#     for i in $(seq 1 $3); do
#         values+=",${time}|0.000000"
#     done

#     echo $values
# }

# Change FGC state to IDLE


to_idle()
{
    while [ "$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G STATE.PC" 2>&1 )" != "OFF" ]; do
        if [ $(date +%s) -gt $timelimit ]; then
            echo "Failed: Setting STATE to OFF #$2"
            return
        else
            ${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "S MODE.PC OFF" > /dev/null    
        fi
        sleep 1
    done

    echo "OK:     [S MODE.PC OFF] #$2"
     
    while [ "$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G STATE.PC" 2>&1 )" != "ON_STANDBY" ]; do 
        if [ $(date +%s) -gt $timelimit ]; then
            echo "Failed: Setting STATE to ON_STANDBY #$2"
            return
        else
            ${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "S MODE.PC ON_STANDBY" > /dev/null       
        fi
        sleep 1
    done

    echo "OK:     [S MODE.PC ON_STANDBY] #$2"
   
    while [ "$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G STATE.PC" 2>&1 )" != "IDLE" ]; do
        if [ $(date +%s) -gt $timelimit ]; then
            echo "Failed: Setting STATE to IDLE #$2"
            return
        else
            ${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "S MODE.PC IDLE" > /dev/null       
        fi
        sleep 1
    done

    echo "OK:     [S MODE.PC IDLE] #$2"
}

# Get time

test_time()
{
    local cmds=()

    for count in $(seq 1 $3); do
        cmds+=("G TIME.NOW")
    done        

    local result=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "${cmds[@]}")

    if [[ "$result" =~ .*\[ERROR].* ]]; then
        echo "Failed: [G TIME.NOW] #$2"
    else
        echo "OK:     [G TIME.NOW] #$2"
    fi
}

# Get a float value

test_float()
{
    local cmds=()

    for count in $(seq 1 $3); do
        cmds+=("S TEST.FLOAT $(printf '%.4f\n' ".$RANDOM")")
    done        

    local result=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "${cmds[@]}")

    if [[ "$result" =~ .*\[ERROR].* ]]; then
        echo "Failed: [S TEST.FLOAT] #$2"
    else
        echo "OK:     [S TEST.FLOAT] #$2"
    fi
}

# Get ref, device & status

test_ref_dev_stat()
{
    local cmds=()

    for count in $(seq 1 $3); do
        cmds+=("G REF")
        cmds+=("G DEVICE")
        cmds+=("G STATUS")
    done        

    local result=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "${cmds[@]}")

    if [[ "$result" =~ .*\[ERROR].* ]]; then
        echo "Failed: [G REF|DEVICE|STATUS] #$2"
        echo $result
    else
        echo "OK:     [G REF|DEVICE|STATUS] #$2"
    fi
}

# Test log

test_log()
{
    local cmds=()

    for count in $(seq 1 $3); do
        cmds+=("G LOG.EVT")
    done        

    local result=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "${cmds[@]}")

    if [[ "$result" =~ .*\[ERROR].* ]]; then
        echo "Failed: [G LOG_EVT] #$2"
    else
        echo "OK:     [G LOG.EVT] #$2"
    fi
} 

# Set a new random reference table

test_long_ref_set()
{
    #local values=$(conf_ref_table "$1" "$2" 1000)
    for count in $(seq 1 $3); do
        local result=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "S REF.TABLE.FUNCTION ${values}")
        sleep 1    

        if [[ "$result" =~ .*\[ERROR].* ]]; then
            echo "Failed: [S REF.TABLE.FUNCTION] #$2"
            echo $result
        else
            echo "OK:     [S REF.TABLE.FUNCTION] #$2"
        fi
       
        while [ "$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G STATE.PC" 2>&1 )" != "ARMED" ]; do
            if [ $(date +%s) -gt $timelimit ]; then
                echo "Failed: Setting STATE to ARMED #$2"
                return
            else
                ${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "S MODE.PC IDLE" > /dev/null
            fi
            sleep 1
        done
 
        while [ "$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G STATE.PC" 2>&1 )" != "IDLE" ]; do
            if [ $(date +%s) -gt $timelimit ]; then
                echo "Failed: Setting STATE to IDLE #$2"
                return
            else
                ${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "S MODE.PC IDLE" > /dev/null
            fi
            sleep 1
        done
    done
}

# Get the reference table from a FGC

test_long_ref_get()
{
    for count in $(seq 1 $3); do
        local result=$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$2" "G REF.TABLE.FUNCTION")

        if [[ "$result" =~ .*\[ERROR].* ]]; then
            echo "Failed: [G REF.TABLE.FUNCTION] #$2"
            echo $result
        else
            echo "OK:     [G REF.TABLE.FUNCTION] #$2"
        fi
    done
}


#EOF
