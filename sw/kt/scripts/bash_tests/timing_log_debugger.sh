#/bin/bash

while read line
do
    if [[ $line = *"New timing signal detected"* ]] || [[ $line = *"Timing signal may have been missed"* ]]; then
        echo ">>> $line"                        >> $1
        echo ""                                 >> $1
        top -bn1 | grep "load average" -A 20    >> $1
        echo ""                                 >> $1
    fi
    echo "$line"
done < /dev/stdin
