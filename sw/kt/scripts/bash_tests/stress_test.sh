#!/bin/bash

# $1 - gateway name

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fgcethpath=$(readlink -m "${DIR}/../..")

source ${DIR}/utils/stress_funcs.sh

# Set time limit

timelimit=0

set_timelim_secs()
{
    timelimit=$(($(date +%s) + $1))
}

# Check available devices

devices_all=$(seq 1 64)
devices=

for i in $devices_all; do
    if [[ "$(${fgcethpath}/fgcethterm/Linux/x86_64/fgcethterm -c "$1" "$i" "G TIME.NOW")" =~ .*ERROR.* ]]; then
        echo "$i is offline/unavailable"
    else
        devices+=" $i "
    fi
done 

# Do stress test

conf_ref_table 1000

# Test get time

for i in $devices; do
    test_time $1 $i 64 &
done

wait

for i in $devices; do
    test_float $1 $i 64 &
done

wait

for i in $devices; do
    test_ref_dev_stat $1 $i 64 &
done

wait

set_timelim_secs 30
for i in $(seq 1 32); do
    if [ -z ${devices##* $i *} ]; then
        to_idle $1 $i &
    fi
done
wait

set_timelim_secs 30
for i in $(seq 33 64); do
    if [ -z ${devices##* $i *} ]; then
        to_idle $1 $i &
    fi
done
wait

set_timelim_secs 30
for i in $devices; do
    test_long_ref_set $1 $i 64 &
done
wait

for i in $devices; do
    test_long_ref_get $1 $i 64 &
done
wait

for i in $devices; do
    test_log $1 $i 64 &
done
wait

#EOF
