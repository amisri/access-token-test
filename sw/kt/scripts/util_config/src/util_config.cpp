/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   util_config.cpp
 * @brief  Utility - Loading configuration for the FGC from a file
 * @author Dariusz Zielinski
 */

#include <libfgceth.h>
//#include <../utils/test.h>

#include <regex.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

/**
 * Loads file (file_name) and performs series of set command included in the config file.
 * If print is set to true it make "verbose mode" and progress informations are displayed.
 *
 * Thread: User
 *
 * @param file_name a string containing path to a configuration file
 * @param dev_id an ID of the device
 * @param print if true then verbose mode is on
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
//fgceth_errno loadConfig(const char* file_name, const uint8_t dev_id, const bool print);
fgceth_errno loadConfig(const char* file_name, const uint8_t dev_id, const bool print)
{
    FILE* file;
    char  line[10000] = { 0 };
    char* parsed_line;
    char* prop;
    char* value;

    // Open the file
    file = fopen(file_name, "r");

    // If the file was not opened
    if (file == NULL)
    {
        return FGC_ETH_FILE_ERROR;
    }

    // ***************************************************

    regex_t    regex;
    regmatch_t p_match;

    regcomp(&regex, "^! *[Ss] *", 0);

    // ***************************************************

    char dev_id_str[32];

    sprintf(dev_id_str, "%d", dev_id);

    // Read file line by line
    while (fgets(line, sizeof(line), file))
    {
        fgceth_rsp * cmd_rsp = fgcethCreateResponse();

        // Find ! and S at the beginning of the line
        if (regexec(&regex, line, 1, &p_match, 0))
        {
            fclose(file);
            return FGC_ETH_INVALID_COMMAND;
        }

        // Get rid of ! and S from the beginning of the line
        parsed_line = line + p_match.rm_eo;

        // Get rid of \r\n at the end
        /*uint32_t len = strlen(parsed_line);

        if (parsed_line[len - 1] == '\n')
        {
            parsed_line[len - 1] = '\0';
        }
        else
        if (parsed_line[len - 2] == '\r' && parsed_line[len - 1] == '\n')
        {
            parsed_line[len - 2] = '\0';
        }*/

        // Print info
        if (print)
        {
        	fgcethLog(dev_id, L_V0, "Executing: %s", parsed_line);
        }

        //Remove trailing white space;

        while(isspace(*parsed_line)) parsed_line++;

        // Find propriety

        prop = parsed_line;

        parsed_line = strchr(parsed_line, ' ');
        *parsed_line = '\0';
        parsed_line++;

        while(isspace(*parsed_line)) parsed_line++;

        // Find value

        value = parsed_line;

        while(!isspace(*parsed_line) && (*parsed_line != '\0')) parsed_line++;

        *parsed_line = '\0';

        // Execute set command
        
        fgcethConfigCmd(cmd_rsp, "", SET, dev_id_str, prop, value);

        fgceth_errno error = fgcethGetSet(cmd_rsp);

        // Print result
        if (print)
        {
        	if(error != FGC_SUCCESS)
        	{
        		fgcethErrorLog(dev_id, "--> Result: %s", fgcethStrerr(error));
        	}
        	else
        	{
        		fgcethLog(dev_id, L_V0, "--> Result: %s", fgcethStrerr(error));
        	}
        }

        fgcethDestroyResponse(cmd_rsp);

        // Exit in case of error
        if (error != FGC_SUCCESS)
        {
            fclose(file);
            return error;
        }
    }

    // ***************************************************

    // Close the file
    fclose(file);

    return FGC_SUCCESS;
}


void printConfigUsage()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name -f config_file \n Add -v for verbose mode -r to reset before config\n");
}

int main(int argc, char* argv[])
{
    int     option;
    char    dev_name[128]    = {0};
    int     dev_id           = 0;
    char    names_file[128]  = {0};
    char    gw_name[128]     = {0};
    char    eth_name[128]    = {0};
    char    config_file[512] = {0};
    bool    verbose          = false;
    bool    reset            = false;

    struct fgceth_names_container names_container;

    // Read parameters from the command line
    while ((option = getopt(argc, argv, "i:d:n:g:f:vr")) != -1)
    {
        switch (option)
        {
             case 'i':
                 strcpy(eth_name, optarg);
                 break;
             case 'd':
            	 strcpy(dev_name, optarg);
                 dev_id = atoi(dev_name);
            	 break;
             case 'n':
                 strcpy(names_file, optarg);
                 break;
             case 'g':
                 strcpy(gw_name, optarg);
                 break;
             case 'f':
                 strcpy(config_file, optarg);
                 break;
             case 'v':
                 verbose = true;
                 break;
             case 'r':
				 reset = true;
				 break;
             default :
                 printConfigUsage();
                 exit(1);
        }
    }

    if (verbose)
    {
    	fgcethSetLogPrintFlags(L_V2);
    }
    else
    {
    	fgcethSetLogPrintFlags(L_V0);
    }

    fgcethDetailedLog(true);

    memset(&names_container, 0, sizeof(names_container));

    // ***************************************************

    // Check for required arguments
    if ((dev_name[0] == 0 && dev_id == 0) || eth_name[0] == 0 || names_file[0] == 0 || config_file[0] == 0 || gw_name[0] == 0)
    {
        printConfigUsage();
        exit(1);
    }

    // Check names file
    if(fgcethNamesParser(names_file, gw_name, &names_container) != FGC_SUCCESS)
    {
    	exit(1);
    }

    // ***************************************************

    // Initialize the library.
    // First parameter is a name of a network interface (NIC) connected to FGC3.
    // Second parameter set to 0 will set  default response pool size.
    
    #define MB(x) ((uint32_t) (x) << 20) 
    
    if (fgcethInit(eth_name, &names_container, NULL, MB(256), 0) != FGC_SUCCESS)
    {
        exit(1);
    }

    // ***************************************************
    // get dev_id from device name
	if(dev_id == 0)
	{
		dev_id = fgcethGetDevId(dev_name);
	}

    fgceth_errno err;

    if(reset)
    {
    	fgceth_rsp * cmd_rsp = fgcethCreateResponse();
    	fgcethLog(dev_id, L_V0, "Reseting device...");

    	while (!fgcethIsDeviceReady(dev_id))
		{
			fgcethLog(dev_id, L_V0, "...");
			sleep(2);
		}

        fgcethConfigCmd(cmd_rsp, "", SET, dev_name, "DEVICE.RESET", "");

    	err = fgcethGetSet(cmd_rsp);

    	fgcethDestroyResponse(cmd_rsp);

    	if(err != FGC_SUCCESS)
    	{
    		fgcethLog(dev_id, L_V0, "Command to reset failed. Setting to OFF mode, and reseting again...");

    		cmd_rsp = fgcethCreateResponse();

            fgcethConfigCmd(cmd_rsp, "", SET, dev_name, "MODE.PC", "OFF");

    		err = fgcethGetSet(cmd_rsp);

    		fgcethDestroyResponse(cmd_rsp);

    		if(err != FGC_SUCCESS)
    		{
    			return err;
    		}
    		else
    		{
    			int  count = 0;
    			char v_rsp[32];

                cmd_rsp = fgcethCreateResponse();

                fgcethConfigCmd(cmd_rsp, "", GET, dev_name, "STATE.PC", "");

    			do
    			{
    				sleep(1);

    				err = fgcethGetSet(cmd_rsp);

    				if(err != FGC_SUCCESS || ++count > 10)
    				{
    					exit(1);
    				}

    				blkbufCopyStr(v_rsp, cmd_rsp->d.cmd_rsp.response, sizeof(v_rsp));

    			}
    			while(strcmp(v_rsp,"OFF"));

                fgcethDestroyResponse(cmd_rsp);

    			cmd_rsp = fgcethCreateResponse();

                fgcethConfigCmd(cmd_rsp, "", SET, dev_name, "DEVICE.RESET", "");

                err = fgcethGetSet(cmd_rsp);

                fgcethDestroyResponse(cmd_rsp);

    			if(err != FGC_SUCCESS)
    			{
    				return err;
    			}
    		}
    	}

    	sleep(1);
    }

    // Wait until device is ready and PLL is locked
	fgcethLog(dev_id, L_V0, "Waiting for the device %d to be ready...", dev_id);

	while (!fgcethIsDeviceReady(dev_id))
	{
		fgcethLog(dev_id, L_V0, "...");
		sleep(2);
	}

    // ***************************************************

    fgcethLog(dev_id, L_V0, "Loading config...");

    // ***************************************************

    err = loadConfig(config_file, dev_id, verbose);

    // ***************************************************

    if (err != FGC_SUCCESS)
    {
    	fgcethErrorLog(dev_id, "Result: %s (%d)", fgcethStrerr(err), err);
    }
    else
    {
    	fgcethLog(dev_id, L_V0, "Result: %s (%d)", fgcethStrerr(err), err);
    }

    if(fgcethClose() != FGC_SUCCESS)
    {
    	exit(1);
    }

    if(fgcethFreeContainers(&names_container, NULL) != FGC_SUCCESS)
	{
		exit(1);
	}

    return err;
}
