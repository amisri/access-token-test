/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethPPrinter.cpp
 * @brief  Pretty prints the FGC Ether library log.
 * @author Joao Afonso
 */

#include <libfgceth.h>
#include <libfgceth/private/fgcethLog.h>

#include <stdint.h>
#include <string.h>



/**************************************
 * Colors / formatting
***************************************/

/// Linux console format definitions
#define F_BOLD      1
#define F_DIM       2
#define F_UNDERLINE 4
#define F_REVERSE   7
#define F_HIDDEN    8

/// Linux console colors definitions
#define C_BLACK     30
#define C_RED       31
#define C_GREEN     32
#define C_YELLOW    33
#define C_LBLUE     94
#define C_CYAN      36
#define C_PURPLE    35
#define C_LGREY     37
#define C_WHITE     7

#define STR(input)  #input
#define PP_1(ARG_1)               "\033[" STR(ARG_1) "m"
#define PP_2(ARG_1, ARG_2)        "\033[" STR(ARG_1) ";" STR(ARG_2) "m"
#define PP_3(ARG_1, ARG_2, ARG_3) "\033[" STR(ARG_1) ";" STR(ARG_2) ";" STR(ARG_3) "m"
#define PP_RESET                  "\033[0m"
#define PP_SPACE_24               "                        "
#define PP_SPACE_8                "        "


int main(int argc, char * argv[])
{
	char     date[64];
	char     time[64];
	uint32_t _dev_id;
	char     _dev_name[64], dev_id_name[64];
	char     _msg_type[64], msg_type[64];
	uint32_t milliseconds;
	uint32_t flags = 0;
	char     _msg[LOG_BUFFER_LEN], msg[LOG_BUFFER_LEN];

	char format[128];
	char buffer[LOG_BUFFER_LEN];
	int  len;

	int max_len_id_name = 20;
	int max_len_msg_type = 7;
	int i, j, n, n_inc;

	bool has_ms_flags;
	bool has_name;
	bool has_valid_id;

	snprintf(format, sizeof(format), "%%%lu[^\n]", sizeof(_msg));

	while(fgets(buffer, LOG_BUFFER_LEN, stdin) != NULL)
	{
		n = 0;

		sscanf(buffer,   "%s%n", date, &n_inc);
		n += n_inc;
		sscanf(buffer+n, "%s%n", time, &n_inc);
		n += n_inc;

		if(sscanf(buffer+n, "%u%n", &_dev_id, &n_inc))
		{
			n += n_inc;
			has_valid_id = true;
		}
		else
		{
			has_valid_id = false;
		}

		if(sscanf(buffer+n, "(%[^)])%n", _dev_name, &n_inc))
		{
			n += n_inc;
			has_name = true;
		}
		else
		{
			has_name = false;
		}

		sscanf(buffer+n, "%s : %n", _msg_type, &n_inc);
		n += n_inc;

		if(sscanf(buffer+n, "%u : 0x%x : %n", &milliseconds, &flags, &n_inc) == 2)
		{
			n += n_inc;
			has_ms_flags = true;
		}
		else
		{
			has_ms_flags = false;
			flags = 0;
		}

		sscanf(buffer+n, format, _msg, &n_inc);
		n += n_inc;

		//Highlight the device ID and name
		if(has_valid_id && has_name)
		{
			snprintf(dev_id_name, sizeof(dev_id_name), PP_2(F_BOLD, C_LBLUE) "%u" PP_RESET "(" PP_2(F_BOLD, C_LBLUE) "%s" PP_RESET ")", _dev_id, _dev_name); //Default
			len = 2 + (_dev_id > 9 ? (_dev_id > 99 ? 3 : 2) : 1) + strlen(_dev_name);
			if(max_len_id_name < len) max_len_id_name = len;
		}
		else if(has_valid_id)
		{
			snprintf(dev_id_name, sizeof(dev_id_name), PP_2(F_BOLD, C_LBLUE) "%u" PP_RESET, _dev_id); //Default
			len = (_dev_id > 9 ? (_dev_id > 99 ? 3 : 2) : 1);
			if(max_len_id_name < len) max_len_id_name = len;
		}
		else
		{
			dev_id_name[0] = '\0'; //Default
			len = 0;
		}

		for(i = strlen(dev_id_name), j = 0; j < max_len_id_name - len; j++)
		{
			dev_id_name[i+j] = ' ';
		}
		dev_id_name[i+j] = '\0';

		//Highlight the type of message

		snprintf(msg_type, sizeof(msg_type), PP_RESET "%s", _msg_type); //Default
		len = strlen(_msg_type);
		if(max_len_msg_type < len) max_len_msg_type = len;

		if(flags & L_INIT)
		{
			if(flags & L_ERROR)
			{
				snprintf(msg_type, sizeof(msg_type), PP_3(F_REVERSE, F_BOLD, C_RED) "%s" PP_RESET, _msg_type);
			}
			else if(flags & L_WARNING)
			{
				snprintf(msg_type, sizeof(msg_type), PP_3(F_REVERSE, F_BOLD, C_YELLOW) "%s" PP_RESET, _msg_type);
			}
			else if(flags & L_V0)
			{
				snprintf(msg_type, sizeof(msg_type), PP_2(F_REVERSE, F_BOLD) "%s" PP_RESET, _msg_type);
			}
		}
		else if(flags)
		{
			if(flags & L_ERROR)
			{
				snprintf(msg_type, sizeof(msg_type), PP_2(F_BOLD, C_RED) "%s" PP_RESET, _msg_type);
			}
			else if(flags & L_WARNING)
			{
				snprintf(msg_type, sizeof(msg_type), PP_2(F_BOLD, C_YELLOW) "%s" PP_RESET, _msg_type);
			}
			else if(flags & L_V0)
			{
				snprintf(msg_type, sizeof(msg_type), PP_1(F_BOLD) "%s" PP_RESET, _msg_type);
			}
		}
		else
		{
			if(!strcmp(_msg_type, "ERROR"))
			{
				snprintf(msg_type, sizeof(msg_type), PP_2(F_BOLD, C_RED) "%s" PP_RESET, _msg_type);
			}
			else if(!strcmp(_msg_type, "WARNING"))
			{
				snprintf(msg_type, sizeof(msg_type), PP_2(F_BOLD, C_YELLOW) "%s" PP_RESET, _msg_type);
			}
			else if(!strcmp(_msg_type, "INFO"))
			{
				snprintf(msg_type, sizeof(msg_type), PP_1(F_BOLD) "%s" PP_RESET, _msg_type);
			}
		}

		len = strlen(_msg_type);
		if(max_len_msg_type < len) max_len_msg_type = len;
		for(i = strlen(msg_type), j = 0; j < max_len_msg_type - len; j++)
		{
			msg_type[i+j] = ' ';
		}
		msg_type[i+j] = '\0';

		//Highlight the message

		snprintf(msg, sizeof(msg), PP_RESET "%s", _msg); //Default

		if     (strstr(_msg, "[Cmd SM]") && (flags & L_V3))
		{
			snprintf(msg, sizeof(msg), PP_2(F_BOLD, C_GREEN) "%s" PP_RESET, _msg);
		}
		else if(strstr(_msg, "[Cmd SM Func]") && (flags & L_V3))
		{
			snprintf(msg, sizeof(msg), PP_2(F_BOLD, C_CYAN) "%s" PP_RESET, _msg);
		}
		else if(strstr(_msg, "[Cmd SM Trans]") && (flags & L_V3))
		{
			snprintf(msg, sizeof(msg), PP_2(F_BOLD, C_PURPLE) "%s" PP_RESET, _msg);
		}
		if     (strstr(_msg, "[Rsp SM]") && (flags & L_V3))
		{
			snprintf(msg, sizeof(msg), PP_SPACE_24 PP_2(F_BOLD, C_GREEN) "%s" PP_RESET, _msg);
		}
		else if(strstr(_msg, "[Rsp SM Func]") && (flags & L_V3))
		{
			snprintf(msg, sizeof(msg), PP_SPACE_24 PP_2(F_BOLD, C_CYAN) "%s" PP_RESET, _msg);
		}
		else if(strstr(_msg, "[Rsp SM Trans]") && (flags & L_V3))
		{
			snprintf(msg, sizeof(msg), PP_SPACE_24 PP_2(F_BOLD, C_PURPLE) "%s" PP_RESET, _msg);
		}

		// Print everything!
		if(has_ms_flags)
		{
			printf("%s %s %s %s : %u : 0x%08x : %s\n", date, time, dev_id_name, msg_type, milliseconds, flags, msg);
		}
		else
		{
			printf("%s %s %s %s : %s\n", date, time, dev_id_name, msg_type, msg);
		}
	}
}
