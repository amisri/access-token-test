from enum import Enum, auto
import tango
import sys

class FGCtype(Enum):
    ABSTIME = auto()
    BOOL = auto()
    CHAR = auto()
    DEV_NAME = auto()
    DOUBLE = auto()
    FIFOADC = auto()
    FLOAT = auto()
    FL_POINT = auto()
    INT16S = auto()
    INT16U = auto()
    INT32S = auto()
    INT32U = auto()
    INT8S = auto()
    INT8U = auto()
    LOG = auto()
    LOGEVT = auto()
    LOGPSU = auto()
    LOGTIMING = auto()
    MAC = auto()
    NULL = auto()
    PARENT = auto()
    POINT = auto()
    RUNLOG = auto()
    STRING = auto()
    UNIXTIME = auto()
    #Custom types (non-FGC standard)
    BITMASK = auto()
    ENUM = auto()

globals().update(FGCtype.__members__)

# Access policy

class FGCaccess(Enum):
    GET = auto()
    SET = auto()
    ALL = auto()
    SUB = auto()
    CMD = auto()

globals().update(FGCaccess.__members__)

# Helper functions for special types

# NOTE:
# All FGC to Tango functions must have the following behaviour:
# 1) Empty string - Raises ValueError (except for str to str conversion)
# 2) No argument - returns default value

def _p_to_s(x=''):
    return '|'.join(map(str, x))

def _s_to_p(x=''):
    return list(map(float, x.split('|')))

def _to_b_arr(x=''):
    try:
        return x.encode()
    except AttributeError:
        return b''

def _to_char(x='\0'):
    return x[0]

def _to_none(x):
    return None

def _fr_bmask(x=''):
    return x.split()

def _to_bmask(x=''):
    return ' '.join(map(str, x))

MIN_UINT32 = 0
MAX_UINT32 = 2**32 - 1
MIN_INT32 = -(2**31)
MAX_INT32 = 2**31 - 1
MIN_USHORT = 0
MAX_USHORT = 2**16 - 1
MIN_SHORT = -(2**15)
MAX_SHORT = 2**15 - 1
MIN_UCHAR = 0
MAX_UCHAR = 2**8 - 1
MIN_CHAR = -(2**7)
MAX_CHAR = 2**7 - 1
MIN_FLOAT = -1.0E+30
MAX_FLOAT = 1.0E+30
MIN_DOUBLE = -1.0E+30
MAX_DOUBLE = 1.0E+30

PropertyTypeHandle = {
    ABSTIME   : {
            'tango_type' : tango.DevDouble,
            'length'     : None,
            'to_tango'   : float,
            'to_fgc'     : None,
            'min_val'    : MIN_UINT32,
            'max_val'    : MAX_UINT32,
            'abs_change' : sys.float_info.min,
            },
    BOOL      : {
            'tango_type' : tango.DevBoolean,
            'length'     : None,
            'to_tango'   : bool,
            'to_fgc'     : None,
            'min_val'    : None,
            'max_val'    : None,
            'abs_change' : None,
            },
    CHAR      : {
            'tango_type' : tango.DevUChar,
            'length'     : None,
            'to_tango'   : _to_char,
            'to_fgc'     : None,
            'min_val'    : None,
            'max_val'    : None,
            'abs_change' : None,
            },
    DEV_NAME  : {
            'tango_type' : tango.DevString,
            'length'     : None,
            'to_tango'   : str,
            'to_fgc'     : None,
            'min_val'    : None,
            'max_val'    : None,
            'abs_change' : None,
            },
    DOUBLE    : {
            'tango_type' : tango.DevDouble,
            'length'     : None,
            'to_tango'   : float,
            'to_fgc'     : None,
            'min_val'    : MIN_DOUBLE,
            'max_val'    : MAX_DOUBLE,
            'abs_change' : sys.float_info.min,
            },
    FIFOADC   : {
            'tango_type' : tango.DevEncoded,
            'length'     : None,
            'to_tango'   : _to_b_arr,
            'to_fgc'     : None,
            'min_val'    : None,
            'max_val'    : None,
            'abs_change' : None,
            },
    FLOAT     : {
            'tango_type' : tango.DevFloat,
            'length'     : None,
            'to_tango'   : float,
            'to_fgc'     : None,
            'min_val'    : MIN_FLOAT,
            'max_val'    : MAX_FLOAT,
            'abs_change' : sys.float_info.min,
            },
    FL_POINT  : {
           'tango_type' : tango.DevFloat,
           'length'     : None,
           'to_tango'   : _s_to_p,
           'to_fgc'     : _p_to_s,
           'min_val'    : MIN_FLOAT,
           'max_val'    : MAX_FLOAT,
           'abs_change' : sys.float_info.min,
           },
    INT16S    : {
           'tango_type' : tango.DevShort,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : MIN_SHORT,
           'max_val'    : MAX_SHORT,
           'abs_change' : 1,
           },
    INT16U    : {
           'tango_type' : tango.DevUShort,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : MIN_USHORT,
           'max_val'    : MAX_USHORT,
           'abs_change' : 1,
           },
    INT32S    : {
           'tango_type' : tango.DevLong,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : MIN_INT32,
           'max_val'    : MAX_INT32 ,
           'abs_change' : 1,
           },
    INT32U    : {
           'tango_type' : tango.DevULong,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : MIN_UINT32,
           'max_val'    : MAX_UINT32,
           'abs_change' : 1,
           },
    INT8S     : {
           'tango_type' : tango.DevShort,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : MIN_CHAR,
           'max_val'    : MAX_CHAR,
           'abs_change' : 1,
           },
    INT8U     : {
           'tango_type' : tango.DevUChar,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : MIN_UCHAR,
           'max_val'    : MAX_UCHAR,
           'abs_change' : 1,
           },
    LOG       : {
           'tango_type' : tango.DevEncoded,
           'length'     : None,
           'to_tango'   : _to_b_arr,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    LOGEVT    : {
           'tango_type' : tango.DevEncoded,
           'length'     : None,
           'to_tango'   : _to_b_arr,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    LOGPSU    : {
           'tango_type' : tango.DevEncoded,
           'length'     : None,
           'to_tango'   : _to_b_arr,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    LOGTIMING : {
           'tango_type' : tango.DevEncoded,
           'length'     : None,
           'to_tango'   : _to_b_arr,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    MAC       : {
           'tango_type' : tango.DevEncoded,
           'length'     : None,
           'to_tango'   : _to_b_arr,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    NULL      : {
           'tango_type' : tango.DevVoid,
           'length'     : None,
           'to_tango'   : _to_none,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    PARENT    : {
           'tango_type' : tango.DevString,
           'length'     : None,
           'to_tango'   : str,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    POINT     : {
           'tango_type' : tango.DevFloat,
           'length'     : None,
           'to_tango'   : _s_to_p,
           'to_fgc'     : _p_to_s,
           'min_val'    : MIN_FLOAT,
           'max_val'    : MAX_FLOAT ,
           'abs_change' : sys.float_info.min
           },
    RUNLOG    : {
           'tango_type' : tango.DevEncoded,
           'length'     : None,
           'to_tango'   : _to_b_arr,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    STRING    : {
           'tango_type' : tango.DevString,
           'length'     : None,
           'to_tango'   : str,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    UNIXTIME  : {
           'tango_type' : tango.DevLong,
           'length'     : None,
           'to_tango'   : int,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : 1,
           },
    # Custom types
    BITMASK   : {
           'tango_type' : tango.DevString,
           'length'     : 32,
           'to_tango'   : _fr_bmask,
           'to_fgc'     : _to_bmask,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
    ENUM      : {
           'tango_type' : tango.DevString,
           'length'     : None,
           'to_tango'   : str,
           'to_fgc'     : None,
           'min_val'    : None,
           'max_val'    : None,
           'abs_change' : None,
           },
}



# EOF
