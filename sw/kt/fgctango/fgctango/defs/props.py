import os
import json

#############
# Structure #
#############
#
# 1) Property name
# 2) Property type (see yellow pages)
#    - Should match a tango type
#    - Provide functions to translate types (FGC <> Tango)
#    - Size and min/max can be provided
# 3) Read/write permissions
#    - Should match tango attribute options
# 4) Max size
#    - Used to define attribute maximum array size
# 5) Flags
#    - List of property flags,or None

with open(os.path.join(os.path.dirname(__file__), 'test_prop_info.json')) as json_file:
    test_props = json.loads(json_file.read())

FGC_PROPS = {}
for filename in os.listdir(os.path.dirname(__file__)):
    if filename.startswith("class_"):
        class_id = int(filename[6:8])
        with open(os.path.join(os.path.dirname(__file__), filename)) as json_file:
            FGC_PROPS[class_id] = json.loads(json_file.read())
        FGC_PROPS[class_id].update(test_props)

with open(os.path.join(os.path.dirname(__file__), 'state_dict.json')) as json_file:
    DEFAULT_STATE_DICT = json.loads(json_file.read())



# EOF
