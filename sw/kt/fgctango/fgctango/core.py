"""
fgctango module.
"""

import concurrent.futures
import functools
import inspect
import threading
import time
import traceback
from collections import defaultdict
from datetime import datetime
from typing import Callable

import pyfgc
import tango
import tango.server

import fgctango.defs.types as ft
from fgctango.defs.props import FGC_PROPS
from fgctango.utils import get_rbac

STATE_PC_SUB = 'STATE_PC'
DEFAULT_SUB_PERIOD = 50
SUB_TIMEOUT = 2.0


class SingletonMeta(type):
    """
    Used for singleton creation.
    """
    object_instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.object_instances:
            cls.object_instances[cls] = super(SingletonMeta, cls).__call__(*args, **kwargs)
        return cls.object_instances[cls]


class RtManager(metaclass=SingletonMeta):
    """ Class responsible of sending the RT values to the gateway every 20ms."""

    def __init__(self):
        # print("Initializing RtManager")
        self.rt_session = None
        self.rt_values = {}
        self.rt_values_future = defaultdict(dict)

        self.rt_lock = threading.Lock()
        self.thread = threading.Thread(target=self._rt_thread)

        # self.thread = threading.Thread(target=self._rt_thread_robust)

        # self.thread.daemon = True
        self.thread.start()

    def start(self, name_file):
        if self.rt_session is None:
            self.rt_session = pyfgc.connect_rt(name_file=name_file)

    def set_rt_value(self, fgc, value, apply_time: datetime = None):
        with self.rt_lock:
            if apply_time:
                self.rt_values_future[apply_time][fgc] = value
            else:
                self.rt_values[fgc] = value
        # self.rt_values[fgc] = value
        # self.lock.acquire()
        # self.rt_values[fgc] = value
        # self.lock.release()

    def set_rt_values(self, fgcs, values, apply_time: datetime = None):
        with self.rt_lock:
            if apply_time:
                for fgc, value in zip(fgcs, values):
                    self.rt_values_future[apply_time][fgc] = value
            else:
                for fgc, value in zip(fgcs, values):
                    self.rt_values[fgc] = value

    def _rt_thread(self):
        # align with the 10th millisecond of the cycle
        curr_time = time.time()
        align_sleep = ((20 - (((curr_time * 1000) % 100) % 20)) + 10) / 1000
        time.sleep(align_sleep)

        while True:
            # loop_time = int(time.time() * 1000)

            with self.rt_lock:
                # print(f"[{loop_time}]: Hello from thread")
                # print(f"{loop_time}")
                # sys.stdout.flush()

                if self.rt_values:
                    v = list(self.rt_values.values())[0]
                    self.rt_session.send_refs(self.rt_values)
                    self.rt_values = {}

                for apply_time in sorted(self.rt_values_future):
                    self.rt_session.send_refs(self.rt_values_future[apply_time], apply_time)
                    del self.rt_values_future[apply_time]

            # next_cycle = loop_time + 20
            # sleep_for = (next_cycle - (time.time() * 1000)) / 1000
            sleep_for = ((20 - (((time.time() * 1000) % 100) % 20)) + 10) / 1000
            time.sleep(sleep_for)

    def _rt_thread_robust(self):
        # align with the 14th millisecond of the cycle
        curr_time = time.time()
        align_sleep = (20 - (((curr_time * 1000) % 100) % 20)) + 14
        time.sleep(align_sleep / 1000)

        while True:
            loop_time = int(time.time() * 1000)

            with self.rt_lock:
                print(f"[{loop_time}]: Hello from thread")

            next_cycle = loop_time + 1
            sleep_for = (next_cycle - (time.time() * 1000)) / 1000
            time.sleep(sleep_for)

            loop_time = int(time.time() * 1000)

            with self.rt_lock:
                print(f"[{loop_time}]: Hello again from thread")

            next_cycle = loop_time + 19
            sleep_for = (next_cycle - (time.time() * 1000)) / 1000
            time.sleep(sleep_for)


rt_manager = RtManager()


# TODO: Wrap session connect/disconnect into a context manager function (@context).
#       This will centralize the creation, destruction and reusing of sessions.

def _truncate_str(string, size):
    return (string[:size] + "..") if len(string) > size else string


def _gen_attr_read_method(prop_name, parser, is_array):
    '''Private function to be used only by FGCMeta metaclass!

    Generates a pyTango (attribute) read method.

    Parameters:
        prop_name: FGC property name
        parser:    FGC result parser (from PyFGC to PyTango format)
        is_array:  True if FGC value is an array (comma-separated values)

    Returns:
        Callable: Method wrapping a FGC property GET
    '''

    parser = parser if parser else (lambda x: x)

    def get_property(self):

        ret_value_str = self._get_property(prop_name)

        try:
            ret_value = ([parser(v) for v in ret_value_str.split(',')]
                         if is_array
                         else parser(ret_value_str))
        except ValueError:
            if ret_value_str == "":
                self.warn_stream("Failed to get {} (value not set).".format(prop_name))
                ret_value = [] if is_array else parser()
                ret_quality = tango.AttrQuality.ATTR_INVALID
                ret_time = time.time()
            else:
                raise
        else:
            self.info_stream(f"FGC command GET {prop_name} successful.")
            ret_quality = tango.AttrQuality.ATTR_VALID
            ret_time = time.time()

        ## Run callbacks
        # for cb in self.fgc_property_callbacks.get(prop_name, []):
        #    cb(prop_name, ret_value, ret_time, ret_quality)

        return ret_value, ret_time, ret_quality

    return get_property


def _gen_attr_write_method(prop_name, parser, is_array):
    '''Private function to be used only by FGCMeta metaclass!

    Generates a pyTango (attribute) read method.

    Parameters:
        prop_name: FGC property name
        parser:    FGC value formatter (from PyTango to PyFGC format)
        is_array:  True if FGC value is an array (comma-separated values)

    Returns:
        Callable: Method wrapping a FGC property SET
    '''

    parser = parser if parser else (lambda x: x)

    def set_property(self, value):
        parsed_val = [parser(v) for v in value] if is_array else parser(value)
        self._set_property(prop_name, parsed_val)
        self.info_stream(f"FGC command SET {prop_name} successful")

    return set_property


def _gen_cmd_method(prop_name, parser):
    '''Private function to be used only by FGCMeta metaclass!

    Generates a pyTango (command) method.

    Parameters:
        prop_name: FGC property name
        parser:    FGC value formatter (into pyFGC format)

    Returns:
        Callable: PyTango command wrapping a FGC property SET
    '''

    parser = parser if parser else (lambda x: x)

    def cmd_property(self, value=None):
        parsed_val = parser(value)
        self._set_property(prop_name, parsed_val)
        self.info_stream(f"FGC command {prop_name} successful")

    return cmd_property


def _gen_pub_attr_read_method(pub_field_name):
    '''Private function to be used only by FGCMeta metaclass!

    Generates a pyTango (attribute) read method for a published data field.
    The data on 'self.pub_data_dict_parsed' is already parsed (into a 3-way
    tuple), so no parser is required.

    Parameters:
        pub_field_name: Published data field name

    Returns:
        Callable: Method wrapping the reading of a published data field (cached)
    '''

    def read_pub_field(self):
        with self.pub_data_lock:
            return self.pub_data_dict_parsed[pub_field_name]

    return read_pub_field


def _gen_class_namespace_attributes(namespace):
    '''Private function to be used only by FGCMeta metaclass!

    Returns a namespace (dictionary), containing all the autogenerated attributes
    (and respective read/write methods) requested by the user.
    This data can be used to update a subclass from fgctango.FGCDevice.

    Parameters:
        namespace: New class namespace

    Returns:
        dict: dictionary of new attributes to be added to the class definition
    '''

    # TODO: ?? Define name of 'shared' variables as constants. Use getattr() to
    # obtain them, instead of typing name separatetly.

    namespace_additions = dict()

    # Wrapping FGC properties on Tango attributes

    try:
        prop_to_attr_dict = namespace['FGC_prop_to_attr']
    except KeyError:
        prop_to_attr_dict = {}

    for prop, params in prop_to_attr_dict.items():

        try:
            prop_config = FGC_PROPS[namespace['FGC_class_id']][prop]
        except KeyError as err:
            raise KeyError(f"Property {prop} is unknown.") from err

        # FGC property arguments
        prop_type = prop_config['type']
        prop_access = ft.FGCaccess[prop_config['access']]
        prop_array_len = prop_config.get('length', 1)

        # FGC property type arguments
        type_metadata = ft.PropertyTypeHandle[ft.FGCtype[prop_type]]

        # Tango attribute arguments
        attr_label = params.get('label', None)
        attr_name = params.get('name', None)
        attr_display_level = params.get('display_level', None)
        attr_poll_period = params.get('polling_period', None)

        # [Check] Fail if a SUB property is handler here
        if prop_access is ft.FGCaccess.SUB:
            raise TypeError(f"{prop} is a published data field. "
                            "Should not be handled by 'FGC_prop_to_attr'.")

        # Tango attribute kwargs generation
        attr_kwargs = {}

        ### Attribute must have a name
        attr_kwargs['name'] = attr_name if attr_name is not None else prop

        ### Attribute must have a label and a type
        attr_kwargs['label'] = attr_label if attr_label is not None else prop
        attr_kwargs['dtype'] = type_metadata['tango_type']

        ### Attribute can be OPERATOR or EXPERT
        if attr_display_level is not None:
            attr_kwargs['display_level'] = getattr(tango.DispLevel, attr_display_level)

        ### Attribute can be polled automatically by Tango
        if attr_poll_period is not None:
            attr_kwargs['polling_period'] = attr_poll_period

        ### Attribute can be scalar, array or image; with dimensions on (X,Y)
        if type_metadata['length']:
            attr_kwargs['dformat'] = tango.SPECTRUM if prop_array_len == 1 else tango.IMAGE
            attr_kwargs['max_dim_x'] = type_metadata['length']
            attr_kwargs['max_dim_y'] = 0 if prop_array_len == 1 else prop_array_len
        else:
            attr_kwargs['dformat'] = tango.SCALAR if prop_array_len == 1 else tango.SPECTRUM
            attr_kwargs['max_dim_x'] = prop_array_len

        ### Attribute can have minimum/maximum values
        if type_metadata['min_val'] is not None:
            attr_kwargs['min_value'] = type_metadata['min_val']
        if type_metadata['max_val'] is not None:
            attr_kwargs['max_value'] = type_metadata['max_val']

        ### Attribute absolute change required to trigger change event
        if type_metadata['abs_change'] is not None:
            attr_kwargs['abs_change'] = type_metadata['abs_change']

        ### Attribute can be READ, WRITE or both
        ### Its is necessary to create methods to handle these operations
        if prop_access in (ft.FGCaccess.GET, ft.FGCaccess.ALL):
            r_attr_method = _gen_attr_read_method(prop,
                                                  type_metadata['to_tango'],
                                                  prop_array_len > 1)
            r_attr_method.__name__ = "FGC_GET_{}".format(prop.replace(".", "_"))
            attr_kwargs['fget'] = r_attr_method
            namespace_additions[r_attr_method.__name__] = r_attr_method

        if prop_access in (ft.FGCaccess.SET, ft.FGCaccess.ALL):
            w_attr_method = _gen_attr_write_method(prop,
                                                   type_metadata['to_fgc'],
                                                   prop_array_len > 1)
            w_attr_method.__name__ = "FGC_SET_{}".format(prop.replace(".", "_"))
            attr_kwargs['fset'] = w_attr_method
            namespace_additions[w_attr_method.__name__] = w_attr_method

        ### Attribute access is mapped from FGC access
        access_map = {
            ft.FGCaccess.GET: tango.READ,
            ft.FGCaccess.SET: tango.WRITE,
            ft.FGCaccess.ALL: tango.READ_WRITE,
        }
        attr_kwargs['access'] = access_map[prop_access]

        ### Save new attribute
        new_attr = tango.server.attribute(**attr_kwargs)
        new_attr_name = "FGC_PROP_{}".format(prop.replace(".", "_"))
        namespace_additions[new_attr_name] = new_attr

    return namespace_additions


def _gen_class_namespace_commands(namespace):
    '''Private function to be used only by FGCMeta metaclass!

    Returns a namespace (dictionary), containing all the autogenerated commands
    requested by the user.
    This data can be used to update a subclass from fgctango.FGCDevice.

    Parameters:
        namespace: New class namespace

    Returns:
        dict: dictionary of new attributes to be added to the class definition
    '''

    # TODO: ?? Define name of 'shared' variables as constants. Use getattr() to
    # obtain them, instead of typing name separatetly.

    def _prop_to_cmd_name(prop_name):
        return ''.join(map(lambda x: x.capitalize(), prop_name.replace('.', '_').split('_')))

    namespace_additions = dict()

    # Wrapping FGC properties on Tango attributes

    try:
        prop_to_cmd_dict = namespace['FGC_prop_to_cmd']
    except KeyError:
        prop_to_cmd_dict = {}

    for prop, params in prop_to_cmd_dict.items():

        try:
            prop_config = FGC_PROPS[namespace['FGC_class_id']][prop]
        except KeyError as err:
            raise KeyError(f"Property {prop} is unknown.") from err

        # FGC property arguments
        prop_type = prop_config['type']
        prop_access = ft.FGCaccess[prop_config['access']]
        prop_array_len = prop_config.get('length', 1)

        # FGC property type arguments
        type_metadata = ft.PropertyTypeHandle[ft.FGCtype[prop_type]]

        # Tango attribute arguments
        cmd_doc_in = params.get('doc_in', None)
        cmd_name = params.get('name', None)
        cmd_display_level = params.get('display_level', None)

        # [Check] Fail if a SUB property is handler here
        if prop_access is ft.FGCaccess.SUB:
            raise TypeError(f"{prop} is a published data field. "
                            "Should not be handled by 'FGC_prop_to_cmd'.")

        # New command parameters
        cmd_kwargs = {}
        name = cmd_name if cmd_name is not None else _prop_to_cmd_name(prop)

        ### Type is mandatory
        cmd_kwargs['dtype_in'] = type_metadata['tango_type']

        ### Document inputs
        if cmd_doc_in is not None:
            cmd_kwargs['doc_in'] = cmd_doc_in

        ### Command display level
        if cmd_display_level is not None:
            cmd_kwargs['display_level'] = getattr(tango.DispLevel, cmd_display_level)

        cmd_method = _gen_cmd_method(prop, type_metadata['to_fgc'])
        cmd_method.__name__ = name  # Override name
        new_cmd = tango.server.command(f=cmd_method, **cmd_kwargs)

        ### Save new command
        namespace_additions[new_cmd.__name__] = new_cmd

    return namespace_additions


def _gen_class_namespace_attributes_pub(namespace):
    '''Private function to be used only by FGCMeta metaclass!

    Returns a namespace (dictionary), containing all the autogenerated attributes
    (and respective read/write methods) requested by the user to wrap published
    data.
    This data can be used to update a subclass from fgctango.FGCDevice.

    Parameters:
        namespace: New class namespace

    Returns:
        dict: dictionary of new attributes to be added to the class definition
    '''

    # Used for updating and notifying published data fields exposed as attributes.
    _manual_change_attributes = []

    # TODO: ?? Define name of 'shared' variables as constants. Use getattr() to
    # obtain them, instead of typing name separatetly.

    namespace_additions = dict()

    # Wrapping FGC properties on Tango attributes

    try:
        pub_data_to_attr_dict = namespace['FGC_pubd_to_attr']
    except KeyError:
        pub_data_to_attr_dict = {}

    for pub_data, params in pub_data_to_attr_dict.items():

        try:
            pub_data_config = FGC_PROPS[namespace['FGC_class_id']][pub_data]
        except KeyError as err:
            raise KeyError(f"Data field {pub_data} is unknown.") from err

        # FGC property arguments
        pub_data_type = pub_data_config['type']
        pub_data_access = ft.FGCaccess[pub_data_config['access']]

        # FGC property type arguments
        type_metadata = ft.PropertyTypeHandle[ft.FGCtype[pub_data_type]]

        # Tango attribute arguments
        attr_label = params.get('label', None)
        attr_name = params.get('name', None)
        attr_display_level = params.get('display_level', None)

        # [Check] Fail if a SUB property is handler here
        if pub_data_access is not ft.FGCaccess.SUB:
            raise TypeError(f"{pub_data} is not a published data field, "
                            "therefore should not be handled by 'FGC_pubd_to_attr'.")

        # Tango attribute kwargs generation
        attr_kwargs = {}

        ### Attribute must have a name
        attr_kwargs['name'] = attr_name if attr_name is not None else pub_data

        ### Attribute must have a label and a type
        attr_kwargs['label'] = attr_label if attr_label is not None else pub_data
        attr_kwargs['dtype'] = type_metadata['tango_type']

        ### Attribute can be OPERATOR or EXPERT
        if attr_display_level is not None:
            attr_kwargs['display_level'] = getattr(tango.DispLevel, attr_display_level)

        ### Attribute can be scalar or array
        if type_metadata['length']:
            attr_kwargs['dformat'] = tango.SPECTRUM
            attr_kwargs['max_dim_x'] = type_metadata['length']

        ### Attribute can have minimum/maximum values
        if type_metadata['min_val'] is not None:
            attr_kwargs['min_value'] = type_metadata['min_val']
        if type_metadata['max_val'] is not None:
            attr_kwargs['max_value'] = type_metadata['max_val']

        ### Attribute absolute change required to trigger change event
        if type_metadata['abs_change'] is not None:
            attr_kwargs['abs_change'] = type_metadata['abs_change']

        ### Its is necessary to create methods to read the attribute
        r_attr_method = _gen_pub_attr_read_method(pub_data)
        r_attr_method.__name__ = "FGC_GET_SUB_{}".format(pub_data.replace(".", "_"))
        attr_kwargs['fget'] = r_attr_method
        namespace_additions[r_attr_method.__name__] = r_attr_method

        ### Save new attribute
        new_attr = tango.server.attribute(**attr_kwargs)
        new_attr_name = "FGC_PROP_SUB_{}".format(pub_data.replace(".", "_"))
        namespace_additions[new_attr_name] = new_attr

        ### The base class will use this list to enable manual events (when new
        ### published dat arrives)
        _manual_change_attributes.append((pub_data, attr_kwargs['name']))

    namespace_additions['_manual_change_attributes'] = _manual_change_attributes
    return namespace_additions


class FGCMeta(tango.server.DeviceMeta):
    """Metaclass for generating methods and attributes to communicate with FGCs."""

    def __new__(cls, name, bases, namespace):

        # Check that base class is correct
        if name != "FGCDevice" and FGCDevice not in bases:
            raise TypeError("'FGCDevice' must be used as base class")

        if name != "FGCDevice" and ('FGC_class_id' not in namespace or namespace['FGC_class_id'] == 0):
            namespace['FGC_class_id'] = 63  # default class is 63

        # Generate new namespace values, based on inputs
        namespace_extension = {}
        namespace_extension.update(_gen_class_namespace_attributes(namespace))
        namespace_extension.update(_gen_class_namespace_commands(namespace))
        namespace_extension.update(_gen_class_namespace_attributes_pub(namespace))

        namespace.update(namespace_extension)

        # Create class object
        return super(FGCMeta, cls).__new__(cls, name, bases, namespace)


class FGCDevice(tango.server.Device, metaclass=FGCMeta):
    """Base class for communicating with a FGC using Tango."""

    fgc_callback_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

    fgc = tango.server.device_property(dtype=str)
    namefile_url = tango.server.device_property(dtype=str)
    sub_period = tango.server.device_property(dtype=int, default_value=DEFAULT_SUB_PERIOD)
    sub_timeout = tango.server.device_property(dtype=float, default_value=None)

    # To be filled by metaclass only.
    # Used for updating and notifying published data fields exposed as attributes.
    _manual_change_attributes = []

    # Class config parameters
    FGC_class_id = 63
    FGC_rt = False
    FGC_auto_sub = True
    FGC_prop_to_attr = {}
    FGC_prop_to_cmd = {}
    FGC_pubd_to_attr = {}
    FGC_state_table = {}
    FGC_state_table_default = 'UNKNOWN'

    # RBAC
    fgc_rbac_token = None

    def _get_property(self, prop_name: str):
        """Private method to be used only by FGCDevice base class!

        Reads a property value from the FGC.

        Parameters:
            prop_name: FGC property name

        Returns:
            str: FGC response
        """
        try:
            ret = self.session.get(prop_name)
        except Exception:
            self._session_reconnect()
            ret = self.session.get(prop_name)

        try:
            return ret.value
        except pyfgc.FgcResponseError:
            self.error_stream("Failed to get {} with error {} ({}).".format(
                prop_name, ret.err_code, ret.err_msg.strip()))
            fgc_err_msg = str(ret.err_code) + " " + str(ret.err_msg.strip())
            tango.Except.throw_exception(
                fgc_err_msg,
                "Unable to get FGC property",
                "G {}".format(prop_name))

    def _set_property(self, prop_name: str, value):
        """Private method to be used only by FGCDevice base class!

        Writes a property value to the FGC.

        Parameters:
            prop_name: FGC property name
            value:     Value to send to FGC

        Returns:
            str: FGC response
        """
        try:
            ret = self.session.set(prop_name, value)
        except Exception:
            self._session_reconnect()
            ret = self.session.set(prop_name, value)

        try:
            ret.value
        except pyfgc.FgcResponseError:
            trunc_value = _truncate_str(str(value), 32)
            self.error_stream("Failed to set {} {} with error {} ({}).".format(
                prop_name, trunc_value, ret.err_code, ret.err_msg.strip()))
            fgc_err_msg = str(ret.err_code) + " " + str(ret.err_msg.strip())
            tango.Except.throw_exception(
                fgc_err_msg,
                "Unable to set FGC property",
                "S {} {}".format(prop_name, trunc_value))

    def get_fgc_property(self, prop_name: str):
        """Reads a property value from the FGC.

        Parameters:
            prop_name: FGC property name

        Returns:
            str: FGC response
        """
        return self._get_property(prop_name)

    def set_fgc_property(self, prop_name: str, value):
        """Writes a property value to the FGC.

        Parameters:
            prop_name: FGC property name
            value:     Value to send to FGC

        Returns:
            str: FGC response
        """
        return self._set_property(prop_name, value)

    def set_rt_ref(self, inp):
        apply_time = None
        if len(inp) > 1:
            value, apply_time = inp
            apply_time = datetime.fromtimestamp(apply_time)
        else:
            value = inp[0]
        rt_manager.set_rt_value(self.fgc, value, apply_time)

    def read_pub_data(self):
        """Returns the last published data, as received from the FGC.

        Data is returned as a dictionary, containing the values of all published
        data fields.
        It also returns the time of the last valid publication, as well as the
        quality (INVALID, if there is a timeout.)

        Returns:
            (data_dict, time, quality):
        """
        with self.pub_data_lock:
            return self.pub_data_dict, self.pub_data_time, self.pub_data_qual

    def notify_on_pub_data(self, callback: Callable):
        """Run callback every time that published data is received.

        Callback format must be respected (3 arguments), otherwise it will fail.

        Parameters:
            callback: foo(data_dict, time, quality)

        NOTE: DICTIONARY IS READ-ONLY - DO NOT MODIFY!
        """
        arg_signature = inspect.signature(callback)
        arg_parameters = list(arg_signature.parameters.values())
        if len(arg_parameters) == 3 or \
                (len(arg_parameters) == 1 and \
                 arg_parameters[0].kind == arg_parameters[0].VAR_POSITIONAL):
            self.pub_data_callbacks.add(callback)
        else:
            raise TypeError("Callback must contain 3 positional arguments: "
                            "data_dict, time, quality. "
                            f"Currently it contains {len(arg_parameters)}.")

    def _init_pub_data_fields(self):
        """Private method to be used only by FGCDevice base class!

        Initialize all variables used for published data management.
        """
        parsed_dict_init = dict()
        init_time = time.time()

        for prop_name, _ in self._manual_change_attributes:
            prop = FGC_PROPS[self.FGC_class_id][prop_name]
            prop_metadata = ft.PropertyTypeHandle[ft.FGCtype[prop['type']]]
            parser = prop_metadata['to_tango']
            self.pub_data_parsers[prop_name] = parser
            parsed_dict_init[prop_name] = (parser(),
                                           init_time,
                                           tango.AttrQuality.ATTR_INVALID)

        with self.pub_data_lock:
            self.pub_data_dict_parsed = parsed_dict_init
            self.pub_data_dict = dict()
            self.pub_data_time = init_time
            self.pub_data_qual = tango.AttrQuality.ATTR_INVALID

        self.info_stream("Published data fields initialized.")

        # Enable attribute manual event
        for _, attr_name in self._manual_change_attributes:
            self.set_change_event(attr_name, True, False)

    def _update_pub_data_fields(self, data_dict, data_list, gateway, curr_time):
        """Private method to be used only by FGCDevice base class!

        Update published data fields, and notify Tango.
        """
        if data_dict and data_list:
            # All received values must be parsed to Tango types, accordingly
            fgc_dict = data_dict[self.fgc]
            parsed_dict = dict()
            for prop_name, _ in self._manual_change_attributes:
                try:
                    value = fgc_dict[prop_name]
                except KeyError:
                    try:
                        class_id = fgc_dict['CLASS_ID']
                    except Exception:
                        print(fgc_dict)
                        raise
                    self.warn_stream(f"Published data field {prop_name} not received "
                                     "[class ID #{class_id}].")
                    parsed_value = self.pub_data_parsers[prop_name]()
                    parsed_qual = tango.AttrQuality.ATTR_INVALID
                else:
                    parsed_value = self.pub_data_parsers[prop_name](value)
                    parsed_qual = tango.AttrQuality.ATTR_VALID

                parsed_dict[prop_name] = (parsed_value, curr_time, parsed_qual)

            with self.pub_data_lock:
                self.pub_data_dict_parsed = parsed_dict
                self.pub_data_dict = fgc_dict
                self.pub_data_time = curr_time
                self.pub_data_qual = tango.AttrQuality.ATTR_VALID
            self.info_stream("Published data fields updated.")

        else:
            parsed_dict = dict()
            for prop_name, _ in self._manual_change_attributes:
                prev_value, prev_time, _ = self.pub_data_dict_parsed[prop_name]
                parsed_dict[prop_name] = (prev_value, prev_time, tango.AttrQuality.ATTR_INVALID)

            with self.pub_data_lock:
                self.pub_data_dict_parsed = parsed_dict
                # Original data dictionary and time should remain the same.
                # Just the quality changes.
                self.pub_data_qual = tango.AttrQuality.ATTR_INVALID
            self.warn_stream("Published data fields timed out.")

        for prop_name, attr_name in self._manual_change_attributes:
            value, recv_time, qual = self.pub_data_dict_parsed[prop_name]
            self.push_change_event(attr_name, value, recv_time, qual)

    def _update_state(self, data_dict, _curr_time, quality):
        """Private method to be used only by FGCDevice base class!

        Update state, based on published data fields.
        """
        try:
            fgc_state = data_dict['STATE_PC']
        except KeyError:
            self.warn_stream("FGC state is unknown.")
            self.set_state(getattr(tango.DevState, self.FGC_state_table_default))
            return

        if quality == tango.AttrQuality.ATTR_INVALID:
            self.warn_stream(f"FGC state '{fgc_state}' is invalid (timed out).")
            self.set_state(getattr(tango.DevState, self.FGC_state_table_default))
            return

        try:
            tango_state = self.FGC_state_table[fgc_state]
            self.set_state(getattr(tango.DevState, tango_state))
        except KeyError:
            self.warn_stream(f"FGC state '{fgc_state}' is unknown.")
            self.set_state(getattr(tango.DevState, self.FGC_state_table_default))
            return
        else:
            self.debug_stream(f"FGC state '{fgc_state}' "
                              f"parsed to Tango state '{tango_state}'.")

    def _wrap_callback(self, callb):
        """Private method to be used only by FGCDevice base class!

        Wrap user callback, to log exceptions properly.
        """

        @functools.wraps(callb)
        def _callb(*args, **kwargs):
            try:
                callb(*args, **kwargs)
            except Exception as err:
                self.error_stream("Property callback failed: {}.\n"
                                  "Traceback:\n"
                                  "{}".format(str(err), traceback.format_exc()))

        return _callb

    def _monitor_callback(self, data_dict, data_list, gateway, curr_time):
        """Private method to be used only by FGCDevice base class!

        Callback that caches the receival of new published data and invokes
        user callbacks.

        User callbacks are executed in a separate thread, to avoid clogging the
        FGC monitor.
        """
        if data_dict and data_list:
            self.debug_stream(f"UDP data received at: {curr_time} s.")
        else:
            self.warn_stream(f"UDP data timeout at: {curr_time} s.")

        if self.FGC_auto_sub:
            self._update_pub_data_fields(data_dict, data_list, gateway, curr_time)

        for func in self.pub_data_callbacks:
            # Run callback in separate thread
            self.fgc_callback_executor.submit(
                self._wrap_callback(func),
                self.pub_data_dict,
                self.pub_data_time,
                self.pub_data_qual
            )

    def _monitor_callback_err(self, err, _gateway, _curr_time):
        """Private method to be used only by FGCDevice base class!

        Callback that handles exceptions produced by FGC monitor.
        """
        # TODO: Log exception using python logging module
        self.error_stream("{}\nTraceback:\n{}".format(str(err), traceback.format_exc()))

    def _session_reconnect(self):
        """Private method to be used only by FGCDevice base class!

        Open a new connection to a FGC.
        """
        counter = self.session_reconnect_counter

        with self.session_lock:
            # Counter is used to avoid reconnecting more than necessary
            if self.session_reconnect_counter == counter:
                if self.session:
                    pyfgc.disconnect(self.session)
                try:
                    self.session = pyfgc.connect(self.fgc,
                                                 protocol="sync",
                                                 name_file=self.namefile_url,
                                                 rbac_token=self.fgc_rbac_token)
                except pyfgc.PyFgcError:
                    self.session = None
                self.session_reconnect_counter += 1

    def init_device(self):
        """Overriding init_device from PyTango."""
        super().init_device()

        if self.FGC_rt:
            rt_manager.start(self.namefile_url)
            self.add_command(
                tango.server.command(f=self.set_rt_ref, dtype_in=(float,), doc_in="the RT ref value to be set"))

        # Initialize pub data attributes
        self.pub_data_lock = threading.Lock()
        self.pub_data_dict = None
        self.pub_data_dict_parsed = None
        self.pub_data_time = None
        self.pub_data_qual = None
        self.pub_data_parsers = dict()
        self._init_pub_data_fields()

        # Validate FGC name
        if not self.fgc or not isinstance(self.fgc, str):
            raise ValueError("Tango property 'fgc' is of wrong value '{}'".format(str(self.fgc)))

        # Manage pub data callbacks
        self.pub_data_callbacks = set()

        if self.FGC_state_table:
            self.pub_data_callbacks.add(self._update_state)

        # Manage timeout
        if self.sub_timeout is not None:
            timeout = self.sub_timeout
        else:
            timeout = (self.sub_period / 50) * 2

        # FGC gateway session
        self.session = None
        self.session_reconnect_counter = 0
        self.session_lock = threading.Lock()
        self._session_reconnect()

        # Start monitor
        self.monitor = pyfgc.monitor_session(
            self._monitor_callback,
            {self.fgc},
            self.sub_period,
            timeout=timeout,
            callback_err=self._monitor_callback_err,
            rbac_token=self.fgc_rbac_token
        )
        self.monitor.start()
        self.info_stream("FGC pytango driver for {} has been initialized.".format(self.fgc))

    def delete_device(self):
        """Overriding delete_device from PyTango."""
        try:
            self.session.disconnect()
        except Exception:
            pass
        super().delete_device()


class FGCRtDevice(tango.server.Device):
    name_file_url = tango.server.device_property(dtype=str)

    def init_device(self):
        super().init_device()

        rt_manager.start(self.name_file_url)

        self.fgcs = None
        self.currents = None
        self.apply_time = None

    @tango.server.command(dtype_in=(str,))
    def set_fgcs(self, fgcs):
        self.fgcs = fgcs

    @tango.server.command(dtype_in=(float,))
    def set_currents(self, currents):
        self.currents = currents

    @tango.server.command(dtype_in=float)
    def set_time(self, apply_time):
        self.apply_time = datetime.fromtimestamp(apply_time)

    @tango.server.command
    def set_rt(self):
        if self.fgcs is None:
            raise ValueError("set_fgcs command needs to be called with a list of FGC devices")
        if self.currents is None:
            raise ValueError("set_currents command needs to be called with a list of currents that correspond to the "
                             "FGC devices")
        # if self.apply_time is None:
        #     raise ValueError("set_time command needs to be called with the targeted apply time")

        rt_manager.set_rt_values(self.fgcs, self.currents, self.apply_time)

        self.currents = None
        self.apply_time = None


class FGCRtDsaDevice(tango.server.Device):
    name_file_url = tango.server.device_property(dtype=str)

    def init_device(self):
        super().init_device()

        rt_manager.start(self.name_file_url)

    @tango.server.command(dtype_in=tango.CmdArgType.DevVarDoubleStringArray)
    def set_dsa(self, argin):
        doubles = argin[0]
        apply_time = datetime.fromtimestamp(doubles[0]) if doubles[0] != 0 else None
        currents = doubles[1:]
        fgcs = argin[1]

        rt_manager.set_rt_values(fgcs, currents, apply_time)


### Execute only when RBAC is required (at CERN) ###

def ask_rbac():
    """Read and store the RBAC token. Only necessary at CERN."""
    FGCDevice.fgc_rbac_token = get_rbac()

# EOF
