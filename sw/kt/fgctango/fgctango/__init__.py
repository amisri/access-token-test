"""
This module provides an extension for PyTango [https://pytango.readthedocs.ioi],
to help with the generation of Tango classes that interact with CERN's FGCs.
"""

import logging
from .core import FGCMeta, FGCDevice, ask_rbac, FGCRtDevice, FGCRtDsaDevice
from .defs.props import DEFAULT_STATE_DICT

logging.getLogger(__name__).addHandler(logging.NullHandler())


# EOF
