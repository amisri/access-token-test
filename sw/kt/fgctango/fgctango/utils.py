"""
Some helper functions
"""

import getpass
import pyfgc_rbac


def get_rbac():
    """
    Obtain rbac token.
    First, try to obtain by location, then try by user.
    """
    try:
        return pyfgc_rbac.get_token_location()
    except Exception:
        username = input("User: ") or getpass.getuser()
        return pyfgc_rbac.get_token_login(username, getpass.getpass("Pass: "))



# EOF
