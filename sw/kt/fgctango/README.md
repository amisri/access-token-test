# Introduction

FGCtango is a module that helps with the creation of Tango devices that interact with FGCs.
It is based on PyTango, and supports its original features.

# Install FGCtango (and PyFGC) from source distribution

To install FGCtango, run:
```
pip install fgctango -f <fgctango_dir>/offline_packages
```
This will look into *<fgctango_dir>/offline_packages* for modules not available in the package index.
It is recommended to install these modules into a virtual environment.

NOTE: If installation fails with *requests.exceptions.HTTPError*, try the following variation:
```
pip install fgctango --no-index -f <fgctango_dir>/offline_packages
```
This way, pip will not look into the python package index, which means that some dependencies may need to be installed manually (possibly *pyserial* and *requests*).

# Demonstration

In *<fgctango_dir>/examples/demo/demo.py* there is a demonstration of all features provided by FGCtango.

It contains the definition of several FGCtango classes, each one demonstrating a different fgctango feature.

## Setup demo devices

To setup the Tango database, with one device for each example, run:
```
python <fgctango_dir>/examples/demo/demo_setup.py
```
This file has to be edited with the following information:
* **NAMEFILE_URL** URL of the FGC namefile OR path to the namefile.
* **SUB_PERIOD** Published data period (in multiples of 1/50 seconds).
* **SUB_TIMEOUT** Published data timeout.
* **DEVICES** Replace FGC.NAME.SIMULATION by the name of a FGC in simulation mode.

This will setup a server named *demo/server*, with a device for each example.

## Starting the demo server

As any PyTango server, it can be executed with:
```
python <fgctango_dir>/examples/demo/demo.py server
```

# [Only at CERN] Generate the FGCtango and PyFGC source distribution

These modules may not be available in external labs.
Therefore it is important to provide a way for installing them.

To create the source distribution, run the following commands:
```
cd <fgctango_dir>/offline_packages
./update_packages
```
All of these modules will be stored in *<fgctango_dir>/offline_packages* as tarballs.

