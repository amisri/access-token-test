import setuptools

with open("README.md", "r") as fh:
    description = fh.read()

requirements = {
    'core' : [
        'pyfgc>=1.1',
        'pytango>=9.2',
    ],
    'test' : [
        'pytest',
    ],
    'doc' : [
    ],
}

setuptools.setup(
    name="fgctango",
    version="0.3.1",
    author="Joao Afonso",
    author_email="joao.afonso@cern.ch",
    description="Creation of pytango classes for interacting with CERN FGC devices.",
    long_description=description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/ccs/fgc/tree/master/sw/kt/fgctango",
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=requirements['core'],
    include_package_data=True,
    extra_requires={
        **requirements,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in requirements.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in requirements.values() for req in reqs],
    },
    tests_require=requirements['test'],
)



# EOF
