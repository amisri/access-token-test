#!/bin/sh

fgctango_path="~/projects/fgc/sw/kt/fgctango"
modules_path="~/projects/fgc/sw/clients/python"
modules="pyfgc_const pyfgc_decoders pyfgc_rbac pyfgc_name pyfgc"
offline_pack_dir="~/projects/fgc/sw/kt/fgctango/offline_packages"

for mod in $modules; do
    echo "Archiving $mod..."
    eval cd $modules_path/$mod
    eval python3 setup.py sdist -d $offline_pack_dir
done

eval cd $fgctango_path
eval python3 setup.py sdist -d $offline_pack_dir

# EOF
