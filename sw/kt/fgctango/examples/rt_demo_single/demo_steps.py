"""
This demo client will send a step function on all the devices using the RT channel.
The step function goes from 0 to 2A with a step of 0.1 every 20ms.
"""

import time
from datetime import datetime, timedelta

from tango import DeviceProxy

num_of_steps = 20
max_ref = 2  # A

FGCS = [
    'RFNA.01.KT',
    'RFNA.02.KT',
    'RFNA.03.KT',
    # 'RFNA.04.KT',
    # 'RFNA.05.KT',
    # 'RFNA.06.KT',
    'RFNA.07.KT',
    'RFNA.08.KT',
    'RFNA.09.KT',
    'RFNA.10.KT',
    'RFNA.11.KT',
    'RFNA.12.KT'
]

rt_device = DeviceProxy("rt_demo_single/fgc/RT")

rt_device.set_fgcs(FGCS)  # we can set the list of FGCs only once.


def calculate_ref_for_step(step):
    return (max_ref / num_of_steps) * step


def test_directly():
    """This will send the RT values every 20ms and will apply them instantly on the cycle."""
    # align with the 20ms cycle
    align_sleep = (20 - ((time.time() * 1000) % 20)) / 1000
    time.sleep(align_sleep)

    print("Sending the step function")
    for step in range(1, num_of_steps + 1):
        curr_time = int(time.time() * 1000)
        ref = calculate_ref_for_step(step)
        print(f"{curr_time}: Step: {step} - ref: {ref}")

        currents = [ref] * len(FGCS)  # the currents list should have the same order as the FGCs list
        rt_device.set_fgcs(FGCS)  # this is not necessary if the list of FGCs is the same as before
        rt_device.set_currents(currents)
        rt_device.set_rt()

        # calculate the next cycle
        next_cycle_time = curr_time + 20
        sleep_time = (next_cycle_time - (time.time() * 1000)) / 1000
        time.sleep(sleep_time)

    curr_time = int(time.time() * 1000)
    print(f"{curr_time}: Setting ref to 0")
    currents = [0] * len(FGCS)
    rt_device.set_fgcs(FGCS)
    rt_device.set_currents(currents)
    rt_device.set_rt()


def test_with_apply_time():
    """This will send the RT values 100ms in advance by using the set_time"""
    # align with the 20ms cycle
    align_sleep = (20 - ((time.time() * 1000) % 20)) / 1000
    time.sleep(align_sleep)
    apply_time = datetime.now() + timedelta(milliseconds=100)

    print(f"Sending the steps with start at: {apply_time}")
    for step in range(1, num_of_steps + 1):
        ref = calculate_ref_for_step(step)
        print(f"Step: {step} - ref: {ref}")

        currents = [ref] * len(FGCS)  # the currents list should have the same order as the FGCs list
        rt_device.set_fgcs(FGCS)  # this is not necessary if the list of FGCs is the same as before
        rt_device.set_currents(currents)
        rt_device.set_time(datetime.timestamp(apply_time))
        rt_device.set_rt()

        # calculate the next cycle
        apply_time += timedelta(milliseconds=20)

    print(f"Setting ref to 0")

    currents = [0] * len(FGCS)
    rt_device.set_fgcs(FGCS)
    rt_device.set_currents(currents)
    rt_device.set_time(datetime.timestamp(apply_time))
    rt_device.set_rt()


# test_with_apply_time()
test_directly()
