import tango.server

import fgctango

if __name__ == "__main__":
    # Run server

    tango.server.run([
        fgctango.FGCRtDsaDevice
    ])
