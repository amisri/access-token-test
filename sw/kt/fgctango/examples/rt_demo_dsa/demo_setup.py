import tango

db = tango.Database()
SERVER_NAME = 'rt_demo_dsa/server'

NAMEFILE_URL = '/home/ktfgc/name'

dev_info = tango.DbDevInfo()
dev_info.server = SERVER_NAME
dev_info.name = 'rt_demo_dsa/fgc/RT'
dev_info._class = 'FGCRtDsaDevice'
db.add_device(dev_info)

db.put_device_property(
    dev_info.name,
    {
        'name_file_url': NAMEFILE_URL,
    }
)

# EOF
