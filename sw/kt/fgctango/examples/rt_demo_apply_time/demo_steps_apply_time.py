"""
This demo client will send a step function on all the devices using the RT channel.
The step function goes from 0 to 2A with a step of 0.1 every 20ms.
"""

import time
from datetime import datetime, timedelta

from tango import DeviceProxy

num_of_steps = 20
max_ref = 2 #A

DEVICE_PREFIX = "rt_demo_apply_time/fgc/dev_FGCRt_"


def calculate_ref_for_step(step):
    return (max_ref / num_of_steps) * step


devices = []
print("Getting the devices")
for dongle_id in range(1, 13):
    try:
        dev = DeviceProxy(f"{DEVICE_PREFIX}{dongle_id:02d}")
        devices.append(dev)
    except:
        pass


# align with the 20ms cycle
align_sleep = (20 - (((time.time() * 1000) % 100) % 20)) / 1000
time.sleep(align_sleep)
apply_time = datetime.now() + timedelta(milliseconds=500)

print(f"Sending the steps with start at: {apply_time}")
for step in range(1, num_of_steps+1):
    ref = calculate_ref_for_step(step)
    print(f"Step: {step} - ref: {ref}")

    # send the ref to the devices:
    for dev in devices:
        dev.set_rt_ref((ref, datetime.timestamp(apply_time)))

    # calculate the next cycle
    apply_time += timedelta(milliseconds=20)

print(f"Setting ref to 0")
for dev in devices:
    dev.set_rt_ref((0, datetime.timestamp(apply_time)))
