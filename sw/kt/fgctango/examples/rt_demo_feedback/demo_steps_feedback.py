import time

from tango import DeviceProxy

num_of_steps = 20
max_ref = 2 #A

DEVICE_PREFIX = "rt_demo_fb/fgc/dev_FGCRt_"


def calculate_ref(curr_value):
    return curr_value + (max_ref / num_of_steps)

devices = []
print("Getting the devices")
for dongle_id in range(1, 13):
    try:
        dev = DeviceProxy(f"{DEVICE_PREFIX}{dongle_id:02d}")
        devices.append(dev)
    except:
        pass


# align with the 20ms cycle
align_sleep = (20 - (((time.time() * 1000) % 100) % 20)) / 1000
time.sleep(align_sleep)

print("Sending the step function")
for step in range(num_of_steps):
    curr_time = int(time.time() * 1000)

    print(f"{curr_time}: Step {step}")
    #send the ref to the devices:
    for dev in devices:
        ref = calculate_ref(dev.i_ref)
        print(f"\t{dev.name()}: Current value: {dev.i_ref} - Setting RT ref to {ref}.")
        dev.set_rt_ref((ref, ))

    next_cycle_time = curr_time + 40
    sleep_time = (next_cycle_time - (time.time() * 1000)) / 1000
    time.sleep(sleep_time)

curr_time = int(time.time() * 1000)
print(f"\t{curr_time}: Setting ref to 0")
for dev in devices:
    dev.set_rt_ref((0, ))
