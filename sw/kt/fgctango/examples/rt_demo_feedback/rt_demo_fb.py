import tango.server

import fgctango

class FGCRt(fgctango.FGCDevice):
    """A pytango Device with the support for RT enabled"""

    FGC_rt = True

    FGC_pubd_to_attr = {
        'I_MEAS': {
            'name': 'i_meas'
        },
        'I_REF': {
            'name': 'i_ref'
        }
    }

if __name__ == "__main__":
    # Run server

    tango.server.run([
        FGCRt
    ])
