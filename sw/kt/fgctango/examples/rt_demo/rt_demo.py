import tango.server

import fgctango

class FGCRt(fgctango.FGCDevice):
    """A pytango Device with the support for RT enabled"""

    FGC_rt = True

if __name__ == "__main__":
    # Run server

    tango.server.run([
        FGCRt
    ])
