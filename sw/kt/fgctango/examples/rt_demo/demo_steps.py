"""
This demo client will send a step function on all the devices using the RT channel.
The step function goes from 0 to 2A with a step of 0.1 every 20ms.
"""

import time

from tango import DeviceProxy

num_of_steps = 20
max_ref = 2 #A

DEVICE_PREFIX = "rt_demo/fgc/dev_FGCRt_"


def calculate_ref_for_step(step):
    return (max_ref / num_of_steps) * step


devices = []
print("Getting the devices")
for dongle_id in range(1, 13):
    try:
        dev = DeviceProxy(f"{DEVICE_PREFIX}{dongle_id:02d}")
        devices.append(dev)
    except:
        pass


# align with the 20ms cycle
align_sleep = (20 - (((time.time() * 1000) % 100) % 20)) / 1000
time.sleep(align_sleep)

print("Sending the step function")
for step in range(1, num_of_steps+1):
    curr_time = int(time.time() * 1000)
    ref = calculate_ref_for_step(step)
    print(f"{curr_time}: Step: {step} - ref: {ref}")

    # send the ref to the devices:
    for dev in devices:
        dev.set_rt_ref((ref, ))

    # calculate the next cycle
    next_cycle_time = curr_time + 20
    sleep_time = (next_cycle_time - (time.time() * 1000)) / 1000
    time.sleep(sleep_time)

curr_time = int(time.time() * 1000)
print(f"{curr_time}: Setting ref to 0")
for dev in devices:
    dev.set_rt_ref((0, ))
