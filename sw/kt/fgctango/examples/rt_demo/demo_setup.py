import tango

db = tango.Database()
SERVER_NAME = 'rt_demo/server'

################# MODIFY - BEGIN ###############################################

# Set common properties

NAMEFILE_URL = '/home/ktfgc/name'
SUB_PERIOD = 50
SUB_TIMEOUT = 2.0

# Device specific info

DEVICES = [
    #('device name',                            'device class',               'FGC name'        ),
    ('rt_demo/fgc/dev_FGCRt_01',                'FGCRt',                      'RFNA.01.KT'),
    ('rt_demo/fgc/dev_FGCRt_02',                'FGCRt',                      'RFNA.02.KT'),
    ('rt_demo/fgc/dev_FGCRt_03',                'FGCRt',                      'RFNA.03.KT'),
    #('rt_demo/fgc/dev_FGCRt_04',                'FGCRt',                      'RFNA.04.KT'),
    #('rt_demo/fgc/dev_FGCRt_05',                'FGCRt',                      'RFNA.05.KT'),
    #('rt_demo/fgc/dev_FGCRt_06',                'FGCRt',                      'RFNA.06.KT'),
    ('rt_demo/fgc/dev_FGCRt_07',                'FGCRt',                      'RFNA.07.KT'),
    ('rt_demo/fgc/dev_FGCRt_08',                'FGCRt',                      'RFNA.08.KT'),
    ('rt_demo/fgc/dev_FGCRt_09',                'FGCRt',                      'RFNA.09.KT'),
    ('rt_demo/fgc/dev_FGCRt_10',                'FGCRt',                      'RFNA.10.KT'),
    ('rt_demo/fgc/dev_FGCRt_11',                'FGCRt',                      'RFNA.11.KT'),
    ('rt_demo/fgc/dev_FGCRt_12',                'FGCRt',                      'RFNA.12.KT'),
]

################# MODIFY - END #################################################

for device in DEVICES:

    dev_name, dev_class, fgc_name = device
    print("Creating device: {}".format(dev_name))

    dev_info = tango.DbDevInfo()
    dev_info.server = SERVER_NAME
    dev_info.name = dev_name
    dev_info._class = dev_class
    db.add_device(dev_info)
    db.put_device_property(
        dev_name,
        {
            'fgc'          : fgc_name,
            'namefile_url' : NAMEFILE_URL,
            'sub_period'   : SUB_PERIOD,
            'sub_timeout'  : SUB_TIMEOUT,
        }
    )



# EOF
