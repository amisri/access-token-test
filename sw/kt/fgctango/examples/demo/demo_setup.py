import tango

db = tango.Database()
SERVER_NAME = 'demo/server'

################# MODIFY - BEGIN ###############################################

# Set common properties

#NAMEFILE_URL = 'http://localhost/name'
NAMEFILE_URL = '/opt/fgc/etc/fgcd/name'
SUB_PERIOD = 50
SUB_TIMEOUT = 2.0

# Device specific info

DEVICES = [
    #('device name',                            'device class',               'FGC name'        ),
    ('demo/fgc/dev_FGCDevice',                  'FGCDevice',                  'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCBasic',                   'FGCBasic',                   'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCAttributeAutoGen',        'FGCAttributeAutoGen',        'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCCommandAutoGen',          'FGCCommandAutoGen',          'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCAttributePubDataAutoGen', 'FGCAttributePubDataAutoGen', 'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCAutoGen',                 'FGCAutoGen',                 'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCNewPubDataNotification',  'FGCNewPubDataNotification',  'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCDirectCommandResponse1',  'FGCDirectCommandResponse1',  'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCDirectCommandResponse2',  'FGCDirectCommandResponse2',  'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCDirectReadUdp',           'FGCDirectReadUdp',           'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCDisableAutoSub',          'FGCDisableAutoSub',          'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCStateConverstionDefault', 'FGCStateConverstionDefault', 'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCStateConverstionCustom1', 'FGCStateConverstionCustom1', 'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCStateConverstionCustom2', 'FGCStateConverstionCustom2', 'FGC.NAME.SIMULATION'),
    ('demo/fgc/dev_FGCComplexStateReading',     'FGCComplexStateReading',     'FGC.NAME.SIMULATION'),
]

################# MODIFY - END #################################################

for device in DEVICES:

    dev_name, dev_class, fgc_name = device
    print("Creating device: {}".format(dev_name))

    dev_info = tango.DbDevInfo()
    dev_info.server = SERVER_NAME
    dev_info.name = dev_name
    dev_info._class = dev_class
    db.add_device(dev_info)
    db.put_device_property(
        dev_name,
        {
            'fgc'          : fgc_name,
            'namefile_url' : NAMEFILE_URL,
            'sub_period'   : SUB_PERIOD,
            'sub_timeout'  : SUB_TIMEOUT,
        }
    )



# EOF
