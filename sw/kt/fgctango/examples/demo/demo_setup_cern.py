import tango

db = tango.Database()
SERVER_NAME = 'demo/server'

################# MODIFY - BEGIN ###############################################

# Set common properties

NAMEFILE_URL = 'http://cs-ccr-www1.cern.ch/~pclhc/etc/fgcd/name'
SUB_PERIOD = 50
SUB_TIMEOUT = 2.0

# Device specific info

DEVICES = [
    #('device name',                            'device class',               'FGC name'        ),
    ('demo/fgc/dev_FGCDevice',                  'FGCDevice',                  'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCBasic',                   'FGCBasic',                   'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCAttributeAutoGen',        'FGCAttributeAutoGen',        'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCCommandAutoGen',          'FGCCommandAutoGen',          'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCAttributePubDataAutoGen', 'FGCAttributePubDataAutoGen', 'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCAutoGen',                 'FGCAutoGen',                 'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCNewPubDataNotification',  'FGCNewPubDataNotification',  'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCDirectCommandResponse1',  'FGCDirectCommandResponse1',  'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCDirectCommandResponse2',  'FGCDirectCommandResponse2',  'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCDirectReadUdp',           'FGCDirectReadUdp',           'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCDisableAutoSub',          'FGCDisableAutoSub',          'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCStateConverstionDefault', 'FGCStateConverstionDefault', 'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCStateConverstionCustom1', 'FGCStateConverstionCustom1', 'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCStateConverstionCustom2', 'FGCStateConverstionCustom2', 'RFNA.866.04.ETH1'),
    ('demo/fgc/dev_FGCComplexStateReading',     'FGCComplexStateReading',     'RFNA.866.04.ETH1'),
]

################# MODIFY - END #################################################

for device in DEVICES:

    dev_name, dev_class, fgc_name = device
    print("Creating device: {}".format(dev_name))

    dev_info = tango.DbDevInfo()
    dev_info.server = SERVER_NAME
    dev_info.name = dev_name
    dev_info._class = dev_class
    db.add_device(dev_info)
    db.put_device_property(
        dev_name,
        {
            'fgc'          : fgc_name,
            'namefile_url' : NAMEFILE_URL,
            'sub_period'   : SUB_PERIOD,
            'sub_timeout'  : SUB_TIMEOUT,
        }
    )



# EOF
