# Configure tango database

Run:
```
python demo_setup.py
```

# Run server

Run:
```
python demo.py server
```
