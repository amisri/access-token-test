import os
import sys
import time
import threading
import tango
import tango.server

from tango.server import command
import fgctango


"""
################################
#    FGCTango demonstration    #
################################

### Introduction

This module provides an extension to the PyTango High Level server API (see
https://pytango.readthedocs.io/en/stable/server_api/server.html), without
obfuscating any of its features.

All new FGC classes must inherit from fgctango.FGCDevice. The FGCDevice class is
already a subclass from tango.server.Device, therefore it inherits its behaviour.

Each instance of FGCDevice is intended to communicate with a single FGC.

### Main features

In general, this module is composed of 4 parts:

  1) Auto-generation of Tango attributes/commands.
    * Users can define in a class attribute (dictionary):
      - Which FGC properties are exposed as Tango attributes.
      - Which FGC properties are exposed as Tango commands.
      - Which FGC published data fields are exposed as Tango attributes.
    * Then, when the new class object is created, the FGCMeta metaclass
    will automatically generate the class (Tango) attributes and methods
    required to get/set these FGC properties. The type conversion is handled
    automatically.
    * Users have some level of control on how these properties are exposed
    in tango (i.e. name, label, polling period, etc.).

  2) New published data notification.
    * Users can be notified when the FGC Tango device receives a new published
    data packet.
    * Callbacks should be used to handle the new data.

  3.1) Methods for directly getting/setting FGC properties.
    * Users can directly get/set any FGC property using methods provided by
    FGCDevice.
    * These methods will interact directly with the FGCs, blocking until a
    response is obtained.

  3.2) Method for reading the FGC published data.
    * Users can read the latest FGC published data fields.
    * This method uses cached data (updated in thew background when a new data
    packet arrives). Therefore it is non-blocking.

  4) FGC state to Tango state translation.
    * Optionally, users can map how FGC states are converted into Tango states.
    * This is achieved through the usage of a dicitonary.

### Important tips

Run all FGC Tango devices in the same Tango server.
This way, they will be able to share resources when communicating through a
common FGC gateway.

### Contact

If any bugs are detected, please contact TE-EPC-CCS @ CERN.

"""


class FGCBasic(fgctango.FGCDevice):
    """
    DEMO:
    FGCDevice class

    FGCDevice is a subclass of pytango Device. Therefore it supports PyTango
    attributes and commands, including Init, State and Status.
    """
    pass



class FGCAttributeAutoGen(fgctango.FGCDevice):
    """
    DEMO:
    Auto generation of Tango attributes

    FGC_prop_to_attr allows the developer to select with FGC properties are
    exposed as Tango attributes.
    The properties must be defined as dictionary keys.
    The dictionary values may be 'None', or another dicitonary containing the
    Tango attribute keyword arguments.

    Tango attribute arguments supported:
    * polling_period
    * name
    * label
    * display_level
    *** More may be added in future ***

    If no attribute arguments are given, the default (pytango) arguments will be used.
    The default name is the name of the FGC property.
    """

    FGC_prop_to_attr = {
        'STATE.PC' : {}, # Will use default attribute arguments.
        'TEST.FLOAT' : {
            'polling_period': 1000,
            'name'          : 'prop_float',
            'label'         : 'Test FGC float_t',
            'display_level' : 'OPERATOR',
            },
        'TEST.INT32S' : {
            'name'          : 'prop_int32s',
            'label'         : 'Test FGC int32s_t',
            'display_level' : 'EXPERT',
            },
        'TEST.INT32U' : {
            'name'          : 'prop_int32u',
            'label'         : 'Test FGC int32u_t',
            'display_level' : 'OPERATOR',
            },
    }



class FGCCommandAutoGen(fgctango.FGCDevice):
    """
    DEMO:
    Auto generation of Tango commands

    FGC_prop_to_cmd allows the developer to select with FGC properties are
    exposed as Tango commands.

    Tango command arguments supported:
    * name (name of the command)
    * doc_in
    * display_level
    *** More may be added in future ***

    If no command arguments are given, the default (pytango) arguments will be used.
    The default name is the name of the FGC property.
    """

    FGC_prop_to_cmd = {
        'STATE.PC' : {}, # Will use default command arguments.
        'TEST.FLOAT' : {
            'name'          : 'SetFloat',
            'doc_in'        : 'Set FGC float_t',
            'display_level' : 'OPERATOR',    # String can be used for enums value.
            },
        'TEST.INT32S' : {
            'name'          : 'SetInt',
            'doc_in'        : 'Set FGC int32s_t',
            'display_level' : 'OPERATOR',
            },
    }



class FGCAttributePubDataAutoGen(fgctango.FGCDevice):
    """
    DEMO:
    Auto generation of Tango attributes (for FGC published data)

    FGC_pubd_to_attr allows the developer to select with FGC published data
    fields are exposed as Tango attributes.
    These are special properties, received periodically by the Tango device and
    cached internally. i
    ** Therefore, there is no overhead on the FGC when reading them **.
    All are read-only.

    Tango attribute arguments supported:
    * name
    * label
    * display_level
    *** More may be added in future ***

    If no attribute arguments are given, the default (pytango) arguments will be used.
    The default name is the name of the FGC property.
    """

    FGC_pubd_to_attr = {
        'STATE_PC' : {}, # Will use default attribute arguments.
        'ST_LATCHED' : {
            'name'          : 'StLatched',
            'label'         : 'Latched Status',
            'display_level' : 'OPERATOR',    # String can be used for enums value.
            },
        'ST_UNLATCHED' : {
            'name'          : 'StUnlatched',
            'label'         : 'Unlatched Status',
            'display_level' : 'EXPERT',
            },
    }



class FGCAutoGen(fgctango.FGCDevice):
    """
    DEMO:
    Usage of FGC_pubd_to_attr, FGC_prop_to_attr and FGC_prop_to_cmd in same class.

    All of them can be combined in the same class.
    """

    FGC_prop_to_attr = {
        'TEST.FLOAT' : {},
        'TEST.INT32S' : {},
        'TEST.INT32U' : {},
    }

    FGC_prop_to_cmd = {
        'MODE.PC' : {},
        'TEST.FLOAT' : {
            'name' : 'SetFloat',
            },
    }

    FGC_pubd_to_attr = {
        'STATE_PC' : {},
    }



class FGCNewPubDataNotification(fgctango.FGCDevice):
    """
    DEMO:
    Be notified when new published data is received.

    It is possible to get a notification when new UDP data arrives.
    This is done through the usage of a callback, registered with the
    'self.run_on_pub_data_update' method from FGCDevice.

    This callback must contain 4 parameters:
    * self     - reference to FGCDevice object.
    * pub_data - dictionary containing published data (will be None if there is
    timeout.
    * time     - data receive time.
    * quality  - quality of the new data. If there is a timeout, it will be
    INVALID.

    NOTE:
    The timeout is, by default, twice the published data refresh rate.
    It can be modified through the device property 'sub_timeout' on the Tango
    database.
    """
    def callback(self, pub_data, time, quality):
        if quality == tango.AttrQuality.ATTR_VALID:
            state_pc = pub_data["STATE_PC"]
            if state_pc == "OFF":
                self.set_state(tango.DevState.OFF)
            elif state_pc == "FLT_OFF":
                self.set_state(tango.DevState.FAULT)
            elif state_pc in ("DIRECT","IDLE","CYCLING"):
                self.set_state(tango.DevState.ON)
            else:
                self.set_state(tango.DevState.UNKNOWN)

        state_pc = pub_data.get("STATE_PC", "Unknown")
        state_op = pub_data.get("STATE_OP", "Unknown")
        state_vs = pub_data.get("STATE_VS", "Unknown")
        state_pll = pub_data.get("STATE_PLL", "Unknown")

        # There may be a bug in pytango:
        # append_status() 'new_line' parameter fails with exception!
        # This was solved with the explicit usage of '\n'.

        self.set_status("FGC status:\n")
        self.append_status(f"State PC  : {state_pc}\n")
        self.append_status(f"State OP  : {state_op}\n")
        self.append_status(f"State VS  : {state_vs}\n")
        self.append_status(f"State PLL : {state_pll}\n")

    def init_device(self):
        super().init_device()
        self.notify_on_pub_data(self.callback)



class FGCDirectCommandResponse1(fgctango.FGCDevice):
    """
    DEMO:
    Straighforward communication with FGCs (1).

    It is possiblie to send commands directly to the FGC. These commands will be
    handled by PyFGC, with no parsing involved.

    Set command:
    'self.set_fgc_property(name, value)'
        Arguments:
        * name  - FGC property name
        * value - Setting value
        Return value:
        * <string> - Response, in the form of string. Should be an empty string.
        An error response will be thrown as an exception.

    Get command:
    'self.get_fgc_property(name)'
        Arguments:
        * name  - FGC property name
        Return value:
        * <string> - Response, in the form of string. An error response will be
        thrown as an exception.

    NOTE: All valid responses are returned as strings! Therefore it is up to
    the user to parse them correctly.
    This approach is preferred (instead to casting every response to its
    respective type) because it guarantees that all invocations of these methods
    behave consistently.
    """

    float_1x = tango.server.attribute(
            dtype=float,
            access=tango.READ_WRITE,
            )
    float_2x = tango.server.attribute(
            dtype=float,
            access=tango.READ_WRITE,
            )

    # Read/write methods for float_1x

    def read_float_1x(self):
        val_str = self.get_fgc_property('TEST.FLOAT')
        val = val_str.split(',')[0]
        try:
            # In this example, result has to be converted to float
            return float(val)
        except ValueError:
            # If no value is defined (empty string returned) the conversion will
            # raise ValueError.
            return 0, time.time(), tango.AttrQuality.ATTR_INVALID

    def write_float_1x(self, value):
        self.set_fgc_property('TEST.FLOAT', value)

    # Read/write methods for float_2x

    def read_float_2x(self):
        val_str = self.get_fgc_property('TEST.FLOAT')
        val = val_str.split(',')[0]
        try:
            return float(val) * 2
        except ValueError:
            return 0, time.time(), tango.AttrQuality.ATTR_INVALID

    def write_float_2x(self, value):
        self.set_fgc_property('TEST.FLOAT', value / 2.0)

    # These methods can also be used in commands

    @tango.server.command(dtype_in=float)
    def SetFloat(self, value):
        self.set_fgc_property('TEST.FLOAT', value)



class FGCDirectCommandResponse2(fgctango.FGCDevice):
    """
    DEMO:
    Straighforward communication with FGCs (2).

    In this example we setup a few simple tango commands for the operators
    (ON/OFF), that hide a more complex FGC.

    ** Read example 'FGCDirectCommandResponse1' for more info **
    """

    FGC_pubd_to_attr = {
        'STATE_PC' : {}, # Just to get feedback on current FGC state
    }

    @tango.server.command
    def TurnOn(self):
        self.set_fgc_property('MODE.PC_SIMPLIFIED', 'ON')

    @tango.server.command
    def TurnOff(self):
        self.set_fgc_property('MODE.PC_SIMPLIFIED', 'OFF')



class FGCDirectReadUdp(fgctango.FGCDevice):
    """
    DEMO:
    Read a cached published data field.

    Contrary to FGC properties, the reading of published data uses cached values,
    pushed directly by the FGC gateway to the clients (using UDP).
    Since this has no direct impact on FGC performance, it should be preferred
    instead of the command/response protocol (if property is published).

    'self.read_pub_data()'
        Return value (3-way tuple):
        * <string>, - Dictionary, containing all of the most recent published
        data fields.
        * <float>   - Time stamp of last data received.
        * <quality> - Quality of the data. Will be turned to invalid after
        timeout has passed.

    Note: Each fields of the dictonary can have a different type, depending on
    the corresponding FGC property type.
    """

    fgc_state = tango.server.attribute(
            dtype=str,
            )

    def read_fgc_state(self):
        pub_dict, timestamp, quality = self.read_pub_data()
        return pub_dict['STATE_PC'], timestamp, quality

    @tango.server.attribute
    def i_ref(self):
        pub_dict, timestamp, quality = self.read_pub_data()
        return pub_dict['I_REF'], timestamp, quality

    @tango.server.attribute
    def i_meas(self):
        pub_dict, timestamp, quality = self.read_pub_data()
        return pub_dict['I_MEAS'], timestamp, quality

    @tango.server.attribute
    def v_ref(self):
        pub_dict, timestamp, quality = self.read_pub_data()
        return pub_dict['V_REF'], timestamp, quality

    @tango.server.attribute
    def v_meas(self):
        pub_dict, timestamp, quality = self.read_pub_data()
        return pub_dict['V_MEAS'], timestamp, quality



class FGCDisableAutoSub(fgctango.FGCDevice):
    """
    DEMO:
    Disable published data receival.

    It is possible to disable the subscription of published data, with is done
    by default.
    This will cause 'self.read_pub_data()' to thrown an exception, and callbacks
    will not be run.
    """
    FGC_auto_sub = False

    @tango.server.attribute(dtype=str)
    def is_invalid(self):
        pub_dict, timestamp, quality = self.read_pub_data()
        return "Ooops", timestamp, quality



class FGCStateConverstionDefault(fgctango.FGCDevice):
    """
    DEMO:
    Automatically parse FGC state from published data.

    'FGC_state_table'
    It is possible to handle the conversion from FGC states to Tango states.
    To apply the default conversion use fgctango.DEFAULT_STATE_TBL.
    By default, no conversion is made.
    """
    FGC_state_table = fgctango.DEFAULT_STATE_DICT



class FGCStateConverstionCustom1(fgctango.FGCDevice):
    """
    DEMO:
    Setup dictionary for parsing FGC states.

    'FGC_state_table'
    It is possible to handle the conversion from FGC states to Tango states.
    To apply a custom conversion use a dictionary.
    Missing keys will be tagged as UNKNOWN, by default.
    """
    FGC_state_table = {
            'FLT_OFF' : 'OFF',
            'OFF'     : 'OFF',
            'IDLE'    : 'ON',
            'DIRECT'  : 'RUNNING',
            }



class FGCStateConverstionCustom2(fgctango.FGCDevice):
    """
    DEMO:
    Setup dictionary for parsing FGC states.
    Define default falue for unknown states.

    'FGC_state_table'
    'FGC_state_table_default'
    The default state can also be customized.
    """
    FGC_state_table_default = "FAULT"
    FGC_state_table = {
            'FLT_OFF' : 'OFF',
            'OFF'     : 'OFF',
            'IDLE'    : 'ON',
            'DIRECT'  : 'RUNNING',
            }



class FGCComplexStateReading(fgctango.FGCDevice):
    """
    DEMO:
    Define state, using a combination of published data and FGC command/response
    protocol.

    Usefull when published data is not enough.
    """

    FGC_pubd_to_attr = {
        'STATE_PC' : {}, # Just to get feedback on current FGC state
    }

    FGC_prop_to_attr = {
        'TEST.FLOAT' : {}, # To manipulate test.float value
        'MODE.PC'    : {}, # To manipulate current state
    }

    def periodic_foo(self):

        while True:
            # Execute periodically, every 2 seconds
            time.sleep(2)

            # Be carefull when polling FGC data. Every get/set command
            # blocks the FGC for a short period of time.
            val_str = self.get_fgc_property('TEST.FLOAT')
            val = val_str.split(',')[0]

            # If TEST.FLOAT is negative (val < 0), set Tango state to FAULT.
            try:
                if float(val) < 0:
                    self.set_state(tango.DevState.FAULT)
                    continue
            except ValueError:
                self.set_state(tango.DevState.UNKNOWN)
                continue

            # Otherwise, define Tango state based on FGC state.

            if self.state_pc_val in ("OFF", "FLT_OFF"):
                self.set_state(tango.DevState.OFF)
            elif self.state_pc_val in ("DIRECT","IDLE","CYCLING"):
                self.set_state(tango.DevState.ON)
            else:
                self.set_state(tango.DevState.UNKNOWN)

    def new_data_callback(self, pub_data, time, quality):
        # Simply store STATE_PC value locally.
        if quality == tango.AttrQuality.ATTR_VALID:
            self.state_pc_val = pub_data["STATE_PC"]
        else:
            self.state_pc_val = None

    def init_device(self):
        super().init_device()
        self.state_pc_val = None
        self.notify_on_pub_data(self.new_data_callback)

        t = threading.Thread(target=self.periodic_foo)
        t.start()


class FGCBasicClass62(fgctango.FGCDevice):
    """
    DEMO:
    Define a different class that the default(class 63).
    """
    FGC_class_id = 62



if __name__ == "__main__":

    # This in only necessary at CERN

    try:
        import rbac_cern
    except ModuleNotFoundError:
        pass

    # Run server

    tango.server.run([
        fgctango.FGCDevice,
        FGCBasic,
        FGCAttributeAutoGen,
        FGCCommandAutoGen,
        FGCAttributePubDataAutoGen,
        FGCAutoGen,
        FGCNewPubDataNotification,
        FGCDirectCommandResponse1,
        FGCDirectCommandResponse2,
        FGCDirectReadUdp,
        FGCDisableAutoSub,
        FGCStateConverstionDefault,
        FGCStateConverstionCustom1,
        FGCStateConverstionCustom2,
        FGCComplexStateReading,
    ])



# EOF
