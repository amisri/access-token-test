import time
import tango
import sys


class EventManager():

    def __init__(self, dp, attr):
        self._deviceProxy = dp
        if dp is not None:
            print("Subscribed to Interface Change Events")
            self._event_id = dp.subscribe_event(
                    attr,
                    tango.EventType.CHANGE_EVENT,
                    self
                )

    def unsubscribe(self):
        self._deviceProxy.unsubscribe_event(self._event_id)

    def push_event(self, ev):
        print("Event -----push_event-----------------")
        print("Timestamp:      ", ev.reception_date)
        print("Event name:     ", ev.attr_name)
        print("Event type:     ", ev.event)
        print("Device server:  ", ev.device)
        print("Event error:    ", ev.err)
        if ev.err:
            print("Caught pipe exception")
            err = ev.errors[0]
            print("Error desc:     ", err.desc)
            print("Error origin:   ", err.origin)
            print("Error reason:   ", err.reason)
            print("Error severity: ", err.severity)
        else:
            print("Event value:    ", ev.attr_value)


def main():
    dev = tango.DeviceProxy(sys.argv[1])
    EventManager(dev, sys.argv[2])
    time.sleep(3000.0)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("python {} <tango_device_name> <tango_attribute>".format(__file__))
    else:
        main()



# EOF
