#!/bin/bash
tar -C ../.. -czvf fgctango_dev_$(date +%s).tar.gz --exclude="scripts" --exclude=".[^/]*" --exclude="__*__" --exclude="tests" --exclude="*egg-info" --exclude="demo*cern*" --exclude="create_tar.sh" --exclude="fgctango_dev*.tar.gz" --exclude="offline_packages/update_packages.sh" ./fgctango -v
