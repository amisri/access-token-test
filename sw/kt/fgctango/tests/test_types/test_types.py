import os
import sys
import tango
import tango.server



class TyperTest(tango.server.Device):
    FGC_class_id = 63
    FGC_prop_filter = []
    FGC_state_table = None

    str_input = tango.server.attribute(
        dtype=tango.DevString,
        access=tango.READ_WRITE,
    )

    str_input_type = tango.server.attribute(
        dtype=tango.DevString,
    )

    str_to_float = tango.server.attribute(
        dtype=tango.DevFloat,
    )

    str_to_double = tango.server.attribute(
        dtype=tango.DevDouble,
    )

    str_to_short = tango.server.attribute(
        dtype=tango.DevShort,
    )

    str_to_long = tango.server.attribute(
        dtype=tango.DevLong,
    )

    str_to_ushort = tango.server.attribute(
        dtype=tango.DevUShort,
    )

    str_to_ulong = tango.server.attribute(
        dtype=tango.DevULong,
    )

    def init_device(self):
        self.string = ""

    # Setting string to be translated into types

    def read_str_input(self):
        return self.string

    def write_str_input(self, value):
        self.string = value
        self.set_status("String [{}] has been inserted.".format(value))

    def read_str_input_type(self):
        return type(self.string).__name__

    # Reading string value ass different tango types

    def read_str_to_float(self):
        return self.string

    def read_str_to_double(self):
        return self.string

    def read_str_to_short(self):
        return self.string

    def read_str_to_long(self):
        return self.string

    def read_str_to_ushort(self):
        return self.string

    def read_str_to_ulong(self):
        return self.string



if __name__ == "__main__":

    tango.server.run([
            TyperTest,
            ])



# EOF
