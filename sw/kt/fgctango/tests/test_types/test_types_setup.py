import tango

db = tango.Database()
SERVER_NAME = 'test_types/server'

# Remove previous instance
print("Cleaning previous server data on {}.".format(SERVER_NAME))
try:
    db.delete_server(SERVER_NAME)
except tango.DevFailed as e:
    print("ERROR: Cleaning failed with: {}.".format(str(e)))

# Device specific info

DEVICES = [
        ('test_types/fgc/test_types_TyperTest', 'TyperTest'),
    ]

for device in DEVICES:

    dev_name, dev_class = device
    print("Creating device: {}".format(dev_name))

    dev_info = tango.DbDevInfo()
    dev_info.server = SERVER_NAME
    dev_info.name = dev_name
    dev_info._class = dev_class
    db.add_device(dev_info)
    db.put_device_property(
        dev_name,
        {
        }
    )



# EOF
