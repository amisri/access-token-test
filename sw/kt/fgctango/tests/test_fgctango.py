
import pyfgc
import pyname
import pytest
import os
import json
import sys
import threading
import enum
import time
import fgctango

def test_set_get(open_pyfgc):
    dev = open_pyfgc

    inp_val = 1.234

    pyfgc.set('TEST.FLOAT', inp_val, devices=[dev['FGC_OK']])
    ret_val = float(pyfgc.get('TEST.FLOAT', devices=[dev['FGC_OK']]).value)

    device = fgctango.FGC63()

    assert inp_val == pytest.approx(ret_val)
