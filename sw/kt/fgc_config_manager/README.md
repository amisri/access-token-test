- Initial setup
The installation script install.sh takes care of the setup required for the FGC Logger, this includes setting up the virtual environment, the configuration and the systemd service.

To execute it simply run ./install.sh and follow the instructions.

NOTE: Please don't forget to manually update the list_gateways.txt file with the list of the gateways that will use the FGC Logger.
