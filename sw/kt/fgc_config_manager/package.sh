#!/bin/bash

project_name="fgc_config_manager"
project_base_path="$(pwd)/../../clients/python/"
offline_packages_dir="$(pwd)/offline_packages"
destination=$(pwd)

client_modules_path="$(pwd)/../../clients/python"
client_modules="pyfgc_statussrv pyfgc_const pyfgc_decoders pyfgc_rbac pyfgc_name pyfgc pyfgc_db fgc_config_manager"

main() {
    generate_offline_packages
    #get_packages_from_accpy
    package
}

generate_offline_packages() {
    echo "Generating offline packages..."
    if [[ -d $offline_packages_dir ]]; then
        rm $offline_packages_dir/*
    else
        mkdir $offline_packages_dir
    fi

    for mod in $client_modules; do
        echo "    Archiving $mod..."
        cd $client_modules_path/$mod
        python3 setup.py -q sdist -d $offline_packages_dir
    done

}

get_packages_from_accpy() {
    source /acc/local/share/python/acc-py/pro/setup.sh
    echo "Getting packages from accpy..."
    if [[ -d $offline_packages_dir ]]; then
        rm $offline_packages_dir/*
    else
        mkdir $offline_packages_dir
    fi

    for mod in $client_modules; do
        echo "    Getting $mod..."
        pip download --no-deps $mod -d $offline_packages_dir
    done
}

package_project() {
    mkdir $pkg/$project_name
    cp requirements.txt $pkg/$project_name
    echo "fgc_config_manager" >> $pkg/$project_name/requirements.txt
    cp $project_base_path/$project_name/$project_name/templates/launcher_example.sh.template $pkg/$project_name/launcher.sh
    cp $project_base_path/$project_name/$project_name/templates/config_example.cfg.template $pkg/$project_name/config.cfg
}

package() {
    echo "Packaging..."
    cd ${destination}
    pkg=${project_name}_$(date +%s)
    mkdir $pkg
    package_project
    cp -r "offline_packages" "install.sh" "README.md" $pkg
    tar czvf ${pkg}.tar.gz $pkg
    rm -r $pkg
}

main
