#!/bin/bash

project_name="fgc_config_manager"

centos_dependencies="python3 python3-devel"


main() {
    install_packages_if_needed
    init_installation_folder
    create_virtual_environment
    source_virtual_environment
    install_dependencies
    configure_kt
    configure_db
    generate_systemd_service
}

output() {
    END="\e[0m"
    echo -e "$1$2$END"
}

output_in_red() {
    RED="\e[01;31m"
    output $RED "$1"
}

output_in_yellow() {
    YELLOW="\e[01;33m"
    output $YELLOW "$1"
}

confirm() {
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        [nN][oO]|[nN])
            false
            ;;
        *)
            $2
            ;;
    esac
}

install_packages_if_needed() {
    echo "Checking if needed packages are installed..."
    for pkg in $centos_dependencies; do
        if ! isinstalled $pkg; then sudo yum -y -q install $pkg; fi
    done
}

isinstalled () {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

init_installation_folder() {
    read -p "Please enter the full installation base path (Default: /opt/fgc): " path
    path=${path:-/opt/fgc}
    project_path="${path}/${project_name}"
    if [[ ! -d $project_path ]]; then
        mkdir $project_path
    fi
}


create_virtual_environment() {
    env_dir="${project_name}_env"
    pythonenv=${project_path}/$env_dir
    if [[ -d "$pythonenv" ]]; then
        output_in_yellow "Virtual environment directory exists already. Will not create a new one! ($pythonenv)"
    else
        echo "Creating virtual environment: $pythonenv"
        python3 -m venv $pythonenv
    fi
}

source_virtual_environment() {
    echo "Switching to virtual enviroment: $pythonenv"
    source $pythonenv/bin/activate
}

install_dependencies() {
    pip install -q --upgrade pip
    echo "Installing dependencies from requirements.txt..."
    pip install -q -f offline_packages -r "$project_name"/requirements.txt
}

configure_kt() {
    mkdir -p $project_path/log
    mkdir -p $project_path/config
    cp $project_name/config.cfg $project_path/config
    config_file=$project_path/config/config.cfg
    touch $project_path/config/list_gateways.txt

    sed -i 's/^status_read_mode        = status_server/;status_read_mode        = status_server/' $config_file
    sed -i '/^status_server_period.*/ s/^;*/;/' $config_file
    sed -i '/^status_server_name.*/ s/^;*/;/' $config_file

    sed -i 's/^;status_read_mode        = status_monitor/status_read_mode        = status_monitor/' $config_file
    sed -i 's/^;status_monitor_port.*/status_monitor_port     = 2634/' $config_file
    sed -i "s@^filter_gateways.*@filter_gateways         = ${project_path}/config/list_gateways.txt@" $config_file

    read -p "Please enter the location of the name file: " name_file
    sed -i "s@^;name_file.*@name_file = ${name_file}@" $config_file
    sed -i "s/^logging_handlers.*/logging_handlers            = file/" $config_file
    sed -i "s@^logging_file_path.*@logging_file_path           = ${project_path}/log/fgc_config_manager.log@" $config_file

    sed -i "s/^use_rbac.*/use_rbac                = No/" $config_file
    sed -i "s@^config_file.*@config_file = ${project_path}/config/db.conf@" $config_file

    cp $project_name/launcher.sh $project_path
    chmod +x $project_path/launcher.sh
    sed -i '3,8 {s/^/#/}' $project_path/launcher.sh
    sed -i "s@^PYTHON_VENV_PATH.*@PYTHON_VENV_PATH=${pythonenv}@" $project_path/launcher.sh
    sed -i "s@^LG_CONFIG_FILE.*@LG_CONFIG_FILE=${project_path}/config/config.cfg@" $project_path/launcher.sh

}

configure_db() {
    db_conf=${project_path}/config/db.conf
    if [[ -f $db_conf ]]; then
        output_in_yellow "Database configuration file $db_conf already exists. Skipping database configuration..."
    else
        echo "Setting up database..."
        db_read_parameters
        db_test_parameters
        db_update_dbconf
    fi
}

db_read_parameters() {
    read -p "    Hostname: " hostname
    read -p "    Port: " port
    read -p "    Database: " database
    read -p "    Username: " username
    read -s -p "    Password: " password
}

#TODO: prompt to re-read the parameters
db_test_parameters() {
    echo -e "\nTesting database connection..."

    result=$(PGPASSWORD=$password psql -h $hostname -p $port -U $username $database -t -c 'SELECT 5432;')
    if [[ $result -ne "5432" ]]; then
       output_in_red "Database connection failed! Establishing connection with the db is necessary for the installation!" 
       output "Exiting setup..."
       exit
    else
        output "Database connection successful!"
    fi
}

db_update_dbconf() {
    db_conf=${project_path}/config/db.conf
    echo "{" >> $db_conf
    echo '"driver": "postgresql",' >> $db_conf
    echo "\"host\": \"${hostname}\"," >> $db_conf
    echo "\"port\": \"${port}\"," >> $db_conf
    echo "\"username\": \"${username}\"," >> $db_conf
    echo "\"password\": \"${password}\"," >> $db_conf
    echo "\"database\": \"${database}\"" >> $db_conf
    echo "}" >> $db_conf
}

generate_systemd_service() {
    service="${project_name}.service"

    if [[ -f /etc/systemd/system/$service ]]; then
        output_in_yellow "$project_name service /etc/systemd/system/$service already exists. Skipping creation..."
    else
        echo "Generating ${service}..."

        if [[ -f $service ]]; then
            rm $service
        fi

        echo "[Unit]" | tee -a $service > /dev/null
        echo -e "Description=FGC Logger Service\n" | tee -a $service > /dev/null
        
        echo "[Service]" | tee -a $service > /dev/null
        echo "Type=forking" | tee -a $service > /dev/null
        echo "StandardInput=null" | tee -a $service > /dev/null
        echo "StandardOutput=null" | tee -a $service > /dev/null
        echo "StandardError=null" | tee -a $service > /dev/null
        echo "ExecStart=$project_path/launcher.sh" | tee -a $service > /dev/null
        echo "ExecStop=/bin/kill $MAINPID" | tee -a $service > /dev/null
        echo "Restart=on-failure" | tee -a $service > /dev/null
        echo -e "RestartSec=5s\n" | tee -a $service > /dev/null
        
        echo "[Install]" | tee -a $service > /dev/null
        echo "WantedBy=multi-user.target" | tee -a $service > /dev/null

        install_service=1
        confirm "Do you want to install and enable ${project_name} service to systemd (location: /etc/systemd/system/) ? [Y/n]: " true || install_service=0
        if [[ $install_service -eq 1 ]]; then
            echo "Enabling ${service}..."
            sudo cp $service /etc/systemd/system/
            sudo systemctl enable --now $project_name
        fi
    fi
    output_in_yellow "Please don't forget to add all the gateways to the $project_path/config/list_gateways.txt file and restart the service"
}


main
