#!/bin/bash

project_base_path="$(pwd)/../../clients/web/"
project_name="fgc_property_manager"
offline_packages_dir="$(pwd)/offline_packages"
destination=$(pwd)

client_modules_path="$(pwd)/../../clients/python"
client_modules="pyfgc_const pyfgc_decoders pyfgc_rbac pyfgc_name pyfgc"

util_modules_path="$(pwd)/../../utilities/python"
util_modules="ccs_utils"

main() {
    generate_offline_packages
    #get_packages_from_accpy
    package
}

generate_offline_packages() {
    echo "Generating offline packages..."
    if [[ -d $offline_packages_dir ]]; then
        rm $offline_packages_dir/*
    else
        mkdir $offline_packages_dir
    fi

    for mod in $client_modules; do
        echo "    Archiving $mod..."
        cd $client_modules_path/$mod
        python3 setup.py -q sdist -d $offline_packages_dir
    done

    for mod in $util_modules; do
        echo "    Archiving $mod..."
        cd $util_modules_path/$mod
        python3 setup.py -q sdist -d $offline_packages_dir
    done
}

get_packages_from_accpy() {
    source /acc/local/share/python/acc-py/pro/setup.sh
    echo "Getting packages from accpy..."
    if [[ -d $offline_packages_dir ]]; then
        rm $offline_packages_dir/*
    else
        mkdir $offline_packages_dir
    fi

    for mod in $client_modules; do
        echo "    Getting $mod..."
        pip download --no-deps $mod -d $offline_packages_dir
    done

    for mod in $util_modules; do
        echo "    Getting $mod..."
        pip download --no-deps $mod -d $offline_packages_dir
    done
}

package() {
    echo "Packaging..."
    cd ${destination}
    pkg=${project_name}_$(date +%s)
    mkdir $pkg
    cp -r $project_base_path/$project_name "offline_packages" "install.sh" "README.md" $pkg
    sed -i 's/^daphne.*/daphne==2.5.0/' $pkg/$project_name/requirements.txt
    sed -i 's/^channels.*/channels==2.4.0/' $pkg/$project_name/requirements.txt
    tar czvf ${pkg}.tar.gz $pkg
    rm -r $pkg
}

main
