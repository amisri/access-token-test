#!/bin/bash

project_name="fgc_property_manager"

kt_dependencies="psycopg2"
centos_dependencies="python3 postgresql-libs postgresql-devel python3-psycopg2 python3-devel python3-mod_wsgi mod_ssl"


main() {
    install_packages_if_needed
    init_installation_folder
    create_virtual_environment
    source_virtual_environment
    install_dependencies
    configure_kt
    configure_db
    init_django_db
    generate_daphne_service
    generate_apache_conf
    #configure_apache
}

output() {
    END="\e[0m"
    echo -e "$1$2$END"
}

output_in_red() {
    RED="\e[01;31m"
    output $RED "$1"
}

output_in_yellow() {
    YELLOW="\e[01;33m"
    output $YELLOW "$1"
}

confirm() {
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        [nN][oO]|[nN])
            false
            ;;
        *)
            $2
            ;;
    esac
}

install_packages_if_needed() {
    echo "Checking if needed packages are installed..."
    for pkg in $centos_dependencies; do
        if ! isinstalled $pkg; then sudo yum -y -q install $pkg; fi
    done
}

isinstalled () {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

init_installation_folder() {
    read -p "Please enter the full installation base path (Default: /opt/fgc): " path
    path=${path:-/opt/fgc}
    project_path="${path}/${project_name}"
    if [[ ! -d $project_path ]]; then
        mkdir $project_path
    fi

    output "Copying files to installation path..."
    cp -r $project_name $project_path
}


create_virtual_environment() {
    env_dir="${project_name}_env"
    pythonenv=${project_path}/$env_dir
    if [[ -d "$pythonenv" ]]; then
        output_in_yellow "Virtual environment directory exists already. Will not create a new one! ($pythonenv)"
    else
        echo "Creating virtual environment: $pythonenv"
        python3 -m venv $pythonenv
    fi
}

source_virtual_environment() {
    echo "Switching to virtual enviroment: $pythonenv"
    source $pythonenv/bin/activate
}

install_dependencies() {
    pip install -q --upgrade pip
    echo "Installing dependencies from requirements.txt..."
    pip install -q -f offline_packages -r "$project_name"/requirements.txt
    for dep in $kt_dependencies; do 
        pip install -q $dep
    done
}

configure_kt() {
    def_host_name=$(hostname -a)
    read -p "Please enter the server name (Default: $def_host_name): " host_name
    host_name=${host_name:-$def_host_name}

    #ask for ssl
    use_ssl=1
    confirm "Will you use SSL? [Y/n]: " true || use_ssl=0

    settings_file=${project_path}/${project_name}/${project_name}/settings.py
    sed -i 's/^USE_RBAC.*/USE_RBAC = False/' $settings_file
    sed -i 's/^DEBUG.*/DEBUG = False/' $settings_file
    read -p "Please enter the location of the name file(make sure it's accessible by apache): " name_file
    sed -i "s@^NAME_FILE.*@NAME_FILE = \"${name_file}\"@" $settings_file
    echo -e "\n\nALLOWED_HOSTS = ['${host_name}']\n" >> $settings_file

    # remove silk
    sed -i "/\s*'silk.middleware.SilkyMiddleware'/d" $settings_file
    sed -i "/\s*'silk',/d" $settings_file
    sed -i "/^SILKY_META = /d" $settings_file
    sed -i "/^SILKY_INTERCEPT_PERCENT = /d" $settings_file
    sed -i "/^\s*.*silk.urls/d" ${path}/${project_name}/${project_name}/${project_name}/urls.py

    if [[ $use_ssl -eq 0 ]]; then
        sed -i 's/^var websocket_protocol = .*/var websocket_protocol = "ws";/' ${path}/${project_name}/${project_name}/static/js/utils/websocket_handler.js
    fi
}

configure_db() {
    if [[ -f ${project_path}/${project_name}/${project_name}/dbconf.py ]]; then
        output_in_yellow "Database configuration file ${path}/${project_name}/${project_name}/${project_name}/dbconf.py already exists. Skipping database configuration..."
    else
        echo "Setting up database..."
        db_read_parameters
        db_test_parameters
        db_update_dbconf
    fi
}

db_read_parameters() {
    read -p "    Hostname: " hostname
    read -p "    Port: " port
    read -p "    Database: " database
    read -p "    Username: " username
    read -s -p "    Password: " password
}

#TODO: prompt to re-read the parameters
db_test_parameters() {
    echo -e "\nTesting database connection..."

    result=$(PGPASSWORD=$password psql -h $hostname -p $port -U $username $database -t -c 'SELECT 5432;')
    if [[ $result -ne "5432" ]]; then
       output_in_red "Database connection failed! Establishing connection with the db is necessary for the installation!" 
       output "Exiting setup..."
       exit
    else
        output "Database connection successful!"
    fi
}

db_update_dbconf() {
    cp ${project_path}/${project_name}/${project_name}/dbconf.py.postgres.template ${project_path}/${project_name}/${project_name}/dbconf.py
    sed -i "2s/.*/name=\"${database}\"/" ${project_path}/${project_name}/${project_name}/dbconf.py
    sed -i "3s/.*/host=\"${hostname}\"/" ${project_path}/${project_name}/${project_name}/dbconf.py
    sed -i "4s/.*/port=\"${port}\"/" ${project_path}/${project_name}/${project_name}/dbconf.py
    sed -i "5s/.*/username=\"${username}\"/" ${project_path}/${project_name}/${project_name}/dbconf.py
    sed -i "6s/.*/password=\"${password}\"/" ${project_path}/${project_name}/${project_name}/dbconf.py
}

init_django_db() {
    echo "Initializing django tables..."
    (cd ${project_path}/${project_name} && python3 manage.py migrate && python3 manage.py createcachetable)
}

generate_daphne_service() {
    daphne_service="fgc_property_manager_daphne.service"
    if [[ -f /etc/systemd/system/$daphne_service ]]; then
        output_in_yellow "Daphne service /etc/systemd/system/$daphne_service already exists. Skipping creation..."
    else
        echo "Generating ${daphne_service}.service..."

        if [[ -f $daphne_service ]]; then
            rm $daphne_service
        fi

        echo "[Unit]" | tee -a $daphne_service > /dev/null
        echo "Description=FGC Property Manager daphne server script" | tee -a $daphne_service > /dev/null
        echo -e "After=network.target\n" | tee -a $daphne_service > /dev/null
        echo "[Service]" | tee -a $daphne_service > /dev/null
        echo "WorkingDirectory=${project_path}/${project_name}" | tee -a $daphne_service > /dev/null
        echo "Environment=DJANGO_SETTINGS_MODULE=${project_name}.settings" | tee -a $daphne_service > /dev/null
        echo "ExecStart=${project_path}/${project_name}_env/bin/daphne -b localhost -v 2 -p 8650 ${project_name}.asgi:application" | tee -a $daphne_service > /dev/null
        echo -e "Restart=always\n" | tee -a $daphne_service > /dev/null
        echo "[Install]" | tee -a $daphne_service > /dev/null
        echo "WantedBy=multi-user.target" | tee -a $daphne_service > /dev/null


        install_service=1
        confirm "Do you want to install and enable daphne service to systemd (location: /etc/systemd/system/) ? [Y/n]: " true || install_service=0
        if [[ $install_service -eq 1 ]]; then
            echo "Enabling daphne service..."
            sudo cp $daphne_service /etc/systemd/system/
            sudo systemctl enable --now daphne
        fi
    fi
}

generate_apache_conf() {
    echo "Generating ${project_name}.conf for Apache..."
    apache_conf="${project_name}.conf"

    if [[ -f $apache_conf ]]; then
        rm $apache_conf
    fi

    echo "Define WSGI_VENV ${project_path}/${project_name}_env" | tee -a $apache_conf > /dev/null
    echo -e "Define WSGI_PROJ ${project_path}/${project_name}\n\n" | tee -a $apache_conf > /dev/null

    echo -e "Define SERVER_NAME $host_name" | tee -a $apache_conf > /dev/null
    echo -e "Define SERVER_DOCUMENT_ROOT /var/www/html" | tee -a $apache_conf > /dev/null
    #read -p "Please enter the e-mail address of the server admin: " apache_mail
    #apache_mail=${apache_mail:-""}
    #echo -e "Define SERVER_ADMIN $apache_mail" | tee -a $apache_conf > /dev/null
    if [ $use_ssl -eq "1" ]; then
        echo -e "Define SERVER_SSL_CERTIFICATE /opt/fgc/ssl/localhost.crt " | tee -a $apache_conf > /dev/null
        echo -e "Define SERVER_SSL_KEYFILE /opt/fgc/ssl/localhost.key\n\n" | tee -a $apache_conf > /dev/null
    fi

    echo "Define WS_ALIAS /websocket/${project_name}" | tee -a $apache_conf > /dev/null
    echo "Define WS_PORT 8650" | tee -a $apache_conf > /dev/null
    echo "Define WS_HOST localhost" | tee -a $apache_conf > /dev/null
    if [ $use_ssl -eq "1" ]; then
        echo -e "Define WS_PROTOCOL wss\n\n" | tee -a $apache_conf > /dev/null
    else
        echo -e "Define WS_PROTOCOL ws\n\n" | tee -a $apache_conf > /dev/null
    fi 

    if [ $use_ssl -eq "1" ]; then
        # for https redirect
        echo "<VirtualHost *:80>" | tee -a $apache_conf > /dev/null
        echo -e "\tServerName \${SERVER_NAME}" | tee -a $apache_conf > /dev/null
        echo -e "\tServerAdmin \${SERVER_ADMIN}" | tee -a $apache_conf > /dev/null
        echo -e "\tDocumentRoot  \${SERVER_DOCUMENT_ROOT}" | tee -a $apache_conf > /dev/null
        echo -e "\tRedirect permanent / \${SERVER_NAME}:/" | tee -a $apache_conf > /dev/null
        echo -e "</VirtualHost>\n" | tee -a $apache_conf > /dev/null
    fi

    if [ $use_ssl -eq "1" ]; then
        echo "<VirtualHost *:443>" | tee -a $apache_conf > /dev/null
    else
        echo "<VirtualHost *:80>" | tee -a $apache_conf > /dev/null
    fi
    echo -e "\tServerName \${SERVER_NAME}" | tee -a $apache_conf > /dev/null
    echo -e "\tDocumentRoot  \${SERVER_DOCUMENT_ROOT}" | tee -a $apache_conf > /dev/null
    echo -e "\tProtocols h2" | tee -a $apache_conf > /dev/null
    if [ $use_ssl -eq "1" ]; then
        echo -e "\tSSLEngine On" | tee -a $apache_conf > /dev/null
        echo -e "\tSSLCertificateFile \${SERVER_SSL_CERTIFICATE}" | tee -a $apache_conf > /dev/null
        echo -e "\tSSLCertificateKeyFile \${SERVER_SSL_KEYFILE}" | tee -a $apache_conf > /dev/null
    fi
    #echo -e "\tAlias /${project_name}/static ${project_path}/${project_name}/static" | tee -a $apache_conf > /dev/null
    echo -e "\tAlias /${project_name}/static \${WSGI_PROJ}/static" | tee -a $apache_conf > /dev/null
    #echo -e "\t<Directory ${project_path}/${project_name}/static>" | tee -a $apache_conf > /dev/null
    echo -e "\t<Directory \${WSGI_PROJ}/static>" | tee -a $apache_conf > /dev/null
    echo -e "\t\tRequire all granted" | tee -a $apache_conf > /dev/null
    echo -e "\t</Directory>" | tee -a $apache_conf > /dev/null
    #echo -e "\t<Directory ${project_path}/${project_name}/${project_name}>" | tee -a $apache_conf > /dev/null
    echo -e "\t<Directory \${WSGI_PROJ}/${project_name}>" | tee -a $apache_conf > /dev/null
    echo -e "#\t\tAuthUserFile \${WSGI_PROJ}/.htpasswd" | tee -a $apache_conf > /dev/null
    echo -e "#\t\tAuthName \"Basic Authentication\"" | tee -a $apache_conf > /dev/null
    echo -e "#\t\tAuthType Basic" | tee -a $apache_conf > /dev/null
    echo -e "#\t\tRequire valid-user" | tee -a $apache_conf > /dev/null

    echo -e "\t\t<Files wsgi.py>" | tee -a $apache_conf > /dev/null
    echo -e "\t\t\tRequire all granted" | tee -a $apache_conf > /dev/null
    echo -e "#\t\t\tRequire valid-user" | tee -a $apache_conf > /dev/null
    echo -e "\t\t</Files>" | tee -a $apache_conf > /dev/null
    echo -e "\t</Directory>" | tee -a $apache_conf > /dev/null
    echo -e "\tWSGIDaemonProcess ${project_name} python-path=\${WSGI_PROJ} python-home=\${WSGI_VENV}" | tee -a $apache_conf > /dev/null
    echo -e "\tWSGIProcessGroup ${project_name}" | tee -a $apache_conf > /dev/null
    #echo -e "\tWSGIScriptAlias /${project_name} ${project_path}/${project_name}/wsgi.py" | tee -a $apache_conf > /dev/null
    echo -e "\tWSGIScriptAlias /${project_name} \${WSGI_PROJ}/${project_name}/wsgi.py\n" | tee -a $apache_conf > /dev/null
    echo -e "\t<Location \${WS_ALIAS}>" | tee -a $apache_conf > /dev/null
    echo -e "\t\tProxyPass \${WS_PROTOCOL}://\${WS_HOST}:\${WS_PORT}" | tee -a $apache_conf > /dev/null
    echo -e "\t\tProxyPassReverse \${WS_PROTOCOL}://\${WS_HOST}:\${WS_PORT}" | tee -a $apache_conf > /dev/null
    echo -e "\t</Location>" | tee -a $apache_conf > /dev/null
    echo "</VirtualHost>" | tee -a $apache_conf > /dev/null


    output_in_yellow "Please configure the apache manually! Use the ${project_name}.conf generated file as a template!"
    if [ $use_ssl -eq "1" ]; then
        output_in_yellow "And don't forget to setup the SERVER_SSL_*"
    fi
}

configure_apache() {
    sudo setsebool -P httpd_can_network_connect_db 1
    sudo setsebool -P httpd_can_network_connect 1
}

main
