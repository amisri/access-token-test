- Initial setup
The installation script install.sh takes care of the setup required for the Property Manager, this includes setting up the virtual environment, the connection with the database, the daphne service for the websockets and the creation of the apache configuration

To execute it simply run ./install.sh and follow the instructions.

- Apache setup
The fcg_property_manager.conf is provided as a template for the configuration and should be edited before use. The main part that should be set is the location of the SSL certificate. 

For testing purposes, on a localhost environment,  the following command can be used to generate a certificate: 
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")


-- Apache CERN setup
The following instructions can be used to deploy the Property Manager in a similar way as CERN.
1) Create folders sites-available and sites-enabled: sudo mkdir /etc/httpd/sites-available /etc/httpd/sites-enabled
2) Append "IncludeOptional sites-enabled/*.conf" at the end of /etc/httpd/conf/httpd.conf
3) Move fgc_property_manager.conf to /etc/httpd/sites-available: sudo cp fgc_property_manager.conf /etc/httpd/sites-available
4) Enable the site: sudo ln -s /etc/httpd/sites-available/fgc_property_manager.conf /etc/httpd/sites-enabled/fgc_property_manager.conf
5) Restart apache: sudo systemctl restart httpd
