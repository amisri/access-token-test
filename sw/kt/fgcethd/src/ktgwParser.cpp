

#include <libfgceth.h>

#include <ktgwGlobals.h>
#include <ktgwParser.h>

#include <stdio.h>
#include <ctype.h>
#include <arpa/inet.h>

/*
 * Process buffer of incoming commands
 */

//Log functions
/*
logPrintf(client->recv_cmd->d.cmd_rsp.device->id,
          "TCP %d (%s@%s) cmd (G %s)\n",
          client->id, client->username, client->hostname,
          client->recv_cmd->d.cmd_rsp.command_string);
logPrintf(client->recv_cmd->d.cmd_rsp.device->id,
          "TCP %d (%s@%s) cmd (S %s %s)\n",
          client->id, client->username, client->hostname,
          client->recv_cmd->d.cmd_rsp.command_string,
          client->recv_cmd->d.cmd_rsp.value);
 */

void ktgwRestartParser(struct ktgw_parsed_cmd * data)
{
    data->cmd_state = TCP_RECV_IDLE;

    data->bin_index = 0;
    data->dev_index = 0;
    data->tag_index = 0;
    data->pro_index = 0;
    data->val_index = 0;
    data->last_ch_space = 0;

    data->valid     = false;
    data->completed = false;

    data->bin_len   = 0;
}

int32_t ktgwParseCmd(/* Inputs  */ char * input, uint32_t input_len,
                     /* Outputs */ struct ktgw_parsed_cmd * data, fgceth_rsp * cmd)
{

    for(uint32_t i = 0 ; i < input_len ; i++)
    {
        switch(data->cmd_state)
        {
            case TCP_RECV_BIN_LENGTH:

                ((unsigned char *)&data->bin_len)[data->bin_index++] = input[i];

                // Check whether binary length has been completely received

                if(data->bin_index == sizeof(data->bin_len))
                {
                    data->bin_len   = ntohl(data->bin_len);
                    data->bin_index = 0;

                    if(data->bin_len)
                    {
                        if(data->bin_len < (KTGW_MAX_VAL_LEN - data->val_index))
                        {
                            data->cmd_state = cmd->d.cmd_rsp.error != FGC_OK_NO_RSP ? TCP_RECV_BIN_ERROR : TCP_RECV_BIN_VALUE;
                        }
                        else // Data is too long
                        {
                            cmd->d.cmd_rsp.error = FGC_CMD_BUF_FULL;
                            data->cmd_state      = TCP_RECV_BIN_ERROR;
                        }
                    }
                    else // Length is zero
                    {

                        // Remove trailing spaces

                        while(data->pro_index && cmd->d.cmd_rsp.property[data->pro_index - 1] == ' ')
                        {
                            data->pro_index--;
                        }

                        // Null-terminate command

                        cmd->d.cmd_rsp.property[data->pro_index++] = '\0';

                        // Replace user name by number

                        /*
                        if (tcpReplaceUserNameByNumber(client->recv_cmd)!=0)
                        {
                            cmd_state = TCP_RECV_BIN_ERROR;
                            break;
                        }*/

                        // Log binary set

                        if(cmd->d.cmd_rsp.cmd_type == GET)
                        {
                            /*logPrintf(0, "TCP %d (%s@%s) cmd (G %s <0B empty binary data>)\n",
                                client->id, client->username, client->hostname,
                                client->recv_cmd->d.cmd_rsp.command_string);*/
                        }
                        else
                        {
                            /*logPrintf(0, "TCP %d (%s@%s) cmd (S %s <0B empty binary data>)\n",
                                client->id, client->username, client->hostname,
                                client->recv_cmd->d.cmd_rsp.command_string);*/
                        }

                        // Queue command

                        data->valid     = true;
                        data->completed = true;

                        return (i + 1);
                    }
                }

                break;

            case TCP_RECV_BIN_VALUE:

                // Add byte to value

                blkbufAppendBin(cmd->d.cmd_rsp.value_opts, &input[i], 1);

                //cmd->d.cmd_rsp.value_opts[data->val_index++] = input[i];

                // Check whether binary value has been completely received

                if(++data->bin_index == data->bin_len)
                {
                    // Command complete

                    //tcp.cmds_received++;

                    // Remove trailing spaces

                    while(data->pro_index &&
                          cmd->d.cmd_rsp.property[data->pro_index - 1] == ' ')
                    {
                        data->pro_index--;
                    }

                    // Null-terminate command

                    cmd->d.cmd_rsp.property[data->pro_index++] = '\0';

                    // Replace the user name by the number

                    /*
                    if(tcpReplaceUserNameByNumber(client->recv_cmd)!=0)
                    {
                        client->cmd_state = TCP_RECV_BIN_ERROR;
                        break;
                    }
                    */

                    // Log binary set

                    if(cmd->d.cmd_rsp.cmd_type == GET)
                    {
                        /*logPrintf(client->recv_cmd->d.cmd_rsp.device->id,
                            "TCP %d (%s@%s) cmd (G %s <%uB binary data>)\n",
                            client->id, client->username, client->hostname,
                            client->recv_cmd->d.cmd_rsp.command_string,
                            client->bin_length);*/
                    }
                    else
                    {
                        /*logPrintf(client->recv_cmd->d.cmd_rsp.device->id,
                            "TCP %d (%s@%s) cmd (S %s <%uB binary data>)\n",
                            client->id, client->username, client->hostname,
                            client->recv_cmd->d.cmd_rsp.command_string,
                            client->bin_length);*/

                    }
                    // Queue command

                    data->valid     = true;
                    data->completed = true;

                    return (i + 1);
                }

                break;

            case TCP_RECV_BIN_ERROR:

                if(++data->bin_index == data->bin_len)
                {
                    // Command received
                    // Return error

                    // If a specific error message has not yet been set, default to FGC_UNKNOWN_CMD

                    if(cmd->d.cmd_rsp.error != FGC_OK_NO_RSP)
                    {
                        cmd->d.cmd_rsp.error = FGC_UNKNOWN_CMD;
                    }

                    data->valid     = false;
                    data->completed = true;

                    return (i + 1);
                }

                break;

            default:

                switch(input[i])
                {
                    case ';':
                        break;

                    case '!': // The start of a command

                        ktgwRestartParser(data);

                        data->cmd_state = TCP_RECV_TAG;

                        break;

                    case ' ':
                        switch(data->cmd_state)
                        {
                            case TCP_RECV_IDLE:
                            case TCP_RECV_TYPE:
                            case TCP_RECV_ERROR:
                                break;

                            case TCP_RECV_TAG:

                                // Tag is finished
                                // Null-terminate tag

                                cmd->d.cmd_rsp.tag[data->tag_index++] = '\0';

                                // Change state to TCP_RECV_TYPE

                                data->cmd_state = TCP_RECV_TYPE;

                                break;

                            case TCP_RECV_DEV:

                                if(data->dev_index > 0)
                                {
                                    // Spaces are not allowed in the middle of a device, return error

                                    cmd->d.cmd_rsp.error      = FGC_NO_COLON;
                                    data->cmd_state = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_PROP:

                                if(!data->last_ch_space)
                                {
                                    // Add space to command string

                                    cmd->d.cmd_rsp.property[data->pro_index++] = ' ';

                                    data->last_ch_space = 1;
                                    data->cmd_state = TCP_RECV_VALUE;
                                }

                                break;

                            case TCP_RECV_VALUE:

                                // Add space to command string
 
                                blkbufAppendBin(cmd->d.cmd_rsp.value_opts, " ", 1);

                                break;

                            default:
                                break;
                        }

                        break;

                    case ':':

                        if(data->cmd_state == TCP_RECV_DEV)
                        {
                            // Device is finished
                            // Null-terminate device name

                            cmd->d.cmd_rsp.dev_name[data->dev_index++] = '\0';

                            // Resolve device so that log messages can be associated with it

                            uint8_t channel;

                            if((channel = fgcethGetDevId(cmd->d.cmd_rsp.dev_name)) != FGCETH_NO_DEV)
                            {
                                //cmd->d.cmd_rsp.dev_id     = channel;
                                data->cmd_state = TCP_RECV_PROP;
                            }
                            else // Device name resolution failed
                            {
                                // Change state to TCP_RECV_ERROR

                                cmd->d.cmd_rsp.error      = FGC_UNKNOWN_DEV;
                                data->cmd_state = TCP_RECV_ERROR;
                            }
                        }
                        else if (data->cmd_state == TCP_RECV_PROP) // colon on the property is a subdevice name
                        {
                            // Check that the maximum command length has not been exceeded

                            if(data->pro_index < KTGW_MAX_PROP_LEN - 1)
                            {
                                // Add character to command string

                                cmd->d.cmd_rsp.property[data->pro_index++] = input[i];
                            }
                            else // Maximum command length exceeded
                            {
                                // Change state to TCP_RECV_ERROR

                                cmd->d.cmd_rsp.error      = FGC_CMD_BUF_FULL;
                                data->cmd_state = TCP_RECV_ERROR;
                            }

                            data->last_ch_space = 0;                            // Change state to TCP_RECV_ERROR
                        }
                        else
                        {
                            // Change state to TCP_RECV_ERROR

                            cmd->d.cmd_rsp.error      = FGC_UNKNOWN_SYM;
                            data->cmd_state = TCP_RECV_ERROR;
                        }

                        break;

                    case '\r':
                    case '\n':

                        if(data->cmd_state == TCP_RECV_PROP ||
                           data->cmd_state == TCP_RECV_VALUE)
                        {
                            // Command string is finished

                            // Remove trailing spaces

                            while(data->pro_index &&
                                  cmd->d.cmd_rsp.property[data->pro_index - 1] == ' ')
                            {
                                data->pro_index--;
                            }

                            // Null-terminate command & value

                            cmd->d.cmd_rsp.property[data->pro_index++] = '\0';
                            //cmd->d.cmd_rsp.value_opts[data->val_index++] = '\0';

                            // Check whether command length is zero (excluding '\0' char)

                            if(data->pro_index <= 1)
                            {
                                // Change state to TCP_RECV_ERROR

                                cmd->d.cmd_rsp.error      = FGC_NO_SYMBOL;
                                data->cmd_state = TCP_RECV_ERROR;
                            }
                            /*else if(tcpReplaceUserNameByNumber(client->recv_cmd)!=0)
                            {
                                cmd_state = TCP_RECV_ERROR;
                            }*/

                            // send command to FGC or error response back to client

                            if(data->cmd_state == TCP_RECV_ERROR)
                            {
                                data->valid     = false;
                                data->completed = true;

                                return (i + 1);
                            }
                            else // Command complete
                            {
                                // Log command

                                data->valid     = true;
                                data->completed = true;

                                return (i + 1);

                            }
                        }
                        else if(data->cmd_state != TCP_RECV_IDLE)
                        {
                            // Return error

                            // If a specific error message has not yet been set, default to FGC_UNKNOWN_CMD

                            if(cmd->d.cmd_rsp.error == FGC_OK_NO_RSP)
                            {
                                cmd->d.cmd_rsp.error = FGC_UNKNOWN_CMD;
                            }

                            data->valid     = false;
                            data->completed = true;

                            return (i + 1);
                        }

                        break;

                    case -1: //  Binary value 0xFF is being received

                        if(data->cmd_state != TCP_RECV_VALUE)
                        {
                            cmd->d.cmd_rsp.error = FGC_UNEXPECTED_BIN_DATA;
                        }
                        cmd->d.cmd_rsp.cmd_type   = SET_BIN;
                        data->cmd_state = TCP_RECV_BIN_LENGTH;

                        break;

                    default:

                        switch(data->cmd_state)
                        {
                            case TCP_RECV_IDLE:
                                break;

                            case TCP_RECV_ERROR:

                                if(input[i] == 0xFF)
                                {
                                    cmd->d.cmd_rsp.cmd_type   = SET_BIN;
                                    data->cmd_state = TCP_RECV_BIN_LENGTH;
                                    break;
                                }

                                break;

                            case TCP_RECV_TAG:

                                // Check that the maximum tag length has not been exceeded

                                if(data->tag_index < KTGW_MAX_TAG_LEN - 1)
                                {
                                    // Add character to tag

                                    cmd->d.cmd_rsp.tag[data->tag_index++] = input[i];
                                }
                                else // Maximum tag length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    cmd->d.cmd_rsp.error      = FGC_CMD_BUF_FULL;
                                    data->cmd_state = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_TYPE:

                                // Check for set or get

                                if(toupper(input[i]) == 'S')
                                {
                                    cmd->d.cmd_rsp.cmd_type   = SET;
                                    data->cmd_state = TCP_RECV_DEV;
                                }
                                else if(toupper(input[i]) == 'G')
                                {
                                    cmd->d.cmd_rsp.cmd_type   = GET;
                                    data->cmd_state = TCP_RECV_DEV;
                                }
                                else // Invalid type character
                                {
                                    // Change state to TCP_RECV_ERROR

                                    data->cmd_state = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_DEV:

                                // Check that the maximum device length has not been exceeded

                                if(data->dev_index < KTGW_MAX_DEV_LEN - 1)
                                {
                                    // Add character to device name

                                    cmd->d.cmd_rsp.dev_name[data->dev_index++] = toupper(input[i]);
                                }
                                else // Maximum device length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    cmd->d.cmd_rsp.error      = FGC_UNKNOWN_DEV;
                                    data->cmd_state = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_PROP:

                                // Check that the maximum command length has not been exceeded

                                if(data->pro_index < KTGW_MAX_PROP_LEN - 1)
                                {
                                    // Add character to command string

                                    cmd->d.cmd_rsp.property[data->pro_index++] = input[i];
                                }
                                else // Maximum command length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    cmd->d.cmd_rsp.error      = FGC_CMD_BUF_FULL;
                                    data->cmd_state = TCP_RECV_ERROR;
                                }

                                data->last_ch_space = 0;

                                break;

                            case TCP_RECV_VALUE:

                                // Check that the maximum command length has not been exceeded

                                if(data->val_index < KTGW_MAX_VAL_LEN - 1)
                                {
                                    // Add character to command string

                                    blkbufAppendBin(cmd->d.cmd_rsp.value_opts, &input[i], 1);
                                    //cmd->d.cmd_rsp.value_opts[data->val_index++] = input[i];
                                }
                                else // Maximum value length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    cmd->d.cmd_rsp.error      = FGC_CMD_BUF_FULL;
                                    data->cmd_state = TCP_RECV_ERROR;
                                }

                                data->last_ch_space = 0;

                                break;

                            default:
                                break;
                        }
                        break;
                }
        }
    }

    return input_len;
}


// EOF
