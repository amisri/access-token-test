
#include <ktgwGlobals.h>
#include <ktgwTcp.h>
#include <ktgwStack.h>
#include <ktgwParser.h>
#include <ktgwProps.h>
#include <ktgwStatPub.h>

#include <libfgceth.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <errno.h>

#include <unistd.h>
#include <fcntl.h>


static void ktgwPubReadSubs(FILE *subs_file, const char *filename)
{
    const uint32_t   MAX_USER_NAME = 64;

    char     user   [MAX_USER_NAME + 1];
    char     format [100];

    // Prepare address_hints

    struct addrinfo address_hints;
    memset(&address_hints, 0, sizeof(struct addrinfo));
    address_hints.ai_family = AF_INET;

    // Read each subscription

    int32_t  items_assigned;
    uint32_t line_number     = 0;

    do
    {
        struct ktgw_udp_client * client = &ktgw_global.udp_client[ktgw_global.num_udp_clients];

        client->user = 0;

        // Create format from constants

        sprintf(format, "%%%d[^:]:%%4u:%%5d", HOST_NAME_MAX);

        items_assigned = fscanf(subs_file, format, client->hostname, &client->port_num, &client->period);
        line_number++;

        if(items_assigned == 3)
        {
            // Attempt to read optional user-defined ID

            items_assigned = fscanf(subs_file, ":%x", &client->id);

            if(items_assigned != 1)
            {
                client->id = 0;
            }
            else
            {
                // Attempt to read optional user

                sprintf(format, ":%%%d[^:\n]", MAX_USER_NAME);
                items_assigned = fscanf(subs_file, format, user);

                if(items_assigned == 1)
                {
                    /*uint32_t user_num;

                    if((user_num = timingUserNumber(user)))
                    {
                        client->user = user_num;
                    }
                    else
                    {
                        fprintf(stderr, "Unknown user %s in subscription file %s line %d.\n",
                                user, filename, line_number);
                        continue;
                    }*/

                    // TODO: Print warning if extra argument is found
                }
            }

            items_assigned = 3;

            // Resolve client's address to a host entry

            int32_t          error;
            struct addrinfo *address_info;

            if((error = getaddrinfo(client->hostname, NULL, &address_hints, &address_info)))
            {
                // Resolution failed - report error and continue with next entry

                // TODO: Change to log warning

                fgcethWarningLog(FGCETH_NO_DEV, "Unable to resolve address for hostname %s in UDP subscription file %s line %d: %s\n",
                        client->hostname, filename, line_number, gai_strerror(error));

                continue;
            }

            // Store client address

            client->address = reinterpret_cast<struct sockaddr_in*>(address_info[0].ai_addr)->sin_addr;
            freeaddrinfo(address_info);

            // Check whether a negative value was specified for an aligned subscription

            if(client->period < 0)
            {
                if(abs(client->period) > FGCETH_CYCLES_PER_SEC)
                {
                    // Check that period is a multiple of seconds

                    if((FGCETH_CYCLE_PERIOD_MS * abs(client->period)) % 1000)
                    {
                        fgcethWarningLog(FGCETH_NO_DEV, "Invalid period %d in subscription file %s line %d.\n", client->period, filename, line_number);
                        continue;
                    }
                }
                else
                {
                    // Check that period is a factor of 1 second

                    if(1000. / (FGCETH_CYCLE_PERIOD_MS * abs(client->period)) != 1000 / (FGCETH_CYCLE_PERIOD_MS * abs(client->period)))
                    {
                        fgcethWarningLog(FGCETH_NO_DEV, "Invalid period %d in subscription file %s line %d.\n", client->period, filename, line_number);
                        continue;
                    }
                }
            }

            ktgw_global.num_udp_clients++;

            // Read until end of line or file

            int32_t c;

            while((c = fgetc(subs_file)) != EOF && c != '\n');
        }
    } while(items_assigned == 3 && ktgw_global.num_udp_clients < MAX_PUB_UDP_CLIENTS);

    if(items_assigned != EOF)
    {
        // Check whether too many clients were specified

        if(ktgw_global.num_udp_clients == MAX_PUB_UDP_CLIENTS)
        {
            fgcethWarningLog(FGCETH_NO_DEV, "Too many UDP subscriptions in file %s at line %d\n",
                    filename, line_number);
        }
        else if(items_assigned != 3)  // Syntax error
        {
            fgcethWarningLog(FGCETH_NO_DEV, "Syntax error in UDP subscription file %s at line %d\n",
                    filename, line_number);
        }
        //fgcddev.status.st_faults |= FGC_FLT_CONFIG;
    }
}

static void ktgwPubScanUDPClients(struct timeval *pub_time)
{
    // Configure socket address structure

    struct sockaddr_in sin;

    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;

    for(uint32_t i = 0 ; i < ktgw_global.num_udp_clients ; i++)
    {
        struct ktgw_udp_client * udp_client  = &ktgw_global.udp_client[i];
        uint32_t                 port_number = udp_client->port_num;

        // Take no action if port number or period is 0 or subscription is for a different user

        if(port_number && udp_client->period)
        {
            // Align subscription if period is negative

            if(udp_client->period < 0)
            {
                if(abs(udp_client->period) > (1000 / FGCETH_CYCLE_PERIOD_MS)) // Align to a multiple of seconds
                {
                    udp_client->period_tick = ((pub_time->tv_sec % ((udp_client->period * FGCETH_CYCLE_PERIOD_MS) / 1000)) * FGCETH_CYCLES_PER_SEC) +
                                              (pub_time->tv_usec / (FGCETH_CYCLE_PERIOD_MS * 1000));
                }
                else // Align within a second
                {
                    udp_client->period_tick = ((pub_time->tv_usec / (FGCETH_CYCLE_PERIOD_MS * 1000)) % udp_client->period);
                }
            }

            ++udp_client->period_tick;

            // Send data

            if(udp_client->period_tick * FGCETH_CYCLE_PERIOD_MS >= abs(udp_client->period * FGCETH_CYCLE_PERIOD_MS))
            {
                // Re-Initialize tick to zero

                udp_client->period_tick = 0;

                // Configure addressing

                sin.sin_addr = udp_client->address;
                sin.sin_port = htons(port_number);

                // Set time of data send

                struct timeval            send_time;
                struct fgc_udp_header    *header;

                fgcethGetCurrentTime(&send_time);

                header = &ktgw_global.pub_head.header;
                header->id              = htonl(udp_client->id);
                header->sequence        = htonl(udp_client->sequence++);
                header->send_time_sec   = htonl(send_time.tv_sec);
                header->send_time_usec  = htonl(send_time.tv_usec);

                // Send data
                
                struct msghdr udp_msg;
                struct iovec  udp_msg_data[2];

                udp_msg_data[0].iov_base = &ktgw_global.pub_head;
                udp_msg_data[0].iov_len  = sizeof(ktgw_global.pub_head);

                udp_msg_data[1].iov_base = ktgw_global.pub_status;
                udp_msg_data[1].iov_len  = sizeof(struct fgceth_fgc_stat) * FGCETH_ALL_DEV_MAX;
                
                udp_msg.msg_name       = &sin;                         // Optional address
                udp_msg.msg_namelen    = sizeof(struct sockaddr_in);   // Size of address
                udp_msg.msg_iov        = udp_msg_data;                 // Scatter/gather array
                udp_msg.msg_iovlen     = 2;                            // # elements in msg_iov
                udp_msg.msg_control    = NULL;                         // Ancillary data, see below
                udp_msg.msg_controllen = 0;                            // Ancillary data buffer len
                udp_msg.msg_flags      = 0;                            // Flags on received message

                int32_t ret = sendmsg(ktgw_global.udp_pub_sock, &udp_msg, 0);

                if(ret < 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "Error \"%s\" during UDP publication.", strerror(errno));
                }

                ktgw_global.udp_pub_load += ret;
            }
        }
    }
}


static void ktgwPubScanTCPClientsUDP(struct timeval *pub_time)
{
    // Configure socket address structure

    struct sockaddr_in sin;

    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;

    for(uint32_t i = 0 ; i < KTGW_MAX_TCP_CLIENTS ; i++)
    {
        struct ktgw_client *tcp_client  = &ktgw_global.client[i];
        uint32_t            port_number = tcp_client->udp_sub_port_num;

        // Take no action if port number or period is 0 or subscription is for a different user

        if(port_number && tcp_client->udp_sub_period)
        {

            // Align subscription if period is negative

            if(tcp_client->udp_sub_period < 0)
            {
                if(abs(tcp_client->udp_sub_period) > (1000 /  FGCETH_CYCLE_PERIOD_MS)) // Align to a multiple of seconds
                {
                    tcp_client->udp_sub_period_tick = ((pub_time->tv_sec % ((tcp_client->udp_sub_period * FGCETH_CYCLE_PERIOD_MS) / 1000)) * FGCETH_CYCLES_PER_SEC) +
                                                      (pub_time->tv_usec / (FGCETH_CYCLE_PERIOD_MS * 1000));
                }
                else // Align within a second
                {
                    tcp_client->udp_sub_period_tick = ((pub_time->tv_usec / (FGCETH_CYCLE_PERIOD_MS * 1000)) % tcp_client->udp_sub_period);
                }
            }

            ++tcp_client->udp_sub_period_tick;

            // Send data

            if(tcp_client->udp_sub_period_tick * FGCD_CYCLE_PERIOD_MS >= abs(tcp_client->udp_sub_period * PUB_UDP_TCP_PERIOD_MS))
            {
                // Re-Initialize tick to zero

                tcp_client->udp_sub_period_tick = 0;

                // Configure addressing

                sin.sin_addr = tcp_client->address;
                sin.sin_port = htons(port_number);

                // Set time of data send

                struct timeval            send_time;
                struct fgc_udp_header    *header;

                fgcethGetCurrentTime(&send_time);

                header = &ktgw_global.pub_head.header;
                header->id              = htonl(tcp_client->udp_sub_id);
                header->sequence        = htonl(tcp_client->udp_sub_sequence++);
                header->send_time_sec   = htonl(send_time.tv_sec);
                header->send_time_usec  = htonl(send_time.tv_usec);

                // Send data

                struct msghdr udp_msg;
                struct iovec  udp_msg_data[2];

                udp_msg_data[0].iov_base = &ktgw_global.pub_head;
                udp_msg_data[0].iov_len  = sizeof(ktgw_global.pub_head);

                udp_msg_data[1].iov_base = ktgw_global.pub_status;
                udp_msg_data[1].iov_len  = sizeof(struct fgceth_fgc_stat) * FGCETH_ALL_DEV_MAX;

                udp_msg.msg_name       = &sin;                         // Optional address
                udp_msg.msg_namelen    = sizeof(struct sockaddr_in);   // Size of address
                udp_msg.msg_iov        = udp_msg_data;                 // Scatter/gather array
                udp_msg.msg_iovlen     = 2;                            // # elements in msg_iov
                udp_msg.msg_control    = NULL;                         // Ancillary data, see below
                udp_msg.msg_controllen = 0;                            // Ancillary data buffer len
                udp_msg.msg_flags      = 0;                            // Flags on received message

                int32_t ret = sendmsg(ktgw_global.udp_pub_sock, &udp_msg, 0);

                if(ret < 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "Error \"%s\" during UDP publication.", strerror(errno));
                }

                ktgw_global.udp_pub_load += ret;
            }
        }
    }
}


static void *ktgwStatUdpSend(void *arg)
{
    struct timeval last_time;
    uint32_t seq_id = 0;


    while(ktgw_global.run)
    {
        if(fgcethGetNextStatusPub(&ktgw_global.pub_status, &last_time, &seq_id) != FGC_SUCCESS)
        {
            // TODO: Log error

            usleep(20000);

            continue;
        }

        ktgw_global.pub_head.time_sec  = htonl(last_time.tv_sec);
        ktgw_global.pub_head.time_usec = htonl(last_time.tv_usec);

        ktgwPubScanUDPClients(&last_time);
        ktgwPubScanTCPClientsUDP(&last_time);
    }

    return 0;
}


int32_t ktgwPubStart(char * subs_file)
{
    FILE       *file;
    int32_t     sock_tos;                           // Socket type of service

    if(subs_file == NULL || subs_file[0] == 0)
    {
        ktgw_global.udp_pub_enabled = 0;
        fgcethWarningLog(FGCETH_NO_DEV, "UDP subscription file not defined. UDP status publication is disabled.");
        return 0;
    }
    else
    {
        ktgw_global.udp_pub_enabled = 1;
    }

    // Create socket

    if((ktgw_global.udp_pub_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "UDP status publication socket failed");
        return 1;
    }

    // Make socket non-blocking

    if(fcntl(ktgw_global.udp_pub_sock, F_SETFL, fcntl(ktgw_global.udp_pub_sock, F_GETFL, 0) | O_NONBLOCK))
    {
        fgcethErrorLog(FGCETH_NO_DEV, "UDP status socket manipulation fcntl failed");
        close(ktgw_global.udp_pub_sock);
        return 1;
    }

    // Set IP Type-Of-Service field for socket

    sock_tos = IPTOS_LOWDELAY;
    if(setsockopt(ktgw_global.udp_pub_sock, IPPROTO_IP, IP_TOS, &sock_tos, sizeof(int32_t)) < 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "UDP status socket options setsockopt failed");
        close(ktgw_global.udp_pub_sock);
        return 1;
    }

    // Read UDP only subscriptions

    // Read global subscriptions

    if((file = fopen(subs_file, "r")) != NULL)
    {
        ktgwPubReadSubs(file, subs_file);
        fclose(file);
    }
    else
    {
        fgcethWarningLog(FGCETH_NO_DEV, "Unable to open UDP subscription file %s\n", subs_file);
    }

    // Start send thread
    
    // Configure thread attributes
     
    int32_t            thread_error;
    pthread_attr_t     thread_attr;
    struct sched_param thread_param;

    if((thread_error = pthread_attr_init(&thread_attr)) != 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "udp_pub_thread thread create(): pthread_attr_init failed with error %d\n", thread_error);
        return 1;
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&thread_attr, KTGW_THR_PUB_SCHED);

    // Configure thread parameters

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = KTGW_THR_PUB_PRIO;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    if(pthread_create(&ktgw_global.udp_pub_thread, &thread_attr, ktgwStatUdpSend, NULL))
    {
        close(ktgw_global.udp_pub_sock);
        pthread_attr_destroy(&thread_attr);
        return 1;
    }

    pthread_attr_destroy(&thread_attr);

    return 0;
}

int32_t ktgwPubClose()
{
    ktgw_global.run = 0;

    if(ktgw_global.udp_pub_enabled != 0)
    {

        pthread_join(ktgw_global.udp_pub_thread, NULL);
        close(ktgw_global.udp_pub_sock);

    }   

    return 0;
}

// EOF
