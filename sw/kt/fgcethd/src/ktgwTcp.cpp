
#include <ktgwGlobals.h>
#include <ktgwTcp.h>
#include <ktgwStack.h>
#include <ktgwParser.h>
#include <ktgwProps.h>

#include <netinet/tcp.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>
#include <poll.h>

#include <libfgceth.h>

// Local function headers

static int32_t ktgwTcpInit(void);
static void    ktgwTcpClearClient(struct ktgw_client *client);
static void   *ktgwTcpListen(void *unused);
static void   *ktgwTcpSend(void *arg);
static void   *ktgwTcpRecv(void *arg);

//typedef ktgw_stack_T<struct ktgw_extra_cmd_data *> extra_data_stack_t;
//extra_data_stack_t extra_data_stack;


// Just a dummy signal handler

void ktgwTcpIntHandler(int sig_num)
{

}



static int32_t ktgwTcpSendToClient(struct ktgw_client *client, uint32_t buffer_length)
{
    uint32_t nbytes_sent = 0;
    ssize_t  nwritten    = 0;

    if(sizeof(client->send_buffer) < buffer_length)
    {
        return -1;
    }

    while(nbytes_sent < buffer_length)
    {
        if((nwritten = send(client->sock, client->send_buffer + nbytes_sent, buffer_length - nbytes_sent, 0)) <= 0)
        {
            if(nwritten < 0 && errno == EINTR)
            {
                // EINTR means no data was sent before the error. We can try sending again

                nwritten = 0;
            }
            else
            {
                // error possibly ocurred after some data was sent. We can't recover

                fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d (%s@%s) error sending response (nwritten=%ld,errno=%d)",
                    client->id,
                    client->username,
                    client->hostname,
                    nwritten,
                    errno);
                return -2;
            }
        }

        nbytes_sent      += nwritten;
        ktgw_global.load += nwritten;
    }

    return 0;
}



int32_t ktgwTcpStart(void)
{
    struct sockaddr_in  sin;
    int                 sock_opt;           // Socket options

    // Create stack for additional command data, and fill it

    if(ktgwTcpInit() != 0) return 1;

    // Check whether server is already started

    bool s_failed = false;
    uint32_t i = 0;

    for(i = 0; i < 2; i++)
    {
        if(ktgw_global.listen_sock[i] > 0)
        {
            return 1;
        }
    }

    for(i = 0; i < 2; i++)
    {
        if((ktgw_global.listen_sock[i] = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            fgcethErrorLog(FGCETH_NO_DEV, "TCP server socket");
            s_failed = true;
            break;
        }

        // Set bind options

        memset((void *)&sin, 0, sizeof(sin));
        sin.sin_addr.s_addr = INADDR_ANY;
        sin.sin_port        = htons(i == 0? KTGW_CERN_PORT_NUM : KTGW_FREE_PORT_NUM);
        sin.sin_family      = AF_INET;

        // Allow socket re-use

        sock_opt = 1;

        if(setsockopt(ktgw_global.listen_sock[i],
                      SOL_SOCKET,
                      SO_REUSEADDR,
                      (const void *)&sock_opt, sizeof(sock_opt)) < 0)
        {
            fgcethErrorLog(FGCETH_NO_DEV, "setsockopt SO_REUSEADDR");
            close(ktgw_global.listen_sock[i]);
            ktgw_global.listen_sock[i] = -1;
            s_failed = true;
            break;
        }

        // Bind to socket

        if(bind(ktgw_global.listen_sock[i], (struct sockaddr *)&sin, sizeof(sin)) < 0)
        {
            fgcethErrorLog(FGCETH_NO_DEV, "TCP bind");
            close(ktgw_global.listen_sock[i]);
            ktgw_global.listen_sock[i] = -1;
            s_failed = true;
            break;
        }

    }

    // If first socket was already opened it needs to be closed as well

    if(s_failed && i > 0)
    {
        close(ktgw_global.listen_sock[0]);
        ktgw_global.listen_sock[0] = -1;
        return 1;
    }


    // Start listening thread

    int32_t            thread_error;
    pthread_attr_t     thread_attr;
    struct sched_param thread_param;

    // Configure thread attributes

    if((thread_error = pthread_attr_init(&thread_attr)) != 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "fgcd_thread_create(): pthread_attr_init failed with error %d\n", thread_error);
        return 1;
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&thread_attr, KTGW_THR_LISTEN_SCHED);

    // Configure thread parameters

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = KTGW_THR_LISTEN_PRIO;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    if(pthread_create(&ktgw_global.listen_thread, &thread_attr, ktgwTcpListen, NULL))
    {
        close(ktgw_global.listen_sock[0]);
        close(ktgw_global.listen_sock[1]);
        ktgw_global.listen_sock[0] = -1;
        ktgw_global.listen_sock[1] = -1;
        pthread_attr_destroy(&thread_attr);
        return 1;
    }

    pthread_attr_destroy(&thread_attr);

    return 0;

}



static int32_t ktgwTcpInit(void)
{
    struct ktgw_client  *client;
    uint32_t            i;

    ktgw_global.listen_sock[0] = -1;
    ktgw_global.listen_sock[1] = -1;
    ktgw_global.tcp_clients = 0;

    if(pipe(ktgw_global.close_p) < 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "Pipe initialization failed");
        return 1;
    }

    if(pthread_mutex_init(&ktgw_global.mutex, NULL) != 0) return 1;
    if(pthread_cond_init(&ktgw_global.cond, NULL) != 0) return 1;

    for(i = 0 ; i < KTGW_MAX_TCP_CLIENTS ; i++)
    {
        client = &ktgw_global.client[i];

        // Initialise mutex

        if(pthread_mutex_init(&client->mutex, NULL) != 0) return 1;

        ktgwTcpClearClient(client);

        client->id                = i;

        if ((client->rsp_queue   = fgcethQueueCreate(KTGW_MAX_CLIENT_CMD_BUF))   == NULL) return 1;
    }

    return 0;
}



static void ktgwTcpClearClient(struct ktgw_client *client)
{
    pthread_mutex_lock(&client->mutex);

    client->connect_time.tv_sec         = 0;
    client->connect_time.tv_usec        = 0;
    client->hostname[0]                 = '\0';
    client->last_activity_time.tv_sec   = 0;
    client->last_activity_time.tv_usec  = 0;
    client->mode                        = CMD_MODE;
    client->num_cmds                    = 0;
    client->rterm_subscribed            = false;
    client->rterm_id                    = 0;
    client->rterm_locked                = 0;
    client->run                         = 0;
    client->udp_sub_id                  = 0;
    client->udp_sub_port_num            = 0;
    client->udp_sub_period              = KTGW_PUB_UDP_DEFAULT_PERIOD;
    client->udp_sub_period_tick         = 0;
    client->udp_sub_sequence            = 0;
    client->username[0]                 = '\0';
    client->use_handshake               = true;

    client->in_use = 0;

    pthread_mutex_unlock(&client->mutex);
}



static void ktgwTcpDisconnect(struct ktgw_client *client)
{
    close(client->sock);
    fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d (%s@%s) Disconnect", client->id, client->username, client->hostname);

    if(client->rterm_subscribed)
    {
        fgcethRtermUnsubscribeDev(&client->rterm_handle);
        client->rterm_subscribed = false;
    }

    ktgwTcpClearClient(client);

    // Decrement published data client count

    pthread_mutex_lock(&ktgw_global.mutex);
    ktgw_global.tcp_clients--;
    pthread_cond_signal(&ktgw_global.cond);
    pthread_mutex_unlock(&ktgw_global.mutex);
}



void ktgwTcpClose()
{
    struct ktgw_client         *client;
    uint32_t                   i;

    ktgw_global.run = 0;

    if(write(ktgw_global.close_p[1], "", sizeof("")) < 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "Pipe signaling (to close) failed");
        exit(1);
    }

    pthread_join(ktgw_global.listen_thread, NULL);

    // Guarantee that all threads have exited!

    pthread_mutex_lock(&ktgw_global.mutex);

    while(ktgw_global.tcp_clients > 0)
    {
        pthread_cond_wait(&ktgw_global.cond, &ktgw_global.mutex);
    }

    pthread_mutex_unlock(&ktgw_global.mutex);

    for(i = 0 ; i < KTGW_MAX_TCP_CLIENTS ; i++)
    {
        client = &ktgw_global.client[i];

        pthread_mutex_destroy(&client->mutex);
        fgcethQueueDestroy(client->rsp_queue);
    }

}



static void *ktgwTcpListen(void *)
{
    struct ktgw_client  *client;
    struct sockaddr_in  from;
    uint32_t            i;
    socklen_t           length;
    int                 sock;
    int                 sock_opt;

    // Ignore SIGPIPE

    sigset_t sig;
    sigemptyset(&sig);
    sigaddset(&sig,SIGPIPE);
    pthread_sigmask(SIG_BLOCK,&sig,NULL);

    // Poll options

    static struct pollfd poll_opts[3];
    poll_opts[0].fd      = ktgw_global.listen_sock[0];
    poll_opts[0].events  = POLLIN;
    poll_opts[0].revents = 0;
    // Configure pipe required for receiving termination message
    poll_opts[1].fd      = ktgw_global.listen_sock[1];
    poll_opts[1].events  = POLLIN;
    poll_opts[1].revents = 0;

    poll_opts[2].fd      = ktgw_global.close_p[0];
    poll_opts[2].events  = POLLIN;
    poll_opts[2].revents = 0;

    listen(ktgw_global.listen_sock[0], KTGW_MAX_TCP_CLIENTS);
    listen(ktgw_global.listen_sock[1], KTGW_MAX_TCP_CLIENTS);

    while(ktgw_global.listen_sock[0] >= 0 && ktgw_global.listen_sock[1] >= 0 && ktgw_global.run)
    {
        // Accept a connection

        length = sizeof(from);

        if (poll(poll_opts, 3, -1) == -1 && errno != EINTR)    // Poll without timeout
        {
            continue;
        }

        if(!ktgw_global.run)
        {
            break;
        }

        for(uint32_t fd = 0; fd < 2; fd++)
        {
            if(poll_opts[fd].revents)
            {

                if((sock = accept(ktgw_global.listen_sock[fd], (struct sockaddr *)&from, &length)) < 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "accept");
                    continue;
                }

                // Increment connections received

                ktgw_global.connections_recvd++;

                // Find a free client structure

                for(i = 0 ; i < KTGW_MAX_TCP_CLIENTS && ktgw_global.client[i].in_use ; i++);

                // Check whether all client structures are in use

                if(i >= KTGW_MAX_TCP_CLIENTS)
                {
                    // Send a '-' to indicate that the server is busy

                    if(fd == 0)
                    {
                        send(sock, "-", 1, 0);
                    }
                    close(sock);

                    fgcethWarningLog(FGCETH_NO_DEV, "TCP Connection rejected, no free client structures");
                    continue;
                }

                // A free client structure was found

                client = &ktgw_global.client[i];
                ktgwTcpClearClient(client);

                if(fd == 0)
                {
                    client->use_handshake = true;
                }
                else
                {
                    client->use_handshake = false;
                }

                client->address = from.sin_addr;
                client->in_use  = 1;
                client->run     = 1;
                client->sock    = sock;

                fgcethGetCurrentTime(&client->connect_time);
                client->last_activity_time = client->connect_time;

                // Resolve client's address to a host name

                if(getnameinfo((struct sockaddr *)&from, sizeof(from), client->hostname, sizeof(client->hostname), NULL, 0, 0))
                {
                    // Resolution failed - disconnect the client

                    fgcethWarningLog(FGCETH_NO_DEV, "TCP %d Connection rejected, unable to resolve hostname", client->id);

                    // Send a '-' to indicate that the server rejected the connection

                    if(client->use_handshake)
                    {
                        send(client->sock, "-", 1, 0);
                    }

                    close(client->sock);
                    ktgwTcpClearClient(client);
                    continue;
                }

                // Increment published data client count

                pthread_mutex_lock(&ktgw_global.mutex);
                ktgw_global.tcp_clients++;
                pthread_mutex_unlock(&ktgw_global.mutex);

                // Send a '+' to indicate that the connection may proceed

                if(client->use_handshake)
                {
                    if(send(client->sock, "+", 1, 0) != 1)
                    {
                        // Close the socket

                        fgcethWarningLog(FGCETH_NO_DEV, "TCP %d (%s@%s) Send failed", client->id, client->username, client->hostname);
                        ktgwTcpDisconnect(client);
                        continue;
                    }
                }


                // Enable keep-alive for the socket

                sock_opt = 1;
                if(setsockopt(client->sock,
                              SOL_SOCKET,
                              SO_KEEPALIVE,
                              (const void *)&sock_opt, sizeof(sock_opt)) < 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "TCP %d (%s@%s) Set keep-alive failed", client->id, client->username, client->hostname);
                    fgcethWarningLog(FGCETH_NO_DEV, "setsockopt SO_KEEPALIVE");
                    ktgwTcpDisconnect(client);
                    continue;
                }

                // Set timeout for the send to client function

                struct timeval send_timeout;
                send_timeout.tv_sec  = KTGW_SNDTIMEO_SEC;
                send_timeout.tv_usec = 0;

                if(setsockopt(client->sock,
                              SOL_SOCKET,
                              SO_SNDTIMEO,
                              (const void *)&send_timeout, sizeof(send_timeout)) < 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "TCP %d (%s@%s) Setting of sending timout failed", client->id, client->username, client->hostname);
                    fgcethWarningLog(FGCETH_NO_DEV, "setsockopt SO_SNDTIMEO");
                    ktgwTcpDisconnect(client);
                    continue;
                }

                // Disable Nagle's algorithm for the socket

                sock_opt = 1;
                if(setsockopt(client->sock,
                              IPPROTO_TCP,
                              TCP_NODELAY,
                              (const void *)&sock_opt, sizeof(sock_opt)) < 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "TCP %d (%s@%s) Disabling of Nagle's algorithm failed", client->id, client->username, client->hostname);
                    fgcethWarningLog(FGCETH_NO_DEV, "setsockopt TCP_NODELAY");
                    ktgwTcpDisconnect(client);
                    continue;
                }

                // Spawn receive thread

                int32_t            thread_error;
                pthread_attr_t     thread_attr;
                struct sched_param thread_param;

                // Configure thread attributes

                if((thread_error = pthread_attr_init(&thread_attr)) != 0)
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "pthread_attr_init failed with error %d\n", thread_error);
                    ktgwTcpDisconnect(client);
                    continue;
                }

                pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
                pthread_attr_setschedpolicy (&thread_attr, KTGW_THR_CLIENT_SCHED);

                // Configure thread parameters

                sched_getparam(getpid(), &thread_param);
                thread_param.sched_priority = KTGW_THR_CLIENT_PRIO;
                pthread_attr_setschedparam(&thread_attr, &thread_param);

                if(pthread_create(&client->recv_thread, &thread_attr, ktgwTcpRecv, client))
                {
                    fgcethWarningLog(FGCETH_NO_DEV, "TCP %d (%s@%s) Start receive thread failed", client->id, client->username, client->hostname);
                    ktgwTcpDisconnect(client);
                    pthread_attr_destroy(&thread_attr);
                    continue;
                }

                pthread_attr_destroy(&thread_attr);
            }
        }

    }

    for(i = 0 ; i < KTGW_MAX_TCP_CLIENTS ; i++)
    {
        if(ktgw_global.client[i].in_use)
        {
            ktgw_global.client[i].run = 0;
            pthread_kill(ktgw_global.client[i].recv_thread, SIGTERM);
            pthread_kill(ktgw_global.client[i].send_thread, SIGTERM);
        }
    }

    if(ktgw_global.listen_sock[0] >= 0) close(ktgw_global.listen_sock[0]);
    if(ktgw_global.listen_sock[1] >= 0) close(ktgw_global.listen_sock[1]);

    fgcethLog(FGCETH_NO_DEV, L_V0, "TCP tcpListen(): tcp.listen_socks = (%d,%d). Exiting thread.", ktgw_global.listen_sock[0], ktgw_global.listen_sock[1]);

    return 0;
}



static void *ktgwTcpSend(void *arg)
{
    int                        buffer_length;
    ssize_t                    bytes_out;
    struct fgceth_rsp         *response;
    struct ktgw_client        *client = reinterpret_cast<ktgw_client *>(arg);
    fgceth_errno               err;

    // Ignore SIGPIPE

    sigset_t sig;
    sigemptyset(&sig);
    sigaddset(&sig,SIGPIPE);
    pthread_sigmask(SIG_BLOCK,&sig,NULL);

    // Use SIGTERM to force a thread to terminate

    struct sigaction sig_term;
    memset(&sig_term, 0, sizeof(sig_term));
    sig_term.sa_handler = ktgwTcpIntHandler;
    sigemptyset(&sig_term.sa_mask);
    sigaddset(&sig_term.sa_mask, SIGTERM);
    sigaction(SIGTERM, &sig_term, NULL);

    // Process responses

    while(client->run)
    {
        if((response = fgcethQueuePop(client->rsp_queue)) == NULL)
        {
            if (!client->run)
            {
                // Client closed the connection.

                break;
            }
            else
            {
                // Wait one cycle and poll again

                usleep(20*1000);

                continue;
            }

        }

        // Handle different types of structure

        switch(response->type)
        {
            case CMD_RSP: // Response is a command structure

                // Process command if remote terminal not in progress

                if(client->rterm_id == 0)
                {
                    // Switch client mode if necessary

                    if(client->mode != CMD_MODE)
                    {
                        // Enable telnet client line-mode and local echo, and clear screen

                        if((err = fgcethRtermUnsubscribeDev(&client->rterm_handle)) != FGC_SUCCESS)
                        {
                            fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d remote terminal (dev %d) unsubscription failed with error %d (%s)", client->id, client->rterm_id, err, fgcethStrerr(err));
                        }

                        client->rterm_subscribed = false;

                        send(client->sock, "\xFF\xFC\x22\xFF\xFC\x01\33c", 8, 0);

                        client->mode = CMD_MODE;
                    }

                    // Fill the response buffer with the formatted response

                    if(response->d.cmd_rsp.error > FGC_OK_RSP) // Response is an error
                    {

                        fgcethLog(fgcethGetDevId(response->d.cmd_rsp.dev_name), L_V0, "TCP %d Request failed: %s", client->id, fgcethStrerr(response->d.cmd_rsp.error));

                        buffer_length = snprintf(client->send_buffer, sizeof(client->send_buffer),
                                                 "$%s !\n%d %s\n;", response->d.cmd_rsp.tag,
                                                 response->d.cmd_rsp.error, fgcethStrerr(response->d.cmd_rsp.error));

                        // Send error response to client

                        if(ktgwTcpSendToClient(client, buffer_length) < 0)
                        {
                            // If failed, terminate connection

                            client->run = 0;
                            pthread_kill(client->recv_thread, SIGTERM);
                            break;
                        }

                    }
                    else // Response is data
                    {
                        // Send tag to client

                        buffer_length = snprintf(client->send_buffer, sizeof(client->send_buffer), "$%s .\n", response->d.cmd_rsp.tag);

                        if(ktgwTcpSendToClient(client, buffer_length) < 0)
                        {
                            // If failed, terminate connection

                            client->run = 0;
                            pthread_kill(client->recv_thread, SIGTERM);
                            break;
                        }

                        // Return value for get commands

                        if(response->d.cmd_rsp.cmd_type == GET)
                        {
                            if(blkbufWriteToFd(client->sock, response->d.cmd_rsp.response) < 0)
                            {
                                fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d (%s@%s) error sending response (errno=%d)",
                                    client->id,
                                    client->username,
                                    client->hostname,
                                    errno);

                                if(errno != EINTR)
                                {
                                    // If failed, terminate connection

                                    client->run = 0;
                                    pthread_kill(client->recv_thread, SIGTERM);
                                }

                                break;
                            }
                        }

                        // Send end of response to client

                        buffer_length = snprintf(client->send_buffer, sizeof(client->send_buffer), "\n;");

                        if(ktgwTcpSendToClient(client, buffer_length) < 0)
                        {
                            // If failed, terminate connection

                            client->run = 0;
                            pthread_kill(client->recv_thread, SIGTERM);
                            break;
                        }
                    }
                }

                break;

            case RTERM: // Response is a remote terminal response structure

                // Process remote terminal response if remote terminal in progress

                if(client->rterm_id != 0)
                {
                    // Switch client mode if necessary

                    if(client->mode != RTERM_MODE)
                    {
                        // Disable telnet client line-mode and local echo

                        send(client->sock, "\xFF\xFD\x22\xFF\xFB\x01", 6, 0);
                        client->mode = RTERM_MODE;

                        fgcethLog(response->d.rterm.dev_id, L_V0, "TCP %d (%s@%s) rterm",
                                  client->id, client->username, client->hostname);
                    }

                    if((bytes_out = send(client->sock,
                                         response->d.rterm.buffer,
                                         response->d.rterm.length,
                                         0)) > 0)
                    {
                        ktgw_global.load += bytes_out;
                    }
                }

                break;

            default:
                break;
        }

        // Return response structure to its queue

        if(response->type == CMD_RSP)
        {
            pthread_mutex_lock(&client->mutex);
            client->num_cmds--;
            pthread_mutex_unlock(&client->mutex);
        }

        fgcethDestroyResponse(response);
        response = NULL;
    }

    // Return current receive command structure to the free queue
    // NOT NECESSARY - Will be freed at end of this function
    /*if(response)
    {
        fgcethDestroyResponse(response);
        response = NULL;
    }*/

    // Check whether a remote terminal is in progress for this client

    if(client->rterm_id != 0)
    {
        // Disconnect remote terminal

        fgcethLog(client->id, L_V0, "TCP %d (%s@%s) Remote terminal disconnected",
                  client->id, client->username, client->hostname);
        client->rterm_id = 0;
    }

    // Close the socket

    //close(client->sock);

    // Wait for the receive thread to be finsihed (i.e. no more TCP command are sent to the cmdqmgr)

    pthread_join(client->recv_thread, NULL);

    if(client->rterm_subscribed)
    {
        // This guarantees that no more RTERM responses will be appended to the queue.

        fgcethRtermUnsubscribeDev(&client->rterm_handle);
        client->rterm_subscribed = false;
    }

    // Return structures to free queues.
    // Note that after this point we do not need to protect the num_cmds and num_rterm_resps

    if(response == NULL)
    {
        response = fgcethQueuePop(client->rsp_queue);
    }

    while(client->num_cmds > 0 || response != NULL)
    {

        if(response != NULL)
        {   
            if(response->type == CMD_RSP)
            {
                pthread_mutex_lock(&client->mutex);
                client->num_cmds--;
                pthread_mutex_unlock(&client->mutex);
            }

            fgcethDestroyResponse(response);
        }
        else
        {
            usleep(20 * 1000);
        }

        response = fgcethQueuePop(client->rsp_queue); //Try to get a new response
    }

    // Disconnect client

    ktgwTcpDisconnect(client);

    // Detach this thread

    pthread_detach(client->send_thread);

    return 0;
}


/*
 * Socket receive thread
 */

static void *ktgwTcpRecv(void *arg)
{
    ssize_t            bytes_in;
    uint32_t           send_idx;
    uint32_t           i;
    struct timeval     time;
    fgceth_errno       err;

    struct ktgw_client     *client = reinterpret_cast<ktgw_client*>(arg);
    struct ktgw_parsed_cmd *parser = &client->parser;

    // Ignore SIGPIPE

    sigset_t sig;
    sigemptyset(&sig);
    sigaddset(&sig,SIGPIPE);
    pthread_sigmask(SIG_BLOCK,&sig,NULL);

    struct sigaction sig_term;
    memset(&sig_term, 0, sizeof(sig_term));
    sig_term.sa_handler = ktgwTcpIntHandler;
    sigemptyset(&sig_term.sa_mask);
    sigaddset(&sig_term.sa_mask, SIGTERM);
    sigaction(SIGTERM, &sig_term, NULL);

    fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d Connect %s", client->id, client->hostname);

    // Receive a '+' to confirm that client is an FGC client

    if(client->use_handshake)
    {
        if((bytes_in = recv(client->sock, client->recv_buffer, 1, 0)) > 0)
        {
            if(client->recv_buffer[0] != '+')
            {
                fgcethWarningLog(FGCETH_NO_DEV, "TCP %d Protocol error", client->id);

                ktgwTcpDisconnect(client);
                return 0;    // return value is not checked
            }
        }
        else
        {
            ktgwTcpDisconnect(client);
            return 0;    // return value is not checked
        }
    }

    // Spawn send thread

    int32_t            thread_error;
    pthread_attr_t     thread_attr;
    struct sched_param thread_param;

    // Configure thread attributes

    if((thread_error = pthread_attr_init(&thread_attr)) != 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "pthread_attr_init failed with error %d\n", thread_error);
        ktgwTcpDisconnect(client);
        return 0;
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&thread_attr, KTGW_THR_CLIENT_SCHED);

    // Configure thread parameters

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = KTGW_THR_CLIENT_PRIO;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    if(pthread_create(&client->send_thread, &thread_attr, ktgwTcpSend, client))
    {
        fgcethWarningLog(FGCETH_NO_DEV, "TCP %d (%s@%s) Start send thread failed", client->id, client->username, client->hostname);
        ktgwTcpDisconnect(client);
        pthread_attr_destroy(&thread_attr);
        return 0;    // return value is not checked
    }

    pthread_attr_destroy(&thread_attr);

    // Handle commands

    ktgwRestartParser(parser);
    send_idx = 0;

    fgceth_rsp * command = NULL;

    while(client->run)
    {
        if((bytes_in = recv(client->sock,
                            client->recv_buffer,
                            client->mode == RTERM_MODE ? 1 : sizeof(client->recv_buffer) - 1,
                            0)) <= 0)
        {
            break;
        }

        if(!client->run) break;

        client->recv_buffer[bytes_in] = '\0';

        // Add bytes to TCP load

        ktgw_global.load += bytes_in;

        // Get current time

        fgcethGetCurrentTime(&time);

        // Set last activity time

        client->last_activity_time = time;

        // Check client mode

        switch(client->mode)
        {
            case CMD_MODE:      // Client is in command mode

                send_idx = 0;

                do
                {

                    uint32_t prev_send_idx = send_idx;

                    if(command == NULL)
                    {
                        command = fgcethCreateResponse();

                        if(command == NULL) break; // TODO: Do this ????
                    }

                    send_idx += ktgwParseCmd(&client->recv_buffer[send_idx], bytes_in - send_idx, parser, command);

                    fgcethLog(FGCETH_NO_DEV, L_V3, "[Parsing]: %.*s", send_idx - prev_send_idx, &client->recv_buffer[prev_send_idx]);

                    if(parser->completed) // new command was parsed and
                    {
                        pthread_mutex_lock(&client->mutex);
                        client->num_cmds++;
                        pthread_mutex_unlock(&client->mutex);

                        strcpy(command->d.cmd_rsp.client_name, client->hostname);

                        fgcethLog(FGCETH_NO_DEV, L_V2, "Command parsing completed:");
                        fgcethLog(FGCETH_NO_DEV, L_V2, "    TAG    : %s",      command->d.cmd_rsp.tag);
                        fgcethLog(FGCETH_NO_DEV, L_V2, "    Type   : %s",      command->d.cmd_rsp.cmd_type == SET || command->d.cmd_rsp.cmd_type == SET_BIN ? "SET" : command->d.cmd_rsp.cmd_type == GET ? "GET" : "???");
                        fgcethLog(FGCETH_NO_DEV, L_V2, "    Device : %s (%u)", command->d.cmd_rsp.dev_name, fgcethGetDevId(command->d.cmd_rsp.dev_name));
                        fgcethLog(FGCETH_NO_DEV, L_V2, "    Command: %s",      command->d.cmd_rsp.property);

                        if(parser->val_index)
                        {
                            fgcethLog(FGCETH_NO_DEV, L_V2, "    Value  : [...]");
                        }
                        else
                        {
                            fgcethLog(FGCETH_NO_DEV, L_V2, "    Value  :");
                        }

                        if(!parser->valid || command->d.cmd_rsp.error > FGC_OK_RSP) // If not valid, send error to queue
                        {
                            fgcethQueueBlockPush(client->rsp_queue, command);
                        }
                        else
                        {
                            // TODO !!!!!!  Verify GW proprieties: Rterm, LockRterm and RBAC

                            if(fgcethGetDevId(command->d.cmd_rsp.dev_name) == 0) // GW commands
                            {
                                // TODO: Validate if this is not too unrestrictive

                                char val_buffer[32];
                                blkbufCopyStr(val_buffer, command->d.cmd_rsp.value_opts, sizeof(val_buffer));

                                if(!strcasecmp(command->d.cmd_rsp.property, "CLIENT.RTERM") && command->d.cmd_rsp.cmd_type == SET)
                                {
                                    if(!SetRterm(client, val_buffer, command->d.cmd_rsp.tag))
                                    {
                                        err = FGC_OK_RSP;
                                    }
                                    else
                                    {
                                        err = FGC_UNKNOWN_ERROR_CODE; // TODO: Add proper error code
                                    }
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "CLIENT.RTERMLOCK") && command->d.cmd_rsp.cmd_type == SET)
                                {
                                    if(!SetRtermLock(client, val_buffer, command->d.cmd_rsp.tag))
                                    {
                                        err = FGC_OK_RSP;
                                    }
                                    else
                                    {
                                        err = FGC_UNKNOWN_ERROR_CODE; // TODO: Add proper error code
                                    }
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "CLIENT.TOKEN") && (command->d.cmd_rsp.cmd_type == SET_BIN || command->d.cmd_rsp.cmd_type == SET))
                                {
                                    err = FGC_OK_RSP;
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "CLIENT.UDP.SUB.ID") && command->d.cmd_rsp.cmd_type == SET)
                                {
                                    if(!SetSubId(client, command->d.cmd_rsp.value_opts, &err))
                                    {
                                        err = FGC_OK_RSP;
                                    }
                                    else
                                    {
                                        // err will contain erro code
                                    }
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "CLIENT.UDP.SUB.PERIOD") && command->d.cmd_rsp.cmd_type == SET)
                                {
                                    if(!SetSubPeriod(client, command->d.cmd_rsp.value_opts, &err))
                                    {
                                        err = FGC_OK_RSP;
                                    }
                                    else
                                    {
                                        // err will contain erro code
                                    }
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "CLIENT.UDP.SUB.PORT") && command->d.cmd_rsp.cmd_type == SET)
                                {
                                    if(!SetSubPort(client, command->d.cmd_rsp.value_opts, &err))
                                    {
                                        err = FGC_OK_RSP;
                                    }
                                    else
                                    {
                                        // err will contain erro code
                                    }
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "FGCD.TIMING.USERS") && (command->d.cmd_rsp.cmd_type == GET))
                                {
                                    err = FGC_OK_RSP;
                                }
                                else if(!strcasecmp(command->d.cmd_rsp.property, "GW.READCODES") && command->d.cmd_rsp.cmd_type == SET)
                                {
                                    if(!SetReadCodes())
                                    {
                                        err = FGC_OK_RSP;
                                    }
                                    else
                                    {
                                        err = FGC_ERROR_READING_FILE;
                                    }
                                }
                                else
                                {
                                    err = FGC_UNKNOWN_SYM; //This error will be handled by powerspy
                                }

                                command->d.cmd_rsp.error = err;
                                fgcethQueueBlockPush(client->rsp_queue, command);

                            }
                            else // FGC commands
                            {
                                if((err = fgcethGetSetAsync(command, client->rsp_queue)) != FGC_SUCCESS)
                                {
                                    command->d.cmd_rsp.error = err;
                                    fgcethQueueBlockPush(client->rsp_queue, command);
                                }
                            }

                        }

                        ktgwRestartParser(parser);
                        command = NULL;

                    }
                    else
                    {
                        fgcethLog(FGCETH_NO_DEV, L_V3, "Command parsing not yet completed...");
                    }
                }
                while(send_idx < bytes_in);

                break;

            case RTERM_MODE:    // Client is in remote terminal mode

                // Add characters to device's queue

                for(i = 0 ; i < bytes_in ; i++)
                {
                    // Check for disconnect character if rterm not locked

                    if(!client->rterm_locked && client->recv_buffer[i] == RTERM_DISCONNECT_CHAR)
                    {
                        // Enable telnet client line-mode and local echo, and clear screen

                        if((err = fgcethRtermUnsubscribeDev(&client->rterm_handle)) != FGC_SUCCESS)
                        {
                            fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d remote terminal (dev %d) unsubscription failed with error %d (%s)", client->id, client->rterm_id, err, fgcethStrerr(err));
                        }

                        client->rterm_subscribed = false;

                        const char str_0[] = "\xFF\xFC\x22\xFF\xFC\x01\33c";
                        const char str_1[] = "Remote terminal disconnected.\n";

                        send(client->sock, str_0, sizeof(str_0), 0);
                        send(client->sock, str_1, sizeof(str_1), 0);

                        client->mode = CMD_MODE;
                        client->rterm_id = 0;

                        fgcethWarningLog(client->rterm_id, "TCP %d (%s@%s) Remote terminal disconnected", client->id, client->username, client->hostname);

                        break;
                    }
                    else // Character is not disconnect character
                    {

                        if(fgcethRtermSendToDev(client->rterm_id, &client->recv_buffer[i], 1) != FGC_SUCCESS)
                        {
                            if((err = fgcethRtermUnsubscribeDev(&client->rterm_handle)) != FGC_SUCCESS)
                            {
                                fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d remote terminal (dev %d) unsubscription failed with error %d (%s)", client->id, client->rterm_id, err, fgcethStrerr(err));
                            }

                            client->rterm_subscribed = false;

                            const char str_0[] = "\xFF\xFC\x22\xFF\xFC\x01\33c";
                            const char str_1[] = "Unable to send remote terminal characters. Remote terminal disconnected.\n";

                            send(client->sock, str_0, sizeof(str_0), 0);
                            send(client->sock, str_1, sizeof(str_1), 0);

                            client->mode = CMD_MODE;
                            client->rterm_id = 0;

                            fgcethWarningLog(client->rterm_id, "Unable to send remote terminal characters. TCP %d (%s@%s) Remote terminal disconnected", client->id, client->username, client->hostname);

                            break;
                        }

                    }
                }

                break;

            default:

                break;
        }
    }

    // Stop the tcpSend thread by changing the run flag

    client->run = 0;

    // Sleep for a couple of 20 ms cycles (FGC_Ether and WFIP), so that there are no more remote terminal commands

    usleep(40*1000);

    return 0;
}

//EOF
