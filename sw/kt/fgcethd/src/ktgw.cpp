/*!
 * © Copyright CERN 2018. All rights not expressly granted are reserved.
 * @file   ktgw.cpp
 * @brief  Knowledge Transfer Gateway
 * @author Joao Afonso
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <termios.h>
#include <getopt.h>

#include <ktgwGlobals.h>
#include <ktgwTcp.h>
#include <ktgwParser.h>
#include <ktgwStatPub.h>
#include <ktgwRt.h>
#include <ktgwVersion.h>

#include <libfgceth.h>


struct ktgw ktgw_global;

#define ENABLE_LOG_FILES 0

#define KTGW_VERBOSE_TIME 900 //15 minutes
#define KTGW_MAX_LOG_LVL  4

volatile int terminate = 0;
volatile int log_lvl = 0;
volatile int log_req = 0;

#define MB(x) ((uint32_t) (x) << 20)

void CleanUp(int sig_num)
{
	int log_l;

	log_l = log_lvl;

	if(sig_num == SIGUSR1) //Debug signal
	{
		log_lvl = (log_l + 1) > KTGW_MAX_LOG_LVL ? KTGW_MAX_LOG_LVL : (log_l + 1);
		log_req = 1;
	}
	else if(sig_num == SIGUSR2) //Debug signal
	{
		log_lvl = (log_l - 1) < -KTGW_MAX_LOG_LVL ? -KTGW_MAX_LOG_LVL : (log_l - 1);
		log_req = 1;
	}
	else
	{
		terminate = 1;
	}

}

void printUsage()
{
    fprintf(stderr, "Usage:\n"
    		        " -e         [or --ethname        ] 'arg' -->            ethernet name\n"
    		        " -n         [or --namesfile      ] 'arg' -->            path to names file\n"
    		        " -c         [or --codespath      ] 'arg' --> (optional) path to code files folder\n"
    		        " -g         [or --localhost      ] 'arg' --> (optional) local host name\n"
                    " -u         [or --udpfile        ] 'arg' --> (optional) udp pub clients file\n"
                    " -m         [or --allocmem       ] 'arg' --> (optional) memory allocated for responses (MB) [default 1024]\n"
#if ENABLE_LOG_FILES
    		        " -l         [or --logfile        ] 'arg' --> (optional) log file\n"
    		        " -i         [or --initfile       ] 'arg' --> (optional) init log file\n"
#endif
    		        " -f         [or --fl             ]       --> (optional) enable the FGC Logger\n"
    				" -v,vv,...  [or --verbose=[0..4] ]       --> (optional) verbose level\n"
                    " -v (only)  [or --version        ]       --> (optional) print version\n"
    );
}

void printVersion()
{
    printf("FGC Ether Gateway build info:\n");
    printf("> Version (Tarball)      : %ld\n", version.time_tar.tv_sec);
    printf("> Version (Build)        : %ld\n", version.time.tv_sec);
    printf("> Hostname               : %s\n", version.hostname);
    printf("> Architecture           : %s\n", version.architecture);
    printf("> Compiler version (C)   : %s\n", version.cc_version);
    printf("> Compiler version (C++) : %s\n", version.cxx_version);
    printf("> User group             : %s\n", version.user_group);
    printf("> Directory              : %s\n", version.directory);
}

void setLogLvl(int log_new_lvl)
{
	switch(log_new_lvl)
	{
		case 0:
			fgcethSetLogPrintFlags(L_V0);
			break;
		case 1:
			fgcethSetLogPrintFlags(L_V1);
			break;
		case 2:
			fgcethSetLogPrintFlags(L_V2);
			break;
		case 3:
			fgcethSetLogPrintFlags(L_V3);
			break;
		case 4:
			fgcethSetLogPrintFlags(L_V4);
			break;
		default:
			break;
	}
}


/**************************************
 * Main thread
***************************************/

int main(int argc, char* argv[])
{
	int 	log_base_lvl = 0;
	int     log_counter  = 0;

	int     option;
	//char    arg_names_file[128] = {0};
	//char    arg_codes_path[128] = {0};
	char    arg_gw_name[128]    = {0};
	char    arg_eth_name[128]   = {0};

    uint32_t alloc_mem = 1024;

#if ENABLE_LOG_FILES
	char    arg_log_file[128]   = {0};
	char    arg_log_i_file[128] = {0};
	FILE    *f  = NULL;
	FILE    *fi = NULL;
	FILE    *fl = NULL;
#endif

	struct fgceth_names_container names_container;
	struct fgceth_codes_container codes_container;

	static struct option long_options[] =
	{
	  /* These options set a flag. */
	  {"ethname",   required_argument, NULL,         'e'},
	  {"namesfile", required_argument, NULL,         'n'},
      {"udpfile",   required_argument, NULL,         'u'},
	  {"codespath", required_argument, NULL,         'c'},
	  {"localhost", required_argument, NULL,         'g'},
      {"allocmem",  required_argument, NULL,         'm'},
#if ENABLE_LOG_FILES
	  {"logfile",   required_argument, NULL,         'l'},
	  {"initfile",  required_argument, NULL,         'i'},
#endif
	  {"fl",        no_argument,       NULL,         'f'},
	  {"verbose",   optional_argument, NULL,         'v'},
      {"version",   no_argument,       NULL,         'v'},     
	  {NULL, 0, NULL, 0}
	};

	terminate    = 0;

	log_base_lvl = log_lvl = 0;
	log_counter  = 0;
	log_req      = 0;

	ktgw_global.arg_names_file[0] = 0;
	ktgw_global.arg_codes_path[0] = 0;
    ktgw_global.arg_udp_c_path[0] = 0;

	/// Error variable

	fgceth_errno    err;

	// Read parameters from the command line
#if ENABLE_LOG_FILES
	while ((option = getopt_long(argc, argv, "e:n:u:c:g:m:l:i:fv::", long_options, NULL)) != -1)
#else
	while ((option = getopt_long(argc, argv, "e:n:u:c:g:m:fv::", long_options, NULL)) != -1)
#endif
	{
		switch (option)
		{
			 case 'e':
				 strcpy(arg_eth_name, optarg);
				 break;
			 case 'n':
				 strcpy(ktgw_global.arg_names_file, optarg);
				 break;
			 case 'c':
				 strcpy(ktgw_global.arg_codes_path, optarg);
				 break;
             case 'u':
                 strcpy(ktgw_global.arg_udp_c_path, optarg);
                 break;
			 case 'g':
				 strcpy(arg_gw_name, optarg);
				 break;
             case 'm':
                 alloc_mem = atoi(optarg);
                 break;
#if ENABLE_LOG_FILES
			 case 'l':
			 	 strcpy(arg_log_file, optarg);
			 	 break;
			 case 'i':
			 	 strcpy(arg_log_i_file, optarg);
			 	 break;
#endif
			 case 'f':
				 fgcethEnableFGCLogger(true);
				 break;
			 case 'v':
				 if(!optarg || atoi(optarg) == 1)
				 {
                     if(argc == 2)
                     {
                         // Special case where -v|--version is the only argument
                         // Print version and exit!
                         printVersion();
                         exit(0);
                     }

					 log_lvl     = log_base_lvl = 1;
				 }
				 else if(!strcasecmp("v", optarg) || atoi(optarg) == 2)
				 {
					 log_lvl     = log_base_lvl = 2;
				 }
				 else if(!strcasecmp("vv", optarg) || atoi(optarg) == 3)
				 {
					 log_lvl     = log_base_lvl = 3;
				 }
				 else if(!strcasecmp("vvv", optarg) || atoi(optarg) == 4)
				 {
					 log_lvl     = log_base_lvl = 4;
				 }
				 break;
			 //case 't':
			 //	 arg_run_test = 1;
			 //	 break;
			 case ':':
			 case '?':
			 default :
				 printUsage();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));
	memset(&codes_container, 0, sizeof(codes_container));

	// Check for required arguments

	if (arg_eth_name[0] == 0 || ktgw_global.arg_names_file[0] == 0 || alloc_mem == 0)
	{
		printUsage();
		exit(1);
	}

	// Check optional gateway name argument

	if (arg_gw_name[0] == 0)
	{
		gethostname(arg_gw_name, sizeof(arg_gw_name));
	}

	// Check optional log file argument

#if ENABLE_LOG_FILES
	if (arg_log_file[0] != 0 && arg_log_i_file[0] != 0 && !strcmp(arg_log_file, arg_log_i_file))
	{
		// If both files have the same name

		f = fopen(arg_log_file, "w");
		fgcethSetLogFileExec(f);
		fgcethSetLogFileInit(f);
	}
	else
	{
		// Check log file option

		if (arg_log_file[0] != 0)
		{
			fl = fopen(arg_log_file, "w");
			fgcethSetLogFileExec(fl);
		}

		// Check init log file option

		if (arg_log_i_file[0] != 0)
		{
			fi = fopen(arg_log_i_file, "w");
			fgcethSetLogFileInit(fi);
		}
	}
#endif

	// Check verbosity, before calling --ANY-- FGC Ether related functions

	fgcethDetailedLog(false);
	setLogLvl(log_base_lvl);

	// Check names file

	fgcethLog(FGCETH_NO_DEV, L_V0, "Reading name file ...");

	if((err = fgcethNamesParser(ktgw_global.arg_names_file, arg_gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser failed with error %d (%s)", err, fgcethStrerr(err));
		exit(1);
	}

	fgcethLog(FGCETH_NO_DEV, L_V0, "Completed.");

	// Check codes folder

	if(ktgw_global.arg_codes_path[0] != 0)
	{
		fgcethLog(FGCETH_NO_DEV, L_V0, "Reading code files...");

		if((err = fgcethCodesParser(ktgw_global.arg_codes_path, &codes_container)) != FGC_SUCCESS)
		{
			fgcethErrorLog(FGCETH_NO_DEV, "fgcethCodesParser failed with error %d (%s)", err, fgcethStrerr(err));
			exit(1);
		}

		fgcethLog(FGCETH_NO_DEV, L_V0, "Completed.");
	}

	/****************************************************************************************/

	// Other variables initializaton

	ktgw_global.run = 1; // Enable thread activation!

	/****************************************************************************************/

	// Catch signals to clean-up

	struct sigaction sig;

	memset(&sig, 0, sizeof(sig));
	sig.sa_handler = CleanUp;

	sigemptyset(&sig.sa_mask);
	sigaddset(&sig.sa_mask, SIGHUP);
	sigaddset(&sig.sa_mask, SIGINT);
	sigaddset(&sig.sa_mask, SIGPIPE);
	sigaddset(&sig.sa_mask, SIGQUIT);
	sigaddset(&sig.sa_mask, SIGTERM);
	sigaddset(&sig.sa_mask, SIGUSR1);
	sigaddset(&sig.sa_mask, SIGUSR2);

	sigaction(SIGHUP,  &sig, NULL);
	sigaction(SIGINT,  &sig, NULL);
	sigaction(SIGPIPE, &sig, NULL);
	sigaction(SIGQUIT, &sig, NULL);
	sigaction(SIGTERM, &sig, NULL);
	sigaction(SIGUSR1, &sig, NULL);
	sigaction(SIGUSR2, &sig, NULL);

    /****************************************************************************************/

    // Set thread priority and scheduling

    int32_t             pthread_error;
    pthread_t           thread_id;
    struct sched_param  thread_params;
    int                 thread_policy;

    thread_id = pthread_self();
    if((pthread_error = pthread_getschedparam(thread_id, &thread_policy, &thread_params)) != 0)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "pthread_getschedparam failed with error %d\n", pthread_error);
        exit(1);
    }

    thread_params.sched_priority = KTGW_THR_MAIN_PRIO;
    if((pthread_error = pthread_setschedparam(thread_id, KTGW_THR_MAIN_SCHED, &thread_params)) != 0)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "pthread_setschedparam failed with error %d\n", pthread_error);
        exit(1);
    }

	/****************************************************************************************/

    // Set 60 second timeout

    fgcethSetTimeout(60000);

    // Initialize library

    fgcethLog(FGCETH_NO_DEV, L_V0, "Initializing FGC Ether library...");

    if ( (err = fgcethInit(arg_eth_name, &names_container, (ktgw_global.arg_codes_path[0] != 0) ? &codes_container : NULL, MB(alloc_mem), 10)) != FGC_SUCCESS)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit failed with error %d (%s)", err, fgcethStrerr(err));
        return 1;
    }

    fgcethFreeContainers(&names_container, (ktgw_global.arg_codes_path[0] != 0) ? &codes_container : NULL);

    fgcethLog(FGCETH_NO_DEV, L_V0, "Completed.");

    /****************************************************************************************/

    ktgwTcpStart();
    ktgwPubStart(ktgw_global.arg_udp_c_path);
    ktgwRtStart();

    /****************************************************************************************/

    fgcethLog(FGCETH_NO_DEV, L_V0, "KT Gateway is active!");

    int temp_update = 0;

    while(!terminate)
    {
    	int log_new_lvl;

    	sleep(1);

    	//Increase/decrease temporarily the debug level

    	if(log_req)
    	{
    		log_req = 0;
    		log_new_lvl = log_lvl;

    		log_counter = 0;

    		setLogLvl(log_new_lvl > 0 ? log_new_lvl : -log_new_lvl);

    		if(log_new_lvl > 0)
    		{
    			temp_update = 1;
    			fgcethLog(FGCETH_NO_DEV, L_V0, "[Log] Log level temporarily updated to L_V%d for %ds!", log_new_lvl, KTGW_VERBOSE_TIME);
    		}
    		else
    		{
    			temp_update = 0;
    			fgcethLog(FGCETH_NO_DEV, L_V0, "[Log] Log level updated to L_V%d!", -log_new_lvl);
    		}


    	}

    	if(log_counter >= KTGW_VERBOSE_TIME && temp_update)
    	{
    		log_lvl = log_base_lvl;
    		setLogLvl(log_base_lvl);
    		fgcethLog(FGCETH_NO_DEV, L_V0, "[Log] Log level returned to to L_V%d!", log_base_lvl);
    		temp_update = 0;
    	}

    	log_counter++;
    }

    ktgw_global.run = 0;

    /****************************************************************************************/

    fgcethLog(FGCETH_NO_DEV, L_V0, "Closing KT Gateway...");

    ktgwTcpClose();
    ktgwPubClose();

    if ((err = fgcethClose()) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit failed to terminate correctly with error %d (%s).", err, fgcethStrerr(err));
		exit(1);
	}

    fgcethLog(FGCETH_NO_DEV, L_V0, "Closed.");

#if ENABLE_LOG_FILES
    if(f  != NULL) fclose(f);
    if(fl != NULL) fclose(fl);
    if(fi != NULL) fclose(fi);
#endif

	exit(0);
}

//EOF
