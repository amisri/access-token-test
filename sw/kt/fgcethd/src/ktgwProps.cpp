
#include <libfgceth.h>

#include <ktgwGlobals.h>

#include <string.h>
#include <pthread.h>
#include <errno.h>

int32_t SetReadCodes()
{
    fgceth_errno err;
    struct fgceth_codes_container codes_container;

    memset(&codes_container, 0, sizeof(codes_container));

    fgcethLog(0, L_V0, "Reading code files...");

    if(ktgw_global.arg_codes_path[0] == 0)
    {
        fgcethWarningLog(0, "No path to code files");
        return -1;
    }

    if((err = fgcethCodesParser(ktgw_global.arg_codes_path, &codes_container)) != FGC_SUCCESS)
    {
        fgcethWarningLog(0, "Code file reading failed with error %d (%s)", err, fgcethStrerr(err));
        return -1;
    }

    if((err = fgcethUpdateCodes(&codes_container)) != FGC_SUCCESS)
    {
        fgcethWarningLog(0, "Code file updating failed with error %d (%s). No codes are being advertised.", err, fgcethStrerr(err));
        fgcethFreeContainers(NULL, &codes_container);

        return -1;
    }

    fgcethLog(0, L_V0, "Code file update Completed.");
    fgcethFreeContainers(NULL, &codes_container);

    return 0;
}



int32_t SetRterm(struct ktgw_client * client, char * dev, char * tag)
{
    char buffer[128];
    uint8_t channel;
    fgceth_errno err;

    // Change client terminal settings for remote terminal
    // Disable telnet client line-mode and local echo

    channel = fgcethGetDevId(dev);

    if(channel == FGCETH_NO_DEV || channel <= 0)
    {
        fgcethWarningLog(0, "Failed to set remote terminal. Channel %d invalid.", channel);
        return -1;
    }

    if((err = fgcethCheckDeviceId(channel)) != FGC_SUCCESS)
    {
        fgcethWarningLog(channel, "Failed to set remote terminal with error %d (%s). Channel %d not available.", err, fgcethStrerr(err), channel);
        return -1;
    }

    // Start remote terminal for client

    if((err = fgcethRtermSubscribeDev(channel, client->rsp_queue, &client->rterm_handle)) != FGC_SUCCESS)
    {
        fgcethWarningLog(channel, "Failed to subscribe to remote terminal with error %d (%s).", err, fgcethStrerr(err));
        return -1;
    }

    client->rterm_locked     = 0;
    client->rterm_id         = channel;
    client->mode             = RTERM_MODE;
    client->rterm_subscribed = true;

    sprintf(buffer, "$%s .\n\n;\xFF\xFD\x22\xFF\xFB\x01", tag);

    if((err = fgcethRtermSendToQueue(client->rsp_queue, buffer, strlen(buffer))) != FGC_SUCCESS)
    {
        fgcethWarningLog(channel, "Failed to acknowledge rterm connection to client, with error %d (%s).", err, fgcethStrerr(err));
        return -1;
    }

    fgcethLog(FGCETH_NO_DEV, L_V0, "TCP %d (%s@%s) rterm connected to device %d.", client->id, client->username, client->hostname, channel);

    return 0;
}



int32_t SetRtermLock(struct ktgw_client * client, char * dev, char * tag)
{
    unsigned char return_val;

    if(!(return_val = SetRterm(client, dev, tag))) // Rterm set okay
    {
        client->rterm_locked = 1;
    }

    return return_val;
}



int32_t SetSubId(struct ktgw_client * client, blk_buf_t * value, fgceth_errno * err) {

    char v_arr[16];
    blkbufCopyStr(v_arr, value, 16);

    char * ptr = v_arr;
    char * ptr_c;
    while(*ptr == ' ') ptr++;

    // Must not be empty
    if(ptr[0] == '\0') {
        *err = FGC_NO_SYMBOL;
        return 1;
    }

    // Must be unsigned
    if(ptr[0] == '-') {
        *err = FGC_BAD_INTEGER;
        return 1;
    }

    uint32_t new_id = strtoul(ptr, &ptr_c, 0);

    if(ptr == ptr_c) {
        *err = FGC_NO_SYMBOL;
        return 1;
    }

    if(errno) {
        *err = FGC_BAD_INTEGER;
        return 1;
    }

    // No limits for -> all values of uint32_t are valid

    client->udp_sub_id = new_id;

    *err = FGC_OK_RSP;
    return 0;
}



int32_t SetSubPeriod(struct ktgw_client * client, blk_buf_t * value, fgceth_errno * err) {

    char v_arr[16];
    blkbufCopyStr(v_arr, value, 16);

    char * ptr = v_arr;
    char * ptr_c;
    while(*ptr == ' ') ptr++;

    // Must not be empty
    if(ptr[0] == '\0') {
        *err = FGC_NO_SYMBOL;
        return 1;
    }

    int32_t new_period = strtol(ptr, &ptr_c, 0);

    if(ptr == ptr_c) {
        *err = FGC_NO_SYMBOL;
        return 1;
    }

    if(errno) {
        *err = FGC_BAD_INTEGER;
        return 1;
    }

    // Check limits
    if(new_period > MAX_PUB_UDP_PERIOD || new_period < -MAX_PUB_UDP_PERIOD) {
        *err = FGC_OUT_OF_LIMITS;
        return 1;
    }


    // Check whether a negative value was specified for an aligned subscription

    if(new_period < 0)
    {
        if(abs(new_period) > FGCD_CYCLES_PER_SEC)
        {
            // Check that period is a multiple of seconds

            if((FGCD_CYCLE_PERIOD_MS * abs(new_period)) % 1000)
            {
                *err                    = FGC_OUT_OF_LIMITS;
                return 1;
            }
        }
        else
        {
            // Check that period is a factor of 1 second

            if(1000. / (FGCD_CYCLE_PERIOD_MS * abs(new_period)) !=
               1000  / (FGCD_CYCLE_PERIOD_MS * abs(new_period)))
            {
                *err                    = FGC_OUT_OF_LIMITS;
                return 1;
            }
        }
    }

    client->udp_sub_period = new_period;
    client->udp_sub_period_tick = 0;

    *err = FGC_OK_RSP;
    return 0;
}



int32_t SetSubPort(struct ktgw_client * client, blk_buf_t * value, fgceth_errno * err) {

    char v_arr[16];
    blkbufCopyStr(v_arr, value, 16);

    char * ptr = v_arr;
    char * ptr_c;
    while(*ptr == ' ') ptr++;

    // Must not be empty
    if(ptr[0] == '\0') {
        *err = FGC_NO_SYMBOL;
        return 1;
    }

    // Must be unsigned
    if(ptr[0] == '-') {
        *err = FGC_BAD_INTEGER;
        return 1;
    }

    uint32_t new_port_num = strtoul(ptr, &ptr_c, 0);

    if(ptr == ptr_c) {
        *err = FGC_NO_SYMBOL;
        return 1;
    }

    if(errno) {
        *err = FGC_BAD_INTEGER;
        return 1;
    }

    // Check limits
    if(new_port_num > USHRT_MAX) {
        *err = FGC_OUT_OF_LIMITS;
        return 1;
    }

    client->udp_sub_port_num = new_port_num;

    *err = FGC_OK_RSP;
    return 0;
}



//EOF
