/*!
 * @file   ktgwVersion.c
 * @brief  Include version information
 * @author Stephen Page / Joao Afonso
 */

#define VERSION_DEF

#include <ktgwVersionDef.h>

// EOF
