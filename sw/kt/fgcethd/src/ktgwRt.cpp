/*!
 * @file   rt.c
 * @brief  Functions for receiving real-time data
 * @author Stephen Page
 */

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

// #include <consts.h>
// #include <defconst.h>
// #include <logging.h>
// #include <timing.h>

#include <ktgwGlobals.h>
#include <ktgwRt.h>



// Global variables

struct Rt rt;



// Static functions

static void* ktgwRtRecv(void*);

static inline int32_t rtTimevalDiffMillis(struct timeval *a, struct timeval *b);



int32_t ktgwRtStart(void)
{
    uint32_t           i;
    struct sockaddr_in sin;

    // Initialise all real-time references to NaN

    for(i = 0 ; i < RT_BUFFER_SIZE ; i++)
    {
        rt.data[i].active_channels = 0;
        memset((void *)&rt.data[i].channels, 0xFF, sizeof(rt.data[i].channels));
    }
    memset((void *)&rt.used_data.channels, 0xFF, sizeof(rt.used_data.channels));

    // Reset data start index

    rt.data_start = 0;

    // Create socket

    if((rt.sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        return 1;
    }

    // Set bind options

    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(RT_UDP_PORT_NUM);
    sin.sin_family      = AF_INET;

    // Bind to socket

    if(bind(rt.sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("bind");
        return 1;
    }

    // Initialise mutex
    pthread_mutexattr_t mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
    pthread_mutexattr_setprotocol(&mutex_attr, PTHREAD_PRIO_INHERIT);

    if(pthread_mutex_init(&rt.mutex, &mutex_attr) != 0) return 1;

    // Start receive thread
    int32_t            thread_error;
    pthread_attr_t     thread_attr;
    struct sched_param thread_param;

    if((thread_error = pthread_attr_init(&thread_attr)) != 0)
    {
        fgcethErrorLog(FGCETH_NO_DEV, "rt_thread thread create(): pthread_attr_init failed with error %d\n", thread_error);
        return 1;
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&thread_attr, KTGW_THR_RT_SCHED);

    // Configure thread parameters

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = KTGW_THR_RT_PRIO;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    if(pthread_create(&rt.thread, &thread_attr, ktgwRtRecv, NULL))
    {
        pthread_mutex_destroy(&rt.mutex);
        close(rt.sock);
        pthread_attr_destroy(&thread_attr);
        return 1;
    }

    pthread_attr_destroy(&thread_attr);

    return 0;
}



// Static functions



/*
 * Thread to receive real-time data
 */

static void *ktgwRtRecv(void *unused)
{
    uint64_t                        active_channels;
    uint32_t                        big_endian = htons(1) == 1 ? 1 : 0;
    struct fgc_udp_rt_data          buffer;
    ssize_t                         bytes_in;
    uint32_t                        cycle_index;
    union { float f; uint32_t l; }  f_l;
    struct sockaddr_in              from;
    socklen_t                       from_len;
    uint32_t                        i;
    unsigned int                    net_latency;
    int32_t                         relative_cycle;
    struct timeval                  time;
    struct timeval                  time_now;

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Zero statistics

    rt.bad_packet_count  = 0;
    rt.good_packet_count = 0;

    for(rt.packet_count = 0 ; 1; rt.packet_count++)
    {
        from_len = sizeof(from);
        pthread_testcancel();
        bytes_in = recvfrom(rt.sock,
                            &buffer, sizeof(buffer),
                            0,
                            (struct sockaddr *)&from, &from_len);
        pthread_testcancel();
        rt.udp_load += bytes_in;

        // Check whether received data was expected size

        if(bytes_in != sizeof(buffer))
        {
            // Ignore packets of incorrect size

            rt.bad_packet_count++;
            rt.bad_size_packet_count++;

            // Wait for next packet

            continue;
        }

        // Calculate network latency

        // if(!fgcethGetCurrentTime(&time_now))
        // {
        fgcethGetCurrentTime(&time_now);
        time.tv_sec     = ntohl(buffer.header.send_time_sec);
        time.tv_usec    = ntohl(buffer.header.send_time_usec);
        net_latency     = rtTimevalDiffMillis(&time, &time_now);

        // Check whether latency is within range of histogram

        if(net_latency < FGC_RT_BUFFER_MS)
        {
            rt.net_latency[net_latency]++;
        }
        else // Latency is beyond range of histogram
        {
            rt.net_latency[FGC_RT_BUFFER_MS - 1]++;
        }
        // }

        // Check whether to hold existing values

        if(rt.hold_values)
        {
            // Ignore packet and wait for next one

            continue;
        }

        // Calculate number of cycles in the future to apply data

        time.tv_sec  = ntohl(buffer.apply_time_sec);
        time.tv_usec = ntohl(buffer.apply_time_usec);

        // If apply time is not set, then apply in next available cycle

        if(!time.tv_sec && !time.tv_usec) // Apply time is not set
        {
            relative_cycle = 0;
        }
        else // Apply time is set
        {
            time.tv_usec = ((time.tv_usec / 1000) - ((time.tv_usec / 1000) % 20)) * 1000;
            relative_cycle = rtTimevalDiffMillis(&time, &rt.cycle_time) / FGCD_CYCLE_PERIOD_MS;
        }

        // Check whether apply time has already passed

        if(relative_cycle < 0) // Late packet
        {
            // Ignore packets whose apply time has passed

            rt.bad_packet_count++;
            rt.late_packet_count++;

            // Wait for next packet

            continue;
        }
        else if(relative_cycle > RT_BUFFER_SIZE) // Early packet
        {
            // Ignore packets outside buffering window

            rt.bad_packet_count++;
            rt.early_packet_count++;

            // Wait for next packet

            continue;
        }

        rt.good_packet_count++;

        // Get active_channels mask in host byte order
        // This is done manually since there is currently no POSIX standard byte-swap for 64 bit values

        if(big_endian)
        {
            active_channels = buffer.active_channels;
        }
        else // Little-endian
        {
            active_channels = ((uint64_t)ntohl( buffer.active_channels & 0x00000000FFFFFFFFLL) << 32) |
                                         ntohl((buffer.active_channels & 0xFFFFFFFF00000000LL) >> 32);
        }

        // Copy data from buffer

        pthread_mutex_lock(&rt.mutex);

        cycle_index = (rt.data_start + relative_cycle) % RT_BUFFER_SIZE;

        for(i = 0 ; i < FGCD_MAX_EQP_DEVS ; i++)
        {
            if(!(active_channels & (1LLU << i))) continue;

            // Byte-swap value to host order

            f_l.f = buffer.channels[i];
            f_l.l = ntohl(f_l.l);

            // Check that value is within accepted range

            if(f_l.f >  RT_MAX_VALUE ||
               f_l.f < -RT_MAX_VALUE)
            {
                rt.bad_value_count++;
                fgcethLog(FGCETH_NO_DEV, L_V0, "RT Value %.7e for channel %d received from %s exceeds limit %d\n",
                          f_l.f, i + 1, inet_ntoa(from.sin_addr), RT_MAX_VALUE);
                continue;
            }

            // Add value to real-time reference data

            rt.data[cycle_index].active_channels   |= 1LL << i;
            rt.data[cycle_index].channels[i]        = f_l.f;
        }
        pthread_mutex_unlock(&rt.mutex);
    }

    return 0;
}



/*
 * Calculate the difference between two timevals in milliseconds
 */

static inline int32_t rtTimevalDiffMillis(struct timeval *a, struct timeval *b)
{
    return ((a->tv_sec  - b->tv_sec)  * 1000) + ((a->tv_usec - b->tv_usec) / 1000);
}

// EOF
