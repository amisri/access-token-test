#!/bin/bash

set -e
set -o pipefail

temp_dir=$(mktemp -d)

close_temp() {
    rm -rf $temp_dir
}

trap "close_temp" EXIT

###############################################################
# Build test
###############################################################

echo "Build test:"

cp -r "$1/." "$temp_dir"

make -C "$temp_dir"

echo "Build test done."


# EOF
