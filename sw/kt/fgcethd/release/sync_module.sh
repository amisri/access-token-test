#!/bin/bash

set -e
set -o pipefail


fgcethd_path=$(readlink -f "$(dirname "$0")/..")
fgcethd_incl="${fgcethd_path}/release/sync_list"
top_repo_path=$(readlink -f "${fgcethd_path}/../../..")


echo "Syncing fgcethd to $1:"


###############################################################
# Generate files
###############################################################

echo "  File auto-generation..."

make -C $top_repo_path/def



###############################################################
# Select and copy files
###############################################################

echo "  Copying files to output directory..."

rsync -avhm --copy-links --ignore-errors --delete --delete-excluded --include-from="${fgcethd_incl}" --exclude={".*","*",".*/"} "${top_repo_path}/" "$1"



###############################################################
# Copy important files to top directory
###############################################################

echo "  Configuring build files..."

# Metafiles

cp -L "$fgcethd_path/README.md"   "$1/README.md"
cp -L "$fgcethd_path/LICENSE.txt" "$1/LICENSE.txt"

# Makefiles

cp -L "$fgcethd_path/release/make/Makefile"       "$1/Makefile"
cp -L "$fgcethd_path/release/make/Makefile.init"  "$1/Makefile.init"
cp -L "$fgcethd_path/release/make/CONFIG"         "$1/CONFIG"
cp -L "$fgcethd_path/release/make/CONFIG_PRIVATE" "$1/CONFIG_PRIVATE"

# Setup path to main Makefile

escaped_path=$(echo "sw/kt/fgcethd" | sed -e 's/\//\\\//g')
sed -i "s/FGCETH_MODULE_DIR =.*/FGCETH_MODULE_DIR = $escaped_path/" "$1/CONFIG_PRIVATE"

if [ ! -z "$3" ]; then
    escaped_version=$(echo "$3" | sed -e 's/\//\\\//g')
    sed -i "s/FGCETH_MODULE_VERSION =.*/FGCETH_MODULE_VERSION = $escaped_version/" "$1/CONFIG_PRIVATE"
fi

########

echo "Syncing done."

# EOF
