# FGC Ether Gateway

FGC Ether Gateway daemon, also known as fgcethd, is a software running on Linux that allows the communication with the FGCs (Functin Generator Controlers) through a set of standard interfaces.

It is a simplified version of CERN's FGCD, which can be deployed by other laboratories as part of a pre-agreed knowledge trasfer protocol.



## Getting started



## Build and install

0) Extract the tarball to the preferred directory.

- Move to the extracted directory.

1) Configure the FGC software top directory:

- In file ./CONFIG, modify FGCETH_HOME to the desired top directory path.
- To setup the top directory (if not setup yet): 
```
make setup
```

2) Build the FGC Ether gateway:

- To build fgcethd inside the tarball directory:
```
make
```

3) Install: 

- To copy fgcethd to the installation directory:
```
make install
```
- Previous executables will be backed up in old/.



## Running

### Prerequisites

- The host machine must contain a NIC (Network Interface Controller) fully dedicated to the connection with the FGCs (data-link layer connection).
- Namefile with information about all FGCs.
- Running user must have *sudo* permissions.

### Running

To display fgcethd options run:
```
<top>/bin/fgcethd --help
```

Command example (replace <>):
```
<top>/bin/fgcethd
    --ethname   <eth_nic_name>
    --namesfile <top>/etc/fgcd/name
    --codespath <top>/firmware/fgcgate
    --udpfile   <top>/etc/fgcd/udp_subscriptions/<udp_subs>
    --pm
```

Command example requirements:
- <eth_nic_name>         : Ethernet NIC connecting to FGCs.
- <top>/[...]/name       : Names file, containing info about all FGCs.
- <top>/[...]/fgcgate    : FGC codes (firmware) directory.
- <top>/[...]/<udp_subs> : File containing UDP/IP info of UDP status publication clients.



## License

#### EOF
