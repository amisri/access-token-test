#ifndef KTGW_GLOBAL_H
#define KTGW_GLOBAL_H


#include <pthread.h>
#include <limits.h>
#include <netinet/in.h>

#include <libfgceth.h>
#include <pub.h>


#define KTGW_MAX_TAG_LEN             FGC_MAX_TAG_LEN
#define KTGW_MAX_DEV_LEN             FGC_MAX_DEV_LEN
#define KTGW_MAX_CMD_LEN             FGC_MAX_CMD_LEN
#define KTGW_MAX_VAL_LEN             FGC_MAX_VAL_LEN
#define KTGW_MAX_PROP_LEN            FGC_MAX_PROP_LEN

#define KTGW_MAX_TCP_CLIENTS         96

#define KTGW_MAX_CLIENT_CMD_BUF      64
#define KTGW_MAX_CLIENT_RTERM_BUF    1024

#define MAX_PUB_UDP_CLIENTS          10
#define MAX_PUB_UDP_PERIOD           500
#define KTGW_PUB_UDP_DEFAULT_PERIOD  PUB_UDP_DEFAULT_PERIOD

#define KTGW_HOSTNAME_MAX            HOST_NAME_MAX
#define KTGW_USERNAME_MAX            16

#define KTGW_EXTRA_CMD_DATA_BUF_SIZE 1024


#define KTGW_CERN_PORT_NUM           FGC_GW_PORT
#define KTGW_FREE_PORT_NUM           (FGC_GW_PORT + 1)

#define RTERM_DISCONNECT_CHAR   	 0x18

#define TELNET_ESC              	 0xFF
#define TELNET_WILL             	 0xFB
#define TELNET_WONT             	 0xFD
#define TELNET_DO               	 0xFC
#define TELNET_DONT             	 0xFE
#define TELNET_LINE             	 0x22

// Send timeout
 
#define KTGW_SNDTIMEO_SEC      60

//Thread priorities

#define KTGW_THR_CLIENT_PRIO   48
#define KTGW_THR_LISTEN_PRIO   48
#define KTGW_THR_MAIN_PRIO     47
#define KTGW_THR_PUB_PRIO      49      
#define KTGW_THR_RT_PRIO       49

//Thread scheduling

#define KTGW_THR_CLIENT_SCHED  SCHED_RR
#define KTGW_THR_LISTEN_SCHED  SCHED_RR
#define KTGW_THR_MAIN_SCHED    SCHED_FIFO
#define KTGW_THR_PUB_SCHED     SCHED_FIFO
#define KTGW_THR_RT_SCHED      SCHED_FIFO



enum ktgw_mode
{
    CMD_MODE,
	RTERM_MODE
};



enum ktgw_cmd_type
{
    CMD_GET,        //!< Get command
    CMD_GET_SUB,    //!< Get command for subscription
    CMD_SET,        //!< Set command
    CMD_SET_BIN,    //!< Set command with binary value
    CMD_SUB,        //!< Subscription request
    CMD_UNSUB       //!< Unsubscription request
};



enum parse_state
{
	TCP_RECV_TAG,
	TCP_RECV_TYPE,
	TCP_RECV_DEV,
	TCP_RECV_PROP,
	TCP_RECV_VALUE,
	TCP_RECV_BIN_LENGTH,
	TCP_RECV_BIN_ERROR,
	TCP_RECV_BIN_VALUE,
	TCP_RECV_IDLE,
	TCP_RECV_ERROR
};



struct ktgw_parsed_cmd
{
    bool               valid;                         //!< If true, then command is valid (was written accordingly with the expected syntax)
    bool               completed;                     //!< If false, command still needs to be completed with incoming data

    uint32_t           bin_len;

    enum parse_state   cmd_state;

    uint32_t           tag_index;
    uint32_t           dev_index;
    uint32_t           pro_index;
    uint32_t           val_index;
    uint32_t           bin_index;
    uint32_t           last_ch_space;
};



struct ktgw_client
{
    int32_t                     id;                                 //!< ID of client structure
    int32_t                     in_use;                             //!< Flag to indicate that this client handle is in use

    struct timeval              connect_time;                       //!< Time connection started
    struct timeval              last_activity_time;                 //!< Time of last activity

    int                         sock;                               //!< Socket
    char               		   	recv_buffer[1500];                  //!< Receive buffer
    char                        send_buffer[1500];                  //!< Send buffer

    char                        hostname[KTGW_HOSTNAME_MAX + 1];    //!< Hostname
    char                        username[KTGW_USERNAME_MAX];        //!< Username

    struct in_addr              address;                            //!< Host address
    enum ktgw_mode              mode;                               //!< Mode (command or rterm)
    uint32_t                    num_cmds;                           //!< Number of commands in progress
    bool                        rterm_subscribed;                   //!< Number of remote terminal responses in progress

    pthread_mutex_t             mutex;                              //!< Mutex to protect command structure counts

    uint32_t                    dev_index;                          //!< Index within device name or address while receiving command
    uint32_t                    bin_index;                          //!< Index within received binary field
    uint32_t                    bin_length;                         //!< Length of received binary value
    uint32_t                    last_char_space;                    //!< Flag to indicate last command character received was a space

    pthread_t                   send_thread;                        //!< Send thread
    pthread_t                   recv_thread;                        //!< Receive thread

    //struct ktgw_extra_cmd_data  *recv_cmd;                          //!< Pointer to current command being filled by receive thread
    fgceth_rsp_queue            *rsp_queue;                         //!< Pointer to queue recipient of remote terminal or command/response protocol data
    uint32_t                    rterm_locked;                       //!< Flag to indicate whether the rterm is locked (ignores close character)
    uint8_t                     rterm_id;                           //!< ID of device to whom rterm is connected
    fgceth_sub_handle           rterm_handle;

    uint32_t                    udp_sub_port_num;                   //!< Port number for UDP published data subscription
    int32_t                     udp_sub_period;                     //!< Period for UDP published data subscription (in 20ms units)
    uint32_t                    udp_sub_period_tick;                //!< Tick within period for UDP published data subscription
    uint32_t                    udp_sub_sequence;                   //!< Sequence number for UDP published data subscription
    uint32_t                    udp_sub_id;                         //!< User-defined ID number to send in packet headers 

    uint32_t                    run;                                //!< Flag to indicate that this client should run

    bool                        use_handshake;						//!< Flag to indicate communication with client should start with an handshake check
    struct ktgw_parsed_cmd      parser;								//!< Used to parse command strings
};



struct ktgw_udp_client
{
    char            hostname[KTGW_HOSTNAME_MAX + 1];                //!< Hostname
    struct in_addr  address;                                        //!< Host address
    uint32_t        port_num;                                       //!< Port number for UDP published data subscription
    int32_t         period;                                         //!< Period for UDP published data subscription (in 10ms units)
    uint32_t        period_tick;                                    //!< Tick within period for UDP published data subscription
    uint32_t        sequence;                                       //!< Sequence number for UDP published data subscription
    uint32_t        id;                                             //!< User-defined ID number to send in packet headers
    uint32_t        user;                                           //!< User selector
};

struct ktgw_pub_header
{
    struct fgc_udp_header    header;
    uint32_t                 time_sec;
    uint32_t                 time_usec;
};



struct ktgw
{
	/*TCP connection*/

	int                     listen_sock[2];                         //!< Socket handle to listen on [0: for CERN (++ handshake) ---- 1: For other labs (no handshake)]
	pthread_t               listen_thread;                          //!< Listening thread handle
	struct ktgw_client      client[KTGW_MAX_TCP_CLIENTS];           //!< Client handles

    /*UDP connection*/

    pthread_t               udp_pub_thread;
    uint32_t                num_udp_clients;
    struct ktgw_udp_client  udp_client[MAX_PUB_UDP_CLIENTS];        //!< UDP clients for status publication
    struct ktgw_pub_header  pub_head;                               //!< Number of devices, including header
    struct fgceth_fgc_stat *pub_status;
    uint32_t                udp_pub_load;                           //!< Outgoing UDP load in bytes per second
    int                     udp_pub_sock;                           //!< Socket to send data
    uint32_t                udp_pub_enabled;                        //!< Enable, if pub is active.

	/*Statistics*/

	uint32_t                cmds_received;                          //!< Count of commands received
	uint32_t                connections_recvd;                      //!< Number of connections received
	uint32_t                load;                                   //!< TCP load in bytes for this second
	uint32_t                tcp_clients;                            //!< Number of TCP clients connected
	pthread_mutex_t         mutex;                                  //!< Mutex to protect command structure counts
	pthread_cond_t          cond;  

	uint32_t                run;                                    //!< Flag used to terminate gw program
	int                     close_p[2];

	char    				arg_names_file[128];
	char    				arg_codes_path[128];
    char                    arg_udp_c_path[128];
};



extern struct ktgw ktgw_global;



#endif

// EOF
