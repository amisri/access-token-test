#ifndef KTGW_PROPS_H
#define KTGW_PROPS_H

#include <libfgceth.h>

int32_t SetReadCodes();
int32_t SetRterm(struct ktgw_client * client, char * dev, char * tag);
int32_t SetRtermLock(struct ktgw_client * client, char * dev, char * tag);
int32_t SetSubId(struct ktgw_client * client, blk_buf_t * value, fgceth_errno * err);
int32_t SetSubPeriod(struct ktgw_client * client, blk_buf_t * value, fgceth_errno * err);
int32_t SetSubPort(struct ktgw_client * client, blk_buf_t * value, fgceth_errno * err);

#endif
//EOF
