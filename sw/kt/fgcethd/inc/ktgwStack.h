/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethQueue.h
 * @brief  General purpose stack.
 */

#ifndef KTGW_STACK_H
#define KTGW_STACK_H

#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethThreads.h>

#include <stdlib.h>


/**************************************
 * Types
***************************************/

/// Structure used to store stack
template<typename T>
struct ktgw_stack_T
{
	bool            blocked;
	T*              buffer;                                       ///< Stack buffer
    size_t          buffer_size;                                  ///< Buffer size
    uint32_t        next;                                         ///< First empty position of stack
    pthread_mutex_t mutex;                                        ///< Mutex protecting access to stack
    pthread_cond_t  not_empty;                                    ///< Condition variable indicating that stack has content
    pthread_cond_t  not_full;                                     ///< Condition variable indicating that stack is not full
};



/**************************************
 * Functions
***************************************/

/// Checks whether the stack is empty.
template <typename T>
bool stackIsEmpty(ktgw_stack_T<T>* q)
{
    return q->next == 0;
}


/// Checks whether the stack is full.
template <typename T>
bool stackIsFull(ktgw_stack_T<T>* q)
{
    return q->next >= q->buffer_size;
}


/**
 * @brief Initializes the stack.
 *
 * Initializing stack by allocating memory and preparing mutexes and conditional variables.
 * For a given stack, this function has to be invoked before any other stack function.
 *
 * @param q pointer to a structure with the stack.
 * @param buffer_size size of the stack being created.
 *
 * @return returns \p FGC_SUCCESS in case of function success, otherwise error code is returned.
 */
template<typename T>
int32_t stackInit(ktgw_stack_T<T>* q, size_t buffer_size)
{
    // Check if buffer_size is valid
    if (buffer_size <= 1)
    {
        buffer_size = 2;
    }

    // If buffer_size > 1 then allocate memory for the stack buffer.
    // Otherwise there's no allocation needed.
    // Increase buffer size by 1 to ensure proper stack operation.
    if (buffer_size++ > 1)
    {
        if(!(q->buffer = reinterpret_cast<T*>(calloc(buffer_size, sizeof(T))) ))
        {
            return -1;
        }
    }

    // Initialize mutex and condition variables
    if (threads_mutex_init(&q->mutex))
    {
        free(q->buffer);
        q->buffer = NULL;
        return -1;
    }

    pthread_cond_init(&q->not_empty, NULL);
    pthread_cond_init(&q->not_full,  NULL);

    q->buffer_size      = buffer_size;
    q->next             = 0;
    q->blocked          = true;

    return 0;
}


/**
 * @brief Unlocks all pop functions.
 *
 * All pop functions will return immediately, regardless of their blocking behaviour (when no more items exist, they will return NULL).
 * It should be called by a thread when the stack is expected to be terminated, to unlock all threads blocking on queueBlockPop.
 *
 * @param q pointer to a structure with the stack.
 * @param b false to disable blocking behaviour, true to enable.
 */
template<typename T>
void stackEnableBlock(ktgw_stack_T<T>* q, bool b)
{
	int cancel_state_orig;

	// Prevent thread from dying while holding mutex
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
	pthread_mutex_lock(&q->mutex);
	q->blocked  = b;
	pthread_cond_broadcast(&q->not_empty);
	pthread_cond_broadcast(&q->not_full);
	pthread_mutex_unlock(&q->mutex);
	pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

}

/**
 * @brief Frees the stack.
 *
 * This function destroys the stack, so the calling function must guarantee that it is called not more than once.
 * Calling this function a second time on the same stack results in undefined behavior.
 *
 * @param q pointer to a structure with the stack.
 */
template<typename T>
void stackFree(ktgw_stack_T<T>* q)
{
    // Free memory
    if(q->buffer != NULL)
    {
        free(q->buffer);
        q->buffer = NULL;
    }

    // Destroy mutex and condition variables
    pthread_mutex_destroy(&q->mutex);
    pthread_cond_destroy(&q->not_empty);
    pthread_cond_destroy(&q->not_full);
}


/**
 * @brief Inserts element into the stack.
 *
 * @param q pointer to a structure with the stack.
 * @param element new element to be inserted into the stack.
 *
 * @return returns \p FGC_SUCCESS in case of function success, otherwise error code is returned.
 */
template<typename T>
int32_t stackPush(ktgw_stack_T<T>* q, T element)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);


    // Check whether stack has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return -1;
    }


    // Check that the stack is not full
    if (!stackIsFull(q))
    {
        // Send signal that stack has content
        if (stackIsEmpty(q))
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        // Add the element to the stack
        q->buffer[q->next] = element;
        q->next++;

        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return 0;
    }
    else // The stack is full
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return -1;
    }
}


/**
 * @brief Inserts element into the stack (block if stack is full).
 *
 * @param q pointer to a structure with the stack.
 * @param element new element to be inserted into the stack.
 *
 * @return returns \p FGC_SUCCESS in case of function success, otherwise error code is returned.
 */
template<typename T>
int32_t queueBlockPush(ktgw_stack_T<T>* q, T element)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether stack has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return -1;
    }

    // Check that the stack is not full
    while (queueIsFull(q))
    {
        // Wait for a member to leave the stack

    	if(!q->blocked)
    	{
    		pthread_mutex_unlock(&q->mutex);
			pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
			return -1;
    	}

        pthread_cond_wait(&q->not_full, &q->mutex);
    }

    // Send signal that stack has content
    if(queueIsEmpty(q))
    {
        pthread_cond_broadcast(&q->not_empty);
    }

    // Add the element to the stack
    q->buffer[q->next] = element;
    q->next++;

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return 0;
}


/**
 * @brief Extracts element from the stack if present, without blocking
 *
 * @param q pointer to a structure with the stack.
 *
 * @return returns element extracted from the stack.
 */
template<typename T>
T stackPop(ktgw_stack_T<T>* q)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether stack has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    if (stackIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    // Send signal that stack is not full
    if (stackIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the element from the stack
    T element  = q->buffer[--q->next];

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return element;
}


/**
 * @brief Extracts element from the stack (block if stack is empty)
 *
 * @param q pointer to a structure with the stack.
 *
 * @return returns element extracted from the stack.
 */
template<typename T>
T queueBlockPop(ktgw_stack_T<T>* q)
{
    int  cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether stack has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    while (queueIsEmpty(q))
    {
    	// If unlock is enforced, do not lock
    	if(!q->blocked)
    	{
    		pthread_mutex_unlock(&q->mutex);
    		pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
    		return NULL;
    	}

        // Wait for a member to enter the stack
        pthread_cond_wait(&q->not_empty, &q->mutex);
    }

    // Send signal that stack is not full
    if (queueIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the element from the stack
    T element  = q->buffer[--q->next];

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return element;
}


#endif
//EOF
