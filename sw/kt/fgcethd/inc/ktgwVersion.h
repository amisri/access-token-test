/*!
 * @file   ktgwVersion.h
 * @brief  Declarations for version information
 * @author Stephen Page / Joao Afonso
 */

#ifndef KTGW_VERSION_H
#define KTGW_VERSION_H

#include <sys/time.h>

/*!
 * Struct containing version information
 */

struct version
{
    struct timeval  time_tar;       //!< Tar creation timestamp
    struct timeval  time;           //!< Build timestamp
    const char      *hostname;      //!< Host name of machine used for the build
    const char      *architecture;  //!< Architecture of the machine used for the build
    const char      *cc_version;    //!< C compiler version
    const char      *cxx_version;   //!< C++ compiler version
    const char      *user_group;    //!< User and group that performed the build
    const char      *directory;     //!< Build directory
    const char      *ident_info;    //!< Information for ident
};

/*!
 * Declare version information.
 *
 * Defined in ktgwVersionDef.h
 */

extern struct version version;

#endif

// EOF
