
#ifndef KTGW_STAT_PUB_H
#define KTGW_STAT_PUB_H

#include <libfgceth.h>

int32_t      ktgwPubStart(char * subs_file);
int32_t      ktgwPubClose();

#endif
//EOF
