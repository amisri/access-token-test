
#ifndef KTGW_PARSER_H
#define KTGW_PARSER_H

#include <libfgceth.h>
#include <ktgwGlobals.h>

void ktgwRestartParser(struct ktgw_parsed_cmd * data);
int32_t ktgwParseCmd(char * input, uint32_t input_len, struct ktgw_parsed_cmd * data, fgceth_rsp * cmd);


#endif
//EOF
