/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   test_rterm.cpp
 * @brief  Code example/test - getting and setting properties asynchronously
 * @author Joao Afonso
 */

#include <libfgceth.h>
#include <../utils/test.h>

#include <stdio.h>
#include <stdlib.h>

void printArgs()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name -l log_file\n");
}

// NOT THREAD SAFE
int responseToString(fgceth_rsp_queue * queue, char * buf, int buf_max)
{
    // Structure used to retrieve a command response
    fgceth_rsp * response;

    int index = 0;
    bool ret = false;

    int failed_attempts = 0;

    while(!ret)
    {

		while(!(response = fgcethQueuePop(queue)))
		{
			failed_attempts++;

			if(failed_attempts > 100)
			{
				return -1;
			}

			usleep(20000);
		}

		char * v = response->d.rterm.buffer;

		for(int i = 0; v[i] != '\0'; i++)
		{
			buf[index++] = v[i];

			if(v[i-1] == '\n' && (v[i] == ';' || v[i] == '!'))
			{
				ret = true;
				break;
			}

			if(index >= buf_max - 1)
			{
				ret = true;
				break;
			}
		}

		fgcethDestroyResponse(response);

    }

    buf[index] = '\0';

    return index;

}

int main(int argc, char* argv[])
{
	int     option;
	char    arg_dev_name[128] = {0};
	int     arg_dev_id        = 0;
	char    arg_names_file[128]   = {0};
	char    arg_gw_name[128]      = {0};
	char    arg_eth_name[128]     = {0};
	char    arg_log_file[128]     = {0};

	FILE *  stdout_file;

	/// Error variable
	fgceth_errno    err;

	fgceth_sub_handle handle;

	struct fgceth_names_container names_container;

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:l:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(arg_eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(arg_dev_name, optarg);
				 arg_dev_id = atoi(arg_dev_name);
				 break;
			 case 'n':
				 strcpy(arg_names_file, optarg);
				 break;
			 case 'g':
				 strcpy(arg_gw_name, optarg);
				 break;
			 case 'l':
				 strcpy(arg_log_file, optarg);
				 break;
			 default :
				 printArgs();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((arg_dev_name[0] == 0 && arg_dev_id == 0) || arg_eth_name[0] == 0 || arg_names_file[0] == 0 || arg_gw_name[0] == 0 || arg_log_file[0] == 0)
	{
		printArgs();
		exit(1);
	}

	stdout_file = fopen(arg_log_file, "w");
	if (stdout_file == NULL) {
		fgcethErrorLog(FGCETH_NO_DEV, "Can't open file %s to redirect stdout and stderr", arg_log_file);
	}

	fgcethSetLogFileInit(stdout_file);
	fgcethSetLogFileExec(stdout_file);


	if((err = fgcethNamesParser(arg_names_file, arg_gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser: Failed with error %d: %s\n", err, fgcethStrerr(err));
		exit(1);
	}

    // Initialize the library
    
    #define MB(x) ((uint32_t) (x) << 20)

    if (fgcethInit(arg_eth_name, &names_container, NULL, MB(256), 0) != FGC_SUCCESS)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit: Failed to initialize the library \n");
        exit(1);
    }

    // get dev_id from device name
	if(arg_dev_id == 0)
	{
		arg_dev_id = fgcethGetDevId(arg_dev_name);
	}

    // Wait until device is ready and PLL is locked
    while (!fgcethIsDeviceReady(arg_dev_id))
    {
        usleep(1000);
    }

    // Queue for asynchronous set and get
    fgceth_rsp_queue * queue = fgcethQueueCreate(5);

    // Buffer for rterm messages to send
    char send_buf[128];
    char recv_buf[128];

    // ***************************************************

    // Subscribe remote terminal
    fgcethRtermSubscribeDev(arg_dev_id, queue, &handle);

    // Send something, wait, and then empty all characters waiting to be received
    sprintf(send_buf, "!S SOMETHING\n");
    fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
    sleep(1);
    while(responseToString(queue, recv_buf, 128) >= 0);

    // Returns OK
    sprintf(send_buf, "!S TEST.INT32U 1,2,3,4,5,6\n");
    fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
    fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
    fgcethAssertExp(strcmp(recv_buf, "$\n;") == 0);

    // Returns propriety previously set
    sprintf(send_buf, "!G TEST.INT32U\n");
	fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$1,2,3,4,5,6\n;") == 0);

	// Returns OK, twice, since two valid commands were sent
	sprintf(send_buf, "!S TEST.INT32U 123,321,333\n!S TEST.INT32U 777\n");
	fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$\n;") == 0);
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$\n;") == 0);

	// Returns propriety previously set (last set)
	sprintf(send_buf, "!G TEST.INT32U\n");
	fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$777\n;") == 0);

	// Returns OK, only once, since first command was not finished
	sprintf(send_buf, "!S TEST.INT32U 123,321,333!S TEST.INT32U 666\n");
	fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$$57 cmd restarted\n!") == 0);
	//fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	//fgcethAssertExp(strcmp(recv_buf, "$\n;") == 0);

	// Returns value of previously valid set prop
	sprintf(send_buf, "!G TEST.INT32U\n");
	fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$777\n;") == 0);

	// Returns error for invalid prop
	sprintf(send_buf, "!G TEST.INVALID_#%%*\n");
	fgcethAssert(fgcethRtermSendToDev(arg_dev_id, send_buf, strlen(send_buf)));
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$$17 unknown sym\n!") == 0);

	// Returns value of previously valid set prop (command sent character by character, as expected from rterm)
	sprintf(send_buf, "!G TEST.INT32U\n");
	for(uint32_t i = 0; i < strlen(send_buf); i++)
	{
		fgcethAssert(fgcethRtermSendToDev(arg_dev_id, &send_buf[i], 1));
		usleep(100000);
	}
	fgcethAssertExp(responseToString(queue, recv_buf, 128) >= 0);
	fgcethAssertExp(strcmp(recv_buf, "$777\n;") == 0);


    // ***************************************************

    // Retrieve global statistics
    fgceth_global_stats global;
    fgcethGetGlobalStats(&global);

    // Retrieve device statistics
    fgceth_device_stats device;
    fgcethGetDeviceStats(&device, arg_dev_id);

    // Check content of the statistics
    fgcethAssertExp(global.eth.rx.all           > 0);
    fgcethAssertExp(global.eth.rx.bad.all      == 0);
    fgcethAssertExp(global.eth.tx.all           > 0);
    fgcethAssertExp(global.eth.tx.failed       == 0);
    fgcethAssertExp(global.load_sent            > 0);
    fgcethAssertExp(global.load_received        > 0);

    fgcethAssertExp(device.ack_miss            == 0);
    fgcethAssertExp(device.offline_transition  == 0);
    fgcethAssertExp(device.proto_error         == 0);
    fgcethAssertExp(device.seq_status_miss     == 0);
    fgcethAssertExp(device.seq_toggle_bit_miss == 0);
    fgcethAssertExp(device.status_rec           > 0);
    fgcethAssertExp(device.status_miss         == 0);
    fgcethAssertExp(device.unlock_transition   == 0);
    fgcethAssertExp(device.rsp_not_expected    == 0);
    fgcethAssertExp(device.rsp_wrong_seq_num   == 0);
    fgcethAssertExp(device.rsp_wrong_first     == 0);

    fgcethClose();

    fclose(stdout_file);

    return 0;
}
