#!/bin/bash

# List of tests to run

tests=(
	test_getSetSync
	test_getSetASync
	test_longGetSet
	test_statistics
	test_subscriptions
	test_setTimeOut
	test_rterm
        test_statPub
)

# Check if the script is run with root privileges
if [[ $EUID -ne 0 ]]; then
   echo -e "\033[1;31mERROR: This script must be run as root.\033[0m" 
   exit 1
fi

# Check if number of parameters is correct
if [[ $# -ne 5 ]] && [[ $# -ne 6 ]]; then
	echo -e "\033[1;31mERROR: This script expects four parameters ([1] eth name; [2] device name/ID; [3] names file; [4] gw name); [5] log file name; [6] (optional) 'r' to config before testing.\033[0m"
	exit 1
fi


# Iterate over tests and run each one of them
index=0

if [ "${6}" == "r" ]; then
    echo "Reconfiguring device ${2}..."
    eval "../../scripts/util_config/Linux/x86_64/util_config -r -v -i ${1} -d ${2} -n ${3} -g ${4} -f config.txt &> ${5}/util_config.out"

    # Check if the script was successful 
    if [ $? -eq 0 ]; then
        echo -e "\033[1;32mOK \033[0m"
        echo ""
    else
        echo -e "\033[1;33mCONFIGURATION ABORTED \033[0m"
        echo ""
    fi
fi

for i in ${tests[@]}; do
	# Increment index
	((index++))

	# Inform
	echo "Running test #${index}: ${i}"

	# Set red color output
	echo -en "\033[1;31m"

	# Run test
	eval "${i}/$(uname -s)/$(uname -m)/${i} -i ${1} -d ${2} -n ${3} -g ${4} -l ${5}/${i}.out"

	# Check if the script was successful 
	if [ $? -eq 0 ]; then
	    echo -e "\033[1;32mOK"
	else
	    echo -e "\033[1;31mFAILED"
	fi

	echo -e "\033[0m"
done


exit 0


}
