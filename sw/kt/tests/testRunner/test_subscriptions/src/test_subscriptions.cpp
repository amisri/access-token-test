/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   test_subscriptions.cpp
 * @brief  Code example/test - subscriptions and publication mechanism
 * @author Dariusz Zielinski
 */

#include <libfgceth.h>
#include <../utils/test.h>

#include <stdio.h>
#include <stdlib.h>

void printArgs()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name -l log_file\n");
}

int main(int argc, char* argv[])
{
	int     option;
	char    arg_dev_name[128] = {0};
	int     arg_dev_id        = 0;
	char    arg_names_file[128]   = {0};
	char    arg_gw_name[128]      = {0};
	char    arg_eth_name[128]     = {0};
	char    arg_log_file[128]     = {0};

	FILE *  stdout_file;

	/// Error variable
	fgceth_errno    err;

	struct fgceth_names_container names_container;

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:l:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(arg_eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(arg_dev_name, optarg);
				 arg_dev_id = atoi(arg_dev_name);
				 break;
			 case 'n':
				 strcpy(arg_names_file, optarg);
				 break;
			 case 'g':
				 strcpy(arg_gw_name, optarg);
				 break;
			 case 'l':
				 strcpy(arg_log_file, optarg);
				 break;
			 default :
				 printArgs();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((arg_dev_name[0] == 0 && arg_dev_id == 0) || arg_eth_name[0] == 0 || arg_names_file[0] == 0 || arg_gw_name[0] == 0 || arg_log_file[0] == 0)
	{
		printArgs();
		exit(1);
	}

	stdout_file = fopen(arg_log_file, "w");
	if (stdout_file == NULL) {
		fgcethErrorLog(FGCETH_NO_DEV, "Can't open file %s to redirect stdout and stderr", arg_log_file);
	}

	fgcethSetLogFileInit(stdout_file);
	fgcethSetLogFileExec(stdout_file);

	if((err = fgcethNamesParser(arg_names_file, arg_gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser: Failed with error %d: %s\n", err, fgcethStrerr(err));
		exit(1);
	}

	// Initialize the library
    
    #define MB(x) ((uint32_t) (x) << 20)

	if (fgcethInit(arg_eth_name, &names_container, NULL, MB(256), 0) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit: Failed to initialize the library \n");
		exit(1);
	}

	// get dev_id from device name
	if(arg_dev_id == 0)
	{
		arg_dev_id = fgcethGetDevId(arg_dev_name);
	}

    // Wait until device is ready and PLL is locked
    while (!fgcethIsDeviceReady(arg_dev_id))
    {}

    // ***************************************************

    // Retrieve global statistics
    fgceth_global_stats global;
    fgcethGetGlobalStats(&global);

    // Retrieve device statistics
    fgceth_device_stats device;
    fgcethGetDeviceStats(&device, arg_dev_id);

    // Check content of the statistics
    // fgcethAssertExp(global.eth.rx.all        > 0);    TODO Remove this comment when the test is fully written
    fgcethAssertExp(global.eth.rx.bad.all      == 0);
    // fgcethAssertExp(global.eth.tx.all        > 0);    TODO Remove this comment when the test is fully written
    fgcethAssertExp(global.eth.tx.failed       == 0);
    //fgcethAssertExp(global.load_sent          > 0);     TODO Remove this comment when the test is fully written
    //fgcethAssertExp(global.load_received      > 0);     TODO Remove this comment when the test is fully written

    fgcethAssertExp(device.ack_miss            == 0);
    fgcethAssertExp(device.offline_transition  == 0);
    fgcethAssertExp(device.proto_error         == 0);
    fgcethAssertExp(device.seq_status_miss     == 0);
    fgcethAssertExp(device.seq_toggle_bit_miss == 0);
    fgcethAssertExp(device.status_rec           > 0);
    fgcethAssertExp(device.status_miss         == 0);
    fgcethAssertExp(device.unlock_transition   == 0);
    fgcethAssertExp(device.rsp_not_expected    == 0);
    fgcethAssertExp(device.rsp_wrong_seq_num   == 0);
    fgcethAssertExp(device.rsp_wrong_first     == 0);

    fclose(stdout_file);

    return 0;
}
