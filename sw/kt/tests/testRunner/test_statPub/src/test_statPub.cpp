/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   test_statPub.cpp
 * @brief  Code example/test - getting and setting properties asynchronously
 * @author Joao Afonso
 */

#include <libfgceth.h>
#include <../utils/test.h>

#include <stdio.h>
#include <stdlib.h>

#define BUFFER_CYCLES 5

void printArgs()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name -l log_file\n");
}

// NOT THREAD SAFE
int responseToString(fgceth_rsp_queue * queue, char * buf, int buf_max)
{
    // Structure used to retrieve a command response
    fgceth_rsp * response;

    int index = 0;
    bool ret = false;

    int failed_attempts = 0;

    while(!ret)
    {

		while(!(response = fgcethQueuePop(queue)))
		{
			failed_attempts++;

			if(failed_attempts > 100)
			{
				return -1;
			}

			usleep(20000);
		}

		char * v = response->d.rterm.buffer;

		for(int i = 0; v[i] != '\0'; i++)
		{
			buf[index++] = v[i];

			if(v[i-1] == '\n' && (v[i] == ';' || v[i] == '!'))
			{
				ret = true;
				break;
			}

			if(index >= buf_max - 1)
			{
				ret = true;
				break;
			}
		}

		fgcethDestroyResponse(response);

    }

    buf[index] = '\0';

    return index;

}

int main(int argc, char* argv[])
{
	int     option;
	char    arg_dev_name[128] = {0};
	int     arg_dev_id        = 0;
	char    arg_names_file[128]   = {0};
	char    arg_gw_name[128]      = {0};
	char    arg_eth_name[128]     = {0};
	char    arg_log_file[128]     = {0};

	FILE *  stdout_file;

	/// Error variable
	fgceth_errno    err;

	struct fgceth_names_container names_container;

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:l:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(arg_eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(arg_dev_name, optarg);
				 arg_dev_id = atoi(arg_dev_name);
				 break;
			 case 'n':
				 strcpy(arg_names_file, optarg);
				 break;
			 case 'g':
				 strcpy(arg_gw_name, optarg);
				 break;
			 case 'l':
				 strcpy(arg_log_file, optarg);
				 break;
			 default :
				 printArgs();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((arg_dev_name[0] == 0 && arg_dev_id == 0) || arg_eth_name[0] == 0 || arg_names_file[0] == 0 || arg_gw_name[0] == 0 || arg_log_file[0] == 0)
	{
		printArgs();
		exit(1);
	}

	stdout_file = fopen(arg_log_file, "w");
	if (stdout_file == NULL) {
		fgcethErrorLog(FGCETH_NO_DEV, "Can't open file %s to redirect stdout and stderr", arg_log_file);
	}

	fgcethSetLogFileInit(stdout_file);
	fgcethSetLogFileExec(stdout_file);


	if((err = fgcethNamesParser(arg_names_file, arg_gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser: Failed with error %d: %s\n", err, fgcethStrerr(err));
		exit(1);
	}

    // Initialize the library
    
    #define MB(x) ((uint32_t) (x) << 20)

    if (fgcethInit(arg_eth_name, &names_container, NULL, MB(256), BUFFER_CYCLES) != FGC_SUCCESS)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit: Failed to initialize the library \n");
        exit(1);
    }

    // get dev_id from device name
	if(arg_dev_id == 0)
	{
		arg_dev_id = fgcethGetDevId(arg_dev_name);
	}

    // Wait until device is ready and PLL is locked
    while (!fgcethIsDeviceReady(arg_dev_id))
    {
        usleep(1000);
    }

    // ***************************************************

    struct fgceth_fgc_stat * status;
    struct timeval time, prev_time;
    uint32_t pub_id, prev_pub_id;

    // Test monotonicity of multiple get status functions
     
    pub_id = 0;

    fgcethAssert(fgcethGetNextStatusPub(&status, &time, &pub_id));

    for(uint32_t i = 0; i < 20; i++)
    {
        // Covers 2 seconds

        prev_pub_id = pub_id;

        fgcethAssert(fgcethGetNextStatusPub(&status, &time, &pub_id));

        fgcethAssertExp(prev_pub_id == (pub_id - 1));
    }

    // Test monotonicity of multiple get status functions with 20ms explicit delay

    fgcethAssert(fgcethGetNextStatusPub(&status, &time, &pub_id));

    for(uint32_t i = 0; i < 20; i++)
    {
        // Covers 2 seconds

        prev_pub_id = pub_id;

        usleep(20000);

        fgcethAssert(fgcethGetNextStatusPub(&status, &time, &pub_id));

        fgcethAssertExp(prev_pub_id == (pub_id - 1));
    }

    // Check timing of multiple calls to get status function

    fgcethAssert(fgcethGetNextStatusPub(&status, &time, &pub_id));

    for(uint32_t i = 0; i < 20; i++)
    {
        // Covers 2 seconds

        prev_pub_id = pub_id;
        prev_time = time;

        fgcethAssert(fgcethGetNextStatusPub(&status, &time, &pub_id));

        uint64_t prev_ms = (prev_time.tv_sec * 1000) + (prev_time.tv_usec / 1000);
        uint64_t curr_ms = (time.tv_sec      * 1000) + (time.tv_usec      / 1000);

        fgcethAssertExp((curr_ms - (prev_ms + 20)) < 1);
    }     

    // Check status flags for files in name file vs others

    for(uint32_t i = 1; i <= FGCETH_FGC_DEV_MAX; i++)
    {
        if(fgcethIsDeviceReady(i))
        {
            fgcethAssertExp((status[i].data_status & FGCETH_DATA_VALID) != 0);
        }
        else
        {
            fgcethAssertExp((status[i].data_status & FGCETH_DATA_VALID) == 0);
        }

        if(names_container.name_data[i].dev_name != NULL)
        {
            fgcethAssertExp((status[i].data_status & FGCETH_DEVICE_IN_DB) != 0);
        }
        else
        {
            fgcethAssertExp((status[i].data_status & FGCETH_DEVICE_IN_DB) == 0);
        }

        // TODO: Test remaining flag
    }

    // Check if status structure is being updated when status packets are received

    memset(&status[arg_dev_id].class_data, 0, sizeof(status[arg_dev_id].class_data));

    // Sleep just enough time before status buffer overflowing happens

    usleep((BUFFER_CYCLES - 1) * 20000);

    uint32_t sum = 0;

    for(uint32_t i = 0; i < sizeof(status[arg_dev_id].class_data); i++)
    {
        sum += ((uint8_t *) &status[arg_dev_id].class_data)[i];
    }

    fgcethAssertExp(sum == 0);

    // Sleep another cycle to observe status buffer overflowing
     
    usleep(20000);

    sum = 0;

    for(uint32_t i = 0; i < sizeof(status[arg_dev_id].class_data); i++)
    {
        sum += ((uint8_t *) &status[arg_dev_id].class_data)[i];
    }

    fgcethAssertExp(sum != 0);

    // ***************************************************

    // Retrieve global statistics
    fgceth_global_stats global;
    fgcethGetGlobalStats(&global);

    // Retrieve device statistics
    fgceth_device_stats device;
    fgcethGetDeviceStats(&device, arg_dev_id);

    // Check content of the statistics
    fgcethAssertExp(global.eth.rx.all           > 0);
    fgcethAssertExp(global.eth.rx.bad.all      == 0);
    fgcethAssertExp(global.eth.tx.all           > 0);
    fgcethAssertExp(global.eth.tx.failed       == 0);
    fgcethAssertExp(global.load_sent            > 0);
    fgcethAssertExp(global.load_received        > 0);

    fgcethAssertExp(device.ack_miss            == 0);
    fgcethAssertExp(device.offline_transition  == 0);
    fgcethAssertExp(device.proto_error         == 0);
    fgcethAssertExp(device.seq_status_miss     == 0);
    fgcethAssertExp(device.seq_toggle_bit_miss == 0);
    fgcethAssertExp(device.status_rec           > 0);
    fgcethAssertExp(device.status_miss         == 0);
    fgcethAssertExp(device.unlock_transition   == 0);
    fgcethAssertExp(device.rsp_not_expected    == 0);
    fgcethAssertExp(device.rsp_wrong_seq_num   == 0);
    fgcethAssertExp(device.rsp_wrong_first     == 0);

    fgcethClose();

    fclose(stdout_file);

    return 0;
}
