/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   test_longGetSet.cpp
 * @brief  Code example/test - getting and setting long property
 * @author Dariusz Zielinski
 */

#include <libfgceth.h>
#include <../utils/test.h>

#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

void printArgs()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name -l log_file\n");
}

int main(int argc, char* argv[])
{
	int     option;
	char    arg_dev_name[128] = {0};
	int     arg_dev_id        = 0;
	char    arg_names_file[128]   = {0};
	char    arg_gw_name[128]      = {0};
	char    arg_eth_name[128]     = {0};
	char    arg_log_file[128]     = {0};

	FILE *  stdout_file;

	/// Error variable
	fgceth_errno    err;

	struct fgceth_names_container names_container;

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:l:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(arg_eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(arg_dev_name, optarg);
				 arg_dev_id = atoi(arg_dev_name);
				 break;
			 case 'n':
				 strcpy(arg_names_file, optarg);
				 break;
			 case 'g':
				 strcpy(arg_gw_name, optarg);
				 break;
			 case 'l':
				 strcpy(arg_log_file, optarg);
				 break;
			 default :
				 printArgs();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((arg_dev_name[0] == 0 && arg_dev_id == 0) || arg_eth_name[0] == 0 || arg_names_file[0] == 0 || arg_gw_name[0] == 0 || arg_log_file[0] == 0)
	{
		printArgs();
		exit(1);
	}

	stdout_file = fopen(arg_log_file, "w");
	if (stdout_file == NULL) {
		fgcethErrorLog(FGCETH_NO_DEV, "Can't open file %s to redirect stdout and stderr", arg_log_file);
	}

	fgcethSetLogFileInit(stdout_file);
	fgcethSetLogFileExec(stdout_file);

	if((err = fgcethNamesParser(arg_names_file, arg_gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser: Failed with error %d: %s\n", err, fgcethStrerr(err));
		exit(1);
	}

	// Initialize the library

    #define MB(x) ((uint32_t) (x) << 20)
 
	if (fgcethInit(arg_eth_name, &names_container, NULL, MB(256), 0) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit: Failed to initialize the library \n");
		exit(1);
	}

	// get dev_id from device name
	if(arg_dev_id == 0)
	{
		arg_dev_id = fgcethGetDevId(arg_dev_name);
	}

    // Wait until device is ready and PLL is locked
    while (!fgcethIsDeviceReady(arg_dev_id))
    {
        usleep(1000);
    }

    // ***************************************************

    // Retrieve minimum and maximum current value (they are needed to set REF.TABLE.FUNCTION)

    fgceth_rsp * limits_IPos  = fgcethCreateResponse();
    fgceth_rsp * limits_IMin  = fgcethCreateResponse();
    fgceth_rsp * limits_IRate = fgcethCreateResponse();

    // ***************************************************

    fgcethConfigCmd(limits_IPos,  "", GET, arg_dev_name, "LIMITS.I.POS[0]",   NULL);
    fgcethConfigCmd(limits_IMin,  "", GET, arg_dev_name, "LIMITS.I.MIN[0]",   NULL);
    fgcethConfigCmd(limits_IRate, "", GET, arg_dev_name, "LIMITS.I.RATE[0]",  NULL);

    fgcethAssert(fgcethGetSet(limits_IPos));
    fgcethAssert(fgcethGetSet(limits_IMin));
    fgcethAssert(fgcethGetSet(limits_IRate));

    fgcethDestroyResponse(limits_IPos);
    fgcethDestroyResponse(limits_IMin);
    fgcethDestroyResponse(limits_IRate);

    // ***************************************************

    // Structure used to hold a command response

    fgceth_rsp * cmd_rsp = fgcethCreateResponse();

    // Reset the FGC state to IDLE
     
    fgcethConfigCmd(cmd_rsp, "", SET, arg_dev_name, "MODE.PC",  "OFF");

    fgcethAssert(fgcethGetSet(cmd_rsp));

    fgcethDestroyResponse(cmd_rsp);

    // ***************************************************

    char read_buffer[128];

    cmd_rsp = fgcethCreateResponse();

    fgcethConfigCmd(cmd_rsp, "", GET, arg_dev_name, "STATE.PC",  "");

    do
    {
        fgcethAssert(fgcethGetSet(cmd_rsp));
        usleep(1000);

        blkbufCopyStr(read_buffer, cmd_rsp->d.cmd_rsp.response, sizeof(read_buffer));

    } while (strcmp(read_buffer, "OFF") != 0);

    fgcethDestroyResponse(cmd_rsp);

    // ***************************************************

    cmd_rsp = fgcethCreateResponse();

    fgcethConfigCmd(cmd_rsp, "", SET, arg_dev_name, "MODE.PC",  "ON_STANDBY");

    fgcethAssert(fgcethGetSet(cmd_rsp));

    fgcethDestroyResponse(cmd_rsp);

    // ***************************************************

    cmd_rsp = fgcethCreateResponse();

    fgcethConfigCmd(cmd_rsp, "", GET, arg_dev_name, "STATE.PC",  "");

    do
    {
        fgcethAssert(fgcethGetSet(cmd_rsp));
        usleep(1000);

        blkbufCopyStr(read_buffer, cmd_rsp->d.cmd_rsp.response, sizeof(read_buffer));

    } while (strcmp(read_buffer, "ON_STANDBY") != 0);

    fgcethDestroyResponse(cmd_rsp);

    // ***************************************************

    cmd_rsp = fgcethCreateResponse();

    fgcethConfigCmd(cmd_rsp, "", SET, arg_dev_name, "MODE.PC",  "IDLE");

    fgcethAssert(fgcethGetSet(cmd_rsp));

    fgcethDestroyResponse(cmd_rsp);

    // ***************************************************

    cmd_rsp = fgcethCreateResponse();

    fgcethConfigCmd(cmd_rsp, "", GET, arg_dev_name, "STATE.PC",  "");

    do
    {
        fgcethAssert(fgcethGetSet(cmd_rsp));
        usleep(1000);

        blkbufCopyStr(read_buffer, cmd_rsp->d.cmd_rsp.response, sizeof(read_buffer));

    } while (strcmp(read_buffer, "IDLE") != 0);

    fgcethDestroyResponse(cmd_rsp);

    // ***************************************************

    // Prepare long command that should cause sending more than one packet so it is to check
    // packet fragmentation.

    const int   count     = 1000;

    char v_pos[32];
    char v_minimum[32];
    char v_rate[32];

    blkbufCopyStr(v_pos,     limits_IPos->d.cmd_rsp.response,  sizeof(v_pos));
    blkbufCopyStr(v_minimum, limits_IMin->d.cmd_rsp.response,  sizeof(v_minimum));
    blkbufCopyStr(v_rate,    limits_IRate->d.cmd_rsp.response, sizeof(v_rate));

    double      pos       = strtod(v_pos,  NULL);
    double      minimum   = strtod(v_minimum,  NULL);
    double      rate      = strtod(v_rate, NULL);

    double      value     = minimum;
    std::string long_value = "";

    char   temp[50];

    srand(time(NULL));

    for (int i = 0; i < count; ++i)
    {
        //There must not be any increment on first iteration
        double increment = (i > 0) ? ((((double)rand() / RAND_MAX) * 0.3) - 0.15) : 0;
        value += std::min(rate * 0.1, std::max(-rate * 0.1, increment));
        value  = std::min(pos, std::max(minimum, value));

        snprintf(temp, 50, "%f", i * 0.1);
        long_value += temp;

        long_value += "|";

        snprintf(temp, 50, "%f", value);
        long_value += temp;

        if (i < 1000 - 1)
        {
            long_value += ",";
        }
    }

    std::string long_command = "REF.TABLE.FUNCTION ";
    long_value = long_command + long_value;

    // ***************************************************

    // Send the command many times
    uint32_t attemps_count      = 100;

    fgcethErrorLog(FGCETH_NO_DEV, "Sending %d commands", attemps_count);

    for (uint32_t i = 0; i < attemps_count; ++i)
    {
        // Send the long command
        cmd_rsp = fgcethCreateResponse();

        fgcethConfigCmd(cmd_rsp, "", SET, arg_dev_name, long_value.c_str(),  long_command.c_str());

        fgcethAssert(fgcethGetSet(cmd_rsp));

        fgcethDestroyResponse(cmd_rsp);

        // Check if the state is ARMED
        
        cmd_rsp = fgcethCreateResponse();

        fgcethConfigCmd(cmd_rsp, "", GET, arg_dev_name, "STATE.PC", ""); 
        
        do
        {
            fgcethAssert(fgcethGetSet(cmd_rsp));
            usleep(1000);

            blkbufCopyStr(read_buffer, cmd_rsp->d.cmd_rsp.response, sizeof(read_buffer));

        } while (strcmp(read_buffer, "ARMED") != 0);

        fgcethDestroyResponse(cmd_rsp);

        // Go to IDLE
        // 
        cmd_rsp = fgcethCreateResponse();

        fgcethConfigCmd(cmd_rsp, "", SET, arg_dev_name, "MODE.PC", "IDLE");

        fgcethAssert(fgcethGetSet(cmd_rsp));

        fgcethDestroyResponse(cmd_rsp);

        // ***************************************************
        
        cmd_rsp = fgcethCreateResponse();

        fgcethConfigCmd(cmd_rsp, "", GET, arg_dev_name, "STATE.PC", "");  
        
        do
        {
            fgcethAssert(fgcethGetSet(cmd_rsp));
            usleep(1000);

            blkbufCopyStr(read_buffer, cmd_rsp->d.cmd_rsp.response, sizeof(read_buffer));

            fgcethDestroyResponse(cmd_rsp);

        } while (strcmp(read_buffer, "IDLE") != 0);

        fgcethDestroyResponse(cmd_rsp);

    }

    // ***************************************************

    // Retrieve global statistics
    fgceth_global_stats global;
    fgcethGetGlobalStats(&global);

    // Retrieve device statistics
    fgceth_device_stats device;
    fgcethGetDeviceStats(&device, arg_dev_id);

    // Check content of the statistics
    fgcethAssertExp(global.eth.rx.all           > 0);
    fgcethAssertExp(global.eth.rx.bad.all      == 0);
    fgcethAssertExp(global.eth.tx.all           > 0);
    fgcethAssertExp(global.eth.tx.failed       == 0);
    fgcethAssertExp(global.load_sent            > 0);
    fgcethAssertExp(global.load_received        > 0);

    fgcethAssertExp(device.ack_miss            == 0);
    fgcethAssertExp(device.offline_transition  == 0);
    fgcethAssertExp(device.proto_error         == 0);
    fgcethAssertExp(device.seq_status_miss     == 0);
    fgcethAssertExp(device.seq_toggle_bit_miss == 0);
    fgcethAssertExp(device.status_rec           > 0);
    fgcethAssertExp(device.status_miss         == 0);
    fgcethAssertExp(device.unlock_transition   == 0);
    fgcethAssertExp(device.rsp_not_expected    == 0);
    fgcethAssertExp(device.rsp_wrong_seq_num   == 0);
    fgcethAssertExp(device.rsp_wrong_first     == 0);

    fgcethClose();

    fclose(stdout_file);

    return 0;
}
