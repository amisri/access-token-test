/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   readFilesTest.cpp
 * @brief  used for testing the code and name files reading functions
 * @author Joao Afonso
 */

#include <libfgceth.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

/**************************************
 * Main thread
***************************************/

void printNamesContainer(struct fgceth_names_container * names_container)
{
	printf("-------- Device names --------\n");
	for(uint32_t i = 0; i < FGCETH_ALL_DEV_MAX; i++)
	{
		printf("Dev #%02d: ", i);
		if(names_container->name_data[i].dev_name == NULL)
		{
			printf("[NULL]\n");
		}
		else
		{
			printf("\n");
			printf("    Dev Name:  $%s$\n",   names_container->name_data[i].dev_name);
			printf("    Dev Class: %d\n",     names_container->name_data[i].dev_class);
			printf("    Dev Mask:  0x%04x\n", names_container->name_data[i].dev_omode_mask);
		}
	}
}

#define ROWS_BEGIN 4
#define ROWS_END   4

void printCodesContainer(struct fgceth_codes_container * codes_container)
{
	uint8_t * ptr;

	printf("----------- Codes ------------\n");
	for(uint32_t i = 0; i < FGCETH_MAX_DEV_CODES; i++)
	{
		printf("Code #%02d: ", i);
		if(codes_container->code_data[i].code == NULL)
		{
			printf("[NULL]\n");
		}
		else
		{
			printf("\n");
			printf("    Code Name:   $%s$\n", codes_container->code_data[i].code_name);
			printf("    Code Length: %d\n",   codes_container->code_data[i].code_length);
			printf("    Code:\n");

			for(uint32_t j = 0; j < 16 * ROWS_BEGIN && j < codes_container->code_data[i].code_length; j += 16)
			{
				printf("        [0x%08x]:  ", j);
				for(uint32_t k = 0; k < 16 && (j + k) < codes_container->code_data[i].code_length; k += 4)
				{
					ptr = &codes_container->code_data[i].code[j + k];
					printf("%02x%02x %02x%02x ", ptr[0], ptr[1], ptr[2], ptr[3]);
				}

				printf(" ");
				for(uint32_t k = 0; k < 16 && (j + k) < codes_container->code_data[i].code_length; k += 4)
				{
					ptr = &codes_container->code_data[i].code[j + k];
					printf
					(
						"%c%c%c%c",
						(ptr[0] > 32 && ptr[0] < 127) ? (char) ptr[0] : '.',
						(ptr[1] > 32 && ptr[1] < 127) ? (char) ptr[1] : '.',
						(ptr[2] > 32 && ptr[2] < 127) ? (char) ptr[2] : '.',
						(ptr[3] > 32 && ptr[3] < 127) ? (char) ptr[3] : '.'
					);
				}
				printf("\n");
			}

			printf("        ...\n");

			for(uint32_t j = (codes_container->code_data[i].code_length - (16 * ROWS_BEGIN)) & ~((1 << 4) - 1); j < codes_container->code_data[i].code_length; j += 16)
			{
				printf("        [0x%08x]:  ", j);
				for(uint32_t k = 0; k < 16 && (j + k) < codes_container->code_data[i].code_length; k += 4)
				{
					ptr = &codes_container->code_data[i].code[j + k];
					printf("%02x%02x %02x%02x ", ptr[0], ptr[1], ptr[2], ptr[3]);
				}

				printf(" ");
				for(uint32_t k = 0; k < 16 && (j + k) < codes_container->code_data[i].code_length; k += 4)
				{
					ptr = &codes_container->code_data[i].code[j + k];
					printf
					(
						"%c%c%c%c",
						(ptr[0] > 32 && ptr[0] < 127) ? (char) ptr[0] : '.',
						(ptr[1] > 32 && ptr[1] < 127) ? (char) ptr[1] : '.',
						(ptr[2] > 32 && ptr[2] < 127) ? (char) ptr[2] : '.',
						(ptr[3] > 32 && ptr[3] < 127) ? (char) ptr[3] : '.'
					);
				}
				printf("\n");
			}
		}
	}
}


int main(int argc, char* argv[])
{
	const char * name_file = "../../misc/name";
	const char * host_name = "cfc-866-reth1";
	const char * log_file = "fgcethlogs.log";

	const char * code_folder = "../../misc/codeFiles";
	//const char * code_file   = "misc/codeFiles/C002_31_IDProg";

    struct fgceth_names_container names_container;
    struct fgceth_codes_container codes_container;

    fgceth_errno    err;

    FILE * f;

    f = fopen(log_file, "w");
	if (f == NULL) {
		printf("Can't open file %s to redirect stdout and stderr", log_file);
	}

	fgcethSetLogFileInit(f);
	fgcethSetLogFileExec(f);

    memset(&names_container, 0, sizeof(names_container));
    memset(&codes_container, 0, sizeof(codes_container));

	if((err = fgcethNamesParser(name_file, host_name, &names_container)) != FGC_SUCCESS)
	{
		printf("fgcethNamesParser failed with error %d (%s)", err, fgcethStrerr(err));
		exit(1);
	}

	printNamesContainer(&names_container);

	if((err = fgcethCodesParser(code_folder, &codes_container)) != FGC_SUCCESS)
	{
		printf("fgcethCodesParser failed with error %d (%s)", err, fgcethStrerr(err));
		exit(1);
	}

	printCodesContainer(&codes_container);

	#define MB(x) ((uint32_t) (x) << 20)

	if((err = fgcethInit("enp2s0", &names_container, &codes_container, MB(128), 0)) != FGC_SUCCESS)
	{
		printf("fgcethInit failed with error %d (%s)", err, fgcethStrerr(err));
		exit(1);
	}

	printf("Press key to finish broadcasting codes and names...\n");
	getchar();

	fgcethClose();

	fgcethFreeContainers(&names_container, &codes_container);

	fgcethClose();
	fclose(f);

    return 0;
}



