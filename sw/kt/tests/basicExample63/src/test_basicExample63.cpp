/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   test_basicExample63.cpp
 * @brief  Code example - Basic example for class 63
 * @author Dariusz Zielinski
 */

#include <libfgceth.h>
#include <../utils/test.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void printUsage()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name\n");
}


int main(int argc, char* argv[])
{
	int     option;
	char    dev_name[128]    = {0};
	int     dev_id           = 0;
	char    names_file[128]  = {0};
	char    gw_name[128]     = {0};
	char    eth_name[128]    = {0};

    char    read_buffer[128];

	/// Error variable
	fgceth_errno    err;

	struct fgceth_names_container names_container;

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(dev_name, optarg);
				 dev_id = atoi(dev_name);
				 break;
			 case 'n':
				 strcpy(names_file, optarg);
				 break;
			 case 'g':
				 strcpy(gw_name, optarg);
				 break;
			 default :
				 printUsage();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((dev_name[0] == 0 && dev_id == 0) || eth_name[0] == 0 || names_file[0] == 0 || gw_name[0] == 0)
	{
		printUsage();
		exit(1);
	}

	fgcethSetLogPrintFlags(L_V0);

	if((err = fgcethNamesParser(names_file, gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser: Failed with error %d: %s\n", err, fgcethStrerr(err));
		exit(1);
	}

    // Initialize the library
    
    #define MB(x) ((uint32_t) (x) << 20) 

    if (fgcethInit(eth_name, &names_container, NULL, MB(128), 0) != FGC_SUCCESS)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit: Failed to initialize the library \n");
        exit(1);
    }

    // get dev_id from device name
	if(dev_id == 0)
	{
		dev_id = fgcethGetDevId(dev_name);
	}

    // Wait until device is ready and PLL is locked
    while (!fgcethIsDeviceReady(dev_id))
    {
        sleep(1);
    }

    // Structure used to hold a command response
    fgceth_rsp * response = fgcethCreateResponse();

    fgcethConfigCmd(response, "TAG_1", SET, dev_name, "MODE.PC_SIMPLIFIED", "ON");

    // Turn on the power converter
    if (fgcethGetSet(response) != FGC_SUCCESS)
    {
    	fgcethErrorLog(dev_id, "fgcethSet: Failed to power on the converter: %s \n", fgcethStrerr(response->d.cmd_rsp.error));
        exit(1);
    }

    // Wait until the converter is on
    
    fgcethConfigCmd(response, "TAG_2", GET, dev_name, "MODE.PC_SIMPLIFIED", ""); 
    
    do
    {
        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read the converter state: %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }

        sleep(1);

        blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

    }while (strcmp(read_buffer, "OFF") == 0);


    // Check if the state of the converter is ON (as it may be FAULT as well)
    if (strcmp(read_buffer, "ON") != 0)
    {
    	fgcethErrorLog(dev_id, "STATE.PC: The converter is not in ON state \n");
        exit(1);
    }


    // Set control value (i.e. current (or voltage) value)
    
    fgcethConfigCmd(response, "", SET, dev_name, "REF.CCV.VALUE", "10"); 
    
    if (fgcethGetSet(response) != FGC_SUCCESS)
    {
    	fgcethErrorLog(dev_id, "Failed to set current to 10 A: %s \n", fgcethStrerr(response->d.cmd_rsp.error));
        exit(1);
    }

    // Start monitoring until the voltage is close to 10 A
    do
    {
        fgcethConfigCmd(response, NULL, GET, dev_name, "STATE.PC_SIMPLIFIED", NULL);

        // Check state of the converter
        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read the converter state: %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }
        else
        {
            blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

            if (strcmp(read_buffer, "ON") != 0)
            {
            	fgcethErrorLog(dev_id, "STATE.PC: The converter is not in ON state \n");
                exit(1);
            }
        }

        // Read current
        
        fgcethConfigCmd(response, NULL, GET, dev_name, "MEAS.I", NULL); 
        
        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read current: %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }
        else
        {
        	blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

            fgcethLog(dev_id, L_V0, "Read current = %s A \n", read_buffer);
        }

        // Read voltage
        
        fgcethConfigCmd(response, NULL, GET, dev_name, "MEAS.V", NULL);
        
        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read voltage: %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }
        else
        {
        	blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

            fgcethLog(dev_id, L_V0, "Read voltage = %s V \n", read_buffer);
        }

        sleep(1);
    }
    while ( abs(strtod(read_buffer, NULL) - 10) > 0.1 );
}
