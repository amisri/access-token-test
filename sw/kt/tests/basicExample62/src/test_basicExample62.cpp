/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   test_basicExample62.cpp
 * @brief  Code example - Basic example for class 62
 * @author Dariusz Zielinski
 */

#include <libfgceth.h>
#include <../utils/test.h>

#include <stdio.h>
#include <stdlib.h>

#define USE_CONFIG_CMD

void printUsage()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name\n");
}

int main(int argc, char* argv[])
{
	int     option;
	char    dev_name[128]    = {0};
	int     dev_id           = 0;
	char    names_file[128]  = {0};
	char    gw_name[128]     = {0};
	char    eth_name[128]    = {0};

    char    read_buffer[128];

	/// Error variable
	fgceth_errno    err;

	struct fgceth_names_container names_container;

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(dev_name, optarg);
				 dev_id = atoi(dev_name);
				 break;
			 case 'n':
				 strcpy(names_file, optarg);
				 break;
			 case 'g':
				 strcpy(gw_name, optarg);
				 break;
			 default :
				 printUsage();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((dev_name[0] == 0 && dev_id == 0) || eth_name[0] == 0 || names_file[0] == 0 || gw_name[0] == 0)
	{
		printUsage();
		exit(1);
	}

	fgcethSetLogPrintFlags(L_V0);

	if((err = fgcethNamesParser(names_file, gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser: Failed with error %d: %s\n", err, fgcethStrerr(err));
		exit(1);
	}

    // Initialize the library
    
    #define MB(x) ((uint32_t) (x) << 20) 
    
    if (fgcethInit(eth_name, &names_container, NULL, MB(128), 0) != FGC_SUCCESS)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit: Failed to initialize the library \n");
        exit(1);
    }

    // get dev_id from device name
	if(dev_id == 0)
	{
		dev_id = fgcethGetDevId(dev_name);
	}

    // Wait until device is ready and PLL is locked
    while (!fgcethIsDeviceReady(dev_id))
    {
        sleep(1);
    }

    // Structure used to hold a command response
    fgceth_rsp * response = fgcethCreateResponse();

    // Turn on the power converter
    
#ifdef USE_CONFIG_CMD
    // Option 1 - Use function 'fgcethConfigCmd' to simplify command configuration
    fgcethConfigCmd(response, "TAG_1", SET, dev_name, "MODE.PC_SIMPLIFIED", "ON");
#else
    // Option 2 - Directly manipulate 'fgceth_rsp' structure members (in cmd_rsp mode)
    strcpy(response->tag, "TAG_1");
    response->d.cmd_rsp.cmd_type = SET;
    strcpy(cmd->dev_name, dev_name); // Could be device ID (converted to string format)
    strcpy(cmd->property, "MODE.PC_SIMPLIFIED");
    blkbufAppendStr(cmd->value_opts, "ON");
#endif
     
    if (fgcethGetSet(response) != FGC_SUCCESS)
    {
    	fgcethErrorLog(dev_id, "fgcethSet: Failed to power on the converter. %s \n", fgcethStrerr(response->d.cmd_rsp.error));
        exit(1);
    }

    // Wait until the converter is on
    
#ifdef USE_CONFIG_CMD
    // Option 1 - Use function 'fgcethConfigCmd' to simplify command configuration
    fgcethConfigCmd(response, "TAG_2", GET, dev_name, "MODE.PC_SIMPLIFIED", "");
#else
    // Option 2 - Directly manipulate 'fgceth_rsp' structure members (in cmd_rsp mode)
    strcpy(response->tag, "TAG_2");
    response->d.cmd_rsp.cmd_type = GET;
    strcpy(cmd->dev_name, dev_name);     
    strcpy(cmd->property, "MODE.PC_SIMPLIFIED");
    blkbufWriteStr(cmd->value_opts, ""); // A write will erase previous information
#endif   
    
    do
    {
        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read the converter state. %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }

        sleep(1);

        blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

    } while (strcmp(read_buffer, "OFF") == 0);


    // Check if the state of the converter is ON (as it may be FAULT as well)
    if (strcmp(read_buffer, "ON") != 0)
    {
    	fgcethErrorLog(dev_id, "STATE.PC: The converter is not in ON state \n");
        exit(1);
    }

#ifdef USE_CONFIG_CMD
    // Option 1 - Use function 'fgcethConfigCmd' to simplify command configuration
    fgcethConfigCmd(response, "", SET, dev_name, "REF.PULSE.REF.VALUE", "100");
#else
    // Option 2 - Directly manipulate 'fgceth_rsp' structure members (in cmd_rsp mode)
    response->tag[0] = '\0'; // TAG is always optional
    response->d.cmd_rsp.cmd_type = SET;
    strcpy(cmd->dev_name, dev_name);     
    strcpy(cmd->property, "REF.PULSE.REF.VALUE");
    blkbufWriteStr(cmd->value_opts, "100"); 
#endif   

    // Set control value (i.e. voltage or current value)
    if (fgcethGetSet(response) != FGC_SUCCESS)
    {
    	fgcethErrorLog(dev_id, "Failed to set current to 100 A. %s \n", fgcethStrerr(response->d.cmd_rsp.error));
        exit(1);
    }


    // Start monitoring for 10 seconds
    for (int i = 0; i < 10; ++i)
    {

#ifdef USE_CONFIG_CMD
        // Option 1 - Use function 'fgcethConfigCmd' to simplify command configuration
        fgcethConfigCmd(response, NULL, GET, dev_name, "STATE.PC_SIMPLIFIED", NULL); // Both TAG and VALUE/OPTIONS can be accepted as NULL. The other parameters cant.
#else
        // Option 2 - Directly manipulate 'fgceth_rsp' structure members (in cmd_rsp mode)
        response->tag[0] = '\0'; 
        response->d.cmd_rsp.cmd_type = GET;
        strcpy(cmd->dev_name, dev_name);     
        strcpy(cmd->property, "REF.PULSE.REF.VALUE");
        blkbufWriteStr(cmd->value_opts, ""); 
#endif  

        // Check state of the converter
        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read the converter state. %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }
        else
        {
            blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

            if (strcmp(read_buffer, "ON") != 0)
            {
            	fgcethErrorLog(dev_id, "STATE.PC: The converter is not in ON state \n");
                exit(1);
            }
        }

        // Read current
        
#ifdef USE_CONFIG_CMD
        // Option 1 - Use function 'fgcethConfigCmd' to simplify command configuration
        fgcethConfigCmd(response, NULL, GET, dev_name, "MEAS.I", NULL); // Both TAG and VALUE/OPTIONS can be accepted as NULL. The other parameters cant.
#else
        // Option 2 - Directly manipulate 'fgceth_rsp' structure members (in cmd_rsp mode)
        response->tag[0] = '\0'; 
        response->d.cmd_rsp.cmd_type = GET;
        strcpy(cmd->dev_name, dev_name);     
        strcpy(cmd->property, "MEAS.I");
        blkbufWriteStr(cmd->value_opts, ""); 
#endif 

        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read current. %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }
        else
        {
            blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

        	fgcethLog(dev_id, L_V0, "Read current = %s A \n", read_buffer);
        }

        // Read voltage
        
#ifdef USE_CONFIG_CMD
        // Option 1 - Use function 'fgcethConfigCmd' to simplify command configuration
        fgcethConfigCmd(response, NULL, GET, dev_name, "MEAS.V", NULL); // Both TAG and VALUE/OPTIONS can be accepted as NULL. The other parameters cant.
#else
        // Option 2 - Directly manipulate 'fgceth_rsp' structure members (in cmd_rsp mode)
        response->tag[0] = '\0'; 
        response->d.cmd_rsp.cmd_type = GET;
        strcpy(cmd->dev_name, dev_name);     
        strcpy(cmd->property, "MEAS.V");
        blkbufWriteStr(cmd->value_opts, ""); 
#endif 

        if (fgcethGetSet(response) != FGC_SUCCESS)
        {
        	fgcethErrorLog(dev_id, "fgcethGet: Failed to read voltage. %s \n", fgcethStrerr(response->d.cmd_rsp.error));
            exit(1);
        }
        else
        {
            blkbufCopyStr(read_buffer, response->d.cmd_rsp.response, sizeof(read_buffer));

        	fgcethLog(dev_id, L_V0, "Read voltage = %s V \n", read_buffer);
        }

        sleep(1);
    }
}
