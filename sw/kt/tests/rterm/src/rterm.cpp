/*!
 * © Copyright CERN 2018. All rights not expressly granted are reserved.
 * @file   rterm.cpp
 * @brief  Testing of remote terminal functionality
 * @author Joao Afonso
 */

#include <libfgceth.h>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <termios.h>

#define RTERM_DISCONNECT_CHAR   0x18

#define TELNET_ESC              0xFF

#define TELNET_WILL             0xFB
#define TELNET_WONT             0xFD
#define TELNET_DO               0xFC
#define TELNET_DONT             0xFE

#define TELNET_LINE             0x22

/// Response queue for the remote terminal responses
fgceth_rsp_queue *  queue;

struct termios  line_mode;
struct termios  raw_mode;

bool running = true;

void CleanUp(int sig_num)
{

	running = false;

}

static void* devListenThread(void *unused)
{
	fgceth_rsp *response;

    while (running)
    {

    	while(!(response = fgcethQueuePop(queue)) && running)
    	{
    		usleep(20000);
    	}

    	if(!running)
    	{
    		fgcethDestroyResponse(response);
    		break;
    	}

        if(response->type != RTERM) // Response of wrong type
        {
        	fgcethDestroyResponse(response);
        	continue;
        }

        // ***************************************************

        printf("%s", response->d.rterm.buffer);
        fflush(stdout);

        fgcethDestroyResponse(response);
    }

    pthread_exit(NULL);
}

void printUsage()
{
    fprintf(stderr, "Usage: -i eth_name -d dev_name (or ID) -n names_file -g gw_name -l log_file\n");
}

/**************************************
 * Main thread
***************************************/

int main(int argc, char* argv[])
{
	int     option;
	char    dev_name[128]    = {0};
	int     dev_id           = 0;
	char    names_file[128]  = {0};
	char    gw_name[128]     = {0};
	char    eth_name[128]    = {0};
	char    log_file[128]     = {0};

	fgceth_sub_handle handle;
	pthread_t         rcv_thread;

	/// Error variable
	fgceth_errno    err;

	struct fgceth_names_container names_container;

	fgcethSetLogPrintFlags(L_V0);

	// Read parameters from the command line
	while ((option = getopt(argc, argv, "i:d:n:g:l:")) != -1)
	{
		switch (option)
		{
			 case 'i':
				 strcpy(eth_name, optarg);
				 break;
			 case 'd':
				 strcpy(dev_name, optarg);
				 dev_id = atoi(dev_name);
				 break;
			 case 'n':
				 strcpy(names_file, optarg);
				 break;
			 case 'g':
				 strcpy(gw_name, optarg);
				 break;
			 case 'l':
				 strcpy(log_file, optarg);
				 break;
			 default :
				 printUsage();
				 exit(1);
		}
	}

	memset(&names_container, 0, sizeof(names_container));

	// Check for required arguments
	if ((dev_name[0] == 0 && dev_id == 0) || eth_name[0] == 0 || names_file[0] == 0 || gw_name[0] == 0 || log_file[0] == 0)
	{
		printUsage();
		exit(1);
	}

	FILE * f = fopen(log_file, "w");

	fgcethSetLogFileExec(f);
	fgcethSetLogFileInit(f);

	// Check names file
	if((err = fgcethNamesParser(names_file, gw_name, &names_container)) != FGC_SUCCESS)
	{
		fgcethErrorLog(FGCETH_NO_DEV, "fgcethNamesParser failed with error %d (%s)", err, fgcethStrerr(err));
		exit(1);
	}

    /// Set 3 second timeout
    fgcethSetTimeout(3000);

    /// Declare the device being used
    //fgcethDeclareDevice(dev_id, "", 63);

    #define MB(x) ((uint32_t) (1 << 20))

    if ( (err = fgcethInit(eth_name, &names_container, NULL, MB(256), 0)) != FGC_SUCCESS)
    {
    	fgcethErrorLog(FGCETH_NO_DEV, "fgcethInit failed with error %d (%s)", err, fgcethStrerr(err));
        return 1;
    }

    fgcethFreeContainers(&names_container, NULL);

    // get dev_id from device name
	if(dev_id == 0)
	{
		dev_id = fgcethGetDevId(dev_name);
	}

    fgcethLog(FGCETH_NO_DEV, L_V0, "Library initialized");


    // Create the queue for the Another Thread
    queue = fgcethQueueCreate(1024);

    tcgetattr(STDIN_FILENO, &line_mode);
    raw_mode = line_mode;

    raw_mode.c_iflag       &= ~(INLCR|ICRNL|IXON);
    raw_mode.c_oflag       &= ~(OPOST);
    raw_mode.c_lflag        = 0;
    raw_mode.c_cc[VMIN]     = 1;
    raw_mode.c_cc[VTIME]    = 0;

    tcsetattr(STDIN_FILENO, 0, &raw_mode);

    // Create and start the Another Thread
    pthread_create(&rcv_thread, NULL, devListenThread, NULL);

    fgcethRtermSubscribeDev(dev_id, queue, &handle);

    // Catch signals to clean-up

    struct sigaction sig;

    memset(&sig, 0, sizeof(sig));
    sig.sa_handler = CleanUp;

    sigemptyset(&sig.sa_mask);
    sigaddset(&sig.sa_mask, SIGHUP);
    sigaddset(&sig.sa_mask, SIGINT);
    sigaddset(&sig.sa_mask, SIGPIPE);
    sigaddset(&sig.sa_mask, SIGQUIT);
    sigaddset(&sig.sa_mask, SIGTERM);

    sigaction(SIGHUP,  &sig, NULL);
    sigaction(SIGINT,  &sig, NULL);
    sigaction(SIGPIPE, &sig, NULL);
    sigaction(SIGQUIT, &sig, NULL);
    sigaction(SIGTERM, &sig, NULL);

    // Clean screen

    printf("\33c");
    //fgcethRtermSendToDev(dev_id, "\x1B\x1B", 2);

    // Send stdin characters

    char c;
    while(running && fread(&c, 1, 1, stdin))
    {
    	switch(c)
		{
			case RTERM_DISCONNECT_CHAR:
				CleanUp(SIGQUIT);
				break;

			default:
				fgcethRtermSendToDev(dev_id, &c, 1);
				break;
		}

    }

    pthread_join(rcv_thread, NULL);

    fgceth_rsp *response;

    while((response = fgcethQueuePop(queue)) != NULL)
    {
    	fgcethDestroyResponse(response);
    }

    fgcethQueueDestroy(queue);

    fgcethClose();

	// Restore terminal to line-mode

	tcsetattr(STDIN_FILENO, 0, &line_mode);
	printf("\33c");

	exit(0);
}
