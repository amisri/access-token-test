#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>

int main(int argc, char * argv[])
{
    if(argc < 2)
    {
        printf("Argument missing: offset in us\n");
    }

    int offset_us = atoi(argv[1]);
    struct timeval delta;
    delta.tv_sec = offset_us / 1000000;
    delta.tv_usec = offset_us % 1000000;
    
    if(adjtime(&delta, NULL) != 0)
    {
        printf("adjtime() failed with error '%s'\n", strerror(errno));
    }
}
