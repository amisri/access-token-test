#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void time_handler(int C1, int C2)
{
    static long int prev_diff = 0;
    static long int acc_err = 0;
    static int      samples = 0;
    static double   avg_err = 0;

    char buf[128];
    struct tm * tmi;

    struct timespec time_real, time_c1, time_c2;
    clock_gettime(CLOCK_REALTIME, &time_real);
    clock_gettime(C1, &time_c1);
    clock_gettime(C2, &time_c2); 

    tmi = gmtime((time_t *) &time_real.tv_sec);

    long int diff = ((time_c2.tv_sec - time_c1.tv_sec) * 1000000000) + (time_c2.tv_nsec - time_c1.tv_nsec);
    long int error_diff = prev_diff ? diff - prev_diff : 0;

    prev_diff  = diff;
    acc_err   += error_diff;
    samples   += 1;
    avg_err    = ((double) acc_err) / samples;

    printf("[%4d-%02d-%02d %02d:%02d:%02d.%09ld UTC] - %ld ns (diff:%+-6ld acc:%+-6ld avg:%.3f)\n", tmi->tm_year + 1900, tmi->tm_mon, tmi->tm_mday, tmi->tm_hour, tmi->tm_min, tmi->tm_sec, time_real.tv_nsec, diff, error_diff, acc_err, avg_err);

}

int parse_clock(char * s)
{
    if(!strcmp("real", s))
    {
        return CLOCK_REALTIME;
    }
    else if(!strcmp("mono", s))
    {
        return CLOCK_MONOTONIC;
    }
    else if(!strcmp("raw", s))
    {
        return CLOCK_MONOTONIC_RAW;
    }
    else
    {
        return -1;
    }
}

void print_args()
{
    printf("Parameters:\n");
    printf(" <refresh period (s)>\n");
    printf(" <clock 1 (real, mono)> -> used also as interrupt\n");
    printf(" <clock 2 (real, mono, raw)>\n");
}


int main(int argc, char * argv[])
{  
    if(argc < 4)
    {
        print_args();
        return 1;
    }

    int clock_1 = parse_clock(argv[2]);
    int clock_2 = parse_clock(argv[3]);

    if(clock_1 < 0 || clock_2 < 0)
    {
        print_args();
        return 1;
    }

    int refresh_period = atoi(argv[1]);
    struct timespec sleep_time;
 
    clock_gettime(clock_1, &sleep_time);

    while(1)
    {
        sleep_time.tv_sec  = sleep_time.tv_sec + refresh_period;
        sleep_time.tv_nsec = 0;

        if(clock_nanosleep(clock_1, TIMER_ABSTIME, &sleep_time, NULL))
        {
            printf("Error\n");
            return 1;
        }

        time_handler(clock_1, clock_2);
    }


    return 0;
}




