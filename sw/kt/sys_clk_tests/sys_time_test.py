import subprocess
import time
import queue
import threading
import signal
import unittest
import pyfgc

PATH_FGCETH = "/home/jlhenriq/projects/fgc/sw/kt"
LOG_FILE = "sys_time_test.log"
PATH_NAME_FILE = f"{PATH_FGCETH}/misc/pcte23185_files/name_file"
ETH_NAME = "enp2s0"
FGC_NAME = "FGC_13"

class GwMgr:

    def __init__(self):
        self.que = queue.Queue()
        self.thr = None
        self.process = None

    def _run_gw(self):
        """
        To be run in a separate thread.
        Executes the FGC Ether gateway, and prints all the stdout and stderr to a private queue.

        :return:
        """

        gw_command = f"{PATH_FGCETH}/fgcethd/Linux/x86_64/fgcethd -e {ETH_NAME} -n " + PATH_NAME_FILE

        self.process = subprocess.Popen(gw_command.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        time.sleep(3)

        pyfgc.connect([FGC_NAME], "sync", name_file=PATH_NAME_FILE, rbac_token="BIN 1")

        with open(LOG_FILE, 'w') as f:
            for line in iter(self.process.stdout.readline, b''):
                print(line, file=f)
                self.que.put(line.decode().strip())

        self.process.stdout.close()

        return_code = self.process.wait()

        if return_code:
            raise subprocess.CalledProcessError(return_code, gw_command)

    def start(self):
        """
        Launches the '_run_gw()' thread.

        :return:
        """

        self.thr = threading.Thread(target=self._run_gw)
        self.thr.start()

    def stop(self):
        """
        Stops the thread gracefully. If it fails, sends a kill signal.

        :return:
        """

        self.process.send_signal(signal.SIGINT)
        self.thr.join(timeout=3.0)

        # In case it failed...
        if self.thr.is_alive():
            self.process.send_signal(signal.SIGKILL)
            self.thr.join()

    def get_new_output(self):
        """
        Gets the list of the last stdout and stderr outputs that were printed by the gateway.

        :return: List of previous stdout and stderr outputs.
        """

        log = []

        while not self.que.empty():
            log.append(self.que.get_nowait())

        return log

    @staticmethod
    def is_fgc_ready():

        return pyfgc.get("FGC.PLL.STATE", devices=FGC_NAME) == 'LOCKED'


class SysClockMgr:

    RESTORE_SYNC = "sync"
    RESTORE_INV = "inv"
    RESTORE_NOT = "not"

    def __init__(self):
        self.registered_offset = 0
        self.ntp_online = True

    def displace_clock_s(self, displacement):

        self.registered_offset += displacement
        curr_time = time.clock_gettime(time.CLOCK_REALTIME)

        try:
            time.clock_settime(time.CLOCK_REALTIME, curr_time + displacement)
        except PermissionError:
            print("Setting new time not permitted.")

    @property
    def offset(self):
        return self.registered_offset

    @staticmethod
    def get_realtime_clock():
        return time.clock_gettime(time.CLOCK_REALTIME)

    @staticmethod
    def get_monotonic_clock():
        return time.clock_gettime(time.CLOCK_MONOTONIC)

    def ntp_stop(self):
        subprocess.check_call("service ntp stop", shell=True)
        self.ntp_online = False

    def ntp_start(self):
        subprocess.check_call("service ntp start", shell=True)
        self.ntp_online = True

    def ntp_reset(self, force_sync=False):
        self.ntp_stop()
        if force_sync:
            # Do not enable '-g' flag! (allow big adjustments)
            subprocess.check_call("ntpd -q", stdout=subprocess.DEVNULL, shell=True)
        self.ntp_start()
        self.registered_offset = 0
        self.ntp_online = True

    def restore_time(self, restore=RESTORE_INV):
        if restore is self.RESTORE_SYNC:
            self.ntp_reset(force_sync=True)
            self.registered_offset = 0
        elif restore is self.RESTORE_INV:
            self.displace_clock_s(-self.registered_offset)
            self.registered_offset = 0
        elif restore is self.RESTORE_NOT:
            pass


def filter_messages(value, sub_strings=None):
    if sub_strings is None:
        return True
    else:
        return any(sub_string in value for sub_string in sub_strings)


TIME_PACKET_ISSUES = [
    'New timing signal detected',
    'Timing signal may have',
    'took longer than expected'
]

PLL_ISSUES = [
    'Detected a transition from',
    'Unlock transition',
    'Sequential number of status',
    'Status packet was missed'
]

ANY_ISSUES = [
    ''
]


class TestGwClock(unittest.TestCase):

    gw = None
    main_clk = None

    def __init__(self, *kwarg, **kwargs):
        super().__init__(*kwarg, **kwargs)
        self.clk = None

    def assertNoIssue(self, issues):
        output = filter(lambda x: filter_messages(x, issues), TestGwClock.gw.get_new_output())
        self.assertFalse(any(output), msg='\n'.join(output))

    @classmethod
    def setUpClass(cls):

        cls.main_clk = SysClockMgr()
        cls.main_clk.restore_time(SysClockMgr.RESTORE_SYNC)

        cls.gw = GwMgr()
        cls.gw.start()

        time.sleep(5)

        while not cls.gw.is_fgc_ready():
            time.sleep(1)

    @classmethod
    def tearDownClass(cls):
        cls.gw.stop()
        cls.main_clk.restore_time(SysClockMgr.RESTORE_SYNC)

    def setUp(self):

        # TODO: Have an exit condition in case it does not lock
        while not TestGwClock.gw.is_fgc_ready():
            time.sleep(1)

        # To clean all messages preceding the tests
        TestGwClock.gw.get_new_output()
        self.clk = SysClockMgr()

    def tearDown(self):

        time.sleep(.1)
        self.clk.restore_time(SysClockMgr.RESTORE_INV)

    def test_XS_offset(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0005)
        time.sleep(.1)

        self.assertNoIssue(PLL_ISSUES)

    def test_XS_offset_jitter_1(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0005)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0015)
        time.sleep(.02)
        self.clk.displace_clock_s(.0005)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0005)
        time.sleep(.02)
        self.clk.displace_clock_s(.0015)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0005)
        time.sleep(.02)

        self.assertNoIssue(PLL_ISSUES)

    def test_XS_offset_jitter_2(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0005)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0015)
        time.sleep(.2)
        self.clk.displace_clock_s(.0005)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0005)
        time.sleep(.2)
        self.clk.displace_clock_s(.0015)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0005)
        time.sleep(.2)

        self.assertNoIssue(PLL_ISSUES)

    def test_S_offset(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0055)
        time.sleep(.1)

        self.assertNoIssue(PLL_ISSUES)

    def test_S_offset_jitter_1(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0055)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0155)
        time.sleep(.02)
        self.clk.displace_clock_s(.0055)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0055)
        time.sleep(.02)
        self.clk.displace_clock_s(.0155)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0055)
        time.sleep(.02)

        self.assertNoIssue(PLL_ISSUES)
        
    def test_S_offset_jitter_2(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0055)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0155)
        time.sleep(.2)
        self.clk.displace_clock_s(.0055)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0055)
        time.sleep(.2)
        self.clk.displace_clock_s(.0155)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0055)
        time.sleep(.2)

        self.assertNoIssue(PLL_ISSUES)

    def test_M_offset(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0555)
        time.sleep(.1)

        self.assertNoIssue(PLL_ISSUES)

    def test_M_offset_jitter_1(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0555)
        time.sleep(.02)
        self.clk.displace_clock_s(-.1555)
        time.sleep(.02)
        self.clk.displace_clock_s(.0555)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0555)
        time.sleep(.02)
        self.clk.displace_clock_s(.1555)
        time.sleep(.02)
        self.clk.displace_clock_s(-.0555)
        time.sleep(.02)

        self.assertNoIssue(PLL_ISSUES)
        
    def test_M_offset_jitter_2(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.0555)
        time.sleep(.2)
        self.clk.displace_clock_s(-.1555)
        time.sleep(.2)
        self.clk.displace_clock_s(.0555)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0555)
        time.sleep(.2)
        self.clk.displace_clock_s(.1555)
        time.sleep(.2)
        self.clk.displace_clock_s(-.0555)
        time.sleep(.2)

        self.assertNoIssue(PLL_ISSUES)

    def test_L_offset(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.5555)
        time.sleep(.1)

        self.assertNoIssue(PLL_ISSUES)

    def test_L_offset_jitter_1(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(-1.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(-.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(1.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(-.5555)
        time.sleep(.02)

        self.assertNoIssue(PLL_ISSUES)
        
    def test_L_offset_jitter_2(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(-1.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(-.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(1.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(-.5555)
        time.sleep(.2)

        self.assertNoIssue(PLL_ISSUES)

    def test_XL_offset(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(5.5555)
        time.sleep(.1)

        self.assertNoIssue(PLL_ISSUES)

    def test_XL_offset_jitter_1(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(5.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(-15.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(5.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(-5.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(15.5555)
        time.sleep(.02)
        self.clk.displace_clock_s(-5.5555)
        time.sleep(.02)

        self.assertNoIssue(PLL_ISSUES)
        
    def test_XL_offset_jitter_2(self):

        # 100's of milliseconds
        self.clk.displace_clock_s(5.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(-15.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(5.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(-5.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(15.5555)
        time.sleep(.2)
        self.clk.displace_clock_s(-5.5555)
        time.sleep(.2)

        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_S_time_correction(self):

        self.clk.displace_clock_s(.0555)
        self.clk.ntp_reset(force_sync=True)
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_M_time_correction(self):
        self.clk.displace_clock_s(.5555)
        self.clk.ntp_reset(force_sync=True)
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_L_time_correction(self):
        self.clk.displace_clock_s(5.5555)
        self.clk.ntp_reset(force_sync=True)
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_off_on_10ms(self):
        self.clk.ntp_stop()
        time.sleep(.01)
        self.clk.ntp_start()
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_off_on_100ms(self):
        self.clk.ntp_stop()
        time.sleep(.1)
        self.clk.ntp_start()
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_off_on_1s(self):
        self.clk.ntp_stop()
        time.sleep(1)
        self.clk.ntp_start()
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)

    def test_NTP_off_on_10s(self):
        self.clk.ntp_stop()
        time.sleep(10)
        self.clk.ntp_start()
        time.sleep(1)
        self.assertNoIssue(PLL_ISSUES)


if __name__ == "__main__":
    unittest.main()


# clk_mgr = SysClockMgr()
# clk_mgr.displace_clock_s(2)
# time.sleep(2)
# clk_mgr.restore_time()

# gw = GwMgr()
# gw.start()
#
# time.sleep(5)
# print('\n'.join(gw.get_new_output()))
#
# gw.stop()
#
# print('\n'.join(gw.get_new_output()))
