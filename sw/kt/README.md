# FGC Ether Package

This directory contains most of the FGC Ether software modules, that provide various services related to the operation of the FGCs (Function Generator Controlers). 

They are intended to be used by CERN and other laboratories, under a knowledge transfer agreement.

## Main modules

### FGC Ether Gateway

The FGC Ether Gateway daemon enables the communication with the FGCs (Functin Generator Controlers) through a set of standard interfaces.
It is the required by all the other modules to communicate with the FGCs.

More information can be found im *fgcethd/README.md*.

### FGC Ether Remote Terminal

The FGC Ether Remote Terminal can be used to open a remote terminal session with any FGC under the control of the FGC Ether Gateway.

Additionally, it is also able to send commands directly from files to a FGC (as well as individual commands) using the command-response protocol.

More information can be found in *fgcethterm/README.md*.

### FGC Ether Library

The FGC Ether Library provides a set of tools to interact with the FGCs.
This library is required by other FGC Ether modules, such as the FGC Ether Gateway and the FGC Ether Remote Terminal.

More information can be found in *libfgceth/README.md*.

### FGC Ether Post-Mortem Server

The Post-Mortem server is a daemon responsible for fetching and saving Post-Mortem, Slow-abort and DIM logs. Without human intervention. 

*Currently under development*

## Compilation

Type `make` in the project root folder. This will trigger the compilation of all major FGC Ether Software modules.
```
cd <projects>/fgc/sw/kt
make
```

## Wireshark plugin

A Wireshark plugin is provided in a *wireshark*  directory. It can be added to your Wireshark program to enable FGC_Ether packet parsing and interpretation.

Installation is very simple and all the instructions are provided in a file *wireshark/HowToInstall.txt*. 
 
