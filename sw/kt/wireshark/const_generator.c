/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   const_generator.c
 * @brief  FGC_Ether Wireshark plugin.
 * @author Dariusz Zielinski
 */

#define FGC_GLOBALS

#include <stdio.h>
#include <inc/fgc_ether.h>
#include <inc/fgc_event.h>
#include <inc/fgc_errs.h>

#define addHex(x) printf(#x " = 0x%04x\n", (x))
#define addInt(x) printf(#x " = %d\n", (int)(x))

int main()
{
	printf("-- FGC_Ether Protocol plugin for Wireshark\n\n");

	addHex(FGC_ETHER_TYPE);

	// Payload types
	addInt(FGC_ETHER_PAYLOAD_TIME);
	addInt(FGC_ETHER_PAYLOAD_STATUS);
	addInt(FGC_ETHER_PAYLOAD_CMD);
	addInt(FGC_ETHER_PAYLOAD_RSP);
	addInt(FGC_ETHER_PAYLOAD_PUB);

	// Time packet
	addInt(FGC_EVT_NONE);
	addInt(FGC_EVT_PM);
	addInt(FGC_EVT_START);
	addInt(FGC_EVT_ABORT);
	addInt(FGC_EVT_SSC);
	addInt(FGC_EVT_NEXT_DEST);
	addInt(FGC_EVT_CYCLE_TAG);
	addInt(FGC_EVT_CYCLE_START);
	addInt(FGC_EVT_START_REF_1);
	addInt(FGC_EVT_START_REF_2);
	addInt(FGC_EVT_START_REF_3);
	addInt(FGC_EVT_START_REF_4);
	addInt(FGC_EVT_SUB_DEVICE_CYCLE_START);
	addInt(FGC_EVT_SUB_DEVICE_START_REF_1);
	addInt(FGC_EVT_SUB_DEVICE_START_REF_2);
	addInt(FGC_EVT_SUB_DEVICE_START_REF_3);
	addInt(FGC_EVT_SUB_DEVICE_START_REF_4);
	addInt(FGC_EVT_PAUSE);
	addInt(FGC_EVT_RESUME);
	addInt(FGC_EVT_ECONOMY_DYNAMIC);
	addInt(FGC_EVT_ACQUISITION);
	addInt(FGC_EVT_TRANSACTION_COMMIT);
	addInt(FGC_EVT_TRANSACTION_ROLLBACK);

	// Status packet
	addInt(FGC_ACK_STATUS);
	addInt(FGC_ACK_TIME);
	addInt(FGC_ACK_RD_PKT);
	addInt(FGC_ACK_CMD_TOG);
	addInt(FGC_ACK_PLL);
	addInt(FGC_SELF_PM_REQ);
	addInt(FGC_EXT_PM_REQ);

	// Command packet
	addInt(FGC_FIELDBUS_FLAGS_FIRST_PKT);
	addInt(FGC_FIELDBUS_FLAGS_LAST_PKT);
	addInt(FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK);
	addInt(FGC_FIELDBUS_FLAGS_SET_CMD);
	addInt(FGC_FIELDBUS_FLAGS_GET_CMD);
	addInt(FGC_FIELDBUS_FLAGS_SET_BIN_CMD);
	addInt(FGC_FIELDBUS_FLAGS_SUB_CMD);
	addInt(FGC_FIELDBUS_FLAGS_UNSUB_CMD);
	addInt(FGC_FIELDBUS_FLAGS_GET_SUB_CMD);

	// Response packet
	addInt(FGC_FIELDBUS_FLAGS_SEQ_MASK);
	addInt(FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK);
	addInt(FGC_FIELDBUS_FLAGS_CMD_PKT);
	addInt(FGC_FIELDBUS_FLAGS_PUB_DIRECT_PKT);
	addInt(FGC_FIELDBUS_FLAGS_PUB_GET_PKT);

	// Publication packet
	addInt(FGC_FIELDBUS_PUB_TRIG_MASK);
	addInt(FGC_FIELDBUS_PUB_TRIG_NEW_SUB);
	addInt(FGC_FIELDBUS_PUB_TRIG_SET);

	// Generate table with errors codes and corresponding strings
	printf("\nlocal tErrors = \n{\n");
	
	for (int i = 0; i < FGC_NUM_ERRS; ++i)
	{
		printf("    [%d] = \"%s\"%c\n", i, fgc_errmsg[(fgc_errno)i], (i + 1 !=FGC_NUM_ERRS ? ',' : ' '));
	}

	printf("}\n\n");
}