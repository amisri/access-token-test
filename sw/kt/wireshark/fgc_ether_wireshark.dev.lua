-----------------------------------------------------------
-- Register new protocol
-----------------------------------------------------------

proto = Proto("fgc_ether", "FGC_Ether Protocol")
f     = proto.fields


-----------------------------------------------------------
-- Defaults values for tables
-----------------------------------------------------------

local def = {__index = function () return "Unknown" end}


-----------------------------------------------------------
-- Constants tables
-----------------------------------------------------------

-----------------------------------
-- Defaults for error codes/strings (generated above by the const_generator.c)
setmetatable(tErrors, def)

-----------------------------------
-- Payload types names
local tPayloadNames = 
{
	[FGC_ETHER_PAYLOAD_TIME]   = "Time",
	[FGC_ETHER_PAYLOAD_STATUS] = "Status",
	[FGC_ETHER_PAYLOAD_CMD]    = "Command",
	[FGC_ETHER_PAYLOAD_RSP]    = "Response",
	[FGC_ETHER_PAYLOAD_PUB]    = "Publication"
}
setmetatable(tPayloadNames, def)

-----------------------------------
-- Time tables

local tTimeEvents =
{
    [FGC_EVT_NONE]                   = "None",
    [FGC_EVT_PM]                     = "PM",
    [FGC_EVT_START]                  = "Start",
    [FGC_EVT_ABORT]                  = "Abort",
    [FGC_EVT_SSC]                    = "SSC",
    [FGC_EVT_NEXT_DEST]              = "Next destination",
    [FGC_EVT_CYCLE_TAG]              = "Cycle tag",
    [FGC_EVT_CYCLE_START]            = "Cycle start",
    [FGC_EVT_START_REF_1]            = "Start ref 1",
    [FGC_EVT_START_REF_2]            = "Start ref 2",
    [FGC_EVT_START_REF_3]            = "Start ref 3",
    [FGC_EVT_START_REF_4]            = "Start ref 4",
    [FGC_EVT_SUB_DEVICE_CYCLE_START] = "Sub device cycle start",
    [FGC_EVT_SUB_DEVICE_START_REF_1] = "Sub device start ref_1",
    [FGC_EVT_SUB_DEVICE_START_REF_2] = "Sub device start ref_2",
    [FGC_EVT_SUB_DEVICE_START_REF_3] = "Sub device start ref_3",
    [FGC_EVT_SUB_DEVICE_START_REF_4] = "Sub device start ref_4",
    [FGC_EVT_PAUSE]                  = "Pause",
    [FGC_EVT_RESUME]                 = "Resume",
    [FGC_EVT_ECONOMY_DYNAMIC]        = "Economy dynamic",
    [FGC_EVT_ACQUISITION]            = "Acquisition",
    [FGC_EVT_TRANSACTION_COMMIT]     = "Transaction commit",
    [FGC_EVT_TRANSACTION_ROLLBACK]   = "Transaction rollback",
}
setmetatable(tTimeEvents, def)


-----------------------------------
-- Status tables
local tAckTime =
{
    [0] = "Not received",
    [1] = "Received"
}

local tAckRdPkt =
{
    [0] = "Not received",
    [1] = "Received"
}

local tAckPll =
{
    [0] = "Not locked",
    [1] = "Locked"
}

local tAckPm =
{
    [0] = "Log data ready for readout",
    [1] = "Self-triggered post-mortem requested"
}

local tAckExtPm =
{
    [0] = "No external post-mortem requested",
    [1] = "External post-mortem requested"
}


-----------------------------------
-- Command tables
local tCmdFlags = 
{
    [FGC_FIELDBUS_FLAGS_SET_CMD]     = "Set",
    [FGC_FIELDBUS_FLAGS_GET_CMD]     = "Get",
    [FGC_FIELDBUS_FLAGS_SET_BIN_CMD] = "Set binary",
    [FGC_FIELDBUS_FLAGS_SUB_CMD]     = "Subscribe",
    [FGC_FIELDBUS_FLAGS_UNSUB_CMD]   = "Unsubscribe",
    [FGC_FIELDBUS_FLAGS_GET_SUB_CMD] = "Get subscription"
}
setmetatable(tCmdFlags, def)


-----------------------------------
-- Response tables
local tRspFlags = 
{
    [FGC_FIELDBUS_FLAGS_CMD_PKT]        = "Command",
    [FGC_FIELDBUS_FLAGS_PUB_DIRECT_PKT] = "Pub. direct",
    [FGC_FIELDBUS_FLAGS_PUB_GET_PKT]    = "Pub. get",
}
setmetatable(tRspFlags, def)


-----------------------------------
-- Publication tables
local tPubNewSet =
{
    [FGC_FIELDBUS_PUB_TRIG_NEW_SUB] = "Triggered by the start of a subscription",
    [FGC_FIELDBUS_PUB_TRIG_SET]     = "Triggered by a set command"
}



-----------------------------------------------------------
-- Subdissector : Time packet
-----------------------------------------------------------

f.timeFgcId          = ProtoField.uint8 ("fgc_ether.time.fgcId"            , "FGC ID")
f.timeFgcChar        = ProtoField.uint8 ("fgc_ether.time.fgcChar"          , "FGC rterm character")
f.timeMsTime         = ProtoField.uint16("fgc_ether.time.msTime"           , "Ms time")
f.timeUnixTime       = ProtoField.absolute_time("fgc_ether.time.unixTime"  , "Unix time")
f.timeNamedbCrc      = ProtoField.uint16("fgc_ether.time.namedbCrc"        , "NameDB checksum")
f.timeOldAdvCodeVer  = ProtoField.uint16("fgc_ether.time.oldAdvCodeVersion", "Old advertised code version")
f.timeCodeVarClassId = ProtoField.uint8 ("fgc_ether.time.codeVarClassId"   , "Code variable class")
f.timeCodeVarId      = ProtoField.uint8 ("fgc_ether.time.codeVarId"        , "Code variable ID")
f.timeCodeVarIdx     = ProtoField.uint16("fgc_ether.time.codeVarIdx"       , "Code variable block index")
f.timeAdvListIdx     = ProtoField.uint8 ("fgc_ether.time.advListIdx"       , "Advertised code list index")
f.timeAdvListLen     = ProtoField.uint8 ("fgc_ether.time.advListLen"       , "Advertised code list length")
f.timeAdvCodeClassId = ProtoField.uint8 ("fgc_ether.time.advCodeClassid"   , "Advertised code class ID")
f.timeAdvCodeId      = ProtoField.uint8 ("fgc_ether.time.advCodeId"        , "Advertised code ID")
f.timeAdvCodeCrc     = ProtoField.uint16("fgc_ether.time.advCodeCrc"       , "Advertised code CRC16")
f.timeAdvCodeLenBlk  = ProtoField.uint16("fgc_ether.time.advCodeLenBlk"    , "Advertised code length")
f.timeRunlogIdx      = ProtoField.uint16("fgc_ether.time.runlogIdx"        , "Run log index")
f.timeFlags          = ProtoField.uint8 ("fgc_ether.time.flags"            , "Flags")
f.timeReserved1      = ProtoField.uint8 ("fgc_ether.time.reserved1"        , "Reserved")
f.timeAdvCodeVer     = ProtoField.uint32("fgc_ether.time.advCodeVersion"   , "Advertised code version")

f.timeEvents         = ProtoField.bytes ("fgc_ether.time.events"           , "FGC event slots")
    f.timeEvent      = ProtoField.bytes ("fgc_ether.time.event"            , "FGC event slot")
        f.timeEventType    = ProtoField.uint8 ("fgc_ether.time.event.type"   , "Event type", base.DEC, tTimeEvents)
        f.timeEventPayload = ProtoField.uint8 ("fgc_ether.time.event.payload", "Event payload")
        f.timeEventDelayMs = ProtoField.uint16("fgc_ether.time.event.delayMs", "Event delay in milliseconds")

f.timeRtRefs         = ProtoField.bytes ("fgc_ether.time.rtRefs"           , "Real-time references")
    f.timeRtRef          = ProtoField.float ("fgc_ether.time.rtRef"            , "Real-time reference")

f.timeCode           = ProtoField.bytes ("fgc_ether.time.code"             , "Code blocks")


function timeDissector(buf, pinfo, tree)
    -- New sub tree
    t = tree:add(proto, buf(), "Time packet payload")

    -- Time items
    t:add(f.timeFgcId          , buf(0, 1)); 
    t:add(f.timeFgcChar        , buf(1, 1)); 
    t:add(f.timeMsTime         , buf(2, 2)); 
    t:add(f.timeUnixTime       , buf(4, 4)); 
    t:add(f.timeNamedbCrc      , buf(8, 2)); 
    t:add(f.timeOldAdvCodeVer  , buf(10, 2)); 
    t:add(f.timeCodeVarClassId , buf(12, 1)); 
    t:add(f.timeCodeVarId      , buf(13, 1)); 
    t:add(f.timeCodeVarIdx     , buf(14, 2)); 
    t:add(f.timeAdvListIdx     , buf(16, 1)); 
    t:add(f.timeAdvListLen     , buf(17, 1)); 
    t:add(f.timeAdvCodeClassId , buf(18, 1)); 
    t:add(f.timeAdvCodeId      , buf(19, 1)); 
    t:add(f.timeAdvCodeCrc     , buf(20, 2)); 
    t:add(f.timeAdvCodeLenBlk  , buf(22, 2)); 
    t:add(f.timeRunlogIdx      , buf(24, 2)); 
    t:add(f.timeFlags          , buf(26, 1)); 
    t:add(f.timeReserved1      , buf(27, 1)); 
    t:add(f.timeAdvCodeVer     , buf(28, 4)); 
    
    timeEvents = t:add(f.timeEvent, buf(32, 32)); 
        for i=0,7 do
            timeEvent = timeEvents:add(f.timeEvent, buf(32 + i*4, 4)); 
                timeEvent:add(f.timeEventType   , buf(32 + i*4, 1)); 
                timeEvent:add(f.timeEventPayload, buf(32 + i*4 + 1, 1)); 
                timeEvent:add(f.timeEventDelayMs, buf(32 + i*4 + 2, 2)); 
        end

    timeRefs = t:add(f.timeRtRefs, buf(64, 256)); 
        for i=0,63 do
            timeRefs:add(f.timeRtRef, buf(64 + i*4, 4)); 
        end

    t:add(f.timeCode, buf(320, 1024));       
end



-----------------------------------------------------------
-- Subdissector : Status packet
-----------------------------------------------------------

f.statusAck       = ProtoField.uint8 ("fgc_ether.status.ack"      , "Acknowledgement"       , base.HEX)
    f.statusAckStatus   = ProtoField.uint8("fgc_ether.status.ack.status"  , "Status"              , base.DEC, nil      , FGC_ACK_STATUS)
    f.statusAckTime     = ProtoField.uint8("fgc_ether.status.ack.time"    , "Time"                , base.DEC, tAckTime , FGC_ACK_TIME)
    f.statusAckRdPkt    = ProtoField.uint8("fgc_ether.status.ack.rdPkt"   , "RT data packet"      , base.DEC, tAckRdPkt, FGC_ACK_RD_PKT)
    f.statusAckCmdTog   = ProtoField.uint8("fgc_ether.status.ack.cmdTog"  , "Command toggle"      , base.DEC, nil      , FGC_ACK_CMD_TOG)
    f.statusAckPll      = ProtoField.uint8("fgc_ether.status.ack.pll"     , "PLL status"          , base.DEC, tAckPll  , FGC_ACK_PLL)
    f.statusAckPmReq    = ProtoField.uint8("fgc_ether.status.ack.pmReq"   , "Post-mortem"         , base.DEC, tAckPm   , FGC_SELF_PM_REQ)
    f.statusAckExtPmReq = ProtoField.uint8("fgc_ether.status.ack.extPmReq", "External post-mortem", base.DEC, tAckExtPm, FGC_EXT_PM_REQ)
f.statusClassId   = ProtoField.uint8 ("fgc_ether.status.classId"  , "Class ID")
f.statusCmdStat   = ProtoField.uint8 ("fgc_ether.status.cmdStat"  , "Command status"        , base.DEC, tErrors)
f.statusRunlog    = ProtoField.uint8 ("fgc_ether.status.runlog"   , "Runlog data")
f.statusDiag      = ProtoField.bytes ("fgc_ether.status.diag"     , "Diagnostic")
    f.statusDiagChan = ProtoField.uint8  ("fgc_ether.status.diag.chan", "Diagnostic channels")
    f.statusDiagData = ProtoField.uint16 ("fgc_ether.status.diag.data", "Diagnostic data")
f.statusClassData = ProtoField.bytes ("fgc_ether.status.classData", "Class specific data")
f.statusRTerm     = ProtoField.string("fgc_ether.status.rterm"    , "Remote terminal data")
f.statusReserved  = ProtoField.bytes ("fgc_ether.status.reserved" , "Reserved (padding)")


function statusDissector(buf, pinfo, tree)
    -- New sub tree
    t = tree:add(proto, buf(), "Status packet payload")
    
    -- Status items
    statusAck = t:add(f.statusAck, buf(0, 1))
        statusAck:add(f.statusAckStatus,   buf(0, 1))
        statusAck:add(f.statusAckTime,     buf(0, 1))
        statusAck:add(f.statusAckRdPkt,    buf(0, 1))
        statusAck:add(f.statusAckCmdTog,   buf(0, 1))
        statusAck:add(f.statusAckPll,      buf(0, 1))
        statusAck:add(f.statusAckPmReq,    buf(0, 1))
        statusAck:add(f.statusAckExtPmReq, buf(0, 1))
    t:add(f.statusClassId, buf(1, 1))
    t:add(f.statusCmdStat, buf(2, 1))
    t:add(f.statusRunlog,  buf(3, 1))

    -- Diagnostic
    statusDiag = t:add(f.statusDiag, buf(4, 12))
        for i=0,3 do statusDiag:add(f.statusDiagChan, buf(4 + i, 1))   end
        for i=0,3 do statusDiag:add(f.statusDiagData, buf(8 + i*2, 2)) end

    -- Class data
    t:add(f.statusClassData, buf(16, 40))
    
    -- Remote terminal data
    t:add(f.statusRTerm, buf(56, 21))

    -- Padding
    t:add(f.statusReserved, buf(77, 3))
end



-----------------------------------------------------------
-- Subdissector : Command packet
-----------------------------------------------------------

f.cmdLength = ProtoField.uint16("fgc_ether.cmd.length", "Length", base.DEC)
f.cmdFlags  = ProtoField.uint8 ("fgc_ether.cmd.flags" , "Flags" , base.HEX)
    f.cmdFlagsFirst = ProtoField.uint8("fgc_ether.cmd.flags.firstPacket", "First packet", base.BOOLEAN, nil      , FGC_FIELDBUS_FLAGS_FIRST_PKT)
    f.cmdFlagsLast  = ProtoField.uint8("fgc_ether.cmd.flags.firstPacket", "Last packet" , base.BOOLEAN, nil      , FGC_FIELDBUS_FLAGS_LAST_PKT)
    f.cmdFlagsType  = ProtoField.uint8("fgc_ether.cmd.flags.firstPacket", "Command type", base.DEC    , tCmdFlags, FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK)
f.cmdUser   = ProtoField.uint8 ("fgc_ether.cmd.user"  , "User"  , base.DEC)
f.cmdData   = ProtoField.string("fgc_ether.cmd.data"  , "Data")


function cmdDissector(buf, pinfo, tree)
    -- New sub tree
    t = tree:add(proto, buf(), "Command packet payload")

    -- Length
    t:add(f.cmdLength, buf(0, 2))
    
    -- Command flags
    cmdFlags = t:add(f.cmdFlags, buf(2, 1))
        cmdFlags:add(f.cmdFlagsType , buf(2, 1))
        cmdFlags:add(f.cmdFlagsFirst, buf(2, 1))
        cmdFlags:add(f.cmdFlagsLast , buf(2, 1))

    -- User
    t:add(f.cmdUser, buf(3, 1))

    -- Data
    t:add(f.cmdData, buf(4, buf(0, 2):uint() - 2))
end



-----------------------------------------------------------
-- Subdissector : Response packet
-----------------------------------------------------------

f.rspLength = ProtoField.uint16("fgc_ether.rsp.length"           , "Length"                    , base.DEC)
f.rspFlags  = ProtoField.uint8 ("fgc_ether.rsp.flags"            , "Flags"                     , base.HEX)
    f.rspFlagsSeqNum = ProtoField.uint8("fgc_ether.rsp.flags.seqNum"     , "Sequence number", base.DEC,     nil      , FGC_FIELDBUS_FLAGS_SEQ_MASK)
    f.rspFlagsFirst  = ProtoField.uint8("fgc_ether.rsp.flags.firstPacket", "First packet"   , base.BOOLEAN, nil      , FGC_FIELDBUS_FLAGS_FIRST_PKT)
    f.rspFlagsLast   = ProtoField.uint8("fgc_ether.rsp.flags.firstPacket", "Last packet"    , base.BOOLEAN, nil      , FGC_FIELDBUS_FLAGS_LAST_PKT)
    f.rspFlagsType   = ProtoField.uint8("fgc_ether.rsp.flags.firstPacket", "Command type"   , base.DEC    , tRspFlags, FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK)
f.rspNTerm  = ProtoField.uint8 ("fgc_ether.rsp.nTerm"            , "Remote terminal characters", base.DEC)
f.rspTime   = ProtoField.absolute_time("fgc_ether.rsp.timeStamp" , "Time stamp")
f.rspData   = ProtoField.string("fgc_ether.rsp.data"             , "Data")


function rspDissector(buf, pinfo, tree)
    -- New sub tree
    t = tree:add(proto, buf(), "Response packet payload")

    -- Length
    t:add(f.rspLength, buf(0, 2))
    
    -- Command flags
    rspFlags = t:add(f.rspFlags, buf(2, 1))
        rspFlags:add(f.rspFlagsSeqNum, buf(2, 1))
        rspFlags:add(f.rspFlagsFirst , buf(2, 1))
        rspFlags:add(f.rspFlagsLast  , buf(2, 1))
        rspFlags:add(f.rspFlagsType  , buf(2, 1))

    -- Number of terminal characters
    t:add(f.rspNTerm, buf(3, 1))

    -- Time stamp
    local offset = 0

    if bit.band(buf(2, 1):uint(), FGC_FIELDBUS_FLAGS_FIRST_PKT) == FGC_FIELDBUS_FLAGS_FIRST_PKT then
        -- Add time stamp only for the first packet
        t:add(f.rspTime, buf(4, 8));
        offset = 8
    end

    -- Data
    t:add(f.rspData, buf(4 + offset, buf(0, 2):uint() - 2 - offset))
end



-----------------------------------------------------------
-- Subdissector : Publication packet
-----------------------------------------------------------

f.pubLength   = ProtoField.uint16("fgc_ether.pub.length"  , "Length"                    , base.DEC)
f.pubFlags    = ProtoField.uint8 ("fgc_ether.pub.flags"   , "Flags"                     , base.HEX)
    f.pubFlagsSeqNum = ProtoField.uint8("fgc_ether.pub.flags.seq"        , "Sequence number", base.DEC,     nil      , FGC_FIELDBUS_FLAGS_SEQ_MASK)
    f.pubFlagsFirst  = ProtoField.uint8("fgc_ether.pub.flags.firstPacket", "First packet"   , base.BOOLEAN, nil      , FGC_FIELDBUS_FLAGS_FIRST_PKT)
    f.pubFlagsLast   = ProtoField.uint8("fgc_ether.pub.flags.firstPacket", "Last packet"    , base.BOOLEAN, nil      , FGC_FIELDBUS_FLAGS_LAST_PKT)
    f.pubFlagsType   = ProtoField.uint8("fgc_ether.pub.flags.firstPacket", "Command type"   , base.DEC    , tRspFlags, FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK)
f.pubNTerm    = ProtoField.uint8 ("fgc_ether.pub.nTerm"   , "Remote terminal characters", base.DEC)
f.pubSubId    = ProtoField.uint8 ("fgc_ether.pub.subId"   , "Subscription ID")
f.pubSubFlags = ProtoField.uint8 ("fgc_ether.pub.subFlags", "Subscription flags")
    f.pubSubFlagsNewSet = ProtoField.uint8("fgc_ether.pub.flags.newSet", "New / set", base.DEC, tPubNewSet, FGC_FIELDBUS_PUB_TRIG_MASK)
f.pubUser     = ProtoField.uint8 ("fgc_ether.pub.subFlags", "Subscription flags")
f.pubTime     = ProtoField.absolute_time("fgc_ether.pub.timeStamp" , "Time stamp")
f.pubData     = ProtoField.string("fgc_ether.pub.data"             , "Data")


function pubDissector(buf, pinfo, tree)
    -- New sub tree
    t = tree:add(proto, buf(), "Publication packet payload")

    -- Length
    t:add(f.pubLength, buf(0, 2))
    local offset = 0
    
    -- Command flags
    pubFlags = t:add(f.pubFlags, buf(2, 1))
        pubFlags:add(f.pubFlagsSeqNum, buf(2, 1))
        pubFlags:add(f.pubFlagsFirst , buf(2, 1))
        pubFlags:add(f.pubFlagsLast  , buf(2, 1))
        pubFlags:add(f.pubFlagsType  , buf(2, 1))
    offset = offset + 1

    -- Number of terminal characters
    t:add(f.pubNTerm, buf(3, 1))
    offset = offset + 1

    -- Subscription ID
    t:add(f.pubSubId, buf(4, 1))
    offset = offset + 1

    -- Subscription flags
    pubNewSet = t:add(f.pubSubFlags, buf(5, 1))
        pubNewSet:add(f.pubSubFlagsNewSet, buf(5, 1))
    offset = offset + 1

    -- User
    t:add(f.pubUser, buf(6, 1))
    offset = offset + 1

    -- Time stamp
    if bit.band(buf(2, 1):uint(), FGC_FIELDBUS_FLAGS_FIRST_PKT) == FGC_FIELDBUS_FLAGS_FIRST_PKT then
        -- Add time stamp only for the first packet
        t:add(f.pubTime, buf(4, 8));
        offset = offset + 8
    end

    -- Data
    t:add(f.pubData, buf(2 + offset, buf(0, 2):uint() - offset))
end



-----------------------------------------------------------
-- Subdissectors handling different payload types
-----------------------------------------------------------

local subDissectors = 
{
    [FGC_ETHER_PAYLOAD_TIME]   = timeDissector,
    [FGC_ETHER_PAYLOAD_STATUS] = statusDissector,
    [FGC_ETHER_PAYLOAD_CMD]    = cmdDissector,
    [FGC_ETHER_PAYLOAD_RSP]    = rspDissector,
    [FGC_ETHER_PAYLOAD_PUB]    = pubDissector
}

local dataDiss = Dissector.get("data")



-----------------------------------------------------------
-- Dissect the protocol header (first step - common for all payload types)
-----------------------------------------------------------

f.payload = ProtoField.int8("fgc_ether.payload", "Payload", base.DEC, tPayloadNames)
f.padding = ProtoField.int8("fgc_ether.padding", "Reserved (padding)")


function proto.dissector(buf, pinfo, tree)

    -- Protocol name
    pinfo.cols.protocol = "FGC_Ether Protocol"
    
    -- Main tree (root) name
    local t = tree:add(proto, buf(), "FGC_Ether Protocol Data")
    
    -- First byte - Payload type
    t:add(f.payload, buf(0, 1))

    -- Second byte - Reserved(padding)
    t:add(f.padding, buf(1, 1))

    
    -- Select proper subdissector depending of the payload type
    local dissector = subDissectors[buf(0, 1):uint()]

    -- Invoke selected subdissector
    if dissector ~= nil then
        -- Dissector was found - invoke subdissector with a new Tvb (skipped first 2 bytes)
        dissector(buf(2):tvb(), pkt, t)
    else
        -- Unknown payload - invoke fallback dissector that just shows the raw data
        data_dis:call(buf(2):tvb(), pkt, tree)
    end

end



-----------------------------------------------------------
-- Load the table for MAC frame (EtherType field)
udp_table = DissectorTable.get("ethertype")

-- Register the protocol to handle EtherType of 0x88B5
udp_table:add(FGC_ETHER_TYPE, proto)
