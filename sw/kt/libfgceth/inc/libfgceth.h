/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved
 * @file   libfgceth.h
 * @brief  FGC Ether Library main file.
 * @author Dariusz Zielinski
 * @author Joao Afonso
 */

#ifndef FGCETH_MAIN_H
#define FGCETH_MAIN_H

#include <stdio.h>

#include <libfgceth/private/fgcethTypes.h>
#include <libfgceth/public/fgcethFParser.h>



/************************************************************************/
/******   Configuration   ***********************************************/

/**
 * Activate post-mortem flag in timing packets.
 * This will tell the FGCs to freeze the post-mortem circular buffers after a fault.
 * 
 * @param       enable_pm set true/false to enable/disable freezing of post-mortem buffers
 */
void fgcethEnablePmFlag(bool enable_pm);

/**
 * Activate the fgc logger flag in timing packets.
 * This will tell the FGCs to freeze the post-mortem circular buffers after a fault.
 *
 * @param       enable_fgc_logger set true/false to enable/disable freezing of post-mortem buffers
 */
void fgcethEnableFGCLogger(bool enable_fgc_logger);



/**
 * Sets timeout used in synchronous functions.
 *
 * @param milliseconds      number of milliseconds after which the synchronous function is aborted and an error is returned
 *
 * @return                  FGC_SUCCESS if succeeded, error code otherwise
 */
void       fgcethSetTimeout(uint32_t milliseconds);



/************************************************************************/
/******   Initialization   **********************************************/

/**
 * Initializes the library.
 *
 * @param eth_name              a system name of a network interface card connected to the FGC3 (or a switch)
 * @param names_container       a pointer to structure containing the device name information
 * @param codes_container       a pointer to structure containing FGC3 code data, can be NULL
 * @param response_pool_size    size of the response pool (for more details see fgcethCreateResponse)
 * @param pub_buffer_cycles     cycles of 20ms that the status publication is able to store before overflowing
 *
 * @return                      FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno       fgcethInit(const char* eth_name, fgceth_names_container * names_container, fgceth_codes_container * codes_container, const uint32_t alloc_mem_size, const uint32_t pub_buffer_cycles);



/************************************************************************/
/******   Code re-configuration   ***************************************/

/**
 * Configures the FGC Ether library to advertize and broadcast a new set of codes to the FGCs.
 * The previous codes will be replaced from the FGC Ether library.
 * 
 * @param  codes_container      a pointer to structure containing new FGC3 code data
 * 
 * @return                      FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno 	   fgcethUpdateCodes(fgceth_codes_container * codes_container);



/************************************************************************/
/******   Device check   ************************************************/

/**
 * Returns the device ID, given its name.
 *
 * @param  dev_name     name of the device
 *
 * @return              device ID if device exists, FGCETH_NO_DEV otherwise
 */
uint8_t            fgcethGetDevId(const char * dev_name);



/**
 * Checks if the device is ready to accept commands.
 *
 * @param  dev_id       ID of the device
 *
 * @return              true if the device is ready, false otherwise
 */
bool               fgcethIsDeviceReady(const uint8_t dev_id);



/**
 * Check if ID matches a device from the name file.
 * 
 * @param  dev_id       ID of the device
 * 
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethCheckDeviceId(const uint8_t dev_id);



/************************************************************************/
/******   Statistics and errors   ***************************************/

/**
 * Returns global statistics.
 *
 * @param  stats        a pointer to a structure that will hold returned statistics
 *
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno       fgcethGetGlobalStats(fgceth_global_stats * stats);



/**
 * Returns device-specific statistics.
 *
 * @param  stats        a pointer to a structure that will hold returned statistics
 * @param  dev_id       ID of the device
 *
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno       fgcethGetDeviceStats(fgceth_device_stats* stats, const uint8_t dev_id);



/**
 * Returns an error message that corresponds to the error code.
 *
 * @param  error_code   error code for which an error message will be returned
 *
 * @return              Character string containing error message corresponding to an error code or an empty string in case of invalid error code
 */
const char*        fgcethStrerr(const fgceth_errno error_code);



/************************************************************************/
/******   Synchronous get and set   *************************************/

/**
 * Sends a command to an FGC, to get/set a property.
 * Blocks until the response is ready to be read.
 * 
 * @param[in/out]  cmd  fgceth_rsp structure whose elements define the get/set command. After the function returns, it will also contain the response and/or error message.
 * 
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethGetSet(fgceth_rsp * cmd);



/************************************************************************/
/******   Asynchronous get and set   ************************************/

/**
 * Sends a command to an FGC, to get/set a property.
 * Is non-blocking.
 * 
 * @param[in]  cmd      fgceth_rsp structure whose elements define the get/set command. After the function returns, it will also contain the response and/or error message.
 * @param[in]  queue    pointer to queue instance that will be used to retrieve the cmd structure after the result is ready to be read.
 * 
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethGetSetAsync(fgceth_rsp * cmd, fgceth_rsp_queue * queue);



/************************************************************************/
/******   Response memory management   **********************************/

/**
 * Get a free command/response structure from the FGC Ether lib memory pool.
 * The structure is initialized according to the CMD_RSP format, and should not be used as RTERM or PUB.
 * 
 * @return          pointer to the free command structure
 */
fgceth_rsp * fgcethCreateResponse();



/**
 * Returns any response structure to the FGC Ether memory pool.
 * Sould be used for all fgceth_rsp structures (i.e. types CMD_RSP, RTERM, PUB).
 *  
 * @param   ptr     pointer to the structure that will be destroyed
 * 
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethDestroyResponse(void * ptr);



/************************************************************************/
/******   Command configuration   ***************************************/

/**
 * Resets a command/response structure with empty values.
 * Should only be used for structures with CMD_RSP format.
 * 
 * @param cmd       pointer to command/response structure
 */
void fgcethResetCmd(fgceth_rsp * cmd);



/**
 * Configures a command/response structure with a new command.
 * New parameters will replace previous ones.
 * Should only be used for structures with CMD_RSP format.
 * 
 * @param cmd       pointer to command/response structure
 * @param tag       command tag (or NULL)
 * @param cmd_type  commant type (GET or SET)
 * @param dev_name  target device name
 * @param property  command property
 * @param val_opts  get options or set value (or NULL)
 */
void fgcethConfigCmd(fgceth_rsp * cmd, const char * tag, enum fgceth_cmd_type cmd_type, const char * dev_name, const char * property, const char * val_opts);



/**
 * Associates a command/response structure with a client name.
 * Should only be used for structures with CMD_RSP format.
 * 
 * @param cmd           pointer to command/response structure
 * @param client_name   name of the client
 */
void fgcethConfigCmdClient(fgceth_rsp * cmd, const char * client_name);



/**
 * Associates a command/response structure with a pointer to any structure.
 * 
 * @param cmd           pointer to command/response structure
 * @param user_data     pointer to used data
 */
void fgcethConfigCmdUserData(fgceth_rsp * cmd, void * user_data);



/************************************************************************/
/******   Subscription   ************************************************/

/**
 * Subscribes to receive publication data from the FGCs.
 * 
 * @param  dev_id   ID of the device
 * @param  queue    pointer to queue instance that will be used to receive publication data.
 * @param  handle   handle a pointer to a handle object used to unsubscribe
 * 
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethSubscribe(uint8_t dev_id, fgceth_rsp_queue * queue, fgceth_sub_handle* handle);



/**
 * Unsubscribes from a publication.
 *
 * @param handle    same pointer used to identify the subscription.
 */
fgceth_errno fgcethUnsubscribe(fgceth_sub_handle* handle);



/************************************************************************/
/******   Response queue manipulation   *********************************/

/**
 * Creates a new queue object.
 *
 * @param buffer_length     maximum number of elements (response objects) the queue can store
 *
 * @return                  If successful the function returns a pointer to a queue structure,
 *                          NULL pointer otherwise
 */
fgceth_rsp_queue * fgcethQueueCreate(const uint32_t buffer_length);



/**
 * Destroys the queue object.
 *
 * @param queue     pointer to the queue object that is to be destroyed
 *
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethQueueDestroy(fgceth_rsp_queue * queue);



/**
 * Extracts an element from the queue in a non-blocking manner.
 *
 * @param queue     pointer to the queue object
 *
 * @return          pointer to a response object or a NULL pointer if the queue is empty
 */
fgceth_rsp * fgcethQueuePop(fgceth_rsp_queue * queue);



/**
 * Extracts an element from the queue in a blocking manner.
 *
 * @param queue     pointer to the queue object
 *
 * @return          pointer to a response object
 */
fgceth_rsp * fgcethQueueBlockPop(fgceth_rsp_queue * queue);



/**
 * Adds an element to the queue in a non-blocking manner.
 *
 * @param queue     pointer to the queue object
 * @param rsp       pointer to the response object
 *
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethQueuePush(fgceth_rsp_queue * queue, fgceth_rsp * rsp);

/**
 * Adds an element to the queue in a blocking manner.
 *
 * @param queue     pointer to the queue object
 * @param rsp       pointer to the response object
 *
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethQueueBlockPush(fgceth_rsp_queue * queue, fgceth_rsp * rsp);



/**
 * Controls blocking behaviour for 'fgcethQueueBlockPop' function.
 * Can be used to abort a blocking pop call by a different thread.
 *
 * @param queue     pointer to the queue object to be set as blocking/unblocking.
 *
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethQueueIsBlocking(fgceth_rsp_queue * queue, bool is_blocking);



/************************************************************************/
/******   Safe library termination and deallocation of resources ********/

/**
 * Close FGC Ether library safelly and free all resources.
 * Should be last library function to be called.
 * 
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethClose();



/************************************************************************/
/******   Logging messages  *** *****************************************/

/**
 * Print an error message, through the FGC Ether library logging system.
 * Should be reserved for errors that are fatal.
 *
 * @param dev_id    device_id, special cases: 0 for gateway, FGCETH_NO_DEV if unrelated to any device (or gateway).
 * @param format    format of string to print.
 * @param ...       arguments
 */
void fgcethErrorLog(uint8_t dev_id, const char * format, ...) __attribute__ ((format (printf, 2, 3)));



/**
 * Print a warning message, through the FGC Ether library logging system.
 * Should be reserved for situations that are non fatal for the program execution.
 *
 * @param dev_id    device_id, special cases: 0 for gateway, FGCETH_NO_DEV if unrelated to any device (or gateway).
 * @param format    format of string to print.
 * @param ...       arguments
 */
void fgcethWarningLog(uint8_t dev_id, const char * format, ...) __attribute__ ((format (printf, 2, 3)));



/**
 * Print a generic message, through the FGC Ether library logging system.
 * Allows the user to set the logging level of the message.
 *
 * @param dev_id    device_id, special cases: 0 for gateway, FGCETH_NO_DEV if unrelated to any device (or gateway).
 * @param flags     logging level of message.
 * @param format    format of string to print.
 * @param ...       arguments
 */
void fgcethLog(uint8_t dev_id, uint32_t flags, const char * format, ...) __attribute__ ((format (printf, 3, 4)));



/**
 * Allows the developer to select the maximum logging level of the messages that will be printed.
 * Errors and warnings are always printed.
 *
 * @param flags     level of messages the developer wants to see logged.
 */
void fgcethSetLogPrintFlags(uint32_t flags);



/**
 * Allows the developer to redirect the execution logging messages to a file.
 * Messages produced during the library initialization/termination are managed separately (see fgcethSetLogFileInit).
 * By default, these messages are printed to stderr.
 *
 * @param f         file
 */
void fgcethSetLogFileExec(FILE * f);



/**
 * Allows the developer to redirect the initialization/termination logging messages to a file.
 * Messages produced during the library normal execution are managed separately (see fgcethSetLogFileExec).
 * By default, these messages are printed to stderr.
 *
 * @param f         file
 */
void fgcethSetLogFileInit(FILE * f);



/**
 * Enable extra information on the logging messages.
 * Useful for debugging.
 *
 * Extra info: Counter of the 20 ms protocol cycles, flags associated with the message
 *
 * @param b set as true to enable additional info, set as false to disable.
 */
void fgcethDetailedLog(bool b);



/************************************************************************/
/******   Remote terminal functions   ***********************************/

/**
 * Subscribe for remote terminal messages returned by a certain device.
 *
 * @param dev_id    the ID of subscribed device.
 * @param queue     to which the remote terminal data will be pushed.
 * @param handle    pointer used to identify a certain subscription.
 *
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethRtermSubscribeDev(uint8_t dev_id, fgceth_rsp_queue* queue, fgceth_sub_handle* handle);



/**
 * Close a remote terminal subscription.
 *
 * @param handle    same pointer used to identify the subscription.
 *
 * @return          FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethRtermUnsubscribeDev(fgceth_sub_handle* handle);



/**
 * Send remote terminal message to a device.
 *
 * @param dev_id        the ID of the device.
 * @param buf buffer    containing characters to be sent.
 * @param buf_len       number of characters to send.
 *
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethRtermSendToDev(uint8_t dev_id, char * buf, uint32_t buf_len);



/**
 * Directly write a remote terminal string to a queue.
 * It will behave as if it was the library returning the message.
 *
 * @param queue         to which the remote terminal data will be pushed.
 * @param buf buffer    containing characters to be sent.
 * @param buf_len       number of characters to send.
 *
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethRtermSendToQueue(fgceth_rsp_queue* queue, char * buf, uint32_t buf_len);



/**
 * Returns a pointer to the array containing the last status packets received from all FGCs during the last cycle.
 * If all cycles have been read, it will block until the next cycle is finished.
 *
 * NOTE: The returned buffer will be ovewriten when a certain number of status publiation cycles. Therefore, the time window to use this data is limited.
 *
 * @param status        Pointer to pointer to array that will contain the status info, in network endian format.
 * @param time          Returns timestamp of next iteration.
 * @param last_pub_id   ID of last iteration, or zero. Returns ID of next iteration. Allows multiple client threads to call this function simultaneously.
 *
 * @return              FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno fgcethGetNextStatusPub(struct fgceth_fgc_stat ** status, struct timeval * time, uint32_t * last_pub_id);



/**
 * Obtain time reference, synchronized to library internal clock.
 *
 * @param time timeval structure
 */
void fgcethGetCurrentTime(struct timeval * time);



/**
 * Obtain time reference, synchronized to library internal clock, in milliseconds.
 *
 * @return         time reference in milliseconds
 */
uint32_t fgcethGetCurrentTimeMs();

#endif

//EOF
