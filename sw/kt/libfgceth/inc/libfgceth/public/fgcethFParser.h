/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethFParser.h
 * @brief  Parsing of external files.
 * @author Joao Afonso
 */

#ifndef FGCETH_FPARSER_H
#define FGCETH_FPARSER_H

#include <stdint.h>
#include <libfgceth/private/fgcethConsts.h>

// Maximum number of device names and codes allowed.
// These values are verified verified at compile time.

#define FGCETH_MAX_DEV_CODES (32 - 1) // Internally, one slot is reserved to broadcast the device names

// A structure used to send the FGC Ether lib the device names.
// Can be filled by calling fgcethNamesParser, or by the developer.

struct fgceth_names_container
{
	struct
	{
		uint8_t   dev_class;                             // Device class.
		uint16_t  dev_omode_mask;                        // Device operational mode mask.
		char *    dev_name;                              // Device name. Use NULL when device does not exist.
	} name_data[FGCETH_ALL_DEV_MAX];                     // Each device information should be stored in the corresponding index.

};

// A structure used to send the FGC Ether lib the device codes.
// Can be filled by calling fgcethCodesParser, or by the developer.

struct fgceth_codes_container
{
	struct
	{
		uint8_t * code;                                  // Code to be sent to FGCs. Should include last 30 Bytes of metadata. Use NULL when device does not exist.
		char      code_name[32];                         // Code file name.
		uint32_t  code_length;                           // Code length, in bytes. Should include last 30 Bytes of metadata.
	} code_data[FGCETH_MAX_DEV_CODES];
};

/************************************************************************/
/******   Parsing of name and code files   ******************************/

/**
 * Given a name file, obtains the list of devices that are managed by the gateway.
 * container is reset every time this function is called.
 *
 * @param path_to_file path to name file
 * @param host_name gateway name
 * @param container pointer to fgceth_names_container structure that will contain all the parsed device names
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */

fgceth_errno fgcethNamesParser(const char * path_to_file, const char * host_name, struct fgceth_names_container * container);

/**
 * Given the path to a folder containing all codes (or to a single code file), obtains the list of codes that are to be broadcasted to the devices..
 * Is able to parse all codes in a folder, or a single one.
 * Different cals to this function accumulate the results in container, without affecting the already parsed ones.
 *
 * @param path path to folder containing codes (or to a code file)
 * @param container pointer to fgceth_codes_container structure that will contain all the parsed codes
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */

fgceth_errno fgcethCodesParser(const char * path, struct fgceth_codes_container * container);

fgceth_errno fgcethFreeContainers(struct fgceth_names_container * names_container, struct fgceth_codes_container * codes_container);




#endif
//EOF
