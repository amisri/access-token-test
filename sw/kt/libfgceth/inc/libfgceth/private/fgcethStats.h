/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethStats.h
 * @brief  Statistics.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_STATS_H
#define FGCETH_STATS_H

#include <stdint.h>


/**************************************
 * Types
***************************************/

/// A structure containing global statistics

struct fgceth_global_stats
{
    struct
    {
        struct
        {
            uint32_t    all;                        ///< Number of frames received
            struct
            {
                uint32_t    all;                    ///< Total number of bad frames received

                uint32_t    address;                ///< Number of frames received with an invalid source address
                uint32_t    size;                   ///< Number of frames received with an incorrect size
                uint32_t    time;                   ///< Number of frames received at an invalid time within the cycle
            } bad;
        } rx;

        struct
        {
            uint32_t    all;                        ///< Number of frames sent
            uint32_t    failed;                     ///< Number of frames that failed sending
        } tx;
    } eth;

    uint32_t load_received;                         ///< Data load received in bytes
    uint32_t load_sent;                             ///< Data load sent in bytes
    uint32_t timer_window;                          ///< Too high time difference between two timer callbacks
    uint32_t losing_frames;                         ///< Number of Ethernet frames lost while receiving

};


/// A structure containing device-specific statistics

struct fgceth_device_stats
{
    uint32_t ack_miss;                               ///< Number of message acknowledgements missed
    uint32_t offline_transition;                     ///< Number of times device has gone offline
    uint32_t proto_error;                            ///< Number of protocol errors
    uint32_t rd_miss;                                ///< Number of real-time messages missed
    uint32_t seq_status_miss;                        ///< Number of sequential status messages missed
    uint32_t time_missed;                            ///< Number of times new time variable was not received
    uint32_t seq_toggle_bit_miss;                    ///< Number of sequential toggle bits missed
    uint32_t status_rec;                             ///< Number of status messages received
    uint32_t status_miss;                            ///< Number of status messages missed
    uint32_t unlock_transition;                      ///< Number of times device has unlocked its PLL
    uint32_t status_out_of_window;                   ///< Number of status messages received outside of the status reception window
    uint32_t rsp_not_expected;                       ///< Number of times the response was not expected
    uint32_t rsp_wrong_seq_num;                      ///< Number of times the sequence number was incorrect
    uint32_t rsp_wrong_first;                        ///< Number of times first packet had no FGC_FIELDBUS_FLAGS_FIRST_PKT flag set
};



#endif
//EOF
