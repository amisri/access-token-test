/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethSm.h
 * @brief  Main command state machine.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_FGCSM_H
#define FGCETH_FGCSM_H


/**************************************
 * Types
***************************************/

/// Command state machines states

enum fgcsm_states
{
    CMD_OFFLINE,       ///< No status packets are being received from the FGC (set by the 20 ms timer thread)
    CMD_IN_BOOT,       ///< Status packets are being received and PLL is locked but class ID is a base class
    CMD_IDLE,          ///< Status packets are being received and PLL is locked. Waiting for a command to send
    CMD_QUEUED,        ///< Transitional state
    CMD_WAIT_ACK,      ///< Waiting for the acknowledgement of command reception from the FGC and sending next packets
    CMD_WAIT_EXE,      ///< Waiting for the command to execute
    CMD_TIMEOUT        ///< Error state because of the command timeout
};

// Prototype of the device type

struct fgc_ether_device;



/**************************************
 * Functions
***************************************/

/**
 * Update command state machine: execute state function and checks for transitions.
 *
 * Thread: Ms timer
 *
 * @param device a structure containing device data
 */
void fgcsm_smUpdate(fgc_ether_device* device);


#endif
//EOF
