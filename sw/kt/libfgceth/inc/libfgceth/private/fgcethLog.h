/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethLog.h
 * @brief  Utilities for the logging of messages by the library.
 * @author Joao Afonso
 */

#ifndef FGCETH_LOG_H
#define FGCETH_LOG_H

#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include <libfgceth/private/fgcethConsts.h>

#include <stdint.h>

///****************************************************

extern int dev_status_number; /// Holds status number - define it somewhere to use devPrint

void fgcethSetMsgLogFlags   (uint32_t new_flag);
void fgcethSetMsgLogFile    (FILE * f);
void fgcethSetMsgLogFileInit(FILE * f);
void fgcethDetailedMsgLog   (bool b);

void fgcethInitErrorMsgLog  (uint8_t dev_id,                 const char * format, ...) __attribute__ ((format (printf, 2, 3)));
void fgcethInitWarningMsgLog(uint8_t dev_id,                 const char * format, ...) __attribute__ ((format (printf, 2, 3)));
void fgcethInitMsgLog       (uint8_t dev_id, uint32_t flags, const char * format, ...) __attribute__ ((format (printf, 3, 4)));
void fgcethErrorMsgLog      (uint8_t dev_id,                 const char * format, ...) __attribute__ ((format (printf, 2, 3)));
void fgcethWarningMsgLog    (uint8_t dev_id,                 const char * format, ...) __attribute__ ((format (printf, 2, 3)));
void fgcethMsgLog           (uint8_t dev_id, uint32_t flags, const char * format, ...) __attribute__ ((format (printf, 3, 4)));

void fgcethErrorMsgLog_vaArgs  (uint8_t dev_id, const char * format, va_list args);
void fgcethWarningMsgLog_vaArgs(uint8_t dev_id, const char * format, va_list args);
void fgcethMsgLog_vaArgs       (uint8_t dev_id, uint32_t flags, const char * format, va_list args);

void fgcethAssertFunc       (const fgceth_errno error, const char* file, const int line);
void fgcethAssertFuncExp    (int expression, const char * exp_string, const char * file, const char * func, const int line);

#define fgcethAssert(error) fgcethAssertFunc(error, __FILE__, __LINE__)
#define fgcethAssertExp(expression) fgcethAssertFuncExp(expression, #expression, __FILE__, __FUNCTION__, __LINE__)

#endif
//EOF
