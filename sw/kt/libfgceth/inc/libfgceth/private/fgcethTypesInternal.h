/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethTypesInternal.h
 * @brief  Private types and variables declarations.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_TYPES_INTERNAL_H
#define FGCETH_TYPES_INTERNAL_H

#include <libfgceth/private/fgcethTypes.h>
#include <libfgceth/private/fgcethQueue.h>
#include <libfgceth/private/fgcethSm.h>
#include <libfgceth/private/fgcethRspSm.h>
#include <libfgceth/private/fgcethTimer.h>
#include <libfgceth/private/fgcethEther.h>
#include <libfgceth/private/fgcethHash.h>
#include <libfgceth/private/fgcethCharq.h>
#include <libfgceth/private/fgcethResManager.h>

#include <cclibs.h>
#include <libblkbuf.h>

#include <stdio.h>


/**************************************
 * Types
***************************************/

struct fgceth_rsp_man
{
    blk_buf_mgr_t     * blk_buf_mgr;
    // fgceth_rsp_queue    rsp_pool;
    // fgceth_rsp        * rsp_allocated;
    // int32_t             rsp_allocated_size;
};

struct fgc_q_link
{
	//fgceth_queue * q; // TODO : Remove !
	fgceth_rsp_queue  * rsp_q;
	fgceth_sub_handle * handle;
	struct fgc_q_link * next;
};

typedef fgc_ether_queue_T<struct fgc_q_link *> q_link_queue;
typedef fgc_ether_queue_T<struct fgceth_rsp *> fgceth_cmd_queue;

/// A structure used to store device's state and properties.

struct fgc_ether_device
{
	uint32_t                   id;                         ///< Device ID
	char                       name[FGC_MAX_DEV_LEN]; ///< Device name

	fgceth_device_stats        stats;               ///< Device-specific statistics

    // fgc_stat_var               last_status;         ///< Latest received status packet

    uint8_t                    ack;                 ///< Latest received status acknowledgment
    uint8_t                    last_ack;            ///< Previously received status acknowledgment
    uint8_t                    status_state;        ///< Latest command state received in a status packet
    uint8_t                    class_id;            ///< Latest class ID received in a status packet
    uint8_t                    pll;                 ///< Latest received PLL status (1 - locked, 0 - unlocked)
    uint8_t                    last_rsp_seq;        ///< Previously received response sequence number
    bool                       rsp_expected;        ///< Is response expected?
    bool                       cmd_finished;        ///< Is current command processed?

    volatile uint32_t          status_missed;       ///< Flag set to 0 when the status is received in a current cycle
    uint32_t                   prev_status_missed;  ///< Check previous number of status miss.
    uint8_t                    ack_toggle_missed;   ///< Number of times acknowledgment byte toggle was missed

    bool                       declared;            ///< Is device initialized? (true after invoking fgcethDeclareDevice)
    uint8_t                    fgc_class;           ///< Class ID read from name file

    struct fgceth_rsp        * current_command;     ///< Pointer to the currently processed command (back of the cmd_queue)
    fgceth_cmd_queue           cmd_queue;           ///< Queue holding commands to be sent and corresponding responses
    enum fgcsm_states          cmd_state;           ///< State of the command state machine
    enum rspsm_states          rsp_state;           ///< State of the response state machine

    // Remote terminal data

    fgc_ether_charq            rterm_char_q;        ///< Remote terminal input chars
    struct fgc_q_link*         rterm_rsp_q_list;    ///< Linked list of queues waiting for rterm responses

};


/// A structure to internally store the FGC codes and/or names and their properties.

struct fgc_ether_codes
{
	uint8_t             code[FGC_CODE_SIZE];    //!< Code data
	char                code_name[32];          //!< Names of files from which codes were loaded
	uint32_t            code_length;            //!< Code lengths
	uint8_t             code_class;             //!< Code classes
	uint8_t             code_id;                //!< Code IDs
	uint16_t            code_chksum;            //!< Code checksums
	uint32_t            code_version;           //!< Code versions
	uint16_t            code_old_version;       //!< Code versions using the old version system
};


/// A structure with global variables

struct fgc_ether_global_struct
{
    static bool            	   lib_initialized;                   ///< Is the library initialized

    fgceth_global_stats        global_stats;                      ///< Instance of the global statistics structure
    //fgceth_queue               cmd_pool;                        ///< Response pool
    fgc_ether_device           devices[FGC_ETHER_MAX_FGCS + 1];   ///< Data of all devices

    struct hash_table        * name_hash;                         ///!< Hash of devices by name
    struct hash_entry          name_entry[FGC_ETHER_MAX_FGCS + 1];///!< Name hash entries

    // FGC code transmission

    fgc_ether_codes            codes[FGC_CODE_NUM_SLOTS];         ///< Codes to be sent to devices
    bool                       send_code_flag;                    //!< Flag to enable or disable sending of FGC codes
    uint32_t                   code_tx_force;                     //!< Force transmission of numbered code
    uint32_t                   code_tx_num;                       //!< Number of code that is currently being transmitted
    bool                       code_tx_valid_flag;                //!< Flag to indicate that currently transmitted code is valid


    static uint32_t            timeout;                           ///< Timeout (in microseconds)

    struct fgc_ether_timer     tim;                               ///< Used by the timer
    struct fgc_ether_eth       eth;                               ///< Used for the ethernet connection

    bool                       closing_protocol;                  ///< To signal the closure of the library
    bool                       reception_thr_finished;            ///< Packet reception thread has finished its execution
    bool                       timer_finished;                    ///< Timer has terminated its execution
    int                        reception_thr_pipe[2];             ///< Fd to add to the poll function (to unblock the thread from it)

    pthread_mutex_t            resource_ctrl_mutex;               ///< Allow threads to access these resou5rces safely
    pthread_cond_t             resource_ctrl_cond;                ///< Signal thread termination

    pthread_t                  timer_thread;                      ///< Pthread to create and start POSIX timer.
    pthread_t                  reception_thread;                  ///< Pthread to handle reception thread


    static FILE              * log_file;
    static FILE              * log_init_file;
    static bool                log_long;

    // Remote terminal data

    //fgceth_queue               rterm_pool;                      ///< Rterm pool
    q_link_queue               q_link_pool;                       ///< For linkin queues waiting for rterm responses
    pthread_mutex_t            rterm_mutex;                       ///< Allow threads to access these resou5rces safely

    pthread_mutex_t            timer_mutex;                       ///< For correct timing packet handling

    static bool                pm_flag_enabled;
    static bool                fgc_logger_enabled;

    // NEW CHANGES

    struct fgceth_rsp_man      rsp_man;
    struct fgceth_rsp_man    * rterm_rsp_man;
    float                      max_mem_used;    


    pthread_mutex_t            pub_mutex;                          ///< For correct timing packet handling
    pthread_cond_t             pub_cond;
    struct fgceth_fgc_stat   * pub_status_buf;                     ///< Latest received status packet
    uint32_t                   pub_status_buf_size;
    uint32_t                   pub_status_write_idx;
    struct timeval           * pub_time;
    uint32_t                 * pub_id;

};



/**************************************
 * Variables
***************************************/

/// A declaration of the externally-defined structure with global variables

extern fgc_ether_global_struct fgceth_vars;

#endif
//EOF
