/*!
 * © Copyright CERN 2018. All rights not expressly granted are reserved.
 * @file   fgcethStatPub.h
 * @brief  FGC_Ether status publication handling.
 * @author Joao Afonso
 */

#ifndef FGCETH_STAT_PUB_H
#define FGCETH_STAT_PUB_H

#include <libfgceth/private/fgcethTypesInternal.h>

fgceth_errno statPubInit(uint32_t buffer_cycles_num);
void statPubClose();
void statPubAddNewStat(uint32_t dev_id, union fgc_stat_var * status);
void statPubFinishStatFrame(struct timeval time);
fgceth_errno statPubGetData_nl(struct fgceth_fgc_stat ** status, struct timeval * time, uint32_t * last_pub_id);

#endif

// EOF