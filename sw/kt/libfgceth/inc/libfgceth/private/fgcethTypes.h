/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethTypes.h
 * @brief  Public types declaration.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_TYPES_H
#define FGCETH_TYPES_H

#include <libfgceth/private/fgcethStats.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libblkbuf.h>

#include <limits.h>

/**************************************
 * Types
***************************************/

#define FGCETH_DEVICE_IN_DB                0x01    // Device is defined in the database
#define FGCETH_DATA_VALID                  0x02    // Data is fresh
#define FGCETH_CLASS_VALID                 0x04    // Device class agrees with database

#define FGCETH_ACK_PLL                     0x20    // PLL is locked
#define FGCETH_SELF_PM_REQ                 0x40    // Self-triggered post-mortem (GW+FGC data) requested
#define FGCETH_EXT_PM_REQ                  0x80    // External post-mortem (GW data only) requested


/// Type of acquisition time

#define FGCETH_MAX_RTERM_LEN 32 // Enough to accommodate the 21 chars defined in the protocol
#define FGCETH_MAX_CMD_LEN FGC_MAX_CMD_LEN
#define FGCETH_MAX_VAL_LEN FGC_MAX_VAL_LEN

struct fgceth_acq_time
{
    uint32_t    tv_sec;             ///< Number of seconds
    uint32_t    tv_usec;            ///< Number of microseconds
};


/// Type of the packet

enum fgceth_rsp_type
{
    CMD_RSP,
    RTERM,
    PUB
};

/// Type of the command

enum fgceth_cmd_type
{
    GET,
    SET,
    SET_BIN
};

// Subscription handle

typedef uint32_t fgceth_sub_handle;

/// Prototype of fgc_ether_queue_T

template<typename T> struct fgc_ether_queue_T;

/// Type of response pool and command/response queue

typedef fgc_ether_queue_T<struct fgceth_rsp *> fgceth_rsp_queue;

/****************************************************************************
 *
 *  ! FOR INTERNAL USE - DO NOT ACCESS/CHANGE !
 *
 ****************************************************************************/

enum cmd_part_
{
    START,
    TAG,
    TYPE,
    DEVICE,
    PROPERTY,
    VALUES,
    GET_OPTS,
    END,
    UNKNOWN
};

struct cmd_rsp_hidden_
{
    fgceth_rsp_queue          * response_queue;
    uint32_t                    timeout_timestamp;

    int32_t                     cmd_remaining;
    int32_t                     cmd_length;
    enum cmd_part_              cmd_step;

    uint32_t                    dev_id;
};


/****************************************************************************
 *
 *  FOR EXTERNAL USE
 *
 ****************************************************************************/

/// Fill this structure with the command to be sent to a FGC
/// Will be returned after the response (or error) is received

struct fgceth_cmd_rsp
{
    // Public

    enum fgceth_cmd_type        cmd_type;                       // Mandatory   : GET/SET
    char                        tag[FGC_MAX_TAG_LEN + 1];       // (Optional)  : Tag will be returned unchanged
    char                        dev_name[FGC_MAX_DEV_LEN + 1];  // Mandatory   : Device name (or ID, in string format)
    char                        property[FGC_MAX_PROP_LEN + 1]; // Mandatory   : Property name
    blk_buf_t *                 value_opts;                     // (Optional*) : Property value (SET commands) or, Get options (GET commands)
                                                                //          *  : 'value_opts' may be required or not, depending on get/set command
    char                        client_name[HOST_NAME_MAX + 1]; // (Optional)  : Can be used to track client name
    void *                      user_data;                      // (Optional)  : Can be used to track any other user data, that will be returned untouched
                                                                //
    blk_buf_t *                 response;                       // In successful commands, will contain the response returned by the FGC
    fgceth_errno                error;                          // Will contain error (or success) code
    fgceth_acq_time             time;                           // Response time, returned by FGC

    // Private - Internal use only

    struct cmd_rsp_hidden_      hidden_members_;
};

// Will be returned with remote terminal data (if subscribed)

struct fgceth_rterm_rsp
{
    // Public

    uint32_t                    dev_id;
    uint32_t                    length;
    char                        buffer[FGCETH_MAX_RTERM_LEN];
};

// For future use

struct fgceth_pub_rsp
{
    uint32_t                    data; // TODO
};

// Union of all previous types
// All responses are returned with this format, and can be safely casted to the type identified by the union member 'type' (or accessed via the other union members).

struct fgceth_rsp
{
    enum fgceth_rsp_type        type;

    union
    {
        struct fgceth_cmd_rsp       cmd_rsp;
        struct fgceth_rterm_rsp     rterm;
        struct fgceth_pub_rsp       pub;
    } d;
};


// Status publication structures

// Gateway status.
// TODO: Fill with correct elements

struct fgceth_gw_stat
{
    uint8_t reserved[40];
};

// Union of all types of class data.
// FGC bytes hidden by reserved[40] array. 

union fgceth_class_data
{
    struct fgceth_gw_stat gw_stat;
    uint8_t               reserved[40];
};

// Status publication structure

struct fgceth_fgc_stat
{
    uint8_t     data_status;                    // [0x00] Class data status
    uint8_t     class_id;                       // [0x01] Class ID for the device
    uint8_t     reserved0;                      // [0x02] FGC command status byte
    uint8_t     runlog;                         // [0x03] FGC runlog data byte

    uint8_t     reserved1[12];                  // [0x04] Padding

    union fgceth_class_data class_data;         // [0x10] Class specific data
};


void * get_stack_pointer();
int get_stack_pointer_direction();
void print_stack_pointer(int val);


#endif
//EOF
