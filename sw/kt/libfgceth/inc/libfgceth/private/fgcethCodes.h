/*!
 * © Copyright CERN 2019. All rights not expressly granted are reserved.
 * @file   fgcethCodes.h
 * @brief  FGC_Ether code file functions.
 * @author Joao Afonso
 */

#ifndef FGCETH_CODES_H
#define FGCETH_CODES_H

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>

/**************************************
 * Functions
***************************************/

fgceth_errno codeInitCodes(struct fgceth_codes_container * codes_container);
fgceth_errno codeRefreshCodes(struct fgceth_codes_container * codes_container);

#endif // FGCETH_CODES_H

//EOF
