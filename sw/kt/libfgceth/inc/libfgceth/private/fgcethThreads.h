/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethThreads.h
 * @brief  Threads handling.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_THREADS_H
#define FGCETH_THREADS_H

#include <pthread.h>
#include <stdint.h>


/*
         THREAD               POLICY         PRIORITY      AFFINITY
 Ethernet reception         SCHED_FIFO          51            -1
*/

// Ethernet reception thread

#define FGCETH_THREAD_ETH_PRIO   94
#define FGCETH_THREAD_ETH_AFF    0

// Ms timer thread

#define FGCETH_THREAD_TIM_PRIO   95
#define FGCETH_THREAD_TIM_AFF    1


/**************************************
 * Types
***************************************/

/// Function pointer used to pass entry point for created threads.

typedef void* (fgc_ether_thread_func)(void*);



/**************************************
 * Functions
***************************************/

/**
 * Creates pthread thread and sets its scheduling policy, priority (1-99) and CPU affinity.
 *
 * Thread: User
 *
 * @param thread a thread handle
 * @param run_func a function pointer used as a thread's entry point
 * @param arg an argument passed to a run_func
 * @param policy a scheduling policy
 * @param priority a thread priority
 * @param affinity used to set affinity to a specific CPU core
 *
 * @return 0 if succeeded, error code otherwise
 */
int threads_create(pthread_t* thread, fgc_ether_thread_func run_func, void* arg, const uint8_t policy, const uint8_t priority, const uint8_t affinity);


/**
 * Create pthread mutex
 *
 * Thread: (depends)
 *
 * @param mutex a mutex handle
 *
 * @return 0 if succeeded, error code otherwise
 */
int threads_mutex_init(pthread_mutex_t* mutex);

/**
 * Create pthread conditional mutex
 *
 * Thread: (depends)
 *
 * @param cond a conditional mutex handle
 *
 * @return 0 if succeeded, error code otherwise
 */
int threads_cond_init(pthread_cond_t* cond);

/**
 * Pthread mutex destroy
 *
 * Thread: (depends)
 *
 * @param mutex a mutex handle
 *
 * @return 0 if succeeded, error code otherwise
 */
int threads_mutex_destroy(pthread_mutex_t* mutex);

/**
 * Conditional mutex destroy
 *
 * Thread: (depends)
 *
 * @param cond a conditional mutex handle
 *
 * @return 0 if succeeded, error code otherwise
 */
int threads_cond_destroy(pthread_cond_t* cond);

int threads_detect_rt_preempt();


#endif
//EOF
