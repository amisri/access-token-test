/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethRspSm.h
 * @brief  Response state machine.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_RSPSM_H
#define FGCETH_RSPSM_H


/**************************************
 * Types
***************************************/

/// Response state machines states

enum rspsm_states
{
    RSP_IDLE,                   ///< Idle state, awaiting to receive response
    RSP_FIRST_PKT,              ///< Parsing of the first received packet (and last for single-packet responses)
    RSP_MIDDLE_PKT,             ///< Parsing of the middle packets (for multi-packet responses)
    RSP_LAST_PKT                ///< Processes the last packet (for both single- and multi-packet responses)
                                ///  and marks that the whole response was received (it is reseted to IDLE by the fgcsm)
};

// Prototype of the device type

struct fgc_ether_device;
struct fgc_ether_rsp_payload;


/**************************************
 * Functions
***************************************/

/**
 * Update reception state machine: execute state function and checks for transitions.
 *
 * Thread: Ethernet
 *
 * @param device a pointer to a structure containing device data
 * @param payload a pointer to structure containing received payload
 */
void rspsm_smUpdate(fgc_ether_device* device, fgc_ether_rsp_payload* payload);


#endif
//EOF
