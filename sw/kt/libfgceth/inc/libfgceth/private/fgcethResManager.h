/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethResManager.h
 * @brief  FGC_Ether response structures and response pool management.
 * @author Joao Afonso
 */

#ifndef FGCETH_RES_MANAGER_H
#define FGCETH_RES_MANAGER_H

#include <libfgceth/private/fgcethTypesInternal.h>

/**************************************
 * Types
***************************************/


/**************************************
 * Functions
***************************************/

fgceth_errno     rspManagerInit        (struct fgceth_rsp_man * mgr, uint32_t blk_buf_used_mem, uint32_t blk_buf_size);
fgceth_errno     rspManagerClose       (struct fgceth_rsp_man * mgr);

fgceth_rsp      *rspManagerGetCmdRsp   (struct fgceth_rsp_man * mgr);
fgceth_rsp      *rspManagerGetRtermRsp (struct fgceth_rsp_man * mgr);
fgceth_rsp      *rspManagerGetPubRsp   (struct fgceth_rsp_man * mgr);

fgceth_errno     rspManagerReturnRsp   (struct fgceth_rsp_man * mgr, fgceth_rsp * rsp);
float            rspManagerMaxUsage    (struct fgceth_rsp_man * mgr);

#endif // FGCETH_RES_MANAGER_H

//EOF
