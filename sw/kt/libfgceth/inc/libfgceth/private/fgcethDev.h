/*!
 * © Copyright CERN 2019. All rights not expressly granted are reserved.
 * @file   fgcethDev.h
 * @brief  FGC_Ether device name functions.
 * @author Joao Afonso
 */

#ifndef FGCETH_DEV_H
#define FGCETH_DEV_H

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>

/**************************************
 * Functions
***************************************/

fgceth_errno devCheckDevId(const uint8_t dev_id);
uint8_t devGetDevId(const char * dev_name);
fgceth_errno devInitNames(struct fgceth_names_container * names_container, struct hash_table * name_hash, struct hash_entry * name_entry);

#endif // FGCETH_DEV_H

//EOF
