/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethEther.h
 * @brief  Low level Ethernet protocol handling.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_ETHER_H
#define FGCETH_ETHER_H

#include <inc/fgc_ether.h>

#include <libfgceth/private/fgcethThreads.h>
#include <libfgceth/private/fgcethConsts.h>

#include <linux/if_packet.h>


typedef void (*fgc_ether_recv_handler)(const uint8_t * src_addr, fgc_ether_frame* frame);

/// A structure used for the Ethernet handling
struct fgc_ether_eth
{
    int                       frame_size;           ///< Size of single frame in reception ring buffer
    uint32_t                  buf_frames;           ///<  Size of reception ring buffer in frames
    uint32_t                  buf_size;             ///<  Size of reception ring buffer in bytes
    char*                     rx_buffer;            ///<  Ring buffer for reception

    int                       socket_handle;        ///<  Handle to opened Ethernet socket
    struct sockaddr_ll        socket_addr;          ///<  Link-layer socket descriptor

    fgc_ether_header_ethernet header_unicast;       ///<  FGC-Ether header template for unicast
    fgc_ether_header_ethernet header_broadcast;     ///<  FGC-Ether header template for broadcast

    fgc_ether_recv_handler    recv_handler;         ///<  Handler for receiving packets
};

/**************************************
 * Functions
***************************************/

/**
 * Initialize Ethernet handling. Creates socket, sets up memory map, spawns Ethernet thread etc.
 *
 * Thread: User
 *
 * @param eth_name a name of the Ethernet interface
 * @param buffer_size size of the buffer used by Ethernet reception
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno ether_init(const char* eth_name, const uint32_t buffer_size, fgc_ether_recv_handler recv_handler);


/**
 * Sends passed frame (packet) via Ethernet. It sets correct MAC addresses and check if the packet
 * was sent correctly (return 0 if ok).
 *
 * Thread: Ms timer
 *
 * @param dev_id an ID of the device
 * @param frame a frame structure to be sent
 * @param frame_size size of the frame
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno ether_send(const uint8_t dev_id, void* frame, const uint32_t frame_size);


#endif
//EOF
