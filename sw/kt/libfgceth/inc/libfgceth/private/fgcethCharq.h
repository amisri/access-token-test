/*!
 * @file   fgcethCharq.h
 * @brief  Declarations for queues (FIFOs) of characters
 * @author Stephen Page (adapted to libfgceth by Joao Afonso)
 */

#ifndef FGCETH_CHARQ_H
#define FGCETH_CHARQ_H


#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethThreads.h>

#include <pthread.h>
#include <stdint.h>

/*!
 * Queue structure
 */

struct fgc_ether_charq
{
    unsigned char      *buffer;         //!< Queue buffer
    uint32_t            buffer_size;    //!< Buffer size
    uint32_t            back;           //!< Back of queue index
    uint32_t            front;          //!< Front of queue index
    uint32_t            block;          //!< Flag to indicate whether queue should allow blocking
    pthread_mutex_t     mutex;          //!< Mutex protecting access to queue
    pthread_cond_t      not_empty;      //!< Condition variable indicating that queue has content
    pthread_cond_t      not_full;       //!< Condition variable indicating that queue is not full
};

// External functions

/*!
 * Initialise the queue
 */

fgceth_errno charqInit(struct fgc_ether_charq *q, uint32_t buffer_size);

/*!
 * Free the queue
 *
 * This function destroys the queue, so the calling function must guarantee that it is called not more than once.
 * Calling this function a second time on the same queue results in undefined behaviour.
 */

void charqFree(struct fgc_ether_charq *q);

/*!
 * Insert member into the queue
 */

fgceth_errno charqPush(struct fgc_ether_charq *q, unsigned char member);

/*!
 * Insert member into the queue (block if queue is full)
 */

fgceth_errno charqBlockPush(struct fgc_ether_charq *q, unsigned char member);

/*!
 * Extract member from the queue if present, without blocking
 */

int32_t charqPop(struct fgc_ether_charq *q);

/*!
 * Extract member from the queue (block if queue is empty)
 */

int32_t charqBlockPop(struct fgc_ether_charq *q);

/*!
 * Set blocking of BlockPushes and BlockPops
 */

void charqSetBlock(struct fgc_ether_charq *q, int32_t state);

#endif

// EOF
