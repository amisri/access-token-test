/*!
 * @file   fgcethHash.h
 * @brief  Declarations for hashes
 * @author Stephen Page
 */

#ifndef HASH_H
#define HASH_H

#include <stddef.h>
#include <stdint.h>

// Constants

#define HASH_MAX_KEY_LEN    23      //!< Maximum length of a hash key

typedef uintptr_t           HASH_VALUE;

/*!
 * Union to hold the hash key
 */

union hash_key
{
    char        c[HASH_MAX_KEY_LEN + 1];
    uint32_t    i[(HASH_MAX_KEY_LEN + 1) / sizeof(uint32_t)];
};

/*!
 * Struct containing hash key and value
 */

struct hash_entry
{
    uint32_t            index;
    HASH_VALUE          value;
    union hash_key      key;
    struct hash_entry   *next;
};

/*!
 * Struct with all data necessary for the table
 */

struct hash_table
{
    size_t              size;       //!< Size of the hash table
    struct hash_entry   **data;     //!< Pointer to the array of table data
};

// External functions

/*!
 * Initialise a hash table of given size
 */

struct hash_table *hashInit(size_t size);

/*!
 * Empty a hash table
 */

void hashEmpty(struct hash_table *table);

/*!
 * Free memory held by the hash table
 */

void hashFree(struct hash_table *table);

/*!
 * Insert an entry into a hash table
 */

void hashInsert(struct hash_table *table, struct hash_entry *entry);

/*!
 * Find an entry in a hash table
 */

struct hash_entry *hashFind(struct hash_table *table, const char *key);

/*!
 * Remove an entry from a hash table
 */

struct hash_entry *hashRemove(struct hash_table *table, const char *key_string);

#endif
// EOF
