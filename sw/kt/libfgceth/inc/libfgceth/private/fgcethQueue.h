/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethQueue.h
 * @brief  General purpose FIFO queue.
 */

#ifndef FGCETH_QUEUE_H
#define FGCETH_QUEUE_H

#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethThreads.h>

#include <stdlib.h>


/**************************************
 * Types
***************************************/

/// Structure used to store queue

template<typename T>
struct fgc_ether_queue_T
{
    bool            force_unlock;
	T*              buffer;                                           ///< Queue buffer
    size_t          buffer_size;                                      ///< Buffer size
    uint32_t        back;                                             ///< Back of queue index
    uint32_t        front;                                            ///< Front of queue index
    pthread_mutex_t mutex;                                            ///< Mutex protecting access to queue
    pthread_cond_t  not_empty;                                        ///< Condition variable indicating that queue has content
    pthread_cond_t  not_full;                                         ///< Condition variable indicating that queue is not full
    void            (*pop_callback)(struct fgc_ether_queue_T<T> *, T); ///< Call-back to be called when an element is popped
};



/**************************************
 * Functions
***************************************/

/// Checks whether the queue is empty.

template <typename T>
bool queueIsEmpty(fgc_ether_queue_T<T>* q)
{
    return q->back == q->front;
}


/// Checks whether the queue is full.

template <typename T>
bool queueIsFull(fgc_ether_queue_T<T>* q)
{
    return ((q->front + 1) % q->buffer_size) == q->back;
}


/**
 * @brief Initializes the queue.
 *
 * Initializing queue by allocating memory and preparing mutexes and conditional variables.
 * For a given queue, this function has to be invoked before any other queue function.
 *
 * @param q pointer to a structure with the queue.
 * @param buffer_size size of the queue being created.
 *
 * @return returns \p FGC_SUCCESS in case of function success, otherwise error code is returned.
 */
template<typename T>
fgceth_errno queueInit(fgc_ether_queue_T<T>* q, size_t buffer_size)
{
    // Check if buffer_size is valid
    if (buffer_size <= 1)
    {
        buffer_size = 2;
    }

    // If buffer_size > 1 then allocate memory for the queue buffer.
    // Otherwise there's no allocation needed.
    // Increase buffer size by 1 to ensure proper queue operation.
    if (buffer_size++ > 1)
    {
        if(!(q->buffer = reinterpret_cast<T*>(calloc(buffer_size, sizeof(T))) ))
        {
            return FGC_ETH_MEM_ALLOC;
        }
    }

    // Initialize mutex and condition variables
    if (threads_mutex_init(&q->mutex))
    {
        free(q->buffer);
        q->buffer = NULL;
        return FGC_ETH_MUTEX_INIT;
    }

    pthread_cond_init(&q->not_empty, NULL);
    pthread_cond_init(&q->not_full,  NULL);

    q->buffer_size      = buffer_size;
    q->back             = 0;
    q->front            = 0;
    q->pop_callback     = NULL;
    q->force_unlock     = false;

    return FGC_SUCCESS;
}

/**
 * @brief Unlocks/locks pop functions.
 *
 * If unblocked, all pop functions will return immediately, regardless of their blocking behaviour (when no more items exist, they will return NULL).
 * It should be called by a thread when the Queue is expected to be terminated, to unlock all threads blocking on queueBlockPop.
 *
 * @param q pointer to a structure with the queue.
 * @param is_blocking blocking behaviour
 */

template<typename T>
void queueSetPopBlock(fgc_ether_queue_T<T>* q, bool is_blocking)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);
    q->force_unlock  = !is_blocking;
    pthread_cond_broadcast(&q->not_empty);
    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

}

/**
 * @brief Unlocks all pop functions.
 *
 * All pop functions will return immediately, regardless of their blocking behaviour (when no more items exist, they will return NULL).
 * It should be called by a thread when the Queue is expected to be terminated, to unlock all threads blocking on queueBlockPop.
 *
 * @param q pointer to a structure with the queue.
 */
/*template<typename T>
void queueUnlockPop(fgc_ether_queue_T<T>* q)
{
	int cancel_state_orig;

	// Prevent thread from dying while holding mutex
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
	pthread_mutex_lock(&q->mutex);
	q->force_unlock  = true;
	pthread_cond_broadcast(&q->not_empty);
	pthread_mutex_unlock(&q->mutex);
	pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

}
*/
/**
 * @brief Undo the queueUnlockPop function
 *
 * Blocking pop function will block again, if the queue is empty.
 *
 * @param q pointer to a structure with the queue.
 */
/*template<typename T>
void queueUnlockPopCancel(fgc_ether_queue_T<T>* q)
{
	int cancel_state_orig;

	// Prevent thread from dying while holding mutex
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
	pthread_mutex_lock(&q->mutex);
	q->force_unlock  = false;
	pthread_cond_broadcast(&q->not_empty);
	pthread_mutex_unlock(&q->mutex);
	pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

}
*/
/**
 * @brief Frees the queue.
 *
 * This function destroys the queue, so the calling function must guarantee that it is called not more than once.
 * Calling this function a second time on the same queue results in undefined behavior.
 *
 * @param q pointer to a structure with the queue.
 */
template<typename T>
void queueFree(fgc_ether_queue_T<T>* q)
{
    // Free memory
    if(q->buffer != NULL)
    {
        free(q->buffer);
        q->buffer = NULL;
    }

    // Destroy mutex and condition variables
    pthread_mutex_destroy(&q->mutex);
    pthread_cond_destroy(&q->not_empty);
    pthread_cond_destroy(&q->not_full);
}


/**
 * @brief Inserts element into the queue.
 *
 * @param q pointer to a structure with the queue.
 * @param element new element to be inserted into the queue.
 *
 * @return returns \p FGC_SUCCESS in case of function success, otherwise error code is returned.
 */
template<typename T>
fgceth_errno queuePush(fgc_ether_queue_T<T>* q, T element)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);


    // Check whether queue has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return FGC_ETH_QUE_NOT_INIT;
    }


    // Check that the queue is not full
    if (!queueIsFull(q))
    {
        // Send signal that queue has content
        if (queueIsEmpty(q))
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        // Add the element to the queue
        q->buffer[q->front] = element;
        q->front            = (q->front + 1) % q->buffer_size;

        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return FGC_SUCCESS;
    }
    else // The queue is full
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return FGC_ETH_QUE_FULL;
    }
}


/**
 * @brief Inserts element into the queue (block if queue is full).
 *
 * @param q pointer to a structure with the queue.
 * @param element new element to be inserted into the queue.
 *
 * @return returns \p FGC_SUCCESS in case of function success, otherwise error code is returned.
 */
template<typename T>
fgceth_errno queueBlockPush(fgc_ether_queue_T<T>* q, T element)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return FGC_ETH_QUE_NOT_INIT;
    }

    // Check that the queue is not full
    while (queueIsFull(q))
    {
        // Wait for a member to leave the queue

        pthread_cond_wait(&q->not_full, &q->mutex);
    }

    // Send signal that queue has content
    if(queueIsEmpty(q))
    {
        pthread_cond_broadcast(&q->not_empty);
    }

    // Add the element to the queue
    q->buffer[q->front] = element;
    q->front            = (q->front + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return FGC_SUCCESS;
}


/**
 * @brief Returns element from the back of the queue if present, without blocking
 * It does not remove element from the queue.
 *
 * @param q pointer to a structure with the queue.
 *
 * @return returns element extracted from the queue.
 */
template<typename T>
T queueBack(fgc_ether_queue_T<T>* q)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    if (queueIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    // Get the element from the back of the queue
    T element = q->buffer[q->back];

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return element;
}


/**
 * @brief Extracts element from the queue if present, without blocking
 *
 * @param q pointer to a structure with the queue.
 *
 * @return returns element extracted from the queue.
 */
template<typename T>
T queuePop(fgc_ether_queue_T<T>* q)
{
    int cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    if (queueIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    // Send signal that queue is not full
    if (queueIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the element from the queue
    T element  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    // Call pop call-back if not NULL
    if (q->pop_callback)
    {
        q->pop_callback(q, element);
    }

    return element;
}


/**
 * @brief Extracts element from the queue (block if queue is empty)
 *
 * @param q pointer to a structure with the queue.
 *
 * @return returns element extracted from the queue.
 */
template<typename T>
T queueBlockPop(fgc_ether_queue_T<T>* q)
{
    int  cancel_state_orig;

    // Prevent thread from dying while holding mutex
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialized
    if (q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return NULL;
    }

    while (queueIsEmpty(q))
    {
    	// If unlock is enforced, do not lock
    	if(q->force_unlock)
    	{
    		pthread_mutex_unlock(&q->mutex);
    		pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
    		return NULL;
    	}

        // Wait for a member to enter the queue
        pthread_cond_wait(&q->not_empty, &q->mutex);
    }

    // Send signal that queue is not full
    if (queueIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the element from the queue
    T element  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    // Call pop call-back if not NULL
    if(q->pop_callback)
    {
        q->pop_callback(q, element);
    }

    return element;
}


#endif
//EOF
