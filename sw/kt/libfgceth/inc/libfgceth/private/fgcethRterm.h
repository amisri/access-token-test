/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethRterm.h
 * @brief  FGC_Ether rterm response structures and rterm response pool management.
 * @author Joao Afonso
 */

#ifndef FGCETH_RTERM_H
#define FGCETH_RTERM_H

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethQueue.h>


/**************************************
 * Functions
***************************************/

#define FGCETH_DEV_CHARQ_SIZE 131072
#define FGCETH_RTERM_RSP_POOL_SIZE 65536

/*TO DELETE*/ //fgceth_errno rtermInit(uint32_t rterm_rsp_pool_size);

/*TO DELETE*/ //fgceth_errno rtermSubscribeDev(const uint8_t dev_id, fgceth_queue* response, fgceth_sub_handle* handle);
/*TO DELETE*/ //fgceth_errno rtermUnsubscribeDev(fgceth_sub_handle* handle);

fgceth_errno rtermPushCharToDev(const uint8_t dev_id, const char input_char);
// Has to be casted to unsigned 8 bits
int32_t      rtermPopCharToDev(const uint8_t dev_id);

/*TO DELETE*/ //fgceth_errno rtermPublishToQueues(const uint8_t dev_id, const char * buf, uint32_t buf_len);

/*TO DELETE*/ //fgceth_response * rtermStringToRtermRsp(char * buf, uint32_t buf_len);

/*TO DELETE*/ //fgceth_errno rtermClose();


fgceth_errno rtermInit(struct fgceth_rsp_man * rsp_manager);
fgceth_errno rtermSubscribeDev(const uint8_t dev_id, fgceth_rsp_queue * queue, fgceth_sub_handle* handle);
fgceth_errno rtermUnsubscribeDev(fgceth_sub_handle* handle);
fgceth_errno rtermPublishToQueues(const uint8_t dev_id, const char * buf, uint32_t buf_len);
fgceth_errno rtermStringToQueue(fgceth_rsp_queue * queue, char * buf, uint32_t buf_len);
void         rtermClose();

#endif
//EOF
