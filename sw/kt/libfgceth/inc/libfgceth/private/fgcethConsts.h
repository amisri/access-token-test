/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethConsts.h
 * @brief  Constants declarations.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_CONSTS_H
#define FGCETH_CONSTS_H

#include <inc/fgc_errs.h>
#include <inc/fgc_consts_gen.h>


/**************************************
 *   Definitions
***************************************/

#define FGC_SUCCESS           FGC_OK_NO_RSP

// log messages
#define L_INIT          (1 << 0)

#define L_ERROR         (1 << 1)
#define L_WARNING       (1 << 2)

#define L_V0            (1 << 3)
#define L_V1            (1 << 4)
#define L_V2            (1 << 5)
#define L_V3            (1 << 6)
#define L_V4            (1 << 7)


#define LOG_BUFFER_LEN      512

#define FGCETH_NO_DEV       255


#define FGCETH_CYCLE_PERIOD_MS  20
#define FGCETH_CYCLES_PER_SEC   (1000 / FGCETH_CYCLE_PERIOD_MS)

#define FGCETH_FGC_DEV_MAX  64
#define FGCETH_ALL_DEV_MAX  (FGCETH_FGC_DEV_MAX + 1)

/**************************************
 * Types
***************************************/

/// Error type

typedef fgc_errno fgceth_errno;

inline const char* fgceth_strerr(const fgceth_errno error_code)
{
    if (error_code == FGC_SUCCESS)
    {
        return "Succeeded";
    }
    else
    {
        if (error_code >= FGC_NUM_ERRS)
        {
            return fgc_errmsg[FGC_UNKNOWN_ERROR_CODE];
        }
        else
        {
            return fgc_errmsg[error_code];
        }
    }
}

#endif
//EOF
