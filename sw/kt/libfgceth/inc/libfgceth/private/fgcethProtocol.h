/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethProtocol.h
 * @brief  FGC_Ether Protocol handling.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_PROTOCOL_H
#define FGCETH_PROTOCOL_H

#include <libfgceth/private/fgcethEther.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethQueue.h>
#include <libfgceth/private/fgcethSm.h>
#include <libfgceth/private/fgcethRspSm.h>
#include <libfgceth/private/fgcethCommand.h>
#include <libfgceth/private/fgcethStats.h>
#include <inc/fgc_ether.h>

#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <string.h>


#define FGCETH_MAX_MISS_STAT_PKT 10

/**************************************
 * Functions
***************************************/

/**
 * Initializes the device by creating command queue
 * and setting default (reset state) values in the device object.
 *
 * Thread: User
 *
 * @param dev_id ID of the device
 * @param dev_name name of the device
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno protocol_initDevice(const uint8_t dev_id, char * dev_name);


/**
 * Initializes the protocol handling by initializing its subsystems.
 *
 * Thread: User
 *
 * @param eth_name a name of the Ethernet interface
 * @param responsePoolSize size of the response pool in number of responses
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
//fgceth_errno protocol_init(const char* eth_name, const uint32_t responsePoolSize);
fgceth_errno protocol_init(const char* eth_name, const uint32_t blk_buf_used_mem, const uint32_t blk_buf_size, const uint32_t pub_buffer_cycles);


/**
 * Invoked by the Ethernet reception function. It checks frame payload type
 * and pass the frame to be processed further by other respective function.
 *
 * Thread: Ethernet
 *
 * @param dev_id an ID of the device
 * @param frame a pointer to received packet
 */
void            protocol_processReceivedFrame(const uint8_t * src_addr, fgc_ether_frame* frame);


/**
 * Used to send commands via Ethernet network interface. It performs partitioning of the command
 * into Ethernet packets and sets all flags and fields required by the protocol.
 * At the end it uses ether_send function to send current packet.
 *
 * Thread: Ms timer
 *
 * @param response a pointer to a structure containing a command to be sent
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno protocol_sendCommand(fgceth_rsp * response);


/**
 * Used to finish command processing. First it pops the command from the command queue then
 * it pushes the command to its response queue. At the end, it invokes protocol_resetCurrentCommand.
 *
 * Thread: Ms timer
 *
 * @param device a pointer to a structure containing device data
 */
void            protocol_finishCurrentCommand(fgc_ether_device* device);


/**
 * It returns an error to all queued commands.
 *
 * Thread: Ms timer, Ethernet
 *
 * @param device a pointer to a structure containing device data
 * @param error an error to be returned to the commands
 */
void            protocol_finishAllCommands(fgc_ether_device* device, const fgceth_errno error);


fgceth_errno protocol_parseNames(struct fgceth_names_container * names_container, struct hash_table * name_hash, struct hash_entry * name_entry);
fgceth_errno protocol_parseCodes(struct fgceth_codes_container * codes_container);
fgceth_errno protocol_close();


#endif
//EOF
