/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethCommand.h
 * @brief  FGC_Ether response structures and response pool management.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_COMMAND_H
#define FGCETH_COMMAND_H

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethQueue.h>


/**************************************
 * Functions
***************************************/

fgceth_errno commandGetSetASync(fgceth_rsp * cmd, fgceth_rsp_queue * queue);
fgceth_errno commandGetSetSync(fgceth_rsp * cmd);

/**
 * Creates response pool (in form of a queue) with given size (which defaults to 512).
 * Then it fills the pool (queue) by allocating memory for response objects.
 *
 * Thread: User
 *
 * @param response_pool_size size of the response pool size in number of response structures
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
//fgceth_errno command_init(uint32_t response_pool_size);


/**
 * Fills and initialize fields in passed response object, according to the parameters and default values.
 * It performs trimming of whitespace characters (at the beginning, ending and in the middle of the command-value).
 * It copies command and value to respective buffers in the response structure and calculates their length.
 *
 * Thread: User
 *
 * @param response a structure containing response data
 * @param dev_id an ID of the device
 * @param commandValue string with command and space-separated value
 * @param is_set true if command is set command, false otherwise
 * @param response_queue a queue into which the ready response is pushed
 * @param user_data an optional pointer to user-specific data that will be passed to the response
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
//fgceth_errno command_construct(fgceth_response* response, const uint8_t dev_id, const char* commandValue, const bool is_set, fgceth_queue* response_queue, void* user_data);


/**
 * Adds the passed response (command) object to the command queue of respective device.
 *
 * Thread: User
 *
 * @param response a structure containing response data
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
//fgceth_errno command_addToQueue(fgceth_response* response);

//TODO: Description
//fgceth_errno command_close();

#endif
//EOF
