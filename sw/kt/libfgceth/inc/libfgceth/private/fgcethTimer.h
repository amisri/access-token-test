/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethTimer.h
 * @brief  POSIX timer handling.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_TIMER_H
#define FGCETH_TIMER_H

#include <libfgceth/private/fgcethConsts.h>

#include <time.h>
#include <stdint.h>
#include <sys/time.h>


/**************************************
 * Types
***************************************/

/// Timer handler

typedef void (*fgc_ether_timer_handler)(struct timeval time);

struct fgc_ether_timer
{
    timer_t                 timer;          // Handle to timer
    fgc_ether_timer_handler handler;        // External handler of timer
};



/**************************************
 * Functions
***************************************/

/**
 * Initializes millisecond timer (Ms timer) by creating separate thread only for initializing purpose.
 * The reason for that is the way Linux usually handles timer threads. Periodic timers create new temporary
 * thread every time the signal is generated (here every 1ms) and inherits affinity of the calling thread.
 * That is way new thread is created with fixed affinity to pass this property to timer threads.
 *
 * Thread: User
 *
 * @param handler_func a function pointer used as an entry point for the timer's thread
 *
 * @return FGC_SUCCESS if succeeded, error code otherwise
 */
fgceth_errno timer_init(fgc_ether_timer_handler handler_func);


/**
 * Returns the current time in microseconds.
 *
 * Thread: -
 *
 * @return current time in microseconds
 */
uint32_t timer_getCurrentTimeMs();
void timer_getCurrentTime(struct timeval * time);


#endif
//EOF
