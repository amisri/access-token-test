/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethFParser.cpp
 * @brief  Parsing of external files.
 * @author Joao Afonso
 */

#include <inc/fgc_ether.h>

#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/public/fgcethFParser.h>
#include <libfgceth/private/fgcethLog.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <dirent.h>


//Verify if global defined values are compliant with fgcd parameters
#if FGCETH_ALL_DEV_MAX != (FGC_ETHER_MAX_FGCS + 1)
    #error "Global const value FGCETH_ALL_DEV_MAX should be the same as FGC_ETHER_MAX_FGCS [inc/fgc_ether.h]"
#endif

#if FGCETH_MAX_DEV_CODES != (FGC_CODE_NUM_SLOTS - 1) // Internally, one slot is reserved to broadcast the device names
    #error "Global const value FGCETH_MAX_DEV_CODES should be the same as (FGC_CODE_NUM_SLOTS - 1) [inc/classes/X/defconst.h]"
#endif


fgceth_errno fgcethNamesParser(const char * path_to_file, const char * host_name, struct fgceth_names_container * names_container)
{
    int                  c                                 = EOF;
    FILE                *name_file;
    char                 format[128];
    uint32_t             items_assigned;
    char                 line_host_name[HOST_NAME_MAX + 1];
    uint32_t             line_number                       = 0;
    uint16_t              device_idx;
    char                 device_name[FGC_MAX_DEV_LEN + 1];
    uint16_t              device_class;
    uint16_t             device_omode_mask;
    int                  count                             = 0;

    fgcethInitMsgLog(FGCETH_NO_DEV, L_V2, "[Name file] Parsing started...");

    if(!(name_file = fopen(path_to_file, "r")))
    {
        fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Device name file: Unable to open file.");
        return FGC_ETH_FILE_ERROR;
    }

    for(int i = 0; i < FGCETH_ALL_DEV_MAX; i++)
    {
        if(names_container->name_data[i].dev_name != NULL)
        {
            free(names_container->name_data[i].dev_name);
            names_container->name_data[i].dev_name         = NULL;
            names_container->name_data[i].dev_class        = FGC_CLASS_UNKNOWN;
            names_container->name_data[i].dev_omode_mask   = 0;
        }
    }

    fgcethInitMsgLog(FGCETH_NO_DEV, L_V0, "[Name file] Filtering by hostname '%s'.", host_name);

    sprintf(format, "%%%d[^:]:%%hu:%%hu:%%%d[^:]:0x%%04hX", HOST_NAME_MAX, FGC_MAX_DEV_LEN);

    do
    {

        items_assigned = fscanf(name_file, format, line_host_name,
                                                   &device_idx,
                                                   &device_class,
                                                   device_name,
                                                   &device_omode_mask);

        line_number++;

        if(items_assigned == 5)
        {
            // Check whether entry is for this host && device is not the GW itself

            if(!strcasecmp(line_host_name, host_name))
            {
                // Check that the entry is within the valid range of channels

                fgcethInitMsgLog(FGCETH_NO_DEV, L_V0, "[Name file] analysing input - %s:%u:%u:%s:0x%04X.", line_host_name, device_idx, device_class, device_name, device_omode_mask);

                if(device_idx > FGCETH_FGC_DEV_MAX)
                {
                    fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Device name file: Invalid device ID %d.", device_idx);
                    return FGC_ETH_WRONG_DEV_ID;
                }

                // Check that device has not already been assigned a name

                if(names_container->name_data[device_idx].dev_name == NULL)
                {
                    names_container->name_data[device_idx].dev_name         = (char*) calloc((FGC_MAX_DEV_LEN + 1), sizeof(char));
                    strncpy(names_container->name_data[device_idx].dev_name, device_name, FGC_MAX_DEV_LEN + 1);
                    names_container->name_data[device_idx].dev_name[FGC_MAX_DEV_LEN] = '\0';

                    names_container->name_data[device_idx].dev_class        = device_class;
                    names_container->name_data[device_idx].dev_omode_mask   = device_omode_mask;
                }
                else
                {
                    fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Device name file: Device ID %d was already assigned.", device_idx);
                    return FGC_ETH_WRONG_DEV_ID;
                }

                fgcethInitMsgLog(FGCETH_NO_DEV, L_V0, "[Name file] device %d (%s) info obtained from file.", device_idx, names_container->name_data[device_idx].dev_name);

                count++;
            }

         }

         // Read until end of line or file

         while((c = fgetc(name_file)) != EOF && c != '\n');

    } while(c != EOF);

    if(count > 0)
    {
        fgcethInitMsgLog(FGCETH_NO_DEV, L_V2, "[Name file] %d devices were read from name file.", count);
    }
    else
    {
        fgcethInitWarningMsgLog(FGCETH_NO_DEV, "[Name file] Did not find any device matching hostname '%s'.", host_name);
    }

    if(names_container->name_data[0].dev_name == NULL)
    {
        fgcethInitErrorMsgLog(FGCETH_NO_DEV, "[Name file] Device 0 (gateway) not found on name file.");
        return FGC_ETH_WRONG_DEV_ID; //TODO: use another code
    }

    fclose(name_file);

    return FGC_SUCCESS;
}

fgceth_errno fgcethCodesParser(const char * path, struct fgceth_codes_container * codes_container)
{
    struct stat     stat_buf;
    struct dirent **name_list;
    int             file_fd;
    char            file_path[256];
    ssize_t         file_length;
    int             i, j, n;
    uint8_t        *code_ptr;
    bool            is_reg_file;
    char           *file_name_ptr;

    fgcethInitMsgLog(FGCETH_NO_DEV, L_V2, "[Code files] Parsing started...");

    // Check whether the host code path exists

    if(stat(path, &stat_buf)) // Local code path does not exist
    {
        fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Code files: Invalid path %s.", path);
        return FGC_ETH_FILE_ERROR;
    }

    if(S_ISREG(stat_buf.st_mode))
    {
        is_reg_file = true;
        n = 1;
    }
    else
    {
        is_reg_file = false;
        // Read code directory in lexicographical order
        if((n = scandir(path, &name_list, NULL, alphasort)) < 0)
        {
            fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Code files: Error while scanning directory content.");
            return FGC_ETH_FILE_ERROR; //TODO Error on reading folder contents
        }
    }

    // Read each code file

    i = 0; // iterator of code slots
    j = 0; // iterator of the namelist

    while(i < FGCETH_MAX_DEV_CODES && j < n)
    {
        if(codes_container->code_data[i].code == NULL) // Only fill is slot is previously empty
        {
            // Find next code file

            if(is_reg_file)
            {
                snprintf(file_path, sizeof(file_path), "%s", path);
                file_name_ptr = strrchr(file_path, '/') + 1;

                if(file_name_ptr == NULL) // NULL means character '/' was not found and therefore, the path contains only the file name.
                {
                    file_name_ptr = file_path;
                }
            }
            else
            {
                snprintf(file_path, sizeof(file_path), "%s/%s", path, name_list[j]->d_name);
                file_name_ptr = name_list[j]->d_name;
            }

            j++;

            // Can not stat file

            if(stat(file_path, &stat_buf))
            {
                fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Code files: Unable to access file %s stat.", file_path);
                return FGC_ETH_FILE_ERROR; //TODO Error on finding directory path
            }

            // Is it a regular file?

            if(!S_ISREG(stat_buf.st_mode))
            {
                fgcethInitWarningMsgLog(FGCETH_NO_DEV, "Code files: %s is not regular file. File ignored.", file_path);
                continue;
            }

            // Open code file

            if((file_fd = open(file_path, O_RDONLY)) < 0)
            {
                fgcethInitWarningMsgLog(FGCETH_NO_DEV, "Code files: Unable to open file %s.", file_path);
                continue;
            }

            // Read code

            if(stat_buf.st_size > FGC_CODE_SIZE)
            {
                fgcethInitWarningMsgLog(FGCETH_NO_DEV, "Code files: File %s has invalid size %ld.", file_path, stat_buf.st_size);
                close(file_fd);
                continue;
            }

            code_ptr = (uint8_t *) calloc(stat_buf.st_size + 32, 1);

            if(code_ptr == NULL)
            {
                fgcethInitErrorMsgLog(FGCETH_NO_DEV, "Code files: Calloc failed.");
                return FGC_ETH_MEM_ALLOC;
            }

            if((file_length = read(file_fd, code_ptr, stat_buf.st_size)) < 0) // Error reading file
            {
                fgcethInitWarningMsgLog(FGCETH_NO_DEV, "Code files: Unable to read from file %s.", file_path);
                free(code_ptr);
                close(file_fd);
                continue;
            }

            close(file_fd);

            // Extract code information

            codes_container->code_data[i].code        = code_ptr;
            codes_container->code_data[i].code_length = file_length;
            strncpy(codes_container->code_data[i].code_name, file_name_ptr, sizeof(codes_container->code_data[i].code_name));
            codes_container->code_data[i].code_name[sizeof(codes_container->code_data[i].code_name) - 1] = '\0';

            fgcethInitMsgLog(FGCETH_NO_DEV, L_V0, "[Code files] Code file %s was parsed (%.2f KB).", codes_container->code_data[i].code_name, codes_container->code_data[i].code_length / 1024.0);
        }

        i++;
    }

    fgcethInitMsgLog(FGCETH_NO_DEV, L_V2, "[Code files] A total of %d files were parsed.", i);

    if(!is_reg_file)
    {
        for(i = 0; i != n; ++i)
        {
            free(name_list[i]);
        }
        free(name_list);
    }

    return FGC_SUCCESS;
}

fgceth_errno fgcethFreeContainers(struct fgceth_names_container * names_container, struct fgceth_codes_container * codes_container)
{
    if(names_container != NULL)
    {
        for(int i = 0; i < FGCETH_ALL_DEV_MAX; i++)
        {
            if(names_container->name_data[i].dev_name != NULL)
            {
                free(names_container->name_data[i].dev_name);

                names_container->name_data[i].dev_name         = NULL;
                names_container->name_data[i].dev_class        = FGC_CLASS_UNKNOWN;
                names_container->name_data[i].dev_omode_mask   = 0;
            }
        }
    }

    if(codes_container != NULL)
    {
        for(int i = 0; i < FGCETH_MAX_DEV_CODES; i++)
        {
            if(codes_container->code_data[i].code != NULL)
            {
                free(codes_container->code_data[i].code);
                codes_container->code_data[i].code             = NULL;
                memset(codes_container->code_data[i].code_name, 0, sizeof(codes_container->code_data[i].code_name));
                codes_container->code_data[i].code_length      = 0;
            }
        }
    }

    return FGC_SUCCESS;
}

//EOF
