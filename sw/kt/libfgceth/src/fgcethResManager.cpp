/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethResManager.cpp
 * @brief  FGC_Ether response structures and response pool management.
 * @author Joao Afonso
 */

#include <libfgceth/private/fgcethResManager.h>
#include <libfgceth/private/fgcethQueue.h>
#include <libfgceth/private/fgcethConsts.h>

#include <cclibs.h>
#include <libblkbuf.h>



fgceth_errno rspManagerInit(struct fgceth_rsp_man * mgr, uint32_t blk_buf_used_mem, uint32_t blk_buf_size)
{
    mgr->blk_buf_mgr = blkbufMgrInit(NULL, blk_buf_used_mem, blk_buf_size);

    if(mgr->blk_buf_mgr == NULL)
    {
        return FGC_ETH_MEM_ALLOC;
    }
    else
    {
        return FGC_SUCCESS;
    }
}



fgceth_rsp * rspManagerGetCmdRsp(struct fgceth_rsp_man * mgr)
{
    fgceth_rsp * rsp = (fgceth_rsp *) blkbufGetVoidBlk(mgr->blk_buf_mgr);

    if(rsp != NULL)
    {
        memset(rsp, 0, sizeof(fgceth_rsp));

        rsp->type                     = CMD_RSP;

        rsp->d.cmd_rsp.value_opts     = blkbufInit(mgr->blk_buf_mgr);
        rsp->d.cmd_rsp.response       = blkbufInit(mgr->blk_buf_mgr);

        if(rsp->d.cmd_rsp.value_opts == NULL || rsp->d.cmd_rsp.response == NULL)
        {
            if(rsp->d.cmd_rsp.value_opts != NULL) 
            {
                blkbufDestroy(rsp->d.cmd_rsp.value_opts);
            }

            if(rsp->d.cmd_rsp.response != NULL)   
            {
                blkbufDestroy(rsp->d.cmd_rsp.response);
            }

            blkbufReturnVoidBlk(mgr->blk_buf_mgr, rsp);

            return NULL;
        }

        rsp->d.cmd_rsp.error          = FGC_SUCCESS;

        rsp->d.cmd_rsp.tag[0]         = '\0';
        rsp->d.cmd_rsp.dev_name[0]    = '\0';
        rsp->d.cmd_rsp.property[0]    = '\0';

        rsp->d.cmd_rsp.client_name[0] = '\0';
        rsp->d.cmd_rsp.user_data      = NULL;

        blkbufIteratorStart(rsp->d.cmd_rsp.value_opts);
        blkbufIteratorStart(rsp->d.cmd_rsp.response);

    }

    return rsp;
}



fgceth_rsp * rspManagerGetRtermRsp(struct fgceth_rsp_man * mgr)
{
    fgceth_rsp * rsp = (fgceth_rsp *) blkbufGetVoidBlk(mgr->blk_buf_mgr);

    if(rsp != NULL)
    {
        rsp->type              = RTERM;

        rsp->d.rterm.length    = 0;
        rsp->d.rterm.buffer[0] = '\0';
    }

    return rsp;
}



fgceth_rsp   * rspManagerGetPubRsp(struct fgceth_rsp_man * mgr)
{
    fgceth_rsp * rsp = (fgceth_rsp *) blkbufGetVoidBlk(mgr->blk_buf_mgr);

    if(rsp != NULL)
    {
        rsp->type = PUB;
    }

    return rsp;
}



fgceth_errno rspManagerReturnRsp(struct fgceth_rsp_man * mgr, fgceth_rsp * rsp)
{
    CC_ASSERT(rsp != NULL);

    switch(rsp->type)
    {
        case CMD_RSP:

            blkbufDestroy(rsp->d.cmd_rsp.value_opts);
            blkbufDestroy(rsp->d.cmd_rsp.response);
            break;

        case RTERM:
        case PUB:
        default:

            break;
    }

    blkbufReturnVoidBlk(mgr->blk_buf_mgr, rsp);

    return FGC_SUCCESS;
}



fgceth_errno rspManagerClose(struct fgceth_rsp_man * mgr)
{

    blkbufMgrDestroy(mgr->blk_buf_mgr);

    return FGC_SUCCESS;
}


float rspManagerMaxUsage(struct fgceth_rsp_man * mgr)
{
    return blkbufMaxDataUsed(mgr->blk_buf_mgr);
}


// EOF