/*!
 * @file   fgcethCharq.cpp
 * @brief  Declarations for queues (FIFOs) of characters
 * @author Stephen Page (adapted to libfgceth by Joao Afonso)
 */

#include <stdio.h>
#include <stdlib.h>

#include <libfgceth/private/fgcethThreads.h>
#include <libfgceth/private/fgcethCharq.h>

// Static functions

static int charqIsEmpty(struct fgc_ether_charq *q);
static int charqIsFull(struct fgc_ether_charq *q);



fgceth_errno charqInit(struct fgc_ether_charq *q, uint32_t buffer_size)
{
    // Allocate memory for queue buffer

    buffer_size++;
    if(!(q->buffer = (unsigned char *)malloc(buffer_size)))
    {
    	return FGC_ETH_MEM_ALLOC;
    }

    // Initialise mutex and condition variables

    if(threads_mutex_init(&q->mutex))
    {
        free(q->buffer);
        q->buffer = NULL;
        return FGC_ETH_MUTEX_INIT;
    }

    pthread_cond_init(&q->not_empty, NULL);
    pthread_cond_init(&q->not_full,  NULL);

    q->block           = 1;
    q->buffer_size     = buffer_size;
    q->back = q->front = 0;

    return FGC_SUCCESS;
}



void charqFree(struct fgc_ether_charq *q)
{
    // Free memory

    if(q->buffer != NULL)
    {
        free(q->buffer);
        q->buffer = NULL;
    }

    // Destroy mutex and condition variables

    pthread_mutex_destroy(&q->mutex);
    pthread_cond_destroy(&q->not_empty);
    pthread_cond_destroy(&q->not_full);
}



fgceth_errno charqPush(struct fgc_ether_charq *q, unsigned char member)
{
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return FGC_ETH_QUE_NOT_INIT;
    }

    // Check that the queue is not full

    if(!charqIsFull(q)) // The queue is not full
    {
        // Send signal that queue has content

        if(charqIsEmpty(q))
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        // Add the member to the queue

        q->buffer[q->front] = member;
        q->front            = (q->front + 1) % q->buffer_size;

        pthread_mutex_unlock(&q->mutex);
        return FGC_SUCCESS;
    }
    else // The queue is full
    {
        pthread_mutex_unlock(&q->mutex);
        return FGC_ETH_QUE_FULL;
    }
}



fgceth_errno charqBlockPush(struct fgc_ether_charq *q, unsigned char member)
{
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return FGC_ETH_QUE_NOT_INIT;
    }

    // Check that the queue is not full

    while(q->block && charqIsFull(q))
    {
        // Wait for a member to leave the queue

        pthread_cond_wait(&q->not_full, &q->mutex);
    }

    // Check whether the queue was unblocked

    if(q->block) // The block was not removed
    {
        // Send signal that queue has content

        if(charqIsEmpty(q))
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        // Add the member to the queue

        q->buffer[q->front] = member;
        q->front            = (q->front + 1) % q->buffer_size;

        pthread_mutex_unlock(&q->mutex);
        return FGC_SUCCESS;
    }
    else // The queue was unblocked
    {
        pthread_mutex_unlock(&q->mutex);
        return FGC_ETH_QUE_FULL;
    }
}



int32_t charqPop(struct fgc_ether_charq *q)
{
    unsigned char   member;

    // Prevent thread from dying while holding mutex

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    if(charqIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    // Send signal that queue is not full

    if(charqIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the member from the queue

    member  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    return member;
}



int32_t charqBlockPop(struct fgc_ether_charq *q)
{
    unsigned char   member;

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    while(q->block && charqIsEmpty(q))
    {
        // Wait for a member to enter the queue

        pthread_cond_wait(&q->not_empty, &q->mutex);
    }

    // Check whether the queue was unblocked

    if(!q->block)
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    // Send signal that queue is not full

    if(charqIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the member from the queue

    member  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    return member;
}



void charqSetBlock(struct fgc_ether_charq *q, int32_t state)
{
    pthread_mutex_lock(&q->mutex);

    if(!(q->block = state))
    {
        pthread_cond_broadcast(&q->not_empty);
        pthread_cond_broadcast(&q->not_full);
    }

    pthread_mutex_unlock(&q->mutex);
}



// Static functions



/*
 * Is the queue empty?
 */

static int charqIsEmpty(struct fgc_ether_charq *q)
{
    return q->back == q->front;
}



/*
 * Is the queue full?
 */

static int charqIsFull(struct fgc_ether_charq *q)
{
    return (q->front + 1) % q->buffer_size == q->back;
}

// EOF
