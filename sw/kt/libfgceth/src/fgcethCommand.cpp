/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethCommand.cpp
 * @brief  FGC_Ether response structures and response pool management.
 * @author Dariusz Zielinski
 * @author Joao Afonso
 */

#include <libfgceth/private/fgcethCommand.h>
#include <libfgceth/private/fgcethProtocol.h>
#include <libfgceth/private/fgcethTimer.h>
#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethDev.h>

#include <cclibs.h>
#include <libblkbuf.h>

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <ctype.h>


/**************************************
 * Functions
***************************************/

fgceth_errno commandGetSetASync(fgceth_rsp * cmd, fgceth_rsp_queue * queue)
{
    fgceth_errno err;

    CC_ASSERT(cmd != NULL);
    CC_ASSERT(queue != NULL);
    CC_ASSERT(cmd->type == CMD_RSP);

    uint8_t dev_id = devGetDevId(cmd->d.cmd_rsp.dev_name);

    if ((err = devCheckDevId(dev_id)) != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(0, "Failed to get/set property value asynchronously. Unable to validate device %s name, with error %d (%s).", cmd->d.cmd_rsp.dev_name, err, fgceth_strerr(err));
        return err;
    }

    blkbufReplaceStr(cmd->d.cmd_rsp.response, "");

    cmd->d.cmd_rsp.hidden_members_.response_queue    = queue;
    cmd->d.cmd_rsp.hidden_members_.timeout_timestamp = timer_getCurrentTimeMs();
    cmd->d.cmd_rsp.error                             = FGC_SUCCESS;
    cmd->d.cmd_rsp.hidden_members_.dev_id            = dev_id;

    cmd->d.cmd_rsp.hidden_members_.cmd_step          = START;

    // Send command to the command queue

    if ((err = queuePush(&fgceth_vars.devices[dev_id].cmd_queue, cmd)) != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(dev_id, "Unable to send command to queue.");
        return err;
    }

    return FGC_SUCCESS;
}

fgceth_errno commandGetSetSync(fgceth_rsp * cmd)
{
    CC_ASSERT(cmd != NULL);
    CC_ASSERT(cmd->type == CMD_RSP);

    uint8_t      dev_id = devGetDevId(cmd->d.cmd_rsp.dev_name);
    fgceth_errno err;

    if ((err = devCheckDevId(dev_id)) != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(0, "Failed to get/set property value synchronously. Unable to validate device %s name, with error %d (%s).", cmd->d.cmd_rsp.dev_name, err, fgceth_strerr(err));
        return err;
    }

    // Create one element queue to implement blocking behavior of the get() / set()

    fgceth_rsp_queue queue;

    if ((err = queueInit(&queue, 2)) != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(dev_id, "Failed to get/set property value synchronously. Unable to initialize queue, with error %d (%s).", err, fgceth_strerr(err));
        return err;
    }

    blkbufReplaceStr(cmd->d.cmd_rsp.response, "");

    cmd->d.cmd_rsp.hidden_members_.response_queue    = &queue;
    cmd->d.cmd_rsp.hidden_members_.timeout_timestamp = timer_getCurrentTimeMs();
    cmd->d.cmd_rsp.error                             = FGC_SUCCESS;
    cmd->d.cmd_rsp.hidden_members_.dev_id            = dev_id;

    cmd->d.cmd_rsp.hidden_members_.cmd_step          = START;

    // Send command to the command queue

    if ((err = queuePush(&fgceth_vars.devices[dev_id].cmd_queue, cmd)) != FGC_SUCCESS)
    {
       fgcethWarningMsgLog(dev_id, "Unable to send command to queue.");
       return err;
    }

    // Block the current thread until the command is ready or a timeout occurs

    queueBlockPop(&queue);
    queueFree(&queue);

     // Return response error code

    return cmd->d.cmd_rsp.error;
}

//EOF
