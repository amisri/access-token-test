/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethLog.h
 * @brief  Utilities for the logging of messages by the library.
 * @author Joao Afonso
 */

#include <unistd.h>
#include <stdio.h>

#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>

#include <inc/fgc_code.h>

FILE      *fgc_ether_global_struct::log_file      = stderr;
FILE      *fgc_ether_global_struct::log_init_file = stdout;

#ifndef NDEBUG
bool       fgc_ether_global_struct::log_long      = true;
#else
bool       fgc_ether_global_struct::log_long      = false;
#endif

#ifndef NDEBUG
uint32_t dev_enabled_flags = L_INIT | L_ERROR | L_WARNING | L_V0 | L_V1 | L_V2 | L_V3 | L_V4; // Erros, warnings and init enabled by default
#else
uint32_t dev_enabled_flags = L_INIT | L_ERROR | L_WARNING | L_V0; // Erros, warnings and init enabled by default
#endif

void fgcethSetMsgLogFlags(uint32_t new_flag)
{
	uint32_t aux = 0;
	aux = L_INIT | L_ERROR | L_WARNING | L_V0;

	if     (new_flag == L_V0) //Each log level should include the levels above
	{
		dev_enabled_flags = aux;
	}
	else if(new_flag == L_V1)
	{
		dev_enabled_flags = aux | L_V1;
	}
	else if(new_flag == L_V2)
	{
		dev_enabled_flags = aux | L_V1 | L_V2;
	}
	else if(new_flag == L_V3)
	{
		dev_enabled_flags = aux | L_V1 | L_V2 | L_V3;
	}
	else if(new_flag == L_V4)
	{
		dev_enabled_flags = aux | L_V1 | L_V2 | L_V3 | L_V4;
	}

}

void fgcethSetMsgLogFile(FILE * f)
{
	fgc_ether_global_struct::log_file = f;
}

void fgcethSetMsgLogFileInit(FILE * f)
{
	fgc_ether_global_struct::log_init_file = f;
}

void fgcethDetailedMsgLog(bool b)
{
	fgc_ether_global_struct::log_long = b;
}


void fgcethDefaultMsgLogPrint(FILE * fd_out, uint8_t dev_id, uint32_t flags, bool optional, const char * msg_type, const char * format, va_list args)
{
	char buffer[LOG_BUFFER_LEN];
	long int len = 0;
	struct timeval tv;

	if((dev_enabled_flags & flags) || !optional)
	{
		gettimeofday(&tv, NULL);
		len =  strftime(    buffer,     sizeof(buffer),     "%Y/%m/%d %H:%M:%S" , localtime(&tv.tv_sec));
		len += snprintf(    buffer+len, sizeof(buffer)-len, ".%03ld",            tv.tv_usec / 1000);

		if(fgc_ether_global_struct::lib_initialized && dev_id <= FGC_ETHER_MAX_FGCS && fgceth_vars.devices[dev_id].declared)
		{
			len += snprintf(buffer+len, sizeof(buffer)-len, " %d(%s)", dev_id, fgceth_vars.devices[dev_id].name);
		}
		else if(fgc_ether_global_struct::lib_initialized && dev_id <= FGC_ETHER_MAX_FGCS)
		{
			len += snprintf(buffer+len, sizeof(buffer)-len, " %d(Unknown)", dev_id);
		}
		else
		{
			//Do not print name or device ID
		}

		len += snprintf(    buffer+len, sizeof(buffer)-len, " %s :",             msg_type);

		if(fgc_ether_global_struct::log_long)
		{
			len += snprintf(buffer+len, sizeof(buffer)-len, " %d : 0x%08x : ",    dev_status_number, flags);
		}

		len += vsnprintf(   buffer+len, sizeof(buffer)-len, format,               args);

		snprintf(buffer+sizeof(buffer)-7, 7, "[...]");
		fprintf(fd_out, "%s\n", buffer);
		fflush(fd_out);
	}
}

void fgcethInitErrorMsgLog(uint8_t dev_id, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethDefaultMsgLogPrint(
			fgc_ether_global_struct::log_init_file,
			dev_id,
			L_INIT | L_ERROR,
			false,
			"ERROR",
			format,
			args);

	va_end(args);

}

void fgcethInitWarningMsgLog(uint8_t dev_id, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethDefaultMsgLogPrint(
			fgc_ether_global_struct::log_init_file,
			dev_id,
			L_INIT | L_WARNING,
			false,
			"WARNING",
			format,
			args);

	va_end(args);
}

void fgcethInitMsgLog(uint8_t dev_id, uint32_t flags, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethDefaultMsgLogPrint(
			fgc_ether_global_struct::log_init_file,
			dev_id,
			flags | L_INIT,
			false,
			"INFO",
			format,
			args);

	va_end(args);
}

void fgcethErrorMsgLog_vaArgs(uint8_t dev_id, const char * format, va_list args)
{
	fgcethDefaultMsgLogPrint(
			fgc_ether_global_struct::log_file,
			dev_id,
			L_ERROR,
			false,
			"ERROR",
			format,
			args);
}

void fgcethErrorMsgLog(uint8_t dev_id, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethErrorMsgLog_vaArgs(
			dev_id,
			format,
			args);

	va_end(args);
}

void fgcethWarningMsgLog_vaArgs(uint8_t dev_id, const char * format, va_list args)
{
	fgcethDefaultMsgLogPrint(
			fgc_ether_global_struct::log_file,
			dev_id,
			L_WARNING,
			false,
			"WARNING",
			format,
			args);
}

void fgcethWarningMsgLog(uint8_t dev_id, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethWarningMsgLog_vaArgs(
			dev_id,
			format,
			args);

	va_end(args);
}

void fgcethMsgLog_vaArgs(uint8_t dev_id, uint32_t flags, const char * format, va_list args)
{
	fgcethDefaultMsgLogPrint(
			fgc_ether_global_struct::log_file,
			dev_id,
			flags,
			true,
			"INFO",
			format,
			args);
}

void fgcethMsgLog(uint8_t dev_id, uint32_t flags, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethMsgLog_vaArgs(
			dev_id,
			flags,
			format,
			args);

	va_end(args);
}

// EOF
