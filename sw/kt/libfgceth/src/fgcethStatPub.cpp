/*!
 * © Copyright CERN 2018. All rights not expressly granted are reserved.
 * @file   fgcethStatPub.cpp
 * @brief  FGC_Ether status publication handling.
 * @author Joao Afonso
 */

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethStatPub.h>
#include <libfgceth/private/fgcethLog.h>


fgceth_errno statPubInit(uint32_t buffer_cycles_num)
{
    CC_ASSERT(sizeof(union fgceth_class_data) == sizeof(union fgc_pub_class_data) && "FGCETH class data structure size does not match globaly defined defined structure in 'fgc_stat.h'");
    CC_ASSERT(sizeof(struct fgceth_fgc_stat)  == sizeof(struct fgc_stat)          && "FGCETH status pub structure size does not match globaly defined defined structure in 'fgc_stat.h'");
    
    fgceth_vars.pub_status_buf_size  = buffer_cycles_num;
    fgceth_vars.pub_status_write_idx = 1; // Start writing at one, because pre-set values at #0 are required for its calculation 

    fgceth_vars.pub_status_buf = (struct fgceth_fgc_stat *) malloc (sizeof(struct fgceth_fgc_stat) * buffer_cycles_num * FGCETH_ALL_DEV_MAX);
    fgceth_vars.pub_time       = (struct timeval *)         malloc (sizeof(struct timeval)         * buffer_cycles_num);
    fgceth_vars.pub_id         = (uint32_t *)               malloc (sizeof(uint32_t)               * buffer_cycles_num);

    if(fgceth_vars.pub_status_buf == NULL || fgceth_vars.pub_time == NULL || fgceth_vars.pub_id == NULL)
    {
        fgcethInitErrorMsgLog(0, "Publication data initialization failed. Unable to allocate buffer mem.");
        return FGC_ETH_MEM_ALLOC;
    }

    for(uint32_t i = 0; i < buffer_cycles_num; i++)
    {
        fgceth_vars.pub_time[i].tv_sec  = 0;
        fgceth_vars.pub_time[i].tv_usec = 0;
        fgceth_vars.pub_id[i]           = 0;
    }

    if(threads_mutex_init(&fgceth_vars.pub_mutex))
    {
        fgcethInitErrorMsgLog(0, "Publication data initialization failed. Unable to initialize mutex.");
        return FGC_ETH_MUTEX_INIT;
    }

    if(threads_cond_init(&fgceth_vars.pub_cond))
    {
        fgcethInitErrorMsgLog(0, "Publication data initialization failed. Unable to initialize conditional mutex.");
        return FGC_ETH_COND_MUTEX_INIT;
    }

    return FGC_SUCCESS;
}



void statPubClose()
{
    free(fgceth_vars.pub_status_buf);
    free(fgceth_vars.pub_time);
    free(fgceth_vars.pub_id);
    threads_mutex_destroy(&fgceth_vars.pub_mutex);
    threads_cond_destroy(&fgceth_vars.pub_cond);
}



void statPubAddNewStat(uint32_t dev_id, union fgc_stat_var * status)
{
    CC_ASSERT(dev_id >= 0 && dev_id <= FGCETH_FGC_DEV_MAX);

    memcpy(&fgceth_vars.pub_status_buf[(fgceth_vars.pub_status_write_idx * FGCETH_ALL_DEV_MAX) + dev_id], status, sizeof(struct fgc_stat));
}



void statPubFinishStatFrame(struct timeval time)
{
    uint32_t write_idx = fgceth_vars.pub_status_write_idx;
    uint32_t prev_idx  = (write_idx + (fgceth_vars.pub_status_buf_size - 1)) % fgceth_vars.pub_status_buf_size;
    uint32_t prev_id   = fgceth_vars.pub_id[prev_idx];

    struct fgceth_fgc_stat * stat_buf = &fgceth_vars.pub_status_buf[FGCETH_ALL_DEV_MAX * write_idx];

    fgceth_vars.pub_time[write_idx] = time;
    fgceth_vars.pub_id[write_idx]   = prev_id + 1;

    stat_buf[0].class_id = 4;
    stat_buf[0].data_status |= FGC_DEVICE_IN_DB;
    stat_buf[0].data_status |= FGC_CLASS_VALID;
    stat_buf[0].data_status |= FGC_DATA_VALID;

    for(uint32_t channel = 1; channel <= FGCETH_FGC_DEV_MAX; channel++)
    {
        struct fgc_ether_device *device = &fgceth_vars.devices[channel];
        struct fgceth_fgc_stat  *status = &stat_buf[channel];

        // Erase all flags except 3 most significant ones (FGC_ACK_PLL, FGC_SELF_PM_REQ, FGC_EXT_PM_REQ)

        status->data_status &= (FGC_ACK_PLL | FGC_SELF_PM_REQ | FGC_EXT_PM_REQ);

        if(device->declared == false || device->class_id == FGC_CLASS_UNKNOWN)
        {
            // Device not in name file

            status->data_status &= ~FGC_DEVICE_IN_DB;
            status->data_status &= ~FGC_CLASS_VALID;
        }
        else if(status->class_id != device->fgc_class)
        {
            // Device is in name file, but class is unexpected

            status->data_status |= FGC_DEVICE_IN_DB;
            status->data_status &= ~FGC_CLASS_VALID;
        }
        else
        {
            // Device is in name file and class is valid

            status->data_status |= FGC_DEVICE_IN_DB;
            status->data_status |= FGC_CLASS_VALID;
        }

        // Check device online status

        if(device->cmd_state != CMD_OFFLINE)
        {
            status->data_status |= FGC_DATA_VALID;
        }
        else
        {
            status->data_status &= ~FGC_DATA_VALID;
        }
    }

    // Change to next write idx

    pthread_mutex_lock(&fgceth_vars.pub_mutex);
    fgceth_vars.pub_status_write_idx = (fgceth_vars.pub_status_write_idx + 1) % fgceth_vars.pub_status_buf_size;
    pthread_cond_broadcast(&fgceth_vars.pub_cond);
    pthread_mutex_unlock(&fgceth_vars.pub_mutex);

}

// Get status publication data, for the last timestamp, in network order

fgceth_errno statPubGetData_nl(struct fgceth_fgc_stat ** status, struct timeval * time, uint32_t * last_pub_id)
{
    // If time input is different from current one (meaning we are ahead in comparison to last call), then we can return new status
    // TODO: Initialize mutex and condicional mutex
     
    uint32_t last_idx = *last_pub_id   % fgceth_vars.pub_status_buf_size;
    uint32_t next_idx = (last_idx + 1) % fgceth_vars.pub_status_buf_size;
    bool     wait_for_next = false;

    // If input ID is zero, move to most recent

    if(*last_pub_id == 0)
    {

        // Blocks waiting for next status packet
         
        wait_for_next = true;
        next_idx = fgceth_vars.pub_status_write_idx;
    }
    else
    {
        while((fgceth_vars.pub_id[next_idx] < *last_pub_id) && (next_idx != fgceth_vars.pub_status_write_idx))
        {
            next_idx = (next_idx + 1) % fgceth_vars.pub_status_buf_size;
        }

        if(next_idx == fgceth_vars.pub_status_write_idx)
        {
            // Blocks waiting for next status packet

            wait_for_next = true;
        }

    }

    if(wait_for_next == true)
    {

        pthread_mutex_lock(&fgceth_vars.pub_mutex);

        while(next_idx == fgceth_vars.pub_status_write_idx)
        {
            // Wait for write pointer to move to next index

            pthread_cond_wait(&fgceth_vars.pub_cond, &fgceth_vars.pub_mutex);
        }

        pthread_mutex_unlock(&fgceth_vars.pub_mutex);

    }

    // Return pointer to status

    *status = &fgceth_vars.pub_status_buf[FGCETH_ALL_DEV_MAX * next_idx];

    // Return ID of status message
    
    *last_pub_id = fgceth_vars.pub_id[next_idx];

    // Update timestamp
     
    *time = fgceth_vars.pub_time[next_idx];


    return FGC_SUCCESS;
}






































// EOF