/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethTimer.cpp
 * @brief  POSIX timer handling.
 * @author Dariusz Zielinski
 */

#include <libfgceth/private/fgcethTimer.h>
#include <libfgceth/private/fgcethThreads.h>
#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethLog.h>

#include <time.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>


/**************************************
 * Local functions
***************************************/

/**
 * Function is executed periodically by the timer (every 1ms).
 * Current time is read from the system and passed to handler function.
 *
 * Thread: Ms timer
 *
 * @param value unused
 */

static void timer_handler(union sigval value)
{
    fgcethAssertExp(fgceth_vars.tim.handler != NULL);
    struct timeval time;
    gettimeofday(&time, NULL);
    fgceth_vars.tim.handler(time);
}



/**
 * Creates millisecond timer (Ms timer), sets its priority and affinity and then starts the timer.
 * It is an entry point of Initialize Timer Thread. See description of timer_init function for details.
 *
 * Thread: Initialize timer
 *
 * @param result used to pass a function result to a calling thread
 */

static void* timer_initThread(void* unused)
{
    // Preparing thread parameters and attributes

    pthread_attr_t        thread_attr;
    struct sched_param    thread_param;
    int32_t               pthread_err = pthread_attr_init(&thread_attr);

    // ***************************************************

    fgcethInitMsgLog(0, L_V2, "[Timer creation thread] Starting timer...");

    if (pthread_err != 0)
    {
    	fgcethInitErrorMsgLog(0, "Timer creation failed. Unable to initialize pthread attributes.");
        pthread_exit((void *) (intptr_t) FGC_ETH_THREAD_ATTR);
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&thread_attr, SCHED_FIFO);
    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = FGCETH_THREAD_TIM_PRIO;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    // ***************************************************

    // Preparing timer event

    struct sigevent event;
    event.sigev_notify              = SIGEV_THREAD;
    event.sigev_notify_attributes   = &thread_attr;
    event.sigev_notify_function     = &timer_handler;
    event.sigev_value.sival_ptr     = &fgceth_vars.tim.timer;

    // Creating timer

    if (timer_create(CLOCK_REALTIME, &event, &fgceth_vars.tim.timer))
    {
    	fgcethInitErrorMsgLog(0, "Timer creation failed.");
        pthread_exit((void *)(intptr_t) FGC_ETH_TIMER_CREATE);
    }

    // Setting timer interval to first event time

	#define FGCETH_MILL 1000000
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);

    // Set timer to trigger on the next 20 ms

    ts.tv_nsec = ((ts.tv_nsec / FGCETH_MILL) + 20) * FGCETH_MILL;
    if(ts.tv_nsec > (1000 * FGCETH_MILL))
    {
    	ts.tv_sec++;
    	ts.tv_nsec -= 1000 * FGCETH_MILL;
    }

    struct itimerspec its;
    its.it_value.tv_sec      = ts.tv_sec;
    its.it_value.tv_nsec     = ts.tv_nsec;  // 1ms initial expiration time
    its.it_interval.tv_sec   = 0;
    its.it_interval.tv_nsec  = FGCETH_MILL; // 1ms interval

    // ***************************************************

    // Starting timer

    if (timer_settime(fgceth_vars.tim.timer, TIMER_ABSTIME, &its, NULL) == -1)
    {
    	fgcethInitErrorMsgLog(0, "Timer creation failed. Unable to set timer interval values.");
        pthread_exit((void *)(intptr_t) FGC_ETH_TIMER_START);
    }

    // ***************************************************

    pthread_attr_destroy(&thread_attr);

    // ***************************************************

    if(threads_mutex_init(&fgceth_vars.timer_mutex))
    {
        fgcethInitErrorMsgLog(0, "Timer creation failed. Unable to initialize timer mutex.");
        pthread_exit((void *)(intptr_t) FGC_ETH_MUTEX_INIT);
    }

    // ***************************************************

    fgcethInitMsgLog(0, L_V2, "[Timer creation thread] Timer successfully started. Timer thread can be closed.");

    pthread_exit((void *)(intptr_t) FGC_SUCCESS);
}



/**************************************
 * Functions
***************************************/

fgceth_errno timer_init(fgc_ether_timer_handler handler_func)
{
    fgcethAssertExp(handler_func != NULL);

    // Assign timer handler

    fgceth_vars.tim.handler = handler_func;

    // ***************************************************

    // The thread will exit immediately after initialization is done.
    // It is used just to set affinity of the created timer, as POSIX timer threads will inherit affinity of the calling thread.

    fgcethInitMsgLog(0, L_V2, "[Timer] Launching timer creation thread...");

    if (threads_create(&fgceth_vars.timer_thread, timer_initThread, NULL, SCHED_FIFO, FGCETH_THREAD_TIM_PRIO, FGCETH_THREAD_TIM_AFF))
    {
    	fgcethInitErrorMsgLog(0, "Timer creation thread has failed. Unable to create thread.");
        return FGC_ETH_THREAD;
    }

    // ***************************************************

    // Join created thread and retrieve error code

    void* error = 0;

    if ( (errno = pthread_join(fgceth_vars.timer_thread, &error)) != 0 )
    {
    	fgcethInitErrorMsgLog(0, "Timer creation thread has failed. Unable to join thread.");

    	return FGC_ETH_THREAD_JOIN;
    }

    fgcethInitMsgLog(0, L_V2, "[Timer] Timer creation thread is closed. Timer successfully created.");

    return (fgceth_errno) (intptr_t) error;
}



void timer_getCurrentTime(struct timeval * time)
{
    // Get time current time

    gettimeofday(time, NULL);
}



uint32_t timer_getCurrentTimeMs()
{
    // Get time current time

    struct timeval time;
    timer_getCurrentTime(&time);

    // Convert it to microseconds

    return (time.tv_sec * 1000000) + time.tv_usec;
}



//EOF
