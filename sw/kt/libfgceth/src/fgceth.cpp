/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgceth.cpp
 * @brief  FGH_Ether Library main file. API implementation.
 * @author Dariusz Zielinski
 * @author Joao Afonso
 */

#define  FGC_GLOBALS
#include <libfgceth.h>
#include <libfgceth/private/fgcethSm.h>
#include <libfgceth/private/fgcethProtocol.h>
#include <libfgceth/private/fgcethQueue.h>
#include <libfgceth/private/fgcethCommand.h>
#include <libfgceth/private/fgcethThreads.h>
#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethRterm.h>
#include <libfgceth/private/fgcethTimer.h>
#include <libfgceth/private/fgcethDev.h>
#include <libfgceth/private/fgcethCodes.h>
#include <libfgceth/private/fgcethStatPub.h>

#include <libfgceth/public/fgcethFParser.h>

#include <libblkbuf.h>
#include <cclibs.h>

#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

/// Definition of the structure with global variables
fgc_ether_global_struct fgceth_vars;
bool          fgc_ether_global_struct::lib_initialized = false;
uint32_t      fgc_ether_global_struct::timeout         = 120000000; // Default timeout is 2 minutes (in microseconds)
bool          fgc_ether_global_struct::pm_flag_enabled = false;
bool          fgc_ether_global_struct::fgc_logger_enabled = false;



/************************************************************************/
/******   Configuration   ***********************************************/

void fgcethEnablePmFlag(bool enable_pm)
{
	fgc_ether_global_struct::pm_flag_enabled = enable_pm;
}



void fgcethEnableFGCLogger(bool enable_fgc_logger)
{
    fgc_ether_global_struct::fgc_logger_enabled = enable_fgc_logger;
}



void fgcethSetTimeout(uint32_t milliseconds)
{
    // Set timeout to a given value or the default one (2 minutes), in microseconds

    fgceth_vars.timeout = (milliseconds == 0 ? 120000000 : milliseconds * 1000);
}



/************************************************************************/
/******   Initialization   **********************************************/

fgceth_errno fgcethInit(const char* eth_name, fgceth_names_container * names_container, fgceth_codes_container * codes_container /* Can be NULL */, const uint32_t alloc_mem_size, const uint32_t pub_buffer_cycles)
{
	fgceth_errno err;

	CC_ASSERT(eth_name != NULL);
	CC_ASSERT(names_container != NULL);
    CC_ASSERT(sizeof(struct fgceth_rsp) <= 1024); // Blk buffer block size must be large enough to be safely casted to fgceth_rsp

	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Initializing...");

	if(!(fgceth_vars.name_hash = hashInit(FGCETH_ALL_DEV_MAX)))
	{
		fgcethInitErrorMsgLog(0, "Failed to initialize library. Unable to initialize name hash.");
		return FGC_ETH_HASH_INIT;
	}

	hashEmpty(fgceth_vars.name_hash);

	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Reading device name data...");

	if((err = devInitNames(names_container, fgceth_vars.name_hash, fgceth_vars.name_entry)) != FGC_SUCCESS)
	{
		fgcethInitErrorMsgLog(0, "Failed to initialize library. Unable to parse device name information, with error %d (%s).", err, fgcethStrerr(err));
		hashFree(fgceth_vars.name_hash);

		return err;
	}

	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Reading code data...");

	if((err = codeInitCodes(codes_container)) != FGC_SUCCESS)
	{
		fgcethInitErrorMsgLog(0, "Failed to initialize library. Unable to parse code file information, with error %d (%s).", err, fgcethStrerr(err));
		hashFree(fgceth_vars.name_hash);

		return err;
	}

	// Initialize protocol handling

	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Initializing protocol handling...");

	if((err = protocol_init(eth_name, alloc_mem_size, 1024, pub_buffer_cycles)) != FGC_SUCCESS)
	{
		fgcethInitErrorMsgLog(0, "Failed to initialize library. Unable to initialize protocol handling, with error %d (%s).", err, fgcethStrerr(err));
		hashFree(fgceth_vars.name_hash);

		return err;
	}

	// Set the lib_initialized to true to mark that the library is initialized

	fgc_ether_global_struct::lib_initialized = true;

	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Successfully initialized.");

	return FGC_SUCCESS;
}



/************************************************************************/
/******   Code re-configuration   ***************************************/

fgceth_errno fgcethUpdateCodes(fgceth_codes_container * codes_container)
{
	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Reading new code data...");

	fgceth_errno err;

	if((err = codeRefreshCodes(codes_container)) != FGC_SUCCESS)
	{
		fgcethInitErrorMsgLog(0, "Unable to parse code file information, with error %d (%s).", err, fgcethStrerr(err));
		hashFree(fgceth_vars.name_hash);
		return err;
	}

	return FGC_SUCCESS;
}



/************************************************************************/
/******   Device check   ************************************************/

uint8_t fgcethGetDevId(const char * dev_name)
{
    return devGetDevId(dev_name);
}



bool fgcethIsDeviceReady(const uint8_t dev_id)
{
    // Check if passed device ID is valid

    if (devCheckDevId(dev_id) != FGC_SUCCESS)
    {
        return false;
    }

    // Return device state

    return (fgceth_vars.devices[dev_id].cmd_state != CMD_OFFLINE &&
            fgceth_vars.devices[dev_id].cmd_state != CMD_IN_BOOT);
}



fgceth_errno fgcethCheckDeviceId(const uint8_t dev_id)
{
    return devCheckDevId(dev_id);
}



/************************************************************************/
/******   Statistics and errors   ***************************************/

fgceth_errno fgcethGetGlobalStats(fgceth_global_stats* stats)
{
    if (!fgc_ether_global_struct::lib_initialized)
    {
    	fgcethWarningMsgLog(0, "Failed to obtain global statistics. Library not initialized.");
    	return FGC_ETH_NOT_INITIALIZED;
    }

    if (stats != NULL)
    {
        *stats = fgceth_vars.global_stats;
        return FGC_SUCCESS;
    }
    else
    {
    	fgcethWarningMsgLog(0, "Failed to obtain global statistics. Null pointer received as parameter.");
        return FGC_ETH_NULL_POINTER;
    }
}



fgceth_errno fgcethGetDeviceStats(fgceth_device_stats* stats, const uint8_t dev_id)
{
    fgceth_errno err;

    // Check if passed device ID is valid

    if ((err = devCheckDevId(dev_id)) != FGC_SUCCESS)
    {
    	fgcethWarningMsgLog(dev_id, "Failed to obtain device statistics. Unable to validate device ID, with error %d (%s).", err, fgcethStrerr(err));
    	return err;
    }

    if (stats != NULL)
    {
        *stats = fgceth_vars.devices[dev_id].stats;
        return FGC_SUCCESS;
    }
    else
    {
    	fgcethWarningMsgLog(dev_id, "Failed to obtain device statistics. NULL pointer received as parameter.");
        return FGC_ETH_NULL_POINTER;
    }
}



const char* fgcethStrerr(const fgceth_errno error_code)
{
    return fgceth_strerr(error_code);
}



/************************************************************************/
/******   Synchronous get and set   *************************************/

fgceth_errno fgcethGetSet(fgceth_rsp * cmd)
{
    CC_ASSERT(cmd->type == CMD_RSP);

    fgceth_errno err = commandGetSetSync(cmd);

    if(err != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(devGetDevId(cmd->d.cmd_rsp.dev_name), "Failed to %s property value synchronously, with error %d (%s), by %s", cmd->d.cmd_rsp.cmd_type == SET ? "set" : "get", err, fgcethStrerr(err), cmd->d.cmd_rsp.client_name);
    }
    else
    {
        char value_array[20];

        blkbufCopyStr(value_array, cmd->d.cmd_rsp.value_opts, sizeof(value_array));
        fgcethMsgLog(devGetDevId(cmd->d.cmd_rsp.dev_name), cmd->d.cmd_rsp.cmd_type == SET ? L_V0 : L_V1, "Sync command sent by %s (%c %s %s).", cmd->d.cmd_rsp.client_name, cmd->d.cmd_rsp.cmd_type == SET ? 'S' : 'G', cmd->d.cmd_rsp.property, value_array);
    }

    return err;
}



/************************************************************************/
/******   Asynchronous get and set   ************************************/

fgceth_errno fgcethGetSetAsync(fgceth_rsp * cmd, fgceth_rsp_queue * queue)
{
    CC_ASSERT(cmd->type == CMD_RSP);

    fgceth_errno err = commandGetSetASync(cmd, queue);

    if(err != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(devGetDevId(cmd->d.cmd_rsp.dev_name), "Failed to %s property value asynchronously, with error %d (%s), by %s", cmd->d.cmd_rsp.cmd_type == SET ? "set" : "get", err, fgcethStrerr(err), cmd->d.cmd_rsp.client_name);
    }
    else
    {
        char value_array[20];

        blkbufCopyStr(value_array, cmd->d.cmd_rsp.value_opts, sizeof(value_array));
        fgcethMsgLog(devGetDevId(cmd->d.cmd_rsp.dev_name), cmd->d.cmd_rsp.cmd_type == SET ? L_V0 : L_V1, "Async command sent by %s (%c %s %s).", cmd->d.cmd_rsp.client_name, cmd->d.cmd_rsp.cmd_type == SET ? 'S' : 'G', cmd->d.cmd_rsp.property, value_array);
    }

    return err;
}



/************************************************************************/
/******   Command configuration   ***************************************/

void fgcethResetCmd(fgceth_rsp * cmd)
{
    CC_ASSERT(cmd != NULL);
    CC_ASSERT(cmd->type == CMD_RSP);

    cmd->d.cmd_rsp.tag[0]      = '\0';
    cmd->d.cmd_rsp.cmd_type    = SET;
    cmd->d.cmd_rsp.dev_name[0] = '\0';
    cmd->d.cmd_rsp.property[0] = '\0';

    blkbufReplaceStr(cmd->d.cmd_rsp.value_opts, "");
    blkbufReplaceStr(cmd->d.cmd_rsp.response,   "");

    cmd->d.cmd_rsp.client_name[0] = '\0';
    cmd->d.cmd_rsp.user_data      = NULL;  

    cmd->d.cmd_rsp.error          = FGC_SUCCESS;
}



void fgcethConfigCmd(fgceth_rsp * cmd, const char * tag, enum fgceth_cmd_type cmd_type, const char * dev_name, const char * property, const char * val_opts)
{
    CC_ASSERT(cmd != NULL);
    CC_ASSERT(cmd->type == CMD_RSP);
    CC_ASSERT(dev_name != NULL);
    CC_ASSERT(property != NULL);

    if(tag != NULL)
    {
        strncpy(cmd->d.cmd_rsp.tag, tag, FGC_MAX_DEV_LEN);
    }
    else
    {
         cmd->d.cmd_rsp.tag[0] = '\0';
    }

    cmd->d.cmd_rsp.cmd_type = cmd_type;
    strncpy(cmd->d.cmd_rsp.dev_name, dev_name, FGC_MAX_DEV_LEN);
    strncpy(cmd->d.cmd_rsp.property, property, FGC_MAX_PROP_LEN);
    blkbufReplaceStr(cmd->d.cmd_rsp.value_opts, val_opts == NULL ? "" : val_opts);
    
    cmd->d.cmd_rsp.tag[FGC_MAX_TAG_LEN]       = '\0';
    cmd->d.cmd_rsp.dev_name[FGC_MAX_DEV_LEN]  = '\0';
    cmd->d.cmd_rsp.property[FGC_MAX_PROP_LEN] = '\0';
}



void fgcethConfigCmdClient(fgceth_rsp * cmd, const char * client_name)
{
    CC_ASSERT(cmd != NULL);
    CC_ASSERT(cmd->type == CMD_RSP);
    CC_ASSERT(client_name != NULL);

    strncpy(cmd->d.cmd_rsp.client_name, client_name, HOST_NAME_MAX);
    cmd->d.cmd_rsp.client_name[HOST_NAME_MAX] = '\0';
}



void fgcethConfigCmdUserData(fgceth_rsp * cmd, void * user_data)
{
    CC_ASSERT(cmd != NULL);
    CC_ASSERT(cmd->type == CMD_RSP);

    cmd->d.cmd_rsp.user_data = user_data;
}



/************************************************************************/
/******   Subscription   ************************************************/

fgceth_errno fgcethSubscribe(uint8_t dev_id, fgceth_rsp_queue* queue, fgceth_sub_handle* handle)
{
    return FGC_SUCCESS;   // To be done
}



fgceth_errno fgcethUnsubscribe(fgceth_sub_handle* handle)
{
    return FGC_SUCCESS;   // To be done
}



/************************************************************************/
/******   Response memory management   **********************************/

fgceth_rsp * fgcethCreateResponse()
{
    fgceth_rsp * ret = rspManagerGetCmdRsp(&fgceth_vars.rsp_man);

    if(ret == NULL)
    {
        fgcethWarningMsgLog(0, "Failed to create response. Unable to get command object (response) from the pool.");
    }

    return ret;
}



fgceth_errno fgcethDestroyResponse(void * ptr)
{
    fgceth_rsp * response = (fgceth_rsp *) ptr;

    if (response == NULL)
    {
        fgcethWarningMsgLog(0, "Failed to destroy response. Received object is NULL.");
        return FGC_ETH_NULL_POINTER;
    }

    return rspManagerReturnRsp(&fgceth_vars.rsp_man, response);
}



/************************************************************************/
/******   Response queue manipulation   *********************************/

fgceth_rsp_queue * fgcethQueueCreate(const uint32_t buffer_length)
{
    fgceth_rsp_queue * queue = new fgceth_rsp_queue; // TODO: Do not dynamically allocate queue, force user to do that task.

    if (queueInit(queue, buffer_length) == FGC_SUCCESS)
    {
        return queue;
    }
    else
    {
        fgcethWarningMsgLog(FGCETH_NO_DEV, "Failed to create queue.");
        return NULL;
    }
}



fgceth_errno fgcethQueueDestroy(fgceth_rsp_queue * queue)
{
    CC_ASSERT(queue != NULL);

    queueFree(queue);

    delete queue; // TODO: Do not dynamically allocate queue, force user to do that task.

    return FGC_SUCCESS;
}



fgceth_rsp * fgcethQueuePop(fgceth_rsp_queue * queue)
{
    CC_ASSERT(queue != NULL);

    fgceth_rsp * ret = queuePop(queue);

    if(ret != NULL && ret->type == CMD_RSP)
    {
        fgcethMsgLog(devGetDevId(ret->d.cmd_rsp.dev_name), ((ret->d.cmd_rsp.cmd_type == SET) || (ret->d.cmd_rsp.error != FGC_SUCCESS)) ? L_V0 : L_V1, "Async response returned from queue, with error %d (%s), to %s (%c %s).", ret->d.cmd_rsp.error, fgcethStrerr(ret->d.cmd_rsp.error), ret->d.cmd_rsp.client_name, ret->d.cmd_rsp.cmd_type == SET ? 'S' : 'G', ret->d.cmd_rsp.property);
    }

    return ret;

}



fgceth_rsp * fgcethQueueBlockPop(fgceth_rsp_queue * queue)
{
    CC_ASSERT(queue != NULL);

    fgceth_rsp * ret = queueBlockPop(queue);

    if(ret != NULL && ret->type == CMD_RSP)
    {
        fgcethMsgLog(devGetDevId(ret->d.cmd_rsp.dev_name), ((ret->d.cmd_rsp.cmd_type == SET) || (ret->d.cmd_rsp.error != FGC_SUCCESS)) ? L_V0 : L_V1, "Async response returned from queue, with error %d (%s), to %s (%c %s).", ret->d.cmd_rsp.error, fgcethStrerr(ret->d.cmd_rsp.error), ret->d.cmd_rsp.client_name, ret->d.cmd_rsp.cmd_type == SET ? 'S' : 'G', ret->d.cmd_rsp.property);
    }

    return ret;
}



fgceth_errno fgcethQueuePush(fgceth_rsp_queue * queue, fgceth_rsp * rsp)
{
    CC_ASSERT(queue != NULL);
    CC_ASSERT(rsp->type == CMD_RSP);

    if (rsp == NULL)
    {
        fgcethWarningMsgLog(FGCETH_NO_DEV, "Failed to push to queue. Received cmd/rsp is NULL");
        return FGC_ETH_NULL_POINTER;
    }

    fgceth_errno err = queuePush(queue, rsp);

    if(err == FGC_SUCCESS)
    {
        char val_buffer[20];
        blkbufCopyStr(val_buffer, rsp->d.cmd_rsp.response, sizeof(val_buffer));

        fgcethMsgLog(devGetDevId(rsp->d.cmd_rsp.dev_name), ((rsp->d.cmd_rsp.cmd_type == SET) || (err != FGC_SUCCESS)) ? L_V0 : L_V1, "Async response directly pushed to queue, with error %d (%s), by %s (%c %s %s).", rsp->d.cmd_rsp.error, fgcethStrerr(rsp->d.cmd_rsp.error), rsp->d.cmd_rsp.client_name, rsp->d.cmd_rsp.cmd_type == SET ? 'S' : 'G', rsp->d.cmd_rsp.property, val_buffer);
    }

    return err;
}



fgceth_errno fgcethQueueBlockPush(fgceth_rsp_queue * queue, fgceth_rsp * rsp)
{
    CC_ASSERT(queue != NULL);

    if (rsp == NULL)
    {
        fgcethWarningMsgLog(FGCETH_NO_DEV, "Failed to push (blocking) to queue. Received cmd/rsp is NULL");
        return FGC_ETH_NULL_POINTER;
    }

    fgceth_errno err = queueBlockPush(queue, rsp);

    if(err == FGC_SUCCESS)
    {
        char val_buffer[20];
        blkbufCopyStr(val_buffer, rsp->d.cmd_rsp.response, sizeof(val_buffer));

        fgcethMsgLog(devGetDevId(rsp->d.cmd_rsp.dev_name), ((rsp->d.cmd_rsp.cmd_type == SET) || (err != FGC_SUCCESS)) ? L_V0 : L_V1, "Async response directly pushed to queue, with error %d (%s), by %s (%c %s %s).", rsp->d.cmd_rsp.error, fgcethStrerr(rsp->d.cmd_rsp.error), rsp->d.cmd_rsp.client_name, rsp->d.cmd_rsp.cmd_type == SET ? 'S' : 'G', rsp->d.cmd_rsp.property, val_buffer);
    }

    return err;
}



fgceth_errno fgcethQueueIsBlocking(fgceth_rsp_queue * queue, bool is_blocking)
{
    CC_ASSERT(queue != NULL);

    queueSetPopBlock(queue, is_blocking);

    return FGC_SUCCESS;
}



/************************************************************************/
/******   Safe library termination and deallocation of resources ********/

fgceth_errno fgcethClose()
{
	fgceth_errno err;

	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Closing...");

	if((err = protocol_close()) != FGC_SUCCESS)
	{
		fgcethInitErrorMsgLog(0, "Failed to close protocol handling, with error %d (%s).", err, fgcethStrerr(err));
		return err;
	}

	hashFree(fgceth_vars.name_hash);
	fgcethInitMsgLog(0, L_V0, "[FGC Ether library] Is closed.");

	return FGC_SUCCESS;
}



/************************************************************************/
/******   Logging functions   *******************************************/

void fgcethSetLogPrintFlags(uint32_t flags)
{
	fgcethSetMsgLogFlags(flags);
}



void fgcethSetLogFileExec(FILE * f)
{
	fgcethSetMsgLogFile(f);
}



void fgcethSetLogFileInit(FILE * f)
{
	fgcethSetMsgLogFileInit(f);
}



void fgcethDetailedLog(bool b)
{
	fgcethDetailedMsgLog(b);
}



void fgcethErrorLog(uint8_t dev_id, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethErrorMsgLog_vaArgs(dev_id, format, args);

	va_end(args);
}



void fgcethWarningLog(uint8_t dev_id, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethWarningMsgLog_vaArgs(dev_id, format, args);

	va_end(args);
}



void fgcethLog(uint8_t dev_id, uint32_t flags, const char * format, ...)
{
	va_list args;
	va_start(args, format);

	fgcethMsgLog_vaArgs(dev_id, flags, format, args);

	va_end(args);
}



/************************************************************************/
/******   Remote terminal functions   ***********************************/

fgceth_errno fgcethRtermSubscribeDev(uint8_t dev_id, fgceth_rsp_queue * queue, fgceth_sub_handle* handle)
{
    return rtermSubscribeDev(dev_id, queue, handle);
}



fgceth_errno fgcethRtermUnsubscribeDev(fgceth_sub_handle* handle)
{
    return rtermUnsubscribeDev(handle);
}



fgceth_errno fgcethRtermSendToDev(uint8_t dev_id, char * buf, uint32_t buf_len)
{
    fgceth_errno err;

    for(uint32_t i = 0; i < buf_len; i++)
    {
        if((err = rtermPushCharToDev(dev_id, buf[i])) != FGC_SUCCESS)
        {
            fgcethWarningMsgLog(0, "Failed to send rterm char to device %d, with error %d (%s).", dev_id, err, fgcethStrerr(err));
            return err;
        }
    }

    return FGC_SUCCESS;
}



fgceth_errno fgcethRtermSendToQueue(fgceth_rsp_queue * queue, char * buf, uint32_t buf_len)
{
    fgceth_errno err;

    err = rtermStringToQueue(queue, buf, buf_len);

    if(err != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(0, "Failed to send rterm string to queue, with error %d (%s).", err, fgcethStrerr(err));
    }

    return err;
}


/************************************************************************/
/******   Status Publication functions   ********************************/

fgceth_errno fgcethGetNextStatusPub(struct fgceth_fgc_stat ** status, struct timeval * time, uint32_t * last_pub_id)
{
    return statPubGetData_nl(status, time, last_pub_id);
}


/************************************************************************/
/******   Timing functions   ********************************************/

void fgcethGetCurrentTime(struct timeval * time)
{
	timer_getCurrentTime(time);
}



uint32_t fgcethGetCurrentTimeMs()
{
    return timer_getCurrentTimeMs();
}



//EOF