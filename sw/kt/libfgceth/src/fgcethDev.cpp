/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethDev.cpp
 * @brief  FGC_Ether device name functions.
 * @author Joao Afonso
 */

#include <inc/fgc_codes_gen.h>

#include <libfgceth/public/fgcethFParser.h>

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethHash.h>
#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethCrc.h>


#include <cclibs.h>
#include <libblkbuf.h>

#include <ctype.h>
#include <string.h>

static uint32_t strCopyToUpper(char * dest, const char * orig)
{
    uint32_t i;

    for(i = 0; i < 128 && orig[i] != '\0'; i++)
    {
        dest[i] = toupper(orig[i]);
    }

    dest[i] = '\0';

    return i;
}

fgceth_errno devCheckDevId(const uint8_t dev_id)
{
    if (!fgc_ether_global_struct::lib_initialized)
    {
        return FGC_ETH_NOT_INITIALIZED;
    }

    if (dev_id == 0 || dev_id > FGC_ETHER_MAX_FGCS)
    {
        return FGC_ETH_WRONG_DEV_ID;
    }

    if (!fgceth_vars.devices[dev_id].declared)
    {
        return FGC_ETH_DEV_NOT_DECLARED;
    }

    return FGC_SUCCESS;
}

uint8_t devGetDevId(const char * dev_name)
{
    struct hash_entry * entry;
    char dev_aux[128];
    uint32_t aux;

    CC_ASSERT(dev_name != NULL);

    if(dev_name[0] == '\0') // No name specified (implicitly FGCD device)
    {
        return 0;
    }
    else if(isdigit(dev_name[0])) // Absolute address
    {
        aux = atoi(dev_name);

        // Check whether the address is invalid

        if(aux < 0 || aux > FGC_ETHER_MAX_FGCS)
        {
            return FGCETH_NO_DEV;
        }

        return aux;
    }

    strCopyToUpper(dev_aux, dev_name);

    entry = hashFind(fgceth_vars.name_hash, dev_aux);

    if(entry == NULL)
    {
        fgcethWarningMsgLog(0, "Failed to get ID from device %s. Device unknown.", dev_name);

        return FGCETH_NO_DEV; // ID not found! Return largest number (it will be recognized as invalid)
    }
    else
    {
        return entry->index;
    }
}


fgceth_errno devInitNames(struct fgceth_names_container * names_container, struct hash_table * name_hash, struct hash_entry * name_entry)
{
    CC_ASSERT(names_container != NULL);
    CC_ASSERT(name_hash != NULL);
    CC_ASSERT(name_entry != NULL);

    // TODO : Put log message in fgceth.cpp

    fgcethInitMsgLog(0, L_V0, "[Device name] Validating device name information...");

    // Initialise the name database and code information to zero

    struct fgc_code_info    code_info;
    struct fgc_name_db      name_db;

    memset(&name_db,    0, sizeof(name_db));
    memset(&code_info,  0, sizeof(code_info));

    // Set name of code

    snprintf(fgceth_vars.codes[0].code_name, sizeof(fgceth_vars.codes[0].code_name), "name");

    // Insert name data for each device

    uint32_t hash_entry_num = 0;

    for(uint32_t i = 0 ; i < FGCETH_ALL_DEV_MAX && i < FGC_CODE_NAMEDB_MAX_DEVS ; i++)
    {
        name_db.dev[i].class_id     = htons(names_container->name_data[i].dev_class);
        name_db.dev[i].omode_mask   = htons(names_container->name_data[i].dev_omode_mask);

        // Check whether a name is defined for the device

        if(names_container->name_data[i].dev_name != NULL)
        {
            strCopyToUpper(names_container->name_data[i].dev_name, names_container->name_data[i].dev_name);

            struct hash_entry * name_entry_ptr = &name_entry[hash_entry_num];

            // Zero + set the key

            name_entry_ptr->index = i; // Save device ID for future hash search results
            memset(name_entry_ptr->key.c, 0, sizeof(name_entry_ptr->key.c));
            strncpy(name_entry_ptr->key.c, names_container->name_data[i].dev_name, FGC_MAX_DEV_LEN);

            if(hashFind(name_hash, name_entry_ptr->key.c))
            {
                fgcethInitErrorMsgLog(0, "Device name validation failed. Name %s is duplicated.", name_entry_ptr->key.c);
                return FGC_ETH_INVALID_DEV_NAME;
            }

            if(isdigit(name_entry_ptr->key.c[0]))
            {
                fgcethInitErrorMsgLog(0, "Device name validation failed. Name %s cannot start with numeric value.", name_entry_ptr->key.c);
                return FGC_ETH_INVALID_DEV_NAME;
            }

            // Check that the class is within the valid range

            if(names_container->name_data[i].dev_class >= FGC_CLASS_UNKNOWN)
            {
                fgcethInitErrorMsgLog(0, "Device name validation failed. Class %d of device %s is unknown.", names_container->name_data[i].dev_class, name_entry_ptr->key.c);
                return FGC_ETH_INVALID_DEV_NAME;
            }

            // Device name is good to go!
            // Insert entry into name hash table

            hashInsert(name_hash, name_entry_ptr);

            // Initializing command queue

            fgceth_errno err;

            if ( (err = queueInit(&fgceth_vars.devices[i].cmd_queue, 512)) != FGC_SUCCESS)
            {
                fgcethInitErrorMsgLog(0, "Device initialization failed. Unable to initialize queue for device %s, with error %d.", name_entry_ptr->key.c, err);
                return err;
            }

            // ***************************************************

            // Set default values

            strncpy(fgceth_vars.devices[i].name, names_container->name_data[i].dev_name, FGC_MAX_DEV_LEN);

            fgceth_vars.devices[i].id                 = i;
            fgceth_vars.devices[i].status_missed      = 1;
            fgceth_vars.devices[i].prev_status_missed = 1;
            fgceth_vars.devices[i].cmd_state          = CMD_OFFLINE;
            fgceth_vars.devices[i].rsp_state          = RSP_IDLE;
            fgceth_vars.devices[i].current_command    = NULL;
            fgceth_vars.devices[i].rsp_expected       = false;
            fgceth_vars.devices[i].fgc_class          = names_container->name_data[i].dev_class;

            // Mark the device as initialized

            fgceth_vars.devices[i].declared        = true;

            strncpy(name_db.dev[i].name, names_container->name_data[i].dev_name, FGC_MAX_DEV_LEN);
            name_db.dev[i].name[FGC_MAX_DEV_LEN] = '\0';
            hash_entry_num++;

            fgcethInitMsgLog(0, L_V0, "[Device name] Device %d (%s) info was successfully registered.", i, names_container->name_data[i].dev_name);
        }
        else // No name is defined
        {
            if(i == 0)
            {
                fgcethInitErrorMsgLog(0, "Gateway (device ID 0) name not found. Gateway name should be on first slot of names container structure.");
                return FGC_ETH_DEV_INIT; // TODO Device class is out of range
            }

            name_db.dev[i].name[0] = '\0';
            fgceth_vars.devices[i].name[0] = '\0';
        }
    }

    if(hash_entry_num <= 1) //1 means only the gateway was detected
    {
        fgcethInitErrorMsgLog(0, "Device initialization failed. No devices were found.");
        return FGC_ETH_DEV_INIT; // TODO No devices extracted from container structure. Lib cannot work.
    }

    // Initialise the name code to zero

    memset(&fgceth_vars.codes[0].code, 0, sizeof(fgceth_vars.codes[0].code));

    // Copy the name database into the code slot

    memcpy(&fgceth_vars.codes[0].code, &name_db, sizeof(name_db));

    fgceth_vars.codes[0].code_class   = code_info.class_id = 0;
    fgceth_vars.codes[0].code_id      = code_info.code_id  = 0;
    fgceth_vars.codes[0].code_version = fgceth_vars.codes[0].code_old_version = 0;
    code_info.version                 = htonl(fgceth_vars.codes[0].code_version);

    // Copy code information to the name code

    memcpy(&fgceth_vars.codes[0].code[(FGC_CODE_NAMEDB_SIZE_BLKS * FGC_CODE_BLK_SIZE) - sizeof(code_info)],
           &code_info, sizeof(code_info));

    // Generate CRC

    fgceth_vars.codes[0].code_length  = FGC_CODE_NAMEDB_SIZE_BLKS * FGC_CODE_BLK_SIZE;
    fgceth_vars.codes[0].code_chksum  = crcCrc16((unsigned char *)&fgceth_vars.codes[0].code, fgceth_vars.codes[0].code_length - sizeof(code_info.crc));
    code_info.crc               = htons(fgceth_vars.codes[0].code_chksum);

    // Copy CRC to the name code

    memcpy(&fgceth_vars.codes[0].code[fgceth_vars.codes[0].code_length - sizeof(code_info.crc)], &code_info.crc, sizeof(code_info.crc));

    // TODO : Put log message in fgceth.cpp

    fgcethInitMsgLog(0, L_V0, "[Device name] Device registering completed, with a total of %d devices identified.", hash_entry_num);

    return FGC_SUCCESS;
}

//EOF
