/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethThreads.cpp
 * @brief  Threads handling.
 * @author Dariusz Zielinski
 */
#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethThreads.h>

#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include <sys/utsname.h>


/**************************************
 * Functions
***************************************/

int threads_detect_rt_preempt()
{
    struct utsname u;
    FILE          *f;
    bool           criteria_1;
    bool           criteria_2 = false;
    bool           rt_linux;

    uname(&u);

    criteria_1 = (strcasestr(u.version, "PREEMPT RT") != NULL);

    if((f = fopen("/sys/kernel/realtime","r")) != NULL)
    {
        int flag;

        criteria_2 = (fscanf(f, "%d", &flag) == 1 && flag == 1);

        if(fclose(f) != 0)
		{
        	fgcethInitErrorMsgLog(0, "Failed to close '/sys/kernel/realtime' file.");
		}
    }

    rt_linux = (criteria_1 && criteria_2);

    if(rt_linux)
    {
        return 1;
    }
    else
    {
    	return 0;
    }
}

int threads_create(pthread_t* thread, fgc_ether_thread_func run_func, void* arg, const uint8_t policy, const uint8_t priority, const uint8_t affinity)
{
    fgcethAssertExp(thread != NULL);
    fgcethAssertExp(run_func != NULL);
    fgcethAssertExp(policy == SCHED_FIFO || policy == SCHED_RR);
    fgcethAssertExp(priority >= 0 && priority <= 99);

    // ***************************************************

    pthread_attr_t attr;

    if ( (errno = pthread_attr_init(&attr)) != 0)
    {
    	fgcethInitErrorMsgLog(0, "Thread creation has failed. Unable to initialize attributes.");
        return errno;
    }

    // ***************************************************

    // Set policy

    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&attr, policy);

    // Set priority

    struct sched_param param;

    sched_getparam(getpid(), &param);
    param.sched_priority = priority;
    pthread_attr_setschedparam(&attr, &param);

    // ***************************************************

    // Create thread

    if ( (errno = pthread_create(thread, &attr, run_func, arg)) != 0)
    {
    	fgcethInitErrorMsgLog(0, "Thread creation has failed. Unable to create thread.");
    	pthread_attr_destroy(&attr);
        return errno;
    }

    pthread_attr_destroy(&attr);

    // ***************************************************

    // Set affinity

    if (affinity > 0)
    {
        cpu_set_t cpu_set;
        CPU_ZERO(&cpu_set);
        CPU_SET(0, &cpu_set);

        if ( (errno = pthread_setaffinity_np(*thread, sizeof(cpu_set), &cpu_set)) )
        {
        	fgcethInitErrorMsgLog(0, "Thread creation has failed. Unable set affinity to CPU 0.");

            return errno;
        }
    }

    return 0;
}



int threads_mutex_init(pthread_mutex_t* mutex)
{
    fgcethAssertExp(mutex != NULL);

    return pthread_mutex_init(mutex, NULL);

}

int threads_cond_init(pthread_cond_t* cond)
{
	fgcethAssertExp(cond != NULL);

	return pthread_cond_init(cond, NULL);

}

int threads_mutex_destroy(pthread_mutex_t* mutex)
{
    fgcethAssertExp(mutex != NULL);

    return pthread_mutex_destroy(mutex);
}

int threads_cond_destroy(pthread_cond_t* cond)
{
	fgcethAssertExp(cond != NULL);

	return pthread_cond_destroy(cond);
}

//EOF
