/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   ether.cpp
 * @brief  Low level Ethernet protocol handling.
 * @author Dariusz Zielinski
 */

#include <libfgceth/private/fgcethEther.h>
#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethStats.h>
#include <libfgceth/private/fgcethLog.h>

#include <netinet/in.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <unistd.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <poll.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>


extern "C"
{
    unsigned int if_nametoindex(__const char* __ifname);
}


/**************************************
 * Local variables
***************************************/

/// Destination MAC address for sending to one FGC

static const uint8_t fgc_ether_unicast_MAC[FGC_ETHER_ADDR_SIZE]   = {0x02, 0x00, 0x00, 0x00, 0x00, 0x00};

/// Destination MAC address for sending to all FGC

static const uint8_t fgc_ether_broadcast_MAC[FGC_ETHER_ADDR_SIZE] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};



/**************************************
 * Local functions
***************************************/

/**
 * Used to free memory in case of error
 *
 * Thread: User
 */
static void ether_cleanUp()
{
	// Pointer to global Ethernet structure

	struct fgc_ether_eth * eth = &fgceth_vars.eth;

	// If socket was opened then close it

    if (eth->socket_handle > -1)
    {
        close(eth->socket_handle);
    }

    // If memory for reception was mapped then unmap it

    if (eth->rx_buffer)
    {
        munmap(eth->rx_buffer, eth->buf_size);
    }
}



/**
 * Entry point for Ethernet thread. Responsible for receiving Ethernet packets using memory map.
 * It checks for errors and checks payload type - base on this it pass the packet to a proper function.
 *
 * Thread: Ethernet
 */
static void* ether_runReception(void* unused)
{
    // Header with meta-data at the beginning of each packet (not Ethernet header)

    struct tpacket_hdr* header;

    // Pointer to global Ethernet structure

    struct fgc_ether_eth * eth = &fgceth_vars.eth;

    // Pointer to current (function-local) head of reception ring buffer

    char*               buffer_head = eth->rx_buffer;

    // Pointer to the end of reception ring buffer

    const char*         buffer_tail = eth->rx_buffer + eth->buf_size - eth->frame_size;

    // ***************************************************

    // Configure structure with options used by poll function

    static struct pollfd poll_opts[2];
    poll_opts[0].fd      = eth->socket_handle;
    poll_opts[0].events  = POLLIN;
    poll_opts[0].revents = 0;
    // Configure pipe required for receiving termination message
    poll_opts[1].fd      = fgceth_vars.reception_thr_pipe[0];
    poll_opts[1].events  = POLLIN;
    poll_opts[1].revents = 0;

    // ***************************************************


    fgcethInitMsgLog(0, L_V2, "[Frame reception thread] Running...");

    // Infinite loop to capture and process packets

    while (!fgceth_vars.closing_protocol)
    {
        // Get pointer to meta-data header in the first available packet in reception ring buffer

        header = reinterpret_cast<tpacket_hdr*>(buffer_head);

        // If packet is still processed by kernel then poll for change (will be in user space when it's ready)

        while ( !(header->tp_status & TP_STATUS_USER) && !(poll_opts[1].revents & POLLIN))
        {
            if (poll(poll_opts, 2, -1) == -1 && errno != EINTR)    // Poll without timeout
            {
                continue;
            }
        }

        fgcethMsgLog(0, L_V4, "[Frame reception thread] Received Ethernet packet at %p.", (void*) header);

        if(poll_opts[1].revents & POLLIN)
        {
        	continue; // Go back to beginning of while loop to re-test while condition (may have changed).
        }

        if(header->tp_status & TP_STATUS_LOSING)
        {
        	fgcethWarningMsgLog(0, "Losing Ethernet frames.");
            fgceth_vars.global_stats.losing_frames++;
        }


        // ***************************************************

        // When frame is in user space (== is ready to be read) then cast it to its structure

        fgc_ether_frame *frame = reinterpret_cast<fgc_ether_frame*>(buffer_head + header->tp_mac);


        fgceth_vars.global_stats.load_received += header->tp_len;
        fgceth_vars.global_stats.eth.rx.all++;

        // ***************************************************

        // Then validate it and process if it's ok

        if (header->tp_len < static_cast<ssize_t>(sizeof(fgc_ether_header_t)))
        {
            // The frame is too small to contain a header.

        	fgcethWarningMsgLog(0, "Ethernet frame has bad size.");
            fgceth_vars.global_stats.eth.rx.bad.all++;
            fgceth_vars.global_stats.eth.rx.bad.size++;
        }
        else if ( frame->header.ethernet.src_addr[5] > FGC_ETHER_MAX_FGCS || memcmp(frame->header.ethernet.src_addr, fgc_ether_unicast_MAC, FGC_ETHER_ADDR_SIZE - 1))
        {
            // Sender address is not FGC one

        	fgcethWarningMsgLog(0, "Ethernet frame has bad address.");
            fgceth_vars.global_stats.eth.rx.bad.all++;
            fgceth_vars.global_stats.eth.rx.bad.address++;
        }
        else if (frame->header.fgc.payload_type >= FGC_ETHER_PAYLOAD_NUM_TYPES)
        {
            // Invalid payload type

        	fgcethWarningMsgLog(0, "Ethernet frame from device %d has bad payload.", frame->header.ethernet.src_addr[5]);
            fgceth_vars.global_stats.eth.rx.bad.all++;
        }
        else
        {
            // Frame is valid - process it

            eth->recv_handler(frame->header.ethernet.src_addr, frame);
        }

        // ***************************************************

        // After processing mark frame status as kernel so it can be used again by kernel

        header->tp_status = TP_STATUS_KERNEL;

        // Advance head to next element in reception ring buffer or reset it when the buffer is at the end

        if(buffer_head == buffer_tail)
        {
        	fgcethMsgLog(0, L_V4, "[Frame reception thread] Ethernet buffer wrap around.");
        }

        buffer_head = ( (buffer_head == buffer_tail) ? eth->rx_buffer : (buffer_head + eth->frame_size));
    }

    fgcethInitMsgLog(0, L_V2, "[Frame reception thread] Received termination signal. Now closing...");

    // Finish thread execution

    fgceth_vars.reception_thr_finished = true;

    pthread_exit((void *)(intptr_t) FGC_SUCCESS);
}



/**************************************
 * Functions
***************************************/

fgceth_errno ether_init(const char* eth_name, const uint32_t buffer_size, fgc_ether_recv_handler recv_handler)
{
	CC_ASSERT(eth_name != NULL);
	CC_ASSERT(buffer_size > 0);
	CC_ASSERT(recv_handler != 0);

	struct fgc_ether_eth * eth = &fgceth_vars.eth;

	eth->recv_handler = recv_handler;

    // ***************************************************

    fgcethInitMsgLog(0, L_V2, "[Ethernet] Enabling Ethernet connection...");

    eth->frame_size    = getpagesize();
    eth->rx_buffer     = NULL;
    eth->socket_handle = -1;
    eth->buf_frames    = buffer_size;
    eth->buf_size      = eth->buf_frames * eth->frame_size;

    // Open a socket for raw packet access with no predefined (default) protocol

    if ((eth->socket_handle = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable to open socket [%s:%d].", __FILE__, __LINE__);
    	ether_cleanUp();
        return FGC_ETH_OPEN_SOCKET;
    }

    // ***************************************************

    // Get index of Ethernet interface (eth_name to corresponding number)

    int eth_index = if_nametoindex(eth_name);

    if (eth_index == 0)
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable to get index of Ethernet interface [%s:%d].", __FILE__, __LINE__);
        ether_cleanUp();
        return FGC_ETH_WRONG_ETHER_NAME;
    }

    // ***************************************************

    // Get MAC address of Ethernet interface

    struct ifreq ifreq;

    strncpy(ifreq.ifr_name, eth_name, sizeof(ifreq.ifr_name) - 1);
    ifreq.ifr_name[sizeof(ifreq.ifr_name) - 1] = '\0';

    if (ioctl(eth->socket_handle, SIOCGIFHWADDR, &ifreq))
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable to get MAC address of Ethernet interface [%s:%d].", __FILE__, __LINE__);
        ether_cleanUp();
        return FGC_ETH_GET_MAC;
    }

    // ***************************************************

    // Initialize frame templates with MAC addresses and EtherType

    memcpy(eth->header_unicast.src_addr,    ifreq.ifr_hwaddr.sa_data,  FGC_ETHER_ADDR_SIZE);
    memcpy(eth->header_broadcast.src_addr,  ifreq.ifr_hwaddr.sa_data,  FGC_ETHER_ADDR_SIZE);
    memcpy(eth->header_unicast.dest_addr,   fgc_ether_unicast_MAC,     FGC_ETHER_ADDR_SIZE);
    memcpy(eth->header_broadcast.dest_addr, fgc_ether_broadcast_MAC,   FGC_ETHER_ADDR_SIZE);

    eth->header_unicast.ether_type   = htons(FGC_ETHER_TYPE);
    eth->header_broadcast.ether_type = htons(FGC_ETHER_TYPE);

    // ***************************************************

    // Binding Ethernet interface index to the socket using link-layer socket descriptor

    memset((void*)&eth->socket_addr, 0, sizeof(eth->socket_addr));
    eth->socket_addr.sll_family     = AF_PACKET;
    eth->socket_addr.sll_protocol   = htons(FGC_ETHER_TYPE);
    eth->socket_addr.sll_ifindex    = eth_index;
    eth->socket_addr.sll_hatype     = ARPHRD_ETHER;
    eth->socket_addr.sll_pkttype    = PACKET_OTHERHOST;
    eth->socket_addr.sll_halen      = ETH_ALEN;

    if (bind(eth->socket_handle, reinterpret_cast<struct sockaddr*>(&eth->socket_addr), sizeof(eth->socket_addr)) < 0)
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable to bind Ethernet interface to socket [%s:%d].", __FILE__, __LINE__);
        ether_cleanUp();
        return FGC_ETH_SOCKET_BIND;
    }

    // ***************************************************

    // Above settings are sufficient for sending packets. Following settings are for receiving.
    // Configuring options used to memory map packet reception

    struct tpacket_req req;
    req.tp_block_size = eth->buf_size;        // Minimal size of contiguous block
    req.tp_block_nr   = 1;                  // Number of blocks
    req.tp_frame_size = eth->frame_size;      // Size of frame
    req.tp_frame_nr   = eth->buf_frames;      // Total number of frames

    if(setsockopt(eth->socket_handle, SOL_PACKET, PACKET_RX_RING, reinterpret_cast<void*>(&req), sizeof(req)))
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable to set socket options [%s:%d].", __FILE__, __LINE__);
        ether_cleanUp();
        return FGC_ETH_SOCKET_OPT;
    }

    // ***************************************************

    // Memory map reception ring buffer and assign it to a pointer

    eth->rx_buffer = reinterpret_cast<char*>(mmap(0, eth->buf_size, PROT_READ | PROT_WRITE, MAP_SHARED, eth->socket_handle, 0));

    if(!eth->rx_buffer)
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable to memory map reception ring buffer [%s:%d].", __FILE__, __LINE__);
        ether_cleanUp();
        return FGC_ETH_MMAP;
    }

    // ***************************************************

    fgcethInitMsgLog(0, L_V2, "[Ethernet] Creating frame reception thread...");

    // Finally, create reception thread and run reception routine

    if (threads_create(&fgceth_vars.reception_thread, ether_runReception, NULL, SCHED_FIFO, FGCETH_THREAD_ETH_PRIO, FGCETH_THREAD_ETH_AFF))
    {
    	fgcethInitErrorMsgLog(0, "Ethernet connection failed. Unable create frame reception thread [%s:%d].", __FILE__, __LINE__);
    	ether_cleanUp();
        return FGC_ETH_THREAD;
    }

    fgcethInitMsgLog(0, L_V2, "[Ethernet] Ethernet connection successfully enabled.");

    return FGC_SUCCESS;
}

#define MAX_BROADCAST_ERR_REPORT 5

fgceth_errno ether_send(const uint8_t dev_id, void* frame, const uint32_t frame_size)
{
	// Pointer to global Ethernet structure
	struct fgc_ether_eth * eth = &fgceth_vars.eth;
	static int broad_cast_warnings = 0;

	fgcethAssertExp(frame != NULL);
    fgcethAssertExp(frame_size > 0);

    fgceth_vars.global_stats.eth.tx.all++;

    // ***************************************************

    fgc_ether_frame* eth_frame = reinterpret_cast<fgc_ether_frame*>(frame);

    if (dev_id == 0)
    {
        eth_frame->header.ethernet = eth->header_broadcast;
    }
    else
    {
        if (dev_id > FGC_ETHER_MAX_FGCS)
        {
        	fgcethWarningMsgLog(0, "Failed to send Ethernet frame to device. Device ID %d is invalid.", dev_id);
            return FGC_ETH_WRONG_DEV_ID;
        }

        eth_frame->header.ethernet = eth->header_unicast;
        eth_frame->header.ethernet.dest_addr[5] = dev_id;
    }

    // ***************************************************

    unsigned int sent_size = static_cast<unsigned int>(sendto(eth->socket_handle, eth_frame, frame_size, 0,
                            reinterpret_cast<struct sockaddr*>(&eth->socket_addr), sizeof(eth->socket_addr)));

    // ***************************************************

    if (sent_size != frame_size)
    {
    	if(dev_id != 0)
    	{
			fgcethWarningMsgLog(dev_id, "Failed to send Ethernet frame to device %d, with error %s.", dev_id, strerror(errno));
			fgceth_vars.global_stats.eth.tx.failed++;
			return FGC_ETH_ETHER_SEND;
    	}
    	else if(dev_id == 0 && broad_cast_warnings <= MAX_BROADCAST_ERR_REPORT)
		{
    		fgcethWarningMsgLog(dev_id, "Failed to broadcast Ethernet frame, with error %s.", strerror(errno));
    		if(broad_cast_warnings == MAX_BROADCAST_ERR_REPORT)
    		{
    			fgcethWarningMsgLog(dev_id, "Ignoring broadcast Ethernet frame warnings.");
    		}
			fgceth_vars.global_stats.eth.tx.failed++;
			broad_cast_warnings++;
			return FGC_ETH_ETHER_SEND;
		}
    }
    else if(dev_id == 0)
    {
    	broad_cast_warnings = 0;
    }

    // ***************************************************

    fgceth_vars.global_stats.load_sent += sent_size;

    return FGC_SUCCESS;
}

//EOF
