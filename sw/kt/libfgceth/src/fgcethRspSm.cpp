/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethRspSm.cpp
 * @brief  Response state machine.
 * @author Dariusz Zielinski
 */

#include <libfgceth/private/fgcethRspSm.h>
#include <libfgceth/private/fgcethProtocol.h>

#include <../utils/test.h>

#include <inc/fgc_errs.h>

#include <stdio.h>
#include <assert.h>


// TODO - Dev - Remove it
void printStateRsp(uint32_t dev_id, char * dev_name, rspsm_states state)
{
    switch (state)
    {
        case RSP_IDLE:       fgcethMsgLog(dev_id, L_V3, "[Rsp SM] RSP_IDLE");        break;
        case RSP_FIRST_PKT:  fgcethMsgLog(dev_id, L_V3, "[Rsp SM] RSP_FIRST_PKT");   break;
        case RSP_MIDDLE_PKT: fgcethMsgLog(dev_id, L_V3, "[Rsp SM] RSP_MIDDLE_PKT");  break;
        case RSP_LAST_PKT:   fgcethMsgLog(dev_id, L_V3, "[Rsp SM] RSP_LAST_PKT");    break;
    };
}



/**************************************
 * Local types & defines
***************************************/

typedef rspsm_states stateFunc(fgc_ether_device* device, fgc_ether_rsp_payload* payload);
typedef stateFunc*   transFunc(fgc_ether_device* device, fgc_ether_rsp_payload* payload);

#define TIME_VAR_LEN sizeof(fgceth_acq_time)



/**************************************
 * Local functions
**************************************/

fgceth_errno processResponse(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
    // Calculate the expected sequence number and update the previous one

    uint8_t exp_rsp_seq  = (device->last_rsp_seq + 1) % (FGC_FIELDBUS_FLAGS_SEQ_MASK + 1);
    device->last_rsp_seq = payload->data.header.flags & FGC_FIELDBUS_FLAGS_SEQ_MASK;

    // ***************************************************

    // Check if the fgcsm state is correct

    if (device->cmd_state != CMD_QUEUED && device->cmd_state != CMD_WAIT_ACK  && device->cmd_state != CMD_WAIT_EXE)
    {
    	fgcethWarningMsgLog(device->id, "Protocol Response SM: Response is not expected (wrong state of the command state machine)");
        device->stats.rsp_not_expected++;
        device->stats.proto_error++;
        device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
    }

    // If this is not the first packet, then check if the sequence number is as expected

    else if ((!(payload->data.header.flags & FGC_FIELDBUS_FLAGS_FIRST_PKT)) && (exp_rsp_seq != device->last_rsp_seq))
    {
    	fgcethWarningMsgLog(device->id, "Protocol Response SM: Response has wrong sequential number");
        device->stats.rsp_wrong_seq_num++;
        device->stats.proto_error++;
        device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
    }
    else
    {
        // Calculate length of the actual payload

        uint16_t len = ntohs(payload->length) - sizeof(payload->data.header);
        uint16_t offset = 0;

        // Check that the response is not longer than the length of the buffer

        if(payload->data.header.flags & FGC_FIELDBUS_FLAGS_FIRST_PKT)
        {
            // Get timestamp

            memcpy((uint8_t *)&device->current_command->d.cmd_rsp.time, payload->data.rsp_pkt, TIME_VAR_LEN);
            len    -= TIME_VAR_LEN;
            offset += TIME_VAR_LEN;

        }

        if(payload->data.header.flags & FGC_FIELDBUS_FLAGS_LAST_PKT)
        {
            // Remove termination character of response ';'

            len--;
        }

        if(blkbufAppendBin(device->current_command->d.cmd_rsp.response, &payload->data.rsp_pkt[offset], len) < len)
        {
            fgcethWarningMsgLog(device->id, "Protocol Response SM: Response buffer is full");
            device->current_command->d.cmd_rsp.error = FGC_RSP_BUF_FULL;
        }

    }

    return device->current_command->d.cmd_rsp.error;
}



/**************************************
 * Response state machine functions forward declarations
***************************************/

// *************** RSP_IDLE
static stateFunc idleFunc;

static transFunc xxToIdle;
static transFunc idleToFirstPkt;


// *************** RSP_FIRST_PKT
static stateFunc firstPktFunc;

static transFunc firstPktToMiddlePkt;
static transFunc firstPktToLastPkt;


// *************** RSP_MIDDLE_PKT
static stateFunc middlePktFunc;

static transFunc middlePktToLastPkt;


// *************** RSP_LAST_PKT
static stateFunc lastPktFunc;


/**************************************
 * Local Variables
***************************************/

// Mapping of the state functions to the states
static stateFunc* const stateToStateFunc[] =
{
    idleFunc,          // IDLE
    firstPktFunc,      // FIRST_PKT
    middlePktFunc,     // MIDDLE_PKT
    lastPktFunc,       // LAST_PKT
};

// Mapping of the state functions to the states
// Note: transitions from LAST_PKT to IDLE is done in the command state machine (fgcsm)
static transFunc* const stateTransitions[][10] =
{
    {xxToIdle,      idleToFirstPkt,                           NULL },  // IDLE
    {xxToIdle,      firstPktToLastPkt,  firstPktToMiddlePkt,  NULL },  // FIRST_PKT
    {xxToIdle,      middlePktToLastPkt,                       NULL },  // MIDDLE_PKT
    {                                                         NULL },  // LAST_PKT - the rspsm is reseted to IDLE by the fgcsm
};

// Mapping of the final state of transitions functions
static const rspsm_states transitions_final_state[][10] =
{
    {RSP_IDLE,      RSP_FIRST_PKT                      },  // IDLE
    {RSP_IDLE,      RSP_LAST_PKT,       RSP_MIDDLE_PKT },  // FIRST_PKT
    {RSP_IDLE,      RSP_LAST_PKT                       },  // MIDDLE_PKT
    {                                                  },  // LAST_PKT
};

// Cascade mapping: decides when to cascade based on a previous state and a new state.
static const bool state_cascade[7][7] =
{
    //   From (old state) -->
    //   IDLE     FIRST_PKT  MIDDLE_PKT  LAST_PKT
    {      0,       true,      true,          0,   },  //    IDLE
    {      0,          0,         0,          0,   },  //    FIRST_PKT
    {      0,          0,         0,          0,   },  //    MIDDLE_PKT
    {      0,       true,      true,          0,   },  //    LAST_PKT
};                                                     // ^^ To (new state) ^^


/**************************************
 * RSP_IDLE
***************************************/

static rspsm_states idleFunc(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
    // Check if the response was received when it was expected

    if (!device->rsp_expected)
    {
    	fgcethWarningMsgLog(device->id, "Protocol Response SM: Response is not expected in IDLE state");
        device->stats.rsp_not_expected++;
    }

    return RSP_IDLE;
}



static stateFunc* xxToIdle(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Trans] xxToIdle");

    // Make sure that there is some active command

    if (device->current_command == NULL)
    {
        // Protocol error: response not expected

    	fgcethWarningMsgLog(device->id, "Protocol Response SM: Response is not expected (no active command)");
        device->stats.rsp_not_expected++;
        device->stats.proto_error++;

        return idleFunc;
    }

    // If there was an error during processing of the command then go to IDLE

    else if (device->current_command->d.cmd_rsp.error != FGC_SUCCESS)
    {
        return idleFunc;
    }

    return NULL;
}



static stateFunc* idleToFirstPkt(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Trans] idleToFirstPkt");

    // If response is expected then process it. If the command has an error then ignore this
    // response (it means that this is some middle response, but there was an error in the previous response)

    if (device->rsp_expected && device->current_command->d.cmd_rsp.error == FGC_SUCCESS)
    {
        return firstPktFunc;
    }

    return NULL;
}



/**************************************
 * RSP_FIRST_PKT
***************************************/

static rspsm_states firstPktFunc(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Func] firstPktFunc");

    // Check if this packet is really the first one

    if (!(payload->data.header.flags & FGC_FIELDBUS_FLAGS_FIRST_PKT))
    {
    	fgcethWarningMsgLog(device->id, "Protocol Response SM: The first packet has no FIRST_PKT flag set");
        device->stats.rsp_wrong_first++;
        device->stats.proto_error++;
        device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
    }

    // Process the response if this is a multi-packet response. For a single-packet response
    // the data will be copied to the response buffer in the lastPktFunc()

    if (!(payload->data.header.flags & FGC_FIELDBUS_FLAGS_LAST_PKT))
    {
        processResponse(device, payload);
    }

    return RSP_FIRST_PKT;
}



static stateFunc* firstPktToLastPkt(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Trans] firstPktToLastPkt");

    // If this is the last packet then go to the LAST_PKT state

    if (payload->data.header.flags & FGC_FIELDBUS_FLAGS_LAST_PKT)
    {
        return lastPktFunc;
    }

    return NULL;
}



static stateFunc* firstPktToMiddlePkt(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Trans] firstPktToMiddlePkt");

    return middlePktFunc;
}



/**************************************
 * RSP_MIDDLE_PKT
***************************************/

static rspsm_states middlePktFunc(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Func] middlePktFunc");
    processResponse(device, payload);

    return RSP_MIDDLE_PKT;
}



static stateFunc* middlePktToLastPkt(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Trans] middlePktToLastPkt");

    // If this is the last packet then go to the LAST_PKT state

    if (payload->data.header.flags & FGC_FIELDBUS_FLAGS_LAST_PKT)
    {
        return lastPktFunc;
    }

    return NULL;
}



/**************************************
 * RSP_LAST_PKT
***************************************/

static rspsm_states lastPktFunc(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
	fgcethMsgLog(device->id, L_V3, "[Rsp SM Func] lastPktFunc");

    // Process the response and if it is successful then finish the processing

    if (processResponse(device, payload) == FGC_SUCCESS)
    {
        fgceth_rsp * current_command = device->current_command;

        // ***************************************************

        // Remove semicolon from the response value and replace it with NULL character

        // ***************************************************

        // Get timestamp

        // Byte swap timestamp

        current_command->d.cmd_rsp.time.tv_sec  = ntohl(current_command->d.cmd_rsp.time.tv_sec);
        current_command->d.cmd_rsp.time.tv_usec = ntohl(current_command->d.cmd_rsp.time.tv_usec);

        // Decrease response length (compensate for time stamp)

        //current_command->value_length -= TIME_VAR_LEN;
    }

    // Response is not longer expected

    device->rsp_expected = false;

    return RSP_LAST_PKT;
}



/**************************************
 * Functions
***************************************/

void rspsm_smUpdate(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
    fgcethAssertExp(device  != NULL);
    fgcethAssertExp(payload != NULL);

    // First pass of the loop?

    bool first_run = true;

    while (1)
    {
        // Reset cascade variable

        bool cascade = false;

        // Invoke transition functions

        transFunc* trans     = NULL;
        stateFunc* next_state = NULL;

        for (int i = 0; (trans = stateTransitions[device->rsp_state][i]) != NULL; ++i)
        {
            // Invoke all transition function in first pass or only allowed ones in next passes

            if (first_run || state_cascade[transitions_final_state[device->rsp_state][i]][device->rsp_state])
                next_state = trans(device, payload);

            // If next_state is different then change the state and invoke state function of the new state

            if (next_state != NULL)
            {
                // Invoke new state function and change the state

                device->rsp_state = next_state(device, payload);

                // Cascade

                cascade = true;
                printStateRsp(device->id, device->name, device->rsp_state);

                break;
            }
        }

        if (first_run && next_state == NULL)
        {
            // Invoke state function

            stateToStateFunc[device->rsp_state](device, payload);
        }
        else if (!cascade) // If there was a transition and the cascade flag was set then run the loop again
        {
            break;
        }

        first_run = false;
    }
}

//EOF
