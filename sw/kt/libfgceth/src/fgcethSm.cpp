/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcsm.cpp
 * @brief  Main command state machine.
 * @author Dariusz Zielinski
 */

#include <libfgceth/private/fgcethSm.h>
#include <libfgceth/private/fgcethProtocol.h>
#include <libfgceth/private/fgcethTimer.h>
#include <libfgceth/private/fgcethLog.h>

#include <../utils/test.h>

#include <inc/fgc_errs.h>

#include <stdio.h>
#include <assert.h>

bool done              = false;

// TODO - Dev - Remove it
void printState(uint32_t dev_id, char * dev_name, fgcsm_states state)
{

    switch (state)
	{
		case CMD_OFFLINE:   fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_OFFLINE" );  break;
		case CMD_IN_BOOT:   fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_IN_BOOT" );  break;
		case CMD_IDLE:      fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_IDLE"    );  break;
		case CMD_QUEUED:    fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_QUEUED"  );  break;
		case CMD_WAIT_ACK:  fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_WAIT_ACK");  break;
		case CMD_WAIT_EXE:  fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_WAIT_EXE");  break;
		case CMD_TIMEOUT:   fgcethMsgLog(dev_id, L_V3, "[Cmd SM] CMD_TIMEOUT" );  break;
	};

}



/**************************************
 * Local types
***************************************/

typedef fgcsm_states stateFunc(fgc_ether_device* device);
typedef stateFunc*   transFunc(fgc_ether_device* device);


/**************************************
 * Command state machine functions forward declarations
***************************************/

// *************** CMD_OFFLINE
static stateFunc offlineFunc;

static transFunc offlineToIdle;
static transFunc offlineToInBoot;

// *************** CMD_IN_BOOT
static stateFunc inBootFunc;

static transFunc inBootToIdle;


// *************** CMD_IDLE
static stateFunc idleFunc;

static transFunc xxToIdle;
static transFunc idleToInBoot;
static transFunc idleToQueued;


// *************** CMD_QUEUED
static stateFunc queuedFunc;

static transFunc queuedToWaitAck;


// *************** CMD_WAIT_ACK
static stateFunc waitAckFunc;

static transFunc waitAckToQueued;
static transFunc waitAckToWaitExe;


// *************** CMD_WAIT_ACK
static stateFunc waitExeFunc;

static transFunc waitExeToIdle;


// *************** CMD_TIMEOUT
static stateFunc timeoutFunc;

static transFunc xxToTimeout;


/**************************************
 * Local Variables
***************************************/

// Mapping of the state functions to the states
static stateFunc* const stateToStateFunc[] =
{
    offlineFunc,         // OFFLINE
    inBootFunc,          // IN_BOOT
    idleFunc,            // IDLE
    queuedFunc,          // QUEUED
    waitAckFunc,         // WAIT_ACK
    waitExeFunc,         // WAIT_EXE
    timeoutFunc          // TIMEOUT
};

// Mapping of the state functions to the states
static transFunc* const stateTransitions[][10] =
{
    {                            offlineToIdle,    offlineToInBoot,                  NULL }, // OFFLINE
    {                            inBootToIdle,                                       NULL }, // IN_BOOT
    {xxToTimeout,                idleToInBoot,    idleToQueued,                      NULL }, // IDLE
    {xxToTimeout,   xxToIdle,    queuedToWaitAck,                                    NULL }, // QUEUED
    {xxToTimeout,   xxToIdle,    waitAckToQueued, waitAckToWaitExe,                  NULL }, // WAIT_ACK
    {xxToTimeout,   xxToIdle,    waitExeToIdle,                                      NULL }, // WAIT_EXE
    {               xxToIdle,                                                        NULL }  // TIMEOUT
};

// Mapping of the final state of transitions functions
static const fgcsm_states transitions_final_state[][10] =
{
    {                           CMD_IDLE,        CMD_IN_BOOT                   }, // OFFLINE
    {CMD_TIMEOUT,               CMD_IDLE                                       }, // IN_BOOT
    {CMD_TIMEOUT,               CMD_IN_BOOT,     CMD_QUEUED                    }, // IDLE
    {CMD_TIMEOUT,  CMD_IDLE,    CMD_WAIT_ACK                                   }, // QUEUED
    {CMD_TIMEOUT,  CMD_IDLE,    CMD_QUEUED,      CMD_WAIT_EXE                  }, // WAIT_ACK
    {CMD_TIMEOUT,  CMD_IDLE,    CMD_IDLE,                                      }, // WAIT_EXE
    {              CMD_IDLE                                                    }  // TIMEOUT
};

// Cascade mapping: decides when to cascade based on a previous state and a new state.
static const bool state_cascade[8][8] =
{
    //   From (old state) -->
    //   OFFLINE   IN_BOOT     IDLE      QUEUED    WAIT_ACK   WAIT_EXEC   TIMEOUT
    {        0,            0,         0,         0,         0,         0,         0    }, //    OFFLINE
    {        true,         0,      true,         0,         0,         0,         0    }, //    IN_BOOT
    {        true,      true,         0,         0,         0,      true,      true    }, //    IDLE
    {        0,            0,      true,         0,      true,         0,         0    }, //    QUEUED
    {        0,            0,         0,         0,         0,         0,         0    }, //    WAIT_ACK
    {        0,            0,         0,         0,      true,         0,         0    }, //    WAIT_EXE
    {        0,            0,      true,      true,      true,      true,         0    }  //    TIMEOUT
};                                                                                                 // ^^ To (new state) ^^



/**************************************
 * CMD_OFFLINE
***************************************/

static fgcsm_states offlineFunc(fgc_ether_device* device)
{
    if (!done) fgcethMsgLog(device->id, L_V3, "[Cmd SM Func] offlineFunc");

    return CMD_OFFLINE;
}


static stateFunc* offlineToIdle(fgc_ether_device* device)
{
    if (!done) fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] offlineToIdle");

    if ( (device->status_missed == 0) && (device->class_id % 10 != 0) )
    {
    	fgcethMsgLog(device->id, L_V0, "Detected a transition from offline state to idle.");
        return idleFunc;
    }

    return NULL;
}



static stateFunc* offlineToInBoot(fgc_ether_device* device)
{
    if (!done) fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] offlineToInBoot");

    if ( (device->status_missed == 0) && (device->class_id % 10 == 0) )
    {
    	fgcethMsgLog(device->id, L_V0, "Detected a transition from offline state to boot.");
        return inBootFunc;
    }

    return NULL;
}


/**************************************
 * CMD_IN_BOOT
***************************************/

static fgcsm_states inBootFunc(fgc_ether_device* device)
{
    protocol_finishAllCommands(device, FGC_DEV_NOT_READY);

    return CMD_IN_BOOT;
}



static stateFunc* inBootToIdle(fgc_ether_device* device)
{
    if ( (device->pll) && (device->class_id % 10 != 0) )
    {
    	fgcethMsgLog(device->id, L_V0, "Detected a transition from boot state to idle.");
        return idleFunc;
    }

    return NULL;
}



/**************************************
 * CMD_IDLE
***************************************/

static fgcsm_states idleFunc(fgc_ether_device* device)
{
    if (!done) fgcethMsgLog(device->id, L_V3, "[Cmd SM Func] idleFunc");

    // If there is an active command then finish processing of this command

    if (device->current_command != NULL)
    {
        protocol_finishCurrentCommand(device);
    }

    // If the command queue is not empty then the first command will be taken from
    // the command queue (otherwise it will be NULL)

    device->current_command = queuePop(&device->cmd_queue);

    CC_ASSERT(device->current_command == NULL || device->current_command->type == CMD_RSP);

    return CMD_IDLE;
}



static stateFunc* xxToIdle(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] xxToIdle");

    if (device->current_command == NULL)
    {
        return idleFunc;
    }
    else
    if (device->current_command->d.cmd_rsp.error != FGC_SUCCESS)
    {
        return idleFunc;
    }

    return NULL;
}



static stateFunc* idleToInBoot(fgc_ether_device* device)
{
    if (!done) fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] idleToInBoot");

    if ( (device->pll) && (device->class_id % 10 == 0) )
    {
        return inBootFunc;
    }

    return NULL;
}



static stateFunc* idleToQueued(fgc_ether_device* device)
{
    if (!done) fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] idleToQueued");

    if (device->current_command != NULL)
    {
        return queuedFunc;
    }

    return NULL;
}



/**************************************
 * CMD_QUEUED
***************************************/

static fgcsm_states queuedFunc(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Func] queuedFunc");

    // Reset reception state machine

    device->rsp_expected = true;
    device->rsp_state    = RSP_IDLE;

    // Mark that the current command is not yet processed

    device->cmd_finished = false;

    // Reset toggle missed counter

    device->ack_toggle_missed = 0;

    // Send command

    char log_buffer[20];

    blkbufCopyStr(log_buffer, device->current_command->d.cmd_rsp.value_opts, sizeof(log_buffer));

    fgcethMsgLog(device->id, L_V3, "SENDING ETH PACKET: |%s %s|", device->current_command->d.cmd_rsp.property, log_buffer);
    device->current_command->d.cmd_rsp.error = protocol_sendCommand(device->current_command);

    return CMD_QUEUED;
}



static stateFunc* queuedToWaitAck(fgc_ether_device *device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] queuedToWaitAck");

    return waitAckFunc;
}



/**************************************
 * CMD_WAIT_ACK
***************************************/

static fgcsm_states waitAckFunc(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Func] waitAckFunc");

    // If acknowledgment bit was missed then increase counter

    if ((device->last_ack & FGC_ACK_CMD_TOG) == (device->ack & FGC_ACK_CMD_TOG))
    {
    	fgcethWarningMsgLog(device->id, "Protocol SM: Acknowledge bit was missed in WAIT_ACK state");
        device->stats.ack_miss++;

        // Increase toggle missed counter

        device->ack_toggle_missed++;

        // If there was no acknowledge bit change two times then this is an warning

        if (device->ack_toggle_missed >= 2)
        {
        	fgcethWarningMsgLog(device->id, "Protocl SM: There was no acknowledge bit change two times in WAIT_ACK state");
            device->stats.proto_error++;
            device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
        }
    }
    else
    {
        // Otherwise reset the counter

        device->ack_toggle_missed = 0;

        // If there are more packets to send then check that the FGC state is correct

        if (device->current_command->d.cmd_rsp.hidden_members_.cmd_step != END)
        {
            // FGC state should be RECEIVING - if not it is an error

            if (device->status_state != FGC_RECEIVING)
            {
                device->current_command->d.cmd_rsp.error = (fgceth_errno) (device->status_state < FGC_NUM_ERRS ? device->status_state : FGC_UNKNOWN_ERROR_CODE);

                // If the error is FGC_OK_NO_RSP, FGC_OK_RSP or FGC_EXECUTING then it is a protocol error

                if (device->current_command->d.cmd_rsp.error <= FGC_EXECUTING)
                {
                    device->stats.proto_error++;
                    device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
                    fgcethWarningMsgLog(device->id, "Protocol SM: Wrong FGC state while receiving command packets");
                }
            }
        }
    }

    return CMD_WAIT_ACK;
}



static stateFunc* waitAckToQueued(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] waitAckToQueued");

    // If there are more packets to send then go back to the sending

    if (device->ack_toggle_missed == 0 && device->current_command->d.cmd_rsp.hidden_members_.cmd_step != END)
    {
        return queuedFunc;
    }

    return NULL;
}



static stateFunc* waitAckToWaitExe(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] waitAckToWaitExe");

    // If the acknowledge bit changed then wait for an execution

    if ( device->ack_toggle_missed == 0 && device->current_command->d.cmd_rsp.hidden_members_.cmd_step == END)
    {
        return waitExeFunc;
    }

    return NULL;
}



/**************************************
 * CMD_WAIT_EXE
***************************************/

static fgcsm_states waitExeFunc(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Func] waitExeFunc %d", device->status_state);

    // Check if command error was not set by the reception state machine
    // And if so that means there was an error in the response reception.

    if (device->current_command->d.cmd_rsp.error == FGC_SUCCESS)
    {
        // Check command state received in status packet.
        // If it's not executing then set error code (result) and push current command to its response queue.

        switch (device->status_state)
        {
            case FGC_EXECUTING:
                return CMD_WAIT_EXE;

            case FGC_RECEIVING:
            	fgcethWarningMsgLog(device->id, "Protocol SM: FGC state is FGC_RECEIVING which is not expected state for WAIT_EXE state");
                device->stats.proto_error++;
                device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
            break;

            case FGC_OK_NO_RSP:
                device->current_command->d.cmd_rsp.error = FGC_SUCCESS;
            break;

            case FGC_OK_RSP:

                // Cross-check that response state machine has received a full response

                if (device->rsp_state == RSP_LAST_PKT)
                {
                    device->current_command->d.cmd_rsp.error = FGC_SUCCESS;

                    // Check whether value is binary (first byte is 0xFF)

                    uint8_t first_value = 0;

                    blkbufCopyBin(&first_value, device->current_command->d.cmd_rsp.value_opts, 1);

                    if (first_value == 0xFF)
                    {
                        // Check that value is long enough to include a length

                        if (blkbufDataLen(device->current_command->d.cmd_rsp.value_opts) < 1 + sizeof(uint32_t))
                        {
                            // Value is too short

                        	fgcethWarningMsgLog(device->id, "Protocol SM: Binary value too short to include header");
                            device->stats.proto_error++;
                            device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
                        }
                        else
                        {
                            uint32_t val;
                            uint8_t  val_bin[16];

                            // Read the length

                            blkbufCopyBin(val_bin, device->current_command->d.cmd_rsp.value_opts, sizeof(val_bin));
                            memcpy(&val, &val_bin[1], sizeof(uint32_t));

                            val = htonl(val);

                            // Check that value matches claimed length

                            if(blkbufDataLen(device->current_command->d.cmd_rsp.value_opts) != (1 + sizeof(uint32_t) + val))
                            {
                            	fgcethWarningMsgLog(device->id, "Protocol SM: Binary value length does not match expected");
                                device->stats.proto_error++;
                                device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
                            }
                        }
                    }
                }
                else
                {
                	fgcethWarningMsgLog(device->id, "Protocol SM: Response is incomplete");
                    device->stats.proto_error++;
                    device->current_command->d.cmd_rsp.error = FGC_PROTO_ERROR;
                }
            break;

            default:
                device->current_command->d.cmd_rsp.error = (fgceth_errno) (device->status_state < FGC_NUM_ERRS ? device->status_state : FGC_UNKNOWN_ERROR_CODE);
        }
    }

    device->cmd_finished = true;

    return CMD_WAIT_EXE;
}



static stateFunc* waitExeToIdle(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Trans] waitExeToIdle");

    if (device->cmd_finished)
    {
        return idleFunc;
    }

    return NULL;
}



/**************************************
 * CMD_TIMEOUT
***************************************/

static fgcsm_states timeoutFunc(fgc_ether_device* device)
{
	fgcethMsgLog(device->id, L_V3, "[Cmd SM Func] timeoutFunc %d", device->status_state);

    device->current_command->d.cmd_rsp.error = FGC_ETH_TIMEOUT;
    device->stats.proto_error++;

    return CMD_TIMEOUT;
}



static stateFunc* xxToTimeout(fgc_ether_device* device)
{
    //devPrint(CPURPLE "C  xxToTimeout \n" CN);

    if (device->current_command != NULL)
    {
        if ((timer_getCurrentTimeMs() - device->current_command->d.cmd_rsp.hidden_members_.timeout_timestamp) >= fgceth_vars.timeout)
        {
            return timeoutFunc;
        }
    }

    return NULL;
}



/**************************************
 * Functions
***************************************/

void fgcsm_smUpdate(fgc_ether_device* device)
{

    fgcethAssertExp(device != NULL);

    done = true;
    if (!done)
    {
    	fgcethMsgLog(device->id, L_V3, "________________________________");
        //done = true;
    }

    // First pass of the loop?

    bool first_run = true;

    while (1)
    {

        // Reset cascade variable

        bool cascade = false;

        // Invoke transition functions

        transFunc* trans     = NULL;
        stateFunc* next_state = NULL;

        for (int i = 0; (trans = stateTransitions[device->cmd_state][i]) != NULL; ++i)
        {
            // Invoke all transition function in first pass or only allowed ones in next passes

            if (first_run || state_cascade[transitions_final_state[device->cmd_state][i]][device->cmd_state])
            {
                next_state = trans(device);
            }

            // If next_state is different then change the state and invoke state function of the new state

            if (next_state != NULL)
            {
                // Invoke new state function and change the state

                device->cmd_state = next_state(device);
                done = false;

                // Cascade

                cascade = true;
                printState(device->id, device->name, device->cmd_state);      // TODO - Dev - Remove it
                break;
            }
        }

        if (first_run && next_state == NULL)
        {
            // Invoke current state function

            stateToStateFunc[device->cmd_state](device);

            break;
        }
        else if (!cascade) // If there was a transition and the cascade flag was set then run the loop again
        {
            break;
        }
        else
        {
        	fgcethMsgLog(device->id, L_V3, "--------------------------------");//devPrint(CN "--------------------------------\n");
        }

        first_run = false;
    }
}

//EOF
