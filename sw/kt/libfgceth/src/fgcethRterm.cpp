

#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethCharq.h>
#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethRterm.h>

#include <string.h>


fgceth_errno rtermInit(struct fgceth_rsp_man * rsp_manager)
{
    CC_ASSERT(rsp_manager != NULL);

    fgceth_vars.rterm_rsp_man = rsp_manager;

    // TODO : Remove to fgceth.cpp

    fgcethInitMsgLog(0, L_V2, "[Rterm] Enabling rterm responses...");

    // Data initialization

    if(threads_mutex_init(&fgceth_vars.rterm_mutex))
    {
        fgcethInitErrorMsgLog(0, "Failed to initialize rterm global mutex.");
        return FGC_ETH_MUTEX_INIT;
    }

    fgceth_errno err;

    if ( (err = queueInit(&fgceth_vars.q_link_pool, FGC_TCP_MAX_CLIENTS * (FGC_ETHER_MAX_FGCS + 1))) != FGC_SUCCESS)
    {
        fgcethInitErrorMsgLog(0, "Failed to initialize rterm link pool. Queue initialization failed at %s:%d.", __FILE__, __LINE__);
        return err;
    }

    // Initialize response pool

    for (uint32_t i = 0; i < FGC_TCP_MAX_CLIENTS * (FGC_ETHER_MAX_FGCS + 1); i++)
    {
        fgc_q_link * new_link = (fgc_q_link *) calloc (1, sizeof(fgc_q_link));

        if(new_link == NULL)
        {
            fgcethInitErrorMsgLog(0, "Failed to initialize rterm link pool. Queue initialization link allocation failed.");
            return FGC_ETH_NULL_POINTER;
        }

        new_link->next  = NULL;
        new_link->rsp_q = NULL;

        queuePush(&fgceth_vars.q_link_pool, new_link);
    }

    for (uint32_t i = 0; i <= FGC_ETHER_MAX_FGCS; ++i)
    {
        charqInit(&fgceth_vars.devices[i].rterm_char_q, FGCETH_DEV_CHARQ_SIZE);
        fgceth_vars.devices[i].rterm_rsp_q_list = NULL;
    }

    // TODO : Remove to fgceth.cpp

    fgcethInitMsgLog(0, L_V2, "[Rterm] Successfully enabled.");

    return FGC_SUCCESS;
}



fgceth_errno rtermSubscribeDev(const uint8_t dev_id, fgceth_rsp_queue * queue, fgceth_sub_handle* handle)
{
    CC_ASSERT(queue != NULL);
    CC_ASSERT(handle != NULL);

    struct fgc_q_link * new_link = queuePop(&fgceth_vars.q_link_pool);

    if (new_link == NULL)
    {
        fgcethWarningMsgLog(dev_id, "Failed to subscribe to remote terminal. Unable to get link object from the pool.");
        return FGC_ETH_NULL_POINTER;
    }

    new_link->handle = handle;
    new_link->rsp_q  = queue;
    new_link->next   = fgceth_vars.devices[dev_id].rterm_rsp_q_list;
    fgceth_vars.devices[dev_id].rterm_rsp_q_list = new_link; // This insertion should be thread safe

    return FGC_SUCCESS;
}


fgceth_errno rtermUnsubscribeDev(fgceth_sub_handle* handle)
{
    for(uint32_t i = 0; i <= FGC_ETHER_MAX_FGCS; i++)
    {
        struct fgc_q_link * link = fgceth_vars.devices[i].rterm_rsp_q_list;
        struct fgc_q_link * prev_link = NULL;

        while(link != NULL)
        {
            if(link->handle == handle) // Remove item
            {

                pthread_mutex_lock(&fgceth_vars.rterm_mutex);

                if(prev_link == NULL)
                {
                    fgceth_vars.devices[i].rterm_rsp_q_list = link->next;
                }
                else
                {
                    prev_link->next = link->next;
                }

                struct fgc_q_link * del_link = link;
                link = link->next;

                pthread_mutex_unlock(&fgceth_vars.rterm_mutex);

                fgceth_errno err;

                if((err = queuePush(&fgceth_vars.q_link_pool, del_link)) != FGC_SUCCESS)
                {
                    return err;
                }
            }
            else
            {
                prev_link = link;
                link = link->next;
            }

        }

    }

    return FGC_SUCCESS;
}



fgceth_errno rtermPushCharToDev(const uint8_t dev_id, const char input_char)
{
	return charqPush(&fgceth_vars.devices[dev_id].rterm_char_q, input_char);
}



// Has to be casted to unsigned 8 bits
int32_t      rtermPopCharToDev(const uint8_t dev_id)
{
	return charqPop(&fgceth_vars.devices[dev_id].rterm_char_q);
}



fgceth_errno rtermPublishToQueues(const uint8_t dev_id, const char * buf, uint32_t buf_len)
{
    pthread_mutex_lock(&fgceth_vars.rterm_mutex);

    fgc_q_link * link = fgceth_vars.devices[dev_id].rterm_rsp_q_list;

    while(link != NULL)
    {
        fgceth_rsp * rterm_rsp = rspManagerGetRtermRsp(fgceth_vars.rterm_rsp_man);

        if (rterm_rsp == NULL)
        {
            pthread_mutex_unlock(&fgceth_vars.rterm_mutex);
            fgcethWarningMsgLog(dev_id, "Failed to publish remote terminal. Unable to get rterm response object from the pool.");
            return FGC_ETH_NULL_POINTER;
        }

        uint32_t rsp_len = buf_len < sizeof(rterm_rsp->d.rterm.buffer) ? buf_len : sizeof(rterm_rsp->d.rterm.buffer) - 1;

        memcpy(rterm_rsp->d.rterm.buffer, buf, rsp_len);

        rterm_rsp->d.rterm.buffer[rsp_len] = '\0';
        rterm_rsp->d.rterm.length          = rsp_len;
        rterm_rsp->d.rterm.dev_id          = dev_id;

        if(queuePush(link->rsp_q, rterm_rsp) != FGC_SUCCESS)
        {
            fgcethWarningMsgLog(dev_id, "Failed to publish remote terminal. Client queue is full.");
        }

        link = link->next;
    }

    pthread_mutex_unlock(&fgceth_vars.rterm_mutex);

    return FGC_SUCCESS;
}



fgceth_errno rtermStringToQueue(fgceth_rsp_queue * queue, char * buf, uint32_t buf_len)
{
    CC_ASSERT(queue != NULL);
    CC_ASSERT(buf != NULL);

    fgceth_rsp * rterm_rsp = rspManagerGetRtermRsp(fgceth_vars.rterm_rsp_man);

    if (rterm_rsp == NULL)
    {
        fgcethWarningMsgLog(0, "Failed to create remote terminal response. Unable to get rterm response object from the pool.");
        return FGC_ETH_NULL_POINTER;
    }

    uint32_t rsp_len = buf_len < sizeof(rterm_rsp->d.rterm.buffer) ? buf_len : sizeof(rterm_rsp->d.rterm.buffer) - 1;

    memcpy(rterm_rsp->d.rterm.buffer, buf, rsp_len);
    rterm_rsp->d.rterm.buffer[rsp_len] = '\0';
    rterm_rsp->d.rterm.length          = rsp_len;

    fgceth_errno err;

    if((err = queuePush(queue, rterm_rsp)) != FGC_SUCCESS)
    {
        fgcethWarningMsgLog(FGCETH_NO_DEV, "Failed to publish remote terminal. Client queue is full.");
    }

    return err;
}



void rtermClose()
{
    fgc_q_link * link;

    while((link = queuePop(&fgceth_vars.q_link_pool)) != NULL)
    {
        free(link);
    }

    queueFree(&fgceth_vars.q_link_pool);

    for (uint32_t i = 0; i <= FGC_ETHER_MAX_FGCS; i++)
    {
        charqFree(&fgceth_vars.devices[i].rterm_char_q);
    }
}

// EOF
