/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethTest.cpp
 * @brief  Utilities.
 * @author Dariusz Zielinski
 */

#include <libfgceth.h>
#include <libfgceth/private/fgcethProtocol.h>
#include <libfgceth/private/fgcethLog.h>

#include <stdio.h>
#include <ctype.h>
#include <regex.h>
#include <getopt.h>


/**************************************
 * Functions
***************************************/

void fgcethAssertFunc(const fgceth_errno error, const char* file, const int line)
{
    if (error != FGC_SUCCESS)
    {
        fgcethErrorMsgLog(FGCETH_NO_DEV, "%s [%s:%d]", fgcethStrerr(error), file, line);
        exit(1);
    }
}

void fgcethAssertFuncExp(int expression, const char * exp_string, const char * file, const char * func, const int line)
{
	if (!expression)
	{
	    fgcethErrorMsgLog(FGCETH_NO_DEV, "Assertion '%s' failed [%s:%d: %s]", exp_string, file, line, func);
	    exit(1);
	}
}

//EOF
