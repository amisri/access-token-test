/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   fgcethProtocol.cpp
 * @brief  FGC_Ether Protocol handling.
 * @author Dariusz Zielinski
 * @author Joao Afonso
 */

#include <libfgceth/private/fgcethProtocol.h>
#include <libfgceth/private/fgcethTypesInternal.h>
#include <libfgceth/private/fgcethCommand.h>
#include <libfgceth/private/fgcethConsts.h>
#include <libfgceth/private/fgcethTimer.h>
#include <libfgceth/private/fgcethStats.h>
#include <libfgceth/private/fgcethThreads.h>
#include <libfgceth/private/fgcethSm.h>
#include <libfgceth/private/fgcethRspSm.h>
#include <libfgceth/private/fgcethCrc.h>
#include <libfgceth/private/fgcethHash.h>
#include <libfgceth/private/fgcethDev.h>
#include <libfgceth/public/fgcethFParser.h>
#include <libfgceth/private/fgcethLog.h>
#include <libfgceth/private/fgcethRterm.h>
#include <libfgceth/private/fgcethStatPub.h>

#include <inc/fgc_codes_gen.h>
#include <inc/fgc_runlog.h>
#include <inc/fgc_code.h>

#include <ktgwRt.h>

#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

/**************************************
 * Local variables
***************************************/

// Used to mark whether status reception window is open (0-15 ms of 20 ms cycle)

static bool           is_in_status_window = true;
int  dev_status_number = 0;

// Time frame structure

static fgc_ether_time time_frame;

// Mutex to synchronize Ethernet thread and ms timer thread

static pthread_mutex_t eth_sync_timer_mutex;




void prepareCode(struct fgc_fieldbus_time *time, uint8_t (*code_msg)[FGC_CODE_BLK_SIZE], uint32_t num_blocks);
void prepareTime(struct fgc_fieldbus_time *time);
void prepareRT(float rt_ref[64], struct timeval *time);

/**************************************
 * Local functions
***************************************/

/**
 * Invoked when new status packet arrives through Ethernet network interface.
 * It calculates different statistics and copies status data to device object.
 *
 * Thread: Ethernet
 *
 * @param device a pointer to a structure containing device data
 * @param payload a pointer to a structure containing status payload
 */

static void protocol_processStatusFrame(fgc_ether_device* device, fgc_ether_status_payload_t* payload)
{
    uint32_t rterm_resp_len;
    fgceth_errno err;

	fgcethAssertExp(device  != NULL);
    fgcethAssertExp(payload != NULL);

    // Extract status byte

    uint8_t ack  = payload->status.fieldbus_stat.ack;

    // ***************************************************

    // Statistics - status received

    device->stats.status_rec++;

    // Statistics - real-time message missed

    device->stats.rd_miss += (ack & FGC_ACK_RD_PKT) != FGC_ACK_RD_PKT;

    // Statistics - sequential status messages missed (the sequence is 1,2,3,1,2,3,1,2,3...)

    if (device->stats.status_rec > 1)
    {
        if ( ((device->ack & FGC_ACK_STATUS) % 3) + 1 != (ack & FGC_ACK_STATUS) )
        {
        	fgcethWarningMsgLog(device->id, "Protocol Status Frame: Sequential number of status was missed.");
            device->stats.seq_status_miss++;
        }
    }

    // Statistics - time packet not received

    if(!(ack & FGC_ACK_TIME))
    {
        device->stats.time_missed++;
        fgcethWarningMsgLog(device->id, "Protocol Status Frame: New time variable was not received by FGC.");
    }

    // Statistics - PLL unlock transition

    if ( (ack & FGC_ACK_PLL) == 0 && device->pll == FGC_ACK_PLL)
    {
    	fgcethWarningMsgLog(device->id, "Protocol Status Frame: Unlock transition was detected.");
        device->stats.unlock_transition++;
    }

    // Statistics - status reception outside of the status window

    if (!is_in_status_window)
    {
        //TODO turn this on: debugLog("Status outside of reception window");
        device->stats.status_out_of_window++;
    }

    // ***************************************************

    // Update last values

    device->last_ack     = device->ack;
    device->ack          = ack;
    device->status_state = payload->status.fieldbus_stat.cmd_stat;
    device->class_id     = payload->status.fieldbus_stat.class_id;
    device->pll          = ack & FGC_ACK_PLL;

    // Copy whole status packet
     
    //memcpy(&device->last_status, &payload->status, sizeof(fgc_stat_var));
    statPubAddNewStat(device->id, &payload->status);

    // ***************************************************

    device->status_missed = 0;

    // ***************************************************

    // Update the command state machine

    fgcsm_smUpdate(device);

    if(!(device->class_id % 10))
    {

        // Process any boot remote terminal data

        union fgc_pub_class_data *class_data = &payload->status.fgc_stat.class_data;

        class_data->boot.rterm[sizeof(class_data->boot.rterm) - 1] = '\0';
    	rterm_resp_len = strlen(class_data->boot.rterm);

    	if(rterm_resp_len > 0)
    	{
    		err = rtermPublishToQueues(device->id, class_data->boot.rterm, rterm_resp_len);

    		if(err != FGC_SUCCESS)
    		{
    			fgcethWarningMsgLog(device->id, "Protocol Status Frame: Unable to send device %d boot remote terminal data to clients.", device->id);
    			// Do not throw this error...
    		}
    	}

    }

    // Process any remote terminal data

	payload->rterm[sizeof(payload->rterm) - 1] = '\0';
	rterm_resp_len = strlen(payload->rterm);

	if(rterm_resp_len > 0) // There is at least 1 character of remote terminal data
	{
		err = rtermPublishToQueues(device->id, payload->rterm, rterm_resp_len);

		if(err != FGC_SUCCESS)
		{
			fgcethWarningMsgLog(device->id, "Protocol Status Frame: Unable to send device %d remote terminal data to clients.", device->id);
			// Do not throw this error...
		}

	}
}



/**
 * Invoked when new response packet arrives through Ethernet network interface.
 * It just pass data to response state machine update function.
 *
 * Thread: Ethernet
 *
 * @param device a pointer to a structure containing device data
 * @param payload a pointer to a structure containing response payload
 */

static void protocol_processResFrame(fgc_ether_device* device, fgc_ether_rsp_payload* payload)
{
    rspsm_smUpdate(device, payload);
}



/* Invoked when new publication packet arrives through Ethernet network interface.
 *
 * Thread: Ethernet
 *
 * @param device a pointer to a structure containing device data
 * @param payload a pointer to a structure containing publication payload
 */

static void protocol_processPubFrame(fgc_ether_device* device, fgc_ether_pub_payload_t* payload)
{
    // To be done
}



/**
 * Used to counts number of times status is missed.
 * Copy current status to status publication 
 *
 * Thread: Ms timer
 *
 * @param device a pointer to a structure containing device data
 */

static void protocol_checkStatus(struct timeval time)
{
    for(int i = 1; i <= FGC_ETHER_MAX_FGCS; ++i)
    {
        fgc_ether_device* device = &fgceth_vars.devices[i];

        if (device->declared)
        {
            // Try to lock the mutex or wait for the lock to be available

            pthread_mutex_lock(&eth_sync_timer_mutex);

            // Statistics (count status missed) and set command state machine to OFFLINE

            if (device->status_missed >= FGCETH_MAX_MISS_STAT_PKT && device->cmd_state != CMD_OFFLINE)
            {
            	fgcethWarningMsgLog(device->id, "Protocol Status Check: Status packet was missed for %d cycles. Transition to offline state.", device->status_missed);
            	//fgcethWarningMsgLog(device->id, "Protocol Status Check: Detected a transition to offline state.");
                device->stats.offline_transition++;

                device->cmd_state = CMD_OFFLINE;
                device->prev_status_missed = 0;
            }

            if (device->status_missed > 1 && device->stats.offline_transition > 0 && device->cmd_state != CMD_OFFLINE)
            {
            	if(device->status_missed == 2)
            	{
            		fgcethWarningMsgLog(device->id, "Protocol Status Check: Status packet was missed.");
            	}
                device->stats.status_miss++;
            }

            if(device->status_missed == 0 && device->prev_status_missed > 1 && device->cmd_state != CMD_OFFLINE)
        	{
        		fgcethWarningMsgLog(device->id, "Protocol Status Check: Status packet was missed for %d cycles.", device->prev_status_missed);
        	}

            // Return an error to all queued commands if the state is OFFLINE

            if (device->cmd_state == CMD_OFFLINE)
            {
                protocol_finishAllCommands(device, FGC_DEV_NOT_READY);
            }

            // It will be set to 0 if the status is received.
            // Otherwise it will count 20ms cycles without status being received.

            if(device->cmd_state != CMD_OFFLINE)
            {
            	device->prev_status_missed = device->status_missed;
            }

            device->status_missed++;

            // Unlock the status mutex

            pthread_mutex_unlock(&eth_sync_timer_mutex);

        }
    }

    struct timeval pub_time;
    uint32_t millisec = time.tv_usec / 1000;

    pub_time.tv_sec  = time.tv_sec;
    pub_time.tv_usec = (millisec - (millisec % 20)) * 1000;

    statPubFinishStatFrame(pub_time);
    
}

/**
 * Invoked by the millisecond timer every 1ms. It performs checks, command state update, timeout checks
 * and is responsible for sending time packets at every 18 and 19 millisecond of the 20ms cycle.
 *
 * Thread: Ms timer
 *
 * @param time a time structure used to determine millisecond number
 */

static void protocol_timerHandler(struct timeval time)
{
    // ***************************************************

    // Check time difference for statistics

    static struct timeval last_time;

    int32_t diff = 0;
    struct timeval exec_time;

    // Check if should be terminated

    pthread_mutex_lock(&fgceth_vars.timer_mutex);

    if(fgceth_vars.closing_protocol)
    {
    	pthread_mutex_unlock(&fgceth_vars.timer_mutex);

    	fgcethInitMsgLog(0, L_V0, "[Timer handler] Received termination signal. Now closing...");

    	struct itimerspec its;
    	its.it_value.tv_sec      = 0;
    	its.it_value.tv_nsec     = 0;
    	its.it_interval.tv_sec   = 0;
    	its.it_interval.tv_nsec  = 0;

    	pthread_mutex_lock(&fgceth_vars.resource_ctrl_mutex);

    	if(!fgceth_vars.timer_finished)
    	{
    	    timer_settime(fgceth_vars.tim.timer, 0, &its, NULL);
    	}

    	fgceth_vars.timer_finished = true;

    	pthread_cond_signal(&fgceth_vars.resource_ctrl_cond);
    	pthread_mutex_unlock(&fgceth_vars.resource_ctrl_mutex);

    	return;
    }



    // ***************************************************

    uint32_t millisec = time.tv_usec / 1000;

    // ***************************************************

    // Millisecond 16 - 1 ms after status reception window closes

    if (millisec % 20 == FGC_ETHER_STATUS_WIN_END_MS + 1)
    {
        is_in_status_window = false;

        // For every device, do status check

        protocol_checkStatus(time);

    }

    if (millisec % 20 == FGC_ETHER_TIME_1_MS - 1)
    {
    	prepareTime(&time_frame.payload.time);

		// Prepare FGC code

    	prepareCode(&time_frame.payload.time, time_frame.payload.code, FGC_FIELDBUS_CODE_BLOCKS_PER_MSG);

        // Prepare real-time references

        prepareRT(time_frame.payload.rt_ref, &time);
    }

    // ***************************************************

    // Millisecond 18 & 19 - Send time packets

    if (millisec % 20 == FGC_ETHER_TIME_1_MS || millisec % 20 == FGC_ETHER_TIME_2_MS)
    {
        // Only once in the 20ms cycle

        if (millisec % 20 == FGC_ETHER_TIME_1_MS)
        {
            // Increase runlog index (runlog processing stub - to be implemented)

            static uint32_t fgc_runlog_idx;
            ++fgc_runlog_idx %= FGC_RUNLOG_SIZE_BYTES;
            time_frame.payload.time.runlog_idx = htons(fgc_runlog_idx);
        }

        // Set Unix time and millisecond

        time_frame.payload.time.unix_time = htonl(time.tv_sec);
        time_frame.payload.time.ms_time   = htons(millisec);

        // **************************************

        // Send time packet

        if(fgc_ether_global_struct::pm_flag_enabled)
        {
        	time_frame.payload.time.flags |= FGC_FLAGS_PM_ENABLED;
        }
        else
        {
        	time_frame.payload.time.flags &= ~FGC_FLAGS_PM_ENABLED;
        }

        if(fgc_ether_global_struct::fgc_logger_enabled)
        {
        	time_frame.payload.time.flags |= FGC_FLAGS_LOGGER_ENABLED;
        }
        else
        {
        	time_frame.payload.time.flags &= ~FGC_FLAGS_LOGGER_ENABLED;
        }

        ether_send(0, &time_frame, sizeof(time_frame));

        // **************************************

        // Mark the start of the status reception window after sending last time packet

        if (millisec % 20 == FGC_ETHER_TIME_2_MS)
        {
            is_in_status_window = true;
            dev_status_number++;
        }
    }

    // To check if routine took too much time

    gettimeofday(&exec_time, NULL);

    // Verify if packet was late / ahead of time

    if (last_time.tv_sec > 0)
    {
        int32_t sec = (time.tv_sec - last_time.tv_sec) * 1000000;
        diff = sec + (time.tv_usec - last_time.tv_usec) - 1000;
    }

    struct timeval last_time_aux = last_time;
    last_time = time;


    // Free lock here
    pthread_mutex_unlock(&fgceth_vars.timer_mutex);

	if (diff > 1000)
	{
		fgceth_vars.global_stats.timer_window++;
		fgcethWarningMsgLog(0, "[Timer handler] Timing signal may have been missed for %d us [Prev: %02ld.%06ld s (@ %ld ms) | This: %02ld.%06ld s (@ %ld ms)].", diff, last_time_aux.tv_sec % 60, last_time_aux.tv_usec, (last_time_aux.tv_usec / 1000) % 20, time.tv_sec % 60, time.tv_usec, (time.tv_usec / 1000) % 20);
	}
	else if (diff < -250 || diff > 250)
	{
		fgceth_vars.global_stats.timer_window++;
		fgcethWarningMsgLog(0, "[Timer handler] New timing signal detected with a %d us mismatch [Prev: %02ld.%06ld s (@ %ld ms) | This: %02ld.%06ld s (@ %ld ms)].", diff, last_time_aux.tv_sec % 60, last_time_aux.tv_usec, (last_time_aux.tv_usec / 1000) % 20, time.tv_sec % 60, time.tv_usec, (time.tv_usec / 1000) % 20);
	}

    int32_t exec_sec = (exec_time.tv_sec - time.tv_sec) * 1000000;
    int32_t exec_diff = exec_sec + (exec_time.tv_usec - time.tv_usec);

	if (exec_diff > 200)
	{
		fgcethWarningMsgLog(0, "[Timer handler] Timing routine (@ %d ms) took longer than expected to complete (%d us).", millisec % 20, exec_diff);
	}

}



/**************************************
 * Functions
***************************************/


fgceth_errno protocol_sendCommand(fgceth_rsp * response)
{
    CC_ASSERT(response != NULL);
    CC_ASSERT(response->type == CMD_RSP);

    // Prepare FGC_Ether Ethernet frame L_PRT_CMD_PKT

    struct fgc_ether_cmd frame;
    memset(&frame, 0, sizeof(frame));

    // ***************************************************

    // Set payload type

    frame.header.fgc.payload_type    = FGC_ETHER_PAYLOAD_CMD;
    frame.payload.data.header.flags  = FGC_FIELDBUS_FLAGS_CMD_PKT;

    // Set get or set command type

    switch (response->d.cmd_rsp.cmd_type)
    {
        case GET:
            frame.payload.data.header.flags |= FGC_FIELDBUS_FLAGS_GET_CMD;
            break;

        case SET:
        case SET_BIN:
            frame.payload.data.header.flags |= FGC_FIELDBUS_FLAGS_SET_CMD;
            break;
    };

    // Setting user - currently this option is not used

    frame.payload.data.header.user = 0;

    // ***************************************************

    // Command is always sent as first so if no command bytes were sent then this is the first packet

    if(response->d.cmd_rsp.hidden_members_.cmd_step == START)
    {
        frame.payload.data.header.flags |= FGC_FIELDBUS_FLAGS_FIRST_PKT;
        response->d.cmd_rsp.hidden_members_.cmd_step      = PROPERTY;
        response->d.cmd_rsp.hidden_members_.cmd_length    = strlen(response->d.cmd_rsp.property);
        response->d.cmd_rsp.hidden_members_.cmd_remaining = response->d.cmd_rsp.hidden_members_.cmd_length;
    }

    // ***************************************************

    int32_t offset     = 0;
    int32_t buffer_len = sizeof(frame.payload.data.cmd_pkt);

    // ***************************************************

    // If there are command bytes left to send, then copy them to the frame buffer

    if (response->d.cmd_rsp.hidden_members_.cmd_step == PROPERTY)
    {
        // If what is left to send is shorter than max packet length (buffer_len) then update buffer_len

        if (response->d.cmd_rsp.hidden_members_.cmd_remaining < buffer_len)
        {
            buffer_len = response->d.cmd_rsp.hidden_members_.cmd_remaining;
        }

        strncpy(frame.payload.data.cmd_pkt,
                &response->d.cmd_rsp.property[response->d.cmd_rsp.hidden_members_.cmd_length - response->d.cmd_rsp.hidden_members_.cmd_remaining],
                buffer_len);

        response->d.cmd_rsp.hidden_members_.cmd_remaining -= buffer_len;
        offset += buffer_len;

        if(response->d.cmd_rsp.hidden_members_.cmd_remaining <= 0)
        {
            if(blkbufDataLen(response->d.cmd_rsp.value_opts) > 0)
            {
                response->d.cmd_rsp.hidden_members_.cmd_step      = VALUES;
                response->d.cmd_rsp.hidden_members_.cmd_remaining = 1; // For the white space after propriety
                blkbufIteratorStart(response->d.cmd_rsp.value_opts);
            }
            else
            {
                response->d.cmd_rsp.hidden_members_.cmd_step      = END;
            }
        }
    }

    // ***************************************************

    // If the command is fully sent, then send the value

    if (response->d.cmd_rsp.hidden_members_.cmd_step == VALUES)
    {
        // Compensate for the length of the sent command

        buffer_len = sizeof(frame.payload.data.cmd_pkt) - offset;

        // If what is left to send is shorter than max packet length (buffer_len) then update buffer_len

        if (response->d.cmd_rsp.hidden_members_.cmd_remaining < buffer_len && response->d.cmd_rsp.hidden_members_.cmd_remaining > 0)
        {
            strncpy(frame.payload.data.cmd_pkt + offset,
                    " ",
                    buffer_len);
            response->d.cmd_rsp.hidden_members_.cmd_remaining = 0;
            buffer_len--;
            offset++;
        }

        if(blkbufIteratorRemaining(response->d.cmd_rsp.value_opts) < buffer_len)
        {
            buffer_len = blkbufIteratorRemaining(response->d.cmd_rsp.value_opts);
        }

        // Copy value to the buffer

        blkbufIterateBin(frame.payload.data.cmd_pkt + offset, response->d.cmd_rsp.value_opts, buffer_len);

        offset += buffer_len;

        if(blkbufIteratorRemaining(response->d.cmd_rsp.value_opts) <= 0)
        {
            response->d.cmd_rsp.hidden_members_.cmd_step = END;
        }
    }

    // ***************************************************

    // Check if the whole command and the whole value were sent

    if (response->d.cmd_rsp.hidden_members_.cmd_step == END)
    {
        // Then set the last packet flag

        frame.payload.data.header.flags |= FGC_FIELDBUS_FLAGS_LAST_PKT;

    }

    // ***************************************************

    // Set payload length

    frame.payload.length = offset + sizeof(frame.payload.data.header);

    // ***************************************************

    uint32_t frame_len    = 0;

    // Calculate frame length: if payload is longer/equal to min. payload length then calculate
    // actual length. If it's shorter then make it as long as minimal required payload length.

    if(frame.payload.length >= FGC_FIELDBUS_MIN_CMD_LEN)
    {
        frame_len = sizeof(frame.header) + sizeof(frame.payload.length) + frame.payload.length;
    }
    else
    {
        frame_len = sizeof(frame.header) + FGC_ETHER_PAYLOAD_MIN;
    }

    // ***************************************************

    frame.payload.length = htons(frame.payload.length);

    // Send the frame

    return ether_send(response->d.cmd_rsp.hidden_members_.dev_id, &frame, frame_len);
}



void protocol_finishCurrentCommand(fgc_ether_device* device)
{
    fgcethAssertExp(device != NULL);
    fgcethAssertExp(device->current_command->d.cmd_rsp.hidden_members_.response_queue != NULL);

    // And push the response into its response queue

    queueBlockPush(device->current_command->d.cmd_rsp.hidden_members_.response_queue, device->current_command);

    // ***************************************************

    // Reset current_command to NULL

    device->current_command = NULL;

    // Response is no longer expected

    device->rsp_expected = false;
    device->rsp_state    = RSP_IDLE;
}



void protocol_finishAllCommands(fgc_ether_device* device, const fgceth_errno error)
{
    // Finish current command

    if (device->current_command != NULL)
    {
        device->current_command->d.cmd_rsp.error = error;
        protocol_finishCurrentCommand(device);
    }

    // ***************************************************

    // Finish all other commands

    fgceth_rsp * command;

    // Return an error to all commands in the command queue

    while ((command = queuePop(&device->cmd_queue)))
    {
        CC_ASSERT(command->type == CMD_RSP);

        command->d.cmd_rsp.error = error;

        // Push the response into its response queue

        queueBlockPush(command->d.cmd_rsp.hidden_members_.response_queue, command);
    }
}



fgceth_errno protocol_init(const char* eth_name, const uint32_t blk_buf_used_mem, const uint32_t blk_buf_size, const uint32_t pub_buffer_cycles)
{
    CC_ASSERT(eth_name != NULL);

    fgceth_errno err;

    // Global structure fields initialization

    fgceth_vars.closing_protocol       = false;
    fgceth_vars.reception_thr_finished = false;
    fgceth_vars.timer_finished         = false;

    // Check if Linux kernel is supports real-time preemption

    if(threads_detect_rt_preempt())
    {
        fgcethInitMsgLog(0, L_V0, "[Protocol] Running on Linux with RT PREMPT patch (real-time).");
    }
    else
    {
        fgcethInitMsgLog(0, L_V0, "[Protocol] Running on Linux without RT PREMPT patch (non real-time).");
    }

    fgcethInitMsgLog(0, L_V2, "[Protocol] Initializing shared resources...");

    if(pipe(fgceth_vars.reception_thr_pipe))
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Unable to open pipe.");
        return FGC_ETH_OPEN_PIPE;
    }

    if(threads_mutex_init(&fgceth_vars.resource_ctrl_mutex))
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Unable to initialize global vars mutex.");
        return FGC_ETH_MUTEX_INIT;
    }

    if(threads_cond_init(&fgceth_vars.resource_ctrl_cond))
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Unable to initialize global vars conditional mutex.");
        return FGC_ETH_COND_MUTEX_INIT;
    }

    // ***************************************************

    // Initialize status mutex

    if(threads_mutex_init(&eth_sync_timer_mutex))
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Unable to initialize timer synchronization mutex.");
        return FGC_ETH_MUTEX_INIT;
    }

    // Command & response pool handling

    fgcethInitMsgLog(0, L_V2, "[Protocol] Initializing response manager...");

    if ( (err = rspManagerInit(&fgceth_vars.rsp_man, blk_buf_used_mem, blk_buf_size)) != FGC_SUCCESS)
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Response manager failed with error %d.", err);
        return err;
    }

    // Remote terminal handling handling

    fgcethInitMsgLog(0, L_V2, "[Protocol] Initializing rterm handling...");

    if((err = rtermInit(&fgceth_vars.rsp_man)) != FGC_SUCCESS)
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Unable to initialize rterm handling, with error %d.", err);
        return err;
    }

    // Status publication initialization
     
    fgcethInitMsgLog(0, L_V2, "[Protocol] Initializing status publication...");

    uint32_t pub_buf_c = pub_buffer_cycles;

    if(pub_buffer_cycles == 0)
    {
        pub_buf_c = 10;
    }

    if((err = statPubInit(pub_buf_c)) != FGC_SUCCESS)
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Unable to initialize status publication, with error %d.", err);
        return err;
    }

    // Initializing Ethernet handling

    fgcethInitMsgLog(0, L_V2, "[Protocol] Initializing Ethernet handling...");

    if((err = ether_init(eth_name, 128, protocol_processReceivedFrame)) != FGC_SUCCESS)
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Ethernet handling failed with error %d.", err);
        return err;
    }

    // Create timer

    fgcethInitMsgLog(0, L_V2, "[Protocol] Initializing timer...");

    if((err = timer_init(protocol_timerHandler)) != FGC_SUCCESS)
    {
        fgcethInitErrorMsgLog(0, "Protocol initialization failed. Timer initialization failed with error %d.", err);
        return err;
    }

    // ***************************************************

    // Prepare time frame

    time_frame.header.fgc.payload_type  = FGC_ETHER_PAYLOAD_TIME;

    // Set fgc_id to 0 (send to no channel)

    time_frame.payload.time.fgc_id = 0;

    // No valid code to advertise

    time_frame.payload.time.adv_code_id = FGC_CODE_NULL;
    time_frame.payload.time.code_var_id = FGC_CODE_NULL;

    // Enable sending of Codes

    fgceth_vars.send_code_flag = true;
    fgceth_vars.code_tx_force  = FGC_CODE_NUM_SLOTS; // Do not force any code

    // Reset maximum memory used tracker
     
    fgceth_vars.max_mem_used   = 0;

    fgcethInitMsgLog(0, L_V2, "[Protocol] All modules successfully initialized.");

    return FGC_SUCCESS;
}



void prepareCode(struct fgc_fieldbus_time *time, uint8_t (*code_msg)[FGC_CODE_BLK_SIZE], uint32_t num_blocks)
{
    static uint32_t fgc_code_var_idx = 0;   // Index within code that is currently being transmitted
    static uint32_t fgc_code_slot = 1;      // Index #0 is reserved for names
    static bool     send_names_next = true;

    // Set name DB checksum

    time->namedb_crc = htons(fgceth_vars.codes[0].code_chksum);

    // Check whether code transmission is disabled

    if(!fgceth_vars.send_code_flag)
    {
        time->adv_code_class_id         = 0;
        time->adv_code_id               = FGC_CODE_NULL;
        time->adv_code_len_blk          = 0;
        time->adv_code_crc              = 0;
        time->adv_code_version          = 0;
        time->old_adv_code_version      = 0;
        time->code_var_id               = FGC_CODE_NULL;
        fgceth_vars.code_tx_valid_flag  = false;

        return;
    }

    // Check whether current code buffer is not valid or all of code has been sent

    if(!fgceth_vars.code_tx_valid_flag                                             ||
        fgc_code_var_idx >= fgceth_vars.codes[fgceth_vars.code_tx_num].code_length ||
       !fgc_code_var_idx)
    {
        // Check whether code index is forced

        if(fgceth_vars.code_tx_force < FGC_CODE_NUM_SLOTS)
        {
        	fgceth_vars.code_tx_num = fgceth_vars.code_tx_force;
        }
        else
        {
            // Find next code in loop, alternating between NameDb and one of the codes
            // NameDb -> Code#1 -> NameDb -> Code#2 -> ... -> NameDb -> Code#N -> nameDb -> Code#1 -> ...

            if(send_names_next)
            {
            	send_names_next = false;
            	fgceth_vars.code_tx_num = 0;
            }
            else
            {
            	send_names_next = true;
            	fgceth_vars.code_tx_num = fgc_code_slot;

            	// Round-robin from 1 to FGC_CODE_NUM_SLOTS-1
            	if(++fgc_code_slot >= FGC_CODE_NUM_SLOTS) fgc_code_slot = 1;
            }
        }

        // Set code if valid

        if(fgceth_vars.codes[fgceth_vars.code_tx_num].code_id != FGC_CODE_NULL)
        {
        	fgceth_vars.code_tx_valid_flag = true;
            fgc_code_var_idx                         = 0;
            time->code_var_class_id                  = fgceth_vars.codes[fgceth_vars.code_tx_num].code_class;
            time->code_var_id                        = fgceth_vars.codes[fgceth_vars.code_tx_num].code_id;
        }
        else // No valid code
        {
        	fgceth_vars.code_tx_valid_flag = false;
        }
    }

    // Send code if valid

    if(fgceth_vars.code_tx_valid_flag) // Code is valid
    {
        memcpy(code_msg,
               &fgceth_vars.codes[fgceth_vars.code_tx_num].code[fgc_code_var_idx],
               FGC_CODE_BLK_SIZE * num_blocks);
        time->code_var_idx  = htons(fgc_code_var_idx / FGC_CODE_BLK_SIZE);
        fgc_code_var_idx   += (FGC_CODE_BLK_SIZE * num_blocks);
    }
    else // No valid code
    {
        time->code_var_id = FGC_CODE_NULL;
    }

}



void prepareTime(struct fgc_fieldbus_time *time)
{
    static uint32_t fgc_code_num_adv;               // Number of code that is currently being advertised
    static uint32_t prev_rterm_channel_index = 1;
    uint32_t i;
    int32_t c;

    // Advertise next code

    fgc_code_num_adv   = (fgc_code_num_adv + 1) % FGC_CODE_NUM_SLOTS;
    time->adv_list_idx = fgc_code_num_adv;
    time->adv_list_len = FGC_CODE_NUM_SLOTS;

    if(fgceth_vars.codes[fgc_code_num_adv].code_id != FGC_CODE_NULL)
    {
        time->adv_code_class_id    = fgceth_vars.codes[fgc_code_num_adv].code_class;
        time->adv_code_id          = fgceth_vars.codes[fgc_code_num_adv].code_id;
        time->adv_code_len_blk     = htons(fgceth_vars.codes[fgc_code_num_adv].code_length / FGC_CODE_BLK_SIZE);
        time->adv_code_crc         = htons(fgceth_vars.codes[fgc_code_num_adv].code_chksum);
        time->adv_code_version     = htonl(fgceth_vars.codes[fgc_code_num_adv].code_version);
        time->old_adv_code_version = htons(fgceth_vars.codes[fgc_code_num_adv].code_old_version);
    }
    else // No valid code to advertise
    {
        time->adv_code_class_id    = 0;
        time->adv_code_id          = FGC_CODE_NULL;
        time->adv_code_len_blk     = 0;
        time->adv_code_crc         = 0;
        time->adv_code_version     = 0;
        time->old_adv_code_version = 0;
    }

    // Initialise fgc_id to 0 (send to no channel)

    time->fgc_id = 0;

    // Find next channel for remote terminal character

	i = prev_rterm_channel_index;
	c = -1;

	do
	{
		i = (i % FGC_ETHER_MAX_FGCS) + 1;

		// Check whether a character can be read from transmit queue

		if((c = rtermPopCharToDev(i)) >= 0)
		{
			prev_rterm_channel_index    = i;
			time->fgc_id                = i;
			time->fgc_char              = c;
		}

	} while(c < 0 && i != prev_rterm_channel_index);

}


void prepareRT(float rt_ref[64], struct timeval *time)
{
    union { float f; uint32_t l; }  f_l;
    uint32_t                        i;

    // Send real-time references

    pthread_mutex_lock(&rt.mutex);

    for(i = 0 ; i < FGCD_MAX_EQP_DEVS ; i++)
    {
        if(rt.data[rt.data_start].active_channels & (1LLU << i)) // Channel is active
        {
            // Set new value

            f_l.f = rt.used_data.channels[i]    = rt.data[rt.data_start].channels[i];
            // fgcethMsgLog(FGCETH_NO_DEV, L_V0, "Preparing RT for channel %d with value %f", i, f_l.f);
            f_l.l                               = htonl(f_l.l);
            rt_ref[i] = f_l.f;
        }
        else // Channel is inactive
        {
            // Retain previous value

            f_l.f                               = rt.used_data.channels[i];
            f_l.l                               = htonl(f_l.l);
            rt_ref[i] = f_l.f;
        }
    }

    // Clear active channels for this reference and advance data start

    rt.data[rt.data_start].active_channels  = 0;
    rt.data_start                           = (rt.data_start + 1) % RT_BUFFER_SIZE;

    rt.cycle_time.tv_sec                    = time->tv_sec;
    rt.cycle_time.tv_usec                   = time->tv_usec;

    pthread_mutex_unlock(&rt.mutex);

    // Send zero reference if real_time_zero flag set

    // if(fieldbus.real_time_zero)
    // {
    //     memset(rt_ref, 0, sizeof(rt_ref));
    // }
}


void protocol_processReceivedFrame(const uint8_t * src_addr, fgc_ether_frame* frame)
{
    uint8_t dev_id = src_addr[5];

    fgcethAssertExp(dev_id > 0 && dev_id <= FGC_ETHER_MAX_FGCS);
    fgcethAssertExp(frame != NULL);

    // Try to lock the mutex or wait for the lock to be available

    pthread_mutex_lock(&eth_sync_timer_mutex);

    // ***************************************************

    // Checking frame payload type and passing frame further

    switch (frame->header.fgc.payload_type)
    {
        case FGC_ETHER_PAYLOAD_RSP:
            // Response frame
        	fgcethMsgLog(dev_id, L_V4, "[Frame] Rsp ether frame detected.");
            protocol_processResFrame(&fgceth_vars.devices[dev_id], reinterpret_cast<fgc_ether_rsp_payload*>(frame->payload));
            break;

        case FGC_ETHER_PAYLOAD_PUB:
            // Publication frame
        	fgcethMsgLog(dev_id, L_V4, "[Frame] Pub ether frame detected.");
            protocol_processPubFrame(&fgceth_vars.devices[dev_id], reinterpret_cast<fgc_ether_pub_payload_t*>(frame->payload));
            break;

        case FGC_ETHER_PAYLOAD_STATUS:
            // Status frame
        	fgcethMsgLog(dev_id, L_V4, "[Frame] Status ether frame detected.");
            protocol_processStatusFrame(&fgceth_vars.devices[dev_id], reinterpret_cast<fgc_ether_status_payload_t*>(frame->payload));
            break;
    }

    // ***************************************************

    // Unlock the status mutex

    pthread_mutex_unlock(&eth_sync_timer_mutex);

    // Check memory usage
    
    uint32_t mem_usage_ppc = (100 * rspManagerMaxUsage(&fgceth_vars.rsp_man));
    uint32_t mem_warning    = 0;

    if(mem_usage_ppc > fgceth_vars.max_mem_used)
    {
        if(mem_usage_ppc > 90)
        {
            mem_warning = mem_usage_ppc;
        }
        else if(mem_usage_ppc > 70)
        {
            if((mem_usage_ppc / 5) > (fgceth_vars.max_mem_used / 5))
            {
                mem_warning = 5 * (mem_usage_ppc / 5);
            }
        }
        else
        {
            if((mem_usage_ppc / 10) > (fgceth_vars.max_mem_used / 10))
            {
                mem_warning = 10 * (mem_usage_ppc / 10);
            }
        }

        fgceth_vars.max_mem_used = mem_usage_ppc;
    }

    if(mem_warning != 0)
    {
        fgcethWarningMsgLog(0, "Maximum memory usage reached %d%%.", mem_warning);
    }

}

fgceth_errno protocol_close()
{
	fgcethInitMsgLog(0, L_V2, "[Protocol handling] Sending termination signal to timer handler and frame reception thread...");

	fgceth_vars.closing_protocol = true;
	__sync_synchronize();

	// Wait for timer and reception thread self-termination

	pthread_mutex_lock(&fgceth_vars.resource_ctrl_mutex);

	while(!fgceth_vars.timer_finished || !fgceth_vars.reception_thr_finished)
	{
		// Reception thread may be blocked on poll(). Send a message to fd to unblock.

		if(!fgceth_vars.reception_thr_finished){
			const char * term_message = "T";
			if(write(fgceth_vars.reception_thr_pipe[1], term_message, sizeof(term_message)) < 0)
			{
				pthread_mutex_unlock(&fgceth_vars.resource_ctrl_mutex);
				return FGC_ETH_WRITE_PIPE;
			}
		}

		pthread_cond_wait(&fgceth_vars.resource_ctrl_cond, &fgceth_vars.resource_ctrl_mutex);
	}

	pthread_mutex_unlock(&fgceth_vars.resource_ctrl_mutex);

	//fgcethInitMsgLog(0, L_V2, "[Protocol handling] Frame reception thread is closed.");
	//fgcethInitMsgLog(0, L_V2, "[Protocol handling] Timer is closed.");
	//fgcethInitMsgLog(0, L_V2, "[Protocol handling] Terminating resource usage...");

	// Reception thread needs to be joined

	void * error;

	if ( (errno = pthread_join(fgceth_vars.reception_thread, &error)) != 0 )
	{
		fgcethInitErrorMsgLog(0, "Protocol closing has failed. Frame reception thread join failed.");
		return FGC_ETH_THREAD_JOIN;
	}

	if(((fgceth_errno) (intptr_t) error) != FGC_SUCCESS)
	{
		fgcethInitErrorMsgLog(0, "Protocol closing has failed. Frame reception thread returned %d.", (fgceth_errno) (intptr_t) error);
		return (fgceth_errno) (intptr_t) error;
	}

	// Destroy all objects still on device queues, and then free each one of the queues
	
    fgceth_rsp * resp;

	for(int i = 0 ; i <= FGC_ETHER_MAX_FGCS ; i++)
	{
		if(fgceth_vars.devices[i].declared)
		{
			while((resp = queuePop(&fgceth_vars.devices[i].cmd_queue)) != NULL)
			{
			    rspManagerReturnRsp(&fgceth_vars.rsp_man, resp);
			}

			queueFree(&fgceth_vars.devices[i].cmd_queue);
		}
	}

	// Free command/response structures
	// command_close();

	// Free rterm response structures

	rtermClose();

    // Close publication module
     
    statPubClose();

	// Close response manager

	rspManagerClose(&fgceth_vars.rsp_man);

	// Timer needs to be destroyed

	timer_delete(fgceth_vars.tim.timer);

	if(threads_mutex_destroy(&fgceth_vars.resource_ctrl_mutex))
	{
		fgcethInitErrorMsgLog(0, "Protocol closing has failed. Unable to destroy mutex.");
		return FGC_ETH_MUTEX_DESTROY;
	}

	if(threads_cond_destroy(&fgceth_vars.resource_ctrl_cond)){
		fgcethInitErrorMsgLog(0, "Protocol closing has failed. Unable to destroy conditional mutex.");
		return FGC_ETH_COND_MUTEX_DESTROY;
	}

	fgcethInitMsgLog(0, L_V2, "[Protocol handling] Successfully closed.");

	return FGC_SUCCESS;
}

//EOF
