# FGC Ether Library

The FGC Ether Library provides a set of tools to interact with the FGCs (Function Generator Controlers).

It is used by other FGC Ether modules, such as the FGC Ether Gateway and the FGC Ether Remote Terminal.

## Specification

You can get the specification [here](https://edms.cern.ch/ui/file/1718728/4/FGC_Ether_library_spec_docx_cpdf.pdf).

## Compilation

Just move to the library root folder and type `make`. The binaries are created in a directory *{OS}/{Version}* (for example *Linux/x86_64*).

The library requires the previous installation of CCLIBS.

```
cd <projects>/fgc/sw/kt/libfgceth
make
```

## Tests

In order to run test, move to the test directory, and run then under sudo previledges:
```
cd <projects>/fgc/sw/kt/tests/testRunner
sudo ./testRunner.sh [PARAMETERS]*
```

## Wireshark plugin

A Wireshark plugin is provided in a *wireshark*  directory. It can be added to your Wireshark program to enable FGC_Ether packet parsing and interpretation.

Installation is very simple and all the instructions are provided in a file *wireshark/HowToInstall.txt*. 
 
