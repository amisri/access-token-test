/*!
 * © Copyright CERN 2017. All rights not expressly granted are reserved.
 * @file   utils.h
 * @brief  Utilities.
 * @author Dariusz Zielinski
 */

#ifndef FGCETH_UTILS_H
#define FGCETH_UTILS_H

#include <libfgceth/private/fgcethLog.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>


/**************************************
 * Functions
***************************************/

/// Log an error message - but only in debug mode
#ifndef NDEBUG
    #define fgcethDebugLog(msg)  do { fgcethInfoMsgLog(255, L_DEBUG, "[DEBUG] %s [%s:%d: %s]", msg, __FILE__, __LINE__, __FUNCTION__); } while (0)
#else
    #define fgcethDebugLog(msg)  do { } while (0)
#endif

#endif
//EOF
