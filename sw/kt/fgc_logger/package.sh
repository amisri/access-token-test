#!/bin/bash

project_name="fgc_logger"
project_base_path="$(pwd)/../../clients/python/"
offline_packages_dir="$(pwd)/offline_packages"
destination=$(pwd)

main() {
    get_packages_from_accpy
    package
}

get_packages_from_accpy() {
    source /acc/local/share/python/acc-py/pro/setup.sh
    echo "Getting packages from accpy..."
    if [[ -d $offline_packages_dir ]]; then
        rm $offline_packages_dir/*
    else
        mkdir $offline_packages_dir
    fi

    while IFS="" read -r mod || [ -n "$mod" ]
    do
        echo "    Getting $mod..."
        pip download --no-deps $mod -d $offline_packages_dir
    done < $project_base_path/$project_name/requirements.txt

    echo "    Getting $project_name..."
    pip download --no-deps $project_name -d $offline_packages_dir
}

package_project() {
    mkdir $pkg/$project_name
    cp $project_base_path/$project_name/requirements.txt $pkg/$project_name
    echo "fgc_logger" >> $pkg/$project_name/requirements.txt
    cp $project_base_path/$project_name/$project_name/templates/launcher_example.sh.template $pkg/$project_name/launcher.sh
    cp $project_base_path/$project_name/$project_name/templates/config_example.cfg.template $pkg/$project_name/config.cfg
}

package() {
    echo "Packaging..."
    cd ${destination}
    pkg=${project_name}_$(date +%s)
    mkdir $pkg
    package_project
    cp -r "offline_packages" "install.sh" "README.md" $pkg
    tar czvf ${pkg}.tar.gz $pkg
    rm -r $pkg
}

main
