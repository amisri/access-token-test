#include <iostream>
using namespace std;

#include <cerrno>
#include <stdint.h>
#include <stdlib.h>
#include <ncurses.h>
#include <sys/shm.h>
#include <fipdiag.h>

const key_t FIPDIAG_SHM_KEY = 0x46444730;    //!< Key for WorldFIP Diamon shared memory

struct FIPdiamon
{   
    FdgType *data;
} fipdiamon;

bool curses_started = false;

static inline void msSleep(uint16_t ms)
{
    struct timespec time;

    time.tv_sec  = ms / 1000;
    time.tv_nsec = (ms % 1000) * 1000000;

    // sleep for the specified no. of ms and resume if interrupted

    while(nanosleep(&time, &time) && errno == EINTR) ;
}



void endCurses(void)
{
    if (curses_started && !isendwin()) endwin();
}



void startCurses(void)
{
    if (curses_started) {
        refresh();
    }
    else
    {
        initscr();
        cbreak();
        noecho();
        intrflush(stdscr, false);
        keypad(stdscr, true);
        nodelay(stdscr, TRUE); // don't block on detect keypress
        atexit(endCurses);
        curses_started = true;
    }
}

int kbhit(void)
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}

int32_t fipDiamonInit(void)
{
    int32_t  shm_id;

    // Get shared memory ID

    shm_id = shmget(FIPDIAG_SHM_KEY, sizeof(*fipdiamon.data), 0666 | IPC_CREAT);

    if(shm_id == -1)
    {
        cerr << "fipDiamonStart(): shmget failed with error " << errno << endl;
        return 1;
    }
    
    // Attach to shared memory
   
    fipdiamon.data = reinterpret_cast<FdgType*>(shmat(shm_id, NULL, 0));
   
    if(fipdiamon.data == reinterpret_cast<FdgType*>(-1))
    {
        cerr << "fipDiamonStop(): shmat failed with error " << errno << endl;
        return 1;
    }

    return 0;
}    



void updateDisplay(void)
{
    char buffer[256];
    char date[40];
    struct tm *tm;
    time_t time_secs;

    erase();

    move(1,1);
    addstr("FIP Diamon Monitor");

    move(3,1);
    sprintf(buffer, "FDM Version: %0.1f", fipdiamon.data->Fdm_version);
    addstr(buffer);

    move(4,1);
    sprintf(buffer, "Library Version: %0.1f", fipdiamon.data->Library_version);
    addstr(buffer);


    move(5,1);
    sprintf(buffer, "FDG Protocol: %d", fipdiamon.data->Fdg_protocol);
    addstr(buffer);

    move(6,1);
    addstr("Date Last Reboot: ");
    time_secs = fipdiamon.data->Date_last_reboot;
    tm = localtime(&time_secs);
    strftime(date, sizeof(date), "%Y-%m-%d %H:%M:%S", tm);
    addstr(date);

    move(8,1);
    addstr("Date Cycle: ");
    time_secs = fipdiamon.data->Date_cycle;
    tm = localtime(&time_secs);
    strftime(date, sizeof(date), "%Y-%m-%d %H:%M:%S", tm);
    addstr(date);

    move(9,1);
    sprintf(buffer, "Cycle Counter: %d", fipdiamon.data->Cycle_counter);
    addstr(buffer);

    move(10,1);
    addstr("Bus Status: ");
    switch(fipdiamon.data->Bus_status)
    {
        case ALL_OK:
            addstr("ALL_OK");
            break;
        case AGENTMISSING:
            addstr("AGENTMISSING: a configured device is offline");
            break;
        case NO_RESPONSE:
            addstr("NO_RESPONSE: diagnostic device is offline");
            break;
        case BUS_FAULT1:
            addstr("BUS_FAULT1: one or more configured devices and the diagnostic device are offline");
            break;
        case BUS_FAULT2:
            addstr("BUS_FAULT2: the cycle counter was not updated");
            break;
        default:
            sprintf(buffer, "FIP Diamon Error! Undefined bus status %d", fipdiamon.data->Bus_status);
            addstr(buffer);
    }

    move(12,1);
    addstr("List_theoric_present");
    move(12,24);
    addstr("List_present");
    move(12,47);
    addstr("List_absent");

    for(int row = 0; row < 32; ++row)
    {
        move(row+14,1);

        uint32_t status = fipdiamon.data->List_theoric_present[row];

        for(int bit = 8; bit >= 0; --bit)
        {
            addch((status & (1 << bit)) > 0 ? '1' : '0');
        }

        move(row+14,24);

        status = fipdiamon.data->List_present[row];

        for(int bit = 8; bit >= 0; --bit)
        {
            addch((status & (1 << bit)) > 0 ? '1' : '0');
        }

        move(row+14,47);

        status = fipdiamon.data->List_absent[row];

        for(int bit = 8; bit >= 0; --bit)
        {
            addch((status & (1 << bit)) > 0 ? '1' : '0');
        }
    }

    refresh();
}



int main(void)
{
    fipDiamonInit();

    startCurses();

    while(!kbhit())
    {
        updateDisplay();
        msSleep(100);
    }

    endCurses();
   
    return 0;
}
