/* fgc_ether-publish.c
 * Routines for FGC_Ether publish frame disassembly
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <epan/etypes.h>
#include <fgc_ether.h>
#include <glib.h>
#include <stdio.h>

static int proto_fgc_ether_publish = -1;

static dissector_handle_t fgc_ether_publish_data_handle;

static int ett_fgc_ether_publish                    = -1;

static int hf_fgc_ether_publish_length              = -1;

static int ett_fgc_ether_publish_flags              = -1;
static int hf_fgc_ether_publish_flags               = -1;
static int hf_fgc_ether_publish_flags_first_pkt     = -1;
static int hf_fgc_ether_publish_flags_last_pkt      = -1;
static int hf_fgc_ether_publish_flags_type          = -1;
static int hf_fgc_ether_publish_flags_seq_num       = -1;

static int hf_fgc_ether_publish_sub_id              = -1;

static int ett_fgc_ether_publish_sub_flags          = -1;
static int hf_fgc_ether_publish_sub_flags           = -1;
static int hf_fgc_ether_publish_sub_flags_new_sub   = -1;
static int hf_fgc_ether_publish_sub_flags_set       = -1;

static int hf_fgc_ether_publish_user                = -1;

static int hf_fgc_ether_publish_acq_timestamp       = -1;
static int hf_fgc_ether_publish_response            = -1;

static const value_string fgc_ether_publish_flags_type_values[] =
{
   { 0, "Command",      },
   { 1, "Pub direct",   },
   { 2, "Pub get",      },
   { 0, NULL            }
};

// Dissect FGC_Ether publish payload

static void dissect_fgc_ether_publish(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    struct tm   acq_time;
    proto_tree  *fgc_ether_publish_tree;
    proto_tree  *fgc_ether_publish_tree_flags;
    guint8      flags;
    gint        offset = 0;
    guint16     publish_length;
    proto_item  *ti;
    char        time_string[32];
    time_t      acq_timestamp_s;
    guint32     acq_timestamp_us;
    guint8      sub_flags;

    // Populate protocol tree

    if(tree)
    {
        fgc_ether_publish_tree = proto_item_add_subtree(tree, ett_fgc_ether_publish);

        publish_length = tvb_get_ntohs(tvb, offset);
        proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_length, tvb, offset, 2, FALSE);
        offset += 2;

        // Flags

        flags = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_flags,            tvb, offset, 1, FALSE);
        fgc_ether_publish_tree_flags = proto_item_add_subtree(fgc_ether_publish_tree, ett_fgc_ether_publish_flags);
        proto_tree_add_item(fgc_ether_publish_tree_flags, hf_fgc_ether_publish_flags_first_pkt, tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_publish_tree_flags, hf_fgc_ether_publish_flags_last_pkt,  tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_publish_tree_flags, hf_fgc_ether_publish_flags_type,      tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_publish_tree_flags, hf_fgc_ether_publish_flags_seq_num,   tvb, offset, 1, FALSE);
        offset += 1;

        // Skip reserved

        offset += 1;

        // sub_id

        proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_sub_id, tvb, offset, 1, FALSE);
        offset          += 1;
        publish_length  -= 1;

        // sub_flags

        sub_flags = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_sub_flags,            tvb, offset, 1, FALSE);
        fgc_ether_publish_tree_flags = proto_item_add_subtree(fgc_ether_publish_tree, ett_fgc_ether_publish_sub_flags);
        proto_tree_add_item(fgc_ether_publish_tree_flags, hf_fgc_ether_publish_sub_flags_new_sub,   tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_publish_tree_flags, hf_fgc_ether_publish_sub_flags_set,       tvb, offset, 1, FALSE);
        offset += 1;
        publish_length  -= 1;

        // user

        proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_user, tvb, offset, 1, FALSE);
        offset          += 1;
        publish_length  -= 1;

        // acq_timestamp

        if(flags & FGC_FIELDBUS_FLAGS_FIRST_PKT && publish_length >= 8)
        {
            ti = proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_acq_timestamp, tvb, offset, 8, FALSE);
            acq_timestamp_s  = tvb_get_ntohl(tvb, offset);
            offset += 4;
            acq_timestamp_us = tvb_get_ntohl(tvb, offset);
            offset += 4;

            localtime_r(&acq_timestamp_s, &acq_time);
            snprintf(time_string, sizeof(time_string), "%02d/%02d/%04d %02d:%02d:%02d",
                     acq_time.tm_mday, acq_time.tm_mon + 1, acq_time.tm_year + 1900,
                     acq_time.tm_hour, acq_time.tm_min, acq_time.tm_sec);
            proto_item_append_text(ti, ", %lu.%06u, %s.%06u",
                                   (unsigned long)acq_timestamp_s, acq_timestamp_us,
                                   time_string, acq_timestamp_us);

            publish_length -= 8;
        }

        // response

        proto_tree_add_item(fgc_ether_publish_tree, hf_fgc_ether_publish_response, tvb, offset,
                            publish_length - sizeof(struct fgc_fieldbus_rsp_header), FALSE);
        offset += publish_length - sizeof(struct fgc_fieldbus_rsp_header);
    }
}

// Register protocol

void proto_register_fgc_ether_publish(void)
{
    // Header field data
    // { &(field id), { name, abbrev, type, display, strings, bitmask, blurb, HFILL } }.

    static hf_register_info hf[] =
    {
        // length

        {
            &hf_fgc_ether_publish_length,
            {
                "length",
                "fgc_ether.payload.publish.length",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "length",
                HFILL
            }
        },

        // flags

        {
            &hf_fgc_ether_publish_flags,
            {
                "flags",
                "fgc_ether.payload.publish.flags",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "flags",
                HFILL
            }
        },

        // flags first_pkt
    
        {
            &hf_fgc_ether_publish_flags_first_pkt,
            {
                "first_pkt",
                "fgc_ether.payload.publish.flags.first_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                0x10,
                "First packet",
                HFILL
            }
        },

        // flags last_pkt

        {
            &hf_fgc_ether_publish_flags_last_pkt,
            {
                "last_pkt",
                "fgc_ether.payload.publish.flags.last_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                0x20,
                "Last packet",
                HFILL
            }
        },

        // flags publish_type

        {
            &hf_fgc_ether_publish_flags_type,
            {
                "type",
                "fgc_ether.payload.publish.flags.type",
                FT_UINT8,
                BASE_HEX,
                VALS(fgc_ether_publish_flags_type_values),
                FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK,
                "Publish type",
                HFILL
            }
        },

        // flags seq_num

        {
            &hf_fgc_ether_publish_flags_seq_num,
            {
                "seq_num",
                "fgc_ether.payload.publish.flags.seq_num",
                FT_UINT8,
                BASE_DEC,
                NULL,
                FGC_FIELDBUS_FLAGS_SEQ_MASK,
                "Sequence number",
                HFILL
            }
        },

        // sub_id

        {
            &hf_fgc_ether_publish_sub_id,
            {
                "sub_id",
                "fgc_ether.payload.publish.sub_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "sub_id",
                HFILL
            }
        },

        // sub_flags

        {
            &hf_fgc_ether_publish_sub_flags,
            {
                "sub_flags",
                "fgc_ether.payload.publish.sub_flags",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "sub_flags",
                HFILL
            }
        },

        // sub flags new sub

        {
            &hf_fgc_ether_publish_sub_flags_new_sub,
            {
                "new_sub",
                "fgc_ether.payload.publish.flags.new_sub",
                FT_BOOLEAN,
                8,
                NULL,
                0x01,
                "New sub",
                HFILL
            }
        },

        // sub flags set

        {
            &hf_fgc_ether_publish_sub_flags_set,
            {
                "set",
                "fgc_ether.payload.publish.flags.set",
                FT_BOOLEAN,
                8,
                NULL,
                0x02,
                "Set",
                HFILL
            }
        },

        // user

        {
            &hf_fgc_ether_publish_user,
            {
                "user",
                "fgc_ether.payload.publish.user",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "user",
                HFILL
            }
        },

        // acq_timestamp

        {
            &hf_fgc_ether_publish_acq_timestamp,
            {
                "acq_timestamp",
                "fgc_ether.payload.publish.acq_timestamp",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "Acquisition time",
                HFILL
            }
        },

        // response

        {
            &hf_fgc_ether_publish_response,
            {
                "response",
                "fgc_ether.payload.publish.response",
                FT_STRING,
                BASE_NONE,
                NULL,
                0x00,
                "response",
                HFILL
            }
        },
    };

    static gint *ett[] =
    {
        &ett_fgc_ether_publish,
        &ett_fgc_ether_publish_flags,
        &ett_fgc_ether_publish_sub_flags,
    };

    // Register protocol

    proto_fgc_ether_publish = proto_register_protocol("FGC_Ether Publish", "FGC_Ether Publish", "fgc_ether_publish");
    proto_register_field_array(proto_fgc_ether_publish, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    register_dissector("fgc_ether_publish", dissect_fgc_ether_publish, proto_fgc_ether_publish);
}

// Register dissector

void proto_reg_handoff_fgc_ether_publish(void)
{
    dissector_handle_t fgc_ether_publish_handle;

    fgc_ether_publish_handle = find_dissector("fgc_ether_publish");
    dissector_add_uint("fgc_ether.payload_type", 4, fgc_ether_publish_handle);
    fgc_ether_publish_data_handle = find_dissector("data");
}

// EOF
