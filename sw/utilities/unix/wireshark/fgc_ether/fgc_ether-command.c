/* fgc_ether-command.c
 * Routines for FGC_Ether command frame disassembly
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <epan/etypes.h>
#include <fgc_ether.h>
#include <glib.h>

static int proto_fgc_ether_command = -1;

static dissector_handle_t fgc_ether_command_data_handle;

static int ett_fgc_ether_command                = -1;

static int hf_fgc_ether_command_length          = -1;
static int hf_fgc_ether_command_flags           = -1;
static int hf_fgc_ether_command_flags_first_pkt = -1;
static int hf_fgc_ether_command_flags_last_pkt  = -1;
static int hf_fgc_ether_command_flags_cmd_type  = -1;
static int hf_fgc_ether_command_user            = -1;
static int hf_fgc_ether_command_command         = -1;

static const value_string fgc_ether_command_flags_cmd_type_values[] =
{
   { FGC_FIELDBUS_FLAGS_SET_CMD,        "Set",          },
   { FGC_FIELDBUS_FLAGS_GET_CMD,        "Get",          },
   { FGC_FIELDBUS_FLAGS_SET_BIN_CMD,    "Set binary",   },
   { FGC_FIELDBUS_FLAGS_SUB_CMD,        "Subscribe",    },
   { FGC_FIELDBUS_FLAGS_UNSUB_CMD,      "Unsubscribe",  },
   { FGC_FIELDBUS_FLAGS_GET_SUB_CMD,    "Get sub",      },
   { 0,                                 NULL            }
};

// Dissect FGC_Ether command payload

#include <stdio.h>

static void dissect_fgc_ether_command(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    guint16     command_length;
    proto_tree  *fgc_ether_command_tree;
    gint        offset = 0;

    // Populate protocol tree

    if(tree)
    {
        fgc_ether_command_tree = proto_item_add_subtree(tree, ett_fgc_ether_command);

        command_length = tvb_get_ntohs(tvb, offset);
        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_length, tvb, offset, 2, FALSE);
        offset += 2;

        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_flags,             tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_flags_first_pkt,   tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_flags_last_pkt,    tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_flags_cmd_type,    tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_user, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_command_tree, hf_fgc_ether_command_command, tvb, offset, command_length - 2, FALSE);
        offset += command_length - 2;
    }
}

// Register protocol

void proto_register_fgc_ether_command(void)
{
    // Header field data
    // { &(field id), { name, abbrev, type, display, strings, bitmask, blurb, HFILL } }.

    static hf_register_info hf[] =
    {
        // length

        {
            &hf_fgc_ether_command_length,
            {
                "length",
                "fgc_ether.payload.command.length",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "length",
                HFILL
            }
        },

        // flags

        {
            &hf_fgc_ether_command_flags,
            {
                "flags",
                "fgc_ether.payload.command.flags",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "flags",
                HFILL
            }
        },

        // flags first_pkt

        {
            &hf_fgc_ether_command_flags_first_pkt,
            {
                "first_pkt",
                "fgc_ether.command.flags.first_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FIELDBUS_FLAGS_FIRST_PKT,
                "First packet",
                HFILL
            }
        },

        // flags last_pkt

        {
            &hf_fgc_ether_command_flags_last_pkt,
            {
                "last_pkt",
                "fgc_ether.command.flags.last_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FIELDBUS_FLAGS_LAST_PKT, 
                "Last packet",
                HFILL
            }
        },

        // flags cmd_type

        {
            &hf_fgc_ether_command_flags_cmd_type,
            {
                "cmd_type",
                "fgc_ether.payload.command.flags.cmd_type",
                FT_UINT8,
                BASE_HEX,
                VALS(fgc_ether_command_flags_cmd_type_values),
                FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK,
                "cmd_type",
                HFILL
            }
        },

        // user

        {
            &hf_fgc_ether_command_user,
            {
                "user",
                "fgc_ether.payload.command.user",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "user",
                HFILL
            }
        },

        // command

        {
            &hf_fgc_ether_command_command,
            {
                "command",
                "fgc_ether.payload.command.command",
                FT_STRING,
                BASE_NONE,
                NULL,
                0x00,
                "command",
                HFILL
            }
        },
    };

    static gint *ett[] =
    {
        &ett_fgc_ether_command,
    };

    // Register protocol

    proto_fgc_ether_command = proto_register_protocol("FGC_Ether Command", "FGC_Ether Command", "fgc_ether_command");
    proto_register_field_array(proto_fgc_ether_command, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    register_dissector("fgc_ether_command", dissect_fgc_ether_command, proto_fgc_ether_command);
}

// Register dissector

void proto_reg_handoff_fgc_ether_command(void)
{
    dissector_handle_t fgc_ether_command_handle;

    fgc_ether_command_handle = find_dissector("fgc_ether_command");
    dissector_add_uint("fgc_ether.payload_type", 2, fgc_ether_command_handle);
    fgc_ether_command_data_handle = find_dissector("data");
}

// EOF
