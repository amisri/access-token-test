/* fgc_ether-status.c
 * Routines for FGC_Ether status frame disassembly
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <epan/etypes.h>
#include <fgc_ether.h>
#include <glib.h>

static int proto_fgc_ether_status = -1;

static dissector_handle_t fgc_ether_status_data_handle;

static int ett_fgc_ether_status                         = -1;

static int ett_fgc_ether_status_ack                     = -1;
static int hf_fgc_ether_status_ack                      = -1;
static int hf_fgc_ether_status_ack_status               = -1;
static int hf_fgc_ether_status_ack_ack_time             = -1;
static int hf_fgc_ether_status_ack_ack_rd_pkt           = -1;
static int hf_fgc_ether_status_ack_cmd_tog              = -1;
static int hf_fgc_ether_status_ack_pll                  = -1;
static int hf_fgc_ether_status_ack_self_pm_req          = -1;
static int hf_fgc_ether_status_ack_ext_pm_req           = -1;

static int hf_fgc_ether_status_class_id                 = -1;
static int hf_fgc_ether_status_cmd_stat                 = -1;
static int hf_fgc_ether_status_runlog                   = -1;

static int ett_fgc_ether_status_diag                    = -1;
static int hf_fgc_ether_status_diag                     = -1;
static int hf_fgc_ether_status_diag_chan                = -1;
static int hf_fgc_ether_status_diag_data                = -1;

static int hf_fgc_ether_status_class_data               = -1;

static int hf_fgc_ether_status_rterm                    = -1;

static const value_string fgc_ether_status_cmd_stat_values[] =
{
   { 0, "OK_NO_RSP",    },
   { 1, "OK_RSP",       },
   { 2, "RECEIVING",    },
   { 3, "EXECUTING",    },
   { 0,  NULL           }
};

// Dissect FGC_Ether status payload

static void dissect_fgc_ether_status(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    proto_tree      *fgc_ether_status_tree;
    proto_tree      *fgc_ether_status_tree_ack;
    proto_tree      *fgc_ether_status_tree_diag;
    unsigned int    i;
    gint            offset = 0;
    proto_item      *ti;

    // Populate protocol tree

    if(tree)
    {
        fgc_ether_status_tree = proto_item_add_subtree(tree, ett_fgc_ether_status);

        // Ack

        ti = proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_ack, tvb, offset, 1, FALSE);
        fgc_ether_status_tree_ack = proto_item_add_subtree(ti, ett_fgc_ether_status_ack);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_status,      tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_ack_time,    tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_ack_rd_pkt,  tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_cmd_tog,     tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_pll,         tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_self_pm_req, tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_status_tree_ack, hf_fgc_ether_status_ack_ext_pm_req,  tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_class_id, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_cmd_stat, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_runlog, tvb, offset, 1, FALSE);
        offset += 1;

        // Diag

        ti = proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_diag, tvb, offset, 12, FALSE);
        fgc_ether_status_tree_diag = proto_item_add_subtree(ti, ett_fgc_ether_status_diag);

        for(i = 0 ; i < 4 ; i++)
        {
            proto_tree_add_item(fgc_ether_status_tree_diag, hf_fgc_ether_status_diag_chan, tvb, offset, 1, FALSE);
            offset += 1;

            proto_tree_add_item(fgc_ether_status_tree_diag, hf_fgc_ether_status_diag_data, tvb, offset, 2, FALSE);
            offset += 2;
        }

        // Class data

        proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_class_data, tvb, offset, 40, FALSE);
        offset += 40;

        // Remote terminal data

        proto_tree_add_item(fgc_ether_status_tree, hf_fgc_ether_status_rterm, tvb, offset, 21, FALSE);
        offset += 21;

        // Skip reserved bytes

        offset += 3;
    }
}

// Register protocol

void proto_register_fgc_ether_status(void)
{
    // Header field data
    // { &(field id), { name, abbrev, type, display, strings, bitmask, blurb, HFILL } }.

    static hf_register_info hf[] =
    {
        // ack

        {
            &hf_fgc_ether_status_ack,
            {
                "ack",
                "fgc_ether.payload.status.ack",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "ack",
                HFILL
            }
        },

        // ack status

        {
            &hf_fgc_ether_status_ack_status,
            {
                "status",
                "fgc_ether.payload.status.ack.status",
                FT_UINT8,
                BASE_DEC,
                NULL,
                FGC_ACK_STATUS,
                "Status counter",
                HFILL
            }
        },

        // ack ack_time

        {
            &hf_fgc_ether_status_ack_ack_time,
            {
                "ack_time",
                "fgc_ether.payload.status.ack.ack_time",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_ACK_TIME,
                "Acknowledge time",
                HFILL
            }
        },

        // ack ack_rd_pkt

        {
            &hf_fgc_ether_status_ack_ack_rd_pkt,
            {
                "ack_rd_pkt",
                "fgc_ether.payload.status.ack.ack_rd_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_ACK_RD_PKT,
                "Acknowledge real-time",
                HFILL
            }
        },

        // ack cmd_tog

        {
            &hf_fgc_ether_status_ack_cmd_tog,
            {
                "cmd_tog",
                "fgc_ether.payload.status.ack.cmd_tog",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_ACK_CMD_TOG,
                "Command toggle",
                HFILL
            }
        },

        // ack pll

        {
            &hf_fgc_ether_status_ack_pll,
            {
                "pll",
                "fgc_ether.payload.status.ack.pll",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_ACK_PLL,
                "PLL",
                HFILL
            }
        },

        // ack self_pm_req

        {
            &hf_fgc_ether_status_ack_self_pm_req,
            {
                "self_pm_req",
                "fgc_ether.payload.status.ack.self_pm_req",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_SELF_PM_REQ,
                "Self-triggered PM request",
                HFILL
            }
        },

        // ack ext_pm_req

        {
            &hf_fgc_ether_status_ack_ext_pm_req,
            {
                "self_pm_req",
                "fgc_ether.payload.status.ack.ext_pm_req",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_EXT_PM_REQ,
                "Externally-triggered PM request",
                HFILL
            }
        },

        // class_id

        {
            &hf_fgc_ether_status_class_id,
            {
                "class_id",
                "fgc_ether.payload.status.class_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "class_id",
                HFILL
            }
        },

        // cmd_stat

        {
            &hf_fgc_ether_status_cmd_stat,
            {
                "cmd_stat",
                "fgc_ether.payload.status.cmd_stat",
                FT_UINT8,
                BASE_HEX,
                VALS(fgc_ether_status_cmd_stat_values),
                0x00,
                "cmd_stat",
                HFILL
            }
        },

        // runlog

        {
            &hf_fgc_ether_status_runlog,
            {
                "runlog",
                "fgc_ether.payload.status.runlog",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "runlog",
                HFILL
            }
        },

        // diag

        {
            &hf_fgc_ether_status_diag,
            {
                "diag",
                "fgc_ether.payload.status.diag",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "diag",
                HFILL
            }
        },

        // diag_chan

        {
            &hf_fgc_ether_status_diag_chan,
            {
                "diag_chan",
                "fgc_ether.payload.status.diag_chan",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "diag_chan",
                HFILL
            }
        },

        // diag_data

        {
            &hf_fgc_ether_status_diag_data,
            {
                "diag_data",
                "fgc_ether.payload.status.diag_data",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "diag_data",
                HFILL
            }
        },

        // class_data

        {
            &hf_fgc_ether_status_class_data,
            {
                "class_data",
                "fgc_ether.payload.status.class_data",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "class_data",
                HFILL
            }
        },

        // rterm

        {
            &hf_fgc_ether_status_rterm,
            {
                "rterm",
                "fgc_ether.payload.status.rterm",
                FT_STRING,
                BASE_NONE,
                NULL,
                0x00,
                "rterm",
                HFILL
            }
        },
    };

    static gint *ett[] =
    {
        &ett_fgc_ether_status,
        &ett_fgc_ether_status_ack,
        &ett_fgc_ether_status_diag,
    };

    // Register protocol

    proto_fgc_ether_status = proto_register_protocol("FGC_Ether Status", "FGC_Ether Status", "fgc_ether_status");
    proto_register_field_array(proto_fgc_ether_status, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    register_dissector("fgc_ether_status", dissect_fgc_ether_status, proto_fgc_ether_status);
}

// Register dissector

void proto_reg_handoff_fgc_ether_status(void)
{
    dissector_handle_t fgc_ether_status_handle;

    fgc_ether_status_handle = find_dissector("fgc_ether_status");
    dissector_add_uint("fgc_ether.payload_type", 1, fgc_ether_status_handle);
    fgc_ether_status_data_handle = find_dissector("data");
}

// EOF
