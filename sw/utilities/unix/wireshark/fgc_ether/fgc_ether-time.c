/* fgc_ether-time.c
 * Routines for FGC_Ether time frame disassembly
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <arpa/inet.h>
#include <epan/packet.h>
#include <epan/etypes.h>
#include <fgc_ether.h>
#include <fgc_event.h>
#include <glib.h>
#include <stdio.h>

static int proto_fgc_ether_time = -1;

static dissector_handle_t fgc_ether_time_data_handle;

static int ett_fgc_ether_time                           = -1;
static int hf_fgc_ether_time_fgc_id                     = -1;
static int hf_fgc_ether_time_fgc_char                   = -1;
static int hf_fgc_ether_time_unixtime                   = -1;
static int hf_fgc_ether_time_ms                         = -1;
static int hf_fgc_ether_time_namedb_crc                 = -1;
static int hf_fgc_ether_time_old_adv_code_version       = -1;
static int hf_fgc_ether_time_code_var_class_id          = -1;
static int hf_fgc_ether_time_code_var_id                = -1;
static int hf_fgc_ether_time_code_var_idx               = -1;
static int hf_fgc_ether_time_adv_list_idx               = -1;
static int hf_fgc_ether_time_adv_list_len               = -1;
static int hf_fgc_ether_time_adv_code_class_id          = -1;
static int hf_fgc_ether_time_adv_code_id                = -1;
static int hf_fgc_ether_time_adv_code_crc               = -1;
static int hf_fgc_ether_time_adv_code_len_blk           = -1;
static int hf_fgc_ether_time_runlog_idx                 = -1;

static int ett_fgc_ether_time_flags                     = -1;
static int hf_fgc_ether_time_flags                      = -1;
static int hf_fgc_ether_time_flags_sync_log             = -1;
static int hf_fgc_ether_time_flags_pc_permit            = -1;
static int hf_fgc_ether_time_flags_sector_access        = -1;
static int hf_fgc_ether_time_flags_pm_enabled           = -1;

static int hf_fgc_ether_time_adv_code_version           = -1;
static int ett_fgc_ether_time_events                    = -1;
static int hf_fgc_ether_time_events                     = -1;
static int ett_fgc_ether_time_event                     = -1;
static int hf_fgc_ether_time_event                      = -1;
static int hf_fgc_ether_time_event_type                 = -1;
static int hf_fgc_ether_time_payload                    = -1;
static int hf_fgc_ether_time_delay_ms                   = -1;

static int ett_fgc_ether_time_rt                        = -1;
static int hf_fgc_ether_time_rt                         = -1;
static int hf_fgc_ether_time_rt_ref                     = -1;
static int hf_fgc_ether_time_code                       = -1;

static const value_string fgc_ether_time_event_types[] =
{
   { 0,                         "none",                 },
   { FGC_EVT_PM,                "PM",                   },
   { FGC_EVT_START,             "Start",                },
   { FGC_EVT_ABORT,             "Abort",                },
   { FGC_EVT_NEXT_CYC_USER,     "Next cycle user",      },
   { FGC_EVT_NEXT_CYC_LEN,      "Next cycle length",    },
   { FGC_EVT_SSC,               "SSC",                  },
   { FGC_EVT_NEXT_DEST,         "Next destination",     },
   { FGC_EVT_CYCLE_TAG_HIGH,    "Cycle tag high byte",  },
   { FGC_EVT_CYCLE_TAG_LOW,     "Cycle tag low byte",   },
   { FGC_EVT_INJECTION,         "Injection",            },
   { FGC_EVT_EXTRACTION,        "Extraction",           },
   { 0,                         NULL                    }
};

// Dissect FGC_Ether time payload

static void dissect_fgc_ether_time(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    proto_tree      *fgc_ether_time_tree;
    proto_tree      *fgc_ether_time_tree_event;
    proto_tree      *fgc_ether_time_tree_events;
    proto_tree      *fgc_ether_time_tree_rt;
    proto_tree      *fgc_ether_time_tree_flags;
    struct tm       fgc_time;
    unsigned int    i;
    gint            offset = 0;
    proto_item      *ti;
    char            time_string[32];
    time_t          unixtime;

    // Populate protocol tree

    if(tree)
    {
        fgc_ether_time_tree = proto_item_add_subtree(tree, ett_fgc_ether_time);

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_fgc_id, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_fgc_char, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_ms, tvb, offset, 2, FALSE);
        offset += 2;

        ti = proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_unixtime, tvb, offset, 4, FALSE);
        unixtime = tvb_get_ntohl(tvb, offset);
        localtime_r(&unixtime, &fgc_time);
        snprintf(time_string, sizeof(time_string), "%02d/%02d/%04d %02d:%02d:%02d",
                 fgc_time.tm_mday, fgc_time.tm_mon + 1, fgc_time.tm_year + 1900,
                 fgc_time.tm_hour, fgc_time.tm_min, fgc_time.tm_sec);
        proto_item_append_text(ti, ", %s", time_string);
        offset += 4;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_namedb_crc, tvb, offset, 2, FALSE);
        offset += 2;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_old_adv_code_version, tvb, offset, 2, FALSE);
        offset += 2;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_code_var_class_id, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_code_var_id, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_code_var_idx, tvb, offset, 2, FALSE);
        offset += 2;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_list_idx, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_list_len, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_code_class_id, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_code_id, tvb, offset, 1, FALSE);
        offset += 1;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_code_crc, tvb, offset, 2, FALSE);
        offset += 2;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_code_len_blk, tvb, offset, 2, FALSE);
        offset += 2;

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_runlog_idx, tvb, offset, 2, FALSE);
        offset += 2;

        ti = proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_flags, tvb, offset, 1, FALSE);
        fgc_ether_time_tree_flags = proto_item_add_subtree(ti, ett_fgc_ether_time_flags);

        proto_tree_add_item(fgc_ether_time_tree_flags, hf_fgc_ether_time_flags_sync_log,        tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_time_tree_flags, hf_fgc_ether_time_flags_pc_permit,       tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_time_tree_flags, hf_fgc_ether_time_flags_sector_access,   tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_time_tree_flags, hf_fgc_ether_time_flags_pm_enabled,      tvb, offset, 1, FALSE);
        offset += 1;

        // Skip reserved byte

        offset += 1;

        ti = proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_adv_code_version, tvb, offset, 4, FALSE);
        unixtime = tvb_get_ntohl(tvb, offset);
        localtime_r(&unixtime, &fgc_time);
        snprintf(time_string, sizeof(time_string), "%02d/%02d/%04d %02d:%02d:%02d",
                 fgc_time.tm_mday, fgc_time.tm_mon + 1, fgc_time.tm_year + 1900,
                 fgc_time.tm_hour, fgc_time.tm_min, fgc_time.tm_sec);
        proto_item_append_text(ti, ", %s", time_string);
        offset += 4;

        // Events

        ti = proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_events, tvb, offset, 32, FALSE);
        fgc_ether_time_tree_events = proto_item_add_subtree(ti, ett_fgc_ether_time_events);

        for(i = 0 ; i < 8 ; i++)
        {
            ti = proto_tree_add_item(fgc_ether_time_tree_events, hf_fgc_ether_time_event, tvb, offset, 4, FALSE);
            fgc_ether_time_tree_event = proto_item_add_subtree(ti, ett_fgc_ether_time_event);

            proto_tree_add_item(fgc_ether_time_tree_event, hf_fgc_ether_time_event_type,    tvb, offset, 1, FALSE);
            offset++;
            proto_tree_add_item(fgc_ether_time_tree_event, hf_fgc_ether_time_payload,       tvb, offset, 1, FALSE);
            offset++;
            proto_tree_add_item(fgc_ether_time_tree_event, hf_fgc_ether_time_delay_ms,      tvb, offset, 2, FALSE);
            offset += 2;
        }

        // Real-time references

        ti = proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_rt, tvb, offset, 256, FALSE);
        fgc_ether_time_tree_rt = proto_item_add_subtree(ti, ett_fgc_ether_time_rt);

        for(i = 0 ; i < 64 ; i++)
        {
            proto_tree_add_item(fgc_ether_time_tree_rt, hf_fgc_ether_time_rt_ref, tvb, offset, 4, FALSE);
            offset += 4;
        }

        // Code

        proto_tree_add_item(fgc_ether_time_tree, hf_fgc_ether_time_code, tvb, offset, 1024, FALSE);
        offset += 1024;
    }
}

// Register protocol

void proto_register_fgc_ether_time(void)
{
    // Header field data
    // { &(field id), { name, abbrev, type, display, strings, bitmask, blurb, HFILL } }.

    static hf_register_info hf[] =
    {
        // fgc_id

        {
            &hf_fgc_ether_time_fgc_id,
            {
                "fgc_id",
                "fgc_ether.payload.time.fgc_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "FGC ID",
                HFILL
            }
        },

        // fgc_char

        {
            &hf_fgc_ether_time_fgc_char,
            {
                "fgc_char",
                "fgc_ether.payload.time.fgc_char",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "FGC character",
                HFILL
            }
        },

        // millisecond

        {
            &hf_fgc_ether_time_ms,
            {
                "ms",
                "fgc_ether.payload.time.ms",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Millisecond",
                HFILL
            }
        },

        // unixtime

        {
            &hf_fgc_ether_time_unixtime,
            {
                "unixtime",
                "fgc_ether.payload.time.unixtime",
                FT_UINT32,
                BASE_DEC,
                NULL,
                0x00000000,
                "Unix time",
                HFILL
            }
        },

        // namedb_crc

        {
            &hf_fgc_ether_time_namedb_crc,
            {
                "namedb_crc",
                "fgc_ether.payload.time.namedb_crc",
                FT_UINT16,
                BASE_HEX,
                NULL,
                0x0000,
                "NameDB CRC",
                HFILL
            }
        },

        // old_adv_code_version

        {
            &hf_fgc_ether_time_old_adv_code_version,
            {
                "old_adv_code_version",
                "fgc_ether.payload.time.old_adv_code_version",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "Old advertised code version",
                HFILL
            }
        },

        // code_var_class_id

        {
            &hf_fgc_ether_time_code_var_class_id,
            {
                "code_var_class_id",
                "fgc_ether.payload.time.code_var_class_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Code variable class ID",
                HFILL
            }
        },

        // code_var_id

        {
            &hf_fgc_ether_time_code_var_id,
            {
                "code_var_id",
                "fgc_ether.payload.time.code_var_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Code variable ID",
                HFILL
            }
        },

        // code_var_idx

        {
            &hf_fgc_ether_time_code_var_idx,
            {
                "code_var_idx",
                "fgc_ether.payload.time.code_var_idx",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "Code variable block index",
                HFILL
            }
        },

        // adv_list_idx

        {
            &hf_fgc_ether_time_adv_list_idx,
            {
                "adv_list_idx",
                "fgc_ether.payload.time.adv_list_idx",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Advertised code list index",
                HFILL
            }
        },

        // adv_list_len
    
        {
            &hf_fgc_ether_time_adv_list_len,
            {
                "adv_list_len",
                "fgc_ether.payload.time.adv_list_len",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Advertised code list length",
                HFILL
            }
        },

        // adv_code_class_id

        {
            &hf_fgc_ether_time_adv_code_class_id,
            {
                "adv_code_class_id",
                "fgc_ether.payload.time.adv_code_class_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Advertised code class ID",
                HFILL
            }
        },

        // adv_code_id

        {
            &hf_fgc_ether_time_adv_code_id,
            {
                "adv_code_id",
                "fgc_ether.payload.time.adv_code_id",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Advertised code ID",
                HFILL
            }
        },

        // adv_code_crc

        {
            &hf_fgc_ether_time_adv_code_crc,
            {
                "adv_code_crc",
                "fgc_ether.payload.time.adv_code_crc",
                FT_UINT16,
                BASE_HEX,
                NULL,
                0x0000,
                "Advertised code CRC",
                HFILL
            }
        },

        // adv_code_len_blk

        {
            &hf_fgc_ether_time_adv_code_len_blk,
            {
                "adv_code_len_blk",
                "fgc_ether.payload.time.adv_code_len_blk",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "Advertised code length in 32 byte blocks",
                HFILL
            }
        },

        // runlog_idx

        {
            &hf_fgc_ether_time_runlog_idx,
            {
                "runlog_idx",
                "fgc_ether.payload.time.runlog_idx",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "Run log index",
                HFILL
            }
        },

        // flags

        {
            &hf_fgc_ether_time_flags,
            {
                "flags",
                "fgc_ether.payload.time.flags",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "Sync flags",
                HFILL
            }
        },

        // flags sync log

        {
            &hf_fgc_ether_time_flags_sync_log,
            {
                "sync log",
                "fgc_ether.payload.time.flags.sync_log",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FLAGS_SYNC_LOG,
                "sync_log",
                HFILL
            }
        },

        // flags pc_permit

        {
            &hf_fgc_ether_time_flags_pc_permit,
            {
                "pc_permit",
                "fgc_ether.payload.time.flags.pc_permit",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FLAGS_PC_PERMIT,
                "pc_permit",
                HFILL 
            }
        },

        // flags sector_access

        {
            &hf_fgc_ether_time_flags_sector_access,
            {
                "sector_access",
                "fgc_ether.payload.time.flags.sector_access",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FLAGS_SECTOR_ACCESS,
                "sector_access",
                HFILL
            }
        },

        // flags pm_enabled

        {
            &hf_fgc_ether_time_flags_pm_enabled,
            {
                "pm_enabled",
                "fgc_ether.payload.time.flags.pm_enabled",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FLAGS_PM_ENABLED,
                "pm_enabled",
                HFILL
            }
        },

        // adv_code_version

        {
            &hf_fgc_ether_time_adv_code_version,
            {
                "adv_code_version",
                "fgc_ether.payload.time.adv_code_version",
                FT_UINT32,
                BASE_DEC,
                NULL,
                0x00000000,
                "Advertised code version",
                HFILL
            }
        },

        // events

        {
            &hf_fgc_ether_time_events,
            {
                "events",
                "fgc_ether.payload.time.events",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "Events",
                HFILL
            }
        },

        // event

        {
            &hf_fgc_ether_time_event,
            {
                "event",
                "fgc_ether.payload.time.event",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "Event",
                HFILL
            }
        },

        // event type

        {
            &hf_fgc_ether_time_event_type,
            {
                "event_type",
                "fgc_ether.payload.time.event.type",
                FT_UINT8,
                BASE_DEC,
                VALS(fgc_ether_time_event_types),
                0x00,
                "Event type",
                HFILL
            }
        },

        // payload

        {
            &hf_fgc_ether_time_payload,
            {
                "payload",
                "fgc_ether.payload.time.event.payload",
                FT_UINT8,
                BASE_DEC,
                NULL,
                0x00,
                "Payload",
                HFILL
            }
        },

        // delay_ms

        {
            &hf_fgc_ether_time_delay_ms,
            {
                "delay_ms",
                "fgc_ether.payload.time.event.delay_ms",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "Delay ms",
                HFILL
            }
        },

        // real-time references

        {
            &hf_fgc_ether_time_rt,
            {
                "rt",
                "fgc_ether.payload.time.rt",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "RT",
                HFILL
            }
        },

        // rt_ref

        {
            &hf_fgc_ether_time_rt_ref,
            {
                "rt_ref",
                "fgc_ether.payload.time.event.rt_ref",
                FT_FLOAT,
                BASE_NONE,
                NULL,
                0x00000000,
                "RT ref",
                HFILL
            }
        },

        // code

        {
            &hf_fgc_ether_time_code,
            {
                "code",
                "fgc_ether.payload.time.code",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "Code",
                HFILL
            }
        },
    };

    static gint *ett[] =
    {
        &ett_fgc_ether_time,
        &ett_fgc_ether_time_flags,
        &ett_fgc_ether_time_event,
        &ett_fgc_ether_time_events,
        &ett_fgc_ether_time_rt,
    };

    // Register protocol

    proto_fgc_ether_time = proto_register_protocol("FGC_Ether Time", "FGC_Ether Time", "fgc_ether_time");
    proto_register_field_array(proto_fgc_ether_time, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    register_dissector("fgc_ether_time", dissect_fgc_ether_time, proto_fgc_ether_time);
}

// Register dissector

void proto_reg_handoff_fgc_ether_time(void)
{
    dissector_handle_t fgc_ether_time_handle;

    fgc_ether_time_handle = find_dissector("fgc_ether_time");
    dissector_add_uint("fgc_ether.payload_type", 0, fgc_ether_time_handle);
    fgc_ether_time_data_handle = find_dissector("data");
}

// EOF
