/* fgc_ether-header.c
 * Routines for FGC_Ether header disassembly
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/column-utils.h>
#include <epan/packet.h>
#include <epan/etypes.h>
#include <fgc_ether.h>
#include <glib.h>

/* Define the FGC_Ether proto */
static int proto_fgc_ether = -1;

static proto_tree *fgc_ether_tree;

static dissector_table_t fgc_ether_dissector_table;
static dissector_handle_t fgc_ether_data_handle;

/* Define the tree for the FGC_Ether header */
static int ett_fgc_ether_header             = -1;
static int hf_fgc_ether_header_payload_type = -1;

static const value_string fgc_ether_payload_types[] =
{
   { FGC_ETHER_PAYLOAD_TIME,    "Time",     },
   { FGC_ETHER_PAYLOAD_STATUS,  "Status",   },
   { FGC_ETHER_PAYLOAD_CMD,     "Command",  },
   { FGC_ETHER_PAYLOAD_RSP,     "Response", },
   { FGC_ETHER_PAYLOAD_PUB,     "Publish",  },
   { 0,                         NULL        }
};

// Dissect FGC_Ether header

static void dissect_fgc_ether_header(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    tvbuff_t            *next_tvb;
    gint                offset = 0;
    guint8              payload_type;
    proto_item          *ti;

    // Set protocol name

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "FGC_Ether");

    // Get the payload type

    payload_type = tvb_get_guint8(tvb, 0);

    // Set info column content if displayed

    if(check_col(pinfo->cinfo, COL_INFO))
    {
        col_clear(pinfo->cinfo, COL_INFO);
        col_add_fstr(pinfo->cinfo, COL_INFO, "%s",
                     val_to_str(payload_type, fgc_ether_payload_types, "Unknown (%u)"));
    }

    // Populate protocol tree

    if(tree)
    {
        ti = proto_tree_add_item(tree, proto_fgc_ether, tvb, offset, 1500, FALSE);
        fgc_ether_tree = proto_item_add_subtree(ti, ett_fgc_ether_header);
        proto_item_append_text(ti, ", %s", val_to_str(payload_type, fgc_ether_payload_types, "Unknown (0x%02x)"));

        proto_tree_add_item(fgc_ether_tree, hf_fgc_ether_header_payload_type, tvb, offset, 1, FALSE);
        offset++;

        // Skip reserved

        offset++;
    }

    // Call sub-dissectors

    next_tvb = tvb_new_subset_remaining(tvb, offset);

    if(!dissector_try_uint(fgc_ether_dissector_table, payload_type, next_tvb, pinfo, fgc_ether_tree))
    {
        // No sub dissector wanted to handle this payload, decode it as general data instead

        call_dissector(fgc_ether_data_handle, next_tvb, pinfo, tree);
    }
}

// Register protocol

void proto_register_fgc_ether(void)
{
    // { &(field id), { name, abbrev, type, display, strings, bitmask, blurb, HFILL } }.

    static hf_register_info hf[] =
    {
        // payload type

        {
            &hf_fgc_ether_header_payload_type,
            {
                "payload_type",
                "fgc_ether.payload_type",
                FT_UINT8,
                BASE_DEC,
                VALS(fgc_ether_payload_types),
                0x00,
                "Payload Type",
                HFILL
            }
        },
    };

    static gint *ett[] =
    {
        &ett_fgc_ether_header,
    };

    // Register protocol

    proto_fgc_ether = proto_register_protocol("FGC_Ether", "FGC_Ether","fgc_ether");
    proto_register_field_array(proto_fgc_ether, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    register_dissector("fgc_ether", dissect_fgc_ether_header, proto_fgc_ether);

    // Define a handle for sub dissectors

    fgc_ether_dissector_table = register_dissector_table("fgc_ether.payload_type",
                                                         "FGC_Ether payload type", FT_UINT8, BASE_DEC);
}

// Register dissector

void proto_reg_handoff_fgc_ether(void)
{
    dissector_handle_t fgc_ether_handle;

    fgc_ether_handle = find_dissector("fgc_ether");
    dissector_add_uint("ethertype", FGC_ETHER_TYPE, fgc_ether_handle);
    fgc_ether_data_handle = find_dissector("data");
}

// EOF
