/* fgc_ether-response.c
 * Routines for FGC_Ether response frame disassembly
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <epan/etypes.h>
#include <fgc_ether.h>
#include <glib.h>
#include <stdio.h>

static int proto_fgc_ether_response = -1;

static dissector_handle_t fgc_ether_response_data_handle;

static int ett_fgc_ether_response                   = -1;

static int hf_fgc_ether_response_length             = -1;

static int ett_fgc_ether_response_flags             = -1;
static int hf_fgc_ether_response_flags              = -1;
static int hf_fgc_ether_response_flags_first_pkt    = -1;
static int hf_fgc_ether_response_flags_last_pkt     = -1;
static int hf_fgc_ether_response_flags_type         = -1;
static int hf_fgc_ether_response_flags_seq_num      = -1;

static int hf_fgc_ether_response_acq_timestamp      = -1;
static int hf_fgc_ether_response_response           = -1;

static const value_string fgc_ether_response_flags_rsp_type_values[] =
{
   { 0, "Command",      },
   { 1, "Pub direct",   },
   { 2, "Pub get",      },
   { 0, NULL            }
};

// Dissect FGC_Ether response payload

static void dissect_fgc_ether_response(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    struct tm   acq_time;
    time_t      acq_timestamp_s;
    guint32     acq_timestamp_us;
    proto_tree  *fgc_ether_response_tree;
    guint8      flags;
    gint        offset = 0;
    guint16     response_length;
    proto_item  *ti;
    char        time_string[32];

    // Populate protocol tree

    if(tree)
    {
        fgc_ether_response_tree = proto_item_add_subtree(tree, ett_fgc_ether_response);

        response_length = tvb_get_ntohs(tvb, offset);
        proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_length, tvb, offset, 2, FALSE);
        offset += 2;

        // flags

        flags = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_flags,      tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_flags_first_pkt, tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_flags_last_pkt,  tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_flags_type,      tvb, offset, 1, FALSE);
        proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_flags_seq_num,   tvb, offset, 1, FALSE);
        offset += 1;

        // Skip reserved

        offset += 1;

        // acq_timestamp

        if(flags & FGC_FIELDBUS_FLAGS_FIRST_PKT && response_length >= 8)
        {
            ti = proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_acq_timestamp, tvb, offset, 8, FALSE);
            acq_timestamp_s  = tvb_get_ntohl(tvb, offset);
            offset += 4;
            acq_timestamp_us = tvb_get_ntohl(tvb, offset);
            offset += 4;

            localtime_r(&acq_timestamp_s, &acq_time);
            snprintf(time_string, sizeof(time_string), "%02d/%02d/%04d %02d:%02d:%02d",
                     acq_time.tm_mday, acq_time.tm_mon + 1, acq_time.tm_year + 1900,
                     acq_time.tm_hour, acq_time.tm_min, acq_time.tm_sec);
            proto_item_append_text(ti, ", %lu.%06u, %s.%06u",
                                   (unsigned long)acq_timestamp_s, acq_timestamp_us,
                                   time_string, acq_timestamp_us);

            response_length -= 8;
        }

        // response

        proto_tree_add_item(fgc_ether_response_tree, hf_fgc_ether_response_response, tvb, offset,
                            response_length - sizeof(struct fgc_fieldbus_rsp_header), FALSE);
        offset += response_length - sizeof(struct fgc_fieldbus_rsp_header);
    }
}

// Register protocol

void proto_register_fgc_ether_response(void)
{
    // Header field data
    // { &(field id), { name, abbrev, type, display, strings, bitmask, blurb, HFILL } }.

    static hf_register_info hf[] =
    {
        // length

        {
            &hf_fgc_ether_response_length,
            {
                "length",
                "fgc_ether.payload.response.length",
                FT_UINT16,
                BASE_DEC,
                NULL,
                0x0000,
                "length",
                HFILL
            }
        },

        // flags

        {
            &hf_fgc_ether_response_flags,
            {
                "flags",
                "fgc_ether.payload.response.flags",
                FT_UINT8,
                BASE_HEX,
                NULL,
                0x00,
                "flags",
                HFILL
            }
        },

        // flags first_pkt
    
        {
            &hf_fgc_ether_response_flags_first_pkt,
            {
                "first_pkt",
                "fgc_ether.payload.response.flags.first_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FIELDBUS_FLAGS_FIRST_PKT,
                "First packet",
                HFILL
            }
        },

        // flags last_pkt

        {
            &hf_fgc_ether_response_flags_last_pkt,
            {
                "last_pkt",
                "fgc_ether.payload.response.flags.last_pkt",
                FT_BOOLEAN,
                8,
                NULL,
                FGC_FIELDBUS_FLAGS_LAST_PKT,
                "Last packet",
                HFILL
            }
        },

        // flags response_type

        {
            &hf_fgc_ether_response_flags_type,
            {
                "type",
                "fgc_ether.payload.response.flags.type",
                FT_UINT8,
                BASE_HEX,
                VALS(fgc_ether_response_flags_rsp_type_values),
                FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK,
                "Response Type",
                HFILL
            }
        },

        // flags seq_num
    
        {
            &hf_fgc_ether_response_flags_seq_num,
            {
                "seq_num",
                "fgc_ether.payload.response.flags.seq_num",
                FT_UINT8,
                BASE_DEC,
                NULL,
                FGC_FIELDBUS_FLAGS_SEQ_MASK,
                "Sequence number",
                HFILL
            }
        },

        // acq_timestamp

        {
            &hf_fgc_ether_response_acq_timestamp,
            {
                "acq_timestamp",
                "fgc_ether.payload.response.acq_timestamp",
                FT_BYTES,
                BASE_NONE,
                NULL,
                0x00,
                "Acquisition time",
                HFILL
            }
        },

        // response

        {
            &hf_fgc_ether_response_response,
            {
                "response",
                "fgc_ether.payload.response.response",
                FT_STRING,
                BASE_NONE,
                NULL,
                0x00,
                "response",
                HFILL
            }
        },
    };

    static gint *ett[] =
    {
        &ett_fgc_ether_response,
        &ett_fgc_ether_response_flags,
    };

    // Register protocol

    proto_fgc_ether_response = proto_register_protocol("FGC_Ether Response", "FGC_Ether Response", "fgc_ether_response");
    proto_register_field_array(proto_fgc_ether_response, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    register_dissector("fgc_ether_response", dissect_fgc_ether_response, proto_fgc_ether_response);
}

// Register dissector

void proto_reg_handoff_fgc_ether_response(void)
{
    dissector_handle_t fgc_ether_response_handle;

    fgc_ether_response_handle = find_dissector("fgc_ether_response");
    dissector_add_uint("fgc_ether.payload_type", 3, fgc_ether_response_handle);
    fgc_ether_response_data_handle = find_dissector("data");
}

// EOF
