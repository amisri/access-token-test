/*
 *  Filename: newave3prog.c
 *
 *  Purpose:  Program Newave 3 cards
 *
 *  Author:   Stephen Page
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mugefhw/newave3.h"

#define FILENAME_DEFAULT "/user/pclhc/etc/mugef/newave/firmware/operational"

int main(int argc, char *argv[])
{
    unsigned int        c;
    struct newave3_card card[NEWAVE3_NUM_ADDRS];
    unsigned int        i;
    char                filename_default[] = FILENAME_DEFAULT;
    char                *filename = filename_default;
    int                 force = 0;

    // Process command-line options

    while((c = getopt(argc, argv, "?b:hf")) != -1)
    {
        switch(c)
        {
            case '?':
            case 'h':
                printf("Usage: %s [-h] [-b<FILENAME>] [-f]\n\n%s\n", argv[0],
                       "            -b<FILENAME>    Set output file for log\n"
                       "            -h              Help\n"
                       "            -f              Force update\n"
                      );
                exit(1);

            case 'b':
                if(optarg)
                {
                    filename = optarg;
                }
                else
                {
                    fprintf(stderr, "Filename not specified\n\n");
                    exit(1);
                }
                break;

            case 'f':
                force = 1;
                break;
        }
    }

    // Process each possible card address

    for(i = 0 ; i < NEWAVE3_NUM_ADDRS ; i++)
    {
        // Attempt to map card

        if(newave3Map(&card[i], i))
        {
            // Card not mapped

            continue;
        }
        printf("Mapped card %u\n", i);

        // Program card

        printf("    Programming...\n");
        if(newave3Program(&card[i], filename, force))
        {
            printf("    Failed to program card.\n");

            newave3Unmap(&card[i]);
            continue;
        }
        printf("\n");
    }

    // Wait for cards to boot

    for(i = 0 ; i < NEWAVE3_NUM_ADDRS ; i++)
    {
        // Skip card if not mapped

        if(!card[i].regs) continue;

        // Wait for card to boot

        if(newave3WaitBoot(&card[i]))
        {
            printf("Card %u failed to boot.\n", i);
        }
        else
        {
            printf("Card %u booted.\n", i);
        }

        // Unmap card

        newave3Unmap(&card[i]);
    }

    return(0);
}

// EOF
