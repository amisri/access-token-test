/*
 *  Filename: svectest.c
 *
 *  Purpose:  Configure and Acquire data from the SPS Mains SVEC card
 */

#include <arpa/inet.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "mugefhw/svec.h"

void print_usage(char **argv)
{
  fprintf(stderr,"Usage: %s [options]\n"
			"      -h              This help\n"
			"      -?              This help\n"
            "      -w              Read the SVEC card registers\n"
            "      -f              Read Filter coeficients\n"
            "      -r              Reset error counters\n"
            "      -c <mask>       Set the channel enable mask (default 0x3F, all enabled)\n"
            "      -t <enable>     Enable/disable the timing input (default 1, enabled)\n"
            "      -m <mask.       Set the timing jitter margin (default 0x5 = +- 5us). Maximum is +-255 us"
			, argv[0]
	);
  fprintf(stderr,"Read data from SPS Mains SVEC card.\n");
	

}

void print_regs(struct svec_card *card)
{
    int      i;
    int64_t  max_value            = 0;

    printf("\nSVEC Card Registers (version %u.%u)\n\n",ntohl(card->regs->filter_version),ntohl(card->regs->filter_revision));
    printf("Soft Error: 0x%02x (%u times) Hard Error: 0x%02x (%u times) Timing Error: 0x%02x (%u times)\n"
            ,(char)ntohl(card->regs->soft_err)
            ,ntohl(card->regs->soft_err_cnt)
            ,(char)ntohl(card->regs->hard_err)
            ,ntohl(card->regs->hard_err_cnt)
            ,(char)ntohl(card->regs->timing_err)
            ,ntohl(card->regs->tim_err_cnt));
    printf("Channel Enable: 0x%02x\n",(char)ntohl(card->regs->channel_enable));
    printf("Timing Margin: +/- %" PRIu8 " us\n",(char)ntohl(card->regs->timing_margin));
    printf("Timing Enable: 0x%02x\n",(char)ntohl(card->regs->timing_enable));

    // Calculate maximum value of the channels
    // The calculation for the filter are done in an accumulator (acc) of 64bits
    // At the end of the filter computation, the bits 46 to 15 (32) of acc are sent to the CPU as filter output value: 32bits signed.
    // The Filter coefficients are 32bits signed (although all coefficienst are positive)

    for(i=0; i != SVEC_RAM_NUM_ELEMENTS; ++i)
    {
        max_value += ntohl(card->filter_coef[i]);
    }
    max_value = max_value >> 15;
    printf("Max Raw Value: %"PRId64"\n",max_value);

    // Show measurements

    for(i=0; i != sizeof(card->regs->ch)/sizeof(card->regs->ch[0]); ++i)
    {
        printf("Ch%d: %d (%.2f %%)\n",i,(int32_t)ntohl(card->regs->ch[i]),1.0/max_value*(int32_t)ntohl(card->regs->ch[i])*100);
    }




}

void print_filter(struct svec_card *card)
{
    int i;

    printf("\nSVEC Card Filter Coeficients\n\n");

    for(i=0; i != SVEC_RAM_NUM_ELEMENTS; ++i)
    {
        printf("%"PRIu32",",ntohl(card->filter_coef[i]));
    }
    printf("\n");
}

int str_to_int(char *ch_str, int *ch)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing option argument\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *ch = strtol(ch_str,&endptr,0);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        fprintf(stderr,"ERROR: '%s' is not an integer\n",ch_str);
        return -1;
    }
}

int main(int argc, char **argv)
{
    int              c;
    int              read_regs            = 0;
    int              read_filter          = 0;
    int              reset_errors         = 0;
    int              ch_enable_mask       = -1;
    int              tim_enable           = -1;
    int              tim_margin           = -1;
    struct svec_card card;

    // Parse input parameters

    while ((c = getopt(argc, argv, "?hrwfc:m:t:")) != -1)
    {
    	switch (c)
    	{
    		case '?':
    		case 'h':
    			print_usage(argv);
    			exit(1);
    			break;

    		case 'r':
    		    reset_errors = 1;
    		    break;

    		case 'w':
    		    read_regs = 1;
    		    break;

    		case 'f':
    		    read_filter = 1;
    		    break;
    		case 't':
    		    if (str_to_int(optarg,&tim_enable))
    		    {
    		        return 1;
    		    }

    		    if (tim_enable)
    		    {
    		        tim_enable = 1;
    		    }
    		    break;

    		case 'c':
    		    if (str_to_int(optarg,&ch_enable_mask))
    		    {
    		        return 1;
    		    }

    		    if (ch_enable_mask < 0 || ch_enable_mask> 0x3F)
    		    {
    		        fprintf(stderr,"ERROR: Channel enable mask out of range ( 0 <= mask <= 0x3F)\n");
    		        return 1;
    		    }
    		    break;

    		case 'm':
    		    if (str_to_int(optarg,&tim_margin))
    		    {
    		        return 1;
    		    }

    		    if (tim_margin < 0 || tim_margin>255)
    		    {
    		        fprintf(stderr,"ERROR: Timing margin out of range ( 0 <= mask <= 255)\n");
    		        return 1;
    		    }
    		    break;

    		default:
                fprintf(stderr,"ERROR: Option '%c' does not exist\n\n",(char)c);
                print_usage(argv);
                return 1;
    	}
    }

    // read the registers if no options requsted

    if (!read_filter && ch_enable_mask == -1 && tim_enable == -1 && tim_margin == -1)
    {
        read_regs = 1;
    }

    if (svecMap(&card))
    {
        return 1;
    }

    if (tim_enable != -1)
    {
        card.regs->timing_enable = htonl((uint32_t)tim_enable);
    }

    if (tim_margin != -1)
    {
        card.regs->timing_margin = htonl((uint32_t)tim_margin);
    }

    if (ch_enable_mask != -1)
    {
        card.regs->channel_enable = htonl((uint32_t)ch_enable_mask);
    }

    if (reset_errors)
    {
        card.regs->soft_err_cnt = 0;
        card.regs->hard_err_cnt = 0;
        card.regs->tim_err_cnt = 0;
    }

    if (read_regs)
    {
        print_regs(&card);
    }

    if (read_filter)
    {
        print_filter(&card);
    }
    svecUnmap(&card);

    return 0;
}

// EOF
