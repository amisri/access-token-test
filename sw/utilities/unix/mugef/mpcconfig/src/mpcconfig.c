/*
 *  Filename: mpcconfig.c
 *
 *  Purpose:  Configure a channel on an MPC card
 *
 *  Author:   Stephen Page
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mugefhw/mpc.h"

int main(int argc, char **argv)
{
    uint16_t        channel;
    uint16_t        link_type;
    uint16_t        link_type_request;
    struct mpc_card mpc;
    uint16_t        mpc_data_buf[MPC_DATA_SIZE];
    uint16_t        parms[MPC_NUM_PARMS];

    // Process arguments

    if(argc != 3)
    {
        fprintf(stderr, "Usage: %s <channel> <link type>\n", argv[0]);
        exit(1);
    }
    channel = atoi(argv[1]);

    if(!strcasecmp("daisy", argv[2]))
    {
        link_type_request = MPC_LINK_DAISY;
    }
    else if(!strcasecmp("serial", argv[2]))
    {
        link_type_request = MPC_LINK_SERIAL;
    }
    else if(!strcasecmp("unused", argv[2]))
    {
        link_type_request = MPC_LINK_UNUSED;
    }
    else // Unknown link type
    {
        fprintf(stderr, "ERROR: Unknown link type %s\n", argv[2]);
        exit(1);
    }

    // Attempt to map MPC card

    if(mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return(1);
    }

    // Initialise MPC card

    if(mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        mpcUnmap(&mpc);
        return(1);
    }

    // Set link type

    mpc.regs->link[channel] = htons(link_type_request);

    memset(parms, 0, sizeof(parms));
    parms[0] = channel;
    if(mpcSendCommandWait(&mpc,
                          MPC_GLOBAL_CHANNEL,
                          MPC_CMD_SET_1_LINK,
                          1, parms,
                          sizeof(mpc_data_buf), mpc_data_buf))
    {
        fprintf(stderr, "%s", (char *)mpc_data_buf);
        mpcUnmap(&mpc);
        return(1);
    }

    // Check that MPC board has set requested link type

    memset(parms, 0, sizeof(parms));
    parms[0] = channel;
    if(mpcSendCommandWait(&mpc,
                          MPC_GLOBAL_CHANNEL,
                          MPC_CMD_GET_1_LINK,
                          1, parms,
                          sizeof(mpc_data_buf), mpc_data_buf) != 1)
    {
        fprintf(stderr, "%s", (char *)mpc_data_buf);
        mpcUnmap(&mpc);
        return(1);
    }
    link_type = ntohs(mpc.regs->data[MPC_GLOBAL_CHANNEL][1]);

    // Zero channel number of parameters

    mpc.regs->parm[channel][0] = 0;

    // Unmap MPC card

    mpcUnmap(&mpc);

    // Check whether link type matches request

    if(link_type != link_type_request)
    {
        fprintf(stderr, "Link type for channel %hu (%s) does not match requested type (%s)\n",
                         channel, mpc_link_name[link_type], mpc_link_name[link_type_request]);
        return(1);
    }
    return(0);
}

// EOF
