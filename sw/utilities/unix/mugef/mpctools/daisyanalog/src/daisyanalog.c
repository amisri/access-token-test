/*
 *  Filename: daisyanalog.c
 *
 *  Purpose:  get analog daisy data
 *
 *  Author:   Alexandre Frassier
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern char * optarg;

#include "mugefhw/mpc.h"
#include "mugef/cmd.h"

int main(int argc, char * argv[])
{
    int c;
    int loop;
    int bit_loop;
    uint16_t nbsmple = 1;
    struct mpc_card mpc;
    uint16_t mpc_data_buf[MPC_DATA_SIZE];
    uint16_t parms[MPC_NUM_PARMS];

    // Process command-line options

    while ((c = getopt(argc, argv, "?n:h")) != -1)
    {
        switch (c)
        {
            case 'n': // number of samples for each bit
                if (optarg)
                {
                    nbsmple = atoi(optarg);
                }
                else
                {
                    fprintf(stderr, "Channel not specified\n\n");
                    exit(1);
                }

                break;

            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "		-h              Help\n"
                       "		-n              Number of samples for each bit\n"
                       , argv[0]
                      );
                printf("Read analog data using daisy chain\n");
                exit(1);

            default :
                exit(1);


        }
    }

    // Attempt to map MPC card

    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    printf("MPC mapped at: 0x%p\n",     mpc.regs);
    printf("Firmware version: %hu\n",   ntohs(mpc.regs->version_firmware));
    printf("PLD version: %hu\n",        ntohs(mpc.regs->version_pld));

    // Initialise MPC card

    if (mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        mpcUnmap(&mpc);
        return (1);
    }

    // get analog data
    parms[0] = nbsmple;

    if (mpcSendCommandWait(&mpc,
                           MPC_SYSTEM_CHANNEL,
                           MPC_CMD_GET_ANA_DAISY,
                           1,
                           parms,
                           sizeof(mpc_data_buf),
                           mpc_data_buf) != 16 * parms[0])
    {
        fprintf(stderr, "ERROR %s\n", (char *)mpc_data_buf);
        mpcUnmap(&mpc);
        return (1);
    }

    for (bit_loop = 0; bit_loop < 16 ; bit_loop++)
    {
        printf("bit%2d\t", bit_loop);
    }

    printf("\n");

    for (loop = 0; loop < parms[0] ; loop++)
    {
        for (bit_loop = 0; bit_loop < 16 ; bit_loop++)
        {
            printf("%1.3f\t", (3.3 / 1023)*mpc_data_buf[16 * loop + bit_loop]);
        }

        printf("\n");

    }

    // Unmap MPC card

    mpcUnmap(&mpc);
    return (0);
}

// EOF
