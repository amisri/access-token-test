/*
 *  Filename: read_mpc.c
 *
 *  Purpose:  Print config for mpc
 *
 *  Author:   Hugo Lebreton
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern char * optarg;

#include "mugefhw/mpc.h"
#include "mugef/cmd.h"

union longword
{
    uint8_t  bytes[4];
    uint16_t word[2];
    uint32_t longword;
};

int main(int argc, char * argv[])
{
    int i;
    int c;
    uint16_t version = 0;
    uint16_t cmd = 0;
    uint16_t status = 0;
    uint16_t time = 0;
    uint16_t link = 0;
    uint16_t buffer = 0;
    uint16_t dump = 0;
    uint16_t channel = 0;
    struct mpc_card mpc;
    uint16_t mpc_data_buf[MPC_DATA_SIZE];
    uint16_t * p;

    // Process command-line options

    while ((c = getopt(argc, argv, "?hvcstlab:d")) != -1)
    {
        switch (c)
        {
            case 'v':
                version = 1;
                break;

            case 'c':
                cmd  = 1;
                break;

            case 's':
                status  = 1;
                break;

            case 't':
                time  = 1;
                break;

            case 'l':
                link  = 1;
                break;

            case 'd':
                dump = 1;
                break;

            case 'b':
                if (optarg)
                {
                    buffer = 1;
                    channel = atoi(optarg);

                    if (channel > MUGEF_MAX_CHANNELS)
                    {
                        fprintf(stderr, "Channel out of range\n");
                        exit(1);
                    }
                }
                else
                {
                    fprintf(stderr, "Channel not specified\n\n");
                    exit(1);
                }

                break;

            case 'a':
                version = cmd = status = time = link = 1;
                break;

            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "      -h              Help\n"
                       "      -v              Version\n"
                       "      -c              Command state\n"
                       "      -s              Status\n"
                       "      -t              Time\n"
                       "      -l              Link type\n"
                       "      -a              -vcstl\n"
                       "      -b chan         buffers for given channel\n"
                       "      -d              Dump all mpc regs\n"
                       , argv[0]
                      );
                printf("\t Read mpc registers\n");
                exit(1);

            default :
                exit(1);


        }
    }

    // Attempt to map MPC card

    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    // Initialise MPC card

    if (mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        //         mpcUnmap(&mpc);
        //         return(1);
    }

    if (version)
    {
        printf("Versions\n");
        printf("\tFirmware version: %hu\n",   ntohs(mpc.regs->version_firmware));
        printf("\tPLD version: %hu\n",        ntohs(mpc.regs->version_pld));
    }

    if (cmd)
    {
        printf("Command state\n");

        for (i = 0; i < MUGEF_MAX_CHANNELS / 2; i++)
        {
            printf("Channel %d\t:\t%s\t\t|\tChannel %d\t:\t%s\n", \
                   i, mpc_cmd_state_name[ntohs(mpc.regs->cmd_state[i])], i + MUGEF_MAX_CHANNELS / 2,
                   mpc_cmd_state_name[ntohs(mpc.regs->cmd_state[i + MUGEF_MAX_CHANNELS / 2])]);
        }
    }

    if (status)
    {
        printf("Status\n");

        for (i = 0; i < MUGEF_MAX_CHANNELS / 2; i++)
        {
            printf("Channel %d\t:\t%X\t\t|\tChannel %d\t:\t%X\n", \
                   i, ntohs(mpc.regs->status[i]), i + MUGEF_MAX_CHANNELS / 2,
                   ntohs(mpc.regs->status[i + MUGEF_MAX_CHANNELS / 2]));
        }
    }

    if (time)
    {
        printf("Time\n");
        printf("\tUnix time: 0x%X%X\n", ntohs(mpc.regs->time_s_h), ntohs(mpc.regs->time_s_h));
    }

    if (link)
    {
        printf("Link type for all channels\n");

        // read config for all channels
        if (mpcSendCommandWait(&mpc,
                               MPC_GLOBAL_CHANNEL,
                               MPC_CMD_GET_ALL_LINK,
                               0,
                               NULL,
                               sizeof(mpc_data_buf),
                               mpc_data_buf) != MUGEF_MAX_CHANNELS)
        {
            fprintf(stderr, "ERROR %s\n", (char *)mpc_data_buf);
            mpcUnmap(&mpc);
            return (1);
        }


        // print out config for all channels
        for (i = 0; i < MUGEF_MAX_CHANNELS / 2; i++)
        {
            printf("Channel %d\t:\t%s\t\t|\tChannel %d\t:\t%s\n", i, mpc_link_name[mpc_data_buf[i]],
                   i + MUGEF_MAX_CHANNELS / 2, mpc_link_name[mpc_data_buf[i + MUGEF_MAX_CHANNELS / 2]]);
        }
    }

    if (buffer)
    {
        for (i = 0; i < MPC_DATA_SIZE; i++)
        {
            printf("Data %d: 0x%X\r\n", i, ntohs(mpc.regs->data[channel][i]));
        }
    }


    if (dump)
    {
        p = (uint16_t *) mpc.regs;
        printf("Size dump %lu\n", sizeof(struct mpc_regs) / 2);

        for (i = 0; i < sizeof(struct mpc_regs) / 2; i++)
        {
            printf("Address 0x%X|%d\t:\t0x%X\n", i, i, ntohs(*(p + i)));
        }
    }

    // Unmap MPC card

    mpcUnmap(&mpc);
    return (0);
}

// EOF
