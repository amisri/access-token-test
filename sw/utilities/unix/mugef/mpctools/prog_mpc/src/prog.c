/*
 *  Filename: prog.c
 *
 *  Purpose:  Reprogram mpc board
 *
 *  Author:   Alexandre Frassier
 *  05/2011    Modify to work with linux board
 *  12/2011    HL add rx to fully communicate with bootloader. Several bug fixes.
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

extern char * optarg;

#include "mugefhw/mpc.h"
#include "mugef/cmd.h"
#include "prog.h"

// #define DEBUG

#define BOOTLOADER_TR_READY  0
#define BOOTLOADER_TR_BUSY   1
#define BOOTLOADER_TMO       100000
#define WAIT_W9600           10000
#define WAIT_W115K           1000

union longword
{
    uint8_t  bytes[4];
    uint16_t word[2];
    uint32_t longword;
};

union word_
{
    uint8_t  bytes[2];
    uint16_t word;
};

static struct mpc_card mpc;
struct mpc_prog_card mpc_prog;

//-----------------------------------------------------------------------------
// Local Functions
//-----------------------------------------------------------------------------
void printStatus(void);
void print_time(void);
int wait_bootloader_tr(void);
void write_bootloader(uint8_t tx_data);
int read_program(void);
int read_bootloader(uint8_t * rx_data, int timeout);
void erase_program(void);
void flash_program(FILE * pFile);
void change_baudrate(uint8_t baudrate);
int read_block(uint8_t * buf, union word_ baddr);
int write_block(const uint8_t * fbuff, union word_ baddr);
void printPage(const uint8_t * page);
void printPageDiff(const uint8_t * page1, const uint8_t * page2);

char const wait[] = { '|', '/', '-', '\\', '|', '/', '-', '\\' };
int const baud_cmd[5] =
{ BL_CMD_9600_BAUD, BL_CMD_19200_BAUD, BL_CMD_38400_BAUD, BL_CMD_57600_BAUD, BL_CMD_115200_BAUD };

//-----------------------------------------------------------------------------
int main(int argc, char * argv[])
//-----------------------------------------------------------------------------
{

    uint8_t rx_data;
    int c;
    int i;
    int err;
    FILE * pFile;
    uint8_t version[9];
    uint8_t bl_status[10];
    int status = 0;
    int erase = 0;
    int program = 0;
    int read = 0;
    int baudrate = 0;
    char * fileName = "../../file/prog.bin";

    // Process command-line options
    while ((c = getopt(argc, argv, "?hseprf:b:")) != -1)
    {
        switch (c)
        {
            case 'f': // File
                if (optarg)
                {
                    fileName = optarg;
                }
                else
                {
                    fprintf(stderr, "File not specified\n\n");
                    exit(1);
                }

                break;

            case 'b': // Baudrate
                if (optarg)
                {
                    baudrate = atoi(optarg);

                    if ((baudrate < 0) || (baudrate >= 5))
                    {
                        fprintf(stderr, "Baudrate out of range [0-4]\n\n");
                        exit(1);
                    }
                }
                else
                {
                    fprintf(stderr, "Baudrate not specified\n\n");
                    exit(1);
                }

                break;

            case 'e':
                erase = 1;
                break;

            case 'p':
                program = 1;
                break;

            case 'r':
                read = 1;
                break;

            case 's':
                status = 1;
                break;

            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "		-h              Help\n"
                       "		-e              Erase\n"
                       "		-p              Program\n"
                       "		-r              Read program\n"
                       "		-s              Print out status\n"
                       "		-f file         Specify .bin program\n"
                       "		-b baudrate     Specify the baudrate (from 0 at 9600 to 4 at 115200)\n"
                       , argv[0]
                      );
                printf("\tRead/Erase/Program a MPC card");
                exit(1);

            default :
                fprintf(stderr, "Wrong option\n\n");
                exit(1);
        }
    }

    printf("MPC remote programming tool\n");

    //------------ Attempt to map MPC card
    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }
    else
    {
        printf("MPC mapped at: 0x%p\n", mpc.regs);
        printf("Firmware version: %hu\n", ntohs(mpc.regs->version_firmware));
        printf("PLD version: %hu\n\n", ntohs(mpc.regs->version_pld));
    }

    //----------- Attempt to map mpc_prog.regs card
    if (mpcProgMap(&mpc_prog))
    {
        fprintf(stderr, "Failed to map MPC prog card.\n");
        return (1);
    }

    if (status)
    {
        printStatus();
        exit(0);
    }

    //----------- Boot loader mode
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_RESET);
    sleep(1);
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_MODE_PROG);
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_RESET);
    sleep(1);
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_MODE_PROG);
    mpc_prog.regs->cpu_baudrate = htons(BL_CMD_9600_BAUD);

    //----------- Wait at least 3ms for start up
    usleep(5000);

    //----------- Check registers
    if (mpc_prog.regs->vme_control & htons(MPC_BL_TX_BUSY))
    {
        printf("Tx busy, give up\n");
        exit(1);
    }


    if (mpc_prog.regs->vme_control & htons(MPC_BL_RX_NEW))
    {
        read_bootloader(&rx_data, 10000);
#ifdef DEBUG
        printf("Unexpected Rx data: %X\n", rx_data);
#endif
    }

    //----------- Auto baud: send 16 NUL bytes at 9600 8bits 1 start 1 stop no parity
    // with a 20ms pause between bytes
    for (i = 0; i < 20; i++)
    {
        write_bootloader(0x00);
        usleep(30000);
    }

    // Read byte if present
    if (mpc_prog.regs->vme_control & htons(MPC_BL_RX_NEW))
    {
        read_bootloader(&rx_data, 10000);
#ifdef DEBUG
        printf("Rx extra byte: 0x%X\n", rx_data);
#endif
    }
    else
    {
#ifdef DEBUG
        printf("No extra byte\n");
#endif
    }

    //----------- Check baud rate. Send baudrate, BL should reply
    write_bootloader(BL_CMD_9600_BAUD);
    err = read_bootloader(&rx_data, 10000);

    if (err)
    {
        fprintf(stdout, "Error %d reading data 0x%X\n", err, rx_data);
        printStatus();
        exit(5);
    }

    if (rx_data != BL_CMD_9600_BAUD)
    {
        fprintf(stderr, "Unexpected Rx data: 0x%X, give up\n", rx_data);
        printStatus();
        exit(5);
    }
    else
    {
        fprintf(stdout, "Initial baudrate correctly set: 0x%X\n", rx_data);
    }

    //--------- Change baudrate if not 9600
    if (baudrate != 0)
    {
        usleep(100);
        change_baudrate(baud_cmd[baudrate]);
        sleep(1);
    }

    //----------- Print out version
    write_bootloader(0xFB);

    for (i = 0; i < 8; i++)
    {
        err = read_bootloader(&(version[i]), 50000);

        if (err)
        {
            fprintf(stdout, "Error %d reading version index %d:  0x%X\n", err, i, version[i]);
            printStatus();
            exit(5);
        }
    }

    version[8] = '\0';
    fprintf(stdout, "Bootloader is running version: %s\n", version);

#ifdef DEBUG
    //----------- Print out status
    write_bootloader(BL_CMD_STATUS);

    for (i = 0; i < 2; i++)
    {
        err = read_bootloader(&(bl_status[i]), 10000);

        if (err)
        {
            fprintf(stdout, "Error %d reading data 0x%X\n", err, rx_data);
            printStatus();
            exit(5);
        }
    }

    fprintf(stdout, "Bootloader status: 0x%X%X\n", bl_status[0], bl_status[1]);
#endif

    //----------- Clear status
    write_bootloader(BL_CMD_CLR_STATUS);

    //----------- Unlock memory
    for (i = 0; i < 12; i++)
    {
        write_bootloader(mugefhw_mpc_ni_Unlock[i]);
    }

    write_bootloader(BL_CMD_STATUS);

    for (i = 0; i < 2; i++)
    {
        err = read_bootloader(&(bl_status[i]), 10000);

        if (err)
        {
            fprintf(stdout, "Error %d reading data 0x%X\n", err, rx_data);
            printStatus();
            exit(5);
        }
    }

    if ((bl_status[1] & 0x0C) == 0x0C)
    {
        fprintf(stdout, "Memory unlocked\n");
    }
    else
    {
        fprintf(stdout, "Error unlocking memory. Status: 0x%X%X\n", bl_status[0], bl_status[1]);
        exit(3);
    }

    //----------- Clear status
    write_bootloader(BL_CMD_CLR_STATUS);

    //----------- Erase all
    if (erase)
    {
        erase_program();
    }

    //----------- Clear status
    write_bootloader(BL_CMD_CLR_STATUS);


    //----------- Program pages.
    if (program)
    {
        // Programing file
        pFile = fopen(fileName, "r");

        if (pFile == NULL)
        {
            perror("fopen");
            return (1);
        }

        printf("Program %s\n", fileName);
        flash_program(pFile);
    }


    //----------- Read program and exit if asked
    if (read)
    {
        read_program();
        // TODO bug if reading just after programming
    }

    //    // Unmap MPC card
    //    mpcUnmap(&mpc);
    //    mpcProgUnmap(&mpc_prog);
    //    // Attempt to map MPC card
    //    if(mpcMap(&mpc))
    //    {
    //       fprintf(stderr, "Failed to map MPC card.\n");
    //       return(1);
    //    }
    //    // Attempt to map mpc_prog.regs card
    //    if(mpcProgMap(&mpc_prog))
    //    {
    //       fprintf(stderr, "Failed to map MPC prog card.\n");
    //       return(1);
    //    }
    mpc_prog.regs->cpu_prog =   htons(MPC_CPU_PROG_MODE_RUN);
    sleep(1);
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_RESET);
    sleep(1);
    mpc_prog.regs->cpu_prog =  htons(MPC_CPU_PROG_MODE_RUN);
    sleep(1);

    sleep(1);

    // Attempt to map MPC card
    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    printf("MPC mapped at: 0x%p\n", mpc.regs);
    printf("Firmware version: %hu\n", ntohs(mpc.regs->version_firmware));
    printf("PLD version: %hu\n\n", ntohs(mpc.regs->version_pld));

    // Unmap MPC card
    mpcUnmap(&mpc);

    mpcProgUnmap(&mpc_prog);
    return (0);
}

//-----------------------------------------------------------------------------
int wait_bootloader_tr()
//-----------------------------------------------------------------------------
{
    uint32_t tmo = 0;

    while (tmo++ < BOOTLOADER_TMO)
    {
        if (ntohs(mpc_prog.regs->cpu_bootloader_rx) == BOOTLOADER_TR_READY)
        {
            printf("\n Bootloader ready 0x%4X %d \n", ntohs(mpc_prog.regs->cpu_bootloader_rx), tmo);
            break;
        }
    };

    if (ntohs(mpc_prog.regs->cpu_bootloader_rx) != BOOTLOADER_TR_READY)
    {
        printf("\n ERROR : Bootloader busy 0x%4X %d \n", ntohs(mpc_prog.regs->cpu_bootloader_rx), tmo);
        exit(-1);
    }

    return (ntohs(mpc_prog.regs->cpu_bootloader_rx));
}

//-----------------------------------------------------------------------------
void write_bootloader(uint8_t tx_data)
//-----------------------------------------------------------------------------
{
    uint16_t timeout = 10000; // 10 ms timeout
    uint16_t tx_data_16 = tx_data;

    mpc_prog.regs->cpu_bootloader_tx = htons(tx_data_16);

    // Wait tx busy to go high
    while (!(mpc_prog.regs->cpu_bootloader_tx & htons(0x0100)) && timeout)
    {
        usleep(1);
        timeout--;
    }

    if (timeout == 0)
    {

        fprintf(stderr, "Tx timeout, never gets busy\n");
        printStatus();
        exit(1);
    }

    while ((mpc_prog.regs->cpu_bootloader_tx & htons(0x0100)) && timeout)
    {
        usleep(1);
        timeout--;
    }

    if (timeout == 0)
    {
        fprintf(stderr, "Tx timeout, still busy\n");
        printStatus();
        exit(1);
    }
}

//-----------------------------------------------------------------------------
void change_baudrate(uint8_t baudrate)
//-----------------------------------------------------------------------------
{

    uint16_t timeout = 10000; // 10 ms timeout
    uint16_t tx_data_16 = baudrate;
    uint8_t rx_data;
    int err, i;

    // Try up to 4 times

    for (i = 0; i < 4; i++)
    {
        mpc_prog.regs->cpu_bootloader_tx = htons(tx_data_16);
        // Note: baudrate will be updated after finishing sending last command, thus for next Rx

        // Wait tx busy to go high
        while (!(mpc_prog.regs->cpu_bootloader_tx & htons(0x0100)) && timeout)
        {
            usleep(1);
            timeout--;
        }

        if (timeout == 0)
        {
            fprintf(stderr, "Tx timeout, never gets busy\n");
            printStatus();
            //       exit (1);
        }

        while ((mpc_prog.regs->cpu_bootloader_tx & htons(0x0100)) && timeout)
        {
            usleep(1);
            timeout--;
        }

        if (timeout == 0)
        {
            fprintf(stderr, "Tx timeout, still busy\n");
            printStatus();
            //       exit (1);
        }

        err = read_bootloader(&rx_data, 500000);

        if (err)
        {
            fprintf(stdout, "Error %d changing baudrate, Rx 0x%X\n", err, rx_data);
            printStatus();
            //       exit(5);
        }

        if (rx_data != baudrate)
        {
            fprintf(stderr, "Unexpected baudrate from bootloader: 0x%X, give up\n", rx_data);
            printStatus();
            //       exit(5);
        }
        else
        {
            fprintf(stdout, "Change baudrate successfully, Rx 0x%X\n", rx_data);
            break;
        }

    }

    mpc_prog.regs->cpu_baudrate = htons(baudrate);
}

//-----------------------------------------------------------------------------
int read_bootloader(uint8_t * rx_data, int  us_timeout)
//-----------------------------------------------------------------------------
{
    uint16_t timeout; // ms timeout
    *rx_data = 0;

    while (!(mpc_prog.regs->vme_control & htons(MPC_BL_RX_NEW)) && us_timeout)
    {
        usleep(1);
        //       printf ("vme_control: 0x%X, vs 0x%X\n", mpc_prog.regs->vme_control, htons(MPC_BL_RX_NEW));
        //       print_time();
        us_timeout--;
    }

    if (us_timeout == 0)
    {
        //       fprintf(stderr, "Rx timeout, never got new data\n");
        //       printStatus();
        return 1;
    }

    // Work around, we need to wait a bit otherwise Rx_new stays high.
    // TO BE CHECKED
    // I don't understand why I need to wait so long time.
    usleep(25);
    // New data
    *rx_data = ntohs(mpc_prog.regs->cpu_bootloader_rx);

    timeout = 500; // 0.5ms timeout

    while ((mpc_prog.regs->vme_control & htons(MPC_BL_RX_NEW)) && timeout)
    {
        usleep(1);
        timeout--;
    }

    if (timeout == 0)
    {
        //       fprintf(stderr, "Rx timeout still busy\n");
        //       printStatus();
        return 2;
    }

    return 0;

}

//-----------------------------------------------------------------------------
void flash_program(FILE * pFile)
//-----------------------------------------------------------------------------
{
    uint16_t loop;
    static uint8_t fbuff[BLOCK_SIZE];
    union word_ baddr;
    uint8_t skip;

    uint8_t rx_data_buf[BLOCK_SIZE];
    uint8_t rx_data_buf2[BLOCK_SIZE];
    uint8_t * buf_to_overwrite;


    printf("Program device ..\n");

    baddr.word = ADDR_START_PROG;

    while (fread(fbuff, BLOCK_SIZE, 1, pFile))
    {
        skip = 1;

        for (loop = 0 ; loop < BLOCK_SIZE ; loop++)
        {
            if (fbuff[loop] != 0xFF)
            {
                skip = 0; // Don't send null blocks.
            }
        }

        if (skip == 0)
        {

            do
            {
#ifdef DEBUG
                fprintf(stdout, "Write page 0x%4X\n", baddr.word);
#endif

                while (write_block(fbuff, baddr));

                // Read back block and check its integrity

                read_block(rx_data_buf, baddr);
                read_block(rx_data_buf2, baddr);
                buf_to_overwrite = rx_data_buf;

                while (memcmp(rx_data_buf, rx_data_buf2, BLOCK_SIZE) != 0)
                {
                    fprintf(stderr, "Read again page 0x%4X\n", baddr.word);

                    read_block(buf_to_overwrite, baddr);

                    if (buf_to_overwrite == rx_data_buf)
                    {
                        buf_to_overwrite = rx_data_buf2;
                    }
                    else
                    {
                        buf_to_overwrite = rx_data_buf;
                    }
                }
            }
            while (memcmp(rx_data_buf, fbuff, BLOCK_SIZE));

        }

        baddr.word ++;

    }

    fclose(pFile);

    printf("Programing done !\n");
}

//-----------------------------------------------------------------------------
void erase_program(void)
//-----------------------------------------------------------------------------
{
    int timeout;
    int go_on;
    int i;
    uint8_t bl_status[2] = {0,0};

    printf("Erase program ...\n");

    for (i = 0; i < 2; i++)
    {
        write_bootloader(mugefhw_mpc_ni_EraseAll[i]);
    }

    timeout = 20; // 20s timeout
    go_on = 1;

    while (go_on && timeout)
    {
        sleep(1);
        write_bootloader(BL_CMD_STATUS);

        if (read_bootloader(&(bl_status[0]), 10000) == 0)
        {
            if (read_bootloader(&(bl_status[1]), 10000) == 0)
            {
                go_on = 0;
            }
        }

#ifdef DEBUG
        fprintf(stdout, "Erasing in progress. Status: 0x%2X%2X\n", bl_status[0], bl_status[1]);
#endif
        timeout--;
    }

    if (go_on) // then timeout.
    {
        fprintf(stdout, "Error erasing memory. Status: 0x%X%X\n", bl_status[0], bl_status[1]);
        exit(2);
    }
    else
    {
        fprintf(stdout, "Memory erased. Status: 0x%X%X\n", bl_status[0], bl_status[1]);
    }
}

int write_block(const uint8_t * fbuff, union word_ baddr)
{
    uint16_t loop;
    int timeout;
    int go_on;
    uint8_t bl_status[2] = {0,0};
    uint16_t wait_loop = 0;

    write_bootloader(BL_CMD_PROG);
    // TODO Depend on endianness!!
    write_bootloader(baddr.bytes[0]);   // Addr 2
    write_bootloader(baddr.bytes[1]);   // Addr 1

    for (loop = 0 ; loop < BLOCK_SIZE ; loop++)
    {
        write_bootloader(fbuff[loop]);
    }

    timeout = 20; // 20s timeout
    go_on = 1;

    while (go_on && timeout)
    {
        sleep(1);
        write_bootloader(BL_CMD_STATUS);

        if (read_bootloader(&(bl_status[0]), 10000) == 0)
        {
            if (read_bootloader(&(bl_status[1]), 10000) == 0)
            {
                go_on = 0;
            }
        }

        timeout--;
    }

    if (go_on) // then timeout.
    {
        fprintf(stdout, "Error programming block %d. Status: 0x%X%X\n", baddr.word, bl_status[0], bl_status[1]);
        return (1);
    }
    else
    {
        fprintf(stderr, " %c %d%%\r", wait[wait_loop++],
                (baddr.word - ADDR_START_PROG) * 100 / (0xFFFF - ADDR_START_PROG));

        if (wait_loop == 7) { wait_loop = 0; }
    }

    return (0);

}

int read_block(uint8_t * buf, union word_ baddr)
{
    int go_on;
    int timeout;
    int err;
    uint16_t loop;
    uint8_t  bl_status[2] = {0,0};
    uint16_t wait_loop = 0;

    write_bootloader(BL_CMD_READ);
    write_bootloader(baddr.bytes[0]);   // Addr 2
    write_bootloader(baddr.bytes[1]);   // Addr 1

    for (loop = 0 ; loop < BLOCK_SIZE ; loop++)
    {
        err = read_bootloader(&(buf[loop]), 10000);

        if (err)
        {
            fprintf(stdout, "Error %d reading block 0x%X block %d\n", err, baddr.word, loop);
            printStatus();
            return (1);
        }

    }

    timeout = 20; // 2ms timeout
    go_on = 1;
    bl_status[0] = bl_status[1] = 0;

    while (go_on && timeout)
    {
        usleep(100);
        write_bootloader(BL_CMD_STATUS);

        if (read_bootloader(&(bl_status[0]), 10000) == 0)
        {
            if (read_bootloader(&(bl_status[1]), 10000) == 0)
            {
                go_on = 0;
            }
        }

        timeout--;
    }

    if (go_on) // then timeout.
    {
        fprintf(stdout, "Error reading block %d. Status: 0x%X%X\n", baddr.word, bl_status[0], bl_status[1]);
        printStatus();
        return (2);
    }
    else
    {
        //          fprintf(stdout, " %c 0x%X block, status%X%X\n:",wait[wait_loop++], baddr.word, bl_status[0], bl_status[1]);
        fprintf(stderr, " %c %d%%\r", wait[wait_loop++],
                (baddr.word - ADDR_START_PROG) * 100 / (0xFFFF - ADDR_START_PROG));

        if (wait_loop == 7) { wait_loop = 0; }
    }

    return (0);
}

//-----------------------------------------------------------------------------
int read_program(void)
//-----------------------------------------------------------------------------
{
    union word_ baddr;
    char * fileName = "read_prog.bin";
    FILE * pFile;
    uint8_t rx_data_buf[BLOCK_SIZE];
    uint8_t rx_data_buf2[BLOCK_SIZE];
    uint8_t * buf_to_overwrite;

    pFile = fopen(fileName, "w");

    if (pFile == NULL)
    {
        perror("fopen");
        return (1);
    }

    //----------- Read pages.
    printf("Read program ..\n");

    baddr.word = ADDR_START_PROG;

    while (baddr.word != 0) // <= 0xFFFF
    {

        read_block(rx_data_buf, baddr);
        read_block(rx_data_buf2, baddr);
        buf_to_overwrite = rx_data_buf;

        while (memcmp(rx_data_buf, rx_data_buf2, BLOCK_SIZE) != 0)
        {
#ifdef DEBUG
            //             printPage(rx_data_buf);
            //             printPage(rx_data_buf2);
            //             printPageDiff(rx_data_buf, rx_data_buf2);
#endif

            fprintf(stderr, "Read again page 0x%4X\n", baddr.word);

            read_block(buf_to_overwrite, baddr);

            if (buf_to_overwrite == rx_data_buf)
            {
                buf_to_overwrite = rx_data_buf2;
            }
            else
            {
                buf_to_overwrite = rx_data_buf;
            }
        }

        fwrite(rx_data_buf, 1, sizeof(rx_data_buf), pFile);
        // TODO check for error

        baddr.word ++;

        //----------- Clear status
        write_bootloader(BL_CMD_CLR_STATUS);

    }

    fclose(pFile);

    printf("Program read\n");
    return (0);
}

//-----------------------------------------------------------------------------
void printStatus(void)
//-----------------------------------------------------------------------------
{
    fprintf(stdout, "Cpu prog: 0x%X\n", ntohs(mpc_prog.regs->cpu_prog));
    fprintf(stdout, "Vme Control: 0x%X\n", ntohs(mpc_prog.regs->vme_control));
    fprintf(stdout, "Rx: 0x%X\n", ntohs(mpc_prog.regs->cpu_bootloader_rx));
    fprintf(stdout, "Tx: 0x%X\n", ntohs(mpc_prog.regs->cpu_bootloader_tx));
    fprintf(stdout, "Baudrate: 0x%X\n", ntohs(mpc_prog.regs->cpu_baudrate));
}

//-----------------------------------------------------------------------------
void print_time(void)
//-----------------------------------------------------------------------------
{
    struct timeval time;
    int err;

    err = gettimeofday(&time, NULL) ;

    if (err)
    {
        perror("fail getting time : ");
    }
    else
    {
        printf("unix seconds %ld.%6ld\n", time.tv_sec, time.tv_usec);
    }
}


void printPage(const uint8_t * page)
{
    int loop;

    for (loop = 0 ; loop < BLOCK_SIZE ; loop++)
    {
        printf("|0x%2X", page[loop]);
    }

    printf("\n");
}

void printPageDiff(const uint8_t * page1, const uint8_t * page2)
{
    int loop;

    for (loop = 0 ; loop < BLOCK_SIZE ; loop++)
    {
        if (page1[loop] != page2[loop])
        {
            printf("%2X diferrent from %2X at index %d\n", page1[loop], page2[loop], loop);
        }
    }

    printf("\n");
}
// EOF

