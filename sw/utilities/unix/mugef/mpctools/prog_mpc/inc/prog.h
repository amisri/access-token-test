/*
 *  Filename: prog.h
 *
 *  Purpose:  include file for programing an ni_auxps card
 *
 *  Author:   Alexandre Frassier
 */

// TODO share with NiAuxPs
#define BLOCK_SIZE              256     /* Block size in bytes */
#define ADDR_START_PROG         0xFD00

const uint16_t mugefhw_mpc_ni_Null        [] = { 0x0000 };
const uint16_t mugefhw_mpc_ni_Baudrate    [] = { 0x00B4 };
const uint16_t mugefhw_mpc_ni_Status      [] = { 0x0070 };
const uint16_t mugefhw_mpc_ni_ClearStatus [] = { 0x0050 };
const uint16_t mugefhw_mpc_ni_Version     [] = { 0x00FB };
const uint16_t mugefhw_mpc_ni_Prog        [] = { 0x0041 };
const uint16_t mugefhw_mpc_ni_EraseAll    [] = { 0x00A7, 0x00D0 };
const uint16_t mugefhw_mpc_ni_Unlock [] = { 0x00F5, 0x00DF, 0x00FF, 0x00FF, 0x0007, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 };

// constants related to Bootloader
#define MPC_BL_TX_BUSY  0x0004   // Set while bootlader is sending a command
#define MPC_BL_RX_NEW   0x0002   // Set when a new data is available, self cleared by a read
#define MPC_SELECT_PROG 0x0001   // 


#define  BL_CMD_9600_BAUD     0xB0
#define  BL_CMD_19200_BAUD    0xB1
#define  BL_CMD_38400_BAUD    0xB2
#define  BL_CMD_57600_BAUD    0xB3
#define  BL_CMD_115200_BAUD   0xB4
#define  BL_CMD_STATUS        0x70
#define  BL_CMD_CLR_STATUS    0x50
#define  BL_CMD_PROG          0x41
#define  BL_CMD_READ          0xFF

// #define  DEBUG
