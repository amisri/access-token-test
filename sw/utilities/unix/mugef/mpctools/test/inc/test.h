/*
 *  Filename: template.h
 *
 *  Purpose:  template file for mpc tool
 *
 *  Author:   Hugo Lebreton
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern char * optarg;

// #include "mpc2.h"
#include "mugefhw/mpc.h"
#include "mugef/cmd.h"


union longword
{
    uint8_t  bytes[4];
    uint16_t word[2];
    uint32_t longword;
};
