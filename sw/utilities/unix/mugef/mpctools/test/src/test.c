/*
 *  Filename: test.c
 *
 *  Purpose:  various system tools for mpc
 *
 *  05/2011    HL
 */

#include "test.h"
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define VME_BUFFER_SIZE_W 32768

struct cmd_test
{
    uint16_t command;
    char     command_name[30];
    uint16_t n_arg;
    int16_t arg[MPC_NUM_PARMS];
    int16_t n_reply;
    uint16_t reply[5];
} cmd_test;


#define NB_MPC_TEST 6
struct cmd_test mpc_test[NB_MPC_TEST] =
{
    // Global channel commands
    //    {  MPC_CMD_SET_1_LINK       ,    "MPC_CMD_SET_1_LINK"      , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Set link type for single channel
    //    {  MPC_CMD_SET_ALL_LINK     ,    "MPC_CMD_SET_ALL_LINK"    , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Set link type for all channels
    //    {  MPC_CMD_SET_1_LINK_T     ,    "MPC_CMD_SET_1_LINK_T"    , 2, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Set link type for single channel
    //    {  MPC_CMD_GET_1_LINK       ,    "MPC_CMD_GET_1_LINK"      , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Get link type for single channel
    //    {  MPC_CMD_GET_ALL_LINK     ,    "MPC_CMD_GET_ALL_LINK"    , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Get link type for all channels
    //    {  MPC_CMD_GET_ALL_UARTS    ,    "MPC_CMD_GET_ALL_UARTS"   , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} },
    //    {  MPC_CMD_GET_ONE_UART     ,    "MPC_CMD_GET_ONE_UART"    , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} }, //
    //    {  MPC_CMD_GET_ALL_DAISY    ,    "MPC_CMD_GET_ALL_DAISY"   , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} },
    //    {  MPC_CMD_GET_ONE_DAISY    ,    "MPC_CMD_GET_ONE_DAISY"   , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} }, //
    //    {  MPC_CMD_CLEAR_RESP_BUF   ,    "MPC_CMD_CLEAR_RESP_BUF"  , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Clean response data buffer
    //    {  MPC_CMD_SEND_VME_BUF     ,    "MPC_CMD_SEND_VME_BUF"    , 2, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Send contents of VME buffer
    // System channel commands
    {  MPC_CMD_FPGA_VERSION     ,    "MPC_CMD_FPGA_VERSION uart" , 1, {0}, 1, {1} }, // Get FPGA version
    {  MPC_CMD_FPGA_VERSION     ,    "MPC_CMD_FPGA_VERSION daisy", 1, {1}, 1, {2} }, // Get FPGA version
    {  MPC_CMD_FPGA_VERSION     ,    "MPC_CMD_FPGA_VERSION vme"  , 1, {2}, 1, {3} }, // Get FPGA version
    {  MPC_CMD_FPGA_VERSION     ,    "MPC_CMD_FPGA_VERSION uart" , 1, {0}, 1, {1} }, // Get FPGA version
    {  MPC_CMD_FPGA_VERSION     ,    "MPC_CMD_FPGA_VERSION daisy", 1, {1}, 1, {2} }, // Get FPGA version
    {  MPC_CMD_FPGA_VERSION     ,    "MPC_CMD_FPGA_VERSION vme"  , 1, {2}, 1, {3} }, // Get FPGA version
    //    {  MPC_CMD_FPGA_RESET       ,    "MPC_CMD_FPGA_RESET"      , 2, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Reset FPGA
    //    {  MPC_CMD_GET_ANA_DAISY    ,    "MPC_CMD_GET_ANA_DAISY"   , 3, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Get analogue data over daisy chain
    //    {  MPC_CMD_HELP_LIST        ,    "MPC_CMD_HELP_LIST"       , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Retrieve help list
    //    {  MPC_CMD_GET_STATE        ,    "MPC_CMD_GET_STATE"       , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} }, // Get channel state
    // {  MPC_CMD_BAD_STATUS_COUNT ,    "MPC_CMD_BAD_STATUS_COUNT", 0, {0}, 2, {0} }  // Get count of bad status values received
};

#define NB_DAISY_TEST 11
struct cmd_test daisy_test[NB_DAISY_TEST] =
            // Results with loopback on daisy chain, to test cmd and half status
{
    //    {  MUGEF_CMD_STOP  & ~(MUGEF_CMD_GET|MUGEF_CMD_SET)           ,    "MUGEF_CMD_STOP"            , 0, {0,0,0,0,0}, 1, {0,0,0,0,0} },  // Stop the power converter
    //    {  MUGEF_CMD_RESET  & ~(MUGEF_CMD_GET|MUGEF_CMD_SET)          ,    "MUGEF_CMD_RESET"           , 0, {0}, 1, {0x0000} },       // Reset faults
    {  0x0000                                                     ,    "MUGEF_CMD_NOCMD"           , 0, {0}, 1, {0x28BD} },  // No cmd
    {  MUGEF_CMD_START & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)           ,    "MUGEF_CMD_START"           , 0, {0}, 1, {0x28B5} }, // Start the power converter
    {  MUGEF_CMD_STOP  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)           ,    "MUGEF_CMD_STOP"            , 0, {0}, 1, {0x289D} }, // Stop the power converter
    {  MUGEF_CMD_STANDBY  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)        ,    "MUGEF_CMD_STANDBY"         , 0, {0}, 1, {0x283D} },     // Power converter to standby
    {  MUGEF_CMD_POL_POS  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)        ,    "MUGEF_CMD_POL_POS"         , 0, {0}, 1, {0x20BD} }, // Polarity switch positive
    {  MUGEF_CMD_POL_NEG  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)        ,    "MUGEF_CMD_POL_NEG"         , 0, {0}, 1, {0x08BD} }, // Polarity switch negative
    {  MUGEF_CMD_MODE2  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)          ,    "MUGEF_CMD_MODE2"           , 0, {0}, 1, {0x28BC} }, // Power converter mode 2
    {  MUGEF_CMD_MODE1  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)          ,    "MUGEF_CMD_MODE1"           , 0, {0}, 1, {0x28B9} }, // Power converter mode 1
    {  MUGEF_CMD_MODE3  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)          ,    "MUGEF_CMD_MODE3"           , 0, {0}, 1, {0x28AD} },    // Power converter mode 3
    //    {  MUGEF_CMD_RESET  & ~(MUGEF_CMD_GET|MUGEF_CMD_SET)          ,    "MUGEF_CMD_RESET"           , 0, {0}, 1, {0x0000} },       // Reset faults
    {  MUGEF_CMD_TEST_BUS_5555 & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)   ,    "MUGEF_CMD_TEST_BUS_5555"   , 0, {0}, 1, {0x0831} },     // Pattern test status with 0x5555
    {  MUGEF_CMD_TEST_BUS_AAAA  & ~(MUGEF_CMD_GET | MUGEF_CMD_SET)  ,    "MUGEF_CMD_TEST_BUS_AAAA"   , 0, {0}, 1, {0x208C} }     // Pattern test status with 0xAAAA
};

#define NB_HC16_DIRECT_TEST 21
struct cmd_test hc16_direct_test[NB_HC16_DIRECT_TEST] =
{
    //    {  MUGEF_CMD_START           ,    "MUGEF_CMD_START"           , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} },  // Start the power converter
    //    {  MUGEF_CMD_STOP            ,    "MUGEF_CMD_STOP"            , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} },  // Stop the power converter
    //    {  MUGEF_CMD_POL_POS         ,    "MUGEF_CMD_POL_POS"         , 2, {0,0,0,0,0}, 0, {0,0,0,0,0} },  // Polarity switch positive
    //    {  MUGEF_CMD_POL_NEG         ,    "MUGEF_CMD_POL_NEG"         , 1, {0,0,0,0,0}, 0, {0,0,0,0,0} },  // Polarity switch negative
    //    {  MUGEF_CMD_MODE1           ,    "MUGEF_CMD_MODE1"           , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} },  // Power converter mode 1
    //    {  MUGEF_CMD_MODE2           ,    "MUGEF_CMD_MODE2"           , 0, {0,0,0,0,0}, 0, {0,0,0,0,0} },  // Power converter mode 2
    //    {  MUGEF_CMD_MODE3           ,    "MUGEF_CMD_MODE3"           , 0, {0}, 0, {0,0,0,0,0},      // Power converter mode 3
    //    {  MUGEF_CMD_RESET           ,    "MUGEF_CMD_RESET"           , 0, {0}, 1, {0x0000} },       // Reset faults
    //    {  MUGEF_CMD_STANDBY         ,    "MUGEF_CMD_STANDBY"         , 0, {0}, 1, {0x0000} },       // Power converter to standby
    {  MUGEF_CMD_DEFAULT_STATUS | MUGEF_CMD_GET  ,    "MUGEF_CMD_DEFAULT_STATUS"  , 0, {0}, 1, {0x8353} },     // Read default status
    {  MUGEF_CMD_FLT1       | MUGEF_CMD_GET  ,    "MUGEF_CMD_FLT1"         , 0, {0}, 1, {0x16F1} },      // Read digital faults
    {  MUGEF_CMD_FLT2       | MUGEF_CMD_GET  ,    "MUGEF_CMD_FLT2"         , 0, {0}, 1, {0x4326} },      // Read analogue faults
    {  MUGEF_CMD_FIRST_FLT1    | MUGEF_CMD_GET  ,    "MUGEF_CMD_FIRST_FLT1"      , 0, {0}, 1, {0x16F1} },      // Read first fault 1
    {  MUGEF_CMD_FIRST_FLT2    | MUGEF_CMD_GET  ,    "MUGEF_CMD_FIRST_FLT2"      , 0, {0}, 1, {0x0326} },      // Read first fault 2
    {  MUGEF_CMD_ANA_CHAN      | MUGEF_CMD_GET  ,    "MUGEF_CMD_ANA_CHAN"        , 0, {0}, 1, {0x0000} },      // Read analogue channel number
    {  MUGEF_CMD_INCOH         | MUGEF_CMD_GET  ,    "MUGEF_CMD_INCOH"           , 0, {0}, 1, {0x0000} },      // Read incoherent states
    {  MUGEF_CMD_TIMEOUTS      | MUGEF_CMD_GET  ,    "MUGEF_CMD_TIMEOUTS"        , 0, {0}, 1, {0x0000} },      // Read converter timeouts
    {  MUGEF_CMD_ACTIVE_FLT1   | MUGEF_CMD_GET  ,    "MUGEF_CMD_ACTIVE_FLT1"     , 0, {0}, 1, {0x1EF1} },      // Read active faults 1
    {  MUGEF_CMD_ACTIVE_FLT2   | MUGEF_CMD_GET  ,    "MUGEF_CMD_ACTIVE_FLT2"     , 0, {0}, 1, {0x433F} },      // Read active faults 2
    {  MUGEF_CMD_LHC_STATUS    | MUGEF_CMD_GET  ,    "MUGEF_CMD_LHC_STATUS"      , 0, {0}, 1, {0x0080} },      // Read status unique to converters in the LHC
    {  MUGEF_CMD_VER_CPU       | MUGEF_CMD_GET  ,    "MUGEF_CMD_VER_CPU"         , 0, {0}, 1, {0x0014} },      // Read version of HC16 program
    {  MUGEF_CMD_VER_FPGA      | MUGEF_CMD_GET  ,    "MUGEF_CMD_VER_FPGA"        , 0, {0}, 1, {0x0003} },      // Read version of FPGA programming
    {  MUGEF_CMD_PC_TYPE       | MUGEF_CMD_GET  ,    "MUGEF_CMD_PC_TYPE"         , 0, {0}, 1, {0x0001} },      // Read type of power converter
    {  MUGEF_CMD_VER_TYPE      | MUGEF_CMD_GET  ,    "MUGEF_CMD_VER_TYPE"        , 0, {0}, 1, {0x0007} },      // Read version of type configuration
    {  MUGEF_CMD_ZERO_PLC_COUNTER,    "MUGEF_CMD_ZERO_PLC_COUNTER", 0, {0}, 1, {0x0000} },       // zero plc counter
    {  MUGEF_CMD_INC_PLC_COUNTER ,    "MUGEF_CMD_INC_PLC_COUNTER" , 0, {0}, 1, {0x0000} },       // increment and read plc counter
    {  MUGEF_CMD_INC_PLC_COUNTER ,    "MUGEF_CMD_INC_PLC_COUNTER" , 0, {0}, 1, {0x0001} },       // increment and read plc counter
    {  MUGEF_CMD_TEST_BUS_5555 | MUGEF_CMD_GET  ,    "MUGEF_CMD_TEST_BUS_5555"   , 0, {0}, 1, {0x5555} },      // Pattern test status with 0x5555
    {  MUGEF_CMD_TEST_BUS_AAAA | MUGEF_CMD_GET  ,    "MUGEF_CMD_TEST_BUS_AAAA"   , 0, {0}, 1, {0xAAAA} }      // Pattern test status with 0xAAAA
};
#define NB_TEST_COM 2
struct cmd_test com_test[NB_TEST_COM] =
{
    {  MUGEF_CMD_TEST_BUS_5555 | MUGEF_CMD_GET  ,    "MUGEF_CMD_TEST_BUS_5555"   , 0, {0}, 1, {0x5555} },      // Pattern test status with 0x5555
    {  MUGEF_CMD_TEST_BUS_AAAA | MUGEF_CMD_GET  ,    "MUGEF_CMD_TEST_BUS_AAAA"   , 0, {0}, 1, {0xAAAA} }      // Pattern test status with 0xAAAA
};


uint32_t channel = 0;
struct mpc_card mpc;
uint16_t mpc_data_buf[MPC_DATA_SIZE];
uint16_t parms[MPC_NUM_PARMS];

void mpc_send_command(uint32_t chan, uint16_t command, uint16_t n_parms, int16_t response_size);
void launchTest(struct cmd_test * my_test, int16_t n_test, uint32_t chan);
void launchTestOldDaisy(struct cmd_test * my_test, int16_t n_test, uint32_t chan);
void launchTestRawDaisy(struct cmd_test * my_test, int16_t n_test, uint32_t chan);

/*----------------------------------------------------------------------------*/
int main(int argc, char * argv[])
/*----------------------------------------------------------------------------*\
  Set of different tests
\*----------------------------------------------------------------------------*/
{
    uint16_t test_mask = 0x000F;
    uint32_t unset_channel = 0;
    uint64_t status_indicator;
    uint16_t link_type = MPC_LINK_UNUSED;
    uint16_t n_loop = 1;
    uint16_t i, j, loop;
    int c;
    //     uint16_t         daisy_tmo;

    // Process command-line options

    while ((c = getopt(argc, argv, "?huc:l:n:t:")) != -1)
    {
        switch (c)
        {
            case 'c': // Channel
                if (optarg)
                {
                    channel = atoi(optarg);

                    if (channel < 1 || channel > MUGEF_MAX_CHANNELS)
                    {
                        fprintf(stderr, "Channel out of range\n");
                        exit(1);
                    }

                    channel--; // from [1-64] to [0-63]
                }
                else
                {
                    fprintf(stderr, "Channel not specified\n\n");
                    exit(1);
                }

                break;

            case 'l':  // link_type
                if (optarg)
                {
                    link_type = atoi(optarg);

                    if (link_type != MPC_LINK_SERIAL && link_type != MPC_LINK_DAISY)
                    {
                        fprintf(stderr, "wrong link_type%d\n", link_type);
                        exit(1);
                    }
                }
                else
                {
                    fprintf(stderr, "link_type not specified\n\n");
                }

                break;

            case 'n':  // number of test iteration
                if (optarg)
                {
                    n_loop = atoi(optarg);
                }
                else
                {
                    fprintf(stderr, "number of iterations not specified\n\n");
                }

                break;

            case 't':  // test number
                if (optarg)
                {
                    test_mask  = atoi(optarg);
                }
                else
                {
                    fprintf(stderr, "Test number not specified\n\n");
                }

                break;

            case 'u':
                unset_channel = 1;
                break;

            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "      -h              Help\n"
                       "      -u              Unset link type for all channels\n"
                       "      -c              Specify channel number 1-64 [default 1]\n"
                       "      -l              Specify channel link type [default serial]\n"
                       "      -n              number of iteration [default 1]\n"
                       "      -t              launch alternative test number t\n"
                       , argv[0]
                      );
                printf("This is a compilation of several tests. See source for details\n");
                exit(1);

            default :
                exit(1);


        }
    }

    // Attempt to map MPC card

    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    printf("MPC mapped at: 0x%p\n",     mpc.regs);
    printf("Firmware version: %hu\n",   ntohs(mpc.regs->version_firmware));
    //     printf("PLD version: %hu\n",        ntohs(mpc.regs->version_pld));

    // Initialise MPC card

    //     if(mpcInit(&mpc))
    //     {
    //         fprintf(stderr, "Failed to initialise MPC card.\n");
    //         mpcUnmap(&mpc);
    //         return(1);
    //     }


    if (unset_channel)
    {
        // set unused for ALL channels
        printf("Set unused for all channels\n");

        for (loop = 0; loop < MUGEF_MAX_CHANNELS; loop++)
        {
            mpc.regs->link[loop] = htons(MPC_LINK_UNUSED);
        }

        mpc_send_command(MPC_GLOBAL_CHANNEL, MPC_CMD_SET_ALL_LINK, 0, 0);
    }

    // set channel if forced with -l option
    // Otherwise read channel type

    if (link_type != MPC_LINK_UNUSED)
    {
        mpc.regs->link[channel] = htons(link_type);
        parms[0] = channel;
        mpc_send_command(MPC_GLOBAL_CHANNEL, MPC_CMD_SET_1_LINK, 1, 0);
        printf("\nPut channel %d in %s mode\n", channel + 1, mpc_link_name[link_type]);
    }
    else
    {
        parms[0] = channel;

        if (mpcSendCommandWait(&mpc, MPC_GLOBAL_CHANNEL, MPC_CMD_GET_1_LINK, 1, parms, sizeof(mpc_data_buf),
                               mpc_data_buf) != 1)
        {
            fprintf(stderr, "read error\n");
            mpcUnmap(&mpc);
            exit(1);
        }
        else
        {
            link_type = mpc_data_buf[0];
        }
    }

    if (link_type == MPC_LINK_UNUSED)
    {
        printf("Skip %s link %d\n", mpc_link_name[link_type], channel + 1);
        exit(1);
    }


    for (i = 0; i < n_loop; i++)
    {

        if (test_mask & 0x0001)
        {
            // test : communication with MPC only
            printf("\nTest com with MPC\n");
            launchTest(mpc_test, NB_MPC_TEST, MPC_SYSTEM_CHANNEL);

            // test : communication with MPC only
            printf("\nTest com with MPC read status\n");
            printf("Status indicators: 0x%" PRIX64 "\n", mpcReadStatusInd(&mpc));
            printf("Status channel %d: %X\n", channel + 1, mpc.regs->status[channel]);
        }

        if (test_mask & 0x0002) // 2
        {
            sleep(1);

            // test : com with hc16 without read_plc command
            printf("\ntest com with hc16. read back without read_plc in %s mode\n", mpc_link_name[link_type]);

            if (link_type == MPC_LINK_DAISY)
            {
                printf("Status channel %d: %X\n", channel + 1, mpc.regs->status[channel]);
                parms[0] = channel;
                mpc_send_command(MPC_GLOBAL_CHANNEL, MPC_CMD_GET_ONE_DAISY, 1, -1);  // to be removed
                printf("Daisy data channel %d: %X\n", channel + 1, mpc_data_buf[0]); //  to be removed
            }

            launchTest(hc16_direct_test, NB_HC16_DIRECT_TEST, channel);

            if (link_type == MPC_LINK_DAISY)
            {
                printf("Status channel %d: %X\n", channel + 1, mpc.regs->status[channel]);
            }
        }

        if (test_mask & 0x0004) //4
        {
            // test : Commissionning test: try to communicate
            //            printf("\nRead debug data\n");
            //            launchTest(&debug_cmd, 1, channel);
        }

        if (test_mask & 0x0008) // 8
        {
            printf("\nTest com\n");
            launchTest(com_test, NB_TEST_COM, channel);
        }

        if (test_mask & 0x0010) // 16
        {


            printf("Test com with zero/increment counter on channel %d\n", channel + 1);

            mpc_send_command(channel, MUGEF_CMD_ZERO_PLC_COUNTER, 0, 1);

            if (mpc_data_buf[0] == 0)
            {
                printf("[\e[32mOK\e[m]\t(%d) Command MUGEF_CMD_ZERO_PLC_COUNTER read %04X\n", 0, 0);
            }
            else
            {
                printf("[\e[31mNOK\e[m]\t(%d) Command MUGEF_CMD_ZERO_PLC_COUNTER read %04X expected %04X\n", 0,
                       mpc_data_buf[0], 0);
            }

            for (j = 1; j < 1024; j++)
            {
                // Experimentally, we need to wait for around 9ms between to INC command or we goes out of sync
                // This is interesting because this command is not idempotent
                // It shows we do need a spare time between commands (otherwise we can lose one command)
                // Also after a while we go out of sync anyway (seeing one spurious command)

                usleep(20000);

                mpc_send_command(channel, MUGEF_CMD_INC_PLC_COUNTER, 0, 1);

                if (mpc_data_buf[0] == j)
                {
                    printf("[\e[32mOK\e[m]\t(%d) Command MUGEF_CMD_INC_PLC_COUNTER read %04X\n", j, j);
                }
                else
                {
                    printf("[\e[31mNOK\e[m]\t(%d) Command MUGEF_CMD_INC_PLC_COUNTER read %04X expected %04X\n", j,
                           mpc_data_buf[0], j);
                }
            }
        }

        if (test_mask & 0x0020) // 32
        {

            printf("\nStress test: VME access on mpc\n");

            uint16_t * random_buf = malloc(VME_BUFFER_SIZE_W * sizeof(uint16_t));

            if (random_buf == NULL)
            {
                printf("malloc failed\n");
                exit(2);
            }

            for (j = 0; j < VME_BUFFER_SIZE_W; j++)
            {
                random_buf[j] = rand();
                mpc.regs->vme_buffer[j] = random_buf[j];
            }

            if (memcmp(random_buf, (const void *) mpc.regs->vme_buffer, VME_BUFFER_SIZE_W * sizeof(uint16_t)))
            {
                printf("[\e[31mNOK\e[m]\t(%d) Error write/read back data buffer at index %d got %04X expected %04X\n", i, 0,
                       0, 0);

                for (j = 0; j < VME_BUFFER_SIZE_W; j++)
                {
                    if (mpc.regs->vme_buffer[j] != random_buf[j])
                    {
                        printf("[\e[31mNOK\e[m]\t(%d) at index %d got %04X expected %04X\n", i, j, mpc.regs->vme_buffer[j],
                               random_buf[j]);
                    }

                }

                for (j = 0; j < VME_BUFFER_SIZE_W; j++)
                {
                    if (mpc.regs->vme_buffer[j] != random_buf[j])
                    {
                        printf("[\e[31mNOK\e[m]\t(%d) at index %d got %04X expected %04X\n", i, j, mpc.regs->vme_buffer[j],
                               random_buf[j]);
                    }

                }
            }
            else
            {
                printf("[\e[32mOK\e[m]\t(%d) Write and read back data buffer successfully %04X\n", i, j);
            }

            free(random_buf);

        }

        if (test_mask & 0x0040) // 64
        {
            printf("\nPoll status\n");

            while (1)
            {
                status_indicator = mpcReadStatusInd(&mpc);

                if (status_indicator)
                {
                    printf("Status indicators: %lX\n", mpcReadStatusInd(&mpc));

                    for (j = 0; j < MUGEF_MAX_CHANNELS; j++)
                    {
                        if (status_indicator & ((uint64_t)0x1 << j))
                        {
                            printf("New status for channel %d: %X\n", j + 1, mpc.regs->status[j]);
                        }

                    }
                }
            }
        }

        if (test_mask & 0x0080) // 128
        {
            printf("\nRead default status\n");

            mpc_send_command(channel, MUGEF_CMD_DEFAULT_STATUS, 0, 1);

            if (mpc_data_buf[0] == 0x7FAA)
            {
                printf("[\e[32mOK\e[m]\t(%d) Command MUGEF_CMD_INC_PLC_COUNTER read %04X\n", 0x7FAA, 0x7FAA);
            }
            else
            {
                printf("[\e[31mNOK\e[m]\t(%d) Command MUGEF_CMD_INC_PLC_COUNTER read %04X expected %04X\n", 0x7FAA,
                       mpc_data_buf[0], 0x7FAA);
            }
        }

        if (test_mask & 0x0100) //256
        {
            printf("\nSend High Priority commands\n");
            printf("\nSend Start\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_START | 0x2000, 0, NULL, 0);
            sleep(1);
            printf("\nSend Stop\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_STOP | 0x2000, 0, NULL, 0);
            sleep(1);
            printf("\nSend Pol Pos\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_POL_POS | 0x2000, 0, NULL, 0);
            sleep(1);
            printf("\nSend Pol Neg\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_POL_NEG | 0x2000, 0, NULL, 0);
            sleep(1);
            printf("\nSend Mode1\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_MODE1 | 0x2000, 0, NULL, 0);
            sleep(1);
            printf("\nSend Mode2\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_MODE2 | 0x2000, 0, NULL, 0);
            sleep(1);
            printf("\nSend Mode3\n");
            mpcSendCommand(&mpc, channel, MUGEF_CMD_MODE3 | 0x2000, 0, NULL, 0);
            sleep(1);

        }

        if (test_mask & 0x0200) //512
        {
            // test : create a warning on mpc board
            // ToDo mpc_send_command exit, and we want to continue in this case
            printf("\nCreate a warning on mpc board\n");
            mpc_send_command(MPC_GLOBAL_CHANNEL, MUGEF_CMD_VER_CPU, 1, 0);
            mpc_send_command(channel, 0x8222, 0, 0);
            mpc_send_command(channel, 0x4222, 1, 0);
        }


        if (test_mask & 0x0400) // 1024
        {
            if (link_type == MPC_LINK_DAISY)
            {

                printf("\ntest com with hc16. read back using old daisy style (reading status)\n");
                launchTestOldDaisy(hc16_direct_test, NB_HC16_DIRECT_TEST, channel);
            }
        }



        if (test_mask & 0x0800) //2048
        {
            printf("\nSend daisy commands\n");

            if (link_type == MPC_LINK_DAISY)
            {

                printf("\nTest cmd lines and (half) status line (plug loopback) \n");
                launchTest(daisy_test, NB_DAISY_TEST, channel);
            }
        }

        // test : Stress com: send 5 command and read data back after
        //
    }

    // Unmap MPC card
    mpcUnmap(&mpc);
    return (0);
}

/*----------------------------------------------------------------------------*/
void mpc_send_command(uint32_t chan, uint16_t command, uint16_t n_parms, int16_t response_size)
/*----------------------------------------------------------------------------*\
  Send the given command. Exit if it fails
\*----------------------------------------------------------------------------*/
{
    int16_t cmd_rsp;

    cmd_rsp = mpcSendCommandWait(&mpc, chan, command, n_parms, parms, sizeof(mpc_data_buf), mpc_data_buf);

    if (cmd_rsp == -1)
    {
        fprintf(stderr, "ERROR command 0x%X get %d, expected %d: %s. Channel %d state: %d\n",
                command, cmd_rsp, response_size, (char *) mpc_data_buf, chan + 1, ntohs(mpc.regs->cmd_state[chan]));
        mpcUnmap(&mpc);
        exit(1);
    }

    if (response_size != -1 && cmd_rsp != response_size)
    {
        fprintf(stderr, "ERROR command 0x%X get %d, expected %d: %X:%X:%X:%X:%X. Channel %d state: %d\n",
                command, cmd_rsp, response_size, mpc_data_buf[0], mpc_data_buf[1], mpc_data_buf[2], mpc_data_buf[3],
                mpc_data_buf[4], chan + 1, ntohs(mpc.regs->cmd_state[chan]));
        mpcUnmap(&mpc);
        exit(1);
    }
}

/*----------------------------------------------------------------------------*/
void launchTest(struct  cmd_test * my_test, int16_t n_test, uint32_t chan)
/*----------------------------------------------------------------------------*\
  Launch the given test
\*----------------------------------------------------------------------------*/
{

    int16_t i;
    int16_t j;

    for (i = 0; i < n_test ; i++)
    {

        for (j = 0; j < my_test[i].n_arg; j++)
        {
            parms[j] = my_test[i].arg[j];
        }

        mpc_send_command(chan, my_test[i].command, my_test[i].n_arg, my_test[i].n_reply);

        if (my_test[i].n_reply != -1)
        {
            for (j = 0; j < my_test[i].n_reply; j++)
            {
                if (mpc_data_buf[j] == my_test[i].reply[j])
                {
                    printf("[\e[32mOK\e[m]\t(%d) Command %s read %04X\n", i,  my_test[i].command_name, my_test[i].reply[j]);
                }
                else
                {
                    printf("[\e[31mNOK\e[m]\t(%d) Command %s read %04X expected %04X\n", i, my_test[i].command_name,
                           mpc_data_buf[j], my_test[i].reply[j]);
                }
            }
        }

        //       printf("Status channel %d: %X\n", channel, mpc.regs->status[channel]); //  to be removed
        //       parms[0] = chan;
        //       mpc_send_command (MPC_GLOBAL_CHANNEL, MPC_CMD_GET_ONE_DAISY, 1, -1); // to be removed
        //       printf("Daisy data channel %d: %X\n", channel, mpc_data_buf[0]); //  to be removed
    }
}

/*----------------------------------------------------------------------------*/
void launchTestOldDaisy(struct  cmd_test * my_test, int16_t n_test, uint32_t chan)
/*----------------------------------------------------------------------------*\
 This is for old version of mpc when status is updated for any command
 with newer version just use normal test
 Is this version ok??
\*----------------------------------------------------------------------------*/
{
    int16_t i;

    for (i = 0; i < n_test ; i++)
    {

        mpc_send_command(chan, my_test[i].command, 0, 1);

        if (my_test[i].n_reply != -1)
        {
            if (my_test[i].reply[0] == mpc.regs->status[channel])
            {
                printf("[\e[32mOK\e[m]\t(%d) Command %s read %04X\n", i,  my_test[i].command_name, my_test[i].reply[0]);
            }
            else
            {
                printf("[\e[31mNOK\e[m]\t(%d) Command %s read %04X expected %04X\n", i, my_test[i].command_name,
                       mpc.regs->status[channel], my_test[i].reply[0]);
            }
        }
    }
}

/*----------------------------------------------------------------------------*/
void launchTestRawDaisy(struct cmd_test * my_test, int16_t n_test, uint32_t chan)
/*----------------------------------------------------------------------------*\
 This is for old version of mpc when status is updated for any command
 with newer version just use normal test
\*----------------------------------------------------------------------------*/
{

    int16_t  i, j;

    for (i = 0; i < n_test ; i++)
    {

        if (mpcSendCommand(&mpc, chan, my_test[i].command, 0, parms, 0))
        {
            printf("[\e[31mNOK\e[m]\t(%d) Command %s failed\n", i, my_test[i].command_name);
        }

        for (j = 0; j < 1000; j++)
        {
            usleep(5000);

            if (my_test[i].n_reply != -1)
            {
                if (my_test[i].reply[0] == mpc.regs->status[channel])
                {
                    printf("[\e[32mOK\e[m]\t(%d) Command %s read %04X\n", i,  my_test[i].command_name, my_test[i].reply[0]);
                }
                else
                {
                    printf("[\e[31mNOK\e[m]\t(%d) Command %s read %04X expected %04X\n", i, my_test[i].command_name,
                           mpc.regs->status[channel], my_test[i].reply[0]);
                }
            }
        }

        usleep(6000);
    }
}

// EOF
