#!/bin/bash
#
# Name:     tst_com.sh
# Purpose:  Communication Test 
# Author:  

cpu=`uname -m`
os=`uname -s`

hostname=`hostname | sed 's/\..*//'`
config_file="/user/pclhc/etc/mugef/mpc/$hostname"
tst_com="`dirname $0`/$os/$cpu/test"

# Check whether configuration file exists

if [ ! -f "$config_file" ]; then
    echo "Configuration file $config_file does not exist"
    exit 1
fi

# Test each channel

while read channel link
do
    if [ "$link" == "serial" ]; then       
        echo "Skip deprecated serial channel $channel:$link"
    elif [ "$link" == "daisy" ]; then
        echo "$channel:$link"
        $tst_com -t 8 -l2 -c $((channel+1))
#    elif [ "$link" == "unused" ]; then
#        echo "Skip unused channel $channel:$link"
    fi
done <"$config_file"

# EOF
