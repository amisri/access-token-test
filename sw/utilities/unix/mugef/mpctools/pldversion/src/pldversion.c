/*
 *  Filename: pldversion.c
 *
 *  Purpose:  various system tools for mpc
 *
 *  Author:   Alexandre Frassier
 *  05/2011 Clean up argument processing HL
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern char * optarg;

#include "mugefhw/mpc.h"
#include "mugef/cmd.h"

union longword
{
    uint8_t  bytes[4];
    uint16_t word[2];
    uint32_t longword;
};

int main(int argc, char * argv[])
{
    int c;
    int loop;
    int version_fpga[3];
    struct mpc_card mpc;
    uint16_t mpc_data_buf[MPC_DATA_SIZE];
    uint16_t parms[MPC_NUM_PARMS];

    // Process command-line options

    while ((c = getopt(argc, argv, "?h")) != -1)
    {
        switch (c)
        {
            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "      -h              Help\n"
                       "\tRead MPC hw and sw versions\n"
                       , argv[0]
                      );
                exit(1);

            default :
                exit(1);
        }
    }

    // Attempt to map MPC card

    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    printf("MPC mapped at: 0x%p\n",     mpc.regs);
    printf("Firmware version: %hu\n",   ntohs(mpc.regs->version_firmware));
    printf("PLD version: %hu\n",        ntohs(mpc.regs->version_pld));

    // Initialise MPC card

    if (mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        mpcUnmap(&mpc);
        return (1);
    }

    // read FPGA version
    for (loop = 0; loop < 3 ; loop++)
    {
        parms[0] = loop;

        if (mpcSendCommandWait(&mpc,
                               MPC_SYSTEM_CHANNEL,
                               MPC_CMD_FPGA_VERSION,
                               1,
                               parms,
                               sizeof(mpc_data_buf),
                               mpc_data_buf) != 1)
        {
            fprintf(stderr, "ERROR %s\n", (char *)mpc_data_buf);
            mpcUnmap(&mpc);
            return (1);
        }

        version_fpga [loop] = mpc_data_buf[0];
    }

    printf("UARTS_VER=%d DAISY_VER=%d VME_VER=%d\n", version_fpga[0], version_fpga[1], version_fpga[2]);

    // Unmap MPC card
    mpcUnmap(&mpc);

    return (0);
}

// EOF
