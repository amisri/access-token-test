/*
 *  Filename: tester.c
 *
 *  Purpose:  test all channels in daisy or serial mode for mpc
 *
 */

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include "mugefhw/mpc.h"
#include "mugef/cmd.h"

extern char * optarg;
struct mpc_card mpc;


static void mpcSet1ChannelMode(int16_t channel, int link_mode)
{
    uint16_t mpc_data_buf[MPC_DATA_SIZE];
    uint16_t parms[MPC_NUM_PARMS];

    mpc.regs->link[channel] = htons(link_mode);
    parms[0] = channel;

    if (mpcSendCommandWait(&mpc, MPC_GLOBAL_CHANNEL, MPC_CMD_SET_1_LINK, 1, parms, sizeof(mpc_data_buf),
                           mpc_data_buf) == -1)
    {
        fprintf(stderr, "ERROR: %s.  MPC_CMD_SET_1_LINK %X on channel %d state: %d\n", (char *) mpc_data_buf,
                MPC_CMD_SET_1_LINK, MPC_GLOBAL_CHANNEL + 1, ntohs(mpc.regs->cmd_state[MPC_GLOBAL_CHANNEL]));
    }
}

static int next_channel(int16_t * chan, uint16_t invert, bool unset_f)
{
    bool is_last_chan;

    if (unset_f)
    {
        mpcSet1ChannelMode(*chan, MPC_LINK_UNUSED);
    }

    if (invert)
    {
        (*chan)--;
        is_last_chan = *chan < 0;
    }
    else
    {
        (*chan)++;
        is_last_chan = (*chan >= MUGEF_MAX_CHANNELS);
    }

    if (!is_last_chan)
    {
        mpcSet1ChannelMode(*chan, MPC_LINK_DAISY);
    }

    return (!is_last_chan);
}

static int mpc_channel_tst(uint16_t channel, uint16_t command, uint16_t status_expected)
{

    uint64_t status_changed;
    uint16_t channel_status;
    uint16_t loop;
    uint16_t err = 1;


    // Reset status indicator

    status_changed = mpcReadStatusInd(&mpc);

    if (status_changed)
    {
        for (loop = 0; loop < MUGEF_MAX_CHANNELS; loop++)
        {
            if (status_changed & ((uint64_t)0x1 << loop))
            {
                volatile int16_t dummy_read;

                // Prevent warning about dummy_read being unused as it is used to read a value from the VME bus

                (void)dummy_read;

                dummy_read = mpc.regs->status[loop];
            }
        }
    }

    mpcSendCommand(&mpc, channel, command, 0, NULL, 1);

    while (ntohs(mpc.regs->cmd_state[channel]) == MPC_CMD_STATE_PROC);

    status_changed = mpcReadStatusInd(&mpc);

    if (status_changed & (1LL << channel))
    {
        if (mpc.regs->status[channel] != htons(~status_expected))
        {
            printf("[\e[32mOK\e[m]\tTest %X Completed Successfully\n", command);
            err = 0;
        }
        else
        {
            printf("[\e[31mNOK\e[m]\tTest %X failed: wrong status %04X\n", command, ntohs(mpc.regs->status[channel]));
            err = 1;
        }
    }
    else
    {
        printf("[\e[31mNOK\e[m]\tTest %X failed: no status received\n", command);
        err = 1;
    }

    // Check there is no cross-talk with another channel

    for (channel_status = 1 ; channel_status <= MUGEF_MAX_CHANNELS ; channel_status++)
    {
        if (status_changed & (1LL << (channel_status - 1)))   // The status has changed
        {
            if (channel_status != channel + 1)
            {
                printf("[\e[31mNOK\e[m]\tReceive artifact from channel %d, status %04X\n", channel_status,
                       ntohs(mpc.regs->status[channel_status - 1]));
                err = 2;
            }
        }
    }

    return err;
}

int main(int argc, char * argv[])
{
    int c = 0;
    int16_t channel;
    uint32_t nb_test_iter = 3;
    uint32_t first_channel = 0;
    uint16_t mpc_data_buf[MPC_DATA_SIZE];
    uint16_t parms[MPC_NUM_PARMS];
    char key;
    uint16_t invert = 0;
    uint16_t cont = 1;
    bool unset_f = false;
    int test_successful_f;
    int i;

    // Process command-line options

    while ((c = getopt(argc, argv, "?hc:n:ru")) != -1)
    {
        switch (c)
        {
            case 'c': // Channel
                if (optarg)
                {
                    first_channel = atoi(optarg);

                    if (first_channel < 1 || first_channel > MUGEF_MAX_CHANNELS)
                    {
                        fprintf(stderr, "Channel out of range\n");
                        exit(1);
                    }

                    first_channel--; // from [1-64] to [0-63]
                }
                else
                {
                    fprintf(stderr, "Channel not specified\n\n");
                    exit(1);
                }

                break;

            case 'n':  // number of iteration
                if (optarg)
                {
                    nb_test_iter = atoi(optarg);
                }
                else
                {
                    fprintf(stderr, "Number of iteration not specified\n\n");
                }

                break;

            case 'r':
                invert = 1;
                break;

            case 'u':
                unset_f = true;
                break;

            case '?':
            case 'h':
                printf(
                    "This program is use to test all the lines of a MPC card one by one, at commissioning.\n");
                printf("Usage: %s [-h]\n"
                       "      -h              Help\n"
                       "      -c              Channel number\n"
                       "      -r              Revert mode (count channel downward)\n"
                       "      -n              Number of iteration of the test\n"
                       "      -u              Set channel as unused except the one under test\n", argv[0]);
                exit(1);

            default:
                exit(1);

        }
    }

    // Attempt to map MPC card

    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    printf("MPC mapped at: 0x%p\n", mpc.regs);
    printf("Firmware version: %hu\n", ntohs(mpc.regs->version_firmware));
    printf("PLD version: %hu\n", ntohs(mpc.regs->version_pld));

    // Initialise MPC card

    if (mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        mpcUnmap(&mpc);
        return (1);
    }

    printf("%s link for all channels\n", unset_f ? "Unset" : "Set daisy");

    for (channel = 0; channel < MUGEF_MAX_CHANNELS; channel++)
    {
        if (unset_f)
        {
            mpc.regs->link[channel] = htons(MPC_LINK_UNUSED);
        }
        else
        {
            mpc.regs->link[channel] = htons(MPC_LINK_DAISY);
        }
    }

    if (mpcSendCommandWait(&mpc, MPC_GLOBAL_CHANNEL, MPC_CMD_SET_ALL_LINK, 0, parms, sizeof(mpc_data_buf),
                           mpc_data_buf) == -1)
    {
        fprintf(stderr, "ERROR: %s.  MPC_CMD_SET_ALL_LINK %X on channel %d state: %d\n", (char *) mpc_data_buf,
                MPC_CMD_SET_ALL_LINK, MPC_GLOBAL_CHANNEL + 1, ntohs(mpc.regs->cmd_state[MPC_GLOBAL_CHANNEL]));
    }

    channel = first_channel;

    mpcSet1ChannelMode(channel, MPC_LINK_DAISY);

    while (cont)
    {
        fprintf(stdout, "\n[%s] Plug cable in channel %d. Press enter when ready, e to exit, s to skip channel\n",
                mpc_link_name[ntohs(mpc.regs->link[channel])], channel + 1);
        key = getchar();


        if (key == 'e')
        {
            mpcUnmap(&mpc);
            return (0);
        }

        if (key == 's')
        {
            cont = next_channel(&channel, invert, unset_f);
        }
        else
        {
            test_successful_f = 1;

            for (i = 0; i < nb_test_iter; i++)
            {
                if (mpc_channel_tst(channel, MUGEF_CMD_TEST_BUS_5555, 0x5555))
                {
                    test_successful_f = false;
                }

                if (mpc_channel_tst(channel, MUGEF_CMD_TEST_BUS_AAAA, 0xAAAA))
                {
                    test_successful_f = false;
                }
            }

            if (test_successful_f)
            {
                cont = next_channel(&channel, invert, unset_f);
                printf("[\e[32mOK\e[m]\tTest Completed Successfully\a\n");
            }
            else
            {
                printf("[\e[31mNOK\e[m]\tTest failed\a\n");
            }
        }
    }

    // Unmap MPC card

    mpcUnmap(&mpc);
    return (0);
}

// EOF
