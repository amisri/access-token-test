/*
 *  Filename: hwreset.c
 *
 *  Purpose:  Reset MPC card
 *
 *  Author:   Alexandre Frassier
 *  04/2011   Clean up. HL.
 *  05/2011   Add linux support. HL.
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern char * optarg;

#include "mugefhw/mpc.h"

int main(int argc, char * argv[])
{
    int c;
    struct mpc_prog_card mpc_prog;

    // Process command-line options

    while ((c = getopt(argc, argv, "?h")) != -1)
    {
        switch (c)
        {
            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "            -h              Help\n"
                       "Reset the MPC card\n"
                       , argv[0]
                      );
                exit(1);
                break;
        }
    }

    // Attempt to map mpc_prog.regs card

    if (mpcProgMap(&mpc_prog))
    {
        fprintf(stderr, "Failed to map MPC prog card.\n");
        return (1);
    }

    printf("mpc_prog.regs->jtag_prog: %X\n", ntohs(mpc_prog.regs->jtag_prog));
    printf("mpc_prog.regs->cpu_prog: %X\n", ntohs(mpc_prog.regs->cpu_prog));
    printf("mpc_prog.regs->vme_control: %X\n", ntohs(mpc_prog.regs->vme_control));
    printf("mpc_prog.regs->cpu_bootloader_rx: %X\n", ntohs(mpc_prog.regs->cpu_bootloader_rx));
    printf("mpc_prog.regs->cpu_bootloader_tx: %X\n", ntohs(mpc_prog.regs->cpu_bootloader_tx));
    printf("mpc_prog.regs->cpu_baudrate: %X\n", ntohs(mpc_prog.regs->cpu_baudrate));

    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_MODE_RUN);
    sleep(1);
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_RESET);
    sleep(1);
    mpc_prog.regs->cpu_prog = htons(MPC_CPU_PROG_MODE_RUN);

    mpcProgUnmap(&mpc_prog);

    return (0);
}

// EOF
