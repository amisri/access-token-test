/*
 *  Filename: currentstate.c
 *
 *  Purpose:  read all current status
 *
 *  Author:   Alexandre Frassier
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

extern char * optarg;

#include "mugefhw/mpc.h"
#include "mugef/cmd.h"

union longword
{
    uint8_t  bytes[4];
    uint16_t word[2];
    uint32_t longword;
};

int main(int argc, char * argv[])
{
    int c;
    struct mpc_card mpc;
    uint16_t loop, loop_line;

    // Process command-line options

    while ((c = getopt(argc, argv, "?h")) != -1)
    {
        switch (c)
        {

            case '?':
            case 'h':
                printf("Usage: %s [-h]\n"
                       "		-h              Help\n"
                       , argv[0]
                      );
                printf("\tPrint status for each channel\n");
                exit(1);

            default :
                exit(1);
        }
    }

    // Attempt to map MPC card

    if (mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return (1);
    }

    printf("MPC mapped at: 0x%p\n",     mpc.regs);
    printf("Firmware version: %hu\n",   ntohs(mpc.regs->version_firmware));
    printf("PLD version: %hu\n",        ntohs(mpc.regs->version_pld));

    // Initialise MPC card

    if (mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        mpcUnmap(&mpc);
        return (1);
    }

    // read current status an print it
    loop_line = 0;

    for (loop = 0; loop < MUGEF_MAX_CHANNELS ; loop++)
    {
        printf(" %2d: %04X |", loop, mpc.regs->status[loop]);

        if (++loop_line == 8)
        {
            printf("\n\r");
            loop_line = 0;
        }
    }

    // Unmap MPC card

    mpcUnmap(&mpc);
    return (0);
}

// EOF
