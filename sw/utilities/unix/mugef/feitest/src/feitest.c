/*
 *  Filename: feitest.c
 *
 *  Purpose:  Test Fast Extraction Interlock card access
 *
 *  Author:   Stephen Page
 */

#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include "mugefhw/fei.h"
#include "mugefhw/vme.h"

#define PULSE_LENGTH_US 100000
#define MAX_PULSE_LENGTH_US 0xFFFFFF

void print_usage(char **argv)
{
  fprintf(stderr,"Usage: %s [options]\n"
            "      -h              This help\n"
            "      -?              This help\n"
            "      -t sec          Enable beam for sec microseconds (i.e. interlock output giving current)\n"
            "      -b num          Control Beam Dump outnput num\n"
            "      -f num          Control the FEI output num\n"
            , argv[0]
    );
  fprintf(stderr,"Enable the FEI card interlock signal (default is disabled)\n");
}

// Retrieve the number

int read_int(char *ch_str, int *num)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Number not specified\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *num = strtol(ch_str,&endptr,0);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        return 1;
    }

}

int main(int argc, char **argv)
{
    int             c;
    int             test_fei  = 0;
    int             test_bd   = 0;
    int             test_pulse= 0;
    int             num       = -1;
    int             t_us      = PULSE_LENGTH_US;
    struct fei_card fei;
    struct timespec wait;

    // Parse input parameters

    while ((c = getopt(argc, argv, "?ht:b:f:")) != -1)
    {
        switch (c)
        {
            case 't':
	        test_pulse = 1;
                if (read_int(optarg,&t_us))
                {
                    exit(1);
                }

                if (t_us<=0)
                {
		    fprintf(stderr,"ERROR: Pulse time should be bigger than 0\n");
		    exit(1);
                }

		if (t_us > MAX_PULSE_LENGTH_US)
	        {
		  fprintf(stderr,"ERROR: Pulse length out of range (1..%d us)\n",MAX_PULSE_LENGTH_US);
		} 
                break;
            case 'b':
                test_bd = 1;
                if (read_int(optarg,&num))
                {
                    exit(1);
                }

                if (num < 0 || num > FEI_NUM_BD_OUTPUTS)
                {
                    fprintf(stderr,"ERROR: Beam bump output number out of range (0..%d)\n",FEI_NUM_BD_OUTPUTS-1);
                    exit(1);
                }
                break;
            case 'f':
                test_fei = 1;
                if (read_int(optarg,&num))
                {
                    exit(1);
                }

                if (num < 0 || num > FEI_NUM_FEI_OUTPUTS)
                {
                    fprintf(stderr,"ERROR: FEI Beam bump output number out of range (0..%d)\n",FEI_NUM_FEI_OUTPUTS-1);
                    exit(1);
                }
                break;
            default:
                print_usage(argv);
                exit(1);
                break;
        }
    }

    // Attempt to map FEI card

    if(feiMap(&fei))
    {
        fprintf(stderr, "Failed to map FEI card.\n");
        exit(1);
    }
    printf("Mapped FEI card with version %hd\n", ntohs(fei.regs->version));

    // micro sleep timer

    wait.tv_sec         =  1000 / 1000000;
    wait.tv_nsec        = (1000 % 1000000) * 1000;

    // Test FEI outputs

    if (test_fei)
    {
         do {
             fei.regs->fei[num]    =  htons( t_us / FEI_TIMER_UNIT_US);
         } while (!test_pulse && !nanosleep(&wait,&wait));
    }

    // Test BEAM DUMP outputs

    if (test_bd)
    {
         do {
             fei.regs->beam_dump[num]     =  htons(t_us / FEI_TIMER_UNIT_US);
         } while (!test_pulse && !nanosleep(&wait,&wait));
    }

    feiUnmap(&fei);
    return(0);
}

// EOF
