/*
 *  Filename: newave3.h
 *
 *  Purpose:  Allow writing values in the newave3 cards, as well as reading the current configuration
 *
 *  Author:   Stephen Page
 *  Author:   Miguel Hermo
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "mugefhw/newave3.h"

int str_to_int(char *ch_str, int *ch)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing option argument\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *ch = strtol(ch_str,&endptr,0);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        fprintf(stderr,"ERROR: '%s' is not an integer\n",ch_str);
        return -1;
    }
}

void print_usage(char **argv)
{
  fprintf(stderr,"Usage: %s [options]\n"
            "      -h              This help\n"
            "      -?              This help\n"
            "      -a<addr>        Address of the newave3 card\n"
            "      -c<channel>     Channel of the newave3 where the <value> will be written to\n"
            "      -v<value>       Value to ouput on the DAC specified by <addr> and <channel>\n"
            
            , argv[0]
    );
  fprintf(stderr, "Write data to newave3 DAC card.\n");
  fprintf(stderr, "If invoked without any parameters, it ouputs the current configuration of all connected newave3 boards.\n");
 
}

#define SET_SLOT    0x001
#define SET_CHANNEL 0x010
#define SET_VALUE   0x100

int main(int argc, char **argv)
{
    struct newave3_card card;
    unsigned int set_mask = 0x000;

    int set_addr      = 0;
    int set_channel   = 0;
    int32_t set_value = 0;

    int addr;
    int channel;

    char c;

    // Read command line arguments

    while ((c = getopt(argc, argv, "?ha:c:v:")) != -1)
    {
        switch (c)
        {
            case '?':
            case 'h':
                print_usage(argv);
                exit(1);
                break;

            case 'a':
                if (str_to_int(optarg,&set_addr))
                {
                    return 1;
                }
                set_mask |= SET_SLOT;
                break;

            case 'c':
                if (str_to_int(optarg,&set_channel))
                {
                    return 1;
                }
                set_mask |= SET_CHANNEL;
                break;

            case 'v':
                if (str_to_int(optarg,&set_value))
                {
                    return 1;
                }
                set_mask |= SET_VALUE;
                break;
            
            default:
                fprintf(stderr,"ERROR: Option '%c' does not exist\n\n",(char)c);
                print_usage(argv);
                return 1;
        }
    }

    if(set_mask != 0x0)
    {
        if((set_mask & SET_SLOT) == 0 )
        {
            fprintf(stderr, "ERROR: Can't write to DAC, addr not specified\n");
            return 1;
        }

        if((set_mask & SET_CHANNEL)==0)
        {
            fprintf(stderr, "ERROR: : Can't write to DAC, channel not specified\n");
            return 1;
        }

        if((set_mask & SET_VALUE)==0)
        {
            fprintf(stderr, "ERROR: : Can't write to DAC, output value not specified\n");
            return 1;
        }

        if(newave3Map(&card, set_addr))
        {
            fprintf(stderr, "ERROR: can't initialise newave3 card in addr %d\n",set_addr);
            return 1;
        }

        card.ref.value[set_channel] = htonl(set_value);
        *card.ref.update_mask       = htons(0x00FF);

        newave3Unmap(&card);    
    }

    // print info for all available cards

    for(addr = 0 ; addr < NEWAVE3_NUM_ADDRS ; addr++)
    {
        if(newave3Map(&card, addr))
        {
            continue;
        }

        printf("\nSlot:%d\n--------\n",addr);

        for(channel = 0; channel<NEWAVE3_NUM_CHANS; channel++)
        {
            int32_t ref_value = ntohl(card.ref.value[channel]);

            float ratio =  ref_value / (float) 0x7FFFFFFF;

            printf("\tch[%d]=%d (%.2f%%)",channel,ntohl(card.ref.value[channel]), ratio*100);
            if(set_mask && addr==set_addr && channel==set_channel) printf(" <== writing value 0x%x",set_value);
            printf("\n");
        }
    
        newave3Unmap(&card);    
    }

    return(0);
}

// EOF
