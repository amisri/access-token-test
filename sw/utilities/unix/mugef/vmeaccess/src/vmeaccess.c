/*
 *  Filename: vme.c
 *
 *  Purpose:  Peek/poke vme utility
 *
 *  Author:   who cares?
 */

#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "mugefhw/vme.h"
#include "mugefhw/mpv915-21.h"

#define  MAX_LENGTH 128


int main(int argc, char * argv[])
{
    char                        c;
    int                         i;
    int                         num_tests = 0;
    int                         max_tests = 1;
    struct vme_map              map; 
    unsigned int                read_f = 0;
    unsigned int                write_f = 0;
    unsigned int                test_mode_f = 0;
    uint16_t                    data = 0;
    uint16_t                    data_r = 0;
    volatile uint16_t           *vme_ram;
    volatile uint16_t           dummy_read = 0;

    // Prevent warning about dummy_read being unused as it is used to read a value from the VME bus

    (void)dummy_read;

    // Process command-line options

    memset(&map, 0, sizeof(map));
    map.am           = MPV915_REG_VME_ADDR_MOD;
    map.data_width   = MPV915_REG_DATA_WIDTH;
//     map.size         = MPV915_REG_SIZE;
    map.size         = 1;


    while ((c = getopt(argc, argv, "?hrwv:a:l:d:t:")) != -1)
    {
        switch (c)
        {
            case 'v':
                if (optarg)
                {
                    map.address = strtol(optarg, NULL, 0);
                }
                else
                {
                    fprintf(stderr, "VME adress not specified\n\n");
                    exit(1);
                }
                break;

            case 'a': 
                if (optarg)
                {
                    map.am = strtol(optarg, NULL, 0);
                }
                else
                {
                    fprintf(stderr, "Address modifier  not specified\n\n");
                    exit(1);
                }
                break;

            case 'l':
                if (optarg)
                {
                    map.size = strtol(optarg, NULL, 0);

                    if (map.size > MAX_LENGTH)
                    {
                        fprintf(stderr, "Length out of range\n");
                        exit(1);
                    }
                }
                else
                {
                    fprintf(stderr, "Length not specified\n\n");
                    exit(1);
                }
                break;

            case 'd': 
                if (optarg)
                {
                    data = strtol(optarg, NULL, 0);
                }
                else
                {
                    fprintf(stderr, "Data not specified\n\n");
                    exit(1);
                }
                break;

            case 't': 
                if (optarg)
                {
                    max_tests = strtol(optarg, NULL, 0);
                    test_mode_f = 1;
                }
                else
                {
                    fprintf(stderr, "Data not specified\n\n");
                    exit(1);
                }
                break;


            case 'r':
                read_f = 1;
                break;

            case 'w':
                write_f = 1;
                break;

            case '?':
            case 'h':
                printf("Usage: %s [-h] %s\n", argv[0],
                       "		-h              Help\n"
                       "		-r              Read\n"
                       "		-w              Write\n"
                       "		-v n            vme address\n"
                       "		-a n            address modifier\n"
                       "		-l n            number of words\n"
                       "		-d n            data for write access\n"
                       "		-t n            number of iterations\n"
                      );
                exit(1);

            default :
                fprintf(stderr, "Wrong option\n\n");
                exit(1);
        }
    }

    // Attempt to map registers

    if(!(vme_ram = vmeMap(&map, 1)))
    {
        fprintf(stderr, "Failed to map at 0x%08x\n", map.address);
        return(1);
    }
    
    do
    {
        if (write_f)
        {
            for (i=0; i<map.size; i++)
            {
                vme_ram[i] = htons(data);
                usleep(1);
                dummy_read = ntohs(vme_ram[i]);
                if (num_tests == 0)
                {    
                    printf("[0x%08X, am 0x%02X] WRITE :\t0x%04X,\t%d\n", map.address + 2*i, map.am, data, data);
                }
    //             VME_INT16U_RW(VME_ADDR_BASE_16, map.address+i) = data;
            }
        }


        if (read_f)
        {
            for (i=0; i<map.size; i++)
            {
    //             data = VME_INT16U_RO(VME_ADDR_BASE_16, map.address+i);
                data_r = ntohs(vme_ram[i]);
                if (num_tests == 0)
                {    
                    printf("[0x%08X, am 0x%02X] READ :\t0x%04X,\t%d\n", map.address + 2*i, map.am, data_r, data_r);
                }
            }
        }
        
        num_tests++;
    }
    while ((num_tests != max_tests) && (data != data_r));


    if (test_mode_f && write_f)
    {
        printf("Test %s after %d attempts\n", (data == data_r)? "succeeded":"failed", num_tests);
    }


    printf("\n");
    vmeUnmap(&map);

    return (0);
}

// EOF
