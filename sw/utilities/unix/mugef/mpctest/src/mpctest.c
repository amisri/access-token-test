/*
 *  Filename: mpctest.c
 *
 *  Purpose:  Test MPC card access
 *
 *  Author:   Stephen Page
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "mugefhw/mpc.h"

int main(void)
{
    uint32_t        i;
    uint16_t        link_types[MUGEF_MAX_CHANNELS];
    struct mpc_card mpc;
    uint16_t        mpc_data_buf[MPC_DATA_SIZE];
    struct timeval  time;

    // Attempt to map MPC card

    if(mpcMap(&mpc))
    {
        fprintf(stderr, "Failed to map MPC card.\n");
        return(1);
    }
    printf("MPC mapped at: 0x%p\n",     mpc.regs);
    printf("Firmware version: %hu\n",   ntohs(mpc.regs->version_firmware));
    printf("PLD version: %hu\n",        ntohs(mpc.regs->version_pld));

    // Initialise MPC card

    if(mpcInit(&mpc))
    {
        fprintf(stderr, "Failed to initialise MPC card.\n");
        mpcUnmap(&mpc);
        return(1);
    }

    printf("MPC time: %u\n", (ntohs(mpc.regs->time_s_h) << 16) |
                              ntohs(mpc.regs->time_s_l));

    // Set MPC time

    gettimeofday(&time, NULL);
    mpcSetTime(&mpc, &time);

    // Read all link types

    if(mpcSendCommandWait(&mpc,
                          MPC_GLOBAL_CHANNEL,
                          MPC_CMD_GET_ALL_LINK,
                          0, NULL,
                          sizeof(mpc_data_buf), mpc_data_buf) != MUGEF_MAX_CHANNELS)
    {
        fprintf(stderr, "ERROR %s", (char *)mpc_data_buf);
        mpcUnmap(&mpc);
        return(1);
    }

    for(i = 0 ; i < MUGEF_MAX_CHANNELS ; i++)
    {
        link_types[i] = mpc_data_buf[i];
    }

    // Read all command acknowledgement and state values

    for(i = 0 ; i < MUGEF_MAX_CHANNELS ; i++)
    {
        printf("Channel %2u: link=0x%04X(%10s) status=0x%04X cmd=0x%04X(%10s) cmd_state=0x%04X(%10s)\n", i,
               link_types[i],                   mpc_link_name[link_types[i]], 
               ntohs(mpc.regs->status[i]),
               ntohs(mpc.regs->cmd[i]),         mpc_cmd_ack_name[ntohs(mpc.regs->cmd[i])],
               ntohs(mpc.regs->cmd_state[i]),   mpc_cmd_state_name[ntohs(mpc.regs->cmd_state[i])]);
    }

    // Unmap MPC card

    mpcUnmap(&mpc);
    return(0);
}

// EOF
