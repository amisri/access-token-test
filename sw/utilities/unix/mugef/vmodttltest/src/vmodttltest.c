/*
 *  Filename: vmodttlconfig.c
 *
 *  Purpose:  Configure the channels and disabling interrupts for the VMOD TTL card
 */

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "mugefhw/vmodttl.h"

void print_usage(char **argv)
{
  fprintf(stderr,"Usage: %s [options]\n"
			"      -h              This help\n"
			"      -?              This help\n"
			"      -i              Initialize the VMOD TTL card to communicate to the PLC system of the SPS Mains (default)\n"
			"      -r              Read the content of all the signals of the VMOD TTL card\n"
			"      -s signal       Sets the bit of the corresponding signal (i.e. VS_RUN_SMD or 48, VS_RUN_SMQF or 16, VS_RUN_SMQD or 0)\n"
			"      -c signal       Clears the bit of the correspondign signal\n\n"
			, argv[0]
	);
  fprintf(stderr,"Read and Write the signals arriving from the CIS SPS Mains System through the VMOD TTL card.\n");
	

}

// Return the channel number from the signal name (only commands)

int channel_number_from_str(char *ch_str,int *ch)
{
	int i;

	for (i=0; i != VMODTTL_MAP_LEN; ++i)
	{
		// if input matches a substr of one of the signals

		if (strstr(vmodttl_sps_mains_cmds[i].name,ch_str) != NULL)
		{
			*ch = vmodttl_sps_mains_cmds[i].mugef_ch;

			return 0;
		}
	}

	fprintf(stderr, "ERROR: Signal name not found '%s'\n",ch_str);

	return -1;


}

// Retrieve the channel number from either a number or a signal name

int channel_number(char *ch_str, int *ch)
{
	char * endptr;

	// Check that the option argument was specified

	if (!ch_str)
	{
		fprintf(stderr, "ERROR: Channel not specified (either a Mugef channel number or the string signal)\n");
	    return -1;
	}

	// Convert the string to number

	errno = 0;
	*ch = strtol(ch_str,&endptr,0);

	// Check that suceeded

	if (!errno && *ch_str != '\0' && *endptr == '\0')
	{
		if (*ch > MUGEF_MAX_CHANNELS)
		{
			fprintf(stderr,"ERROR: Mugef channel out of range (ch > %d)\n",MUGEF_MAX_CHANNELS);
			return -1;
		}
	}
	else
	{
		// if if didn't succeed then try to parse the signal id as a string

		if (channel_number_from_str(ch_str,ch))
		{
			return -1;
		}
	}

	return 0;

}

int read_info(struct vmodttl_card * vmod)
{
	int i;
	int fault,state;
	int err;


	for (i=0; i != VMODTTL_MAP_LEN; ++i)
	{
		// Read State for the channel

		if ((err = vmodttlGetState(vmod,vmodttl_sps_mains_status[i].mugef_ch,&state)))
		{
			fprintf(stderr,"ERROR: Read state failed (errno = %d)\n",err);
			return -1;
		}

		// Read Fault for the channel

		if ((err = vmodttlGetNoFault(vmod,vmodttl_sps_mains_faults[i].mugef_ch,&fault)))
		{
			fprintf(stderr,"ERROR: Read fault failed (errno = %d)\n",err);
			return -1;
		}

		printf("Mugef channel %d: \t%s = %d, \t%s = %d\n "
				,vmodttl_sps_mains_faults[i].mugef_ch
				,vmodttl_sps_mains_status[i].name
				,state
				,vmodttl_sps_mains_faults[i].name
				,fault);
	}

	return 0;
}

int main(int argc, char **argv)
{
    struct vmodttl_card vmod;
    int init = 0;
    int set = 0;
    int clear = 0;
    int read = 0;
    int cmd_channel = -1;
    int c;
    int err;

    // Parse input parameters

    while ((c = getopt(argc, argv, "?hirs:c:")) != -1)
    {
    	switch (c)
    	{
    		case 'i':
    			init = 1;
    			break;
    		case 'r':
    			read = 1;
    			break;
    		case 's':
    		case 'c':
    			if (c == 's')
    			{
    				set = 1;
    			}
    			else
    			{
    				clear = 1;
    			}

   				if (channel_number(optarg,&cmd_channel))
   				{
   					exit(1);
   				}

   				break;

    		case '?':
    		case 'h':
    			print_usage(argv);
    			exit(1);
    			break;
    		default :
		        init = 1;
    	                break;
    	}
    }


    // Attempt to map the card

    if ( (err = vmodttlMap(&vmod)) )
    {
		fprintf(stderr,"ERROR: Cannot open device (errno=%d)\n",err);
		exit(1);
    }

    // Initialize the card

    if (init)
    {
    	// Attempt to configure the card

    	if ( (err = vmodttlInit(&vmod)) )
    	{
    		fprintf(stderr,"ERROR: Cannot configure the VMOD TTL device (errno=%d)\n",err);
    		exit(1);
    	}
    }

    // Clear or set the channels

    if (clear || set)
    {
    	if ( (err = vmodttlSetState(&vmod,cmd_channel,set?1:0)) )
    	{
    		fprintf(stderr,"ERROR: Write command failed (errno = %d)\n",err);
    		exit(1);
    	}
    }

    // Read all the channels

    if (read)
    {
    	if (read_info(&vmod))
    	{
    		exit(1);
    	}
    }

    // Unmap the card

    vmodttlUnmap(&vmod);

    return(0);
}

// EOF
