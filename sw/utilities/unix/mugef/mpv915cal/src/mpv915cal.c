/*
 *  Filename: mpv915cal.c
 *
 *  Purpose:  Calibrate MPV915-21 cards
 *
 *  Author:   Stephen Page
 */

#include <stdio.h>

#include "mugefhw/mpv915-21.h"

#define CAL_MAX_ATTEMPTS    5

int main(void)
{
    unsigned int                cal_attempt;
    unsigned int                cal_failed;
    struct mpv915_cal_results   cal_results;
    struct mpv915_card          card;
    unsigned int                failed_cards = 0;
    unsigned int                i;

    // Process each possible card

    for(i = 0 ; i < MPV915_NUM_ADDRS ; i++)
    {
        // Attempt to map card

        if(mpv915Map(&card, i))
        {
            // Card not mapped

            continue;
        }
        printf("Card %u mapped.\n", i);

        // Calibrate card

        cal_attempt = 1;
        do
        {
            printf("    Calibrating (attempt %u/%d)...\n", cal_attempt, CAL_MAX_ATTEMPTS);

            // Perform calibration

            cal_failed = mpv915Calibrate(&card, &cal_results) ? 1 : 0;

            // Print calibration results

            printf("        Neg    %9.3f %9.3f\n", cal_results.neg[0],  cal_results.neg[1]);
            printf("        Zero   %9.3f %9.3f\n", cal_results.zero[0], cal_results.zero[1]);
            printf("        Pos    %9.3f %9.3f\n", cal_results.pos[0],  cal_results.pos[1]);
        } while(cal_failed && cal_attempt++ < CAL_MAX_ATTEMPTS);

        // Check whether all calibration attempts failed

        if(cal_failed)
        {
            failed_cards++;
            printf("    Calibration failed.\n");
        }
        else // Calibration succeeded
        {
            printf("        Gain   %5u     %5u\n", cal_results.adc_gain[0],   cal_results.adc_gain[1]);
            printf("        Offset %5u     %5u\n", cal_results.adc_offset[0], cal_results.adc_offset[1]);

            printf("    Calibration successful.\n");
        }

        // Unmap MPV915-21 card

        mpv915Unmap(&card);

        printf("\n");
    }
    return(failed_cards);
}

// EOF
