#!/bin/sh
#
# Filename: history.sh
#
# Purpose:  Add information to history files when a new version is installed

onfail()
{
   # Recover original history file if something fails

   echo "$0: Something went wrong, exiting." >&2
   git checkout $2
   exit 1
}

if [ $# -ne 3 ]; then
    echo "Usage: $0 <version> <history file> <package>"
    exit 1
fi

cpu=$(uname -m)
os=$(uname -s)
str_date=`date +"%a %b %d %H:%M:%S %Y"`

VERSION=$1
HISTORY_FILE=$2
PACKAGE=$3

trap onfail INT TERM ERR 

# Update history file

mv $HISTORY_FILE ${HISTORY_FILE}.tmp
echo -e "${PACKAGE}: $os/$cpu $str_date (version $VERSION)\n" > $HISTORY_FILE
cat ${HISTORY_FILE}.tmp >>$HISTORY_FILE
rm -f ${HISTORY_FILE}.tmp

# Commit and tag the new release

git add $HISTORY_FILE
git commit -m "Release $PACKAGE/$VERSION"

# EOF
