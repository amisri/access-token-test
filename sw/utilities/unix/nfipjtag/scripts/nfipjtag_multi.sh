#!/bin/bash
#
# Name:     nfipjtag_multi.sh
# Purpose:  Program multiple JTAG devices over a WorldFIP bus
# Author:   Stephen Page

# Path to nfipjtag executable

jtag=~pclhc/bin/$(uname -s)/$(uname -m)/nfipjtag

# Print program usage

printusage()
{
    echo -e "\nUsage: $0 [-b<BUS SPEED>] [-i<INTERFACE>] (-s<FILE> | -x<FILE>) <ADDRESS 1> [... <ADDRESS n>]\n\n" \
            "            -b<BUS SPEED>   Bus speed (0=31.25Kb/s, 1=1000Kb/s, 2=2500Kb/s (default))\n" \
            "            -i<INTERFACE>   FullFIP interface number (default 0)\n" \
            "            -s<FILE>        SVF file\n" \
            "            -x<FILE>        XSVF file\n" >&2
}

# End of functions


# Print usage if no arguments are supplied

if [ $# -eq 0 ]; then
    printusage
    exit 1
fi

# Set default option values

bus_speed=2
interface=0

# Process options

while getopts "b:i:s:x:" c
do
    case $c in
        b)
            bus_speed=$OPTARG
            ;;

        i)
            interface=$OPTARG
            ;;

        s)
            file_opt=s
            filename=$OPTARG
            ;;

        x)
            file_opt=x
            filename=$OPTARG
            ;;

        *)
            printusage
            exit 1
            ;;
    esac
done

# Check that a file name was specified

if [ -z "$filename" ]; then
    echo No file was specified. >&2
    printusage
    exit 1
fi

# Check that addresses were specified

if [ $OPTIND -gt $# ]; then
    echo No addresses were specified. >&2
    printusage
    exit 1
fi

# Catch signals to exit

trap "exit 1" SIGHUP SIGINT SIGTERM

# Run the JTAG operation for each supplied address

failures=
successes=
for address in "${@:$OPTIND}"
do
    echo Sending file $filename to interface $interface address $address...

    echo "$jtag" -i"$interface" -b"$bus_speed" -"$file_opt" "$address" "$filename"
    if   "$jtag" -i"$interface" -b"$bus_speed" -"$file_opt" "$address" "$filename"; then
        # Add the address to the list of successes

        successes="$successes $address"
    else # The JTAG operation failed
        # Add the address to the list of failures

        failures="$failures $address"
    fi

    echo
done

# Report a summary of successes and failures

if [ -n "$successes" ]; then
    echo -e "Successful addresses:$successes"
fi

if [ -n "$failures" ]; then
    echo -e "    Failed addresses:$failures"
    exit 1
fi

# EOF
