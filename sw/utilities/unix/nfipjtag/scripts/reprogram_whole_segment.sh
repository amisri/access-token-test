#!/bin/bash
#
# Reprogram FGClite FPGAs
#

vector=`seq $2 $3`

while : ; do
    echo " "
    echo " "
    echo " "
    echo "================================================================================"
    echo "==========================> Devices to be programmed:<=========================="
    echo "================================================================================"
    echo $vector
    echo " "
    failedvector=""
    for station in $vector
    do
        sleep 25
        sudo ~pclhc/bin/nfipjtag_multi.sh -s $1/program_cf.svf $station
        if [ $? -eq 0 ]
        then
            sleep 25
            sudo ~pclhc/bin/nfipjtag_multi.sh -s $1/verify_cf.svf $station
            if [ $? -eq 0 ]
            then
                echo "--------------------------------------------------------------------------------"
                echo "------->Programming of device $station successful, removing it from the list! <-------"
                echo "--------------------------------------------------------------------------------"
            else
                failedvector="$failedvector $station"
            fi
        else
            failedvector="$failedvector $station"
        fi
        sleep 25
    done
    if [ -z "$failedvector" ]
    then
	echo ""
	echo "============================= OPERATION COMPLETED! ============================="
	echo "======================ALL DEVICES CORRECTLY REPROGRAMMED========================"
	echo "=============================== GREAT SUCCESS!!! ==============================="
        echo ""
	break
    else
	echo ""
	echo "<< SOME DEVICES ARE STILL UNPROGRAMMED: WILL REITERATE THE PROCESS ON THEM... >>"
    fi
    vector=$failedvector
done

# EOF
