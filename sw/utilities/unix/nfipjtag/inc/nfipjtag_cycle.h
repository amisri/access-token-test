/*!
 * @file   nfipjtag_cycle.h
 * @brief  Define the BA program to reprogram FPGAs attached to a nanoFIP
 * @author Michael Davis
 */

#ifndef __NFIPJTAG_CYCLE_H
#define __NFIPJTAG_CYCLE_H

#include <stdint.h>
#include <stdbool.h>
#include <mqueue.h>



// Types

/*!
 * Values for TRST reset
 */

enum Nfipjtag_TRST_value
{
    TRST_LOW  = 0xA5,
    TRST_HIGH = 0xDB
};



// Structures for Nfipjtag command and reception variables

/*!
 * TRST command
 */

struct __attribute__((__packed__)) Nfipjtag_TRST_command
{
    uint8_t low_high;
};



/*!
 * Transmission buffer for JTAG data
 */

struct __attribute__((__packed__)) Nfipjtag_tx_var
{
    uint16_t length_bits;       //!< Length of data within jtag_bits
    uint8_t  jtag_bits[122];    //!< Encoded JTAG data
};



/*!
 * Nfipjtag reception variable
 */

struct __attribute__((__packed__)) Nfipjtag_rx_var
{
    uint8_t tdo;            //!< TDO (least significant bit)
    uint8_t nfip_status;    //!< nanoFIP status
};



/*!
 * nanoFIP reset variable
 */

struct __attribute__((__packed__)) Nfipjtag_nanofip_reset
{
    char addr[2];    //!< Byte 0 is address of device to soft reset. Byte 1 is address of device to power cycle.
};



/*!
 * Transmit queue item
 */

struct Nfipjtag_tx_queue_item
{
    uint32_t               delay_us;      //!< Number of microseconds to delay without sending data
    bool                   rx_request;    //!< Flag to indicate the request of received data

    struct Nfipjtag_tx_var payload;       //!< Data to be transmitted across the FIP bus
};



/*!
 * Structure for queue descriptors
 */

struct Nfipjtag_queues
{
    mqd_t rx_queue;    //!< Queue to receive FIP variables
    mqd_t tx_queue;    //!< Queue to transmit FIP variables
};



/*!
 * Queue descriptors
 */

extern struct Nfipjtag_queues nfipjtag_q;



// External function declarations with C linkage

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Set TRST high or low.
 *
 * This function must be called with the FIP interface up but with no BA macrocycle program running.
 *
 * On FGClite, TRST is active low, to protect against radiation-induced Single Event Upsets (SEUs).
 * TRST must be set high before reprogramming using JTAG, and set low again afterwards. This program
 * runs in its own FIP bus cycle, so it can't be called while the main FIP BA is running.
 *
 * Note that there is no way to read the TRST value back from the device, so it is not possible to be
 * sure that the command succeeded. However, if TRST is low, it is not possible to reprogram the device
 * and if it is high, it is not possible to do a remote power cycle.
 *
 * @param[in] address    FIP address of the device to set
 * @param[in] value      Value to set: TRST_LOW or TRST_HIGH
 *
 * @retval  0    The command probably succeeded (at least, it did not encounter an error)
 * @retval -1    The command failed
 */

int32_t nfipjtagCycleSetTRST(uint8_t address, enum Nfipjtag_TRST_value value);



/*!
 * Initialise the inter-process communication for JTAG programming.
 */

int32_t nfipjtagCycleInit(void);



/*!
 * Clean up inter-process communication.
 */

void nfipjtagCycleCleanUp(void);



/*!
 * Initialise and start the JTAG FIP BA macrocycle program.
 *
 * @param[in] address      WorldFIP address of the device to be reprogrammed
 * @param[in] bus_speed    Set to 0=31.25 Kb/s, 1=1000 Kb/s or 2=2500 Kb/s
 *
 * @retval  0    The BA was started successfully
 * @retval -1    Starting the BA failed
 */

int32_t nfipjtagCycleStartProtocol(uint8_t address, uint8_t bus_speed);

#ifdef __cplusplus
}
#endif

#endif

// EOF
