/*!
 * @file   jtag.h
 * @brief  WorldFIP JTAG programmer
 * @author Stephen Page
 * @author Michael Davis
 */

#ifndef JTAG_H
#define JTAG_H

#include <stdbool.h>

// Global variables

extern bool quiet_mode;
extern bool debug_mode;

// Types of input file

enum jtag_file_type
{
    JTAG_FILE_UNSET,
    JTAG_FILE_SVF,
    JTAG_FILE_XSVF
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Perform JTAG programming
 *
 * @param[in] filename    Name of the file to program
 * @param[in] type        Specify if the file is in SVF or XSVF format
 *
 * @retval  0    Programming completed successfully
 * @retval -1    An error occurred
 */

int jtagProg(char *filename, enum jtag_file_type type);



/*!
 * Signal handler.
 *
 * When the JTAG programmer receives a signal, it sets a flag indicating that jtagProg should clean up and exit.
 *
 * @param[in] signal    Signal number
 */

void jtagSignalHandler(int signal);

#ifdef __cplusplus
}
#endif

#endif

// EOF
