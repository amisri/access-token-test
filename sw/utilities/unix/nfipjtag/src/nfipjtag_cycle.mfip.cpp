/*!
 * @file   nfipjtag_cycle.cpp
 * @brief  Define the BA program to reprogram FPGAs attached to a nanoFIP
 * @author Michael Davis
 */

#include <cstdio>
#include <cerrno>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <time.h>
#include <string.h>

#include <fip.mfip.h>
#include <nfipjtag_cycle.h>

// Constants and enumerated types

// POSIX queues

const uint32_t JTAG_RX_QUEUE_LENGTH   = 1;           // Reception message queue length
const char*    JTAG_RX_QUEUE_NAME     = "/jtag_rx";  // Name of reception message queue
const uint32_t JTAG_TX_QUEUE_LENGTH   = 10;          // Transmission message queue length
const char*    JTAG_TX_QUEUE_NAME     = "/jtag_tx";  // Name of transmission message queue

// Nanofip status mask

enum Nfipjtag_nanofip_status_mask
{
    JTAG_NFIP_STAT_U_CACER = 0x04,                   // User consumed variable access error
    JTAG_NFIP_STAT_U_PACER = 0x08,                   // User produced variable access error
    JTAG_NFIP_STAT_R_TLER  = 0x10,                   // Received control or PDU_TYPE error or incoherent length
    JTAG_NFIP_STAT_R_FCSER = 0x20,                   // Received FCS or Manchester encoding or bit number error
    JTAG_NFIP_STAT_T_TXER  = 0x40,                   // Transmit error (FIELDRIVE)
    JTAG_NFIP_STAT_T_WDER  = 0x80                    // Watchdog error (FIELDRIVE)
};

const uint8_t JTAG_NFIP_STAT_BITS = JTAG_NFIP_STAT_R_TLER | JTAG_NFIP_STAT_R_FCSER | JTAG_NFIP_STAT_T_TXER  | JTAG_NFIP_STAT_T_WDER;

/*!
 * Unique frame IDs for the BA program, as defined in the Nfipjtag Fieldbus Cycle document, section 3.
 * These values specify ID_DAT for each frame. Each value must be used at most once per cycle. Note
 * that the packing ID is reserved (0x9080).
 */

enum Nfipjtag_protocol_frame_id
{
    NFIPJTAG_NOID,                            //!< Used for frames which do not correspond to a variable

    NFIPJTAG_FIP_TRST            = 0x0500,    //!< nanoFIP VAR1 frame to set TRST high or low. Last two bytes specify the FIP address.
    NFIPJTAG_FIP_ID_TX           = 0xAA00,    //!< JTAG transmission frame. Last two bytes specify the FIP address.
    NFIPJTAG_FIP_ID_RX           = 0xAB00,    //!< JTAG reception frame. Last two bytes specify the FIP address.

    NFIPJTAG_NANOFIP_RESET       = 0xE000     //!< nanoFIP RESET (R) command. The addresses to reset is specified in the (two-byte) payload.
};

/*!
 * Structure for Nfipjtag data pointers and device statuses
 */

struct Nfipjtag_data
{
    // WorldFIP cycle period (used to handle delay requests)

    uint32_t cycle_period;

    // Address of the device to be reprogrammed

    uint8_t address;

    // Flag to indicate device is still sending data

    bool rx_status;

    // Send response next cycle?

    bool rx_request;

    // Pointers to Nfipjtag variable buffers

    struct Nfipjtag_TRST_command    *trst;                    //!< Command to set TRST high or low
    struct Nfipjtag_tx_var          *tx_var;                  //!< Command to transmit JTAG data
    struct Nfipjtag_rx_var          *rx_var;                  //!< nanoFIP response
    struct Nfipjtag_nanofip_reset   *nanofip_reset;           //!< nanoFIP reset variable
};

// Declarations of callback functions

static void nfipjtagTxVarSent(struct mstrfip_dev *dev,
			struct mstrfip_data *data, struct mstrfip_irq *irq);

static void nfipjtagRxVarReceived(struct mstrfip_dev *dev,
			struct mstrfip_data *data, struct mstrfip_irq *irq);

// Global variables

extern bool     quiet_mode;                                 // From nfipjtag_main.cpp
extern bool     debug_mode;                                 // From nfipjtag_main.cpp
extern uint32_t ba_wait_time[2];                            // From nfipjtag_main.cpp
extern uint32_t silence_time_us;                            // From nfipjtag_main.cpp

/*
 * Queue descriptors (extern). Initialised to zero.
 */

struct Nfipjtag_queues nfipjtag_q = {};

/*
 * Variable data pointers and IPC for Nfipjtag cycle. Initialised to zero.
 */

struct Nfipjtag_data nfipjtag = {};

/*
 * Address reinterpret cast macro (to make the FIP_protocol_frame tables below more legible)
 */

#define ADDR(x) reinterpret_cast<void **>(&x)

/*
 * Definition of the fieldbus cycle to set/unset TRST
 *
 * Each line defines one frame of the Bus Arbitrator macrocycle.
 */

struct fgc_fip_per_var_desc nfipjtag_per_varlist1[] = {
    // send TRST
    {NFIPJTAG_FIP_TRST, FGC_FIP_PER_VAR_PROD, sizeof(Nfipjtag_TRST_command), NULL, ADDR(nfipjtag.trst)},
};
#define NFIPJTAG_PER_VARLIST1_NVAR sizeof(nfipjtag_per_varlist1) / sizeof(struct fgc_fip_per_var_desc)
// Macro cycle duration for TRST cycle.
// This time should be enough even for the lowest bus speed 31.25 Kb/s
#define NFIPJTAG_TRST_MCYCLE_USTIME 10000

/*!
 * Definition of the fieldbus cycle to reprogram the FPGAs.
 *
 * Each line defines one frame of the Bus Arbitrator macrocycle.
 */

struct fgc_fip_per_var_desc nfipjtag_per_varlist2[] = {
    // send JTAG TDI command
    {NFIPJTAG_FIP_ID_TX, FGC_FIP_PER_VAR_PROD, sizeof(Nfipjtag_tx_var), &nfipjtagTxVarSent, ADDR(nfipjtag.tx_var)},
};
#define NFIPJTAG_PER_VARLIST2_NVAR sizeof(nfipjtag_per_varlist2) / sizeof(struct fgc_fip_per_var_desc)

struct fgc_fip_per_var_desc nfipjtag_per_varlist3[] = {
    // Receive JTAG TDO reply
    {NFIPJTAG_FIP_ID_RX, FGC_FIP_PER_VAR_CONS, sizeof(Nfipjtag_rx_var), &nfipjtagRxVarReceived, ADDR(nfipjtag.rx_var)},
};
#define NFIPJTAG_PER_VARLIST3_NVAR sizeof(nfipjtag_per_varlist3) / sizeof(struct fgc_fip_per_var_desc)

struct fgc_fip_mcycle_desc *nfipjtag_mcycle_desc = NULL;

// Static functions

#if 0
/*
 * Unpack and display contents of a transmit queue item.
 *
 * Useful for debugging: the bit patterns can be compared to what is seen on the nanoFIP TDI and TMS pins with a scope.
 */

static void debugPrintf(const struct Nfipjtag_tx_queue_item &item)
{
    uint8_t l;
    uint8_t byte;
    uint8_t bit;

    fprintf(stderr, "delay_us = %d, rx_request = %d, length_bits = %d, jtag_bits = ", item.delay_us, item.rx_request, item.payload.length_bits);
    for(l = 0; l <= item.payload.length_bits / 8; ++l)
    {
        fprintf(stderr, "%02x", item.payload.jtag_bits[l]);
    }
    fprintf(stderr, "\nTDI: ");
    for(l = 0; l < item.payload.length_bits; l += 2)
    {
        byte = l / 8;
        bit  = 0x40 >> (l % 8);
        fprintf(stderr, "%d", (item.payload.jtag_bits[byte] & bit) ? 1 : 0);
    }
    fprintf(stderr, "\nTMS: ");
    for(l = 0; l < item.payload.length_bits; l += 2)
    {
        byte = l / 8;
        bit  = 0x80 >> (l % 8);
        fprintf(stderr, "%d", (item.payload.jtag_bits[byte] & bit) ? 1 : 0);
    }
    fprintf(stderr, "\n");
}
#endif

/*
 * Callback after sending transmission variable
 */

static void nfipjtagTxVarSent(struct mstrfip_dev *dev,
			struct mstrfip_data *data, struct mstrfip_irq *irq)
{
    static uint32_t               delay_cycles = 0;

    mq_attr                       tx_q_attr;
    struct Nfipjtag_tx_queue_item next_item;

    // Check device is alive

    if(!nfipjtag.rx_status)
    {
        fprintf(stderr, "\rFIP variable not received from device %d.\n", nfipjtag.address);
        raise(SIGHUP);
    }
    nfipjtag.rx_status = false;

    // Read a message from the transmit queue, if there is no delay this cycle and there is at least one item in the queue
    // (as we don't want to block in a callback)

    if(delay_cycles == 0 &&
       mq_getattr(nfipjtag_q.tx_queue, &tx_q_attr) == 0 &&
       tx_q_attr.mq_curmsgs > 0)
    {
        // Read data from the transmit queue

        ssize_t qitem_size = mq_receive(nfipjtag_q.tx_queue, reinterpret_cast<char*>(&next_item), sizeof(struct Nfipjtag_tx_queue_item), NULL);

        if(qitem_size == sizeof(struct Nfipjtag_tx_queue_item))
        {
            // Set delay_cycles if a delay was requested

            if(next_item.delay_us > 0)
            {
                delay_cycles = next_item.delay_us / nfipjtag.cycle_period + 1;
                if(next_item.delay_us > 1000000 && quiet_mode == false)
                {
                    printf("  Delay for %ds                                   \r", next_item.delay_us / 1000000);
                    fflush(stdout);
                }
            }

            // The length is specified as big-endian (see EDMS document 1107940: nanoFIP Functional Specification, Annex: JTAG Feature)

            next_item.payload.length_bits = htons(next_item.payload.length_bits);

            nfipjtag.rx_request = next_item.rx_request;

            memcpy(nfipjtag.tx_var, &next_item.payload, sizeof(struct Nfipjtag_tx_var));
        }
        else
        {
            perror("nfipjtagTxVarSent(): mq_receive");
            raise(SIGHUP);
        }

    }
    else if(delay_cycles > 0)
    {
        --delay_cycles;
    }

    // Write transmission variable

    fipMfipWriteVar(NFIPJTAG_FIP_ID_TX | nfipjtag.address);

    // Zero the length to stop data being sent again in next BA cycle

    nfipjtag.tx_var->length_bits = 0;
}

/*
 * Callback on arrival of reception variable
 */

static void nfipjtagRxVarReceived(struct mstrfip_dev *dev,
			struct mstrfip_data *data, struct mstrfip_irq *irq)
{
    static FIP_device_stats error_stats = {};
    static bool             rx_request  = false;

    // Set device alive flag

    nfipjtag.rx_status = true;

    // Process the received status variable

    if(!fipMfipReadVar(data, &error_stats))
    {
        fprintf(stderr, "WorldFIP error: MPS status fault.\n");
        raise(SIGHUP);
    }

    // Check whether the nanoFIP reports an error

    if(nfipjtag.rx_var->nfip_status & JTAG_NFIP_STAT_BITS)
    {
        printf("\n");
        fprintf(stderr, "WorldFIP error: nanoFIP status 0x%02X\n", nfipjtag.rx_var->nfip_status);
        raise(SIGHUP);
    }

    // Handle request for received data

    if(rx_request)
    {
        mq_attr rx_q_attr;

        // Check queue is not full (as we don't want to block in a callback)

        if(mq_getattr(nfipjtag_q.tx_queue, &rx_q_attr) != 0)
        {
            perror("nfipjtagRxVarReceived(): mq_getattr");
            raise(SIGHUP);
        }

        if(rx_q_attr.mq_curmsgs == rx_q_attr.mq_maxmsg)
        {
            fprintf(stderr, "nfipjtagRxVarReceived(): Receive queue is full, increase JTAG_RX_QUEUE_LENGTH and recompile.\n");
            raise(SIGHUP);
        }

        // Add data to reception queue

        if(mq_send(nfipjtag_q.rx_queue, reinterpret_cast<char*>(nfipjtag.rx_var), sizeof(struct Nfipjtag_rx_var), 0))
        {
            perror("nfipjtagRxVarReceived(): mq_send");
            raise(SIGHUP);
        }
    }

    rx_request          = nfipjtag.rx_request;
    nfipjtag.rx_request = false;
}

void nfipjtagFreeMacroCycleDesc() {
    if (nfipjtag_mcycle_desc == NULL)
    	return;

    delete nfipjtag_mcycle_desc;
    nfipjtag_mcycle_desc = NULL;
}

#define NFIPJTAG_TRST_MCYCLE_DESC_NENTRIES 2
static void nfipjtagTRSTBuildMacroCycleDesc()
{
    nfipjtagFreeMacroCycleDesc();

    nfipjtag_mcycle_desc = new fgc_fip_mcycle_desc[NFIPJTAG_TRST_MCYCLE_DESC_NENTRIES];
    /* periodic window */
    nfipjtag_mcycle_desc[0].type = FGC_FIP_PER_VAR_WIND;
    nfipjtag_mcycle_desc[0].per_var_wind.nvar = NFIPJTAG_PER_VARLIST1_NVAR;
    nfipjtag_mcycle_desc[0].per_var_wind.varlist = nfipjtag_per_varlist1;

    /* wait window */
    nfipjtag_mcycle_desc[1].type = FGC_FIP_WAIT_WIND;
    nfipjtag_mcycle_desc[1].wait_wind.end_ustime = NFIPJTAG_TRST_MCYCLE_USTIME;
    nfipjtag_mcycle_desc[1].wait_wind.silent = 1;
};

// External functions

int32_t nfipjtagCycleSetTRST(uint8_t address, enum Nfipjtag_TRST_value value)
{
    struct timespec sleep_time;

    // Initialise the FIP BA macrocycle program

    nfipjtag_per_varlist1[0].id &= 0xFF00;
    nfipjtag_per_varlist1[0].id |= address;

    nfipjtagTRSTBuildMacroCycleDesc();
    fipMfipInitProtocol(nfipjtag_mcycle_desc, NFIPJTAG_TRST_MCYCLE_DESC_NENTRIES);

    // Initialise FIP variables

    nfipjtag.trst->low_high = value;

    // Start the FIP BA macrocycle program

    if(fipMfipStartProtocol() != 0)
    {
        fprintf(stderr, "nfipjtagCycleSetTRST(): Failed to start BA macrocycle with error %d.\n", errno);
        nfipjtagFreeMacroCycleDesc();
	return -1;
    }

    // Note: it is not possible to write variables before AE/LE has started

    fipMfipWriteVars();

    // wait a few cycles to process the command

    sleep_time.tv_sec = 0;
    sleep_time.tv_nsec = 100000000;
    while(nanosleep(&sleep_time, &sleep_time) && errno == EINTR);

    // Shut down the FIP BA macrocycle program

    fipMfipStopProtocol();
    return 0;
}



int32_t nfipjtagCycleInit(void)
{
    // Initialise the queues to communicate between the JTAG file parser and the Nfipjtag cycle thread.

    struct mq_attr queue_attr = {};

    // Message queues persist after the process quits, so remove and recreate them to be sure they are empty.

    if((mq_unlink(JTAG_RX_QUEUE_NAME) == -1 && errno != ENOENT) ||
       (mq_unlink(JTAG_TX_QUEUE_NAME) == -1 && errno != ENOENT))
    {
        fprintf(stderr, "nfipjtagCycleInit(): error unlinking message queues.\n");
        return -1;
    }

    // Create reception message queue

    queue_attr.mq_maxmsg  = JTAG_RX_QUEUE_LENGTH;
    queue_attr.mq_msgsize = sizeof(struct Nfipjtag_rx_var);

    nfipjtag_q.rx_queue = mq_open(JTAG_RX_QUEUE_NAME, O_RDWR | O_CREAT | O_EXCL, S_IREAD | S_IWRITE, &queue_attr);
    if(nfipjtag_q.rx_queue == -1)
    {
        fprintf(stderr, "nfipjtagCycleInit(): error initialising reception message queue.\n");
        return -1;
    }

    // Create transmission message queue

    queue_attr.mq_maxmsg  = JTAG_TX_QUEUE_LENGTH;
    queue_attr.mq_msgsize = sizeof(struct Nfipjtag_tx_queue_item);

    nfipjtag_q.tx_queue = mq_open(JTAG_TX_QUEUE_NAME, O_RDWR | O_CREAT | O_EXCL, S_IREAD | S_IWRITE, &queue_attr);
    if(nfipjtag_q.tx_queue == -1)
    {
        fprintf(stderr, "nfipjtagCycleInit(): error initialising transmission message queue.\n");
        return -1;
    }

    return 0;
}



void nfipjtagCycleCleanUp(void)
{
    // Shut down FIP FDM if it is running

    fipMfipStopInterface();

    nfipjtagFreeMacroCycleDesc();

    // Clean up the queues

    mq_close(nfipjtag_q.rx_queue);
    mq_close(nfipjtag_q.tx_queue);

    mq_unlink(JTAG_RX_QUEUE_NAME);
    mq_unlink(JTAG_TX_QUEUE_NAME);
}


#define NFIPJTAG_MCYCLE_DESC_NENTRIES 4
static void nfipjtagBuildMacroCycleDesc(uint32_t *wait_time)
{
    nfipjtagFreeMacroCycleDesc();

    nfipjtag_mcycle_desc = new fgc_fip_mcycle_desc[NFIPJTAG_MCYCLE_DESC_NENTRIES];
    /* periodic window */
    nfipjtag_mcycle_desc[0].type = FGC_FIP_PER_VAR_WIND;
    nfipjtag_mcycle_desc[0].per_var_wind.nvar = NFIPJTAG_PER_VARLIST2_NVAR;
    nfipjtag_mcycle_desc[0].per_var_wind.varlist = nfipjtag_per_varlist2;

    /* wait window */
    nfipjtag_mcycle_desc[1].type = FGC_FIP_WAIT_WIND;
    nfipjtag_mcycle_desc[1].wait_wind.end_ustime = wait_time[0];
    nfipjtag_mcycle_desc[1].wait_wind.silent = 1;

    /* periodic window */
    nfipjtag_mcycle_desc[2].type = FGC_FIP_PER_VAR_WIND;
    nfipjtag_mcycle_desc[2].per_var_wind.nvar = NFIPJTAG_PER_VARLIST3_NVAR;
    nfipjtag_mcycle_desc[2].per_var_wind.varlist = nfipjtag_per_varlist3;

    /* wait window */
    nfipjtag_mcycle_desc[3].type = FGC_FIP_WAIT_WIND;
    nfipjtag_mcycle_desc[3].wait_wind.end_ustime = wait_time[1];
    nfipjtag_mcycle_desc[3].wait_wind.silent = 1;
};


int32_t nfipjtagCycleStartProtocol(uint8_t address, uint8_t bus_speed)
{
    uint32_t wait_time[2];
    // Set BA wait times based on bus speed.

    // Note: these timings were determined empirically using a scope.
    //
    // The delay to allow JTAG programming to complete can be calculated as follows:
    //
    // The max data size is 122 bytes = 976 bits. This is processed at a rate of two bits per clock cycle
    // (1xTMS bit + 1xTDI bit). The JTAG reprogramming clock runs at 5 MHz, so max. total time to reprogram
    // is 97.6 microseconds. This is the programming time only: there is an additional overhead as the nanoFIP
    // sets up the clock, transfers the program and transfers the sampled TDO value back at the end.

    nfipjtag.address = address;

    // Set node ID to the required address

    nfipjtag_per_varlist2[0].id |= address;
    nfipjtag_per_varlist3[0].id |= address;

    // Set BA_WAIT times according bus speed

    switch(bus_speed)
    {
        case 0: // 31.25 Kb/s

            wait_time[0]          = 38700;
            wait_time[1]          = 48700;
            nfipjtag.cycle_period = 69700;
            break;

        case 1: // 1000 Kb/s

            wait_time[0]          = 1950;
            wait_time[1]          = 2420;
            nfipjtag.cycle_period = 2420;
            break;

        default: // 2500 Kb/s

            wait_time[0]          = ba_wait_time[0];
            wait_time[1]          = ba_wait_time[1] + wait_time[0];
            nfipjtag.cycle_period = 40 + wait_time[1];
    }

    if(debug_mode)
    {
        printf("WAIT1: %u    WAIT2: %u    Period: %u\n",
                wait_time[0], wait_time[1],
                nfipjtag.cycle_period);
    }

    // Initialise the FIP BA macrocycle program

    nfipjtagBuildMacroCycleDesc(wait_time);
    fipMfipInitProtocol(nfipjtag_mcycle_desc, NFIPJTAG_MCYCLE_DESC_NENTRIES);

    // Initialise FIP variables

    nfipjtag.rx_status           = true;
    nfipjtag.tx_var->length_bits = 0;

    // Start the FIP BA macrocycle program

    if(fipMfipStartProtocol() != 0) return -1;

    // Note: it is not possible to write variables before AE/LE has started

    fipMfipWriteVars();

    return 0;
}

// EOF
