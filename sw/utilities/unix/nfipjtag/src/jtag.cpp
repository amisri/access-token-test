/*!
 * @file   jtag.c
 * @brief  WorldFIP JTAG programmer
 * @author Stephen Page
 * @author Michael Davis
 *
 * The functions within this file read an SVF or XSVF input file and translate the contents into JTAG signals (TDI, TMS and TDO)
 * using libxsvf. TDI and TMS are buffered into structures, which are then added to a message queue for transmission over a
 * WorldFIP bus. A second message queue is used to receive TDO for verification against expected values.
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <stdlib.h>
#include <libxsvf.h>
#include <jtag.h>
#include <nfipjtag_cycle.h>

// Constants

#define MAX_JTAG_BITS             (4 * 122)       // 4 pairs of [TMS,TDI] per transmitted byte

// Global variables

struct Jtag
{
    bool                          error;          // Flag to indicate that an error has occurred

    int                           input_fd;       // File descriptor for the input file
    struct stat                   input_stat;     // File status/size for the input file
    int32_t                       bits_sent;      // For progress meter
    int32_t                       bytes_read;     // For progress meter
    int32_t                       file_size;      // For progress meter
    int32_t                       read_rate;      // Bytes read from file per second
    int32_t                       sent_rate;      // JTAG TDI bits sent per second
    bool                          eof;            // Flag to indicate that the end of the input file has been reached

    struct Nfipjtag_tx_queue_item tx_msg;         // item to add to the transmit queue
};

struct Jtag jtag = {};

// Static variables

static time_t   start_time;                             // Start time for playing the current file
static uint32_t jtag_bits_histogram[MAX_JTAG_BITS+1];   // Histogram of JTAG TDI bits sent

// Static functions

/*
 * Calculate playing rate in bytes/sec since start of file
 */

uint32_t jtagCalculateRates(time_t time_now)
{
    const uint32_t elapsed_time = (uint32_t)(time_now - start_time);

    if(elapsed_time > 0)
    {
        jtag.read_rate = jtag.bytes_read / elapsed_time;
        jtag.sent_rate = jtag.bits_sent  / elapsed_time;
    }

    return elapsed_time;
}



/*
 * Add a JTAG bit to the transmission buffer
 */

inline void jtagSetBit(int jtag_bit_value)
{
    // Calculate the byte and bit indexes

    uint32_t byte_bit_index = jtag.tx_msg.payload.length_bits % 8;
    uint32_t byte_index     = jtag.tx_msg.payload.length_bits / 8;

    // Clear the byte if necessary

    if(!byte_bit_index)
    {
        jtag.tx_msg.payload.jtag_bits[byte_index] = 0;
    }

    // Set the bit within the transmission buffer

    if(jtag_bit_value)
    {
        jtag.tx_msg.payload.jtag_bits[byte_index] |= 0x80 >> byte_bit_index;
    }

    jtag.tx_msg.payload.length_bits++;
}



/*
 * Set JTAG signals
 */

static void jtagSet(int tdi, int tms)
{
    // Do nothing if an error has occurred

    if(jtag.error)
    {
        return;
    }

    // Add JTAG bits to the transmission buffer

    jtagSetBit(tms);
    jtagSetBit(tdi);

    // If the end of the input file has been reached, or a response is requexted, 
    // or the buffer is full then add the message to the transmission queue 

    if(jtag.eof || jtag.tx_msg.rx_request ||
       jtag.tx_msg.payload.length_bits >= sizeof(jtag.tx_msg.payload.jtag_bits) * 8)
    {
        jtag.tx_msg.delay_us = 0;

        // Add the message to the transmission queue

        if(mq_send(nfipjtag_q.tx_queue, reinterpret_cast<char*>(&jtag.tx_msg), sizeof(jtag.tx_msg), 0))
        {
            perror("jtagSet(): mq_send");
            jtag.error = true;
        }

        // Record the number of TDI bits sent and reset the transmit length

        const uint32_t bits_sent = jtag.tx_msg.payload.length_bits / 2;   // TMS and TDI are sent for each clock

        jtag.bits_sent += bits_sent;
        jtag_bits_histogram[bits_sent]++;
        jtag.tx_msg.payload.length_bits = 0;
    }
}



/*
 * Get JTAG TDO signal
 */

static int jtagGetTdo(void)
{
    // Do nothing if an error has occurred

    if(jtag.error)
    {
        return -1;
    }

    // Wait to receive JTAG status

    ssize_t                size;
    struct Nfipjtag_rx_var rx_var;
    struct timespec        timeout;

    timeout.tv_sec  = 1;
    timeout.tv_nsec = 0;

    while(!jtag.error &&
          (size = mq_timedreceive(nfipjtag_q.rx_queue, reinterpret_cast<char*>(&rx_var), sizeof(rx_var), NULL, &timeout)) == -1 &&
          errno == ETIMEDOUT);

    // Check whether an error occurred

    if(jtag.error)
    {
        return -1;
    }

    if(size != sizeof(rx_var))
    {
        if(errno)
        {
            perror("jtagGetTdo(): mq_timedreceive");
        }

        return -1;
    }

    // Return TDO

    return rx_var.tdo;
}



/*
 * Callback for libxsvf to set-up the JTAG interface
 */

static int jtagXsvfSetup(struct libxsvf_host *h)
{
    return 0;
}



/*
 * Callback for libxsvf to shutdown the JTAG interface
 */

static int jtagXsvfShutdown(struct libxsvf_host *h)
{
    return 0;
}



/*
 * Callback for libxsvf to delay
 */

static void jtagXsvfUDelay(struct libxsvf_host *h, long usecs, int tms, long num_tck)
{
    // Do nothing if an error has occurred

    if(jtag.error)
    {
        return;
    }

    // Request a delay

    if(usecs)
    {
        // Request a delay and add the message to the transmission queue

        jtag.tx_msg.delay_us   = usecs;
        jtag.tx_msg.rx_request = 0;

        if(mq_send(nfipjtag_q.tx_queue, reinterpret_cast<char*>(&jtag.tx_msg), sizeof(jtag.tx_msg), 0))
        {
            perror("jtagXsvfUDelay(): mq_send");
            jtag.error = true;
            return;
        }

        // Reset the length

        jtag.tx_msg.payload.length_bits = 0;
    }

    // Toggle clock if requested

    while(num_tck--)
    {
        jtagSet(0, tms);
    }
}



/*
 * Callback for libxsvf to read a byte from the input file
 */

static int jtagXsvfGetByte(struct libxsvf_host *h)
{
    uint8_t c;
    ssize_t size;

    // Do nothing if an error has occurred

    if(jtag.error)
    {
        return -1;
    }

    // Read next byte from the input file

    if((size = read(jtag.input_fd, &c, 1)) == -1)
    {
        perror("jtagXsvfGetByte(): read");
        jtag.error = true;
        return -1;
    }

    jtag.bytes_read++;

    // Print progress at the start, every 64 kilobytes and at end-of-file

    if(quiet_mode == false && (jtag.bytes_read % 65536 == 0 || size == 0))
    {
        jtagCalculateRates(time(NULL));

        printf(" %3d%% %u KB @ %u KB/s %u b/s\r",
               (int)(((float)jtag.bytes_read / jtag.input_stat.st_size) * 100),
               jtag.bytes_read / 1024,
               jtag.read_rate / 1024,
               jtag.sent_rate);
        fflush(stdout);
    }

    // Handle end-of-file

    if(size == 0)
    {
        // Set a flag to ensure that all data is queued

        jtag.eof = true;
        return -1;
    }

    return c;
}



/*
 * Callback for libxsvf to send JTAG signals
 */

static int jtagXsvfPulseTck(struct libxsvf_host *h, int tms, int tdi, int tdo, int rmask, int sync)
{
    // Request reception of TDO when required

    jtag.tx_msg.rx_request = (tdo != -1);

    // Prepare to transmit TDI and TMS

    jtagSet(tdi == -1 ? 0 : tdi, tms);

    // Return 1 if TDO comparison is not requested

    int response = 1;

    if(jtag.tx_msg.rx_request)
    {
        // TDO response has been requested so wait for it

        const int tdo_value = jtagGetTdo();

        // Return TDO if it matches expected value, otherwise return -1

        response = (!jtag.error && tdo_value == tdo) ? tdo_value : -1;
    }

    return response;
}



/*
 * Callback for libxsvf to set TCK frequency
 */

static int jtagXsvfSetFrequency(struct libxsvf_host *h, int v)
{
    // Ignore frequency request

    return 0;
}



/*
 * Callback for libxsvf to report an error
 */

static void jtagXsvfReportError(struct libxsvf_host *h, const char *file, int line, const char *message)
{
    // Report the error if one has not already been reported elsewhere

    if(!jtag.error)
    {
        printf("\n");
        fprintf(stderr, "Error: %s\n", message);
    }
}



/*
 * Callback for libxsvf to re-allocate memory
 */

static void *jtagXsvfRealloc(struct libxsvf_host *h, void *ptr, int size, enum libxsvf_mem which)
{
    return realloc(ptr, size);
}



// External functions

int jtagProg(char *filename, enum jtag_file_type type)
{
    struct libxsvf_host xsvf = {};

    // Set libxsvf callbacks

    xsvf.getbyte       = jtagXsvfGetByte;
    xsvf.pulse_tck     = jtagXsvfPulseTck;
    xsvf.realloc       = jtagXsvfRealloc;
    xsvf.report_error  = jtagXsvfReportError;
    xsvf.set_frequency = jtagXsvfSetFrequency;
    xsvf.setup         = jtagXsvfSetup;
    xsvf.shutdown      = jtagXsvfShutdown;
    xsvf.udelay        = jtagXsvfUDelay;

    // Set file mode

    enum libxsvf_mode  xsvf_file_mode = type == JTAG_FILE_XSVF ? LIBXSVF_MODE_XSVF : LIBXSVF_MODE_SVF;

    // Open input file

    jtag.bytes_read = 0;
    jtag.eof        = false;

    if((jtag.input_fd = open(filename, O_RDONLY)) == -1)
    {
        perror("jtagProg(): Open input file");
        return -1;
    }

    // Get input file status

    if(fstat(jtag.input_fd, &jtag.input_stat))
    {
        perror("jtagProg(): fstat");
        close(jtag.input_fd);
        return -1;
    }

    jtag.file_size = jtag.input_stat.st_size;
    
    // Print start time

    start_time = time(NULL);

    printf("JTAG operation started at   %s", ctime(&start_time));

    // Perform JTAG programming

    if(libxsvf_play(&xsvf, xsvf_file_mode) < 0 || jtag.error)
    {
        const time_t   end_time     = time(NULL);
        const uint32_t elapsed_time = jtagCalculateRates(end_time);

        printf("\rJTAG operation aborted at   %sElapsed time: %u s.  At byte %u.\n",
                   ctime(&end_time),
                   elapsed_time,
                   jtag.bytes_read);

        if(debug_mode)
        {
            ssize_t size;
            char next_file_data[1025];

            size = read(jtag.input_fd, &next_file_data, sizeof(next_file_data)-1);

            if(size > 0)
            {
                next_file_data[size] = '\0';
                printf("Continuing in file:\n%s...\n", next_file_data);
            }
        }

        close(jtag.input_fd);
        return -1;
    }

    close(jtag.input_fd);

    // Wait for all transmission queue entries to be processed

    struct mq_attr mq_attr;

    do
    {
        // Get transmission queue attributes

        if(mq_getattr(nfipjtag_q.tx_queue, &mq_attr))
        {
            perror("jtagProg(): mq_get_attr");
            return -1;
        }

        // Sleep for 100 ms to allow transmission

        struct timespec sleep_time;

        sleep_time.tv_sec  = 0;
        sleep_time.tv_nsec = 100000000;

        while(nanosleep(&sleep_time, &sleep_time) && errno == EINTR);

    } while(mq_attr.mq_curmsgs); // Transmission queue is not empty

    // Print end time, elapsed time and rate

    const time_t   end_time     = time(NULL);
    const uint32_t elapsed_time = jtagCalculateRates(end_time);

    printf("\rJTAG operation completed at %sElapsed time: %u s.  Read %u KB @ %u KB/s.  Sent %u b @ %u b/s.\n",
               ctime(&end_time),
               elapsed_time,
               jtag.bytes_read / 1024,
               jtag.read_rate / 1024,
               jtag.bits_sent,
               jtag.sent_rate);

    // Dump histogram of number of jtag bits sent per transmission

    uint32_t i;

    if(quiet_mode == false)
    {
        for(i = 0 ; i <= MAX_JTAG_BITS; i++)
        {
            if(jtag_bits_histogram[i] > 0)
            {
                printf("%03u ; %u\n", i, jtag_bits_histogram[i]);
            }
        }
    }

    return 0;
}



void jtagSignalHandler(int signal)
{
    fprintf(stderr, "Received signal %d\n", signal);
    jtag.error = true;
}

// EOF
