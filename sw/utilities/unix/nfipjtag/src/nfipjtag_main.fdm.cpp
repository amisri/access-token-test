/*!
 * @file   nfipjtag_main.cpp
 * @brief  JTAG programmer for nanoFIP devices
 * @author Stephen Page
 * @author Michael Davis
 */

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <csignal>
#include <stdint.h>
#include <unistd.h>
#include <time.h>

#include <fdm.h>
#include <fip.fdm.h>
#include <nfipjtag_cycle.h>
#include <jtag.h>

// Global variables

bool     quiet_mode;
bool     debug_mode;
uint32_t ba_wait_time[2] = { 1080, 400 };
uint32_t silence_time_us = 0;

// Static functions

static void printUsage(char *program_name)
{
    fprintf(stderr, "\nUsage: %s [-i<INTERFACE>] [-b<BUS SPEED>] [-qd] [(-s | -x)] <ADDRESS> <FILE> [<FILE>...]\n"
                    "or: %s [-i<INTERFACE>] [-b<BUS SPEED>] -r<TRST_VALUE> <ADDRESS>\n\n%s",
                    program_name, program_name,
                    "            -1<WAIT1>            Wait time (us) for RX ID_DAT"
                    "            -2<WAIT2>            Wait time (us) for end of cycle, relative to WAIT1"
                    "            -t<SILENCE_TIME>     Set silence time (us) (0-272)"
                    "            -b<BUS SPEED>        Select bus speed: 0=31.25 Kb/s, 1=1000 Kb/s, 2=2500 Kb/s (default)\n"
                    "            -i<INTERFACE>        FullFIP interface number (default 0)\n"
                    "            -q                   Quiet mode - does not report progress\n"
                    "            -d                   Debug mode - report extra debug information\n"
                    "            -s | -x              Select SVF file format (default) or XSVF file format\n"
                    "            -r<TRST_VALUE>       Set TRST low (0) or high (1)\n"
                    "            <ADDRESS>            Target nanoFIP address\n"
                    "            <FILE>               SVF or XSVF file to process\n\n");
}

// Global functions

int main(int argc, char *argv[])
{
    int32_t              c;
    uint32_t             nfip_address;

    int32_t              retval          = 0; // initialised to suppress compiler warning
    uint32_t             interface       = 0;
    uint32_t             bus_speed       = 2;
    uint32_t             trst            = 2;
    char                *filename        = NULL;
    enum jtag_file_type  type            = JTAG_FILE_UNSET;

    // Process command-line options

    while((c = getopt(argc, argv, "hqdsx1:2:t:b:i:r:")) != -1)
    {
        switch(c)
        {
            case 'h':

                printUsage(argv[0]);
                return 0;

            case '1':
            case '2':
                
                {
                    errno = 0;
                    uint32_t wait_time = strtoul(optarg, NULL, 0);
                    if(errno || wait_time > 10000)
                    {
                        fprintf(stderr, "Invalid wait (0-10000).\n");
                        return 1;
                    }
                    ba_wait_time[c-'1'] = wait_time;
                }
                break;

            case 't':

                errno     = 0;
                silence_time_us = strtoul(optarg, NULL, 0);
                if(errno || silence_time_us > 272)
                {
                    fprintf(stderr, "Invalid silence time (0-272).\n");
                    return 1;
                }
                break;

            case 'b':

                errno     = 0;
                bus_speed = strtoul(optarg, NULL, 0);
                if(errno || bus_speed > 2)
                {
                    fprintf(stderr, "Invalid bus speed.\n");
                    return 1;
                }
                break;

            case 'i':

                errno     = 0;
                interface = strtol(optarg, NULL, 0);
                if(errno || interface > 255)
                {
                    fprintf(stderr, "Invalid interface number.\n");
                    return 1;
                }
                break;

            case 'r':

                errno = 0;
                trst  = strtol(optarg, NULL, 0);
                if(errno > 0 || trst > 1)
                {
                    fprintf(stderr, "TRST must be set to 0 (low) or 1 (high).\n");
                    return 1;
                }
                break;

            case 'q':

                quiet_mode = true;
                break;

            case 'd':

                debug_mode = true;
                break;

            case 's':

                if(type == JTAG_FILE_UNSET)
                {
                    type = JTAG_FILE_SVF;
                }
                else
                {
                    fprintf(stderr, "Options -x and -s are mutually exclusive.\n");
                    return 1;
                }
                break;

            case 'x':

                if(type == JTAG_FILE_UNSET)
                {
                    type = JTAG_FILE_XSVF;
                }
                else
                {
                    fprintf(stderr, "Options -s and -x are mutually exclusive.\n");
                    return 1;
                }
                break;
        }
    }

    // Print usage information if not enough arguments were supplied

    if(argc-optind < 1 || (trst > 1 && argc-optind < 2))
    {
        fprintf(stderr, "Insufficient arguments.\n");
        printUsage(argv[0]);
        return 1;
    }

    // Set file type to default if it was not set by command line args

    if(type == JTAG_FILE_UNSET) type = JTAG_FILE_SVF;

    // Get the device address

    errno = 0;
    nfip_address = strtoul(argv[optind++], NULL, 0);
    if(errno > 0 || nfip_address == 0 || nfip_address > 255)
    {
        fprintf(stderr, "Invalid address.\n");
        return 1;
    }

    // Update FDM hard and soft config parameters

    fipFdmSetConfigHard(interface);

    switch(bus_speed)
    {
        case 0: // 31.5 kbps

            fipFdmSetConfigSoft(WORLD_FIP_31, 0, 0, 100);
            break;

        case 1: // 1 Mbps

            fipFdmSetConfigSoft(WORLD_FIP_1000, 0, 0, 62);
            break;

        case 2: // 2.5 Mbps

            if(silence_time_us == 0)
            {
                fipFdmSetConfigSoft(WORLD_FIP_2500, 0, 0, 0);
            }
            else
            {
                uint32_t st = (silence_time_us - 17)/4;

                if(debug_mode)
                {
                    printf("SILENCE_TIME: %u us -> %u\n",silence_time_us, st);
                }

                fipFdmSetConfigSoft(FDM_OTHER_2500, 0, st, 50);
            }
            break;
    }

    // Start WorldFIP interface

    if(fipFdmStartInterface() != 0)
    {
        fprintf(stderr, "Fatal error: Failed to start WorldFIP interface (error %d)\n", errno);
        nfipjtagCycleCleanUp();
        return 1;
    }

    // Set/reset TRST

    printf("Setting TRST_%s in interface %d, device %d\n",
        trst == 0 ? "LOW" : "HIGH",
        interface, nfip_address);

    switch(trst)
    {
        case 0:
            return nfipjtagCycleSetTRST(nfip_address, TRST_LOW);
        case 1:
            return nfipjtagCycleSetTRST(nfip_address, TRST_HIGH);
        default:
            if(nfipjtagCycleSetTRST(nfip_address, TRST_HIGH) != 0) return 1;
    }

    // Catch signals to allow clean-up before exiting

    signal(SIGHUP,  jtagSignalHandler);
    signal(SIGINT,  jtagSignalHandler);
    signal(SIGQUIT, jtagSignalHandler);
    signal(SIGTERM, jtagSignalHandler);

    // Reconfigure the BA macrocycle program and set up queues

    if(nfipjtagCycleInit() != 0)
    {
        fprintf(stderr, "Fatal error: Failed to initialise WorldFIP Bus Arbitrator program (error %d)\n", errno);
        nfipjtagCycleCleanUp();
        return 1;
    }

    nfipjtagCycleStartProtocol(nfip_address, bus_speed);

    // Process each file in turn

    for( ; optind < argc; ++optind)
    {
        filename = argv[optind];

        printf("Programming interface %d (%d Kb/s), device %d: playing %s (%s format)\n",
               interface,
               bus_speed == 0 ? 31 : bus_speed == 1 ? 1000 : 2500,
               nfip_address, filename,
               type == JTAG_FILE_SVF ? "SVF" : "XSVF");

        // Perform JTAG programming

        retval = jtagProg(filename, type);

        if(retval != 0) break;
    }

    // Programming finished, set TRST low

    fipFdmStopProtocol();

    printf("Setting TRST_LOW in interface %d, device %d", interface, nfip_address);
    if(nfipjtagCycleSetTRST(nfip_address, TRST_LOW) != 0)
    {
        printf("...FAILED.\n");
    }
    printf("\n");

    // Clean up

    nfipjtagCycleCleanUp();

    return retval;
}

// EOF
