/*!
 *  @file      svec_pops_acq.c
 *  @brief     Application to acquire the data from the SVEC POPS card
 *
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <encore/libencore.h>

int main(int argc, char **argv)
{
    int reg_id;
    uint32_t value;
    encore_handle h;
    struct encore_irq irq;

    h = encore_open("PAL", 0);

    if (h == NULL)
    {
        fprintf(stderr,"Failed to open PAL driver");
        exit(1);
    }

    // to be changed witht he values of the real data

    reg_id = encore_reg_id(h, "PAL2DRV_Command");
    if (reg_id < 0)
    {
        fprintf(stderr,"Failed to retrieve register PAL2DRV_Command");
        exit(1);
    }

    if (encore_get_register(h, reg_id, &value) < 0)
    {
        fprintf(stderr,"Failed to read register PAL2DRV_Command");
        exit(1);
    }

    printf("PAL2DRV_Command = 0x%08x\n", value);

    while(encore_wait_irq(h, &irq) < 0)
    {
        printf("Interruption received from PAL with value = 0x%08x\n", irq.irq_value);
    }

    return 0;
}
