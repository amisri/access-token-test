/*
 *  Filename: ctrxtest.c
 *
 *  Purpose:  Configure and test certain events of the CTR card
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <math.h>

#include <err/err.h>
#include <tgm/tgm.h>
#include <tim/TimLib.h>

// Flag to select whether telegrams should be read from driver or get_tgm_tim

extern int TimLibClient;

void print_usage(char **argv)
{
  fprintf(stderr,"Usage: %s [options]\n"
			"      -h                      This help\n"
			"      -?                      This help\n"
			"      -m <machine>            Timing source (default LHC)\n"
			"      -n <number>             Number of events to be received (default 10)\n"
            "      -e <LABEL:CODE:CLASS>   Event to subscribe to (default Millisecond:4096:H):\n"
            "                                    CODE is an uint32_t event code\n"
            "                                    CLASS is the event class either (C: TimLibClassCTIM, H: TimLibClassHARDWARE, P: TimLibClassPTIM)\n"
            "      -a <cpu>                Sets the cpu affinity (default not set)\n"
            "      -p <priority>           Sets thread priority"
			, argv[0]
	);
  fprintf(stderr,"\nConfigure and test certain functions of the CTR card.\n");
	

}


long timediff(TimLibTime *end, TimLibTime *start)
{
  long usec;
  usec =  (end->Second - start->Second)*1000000;
  usec += (end->Nano - start->Nano)/1000;
  return usec;
}

long ctri_latency(TimLibTime *evt_time)
{
  long usec;
  
  usec =  evt_time->Second*1000000;
  usec += evt_time->Nano/1000;
  
  usec = usec - (usec/1000)*1000;

  return usec;
}

// Retrieve the int for a given string

int strtoint(char *ch_str, int *ch)
{
	char * endptr;

	// Check that the option argument was specified

	if (!ch_str)
	{
	  fprintf(stderr, "ERROR: Missing string\n");
	  return -1;
	}

	// Convert the string to number

	errno = 0;
	*ch = strtol(ch_str,&endptr,0);

	// Check that suceeded

	if (!errno && *ch_str != '\0' && *endptr == '\0')
	{
	  return 0;
	}
	else
	{
	  return -1;
	}

}

// Function to use as error handler for TimLib to suppress printing of errors and prevent fatal errors

static int errorHandler(void)
{
    return(0);
}

int main(int argc, char **argv)
{
    TgmName             tgm_name;
    TgmMachine          tgm_machine = TgmMACHINE_NONE;
    TimLibError         timlib_error;
    TimLibClass         sub_class[1024],evt_class;
    uint32_t            nsubscriptions = 0;
    char                sub_class_c;
    TimLibTime          evt_time,wait_time;
    uint32_t            sub_code[1024];
    unsigned long       equip,payload;
    int                 nevents = 10;
    int                 i,c,e;
    int                 fd;
    int                 cpu;
    cpu_set_t           mask;
    struct sched_param  sched_param;
    int                 priority;

    // Parse input parameters

    while ((c = getopt(argc, argv, "?hm:n:e:a:p:")) != -1)
    {
    	switch (c)
    	{
    		case 'm':
    		    strncpy(tgm_name, optarg, sizeof(tgm_name));
    		    tgm_name[sizeof(tgm_name) - 1] = '\0';
    		    if ((tgm_machine = TgmGetMachineId(tgm_name)) == TgmMACHINE_NONE)
    		    {
    		        fprintf(stderr, "ERROR: Failed to get telegram machine ID for the machine \"%s\"\n",tgm_name);
    		        exit(1);
    		    }
    		    break;
    		case 'n':
    		    if ( strtoint(optarg,&nevents) < 0)
    		    {
                    fprintf(stderr, "ERROR: Number of events should be a number bigger than 0, but founf '%s'\n",optarg);
                    exit(1);

    		    }
    		    break;
    		case 'e':
    		    if ((i = sscanf(optarg, "%u:%c", &sub_code[nsubscriptions],&sub_class_c)) != 2)
    		    {
    		        fprintf(stderr,"ERROR: Syntax error in event identifier \"%s\" (should be CODE:CLASS)",optarg);
    		        exit(1);
    		    }

                switch(sub_class_c)
                {
                    case 'C':
                        sub_class[nsubscriptions] = TimLibClassCTIM;
                        break;

                    case 'H':
                        sub_class[nsubscriptions] = TimLibClassHARDWARE;
                        break;

                    case 'P':
                        sub_class[nsubscriptions] = TimLibClassPTIM;
                        break;

                    default:
                        fprintf(stderr, "ERROR: Syntax error in event class format \"%c\" (should be C, H, or P)",
                                sub_class_c);
                        exit(1);
                }
                nsubscriptions++;

                break;
            case 'a':
                if ( strtoint(optarg,&cpu) < 0 || cpu < 0)
                {
                    fprintf(stderr, "ERROR: CPU affinity should be a numbbr bigger than 0\n");
                    exit(1);
                }
                CPU_SET(cpu, &mask);
                if ((e = sched_setaffinity(0, sizeof(mask), &mask)) < 0)
                {
                    fprintf(stderr, "ERROR: setaffinity(%d) failed: %s\n",cpu,strerror(errno));
                    exit(1);
                }
                break;
            case 'p':
                if ( strtoint(optarg,&priority) < 0 || priority < 0)
                {
                    fprintf(stderr, "ERROR: Thread priority should be a number bigger than 0\n");
                    exit(1);
                }

                sched_param.sched_priority = priority;
                sched_setscheduler(0, SCHED_FIFO, &sched_param);

                if ((e = sched_setscheduler(0, SCHED_FIFO, &sched_param)) < 0)
                {
                    fprintf(stderr, "ERROR: sched_setscheduler(%d) failed: %s\n",priority,strerror(errno));
                    exit(1);
                }
                break;
    		case '?':
    		case 'h':
    			print_usage(argv);
    			exit(1);
    			break;
    	}
    }

    if (nsubscriptions == 0)
    {
        fprintf(stderr,"WARNING: Subscribing to default event (Milliseconf task 4096:H)\n");
        sub_class[0] = TimLibClassHARDWARE;
        sub_code[0]  = 4096;
        nsubscriptions++;
    }

    // LHC machine by default

    if (tgm_machine == TgmMACHINE_NONE)
    {

        fprintf(stderr, "WARNING: Setting default machine as \"LHC\"\n");

        strncpy(tgm_name, "LHC", sizeof(tgm_name));
        tgm_name[sizeof(tgm_name) - 1] = '\0';

        if ((tgm_machine = TgmGetMachineId(tgm_name)) == TgmMACHINE_NONE)
        {
            fprintf(stderr, "ERROR: Failed to get telegram machine ID for the machine \"LHC\"\n");
            exit(1);
        }
    }

    // Initialize timlib

    if ((timlib_error = TimLibInitialize(TimLibDevice_CTR)))
    {
        fprintf(stderr, "ERROR: Failed to initialise timlib: %s\n",TimLibErrorToString(timlib_error));
        exit(1);
    }

    // Set a timelib error handler

    ErrSetHandler((ErrHandler)errorHandler);

    // Set flag to cause telegrams to be read from the driver rather than get_tgm_tim

    TimLibClient = 1;

    // Enable queuing and disable timeouts

    if((timlib_error = TimLibQueue(0, 0)))
    {
        fprintf(stderr, "ERROR: Failed to set timlib queuing and timeouts: %s\n",TimLibErrorToString(timlib_error));
        exit(1);
    }

    // Subscribe to timing events

    for(i=0; i != nsubscriptions; i++)
    {
        if((timlib_error = TimLibConnect(sub_class[i],
            sub_code[i],
            1)))
        {
            fprintf(stderr, "ERROR: Subscription to timing event %c%u failed: %s\n",
                sub_class[i],
                sub_code[i],
                TimLibErrorToString(timlib_error));
            exit(1);
        }
    }

    // Print the driver latency output if we listen just the millisecond task

    if (nsubscriptions == 1 &&
        sub_class[0] == TimLibClassHARDWARE &&
        sub_code[0]  == 4096)
    {
        fprintf(stdout,"Event, payload, event timestamp [sec], cycle event time [ms], wait timestamp [sec], ctri latency [us], timlib latency [us], total latency [us]\n");
    }
    else
    {
        fprintf(stdout,"Event, payload, event timestamp [sec], cycle event time [ms], wait timestamp [sec], timlib latency [us]\n");
    }

    // Listen events


    for(i=0; i != nevents; ++i)
    {
        // Wait for a timing event

        if((timlib_error = TimLibWait(  &evt_class, // Class of interrupt
                                        &equip,     // PTIM, CTIM or hardware mask
                                        NULL,       // Ptim line number 1..n or 0
                                        NULL,       // Hardware source of interrupt
                                        &evt_time,  // Time of interrupt/output
                                        NULL,       // Time of counters load
                                        NULL,       // Time of counters start
                                        NULL,       // CTIM trigger equipment ID
                                        &payload,   // Payload of trigger event
                                        NULL,       // Module that interrupted
                                        NULL,       // Number of missed interrupts
                                        NULL,       // Remaining interrupts on queue
                                        NULL)       // Corresponding TgmMachine
          ))
            {
                fprintf(stderr, "ERROR: TimlibWait: %s\n",TimLibErrorToString(timlib_error));
                exit(1);
            }

        // Read time

        if((timlib_error = TimLibGetTime(1, &wait_time)))
        {
            fprintf(stderr, "ERROR: TimLibGetTime: %s\n",TimLibErrorToString(timlib_error));
            exit(1);
        }

        if (nsubscriptions == 1 &&
            sub_class[0] == TimLibClassHARDWARE &&
            sub_code[0]  == 4096)
        {
            // Print the driver latency if we are listennig to the millisecond task

            fprintf(stdout,"%c%lu, 0x%04lX, %lu.%06lu, %lums, %lu.%06lu, %ld, %ld,%ld\n",
                evt_class == TimLibClassHARDWARE ? 'H' : evt_class == TimLibClassCTIM ? 'C' : 'P',
                equip,
                payload,
                evt_time.Second,
                evt_time.Nano / 1000,
                evt_time.CTrain,
                wait_time.Second,
                wait_time.Nano / 1000,
                ctri_latency(&evt_time),
                timediff(&wait_time,&evt_time),
                ctri_latency(&evt_time)+timediff(&wait_time,&evt_time)
            );
        }
        else
        {
            fprintf(stdout,"%c%lu, 0x%04lX, %lu.%06lu, %lums, %lu.%06lu, %ld\n",
                evt_class == TimLibClassHARDWARE ? 'H' : evt_class == TimLibClassCTIM ? 'C' : 'P',
                equip,
                payload,
                evt_time.Second,
                evt_time.Nano / 1000,
                evt_time.CTrain,
                wait_time.Second,
                wait_time.Nano / 1000,
                timediff(&wait_time,&evt_time)
            );

        }
    }

    // Close timlib

    if((timlib_error = TimLibGetHandle(&fd)))
    {
        fprintf(stderr, "ERROR: Failed to get timlib file descriptor: %s\n",TimLibErrorToString(timlib_error));
        exit(1);
    } else
    {
        close(fd);
    }

    return(0);
}

// EOF
