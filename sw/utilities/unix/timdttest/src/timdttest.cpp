/*
 *  Filename: timdttest.c
 *
 *  Purpose:  Configure and test certain events of the CTR card
 */

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <math.h>

#include <exception>
#include <string>

#include <timdt-lib-cpp/Timing.h>

// Flag to select whether telegrams should be read from driver or get_tgm_tim

void print_usage(char **argv)
{
    fprintf(stderr,"Usage: %s [options]\n"
        "      -h                      This help\n"
        "      -?                      This help\n"
        "      -m <machine>            Timing source (default LHC)\n"
        "      -n <number>             Number of events to be received (default 10)\n"
        "      -t <timeout_ms>         Timeout in milliseconds (default 60000)\n"
        "      -s <LABEL>              Comma separated list of events\n"
        "      -a <cpu>                Sets the cpu affinity (default not set)\n"
        "      -p <priority>           Sets thread priority"
        , argv[0]
    );
    fprintf(stderr,"\nConfigure and test certain functions of the CTR card.\n");


}


uint64_t timediff(tTimingTime *end, tTimingTime *start)
{
    uint64_t nsec;
    nsec =  (end->time.tv_sec - start->time.tv_sec)*1000000000;
    nsec += (end->time.tv_nsec - start->time.tv_nsec);
    return nsec;
}

uint64_t distance_to_ms(tTimingTime *evt_time)
{
    long nsec;

    nsec =  evt_time->time.tv_sec*1000000000;
    nsec += evt_time->time.tv_nsec;

    nsec = nsec - (nsec/1000000)*1000000;

    return nsec;
}

// Retrieve the int for a given string

int strtoint(char *ch_str, int *ch)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing string\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *ch = strtol(ch_str,&endptr,0);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        return -1;
    }

}

void list_events(const char* sDomain)
{
    Timing::Domain*         tdomain       = NULL;

    
    try
    {
        tdomain = new Timing::Domain(sDomain);

        // Retrieve event list

        std::vector<Timing::EventDescriptor*>   evt_desc_list = tdomain->getEventDescList();

        std::vector<Timing::EventDescriptor*>::iterator i;
        unsigned count_events;
        for(i = evt_desc_list.begin(), count_events = 1; i != evt_desc_list.end(); ++i, ++count_events)
        {
            Timing::EventDescriptor* evt = *i;

            // Print event

            printf ("%u/%zu %s (%s)\n",
                count_events,
                evt_desc_list.size(),
                evt->getName().c_str(),
                evt->getDomain()->getName().c_str());

            // Retrieve field list

            std::vector<Timing::Field*> field_list = evt->getFieldList();
            std::vector<Timing::Field*>::iterator j;
            unsigned count_fields;
            for(j = field_list.begin(), count_fields = 1; j != field_list.end(); ++j, ++count_fields)
            {
                Timing::Field* field = *j;

                // Print fields

                printf ("\t%u/%zu %s (enum #%d)\n",
                    count_fields,
                    field_list.size(),
                    field->getName().c_str(),
                    field->getType());

            }
        }
    }
    catch(std::exception& e)
    {
        fprintf(stderr,"ERROR: Timing exception: %s\n", e.what());
        exit(1);
    }

    delete(tdomain);
}
void subscribe(Timing::ConnectionContext* tconnection, char* event_list, const char* sDomain,int timeout_ms)
{
    char*                     token         = NULL;
    const char*               search        = ",";

    // tokenize the subscription list

    token = strtok(event_list, search);

    while( token != NULL ) 
    {
        try
        {
            tconnection->connect(std::string(token));
        }
        catch(std::exception& e)
        {
            printf("ERROR: Failed to connect(%s): %s", token, e.what());
            exit(1);
        }

        token = strtok(NULL, search);
    }
}

void wait_events(Timing::ConnectionContext* tconnection, int n)
{
    int i;
    Timing::EventValue        value;
    tTimingTime               start, evt_time;


    printf("Event, HW Time [s], UTC time [s], Wait Latency relative to HW IRQ [ns]\n");

    // Wait for events

    for(i=0; n == 0 || (i != n) ; ++i)
    {
        // Wait

        tconnection->wait(&value);

        start    = tconnection->getUTCTime();
        evt_time = value.getHwTimestamp();

        printf("%s, %lu.%06lu, %lu.%06lu, %" PRIu64 "\n",
            value.getName().c_str(),
            evt_time.time.tv_sec,
            evt_time.time.tv_nsec/1000,
            start.time.tv_sec,
            start.time.tv_nsec/1000,
            timediff(&start, &evt_time));
    }

}

int main(int argc, char **argv)
{
    const char*               sDomain = NULL;
    char                      sEventList[4096];
    int                       c,e;
    int                       nevents = 10;
    int                       listevents = 0;
    int                       cpu;
    cpu_set_t                 mask;
    struct sched_param        sched_param;
    int                       priority;
    int                       timeout_ms = 0;


    // Parse input parameters

    while ((c = getopt(argc, argv, "?hm:ln:t:a:p:s:")) != -1)
    {
        switch (c)
        {
            case 'm':
                sDomain = optarg;
                break;
            case 'n':
                if ( strtoint(optarg,&nevents) < 0)
                {
                    fprintf(stderr, "ERROR: Number of events should be a number bigger than 0, but found '%s'\n",optarg);
                    exit(1);
                }

                if (nevents<0)
                {
                    fprintf(stderr, "ERROR: Number of events should be a number bigger than 0, but found '%s'\n",optarg);
                    exit(1);
                }
                break;
            case 't':
                if ( strtoint(optarg,&timeout_ms) < 0)
                {
                    fprintf(stderr, "ERROR: Timeout should be a number bigger than 0, but found '%s'\n",optarg);
                    exit(1);
                }

                if (timeout_ms<0)
                {
                    fprintf(stderr, "ERROR: Timeout should be a number bigger than 0, but found '%s'\n",optarg);
                    exit(1);
                }
                break;
            case 's':
                if(strlen(optarg) > sizeof(sEventList) - 1)
                {
                    fprintf(stderr, "ERROR: Subscription event list is bigger than allowed (strlen > %d)\n",strlen(optarg));
                    exit(1);                    
                }
                strncpy(sEventList, optarg, sizeof(sEventList));
                sEventList[strlen(optarg)] = '\0';

                break;
            case 'l':
                listevents = 1;

                break;
            case 'a':
                if ( strtoint(optarg,&cpu) < 0 || cpu < 0)
                {
                    fprintf(stderr, "ERROR: CPU affinity should be a numbbr bigger than 0\n");
                    exit(1);
                }
                CPU_SET(cpu, &mask);
                if ((e = sched_setaffinity(0, sizeof(mask), &mask)) < 0)
                {
                    fprintf(stderr, "ERROR: sched_setaffinity(%d) failed: %s\n",cpu,strerror(errno));
                    exit(1);
                }
                break;
            case 'p':
                if ( strtoint(optarg,&priority) < 0 || priority < 0)
                {
                    fprintf(stderr, "ERROR: Thread priority should be a number bigger than 0\n");
                    exit(1);
                }

                sched_param.sched_priority = priority;
                sched_setscheduler(0, SCHED_FIFO, &sched_param);

                if ((e = sched_setscheduler(0, SCHED_FIFO, &sched_param)) < 0)
                {
                    fprintf(stderr, "ERROR: sched_setscheduler(%d) failed: %s\n",priority,strerror(errno));
                    exit(1);
                }
                break;
            case '?':
            case 'h':
                print_usage(argv);
                exit(1);
                break;
        }
    }


    // List events

    if (listevents)
    {
        list_events(sDomain);
        exit(0);
    }

    // Subscribe to event

    if (sEventList)
    {
        tTimingError                  terr;
        Timing::ConnectionContext     tconnection;

        try
        {
            // Set Queue

            tconnection.setQueue(true);

            // set timeout

            tconnection.setTimeOut(timeout_ms);

        }
        catch (std::exception& e)
        {
            fprintf(stderr,"ERROR: Timing exception: %s\n", e.what());
            exit(1);
        }

        subscribe(&tconnection, sEventList,sDomain,timeout_ms);

        wait_events(&tconnection,nevents);

    }

    // Subscription loop

    return(0);
}

// EOF
