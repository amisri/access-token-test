#!/bin/bash
#
# Filename: email.sh
#
# Purpose: Send an email and update the wiki when a new version is installed

set -e

if [ $# -ne 4 ]; then
    echo "Usage: $0 <email template> <package name> <version> <area>"
    exit 1
fi

export package="$2"
export version="$3"
export area="$4"
export branch=`git rev-parse --abbrev-ref HEAD`

# Send notification e-mail

email_template=`envsubst < $1`

ssh "cs-ccr-teepc2" "mail -s '$package version $version installed on $area' 'converter-controls-software-updates@cern.ch'" <<EOF
$email_template
EOF

# EOF
