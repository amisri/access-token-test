/*
 *  Filename: pertu_csv.c
 *
 *  Purpose:  Convert pertu files into csv files
 *
 *  Author:   Stephen Page
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define PERTU_CSV

#include "pertu.h"

int main(int argc, char *argv[])
{
    uint32_t            i;
    struct pertu_data   pertu_data;
    uint8_t             *sample;
    uint32_t            sample_idx;

    for(i = 1 ; i < argc ; i++)
    {
        if(pertu_read_file(argv[i], &pertu_data))
        {
            exit(1);
        }

        // Print headings

        printf("Time");
        for(i = 0 ; i < pertu_data.num_signals ; i++)
        {
            printf(",%s", pertu_data.signal_names[i]);
        }
        printf("\n");

        // Print each sample

        for(sample_idx = 0 ; sample_idx < pertu_data.num_samples ; sample_idx++)
        {
            // Print timestamp

            printf("%ld.%03ld",
                   (long)pertu_data.start_time.tv_sec,
                   (long)pertu_data.start_time.tv_usec / 1000);

            // Print sample for each signal

            for(i = 0 ; i < pertu_data.num_signals ; i++)
            {
                sample = &pertu_data.signal_data[PERTU_SAMPLE_SIZE * ((i * pertu_data.num_samples) + sample_idx)];

                switch(pertu_data.signal_types[i])
                {
                    case PERTU_FLOAT:
                        printf(",%.07e", *(float *)sample);
                        break;

                    case PERTU_BOOL:
                    case PERTU_LONG:
                        printf(",%ld", *(long *)sample);
                        break;
                }
            }
            printf("\n");

            // Set time for next sample

            pertu_data.start_time.tv_usec += pertu_data.period_ms * 1000;

            // Check whether a second boundary has been passed

            if(pertu_data.start_time.tv_usec >= 1000000)
            {
                pertu_data.start_time.tv_sec++;
                pertu_data.start_time.tv_usec -= 1000000;
            }
        }

        pertu_free_data(&pertu_data);
    }

    exit(0);
}

// EOF
