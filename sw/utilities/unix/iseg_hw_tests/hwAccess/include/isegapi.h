#ifndef ISEGAPI_H
#define ISEGAPI_H

#ifdef __cplusplus
    extern "C" {
#endif

#include "isegcommon.h"

unsigned int iseg_getVersion(void);
const char *iseg_getVersionString(void);

IsegResult iseg_connect(const char *name, const char *interface, void *reserved);
IsegResult iseg_disconnect(const char *name);
IsegResult iseg_setItem(const char *name, const char *object, const char *value);
IsegItem iseg_getItem(const char *name, const char *object);
IsegItemProperty iseg_getItemProperty(const char *name, const char *object);

#ifdef __cplusplus
    }
#endif

#endif // ISEGAPI_H
