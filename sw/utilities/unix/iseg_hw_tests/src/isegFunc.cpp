/*!
 *  @file     isegFunc.cpp
 *  @defgroup iseg_hw_tests
 *  @brief    isegHAL testing program
 *
 *  Functions to prepare the parameters and call the ISEG API.
 */


// Includes

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include "isegapi.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include "../include/isegFunc.h"
using namespace std;

extern int MAX_PROPERTIES;


// Internal function declarations

/*!
 * Print program usage
 */

void PrintUsage(char **argv)
{
  fprintf(stderr,"Usage: %s [options]\n"
            "      -h                           This help\n"
            "      -?                           This help\n"
            "      -c                           Connect to the crate \n"
            "      -d                           Disconnect from the crate \n"
            "      -n  <ConnectionName>         Connection name \n"
            "      -a  <Address>                IP address of the crate \n"
            "      -u  <User>                   User name \n"
            "      -x  <Password>               Password \n"
            "      -p  <PropertyName>           Property name: line.module.channel.item e.g. 0.4.1.VoltageMeasure    \n"
            "                                   Multiple properties separated by , : property1,property2,property3   \n"
            "      -v   <SetValue>              Desired set value for the property. (required for -w and -z)         \n"
            "                                   If multiple properties -> multiple values: value1,value2,value3      \n"
            "      -r                           Read (get) a property value. Property name: line.module.channel.item \n"
            "      -w                           Write (set) a property value.                                        \n"
            "      -z                           Write-poll a property value.                                         \n"
            "      -s                           Read-poll a property value.                                          \n"
            "      -t  <PollTime>               Poll time in ms.                                                     \n"

            , argv[0]
    );
  fprintf(stderr,"\nTest functions of the isegHAL remote service.\n");
}



/*!
 * Connection to the crate system using API ISEGHAL
 */

int ConnectCrate(const char *connectionName, const char *interface)
{
    fprintf(stdout,"%s INFO %s %s CONNECT \n",CurrentDateTime(),connectionName, interface);
    IsegResult result = iseg_connect(connectionName,interface,NULL);

    // Check the result

    switch(result)
    {
        case ISEG_ERROR                : fprintf(stderr,"%s ERROR %s CONNECT failed: ISEG_ERROR \n",CurrentDateTime(),connectionName); return EXIT_FAILURE;               break;
        case ISEG_WRONG_SESSION_NAME   : fprintf(stderr,"%s ERROR %s CONNECT failed: ISEG_WRONG_SESSION_NAME \n",CurrentDateTime(),connectionName); return EXIT_FAILURE;  break;
        case ISEG_WRONG_USER           : fprintf(stderr,"%s ERROR %s CONNECT failed: ISEG_WRONG_USER \n",CurrentDateTime(),connectionName); return EXIT_FAILURE;          break;
        case ISEG_WRONG_PASSWORD       : fprintf(stderr,"%s ERROR %s CONNECT failed: ISEG_WRONG_PASSWORD \n",CurrentDateTime(),connectionName); return EXIT_FAILURE;      break;
        case ISEG_NOT_AUTHORIZED       : fprintf(stderr,"%s ERROR %s CONNECT failed: ISEG_NOT_AUTHORIZED \n",CurrentDateTime(),connectionName); return EXIT_FAILURE;      break;
        default : break;
    }

    return EXIT_SUCCESS;
}


/*!
 * Disconnection to the crate system using API ISEGHAL
 */

int DisconnectCrate(const char *connectionName)
{
    fprintf(stdout,"%s INFO %s DISCONNECT \n",CurrentDateTime(),connectionName);
    IsegResult result = iseg_disconnect(connectionName);
  
    // Check the result of the disconnection
    if (result != ISEG_OK)
    {
        fprintf(stderr,"%s ERROR %s DISCONNECT failed: ISEG_ERROR \n",CurrentDateTime(),connectionName);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



/*!
 * Read a property value using the API ISEGHAL
 */

int ReadPropertyValue(const char *connectionName, const char *propertyName)
{
    // getItem call to API
    IsegItem resultValue =  iseg_getItem(connectionName, propertyName);

    // Check the result of the connection

    if (!strcmp(resultValue.value,""))
    {
        fprintf(stderr,"%s ERROR %s GET %s failed: ISEG ERROR unknown \n", CurrentDateTime(), connectionName,propertyName);
        return EXIT_FAILURE;
    }

    if (strcmp(resultValue.quality,ISEG_ITEM_QUALITY_OK))
    {
        fprintf(stdout,"%s, %s, %s \n", CurrentDateTime(), propertyName, resultValue.value);
        return EXIT_SUCCESS;
    }

    if (strcmp(resultValue.quality,ISEG_ITEM_QUALITY_INVALID))
    {
        fprintf(stderr,"%s ERROR %s GET %s failed: ISEG_ITEM_QUALITY_INVALID \n", CurrentDateTime(), connectionName, propertyName);
        return EXIT_FAILURE;
    }

    if (strcmp(resultValue.quality,ISEG_ITEM_QUALITY_INITIALIZE))
    {
        fprintf(stderr,"%s WARNING %s GET %s RESULT Value = %s Quality = INITIALIZE \n", CurrentDateTime(), connectionName, propertyName, resultValue.value);
        return EXIT_SUCCESS; // don't stop the program.
    }

    if (strcmp(resultValue.quality,ISEG_ITEM_QUALITY_COMMUNICATION_BAD))
    {
        fprintf(stderr,"%s ERROR %s GET %s failed: ISEG_ITEM_QUALITY_COMMUNICATION_BAD code %s \n", CurrentDateTime(), connectionName, propertyName, resultValue.quality);
        return EXIT_FAILURE;
    }

    if (strcmp(resultValue.quality,ISEG_ITEM_QUALITY_ERROR))
    {
        fprintf(stderr,"%s ERROR %s GET %s failed: ISEG_ITEM_QUALITY_ERROR code \n", CurrentDateTime(), connectionName, propertyName);
        return EXIT_FAILURE;
    }

    else
    {
        fprintf(stderr,"%s ERROR %s GET %s failed: ISEG ERROR unknown \n", CurrentDateTime(), connectionName,propertyName);
    }



    return EXIT_SUCCESS;


}





/*!
 * Write a property value using the API ISEGHAL
 */

int WritePropertyValue (const char *connectionName, const char *propertyName, const char *value)
{

    // Call the API function iseg_setItem
    IsegResult result =  iseg_setItem(connectionName, propertyName, value);

    // Check the result

    if (result == ISEG_OK)
    {
        return EXIT_SUCCESS;
    }

    else
    {
        switch (result)
           {
               case ISEG_ERROR:                fprintf(stderr,"%s ERROR %s SET %s = %s failed: ISEG_ERROR  \n",CurrentDateTime(),connectionName,propertyName,value);              return EXIT_FAILURE;    break;
               case ISEG_WRONG_SESSION_NAME:   fprintf(stderr,"%s ERROR %s SET %s = %s failed: ISEG_WRONG_SESSION_NAME \n",CurrentDateTime(),connectionName,propertyName,value);  return EXIT_FAILURE;    break;
               case ISEG_WRONG_USER:           fprintf(stderr,"%s ERROR %s SET %s = %s failed: ISEG_WRONG_USER \n",CurrentDateTime(),connectionName,propertyName,value);          return EXIT_FAILURE;    break;
               case ISEG_WRONG_PASSWORD:       fprintf(stderr,"%s ERROR %s SET %s = %s failed: ISEG_WRONG_PASSWORD \n",CurrentDateTime(),connectionName,propertyName,value);      return EXIT_FAILURE;    break;
               case ISEG_NOT_AUTHORIZED:       fprintf(stderr,"%s ERROR %s SET %s = %s failed: ISEG_NOT_AUTHORIZED \n",CurrentDateTime(),connectionName,propertyName,value);      return EXIT_FAILURE;    break;
               default:                        fprintf(stderr,"%s ERROR %s SET %s = %s failed: Unknown error \n",CurrentDateTime(),connectionName,propertyName,value);            return EXIT_FAILURE;    break;
           }
        return EXIT_FAILURE;
    }
}



/*!
 * Format current time (calculated as an offset in current day) in this form:
 * "hh:mm:ss.SSS" (where "SSS" are milliseconds)
 */

char *CurrentDateTime()
{
    // Get current time from the clock, using microseconds resolution
    const boost::posix_time::ptime now =
        boost::posix_time::microsec_clock::local_time();

    // Get the time offset in current day
    const boost::posix_time::time_duration td = now.time_of_day();

    //
    // Extract hours, minutes, seconds and milliseconds.
    //
    // Since there is no direct accessor ".milliseconds()",
    // milliseconds are computed _by difference_ between total milliseconds
    // (for which there is an accessor), and the hours/minutes/seconds
    // values previously fetched.

    const long hours        = td.hours();
    const long minutes      = td.minutes();
    const long seconds      = td.seconds();
    const long milliseconds = td.total_milliseconds() -
                              ((hours * 3600 + minutes * 60 + seconds) * 1000);


    // Format like this:
    //
    //      hh:mm:ss.SSS
    //
    // e.g. 02:15:40:321
    //
    //      ^          ^
    //      |          |
    //      123456789*12
    //      ---------10-     --> 12 chars + \0 --> 13 chars should suffice

    static char buf[40];
    sprintf(buf, "%02ld:%02ld:%02ld.%03ld",
        hours, minutes, seconds, milliseconds);

    return buf;
}


/*
// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
char *CurrentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    static char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}
*/


