/*!
 *  @file     ISEG_HW_tests.cpp
 *  @defgroup iseg_hw_tests
 *  @brief    isegHAL testing program
 *
 *  Testing program to connect/disconnect, read/write from an ISEG ECH44A using
 *  the isegHAL remote service.
 */


// Includes

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include "../include/isegFunc.h"
using namespace std;

//Constant values
extern const int  MAX_PROPERTIES = 20;

// Static functions

static void getInterfaceName(char *crateAddress, char *userName, char *password, char *interfaceName);
static void ReadProperties(char *propertyNames, const char *connectionName);
static void WriteProperties(char *propertyNames, char *propertySetValues, const char *connectionName);



/*!
 * Main
 */

int main(int argc, char **argv) {

    //Variables

    int pollTime = 50; //default value
    bool connect = false;
    bool disconnect = false;
    bool readValue = false;
    bool writeValue = false;
    bool valueFlag = false;
    bool pollReadValue = false;
    bool pollWriteValue = false;
    bool userNameFlag = false;
    bool passwordFlag = false;
    bool connectionNameFlag = false;
    bool crateAddressFlag = false;
    char *propertyNames = NULL;
    char *propertySetValues = NULL;
    char crateAddress[50];
    char userName[50];
    char password[50];
    char connectionName[50];
    char interfaceName[100];


    // Parse input parameters

    int a;
    while ((a = getopt(argc, argv, "?hcdn:a:u:x:p:v:rwzst:")) != -1)
    {
        switch (a)
        {
            case 'c':
                connect = true;
                break;
            case 'n':
                connectionNameFlag = true;
                strcpy(connectionName,optarg);
                break;
            case 'a':
                crateAddressFlag = true;
                strcpy(crateAddress,optarg);
                break;
            case 'u':
                userNameFlag = true;
                strcpy(userName,optarg);
                break;
            case 'x':
                passwordFlag = true;
                strcpy(password,optarg);
                break;
            case 'd':
                disconnect = true;
                break;
            case 'p':
                propertyNames = optarg;
                break;
            case 'r':
                connect = true;
                readValue = true;
                break;
            case 'w':
                connect = true;
                writeValue = true;
                break;
            case 'v':
                propertySetValues = optarg;
                valueFlag = true;
                break;
            case 's':
                connect = true;
                pollReadValue = true;
                break;
            case 'z':
                connect = true;
                pollWriteValue = true;
                break;
            case 't':
                pollTime = atoi(optarg);
                break;
            case '?':
            case 'h':
                PrintUsage(argv);
                exit(1);
                break;
        }
    }


    // Check parameters for Write Value action

    if ((writeValue == true or pollWriteValue == true) and valueFlag == false)
    {
        fprintf(stderr, "ERROR: SetValue argument missing. Please specify -v <setValue> \n");
        exit(1);
    }


    // Assign default connection parameters if not passed to the program

    if (connectionNameFlag == false)
    {
        char str_connection[] = "eth1";
        strncpy(connectionName,str_connection,sizeof(str_connection));
    }

    if (crateAddressFlag == false)
    {
        char str_address[] = "hal://192.168.0.2:1454/can0";
        strncpy(crateAddress,str_address,sizeof(str_address));
    }

    if (userNameFlag == false)
    {
        char str_name[] = "user";
        strncpy(userName,str_name,sizeof(str_name));
    }

    if (passwordFlag == false)
    {
        char str_password[] = "pass";
        strncpy(password,str_password,sizeof(str_password));
    }


    // Prepare char* interface to be: "hal://ics.iseg-hv.com:1454/can0,user,pass"
    getInterfaceName(crateAddress, userName, password, interfaceName);


    /*
     * EXECUTION OF TESTS
    */

    // Connection
    if (connect)
    {
        ConnectCrate(connectionName, interfaceName);
    }


    // Write a property value
    if (writeValue)
    {
        WriteProperties(propertyNames, propertySetValues, connectionName);
    }

    // Poll write a property
    if (pollWriteValue && !pollReadValue)
    {
        while(true)
        {
            WriteProperties(propertyNames, propertySetValues, connectionName);
            usleep(pollTime*1000);
        }

    }


    // Read a property value
    if (readValue)
    {
        ReadProperties(propertyNames, connectionName);
    }


    // Poll read a property
    if (pollReadValue && !pollWriteValue)
    {
        while(true)
        {
            ReadProperties(propertyNames, connectionName);
            usleep(pollTime*1000);
        }

    }


    if (pollReadValue && pollWriteValue)
    {
        while(true)
        {
            WriteProperties(propertyNames, propertySetValues, connectionName);
            ReadProperties(propertyNames, connectionName);
            usleep(pollTime*1000);
        }
    }

    if (disconnect)
    {
        DisconnectCrate(connectionName);
    }


    // Free the memory
    //delete connectionName,propertyNames,propertySetValues,userName,password,crateAddress,interface;


} /* end main */




/*!
 * Function to prepare a const char* with the full interface name ready to be
 * used by ISEGHAL API.
 * Prepare char* interface to be: "hal://ics.iseg-hv.com:1454/can0,user,pass"
 */

static void getInterfaceName(char *crateAddress, char *userName, char *password, char *interfaceName)
{
    strcpy(interfaceName,crateAddress);
    strcat(interfaceName,",");
    strcat(interfaceName,userName);
    strcat(interfaceName,",");
    strcat(interfaceName,password);
}

/*!
 *  WriteProperties reads the parameters passed to terminal by the user, and prepares the calls to the
 *  WritePropertyValue function.
 */

static void WriteProperties(char *propertyNames, char *propertySetValues, const char *connectionName)
{
    char *tokenName;
    char *tokenValue;
    const char *valueList[MAX_PROPERTIES];
    const char *propertyList[MAX_PROPERTIES];

    int numberProperties = 0;
    for (tokenName = strtok(propertyNames,","); tokenName; tokenName = strtok(NULL,","))
    {
        propertyList[numberProperties] = tokenName;
        numberProperties++;
    }

    int numberValues = 0;
    for (tokenValue = strtok(propertySetValues,","); tokenValue; tokenValue = strtok(NULL,","))
    {
        valueList[numberValues] = tokenValue;
        numberValues++;
    }

    if (numberProperties != numberValues)
    {
        fprintf(stderr,"%s ERROR %s SET Wrong number of values passed for the properties \n",CurrentDateTime(),connectionName);
    }

    for (int i=0;i<numberProperties;i++)
    {

        int result = WritePropertyValue(connectionName,propertyList[i],valueList[i]);

        // Write successfully done

        if (result == EXIT_SUCCESS)
        {
            fprintf(stdout,"%s INFO %s SET %s = %s \n",CurrentDateTime(),connectionName,propertyList[i],valueList[i]);
        }

        // Else ERROR

        else
        {
            fprintf(stderr,"%s ERROR %s SET %s = %s \n",CurrentDateTime(),connectionName,propertyList[i],valueList[i]);
        }

    }

}

/*!
 *  ReadProperties reads the parameters passed to terminal by the user, and prepares the calls to the
 *  ReadPropertyValue function. Manages lecture of several properties.
 */

static void ReadProperties(char *propertyNames, const char *connectionName)
{
    const char *token;
    for (token = strtok(propertyNames,","); token; token = strtok(NULL,","))
    {
        fprintf(stdout,"%s INFO %s GET %s \n",CurrentDateTime(),connectionName,token);
        ReadPropertyValue(connectionName, token);
    }
}







