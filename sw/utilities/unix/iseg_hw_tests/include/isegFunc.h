/*!
 *  @file     isegFunc.h
 *  @defgroup iseg_hw_tests
 *  @brief    isegHAL testing program
 *
 *  Functions to prepare the parameters and call the ISEG API.
 */

#ifndef ISEGFUNC_H
#define ISEGFUNC_H

/*!
 * Print program usage
 */

void PrintUsage(char **argv);

/*!
 * Format current time (calculated as an offset in current day) in this form:
 * "hh:mm:ss.SSS" (where "SSS" are milliseconds)
 */

char *CurrentDateTime();

/*!
 * Connection to the crate system using the API ISEGHAL
 */

int ConnectCrate(const char *connectionName, const char *interface);

/*!
 * Disconnection to the crate system using the API ISEGHAL
 */

int DisconnectCrate(const char *connectionName);

/*!
 * Read a property value using the API ISEGHAL
 */

int ReadPropertyValue(const char *connectionName, const char *propertyName);

/*!
 * Write a property value using the API ISEGHAL
 */

int WritePropertyValue(const char *connectionName, const char *propertyName, const char *value);




#endif /* ISEGFUNC_H */

