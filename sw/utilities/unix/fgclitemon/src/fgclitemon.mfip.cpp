/*!
 * @file   fgclitemon.cpp
 * @brief  Utility for testing and calibrating FGClite hardware and firmware
 * @author Michael Davis
 *
 * Use the separate program test_diamon to monitor Diamon status while fgclitemon is running.
 */

#include <iostream>
#include <iomanip>
#include <sstream>

#include <cstdio>
#include <cerrno>
#include <cstdio>
#include <stdlib.h>
#include <ncurses.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>

// FIP FDM includes

#include <fip.mfip.h>
#include <fip_diamon.h>
#include <fip_watchdog.h>

// FGCD includes

#include <serial.h>
#include <fgclite_cycle.h>
#include <fgcd_thread.h>
#include <fip.h>
#include <fieldbus.h>
#include <timing.h>                     // stub with timingSetUserTime and timingGetUserTime prototypes
#include <fgcd.h>                       // stub with struct FGCD definition
#include <fgcddev.h>                    // stub with struct FGCDdev definition
#include <equipdev.h>                   // stub with struct Equipdev definition
#include <pub.h>                        // stub with struct Pub definition

#include <fgclitemon.h>



// Constants

const uint32_t col1 = 35;               // First display column
const uint32_t col2 = 55;               // Second display column
const uint32_t col3 = 75;               // Third display column

const char* const DEVICE_NAME = "FGCLITE"; // Dummy name file entry



// Global variables

struct FGClitemon          fgclitemon;  // FGClitemon globals

struct Fgcd                fgcd;        // FGCD properties
struct FGCDdev             fgcddev;     // FGCD device properties
struct Equipdev            equipdev;    // Equipment device properties
struct Pub                 pub;         // Publication properties



// Stub functions to avoid having to link with all the FGCD code

void timingSetUserTime(uint32_t user, struct timeval *time)
{
    fgclitemon.user_time = *time;
}

void timingGetUserTime(uint32_t user, struct timeval *time)
{
    *time = fgclitemon.user_time;
}



// Static functions

/*
 * Print usage information
 */

static void printUsage(char *program_name)
{
    using namespace std;

    cerr << endl << "Usage: " << program_name << " [-h] [-s script_file] [address]" << endl;
    cerr << endl << "           -h                Show this help information." << endl;
    cerr <<         "           -s script_file    Filename for test command script. Defaults to \"" << DEFAULT_TEST_SCRIPT_FILENAME << "\"." << endl;
    cerr <<         "           address           FIP address of the FGClite device (1-30). Defaults to " << +DEFAULT_DEVICE_ID << "." << endl;
    cerr << endl << "The script file is loaded by pressing \"l\" after the program starts." << endl << endl;
}



/*
 * Reset terminal screen on exit
 */

static void endCurses(void)
{
    if (fgclitemon.curses_started && !isendwin()) endwin();
}



/*
 * Initialise curses
 */

static void cursesStart(void)
{
    if (fgclitemon.curses_started) {
        refresh();
    }
    else
    {
        initscr();
        cbreak();
        noecho();
        intrflush(stdscr, false);
        keypad(stdscr, true);
        nodelay(stdscr, TRUE); // don't block on detect keypress
        atexit(endCurses);
        start_color();
        use_default_colors();
        fgclitemon.curses_started = true;
    }
}



/*
 * Detect a keypress
 */

static int kbhit(void)
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}



/*
 * Display paged data
 */

static void printPaged(uint32_t device, uint32_t y, uint32_t x)
{
    char buffer[256];

    attron(A_BOLD);
    attron(COLOR_PAIR(2));

    move(y,x);    addstr("PAGED DATA");

    attroff(COLOR_PAIR(2));
    attroff(A_BOLD);

    for(uint32_t row = 0; row < 8; ++row)
    {
        for(uint32_t el = 0; el < 8; ++el)
        {
            sprintf(buffer + (el * 3), "%02X ", fgcliteCycleGetStatusPtr(device)->paged[(row * 8) + el]);
        }
        move(row + y + 2, x);
        addstr(buffer);
    }
}



/*
 * Display critical data
 */

static void printCritical(uint32_t device, uint32_t y)
{
    char buffer[256];

    attron(A_BOLD);
    attron(COLOR_PAIR(2));

    move(y,1);    addstr("CRITICAL DATA");
    move(y,col1); addstr("HEX");
    move(y,col2); addstr("DEC");

    attroff(COLOR_PAIR(2));
    attroff(A_BOLD);

    move(y+2,1);    addstr("V_MEAS_0_9");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.v_meas_0_9);
    move(y+2,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.v_meas_0_9);
    move(y+2,col2); addstr(buffer);

    move(y+3,1);    addstr("V_MEAS_10_19");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.v_meas_10_19);
    move(y+3,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.v_meas_10_19);
    move(y+3,col2); addstr(buffer);

    move(y+4,1);    addstr("I_A_0_9");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.i_a_0_9);
    move(y+4,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.i_a_0_9);
    move(y+4,col2); addstr(buffer);

    move(y+5,1);    addstr("I_A_10_19");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.i_a_10_19);
    move(y+5,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.i_a_10_19);
    move(y+5,col2); addstr(buffer);

    move(y+6,1);    addstr("I_B_0_9");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.i_b_0_9);
    move(y+6,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.i_b_0_9);
    move(y+6,col2); addstr(buffer);

    move(y+7,1);    addstr("I_B_10_19");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.i_b_10_19);
    move(y+7,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.i_b_10_19);
    move(y+7,col2); addstr(buffer);

    move(y+8,1);    addstr("DIM_A_TRIG_LAT");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_a_trig_lat);
    move(y+8,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_a_trig_lat);
    move(y+8,col2); addstr(buffer);

    move(y+9,1);    addstr("DIM_A_TRIG_UNL");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_a_trig_unl);
    move(y+9,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_a_trig_unl);
    move(y+9,col2); addstr(buffer);

    move(y+10,1);    addstr("DIM_A_1_ANA_0");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_0);
    move(y+10,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_0);
    move(y+10,col2); addstr(buffer);

    move(y+11,1);    addstr("DIM_A_1_ANA_1");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_1);
    move(y+11,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_1);
    move(y+11,col2); addstr(buffer);

    move(y+12,1);    addstr("DIM_A_1_ANA_2");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_2);
    move(y+12,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_2);
    move(y+12,col2); addstr(buffer);

    move(y+13,1);    addstr("DIM_A_1_ANA_3");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_3);
    move(y+13,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_a_1_ana_3);
    move(y+13,col2); addstr(buffer);

    move(y+14,1);    addstr("CYCLE_PERIOD");
    sprintf(buffer, "%08X", fgcliteCycleGetStatusPtr(device)->critical.cycle_period);
    move(y+14,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.cycle_period);
    move(y+14,col2); addstr(buffer);

    move(y+15,1);    addstr("ADC_LOG_INDEX");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.adc_log_index);
    move(y+15,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.adc_log_index);
    move(y+15,col2); addstr(buffer);

    move(y+16,1);    addstr("DIM_LOG_INDEX");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.dim_log_index);
    move(y+16,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.dim_log_index);
    move(y+16,col2); addstr(buffer);

    move(y+17,1);    addstr("CONVERTER_INPUT");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.converter_input);
    move(y+17,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.converter_input);
    move(y+17,col2); addstr(buffer);

    move(y+18,1);    addstr("CONVERTER_OUTPUT");
    sprintf(buffer, "%02X", fgcliteCycleGetStatusPtr(device)->critical.converter_output);
    move(y+18,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.converter_output);
    move(y+18,col2); addstr(buffer);

    move(y+19,1);    addstr("SEU_COUNT");
    sprintf(buffer, "%02X", fgcliteCycleGetStatusPtr(device)->critical.seu_count);
    move(y+19,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.seu_count);
    move(y+19,col2); addstr(buffer);

    // Mask out the cycle counter from the hex output, and show only the cycle counter in the decimal output

    move(y+20,1);    addstr("CONTROLLER_STATUS");
    sprintf(buffer, "%04X", fgcliteCycleGetStatusPtr(device)->critical.controller_status & 0xFFFC);
    move(y+20,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetStatusPtr(device)->critical.controller_status & 0x0003);
    move(y+20,col2); addstr(buffer);

    move(y+21,1);    addstr("SERIAL_DATA");
    for(uint32_t i = 0; i < 7; ++i)
    {
        sprintf(buffer + (i*2), "%02X", fgcliteCycleGetStatusPtr(device)->critical.serial_data[i]);
    }
    move(y+21,col1); addstr(buffer);
    for(uint32_t i = 0; i < 7; ++i)
    {
        char c = fgcliteCycleGetStatusPtr(device)->critical.serial_data[i];
        if(c < 32 || c > 127) c = '?';
        *(buffer + i) = c;
    }
    buffer[7] = '\0';
    move(y+21,col2); addstr(buffer);

    uint16_t *i_ptr = reinterpret_cast<uint16_t*>(&(fgcliteCycleGetStatusPtr(device)->critical.serial_data[1]));
    sprintf(buffer, "MS_PERIOD = %d, Counter = %d", *i_ptr, *(i_ptr+1));
    move(y+21,col3); addstr(buffer);

    // Data from the Round Robin registers

    move(y+22,1);    addstr("UPTIME_COUNTER");
    sprintf(buffer, "%04X", fgcliteCycleGetUptimeCounter(device));
    move(y+22,col1); addstr(buffer);
    sprintf(buffer, "%d", fgcliteCycleGetUptimeCounter(device));
    move(y+22,col2); addstr(buffer);

    move(y+23,1);    addstr("PWRCYCLE_COUNTER");
    sprintf(buffer, "%04X", equipdev.device[device].device.pwrcyc_count);
    move(y+23,col1); addstr(buffer);
    sprintf(buffer, "%d", equipdev.device[device].device.pwrcyc_count);
    move(y+23,col2); addstr(buffer);

    move(y+24,1);    addstr("PWRCYCLE_REASON");
    sprintf(buffer, "%04X", equipdev.device[device].device.pwrcyc_reason);
    move(y+24,col1); addstr(buffer);
    sprintf(buffer, "%d", equipdev.device[device].device.pwrcyc_reason);
    move(y+24,col2); addstr(buffer);

    move(y+25,1);    addstr("VERSION");
    sprintf(buffer, "%X%X%X%X",
        equipdev.device[device].device.fpga_version[0],
        equipdev.device[device].device.fpga_version[1],
        equipdev.device[device].device.fpga_version[2],
        equipdev.device[device].device.fpga_version[3]);
    move(y+25,col1); addstr(buffer);
    sprintf(buffer, "NF = %d CF = %d XF = %d PF = %d",
        equipdev.device[device].device.fpga_version[0],
        equipdev.device[device].device.fpga_version[1],
        equipdev.device[device].device.fpga_version[2],
        equipdev.device[device].device.fpga_version[3]);
    move(y+25,col2); addstr(buffer);

    move(y+26,1);    addstr("BACKPLANE_TYPE");
    sprintf(buffer, "%02X", equipdev.device[device].crate_type | (equipdev.device[device].crate_position << 5));
    move(y+26,col1); addstr(buffer);
    sprintf(buffer, "Type = %02X, Position = %d", equipdev.device[device].crate_type, equipdev.device[device].crate_position);
    move(y+26,col2); addstr(buffer);
}



/*
 * Display WorldFIP bus and device statistics
 */

static void printWorldFipStats(uint32_t device, uint32_t y)
{
    char buffer[256];

    attron(A_BOLD);
    attron(COLOR_PAIR(2));

    move(y,1);    addstr("WorldFIP BUS STATISTICS");
    move(y+8,1);    addstr("WorldFIP DEVICE STATISTICS");
    move(y+8,col1); addstr("DIAG_DEV");
    move(y+8,col2); addstr("FGClite");

    attroff(COLOR_PAIR(2));
    attroff(A_BOLD);

    move(y+2,1);    addstr("Start Count");
    sprintf(buffer, "%d", fip.fieldbus_stats.start_count);
    move(y+2,col1); addstr(buffer);

    move(y+3,1);    addstr("Cycle Count");
    sprintf(buffer, "%d", fieldbus.stats.cycle_count);
    move(y+3,col1); addstr(buffer);

    move(y+4,1);    addstr("Error Flag");
    sprintf(buffer, "%d", fip.fieldbus_stats.error_flag);
    move(y+4,col1); addstr(buffer);

    move(y+5,1);    addstr("Interface Error Count/Value");
    sprintf(buffer, "%d", fip.fieldbus_stats.interface_error_count);
    move(y+5,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.fieldbus_stats.last_interface_error);
    move(y+5,col2); addstr(buffer);

    move(y+10,1);    addstr("Device Present");
    move(y+10,col1); addstr(fipMfipIsDevicePresent(FIP_DIAG_DEV_ADDR) ? "Yes" : "No");
    move(y+10,col2); addstr(fipMfipIsDevicePresent(fgclitemon.device_id) ? "Yes" : "No");

    move(y+11,1);    addstr("Variable Receive Count");
    sprintf(buffer, "%d", fip.diag_dev.var_recv_count);
    move(y+11,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.var_recv_count);
    move(y+11,col2); addstr(buffer);
    sprintf(buffer, "Difference = %d", fip.diag_dev.var_recv_count-fip.channel[device].fip_stats.var_recv_count);
    move(y+11,col3); addstr(buffer);

    move(y+12,1);    addstr("Variable Miss Count");
    sprintf(buffer, "%d", fip.diag_dev.var_miss_count);
    move(y+12,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.var_miss_count);
    move(y+12,col2); addstr(buffer);

    move(y+13,1);    addstr("Variable Late Count");
    sprintf(buffer, "%d", fip.diag_dev.var_late_count);
    move(y+13,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.var_late_count);
    move(y+13,col2); addstr(buffer);

    move(y+14,1);    addstr("Nonsignificance Fault Count");
    sprintf(buffer, "%d", fip.diag_dev.nonsignificance_fault_count);
    move(y+14,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.nonsignificance_fault_count);
    move(y+14,col2); addstr(buffer);

    move(y+15,1);    addstr("Promptness Fail Count");
    sprintf(buffer, "%d", fip.diag_dev.promptness_fail_count);
    move(y+15,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.promptness_fail_count);
    move(y+15,col2); addstr(buffer);

    move(y+16,1);    addstr("Significance Fault Count");
    sprintf(buffer, "%d", fip.diag_dev.significance_fault_count);
    move(y+16,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.significance_fault_count);
    move(y+16,col2); addstr(buffer);

    move(y+17,1);    addstr("User Error Count");
    sprintf(buffer, "%d", fip.diag_dev.user_error_count);
    move(y+17,col1); addstr(buffer);
    sprintf(buffer, "%d", fip.channel[device].fip_stats.user_error_count);
    move(y+17,col2); addstr(buffer);
}

/*
 * Display the menu
 */

static void printMenu(uint32_t device, uint32_t y)
{
    char buffer[256];

    attron(A_BOLD);
    attron(COLOR_PAIR(3));

    move(y,1); addstr("[s] nanoFIP soft reset    [h] nanoFIP hard reset    [P] Restart BA protocol    [I] Restart FIP interface    [r] Reset counters    [q] Quit");
    sprintf(buffer, "[l] load test script file [%s]", fgclitemon.script_filename);
    move(y+2,1); addstr(buffer);

    attroff(COLOR_PAIR(3));
    attron(COLOR_PAIR(4));

    move(y+2,col3); addstr(fgclitemon.command);

    attroff(COLOR_PAIR(4));
    attroff(A_BOLD);
}



/*
 * Refresh the display
 */

static void updateDisplay(uint32_t device)
{
    char buffer[256];
    char date[40];
    struct tm *tm;
    time_t time_secs;

    init_pair(1, COLOR_BLACK, -1);
    init_pair(2, COLOR_BLUE, -1);
    init_pair(3, COLOR_GREEN, -1);
    init_pair(4, COLOR_RED, -1);

    erase();

    // Header info

    attron(A_BOLD);
    attron(COLOR_PAIR(2));

    move(1,1);    addstr("FGClite MONITOR");

    attroff(COLOR_PAIR(2));
    attroff(A_BOLD);

    move(3,1);    addstr("Device address:");
    sprintf(buffer, "%d", device);
    move(3,col1); addstr(buffer);

    move(4,1);    addstr("FIP Start time:");
    time_secs = fipMfipGetStartTime();
    tm = localtime(&time_secs);
    strftime(date, sizeof(date), "%Y-%m-%d %H:%M:%S", tm);
    move(4,col1); addstr(date);

    // Data

    printPaged(device, 6, col3);
    printCritical(device, 6);
    printWorldFipStats(device, 34);

    // Menu

    printMenu(device,53);

    refresh();
}



static void fgclitemonResetDevice(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Reset the serial buffers

    serialReset(channel);

    // Initialise the crate type and side as unknown

    device.crate_type     = 0x1F;
    device.crate_position = '?';

    // Start with a COMMS fault until communication is established,
    // and a PC state fault until the device is configured.

    device.status.st_faults |= FGC_FLT_COMMS;
}



/*
 * Start the FIP interface
 */

static int32_t startFip(void)
{
    time_t timeNow; 

    if(fgcliteCycleStart() != 0)
    {
        time(&timeNow);
        logTime(timeNow);
        fgclitemon.error_log << "Fatal error: fgcliteCycleStart() failed, errno = " << errno << std::endl;
        return -1;
    }

    if(fgcliteCycleFipStart() != 0)
    {
        time(&timeNow);
        logTime(timeNow);
        fgclitemon.error_log << "Fatal error: fgcliteCycleFipStart() failed, errno = " << errno << std::endl;
        return -1;
    }

    time(&timeNow);
    logTime(timeNow);
    fgclitemon.error_log << "FIP start time is ";
    logTime(fipMfipGetStartTime());
    fgclitemon.error_log << std::endl;

    if(fgcliteCycleProtocolStart() != 0)
    {
        time(&timeNow);
        logTime(timeNow);
        fgclitemon.error_log << "Fatal error: fgcliteCycleProtocolStart() failed, errno = " << errno << std::endl;
        return -1;
    }

    return 0;
}



/*
 * Shut down the FIP interface
 */

static void stopFip(void)
{
    time_t timeNow; 
    time(&timeNow);
    logTime(timeNow);
    fgclitemon.error_log << "Shutting down FIP interface" << std::endl;

    fipMfipStopInterface();

    fipDiamonCleanUp();
}


/*
 * Start logging a new type of paged data
 */

static void cmdGetIndex(uint8_t device_id, int32_t index_type_int, uint32_t index, bool inc_index)
{
    time_t timeNow;

    // Stop logging

    fgclitemon.paged_log.close();

    // Initialise index

    fgclitemon.index = index;

    // Set index to auto-increment?

    fgclitemon.inc_index = inc_index;

    // Check that the requested index type is valid

    if(index_type_int < 0 || index_type_int > DIM_LOG)
    {
        strcpy(fgclitemon.command, "LOG STOP");
        return;
    }

    enum Ctrl_cmd_index_type index_type = static_cast<enum Ctrl_cmd_index_type>(index_type_int);

    // Send the command to the FGClite

    fgcliteCmdRequestCtrl(fgcliteCycleGetCmdPtr(CMD_CTRL), device_id, index_type, index);

    // Build the filename for the new logfile

    char paged_log_filename[256];
    strcpy(paged_log_filename, LOG_DIR);
    strcat(paged_log_filename, "paged_");

    switch(index_type)
    {
        case DIM_BUS:
            strcat(paged_log_filename, "dim_bus");
            strcpy(fgclitemon.command, "LOG DIM_BUS");
            break;

        case OW_BUS:
            strcat(paged_log_filename, "ow_bus");
            strcpy(fgclitemon.command, "LOG OW_BUS");
            break;

        case ADC_I_B_LOG:
            strcat(paged_log_filename, "adc_i_b");
            strcpy(fgclitemon.command, "LOG ADC_I_B");
            break;

        case ADC_I_A_LOG:
            strcat(paged_log_filename, "adc_i_a");
            strcpy(fgclitemon.command, "LOG ADC_I_A");
            break;

        case ADC_V_MEAS_LOG:
            strcat(paged_log_filename, "adc_v_meas");
            strcpy(fgclitemon.command, "LOG ADC_V_MEAS");
            break;

        case DIM_LOG:
            strcat(paged_log_filename, "dim_log");
            strcpy(fgclitemon.command, "LOG DIM_LOG");
            break;
    }
    strcat(paged_log_filename, ".csv");

    // Open the logfile

    fgclitemon.paged_log.open(paged_log_filename, std::ofstream::trunc);
    if(!fgclitemon.paged_log.is_open())
    {
        time(&timeNow);
        logTime(timeNow);
        fgclitemon.error_log << "Failed to open " << paged_log_filename << " for writing" << std::endl;
        return;
    }
    fgclitemon.paged_log << std::setfill('0');

    // Temporarily disable ADC logging. Logging will start on the next 1 Hz boundary.

    if(index_type == ADC_V_MEAS_LOG || index_type == ADC_I_A_LOG || index_type == ADC_I_B_LOG)
    {
        fgclitemon.log_adc_data = false;
    }
}



void logTime(const time_t &s)
{
    struct tm *tm = localtime(&s);
    char date[20];
    strftime(date, sizeof(date), "%Y-%m-%d %H:%M:%S", tm);
    fgclitemon.error_log << date << " ";
}



void processCommand(std::string &buffer)
{
    using namespace std;

    struct FGClite_command *COMMAND0 = fgcliteCycleGetCmdPtr(CMD_SYNC);
    struct FGClite_command *COMMAND2 = fgcliteCycleGetCmdPtr(CMD_CTRL);
    struct FGClite_command *COMMAND3 = fgcliteCycleGetCmdPtr(CMD_CONV);

    string  cmd;
    int32_t param1;
    int32_t param2;

    // Split into command + parameter

    istringstream s(buffer);
    s >> cmd;
    s >> param1;
    s >> param2; // only used by SLEEP and GET_INDEX

    if(cmd == "" || cmd == "#") return;

    // Terminate program

    if(cmd == "QUIT")
    {
        fgclitemon.do_quit = true;
        return;
    }

    // Sleep command

    if(cmd == "SLEEP")
    {
       sleep(param1);
       msSleep(param2);
       return;
    }

    // Send command to device

    strcpy(fgclitemon.command, buffer.c_str());

    // nanoFIP reset commands

         if(cmd == "RESET_SOFT")          fgcliteCycleNanofipReset (fgclitemon.device_id, RESET_SOFT);
    else if(cmd == "RESET_HARD")          fgcliteCycleNanofipReset (fgclitemon.device_id, RESET_HARD);

    // SYNC_CMD

    else if(cmd == "SEFI_DET_TEST_VS_M0") fgcliteCmdSetSEFI        (COMMAND0, fgclitemon.device_id, SEFI_DET_TEST_VS_M0, static_cast<enum SEFI_test_value>(param1));
    else if(cmd == "SEFI_DET_TEST_VS_M1") fgcliteCmdSetSEFI        (COMMAND0, fgclitemon.device_id, SEFI_DET_TEST_VS_M1, static_cast<enum SEFI_test_value>(param1));
    else if(cmd == "SEFI_DET_TEST_IA_M0") fgcliteCmdSetSEFI        (COMMAND0, fgclitemon.device_id, SEFI_DET_TEST_IA_M0, static_cast<enum SEFI_test_value>(param1));
    else if(cmd == "SEFI_DET_TEST_IA_M1") fgcliteCmdSetSEFI        (COMMAND0, fgclitemon.device_id, SEFI_DET_TEST_IA_M1, static_cast<enum SEFI_test_value>(param1));
    else if(cmd == "SEFI_DET_TEST_IB_M0") fgcliteCmdSetSEFI        (COMMAND0, fgclitemon.device_id, SEFI_DET_TEST_IB_M0, static_cast<enum SEFI_test_value>(param1));
    else if(cmd == "SEFI_DET_TEST_IB_M1") fgcliteCmdSetSEFI        (COMMAND0, fgclitemon.device_id, SEFI_DET_TEST_IB_M1, static_cast<enum SEFI_test_value>(param1));

    else if(cmd == "MS_PERIOD")           fgcliteCmdSetMsPeriod    (COMMAND0, fgclitemon.device_id, param1);

    // CONTROLLER_CMD

    else if(cmd == "ADC_INDEX_TYPE")      cmdGetIndex              (fgclitemon.device_id, param1, fgclitemon.index, true);     // increment index automatically
    else if(cmd == "GET_INDEX")           cmdGetIndex              (fgclitemon.device_id, param1, param2,           false);    // don't increment index automatically

    else if(cmd == "SET_CTRL")            fgcliteCmdSetCtrl        (COMMAND2, fgclitemon.device_id, param1);
    else if(cmd == "UNSET_CTRL")          fgcliteCmdUnsetCtrl      (COMMAND2, fgclitemon.device_id, param1);
    else if(cmd == "RESET_CTRL")          fgcliteCmdResetCtrl      (COMMAND2, fgclitemon.device_id);

    // CONVERTER_CMD

    else if(cmd == "SET_VREF")            fgcliteCmdSetVRef        (COMMAND3, fgclitemon.device_id, param1);
    else if(cmd == "SET_CAL_SRC")         fgcliteCmdSetCalSource   (COMMAND3, fgclitemon.device_id, static_cast<enum Cal_source>(param1));

    else if(cmd == "SET_CONV")            fgcliteCmdSetConv        (COMMAND3, fgclitemon.device_id, param1);
    else if(cmd == "UNSET_CONV")          fgcliteCmdUnsetConv      (COMMAND3, fgclitemon.device_id, param1);
    else if(cmd == "RESET_CONV")          fgcliteCmdResetConv      (COMMAND3, fgclitemon.device_id);

    // Unknown command

    else
    {
        strcpy(fgclitemon.command, "ERROR: Check log");

        time_t timeNow; 
        time(&timeNow);
        logTime(timeNow);
        fgclitemon.error_log << "Invalid command: " << buffer << endl;
    }
}



void *commandFileRun(void *unused)
{
    using namespace std;

    string buffer;

    while(true)
    {
        if(!fgclitemon.test_script.is_open())
        {
            msSleep(100);
            continue;
        }

        if(fgclitemon.test_script.eof())
        {
            fgclitemon.test_script.close();
            strcpy(fgclitemon.command, "EOF");
            continue;
        }

        // File is open and not EOF--read one line

        getline(fgclitemon.test_script, buffer);

        processCommand(buffer);
    }

    return 0;
}



void *commandPipeRun(void *unused)
{
    using namespace std;

    string buffer;

    fgclitemon.command_pipe.open(COMMAND_PIPE);
    if(!fgclitemon.command_pipe.is_open())
    {
        cerr << "Failed to open named pipe " << COMMAND_PIPE << endl;
        exit(1);
    }

    while(true)
    {
        getline(fgclitemon.command_pipe, buffer);

        if(fgclitemon.command_pipe.eof())
        {
            fgclitemon.command_pipe.clear();
        }
        else
        {
            processCommand(buffer);
        }
    }

    return 0;
}



void updateDisplayRun(void)
{
    while(true)
    {
        if(fgclitemon.do_quit) return;

        updateDisplay(fgclitemon.device_id);
        msSleep(100);

        if(kbhit())
        {
            char c = getch();

            if(c == 'q' || c == 'Q') return;

            if(c == 'r' || c == 'R')
            {
                // reset statistic counters

                fipMfipResetDeviceStats(&fip.diag_dev);
                fipMfipResetDeviceStats(&fip.channel[fgclitemon.device_id].fip_stats);

            }

            if(c == 'P') fipMfipRestartProtocol();

            if(c == 'I') fipMfipRestartInterface();

            if(c == 's' || c == 'S') fgcliteCycleNanofipReset(fgclitemon.device_id, RESET_SOFT);

            if(c == 'h' || c == 'H') fgcliteCycleNanofipReset(fgclitemon.device_id, RESET_HARD);

            if(c == 'l' || c == 'L')
            {
                if(!fgclitemon.test_script.is_open()) fgclitemon.test_script.open(fgclitemon.script_filename);
                if(!fgclitemon.test_script.is_open())
                {
                    time_t timeNow; 
                    time(&timeNow);
                    logTime(timeNow);
                    fgclitemon.error_log << "Failed to open " << fgclitemon.script_filename << " for reading" << std::endl;
                }
            }
        }
    }
}



int main(int argc, char *argv[])
{
    using namespace std;

    // Initialise data structures (not used, but required to link with fgclite_cycle.cpp)

    for(uint32_t channel = 1; channel <= 30; ++channel)
    {
        equipdev.device[channel].fgcd_device = &fgcddev.device;
    }

    // Process command line options

    int32_t c;

    while((c = getopt(argc, argv, "hs:")) != -1)
    {
        switch(c)
        {
            case 's':
                fgclitemon.script_filename = optarg;
                break;

            case 'h':
                printUsage(argv[0]);
                return 0;

            default:
                printUsage(argv[0]);
                return 1;
        }
    }

    if(argc-optind > 1)
    {
        printUsage(argv[0]);
        return 1;
    }

    if(argc-optind == 1)
    {
        errno = 0;
        uint64_t device_id = strtoul(argv[argc-1], NULL, 0);
        if(errno > 0 || device_id < 1 || device_id > 30)
        {
            cerr << "Invalid WorldFIP address " << argv[1] << endl;
            printUsage(argv[0]);
            return 1;
        }
        fgclitemon.device_id = device_id;
    }

    // Open the error logfile

    char log_filename[256];
    strcpy(log_filename, LOG_DIR);
    strcat(log_filename, "fgclitemon_err.log");

    fgclitemon.error_log.open(log_filename, ofstream::trunc);
    if(!fgclitemon.error_log.is_open())
    {
        cerr << "Failed to open " << log_filename << " for writing" << endl;
        exit(1);
    }

    // Open the ADC critical data logfile

    strcpy(log_filename, LOG_DIR);
    strcat(log_filename, "critical_adc.csv");

    fgclitemon.critical_log.open(log_filename, ofstream::trunc);
    if(!fgclitemon.critical_log.is_open())
    {
        cerr << "Failed to open " << log_filename << " for writing" << endl;
        exit(1);
    }
    fgclitemon.critical_log << "Timestamp,Index,V_MEAS,I_A,I_B,Cycle Period,Version,SEU Count,Controller Status,Backplane Type" << fixed << setprecision(3) << endl;

    // Create and open a named pipe to accept commands from external programs

    unlink(COMMAND_PIPE);
    mode_t old_mask = umask(0111);
    if(mkfifo(COMMAND_PIPE, 0666) != 0)
    {
        cerr << "Failed to create named pipe " << COMMAND_PIPE << endl;
        exit(1);
    }
    umask(old_mask);

    // Open a file to allow external programs to view the critical data

    fgclitemon.critical_fd = open(CRITICAL_FILE, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if(fgclitemon.critical_fd < 0)
    {
        cerr << "Failed to open " << CRITICAL_FILE << endl;
        exit(1);
    }

    // Stretch the file size to the data size

    lseek(fgclitemon.critical_fd, MEMMAP_FILE_SIZE, SEEK_SET);
    write(fgclitemon.critical_fd, "", 1);

    // Map memory region to file

    fgclitemon.critical_data = mmap(NULL, MEMMAP_FILE_SIZE, PROT_WRITE, MAP_SHARED, fgclitemon.critical_fd, 0);
    if(fgclitemon.critical_data == MAP_FAILED)
    {
        cerr << "Failed to memory map critical data to " << CRITICAL_FILE << endl;
        perror("mmap");
        exit(1);
    }
    close(fgclitemon.critical_fd);

    // Initialise FGClite device

    fgclitemonResetDevice(fgclitemon.device_id);

    // Register test device with Diamon

    for(uint8_t device_id = 1; device_id < 31; ++device_id)
    {
        fgcd.device[device_id].name = const_cast<char*>(DEVICE_NAME);

        fipMfipConfigureDevice(device_id);
    }

    // Initialise CTRI timing

    TimLibInitialize(TimLibDevice_CTR);

    // Initialise WorldFIP

    startFip();

    // Start Watchdog and Diamon status reporting

    watchdogStart();

    // Initialise curses

    cursesStart();

    // Start command processing threads and mainloop

    fgcd_thread_create(&fgclitemon.file_thread, commandFileRun, NULL, FGCLITE_CYCLE_THREAD_POLICY, static_cast<enum FGCD_priority>(30),NO_AFFINITY);
    fgcd_thread_create(&fgclitemon.pipe_thread, commandPipeRun, NULL, FGCLITE_CYCLE_THREAD_POLICY, static_cast<enum FGCD_priority>(31),NO_AFFINITY);

    updateDisplayRun();

    // Shut down

    endCurses();
    stopFip();

    // Clean up

    fgclitemon.command_pipe.close();
    unlink(COMMAND_PIPE);

    munmap(fgclitemon.critical_data, MEMMAP_FILE_SIZE);
    remove(CRITICAL_FILE);
}

// EOF
