/*
 * @file   fgclite_reg.cpp
 * @brief  Definitions for FGClite regulation functions
 * @author Michael Davis
 */

#include <iostream>
#include <iomanip>
#include <time.h>

#include <serial.h>
#include <fgclitemon.h>
#include <fgclite_cmd.h>
#include <fgclite_reg.h>



/*
 * Get a 16-bit little-endian signed integer
 */

static int16_t getInt16(const uint8_t *ptr)
{
    int16_t result = 0;

    result += *ptr++;            // Least significant byte
    result += *ptr     <<  8;    // Most significant byte

    return result;
}



/*
 * Convert a 24-bit little-endian signed integer into a 32-bit signed integer
 */

static int32_t getInt24(const uint8_t *ptr)
{
    int32_t result = 0;

    result += *ptr++;            // Least significant byte
    result += *ptr++   <<  8;    // Middle byte
    result += *ptr     << 16;    // Most significant byte

    // If it's a negative number, flip all the bits in the top byte

    if(*ptr & 0x80) result |= 0xFF000000;

    return result;
}



/*
 * update critical data in memmapped region
 */

static void updateCritical(const struct FGClite_status *status)
{
    char *mmap_ptr = static_cast<char*>(fgclitemon.critical_data);

    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.v_meas_0_9);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.v_meas_10_19);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.i_a_0_9);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.i_a_10_19);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.i_b_0_9);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.i_b_10_19);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_a_trig_lat);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_a_trig_unl);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_a_1_ana_0);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_a_1_ana_1);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_a_1_ana_2);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_a_1_ana_3);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.cycle_period);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.roundrobin_register_payload);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.adc_log_index);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.dim_log_index);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.converter_input);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.converter_output);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.seu_count);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.controller_status);
    mmap_ptr += sprintf(mmap_ptr, "%d\n", status->critical.roundrobin_register_tag);
    mmap_ptr += snprintf(mmap_ptr, 7, "%s\n", status->critical.serial_data);
}



/*
 * Pre- and post-regulation functions
 */

void fgcliteRegPre(uint8_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{

}

void fgcliteRegPost(uint8_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{

}



/*
 * Function called each FGClite cycle
 */

void fgcliteRegDevicePresent(uint8_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{
    using namespace std;

    static int32_t total[2] = {};

    // Only log data for the selected device

    if(channel != fgclitemon.device_id) return;

    // Log entries are uniquely identified using the UNIX timestamp and the ADC or DIM log index for the current cycle.

    time_t        log_time;
    static time_t log_time_1Hz;

    // Get UNIX timestamp for this cycle.

    time(&log_time);

    // Keep log times for critical data and ADC data in sync.
    // The paged data is one cycle behind the aggregated critical data.

    if(++fgclitemon.cycle_count == 2)
    {
        log_time_1Hz = log_time;
    }

    // Read the serial data

    serialReadCmd(channel, status->critical.serial_data);

    // Copy critical data to memory-mapped region

    updateCritical(status);

    // Output SEU_COUNT from the critical data if it is non-zero

    if(status->critical.seu_count != 0)
    {
        logTime(log_time);
        fgclitemon.error_log << "SEU_COUNT: " << static_cast<uint32_t>(status->critical.seu_count) << endl;
    }

    // Accumulate the critical ADC data at a rate of 20 samples per FIP cycle

    fgclitemon.critical_v_meas += status->critical.v_meas_0_9 + status->critical.v_meas_10_19;
    fgclitemon.critical_i_a    += status->critical.i_a_0_9    + status->critical.i_a_10_19;
    fgclitemon.critical_i_b    += status->critical.i_b_0_9    + status->critical.i_b_10_19;

    // Output the average at 1 Hz

    if(fgclitemon.cycle_count == 50)
    {
        int16_t log_index = (status->critical.adc_log_index - 1000 + 32768) % 32768;

        fgclitemon.critical_log << setbase(10)       << log_time_1Hz << ","
                                << log_index                         << ","
                                << fgclitemon.critical_v_meas / 1000 << ","
                                << fgclitemon.critical_i_a    / 1000 << ","
                                << fgclitemon.critical_i_b    / 1000 << ",";

        fgclitemon.cycle_count     = 0;
        fgclitemon.critical_v_meas = 0;
        fgclitemon.critical_i_a    = 0;
        fgclitemon.critical_i_b    = 0;

        // Log some other critical data values at 1 Hz

        fgclitemon.critical_log << status->critical.cycle_period                                      << ",0x"
                                << setbase(10) << static_cast<int32_t>(status->critical.seu_count)    << ",0x"
                                << setbase(16) << status->critical.controller_status                  << endl;
    }

    // Output the data from the current page

    Ctrl_cmd_index_type index_type = static_cast<Ctrl_cmd_index_type>((ctrl_cmd->payload[channel-1] >> 8) & 0x7);

    if(fgclitemon.paged_log.is_open()) switch(index_type)
    {
        uint32_t ow_bus_select;
        bool     ow_busy;

        case DIM_BUS:

            // There are 4 pages, each of them containing data from 4 DIMs (16 DIMs in total).
            // This is all the data coming from a DIM in the previous 20ms cycle. Each DIM has 8 16-bit registers.

            for(uint32_t dim = 0; dim < 4; ++dim)
            {
                fgclitemon.paged_log << "DIM_BUS," << setbase(10) << fgclitemon.index*4+dim << setbase(16);
                for(uint32_t i = 0; i < 8; ++i)
                {
                    fgclitemon.paged_log << ",0x" << setw(4) << static_cast<uint16_t>(getInt16(&status->paged[ (dim*8+i)*2 ]));
                }
                fgclitemon.paged_log << endl;
            }
            break;

        case OW_BUS:

            // There are 16 pages. Each page has data for 4 devices. Each device has 16 bytes.
            // The 16 bytes contain the ID and (optionally, depending on the type of device) the temperature.
            // The CCE Wiki says the unique ID is represented in 64 bits (8 bytes) and the temperature in 16 bits.
            // It looks like the remaining 6 bytes are unused. It's not clear to me what the format is for the ID
            // and temperature.

            ow_bus_select = ctrl_cmd->payload[channel-1] & 0x7;
            ow_busy       = status->critical.controller_status & OW_SCAN_BUSY;

            for(uint32_t ow = 0; ow < 4; ++ow)
            {
                fgclitemon.paged_log << "OW_BUS," << setbase(10) << setw(2) << ow_bus_select << "," << setw(2) << (fgclitemon.index * 4) + ow << setbase(16);
                fgclitemon.paged_log << "," << (ow_busy ? "BUSY " : "READY");
                for(uint32_t i = 0; i < 16; ++i)
                {
                    fgclitemon.paged_log << "," << setw(2) << static_cast<uint32_t>(status->paged[ ow*16+i ]);
                }
                fgclitemon.paged_log << endl;
            }
            break;

        case DIM_LOG:

            // Output eight samples for each DIM. Each sample is four 16-bit values.

            for(uint32_t sample = 0; sample < 7; ++sample)
            {
                fgclitemon.paged_log << "DIM_LOG," << setbase(10) << (fgclitemon.index & 0x7800) << "," << ((fgclitemon.index+sample) & 0x07FF) << setbase(16);

                // Log values in hex

                for(uint32_t i = 0; i < 4; ++i)
                {
                    fgclitemon.paged_log << ",0x" << setw(4) << static_cast<uint16_t>(getInt16(&status->paged[ (sample*4+i)*2 ]));
                }

                // Same values in decimal

                for(uint32_t i = 0; i < 4; ++i)
                {
                    fgclitemon.paged_log << "," << setbase(10) << static_cast<uint16_t>(getInt16(&status->paged[ (sample*4+i)*2 ]) & 0x0FFF);
                }
                fgclitemon.paged_log << endl;
            }
            break;

        case ADC_V_MEAS_LOG:
        case ADC_I_A_LOG:
        case ADC_I_B_LOG:

            // Each cycle we receive two groups of 10 samples. Each sample is a 24-bit signed value.

            fgclitemon.paged_log << setbase(10);

            // 1KHz logging: insert a line break in the data every 50 cycles
            // Note that the paged data is one cycle behind the aggregated critical data

            if(fgclitemon.cycle_count == 2)
            {
                // New line, enable ADC logging

                fgclitemon.log_adc_data = true;

                fgclitemon.paged_log << endl << log_time_1Hz << "," << fgclitemon.index;
            }

            for(uint32_t i = 0; i < 2; ++i)
            {
                int32_t row_total = 0;

                for(uint32_t j = 0; j < 10; ++j)
                {
                    int32_t val = getInt24(&status->paged[ (i*10+j)*3 ]);

                    if(fgclitemon.log_adc_data) fgclitemon.paged_log << ',' << val;

                    row_total += val;
                }

                if(row_total != total[i] && status->critical.adc_log_index-fgclitemon.index == 40)
                {
                    logTime(log_time);
                    fgclitemon.error_log << "ERROR in " << (index_type == ADC_V_MEAS_LOG ? "ADC_V_MEAS_LOG" :
                                                            index_type == ADC_I_A_LOG    ? "ADC_I_A_LOG"    : "ADC_I_B_LOG")
                                         << ": Sum of paged data samples (index " << fgclitemon.index + (i*10) << ".." << fgclitemon.index + (i*10) + 9 << ") is " << row_total
                                         << ", but sum in critical data is " << total[i] << endl;
                }
            }

            // Update totals for next cycle

            total[0] = index_type == ADC_V_MEAS_LOG ? status->critical.v_meas_0_9   :
                       index_type == ADC_I_A_LOG    ? status->critical.i_a_0_9      : status->critical.i_b_0_9;
            total[1] = index_type == ADC_V_MEAS_LOG ? status->critical.v_meas_10_19 :
                       index_type == ADC_I_A_LOG    ? status->critical.i_a_10_19    : status->critical.i_b_10_19;
    }

    // Request the page of data for the next cycle

    switch(index_type)
    {
        case DIM_BUS:
            // index is set manually, so extract it from the command
            fgclitemon.index = ctrl_cmd->payload[channel-1] >> 16;
            break;

        case OW_BUS:
            if(fgclitemon.inc_index)
            {
                // Cycle through all 16 pages, one per cycle.
                // Note that the data in these pages should only change after a scan of the OW bus.
                fgclitemon.index = (fgclitemon.index + 1) % 16;
            }
            break;

        case DIM_LOG:
            // I think upper 4 bits should be used to set the address of the DIM we want to index
            fgclitemon.index = (ctrl_cmd->payload[channel-1] >> 16) & 0x7800;
            if(fgclitemon.inc_index)
            {
                // Set lower 20 bits from the DIM log index
                fgclitemon.index += (status->critical.dim_log_index-8)   & 0x07FF;
            }
            break;

        case ADC_V_MEAS_LOG:
        case ADC_I_A_LOG:
        case ADC_I_B_LOG:
            if(fgclitemon.inc_index)
            {
                fgclitemon.index = (status->critical.adc_log_index - 20 + 32768) % 32768;
            }
            break;
    }

    fgcliteCmdRequestCtrl(ctrl_cmd, channel, index_type, fgclitemon.index);
}



/*
 * Function called if the FGClite is not present for one cycle
 */

void fgcliteRegDeviceNotPresent(uint8_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{
    time_t timeNow;
    time(&timeNow);
    logTime(timeNow);
    fgclitemon.error_log << "Warning: FGClite device " << static_cast<uint32_t>(channel) << " missed a cycle" << std::endl;
}

// EOF
