/*!
 * @file   serial.cpp
 * @brief  Functions for serial tunnelling across the FIP interface
 * @author Michael Davis
 */

#include <cstring>
#include <cstdio>    // for testing/debugging only

#include <cmdqmgr.h>
#include <serial.h>



// Constants

const uint32_t INPUT_CHUNK_SIZE     = 7;                                                   // Size in bytes of the serial field in the FGClite critical data
const uint32_t OUTPUT_CHUNK_SIZE    = 4;                                                   // Size in bytes of the serial payload for each device in SERIAL_CMD

const uint32_t INPUT_NUM_CHUNKS  = (CMD_MAX_LENGTH            / INPUT_CHUNK_SIZE)  + 2;    // Number of chunks in the input buffer
const uint32_t OUTPUT_NUM_CHUNKS = (CMD_MAX_RTERM_RESP_LENGTH / OUTPUT_CHUNK_SIZE) + 2;    // Number of chunks in the output buffer



// Types

// Circular buffer for serial data

struct SerialBuffer
{
    uint8_t *buffer;        // Pointer to start of memory allocated for this buffer

    uint32_t chunk_size;    // Chunk size for this buffer
    uint32_t num_chunks;    // Number of chunks for this buffer
    uint32_t start;         // First chunk in use
    uint32_t end;           // One after the last chunk in use
};



// Struct containing global variables

struct Serial
{
    // Data storage for all serial buffers. Buffer sizes are an integer number of chunks,
    // so we can always read or write one chunk at a time without overflow

    uint8_t input_buffers [INPUT_NUM_CHUNKS  * INPUT_CHUNK_SIZE  * FGCD_MAX_EQP_DEVS];
    uint8_t output_buffers[OUTPUT_NUM_CHUNKS * OUTPUT_CHUNK_SIZE * FGCD_MAX_EQP_DEVS];

    // Management of input and output buffers for each device

    SerialBuffer input [FGCD_MAX_EQP_DEVS];
    SerialBuffer output[FGCD_MAX_EQP_DEVS];
};

struct Serial serial = {};



// Static functions

/*
 * (Re-)initialise a serial buffer
 */

static void serialBufferReset(struct SerialBuffer *serial_buffer, uint8_t *buffer, uint32_t channel, uint32_t chunk_size, uint32_t num_chunks)
{
    serial_buffer->buffer     = buffer + ((channel-1) * chunk_size * num_chunks);
    serial_buffer->chunk_size = chunk_size;
    serial_buffer->num_chunks = num_chunks;
    serial_buffer->start      = 0;
    serial_buffer->end        = 0;
}



/*
 * Read a chunk from a buffer
 *
 * @returns Number of bytes read
 */

static uint32_t serialBufferRead(struct SerialBuffer *serial_buffer, uint8_t *output)
{
    if(serial_buffer->start == serial_buffer->end)
    {
        // Buffer is empty -- return zeros

        memset(output, 0, serial_buffer->chunk_size);

        return 0;
    }

    memcpy(output, serial_buffer->buffer + (serial_buffer->start * serial_buffer->chunk_size), serial_buffer->chunk_size);

    serial_buffer->start = (serial_buffer->start + 1) % serial_buffer->num_chunks;

    return serial_buffer->chunk_size;
}



/*
 * Write a chunk to a buffer
 *
 * @returns Number of bytes written
 */

static uint32_t serialBufferWrite(struct SerialBuffer *serial_buffer, const uint8_t *input)
{
    if((serial_buffer->start == 0 && serial_buffer->end == serial_buffer->num_chunks) ||
       (serial_buffer->start  > 0 && serial_buffer->end == serial_buffer->start - 1))
    {
        // Buffer is full, don't write anything

        return 0;
    }

    memcpy(serial_buffer->buffer + (serial_buffer->end * serial_buffer->chunk_size), input, serial_buffer->chunk_size);

    serial_buffer->end = (serial_buffer->end + 1) % serial_buffer->num_chunks;

    return serial_buffer->chunk_size;
}



// External functions

void serialReset(uint8_t channel)
{
    serialBufferReset(&serial.input [channel-1], serial.input_buffers,  channel, INPUT_CHUNK_SIZE,  INPUT_NUM_CHUNKS);
    serialBufferReset(&serial.output[channel-1], serial.output_buffers, channel, OUTPUT_CHUNK_SIZE, OUTPUT_NUM_CHUNKS);
}



void serialWriteRsp(uint8_t *output)
{
    for(uint8_t device_id = 0; device_id < FGCD_MAX_EQP_DEVS; ++device_id)
    {
        serialBufferRead(&serial.output[device_id], output);

        output += OUTPUT_CHUNK_SIZE;
    }
}



uint32_t serialReadCmd(uint8_t channel, const uint8_t *input)
{
    if(serialBufferWrite(&serial.input[channel-1], input) != 0)
    {
        // For hardware debugging: loop the serial input back to the serial output

        // Copy the data we just wrote from the input buffer into the output buffer

        uint8_t tmp_buffer[OUTPUT_CHUNK_SIZE * 3] = {};

        serialBufferRead(&serial.input[channel-1], tmp_buffer);

        // NB if the output buffer is full these calls will fail silently and we will lose data

        serialBufferWrite(&serial.output[channel-1], tmp_buffer);                        // write 4 bytes into the output buffer
        serialBufferWrite(&serial.output[channel-1], tmp_buffer + OUTPUT_CHUNK_SIZE);    // write 4 bytes into the output buffer

        // Write 3 packets of filler for testing. Last packet is 3 bytes and a zero.

        memset(tmp_buffer, 0, OUTPUT_CHUNK_SIZE * 3);
#if 0
        strcpy(reinterpret_cast<char*>(tmp_buffer), "\xAA\xAA\xAA\xAA\xBB\xBB\xBB\xBB\xCC\xCC\xCC");
#endif
        serialBufferWrite(&serial.output[channel-1], tmp_buffer);                            // write 4 bytes into the output buffer
        serialBufferWrite(&serial.output[channel-1], tmp_buffer + OUTPUT_CHUNK_SIZE);        // write 4 bytes into the output buffer
        serialBufferWrite(&serial.output[channel-1], tmp_buffer + OUTPUT_CHUNK_SIZE * 2);    // write 4 bytes into the output buffer

        return INPUT_CHUNK_SIZE;
    }

    return 0;
}

// EOF
