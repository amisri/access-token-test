#!/bin/bash
#
# @file    example_tests.sh
# @brief   Example of automated testing using FGClitemon
# @author  Michael Davis
#
# Usage: example_tests.sh [address]

FGCLITEMON=~pclhc/bin/$(uname -s)/$(uname -m)/fgclitemon

# critical_data is an array with the following elements:
#   critical_data[0]     V_MEAS_0_9                        
#   critical_data[1]     V_MEAS_10_19                      
#   critical_data[2]     I_A_0_9                           
#   critical_data[3]     I_A_10_19                         
#   critical_data[4]     I_B_0_9                           
#   critical_data[5]     I_B_10_19                         
#   critical_data[6]     DIM_A_TRIG_LAT                    
#   critical_data[7]     DIM_A_TRIG_UNL                    
#   critical_data[8]     DIM_A_1_ANA_0                     
#   critical_data[9]     DIM_A_1_ANA_1                     
#   critical_data[10]    DIM_A_1_ANA_2                     
#   critical_data[11]    DIM_A_1_ANA_3                     
#   critical_data[12]    CYCLE_PERIOD                      
#   critical_data[13]    VERSION                           
#   critical_data[14]    ADC_LOG_INDEX                     
#   critical_data[15]    DIM_LOG_INDEX                     
#   critical_data[16]    CONVERTER_INPUT                   
#   critical_data[17]    CONVERTER_OUTPUT                  
#   critical_data[18]    SEU_COUNT                         
#   critical_data[19]    CONTROLLER_STATUS                 
#   critical_data[20]    BACKPLANE_TYPE                    
#   critical_data[21]    SERIAL_DATA        

run_tests()
{
  # Do a soft reset of the FGClite

  send_cmd SOFT_RESET
  sleep 2

  # Check the value of OW_SCAN_BUSY

  OW_SCAN_BUSY=$(( $(get_status 19) & 2048 ))
  echo Before scan: $OW_SCAN_BUSY

  # Start OW scan of bus 4 (local)

  send_cmd UNSET_CTRL 15
  send_cmd SET_CTRL 12
  sleep 1

  # Check the value of OW_SCAN_BUSY

  OW_SCAN_BUSY=$(( $(get_status 19) & 2048 ))
  echo During scan: $OW_SCAN_BUSY

  # Stop OW scan

  send_cmd UNSET_CTRL 15
  sleep 1

  # Check the value of OW_SCAN_BUSY

  OW_SCAN_BUSY=$(( $(get_status 19) & 2048 ))
  echo After scan: $OW_SCAN_BUSY

  # Log the results of the scan (takes 16 cycles)

  send_cmd ADC_INDEX_TYPE 1 
  send_cmd SLEEP 0 320

  # Take the 3 ADC channels out of reset

  send_cmd SET_CONV 1792

  # Enable calibration on all 3 channels

  send_cmd SET_CONV 14336

  # Set calibration source to +V_REF

  send_cmd SET_CAL_SRC 1

  # Start logging V_MEAS

  send_cmd ADC_INDEX_TYPE 4

  # Create a triangular wave

  for iter in {1..5}
  do
    for val in {0..31744..1024} {30720..-31744..1024} {-30720..0..1024}
    do
       # Send all points with a one-cycle delay between them
       send_cmd SET_VREF $val
       send_cmd SLEEP 0 20
    done
  done

  # Stop logging

  send_cmd ADC_INDEX_TYPE 6

  # Wait for logging to finish, save the logfile for offline analysis

  sleep 13
  cp paged_adc_v_meas.csv triangular_wave.csv
}

send_cmd()
{
  echo $* >/tmp/fgclite_command_pipe
}

get_status()
{
  declare -a critical_data
  critical_data=( $(cat /tmp/fgclite_critical_data) )
  echo ${critical_data[$1]}
}

error()
{
  echo "$*" >&2
  exit 1
}

# Check that fgclitemon binary is present

[ -x $FGCLITEMON ] || error "$FGCLITEMON not found or not executable."

# Check that FGCD is not running

if [ ! -z "$(ps ax | grep fgcd | grep -v grep)" ]
then
  ps ax | grep fgcd | grep -v grep
  error "The FGCD process is running. Shut down the FGCD and try again."
fi

# Start fgclitemon in the background

sudo $FGCLITEMON $* 2>stderr.log &

# Run tests

run_tests 2>&1 >test.log

# Shut down fgclitemon

send_cmd QUIT

# Reset the terminal

reset

# EOF
