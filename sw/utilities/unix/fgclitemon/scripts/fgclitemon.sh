#!/bin/bash
#
# @file    fgclitemon.sh
# @brief   Start fgclitemon as superuser
# @author  Michael Davis
#
# Usage: fgclitemon.sh [address]

error()
{
  echo "$*" >&2
  exit 1
}

# Check that the CERN MasterFip is not running

#mfip_not_present=`echo 'stats, s 1' | sudo /usr/local/bin/masterfip-diag 2>&1 | grep Failed`
#mfip_cycle_counter1=`echo 'stats, s 1' | sudo /usr/local/bin/masterfip-diag | grep cycles_counter`
#sleep 1
#mfip_cycle_counter2=`echo 'stats, s 1' | sudo /usr/local/bin/masterfip-diag | grep cycles_counter`
FGCLITEMON=~pclhc/bin/$(uname -s)/$(uname -m)/fgclitemon

# Check that fgclitemon binary is present

[ -x $FGCLITEMON ] || error "$FGCLITEMON not found or not executable."

# Check that FGCD is not running

if [ ! -z "$(ps ax | grep fgcd | grep -v grep)" ]
then
  ps ax | grep fgcd | grep -v grep
  error "The FGCD process is running. Shut down the FGCD and try again."
fi

# Check we are running as root

[ "$(id -u)" == "0" ] || error "$0 must be run as root"

# Start fgclitemon

$FGCLITEMON $*

# EOF
