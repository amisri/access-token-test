/*!
 * @file   rt.h
 * @brief  Stub version of rt.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef RT_H
#define RT_H

#include <fgc_rtdata.h>

// Constants

#define RT_BUFFER_SIZE      2

/*!
 * Struct containing global variables
 */

struct Rt
{
    struct fgc_udp_rt_data  data[RT_BUFFER_SIZE];           //!< Real-time data
    uint32_t                data_start;                     //!< Real-time data start index
    struct fgc_udp_rt_data  used_data;                      //!< Last real-time data that was used
};

struct Rt rt;

#endif

// EOF
