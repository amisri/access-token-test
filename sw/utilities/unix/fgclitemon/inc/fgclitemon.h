/*!
 * @file   fgclitemon.h
 * @brief  Utility for testing and calibrating FGClite hardware and firmware
 * @author Michael Davis
 *
 * Use the separate program test_diamon to monitor Diamon status while fgclitemon is running.
 */

#ifndef __FGCLITEMON_H
#define __FGCLITEMON_H

#include <fstream>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>
#include <pthread.h>



// Constants

const uint8_t     DEFAULT_DEVICE_ID            = 30;                              //!< Default WorldFIP address of the device to monitor
const char* const DEFAULT_TEST_SCRIPT_FILENAME = "fgclite.test";                  //!< Default test script filename
const char* const LOG_DIR                      = "./";                            //!< Directory in which to store logfiles

const char* const CRITICAL_FILE                = "/tmp/fgclite_critical_data";    //!< File memory mapped to critical data
const char* const COMMAND_PIPE                 = "/tmp/fgclite_command_pipe";     //!< Named pipe to receive commands

const uint32_t    MEMMAP_FILE_SIZE             = 4096;                            //!< Maximum size of the memmapped file. 4096 bytes = 1 page.



// Types

struct FGClitemon
{
    bool              curses_started;    //!< Flag to initialise Ncurses
    bool              do_quit;           //!< Flag to terminate program
    pthread_t         file_thread;       //!< Command processing thread (script file)
    pthread_t         pipe_thread;       //!< Command processing thread (named pipe)

    uint8_t           device_id;         //!< WorldFIP address of the FGClite device
    const char       *script_filename;   //!< Filename for test script
    char              command[256];      //!< Command currently being executed
    struct timeval    user_time;         //!< Current cycle time

    // Input and output streams

    std::ifstream     test_script;       //!< Test script file stream
    std::ifstream     command_pipe;      //!< Named pipe stream to accept commands

    std::ofstream     error_log;         //!< Logfile for fgclitemon errors
    std::ofstream     critical_log;      //!< Logfile for ADC critical data
    std::ofstream     paged_log;         //!< Logfile for paged data

    int               critical_fd;       //!< File descriptor for memory mapping critical data
    void             *critical_data;     //!< Pointer to memory region for critical data

    // Logging of critical and paged data.
    //
    // We accumulate 1000 24-bit samples to calculate the average at 1 Hz, so we need
    // a 64-bit accumulator to not lose precision.

    double            critical_v_meas;   //!< Aggregated critical data for ADC V_MEAS
    double            critical_i_a;      //!< Aggregated critical data for ADC I_A
    double            critical_i_b;      //!< Aggregated critical data for ADC I_B

    bool              log_adc_data;      //!< Flag to disable ADC logging until the next 1 Hz boundary
    uint32_t          cycle_count;       //!< Count of no. of cycles we have accumulated
    int16_t           index;             //!< Current ADC_LOG_INDEX for paged data
    bool              inc_index;         //!< Whether or not to increment the index for paged data on each cycle

    // Initialise values in this struct

    FGClitemon() :
        curses_started(false),
        do_quit(false),
        critical_fd(0),
        critical_data(NULL),
        critical_v_meas(0),
        critical_i_a(0),
        critical_i_b(0),
        log_adc_data(false),
        cycle_count(0),
        index(0),
        inc_index(false)
    {
        device_id       = DEFAULT_DEVICE_ID;
        script_filename = DEFAULT_TEST_SCRIPT_FILENAME;
        command[0]      = '\0';
    }
};

extern struct FGClitemon fgclitemon;



#ifdef __cplusplus
extern "C" {
#endif

/*
 * Write a time to the log in human-readable format
 */

void logTime(const time_t &s);

#ifdef __cplusplus
}
#endif

#endif

// EOF
