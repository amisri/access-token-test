/*!
 * @file   pm.h
 * @brief  Stub version of pm.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef PM_H
#define PM_H

inline void pmBufferFreeze(uint8_t channel)
{
    // Does nothing in FGClitemon
}

#endif

// EOF
