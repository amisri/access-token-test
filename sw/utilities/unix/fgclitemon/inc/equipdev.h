/*!
 * @file   equipdev.h
 * @brief  Stub version of equipdev.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef EQUIPDEV_H
#define EQUIPDEV_H

#include <92_stat.h>

#ifndef FGC_FLT_COMMS
#define FGC_FLT_COMMS 1
#endif

#include <consts.h>

//! Struct for VS properties

struct Equipdev_channel_vs
{
    uint16_t                        sim_intlks;             //!< VS.SIM_INTLKS property
};

/*!
 * Struct for DEVICE properties
 */

struct Equipdev_channel_device
{
    struct timeval                  pwr_time;               //!< DEVICE.PWR_TIME property.
    uint8_t                         pwrcyc_count;           //!< DEVICE.PWRCYC_COUNT property.
    uint8_t                         pwrcyc_reason;          //!< DEVICE.PWRCYC_REASON property.
    uint32_t                        fpga_version[4];        //!< DEVICE.VERSION.FPGA property
};

/*!
 * Struct for FGC properties
 */

struct Equipdev_channel_fgc
{
    uint32_t        cycle_period_raw;                       //!< FGClite raw CYCLE_PERIOD (from the critical status)
    uint32_t        cycle_period_filtered;                  //!< FGClite filtered CYCLE_PERIOD
    uint16_t        ms_period;                              //!< MS_PERIOD sent to the device
    uint16_t        ms_period_override;                     //!< Override value for MS_PERIOD (set to zero to calculate from CYCLE_PERIOD)
};

//! Data for an equipment device channel

struct Equipdev_channel
{
    struct fgc92_stat               status;                 //!< Published status
    uint16_t                        sector_access;          //!< Sector access flag
    uint16_t                        crate_type;             //!< CRATE.TYPE property. Type of electronics
                                                            //!< crate in which the FGClite is inserted.
    uint16_t                        crate_position;         //!< CRATE.POSITION property. Position of the
                                                            //!< FGClite in the electronics crate.
    struct Equipdev_channel_vs      vs;                     //!< VS properties
    struct Equipdev_channel_device  device;                 //!< DEVICE properties
    struct Equipdev_channel_fgc     fgc;                    //!< FGC properties
    struct FGCD_device             *fgcd_device;
};

//! Struct containing global variables

struct Equipdev
{
    bool gw_pc_permit;

    struct Equipdev_channel device   [FGCD_MAX_DEVS];       //!< Data for equipment devices

    uint32_t                deadline [FGCD_MAX_DEVS+1];     //!< Deadline in ns within each cycle to receive the status packet
                                                            //!< for all FGClite devices + the diagnostic device
};

extern struct Equipdev equipdev;

// Stub functions

inline void fgcliteRegResetDevice(uint8_t channel)
{

}

inline void equipdevPublish(void)
{

}

inline int32_t pagedInit(void)
{
    return 0;
}

static inline void setStatusBit(uint16_t *status, uint16_t mask, bool value)
{
    if(value)
    {
        *status |= mask;
    }
    else
    {
        *status &= ~mask;
    }
}

static inline bool getStatusBit(uint16_t status, uint16_t mask)
{
    return status & mask;
}

#endif

// EOF
