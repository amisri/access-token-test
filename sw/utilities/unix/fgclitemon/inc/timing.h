/*!
 * @file   timing.h
 * @brief  Stub version of timing.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef TIMING_H
#define TIMING_H

#undef Boolean
extern "C" {
#include <tim/TimLib.h>
}

inline int32_t timingReadTime(struct timeval *time)
{
    TimLibTime timlib_time;

    TimLibGetTime(1, &timlib_time);

    time->tv_sec    = timlib_time.Second;
    time->tv_usec   = timlib_time.Nano / 1000;

    return 0;
}

/*!
 * Increment or decrement a timeval by another timeval.
 *
 * This function is declared inline as it is used within FIP callbacks, which can pre-empt the
 * kernel, so we would like to keep the function in the same compilation unit to avoid the overhead
 * of a function call.
 *
 * tv_second is added to tv_first, adjusting tv_sec and tv_usec if we cross a second boundary. If
 * tv_second is negative, it is subtracted; however, tv_first->tv_sec must be greater than
 * -(tv_second->tv_sec), as the number of seconds is unsigned. The number of microseconds is
 * signed.
 *
 * @param[in,out]    tv_first     timeval to increment or decrement
 * @param[in]        tv_second    timeval to add to tv_first. Can be negative.
 */

inline void timevalAdd(struct timeval *tv_first, const struct timeval* const tv_second)
{
    tv_first->tv_sec  += tv_second->tv_sec;
    tv_first->tv_usec += tv_second->tv_usec;

    // Check if we crossed a second boundary

    if(tv_first->tv_usec > 999999)
    {
        tv_first->tv_sec  += tv_first->tv_usec / 1000000;
        tv_first->tv_usec %= 1000000;
    }

    // Ensure usec field is positive after a subtraction

    while(tv_first->tv_usec < 0)
    {
        tv_first->tv_sec  -= 1;
        tv_first->tv_usec += 1000000;
    }
}

void timingSetUserTime(uint32_t user, struct timeval *time);

void timingGetUserTime(uint32_t user, struct timeval *time);

#endif

// EOF
