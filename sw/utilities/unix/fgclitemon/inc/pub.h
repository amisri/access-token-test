/*!
 * @file   pub.h
 * @brief  Stub version of pub.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef PUB_H
#define PUB_H

#include <netinet/in.h>

// Published data structure
// Note that struct FGC_udp_pub_data in fgc_pubdata.h must mirror this structure

struct Pub_data
{
    uint32_t                time_sec;
    uint32_t                time_usec;
};

// Struct containing global variables

struct Pub
{
    struct Pub_data         published_data;                         // Published data
};

extern struct Pub pub;

inline int32_t pubTrigger(void)
{
    return 0;
}

#endif

// EOF
