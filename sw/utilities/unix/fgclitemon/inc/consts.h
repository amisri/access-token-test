/*!
 * @file   consts.h
 * @brief  Stub version of consts.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef CONSTS_H
#define CONSTS_H

#define FGCD_MAX_EQP_DEVS 30
#define FGCD_MAX_DEVS     31

#endif

// EOF
