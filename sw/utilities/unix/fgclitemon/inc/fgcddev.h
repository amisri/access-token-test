/*!
 * @file   fgcddev.h
 * @brief  Stub version of fgcddev.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef FGCDDEV_H
#define FGCDDEV_H

#include <consts.h>
#include <6_stat.h>
#include <fgc_stat_consts.h>

#ifndef FGC_CTRL_ENABLED
#define FGC_CTRL_DISABLED 0
#define FGC_CTRL_ENABLED  1
#endif

// Struct containing global variables

struct FGCD_device
{
    bool online;
    bool ready;

    char *name;
};

struct FGCDdev
{
    struct FGCD_device device;
    struct fgc6_stat   status;
};

extern struct FGCDdev fgcddev;

inline void fgcddevPublish(void)
{

}

#endif

// EOF
