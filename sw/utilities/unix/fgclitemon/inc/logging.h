/*!
 * @file   logging.h
 * @brief  Stub version of logging.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <stdarg.h>

inline int32_t logPrintf(uint32_t channel, const char *format, ...)
{
    va_list args;

    fprintf(stderr, "Channel %d: ", channel);

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    return 0;
}

#endif

// EOF
