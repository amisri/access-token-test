/*!
 * @file   fgcd.h
 * @brief  Stub version of fgcd.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef FGCD_H
#define FGCD_H

#include <consts.h>

#define FGC_OP_SIMULATION 2

// Data for each device

struct fgcd_device
{
    char *name;              // Device name
};

// Struct containing global variables

struct Fgcd
{
    struct fgcd_device              device[FGCD_MAX_DEVS];                                      // Device data
};

extern struct Fgcd fgcd;

#endif

// EOF
