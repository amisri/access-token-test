/*!
 * @file   cmdqmgr.h
 * @brief  Stub version of cmdqmgr.h to allow fgclitemon to compile
 * @author Michael Davis
 */

#ifndef CMDQMGR_H
#define CMDQMGR_H

#include <stdint.h>

const uint32_t CMD_MAX_LENGTH = 1000;
const uint32_t CMD_MAX_RTERM_RESP_LENGTH = 100;
const uint32_t FGCD_MAX_EQP_DEVS = 30;

#endif

// EOF
