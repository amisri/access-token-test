#include <cmath>
#include <stdexcept>

#include "math_funcs.h"


int Math::power(int base, int exponent)
{
    int result = 1;

    for (; exponent>0; exponent--)
    {
        result = result * base;
    }

    return result;
}

double Math::sqaureRoot(double arg)
{
    if (arg < 0)
    {
        throw std::runtime_error("Negative input for square root");
    }

    return std::sqrt(arg);
}

// EOF