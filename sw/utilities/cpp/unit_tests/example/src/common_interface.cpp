#include "common_interface.h"

//! Common interface to a shape
Shape::Shape(): m_width(0), m_height(0)
{
}

Shape::~Shape()
{
}

void Shape::setWidth(int width)
{
    if (width > 0)
    {
        m_width = width;
    }
}

int Shape::getWidth()
{
    return m_width;
}

void Shape::setHeight(int height)
{
    if (height > 0)
    {
        m_height = height;
    }
}

int Shape::getHeight()
{
    return m_height;
}

//! Implementation for a rectangle
double Rectangle::calculateArea()
{
    return m_width * m_height;
}

//! Implementation for a triangle
double Triangle::calculateArea()
{
    return 0.5 * m_width * m_height;
}

//! Implementation for an ellipse
double Ellipse::calculateArea()
{
    return 3.14159265359 * (m_width / 2.0) * (m_height / 2.0);
}


// EOF