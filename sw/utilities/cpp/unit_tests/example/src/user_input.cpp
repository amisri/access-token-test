#include <iostream>
#include "user_input.h"


std::string Multiply_input::multiply()
{
    std::string result;
    auto        params = readUserInput();

    for (int i = 0; i < params.first; ++i)
    {
        result.append(params.second);
    }

    return result;
}

std::pair<int, std::string> Multiply_input::readUserInput()
{
    int         number_of_times;
    std::string line;

    std::cin >> number_of_times;
    std::getline(std::cin, line);

    return std::make_pair(number_of_times, line);


}


// EOF