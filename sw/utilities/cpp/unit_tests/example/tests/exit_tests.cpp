// This directive might be useful to hide the "Segmentation fault" error that is expected to be printed in such exit tests.
// @hide_std_err

TEST_CRASH(testCrashing)
{
    ERROR() << "My user-additional info." << std::flush;
    int* abc = 0;

    *abc = 5;
}

// This will have the same effect as the above test
// Instead of writing "<0, >0" it can be simply written as "!0" - this is just to present how to pass a few conditions
TEST_EXIT(testExiting, <0, >0)
{
    int* abc = 0;
    *abc = 5;
}


// EOF