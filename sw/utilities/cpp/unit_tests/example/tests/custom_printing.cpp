class Some_class
{
    public:
        Some_class(const int& some_param) : m_some_param(some_param)
        {}

        int getParam()
        {
            return m_some_param;
        }

        bool operator==(const Some_class& other)
        {
            return m_some_param == other.m_some_param;
        }

    private:
        int m_some_param;
};

// This is how to print this Some_class
PRINT(Some_class)
{
    stream << object.getParam();
}

// Instances of Some_class
Some_class a(10);
Some_class b(10);

// The test
TEST(someClassEquality)
{
    EXPECT_EQ(a, b) + "This error will be added to the standard message";

    EXPECT_EQ(a, b) << "This error will replace the standard message";
}


// EOF