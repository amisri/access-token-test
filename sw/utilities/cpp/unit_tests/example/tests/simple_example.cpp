#include "math_funcs.h"

TEST(powerRegularInput)
{
    EXPECT_EQ(8, Math::power(2, 3));
    EXPECT_EQ(9, Math::power(3, 2));
    EXPECT_EQ(3125, Math::power(5, 5));
}

TEST(powerUnitInput)
{
    EXPECT_EQ(1, Math::power(1, 100));
}

TEST(powerNegativeInput)
{
    EXPECT_EQ(-8, Math::power(-2, 3));
    EXPECT_EQ(16, Math::power(-2, 4));
}

TEST(powerInvalidInput)
{
    EXPECT_EQ(1, Math::power(-2, -3));
}


// EOF