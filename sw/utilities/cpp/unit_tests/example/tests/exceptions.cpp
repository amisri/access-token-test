#include "math_funcs.h"

TEST(squareRootNormalInput)
{
    EXPECT_FLOAT_EQ(1, Math::sqaureRoot(1));
    EXPECT_FLOAT_EQ(3, Math::sqaureRoot(9));
    EXPECT_FLOAT_EQ(5, Math::sqaureRoot(25));

    // This should not throw any exception
    EXPECT_NO_THROW(Math::sqaureRoot(25));
}


//! One way to test exceptions is to use exceptions testing macros like in the function below
TEST(squareRootNegativeInput1)
{
    EXPECT_NO_THROW(Math::sqaureRoot(9));
    EXPECT_ANY_THROW(Math::sqaureRoot(-9));
    EXPECT_THROW(Math::sqaureRoot(-9), std::runtime_error);

    EXPECT_THROW(Math::sqaureRoot(-9), std::runtime_error) + "Custom errors are normally supported as well";

    // You can also test multiple-line code, using braces {}
    ASSERT_THROW(
    {
        auto input = -9;
        Math::sqaureRoot(input);
    }, std::runtime_error);
}

//! There is another way to test exception, more manual - this might be useful for more complex cases, for example
//! when you want to test the .what() message, etc.
//! The only thing to remember is to use ADD_FAILURE() and not FAIL() macro - as the FAIL() macro throws on its own.
TEST(squareRootNegativeInput2)
{
    try
    {
        Math::sqaureRoot(-9);
        ADD_FAILURE() << "The sqaureRoot function didn't throw an exception for negative input";
    }
    catch (std::runtime_error& e)
    {
    }
    catch (...)
    {
        ADD_FAILURE() << "The sqaureRoot function threw an incorrect exception for negative input";
    }
}


// EOF