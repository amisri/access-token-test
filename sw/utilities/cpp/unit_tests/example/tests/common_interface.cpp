// @variant 1:impl_class:Rectangle
// @variant 1:shape_area:100

// @variant 2:impl_class:Triangle
// @variant 2:shape_area:50

// @variant 3:impl_class:Ellipse 
// @variant 3:shape_area:78.5398

// @show_std_out

#include "common_interface.h"


Shape* shape;

BEFORE(before)
{
    shape = new impl_class();
}


TEST(settingWidth)
{
    EXPECT_EQ(0, shape->getWidth());

    shape->setWidth(10);
    EXPECT_EQ(10, shape->getWidth());

    shape->setWidth(9999);
    EXPECT_EQ(9999, shape->getWidth());
}


TEST(settingNegativeWidth)
{
    shape->setWidth(100);
    shape->setWidth(-5);
    EXPECT_EQ(100, shape->getWidth());
}


TEST(settingHeight)
{
    EXPECT_EQ(0, shape->getHeight());

    shape->setHeight(10);
    EXPECT_EQ(10, shape->getHeight());

    shape->setHeight(9999);
    EXPECT_EQ(9999, shape->getHeight());
}


TEST(settingNegativeHeight)
{
    shape->setHeight(100);
    shape->setHeight(-5);
    EXPECT_EQ(100, shape->getHeight());
}


TEST(areaCalculation)
{
    shape->setWidth(10);
    shape->setHeight(10);
    EXPECT_FLOAT_EQ(shape_area, shape->calculateArea());
}


AFTER(after)
{
    delete shape;
}


// EOF