// @hide_std_err

#include "user_input.h"
#include <vector>

//! Global objects to store mocking params
int         number_of_times;
std::string line;

//! Mock the user input function
std::pair<int, std::string> Multiply_input::readUserInput()
{
    return std::make_pair(number_of_times, line);
}

//! Multiply object
Multiply_input multiply_input;

TEST(multiply)
{
    // First test
    number_of_times = 5;
    line            = "A";
    EXPECT_EQ("AAAAA", multiply_input.multiply());

    // Second test
    number_of_times = 3;
    line            = "A a";
    EXPECT_EQ("A aA aA a", multiply_input.multiply());
}

TEST(multiplyZero)
{
    number_of_times = 0;
    line            = "TEST";
    EXPECT_EQ("", multiply_input.multiply()) << "Multiplying by zero doesn't give empty string at line `" << __LINE__ << "`";
}


// EOF