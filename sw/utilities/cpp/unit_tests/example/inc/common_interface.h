#pragma once

//! Common interface to a shape
class Shape
{
    public:
        Shape();

        virtual ~Shape();

        void setWidth(int width);

        int getWidth();

        void setHeight(int height);

        int getHeight();

        virtual double calculateArea() = 0;

    protected:
        int m_width;
        int m_height;
};


//! Implementation for a rectangle
class Rectangle : public Shape
{
    public:
        double calculateArea() override;
};


//! Implementation for a triangle
class Triangle : public Shape
{
    public:
        double calculateArea() override;
};


//! Implementation for an ellipse
class Ellipse : public Shape
{
    public:
        double calculateArea() override;
};


// EOF