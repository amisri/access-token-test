#pragma once

#include <utility>
#include <string>

class Abc;

class Multiply_input
{

    public:
        //! Multiplies user input number of times specified by the user
        std::string multiply();

    private:
        //! Reads int and a line from the user console
        std::pair<int, std::string> readUserInput();


};


// EOF