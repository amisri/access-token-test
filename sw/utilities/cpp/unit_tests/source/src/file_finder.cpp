//! @file   file_finder.cpp
//! @brief  A function that prepares list of files with tests.
//! @author Dariusz Zielinski

// ---------- Includes
#include <algorithm>
#include <filesystem>
#include <iostream>

#include "../inc/file_finder.h"
#include "../inc/options.h"

namespace fs = std::filesystem;

std::vector<Test_file_ptr> File_finder::findTestFiles()
{
    // First check if there are user specified tests paths.
    // If not, then add root test as the only path (and the rest of the code will stay the same)
    if (Options::test_paths.empty())
    {
        Options::test_paths.push_back(Options::source);
    }

    std::vector<std::string> file_list;

    // Iterate over path list and get all the *.c or *.cpp files in there.
    while (!Options::test_paths.empty())
    {
        // Pop the element from the vector
        auto path = Options::test_paths[0];
        Options::test_paths.erase(Options::test_paths.begin());

        // Check if the path if a file
        if (fs::is_regular_file(path))
        {
            // If that case add it straight to the list
            file_list.push_back(path);
        }
        else
        {
            // Otherwise, list everything in the directory
            for (const auto &entry : fs::directory_iterator(path))
            {
                if (fs::is_directory(entry))
                {
                    //In case of directory push it to the test_paths
                    Options::test_paths.push_back(entry.path().string());
                }
                else
                {
                    auto ext = entry.path().extension().string();

                    if (fs::is_regular_file(entry) && (ext == ".cpp" || ext == ".c"))
                    {
                        // If the file is *.cpp or *.c, then add it to the list
                        file_list.push_back(entry.path().string());
                    }
                }
            }
        }
    }

    // Sort the vector to get the test in order
    sort(file_list.begin(), file_list.end());

    // Create vector of Test_file from the paths
    std::vector<Test_file_ptr> test_files;

    for (const auto& path : file_list)
    {
        test_files.emplace_back(std::make_shared<Test_file>(path));
    }

    return test_files;
}


//************************************************************

std::vector<Object_file> File_finder::findObjectFiles()
{
    std::vector<Object_file> objects;
    std::vector<std::string> dirs = {Options::pro_obj};

    // Iterate over path list and get all the files with Options::project_obj_ext
    while (dirs.empty() == false)
    {
        // Pop the element from the vector
        auto path = dirs[0];
        dirs.erase(dirs.begin());

        // List everything in the directory
        for (const auto &entry : fs::directory_iterator(path))
        {
            if (fs::is_directory(entry))
            {
                //In case of directory push it to the test_paths
                dirs.push_back(entry.path().string());
            }
            else
            {
                auto ext = entry.path().extension().string();

                if (fs::is_regular_file(entry) && (ext == ".o" || ext == ".a"))
                {
                    objects.emplace_back(Object_file(entry.path().string()));
                }
            }
        }
    }

    return objects;
}


// EOF