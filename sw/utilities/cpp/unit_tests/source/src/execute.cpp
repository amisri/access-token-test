//! @file   execute.cpp
//! @brief  Functions to run external programs.
//! @author Dariusz Zielinski

#include <sys/wait.h>

#include "../inc/execute.h"
#include "../inc/logging.h"


Exit_Program_Exception::Exit_Program_Exception(const int &exit_code_) : exit_code(exit_code_)
{
}

//************************************************************

void exit_program(const int &exit_code)
{
    throw Exit_Program_Exception(exit_code);
}

//************************************************************

int Exe::run(std::string command, int hide)
{
    command += createHideSuffix(hide);

    auto status = std::system(command.c_str());

    if (WIFEXITED(status))
    {
        // WEXITSTATUS make sense only when WIFEXITED returns true. See man for <sys/wait.h>
        return WEXITSTATUS(status);
    }
    else
    {
        // 99 is the arbitrary exit code. This is fine for the Exe::run use cases
        return 99;
    }
}

//************************************************************

void Exe::runCritical(std::string command, const std::string& error_msg, int hide)
{
    command += createHideSuffix(hide);

    if (std::system(command.c_str()) != 0)
    {
        log::error << error_msg << "\n";
        exit_program(2);
    }
}

//************************************************************

std::string Exe::createHideSuffix(int hide)
{
    std::string hide_suffix;

    if ((hide & hide_std_out) == hide_std_out)
    {
        hide_suffix = " > /dev/null";
    }

    if ((hide & hide_std_err) == hide_std_err)
    {
        hide_suffix += " 2> /dev/null";
    }

    return hide_suffix;
}


// EOF