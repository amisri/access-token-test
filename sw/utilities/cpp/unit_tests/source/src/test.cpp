//! @file   test.cpp
//! @brief  Classes to represent tests.
//! @author Dariusz Zielinski

// ---------- Includes
#include <exception>
#include <filesystem>
#include <map>
#include <set>
#include <sstream>
#include <thread>

#include "../inc/execute.h"
#include "../inc/file_templates.h"
#include "../inc/file_finder.h"
#include "../inc/logging.h"
#include "../inc/options.h"
#include "../inc/test.h"
#include "../inc/utils.h"


namespace fs = std::filesystem;

//************************************************************

constexpr auto lack_of_exit_error_msg = "The exit/crash test did not exit the program.";
constexpr int  valgrind_error_code    = 99;

//************************************************************

auto directives =
{
    Directive(Directive_type::test,                  R"(^[\s]*TEST\(([^\)]*)\))"),
    Directive(Directive_type::test_exit,             R"(^[\s]*TEST_EXIT\(([^\),]*),([^\)]*)\))"),
    Directive(Directive_type::test_exit_no_param,    R"(^[\s]*TEST_EXIT\(([^\),]*)\))"),
    Directive(Directive_type::test_crash,            R"(^[\s]*TEST_CRASH\(([^\)]*)\))"),
    Directive(Directive_type::before,                R"(^[\s]*BEFORE\(([^\)]*)\))"),
    Directive(Directive_type::after,                 R"(^[\s]*AFTER\(([^\)]*)\))"),
    Directive(Directive_type::varaint,               R"([\s]*\/\/[\s]*@variant[\s]*([^:]*):([^:]*):(.*))"),
    Directive(Directive_type::show_std_out,          R"([\s]*\/\/[\s]*@show_std_out)"),
    Directive(Directive_type::hide_std_err,          R"([\s]*\/\/[\s]*@hide_std_err)")
};

//************************************************************

Directive::Directive(Directive_type type, std::string pattern)
{
    m_type  = type;
    m_regex = std::regex(pattern);
}

//************************************************************

bool Directive::match(const std::string& line, std::smatch& match) const
{
    return std::regex_match(line, match, m_regex);
}

//************************************************************

Directive_type Directive::type() const
{
    return m_type;
}

//************************************************************

Object_file::Object_file(const std::string& source_path) : source(source_path)
{
    build = (fs::path(Options::build_dir_obj) / relativePath(source, Options::pro_obj)).string();
}

//************************************************************

Test::Test(const std::string &name) : m_name(name)
{
}

//************************************************************

std::string Test::name()
{
    return m_name;
}

//************************************************************

bool Test::isExitTest()
{
    return false;
}

//************************************************************

Regular_test::Regular_test(const std::string &name) : Test(name)
{

}

//************************************************************

bool Regular_test::checkIfPassed(const int &exit_code, std::vector<std::string>& report)
{
    return exit_code == 0;
}

//************************************************************

Exit_test::Rule_match::Rule_match(const std::string &regex, int groups, const std::function<Rule(int, int)>& func)
{
    m_regex      = std::regex(regex);
    m_groups     = groups;
    m_rule_func  = func;
}

//************************************************************

bool Exit_test::Rule_match::match(std::string input)
{
    std::smatch match;
    if (std::regex_match(input, match, m_regex))
    {
        if (m_groups == 2)
        {
            m_param1 = std::stoi(match[1]);
            m_param2 = std::stoi(match[2]);
        }
        else
        {
            m_param1 = std::stoi(match[1]);
        }

        return true;
    }
    else
    {
        return false;
    }
}

//************************************************************

Exit_test::Rule Exit_test::Rule_match::getRule()
{
    return m_rule_func(m_param1, m_param2);
}

//************************************************************

Exit_test::Exit_test(const std::string &name, const std::string &params) : Test(name), m_user_rules(params)
{
    std::vector<Rule_match> rules_matches =
    {
        Rule_match(R"(^[\s]*([-]?[\d]+)[\s]*$)"            , 1, [](int a, int b) { return [=](int code) { return code == a; }; }),
        Rule_match(R"(^[\s]*!([-]?[\d]+)[\s]*$)"           , 1, [](int a, int b) { return [=](int code) { return code != a; }; }),
        Rule_match(R"(^[\s]*([-]?[\d]+):([-]?[\d]+)[\s]*$)", 2, [](int a, int b) { return [=](int code) { return code >= a && code <= b; }; }),
        Rule_match(R"(^[\s]*<([-]?[\d]+)[\s]*$)"           , 1, [](int a, int b) { return [=](int code) { return code < a; }; }),
        Rule_match(R"(^[\s]*>([-]?[\d]+)[\s]*$)"           , 1, [](int a, int b) { return [=](int code) { return code > a; }; }),
    };

    auto params_vec = split(params, ',');

    for (const auto& param : params_vec)
    {
        bool found = false;
        for (auto& rule_match : rules_matches)
        {
            if (rule_match.match(param))
            {
                found = true;
                m_rules.push_back(rule_match.getRule());
            }
        }

        if (found == false)
        {
            throw std::runtime_error("The parameter (" + param + ") doesn't define a valid rule.");
        }
    }
}

//************************************************************

bool Exit_test::checkIfPassed(const int &exit_code, std::vector<std::string>& report)
{
    // Check that there is no message that the program did not return - that means that the test failed
    std::regex  not_exit_msg(std::string("^") + lack_of_exit_error_msg);
    std::smatch match;

    for (const auto& line : report)
    {
        if (std::regex_match(line, match, not_exit_msg))
        {
            return false;
        }
    }

    // Iterate over all rules and if at least one if ok, then return true (they are OR'ed).
    for (auto& rule : m_rules)
    {
        if (rule(exit_code))
        {
            return true;
        }
    }

    report.push_back("The exit code of the test is not as expected:");
    report.push_back("   The program returned `" + std::to_string(exit_code) + "`");
    report.push_back("   Expected rules: `" + m_user_rules + "`");

    return false;
}

//************************************************************

bool Exit_test::isExitTest()
{
    return true;
}

//************************************************************

Test_variant::Test_variant(int index, const std::string& test_file_name, const std::vector<Variant_case>& variant_cases)
{
    m_index          = index;
    m_test_file_name = test_file_name;
    m_variant_cases  = variant_cases;
}

//************************************************************

std::string Test_variant::getName() const
{
    return m_index == 0 ? "" : ":variant(" + std::to_string(m_index) + ")";

}

//************************************************************

std::string Test_variant::getSuffix() const
{
    return ext::variant + std::to_string(m_index);
}

//************************************************************

std::string Test_variant::getDescription(const bool& skip_colon) const
{
    if (m_index == 0)
    {
        return "";
    }
    else
    {
        std::stringstream desc;
        desc << " variant " << m_index << "(";

        for (unsigned int i = 0; i < m_variant_cases.size(); ++i)
        {
            desc << std::get<0>(m_variant_cases[i]) << " = " << std::get<1>(m_variant_cases[i]);

            if (i < m_variant_cases.size() - 1)
            {
                desc << ", ";
            }
        }

        if (skip_colon == false)
        {
            desc << "):";
        }

        return desc.str();
    }
}

//************************************************************

std::string Test_variant::getFile(const std::string& file, const std::vector<Test_ptr>& tests,
                                  const std::string& before, const std::string& after) const
{
    std::string processed_file(file);

    // Replace all variant cases
    for (const auto& variant_case : m_variant_cases)
    {
        // Replace each key (get<0>) with value (get<1>)
        replaceSubstringIn(processed_file, std::get<0>(variant_case), std::get<1>(variant_case));
    }

    // Generate switch cases for test firing
    std::stringstream cases;
    int               index = 0;

    for (const auto& test : tests)
    {
        cases << "case " << index << R"(: unit_tests::error_output_stream << "testing\n" << std::flush; )" << test->name() << "(); break; \n";
        index++;
    }

    // Generate the main() function
    std::string main = file_template::main;
    replaceSubstringIn(main, "_REPLACE_THIS_FILE_NAME_", m_test_file_name);
    replaceSubstringIn(main, "_REPLACE_SWITCH_CASES_",   cases.str());
    replaceSubstringIn(main, "_REPLACE_EXIT_ERROR_",     lack_of_exit_error_msg);
    replaceSubstringIn(main, "_REPLACE_BEFORE_INVOKE_",  before);
    replaceSubstringIn(main, "_REPLACE_AFTER_INVOKE_",   after);

    // Add the main() function at the end of the source file
    processed_file += main;

    return processed_file;
}


//************************************************************

Test_file::Test_file(std::string file_path)
{
    // Prepare file paths
    m_path_src_file   = file_path;
    m_path_build_file = Options::build_dir_src + "/" + relativePath(file_path, Options::root).string();

    // Get directory of those files
    m_path_src_dir    = fs::path(m_path_src_file).parent_path().string();
    m_path_build_dir  = fs::path(m_path_build_file).parent_path().string();

    // Extract file name
    m_name = relativePath(fs::path(file_path), Options::source).string();

    // Load and parse test file
    parse();
}

//************************************************************

Test_file::~Test_file()
{
    for (const auto& file : m_files_to_remove)
    {
        if (fs::exists(file))
        {
            Exe::run("rm " + file, Exe::hide_all);
        }
    }
}

//************************************************************

void Test_file::parse()
{
    // Load file to a string
    m_file = readFromFile(m_path_src_file);

    // Load file into lines
    auto file_lines = readLinesFromFile(m_path_src_file);

    // This will hold parsed variants for later validation
    std::vector<Parsed_variant> parsed_variants;

    // Go line by line to match directives
    for (const auto& line : file_lines)
    {
        for (const auto& directive : directives)
        {
            std::smatch match;
            if (directive.match(line, match))
            {
                switch (directive.type())
                {
                    case Directive_type::test:
                    case Directive_type::test_crash:
                    case Directive_type::before:
                    case Directive_type::after:
                        {
                            std::string func_name(match[1]);

                            if (func_name.empty() == false)
                            {
                                switch (directive.type())
                                {
                                    case Directive_type::test:
                                        m_tests.push_back(std::make_shared<Regular_test>(func_name));
                                        break;
                                    case Directive_type::test_crash:
                                        m_tests.push_back(std::make_shared<Exit_test>(func_name, "!0"));
                                        break;
                                    case Directive_type::before:
                                        m_func_before = func_name + "();";
                                        break;
                                    case Directive_type::after:
                                        m_func_after = func_name + "();";
                                        break;
                                    default:
                                        break;
                                }
                            }
                            else
                            {
                                log::error << "Missing name of a test/function declaration in a file: " << m_name
                                           << " at line: \n" << line << "\n";
                                exit_program(2);
                            }
                        }
                        break;

                    case Directive_type::test_exit:
                        if (match.size() == 3)
                        {
                            std::string func_name(match[1]);
                            std::string params(match[2]);

                            try
                            {
                                m_tests.push_back(std::make_shared<Exit_test>(func_name, params));
                            }
                            catch (std::exception& e)
                            {
                                log::error << "Wrong format of an exit test parameter (" << e.what() << ") in a file: " << m_name << " at line : \n" << line << "\n";
                                exit_program(2);
                            }
                        }
                        else
                        {
                            log::error << "Wrong format of an exit test declaration in a file: " << m_name << " at line : \n" << line << "\n";
                            exit_program(2);
                        }
                        break;

                    case Directive_type::test_exit_no_param:
                        log::error << "Empty parameter list for an exit test declaration in a file: " << m_name << " at line : \n" << line << "\n";
                        exit_program(2);
                        break;

                    case Directive_type::varaint:
                        if (match.size() == 4)
                        {
                            if (isInteger(match[1]) == false)
                            {
                                log::error << "First parameter of variant declaration is not a number in a file: " << m_name << "\n";
                                exit_program(2);
                            }

                            int index = std::stoi(match[1]);

                            if (index < 1)
                            {
                                log::error << "Variants index must start from 1 in a file: " << m_name << "\n";
                                exit_program(2);
                            }

                            parsed_variants.emplace_back(Parsed_variant(index, match[2], match[3]));
                        }
                        else
                        {
                            log::error << "Wrong format of a variant declaration in a file: " << m_name << "\n";
                            exit_program(2);
                        }
                        break;

                    case Directive_type::show_std_out:
                        m_show_std_out = true;
                        break;

                    case Directive_type::hide_std_err:
                        m_hide_std_err = true;
                        break;
                }
            }
        }
    }

    // Validate and further parse the parsed variants
    processParsedVariants(parsed_variants);

    // Now, after processParsedVariants, if the variant number is 0, then add 0-index (default) variant
    if (m_variants.empty())
    {
        m_variants.emplace_back(Test_variant(0, m_name));
    }
}


void Test_file::processParsedVariants(const std::vector<Parsed_variant> &parsed_variants)
{
    // First, split key-value pairs into variants groups
    using Group = std::vector<Variant_case>;
    std::map<int, Group> groups;

    for (const auto& parsed_variant : parsed_variants)
    {
        // Split parsed variant
        int  index = std::get<0>(parsed_variant);
        auto key   = std::get<1>(parsed_variant);
        auto value = std::get<2>(parsed_variant);

        // Get list for this index or create new if the index is new
        auto& group_list = groups[index];

        // Add the variant case to the list
        group_list.emplace_back(Variant_case(key, value));
    }

    // Because the map is already ordered, the keys are in the order.
    // So iterate over keys to check that none is missing
    int next_index = 1;

    for (const auto& group : groups)
    {
        if (group.first != next_index)
        {
            log::error << "Variant index " << next_index << " is missing in a file " << m_path_src_file << "\n";
            exit_program(2);
        }

        next_index++;
    }

    // Now create a set with all the keys
    std::set<std::string> keys;

    for (const auto& group : groups)
    {
        for (const auto& variant_case : group.second)
        {
            keys.insert(std::get<0>(variant_case));
        }
    }

    // And then use that set to check that each variant defines all keys
    for (const auto& group : groups)
    {
        for (const auto& key : keys)
        {
            // Iterate over cases and see if key is present
            bool found = false;
            for (const auto& variant_case : group.second)
            {
                if (std::get<0>(variant_case) == key)
                {
                    found = true;
                    break;
                }
            }

            if (found == false)
            {
                log::error << "Variant with index " << group.first << " does not define key " << key << " in a file: " << m_path_src_file << "\n";
                exit_program(2);
            }
        }
    }

    // Finally when everything is correct, create variants classes
    for (const auto& group : groups)
    {
        m_variants.emplace_back(Test_variant(group.first, m_name, group.second));
    }
}

//************************************************************

int Test_file::getVariantsCount()
{
    return m_variants.size();
}

//************************************************************

int Test_file::getTestCount()
{
    return m_tests.size() * m_variants.size();
}

//************************************************************

void Test_file::compile(const std::vector<Object_file>& objects, int& global_index, const int& test_file_count)
{
    // Create a folder for this test file
    Exe::runCritical("mkdir -p " + m_path_build_dir, "Failed to create a folder: " + m_path_build_dir);

    // Iterate over variants and compile them
    for (const auto& variant : m_variants)
    {
        runFunc(global_index, test_file_count, "Compiling test file", m_name, [&]()
        {
            // Prepare file names
            auto path_makefile     = m_path_build_file + variant.getSuffix() + ext::makefile;
            auto path_source_build = m_path_build_file + variant.getSuffix() + ext::source;
            auto path_source_src   = m_path_src_file   + variant.getSuffix() + ext::source;

            // Generate Makefile
            auto makefile = generateMakefile(objects, variant);

            // Write the makefile into file
            writeToFile(path_makefile, makefile);

            // Generate and write a source file for the variant
            writeToFile(path_source_build, variant.getFile(m_file, m_tests, m_func_before, m_func_after));

            // Copy the generated source file into source directory
            Exe::runCritical("cp " + path_source_build + " " + path_source_src, "Failed to copy test source.");

            // Add generated file to the list of files that will be removed at the program exit in case of GDB run
            if (Options::run_gdb)
            {
                m_files_to_remove.push_back(path_source_src);
            }

            // Otherwise, remove that at the end of this function
            Finally remove_copied_file([=]()
            {
                if (fs::exists(path_source_src) && Options::run_gdb == false)
                {
                    Exe::run("rm " + path_source_src, Exe::hide_all);
                }
            });

            // Compile the file
            Exe::runCritical("make -f " + path_makefile, "Compilation of test " + m_name + variant.getName() + " failed.");
        });
    }
}

//************************************************************

std::string Test_file::generateMakefile(const std::vector<Object_file>& objects, const Test_variant& variant)
{
    // Prepare file name
    std::string file = m_path_build_file + variant.getSuffix();

    // Load the Makefile template
    std::string makefile = file_template::makefile;

    // Replace in the Makefile
    replaceSubstringIn(makefile, "_REPLACE_TAB_",         "\t");
    replaceSubstringIn(makefile, "_REPLACE_CXX_",         Options::pro_cxx);
    replaceSubstringIn(makefile, "_REPLACE_OBJCOPY_",     Options::pro_objcopy);
    replaceSubstringIn(makefile, "_REPLACE_FLAGS_CXX_",   Options::pro_cxx_flags);
    replaceSubstringIn(makefile, "_REPLACE_LIBS_",        Options::pro_libs);

    replaceSubstringIn(makefile, "_REPLACE_EXE_",         file + ext::executable);
    replaceSubstringIn(makefile, "_REPLACE_OBJ_",         file + ext::object);
    replaceSubstringIn(makefile, "_REPLACE_API_FILE_",    Options::framework_api);
    replaceSubstringIn(makefile, "_REPLACE_INCLUDE_",     join(Options::pro_inc, " -I", " "));
    replaceSubstringIn(makefile, "_REPLACE_SRC_",         m_path_src_file);
    replaceSubstringIn(makefile, "_REPLACE_VARIANT_SRC_", m_path_src_file + variant.getSuffix() + ext::source);
    replaceSubstringIn(makefile, "_REPLACE_DEP_",         file + ext::depend);

    // Prepare list of project object files
    std::string build_obj_list;
    for (const auto& object : objects)
    {
        // Add pats to the lists
        build_obj_list += " " + object.build;

        makefile += "\n" + object.build + ":" + object.source + "\n";
        makefile += std::string("\t") + "@$(OBJCOPY) --weaken " + object.source + " " + object.build + "\n\n";
    }

    // Replace object file list
    replaceSubstringIn(makefile, "_REPLACE_PRO_OJBS_BUILD_", build_obj_list);

    return makefile;
}

//************************************************************

void Test_file::listAllTests() const
{
    // Print test file name
    log::brief << No_prefix()
    << "Test file: " << Colors::white << m_name << Colors::blue <<  " contains" << (m_tests.empty() ? " no tests." : ":") << "\n";

    // Print tests
    for (const auto& test : m_tests)
    {
        log::brief << No_prefix() << Colors::no_color << "   - " << test->name() << "\n";
    }

    // Print how many variants, if more than 1
    if (m_variants.size() > 1)
    {
        log::brief << No_prefix() << "with " << m_variants.size() << " variants.\n";
    }
}

//************************************************************

void Test_file::execute(Test_Stats& test_stats, int& global_index, const int& test_count)
{
    // For each variant, execute its program and pass test number
    for (const auto& variant : m_variants)
    {
        int test_index = 0;
        for (const auto& test : m_tests)
        {
            runFunc(global_index, test_count, "Running test", m_name + variant.getName() + ":" + test->name(), [&]()
            {
                // File names
                std::string output_file = m_path_build_file + variant.getSuffix() + ext::output;
                std::string test_file   = m_path_build_file + variant.getSuffix() + ext::executable;

                // Remove output file if present from the previous run
                if (fs::exists(output_file))
                {
                    Exe::run("rm " + output_file, Exe::hide_all);
                }

                // Prepare params
                auto coredump = std::string("ulimit -c ") + (Options::core_dumps ? "unlimited" : "0");

                std::stringstream params;
                params << '"' << output_file << '"' << " "
                       << std::to_string(test_index) << " "
                       << (test->isExitTest() ? "true" : "false") << " ";

                int output_hide  = Exe::hide_none;
                    output_hide |= (m_show_std_out ? Exe::hide_none    : Exe::hide_std_out);
                    output_hide |= (m_hide_std_err ? Exe::hide_std_err : Exe::hide_none);

                // Run the test
                int exit_code = Exe::run(coredump + " && \"" + test_file + "\" " + params.str(), output_hide);

                // Load the file with error description, if present
                std::vector<std::string> error_report;
                if (fs::exists(output_file))
                {
                    error_report = readLinesFromFile(output_file);
                }

                // Get first line of the report file and remove it
                std::string report_first_line;
                if (error_report.empty() == false)
                {
                    report_first_line = error_report[0];
                    error_report.erase(error_report.begin());
                }

                // Now, if any test has been run, then first line of the file should be equal to "testing"
                // Otherwise, it means an internal error (which should not happen).
                if (report_first_line == "testing")
                {
                    // Check status
                    if (test->checkIfPassed(exit_code, error_report))
                    {
                        // Update stats on success
                        test_stats.passed++;
                    }
                    else
                    {
                        // Update stats
                        test_stats.failed++;

                        // Print which test failed
                        log::error << No_prefix() << "\n";
                        log::error << "Test failed: " << m_name << ":" << variant.getDescription();
                        log::error << No_prefix() << (Options::no_colors ? "" : Colors::red_value)
                                   << test->name() << (Options::no_colors ? "" : Colors::red) << "\n";

                        bool first_line = true;

                        // Print error description with a front margin
                        bool opening = true;

                        for (const auto &line : error_report)
                        {
                            if (first_line && line.empty())
                            {
                                // Skip first line if it's empty
                                continue;
                            }
                            first_line = false;

                            // Convert ` (back-ticks) into colors formatting
                            std::string formatted_line;

                            // Check if the coloring is already open from the previous line
                            if (Options::no_colors == false)
                            {
                                formatted_line.append((!opening) ? Colors::red_value : Colors::red);
                            }

                            for (const auto &c : line)
                            {
                                if (Options::no_colors || c != '`')
                                {
                                    formatted_line.push_back(c);
                                }
                                else
                                {
                                    formatted_line.append(opening ? Colors::red_value : Colors::red);
                                    opening = !opening;
                                }
                            }

                            log::error << No_prefix() << "       " << formatted_line << "\n";
                        }
                    }
                }
                else
                {
                    // If exit_code is > 1 (or any other) then it is returned from the auto-generated main() function
                    // and this should normally never happen.
                    log::error << No_prefix() << "\n";
                    log::error << "Framework internal error (" << exit_code << ") while executing test: " << m_name << ":" << test->name() << ".\n";
                    exit_program(2);
                }
            });

            test_index++;

            if (test_stats.failed > 0 && Options::stop_at_fail)
            {
                return;
            }
        }
    }
}

//************************************************************

void Test_file::valgrind(Test_Stats &test_stats, int &global_index, const int &test_count)
{
    // For each variant, execute its program and pass test number
    for (const auto& variant : m_variants)
    {
        int test_index = 0;
        for (const auto& test : m_tests)
        {
            runFunc(global_index, test_count, "Running valgrind on test", m_name + variant.getName() + ":" + test->name(), [&]()
            {
                // In case of exit test, don't run Valgrind - exit test is usually a crash test and is very likely
                // to cause memory problems, but the crash is expected, so it would be impossible to tell if the problem  is really a problem.
                if (test->isExitTest())
                {
                    test_stats.skipped++;
                    return;
                }

                // File names
                std::string output_file   = m_path_build_file + variant.getSuffix() + ext::output;
                std::string valgrind_file = m_path_build_file + variant.getSuffix() + ext::valgrind;
                std::string test_file     = m_path_build_file + variant.getSuffix() + ext::executable;

                // Remove output files if present from the previous run
                if (fs::exists(output_file))
                {
                    Exe::run("rm " + output_file, Exe::hide_all);
                }

                if (fs::exists(valgrind_file))
                {
                    Exe::run("rm " + valgrind_file, Exe::hide_all);
                }

                // Valgrind command
                std::stringstream valgrind;
                valgrind << "valgrind " << " "
                         << Options::valgrind_flags << " "
                         << "--error-exitcode=" + std::to_string(valgrind_error_code) << " "
                         << "--log-file=" << '"' << valgrind_file << '"';

                // Run the test
                std::stringstream params;
                params << '"' << output_file << '"' << " "
                       << std::to_string(test_index) << " "
                       << (test->isExitTest() ? "true" : "false") << " ";

                int exit_code = Exe::run(valgrind.str() + " \"" + test_file + "\" " + params.str(), Exe::hide_all);

                if (exit_code == valgrind_error_code)
                {
                    test_stats.failed++;

                    // Print that the test failed
                    auto red       = (Options::no_colors ? "" : Colors::red);
                    auto red_value = (Options::no_colors ? "" : Colors::red_value);

                    log::error << No_prefix() << "\n";
                    log::error << "Valgrind detected problems in: " << red_value << m_name << red << ":" << variant.getDescription();
                    log::error << No_prefix() << red_value << test->name() << red << "\n";

                    // Load the error log
                    std::vector<std::string> error_report;
                    if (fs::exists(valgrind_file))
                    {
                        error_report = readLinesFromFile(valgrind_file);
                    }

                    // Define things to format
                    auto valgrind_keywords = { R"(([\d,]+ bytes))", R"(([\d,]+ blocks))", R"(([\d,]+ allocs))", R"(([\d,]+ frees))", R"(([\d,]+ errors))",
                                              "(HEAP SUMMARY:)", "(LEAK SUMMARY:)", "(ERROR SUMMARY:)", "(errors)"};

                    auto valgrind_skip_lines = {"Memcheck, a memory error detector", "Copyright (C)", "rerun with -h for copyright info",
                                                test_file.c_str(), "Parent PID"};

                    std::regex  skip_pid(R"((==\d+==)(.*))");
                    std::smatch smatch;

                    // Format and print the error log
                    // Print error description with a front margin
                    for (auto &line : error_report)
                    {
                        bool skip = false;

                        // Check if this line should be skipped
                        for (const auto& skip_line : valgrind_skip_lines)
                        {
                            if (line.find(skip_line, 0) != std::string::npos)
                            {
                                skip = true;
                                break;
                            }
                        }

                        if (skip)
                        {
                            continue;
                        }

                        // Remove ==PID== from the line
                        if (std::regex_match(line, smatch, skip_pid))
                        {
                            line = smatch[2];
                        }

                        // Colorize keywords
                        for (const auto& keyword : valgrind_keywords)
                        {
                            std::regex regex(keyword);
                            line = std::regex_replace(line, regex, std::string(red_value) + "$1" + red);
                        }

                        log::error << No_prefix() << "   " << line << "\n";
                    }
                }
                else
                {
                    test_stats.passed++;
                }
            });

            test_index++;

            if (test_stats.failed > 0 && Options::stop_at_fail)
            {
                return;
            }
        }
    }
}

//************************************************************

void Test_file::runGdb()
{
    // Check if the specified variant is correct
    if (m_variants.size() == 1)
    {
        // If there is only one variant, then the variant index can be 0 or 1
        if (Options::run_gdb_variant != 0 && Options::run_gdb_variant != 1)
        {
            log::error << "This test has only one variant, but you selected to run variant " << Options::run_gdb_variant << ".\n";
            exit_program(2);
        }
    }
    else
    {
        // Convert variant number into variant index
        Options::run_gdb_variant = (Options::run_gdb_variant == 0 ? 0 : Options::run_gdb_variant - 1);

        // Check if that makes sense
        if (Options::run_gdb_variant >= m_variants.size())
        {
            log::error << "This test has only " << m_variants.size() << " variants, but you selected to run variant " << (Options::run_gdb_variant + 1) << ".\n";
            exit_program(2);
        }
    }

    // Check if the specified test name is correct
    int test_index_to_run = -1;

    for (unsigned int i = 0; i < m_tests.size(); ++i)
    {
        if (m_tests[i]->name() == Options::run_gdb_test)
        {
            test_index_to_run = i;
            break;
        }
    }

    if (test_index_to_run < 0)
    {
        log::error << "The specified test name '" << Options::run_gdb_test << "' has not been found within selected test file." << "\n";
        exit_program(2);
    }

    // Select correct variant
    const auto& variant = m_variants[Options::run_gdb_variant];

    // Run the test with GDB
    std::string output_file = m_path_build_file + variant.getSuffix() + ext::output;
    std::string test_file   = m_path_build_file + variant.getSuffix() + ext::executable;

    // Remove output file if present from the previous run
    if (fs::exists(output_file))
    {
        Exe::run("rm " + output_file, Exe::hide_all);
    }

    // Prepare params
    std::stringstream params;
    params << '"' << output_file << "\" "
           << std::to_string(test_index_to_run) << " "
           << (m_tests[test_index_to_run]->isExitTest() ? "true" : "false") << " ";

    // Print info
    log::success << No_prefix() << "\n";
    log::success << "Running gdb for " << m_name << ":" << variant.getDescription() << ":" << Options::run_gdb_test;
    log::brief   << No_prefix() << (Options::no_colors ? "\n" : Colors::no_color) << "\n";

    // Gdb command
    std::stringstream gdb_cmd;
    gdb_cmd << "gdb "
            << "-ex 'break " << Options::run_gdb_test << "' "
            << "--args \""   << test_file << "\" " << params.str();
    // Run the test with GDB
    Exe::run(gdb_cmd.str(), Exe::hide_none);
}

//************************************************************

void Test_file::runFunc(int &global_index, const int &count, const std::string& action, const std::string& name,
                        std::function<void()> func)
{
    // Info about current action
    log::info << action << " ";
    if (Options::no_colors == false)
    {
        log::brief_only << No_prefix() << action << " ";
    }

    log::brief << No_prefix() << "[" << global_index << " / " << count << "]";
    log::info  << No_prefix() << ": " << name << "   " << Colors::no_color;

    // Execute function
    func();

    // Increment test counters
    global_index++;

    // Reset the terminal line or print a new line
    log::info       << No_prefix() << (Options::no_colors ? "\n" : log::reset_line);
    log::brief_only << No_prefix() << (Options::no_colors ? ""   : log::reset_line);
}


// EOF