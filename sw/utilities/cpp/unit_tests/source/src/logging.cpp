//! @file   logging.cpp
//! @brief  Provides global objects, similar to std::cout, for printing.
//! @author Dariusz Zielinski

#include "../inc/logging.h"

// Global objects with loggers
// Level: normal
Logger log::text          ("",          Colors::white,    [](){ return !Options::errors_only && !Options::brief; });
Logger log::info          ("INFO: ",    Colors::blue,     [](){ return !Options::errors_only && !Options::brief; });
Logger log::success       ("INFO: ",    Colors::green,    [](){ return !Options::errors_only && !Options::brief; });
// Level: brief
Logger log::brief         ("INFO: ",    Colors::blue,     [](){ return !Options::errors_only;                    });
Logger log::brief_success ("INFO: ",    Colors::green,    [](){ return !Options::errors_only;                    });
Logger log::brief_only    ("INFO: ",    Colors::blue,     [](){ return !Options::errors_only && Options::brief;  });
Logger log::warning       ("WARNING: ", Colors::yellow,   [](){ return !Options::errors_only;                    });
// Level: errors only
Logger log::error         ("ERROR: ",   Colors::red,      [](){ return true;                                     });

//************************************************************

LoggerEntry::LoggerEntry(const std::function<bool()> &enabled)
{
    m_enabled = enabled;
}

LoggerEntry::~LoggerEntry()
{
    std::cout << Colors::no_color << std::flush;
}

//************************************************************

Logger::Logger(const std::string& prefix, const std::string& color, const std::function<bool()> &enabled)
{
    m_prefix  = prefix;
    m_color   = color;
    m_enabled = enabled;
}


// EOF