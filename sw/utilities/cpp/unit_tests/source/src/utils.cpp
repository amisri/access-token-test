//! @file   utils.cpp
//! @brief  Utilities.
//! @author Dariusz Zielinski

#include <algorithm>
#include <filesystem>

#include "../inc/utils.h"

namespace fs = std::filesystem;

std::string readFromFile(const std::string &file_name)
{
    std::ifstream input_stream(file_name);
    return std::string(std::istreambuf_iterator<char>(input_stream),
                       std::istreambuf_iterator<char>());
}

//************************************************************

std::vector<std::string> readLinesFromFile(const std::string &file_name)
{
    std::vector<std::string> result;

    std::ifstream input_stream(file_name);
    for (std::string line; std::getline(input_stream, line);)
    {
        result.push_back(line);
    }

    return result;
}

//************************************************************

void writeToFile(const std::string &file_name, const std::string &data)
{
    std::ofstream output_stream(file_name);
    output_stream << data;
}

//************************************************************

std::vector<std::string> split(const std::string &input, const char& delimiter)
{
    std::stringstream        stream(input);
    std::vector<std::string> result;

    while( stream.good() )
    {
        std::string sub_string;
        getline(stream, sub_string, delimiter);
        result.push_back( sub_string);
    }

    return result;
}

//************************************************************


void replaceSubstringIn(std::string &input, const std::string &toReplace, const std::string &replaceWith)
{
    size_t index = 0;
    while (true)
    {
        index = input.find(toReplace, index);
        if (index == std::string::npos)
        {
            break;
        }

        input.replace(index, toReplace.size(), replaceWith);
        index += replaceWith.size();
    }
}

std::string replaceSubstring(const std::string& input, const std::string &toReplace, const std::string &replaceWith)
{
    std::string copy = input;
    replaceSubstringIn(copy, toReplace, replaceWith);
    return copy;
}

//************************************************************

bool isInteger(const std::string &s)
{
    return !s.empty() && std::all_of(s.begin(), s.end(), ::isdigit);
}

//************************************************************

std::string readEnv(const std::string &env_name)
{
    auto env = std::getenv(env_name.c_str());
    return std::string(env == nullptr ? "" : env);
}

//************************************************************

Finally::Finally(std::function<void()> final_action) : m_final_action{std::move(final_action)}
{
}

//************************************************************

Finally::~Finally()
{
    m_final_action();
}

//************************************************************

fs::path relativePath(fs::path p, fs::path base)
{
    p    = fs::absolute(p);
    base = fs::absolute(base);

    auto mismatched = std::mismatch(p.begin(), p.end(), base.begin(), base.end());

    if (mismatched.first == p.end() && mismatched.second == base.end())
    {
        return ".";
    }

    fs::path relative_path;

    // 4. iterate abase to the shared root and append "../"
    for (auto it = mismatched.second; it != base.end(); ++it)
    {
        if (!it->empty())
        {
            relative_path /= "..";
        }
    }

    for (auto it = mismatched.first; it != p.end(); ++it)
    {
        relative_path /= *it;
    }

    return relative_path;
}


// EOF