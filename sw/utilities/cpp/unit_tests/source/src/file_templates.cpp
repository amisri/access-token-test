//! @file   file_templates.cpp
//! @brief  Constants with files.
//! @author Dariusz Zielinski

#include "../inc/file_templates.h"

const char* file_template::makefile = R"(
# Auto-generated Makefile for _REPLACE_EXE_

CXX        = _REPLACE_CXX_
OBJCOPY    = _REPLACE_OBJCOPY_
CXX_FLAGS  = _REPLACE_FLAGS_CXX_
LINK_LIBS  = _REPLACE_LIBS_

# Link
all: _REPLACE_EXE_

_REPLACE_EXE_: _REPLACE_OBJ_ _REPLACE_PRO_OJBS_BUILD_
_REPLACE_TAB_@$(CXX) -O0 -Wall _REPLACE_OBJ_ _REPLACE_PRO_OJBS_BUILD_ -o _REPLACE_EXE_ $(addprefix -l ,$(LINK_LIBS)) -static-libstdc++

# Compile test object
_REPLACE_OBJ_: _REPLACE_SRC_
_REPLACE_TAB_@$(CXX) $(CXX_FLAGS) -include _REPLACE_API_FILE_  _REPLACE_INCLUDE_ -MT $@ -MMD -MP -MF _REPLACE_DEP_ -c _REPLACE_VARIANT_SRC_ -o $@
_REPLACE_TAB_@sed -i 's:_REPLACE_VARIANT_SRC_:_REPLACE_SRC_:g' _REPLACE_DEP_

# Include dependency file
-include _REPLACE_DEP_

)";

//************************************************************

const char* file_template::main = R"(

#include <exception>
#include <fstream>

int main(int argc, char **argv)
{
    // Fill the name of this file
    unit_tests::this_file_name = "_REPLACE_THIS_FILE_NAME_";

    // There has to be at least 4 arguments (0-program path, 1-output file, 2-test number, 3-is exit Test)
    if (argc < 4)
    {
        exit(2);
    }

    // Get arguments
    std::string _file_name(argv[1]);
    int         _test_number  = 0;
    bool        _is_exit_test = (std::string(argv[3]) == "true");

    try
    {
        _test_number = std::stoi(argv[2]);
    }
    catch (...)
    {
        exit(3);
    }

    // Open a file to write
    unit_tests::error_output_stream.open(_file_name);

    if (unit_tests::error_output_stream.is_open() == false)
    {
        exit(4);
    }

    // Invoke the BEFORE function (if defined)
    try
    {
        _REPLACE_BEFORE_INVOKE_
    }
    catch (unit_tests::Assert_exception& e)
    {
        unit_tests::error_output_stream << "An assert should not be used in the BEFORE function.";
        exit(1);
    }
    catch (std::exception& e)
    {
        unit_tests::error_output_stream << "An exception has been thrown in the BEFORE function: " << e.what();
    }
    catch (...)
    {
        unit_tests::error_output_stream << "A non-standard has been thrown exception in the BEFORE function.";
    }


    // Execute a test
    try
    {
        switch (_test_number)
        {
            _REPLACE_SWITCH_CASES_

            // If test number has not been served, that's an internal error
            default: exit(5);
        }

        // If we are here and this is exit test, that's an error - the test didn't terminated the program
        unit_tests::reportNotExiting(_is_exit_test, "_REPLACE_EXIT_ERROR_");
    }
    catch (unit_tests::Assert_exception& e)
    {
        // This is ok, continue the program
    }
    catch (std::exception& e)
    {
        unit_tests::reportNotExiting(_is_exit_test, std::string("_REPLACE_EXIT_ERROR_") + "\n");
        unit_tests::error_output_stream << "An exception has been thrown in the test function: " << e.what();
    }
    catch (...)
    {
        unit_tests::reportNotExiting(_is_exit_test, std::string("_REPLACE_EXIT_ERROR_") + "\n");
        unit_tests::error_output_stream << "A non-standard exception has been thrown in the test function.";
    }


    // Invoke the AFTER function (if defined)
    try
    {
        _REPLACE_AFTER_INVOKE_
    }
    catch (unit_tests::Assert_exception& e)
    {
        unit_tests::error_output_stream << "An assert should not be used in the BEFORE function.";
        exit(1);
    }
    catch (std::exception& e)
    {
        unit_tests::error_output_stream << "An exception has been thrown in the BEFORE function: " << e.what();
    }
    catch (...)
    {
        unit_tests::error_output_stream << "A non-standard exception has been thrown in the BEFORE function.";
    }


    //! Return exit code
    return unit_tests::exit_code;
}
)";


// EOF