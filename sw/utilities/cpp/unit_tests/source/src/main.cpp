//! @file   main.cpp
//! @brief  Main file for the C++ Unit Test Framework
//! @author Dariusz Zielinski

// ---------- Includes
#include <cstdlib>
#include <filesystem>
#include <iomanip>
#include <string>
#include <vector>

#include "../inc/execute.h"
#include "../inc/file_finder.h"
#include "../inc/logging.h"
#include "../inc/options.h"


namespace fs = std::filesystem;


void preparePaths(const std::vector<Object_file>& objects)
{
    std::vector<std::string> paths = {Options::build_dir_src, Options::build_dir_obj};

    // Add objects paths
    for (const auto& object : objects)
    {
        paths.push_back(fs::path(object.build).parent_path().string());
    }

    // Create paths
    for (const auto& path : paths)
    {
        Exe::runCritical("mkdir -p " + path, "Failed to create a folder: " + path);
    }
}

int run(int argc, char **argv)
{
    //------------------- Parse arguments
    Options::parse(argv[0], std::vector<std::string>(argv + 1, argv + argc));

    //------------------- Say hello
    log::text << "C++ Unit Tests Framework. ";
    if (Options::pro_name.empty() == false)
    {
        log::text << "Testing project: " << Options::pro_name << "\n";
    }

    //-------------------  Find list of files and parse them
    auto test_files = File_finder::findTestFiles();

    //-------------------  Find list the project object files
    auto objects = File_finder::findObjectFiles();

    //------------------- Prepare paths in build folder
    preparePaths(objects);

    //-------------------  Count tests and variants
    Test_Stats test_stats;

    int variants_count = 0;
    int test_count     = 0;

    for (auto& test_file : test_files)
    {
        variants_count += test_file->getVariantsCount();
        test_count     += test_file->getTestCount();
    }

    if (test_count == 0)
    {
        log::error << "No tests found\n";
        exit_program(2);
    }

    //------------------- Info about discovered files and tests
    log::info << "Found " << test_count << " tests in " << test_files.size() << " files. \n";

    //------------------- List tests
    if (Options::do_list)
    {
        for (const auto& test_file : test_files)
        {
            log::brief << No_prefix() << "\n";
            test_file->listAllTests();
        }

        return 0;
    }

    //------------------- Compile tests
    log::brief_only << "Compiling tests: ";

    int test_file_index = 1;
    for (auto& test_file : test_files)
    {
        test_file->compile(objects, test_file_index, variants_count);
    }

    log::brief_only << No_prefix() << "\n";

    //------------------- Run GDB
    if (Options::run_gdb)
    {
        test_files[0]->runGdb();
        return 0;
    }

    //------------------- Run tests
    if (Options::no_colors)
    {
        log::brief_only << "Running tests: ";
    }

    if (Options::run_regular)
    {
        int test_index = 1;
        for (auto& test_file : test_files)
        {
            test_file->execute(test_stats, test_index, test_count);

            if (test_stats.failed > 0 && Options::stop_at_fail)
            {
                break;
            }
        }
    }

    if (Options::run_valgrind)
    {
        int test_index = 1;
        for (auto& test_file : test_files)
        {
            test_file->valgrind(test_stats, test_index, test_count);

            if (test_stats.failed > 0 && Options::stop_at_fail)
            {
                break;
            }
        }
    }

    // Finish when stop_at_fail is enabled and there are errors
    if (test_stats.failed > 0 && Options::stop_at_fail)
    {
        log::error << "Stopped: a test has failed and it was requested to stop at first error.\n";
        return 1;
    }


    log::brief_only << No_prefix() << (Options::no_colors ? "\n" : "");

    //------------------- Print stats
    auto no_color = (Options::no_colors ? "" : Colors::no_color);

    // If both regular and valgrind tests were run, then multiply test count by 2
    if (Options::run_regular && Options::run_valgrind)
    {
        test_count *= 2;
    }

    // Subtract skipped tests from the test count
    test_count -= test_stats.skipped;

    log::text << "\n";

    if (test_stats.skipped > 0)
    {
        log::text << "INFO: " << "Skipped tests: " << std::setw(3) << test_stats.skipped << "\n";
    }

    log::success << "Passed tests: " << std::setw(4) << test_stats.passed
                 << " (success rate: "  << std::setprecision(2) << std::fixed  << (100.0 * test_stats.passed / test_count) << " %)\n";


    if (test_stats.failed > 0)
    {
        log::error << No_prefix() << "INFO: " << "Failed tests: " << std::setw(4) << test_stats.failed
                   << " (failure rate: "  << std::setprecision(2) << std::fixed << (100.0 * test_stats.failed / test_count) << " %)\n" << no_color;

        // Exit code of the whole program
        return 1;
    }
    else
    {
        log::brief_success << "All tests passed.\n" << no_color;

        // Exit code of the whole program
        return 0;
    }
}

//************************************************************

int main(int argc, char **argv)
{
    try
    {
        return run(argc, argv);
    }
    catch (Exit_Program_Exception& e)
    {
        return e.exit_code;
    }
}


// EOF