//! @file   options.cpp
//! @brief  Parses command line and provides access to options
//! @author Dariusz Zielinski

// ---------- Includes
#include <filesystem>
#include <iostream>
#include <regex>

#include "../inc/execute.h"
#include "../inc/logging.h"
#include "../inc/options.h"
#include "../inc/utils.h"

namespace fs = std::filesystem;

std::string              Options::framework_root;
std::string              Options::framework_api;
std::string              Options::root;

std::string              Options::source;
std::string              Options::build_dir;
std::string              Options::build_dir_src;
std::string              Options::build_dir_obj;
std::string              Options::valgrind_flags;

std::string              Options::pro_name;
std::vector<std::string> Options::pro_inc;
std::string              Options::pro_obj;
std::string              Options::pro_cxx;
std::string              Options::pro_objcopy;
std::string              Options::pro_cxx_flags;
std::string              Options::pro_libs;

std::vector<std::string> Options::test_paths;
bool                     Options::do_dump      = false;
bool                     Options::do_list      = false;
bool                     Options::stop_at_fail = false;
bool                     Options::brief        = false;
bool                     Options::errors_only  = false;
bool                     Options::no_colors    = false;
bool                     Options::core_dumps   = false;
bool                     Options::run_valgrind = false;
bool                     Options::run_regular  = false;
bool                     Options::run_gdb      = false;

bool                     Options::run_gdb_matched = false;
unsigned int             Options::run_gdb_variant;
std::string              Options::run_gdb_test;


//************************************************************

auto assignValue(std::string& option)
{
    return [&](const std::string& value)
    {
        option = value;
    };
}

// Define options in the user config file
std::vector<User_option> valid_config_options =
{
    User_option("TEST_FRAMEWORK", [](const std::string& value)
    {
        Options::framework_root = value;
        Options::framework_api  = (fs::path(Options::framework_root) / "api/unit_test_api.h").string();
    }),
    User_option("TEST_ROOT", assignValue(Options::root)),
    User_option("TEST_SOURCE", [](const std::string& value)
    {
        Options::source = (fs::path(Options::root) / value).string();
    }),
    User_option("TEST_BUILD_DIR",[](const std::string& value)
    {
        Options::build_dir     = value;
        Options::build_dir_src = Options::build_dir + "/src";
        Options::build_dir_obj = Options::build_dir + "/obj";
    }),
    User_option("TEST_VALGRIND_FLAGS", assignValue(Options::valgrind_flags)),
    User_option("TEST_PRO_NAME", assignValue(Options::pro_name)),
    User_option("TEST_PRO_INC", [](const std::string& value)
    {
        // Split paths by comma
        auto inc_array = split(value, ' ');

        // Make them absolute
        for (const auto& path : inc_array)
        {
            if (path.empty() == false)
            {
                Options::pro_inc.push_back((fs::path(Options::root) / path).string());
            }
        }
    }),
    User_option("TEST_PRO_OBJ", [](const std::string& value)
    {
        Options::pro_obj = (fs::path(Options::root) / value).string();
    }),
    User_option("TEST_PRO_CXX",        assignValue(Options::pro_cxx)),
    User_option("TEST_PRO_OBJCOPY",    assignValue(Options::pro_objcopy)),
    User_option("TEST_PRO_CXX_FLAGS",  assignValue(Options::pro_cxx_flags)),
    User_option("TEST_PRO_LINK_FLAGS", assignValue(Options::pro_libs)),
};


// Define valid arguments
// If you change something here, don't forget to update docs under sw\utilities\cpp\unit_tests\README.md
std::vector<Argument> valid_args =
{
    Argument("-h", "Prints help",                                                        &Options::printUsage),
    Argument("-i", "Dumps all options and config and exits the framework.",              []() { Options::do_dump      = true; }),
    Argument("-l", "Lists all the tests instead of running them.",                       []() { Options::do_list      = true; }),
    Argument("-s", "Stops executing other tests on a first error.",                      []() { Options::stop_at_fail = true; }),
    Argument("-b", "Brief printing - limits output printed to a bare minimum.",          []() { Options::brief        = true; }),
    Argument("-e", "Prints only errors. In case of no errors, nothing will be printed.", []() { Options::errors_only  = true; }),
    Argument("-c", "Do not use colors when printing.",                                   []() { Options::no_colors    = true; }),
    Argument("-d", "Sets core dump size to unlimited, thus enabling core dumps.",        []() { Options::core_dumps   = true; }),
    Argument("-v", "Run valgrind tests. Specify -r to run regular tests as well.",       []() { Options::run_valgrind = true; }),
    Argument("-r", "Run regular tests. This is also default behavior.",                  []() { Options::run_regular  = true; }),
    Argument("-g", "Run gdb for a single, specified test: test_path:variant:test_name.", []() { Options::run_gdb      = true; })
};

// Lists of found errors during parsing.
bool                     invalid_user_config_file;   //!< True if config file has an error.
std::vector<std::string> missing_user_options;       //!< A list of missing user options in the user config file.
std::vector<std::string> unrecognized_options;       //!< A list of unrecognized command line arguments.
std::vector<std::string> invalid_paths;              //!< A list of invalid paths to tests.

//************************************************************

User_option::User_option(const std::string &name, const std::function<void(const std::string&)> &action)
{
    m_name   = name;
    m_action = action;
}

//************************************************************

void User_option::getEnv() const
{
    m_action(readEnv(m_name));
}

//************************************************************

Argument::Argument(const std::string& argument, const std::string& description, const std::function<void()>& action)
{
    m_action = action;
    m_arg    = argument;
    m_desc   = description;
}

//************************************************************

std::string Argument::print() const
{
    return "   " + m_arg + "   " + m_desc;
}

//************************************************************

bool Argument::test(std::string arg) const
{
    return arg == m_arg;
}

//************************************************************

void Argument::execute() const
{
    m_action();
}

//************************************************************

void Options::parse(const std::string& program_path, std::vector<std::string> arguments)
{
    // Process arguments
    readEnvVariables();

    // Parse command line arguments
    parseCommandLine(arguments);

    // Dump config if enabled
    if (do_dump)
    {
        dump();
    }

    // Print found errors
    printErrors();
    validateCommandLine();
    validateUserOptions();
}

//************************************************************

void Options::printUsage()
{
    std::cout << "Usage: ./cpp_unit_tests [path_to_test] [path_to_another_test] [-options]\n\n";
    std::cout << "Options: \n";
    for (const auto& arg : valid_args)
    {
        std::cout << arg.print() << std::endl;
    }

    // Terminate program when printing help
    exit_program(0);
}

//************************************************************

void Options::dump()
{
    auto color = Options::no_colors ? "" : Colors::blue;

    std::cout
    << "Config options:\n"
    << "   Framework root dir   = " << color << Options::framework_root                << Colors::no_color << "\n"
    << "   Framework API file   = " << color << Options::framework_api                 << Colors::no_color << "\n"
    << "   Project root dir     = " << color << Options::root                          << Colors::no_color << "\n"
    << "   Tests source dir     = " << color << Options::source                        << Colors::no_color << "\n"
    << "   Tests build dir      = " << color << Options::build_dir                     << Colors::no_color << "\n"
    << "   Project name         = " << color << Options::pro_name                      << Colors::no_color << "\n"
    << "   Project include dirs = " << color << join(Options::pro_inc, "", ",")        << Colors::no_color << "\n"
    << "   Project object dir   = " << color << Options::pro_obj                       << Colors::no_color << "\n"
    << "   C++ compiler command = " << color << Options::pro_cxx                       << Colors::no_color << "\n"
    << "   GCC objcopy command  = " << color << Options::pro_objcopy                   << Colors::no_color << "\n"
    << "   C++ flags            = " << color << Options::pro_cxx_flags                 << Colors::no_color << "\n"
    << "   Libraries to link    = " << color << Options::pro_libs                      << Colors::no_color << "\n"
    << "\n"
    << "Command-line options:\n"
    << "   Test paths to run    = " << color << join(Options::test_paths, "", ",")     << Colors::no_color << "\n"
    << "   Stop on error        = " << color << (Options::stop_at_fail ? "Yes" : "No") << Colors::no_color << "\n"
    << "   Brief output         = " << color << (Options::brief        ? "Yes" : "No") << Colors::no_color << "\n"
    << "   Only-errors output   = " << color << (Options::errors_only  ? "Yes" : "No") << Colors::no_color << "\n"
    << "   No-formatting output = " << color << (Options::no_colors    ? "Yes" : "No") << Colors::no_color << "\n"
    << "   Generate core dumps  = " << color << (Options::core_dumps   ? "Yes" : "No") << Colors::no_color << "\n"
    << "   Run Valgrind         = " << color << (Options::run_valgrind ? "Yes" : "No") << Colors::no_color << "\n"
    << "   Run regular tests    = " << color << (Options::run_regular  ? "Yes" : "No") << Colors::no_color << "\n";

    // Terminate program when dumping options
    exit_program(0);
}

//************************************************************

void Options::readEnvVariables()
{
    // Iterate over config options and try to match them
    for (const auto& option : valid_config_options)
    {
        option.getEnv();
    }
}

//************************************************************

void Options::parseCommandLine(const std::vector<std::string> &arguments)
{
    std::regex gdb_format(R"((.+):(.+))");
    std::regex gdb_format_with_variant(R"((.+):([\d]+):(.+))");

    // Parse arguments
    for (const auto& arg : arguments)
    {
        // Check if the argument starts with a hyphen
        if (arg[0] == '-')
        {
            bool found = false;
            for (const auto& valid_arg : valid_args)
            {
                if (valid_arg.test(arg))
                {
                    valid_arg.execute();
                    found = true;
                    break;
                }
            }

            if (found == false)
            {
                unrecognized_options.push_back(arg);
            }
        }
        else
        {
            // Otherwise, the argument is treated as the path to test (relative to test source dir)
            std::string test_path;
            std::smatch match;
            bool        matched = false;

            // Check if it matches the GDB format regex
            if (std::regex_match(arg, match, gdb_format_with_variant))
            {
                test_path       = match[1];
                run_gdb_variant = stoi(match[2]);
                run_gdb_test    = match[3];
                matched         = true;
            }
            else if (std::regex_match(arg, match, gdb_format))
            {
                test_path       = match[1];
                run_gdb_variant = 0;
                run_gdb_test    = match[2];
                matched         = true;
            }
            else
            {
                test_path = arg;
            }

            // Get the absolute path
            std::string path = source + "/" + test_path;

            // Before adding, check if it's valid
            if (fs::exists(path))
            {
                test_paths.push_back(path);
                run_gdb_matched = matched;
            }
            else
            {
                invalid_paths.push_back(path);
            }
        }
    }

    // If both valgrind and regular tests are not enabled, then default behavior (run regular tests) is selected
    if (run_regular == false && run_valgrind == false)
    {
        run_regular = true;
    }
}

void Options::printErrors()
{
    if (invalid_user_config_file)
    {
        log::error << "A path to the user config file is invalid.\n";
    }

    // Print missing options in the user config file.
    for (const auto& option : missing_user_options)
    {
        log::error << "User config file does not define mandatory option: '" << option << "'\n";
    }

    // Print unrecognized options and invalid path.
    // Note, that if only error are printed, then nothing will be printed here.
    // Also, this is separated here from the main loop above to take "only errors" flag into account.
    for (const auto& arg : unrecognized_options)
    {
        log::warning << "Unrecognized option '" << arg << "' ignored. \n";
    }

    for (const auto& path : invalid_paths)
    {
        log::warning << "Invalid test path '" << path << "' ignored. \n";
    }

    // In case there were some errors in the user file, then terminate the program
    if (invalid_user_config_file || missing_user_options.empty() == false)
    {
        exit_program(2);
    }
}


void Options::validateCommandLine()
{
    if (run_gdb)
    {
        // In case GDB is selected to run, only one test has to be provided
        if (test_paths.empty())
        {
            log::error << "When trying to run gdb, a single test has to be specified in the format: test_path:variant:test_name\n";
            exit_program(2);
        }
        else if (test_paths.size() > 1)
        {
            log::error << "When trying to run gdb, only one test can be specified.\n";
            exit_program(2);
        }

        // Check if the format is correct
        if (run_gdb_matched == false)
        {
            log::error << "When trying to run gdb, specify test path and name (test_path:test_name) or with variant (test_path:variant:test_name).\n";
            exit_program(2);
        }
    }
}


void Options::validateUserOptions()
{
    bool fatal_error = false;

    // The build directory is not checked here - it will be check after the preparation code tries to create it
    // and if it fails, then it will print error.

    // Check if path to tests sources is valid
    if (fs::is_directory(Options::source) == false)
    {
        log::error << "The path to the tests source (" << Options::source << ") does not point to a valid directory\n";
        fatal_error = true;
    }

    // Check if path to the project object files
    if (fs::is_directory(Options::pro_obj) == false)
    {
        log::error << "The path to the project object files (" << Options::pro_obj << ") does not point to a valid directory\n";
        fatal_error = true;
    }

    // Check if paths to project dirs are ok
    for (auto& path : Options::pro_inc)
    {
        if (fs::is_directory(path) == false)
        {
            log::error << "The path to one of the project directories '" << path << "' (defined in TEST_PROJECT_DIRS in the user config file) does not point to a valid directory\n";
            fatal_error = true;
        }
    }

    // Terminate program on error
    if (fatal_error)
    {
        exit_program(2);
    }
}


// EOF