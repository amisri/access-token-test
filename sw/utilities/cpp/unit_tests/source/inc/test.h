#pragma once

//! @file   test.h
//! @brief  Classes to represent tests.
//! @author Dariusz Zielinski

// ---------- Includes
#include <functional>
#include <regex>
#include <memory>
#include <string>
#include <tuple>
#include <vector>



//! Parsed variant - variant index, key, value
using Parsed_variant = std::tuple<int, std::string, std::string>;

//! Variant case - key, value
using Variant_case = std::tuple<std::string, std::string>;

//! Directive
enum class Directive_type
{
    test,
    test_exit,
    test_exit_no_param,
    test_crash,
    before,
    after,
    varaint,
    show_std_out,
    hide_std_err
};

//! File extensions
namespace ext
{
    constexpr auto variant    = ".v";
    constexpr auto makefile   = ".makefile";
    constexpr auto source     = ".cpp";
    constexpr auto object     = ".obj";
    constexpr auto executable = ".exe";
    constexpr auto depend     = ".dep";
    constexpr auto output     = ".output";
    constexpr auto valgrind   = ".valgrind";
}

//************************************************************

class Directive
{
    public:
        Directive(Directive_type type, std::string pattern);

        bool match(const std::string& line, std::smatch& match) const;

        Directive_type type() const;

    private:
        Directive_type m_type;
        std::regex     m_regex;
};

//************************************************************

class Object_file
{
    public:
        explicit Object_file(const std::string& source_path);

        std::string build;
        std::string source;
};

//************************************************************

class Test_Stats
{
    public:
        int passed  = 0;
        int failed  = 0;
        int skipped = 0;
};

//************************************************************

class Test
{
    public:
        explicit Test(const std::string& name);

        std::string name();

        virtual bool checkIfPassed(const int& exit_code, std::vector<std::string>& report) = 0;
        virtual bool isExitTest();

    protected:
        std::string m_name;
};

using Test_ptr = std::shared_ptr<Test>;

//************************************************************

class Regular_test : public Test
{
    public:
        explicit Regular_test(const std::string& name);

        bool checkIfPassed(const int& exit_code, std::vector<std::string>& report) override;
};

//************************************************************

class Exit_test : public Test
{
    using Rule = std::function<bool(int)>;

    class Rule_match
    {
        public:
            Rule_match(const std::string& regex, int groups, const std::function<Rule(int, int)>& func);
            bool match(std::string input);
            Rule getRule();

        private:
            std::regex                    m_regex;
            int                           m_groups;
            std::function<Rule(int, int)> m_rule_func;
            int                           m_param1 = 0;
            int                           m_param2 = 0;

    };

    public:
        Exit_test(const std::string& name, const std::string& params);

        bool checkIfPassed(const int& exit_code, std::vector<std::string>& report) override;
        bool isExitTest() override;

    private:
        std::vector<Rule> m_rules;
        std::string       m_user_rules;

};

//************************************************************

class Test_variant
{
    public:
        Test_variant(int index, const std::string& test_file_name, const std::vector<Variant_case>& variant_cases = {});

        std::string getName() const;
        std::string getSuffix() const;
        std::string getDescription(const bool& skip_colon = false) const;
        std::string getFile(const std::string& file, const std::vector<Test_ptr>& tests,
                            const std::string& before, const std::string& after) const;

    private:
        std::string               m_test_file_name;
        int                       m_index;
        std::vector<Variant_case> m_variant_cases;
};

//************************************************************

class Test_file
{
    public:
        explicit Test_file(std::string file_path);
        ~Test_file();

        int getVariantsCount();
        int getTestCount();

        void compile(const std::vector<Object_file>& objects, int& global_index, const int& test_file_count);

        void listAllTests() const;
        void execute(Test_Stats& test_stats, int& global_index, const int& test_count);
        void valgrind(Test_Stats& test_stats, int& global_index, const int& test_count);
        void runGdb();

    private:
        void parse();

        void processParsedVariants(const std::vector<Parsed_variant> &parsed_variants);

        std::string generateMakefile(const std::vector<Object_file>& objects, const Test_variant& variant);

        void runFunc(int& global_index, const int& count, const std::string& action, const std::string& name, std::function<void()> func);

        std::string               m_name;

        std::string               m_path_src_file;
        std::string               m_path_src_dir;
        std::string               m_path_build_file;
        std::string               m_path_build_dir;
        std::vector<std::string>  m_files_to_remove;

        bool                      m_show_std_out = false;
        bool                      m_hide_std_err = false;
        std::string               m_func_before;
        std::string               m_func_after;


        std::string               m_file;
        std::vector<Test_variant> m_variants;
        std::vector<Test_ptr>     m_tests;
};

using Test_file_ptr = std::shared_ptr<Test_file>;


// EOF