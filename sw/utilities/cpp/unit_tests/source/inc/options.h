#pragma once

//! @file   options.h
//! @brief  Parses command line and provides access to options
//! @author Dariusz Zielinski

// ---------- Includes
#include <functional>
#include <regex>
#include <string>
#include <vector>


// ---------- Classes declarations ---------------------------

class User_option
{
    public:
        //! Constructor that assign variables.
        User_option(const std::string& name, const std::function<void(const std::string&)>& action);

        //! Tries to find matching regex in the input and if it's found, then executes action.
        void getEnv() const;

    private:
        std::function<void(const std::string&)> m_action;
        std::string                             m_name;
};

//************************************************************

class Argument
{
    public:
        //! Default constructor.
        Argument() = default;

        //! Constructor that assign variables.
        Argument(const std::string& argument, const std::string& description, const std::function<void()>& action);

        //! Prints usage of this argument.
        std::string print() const;

        //! Test if the passed option represents this argument.
        bool test(std::string arg) const;

        //! Execute argument action.
        void execute() const;

    private:
        std::function<void()> m_action;
        std::string           m_arg;
        std::string           m_desc;
};

//************************************************************

class Options
{
    public:
        //! Parses command line arguments and fills the options below
        static void parse(const std::string& program_path, std::vector<std::string> arguments);

        //! Prints usage and exits the framework
        static void printUsage();

        //! Dumps all options and config and exits the framework
        static void dump();

        //! Program under test options
        static std::string              framework_root;  //!< Root directory of this C++ Unit Tests framework.
        static std::string              framework_api;   //!< A path to the file with the API of this C++ Unit Test framework.
        static std::string              root;            //!< Root directory of the project under test.

        static std::string              source;          //!< An absolute path to the main folder with tests.
        static std::string              build_dir;       //!< The test build directory.
        static std::string              build_dir_src;   //!< The build_dir with "/src" suffix.
        static std::string              build_dir_obj;   //!< The build_dir with "/obj" suffix.
        static std::string              valgrind_flags;  //!< Flags passed to valgrind.

        static std::string              pro_name;        //!< A global name for all the tests.
        static std::vector<std::string> pro_inc;         //!< Set of paths used with -I flag for the g++.
        static std::string              pro_obj;         //!< A root folder of the project's object files.
        static std::string              pro_cxx;         //!< g++ compiler command.
        static std::string              pro_objcopy;     //!< objcopy command.
        static std::string              pro_cxx_flags;   //!< Additional flags for the g++.
        static std::string              pro_libs;        //!< Additional flags for the linker with library list.

        // Program command-line options
        static std::vector<std::string> test_paths;      //!< A list of test to run. If empty, all tests are run.
        static bool                     do_dump;         //!< If true, dumps the all the options and exits the program.
        static bool                     do_list;         //!< If true, lists all the tests instead of running them.
        static bool                     stop_at_fail;    //!< If true, it will stop running further tests on first test fail.
        static bool                     brief;           //!< If true, limits output printed to a bare minimum.
        static bool                     errors_only;     //!< If true, prints only errors. In case of no errors, nothing will be printed.
        static bool                     no_colors;       //!< If true, prints output without colors.
        static bool                     core_dumps;      //!< If true, sets core dump size to unlimited.
        static bool                     run_valgrind;    //!< If true, runs tests with valgrind. If run_regular is false, regular tests won't be run.
        static bool                     run_regular;     //!< If true, runs regular tests. If run_valgrind is false, valgrind tests won't be run.
        static bool                     run_gdb;         //!< If true, no tests are run, instead GDB is run with a single test, that has to be specified by the user.

        static bool                     run_gdb_matched; //!< Becomes true if run_gdb_variant and run_gdb_test where correctly passed on the command line.
        static unsigned int             run_gdb_variant; //!< Variant of the test file to run with GDB.
        static std::string              run_gdb_test;    //!< Test name withing the test file to run with GDB.



    private:
        //! Iterates over all valid config options and read them from the environmental variables
        static void readEnvVariables();

        //! Parses command line arguments
        static void parseCommandLine(const std::vector<std::string>& arguments);

        //! Check correctness of the command line arguments
        static void validateCommandLine();

        //! Check correctness of read config options
        static void validateUserOptions();

        //! Prints found errors
        static void printErrors();
};


// EOF