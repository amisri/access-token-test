#pragma once

//! @file   file_finder.h
//! @brief  A function that prepares list of files with tests.
//! @author Dariusz Zielinski

// ---------- Includes
#include <functional>
#include <regex>
#include <string>
#include <vector>

#include "test.h"

// ---------- Classes declarations ---------------------------

class File_finder
{
    public:
        static std::vector<Test_file_ptr> findTestFiles();

        static std::vector<Object_file> findObjectFiles();
};


// EOF