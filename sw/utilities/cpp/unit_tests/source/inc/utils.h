#pragma once

//! @file   utils.h
//! @brief  Utilities.
//! @author Dariusz Zielinski

// ---------- Includes
#include <fstream>
#include <functional>
#include <filesystem>
#include <iterator>
#include <stdint.h>
#include <string>
#include <sstream>
#include <vector>


//! Read content of a file into an std::string. Currently no error handling.
//!
//! @param file_name path to a file to read.
//! @return returns an std::string with the content of the file.
std::string readFromFile(const std::string& file_name);

//************************************************************

//! Read content of a file into a vector of std::strings. Currently no error handling.
//!
//! @param file_name path to a file to read.
//! @return returns a vector of std::strings where each element corresponds to a line in the file.
std::vector<std::string> readLinesFromFile(const std::string& file_name);

//************************************************************

//! Write data given in an std::string into a file. Currently no error handling.
//!
//! @param file_name path to a file to write.
//! @param data data to write to the file.
void writeToFile(const std::string& file_name, const std::string& data);

//************************************************************

//! Splits input string into a vector by a given delimiter.
std::vector<std::string> split(const std::string& input, const char& delimiter);

//************************************************************

//! Prints each element of given vector, separating it with a delimiter
template<class T>
std::string join(const std::vector<T>& vector, const std::string& before, const std::string& after)
{
    std::stringstream ss;

    for (auto it = vector.begin(); it != vector.end(); ++it)
    {
        ss << before << (*it);

        if (it + 1 != vector.end())
        {
            ss << after;
        }
    }

    return ss.str();
}


//************************************************************

//! Replaces all `toReplace` with `replaceWith`.
//! @param input input string
//! @param toReplace substring that will be replaced
//! @param replaceWith string that will be used to replace `toReplace`
void        replaceSubstringIn(std::string& input, const std::string& toReplace, const std::string& replaceWith);
std::string replaceSubstring(const std::string& input, const std::string& toReplace, const std::string& replaceWith);

//************************************************************

//! Is string integer?
bool isInteger(const std::string &s);

//************************************************************

//! Gets an environmental variable, if present, or empty string otherwise.
std::string readEnv(const std::string &env_name);

//************************************************************

//! Finally will execute given action in its destructor (so when it goes out of scope).
class Finally
{
    public:
        //! Constructs the object.
        //! @param final_action a simple function, with no arguments and no return, to be executed in the destructor.
        explicit Finally(std::function<void()> final_action);

        //! Executes the callback passed in the constructor.
        ~Finally();

    private:
        std::function<void()> m_final_action;  //!< Callback to be called during the class destructor.
};

//************************************************************

//! This function provides the same functionality as std::filesystem::relative, which is
//! unavailable in std::experimental::filesystem. TODO remove it - we're not using experimental anymore
std::filesystem::path relativePath(std::filesystem::path p, std::filesystem::path base);


// EOF