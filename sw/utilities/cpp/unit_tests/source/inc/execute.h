#pragma once

//! @file   execute.h
//! @brief  Functions to run external programs.
//! @author Dariusz Zielinski

#include <cstdlib>
#include <string>

class Exit_Program_Exception
{
    public:
        explicit Exit_Program_Exception(const int& exit_code);
        const int exit_code;
};

//************************************************************

void exit_program(const int& exit_code);

//************************************************************

class Exe
{
    public:
        static constexpr auto hide_none    = 0b00;
        static constexpr auto hide_std_out = 0b01;
        static constexpr auto hide_std_err = 0b10;
        static constexpr auto hide_all     = 0b11;

        //! Runs a command and returns its exit code
        static int run(std::string command, int hide = hide_std_out);

        //! Runs a command and terminates the program if the command returns an error, printing error_msg
        static void runCritical(std::string command, const std::string& error_msg, int hide = hide_std_out);

    private:
        static std::string createHideSuffix(int hide);
};


// EOF