#pragma once

//! @file   logging.h
//! @brief  Provides global objects, similar to std::cout, for printing.
//! @author Dariusz Zielinski

// ---------- Includes
#include <functional>
#include <iostream>
#include <ostream>
#include <string>
#include <type_traits>

#include "options.h"

// ---------- Misc declarations -----------------------------
namespace Colors
{
    constexpr auto no_color  = "\033[0m";
    constexpr auto white     = "\033[1;37m";
    constexpr auto blue      = "\033[1;34m";
    constexpr auto green     = "\033[1;32m";
    constexpr auto yellow    = "\033[1;33m";
    constexpr auto red       = "\033[1;31m";

    constexpr auto red_value = "\033[38;5;215m";
}

// ---------- Classes declarations ---------------------------

//! Use to skip printing of prefix
class No_prefix
{
};

//************************************************************

class LoggerEntry
{
    public:
        //! Construct the LoggerEntry class and prints the logger's prefix into the stream.
        LoggerEntry(const std::function<bool()> &enabled);

        //! Destructor that flushes std::cout.
        ~LoggerEntry();

        //! Template method that passed its argument into the string stream.
        //!
        //! @param obj Object passed into stream.
        //! @return Returns reference to the itself to enable chaining of <<.
        template <typename T>
        LoggerEntry& operator<<(T obj)
        {
            if (m_enabled())
            {
                std::cout << obj;
            }

            return *this;
        }

    private:
        std::function<bool()> m_enabled;
};

//************************************************************

class Logger
{
    public:
        //! Constructs the logger
        explicit Logger(const std::string& prefix, const std::string& color, const std::function<bool()> &enabled);

        //! Printing operator
        template <typename T>
        LoggerEntry operator<<(T obj)
        {
            // If it's not enabled, then do nothing
            if (m_enabled())
            {
                if (Options::no_colors == false)
                {
                    std::cout << m_color;
                }

                if constexpr (std::is_same<T, No_prefix>::value == false)
                {
                    std::cout << m_prefix << obj;
                }
            }

            LoggerEntry loggerEntry(m_enabled);

            // Return loggerEntry anyway, so that chaining of << compiles.
            return loggerEntry;
        }

    private:
        std::function<bool()> m_enabled;
        std::string           m_prefix;
        std::string           m_color;
};



// Global objects with loggers
namespace log
{
    //! Printing this erase current line and goes to its beginning
    constexpr auto reset_line = "\33[2K\r";

    // Logger
    extern Logger text;
    extern Logger info;
    extern Logger success;
    extern Logger brief;
    extern Logger brief_success;
    extern Logger brief_only;
    extern Logger warning;
    extern Logger error;
}


// EOF