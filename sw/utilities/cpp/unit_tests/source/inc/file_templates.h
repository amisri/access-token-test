#pragma once

//! @file   file_templates.h
//! @brief  Constants with files.
//! @author Dariusz Zielinski

namespace file_template
{
    extern const char* makefile;
    extern const char* main;
}


// EOF