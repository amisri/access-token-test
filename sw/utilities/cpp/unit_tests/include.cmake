
macro(cutf_set var value)
    if (NOT DEFINED ${var})
        set(${var} ${value})
    endif()
endmacro()


macro(cutf_configure target)

    # The root directory of this C++ Unit Tests framework.
    cutf_set(CUTF_FRAMEWORK "${CMAKE_CURRENT_LIST_DIR}")

    # The root directory of the project under test
    cutf_set(CUTF_ROOT      "${CMAKE_CURRENT_SOURCE_DIR}")

    # A path to the main folder with tests
    cutf_set(CUTF_SOURCE    "tests")

    # An absolute path where the tests are built
    cutf_set(CUTF_BUILD_DIR  "${CMAKE_CURRENT_BINARY_DIR}/unit_tests")

    # Flags passed to valgrind
    cutf_set(CUTF_VALGRIND_FLAGS "--tool=memcheck --leak-check=full --show-reachable=yes --num-callers=20")

    # Project name (just for printing it / identification)
    cutf_set(CUTF_PRO_NAME       "${CMAKE_PROJECT_NAME}")

    # Project include paths
    get_target_property(includes ${target} INCLUDE_DIRECTORIES)
    cutf_set(CUTF_PRO_INC ${includes})

    # A path to the project object files
    cutf_set(CUTF_PRO_OBJ "TODO")

    # A command for g++
    cutf_set(CUTF_PRO_CXX        "${CMAKE_CXX_COMPILER}")

    # A command for objcopy
    cutf_set(CUTF_PRO_OBJCOPY    "objcopy")

    # g++ flags used when compiling tests
    cutf_set(CUTF_PRO_CXX_FLAGS  "${CMAKE_CXX_FLAGS}")

    # Additional libraries to link
    cutf_set(CUTF_PRO_LINK_FLAGS "TODO")

    # Generate CUTF config script
    file(GENERATE OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/cutfRunTests.sh" CONTENT "# Auto-generated for target ${target}:
         CUTF_FRAMEWORK = ${CUTF_FRAMEWORK}
              CUTF_ROOT = ${CUTF_ROOT}
            CUTF_SOURCE = ${CUTF_SOURCE}
         CUTF_BUILD_DIR = ${CUTF_BUILD_DIR}
    CUTF_VALGRIND_FLAGS = ${CUTF_VALGRIND_FLAGS}
          CUTF_PRO_NAME = ${CUTF_PRO_NAME}
           CUTF_PRO_INC = ${CUTF_PRO_INC}
           CUTF_PRO_OBJ = ${CUTF_PRO_OBJ}
           CUTF_PRO_CXX = ${CUTF_PRO_CXX}
       CUTF_PRO_OBJCOPY = ${CUTF_PRO_OBJCOPY}
     CUTF_PRO_CXX_FLAGS = ${CUTF_PRO_CXX_FLAGS}
    CUTF_PRO_LINK_FLAGS = ${CUTF_PRO_LINK_FLAGS}
    ")

endmacro()

# EOF