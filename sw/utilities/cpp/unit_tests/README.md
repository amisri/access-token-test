# C++ Unit Tests Framework

This folder contains a framework to write and execute unit tests for
the C++ language. The framework itself is written in C++17.

**Table of Contents**
1. [Compiling](#compiling)
2. [How to use the framework in your project](#adding)
3. [Command-line options](#options)
4. [Writing the tests](#writing)




## Compiling <a name="compiling"></a>

#### Prerequisites
The framework is written in pure C++17 (to make use of the Filesystem
Standard Library) and does not require any additional libraries.  
The only requirement is to have GCC that supports C++17.

#### Compiling
Simply type `make` in this directory and that's it. The executable will
be created under `Linux\x86_64\unit_tests_framework`.  
Actually, you don't have to compile it here - if you try to run the
tests and the framework is not built, it will be built by the Makefile
that runs the tests.





## How to use the framework in your project <a name="adding"></a>

#### Example project
This directory contains subdirectory `example` where you can find a
simple C++ project, that doesn't do anything useful, but it has Makefile
with tests and some test examples. You can use it to    see how to include,
use and write tests for this framework.

#### How to include the framework Makefile
This framework assumes you use our internal Makefile building system.   
To add support for this framework, simply include this Makefile in your
project Makefile:

`include $(build_home)/sw/make/inc/Makefile.cpp.unit.tests`

And that's it. This adds a new target `test` - now, simply writing `make
test` should run your tests (and build framework if it's not built yet).  

#### Where to place tests
By default, the framework assumes there is a folder `tests` in the main
directory of your project (you can change this). You don't need to
declare your tests files anywhere - all the *.cpp files you place there
(also including subdirectories) will be discovered and parsed by the
framework.

#### Additional configuration
The test Makefile you've just included contains a set of options with
default values, that should be fine for most projects.  
If you need to change any of the default values, simply declare custom
options somewhere in your main Makefile:


| Option | Purpose | Default value |
|---|---|---|
| TEST_FRAMEWORK      | The root directory of this C++ Unit Tests framework  | $(build_home)/sw/utilities/cpp/unit_tests/ |
| TEST_ROOT           | The root directory of the project under test         | $(shell pwd) |
| TEST_SOURCE         | A path to the main folder with tests                 | tests |
| TEST_BUILD_DIR      | A path where the tests are built                     | $(exec_path)/unit_tests |
| TEST_VALGRIND_FLAGS | Flags passed to valgrind                             | --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=20 |
| TEST_PRO_NAME       | Project name (just for printing it)                  | $(shell basename $(exec)$(lib)) |
| TEST_PRO_INC        | Project include paths                                | (inc_path) |
| TEST_PRO_OBJ        | A path to the project object files                   | (obj_path) |
| TEST_PRO_CXX        | A command for g++                                    | $(CXX) |
| TEST_PRO_OBJCOPY    | A command for objcopy                                | $(OBJCOPY) |
| TEST_PRO_CXX_FLAGS  | g++ flags used when compiling tests                  | -g -O0 |
| TEST_PRO_LINK_FLAGS | Additional libraries to link                         | $(libs) |

These options are exported from the Makefile into shell and are read by
the framework as environmental variables.  
That means you can also define any of these options in your terminal,
before running the `make`. This could be useful to temporarily change
the value. To go back to the default value you can open a new terminal
or write `unset TEST_OPTION_YOU_CHANGED`.


#### Running the tests
To run tests (located in your test folder) all you need to do is to
write `make test`.  

Don't forget to **build your project first**, before running tests.

**Passing options**  
You can pass additional options to the command line (see the chapter
below). To do so, write `make test TEST_ARG="-your -arguments"`.  

If you prefer, you can also copy the file `test.sh` from this
framework main directory to where your Makefile is. This is a very
simple wrapper around the make command that allows you to write
`./test.sh -your -arguments`.


## Command-line options <a name="options"></a>
You can always pass `-h` option to see this help:

| Option | Purpose |
|:---:|---|
| -h | Prints help |
| -i | Dumps all options and config and exits the framework |
| -l | Lists all the tests instead of running them |
| -s | Stops executing other tests on a first error |
| -b | Brief printing - limits output printed to a bare minimum |
| -e | Prints only errors. In case of no errors, nothing will be printed |
| -c | Do not use colors when printing |
| -d | Sets core dump size to unlimited, thus enabling core dumps |
| -v | Run valgrind tests. Specify -r to run regular tests as well |
| -r | Run regular tests. This is also default behavior |
| -g | Run gdb for a single, specified test: test_path:variant:test_name |

Besides these options, every argument starting without the hyphen (-),
will be treated as a path to test or folder that should be run.   
The path is relative to the main folder with tests. If the path points
to the directory, then all the tests in this directory will be run. When
you don't pass any such argument, all tests are run (default behavior).
  
Example:   
`./test.sh -v dir1/test.cpp dir2`  
This will run valgrind tests on a single test file `dir1/test.cpp` and
all the test files under `dir2` directory.



## Writing the tests <a name="writing"></a>

#### How does this work?
As already mentioned, to create a test file you just need to create a
regular *.cpp file inside your main test directory (specified by the
option `TEST_SOURCE`, which defaults to `tests`). Inside these files you
simply declare test functions and that's it. You don't need to register
them anywhere - they will be automatically detected by the framework.  

Each such C++ file will be compiled as a separate executable and linked
with the object files of your project (you need to build your project
first). The framework automatically includes the API header (allowing
you to use assertions, declaring tests and so on) and the main function.  

When the tests are run, each test within the test file is a separate
program execution - that means that each test function gets a fresh,
clean environment and the state of the program from one tests doesn't
interfere with the other tests. If you need to do some actions every time,
before and/or after each test runs, you can define special *before* and
*after* function (see the chapter below).

The test program writes error descriptions to a special file, so that
writing to standard output and standard error output (if the tested
program writes there) it's not a problem (see the chapter below).

#### Declaring tests
To declare the test within your test file, use a simple macro:   
```(C++)
TEST(yourTestName)
{
    (...test code...)
}
```
Names of the tests must be unique within the same test file and they
have to represent a valid C++ function name.

Generally, now it's a good time to see the `example` directory in this
framework and see how the tests are written.  
Also, it's worth mentioning that the naming and syntax of the framework
is based on the **Google Test** framework - so if you're familiar with
it, this should feel natural.

#### Declaring exit and crash tests
Normally, if your test function crashes, throws an uncaught exception or
otherwise exits the program - it is considered as a test failure.  
Sometimes, however, you may want to test if your program exists or
crashes - for example to test if assertions in your code work. For this
you can declare an exit test (Google Test call it *death test*).  

Exit tests are declared in an analogous way:
```(C++)
TEST_EXIT(yourExitTestName, conditions)
{
    (...test code...)
}
```
If such test does not make the program exit, then it is considered that
a test failure. Mind that, throwing uncaught exception is not
considered as *exit* - for testing exceptions you can use standard
assertions to test exceptions (see below), while exit tests are meant to
test when program exits (mainly crashes).   

**Conditions**   
The `TEST_EXIT` macro expects a second parameter - a list of
comma-separated conditions. Every such condition is OR-ed (meaning, a
test passes if at least one of them evaluates to true). The conditions
are written in a simple syntax and the following options are valid:
- `number` - exit code is this number.
- `!number` - exit code is anything but this number.
- `number1:number2` - exit code is withing number1 to number2 range
  (both inclusive).
- `<number` - exit code is lesser than this number.
- `>number` - exit code is greater than this number.

The number can be positive or negative. Some examples of `conditions`:
- `!0` - test succeeds if the exit code is different than 0.
- `1,2,>10` - test succeeds if the exit code is equal to 1 or equal to
  2 or greater than 10.
- `-10:-1` - test succeeds if the exit code is greater or equal to -10
  but not greater than -1.
- `1:4,>5` - test succeeds if the exit code is positive and different
  than 5.

**Crash test**   
Instead of writing the exit test, you can also declare *crash test*:
```(C++)
TEST_CRASH(testCrashing)
{
    (...test code...)
}
```
It's just convenience wrapper for writing the exit test with a condition
`!0`. Everything else is the same as for the exit test.


#### Declaring *BEFORE* and *AFTER* functions
Sometimes you may find that you want to do something for each and every
test in your test file (for example create and destroy some object, open
and close a file or a connection to a database, etc).  
 
Of course, you can write a free function and execute it at the
beginning/end of every test function. Or you can define *before* and/or
*after* functions that facilitates this task:
```(C++)
BEFORE(beforeFuncName)
{
    (...init something...)
}

AFTER(afterFuncName)
{
    (...deinit something...)
}
```
There can only be one *before* and one *after* function. You can declare
either or none of them. They are executed, respectively, just before and
after running your test function (remember that running each test
function is a separate execution of a test program).    

**Do the cleaning**   
You may be tempted to create dynamic object and don't destroy them
(after all, it's just a test program, so it doesn't matter, right?) -
but you should free all allocated memory, otherwise when running
*valgrind* memory tests, *valgrind* will report all memory that wasn't
freed as an error.


#### Assertions

Assertions are macros that expect a certain condition to be true and if
the condition is not, then the test fails and the error is reported.  
The convention and the naming of assertions is the same as in the
Google Test framework.

**Assert and expect**  
Every macro is available in two versions: an assertion and an expectation.  
They both accept the same conditions and cause the test to fail if they
fail. The difference is that if the assertion fails, the test program
is terminated. If the expectation fails, the test program continues
its execution.

If you're writing a test function that checks multiple things, then try
to use expectations - if the first one fails, the others will still
be executed and multiple errors might be reported at once, making
debugging faster.  
Assertions are useful when there is no point in checking any further
conditions. For example, when you test a function that returns some
dynamically created object, you can use assertion to check that it's
not NULL - if it is, the test will terminate, as there is probably
no point in next checks.

**Available macros**

| Assertion | Expectation | Arguments | Succeeds if |
|---|---|---|---|
ASSERT_TRUE      | EXPECT_TRUE      | (val)                   | If val is true |
ASSERT_FALSE     | EXPECT_FALSE     | (val)                   | If val is false |
ASSERT_EQ        | EXPECT_EQ        | (val1, val2)            | If val1 == val2 |
ASSERT_NE        | EXPECT_NE        | (val1, val2)            | If val1 != val2 |
ASSERT_LT        | EXPECT_LT        | (val1, val2)            | If val1 < val2 |
ASSERT_LE        | EXPECT_LE        | (val1, val2)            | If val1 <= val2 |
ASSERT_GT        | EXPECT_GT        | (val1, val2)            | If val1 > val2 |
ASSERT_GE        | EXPECT_GE        | (val1, val2)            | If val1 >= val2 |
ASSERT_NEAR      | EXPECT_NEAR      | (val1, val2, abs_error) | If abs(val1-val2) < abs_error |
ASSERT_FLOAT_EQ  | EXPECT_FLOAT_EQ  | (val1, val2)            | If abs(val1-val2) < 0.0001 |
ASSERT_THROW     | EXPECT_THROW     | (code, exception_class) | If code throws exception with given class|
ASSERT_ANY_THROW | EXPECT_ANY_THROW | (code)                  | If code throws any exception |
ASSERT_NO_THROW  | EXPECT_NO_THROW  | (code)                  | If code does not throw any exception |

Additionally, you can directly instruct the test to fail (especially
useful in control statements) with the following macros:  
`FAIL()` and `ADD_FAILURE()`

The `FAIL` macro is similar to an assertion - meaning that a test
program terminates. The `ADD_FAILURE` macro is similar to an expectation -
the test program will continue.

**Custom errors**

When an ASSERT* or EXPECT* fails, the standard error message is reported.
It contains a file, a line number and a name of a test function.
Also, the `val1` and `val2` will be printed, both their calculated values
and their code.  
You can, however, customize this error message, either be adding your
message to the standard one, or by completely replacing the standard
message with your own.

For example, to add your message to the standard one, use + operator
after the test macro:
`EXPECT_EQ(3, queue.size()) + "Wrong size for the queue " + queue.name();`  
To completely replace the standard message, use << operator:
`EXPECT_EQ(3, queue.size()) << "Wrong size for the queue " << queue.name();`

Finally, you can also use the `ERROR()` macro to access the error stream
outside of test macros:  
`ERROR() << "This will be printed as an error";`  
**Note** that this won't be printed to the user if the test doesn't fail
(i.e. using the `ERROR()` macro by itself does not cause the test to fail).

#### Variants

Sometimes it is useful to run exactly the same test, but for a few
different classes or cases. For example, when testing a base class
interface, you may want to write the same test code, but run it for a few
different implementations of this interface.  
See the example `common_interface.cpp` in the example/tests folder
to see a use case.

To address such cases, this framework has a feature called "variants".  
You can declare any number of variants in a single test file and each
test function in this file will be run for each variant (for example,
having a test file with 3 variants and 5 tests function will result in
running 15 tests).

**Idea**  
The basic idea is that you declare some keywords and their values for
each variant. When compiling, the framework will simply replace
any keyword with its value. Note, that this is a text-based replace -
it has no knowledge about the C++ language. It will simply find any
occurrence of the keyword and replace it with the value, before
compiling the variant.

**Declaring the variants**  
Variants' keyword-value pairs are declared in single-line comments
with a directive, for example:
```(C++)
// @variant 1:impl_class:Rectangle
// @variant 1:shape_area:100

// @variant 2:impl_class:Triangle
// @variant 2:shape_area:50

// @variant 3:impl_class:Ellipse 
// @variant 3:shape_area:78.5398
```
First, you write a single-line comment and `@variant` directive,
then a variant number.
Then, after the colon, you write your keyword name, then colon and then
everything after this becomes the value of this keyword, for this variant.

The variants can be declared in any order and in any place in the code,
however the numbers must be consecutive (i.e. you can have variants
(1,2,3) but not (1,2,4)) and each keyword must be defined for each variant.

Later in your code, you may write for example:  
`Shape shape = new impl_class();`  
and this will be replaced by
`Shape shape = new Rectangle();`, `Shape shape = new Triangle();`
and `Shape shape = new Ellipse();` for variant 1, 2 and 3, respectively.


#### Standard output and standard error

The framework uses its own, separate file to report errors from the running
test program back to the framework, so whatever is printed to the standard
output or standard error does not interfere with the error reporting.

The default behavior is that, when running a test, everything that is
printed to the standard error is also printed to the user,
and everything printed to the standard output is ignored (hidden).  
You can, however, change that behavior by declaring either of those
directives (independently) anywhere in your code:
```(C++)
// @show_std_out
// @hide_std_err
```


#### Exceptions

If an exception is thrown out of your test, it is considered as a test
failure. It is caught by the framework and reported as an error.

If you expect an exception to be thrown, then catch it inside the
test function.  
You may then use `FAIL` or `ADD_FAILURE` macros to report an error when
an exception is thrown, or when you except it to be thrown but it's not.


#### Mocking functions

One of the most important features is a possibility to mock (replace)
certain functions.

This framework assumes the GCC (G++) is used to compile your project
and it uses a trick with the program called `objcopy` - your object
files are copied using this program with a `--weaken` option.
That causes all functions (and class methods) to be declared as "weak".  
A weak declaration makes it possible for the function to be declared
multiple times - you won't get the "*multiple definitions*" error from
the linker. Instead, the linker will simply choose to use the first
function definition it finds and ignore any other.

The framework makes sure that, during linking, the functions definitions
from your test files are found first and thus they will be used.

**So in short** - to mock any function/method (even private) simply
define its body in your test code and this one will be used, replacing
the original one.  
See file `mocking.cpp` in the example/tests folder to see a potential
use case.


#### Additional features

**Printing custom classes**

When reporting errors, the framework prints values (val1, val2) passed
to the test macros.
The framework uses the `std::stringstream`'s << operator for printing,
so all standard types are supported. Additionally, all STL containers
are also printed, by printing all of their elements.

If your class already supports standard `std::stream` << operator
for printing, then there is nothing you have to do.
If not, you can use this macro to define how to print your type:
```(C++)
PRINT(uint8_t)
{
    stream << static_cast<unsigned int>(object);
}
```

This is actual code from the framework to correctly print `uint_8`
as a number (and not as a byte).

To use this macro, write `PRINT(your_type)` and then inside the body,
use `stream` to access the `std::stringstream` you're printing to
and `object` to access the object of your class that is being printed.


**Different color in the error messages**

This is a small and easy feature. When color output is enabled (default)
you can use back-ticks characters (`) to change color of printed error
message. In color output the back-ticks will be removed, in non-formatting
output, the back-ticks will stay.

When providing custom error messages, everything enclosed in the
back-ticks will have orange color (instead of red for errors).



<!-- EOF -->