#pragma once

#include <fstream>
#include <sstream>
#include <stdint.h>

#include <iostream>
#include <type_traits>

//************************************************************
// Macro definitions
//************************************************************

//************************************************************
// Function-declaring macros
#define BEFORE(x)           void x()
#define TEST(x)             void x()
#define TEST_EXIT(x, ...)   void x()
#define TEST_CRASH(x)       void x()
#define AFTER(x)            void x()

//************************************************************
// Misc

// Macro to define user-specific printing function
#define PRINT(T) template <> void unit_tests::print(std::stringstream& stream, T& object)

// Allows the user to access the error file stream directly
#define ERROR() unit_tests::error_output_stream

//************************************************************
// Helper macros
#define _ASSERT(name, check) unit_tests::Error_stream(true,  #name, __LINE__).ctrl(check)
#define _EXPECT(name, check) unit_tests::Error_stream(false, #name, __LINE__).ctrl(check)

#define _CHECK1(func, val)               unit_tests:: func ((val), #val)
#define _CHECK2(func, val1, val2)        unit_tests:: func ((val1), (val2), #val1, #val2)
#define _CHECK3(func, val1, val2, val3)  unit_tests:: func ((val1), (val2), (val3), #val1, #val2, #val3)

#define _DEF_CHECK1(name) template<class Val> Error_stream_control name(Val val, const char* val_str)
#define _DEF_CHECK2(name) template<class Val1, class Val2> Error_stream_control name(Val1 val1, Val2 val2, const char* val1_str, const char* val2_str)
#define _DEF_CHECK3(name) template<class Val1, class Val2, class Val3> Error_stream_control name(Val1 val1, Val2 val2, Val3 val3, const char* val1_str, const char* val2_str, const char* val3_str)

//************************************************************
// Basic assertions

#define ASSERT_TRUE(val) _ASSERT(TRUE, _CHECK1(checkTrue, val))
#define EXPECT_TRUE(val) _EXPECT(TRUE, _CHECK1(checkTrue, val))

#define ASSERT_FALSE(val) _ASSERT(FALSE, _CHECK1(checkFalse, val))
#define EXPECT_FALSE(val) _EXPECT(FALSE, _CHECK1(checkFalse, val))

//************************************************************
// Special functions
#define FAIL()        _ASSERT(request_fail, _CHECK1(fail, false))
#define ADD_FAILURE() _EXPECT(request_fail, _CHECK1(fail, false))

//************************************************************
// Comparison assertions
#define ASSERT_EQ(val1, val2) _ASSERT(EQ, _CHECK2(checkEq, val1, val2))
#define EXPECT_EQ(val1, val2) _EXPECT(EQ, _CHECK2(checkEq, val1, val2))

#define ASSERT_NE(val1, val2) _ASSERT(NE, _CHECK2(checkNe, val1, val2))
#define EXPECT_NE(val1, val2) _EXPECT(NE, _CHECK2(checkNe, val1, val2))

#define ASSERT_LT(val1, val2) _ASSERT(LT, _CHECK2(checkLt, val1, val2))
#define EXPECT_LT(val1, val2) _EXPECT(LT, _CHECK2(checkLt, val1, val2))

#define ASSERT_LE(val1, val2) _ASSERT(LE, _CHECK2(checkLe, val1, val2))
#define EXPECT_LE(val1, val2) _EXPECT(LE, _CHECK2(checkLe, val1, val2))

#define ASSERT_GT(val1, val2) _ASSERT(GT, _CHECK2(checkGt, val1, val2))
#define EXPECT_GT(val1, val2) _EXPECT(GT, _CHECK2(checkGt, val1, val2))

#define ASSERT_GE(val1, val2) _ASSERT(GE, _CHECK2(checkGe, val1, val2))
#define EXPECT_GE(val1, val2) _EXPECT(GE, _CHECK2(checkGe, val1, val2))


//************************************************************
// Floating-point assertions
#define ASSERT_NEAR(val1, val2, abs_error) _ASSERT(NEAR, _CHECK3(near, val1, val2, abs_error))
#define EXPECT_NEAR(val1, val2, abs_error) _EXPECT(NEAR, _CHECK3(near, val1, val2, abs_error))

#define ASSERT_FLOAT_EQ(val1, val2) _ASSERT(FLOAT_EQ, _CHECK3(near, val1, val2, 0.0001))
#define EXPECT_FLOAT_EQ(val1, val2) _EXPECT(FLOAT_EQ, _CHECK3(near, val1, val2, 0.0001))


//************************************************************
// Exception throwing assertions
#define _CHECK_THROW(is_assert, name, code, excep, thrown, error)                                             \
      unit_tests::was_thrown = false;                                                                         \
      try                                                                                                     \
      {                                                                                                       \
          try                                                                                                 \
          {                                                                                                   \
              code;                                                                                           \
          }                                                                                                   \
          catch (excep) { unit_tests::was_thrown = true; }                                                    \
      }                                                                                                       \
      catch (...){}                                                                                           \
      unit_tests::Error_stream(is_assert, name, __LINE__).ctrl(                                               \
                               unit_tests::exceptionError(thrown == unit_tests::was_thrown, #code, error ) )  \


#define ASSERT_THROW(code, excep)  _CHECK_THROW(true,  "THROW",     code, excep &e, true, "did not throw an exception of class `" #excep "`")
#define ASSERT_ANY_THROW(code)     _CHECK_THROW(true,  "ANY_THROW", code, ...,      true, "did not throw any exception")
#define ASSERT_NO_THROW(code)      _CHECK_THROW(true,  "NO_THROW",  code, ...,      false, "threw an exception")

#define EXPECT_THROW(code, excep)  _CHECK_THROW(false, "THROW",     code, excep &e, true, "did not throw an exception of class `" #excep "`")
#define EXPECT_ANY_THROW(code)     _CHECK_THROW(false, "ANY_THROW", code, ...,      true, "did not throw any exception")
#define EXPECT_NO_THROW(code)      _CHECK_THROW(false, "NO_THROW",  code, ...,      false, "threw an exception")


//************************************************************
// C++ code
//************************************************************

namespace unit_tests
{
    //************************************************************
    // Variables and simple types

    //! Global-scope file stream used to report errors
    std::ofstream error_output_stream;

    //! Name of the test file (will be filled by the auto-generated main function)
    std::string this_file_name;

    //! Exit code of the program
    int exit_code = 0;

    //! Used in macros testing exceptions
    bool was_thrown;

    //! Exception type used to quit program
    class Assert_exception
    {};

    //! Function that sets the exit_code
    void signalFailure()
    {
        exit_code = 1;
    }

    //! Function that sets an error if program did not exit when it was supposed to
    void reportNotExiting(bool is_it_exit_test, const std::string& error)
    {
        if (is_it_exit_test)
        {
            error_output_stream << "\n" << error;
            signalFailure();
        }
    }

    //************************************************************
    // Printing

    // The code below does this thing: it detects if the printed type has already defined operator<<
    // for std::ostream - in other words, can it be printed to streams, like std::cout, std::stringstream etc.
    // If the type is detected to have this operator, this operator will be used to print the object of this type.
    // If the type has no such operator, then "(non-printable)" will be printed instead.
    // This is done so that the test code compiles, even when there is no way to print the class.

    //! This helper function will be well-defined (and thus exist, using SFINAE) if (printable) is true.
    //! If so, then the standard << operator will be used to print the printed object into the stream.
    template<class T, bool printable>
    typename std::enable_if<printable, void>::type defaultPrint(std::stringstream& ss, const T& object)
    {
        ss << object;
    }

    //! This one, on the other hand, will exist in printable is false and it is use
    //! to print that the type is non-printable.
    template<class T, bool printable>
    typename std::enable_if<!printable, void>::type defaultPrint(std::stringstream& ss, const T& object)
    {
        ss << "(non-printable)";
    }

    //! An user-extensible print function used to print all sort of classes and types.
    //! This function detects if the type has << operator defined and appropriate defaultPrint will be executed.
    template<class T>
    void print(std::stringstream& ss, T& object)
    {
        // First, a new class is created, called No_printable, that has a casting constructor.
        // So an object of type T can be casted into No_printable.
        struct No_printable
        {
            No_printable(T convert) { };
        };

        // Then, a << operator is declared (not need to define it) for the std::ostream and the No_printable,
        // that returns No_printable type - normally they return std::ostream (to allow chaining), but here
        // No_printable is returned.
        No_printable operator << (std::ostream&, const No_printable&);

        // Now, the detection part. The inside of decltype, that is: std::cout << std::declval<T>() will
        // return std::ostream for types T that have this operator defined (this is what we want to detect).
        // For types that don't have this operator, an implicit casting will occur, that will cast T to No_printable
        // type, and then the above defined operator<< will kick in, returning No_printable.
        // The value of the decltype will be std::ostream for printable types and No_printable otherwise.
        // Later, a simple std::is_same is used to check which one is returned and based on this an appropriate
        // printing function is selected.
        defaultPrint<T, !std::is_same<decltype(std::cout << std::declval<T>()), No_printable>::value>(ss, object);
    }

    //! This print version prints containers
    template< template<class ...> class Container, class ... Args >
    void print(std::stringstream& ss, Container<Args...>& object)
    {
        ss << '[';
        for (auto it = object.begin(); it != object.end(); ++it)
        {
            print(ss, *it);

            if (it + 1 != object.end())
            {
                ss << ",";
            }
        }
        ss << ']';
    }

    //! This is done, so that the above function for containers doesn't print std::string
    template <>
    void print(std::stringstream& stream, std::string& object)
    {
        stream << object;
    }

    //! Print uint8_t as a number
    template <>
    void print(std::stringstream& stream, uint8_t& object)
    {
        stream << static_cast<unsigned int>(object);
    }




    //************************************************************

    //! Simple function to invoke print
    template <class T>
    std::string usePrint(T object)
    {
        std::stringstream ss;
        print(ss, object);
        return ss.str();
    };

    //************************************************************
    // Error stream

    //! Class to control the Error_stream below
    class Error_stream_control
    {
        public:
            Error_stream_control(bool condition)
            {
                if (!condition)
                {
                    m_error_occurred = true;
                }
            }

            template <typename T>
            Error_stream_control& operator<<(T obj)
            {
                m_default_error.append(usePrint(obj));
                return *this;
            }

            bool errorOccurred() const
            {
                return m_error_occurred;
            }

            std::string defaultError() const
            {
                return m_default_error;
            }

        private:
            bool        m_error_occurred = false;
            std::string m_default_error;
    };

    //************************************************************

    //! Class used to print (stream) default error or custom user error
    class Error_stream
    {
        public:
            Error_stream(const bool& from_assert, const char* name, const int& line)
            {
                m_from_assert = from_assert;

                std::string name_str(name);
                if (name_str == "request_fail")
                {
                    m_default << "Failed at line `" << this_file_name << ":" << line << "`:\n";
                }
                else
                {
                    m_default << (from_assert ? "ASSERT_" : "EXCEPT_") << name << " failed at line `" << this_file_name << ":" << line << "`:\n";
                }
            }

            ~Error_stream() noexcept(false)
            {
                if (m_error_occurred)
                {
                    signalFailure();

                    // On destruction, print the selected stream to the error file
                    if (m_user_used)
                    {
                        error_output_stream << "\n" << m_user.str() << "\n";
                    }
                    else
                    {
                        error_output_stream << "\n" << m_default.str() << "\n";
                    }

                    if (m_from_assert)
                    {
                        // Yes, this is throwing an exception from a destructor. But Error_stream is an internal
                        // class of the framework and should only be used as a temporary object, so this destructor
                        // shouldn't be called in the process of stack unwinding.
                        throw Assert_exception();
                    }
                }
            }

            Error_stream& ctrl(const Error_stream_control& control)
            {
                m_error_occurred = control.errorOccurred();
                m_default << control.defaultError();

                return *this;
            }

            template <typename T>
            Error_stream& operator<<(T obj)
            {
                m_user_used = true;
                m_user << usePrint(obj);

                return *this;
            }

            template <typename T>
            Error_stream& operator+(T obj)
            {
                m_user_used = true;
                if (m_def_added_to_user == false)
                {
                    m_def_added_to_user = true;
                    m_user << m_default.str() << "\n";
                }

                m_user << usePrint(obj);

                return *this;
            }

        private:
            std::stringstream m_default;
            std::stringstream m_user;
            bool              m_user_used         = false;
            bool              m_def_added_to_user = false;
            bool              m_error_occurred    = false;
            bool              m_from_assert       = false;
    };

    //************************************************************
    // Checkers

    //! Special checker that always fails and is used to request fail
    _DEF_CHECK1(fail)
    {
        return Error_stream_control(val);
    }

    //! Is the value TRUE
    _DEF_CHECK1(checkTrue)
    {
        return Error_stream_control(val) << "    Code: `" << val_str << "` is not true\n"
                                         << "   Value: `" << val << "` is not true";

    }

    //! Is the value FALSE
    _DEF_CHECK1(checkFalse)
    {
        return Error_stream_control(!val) << "    Code: `" << val_str << "` is not false\n"
                                          << "   Value: `" << val << "` is not false";
    }

    //! Are 2 values equal
    _DEF_CHECK2(checkEq)
    {
        return Error_stream_control(val1 == val2) << "    Code: `" << val1_str << "` is not equal to `" << val2_str << "`\n"
                                                  << "   Value: `" << val1 << "` is not equal to `" << val2 << '`';
    }

    //! Are 2 values not equal
    _DEF_CHECK2(checkNe)
    {
        return Error_stream_control(val1 != val2) << "    Code: `" << val1_str << "` is not different than `" << val2_str << "`\n"
                                                  << "   Value: `" << val1 << "` is not different than `" << val2 << '`';
    }

    //! Is val 1 < val 2
    _DEF_CHECK2(checkLt)
    {
        return Error_stream_control(val1 < val2) << "    Code: `" << val1_str << "` is not less than `" << val2_str << "`\n"
                                                  << "   Value: `" << val1 << "` is not less than `" << val2 << '`';
    }

    //! Is val 1 <= val 2
    _DEF_CHECK2(checkLe)
    {
        return Error_stream_control(val1 <= val2) << "    Code: `" << val1_str << "` is not less than or equal to  `" << val2_str << "`\n"
                                                 << "   Value: `" << val1 << "` is not less than or equal to  `" << val2 << '`';
    }


    //! Is val 1 > val 2
    _DEF_CHECK2(checkGt)
    {
        return Error_stream_control(val1 > val2) << "    Code: `" << val1_str << "` is not greater than `" << val2_str << "`\n"
                                                  << "   Value: `" << val1 << "` is not greater than `" << val2 << '`';
    }

    //! Is val 1 >= val 2
    _DEF_CHECK2(checkGe)
    {
        return Error_stream_control(val1 >= val2) << "    Code: `" << val1_str << "` is not greater than or equal to  `" << val2_str << "`\n"
                                                 << "   Value: `" << val1 << "` is not greater than or equal to `" << val2 << '`';
    }

    //! Floating point comparison
    _DEF_CHECK3(near)
    {
        bool cond = (std::abs(val1 - val2) < val3);
        return Error_stream_control(cond) << "    Code: `" << val1_str << "` differs more than " << val3 << " from `" << val2_str << "`\n"
                                          << "   Value: `" << val1 << "` differs more than " << val3 << " from `" << val2 << '`';
    }

    //! Used to report exceptions errors
    Error_stream_control exceptionError(bool is_error, const char* val1_str, const char* val2_str)
    {
        return Error_stream_control(is_error) << "    The code: `" << val1_str << "\n    " << val2_str;
    }


}

