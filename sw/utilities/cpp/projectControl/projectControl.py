#!/usr/bin/env python3

import argparse
import logging
import os
import json
import subprocess
import sys

from projectControl.config import Config
from projectControl.utils import *
from projectControl.logger import *


class ProjectControl:

    APP_NAME = "projectControl"

    def __init__(self):
        self.config: Config
        self.modules = {}
        self.parser = None
        self.subparser = None
        self.args = None

        setup_logger()

        self.config_parser()
        self.parse_partial_args()

        if self.args.config is None:
            self.print_help_and_exit()

        self.read_config()

        self.load_commands()

        self.validate_commands()
        self.initialize_commands()

        # Parse commands arguments
        self.parse_args()

        self.execute_commands()

    def config_parser(self):

        self.parser = argparse.ArgumentParser(
            description="Script running commands to build, deploy, debug and develop C++ projects.",
            add_help=False
        )

        required_args = self.parser.add_argument_group('required arguments')
        required_args.add_argument("--config", action="store", default=None,
                                   help="Path to the project directory containing 'config.json' file.")

        self.parser.add_argument("--cmake", action="store", default=self.default_cmake(),
                                 help="Path to the cmake command.")
        self.parser.add_argument("-h", "--help", action="store_true",
                                 help="Print help.")

    @staticmethod
    def default_cmake():
        rc = subprocess.call(['which', 'cmake3'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        return "cmake3" if rc == 0 else "cmake"

    def load_commands(self):
        for command_dir in self.config.command_dirs:
            sys.path.append(command_dir)

            for file in os.listdir(command_dir):
                if file.endswith(".py"):
                    module_name = os.path.splitext(file)[0]
                    mod = load_module(module_name, os.path.join(command_dir, file))
                    self.modules[mod.__name__] = mod

    def validate_commands(self):
        functions = ['init', 'run']

        for name, module in self.modules.items():
            command = getattr(module, "Command", None)

            if command is None:
                fatal(f"Module '{name}' is missing 'Command' class.")

            for func in functions:
                method = getattr(command, func, None)
                if not callable(method):
                    fatal(f"Command class in module '{name}' is missing '{func}' function.")

    def initialize_commands(self):
        if self.subparser is None:
            self.subparser = self.parser.add_subparsers(required=False, dest="command")

        for name, module in self.modules.items():
            try:
                module.Command.init(self.subparser)
            except Exception as e:
                fatal(f"Initializing command '{name}' failed with following error: '{e}'")

    def parse_partial_args(self):
        self.args, _ = self.parser.parse_known_args()

    def parse_args(self):
        self.args = self.parser.parse_args()

    def print_help_and_exit(self):
        self.load_commands()
        self.validate_commands()
        self.initialize_commands()

        self.parse_args()
        sys.exit(0)

    def read_config(self):
        config_file = os.path.join(self.args.config, 'config.json')

        fatal_if_missing(config_file, "Config file cannot be found in the given path")

        with open(config_file, "r") as file:
            loaded_config = json.load(file)
            if self.APP_NAME not in loaded_config:
                fatal(f"Config file is missing '{self.APP_NAME}' config")

            self.config = Config(**loaded_config[self.APP_NAME], project_dir=self.args.config)

    def execute_commands(self):
        if self.args.command in self.modules:
            command = self.modules[self.args.command].Command
            command.run(self.args, self.config, self.config.project_args)
        else:
            self.parser.print_help()


if __name__ == "__main__":
    app = ProjectControl()
