# Project control

A script created to make operation on cmake project easier.

## Executing program

In order to run script, user have to run following command:
``` bash
projectControl.py <project_dir> <command> [params...]
```
Where:
* project_dir - path to project main directory, containing config.json file,
* command - the command to be executed for the project,
* params - parameters associated with selected command.

To get help information about specific command, user have to run following command:
``` bash
projectControl.py <project_dir> <command> -h
```

To see all available commands for given project, user have to run following command:
``` bash
projectControl.py <project_dir> -h
```

## Structure of config.json
A project needs to containg config.json file in order to run Project Control scipt.
The structure of the file is following:
{
    "projectControl": {
        "build_dir": "<path>",
        "source_dir": "<path>",
        "test_exec": "<path>",
        "command_dirs": [
            "<path>",
            "<path>",
            ...
        ]
    }
}

where:
test_exec - the name of the main executable file created in the project,
build_dir - path where to create build folder, relative to config.json directory,
source_dir - path to the directory containing main cmake file,
command_dirs - list of paths to the directories containing custom commands.

Remark:
All paths are realtive to config.json file.

## Custom commands
Script's user can provide set of custom commands for a project.
To do so, It is required to provide appropriate path in config.json file.
The path needs to point to directory containing *.py file, where every separate file is distinct command.
A command's file have to fulfill following requirements:
* Contain Command class.
* The class needs to implement two class methods: init and run.
* Init method is required to provide parsing details of given command. Such method takes two parameters:
** class - referance to class,
** parser - command subparser, enables to create parser associated with given command.
* Run method is responsible for making appropriate actions. Such method takes three parameters: class and opts, cfg.
** class - referance to class,
** opts - object containing parsed command line arguments,
** cfg - object containing informations from config.json file.

Example of a command is shown below:
``` python
class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("clean", help="Removes folders from project's build directory.")
        cls.subparser.add_argument("path", nargs="*", help="Path to directory to be removed.")
    
    @classmethod
    def run(cls, args, cfg):
        if len(opts.path) == 0:
            shutil.rmtree(cfg.build_dir, ignore_errors=True)
        else:
            for path in opts.path:
                deletePath = os.path.join(cfg.build_dir, path)
                shutil.rmtree(deletePath, ignore_errors=True)
```


# EOF