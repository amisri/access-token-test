import os
import pathlib
import shutil
import subprocess
import sys
import logging
import shlex
import re


from projectControl.utils import *
from projectControl.logger import ColorFormatter


class Valgrind:
    DEFAULT_CMD = "--tool=memcheck --leak-check=full --show-reachable=yes --num-callers=20 -s"

    VALGRIND_KEYWORDS = [r"([\d,]+ bytes)", r"([\d,]+ blocks)", r"([\d,]+ allocs)", r"([\d,]+ frees)", r"([\d,]+ errors)",
                         "(HEAP SUMMARY:)", "(LEAK SUMMARY:)", "(no leaks are possible)", "(SUMMARY:)", "(errors)", "(error[^s])"]

    VALGRIND_SKIP_LINES = ["Memcheck, a memory error detector", r"Copyright \(C\)", "rerun with -h for copyright info",
                           "Parent PID"]

    SKIP_PID = r"(==\d+==) (.*)"

    def __init__(self, args=None):
        self.args = args if args else self.DEFAULT_CMD

    def run(self, target_name, target_command, args):
        if isinstance(target_command, list):
            program = target_command
        else:
            program = [target_command]

        self.VALGRIND_SKIP_LINES.append(subprocess.list2cmdline(program))

        process = subprocess.Popen(["valgrind"] + shlex.split(self.args) + program + args,
                                   stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        while process.poll() is None:
            line = process.stdout.readline()

            if line is not None and len(line) > 0:
                self._parse_line(line.decode("utf-8"))

        if process.returncode != 0:
            print()
            fatal(f"Valgrind returned errors for target {target_name}")

        return process

    def _parse_line(self, line):
        # Skip certain lines
        for to_skip in Valgrind.VALGRIND_SKIP_LINES:
            if re.search(to_skip, line):
                return

        # Remove == PID == from the line
        split = re.split(self.SKIP_PID, line)
        if len(split) > 1:
            line = f"{split[2]}\n"

        # Highlight certain keywords
        for keyword in self.VALGRIND_KEYWORDS:
            line = re.sub(keyword, ColorFormatter.YELLOW + r"\1" + ColorFormatter.RESET, line, flags=re.I)

        print(line, end='')


