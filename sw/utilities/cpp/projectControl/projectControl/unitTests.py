from projectControl.utils import *
from projectControl.valgrind import *


class UnitTests:

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("-a", "--args", action="store",
                            help="Additional arguments passed to gtest.")
        parser.add_argument("-f", "--filter", action="store",
                            help="Optionals filter for gtest.")
        parser.add_argument("-c", "--compile", action="store_true",
                            help="Do not run cmake generation and only compile. "
                                 "Note: this will not take 'modules' parameter into account.")
        parser.add_argument("-n", "--number", action="store", default=1,
                            help="Repeats selected tests given number of times.")
        parser.add_argument("-s", "--stop_on_failure", action="store_true",
                            help="Stops tests execution on failure.")
        parser.add_argument("-v", "--verbose", action="store_true",
                            help="Show make commands.")
        parser.add_argument("-l", "--leaks", action="store", default=None, const="", nargs='?',
                            help="Run tests through valgrind. If empty, it uses default valgrind arguments.")

    @staticmethod
    def build(source_path, build_path, args, config_args="", build_args=""):
        logging.info(f"Building unit tests target:\n")

        build_project(args.cmake, build_path, source_path, f"Failed to build unit tests target",
                      only_compile=args.compile, verbose=args.verbose, config_args=config_args)

    @staticmethod
    def run(test_exec, filters, repeat, stop, leaks, args):
        valgrind_info = "" if leaks is None else "through valgrind"
        logging.info(f"Running unit tests {valgrind_info}:\n")

        fatal_if_missing(test_exec, f"Cannot find test executable at {test_exec}. Have you built the project?")

        filter_arg = f'--gtest_filter={filters}' if filters else ""

        cmd = [test_exec, filter_arg, f"--gtest_repeat={repeat}", "--gtest_color=yes"]

        if stop:
            cmd.append("--gtest_break_on_failure")

        if leaks is None:
            return runCommand(cmd, args=args, check=False)
        else:
            valgrind = Valgrind(leaks)
            return valgrind.run("unit_tests", cmd, [])
