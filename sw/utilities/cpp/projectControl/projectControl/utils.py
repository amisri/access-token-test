import os
import pathlib
import importlib.util
import shutil
import subprocess
import sys
import logging
import shlex
import platform

from projectControl.logger import ColorFormatter


def fatal(error):
    logging.error(error)
    sys.exit(1)


def fatal_if(cond, error):
    if not cond:
        fatal(error)


def fatal_if_empty(var, error):
    if not var:
        fatal(error)


def fatal_if_missing(path, error):
    if not os.path.exists(path):
        fatal(error)


def findFile(path, name):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)
    return None


def remove_dir(path):
    if os.path.exists(path):
        shutil.rmtree(path)


def load_module(name, path):
    fatal_if_missing(path, f"Failed to load module. No such file {path}")
    spec = importlib.util.spec_from_file_location(name, path)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    return mod


def check_build_folder(build_path):
    existed = os.path.exists(build_path)
    pathlib.Path(build_path).mkdir(parents=True, exist_ok=True)
    return existed


def build_project(cmake_cmd, build_path, source_path, error, only_compile=False, rebuild=False, verbose=False,
                  config_args="", build_args=""):
    if rebuild:
        remove_dir(build_path)

    if not check_build_folder(build_path):
        # If check_build_folder created the folder then we always run cmake configuration before compilation
        only_compile = False

    config_args += " --log-level=VERBOSE" if verbose else ""
    build_args += " -- VERBOSE=1" if verbose else ""

    if not only_compile:
        runCommand([cmake_cmd, '-S', source_path, '-B', build_path, "-DCMAKE_EXPORT_COMPILE_COMMANDS=1"], args=config_args, error=error)

    runCommand([cmake_cmd, '--build', build_path, '--parallel'], args=build_args, error=error)


def runCommand(cmd, *process_params, sudo=False, args=None, check=True, error="", **kwargs):
    if sudo:
        cmd = ['sudo', *cmd]

    if args:
        cmd.extend(shlex.split(args))

    # TODO When creating command variable below, take kwargs somehow into account
    command = subprocess.list2cmdline(cmd) #subprocess.list2cmdline([cmd, process_params , kwargs])

    logging.info_dim(f"Running command: {command}")
    result = subprocess.run(cmd, *process_params, **kwargs)

    command = subprocess.list2cmdline(result.args)
    logging.info_dim(f"Following command was executed: {command}\n")

    if not error:
        error = f"The following command failed: '{command}'"

    if check and result.returncode != 0:
        print()
        fatal(f"{error}")

    return result


def open_by_system(path):
    if 'microsoft' in platform.uname().release.lower():
        subprocess.run(["powershell.exe", "-c", path])
    else:
        subprocess.run(["xdg-open", path], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)


def updateArgsNamespace(namespace, key, value):
    data = vars(namespace)
    data[key] = value

