import os
import pathlib
import shutil
import subprocess
import sys
import logging
import shlex
import re

from projectControl.utils import *

# TODO This is ugly, refactor this
STATIC_CHECK_PATH = os.path.join(os.path.dirname(__file__), '..', '..', 'staticChecks')
sys.path.append(STATIC_CHECK_PATH)
from staticChecks import StaticChecks


class StaticCheck:
    SCRIPT = os.path.join(STATIC_CHECK_PATH, 'staticChecks.py')

    def __init__(self, parser):
        fatal_if_missing(self.SCRIPT,
                         f"The static checks script at {self.SCRIPT} doesn't exist, please update command's staticChecks path.")

        StaticChecks.add_arguments_to_parser(parser)

    def run(self, args, db_path, config_path):
        if db_path is not None:
            updateArgsNamespace(args, 'build', db_path)

        if config_path is not None:
            updateArgsNamespace(args, 'path', config_path)

        checker = StaticChecks()
        checker.set_parsed_arguments(args)
        checker.run()
