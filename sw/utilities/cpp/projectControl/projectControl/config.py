from dataclasses import dataclass, InitVar
from typing import List
import os

from projectControl.utils import *


@dataclass
class Config:
    project_dir:  InitVar[str]

    source_dir:   str
    build_dir:    str
    command_dirs: List[str]
    project_args: dict

    def __post_init__(self, project_dir):
        # Validate every option
        fatal_if_empty(self.source_dir, "The config option 'source_dir' is not defined or is empty.")
        fatal_if_empty(self.build_dir, "The config option 'build_dir' is not defined or is empty.")
        fatal_if_empty(self.command_dirs, "The config option 'command_dirs' is not defined or is empty.")

        # Add project directory to the paths
        self.command_dirs = [os.path.join(project_dir, path) for path in self.command_dirs]
        self.build_dir = os.path.join(project_dir, self.build_dir)
        self.source_dir = os.path.join(project_dir, self.source_dir)
