import logging


class ColorFormatter(logging.Formatter):

    GRAY = "\x1b[38;20m"
    YELLOW = "\x1b[38;5;214m"
    RED = "\x1b[38;5;15;48;5;160m"
    BOLD_RED = "\x1b[38;5;9;48;5;236m"
    BLUE = "\x1b[38;5;51m"
    BLUE_DIM = "\x1b[38;5;12m"
    GREEN = "\x1b[38;5;118m"
    RESET = "\x1b[0m"
    FORMAT = ">>> %(message)s "

    # Add custom levels
    logging.INFO_DIM = logging.INFO + 1
    logging.SUCCESS = logging.INFO + 2

    FORMATS = {
        logging.DEBUG: BOLD_RED + FORMAT + RESET,
        logging.INFO: BLUE + FORMAT + RESET,
        logging.INFO_DIM: BLUE_DIM + FORMAT + RESET,
        logging.SUCCESS: GREEN + FORMAT + RESET,
        logging.WARNING: YELLOW + FORMAT + RESET,
        logging.ERROR: RED + FORMAT + RESET,
        logging.CRITICAL: BOLD_RED + FORMAT + RESET
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)




def setup_logger():
    # for i in range(1, 300):
        # print(f"\x1b[38;5;{i};48;5;15m This is a sample ABC text {i} ")
        # print(f"\x1b[38;5;{i}m This is a sample ABC text {i} ")

    # Create logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # Add custom levels
    logging.addLevelName(logging.INFO_DIM, 'SUCCESS')
    setattr(logging, 'info_dim', lambda *args: logger.log(logging.INFO_DIM, *args))
    logging.addLevelName(logging.SUCCESS, 'SUCCESS')
    setattr(logging, 'success', lambda *args: logger.log(logging.SUCCESS, *args))

    # Create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # Add formatter to ch
    ch.setFormatter(ColorFormatter())

    # Add ch to logger
    logger.addHandler(ch)

