function expandErrors() {
    let buttons = document.getElementsByTagName('a')
    for (let index = 0; index < buttons.length; index++) {
        if('false' === buttons[index].getAttribute('aria-expanded') &&
            isError(buttons[index])) {
            buttons[index].click()
            }
    }
}

function isError(element) {
    var number = parseInt(element.text)
    if (isNaN(number)) return false
    if (number > 0)    return true
    else               return false
}

function hideErrors() {
    let buttons = document.getElementsByTagName('a')
    for (let index = 0; index < buttons.length; index++) {
        if('true' === buttons[index].getAttribute('aria-expanded'))
            buttons[index].click()
    }
}
