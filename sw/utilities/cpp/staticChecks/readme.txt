1. Installing static-checkers

	1.1 Clang compiler

	wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
	sudo apt-add-repository "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-6.0 main"
	sudo apt-get update
	sudo apt-get install  clang-6.0 clang-tools-6.0 clang-tidy-6.0


	1.2 CPPCheck

	sudo apt-get install cppcheck

	1.3 Create and run python virtual environment 

	python3 -m venv venv
	source ./venv/bin/activate

	1.4 Install dependencies

	pip install -r requirements.txt

