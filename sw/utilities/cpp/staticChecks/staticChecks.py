#!/usr/bin/env python3

##########################################
# Static analyzers runner
import json
import argparse
import os
import importlib.util

from utils.model import FileReport, Config
from utils.misc import fatal, findFile
from jinja2 import Environment, PackageLoader, select_autoescape


##########################################
# Checker module interface
#
# def get_name():
#       Returns name of the checker that will be use to identify results from this checker.
#
# def init(config):
#       Invoked once, before checking files. The 'config' is a instance of class Config that has those fields:
#           build_path          - path to the folder containing compilation database
#           checkers_config     - dictionary mapping checker name to custom checker configuration. It contains:
#               key             - checker name
#               value           - custom checker configuration wrapped into CheckerConfig. CheckerConfig contains:
#                   name        - checker name
#                   params      - string containing custom checker arguments
#       If the initialization is successful, the init function must return True, otherwise the checker will be excluded.
#
# def check_file(file, compilation_db_path, params):
#       Runs checks on the passed file path. The 'compilation_db_path' is a path to compilation database.
#       The 'params' is a string containg custom checker arguments.
#
# def clean_up():
#       Performs (optional) clean up, e.g. removing temporary files.
#


##########################################
# Globals
class FontColors:
    file = '\033[95m'
    checker = '\033[94m'
    error = '\033[31m'
    end = '\033[0m'


##########################################
# Arguments definitions

class StaticChecks:
    CONFIG_FILENAME = "config.json"
    OUTPUT_FILENAME = "staticChecks.html"
    APPLICATION_NAME = "staticChecks"

    def __init__(self):
        self.args = []
        self.checkers_list = []
        self.used_checkers_list = []
        self.report = {}
        self.config = Config([], "", {})
        self.compilation_db = None
        self.compilation_db_path = None

        self.env = Environment(
            loader=PackageLoader("staticChecks"),
            autoescape=select_autoescape()
        )

    @staticmethod
    def add_arguments_to_parser(parser):
        parser.add_argument('-l', '--list', dest='list', action="store_true", default=False,
                            help='Prints names of checkers found in checkers directory and exits')

        parser.add_argument('-u', '--use', dest='use', nargs=1, default=[],
                            help='A comma-separated list of checkers to use. By default all checkers are used')

        parser.add_argument('-o', '--out', dest='out', action="store", default=None,
                            help='Path of the output file, defaults to current working directory.')

        parser.add_argument('-m', '--mode', dest='mode', nargs=1, default='html', choices=['html', 'stdout'],
                            help='Setting \'html\' (default) produces website as a file, \'stdout\' produces gcc-like output to the stdout.')

        parser.add_argument('-p', '--path', dest='path', default=os.getcwd(),
                            help='Path to project to be analysed.')

        parser.add_argument('-b', '--build', dest='build', nargs="?",
                            help='Path to the folder containing compilation database.')

    def set_parsed_arguments(self, args):
        self.args = args

    def gen_checkers_list(self):
        self_dir = os.path.dirname(os.path.realpath(__file__))
        checkers_dir = os.path.join(self_dir, 'checkers')
        checkers_files = os.listdir(checkers_dir)

        self.checkers_list.clear()

        for file in checkers_files:
            if file.endswith(".py"):
                spec = importlib.util.spec_from_file_location("module.name", os.path.join(checkers_dir, file))
                mod = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(mod)

                self.checkers_list.append({'mod': mod, 'file': file})

    def validate(self):
        # Check that there are any checkers to run
        if not self.checkers_list:
            fatal("No checkers scripts found in checkers directory.")

        # Make sure that checkers modules have expected functions
        module_funs = ('get_name', 'init', 'check_file', 'clean_up')

        for checker in self.checkers_list:
            # Check that checker module has all expected functions
            for func in module_funs:
                try:
                    getattr(checker['mod'], func)
                except Exception:
                    fatal("Checker module '{}' does not have '{}' function.".format(checker['file'], func))

            # If so, add names returned from modules as names of checkers
            checker['name'] = checker['mod'].get_name()

        # If the --use argument is not provided, copy checkers_list as used checkers
        if not self.args.use:
            self.used_checkers_list = self.checkers_list
        else:
            # Otherwise, check that checkers selected via command line are valid ones and if so, copy the right one
            for use_checker in self.args.use:
                found = None

                for checker in self.checkers_list:
                    if checker['name'] == use_checker:
                        found = checker
                        break

                if not found:
                    self.print_checker()
                    print("")
                    fatal("Checker '{}' was selected to be used (via option -u (--use)), but it does not exists.".format(use_checker))
                else:
                    self.used_checkers_list.append(found)

        # Validate that there are any checkers to run
        if not self.used_checkers_list:
            fatal("No checkers were selected to be used. Terminating.")

    def print_checker(self):
        print('List of found checkers:\n')

        for i, checker in enumerate(self.checkers_list):
            print('Checker {:<2} :  {:<20}  from file \'{}\' '.format(i + 1, checker['name'], checker['file']))

    def read_config_file(self):
        # Try to find a compilation database
        config_path = os.path.join(self.args.path, StaticChecks.CONFIG_FILENAME)

        if not os.path.isfile(config_path):
            fatal(f"Static checker configuration was not found in given directory '{self.args.path}'.")

        # Once there is the compilation database, store it as a list of dictionaries inside a config variable
        with open(config_path, "r") as configFile:
            loaded_config = json.load(configFile)
            if StaticChecks.APPLICATION_NAME not in loaded_config:
                fatal(f"Config file in '{self.args.path}' is missing '{self.APPLICATION_NAME}' field.")

            try:
                self.config = Config(**loaded_config[StaticChecks.APPLICATION_NAME])
            except TypeError as e:
                fatal(f"Static checks config file is missing all required fields: {e}")

    def configure(self):
        self.read_config_file()

        # Overwrite compilation database location
        if self.args.build is not None:
            self.config.build_path = self.args.build

    def load_compilation_db(self):
        # Construct project build path
        build_dir = os.path.abspath(self.config.build_path)

        # Find compilation database in build directory
        db_path = findFile(build_dir, "compile_commands.json")

        if not db_path:
            fatal(f"Cannot find a compilation database in '{build_dir}'. Make sure that the file was generated.")

        self.compilation_db_path = os.path.dirname(db_path)

        try:
            # Once there is the compilation database, store it as a list of dictionaries inside a config variable
            with open(db_path, "r") as db_file:
                loaded_db = json.load(db_file)

            # Keep only object files from paths provided by the user
            source_path = os.path.normpath(os.path.join(self.args.path, self.config.source_path))
            self.compilation_db = [x for x in loaded_db if source_path in x['file']]

        except Exception:
            fatal("Error while retrieving compilation database")

    def init_checkers(self):
        # Validate checkers, i.e. execute their init functions, and if the result isn't True then exclude this checker from the list
        initialized_checkers = []
        for checker in self.used_checkers_list:
            if checker['mod'].init(self.config):
                initialized_checkers.append(checker)
            else:
                print(f"ERROR: Initializing checker {checker['name']} failed.")
        self.used_checkers_list = initialized_checkers

    def run_checkers(self):
        # Iterate over files
        for index, record in enumerate(self.compilation_db):
            file = record['file']
            print("Checking file {:>3} / {:<3}: {} with: ".format(
                index + 1, len(self.compilation_db), FontColors.file + os.path.basename(file) + FontColors.end), end='')
            self.report[file] = FileReport(file, 0, [])

            for checker in self.used_checkers_list:
                # Retrieve configuration for given checker
                checker_params = None
                if checker['name'] in self.config.checkers_config:
                    checker_params = self.config.checkers_config[checker['name']].params

                # Check the file with the current checker
                checker_report = checker['mod'].check_file(file, self.compilation_db_path, checker_params)

                self.report[file].checkerReports.append(checker_report)
                self.report[file].errorsCount += checker_report.errorsCount

                # Report number of errors
                if checker_report.errorsCount > 0:
                    output_str = f"{FontColors.error}{checker['name']}({str(checker_report.errorsCount)}){FontColors.end}..."
                else:
                    output_str = f"{FontColors.checker}{checker['name']}({str(checker_report.errorsCount)}){FontColors.end}..."
                print(output_str, end='')

            print("\n")

    def output_website(self):
        template = self.env.get_template("report.html")
        output_path = self.args.out if self.args.out is not None else self.config.build_path

        output_file_path = os.path.abspath(os.path.join(output_path, StaticChecks.OUTPUT_FILENAME))
        output_dir = os.path.dirname(output_file_path)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        with open(output_file_path, 'w') as f:
            f.write(template.render(reports=self.report))

        # TODO convert path to absolute before printing
        print(f"file://{output_file_path}")

    def output_stdout(self):
        for file in self.report:
            for checker in self.report[file].checkerReports:
                if checker.rawOutput:
                    print(checker.rawOutput)

    def clean_up(self):
        if not all(checker['mod'].clean_up() for checker in self.used_checkers_list):
            fatal("Checkers cleanup failed.")

    def run(self):
        # Prepare list of checkers
        self.gen_checkers_list()

        # Perform some checks and prepare variables
        self.validate()

        if self.args.list:
            # List found checkers
            self.print_checker()
        else:
            self.configure()

            # Run make via bear to generate compilation database
            self.load_compilation_db()

            self.init_checkers()

            # Run checkers
            self.run_checkers()

            # Generate output as website or gcc-like
            if 'html' in self.args.mode:
                self.output_website()
            if 'stdout' in self.args.mode:
                self.output_stdout()

            # Remove compilation database (if cleanup selected)
            self.clean_up()


##########################################
# Functions
def parse_args(checker):
    parser = argparse.ArgumentParser(description='Runs static checkers.')
    StaticChecks.add_arguments_to_parser(parser)

    # Parse args
    checker.set_parsed_arguments(parser.parse_args())


##########################################
# Entry point
if __name__ == "__main__":
    checker_obj = StaticChecks()
    parse_args(checker_obj)

    checker_obj.run()

