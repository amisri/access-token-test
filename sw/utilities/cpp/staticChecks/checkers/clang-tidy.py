import subprocess
import re
import shlex

from shutil import which
from utils.model import CheckerError, CheckerReport


class TidyCheck:
    regex = r"([^:^\n]+:\d+:\d+):\s(\w+\s*\w*):\s(.+)\s+(.+)(.*)\s*[\^]?"
    executable = "clang-tidy"


def get_name():
    return 'Clang-tidy'


def init(config):
    return which(TidyCheck.executable) is not None


def check_file(file, compilation_db_path, params) -> CheckerReport:
    # Prepare command
    if params is not None:
        cmd = [TidyCheck.executable, *shlex.split(params), '-p', compilation_db_path, file]
    else:
        cmd = [TidyCheck.executable, '-p', compilation_db_path, file]

    # Execute clang-check and get output
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = result.stdout.decode('utf-8')

    # Parse the output
    matches = re.finditer(TidyCheck.regex, out, re.MULTILINE)

    # Prepare dict to store errors
    errors = []

    for match in matches:
        errors.append(CheckerError(match.group(1), match.group(2), match.group(3), match.group(4)))

    return CheckerReport(get_name(), out, errors)


def clean_up():
    return True
