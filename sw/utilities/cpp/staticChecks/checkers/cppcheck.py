import re
import subprocess
import shlex

from shutil import which
from utils.model import CheckerError, CheckerReport


class CppCheck:
    regex = r"([^:^\n]+:\d+:\d+):\s(\w+\s*\w*:?\w*):\s(.+)\s+(.+)(.*)?"
    executable = "cppcheck"


def get_name():
    return 'CPP-Check'


def init(config):
    return which(CppCheck.executable) is not None


def check_file(file, compilation_db_path, params) -> CheckerReport:
    # Prepare command
    if params is not None:
        cmd = [CppCheck.executable, '--verbose', *shlex.split(params), file]
    else:
        cmd = [CppCheck.executable, '--enable=style,performance,portability,information,missingInclude',
               '--inconclusive', '--force', '--verbose', '--suppress=missingIncludeSystem', file]

    # Execute clang-check and get output
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = result.stderr.decode('utf-8')

    # Parse the output
    matches = re.finditer(CppCheck.regex, out, re.MULTILINE)

    # Prepare dict to store errors
    errors = []

    for match in matches:
        errors.append(CheckerError(match.group(1), match.group(2), match.group(3), match.group(4)))

    return CheckerReport(get_name(), out, errors)


def clean_up():
    return True
