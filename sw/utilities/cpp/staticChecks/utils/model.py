from dataclasses import dataclass, field, InitVar
from typing import List, Dict
from utils.misc import fatal


@dataclass
class CheckerError:
    location: str
    type: str
    text: str
    code: str

    def __str__(self):
        return (f"type:    \t{self.type}\n"
                f"message: \t{self.text}\n"
                f"location:\t{self.location}\n"
                f"code:    \t{self.code}\n")


@dataclass
class CheckerReport:
    checkerName: str
    rawOutput: str
    errors: List[CheckerError]
    errorsCount: int = field(init=False)

    def __post_init__(self):
        self.errorsCount = sum([1 for error in self.errors if error.type != 'note'])


@dataclass
class FileReport:
    filename: str
    errorsCount: int
    checkerReports: List[CheckerError]


@dataclass
class CheckerConfig:
    name: str
    params: str


@dataclass
class Config:
    checkers: InitVar[List[str]]

    build_path: str
    source_path: str
    checkers_config: Dict[str, CheckerConfig] = field(default_factory=lambda: {})

    def __post_init__(self, checkers):
        for checker in checkers:
            checker_config = CheckerConfig(**checker)
            if checker_config.name not in self.checkers_config:
                self.checkers_config[checker_config.name] = checker_config
            else:
                fatal(f"Configuration for checker '{checker_config.name}' was already provided.")
