import sys
import os


def fatal(error):
    print('Error : {}'.format(error))
    sys.exit(1)


def findFile(path, name):
    for root, _, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)
    return None
