#include <api/hal.h>
#include <cpp_utils/files.h>

#include <stdio.h>


int main(int argc, char** argv) {
    if (argc < 2) {
        fprintf(stderr, "usage: fgc4_flash <image>\n");
        return 1;
    }

    fprintf(stderr, "fgc4_flash: note: initializing HAL\n");
    auto hal = fgc::hal::Hal::create(fgc::hal::Version::v32);
    auto fpga = hal->getFpgaController();

    fprintf(stderr, "fgc4_flash: note: loading image file\n");
    auto image = utils::readFromBinaryFile(argv[1]);

    fprintf(stderr, "fgc4_flash: note: programming...\n");
    bool res = fpga->programAndVerify(image);

    fprintf(stderr, "fgc4_flash: programAndVerify returned %s\n", res ? "TRUE" : "FALSE");

    return res ? 0 : 1;
}
