#!/bin/sh

echo 505 > /sys/class/gpio/export
echo 507 > /sys/class/gpio/export
echo 508 > /sys/class/gpio/export
echo 509 > /sys/class/gpio/export

echo -n "Conf_mem_prog_en:"
cat /sys/class/gpio/gpio505/value

echo -n "Fpga_conf_done:  "
cat /sys/class/gpio/gpio507/value

echo -n "Fpga_init:       "
cat /sys/class/gpio/gpio508/value

echo -n "Fpga_prog_rst:   "
cat /sys/class/gpio/gpio509/value

# To reset the FPGA
# Release flash, then toggle Fpga_prog_rst
# echo 1 > /sys/class/gpio/gpio505/value
# echo out > /sys/class/gpio/gpio509/direction
# echo 0 > /sys/class/gpio/gpio509/value
# echo 1 > /sys/class/gpio/gpio509/value

# To erase the flash (fill with 0xff)
# echo 0 > /sys/class/gpio/gpio505/value
# tr '\0' '\377' < /dev/zero | dd of=/dev/mtdblock0 bs=4096 count=4096
# Note: bs=16777216 count=1 should do a chip erase, but DD reports an incomplete write :shrug:

# Program the flash from a file
# echo 0 > /sys/class/gpio/gpio505/value
# dd if=top_mf.bit of=/dev/mtdblock0 bs=4096
