#ifdef __aarch64__
#include <api/hal.h>
#include <cpp_utils/files.h>
#include <fgc32/inc/mem_map.32.h>
#endif

#include <algorithm>
#include <math.h>
#include <numeric>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <vector>

#ifndef __aarch64__
// x86 fallback
static int NOW() {
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    return tp.tv_sec * 1'000'000'000 + tp.tv_nsec;
}
#endif

int main(int argc, char** argv) {
#ifdef __aarch64__
    // if (argc < 2) {
    //     fprintf(stderr, "usage: fgc4_timing_test <image>\n");
    //     return 1;
    // }

    fprintf(stderr, "fgc4_timing_test: initializing HAL\n");

    auto hal = fgc::hal::Hal::create(fgc::hal::Version::v32);
    auto fpga = hal->getFpgaController();
    auto& ifc = fpga->getIfcRegion();

    // fprintf(stderr, "fgc4_timing_test: note: loading image file\n");
    // auto image = utils::readFromBinaryFile(argv[1]);

    // fprintf(stderr, "fgc4_timing_test: note: programming FPGA...\n");
    // bool res = fpga->programAndVerify(image);

    // fprintf(stderr, "fgc4_flash: programAndVerify returned %s\n", res ? "TRUE" : "FALSE");

    // if (!res) {
    //     return 1;
    // }

    // sleep(2);

    // Start timer
    ifc.write<uint32_t>(0x1b70, 0x02);

#define NOW() ifc.read<uint32_t>(0x1b7c)

#endif

    printf("sample 1: %08X\n", NOW());
    printf("sample 2: %08X\n", NOW());

    std::vector<int> deltas;
    constexpr int num_attempts = 1000;
    deltas.resize(num_attempts);

    // struct timespec ts;
    // memset(&ts, 0, sizeof(ts));
    // ts.tv_nsec = 1'000'000;

    int NUM_ITER = 10'000'000;

    printf("running %d tests with a %dM-iteration NOP loop\n", num_attempts, NUM_ITER/1'000'000);
    // printf("running %d tests with a delay of %d ns\n", num_attempts, ts.tv_nsec);

    for (int attempt = 0; attempt < num_attempts; attempt++) {
        auto pre = NOW();
        // nanosleep(&ts, nullptr);
        for (int i = 0; i < NUM_ITER; i++) {
            __asm__ __volatile__ ("nop");
        }
        auto post = NOW();

        deltas[attempt] = post - pre;
    }

#ifdef __aarch64__
    // Stop timer
    ifc.write<uint32_t>(0x1b70, 0);
#endif

    // Drop the first 10 % samples
    deltas.erase(deltas.begin(), deltas.begin() + num_attempts / 10);

    // Compute statistics
    // https://stackoverflow.com/a/12405793
    double sum = std::accumulate(std::begin(deltas), std::end(deltas), 0.0);
    double m =  sum / deltas.size();

    double accum = 0.0;
    std::for_each (std::begin(deltas), std::end(deltas), [&](const double d) {
        accum += (d - m) * (d - m);
    });

    double stdev = sqrt(accum / (deltas.size()-1));

    printf("Mean: %14.6f\n", m);
    printf("Ssd:  %14.6f\n", stdev);

    const auto [min, max] = std::minmax_element(std::begin(deltas), std::end(deltas));

    printf("Min:  %7d\n", *min);
    printf("Max:  %7d\n", *max);
}
