set(_WEAKER_MODULE_BASE_DIR "${CMAKE_CURRENT_LIST_DIR}")

macro(make_symbols_weak targetName files)
    # Set MAKE_WEAK_ARG_EXCLUDE_TESTS variable based on optional parameter - EXCLUDE_TESTS
    cmake_parse_arguments(
        MAKE_WEAK_ARG
        "EXCLUDE_TESTS"
        ""
        "" ${ARGN})

    if(MAKE_WEAK_ARG_EXCLUDE_TESTS)
        # Filter all object file paths consisting of folder 'tests'
        set(files_result "$<FILTER:${files},EXCLUDE,.*/tests/.*>")
    else()
        set(files_result "${files}")
    endif(MAKE_WEAK_ARG_EXCLUDE_TESTS)

    # Join list of files into one string, using space as separator
    set(files_result "$<JOIN:${files_result},\ >")

    add_custom_command(TARGET 
        ${targetName} PRE_LINK
        COMMAND
            ${_WEAKER_MODULE_BASE_DIR}/command.py
        ARGS 
            ${files_result}
    )
endmacro()

# EOF