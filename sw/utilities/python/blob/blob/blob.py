from functools import partial
from itertools import chain

import blob.constants as bc
import blob.utils as utils
from blob.migrator import BlobMigrator
from blob.parameters import Parameter

class Blob:
    '''
    '''
    def __init__(self, blob_str='', blob_dict=dict(), blob_version=2, converter_type=''):
        self._blob_dec_list = list()
        self._blob_dict = dict()
        self._version = 0
        self._converter_type = converter_type

        self._build_blob_representations(blob_str, blob_dict, blob_version)

    @classmethod
    def from_string(cls, blob_str):
        return Blob(blob_str=blob_str)

    @classmethod
    def from_dict(cls, blob_dict):
        return Blob(blob_dict=blob_dict)

    def as_string(self, base=10):
        if base == 10:
            return ','.join(list(map(str, self._blob_dec_list)))

        if base == 16:
            return ','.join(list(map('0x{:08X}'.format, self._blob_dec_list)))

        raise BlobException(f'Blob translation into base {base} not implemented')

    def as_dict(self):
        return self._blob_dict

    def as_json(self):
        raise NotImplementedError
    
    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        try:
            shifted_version = version << 16
            updated_first_element = shifted_version | self._blob_dec_list[0] & 0x0000FFFF
            self._blob_dec_list[0] = updated_first_element

        except IndexError:
            self._blob_dec_list.append(shifted_version)

        self._version = version

    @property
    def slots(self):
        return list(self._blob_dict.keys())

    def devices(self, slot):
        try:
            devices = list(self._blob_dict[slot].keys())

        except KeyError as ke:
            raise KeyError(f'Blob does not have slot {slot}') from ke
        
        else:
            return devices

    def blocks(self, slot, device):
        try:
            blocks = list(self._blob_dict[slot][device].keys())

        except KeyError as ke:
            raise KeyError(f'Blob does not have device {device} in slot {slot}') from ke
        
        else:
            return blocks

    def params(self, slot, device, block, base=16):
        #TODO: return dictionary of param objects
        try:
            params = self._blob_dict[slot][device][block]

        except KeyError as ke:
            raise KeyError(f'Blob does not have block {block} in device {device} at slot {slot}') from ke
        
        else:
            if base == 16:
                return params

            elif base == 10:
                return {param_number:int(param_value,  base=16) for param_number,param_value in params.items()}
            
            else:
                raise NotImplementedError(f'Parameter conversion to base {base} not implemented')
    
    def take(self, init, end):
        return self._blob_dec_list[init:end]

    def migrate(self, to=2):
        try:
            b = Blob(blob_str=BlobMigrator.migrate(self, to))

        except KeyError as ke:
            raise BlobException(f'Migration not allowed from {self._version} to {to}') from ke

        else:
            return b

    def __len__(self):
        return len(self._blob_dec_list)

    def __getitem__(self, item):
        return self._blob_dec_list[item]

    def _build_blob_representations(self, blob_str, blob_dict, blob_version):
        if blob_str and blob_dict:
            raise NotImplementedError('Blob construction from string and dictionary not implemented!')

        if not blob_str and not blob_dict:
            self._blob_dec_list = list()
            self._blob_dict = dict()
            self._version = 0
            return
            
        # Try from dict or string
        if blob_dict:
            self._build_from_dict(blob_dict, blob_version)

        else: 
            self._build_from_str(blob_str)

    def _build_from_str(self, blob_str):
        # Initialize version 
        self._version = 0
        
        # In dec or hex format?
        try:
            self._blob_dec_list = list(map(int, blob_str.split(',')))

        except ValueError:
            # Blob is in hexadecimal format
            self._build_from_hex_string(blob_str)
                    
        self._version = int(self._blob_dec_list[0]) >> 16
        self._run_checks()

    def _build_from_hex_string(self, blob_str):
        try:
            self._blob_dec_list = list(map(lambda x: int(x, base=16), blob_str.split(',')))

        except ValueError as ve:
            raise BlobException('Blob string not given in either decimal or hexadecimal formats!') from ve

    def _run_checks(self):
        # Check version 
        if self._version not in bc.BLOB_VERSIONS:
            raise BlobException(f'Blob version not recognized! Valid versions: {",".join(map(str, bc.BLOB_VERSIONS))}')

        # Check magic word presence
        magic_word = [idx for idx, el in enumerate(self._blob_dec_list) if el & 0x0000FFFF == bc.MAGIC_WORD]
        try:
            magic_word_idx = magic_word[0]

        except IndexError as ie:
            raise BlobException('Magic word not found in blob!') from ie

        # Check slot, devices, blocks and number of params values
        try:
            self._blob_str_to_dict(magic_word_idx, bc.versions_to_meta_data_info[self._version]['param_idx_size'])

        except BlobException:
            self._version = 1.5
            self._blob_str_to_dict(magic_word_idx, bc.versions_to_meta_data_info[1.5]['param_idx_size'])

    def _build_from_dict(self, blob_dict, blob_version):
        # Check slot, devices, blocks and number of params values
        for slot in blob_dict.keys():
            if slot > bc.SCIVS_N_SLOTS:
                raise BlobException(f'Slot value {slot} not allowed (max {bc.SCIVS_N_SLOTS})')

            for device in blob_dict[slot].keys():
                if device not in bc.SCIVS_DEVICES:
                    raise BlobException(f'Device {device} value not allowed for slot {slot}')

                for block in blob_dict[slot][device].keys():
                    if block > bc.SCIVS_N_BLOCKS:
                        raise BlobException(f'Block value {block} for device {device}, slot {slot} not allowed (max {bc.SCIVS_N_BLOCKS})')

                    block_params = blob_dict[slot][device][block]
                    if len(block_params) > bc.SCIVS_MAX_BLOCK_SIZE:
                        raise BlockException(f'Number of parameters for block {block} exceeds maximux allowed (slot {slot}, device {device})')

        self._build_dict_for_converter_type(blob_dict)
        self._version = blob_version
        self._blob_dict_to_str(blob_version)

    def _blob_str_to_dict(self, magic_word_idx, parameter_index_size): 
        meta_blob  = self._blob_dec_list[0:magic_word_idx+1]
        param_blob = self._blob_dec_list[magic_word_idx + 1:]

        # Divide each 32-bit element of meta_blob in two 16-bit words
        meta_blob_16_bit = list(map((lambda x: ((x >> 16) & 0xFFFF, x & 0x0000FFFF)), meta_blob))

        # Flatten the above list of tuples
        meta_blob_16_bit = list(chain.from_iterable(meta_blob_16_bit))

        # Get rid of blob version and magic word
        meta_blob_16_bit.pop(0)
        meta_blob_16_bit.pop()

        # Extract and store parameters 
        blob_dict = dict()
        slot, device, block, pos_blob = 0, 0, 0, 0

        for _, m in enumerate(meta_blob_16_bit):
            slot = pos_blob // (bc.SCIVS_N_BLOCKS * bc.SCIVS_N_DEVICES)
            if slot > bc.SCIVS_N_SLOTS:
                raise BlobException(f'Slot value {slot} not allowed (max {bc.SCIVS_N_SLOTS})')

            mod_pos_blob = pos_blob % (bc.SCIVS_N_BLOCKS * bc.SCIVS_N_DEVICES)

            if mod_pos_blob < bc.SCIVS_N_BLOCKS:
                device, block = 'MF', mod_pos_blob

            else:
                device, block = 'DEVICE_2', mod_pos_blob - bc.SCIVS_N_BLOCKS

            if block > bc.SCIVS_N_BLOCKS:
                raise BlobException(f'Block value {block} for device {device}, slot {slot} not allowed (max {bc.SCIVS_N_BLOCKS})')

            block_size, param_index = ((m >> parameter_index_size) & 0xFF), (m & (2 ** parameter_index_size - 1))
            if block_size > bc.SCIVS_MAX_BLOCK_SIZE:
                raise BlobException(f'Block size {block_size} for block {block}, device {device}, slot {slot} not allowed (max {bc.SCIVS_MAX_BLOCK_SIZE})')
                
            if block_size:
                try:
                    blob_dict[slot]

                except KeyError:
                    blob_dict[slot] = dict()

                try:
                    blob_dict[slot][device]

                except KeyError:
                    blob_dict[slot][device] = dict()

                try:
                    blob_dict[slot][device][block]

                except KeyError:
                    #TODO: create parameter objects
                    block_values_hex = list(map(lambda x: '0x{:08X}'.format(x), param_blob[param_index:param_index + block_size]))
                    block_values_hex = {i:'0x{:08X}'.format(param_value) for i,param_value in enumerate(param_blob[param_index:param_index+block_size])}
                    blob_dict[slot][device][block] = block_values_hex

            pos_blob += 1
    
        self._build_dict_for_converter_type(blob_dict)

    def _blob_dict_to_str(self, blob_version):
        meta_data, data = list(), list()

        if blob_version == 1.5:
            meta_data.append(1)
        
        else:
            meta_data.append(blob_version)

        next_free_index = 0
        for slot in range(bc.SCIVS_N_SLOTS):
            try:
                self._blob_dict[slot]

            except KeyError:
                meta_data += [next_free_index] * bc.SCIVS_N_DEVICES * bc.SCIVS_N_BLOCKS
                continue

            slot_devices = self._blob_dict[slot]
            for dev in bc.SCIVS_DEVICES:
                try:
                    device_dict = slot_devices[dev]

                except KeyError:
                    meta_data += [next_free_index] * bc.SCIVS_N_BLOCKS
                    continue

                for block in range(bc.SCIVS_N_BLOCKS):
                    try:
                       block_params = device_dict[block]
                       block_size = len(block_params)
                        
                    except KeyError:
                        meta_data.append(next_free_index)
                        continue
                
                    new_meta_data_word = (block_size << (16 - bc.versions_to_meta_data_info[blob_version]['block_size'])) | next_free_index
                    meta_data.append(new_meta_data_word)
                    next_free_index += len(block_params)
                    
                    data += list(int(param_value, base=16) for _, param_value in block_params.items())

        meta_data.append(bc.MAGIC_WORD)
        meta_data_32_bits = self._prepare_meta_data(meta_data)
        self._blob_dec_list = meta_data_32_bits + data

    def _prepare_meta_data(self, meta):
        processed_meta_data = list()

        for first, second in utils.grouper(meta, 2):
            processed_meta_data.append(first << 16 | second)

        return processed_meta_data

    def _build_dict_for_converter_type(self, blob_dict):
        if self._converter_type:
            for s in blob_dict:
                for d in blob_dict[s]:
                    for b in blob_dict[s][d]:
                        self._build_parameter_block(s, d, b, blob_dict[s][d][b])

        else:
            self._blob_dict = blob_dict

    def _build_parameter_block(self, slot, device, block, parameter_block_dict):
        parameter_block_dict = dict(map(Parameter.parameter_builder, parameter_block_dict.items(), self._converter_type))
    

class BlobException(Exception):
    pass    