from blob import Blob
from blob.tests import blob_static_data as blob_static

def test_blob_migration_is_ok_from_v1_5_to_v2():
    b = Blob.from_string(blob_static.BLOB_V1_5)
    assert b.migrate(to=2).as_dict() == b.as_dict()

# def test_blob_migration_is_ok_from_v2_to_v1():
#     b = Blob.from_string(BLOB_V2)
#     assert b.migrate(to=Blob.VERSION_1).as_string() == BLOB_V1

# def test_blob_migration_is_ok_from_v1_to_v1_5():
#     b = Blob.from_string(BLOB_V1)
#     assert b.migrate(to=Blob.VERSION_1_5).as_string() == BLOB_V1_5

# def test_blob_migration_is_ok_from_v1_5_to_v1():
#     b = Blob.from_string(BLOB_V1_5)
#     assert b.migrate(to=Blob.VERSION_1).as_string() == BLOB_V1

# def test_blob_migration_is_ok_from_v1_5_to_v2():
#     b = Blob.from_string(BLOB_V1_5)
#     assert b.migrate(to=Blob.VERSION_2).as_string() == BLOB_V2

# def test_blob_migration_is_ok_from_v2_to_v1_5():
#     b = Blob.from_string(BLOB_V2)
#     assert b.migrate(to=Blob.VERSION_1_5).as_string() == BLOB_V1_5
