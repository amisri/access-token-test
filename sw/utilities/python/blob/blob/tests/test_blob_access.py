from blob import Blob
from blob import constants as bc
from blob.tests import blob_static_data as bsd

import pytest

@pytest.mark.parametrize('blob_version, first_element_dec_list, first_element_hex_list',
[(bc.BLOB_VERSION_1, 65536, '0x00010000'),
(bc.BLOB_VERSION_2, 131072, '0x00020000')]
)
def test_blob_version_is_correctly_updated(blob_version, first_element_dec_list, first_element_hex_list):
    '''Tests that changes in the version member also alter the internal representation of the blob'''
    b = Blob()
    assert b.version == 0
    b.version = blob_version

    assert b.version == blob_version
    assert b[0] == first_element_dec_list
    assert '0x{:08X}'.format(b[0]) == first_element_hex_list

@pytest.mark.parametrize('input_blob, expected',
[(bsd.BLOB_V1_5, bsd.BLOB_V1_5_DICT.keys()),
(bsd.BLOB_V2, bsd.BLOB_V2_DICT.keys())
])
def test_blob_slots_are_accessible(input_blob, expected):
    '''Tests that slots are accessible and returned as a list of positive integers'''
    b = Blob.from_string(input_blob)
    assert b.slots == list(expected)

@pytest.mark.parametrize('input_blob, expected',
[
    (bsd.BLOB_V1_5_DICT, {slot:bsd.BLOB_V1_5_DICT[slot].keys() for slot in bsd.BLOB_V1_5_DICT.keys()}),
    (bsd.BLOB_V2_DICT, {slot:bsd.BLOB_V2_DICT[slot].keys() for slot in bsd.BLOB_V2_DICT.keys()}),
])
def test_devices_within_slot_are_accessible(input_blob, expected):
    '''Tests that devices within a slot are accessible and returned as a list of strings'''
    b = Blob.from_dict(input_blob)
    for s in input_blob.keys():
        assert b.devices(s) == list(expected[s])

@pytest.mark.parametrize('input_blob, expected',
[
    (bsd.BLOB_V1_5_DICT, {slot:{device:bsd.BLOB_V1_5_DICT[slot][device]} for slot in bsd.BLOB_V1_5_DICT for device in bsd.BLOB_V1_5_DICT[slot]}),
    (bsd.BLOB_V2_DICT, {slot:{device:bsd.BLOB_V2_DICT[slot][device]} for slot in bsd.BLOB_V2_DICT for device in bsd.BLOB_V2_DICT[slot]}),
])
def test_blocks_within_device_are_accessible(input_blob, expected):
    '''Tests that blocks within a device within a slot are accessible and returned as a list of positive integers'''
    b = Blob.from_dict(input_blob)
    for s in expected:
        for d in expected[s]:
            assert b.blocks(s, d) == list(expected[s][d])

@pytest.mark.parametrize('input_blob, expected',
[
    (bsd.BLOB_V1_5_DICT, {slot:{device:{block:bsd.BLOB_V1_5_DICT[slot][device][block]}} for slot in bsd.BLOB_V1_5_DICT for device in bsd.BLOB_V1_5_DICT[slot] for block in bsd.BLOB_V1_5_DICT[slot][device]}),
    (bsd.BLOB_V2_DICT, {slot:{device:{block:bsd.BLOB_V2_DICT[slot][device][block]}} for slot in bsd.BLOB_V2_DICT for device in bsd.BLOB_V2_DICT[slot] for block in bsd.BLOB_V2_DICT[slot][device]}),
])
def test_params_list_as_hex_and_dec_is_correct(input_blob, expected):
    '''Tests that parameters within blocks (within a device within a slot) are accessible and returned as a list of decimal and hexadecimal numbers'''
    b = Blob.from_dict(input_blob)
    for s in expected:
        for d in expected[s]:
            for block in expected[s][d]:
                hex_param_dict = expected[s][d][block]
                dec_dict = {param_idx:int(param_value, base=16) for param_idx, param_value in hex_param_dict.items()}
                assert b.params(s, d, block)          == hex_param_dict
                assert b.params(s, d, block, base=10) == dec_dict

# def test_integer_parameter_attributes_are_accessible(input_blob):
#     b = Blob.from_dict(input_blob)
#     param = b.param(s)
#     assert param.board == ''
#     assert param.device == ''
#     assert param.block == 9
#     assert param.name == 'param_name'
#     assert param.c_type == 'uint8_t'
#     assert param.type == 'int'
#     assert param.size_bits == 8
#     assert param.default_value == 0
#     assert param.for_humans() == 100

    