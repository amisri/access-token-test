"""
High-level tests for the  package.

"""
import pytest

from hypothesis import given, assume
from hypothesis.strategies import integers

from blob.tests import blob_static_data as blob_static

import blob
from blob import Blob, BlobException

def test_version():
    assert blob.__version__ is not None

def test_blob_can_be_built_from_empty_string():
    b = Blob.from_string('')
    assert b.as_string() == ''
    assert b.as_string(base=16) == ''
    assert b.as_dict() == dict()
    assert b.version == 0

def test_blob_can_be_build_from_empty_dict():
    b = Blob.from_dict(dict())
    assert b.as_string() == ''
    assert b.as_string(base=16) == ''
    assert b.as_dict() == dict()
    assert b.version == 0

def test_blob_can_be_build_from_empty_blob():
    b = Blob()
    assert b.as_string() == ''
    assert b.as_string(base=16) == ''
    assert b.as_dict() == dict()
    assert b.version == 0

@pytest.mark.parametrize('blob_input, expected', [
(blob_static.BLOB_V1_5, blob_static.BLOB_V1_5_HEX),
(blob_static.BLOB_V2, blob_static.BLOB_V2_HEX),
])
def test_blob_construction_from_dec_and_hex_strings(blob_input, expected):
    bdec = Blob.from_string(blob_input)
    assert bdec.as_string(base=16) == expected

def test_blob_construction_from_invalid_format_raises_exception():
    invalid_blob = ','.join(['1', '3', 'a', 'z'])
    with pytest.raises(BlobException):
        b = Blob(blob_str= invalid_blob)

@pytest.mark.parametrize('blob_input', [
    (blob_static.BLOB_V1_5),
    (blob_static.BLOB_V2)
])
def test_blob_can_be_build_with_valid_blob(blob_input):
    bdec = Blob.from_string(blob_input)

@pytest.mark.parametrize('blob_str, blob_dict, blob_version', [
    (blob_static.BLOB_V1_5, blob_static.BLOB_V1_5_DICT, 1.5),
    (blob_static.BLOB_V2, blob_static.BLOB_V2_DICT, 2),
])
def test_blob_string_and_dict_repr_are_the_same(blob_str, blob_dict, blob_version):
    b = Blob(blob_str=blob_str)
    assert b.as_dict() == blob_dict

    b = Blob(blob_dict=blob_dict, blob_version=blob_version)
    assert b.as_string() == blob_str

def test_blob_raises_exception_if_slots_out_of_limit():
    with pytest.raises(BlobException):
        b = Blob(blob_dict={32:{'MF':{0:[0]}}, 33:{'MF':{0:[0]}}})

def test_blob_raises_exception_if_devices_out_of_limits():
    with pytest.raises(BlobException):
        b = Blob(blob_dict={32:{'MF3':{0:[0]}}, 33:{'MF':{0:[0]}}})

def test_blob_raises_exception_if_blocks_out_of_limits():
    with pytest.raises(BlobException):
        b = Blob(blob_dict={32:{'MF':{31:[0]}}, 28:{'MF':{32:[0]}}})

def test_blob_raises_exception_if_param_number_out_of_limits():
    with pytest.raises(BlobException):
        b = Blob(blob_dict={20:{'MF':{31:[0]*31}}, 32:{'MF':{31:[0]*32}}})

@given(integers())
def test_blob_as_string_into_not_implemented_base_raises_exception(x):
    with pytest.raises(BlobException):
        assume(x != 10 and x != 16)
        b = Blob.from_string('')
        b.as_string(base=x)

@pytest.mark.parametrize('blob_input, converter_type', [
    (blob_static.BLOB_V2_DICT, blob_static.BLOB_V2_CONVERTER)
])
def test_blob_can_be_built_with_converter_type_input(blob_input, converter_type):
    b = Blob(blob_dict=blob_input, converter_type=converter_type)
        


