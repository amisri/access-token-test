"""
Documentation for the blob package

"""
from .blob import Blob, BlobException

__version__ = "0.0.1.dev0"
