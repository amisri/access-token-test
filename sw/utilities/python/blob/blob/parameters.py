class Parameter:
    def __init__(self, param_idx, param_value, converter_type):
        pass

    @classmethod
    def parameter_builder(cls, param_item, converter_type):
        param_idx, param_value = param_item
        return cls(param_idx, param_value, converter_type)
