# SCIVS stuff
SCIVS_N_SLOTS = 32
SCIVS_N_DEVICES = 2
SCIVS_N_BLOCKS = 7
SCIVS_MAX_BLOCK_SIZE = 31
SCIVS_DEVICES = ['MF', 'DEVICE_2']

# Blob stuff
BLOB_VERSION_1 = 1
BLOB_VERSION_2 = 2
BLOB_VERSION_3 = 3
BLOB_VERSION_4 = 4
BLOB_VERSIONS = [BLOB_VERSION_1, BLOB_VERSION_2, BLOB_VERSION_3, BLOB_VERSION_4]
MAGIC_WORD = 0xB10B

V1_PARAM_INDEX_SIZE_BITS = 8
V2_PARAM_INDEX_SIZE_BITS = 11
V4_PARAM_INDEX_SIZE_BITS = 0

V1_BLOCK_SIZE_BITS = 8
V2_BLOCK_SIZE_BITS = 5
V4_BLOCK_SIZE_BITS = 0

versions_to_meta_data_info = {BLOB_VERSION_1: {
                        'param_idx_size': V1_PARAM_INDEX_SIZE_BITS,
                        'block_size'    : V1_BLOCK_SIZE_BITS},
                        BLOB_VERSION_2: {
                        'param_idx_size': V2_PARAM_INDEX_SIZE_BITS,
                        'block_size'    : V2_BLOCK_SIZE_BITS},
                        BLOB_VERSION_4: {
                        'param_idx_size': V4_PARAM_INDEX_SIZE_BITS,
                        'block_size': V4_BLOCK_SIZE_BITS}}