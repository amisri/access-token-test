from blob import constants as bc
from blob import utils

class BlobMigrator:

    def _migrate_v1_v2(blob):
        meta, data = utils.get_meta_data_and_data_v1_and_v2(blob)

        new_meta_16_bit_words = list()
        new_meta_16_bit_words.append(bc.BLOB_VERSION_2)

        for m in meta[1:-1]:
            size, index = (m >> 8) & 0xFF, m & 0xFF
            new_meta_data_word = (size << bc.versions_to_meta_data_info[bc.BLOB_VERSION_2['param_idx_size']]) | index
            new_meta_16_bit_words.append(new_meta_data_word)

        new_meta_16_bit_words.append(meta[-1])

        new_meta_data_32_bit = list()
        for high, low in utils.grouper(new_meta_16_bit_words, 2):
            new_32_bit_word = (high << 16) | low
            new_meta_data_32_bit.append(new_32_bit_word)

        new_blob_list_int = new_meta_data_32_bit + data
        return ','.join(map(str, new_blob_list_int))

    def _migrate_v2_v4(blob):
        meta, data = utils.get_meta_data_and_data_v1_and_v2(blob)

        new_meta_words = list()

        slot_idx   = 0
        device_idx = 0
        block_idx  = 0
        block_bitmask = 0
        
        for m in meta[1:-1]:
            size, index = (m >> 8) & 0xFF, m & 0xFF

            if size > 0:
                block_bitmask = block_bitmask | 1 << block_idx

            block_idx = block_idx + 1

            if block_idx >= bc.SCIVS_N_BLOCKS:
                new_meta_words.append(block_bitmask)
                block_idx = 0
                block_bitmask = 0
                device_idx = device_idx + 1    
                if device_idx >= bc.SCIVS_N_DEVICES:
                    device_idx = 0
                    slot = slot + 1

        print(new_meta_words)
            







        # new_meta_data_32_bit = list()
        # for high, low in utils.grouper(new_meta_16_bit_words, 2):
        #     new_32_bit_word = (high << 16) | low
        #     new_meta_data_32_bit.append(new_32_bit_word)

        # new_blob_list_int = new_meta_data_32_bit + data
        # return ','.join(map(str, new_blob_list_int))

    def _migrate_v1_v3(blob):
        raise NotImplementedError

    def _migrate_v1_5_v2(blob):
        blob.version = bc.BLOB_VERSION_2
        return blob.as_string()

    def _migrate_v2_v3(blob):
        raise NotImplementedError

    versions_to_migrators = {bc.BLOB_VERSION_1: {bc.BLOB_VERSION_2: _migrate_v1_v2,
                                                 bc.BLOB_VERSION_3: _migrate_v1_v3},
                             bc.BLOB_VERSION_2: {bc.BLOB_VERSION_4: _migrate_v2_v4}}

    def _get_migrator(from_version, to_version):
        # Potential exception catched in Blob.migrate
        return BlobMigrator.versions_to_migrators[from_version][to_version]

    @staticmethod
    def migrate(blob, to):
        migrator =BlobMigrator._get_migrator(blob.version, to)
        return migrator(blob)

# EOF