from itertools import chain, zip_longest

from blob.constants import MAGIC_WORD

def grouper(iterable, group_by):
    args = [iter(iterable)] * group_by
    return zip_longest(*args)

def get_meta_data_and_data_v1_and_v2(blob):
    magic_word_idx = [i for i in range(len(blob)) if (blob[i] & 0x0000FFFF == MAGIC_WORD)][0]
    meta_data = blob.take(0, magic_word_idx + 1)
    data = blob.take(magic_word_idx + 1, len(blob))

    meta_data_16_bit = list(map(lambda x: ((x >> 16) & 0xFFFF, x & 0xFFFF), meta_data))
    meta_data_16_bit = list(chain.from_iterable(meta_data_16_bit))

    return meta_data_16_bit, data 