#!/usr/bin/env python3.6
"""
This tool allows to generate CCRT params and drive files based on a FGC's config properties and reference functions.
Jira Issue: https://issues.cern.ch/browse/EPCCCS-6346
Wiki: https://wikis.cern.ch/display/TEEPCCCS/FGC2CCRT
"""

import argparse
import csv
import os
import pyfgc
import pyfgc_name
import re
import sys

from collections import namedtuple, defaultdict
from operator import attrgetter
from pathlib import Path
from shutil import copyfile


__author__ = 'Kevin Timo Kessler'
__email__ = 'kevin.timo.kessler@cern.ch'
__version__ = '0.1.0'

PROGRAM_DIR = os.path.dirname(os.path.realpath(__file__))           # abs path to the parent dir of this file
PROJECT_ROOT = PROGRAM_DIR+"/../../../.."                           # repository root, relative to this file
MAX_SUB_DEVS = 5                                                    # maximum amount of possible sub devices (MPPM)

# output directory paths
OUT_DIR = PROJECT_ROOT + "/sw/lib/cclibs/ccrt/converters/{device}"  # parent output dir
SCRIPT_DIR = OUT_DIR + "/scripts"                                   # output script dir
TESTS_DIR = OUT_DIR + "/tests"                                      # output tests dir
REF_DIR = SCRIPT_DIR + "/ref"                                       # output ref funcs dir

# output file paths
CFG_PATH = SCRIPT_DIR + "/config.cct"                               # output config params file
ARM_PATH = REF_DIR + "/{sub_device_index}/arm.cct"                  # output func mapping file
USER_PATH = REF_DIR + "/{sub_device_index}/{user}.cct"              # output file per user / cycle
RUN_PATH = TESTS_DIR + "/{mode_pc_on}/{mode_pc_on}.cct"             # output run file
CLOCK_PATH = OUT_DIR + "/CLOCK"                                     # output clock file

# output formatting
LEN_FIRST_COL = 12                                                  # num of chars of 1st column in output files
LEN_SECOND_COL = 27                                                 # num of chars of 2nd column in output files

# Files that define the translation from FGC world into CCRT world
CONFIG_MAPPING_CSV = PROGRAM_DIR + "/fgc2ccrt_config.csv"           # mapping of FGC config props to ccrt params
REF_MAPPING_CSV = PROGRAM_DIR + "/fgc2ccrt_ref.csv"                 # mapping of FG_TYPE to relevant ref props

# Run file templates
RUN_TEMPLATE = PROGRAM_DIR + "/template_{mode_pc_on}.cct"             # template file for state specific cct run file generation

def convert_config(dev_name: str) -> None:
    """
    Reads CONFIG.SET from device with given dev_name, converts it into CCRT Params and writes them to config.cct
    :param dev_name: Name of the device to read CONFIG.SET from
    :return:
    """
    print(f'{dev_name}: Converting config properties to CCRT Parameters ...')

    # get necessary CONFIG.SET from device
    config = pyfgc.get(dev_name, "CONFIG.SET").value

    # parse CONFIG.SET to dict['fgc_property'] -> property_value
    config_data = dict()
    for prop_to_value in config.splitlines():
        prop, value = prop_to_value.strip().split(':')
        config_data[prop] = value

    # parse config csv to a list of namedtuple(col1, col2, ...)
    csv_data = list()
    with open(CONFIG_MAPPING_CSV, newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        CfgData = namedtuple('CfgData', next(reader))  # using the csv column headers as variable names
        for data in map(CfgData._make, reader):
            if not data.IS_COMMENT:
                csv_data.append(data)

    # sort the data based on ccrt_group and ccrt_parameter
    csv_data = sorted(csv_data, key=attrgetter('CCRT_GROUP', 'CCRT_PARAMETER'))

    # convert fgc_config_properties to ccrt_params, assign the property values read from fgc, and write to file
    abs_cfg_path = CFG_PATH.format(device=dev_name)
    os.makedirs(os.path.dirname(abs_cfg_path), exist_ok=True)
    with open(abs_cfg_path, 'w') as out:
        out.write(f'# Config parameters for {dev_name}\n')
        prev_group = ''
        for data in csv_data:

            # get value of the fgc_property mapped to the ccrt param
            fgc_value = config_data.get(data.FGC_PROPERTY, None)
            if not fgc_value:
                continue

            # append blank line between ccrt_groups
            if prev_group != data.CCRT_GROUP:
                out.write('\n')
                prev_group = data.CCRT_GROUP

            # write ccrt data mapped to fgc value to file
            out.write(data.CCRT_GROUP.ljust(LEN_FIRST_COL, ' ') + data.CCRT_PARAMETER.ljust(LEN_SECOND_COL, ' ')
                      + translate_property_value(fgc_value) + '\n')

        out.write('\n# EOF\n')


def create_func_files(dev_name: str, mode_pc_on: str, sub_device_idx: str = None) -> None:
    """
    Extracts reference functions from device with given dev_name and optional sub_device index.
    Creates CCRT specific output files based on the extracted reference functions, ref properties and cycle users.
    :param dev_name: Name of the device to extract reference functions from
    :param mode_pc_on: MODE.PC_ON value of the given device
    :param sub_device_idx: OPTIONAL index that references a sub device of the given device to extract information from
    (MPPM only).
    :return:
    """

    # if a sub_dev is provided, we need to modify the get requests by prepending 'x:' to address the sub_dev (MPPM)
    sub_device_prefix = ''
    if sub_device_idx is not None and sub_device_idx != '' and device_supports_multi_ppm(dev_name):
        sub_device_prefix = sub_device_idx + ':'
    else:
        sub_device_idx = '0'

    print(f'{dev_name}: Extracting reference functions (sub_idx: {sub_device_idx}) ...')

    # parse ref csv to a dict['fg_type'] -> list of namedtuple(col1, col2, ...)
    csv_data = defaultdict(list)
    with open(REF_MAPPING_CSV, newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        RefData = namedtuple('RefData', next(reader))  # using the csv column headers as variable names
        for data in map(RefData._make, reader):
            if not data.IS_COMMENT:
                fg_types = data.REF_FUNC_TYPES.strip().split('|')
                for fg_type in fg_types:
                    csv_data[fg_type].append(data._replace(REF_FUNC_TYPES=fg_type))

    # get necessary property values from device
    device = pyfgc_name.devices[dev_name]
    device_class = device['class_id']
    ref_data = defaultdict(list)
    device_host_name = pyfgc.get(dev_name, "DEVICE.HOSTNAME").value

    # mppm properties
    device_fg_type = pyfgc.get(dev_name, sub_device_prefix + "REF.FUNC.TYPE()").value.splitlines()

    # ref properties defined by ref csv
    for fg_type, data_list in csv_data.items():
        for data in data_list:
            # determine ppm_type column depending on FGC class
            ppm_type = data.FGC_63 if device_class == 63 \
                    else data.FGC_92 if device_class == 92 \
                    else data.FGC_94 if device_class == 94 \
                    else 'UNKNOWN'

            # validate property and ppm_type fields before getting value from fgc
            if not data.FGC_PROPERTY or not (ppm_type == 'PPM' or ppm_type == 'MPPM' or ppm_type == 'NON_PPM'):
                ref_data[fg_type].append(data._replace(FGC_VALUE=None, IS_PPM=False))
                continue

            # adjust property syntax / selectors depending on ppm type
            if ppm_type == 'PPM':
                data = data._replace(FGC_PROPERTY=data.FGC_PROPERTY + "()", IS_PPM=True)
            elif ppm_type == 'MPPM':
                data = data._replace(FGC_PROPERTY=sub_device_prefix + data.FGC_PROPERTY + "()", IS_PPM=True)
            else:
                data = data._replace(IS_PPM=False)

            # get and validate value for property, before appending to dict
            response = pyfgc.get(dev_name, data.FGC_PROPERTY)
            try:
                data = data._replace(FGC_VALUE=str(response.value).splitlines())
            except pyfgc.FgcResponseError:
                raise Exception(f"Invalid value for {data.FGC_PROPERTY}: {response.err_msg}")

            ref_data[fg_type].append(data)

    timing_users = pyfgc.get(device_host_name, "FGCD.TIMING.USERS").value.strip().split(',')

    # assert that values of ppm properties are of the same length
    users_len = len(timing_users)
    assert_msg = "The array length of FGCD.TIMING.USERS is greater than the array length of {property}"
    assert users_len <= len(device_fg_type), assert_msg.format(property="REF.FUNC.TYPE()")
    for fg_type, data_list in ref_data.items():
        for data in data_list:
            if data.IS_PPM:
                assert users_len <= len(data.FGC_VALUE), assert_msg.format(property=data.FGC_PROPERTY)

    # parse timing users and ref data to list[user_idx] -> namedtuple(user_name, fg_type, ref_props)
    UserData = namedtuple('UserData', 'user_idx, user_name, fg_type, ref_props')
    user_data = list()
    for timing_user in timing_users:
        user_name, user_idx, was_used = timing_user.split('|')
        user_idx = int(user_idx)
        fg_type = device_fg_type[user_idx].split(':')[1]

        assert fg_type in ref_data, f"'{user_name}' has unsupported REF.FUNC.TYPE: '{fg_type}'. See {REF_MAPPING_CSV}"

        ref_props = dict()
        for data in ref_data[fg_type]:
            if not data.FGC_VALUE or not data.CCRT_GROUP or not data.CCRT_PARAMETER:
                continue

            ccrt_key = data.CCRT_GROUP + " " + data.CCRT_PARAMETER
            value = data.FGC_VALUE if not data.IS_PPM else data.FGC_VALUE[user_idx].split(':')[1]
            ref_props[ccrt_key] = value

        user_data.append(UserData(user_idx, user_name, fg_type, ref_props))
    user_data = sorted(user_data, key=attrgetter('user_idx'))

    # create arm file for the converter / sub device
    # and create user func files for each user of the converter / sub device and reference them in arm file
    abs_arm_path = ARM_PATH.format(device=dev_name, sub_device_index=sub_device_idx)
    os.makedirs(os.path.dirname(abs_arm_path), exist_ok=True)
    with open(abs_arm_path, 'w') as arm_out:
        arm_out.write(f'# Read in functions for {dev_name} {sub_device_prefix}\n\n')
        for data in user_data:
            abs_user_path = USER_PATH.format(device=dev_name, sub_device_index=sub_device_idx, user=data.user_name)
            rel_user_path = abs_user_path[len(OUT_DIR.format(device=dev_name)) + 1:]
            arm_out.write('READ(' + f'{str(data.user_idx)})'.ljust(4, ' ') + f'{rel_user_path}\n')

            # create user file
            with open(abs_user_path, 'w') as user_out:
                user_out.write(f'# CCRT function for {dev_name} {sub_device_prefix} {data.user_name}\n\n')
                user_out.write(f'REF FG_TYPE {data.fg_type}\n')
                for ccrt_key, value in data.ref_props.items():
                    user_out.write(f'{ccrt_key} {value}\n\n')
                user_out.write('# EOF\n')

        arm_out.write('\n# Arm the functions with REF.FUNC.TYPE != NONE\n\n')
        users_to_arm = list(filter(lambda x: x.fg_type != 'NONE', user_data))
        arm_out.write('ARM'.ljust(LEN_FIRST_COL, ' ') + ' '.join(str(x.user_idx) for x in users_to_arm) + '\n\n')
        arm_out.write('MODE'.ljust(LEN_FIRST_COL, ' ') + 'REF'.ljust(LEN_SECOND_COL, ' ') + ' ' + mode_pc_on + '\n\n')
        arm_out.write('# EOF\n')


def create_run_file(dev_name: str, mode_pc_on: str) -> None:
    """
    Creates a CCRT specific run file for the device with the given dev_name
    :param dev_name: Name of the device to create the run file for
    :param mode_pc_on: MODE.PC_ON value of the given device
    :return:
    """

    mode_pc_on = mode_pc_on.lower()

    # we create the run file based on predefined state specific templates, so we have to make sure the template exists
    abs_template_path = Path(RUN_TEMPLATE.format(device=dev_name, mode_pc_on=mode_pc_on))
    if not abs_template_path.is_file():
        print(f'{dev_name}: Skipping CCRT run file generation \'{mode_pc_on}.cct\'. '
              f'No template file found at \'{abs_template_path}\'')
        return

    # prepare output path
    abs_run_path = RUN_PATH.format(device=dev_name, mode_pc_on=mode_pc_on)
    os.makedirs(os.path.dirname(abs_run_path), exist_ok=True)

    # generate copy of template
    print(f'{dev_name}: Creating CCRT run file \'{mode_pc_on}.cct\')...')
    copyfile(abs_template_path, abs_run_path)


def create_clock_file(dev_name: str) -> None:
    """
    Creates a CCRT specific CLOCK file for the device with the given dev_name
    :param dev_name: Name of the device to create the CLOCK file for
    :return:
    """
    print(f'{dev_name}: Creating CCRT CLOCK file ...')

    iter_period = get_iter_period(dev_name)
    iter_period_us = round(iter_period * 1e6)
    event_log_iter = round(5000 / iter_period_us)

    abs_clock_path = CLOCK_PATH.format(device=dev_name)
    os.makedirs(os.path.dirname(abs_clock_path), exist_ok=True)
    with open(abs_clock_path, 'w') as out:
        out.write('ITER_PERIOD_US ' + str(iter_period_us) + '\n')
        out.write('EVENT_LOG_PERIOD_ITERS ' + str(event_log_iter) + '\n')
        out.write('\n# EOF\n')


def translate_property_value(property_value: str) -> str:
    """
    Some property values need to be translated from FGC to CCRT.
    E.g. CCRT rather uses the non cryptic term CURRENT instead of I
    :param property_value: the fgc property value to be translated
    :return: the translated property value for CCRT
    """
    if property_value == 'I':
        return "CURRENT"
    if property_value == 'V':
        return "VOLTAGE"
    if property_value == 'B':
        return "FIELD"
    return property_value


def get_mode_pc_on(dev_name: str) -> str:
    """
    Returns the value of MODE.PC_ON property for the device with the given dev_name.
    Returns always 'CYCLING' for class 94 devices.
    Returns 'UNKNOWN' for classes that do not support the property (except 94).
    :param dev_name: Name of the device that shall be validated
    :return: The value of MODE.PC_ON property for the device with the given dev_name.
    """
    device = pyfgc_name.devices[dev_name]
    class_id = device['class_id']

    # assume class 94 devices are always in CYCLING as they do not know about this property
    if class_id == 94:
        return 'CYCLING'

    # otherwise get state pc_on from device
    response = pyfgc.get(dev_name, "MODE.PC_ON")
    try:
        return response.value
    # Return 'UNKNOWN' if the property is not known to the device
    except pyfgc.FgcResponseError:
        return 'UNKNOWN'


def get_iter_period(dev_name: str) -> float:
    """
    Determines and returns a device's ITER_PERIOD
    :param dev_name: Name of the device for which the iter period should be determined
    :return: the device's ITER_PERIOD
    """
    device = pyfgc_name.devices[dev_name]
    device_class = str(device['class_id'])

    lib2_classes = re.compile("^(63|92|94)$")
    fgc3_classes = re.compile("^(61|62|63)$")
    fgc2_mugef_classes = re.compile("^(51|53|94)$")
    fgclite_classes = re.compile("^(92)$")

    if lib2_classes.match(device_class):
        response = pyfgc.get(dev_name, "FGC.ITER_PERIOD")
        try:
            iter_period = response.value
            return float(iter_period)

        except pyfgc.FgcResponseError:
            if fgc3_classes.match(device_class):
                return 1 / 10000

            if fgc2_mugef_classes.match(device_class):
                return 1 / 1000

            if fgclite_classes.match(device_class):
                return 1 / 100


def device_supports_multi_ppm(dev_name: str) -> bool:
    """
    Checks whether the given device's class supports multi ppm (mppm)
    :param dev_name: Name of the device that shall be validated
    :return: True if the class supports mppm, False otherwise
    """
    device = pyfgc_name.devices.get(dev_name, None)
    mppm_classes = re.compile("^(63|64)$")
    device_class = str(device['class_id'])
    return mppm_classes.match(device_class)


def validate_device(dev_name: str) -> None:
    """
    Validates the device name input. Checks whether the device can be found in the name file and that it's class is
    supported by CCRT / cclibs v2
    :param dev_name: Name of the device that shall be validated
    :return:
    """
    device = pyfgc_name.devices.get(dev_name, None)
    if not device:
        sys.tracebacklimit = None
        raise Exception(f'Device not found in name file: \'{dev_name}\'.')

    lib2_classes = re.compile("^(63|64|92|94)$")
    device_class = str(device['class_id'])
    if not lib2_classes.match(device_class):
        sys.tracebacklimit = None
        raise Exception(f'Device class not supported: \'{device_class}\'.')


def validate_sub_idx(sub_device_idx) -> None:
    """
    Validates the sub_device_idx input. We allow None, '', 'x' and x (where x must be between 0 and MAX_SUB_DEVS)
    #TODO: how to reference variable in docstring?
    :param sub_device_idx: index of the sub device to address in a MPPM context
    :return:
    """
    if not sub_device_idx or sub_device_idx == '':
        return

    if isinstance(sub_device_idx, str) and sub_device_idx.isdigit() and MAX_SUB_DEVS > int(sub_device_idx) > -1:
        return

    if isinstance(sub_device_idx, int) and MAX_SUB_DEVS > sub_device_idx > -1:
        return

    sys.tracebacklimit = None
    raise Exception(f'Invalid sub device index: \'{str(sub_device_idx)}\'. ' +
                    f'Must either be empty or whole number between 0 and {str(MAX_SUB_DEVS - 1)}')


def main() -> None:
    # parse command line arguments
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-d', '--device', type=str, required=True, help='name of device to generate CCRT params for')
    argparser.add_argument('-s', '--sub_idx', type=str, help='sub device index (optional for multi-PPM devices)')
    args = argparser.parse_args()

    # parse name file and validate input
    pyfgc_name.read_name_file()
    validate_device(args.device)
    validate_sub_idx(args.sub_idx)

    # perform conversion of FGC config properties to CCRT params
    convert_config(args.device)
    create_clock_file(args.device)

    # create run file depending on the devices MODE.PC_ON
    mode_pc_on = get_mode_pc_on(args.device)
    create_run_file(args.device, mode_pc_on)

    # in case of CYCLING, we want to create cycling.cct AND idle.cct
    if mode_pc_on == 'CYCLING':
        create_run_file(args.device, 'IDLE')

    # Reference functions should only be extracted if MODE.PC_ON is CYCLING
    if mode_pc_on != 'CYCLING':
        print(f'{args.device}: Skipping extraction of reference functions. MODE.PC_ON is \'{mode_pc_on}\'')
        print(f'{args.device}: Done')
        exit(0)

    # Extract reference functions and generate output files
    create_func_files(args.device, mode_pc_on, args.sub_idx)
    print(f'{args.device}: Done')


if __name__ == '__main__':
    main()
