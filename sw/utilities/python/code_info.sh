#!/bin/bash
# 
# Wrapper for using code_info

source ~pclhc/bin/python/ccs_venv/bin/activate
ccs.deployment.code_info $@
deactivate

# EOF

