#!/bin/bash
#
# Name:     code_inventory.sh
# Purpose:  Display details of all deployed code files
# Author:   Stephen Page

code_dir_filter=$1 # Code directory names will be filtered with this string

cd /user/pclhc/etc/fgcd
for code_type_dir in code_groups
do
    find $code_type_dir -mindepth 1 -type d -name "*$code_dir_filter*" | sort | while read dir;
    do
        echo $dir

        # Store IFS and set it to newline

        ifs_orig=$IFS
        IFS=$'\n'

        # Get the code information for the directory

        cd $dir || continue
	code_info=($(find -maxdepth 1 -type f -exec basename {} \; | sort | xargs -r /user/pclhc/bin/python/code_info.sh))
        cd - >/dev/null

        # Print the code information and the count

        if [ -n "${code_info[*]}" ]; then
            echo "${code_info[*]}"
        fi
        echo ${#code_info[*]} codes
        echo

        # Restore IFS

        IFS=$ifs_orig
    done
done

# EOF
