# Support script

To run the utility, run `bash run.sh`.

You need to be in the TN (e.g. cs-ccr-dev*).

```bash
Choose an action:
  u: (u)pdate/rename/create/delete devices
  c:  |- (c)heck namefile
  r:  |- (r)eset abpo1
  t:  |- commi(t) changes to name and sub_devices files
  e:  |- r(e)name in ccde
  b:  |- (b)uild new access map
  l:  |- (l)oad new access map
  a:  |- ren(a)me devices in ccs database
  m:  |- u(p)date device (m)etadata in ccde
  i: l(i)st devices information
  o: l(o)ad class91 devices properties from NFS and export to CSV
```

It is recommended to use the template files in `templates` like this:
- Open the template corresponding to what you want to do (renaming, device update, addition, removal)
- Set the values in the template as described
- Run the script with `bash run.sh`, select the first option (`u`)
- Copy the value cells (start from the first row, second line, select until the last device line and the last header column)
- Copy it in the terminal

Templates files:
- `default-advanced.csv`: Allow to make any modification to any device (renaming, update, addition, removal). Details are in the header
- `device-removal.csv`: Remove devices
- `device-renaming.csv`: Rename devices and eventually set a new alias. PPM aliases are not fully supported
- `device-update.csv`: Change devices information (move to a new gateway, rename, set new alias, change dongle or mask)
- `device-addition.csv`: Add new devices, specifying their gateway, dongle, class and mask
