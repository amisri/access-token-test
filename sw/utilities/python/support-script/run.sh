#!/bin/bash

# Create and init venv if it does not exist
[ ! -d "venv" ] && source /acc/local/share/python/acc-py/pro/setup.sh && python -m venv venv && source venv/bin/activate && pip install -r requirements.txt
source venv/bin/activate

# Run api
python -m src.main;
