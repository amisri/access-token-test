import typing
from .helpers import remote_ssh

USER = 'pclhc'
HOST = 'cs-ccr-dev3.cern.ch'


def critical_settings_find_accelerator(gateway: str) -> typing.Optional[str]:
    accelerator = remote_ssh(USER, HOST, 'ls -d /acc/src/dsc/*/%s | cut -d/ -f5' % gateway.lower())
    return accelerator.strip() if accelerator else None


def critical_settings_exists(gateway: str) -> bool:
    accelerator = critical_settings_find_accelerator(gateway)
    if not accelerator:
        return False
    filepath = '/acc/dsc/%s/%s/data/FGC_%sAccessConfiguration.xml' % (accelerator, gateway.lower(), gateway.upper())
    content = remote_ssh(USER, HOST, 'cat %s' % filepath)
    return not not content
