import getpass
import json
import time
import re
import urllib3
import cx_Oracle

import pyfgc_rbac
import requests
from .namefile import *
from .prompt import *
from .gateways import *
from .ccde import *
from .class91_property_reader import *
from .critical_settings import *
from .device_config_backups import *
from .nonvolatile import *
from .sql import *
from .sso import get_rbac_token


# Disable ssl warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



def prompt_sql_password():
    print("Enter CCS production database password")
    sql_password = getpass.getpass()
    if not sql_production_verify_password(sql_password):
        print('Unvalid user/pasword for CCS production database')
        return prompt_sql_password()
    return sql_password


def prompt_list(entities):
    print('Enter the list%s, separated by commas' % (' of ' + entities if entities else ''))
    entries = [name.strip().upper() for name in input('>>> ').split(',')]
    print('\nSelected entries:')
    for entry in entries:
        print('  - %s' % entry)
    if not confirm('\nAre you satisfied with this selection?'):
        return prompt_list(entities)
    return entries


def prompt_device_updates():

    # Read namefile
    namefile = git_file_parse(namefile_read_current())
    device_exists = lambda device: len([row for row in namefile if row[3] == device.upper()]) > 0
    get_device_gw = lambda device: [row for row in namefile if row[3] == device.upper()][0][0] if device_exists(device) else None

    # This action dict keeps track of all actions that need to be performed
    # Keys are upper case device name
    # Values are dictionary containing information about the action to peform for this device
    actions = {}

    print('Please enter the list of devices to update.')
    print('Enter one device per line, using this format:')
    print('$current_epc_name $new_epc_name $new_epc_alias $new_gateway $new_dongle')
    print('    $current_epc_name : Current EPC device name / CCDB alias (if adding device, let empty)')
    print('    $new_epc_name     : New EPC device name / CCDB alias (if deleting device, let empty)')
    print('    $new_epc_alias    : If FGC: New EPC alias / CCDB device name (let empty to not change it)')
    print('                        If gateway: New domain and path e.g. "PSB:Development/FGC3"')
    print('    $new_gateway  : New gateway name (let empty to not change it)')
    print('    $new_dongle   : New dongle id (let empty to not change it)')
    print('    $new_class    : New class id (let empty to not change it)')
    print('    $new_mask     : New mask (let empty to not change it)')
    print('    > accepted delimiters are spaces, tabs, commas and semicolons')
    print('    > it is recommend to copy/paste from a csv file to avoid errors')
    for line in read_multilines(">>> ").split("\n"):
        line = re.split(' |\t|,|;', line)
        # Extract values
        current_epc_name = line[0].upper() if len(line) > 0 else ""
        new_epc_name = line[1].upper() if len(line) > 1 else ""
        new_epc_alias = line[2] if len(line) > 2 else ""
        new_gateway = line[3].lower() if len(line) > 3 else ""
        new_dongle = line[4] if len(line) > 4 else ""
        new_class = line[5] if len(line) > 5 else ""
        new_mask = line[6] if len(line) > 6 else ""
        # User needs to specify either current or new device name
        if current_epc_name == '' and new_epc_name == '':
            print('  /!\ Please specify either the current or new device name')
            return prompt_device_updates()
        # Find action to perform
        if current_epc_name == '':
            # Add a new device or gateway
            if new_epc_name in actions:
                print('  /!\ Device specified twice in the data: %s' % new_epc_name)
                return prompt_device_updates()
            if not new_gateway or not new_dongle or not new_class or not new_mask:
                print('  /!\ Please specify all fields to add device %s' % new_epc_name)
                return prompt_device_updates()
            if device_exists(new_epc_name):
                print('  /!\ Warning: Device %s already exists' % new_epc_name)
            actions[new_epc_name] = {
                'action': 'create',
                'new_epc_name': new_epc_name,
                'new_epc_alias': new_epc_alias,
                'new_gateway': new_gateway,
                'new_dongle': new_dongle,
                'new_class': new_class,
                'new_mask': new_mask,
            }
        elif new_epc_name == 'KEEPFILE':
            # Delete a device or gateway
            if current_epc_name in actions:
                print('  /!\ Device specified twice in the data: %s' % current_epc_name)
                return prompt_device_updates()
            if not device_exists(current_epc_name):
                print('  /!\ Warning: Device %s does not exist' % current_epc_name)
            device_gw = get_device_gw(current_epc_name)
            if device_gw.lower() == current_epc_name.lower():
                print('  /!\ ERROR: Cannot temporarily remove gateway %s.' % current_epc_name)
                return prompt_device_updates()
            actions[current_epc_name] = {
                'action': 'temporary_delete',
                'current_gateway': device_gw,
                'current_epc_name': current_epc_name,
            }
        elif new_epc_name == '':
            # Delete a device or gateway
            if current_epc_name in actions:
                print('  /!\ Device specified twice in the data: %s' % current_epc_name)
                return prompt_device_updates()
            if not device_exists(current_epc_name):
                print('  /!\ Warning: Device %s does not exist' % current_epc_name)
            device_gw = get_device_gw(current_epc_name)
            if device_gw.lower() == current_epc_name.lower():
                print('  /!\ Warning: Removing gateway %s. Is this what you want to do?' % current_epc_name)
            actions[current_epc_name] = {
                'action': 'delete',
                'current_gateway': device_gw,
                'current_epc_name': current_epc_name,
            }
        else:
            # Modify an existing device
            if current_epc_name in actions:
                print('  /!\ Device specified twice in the data: %s' % current_epc_name)
                return prompt_device_updates()
            if new_epc_name.lower() == new_gateway.lower():
                print('  /!\ Updating gateways is not supported')
                return prompt_device_updates()
            if not device_exists(current_epc_name):
                print('  /!\ Warning: Device %s does not exist' % current_epc_name)
            if current_epc_name != new_epc_name and device_exists(new_epc_name):
                print('  /!\ Warning: Device %s already exists' % new_epc_name)
            actions[current_epc_name] = {
                'action': 'update',
                'current_gateway': get_device_gw(current_epc_name) or get_device_gw(new_epc_name),
                'current_epc_name': current_epc_name,
                'new_epc_name': new_epc_name,
                'new_epc_alias': new_epc_alias,
                'new_gateway': new_gateway,
                'new_dongle': new_dongle,
                'new_class': new_class,
                'new_mask': new_mask,
            }

    # Print parsed actions to user for validation
    print('Actions to perform:')
    for device, data in actions.items():
        action = data['action']
        info = ''
        if action == 'create':
            info = '(gw: %s, dongle: %s, class: %s, mask: %s' % (data['new_gateway'], data['new_dongle'], data['new_class'], data['new_mask'])
            if new_epc_alias:
                info += ', epc alias (ccdb name): %s' % data['new_epc_alias']
            info += ')'
        elif action == 'delete':
            info = ''
        elif action == 'temporary_delete':
            info = ''
        elif action == 'update':
            info = '(epc name: %s' % data['new_epc_name'] if data['current_epc_name'] != data['new_epc_name'] else '(epc name: unchanged'
            if data['new_epc_alias']:
                info += ', epc alias (ccdb name): %s' % data['new_epc_alias']
            if data['new_gateway']:
                info += ', gw: %s' % data['new_gateway']
            if data['new_dongle']:
                info += ', dongle: %s' % data['new_dongle']
            if data['new_class']:
                info += ', class: %s' % data['new_class']
            if data['new_mask']:
                info += ', mask: %s' % data['new_mask']
            info += ')'
        else:
            print('  /!\ Unsupported action %s (%s)' % (action, data))
            return prompt_device_updates()

        print('%s: %s: %s' % (device.ljust(35), action.ljust(8), info))

    print('')
    if not confirm('Please confirm that this correspond to what you want to do'):
        return prompt_device_updates()

    return actions.values()


def prompt_device_list():

    # Read namefile
    namefile = git_file_parse(namefile_read_current())
    get_device_matches = lambda device: [row for row in namefile if device.upper() in row[3].upper()]
    #get_device_matches = lambda device: [row for row in namefile if device.upper() in row[3].upper()]

    # List of devices and/or aliases
    devices = []

    print('Please enter the list of devices.')
    print('Enter one device per line.')
    for line in read_multilines(">>> ").split("\n"):
        line = re.split(' |\t|,|;', line)
        name = line[0].upper() if len(line) > 0 else ""
        if not name:
            continue
        matches = get_device_matches(name)
        # If no match, print a warning
        if len(matches) == 0:
            print('Device %s not found in the namefile' % name)
        # If only one match, we use the match
        if len(matches) == 1 and name != matches[0][3]:
            print('Autocompleted %s name to %s' % (name, matches[0][3]))
            name = matches[0][3]
        if len(matches) > 1:
            print('More than one match for %s. Keeping the original name.' % name)
        devices.append(name)

    # Print parsed actions to user for validation
    print('\nSelected devices:')
    for device in devices:
        print(device)

    print('')
    if not confirm('Please confirm that this correspond to what you want to do'):
        return prompt_device_list()

    return devices


def api_update_devices():

    # Check that git is clean
    ensure_name_file_repo_up_to_date()

    # Prompt actions to perform
    updates = prompt_device_updates()

    # Sequentially run updates
    files, affected_gateways = git_apply_updates(updates)

    # Find gateways with critical settings
    critical_settings_gateways = [gw for gw in affected_gateways if critical_settings_exists(gw)]
    if len(critical_settings_gateways) > 0:
        print('\n')
        print('===============================================')
        print(' /!\ Some gateways have critical settings /!\\')
        print('===============================================')
        print('\n')
        print('Some gateways have critical settings:')
        for gateway in critical_settings_gateways:
            print('  - %s' % gateway)
        print('')
        print('These critical settings will NOT be updated by this script.')
        print('You can still use this script to update the devices, but it is crucial that the critical settings are updated by hand afterward')
        print('Check with an expert if you are unsure what to do')
        print('')
        if not confirm('Continue?'):
            return


    # Print affected gateways
    print('\nAffected gateways')
    for gateway in affected_gateways:
        print('  - %s' % gateway)

    # Print all modified files diffs
    print('\nPre-computed diff:')
    for filename, data in files.items():
        print('[%s]' % filename)
        print(diff_contents(data['current'], git_file_parsed_to_text(filename, data['new'])))
        print('\n')

    print('Please verify that the pre-computed diff above correspond to what you want.')
    if not confirm('Do you want to continue? If you respond yes, these changes will be deployed to production.'):
        print('Production has not been changed. Exiting.')
        return

    print('\nUploading files in production')
    # Get a commit message
    commit_message = None
    while not commit_message:
        new_commit_message = input('Enter your commit message:' + "\n>>> ")
        if len(new_commit_message) == 0:
            continue
        print('Your commit message: %s' % new_commit_message)
        if not confirm('Are you satisfied with this commit message?'):
            continue
        commit_message = new_commit_message
    # Deploy files
    git_deploy_files(files, commit_message)

    print('\nFiles have been deployed to production.')
    #print('Waiting for remote to be updated.. (usually takes a few minutes)')
    #while True:
    #    # TODO: Check if remote is updated
    #    time.sleep(5)
    while not confirm('Go there: https://gitlab.cern.ch/ccs/fgc_prod/-/pipelines\nHas the deploy pipeline finished yet?'):
        pass

    #print('Change was propagated to production.')
    print('OK.')

    # Reset status server
    if confirm("Do you want to reset the status server?"):
        api_reset_abpo1()

    # Extract updates that are renamings (exluding alias changes)
    renamings = [update for update in updates if update['action'] == 'update' and update['current_epc_name'] != update['new_epc_name']]
    # Extract updates that are device creations
    additions = [update for update in updates if update['action'] == 'create']
    # Extract updates that are device deletions
    deletions = [update for update in updates if update['action'] == 'delete']

    # If there are some renamings
    if len(renamings) > 0:

        if confirm("(For renamings) Rename devices in CCDB?\n\t- Mandatory to run if you work on device renaming (needed before running update_properties.pl).\n\t- Note: This will update the CCDB alias field (equivalent to EPC device name).", True):
            api_rename_in_ccde(renamings)

        if confirm("(For renamings) Do you want to rename some devices in FGC database?\n\t- Run this if you work on device renaming. - If there are spare combinations with the renamed devices as operational device in the spare manager, the spare combinations will be renamed accordingly. You can verify this via https://cern.ch/fgc-spare-manager", True):
            api_rename_in_ccs_db(renamings)

    # Synchronize CCDE with the namefile
    if confirm("Do you want to synchronize CCDB with the namefile (i.e. run update_devices.pl)? \n\t- Run this if you added, renamed or deleted devices.\n\t- Run this if EPC aliases were added/changed (i.e. sub devices were defined in the sub device files).\n\t- Run this if you updated other fields of the name file such as fieldbus addresses (dongles), classes, etc. \n\t- IMPORTANT: In case of renamings: Requires that devices have been renamed in CCDB (in a previous step) - in fact CCDB aliases have been updated by renamed EPC device names.", True):
        api_run_update_devices()

    # If creating devices
    if len(additions) > 0:

        # If new device classes
        if confirm('(If adding a first device for a new class) Initialize RBAC properties and property groups?', False):
            api_create_rbac_properties(updates)

        # Attach devices to correct rbac groups in CCDE
        if confirm('(For new devices) Update RBAC device groups in CCDE?', True):
            ccde_update_rbac_groups(updates)

        # Instantiate properties in the property manager
        if confirm('(If new devices of class 5x, 6x or 92) Import properties to the property manager?', True):
            api_import_properties_in_prop_mngr(updates)

    # Build and load access map to gateways
    if confirm("Build and load new access maps to gateways?", True):
        if confirm("Will be building and loading access map on gateways: %s\nAre you ok with this list?" % affected_gateways, True):
            gateways = affected_gateways
        else:
            gateways = prompt_list('gateways')
        api_build_access_map(gateways)
        api_load_new_access_maps(gateways)

    # Resets gateways
    if len(renamings) > 0 or len(deletions) > 0:
        if confirm("Gateway's subscriptions will not be updated unless the gateway is restarted which can shortly impact operation. Do you want to reset affected gateways?"):
            if confirm("Will be reseting these gateways: %s\nAre you ok with this list?" % affected_gateways, True):
                gateways = affected_gateways
            else:
                gateways = prompt_list('gateways')
            api_reset_gateways(gateways)

    # If there are some renamings: Update non-volatile storage and device config files
    if len(renamings) > 0 or len(deletions) > 0:
        if confirm("Update device configuration backup files?", True):
            api_move_device_config_backups(updates)

        if confirm("Update non-volatile storage?", True):
            api_move_nonvolatile_storage(updates)

    # Finalize updates
    if len(additions) > 0:
        print("""
    Unfortunately, these last steps are not yet automated for device additions. You need to do this:
        1. Set the properties State, Accelerator, Accelerator Zone, and PPM using the Controls Configuration Device Editor.
            In case of SPARE devices, set state to "development".
            Exception: If the SPARE has a CCDB device name that is different from its CCDB alias and it is going to be used in a working set, set it's state to "operational".
        2. Unless otherwise stated, make sure the device responsible is set to the corresponding FEC's "Operational Support" value using the Controls Configuration Device Editor. You can find the FEC and it's "Operational Support" value via the Controls Configuration Hardware Editor.
        3. (only operational devices) Contact OP experts (be-co-oplink*) and notify them about added devices:
            Determine which OP experts (e-group) to notify, depending on the accelerator / zone of the devices. You can search for the different be-co-oplink* e-groups here.
            You will need to send them a list with all devices that have been added so that they can integrate them to their applications (InCA/LSA, Knobs & WorkingSets, etc.).
            Once you get the confirmation this has been done, get from the operators / operation responsable of the accelerator/experiment the parameters (properties) they need to see in their knobs. Add these parameters to the operational devices.
        4. (only for the LHC) Copy CircuitDetails.pm to pclhc and check that the new devices appear in the Powering Status Page:
            cd projects/fgc/sw
            make parse
            scp sw/clients/perl/FGC/CircuitDetails.pm pclhc@cs-ccr-dev3:~/lib/perl/FGC
        5. Tell the device responsible if some variable has to be tracked on NXCALS, they can do so themselves in CCDE.
        6. (FGC3 only) Ask the device responsible if the device has to be added to OASIS. If this is the case, then they should fill in a request following a send the list of devices to oasis-support@cern.ch in order to create the corresponding virtual devices (e.g. <alias name>-DS). The request must follow the template provided in the 'Data Source Signals Request' tab of the OASIS signal request template spreadsheet. The following columns are needed:
            FGC data source device
            FGC FEC name
            Proposed OASIS signal name, following the pattern XXXXX-DS (see other %-DS FGC devices in CCDB to get an idea)
            Trigger signal name, following the pattern XXXX-TS (again, see other %-TS trigger devices in CCDB to get an idea)
            Tigger FEC name (normally the same FEC as source FGC device, unless it is triggered from a custom timing source)
        7. Done. You can now grab some coffee!
        """)
    else:
        print("\n" + 'Done. You can now grab some coffee!')


def api_create_rbac_properties(updates=None):
    updates = updates or prompt_device_updates()
    print("""
    Because you are creating devices for new classes, create the RBAC properties and property groups
    Execute this in a new shell (execute script as user@dbabco):
        ssh cs-ccr-teepc2
        cd projects/fgc/sw/utilities/perl/databases/controls
        ./update_properties.pl 61 [--commit]
        ./update_property_groups.pl 63 [--commit]
    """)
    if not confirm('Have you executed these commands?'):
        api_create_rbac_properties(updates)


def api_import_properties_in_prop_mngr(updates=None):
    updates = updates or prompt_device_updates()
    print("""
    - (only if new devices of classes 5x, 6x, and 92): Import the new systems to pocontrols_mod@dbabco
        1. Go to https://accwww.cern.ch/fgc-property-manager/scripts/device
        2. Under the heading "Import devices from name file", select the appropriate class and press "Import devices"

    - (only if new system types was not defined before): Import the new system type properties to pocontrols_mod@dbabco
        1. Go to https://accwww.cern.ch/fgc-property-manager/scripts/property
        2. Click the "Instantiate Type Properties" script button
        3. If this is the first device of a given type, make sure the associated type properties have been defined in the database:
            "Setting a Config Property Directly to the DB#SettingaTypeProperty."
            https://wikis.cern.ch/display/TEEPCCCS/Setting+a+Config+Property+Directly+to+the+DB#SettingaConfigPropertyDirectlytotheDB-SettingaTypeProperty

    - (only if adding/renaming/removing devices of classes 5x, 6x, and 92): Instantiate the new system properties in the FGC DB
        This step is automated and runs once per night via a scheduled gitlab-ci job.
        If needed urgently, you can trigger it manually,
            but be aware that for classes with many devices and properties this can take a long time (e.g. up to 30min for FGC_63).
        To trigger it manually:
            1. Go to https://accwww.cern.ch/fgc-property-manager/scripts/property
            2. Click the "Instantiate System Properties" with the class(es) needing to be updated
    """)
    if not confirm('Have you followed these 3 steps?'):
        api_import_properties_in_prop_mngr(updates)


def api_rename_in_ccde(updates=None):
    updates = updates or prompt_device_updates()
    # Filter the device updates to only keep the renamings
    renamings = [update for update in updates if update['action'] == 'update' and update['current_epc_name'] != update['new_epc_name']]
    warnings = []
    # Build csv compatible with CCDE bulk rename feature. Format and naming defined by CCDE
    # name: current ccdb devicename | newName: new ccdb devicename | alias: epc device name
    csv_entries = ['name,newName,alias,description']
    for renaming in renamings:
        warning = None
        if ccde_get_device(renaming["new_epc_name"]):
            warnings.append('Unable to rename %s to %s in CCDB. Device with CCDB alias %s already exists in CCDB. Skipping this device.' % (renaming["current_epc_name"], renaming["new_epc_name"], renaming["new_epc_name"]))
            continue
        ccde_device = ccde_get_device(renaming["current_epc_name"])
        if not ccde_get_device(renaming["current_epc_name"]):
            warnings.append('Unable to rename %s to %s in CCDB. Device with CCDB alias %s not found in CCDB. Skipping this device.' % (renaming["current_epc_name"], renaming["new_epc_name"], renaming["current_epc_name"]))
            continue
        # We do not want to change the CCDB device_name, but the CCDB alias (i.e. the EPC device name)
        # Hence, we reuse the device_name returned from CCDB: ccde_device["name"] in the first two columns, and only append the new EPC expert name in the third column
        csv_entries.append('%s,%s,%s' % (ccde_device["name"], ccde_device["name"], renaming["new_epc_name"]))
    # Generate the csv file for ccde
    filename = 'ccde_bulk_rename.csv'
    with open(filename, 'w') as fp:
        fp.write('\n'.join(csv_entries))
    # Display the renaming procedure and ask the user to upload the generated .csv
    print('Renaming device aliases in CCDE:')
    if len(warnings) > 0:
        print('  0. The following entries were skipped:')
        for warning in warnings:
            print('    /!\ %s' % warning)
        if not confirm('    Do you want to proceed with the renaming and ignore these entries?'):
            if confirm('    Please go fix CCDE manually, then confirm here to re-try the renaming'):
                return api_rename_in_ccde(renamings)
    # If no device to rename and no error
    if len(csv_entries) == 1:
        print('  1. No device alias to rename.')
        return
    print('  1. Navigate there: https://ccde.cern.ch/devices/search/bulkRenames?query=&mode=b')
    print('  2. Upload the file named `%s` which has just been created in the current directory' % filename)
    if confirm('     Do you want to output the csv content here (in case you can not access the created csv)?'):
        print('\n[BEGINNING OF THE FILE]\n%s\n[END OF THE FILE]\n' % '\n'.join(csv_entries))
    # Wait for user confirmation that he did upload the file
    while not confirm('     Have you uploaded the file to CCDE?'):
        pass
    # Remove the csv
    os.system('rm %s' % filename)
    # Verifying changes
    print('     Verifying changes')
    warnings = []
    for renaming in renamings:
        if ccde_get_device(renaming["current_epc_name"]):
            warnings.append('Device alias %s still exists but should have been renamed to %s.' % (renaming["current_epc_name"], renaming["new_epc_name"]))
            continue
        if not ccde_get_device(renaming["new_epc_name"]):
            warnings.append('Device alias %s should have been renamed to %s, but device with alias %s does not exist in CCDE.' % (renaming["current_epc_name"], renaming["new_epc_name"], renaming["new_epc_name"]))
            continue
    if len(warnings) > 0:
        for warning in warnings:
            print('     /!\ %s' % warning)
        if confirm('     /!\ Some device aliases have NOT been renamed\n     Do you want to retry the whole renaming process for CCDE?'):
            return api_rename_in_ccde(renamings)
        else:
            print('     /!\ Some device aliases have NOT been renamed in CCDE. You choose to ignore this warning.')


def api_rename_in_ccs_db(updates=None, sql_password=None):
    updates = updates or prompt_device_updates()
    # Filter the device updates to only keep the renamings
    renamings = [update for update in updates if update['action'] == 'update' and update['current_epc_name'] != update['new_epc_name']]
    sql_password = sql_password or prompt_sql_password()

    # Get device in FGC database by name
    def get_device_by_name(name: str) -> Optional[dict]:
        rows = sql_production_select(
            sql_password,
            "select sys_id, sys_name, sys_class_id from systems where sys_name=:name",
            {'name': name}
        )
        return {
            'id': rows[0][0],
            'name': rows[0][1],
            'class': rows[0][2],
        } if len(rows) == 1 else None

    for renaming in renamings:

        # If device has not been renamed
        if renaming["current_epc_name"] == renaming["new_epc_name"]:
            continue

        # Start renaming
        print("Renaming device '" + renaming["current_epc_name"] + "' to '" + renaming["new_epc_name"] + "'")

        # If device to rename is not in FGC database, skip
        device_with_old_name = get_device_by_name(renaming["current_epc_name"])
        if not device_with_old_name:
            print('Unable to rename %s to %s because %s is not in FGC database. Bear in mind that not all device classes are in FGC database. Skipping.' % (renaming["current_epc_name"], renaming["new_epc_name"], renaming["current_epc_name"]))
            continue

        # If new device name already exists, skip
        if get_device_by_name(renaming["new_epc_name"]):
            print('Unable to rename %s to %s because there is already a device named %s. Skipping.' % (renaming["current_epc_name"], renaming["new_epc_name"], renaming["new_epc_name"]))
            continue

        # Try to update the device
        sql_production_execute(
            sql_password,
            'update systems set sys_name=:new_epc_name where sys_name=:old_epc_name',
            {'old_epc_name': renaming["current_epc_name"], 'new_epc_name': renaming["new_epc_name"]}
        )

        # Try to update names of spare combinations related to the device
        sql_production_execute(
            sql_password,
            """update systems set sys_name=replace(sys_name, :old_epc_name, :new_epc_name)
            where sys_id in (select scm_comb_id from fgc_spare_combination_mappings where scm_op_id=:operational_device_id)""",
            {'new_epc_name': renaming["new_epc_name"], 'old_epc_name': renaming["current_epc_name"], 'operational_device_id': device_with_old_name['id']}
        )

        # Ensuring that the device has been renamed
        if not get_device_by_name(renaming["new_epc_name"]) or get_device_by_name(renaming["current_epc_name"]):
            print('Unable to rename %s to %s. Please rename the device manually.' % (renaming["current_epc_name"], renaming["new_epc_name"]))
            continue


def api_run_update_devices():
    while confirm("Do you want to run a simulation to preview the changes?"):
        run_update_devices(True)
    run_update_devices(False)


def api_reset_abpo1():
    rbac_token = get_rbac_token()
    reset_abpo1(rbac_token)
    print('Reseted status server abpo1')


def api_build_access_map(gateways=None):
    gateways = gateways if gateways is not None else prompt_list('gateways')

    # For each selected gateway, generate new access map
    for gateway in gateways:
        command = '/user/rbac/tools/rbac-maps-server/bin/RBAC-MAPS-SERVER.jvm FGC_' + gateway.upper()
        os.system(command)


def api_load_new_access_maps(gateways = None):
    gateways = gateways if gateways is not None else prompt_list('gateways')
    rbac_token = get_rbac_token()

    print("Loading new access map")
    for gateway in gateways:
        try:
            load_new_access_maps(gateway, rbac_token)
        except Exception as error:
            print('  /!\ Unable to load access map to %s: %s' % (gateway, error))


def api_reset_gateways(gateways = None):
    gateways = gateways if gateways is not None else prompt_list('gateways')
    rbac_token = get_rbac_token()

    print("Resetting gateway(s)")
    for gateway in gateways:
        try:
            reset_gateway(gateway, rbac_token)
        except Exception as error:
            print('  /!\ Unable to reset gateway: %s' % (gateway, error))


def api_move_device_config_backups(updates=None):
    updates = updates or prompt_device_updates()
    # Filter the device updates to only keep the renamings and deletion
    renamings = [update for update in updates if update['action'] == 'update' and update['current_epc_name'] != update['new_epc_name']]
    deletions = [update for update in updates if update['action'] == 'delete']
    # Init the output variable
    output_str = ''
    # Handle renamings
    if len(renamings) > 0:
        output_str += '\nMoving device configuration backup files\n'
        for renaming in renamings:
            # Check that the current file exists
            if not device_config_backup_exists(renaming['current_epc_name']):
                output_str += '  - %s -> %s: Current device configuration backup not found. Skipped.\n' % (renaming['current_epc_name'], renaming['new_epc_name'])
                continue
            # Check that the new file does not exist
            if device_config_backup_exists(renaming['new_epc_name']):
                output_str += '  - %s -> %s: New device configuration backup already exists. Skipped.\n' % (renaming['current_epc_name'], renaming['new_epc_name'])
                continue
            # Actually move the file
            device_config_backup_move(renaming['current_epc_name'], renaming['new_epc_name'])
            output_str += '  - %s -> %s: File renamed\n' % (renaming['current_epc_name'], renaming['new_epc_name'])
    # Handle deletions
    if len(deletions) > 0:
        output_str += '\nRemoving obsolete device configuration backup files\n'
        for renaming in renamings:
            # Check that the current file exists
            if not device_config_backup_exists(renaming['current_epc_name']):
                output_str += '  - %s: Current device configuration backup not found. Skipped.\n' % renaming['current_epc_name']
                continue
            # Actually remove the file
            device_config_backup_remove(renaming['current_epc_name'], renaming['new_epc_name'])
            output_str += '  - %s: File deleted\n' % renaming['current_epc_name']
    print(output_str)


def api_move_nonvolatile_storage(updates=None):
    updates = updates or prompt_device_updates()
    updates = [update for update in updates if update['action'] == 'update' and (update['current_epc_name'] != update['new_epc_name'] or update['new_gateway'])]
    deletions = [update for update in updates if update['action'] == 'delete']
    output_str = ''
    for update in updates:
        try:
            new_gateway = update['new_gateway'] or update['current_gateway']
            nonvolatile_storage_move(update['current_gateway'], update['new_epc_name'], new_gateway, update['new_epc_name'])
        except Exception as error:
            output_str += 'Error %s: %s\n' % (update, error)
    for deletion in deletions:
        try:
            nonvolatile_storage_remove(deletion['current_gateway'], deletion['new_epc_name'])
        except Exception as error:
            output_str += 'Error %s: %s\n' % (deletion, error)
    print(output_str)


def api_check_namefile():

    # Load name file
    namefile = git_file_parse(namefile_read_current())

    # If no error raised
    print("Ok")


def api_search_device_information(devices: list = None):

    # Load name file
    namefile = git_file_parse(namefile_read_current())

    # Prompt devices to delete
    output_str = ''
    devices = prompt_device_list()
    for device in devices:

        output_str += '\n%s:\n' % device

        namefile_match = [line for line in namefile if line[3] == device]
        namefile_match = namefile_match[0] if len(namefile_match) > 0 else None
        if namefile_match:
            namefile_match_str = git_file_parsed_to_text('name', [namefile_match]).strip()
            output_str += '  namefile entry : %s' % namefile_match_str + '\n'

            subdevices = git_file_parse(subdevices_read_current(namefile_match[0]))
            subdevices_matches = [line for line in subdevices if line[0] == device]

            for subdevices_match in subdevices_matches:
                output_str += '  subdevice      : %s' % git_file_parsed_to_text('name', [subdevices_match]).strip() + '\n'
        else:
            output_str += '  not in namefile\n'

        # Try to match CCDE
        data = ccde_get_device(device)
        if data:
            output_str += '  ccde           : https://ccde.cern.ch/devices/%d/relations' % data['id'] + '\n'
        else:
            output_str += '  not in ccde\n'

    print(output_str)


def api_load_class91_properties_from_nfs():

    # Prompt devices to delete
    devices = prompt_list('devices')

    # Get all properties organized by device
    device_properties = read_devices_properties(devices)

    # Get the list of all properties
    all_properties = sorted(list(set([property for properties in device_properties.values() for property in properties.keys()])))

    # Build a csv with the result
    csv_rows = []
    csv_rows.append(['device', 'status'] + all_properties)
    for device in devices:

        # If the device has been found
        if device in device_properties:
            # Build the property list
            csv_properties = [
                device_properties[device][property] if property in device_properties[device] else ''
                for property in all_properties
            ]
            # Add the device row
            csv_rows.append([device, 'OK'] + csv_properties)
        else:
            # If the device has not been found
            csv_rows.append([device, 'NOT FOUND'] + ['' for i in range(len(all_properties))])

    # Build csv content out of the rows
    csv_content = "\n".join(';'.join(csv_row) for csv_row in csv_rows)
    print(csv_content)

    # Output filepath
    if confirm('Do you want to save the result?'):
        print('Enter file path:')
        output_path = input('>>> ')
        with open(output_path, 'w+') as file:
            file.write(csv_content)
