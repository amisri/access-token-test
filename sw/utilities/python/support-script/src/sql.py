import cx_Oracle
import subprocess
from typing import Optional


CCS_DB_USER = 'pocontrols_mod'
CCS_DB_DSN = """(DESCRIPTION=
(ADDRESS=(PROTOCOL=TCP)(HOST=acccon-s.cern.ch)(PORT=10121))
(ENABLE=BROKEN)
(CONNECT_DATA = (SERVICE_NAME = acccon_s.cern.ch)))"""


def sql_production_verify_password(password: str) -> bool:
    try:
        with cx_Oracle.connect(CCS_DB_USER, password, CCS_DB_DSN) as dbc:
            pass
        return True
    except Exception as error:
        print(error)
        return False


def sql_production_execute(password: str, query: str, values: Optional[dict] = None):
    with cx_Oracle.connect(CCS_DB_USER, password, CCS_DB_DSN) as dbc:
        with dbc.cursor() as cursor:
            cursor.execute(query, values or {})
        dbc.commit()


def sql_production_select(password: str, query: str, values: Optional[dict] = None):
    with cx_Oracle.connect(CCS_DB_USER, password, CCS_DB_DSN) as dbc:
        with dbc.cursor() as cursor:
            return [row for row in cursor.execute(query, values or {})]
