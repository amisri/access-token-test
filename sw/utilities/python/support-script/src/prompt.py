import sys
from distutils.util import strtobool


def confirm(question: str, default: bool = None) -> bool:
    if default is None:
        prompt = " [y/n] "
    elif default:
        prompt = " [Y/n] "
    elif not default:
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)

        try:
            choice = input().lower()
            if choice == "" and default is not None:
                return default
            return strtobool(choice)
        except ValueError:
            sys.stdout.write("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")
            pass
