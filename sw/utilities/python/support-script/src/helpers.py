import difflib
import os


def read_multilines(message: str) -> str:
    print(message)
    output = ''
    while True:
        new_line = input()
        if new_line == '':
            break
        else:
            output += new_line + "\n"
    return output[:-1]


def remote_ssh(user: str, machine: str, command: str):
    command = 'ssh %s@%s "%s"' % (user, machine, command.replace("\"", "\\\""))
    print('# ' + command)
    return os.popen(command).read()


def diff_contents(content1: str, content2: str) -> str:
    output = []
    for text in difflib.unified_diff(content1.split("\n"), content2.split("\n")):
        if text[:3] not in ('+++', '---', '@@ '):
            output.append(text)
    return "\n".join(output)


def ssh_file_exists(ssh_user: str, ssh_host: str, filepath: str) -> bool:
    return remote_ssh(ssh_user, ssh_host, 'ls -d %s' % filepath).strip() != ""
