from .api import *

available_commands = {
    'u': ['(u)pdate/rename/create/delete devices', api_update_devices],
    'c': [' |- (c)heck namefile', api_check_namefile],
    'r': [' |- (r)eset abpo1', api_reset_abpo1],
    'e': [' |- r(e)name in ccde', api_rename_in_ccde],
    'b': [' |- (b)uild new access map', api_build_access_map],
    'l': [' |- (l)oad new access map', api_load_new_access_maps],
    'a': [' |- ren(a)me devices in ccs database', api_rename_in_ccs_db],
    'm': [' |- update device (m)etadata in ccde', api_run_update_devices],
    'v': [' |- mo(v)e device configuration backup files', api_move_device_config_backups],
    'p': [' |- u(p)date non-volatile storage', api_move_nonvolatile_storage],
    'i': ['search devices (i)nformation', api_search_device_information],
    'o': ['l(o)ad class91 devices properties from NFS and export to CSV', api_load_class91_properties_from_nfs],
}

# Get valid command
while True:

    # Print available commands
    print('Choose an action:')
    for shortcut in available_commands.keys():
        print('  ' + shortcut + ': ' + available_commands[shortcut][0])

    # Prompt command
    command = input('>>> ')
    if command in available_commands:
        break

# Run command
available_commands[command][1]()
