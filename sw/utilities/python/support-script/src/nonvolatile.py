from .helpers import ssh_file_exists, remote_ssh

USER = 'pclhc'
HOST = 'accwww.cern.ch'


def nonvolatile_storage_get_path(gw: str, device: str):
    return '/user/pclhc/var/log/fgcd/%s/device_config/%s' % (gw.lower(), device.upper())


def nonvolatile_storage_get_path_last(gw: str, device: str):
    return '/user/pclhc/var/log/fgcd/%s/device_config/last/%s' % (gw.lower(), device.upper())


def nonvolatile_storage_remove(gw: str, name: str):
    remote_ssh(USER, HOST, 'rm %s' % (nonvolatile_storage_get_path(gw, name)))
    remote_ssh(USER, HOST, 'rm %s' % (nonvolatile_storage_get_path_last(gw, name)))


def nonvolatile_storage_move(current_gw: str, current_epc_name: str, new_gw: str, new_epc_name: str):

    if ssh_file_exists(USER, HOST, nonvolatile_storage_get_path(new_gw, new_epc_name)) or ssh_file_exists(USER, HOST, nonvolatile_storage_get_path_last(new_gw, new_epc_name)):
        raise Exception('Unable to move non-volatile storage: New file already exists')

    if ssh_file_exists(USER, HOST, nonvolatile_storage_get_path(current_gw, current_epc_name)):
        remote_ssh(USER, HOST, 'sudo mv %s %s' % (nonvolatile_storage_get_path(current_gw, current_epc_name), nonvolatile_storage_get_path(new_gw, new_epc_name)))

    if ssh_file_exists(USER, HOST, nonvolatile_storage_get_path_last(current_gw, current_epc_name)):
        remote_ssh(USER, HOST, 'sudo mv %s %s' % (nonvolatile_storage_get_path_last(current_gw, current_epc_name), nonvolatile_storage_get_path_last(new_gw, new_epc_name)))
