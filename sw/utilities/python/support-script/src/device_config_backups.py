import typing
from .helpers import remote_ssh, ssh_file_exists

USER = 'pofgcadm'
HOST = 'cs-ccr-dev3.cern.ch'


def device_config_backup_get_path(name: str) -> str:
    return '/user/pclhc/etc/config/devices/%s.txt' % name.upper()


def device_config_backup_exists(name: str) -> bool:
    return ssh_file_exists(USER, HOST, device_config_backup_get_path(name))


def device_config_backup_move(current_epc_name: str, new_epc_name: str):
    command = 'mv %s %s' % (device_config_backup_get_path(current_epc_name), device_config_backup_get_path(new_epc_name))
    remote_ssh(USER, HOST, command)


def device_config_backup_remove(name: str):
    command = 'rm %s' % device_config_backup_get_path(name)
    remote_ssh(USER, HOST, command)
