import pyfgc_rbac, ccs_sso_utils
import getpass


# Defined in the application portal
OAUTH2_APP_ID = 'ccs-support-script'
OAUTH2_REDIRECT_URI = 'ch.cern.ccs-support-script:/oauth2redirect'

# Attribute filled on-demand
rbac_token = None

def get_rbac_token():
    global rbac_token

    if rbac_token:
        return rbac_token
    

    # Try with Kerberos
    try:
        print('Getting a RBAC token using Kerberos')
        token = ccs_sso_utils.get_sso_token(OAUTH2_REDIRECT_URI, OAUTH2_APP_ID, verify=False)
        rbac_token = pyfgc_rbac.get_token_oauth2(token, OAUTH2_APP_ID)
    except Exception:
        print('Failed to get a RBAC token using Kerberos. Using username/password instead')

        # If not possible, try with username + password until credentials are approved
        while not rbac_token:
            try:
                rbac_token = pyfgc_rbac.get_token_login(getpass.getuser(), getpass.getpass())
            except Exception:
                pass
    
    return rbac_token
