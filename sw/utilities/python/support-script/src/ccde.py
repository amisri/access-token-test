import requests
import json
import os
from .prompt import *

def ccde_get_device(epc_device_name: str):
    """Returns a CCDB device definition based on the provided epc_device_name.
    EPC device names correspond to CCDB device aliases, which is why the CCDA /devices/alias/ endpoint is being used.
    https://wikis.cern.ch/display/TEEPCCCS/Converter+names%3A+Aliases+vs+full+names
    """
    request = requests.get("https://ccda.cern.ch:8900/api/devices/alias/%s" % epc_device_name, verify=False)
    return json.loads(request.content) if request.status_code == 200 else None


def run_update_devices(simulation: bool):
    os.system('cd ../../perl/databases/controls && pwd && ./update_devices.pl' + ('' if simulation else ' --commit'))


def ccde_update_rbac_groups(updates: list):
    print("""
    Add devices to device group map (or create a new group if an appropriate one doesn't exist).
    These groups are later used to define the RBAC access right rules.
        1. Navigate there: https://ccde.cern.ch/rbac/propertyDeviceGroups
            Or go to CCDE → RBAC Editor → Property & Device Groups.
        2. Fill the field "Please select a device class" with the class of the device you're adding (for example, for 63 it would be FGC_63).
        3. In the right panel "Device Groups" find the group that you want to add your device to.
            You need to know the physical location (accelerator complex) of the device(s) to find the correct group.
            If you are still unsure you may check the group of other device on the same gateway.
        4. Click on the edit button for this group.
            A new pop-up window will appear.
            In the left panel you have all the FGC_63 (or other class you selected) devices that are still available to add to this group.
            In the right panel you have devices already in this group.
        5. Search for the device(s) you are adding in the left panel (using "Device name" or "FEC Name" filter). Once found, click on the "plus" button.
            Note: "Device name" corresponds to the EPC alias (operational name), unless there is no operational name (e.g. lab devices) - then use the EPC device name.
        7. Make sure to also add any new virtual/sub device(s) to the group.
        6. At the end click an "Update" button.
    """)
    if not confirm('Is this done?'):
        ccde_update_rbac_groups(updates)
