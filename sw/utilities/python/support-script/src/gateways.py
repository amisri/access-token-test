import pyfgc


def reset_abpo1(rbac):
    reset_gateway('CS-CCR-ABPO1', rbac)


def load_new_access_maps(gateway_name, rbac):
    with pyfgc.fgc(gateway_name, protocol='sync', rbac_token=rbac) as fgc:
        response = fgc.set('fgcd.readnames', '')
        if response.err_msg != "":
            raise Exception("Unable to load new access map for gateway " + gateway_name + ": " + response.err_msg)
        print('readnames', response)
        response = fgc.set('fgcd.readrbac', '')
        if response.err_msg != "":
            raise Exception("Unable to load new access map for gateway " + gateway_name + ": " + response.err_msg)
        print('readrbac', response)


def reset_gateway(gateway_name, rbac_token):
    response = pyfgc.set(gateway_name, 'device.reset', '', protocol='sync', rbac_token=rbac_token)
    print('device.reset', response)
    if response.err_msg != "":
        raise Exception("Unable to reset device %s" % gateway_name)
