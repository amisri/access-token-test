import getpass
import os
from .helpers import *

DEV_MACHINE = 'cs-ccr-dev3.cern.ch'

def read_devices_properties(devices):
    device_properties = {}
    for device in devices:
        properties = read_device_properties(device)
        if properties:
            device_properties[device] = properties
    return device_properties

def read_device_properties(device):

    # Try to get the configuration path to the properties
    config_path = remote_ssh(getpass.getuser(), DEV_MACHINE, 'cd /acc/dsc/sps/*/data/fgcd/device_config/%s && pwd' % device)
    if not config_path:
        return None

    # Remove newline char at the end
    config_path = config_path.strip()
    # Path to non-volatile operational references
    last_config_path = '/'.join(config_path.split('/')[:-1]) + '/last/' + device

    # Keeps track of property values
    properties = {}

    # For each property, store its value in the dict
    for property in os.listdir(config_path):
        with open(config_path + '/' + property, 'r') as file:
            properties[property] = file.readline().strip()
    for property in os.listdir(last_config_path):
        with open(last_config_path + '/' + property, 'r') as file:
            properties[property] = file.readline().strip()

    return properties
