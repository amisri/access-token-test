import pyfgc
import pyfgc_name
import copy
import os
from typing import List, Dict, Tuple, Optional
from .helpers import *
from .prompt import *


DEV_MACHINE = None
REPO_REMOTE = ''
REPO_PATH = 'fgc_prod/fgcd/'
NAME_FILE_PATH = REPO_PATH + 'name'
KEEP_FILE_PATH = REPO_PATH + 'keep'
GROUP_FILE_PATH = REPO_PATH + 'group'
ALIAS_FILE_PATH = REPO_PATH + 'sub_devices/'
ALIAS_KEEP_FILE_PATH = REPO_PATH + 'sub_devices_keep/'


def ensure_name_file_repo_up_to_date():
    # Ensure repo exists. If not, clone it
    if not os.path.exists(REPO_PATH):
        print('no exist.')
        os.system(f'git clone https://gitlab.cern.ch/ccs/fgc_prod.git')
        # We just cloned it so it is up to date
        return
    # Go in repo and pull
    os.system(f'cd {REPO_PATH} && git fetch --prune origin master && git reset --hard origin/master')


def git_get_unstaged() -> List[str]:
    # Get list of files that are not staged
    filenames = os.popen(f'cd {REPO_PATH} && git diff --name-only name group sub_devices').read().split('\n')
    filenames = [filename.strip() for filename in filenames if filename.strip() != '']
    return filenames


def git_file_parse(file_content: str) -> List[List[str]]:
    return [line.split(':') for line in file_content.split('\n') if len(line) > 1] if file_content else []


def git_file_parsed_to_text(filename: str, parsed: List[List[str]]) -> str:
    """
    :filename: Name of the file 'group' 'name' or a gateway name for subdevice files
    """
    # Sort lines, in case the sort has been corrupted
    if filename == 'group':
        # For the group file we sort by the strings
        lines = parsed
        #lines = sorted(parsed, key=lambda line: [line[0], line[1]])
    else:
        # For the name and sub device files we sort by gateway and dongle/subdevice index
        lines = sorted(parsed, key=lambda line: [line[0], int(line[1])] if not line[0].strip().startswith('#') else ['a', 0])

    # Build the namefile as a string and return it
    lines = [':'.join(line) for line in lines]
    return "\n".join(lines).strip() + "\n"


def namefile_read_current() -> str:
    return os.popen(f'cat {NAME_FILE_PATH}').read().strip()


def namefile_set_content(content: str):
    with open(NAME_FILE_PATH, 'w+') as f:
        f.write(content)


def keepfile_read_current() -> str:
    return os.popen(f'cat {KEEP_FILE_PATH}').read().strip()


def keepfile_set_content(content: str):
    with open(KEEP_FILE_PATH, 'w+') as f:
        f.write(content)


def groupfile_read_current() -> str:
    return os.popen(f'cat {GROUP_FILE_PATH}').read().strip()


def groupfile_set_content(content: str):
    with open(GROUP_FILE_PATH, 'w+') as f:
        f.write(content)


def subdevices_get_file_path(gateway: str, keep=False) -> str:
    if keep:
        return ALIAS_KEEP_FILE_PATH + gateway.lower()
    else:
        return ALIAS_FILE_PATH + gateway.lower()


def subdevices_read_current(gateway: str, keep=False) -> str:
    return os.popen(f'cat {subdevices_get_file_path(gateway, keep=keep)}').read().strip()


def subdevices_set_content(gateway: str, content: str, keep=False):
    filepath = subdevices_get_file_path(gateway, keep=keep)
    # Create a local file that will be scp-ed to the dev machine
    with open(filepath, 'w+') as f:
        f.write(content)


def git_apply_update(update: dict, files: dict) -> Tuple[dict, List[str]]:

    # Read the namefile if necessary, it will be required for any action
    if 'name' not in files:
        file_content = namefile_read_current()
        files['name'] = {
            'current': file_content,
            'new': git_file_parse(file_content)
        }

    if 'keep' not in files:
        file_content = keepfile_read_current()
        files['keep'] = {
            'current': file_content,
            'new': git_file_parse(file_content)
        }

    if 'group' not in files:
        file_content = groupfile_read_current()
        files['group'] = {
            'current': file_content,
            'new': git_file_parse(file_content)
        }

    # Keep track of affected gateways
    affected_gateways = []

    # Extract action
    action = update['action']

    # Create a new device
    if action == 'create':
        # Add the entry in the namefile
        files['name']['new'].append([update['new_gateway'], update['new_dongle'], update['new_class'], update['new_epc_name'], update['new_mask']])
        if update['new_gateway'].lower() == update['new_epc_name'].lower():
            # If adding a gateway, add gateway to the group file
            files['group']['new'].append([update['new_epc_name'].lower(), update['new_epc_alias']])
        elif update['new_epc_alias'] and update['new_epc_alias'] != '-':
            # If adding FGC and an alias is provided, add the alias
            # Read the file if required
            if update['new_gateway'] not in files:
                file_content = subdevices_read_current(update['new_gateway'])
                files[update['new_gateway']] = {
                    'current': file_content,
                    'new': git_file_parse(file_content)
                }
            # Add the alias
            files[update['new_gateway']]['new'].append([update['new_epc_name'], '0', update['new_epc_alias']])
        affected_gateways.append(update['new_gateway'])

    # Delete a new device
    if action in ('delete', 'temporary_delete'):
        # Find device in the list
        current_entry = [row for row in files['name']['new'] if row[3] == update['current_epc_name']][0]
        current_gateway = current_entry[0]
        is_gateway = current_entry[0] == current_entry[3].lower()
        if is_gateway:
            # If removing gateway, remove gateway from group file
            files['group']['new'] = [row for row in files['group']['new'] if row[0] != update['current_epc_name'].lower()]
        else:
            # If removing FGC, remove FGC from sub devices file
            file_content = subdevices_read_current(current_gateway)
            if file_content:
                if current_gateway not in files:
                    files[current_gateway] = {'current': file_content, 'new': git_file_parse(file_content)}

                # Add sub-device to gateway's subdevices keepfile
                if action == 'temporary_delete':
                    # If removing FGC, remove FGC from sub devices file
                    keep_sub_content = subdevices_read_current(current_gateway, keep=True)

                    if keep_sub_content:
                        if 'keep' not in files[current_gateway]:
                            files[current_gateway]['keep'] = {'current': keep_sub_content, 'new': git_file_parse(keep_sub_content)}

                    subdevice_to_keep = [row for row in files[current_gateway]['new'] if row[0] == update['current_epc_name']][0]
                    files[current_gateway]['keep']['new'].append(subdevice_to_keep)
                    while not confirm("Set 'State' of %s to 'development' in CCDB: https://ccde.cern.ch/devices/search?query=%s\nDone?" % (update['current_epc_name'], update['current_epc_name']), True):
                        pass

                # Remove entry from the subdevice file
                files[current_gateway]['new'] = [row for row in files[current_gateway]['new'] if row[0] != update['current_epc_name']]

        # If the device must be stored in case of temporary delete
        if action == 'temporary_delete':
            to_keep = [row for row in files['name']['new'] if row[3] == update['current_epc_name']][0]

            # check if gateway already exists
            gateway_in_keep = [row for row in files['keep']['new'] if not row[0].strip().startswith('#') and row[3] == current_gateway.upper()]

            if gateway_in_keep:
                if not is_gateway:
                    files['keep']['new'].append(to_keep)
                    while not confirm("Set 'State' of %s to 'development' in CCDB: https://ccde.cern.ch/devices/search?query=%s\nDone?" % (update['current_epc_name'], update['current_epc_name']), True):
                        pass
            else:
                gateway_entry = [row for row in files['name']['new'] if row[3] == current_gateway.upper()][0]
                files['keep']['new'].extend([to_keep, gateway_entry])

        # Remove the device/gw from the namefile
        files['name']['new'] = [row for row in files['name']['new'] if row[3] != update['current_epc_name']]
        affected_gateways.append(current_gateway)

    # Update a device
    if action == 'update':
        # Find device index in the list
        device_index = [row[3] for row in files['name']['new']].index(update['current_epc_name'])
        current_device_row = files['name']['new'][device_index]
        current_device_gw = current_device_row[0]
        # We need to update the alias if the gateway changed, if the device has been renamed or if the alias changed
        gateway_changed = not not update['new_gateway'] and current_device_gw != update['new_gateway']
        name_changed = not not update['new_epc_name'] and update['current_epc_name'] != update['new_epc_name']
        alias_changed = not not update['new_epc_alias']
        class_changed = not not update['new_class']
        # If we need to update the alias
        if alias_changed or name_changed or gateway_changed:
            # Load current gateway subdevices file
            if current_device_gw not in files:
                file_content = subdevices_read_current(current_device_gw)
                files[current_device_gw] = {'current': file_content, 'new': git_file_parse(file_content)}
            # Load new gateway subdevices file, if moving device to a new gateway
            new_gateway_name = update['new_gateway']
            if new_gateway_name and new_gateway_name not in files:
                file_content = subdevices_read_current(new_gateway_name)
                files[new_gateway_name] = {'current': file_content, 'new': git_file_parse(file_content)}
            # Update alias if needed
            if alias_changed:
                if update['new_epc_alias'] == '-':
                    # First, remove the old alias, even ppm aliases
                    files[current_device_gw]['new'] = [row for row in files[current_device_gw]['new'] if row[0] != update['current_epc_name']]
                else:
                    # First, remove the old alias
                    files[current_device_gw]['new'] = [row for row in files[current_device_gw]['new'] if row[0] != update['current_epc_name'] or row[1] != '0']
                    # Add the new alias entry
                    files[current_device_gw]['new'] += [[update['current_epc_name'], '0', update['new_epc_alias']]]
            # Update device name if needed
            if name_changed:
                files[current_device_gw]['new'] = [
                    [update['new_epc_name'], row[1], row[2]] if row[0] == update['current_epc_name'] else row
                    for row in files[current_device_gw]['new']
                ]
            # If gateway changed
            if gateway_changed:
                # Get and remove the lines from the old gateway
                curr_gateway_lines = [row for row in files[current_device_gw]['new'] if row[0] == update['new_epc_name']]
                files[current_device_gw]['new'] = [row for row in files[current_device_gw]['new'] if row[0] != update['new_epc_name']]
                # Add the removed rows in the new gateway
                files[new_gateway_name]['new'] += curr_gateway_lines

        # Update entry in the namefile
        entries = ['new_gateway', 'new_dongle', 'new_class', 'new_epc_name', 'new_mask']
        for i in range(len(entries)):
            if update[entries[i]]:
                files['name']['new'][device_index][i] = update[entries[i]]

        # Add affected gateways
        if name_changed or alias_changed or gateway_changed or class_changed:
            affected_gateways.append(current_device_gw)
        if gateway_changed:
            affected_gateways.append(new_gateway_name)

    return files, list(set(affected_gateways))


def git_apply_updates(updates: List[dict], files: Optional[dict] = None) -> dict:
    files = files or {}
    affected_gateways = []
    for update in updates:
        files, new_affected_gateways = git_apply_update(update, files)
        affected_gateways += new_affected_gateways
    return files, list(set(affected_gateways))


def git_deploy_files(files: dict, commit_message: str):
    for filename, data in files.items():
        content = git_file_parsed_to_text(filename, data['new'])
        if filename == 'name':
            namefile_set_content(content)
        elif filename == 'group':
            groupfile_set_content(content)
        elif filename == 'keep':
            keepfile_set_content(content)
        else:
            # In all cases, push the gateway subdevice file
            subdevices_set_content(filename, content)

            if 'keep' in data:
                subdevices_set_content(filename, git_file_parsed_to_text(data['keep']['new']), keep=True)

    os.system(f'cd {REPO_PATH} && git commit -m "%s" name keep group sub_devices sub_devices_keep && git push origin master' % commit_message.replace("\"", "\\\""))
