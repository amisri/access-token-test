#!/bin/bash
set -ex
whoami

MY_VENV=~pclhc/bin/python/ccs_venv
MY_VENV_BACKUP=~pclhc/bin/python/ccs_venv.bak

# demote current env to back-up
if [[ -d $MY_VENV ]] ; then
  # (delete previous back-up if any)
  [[ -d $MY_VENV_BACKUP ]] && rm -rv $MY_VENV_BACKUP
  mv $MY_VENV $MY_VENV_BACKUP
fi

# error handling inspired by https://stackoverflow.com/a/25180186
(
  # activate Acc-Py & create virtual environment
  # FIXME: pclhc already has Acc-Py active from .bashrc, and it takes precedence over this
  source /acc/local/share/python/acc-py/base/2020.11/setup.sh || exit 1
  acc-py venv $MY_VENV || exit 1

  # descend into virtualenv, install packages
  source $MY_VENV/bin/activate || exit 1
  python --version || exit 1
  pip install -r ~pclhc/bin/python/ccs_venv.requirements.tmp || exit 1

  # add a note for anyone passing by
  # it would be really nice to have these available: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
  cat > $MY_VENV/README <<EOT
This is the CCS Python environment as per EPCCCS-8732.

This virtual environment is generated automatically, please do not attempt to modify it by hand.

If you really need to update a package ahead of the next re-deployment cycle, you can use a command like this:

ssh pclhc@cs-ccr-dev2 bin/python/ccs_venv/bin/pip install --upgrade <package-name>
EOT

# upon any error, restore the backed-up version and fail the pipeline
) || {
  echo "error: Virtualenv installation failed. Restoring back-up." >&2
  rm -rf $MY_VENV
  mv $MY_VENV_BACKUP $MY_VENV
  exit 1
}
