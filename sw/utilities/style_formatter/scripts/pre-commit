#!/bin/bash
#
# author	: Michal Dudek
# version	: 1.0.1
# last change	: 14.09.2013
#
# a git hook responsible for checking the code style
# in source files and headers before commit
#
# it does not stop the commit, just displays
# information about files with incorret formatting
#

# checking if the first commit exists
if git rev-parse --verify HEAD >/dev/null 2>&1
then
    against=HEAD
else
    # initial commit: diff against an empty tree object
    against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

# enables case insensitive string match
shopt -s nocasematch

# escape special characters
sed_escape_special_chars='sed -e 's/[/\$*.^]/\\\\\&/g''

# reading paths from environment variables
astyle_cmd="$ASTYLE"
astyle_cfg="$ASTYLE_CONFIG"

# getting time in seconds
date=$(date +%s)

# checking if the path to astyle is correct
command -v "$astyle_cmd" > /dev/null

if [ $? -ne 0 ]
then
    printf "The path to astyle program \"$astyle_cmd\" is incorrect! Exiting with code 1! Terminating commit!\n"
    exit 1
fi

# checking if config file for astyle exists
if [ -a "$astyle_cfg" ]
then
    astyle_cmd="$astyle_cmd --options=$astyle_cfg"
else
    printf "The config file \"$astyle_cfg\" for astyle does not exist! Exiting with code 1! Terminating commit!\n"
    exit 1
fi

# checking if system environment is set
if [ "$TMPDIR" != "" ]
then
    # using system config
    temporary_directory="$TMPDIR/"
else
    # otherwise using /tmp/
    temporary_directory="/tmp/"
fi

# exit codes of the script
success=0
failure=1
diff_exit_code=$success
script_exit_code=$success

printf 'Running astyle rules against files'

git diff-index -z --name-only --cached $against |
{
    while read -r -d $'\0' staged_file
    do
	if [[ "$staged_file" == *".c" ]] || [[ "$staged_file" == *".h" ]] || 
	   [[ "$staged_file" == *".cpp" ]] || [[ "$staged_file" == *".hpp" ]]
	then
	    # removing directory path, leaving only file name
	    # used when file is passed as subdirectory/source_file
	    original_file_name_only=$(basename "$staged_file")

	    # temporary file names templates
	    tmp_original_file_name="$original_file_name_only"_original.XXXX_"$date"
	    tmp_formatted_file_name="$original_file_name_only"_formatted.XXXX_"$date"
	    tmp_patch_no_color_file_name="$original_file_name_only"_patch_no_color.XXXX_"$date"
	    tmp_patch_color_file_name="$original_file_name_only"_patch_color.XXXX_"$date"

	    # temporary file paths
	    # tmp_original_file_path=$(mktemp --tmpdir="$temporary_directory" "$tmp_original_file_name") # msys not handling properly
	    # tmp_formatted_file_path=$(mktemp --tmpdir="$temporary_directory" "$tmp_formatted_file_name") # msys not handling properly
	    # tmp_patch_no_color_file_path=$(mktemp --tmpdir="$temporary_directory" "$tmp_patch_no_color_file_name") # msys not handling properly
	    # tmp_patch_color_file_path=$(mktemp --tmpdir="$temporary_directory" "$tmp_patch_color_file_name") # msys not handling properly

	    # msys in not properly handling mktemp (commented code above)
	    # code below is a replacement for it
	    tmp_original_file_path="$temporary_directory$tmp_original_file_name"
	    tmp_formatted_file_path="$temporary_directory$tmp_formatted_file_name"
	    tmp_patch_no_color_file_path="$temporary_directory$tmp_patch_no_color_file_name"
	    tmp_patch_color_file_path="$temporary_directory$tmp_patch_color_file_name"

	    # getting real temporary file names from the mktemp path
	    tmp_original_file_name=$(basename $tmp_original_file_path)
	    tmp_formatted_file_name=$(basename $tmp_formatted_file_path)
	    tmp_patch_no_color_file_name=$(basename $tmp_patch_no_color_file_path)
	    tmp_patch_color_file_name=$(basename $tmp_patch_color_file_path)

	    # making a copy of staged file
	    export_dir=".git-export-dir/"
	    git checkout-index -f --prefix="$export_dir" "$staged_file"
	    mv -f "$export_dir$staged_file" "$tmp_original_file_path"
	    cp "$tmp_original_file_path" "$tmp_formatted_file_path"

	    # running astyle
	    astyle_summary=$($astyle_cmd "$tmp_formatted_file_path")

	    # comparing original file with formatted file
	    # creating diff files
	    $(cd "$temporary_directory" && git diff --no-index "$tmp_original_file_name" "$tmp_formatted_file_name" > "$tmp_patch_no_color_file_path")
	    $(cd "$temporary_directory"	&& git diff --no-index --color=always "$tmp_original_file_name" "$tmp_formatted_file_name" > "$tmp_patch_color_file_path")

	    # assigning differences between files to variable
	    astyle_vs_user_coding=$(cat "$tmp_patch_no_color_file_path")

	    if [ "$astyle_vs_user_coding" == "" ]
	    then
		# printf "\n\t- coding style for file \"$staged_file\" is OK"
		diff_exit_code=$success
	    else
		printf "\n\t- coding style for file \"$staged_file\" is different from standard!"
		diff_exit_code=$failure
	    fi
	else
	    # printf "\n\t- omitting file \"$staged_file\""
	    diff_exit_code=$success
	fi

	rm -rf "$tmp_original_file_path" "$tmp_formatted_file_path" "$tmp_patch_no_color_file_path" "$tmp_patch_color_file_path" "$export_dir"
	let script_exit_code="$script_exit_code | $diff_exit_code"
    done

    if [ "$script_exit_code" == "$failure" ]
    then
	printf "\n\nFAIL: at least one of your file does not follow coding style!"
    else
	printf "\n\nOKAY: all of your files follow coding style"
    fi

    printf "\n\n------------------\n\n"
    exit $success
}