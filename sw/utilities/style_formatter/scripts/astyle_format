#!/bin/bash
#
# author        : Michal Dudek
# version       : 1.0
# last change   : 14.09.2013
#
# script responsible for formatting source code files and headers
#

# reading paths from environment variables
astyle_cmd="$ASTYLE"
astyle_cfg="$ASTYLE_CONFIG"

# checking if the path to astyle is correct
command -v "$astyle_cmd" > /dev/null

if [ $? -ne 0 ]
then
    printf "The path to astyle program \"$astyle_cmd\" is incorrect!\n"
    exit 1
fi

# checking if config file for astyle exists
if [ -a "$astyle_cfg" ]
then
    astyle_cmd="$astyle_cmd --options=$astyle_cfg"
else
    printf "The config file \"$astyle_cfg\" for astyle does not exist!\n"
    exit 1
fi

# checking if test program is installed
command -v test > /dev/null

if [ $? -ne 0 ]
then
    printf "Program \"test\" is not installed!\n"
    exit 1
fi

recursively=''

while getopts "R" OPTION
do
    case $OPTION in
        R)
            recursively=y
            ;;
    esac
done
shift $(($OPTIND-1))

# checking if any data was piped
test -t 0 > /dev/null

if [ $? -ne 0 ]
then
    files=$(cat -)
else
    files="$@"
fi

for file in $files
do
    if [ "$recursively" == "" ]
    then
	$astyle_cmd "$file"
    else
	$astyle_cmd -R "$file"
    fi
done
