#!/usr/bin/perl -w

# Script that can be used to debug functions that require a connection to a device or gateway

use FGC::Async;
use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sync;
use FGC::Utils;
use IO::Socket;
use strict;
use warnings;

use Data::Dumper;

use constant SUB_PERIOD => FGC_FIELDBUS_CYCLE_FREQ;

my $udp_socket;
my $udp_port_number;
my $devices; 
my $gateways;

sub init()
{
    socket($udp_socket, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

    for($udp_port_number = 1024 ; $udp_port_number <= 65535 ; $udp_port_number++)
    {
        bind($udp_socket, sockaddr_in($udp_port_number, INADDR_ANY)) && last;
    }

    ($devices, $gateways) = FGC::Names::read();
    FGC::Async::init($devices, $gateways);
}

sub con($)
{
    my ($device_name) = @_;

    my $device = $devices->{$device_name};
    my $gw = $gateways->{$device->{gateway}->{name}};

    my $rbac_token = FGC::RBAC::authenticate_by_location();

    eval { 
        my $socket = FGC::Async::connect($gw);
        $gw->{socket} = $socket;
        $device->{gateway}->{socket} = $socket;
        
        FGC::Async::set($gw->{channels}->[0], "CLIENT.TOKEN", \$rbac_token, 1);
        FGC::Sync::set($gw->{socket}, 0, "CLIENT.UDP.SUB.PERIOD", SUB_PERIOD);
        FGC::Sync::set($gw->{socket}, 0, "CLIENT.UDP.SUB.PORT",   $udp_port_number);
    };
    if($@)
    {
        die "ERROR: Unable to connect to gateway $gw->{name} $@";
    }

    return $device;
}

init();
my $device = con("RFNA.866.03.ETH2"); #63
#my $device = con("RPAFL.866.1.FIP9"); #92
#my $device = con("CFC-866-RFIP9"); #6


##### DEBUG HERE #####

#use FGC::ExtRegulation;
#my $reg = FGC::ExtRegulation->new($device, FGC::ExtRegulation::CONTROL_LOOP_CURRENT, FGC::ExtRegulation::EXEC_MODE_MODEL);
#$reg->setInputModel('100', '0.8', '0.5');
#$reg->setInputOptMethod(FGC::ExtRegulation::OPT_METHOD_INF_N);
#$reg->{quality} = 0.5;
#
#my ($text, $col) = $reg->getQuality();
#
#print(Dumper($text));
#print(Dumper($col));


# E.g. debug get_device_api
my $api = FGC::Utils::get_device_api($device);
print(Dumper($api));
