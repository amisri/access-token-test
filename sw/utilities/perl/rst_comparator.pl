#!/usr/bin/perl

use File::Path qw(make_path);

use Test::Utils;
use FGC::Log qw(decode compose);

use strict;
use warnings;
use diagnostics;



sub rampTo
{
    my ($device, $ref) = @_;

    set($device, "REF.BCV.VALUE", $ref);
    wait_until( sub { get($device, "STATE.DIRECT") eq "IDLE" }, 30, 0.5);
}



sub getLogMenuProperty
{
    my ($device, $log_menu) = @_;

    my @log_menu_names = split(/,/, get($device, "LOG.MENU.NAMES"));
    my @log_menu_props = split(/,/, get($device, "LOG.MENU.PROPS"));

    if(scalar @log_menu_names != scalar @log_menu_props)
    {
        die "Number of elements in LOG.MENU.NAMES doesn't match LOG.MENU.PROPS";
    }

    # Find index of requested log menu

    my ($index) = grep { $log_menu_names[$_] eq $log_menu } 0..$#log_menu_names;

    if(!$index)
    {
        die "Couldn't find log menu \"$log_menu\" in LOG.MENU.NAMES array";
    }

    my $log_menu_prop = $log_menu_props[$index];

    # Add non-PPM user selector to the property name (replace '()' with '(0)')

    $log_menu_prop =~ s/\(\)/\(0\)/;

    return $log_menu_prop;
}



my $device = "";

my @rsts = (
               { name => "resident",                                          filter => "0,0"   },
               #{ name => "xsep/RST_10Hz_zeta_0p9_FIRLENGTH_200.txt",          filter => "200,0" },
               #{ name => "xsep/RST_10Hz_zeta_0p9_FIRLENGTH_200_WEIGHTED.txt", filter => "200,0" },
               #{ name => "xsep/RST_10Hz_zeta_0p9.txt",                        filter => "0,0"   },
           );

my %opts = (protocol=>'');
   %opts = parse_opts %opts;

my @ramps = (
                #{ start => "50", final => "4300" },
                #{ start => "4300", final => "50" },
                { start => "2500", final => "2500.1" },
                { start => "2500", final => "2500.5" },
                { start => "2500", final => "2501" },
                { start => "2500", final => "2505" },
                { start => "2500", final => "2510" },
                { start => "2500", final => "2600" },
                { start => "2500", final => "3500" },

                #{ start => "500", final => "500.1" },
                #{ start => "500", final => "500.5" },
                #{ start => "500", final => "501"   },
                #{ start => "500", final => "505"   },
                #{ start => "500", final => "510"   },
                #{ start => "500", final => "530"   },
                #{ start => "500", final => "600"   },
                #
                #{ start => "-500", final => "-500.1" },
                #{ start => "-500", final => "-500.5" },
                #{ start => "-500", final => "-501"   },
                #{ start => "-500", final => "-505"   },
                #{ start => "-500", final => "-510"   },
                #{ start => "-500", final => "-530"   },
                #{ start => "-500", final => "-600"   },
            );

my @ramp_log_menus = ("B_REG", "B_MEAS");

my @ramp_log_props = map { getLogMenuProperty($device, $_) } @ramp_log_menus;

my $ramp_time_offset = 8;

foreach my $rst (@rsts)
{
    print "\nRST: " . $rst->{name} . "\n\n";

    my $path = $rst->{name};
    $path =~ s/.txt//;

    make_path($path);

    # Wait for OFF or FLT_OFF

    set($device, "PC", "OF");
    wait_until( sub { get($device, "STATE.PC") =~ /OFF/} );

    # Load RST

    if($rst->{name} ne "resident")
    {
        load_config($device, $rst->{name});
    }

    set($device, "MEAS.B_FIR_LENGTHS", $rst->{filter});
    set($device, "REG.B.EXTERNAL.MEAS_SELECT", "FILTERED");

    sleep(2);
    set($device, "PC", "OF");
    wait_until( sub { get($device, "STATE.PC") =~ /OFF/} );

    # Go to STANDBY and then to DIRECT

    set($device, "REF.FUNC.REG_MODE", "B");
    setPc($device, "SB");
    set($device, "REF.BCV.VALUE", get($device, "LIMITS.B.MIN[0]"));
    setPc($device, "DT");

    set($device, "LOG.SPY.TIME_OFFSET", $ramp_time_offset);

    foreach my $ramp (@ramps)
    {
        print "Ramp from " . $ramp->{start} . " to " . $ramp->{final} . "\n";

        my @buffers = ();

        rampTo($device, $ramp->{start});

        sleep($ramp_time_offset);

        rampTo($device, $ramp->{final});

        sleep(3);

        # Acquire all requested logs

        foreach my $log (@ramp_log_props)
        {
            my $buffer = get($device, $log . " bin");

            push @buffers, \$buffer;
        }

        # Save all requested logs

        for my $i (0..$#ramp_log_props)
        {
            my $results = compose(decode($buffers[$i], $ramp_log_props[$i]), '', $device, $ramp_log_menus[$i] . $path);

            my $filename = $path . "/" . $ramp_log_menus[$i] . "_ramp_from_" . $ramp->{start} . "G_to_" . $ramp->{final} . "G.json";

            print "Saving $filename\n";

            open my $filehandle, ">", $filename or die "Error opening file $filename: $!";
            print $filehandle $results;
            close $filehandle;
        }
    }
}

# EOF