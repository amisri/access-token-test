#!/usr/bin/perl

# File:    nvs.pl

use File::Slurp;

use Test::Utils;
use FGC::Properties;

use strict;
use warnings;
use diagnostics;



sub isDsp
{
    my ($d, $property) = @_;

    my $dsp_idx = getPropertyInfo($d, $property)->{"dsp_idx"};

    return ($dsp_idx != 0);
}



sub getDspPropStructAddress
{
    my ($d, $property, $base_addr) = @_;

    my $dsp_idx = getPropertyInfo($d, $property)->{"dsp_idx"};

    my $prop_struct_size = 7; # 7 long words

    return hex($base_addr) + $prop_struct_size * $dsp_idx;
}



sub getMcuPropAddress
{
    return 0;
}



sub printUsageMessage
{
    print "Empty\n";
}



sub printPropStruct
{
    my ($d, $address) = @_;

    set($d, "FGC.DEBUG.MEM.DSP", "$address");

    sleep(2);

    my $data = get($d, "FGC.DEBUG.MEM.DSP");

    $data =~ s/\s//g;
    $data =~ s/,/\n/g;

    print "float_f:      " . (split /\n/, $data)[0] . "\n";
    print "flags:        " . (split /\n/, $data)[1] . "\n";
    print "el_size:      " . (split /\n/, $data)[2] . "\n";
    print "n_elements:   " . (split /\n/, $data)[3] . "\n";
    print "max_elements: " . (split /\n/, $data)[4] . "\n";
    print "pars_idx:     " . (split /\n/, $data)[5] . "\n";
    print "value:        " . (split /\n/, $data)[6] . "\n";
}



my %params = (device=>undef, config=>'', protocol=>'', property=>undef, address=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};
my $p = $params{property};
my $a = $params{address};

if(!defined $d)
{
    printUsageMessage();

    die "Device not specified";
}

if(!defined $p)
{
    printUsageMessage();

    die "Property not specified";
}

if(!defined $a)
{
    printUsageMessage();

    die "Address not specified";
}

if(isDsp($d, $p))
{
    my $address = getDspPropStructAddress($d, $p, $a);

    printPropStruct($d, $address);

    $address += 6;

    set($d, "FGC.DEBUG.MEM.DSP", "$address");

    sleep(2);

    my $data = get($d, "FGC.DEBUG.MEM.DSP");

    $data =~ s/\s//g;
    $data =~ s/,/\n/g;

    my $value_addr = (split "\n", $data)[0];
       $value_addr = (split ":", $value_addr)[1];

    print $value_addr."\n";

    set($d, "FGC.DEBUG.MEM.DSP", "$value_addr");

    sleep(2);

    $data = get($d, "FGC.DEBUG.MEM.DSP");

    $data =~ s/\s//g;
    $data =~ s/,/\n/g;

    print $data."\n";
}
else
{

}

# EOF
