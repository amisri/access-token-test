#!/usr/bin/perl

use strict; 
use warnings; 
use Test::FGC qw (sendTest);

use constant MINIMUM_NUMBER_ARGS    => 5;


my @program_tests = qw(
                    SET_SLOT_NUMBER
                    ACCESS_SETTINGS               
                    SET_DATA                  
                    DISPLAY_WRITE_BLOCK       
                    SEND_BLOCK                  
                    DISPLAY_RECEIVED_HEADER      
                    DISPLAY_ALL_RECEIVED_DATA
                    );


my %test_data = ();

my @data = ();

my %test_func_map = (
    SET_SLOT_NUMBER             => \&test_no_iterations, 
    ACCESS_SETTINGS             => \&test_no_iterations,
    SET_DATA                    => \&test_set_data,         
    DISPLAY_WRITE_BLOCK         => \&test_no_iterations,    
    SEND_BLOCK                  => \&test_no_iterations,                
    DISPLAY_RECEIVED_HEADER     => \&test_no_iterations,      
    DISPLAY_ALL_RECEIVED_DATA   => \&test_no_iterations,
    );


sub usage
{
	print "\nUsage: $0 <header_file> <data_file> <output_file> <decimal_output_file> <hex_output_file>\n";
	exit;
}



sub process_response($$)
{
    my ($response, $test) = @_; 
    my $result;
    my @response_data;

    printf("%-10s", "response: ");

    if ($response->{status} ne "OKAY")
    {
      foreach (@{$response->{value}})       
        {
            if ($_ =~ m/\$4/)     # Lines of the FGC reply that contain error data start with $4              
            {
                s/.*\$4,\s*//;    # Delete everything before and inluding $4, leave only error message
                                              
                $result = sprintf("%-26s : %s", $test, $_);
                print "$result\n";                                                              
            }                   
        } 
    }
    else
    {
        print "OK\n";
        
        foreach (@{$response->{value}})
        {
            push(@response_data, $_) if ( (defined($_)) && ($_ !~ /^\$[013]/) );  
        }

        return \@response_data; 
    }

}


sub process_all_receive_data($$$)
{
    my ($response, $d_res, $h_res) = @_;  
    my ($dec_res, $hex_res) = "";

    for my $res_part (@$response)
    {
        $res_part =~ s/\$2//;

        if ($res_part =~ /^,0x/)
        {
            $hex_res .= $res_part;
        }
        else
        {
            $dec_res .= $res_part;
        }
    }

    print "hex: $hex_res \n";
    print "dec: $dec_res \n";

    open (my $dec_results_fh, '>', $d_res);
    my $dec_res_without_first_comma = substr($dec_res, 1);
    $dec_res_without_first_comma =~ s/,/\n/g;
    print $dec_results_fh $dec_res_without_first_comma; 
    close ($dec_results_fh);

    open (my $hex_results_fh, '>', $h_res);
    my $hex_res_without_first_comma = substr($hex_res, 1);
    $hex_res_without_first_comma =~ s/,/\n/g;
    print $hex_results_fh $hex_res_without_first_comma;
    close ($hex_results_fh);
}


sub test_no_iterations($$$)
{
    my ($dev, $test, $args) = @_;
    
    my $response = sendTest($dev, $test, $args);
    return process_response($response, $test);
}


sub test_set_data($$$)
{
    my ($dev, $test, $args) = @_;

    my ($response, $processed_response);  
    my @processed_responses; 
    my $index = 0;

    print "\n";
    foreach my $data (@$args)
    {
        printf("%-45s", "index -> $index, value -> $data");
        my $arg = $index.','.$data;
        $response = sendTest($dev, $test, $arg);
        $processed_response = process_response($response, $test);
        print join("\n", @$processed_response); 
        push @processed_responses, join("\n", @$processed_response);
        $index++;
    }

    return \@processed_responses; 
}



sub get_arguments($)
{
    my $test_name = shift; 
    my $arg; 

    if ($test_name ~~ @program_tests)
    {
        if ($test_name eq 'SET_SLOT_NUMBER')
        {
            return $test_data{SLOT};
        }

        if ($test_name eq 'ACCESS_SETTINGS')
        {
            return $test_data{WRITE_ADDRESS}.','.$test_data{WRITE_LENGTH}.','.$test_data{READ_ADDRESS}.','.$test_data{READ_LENGTH};
        }

        if ($test_name eq 'SET_DATA')
        {
            return \@data;
        }

        return "";
    }
}



sub read_files($$)
{
	my ($h_file, $d_file) = @_;
    my $line = ''; 
    open(my $fh_header, '<', $h_file) or die "Could not open ".$h_file."\n";

    while ($line = <$fh_header>)
    {
        chomp $line; 
        next if ($line =~ /^\s*#/); 
        my @line_split = split('=', $line);
        $test_data{$line_split[0]} = $line_split[1];
    }

    close($fh_header);

    $line = '';

    open(my $fh_data, '<', $d_file) or die "Could not open ".$d_file."\n";

    while ($line = <$fh_data>)
    {
        chomp $line; 
        next if ($line =~ /^\s*#/);
        my @data_split = split(',', $line);
        push @data, @data_split; 
    }

    close($fh_data);
}



sub run_test($$$)
{
    my ($dev, $test, $args) = @_;

    return $test_func_map{$test}->($dev, $test, $args);
}



sub run_tests($$$)
{
    my ($res_file, $d_results, $h_results) = @_; 
    my $arguments; 
    my $response; 
    my $result; 
    my $device = $test_data{DEVICE};

    open (my $results_test_fh, '>', $res_file) or die "Could not open results file $res_file!\n";

    printf $results_test_fh "%-25s",   "DEVICE";
    printf $results_test_fh "%-25s\n", $device;

    
    foreach my $test (@program_tests)
    {
        $arguments = get_arguments($test);

        printf $results_test_fh "%-25s", $test;
        printf $results_test_fh "%-20s", $arguments if (ref($arguments) eq "");

        $response = run_test($device, $test, $arguments);

        if ($test eq 'DISPLAY_ALL_RECEIVED_DATA')
        {
            process_all_receive_data($response, $d_results, $h_results);
        }

        print $results_test_fh join("\n", @$response);
        print $results_test_fh "\n";
    }

    close ($results_test_fh);
}

# End of functions

if (@ARGV < MINIMUM_NUMBER_ARGS)
{
	usage();
}

my ($header_file, $data_file, $results_file, $dec_results, $hex_results) = @ARGV;

read_files($header_file, $data_file);
run_tests($results_file, $dec_results, $hex_results);
