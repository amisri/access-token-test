#!/bin/sh

cpu=$(uname -m)
os=$(uname -s)

export PATH="/user/pclhc/bin/$os/$cpu:/user/pclhc/bin/$os:/user/pclhc/bin/perl:$PATH"
export PERL5LIB="/user/pclhc/lib/perl"

# Path of logfiles

logpath="/user/poccdev/dropbox/FGC3_reception_test/"
parentpath="/user/pclhc/bin/perl/"

if [ $# -ne 0 ]; then
   fgc3_reception_test.pl "$@"
else
   fgc3_reception_test.pl '' 'courier 9'
fi

printf "\nPress ENTER to exit...\n"
read -n 1 -s


# EOF
