#!/usr/bin/perl

# File:    fgc3_reception_test.pl
# Purpose: Given the device name as input, launch FGC3 Reception Test script.
#          Creates a logfile after the ccomponentte name in the /poccdev/dropbox or at logpath specified by user.
#          It appends the header of the logfile, where the results of the previous results are, with the result of the current test.
#          If FGC3 reception test fails, it writes the tests that failed and the error message they returned.
#          It is executed by FGCRun+ but it can also run directly on a terminal.If 72h have passed since last
#          test it updates the TE-EPC database with the result of the test.

use strict;
use warnings;

use Test::Most;
use Getopt::Long;
use FGC::DB;
use FGC::RBAC;
use File::Copy;
use TAP::Formatter::Console;
use Carp;
use Test::Boot  qw( sendCommand
                    sendCommandNoWait);
use Test::FGC   qw( assureBootNoSub
                    assureMain
                    assureMainNoSub
                    assureDSP
                    sendTest
                    syncFgc
                    isInBootNoSub
                    isInMainNoSub
                    sendPowercycle
                    sendGlobalReset
                    sendSlowWatchdog);
use Test::TCP   qw( get);
use DBI         qw(:sql_types);
use Time::HiRes qw( sleep time);


use constant DB_ID          => 'dbi:Oracle:cerndb1';    # ID of the database database
use constant DB_USERNAME    => 'fgc3reception_user';    # Public username for FGC3 reception test in TE-EPC database
use constant DB_PASSWORD    => ']0nF3r3c_s';            # Public password for FGC3 reception test


# Define variables

my $result = "FAIL";
my $repair_status = 1;               # DB: repair_status FAIL
my $problem = 'Reception Test';
my $solution = 'Test OK';
my $previous_test = 0;
my $ccomponentte;
my $logfile;
my $tests_failed = 0;

# Define variables

my @reset_tests  =  ( "GLOBAL_RESET", "TOGGLE_SLOW_WD_TRIG_ENABLE", "POWER_CYCLE");                      # All the reset tests available

my @dsp_tests    =  ( "DSP_SDRAM_ST", "ADCS", "DACS", "SPIVS_ST");                                       # All the dsp tests available

my @self_tests   =  ( "COMMANDS", "INTERLOCKS", "SUPPLEMENTAL_IO", "SRAM_ST", "PLD_DPRAM_ST", "MRAM_ST", # All the self tests available
                      "MCU_ANALOGUE","HEATER", "QSPI_ST", "JTAG_ST", "POWER_CONTROL", "SWITCH_PROGRAM");

my %reset_source =  ( "GLOBAL_RESET"               => "0x0002",   # SOFTWARE
                      "TOGGLE_SLOW_WD_TRIG_ENABLE" => "0x0020",   # SLOW WATCHDOG
);

my @tests_all;              # All tests that can be performed
my @tests;                  # Tests selected by user to be performed
my $response;               # Response of the FGC after the boot command is sent
my @results;                # Store all the responses

# Declare subroutines

sub showlist();
sub usage();
sub parse_args(%);
sub get_ccomponentte_name();
sub launch_reception_tests();
sub run_tests();
sub run_self_tests($);
sub run_DSP_tests($);
sub run_reset_tests($);
sub create_logfile();
sub write_to_DB();
sub text_to_BLOB();

# Get date and timestamp

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
my $datetime = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $year+1900, $mon+1, $mday, $hour, $min, $sec);
my $timestamp = sprintf("%10d",time());

# Get parameters

my %params = (device=>undef, logpath=>'/user/poccdev/dropbox/FGC3_reception_test/', parentpath=> '/user/pclhc/bin/perl/', user=>(getpwuid($<))[0], i=>undef);
%params = parse_args(%params);

# Authenticate via RBAC

if($params{authenticate} eq 'token'){
    if(defined($params{serialized_token})){
        $params{binary_token} = FGC::RBAC::deserialize_token($params{serialized_token});
        Test::TCP::import_token($params{binary_token});
    }
    else{
        $params{authenticate} = 'explicit';
    }
}

Test::TCP::authenticate($params{authenticate});


# Device must be defined

my $device = $params{device};
usage() if (! defined $device);

# If user is not specified, it is the username from the running process

my $user = $params{user};

# Get optional parameters

my $interactive = $params{i};

my $logpath = $params{logpath};
mkdir $logpath unless -d $logpath;

my $parentpath = $params{parentpath};


# Start FGC3 reception test

printf "\n%10d %s %s\n\n",$timestamp, $datetime, $user;

print "FGC3 reception test in progress...\n";

# Go to main to find the ccomponente name

print "\nGo to main...\n";
assureMainNoSub($device);

print "\nWait for state to be synchronised...\n";
syncFgc($device);

# Fail if not FGC3

my $platform  = get($device, "DEVICE.PLATFORM_NM");

if ($platform ne "FGC3")
{
    print("Device is not FGC3!\n\nPress ENTER to exit...");;
    <STDIN>;
    exit;
}

# Get ccomponentte name

$ccomponentte = get_ccomponentte_name();

# Run the tests

my $output = $logpath . '.' . $ccomponentte. '.txt';

# Remove the temporary file if it exists

if (-e $output)
{
    unlink $output or die "Failed to remove temporary file $output\n";
}

# Launch the tests

launch_reception_tests();

if ($tests_failed eq 0)
{
    $result =  "PASS";
    $repair_status = -1; # Reception OK for DB
}
else
{
     $result =  "FAIL";
     $repair_status = 1;
}

print "Reception test: $result\n\n";

# Create logfile for this device

if (!defined($interactive))
{
	$logfile = create_logfile();

	# Check that at least 72 hours have passed since the last test

	my $timespan =  72 * 60 * 60;
	my $time_passed = $timestamp - $previous_test;
	my $hours_passed = int($time_passed/3600);
	my $mins_passed = int(($time_passed -  $hours_passed *3600)/60);

	if ($previous_test != 0 && ($result ne "STOP"))
	{
		# Always write to DB if all tests performed and 72 hours have passed

	    if ($time_passed >= $timespan)
	    {
	          write_to_DB();
	    }

	    # Ask user to write in the database if 72 hours have not passed

	    else
	    {
	        printf ("\nOnly %d hours %d mins passed since last test.\nWrite to TE-EPC Database? (y/n) ", $hours_passed, $mins_passed);
	        my $answer = <STDIN>;
	        print "\n";

	        if ($answer =~ m/(y|Y|yes)/)
	        {
	             write_to_DB();

	        }
	    }
	}
}


#-----------------------------------------------       Functions      --------------------------------------------------------------------------#

sub run_tests()
{
#    my $device = shift;
#    my @tests = @{$_[0]};  # Tests chosen by user

    my @my_reset_tests;    # Reset tests chosen by user
    my @my_dsp_tests;      # DSP tests chosen by user
    my @my_self_tests;     # Self tests chosen by user
    my %reset_tests_hash = map { $_ => 1 } @reset_tests;
    my %dsp_tests_hash = map { $_ => 1 } @dsp_tests;
    my $test;

    # For every test selected by user, separate reset tests and DSP tests;

    foreach (@tests)
    {
        $test = $_;

        if (exists($reset_tests_hash{$test}))
        {
            push(@my_reset_tests,$test);
        }
        elsif (exists($dsp_tests_hash{$test}))
        {
            push(@my_dsp_tests,$test);
        }
        else
        {
            push(@my_self_tests,$test);
        }
    }

    # Make sure the device stays in Boot Program after a reset

    sendTest($device, "STAY_IN_BOOT_AFTER_RESET", 1);

    # Perform the the self tests

    run_self_tests(\@my_self_tests);

    # Perfom all the DSP tests

    run_DSP_tests(\@my_dsp_tests);

    # Perform all the reset tests

    run_reset_tests(\@my_reset_tests);
}

sub run_self_tests($)
{
    my @my_self_tests = @{$_[0]};  # Tests chosen by user
    my $test;
    my $result;

    foreach (@my_self_tests)
    {
        $test = $_;

        if ($test eq "POWER_CONTROL")
        {
            # First turn off the M16C62. Ignore the response the first response because if already powered off, it will return an error

            $response = sendTest($device, "POWER_CONTROL", 0);

            # Now turn on the M16C62

            $response = sendTest($device, "POWER_CONTROL", 1);
        }
        elsif ($test eq "SWITCH_PROGRAM")
        {
            # Make sure that M16C62 is powered

            sendTest($device, "POWER_CONTROL", 1); # Power control needs argument 1 to turn on the M16C62

            $response = sendTest($device, $test);
        }
        else
        {
            $response = sendTest($device, $test);
        }

        # Parse response

        if ($response->{status} ne "OKAY" )
        {
            foreach (@{$response->{value}})
            {
                if ($_ =~ m/\$4/)     # Lines of the FGC reply that contain error data start with $4
                {
                    s/.*\$4,\s*//;    # Delete everything before and inluding $4, leave only error message

                    $result = sprintf("%-26s : %s", $test, $_);
                    push(@results, $result);
                }
            }

            $tests_failed++;
        }
        ok($response->{status} eq "OKAY", $test);
    }
}

sub run_DSP_tests($)
{
    my @my_dsp_tests = @{$_[0]};  # Tests chosen by user
    my $test;
    my $result;

    foreach (@my_dsp_tests)
    {
        $test = $_;

        # Make sure DSP is responding before the test

        assureDSP($device);

        $response = sendTest($device, $test);

        # Parse error

        if ($response->{status} ne "OKAY" )
        {
            foreach (@{$response->{value}})
            {
                if ($_ =~ m/\$4/)     # Lines of the FGC reply that contain error data start with $4
                {
                    s/.*\$4,\s*//;    # Delete everything before and inluding $4, leave only error message
                    $result = sprintf("%-26s : %s", $test, $_);
                    push(@results, $result);
                }
            }

            $tests_failed++;
        }

        ok($response->{status} eq "OKAY", $test);
    }
}

sub run_reset_tests($)
{
    my @my_reset_tests = @{$_[0]};  # Tests chosen by user
    my $test;
    my $result;
    my $powercycle = 0;

    foreach (@my_reset_tests)
    {
        $test = $_;

        if ($test eq "POWER_CYCLE")
        {
            # Perform powercycle last in order to test going to main

            $powercycle = 1;
            next;
        }
        if ($test eq "TOGGLE_SLOW_WD_TRIG_ENABLE")
        {
            sendSlowWatchdog($device);
        }
        elsif ($test eq "GLOBAL_RESET")
        {
            sendGlobalReset($device);
        }

       # Check the the source of the reset is the corresponing reset test

        $response =  sendTest($device, "SHOW_RESET_SOURCES");

        foreach (@{$response->{value}})
        {
           if ($_ =~ m/\$2,0x/)  # Lines of the FGC reply that contain response data start with $2
           {
               ok($_ =~ $reset_source{$test}, $test);
               $result = sprintf("%-26s : %s", $test, $_);

               if($_ !~ $reset_source{$test})
               {
                    $tests_failed++;
                    push(@results, $result);
               }

               last;
           }
        }
    }

    # Power cycle must always be the last test as it goes to main

    if($powercycle)
    {
        my $inMain = sendPowercycle($device);

        if (!$inMain)
        {
             $result = sprintf("%-26s : %s", "POWER_CYCLE", "Timeout exceeded waiting to go to Main");
             push(@results, $result);
             $tests_failed++;
        }
        ok($inMain == 1, "POWER_CYCLE");
    }
}

# Parse command line arguments. Based on Test::Utils::parse_opts.

sub parse_args(%)
{
    my %default_opts = @_;

    $default_opts{authenticate}     ||= 'explicit';     # Default authentication is by username and password
    $default_opts{protocol}         ||= 'tcp';          # Protocol is TCP by default
    $default_opts{serialized_token} ||= undef;          # By default, no RBAC token is provided

    my %opts = %default_opts;                                             # Options are based on default values
    my %get_options_args = map( { $_.':s', \$opts{$_} } keys %opts );     # Prepare list of command-line options for GetOptions

    $get_options_args{help} = \$opts{help};         # Add --help option
    GetOptions( %get_options_args ) or usage();   # Parse command line options

    usage() if($opts{help});     # Print usage message if --help option was specified

    %opts;
}


# Show list of availble tests. Function is called when --showlist option is provided or if scipt is called used in command line

sub showlist()
{
    my $count = 1;

    print "\nList of tests available:\n\n";

    foreach(@tests_all)
    {
        printf "%-2d %s\n", $count, $_;
        $count++;
    }

    print "\n";

}

# Display usage and options when wrong arguments are provided or with --help option

sub usage()
{
    print "\nFGC3 Reception test script \n\n";
    print "Usage: $0 --device=<device name> [options]\n\n";

    print "Options:\n\n";
    print "--i :\n  Interactive mode for command line interface.\n  Shows lists of tests, then user selects which ones to run or skip.\n\n";
    print "--logpath=<path> :\n  Path to location where the logfile will be created.\n  By default is poccdev/dropbox/FGC3_reception_test/\n  Logfile is not created if interactive mode is selected\n\n";
    print "--help :\n  This help message.\n";

    exit(-1);
}


sub launch_reception_tests()
{
	my $test;
	my $skip;
	my $input;

    # Keep all the tests that can be performed in one table in order to parse the user selected tests

    push(@tests_all, @self_tests);
    push(@tests_all, @dsp_tests);
    push(@tests_all, @reset_tests);

    # Show list of available tests and exit

    if (defined ($interactive))
    {
    	 showlist();
    	 print "Use -test option to choose the tests to run or -skip to choose the tests to skip, following by the numbers of the test.\n\n";
    	 $input = <STDIN>;

    	 if ($input  =~ m/-test/)
    	 {
    	 	$test =  substr($input, 5);
    	 }

    	 if ($input  =~ m/-skip/)
    	 {
    	 	$skip =  substr($input, 5);
    	 }

    	 if ((defined($test)) && (defined($skip)))
		 {
		    print "\nCannot use both -test and -skip option at the same time!\n";
		    exit;
		 }
    }


    # Open logfile if specified

    if (defined $output)
    {
        open(LOGFILE, '>', $output) or die("Can't open file $output!: $!");
    }

    # Make sure device is in Boot in order to run the reception test

    print "\nGo to boot...\n";
    assureBootNoSub($device);

    sleep(3);

    print "\nStart tests...\n\n";

    # Redirect TAP's detailed failure output, as there is no use of showing the line that failed on the terminal.
    # All the diagnostics for the errors are parsed and shown in the logfile.

    my $Test = Test::Builder->new;
    open $Test->failure_output, '>/dev/null' or warn "Can't open /dev/null: $!";

    # Select tests to run

    if(defined($test))
    {
        my @test_choices = split(/ /, $test);   # Numbers of  the tests selected to be included
        my $powercycle = 0;

        foreach(@test_choices)
        {
            next if ($_ eq '' || $_ eq "0");    # Do not take zero or whitespace as a test number
            push(@tests, $tests_all[$_-1]);     # Push in selected tests  the name of the test
        }
        run_tests();
    }
    elsif (defined($skip))
    {
        my @skip_choices = split(/ /, $skip);   # Numbers of the tests selected to be skipped

        my %skip_hash = map { $_ => 1 } @skip_choices;

        my $count = 1;                          # Start searching for test number 1 (Commands)

        foreach(@tests_all)
        {
        	next if ($_ eq '' || $_ eq "0");   # Do not take zero or whitespace as a test number

            if(!exists($skip_hash{$count}))    # If the number of the test does not exists in the skip list
            {
                push(@tests, $_);              # Push in selected tests the name of the test
            }
            $count++;
        }
        run_tests();
    }
    else # All tests
    {
    	push(@tests, @tests_all);
        run_tests();
    }

    # Inform TAP that tests finished running

    done_testing();

    # Write out the results

    if(defined $output)
    {
        print LOGFILE join("\n",@results),"\n";
        close (LOGFILE);
    }

    # Display results also in standard output

    print("\n");
    print join("\n",@results),"\n";
}


sub get_ccomponentte_name()
{
    my $ccomponentte_name;

    print "\nGet ccomponentte name... ";
    $ccomponentte_name = get($device,'BARCODE.FGC.CASSETTE');

    if(!defined($ccomponentte_name) or $ccomponentte_name eq "")
    {
        print("Could not retrieve ccomponentte!\n\nPress ENTER to exit...");
        <STDIN>;
        exit;
    }

    $ccomponentte_name =~ s/[,]//g;

    print "$ccomponentte_name\n";

    if (!($ccomponentte_name =~ m/^HC/))
    {
        print("Ccomponentte name not valid!\n\nPress ENTER to exit...");
        <STDIN>;
        exit;
    }

    return $ccomponentte_name;
}

sub create_logfile()
{
    my $logfile = $logpath . $ccomponentte . '.txt';
    my $logfile_tmp = $logpath . '.' . $ccomponentte  .'_tmp'.'.txt';
    my $first = 1;  # Flag showing that it is the first line of the logfile

    # If logfile exists, copy header without the closing line in order to append the latest test

    if (-e $logfile)
    {
        # Read from logfile

        open( LOGFILE, "<$logfile" ) or die("Can't read file $logfile : $!");
        my @LINES = <LOGFILE>;
        close( LOGFILE );

        # Write to temp logfile

        open( LOGFILE, ">$logfile_tmp" );

         # Parse the logfile with by two lines

        my $line;
        my $previous_line;

        foreach $line (@LINES)
        {
            if ($first)
            {
                print LOGFILE $line; # Copy the opening line of the header
                $first = 0;
                $previous_line = $line; # Initialise the previous line
                next;
            }

            if ($line =~ m/-----/) # Closing line of the header found
            {
                $previous_test =  substr($previous_line, 20, 11); # Last test is on the previous line
                last;
            }
            print LOGFILE $line;
            $previous_line = $line;
        }
    }
    else # If logfile does not exist, create one with the opening line of the header
    {
        open( LOGFILE, '>', $logfile) or die("Can't create file $logfile : $!");
        print LOGFILE ('-' x 120);
        print LOGFILE "\n";
    }

    # Get the results of the latest test

	my @tests;
	my @LINES;

	if ($result eq "FAIL")
	{
	    # If latest test did not pass, get the output with the results of the failed tests it created

	    open( RESULTS, "<$output" ) or die("$0: Can't open $output : $!");
	    @LINES = <RESULTS>;
	    @tests = @LINES;
	    close(RESULTS);

	    # If no tests were executed, test did not finish
	    if (@tests == 0 )
	    {
	        $result = "STOP";
	    }
	}

	unlink "$output";


    # Append to the header of the logfile the info about the latest test, if it was completed

	if ($result ne "STOP")
	{
		printf LOGFILE "%s, %10d, %s, %s, %6s\t",$ccomponentte, $timestamp, $device, $datetime, $result;
	}


	if ($result eq "FAIL")
	{
	    $problem .= ": ";       # DB: Start list of problems
	    $solution = '';         # DB: Leave the solution field empty

	    # Parse output and get the name of the tests

	    foreach my $test (@tests)
	    {
	        if ($test eq $tests[-1])    # Last element of array
	        {
	            $test =~ s/\ .*//;      # Remove error messages
	        }
	        else
	        {
	            $test =~ s/\ .*/, /;    # Remove error messages and put comma
	        }
	        $test =~ s/\R//g;           # Remove new lines

	        print LOGFILE "$test";

	        $problem .= "$test";        # DB: Append to list of problems
	    }
	}

	print LOGFILE "\n";

	# Close header again

	print LOGFILE ('-' x 120);
	print LOGFILE "\n";


	# Add output

	foreach my $line (@LINES)
	{
	   print LOGFILE "$line";
	}

	if (-e $logfile_tmp)
	{
	    move("$logfile_tmp", "$logfile") or die "Move failed: $!";
	}

	close (LOGFILE);

	# Ignore TAP's detailed failure output, as there is no use of showing the line that failed on the terminal.
	# All the diagnostics for the errors are parsed and shown in the logfile in dropbox.

	my $Test = Test::Builder->new;
	open ( $Test->failure_output, '>/dev/null') or warn "Can't open /dev/null: $!";

	return $logfile;

}


sub text_to_BLOB()
{
    my $blob_buffer;

    open( BLOB, "<", "$logfile") or die("Can't read file $logfile : $!");
    binmode BLOB;          # Set it to binary mode

    # TODO Read properly in smaller chunks
    my $size = -s $logfile;
    my $bytes_read = 0;
    my $blob_data;

    while($bytes_read < $size)
    {
        $bytes_read += read( BLOB, $blob_buffer, 1024);
        $blob_data .= $blob_buffer;
    }

    close BLOB;

    return $blob_data;
}

sub write_to_DB()
{
    # Connect to database

    my $dbh = DBI->connect(DB_ID, DB_USERNAME, DB_PASSWORD, {AutoCommit => 1, PrintError => 0, RaiseError => 0}) or die $DBI::errstr;

    # Write to DB

    # Get fields for database

    my $prefix          = substr($ccomponentte, 0, 2);
    my $equipment_code  = substr($ccomponentte, 2, 5);
    my $version         = substr($ccomponentte, 7, 3);
    my $production_code = substr($ccomponentte, 11, 2);
    my $series_number   = int(substr($ccomponentte, 13, 6));
    my $date = sprintf("%02d/%02d/%04d", $mday, $mon+1, $year+1900);
    my $validation = '';
    my $repair_duration = 0;
    my $return_location = 0;
    my $filename = $ccomponentte;
    my $filetype = "txt";

    # Prepare BLOB data from logfile

    my $blob = text_to_BLOB();

    # Prepare query

    my $sql= "
        BEGIN
          Alim.Repair_CreateRepair(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
        END;";

    my $query = $dbh->prepare($sql) or die $DBI::errstr;

    my $rc;
    $rc = $query->bind_param(1, $prefix, SQL_VARCHAR);
    $rc = $query->bind_param(2, $equipment_code, SQL_VARCHAR);
    $rc = $query->bind_param(3, $version, SQL_VARCHAR);
    $rc = $query->bind_param(4, $production_code, SQL_VARCHAR);
    $rc = $query->bind_param(5, $user, SQL_VARCHAR);
    $rc = $query->bind_param(6, $series_number);
    $rc = $query->bind_param(7, $date, SQL_VARCHAR);
    $rc = $query->bind_param(8, $problem, SQL_VARCHAR);
    $rc = $query->bind_param(9, $solution, SQL_VARCHAR);
    $rc = $query->bind_param(10,$validation, SQL_VARCHAR);
    $rc = $query->bind_param(11,$repair_status);
    $rc = $query->bind_param(12,$repair_duration);
    $rc = $query->bind_param(13,$return_location);
    $rc = $query->bind_param(14,$filename, SQL_VARCHAR);
    $rc = $query->bind_param(15,$filetype,SQL_VARCHAR);
    $rc = $query->bind_param(16,$blob, SQL_BLOB);


    $query->execute();

    if($DBI::errstr )
    {
    	print $DBI::errstr;
        print "\n\nDatabase not updated!\n";
    }
    else
    {
        print "\nDatabase updated successfully!\n";
    }

    $dbh->disconnect;
}

# EOF


