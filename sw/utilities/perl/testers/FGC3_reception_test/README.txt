
================================== GENERAL =====================================

The FGC3 reception test is developed in Perl v5.10.1 on Scientific Linux CERN
SLC release 6.8 

It performs a number of diagnostics while the device is in the Boot Program.
These are self tests regarding the digital interface, the memories, the analogue
interface, the DSP, manual tests of the M16C62 and tests checking the device
resets.

The list of tests available is:

1  COMMANDS
2  INTERLOCKS
3  SUPPLEMENTAL_IO
4  SRAM_ST
5  PLD_DPRAM_ST
6  MRAM_ST
7  MCU_ANALOGUE
8  HEATER
9  QSPI_ST
10 JTAG_ST
11 POWER_CONTROL
12 SWITCH_PROGRAM
13 DSP_SDRAM
14 ADCS
15 DACS
16 SPIVS_ST
17 GLOBAL_RESET
18 TOGGLE_SLOW_WD_TRIG_ENABLE
19 POWER_CYCLE

================================== USAGE =======================================

The FGC3 reception test consits of the fgc3_recepetion_test perl and shell scripts.

It can be used either on a terminal for one device or launched via
FGCRun+ for all the devices selected. The test is available under the FGC menu.
The user can select multiple devices and then press the 'Run FGC3 reception test'.

This will start a remote terminal for each device, where the output of the
launch_reception_test will be diplayed. Terminals close by pressing ENTER.

Devices should be already reset in order to avoid timeouts due to them being
reprogrammed.

In the command line interface, it can be run in interactive mode 
using the --i option. This shows the list of all available tests and
user then selects the numbers of the test to run or skip using the
-test or -skip arguments.



Usage: /user/pclhc/bin/perl/fgc3_reception_test.pl --device=<device name> [options]

Options:

--i :
  Interactive mode for command line interface.
  Shows lists of tests, then user selects which ones to run or skip.

--logpath=<path> :
  Path to location where the logfile will be created.
  By default is poccdev/dropbox/FGC3_reception_test/
  Logfile is not created if interactive mode is selected

--help :
  This help message.


================================== LOGFILES =====================================


The results of the test are saved in a logfile, named after the barcode cassette
at /poccdev/dropbox/FGC3_reception_test/. The path to the location where the
logfile will be stored can also be specified by the user using the --logpath option.

If 72 hours have passed since the last test for the same device, the TE-EPC
database is updated with the results of the test. Also, the logfile showing
the history of the previous executions is attached in the field in the database.
If the last test was less than 72 hours ago, there is a prompt appearing
informing the user.

In interactive mode the results of the test are not saved.

    

============================== OUTPUT EXAMPLE ==================================

----> Terminal in interactive mode :


$ ./fgc3_reception_test.sh --device="RFNA.866.06.ETH3" --i

1492702766 2017-04-20 17:39:26 lzacharo

FGC3 reception test in progress...

Go to main...

Wait for state to be synchronised...

Get cassette name... HCRFNAE002-LE000196

List of tests available:

1  COMMANDS
2  INTERLOCKS
3  SUPPLEMENTAL_IO
4  SRAM_ST
5  PLD_DPRAM_ST
6  MRAM_ST
7  MCU_ANALOGUE
8  HEATER
9  QSPI_ST
10 JTAG_ST
11 POWER_CONTROL
12 SWITCH_PROGRAM
13 DSP_SDRAM
14 ADCS
15 DACS
16 SPIVS_ST
17 GLOBAL_RESET
18 TOGGLE_SLOW_WD_TRIG_ENABLE
19 POWER_CYCLE

Use -test option to choose the tests to run or -skip to choose the tests to skip, following by the numbers of the test.

-test 1 14 3 19

Go to boot...

Start tests...

ok 1 - COMMANDS
ok 2 - SUPPLEMENTAL_IO
ok 3 - ADCS
ok 4 - POWER_CYCLE
1..4

Reception test: PASS

Press ENTER to exit...



----> Terminal or FGCRun+ remote terminal :



$ ./fgc3_reception_test.sh --device="RFNA.866.06.ETH3"

1492703093 2017-04-20 17:44:53 lzacharo

FGC3 reception test in progress...

Go to main...

Wait for state to be synchronised...

Get cassette name... HCRFNAE002-LE000196

Go to boot...

Start tests...

ok 1 - COMMANDS
ok 2 - INTERLOCKS
ok 3 - SUPPLEMENTAL_IO
ok 4 - SRAM_ST
ok 5 - PLD_DPRAM_ST
ok 6 - MRAM_ST
ok 7 - MCU_ANALOGUE
ok 8 - HEATER
ok 9 - QSPI_ST
ok 10 - JTAG_ST
ok 11 - POWER_CONTROL
ok 12 - SWITCH_PROGRAM
ok 13 - DSP_SDRAM
ok 14 - ADCS
ok 15 - DACS
ok 16 - SPIVS_ST
ok 17 - GLOBAL_RESET
ok 18 - TOGGLE_SLOW_WD_TRIG_ENABLE
not ok 19 - POWER_CYCLE
1..19

POWER_CYCLE                : Timeout exceeded waiting to go to Main
Reception test: FAIL


Database updated successfully!

Press ENTER to exit...













