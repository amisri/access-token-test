#!/usr/bin/perl -w
#
# Name:     bin2codecmd.pl
# Author:   Stephen Page
# Purpose:  Make a command file from a code file

use File::Basename;
use strict;

# Handle arguments

die "\nUsage: ", basename($0), "<code slot> <input file>\n\n" if(@ARGV != 2);

my ($slot, $in_file) = @ARGV;

# Read contents of input file into $data

open(IN_FILE, "<", $in_file)
    or die "Unable to open input file $in_file :$!\n";
binmode(IN_FILE);

my $buf;
my $data;
while(read(IN_FILE, $buf, 16384))
{
    $data .= $buf;
}
close(IN_FILE);

# Extract bytes from data

my @bytes = split("", $data);
map { $_ = unpack("C", $_) } @bytes;

# Construct commands

my $i = 0;
while($i < @bytes)
{
    my $start_i = $i;

    my $command = "! s :code.slot".$slot."[$i,] ";

    my $first = 1;
    for( ; $i < @bytes && $i < ($start_i + 1024) ; $i++)
    {
        $command .= "," if(!$first);
        $command .= sprintf("0x%02X", $bytes[$i]);
        $first = 0;
    }

    print $command, "\n";
}

# EOF
