#!/usr/bin/perl -w
#
# Name:     tagcode.pl
# Authors:  Stephen Page, Krystian Wojtas
# Purpose:  Tags a code file with version information and checksum

use Digest::CRC qw(crc16);
use File::Basename;
use File::Path qw(mkpath rmtree);
use File::Slurp;
use strict;

use constant BLOCK_SIZE => 32; # Size of blocks for use in the serial code file
use constant TAG_SIZE   => 30; # Total size of tags in bytes

# Parameters from CLI

my $code_name;
my $version;
my $output_size;
my $class_id;
my $code_id;

# Some output paths. They would be parsed from cli parameters

my $fieldbus_dir;
my $serial_dir;
my $code_file;
my $log_file;

# Array stores pairs of paths to binaries linked with theirs offsets in output file

my @binpaths_offsets;

# $data variable will contain all binary data from all input files
# Empty spaces will be filled with 0xFF
# $serial_data contains the same data but divided into BLOCK_SIZE-byte blocks

my $data = '';
my $serial_data = '';

# Read cli parameters and build global arrays with them

sub parse_args()
{
    my $out_dir;

    # Handle arguments

    if(@ARGV < 8)
    {
        die "\nUsage: ", basename($0), " <output dir> <code name> <unixtime version> <output size> <class ID> <code ID> <input file 1> <offset 1> [<input file 2> <offset 2> [ ... ] ]\n\n";
    }
    ($out_dir, $code_name, $version, $output_size, $class_id, $code_id, @binpaths_offsets) = @ARGV;

    # Check if version is unixtime

    if($version =~ /\D/)
    {
        die "<unixtime version> parameter should contain only digits\n";
    }

    # Reduce $output_size to allow for code information

    $output_size -= TAG_SIZE;

    if($output_size <= 0)
    {
        die "Output size is too small to contain data and tags\n";
    }

    # Validate if <class ID> and <code ID> are in byte range

    if($class_id < 0 || $class_id > 255)
    {
        die "Tag <class ID> $class_id must be in byte range 0..255\n";
    }

    if($code_id < 0 || $code_id > 255)
    {
        die "Tag <code ID> $code_id must be in byte range 0..255\n";
    }

    # Check if number of cli arguments is even as there are passed pairs of binaries and their offsets

    if(@binpaths_offsets % 2)
    {
        die "It should be passed pairs of binaries and mandatory their offsets. One pair is not complete";
    }

    # Check if passed offsets are numbers

    for(my $i=0; $i < @binpaths_offsets; $i += 2)
    {
        my ($binpath, $offset) = ($binpaths_offsets[$i], $binpaths_offsets[$i+1]);

        if($offset =~ /\D/)
        {
            die "Passed offsets must be decimal numbers. Passed pair contains path to binary as '$binpath' and its offset '$offset'";
        }
    }

    # Build output paths

    $fieldbus_dir = "$out_dir/fieldbus/$version";
    $serial_dir   = "$out_dir/serial/$version";

    # Build output code file name

    $code_file =
        'C'.
        sprintf("%03d", ${code_id}).
        '_'.
        sprintf('%02d', ${class_id}).
        '_'.
        $code_name;

    # Build log file name

    $log_file = "$out_dir/tagcode_log_$code_name.txt";
}

# Delete directories for tagcoding version if they exist

sub rm_dirs()
{
    rmtree $fieldbus_dir or die "Failed to remove dir $fieldbus_dir :$!\n" if ( $fieldbus_dir && -d $fieldbus_dir);
    rmtree $serial_dir   or die "Failed to remove dir $serial_dir :$!\n"   if ( $serial_dir   && -d $serial_dir  );
}

# Create dirs if they don't exist

sub prepare_dirs()
{
    mkpath $fieldbus_dir or die "Failed to create dir $fieldbus_dir :$!\n" unless ( -d $fieldbus_dir);
    mkpath $serial_dir   or die "Failed to create dir $serial_dir :$!\n"   unless ( -d $serial_dir);
}

# Read data from input files

sub read_data()
{
    for (my $i=0; $i < @binpaths_offsets; $i += 2)
    {
        my ($binpath, $offset) = ($binpaths_offsets[$i], $binpaths_offsets[$i+1]);

        # Calculate how much empty space must be between binaries

        my $pad_size = $offset - length $data;

        # Check that offset does not overlap with previous file's data

        if($pad_size < 0)
        {
            die "Offset '$offset' for file '$binpath' is lower than so far created output which is already ".(length $data)." length\n";
        }

        # Pad data if necessary

        $data .= pack('C*', (0xFF) x $pad_size);

        # Read contents of input file

        $data .= read_file($binpath, binmode => ':raw');

        # Check whether data is under $output_size size long

        if(length($data) >= $output_size)
        {
            die "Specified output size '$output_size' already reached after file '$binpath'\n";
        }
    }

    # Pad end of data if necessary

    $data .= pack('C*', (0xFF) x ($output_size - length($data)));
}

# Tag code with additional information

sub add_tags()
{
    # Construct a string representation of the time used for the version

    my %localtime;
    (
        $localtime{sec},
        $localtime{min},
        $localtime{hour},
        $localtime{month_day},
        $localtime{month},
        $localtime{year},
        $localtime{week_day},
        $localtime{year_day},
        $localtime{is_dst},
    ) = localtime($version);

    my $time_string = sprintf("%02d/%02d/%04d %02d:%02d:%02d",
                               $localtime{month_day},
                               $localtime{month} + 1,
                               $localtime{year} + 1900,
                               $localtime{hour},
                               $localtime{min},
                               $localtime{sec});

    # Append code information to data

    $data .= pack("Z20",    $time_string)
          .  pack("N",      $version)
          .  pack("C",      $class_id)
          .  pack("C",      $code_id)
          .  pack("n",      0x0000);

    # Perform checksum on data

    my $crc = Digest::CRC->new(
                                init    => 0x0000,
                                poly    => 0x1021,
                                refin   => 1,
                                refout  => 1,
                                xorout  => 0x0000,
                                width   => 16,
                              )->add($data)->digest;
    $data .= pack("n", $crc);

    return $crc;
}

# Split $data into BLOCK_SIZE-byte blocks with additional headers information

sub build_serial_data()
{
    # Get array of data bytes

    my @data_bytes = split("", $data);

    # Construct array of data blocks

    my @data_blocks;
    for(my $i = 0 ; $i < @data_bytes ; $i++)
    {
        $data_blocks[$i / BLOCK_SIZE] .= $data_bytes[$i];
    }

    # Construct serial data

    for(my $i = 0 ; $i < @data_blocks ; $i++)
    {
        # Skip block if every byte in this block is 0xFF

        next if($data_blocks[$i] eq pack("C", 0xFF) x BLOCK_SIZE);

        $serial_data .= pack("C", $class_id)
                     .  pack("C", $code_id)
                     .  pack("n", $i);

        # Append block data

        $serial_data .= $data_blocks[$i];
    }
}

# Write the output files

sub write_outputs()
{
    # Write prepared data to fieldbus and serial output files

    write_file("$fieldbus_dir/$code_file", { binmode => ':raw'}, $data       );
    write_file("$serial_dir/$code_file"  , { binmode => ':raw'}, $serial_data);
}

# Append log of current build to logfile

sub log_txt($)
{
    my ($crc) = @_;

    # Write history file

    append_file($log_file, sprintf("Class:%s - $code_name:$version - %s - CRC:%04hX\n",
                                   $class_id, scalar(localtime($version)), $crc));
}

# End of functions

# Evaluate functions and catch exception if occurs

eval
{
    # Parse command line arguments

    parse_args();

    # Create output dirs if they don't exist

    prepare_dirs();

    # Read input files into $data

    read_data();

    # Add tags to code and calculate checksum

    my $crc = add_tags();

    # Create $serial_data

    build_serial_data();

    # Write the output files

    write_outputs();

    # Append a log to logfile

    log_txt($crc);

    # Show the final results on stdout

    printf("CLASS_ID=$class_id CODE_ID=$code_id VERSION=$version CRC=0x%04X SIZE=%d\n",
           $crc, length($data));
};
if($@)
{
    # Clean up created dirs

    rm_dirs();

    # Then die

    die "Error: $@\n";
}

# EOF
