#!/usr/bin/perl -w
#
# Name:     fgctransconfig.pl
# Purpose:  Translate version 1 FGC configurations to version 2
# Author:   Stephen Page
# Notes:    Order of processing is: deletions -> x4s -> renames -> merge (new and v1 into v2) -> special handling

use strict;

# Properties to be deleted

my @deletions = (
                    "VS.LIMITS.GAIN_ERR",
                );

# New properties with values

my %new =       (
                    "DEVICE.PPM"                => ["DISABLED"],
                    "LIMITS.I.RMS"              => ["0.0"],
                    "LIMITS.V.ERR_FAULT"        => ["0.0"],
                    "LOAD.OHMS_MAG"             => ["0.0,0.0,0.0,0.0"],
                    "LOAD.HENRYS_SAT"           => ["0.0,0.0,0.0,0.0"],
                    "LOAD.I_SAT_START"          => ["0.0,0.0,0.0,0.0"],
                    "LOAD.I_SAT_END"            => ["0.0,0.0,0.0,0.0"],
                    "MODE.RT"                   => ["DISABLED"],
                    "REF.START.VREF"            => ["0.0"],
                    "REF.START.TIME"            => ["0.1"],
                    "REF.START.DURATION"        => ["0.1"],
                    "REG.I.PURE_DELAY"          => ["0.0,0.0,0.0,0.0"],
                    "REG.I.MANUAL.TRACK_DELAY"  => ["1.0E-3"],
                    "REG.V.TRACK_DELAY"         => ["1.0E-3"],
                    "VS.DAC_CLAMP_PCT"          => ["-40.0"],
                );

# Name changes - old to new

my %renames =   (
                    "ADC.TYPE"                      => "ADC.EXTERNAL",
                    "BARCODE.FGC2.CASSETTE"         => "BARCODE.FGC.CASSETTE",
                    "CAL.A.ADC16.GAIN"              => "CAL.A.ADC.INTERNAL.GAIN",
                    "CAL.A.ADC16.TC"                => "CAL.A.ADC.INTERNAL.TC",
                    "CAL.A.ADC16.DTC"               => "CAL.A.ADC.INTERNAL.DTC",
                    "CAL.A.ADC16.ONE_MHZ"           => "CAL.A.ADC.INTERNAL.ONE_MHZ",
                    "CAL.A.ADC22.GAIN"              => "CAL.A.ADC.EXTERNAL.GAIN",
                    "CAL.A.ADC22.ERR"               => "CAL.A.ADC.EXTERNAL.ERR",
                    "CAL.B.ADC16.GAIN"              => "CAL.B.ADC.INTERNAL.GAIN",
                    "CAL.B.ADC16.TC"                => "CAL.B.ADC.INTERNAL.TC",
                    "CAL.B.ADC16.DTC"               => "CAL.B.ADC.INTERNAL.DTC",
                    "CAL.B.ADC16.ONE_MHZ"           => "CAL.B.ADC.INTERNAL.ONE_MHZ",
                    "CAL.B.ADC22.GAIN"              => "CAL.B.ADC.EXTERNAL.GAIN",
                    "CAL.B.ADC22.ERR"               => "CAL.B.ADC.EXTERNAL.ERR",
                    "ILOOP.PERIOD"                  => "REG.I.PERIOD_DIV",
                    "ILOOP.CLBW"                    => "REG.I.CLBW",
                    "ILOOP.CLBW2"                   => "REG.I.CLBW2",
                    "ILOOP.Z"                       => "REG.I.Z",
                    "ILOOP.LIMITS.RMS_ERR_WARN"     => "LIMITS.I.ERR_WARNING",
                    "ILOOP.LIMITS.RMS_ERR_FAULT"    => "LIMITS.I.ERR_FAULT",
                    "ILOOP.MANUAL.R"                => "REG.I.MANUAL.R",
                    "ILOOP.MANUAL.S"                => "REG.I.MANUAL.S",
                    "ILOOP.MANUAL.T"                => "REG.I.MANUAL.T",
                    "LOAD.I_MIN"                    => "LIMITS.I.MIN",
                    "LOAD.I_CLOSELOOP"              => "LIMITS.I.CLOSELOOP",
                    "LOAD.LIMITS.I_ACCESS"          => "LIMITS.I.ACCESS",
                    "LOAD.LIMITS.I_POS"             => "LIMITS.I.POS",
                    "LOAD.LIMITS.I_NEG"             => "LIMITS.I.NEG",
                    "LOAD.LIMITS.DIDT"              => "LIMITS.I.RATE",
                    "LOAD.LIMITS.D2IDT2"            => "LIMITS.I.ACCELERATION",
                    "LOAD.DIDT.STARTING"            => "REF.RATE.I.STARTING",
                    "LOAD.DIDT.TO_STANDBY"          => "REF.RATE.I.TO_STANDBY",
                    "LOAD.DIDT.STOPPING"            => "REF.RATE.I.STOPPING",
                    "LOAD.RST.OHMS_SER"             => "LOAD.OHMS_SER",
                    "LOAD.RST.OHMS_PAR"             => "LOAD.OHMS_PAR",
                    "LOAD.RST.HENRYS"               => "LOAD.HENRYS",
                    "MEAS.SELECT"                   => "DCCT.SELECT",
                    "MEAS.I_DIFF_LIM"               => "LIMITS.I.MEAS_DIFF",
                    "MODE.FB"                       => "REG.MODE_INIT",
                    "POL_SWITCH.TIMEOUT"            => "SWITCH.POLARITY.TIMEOUT",
                    "REF.IREF.ACCELERATION"         => "REF.DEFAULTS.I.ACCELERATION",
                    "REF.IREF.LINEAR_RATE"          => "REF.DEFAULTS.I.LINEAR_RATE",
                    "REF.VREF.ACCELERATION"         => "REF.DEFAULTS.V.ACCELERATION",
                    "REF.VREF.LINEAR_RATE"          => "REF.DEFAULTS.V.LINEAR_RATE",
                    "VS.LIMITS.V_POS"               => "LIMITS.V.POS",
                    "VS.LIMITS.V_NEG"               => "LIMITS.V.NEG",
                    "VS.LIMITS.V_Q41"               => "LIMITS.V.QUADRANTS41",
                    "VS.LIMITS.I_Q41"               => "LIMITS.I.QUADRANTS41",
                    "VS.LIMITS.DVDT"                => "LIMITS.V.RATE",
                    "VS.LIMITS.I_EARTH"             => "LIMITS.I.EARTH",
                    "VS.LIMITS.I_HARDWARE"          => "LIMITS.I.HARDWARE",
                );

# Properties to have their values replicated to arrays of 4

my @x4s =       (
                );

die "Usage: $0 <V1 config file>\n" if(@ARGV < 1);

my ($v1_filename) = @ARGV;

# Read version 1 configuration

open(V1FILE, "<", $v1_filename)
    or die "Failed to open V1 config file $v1_filename: $!\n";

my %v1_config;
while(<V1FILE>)
{
    chomp;

    s/^!\s*S //i; # Remove "! S " from beginning of line

    my ($property, $value)  = split(/\s+/,  $_);
    $property               = "\U$property";
    my @values              = split(/,/,    $value);
    $v1_config{$property}   = \@values;
}

close(V1FILE);

die "Version 1 file $v1_filename is empty.\n" if(!scalar(keys(%v1_config)));

# Create version 2 configuration

my %v2_config;

# Perform deletions

for my $v1_name (@deletions)
{
    delete($v1_config{$v1_name});
}

# Perform x4s

for my $v1_name (@x4s)
{
    next if(!defined($v1_config{$v1_name}));

    for(my $i = 1 ; $i < 4 ; $i++)
    {
        $v1_config{$v1_name}->[$i] = $v1_config{$v1_name}->[0];
    }
}

# Perform renames

for my $v1_name (keys(%renames))
{
    next if(!defined($v1_config{$v1_name}));

    $v2_config{$renames{$v1_name}} = $v1_config{$v1_name};
    delete($v1_config{$v1_name});
}

# Merge new properties and remaining version 1 properties into version 2 configuration

%v2_config = (%new, %v1_config, %v2_config);

# Properties requiring special handling

# ADC.EXTERNAL

$v2_config{"ADC.EXTERNAL"} = $v2_config{"ADC.EXTERNAL"}->[0] eq "INTERNAL" ?
                             ["DISABLED"] : ["ENABLED"];

# DCCT.SELECT

if($v2_config{"DCCT.SELECT"}->[0] eq "IAB")
{
    $v2_config{"DCCT.SELECT"} = ["AB"];
}
elsif($v2_config{"DCCT.SELECT"}->[0] eq "IA")
{
    $v2_config{"DCCT.SELECT"} = ["A"];
}
else # "IB"
{
    $v2_config{"DCCT.SELECT"} = ["B"];
}

# REG.MODE_INIT

$v2_config{"REG.MODE_INIT"} = $v2_config{"REG.MODE_INIT"}->[0] eq "DISABLED" ?
                              ["V"] : ["I"];

# LIMITS.V.ERR_WARNING

$v2_config{"LIMITS.V.ERR_WARNING"} = [$v2_config{"LIMITS.V.POS"}[0] * 0.05];

# Output version 2 configuration

for my $key (sort(keys(%v2_config)))
{
    next if(!defined($v2_config{$key}));

    printf ("! S %-30s %s\n", $key, join(",", @{$v2_config{$key}}));
}

# EOF
