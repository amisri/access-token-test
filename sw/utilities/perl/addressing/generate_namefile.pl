#!/usr/bin/perl -w
#
# Name:     generate_namefile.pl
# Purpose:  Generate fgcd namefile from database
# Author:   Stephen Page

use FGC::EDMSDB;
use strict;

# Check arguments

# Connect to database

my $dbh = FGC::EDMSDB::connect
            or die "Unable to connect to database: $!\n";

# Get WorldFIP addresses

my $previous_gateway = "";
for my $row (@{FGC::EDMSDB::get_all_worldfip_addresses($dbh)})
{
    # Print gateway entry if this devices gateway is not the same as the previous one

    if($row->{GATEWAY} ne $previous_gateway) # Gateway is not the same as the previous one
    {
        $previous_gateway = $row->{GATEWAY};

        print "\L$row->{GATEWAY}", ":0:1:$row->{GATEWAY}\n";
    }

    # Print device entry

    print "\L$row->{GATEWAY}", ":$row->{WORLDFIP_ADDRESS}:51:$row->{PC_NAME}\n";
}

# Disconnect from database

FGC::EDMSDB::disconnect($dbh);

# EOF
