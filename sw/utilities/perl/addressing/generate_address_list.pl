#!/usr/bin/perl -w
#
# Name:     generate_namefile.pl
# Purpose:  Generate address list for manual checking from database
# Author:   Stephen Page

use FGC::EDMSDB;
use strict;

# Check arguments

# Connect to database

my $dbh = FGC::EDMSDB::connect
            or die "Unable to connect to database: $!\n";

# Get and print WorldFIP addresses

printf("%-26s %-26s %s\n", "CONVERTER", "GATEWAY", "WORLDFIP ADDRESS");
print "----------------------------------------------------------------------\n";

my $previous_gateway = "";
for my $row (sort { $a->{PC_NAME} cmp $b->{PC_NAME} } @{FGC::EDMSDB::get_all_worldfip_addresses($dbh)})
#for my $row (@{FGC::EDMSDB::get_all_worldfip_addresses($dbh)})
{
    printf("%-26s %-26s %02d\n\n", $row->{PC_NAME}, "\L$row->{GATEWAY}", $row->{WORLDFIP_ADDRESS});
}

print "# End of file";

# Disconnect from database

FGC::EDMSDB::disconnect($dbh);

# EOF
