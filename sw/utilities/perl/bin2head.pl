#!/usr/bin/perl -w
#
# Name:     bin2head.pl
# Purpose:  Generates a header file containing an array representing a binary file
# Author:   Stephen Page

use bytes;
use File::Basename;
use strict;

die "Usage: ", basename($0), " <variable prefix> <binary file> <output file>\n" if(@ARGV != 3);

my ($var_prefix, $in_filename, $out_filename) = @ARGV;

# Keep a backup of the old header to be later compared

if (-e $out_filename)
{
	system("cp -p $out_filename $out_filename~");
}

open(OUT_FILE, '>', $out_filename)
    or die "Unable to open output file $out_filename : $!\n";

my $address;
my $previous_address;

print OUT_FILE  "// This file was automatically generated - DO NOT EDIT\n",
                "\n",
                "#ifndef ", "\U$var_prefix", "CODE_H\n",
                "#define ", "\U$var_prefix", "CODE_H\n",
                "\n",
                "static uint8_t const ", "\L$var_prefix", "_code[]\n",
                "= {";

open(IN_FILE, '<', $in_filename)
    or die "Unable to open input file $in_filename : $!\n";
binmode(IN_FILE);

my $line_bytes  = 0;
my $num_bytes   = 0;

# Write data to file

while(read(IN_FILE, $_, 1))
{
    print  OUT_FILE "\n" if(!$line_bytes);
    printf OUT_FILE ("0x%02X, ", ord($_));
    $num_bytes++;

    $line_bytes = ($line_bytes + 1) % 16;
}

close(IN_FILE);

print OUT_FILE  "\n",
                "}\n",
                ";\n",
                "\n",
                "#define ", "\U$var_prefix", "_CODE_SIZE $num_bytes\n",
                "\n",
                "#endif\n",
                "\n",
                "// EOF\n";
close(OUT_FILE);

# Use the old file (to avoid triggering the Makefile) if the file didn't change

if (-e "$out_filename~")
{
	system("diff $out_filename $out_filename~") or system("mv $out_filename~ $out_filename");
}

# EOF
