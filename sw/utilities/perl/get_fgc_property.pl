#!/usr/bin/perl
#
# Reads a property from the specified FGC device
#

use Test::TCP;

die "Usage: $0 <device> <property> [bin]\n" if(@ARGV < 2);

my ($device, $property, $type) = @ARGV;

$type='' if(!defined($type));

my $prop = "$property $type";

my $buffer = get($device, $prop);

print $buffer;
