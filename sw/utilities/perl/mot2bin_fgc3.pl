#!/usr/bin/perl -w
#
# Name:     mot2bin.pl
# Purpose:  Convert a Motorola S-record file into a binary file
# Author:   Stephen Page

use File::Basename;
use strict;

die "Usage: ", basename($0), " <S-record file> <binary file>\n" if(@ARGV != 2);

my ($mot_filename, $bin_filename) = @ARGV;

open(MOT_FILE, '<', $mot_filename)
    or die "Unable to open S-record file $mot_filename : $!\n";

# Read Motorola file and extract values of all addresses

my %address_data;

while(<MOT_FILE>)
{
    # Remove carriage-return and line-feed

    s/\015\012//;
    chomp;

    if(/^S2/ || /^S3/)
    {
        # Extract address and data from line

        my $address;
        my $data_ascii;

        if(/^S2/)
        {
            $address    = hex(substr($_, 4, 6));
            $data_ascii = substr($_, 10, length($_) - 12);
        }
        elsif(/^S3/)
        {
            $address    = hex(substr($_, 4, 8));
            $data_ascii = substr($_, 12, length($_) - 14);
        }

        my @data_bin;

        # Convert data to binary

        my $byte;
        my $i;
        for($i = 0 ; $i < length($data_ascii) ; $i += 2)
        {
            $byte = substr($data_ascii, $i, 2);
            push(@data_bin, pack("C", hex($byte)));
        }

        # Store data in hash
        # printf "-%08x-\n",$address;

        $address_data{$address} = \@data_bin;
    }
}

close(MOT_FILE);

# Write binary file

open(BIN_FILE, '>', $bin_filename)
    or die "Unable to open binary file $bin_filename : $!\n";


my $boot_loader = 1;
my $section_address;
my $section_length = 0;
my @section_data = ();

# Adress 0x100003fc is special because it contains the entry point
# to jump into once the Boot and Main have been loaded. This is not 
# needed however for the DSP bootloader. This address is defined in
# the link.cmd files.

if(exists $address_data{0x100003fc})
{    
    syswrite(BIN_FILE, join('', @{$address_data{0x100003fc}}));
    delete($address_data{0x100003fc});

    $boot_loader = 0;
}

my @addresses = (sort {$a <=> $b} (keys(%address_data)));


# foreach my $address (@addresses)
for my $i (0 .. $#addresses)
{
    my $address = $addresses[$i];

    if(!defined($section_address))
    {
        $section_address = $address;
    }

    push @section_data, @{$address_data{$address}};

    $section_length = scalar(@section_data);

    # Check whether data is not sequential

    if(   $address == $addresses[-1]
       || ($section_address + $section_length) != $addresses[$i + 1])
       
    {
        # Write data to file

        my $remaining = 4 - $section_length % 4;

        $section_length += $remaining;

        while ($remaining-- > 0)
        {
            push @section_data, pack('C', '0');
        }

        # Do not write the section address and length if parsing the DSP bootloader
        # since it cannot interpret itself. This binary only has one memory section
        # hence why this check is only done here

        if ($boot_loader == 0)
        {
            syswrite(BIN_FILE, pack('L', $section_address));
            syswrite(BIN_FILE, pack('L', $section_length));
        }
        syswrite(BIN_FILE, join('' , @section_data));

        # Reset section data

        $section_address = undef;
        $section_length = 0;
        @section_data = ();
    }
}


close(BIN_FILE);

# EOF
