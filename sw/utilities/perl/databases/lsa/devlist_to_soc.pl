#!/usr/bin/perl -w
#
# Name:     devlist_to_soc.pl
# Purpose:  Convert an FGC device list to a set of circuits (SOC) in the LSA database
# Notes:    SOC must exist in the LSA database before this is run
# Author:   Stephen Page

use FGC::DeviceList;
use FGC::LSADB;
use FGC::Names;
use strict;

die "Usage: $0 <filename> <SOC name>\n" if(@ARGV != 2);
my ($filename, $soc_name) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $list_devices = FGC::DeviceList::read($devices, $filename)
                    or die "Failed to read device list $filename\n";

# Connect to database

my $dbh = FGC::LSADB::connect()
            or die "Unable to connect to database\n";

# Get SOCS

my $socs = FGC::LSADB::get_socs($dbh)
            or die "Failed to get SOCS from database\n";

# Check that SOC exists

die "SOC $soc_name does not exist\n" if(!defined($socs->{$soc_name}));

# Set devices in SOC

FGC::LSADB::set_soc_converters($dbh, $soc_name, [map { $_->{name} } @$list_devices]);

$dbh->disconnect;

# EOF
