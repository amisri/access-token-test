#!/usr/bin/perl -w
#
# Name:     lsa_rf_chans.pl
# Purpose:  Read RF channel details from LSA
# Author:   Stephen Page

use FGC::LSADB;
use FGC::Names;
use strict;

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Connect to database

my $dbh = FGC::LSADB::connect() or die "Unable to connect to database\n";

# Read data for all RF channels

my $rf_chans = FGC::LSADB::get_rf_channels($dbh);
$dbh->disconnect;

if(!defined($rf_chans))
{
    die "Failed to read RF channels from database\n";
}

# Output data

for my $channel (@$rf_chans)
{
    print "$channel->{FGC_NAME}:$channel->{CHANNEL}:$channel->{CHANNEL_NAME}\n";
}

# EOF
