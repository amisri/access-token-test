#!/usr/bin/perl -w
#
# Name:     lsa_prop.pl
# Purpose:  Read converter property from LSA
# Author:   Stephen Page

use FGC::LSADB;
use FGC::Names;
use strict;

die "\nUsage: $0 <property>\n\n" if(@ARGV != 1);
my ($property) = @ARGV;

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Connect to database

my $dbh = FGC::LSADB::connect() or die "Unable to connect to database\n";

for my $device (sort { $a->{name} cmp $b->{name} } values(%$devices))
{
    my $value = FGC::LSADB::get_converter_parameter($dbh, $device->{name}, $property)
                or warn "Failed to get $property for device $device->{name}\n" and next;

    print "$device->{name},$value\n";
    #printf("$device->{name},%.5E\n", $value);
}
$dbh->disconnect;

# EOF
