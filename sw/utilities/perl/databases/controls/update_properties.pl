#!/usr/bin/perl -w
#
# Name:     update_properties.pl
# Purpose:  Update Controls Database with details of FGC properties
# Author:   Stephen Page

use Data::Dumper;
use Getopt::Long;
use FGC::Consts;
use FGC::ControlsDB;
use FGC::Properties;
use File::Basename;
use Carp;

use strict;

# FGC types mapped to database types
# Note that any type not declared here will default to NULL

my %db_types =  (
                    ABSTIME     => 'DOUBLE',
                    ABSTIME8    => 'DOUBLE',
                    BOOL        => 'BOOL',
                    CHAR        => 'STRING',
                    CYCSEL      => 'STRING',
                    DOUBLE      => 'DOUBLE',
                    FLOAT       => 'FLOAT',
                    INT8S       => 'BYTE',
                    INT8U       => 'SHORT',
                    INT16S      => 'SHORT',
                    INT16U      => 'INT',
                    INT32S      => 'INT',
                    INT32U      => 'LONG',
                    NULL        => undef,
                    PARENT      => 'MAP',
                    POINT       => 'DISCRETE_FUNCTION',
                    FL_POINT    => 'DISCRETE_FUNCTION_LIST',
                    STRING      => 'STRING',
                    UNIXTIME    => 'STRING',
                    DEV_NAME    => 'STRING',
                    MAC         => 'STRING',
                );

my $access = FGC_PRODUCTION_ACCESS;
my $dest_dir = FGC_PRODUCTION_LIBS;
my $fgc_home=dirname(__FILE__)."/../../../../../";

sub update_properties($$)
{
    my ($dbh, $class) = @_;

    # Get properties

    my $db_property_rows = FGC::ControlsDB::get_hw_properties($dbh, $class)
                            or die "Failed to get properties from Controls Database\n";


    # Add subscription property to %fgc_properties

    $fgc_properties{"SUB_$class"} = {
                                        classes     => { $class => {maxels => 1, operational => 1, type => 'PARENT', flags => {} } },
                                        group       => 'OPERATIONAL',
                                        name        => "SUB_$class",
                                        subscribe   => 1,
                                        class_symbols => {},
                                    };

    # Add subscription property to %fgc_properties

    $fgc_properties{"SUB"} = {
                                        classes     => { $class => {maxels => 1, operational => 1, type => 'PARENT', flags => {}} },
                                        group       => 'OPERATIONAL',
                                        name        => "SUB",
                                        subscribe   => 1,
                                        class_symbols => {},
                                    };

    # Hack to keep class 51 and 91 backward compatible with respect to class 92 and 94, respectively

    if($class == 92)
    {
          $fgc_properties{"SUB_51"} = {
                                              classes     => { $class => {maxels => 1, operational => 1, type => 'PARENT', flags => {}} },
                                              group       => 'OPERATIONAL',
                                              name        => "SUB_51",
                                              subscribe   => 1,
                                              class_symbols => {},
                                          };


    }

    if($class == 94)
    {
          $fgc_properties{"SUB_91"} = {
                                              classes     => { $class => {maxels => 1, operational => 1, type => 'PARENT', flags => {}} },
                                              group       => 'OPERATIONAL',
                                              name        => "SUB_91",
                                              subscribe   => 1,
                                              class_symbols => {},
                                          };


    }

    # Check that each property in the database is consistent with its entry in the file

    my %db_properties;
    for my $property (@$db_property_rows)
    {
        $db_properties{$property->{PROPERTY_NAME}} = $property;
        my $file_property = $fgc_properties{$property->{PROPERTY_NAME}};


        # Check whether property is in file

        if(defined($file_property) && defined($file_property->{classes}->{$class})) # Property is in file
        {
            my $symbols_type;
            my $type;

            if(defined($file_property->{classes}->{$class}->{flags}->{SYM_LST}) && $file_property->{classes}->{$class}->{type} ne 'PARENT')
            {
                $symbols_type   = defined($file_property->{classes}->{$class}->{flags}->{BIT_MASK}) ? 'BITMASK' : 'ENUM';
                $type           = $db_types{STRING};
            }
            else
            {
                $symbols_type   = 'NONE';
                $type           = $db_types{$file_property->{classes}->{$class}->{type}};
            }

            if(!defined($type) && $file_property->{classes}->{$class}->{type} ne 'NULL')
            {
                $type = $db_types{STRING};

                # Supress warnings for known types:  FIFOADC, LOGPSU, LOG, LOGTIMING, LOGEVT

                if ($file_property->{classes}->{$class}->{type} ne 'FIFOADC'   &&
                    $file_property->{classes}->{$class}->{type} ne 'LOGPSU'    &&
                    $file_property->{classes}->{$class}->{type} ne 'LOG'       &&
                    $file_property->{classes}->{$class}->{type} ne 'LOGTIMING' &&
                    $file_property->{classes}->{$class}->{type} ne 'LOGEVT')
                 {
                     warn "WARNING: No DB type for type $file_property->{classes}->{$class}->{type} for property $file_property->{name} (defaulting to STRING)\n";
                 }
            }

            # Set maximum number of elements

            my $maxels;
            if($file_property->{classes}->{$class}->{type} eq 'CHAR' && $type eq 'STRING')
            {
                # Property is a character array appearing as a string
                # Set maxels to 1

                $maxels = 1;
            }
            else
            {
                $maxels = $file_property->{classes}->{$class}->{maxels};
            }

            # Undefine maximum number of elements for scalar properties

            if(defined($maxels) && $maxels == 1)
            {
                $maxels = undef;
            }

            # Undefine maximum number of elements for DISCRETE FUNCTION or DISCRETE FUNCTION LIST

            if($file_property->{classes}->{$class}->{type} eq 'POINT' or $file_property->{classes}->{$class}->{type} eq 'FL_POINT')
            {
                $maxels = undef;
            }

            # Set operational

            my $operational = $file_property->{classes}->{$class}->{operational} ? 'Y' : 'N';

            # Check whether property should be monitorable (class 5x only provide subscription for the SUB_5x property)

            my $monitorable = $file_property->{name} =~ /^SUB_|^SUB$/ ||
                              ($file_property->{classes}->{$class}->{get} && $class != 51 && $class != 53 && $class != 59)
                              ? 'Y' : 'N';

            # Check whether property should be settable (parent properties are settable to allow composite sets)

            my $settable;
            if($file_property->{classes}->{$class}->{type} eq 'PARENT') {
               $settable = 'N';
               for my $child_file_property (grep {$_->{name} =~ /^$property->{PROPERTY_NAME}\../} values %fgc_properties)
               {
                   my $child_settable = $child_file_property->{name} !~ /^SUB_|^SUB$/ &&
                                  defined($child_file_property->{classes}->{$class}) &&
                                  $child_file_property->{classes}->{$class}->{set}
                                  ? 'Y' : 'N';
                   if($child_settable eq 'Y') {
                       $settable = 'Y';
                       last;
                   }
               }
            } else {
               $settable = $file_property->{name} !~ /^SUB_|^SUB$/ &&
                              $file_property->{classes}->{$class}->{set}
                              ? 'Y' : 'N';
            }

            my $gettable = $file_property->{classes}->{$class}->{get} ? 'Y' : 'N';

            # PPM can only be set, if property is settable. Else, the flag represents cycle_bound

            my $ppm = 'N';
            my $is_cycle_bound = 'N';
            if(defined($file_property->{classes}->{$class}->{flags}->{PPM}))
            {
                if($settable eq 'Y')
                {
                    $ppm = 'Y';
                }
                else
                {
                    $is_cycle_bound = 'Y';
                }
            }

            # TODO: EPCCCS-9414: logic of how the transactional flag should be set to Y or N still under discussion. For the time being: 'N' for all.
            my $is_transactional = defined($file_property->{classes}->{$class}->{flags}->{TRANSACTIONAL}) ? 'Y' : 'N';
            $is_transactional = 'N';

            # Check whether property attributes have changed

            if(
                defined($property->{PRIMITIVE_DATA_TYPE})   && !defined($type)                                          ||  # Property type has been removed
               !defined($property->{PRIMITIVE_DATA_TYPE})   &&  defined($type)                                          ||  # Property type has been added
               (defined($property->{PRIMITIVE_DATA_TYPE})   &&  defined($type)                                          &&
                $property->{PRIMITIVE_DATA_TYPE}            ne $type)                                                   ||  # Type has changed
                defined($property->{ARRAY_DIM})             && !defined($maxels)                                        ||  # Number of elements has been removed
               !defined($property->{ARRAY_DIM})             &&  defined($maxels)                                        ||  # Number of elements has been added
               (defined($property->{ARRAY_DIM})             &&  defined($maxels)                                        &&
               $property->{ARRAY_DIM}                       ne $maxels)                                                 ||  # Number of elements has changed
               $property->{OPERATIONAL}                     ne $operational                                             ||  # Property operational status has changed
               $property->{GETTABLE}                        ne $gettable                                                ||  # Property gettable state has changed
               $property->{SETTABLE}                        ne $settable                                                ||  # Property settable state has changed
               $property->{MONITORABLE}                     ne $monitorable                                             ||  # Property monitorable state has changed
               $property->{PPM}                             ne $ppm                                                     ||  # Property PPM state has changed
                defined($property->{UNITS})                 && !defined($file_property->{units})                        ||  # Property units have been removed
               !defined($property->{UNITS})                 &&  defined($file_property->{units})                        ||  # Property units have been added
               (defined($property->{UNITS})                 &&  defined($file_property->{units})                        &&
               $property->{UNITS}                           ne $file_property->{units})                                 ||  # Property units have changed
               $property->{SYMBOLS_TYPE}                    ne $symbols_type                                            ||  # Property symbols type has changed
               $property->{IS_CYCLE_BOUND}                  ne $is_cycle_bound                                          ||  # Property cycle bound state has changed
               $property->{IS_TRANSACTIONAL}                ne $is_transactional                                            # Property transactional state has changed
              )
            {
                # Update the property entry

                print "Updating property $file_property->{name} in database...\n";

                printf("FROM: CLASS %s, NAME %s, TYPE %s, ELS %s, OP %s, GET %s, SET %s, MONITORABLE %s, PPM %s, UNITS %s, SYMBOLS_TYPE %s, IS_CYCLE_BOUND %s, IS_TRANSACTIONAL %s\n",
                        $class,
                        $property->{PROPERTY_NAME},
                        defined($property->{PRIMITIVE_DATA_TYPE}) ? $property->{PRIMITIVE_DATA_TYPE} : '',
                        defined($property->{ARRAY_DIM}) ? $property->{ARRAY_DIM} : '',
                        $property->{OPERATIONAL},
                        $property->{GETTABLE},
                        $property->{SETTABLE},
                        $property->{MONITORABLE},
                        $property->{PPM},
                        $property->{UNITS} || '',
                        $property->{SYMBOLS_TYPE},
                        $property->{IS_CYCLE_BOUND},
                        $property->{IS_TRANSACTIONAL});

                printf("TO:   CLASS %s, NAME %s, TYPE %s, ELS %s, OP %s, GET %s, SET %s, MONITORABLE %s, PPM %s, UNITS %s, SYMBOLS_TYPE %s, IS_CYCLE_BOUND %s, IS_TRANSACTIONAL %s\n",
                       $class,
                       $file_property->{name},
                       defined($type) ? $type : '',
                       defined($maxels) ? $maxels : '',
                       $operational,
                       $gettable,
                       $settable,
                       $monitorable,
                       $ppm,
                       $file_property->{units} || '',
                       $symbols_type,
                       $is_cycle_bound,
                       $is_transactional);

                FGC::ControlsDB::update_hw_property($dbh,
                                                    $class,
                                                    $file_property->{name},
                                                    $type,
                                                    $maxels,
                                                    $operational,
                                                    $gettable,
                                                    $settable,
                                                    $monitorable,
                                                    $ppm,
                                                    $file_property->{units} || '',
                                                    $symbols_type,
                                                    $is_cycle_bound,
                                                    $is_transactional)
                    or die "Failed to update property $property->{PROPERTY_NAME} in Controls Database: $!\n";
            }

            # Update property symbols

            if($symbols_type ne 'NONE')
            {
                my $db_symbol_rows = FGC::ControlsDB::get_hw_symbols($dbh, $class, $property->{PROPERTY_NAME})
                                        or die "Failed to get symbols for property $property->{PROPERTY_NAME} from Controls Database\n";

                my $file_symbols = $file_property->{class_symbols}->{$class};

                # Update symbols in the database

                my %db_symbols;
                for my $db_symbol (@$db_symbol_rows)
                {
                    $db_symbols{$db_symbol->{SYMBOL}} = $db_symbol;

                    my $file_symbol = $file_symbols->{$db_symbol->{SYMBOL}};

                    # Check whether symbol is in file

                    if(defined($file_symbol))
                    {
                        # Check whether symbol attributes have changed

                        if(
                           !defined($db_symbol->{NUMERIC_VALUE})                                                        ||  # Numeric value should be set (CCS-10504)
                           $db_symbol->{NUMERIC_VALUE}                  ne $db_symbol->{SYMBOL_ID}                      ||  # Numeric value only used internally. Expose unique arbitrary value (CCS-10504)
                           $db_symbol->{SETTABLE}                       ne ($file_symbol->{settable} ? 'Y' : 'N')       ||  # Symbol settable state has changed
                           $db_symbol->{MEANING}                        ne $file_symbol->{meaning}                          # Symbol meaning has changed
                          )
                        {
                            # Update the symbol entry

                            print "Updating symbol $db_symbol->{SYMBOL} for $file_property->{name} in database...\n";

                            printf("FROM: CLASS %s, PROPERTY %s, SYMBOL %s, VALUE %s, SETTABLE %s, MEANING %s\n",
                                    $class,
                                    $property->{PROPERTY_NAME},
                                    $db_symbol->{SYMBOL},
                                    defined($db_symbol->{NUMERIC_VALUE}) ? $db_symbol->{NUMERIC_VALUE} : '',
                                    $db_symbol->{SETTABLE},
                                    $db_symbol->{MEANING});

                            printf("TO:   CLASS %s, PROPERTY %s, SYMBOL %s, VALUE %s, SETTABLE %s, MEANING %s\n",
                                    $class,
                                    $property->{PROPERTY_NAME},
                                    $db_symbol->{SYMBOL},
                                    $db_symbol->{SYMBOL_ID},
                                    $file_symbol->{settable} ? 'Y' : 'N',
                                    $file_symbol->{meaning});

                            FGC::ControlsDB::update_hw_symbol($dbh,
                                                              $class,
                                                              $file_property->{name},
                                                              $db_symbol->{SYMBOL},
                                                              $file_symbol->{settable} ? 'Y' : 'N',
                                                              $file_symbol->{meaning})
                                or die "Failed to update symbol $db_symbol->{SYMBOL} for property $property->{PROPERTY_NAME} in Controls Database: $!\n";
                        }
                    }
                    else # Symbol is not in file
                    {
                        # Remove symbol

                        print "Removing symbol $db_symbol->{SYMBOL} for property $property->{PROPERTY_NAME} from database...\n";
                        FGC::ControlsDB::remove_hw_symbol($dbh, $class, $property->{PROPERTY_NAME}, $db_symbol->{SYMBOL})
                            or die "Unable to remove symbol $db_symbol->{SYMBOL} for property $property->{PROPERTY_NAME} from Controls Database: $!\n";
                    }
                }

                # Add missing symbols to the database

                for my $file_symbol (keys(%$file_symbols))
                {
                    if(!defined($db_symbols{$file_symbol}))
                    {
                        print "Adding symbol $file_symbol for property $file_property->{name} to database...\n";

                        FGC::ControlsDB::add_hw_symbol($dbh,
                                                       $class,
                                                       $file_property->{name},
                                                       $file_symbol,
                                                       $file_symbols->{$file_symbol}->{settable} ? 'Y' : 'N',
                                                       $file_symbols->{$file_symbol}->{meaning})
                            or die "Failed to add symbol $file_symbol for property $file_property->{name} to Controls Database: $!\n";
                    }
                }
            }
            else # Property has no symbols
            {
                FGC::ControlsDB::remove_hw_property_symbols($dbh, $class, $property->{PROPERTY_NAME})
                    or die "Failed to remove symbols for property $property->{PROPERTY_NAME} from Controls Database: $!\n";
            }
        }
        else # Property is not in file
        {
            # Remove symbols for the property

            FGC::ControlsDB::remove_hw_property_symbols($dbh, $class, $property->{PROPERTY_NAME})
                or die "Failed to remove symbols for property $property->{PROPERTY_NAME} from Controls Database: $!\n";

            # Remove property

            print "Removing property $property->{PROPERTY_NAME} from database...\n";
            FGC::ControlsDB::remove_hw_property($dbh, $class, $property->{PROPERTY_NAME})
                or die "Unable to remove property $property->{PROPERTY_NAME} from Controls Database: $!\n";
        }

        # SUB and SUB_ property fields

        if($property->{PROPERTY_NAME} =~ /^SUB_|^SUB$/)
        {
            # SUB property was in DB

            my $db_sub_fields   = FGC::ControlsDB::get_hw_property_fields($dbh, $class, $property->{PROPERTY_NAME});
            my $file_sub_fields = $fgc_cmw_fields_func[$class]();

            for my $row (@$db_sub_fields)
            {
                my $db_field = $row->{FIELD_NAME};
                my $db_type  = $row->{PRIMITIVE_DATA_TYPE};

                if(defined($file_sub_fields->{$db_field}))
                {
                    # Field defined in DB and file, try to update type

                    my $file_type =  $db_types{$file_sub_fields->{$db_field}};

                    # Hack to make the CMW and DB types compatible (EPCCCS-6514)
                    if($file_type eq 'SHORT' or $file_type eq 'BYTE') {
                      $file_type = 'INT';
                    }

                    if($db_type ne $file_type)
                    {
                        print "Updating field $db_field for property $property->{PROPERTY_NAME} in database...\n";

                        printf("FROM: CLASS %s, PROPERTY %s, FIELD %s, TYPE %s\n",
                            $class,
                            $property->{PROPERTY_NAME},
                            $db_field,
                            $db_type);

                        printf("TO: CLASS %s, PROPERTY %s, FIELD %s, TYPE %s\n",
                            $class,
                            $property->{PROPERTY_NAME},
                            $db_field,
                            $file_type);

                        FGC::ControlsDB::update_hw_property_field($dbh,
                            $class,
                            $property->{PROPERTY_NAME},
                            $db_field,
                            $file_type,
                            '',
                            'NONE')
                            or die "Unable to update field $db_field for property $property->{PROPERTY_NAME} from Controls Database: $!\n";
                    }

                }
                else
                {
                    # Field not defined in file, remove it

                    print "Removing field $db_field for property $property->{PROPERTY_NAME} from database...\n";
                    FGC::ControlsDB::remove_hw_property_field($dbh, $class, $property->{PROPERTY_NAME}, $db_field)
                    or die "Unable to remove field $db_field for property $property->{PROPERTY_NAME} from Controls Database: $!\n";
                }
            }
        }


    }

    # Check whether each property in the file is in the database

    for my $property (sort { $a->{name} cmp $b->{name} } values(%fgc_properties))
    {
        next if(!defined($property->{classes}->{$class}));

        if(!defined($db_properties{$property->{name}})) # Property is not in the database
        {
            my $symbols_type;
            my $type;

            if(defined($property->{classes}->{$class}->{flags}->{SYM_LST}) && $property->{classes}->{$class}->{type} ne 'PARENT')
            {
                $symbols_type   = defined($property->{classes}->{$class}->{flags}->{BIT_MASK}) ? 'BITMASK' : 'ENUM';
                $type           = $db_types{STRING};
            }
            else
            {
                $symbols_type   = 'NONE';
                $type           = $db_types{$property->{classes}->{$class}->{type}};
            }

            if(!defined($type) && $property->{classes}->{$class}->{type} ne 'NULL')
            {
                $type = $db_types{STRING};
                warn "WARNING: No DB type for type $property->{classes}->{$class}->{type} for property $property->{name} (defaulting to STRING)\n";
            }

            my $operational = $property->{classes}->{$class}->{operational} ? 'Y' : 'N';

            # Check whether property should be monitorable

            my $monitorable = $property->{name} =~ /^SUB_|^SUB$/ ||
                              ($property->{classes}->{$class}->{get} && $class != 51 && $class != 53 && $class != 59)
                              ? 'Y' : 'N';

            # Check whether property should be settable (parent properties are settable to allow composite sets)

            my $settable;
            if($property->{classes}->{$class}->{type} eq 'PARENT') {
               $settable = 'N';
               for my $child_file_property (grep {$_->{name} =~ /^$property->{name}\../} values %fgc_properties)
               {
                   my $child_settable = $child_file_property->{name} !~ /^SUB_|^SUB$/ &&
                                  defined($child_file_property->{classes}->{$class}) &&
                                  $child_file_property->{classes}->{$class}->{set}
                                  ? 'Y' : 'N';
                   if($child_settable eq 'Y') {
                       $settable = 'Y';
                       last;
                   }
               }
            } else {
               $settable = $property->{name} !~ /^SUB_|^SUB$/ &&
                              $property->{classes}->{$class}->{set}
                              ? 'Y' : 'N';
            }

            my $gettable = $property->{classes}->{$class}->{get} ? 'Y' : 'N';

            my $units = defined($property->{units}) ? $property->{units} : '';

            # PPM can only be set, if property is settable. Else, the flag represents cycle_bound

            my $ppm = 'N';
            my $is_cycle_bound = 'N';
            if(defined($property->{classes}->{$class}->{flags}->{PPM}))
            {
                if($settable eq 'Y')
                {
                    $ppm = 'Y';
                }
                else
                {
                    $is_cycle_bound = 'Y';
                }
            }

            # TODO: EPCCCS-9414: logic of how the transactional flag should be set to Y or N still under discussion. For the time being: 'N' for all.
            my $is_transactional = defined($property->{classes}->{$class}->{flags}->{TRANSACTIONAL}) ? 'Y' : 'N';
            $is_transactional = 'N';

            # Set maximum number of elements

            my $maxels;
            if($property->{classes}->{$class}->{type} eq 'CHAR' && $type eq 'STRING')
            {
                # Property is a character array appearing as a string
                # Set maxels to 1

                $maxels = 1;
            }
            else
            {
                $maxels = $property->{classes}->{$class}->{maxels};
            }

            # Undefine maximum number of elements for scalar properties

            if(defined($maxels) && $maxels == 1)
            {
                $maxels = undef;
            }

            # Undefine maximum number of elements for DISCRETE FUNCTION

            if($property->{classes}->{$class}->{type} eq 'POINT' or $property->{classes}->{$class}->{type} eq 'FL_POINT')
            {
                $maxels = undef;
            }

            # Add property to database

            print "Adding property $property->{name} to database...\n";
            FGC::ControlsDB::add_hw_property($dbh,
                                             $class,
                                             $property->{name},
                                             defined($type) ? $type : '',
                                             defined($maxels) ? $maxels : '',
                                             $operational,
                                             $gettable,
                                             $settable,
                                             $monitorable,
                                             $ppm,
                                             $units,
                                             $symbols_type,
                                             $is_cycle_bound,
                                             $is_transactional)
                or die "Failed to add property $property->{name} to Controls Database: $!\n";

            # Add symbols for the property to the database

            if($symbols_type ne 'NONE')
            {
                my $symbols = $property->{class_symbols}->{$class};

                for my $symbol (keys(%$symbols))
                {
                    print "Adding symbol $symbol for property $property->{name} to database...\n";

                    FGC::ControlsDB::add_hw_symbol($dbh,
                                                   $class,
                                                   $property->{name},
                                                   $symbol,
                                                   $gettable,
                                                   $symbols->{$symbol}->{meaning})
                        or die "Failed to add symbol $symbol for property $property->{name} to Controls Database: $!\n";
                }
            }
        }

        # SUB and SUB_ property fields

        if($property->{name} =~ /^SUB_|^SUB$/)
        {
            # Property define in file

            my $file_sub_fields = $fgc_cmw_fields_func[$class]();
            my $db_sub_fields   = FGC::ControlsDB::get_hw_property_fields($dbh, $class, $property->{name});

            for my $file_field (keys(%$file_sub_fields))
            {
                my @db_field_row = grep { $_->{FIELD_NAME} eq $file_field } @$db_sub_fields;

                if(!defined($db_field_row[0]))
                {
                    print "Adding field $file_field for property $property->{name} to database...\n";

                    my $db_type = $db_types{$file_sub_fields->{$file_field}};

                    # Hack to make the CMW and DB types compatible (EPCCCS-6514)
                    if($db_type eq 'SHORT' or $db_type eq 'BYTE') {
                      $db_type = 'INT';
                    }

                    FGC::ControlsDB::add_hw_property_field($dbh,
                        $class,
                        $property->{name},
                        $file_field,
                        $db_type,
                        '',
                        'NONE')
                        or die "Failed to add field $file_field for property $property->{name} to Controls Database: $!\n";
                }
            }
        }
    }
} # end update_properties

########### BEGIN Functions to allow creation of migration maps from CLASS_A to CLASS_A_NEW ################
# These functions allow to publish a current property model of a given class
# under a new class name to CCDB to prevent overriding the property model of the actual class in CCDB.
# This comes in handy when we want to create a new migration map from and property model of CLASS to a new property model of CLASS

sub _prepare_swap($$)
{
    # Renames the  $class to $class_SWAP (to not lose existing property associations)
    # and then creates a new entry for $class (based on the existing $class_SWAP entry) which then can be used to insert the new class property model
    my ($dbh, $class) = @_;
    FGC::ControlsDB::rename_class($dbh, $class, $class."_SWAP");
    FGC::ControlsDB::duplicate_class($dbh, $class."_SWAP", $class);
}

sub _finish_swap($$$)
{
    # Renames $class to $new_class in CCDB
    # and renames the $class_SWAP back to $class
    # so that everything is back to normal
    my ($dbh, $class, $new_class) = @_;
    FGC::ControlsDB::rename_class($dbh, $class, $new_class);
    FGC::ControlsDB::rename_class($dbh, $class."_SWAP", $class);
}

sub publish_property_model_of_class_to_class_new($$$)
{
    # Creates a new class in CCDB called $new_class (by duplicating the entry of $class)
    # and publishs the local repos property model of $class to it instead of overwriting the property model of $class in CCDB
    my ($dbh, $class, $new_class) = @_;
    my $prev_commit_flag = $dbh->{AutoCommit};
    $dbh->{AutoCommit} = 0;

    _prepare_swap($dbh, $class);
    update_properties($dbh, $class);
    _finish_swap($dbh, $class, $new_class);

    if(!FGC::ControlsDB::try_to_commit($dbh))
    {
        $dbh->rollback or die $dbh->errstr;
    }

    $dbh->{AutoCommit} = $prev_commit_flag;
}
########### END Functions to allow creation of migration maps from CLASS_A to CLASS_A_NEW ################

# Check arguments

my ($username, $password, $commit);
GetOptions('commit'=>\$commit, 'db_user:s'=>\$username, 'db_pw:s'=>\$password);

# Enable simulation mode by default (i.e. if no commit flag has been supplied).

my $simulation = !defined($commit);

die "Usage: $0 <class> [<new_class>] [--commit] [--db_user=] [--db_pw=] \n --commit: Commit changes to CCDB.\n" if(@ARGV != 1 && @ARGV != 2);
my ($class, $new_class) = @ARGV;

if($simulation)
{
    print "\n-------------------------------------------------------------------------------------\n";
    print "SIMULATION MODE:\nThe script will print its actions, but it will not modify CCDB.\nTo propagate the changes to CCDB, run the script with the --commit option.";
    print "\n-------------------------------------------------------------------------------------\n\n";

}
else
{
    print "\n----------------------------------------------------------------------------------------------------------\n";
    print "WARNING:\nChanges will be propagated to CCDB.\nTo run the script in simulation mode, cancel the execution and run the script without the --commit option.";
    print "\n----------------------------------------------------------------------------------------------------------\n\n";
}

# Ask user for name and pw if not provided in options
if(!defined($username) || !defined($password))
{
    # Prompt for username

    print "\nUsername: ";
    chomp($username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp($password = <STDIN>);
    system("stty echo");
    print "\n\n";
}

# Connect to database

my $dbh = FGC::ControlsDB::connect($username, $password)
            or die "Unable to connect to Controls Database: $!\n";

$dbh->{AutoCommit} = 0 if($simulation);

# Publish property model
if(defined($new_class))
{
    print "Publishing configuration properties of class $class as property model of class $new_class in CCDB...\n";
    publish_property_model_of_class_to_class_new($dbh, $class, $new_class);
}
else
{
    print "Updating configuration properties in CCDB for class $class...\n";
    update_properties($dbh, $class);
}

$dbh->rollback or die $dbh->errstr if($simulation);

# Disconnect from database
FGC::ControlsDB::disconnect($dbh);

die "Exiting simulation mode." if($simulation);

# Copy Properties.pm to production

print "Copying Properties.pm to production...\n\n";

system ("scp ${fgc_home}/sw/clients/perl/FGC/Properties.pm $access:$dest_dir") == 0
    or confess("ERROR! Could not copy Properties.pm to production");

# EOF
