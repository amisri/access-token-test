#!/usr/bin/perl -w
#
# Name:     update_alarms.pl
# Purpose:  Update alarm definitions for a class within the Controls Database
# Author:   Stephen Page

use FGC::ControlsDB;
use File::Basename;
use strict;

die "Usage: $0 <class>\n" if(@ARGV != 1);
my ($class) = @ARGV;

# Begin functions

# Read alarms from fault code file

sub read_alarms_file()
{
    # Open fault code file

    open(FILE, "<", "fault_codes")
        or die "Unable to open fault code file: $!\n";

    my %alarms;

    my ($alarm_class, $symbol, $fault_code, $priority, $problem, $cause, $consequence, $action);
    while(( $alarm_class,
            $symbol,
            $fault_code,
            $priority,
            $problem,
            $cause,
            $consequence,
            $action) = split(":", <FILE>))
    {
        # Skip data for classes other than the target class

        next if($alarm_class != $class);

        chomp($action);

        $symbol = undef if($symbol eq "");

        my %alarm;
        $alarm{ACTION}      = $action       || '';
        $alarm{CAUSE}       = $cause        || '';
        $alarm{CONSEQUENCE} = $consequence  || '';
        $alarm{FAULT_CODE}  = $fault_code;
        $alarm{PRIORITY}    = $priority;
        $alarm{PROBLEM}     = $problem;
        $alarm{SYMBOL}      = $symbol;

        $alarms{$fault_code} = \%alarm;
    }
    close(FILE);

    return(\%alarms);
}

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read alarms for class from file

my $file_alarms = read_alarms_file();

# Connect to database

my $dbh = FGC::ControlsDB::connect($username, $password)
            or die "Unable to connect to Controls Database: $!\n";

# Get alarms from database

my $db_alarm_rows = FGC::ControlsDB::get_class_alarms($dbh, $class)
                        or die "Failed to get alarms from Controls Database\n";

# Check that each device in the database is consistent with its entry in the name file

my %db_alarms;
for my $db_alarm (@$db_alarm_rows)
{
    $db_alarms{$db_alarm->{FAULT_CODE}} = $db_alarm;

    my $file_alarm = $file_alarms->{$db_alarm->{FAULT_CODE}};

    # Check whether alarm is in file

    if(defined($file_alarm)) # Alarm is in file
    {
        # Compare database and file alarm definitions

        for my $field (keys(%$db_alarm))
        {
            next if($field eq 'ID' || $field eq 'HELP_URL'); # Ignore database record ID and help URL

            if(!defined($db_alarm->{$field}) || $db_alarm->{$field} ne $file_alarm->{$field})
            {
                warn "$field has changed for alarm $db_alarm->{FAULT_CODE}\n";

                # Update alarm in the database

                print "Updating alarm [$file_alarm->{PROBLEM}] in database...\n";

                FGC::ControlsDB::update_class_alarm($dbh,
                                                    $class,
                                                    $file_alarm->{FAULT_CODE},
                                                    $file_alarm->{PROBLEM},
                                                    $file_alarm->{PRIORITY},
                                                    $file_alarm->{CAUSE},
                                                    $file_alarm->{CONSEQUENCE},
                                                    $file_alarm->{ACTION},
                                                    $file_alarm->{SYMBOL});
            }
        }
    }
    else # Alarm is not in file
    {
        # Remove alarm from database

        print "Removing alarm [$db_alarm->{PROBLEM}] from database...\n";

        FGC::ControlsDB::remove_class_alarm($dbh, $class, $db_alarm->{FAULT_CODE});
    }
}

# Check whether each alarm in the file is in the database

for my $file_alarm (values(%$file_alarms))
{
    if(!defined($db_alarms{$file_alarm->{FAULT_CODE}})) # Alarm is not in the database
    {
        # Add alarm to database

        print "Adding alarm [$file_alarm->{PROBLEM}] to database...\n";

        FGC::ControlsDB::add_class_alarm($dbh,
                                         $class,
                                         $file_alarm->{FAULT_CODE},
                                         $file_alarm->{PROBLEM},
                                         $file_alarm->{PRIORITY},
                                         $file_alarm->{CAUSE},
                                         $file_alarm->{CONSEQUENCE},
                                         $file_alarm->{ACTION},
                                         $file_alarm->{SYMBOL});
    }
}

# Disconnect from database

FGC::ControlsDB::disconnect($dbh);

# EOF
