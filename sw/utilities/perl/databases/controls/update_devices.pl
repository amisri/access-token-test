#!/usr/bin/perl -w
#
# Name:     update_devices.pl
# Purpose:  Update Controls Database (CCDB) with details of FGC devices
# Author:   Stephen Page

use Getopt::Long;
use FGC::ControlsDB;
use FGC::Names;
use File::Basename;
use strict;
use Data::Dumper;

# Check if running in simulation mode or if commiting changes to DB

my ($commit);
GetOptions('commit'=>\$commit);

die "Usage: \n$0 [--commit] \n--commit: Commit changes to CCDB.\n" if(@ARGV != 0);

my $simulation = !defined($commit);
my $simulation_info = "";

if($simulation)
{
    $simulation_info = "SIMULATION MODE: ";
    print "\n-------------------------------------------------------------------------------------\n";
    print "${simulation_info}\nThe script will print its actions, but it will not modify CCDB.\nTo propagate the changes to CCDB, run the script with the --commit option.";
    print "\n-------------------------------------------------------------------------------------\n\n";
}
else
{
    print "\n----------------------------------------------------------------------------------------------------------\n";
    print "WARNING:\nChanges will be propagated to CCDB.\nTo run the script in simulation mode, cancel the execution and run the script without the --commit option.";
    print "\n----------------------------------------------------------------------------------------------------------\n\n";
}

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read names from FGC name file

my ($epc_devices, $epc_gateways) = FGC::Names::read() or die "Error reading FGC names\n";
die "Unable to read FGC name file\n" if(!defined($epc_devices));

FGC::Names::read_sub($epc_devices, $epc_gateways);

# Read device names from the FGC keep file - See EPCCCS-8803
# These devices are kept in the databases but hidden in expert applications as they are subject to be repaired in the future

my ($keep_devices, $keep_gateways) = FGC::Names::read_keep() or die "Error reading FGC keep file\n";

FGC::Names::read_sub_keep($keep_devices, $keep_gateways);


# Function to elevate sub-devices (except for sub-device 0) to devices as they are equivalent in the database
sub elevate_sub_devices($$$)
{
    my ($sub_files_base_dir, $parsed_devices, $parsed_gateways) = @_;

    # Read sub-device file names (gw names)
    chdir($sub_files_base_dir);
    for my $gateway_name (map { basename $_ } <*>)
    {
        # Elevate sub-devices (except for sub-device 0) to devices as they are equivalent in the database

        for my $device (@{$parsed_gateways->{$gateway_name}->{channels}})
        {
            next if(!defined($device));

            if(defined($device->{sub_devices}))
            {
                # Elevate other sub-devices to devices. Start at index 1 to exclude sub-device 0
                # (which is just an alias for a parent device and already included in the devices hash)

                for(my $i = 1 ; $i < @{$device->{sub_devices}} ; $i++)
                {
                    my $sub_device = $device->{sub_devices}->[$i];
                    next if(!defined($sub_device));

                    $sub_device->{class} = $device->{class};
                    $sub_device->{gateway} = $device->{gateway};

                    # Elevate sub_device to device (i.e. treat them equally)
                    $parsed_devices->{$sub_device->{name}} = $sub_device;
                }
            }
        }
    }
}

elevate_sub_devices(FGC_SUB_NAME_FILE_BASE, $epc_devices, $epc_gateways);
elevate_sub_devices(FGC_SUB_KEEP_FILE_BASE, $keep_devices, $keep_gateways);

# Connect to database

my $dbh = FGC::ControlsDB::connect($username, $password)
            or die "Unable to connect to CCDB: $!\n";

# Get devices

my $ccdb_device_rows = FGC::ControlsDB::get_all_devices($dbh)
                        or die "Failed to get devices from CCDB\n";

# Check that each device in the database is consistent with its entry in the name file

my %ccdb_devices;
for my $ccdb_device (@$ccdb_device_rows)
{
    # We identify CCDB devices by their alias field instead of their device name field.
    # Reasoning:
    # In CCDB, our devices are stored with the operational names (epc aliases) as main device name, because OP does not care about our EPC internal long device names
    # Hence, we store the EPC internal names in the CCDB 'alias' field, and use our EPC aliases as CCDB device name.
    # https://wikis.cern.ch/display/TEEPCCCS/Converter+names%3A+Aliases+vs+full+names
    # https://issues.cern.ch/browse/EPCCCS-9412

    my $epc_device = $epc_devices->{$ccdb_device->{ALIAS}};

    # Check whether device is in name file

    if(defined($epc_device)) # Device is in name file
    {
        my $epc_gateway    = $epc_device->{gateway};
        my $server_name     = "FGC_\U$epc_gateway->{name}";

        # Check whether device attributes have changed

        if($ccdb_device->{DEVICENAME} ne $epc_device->{alias} ||  # EPC Alias has changed
           $ccdb_device->{CLASSNAME} ne "FGC_$epc_device->{class}" ||  # Class has changed
           $ccdb_device->{FECNAME} ne $epc_gateway->{name} ||  # Gateway has changed
           $ccdb_device->{SERVERNAME} ne $server_name ||  # Gateway server name has changed
           $ccdb_device->{FGC_FIELDBUS_ADDRESS} ne $epc_device->{fieldbus_address}  # Fieldbus address has changed
          )
        {
            # Update the device entry
            print "${simulation_info}Updating device $ccdb_device->{ALIAS} in database...\n";

            printf("FROM: NAME %s, ALIAS %s, CLASS %s, GATEWAY %s, SERVER %s, FGC_FIELDBUS_ADDRESS %s\n",
                   $ccdb_device->{DEVICENAME}, # equals EPC alias
                   $ccdb_device->{ALIAS}, # equals EPC device name
                   $ccdb_device->{CLASSNAME},
                   $ccdb_device->{FECNAME},
                   $ccdb_device->{SERVERNAME},
                   $ccdb_device->{FGC_FIELDBUS_ADDRESS});

            printf("TO:   NAME %s, ALIAS %s, CLASS %s, GATEWAY %s, SERVER %s, FGC_FIELDBUS_ADDRESS %s\n",
                   $epc_device->{alias}, # equals CCDB device name
                   $epc_device->{name}, # equals CCDB Alias
                   "FGC_$epc_device->{class}",
                   $epc_gateway->{name},
                   $server_name,
                   $epc_device->{fieldbus_address});

            unless ($simulation)
            {
                FGC::ControlsDB::update_device( $dbh,
                                                $epc_device->{name},
                                                $epc_device->{alias},
                                                $epc_device->{class},
                                                $epc_gateway->{name},
                                                $server_name,
                                                $epc_device->{fieldbus_address});
            }
        }
    }
    else # Device is not in name file
    {
        my $keep_device = $keep_devices->{$ccdb_device->{ALIAS}};

        if(defined($keep_device)) # Device is in keep file
        {
            # Keep device

            print "${simulation_info}Not removing device $ccdb_device->{ALIAS} ($ccdb_device->{DEVICENAME}) from CCDB as it is defined in the keep file...\n";
        }
        else # Device is neither in name nor in keep file
        {
            # Remove device

            print "${simulation_info}Removing device $ccdb_device->{ALIAS} ($ccdb_device->{DEVICENAME}) from CCDB...\n";

            unless ($simulation)
            {
                FGC::ControlsDB::remove_device($dbh, $ccdb_device->{ALIAS})
                    or die "Unable to remove device $ccdb_device->{ALIAS} ($ccdb_device->{DEVICENAME}) from CCDB: $!\n";
            }
        }
    }

    # For next step: Make DB devices gettable via CCDB device name (which is equal to EPC alias)
    $ccdb_devices{$ccdb_device->{DEVICENAME}} = $ccdb_device;
}

# Check whether each device in the FGC name file is in the database

for my $epc_device (values(%$epc_devices))
{
    if(!defined($ccdb_devices{$epc_device->{alias}})) # Device is not in the database
    {
        my $epc_gateway = $epc_device->{gateway};
        my $server_name = "FGC_\U$epc_gateway->{name}";

        # Add device to database

        print "${simulation_info}Adding device $epc_device->{name} ($epc_device->{alias}) to CCDB...\n";

        unless ($simulation)
        {
            FGC::ControlsDB::add_device($dbh,
                                        $epc_device->{name},
                                        $epc_device->{alias},
                                        $epc_device->{class},
                                        $epc_gateway->{name},
                                        $server_name,
                                        $epc_device->{fieldbus_address})
                or die "Failed to add device $epc_device->{name} to CCDB: $!\n";
        }
    }
}

# Disconnect from database

FGC::ControlsDB::disconnect($dbh);

# EOF
