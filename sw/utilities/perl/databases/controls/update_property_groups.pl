#!/usr/bin/perl -w
#
# Name:     update_property_groups.pl
# Purpose:  Update Controls DB with latest device groups
# Author:   Stephen Page

use Getopt::Long;
use FGC::ControlsDB;
use FGC::Properties;
use File::Basename;
use strict;

my %groups;
my %properties;

# Begin functions

sub readgroups($)
{
    my ($class) = @_;

    # Add subscription property to %fgc_properties

    $fgc_properties{"SUB_$class"} = {
                                        classes     => { $class => {maxels => 1, type => 'PARENT'} },
                                        group       => 'OPERATIONAL',
                                        name        => "SUB_$class",
                                        subscribe   => 1,
                                    };

    $fgc_properties{"SUB"}        = {
                                        classes     => { $class => {maxels => 1, type => 'PARENT'} },
                                        group       => 'OPERATIONAL',
                                        name        => "SUB",
                                        subscribe   => 1,
                                    };

    # Hack to keep class 51 and 91 backward compatible with respect to class 92 and 94, respectively

    if($class == 92)
    {
          $fgc_properties{"SUB_51"} = {
                                              classes     => { $class => {maxels => 1, type => 'PARENT'} },
                                              group       => 'OPERATIONAL',
                                              name        => "SUB_51",
                                              subscribe   => 1,
                                          };


    }

    if($class == 94)
    {
          $fgc_properties{"SUB_91"} = {
                                              classes     => { $class => {maxels => 1, type => 'PARENT'} },
                                              group       => 'OPERATIONAL',
                                              name        => "SUB_91",
                                              subscribe   => 1,
                                          };


    }

    for my $property (values(%fgc_properties))
    {
        next if(!defined($property->{classes}->{$class}));

        $groups{$class}->{$property->{group}}->{name}                           = $property->{group};
        $groups{$class}->{$property->{group}}->{members}->{$property->{name}}   = 1;
        $properties{$class}->{$property->{name}}                                = 1;
    }
}

# End of functions

my ($commit);
GetOptions('commit'=>\$commit);

die "Usage: \n$0 <class> [--commit] \n--commit: Commit changes to CCDB.\n" if(@ARGV != 1);

my ($requested_class) = @ARGV;

# Enable simulation mode by default (i.e. if no commit flag has been supplied).

my $simulation = !defined($commit);
my $simulation_info = "";

if($simulation)
{
    $simulation_info = "SIMULATION MODE: ";
    print "\n-------------------------------------------------------------------------------------\n";
    print "${simulation_info}\nThe script will print its actions, but it will not modify CCDB.\nTo propagate the changes to CCDB, run the script with the --commit option.";
    print "\n-------------------------------------------------------------------------------------\n\n";

}
else
{
    print "\n----------------------------------------------------------------------------------------------------------\n";
    print "WARNING:\nChanges will be propagated to CCDB.\nTo run the script in simulation mode, cancel the execution and run the script without the --commit option.";
    print "\n----------------------------------------------------------------------------------------------------------\n\n";
}

# Print an informative message

print "Updating groups for configuration properties in CCDB for class $requested_class...\n";

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";


# Read latest groups

readgroups($requested_class);

# Connect to database

my $dbh = FGC::ControlsDB::connect($username, $password)
            or die "Unable to connect to database: $!\n";

for my $class (sort(keys(%groups)))
{
    my $class_groups        = $groups{$class};
    my $class_properties    = $properties{$class};

    # Get properties from database

    print "Getting properties for class $class from database... ";
    my $db_properties = FGC::ControlsDB::get_properties($dbh, $class);
    print scalar(@$db_properties), " found.\n";

    # Delete groups that do not exist in file

    my %db_properties;
    for my $db_property (@$db_properties)
    {
        if(!defined($class_properties->{$db_property->{PROPERTY_NAME}}))
        {
            print "\tDeleting property $db_property->{PROPERTY_NAME}...\n";
            unless ($simulation)
            {
                FGC::ControlsDB::remove_property($dbh, $class, $db_property->{PROPERTY_NAME});
            }
        }
        else
        {
            $db_properties{$db_property->{PROPERTY_NAME}} = 1;
        }
    }

    # Add properties from file

    for my $property (sort(keys(%$class_properties)))
    {
        # Create property in database if it does not exist

        if(!defined($db_properties{$property}))
        {
            print "\tCreating property $property...\n";
            unless ($simulation)
            {
                FGC::ControlsDB::add_property($dbh, $class, $property);
            }
        }
    }

    # Get groups from database

    print "Getting groups for class $class from database... ";
    my $db_groups = FGC::ControlsDB::get_property_groups($dbh, $class);
    print scalar(@$db_groups), " found.\n";

    # Delete groups that do not exist in files

    my %db_groups;
    for my $db_group (@$db_groups)
    {
        if(!defined($class_groups->{$db_group->{PROPERTYGROUP_NAME}}))
        {
            print "\tWARNING: Property Group '$db_group->{PROPERTYGROUP_NAME}' found in DB but not used by class $class. Consider deleting it from this class via CCDE interface. \n";
        }
        else
        {
            my $members             = FGC::ControlsDB::get_property_group_members($dbh, $class, $db_group->{PROPERTYGROUP_NAME});
            $db_group->{members}    = {map { $_->{PROPERTY_NAME} => 1 } @$members};
            $db_groups{$db_group->{PROPERTYGROUP_NAME}} = $db_group;
        }
    }

    # Add and update groups from files

    for my $group (sort(values(%$class_groups)))
    {
        # Create group in database if it does not exist

        if(!defined($db_groups{$group->{name}}))
        {
            print "\tCreating group $group->{name}...\n";
            unless ($simulation)
            {
                FGC::ControlsDB::add_property_group($dbh, $class, $group->{name});
            }
            $db_groups{$group->{name}}->{members} = {};
        }
        else
        {
            my $db_group = $db_groups{$group->{name}};

            # Check for members that don't exist in files

            print "\tChecking for members to remove from group $group->{name}...\n";

            for my $member (sort(keys(%{$db_group->{members}})))
            {
                if(!defined($group->{members}->{$member}))
                {
                    print "\t\tRemoving $member from group $group->{name}...\n";
                    unless ($simulation)
                    {
                        FGC::ControlsDB::remove_property_from_group($dbh, $class, $group->{name}, $member);
                    }
                }
            }
        }

        my $db_group = $db_groups{$group->{name}};

        # Add new members to group

        print "\tChecking for members to add to group $group->{name}...\n";

        for my $member (sort(keys(%{$group->{members}})))
        {
            if(!defined($db_group->{members}->{$member}))
            {
                print "\t\tAdding $member to group $group->{name}...\n";
                unless ($simulation)
                {
                    FGC::ControlsDB::add_property_to_group($dbh, $class, $group->{name}, $member);
                }
            }
        }
    }
    print "\n";
}
FGC::ControlsDB::disconnect($dbh);

# EOF
