#!/usr/bin/perl -w
#
# Name swap migration helper to propagate EPC subdevice naming convention to CCDB

use Getopt::Long;
use FGC::ControlsDB;
use FGC::Names;
use File::Basename;
use strict;
use Data::Dumper;

# Check if running in simulation mode or if commiting changes to DB

my ($commit);
GetOptions('commit'=>\$commit);

die "Usage: \n$0 [--commit] \n--commit: Commit changes to CCDB.\n" if(@ARGV != 0);

my $simulation = !defined($commit);
my $simulation_info = "";

if($simulation)
{
    $simulation_info = "SIMULATION MODE: ";
    print "\n-------------------------------------------------------------------------------------\n";
    print "${simulation_info}\nThe script will print its actions, but it will not modify CCDB.\nTo propagate the changes to CCDB, run the script with the --commit option.";
    print "\n-------------------------------------------------------------------------------------\n\n";
}
else
{
    print "\n----------------------------------------------------------------------------------------------------------\n";
    print "WARNING:\nChanges will be propagated to CCDB.\nTo run the script in simulation mode, cancel the execution and run the script without the --commit option.";
    print "\n----------------------------------------------------------------------------------------------------------\n\n";
}

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);

system("stty echo");
print "\n\n";

# Read names from FGC name file

my ($epc_devices, $epc_gateways) = FGC::Names::read() or die "Error reading FGC names\n";
die "Unable to read FGC name file\n" if(!defined($epc_devices));

FGC::Names::read_sub($epc_devices, $epc_gateways);

sub gather_sub_devices($$)
{
    my ($sub_files_base_dir, $parsed_gateways) = @_;

    # Gather sub-devices (except for sub-device 0) in a hash to proceed with alias update

    my $sub_devices = {};

    # Read sub-device file names (gw names)
    chdir($sub_files_base_dir);
    for my $gateway_name (map { basename $_ } <*>)
    {
        for my $parent_device (@{$parsed_gateways->{$gateway_name}->{channels}})
        {
            next if(!defined($parent_device));

            if(defined($parent_device->{sub_devices}))
            {
                # Start gathering at index 1 and exclude sub-device at index 0
                # (which is just an alias for the parent device, treated in update_devices.pl)

                for(my $i = 1 ; $i < @{$parent_device->{sub_devices}} ; $i++)
                {
                    my $sub_device = $parent_device->{sub_devices}->[$i];
                    next if(!defined($sub_device));
                    $sub_devices->{$sub_device->{name}} = $sub_device;
                }
            }
        }
    }

    return $sub_devices;
}

my $sub_devices = gather_sub_devices(FGC_SUB_NAME_FILE_BASE, $epc_gateways);

# Connect to database

my $dbh = FGC::ControlsDB::connect($username, $password)
            or die "Unable to connect to CCDB: $!\n";

# Get devices from CCDB

my $ccdb_device_rows = FGC::ControlsDB::get_all_devices($dbh)
                        or die "Failed to get devices from CCDB\n";

my %ccdb_devices;
for my $ccdb_device (@$ccdb_device_rows)
{
    $ccdb_devices{$ccdb_device->{DEVICENAME}} = $ccdb_device;
}

# Check whether each EPC sub device from sub device files is in the database and make sure its alias is up to date

for my $sub_device (values(%$sub_devices))
{
    # We identify CCDB devices by the EPC alias field instead of the EPC device name.
    # Reasoning:
    # In CCDB, our devices are stored with the operational names (epc aliases) as main device name, because OP does not care about our EPC internal long device names
    # Hence, we store the EPC internal names in the CCDB 'alias' field, and use our EPC aliases as CCDB device name.
    # https://wikis.cern.ch/display/TEEPCCCS/Converter+names%3A+Aliases+vs+full+names
    # https://issues.cern.ch/browse/EPCCCS-9412

    my $ccdb_device = $ccdb_devices{$sub_device->{alias}};
    if(!defined($ccdb_device))
    {
        # Sub device is not in the database, so add it.

        print "${simulation_info}Adding sub device $sub_device->{alias} ($sub_device->{name}) to CCDB...\n";

        unless ($simulation)
        {
            my $gateway_name = $sub_device->{gateway}->{name};
            my $server_name = "FGC_\U$gateway_name";

            FGC::ControlsDB::add_device($dbh,
                                        $sub_device->{name},
                                        $sub_device->{alias},
                                        $sub_device->{class},
                                        $gateway_name,
                                        $server_name,
                                        $sub_device->{fieldbus_address})
            or die "Failed to add sub device $sub_device->{name} to CCDB: $!\n";
        }
    }
    else
    {
        # Sub device is defined in database.

        if(defined($ccdb_device->{ALIAS}) && $ccdb_device->{ALIAS} eq $sub_device->{name})  {
            next;
        }

        # CCDB alias field is not up to date. Update it to match EPC convention

        print "\n${simulation_info}Updating sub device $ccdb_device->{DEVICENAME} in database...\n";
        printf("${simulation_info}FROM: ALIAS %s\n", $ccdb_device->{ALIAS} || ''); # equals EPC name
        printf("${simulation_info}TO:   ALIAS %s\n", $sub_device->{name}); # equals CCDB Alias

        unless ($simulation)
        {
            FGC::ControlsDB::update_device_alias($dbh,
                                                 $ccdb_device->{DEVICENAME},
                                                 $sub_device->{name})
            or die "Failed to update sub device alias of $ccdb_device->{DEVICENAME} ($sub_device->{name}) to CCDB: $!\n";
        }
    }
}

# Disconnect from database

FGC::ControlsDB::disconnect($dbh);

# EOF
