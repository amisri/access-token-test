#!/usr/bin/perl -w
#
# Name:        update_component_barcode_properties.pl
# Purpose:     Import the barcode properties of FGC components
# Description: Updates all the empty component properties called %BARCODE% and sets their value with the
#              barcode of the corresponding component.
#
# Author:      Miguel Hermo

use strict;

use FGC::DB;
use Getopt::Long;
use JSON;

my $username = '';
my $password = '';

my $db_config_file = undef;

# Read command-line options

GetOptions('dbconfig:s'=>\$db_config_file);

if(defined($db_config_file))
{
    # Read username/password from file
    my $json_text = do 
    {
       open(my $json_fh, "< $db_config_file")  or die("Can't open $db_config_file". $!."\n");
       local $/;
       <$json_fh>
    };
    
    eval
    {
        my $config=decode_json($json_text);
        $username = $config->{db_username};
        $password = $config->{db_password};
    }
    or do
    {
        die "ERROR: can't read JSON configuration:\n$@!\n";
    };
}
else
{
    # Prompt for username

    print "\nUsername: ";
    chomp($username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp($password = <STDIN>);
    system("stty echo");
    print "\n\n";
}

# Read properties

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n" ;

my $affected=FGC::DB::update_component_barcode_properties($dbh) or die "Could not update the components' barcode properties" ;

if($affected eq '0E0')
{
    print "No barcode properties were updated" ;
}
else
{
    print "Updated $affected %BARCODE% properties\n" ;
}   

FGC::DB::disconnect($dbh);

print "\nDONE\n";

# EOF
