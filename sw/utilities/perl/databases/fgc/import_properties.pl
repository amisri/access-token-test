#!/usr/bin/perl -w
#
# Name:     import_properties.pl
# Purpose:  Import properties into FGC database
# Author:   Stephen Page

use strict;

use FGC::DB;

sub read_properties($)
{
    my ($filename) = @_;

    open(FILE, "<", $filename) or die "Unable to open $filename: $!\n";

    my %properties;

    my ($usage, $property_name, $category);
    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline

        ($usage, $property_name, $category) = split(",", $_);

        $properties{$property_name}->{name}     = $property_name;
        $properties{$property_name}->{usage}    = $usage;
        $properties{$property_name}->{category} = $category || "";
    }
    close(FILE);

    return(\%properties);
}

# End of functions


die "Usage: $0 <filename>\n" if(@ARGV != 1);
my ($filename) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read properties

my $properties = read_properties($filename);

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

for my $property (sort {$a->{name} cmp $b->{name}} values(%$properties))
{
    my $result = FGC::DB::add_property($dbh, $property->{name});    

    if(!defined($result))
    {
        FGC::DB::disconnect($dbh);
        die "ERROR: Unable to add property $property->{name}\n";
    } 
    elsif ($result != 0)
    {
        print "Property $property->{name} added\n";
    }
}
FGC::DB::disconnect($dbh);

print "\nDONE\n";

# EOF
