#!/usr/bin/perl -w
#
# Purpose:  Adds to components.xml the component types for which barcode/id associations have been added on the DB
# Author:   Miguel Hermo

use strict;

use FGC::Consts;
use FGC::DB;
use XML::Simple;

# Removes characters not recognized by fgc
sub encode($)
{
    my($txt) = @_;

    $txt =~ s/,\s*/ /g;                          # Replace commas followed by spaces with a single space
    $txt =~ s/&/+/g;                             # Replace ampersands with pluses
    $txt =~ s/"//g;                              # Remove quotes
    $txt =~ s/\261/+-/g;                         # Replace the single +- character with a pair
    $txt =~ s/[\340\341\342\343\344\345]/a/g;    # Remove accents from 'a'
    $txt =~ s/[\350\351\352\353]/e/g;            # Remove accents from 'e'
    $txt =~ s/[\354\355\356\357]/i/g;            # Remove accents from 'i'
    $txt =~ s/[\362\363\364\365\366]/o/g;        # Remove accents from 'o'
    $txt =~ s/[\371\372\373\374]/u/g;            # Remove accents from 'u'

    $txt =~ s/[\200-\377]//g;                    # Remove all remaining extended ASCII characters
    $txt =~ s/\s+/ /g;                           # Replace multiple spaces with a single one
    return $txt;
}

# Get the types of components for which there are barcodes in the database

sub get_types()
{
    # Connect to database

    my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD) or die "Unable to connect to database: $!\n";

    # Prepare database query

    my $query;
    if(!($query = $dbh->prepare("SELECT TP_NAME, TP_DESCRIPTION, TP_DALLAS_ID, TP_MULTIPLE_IDS, TP_BARCODE_PROP, TP_TEMP_PROP, LISTAGG(lower(PLA_NAME), ',') AS PLA_NAME FROM POCONTROLS.FGC_TYPES JOIN POCONTROLS.FGC_PLATFORM_TYPES ON TP_ID=PLT_TP_ID JOIN POCONTROLS.FGC_PLATFORMS ON PLT_PLA_ID=PLA_ID GROUP BY TP_NAME, TP_DESCRIPTION, TP_DALLAS_ID, TP_MULTIPLE_IDS, TP_BARCODE_PROP, TP_TEMP_PROP ORDER BY TP_NAME")))
    {
        FGC::DB::disconnect($dbh);
        print $dbh->err;
        die "Failed to prepare database query\n";
    }

    # Execute database query

    if(!$query->execute())
    {
        FGC::DB::disconnect($dbh);
        die "Failed to execute database query\n";
    }

    # Retrieve results

    my $db_results = $query->fetchall_arrayref({});

    FGC::DB::disconnect($dbh);

    my @types = @$db_results;

    return \@types;
}

sub components_to_xml($)
{
    my ($components) = @_;

    my $xml = "<?xml version=\"1.0\"?>\n\n";
    $xml .= "<!-- WARNING:\nThis file is generated automatically. Do not edit it manually, your changes will be lost.\nPlease use the Property Manager to apply any changes to component types of the components.xml!\nNote: components.xml only includes component types that have a platform associated in the Property Manager\n-->\n\n";
    $xml .= "<!-- Components list -->\n\n";
    $xml .= "<components>\n\n";
    for my $type (sort keys %$components)
    {
        $xml .= "    <!-- ".encode($components->{$type}->{description})." -->\n";
        $xml .= "    <component ";
        for my $property (sort keys %{$components->{$type}})
        {
            # ignore the description
            if ($property eq "description")
            {
                next;
            }
            $xml .= "$property=\"$components->{$type}->{$property}\"\n";
            $xml .= "               "; #add space to align properties
        }
        $xml = substr($xml, 0, -11); #remove last space so that closing tag is aligned with opening tag
        $xml .= "/>\n\n";
    }
    $xml.="</components>\n\n";
    $xml.="<!-- EOF -->";
    return $xml;
}

die "Usage: $0 <path_to_components_xml>\n" if(@ARGV != 1);
my ($filename) = @ARGV;

my $types = get_types();

print "creating components.xml\n";

my $components;
for my $type (@$types)
{

    my $component;
    $component->{barcode} = $type->{TP_NAME};

    $component->{description} = $type->{TP_DESCRIPTION};

    if ($type->{TP_DALLAS_ID} == 1) {
        $component->{dallas_id} = "yes";
    }

    if (($type->{TP_DALLAS_ID} == 1) && (defined $type->{TP_MULTIPLE_IDS}) && ($type->{TP_MULTIPLE_IDS} ne '') && ($type->{TP_MULTIPLE_IDS} > 1))
    {
        $component->{multiple_ids} = $type->{TP_MULTIPLE_IDS};
    }

    my $platform = $type->{PLA_NAME};
    $platform =~ s/fgcdeqp/fgclite/g;
    if (scalar split(',', $platform) < 3)
    {
        $component->{platform} = $platform;
    }

    if (defined $type->{TP_BARCODE_PROP})
    {
        $component->{barcode_prop} = $type->{TP_BARCODE_PROP};
    }
    if (defined $type->{TP_TEMP_PROP})
    {
        $component->{temp_prop} = $type->{TP_TEMP_PROP};
    }

    $components->{$type->{TP_NAME}} = $component;
}

# Write components.xml
open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";

print $fh components_to_xml($components);
