#!/usr/bin/perl -w
#
# Name:     set_type_props.pl
# Purpose:  Set type properties in database to values read from file
# Author:   Stephen Page

use strict;

use FGC::Consts;
use FGC::DB;

die "Usage: $0 <property> <file name>\n" if(@ARGV != 2);
my ($property, $filename) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

my %values;

open(FILE, '<', $filename) or die "Error opening file $filename: $!\n";
while(<FILE>)
{
    s/[\012\015]//g; # Remove newline

    my ($system, @value)    = split(',', $_);
    $values{$system}        = join(',', @value);
}

# Connect to database

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

for my $system (sort(keys(%values)))
{
    my $value = $values{$system};

    print "Setting $property=$value for system $system...\n";
    FGC::DB::update_system_property_values($dbh, $system, { $property => $value });
}
FGC::DB::disconnect($dbh);

# EOF
