#!/usr/bin/perl -w
#
# Name:     check_config.pl
# Purpose:  Ensure consistency between FGC_Properties.csv <---> XML CONFIG properties (FGC::Properties)
# Author:   Kevin Kessler

use strict;
use warnings;

use File::Slurp qw(read_file);
use FGC::Properties; # provides access to %fgc_properties which stores properties parsed from xml.
use FGC::Components; # provides access to %fgc_components which stores components parsed from xml.
use FGC::DB;
use List::MoreUtils qw(uniq);
use Term::ANSIColor;
use Getopt::Long;

# Array of classes for which the consistency of properties should be ignored
use constant DISCONTINUED_CLASSES => qw/ 52 /;

# Array of regexes describing properties for which the consistency should not be checked

use constant PROPERTIES_TO_IGNORE => qw/ BARCODE. /;

# Array of symbols to ignore when checking symlists, since they are aliases to other symbols with the same value
use constant SYMBOL_ALIASES => qw / OF SA SB IL CY BK EC DT DAC_A /;

# check symlists xml
sub check_symlists_xml()
{
    print "Checking Symlists for inconsistencies based on Properties.pm...\n";

    # Check that XML properties use valid symlists
    for my $property_name (sort(keys(%fgc_properties)))
    {
        # Do not check properties to ignore
        next if(array_element_matches($property_name, PROPERTIES_TO_IGNORE));

        # Skip non SYMLIST properties
        my $is_symlist = 0;
        for my $class (keys(%{$fgc_properties{$property_name}->{classes}}))
        {
            if(defined($fgc_properties{$property_name}->{classes}->{$class}->{flags}->{SYM_LST}))
            {
                $is_symlist = 1;
                last;
            }
        }
        next if !$is_symlist;

        if(!keys(%{$fgc_properties{$property_name}->{class_symbols}}))
        {
            report_error("ERROR: Detected SYM_LST flag but found no associated symlist on property '$property_name'");
        }

        # check class specifc symlist values for this property
        for my $sym_class (sort(keys(%{$fgc_properties{$property_name}->{class_symbols}})))
        {
            my $class_bitmask = $fgc_properties{$property_name}->{classes}->{$sym_class}->{flags}->{BIT_MASK};
            $class_bitmask = 0 if(!defined($class_bitmask));
            check_symlist_values($property_name, $class_bitmask, $fgc_properties{$property_name}->{class_symbols}->{$sym_class});
        }
    }
}

# checks the values of a given symlist for proper format and duplicates
sub check_symlist_values($$$)
{
    my ($property_name, $is_bitmask, $symlist) = @_;

    # keep track of all symlist values that are used by the same class and property
    my $class_to_values = {};

    for my $symbol (sort(keys(%{$symlist})))
    {
        # Do not check symbols to ignore. e.g. we don't want to check the symbols that have aliases (since they use the same value)
        next if(array_element_matches($symbol, SYMBOL_ALIASES));

        for my $class (sort(keys(%{$symlist->{$symbol}->{values}})))
        {
            # init hash for values of that class. keys will be the values, for easy duplicates check
            if(!defined($class_to_values->{$class}))
            {
                $class_to_values->{$class} = {};
            }

            my $current_value = $symlist->{$symbol}->{values}->{$class};

            # check that the value is a whole number
            if($current_value !~ /^[+-]?\d+$/)
            {
                # TODO: in case the value is not a whole number, we might want to check whether it is a string that corresponds to a variable name of a known FGC constant.
                # This could be checked by parsing sw/inc/classes/*/defconsts.h
                report_error("Invalid value for symbol '$symbol' ($current_value)! Must either be integer, bitmask or a known FGC constant.");
            }

            # in case of bitmask, check if value is a power of two or if symbol is NONE in case of zero bitmask
            my $is_power_of_two = !( $current_value & ($current_value-1) ) && ($current_value != 0);
            my $is_none_symbol = $current_value == 0 && $symbol eq 'NONE';
            if($is_bitmask && !$is_power_of_two && !$is_none_symbol)
            {
                report_error("Property '$property_name' is marked as BIT_MASK, but the value for symbol '$symbol' (class $class) is neither a power of two nor a zero bitmask ($current_value)! Zero bitmasks may only be used for NONE symbols.");
            }

            # check if value has been used as key already -> duplicate
            if(defined($class_to_values->{$class}->{$current_value}))
            {
                #TODO: Maintaining the SYMBOL_ALIASES array can be error prone and cause overhead. Best practice would be to redesign the alias mechanism e.g. by using an alias attribute in XML
                my $used_symbol = $class_to_values->{$class}->{$current_value};
                report_error("Duplicated value ($current_value) for symbols ($symbol vs $used_symbol) used by the same property ($property_name) and class ($class)!\n".
                             "HINT : If $symbol is an alias for $used_symbol, consider to add it to the SYMBOL_ALIASES array in check_config.pl");
                next;
            }

            $class_to_values->{$class}->{$current_value} = $symbol;
        }
    }
}

# $filename should contain the path to FGC_properties.csv
sub parse_properties_csv($)
{
    my ($filename) = @_;

    # Store system properties by class/category parsed from the given csv file.
    my %sys_props_per_class;
    my %cmp_props_per_cat;
    my %type_props_per_cat;

    # Additionally store all properties and categories in one array for convenience
    my @all_props = ();
    my @all_cats = ();

    # Parse FGC_Properties.csv
    my $file_content = read_file($filename) or die(colored("ERROR: Unable to read $filename: $!", "red")."\n");
    my @rows = split("\n", $file_content);
    for(my $i=0; $i<scalar(@rows); $i++)
    {
        my $row = $rows[$i];

        # Validate format. Ensure we have a table with 3 comma separated columns
        if($row !~ /^.+,.+,.+$/)
        {
            report_error("File must be a table with 3 comma separated columns");
        }

        # Check for duplicate rows
        if(grep(/^$row$/, @rows[$i+1 .. scalar(@rows)-1]))
        {
            report_error("Duplicate row detected in $filename: $row");
        }

        # Extract property name from current row
        my ($group, $name, $category) = split(",", $row);

        # Skip "Table Header" row
        if($name eq "Property Name")
        {
            next;
        }

        #determine to which hash the property needs to be added
        my $hashref = undef;
        $hashref = \%sys_props_per_class if($group eq "SYS");
        $hashref = \%cmp_props_per_cat if($group eq "CMP");
        $hashref = \%type_props_per_cat if($group eq "TYPE");
        next if(!defined($hashref));

        # add to specific sys,cmp,type list
        $hashref->{$category} = [] if(!defined($hashref->{$category}));
        push(@{$hashref->{$category}}, $name);
    }

    # Transform .?. props to .A/B. props and populate the all_properties list
    for my $class (keys(%sys_props_per_class))
    {
        my @transformed_props = sort(transform_questionmark_props(@{$sys_props_per_class{$class}}));
        $sys_props_per_class{$class} = \@transformed_props;
        push(@all_props, @transformed_props);
    }
    for my $cat (keys(%cmp_props_per_cat))
    {
        my @transformed_props = sort(transform_questionmark_props(@{$cmp_props_per_cat{$cat}}));
        $cmp_props_per_cat{$cat} = \@transformed_props;
        push(@all_props, @transformed_props);
    }
    for my $cat (keys(%type_props_per_cat))
    {
        my @transformed_props = sort(transform_questionmark_props(@{$type_props_per_cat{$cat}}));
        $type_props_per_cat{$cat} = \@transformed_props;
        push(@all_props, @transformed_props);
    }

    # Remove duplicates from all_properties list
    @all_props = sort(uniq(@all_props));

    # Get a list of all property categories by merging Type and CMP categories without duplicates
    @all_cats = (keys(%cmp_props_per_cat), keys(%type_props_per_cat));
    @all_cats = sort(uniq(@all_cats));

    my $result_ref = {
        sys_props_per_class => \%sys_props_per_class,
        cmp_props_per_cat => \%cmp_props_per_cat,
        type_props_per_cat => \%type_props_per_cat,
        all_props => \@all_props,
        all_cats => \@all_cats,
    };

    return $result_ref;
}

# Check consistency between CSV properties and Properties.pm
sub check_csv_xml_props($)
{
    my ($csv_properties_ref) = @_;
    my @csv_properties = sort(@$csv_properties_ref);

    print "Checking for inconsistencies between config properties in XML (Properties.pm) and config properties in CSV...\n";

    # Check that all csv_properties are also in the Properties.pm (generated from XML) and defined as CONFIG
    for my $property_name (@csv_properties)
    {
        # Do not check properties to ignore
        next if(array_element_matches($property_name, PROPERTIES_TO_IGNORE));

        if(!defined($fgc_properties{$property_name}))
        {
            report_error("$property_name declared in CSV, but not defined in the XML");
        }

        my $is_config = 0;
        for my $class (keys(%{$fgc_properties{$property_name}->{classes}}))
        {
            if(defined($fgc_properties{$property_name}->{classes}->{$class}->{flags}->{CONFIG}))
            {
                $is_config = 1;
                last;
            }
        }
        if(!$is_config)
        {
            report_error("$property_name declared in CSV, but not defined as CONFIG for any class in the XML");
        }
    }

    # Check that XML CONFIG properties are in the csv file
    for my $property_name (sort(keys(%fgc_properties)))
    {
        # Do not check properties to ignore
        next if(array_element_matches($property_name, PROPERTIES_TO_IGNORE));

        # Skip non CONFIG properties
        my $is_config = 0;
        for my $class (keys(%{$fgc_properties{$property_name}->{classes}}))
        {
            if(defined($fgc_properties{$property_name}->{classes}->{$class}->{flags}->{CONFIG}))
            {
                $is_config = 1;
                last;
            }
        }
        next if(!$is_config);

        # Does @csv_properties contain $property_name?
        if(!grep(/^$property_name$/, @csv_properties))
        {
            report_error("$property_name defined as CONFIG in the XML, but not declared in CSV");
        }
    }
}

# Check consistency between CSV Properties and Properties.pm per class
# Class + Config Flag => included in CSV either as CMP prop, TYPE prop, or SYS prop of that class
sub check_csv_xml_props_per_class($$$)
{
    my ($csv_sys_props_per_class_ref, $csv_cmp_props_per_cat_ref, $csv_type_props_per_cat_ref) = @_;

    my %csv_sys_props_per_class = %{$csv_sys_props_per_class_ref};
    my %csv_cmp_props_per_cat = %{$csv_cmp_props_per_cat_ref};
    my %csv_type_props_per_cat = %{$csv_type_props_per_cat_ref};

    # Check that XML CONFIG properties are in the csv file (and also assigned to the correct classes, in case of system property)
    for my $property_name (sort(keys(%fgc_properties)))
    {
        # Do not check properties to ignore
        next if(array_element_matches($property_name, PROPERTIES_TO_IGNORE));


        # EPCCCS-5787: Temporarily do not check DIAG (dims) properties, as the way they will be treated is currently changing.
        next if($property_name =~ /^DIAG./);

        # Check for properties that are not used by any class
        if(!defined($fgc_properties{$property_name}->{classes}) || scalar(keys(%{$fgc_properties{$property_name}->{classes}})) <= 0)
        {
            report_error("$property_name not used by any class in XML", "red");
            #next;
        }

        # Skip non CONFIG properties
        my $is_config = 0;
        for my $class (keys(%{$fgc_properties{$property_name}->{classes}}))
        {
            if(defined($fgc_properties{$property_name}->{classes}->{$class}->{flags}->{CONFIG}))
            {
                $is_config = 1;
                last;
            }
        }
        next if(!$is_config);

        # No error if included in CSV cmp properties
        my $is_non_sys_prop = 0;
        for my $cat (keys(%csv_cmp_props_per_cat))
        {
            if(grep(/^$property_name$/, @{$csv_cmp_props_per_cat{$cat}}))
            {
                $is_non_sys_prop = 1;
                last;
            }
        }
        next if($is_non_sys_prop);

        # No error if included in CSV type properties
        for my $cat (keys(%csv_type_props_per_cat))
        {
            if(grep(/^$property_name$/, @{$csv_type_props_per_cat{$cat}}))
            {
                $is_non_sys_prop = 1;
                last;
            }
        }
        next if($is_non_sys_prop);

        # Do the CSV system properties of the respective class contain $property_name?
        for my $class (sort(keys(%{$fgc_properties{$property_name}->{classes}})))
        {
            next if(!defined($csv_sys_props_per_class{$class}));

            if(!grep(/^$property_name$/, @{$csv_sys_props_per_class{$class}}))
            {
                report_error("$property_name used by class $class in XML but neither declared as SYS property of class $class nor declared as CMP or TYPE property in CSV");
            }
        }
    }

    # Check that CSV system properties are in class/properties.xml of the respective class
    for my $class (keys(%csv_sys_props_per_class))
    {
        for my $property_name (sort(@{$csv_sys_props_per_class{$class}}))
        {
            # Do not check properties to ignore
            next if(array_element_matches($property_name, PROPERTIES_TO_IGNORE));

            if(!defined($fgc_properties{$property_name}->{classes}) || !defined($fgc_properties{$property_name}->{classes}->{$class}))
            {
                report_error("$property_name declared as SYS property of class $class in CSV but not used by class $class in XML");
            }
        }
    }
}

# Check consistency between CSV categories and Components.pm categories
sub check_csv_xml_categories($)
{
    my ($csv_categories_ref) = @_;
    my @csv_categories = sort(@$csv_categories_ref);

    print "Checking for inconsistencies between property categories in XML (Components.pm) and property categories in CSV...\n";

    # Check that all csv_categories are also used in the components.pm (generated by XML)
    for my $property_category (@csv_categories)
    {
        my $is_used = 0;
        for my $component (values(%fgc_components))
        {
            if(grep(/^$property_category$/, @{$component->{categories}}))
            {
                $is_used = 1;
                last;
            }
        }

        if(!$is_used)
        {
            report_error("$property_category defined as category in CSV, but not used as category in Components.pm");
        }
    }

    # Check that all XML categories are also used in the csv file
    my @checked_categories;
    for my $component (values(%fgc_components))
    {
        next if(!defined($component->{categories}));

        for my $component_category (sort(@{$component->{categories}}))
        {
            # Skip already checked categories
            if(grep(/^$component_category$/, @checked_categories))
            {
                next;
            }

            # Check if XML category contained in CSV categories
            if(!grep(/^$component_category$/, @csv_categories))
            {
                report_error("$component_category used as category in XML, but not defined as category in CSV");
            }

            push(@checked_categories, $component_category);
        }
    }
}

# Check whether types from Components.pm are defined in DB
sub db_check_types($)
{
    my ($dbh) = @_;

    print "Checking for types to be defined in DB (XML vs DB)...\n";

    my @db_types = FGC::DB::get_barcode_types($dbh);

    # Check DB results
    report_error("Failed to get the barcode from TE-EPC DB") if (!@db_types);

    for my $type (keys(%fgc_components))
    {
        report_error("ERROR: Type '$type' is not defined in TE-EPC DB") if(!($type ~~ @db_types));
    }
}

# checks the DB for all systems and their system properties to be valid in respect to the system's class
sub db_check_systems_for_wrong_sys_props($)
{
    my ($dbh) = @_;

    print "Checking systems for wrong system properties in DB...\n";

    # Find all system property names that are in DB and map them to the FGC classes that use them
    # $class->$system->$property
    my $db_sys_props_per_system_per_class = FGC::DB::get_class_to_system_to_system_properties($dbh);

    # Do not check systems of discontinued classes
    for my $class (DISCONTINUED_CLASSES)
    {
        delete($db_sys_props_per_system_per_class->{$class});
    }

    # Identify systems with wrong system properties
    # $class->$system->$property
    for my $class (sort(keys(%{$db_sys_props_per_system_per_class})))
    {
        for my $system (sort(keys(%{$db_sys_props_per_system_per_class->{$class}})))
        {
            for my $property (keys(%{$db_sys_props_per_system_per_class->{$class}->{$system}}))
            {
                # Delete from hash the system's system properties that are in class/properties.xml of its class.
                if(defined($fgc_properties{$property}->{classes}) && defined($fgc_properties{$property}->{classes}->{$class}))
                {
                    delete($db_sys_props_per_system_per_class->{$class}->{$system}->{$property});
                }
                # Do not check properties to ignore
                elsif(array_element_matches($property, PROPERTIES_TO_IGNORE))
                {
                    delete($db_sys_props_per_system_per_class->{$class}->{$system}->{$property});
                }
            }

            # The remaining properties are wrong
            if(scalar(keys(%{$db_sys_props_per_system_per_class->{$class}->{$system}})) > 0)
            {
                my $remaining_props = "\n\t".join("\n\t", keys(%{$db_sys_props_per_system_per_class->{$class}->{$system}}));
                report_error("$system has system properties in DB that are not declared in the XML of its class ($class):$remaining_props");
            }
        }
    }
}

sub db_check_sys_props($$)
{
    my ($dbh, $csv_sys_props_per_class_ref) = @_;

    # Find all system property names that are in DB and map them to the FGC classes that use them
    my $result_ref = FGC::DB::get_class_to_system_properties($dbh);
    my $db_sys_props_per_class_ref = $result_ref->{CLASS_TO_PROPERTY_NAMES};

    return db_check_props($dbh, "SYS", $csv_sys_props_per_class_ref, $db_sys_props_per_class_ref);
}

sub db_check_cmp_props($$)
{
    my ($dbh, $csv_cmp_props_per_cat_ref) = @_;

    # Find all component property names that are in DB and map them to the FGC classes that use them
    my $result_ref = FGC::DB::get_class_to_component_properties($dbh);
    my $db_cmp_props_per_class_ref = $result_ref->{CLASS_TO_PROPERTY_NAMES};

    return db_check_props($dbh, "CMP", $csv_cmp_props_per_cat_ref, $db_cmp_props_per_class_ref);
}

sub db_check_type_props($$)
{
    my ($dbh, $csv_type_props_per_cat_ref) = @_;

    # Find all type property names that are in DB and map them to the FGC classes that use them
    my $result_ref = FGC::DB::get_class_to_type_properties($dbh);
    my $db_type_props_per_class_ref = $result_ref->{CLASS_TO_PROPERTY_NAMES};

    return db_check_props($dbh, "TYPE", $csv_type_props_per_cat_ref, $db_type_props_per_class_ref);
}

# Check for obsolete/unknown properties by comparing properties from DB with CSV and XML
sub db_check_props($$$$)
{
    my ($dbh, $kind, $csv_props_per_cat_ref, $db_props_per_class_ref) = @_;

    my %csv_props_per_cat = %{$csv_props_per_cat_ref};
    my %db_props_per_class = %{$db_props_per_class_ref};
    my @db_props_all = ();

    print "Checking for unknown/obsolete $kind properties (DB vs. XML and CSV)...\n";

    # Do not check properties of discontinued classes
    for my $class (DISCONTINUED_CLASSES)
    {
        delete($db_props_per_class{$class});
    }

    # Transform .?. props to .A/B. props
    for my $class (keys(%db_props_per_class))
    {
        my @transformed_props = sort(transform_questionmark_props(@{$db_props_per_class{$class}}));
        $db_props_per_class{$class} = \@transformed_props;
        push(@db_props_all, @transformed_props);
    }
    @db_props_all = uniq(@db_props_all);

    return unless(scalar(@db_props_all) > 0);

    # Check whether the DB properties are known to the CSV
    for my $db_prop (@db_props_all)
    {
        # Do not check properties to ignore
        next if(array_element_matches($db_prop, PROPERTIES_TO_IGNORE));

        my $found = 0;

        for my $cat (keys(%csv_props_per_cat))
        {
            for my $csv_prop (@{$csv_props_per_cat{$cat}})
            {
                last if($found = $csv_prop eq $db_prop);
            }
            last if($found);
        }

        if(!$found)
        {
            report_error("$db_prop not in CSV but in DB ($kind property)");
        }
    }

    # Check whether the DB properties are known to the classes that they are associated to in DB
    for my $class (sort(keys(%db_props_per_class)))
    {
        my @db_props = @{$db_props_per_class{$class}};

        next if(scalar(@db_props) <= 0);

        # System properties can further be checked by their "property category" in CSV
        if($kind eq "SYS")
        {
            my @csv_props = ();
            @csv_props = @{$csv_props_per_cat{$class}} if(defined($csv_props_per_cat{$class}));

            for my $db_prop (@db_props)
            {
                # Do not check properties to ignore
                next if(array_element_matches($db_prop, PROPERTIES_TO_IGNORE));

                my $found = 0;
                for my $csv_prop (@csv_props) {
                    last if($found = $csv_prop eq $db_prop);
                }

                if(!$found)
                {
                    # get systems of that class that use this property
                    my $false_instances = FGC::DB::get_instances_by_class_and_property($dbh, $class, $db_prop, $kind);
                    report_error("$db_prop not in CSV for category '$class' but used by instances associated to class '$class' in DB ($kind property):\n\t".join("\n\t",@$false_instances));
                }
            }
        }

        # Get properties of this class from Properties.pm
        my @xml_props = ();
        for my $xml_prop (sort(keys(%fgc_properties)))
        {
            if(defined($fgc_properties{$xml_prop}->{classes}) && defined($fgc_properties{$xml_prop}->{classes}->{$class}))
            {
                push(@xml_props, $xml_prop);
            }
        }

        # Check whether the DB properties are in the respective CLASS/properties.xml
        for my $db_prop (@db_props)
        {
            # Do not check properties to ignore
            next if(array_element_matches($db_prop, PROPERTIES_TO_IGNORE));

            if(!grep(/^$db_prop$/, @xml_props))
            {
                my $false_instances = FGC::DB::get_instances_by_class_and_property($dbh, $class, $db_prop, $kind);
                report_error("$db_prop not in '$class/properties.xml' but used by instances associated to class '$class' in DB ($kind property):\n\t".join("\n\t",@$false_instances));
            }
        }
    }
}

sub transform_questionmark_props
{
    my @prop_list = @_;
    my @transformed_list = ();

    for my $prop (@prop_list)
    {
        push(@transformed_list, $prop);

        # If property name contains the .?. placeholder for channel A/B, remove it and add it twice (once for A, once for B)
        if($prop =~ /(\.\?\.){1}/)
        {
            pop(@transformed_list);
            push(@transformed_list, join(".A.", split(/\.\?\./, $prop)));
            push(@transformed_list, join(".B.", split(/\.\?\./, $prop)));
        }
    }

    return @transformed_list;
}

sub array_element_matches($$)
{
    my ($str, @array) = @_;

    my $is_ignored = 0;
    for my $regex (@array)
    {
        if($str =~ /$regex/)
        {
            return 1;
        }
    }

    return 0;
}

sub get_options()
{
    my %opts = (check_db=>undef);
    GetOptions("check_db!"=> \$opts{check_db});

    $opts{check_db} = defined($opts{check_db}) && $opts{check_db};

    return \%opts;
}

sub print_usage_message
{
    print "\nThe script performs consistency checks between XML and DB.\n";
    print "\nUsage: $0 [--check_db]\n\n";
}

sub report_error
{
    my ($message) = @_;
    print("\n".colored("ERROR: " . $message, "red")."\n");
    die("\n");
}

# End of functions

my $opts = get_options();
my $check_db = $opts->{check_db};

# Check symlists for consistency based on Properties.pm
check_symlists_xml();

# Check consistency
# between CSV properties and Properties.pm properties
# between CSV categories and Components.pm categories
#my $parsed_csv = parse_properties_csv(PATH_TO_CSV);
#check_csv_xml_props($parsed_csv->{all_props});
#check_csv_xml_props_per_class($parsed_csv->{sys_props_per_class}, $parsed_csv->{cmp_props_per_cat}, $parsed_csv->{type_props_per_cat});
#check_csv_xml_categories($parsed_csv->{all_cats});

# Only check DB consistency if option was specified
exit(0) if(!$check_db);

# Check db credentials and connect to DB
my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD) or die "Unable to connect to database: $!\n";

# Check whether types from Components.pm are defined in DB
db_check_types($dbh);

# Check for obsolete/unknown properties by comparing properties from DB with CSV and XML. See: EPCCCS-6036
#db_check_sys_props($dbh, $parsed_csv->{sys_props_per_class});
#db_check_cmp_props($dbh, $parsed_csv->{cmp_props_per_cat});
#db_check_type_props($dbh, $parsed_csv->{type_props_per_cat});
db_check_systems_for_wrong_sys_props($dbh);

# Disconnect from DB
FGC::DB::disconnect($dbh);

exit(0);

# EOF
