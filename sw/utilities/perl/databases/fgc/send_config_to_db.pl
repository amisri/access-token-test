#!/usr/bin/perl -w
#
# Name:     send_config_to_db.pl
# Purpose:  Allows to update config property values of systems in DB based on a given config file
#           Useful when a device is offline but the config properties need to be prepared.
# Author:   Kevin Kessler

use strict;
use warnings;
use FindBin qw($Bin);
use Getopt::Long    qw(GetOptionsFromArray);
use List::MoreUtils qw(uniq);

use FGC::DB;
use FGC::Names;
use FGC::Properties;
use FGC::Components;

# parses the given config file and returns a property-value hash
sub parse_config_file($;$)
{
    my ($config_file, $is_regfgc3_config) = @_;

    print("\nParsing config properties from file ...\n");

    # get config properties to be ignored and escape them to be used as regular expressions
    my @config_props_to_ignore = map(quotemeta, @{get_config_properties_to_ignore()});
    my $ignore_regex = "(".join("|", @config_props_to_ignore).")";
    $ignore_regex = qr/$ignore_regex/;

    # Open config file
    if(!open(CONFIG_FILE, "<", $config_file))
    {
        die("ERROR: Failed to open $config_file: $!");
    }

    # Read lines from the config file and store in hash
    my %config_properties = ();
    while(<CONFIG_FILE>)
    {
        s/[\012\015]//g;    # Remove newline
        s/#.*//;            # Remove comments

        next if(/^\s*$/);   # Ignore blank lines

        s/^!\s*S //i;       # Remove "! S " from beginning of line

        (my $property   = $_) =~ s/^(\S+).*/$1/;
        (my $value      = $_) =~ s/^\S+\s+(.*)/$1/;
        $value =~ s/\s+//g; # Remove spaces from value

        # Filters for REGFGC3
        if($is_regfgc3_config)
        {
            # Skip everything but regfgc3 properties
            next if($property !~ /^REGFGC3./i); 
        }

        # Filters for CONFIG.SET
        else
        {
            if($property =~ /$ignore_regex/i)
            {
                print("\tIgnoring blacklisted property $property\n");
                next;
            }
        }

        $config_properties{$property} = $value;
    }

    close(CONFIG_FILE);

    return \%config_properties;
}

sub get_config_properties_to_ignore
{
    my @filters = ();

    # Ignore ADC.INTERNAL.TAU_TEMP. See EPCCCS-5977
    push(@filters, 'ADC.INTERNAL.TAU_TEMP');

    # Ignore ADC.INTERNAL.VREF_TEMP.PID and ADC.INTERNAL.VREF_TEMP.REF. See EPCCCS-3821
    push(@filters, 'ADC.INTERNAL.VREF_TEMP.REF');
    push(@filters, 'ADC.INTERNAL.VREF_TEMP.PID');

    # Ignore barcode properties
    push(@filters, 'BARCODE.'); 

    # Ignore cal properties if $ignore_cal defined and not zero. See also EPCCCS-176
    push(@filters, 'CAL.');

    # Ignore LIMTIS.I.MEAS_DIFF. See EPCCCS-4988
    push(@filters, 'LIMITS.I.MEAS_DIFF'); 

    # Ignore REGFGC3 properties. See EPCCCS-6781
    push(@filters, 'REGFGC3.'); 

    return \@filters;
}

sub synchronise_to_db($$$)
{
    my ($dbh, $config_properties, $device_name) = @_;
    my $failed = 0;

    print "\nSynchronising from file to database for $device_name...\n";

    print "\tReading system details from database...\n";
    my $db_system = FGC::DB::get_system_details($dbh, $device_name, \%fgc_components);

    if(!defined($db_system))
    {
        print "\tFailed to read details for system $device_name from database\n";
        return(undef);
    }

    # create a key in update hash for each system property of the current device
    my %system_properties = map { $_ => undef } keys(%{$db_system->{properties}});

    # read system properties from config
    for my $config_property (keys(%$config_properties)) 
    {
        $system_properties{$config_property} = $config_properties->{$config_property} if(exists($system_properties{$config_property}));
    }

    print "\tChecking for empty, unchanged or ignored system properties ...\n";

    # Check whether properties between file and db differ - remove from update hash if not
    for my $property_name (sort(keys(%system_properties)))
    {
        my $db_value = $db_system->{properties}->{$property_name};
        my $update_value = $system_properties{$property_name};

        if(!defined($update_value) || $update_value eq '' || (defined($db_value) && $db_value eq $update_value))
        {
            # Property value is empty or has not changed, remove from update set
            # print "\t\tSkipping $property_name\n";
            delete($system_properties{$property_name});
            next;
        }
    }

    # Update values of system properties in database
    if(scalar(keys(%system_properties)))
    {
        print "\tUpdating database with new system property values...\n";

        for my $property_name (sort(keys(%system_properties)))
        {
            print "\t\t$property_name=\"", $system_properties{$property_name} || "", "\" (was ", $db_system->{properties}->{$property_name} || "<null>", ")\n";
        }

        if(!defined(FGC::DB::update_system_property_values($dbh, $device_name, \%system_properties)))
        {
            print "\t\t\tFailed to update system properties for device $device_name\n";
            $failed = 1;
        }
    }
    else
    {
        print "\tNo system property values have changed.\n";
    }

    # Handle component properties
    for my $component (values(%{$db_system->{components}}))
    {   
        my %component_properties = ();

        # create a key in update hash for each component property of the current component
        for my $property_name (keys(%{$component->{properties}}))
        {
            $property_name              =~ s/\?/$component->{COMPONENT_CHANNEL}/g if(defined($component->{COMPONENT_CHANNEL}));
            $component_properties{$property_name} = undef;
        }

        # read respective property values from config
        for my $config_property (keys(%$config_properties)) 
        {
            $component_properties{$config_property} = $config_properties->{$config_property} if(exists($component_properties{$config_property}));
        }

        print "\tChecking for empty, unchanged or ignored component properties ...\n";

        # Check whether properties between file and db differ - remove from update hash if not
        for my $property_name (keys(%component_properties))
        {
            my $bus_property_name   = $property_name;
            $bus_property_name      =~ s/\?/$component->{COMPONENT_CHANNEL}/g if(defined($component->{COMPONENT_CHANNEL}));

            my $db_value = $component->{properties}->{$property_name};
            my $update_value = $component_properties{$bus_property_name};

            if(!defined($update_value) || $update_value eq '' || (defined($db_value) && $db_value eq $update_value))
            {
                # Property value is empty or has not changed, remove from update set
                # print "\t\tSkipping $property_name\n";
                delete($component_properties{$bus_property_name});
                next;
            }
        }

        # Update values of component properties in database
        if(scalar(keys(%component_properties)))
        {
            print "\tUpdating database with new property values for component $component->{COMPONENT_MTF_ID}...\n";

            for my $property_name (sort(keys(%component_properties)))
            {
                print "\t\t$property_name=\"", $component_properties{$property_name} || "", "\" (was ", $component->{properties}->{$property_name} || "<null>", ")\n";
            }

            if(!defined(FGC::DB::update_component_property_values($dbh, $component->{COMPONENT_MTF_ID}, \%component_properties)))
            {
                print "\t\t\tERROR: Failed to update component properties for component $component->{COMPONENT_MTF_ID}\n";
                $failed = 1;
                next;
            }
        }
        else
        {
            print "\t\tNo component property values have changed.\n";
        }
    }
    
    if ($failed)
    {
        print "ERROR: Failed to complete the synchronisation from config file to database for $device_name at ", scalar(localtime), ".\n";
        return(undef);
    }
    else
    {
        print "Synchronisation from config file to database for $device_name completed at ", scalar(localtime), ".\n";
        return(0);
    }
}

sub getOpts
{
    my @args    = @ARGV;
    my %options = ();

    GetOptionsFromArray(\@args, \%options, 'db_user:s', 'db_pw:s', 'devices:s', 'config_file:s');

    if(isEmpty($options{devices}) || isEmpty($options{config_file}))
    {
        printHelpAndDie();
    }

    # convert devicestring into array
    $options{devices} = uc($options{devices});
    my @devices = split(",", $options{devices});

    # Read name file for perfoming checks on selected devices
    my ($file_devices, $file_gateways) = FGC::Names::read();
    die "Unable to read FGC name file\n" if(!defined($file_devices));

    # validate devicenames
    my @valid_devices = ();
    my @invalid_devices = ();
    for my $device_name (@devices)
    {
        my $device = $file_devices->{$device_name};
        if(defined($device))
        {
            push(@valid_devices, $device);
        }
        else
        {
            push(@invalid_devices, $device);
        }
    }

    if(scalar(@invalid_devices) > 0)
    {
        print("WARNING: Skipping devices that are not included in name file:".join("\n\t", @invalid_devices));
    }

    $options{devices} = \@valid_devices;

    if(isEmpty($options{db_user}))
    {
        # Prompt for username
        print "\nUsername: ";
        chomp($options{db_user} = <STDIN>);
    }
    
    # Prompt for password
    print "Password: ";
    system("stty -echo");
    chomp($options{db_pw} = <STDIN>);
    system("stty echo");
    print "\n\n";

    return \%options;
}

sub import_system_properties($$$)
{
    my ($username, $password, $devices) = @_;

    print("\nMaking sure system properties are properly instantiated in DB ...\n");

    my $import_system_properties_pl = File::Spec->catfile($Bin, 'import_system_properties.pl');

    # group devices by class
    my %class_to_devices = ();
    for my $device (@{$devices}) 
    {
        my $class = $device->{class};
        $class_to_devices{$class} = [] if(!exists($class_to_devices{$class}));
        push(@{$class_to_devices{$class}}, $device);
    }

    # make sure system properties for respective class and devices are initialized
    for my $class (sort(keys(%class_to_devices))) 
    {
        my @device_names = ();
        for my $device (@{$class_to_devices{$class}}) 
        {
            push(@device_names, $device->{name});
        }
        my $user_devices = join(",", @device_names);
        system("$import_system_properties_pl", "FGC_properties.csv", "$class", "--db_user=$username", "--db_pw=$password", "--devices=$user_devices");
    }
}

sub isEmpty
{
    return (!defined($_[0]) || !length($_[0]));
}

sub printHelpAndDie
{
    print "Usage: --config=<filename> --devices=<comma_sep_list> [--db_user=<username>]\n";
    die "";
}

sub main
{
    my $options = getOpts();

    # parse config file to hash 'property->value'
    my $config_properties = parse_config_file($options->{config_file}, $options->{is_regfgc3_config});

    # make sure system properties for respective devices are initialized in DB
    import_system_properties($options->{db_user}, $options->{db_pw}, $options->{devices});

    # Check db credentials and connect to DB
    my $dbh = FGC::DB::connect($options->{db_user}, $options->{db_pw}) or die "Unable to connect to database: $!\n";

    # update properties of each device in DB based on parsed config file
    for my $device (@{$options->{devices}}) 
    {
        synchronise_to_db($dbh, $config_properties, $device->{name});
    }

    # Disconnect from DB
    FGC::DB::disconnect($dbh);
}

main();

# EOF
