#!/usr/bin/perl -w
#
# Name:     extract_component_property_values.pl
# Purpose:  List equipment property values from FGC database
# Author:   Stephen Page

use strict;

use FGC::Consts;
use FGC::DB;

# Connect to database

my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD)
            or die "Unable to connect to database: $!\n";

# Prepare database query

my $query;
if(!($query = $dbh->prepare("SELECT CMP_MTF_ID, PRO_NAME, CPR_VALUE
                             FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_COMPONENT_PROPERTIES, POCONTROLS.FGC_PROPERTIES
                             WHERE CMP_ID=CPR_CMP_ID AND CPR_PRO_ID=PRO_ID")))
{
    FGC::DB::disconnect($dbh);
    die "Failed to prepare database query\n";
}

# Execute database query

if(!$query->execute())
{
    FGC::DB::disconnect($dbh);
    die "Failed to execute database query\n";
}

# Retrieve results

my $results = $query->fetchall_arrayref({ CMP_MTF_ID => 1, CPR_VALUE => 1, PRO_NAME => 1 });

# Disconnect from database

FGC::DB::disconnect($dbh);

for my $line (sort { $a->{CMP_MTF_ID}   cmp $b->{CMP_MTF_ID} ||
                         $a->{PRO_NAME}     cmp $b->{PRO_NAME} } @$results)
{
    next if(!defined($line->{CPR_VALUE}));
    print "$line->{CMP_MTF_ID}, $line->{PRO_NAME}, $line->{CPR_VALUE}\n";
}

# EOF
