#!/usr/bin/perl -w
#
# Name:     import_component_properties.pl
# Purpose:  Import component properties into FGC database
# Author:   Stephen Page

use strict;

use FGC::Components;
use FGC::DB;
use Getopt::Long;
use JSON;
use File::Slurp;

my $username = '';
my $password = '';

# TDefault calibration value. See more details in EDMS 1767494: https://edms.cern.ch/document/1767404

my %defaults = (
    # DCCT
    'CAL\.[AB?]\.DCCT\.HEADERR'          => {     '.*'                =>  '0.0000000E+00' },
    'CAL\.[AB?]\.DCCT\.ERR'              => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00' },
    'CAL\.[AB?]\.DCCT\.TC'               => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00' },
    'CAL\.[AB?]\.DCCT.DTC'               => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00' },

    # EXTERNAL ADC
    'CAL\.[AB?]\.ADC\.EXTERNAL\.ONE_MHZ' => {     '.*'                =>  'DISABLED' },
    'CAL\.[AB?]\.ADC\.EXTERNAL\.GAIN'    => {     '.*'                =>  '20000000' },
    'CAL\.[AB?]\.ADC\.EXTERNAL\.ERR'     => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00' },

    # INTERNAL ADC
    'CAL\.[ABCD?]\.ADC\.INTERNAL\.GAIN'  => {     'HCRFBAY001'        =>  '20000000',     # FGClite Analogue Board
                                                  'HCRFBAY002'        =>  '20000000',     # FGClite Analogue Board
                                                  'HCRFBBH002'        =>  '20000000',     # FGClite Analogue Board Radiation Tolerant V2-1
                                                  'HCRFBFA___'        =>  '20000000',     # FGC2 SD-350:Delta-Sigma ADC/DAC Interface
                                                  'HCRFBFB___'        =>  '20000000',     # FGC2 SD-351:Delta-Sigma ADC/DAC with serial receivers
                                                  'HCRFBFC___'        =>  '20000000',     # FGC2 SD-360:HP Delta-Sigma ADC/DAC Interface
                                                  'HCRFBGA___'        =>  '20000000',     # FGC2 SAR-400:SAR ADC/DAC Interface
                                                  'HCRFBGD001'        =>  '6000000' ,     # FGC3.1 Analog Type 1
                                                  'HCRFBGE001'        =>  '6000000' ,     # FGC3.1 Analog Type 2
                                                  'HCRFBGF001'        =>  '20000000',    # FGC3.1 Analog Type 3 ANA103
                                                  'HCRFBGG001'        =>  '20000000',    # FGC3 Analog Board ANA103 V1-0
                                                  'HCRAMET002'        =>  '20000000',    # FGC3 Analog Board ANA103 V2-0
                                            },
    'CAL\.[ABCD?]\.ADC\.INTERNAL\.ERR'   => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00' },
    'CAL\.[ABCD?]\.ADC\.INTERNAL\.TC'    => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00' },
    'CAL\.[ABCD?]\.ADC\.INTERNAL\.DTC'   => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00' },
    'ADC\.INTERNAL\.TAU_TEMP'            => {     '.*'                =>  '8.0000000E+01' },

    # VREF
    'CAL\.VREF\.ERR'                     => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00' },
    'CAL\.VREF\.TC'                      => {     '.*'                =>  '0.0000000E+00,0.0000000E+00,0.0000000E+00' },

    # BARCODES
    'BARCODE\.FGC\.CASSETTE'             => {     '.*'                =>  'UNKNOWN' },
    'BARCODE\.FGC\.ANA'                  => {     '.*'                =>  'UNKNOWN' },
    'BARCODE\.[AB?]\.DCCT\.HEAD'         => {     '.*'                =>  'UNKNOWN' },
    'BARCODE\.[AB?]\.DCCT\.ELEC'         => {     '.*'                =>  'UNKNOWN' },
    'BARCODE\.[AB?]\.EXADC.CASSETTE'     => {     '.*'                =>  'UNKNOWN' },

);

my $db_config_file = undef;

sub read_properties($)
{
    my ($filename) = @_;

    open(FILE, '<', $filename) or die "Unable to open $filename: $!\n";

    my %properties;

    my ($usage, $property_name, $category);
    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline

        ($usage, $property_name, $category) = split(',', $_);

        next if($usage ne 'CMP');
        if(!defined($category) || $category eq "")
        {
            warn "Category not defined for property $property_name, ignoring\n";
            next;
        }

        if(!exists($properties{$property_name}))
        {
            my @categories;
            $properties{$property_name} = \@categories;
        }

        push(@{$properties{$property_name}},$category);

    }
    close(FILE);

    return(\%properties);
}

# Read command-line options

GetOptions('dbconfig:s'=>\$db_config_file);

die "Usage: $0 [--dbconfig=DB_AUTH_FILE ] <filename>\n" if(@ARGV != 1);

if(defined($db_config_file))
{
    # Read username/password from file

    my $json_text = do
    {
       open(my $json_fh, "< $db_config_file")  or die("Can't open $db_config_file". $!."\n");
       local $/;
       <$json_fh>
    };

    eval
    {
        my $config=decode_json($json_text);
        $username = $config->{db_username};
        $password = $config->{db_password};
    }
    or do
    {
        die "ERROR: can't read JSON configuration:\n$@!\n";
    };
}
else
{
    # Prompt for username

    print "\nUsername: ";
    chomp($username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp($password = <STDIN>);
    system("stty echo");
    print "\n\n";
}

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

my ($filename) = @ARGV;

my $properties = read_properties($filename);

# Add missing property names

for my $property (sort(keys(%$properties)))
{
    my $result = FGC::DB::add_property($dbh, $property);

    if(!defined($result))
    {
        FGC::DB::disconnect($dbh);
        die "ERROR: Unable to add property $property\n";
    }
    elsif ($result != 0)
    {
         print "Property $property added\n";
    }
}

# Add missing equipment properties

my $count = 1;
my $total = scalar(keys(%fgc_components));
for my $type (values(%fgc_components))
{
    # Show progress

    print "\r$count/$total Importing properties for component./ type $type->{barcode}                                        ";
    $count++;

    for my $category (@{$type->{categories}})
    {
        for my $property (sort(keys(%$properties)))
        {
            for my $prop_category (grep {$_ eq $category} @{$properties->{$property}})
            {
                my $result = FGC::DB::add_component_property($dbh, $type->{barcode}, $property, undef);

                if(!defined($result))
                {
                    FGC::DB::disconnect($dbh);
                    die "ERROR: Unable to add property $property\n";
                }
                elsif ($result != 0)
                {
                    print "\n\tProperty $property added to $result components of type $type->{barcode}\n";
                }
            }
        }
    }
}

#  Update the equipment properties with NULL value to default values

my $components_with_null_properties = FGC::DB::get_component_properties_with_null_value($dbh);
for my $mtf_id (sort(keys(%$components_with_null_properties)))
{
    my $props_to_update = {};

    # check for default values for each null_property of this specific component

    for my $null_property (sort(@{ $components_with_null_properties->{$mtf_id} }))
    {
        for my $property_regex (sort(keys(%defaults)))
        {
            # is there a default value assigned to this specific property?

            if($null_property =~ /$property_regex/)
            {
                for my $component_regex (sort(keys( %{ $defaults{$property_regex} } )))
                {
                    # is there a default value for this specific component (for this property)?

                    if($mtf_id =~ /$component_regex/)
                    {
                        # map property_to_update to default value

                        $props_to_update->{$null_property} = $defaults{$property_regex}{$component_regex};
                        print "\nSetting default value of '$null_property' for '$mtf_id' (from NULL to '$defaults{$property_regex}{$component_regex}')\n"
                    }
                }
            }
        }
    }

    # execute update for this component

    FGC::DB::update_component_property_values($dbh, $mtf_id, $props_to_update) if(keys(%{$props_to_update}) > 0);
}

FGC::DB::disconnect($dbh);

print "\nDONE\n";

# EOF
