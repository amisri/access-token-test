#!/usr/bin/perl -w

use FGC::DB;

die "Usage: $0 <file>\n" if(@ARGV != 1);
my ($file) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

open(FILE, "<", $file) or die "Failed to open file $file: $!\n";

my %child_parent;
while(<FILE>)
{
    s/[\012\015]//g; # Remove newline

    my ($parent, $child)    = split(',');
    $child_parent{$child}   = $parent;
}
close(FILE);

my $dbh = FGC::DB::connect($username, $password);
die "Failed to connect to database\n" if(!defined($dbh));

FGC::DB::set_components_parents($dbh, \%child_parent);
FGC::DB::add_children_to_parents_systems($dbh);
FGC::DB::disconnect($dbh);

# EOF
