#!/usr/bin/perl -w
#
# Name:     update_pal_cals.pl
# Purpose:  Update PAL calibrations in FGC configuration database
# Author:   Stephen Page

use FGC::Consts;
use FGC::DB;
use File::Basename;
use strict;

# Type for type properties

use constant EQUIPMENT_TYPE => 'HCRPOPS___';

# Fields required to be present in data files

my %data_req_fields = (
                        DIVPAM_TYPE     => 1,
                        FILTER_GAIN     => 1,
                        ID              => 1,
                        V_DVM_POS       => 1,
                        V_PAM_POS       => 1,
                        V_PAM_ZERO      => 1,
                      );

# Fields required to be present in data files for DIVPAM_TYPE eq 'B'

my %data_req_fields_b = (
                            V_DVM_NEG   => 1,
                            V_PAM_NEG   => 1,
                        );

# Read a calibration data file

sub read_data_file($)
{
    my ($filename) = @_;

    # Read data file into hash

    open(DATA_FILE, '<', $filename)
        or die "Failed to open $filename: $!\n";

    my %cal_data;
    while(<DATA_FILE>)
    {
        s/[\012\015]//g; # Remove newline
		chomp;
        my ($field, $value) = split(':');
        $cal_data{$field}   = $value;
        print "$field : $value\n"
    }
    close(DATA_FILE);

    # Check that all required fields are present

    for my $field (keys(%data_req_fields))
    {
        if(!defined($cal_data{$field}))
        {
            die "Required field $field not present in file $filename\n";
        }
    }

    # If DIVPAM_TYPE is 'B', then check additional required fields are present

    if($cal_data{DIVPAM_TYPE} eq 'B')
    {
        for my $field (keys(%data_req_fields_b))
        {
            if(!defined($cal_data{$field}))
            {
                die "Required field $field not present in file $filename\n";
            }
        }
    }

    return(\%cal_data);
}

# Modify property value to include new value for element corresponding to $cal_data->{ID}

sub modify_prop_value($$$$)
{
    my ($prop_name, $prop_value, $id, $value) = @_;

    # Default to existing values

    my @prop_value_array;
    if(defined($prop_value))
    {
        @prop_value_array = split(',', $prop_value);
    }

    my $old_value           = defined($prop_value_array[$id]) ? $prop_value_array[$id] : '';
    $prop_value_array[$id]  = sprintf("%.7E", $value);
	
    # Initialise undefined elements to empty string

    for(my $i = 0 ; $i < $id ; $i++)
    {
        if(!defined($prop_value_array[$i]))
        {
            $prop_value_array[$i] = '';
        }
    }
	print "$prop_name old: $prop_value\n";
	print ("$prop_name new: ", join(',', @prop_value_array),"\n");
	
    return(join(',', @prop_value_array));
}

# Update FGC property values in database

sub update_db_props($$$)
{
    my ($dbh,$db_props, $cal_data) = @_;

    my $property;
    my %updated_props;

    # Negative voltage reference

    $property = 'PAL.DIVPAM.VREF_NEG';
    $updated_props{$property} = modify_prop_value($property,
                                                  $db_props->{$property},
                                                  $cal_data->{ID},
                                                  $cal_data->{DIVPAM_TYPE} eq 'B' ? $cal_data->{V_DVM_NEG} : 0);
	
    # Positive voltage reference

    $property = 'PAL.DIVPAM.VREF_POS';
    $updated_props{$property} = modify_prop_value($property,
                                                  $db_props->{$property},
                                                  $cal_data->{ID},
                                                  $cal_data->{V_DVM_POS});

    # Zero voltage reference

    $property = 'PAL.DIVPAM.VREF_ZERO';
    $updated_props{$property} = modify_prop_value($property,
                                                  $db_props->{$property},
                                                  $cal_data->{ID},
                                                  $cal_data->{DIVPAM_TYPE} eq 'B' ? $cal_data->{V_DVM_ZERO} : 0);

    # Calibration high

    $property = 'PAL.DIVPAM.CAL_HIGH';
    $updated_props{$property} = modify_prop_value($property,
                                                  $db_props->{$property},
                                                  $cal_data->{ID},
                                                  $cal_data->{V_PAM_POS} / $cal_data->{FILTER_GAIN});

    # Calibration low

    $property = 'PAL.DIVPAM.CAL_LOW';
    $updated_props{$property} = modify_prop_value($property,
                                                  $db_props->{$property},
                                                  $cal_data->{ID},
                                                  ($cal_data->{DIVPAM_TYPE} eq 'B'  ?
                                                   $cal_data->{V_PAM_NEG}           :
                                                   $cal_data->{V_PAM_ZERO}) / $cal_data->{FILTER_GAIN});

    # Remove unchanged values from %updated_props
    for my $property (sort(keys(%updated_props)))
    {
        next if(!defined($db_props->{$property}));

        if($db_props->{$property} eq $updated_props{$property})
        {
            delete($updated_props{$property});
        }
        
    }
	
    # Update property values in database

	if (%updated_props)
	{
		print "Updating ", EQUIPMENT_TYPE, " properties: ", join(',', sort(keys(%updated_props))),"\n";
	} else
	{
		print EQUIPMENT_TYPE, " is up-to-date\n"
	}
	
    FGC::DB::update_type_property_values($dbh, EQUIPMENT_TYPE, \%updated_props) or die "Failed to update database values\n";

}

# End of functions

# 

die "Usage: $0 <DVPAM_XX_YYY-MM-DD_HH-MM-SS.txt>\n" if(@ARGV != 1);
my ($data_file) = @ARGV;

# Prompt for username and password

print "\nUsername: ";
chomp(my $username = <STDIN>);
print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Connect to database

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database\n";

# Get the current type properties 

my $db_props = FGC::DB::get_type_properties($dbh, EQUIPMENT_TYPE);

# Get the new type properties

my $cal_data = read_data_file($data_file);

# Update DB

update_db_props($dbh,$db_props,$cal_data);

# Disconnect

FGC::DB::disconnect($dbh);

## EOF
