#!/usr/bin/perl -w
#
# Name:     rename_systems.pl
# Purpose:  Rename devices in FGC Configuration Database
# Author:   Stephen Page

use FGC::DB;
use strict;

# Check arguments

die "Usage: $0 <rename file>\n" if(@ARGV != 1);
my ($file) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read file containing old and new names

open(FILE, "<", $file) or die "Unable to open file $file: $!\n";

my %renames;
while(my ($old_name, $new_name) = split(/\s+/, <FILE>))
{
    $renames{$old_name} = $new_name;
}
close(FILE);

# Connect to database

my $dbh = FGC::DB::connect($username, $password)
            or die "Unable to connect to database: $!\n";

for my $old_name (keys(%renames))
{
    print "Renaming $old_name to $renames{$old_name}...\n";
    FGC::DB::rename_system($dbh, $old_name, $renames{$old_name});
}
FGC::DB::disconnect($dbh);

# EOF
