#!/usr/bin/perl -w
#
# Name:     filter_update.pl
# Purpose:  Update gain values for SD boards as a result of change to filter
# Author:   Stephen Page

use strict;

use FGC::Consts;
use FGC::DB;

die "Usage: $0 <username> <password>\n" if(@ARGV != 2);
my ($username, $password) = @ARGV;

# Connect to database

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

# Prepare database query to retrieve existing values

my $query;
if(!($query = $dbh->prepare("SELECT cpr_id, cpr_value
                             FROM pocontrols.fgc_components, pocontrols.fgc_component_properties, pocontrols.fgc_properties
                             WHERE cpr_pro_id=pro_id
                             AND pro_name=?
                             AND cpr_cmp_id=cmp_id
                             AND cmp_mtf_id LIKE 'HCRFBF%'")))
{
    FGC::DB::disconnect($dbh);
    die "Failed to prepare database query\n";
}

# Execute database query

if(!$query->execute('CAL.A.ADC16.GAIN'))
{
    FGC::DB::disconnect($dbh);
    die "Failed to execute database query\n";
}

# Retrieve results

my $results = $query->fetchall_arrayref({ cpr_id => 1, cpr_value => 1 });

# Prepare query to update database with new value

if(!($query = $dbh->prepare("UPDATE pocontrols.fgc_component_properties
                             SET cpr_value=?
                             WHERE cpr_id=?")))
{
    FGC::DB::disconnect($dbh);
    die "Failed to prepare database query\n";
}

foreach my $result (@$results)
{
    next if(!defined($result->{cpr_value}));
    next if($result->{cpr_value} > 10000000);

    my $new_value = $result->{cpr_value} * 4;
    print "$result->{cpr_id}, $result->{cpr_value}, $new_value\n";

    # Execute database query

    if(!$query->execute("$new_value", $result->{cpr_id}))
    {
        FGC::DB::disconnect($dbh);
        die "Failed to execute database query\n";
    }
}
FGC::DB::disconnect($dbh);

# EOF
