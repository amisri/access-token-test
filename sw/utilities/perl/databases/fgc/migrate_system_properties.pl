#!/usr/bin/perl -w
#
# Name:     migrate_system_properties_61_to_63.pl
# Purpose:  Copies values from old 61 system properties to 63 system properties, where applicable. Sets default values for new 63 properties.
# Author:   Kevin Kessler

use strict;
use warnings;
use FindBin qw( $Bin );
use File::Spec;
use File::Basename;
use File::Slurp;
use Getopt::Long    qw(GetOptionsFromArray);
use List::MoreUtils qw(uniq);

use FGC::DB;
use FGC::Names;
use FGC::Properties;
use FGC::Utils;


# properties which exist in both classes (A and B) but were renamed
my $renamed_properties =
{
    # from class (A)
    61 =>
    {
        # to class (B)
        63 =>
        {
            # old name -> new name
            'LIMITS.I.MIN'=>'LIMITS.I.STANDBY',
            'REF.DEFAULTS.B.ACCELERATION'=>'REF.DEFAULTS.B.DECELERATION',
            'REF.DEFAULTS.I.ACCELERATION'=>'REF.DEFAULTS.I.DECELERATION',
            'REF.DEFAULTS.V.ACCELERATION'=>'REF.DEFAULTS.V.DECELERATION',
            'REG.I.CLBW'=>'REG.I.INTERNAL.AUXPOLE1_HZ',
            'REG.I.CLBW2'=>'REG.I.INTERNAL.AUXPOLES2_HZ',
            'REG.I.Z'=>'REG.I.INTERNAL.AUXPOLES2_Z',
            'REG.I.MANUAL.R'=>'REG.I.EXTERNAL.OP.R',
            'REG.I.MANUAL.S'=>'REG.I.EXTERNAL.OP.S',
            'REG.I.MANUAL.T'=>'REG.I.EXTERNAL.OP.T',
            'REG.I.MANUAL.TRACK_DELAY'=>'REG.I.EXTERNAL.TRACK_DELAY_PERIODS',
            'REG.I.PERIOD_DIV'=>'REG.I.PERIOD_ITERS',
            'REG.MODE_INIT' => 'REG.MODE_CYC',
            'STATE.PC_ON' => 'MODE.PC_ON',
        }
    },

    # from class (A)
    ELENA_63 =>
    {
        # to class (B)
        63 =>
        {
            'INTER_FGC.MASTERS' => 'INTER_FGC.MASTER',
            'LIMITS.B.MIN' => 'LIMITS.B.STANDBY',
            'LIMITS.I.MIN' => 'LIMITS.I.STANDBY',
            'MEAS.B_DELAY_ITERS' => 'MEAS.B.DELAY_ITERS',
            'MEAS.B_FIR_LENGTHS' => 'MEAS.B.FIR_LENGTHS',
            'MEAS.I_DELAY_ITERS' => 'MEAS.I.DELAY_ITERS',
            'MEAS.I_FIR_LENGTHS' => 'MEAS.I.FIR_LENGTHS',
            'REF.DEFAULTS.PLAT_DURATION' => 'REF.DEFAULTS.PLATEAU_DUR',
            'REG.MODE_INIT' => 'REG.MODE_CYC',
            'STATE.PC_ON' => 'MODE.PC_ON',
            'VS.PRESENT' => 'INTER_FGC.VS_PRESENT',
        }
    },

    # from class (A)
    LS2_51 =>
    {
        # to class (B)
        51 =>
        {
            'ADC.EXTERNAL' => 'ADC.EXTERNAL.CONTROL',
            'LIMITS.I.MIN' => 'LIMITS.I.STANDBY',
            'REG.I.CLBW'=>'REG.I.INTERNAL.AUXPOLE1_HZ',
            'REG.I.CLBW2'=>'REG.I.INTERNAL.AUXPOLES2_HZ',
            'REG.I.MANUAL.R'=>'REG.I.EXTERNAL.OP.R',
            'REG.I.MANUAL.S'=>'REG.I.EXTERNAL.OP.S',
            'REG.I.MANUAL.T'=>'REG.I.EXTERNAL.OP.T',
            'REG.I.MANUAL.TRACK_DELAY'=>'REG.I.EXTERNAL.TRACK_DELAY',
            'REG.I.PURE_DELAY'=>'REG.I.INTERNAL.PURE_DELAY',
            'REG.I.Z'=>'REG.I.INTERNAL.AUXPOLES2_Z',
            'VS.DAC_CLAMP_PCT'=>'VS.DAC_CLAMP',  # TODO: value to be divided by 10
        }
    },

    # from class (A)
    LS2_53 =>
    {
        # to class (B)
        53 =>
        {
            'ADC.EXTERNAL' => 'ADC.EXTERNAL.CONTROL',
            'LIMITS.I.MIN' => 'LIMITS.I.STANDBY',
            'REG.B.B_I_RATIO' => 'LOAD.GAUSS_PER_AMP',
            'REG.B.R' => 'REG.B.EXTERNAL.OP.R',
            'REG.B.S' => 'REG.B.EXTERNAL.OP.S',
            'REG.B.T' => 'REG.B.EXTERNAL.OP.T',
            'REG.B.TRACK_DELAY' => 'REG.B.EXTERNAL.TRACK_DELAY',
            'REG.I.CLBW'=>'REG.I.INTERNAL.AUXPOLE1_HZ',
            'REG.I.CLBW2'=>'REG.I.INTERNAL.AUXPOLES2_HZ',
            'REG.I.MANUAL.R'=>'REG.I.EXTERNAL.OP.R',
            'REG.I.MANUAL.S'=>'REG.I.EXTERNAL.OP.S',
            'REG.I.MANUAL.T'=>'REG.I.EXTERNAL.OP.T',
            'REG.I.MANUAL.TRACK_DELAY'=>'REG.I.EXTERNAL.TRACK_DELAY',
            'REG.I.PURE_DELAY'=>'REG.I.INTERNAL.PURE_DELAY',
            'REG.I.Z'=>'REG.I.INTERNAL.AUXPOLES2_Z',
        }
    },

    # from class (A)
    LS2_63 =>
    {
        # to class (B)
        63 =>
        {
            'LIMITS.I.MIN' => 'LIMITS.I.STANDBY',
            'REG.B.TRACK_DELAY' => 'REG.B.EXTERNAL.TRACK_DELAY_PERIODS',
            'REG.B.R' => 'REG.B.EXTERNAL.OP.R',
            'REG.B.S' => 'REG.B.EXTERNAL.OP.S',
            'REG.B.T' => 'REG.B.EXTERNAL.OP.T',
            'REG.I.CLBW'=>'REG.I.INTERNAL.AUXPOLE1_HZ',
            'REG.I.CLBW2'=>'REG.I.INTERNAL.AUXPOLES2_HZ',
            'REG.I.MANUAL.R'=>'REG.I.EXTERNAL.OP.R',
            'REG.I.MANUAL.S'=>'REG.I.EXTERNAL.OP.S',
            'REG.I.MANUAL.T'=>'REG.I.EXTERNAL.OP.T',
            'REG.I.MANUAL.TRACK_DELAY' => 'REG.I.EXTERNAL.TRACK_DELAY_PERIODS',
            'REG.I.PERIOD_DIV'=>'REG.I.PERIOD_ITERS',
            'REG.I.PURE_DELAY'=>'REG.I.INTERNAL.PURE_DELAY',
            'REG.I.Z'=>'REG.I.INTERNAL.AUXPOLES2_Z',
            'REG.MODE_INIT'=>'REG.MODE_CYC',
        }
    },

    # from class (A)
    LS2_92 =>
    {
        # to class (B)
        92 =>
        {
            'LIMITS.I.MIN' => 'LIMITS.I.STANDBY',
            'REG.I.CLBW'=>'REG.I.INTERNAL.AUXPOLE1_HZ',
            'REG.I.CLBW2'=>'REG.I.INTERNAL.AUXPOLES2_HZ',
            'REG.I.Z'=>'REG.I.INTERNAL.AUXPOLES2_Z',
            'REG.MODE_INIT' => 'REG.MODE_CYC',
        }
    },
};


# properties which did not exist in class A but exist in class B
my $new_properties =
{
    # from class (A)
    61 =>
    {
        # to class (B)
        63 =>
        {
            # property name -> default value to init with
            'ADC.EXTERNAL.SIGNALS'=>'I_DCCT_A,I_DCCT_B',
            'ADC.EXTERNAL.TYPE'=>'NONE,NONE',
            'B_PROBE.SELECT'=>'A',
            'B_PROBE.A.GAIN'=>'1.0000000E+00',
            'B_PROBE.B.GAIN'=>'1.0000000E+00',
            'DEVICE.MULTI_PPM'=>'DISABLED',
            'I_PROBE.CAPA.GAIN'=>'1.0000000E+01',
            'INTER_FGC.BROADCAST'=>'DISABLED',
            'INTER_FGC.ISR_OFFSET_US'=>'400',
            'INTER_FGC.SIG_SOURCES'=>'NONE',
            'INTER_FGC.MASTER'=>'NONE',
            'INTER_FGC.SLAVES'=>'NONE',
            'INTER_FGC.TOPOLOGY'=>'NONE',
            'INTER_FGC.CONSUMERS'=>'NONE',
            'INTER_FGC.PRODUCED_SIGS'=>'NONE',
            'INTER_FGC.VS_PRESENT'=>'ENABLED',
            'LIMITS.B.CLOSELOOP'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'LIMITS.B.NEG'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'LIMITS.B.STANDBY'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'LIMITS.I.LOAD.RMS_FAULT'=>'0.0000000E+00',
            'LIMITS.I.LOAD.RMS_TC'=>'0.0000000E+00',
            'LIMITS.I.LOAD.RMS_WARNING'=>'0.0000000E+00',
            'LIMITS.P.POS'=>'0',
            'LIMITS.P.NEG'=>'0',
            'LIMITS.V.RATE_RMS_TC'=>'0.0000000E+00',
            'LIMITS.V.RATE_RMS_FAULT'=>'0.0000000E+00',
            'LOAD.I_SAT_GAIN'=>'1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
            'LOAD.SAT_SMOOTHING'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'LOAD.TEST_SELECT'=>'0',
            'LOG.OASIS.SUBSAMPLE'=>'1',
            'MEAS.B.DELAY_ITERS'=>'1.0000000E+00',
            'MEAS.B.FIR_LENGTHS'=>'0,0',
            'MEAS.I.DELAY_ITERS'=>'1.0000000E+00',
            'MEAS.I.FIR_LENGTHS'=>'0,0',
            'MEAS.V.DELAY_ITERS' => '5.0000000E-01',
            'MODE.POST_FUNC'=>'MINRMS',
            'MODE.PRE_FUNC'=>'RAMP',
            'MODE.RT_NUM_STEPS' => '0',
            'MODE.SLOW_ABORT_FAULTS' => 'NONE',
            'MODE.USE_ARM_NOW'=>'DISABLED',
            'REF.DEFAULTS.B.LINEAR_RATE'=>'1.0000000E+00',
            'REF.DEFAULTS.B.MIN_RMS'=>'0.0000000E+00',
            'REF.DEFAULTS.B.PRE_FUNC_MAX'=>'0.0000000E+00',
            'REF.DEFAULTS.B.PRE_FUNC_MIN'=>'0.0000000E+00',
            'REF.DEFAULTS.I.MIN_RMS'=>'0.0000000E+00',
            'REF.DEFAULTS.I.PRE_FUNC_MAX'=>'0.0000000E+00',
            'REF.DEFAULTS.I.PRE_FUNC_MIN'=>'0.0000000E+00',
            'REF.DEFAULTS.PLATEAU_DUR'=>'0.0000000E+00',
            'REF.DEFAULTS.V.MIN_RMS'=>'0.0000000E+00',
            'REF.DEFAULTS.V.PRE_FUNC_MAX'=>'0.0000000E+00',
            'REF.DEFAULTS.V.PRE_FUNC_MIN'=>'0.0000000E+00',
            'REF.DIRECT.DEGAUSS_PERIOD'=>'0.0000000E+00',
            'REF.DIRECT.DEGAUSS_PP'=>'0.0000000E+00',
            'REF.EXTERNAL.GAIN'=>'1.0000000E+00',
            'REG.B.INTERNAL.AUXPOLE1_HZ'=>'1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
            'REG.B.INTERNAL.AUXPOLES2_HZ'=>'1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
            'REG.B.INTERNAL.AUXPOLES2_Z'=>'1.0000000E-01,1.0000000E-01,1.0000000E-01,1.0000000E-01',
            'REG.B.EXTERNAL.OP.R'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.B.EXTERNAL.OP.S'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.B.EXTERNAL.OP.T'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.B.EXTERNAL_ALG'=>'DISABLED,DISABLED,DISABLED,DISABLED',
            'REG.B.EXTERNAL.CLBW'=>'1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
            'REG.B.EXTERNAL.MOD_MARGIN'=>'5.0000000E-01,5.0000000E-01,5.0000000E-01,5.0000000E-01',
            'REG.B.EXTERNAL.Z'=>'1.0000000E-01,1.0000000E-01,1.0000000E-01,1.0000000E-01',
            'REG.B.INTERNAL.MEAS_SELECT'=>'UNFILTERED,UNFILTERED,UNFILTERED,UNFILTERED',
            'REG.B.EXTERNAL.MEAS_SELECT'=>'UNFILTERED,UNFILTERED,UNFILTERED,UNFILTERED',
            'REG.B.INTERNAL.PURE_DELAY_PERIODS'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.B.PERIOD_ITERS'=>'100,100,100,100',
            'REG.B.EXTERNAL.TRACK_DELAY_PERIODS'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.I.EXTERNAL_ALG'=>'DISABLED,DISABLED,DISABLED,DISABLED',
            'REG.I.EXTERNAL.CLBW'=>'1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
            'REG.I.EXTERNAL.MOD_MARGIN'=>'5.0000000E-01,5.0000000E-01,5.0000000E-01,5.0000000E-01',
            'REG.I.EXTERNAL.Z'=>'1.0000000E-01,1.0000000E-01,1.0000000E-01,1.0000000E-01',
            'REG.I.INTERNAL.MEAS_SELECT'=>'UNFILTERED,UNFILTERED,UNFILTERED,UNFILTERED',
            'REG.I.EXTERNAL.MEAS_SELECT'=>'UNFILTERED,UNFILTERED,UNFILTERED,UNFILTERED',
            'REG.I.INTERNAL.PURE_DELAY_PERIODS'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.V.EXTERNAL.K_INT'=>'1.0000000E+00',
            'REG.V.EXTERNAL.K_P'=>'1.0000000E+00',
            'REG.V.EXTERNAL.K_FF'=>'1.0000000E+00',
            'REG.V.EXTERNAL.CLBW'=>'1.0000000E+00',
            'REG.V.EXTERNAL.Z'=>'1.0000000E-01',
            'REG.V.FILTER.EXTERNAL.CLBW'=>'1.0000000E+00',
            'REG.V.FILTER.EXTERNAL.Z'=>'1.0000000E-01',
            'REG.V.FILTER.EXTERNAL.K_U'=>'1.0000000E+00',
            'REG.V.FILTER.EXTERNAL.K_I'=>'1.0000000E+00',
            'REG.V.FILTER.EXTERNAL.K_D'=>'1.0000000E+00',
            'REG.V.FILTER.FULL_I_CAPA'=>'DISABLED',
            'REG.V.FILTER.I_CAPA_SOURCE'=>'MEASUREMENT',
            'REG.V.FILTER.V_MEAS_SOURCE'=>'MEASUREMENT',
            'SWITCH.POLARITY.AUTO'=>'ENABLED',
            'VS.ACT_DELAY_ITERS'=>'1.0000000E+00',
            'VS.ACTUATION'=>'VOLTAGE_REF',
            'VS.DAC_CLAMP'=>'0.0000000E+00',
            'VS.FILTER.FARADS1'=>'0.0000000E+00',
            'VS.FILTER.FARADS2'=>'0.0000000E+00',
            'VS.FILTER.HENRYS'=>'0.0000000E+00',
            'VS.FILTER.OHMS'=>'0.0000000E+00',
            'VS.FIRING.MODE'=>'CASSEL',
            'VS.FIRING.DELAY'=>'1.0000000E+00',
            'VS.SIM.BANDWIDTH'=>'2.0000000E+02',
            'VS.SIM.Z'=>'7.0000000E-01',
        }
    },

    # from class (A)
    ELENA_63 =>
    {
        # to class (B)
        63 =>
        {
            'ADC.EXTERNAL.SIGNALS' => 'I_DCCT_A,I_DCCT_B',
            'ADC.EXTERNAL.TYPE' => 'NONE,NONE',
            'DEVICE.MULTI_PPM'=>'DISABLED',
            'LIMITS.P.POS' => '0',
            'LIMITS.P.NEG' => '0',
            'MEAS.V.DELAY_ITERS' => '5.0000000E-01',
            'MODE.RT_NUM_STEPS' => '0',
            'MODE.SLOW_ABORT_FAULTS' => 'NONE',
            'VS.DAC_CLAMP' => '0.0000000E+00',

            # these properties were already in ELENA, but were allowed to be null
            # we want to make sure, null values will be overridden
            'INTER_FGC.BROADCAST'=>'DISABLED',
            'INTER_FGC.ISR_OFFSET_US'=>'400',
            'INTER_FGC.SIG_SOURCES'=>'NONE',
            'INTER_FGC.MASTER'=>'NONE',
            'INTER_FGC.SLAVES'=>'NONE',
            'INTER_FGC.TOPOLOGY'=>'NONE',
            'INTER_FGC.CONSUMERS'=>'NONE',
            'INTER_FGC.PRODUCED_SIGS'=>'NONE',
            'INTER_FGC.VS_PRESENT'=>'ENABLED',
            'MEAS.B.DELAY_ITERS'=>'1.0000000E+00',
            'MEAS.B.FIR_LENGTHS'=>'0,0',
            'MEAS.I.DELAY_ITERS'=>'1.0000000E+00',
            'MEAS.I.FIR_LENGTHS'=>'0,0',
            'MEAS.V.DELAY_ITERS' => '5.0000000E-01',
            'REF.DEFAULTS.PLATEAU_DUR'=>'0.0000000E+00',
        }
    },

    LS2_51 => { 51 => { } },
    LS2_53 => { 53 => { } },
    LS2_63 => { 63 => { } },
    LS2_92 => { 92 => { } },

    # from class (A)
    51 =>
    {
        # to class (B)
        92 =>
        {
            'REG.I.EXTERNAL.CLBW'=>'1.0000000E+00,1.0000000E+00,1.0000000E+00,1.0000000E+00',
            'REG.I.EXTERNAL.MEAS_SELECT'=>'UNFILTERED,UNFILTERED,UNFILTERED,UNFILTERED',
            'REG.I.EXTERNAL.MOD_MARGIN'=>'5.0000000E-01,5.0000000E-01,5.0000000E-01,5.0000000E-01',
            'REG.I.EXTERNAL.TRACK_DELAY_PERIODS'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.I.EXTERNAL.Z'=>'1.0000000E-01,1.0000000E-01,1.0000000E-01,1.0000000E-01',
            'REG.I.EXTERNAL_ALG'=>'DISABLED,DISABLED,DISABLED,DISABLED',
            'REG.I.INTERNAL.MEAS_SELECT'=>'EXTRAPOLATED,EXTRAPOLATED,EXTRAPOLATED,EXTRAPOLATED',
            'REG.I.INTERNAL.PURE_DELAY_PERIODS'=>'0.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00',
            'REG.I.PERIOD_ITERS'=>'2,2,2,2',
            'REG.MODE_CYC' => 'I,I,I,I'

        }
    },
};

# used for removing properties explicitly even though they might be part of the Properties.pm (fgc_properties)
# this is for example useful to clean non-settable properties from config files
my $properties_to_be_explicitly_removed =
{
    # from class (A)
    LS2_53 =>
    {
        # to class (B)
        53 =>
        {
            'REG.B.PERIOD' => 1,
            'REG.I.PERIOD' => 1,
        }
    },
};

# sometimes we want to remove a property to trigger an unconfigured warning explicitly
# for example if the format of the property value changed and is not easily convertible and the value is different from 0
my $properties_to_be_removed_if_true =
{
    # from class (A)
    LS2_63 =>
    {
        # to class (B)
        63 =>
        {
            # old property (class b) -> test function to determine removal of the property
            'REG.B.TRACK_DELAY' => sub { my ($property_value) = @_; return $property_value != 0; },
            'REG.I.MANUAL.TRACK_DELAY' => sub { my ($property_value) = @_; return $property_value != 0; },
        }
    },
};


# properties whose symlist values have changed from class A to class B
my $adjusted_symlist_properties =
{
    # from class (A)
    61 =>
    {
        # to class (B)
        63 =>
        {
            # to_class property -> [old_sym_value, new_sym_value]
            'ADC.INTERNAL.SIGNALS' => ['V_LOAD', 'V_MEAS']
        }
    },
};


# properties whose limits have changed from class A to class B
my $adjusted_limited_properties =
{
    # from class (A)
    61 =>
    {
        # to class (B)
        63 =>
        {
            # property -> [new_min, new_max]
            'REG.I.INTERNAL.AUXPOLE1_HZ' => ['1.0000000E-03', '1.0000000E+06'],
            'REG.I.INTERNAL.AUXPOLES2_Z' => ['1.0000000E-01', '1.0000000E+00'],
            'ADC.INTERNAL.TAU_TEMP' => ['1.0000000E+01', '1.0000000E+03'],

            # this property already existed in class 61, but was not used. so we make sure to set it to 0.
            'LIMITS.B.POS' => ['0.0000000E+00', '0.0000000E+00'],
        }
    },

    # from class (A)
    ELENA_63 =>
    {
        # to class (B)
        63 =>
        {
            # property -> [new_min, new_max]
            'ADC.INTERNAL.TAU_TEMP' => ['1.0000000E+01', '1.0000000E+03'],
        }
    },
};

# properties whose numeric values need to be converted from class A to class B
my $adjusted_numeric_properties =
{
    # from class (A)
    LS2_51 =>
    {
        # to class (B)
        51 =>
        {
            # new property (class b) -> function to apply to value for conversion
            'VS.DAC_CLAMP' => sub { my ($old_val) = @_; return $old_val / 10.0; },
        }
    },
};


sub isEmpty
{
    return (!defined($_[0]) || !length($_[0]));
}


# TODO: Extend the script to support migrations for other classes, too. See EPCCCS-7141
# Therefore, the above defined hashs need to be extended and the combination needs to be added to the supported_migrations hash below.
sub migrationIsSupported
{
    my ($source_class_id, $target_class_id) = @_;

    # supported source class to target classes
    my %supported_migrations = (
        '51' => ['92'],
        '61' => ['63'],
        'ELENA_63' => ['63'],
        'LS2_51' => ['51'],
        'LS2_53' => ['53'],
        'LS2_63' => ['63'],
        'LS2_92' => ['92'],
    );

    my $supported_targets = $supported_migrations{$source_class_id};

    return defined($supported_targets) && FGC::Utils::array_contains_element($target_class_id, $supported_targets);
}


sub printHelpAndDie
{
    print "Usage: $0 <filename> --from=<class_id_a> --to=<class_id_b> --devices=<comma_sep_list> [--db_user=<username> --db_pw=<pw>]\n";
    print "\tor\n";
    print "Usage: $0 --from=<class_id_a> --to=<class_id_b> --old_config=<filename> --new_config=<filename>\n";
    die "";
}



sub getOpts
{
    my @opts    = @ARGV;
    my %options = ();

    GetOptionsFromArray(\@opts, \%options, 'db_user:s', 'db_pw:s', 'devices:s', 'old_config:s', 'new_config:s', 'from:s', 'to:s');

    if(isEmpty($options{from}) || isEmpty($options{to}))
    {
        printHelpAndDie();
    }

    if(!migrationIsSupported($options{from}, $options{to}))
    {
        die "Migrations from class $options{from} to class $options{to} are not supported.\n";
    }

    # If 'devices' argument is specified, we will alter the database
    # Otherwise, we are going to migrate a config file

    if(not isEmpty($options{devices}))
    {
        printHelpAndDie() if(@opts != 1);

        ($options{fgc_properties}) = @opts;

        return \%options;
    }

    # devices not specified, so we migrate config file

    if(isEmpty($options{old_config}) || isEmpty($options{new_config}))
    {
        printHelpAndDie();
    }

    return \%options;
}


sub migrate
{
    my ($all_sys_props, $from_class, $to_class) = @_;

    # local copy to add device_specific values of old properties
    my %device_new_props = %{$new_properties->{$from_class}->{$to_class}};

    # hash to keep track of properties to be removed from DB
    my %device_old_props = ();

    for my $sys_prop (sort(keys(%$all_sys_props)))
    {
        # get current value of the property (from db)
        my $prop_value = $all_sys_props->{$sys_prop};

        my $used_by_new_class = defined($fgc_properties{$sys_prop}->{classes}->{$to_class});
        my $renamed_in_new_class = defined($renamed_properties->{$from_class}->{$to_class}->{$sys_prop});
        my $to_be_explicitly_removed = defined($properties_to_be_explicitly_removed->{$from_class}->{$to_class}->{$sys_prop});

        my $to_be_removed_if_true = 0;
        if(defined($properties_to_be_removed_if_true->{$from_class}->{$to_class}->{$sys_prop}))
        {
            my $test_function = $properties_to_be_removed_if_true->{$from_class}->{$to_class}->{$sys_prop};
            $to_be_removed_if_true = $test_function->($prop_value);
        }

        # track the to-be-deleted properties
        if(!$used_by_new_class || $to_be_explicitly_removed || $to_be_removed_if_true)
        {
            $device_old_props{$sys_prop} = $prop_value;
        }

        # skip migration of property if not used and not to be renamed
        next if(!$used_by_new_class && !$renamed_in_new_class);

        # skip migration of property if it shall be explicitly removed
        next if($to_be_explicitly_removed || $to_be_removed_if_true);

        # migrate property name, if there is a mapping from old name to new name
        if($renamed_in_new_class)
        {
            $sys_prop = $renamed_properties->{$from_class}->{$to_class}->{$sys_prop};
        }

        # do not overwrite default values with null values, if property is already defined
        if(!defined($prop_value))
        {
            if(defined($device_new_props{$sys_prop}))
            {
                next;
            }
            else
            {
                print("\tWARNING: $sys_prop will be initialized with <null>\n");
            }
        }

        # adjust property value for specific properties
        else
        {
            $prop_value = adjustPropValue($sys_prop, $prop_value, $from_class, $to_class);
        }

        # set the new property-value pair
        $device_new_props{$sys_prop} = $prop_value;
    }

    return (\%device_new_props, \%device_old_props);
}


# the values of some properties need to be adjusted when migrating from class A to class B
# as they are treated differently. e.g. different ranges
sub adjustPropValue
{
    my ($property, $value, $from_class, $to_class) = @_;

    return undef if(!defined($property) || !defined($value));

    # adjust symlist values
    for my $adjusted_symlist_property (keys(%{$adjusted_symlist_properties->{$from_class}->{$to_class}}))
    {
        # symlist from class A to class B changed for $adjusted_symlist_property.
        # so we need to convert values containing '$from_sym_val' to '$to_sym_val'
        if($property eq $adjusted_symlist_property)
        {
            my ($from_sym_val, $to_sym_val) = @{$adjusted_symlist_properties->{$from_class}->{$to_class}->{$adjusted_symlist_property}};
            $value =~ s/$from_sym_val/$to_sym_val/g;
        }
    }

    # adjust limited values
    for my $adjusted_limited_property (keys(%{$adjusted_limited_properties->{$from_class}->{$to_class}}))
    {
        # the min/max limit for property changed from class A to class B.
        # Replace values by respective min/max if exceeding
        if($property eq $adjusted_limited_property)
        {
            my ($min, $max) = @{$adjusted_limited_properties->{$from_class}->{$to_class}->{$adjusted_limited_property}};

            # split value in multiple values if comma separated (e.g. load select parameters)
            my @values = index($value, ",") != -1 ? split(',', $value) : ($value);

            for (my $i = 0; $i < scalar(@values); $i++)
            {
                $values[$i] = $min if($values[$i] <= $min);
                $values[$i] = $max if($values[$i] >= $max);
            }

            $value = join(',', @values);
        }
    }

    # adjust numeric values by applying the specified conversion functions
    for my $adjusted_numeric_property (keys(%{$adjusted_numeric_properties->{$from_class}->{$to_class}}))
    {
        if($property eq $adjusted_numeric_property)
        {
            my $conversion_func = $adjusted_numeric_properties->{$from_class}->{$to_class}->{$adjusted_numeric_property};

            # split value in multiple values if comma separated (e.g. load select parameters)
            my @values = index($value, ",") != -1 ? split(',', $value) : ($value);

            for (my $i = 0; $i < scalar(@values); $i++)
            {
                $values[$i] = $conversion_func->($values[$i]);
            }

            $value = join(',', @values);
        }
    }

    # properties with the DONT_SHRINK flag are forced to have as many values as their maxels attribute specifies
    # so if they have less, we fill the rest with the value of the last element
    if(defined($fgc_properties{$property}) && defined($fgc_properties{$property}->{classes}->{$to_class}) && $fgc_properties{$property}->{classes}->{$to_class}->{flags}->{DONT_SHRINK})
    {
        my $maxels = $fgc_properties{$property}->{classes}->{$to_class}->{maxels};
        my @values = index($value, ",") != -1 ? split(',', $value) : ($value);
        for(my $i = (scalar @values); $i < $maxels; $i++)
        {
            push @values, $values[$i-1];
        }
        $value = join(',', @values);
    }

    return $value;
}


sub migrateDb
{
    my ($filename, $user_devices, $username, $password, $from_class, $to_class) = @_;

    die "Could not open FGC_properties.csv file (argument passed: $filename)" unless (-e $filename);

    # Devices to be migrated from fromclass to toclass
    $user_devices = uc($user_devices);
    my @selected_devices = uniq(split(",", $user_devices));

    # Read name file for perfoming checks on selected devices
    my ($devices, $gateways) = FGC::Names::read();
    die "Unable to read FGC name file\n" if(!defined($devices));

    # Ask user for name and pw if not provided in options
    if(!defined($username))
    {
        # Prompt for username
        print "\nUsername: ";
        chomp($username = <STDIN>);
    }
    if(!defined($password))
    {
        # Prompt for password
        print "Password: ";
        system("stty -echo");
        chomp($password = <STDIN>);
        system("stty echo");
        print "\n\n";
    }

    # make sure system properties for to_class are initialized
    my $import_system_properties_pl = File::Spec->catfile($Bin, 'import_system_properties.pl');
    system("$import_system_properties_pl", "$filename", $to_class, "--db_user=$username", "--db_pw=$password", "--devices=$user_devices");

    # Check db credentials and connect to DB
    my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

    # Perform migration (copy property values)
    for my $device_name (@selected_devices)
    {
        my $warning;
        print $warning = "WARNING: Unable to find device $device_name. Not included in name file.\n" if(!defined($devices->{$device_name}));
        print $warning = "WARNING: Unable to migrate device $device_name. Not of class $to_class in name file.\n" if(!$warning && $devices->{$device_name}->{class} != $to_class);
        next if($warning);

        print("Migrating system properties from FGC_$from_class to FGC_$to_class for $device_name...\n");

        # get current system properties of this device from DB
        my $all_sys_props = FGC::DB::get_system_properties($dbh, $device_name);

        my ($device_new_props, $device_old_props) = migrate($all_sys_props, $from_class, $to_class);

        # update values of the system properties (using old values for migrated properties and default values for new properties)
        FGC::DB::update_system_property_values($dbh, $device_name, $device_new_props);

        # remove old properties (not used by to_class)
        FGC::DB::remove_system_properties($dbh, $device_name, $device_old_props);
    }

    # Disconnect from DB
    FGC::DB::disconnect($dbh);
}


sub migrateFile
{
    my ($input_file, $output_file, $from_class, $to_class) = @_;

    my @lines      = read_file($input_file);
    my %properties = ();

    foreach my $line (@lines)
    {
        # Get rid of leading "! S ";

        $line =~ s/! S //;

        my ($property, $value) = split(" ", $line);

        if(!defined($property) || !defined($value)){
            next;
        }

        $properties{$property} = $value;
    }

    my ($new_props, $old_props) = migrate(\%properties, $from_class, $to_class);

    # Save only properties for new class in the output file

    open(my $file_handle, '>', $output_file);

    foreach my $property (sort keys %$new_props)
    {
        printf $file_handle "! S %-30s %s\n", $property, $new_props->{$property};
    }

    close($file_handle);
}

# Start main program
my $options = getOpts();


if($options->{devices})
{
    migrateDb($options->{fgc_properties}, $options->{devices}, $options->{db_user}, $options->{db_pw}, $options->{from}, $options->{to});
}
else
{
    # if directory was specified, perform migration for all config files in the directory
    if(-d $options->{old_config} && -d $options->{new_config})
    {
        opendir(DIR, $options->{old_config});
        my @files = grep(/\.txt$/, readdir(DIR));
        closedir(DIR);

        for my $file (@files)
        {
            my $src = $options->{old_config}."/".$file;
            my $target = $options->{new_config}."/".$file;
            migrateFile($src, $target, $options->{from}, $options->{to});
        }
    }

    # migrate single file
    else
    {
        migrateFile($options->{old_config}, $options->{new_config}, $options->{from}, $options->{to});
    }
}

# EOF
