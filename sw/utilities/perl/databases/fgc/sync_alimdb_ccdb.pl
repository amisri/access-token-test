#!/usr/bin/perl -w
#
# Name:     sync_alimdb_ccdb.pl
# Purpose:  syncing AlimDB with CCDB to be ran via Bamboo
# Author:   Dimitrios Milios

use strict;
use warnings;

use FGC::Consts;
use FGC::DB;
use DBI;
use Carp;
use Getopt::Long;

my $username;
my $password;

GetOptions('db_user:s'=>\$username, 'db_pw:s'=>\$password);

# Ask user for name and pw if not provided in options
if(!defined($username) || !defined($password))
{
    # Prompt for username

    print "\nUsername: ";
    chomp($username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp($password = <STDIN>);
    system("stty echo");
    print "\n\n";
}

my $dbh =DBI->connect(FGC::DB::FGC_DB_ID, $username, $password, {
        PrintError => 1,
        PrintWarn  => 1,
        RaiseError => 1,
        AutoCommit => 1,
        LongReadLen => FGC::Consts::FGC_MAX_DB_PROP_SIZE_BYTES
    }
);

my $query;

print "syncing CCDB with data from AlimDB ...\n";

# Import new types
eval {
  $query = $dbh->prepare("call pocontrols.data_imports.insert_new_type('ALIM')");
  $query->execute();
};

if ($@) {
   FGC::DB::disconnect($dbh);
   #confess($@);
   confess("error: could not execute insert_new_type procedure for syncing AlimDB with CCDB");
}


# Import new type descriptions (labels)

eval {
  $query = $dbh->prepare("call pocontrols.data_imports.update_type_description('ALIM')");
  $query->execute();
};

if ($@) {
   FGC::DB::disconnect($dbh);
   #confess($@);	
   confess('error: could not execute update_type_description procedure for syncing AlimDB with CCDB');
}

# Import new equipment

eval {
  $query = $dbh->prepare("call pocontrols.data_imports.insert_new_components('ALIM')");
  $query->execute();
};

if ($@) {
   FGC::DB::disconnect($dbh);
   #confess($@);		
   confess('error: could not execute insert_new_components procedure for syncing AlimDB with CCDB');
}

FGC::DB::disconnect($dbh);

#EOF





