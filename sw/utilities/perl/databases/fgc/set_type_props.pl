#!/usr/bin/perl -w
#
# Name:     set_type_props.pl
# Purpose:  Set type properties in database to values read from file
# Author:   Stephen Page

use strict;

use FGC::Consts;
use FGC::DB;
use FGC::Properties;

die "Usage: $0 <file name>\n" if(@ARGV != 1);
my ($filename) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

my %types;

open(FILE, '<', $filename) or die "Error opening file $filename: $!\n";
while(<FILE>)
{
    s/[\012\015]//g; # Remove newline

    next if /^$/;    # Ommit empty lines

    my ($type, $property, @values)              = split(',', $_);

    # Confirm that property is known

    die "ERROR: Property '' is not known" unless defined $FGC::Properties::fgc_properties{uc $property};

    # Extract property type.
    my $any_class = @{keys($FGC::Properties::fgc_properties{uc $property}->{classes})}[0];
    my $property_type = $FGC::Properties::fgc_properties{uc $property}->{classes}->{$any_class}->{type};

    # Format input values to scientific notation as it is stored in db in case of floats

    @values = map { sprintf '%.7E', $_ } @values if $property_type eq 'FLOAT';

    # Fill intermediate data structure

    $types{$type}->{properties}->{$property}    = join(',', @values);

    # Print some output to see what is going on

    print "$type, $property = $types{$type}->{properties}->{$property}\n";
}
close FILE;

# Connect to database

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

for my $type (sort(keys(%types)))
{
    my $properties = $types{$type}->{properties};

    print "Setting properties for type $type...\n";
    FGC::DB::update_type_property_values($dbh, $type, $properties);
}
FGC::DB::disconnect($dbh);

# EOF
