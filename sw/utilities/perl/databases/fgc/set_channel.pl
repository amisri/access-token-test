#!/usr/bin/perl -w

use FGC::DB;

die "Usage: $0 <file> <channel>\n" if(@ARGV != 2);
my ($file, $channel) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

open(FILE, "<", $file) or die "Failed to open file $file: $!\n";

my @barcodes;
while(<FILE>)
{
    s/[\012\015]//g; # Remove newline

    push(@barcodes, $_);
}
close(FILE);

my $dbh = FGC::DB::connect($username, $password);
die "Failed to connect to database\n" if(!defined($dbh));

FGC::DB::set_component_channel($dbh, $channel, \@barcodes);
FGC::DB::disconnect($dbh);

# EOF
