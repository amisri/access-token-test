#!/usr/bin/perl -w
#
# Name:     import_system_properties.pl
# Purpose:  Import system properties into FGC database
# Author:   Stephen Page

use strict;

use Getopt::Long;

use FGC::DB;
use FGC::Names;

my $devices;
my $gateways;

local $| = 1;

sub read_properties($$)
{
    my ($filename, $class) = @_;

    open(FILE, '<', $filename) or die "Unable to open $filename: $!\n";

    my %properties;

    my ($usage, $property_name, $property_class);
    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline

        ($usage, $property_name, $property_class) = split(',', $_);

        next if($usage ne 'SYS' || $property_class != $class);

        $properties{$property_name}->{name}     = $property_name;
        $properties{$property_name}->{usage}    = $usage;
        $properties{$property_name}->{class}    = $property_class || "";
    }
    close(FILE);

    return(\%properties);
}

# End of functions

my ($username, $password, $user_devices);
GetOptions('db_user:s'=>\$username, 'db_pw:s'=>\$password, 'devices:s'=>\$user_devices);

die "Usage: $0 <filename> <class> [--db_user=username --db_pw=pw --devices=comma_sep_list]\n" if(@ARGV != 2);
my ($filename, $class) = @ARGV;

# Ask user for name and pw if not provided in options
if(!defined($username) || !defined($password))
{
    # Prompt for username

    print "\nUsername: ";
    chomp($username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp($password = <STDIN>);
    system("stty echo");
    print "\n\n";
}

# Read names

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Only work with devices the user optionally specified

if($user_devices)
{
    $user_devices = uc($user_devices);
    my @selected_devices = split(",", $user_devices) ;
    for my $file_device_ref (keys(%$devices)) 
    {
        # delete device from hash if not part of the user specified devices

        my $file_device = $devices->{$file_device_ref};
        if(!grep(/^$file_device->{name}$/, @selected_devices))
        {
            delete $devices->{$file_device_ref};
        }
    }
}

# Read properties

my $properties = read_properties($filename, $class);

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

my $systems;
if(!($systems = FGC::DB::get_system_names($dbh)))
{
    FGC::DB::disconnect($dbh);
    die "Unable to get system names: $!\n";
}

my @sorted_properties = sort { $a->{name} cmp $b->{name} } values(%$properties);

for my $property (@sorted_properties)
{
    my $result = FGC::DB::add_property($dbh, $property->{name});
    
    if(!defined($result))
    {
        FGC::DB::disconnect($dbh);
        die "ERROR: Unable to add property $property->{name}\n";
    } 
    elsif ($result != 0)
    {
         print "Property $property->{name} added\n";
    }
    
}

my $count = 1;
my $total = scalar(keys(%$devices));
for my $system (sort { $a->{name} cmp $b->{name} } (values(%$devices)))
{  
    # Show progress
        
    print "\r$count/$total Importing properties for system $system->{name}                                              "; 
    $count++;

    next if($system->{class} ne $class);

    for my $property (@sorted_properties)
    {
        # EPCCCS-4566 Set default value for VREF_TEMP.REF. Simplify some misunderstandings regarding who should set this value.

        my $condVal = ($property->{name} eq "ADC.INTERNAL.VREF_TEMP.REF") ? "4.00000E+01" : undef;

        # Insert system property 

        my $result = FGC::DB::add_system_property($dbh, $system->{name}, $property->{name}, $condVal);
        
        if(!defined($result))
        {
            FGC::DB::disconnect($dbh);
            die "ERROR: Unable to add property $property->{name} to system $system->{name}\n";
        }
        elsif ($result != 0)
        {
             print "\n\tProperty $property->{name} added to system $system->{name}\n";
             
        }
       
    }
}
FGC::DB::disconnect($dbh);

print "\nDONE\n";

# EOF
