#!/usr/bin/perl -w
#
# Name:     update_systems.pl
# Purpose:  Updates fields of systems table on base of name file
# Author:   Kevin Kessler

use strict;

use FGC::DB;
use FGC::Names;

my $devices;
my $gateways;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read names

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

# read types from DB
my $db_types_rows;
if(!($db_types_rows = FGC::DB::get_types($dbh)))
{
    FGC::DB::disconnect($dbh);
    die "Unable to get types: $!\n";
}

# hash types once by id and once by codes
my %db_types;
for my $db_type (@$db_types_rows)
{
    $db_types{$db_type->{TP_ID}} = $db_type;
}

# read systems from DB
my $db_systems_rows;
if(!($db_systems_rows = FGC::DB::get_systems($dbh)))
{
    FGC::DB::disconnect($dbh);
    die "Unable to get systems: $!\n";
}

# Update DB systems based on namefile, if necessary
for my $db_system (@$db_systems_rows)
{
    my $device = $devices->{$db_system->{SYS_NAME}};

    # Device not in Name File -> attributes don't need to be updated
    if(!defined($device))
    {
        next;

        # Remove device?
        #print "Removing device $db_system->{SYS_NAME} from database...\n";
        #todo: delete or die "Unable to remove device $db_system->{SYS_NAME} from POControls Database: $!\n";
    }

    # associate db_type info to db_system
    my $db_type = $db_types{$db_system->{SYS_TP_ID}} if(defined($db_system->{SYS_TP_ID}));
    $db_system->{type} = $db_type->{TP_EQUIPMENT_CODE} if(defined($db_type));

    # Check whether device attributes have changed.
    # TODO: do the same for class, once it is part of the db system table
    if(defined($device->{type})  && !defined($db_system->{type}) ||  # Type has been removed
       !defined($device->{type}) && defined($db_system->{type})  ||  # Type has been added
       (defined($device->{type}) && defined($db_system->{type})  &&  $device->{type} ne $db_system->{type}) # Type has changed
      )
    {
        # Update the device entry

        print "\nUpdating device $db_system->{SYS_NAME} in database...\n";

        printf("FROM: NAME %s, TYPE %s\n",
               $db_system->{SYS_NAME},
               $db_system->{type} ? $db_system->{type} : '');

        printf("TO:   NAME %s, TYPE %s\n",
               $device->{name},
               $device->{type} ? $device->{type} : '');

        FGC::DB::update_system($dbh,
                               $device->{name},
                               $device->{type},
                               );
    }
}


FGC::DB::disconnect($dbh);

print "DONE\n";

# EOF
