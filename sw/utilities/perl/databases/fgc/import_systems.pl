#!/usr/bin/perl -w
#
# Name:     import_systems.pl
# Purpose:  Import systems into database
# Author:   Stephen Page

use strict;

use FGC::DB;
use FGC::Names;

my $devices;
my $gateways;

die "Usage: $0 <class>\n" if(@ARGV != 1);
my ($class) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read names

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

my $db_systems;
if(!($db_systems = FGC::DB::get_system_names($dbh)))
{
    FGC::DB::disconnect($dbh);
    die "Unable to get system names: $!\n";
}

my %db_systems = map { $_->{SYS_NAME} => 1 } @$db_systems;

for my $device (grep { $_->{class} == $class } values(%$devices))
{
    if(!defined($db_systems{$device->{name}}))
    {
        print "Adding system $device->{name} to database\n";
        if(!defined(FGC::DB::add_system($dbh, $device->{name}, $device->{type})))
        {
            warn "Failed to add system $device->{name} to database\n";
            FGC::DB::disconnect($dbh);
            exit;
        }
    }
}
FGC::DB::disconnect($dbh);

print "DONE\n";

# EOF
