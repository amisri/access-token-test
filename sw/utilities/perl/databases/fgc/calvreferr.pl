#!/usr/bin/perl -w

use strict;

use FGC::Consts;
use FGC::DB;

die "Usage: $0 <username> <password>\n" if(@ARGV != 2);
my ($username, $password) = @ARGV;

# Connect to database

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

# Prepare database query

my $query;
if(!($query = $dbh->prepare("SELECT CMP_MTF_ID, CPR_VALUE
                             FROM POCONTROLS.FGC_COMPONENTS, POCONTROLS.FGC_PROPERTIES, POCONTROLS.FGC_COMPONENT_PROPERTIES
                             WHERE CPR_CMP_ID=CMP_ID AND CPR_PRO_ID=PRO_ID AND PRO_NAME='CAL.VREF.ERR'")))
{
    FGC::DB::disconnect($dbh);
    die "Failed to prepare database query\n";
}

# Execute database query

if(!$query->execute())
{
    FGC::DB::disconnect($dbh);
    die "Failed to execute database query\n";
}

# Retrieve results

my $results = $query->fetchall_arrayref({ CMP_MTF_ID => 1, CPR_VALUE => 1 });

for my $line (sort { $a->{CMP_MTF_ID} cmp $b->{CMP_MTF_ID} } @$results)
{
    next if(!defined($line->{CPR_VALUE}));

    my @values = split(',', $line->{CPR_VALUE});
    my $newval = $line->{CPR_VALUE};

    if(@values == 2)
    {
        $newval = "0.00000E+00,$values[0],$values[1]";
    }
    elsif(@values == 3 && $values[0] != 0 && $values[1] == $values[2])
    {
        $newval = "0.00000E+00,$values[0],$values[1]";
    }
    elsif(@values == 6)
    {
    }
    else
    {
        next;
    }

    if($newval ne $line->{CPR_VALUE})
    {
        printf("$line->{CMP_MTF_ID}     %75s     %75s\n", $line->{CPR_VALUE},$newval)   if($newval ne $line->{CPR_VALUE});
        FGC::DB::update_component_property_values($dbh, $line->{CMP_MTF_ID}, { "CAL.VREF.ERR" => $newval }) or die "Failed to update value for $line->{CMP_MTF_ID}\n";
    }
    #warn sprintf("$line->{CMP_MTF_ID}     %75s\n", $line->{CPR_VALUE}) if($newval eq $line->{CPR_VALUE});
}
FGC::DB::disconnect($dbh);

# EOF
