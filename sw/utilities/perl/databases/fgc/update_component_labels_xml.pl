#!/usr/bin/perl -w
#
# Name:     update_component_labels_xml.pl
# Purpose:  Extract type labels from FGC database and generate XML file
# Author:   Stephen Page

use strict;

use File::Slurp;
use FGC::Consts;
use FGC::DB;

die("usage: $0 <path_to_component_labels_xml>") if(@ARGV != 1);

my ($filename) = @ARGV;

my $old_xml = read_file($filename);

# Connect to database

my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD) or die "Unable to connect to database: $!\n";

# Prepare database query

my $query;
if(!($query = $dbh->prepare("SELECT TP_NAME, TP_DESCRIPTION FROM POCONTROLS.TYPES ORDER BY TP_NAME")))
{
    FGC::DB::disconnect($dbh);
    die "Failed to prepare database query\n";
}

# Execute database query

if(!$query->execute())
{
    FGC::DB::disconnect($dbh);
    die "Failed to execute database query\n";
}

# Retrieve results

my $results = $query->fetchall_arrayref({ TP_DESCRIPTION => 1, TP_NAME => 1 });

# Disconnect from database

FGC::DB::disconnect($dbh);

my $new_xml = sprintf("<?xml version='1.0'?>\n\n");
$new_xml.=    sprintf("<component_labels>\n");

for my $type (@$results)
{
    # Replace commas with spaces as they break the FGC communications protocol

    $type->{TP_DESCRIPTION} =~ s/,\s*/ /g;                          # Replace commas followed by spaces with a single space
    $type->{TP_DESCRIPTION} =~ s/&/+/g;                             # Replace ampersands with pluses
    $type->{TP_DESCRIPTION} =~ s/"//g;                              # Remove quotes
    $type->{TP_DESCRIPTION} =~ s/\261/+-/g;                         # Replace the single +- character with a pair
    $type->{TP_DESCRIPTION} =~ s/[\340\341\342\343\344\345]/a/g;    # Remove accents from 'a'
    $type->{TP_DESCRIPTION} =~ s/[\350\351\352\353]/e/g;            # Remove accents from 'e'
    $type->{TP_DESCRIPTION} =~ s/[\354\355\356\357]/i/g;            # Remove accents from 'i'
    $type->{TP_DESCRIPTION} =~ s/[\362\363\364\365\366]/o/g;        # Remove accents from 'o'
    $type->{TP_DESCRIPTION} =~ s/[\371\372\373\374]/u/g;            # Remove accents from 'u'

    $type->{TP_DESCRIPTION} =~ s/[\200-\377]//g;                    # Remove all remaining extended ASCII characters
    $type->{TP_DESCRIPTION} =~ s/\s+/ /g;                           # Replace multiple spaces with a single one

    $new_xml.=sprintf("    <component_label barcode = %-12s label = %-82s/>\n", "\"$type->{TP_NAME}\"", "\"$type->{TP_DESCRIPTION}\"");
}

$new_xml.=sprintf("</component_labels>\n\n");
$new_xml.=sprintf("<!-- EOF -->");

# We only overwrite the file if there have been changes. 
# In this way, any dependencies on this file in Makefiles will not be triggered unnecessarily
if( $old_xml ne $new_xml )
{
    print "updating component_labels.xml\n";
    write_file($filename, $new_xml);
}
else
{
    print "component_labels.xml already up to date\n";
}

# EOF
