#!/usr/bin/perl -w
#
# Name:     extract_type_labels.pl
# Purpose:  Extract type labels from FGC database and generate XML file
# Author:   Stephen Page

use strict;

use FGC::Components;
use FGC::Consts;
use FGC::DB;

die "Usage: $0 <property file>\n" if(@ARGV != 1);
my ($file) = @ARGV;

# Read property categories from file

my %property_category;

open(FILE, '<', $file) or die "Error opening property file $file: $!\n";
while(<FILE>)
{
    chomp;

    my ($group, $property, $category) = split(',', $_);
    next if($group ne 'TYPE');

    $property_category{$property} = $category;
}
close(FILE);

# Connect to database

my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD) or die "Unable to connect to database: $!\n";

# Prepare database query

my $query;
if(!($query = $dbh->prepare("SELECT TYPE_NAME, PROPERTY_NAME, TYPE_PROPERTY_VALUE
                             FROM POCONTROLS.TYPE_PROPERTIES_V
                             ORDER BY TYPE_NAME, PROPERTY_NAME")))
{
    FGC::DB::disconnect($dbh);
    die "Failed to prepare database query\n";
}

# Execute database query

if(!$query->execute())
{
    FGC::DB::disconnect($dbh);
    die "Failed to execute database query\n";
}

# Retrieve results

my $results = $query->fetchall_arrayref({ PROPERTY_NAME => 1, TYPE_NAME => 1, TYPE_PROPERTY_VALUE => 1 });

# Disconnect from database

FGC::DB::disconnect($dbh);

# Print table

print "Category,Type,Property,Value,Type label\n";
for my $type_property (sort { $property_category{$a->{PROPERTY_NAME}} cmp $property_category{$b->{PROPERTY_NAME}} } @$results)
{
    my $type    = $fgc_components{$type_property->{TYPE_NAME}};
    my $label   = defined($type) ? $type->{label} : "";
    my $value   = defined($type_property->{TYPE_PROPERTY_VALUE}) ? $type_property->{TYPE_PROPERTY_VALUE} : "";

    print "$property_category{$type_property->{PROPERTY_NAME}},$type_property->{TYPE_NAME},$type_property->{PROPERTY_NAME},$value,$label\n";
}

# EOF
