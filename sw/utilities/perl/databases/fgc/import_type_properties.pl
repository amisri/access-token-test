#!/usr/bin/perl -w
#
# Name:     import_type_properties.pl
# Purpose:  Import type properties into FGC database
# Author:   Stephen Page

use strict;

use FGC::Components;
use FGC::DB;

my %categories;

sub read_properties($)
{
    my ($filename) = @_;

    open(FILE, '<', $filename) or die "Unable to open $filename: $!\n";

    my %properties;

    my ($usage, $property_name, $category);
    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline

        ($usage, $property_name, $category) = split(',', $_);

        next if($usage ne 'TYPE');
        if(!defined($category) || $category eq "")
        {
            warn "Category not defined for property $property_name, ignoring\n";
            next;
        }

        $properties{$property_name}->{name}     = $property_name;
        $properties{$property_name}->{category} = $category;
        push(@{$properties{$property_name}->{categories}}, $category);
        push(@{$categories{$category}}, $properties{$property_name});
    }
    close(FILE);

    return(\%properties);
}

# End of functions


die "Usage: $0 <filename>\n" if(@ARGV != 1);
my ($filename) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Read properties

my $properties = read_properties($filename);

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

for my $property (sort {$a->{name} cmp $b->{name}} values(%$properties))
{
    my $result = FGC::DB::add_property($dbh, $property->{name});
     
    if(!FGC::DB::add_property($dbh, $property->{name}))
    {
        FGC::DB::disconnect($dbh);
        die "ERROR: Unable to add property $property->{name}: $!\n";
    }
    elsif ($result != 0)
    {
        print "Property $property->{name} added\n";
    }
}

for my $type (sort { $a->{barcode} cmp $b->{barcode} } (values(%fgc_components)))
{
    next if(!defined($type->{categories}));

    for my $category (@{$type->{categories}})
    {
        next if(!defined($categories{$category}));

        for my $property (@{$categories{$category}})
        {
            my $result = FGC::DB::add_type_property($dbh, $type->{barcode}, $property->{name}, undef);
            
            if (!defined($result))
            {
                FGC::DB::disconnect($dbh);
                die "ERROR: Unable to add property $property->{name}: $!\n";
            }
            elsif ($result != 0)
            {
                print "\tProperty $property->{name} added to type $type->{barcode}\n";
            }
            
        }
    }
}
FGC::DB::disconnect($dbh);

print "DONE\n";

# EOF
