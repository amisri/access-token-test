#!/usr/bin/perl -w
#
# Name:     set_component_props.pl
# Purpose:  Set component properties in database to values read from file
# Author:   Stephen Page

use strict;

use FGC::Consts;
use FGC::DB;

die "Usage: $0 <property> <file name>\n" if(@ARGV != 2);
my ($property, $filename) = @ARGV;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

my %component_value;

open(FILE, '<', $filename) or die "Error opening file $filename: $!\n";
while(<FILE>)
{
    s/[\012\015]//g; # Remove newline

    my ($component, @values)    = split(',', $_);

    # Format input values to scientific notation as it is stored in db

    my @values_formatted = map { sprintf '%.5E', $_ } @values;

    $component_value{$component}    = join(',', @values_formatted);
}
close(FILE);

# Connect to database

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";

for my $component (sort(keys(%component_value)))
{
    my $value = $component_value{$component};

    print "Setting $property=$value for $component...\n";
    FGC::DB::update_component_property_values($dbh, $component, { $property => $value });
}
FGC::DB::disconnect($dbh);

# EOF
