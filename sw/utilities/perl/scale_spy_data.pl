#!/usr/bin/perl -w
#
# Name:     scale_spy_data.pl
# Purpose:  Scale raw spy data into standard units based on factors read from a configuration file
# Author:   Stephen Page

use File::Basename;
use strict;
use Tk;

my $window  = new MainWindow;
$window->optionAdd('*BorderWidth' => 1);

# Read signal configuration file
#
# The file should contain a line per signal with the following comma-delimited fields: name,gain,offset

sub read_config($)
{
    my ($file_name) = @_;

    if(!open(FILE, "<", $file_name))
    {
        $window->messageBox(
                            -icon       => "error",
                            -message    => "Failed to open config file $file_name: $!",
                            -title      => "Error",
                            -type       => "ok",
                           );
        exit 1;
    }

    my @names  = ("TIME");
    my @config = { gain => 1, offset => 0 };

    my $line = 0;
    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline
        $line++;

        my @fields = split(",", $_);
        if(@fields != 3)
        {
            $window->messageBox(
                                -icon       => "error",
                                -message    => "Error in configuration file at line $line",
                                -title      => "Error",
                                -type       => "ok",
                               );
            exit 1;
        }

        my $name;
        my %sig_config;
        (
            $name,
            $sig_config{gain},
            $sig_config{offset},
        ) = @fields;

        push(@names,  $name);
        push(@config, \%sig_config);
    }
    close(FILE);

    return(\@names, \@config);
}

# Read data file, scale signals and write them to output data file

sub scale_data($$$)
{
    my ($names, $config, $in_file_name) = @_;

    (my $out_file_name = $in_file_name) =~ s/\.csv$/_scaled.csv/i;

    if(!open(IN_FILE, "<", $in_file_name))
    {
        $window->messageBox(
                            -icon       => "error",
                            -message    => "Failed to open input data file $in_file_name: $!",
                            -title      => "Error",
                            -type       => "ok",
                           );
        exit 1;
    }

    if(!open(OUT_FILE, ">", $out_file_name))
    {
        $window->messageBox(
                            -icon       => "error",
                            -message    => "Failed to open output data file $out_file_name: $!",
                            -title      => "Error",
                            -type       => "ok",
                           );
        exit 1;
    }

    # Check whether first line contains signal names

    $_ = <IN_FILE>;
    s/[\012\015]//g; # Remove newline

    if(/^[a-zA-Z]/) # Line starts with an alphabetic character
    {
        # Use old names as defaults

        my @old_names = split(",", $_);

        for(my $i = 0 ; $i < @old_names ; $i++)
        {
            if(!defined($names->[$i]))
            {
                $names->[$i] = $old_names[$i];
            }
        }
    }
    else # First line does not contain signal names
    {
        # Return to start of file

        seek(IN_FILE, 0, 0);
    }

    # Write signal names to output file

    print OUT_FILE join(",", @$names), "\n";

    while(<IN_FILE>)
    {
        s/[\012\015]//g; # Remove newline

        my @samples = split(",", $_);

        my $scaled_line = $samples[0]; # Timestamp
        for(my $i = 1 ; $i < @samples ; $i++)
        {
            $scaled_line .= ",";

            my $sig_config = $config->[$i];
            if(defined($sig_config))
            {
                # Scale sample

                $scaled_line .= sprintf("%.07E", ($samples[$i] * $sig_config->{gain}) + $sig_config->{offset});
            }
            else # No configuration for signal
            {
                # Copy sample without modification

                $scaled_line .= $samples[$i];
            }
        }
        print OUT_FILE $scaled_line, "\n";
    }
    close(IN_FILE);
    close(OUT_FILE);
}

# End of functions


# Prompt for data and configuration files

my $data_file;
exit 1 if(!defined(($data_file      = $window->getOpenFile(-title       => "Select data file",
                                                           -filetypes   => [["CSV files", ".csv"], ["All files", "*"]]))));
chdir(dirname($data_file));

my $config_file;
exit 1 if(!defined(($config_file    = $window->getOpenFile(-title       => "Select scaling configuration file",
                                                           -filetypes   => [["TXT files", ".txt"], ["All files", "*"]]))));

my ($names, $config) = read_config($config_file);
scale_data($names, $config, $data_file);

# EOF
