#!/usr/bin/perl -w
#
# Name:     read_fgc_code.pl
# Purpose:  Restoring advertised FGC codes on the gateway

use strict;
use warnings;

use Test::Utils;
use List::Util qw(first);

my @allowed_gateways = ("CFC-866-RFIP1",
                        "CFC-866-RFIP2",
                        "CFC-866-RFIP3",
                        "CFC-866-RETH1",
                        "CFC-866-RETH2",
                        "CFC-866-RETH5");

sub print_help()
{
    print "Usage: ./read_fgc_codes.pl --gateway=<gateway_name>\n";
    print "Where:\n";

    print "\tgateway:  ";

    foreach(@allowed_gateways)
    {
        print " $_";
    }

    print "\n\n";
}

# Get the input and validate it

my %params = (gateway=>undef, protocol=>'TCP');
   %params = parse_opts %params;

my $gateway = $params{gateway};

if(!defined $gateway)
{
    print_help() && die "ERROR: Missing gateway name";
}

# Check if the specified gateway is one of the development gateways

my $gateway_index = first { $allowed_gateways[$_] eq $gateway } 0..$#allowed_gateways;

if(!defined $gateway_index)
{
    print_help() && die "ERROR: Unallowed gateway specified";
}

authenticate('location');

# Check if the gateway name matches

ok(get($gateway, "DEVICE.NAME") eq $gateway, "Connected to the gateway");

set($gateway, "GW.READCODES", "");

ok(1, "Codes read");

# TODO Check code versions after READ

# EOF
