#!/usr/bin/perl

use File::Slurp;

use Test::FGC;

use strict;
use warnings;
use diagnostics;



sub printUsageMessage
{
    print "The script merges REF.TABLE.REF, REF.TABLE.REF_TIME and REF.FUNC.TYPE TABLE into a single REF.TABLE.FUNCTION property\n\n";

    print "Usage: $0 <input_file> <output_file>\n";
}



sub printZippedTable
{
    my ($file, $table_hash, $user) = @_;

    my @time_vector = split(/,/, $table_hash->{"REF_TIME($user)"});
    my @ref_vector  = split(/,/, $table_hash->{"REF($user)"});

    if(scalar @time_vector != scalar @ref_vector)
    {
        die "Ill formed config - Number of elements in REF.TABLE.REF and REF.TABLE.REF_TIME for user $user are different";
    }

    # Remove whitespaces from elements of time and reference vector

    for(my $i = 0; $i < scalar @time_vector; $i++)
    {
        $time_vector[$i] =~ s/^\s+|\s+$//g;
        $ref_vector[$i]  =~ s/^\s+|\s+$//g;
    }

    if($user == 0)
    {
        print $file "!S REF.TABLE.FUNCTION ";
    }
    else
    {
        print $file "!S REF.TABLE.FUNCTION($user) ";
    }

    print $file zipTable(\@time_vector, \@ref_vector) . "\n";
}



my ($source_file, $target_file) = @ARGV;

if(!defined $source_file || !defined $target_file)
{
    die "Source and/or target file name not specified";
}

open(my $file_out, ">$target_file") or die " Couldn't open file $target_file: $!";

my @lines = read_file $source_file;

my %table_hash;

foreach my $line (@lines)
{
    if($line =~ /!S REF.TABLE.REF(\(\d*?\))?\s(.*)/)
    {
        my $user = 0;

        # Remove brackes from user

        if(defined $1)
        {
            $user = $1;
            $user =~ tr/)(//d;
        }

        if(exists $table_hash{"REF($user)"})
        {
            die "Ill-formed config - multiple set commands for REF.TABLE.REF for user $user";
        }

        $table_hash{"REF($user)"} = $2;

        if(exists $table_hash{"REF_TIME($user)"})
        {
            printZippedTable($file_out, \%table_hash, $user);
        }
    }
    elsif($line =~ /!S REF.TABLE.REF_TIME(\(\d*?\))?\s(.*)/)
    {
        my $user = 0;

        # Remove brackes from user

        if(defined $1)
        {
            $user = $1;
            $user =~ tr/)(//d;
        }

        if(exists $table_hash{"REF_TIME($user)"})
        {
            die "Ill-formed config - multiple set commands for REF.TABLE.REF_TIME for user $user";
        }

        $table_hash{"REF_TIME($user)"} = $2;

        if(exists $table_hash{"REF($user)"})
        {
            printZippedTable($file_out, \%table_hash, $user);
        }
    }
    elsif($line =~ /!S REF.FUNC.TYPE\((\d*?)\)\sTABLE/)
    {
        if(!exists $table_hash{"REF($1)"} || !exists $table_hash{"REF_TIME($1)"})
        {
            die "Ill-formed config - REF.FUNC.TYPE set before REF.TABLE.REF and REF.TABLE.REF_TIME for user $1";
        }
    }
    else
    {
        print $file_out $line;
    }
}

# EOF