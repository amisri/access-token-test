#!/usr/bin/perl -w
#
# Name:     syncwebs.pl
# Purpose:  Performs synchronisation of web sites
# Author:   Stephen Page

use File::Copy;
use File::Find;
use strict;

die "Usage: $0 <source location> <destination location>\n" if(@ARGV != 2);
our ($source, $destination) = @ARGV;

if(! -d $source)
{
    die "Source ($source) is not a directory!\n";
}
elsif(! -d $destination)
{
    die "Destination ($destination) is not a directory!\n";
}

# Remove trailing slash from $source and $destination

$source         =~ s#/$##;
$destination    =~ s#/$##;

my @found_dirs;
my @found_files;

# Regular expressions matching files to leave untouched on target web site

my @ignore = ("_vti_pvt");

# Check whether source path is Unix or Windows and set find's follow flag appropriately

my $follow = ($source =~ /^\//) ? 1 : 0;

# Find all except ignored files

print STDERR "Getting list of current files in destination...";

@found_dirs     = ();
@found_files    = ();
find(
        {
            follow      => $follow,
            no_chdir    => 1,
            wanted      => \&fill_found_files,
        },
        $destination
);

print STDERR "DONE\n";

# Remove files

print STDERR "Removing ", scalar(@found_files), " files from destination...";
unlink @found_files;
print STDERR "DONE\n";

print STDERR "Removing ", scalar(@found_dirs), " directories from destination...";

my $dirname;

for $dirname (@found_dirs)
{
    rmdir $dirname;
}
print STDERR "DONE\n";

# Find source files to copy

print STDERR "Getting list of source files...";

@found_dirs     = ();
@found_files    = ();
find(
        {
            follow      => $follow,
            no_chdir    => 1,
            wanted      => \&fill_found_files,
        },
        $source
    );

# Remove source path from files

for(@found_files, @found_dirs)
{
    s/^\Q$source\E//;
}

print STDERR "DONE\n";

# Make directories

print STDERR "Making ", scalar(@found_dirs), " directories...";

for $dirname (@found_dirs)
{
    mkdir "$destination/$dirname";
}

print STDERR "DONE\n";

# Copy files

print STDERR "Copying ", scalar(@found_files), " files...";

my $filename;
for $filename (@found_files)
{
    copy("$source/$filename", "$destination/$filename")
        or die "Unable to copy file $source/$filename to $destination/$filename : $!\n";
}

print STDERR "DONE\n";

print STDERR "\nOperation complete.\n";
exit(0);


# Begin functions

sub fill_found_files
{
    # Check whether file should be ignored

    my $regexp;
    for $regexp (@ignore)
    {
        return if /$regexp/;
    }

    # Check whether entry is a directory

    if(-d $_)
    {
        push(@found_dirs, $_);
    }
    else
    {
        push(@found_files, $_);
    }
}

# EOF
