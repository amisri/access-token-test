#!/usr/bin/perl -w
#
# Name:     reprogram_fgc.pl
# Purpose:  Resets an FGC and waits for it to reprogram

use strict;
use warnings;
use diagnostics;

use Test::Utils;
use List::Util qw(first);

my @allowed_gateways = ("CFC-866-RFIP1",
                        "CFC-866-RFIP2",
                        "CFC-866-RFIP3",
                        "CFC-866-RETH1",
                        "CFC-866-RETH2",
                        "CFC-866-RETH5");

sub print_help()
{
    print "Usage: ./reprogram_fgc.pl --devices=\"<dev_name> [<dev_name> [...]]\"\n";
}



sub subscribeAll
{
    my @devices = @_;

    foreach my $fgc (@devices)
    {
        subscribe($fgc);
    }
}



sub unsubscribeAll
{
    my @devices = @_;

    #foreach my $fgc (@devices)
    #{
        unsubscribe($devices[0]);
    #}
}



sub isAnyReprogramming
{
    my @devices = @_;

    foreach my $fgc (@devices)
    {
        if(get_next_publication($fgc) =~ /STATE_OP:PROGRAMMING/)
        {
            return 1;
        }
    }

    return 0;
}



my %params = (devices=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my @fgcs = split " ", $params{devices};

if(!@fgcs)
{
    print_help() && die "ERROR: Missing FGC name(s)";
}

authenticate('location');

foreach my $fgc (@fgcs)
{
    my $state_pc = "";

    # This will fail when the device is in boot or when it doesn't have the PC state machine

    eval
    {
        $state_pc = get($fgc, "STATE.PC");
    };

    if($state_pc !~ /OFF/)
    {
        assureMain($fgc);

        if(get($fgc, "DEVICE.CLASS_ID") != 59)
        {
            if(get($fgc, "DEVICE.CLASS_ID") == 53)
            {
                set($fgc, "MODE.PC", "OFF");
                wait_until(sub { get($fgc, "STATE.PC") eq "FLT_STOPPING"} );
            }
            else
            {
                setPc($fgc, "OFF");
            }
        }
    }

    # Get gateway name from the FGC and check if it's one of the development gateways

    my $gateway = get($fgc, "DEVICE.HOSTNAME");

    my $gateway_index = first { $allowed_gateways[$_] eq $gateway } 0..$#allowed_gateways;

    die "ERROR: Unallowed gateway specified" if(!defined $gateway_index);

    # Cross-check the device name

    my $name = get($fgc, "DEVICE.NAME");

    if($name !~ /$fgc|UKNWN/)
    {
        die "Connected to unknown device: $name";
    }

    # Disable all channels on class 59 device, or it won't be possible to reset it

    if(get($fgc, "DEVICE.CLASS_ID") == 59)
    {
        for my $channel (0..15)
        {
            set($fgc, "FGCONF.TRIGGER[$channel]", "DISABLED");
        }
    }

    #my $installed_mainprog = get($fgc, "CODE.VERSION.MAINPROG");
    #my $installed_bootprog = get($fgc, "CODE.VERSION.BOOTPROG");
}

foreach my $fgc (@fgcs)
{
    set($fgc, "DEVICE.RESET", "");
}

my $loop        = 0;
my $programming = 0;

# Subscribe to the FGC, so that we have access to STATE_OP

subscribeAll(@fgcs);

# Loop for ~30s
# This should be more than enough to go main->boot->main
# Depending on how many new codes are available,
# the FGC can transition between boot and programming many times

while($loop++ < 30)
{
    # New publication is available each ~1s

    if(isAnyReprogramming(@fgcs))
    {
        $loop = 0;
        $programming++;
    }

    # Safety timeout (~500s)

    if($programming > 500)
    {
        unsubscribeAll(@fgcs);
        die "Programming timed-out";
    }
}

foreach my $fgc (@fgcs)
{
    ok(get_next_publication($fgc) =~ /STATE_OP:(NORMAL|SIMULATION|CALIBRATING|TEST|UNCONFIGURED)/, "$fgc in main after reprogramming");
}

unsubscribeAll(@fgcs);

# Check if the versions have changed

#ok($installed_bootprog != get($fgc, "CODE.VERSION.BOOTPROG"), "Bootprog version didn't change");
#ok($installed_mainprog != get($fgc, "CODE.VERSION.MAINPROG"), "Mainprog version didn't change");

# EOF