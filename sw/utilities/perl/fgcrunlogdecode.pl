#!/usr/bin/perl -w
#
# Name:     fgclogdecode.pl
# Purpose:  Decode FGC runlog
# Author:   Stephen Page

use strict;

while(<>)
{
    print and next if(!/Unix time/);

    s/[\012\015]//g; # Remove newline

    my $line    = $_;
    my @fields  = split(/ : /, $line);

    my %localtime;
    (
        $localtime{sec},
        $localtime{min},
        $localtime{hour},
        $localtime{month_day},
        $localtime{month},
        $localtime{year},
        $localtime{week_day},
        $localtime{year_day},
        $localtime{is_dst},
    ) = localtime(hex($fields[1]));

    printf("$line,%02d/%02d/%04d %02d:%02d:%02d\n",
           $localtime{month_day},
           $localtime{month} + 1,
           $localtime{year} + 1900,
           $localtime{hour},
           $localtime{min},
           $localtime{sec});
}

# EOF
