#!/usr/bin/perl
#
# Name:     spreadsheet_parser.pl
# Authors:  Krystian Wojtas
# Purpose:  Parse definition of systems provided as csv files exported from spreedsheet
#           and write them as xml files

use strict;
use warnings; 

use File::Basename;
use File::Slurp qw(read_file write_file);
use List::Util qw(first min);
use Text::Tabs qw($tabstop expand);
use XML::LibXML; 

use FGC::Components;
use FGC::RegFGC3_parameters;

# This constant is used to execute different parts of code. 
# If = 1, the script has been used to generate RegFGC3 XML files for already existing systems.
# If = 0, the script behaves as usual.
# The constant was added because in the definition templates for already existing systems, 
# the name of the boards are not values from the boards symlist. With the constant = 1, the script
# suggests board names based on a string distance algorithm. This should not happen "in production" and 
# instead errors should be risen if a board with the incorrect name is found in the template. 
# TODO: remove when generation of RegFGC3 XML files for already existing systems finishes.
use constant GENERATE_FOR_CURRENT_SYSTEMS => 0; 
use constant NUMBER_PROG_DEVICES          => 2; 


# To indicate whether it is a RegFGC3 system
my $is_regfgc3_system = 0;

# Array of possible errors
my @errors;


# Check if provided string is empty
# It means only whitespaces or '-' char optionally surrounded by whitespaces
sub empty($)
{
    my ( $string ) = @_;

    if (($string =~ /^\s*-\s*$/) or ($string =~ /^\s*$/)) {
        # If the input is an empty string, the regex will match;
        # however, the expression will evaluate to this empty string, which is treated as *false* when used as test later.
        # Therefore, we need to capture it with an if-statement right away (which _does_ work) and return an explicit 'true' value.
        return 1;
    }
    else {
        return 0;
    }
}


# Gets label of passed barcode
sub get_label($)
{
    my ( $barcode ) = @_;

    my $label = $fgc_components{$barcode}->{label};

    # Make sure each used barcode is defined in a file
    unless($label)
    {
        push @errors, 'Barcode "'.$barcode.'" is not defined in component to labels file mapping (or rerun the parser)';
    }

    # Return label of passed component's barcode
    return $label;
}



# Check if parsed system has passed correct values in it's fields
sub validate_system($)
{
    my ( $system ) = @_;

    # Current system should be defined already
    if(not defined $system->{name})
    {
        push(@errors, "There is a definition of components without specifying it's system name previously");
    }

    # Check if system hc code is not too long
    if(length $system->{code} > 10)
    {
        push(@errors, 'System '.$system->{name}.' has too long hc code');
    }

    # Check if system hc code is not too short
    if(length $system->{code} < 1)
    {
        push(@errors, 'System '.$system->{name}.' has too short hc code');
    }

    # Make sure all system fields are provided
    for my $property (qw(name class code))
    {
        if(empty($system->{$property}))
        {
            push(@errors, 'System '.$system->{name}.' definition is not complete (missing field: $property)');
        }
    }

    # Check if specified class is one of the valid one
    # TODO: the fact that all classes must be enumerated here is less-than-great 
    if($system->{class} !~ /^(50|51|53|59|60|61|62|63|64|65|92)$/i)
    {
        push(@errors, 'System '.$system->{name}.' class '.$system->{class}.' invalid!');
    }

    # Ensure that code fields starts with 'HC' characters
    if($system->{code} !~ /^HC/)
    {
        push(@errors,'System '.$system->{name}.' code '.$system->{code}.' should start with "HC" characters');
    }

    # Parse array of backplane barcodes
    # multiple barcodes -- example: "HCRECAH001 or HCRECAH002"
    # TODO: add other formats as needed
    if($system->{backplane} =~ /^\s*(HC[A-Z0-9]+)\s+or\s+(HC[A-Z0-9]+)\s*$/)
    {
        $system->{backplane_barcodes} = [$1, $2];
    }
    # single barcode -- example: "HCRECAH001"
    elsif($system->{backplane} =~ /^\s*(HC[A-Z0-9]+)\s*$/)
    {
        $system->{backplane_barcodes} = [$1];
    }
    # single barcode -- example: "REGFGC3 BACKPLANE TYPE 21 (HCRNEAR001)"
    elsif($system->{backplane} =~ /\((HC[A-Z0-9]{8})\)$/)
    {
        $system->{backplane_barcodes} = [$1];
    }
    # single barcode -- example: "REGFGC3 BACKPLANE TYPE 7 (RECAH002)"
    # (this format should probably be discouraged in favor of the one with 'HC' prefix)
    elsif($system->{backplane} =~ /\(([A-Z0-9]{8})\)$/)
    {
        $system->{backplane_barcodes} = ["HC" . $1];
    }
    # no backplane given
    elsif(empty($system->{backplane}))
    {
        print STDERR "WARNING: no backplane specified for system ".$system->{code}."\n";
        $system->{backplane_barcodes} = [];
    }
    else
    {
        print STDERR "WARNING: backplane specification '".$system->{backplane}."' for system ".$system->{code}." not understood.\n";
        $system->{backplane_barcodes} = [];
    }

    # Check that system is properly defined in components.xml
    my $system_component=(pack 'A10', uc $system->{code}.('_' x 10));
    $system_component =~ s/^\s+|\s+$//g; # trim remove white space from both ends of the string
    if(! defined $fgc_components{$system_component})
    {
        push(@errors, "Component ${system_component} is not defined in components.xml");
    }
}



# Check if parsed component is valid
sub validate_card_position($$$)
{
    my ($system, $card_position, $barcodes) = @_;

    # Create position should contain only digits or be empty
    if(not empty($card_position) and $card_position =~ /\D/)
    {
        my $err_message = 'System '.$system->{name}.' has defined component '.$barcodes.
            ", but its 'crate position' field with value '".$card_position.
            "' is not a number";

        push(@errors, $err_message);   
    }
}



# Identique barcodes must have the same card_type_name
sub validate_card_type_name($$$)
{
    my ($system, $card_type_name, $barcodes) = @_;

    # Templates may have an entry with a card position empty (no card defined)
    return if (! defined($card_type_name));

    my $err_component = first { $_->{barcodes} eq $barcodes and $_->{card_type_name} ne $card_type_name } @{ $system->{components} };

    if ( $err_component )
    {
          my $err_message = 'System '.$system->{name}.
            ' has defined components '.$barcodes.
            ', but they are NOT defined under the name '.$card_type_name;

        push(@errors, $err_message);
            
    }
}


# Implements the Levenshtein algorithm for string distances
sub levenshtein($$)
{
    my ($str1, $str2) = @_;
    my ($len1, $len2) = (length $str1, length $str2);

    return $len2 if $len1 == 0;
    return $len1 if $len2 == 0;


    my %mat;

    for (my $i = 0; $i <= $len1; ++$i) 
    {
       $mat{0}{$i} = $i;
       $mat{1}{$i} = 0;
    }

    my @ar1 = split //, $str1;
    my @ar2 = split //, $str2;

    for (my $j = 1; $j <= $len2; ++$j) 
    {
        my $p = $j % 2;
        my $q = ($j + 1) % 2;
        $mat{$p}{0} = $j;

        for (my $i = 1; $i <= $len1; ++$i) 
        {
            my $cost = 0;
            if ($ar1[$i-1] ne $ar2[$j-1]) 
            {
                $cost = 1;
            }
            $mat{$p}{$i} = min($cost + $mat{$q}{$i-1},
            $mat{$p}{$i-1} + 1, $mat{$q}{$i} + 1);
        }
    }

    return $mat{$len2%2}{$len1};
}


# Uses levenshtein to find a replacement for a given string
sub find_replacement($$)
{
    my ($origin, $possibilities) = @_;
    my $result = "";

    my $min_distance = length($origin);

    foreach my $key (keys %$possibilities)
    {
        my $dist = levenshtein($origin, $key);
        if ($dist < $min_distance)
        {
            $result = $key; 
            $min_distance = $dist; 
        } 
    }

    return $result; 
}



# Checks consistency of the RegFGC3 data
sub validate_regfgc3_data($$$$)
{
    use constant FGC3_SCIVS_SLOT        => 0;
    use constant EXTENSION_SCIVS_SLOT   => 1; 
    use constant SCIVS_MAX_SLOT_NUMBER  => 31; 

    my $err_message = "";
    my ($scivs_slot, $card_type_name_ref, $device1_variant_ref, $device2_variant_ref) = @_;

    # RegFGC3 data are only validated for cards with at least the following defined fields: 
    # SCIVS SLOT NUMBER, CARD TYPE NAME, DEVICE1 PROGRAM VARIANT. 
    # DEVICE2 PROGRAM VARIANT may be required for certain boards
    return if (! defined($$card_type_name_ref) || ! defined($scivs_slot) || ! defined($$device1_variant_ref));

    # From this point, cards have name, scivs slot and device1 variant. It is therefore a RegFGC3 system.
    $is_regfgc3_system = 1; 

    # Check SCIVS slot validity
    if ($scivs_slot < FGC3_SCIVS_SLOT || $scivs_slot > SCIVS_MAX_SLOT_NUMBER)
    {
        push (@errors, "Card $$card_type_name_ref has been assigned a non-valid SCIVS slot number: $scivs_slot\n");
        return; 
    }
    
    # FGC3 and extension board are not reprogrammable
    return if ($scivs_slot == FGC3_SCIVS_SLOT || $scivs_slot == EXTENSION_SCIVS_SLOT);

    # Check card exists in module RegFGC3. If not, suggest and return.
    my %valid_cards = map {$_ => 1} keys %regfgc3_parameters; 

    if (! exists($valid_cards{$$card_type_name_ref}))
    {
        my $replacement = find_replacement(uc $$card_type_name_ref, \%regfgc3_parameters);

        if (GENERATE_FOR_CURRENT_SYSTEMS == 1)
        {
            $$card_type_name_ref = $replacement; 
        }
        else
        {
            $err_message = "Card name $$card_type_name_ref does not exist or has been named differently! Did you mean: $replacement?\n".
            "Otherwise correct this with one of the following suggestions:\n";

            foreach my $valid_card (keys %valid_cards)
            {
                $err_message .= $valid_card."\n";
            }
            $err_message .= "\n";

            push(@errors, $err_message);
            return; 
        }
    }

    # Grab variants for the existing card. If there are variants and the device1 is not valid, then variant should be set in file    
    my %valid_variants_mf = map {$_ => 1} keys %{$regfgc3_parameters{$$card_type_name_ref}{MF}};
    if (! exists($valid_variants_mf{$$device1_variant_ref}) )
    {
        my $replacement = find_replacement(uc $$device1_variant_ref, \%valid_variants_mf);

        if (GENERATE_FOR_CURRENT_SYSTEMS == 1)
        {
            $$device1_variant_ref = $replacement; 
        }
        else
        {
            $err_message = "FPGA variant $$device1_variant_ref for card $$card_type_name_ref does not exist or has been named differently! Did you mean: $replacement?\n".
            "Please correct this with one of the following suggestions:\n";

            foreach my $valid_variant (keys %valid_variants_mf)
            {
                $err_message .= $valid_variant." ";
            }
            $err_message .= "\n";

            push(@errors, $err_message);
            return; 
        } 
    }


    # Check if device2 VARIANT field should be defined
    my %valid_variants_dev2 = map {$_ => 1} keys %{$regfgc3_parameters{$$card_type_name_ref}{DEVICE_2}};
    if (! defined($$device2_variant_ref))
    {
        $$device2_variant_ref = "EMPTY";
    }
    elsif (! exists($valid_variants_dev2{$$device2_variant_ref}))
    {
        my $replacement = find_replacement(uc $$device2_variant_ref, \%valid_variants_dev2);

        if (GENERATE_FOR_CURRENT_SYSTEMS == 1)
        {
            $replacement = "EMPTY" if ($replacement eq "");
            $$device2_variant_ref = $replacement; 
        }
        else
        {
            $err_message = "DSP variant $$device2_variant_ref for card $$card_type_name_ref does not exist or has been named differently! Did you mean: $replacement?\n";
            push(@errors, $err_message);
            return;
        }
    }        
}



# Parse the csv file which should be exported from spreedsheet table
sub parse
{
    my ($systems_csv) = @_;

    # Definition of the system
    my $system = { components => [] };

    if (scalar(@$systems_csv) == 0)
    {
        return; 
    }

    # Split the CSV information between system and components in two different arrays
    my ($count_row, $components_starting_row) = 0;

    for my $row (@$systems_csv)
    {
        if ($row =~ m/COMPONENTS/)
        {
            $components_starting_row = $count_row; 
        }
        $count_row++; 
    }

    my @system_definition_csv     = @$systems_csv[0..$components_starting_row-1];
    my @components_definition_csv = @$systems_csv[$components_starting_row..scalar(@$systems_csv) - 1]; 


    # Parse the system first
    for my $system_row (@system_definition_csv)
    {
        my @columns = split ',', $system_row;
        my ($key1, $value1, $key2, $value2, $key3, $value3) = @columns[0..1, 3..4, 6..7];

        my $add_system_property =
            sub
            {
                my ( $key, $value ) = @_;

                # 'System definition' string should be parsed already

                if(not defined $system)
                {
                    my $err_message = "Before providing any of the system properties, it is need a row with 'DEFINITION OF A TYPE OF FGC SYSTEM' string";
                    push(@errors, $err_message);
                        
                }

                # Save the property without surrounding whitespaces

                $system->{lc $key} = $1 if $value =~ /^\s*(.*)\s*$/;
            };

        # Add system properties

        $add_system_property->($1, $value1) if($key1 =~ /(name|class|code)/i);
        $add_system_property->($1, $value2) if($key2 =~ /(voltage|current|quadrant)/i);
        $add_system_property->($1, $value3) if($key3 =~ /(backplane)/i);

        # Add code property without HC prefix
        $add_system_property->('code_nohc', $value1 =~ /^HC(.*)/i) if($key1 =~ /code/i);
    }


    # Validate system
    validate_system($system);

    # Parse components
    for my $component_row (@components_definition_csv)
    {
        # Ensure that row has more then 9 columns and than fields does not contain any extra new line
        my @columns = split ',', $component_row; 

        if (@columns < 9)
        {
            my $err_message = "Row '$component_row' does not contain at least 9 columns. Make sure that there is no extra new line in any field";
            push(@errors, $err_message);
        }

        # Interpretation of columns fields in case of parsing components

        my ($card_position, $scivs_slot, $device1_variant, $device2_variant, $card_type_name, $barcodes) = map { /^\s*(.*)\s*$/; } @columns[0,1,3,4,5,6];

        $card_type_name  = undef if ($card_type_name !~ /^\s*\w+/);                
        $scivs_slot      = undef if ($scivs_slot     !~ /^\s*\d+/);                
        $device1_variant = undef if ($device1_variant   !~ /^\s*\w+/);    
        $device2_variant = undef if ($device2_variant    !~ /^\s*\w+/);
        # Don't parse table description

        next if($card_position =~ /card\s+position/i or $card_position =~ /components/i);

        # If barcodes are not provided, then skip this row

        next if(empty($barcodes));

        # Validate input data
        validate_card_position($system, $card_position, $barcodes);

        validate_regfgc3_data($scivs_slot, \$card_type_name, \$device1_variant, \$device2_variant) if ($system->{class} =~ /^6\d{1}/);

        validate_card_type_name($system, $card_type_name, $barcodes);

        $barcodes = filter_barcodes($system, $barcodes);

        push
            @{ $system->{components} },
            {
                card_position   => $card_position,
                card_type_name  => $card_type_name,
                barcodes        => $barcodes,
                scivs_slot      => $scivs_slot, 
                device1_variant => $device1_variant,
                device2_variant => $device2_variant
            };

    }

    return $system;
}



# Barcodes must be in components.xml
# The FGC will try to find all barcodes in the system definition, and will generate an FGC_HW warning any is missing
# Therefore, we filter out barcodes without dallas ID from the system definition (as they can't be detected by the FGC)
sub filter_barcodes($$)
{
    my ($system, $barcodes) = @_;

    my @filtered_barcodes;

    for my $barcode (split (" ", $barcodes))
    {
        $barcode =~ s/^\s+|\s+$//g; # trim remove white space from both ends of the string

        if(! defined($fgc_components{$barcode}))
        {
            push @errors, "Component $barcode is not defined in components.xml";
        }
        elsif( exists $fgc_components{$barcode}->{dallas_id} and $fgc_components{$barcode}->{dallas_id} eq "yes")
        {
            push @filtered_barcodes, $barcode;
        }
        else
        {
            print STDERR "WARNING: component $barcode doesn't have a DALLAS ID and will be filtered out\n";
        }
    }

    return join " ", @filtered_barcodes;
}



# Filter which components are parts of bigger group and which one are singles
# Also extends these new partials component lists by counting how many times the same barcodes or their grouped combinations appear
sub prepare_data($)
{
    my ( $system ) = @_;

    # Mapping between standarized barcodes keys and assigned components extended by splitted barcodes and required fields
    # Standarized barcode key means one without any surrounding whitespaces
    my %barcodes_key__component = ();

    # Fill mapping hash
    for my $component( @{ $system->{components} } )
    {
        # Split barcodes in case of group, then sort them
        my @splitted_barcodes = sort( { $a cmp $b } split(/\s/, $component->{barcodes}) );

        # Prepare components hash key based on splitted, sorted and then joined in standard way barcodes
        my $barcodes_key = join(' ', @splitted_barcodes);

        # Add component to a system under it's barcodes key if it does not yet exist
        $barcodes_key__component{$barcodes_key} ||=
            {
                card_position     => $component->{card_position},
                card_type_name    => $component->{card_type_name},
                splitted_barcodes => \@splitted_barcodes,
                required          => 0,
            };

        # Increment required field
        $barcodes_key__component{$barcodes_key}->{required}++;
    }

    # Add entry for backplane
    if(@{ $system->{backplane_barcodes} } > 0)
    {
        my $barcodes_key = join(' ', @{ $system->{backplane_barcodes} });

        $barcodes_key__component{$barcodes_key} =
            {
                card_type_name    => "BACKPLANE",
                splitted_barcodes => \@{ $system->{backplane_barcodes} },
                required          => 1,
            };
    }

    # Filter groups of components
    my @grouped_components = grep { @{ $_->{splitted_barcodes} } > 1 } values %barcodes_key__component;

    # Collect single components without any larger group
    my @single_components = grep { @{ $_->{splitted_barcodes} } == 1 } values %barcodes_key__component;

    # Single components contains only one barcode under 'splitted_barcodes' key
    # This barcode is without any whitespaces around, it's more standarized
    # So it's better to extract it and save under another key for easier access
    for my $component (@single_components)
    {
        $component->{barcode} = $component->{splitted_barcodes}->[0];

        # Remove redundant field
        delete $component->{splitted_barcodes};
    }

    # Return prepared data to easly fill in template
    return (\@grouped_components, \@single_components);
}



# Make output xml document based on prepared data
sub fill_system_template(@)
{
    my ( $system, $grouped_components, $single_components ) = @_;

    # Distinguish if given system is fgc2 or fgc3 platform
    my $platform_fgc3 = ( $system->{class} =~ /^\s*6/ );

    my ($platform_name,$platform_inc);

    ($platform_name,$platform_inc) = ('FGC2','rfmba') if($system->{class} =~ /\s*5/);
    ($platform_name,$platform_inc) = ('FGC3','rfnae') if($system->{class} =~ /\s*6/);
    ($platform_name,$platform_inc) = ('FGCLite','rfmaf') if($system->{class} =~ /\s*92/);

    # Check out if system component is properly defined
    my $system_component=(pack 'A10', uc $system->{code}.('_' x 10));

    my %root_attributes = (
            platform => lc $platform_name,
            comp     => $system_component
        );

    my $doc = XML::LibXML::Document->new();

    my $root = $doc->createElement('system');

    while ( my ($k, $v) = each(%root_attributes))
    {   
        $root->setAttribute($k => $v);
    }


    for my $component (grep { $_->{barcode} !~ /rfmba|rfnae/i } @$single_components)
    {
        my $component_tag = $doc->createElement('component');
        $component_tag->addChild( $doc->createAttribute('barcode'   => $component->{barcode}) );
        $component_tag->addChild( $doc->createAttribute('required'  => $component->{required}) );
        
        $root->addChild($component_tag);
        $root->addChild( $doc->createComment(get_label($component->{barcode})) );
    }


    for my $group (@$grouped_components)
    {
        my $group_tag = $doc->createElement('group');
        $group_tag->addChild( $doc->createAttribute('tag'       => $group->{card_type_name}) );
        $group_tag->addChild( $doc->createAttribute('required'  => $group->{required}) );

        for my $barcode ( @{$group->{splitted_barcodes}} ) 
        {
                my $grouped_component_tag = $doc->createElement('component');
                $grouped_component_tag->addChild( $doc->createAttribute('barcode' => $barcode) );
                $group_tag->addChild($grouped_component_tag); 
                $group_tag->addChild( $doc->createComment(get_label($barcode)) ); 
        }

        $root->addChild($group_tag);
    }

    $doc->setDocumentElement($root);

    my $xml_string = $doc->toString(2);

    # Place comments on the same line as non-commented lines
    $xml_string =~ s/\n\s*<!/\t<!/g;

    # Add four spaces before entities
    $xml_string =~ s/(\&\w+)/\t$1/g;
    $tabstop = 4;
    $xml_string = expand($xml_string);

    # Add DTD string
    my $low_case_code = lc($system->{code_nohc});

    my $dtd_string = "<!DOCTYPE inc [\n";
    
    # Do not add the log_menu on FGC3 systemsAdd log_menu only on FGC2
    
    if ($platform_fgc3 eq '')
    {
        $dtd_string .= "    <!ENTITY log_menu   SYSTEM \"classes/$system->{class}/log_menu.xml\">\n";        
    }

    $dtd_string .= "    <!ENTITY $platform_inc      SYSTEM \"systems_inc/$platform_inc.xml\">\n";
    $dtd_string .= "    <!ENTITY $low_case_code\_dims SYSTEM \"dims_inc/$low_case_code.xml\">\n";
    $dtd_string .= "]>\n";

    # Add entity references
    my %entities_comments;

    if ($platform_fgc3 eq '')
    {
        %entities_comments = (
                log_menu                          => 'Standard log menu',
                $platform_inc                     => $platform_name.' Generic',
                lc( $system->{code_nohc} ).'_dims'=> (uc $system->{code_nohc}).' Dim definitions'
            );
    }
    else
    {
        %entities_comments = (
                $platform_inc                     => $platform_name.' Generic',
                lc( $system->{code_nohc} ).'_dims'=> (uc $system->{code_nohc}).' Dim definitions'
            );
    }

    my $entities_string = "";
    while ( my ($k, $v) = each (%entities_comments) )
    {
        $entities_string .= "\n&$k;\n<!--$v-->";
    }
    # Place comments on the same line as non-commented lines
    $entities_string =~ s/\n\s*<!/\t<!/g;
    # Add four spaces before entities
    $entities_string =~ s/(\&\w+)/\t$1/g;
    $tabstop = 4;
    $entities_string = expand($entities_string);

    my @xml_array = split("\n", $xml_string);
    $xml_array[0] .= "\n".$dtd_string;
    if ($xml_array[1] =~ /\/>$/) {
        $xml_array[1] = substr($xml_array[1], 0, -2).">";
        $xml_array[1] .= $entities_string;
        $xml_array[1] .= "\n</system>";
    } else {
        $xml_array[1] .= $entities_string;
    }

    return join("\n", @xml_array)."\n";
}



# End of functions



# Run this script
# Check if paths are provided to script as arguments or help message is requested
if(@ARGV < 1 or first {/-h/i} @ARGV)
{
    die 'Usage: '.basename($0).' <system_description.csv> [ <component_labels.xml> ]'."\n";
}

# ToDO: remove these lines
my $converter_type = (split('/', $ARGV[0]))[0];
my @avoid_devices_array = qw(R2ETE 
                            RMEAS 
                            RPADC_Commercial 
                            RPACQ_Commercial 
                            RPACR_Commercial 
                            RPACS_Commercial 
                            RPHA_Danfysik
                            RPHA_Danfysik
                            RPHB_Danfysik
                            RPZEK_Commercial
                            RPZEO_Commercial);
my %avoid_devices = map {$_ => 1} (@avoid_devices_array); 
exit if (exists($avoid_devices{$converter_type}));


# Resolve bundle path
my $script_path = dirname($0);
my $bundle_dir  = `cd '${script_path}'; git rev-parse --show-toplevel`;
chomp $bundle_dir;


# Get paths
my $system_description_filename = shift;

# Parse systems
my $system = parse(read_file($system_description_filename, array_ref => 1));

# Print all parsing errors if any
if(@errors)
{
    die "ERRORS were encountered\n".join("\n", @errors)."\n";
}

my ($grouped_components_ref, $single_components_ref) = prepare_data($system);

# Write output xml's
my $system_file = $bundle_dir.'/def/src/systems/'.$system->{code_nohc}.'.xml';
write_file($system_file,  fill_system_template($system, $grouped_components_ref, $single_components_ref));

# Print system name to stdout
print "$system->{code_nohc}";

# EOF
