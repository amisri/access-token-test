#!/usr/bin/perl

# File:    nvs.pl

use File::Slurp;

use Test::Utils;
use FGC::Properties;

use strict;
use warnings;
use diagnostics;



sub isPpm
{
    my ($d, $property) = @_;

    my $flags   = getPropertyInfo($d, $property)->{"flags"};
    my $dev_ppm = get($d, "DEVICE.PPM");
    my $res     = (hex($flags) & hex("0x0400"));

    return ($res != 0 && $dev_ppm eq "ENABLED");
}



sub getPropType
{
    my ($property, $class) = @_;

    foreach my $key (keys %fgc_properties)
    {
        if($key eq $property)
        {
            return($fgc_properties{$key}{classes}{$class}{type});
        }
    }
}



sub decodeData
{
    my ($binary_data, $type) = @_;

    my %data_decoders =
    (
        "FLOAT"  => "nf>*",
        "CHAR"   => "nA*",
        "INT16U" => "nn*",
        "INT16S" => "nn!*",
        "INT32U" => "nN*",
        "INT32S" => "nN!*",
        "POINT"  => "nf>*",
    );

    if(!exists $data_decoders{$type})
    {
        print "WARNING: Data decoding for type $type not implemented\n";

        return (0, ());
    }

    my ($n_els, @interpreted_data) = unpack($data_decoders{$type}, $binary_data);

    if($type eq "POINT")
    {
        my @points  = ();
        my @indexes = map { $_ * 2 } (0..$n_els-1);

        foreach my $i (@indexes)
        {
            push @points, $interpreted_data[$i]."|".$interpreted_data[$i + 1];
        }

        return ($n_els, @points);
    }

    return ($n_els, @interpreted_data);
}



sub printDecodedData
{
    my ($n_els, $decoded_data) = @_;

    print "\tn_els: $n_els\n";
    print "\tdata:\n";

    my $index = 1;

    foreach my $value (@$decoded_data)
    {
        print "\t\t$index: $value\n";

        $index++;
    }
}



sub printUsageMessage
{
    print "\nThe script prints content of non-volatile property storage.\n";
    print "It works only with FGC2 devices.\n";

    print "\nUsage: $0 --device=<device_name> [--property=<property_name>]\n\n";

    print "When property is specified, only the part of NVS holding this property will be displayed.\n";
    print "Otherwise, the script will dump NVS data for all NVS properties on the device.\n\n";
}



my %params = (device=>undef, config=>'', protocol=>'', property=>undef);
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};
my $p = $params{property};

if(!defined $d)
{
    printUsageMessage();

    die "Device not specified";
}

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

my $class = get($d, "DEVICE.CLASS_ID");

my @nvs_properties;

if(defined $p)
{
    my $nvs_idx = getPropertyInfo($d, $p)->{"nvs_idx"};

    if(defined $nvs_idx && $nvs_idx ne "0xFF")
    {
        push @nvs_properties, $p;
    }
    else
    {
        die "Property $p doesn't exist or is not non-volatile";
    }
}
else
{
    foreach my $property (keys %fgc_properties)
    {
        if(exists $fgc_properties{$property}{classes}{$class})
        {
            my $nvs_idx = getPropertyInfo($d, $property)->{"nvs_idx"};

            if(defined $nvs_idx && $nvs_idx ne "0xFF")
            {
                push @nvs_properties, $property;
            }
        }
    }
}

foreach my $property (@nvs_properties)
{
    my $nvs_idx            = getPropertyInfo($d, $p)->{"nvs_idx"};
    my $nvs_record_address = 0xFE008 + 40 * hex($nvs_idx);

    set($d, "FGC.DEBUG.MEM.MCU", "$nvs_record_address,20");
    my $nvs_record_bin = get($d, "FGC.DEBUG.MEM.MCU bin");

    my ($prop_address, $data_address, $bufsize, $name) = unpack("nNnA*", $nvs_record_bin);

    my $type = getPropType($property, $class);

    print  "$property\n";
    print  "\tnvs_idx:            $nvs_idx\n";
    printf "\tnvs_record_address: 0x%X\n", $nvs_record_address;
    printf "\tprop_address:       0x%X\n", $prop_address;
    printf "\tdata_address:       0x%X\n", $data_address;
    print  "\tbufsize:            $bufsize\n";
    print  "\tname:               $name\n";
    print  "\ttype:               $type\n";
    print  "\n";

    my $num_users = isPpm($d, $property) ? 32 : 1;

    # Alignt to word boundary - on FGC2, FRAM can be accessed only by word

    my $bufsize_words = int(($bufsize + 1) / 2);

    for(my $user = 0; $user < $num_users; $user++)
    {
        my $user_data_address = $data_address + $user * $bufsize_words;

        set($d, "FGC.DEBUG.MEM.MCU", "$user_data_address,$bufsize_words");
        my $data     = get($d, "FGC.DEBUG.MEM.MCU");
        my $data_bin = get($d, "FGC.DEBUG.MEM.MCU bin");

        print "\tuser $user\n";
        print "\traw_data:\n";
        print "\t$data\n";

        my ($n_els, @decoded_data) = decodeData($data_bin, $type);

        printDecodedData($n_els, \@decoded_data);

        print "\n";
    }
}

# EOF