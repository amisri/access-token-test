#!/usr/bin/perl -w
#
# Purpose:  For a given timber variable, it counts the number of cycles in which it goes above the specified threshold
# Author:   Miguel Hermo

use strict;
use warnings;

use Data::Dumper;
use POSIX qw(mktime strftime);
use Carp;

use Timber::Timber;

my $MAX_D2IDT = 0.15;
my $MAX_DIDT  = 0.01;
my $I_MIN     = 1000;

sub max ($$) { $_[$_[0] < $_[1]] }
sub min ($$) { $_[$_[0] > $_[1]] }

sub gradient($)
{
    my ($x) = @_; 
    
    my @grad;
    
    for (my $i = 2; $i <= $#$x; $i++) 
    {
        push(@grad,1/2.0*( ($x->[$i] - $x->[$i-2])))
    }  

    return \@grad;
    
}

sub detect($$)
{
    my ($meas,$datetime) = @_; 

    my $d = gradient($meas);
    my $dd = gradient($d);
    my $avg_didt = 0;

    for (my $i = 0; $i <= $#$dd-2; $i++) 
    {
       
        # Moving average for 10 samples

        $avg_didt = 1/10*$d->[$i+1] + (1-1/10)*$avg_didt;
        
        # Reject measurements below I_MIN

        if (defined($I_MIN) && $meas->[$i+2] < $I_MIN)
        {
            next;
        }
    
        # Reject measurements when ramping down
    
        if (defined($MAX_DIDT) && $avg_didt < -$MAX_DIDT)
        {
            next;
        }
    
        # Trigger if the d2I is above MAX_D2IDT
    
        my $di = $dd->[$i];
        
        if (abs($di) > $MAX_D2IDT )
        {
            print "Glitch detected during cyle at $datetime (i=$i, I=$meas->[$i+2], dI=$d->[$i+1], d2I=$dd->[$i])...\n";
                
            print "\tms,meas,delta meas,delta^2 meas\n";
            for (my $j = max($i-10,0); $j <= min($i+10,$#$dd); $j++) 
            {
                print "\t$j,$meas->[$j],$d->[$j],$dd->[$j]\n";
            }
            
            return;
              
            }  
    }
}

sub add_hour_to_date($)
{
    my ($date) = @_;

    my ($year, $mon, $day, $hour, $minute, $second) = $date =~ /^(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/;
    my $start_epoch = mktime($second, $minute, $hour, int($day), int($mon)-1, int(($year%100)+100));
    my $next_epoch  = $start_epoch + 3600;

    return strftime('%Y-%m-%d %H:%M:%S', localtime($next_epoch));
}

die "Usage: $0 <variable> <init_date> <end_date>\n   Date format: YYYY-MM-DD hh:mm:ss\n" if(@ARGV < 3);

my ($var_name, $init_date, $end_date) = @ARGV;

my $tmp_file  = 'timber.tmp.csv';

# read 1h worth of data at a time, to not overload the timber server
my $num_cycles = 0;
my $next_date;
do
{ 
    $next_date = add_hour_to_date($init_date);
    $next_date = $end_date if($next_date ge $end_date);
    
    print "Retrieving Logging DB data ($init_date <= t <= $next_date)...\n";
    
    (Timber::download_from_timber($var_name, $init_date, $next_date, $tmp_file)==0) or confess("could not download $var_name between $init_date and $next_date");

    my $timber_data = Timber::load_timber_file($tmp_file);
    
    my ($variables, $timestamps, $data) = Timber::load_timber_file($tmp_file);   

    for my $i ( 0 .. scalar(@$variables) -1)
    {
    	my $datetime = @$timestamps[$i];
    	my $cycle_data = @$data[$i];
    	
    	detect($cycle_data,$datetime);
    }

    unlink($tmp_file);
    $init_date = $next_date;
}
while($next_date ne $end_date);

print("total: $num_cycles\n");
