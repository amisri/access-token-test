#!/usr/bin/perl -w
#
# Purpose:  For a given timber variable, it counts the number of cycles in which it goes above the specified threshold
# Author:   Miguel Hermo

use strict;
use warnings;

use Data::Dumper;
use POSIX qw(mktime strftime);
use Carp;

use Timber::Timber;

sub add_hour_to_date($)
{
    my ($date) = @_;

    my ($year, $mon, $day, $hour, $minute, $second) = $date =~ /^(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/;
    my $start_epoch = mktime($second, $minute, $hour, int($day), int($mon)-1, int(($year%100)+100));
    my $next_epoch  = $start_epoch + 3600;

    return strftime('%Y-%m-%d %H:%M:%S', localtime($next_epoch));
}

die "Usage: $0 <variable> <init_date> <end_date> <threshold>\n   Date format: YYYY-MM-DD hh:mm:ss\n" if(@ARGV < 3);

my ($var_name, $init_date, $end_date, $threshold) = @ARGV;

my $tmp_file  = 'timber.tmp.csv';

# read 1h worth of data at a time, to not overload the timber server
my $num_cycles = 0;
my $next_date;
do
{ 
    $next_date = add_hour_to_date($init_date);
    $next_date = $end_date if($next_date ge $end_date);
    
    (Timber::download_from_timber($var_name, $init_date, $next_date, $tmp_file)==0) or confess("could not download $var_name between $init_date and $next_date");

    my $timber_data = Timber::load_timber_file($tmp_file);
    
    my ($variables, $timestamps, $data) = Timber::load_timber_file($tmp_file);   
    
    print("cycle_date cycle_time max_value is_above_threshold");

    for my $i ( 0 .. scalar(@$variables) -1)
    {
    	my $datetime = @$timestamps[$i];
    	my $cycle_data = @$data[$i];
    	my $max_current = (sort { $b <=> $a } @$cycle_data)[0];
    	
    	print("$datetime $max_current");
    	
    	if (!defined $threshold || $max_current>$threshold)
        {
            $num_cycles++;
            print("1\n");
        }
        else
        {
            print("0\n");
        }
    }

    unlink($tmp_file);
    $init_date = $next_date;
}
while($next_date ne $end_date);

print("total: $num_cycles\n");
