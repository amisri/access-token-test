#!/usr/bin/perl -w
#
# Purpose:  Downloads a variable from timber between the specified dates
#           It is then written into directory with a subdirectory for each variable and day
#           This is required because AFS has a limit on the number of files on a single directory
# Author:   Miguel Hermo

use strict;
use warnings;

use POSIX qw(mktime strftime);
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use Carp;

use Timber::Timber;

sub save_file($$)
{
    my ($data,$path) = @_;
    my $dir = dirname($path);
    
    mkpath($dir) if (! -d $dir);
    print("\tsaving : $path\n");
            
    open(my $out_fh, '>', $path) or die("can't create file $path\n");
    
    for my $i(0 .. scalar @{$data}-1)
    {
        print $out_fh "@$data[$i]\n";
    }
    close($out_fh);
}

sub add_half_hour_to_date($)
{
    my ($date) = @_;

    my ($year, $mon, $day, $hour, $minute, $second) = $date =~ /^(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/;
    my $start_epoch = mktime($second, $minute, $hour, int($day), int($mon)-1, int(($year%100)+100));
    my $next_epoch  = $start_epoch + 1800;

    return strftime('%Y-%m-%d %H:%M:%S', localtime($next_epoch));
}

die ("Usage: $0 <variable> <init_date> <end_date> <output_dir>\n   Date format: YYYY-MM-DD hh:mm:ss\n") if(@ARGV != 4);

my ($var_name, $init_date, $end_date, $output_dir) = @ARGV;
my $tmp_file  = 'timber.tmp.csv';

# read 1h worth of data at a time, to not overload the timber server
my $next_date;
do
{ 
    $next_date = add_half_hour_to_date($init_date);
    $next_date = $end_date if($next_date ge $end_date);
    
    print("Downloading $var_name between $init_date and $next_date\n");
    (Timber::download_from_timber($var_name, $init_date, $next_date, $tmp_file)==0) or confess("could not download $var_name between $init_date and $next_date");

    print("Extracting data to subdirectories\n");
    my ($variables, $timestamps, $data) = Timber::load_timber_file($tmp_file);   
    
    for my $i ( 0 .. scalar(@$variables) -1)
    {
    	my $var = @$variables[$i];
    	my $datetime = @$timestamps[$i];
    	my($date,$time) = split(" ",$datetime);
    	my $cycle_data = @$data[$i];
    	
    	my $output_filename = "$output_dir/$var/$date/$var:$date\_$time.txt";
    	
    	save_file($cycle_data, $output_filename);
    }

    unlink($tmp_file);
    $init_date = $next_date;
}
while($next_date ne $end_date);
