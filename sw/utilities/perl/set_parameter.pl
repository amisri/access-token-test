#!/usr/bin/perl
#
# Name: set_parameter.pl
# Purpose: Set RegFGC3 parameter command line client

use FGC::RegFGC3;
use Getopt::Long;
use Carp;
use strict;

my $device;
my $param;
my $value;
my $rbac = "location";


sub print_help()
{
    print "Usage: $0 --dev|-d device --param|-p parameter [--rbac|-r location|explicit]  [--help|-h]\n";
    print "Set RegFGC3 parameter command line client\n";
}


GetOptions( "dev|d=s" => \$device,
    "param|p=s" => \$param,
    "value|v=s" => \$value,
    "rbac|r=s" => \$rbac,
    "help|h" => \&exit_with_help) or exit_with_help();


print_help() && die "ERROR: No device and/or property and/or value specified - nothing to do.\n" unless (defined($device) && defined($param) && defined($value));

FGC::RegFGC3::set_param_value($device, $param, $value, $rbac);
exit(0);

#EOF
