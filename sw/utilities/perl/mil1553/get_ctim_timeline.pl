#!/usr/bin/perl -w
my $HELP = <<'HELP_MESSAGE'
Retrieve the timeline of CTIM events and users that occured for a given machine for the past 10 seconds.
usage:

get_ctim_timeline.pl MACHINE options

  MACHINE             - One of PSB,CPS,SPS,LEI,ADE
  
  Options:
  -e EVENT1,EVENT2... - Comma separated list of events to include in the results. 
                        If left empty it will retrieve all events. 
                        It's possible to include only part of the name (all matching event names will be included)
                        
  -b                  - Prints the users in bold to make them stand out more easily 
  -t                  - Timespan to look at in seconds. (default 10 seconds)
Output:
    A list of events, with the following tab separated fields:
    absolute_time   cycle_time  event

Example:
$ ./get_ctim_timeline.pl CPS -t 1 -d PX.SCY-CT,PX.APOW-CT

1436800551300000000 0   CPS.USER.EAST2(x2)
1436800552400186825 2300    PX.APOW-CT
1436800552500000000 0   CPS.USER.SFTPRO1(x1)
1436800552500186825 0   PX.SCY-CT
1436800552700186825 200 PX.APOW-CT

...

HELP_MESSAGE
;

# Retrieves the timeline of CTIM events for the specified machine
use constant 
{
	BOLD => "\e[1m",
	NOBOLD => "\e[0m",
};
use Switch;
use Carp;
use Data::Dumper;
use mil1553::GmtLogReader qw( gmtlogreader_init gmtlogreader_filter_dates gmtlogreader_next_result get_log_servers);

if(!scalar(@ARGV))
{
	print $HELP;
	exit(0);
}

my $machine = $ARGV[0];
my $timespan = 10;
my $do_bold = 0;
my $filter = "";
while ($opt = shift)
{
	switch($opt)
	{
		case '-b' { $do_bold = 1;           }
		case '-e' {	$filter = shift;        }
		case '-t' { $timespan = int(shift); }
	}
}

$filter =~ s/,/\|/g;

confess("unsupported machine $machine") if(!defined((get_log_servers())->{$machine}));

my @telegrams;
my @events;

my $now = int(time."000"); #current timestamp in ms.
my $before = $now - 1000 * $timespan; #retrieve the last minute.

my $gmtlogreader_opts_tg = gmtlogreader_init($machine,'telegrams');
gmtlogreader_filter_dates($gmtlogreader_opts_tg, $before, $now);
#gmtlogreader_filter_dates($gmtlogreader_opts_tg,  1435242233176, 1435242254176);
   
   
my @lines="";
while( my $tg = gmtlogreader_next_result(\@telegrams, $gmtlogreader_opts_tg))
{ 
    exit() if (!defined($tg));
    my $durn = $tg->{DURN} || 1; #for PSB, duration is always 1
    my $text = "$tg->{AbsoluteTimeTG}\t0\t";
    $text .= BOLD if($do_bold);
    $text .= "$machine.USER.$tg->{USER_NAME}";
    $text .= NOBOLD if($do_bold);
    $text .= "(x$durn)\n";
    push(@lines, $text);
}

my $gmtlogreader_opts_ev = gmtlogreader_init($machine,'events');
gmtlogreader_filter_dates($gmtlogreader_opts_ev, $before, $now);
#gmtlogreader_filter_dates($gmtlogreader_opts_ev,  1435242233176, 1435242254176);
    

while( my $ev = gmtlogreader_next_result(\@events, $gmtlogreader_opts_ev))
{ 
    exit() if (!defined($ev));
    my $line = "$ev->{AbsoluteTimeEV}\t$ev->{CTime}\t$ev->{EventName}\n";
    push(@lines,$line) if($line =~ /$filter/);
}

@lines = sort @lines;

foreach $line (@lines)
{
    print $line;
}
