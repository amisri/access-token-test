#!/usr/bin/perl -w
#
# Purpose:  Interface to the GMT Logging System
# Author:   Miguel Hermo Serans

package mil1553::Mil1553Timing;

use mil1553::GmtLogReader qw( gmtlogreader_init gmtlogreader_filter_dates gmtlogreader_next_result);

#get last users

