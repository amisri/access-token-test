#!/usr/bin/perl -w

use mil1553::GmtLogReader qw( gmtlogreader_init gmtlogreader_filter_dates gmtlogreader_next_result);
use strict;
use warnings;
use Data::Dumper;
use Carp;

use constant BP_DURATION => 1200;
my $TIME_PER_DEVICE = 2;
my $IGNORE_USER_ZERO = 0;
my $IGNORE_TRIGG_AT_SAME_TIME = 1;
my @machines = ('PSB', 'CPS', 'ADE', 'LEI');

=pod
returns true whenever $haystack (array reference) contains $needle
=cut
sub contains($$)
{
	my ($haystack, $needle) = @_;
	foreach(@$haystack) 
	{ 
		return(1) if($_ eq $needle);
	}
	return(0);
}


=pod
Adds a $device to the \%waiting_list

$waiting_list 
{
	dev1 => {events=>[event1, event2, .. ] , time=>[t1,t2,...] }}
	...
}
device - device to add to the waiting list
event - event that the device is processing
time - time at which the event happened
=cut
sub add_devs_waiting($$$$)
{
	my ($waiting_list, $device, $event, $time) = @_;
	
	my $device_list;
	foreach(@$device)
	{
		if(!exists($waiting_list->{$_}))
		{
			$waiting_list->{$_} = { events=>[$event], time=>[$time] } 
		} 
		else
		{
			push ( @{$waiting_list->{$_}{events}}, $event);
			push ( @{$waiting_list->{$_}{time}},   $time );
		}
	}
}


=pod
Check if there were any collisions between the read acq/write ccv events
and the trigger acquisition according to the events recorded in the $timeline,
and the configuration specified in $events

Params:
$timeline (see get_timeline)

$events = {
   evTrig => { 
        trig_1 => [dev1, dev2,...devn],
        trig_2 => [dev1, dev2,...devn],
        ...
   },
   evReadWrite => { 
        event_1 => [dev1, dev2,...devn],
        event_2 => [dev1, dev2,...devn],
        ...
   }
}

Returns:
A list of collisions (if any)
A traceback of
=cut
sub check_timing($$)
{
	my ($timeline, $events) = @_;

	my @event_time = sort {$a <=> $b} ( keys %$timeline);
	
	my ($t,$prev_t)=(0,0);
	
	my $num_devs_waiting=0;	
	my %devs_waiting;
	
	my @trace;
	my @collisions;
	
	foreach(@event_time)
	{

		$t = $_;
		my $num_rw=0;
		my $trig_event=0;
		my $trig_collides=0;
		my $has_collisions=0;
		
		#Reduce num. of waiting devices depending on time since last event
		
		$num_devs_waiting-=($t-$prev_t)*(1/$TIME_PER_DEVICE);
		if($num_devs_waiting<=0)
		{
			$num_devs_waiting = 0;
			%devs_waiting=();
		}
		
		#For each event in the current time
		
		foreach(@{$timeline->{$t}})
		{
			my $ev = $_;
			
			#Read / Write events
			
			if(exists($events->{evReadWrite}{$ev})) 
			{
				add_devs_waiting(\%devs_waiting, $events->{evReadWrite}{$ev},$ev,$t);
				$num_rw=scalar(@{$events->{evReadWrite}{$ev}});
				$num_devs_waiting+=$num_rw;
			}
			
			#Trigger events
			
			if(exists($events->{evTrig}{$ev})) 
			{
				$trig_event=1;
				if($num_devs_waiting>0) 
				{
					my @affected;
					
					# Check if any of the devices that are potentially waiting for some R/W operation to finish are
					# affected by the current trigger
					
					foreach(keys(%devs_waiting)) 
					{ 
						push ( @affected, $_) if(contains($events->{evTrig}{$ev}, $_));
					}
					
					# if there are, report the collision
					
					if( scalar(@affected) > 0) 
					{
						my %collision = (
						     devs_affected  => \@affected,
						     rw_events => {},
						     trigger => { $ev => {	time => $t  } }
						);
						
						#retrieve the events for which the affected devices were waiting
						
						my %rw_events;
						foreach(@affected)
						{
							my $dev_affected = $_;
							for(my $i=0; $i < scalar(@{$devs_waiting{$dev_affected}{events}}); $i++)
							{
								my $event = $devs_waiting{$dev_affected}{events}[$i];
								my $time  = $devs_waiting{$dev_affected}{time}[$i];
								$collision{rw_events}{$event} = { time => $time };
							}
						}
						push (@collisions, \%collision);
						
						$has_collisions = 1;
					}
				};
			}
		}
		if ($num_rw || $trig_event)
		{
			my %trace_txt=(time=>$t);
			$trace_txt{time}=$t;
			$trace_txt{num_rw_operations}=$num_rw;
			$trace_txt{trigger}=$trig_event;
			$trace_txt{has_collisions}=$has_collisions;				
			push ( @trace, \%trace_txt );
			
			$prev_t = $t;
		}
	}
	
	return (\@collisions, \@trace);
}

=pod
Get a list of ltim events and the CTim at which they are fired for a given machine and user
TO-DO: obtain data from DB/rda instead of file
=cut
sub get_ltims($$)
{
	my ($machine,$user) = @_;

	open(my $data, '<', 'timing_data/ltims.csv') or confess("Could not open ltims file $!\n");

 	my @ltims;
	while (my $line = <$data>) 
	{
		chomp($line);
	 	if ($line =~ /$machine,$user/ )
	 	{
			my @fields = split("," , $line);
			push (@ltims, {EventName => $fields[2], CTime => $fields[3]}) ;
	 	}
	}
	return @ltims;
}

=pod
Returns the list of mil1553 fecs on a given machien
TO-DO: obtain from database
=cut
sub get_fecs_by_machine($)
{
	my $machine = shift;
	
	open(my $data, '<', 'timing_data/timing_config.csv') or confess("Could not open config file $!\n");
	
 	my %fecs;
	while (my $line = <$data>) 
	{
		chomp($line);
	 	my ($nmachine, $nfec, @rest) = split("," , $line); 
		$fecs{$nfec}=1 if ($nmachine eq $machine);
		
	}
	return keys %fecs;
}

=pod
Returns the list of interrupts used by mil1553 devices
TO-DO: obtain from database
=cut
sub get_timing_config($)
{
	my $mfec = shift;
	
	open(my $data, '<', 'timing_data/timing_config.csv') or confess("Could not open config file $!\n");
 	my (%rw_events, %trig_events);
 	my @ltims;
	while (my $line = <$data>) 
	{
		chomp($line);
	 	my ($machine, $fec, $dev, $write, $read, $trig) = split(",",$line);
	 	if($fec eq $mfec) 
	 	{
	 		$rw_events{$read}   =[] if(!exists $rw_events{$read});
	 		$rw_events{$write}  =[] if(!exists $rw_events{$write});
	 		$trig_events{$trig} =[] if(!exists $trig_events{$trig});
	 		
	 		push @{$rw_events{$read}},   $dev;
	 		push @{$rw_events{$write}},  $dev;
	 		push @{$trig_events{$trig}}, $dev;
	 	}
	}
	my %results = ("evReadWrite"=>\%rw_events, "evTrig"=>\%trig_events);
	
}

=pod
Retrieve the read ACQ, write CCV and trigger acquisition events for the specified device
=cut
sub get_device_config($)
{
    my $mdev = shift;
	
	open(my $data, '<', 'timing_data/timing_config.csv') or confess("Could not open config file $!\n");

 	my @ltims;
	while (my $line = <$data>) 
	{
		chomp($line);
	 	my ($machine, $fec, $dev, $write, $read, $trig) = split(",",$line);
	 	if($dev eq $mdev) 
	 	{
	 		my %results=(write=>$write, read=>$read, trig=>$trig);
	 		return(%results);
	 	}
	}
}

=pod
Retrieves all the combinations of users that happen between begin and end timestamps
on the telegram logs 
reutrns:
hash
{
	"user1,user1,user2" => absolute_time,
	"user3,user1" => absolute_time,
	...
} 
=cut
sub get_telegram_pairs($$$)
{
	my ($machine,$begin,$end) = @_;
	
	my %pairs;
	my ($u0,$u1);
	
	#obtain the different combinations of users that there is
	my @telegrams;
	my $telegram_logs_opts = gmtlogreader_init($machine,'telegrams');
	gmtlogreader_filter_dates($telegram_logs_opts, $begin, $end);
	
	while(my $tg = gmtlogreader_next_result(\@telegrams, $telegram_logs_opts))
	{
		my $username = $tg->{USER_NAME};
		my $duration;
		
		if(!defined($username)) {
			print Dumper($tg);
			confess("unknown user on telegram");
		} 
		
        if(defined $tg->{DURN}) 
        { 
        	$duration = $tg->{DURN}; 
        }
        else                    
        { 
        	$duration = 1; 
        }
            
		if(!defined $u0) 
		{ 
			$u0="$duration:$username" 
		}
		else  
		{  
			$u1 = "$duration:$username";
			$pairs{"$u0,$u1"} = $tg->{AbsoluteTimeTG};
			 
			while($tg->{DURN}-- > 1) 
			{
				gmtlogreader_next_result(\@telegrams, $telegram_logs_opts)
			}

			$u0=$u1;
		}
	}
	return(%pairs);
}

=pod
It retrieves all the different CTim and Ltim events that occured beginning in the
$bagining_timestamp absolute time (ms), for the users specified in \@users, each
user taking BP_DURATION (ms).

input:
$machine = machine name
$users= ref to array with users
$begin_timestamp = moment in time where the first basic period begins 

output:
$timeline = {
    t0 => [event1],
    t1 => [event2,event3],
    ... 
}

=cut
sub get_events_timeline($$$)
{
	my ($machine, $begin_timestamp, $users_txt) =@_;
	
	sub add_event
	{
		my ($time, $name, $timeline)=@_;
		if(!exists $timeline->{$time}) { $timeline->{$time} = []; }
		push(@{$timeline->{$time}}, $name);
	}
		
	my %timeline;
	my ($dur1, $usr1, $dur2, $usr2) = ($users_txt =~ /^(\d+)\:([A-Z0-9]+),(\d+)\:([A-Z0-9]+)/);
	my $end_timestamp = $begin_timestamp + (BP_DURATION * ($dur1+$dur2) * 1000000);
	
	#get CTIMs
	
	my @events;
	my $ctim_log_opts = gmtlogreader_init($machine,'events');
	gmtlogreader_filter_dates($ctim_log_opts, $begin_timestamp, $end_timestamp);
	while( my $ctim = gmtlogreader_next_result(\@events, $ctim_log_opts))
	{
		my $cycle_n = int ( ($ctim->{AbsoluteTimeEV}-$begin_timestamp)/( BP_DURATION * 1000000));
		my $t0 = $cycle_n * BP_DURATION + $ctim->{CTime};
		add_event($t0, $ctim->{EventName}, \%timeline); 
	}
	
	#get LTIMs
	
	foreach(get_ltims($machine, $usr1))
	{
	    add_event($_->{CTime}, $_->{EventName}, \%timeline);
	}
	
	foreach(get_ltims($machine, $usr2))
    {
        add_event(BP_DURATION * $dur1 + $_->{CTime}, $_->{EventName}, \%timeline);
    }
	
	return(%timeline);
}

sub pairs_get_ctim($$)
{
	my ($time, $users_txt) = @_;
	
	my ($dur1, $usr1, $dur2, $usr2) = ($users_txt =~ /^(\d+)\:([A-Z0-9]+),(\d+)\:([A-Z0-9]+)/);
	
	return($time-($dur1 * BP_DURATION)) if($time >= $dur1 * BP_DURATION);
	return($time);
}

sub pairs_get_user($$)
{
	my ($time, $users_txt) = @_;
    
    my ($dur1, $usr1, $dur2, $usr2) = ($users_txt =~ /^(\d+)\:([A-Z0-9]+),(\d+)\:([A-Z0-9]+)/);
    
    return($usr2) if($time >= $dur1 * BP_DURATION);
    return($usr1);
}

###############################
# Main Program                #
###############################

my $end   = time * 1000;
my $begin = $end - (5000 * 120);

foreach (@machines)
{
	my $machine = $_;
	my %user_pairs = get_telegram_pairs($machine, $begin, $end);

    my %collisions;
    
	while(my($user_pair, $begin_timestamp) = each %user_pairs)
	{
		#my @users = pairs_to_array($user_pair);
		
		my %timeline = get_events_timeline($machine, $begin_timestamp, $user_pair); #\@users);
				
		foreach(get_fecs_by_machine($machine))
		{
			my $fec=$_;
	        print("$machine,$user_pair,$fec,");
			my %timing_config = get_timing_config($fec);
			my($col, $trace) = check_timing(\%timeline,\%timing_config);
			if (scalar @$col>0)
			{
				$collisions{$fec}=[] if(!defined $collisions{$fec});
				foreach(@$col)
				{
					$_->{user_pair}= $user_pair;
		            push @{$collisions{$fec}}, $_;
				}
				print("ERROR\n");
			}
			else
			{
			    print("OK\n");
			}
		}
	}

	# Print Collisions
	my @lines;
	foreach (keys(%collisions)) 
	{

		my $fec = $_;
		print("$fec\n");
		foreach(@{$collisions{$fec}})
		{
			my $col = $_;
			my $line = "\taffected:" . (join ",", @{$col->{'devs_affected'}})."\n";
			
			foreach( keys %{$col->{rw_events}})
			{
				my $event = $_;
				my $user = pairs_get_user($col->{rw_events}{$event}{time},$col->{user_pair});
				my $ctim = pairs_get_ctim($col->{rw_events}{$event}{time},$col->{user_pair}); 
				$line .= "\tevent:$event,$user,$ctim\n";
			}
			
			my $trigger = (keys %{$col->{trigger}})[0];
			my $user = pairs_get_user($col->{trigger}{$trigger}{time},$col->{user_pair});
			my $ctim = pairs_get_ctim($col->{trigger}{$trigger}{time},$col->{user_pair});
			
			$line.= "\ttrigger:$trigger,$user,$ctim\n";
			$line .= "\n";
			if(!contains(\@lines, $line)) #do not print duplicates
			{
				print ($line);
				push (@lines, $line);
			}

		}
	}
}
