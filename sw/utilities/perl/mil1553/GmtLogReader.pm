#!/usr/bin/perl -w
#
# Purpose:  Interface to the GMT Logging System
# Author:   Miguel Hermo Serans
package mil1553::GmtLogReader;

use strict;
use warnings;
use Data::Dumper;
use Carp;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(gmtlogreader_init gmtlogreader_filter_dates gmtlogreader_next_result get_log_servers);

my $log_servers = 
{
	'PSB' => 'BLX.GMTLOG-CTXPS',
	'CPS' => 'CLX.GMTLOG-CTXPS',
	'SPS' => 'SLX.GMTLOG-CTLOG',
    'ADE' => 'DLX.GMTLOG-CTXADE',
    'LEI' => 'ELX.GMTLOG-CTXLEI',
	'LHC' => 'HLX.GMTLOG-CTLOG',
};

sub get_log_servers()
{
	return($log_servers);
}

my $users = 
{
    'ADE' => {1 => 'ADE'},
    'CPS' => {1 => 'AD',  2 => 'EAST1',  3 => 'EAST2',  4 => 'ION1',   5 => 'ION2',    6 => 'ION3',   7 => 'LHC1',    8 => 'LHC2',   9 => 'LHC3',    10 => 'LHC4',    11 => 'LHCINDIV',12 => 'LHCPROBE',13 => 'MD1',     14 => 'MD2',     15 => 'MD3',     16 => 'MD4',     17 => 'MD5',     18 => 'MD6',     19 => 'MD7',     20 => 'MD8',     21 => 'SFTPRO1', 22 => 'SFTPRO2', 23 => 'TOF',    24 => 'ZERO'},
    'LEI' => {1 => 'ZERO',2 => 'EARLY',  3 => 'NOMINAL',4 => 'MDRF',   5 => 'ANOMINAL',6 => 'MDEC',   7 => 'AMDOPTIC',8 => 'MDOPTIC',9 => 'AMDRF',   10 => 'LIN3MEAS',11 => 'MDEARLY', 12 => 'MDNOM',   13 => 'FL_IN_SU',14 => 'FL_NO_SU',15 => 'FL_SP_SU',16 => 'FL_OU_SU',17 => 'FL_IN_MD',18 => 'FL_NO_MD',19 => 'FL_SP_MD',20 => 'FL_OU_MD',21 => 'AMDEC',   22 => 'AMDNOM',  23 => 'AMD',    24 => 'BIOMD'},
    'LHC' => {1 => 'LHC'},
    'PSB' => {1 => 'AD',  2 => 'EAST1',  3 => 'EAST2',  4 => 'LHC1A',  5 => 'LHC1B',   6 => 'LHC2A',  7 => 'LHC2B',   8 => 'LHC3',   9 => 'LHC4',    10 => 'LHCINDIV',11 => 'LHCPROBE',12 => 'MD1',     13 => 'MD2',     14 => 'MD3',     15 => 'MD4',     16 => 'MD5',     17 => 'MD6',     18 => 'NORMGPS', 19 => 'NORMHRS', 20 => 'SFTPRO1', 21 => 'SFTPRO2', 22 => 'STAGISO', 23 => 'TOF',    24 => 'ZERO'},
    'SPS' => {1 => 'ZERO',2 => 'SFTPRO1',3 => 'SFTPRO2',4 => 'SFTPRO3',5 => 'SFTION1', 6 => 'SFTION2',7 => 'SFTION3', 8 => 'SFTION4',9 => 'HIRADMT1',10 => 'HIRADMT2',11 => 'LHCPILOT',12 => 'LHCINDIV',13 => 'LHC50NS', 14 => 'LHC25NS', 15 => 'LHC1',    16 => 'LHC2',    17 => 'LHC3',    18 => 'LHC4',    19 => 'LHCFAST1',20 => 'LHCFAST2',21 => 'LHCION1', 22 => 'LHCION2', 23 => 'LHCION3',24 => 'LHCION4', 25 => 'LHCMD1', 26 => 'LHCMD2', 27 => 'LHCMD3', 28 => 'LHCMD4', 29 => 'MD1', 30 => 'MD2', 31 => 'MD3', 32 => 'MD4'},
    'SCT' => {1 => 'SETUP'}
};

sub gmtlogreader_init($$)
{

    my ($machine, $log_type) = @_;
	confess("ERROR: trying to initialize GmtLogReader without specifying machine and log type (telegrams or events)") if (scalar @_ < 2); 
	
	my $log_property;
	
	if($log_type eq 'telegrams') 
	{ 
		$log_property='HistTgLog'; 
	}
	elsif($log_type eq 'events') 
	{ 
		$log_property='HistEvLog'; 
	}
	else 					     
	{
		confess("ERROR: unknown log type `$log_type` (possible values: telegrams,events)"); 
	}
	
	confess("ERROR: log server not found for machine '$machine'") if (!exists $log_servers->{$machine} );

    my $gmt_config = {};
    
    $gmt_config->{dev} = $log_servers->{$machine};
    $gmt_config->{property} = $log_property;
       
    #defaults
    $gmt_config->{cmw_retries}  = 2;
    $gmt_config->{cmw_blocking} = 1;
    $gmt_config->{rbac} = 'location';
    $gmt_config->{filters} = [];
	$gmt_config->{machine} = $machine;
	
    return($gmt_config);
}

sub _get_filter_cmd_options($)
{
	my $filters = shift;
	my ($fname, $ftype, $fvalue) =('','','');
	if (scalar @$filters>0)
	{
		foreach (@$filters)
		{
			$fname.="$_->{fname},";
			$ftype.="$_->{ftype},";
			$fvalue.="$_->{fvalue},";
		}
		
		return (" --fname ".substr($fname,0,-1)." --ftype ".substr($ftype,0,-1)." --fvalue ".substr($fvalue,0,-1)." ");
	}
	return (""); 
}

#adds filter options
sub _gmtlogreader_add_filter($$$$)
{
	my $gmt_config = shift;
	my %new_filter;
	
	
	($new_filter{fname},$new_filter{ftype},$new_filter{fvalue}) = @_;
	
	foreach(@{$gmt_config->{filters}})
	{
		if($_->{fname} eq $new_filter{fname}) 
		{
			($_->{ftype},$_->{fvalue}) = ($new_filter{ftype},$new_filter{fvalue});
			return();
		};
	}
	
	push (@{$gmt_config->{filters}}, \%new_filter);
}

sub gmtlogreader_filter_dates($$$)
{
	my $gmt_config = shift;
	my ($startTime,$endTime) = @_;
	_gmtlogreader_add_filter($gmt_config, 'StartTime', 'int64', $startTime);
	_gmtlogreader_add_filter($gmt_config, 'EndTime', 'int64', $endTime);
	return ($gmt_config);
}

sub _exec_cmd;
sub _exec_cmd($)
{
	my $gmt_config = shift;
	
	my $cmd = "rda-get -r $gmt_config->{rbac} -d $gmt_config->{dev} -p $gmt_config->{property}";
	
	$cmd.=" -c $gmt_config->{user}" if (exists $gmt_config->{user});
	
	$cmd.= _get_filter_cmd_options($gmt_config->{filters});

	my $output = `$cmd 2>&1` || "";
	
	if( $? != 0 )
	{
		if($output =~ /\(CMW\) unsufficient resources \(client limit reached\)/ )
		{
			warn("WARNING: CMW client limit reached.\n");
			confess("ERROR: Retry limit reached.\n") if($gmt_config->{cmw_retries} == 0);
			
			if($gmt_config->{cmw_blocking})
			{
				$gmt_config->{cmw_retries}--;
				warn("WARNING: Waiting 60s\n");
				sleep(60);
				return _exec_cmd($gmt_config);
			}
		}
		else
		{
			$output =~ s/\r|\n/ /g;
			confess("unknown error: '$output'\ncommand run:$cmd");
		}
	}

	return ($output);
}

sub _process_cmd_output($$)
{
    my ($output, $gmt_config) = @_;
    my $currentField;
	my @results;
	
	foreach my $line (split(/\n/,$output))
    {
        if ( $line =~ /^Name/ )
        {
            ($currentField = $line ) =~ s/^Name(\s|:)*|(\s|\n)*$//g;
        }
        elsif ( $line =~ /^Value/ )
        {
        	$line =~ s/^Value(\s|:)*//g;

        	if($currentField eq "Overflow")
        	{
        		$gmt_config->{overflow} = ($line =~ /true/);
        	}
        	elsif($currentField ne "GroupValueS") # this property has more values than the rest! I just filter it out
        	{	
	            my @values = split(/ /,$line);
	            for(my $i=0; $i<=$#values; $i++) 
	            {
	            	if (exists $results[$i]) 
	            	{
	            		$results[$i]{$currentField} = $values[$i];
	            	}
	            	else
	            	{
	            		push ( @results, {$currentField => $values[$i]} );
	            	}
	            }
        	}
        }
    }
    
    return (@results);
}

sub _process_telegrams($$)
{
    my ($results,$machine) = @_;
    my %telegrams;
    
	foreach ( @$results )
	{
		if(!defined $telegrams{$_->{AbsoluteTimeTG}})
		{
			$telegrams{$_->{AbsoluteTimeTG}} = {
				SeqNumberTG     => $_->{SeqNumberTG}, 
				AbsoluteTimeTG  => $_->{AbsoluteTimeTG},
				$_->{GroupName} => $_->{GroupValue}
			};
		}
		else
		{
			$telegrams{$_->{AbsoluteTimeTG}}{$_->{GroupName}} = $_->{GroupValue};
		}
		
		$telegrams{$_->{AbsoluteTimeTG}}{USER_NAME} = $users->{$machine}{$_->{GroupValue}} if($_->{GroupName} eq "USER")
	}
	
	if(!%telegrams)
	{
	    return ([]);
	}
	else
	{
	    #return list of telegrams ordered by AbsoluteTimeTG time
	    
	    my @ordered_telegrams;
	    foreach my $tg (sort {$a <=> $b} ( keys %telegrams ))
	    {
	        push ( @ordered_telegrams, $telegrams{$tg} );
	    }
        return ( @ordered_telegrams );
	    
    }
}

sub gmtlogreader_next_result($$)
{
    my ($results, $gmt_config) = @_;

    my $atim;
    $atim = "AbsoluteTimeTG" if ($gmt_config->{property} eq "HistTgLog");
    $atim = "AbsoluteTimeEV" if ($gmt_config->{property} eq "HistEvLog");
    

    if(scalar @$results == 0)
    {
        if(defined $gmt_config->{last_timestamp})
        {
            return undef if(!$gmt_config->{overflow});

            #read next bunch
            my $last_timestamp_ms = $gmt_config->{last_timestamp}; #substr $gmt_config->{last_timestamp},0,13; #truncate to ms precission
			_gmtlogreader_add_filter($gmt_config,'StartTime','int64',$last_timestamp_ms);
        }
        
        my $out = _exec_cmd($gmt_config);

        my @raw_results = _process_cmd_output($out, $gmt_config);
        
        if($gmt_config->{property} eq "HistTgLog")
        {
            @$results = _process_telegrams(\@raw_results,$gmt_config->{machine});
        }
        else
        {
            @$results = @raw_results;
        }
        return(undef) if(!scalar(@$results));
    }
    
    my $current;

    # Skips results that we already read in the previous bunch
    do
    {
	    $current = shift @$results;
	    return(undef) if(!defined($current) || !%$current);
	} 
	while(defined $gmt_config->{last_timestamp} and 
	              $gmt_config->{last_timestamp} >= $current->{$atim});

	$gmt_config->{last_timestamp} = $current->{$atim};
	
	return($current);
}

1;

unless (caller) 
{
	my @telegrams;
    my @events;
=pod
    # test ade stuff
    my $ade_evt= gmtlogreader_init('ADE','events');
    my $now = time*1000000000;
    my $before = $now - (24000 * 1000000000);
    
    gmtlogreader_filter_dates($ade_evt,  $before,$now);
    
    while( my $tg = gmtlogreader_next_result(\@events, $ade_evt))
    { 
        exit() if (!defined($tg));
        print("$tg->{CTime} $tg->{AbsoluteTimeEv} $tg->{EventName}\n");
    }
=cut
#gmtlogreader_filter_dates($gmtlogreader_opts_tg,  1430319373335000000,
                                                  #1430329484340000000)
	# tests telegram logs
# 1435242232055
# 1291018137000
# 1435242233176
# 1435242233176000000
	my $gmtlogreader_opts_tg = gmtlogreader_init('ADE','telegrams');
	gmtlogreader_filter_dates($gmtlogreader_opts_tg,  1435242233176,
	                                                  1435242254176);
	
	while( my $tg = gmtlogreader_next_result(\@telegrams, $gmtlogreader_opts_tg))
	{ 
	    exit() if (!defined($tg));
	    print("$tg->{AbsoluteTimeTG} USER $tg->{USER_NAME} $tg->{USER} $tg->{DURN}  \n");
	    #print ".";
	}
	#die;
	#test events log
	
	my $gmtlogreader_opts_ev = gmtlogreader_init('ADE','events');
	gmtlogreader_filter_dates($gmtlogreader_opts_ev,  1435242233176,
	                                                  1435242254176);
	

	while( my $ev = gmtlogreader_next_result(\@events, $gmtlogreader_opts_ev))
	{ 
	    exit() if (!defined($ev));
	    print("$ev->{AbsoluteTimeEV} $ev->{CTime} $ev->{EventName}\n");
	    #print ".";
	}
	
}
