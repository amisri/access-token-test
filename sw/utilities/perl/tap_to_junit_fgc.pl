#!/usr/bin/perl -w

use strict;
use warnings;

my $filename = $ARGV[0];

# Consider the test as failed when a line with TAP's 'not ok' has been found

my $failed = 0;

my $line;
my @lines;

foreach $line ( <STDIN> )
{
    if($line =~ /^not ok\b/)
    {
        $failed = 1;
    }

    push(@lines, $line);
}

# Create a JUnit xml file

open my $junit_xml_file, ">", $filename or die "Cannot open file '$filename': $!";

# Create the xml file

print $junit_xml_file "<testsuite name='$filename' tests='1' failures='$failed'>\n";
print $junit_xml_file "<testcase name='$filename'>\n";

if($failed)
{
    print $junit_xml_file "<failure>\n";
}

foreach(@lines)
{
    print $junit_xml_file "$_";
}

if($failed)
{
    print $junit_xml_file "</failure>\n";
}

print $junit_xml_file "</testcase>\n";
print $junit_xml_file "</testsuite>\n";
