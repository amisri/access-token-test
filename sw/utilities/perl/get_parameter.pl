#!/usr/bin/perl
#
# Name: get_parameter.pl
# Purpose: Get RegFGC3 parameter command line client

use FGC::RegFGC3;
use Getopt::Long;
use Carp;
use strict;

my $device;
my $param;
my $rbac = "location";

sub print_help()
{
    print "Usage: $0 --dev|-d device --param|-p parameter --value|-v value [--rbac|-r location|explicit] [--help|-h]\n";
    print "Get RegFGC3 parameter command line client\n";

}


GetOptions( "dev|d=s" => \$device,
    "param|p=s" => \$param,
    "rbac|r=s" => \$rbac,
    "help|h" => \&exit_with_help) or exit_with_help();

print_help && die "ERROR: No device and/or property specified - nothing to do.\n" unless (defined($device) && defined($param));

my ($param_value, $param_raw_value, $unit) = FGC::RegFGC3::get_param_value($device, $param, $rbac);
printf ("$param: $param_value $unit (0x%08X)\n", $param_raw_value);

exit(0);

#EOF
