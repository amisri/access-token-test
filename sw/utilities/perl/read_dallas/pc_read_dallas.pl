#!/usr/bin/perl -w
#
# Name:     pc_read_dallas.pl
# Purpose:  Read power converter Dallas IDs with DS2480
# Author:   Stephen Page

use IPC::Open2;
use strict;

if ("$^0" =~ /win/i)
{
    require Win32::OLE;
}
else
{
    die "ERROR: $0 is not windows compatible\n"; 
}

die "Usage: $0 <barcode_id file> <port>\n" if(@ARGV != 2);
my ($barcode_id_file, $port) = @ARGV;

# Read barcode_id file

open(BARCODE_ID, "<", $barcode_id_file)
    or die "Failed to open barcode_id file: $!\n";

my %barcode_id;
my %id_barcode;
while(<BARCODE_ID>)
{
    chop;

    my ($barcode, $id) = split(',');
    next if($barcode =~ /000000$/);

    $barcode_id{$barcode}   = $id;
    $id_barcode{$id}        = $barcode;
}
close(BARCODE_ID);

if(open(NEW_BARCODE_ID, "<", "pc_new_barcode_id.txt"))
{
    while(<NEW_BARCODE_ID>)
    {
        chop;

        my ($barcode, $id)      = split(',');
        next if($barcode =~ /000000$/);

        $barcode_id{$barcode}   = $id;
        $id_barcode{$id}        = $barcode;
    }
    close(NEW_BARCODE_ID);
}

# Run tstfind

my $tstfind_pid = open2(\*TSTFIND_R, \*TSTFIND_W, 'tstfind.exe', $port);

# Read until start of search

while(<TSTFIND_R>) { last if(/Start of search/); }

# Read IDs

my @ids;
while(<TSTFIND_R>)
{
    chop; chop;
    last if(/End of search/);

    my ($index, $id) = split(' ');

    push(@ids, $id);
}
close(TSTFIND_R);
close(TSTFIND_W);

# Kill tstfind

my $tstfind_proc = Win32::OLE->GetObject('winmgmts:\\\\.\\root\\cimv2:Win32_Process.Handle=\''.$tstfind_pid.'\'');
$tstfind_proc->Terminate() == 0 or warn "Failed to kill tstfind\n";

# if empty array, exit

die "Error: no IDs scanned\n" if(!@ids);

# Process IDs

# Map scanned IDs to board types;

my %scanned_barcode_id;
my %unknown_ids = ( 01 => [], 28 => [] );
for my $id (@ids)
{
    my $barcode = $id_barcode{$id};

    if(defined($barcode))
    {
        $scanned_barcode_id{$barcode} = $id;
    }
    else
    {
        # Add ID to array of unknown IDs of Dallas type

        push(@{$unknown_ids{substr($id, 14, 2)}}, $id);
        print "$id UNKNOWN\n";
        next;
    }
}

# Print matched IDs found

for my $barcode (sort(keys(%scanned_barcode_id)))
{
    print "$scanned_barcode_id{$barcode} $barcode\n";
}
print "\n";

print scalar(keys(%scanned_barcode_id)), "/", scalar(@ids), " IDs known\n\n";

# Print counts of each type found

my %types;
for my $barcode (keys(%scanned_barcode_id))
{
    $types{substr($barcode, 0, 10)}++;
}

for my $type (sort(keys(%types)))
{
    print "$type x $types{$type}\n";
}
print "\n";

# Open barcode_id file

open(NEW_BARCODE_ID, ">>", "pc_new_barcode_id.txt")
    or die "Failed to open pc_new_barcode_id.txt: $!\n";

my $error = 0;
for my $id_type (sort(keys(%unknown_ids)))
{
    my $num_unknown = @{$unknown_ids{$id_type}};

    next if($num_unknown == 0);

    if($num_unknown == 1)
    {
        # Check whether too many IDs are visible

        if((grep { $_ !~ /^HCRFBKA/ && $_ !~ /^HCRMAD/ && $_ !~ /^HCRMOD/ && $_ !~ /^HCRMPDA/ } keys(%scanned_barcode_id)) != 0)
        {
            print   "Too many IDs to scan unknown IDs.\n".
                    "Connect only a single unknown device and restart.\n";

            close(NEW_BARCODE_ID);
            exit(1);
        }

        # Scan barcode of unknown device

        my $barcode;
        my $barcode_valid = 0;
        do
        {
            print "Scan barcode for unknown device with ID type $id_type\n";
            $barcode = <STDIN>;
            chomp($barcode);
            print "\n";

            if(defined($barcode_id{$barcode}))
            {
                print "Barcode $barcode is already associated with ID $barcode_id{$barcode}\n\n";
            }
            elsif($barcode =~ /^HC.{8,8}-..\d{6,6}$/)
            {
                $barcode_valid = 1;
            }
            else
            {
                print "Invalid barcode\n\n";
            }
        } while(!$barcode_valid);

        my $id = $unknown_ids{$id_type}->[0];

        print "New mapping $barcode,$id\n";
        print NEW_BARCODE_ID "$barcode,$id\n";
    }
    else
    {
        $error++;

        warn    "$num_unknown devices with unknown barcode for ID type $id_type.\n".
                "Disconnect ".($num_unknown - 1)." unknown devices and retry\n";
    }
}
close(NEW_BARCODE_ID);

print "Scan completed successfully.\n" if(!$error);

# EOF
