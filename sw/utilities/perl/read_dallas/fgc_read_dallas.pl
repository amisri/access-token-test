#!/usr/bin/perl -w
#
# Name:     fgc_read_dallas.pl
# Purpose:  Read FGC Dallas IDs with DS2480
# Author:   Stephen Page

use FGC::Components;
use IPC::Open2;
use strict;

if ("$^0" =~ /win/i)
{
    require Win32::OLE;
}
else
{
    die "ERROR: $0 is not windows compatible\n"; 
}

die "Usage: $0 <barcode_id file> <port> [<mb_fp file>]\n" if(@ARGV < 2);
my ($barcode_id_file, $port, $mb_fp_file) = @ARGV;

# Read barcode_id file

open(BARCODE_ID, "<", $barcode_id_file)
    or die "Failed to open barcode_id file: $!\n";

my %id_barcode;
while(<BARCODE_ID>)
{
    chop;

    my ($barcode, $id)  = split(',');
    $id_barcode{$id}    = $barcode if($barcode !~ /000000$/);
}
close(BARCODE_ID);

if(open(NEW_BARCODE_ID, "<", "new_barcode_id.txt"))
{
    while(<NEW_BARCODE_ID>)
    {
        chop;

        my ($barcode, $id)  = split(',');
        next if(!defined($barcode) || !defined($id));

        $id_barcode{$id}    = $barcode if($barcode !~ /000000$/);
    }
    close(NEW_BARCODE_ID);
}

# Read mb_fp file if supplied

my %mb_fp;
if(defined($mb_fp_file))
{
    open(MB_FP, "<", $mb_fp_file)
        or die "Failed to open mb_fp file: $!\n";

    while(<MB_FP>)
    {
        chop;

        my ($mb, $fp)  = split(',');
        next if(!defined($mb) || !defined($fp));

        $mb_fp{$mb}    = $fp;
    }
    close(MB_FP);
}

if(open(NEW_MB_FP, "<", "new_mb_fp.txt"))
{
    while(<NEW_MB_FP>)
    {
        chop;

        my ($mb, $fp)  = split(',');
        next if(!defined($mb) || !defined($fp));

        $mb_fp{$mb}    = $fp;
    }
    close(NEW_MB_FP);
}

# Run tstfind

my $tstfind_pid = open2(\*TSTFIND_R, \*TSTFIND_W, 'tstfind.exe', $port);

# Read until start of search

while(<TSTFIND_R>) { last if(/Start of search/); }

# Read IDs

my @ids;
while(<TSTFIND_R>)
{
    chop; chop;
    last if(/End of search/);

    my ($index, $id) = split(' ');

    push(@ids, $id);
}
close(TSTFIND_R);
close(TSTFIND_W);

# Kill tstfind

my $tstfind_proc = Win32::OLE->GetObject('winmgmts:\\\\.\\root\\cimv2:Win32_Process.Handle=\''.$tstfind_pid.'\'');
$tstfind_proc->Terminate() == 0 or warn "Failed to kill tstfind\n";

# if empty array, exit

die "Error: no IDs scanned\n" if(!@ids);

# Process IDs

# Create a hash of expected FGC boards

my %fgc_boards = (
                    ANA     => [],
                    CPU     => [],
                    DIG     => [],
                    DIPS    => [],
                    MB      => [],
                    MEM     => [],
                    NDI     => [],
                 );

my %fgc_board_id_types = (
                            ANA     => "01",
                            CPU     => "28",
                            DIG     => "01",
                            DIPS    => "01",
                            MB      => "28",
                            MEM     => "01",
                            NDI     => "01",
                         );

# Create a hash mapping barcodes to board types

my %board_types = (
                    HCRFBF  => "ANA",
                    HCRFBG  => "ANA",
                    HCRFBJA => "ANA",
                    HCRFBC  => "CPU",
                    HCRFBE  => "DIG",
                    HCRFBH  => "DIPS",
                    HCRFMBA => "FGC",
                    HCRFMBB => "FGC",
                    HCRFMCA => "FGC",
                    HCRFMDA => "FGC",
                    HCRFBA  => "MB",
                    HCRFBD  => "MEM",
                    HCRFBB  => "NDI",
                  );

# Map scanned IDs to board types;

my %barcode_id;
my %unknown_ids = ( 01 => [], 28 => [] );
for my $id (@ids)
{
    my $barcode = $id_barcode{$id};
    print "$id ", defined($barcode) ? $barcode : "UNKNOWN", "\n";

    if(defined($barcode))
    {
        $barcode_id{$barcode} = $id;
    }
    else
    {
        # Add ID to array of unknown IDs of Dallas type

        push(@{$unknown_ids{substr($id, 14, 2)}}, $id);
        next;
    }

    for my $type (keys(%board_types))
    {
        if($barcode =~ "^$type")
        {
            push(@{$fgc_boards{$board_types{$type}}}, $barcode);
            last;
        }
    }
}
print "\n";

# Scan front-panel barcode

my $valid_type = 0;
my $fp_barcode;
do
{
    print "Scan front-panel barcode\n";
    $fp_barcode = <STDIN>;
    chomp($fp_barcode);

    for my $type (grep { $board_types{$_} eq "FGC" } keys(%board_types))
    {
        if($fp_barcode =~ $type)
        {
            $valid_type = 1;
            last;
        }
    }
} while(!$valid_type);
print "\n";

# Check whether MB barcode is known

my $mb_barcode;
if(@{$fgc_boards{MB}} != 1) # MB barcode not known
{
    $fgc_boards{MB} = [];
    $valid_type     = 0;

    do
    {
        print "Scan MB barcode\n";
        $mb_barcode = <STDIN>;
        chomp($mb_barcode);

        for my $type (grep { $board_types{$_} eq "MB" } keys(%board_types))
        {
            if($mb_barcode =~ $type)
            {
                $valid_type = 1;
                last;
            }
        }
        print "Invalid type for MB\n" if(!$valid_type);
    } while(!$valid_type);
    print "\n";
}
else
{
    $mb_barcode = $fgc_boards{MB}->[0];
}

# Check whether a new MB->FP relationship has been found

if(!defined($mb_fp{$mb_barcode}) || $mb_fp{$mb_barcode} ne $fp_barcode)
{
    print "New MB->FP mapping $mb_barcode,$fp_barcode\n\n";

    open(NEW_MB_FP, ">>", "new_mb_fp.txt")
        or die "Failed to open new_mb_fp.txt: $!\n";

    print NEW_MB_FP "$mb_barcode,$fp_barcode\n";
    close(NEW_MB_FP);
}

# Check whether the one board of each type has been found

my $fatal = 0;
for my $board (sort {
                        !($a eq "NDI") <=> !($b eq "NDI") ||
                        !($a eq "MB")  <=> !($b eq "MB")  ||
                        $a cmp $b
                    } (keys(%fgc_boards)))
{
    my $num_found = scalar(@{$fgc_boards{$board}});
    next if($num_found == 1);

    warn "$num_found $board boards found where 1 expected\n" if($num_found != 1);
    if(!$num_found)
    {
        if(@{$unknown_ids{$fgc_board_id_types{$board}}} == 1)
        {
            my $id = pop(@{$unknown_ids{$fgc_board_id_types{$board}}});

            print "$board discovered to have ID $id\n";

            if($board eq "MB")
            {
                $fgc_boards{MB}->[0] = $mb_barcode;

                print "New MB mapping $mb_barcode,$id\n";

                open(NEW_BARCODE_ID, ">>", "new_barcode_id.txt")
                    or die "Failed to open new_barcode_id.txt: $!\n";

                print NEW_BARCODE_ID "$mb_barcode,$id\n";
                close(NEW_BARCODE_ID);
            }
            else
            {
                my $barcode;
                $valid_type = 0;
                do
                {
                    print "Scan $board barcode\n";
                    $barcode = <STDIN>;
                    chomp($barcode);

                    for my $type (grep { $board_types{$_} eq $board } keys(%board_types))
                    {
                        if($barcode =~ $type)
                        {
                            $valid_type = 1;
                            last;
                        }
                    }
                    print "Invalid type for $board\n" if(!$valid_type);
                } while(!$valid_type);
                print "\n";

                $fgc_boards{$board}->[0] = $barcode;

                print "New $board mapping $barcode,$id\n";

                open(NEW_BARCODE_ID, ">>", "new_barcode_id.txt")
                    or die "Failed to open new_barcode_id.txt: $!\n";

                print NEW_BARCODE_ID "$barcode,$id\n";
                close(NEW_BARCODE_ID);
            }
        }
        else
        {
            warn "Too many unknown IDs.  Remove all but one unknown boards and retry.\n";
            $fatal = 1;
        }
    }
    elsif($num_found > 1)
    {
        warn "Too many $board boards found.  Return FGC to lab.\n";
        $fatal = 1;
    }
    print "\n";
}
die "Fatal error, return FGC to lab.\n" if($fatal);

# Check that boards are in a valid combination

my %type_codes = map { substr($_->[0], 0, 10) => 1 } values(%fgc_boards);
$type_codes{substr($fp_barcode, 0, 10)} = 1;
for my $type_code (keys(%type_codes))
{
    my $type = $fgc_components{$type_code};
    if(!defined($type))
    {
        warn "Unknown type $type_code\n\n";
        next;
    }

}
die "Fatal error, return FGC to lab.\n" if($fatal);

print "Scan completed successfully.\n";

# EOF
