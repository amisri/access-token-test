#!/usr/bin/perl
#
# Name:     dims_parser.pl
# Authors:  Krystian Wojtas
# Purpose:  Parse definition of dims provided as csv files exported from spreedsheet
#           and write them as xml files

use strict; 
use warnings;

use FGC::Components;

use File::Basename;
use File::Slurp qw(read_file write_file);
use Getopt::Long;
use List::Util qw(first);
use List::MoreUtils qw(all);
use Switch;

# Array of possible errors
my @errors;

my $current_filename;
my $current_line;


# Check if provided string is empty
# It means only whitespaces or '-' char optionally surrounded by whitespaces
sub empty($)
{
    my $string = shift;

    return $string =~ /^\s*-\s*$/ or $string =~ /^\s*$/;
}


sub clear_empty($)
{
    my $string_ref = shift;

    if(empty($$string_ref))
    {
        $$string_ref = '';
    }

    return $$string_ref;
}


sub string_has_special_xml_chars($)
{
    my $string = shift;

    return scalar( $string =~ /[&<>]/);
}


# Gets label of passed barcode
sub get_label($)
{
    my $barcode = shift;

    my $label = $fgc_components{$barcode}->{label};

    # Make sure each used barcode is defined in a file
    unless($label)
    {
        push @errors, 'Barcode "'.$barcode.'" is not defined in component to labels file mapping (or rerun the parser)';
    }

    # Return label of passed component's barcode
    return $label;
}


# Prints a validation error message
sub log_error($;$$$)
{
    my ($msg, $dim, $addr) = @_;

    push(@errors, "$current_filename:$current_line\t$dim\t$addr\t$msg");
}


# Parse the csv file which should be exported from spreedsheet table
sub parse
{
    my $dims_csv = shift;

    my %host = ();

    my @dims = ();

    # Firstly it is given definition of system and it's properties
    # Then it is going to be a list of this system's components
    # Logic of the parsing is different for this two cases
    # Variable stores string 'system' or 'components' as a parsing case

    my $parsing = 'host';
    
    $current_line = 1;
    for my $row (@$dims_csv)
    {
        # Remove last new line char

        chomp $row;

        # Split row to columns

        my @columns = split ',', $row;

        my ($key, $value) = @columns[0..1];

        if(!defined($key))
        {
            $current_line++;
            next;
        }

        # Last dim to operate on and fill it's fields

        my $dim = $dims[$#dims];

        # Parsing state machine

        switch($key)
        {
            case /HOST CARD INFORMATION/i
            {
                $parsing = 'host';
            }
            case /DIM TYPE DEFINITION/i
            {
                $parsing = 'dim type definition';

                # New dim is going to be defined

                push @dims, { analog => [], digital => [], position => (chr(ord('A')+@dims)) };
            }
            case /ANALOG CHANNELS/i
            {
                $parsing = 'analog';
                
                #we are already defining channels, dim name must be set
                if(empty($dim->{name}))
                {
                    log_error("No name provided for DIM");
                }
            }
            case /DIGITAL BANK/i
            {
                $parsing = 'digital';

                # Add a new digital bank

                push @{ $dim->{digital} }, [];
            }

            # State machine is not affected, let's parse the data

            else
            {
                # Parsing data

                switch($parsing)
                {
                    case 'host'
                    {
                        $host{lc $1} = $value if $key =~ /(NAME|CODE|FGC|VARIANT)/i;

                        # Make HC code without leading 'HC' chars and trailing underscores or numbers
                        if ($key =~ /CODE/i)
                        {
                            ( $host{code} ) = $host{code} =~ /^\s*(.*)\s*$/;
                            ( $host{code} ) = split(" ", $host{code});
                            ( $host{code_nohc} ) = $host{code} =~ /^\s*HC([^\d|_]+)/ ;
                        }
                    }
                    case 'dim type definition'
                    {
                        $dim->{lc $1} = $value if $key =~ /(ORDER|NAME)/i;
                    }
                    case 'analog'
                    {
                        # Interpretation of columns fields in case of parsing analog channels

                        my %analog;
                        @analog
                        {
                            'channel_index',
                            'signal_name',
                            'lower_range',
                            'upper_range',
                            'units',
                        } = @columns;

                        # If analog channel is not used, then parse next one

                        next if all { empty $_ } @columns[1..4];

                        # Ommit parsing table description

                        next if $analog{channel_index} =~ /CHANNEL\s+INDEX/i;

                        # Validate input

                        validate_analog(\%host, $dim, \%analog);

                        # Add parsed analog dim to the end of the list

                        push @{ $dim->{analog} }, \%analog;
                    }
                    case 'digital'
                    {
                        # Interpretation of columns fields in case of parsing digital channel

                        my %digital;
                        @digital
                        {
                            'bit_number',
                            'signal_name',
                            'fault',
                            'zero',
                            'one',
                        } = @columns;

                        # Ommit parsing table description

                        next if $digital{bit_number} =~ /BIT\s+NUMBER/i;

                        # Ommit unused bits

                        next if empty($digital{signal_name});
                        
                        # Convert 'LOW' value to lowercase and 'HIGH' value to uppercase
                        
                        $digital{zero} = lc($digital{zero});
                        $digital{one} = uc($digital{one});

                        # Validate

                        validate_digital(\%host, $dim, \%digital);

                        # Find last digital bank

                        my $last_digital_bank = $dim->{digital}->[ $#{ $dim->{digital} } ];

                        # Add parsed analog dim to the end of the list

                        push @{ $last_digital_bank }, \%digital;
                    }
                }
            }
        }
        $current_line++;
    }

    # Assosiate dims with their's host
    $host{dims} = \@dims;

    # Validate host
    validate_host( \%host );

    # Return parsed structure
    return \%host;
}


sub validate_analog($$$)
{
    my ($host, $dim, $analog) = @_;

    if (length $analog->{signal_name} > 13)
    {
        my $msg = 'Name too long: "'.$analog->{signal_name}.'" (max 13 chars)';

        log_error( $msg, $dim->{name}, "ANALOG_".$analog->{channel_index} );
    }

    if(string_has_special_xml_chars($analog->{signal_name}))
    {
        my $msg = 'Name "'.$analog->{signal_name}.'" contains xml chars like & < > ';

        log_error( $msg, $dim->{name}, "ANALOG_".$analog->{channel_index} );
    }

    if($analog->{lower_range} !~ /^-?[\d\.]{1,12}+$/)
    {
        my $msg = 'Lower range "'.$analog->{lower_range}.'" is not numeric or is too long (max 13 digits, including "-" and ".")';

        log_error( $msg, $dim->{name}, "ANALOG_".$analog->{channel_index} );
        
    }

    if($analog->{upper_range} !~ /^-?[\d\.]{1,12}+$/)
    {
        my $msg = 'Upper range "'.$analog->{upper_range}.'" is not numeric or is too long (max 13 digits, including "-" and ".")';

        log_error( $msg, $dim->{name}, "ANALOG_".$analog->{channel_index} );
    }

    if
    (
       first
           { $_->{channel_index} eq $analog->{channel_index} }
           @{ $dim->{analog} }
    )
    {
        my $msg = 'Channel number "'.$analog->{channel_index}.'" already taken ';

        log_error( $msg, $dim->{name}, "ANALOG_".$analog->{channel_index} );

     }
}


sub validate_digital($$$)
{

    my ($host, $dim, $digital) = @_;
    
    if(length $digital->{signal_name} > 31)
    {
        my $msg = 'Name too long: "'.$digital->{signal_name}.'" (max 31 chars)';

        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
    }
    
    if(string_has_special_xml_chars($digital->{signal_name}))
    {
        my $msg = 'Name "'.$digital->{signal_name}.'" contains xml chars like & < > ';

        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
    }

    if(length $digital->{one} > 7)
    {
        my $msg = 'HIGH value too long "'.$digital->{one}.'" (max 7 chars)';

        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
    }

    if (length $digital->{zero} > 7)
    {
        my $msg = 'LOW value too long "'.$digital->{zero}.'" (max 7 chars)';
        
        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
    }

    if ($digital->{bit_number} =~ /\D/)
    {
        my $msg = 'DIM number "'.$digital->{bit_number}.'" is not a number';
        
        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
    }

    if($digital->{fault} !~ /^\s*(FAULT|STATUS)\s*$/i)
    {
        my $msg = 'Signal type "'.$digital->{fault}.'" should be either FAULT or STATUS';
        
        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
        
    }

    if($digital->{fault} =~ /FAULT/i and not ( empty($digital->{zero}) and $digital->{one} =~ /^\s*FAULT|INCOHER\s*$/i ) )
    {
        my $msg = "FAULT signals must define \"FAULT\" or \"INCOHER\" as value when high, and empty when low";
        
        log_error( $msg, $dim->{name}, "DIG_".$digital->{bit_number} );
        #" is defining fault, but it's low meaning is not empty or hight meaning is not fault";
    }
}


sub validate_host($)
{
    my $host = shift;

    if(empty($host->{variant}))
    {
        push(@errors, "Variant is not defined");
    }
}


# Make output xml document based on prepared data
sub fill_template(@)
{
    my ($host, $dim) = @_;

    my $out = '';

    $out .=
'<!-- DIM TYPE '.$host->{code_nohc}.' : '.get_label($host->{code}).' -->

<dim_type comp="'.$host->{code}.'" platform="'.(lc $host->{fgc}).'" position="'.$dim->{position}.'" variant="'.$host->{variant}.'">';

    # Add analogue channels if defined

    if(@{ $dim->{analog}})
    {
        $out .=
'

    <!-- Analogue Channels        1234567890123 -->
';

        for my $analog (@{ $dim->{analog} })
        {
            $out .=
'
    <ana_channel index="'.$analog->{channel_index}.
    '" label="'.( pack 'A14', clear_empty( \$analog->{signal_name} ).'"' ).
    ' min_0V="'.( pack 'A14', clear_empty( \$analog->{lower_range} ).'"' ).
    ' max_5V="'.( pack 'A14', clear_empty( \$analog->{upper_range} ).'"' ).
    ' units="'.( clear_empty \$analog->{units} ).
    '"/>';
        }
    }

    # Add digital channels

    $out .=
'

    <!-- Digital Channels                  1234567890123456789012345678901        1234567       1234567 -->
';
    for my $digital (@{ $dim->{digital} })
    {
        next unless @$digital;

        $out .=
'
    <dig_bank index="'.($digital == $dim->{digital}->[0] ? '4' : '5').'">';

        for my $input (@$digital)
        {
            $out .=
'
        <input index="'.( pack 'A3', $input->{bit_number}.'"' ).
        ' fault="'.( $input->{fault} =~ /FAULT/i or 0 ).'"'.
        ' label="'.( pack 'A32', $input->{signal_name}.'"' ).
        ' zero="'.( pack 'A8', clear_empty( \$input->{zero} ).'"' ).
        ' one="'.( pack 'A8', clear_empty( \$input->{one} ).'"' ).
        '/>'
        }

        $out .=
'
    </dig_bank>
';
    }

    $out .=
    '
</dim_type>

<!-- EOF                                   1234567890123456789012345678901        1234567       1234567 -->
';

    $out;
}


sub fill_template_dims_inc($$$)
{
    my ($hosts, $system, $label) = @_;

    my $out = '';
    my $dim_idx = 0;

    $out .=
'<!-- '.
$system.' '.$label.
' -->

';

    for my $host (@$hosts)
    {
        for my $dim (@{ $host->{dims} })
        {
            $out .=
'<dim'.
' name="'.$dim->{name}.'"'.(' ' x (13 - length($dim->{name}))).
' type="'.$host->{code}.'"'.
' position="'.$dim->{position}.'"'.
' variant="'.$host->{variant}.'"'.
' dim_idx="'.$dim_idx.'"'.
' bus_addr="'.'"'.
' />
';

            $dim_idx++;
        }

        $out .= '
'
    }

    return $out;
}

# End of functions



# Run this script
# Check if paths are provided to script as arguments or help message is requested
if(@ARGV < 1 or first {/-h|-help/} @ARGV)
{
    die 'Usage: '.dirname($0).' <dims_description.csv>'."\n";
}

# Resolve bundle path
my $script_path = dirname($0);
my $bundle_dir  = `cd '${script_path}'; git rev-parse --show-toplevel`;
chomp $bundle_dir;


# Get name of the system from script argument
# If no system name is provided, by default a file named "mapping.xml" under dims_inc will be created.
# To skip writing this file (=> just compile the provided DIM definitions), pass the --no-system option.
my $no_system;
my $system;

GetOptions(
    "no-system" => \$no_system,
    "system|s=s" => \$system
);

my $mapping_filename;
if (!$no_system)
{
    $mapping_filename = $bundle_dir.'/def/src/dims_inc/'.( lc $system || 'mapping' ).'.xml';
}


# Get paths
my (@dims_description_filenames) = @ARGV;
my $component_labels_filename    = $bundle_dir.'/def/src/component_labels.xml';


# Prepare hosts array
my @hosts = ();

# Parse dims
for my $dims_description_filename (@dims_description_filenames)
{

    $current_filename = basename($dims_description_filename);
    push(@hosts, parse(read_file($dims_description_filename, array_ref => 1)));
}

# Print all parsing errors if any

if(@errors)
{
    die "ERRORS were encountered\n"."File:line\tDIM\tChannel\tError\n".join("\n", @errors)."\n";
}


# Write output xml's
for my $host (@hosts)
{
    for my $dim (@{ $host->{dims} })
    {
        # Concatenate suffix

        my $output_filename = $bundle_dir.'/def/src/dim_types/'.lc( $host->{code_nohc}.'_'.$dim->{position}.$host->{variant}.'.xml' );
	print("$output_filename\n");

        # Write filled template into an output file

        write_file($output_filename, fill_template($host, $dim));
    }
}

# Write mapping
if ($mapping_filename)
{
    write_file($mapping_filename, fill_template_dims_inc(\@hosts, $system, get_label('HC'.uc($system).'___')));
}

# EOF
