#!/usr/bin/perl -w
#
# Name:     send_fgc_code.pl
# Purpose:  Clearing a gateway code slot and sending the latest code to it.

use strict;
use warnings;

use Cwd qw/abs_path cwd/;
use Test::Utils;
use List::Util qw(first);

my @code_paths = ("../../fgc/codes/C001_50_BootProg/fieldbus/",
                  "../../fgc/codes/C008_51_MainProg/fieldbus/",
                  "../../fgc/codes/C017_53_MainProg/fieldbus/",
                  "../../fgc/codes/C010_59_MainProg/fieldbus/",
                  "../../fgc/codes/C011_60_BootProg/fieldbus/",
                  "../../fgc/codes/C023_62_MainProg/fieldbus/",
                  "../../fgc/codes/C024_63_MainProg/fieldbus/");

my @allowed_classes = qw(50 51 53 59 60 62 63);

my @allowed_gateways = ("CFC-866-RFIP1",
                        "CFC-866-RFIP2",
                        "CFC-866-RFIP3",
                        "CFC-866-RETH1",
                        "CFC-866-RETH2",
                        "CFC-866-RETH5");

sub print_help()
{
    print "Usage: ./send_fgc_code.pl --class=<fgc_class> --gateway=<gateway>\n";
    print "Where:\n";
    print "\tfgc_class:";

    foreach (@allowed_classes)
    {
        print " $_";
    }

    print "\n";

    print "\tgateway:  ";

    foreach (@allowed_gateways)
    {
        print " $_";
    }

    print "\n\n";
}

# Get the input and validate it

my %params = (class=>undef, gateway=>undef, protocol=>'TCP');
   %params = parse_opts %params;

my $class   = $params{class};
my $gateway = $params{gateway};

if(!defined $class or !defined $gateway)
{
    print_help() && die "ERROR: Missing fgc class or gateway name";
}

# Get the fgc class index in the array

my $code_index = first { $allowed_classes[$_] eq $class } 0..$#allowed_classes;

if(!defined $code_index)
{
    print_help() && die "ERROR: Unallowed fgc class specified";
}

# Check if the specified gateway is one of the development gateways

my $gateway_index = first { $allowed_gateways[$_] eq $gateway } 0..$#allowed_gateways;

if(!defined $gateway_index)
{
    print_help() && die "ERROR: Unallowed gateway specified";
}

# Get the absolute path to the script

my ($real_path) = abs_path($0) =~ m/(.*)send_fgc_code.pl/i;

# chdir to the scripts directory first and then to the proper code directory

chdir($real_path)               or die "Cannot chdir to $real_path - $!";
chdir($code_paths[$code_index]) or die "Cannot chdir to $code_paths[$code_index] - $!";

my $dir = cwd();

# Get list of codes sorted by date and choose the newest one
# TODO Use perl instead of ls?

my @list   = `ls -t`;
my $newest = $list[0];

if(!defined $newest)
{
    die "ERROR: No code could be found in $dir\n";
}

chomp $newest;

$dir .= "/$newest";

# Authenticate by location

authenticate($params{authenticate});

# Check if the gateway name matches

ok(get($gateway, "DEVICE.NAME") eq $gateway, "Connected to the gateway");

chdir($newest) or die "Cannot chdir to $newest - $!";

opendir my $code_dir, $dir or die "ERROR: Cannot open directory: $!";

while (my $code = readdir $code_dir)
{
    # Skip . and ..

    next if $code =~ /^[.]/;

    # Get code info

    my $code_info_path = $real_path . "/code_info.pl";

    my $code_info = `$code_info_path $code`;

    # Extract code id from the info

    my $slot = 0;

    if($code_info =~ /code_id=\s*(\d+)/)
    {
        $slot = $1;

        if($slot < 1 || $slot > 31)
        {
            die "Code slot should be in range [1, 31], found $slot, possible corruption of the code";
        }

        # This is a hack! Apparently code_id coming from the code_info doesn't have to match code slot
        # on the gateway! After removal of class 52 binaries (code_id 9), the slot numbers of codes
        # with higher ID got left shifted by one.

        if($slot > 9)
        {
            $slot--;
        }
    }
    else
    {
        die "Couldn't extract code_id from code info";
    }

    # Send command to clear code

    set($gateway, "CODE.SLOT$slot", "");

    # Check if the code was cleared

    ok(get($gateway, "CODE.INFO.CLASS[$slot]")    eq "0",   "CODE.INFO.CLASS[$slot] == 0");
    ok(get($gateway, "CODE.INFO.VERSION[$slot]")  eq "0",   "CODE.INFO.VERSION[$slot] == 0");
    ok(get($gateway, "CODE.INFO.ID[$slot]")       eq "255", "CODE.INFO.ID[$slot] == 255");
    ok(get($gateway, "CODE.INFO.CHECKSUM[$slot]") eq "0",   "CODE.INFO.CHECKSUM[$slot] == 0");

    ok(1, "Code cleared");

    # Open the code file

    open(CODE_FILE, "<:raw", "$code") or die "ERROR: Unable to open file $code: $!";

    # Read contents of input file into $data

    my $buf;
    my $data;

    while(read(CODE_FILE, $buf, 16384))
    {
        $data .= $buf;
    }

    close(CODE_FILE);

    # Extract bytes from data

    my @bytes = split("", $data);
    $_        = unpack("C", $_) for @bytes;

    # Construct commands

    my $i = 0;
    while($i < @bytes)
    {
        my $start_i = $i;
        my $value   = '';

        my $first = 1;
        for( ; $i < @bytes && $i < ($start_i + 30000) ; $i++)
        {
            $value .= "," if(!$first);
            $value .= sprintf("0x%02X", $bytes[$i]);
            $first  = 0;
        }

        set($gateway, "CODE.SLOT$slot\[$start_i,]", $value);
    }

    ok(1, "Code loaded");

    # Cross-check that the code version on the gateway matches the local code version

    ok(get($gateway, "CODE.INFO.CLASS[$slot]")   eq $class,  "Code classes match");
    ok(get($gateway, "CODE.INFO.VERSION[$slot]") eq $newest, "Code versions match");
}

closedir $code_dir;

# EOF
