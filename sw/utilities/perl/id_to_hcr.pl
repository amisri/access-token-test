#!/usr/bin/perl -w
#
# Purpose: Reads a file containing a list of dallas id, and finds the corresponding hcr on the DB
# Author: Miguel Hermo

use strict;

use FGC::DB;
use FGC::Names;

sub is_dallas_id($)
{
    my ($dallas_id) = @_;
       
    if( !$dallas_id || $dallas_id !~ /^[A-Z\d]{16}$/ )
    {
        return(0);
    }
    else
    {
        return(1);
    }
}



die "Usage: $0 <filename>\n" if(@ARGV != 1);
my ($filename) = @ARGV;

# Prompt for username

my $dbh = FGC::DB::connect(FGC::DB::FGC_DB_USERNAME, FGC::DB::FGC_DB_PASSWORD) or die "Unable to connect to database: $!\n";

open(FILE, '<', $filename) or die "Unable to open $filename: $!\n";

while(<FILE>)
{
    s:\r\n:\n:g;
    chomp;
    
    my $dallas_id = $_;
    if(is_dallas_id($dallas_id))
    {
        my $mapping = FGC::DB::get_component_mappings($dbh, undef, $dallas_id);

        if(defined($$mapping[0]->{CMA_CMP_ID}))
        {
            my $entry =@$mapping[0];
            print "$dallas_id, $entry->{CMA_CMP_ID}\n";
        }
        else
        {
            print "$dallas_id, UNKNOWN DALLAS_ID\n";
        }
    }
    else
    {
        print "$_\n";
    }
}

close(FILE);

FGC::DB::disconnect($dbh);

# EOF

