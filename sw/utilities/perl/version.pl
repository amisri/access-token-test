#!/usr/bin/perl -w
#
# Name:     version.pl
# Purpose:  Make a header file containing an incrementing version
# Author:   Stephen Page

use File::Basename;
use IO::Seekable;
use strict;

die "Usage: ", basename($0), " <version file>\n" if(@ARGV != "1");
my ($version_filename) = @ARGV;

# Get current version from file

open(VERSION_FILE, '+<', $version_filename)
    or die "Unable to open version file $version_filename: $!\n";

# Read constant name and version number

my $const   =  my $version =  <VERSION_FILE>;
$const      =~ s/#define\s+(\w+)\s+(\d+)\s*/$1/;
$version    =~ s/#define\s+(\w+)\s+(\d+)\s*/$2/;

# Increment version number

$version++;

# Write new version to version file

seek( VERSION_FILE, 0, SEEK_SET);
print VERSION_FILE "#define $const $version\n";
close(VERSION_FILE);

# EOF
