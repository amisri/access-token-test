#!/usr/bin/perl -w

use strict;
use warnings;

use File::Slurp;

my $filename = $ARGV[0];

my @lines = read_file($filename, chomp => 1);

# Get test name from the filename

my $test_name = $filename;
$test_name =~ s!.*/converters/!!;
$test_name =~ s/results/tests/;
$test_name =~ s/\.log/\.cct/;

# Consider the test as failed when a line with TAP's 'not ok' has been found

my $failed = 0;

foreach(@lines)
{
	if($_ =~ /^not ok\b/)
	{
		$failed = 1;
	}
}

# Create a JUnit xml file in the same place as the log file

my $f = $filename;
$f =~ s/\.log/\.xml/;

open my $junit_xml_file, ">", $f or die "Cannot open file '$f': $!";

# Create the xml file

print $junit_xml_file "<testsuite name='$test_name' tests='1' failures='$failed'>\n";
print $junit_xml_file "<testcase name='$test_name'>\n";

if($failed)
{
	print $junit_xml_file "<failure>\n";
}

foreach(@lines)
{
	print $junit_xml_file "$_\n";
}

if($failed)
{
	print $junit_xml_file "</failure>\n";
}

print $junit_xml_file "</testcase>\n";
print $junit_xml_file "</testsuite>\n";
