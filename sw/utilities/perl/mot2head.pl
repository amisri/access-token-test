#!/usr/bin/perl -w
#
# Name:     mot2head.pl
# Purpose:  Generates a header file containing an array representing code from an S-record file
# Author:   Stephen Page

use File::Basename;
use strict;

die "Usage: ", basename($0), " <variable prefix> <S-record file> <output file>\n" if(@ARGV != 3);

my ($var_prefix, $mot_filename, $output_filename) = @ARGV;

open(MOT_FILE, '<', $mot_filename)
    or die "Unable to open S-record file $mot_filename : $!\n";

# Read Motorola file and extract values of all addresses

my %addresses;

while(<MOT_FILE>)
{
    # Remove carriage-return and line-feed

    s/\015\012//;
    chomp;

    if(/^S2/ || /^S3/)
    {
        # Extract address and data from line

        my $address;
        my $data_ascii;

        if(/^S2/)
        {
            $address    = hex(substr($_, 4, 6));
            $data_ascii = substr($_, 10, length($_) - 12);
        }
        elsif(/^S3/)
        {
            $address    = hex(substr($_, 4, 8));
            $data_ascii = substr($_, 12, length($_) - 14);
        }

        my @data_bytes;

        # Convert data to binary

        my $byte;
        my $i;
        for($i = 0 ; $i < length($data_ascii) ; $i += 2)
        {
            $byte = substr($data_ascii, $i, 2);
            push(@data_bytes, "0x$byte");
        }

        # Store data in hash

        $addresses{$address} = \@data_bytes;
    }
}

close(MOT_FILE);

# Write header file

open(OUT_FILE, '>', $output_filename)
    or die "Unable to open binary file $output_filename : $!\n";

print OUT_FILE  "// This file was automatically generated - DO NOT EDIT\n",
                "\n",
                "#ifndef ", "\U$var_prefix", "CODE_H\n",
                "#define ", "\U$var_prefix", "CODE_H\n",
                "\n",
                "static INT8U const ", "\L$var_prefix", "_code[]\n",
                "= {";

my $line_bytes  = 0;
my $num_bytes   = 0;

my $previous_address;
for my $address (sort {$a <=> $b} (keys(%addresses)))
{
    # Check whether data is not sequential

    if(defined($previous_address) &&
       $address != $previous_address + @{$addresses{$previous_address}})
    {
        # Write pad bytes between addresses

        my $num_pad_bytes = ($address - $previous_address) - @{$addresses{$previous_address}};

        my $i;
        for($i = 0 ; $i < $num_pad_bytes ; $i++)
        {
            print OUT_FILE "\n" if(!$line_bytes);
            print OUT_FILE "0xFF, ";
            $num_bytes++;

            $line_bytes = ($line_bytes + 1) % 16;
        }
    }

    # Write data to file

    for(@{$addresses{$address}})
    {
        print OUT_FILE "\n" if(!$line_bytes);
        print OUT_FILE "$_, ";
        $num_bytes++;

        $line_bytes = ($line_bytes + 1) % 16;
    }

    $previous_address = $address;
}

print OUT_FILE  "\n",
                "}\n",
                ";\n",
                "\n",
                "#define ", "\U$var_prefix", "_CODE_SIZE $num_bytes\n",
                "\n",
                "#endif\n",
                "\n",
                "// EOF\n";

close(OUT_FILE);

# EOF
