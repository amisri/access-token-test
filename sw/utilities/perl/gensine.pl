#!/usr/bin/perl -w
#
# Name:     gensine.pl
# Purpose:  Generate a sine wave in FGC Spy compatible format
# Author:   Stephen Page

use constant PI => 3.1415926535897932384626433832795029;
use strict;

die "Usage: $0 <number of samples> <frequency> <PP amplitude>\n" if(@ARGV != 3);
my ($num_samples, $frequency, $amplitude) = @ARGV;
$amplitude /= 2;

my $angular_frequency = 2 * PI * $frequency;

# Write headings

print "Time,Sine\n";

# Write samples

for(my $i = 0 ; $i < $num_samples ; $i++)
{
    my $time    = $i * 0.001;
    my $value   = $amplitude * sin($angular_frequency * $time);

    printf("%.3f,%.10e\n", $time, $value);
}

# EOF
