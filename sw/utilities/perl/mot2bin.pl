#!/usr/bin/perl -w
#
# Name:     mot2bin.pl
# Purpose:  Convert a Motorola S-record file into a binary file
# Author:   Stephen Page

use File::Basename;
use strict;

die "Usage: ", basename($0), " <S-record file> <binary file>\n" if(@ARGV != 2);

my ($mot_filename, $bin_filename) = @ARGV;

open(MOT_FILE, '<', $mot_filename)
    or die "Unable to open S-record file $mot_filename : $!\n";

# Read Motorola file and extract values of all addresses

my %addresses;

while(<MOT_FILE>)
{
    # Remove carriage-return and line-feed

    s/\015\012//;
    chomp;

    if(/^S2/ || /^S3/)
    {
        # Extract address and data from line

        my $address;
        my $data_ascii;

        if(/^S2/)
        {
            $address    = hex(substr($_, 4, 6));
            $data_ascii = substr($_, 10, length($_) - 12);
        }
        elsif(/^S3/)
        {
            $address    = hex(substr($_, 4, 8));
            $data_ascii = substr($_, 12, length($_) - 14);
        }

        my @data_bin;

        # Convert data to binary

        my $byte;
        my $i;
        for($i = 0 ; $i < length($data_ascii) ; $i += 2)
        {
            $byte = substr($data_ascii, $i, 2);
            push(@data_bin, pack("C", hex($byte)));
        }

        # Store data in hash

        $addresses{$address} = \@data_bin;
    }
}

close(MOT_FILE);

# Write binary file

open(BIN_FILE, '>', $bin_filename)
    or die "Unable to open binary file $bin_filename : $!\n";

my $previous_address;
for my $address (sort {$a <=> $b} (keys(%addresses)))
{
    # Check whether data is not sequential

    if(defined($previous_address) &&
       $address != $previous_address + @{$addresses{$previous_address}})
    {
        # Write pad bytes between addresses

        my $num_pad_bytes = ($address - $previous_address) - @{$addresses{$previous_address}};

        syswrite(BIN_FILE, pack("C", 0xFF) x $num_pad_bytes);
    }

    # Write data to file

    syswrite(BIN_FILE, join('', @{$addresses{$address}}));

    $previous_address = $address;
}

close(BIN_FILE);

# EOF
