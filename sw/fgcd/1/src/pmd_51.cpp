/*!
 * @file   pmd_51.cpp
 * @brief  Handle FGC class 51 post-mortem data
 * @author Stephen Page
 * @author Michael Davis
 */

#include <arpa/inet.h>
#include <cstdint>
#include <cstring>
#include <pm-dc-rda3-lib/PMClient.hpp>
#include <pm-dc-rda3-lib/PMData.hpp>
#include <pm-dc-rda3-lib/PMError.hpp>

#include <classes/51/logPostmortem.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <pmd_51.h>
#include <sub_cmwdata_gen.h>
#include <sub_utilities.h>


// Global variables

const uint8_t  MAX_SUB_STRING_LENGTH = 255;
const uint32_t PM_STATUS_MAX_BUF_LEN = PM_SELF_BUF_LEN_51;

//! Data structure to store the intermediate status arrays to produce the PMD data

struct pmd_51_status_arrays_t
{
    uint32_t  num_samples;
    long long timestamps[PM_STATUS_MAX_BUF_LEN];
    int       class_id[PM_STATUS_MAX_BUF_LEN];
    char      data_status[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     data_status_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_faults[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_faults_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_warnings[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_warnings_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_latched[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_latched_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_unlatched[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_unlatched_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_op[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_op_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_vs[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_vs_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_pc[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_pc_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_meas_a[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_meas_a_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_meas_b[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_meas_b_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_dcct_a[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_dcct_a_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_dcct_b[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_dcct_b_ptr[PM_STATUS_MAX_BUF_LEN];
    int16_t   i_err_ma[PM_STATUS_MAX_BUF_LEN];
    int16_t   i_diff_ma[PM_STATUS_MAX_BUF_LEN];
    float     i_earth_pcnt[PM_STATUS_MAX_BUF_LEN];
    float     i_err[PM_STATUS_MAX_BUF_LEN];
    float     i_diff[PM_STATUS_MAX_BUF_LEN];
    float     i_meas[PM_STATUS_MAX_BUF_LEN];
    float     i_ref[PM_STATUS_MAX_BUF_LEN];
    float     v_meas[PM_STATUS_MAX_BUF_LEN];
    float     v_ref[PM_STATUS_MAX_BUF_LEN];

};

pmd_51_status_arrays_t pm_status_arrays;

//! Data structure to store the intermediate FGC_61 arrays to produce the PMD data

struct pmd_51_self_arrays_t
{
    // Events arrays

    long long evt_timestamps[PM_FGC_51_EVENTS_LEN];
    char      evt_property[PM_FGC_51_EVENTS_LEN][PM_FGC_51_EVENTS_PROPERTY_LEN+1];
    char*     evt_property_ptr[PM_FGC_51_EVENTS_LEN];
    char      evt_symbol[PM_FGC_51_EVENTS_LEN][PM_FGC_51_EVENTS_SYMBOL_LEN+1];
    char*     evt_symbol_ptr[PM_FGC_51_EVENTS_LEN];
    char      evt_action[PM_FGC_51_EVENTS_LEN][PM_FGC_51_EVENTS_ACTION_LEN+1];
    char*     evt_action_ptr[PM_FGC_51_EVENTS_LEN];

    // iab arrays

    long long iab_timestamps[PM_FGC_51_IAB_LEN];
    float     iab_i_a[PM_FGC_51_IAB_LEN];
    float     iab_i_b[PM_FGC_51_IAB_LEN];

    // ireg arrays

    long long ireg_timestamps[PM_FGC_51_IREG_LEN];
    float     ireg_i_ref_rst[PM_FGC_51_IREG_LEN];
    float     ireg_i_meas[PM_FGC_51_IREG_LEN];
    float     ireg_v_ref[PM_FGC_51_IREG_LEN];
    float     ireg_v_meas[PM_FGC_51_IREG_LEN];

    // ileads arrays

    long long ileads_timestamps[PM_FGC_51_ILEADS_LEN];
    float     ileads_i_meas[PM_FGC_51_ILEADS_LEN];
    float     ileads_v_meas[PM_FGC_51_ILEADS_LEN];
    float     ileads_u_lead_pos[PM_FGC_51_ILEADS_LEN];
    float     ileads_u_lead_neg[PM_FGC_51_ILEADS_LEN];

    // iearth array

    long long iearth_timestamps[PM_FGC_51_IEARTH_LEN];
    float     iearth_i_earth[PM_FGC_51_IEARTH_LEN];
};

struct pmd_51_self_arrays_t pmd_51_arrays;

static void pmd_51AddStatus(PMData& pmdata, int64_t timestamp, const struct fgc_stat* trigger, const struct PM_data* status, uint32_t LEN)
{
    const fgc51_stat *c51 = &trigger->class_data.c51;

    // Parameters: Trigger data

    pmdata.registerParameter("TIMESTAMP", timestamp);

    subUtilitiesPmdBitmaskToText(trigger->data_status,sym_names_51_data_status,pm_status_arrays.data_status[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("DATA_STATUS",pm_status_arrays.data_status[0]);

    pmdata.registerParameter("CLASS_ID", (int32_t)trigger->class_id);

    subUtilitiesPmdBitmaskToText(ntohs(c51->st_faults),sym_names_51_flt,pm_status_arrays.st_faults[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_FAULTS",pm_status_arrays.st_faults[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c51->st_warnings),sym_names_51_wrn,pm_status_arrays.st_warnings[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_WARNINGS",pm_status_arrays.st_warnings[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c51->st_latched),sym_names_51_lat,pm_status_arrays.st_latched[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_LATCHED",pm_status_arrays.st_latched[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c51->st_unlatched),sym_names_51_unl,pm_status_arrays.st_unlatched[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_UNLATCHED",pm_status_arrays.st_unlatched[0]);

    subUtilitiesPmdEnumToText(c51->state_op,sym_names_51_op,pm_status_arrays.state_op[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_OP",pm_status_arrays.state_op[0]);

    subUtilitiesPmdEnumToText(c51->state_vs,sym_names_51_vs,pm_status_arrays.state_vs[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_VS",pm_status_arrays.state_vs[0]);

    subUtilitiesPmdEnumToText(c51->state_pc,sym_names_51_pc,pm_status_arrays.state_pc[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_PC",pm_status_arrays.state_pc[0]);

    subUtilitiesPmdBitmaskToText(c51->st_meas_a,sym_names_51_meas,pm_status_arrays.st_meas_a[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_MEAS_A",pm_status_arrays.st_meas_a[0]);

    subUtilitiesPmdBitmaskToText(c51->st_meas_b,sym_names_51_meas,pm_status_arrays.st_meas_b[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_MEAS_B",pm_status_arrays.st_meas_b[0]);

    subUtilitiesPmdBitmaskToText(c51->st_dcct_a,sym_names_51_dcct,pm_status_arrays.st_dcct_a[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_DCCT_A",pm_status_arrays.st_dcct_a[0]);

    subUtilitiesPmdBitmaskToText(c51->st_dcct_b,sym_names_51_dcct,pm_status_arrays.st_dcct_b[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_DCCT_B",pm_status_arrays.st_dcct_b[0]);

    pmdata.registerParameter("I_ERR_MA",subUtilitiesSignedNtohs(c51->i_err_ma));
    pmdata.addUnits("I_ERR_MA","mA");

    pmdata.registerParameter("I_DIFF_MA",subUtilitiesSignedNtohs(c51->i_diff_ma));
    pmdata.addUnits("I_DIFF_MA","mA");

    pmdata.registerParameter("I_EARTH_PCNT",0.01F * (float)subUtilitiesSignedNtohs(c51->i_earth_cpcnt));
    pmdata.addUnits("I_EARTH_PCNT","%");

    pmdata.registerParameter("I_MEAS",subUtilitiesNtohf(c51->i_meas));
    pmdata.addUnits("I_MEAS","A");

    pmdata.registerParameter("I_REF",subUtilitiesNtohf(c51->i_ref));
    pmdata.addUnits("I_REF","A");

    pmdata.registerParameter("V_MEAS",subUtilitiesNtohf(c51->v_meas));
    pmdata.addUnits("V_MEAS","V");

    pmdata.registerParameter("V_REF",subUtilitiesNtohf(c51->v_ref));
    pmdata.addUnits("V_REF","V");

    // STATUS Signal

    memset(&pm_status_arrays,0,sizeof(pm_status_arrays));

    uint32_t &num_samples = pm_status_arrays.num_samples;

    for(uint32_t i = 0; i != LEN; i++)
    {
        const PM_data *data = &status[i];
        c51 = &data->status.class_data.c51;

        // Ignore missing status data

        if(!data->time_sec) continue;

        // Fill arrays

        pm_status_arrays.timestamps[num_samples]= static_cast<long long>(ntohl(data->time_sec))*1000*1000*1000 + static_cast<long long>(ntohl(data->time_usec))*1000;

        subUtilitiesPmdBitmaskToText(data->status.data_status,sym_names_51_data_status,pm_status_arrays.data_status[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.data_status_ptr[num_samples] = pm_status_arrays.data_status[num_samples];

        pm_status_arrays.class_id[num_samples]  = data->status.class_id;

        subUtilitiesPmdBitmaskToText(ntohs(c51->st_faults),sym_names_51_flt,pm_status_arrays.st_faults[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_faults_ptr[num_samples] = pm_status_arrays.st_faults[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c51->st_warnings),sym_names_51_wrn,pm_status_arrays.st_warnings[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_warnings_ptr[num_samples] = pm_status_arrays.st_warnings[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c51->st_latched),sym_names_51_lat,pm_status_arrays.st_latched[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_latched_ptr[num_samples] = pm_status_arrays.st_latched[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c51->st_unlatched),sym_names_51_unl,pm_status_arrays.st_unlatched[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_unlatched_ptr[num_samples] = pm_status_arrays.st_unlatched[num_samples];

        subUtilitiesPmdEnumToText(c51->state_op,sym_names_51_op,pm_status_arrays.state_op[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_op_ptr[num_samples] = pm_status_arrays.state_op[num_samples];

        subUtilitiesPmdEnumToText(c51->state_vs,sym_names_51_vs,pm_status_arrays.state_vs[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_vs_ptr[num_samples] = pm_status_arrays.state_vs[num_samples];

        subUtilitiesPmdEnumToText(c51->state_pc,sym_names_51_pc,pm_status_arrays.state_pc[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_pc_ptr[num_samples] = pm_status_arrays.state_pc[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c51->st_meas_a),sym_names_51_meas,pm_status_arrays.st_meas_a[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_meas_a_ptr[num_samples] = pm_status_arrays.st_meas_a[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c51->st_meas_b),sym_names_51_meas,pm_status_arrays.st_meas_b[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_meas_b_ptr[num_samples] = pm_status_arrays.st_meas_b[num_samples];

        subUtilitiesPmdBitmaskToText(c51->st_dcct_a,sym_names_51_dcct,pm_status_arrays.st_dcct_a[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_dcct_a_ptr[num_samples] = pm_status_arrays.st_dcct_a[num_samples];

        subUtilitiesPmdBitmaskToText(c51->st_dcct_b,sym_names_51_dcct,pm_status_arrays.st_dcct_b[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_dcct_b_ptr[num_samples] = pm_status_arrays.st_dcct_b[num_samples];

        pm_status_arrays.i_err_ma[num_samples] = subUtilitiesSignedNtohs(c51->i_err_ma);
        pm_status_arrays.i_diff_ma[num_samples] = subUtilitiesSignedNtohs(c51->i_diff_ma);
        pm_status_arrays.i_earth_pcnt[num_samples] = 0.01F * (float)subUtilitiesSignedNtohs(c51->i_earth_cpcnt);
        pm_status_arrays.i_meas[num_samples] = subUtilitiesNtohf(c51->i_meas);
        pm_status_arrays.i_ref[num_samples] = subUtilitiesNtohf(c51->i_ref);
        pm_status_arrays.v_meas[num_samples] = subUtilitiesNtohf(c51->v_meas);
        pm_status_arrays.v_ref[num_samples] = subUtilitiesNtohf(c51->v_ref);

        // Increase number of samples counter

        num_samples++;
    }

    // Register timestamp

    pmdata.registerArray("STATUS.TIMESTAMP",pm_status_arrays.timestamps,num_samples);

    // Register arrays and associate them to the STATUS signal and the STATUS.TIMESTAMP

    pmdata.registerArray("STATUS.CLASS_ID",pm_status_arrays.class_id,num_samples);
    pmdata.addAttribute("STATUS.CLASS_ID","timescale","STATUS");
    pmdata.addAttribute("STATUS.CLASS_ID","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.DATA_STATUS",const_cast<const char **>(pm_status_arrays.data_status_ptr),num_samples);
    pmdata.addAttribute("STATUS.DATA_STATUS","timescale","STATUS");
    pmdata.addAttribute("STATUS.DATA_STATUS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_FAULTS",const_cast<const char **>(pm_status_arrays.st_faults_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_FAULTS","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_FAULTS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_WARNINGS",const_cast<const char **>(pm_status_arrays.st_warnings_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_WARNINGS","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_WARNINGS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_LATCHED",const_cast<const char **>(pm_status_arrays.st_latched_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_LATCHED","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_LATCHED","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_UNLATCHED",const_cast<const char **>(pm_status_arrays.st_unlatched_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_UNLATCHED","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_UNLATCHED","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_OP",const_cast<const char **>(pm_status_arrays.state_op_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_OP","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_OP","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_VS",const_cast<const char **>(pm_status_arrays.state_vs_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_VS","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_VS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_PC",const_cast<const char **>(pm_status_arrays.state_pc_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_PC","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_PC","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_MEAS_A",const_cast<const char **>(pm_status_arrays.st_meas_a_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_MEAS_A","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_MEAS_A","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_MEAS_B",const_cast<const char **>(pm_status_arrays.st_meas_b_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_MEAS_B","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_MEAS_B","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_DCCT_A",const_cast<const char **>(pm_status_arrays.st_dcct_a_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_DCCT_A","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_DCCT_A","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_DCCT_B",const_cast<const char **>(pm_status_arrays.st_dcct_b_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_DCCT_B","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_DCCT_B","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.I_ERR_MA",pm_status_arrays.i_err_ma,num_samples);
    pmdata.addAttribute("STATUS.I_ERR_MA","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_ERR_MA","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_ERR_MA","mA");

    pmdata.registerArray("STATUS.I_DIFF_MA",pm_status_arrays.i_diff_ma,num_samples);
    pmdata.addAttribute("STATUS.I_DIFF_MA","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_DIFF_MA","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_DIFF_MA","mA");

    pmdata.registerArray("STATUS.I_EARTH_PCNT",pm_status_arrays.i_earth_pcnt,num_samples);
    pmdata.addAttribute("STATUS.I_EARTH_PCNT","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_EARTH_PCNT","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_EARTH_PCNT","%");

    pmdata.registerArray("STATUS.I_MEAS",pm_status_arrays.i_meas,num_samples);
    pmdata.addAttribute("STATUS.I_MEAS","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_MEAS","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_MEAS","A");

    pmdata.registerArray("STATUS.I_REF",pm_status_arrays.i_ref,num_samples);
    pmdata.addAttribute("STATUS.I_REF","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_REF","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_REF","A");

    pmdata.registerArray("STATUS.V_MEAS",pm_status_arrays.v_meas,num_samples);
    pmdata.addAttribute("STATUS.V_MEAS","timescale","STATUS");
    pmdata.addAttribute("STATUS.V_MEAS","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.V_MEAS","V");

    pmdata.registerArray("STATUS.V_REF",pm_status_arrays.v_ref,num_samples);
    pmdata.addAttribute("STATUS.V_REF","timescale","STATUS");
    pmdata.addAttribute("STATUS.V_REF","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.V_REF","V");
}

static void pmd_51AddFgc(PMData& pmdata)
{
    union  pm_51_self_union_t
    {
        pm_51_self_t self;
        char         buffer[PM_FGC_LOG_MAX_LEN];
    };

    // Circumvent strict-aliasing warning to cast to the FGC_51 self buffer contents format

    union  pm_51_self_union_t* self_union = reinterpret_cast<union pm_51_self_union_t*>(pm_globals.buffer.self.c51.fgc_buffers);
    struct pm_51_self_t* self = &self_union->self;

    // Signals

    // Generate arrays

    memset(&pmd_51_arrays, 0, sizeof(struct pmd_51_self_arrays_t));

    // EVENTS

    for(uint32_t i=0; i != PM_FGC_51_EVENTS_LEN; i++)
    {
        pmd_51_arrays.evt_timestamps[i] = static_cast<long long>(ntohl(self->events[i].time_sec))  * 1000 * 1000 * 1000
                                        + static_cast<long long>(ntohl(self->events[i].time_usec)) * 1000;

        memcpy(pmd_51_arrays.evt_property[i],self->events[i].property, PM_FGC_51_EVENTS_PROPERTY_LEN);
        pmd_51_arrays.evt_property_ptr[i] = pmd_51_arrays.evt_property[i];

        memcpy(pmd_51_arrays.evt_symbol[i],self->events[i].symbol, PM_FGC_51_EVENTS_SYMBOL_LEN);
        pmd_51_arrays.evt_symbol_ptr[i] = pmd_51_arrays.evt_symbol[i];

        memcpy(pmd_51_arrays.evt_action[i],self->events[i].action, PM_FGC_51_EVENTS_ACTION_LEN);
        pmd_51_arrays.evt_action_ptr[i] = pmd_51_arrays.evt_action[i];
    }

    // IAB

    long long iab_start = static_cast<long long>(ntohl(self->iab_last_time_sec))  * 1000 * 1000 * 1000
                        + static_cast<long long>(ntohl(self->iab_last_time_usec)) * 1000;

    iab_start -= (PM_FGC_51_IAB_LEN - 1) * PM_FGC_51_IAB_SAMPLING_NS;

    for(long long i=0; i != PM_FGC_51_IAB_LEN; i++)
    {
        pmd_51_arrays.iab_timestamps[i] = iab_start + i * PM_FGC_51_IAB_SAMPLING_NS;

        pmd_51_arrays.iab_i_a[i] = subUtilitiesNtohf(self->iab[i].i_a);
        pmd_51_arrays.iab_i_b[i] = subUtilitiesNtohf(self->iab[i].i_b);
    }

    // IREG (ILOOP)

    long long ireg_start = static_cast<long long>(ntohl(self->ireg_last_time_sec))  * 1000 * 1000 * 1000
                         + static_cast<long long>(ntohl(self->ireg_last_time_usec)) * 1000;

    ireg_start -= (PM_FGC_51_IREG_LEN - 1) * PM_FGC_51_IREG_SAMPLING_NS;

    for(long long i=0; i != PM_FGC_51_IREG_LEN; i++)
    {

        pmd_51_arrays.ireg_timestamps[i] = ireg_start + i * PM_FGC_51_IREG_SAMPLING_NS;

        pmd_51_arrays.ireg_i_ref_rst[i] = subUtilitiesNtohf(self->ireg[i].i_ref_rst);
        pmd_51_arrays.ireg_i_meas[i]    = subUtilitiesNtohf(self->ireg[i].i_meas);
        pmd_51_arrays.ireg_v_ref[i]     = subUtilitiesNtohf(self->ireg[i].v_ref);
        pmd_51_arrays.ireg_v_meas[i]    = subUtilitiesNtohf(self->ireg[i].v_meas);
    }

    // ILEADS

    long long ileads_start = static_cast<long long>(ntohl(self->ileads_last_time_sec))  * 1000 * 1000 * 1000
                           + static_cast<long long>(ntohl(self->ileads_last_time_usec)) * 1000;

    ileads_start -= (PM_FGC_51_ILEADS_LEN - 1) * PM_FGC_51_ILEADS_SAMPLING_NS;

    for(long long i=0; i != PM_FGC_51_ILEADS_LEN; i++)
    {
        pmd_51_arrays.ileads_timestamps[i] = ileads_start + i * PM_FGC_51_ILEADS_SAMPLING_NS;

        pmd_51_arrays.ileads_i_meas[i]     = subUtilitiesNtohf(self->ileads[i].i_meas);
        pmd_51_arrays.ileads_v_meas[i]     = subUtilitiesNtohf(self->ileads[i].v_meas);
        pmd_51_arrays.ileads_u_lead_neg[i] = subUtilitiesNtohf(self->ileads[i].u_lead_neg);
        pmd_51_arrays.ileads_u_lead_pos[i] = subUtilitiesNtohf(self->ileads[i].u_lead_pos);
    }

    // IEARTH

    long long iearth_start = static_cast<long long>(ntohl(self->iearth_last_time_sec))  * 1000 * 1000 * 1000
                           + static_cast<long long>(ntohl(self->iearth_last_time_usec)) * 1000;

    iearth_start -= (PM_FGC_51_IEARTH_LEN - 1) * PM_FGC_51_IEARTH_SAMPLING_NS;

    for(long long i=0; i != PM_FGC_51_IEARTH_LEN; i++)
    {
        pmd_51_arrays.iearth_timestamps[i] = iearth_start + i * PM_FGC_51_IEARTH_SAMPLING_NS;

        pmd_51_arrays.iearth_i_earth[i]     = subUtilitiesNtohf(self->iearth[i].i_earth);
    }

    // Add Signals

    pmdata.registerArray("EVENTS.TIMESTAMP",pmd_51_arrays.evt_timestamps,PM_FGC_51_EVENTS_LEN);

    pmdata.registerArray("EVENTS.PROPERTY",const_cast<const char **>(pmd_51_arrays.evt_property_ptr),PM_FGC_51_EVENTS_LEN);
    pmdata.addAttribute("EVENTS.PROPERTY","timescale","EVENTS");
    pmdata.addAttribute("EVENTS.PROPERTY","timestamps","EVENTS.TIMESTAMP",true);

    pmdata.registerArray("EVENTS.SYMBOL",const_cast<const char **>(pmd_51_arrays.evt_symbol_ptr),PM_FGC_51_EVENTS_LEN);
    pmdata.addAttribute("EVENTS.SYMBOL","timescale","EVENTS");
    pmdata.addAttribute("EVENTS.SYMBOL","timestamps","EVENTS.TIMESTAMP",true);

    pmdata.registerArray("EVENTS.ACTION",const_cast<const char **>(pmd_51_arrays.evt_action_ptr),PM_FGC_51_EVENTS_LEN);
    pmdata.addAttribute("EVENTS.ACTION","timescale","EVENTS");
    pmdata.addAttribute("EVENTS.ACTION","timestamps","EVENTS.TIMESTAMP",true);

    pmdata.registerArray("IAB.TIMESTAMP",pmd_51_arrays.iab_timestamps,PM_FGC_51_IAB_LEN);

    pmdata.registerArray("IAB.I_A",pmd_51_arrays.iab_i_a,PM_FGC_51_IAB_LEN);
    pmdata.addAttribute("IAB.I_A","timescale","IAB");
    pmdata.addAttribute("IAB.I_A","timestamps","IAB.TIMESTAMP",true);
    pmdata.addUnits("IAB.I_A","A");

    pmdata.registerArray("IAB.I_B",pmd_51_arrays.iab_i_b,PM_FGC_51_IAB_LEN);
    pmdata.addAttribute("IAB.I_B","timescale","IAB");
    pmdata.addAttribute("IAB.I_B","timestamps","IAB.TIMESTAMP",true);
    pmdata.addUnits("IAB.I_B","A");

    pmdata.registerArray("ILOOP.TIMESTAMP",pmd_51_arrays.ireg_timestamps,PM_FGC_51_IREG_LEN);

    pmdata.registerArray("ILOOP.I_REF",pmd_51_arrays.ireg_i_ref_rst,PM_FGC_51_IREG_LEN);
    pmdata.addAttribute("ILOOP.I_REF","timescale","ILOOP");
    pmdata.addAttribute("ILOOP.I_REF","timestamps","ILOOP.TIMESTAMP",true);
    pmdata.addUnits("ILOOP.I_REF","A");

    pmdata.registerArray("ILOOP.I_MEAS",pmd_51_arrays.ireg_i_meas,PM_FGC_51_IREG_LEN);
    pmdata.addAttribute("ILOOP.I_MEAS","timescale","ILOOP");
    pmdata.addAttribute("ILOOP.I_MEAS","timestamps","ILOOP.TIMESTAMP",true);
    pmdata.addUnits("ILOOP.I_MEAS","A");

    pmdata.registerArray("ILOOP.V_REF",pmd_51_arrays.ireg_v_ref,PM_FGC_51_IREG_LEN);
    pmdata.addAttribute("ILOOP.V_REF","timescale","ILOOP");
    pmdata.addAttribute("ILOOP.V_REF","timestamps","ILOOP.TIMESTAMP",true);
    pmdata.addUnits("ILOOP.V_REF","V");

    pmdata.registerArray("ILOOP.V_MEAS",pmd_51_arrays.ireg_v_meas,PM_FGC_51_IREG_LEN);
    pmdata.addAttribute("ILOOP.V_MEAS","timescale","ILOOP");
    pmdata.addAttribute("ILOOP.V_MEAS","timestamps","ILOOP.TIMESTAMP",true);
    pmdata.addUnits("ILOOP.V_MEAS","V");

    pmdata.registerArray("ILEADS.TIMESTAMP",pmd_51_arrays.ileads_timestamps,PM_FGC_51_ILEADS_LEN);

    pmdata.registerArray("ILEADS.I_MEAS",pmd_51_arrays.ileads_i_meas,PM_FGC_51_ILEADS_LEN);
    pmdata.addAttribute("ILEADS.I_MEAS","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.I_MEAS","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.I_MEAS","A");

    pmdata.registerArray("ILEADS.V_MEAS",pmd_51_arrays.ileads_v_meas,PM_FGC_51_ILEADS_LEN);
    pmdata.addAttribute("ILEADS.V_MEAS","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.V_MEAS","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.V_MEAS","V");

    pmdata.registerArray("ILEADS.U_LEAD_POS",pmd_51_arrays.ileads_u_lead_pos,PM_FGC_51_ILEADS_LEN);
    pmdata.addAttribute("ILEADS.U_LEAD_POS","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.U_LEAD_POS","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.U_LEAD_POS","V");

    pmdata.registerArray("ILEADS.U_LEAD_NEG",pmd_51_arrays.ileads_u_lead_neg,PM_FGC_51_ILEADS_LEN);
    pmdata.addAttribute("ILEADS.U_LEAD_NEG","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.U_LEAD_NEG","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.U_LEAD_NEG","V");

    pmdata.registerArray("IEARTH.TIMESTAMP",pmd_51_arrays.iearth_timestamps,PM_FGC_51_IEARTH_LEN);

    pmdata.registerArray("IEARTH.I_EARTH",pmd_51_arrays.iearth_i_earth,PM_FGC_51_IEARTH_LEN);
    pmdata.addAttribute("IEARTH.I_EARTH","timescale","IEARTH");
    pmdata.addAttribute("IEARTH.I_EARTH","timestamps","IEARTH.TIMESTAMP",true);
    pmdata.addUnits("IEARTH.I_EARTH","A");
}

void pmd_51GenerateAndSendExt(pm::Client &pmclient, struct FGCD_device *device, const struct PM_ext_buffer &buffer, int64_t timestamp)
{
    try
    {
        // Create PMData object

        PMData pmdata(PM_SYSTEM_NAME, "lhc_ext_pmd", device->name);

        // Add trigger and status data

        pmd_51AddStatus(pmdata, timestamp, &buffer.trigger, buffer.status, PM_EXT_BUF_LEN);

        // Send PMD Data

        logPrintf(device->id,
                  "PM Sending externally-triggered PM (system=%s, class=%s, source=%s, timestamp=%lld)...\n",
                  PM_SYSTEM_NAME, "lhc_self_pmd", device->name, timestamp);

        pmclient.send(pmdata, timestamp);
    }
    catch(PMError& e)
    {
       logPrintf(device->id, "PM PMD Generation and sending threw PMError: %s\n", e.what());
       fgcddev.status.st_faults |= FGC_FLT_PM;
    }
    catch(std::exception& e)
    {
        logPrintf(device->id, "PM PMD Generation and sending threw std::exception: %s\n", e.what());
        fgcddev.status.st_faults |= FGC_FLT_PM;
    }
}

void pmd_51GenerateAndSendSelf(pm::Client &pmclient, struct FGCD_device *device, const struct PM_self_buffer_51 &buffer, int64_t timestamp)
{
    try
    {
        // Create PMData object

        PMData pmdata(PM_SYSTEM_NAME, "lhc_self_pmd", device->name);

        // Add trigger and status data

        pmd_51AddStatus(pmdata,
                        timestamp,
                        &buffer.trigger,
                        buffer.status,
                        PM_SELF_BUF_LEN_51);

        // Add FGC_51 buffers

        pmd_51AddFgc(pmdata);

        // Send PMD Data

        logPrintf(device->id,
                  "PM Sending self-triggered PM (system=%s, class=%s, source=%s, timestamp=%lld)...\n",
                  PM_SYSTEM_NAME, "lhc_self_pmd", device->name, timestamp);

        pmclient.send(pmdata, timestamp);
    }
    catch(PMError& e)
    {
       logPrintf(device->id, "PM PMD Generation and sending threw PMError: %s\n", e.what());
       fgcddev.status.st_faults |= FGC_FLT_PM;
    }
    catch(std::exception& e)
    {
        logPrintf(device->id, "PM PMD Generation and sending threw std::exception: %s\n", e.what());
        fgcddev.status.st_faults |= FGC_FLT_PM;
    }
}

// EOF
