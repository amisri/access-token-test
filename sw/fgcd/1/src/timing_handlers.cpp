/*!
 * @file   timing_handlers.c
 * @author Stephen Page
 *
 * Functions for handling timing events
 */

#include <arpa/inet.h>
#include <exception>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fgc_event.h>
#include <fieldbus.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>

#include <timing.h>
#include <timing_handlers.h>


void timingAddMultiPpmEvent(const char *mppm_condition)
{
    ; // Multi-PPM not supported in this class
}


static void timingSetTelegramBits(const char* field_name,uint8_t bit_number, Timing::EventValue* evt_value, uint32_t *telegram_value, bool first_telegram)
{
    static Timing::Value value;

    try
    {
        evt_value->getFieldValue(field_name, &value);

        if(value.getAsBool())
        {
            *telegram_value |= (1 << bit_number);
        }
        else
        {
            *telegram_value &= ~(1 << bit_number);
        }
    }
    catch(std::exception& e)
    {
        if (first_telegram)
        {
            // Print to std::cerr because logger is not ready at this point (during timing initialization)
            std::cerr << "Failed to set telegram bit " << field_name << " : " << e.what() <<". Exiting" << std::endl;
            exit(1);
        }
    }
}

/*
 * Perform actions on telegram reception for cycling machines
 */

static void timingTelegramCyclingMachines(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    // USER (aka USER)

    try
    {
        evt_value->getFieldValue("USER", &value);

        uint32_t user = timingUserNumber(value.getAsString().c_str());
        if(!user) return;

        timing.field_value[TIMING_FIELD_USER] = user;

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field USER = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.field_value[TIMING_FIELD_USER]);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Set user

    timing.prev_user    = timing.user;
    timing.user         = timing.field_value[TIMING_FIELD_USER] & 0x000000FF;

    // BP_DURATION_MS

    try
    {
        evt_value->getFieldValue("BP_DURATION_MS", &value);

        timing.field_value[TIMING_FIELD_BP_DURATION_MS] = static_cast<uint32_t>(value.getAsLong32());
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // CYCLE_DURATION_MS (aka CYCLELEN)

    try
    {
        evt_value->getFieldValue("CYCLE_DURATION_MS", &value);
        timing.field_value[TIMING_FIELD_CYCLE_LEN] = static_cast<uint32_t>(value.getAsLong32()/timing.field_value[TIMING_FIELD_BP_DURATION_MS]);


        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field CYCLE_DURATION_MS = %u ms, CYCLE_LEN = %u bps, BP_DURATION_MS = %u ms\n",
                evt_value->getName().c_str(),
                static_cast<uint32_t>(value.getAsLong32()),
                timing.field_value[TIMING_FIELD_CYCLE_LEN],
                timing.field_value[TIMING_FIELD_BP_DURATION_MS]);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

     // CYCLE_TAG (aka CYCLETAG)

    try
    {
        evt_value->getFieldValue("CYCLE_TAG", &value);
        timing.field_value[TIMING_FIELD_CYCLE_TAG] = static_cast<uint32_t>(value.getAsLong32());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field CYCLE_TAG = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_CYCLE_TAG]);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // CYCLE_NB (aka CYCLENUM)

    try
    {
        evt_value->getFieldValue("CYCLE_NB", &value);
        timing.field_value[TIMING_FIELD_CYCLE_NUM] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field CYCLE_NB = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_CYCLE_NUM]);
        }

        // Set cycle number

        timing.cycle_num = timing.field_value[TIMING_FIELD_CYCLE_NUM] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Set cycle number by default

    timing.cycle_num = timing.field_value[TIMING_FIELD_CYCLE_NUM] & 0x000000FF;

    // FGC EVENTS

    uint32_t            basic_period = ((evt_value->getCycleTime() / timing.field_value[TIMING_FIELD_BP_DURATION_MS]) %
                                        timing.field_value[TIMING_FIELD_CYCLE_LEN]) + 1;
    struct fgc_event    fgc_event;

    // Take actions in the first basic period of the cycle

    if(basic_period == 1)
    {
        // Send the cycle tag fieldbus event

        fgc_event.type          = FGC_EVT_CYCLE_TAG;
        fgc_event.trigger_type  = 0;
        fgc_event.payload       = timing.field_value[TIMING_FIELD_CYCLE_TAG];
        fgc_event.delay_us      = FGC_EVT_MIN_CYCLES * FGCD_CYCLE_PERIOD_MS * 1000;
        fieldbusSendEvent(&fgc_event);
    }
}

/*
 * Handle telegram values for LHC
 */

static void timingTelegramLHC(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static bool first_telegram = true;
    uint16_t sector_mask = fgcd.device[0].omode_mask & 0x00FF;

    // PP60A (aka PC_PERMIT)

    timingSetTelegramBits("PP60A_ARC12",0, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC23",1, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC34",2, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC45",3, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC56",4, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC67",5, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC78",6, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC81",7, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field PP60A = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_PC_PERMIT]);
    }

    // Set PC_PERMIT

    if((timing.field_value[TIMING_FIELD_PC_PERMIT] & sector_mask) == sector_mask)
    {
      if(!fieldbus.pc_permit)
      {
           logPrintf(0, "TIMING PP60A (PC_PERMIT) set\n");
           fieldbus.pc_permit = 1;
      }
    }
    else if(fieldbus.pc_permit)
    {
      logPrintf(0, "TIMING PP60A (PC_PERMIT) cleared\n");
      fieldbus.pc_permit = 0;
    }

    // OMODE (aka OPMODE)

    timingSetTelegramBits("OMODE_BEAMOP", 8, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);
    timingSetTelegramBits("OMODE_CMS",    9, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);
    timingSetTelegramBits("OMODE_ATLAS",  10, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field OMODE = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_OP_MODE]);
    }

    // Set operational mode only if succeeded

    uint16_t op_mode = static_cast<uint16_t>(timing.field_value[TIMING_FIELD_OP_MODE] & 0x0000FFFF);

    if(fgcd.op_mode != op_mode)
    {
        if (first_telegram)
        {
            std::cout << "TIMING Operational mode mask changed from " << fgcd.op_mode << " to " << op_mode << std::endl;
        }

        logPrintf(0, "TIMING Operational mode mask changed from 0x%04hX to 0x%04hX\n", fgcd.op_mode, op_mode);
        fgcd.op_mode = op_mode;
    }

    // SECTACC (aka SECTORACCESS)

    timingSetTelegramBits("SECTACC_SEC12", 0, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC23", 1, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC34", 2, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC45", 3, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC56", 4, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC67", 5, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC78", 6, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC81", 7, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field SECTACC = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_SECTOR_ACCESS]);
    }

    // Set fieldbus SECTOR_ACCESS bit

    if(timing.field_value[TIMING_FIELD_SECTOR_ACCESS] & sector_mask)
    {
        if(!fieldbus.sector_access)
        {
           logPrintf(0, "TIMING SECTACC (SECTOR_ACCESS) set\n");
           fieldbus.sector_access = 1;
        }
    }
    else if(fieldbus.sector_access)
    {
      logPrintf(0, "TIMING SECTACC (SECTOR_ACCESS) cleared\n");
      fieldbus.sector_access = 0;
    }

    first_telegram = false;
}

void timingTelegramReady(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Machine specific handling

    switch(fgcd.machine)
    {
        case MACHINE_ADE:
        case MACHINE_CPS:
        case MACHINE_LN4:
        case MACHINE_LNA:
        case MACHINE_PSB:
        case MACHINE_SPS:
            timingTelegramCyclingMachines(handler, info, evt_time, evt_value);
            break;

        case MACHINE_LHC:
            timingTelegramLHC(handler, info, evt_time, evt_value);
            break;

        default:
            break;
    }


}

void timingPostMortem(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    struct fgc_event fgc_event;

    fieldbus.global_pm_time_sec     = evt_time.time.tv_sec;
    fieldbus.global_pm_time_nsec    = evt_time.time.tv_nsec;

    fgc_event.type          = FGC_EVT_PM;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = 0;
    fgc_event.delay_us      = FGC_EVT_MIN_CYCLES * FGCD_CYCLE_PERIOD_MS * 1000;
    fieldbusSendEvent(&fgc_event);
}

void timingAbortRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    struct fgc_event     fgc_event;
    struct timeval       relative_time;
    static Timing::Value value;

    // EVENT_GROUP

    try
    {
        evt_value->getFieldValue("EVENT_GROUP",&value);
        fgc_event.payload = static_cast<uint16_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field EVENT_GROUP = %u\n",
                      evt_value->getName().c_str(),
                      fgc_event.payload);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    fgc_event.type          = FGC_EVT_ABORT;
    fgc_event.trigger_type  = 0;
    fgc_event.delay_us      = 1000 * (info.delay_ms - ((relative_time.tv_sec  * 1000) +
                                                           (relative_time.tv_usec / 1000)));
    fieldbusSendEvent(&fgc_event);
}

void timingStartRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    struct fgc_event     fgc_event;
    struct timeval       relative_time;
    static Timing::Value value;

    // EVENT_GROUP

    try
    {
        evt_value->getFieldValue("EVENT_GROUP",&value);
        fgc_event.payload = static_cast<uint16_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field EVENT_GROUP = %u\n",
                      evt_value->getEventDesc()->getName().c_str(),
                      fgc_event.payload);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    fgc_event.type          = FGC_EVT_START;
    fgc_event.trigger_type  = FGC_EVT_START;
    fgc_event.delay_us      = 1000 * (info.delay_ms - ((relative_time.tv_sec  * 1000) +
                                                           (relative_time.tv_usec / 1000)));
    fieldbusSendEvent(&fgc_event);
}

void timingSSC(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    struct fgc_event fgc_event;

    fgc_event.type          = FGC_EVT_SSC;
    fgc_event.trigger_type  = 0;
    fgc_event.payload   = 0;
    fgc_event.delay_us  = FGC_EVT_MIN_CYCLES * FGCD_CYCLE_PERIOD_MS * 1000;
    fieldbusSendEvent(&fgc_event);
}

static void timingGenericWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value,uint8_t warning_event_type)
{
    static Timing::Value value;
    struct fgc_event     fgc_event;
    struct timeval       relative_time;

    // Retrieve next USER

    try
    {
        evt_value->getFieldValue("USER", &value);

        uint32_t next_user = timingUserNumber(value.getAsString().c_str());
        if(!next_user) return;

        timing.field_value[TIMING_FIELD_NEXT_USER] = next_user;

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field USER = %s (%u)\n",
                      evt_value->getName().c_str(),
                      value.getAsString().c_str(),
                      timing.field_value[TIMING_FIELD_NEXT_USER]);
        }

        timing.next_user = timing.field_value[TIMING_FIELD_NEXT_USER] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Send fieldbus events

    // Get the time relative to the start of the next fieldbus cycle

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    fgc_event.delay_us = 1000 * (info.delay_ms - ((relative_time.tv_sec  * 1000) +
                                                      (relative_time.tv_usec / 1000)));

    // Send the event

    fgc_event.type          = warning_event_type;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = timing.field_value[TIMING_FIELD_NEXT_USER];

    fieldbusSendEvent(&fgc_event);

}

void timingCycleWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    timingGenericWarning(handler, info, evt_time, evt_value, FGC_EVT_CYCLE_START);
}

void timingStartRef1Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarning(handler, info, evt_time, evt_value, FGC_EVT_START_REF_1);
}

void timingStartRef2Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarning(handler, info, evt_time, evt_value, FGC_EVT_START_REF_2);
}

void timingStartRef3Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarning(handler, info, evt_time, evt_value, FGC_EVT_START_REF_3);
}

void timingStartRef4Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarning(handler, info, evt_time, evt_value, FGC_EVT_START_REF_4);
}

void timingStartRef5Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarning(handler, info, evt_time, evt_value, FGC_EVT_START_REF_5);
}

struct Timing_event_handler timing_event_handlers[]
= {
    { timing_handler_names[TIMING_HANDLER_ABORTREF],            &timingAbortRef             },
    { timing_handler_names[TIMING_HANDLER_CYCLEWARNING],        &timingCycleWarning         },
    { timing_handler_names[TIMING_HANDLER_POSTMORTEM],          &timingPostMortem           },
    { timing_handler_names[TIMING_HANDLER_SSC],                 &timingSSC                  },
    { timing_handler_names[TIMING_HANDLER_STARTREF1WARNING],    &timingStartRef1Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF2WARNING],    &timingStartRef2Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF3WARNING],    &timingStartRef3Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF4WARNING],    &timingStartRef4Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF5WARNING],    &timingStartRef5Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF],            &timingStartRef             },
    { timing_handler_names[TIMING_HANDLER_TELEGRAM],            &timingTelegramReady        },
    { "",                                                       NULL                        }
};

// EOF
