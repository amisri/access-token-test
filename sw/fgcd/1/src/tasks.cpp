/*!
 * @file   tasks.cpp
 * @brief  Define tasks to be started and stopped
 * @author Stephen Page
 */

#include <alarms.h>
#include <cmdqmgr.h>
#include <consts.h>

#include <cmw.h>
#include <cmwpub.h>
#include <cmwutil.h>

#include <devname.h>
#include <fip.h>
#include <fipba.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <nonvol.h>
#include <parser.h>
#include <pm.h>
#include <pub.h>
#include <rbac.h>
#include <rt.h>
#include <taskmgr.h>
#include <tcp.h>
#include <timing.h>
#include <watchdog.h>

Task tasks[]
=
{
    // label                        start_func             fatal   flag

    { "RBAC",                       &rbacInit,              1,      &fgcd.enable_rbac   },
    { "parser",                     &parserInit,            1,      NULL                },
    { "command queue manager",      &cmdqmgrInit,           1,      NULL                },
    { "device naming",              &devnameInit,           1,      NULL                },
    { "timing",                     &timingInit,            1,      NULL                },
    { "FGCD device",                &fgcddevStart,          1,      NULL                },
    { "logging",                    &logStart,              1,      NULL                },
    { "CMW utilities",              &cmwutilInit,           1,      NULL                },
    { "CMW publication",            &cmwpubStart,           1,      NULL                },
    { "FIP interface",              &fipStartInterface,     1,      NULL                },
    { "FIP bus arbitrator",         &fipbaStart,            1,      NULL                },
    { "timing events",              &timingSubscribeEvents, 1,      NULL                },
    { "real-time data reception",   &rtStart,               1,      NULL                },
    { "CMW server",                 &cmwStart,              1,      NULL                },
    { "alarm surveillance",         &alarmsStart,           1,      NULL                },
    { "post-mortem",                &pmStart,               1,      &fgcd.enable_pm     },
    { "publishing",                 &pubStart,              1,      NULL                },
    { "TCP server",                 &tcpStart,              1,      NULL                },
    { "watchdog",                   &watchdogStart,         1,      NULL                },
    { "",                           NULL,                   0,      NULL                },
};

// EOF
