/*!
 * @file   watchdog.c
 * @author Stephen Page
 *
 * Functions for the watchdog thread
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include <consts.h>
#include <fieldbus.h>
#include <fip.h>
#include <fipdiamon.h>
#include <logging.h>
#include <watchdog.h>
#include <fgcd_thread.h>

// FIP missed interrupts

extern unsigned int fip_int_missed;

// Global variables

struct Watchdog watchdog = {};



// Static functions

static FGCD_thread_func watchdogRun;



int32_t watchdogStart(void)
{
    // Initialise Diamon WorldFIP data

    fipdiamonInit();

    // Start watchdog thread

    return fgcd_thread_create(&watchdog.thread, watchdogRun, NULL, WATCHDOG_THREAD_POLICY, WATCHDOG_THREAD_PRIORITY, NO_AFFINITY);
}



/*
 * Watchdog thread
 */

static void *watchdogRun(void *unused)
{
    while(1)
    {
        // Reset WorldFIP cycle toggle

        if(watchdog.fip_toggle != WATCHDOG_DISABLE)
        {
            watchdog.fip_toggle = 0;
        }

        // Wait

        sleep(WATCHDOG_PERIOD);

        // Check WorldFIP error status and toggle

        if(watchdog.fip_toggle != WATCHDOG_DISABLE && !watchdog.fip_toggle)
        {
            // Restart WorldFIP interface

            logPrintf(0, "WATCHDOG Restarting WorldFIP interface\n");
            fipRestartInterface();
        }

        // Update Diamon WorldFIP data

        fipdiamonUpdate();
    }

    return 0;
}

// EOF
