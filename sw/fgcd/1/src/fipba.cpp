/*!
 * @file   fipba.c
 * @author Stephen Page
 *
 * Functions for the WorldFIP bus arbitrator
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include <defconst.h>
#include <fgc_errs.h>
#include <fip.h>
#include <fipba.h>
#include <pub.h>
#include <watchdog.h>
#include <fgcd_thread.h>



// Global variables

struct FIPba fipba;



// Static functions

static int32_t fipbaInit(void);



int32_t fipbaStart(void)
{
    // Check whether BA is already running

    if(fipba.run) return 0;

    // Increment start count

    fipba.start_count++;

    // Initialise configuration

    if(fipbaInit() != 0) return 1;

    // Start the BA
    fipba.run = 1;

    if (mstrfip_ba_load(fip.dev, fip.ba.mcycle) < 0) 
    {
		fprintf(stderr, "ERROR: Failed to load macrocycle.\n");
		fipba.run = 0;
		mstrfip_ba_reset(fip.dev);
		return 1;
    }

    if (mstrfip_ba_start(fip.dev) < 0) 
    {
		fprintf(stderr, "ERROR: Failed to start BA\n");
		fipba.run = 0;
		mstrfip_ba_reset(fip.dev);
		return 1;
    }

    return 0;
}



void fipbaStop(void)
{
    struct timespec time;

    // Check whether the bus arbitrator is already stopped

    if(!fipba.run) return;
    fipba.run = 0;

    // Stop the bus arbitrator

    mstrfip_ba_reset(fip.dev);

    // Close device
    
    mstrfip_close(fip.dev);

    // A delay is necessary before shutting down the WorldFIP interface

    time.tv_sec  = 0;
    time.tv_nsec = 1000000; // 1ms

    while(nanosleep(&time, &time) && errno == EINTR);
}

// Static functions



/*
 * Initialise variables
 */

static int32_t fipbaInit(void)
{
    static uint32_t first_time  = 1;
    uint32_t idx, j, res;
    struct mstrfip_per_var_wind_cfg pwind_cfg;
    struct mstrfip_aper_msg_wind_cfg apmsgwind_cfg;
    struct mstrfip_data **varlist;
    int nvar;

    fipba.run = 0;

    // Initialise mutex first time only

    if(first_time)
    {
        first_time = 0;

        if(fgcd_mutex_init(&fipba.mutex, FIP_MUTEX_POLICY) != 0) return 1;
    }

    // Program the macrocycle: append periodic or aperiodic window in the
    // order we want them to be scheduled.
    
    // 1) Append periodic window: in order to schedule a bunch op periodic
    // variables which are : Time var + FIP_NUM_EXTRA_VARS + code var +
    //				 FGCD_MAX_DEVS status vars + diag var + end var
    //  build the varlist sorted this way, and append periodic window

    nvar = 1 /*time var */ + FIP_NUM_EXTRA_VARS + 1 /* code var */ +
		   FGCD_MAX_EQP_DEVS /* status var */ + 1 /* diag var */ + 1 /* end var */;
    varlist = (struct mstrfip_data **)calloc(nvar, sizeof(struct mstrfip_data *));
    idx = 0;
    varlist[idx++] = fip.ba.time_var;                 // time var
    for (j = 0; j < FIP_NUM_EXTRA_VARS ; ++j)
    {
		varlist[idx++] = fip.ba.extra_var[j];     // extra vars
    }

    varlist[idx++] = fip.ba.code_var;                 // code var
    for (j = 0; j < FGCD_MAX_EQP_DEVS; ++j)
    {
		varlist[idx++] = fip.ba.status_vars[j];   // status vars
    }
    varlist[idx++] = fip.ba.diagnostic_dev_var;			 // diag var
    varlist[idx++] = fip.ba.per_end_var;		 // end var

    pwind_cfg.varlist = varlist;
    pwind_cfg.var_count = nvar; // number of per vars in the list
    res = mstrfip_per_var_wind_append(fip.ba.mcycle, &pwind_cfg);
    free(varlist); // free temporary array
    if (res < 0)
    {
		fprintf(stderr, "ERROR: Appending first periodic window failed: %s\n",
            mstrfip_strerror(res));
        return 1;
    }

    // 2) Append silent wait window

    if ((res = mstrfip_wait_wind_append(fip.ba.mcycle, 1, 11000)) < 0)
    {
        fprintf(stderr, "ERROR: Appending first wait window failed: %s\n",
            mstrfip_strerror(res));
		return 1;
    }
    // 3) Append aperiodic msg window which last till 18000

    apmsgwind_cfg.end_ustime = 18000;
    apmsgwind_cfg.prod_msg_fifo_size = FGCD_MAX_DEVS;
    apmsgwind_cfg.cons_msg_fifo_size = FGCD_MAX_DEVS;
    apmsgwind_cfg.mstrfip_cons_msg_handler = NULL;
    apmsgwind_cfg.mstrfip_prod_msg_handler = NULL;
    if ((res = mstrfip_aper_msg_wind_append(fip.ba.mcycle, &apmsgwind_cfg)) < 0)
    {
        fprintf(stderr, "ERROR: Appending aperiodic message window failed: %s\n",
            mstrfip_strerror(res));
		return 1;
    }

    // 4) Append silent wait window

    if ((res = mstrfip_wait_wind_append(fip.ba.mcycle, 1, 19000)) < 0)
    {
        fprintf(stderr, "ERROR: Appending second wait window failed: %s\n",
            mstrfip_strerror(res));
		return 1;
    }

    // 5) Append periodic window: in order to schedule 2 periodic vars
    //  realtime1 var + realtime2 var
    //  build the varlist arranged in this order and append periodic window

    nvar = 1 /* realtime1 */ + 1 /* realtime2 */;
    varlist = (struct mstrfip_data **)calloc(nvar, sizeof(struct mstrfip_data *));
    idx = 0;
    varlist[idx++] = fip.ba.real_var1;
    varlist[idx++] = fip.ba.real_var2;
    pwind_cfg.varlist = varlist;
    pwind_cfg.var_count = nvar; // number of per vars in the list
    res = mstrfip_per_var_wind_append(fip.ba.mcycle, &pwind_cfg);
    free(varlist);
    if (res < 0)
    {
        fprintf(stderr, "ERROR: Appending second periodic window failed: %s\n",
            mstrfip_strerror(res));
		return 1;
    }

    // 6)append silent wait window

    if ((res = mstrfip_wait_wind_append(fip.ba.mcycle, 1, 20000)) < 0)
    {
        fprintf(stderr, "ERROR: Appending third silent window failed: %s\n",
            mstrfip_strerror(res));
		return 1;
    }
    
    return 0;
}

// EOF
