/*!
 * @file   fipdiamon.c
 * @author Stephen Page
 *
 * Functions for reporting WorldFIP status to Diamon
 */

#include <stdio.h>
#include <string.h>
#include <sys/shm.h>

#include <fgcd.h>
#include <fgcddev.h>
#include <fieldbus.h>
#include <fip.h>
#include <fipdiamon.h>
#include <timing.h>



// Struct containing global variables

struct FIPdiamon
{
    struct mstrfip_diag_shm *data;
};



// Global variables

struct FIPdiamon fipdiamon;



int fipdiamonInit(void)
{
    int shm_id;

    // Get shared memory ID

    shm_id = shmget(FIPDIAG_SHM_KEY, sizeof(*fipdiamon.data), 0666 | IPC_CREAT);

    if(shm_id == -1)
    {
        perror("ERROR: shmget");
        return 1;
    }

    // Attach to shared memory

    fipdiamon.data = reinterpret_cast<struct mstrfip_diag_shm *>
    		     (shmat(shm_id, NULL, 0));

    if(fipdiamon.data == (void *)-1)
    {
        perror("ERROR: shmat");
        return 1;
    }

    // Set constant values within shared memory

    fipdiamon.data->start_time 		          = fip.fieldbus_stats.start_time;
    fipdiamon.data->present_theoric_list[0]   = 0x01; 	                       // Gateway device
    fipdiamon.data->present_list[0]           = 0x01;	                       // Gateway device is always online
    fipdiamon.data->present_theoric_list[15]  = 0x80;	                       // Diagnostic device

    return 0;
}

void fipdiamonUpdate(void)
{
    uint32_t        i;
    uint32_t        list_idx;
    uint8_t         mask;
    uint16_t        status = MSTRFIP_STATUS_DIAG_OK;
    struct timeval  time;

    // Do nothing if shared memory not mapped

    if(!fipdiamon.data) return;

    // Set cycle time

    timingGetUserTime(0, &time);
    fipdiamon.data->last_cycle_date = time.tv_sec;

    // Handle per device data

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        mask        = 1 << (i % 8); // Mask within presence element
        list_idx    = i / 8;        // Presence element index

        // Check whether device has a configured name

        if(fgcd.device[i].name)
        {
            // Check whether the device is operational

            if(fgcd.device[i].omode_mask)
            {
		        fipdiamon.data->present_theoric_list[list_idx] |= mask;
            }
            else
            {
                fipdiamon.data->present_theoric_list[list_idx] &= ~mask;
            }
        }
        else // No FGC configured with address i
        {
	        fipdiamon.data->present_theoric_list[list_idx] &= ~mask;
        }


        // Check whether device is online

        if(fgcd.device[i].online)
        {
    	    fipdiamon.data->present_list[list_idx] |= mask;
    	    fipdiamon.data->absent_list[list_idx] &= ~mask;
        }
        else // No device online with address i
        {
	        fipdiamon.data->present_list[list_idx] &= ~mask;

            // Set status to indicate missing agent if device is configured

            if(fgcd.device[i].name)
            {
                fipdiamon.data->absent_list[list_idx] |= mask;
                status = MSTRFIP_STATUS_AGT_MISS;
            }
            else // No FGC configured with address i
            {
	    	    fipdiamon.data->absent_list[list_idx] &= ~mask;
            }
        }
    }

    // Check whether diagnostic device is present

    if(fgcddev.status.fgc_mask & (1U << (FGCD_MAX_EQP_DEVS + 1))) // Diagnostic device present
    {
        fipdiamon.data->present_list[15]    |=  0x80;
        fipdiamon.data->absent_list[15]     &= ~0x80;
    }
    else // Diagnostic device not present
    {
        fipdiamon.data->present_list[15]    &= ~0x80;
        fipdiamon.data->absent_list[15]     |=  0x80;

        status = status == MSTRFIP_STATUS_AGT_MISS ? MSTRFIP_STATUS_DIAG_FAULT1 : MSTRFIP_STATUS_DIAG_HW_ERR;
    }

    // Check whether cycles have occurred since last update

    if(fipdiamon.data->cycle_count == fieldbus.stats.cycle_count)
    {
        // No cycles have occurred

        status = MSTRFIP_STATUS_DIAG_FAULT2;
    }
    
    fipdiamon.data->cycle_count = fieldbus.stats.cycle_count;

    // Set status summary

    fipdiamon.data->com_status = status;
}

// EOF
