/*!
 * @file   fip.c
 * @author Stephen Page
 *
 * Functions for managing the WorldFIP bus
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <charq.h>
#include <cmd.h>
#include <cmdqmgr.h>
#include <cmwpub.h>
#include <defconst.h>
#include <fgc_code.h>
#include <fgc_errs.h>
#include <fgc_event.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <fgcsm.h>
#include <fieldbus.h>
#include <fip.h>
#include <fipba.h>
#include <logging.h>
#include <pub.h>
#include <queue.h>
#include <rt.h>
#include <tcp.h>
#include <timing.h>
#include <watchdog.h>
#include <fgcd_thread.h>

// Global variables

struct Fip fip;

// Static functions

static int32_t fipInit(void);
static int32_t fipSendAperiodicMsg(uint32_t channel_num, uint32_t length);
static void fipSetTime(void);
static void fipTimeVarSent(struct mstrfip_dev *dev, struct mstrfip_data *data,
                    struct mstrfip_irq *irq);
static void fipDiagDevReceived(struct mstrfip_dev *dev, struct mstrfip_data *data,
                    struct mstrfip_irq *irq);
static void fipStatusVarReceived(struct mstrfip_dev *dev, struct mstrfip_data *data,
                    struct mstrfip_irq *irq);
static void fipPerEndVarSent(struct mstrfip_dev *dev, struct mstrfip_data *data,
                    struct mstrfip_irq *irq);
static void fipAperiodicMsgSent(struct mstrfip_dev *dev, struct mstrfip_data *data,
                    struct mstrfip_irq *irq);
static void fipAperiodicMsgRecv(struct mstrfip_dev *dev, struct mstrfip_data *data,
                    struct mstrfip_irq *irq);
static void fipInterfaceFatalError(struct mstrfip_dev *dev,
                    enum mstrfip_error_list error);
//static void fipInterfaceWarning(FDM_REF * fdm_ref, FDM_ERROR_CODE error);

static int32_t fipReadBusSpeed()
{
    int res;
    enum mstrfip_bitrate speed;
    char *speed_str = NULL;

    res = mstrfip_hw_speed_get(fip.dev, &speed);
    if (res)
    {
                fprintf(stderr, "ERROR: Can't get FIP speed: %s. Exit\n",
            mstrfip_strerror(errno));
        return res;
    }
    if (speed != FIP_BUS_SPEED)
    {
        switch (speed)
        {
        case MSTRFIP_BITRATE_31:
            speed_str = (char *)"31.25 kb/s";
            break;
        case MSTRFIP_BITRATE_1000:
            speed_str = (char *)"1 Mb/s";
            break;
        case MSTRFIP_BITRATE_2500:
            speed_str = (char *)"2.5 Mb/s";
            break;
        case MSTRFIP_BITRATE_UNDEFINED:
            speed_str = (char *)"Undefined speed";
            break;
        }

        fprintf(stderr, "ERROR: Expected FIP bus speed 2.5 Mb/s and got %s\n",
                speed_str);
        return 1;
    }
    return 0;
}

static int32_t fipOpenDevice()
{
    int res;

    res = mstrfip_init();
    if (res)
    {
                fprintf(stderr, "ERROR: Cannot init fip library: %s\n",
                mstrfip_strerror(errno));
        return res;
    }

    fip.dev = mstrfip_open_by_lun(FIP_MEZZANINE_NR);
    if(fip.dev == NULL)
    {
                fprintf(stderr, "ERROR: Cannot open masterfip dev: %s\n",
                mstrfip_strerror(errno));
        return res;
    }

    /* reset mockturtle RT app */
    if (mstrfip_rtapp_reset(fip.dev) < 0)
    {
        fprintf(stderr, "ERROR: Cannot reset rt app: %s\n",
                mstrfip_strerror(errno));
        return res;
    }
    return 0;
}

static int32_t fipCreatePeriodicVar()
{
    struct mstrfip_data_cfg var_cfg;
    int i;

    // Time variable
    var_cfg.max_bsz = sizeof(struct fgc_fieldbus_time);
    var_cfg.id = FIP_TIME_VAR_ID;
    // produced var with callback
    var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD;
    var_cfg.mstrfip_data_handler = fipTimeVarSent;
    fip.ba.time_var = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
    if (fip.ba.time_var == NULL)
    {
        fprintf(stderr, "ERROR: Time var creation failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }
    fip.time_var_value = (struct fgc_fieldbus_time *)fip.ba.time_var->buffer;

    // Code variable
    var_cfg.max_bsz = FGC_FIELDBUS_CODE_BLOCKS_PER_MSG * FGC_CODE_BLK_SIZE;
    var_cfg.id = FIP_CODE_VAR_ID;
    var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD; // produced var
    var_cfg.mstrfip_data_handler = NULL; // no callbcak
    fip.ba.code_var = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
    if (fip.ba.code_var == NULL)
    {
        fprintf(stderr, "ERROR: code var creation failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }
    fip.code_var_value =
        (uint8_t (*)[FGC_CODE_BLK_SIZE])fip.ba.code_var->buffer;

    // Real-time variable 1
    var_cfg.max_bsz = sizeof (struct fgc_fip_rt_data);
    var_cfg.id = FIP_REAL_TIME_VAR1_ID;
    var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD; // produced var
    var_cfg.mstrfip_data_handler = NULL; // no callbcak
    fip.ba.real_var1 = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
    if (fip.ba.real_var1 == NULL)
    {
        fprintf(stderr, "ERROR: realtime1 var creation failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }

    // Real-time variable 2
    var_cfg.max_bsz = sizeof (struct fgc_fip_rt_data);
    var_cfg.id = FIP_REAL_TIME_VAR2_ID;
    var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD; // produced var
    var_cfg.mstrfip_data_handler = NULL; // no callbcak
    fip.ba.real_var2 = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
    if (fip.ba.real_var2 == NULL)
    {
        fprintf(stderr, "ERROR: realtime2 var creation failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }

    // Periodic end variable
    var_cfg.max_bsz = sizeof (uint16_t);
    var_cfg.id = FIP_PER_END_VAR_ID;
    // produced var with callback
    var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD;
    var_cfg.mstrfip_data_handler = fipPerEndVarSent;
    fip.ba.per_end_var = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
    if (fip.ba.per_end_var == NULL)
    {
        fprintf(stderr, "ERROR: PerEnd var creation failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }

    // Extra message variables :
    // TODO: the purpose of those extra variables were to carry request
    // message flag to allow all fgc devices to send a message in the
    // aperiodic window. This is not anymore necessary and should be
    // removed
    for (i = 0; i < FIP_NUM_EXTRA_VARS; i++)
    {
        var_cfg.max_bsz = sizeof (uint16_t);
        var_cfg.id = FIP_EXTRA_BASE_ID + i;
        var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD; // produced var
        var_cfg.mstrfip_data_handler = NULL; // no callbcak
        fip.ba.extra_var[i] = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
        if (fip.ba.extra_var[i] == NULL)
        {
            fprintf(stderr, "ERROR: extra var_%i creation failed: %s\n",
                i, mstrfip_strerror(errno));
            return 1;
        }
    }

    // Status variables for each node
    for (i = 0; i < FGCD_MAX_EQP_DEVS; i++) {
        var_cfg.max_bsz = sizeof(struct fgc_stat);
        var_cfg.id = FIP_STATUS_BASE_ID + i;
        // consumed var with callback
        var_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
        var_cfg.mstrfip_data_handler = fipStatusVarReceived;
        fip.ba.status_vars[i] = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
        if (fip.ba.status_vars[i] == NULL)
        {
            fprintf(stderr, "ERROR: status var_%i creation failed: %s\n",
                i, mstrfip_strerror(errno));
            return 1;
        }
    }

    // Variable for diagnostic device
    var_cfg.max_bsz = sizeof(uint16_t);
    var_cfg.id = FIP_DIAG_ID;
    // consumed var with callback
    var_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
    var_cfg.mstrfip_data_handler = fipDiagDevReceived;
    fip.ba.diagnostic_dev_var = mstrfip_var_create(fip.ba.mcycle, &var_cfg);
    if (fip.ba.diagnostic_dev_var == NULL)
    {
        fprintf(stderr, "ERROR: diag var creation failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }
    fip.diagnostic_dev_value = (uint16_t *)fip.ba.diagnostic_dev_var->buffer;

    return 0;
}

static int32_t fipCreateAperiodicMsg()
{
    struct mstrfip_data_cfg msg_cfg;
    int i;

    // Aperiodic message for each node: potentially each fgc node can
    // receive and send aperiodic message
    for (i = 0; i <= FGCD_MAX_EQP_DEVS; i++)
    {
        msg_cfg.id = i; /* remote agent address */

        // The aperiodic message for channel 0 (Master of the FIP bus) is used to power cycle the FGC
        // This message sends FIP_MSG_LONG_LENGTH bytes with zeros
        // This mesage is triggered by the FIELDBUS.POWER_SIGNAL counter
        if(i != 0)
        {
            msg_cfg.max_bsz = sizeof(struct fgc_fieldbus_cmd);
        }
        else
        {
            msg_cfg.max_bsz = FIP_MSG_LONG_LENGTH;
        }

        // produced aperiodic message with callback on sending
        msg_cfg.flags = MSTRFIP_DATA_FLAGS_PROD;
        msg_cfg.mstrfip_data_handler = fipAperiodicMsgSent;
        fip.ba.tx_msg[i] = mstrfip_msg_create(fip.ba.mcycle, &msg_cfg);
        if (fip.ba.tx_msg[i] == NULL)
        {
            fprintf(stderr, "ERROR: TX msg_%i creation failed: %s\n",
                i, mstrfip_strerror(errno));
            return 1;
        }

        // map command_message to fip msg buffer

        if(i != 0)
        {
            fip.channel[i].command_message = (struct fgc_fieldbus_cmd *)fip.ba.tx_msg[i]->buffer;
        }
        else
        {
            // The long aperiodic message buffer is assumed to be zeroed by the MFIP library.
            // This value is the one required for the power cycle signal, and we do not need to do anything else
        }

        // consumed aperiodic message with callback on receiving
        msg_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
        msg_cfg.mstrfip_data_handler = fipAperiodicMsgRecv;
        fip.ba.rx_msg[i] = mstrfip_msg_create(fip.ba.mcycle, &msg_cfg);
        if (fip.ba.rx_msg[i] == NULL)
        {
            fprintf(stderr, "ERROR: RX msg_%i creation failed: %s\n",
                i, mstrfip_strerror(errno));
            return 1;
        }
    }

    return 0;
}

/*
 * Initialise variables
 */

static int32_t fipInit(void)
{
    uint32_t        i           = 0;
    struct mstrfip_hw_cfg hw_cfg;
    struct mstrfip_sw_cfg sw_cfg;

    // Set WorldFIP starting flags

    fip.run = 0;
    fip.starting_flag = 1;

    // Initialise the fieldbus

    if(fieldbusInit())
    {
        return 1;
    }

    // Initialise FGC state machine

    fgcsmInit();

    // Initialise channels

    for(i = 0 ; i < FGCD_MAX_DEVS ; i++)
    {
        fip.channel[i].device                                = &fgcd.device[i];
        fip.channel[i].aper_msg_rec_count                    = 0;
        fip.channel[i].prev_status_seq                       = -1;
        fip.channel[i].fip_stats.promptness_fail_count       = 0;
        fip.channel[i].fip_stats.nonsignificance_fault_count = 0;
        fip.channel[i].fip_stats.significance_fault_count    = 0;
        fip.channel[i].fip_stats.user_error_count            = 0;
    }

    // Open masterfip device
    if (fipOpenDevice()) {
        return 1;
    }

    // Check the FIP bus speed: 2.5MB/s is expected
    if (fipReadBusSpeed()) {
        return 1;
    }

    // Software configuration
    sw_cfg.irq_thread_prio = FIP_FDM_THREAD_PRIORITY;
    sw_cfg.mstrfip_error_handler = fipInterfaceFatalError;
    if (mstrfip_sw_cfg_set(fip.dev, &sw_cfg) < 0) {
        fprintf(stderr, "ERROR: Cannot configure SW: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }

    // Hardware configuration
    hw_cfg.enable_ext_trig = 1; /* Enable external trigger */
    hw_cfg.enable_int_trig = 0; /* Disable internal trigger */
    hw_cfg.enable_ext_trig_term = 1; /* No 50ohms term on external trigger */
    if (mstrfip_hw_cfg_set(fip.dev, &hw_cfg) < 0) {
        fprintf(stderr, "ERROR: Cannot configure HW: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }
    // Create macro cycle
    fip.ba.mcycle = mstrfip_macrocycle_create(fip.dev);
    if (fip.ba.mcycle == NULL) {
        fprintf(stderr, "ERROR: Create macrocycle failed: %s\n",
            mstrfip_strerror(errno));
        return 1;
    }

    // Create all periodic FIP variables
    if (fipCreatePeriodicVar()) {
        return 1;
    }

    // Create all aperiodic messages
    if (fipCreateAperiodicMsg()) {
        return 1;
    }

    return 0;
}



int32_t fipStartInterface(void)
{
    struct timeval time;

    // Check whether interface should already be running

    if(fip.run) return 1;

    // Increment start count and set start time

    fip.fieldbus_stats.start_count++;
    timingGetUserTime(0, &time);
    fip.fieldbus_stats.start_time = time.tv_sec;

    // Zero errors and warnings since reset

    fip.fieldbus_stats.interface_error_count_since_reset = 0;
    fip.fieldbus_stats.error_flag                        = 0;
    fip.fieldbus_stats.last_interface_error              = 0;

    // Initialise configuration

    if(fipInit())
    {
        fprintf(stderr, "ERROR: FIP initialisation failed\n");
        fip.starting_flag = 0;
        fip.run = 0;
        return 1;
    }
    fip.run = 1;

    // Clear fieldbus fault

    fgcddev.status.st_faults &= ~FGC_FLT_FIELDBUS;

    return 0;
}



void fipStopInterface(void)
{
    // Check whether the interface is already stopped

    if(!fip.run) return;
    fip.run = 0;

    // Set WorldFIP fault

    fgcddev.status.st_faults |= FGC_FLT_FIELDBUS;

    // Stop bus arbitrator

    fipbaStop();

    fip.dev = NULL;
}



int32_t fipRestartInterface(void)
{
    int cancel_state_orig;

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&fipba.mutex);

    // Disable watchdog for WorldFIP

    watchdog.fip_toggle = WATCHDOG_DISABLE;

    fipStopInterface();

    if(fipStartInterface())
    {
        // Interface failed to restart

        // Re-enable watchdog

        watchdog.fip_toggle = WATCHDOG_ENABLE;

        // Return error

        pthread_mutex_unlock(&fipba.mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return FGC_FIP_FAILURE;
    }

    // Restart the bus arbitrator

    if(fipbaStart())
    {
        // Bus arbitrator failed to restart

        // Un-set WorldFIP starting flag

        fip.starting_flag = 0;

        // Re-enable watchdog

        watchdog.fip_toggle = WATCHDOG_ENABLE;

        // Return error

        pthread_mutex_unlock(&fipba.mutex);
        pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
        return FGC_FIP_BA_FAILURE;
    }

    // Re-enable watchdog

    watchdog.fip_toggle = WATCHDOG_ENABLE;

    pthread_mutex_unlock(&fipba.mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);
    return 0;
}



/*
 * Send an aperiodic message
 */

static int32_t fipSendAperiodicMsg(uint32_t channel_num, uint32_t length)
{
    // Set-up the message

    fip.ba.tx_msg[channel_num]->bsz = length;

    // Send the message

    if (mstrfip_msg_write(fip.dev, fip.ba.tx_msg[channel_num]))
    {
        logPrintf(channel_num, "FIP Failed to queue aperiodic message\n");

        // Increment count of messages failed

        fip.fieldbus_stats.message_send_fail_count++;

        fip.fieldbus_stats.error_flag = 1;
        return 1;
    }
    else // Write was successful
    {
        // Increment count of messages sent

        fip.fieldbus_stats.message_send_count++;
    }

    return 0;
}



int32_t fipSendCommand(uint32_t channel_num, struct Cmd *cmd)
{
    uint32_t    cmd_msg_len;

    // Prepare command to be sent

    if(!(cmd_msg_len = fgcsmPrepareCommand(channel_num, cmd, fip.channel[channel_num].command_message)))
    {
        // No data could be sent

        return 0;
    }

    // Send command message

    if(fipSendAperiodicMsg(channel_num, cmd_msg_len))
    {
        // Message queue failed

        return 1;
    }

    return 0;
}



static void fipSetRealtimeRefs(int start_ch, int end_ch)
{
    int i;
    union {
        float f;
        uint32_t l;
    } f_l;

    // Set real-time sequence number
    fip.real_time_value->seq_num = fip.real_sequence;
    for (i = start_ch; i < end_ch; i++) {
        if (rt.data[rt.data_start].active_channels & (1LL << i)) { // Channel is active
            // Set new value
            f_l.f = rt.used_data.channels[i] =
                rt.data[rt.data_start].channels[i];
            f_l.l = htonl(f_l.l);
            fip.real_time_value->rt_data[i - start_ch] = f_l.f;
        } else { // Channel is inactive
            // Retain previous value
            f_l.f = rt.used_data.channels[i];
            f_l.l = htonl(f_l.l);
            fip.real_time_value->rt_data[i - start_ch] = f_l.f;
        }
    }
    // Set zero reference if real_time_zero flag set
    if (fieldbus.real_time_zero) {
        memset(&fip.real_time_value->rt_data, 0,
               sizeof (fip.real_time_value->rt_data));
    }
}



void fipPrepNextCycle(void)
{
    // Receive timing events

    timingReceiveEvents();

    // Get the UTC millisecond of the start of the WorldFIP cycle

    struct timeval time;
    timingGetUserTime(0, &time);

    // Copy fieldbus events to the time variable

    pthread_mutex_lock(&fieldbus.event_mutex);

    // Fill the events slots with events from the queue

    fieldbusFillSlotsFromQueue();

    // Copy the events into the Ethernet frame

    memcpy(fip.time_var_value->event, fieldbus.event, sizeof(fip.time_var_value->event));

    // Update fieldbus event slots

    fieldbusUpdateEvents();

    pthread_mutex_unlock(&fieldbus.event_mutex);

    // Set the time for the next cycle

    fipSetTime();

    // Prepare time variable

    fieldbusPrepareTime(fip.time_var_value);

    // Prepare code

    fieldbusPrepareCode(fip.time_var_value, fip.code_var_value,
                FGC_FIELDBUS_CODE_BLOCKS_PER_MSG);

    // Write code variable

    if(mstrfip_var_write(fip.dev, fip.ba.code_var))
    {
        logPrintf(0, "FIP Failed to write code variable\n");
        fip.fieldbus_stats.error_flag = 1;
    }

    // Write time variable

    if(mstrfip_var_write(fip.dev, fip.ba.time_var))
    {
        logPrintf(0, "FIP Failed to write time variable\n");
        fip.fieldbus_stats.error_flag = 1;
    }

    // Write real-time variables

    fip.real_sequence++; // TODO not anymore used ....

    // Set real-time references

    pthread_mutex_lock(&rt.mutex);

    // Set real-time references for channels 1-15
    /* points to the buffer of real_var1 periodic variable */
    fip.real_time_value = (struct fgc_fip_rt_data *)fip.ba.real_var1->buffer;
    fipSetRealtimeRefs(0, 15);

    if(mstrfip_var_write(fip.dev, fip.ba.real_var1))
    {
        logPrintf(0, "FIP Failed to write real-time variable 1\n");
        fip.fieldbus_stats.error_flag = 1;
    }

    // Set real-time references for channels 16-30
    /* points to the buffer of real_var2 periodic variable */
    fip.real_time_value = (struct fgc_fip_rt_data *)fip.ba.real_var2->buffer;
    fipSetRealtimeRefs(15, 30);

    if(mstrfip_var_write(fip.dev, fip.ba.real_var2))
    {
        logPrintf(0, "FIP Failed to write real-time variable 2\n");
        fip.fieldbus_stats.error_flag = 1;
    }

    // Clear active channels for this reference and advance data start

    rt.data[rt.data_start].active_channels  = 0;
    rt.data_start                           = (rt.data_start + 1) % RT_BUFFER_SIZE;

    pthread_mutex_unlock(&rt.mutex);
}



/*
 * Set the time from time source
 */

static void fipSetTime(void)
{
    uint32_t        last_ms;
    struct timeval  time;

    // Read the time from the time source

    timingReadTime(&time);

    // As this is for the next cycle, the time must be incremented

    last_ms = (time.tv_usec / 1000) + FGC_FIELDBUS_CYCLE_PERIOD_MS;

    // Check for the passing of a second boundary

    if(last_ms >= 1000)
    {
        time.tv_sec++;
        last_ms -= 1000;
    }

    // Force last_ms to be on an even cycle period

    if(last_ms % FGC_FIELDBUS_CYCLE_PERIOD_MS)
    {
        last_ms -= last_ms % FGC_FIELDBUS_CYCLE_PERIOD_MS;
    }

    fip.time_var_value->unix_time    = htonl(time.tv_sec);
    fip.time_var_value->ms_time      = htons(last_ms);
    time.tv_usec                    = last_ms * 1000;

    // Set FGCD iteration time

    timingSetUserTime(0, &time);
}



/*
 * Call-back on time variable sent
 */

static void fipTimeVarSent(struct mstrfip_dev *dev, struct mstrfip_data *data,
               struct mstrfip_irq *irq)
{
    // Check whether to send long messages for FGC reset

    if(fieldbus.fgc_power_sig_count)
    {
        fieldbus.fgc_power_sig_count--;

        // Send the message

        if(fipSendAperiodicMsg(0, FIP_MSG_LONG_LENGTH))
        {
            logPrintf(0, "FIP Failed to queue long message\n");
        }
        else // Write was successful
        {
            logPrintf(0, "FIP Queued long message, %d remaining\n", fieldbus.fgc_power_sig_count);
        }

        // Set WorldFIP starting flag to satisfy watchdog until FGCs re-lock

        fip.starting_flag = 1;
    }
}



/*
 * Call-back on reception of diagnostic device variable
 */

static void fipDiagDevReceived(struct mstrfip_dev *dev, struct mstrfip_data *data,
                   struct mstrfip_irq *irq)
{
    // Retrieve data

    mstrfip_var_update(fip.dev, data);

    switch (data->status) {
    case MSTRFIP_DATA_OK:
        fip.diag_dev.present = 1;
        fip.diag_dev.var_recv_count++;

        // Add to published mask of online devices
        fgcddev.status.fgc_mask |= 1U << (FGCD_MAX_EQP_DEVS + 1);

        // Clear alarm
        if (fgcddev.status.st_warnings & FGC_WRN_WORLDFIP_DIAG) {
            logPrintf(0, "FIP Diagnostic device online\n");
            fgcddev.status.st_warnings &= ~FGC_WRN_WORLDFIP_DIAG;
        }
        break;

    case MSTRFIP_DATA_PAYLOAD_ERROR:
        // Update statistics with fault details
        //TODO: asked FGC's guys what they expect throught Promptness_false
        //Non_Significant and Signifiance_status_false

        //if(data->payload_errors & MSTRFIP_FRAME_PAYLOAD_NOT_REFRESH)
            //fip.diag_dev.notfreshed_fault_count++;

        if(data->payload_error & MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT)
            fip.diag_dev.nonsignificance_fault_count++;
        break;

    case MSTRFIP_DATA_FRAME_ERROR:
        //TODO lost error type TMO, CRC error, bad PDU error...
        fip.diag_dev.user_error_count++;
        break;
    case MSTRFIP_DATA_NOT_RECEIVED:
        // should never happen because we are in the var callback
        break;
    }
}



/*
 * Call-back on status variable reception
 */

static void fipStatusVarReceived(struct mstrfip_dev *dev, struct mstrfip_data *data,
                 struct mstrfip_irq *irq)
{
    struct FIP_channel  *channel;
    uint32_t            channel_num;
    struct FGCD_device  *device;
    union fgc_stat_var  *status;
    int32_t             status_seq;

    // Store the time at which the function started

    channel_num = (data->id & 0xFF);

    mstrfip_var_update(dev, data); // update the periodic status variable
    channel     = &fip.channel[channel_num];
    device      = channel->device;
    status      = &fieldbus.channel[channel_num].status;

    // Act upon FDM read status

    switch (data->status) {
    case MSTRFIP_DATA_OK:
        // Copy FGC status structure
        memcpy(status, data->buffer, sizeof (*status));
        // Check sequence number for change to ensure that status
        // variable has been updated
        status_seq = status->fieldbus_stat.ack & FGC_ACK_STATUS;

        if (status_seq != channel->prev_status_seq) {
            channel->prev_status_seq = status_seq;

            fgcsmProcessStatus(channel_num);
        }
        break;

    case MSTRFIP_DATA_PAYLOAD_ERROR:
        if(data->payload_error & MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT) {
            channel->fip_stats.promptness_fail_count++;
            fip.fieldbus_stats.errors.promptness_fail_count++;
            channel->fip_stats.nonsignificance_fault_count++;
            fip.fieldbus_stats.errors.nonsignificance_fault_count++;
            channel->fip_stats.significance_fault_count++;
            fip.fieldbus_stats.errors.significance_fault_count++;
        }
        break;

    case MSTRFIP_DATA_FRAME_ERROR:
        if (data->frame_error == MSTRFIP_FRAME_TMO)
            // current FGC implementation seems to ignore TMO
            break;
        // other errors (frame_err, crc,..)are treated as user_error ..
        logPrintf(device->id, "FIP FGC Status variable user error: 0x%02X\n", data->frame_error);
        channel->prev_status_seq = 0xFF;
        fip.fieldbus_stats.errors.user_error_count++;
        break;

    case MSTRFIP_DATA_NOT_RECEIVED:
        // should never happen because we are in the var callback
        break;
    }

}

/*
 * Call-back on periodic end variable transmission
 */

static void fipPerEndVarSent(struct mstrfip_dev *dev, struct mstrfip_data *data,
                 struct mstrfip_irq *irq)
{
    struct timeval  time;

    // Update FGC state machine

    fgcsmCheckStatus();

    // Set watchdog toggle bit if no error has occured

    if(!fip.fieldbus_stats.error_flag)
    {
        watchdog.fip_toggle = 1;
    }

    // Set device 0 (FGCD) as online in published FGC mask

    fgcddev.status.fgc_mask |= 1;

    // Check whether there are any FGCs online on the WorldFIP

    if(fgcddev.status.fgc_mask & 0x7FFFFFFE) // At least one FGC is online (0x7FFFFFFE is bits 1-30 for FGCs)
    {
        // Clear fault

        fgcddev.status.st_faults &= ~FGC_FLT_WORLDFIP_NOFGCS;

        // If WorldFIP interface was starting, it is now started

        fip.starting_flag = 0;
    }
    else // No FGCs are online
    {
        // Set fault

        fgcddev.status.st_faults |= FGC_FLT_WORLDFIP_NOFGCS;
    }

    // Check whether WorldFIP diagnostic device is missing

    if(!fip.diag_dev.present)
    {
        fip.diag_dev.var_miss_count++;

        // Set diagnostic device as offline within published data

        fgcddev.status.fgc_mask &= ~(1U << (FGCD_MAX_EQP_DEVS + 1));

        if(!(fgcddev.status.st_warnings & FGC_WRN_WORLDFIP_DIAG)) // Diagnostic device warning not yet set
        {
            logPrintf(0, "FIP Diagnostic device offline\n");
            fgcddev.status.st_warnings |= FGC_WRN_WORLDFIP_DIAG;
        }
    }
    fip.diag_dev.present = 0;

    // Timestamp published data

    timingGetUserTime(0, &time);
    pub.published_data.time_sec  = htonl(time.tv_sec);
    pub.published_data.time_usec = htonl(time.tv_usec);

    // Publish FGCD device data

    fgcddevPublish();

    // Trigger sending of published data

    pubTrigger();

    // Prepare for next cycle

    fipPrepNextCycle();
}

/*
 * Call-back on aperiodic message transmission
 */

static void fipAperiodicMsgSent(struct mstrfip_dev *dev, struct mstrfip_data *msg,
                    struct mstrfip_irq *irq)
{
    uint32_t channel_num = (msg->id & 0xFF);
    struct FIP_channel *channel = &fip.channel[channel_num];

    // Reset sequential toggle bit miss count

    fgcsm.channel[channel->device->id].count.seq_toggle_bit_miss = 0;

    // Add bytes to load

    fieldbus.stats.tx.load += msg->bsz;

    // Update FGC state machine if device is not the gateway

    if(channel->device->id != 0)
    {
        fgcsmCmdSent(channel->device->id);
    }
}



/*
 * Call-back on aperiodic message reception
 */

static void fipAperiodicMsgRecv(struct mstrfip_dev *dev, struct mstrfip_data *msg,
                    struct mstrfip_irq *irq)
{
    uint32_t                channel_num = (msg->id & 0xFF);
    struct FIP_channel      *channel    = &fip.channel[channel_num];
    uint32_t                i;
    struct fgc_fieldbus_rsp *rsp_msg;
    uint32_t                pkt_length, data_length;
    struct Rterm_resp       *rterm_resp;

    // Update the aperiodic message

    mstrfip_msg_update(dev, msg);

    // Check message status

    switch(msg->status)
    {
        case MSTRFIP_DATA_OK:
            break;

        case MSTRFIP_DATA_PAYLOAD_ERROR:
            logPrintf(channel->device->id, "FIP message payload error: 0x%02X\n", msg->payload_error);
            return;

        case MSTRFIP_DATA_FRAME_ERROR:
            logPrintf(channel->device->id, "FIP message frame error: 0x%02X\n", msg->frame_error);
            return;

        case MSTRFIP_DATA_NOT_RECEIVED:
            logPrintf(channel->device->id, "FIP message data not received error\n");
            return;
    }

    rsp_msg = (struct fgc_fieldbus_rsp *)msg->buffer;
    pkt_length = msg->bsz - sizeof(rsp_msg->header);
    data_length = pkt_length - rsp_msg->header.n_term_chs;

    // Increment aperiodic message count

    channel->aper_msg_rec_count++;

    // Add bytes to load

    fieldbus.stats.rx.load += msg->bsz;

    // Process message content if the number of terminal characters is valid

    if(rsp_msg->header.n_term_chs <= FGC_FIELDBUS_MAX_RTERM_CHARS &&
       rsp_msg->header.n_term_chs <= pkt_length)
    {
        // Process any remote terminal characters

        if(rsp_msg->header.n_term_chs)
        {
            // Check whether each client has a remote terminal on this channel

            for(i = 0 ; i < FGC_TCP_MAX_CLIENTS ; i++)
            {
                if(tcp.client[i].run                             &&
                   tcp.client[i].rterm_device == channel->device &&
                   (rterm_resp = (struct Rterm_resp *)queuePop(&cmdqmgr.free_rterm_q)))
                {
                    memcpy(rterm_resp->buffer, rsp_msg->rsp_pkt, rsp_msg->header.n_term_chs);
                    rterm_resp->buffer_length = rsp_msg->header.n_term_chs;

                    pthread_mutex_lock(&tcp.client[i].mutex);
                    tcp.client[i].num_rterm_resps++;
                    pthread_mutex_unlock(&tcp.client[i].mutex);

                    queuePush(&tcp.client[i].response_queue, rterm_resp);
                    rterm_resp = NULL;
                }
            }
        }

        // Handle response message

        if(data_length)
        {
            switch(rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK)
            {
                case FGC_FIELDBUS_FLAGS_CMD_PKT:
                    fgcsmProcessResponse(channel_num, rsp_msg, data_length);
                    break;

                case FGC_FIELDBUS_FLAGS_PUB_DIRECT_PKT:
                case FGC_FIELDBUS_FLAGS_PUB_GET_PKT:
                    fgcsmProcessPublishMsg(channel_num, rsp_msg, data_length);
                    break;
            }
        }
    }
    else // The number of terminal characters is invalid
    {
        logPrintf(channel->device->id, "FIP Invalid number of rterm characters (%hhu)\n", rsp_msg->header.n_term_chs);
    }
}

/*
 * Call-back on interface fatal error
 */

static void fipInterfaceFatalError(struct mstrfip_dev *dev,
                   enum mstrfip_error_list error)
{
    if(fip.fieldbus_stats.last_interface_error != static_cast<uint32_t>(error))
    {
        logPrintf(0,"fipInterfaceFatalError: %s (error = %u)\n", mstrfip_strerror(error), error);
    }

   fip.fieldbus_stats.error_flag = 1;
   fip.fieldbus_stats.last_interface_error = error;
   fip.fieldbus_stats.interface_error_count++;
   fip.fieldbus_stats.interface_error_count_since_reset++;
}


// EOF
