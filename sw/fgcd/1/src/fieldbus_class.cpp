/*!
 * @file   fieldbus_class.c
 * @author Stephen Page
 *
 * Class-specific functions for the fieldbus
 */

#include <cmd.h>
#include <fgcsm.h>
#include <fieldbus_class.h>
#include <fip.h>

int32_t fieldbusSendCommand(uint32_t channel_index, struct Cmd *cmd)
{
    int32_t status;

    if((status = fipSendCommand(channel_index, cmd)))
    {
        return status;
    }
    else // Command packet was queued successfully
    {
        fgcsmCmdQueued(channel_index);
        return 0;
    }
}

// EOF
