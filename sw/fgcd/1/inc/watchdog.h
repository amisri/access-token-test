/*!
 * @file   watchdog.h
 * @brief  Declarations for the watchdog thread
 * @author Stephen Page
 */

#ifndef WATCHDOG_H
#define WATCHDOG_H

#include <stdint.h>
#include <pthread.h>

// Constants

#define WATCHDOG_DISABLE   -1       //!< Disable watchdogging of a resource
#define WATCHDOG_ENABLE     1       //!< Enable watchdogging of a resource
#define WATCHDOG_PERIOD     5       //!< Period between watchdog checks in seconds

/*!
 * Struct containing global variables
 */

struct Watchdog
{
    pthread_t thread;               //!< Thread handle
    int32_t   fip_toggle;           //!< Toggle to check that WorldFIP is alive
};

extern struct Watchdog watchdog;

// External functions

/*!
 * Start the watchdog thread
 */

int32_t watchdogStart(void);

#endif

// EOF
