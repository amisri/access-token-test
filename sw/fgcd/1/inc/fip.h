/*!
 * @file   fip.h
 * @brief  Declarations for the WorldFIP bus
 * @author Stephen Page
 */

#ifndef FIP_MFIP_H
#define FIP_MFIP_H

#include <stdint.h>

#include <masterfip/libmasterfip.h>

#include <consts.h>
#include <defconst.h>

// Constants

#define FIP_CODE_VAR_ID         0x3004                                          //!< ID for Code variable
#define FIP_DIAG_ID             0x067F                                          //!< ID for diagnostic device variable
#define FIP_EXTRA_BASE_ID       0x3005                                          //!< Base ID for FIP extra variables
#define FIP_MEZZANINE_NR        0                                               //!< Card number
#define FIP_NUM_EXTRA_VARS      10                                              //!< Number of extra variables
#define FIP_MAX_VARS            (FGCD_MAX_EQP_DEVS + FIP_NUM_EXTRA_VARS + 7)    //!< Maximum number of variables in AE/LE
#define FIP_MSG_LONG_LENGTH     256                                             //!< Length of long message (for FGC power-cycle)
#define FIP_PER_END_VAR_ID      0x3001                                          //!< ID for periodic end variable
#define FIP_REAL_TIME_VAR1_ID   0x3002                                          //!< Periodic real time reference
#define FIP_REAL_TIME_VAR2_ID   0x3003                                          //!< Periodic real time reference
#define FIP_STATUS_BASE_ID      0x0601                                          //!< Base ID for FIP status variables
#define FIP_SUB_ID_ERROR        0xFF                                            //!< Subscription ID value to indicate that there is an error
#define FIP_TIME_VAR_ID         0x3000                                          //!< Periodic time variable
#define FIP_BUS_SPEED           MSTRFIP_BITRATE_2500                            //!< FGC app expects 2.5Mb/s FIP bus

#include <cmd.h>
#include <fgc_fip.h>
#include <fgc_stat.h>

struct FIP_channel
{
    struct FGCD_device      *device;                    //!< Pointer to device structure for this channel
    struct fgc_fieldbus_cmd *command_message;            //!< Message structure for sending commands
    uint32_t                aper_msg_rec_count;         //!< Number of aperiodic messages received
    int32_t                 prev_status_seq;            //!< Sequence number of previous status variable received

    struct
    {
        uint32_t nonsignificance_fault_count;           //!< Non-significance faults
        uint32_t promptness_fail_count;                 //!< Promptness failures
        uint32_t significance_fault_count;              //!< Significance status
        uint32_t user_error_count;                      //!< FIP user errors
    } fip_stats;                                        //!< WorldFIP statistics
};

struct fip_ba {
	// macro cycle object
	struct mstrfip_macrocycle *mcycle;
	// WorldFIP variables
	struct mstrfip_data *code_var;
	struct mstrfip_data *extra_var[FIP_NUM_EXTRA_VARS];
	struct mstrfip_data *per_end_var;
	struct mstrfip_data *real_var1;
	struct mstrfip_data *real_var2;
	struct mstrfip_data *time_var;
	// Status variables
	struct mstrfip_data *status_vars[FGCD_MAX_DEVS];
	// Variable for diagnostic device
	struct mstrfip_data *diagnostic_dev_var;
	// FGC's tx and rx aperiodic messages
	struct mstrfip_data *rx_msg[FGCD_MAX_EQP_DEVS + 1];
	struct mstrfip_data *tx_msg[FGCD_MAX_EQP_DEVS + 1];
};

struct fip_diag_dev {
	uint32_t present;	//!< Flag to indicate present during cycle
	uint32_t var_recv_count;	//!< Variables received count
	uint32_t var_miss_count;	//!< Variables received count
	uint32_t nonsignificance_fault_count;	//!< Non-significance faults
	uint32_t promptness_fail_count;	//!< Promptness failures
	uint32_t significance_fault_count;	//!< Significance status
	uint32_t user_error_count;	//!< FIP user errors
};

/*!
 * Struct containing global variables
 */

struct Fip
{
    int32_t                     run;                        //!< Flag to indicate that the WorldFIP should run
    int32_t                     starting_flag;              //!< Flag to indicate that the interface is starting
    int32_t                     read_time;                  //!< Flag to read the time from time source (selected using fgcd.time_source) on next time update

    struct mstrfip_dev 		    *dev;                       //!< the MFIP device
    struct FIP_channel          channel[FGCD_MAX_DEVS];     //!<Data for each WorldFIP channel
    struct fip_ba		        ba;

    struct fip_diag_dev 	    diag_dev;

    // Statistics for WorldFIP fieldbus

    struct
    {
        int32_t     error_flag;                         //!< Flag to indicate that an error has occured with WorldFIP interface
        uint32_t    start_count;                        //!< Count of number of interface starts/restarts
        uint32_t    start_time;                         //!< Time of last WorldFIP interface start

        uint32_t    interface_error_count;              //!< Interface errors
        uint32_t    interface_error_count_since_reset;  //!< Interface errors since last reset
        uint32_t    last_interface_error;               //!< Last interface error
        uint32_t    message_send_count;                 //!< Messages sent
        uint32_t    message_send_fail_count;            //!< Messages failed

        // Total error counts for all devices

        struct
        {
            uint32_t nonsignificance_fault_count;       //!< Non-significance faults
            uint32_t promptness_fail_count;             //!< Promptness failures
            uint32_t significance_fault_count;          //!< Significance status
            uint32_t user_error_count;                  //!< FIP user errors
        } errors;
    } fieldbus_stats;

    // Variable values

    uint8_t 			(* code_var_value)[FGC_CODE_BLK_SIZE];
    uint16_t 			*diagnostic_dev_value;
    uint8_t 			real_sequence;	//!< Real-time data sequence number
    struct fgc_fip_rt_data 	*real_time_value;
    struct fgc_fieldbus_time 	*time_var_value;

};

extern struct Fip fip;

// External functions

/*!
 * Initialise and start the FIP interface
 */

int32_t fipStartInterface(void);

/*!
 * Stop the FIP interface
 */

void fipStopInterface(void);

/*!
 * Restart the FIP interface
 */

int32_t fipRestartInterface(void);

/*!
 * Send a command
 */

int32_t fipSendCommand(uint32_t channel_num, struct Cmd *cmd);

/*!
 * Prepare for next cycle
 */

void fipPrepNextCycle(void);

#endif

// EOF
