/*!
 * @file   fieldbus_class.h
 * @brief  Class-specific fieldbus declarations
 * @author Stephen Page
 */

#ifndef FIELDBUS_CLASS_H
#define FIELDBUS_CLASS_H

#include <stdint.h>

// Forward declarations

struct Cmd;

// External functions

/*!
 * Set an event
 */

int32_t fieldbusSendCommand(uint32_t channel_index, struct Cmd *cmd);

#endif

// EOF
