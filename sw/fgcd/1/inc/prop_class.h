/*!
 * @file   prop_class.h
 * @brief  Declarations for class-specific properties
 * @author Stephen Page
 */

#ifndef PROP_CLASS_H
#define PROP_CLASS_H

#include <stdint.h>

#include <fgc_event.h>

// Forward declarations

struct prop;
struct Cmd;

/*!
 * Struct containing global variables
 */

struct Prop_class
{
    uint32_t event_buf[sizeof(struct fgc_event)];    //!< Buffer for set of FGC FIP event
};

extern struct Prop_class prop_class;

// External functions

/*!
 * Check whether a reset is currently permitted
 */

int32_t SetifResetOk(struct Cmd *command);

/*!
 * Set an event
 */

int32_t SetEvent(struct Cmd *command, struct prop *property);

/*!
 * Set an FGC code property
 */

int32_t SetFGCCode(struct Cmd *command, struct prop *property);

/*!
 * Set a sending of FGC log synchonisation signal
 */

int32_t SetFGCLogSync(struct Cmd *command, struct prop *property);

/*!
 * Set a read of FGC codes
 */

int32_t SetReadCodes(struct Cmd *command, struct prop *property);

/*!
 * Terminate the FGCD.
 *
 * @param[in] command     Pointer to the SET command which initiated the terminate function
 * @param[in] property    Pointer to the property which indicates the type of termination
 *
 * @returns If the function is successful, it does not return. It returns 1 on failure, i.e. it was called
 *          with a property it does not handle.
 */

int32_t SetTerminate(struct Cmd *command, struct prop *property);

#endif

// EOF
