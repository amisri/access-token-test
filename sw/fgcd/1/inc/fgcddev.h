/*!
 * @file   fgcddev.h
 * @brief  Declarations for the FGCD device.
 * @author Stephen Page
 *
 * Name code support added for class 1
 */

#ifndef FGCDDEV_H
#define FGCDDEV_H

#include <stdint.h>

#include <consts.h>
#include <fgc_consts_gen.h>
#include <fgc_stat.h>
#include <parser.h>

// Forward declarations

struct FGCD_device;
struct hash_table;

// Constants

#define FGCDDEV_FGC_CODE_MIN_LEN    32                          //!< Minimum length of an FGC code in bytes
#define FGCDDEV_CODE_GROUP_PATH     CONFIG_PATH"/code_groups"   //!< Path containing code group directories

/*!
 * Struct containing global variables
 */

struct FGCDdev
{
    struct FGCD_device  *device;                                        //!< Pointer to device structure
    struct fgc1_stat    status;                                         //!< Published data
    struct Parser       parser;                                         //!< Parser
    struct hash_table   *const_hash;                                    //!< Hash of constants
    struct hash_table   *prop_hash;                                     //!< Hash of properties
    uint8_t             test_bin[64];                                   //!< Test binary property
    char                test_char[64];                                  //!< Test character property
    float               test_float[16];                                 //!< Test float property
    int8_t              test_int8s[64];                                 //!< Test integer property
    uint8_t             test_int8u[64];                                 //!< Test integer property
    int16_t             test_int16s[32];                                //!< Test integer property
    uint16_t            test_int16u[32];                                //!< Test integer property
    int32_t             test_int32s[16];                                //!< Test integer property
    uint32_t            test_int32u[16];                                //!< Test integer property
    char const         *test_string[2];                                //!< Test string property
    char const          *fgc_class_name;                                //!< Class name
    uint8_t             fgc_platform;                                   //!< Platform
    char const          *fgc_platform_name;                             //!< Platform name

    // FGC codes

    char                fgc_code[FGC_CODE_NUM_SLOTS][FGC_CODE_SIZE];    //!< Code data
    char                fgc_code_name[FGC_CODE_NUM_SLOTS][32];          //!< Names of files from which codes were loaded
    char                *fgc_code_name_property[FGC_CODE_NUM_SLOTS];    //!< Pointers to access code names as a string property
    uintptr_t           fgc_code_length[FGC_CODE_NUM_SLOTS];            //!< Code lengths
    uint8_t             fgc_code_class[FGC_CODE_NUM_SLOTS];             //!< Code classes
    uint8_t             fgc_code_id[FGC_CODE_NUM_SLOTS];                //!< Code IDs
    uint16_t            fgc_code_chksum[FGC_CODE_NUM_SLOTS];            //!< Code checksums
    uint32_t            fgc_code_version[FGC_CODE_NUM_SLOTS];           //!< Code versions
    uint16_t            fgc_code_old_version[FGC_CODE_NUM_SLOTS];       //!< Code versions using the old version system
    int32_t             send_fgc_code_flag;                             //!< Flag to enable or disable sending of FGC codes

    // FGC code transmission

    uint32_t            fgc_code_tx_force;                              //!< Force transmission of numbered code
    uint32_t            fgc_code_tx_num;                                //!< Number of code that is currently being transmitted
    uint32_t            fgc_code_tx_valid_flag;                         //!< Flag to indicate that currently transmitted code is valid
};

extern struct FGCDdev fgcddev;

// External functions

/*!
 * Start the device
 */

int32_t fgcddevStart(void);

/*!
 * Initialise data for device
 */

void fgcddevInitDevice(void);

/*!
 * Publish data for FGCD device
 */

void fgcddevPublish(void);

/*!
 * Read FGC codes from files
 */

int32_t fgcddevReadCodes(void);

#endif

// EOF
