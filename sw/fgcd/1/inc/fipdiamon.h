/*!
 * @file   fipdiamon.h
 * @brief  Declarations for reporting WorldFIP status to Diamon
 * @author Stephen Page
 */

#ifndef FIPDIAMON_MFIP_H
#define FIPDIAMON_MFIP_H

#include <stdint.h>

// Constants

#define FIPDIAG_SHM_KEY 0x46444730  //!< Key for WorldFIP Diamon shared memory

#include <masterfip/libmasterfip.h>

// External functions

/*!
 * Initialise variables
 */

int32_t fipdiamonInit(void);

/*!
 * Update data
 */

void fipdiamonUpdate(void);

#endif

// EOF
