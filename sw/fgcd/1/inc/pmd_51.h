/*!
 * @file   pmd_51.h
 * @brief  Declarations for submitting post-mortem data
 * @author Stephen Page
 */

#pragma once

#include <cstdint>
#include <pm-dc-rda3-lib/PMClient.hpp>

#include <fgcd.h>
#include <pm.h>

void pmd_51GenerateAndSendExt(pm::Client &pmclient, struct FGCD_device *device, const struct PM_ext_buffer &buffer, int64_t timestamp);
void pmd_51GenerateAndSendSelf(pm::Client &pmclient, struct FGCD_device *device, const struct PM_self_buffer_51 &buffer, int64_t timestamp);

// EOF
