/*!
 * @file   fipba.h
 * @brief  Declarations for the WorldFIP bus arbitrator
 * @author Stephen Page
 */

#ifndef FIPBA_MFIP_H
#define FIPBA_MFIP_H

#include <stdint.h>
#include <masterfip/libmasterfip.h>

#include <consts.h>
#include <fgc_consts_gen.h>

// Constants

#define FIPBA_PRIORITY      1
#define FIPBA_PRIORITY_MAX  10

#include <fip.h>

/*!
 * Struct containing global variables
 */

struct FIPba
{
    int32_t             run;                                        //!< Flag to indicate that the BA should run
    uint32_t            start_count;                                //!< Count of number of starts/restarts
    pthread_mutex_t     mutex;                                      //!< Mutex to protect stopping/starting
};

extern struct FIPba fipba;

// External functions

/*!
 * Start the bus arbitrator
 */

int32_t fipbaStart(void);

/*!
 * Stop the bus arbitrator
 */

void    fipbaStop(void);

#endif

// EOF
