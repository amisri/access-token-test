//! @file   pmd_92.h
//! @brief  Declaration of the PMX binary format for class 92

#pragma once

#include <cstdint>
#include <pm-dc-rda3-lib/PMClient.hpp>

#include <fgcd.h>
#include <pm.h>

void pmd_92GenerateAndSendExt(pm::Client &pmclient, struct FGCD_device *device, const struct PM_ext_buffer &buffer, int64_t timestamp);
void pmd_92GenerateAndSendSelf(pm::Client &pmclient, struct FGCD_device *device, const struct PM_self_buffer_std &buffer, int64_t timestamp);

// EOF
