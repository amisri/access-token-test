//! @file   mode_pc.h
//! @brief  Manage Power Converter mode changes for FGClite devices

#ifndef MODE_PC_H
#define MODE_PC_H

#include <stdint.h>
#include <fgc_errs.h>

//! Set the Power Converter State Machine requested state.
//!
//! Sets MODE.PC and requests the appropriate Voltage Source command (START/STOP/RESET) from the Power Converter State Machine.
//!
//! @param[in]    channel    FIP address of the FGClite device
//! @param[in]    mode       Desired PC state to transition to
//!
//! @returns      FGC_OK on success, or an error code on failure

enum fgc_errno modePcSet(uint32_t channel, uint32_t mode);


//! Asynchronous change of the CCLIBS mode_ref variable as a function of the external conditions (e.g. PC_PERMIT)
//!
//! @param[in]    channel    FIP address of the FGClite device

void modePcUpdate(uint32_t channel);

#endif

// EOF
