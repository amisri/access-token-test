//! @file   prop_class.h
//!
//! Declarations for class-specific properties

#ifndef PROP_CLASS_H
#define PROP_CLASS_H

#include <stdint.h>

// Forward declarations

struct prop;
struct Cmd;

//! Check whether a reset is currently permitted.
//!
//! @returns Always returns 1; reset is always permitted.

int32_t SetifResetOk(struct Cmd *command);

//! Simulate a timing event
//!
//! Implements the FIELDBUS.EVENT property
//!
//! @param[in] command     Pointer to the SET command
//! @param[in] property    Not used
//!
//! @returns    0 on success, FGC error code on failure

int32_t SetEvent(struct Cmd *command, struct prop *property);

//! Set an FGC code property
//!
//! @param[in] command     Pointer to the SET command
//! @param[in] property    Not used
//!
//! @returns    0 on success, FGC error code on failure

int32_t SetFGCCode(struct Cmd *command, struct prop *property);

//! Set a FIP bus arbitrator restart
//!
//! @param[in] command     Pointer to the SET command which initiated the restart
//! @param[in] property    Not used
//!
//! @returns    0 on success, FGC error code on failure

int32_t SetFipBARestart(struct Cmd *command, struct prop *property);

//! Set a FIP restart
//!
//! @param[in] command     Pointer to the SET command which initiated the restart
//! @param[in] property    Not used
//!
//! @returns    0 on success, FGC error code on failure

int32_t SetFipRestart(struct Cmd *command, struct prop *property);

//! Set a read of FGC codes
//!
//! @param[in] command     Pointer to the SET command which initiated the read codes
//! @param[in] property    Not used
//!
//! @returns    0 on success, FGC error code on failure

int32_t SetReadCodes(struct Cmd *command, struct prop *property);

//! Terminate the FGCD.
//!
//! @param[in] command     Pointer to the SET command which initiated the terminate function
//! @param[in] property    Pointer to the property which indicates the type of termination
//!
//! @returns If the function is successful, it does not return. It returns 1 on failure, i.e. it was called
//!          with a property it does not handle.

int32_t SetTerminate(struct Cmd *command, struct prop *property);

//! Terminate a FGClite device from the gateway
//!
//! This command is the same as SetTerminate, except it allows FGClite devices to be reset from the
//! gateway, even if they are offline (as long as they are still able to receive FIP communications).
//!
//! @param[in] command     Pointer to the SET command which initiated the terminate function
//! @param[in] property    Pointer to the property which indicates the type of termination
//!
//! @returns    0 on success, FGC error code on failure

int32_t SetGwTerminate(struct Cmd *command, struct prop *property);

#endif

// EOF
