//! @file   dim_bus.h
//! @brief  Declarations for the FGClite DIM Bus functions

#pragma once

#include <stdbool.h>
#include <stdint.h>

// Forward declarations

struct FGClite_status;
struct FGClite_command;

// External Functions

//! Clears the digital mask, indicating that the corresponding digital input should not be printed anymore in the event log
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_no          DIM number

void dimBusClearDigitalMask(uint32_t channel, uint8_t dim_num);


//! Returns a bitmask indicating which DIM positions contain a DIM
//!
//! @param[in]     channel         FIP address of the FGClite device
//!
//! @returns       bitmask of which DIMs are visible

uint16_t dimBusActiveDimsBitmask(uint32_t channel);


//! Check if the DIM scan has been completed
//!
//! @param[in]     channel         FIP address of the FGClite device
//!
//! @retval        true            The DIM scan is complete
//! @retval        false           The DIM scan has not started or is busy

bool dimBusIsScanComplete(uint32_t channel);


//! Full scan of the DIM bus - this is done once after a reset of the FGClite
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     status          FGClite status variable containing the paged and critical data
//! @param[in]     request_scan    Set to true to start a new scan of the DIM bus. Set to false to continue current scan in progress (if any).
//! @param[out]    ctrl_cmd        FGClite command variable to send the paged data requests to the device

void dimBusFullScan(uint32_t channel, const struct FGClite_status *status, bool request_scan, struct FGClite_command *ctrl_cmd);


//! Rescan DIM bus pages which contain at least one DIM - this is done once per second
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     status          FGClite status variable containing the paged and critical data
//! @param[in]     request_read    Set to true to start a new read of the DIMs. Set to false to continue from the previous read (if any).
//!                                When the function starts a new read, it sets the value to false.
//! @param[in]     ctrl_cmd_set    Set to true if the CTRL_CMD has already been set in this cycle (and thus cannot be set again until
//!                                next cycle). If the function sets the CTRL_CMD, it sets the value to true.
//! @param[out]    ctrl_cmd        FGClite command variable to send the paged data requests to the device

void dimBusScan(uint32_t channel, const struct FGClite_status *status, bool *request_scan, bool *ctrl_cmd_set, struct FGClite_command *ctrl_cmd);

// EOF
