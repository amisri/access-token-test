//! @file   logInit_class.h
//! @brief  Includes log structure initialisation header file for the class 92


#ifndef LOGINIT_CLASS_H
#define LOGINIT_CLASS_H

#include <classes/92/logStructsInit.h>
#include <classes/92/logMenusInit.h>

#endif

// EOF
