//! @file   cc_reg.h
//! @brief  Initialise the regulation manager

#ifndef CC_REG_H
#define CC_REG_H

#include <stdint.h>

//! Bind CCLIBS regulation parameters to FGClite data structures and initialise regulation values.
//!
//! @param[in] channel    FIP address of the FGClite device
//!
//! @retval    0          Success
//! @retval    1          Failure

uint32_t ccRegInit(uint32_t channel);

#endif

// EOF
