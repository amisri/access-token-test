//! @file   state_pc.h
//! @brief  Manage Power Converter state and Reference state for FGClite devices

#ifndef STATE_PC_H
#define STATE_PC_H

#include <stdint.h>
#include <fgc_errs.h>

//! Reset the faults
//!
//! @param[in]    channel    FIP address of the FGClite device

enum fgc_errno statePcResetFaults(uint32_t channel);

//! Initialize STATE.PC for a channel to be FLT_OFF
//!
//! @param[in]    channel    FIP address of the FGClite device

void statePcInit(uint32_t channel);

//! Update the Power Converter State Machine.
//!
//! Transition from current PC state to requested PC state.
//!
//! @param[in]    channel    FIP address of the FGClite device

void statePcRT(uint32_t channel);

#endif

// EOF
