//! @file   fieldbus.h
//! @brief  Definitions for FGCD fieldbus properties

#ifndef FGCD_FIELDBUS_H
#define FGCD_FIELDBUS_H

#include <fgc_runlog.h>
#include <consts.h>

//! Struct to hold FGC run log

struct Fieldbus_channel
{
    uint8_t runlog[FGC_RUNLOG_SIZE_BYTES];                  //!< FGC run log
};


//! Struct for FGCD fieldbus properties

struct Fieldbus_properties
{
    //! Fieldbus statistics

    struct
    {
        uint32_t cycle_count;                               //!< Count of fieldbus cycles

        struct fieldbus_stats_rx
        {
            uint32_t load;                                  //!< Load in bytes per second
        } rx;                                               //!< Reception

        struct fieldbus_stats_tx
        {
            uint32_t load;                                  //!< Load in bytes per second
        } tx;                                               //!< Transmission
    } stats;

    // Values

    uint32_t            fgc_power_sig_count;                //!< Count of FGC power cycle triggers to send
    uint32_t            pc_permit;                          //!< pc_permit software interlock state
    int32_t             pc_permit_override;                 //!< pc_permit override (-1=force false, 1=force true, 0=no override)
    uint32_t            real_time_zero;                     //!< Flag to indicate whether to force all real-time values to zero
    uint32_t            sector_access;                      //!< sector_access software interlock state
    int32_t             sector_access_override;             //!< sector_access override (-1=force false, 1=force true, 0=no override)

    struct Fieldbus_channel channel[FGCD_MAX_DEVS];         //!< FGC Run Log
};


//! Global variables for FGC properties

extern struct Fieldbus_properties fieldbus;                 //!< Fieldbus properties


// External functions

//! Initialise FIELDBUS properties on the gateway

void fieldbusInit(void);

#endif

// EOF
