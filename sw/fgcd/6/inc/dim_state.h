//! @file   dim_state.h
//! @brief  Declarations for the FGClite DIM trigger state machine

#pragma once

#include <stdint.h>
#include <sys/time.h>

// Forward declarations

struct FGClite_status;
struct FGClite_command;

#include <fgclite_cmd.h>

// Constants

//! Maximum number of DIMs that will log analogue data.
//!
//! There is not sufficient bandwidth to log 16 DIMs, because the FGClite will overwrite its circular
//! buffers for DIM data before we have time to read it all out. The maximum no. of DIMs that we can
//! read out without overrunning is 13. See the ``Paged Memory Manager for FGClite'' document for
//! details and calculations.
//!
//! Therefore, to prevent DIM_LOG_INDEX from overrunning, we never log more than 13 DIMs, even if more
//! than 13 DIMs are installed. For the last three DIMs, although there is no logging, the analogue
//! and digital data is updated at 1 Hz as normal, accessible via the DIAG properties.
//!
//! This should not affect any operational systems, as the maximum number of DIMs envisaged for power
//! converters controlled by FGClite is 9.

const uint32_t DIM_BUS_MAX_LOG_DEVS = 13;

// Types

enum State_DIM_trigger
{
    DIM_RUNNING,        // RUNNING   (Normal mode)
    DIM_TRIGGERED,      // TRIGGERED (DIM has triggered, write digital data to Event Log)
    DIM_RESETTING,      // RESETTING (Send DIM reset, waiting for the DIM to reset)
    DIM_POLLING         // POLLING   (After DIM reset but before converter reset)
};


// Function declarations

//! Reset all the DIMs
//!
//! @param[in]     channel         FIP address of the FGClite device

void dimStateReset(uint32_t channel);


//! Initialise the list of active DIMs.
//!
//! DIMs in RUNNING state are active (the DIM is actively logging analogue data). If the DIM triggers,
//! it remains active while it logs data from 5s before to 1s after the trigger event. Then it becomes
//! inactive until the DIM fault is cleared.
//!
//! This function sets the initial state of all DIMs which are present in dim_mask. If the latched or
//! unlatched bit for a DIM is already set before this function is called, the state is set to
//! RESETTING, which will issue a DIM reset. Otherwise, the initial state is set to RUNNING.
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_mask        Bitmask of which DIMs are installed in this FGClite
//! @param[in]     status          FGClite status variable containing the latched and unlatched statuses

void dimStateInitActiveList(uint32_t channel, uint16_t dim_mask, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd);


//! Reset the list of active DIMs
//!
//! Sets the DIM logging status for all DIMs to INACTIVE. This is called on a device reset or if the
//! DIM log is overrun (which should never happen).
//!
//! @param[in]     channel         FIP address of the FGClite device

void dimStateResetActiveList(uint32_t channel);


//! Get the list of active DIMs
//!
//! This list should be used by the analogue logging manager to select which page of DIM analogue data
//! should be selected next.
//!
//! @param[in]     channel         FIP address of the FGClite device
//!
//! @returns       Bitmask of the DIMs which are actively logging analogue data

uint16_t dimStateGetActiveList(uint32_t channel);


//! Set the state of a DIM to INACTIVE
//!
//! This function only takes effect if the DIM is in TRIGGERED, RESETTING or POLLING state. It should
//! be called when post-trigger logging for the DIM has completed to signal that the DIM should stop
//! logging until it returns to RUNNING state.
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_no          DIM bus address

void dimStateSetInactive(uint32_t channel, uint8_t dim_no);


//! Determine if the DIM is reporting a latched fault state
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_no          DIM bus address
//!
//! @retval        true            The VS is in a fault state and the DIM trigger is latched
//! @retval        false           The VS is not in fault or the DIM trigger is unlatched

bool dimStateIsLatched(uint32_t channel, uint8_t dim_no);


//! Determine if the DIM is reporting an unlatched fault state
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_no          DIM bus address
//!
//! @retval        true            The VS is in a fault state
//! @retval        false           The VS is not in a fault state

bool dimStateIsUnlatched(uint32_t channel, uint8_t dim_no);


//! Check if the DIM has triggered.
//!
//! If the DIM has triggered, request a DIM reset. This function returns the current state of the
//! DIM. If the DIM has triggered, the calling function should write the 2x12-bit banks of digital
//! data to the Event Log. When the Voltage Source is reset, the DIM will return to RUNNING state.
//!
//! This function should be called once per second, when the DIM digital data is polled.
//!
//! @param[in]     channel          FIP address of the FGClite device
//! @param[in]     dim_no           DIM bus address
//! @param[in]     trigger_count    Count of 8us clock ticks from the start of the 20ms FIP cycle to the DIM trigger event
//! @param[out]    trigger_time     High-resolution time of the trigger event, adjusted by trigger_count
//!
//! @returns       DIM state (RUNNING, TRIGGERED, RESETTING or POLLING)

enum State_DIM_trigger dimStateCheckTrigger(uint32_t channel, uint8_t dim_no, uint16_t trigger_count, struct timeval *trigger_time);


//! Check DIM_LOG_INDEX for overrunning.
//!
//! Checks the log indices of all the DIMs in dim_mask against dim_log_index. If we are overwriting
//! the 8 samples in the range (0,+7), then the log is overwriting data that we are in the process of
//! reading.
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_log_index   Current value of DIM_LOG_INDEX from the critical data
//! @param[out]    dim             DIM bus address of the device which is overrunning
//!
//! @retval        true            The log for at least one DIM is overrunning. The DIM address of the
//!                                first detected overrun is returned in ::dim
//! @retval        false           The log is not overrunning

bool dimStateCheckOverrun(uint32_t channel, uint16_t dim_log_index, uint8_t *dim);


//! Reset DIM log indices for all DIMs, except those which have triggered
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     num_samples     Number of samples to log
//! @param[in]     dim_log_index   Current value of DIM_LOG_INDEX from the critical data

void dimStateResetLogIndices(uint32_t channel, uint32_t num_samples, uint16_t dim_log_index);


//! Prepare a FGClite command to request the next page of data for the specified DIM
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_no          DIM bus address
//! @param[out]    ctrl_cmd        FGClite command variable to send the paged data requests to the device
//!
//! @returns       Log index of the page that was requested

uint16_t dimStateRequestPage(uint32_t channel, uint8_t dim_no, struct FGClite_command *ctrl_cmd);


//! Update DIM trigger states
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_mask        Bitmask of which DIMs are installed in this FGClite
//! @param[in]     status          FGClite status variable containing the paged and critical data
//! @param[out]    ctrl_cmd        FGClite command variable to send the trigger reset command

void dimStateUpdate(uint32_t channel, uint16_t dim_mask, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd);

// EOF
