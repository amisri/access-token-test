//! @file   onewire.h
//! @brief  Declarations for the FGClite Dallas One-Wire Bus functions

#ifndef ONEWIRE_H
#define ONEWIRE_H

#include <stdbool.h>
#include <stdint.h>

// forward declaration

struct FGClite_status;
struct FGClite_command;

//! Invalidate the 1-wire data until the next scan of the bus
//!
//! @param[in]     channel         FIP address of the FGClite device

void onewireReset(uint32_t channel);

//! Start a new scan of all the 1-wire buses in the system
//!
//! If a scan is currently in progress, it is abandoned and re-started
//!
//! @param[in]     channel         FIP address of the FGClite device

void onewireStartFullScan(uint32_t channel);

//! Start a scan of 1-wire devices which contain a temperature sensor
//!
//! @param[in]     channel         FIP address of the FGClite device

void onewireStartTempScan(uint32_t channel);

//! Returns the number of page requests required to scan all 1-wire temperature sensors
//!
//! @param[in]     channel         FIP address of the FGClite device
//!
//! @returns       Number of pages containing 1-wire temperature sensors

uint32_t onewireTempPageCount(uint32_t channel);

//! Check if we have completed a full scan of the 1-wire buses
//!
//! @param[in]     channel         FIP address of the FGClite device
//!
//! @retval        true            The 1-wire scan is complete
//! @retval        false           The 1-wire scan has not started or is still busy

bool onewireIsScanComplete(uint32_t channel);

//! Check if we have populated the BARCODE and FGC.ID.* properties
//!
//! @param[in]     channel         FIP address of the FGClite device
//!
//! @retval        true            The 1-wire data is ready
//! @retval        false           The 1-wire scan has not started or is still busy

bool onewireIsDataReady(uint32_t channel);

//! Update BARCODE and FGC.ID.* properties with the results of the last 1-wire scan
//!
//! @param[in]     channel         FIP address of the FGClite device

void onewireUpdateProps(uint32_t channel);

//! Update the 1-wire state machine
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     status          FGClite status variable containing the paged and critical data
//! @param[out]    ctrl_cmd        FGClite command variable to send the scan 1-wire and get paged data requests to the device

void onewireUpdate(uint32_t channel, const struct FGClite_status *status, bool *ctrl_cmd_set, struct FGClite_command *ctrl_cmd);

#endif

// EOF
