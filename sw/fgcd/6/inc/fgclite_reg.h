//! @file   fgclite_reg.h
//! @brief  Declarations for FGClite regulation functions

#ifndef FGCLITE_REG_H
#define FGCLITE_REG_H

#include <stdint.h>

// Forward declarations

struct FGClite_status;
struct FGClite_command;

//! Reset the configuration of a FGClite device.
//!
//! Reset properties to default state and request load of configuration from non-volatile storage.
//! This function should be called when the FGClite hardware is reset, to bring the software state
//! into sync with the hardware state.
//!
//! @param[in] channel    FIP address of the FGClite device

void fgcliteRegResetDevice(uint32_t channel);

//! Tasks which are executed every cycle before acquiring the measurements.
//!
//! This function is executed regardless of whether the device has reported itself as present in the cycle.
//!
//! @param[in] channel    FIP address of the FGClite device
//! @param[in] status     Pointer to device status struct
//! @param[in] sync_cmd   Pointer to device SYNC_CMD struct
//! @param[in] ctrl_cmd   Pointer to device CTRL_CMD struct
//! @param[in] conv_cmd   Pointer to device CONV_CMD struct

void fgcliteRegPre(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd);

//! Tasks which are executed every cycle after acquiring the measurements
//!
//! This function regulates the device, updates the PC state machine, and sets the CONVERTER_CMD. It
//! is executed regardless of whether the device has reported itself as present in the cycle.
//!
//! For devices which are UNCONFIGURED, the function does not regulate the device or update the PC
//! state machine. It simply checks the device status and returns.
//!
//! @param[in] channel    FIP address of the FGClite device
//! @param[in] status     Pointer to device status struct
//! @param[in] sync_cmd   Pointer to device SYNC_CMD struct
//! @param[in] ctrl_cmd   Pointer to device CTRL_CMD struct
//! @param[in] conv_cmd   Pointer to device CONV_CMD struct

void fgcliteRegPost(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd);

//! Acquire measurements for a device which returned a status in the current cycle.
//!
//! @param[in] channel    FIP address of the FGClite device
//! @param[in] status     Pointer to device status struct
//! @param[in] sync_cmd   Pointer to device SYNC_CMD struct
//! @param[in] ctrl_cmd   Pointer to device CTRL_CMD struct
//! @param[in] conv_cmd   Pointer to device CONV_CMD struct

void fgcliteRegDevicePresent(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd);

//! Estimate measurements for a device which did not return a status in the current cycle.
//!
//! @param[in] channel    FIP address of the FGClite device
//! @param[in] status     Pointer to device status struct
//! @param[in] sync_cmd   Pointer to device SYNC_CMD struct
//! @param[in] ctrl_cmd   Pointer to device CTRL_CMD struct
//! @param[in] conv_cmd   Pointer to device CONV_CMD struct

void fgcliteRegDeviceNotPresent(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd);

#endif

// EOF
