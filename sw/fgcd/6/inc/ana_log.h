//! @file   ana_log.h
//! @brief  Declarations for the FGClite analogue logging functions
//! @author Michael Davis

#ifndef ANA_LOG_H
#define ANA_LOG_H

#include <stdint.h>

// Forward declarations

struct FGClite_status;
struct FGClite_command;

// Constants

const uint32_t ADC_BUFFER_SIZE = 32768;    // Total number of samples per ADC channel = 32768 samples @ 1 KHz = 32.77s



// External functions

//! Initialise analogue logging with which channels are to be logged
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     dim_mask        Bitmask of which DIM channels are to be logged

void anaLogInit(uint32_t channel, uint16_t dim_mask);

//! Reset all indices of ADC and DIM channels
//!
//! Note: To reset the indices to start logging from the page ending with the last sample written, set
//!       time_offset_ms = -20. The last sample that was written completes on the 20ms FIP cycle
//!       boundary and will be timestamped with the current FIP cycle time. We are reading 21 samples
//!       at a time, so the timestamp of the first sample in the new log should be offset from the
//!       current FIP cycle time by -20ms.
//!
//! @param[in]     channel              FIP address of the FGClite device
//! @param[in]     status               Pointer to critical and paged data for the device
//! @param[in]     is_adc_log_frozen    If ADC log is frozen, do not restart ADC logging
//! @param[in]     time_offset_ms       Offset for first sample in the new log from the last sample that was written

void anaLogResetIndex(uint32_t channel, const struct FGClite_status *status, bool is_adc_log_frozen, int32_t time_offset_ms);

//! Request the next page of analogue data
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[out]    ctrl_cmd        Pointer to CONTROLLER_CMD for the device
//!
//! @retval    true     All analogue data has been requested: reset the log indices
//! @retval    false    Some analogue data pages are still pending

bool anaLogRequestPage(uint32_t channel, struct FGClite_command *ctrl_cmd);

//! Read back analogue paged data written in the last cycle
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     status          Pointer to critical and paged data for the device

void anaLogGetPage(uint32_t channel, const struct FGClite_status *status);

//! Sanity check that ADC_LOG_INDEX and DIM_LOG_INDEX do not overrun.
//!
//! In theory, an overrun is impossible in systems with fewer than 15 DIMs. This function is a sanity check
//! which should never return true in an operational system.
//!
//! However, an overrun can occur if the FGClite spontaneously resets itself. The ADC_LOG_INDEX and DIM_LOG_INDEX
//! return to zero, invalidating the current read. In this case, the overrun detection will restart the logging
//! so the system can recover.
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     status          Pointer to critical and paged data for the device
//!
//! @retval        true            There was an overrun error, the page request is invalid
//! @retval        false           There is no overrun error

bool anaLogIsOverrun(uint32_t channel, const struct FGClite_status *status);

//! Freeze FGClite ADC index before it overruns.
//!
//! This should be called once per cycle while the logging system is in Post Mortem state, to ensure
//! that the FGClite onboard log buffer does not overrun.
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[in]     status          Pointer to critical and paged data for the device
//! @param[out]    ctrl_cmd        Pointer to CONTROLLER_CMD for the device

void anaLogFreezeAdc(uint32_t channel, const struct FGClite_status *status,  struct FGClite_command *ctrl_cmd);

//! Unfreeze FGClite ADC index.
//!
//! This should be called after reading out the FGClite ADC logs, to return the onboard logging system
//! to normal operation
//!
//! @param[in]     channel         FIP address of the FGClite device
//! @param[out]    ctrl_cmd        Pointer to CONTROLLER_CMD for the device

void anaLogUnfreezeAdc(uint32_t channel, struct FGClite_command *ctrl_cmd);

#endif

// EOF
