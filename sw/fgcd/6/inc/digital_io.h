//! @file   digital_io.h
//! @brief  Declarations for I/O across the digital interface

#ifndef DIGITAL_IO_H
#define DIGITAL_IO_H

#include <stdint.h>
#include <stdbool.h>

#include <classes/92/defconst.h>

// Types

//! Bit mask values for digital IO requests

enum Digital_IO_Request
{
    DIG_IO_VS_ON      = 1,     //!< Switch the voltage source on
    DIG_IO_VS_OFF     = 2,     //!< Switch the voltage source off
    DIG_IO_VS_RESET   = 4,     //!< Reset the voltage source
};

//! Struct to manage I/O over the digital interface (CONVERTER_OUTPUT)

struct Digital_IO
{
    uint16_t           status;                        //!< DIG.STATUS property. Direct digital input register.
    uint16_t           commands;                      //!< DIG.COMMANDS property. Requested commands register.
    uint32_t           pol_switch_requested_state;    //!< Previous polarity switch requested state.
    uint8_t            request;                       //!< Digital command requests.

    uint32_t           vs_reset_timer;                //!< Remaining number of cycles that the VS_RESET signal should be pulsed.
    uint32_t           vs_run_timeout_timer;          //!< Remaining number of cycles we waiting STARTING for VS_READY and VS_POWER_ON before a VS_RUN_TO_FLT
    uint32_t           pol_switch_cmd_timer;          //!< Remaining number of cycles that the POL_TO_POS or POL_TO_NEG commands should be pulsed.
};


// Function declarations

//! Get the Voltage Source state.
//!
//! @param[in]    dig                     Digital IO structure
//! @param[in]    state_pc_is_starting    true if the PC state machine is in the STARTING state, false otherwise
//!
//! @returns      Voltage state corresponding to the current state of the digital inputs

uint8_t digitalIOVsGetState(const struct Digital_IO *dig, bool state_pc_is_starting);


//! Request the set/reset of the VS_RUN or the generation of VS_RESET.
//!
//! @param[out] dig        Digital IO structure to update
//! @param[in]  request    Command to set

void digitalIORequestCmd(struct Digital_IO *dig, enum Digital_IO_Request request);


//! Process VS_RUN and VS_RESET digital command requests, setting the appropriate bits in the digital command register.
//!
//! @param[out] dig        Digital IO structure to update

void digitalIOProcessRequests(struct Digital_IO *dig);


//! Control the polarity switch position commands according to the requested state from libref.
//!
//! @param[out] dig                         Digital IO structure to update
//! @param[in]  pol_switch_state            Actual polarity switch state
//! @param[in]  pol_switch_requested_state  Requested polarity switch state

void digitalIOPolSwitch(struct Digital_IO *dig, uint32_t pol_switch_state, uint32_t pol_switch_requested_state);


//! Set or unset one or more digital commands.
//!
//! @param[out] dig      Digital IO structure to update
//! @param[in]  mask     Bit mask indicating which command bit(s) to set/unset
//! @param[in]  value    Value to set the bits to (0/1)

void digitalIOSetCmd(struct Digital_IO *dig, uint16_t mask, bool value);


//! Get the digital command value.
//!
//! @param[in] dig      Digital IO structure
//!
//! @returns    Value of the digital command structure

uint8_t digitalIOGetCmd(struct Digital_IO *dig);


//! Set or unset one or more bits in the digital status.
//!
//! @param[out] dig      Digital IO structure to update
//! @param[in]  mask     Bit mask indicating which status bit(s) to set/unset
//! @param[in]  value    Value to set the bits to (0/1)

void digitalIOSetStatus(struct Digital_IO *dig, uint16_t mask, bool value);


//! Test if one or more digital status bits are set.
//!
//! @param[in] dig      Digital IO structure to test
//! @param[in] mask     Bit mask indicating which status bit(s) to test
//!
//! @returns   true if all bits in the mask are set, false otherwise

bool digitalIOGetStatus(const struct Digital_IO *dig, uint16_t mask);

#endif

// EOF
