//! @file   defprops_class.h
//! @brief  Includes defconst, definfo and defprops for the class 92


#ifndef DEFPROPS_CLASS_H
#define DEFPROPS_CLASS_H

#include <classes/92/defconst.h>
#include <classes/92/definfo.h>
#include <classes/92/defprops.h>

#endif

// EOF
