//! @file   cc_sig.h
//! @brief  Interface to libsig and calibration functions
//!
//! Functions to initialise the CCLIBS analogue signal manager structures and provide an interface
//! and state machine for calibration
//!
//! <h2>Note on Calibration</h2>
//!
//! There are three ways in which calibration can be started:
//!
//! 1. The user requests calibration of ADCs, DCCTs or the DAC using S CAL <channel>[,<level>] or
//!    S CAL.AUTO.DCCTS <level>. This is called from a non-realtime thread and can only be performed
//!    when SetifCalOk() is true, i.e.:
//!
//!    Operational state is NORMAL or SIMULATION and PC state is OFF or FAULT_OFF
//!
//!    This calls ccSigCalRequest(), which sets a flag to request calibration of the selected
//!    channel(s). Once all selected channels have been requested, it calls ccSigCalStart(), which
//!    checks we are not already calibrating, then copies the requested list into the active list.
//!    The calibration will start on the next call to ccSigCalUpdate(), which is only called from the
//!    real-time thread.
//!
//! 2. The DAC is automatically calibrated when the PC state machine enters the STARTING state,
//!    before switching the PC on. This takes place in the realtime thread and can only happen when:
//!
//!    Operational state is NORMAL and PC state is OFF
//!
//!    This calls ccSigCalRequest() and ccSigCalStart() as above, followed by a call to
//!    ccSigCalUpdate(), which starts the calibration sequence. When the calibration has completed,
//!    the PC is switched on.
//!
//! 3. The ADCs are automatically calibrated once every 24 hours. This is configured with a call to
//!    ccSigScheduleAutoAdcsCal(). After the time to the next scheduled calibration has elapsed,
//!    calibration will start on the next call to ccSigCalUpdate(). ccSigScheduleAutoAdcsCal() is also
//!    called from SetCal if the user requests an automatic ADC calibration.
//!
//! ccSigCalUpdate() manages the Calibration state machine. This includes a check to start
//! calibration; calibration can ONLY be started by a call to this function. It is only called from
//! the realime thread in the following states:
//!
//! Function       OP state                          PC state
//!
//! statePcFO()    NORMAL                            FAULT_OFF
//! statePcOF()    NORMAL                            OFF
//! statePcST()    NORMAL                            STARTING (before switching on VS)
//! stateOpCL()    CALIBRATING                       (Must be FAULT_OFF | OFF | STARTING to enter CALIBRATING state)

#ifndef CC_SIG_H
#define CC_SIG_H

#include <stdint.h>

#include "libsig.h"

// Types

//! FGClite calibration channel bitmasks

enum Cal_Channel
{
    CAL_ADC_A  =  1,
    CAL_ADC_B  =  2,
    CAL_ADC_C  =  4,
    CAL_DCCT_A =  8,
    CAL_DCCT_B = 16,
    CAL_DAC    = 32
};


//! FGClite calibration non-volatile properties

enum Cal_Nonvol_Prop
{
    CAL_A_ADC_INTERNAL_GAIN,
    CAL_A_ADC_INTERNAL_ERR,
    CAL_B_ADC_INTERNAL_GAIN,
    CAL_B_ADC_INTERNAL_ERR,
    CAL_C_ADC_INTERNAL_GAIN,
    CAL_C_ADC_INTERNAL_ERR,
    CAL_A_DCCT_ERR,
    CAL_B_DCCT_ERR,
    CAL_NONVOL_PROP_MAX = CAL_B_DCCT_ERR
};


//! FGClite calibration non-volatile property names

const char* const Cal_nonvol_prop_string[] =
{
    "CAL.A.ADC.INTERNAL.GAIN",
    "CAL.A.ADC.INTERNAL.ERR",
    "CAL.B.ADC.INTERNAL.GAIN",
    "CAL.B.ADC.INTERNAL.ERR",
    "CAL.C.ADC.INTERNAL.GAIN",
    "CAL.C.ADC.INTERNAL.ERR",
    "CAL.A.DCCT.ERR",
    "CAL.B.DCCT.ERR"
};


// Function declarations

//! Bind CCLIBS analogue signal parameters to FGClite data structures and initialise values.
//!
//! @param[in]    channel    FIP address of the FGClite device
//!
//! @returns      0 on success, non-zero on failure

int32_t ccSigInit(uint32_t channel);


//! Reset the calibration state to IDLE.
//!
//! Calling this function also schedules the first automatic calibration of the ADCs
//!
//! @param[in]    channel    FIP address of the FGClite device

void ccSigCalReset(uint32_t channel);


//! Initialise Calibration properties
//!
//! Calibration properties are usually set from non-volatile storage on start-up. Properties which
//! have no value stored are initialised to a default value (usually zero), to allow us to
//! transition from UNCONFIGURED to NORMAL operational state. This is required because calibration
//! cannot take place in the UNCONFIGURED state, because we only start calling refIterationRT()
//! after the device is configured.
//!
//! @param[in]    channel    FIP address of the FGClite device

void ccSigInitCal(uint32_t channel);


//! Check whether a FGClite device is in its calibration sequence
//!
//! @param[in]    channel    FIP address of the FGClite device
//!
//! @returns      true if the specified device is calibrating, false otherwise

bool ccSigCalIsCalibrating(uint32_t channel);


//! Check whether a FGClite device is calibrating a specific channel
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    cal_channel    Calibration channel to check
//!
//! @returns      true if the specified channel is calibrating, false otherwise

bool ccSigCalChannelIsCalibrating(uint32_t channel, enum Cal_Channel cal_channel);


//! Get the calibration bits to set for CONVERTER_CMD
//!
//! @param[in]    channel    FIP address of the FGClite device
//!
//! @returns      Bitmask for the channels and level being calibrated

uint16_t ccSigCalGetCmdBits(uint32_t channel);


//! Get the raw value for DAC calibration
//!
//! @param[in]    channel    FIP address of the FGClite device
//!
//! @returns      zero in the case of ADC or DCCT calibration.
//!               Raw value for the level in the case of DAC calibration.

int16_t ccSigCalGetRawValue(uint32_t channel);


//! Request calibration of the specified calibration channel.
//!
//! Call this function once for each channel and level that you wish to calibrate. Then call
//! ccSigCalStart() to start the calibration sequence.
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    cal_channel    Calibration channel (ADCs, DCCTs or DAC)
//! @param[in]    level          Calibration level (OFFSET, POSITIVE or NEGATIVE)

void ccSigCalRequest(uint32_t channel, enum Cal_Channel cal_channel, enum SIG_cal_level level);


//! Start a new manual calibration sequence.
//!
//! This function triggers the calibration of all channels requested by ccSigCalRequest().
//!
//! @param[in]    channel        FIP address of the FGClite device
//!
//! @returns      0 if the calibration can be started, non-zero if the FGClite is in a
//!               state where calibration cannot be started.

int32_t ccSigCalStart(uint32_t channel);


//! Update the calibration state machine.
//!
//! This function checks whether to start a new calibration sequence, and manages the calibration. A
//! manual calibration sequence is triggered after a call to ccSigCalStart(). An automatic calibration
//! sequence is triggered when the elapsed time to the next automatic calibration has expired.
//!
//! A calibration sequence can only be triggered when the PC is OFF (or FAULT_OFF), and in the case
//! of automatic calibration, that the I_LOW flag is set. (If the current is decaying and there is a
//! quench, we don't want to lose data from the Post-Mortem because we were calibrating at the time).
//!
//! After auto-calibration of the ADCs has completed, this function will set SYNC_DB_CAL in the FGClite.
//!
//! @param[in]     channel       FIP address of the FGClite device

void ccSigCalUpdate(uint32_t channel);


//!  Schedule the next automatic ADC calibration
//!
//!  @param[in]    channel    FIP device to calibrate
//!  @param[in]    secs       Number of seconds in the future to schedule the ADC calibration

void ccSigScheduleAutoAdcsCal(uint32_t channel, uint32_t secs);

#endif

// EOF
