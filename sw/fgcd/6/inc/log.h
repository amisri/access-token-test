//! @file   6/inc/log.h
//! @brief  Interface to liblog
//!
//! Function declarations for initialising the CCLIBS log manager structures
//! and instantiating LOG properties


#ifndef LOG_H
#define LOG_H

#include <stdbool.h>
#include <stdint.h>
#include <cclibs.h>
#include <consts.h>

#include "classes/92/logMenus.h"    // Auto-generated log menu definitions

// Debug signal macro

#define DEBUG_SIG   &device.log.debug.debug_sig

//! Bind CCLIBS analogue and digital logging parameters to FGClite data structures and initialise values
//!
//! @param[in]        channel         FIP address of the FGClite device
//!
//! @returns          0 on success, non-zero on failure

void logInitClass(uint32_t channel);


//! Set the log menu name corresponding to the specified DIM index
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        dim_idx         DIM index
//! @param[in]        menu_name       Name of the log menu

void ccLogSetDimMenuName(uint32_t channel, uint32_t dim_idx, const char * const menu_name);


//! Update LOG properties for 100 Hz reference and measurement data
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    timestamp      Timestamp for this iteration

void ccLogIteration(uint32_t channel, const struct CC_us_time *timestamp);


//! Update LOG.* properties for 50 Hz critical data
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    status         Critical data

void ccLogCritical(uint32_t channel, const struct FGClite_status *status);


//! Check if we are currently logging to the ADC 1 kHz discontinuous log
//!
//! @param[in]    channel        FIP address of the FGClite device
//!
//! @retval       true           We are still waiting for more 1 kHz ADC paged data
//! @retval       false          The current 1 kHz ADC data log is complete

bool ccLogAdcIsLogging(uint32_t channel);


//! Finish a discontinuous logfile
//!
//! @param[in]    channel         FIP address of the FGClite device
//! @param[in]    log_idx         Log index
//! @param[in]    sample_count    Number of samples still to be logged. If this equals zero, the log
//!                               will be finalised. If it is less than zero, the log will be
//!                               abandoned. In the case of a DIM trigger, we explicitly set
//!                               sample_count to -1 to force the cancellation of the log.
//! @param[in]    timestamp       Timestamp of the final sample (used only if the log is finalised)

void ccLogEndDiscontinuous(uint32_t channel, enum LOG_index log_idx, int32_t sample_count, const CC_us_time *timestamp);


//! Start a new discontinuous log for 1 KHz ADC data
//!
//! The log will be automatically closed when the number of samples specified in the log definition
//! have been logged.
//!
//! @param[in]    channel           FIP address of the FGClite device
//! @param[in]    time_offset_ms    Offset in ms to be applied to the timestamp of the first sample in
//!                                 the log

void ccLogAdcStart(uint32_t channel, int32_t time_offset_ms);


//! Start a new discontinuous log for DIM analogue data
//!
//! The log will be automatically closed when the number of samples specified in the log definition
//! have been logged. If the DIM triggers, the log will be abandoned and logging will restart to
//! capture the time period before/after the trigger.
//!
//! @param[in]    channel           FIP address of the FGClite device
//! @param[in]    dim_no            DIM number
//! @param[in]    num_samples       Number of samples to log
//! @param[in]    time_offset_ms    Offset in ms to be applied to the timestamp of the first sample in
//!                                 the log

void ccLogDimStart(uint32_t channel, uint32_t dim_no, uint32_t num_samples, int32_t time_offset_ms);


//! Update LOG.SPY.* properties for 1 kHz ADC data.
//!
//! The timestamp for the paged data is calculated from index_offset. ADC_LOG_INDEX (the index of the
//! last-written ADC values) is timestamped with the FIP cycle time for the current 20ms cycle.
//! index_offset indicates the difference in ms between the first sample in the paged data and the
//! time of the current cycle.
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    adc_channel    Indicates which ADC channel is logged in paged data
//! @param[in]    status         Critical and paged data

void ccLogAdc(uint32_t channel, uint8_t adc_channel, const struct FGClite_status *status);


//! Update LOG.DIM.* properties for 50 Hz DIM data
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    dim_idx        Indicates which DIM was logged in paged data
//! @param[in]    log_idx        Index of first sample in paged data
//! @param[in]    status         Critical and paged data

void ccLogDim(uint32_t channel, uint8_t dim_idx, uint16_t log_idx, const struct FGClite_status *status);

#endif


// EOF
