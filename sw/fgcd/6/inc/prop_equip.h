//! @file   prop_equip.h
//!
//! Declarations for equipment device-specific functions for getting and setting properties

#ifndef PROP_EQUIP_H
#define PROP_EQUIP_H

#include <stdint.h>

// Forward declarations

struct prop;
struct Cmd;

// SETIF function declarations

//! Check whether CAL.ACTIVE can be set

int32_t SetifCalActiveOk(struct Cmd *command);

//! Check whether auto-calibration is permitted

int32_t SetifCalOk(struct Cmd *command);

//! Check whether the user can set CONFIG.MODE.

int32_t SetifConfigModeOk(struct Cmd *command);

//! Setif Fieldbus Command.
//!
//! Is the command coming from the WorldFIP/Ethernet network? This is to ensure the command is passed from a tool with secured access, like FGCRun+.

int32_t SetifFbsCmd(struct Cmd *command);

//! Check whether the operational mode can be set

int32_t SetifModeOpOk(struct Cmd *command);

//! Check whether the real-time mode can be set

int32_t SetifModeRtOk(struct Cmd *command);

//! Check whether operational state is normal or simulation

int32_t SetifOpNormalSim(struct Cmd *command);

//! Check whether operational state is not calibration

int32_t SetifOpNotCal(struct Cmd *command);

//! Check whether operational state is testing or simulation

int32_t SetifOpTestingSim(struct Cmd *command);

//! Check whether power converter is armed

int32_t SetifPcArmed(struct Cmd *command);

//! Check whether power converter is off

int32_t SetifPcOff(struct Cmd *command);

//! Check if it is possible to set REF.DAC

int32_t SetifRefDac(struct Cmd *command);

//! Check whether a new reference can be accepted

int32_t SetifRefOk(struct Cmd *command);

//! This function is required only for cycling operation. It always succeeds.

int32_t SetifRefUnlock(struct Cmd *command);

//! Check whether regulation state is NONE

int32_t SetifRegStateNone(struct Cmd *command);

//! Check whether it is okay to move switches
int32_t SetifSwitchOk(struct Cmd *command);



// SET function declarations

//! Set DAC, ADC and DCCT calibration error properties

int32_t SetCal(struct Cmd *command, struct prop *property);

//! Trigger calibration of DCCTs on both channels

int32_t SetCalDccts(struct Cmd *command, struct prop *property);

//! Handler for S FGC.DEBUG (not implemented)

int32_t SetCore(struct Cmd *command, struct prop *property);

//! Set PPM user.
//!
//! It is not valid to enable PPM for FGClite. This function is provided only for compatibility with the property
//! model.

int32_t SetDevicePPM(struct Cmd *command, struct prop *property);

//! Set the defaults for PLEP according to the value of LOAD.SELECT.
//!
//! This function is called when REF.DEFAULTS.I.RESET property is set (with no parameters). This is done automatically
//! when CONFIG.STATE is reset following a synchronisation by the FGC configuration manager.

int32_t SetDefaults(struct Cmd *command, struct prop *property);

//! Set DIG.COMMANDS property.
//!
//! Following the FGC2 implementation, we ignore the symbol list for SET. The symbol list is used to read back the property
//! only (using GetInteger).
//!
//! From the XML documentation: The command accepts an unsigned 16 bit integer parameter. The low byte is the set mask, and
//! the high byte is the reset mask, so it's easiest to enter the value in hex (prefix with "0x" or "0X"). Note that the
//! hardware does not allow bits to be set and reset at the same time so low and high bytes may not both be non-zero.

int32_t SetDirectCommand(struct Cmd *command, struct prop *property);

//! This function ignores any argments and flushes the Fifo log.

int32_t SetFifo(struct Cmd *command, struct prop *property);

//! Set internal ADC Multiplexing

int32_t SetInternalAdcMpx(struct Cmd *command, struct prop *property);

//! Set a power converter state

int32_t SetPC(struct Cmd *command, struct prop *property);

//! Validate and set i_period_iters

int32_t SetPeriodIters(struct Cmd *command, struct prop *property);

//! Set polarity

int32_t SetPolaritySwitch(struct Cmd *command, struct prop *property);

//! Reset a component of the FGClite.
//!
//! This function handles the RESET action for the following properties:
//!
//! S ADC RESET                  Synonym for S ADC.FILTER RESET.
//! S ADC.FILTER RESET           Reset of the FPGA dedicated to filter functions.
//! S CONFIG.STATE RESET         Reset CONFIG.MODE. This can only be done by the FGC configuration manager.
//! S DIAG RESET                 Reset diagnostic bus. This is done automatically following a DIM trigger.
//! S FGC.DLS RESET              Reset Dallas ID bus system. This will clear FGC.DLS error properties.
//! S LOG RESET                  Reset POST_MORTEM bit in unlatched status. This is done automatically by the gateway once the PM logs have been read.
//! S MEAS.STATUS RESET          Reset the status of the analogue measurement channels.
//! S SPY RESET                  Reset SPY.MPX to the default values.
//! S STATUS.ST_LATCHED RESET    Reset the latched status bits.
//! S VS RESET                   Clear the latched faults in STATUS.ST_LATCHED, and send a reset pulse to the voltage source to clear the external faults.
//! S VS.FW_DIODE RESET          Reset faults on the status of the Free Wheel Diode. This can only be done by an authorised individual (CCC operators are not authorised).
//!                              This is to ensure that someone visits the converter to read out which diode has failed, since that can only be done locally.
//! S VS.FABORT_UNSAFE RESET     Reset the Fast Abort Unsafe status. This can only be done by an authorised individual (CCC operators are not authorised).
//!                              This is to ensure that a responsible person is notified about the loss of redundancy in the reception of Fast Abort.
//! S VS.THYR_UNSAFE RESET       Reset the Thyristor Unsafe status. This can only be done by an authorised individual (CCC operators are not authorised).
//!                              This is to ensure that a responsible person is notified about the loss of redundancy in the triggering of the crowbar thyristors.

int32_t SetReset(struct Cmd *command, struct prop *property);

//! Reset the Voltage Source.
//!
//! This is identical to doing S VS RESET.

int32_t SetResetFaults(struct Cmd *command, struct prop *property);

//! Set the configuratoin mode to synchronize either the DB or the FGC

int32_t SetConfigMode(struct Cmd *command, struct prop *property);

//! Set VS.SIM_INTLKS

int32_t SetSimIntlks(struct Cmd *command, struct prop *property);

//! SetTerminate function for equipment devices
//!
//! Called from SetTerminate()

int32_t propEquipTerminate(struct Cmd *command, struct prop *property);


// GET function declarations

//! Get COMPONENT properties.
//!
//! The COMPONENT properties report the expected and unexpected components that were found,
//! and the component groups that were expected but not found (MISSING).

int32_t GetComponent(struct Cmd *command, struct prop *property);

//! Return CONFIG.CHANGED.
//!
//! This property contains the names and data of all configuration properties which have been changed since the last time CONFIG.STATE was reset,
//! which is done after synchronising with the database. It is intended for debugging the device configuration and is not used in normal operation.
//!
//! Note: The implementation of this function does a scan of the non-volatile storage, which for FGClite is on a filesystem. There is a risk
//!       that if the filesystem is inaccessible, the function could block the FGCD command thread. Therefore the function can only be used
//!       when the PC is in the OFF or FAULT_OFF states.

int32_t GetConfigChanged(struct Cmd *command, struct prop *property);

//! Return DIAG.ANA.
//!
//! Returns the labels and analogue values for all installed DIMs

int32_t GetDiagAna(struct Cmd *command, struct prop *property);

//! Return DIAG.DIG.
//!
//! Returns the labels and digital values for all installed DIMs

int32_t GetDiagDig(struct Cmd *command, struct prop *property);

//! Get list of Diagnostic Information Modules (DIMs).
//!
//! Returns either the names and addresses of the DIMs installed on this system, or the list of digital DIM inputs that are active faults.

int32_t GetDim(struct Cmd *command, struct prop *property);

//! Get FIFO log property

int32_t GetFifo(struct Cmd *command, struct prop *property);

//! Get the Dallas ID

int32_t GetId(struct Cmd *command, struct prop *property);

//! Retrieve LOG.PM.BUF buffer

int32_t GetLogPmBuf(struct Cmd *command, struct prop *property);

//! Get TEMP properties.
//!
//! Internally, the TEMP properties store the temperature in the raw units returned by the Dallas temperature sensors (1/16th of a degree per bit).
//! Externally they are returned as a floating point value in degrees Celsius with two decimal places.

int32_t GetTemp(struct Cmd *command, struct prop *property);



// PARS function declarations

//! Set DAC calibration parameters
//!
//! @param[in] channel    FIP address of the FGClite device

void ParsCalDac(uint32_t channel);

//! Set ADC and DCCT calibration parameters
//!
//! @param[in] channel    FIP address of the FGClite device

void ParsCalFactors(uint32_t channel);

//! Set limit parameters
//!
//! @param[in] channel    FIP address of the FGClite device

void ParsLimits(uint32_t channel);

//! Set libreg parameters
//!
//! @param[in] channel    FIP address of the FGClite device

void ParsRegPars(uint32_t channel);

#endif

// EOF
