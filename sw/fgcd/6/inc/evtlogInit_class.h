//! @file   evtlogInit_class.h
//! @brief  Includes evtlog structure init header files for the class 92


#ifndef EVTLOGINIT_CLASS_H
#define EVTLOGINIT_CLASS_H

#include <classes/92/defprops.h>
#include <classes/92/evtlogStructsInit.h>

#endif

// EOF
