//! @file   fgclite_cycle.h
//! @brief  Definitions for the FGClite Fieldbus Cycle
//!
//! Implementation of the FGClite Fieldbus Cycle protocol as defined in:
//!
//! B. Todd, "FGClite: Fieldbus Cycle", 2014

#ifndef FGCLITE_CYCLE_H
#define FGCLITE_CYCLE_H

#include <fgclite_cmd.h>

//! Issue a soft or hard reset command to the nanoFIP controller of the specified device
//!
//! This function queues nanoFIP reset requests as it is only possible to issue a single reset
//! in each 20ms cycle.
//!
//! @param[in]    channel      FIP address of the FGClite device
//! @param[in]    soft_hard    Specifies whether to perform a soft reset or power cycle the device.
//!
//! @returns      0 on success, otherwise returns the error code from queuePush()

int32_t fgcliteCycleNanofipReset(uint32_t channel, enum Nanofip_reset_type soft_hard);

//! Get a pointer to an FGClite command variable
//!
//! @param[in]    id    Specify the command
//!
//! @returns      pointer to the command struct for id

struct FGClite_command *fgcliteCycleGetCmdPtr(enum FGClite_protocol_command_id id);

//! Get a pointer to an FGClite status
//!
//! @param[in]    channel    FIP address of the FGClite device
//!
//! @returns      pointer to the last received status struct for the device

struct FGClite_status *fgcliteCycleGetStatusPtr(uint32_t channel);

 //! Get the value of the Uptime counter
 //!
 //! @param[in]    channel    FIP address of the FGClite device
 //!
 //! @returns      Value of the Uptime Counter that was last returned in the Round Robin register

uint32_t fgcliteCycleGetUptimeCounter(uint32_t channel);

//! Initialise and start the FGClite Cycle thread.
//!
//! The FGClite Cycle thread handles all the commands and statuses in each cycle.
//!
//! @retval  0    Initialisation and startup was successful
//! @retval -1    Initialisation and startup failed

int32_t fgcliteCycleStart(void);

//! Initialise and start the FIP interface.
//!
//! @retval  0    Initialisation and startup was successful
//! @retval -1    Initialisation and startup failed.

int32_t fgcliteCycleFipStart(void);

//! Initialise and start the BA macrocycle program
//!
//! @retval  0    Initialisation and startup was successful
//! @retval -1    Initialisation and startup failed

int32_t fgcliteCycleProtocolStart(void);

#endif

// EOF
