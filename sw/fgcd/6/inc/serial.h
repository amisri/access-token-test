//! @file   serial.h
//! @brief  Function declarations for serial tunnelling across the FIP interface

#ifndef SERIAL_H
#define SERIAL_H

#include <stdint.h>

//! Reset the serial buffers for a FGClite device.
//!
//! @param[in] channel    FIP address of the FGClite device

void serialReset(uint32_t channel);

//! Read the serial data from a FGClite device.
//!
//! This should be called after a valid status has been received from a device. The serial data is
//! sent as a 7-byte field within the critical data.
//!
//! @param[in] channel    FIP address of the FGClite device
//! @param[in] input      Pointer to the SERIAL_DATA field in the FGClite critical data
//!
//! @returns The number of bytes read

uint32_t serialReadCmd(uint32_t channel, const uint8_t *input);

//! Write serial responses to the FGClite
//!
//! This should be called before sending each COMMAND 1: SERIAL_CMD. Each cycle, this function should
//! be called for every SERIAL_CMD in the BA macrocycle.
//!
//! @param[in] output_ptr    Pointer to the payload field in SERIAL_CMD

void serialWriteRsp(uint8_t *output_ptr);

#endif

// EOF
