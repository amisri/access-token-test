//! @file   reference.h
//! @brief  Initialise the reference manager
//!
//! Functions to initialise the CCLIBS reference manager structure

#ifndef REFERENCE_H
#define REFERENCE_H

#include <stdint.h>

//! Class specific initialisation for libref
//!
//! @param[in] device_id         Device ID
//!
//! @retval 0 on success
//! @retval 1 if any parameter pointers are not set

uint32_t referenceInitClass(uint32_t device_id);

#endif

// EOF
