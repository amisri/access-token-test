//! @file   fip.h
//! @brief  Definitions for FGCD FIP properties

#ifndef FGCD_FIP_H
#define FGCD_FIP_H

#include <fip.mfip.h>
#include <consts.h>

//! Struct for FIP device statistics

struct FIP_channel
{
    struct FIP_device_stats fip_stats;                      //!< FIP device error statistics
};

//! Struct for FGCD FIP properties

struct FIP_properties
{
    uint32_t    start_time;                                 //!< Time of last WorldFIP interface start
    int32_t     read_time;                                  //!< Flag to read the time from time source (selected using fgcd.time_source) on next time update

    // Error statistics

    struct FIP_fieldbus_stats fieldbus_stats;               //!< FIP fieldbus error statistics

    struct FIP_channel        channel[FGCD_MAX_DEVS];       //!< Data for each WorldFIP channel

    struct FIP_device_stats   diag_dev;                     //!< Diagnostic device error statistics
};

//! Struct for FGCD FIP Bus Arbitrator properties

struct FIP_BA_properties
{
    uint32_t start_count;                                   //!< Number of times FIP BA has been started
};

// Global variables for FGC properties

extern struct FIP_properties    fip;                        //!< FIP properties
extern struct FIP_BA_properties fipba;                      //!< FIP BA properties

#endif

// EOF
