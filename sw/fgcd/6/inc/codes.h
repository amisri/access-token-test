//! @file   codes.h
//! @brief  Declarations for reading the FGC_DB codes


#ifndef CODES_H
#define CODES_H

#include <stdint.h>
#include <stdlib.h>

// Forward declaration

struct Dallas_id;

//constants

const uint32_t           CODES_COMPOSITE_BUS_ADDR_MASKS = 0x80;    //<! Mask for the initial bus address of the composite DIM


//! Initialise the FGC_DB codes
//!
//! @returns    Zero on success, non-zero if initialisation failed

int32_t codesInit(void);


//! Initialise device type and expected components per group.
//!
//! If the first characters of device_name (up to the first dot) is a valid type, then
//! type_prop is set to the same value. Otherwise, the value of type_prop is used to
//! initialise the system type. If neither are valid system types, type_prop will be
//! set to UKNWN.
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        device_name     The full device name
//! @param[in,out]    type_prop       The value of the DEVICE.TYPE property

void codesInitDevice(uint32_t channel, const char *device_name, char *type_prop);


//! Initialise the DIM definitions for the current system
//!
//! @param[in]        channel         FIP address of the FGClite device

void codesDimsInit(uint32_t channel);


//! Return a list of DIMs for current channel.
//!
//! If is_triggered is true, returns a list of DIMs in fault state, along with their DIM description and state description
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        is_triggered    If true, only list DIMs which have triggered (VS has an active fault)
//! @param[in]        from            Array index to start from
//! @param[in]        to              Array index to finish at
//! @param[in]        step            Array index step
//! @param[out]       buffer          Output buffer to write the results
//! @param[in]        max_len         Maximum number of characters to write to buffer (including terminating null)
//!
//! @returns          Number of characters added to buffer (not including terminating null)

int32_t codesDimsList(uint32_t channel, bool is_triggered, int32_t from, int32_t to, int32_t step, char *buffer, int32_t max_len);


//! Return either the analogue data or the digital data for all DIMs.
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        get_ana         If true, return the analogue data for all DIMs.
//!                                   If false, return the digital data for all DIMs.
//! @param[out]       buffer          Output buffer to write the results
//! @param[in]        max_len         Maximum number of characters to write to buffer (including terminating null)
//!
//! @returns          Number of characters added to buffer (not including terminating null)

int32_t codesDimsData(uint32_t channel, bool get_ana, char *buffer, int32_t max_len);


//! Get the name of a DIM analogue channel
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        dim_no          DIM number 0..15
//! @param[in]        dim_channel     DIM analogue channel 0..3
//!
//! @returns          Pointer to a char string containing the signal name for the specified channel

const char *codesDimsSignalName(uint32_t channel, uint32_t dim_no, uint32_t dim_channel);


//! Get the units of a DIM analogue channel
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        dim_no          DIM number 0..15
//! @param[in]        dim_channel     DIM analogue channel 0..3
//!
//! @returns          Pointer to a char string containing the signal units for the specified channel

const char *codesDimsSignalUnits(uint32_t channel, uint32_t dim_no, uint32_t dim_channel);


//! Log the state of each bit of the digital status of a triggered DIM in the Event Log.
//!
//! The semantics in TRIGGERED and POLLING states are slightly different:
//!
//! In TRIGGERED state, we log all FAULT bits which are set (in fact there should be exactly one fault
//! which caused the trigger) and all STATUS bits, regardless of whether they are set or not.
//!
//! In POLLING state, we log all FAULT and STATUS bits which are set but which have not previously been
//! logged.
//!
//! We track which bits have been logged between subsequent calls to this function using the
//! digital_mask parameter. This is updated and passed back to the calling function.
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        dim_no          DIM number 0..15
//! @param[in]        is_triggered    DIM is in TRIGGERED state (as opposed to POLLING)
//! @param[in]        timestamp       Timestamp of the event
//! @param[in]        digital_bank    Two-element array (for DIM digital bank A/B). Each element is a
//!                                   12-bit bitmask of bits set in that bank.
//! @param[in,out]    digital_mask    Two-element array corresponding to digital_bank. Each element is
//!                                   a 12-bit bitmask to mask out faults/statuses which have already
//!                                   been logged. This should be initialised to [0,0] when the DIM
//!                                   triggers. The returned value should be passed to subsequent
//!                                   calls to this function, as long as the DIM is in POLLING state.

void codesDimsEvtLog(uint32_t channel, uint32_t dim_no, bool is_triggered, const struct timeval *timestamp, const uint16_t digital_bank[2], uint16_t digital_mask[2]);


//! Retrieve the sysdb index

uint16_t codesGetSysdbIdx(uint32_t channel);


//! Add a detected component to a group
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        comp_idx        Index of the detected component in CompDb

void codesGroupsAdd(uint32_t channel, uint16_t comp_idx);


//! Check that all expected Dallas devices are present
//!
//! @param[in]        channel         FIP address of the FGClite device
//!
//! @retval           true            All expected Dallas devices are present, and there are no
//!                                   unexpected Dallas devices
//! @retval           false           An unexpected device was detected, or an expected device was
//!                                   missing

bool codeGroupsCheckOK(uint32_t channel);


//! Return a list of components expected/detected in each group
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        is_matched      If true, returns a list of all groups where the number of detected
//!                                   components matched the number of expected components. If false,
//!                                   returns a list where the number of detected and expected components
//!                                   do not match.
//! @param[out]      *buffer          Output buffer to write the results
//! @param[in]        max_len         Maximum number of characters to write to buffer (including
//!                                   terminating null)
//!
//! @returns          Number of characters added to buffer (not including terminating null)

int32_t codesGroupsList(uint32_t channel, bool is_matched, char *buffer, int32_t max_len);


//! Validate the Dallas ID, then look it up in IDDB, PTDB and CompDb
//!
//! @param[in]        dallas_id       Dallas ID to look up
//! @param[out]       barcode_text    Output buffer for barcode (minimum 20 chars)
//!
//! @returns          Index of the device in CompDb. 0 == Unknown device.
//!                   Errors are returned in barcode_text

uint16_t codesDallasLookup(struct Dallas_id *dallas_id, char *barcode_text);

#endif

// EOF
