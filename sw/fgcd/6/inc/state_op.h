//! @file   state_op.h
//! @brief  Manage Operational state for FGClite devices

#ifndef STATE_OP_H
#define STATE_OP_H

#include <stdint.h>


//! Initialize STATE.OP for a channel to be UNCONFIGURED
//!
//! @param[in]    channel    FIP address of the FGClite device

void stateOpInit(uint32_t channel);


//! Update the Operational State Machine.
//!
//! Transition from current operational state to requested operational state.
//!
//! @param[in]    channel    FIP address of the FGClite device

void stateOpRT(uint32_t channel);

#endif

// EOF
