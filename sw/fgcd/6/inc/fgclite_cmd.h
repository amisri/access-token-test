// @file   fgclite_cmd.h
// @brief  Define the command and status structures for the WorldFIP Fieldbus Cycle protocol
//
// Implementation of the commands for the WorldFIP Fieldbus Cycle protocol as defined in:
//
// https://edms.cern.ch/document/1523469

#ifndef FGCLITE_CMD_H
#define FGCLITE_CMD_H

#include <consts.h>


// Types

//! Command IDs enumerating the distinct commands in each cycle

enum FGClite_protocol_command_id
{
    CMD_SYNC,                                     //!< COMMAND 0: SYNC_COMMAND
    CMD_CTRL,                                     //!< COMMAND 2: CONTROLLER_COMMAND
    CMD_CONV,                                     //!< COMMAND 3: CONVERTER_COMMAND
    CMD_SER0,                                     //!< COMMAND 1: SERIAL_COMMAND
    FGCLITE_NUM_CMDS                              //!< Number of FIP variables broadcast from the gateway in each cycle
};


//! SEFI detector IDs for SYNC_CMD.
//!
//! Specifies the number of bits to shift SEFI_test_value when inserting into the payload.

enum SEFI_test_id
{
    SEFI_DET_TEST_VS_M0       = 26,               //!< SEFI detector VS M0
    SEFI_DET_TEST_VS_M1       = 24,               //!< SEFI detector VS M1
    SEFI_DET_TEST_IA_M0       = 22,               //!< SEFI detector IA M0
    SEFI_DET_TEST_IA_M1       = 20,               //!< SEFI detector IA M1
    SEFI_DET_TEST_IB_M0       = 18,               //!< SEFI detector IB M0
    SEFI_DET_TEST_IB_M1       = 16                //!< SEFI detector IB M1
};


//! SEFI test values for SYNC_CMD.

enum SEFI_test_value
{
    SEFI_NO_TEST              = 0,                //!< Normal operation / no test
    SEFI_ZERO                 = 1,                //!< Force to 0
    SEFI_ONE                  = 2,                //!< Force to 1
    SEFI_TOGGLE               = 3                 //!< Toggle
};


//! Which bit values to set in CONTROLLER_CMD.

enum Ctrl_cmd_id
{
    ADC_LOG_FREEZE            = 64,               //!< Freeze or unfreeze the post-mortem buffer for ADC values.
    DIM_LOG_FREEZE            = 32,               //!< Freeze or unfreeze the post-mortem buffer for DIM values.
    DIM_RESET                 = 16,               //!< Trigger inputs on all DIMs will be reset at the start of the next cycle.

    // Bits to control the One-Wire Bus scan

    OW_SCAN                   = 8,                //!< Bit 4 enables the OW bus scan (active high)

    // The last 3 bits select which bus to scan

    OW_BUS_SELECT_LOCAL       = 4,                //!< Scan local bus
    OW_BUS_SELECT_MEAS_B      = 3,                //!< Scan bus MEAS_B
    OW_BUS_SELECT_MEAS_A      = 2,                //!< Scan bus MEAS_A
    OW_BUS_SELECT_VS_B        = 1,                //!< Scan bus VS_B
    OW_BUS_SELECT_VS_A        = 0                 //!< Scan bus VS_A
};


//! Select which part of memory should be returned in CONTROLLER_CMD.
//!
//! The index type determines whether the index is interpreted as a page number or sample address.

enum Ctrl_cmd_index_type
{
    DIM_BUS                   = 0,                //!< Page number
    OW_BUS                    = 1,                //!< Page number
    ADC_V_MEAS_LOG            = 2,                //!< Sample address
    ADC_I_A_LOG               = 3,                //!< Sample address
    ADC_I_B_LOG               = 4,                //!< Sample address
    DIM_LOG                   = 5                 //!< Sample address
};


//! ADC channel setting for calibration.

enum Cal_source
{
    CAL_SOURCE_GND            = 0,                //!< Set calibration source to ground (0V)
    CAL_SOURCE_VREF_POS       = 1,                //!< Set calibration source to maximum positive V_REF (+10V)
    CAL_SOURCE_VREF_NEG       = 2,                //!< Set calibration source to maximum negative V_REF (-10V)
    CAL_SOURCE_DAC            = 3                 //!< Set calibration source to DAC (V_REF)
};


//! Which bit values to set in CONVERTER_CMD.
//!
//! The calibration commands CAL_VS_EN, CAL_IA_EN and CAL_IB_EN have no effect while the power converter is running.
//! If calibration is disabled, the ADC channel is connected to its normal external input.

enum Conv_cmd_id
{
    CAL_VS_EN                 = 0x2000,           //!< Enable calibration for the VS ADC channel.
    CAL_IA_EN                 = 0x1000,           //!< Enable calibration for the IA ADC channel.
    CAL_IB_EN                 = 0x0800,           //!< Enable calibration for the IB ADC channel.
    ADC_VS_RESET              = 0x0400,           //!< Reset the IB ADC channel.
    ADC_IA_RESET              = 0x0200,           //!< Reset the IA ADC channel.
    ADC_IB_RESET              = 0x0100,           //!< Reset the VS ADC channel.
    CMD_7_FGC_FLT             = 0x0080,           //!< FGC fault, contributes to the POWERING_FAILURE signal output from the FGC.
    CMD_6_INTLK_OUT           = 0x0040,           //!< Drives the spare interlock relay output from the FGClite.
    CMD_5_AF_RUN              = 0x0020,           //!< Sets the active filter run command.
    CMD_4_NOTIFY_RF           = 0x0010,           //!< Sets the notify RF command.
    CMD_3_POL_TO_NEG          = 0x0008,           //!< Move the converter from positive to negative polarity. This command should be pulsed and remain high for 2 FIP cycles (40 ms).
    CMD_2_POL_TO_POS          = 0x0004,           //!< Move the converter from negative to positive polarity. This command should be pulsed and remain high for 2 FIP cycles (40 ms).
    CMD_1_VS_RESET            = 0x0002,           //!< Set the hardware reset output signal. This command should be pulsed and remain high for 2 FIP cycles (40 ms).
    CMD_0_VS_RUN              = 0x0001            //!< Set the hardware run output signal.
};


//! Bitmasks for CONVERTER_INPUT

enum Conv_input
{
    POWER_ON                  = 0x0001,           //!< VS_POWER_ON digital status bit
    VLOOP_OK_NOT              = 0x0002,           //!< VLOOP_OK digital status bit
    VS_FAULT                  = 0x0004,           //!< VS_FAULT digital status bit
    VS_EXTINTLK               = 0x0008,           //!< VS_EXTINTLK digital status bit
    FASTPA_MEM                = 0x0010,           //!< FAST_ABORT digital status bit
    SW_OP_REQ                 = 0x0020,           //!< PC_DISCH_RQ digital status bit
    VS_NO_CABLE               = 0x0040,           //!< VS_NO_CABLE digital status bit
    VS_RSVD                   = 0x0080,           //!< Reserved (not used)
    DCCT_A_FLT                = 0x0100,           //!< DCCT_A_FLT status bit
    DCCT_B_FLT                = 0x0200,           //!< DCCT_B_FLT status bit
    POLSWPOS                  = 0x0400,           //!< POL_SW1_POS digital status bit
    POLSWNEG                  = 0x0800            //!< POL_SW1_NEG digital status bit
};


//! Bitmasks for CONTROLLER_STATUS

enum Ctrl_status
{
    SEQUENCE_NUMBER           = 0x0003,           //!< 2-bit sequence number
    ADC_IB_M1_SEFI_DET_TEST   = 0x0004,           //!< SEFI detector test for ADC IB M1
    ADC_IB_M0_SEFI_DET_TEST   = 0x0008,           //!< SEFI detector test for ADC IB M0
    ADC_IA_M1_SEFI_DET_TEST   = 0x0010,           //!< SEFI detector test for ADC IA M1
    ADC_IA_M0_SEFI_DET_TEST   = 0x0020,           //!< SEFI detector test for ADC IA M0
    ADC_VS_M1_SEFI_DET_TEST   = 0x0040,           //!< SEFI detector test for ADC VS M1
    ADC_VS_M0_SEFI_DET_TEST   = 0x0080,           //!< SEFI detector test for ADC VS M0
    COMMAND0_MISSING          = 0x0100,           //!< SYNC_CMD missing from FIP BA macrocycle
    COMMAND2_MISSING          = 0x0200,           //!< CONTROLLER_CMD missing from FIP BA macrocycle
    COMMAND3_MISSING          = 0x0400,           //!< CONVERTER_CMD missing from FIP BA macrocycle
    OW_SCAN_BUSY              = 0x0800,           //!< One-Wire Bus scan in progress
    PC_PERMIT                 = 0x1000,           //!< Status of PC_PERMIT interlock from the power converter (inverted logic)
    INTLK_IN                  = 0x2000,           //!< Status of INTLK_IN interlock from the power converter. This should be the negation of PC_PERMIT.
    POWERING_FAILURE          = 0x4000,           //!< POWERING_FAILURE status from the power conveter
    INTLK_OUT                 = 0x8000            //!< Status of INTLK_OUT interlock from the power converter
};


//! How to interpret the Round Robin bytes

enum Roundrobin_register_tag
{
    UPTIME_COUNTER_LO         = 0,                //!< Low bytes of uptime_counter
    UPTIME_COUNTER_HI         = 1,                //!< High bytes of uptime_counter
    PWRCYCLE_COUNTER          = 2,                //!< Power cycle reason and power cycle counter
    FPGA_VERSIONS             = 3,                //!< Versions of CF / NF / XF / PF FPGAs
    BACKPLANE_TYPE            = 4                 //!< Backplane type
};


//! Indicate which bits of the version field apply to each FPGA.
//!
//! The value is the number of bits to shift the version register to the right, and the last 4
//! bits indicate the version number.

enum FGClite_FPGA_version_bits
{
    PF_VERSION                = 0,                //!< Auxillary FPGA version
    XF_VERSION                = 4,                //!< Critical FPGA version
    CF_VERSION                = 8,                //!< Power FPGA version
    NF_VERSION                = 12,               //!< NanoFIP FPGA version
    RAW_VERSION                                   //!< Get the entire 16-bit register
};


//! Whether to send a soft or hard reset command.

enum Nanofip_reset_type
{
    RESET_SOFT                = 0,                //!< Perform a soft-reset of the nanoFIP and field-drive components.
    RESET_HARD                = 1                 //!< Remove power from the FGClite and restore it after a pre-defined interval has elapsed.
};


// FGClite command

struct __attribute__((__packed__)) FGClite_command
{
    uint32_t id;                                  //!< Byte 0 is the command ID. Bytes 1-3 are unused.
    uint32_t payload[FGCD_MAX_EQP_DEVS];          //!< Payload as defined in FGClite Fieldbus Cycle document.
};


// FGClite nanoFIP reset variable

struct __attribute__((__packed__)) FGClite_nanofip_reset
{
    uint8_t addr[2];                              //!< Byte 0 is address of device to soft reset. Byte 1 is address of device to power cycle.
};


// FGClite diagnostic device variable

struct __attribute__((__packed__)) FGClite_diag
{
    uint8_t byte[2];                              //!< Received value (not used).
};


//! Struct for FGClite critical data.
//!
//! The source of each element is labelled as CF (Critical FGGA) or XF
//!
//! The byte order and size of variables falls on byte boundaries as follows (p.13):
//!    1. The minimum data size is one byte.
//!    2. 16-bit values should always start on a byte number divisible by two
//!    3. 32-bit values should always start on an byte number divisible by four
//!
//! We use __attribute__((__packed__)) to enforce the exact data layout. This disables padding between struct members,
//! so members do not necessarily align with word boundaries. The cost of this may be that accessing struct members
//! requires more code and may be slower than if the struct elements were aligned. Also care must be taken when
//! accessing struct members, e.g. it is not possible to dereference a pointer to a member of a packed struct.
//!
//! Multi-byte values are little-endian.

struct __attribute__((__packed__)) FGClite_critical
{
    int32_t  v_meas_0_9;                          //!< CF Sum of voltage measurements for milliseconds 0-9. The filtered output from each of the three ADC channels, accumulated over 10 samples.
    int32_t  v_meas_10_19;                        //!< CF Sum of voltage measurements for milliseconds 10-19. The filtered output from each of the three ADC channels, accumulated over 10 samples.
    int32_t  i_a_0_9;                             //!< CF Sum of current measurements for channel A for milliseconds 0-9. The filtered output from each of the three ADC channels, accumulated over 10 samples.
    int32_t  i_a_10_19;                           //!< CF Sum of current measurements for channel A for milliseconds 10-19. The filtered output from each of the three ADC channels, accumulated over 10 samples.
    int32_t  i_b_0_9;                             //!< CF Sum of current measurements for channel B for milliseconds 0-9. The filtered output from each of the three ADC channels, accumulated over 10 samples.
    int32_t  i_b_10_19;                           //!< CF Sum of current measurements for channel B for milliseconds 10-19. The filtered output from each of the three ADC channels, accumulated over 10 samples.
    uint16_t dim_a_trig_lat;                      //!< XF 16 flags indicating whether each of the DIMs has triggered (latched)
    uint16_t dim_a_trig_unl;                      //!< XF 16 flags indicating whether each of the DIMs has triggered (unlatched)
    uint16_t dim_a_1_ana_0;                       //!< XF Dim bus A device 1 analogue signal 0. ADC values from the first DIM on bus A.
    uint16_t dim_a_1_ana_1;                       //!< XF Dim bus A device 1 analogue signal 1. ADC values from the first DIM on bus A.
    uint16_t dim_a_1_ana_2;                       //!< XF Dim bus A device 1 analogue signal 2. ADC values from the first DIM on bus A.
    uint16_t dim_a_1_ana_3;                       //!< XF Dim bus A device 1 analogue signal 3. ADC values from the first DIM on bus A.
    uint32_t cycle_period;                        //!< CF Fieldbus cycle period, incrementing every FPGA clock cycle (25 ns).
    uint16_t roundrobin_register_payload;         //!<    Value rotates between uptime counter, powercycle counter, FPGA version numbers and backplane type
    uint16_t adc_log_index;                       //!< CF The last INDEX which was written with logging information
    uint16_t dim_log_index;                       //!< CF The last INDEX which was written with logging information
    uint16_t converter_input;                     //!< CF Values of digital signals received from the power converter. See p.17.
    uint8_t  converter_output;                    //!< CF Values of digital signals sent to the power converter. See p.17.
    uint8_t  seu_count;                           //!< XF Count of single event upsets in the SRAM memory.
    uint16_t controller_status;                   //!< XF,CF FGClite status. See p.18.
    uint8_t  roundrobin_register_tag;             //!<    Code to give the interpretation of roundrobin_register_payload (above)
    uint8_t  serial_data[7];                      //!< CF Serial terminal emulator for calibration
};


//! Struct for FGClite status.
//!
//! The status is 124 bytes long. It is transmitted as a 128-byte broadcast-consumed variable; the first two bytes are
//! reserved for PDU_TYPE (= 0x40) and LENGTH (= 0x7E) which are not copied into the struct below. This is followed by
//! 64 bytes of paged data and 60 bytes of critical data (as defined above). The last two bytes are used for the nanoFIP
//! status (included below) and FIP MPS status. See p.14.

struct __attribute__((__packed__)) FGClite_status
{
    uint8_t                 paged[64];            //!< PAGED_0..PAGED_63 (See FGClite document p.18)
    struct FGClite_critical critical;             //!< p.13
    uint8_t                 nanofip_status;       //!< nanoFIP status
};


// Inline functions

//! Set command ID.
//!
//! The ID_DAT for each command does not get passed through the nanoFIP to the other FPGAs, so it needs to
//! be included in the command payload. The upper byte of ID_DAT for FGClite commands is set to 0x91 and
//! the lower byte specifies the command type (see the FGClite Fieldbus Cycle document).
//!
//! FIP FDM requires that each command has a unique ID_DAT, but FGClite requires that we have multiple commands
//! of the same type in one cycle (i.e. serial commands). This is handled by putting the FPGA command ID in the
//! least-significant nybble of ID_DAT and using the second nybble to ensure each ID_DAT is unique. Therefore
//! only the lowest 4 bits of each id are passed to the FPGA.
//!
//! @param[out] cmd    The command struct to change
//! @param[in]  id     The least significant nybble contains the command ID. The other bits are ignored.

static inline void fgcliteCmdSetId(struct FGClite_command *cmd, uint16_t id)
{
    cmd->id = id & 0x000F;
}


//! Set, unset or toggle the input of the SEFI detectors.
//!
//! Used to emulate Single Even Functional Interrupts (SEFI) for testing. This operation has no effect while
//! the power converter is running.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set
//! @param[in]  test_id     Specifies which SEFI detector to set
//! @param[in]  val         2-bit value which specifies whether to reset, set, unset or toggle the SEFI detector

static inline void fgcliteCmdSetSEFI(struct FGClite_command *cmd, uint8_t address, enum SEFI_test_id test_id, enum SEFI_test_value val)
{
    cmd->payload[address-1] &=  ~(3 << test_id);
    cmd->payload[address-1] |= (val << test_id);
}


//! Set all SEFI detectors to normal operation / no test
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set

static inline void fgcliteCmdResetSEFI(struct FGClite_command *cmd, uint8_t address)
{
    cmd->payload[address-1] &= 0x0000FFFF;
}


//! Set MS_PERIOD.
//!
//! MS_PERIOD is the period used for generating 20 millisecond strobes in terms of 25ns FPGA clock cycles.
//! The default value should be set to 0x9C40 = 40000 = 1ms.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set
//! @param[in]  ms_period   Value to set

static inline void fgcliteCmdSetMsPeriod(struct FGClite_command *cmd, uint8_t address, uint16_t ms_period)
{
    cmd->payload[address-1] &= 0xFFFF0000;
    cmd->payload[address-1] |= ms_period;
}


//! Request a page or memory region from a FGClite device.
//!
//! The requested page will be returned in the next FIP cycle.
//!
//! @param[out] cmd           The command struct to change
//! @param[in]  address       The FIP address of the device to set
//! @param[in]  index_type    Select which part of memory should be returned in a page starting at index.
//!                           If the type is a sample address, the page will be filled starting at the
//!                           given address.
//! @param[in]  index         Select the page or memory region to be returned

static inline void fgcliteCmdRequestCtrl(struct FGClite_command *cmd, uint8_t address, enum Ctrl_cmd_index_type index_type, uint16_t index)
{
    cmd->payload[address-1] &= 0x000000FF;
    cmd->payload[address-1] |= (index << 16) | (index_type << 8);
}


//! Set a controller command for a FGClite device.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set
//! @param[in]  cmd_bits    Which command bits to set

static inline void fgcliteCmdSetCtrl(struct FGClite_command *cmd, uint8_t address, uint8_t cmd_bits)
{
    // If we are setting OW_SCAN, mask out the last 3 bits

    if(cmd_bits & OW_SCAN) cmd->payload[address-1] &= 0xFFFFFFF8;

    cmd->payload[address-1] |= cmd_bits;
}


//! Unset a controller command for a FGClite device.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set
//! @param[in]  cmd_bits    Which command bits to unset

static inline void fgcliteCmdUnsetCtrl(struct FGClite_command *cmd, uint8_t address, uint8_t cmd_bits)
{
    cmd->payload[address-1] &= (0xFFFFFF00 | ~cmd_bits);
}


//! Reset the controller command for a FGClite device.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set

static inline void fgcliteCmdResetCtrl(struct FGClite_command *cmd, uint8_t address)
{
    cmd->payload[address-1] &= 0xFFFFFF00;
}


//! Set the reference voltage in a power converter.
//!
//! @param[out] cmd           The command struct to change
//! @param[in]  address       The FIP address of the device to set
//! @param[in]  v_ref

static inline void fgcliteCmdSetVRef(struct FGClite_command *cmd, uint8_t address, int16_t v_ref)
{
    cmd->payload[address-1] &= 0x0000FFFF;
    cmd->payload[address-1] |= (v_ref << 16);
}


//! Set the calibration source for a FGClite device.
//!
//! For calibration, the ADC channels can be set to certain fixed inputs. When calibrating,
//! only one value can be selected for all channels.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set
//! @param[in]  cal_source  2-bit value specifying the calibration source (GND, +VREF, -VREF, DAC)

static inline void fgcliteCmdSetCalSource(struct FGClite_command *cmd, uint8_t address, enum Cal_source cal_source)
{
    cmd->payload[address-1] &= 0xFFFF3FFF;
    cmd->payload[address-1] |= (cal_source << 14);
}


//! Set the converter command for a FGClite device.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to set
//! @param[in]  cmd_bits    Which command bits to set

static inline void fgcliteCmdSetConv(struct FGClite_command *cmd, uint8_t address, uint16_t cmd_bits)
{
    cmd->payload[address-1] |= cmd_bits;
}


//! Unset the converter command for a FGClite device.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to unset
//! @param[in]  cmd_bits    Which command bits to unset

static inline void fgcliteCmdUnsetConv(struct FGClite_command *cmd, uint8_t address, uint16_t cmd_bits)
{
    cmd->payload[address-1] &= (0xFFFF00000 | ~cmd_bits);
}


//! Reset the converter command for a FGClite device.
//!
//! @param[out] cmd         The command struct to change
//! @param[in]  address     The FIP address of the device to reset

static inline void fgcliteCmdResetConv(struct FGClite_command *cmd, uint8_t address)
{
    cmd->payload[address-1] &= 0xFFFF0000;
}


//! Read back the converter command for a FGClite device.
//!
//! @param[out] cmd         The command struct to read
//! @param[in]  address     The FIP address of the device to read
//!
//! @returns 32-bit CMD_CONV payload for the specified device

static inline uint32_t fgcliteCmdGetConv(const struct FGClite_command *cmd, uint8_t address)
{
    return cmd->payload[address-1];
}

#endif

// EOF
