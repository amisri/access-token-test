//! @file   defprops_inc.h
//! @brief  Includes header files needed by defprops.h

#ifndef DEFPROPS_INC_H
#define DEFPROPS_INC_H

#include <alarms.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <equip_libref.h>
#include <fgcsm.h>
#include <fieldbus.h>
#include <fip.h>
#include <hash.h>
#include <prop.h>
#include <prop_class.h>
#include <prop_equip.h>
#include <rt.h>
#include <tcp.h>
#include <timing.h>
#include <version.h>

#endif

// EOF
