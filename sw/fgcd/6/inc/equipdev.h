//! @file   equipdev.h
//! @brief  Declarations for equipment devices


#ifndef EQUIPDEV_H
#define EQUIPDEV_H

// CCLIBS includes

#include <sys/time.h>
#include <libreg.h>
#include <libref.h>
#include "classes/92/sigStructs.h"                          //!< Auto-generated libsig header
#include "classes/92/logMenus.h"                            //!< Auto-generated liblog header
#include "classes/92/logStructs.h"                          //!< Auto-generated liblog header
#include <classes/92/defconst.h>
#include <equip_common.h>
#include <equip_libevtlog.h>
#include <devname.h>
#include <fgc_errs.h>
#include <fgc_stat.h>
#include <digital_io.h>
#include <equip_libref.h>


// Constants

const uint32_t EQUIPDEV_CLASS_ID  = 92;                     //!< FGC class ID for equipment devices
const uint32_t EQUIPDEV_MAX_PROPS = 800;                    //!< Maximum number of properties supported by an equipment device
const uint32_t OW_BUS_MAX_DEVS    = 16;                     //!< Maximum number of devices on a single 1-wire bus


// Forward declarations

struct Parser;
struct prop;


//! State machine typedefs

typedef uint32_t  State(uint32_t channel, bool first_call);  //!< Type for state machine state function returning state number
typedef State   * Transition(uint32_t channel);              //!< Type for pointer to transition function returning pointer to state function


//! One-Wire Buses on FGClite

enum OW_buses
{
    OW_BUS_VS_A,                                            //!< Voltage Source Dallas ID Branch A

    OW_BUS_VS_B,                                            //!< Voltage Source Dallas ID Branch B
    OW_BUS_MEAS_A,                                          //!< Measurement Dallas ID Branch A
    OW_BUS_MEAS_B,                                          //!< Measurement Dallas ID Branch B
    OW_BUS_LOCAL,                                           //!< FGC Local Dallas ID Branch
    OW_BUS_MAX = OW_BUS_LOCAL                               //!< Maximum selectable bus on the FGClite (0..4)
};


//! FGClite property names corresponding to enum OW_buses

static const char* const OW_bus_property_name[] =
{
    "FGC.ID.VS.A",
    "FGC.ID.VS.B",
    "FGC.ID.MEAS.A",
    "FGC.ID.MEAS.B",
    "FGC.ID.LOCAL"
};


//! U_LEADS array indices
//!
//! For the 60A and 120A circuits, the current lead voltages are sampled using the DIM in the voltage
//! source at 50Hz (12-bit uncalibrated ADC accurate to 8-bit) and averaged at 1 Hz. If the measurement
//! exceeds 150mV, the converter will trip off.

enum U_Leads
{
   U_LEAD_POS,
   U_LEAD_NEG
};


//! ADC

struct Equipdev_channel_adc
{
    uint32_t        reset_counter;                          //!< Number of cycles to hold ADCs in RESET state (set by S ADC RESET or device reset).
    uint32_t        last_cal_time;                          //!< ADC.INTERNAL.LAST_CAL_TIME property. Contains the UNIX time when the internal
                                                            //!< 16-bit ADCs were last calibrated.
    int32_t         inhibit_cal;                            //!< ADC.INTERNAL.INHIBIT_CAL property

    uint16_t        st_meas_c;                              //!< STATUS.ADC.C.STATE property. Status of ADC C, connected to the V_MEAS channel.
                                                            //!< The statuses of ADC A and B are in the published data, but ADC C status is not
                                                            //!< published so we save it here.
    uint32_t        filter_state;                           //!< ADC.FILTER.STATE property
};


//! BARCODE

struct Equipdev_channel_barcode
{
    const char     *bus_L        [FGC_MAX_L_BARCODES];      //!< Local bus barcodes. This includes the FGC and backplane components.
    const char     *bus_V        [FGC_MAX_V_BARCODES];      //!< Voltage source bus barcodes
    const char     *bus_A        [FGC_MAX_A_BARCODES];      //!< Measurement bus A barcodes
    const char     *bus_B        [FGC_MAX_B_BARCODES];      //!< Measurement bus B barcodes

    char            fgc_cassette [FGC_ID_BARCODE_LEN];      //!< Barcode for FGC cassette
    char            fgc_ana      [FGC_ID_BARCODE_LEN];      //!< Barcode for FGC analogue interface board

    struct
    {
        char        head         [FGC_ID_BARCODE_LEN];      //!< Barcode for DCCT head
        char        elec         [FGC_ID_BARCODE_LEN];      //!< Barcode for DCCT electronics
    } dcct[2];                                              //!< Barcodes for DCCT A and DCCT B

    struct
    {
        char        cassette     [FGC_ID_BARCODE_LEN];      //!< Barcode for ADC cassette
        char        elec         [FGC_ID_BARCODE_LEN];      //!< Barcode for ADC modulator. This is set by the database as it cannot be read by the FGClite.
        char        psu          [FGC_ID_BARCODE_LEN];      //!< Barcode for ADC power supply unit
    } ext_adc[3];                                           //!< Barcodes for ADCs I_A, I_B and V_MEAS

    //! Structure to hold number of elements for each of the bus arrays.

    struct
    {
        uint32_t    bus_L;                                  //!< Length of local bus barcodes array
        uint32_t    bus_V;                                  //!< Length of voltage source bus barcodes array
        uint32_t    bus_A;                                  //!< Length of measurement bus A barcodes array
        uint32_t    bus_B;                                  //!< Length of measurement bus B barcodes array
    } num_els;
};


//! CAL properties

struct Equipdev_channel_cal
{
    uint32_t    active;                                     //!< CAL.ACTIVE property.
    float       dcct_a_headerr;                             //!< CAL.A.DCCT.HEADERR
    float       dcct_b_headerr;                             //!< CAL.B.DCCT.HEADERR

    uint32_t    nonvol_mask;                                //!< Bitmask of which CAL properties have been updated and need to be written to non-volatile storage
};


//! DCCT properties

struct Equipdev_channel_dcct
{
    uint32_t    primary_turns;                              //!< DCCT.PRIMARY_TURNS property. No. of primary turns for both DCCT channels. There is only one
                                                            //!< property for both channels, pointed to by primary_turns_p in each DCCT libsig transducer structure

    float       tau_temp;                                   //!< DCCT.TAU_TEMP property. Time constant for first-order temperature filters for both DCCT
                                                            //!< channels. There is only one property for both channels, pointed to by time_constant_s_p in each
                                                            //!< DCCT's libsig temp_filter structure.
};


//! DEVICE properties

struct Equipdev_channel_device
{
    uint16_t        omode_mask;                             //!< DEVICE.LHC_SECTORS property.
    uint32_t        ppm_enabled;                            //!< DEVICE.PPM property. Always set to DISABLED for FGClite.
    uint32_t        pwr_time;                               //!< DEVICE.PWR_TIME property.
    uint8_t         pwrcyc_count;                           //!< DEVICE.PWRCYC_COUNT property.
    uint8_t         pwrcyc_reason;                          //!< DEVICE.PWRCYC_REASON property.
    struct timeval  run_time;                               //!< DEVICE.RUN_TIME property. Time at which device was last reset.
    char            type[6];                                //!< DEVICE.TYPE property, used only for development and lab systems.
    uint32_t        fpga_version[4];                        //!< DEVICE.VERSION.FPGA property
};


//! DIAG properties

// Special DIM digital signals: FABORT_UNSAFE, FW_DIODE, THYR_UNSAFE, SUBCONV_LOST

#define DIAG_DIG_MAX_SIGS           5                       //!< Max number of signals for a given value
#define DIAG_DIG_SIG_LOGIC_OR       0                       //!< Logical OR: true if any digital signal is high
#define DIAG_DIG_SIG_LOGIC_DIFF     1                       //!< Logical different: true if any digital signal is different from the others

struct Equipdev_channel_diag_dig_sig
{
    const char     *sig_name;                               //!< Pointer to signal name
    uint32_t        fault_mask;                             //!< Fault mask
    uint32_t        warning_mask;                           //!< Warning mask
    uint16_t        num_sigs;                               //!< Number of digital signals for this value
    uint16_t        logic;                                  //!< Logic for combining multiple digital signals
    uint16_t        state;                                  //!< Signal state
    uint16_t        filter_count;                           //!< Filter counter
    uint16_t       *dim_bits_ptr[DIAG_DIG_MAX_SIGS];        //!< Pointer to the DIM channel containing the digital signal bit
    uint16_t        dim_bits_mask[DIAG_DIG_MAX_SIGS];       //!< Mask for the digital signal bit
};

struct Equipdev_channel_diag
{
    uint32_t        dims_expected[2];                       //!< DIAG.DIMS_EXPECTED property
    uint32_t        dims_detected[2];                       //!< DIAG.DIMS_DETECTED property
    uint32_t        dims_exp_errs;                          //!< DIAG.DIMS_EXP_ERRS property
    uint32_t        sync_resets;                            //!< DIAG.SYNC_RESETS property
    uint32_t        dim_sync_flts;                          //!< DIAG.DIM_SYNC_FLTS property
    uint32_t        vs_dim_idx;                             //!< DIM index of the DIM in the Voltage Source

    uint32_t        bus_address     [FGC_MAX_DIMS];         //!< DIAG.BUS_ADDRESSES property
    uint32_t        dim_num         [FGC_MAX_DIMS];
    uint32_t        trigus          [FGC_MAX_DIMS];         //!< DIAG.TRIGUS property

    uint16_t        warning_masks[FGC_MAX_DIMS][FGC_N_DIM_DIG_BANKS];   //!< Bit masks of digital DIM signals that should trigger DIM_WRN status when '1'
    uint16_t        warning_state[FGC_MAX_DIMS][FGC_N_DIM_DIG_BANKS];   //!< Snapshot of warning-relevant digital DIM signals

    float           gain            [FGC_N_DIAG_ANA_CHANS]; //!< DIAG.GAINS property
    float           offset          [FGC_N_DIAG_ANA_CHANS]; //!< DIAG.OFFSETS property
    float           anasigs         [FGC_N_DIAG_ANA_CHANS]; //!< DIAG.ANASIGS property

    uint16_t        data            [352];                  //!< DIAG.DATA property

    struct Equipdev_channel_diag_dig_sig    fw_diode;       //!< Free Wheel Diode fault
    struct Equipdev_channel_diag_dig_sig    fabort_unsafe;  //!< Fast Abort Unsafe fault
    struct Equipdev_channel_diag_dig_sig    thyr_unsafe;    //!< Crowbar thyristor unsafe fault
    struct Equipdev_channel_diag_dig_sig    subconv_lost;   //!< Sub-converter lost warning
};


//! FGC properties

struct Equipdev_channel_fgc
{
    // Dallas (DLS) properties

    const char     *id[FGC_ID_N_BRANCHES][OW_BUS_MAX_DEVS]; //!< FGC.ID.*
    const char     *id_summary [FGC_ID_N_BRANCHES];         //!< FGC.ID.SUMMARY

    uint16_t        dls_inhibit;                            //!< FGC.DLS.INHIBIT property
    uint32_t        dls_num_errors;                         //!< FGC.DLS.NUM_ERRORS property
    struct timeval  dls_error_time;                         //!< FGC.DLS.ERROR_TIME property

    uint32_t        cycle_period_raw;                       //!< FGClite raw CYCLE_PERIOD (from the critical status)
    uint32_t        cycle_period_filtered;                  //!< FGClite filtered CYCLE_PERIOD
    uint16_t        ms_period;                              //!< MS_PERIOD sent to the device
    uint16_t        ms_period_override;                     //!< Override value for MS_PERIOD (set to zero to calculate from CYCLE_PERIOD)

    float           psu[4];                                 //!< PSU_FGC and PSU_ANA containing the measurmenet for the +15V, -15V, +5V, and +3.3V

    struct
    {
        uint32_t    n_elements;
        uint8_t     type[FGC_MAX_SEU_EVENTS];
        uint32_t    tv_sec[FGC_MAX_SEU_EVENTS];
    } seu;
};


//! LIMITS properties

struct Equipdev_channel_limits
{
    float    i_access;                                      //!< LIMITS.I.ACCESS property. Maximum permitted current when FGC.SECTOR_ACCESS is ENABLED
    float    i_earth;                                       //!< LIMITS.I.EARTH property. Hardware trip level for the earth current
    float    i_hardware;                                    //!< LIMITS.I.HARDWARE property. Hardware trip level for the circuit current
    float    op_i_available;                                //!< LIMITS.I.AVAILABLE property. Available current. The maximum absolute current that the converter can produce at this moment.
};


//! LOG properties

struct Equipdev_channel_log
{
    struct LOG_menus                    menus;              //!< liblog menus
    struct LOG_mgr                      mgr;                //!< liblog manager
    struct LOG_read                     read;               //!< liblog readout
    struct LOG_read_control             read_control;       //!< liblog read control
    struct LOG_structs                  structs;            //!< liblog structures
    struct LOG_buffers                  buffers;            //!< liglog buffers
    struct timeval                      post_mortem_time;   //!< Post mortem time

    struct Equipdev_channel_debug_log                       //!< CCLIBS debug log
    {
        float reg_mode;
        float ref_state;
        float fg_type;
        float fg_status;
        float ramp_mgr_stat;
        float cyc_status;
        float cyc_sel;
        float iter_time_us;
        float iter_index_i;
        float debug_sig;
    } debug;

    cc_float     adc_i_a_1kHz       [21];                   //!< Page data for ADC_A
    cc_float     adc_i_b_1kHz       [21];                   //!< Page data for ADC_B
    cc_float     adc_v_meas_1kHz    [21];                   //!< Page data for ADC_C
    cc_float     dim                [4][8];                 //!< Page data for the analogue channels for one DIM

    uint16_t     pm_state;                                  //!< LOG.PM.STATE
    uint16_t     pm_trig;                                   //!< LOG.PM.TRIG
};


//! MEAS properties

struct Equipdev_channel_meas
{
    struct
    {
        int32_t                        i_a;                //!< Raw value for ADC channel A
        int32_t                        i_b;                //!< Raw value for ADC channel B
        int32_t                        v_meas;             //!< Raw value for ADC channel V_MEAS
    }                                  raw[2];             //!< Raw measurements. Element 0 is the average for milliseconds 0-9. Element 1 is the average for milliseconds 10-19.

    struct
    {
        cc_float                        i_a;                //!< Raw value for ADC channel A
        cc_float                        i_b;                //!< Raw value for ADC channel B
        cc_float                        v_meas;             //!< Raw value for ADC channel C
    }                                   raw_f;              //!< Raw measurements, converted into a float for logging with liblog

    uint32_t                            sim;                //!< MEAS.SIM property

    float                              *i_earth_ptr;        //!< Pointer to I_EARTH value in DIAG.ANASIGS
    float                               i_earth;            //!< MEAS.I_EARTH property. MEAS.I_EARTH_PCNT = MEAS.I_EARTH * 100 / LIMITS.I.EARTH.
    float                               max_i_earth;        //!< MEAS.MAX.I_EARTH property. Absolute maximum observed value of Equipdev_channel_meas::i_earth.

    float                              *u_leads_ptr [2];    //!< Pointer to U_LEADS values in DIAG.ANASIGS
    float                               u_leads_acc [2];    //!< Accumulator for U_LEADS measurement (MEAS.U_LEADS records the 1 second average)
    float                               u_leads     [2];    //!< MEAS.U_LEADS property. Positive and negative current lead voltages sampled from the DIM.
    float                               max_u_leads [2];    //!< MEAS.MAX.U_LEADS property. Absolute maximum observed values of positive and negative current lead voltages. See Equipdev_channel_meas::u_leads.
};


//! REF properties

struct Equipdev_channel_ref
{
    int16_t                       dac_val;                  //!< The value we will send to the DAC output register. FGClite has a 16-bit DAC.
    uint16_t                      event_group;              //!< REF.EVENT_GROUP.
    int32_t                       dac;                      //!< REF.DAC property. Direct DAC reference. Used to set DAC output register when STATE.OP = TEST.
    struct timeval                abort;                    //!< REF.ABORT property. Absolute time at which to abort the running change.
    float                         remaining;                //!< REF.REMAINING property. Readback of time remaining to run the reference function (secs).
    bool                          run_now;                  //!< Set for REF NOW to trigger execution 1s after entering ARMED state
};


//! REG properties

struct Equipdev_channel_reg
{
     float                        i_period [FGC_N_LOADS];   //!< REG.I.PERIOD property. Applied period for the operational current regulation algorithm (secs).
};


//! SWITCH.POLARITY

struct Equipdev_channel_polswitch
{
    // Polarity switch signals that are supplied to the polarity switch manager in libref

    enum CC_enabled_disabled positive;                      //!< Switch positive position status
    enum CC_enabled_disabled negative;                      //!< Switch negative position status
    enum CC_enabled_disabled fault;                         //!< Switch fault status - Not used by FGClite so always DISABLED.
    enum CC_enabled_disabled locked;                        //!< Switch locked status - Not used by FGClite so always DISABLED.
};


//! Temperature (TEMP) properties

struct Equipdev_channel_temp
{
    float                    fgc_in;                        //!< TEMP.FGC.IN property
    float                    fgc_out;                       //!< TEMP.FGC.OUT property
    float                    fgc_delta;                     //!< TEMP.FGC.DELTA property

    struct
    {
        float                head;                          //!< TEMP.{A,B}.DCCT.HEAD properties
        float                elec;                          //!< TEMP.{A,B}.DCCT.ELEC properties
    }                        dcct[2];                       //!< TEMP.{A,B}.DCCT properties
};


//! VS properties

struct Equipdev_channel_vs
{
    uint16_t                      state_counter;            //!< VS invalid state test counter
    uint16_t                      active_filter;            //!< VS.ACTIVE_FILTER property
    float                         vsrun_timeout;            //!< VS.VSRUN_TIMEOUT property
    float                         offset;                   //!< VS.OFFSET property
    uint16_t                      sim_intlks;               //!< VS.SIM_INTLKS property
};


//! PPM Channel properties

struct Equipdev_channel_ppm                                 //!< PPM variables
{
    uintptr_t prop_num_els[EQUIPDEV_MAX_PROPS];             //!< Numbers of elements for device properties

    struct REF_ctrl_pars ctrl;                              //!< REF.FUNC.PLAY and REF.ECONOMY.DYN_END_TIME - these not transactional propertie

    // Transactional settings support

    uint16_t                        transaction_id;                 //!< Transaction ID for transactional settings
    uint16_t                        transaction_state;              //!< State of a transaction
    struct REF_transaction_pars     transaction;                    //!< TRANSACTION LAST_FG_PAR_INDEX - this is not a property but is used

    struct Equipdev_ppm_transactional
    {
        struct REF_ref_pars         ref;                    //!< REF.FUNC.TYPE
        struct REF_ramp_pars        ramp;                   //!< REF.RAMP.*
        struct REF_pulse_pars       pulse;                  //!< REF.PULSE.*
        struct REF_plep_pars        plep;                   //!< REF.PLEP.*
        struct REF_pppl_pars        pppl;                   //!< REF.PPPL.*
        struct REF_cubexp_pars      cubexp;                 //!< REF.CUBEXP.*
        struct REF_trim_pars        trim;                   //!< REF.TRIM.*
        struct REF_test_pars        test;                   //!< REF.TEST.*
        struct REF_prbs_pars        prbs;                   //!< REF.PRBS.*

        struct REF_table_pars
        {
            FG_point             function[FGC_TABLE_LEN];   //!< REF.TABLE.FUNC.VALUE
            uintptr_t            num_elements;              //!< Number of elements in REF.TABLE.FUNC.VALUE
        } table;
    } transactional, transactional_backup;                  //!< PPM transactional settings and a backup to allow rollback
};


//! All properties and data for a single FGClite device

struct Equipdev_channel
{
    struct FGCD_device                 *fgcd_device;        //!< Pointer to FGCD device structure

    // Configured flag

    bool                                is_configured;      //!< Device is configured flag

    // System DB

    uint16_t                            sysdb_idx;          //!< SysDB index
    uint16_t                            default_sysdb_idx;  //!< SysDB index for default system (RPAFL) - for internal DIM with FGC_PSU analog channels
    bool                                mask_NO_VS_CABLE;   //!< Work-around for issue EPCCCS-8395

    // Device interlocks

    uint16_t                            sector_access;      //!< Sector access flag copied from the GW

    // CCLIBS data structures

    struct REG_mgr                      reg_mgr;            //!< Regulation manager data for this device
    struct REG_pars                     reg_pars;           //!< Regulation parameters for this device
    struct REF_mgr                      ref_mgr;            //!< Reference manager data for this device
    struct POLSWITCH_mgr                polswitch_mgr;      //!< Polarity switch manager data - required by libref even though it is not used
    struct SIG_struct                   sig_struct;         //!< Signal data for this device
    struct SIG_dac                      dac;                //!< Digital/Analogue Converter
    struct EVTLOG_data                  evtlog_data;        //!< Allocation of memory for device data, prop_data, last_evt and records
    struct LIBREF_vars                  libref_vars;        //!< Variables associated with libref integration
    pthread_mutex_t                     reg_pars_mutex;     //!< Mutex to protect regParsCheck() and regParsProcess() from being run simultaneously
    pthread_mutex_t                     arm_event_mutex;    //!< Mutex to protect refArm() and refEventStartFunc() from being run simultaneously
    pthread_mutex_t                     evtlog_mutex;       //!< Mutex to protect event log callbacks and to lock resources
    bool                                arming_enabled;     //!< Arming is disabled until all config is loaded from nonvol

    // Parser

    Parser                              parser;

    // Publication

    struct fgc92_stat                   status;             //!< Published status. Used by STATUS, STATE and PC properties.

    // ADC properties

    struct Equipdev_channel_adc         adc;                //!< ADC properties for this device

    // BARCODE properties

    struct Equipdev_channel_barcode     barcode;            //!< BARCODE properties for this device

    // CONFIG properties

    time_t                              config_reset_time;  //!< Time of the last S CONFIG.STATE RESET
    uint16_t                            config_mode;        //!< Configuration mode
    uint16_t                            config_state;       //!< Configuration state
    bool                                config_read;        //!< Set to true to trigger reading of config from NVS files

    // CRATE properties

    uint16_t                            crate_type;         //!< CRATE.TYPE property. Type of electronics crate in which the FGClite is inserted.
    uint16_t                            crate_position;     //!< CRATE.POSITION property. Position of the FGClite in the electronics crate (for crates with two slots).

    // CAL properties

    struct Equipdev_channel_cal         cal;                //!< CAL properties for this device

    // DCCT properties

    struct Equipdev_channel_dcct        dcct;               //!< DCCT properties for this device

    // DEVICE properties

    struct Equipdev_channel_device      device;             //!< DEVICE properties for this device

    // DIAG properties

    struct Equipdev_channel_diag        diag;               //!< DIAG properties for this device

    // DIG properties

    struct Digital_IO                   dig;                //!< Digital commands and status

    // FGC properties

    struct Equipdev_channel_fgc         fgc;                //!< FGC properties for this device

    // LIMITS properties

    struct Equipdev_channel_limits      limits;             //!< LIMITS properties for this device

    // LOG properties

    struct Equipdev_channel_log         log;                //!< LOG properties for this device

    // MEAS properties

    struct Equipdev_channel_meas        meas;               //!< MEAS properties for this device

    // MODE properties

    uint16_t                            mode_op;            //!< MODE.OP. Operational mode requested by client
    uint16_t                            mode_pc;            //!< MODE.PC. Power Converter mode requested by client
    uint16_t                            mode_pc_prev;       //!< Previous MODE.PC. Used to notify CMW when MODE.PC changes.
    uint16_t                            mode_pc_on;         //!< LOG.MENUS.MODE_PC_ON. Default Power Converter mode (IDLE for Class 92)

    // Real-time reference

    REG_mode                            rt_reg_mode;        //!< Regulation mode in which we apply real-time updates
    uint32_t                            rt_num_steps;       //!< Number of steps with which to interpolate real-time updates
    uint32_t                            coast;              //!< Flag to indicate whether the device will respond to coast and recover timing events

    // REF properties

    struct Equipdev_channel_ref         ref;                //!< REF properties for this device

    // REG properties

    struct Equipdev_channel_reg         reg;                //!< REG properties for this device that are not part of libreg

    // STATE properties

    uint64_t                            state_pc_time_ms;   //!< Time since the last STATE.PC change (ms)
    uint32_t                            state_pc_bit_mask;  //!< STATE.PC as a bit mask (only 1 bit set at a time)
    State                             * state_pc_func;      //!< Pointer to STATE.PC state machine function
    State                             * state_op_func;      //!< Pointer to STATE.OP state machine function

    // SWITCH properties

    struct Equipdev_channel_polswitch   polswitch;          //!< SWITCH.POLARITY properties for this device

    // TEMP properties

    struct Equipdev_channel_temp        temp;               //!< Temperature properties for this device

    // TEST properties

    struct Equipdev_channel_test        test;               //!< TEST properties for this device

    // VS properties

    struct Equipdev_channel_vs          vs;                 //!< VS properties for this device

    // PPM variables must be within the following structure

    struct Equipdev_channel_ppm ppm[NUM_CYC_SELECTORS];     //<! PPM properties
};



//! Struct containing global variables


struct Equipdev
{
    pthread_t                thread;                        //!< Thread ID for Equipdev thread

    uint8_t                  fgc_platform;                  //!< Platform number
    const char              *fgc_platform_name;             //!< Platform name
    const char              *fgc_class_name;                //!< Class name
    struct hash_table       *prop_hash;                     //!< Hash of properties
    struct hash_table       *const_hash;                    //!< Hash of constants

    bool                     gw_pc_permit;                  //!< PC_PERMIT interlock signal from the timing system/GW properties

    struct Equipdev_channel  device   [FGCD_MAX_DEVS];      //!< Data for equipment devices
    uint32_t                 deadline [FGCD_MAX_DEVS + 1];  //!< Deadline in ns within each cycle to receive the status packet
                                                            //!< for all FGClite devices + the diagnostic device

    //! We can't set the INDIRECT_N_ELS flag on the DIAG.DIM_NAMES and DIAG.FAULTS properties, as this
    //! breaks FGC2 and FGC3. The workaround is that each device maintains its own list of DIMs, but
    //! we have only one instance of the pointers to the properties for all equipment devices. When
    //! the GetDim() function, is called, we change these pointers to point to the correct device.
    //! This workaround is possible because FGCD commands are processed strictly one-at-a-time.
    //!
    //! As these data structures are outside the equipdev.device struct above, the FGCD parser treats
    //! them as having only one value, instead of one value per device.


    const char              *dim_names [FGC_MAX_DIMS];           //!< DIAG.DIM_NAMES property

    //! NOT USED. FGClite does not support PPM, but the ppm field must exist as parserConfigProp (in parser.c)
    //! does a pointer comparison between property->value and the address of this field to determine if the
    //! property is PPM or not. See EPCCCS-2379.


    uint32_t ppm[2];
};

extern struct Equipdev equipdev;


// External function declarations

//! Initialise equipment devices and start the "non-real time" equipment device thread

int32_t equipdevStart(void);


//! Initialise data for devices.
//!
//! This function is called each time the device names are read.

void equipdevInitDevices(void);


//! Publish data for equipment devices

void equipdevPublish(void);


// Static inline functions

//! Called from nonvol.cpp if a config property is changed.

static inline void equipdevConfigUnsync(uint32_t channel)
{
    equipdev.device[channel].config_state = FGC_CFG_STATE_UNSYNCED;
}


//! Returns TRUE if PC_PERMIT is enabled for this device
//!
//! Returns the logical AND of PC_PERMIT on the digital interface and PC_PERMIT in the gateway.
//! If VS.SIM_INTLKS is enabled for the device, this function always return true.
//!
//! @param[in] channel           FIP address of the FGClite device
//!
//! @retval    true              PC_PERMIT is enabled in the device and in the gateway,
//!                              or simulated interlocks is enabled for the device
//!            false             At least one of the above conditions is not true

bool equipdevIsPcPermit(uint32_t channel);


//! Look up a property and set the number of elements.
//!
//! This function allows us to set the number of elements for the FGC.ID.* properties.
//! This cannot be done in the normal way as these properties have no SET function, so
//! this function is a bit of a hack to get around this limitation of the property model.
//!
//! It is also used to initialise Calibration properties on start-up, if they have no
//! value set in non-volatile storage.
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        bus_id          If the property name contains a '?', it will be replaced
//!                                   with this parameter. (Usually set to 'A' or 'B').
//! @param[in]        prop_label      Name of the property to update
//! @param[in]        num_els         Number of elements to set

void equipdevPropSetNumEls(uint32_t channel, char bus_id, const char *prop_label, uint32_t num_els);


//! Look up a property and set its value
//!
//! For properties which have no SET function, we have to dig into the innards of the property model
//! to set their value. The main use case is setting the BARCODE.* properties from their text labels
//! in the CompDb.
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        bus_id          If the property name contains a '?', it will be replaced
//!                                   with this parameter. (Usually set to 'A' or 'B').
//! @param[in]        prop_label      Name of the property to update
//! @param[in]        prop_value      Value to assign to the property

void equipdevPropSetString(uint32_t channel, char bus_id, const char *prop_label, const char *prop_value);


//! Get a pointer to the property for a temperature reading
//!
//! @param[in]        channel         FIP address of the FGClite device
//! @param[in]        bus_id          If the property name contains a '?', it will be replaced
//!                                   with this parameter. (Usually set to 'A' or 'B').
//! @param[in]        prop_label      Name of the property to update
//!
//! @returns          Pointer to the property, or NULL if it is invalid (property not found or is not
//!                   a float)

float *equipdevPropGetTempPtr(uint32_t channel, char bus_id, const char *prop_label);

#endif

// EOF
