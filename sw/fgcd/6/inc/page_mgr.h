//! @file   page_mgr.h
//! @brief  Declarations for managing FGClite paged memory

#ifndef PAGE_MGR_H
#define PAGE_MGR_H

#include <stdint.h>
#include <sys/time.h>

// forward declaration

struct FGClite_status;
struct FGClite_command;
// Reset the Paged Memory Manager.
//
// The reset will initiate a scan of the Dallas bus after 10 seconds. The 10-second delay is to
// allow the FIP timing to settle.
//
// @param[in] channel       FIP address of the FGClite device
// @param[in] start_time    Time at which the device was last reset. In the case of a hard reset,
//                          this time can be a few seconds in the future.

void pageMgrReset(uint32_t channel, const struct timeval *start_time);

//! Invalidate the paged data in the next cycle.
//!
//! This function should be called in cycles where a valid status was not received.
//!
//! @param[in] channel     FIP address of the FGClite device

void pageMgrInvalidate(uint32_t channel);

//! Put the Paged Memory Manager into Post Mortem logging state.
//!
//! This function should be called when a self-triggered PM event occurs.
//!
//! @param[in] channel          FIP address of the FGClite device

void pageMgrPMTrigger(uint32_t channel);

//! Return the Paged Memory Manager to normal logging state
//!
//! This function should be called after the Post Mortem Dump has completed.
//!
//! @param[in] channel     FIP address of the FGClite device

void pageMgrPMReset(uint32_t channel);

//! Handle paged memory requests.
//!
//! This function should only be called in cycles where a valid status has been received.
//!
//! @param[in] channel     FIP address of the FGClite device
//! @param[in] status      Pointer to critical and paged data for the device
//! @param[in] ctrl_cmd    Pointer to CONTROLLER_CMD for the device

void pageMgrUpdate(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd);

#endif

// EOF
