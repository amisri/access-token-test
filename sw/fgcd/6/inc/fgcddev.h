//! @file   fgcddev.h
//! @brief  Declarations for the FGCD device

#ifndef FGCDDEV_H
#define FGCDDEV_H

#include <stdint.h>
#include <consts.h>
#include <fgc_codes_gen.h>
#include <fgc_consts_gen.h>
#include <fgc_stat.h>
#include <devname.h>
#include <parser.h>

// Forward declarations

struct FGCD_device;
struct hash_table;

// Types

//! Enumeration of array elements for FIELDBUS.EVENT property

enum
{
   EVENT_TYPE,
   EVENT_TRIGGER_TYPE,
   EVENT_PAYLOAD,
   EVENT_DELAY_MS,
   EVENT_BUF_SIZE
};



//! Packed version of fgc_code_info.
//!
//! This is the format of the last 30 bytes of FGCDdev::fgc_code

struct __attribute__((__packed__)) FGC_code_info
{
    char                  version_string[FGC_CODE_VERSION_STRING_LEN];                   //!< Version local time string
    uint32_t              version;                                                       //!< Version (unixtime)
    uint8_t               class_id;                                                      //!< FGC class ID
    uint8_t               code_id;                                                       //!< Code ID
    uint16_t              old_version;                                                   //!< Version used for the old versioning system
    uint16_t              crc;                                                           //!< CRC16
};



//! Struct containing global variables

struct FGCDdev
{
    struct FGCD_device   *device;                                                        //!< Pointer to device structure

    struct fgc6_stat      status;                                                        //!< Published data
    struct Parser         parser;                                                        //!< Parser
    struct hash_table    *const_hash;                                                    //!< Hash of constants
    struct hash_table    *prop_hash;                                                     //!< Hash of properties

    const char           *fgc_class_name;                                                //!< Class name
    const char           *fgc_platform_name;                                             //!< Platform name
    uint8_t               fgc_platform;                                                  //!< Platform

    // FIELDBUS properties

    uint16_t              event_buf   [EVENT_BUF_SIZE];                                  //!< Buffer for FIELDBUS.EVENT property

    // TEST properties

    uint8_t               test_bin    [64];                                              //!< TEST.BIN property
    char                  test_char   [64];                                              //!< TEST.CHAR property
    float                 test_float  [16];                                              //!< TEST.FLOAT property
    int8_t                test_int8s  [64];                                              //!< TEST.INT8S property
    uint8_t               test_int8u  [64];                                              //!< TEST.INT8U property
    int16_t               test_int16s [32];                                              //!< TEST.INT16S property
    uint16_t              test_int16u [32];                                              //!< TEST.INT16U property
    int32_t               test_int32s [16];                                              //!< TEST.INT32S property
    uint32_t              test_int32u [16];                                              //!< TEST.INT32U property
    const char           *test_string [2];                                               //!< TEST.STRING property

    // FGC codes

    char                  fgc_code               [FGC_CODE_NUM_SLOTS][FGC_CODE_SIZE];    //!< Code data
    char                  fgc_code_name          [FGC_CODE_NUM_SLOTS][32];               //!< Names of files from which codes were loaded
    char                 *fgc_code_name_property [FGC_CODE_NUM_SLOTS];                   //!< Pointers to access code names as a string property
    uintptr_t             fgc_code_length        [FGC_CODE_NUM_SLOTS];                   //!< Code lengths
    uint8_t               fgc_code_class         [FGC_CODE_NUM_SLOTS];                   //!< Code classes
    uint8_t               fgc_code_id            [FGC_CODE_NUM_SLOTS];                   //!< Code IDs
    uint16_t              fgc_code_chksum        [FGC_CODE_NUM_SLOTS];                   //!< Code checksums
    uint32_t              fgc_code_version       [FGC_CODE_NUM_SLOTS];                   //!< Code versions
    uint16_t              fgc_code_old_version   [FGC_CODE_NUM_SLOTS];                   //!< Code versions using the old version system
};

extern struct FGCDdev fgcddev;


// Function declarations

//! Start the device

int32_t fgcddevStart(void);


//! Initialise data for device
//!
//! This function does not do anything

void fgcddevInitDevice(void);


//! Publish data for FGCD device

void fgcddevPublish(void);


//! Read FGC codes from files
//!
//! @retval    0   The function completed successfully
//! @retval    1   An error occurred

int32_t fgcddevReadCodes(void);


//! Set info field values after setting FGC codes using the property interface
//!
//! @param[in]    code    Pointer to the start of the codes that were set

void fgcddevSetFGCCodeInfo(char *code);


//! Returns a pointer to the specified FGC_DB code
//!
//! @param[in]    id    ID of the code to return
//!
//! @returns      Pointer to the fgc_code corresponding to ID, or NULL if not found

char *fgcddevGetCodePtr(enum fgc_code_ids id);

#endif

// EOF
