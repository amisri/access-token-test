#! /usr/bin/bash

# config file migration script for Class 6/92

migrate()
{
    echo Migrating $fgc

#    rm -f $cfg/*/DEVICE.CYC $cfg/*/*SMOOTH_ACTUATION

#    head $cfg/../log | grep "SERIAL \$\$43 proto error" |  wc

#    for fgc in $cfg/*
#    do


#        echo  "1.0000000E+03" > $fgc/VS.SIM.BANDWIDTH
#        echo  "1.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00" > $fgc/VS.SIM.DEN
#        echo  "1.0000000E+00,0.0000000E+00,0.0000000E+00,0.0000000E+00" > $fgc/VS.SIM.NUM
#        echo  "8.9999998E-01" > $fgc/VS.SIM.Z

#        mv -f $fgc/LIMITS.I.MIN $fgc/LIMITS.I.STANDBY
#        mv -f $fgc/REG.MODE_INIT $fgc/REG.MODE_CYC
#        cp $fgc/REF.DEFAULTS.I.ACCELERATION $fgc/REF.DEFAULTS.I.DECELERATION
#        cp $fgc/REF.DEFAULTS.V.ACCELERATION $fgc/REF.DEFAULTS.V.DECELERATION
#
#        # Get list of all config property files for this device
#
#        cd $fgc
#        local files=$(ls)
#        cd $root
#
#        # Run script to check that all DONT_SHRINK CONFIG properties have the correct number of values
#
#        awk -f dont_shrink_props.awk $fgc $files

#    done
}

cd $(dirname $0)
root=$(pwd)
fecs=$(gawk -F : '//{if($3==6)print $1}' ~pclhc/etc/fgcd/name | sort)

source ../../scripts/config_common.sh

exit 0

# EOF
