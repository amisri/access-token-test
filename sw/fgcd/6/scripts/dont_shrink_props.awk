#!/usr/bin/awk -f
#
# dont_shrink_config.awk property [property...]
#
# The script will read each property file and check that the number of values is sufficient.
# If not, it will repeat the last value to make up the full set.

BEGIN {

    prop["LIMITS.I.POS"] =                       4
    prop["LIMITS.I.STANDBY"] =                   4
    prop["LIMITS.I.NEG"] =                       4
    prop["LIMITS.I.RATE"] =                      4
    prop["LIMITS.I.CLOSELOOP"] =                 4
    prop["LIMITS.I.LOW"] =                       4
    prop["LIMITS.I.ZERO"] =                      4
    prop["LIMITS.I.ERR_WARNING"] =               4
    prop["LIMITS.I.ERR_FAULT"] =                 4
    prop["LIMITS.I.QUADRANTS41"] =               2
    prop["LIMITS.V.POS"] =                       4
    prop["LIMITS.V.NEG"] =                       4
    prop["LIMITS.V.QUADRANTS41"] =               2
    prop["LOAD.OHMS_SER"] =                      4
    prop["LOAD.OHMS_PAR"] =                      4
    prop["LOAD.OHMS_MAG"] =                      4
    prop["LOAD.HENRYS"] =                        4
    prop["LOAD.HENRYS_SAT"] =                    4
    prop["LOAD.I_SAT_START"] =                   4
    prop["LOAD.I_SAT_END"] =                     4
    prop["REF.DEFAULTS.I.ACCELERATION"] =        4
    prop["REF.DEFAULTS.I.DECELERATION"] =        4
    prop["REF.DEFAULTS.I.LINEAR_RATE"] =         4
    prop["REF.DEFAULTS.V.ACCELERATION"] =        4
    prop["REF.DEFAULTS.V.DECELERATION"] =        4
    prop["REF.DEFAULTS.V.LINEAR_RATE"] =         4
    prop["REG.I.PERIOD_ITERS"] =                 4
    prop["REG.I.EXTERNAL_ALG"] =                 4
    prop["REG.I.INTERNAL.MEAS_SELECT"] =         4
    prop["REG.I.INTERNAL.PURE_DELAY_PERIODS"] =  4
    prop["REG.I.INTERNAL.AUXPOLE1_HZ"] =         4
    prop["REG.I.INTERNAL.AUXPOLES2_HZ"] =        4
    prop["REG.I.INTERNAL.AUXPOLES2_Z"] =         4
    prop["REG.I.EXTERNAL.MEAS_SELECT"] =         4
    prop["REG.I.EXTERNAL.CLBW"] =                4
    prop["REG.I.EXTERNAL.Z"] =                   4
    prop["REG.I.EXTERNAL.MOD_MARGIN"] =          4
    prop["REG.I.EXTERNAL.TRACK_DELAY_PERIODS"] = 4
    prop["REG.I.EXTERNAL.OP.R"] =                16
    prop["REG.I.EXTERNAL.OP.S"] =                16
    prop["REG.I.EXTERNAL.OP.T"] =                16

    FS = ","

    path = ARGV[1]

    printf " ARGC=%u  PATH=%s\n", ARGC, path


    for(i=2 ; i <= ARGC ; i++)
    {
        process(ARGV[i])
    }

    exit 0
}



function process(property,  num_values, filename, j)
{
    if(property in prop)
    {
        num_values = prop[property]

        filename = path "/" property

        getline < filename

#        printf "Trying to read a line from %s NF=%u\n",filename,NF

        if(NF > num_values)
        {
            close(filename)

            printf "Property %s has %u values - expected %u - skipping\n",property,NF,num_values
            return
        }
        else if(NF < num_values)
        {
            printf "Property %s has %u values - expected %u - extending\n",property,NF,num_values

            printf "%s", $0 > filename

            for(j=NF ; j < num_values ; j++) printf ",%s", $NF > filename
        }

        close(filename)
    }
}

# EOF
