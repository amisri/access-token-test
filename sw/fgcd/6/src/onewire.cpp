/*
 * @file   onewire.cpp
 * @brief  FGClite Dallas One-Wire Bus functions
 * @author Michael Davis
 */

//#define DEBUG

#ifdef DEBUG
#include <cstdio>
#endif
#include <cstring>
#include <bitset>

#include <limits>

#include <codes.h>
#include <equipdev.h>
#include <fgc/fgc_db.h>
#include <fgclite_cmd.h>
#include <timing.h>
#include <logging.h>
#include <onewire.h>



// Constants

const uint32_t OW_PAGE_MAX          =   3;                                           // Maximum page number for One-Wire Data (0..3)
const uint32_t OW_DEVS_PER_PAGE     =   4;                                           // 4 devices per page
const uint32_t OW_PAGES_PER_BUS     =   4;                                           // 4 pages per bus = maximum 16 devices per bus

/*
 * Length of the char* buffer for FGC.ID.* properties. Each row consists of the following elements:
 *
 * Element                 Size
 *
 * Dallas_ID:              16+1     =  17
 * Raw_temp:               2+1+2+1  =   6
 * Barcode___-Barcode=     13+6+1   =  20
 * Description                      =  80
 * Terminating Null                 =   1
 *
 * Total                            = 124
 */

const uint32_t OW_FGC_ID_LEN        = 124;



// Types

/*
 * One-wire states
 */

enum State_OneWire
{
    IL = 0,    // IDLE
    ST,        // STARTING
    SC,        // SCANNING
    RD,        // READING
    TS,        // TEMP_SCANNING
    TR         // TEMP_READING
};



/*
 * State of One-Wire Bus
 */

struct OW_state
{
    bool                          start_scan;                                        // Flag to start a new 1-wire scan
    bool                          start_temp_scan;                                   // Flag to start a new scan of devices with a digital thermometer
    enum State_OneWire            state;                                             // Current 1-wire bus state

    bool                          is_busy;                                           // Is 1-wire bus busy?
    bool                          is_scan_complete;                                  // Have we completed a full scan of all the 1-wire buses?
    bool                          is_prop_updated;                                   // Are the BARCODE and FGC.ID.* properties up-to-date?
    bool                          ctrl_cmd_set;                                      // Has the CTRL_CMD been set this cycle?

    uint8_t                       bus_no;                                            // Number of the 1-wire bus to scan
    uint8_t                       page_no;                                           // Number of the page to read back

    // Fields used only for the partial scan (temp sensors only)

    bool                          is_page_request;                                   // A page request was sent in the last cycle
    uint8_t                       requested_bus_no;                                  // Number of the last requested 1-wire bus
    uint8_t                       requested_page_no;                                 // Number of the last requested page
};



/*
 * Struct for reading Dallas IDs and temperatures from the One-Wire Bus pages
 */

struct __attribute__((__packed__)) FGClite_onewire
{
    uint8_t                       bus_dev_id;                                        //  4-bit Dallas bus ID (0-4) +
                                                                                     //  4-bit Dallas device ID (0-15)
    Dallas_id                     dallas_id;                                         // 64-bit Dallas ID
    uint8_t                       raw_temp[2];                                       // 16-bit Temperature raw value

    uint8_t                       reserved[5];                                       // 40 bits reserved (unused)
};



/*
 * Struct to store page responses from the Dallas One-Wire Bus scan
 */

struct __attribute__((__packed__)) FGClite_onewire_bus
{
    struct FGClite_onewire        id      [OW_BUS_MAX_DEVS];                         // Scanned IDs and temperatures for all devices on a single One-Wire Bus
};



/*
 * Struct to store Dallas ID to Barcode mapping.
 */

struct OW_Dallas_Barcode
{
    char                          barcode [FGC_ID_BARCODE_LEN];                      // Barcode for this Dallas ID
    char                          desc    [OW_FGC_ID_LEN];                           // Long description (Dallas ID:Raw temp:Barcode=Component description)
    float                        *temp_ptr;                                          // Pointer to property to store temperature in Celsius for this Dallas ID
};



/*
 * One-Wire Bus data
 */

struct OW_bus
{
    struct OW_state               ow_state;                                          // State of the One-Wire Bus

    uint8_t                       bus_temp_sensor_mask;                              // Bitmask of which buses contain temperature sensors
    uint32_t                      page_temp_sensor_mask;                             // Bitmask of which pages contain temperature sensors

    struct FGClite_onewire_bus    ow_bus[OW_BUS_MAX+1];                              // Stored results of the One-Wire Bus scan (Dallas IDs and temperatures)

    char                          ow_bus_summary   [OW_BUS_MAX+1][80];               // Storage for FGC.ID.SUMMARY
    struct OW_Dallas_Barcode      ow_dallas_barcode[OW_BUS_MAX+1][OW_BUS_MAX_DEVS];  // Storage for FGC.ID.{VS,MEAS}.{A,B} and FGC.ID.LOCAL

    uint8_t                       num_els          [OW_BUS_MAX+1];                   // Number of devices on each bus
    uint8_t                       sort_order       [OW_BUS_MAX+1][OW_BUS_MAX_DEVS];  // Sort order for devices on each bus (sorted in lexicographic order of barcodes)
};



// Global variables, zero initialised

struct OW_bus ow_bus[FGCD_MAX_EQP_DEVS] = {};



// Types for state and transition functions

typedef enum State_OneWire       StateFunc(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call);
typedef StateFunc*               TransFunc(uint32_t channel);



// Declare static state and transition functions (to ensure type consistency)

static StateFunc stateOwIL;
static StateFunc stateOwST;
static StateFunc stateOwSC;
static StateFunc stateOwRD;
static StateFunc stateOwTS;
static StateFunc stateOwTR;

static TransFunc XXtoST;
static TransFunc STtoSC;
static TransFunc SCtoRD;
static TransFunc RDtoSC;
static TransFunc RDtoIL;
static TransFunc ILtoTS;
static TransFunc TStoTR;
static TransFunc TRtoTS;
static TransFunc TRtoIL;



// Constants

/*
 * Mapping of state enums to state functions
 */

static const StateFunc *run_state [] =
{
    /* IL */ stateOwIL,
    /* ST */ stateOwST,
    /* SC */ stateOwSC,
    /* RD */ stateOwRD,
    /* TS */ stateOwTS,
    /* TR */ stateOwTR
};



/*
 * Transition functions : called in priority order, from left to right
 */

static const TransFunc* Transitions [][4] =
{
    /* IL */ { XXtoST,                 ILtoTS,                 NULL },
    /* ST */ {         STtoSC,                                 NULL },
    /* SC */ { XXtoST,         SCtoRD,                         NULL },
    /* RD */ { XXtoST, RDtoSC,                         RDtoIL, NULL },
    /* TS */ { XXtoST,                         TStoTR,         NULL },
    /* TR */ { XXtoST,                 TRtoTS,         TRtoIL, NULL }
};



// Static function definitions

/*
 * Convert temperature raw values into degrees Celsius and store the result as a float.
 *
 * Implementation based on the datasheet for the Maxim DS18B20 1-Wire Digital Thermometer.
 *
 * The operating temperature range is -55oC to +125oC and is accurate to +/-0.5oC across
 * the nominal range. Default configuration is 12-bit resolution, which gives increments
 * of 0.0625oC. The temperature register is a 16-bit sign-extended two's complement number.
 * The data is little-endian.
 *
 * @param[in]    temp_ptr     Pointer to temperature property associated with the Dallas ID
 * @param[in]    raw_value    Two-byte array in big-endian order
 *
 * @retval       true         The raw value was converted into a valid temperature
 * @retval       false        No temperature conversion was performed; either temp_ptr was
 *                            NULL or the raw value was invalid
 */

static void onewireTempFloat(float* const temp_ptr, const uint8_t *raw_value)
{
    assert(temp_ptr);

    // The 5 high-order bits contain the sign and they must all be the same value.

    uint8_t sign = raw_value[0] & 0xF8;

    if(sign != 0 && sign != 0xF8)
    {
        // If the raw value is invalid, set the value to FLOAT_MIN and return false

        *temp_ptr = 0.0;
    }
    else
    {
        // Convert the raw value to float

        int16_t t_raw = (raw_value[0] << 8) + raw_value[1];

        *temp_ptr = static_cast<float>(t_raw) / 16;
    }
}



static void onewireTempProp(uint32_t channel, float* const temp_ptr)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Update the ADC.INTERNAL.TEMPERATURE and DCCT.{A,B}.TEMPERATURE properties

    if(temp_ptr == &device.temp.fgc_in || temp_ptr == &device.temp.fgc_out)
    {
        // Update TEMP.FGC_DELTA

        device.temp.fgc_delta = device.temp.fgc_out - device.temp.fgc_in;

        if(temp_ptr == &device.temp.fgc_in)
        {
            // Update the ADC.INTERNAL.TEMPERATURE property

            sigTempMeas(&device.sig_struct.temp_filter.named.internal, device.temp.fgc_in);

            // If crate type uses RITZ DCCTs, also use the FGC temperature for DCCTs

            if(device.crate_type == FGC_CRATE_TYPE_PC_60A || device.crate_type == FGC_CRATE_TYPE_PC_120A)
            {
                sigTempMeas(&device.sig_struct.temp_filter.named.dcct_a, device.temp.fgc_in);
                sigTempMeas(&device.sig_struct.temp_filter.named.dcct_b, device.temp.fgc_in);
            }
        }
    }
    else if(temp_ptr == &device.temp.dcct[0].elec)
    {
        // Update DCCT.A.TEMPERATURE for 600A/kA converters

        sigTempMeas(&device.sig_struct.temp_filter.named.dcct_a, device.temp.dcct[0].elec);
    }
    else if(temp_ptr == &device.temp.dcct[1].elec)
    {
        // Update DCCT.B.TEMPERATURE for 600A/kA converters

        sigTempMeas(&device.sig_struct.temp_filter.named.dcct_b, device.temp.dcct[1].elec);
    }
}



/*
 * Add a new barcode to the BARCODE.BUS.* property
 */

static void onewireBarcodeBusAdd(uint32_t channel, enum OW_buses bus, const char *barcode)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Barcode text in lower case is an error message, don't add it

    if(!isupper(barcode[0])) return;

    // Add the barcode to the appropriate bus

    switch(bus)
    {
        case OW_BUS_VS_A:
        case OW_BUS_VS_B:
            if(device.barcode.num_els.bus_V < FGC_MAX_V_BARCODES)
            {
                device.barcode.bus_V[device.barcode.num_els.bus_V++] = barcode;
            }
            else
            {
                logPrintf(channel, "ONEWIRE Exceeded maximum no. of barcodes (%d) on VS Bus\n", FGC_MAX_V_BARCODES);

                latchStatusBit(&device.status.st_latched, FGC_LAT_DALLAS_FLT, true);
            }
            break;

        case OW_BUS_MEAS_A:
            if(device.barcode.num_els.bus_A < FGC_MAX_A_BARCODES)
            {
                device.barcode.bus_A[device.barcode.num_els.bus_A++] = barcode;
            }
            else
            {
                logPrintf(channel, "ONEWIRE Exceeded maximum no. of barcodes (%d) on MEAS_A Bus\n", FGC_MAX_A_BARCODES);

                latchStatusBit(&device.status.st_latched, FGC_LAT_DALLAS_FLT, true);
            }
            break;

        case OW_BUS_MEAS_B:
            if(device.barcode.num_els.bus_B < FGC_MAX_B_BARCODES)
            {
                device.barcode.bus_B[device.barcode.num_els.bus_B++] = barcode;
            }
            else
            {
                logPrintf(channel, "ONEWIRE Exceeded maximum no. of barcodes (%d) on MEAS_B Bus\n", FGC_MAX_B_BARCODES);

                latchStatusBit(&device.status.st_latched, FGC_LAT_DALLAS_FLT, true);
            }
            break;

        case OW_BUS_LOCAL:
            if(device.barcode.num_els.bus_L < FGC_MAX_L_BARCODES)
            {
                device.barcode.bus_L[device.barcode.num_els.bus_L++] = barcode;
            }
            else
            {
                logPrintf(channel, "ONEWIRE Exceeded maximum no. of barcodes (%d) on LOCAL Bus\n", FGC_MAX_L_BARCODES);

                latchStatusBit(&device.status.st_latched, FGC_LAT_DALLAS_FLT, true);
            }
    }
}



/*
 * Sort the barcodes on each bus into lexicographic order
 */

static void onewireBarcodeSort(uint32_t channel, enum OW_buses bus, const char *new_barcode)
{
    uint8_t  &num_els    = ow_bus[channel-1].num_els[bus];
    uint8_t*  sort_order = ow_bus[channel-1].sort_order[bus];

    uint32_t i;

    // Linear search of barcodes to find the insertion point

    for(i = 0; i < num_els; ++i)
    {
        if(strncmp(new_barcode, ow_bus[channel-1].ow_dallas_barcode[bus][sort_order[i]].barcode, FGC_ID_BARCODE_LEN) < 0) break;
    }

    // Shunt up all barcodes above the insertion point

    for(uint32_t j = num_els; j > i; --j)
    {
        sort_order[j] = sort_order[j-1];
    }

    // Insert the new barcode

    sort_order[i] = num_els++;
}



/*
 * Check the data integrity of the 1-wire page read
 */

static bool onewireCheckPage(uint32_t channel, uint8_t id, uint8_t ow_bus_no, uint8_t ow_device_no)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if((id >> 4) != ow_bus_no || (id & 0x0F) != ow_device_no)
    {
        logPrintf(channel, "ONEWIRE read inconsistency : expected bus %d, device %d but received bus %d, device %d\n",
            ow_bus_no, ow_device_no, id >> 4, id & 0x0F);

        // Update FGC.DLS.NUM_ERRORS and FGC.DLS.ERROR_TIME

        device.fgc.dls_num_errors += 1;
        timingGetUserTime(0, &device.fgc.dls_error_time);

        // Latch fault

        latchStatusBit(&device.status.st_latched, FGC_LAT_DALLAS_FLT, true);

        return false;
    }

    return true;
}



/*
 * Read 1-wire devices and temperatures from FGClite paged memory
 */

static void onewireRead(uint32_t channel, uint8_t ow_bus_no, uint8_t ow_page_no, const struct FGClite_status *status)
{
    struct OW_bus &obus = ow_bus[channel-1];

#ifdef DEBUG
    fprintf(stderr, "Device %d: reading data for bus %d, page %d\n", channel, ow_bus_no, ow_page_no);
#endif

    // Check data integrity of OW scan results

    bool is_data_error = false;

    for(uint32_t i = 0; i < OW_DEVS_PER_PAGE; ++i)
    {
        if(!onewireCheckPage(channel, status->paged[i*16], ow_bus_no, ow_page_no * OW_DEVS_PER_PAGE + i))
        {
            is_data_error = true;
            continue;
        }

        // Register the location of any 1-wire devices which contain a digital thermometer

        const uint8_t &type = status->paged[i*16 + 8];

        if(type == DLS_TEMP_SENSOR)
        {
            obus.bus_temp_sensor_mask  |= (1 << ow_bus_no);
            obus.page_temp_sensor_mask |= (1 << ((ow_bus_no * OW_PAGES_PER_BUS) + ow_page_no));
        }
    }

    if(!is_data_error)
    {
        // Copy the data page into memory

        memcpy(&obus.ow_bus[ow_bus_no].id[ow_page_no * OW_DEVS_PER_PAGE], status->paged, sizeof(FGClite_onewire) * OW_DEVS_PER_PAGE);
    }
}



/*
 * Write the raw temperature as a string into the correct place in the description buffer.
 *
 * As the raw temperature is a substring of a longer description,  it is not null-terminated.
 */

static void onewireTempString(char *desc, const float temp)
{
    int n,i;

    n = sprintf(desc + 16, ":%2.2f", temp);

    // Write empty characters up to the character 22

    for(i=16+n; i < 22; ++i) desc[i] = ' ';

    desc[i] = ':';
}



/*
 * Read temperatures only from FGClite paged memory
 */

static void onewireReadTemp(uint32_t channel, uint8_t ow_bus_no, uint8_t ow_page_no, const struct FGClite_status *status)
{
    struct OW_bus &obus = ow_bus[channel-1];

#ifdef DEBUG
    fprintf(stderr, "Device %d: reading data for bus %d, page %d\n", channel, ow_bus_no, ow_page_no);
#endif

    bool is_data_error = false;

    for(uint32_t i = 0; i < OW_DEVS_PER_PAGE; ++i)
    {
        if(!onewireCheckPage(channel, status->paged[i*16], ow_bus_no, ow_page_no * OW_DEVS_PER_PAGE + i)) is_data_error = true;
    }

    if(is_data_error) return;

    // Copy the data page into memory

    memcpy(&obus.ow_bus[ow_bus_no].id[ow_page_no * OW_DEVS_PER_PAGE], status->paged, sizeof(struct FGClite_onewire) * OW_DEVS_PER_PAGE);

    // Copy temperature readings into the FGC.ID.* properties

    for(uint32_t i = 0; i < OW_DEVS_PER_PAGE; ++i)
    {
        uint8_t                 device_no = ow_page_no * OW_DEVS_PER_PAGE + i;
        struct FGClite_onewire &ow_id     = obus.ow_bus[ow_bus_no].id[device_no];

        if(ow_id.dallas_id.type == DLS_TEMP_SENSOR)
        {

            // Copy the raw temperature into FGC.ID.* and into the temperature property associated to this Dallas ID

            if(obus.ow_dallas_barcode[ow_bus_no][device_no].temp_ptr)
            {
                // Temperature property is defined

                float* temp_ptr = obus.ow_dallas_barcode[ow_bus_no][device_no].temp_ptr;

                onewireTempFloat (temp_ptr,    ow_id.raw_temp);
                onewireTempString(obus.ow_dallas_barcode[ow_bus_no][device_no].desc,*temp_ptr);
                onewireTempProp(channel, temp_ptr);
            }
            else
            {
                // Temperature property is not defined

                float temp = 0.0;
                onewireTempFloat (&temp, ow_id.raw_temp);
                onewireTempString(obus.ow_dallas_barcode[ow_bus_no][device_no].desc, temp);

            }
#ifdef DEBUG
            fprintf(stderr, "Channel %d Bus %d Device %d Temperature updated\n", channel, ow_bus_no, (ow_page_no*4)+i);
#endif
        }
    }
}



/*
 * Increment the bus number to the next bus that contains a temperature sensor
 */

static bool onewireTempNextBus(uint32_t channel)
{
    struct OW_bus &obus = ow_bus[channel-1];

    while(!(obus.bus_temp_sensor_mask & (1 << obus.ow_state.bus_no)) && obus.ow_state.bus_no < OW_BUS_MAX)
    {
        obus.ow_state.bus_no++;
    }

    return obus.ow_state.bus_no <= OW_BUS_MAX;
}



/*
 * Increment the page number to the next page that contains a temperature sensor
 */

static bool onewireTempNextPage(uint32_t channel)
{
    struct OW_bus &obus = ow_bus[channel-1];

    uint8_t pages = obus.page_temp_sensor_mask >> (obus.ow_state.bus_no * OW_PAGES_PER_BUS);

    while(!((1 << obus.ow_state.page_no) & pages) && obus.ow_state.page_no <= OW_PAGE_MAX)
    {
        obus.ow_state.page_no++;
    }

    return obus.ow_state.page_no <= OW_PAGE_MAX;
}



/*
 * Update the BARCODE and FGC.ID.* properties for a Dallas device
 */

static void onewireUpdateBarcode(uint32_t channel, enum OW_buses bus, uint8_t dallas_id)
{
    struct FGClite_onewire &ow_id        = ow_bus[channel-1].ow_bus           [bus].id[dallas_id];
    char* const             fgc_id_text  = ow_bus[channel-1].ow_dallas_barcode[bus][dallas_id].desc;        // Buffer for FGC.ID.* property
    char* const             barcode_text = ow_bus[channel-1].ow_dallas_barcode[bus][dallas_id].barcode;     // Buffer for BARCODE.BUS.* property
    float*                 &temp_ptr     = ow_bus[channel-1].ow_dallas_barcode[bus][dallas_id].temp_ptr;    // Pointer to temperature property

    // Copy the Dallas ID into FGC.ID.*

    IdDbDallasIDString(fgc_id_text, &ow_id.dallas_id);

    // Validate the Dallas ID and look it up in IDDB, PTDB and CompDb

    uint16_t comp_idx = codesDallasLookup(&ow_id.dallas_id, barcode_text);

    // Increment the count for the group this barcode belongs to

    codesGroupsAdd(channel, comp_idx);

    // Copy barcode and component description to FGC.ID.*

    snprintf(fgc_id_text + 23, OW_FGC_ID_LEN - 23, "%s=%s", barcode_text, CompDbLabel(comp_idx));

    // Update the sort order for the current bus with the new barcode

    onewireBarcodeSort(channel, bus, barcode_text);

    // Update the BARCODE.BUS.* properties with the new barcode

    onewireBarcodeBusAdd(channel, bus, barcode_text);

    // If the component type for the barcode has a property specified, update the property with the barcode

    char bus_label = bus == OW_BUS_VS_A || bus == OW_BUS_MEAS_A ? 'A' : 'B';

    equipdevPropSetString(channel, bus_label, CompDbBarcodeProperty(comp_idx), barcode_text);

    // If the temperature for the barcode has a property specified, update the pointer to the property

    temp_ptr = equipdevPropGetTempPtr(channel, bus_label, CompDbTempProperty(comp_idx));

    // Copy the temperature into FGC.ID.* and into the temperature property associated to this Dallas ID

    if(temp_ptr)
    {
        // Temperature property is defined

        onewireTempFloat (temp_ptr,    ow_id.raw_temp);
        onewireTempString(fgc_id_text, *temp_ptr);
        onewireTempProp(channel, temp_ptr);
    }
    else
    {
        // Temperature property is not defined

        float temp = 0.0;
        onewireTempFloat (&temp, ow_id.raw_temp);
        onewireTempString(fgc_id_text, temp);
    }
}



// State functions

static enum State_OneWire stateOwIL(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(first_call)
    {
        ow_state.bus_no = 0;
    }

    return IL;
}



static enum State_OneWire stateOwST(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct OW_bus           &obus   = ow_bus[channel-1];

    if(first_call)
    {
        logPrintf(channel, "ONEWIRE Starting full scan of all 1-wire buses\n");

        obus.ow_state.bus_no           = 0;
        obus.ow_state.is_scan_complete = false;

        // Clear the devices on each bus

        for(uint32_t bus = 0; bus <= OW_BUS_MAX; ++bus) obus.num_els[bus] = 0;

        // Clear the register of which 1-wire devices have a temperature sensor

        obus.bus_temp_sensor_mask      = 0;
        obus.page_temp_sensor_mask     = 0;

        // Clear the BARCODE.BUS.* properties

        device.barcode.num_els.bus_V   = 0;
        device.barcode.num_els.bus_A   = 0;
        device.barcode.num_els.bus_B   = 0;
        device.barcode.num_els.bus_L   = 0;
    }

    return ST;
}



static enum State_OneWire stateOwSC(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(first_call)
    {
#ifdef DEBUG
        fprintf(stderr, "DEVICE %d : Scanning bus %d\n", channel, ow_state.bus_no);
#endif
        // Force the 1-wire status to BUSY to prevent transitioning out of SCANNING state

        ow_state.is_busy = true;

        // Request the scan of the next bus in sequence

        fgcliteCmdSetCtrl(ctrl_cmd, channel, OW_SCAN | ow_state.bus_no);
    }
    else
    {
        // Unset the OW_SCAN bit in CTRL_CMD

        fgcliteCmdUnsetCtrl(ctrl_cmd, channel, OW_SCAN);

        // Get the 1-wire busy status

        ow_state.is_busy = status->critical.controller_status & OW_SCAN_BUSY;
    }

    return SC;
}



static enum State_OneWire stateOwRD(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(first_call)
    {
        ow_state.page_no = 0;
    }
    else
    {
        // Read out the page requested in the last cycle

        onewireRead(channel, ow_state.bus_no, ow_state.page_no++, status);
    }

    if(ow_state.page_no <= OW_PAGE_MAX)
    {
        // Request the next page
#ifdef DEBUG
        fprintf(stderr, "DEVICE %d : Requesting page %d\n", channel, ow_state.page_no);
#endif

        fgcliteCmdRequestCtrl(ctrl_cmd, channel, OW_BUS, ow_state.page_no);
        ow_state.ctrl_cmd_set = true;
    }

    return RD;
}



static enum State_OneWire stateOwTS(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(first_call)
    {
#ifdef DEBUG
        fprintf(stderr, "DEVICE %d : Scanning bus %d\n", channel, ow_state.bus_no);
#endif
        // Force the 1-wire status to BUSY to prevent transitioning out of TEMP_SCANNING state

        ow_state.is_busy = true;

        if(onewireTempNextBus(channel))
        {
            // Request the scan of the next bus in sequence

            fgcliteCmdSetCtrl(ctrl_cmd, channel, OW_SCAN | ow_state.bus_no);
        }
    }
    else
    {
        // Unset the OW_SCAN bit in CTRL_CMD

        fgcliteCmdUnsetCtrl(ctrl_cmd, channel, OW_SCAN);

        // Get the 1-wire busy status

        ow_state.is_busy = status->critical.controller_status & OW_SCAN_BUSY;
    }

    return TS;
}



static enum State_OneWire stateOwTR(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd, bool first_call)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(first_call)
    {
        ow_state.page_no          = 0;
        ow_state.requested_bus_no = ow_state.bus_no;
    }

    // If we missed a FIP status, abandon the current set of temperature readings

    if(ow_state.ctrl_cmd_set)
    {
        ow_state.is_page_request = false;
        ow_state.page_no         = OW_PAGE_MAX + 1;
    }

    // Read out the last requested page if there is one

    if(ow_state.is_page_request)
    {
        onewireReadTemp(channel, ow_state.requested_bus_no, ow_state.requested_page_no, status);

        ow_state.is_page_request = false;
    }

    // Request the next page

    if(onewireTempNextPage(channel))
    {
#ifdef DEBUG
        fprintf(stderr, "DEVICE %d : Requesting page %d\n", channel, ow_state.page_no);
#endif
        fgcliteCmdRequestCtrl(ctrl_cmd, channel, OW_BUS, ow_state.page_no);

        ow_state.ctrl_cmd_set      = true;
        ow_state.is_page_request   = true;
        ow_state.requested_page_no = ow_state.page_no++;
    }

    return TR;
}



// Transition functions

static StateFunc* XXtoST(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(ow_state.start_scan)
    {
        ow_state.start_scan = false;

        return stateOwST;
    }

    return NULL;
}



static StateFunc* STtoSC(uint32_t channel)
{
    return stateOwSC;
}



static StateFunc* SCtoRD(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    // Stay in SCANNING state as long as the 1-wire bus is busy

    if(ow_state.is_busy)
    {
#ifdef DEBUG
        fprintf(stderr, "[%d] busy ", channel);
#endif

        return NULL;
    }

    return stateOwRD;
}



static StateFunc* RDtoSC(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    // If we have read the last page, scan the next bus

    if(  ow_state.page_no >  OW_PAGE_MAX &&
       ++ow_state.bus_no  <= OW_BUS_MAX)
    {
        return stateOwSC;
    }

    return NULL;
}



static StateFunc* RDtoIL(uint32_t channel)
{
    struct Equipdev_channel &device   = equipdev.device[channel];
    struct OW_state         &ow_state = ow_bus[channel-1].ow_state;

    if(ow_state.bus_no > OW_BUS_MAX)
    {
        logPrintf(channel, "ONEWIRE Full scan of 1-wire buses complete\n");

        ow_state.is_scan_complete = true;

        // Notify configuration manager that we are ready to sync

        device.config_mode  = FGC_CFG_MODE_SYNC_FGC;
        device.config_state = FGC_CFG_STATE_UNSYNCED;

        return stateOwIL;
    }

    return NULL;
}



static StateFunc* ILtoTS(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(ow_state.start_temp_scan)
    {
        ow_state.start_temp_scan = false;

        return stateOwTS;
    }

    return NULL;
}



static StateFunc* TStoTR(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    // Stay in TEMP_SCANNING state as long as the 1-wire bus is busy

    if(ow_state.is_busy)
    {
#ifdef DEBUG
        fprintf(stderr, "[Channel %d busy]", channel);
#endif

        return NULL;
    }

    return stateOwTR;
}



static StateFunc* TRtoTS(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    // If we have read the last page, scan the next bus

    if(ow_state.page_no > OW_PAGE_MAX)
    {
        ++ow_state.bus_no;

        if(onewireTempNextBus(channel)) return stateOwTS;
    }

    return NULL;
}



static StateFunc* TRtoIL(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    if(ow_state.bus_no > OW_BUS_MAX) return stateOwIL;

    return NULL;
}



// External functions

void onewireReset(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    ow_state.is_scan_complete = false;
    ow_state.is_prop_updated  = false;
}



void onewireStartFullScan(uint32_t channel)
{
    ow_bus[channel-1].ow_state.start_scan = true;
}



void onewireStartTempScan(uint32_t channel)
{
    struct Equipdev_channel &device   = equipdev.device[channel];
    struct OW_state         &ow_state = ow_bus[channel-1].ow_state;

    // If FGC.DLS.INHIBIT == ENABLED, skip one set of temperature acquisitions

    if(device.fgc.dls_inhibit == FGC_CTRL_ENABLED)
    {
        device.fgc.dls_inhibit = FGC_CTRL_DISABLED;

        return;
    }

    // Start the scan of temperature devices

    ow_state.start_temp_scan = true;
}



uint32_t onewireTempPageCount(uint32_t channel)
{
    struct OW_bus &obus = ow_bus[channel-1];

    return std::bitset<32>(obus.page_temp_sensor_mask).count();
}



bool onewireIsScanComplete(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    return ow_state.is_scan_complete;
}



bool onewireIsDataReady(uint32_t channel)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    return ow_state.is_prop_updated;
}



/*
 * Update the data for FGC.ID.* properties
 */

void onewireUpdateProps(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct OW_bus           &obus   = ow_bus[channel-1];

    // If there is nothing to update, return immediately

    if(!obus.ow_state.is_scan_complete || obus.ow_state.is_prop_updated) return;

    // Update the properties for each bus in turn

    for(uint8_t b = 0; b <= OW_BUS_MAX; ++b)
    {
        enum OW_buses bus = static_cast<enum OW_buses>(b);

        uint32_t idonly    = 0;
        uint32_t idtemp    = 0;
        uint32_t other     = 0;
        uint32_t max_reads = 0;

        // Count the number of devices of each type

        for(uint8_t i = 0; i < OW_BUS_MAX_DEVS; ++i)
        {
            struct FGClite_onewire &ow_id = obus.ow_bus[bus].id[i];

            if(ow_id.dallas_id.type != DLS_NONE)
            {
                // Increment the count of each type of device

                switch(ow_id.dallas_id.type)
                {
                    case DLS_SERIAL_NUMBER:    ++idonly; break;
                    case DLS_TEMP_SENSOR:      ++idtemp; break;
                    default:                   ++other;
                }

                // Update the BARCODE properties for the device

                onewireUpdateBarcode(channel, bus, i);
            }
        }

        // Connect the FGC.ID.* properties to the 1-wire desc structures in the sorted order

        for(uint32_t i = 0; i < obus.num_els[bus]; ++i)
        {
            const uint8_t &sort_order = obus.sort_order[bus][i];

            device.fgc.id[bus][i] = obus.ow_dallas_barcode[bus][sort_order].desc;
        }

        // Update the number of elements for the property

        equipdevPropSetNumEls(channel, '\0', OW_bus_property_name[bus], obus.num_els[bus]);

        // Count the number of pages that contain temperature sensors

        max_reads += std::bitset<4>((obus.page_temp_sensor_mask >> (bus * OW_PAGES_PER_BUS)) & 0xF).count();

        // Update FGC.ID.SUMMARY

        device.fgc.id_summary[bus] = obus.ow_bus_summary[bus];

        sprintf(obus.ow_bus_summary[bus], "%d|devs %2u {idonly %2u idtemp %2u other %2u} max_reads %u",
            bus, idonly + idtemp + other, idonly, idtemp, other, max_reads);
    }

    // Set flag to indicate we are up-to-date

    if(obus.ow_state.is_scan_complete) obus.ow_state.is_prop_updated = true;
}



void onewireUpdate(uint32_t channel, const struct FGClite_status *status, bool *ctrl_cmd_set, struct FGClite_command *ctrl_cmd)
{
    struct OW_state &ow_state = ow_bus[channel-1].ow_state;

    StateFunc *target_state = NULL;    // pointer to the state to transition to
    TransFunc *trans_fptr;             // pointer to the next transition function to test

    // ctrl_cmd set is true if we missed a FIP status. Abandon the current read of the 1-wire bus.

    if(ctrl_cmd_set != NULL) ow_state.ctrl_cmd_set = *ctrl_cmd_set;

    // Scan transitions for the current state, testing each condition in priority order

    for(uint32_t i = 0; (trans_fptr = Transitions[ow_state.state][i]) != NULL; ++i)
    {
        target_state = (*trans_fptr)(channel);

        if(target_state != NULL)
        {
            // Change state

            ow_state.state = (*target_state)(channel, status, ctrl_cmd, true);

            break;
        }
    }

    if(target_state == NULL)
    {
        // No transition : stay in current state and run the state function with the first_call parameter set to false

        (*(run_state[ow_state.state]))(channel, status, ctrl_cmd, false);
    }

    // Notify the calling application whether a paged memory request was set in this cycle

    if(ctrl_cmd_set != NULL) *ctrl_cmd_set = ow_state.ctrl_cmd_set;
}

// EOF
