/*!
 * @file   serial.cpp
 * @brief  Functions for serial tunnelling across the FIP interface
 * @author Michael Davis
 */

#include <cstring>
#include <cstdio>

#include <fgc_errs.h>
#include <logging.h>
#include <cmdqmgr.h>
#include <serial.h>



// Constants

const uint32_t INPUT_CHUNK_SIZE     = 7;                    // Size in bytes of the serial field in the FGClite critical data
const uint32_t OUTPUT_CHUNK_SIZE    = 4;                    // Size in bytes of the serial payload for each device in SERIAL_CMD

// Ensure that the output buffer size is an integer multiple of the output chunk size

const uint32_t OUTPUT_BUFFER_CHUNKS = CMD_MAX_VAL_LENGTH / OUTPUT_CHUNK_SIZE;           // Max no. of chunks in output buffer
const uint32_t OUTPUT_BUFFER_SIZE   = OUTPUT_BUFFER_CHUNKS * OUTPUT_CHUNK_SIZE;         // Buffer size must be a multiple of OUTPUT_CHUNK_SIZE



// Struct for serial buffers

struct SerialBuffers
{
    struct Cmd    cmd;                                      // Command string built up from the serial input
    struct Queue  response_queue;                           // Response queue for serial port commands
    bool          is_cmd_set;                               // cmd.type is valid
    char          cmd_value [CMD_MAX_VAL_LENGTH];           // cmd.value buffer
    char          output_buffer [OUTPUT_BUFFER_SIZE];       // Output buffer for this device
    uint32_t      start;                                    // First item in output buffer
    uint32_t      end;                                      // Index after last item in output buffer

    bool          is_Xon;                                   // Manage START/STOP serial protocol for this device
};

// Array of global variables per device

struct SerialBuffers serial [FGCD_MAX_EQP_DEVS] = {};



// Static functions

/*
 * Write data to a circular buffer
 *
 * @returns Number of bytes written
 */

static uint32_t outputBufferWrite(uint32_t channel, const char *input, uint32_t input_size)
{
    struct SerialBuffers &serial_buffers = serial[channel-1];

    if(input_size == 0) return 0;

    uint32_t at_0;      // Available space between 0 and start
    uint32_t at_end;    // Available space between end and the end of the buffer

    if(serial_buffers.start <= serial_buffers.end)
    {
        at_end = OUTPUT_BUFFER_SIZE - serial_buffers.end;
        at_0   = serial_buffers.start;
    }
    else
    {
        at_end = serial_buffers.start - serial_buffers.end - 1;
        at_0   = 0;
    }

    // Calculate the input size rounded up to an integer number of chunks.
    // If there is insufficient space in the buffer, don't write anything and return

    uint32_t input_chunk_size = input_size + OUTPUT_CHUNK_SIZE - input_size % OUTPUT_CHUNK_SIZE;

    if(input_chunk_size > at_end + at_0) return 0;

    // Copy the data into the circular buffer

    if(input_size <= at_end)
    {
        memcpy(&serial_buffers.output_buffer[serial_buffers.end], input, input_size);

        serial_buffers.end += input_size;
    }
    else
    {
        memcpy(&serial_buffers.output_buffer[serial_buffers.end], input, at_end);

        serial_buffers.end = input_size - at_end;

        memcpy(&serial_buffers.output_buffer[0], &input[at_end], serial_buffers.end);
    }

    // Zero the unused bytes in the last chunk in case we didn't finish on a chunk boundary

    memset(&serial_buffers.output_buffer[serial_buffers.end], 0, OUTPUT_CHUNK_SIZE - serial_buffers.end % OUTPUT_CHUNK_SIZE);

    return input_size;
}



/*
 * Write an error message to an output buffer.
 *
 * Error response without data: $$nn_err msg\n!
 * Error response with data:    $object data$nn_err msg\n!
 */

static void serialSendError(uint32_t channel, enum fgc_errno err_no, const char *error_data)
{
    char error_text[CMD_MAX_RTERM_RESP_LENGTH];

    int32_t len = snprintf(error_text, CMD_MAX_RTERM_RESP_LENGTH, "$%s$%d %s\n!",
                           error_data == NULL ? "" : error_data,
                           err_no, fgc_errmsg[err_no]);

    // Log the error in the FGCD log

    logPrintf(channel, "SERIAL %s\n", error_text);

    // Send the error to the serial port

    if(outputBufferWrite(channel, error_text, len) == 0)
    {
        // Can't write error because response buffer is full

        logPrintf(channel, "SERIAL Response buffer full\n");
    }
}



/*
 * Add the buffered command to the command queue
 */

static void serialQueueCmd(uint32_t channel)
{
    struct Cmd &cmd        = serial[channel-1].cmd;
    bool       &is_cmd_set = serial[channel-1].is_cmd_set;

    // If the command is empty, do nothing.
    // This could happen if commands have more than one terminator (; CR LF)

    if(cmd.command_length == 0) return;

    // Check the command type was set correctly

    if(!is_cmd_set)
    {
        serialSendError(channel, FGC_UNKNOWN_CMD, NULL);

        cmd.command_length = 0;

        return;
    }

    // Make sure the command string is properly terminated

    cmd.command_string[cmd.command_length] = '\0';

    // For SET commands, separate the command and the value

    char *cmd_val;

    if(cmd.type == CMD_SET && (cmd_val = strchr(cmd.command_string, ' ')) != NULL)
    {
        // Terminate the command at the first space

        *cmd_val++ = '\0';

        cmd.command_length = strlen(cmd.command_string);

        // Advance to the first non-space character, and copy the rest of the string to value

        while(*cmd_val == ' ') ++cmd_val;

        cmd.value_length = strlen(strcpy(cmd.value, cmd_val));
    }

    // Log the command

    if(cmd.type == CMD_GET)
    {
        logPrintf(channel, "SERIAL cmd (G %s)\n", cmd.command_string);
    }
    else // Set command
    {
        logPrintf(channel, "SERIAL cmd (S %s %s)\n", cmd.command_string, cmd.value);
    }

    // Get a command queue item from the free queue

    struct Cmd *new_cmd;

    if((new_cmd = reinterpret_cast<struct Cmd*>(queuePop(&cmdqmgr.free_cmd_q))) == NULL)
    {
        logPrintf(channel, "SERIAL Could not obtain a command struct from free queue.\n");
        return;
    }

    memcpy(new_cmd, &cmd, sizeof(struct Cmd));

    cmdqmgrQueue(new_cmd);

    if(new_cmd->error != FGC_OK_NO_RSP)
    {
        serialSendError(channel, static_cast<fgc_errno>(new_cmd->error), NULL);
    }

    // Reset the command buffer

    cmd.command_length = 0;
}



// External functions

void serialReset(uint32_t channel)
{
    struct SerialBuffers &serial_buffers = serial[channel-1];
    struct Cmd           &cmd            = serial_buffers.cmd;

    // Initialise the response queue on first call only

    if(cmd.response_queue == NULL)
    {
        if(queueInit(&serial_buffers.response_queue, cmdqmgr.num_cmds) != 0)
        {
            logPrintf(channel, "SERIAL Failed to initialise command response queue.\n");
        }
    }

    // Attach the command value buffer and then reset the serial command

    cmd.value = serial_buffers.cmd_value;

    cmdqmgrInitCmd(&cmd);

    cmd.struct_type    = CMD_STRUCT;
    cmd.response_queue = &serial_buffers.response_queue;

    sprintf(cmd.device_name, "%d", channel);

    // Disable RBAC authentication for commands received across the serial port

    cmd.client = CMD_FGCD_CLIENT;

    // Reset the response buffer

    serial_buffers.start              = 0;
    serial_buffers.end                = 0;
    serial_buffers.is_Xon             = true;
}



uint32_t serialReadCmd(uint32_t channel, const uint8_t *input)
{
    struct Cmd &cmd        = serial[channel-1].cmd;
    bool       &is_cmd_set = serial[channel-1].is_cmd_set;

    /*
     * Copy characters received across the serial port into the buffer and handle special characters.
     *
     * In case of no data on the serial port, FGClite will send zeros:
     *
     * 0x00 NUL             Ignored
     *
     * In addition, the Serial Command Response Protocol (SCRP) recognises the following
     * non-printable characters:
     *
     * 0x03 ETX (Ctrl-C)    Abort Get command
     * 0x0A LF              Execute command (synonym for CR)
     * 0x0D CR              Execute command
     * 0x11 TAB (Xon)       Resume transmission by FGC
     * 0x13 VT  (Xoff)      Suspend transmission by FGC
     * 0x19 EM  (Ctrl-Y)    Switch to Diagnostic mode     (Not implemented in FGClite)
     * 0x1A SUB (Ctrl-Z)    Switch to Direct mode         (Ignored in FGClite: we are always in Direct mode)
     * 0x1B ESC             Switch to Editor mode         (Not implemented in FGClite)
     *
     * The Serial Command Response Protocol (SCRP) also defines the following characters:
     *
     * !    Start command. Characters before the ! are ignored. If ! is repeated, the buffer is
     *      discarded and a new command is started. Spaces after the ! are ignored.
     * G/g  GET command (immediately after !)
     * S/s  SET command (immediately after !)
     * ;    end of command delimiter (synonym for LF and CR
     * $    SCRP response delimiter character, ignored in commands
     */

    for(uint32_t i = 0; i < INPUT_CHUNK_SIZE; ++i)
    {
        if(cmd.command_length > CMD_MAX_LENGTH - 1)
        {
            // Command buffer full: return an error and reset the buffer

            serialSendError(channel, FGC_CMD_BUF_FULL, NULL);

            cmd.command_length = 0;

            continue;
        }

        // Parse SCRP command

        switch(input[i])
        {
            case '$':
            case 0x00: /* NUL */    /* Ignored */                                           break;

            case '!':               /* Start command */
            case 0x03: /* ETX */    cmd.command_length = 0; is_cmd_set = false;             break;

            case ';':
            case 0x0A: /* LF  */    /* End command */
            case 0x0D: /* CR  */    serialQueueCmd(channel);                                break;

                                    /* Control X_ON/X_OFF */
            case 0x11: /* TAB */    serial[channel-1].is_Xon = true;                        break;
            case 0x13: /* VT  */    serial[channel-1].is_Xon = false;                       break;

                                    /* Switch modes */
            case 0x19: /* EM  */    serialSendError(channel, FGC_NOT_IMPL, NULL);           break;
            case 0x1A: /* SUB */    /* Ignored */                                           break;
            case 0x1B: /* ESC */    serialSendError(channel, FGC_NOT_IMPL, NULL);           break;

            case ' ':               /* Ignore initial spaces */ if(cmd.command_length == 0) break;
            case '"' ... '#':
            case '%' ... ':':       /* Normal ASCII characters except ! $ ; */
            case '<' ... '~':       cmd.command_string[cmd.command_length++] = input[i];    break;

                                    /* Unrecognised character outside printable range */
            default:                serialSendError(channel, FGC_PROTO_ERROR, NULL);
        }

        // If this is the first character in the command, set the command type

        if(!is_cmd_set && cmd.command_length == 1)
        {
            is_cmd_set = true;

            switch(cmd.command_string[0])
            {
                case 'g':           /* GET command */
                case 'G':           cmd.type = CMD_GET; break;

                case 's':           /* SET command */
                case 'S':           cmd.type = CMD_SET; break;

                                    /* Unrecognised command type */
                default:            is_cmd_set = false;
            }

            if(is_cmd_set) cmd.command_length = 0;
        }
    }

    return INPUT_CHUNK_SIZE;
}



void serialWriteRsp(uint8_t *output_ptr)
{
    // Zero the output buffers for all devices

    memset(output_ptr, 0, OUTPUT_CHUNK_SIZE * FGCD_MAX_EQP_DEVS);

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        struct SerialBuffers &serial_buffers = serial[channel-1];

        // Find the end of the buffer in chunks

        uint32_t chunk_end = serial_buffers.end;

        if(chunk_end % OUTPUT_CHUNK_SIZE != 0) chunk_end += OUTPUT_CHUNK_SIZE - serial_buffers.end % OUTPUT_CHUNK_SIZE;

        // Update all devices with serial data pending

        if(serial_buffers.start == chunk_end % OUTPUT_BUFFER_SIZE)
        {
            // Ensure that start and end pointers are kept in sync, in case data size does not align to chunk size

            serial_buffers.end = serial_buffers.start;
        }
        else if(serial_buffers.is_Xon)
        {
            // If there is data in the buffer and XON is active, copy the data to the FIP buffer. Note
            // that the FIP data is treated as a 32-bit integer, so we need to flip the byte ordering.

            for(uint32_t i = 0; i < OUTPUT_CHUNK_SIZE; ++i)
            {
                output_ptr[i] = serial_buffers.output_buffer[serial_buffers.start + OUTPUT_CHUNK_SIZE - (i + 1)];
            }

            serial_buffers.start = (serial_buffers.start + OUTPUT_CHUNK_SIZE) % OUTPUT_BUFFER_SIZE;
        }

        output_ptr += OUTPUT_CHUNK_SIZE;

        // Copy data arriving on the response queue into the serial reply buffer

        struct Cmd *response;

        if(serial_buffers.cmd.response_queue                                                       != NULL &&
           (response = reinterpret_cast<struct Cmd*>(queuePop(serial_buffers.cmd.response_queue))) != NULL)
        {
            if(response->error != FGC_OK_NO_RSP)
            {
                serialSendError(channel, static_cast<fgc_errno>(response->error), response->value);
            }
            else
            {
                if(response->type == CMD_SET) response->value_length = 0;

                // Log response to serial command

                logPrintf(channel, "SERIAL rsp (%s)\n", response->type == CMD_SET ? "OK" : response->value);

                // Write response to serial port

                if(outputBufferWrite(channel, "$", 1) == 0                                                                 ||
                  (response->value_length > 0 && outputBufferWrite(channel, response->value, response->value_length) == 0) ||
                   outputBufferWrite(channel, "\n;", 2) == 0)
                {
                    logPrintf(channel, "SERIAL Response buffer full, dropping response to: %s\n", response->command_string);
                }
            }

            // Return the response to the free queue

            queuePush(&cmdqmgr.free_cmd_q, response);
        }
    }
}


// EOF
