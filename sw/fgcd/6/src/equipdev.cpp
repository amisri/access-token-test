//! @file   equipdev.cpp
//! @brief  Functions for equipment devices

//#include <stdint.h>
//#include <unistd.h>

#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <unistd.h>

#include <equipdev.h>
#include <equip_liblog.h>

#include <classes/92/definfo.h>
#include <classes/92/defprops.h>
#include <classes/92/defsyms.h>      // Import symbol definitions for CMW logging

#include <fip.mfip.h>
#include <cmdqmgr.h>
#include <fgcd_thread.h>
#include <nonvol.h>
#include <logging.h>
#include <equip_libevtlog.h>
#include <equip_logger.h>
#include <prop_equip.h>
#include <state_pc.h>
#include <state_op.h>
#include <cc_reg.h>
#include <log.h>
#include <cc_sig.h>
#include <page_mgr.h>
#include <onewire.h>
#include <codes.h>

#include <equipdev.h>

#if N_PROP_SYMBOLS > EQUIPDEV_MAX_PROPS
    #error EQUIPDEV_MAX_PROPS must be greater than or equal to N_PROP_SYMBOLS
#endif

// Global variables, zero initialised

struct Equipdev equipdev = {};


// Static functions

// Initialise the configuration of an equipment device on power-up or following a reset of the hardware

static void equipdevReloadConfig(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Reset the read config flag that was set by fgcliteRegResetDevice()

    device.config_read = false;

    // Load device configuration from non-volatile storage

    device.arming_enabled = false;

    nonvolLoad(device.fgcd_device, 0);

    device.arming_enabled = true;

    // Initialise unconfigured Calibration properties

    ccSigInitCal(channel);

    // Initialise the device type and component groups

    codesInitDevice(channel, fgcd.device[channel].name, device.device.type);

    // Workaround for EPCCCS-8395: Class 92: Suppress NO_VS_CABLE for RPMBD converters

    if (strncmp(device.device.type, "RPMBD", 5) == 0)
    {
        device.mask_NO_VS_CABLE = true;
    }
    else
    {
        device.mask_NO_VS_CABLE = false;
    }

    // Initialise the DIM descriptions and set DIAG.DIMS_EXPECTED

    codesDimsInit(channel);

    // Config has not been reset since the last device reset

    device.config_reset_time = 0;
}



// Manage "non-real time" functions for equipment devices

static void *equipdevRun(void *unused)
{
    // If any of these properties change, set CONFIG.STATE to SYNC_DB_CAL

    const uint32_t sync_db_mask = (1 << CAL_A_ADC_INTERNAL_GAIN) | (1 << CAL_A_ADC_INTERNAL_ERR) |
                                  (1 << CAL_B_ADC_INTERNAL_GAIN) | (1 << CAL_B_ADC_INTERNAL_ERR) |
                                  (1 << CAL_C_ADC_INTERNAL_GAIN) | (1 << CAL_C_ADC_INTERNAL_ERR);

    while(true)
    {
        // Receive timing events: PC_PERMIT is sent in a timing telegram every ms

        timingReceiveEvents();

        // Check each device for updated parameters

        for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            // Skip non-existent devices

            if(device.fgcd_device->name == NULL) continue;

            // If the device was reset, fgcliteRegResetDevice() will request the reading of config data from NVS files
            // Also check here if the device is ready.

            if(device.config_read && device.fgcd_device->ready)
            {
                equipdevReloadConfig(channel);
            }

            // If the device was reset, re-read the one-wire properties

            onewireUpdateProps(channel);

            // Check if any parameters have been set

            if(device.status.state_op != FGC_OP_UNCONFIGURED)
            {
                // Call set property pars functions

                parserCallPropSetParsFunctions(channel);

                // Process any pending changed libreg parameters

                regMgrParsProcess(&device.reg_mgr);
            }

            // Check if any calibration properties have changed and should be written to non-volatile storage

            if(device.status.state_op != FGC_OP_CALIBRATING &&
               device.cal.nonvol_mask != 0)
            {
                uint32_t   nonvol_mask = device.cal.nonvol_mask;
                device.cal.nonvol_mask = 0;

                for(uint32_t i = 0; i <= CAL_NONVOL_PROP_MAX; ++i)
                {
                    if(nonvol_mask & (1 << i))
                    {
                        nonvolStore(device.fgcd_device, Cal_nonvol_prop_string[i], 0, 0, 1);
                    }
                }

                if(device.config_mode == FGC_CFG_MODE_SYNC_NONE && (nonvol_mask & sync_db_mask))
                {
                    device.config_mode = FGC_CFG_MODE_SYNC_DB_CAL;
                }
            }

            // Process logger events

            if(loggerProcess(channel) == true)
            {
                // Logger and PM_BUF readout complete - request page manager state transition from POSTMORTEM to LOGGING state

                pageMgrPMReset(channel);
            }
        }

        usleep(1000);    // sleep for 1ms
    }

    return NULL;
}


// Initialise the configuration of an equipment device

static uint32_t equipdevInitDevice(uint32_t channel)
{
    fprintf(stderr,"equipdevInitDevice: channel %u %s\n",channel, fgcd.device[channel].name);

    struct Equipdev_channel &device = equipdev.device[channel];

    device.fgcd_device = &fgcd.device[channel];

    // Set test strings

    device.test.strings[0] = "FIRST";
    device.test.strings[1] = "SECOND";

    // Initialize libevtlog

    if(ccEvtlogInit(channel) != 0) return 1;

    // Initialize libsig
    //
    // This must be done before reading from non-volatile storage because otherwise
    // defaults would overwrite non-volatile values

    if(ccSigInit(channel) != 0) return 1;

    // Initilise libreg

    if(ccRegInit(channel) != 0) return 1;

    // Initialize libref

    if(referenceInit(channel) != 0) return 1;

    // Initialise logging : This must be done AFTER initialising libref & libreg to make sure pointers

    ccLogInit(channel);

    // Initialise STATE.PC to FLT_OFF and STATE.OP to UNCONFIGURED

    statePcInit(channel);
    stateOpInit(channel);

    return 0;
}


// External functions

int32_t equipdevStart(void)
{
    fprintf(stderr,"equipdevStart\n");

    // Initialise the standard equipdev services (hash tables, parser data, ...)

     equipdevInit(NUM_PARS_FUNC_92, ccEvtlogStoreSetCmd);

    // Initialise each device

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        equipdev.device[channel].fgcd_device = &fgcd.device[channel];

        if(fgcd.device[channel].name != NULL)
        {
            if(equipdevInitDevice(channel) != 0) return 1;
        }
    }

    return fgcd_thread_create(&equipdev.thread, equipdevRun, NULL, EQUIPDEV_THREAD_POLICY, EQUIPDEV_THREAD_PRIORITY, NO_AFFINITY);
}



// Called from devnameRead()

void equipdevInitDevices(void)
{
    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
    {
        struct FGCD_device &fgcd_device = fgcd.device[channel];

        if(fgcd_device.fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Start the parser
            // This also initializes the command queue

            if(parserStart(&equipdev.device[channel].parser) != 0)
            {
                fprintf(stderr, "WARNING: Failed to start parser\n");
            }

            // Set command queue

            fgcd_device.command_queue = &equipdev.device[channel].parser.command_queue;
        }

        // Configure/unconfigure FIP devices

        if(fgcd.device[channel].name != NULL)
        {
            fipMfipConfigureDevice(channel);
        }
        else
        {
            fipMfipUnconfigureDevice(channel);
        }
    }
}



void equipdevPublish(void)
{
    for(uint32_t i = 1; i <= FGCD_MAX_EQP_DEVS; ++i)
    {
        struct Equipdev_channel &device     = equipdev.device[i];
        struct fgc_stat         &pub_data   = pub.published_data.status[i];
        struct fgc92_stat       &pub_status = pub_data.class_data.c92;

        if(fgcd.device[i].fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set class in published data

            pub_data.class_id = EQUIPDEV_CLASS_ID;

            // Swap byte order and copy device status into published data

            pub_status.st_faults     = htons(device.status.st_faults);
            pub_status.st_warnings   = htons(device.status.st_warnings);
            pub_status.st_latched    = htons(device.status.st_latched);
            pub_status.st_unlatched  = htons(device.status.st_unlatched);
            pub_status.state_op      = device.status.state_op;
            pub_status.state_vs      = device.status.state_vs;
            pub_status.state_pc      = device.status.state_pc;
            pub_status.st_meas_a     = htons(device.status.st_meas_a);
            pub_status.st_meas_b     = htons(device.status.st_meas_b);
            pub_status.st_dcct_a     = device.status.st_dcct_a;
            pub_status.st_dcct_b     = device.status.st_dcct_b;
            pub_status.i_err_ma      = htons(device.status.i_err_ma);
            pub_status.i_diff_ma     = htons(device.status.i_diff_ma);
            pub_status.i_earth_cpcnt = htons(device.status.i_earth_cpcnt);
            pub_status.i_ref         = htonf(device.status.i_ref);
            pub_status.i_meas        = htonf(device.status.i_meas);
            pub_status.v_ref         = htonf(device.status.v_ref);
            pub_status.v_meas        = htonf(device.status.v_meas);
        }
    }
}



bool equipdevIsPcPermit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Always return true if we are simulating interlocks

    if(device.status.state_op == FGC_OP_SIMULATION && device.vs.sim_intlks == FGC_CTRL_ENABLED) return true;

    if(device.crate_type == FGC_CRATE_TYPE_PC_60A)
    {
        return equipdev.gw_pc_permit;
    }
    else
    {
        return digitalIOGetStatus(&device.dig, FGC_DIG_STAT_PC_PERMIT);
    }
}



void equipdevPropSetString(uint32_t channel, char bus_id, const char *prop_label, const char *prop_value)
{
    struct prop    *property;                               // Pointer to the property
    char            prop_label_expand[FGC_MAX_PROP_LEN];    // Expanded label for the property, with ? converted to A or B

    // Do nothing if prop name is unset

    if(prop_label[0] == '\0') return;

    if((property = equipdevLookupProp(channel, bus_id, prop_label, prop_label_expand)) == NULL)
    {
        logPrintf(channel, "EQUIPDEV Cannot set %s = %s.\n", prop_label_expand, prop_value);
        return;
    }
    else if(property->type != PT_CHAR || !(property->flags & PF_INDIRECT_N_ELS))
    {
        // This function should only be called for CHAR properties with an indirect number of elements

        logPrintf(channel, "EQUIPDEV Property is incorrect type for value. Cannot set %s = %s.\n", prop_label_expand, prop_value);
        return;
    }

    // Set the property to the requested value

    const uint32_t offset = sizeof(Equipdev_channel) * channel;

    char *value = reinterpret_cast<char*>(property->value) + offset;

    strcpy(value, prop_value);

    uint32_t *num_elements = reinterpret_cast<uint32_t*>(reinterpret_cast<char*>(property->num_elements) + offset);

    *num_elements = strlen(value);

    // Store in non-volatile memory if required

    if(property->set_func_idx != SET_NONE && property->flags & PF_NON_VOLATILE  && !(property->flags & PF_DONT_STORE_ON_SET))
    {
        nonvolStore(&fgcd.device[channel], prop_label_expand, 0, 0, property->flags & PF_CONFIG ? 1 : 0);
    }
}



float *equipdevPropGetTempPtr(uint32_t channel, char bus_id, const char *prop_label)
{
    struct prop    *property;                               // Pointer to the property
    char            prop_label_expand[FGC_MAX_PROP_LEN];    // Expanded label for the property, with ? converted to A or B

    // Do nothing if prop name is unset

    if(prop_label[0] == '\0') return NULL;

    if((property = equipdevLookupProp(channel, bus_id, prop_label, prop_label_expand)) == NULL)
    {
        logPrintf(channel, "EQUIPDEV Cannot store temperature in %s.\n", prop_label_expand);
        return NULL;
    }
    else if(property->type != PT_FLOAT)
    {
        logPrintf(channel, "EQUIPDEV Cannot store temperature in %s, as it is not of type FLOAT.\n", prop_label_expand);
        return NULL;
    }

    // Return a pointer to the property value for the correct device

    const uint32_t offset = sizeof(Equipdev_channel) * channel;

    return reinterpret_cast<float*>(reinterpret_cast<char*>(property->value) + offset);
}

// EOF
