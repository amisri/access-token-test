//! @file   state_pc.cpp
//! @brief  Manage Power Converter state and Reference state for FGClite devices


#include <stdint.h>

#include <classes/92/defsyms.h>
#include <state_pc.h>
#include <cc_sig.h>
#include <cmwpub.h>
#include <logging.h>
#include <timing.h>
#include <equipdev.h>
#include <equip_logger.h>
#include <fgclite_cycle.h>
#include <pm.h>



enum fgc_errno statePcResetFaults(uint32_t channel)
{
    struct Equipdev_channel &device  = equipdev.device[channel];

    // The Free-Wheel Diode (FWD) is monitored by the DIMs. If a fault is detected, a Diode Fault or Fast Abort Unsafe fault will be asserted, and
    // it is not possible to reset the converter until these faults are reset by an authorized individual (CCC operators are not authorised), i.e.
    // the person responsible for the power converter.

    if(device.diag.fw_diode.state == FGC_VDI_FAULT) return FGC_FW_DIODE_FAULT;

    // The same concept is used for FAST_ABORT_UNSAFE, which must be acknowledged by an authorized individual.

    if(device.diag.fabort_unsafe.state == FGC_VDI_FAULT) return FGC_FABORT_UNSAFE;

    // The same concept is used for THYR_UNSAFE, which must be acknowledged by an authorized individual.

    if(device.diag.thyr_unsafe.state == FGC_VDI_FAULT) return FGC_THYR_UNSAFE;

    // Request the reset signal to be sent to the VS and force VS_OFF in case it isn't

    digitalIORequestCmd(&device.dig, DIG_IO_VS_RESET);
    digitalIORequestCmd(&device.dig, DIG_IO_VS_OFF);

    // Reset the subconverter lost warning

    device.diag.subconv_lost.state = FGC_VDI_RESET;

    return FGC_OK_NO_RSP;
}

// --------------------------------------- STATE FUNCTIONS ----------------------------------------

// STATE: FLT_OFF (FO)

static uint32_t statePcFO(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check for unexpected switch on of the converter

    latchStatusBit(&device.status.st_faults, FGC_FLT_VS_STATE, digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_RUN | FGC_DIG_STAT_VS_POWER_ON | FGC_DIG_STAT_VS_READY));

    // Check whether we should start a calibration sequence

    if(device.status.state_op == FGC_OP_NORMAL)
    {
        ccSigCalUpdate(channel);
    }

    // Keeping forcing VS_RUN to be off - just in case

    digitalIORequestCmd(&device.dig, DIG_IO_VS_OFF);

    return FGC_PC_FLT_OFF;
}



// STATE: OFF (OF)

static uint32_t statePcOF(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check for unexpected switch on of the converter

    latchStatusBit(&device.status.st_faults, FGC_FLT_VS_STATE, digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_RUN | FGC_DIG_STAT_VS_POWER_ON | FGC_DIG_STAT_VS_READY));

    // Check whether we should start a calibration sequence

    if(device.status.state_op == FGC_OP_NORMAL)
    {
        ccSigCalUpdate(channel);
    }

    return FGC_PC_OFF;
}



// STATE: FLT_STOPPING (FS)

static uint32_t statePcFS(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        // Entering state - Switch off voltage source by reseting VS_RUN command

        digitalIORequestCmd(&equipdev.device[channel].dig, DIG_IO_VS_OFF);

        // Trigger a Postmortem (SELF)

        struct timeval  event_tv;

        timingGetUserTime(0, &event_tv);

        pmTriggerSelf(channel, event_tv.tv_sec, event_tv.tv_usec*1000);

        // Trigger an FGC logger post-mortem event

        loggerTriggerPm(channel);
    }

    return FGC_PC_FLT_STOPPING;
}



// STATE: STOPPING (SP)

static uint32_t statePcSP(uint32_t channel, bool first_call)
{
    return FGC_PC_STOPPING;
}



// STATE: STARTING (ST)

static uint32_t statePcST(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Entering state - Set VS_RUN timeout down counter from VS.VSRUN_TIMEOUT property (seconds as a float)

        device.dig.vs_run_timeout_timer = (uint32_t)(device.vs.vsrun_timeout * 1000.0 / FGCD_CYCLE_PERIOD_MS);

        // Reset MEAS.MAX.U_LEADS

        device.meas.max_u_leads[U_LEAD_POS] = device.meas.max_u_leads[U_LEAD_NEG] = 0;

        // Calibrate the DAC
        //
        // We perform an automatic calibration of the DAC before starting the PC under the following
        // conditions:
        //
        // 1. 1 or 2-quadrant converters have analogue voltage loops which can wind up their integrators
        //    if they see the DAC calibration voltages (+/-8V). If I > I_MIN then the software will
        //    try to ramp the current immediately and this will have a problem with the VLOOP card,
        //    so if the circuit is still discharging, then DAC calibration is suppressed. If I < I_MIN,
        //    the software leaves enough time for the VLOOP to recover and unwind, so the problem is
        //    avoided and the DAC calibration can take place.
        //
        // 2. The RPTK (RF modulator) converters are run in openloop. They have very poor accuracy current
        //    sensors which drift and often I > I_MIN even when off, but the DAC calibration must always
        //    be done as they run in openloop. So if REF.FUNC.REF_MODE[0] is V, then the DAC calibration
        //    will always be done.
        //
        // 3. 4-quadrant converters are not ramped up the way that 1/2-quadrant converters are, so there
        //    is never a problem with the VLOOP integrator and the DAC calibration can always be done.

        if(   device.status.state_op == FGC_OP_NORMAL
           && (   regMgrVarValue(&device.reg_mgr, FLAG_I_MEAS_LOW)        // Condition 1. I < LIMITS.I.LOW
               || regMgrVarValue(&device.reg_mgr, REG_MODE) == FGC_REG_V  // Condition 2. Voltage regulation mode
               || device.reg_mgr.v.lim_ref.flags.unipolar == false))         // Condition 3. 4Q converter
        {
            // Request full automatic DAC calibration sequence

            ccSigCalRequest(channel, CAL_DAC, SIG_CAL_OFFSET);
            ccSigCalRequest(channel, CAL_DAC, SIG_CAL_POSITIVE);
            ccSigCalRequest(channel, CAL_DAC, SIG_CAL_NEGATIVE);

            // Force the start of the calibration

            ccSigCalStart(channel);     // ignore returned status since calibration cannot be running when entering this state
            ccSigCalUpdate(channel);
        }
    }
    else
    {
        // Not the first call

        if(ccSigCalIsCalibrating(channel))
        {
            // DAC calibration is active

            if(device.status.state_op == FGC_OP_NORMAL)
            {
                // Check whether we should finish the calibration sequence

                ccSigCalUpdate(channel);
            }
        }
        else
        {
            // DAC calibration completed

            if(digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_RUN) == 0)
            {
                // VS_RUN not active so Pulse VS RESET and activate VS_RUN

                digitalIORequestCmd(&device.dig, DIG_IO_VS_RESET);
                digitalIORequestCmd(&device.dig, DIG_IO_VS_ON);
            }
            else
            {
                // VS_RUN is active so check for VS.VSRUN_TIMEOUT being exceeded

                if(device.dig.vs_run_timeout_timer > 0 && --device.dig.vs_run_timeout_timer == 0)
                {
                    device.status.st_faults |= FGC_FLT_VS_RUN_TO;
                }
            }
        }
    }

    return FGC_PC_STARTING;
}



// STATE: SLOW_ABORT (SA) - LIBREF STATE: TO_OFF

static uint32_t statePcSA(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        // If the SLOW_ABORT is caused by a lack of PC_PERMIT, then trigger an external Post Mortem

        if(equipdevIsPcPermit(channel) == false)
        {
            struct timeval event_tv;

            timingGetUserTime(0, &event_tv);

            pmTriggerExternal(channel, event_tv.tv_sec, event_tv.tv_usec*1000);
        }
    }
    return FGC_PC_SLOW_ABORT;
}



// STATE: TO_STANDBY (TS) - LIBREF STATE: TO_STANDBY

static uint32_t statePcTS(uint32_t channel, bool first_call)
{
    return FGC_PC_TO_STANDBY;
}



// STATE: ON_STANDBY (SB) - LIBREF STATE: STANDBY

static uint32_t statePcSB(uint32_t channel, bool first_call)
{
    return FGC_PC_ON_STANDBY;
}



// STATE: IDLE (IL) - LIBREF STATE: IDLE

static uint32_t statePcIL(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        equipdev.device[channel].ref.run_now = false;
    }

    return FGC_PC_IDLE;
}



// STATE: TO_CYCLING (TC) - LIBREF STATE: TO_CYCLING

static uint32_t statePcTC(uint32_t channel, bool first_call)
{
    return FGC_PC_TO_CYCLING;
}



// STATE: ARMED (AR) - LIBREF STATE: ARMED

static uint32_t statePcAR(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Start running if run_now flag is set

        if(device.ref.run_now == true)
        {
            // Pass a run_time of zero to request event in 1s

            struct CC_us_time run_time = { 0 };

            refMgrParValue(&device.ref_mgr, REF_RUN) = run_time;

            refEventRun(&device.ref_mgr);

            fprintf(stderr,"statePcAR: channel=%u  run_now\n", channel);
        }
    }

    return FGC_PC_ARMED;
}



// STATE: RUNNING (RN) - LIBREF STATE: RUNNING

static uint32_t statePcRN(uint32_t channel, bool first_call)
{
    return FGC_PC_RUNNING;
}



// STATE: ABORTING (AB) - LIBREF STATE: TO_IDLE

static uint32_t statePcAB(uint32_t channel, bool first_call)
{
    return FGC_PC_ABORTING;
}



// STATE: CYCLING (CY) - LIBREF STATE: CYCLING

static uint32_t statePcCY(uint32_t channel, bool first_call)
{
    return FGC_PC_CYCLING;
}



// STATE: BLOCKING (BK) - LHC converters do not support BLOCKING but this state must be passed through

static uint32_t statePcBK(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call && refMgrParValue(&device.ref_mgr, MODE_REF) == REF_OFF)
    {
        // Remove VS_RUN to turn off the converter

        digitalIORequestCmd(&device.dig, DIG_IO_VS_OFF);
    }

    return FGC_PC_BLOCKING;
}



// STATE: ECONOMY (EC) - LIBREG STATES: DYN_ECO and FULL_ECO

static uint32_t statePcEC(uint32_t channel, bool first_call)
{
    return FGC_PC_ECONOMY;
}



// State machine initialization

void statePcInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize channel state to be OFF

    device.status.state_pc   = FGC_PC_FLT_OFF;
    device.state_pc_bit_mask = FGC_PC_FLT_OFF_BIT_MASK;
    device.state_pc_func     = statePcFO;

    // Set default value for LOG.MENUS.MODE_PC_ON (IDLE for Class 92)

    device.mode_pc_on = FGC_PC_ON_IDLE;
}



// ------------------------------ TRANSITION CONDITION FUNCTIONS ----------------------------------

// TRANSITION: ANY LIBREF STATE to ANY LIBREF STATE

CC_STATIC_ASSERT(REF_OFF              ==  0, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_POL_SWITCHING    ==  1, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_OFF           ==  2, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_STANDBY       ==  3, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_CYCLING       ==  4, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_IDLE          ==  5, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_DIRECT           ==  6, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_STANDBY          ==  7, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_CYCLING          ==  8, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_DYN_ECO          ==  9, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_FULL_ECO         == 10, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_PAUSED           == 11, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_IDLE             == 12, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_ARMED            == 13, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_RUNNING          == 14, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_NUM_STATES       == 15, Unexpected_REF_STATE_constant_value);

static State * XXtoXX(uint32_t channel)
{
    // Mapping between libref ref_state and state_pc - this must match the order of the ref state
    // constants, so they are checked above with static assert in case libref is changed in future.

    static State * ref_state_to_state_pc[] =
    {
        statePcBK ,     //   0. [REF_OFF          ]
        statePcFS ,     //   1. [REF_POL_SWITCHING] - not implemented to jump to FLT_STOPPING
        statePcSA ,     //   2. [REF_TO_OFF       ]
        statePcTS ,     //   3. [REF_TO_STANDBY   ]
        statePcTC ,     //   4. [REF_TO_CYCLING   ]
        statePcAB ,     //   5. [REF_TO_IDLE      ]
        statePcFS ,     //   6. [REF_DIRECT       ] - not implemented to jump to FLT_STOPPING
        statePcSB ,     //   7. [REF_STANDBY      ]
        statePcCY ,     //   8. [REF_CYCLING      ]
        statePcEC ,     //   9. [REF_DYN_ECO      ]
        statePcEC ,     //  10. [REF_FULL_ECO     ]
        statePcFS ,     //  11. [REF_PAUSED       ] - not implemented to jump to FLT_STOPPING
        statePcIL ,     //  12. [REF_IDLE         ]
        statePcAR ,     //  13. [REF_ARMED        ]
        statePcRN       //  14. [REF_RUNNING      ]
    };

    struct Equipdev_channel &device = equipdev.device[channel];

    // Look up STATE.PC state function that corresponds to the current libref ref_state

    const State * next_state_pc_func = ref_state_to_state_pc[refMgrVarValue(&device.ref_mgr, REF_STATE)];

    if(next_state_pc_func == statePcFS)
    {
        // libref has returned an unimplemented state for FGClite - jump to FLT_STOPPING

        logPrintf(channel, "libref REF state machine returned unimplemented state %u\n", refMgrVarValue(&device.ref_mgr, REF_STATE));
    }

    // Return pointer to the new state function only if it has changed

    if(next_state_pc_func != device.state_pc_func)
    {
        return next_state_pc_func;
    }

    // Return NULL to indicate that state has not changed

    return NULL;
}



// TRANSITION: ANY STATE to FLT_STOPPING

static State * XXtoFS(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults != 0 ||
       digitalIOGetStatus(&equipdev.device[channel].dig, FGC_DIG_STAT_PWR_FAILURE | FGC_DIG_STAT_FAST_ABORT) == true)
    {
        return statePcFS;
    }

    return NULL;
}



// TRANSITION: FLT_STOPPING to FLT_OFF

static State * FStoFO(uint32_t channel)
{
    if(digitalIOGetStatus(&equipdev.device[channel].dig, FGC_DIG_STAT_VS_POWER_ON) == false)
    {
        return statePcFO;
    }

    return NULL;
}



// TRANSITION: FLT_STOPPING to STOPPING

static State * FStoSP(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults == 0)
    {
        return statePcSP;
    }

    return NULL;
}



// TRANSITION: STOPPING to OFF

static State * SPtoOF(uint32_t channel)
{
    if(digitalIOGetStatus(&equipdev.device[channel].dig, FGC_DIG_STAT_VS_POWER_ON) == false)
    {
        return statePcOF;
    }

    return NULL;
}



// TRANSITION: FLT_OFF to OFF

static State * FOtoOF(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults == 0)
    {
        return statePcOF;
    }

    return NULL;
}



// TRANSITION: OFF to FLT_OFF

static State * OFtoFO(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults != 0)
    {
        return statePcFO;
    }

    return NULL;
}



// TRANSITION: OFF to STARTING

static State * OFtoST(uint32_t channel)
{
    if(refMgrParValue(&equipdev.device[channel].ref_mgr,MODE_REF) != REF_OFF)
    {
        return statePcST;
    }

    return NULL;
}



// TRANSITION: STARTING to STOPPING

static State * STtoSP(uint32_t channel)
{
    if(refMgrParValue(&equipdev.device[channel].ref_mgr,MODE_REF) == REF_OFF)
    {
        return statePcSP;
    }

    return NULL;
}




// TRANSITION: STARTING to BLOCKING

static State * STtoBK(uint32_t channel)
{
    if(equipdev.device[channel].status.state_vs == FGC_VS_READY)
    {
        return statePcBK;
    }

    return NULL;
}



// TRANSITION: BLOCKING to STOPPING

static State * BKtoSP(uint32_t channel)
{
    if(digitalIOGetStatus(&equipdev.device[channel].dig, FGC_DIG_STAT_VS_RUN) == 0)
    {
        return statePcSP;
    }

    return NULL;
}



// TRANSITION: ARMED to XX

static State * ARtoXX(uint32_t channel)
{
    State * next_state = XXtoXX(channel);

    if(   next_state == statePcRN
       && refMgrVarValue(&equipdev.device[channel].ref_mgr, REF_FG_STATUS) == FG_PRE_FUNC)
    {
        next_state = NULL;
    }

    return next_state;
}


// State transition functions : called in priority order, from left to right, return the state function.
//
// The initialization order of StateTransitions must match the STATE.PC constant values from the XML symlist.
// This is checked with static assertions.

CC_STATIC_ASSERT(FGC_PC_FLT_OFF       ==  0, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_OFF           ==  1, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_FLT_STOPPING  ==  2, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_STOPPING      ==  3, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_STARTING      ==  4, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_SLOW_ABORT    ==  5, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_TO_STANDBY    ==  6, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ON_STANDBY    ==  7, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_IDLE          ==  8, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_TO_CYCLING    ==  9, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ARMED         == 10, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_RUNNING       == 11, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ABORTING      == 12, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_CYCLING       == 13, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_POL_SWITCHING == 14, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_BLOCKING      == 15, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ECONOMY       == 16, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_DIRECT        == 17, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_PAUSED        == 18, Unexpected_STATE_PC_constant_value);

#define REF_MAX_TRANSITIONS     3      //!< Maximum transitions from any state

static  Transition * StateTransitions[][REF_MAX_TRANSITIONS + 1] =
{                                                            // LIBREF STATE    Used in FGClite
    /*  0.FO */ {                 FOtoOF,         NULL },    //      NO               YES
    /*  1.OF */ {                 OFtoFO, OFtoST, NULL },    //      NO               YES
    /*  2.FS */ {                 FStoFO, FStoSP, NULL },    //      NO               YES
    /*  3.SP */ { XXtoFS,         SPtoOF,         NULL },    //      NO               YES
    /*  4.ST */ { XXtoFS,         STtoSP, STtoBK, NULL },    //      NO               YES
    /*  5.SA */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  6.TS */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  7.SB */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  8.IL */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  9.TC */ {                                 NULL },    //      YES              NO
    /* 10.AR */ { XXtoFS, ARtoXX,                 NULL },    //      YES              YES
    /* 11.RN */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 12.AB */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 13.CY */ {                                 NULL },    //      YES              NO
    /* 14.PL */ {                                 NULL },    //      YES              NO
    /* 15.BK */ { XXtoFS, XXtoXX, BKtoSP,         NULL },    //      NO               YES
    /* 16.EC */ {                                 NULL },    //      YES              NO
    /* 17.DT */ {                                 NULL },    //      YES              NO
    /* 18.PA */ {                                 NULL }     //      YES              NO
};



void statePcRT(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    State       * next_state        = NULL;
    Transition ** state_transitions = &StateTransitions[device.status.state_pc][0];
    Transition  * transition;

    // Scan transitions for the current state, testing each condition in priority order

    while((transition = *(state_transitions++)) != NULL &&      // there is another transition function, and
          (next_state = transition(channel)) == NULL)           // it return NULL (transition condition is false)
    {                                                           // then loop doing nothing
    }

    // If a transition condition was true then switch to the new state

    if(next_state != NULL)
    {
        // Run new state function with first_call flag set to true

        device.status.state_pc = next_state(channel, true);     // State function returns the state enum

        // Cache the state function and bit mask

        device.state_pc_func     = next_state;
        device.state_pc_bit_mask = 1 << device.status.state_pc;

        // Reset the time since last state change

        device.state_pc_time_ms = 0;

        // Reset the reference event group if the new state is not ARMED or RUNNING

        if((device.state_pc_bit_mask & (FGC_PC_ARMED_BIT_MASK | FGC_PC_RUNNING_BIT_MASK)) == 0)
        {
            device.ref.event_group = 0;
        }

        // Log state change

        if((device.state_pc_bit_mask & (FGC_PC_FLT_OFF_BIT_MASK | FGC_PC_FLT_STOPPING_BIT_MASK)) != 0)
        {
            char buffer[LOG_MESSAGE_LENGTH + 1];

            size_t pos = sprintf(buffer, "STATE_PC = %s   dig.status = 0x%04X   st_faults=0x%04hX :", sym_names_pc[device.status.state_pc].label, device.dig.status, device.status.st_faults);

            // Print the labels for all the fault bits that are set

            for(uint32_t i = 0; sym_names_flt[i].label; ++i)
            {
                if (sym_names_flt[i].type & device.status.st_faults)
                {
                    pos += snprintf(buffer+pos, LOG_MESSAGE_LENGTH-10-pos, " %s", sym_names_flt[i].label);
                }
            }
            sprintf(buffer+pos, "\n");

            logPrintf(channel, buffer);
        }
        else
        {
            logPrintf(channel, "STATE_PC = %s   dig.status = 0x%04X   ref_state=%u  ref_mgr.iter_cache.pc_state=%u\n",
                        sym_names_pc[device.status.state_pc].label,
                        device.dig.status,
                        refMgrVarValue(&device.ref_mgr, REF_STATE),
                        device.ref_mgr.iter_cache.pc_state);
        }
    }
    else
    {
        device.state_pc_time_ms += FGCD_CYCLE_PERIOD_MS;

        device.state_pc_func(channel, false);
    }
}

// EOF
