/*
 * @file   dim_state.cpp
 * @brief  FGClite DIM trigger state machine
 * @author Michael Davis
 */

//#define DEBUG

#include <codes.h>
#include <equipdev.h>
#include <fgcd_thread.h>
#include <logging.h>
#include <timing.h>
#include <equip_liblog.h>
#include <log.h>
#include <dim_state.h>
#include <dim_bus.h>
#include <equip_logger.h>
#include <bitset>

// Constants

const uint32_t DIM_BUFFER_SIZE        = 2048;                  // Total number of samples per DIM channel. 2048 samples @ 50 Hz = 40.96s
const uint32_t DIM_TRIGGER_START_TIME =  250;                  // Start logging 5 seconds before the trigger = 250 cycles
const uint32_t DIM_TRIGGER_END_TIME   =   53;                  // End logging 1 second after the trigger = 50 cycles, rounded up so NUM_CYCLES is divisible by 8

// Types for state and transition functions

typedef enum State_DIM_trigger StateFunc(uint32_t channel, uint8_t dim_num, uint16_t dim_log_index, struct FGClite_command *ctrl_cmd, bool first_call);
typedef StateFunc            * TransFunc(uint32_t channel, uint8_t dim_num);

// Structures

// State of a DIM

struct DIM_state
{
    uint16_t                      log_index;                   // Log index for the next analogue sample to be read out from this DIM
    uint16_t                      log_index_end;               // Log index on which to stop logging (after a trigger)

    enum State_DIM_trigger        trigger_state;               // State of the DIM trigger
    StateFunc                   * state_function;              // Current DIM trigger state function pointer
    struct timeval                timestamp;                   // Time that the trigger occurred
    int32_t                       timestamp_adjust_us;         // Timestamp adjust in us within the FIP cycle

    bool                          UT_value_at_trigger_time;    // Value of UT bit during cycle where trigger arrived
};


// Array of all DIMs for a device

using std::bitset;

struct DIM_array
{
    struct timeval                reset_time;                   // Scheduled time for the next DIM reset

    uint16_t                      active_mask;                  // Bitmask of which DIM channels are currently logging analogue data
    bitset<DIM_BUS_MAX_LOG_DEVS>  requiring_reset_mask;         // Bitmask of which DIM channels require a reset

    struct DIM_state              state [DIM_BUS_MAX_LOG_DEVS]; // State of DIM logging and triggers

    uint16_t                      dim_a_trig_unl;               // Translation of the critical data from dim bus addresses to logical addresses
    uint16_t                      dim_a_trig_lat;               // Translation of the critical data from dim bus addresses to logical addresses
};

// Global variables, zero initialised

struct DIM_array dim_array[FGCD_MAX_DEVS] = {};

// Static functions

static uint16_t dimStateFromBusToLogicalMask(uint32_t channel, uint16_t dim_addr_mask)
{
    struct Equipdev_channel &device      = equipdev.device[channel];
    uint16_t                 dim_num_mask = 0;

    for(uint8_t bus_addr=0; bus_addr != FGC_MAX_REAL_DIMS; ++bus_addr)
    {
        uint32_t dim_num = device.diag.dim_num[bus_addr];

        if(dim_addr_mask & (1 << bus_addr))
        {
            dim_num_mask |= (1 << dim_num);
        }
    }

    return dim_num_mask;
}

// State function: RUNNING

static enum State_DIM_trigger stateDimRunning(uint32_t                 const channel,
                                              uint8_t                  const dim_num,
                                              uint16_t                 const dim_log_index,
                                              struct FGClite_command * const ctrl_cmd,
                                              bool                     const first_call)
{
    if(first_call)
    {
#ifdef DEBUG
        logPrintf(channel, "stateDimRN: DIM %d is RUNNING\n", dim_num);
#endif
    }

    return DIM_RUNNING;
}



// State function: TRIGGERED

static enum State_DIM_trigger stateDimTriggered(uint32_t                 const channel,
                                                uint8_t                  const dim_num,
                                                uint16_t                 const dim_log_index,
                                                struct FGClite_command * const ctrl_cmd,
                                                bool                     const first_call)
{
    struct DIM_state &dim         = dim_array[channel].state[dim_num];
    uint16_t         &active_mask = dim_array[channel].active_mask;
    struct timeval   &reset_time  = dim_array[channel].reset_time;

    if(first_call)
    {
#ifdef DEBUG
        logPrintf(channel, "stateDimTriggered: DIM %d has TRIGGERED\n", dim_num);
#endif
        // Record the cycle time of the DIM trigger

        timingGetUserTime(0, &dim.timestamp);

        // Remember whether the corresponding Unlatched Trigger (UT) bit was also set

        dim.UT_value_at_trigger_time = ((dim_array[channel].dim_a_trig_unl & (1 << dim_num)) != 0);

        // Remove 20ms of delay of the fieldbus

        struct timeval timestamp_adjust = {0,-20000};
        timevalAdd(&dim.timestamp, &timestamp_adjust);

        // Set the reset time for two seconds in the future. If more DIMs trigger within this two
        // second period, the reset will take place two seconds after the last trigger.

        reset_time.tv_sec  = dim.timestamp.tv_sec + 2;
        reset_time.tv_usec = dim.timestamp.tv_usec;

        // Abandon the current analogue log for this DIM

        enum LOG_index dim_log_idx = static_cast<enum LOG_index>(LOG_DIM + dim_num);

        ccLogEndDiscontinuous(channel, dim_log_idx, -1, NULL);

        /*
         * Reset the log index for this DIM to log the analogue data before and after the trigger.
         * log_index_end is set to the index of the first sample in the last page of this log. The
         * FGClite returns 8 samples per page, so the total number of samples must be divisible by 8.
         */

        const uint32_t NUM_SAMPLES = DIM_TRIGGER_START_TIME + 1 + DIM_TRIGGER_END_TIME;

        assert(NUM_SAMPLES % 8 == 0);

        dim.log_index     = (dim_log_index + DIM_BUFFER_SIZE - DIM_TRIGGER_START_TIME) % DIM_BUFFER_SIZE;
        dim.log_index_end = (dim.log_index + NUM_SAMPLES     - 8)                      % DIM_BUFFER_SIZE;

        // Ensure the active bit is set

        active_mask |= (1 << dim_num);

        // Restart logging for this DIM at the trigger time

        ccLogDimStart(channel, dim_num, NUM_SAMPLES, -20 * DIM_TRIGGER_START_TIME);

        // Clear digital mask

        dimBusClearDigitalMask(channel, dim_num);

        // Trigger the logger to collect the DIM data once it's retrieved

        // 2021-01-17 : This is not yet activated because Class 92 DIM logs are discontinuous and it is much
        // more complicated that for FGC51 and FGC63.

        // loggerTriggerDim(channel, LOG_MENU_DIM + dim_num);
    }

    return DIM_TRIGGERED;
}



// State function: RESETTING

static enum State_DIM_trigger stateDimResetting(uint32_t                 const channel,
                                                uint8_t                  const dim_num,
                                                uint16_t                 const dim_log_index,
                                                struct FGClite_command * const ctrl_cmd,
                                                bool                     const first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct DIM_state        &dim    = dim_array[channel].state[dim_num];

    if(first_call)
    {
#ifdef DEBUG
        logPrintf(channel, "stateDimRS: DIM %d  dim.timestamp=%ld.%06ld  dim.timestamp_adjust_us=%d us\n", dim_num, dim.timestamp.tv_sec, dim.timestamp.tv_usec, dim.timestamp_adjust_us);
#endif
        dim.timestamp.tv_usec      += dim.timestamp_adjust_us;
        device.diag.trigus[dim_num]  = dim.timestamp.tv_usec;
#ifdef DEBUG
        logPrintf(channel, "stateDimRS: DIM %d  dim.timestamp=%ld.%06ld\n", dim_num, dim.timestamp.tv_sec, dim.timestamp.tv_usec);
#endif
        dim.timestamp_adjust_us = 0;
    }

    // Set the DIM_RESET bit and clear the reset time

    dim_array[channel].requiring_reset_mask[dim_num] = true;

    return DIM_RESETTING;
}



// State function: POLLING

static enum State_DIM_trigger stateDimPolling(uint32_t                 const channel,
                                              uint8_t                  const dim_num,
                                              uint16_t                 const dim_log_index,
                                              struct FGClite_command * const ctrl_cmd,
                                              bool                     const first_call)
{
    uint16_t &dim_a_trig_lat = dim_array[channel].dim_a_trig_lat;

    if(first_call)
    {
#ifdef DEBUG
        logPrintf(channel, "stateDimPolling: DIM %d is POLLING\n", dim_num);
#endif
    }

    // While POLLING remove the additional triggers

    if((1 << dim_num) & dim_a_trig_lat)
    {
#ifdef DEBUG
        logPrintf(channel, "Channel %d Sending DIM_RESET to all DIMs\n", channel);
#endif
        // Set the DIM_RESET bit and clear the reset time

        dim_array[channel].requiring_reset_mask[dim_num] = true;
    }
    else
    {
        // Clear the DIM_RESET bit

#ifdef DEBUG
        if (dim_array[channel].requiring_reset_mask[dim_num] == true) {
            logPrintf(channel, "DIM %d Clearing DIM_RESET\n", dim_num);
        }
#endif

        dim_array[channel].requiring_reset_mask[dim_num] = false;

    }

    return DIM_POLLING;
}



// Transition functions

// Condition for transition from RUNNING to TRIGGERED

static StateFunc* RNtoTG(uint32_t const channel, uint8_t const dim_num)
{
    uint16_t &dim_a_trig_lat = dim_array[channel].dim_a_trig_lat;

    // If the latched bit is set, transition to TRIGGERED

    if((1 << dim_num) & dim_a_trig_lat) return stateDimTriggered;

    return NULL;
}



// Condition for transition from TRIGGERED to RESETTING

static StateFunc* TGtoRS(uint32_t const channel, uint8_t const dim_num)
{
    struct timeval &reset_time   = dim_array[channel].reset_time;

    struct timeval timeNow;

    timingGetUserTime(0, &timeNow);

    if(timevalIsLessThan(&reset_time, &timeNow))
    {
        return stateDimResetting;
    }

    return NULL;
}



// Condition for transition from RESETTING to POLLING

static StateFunc* RStoPL(uint32_t const channel, uint8_t const dim_num)
{
    uint16_t &dim_a_trig_lat = dim_array[channel].dim_a_trig_lat;

    if((1 << dim_num) & dim_a_trig_lat) return NULL;

    // When the DIM has been reset (= latched bit unset), transition to POLLING

    return stateDimPolling;
}



// Condition for transition from POLLING to RUNNING

static StateFunc* PLtoRN(uint32_t const channel, uint8_t const dim_num)
{
    uint16_t &active_mask    = dim_array[channel].active_mask;
    uint16_t &dim_a_trig_unl = dim_array[channel].dim_a_trig_unl;
    uint16_t &dim_a_trig_lat = dim_array[channel].dim_a_trig_lat;

    if((1 << dim_num) & (dim_a_trig_unl | dim_a_trig_lat | active_mask)) return NULL;

   // When the Voltage Source fault has been cleared (= unlatched bit unset) and the DIM has finished its
   // post-trigger logging (= active_mask bit unset), then transition to RUNNING.

    return stateDimRunning;
}

// Transition functions : called in priority order, from left to right

#define MAX_TRANSITIONS     1      // Maximum transitions from a state

static const TransFunc * transitions[][MAX_TRANSITIONS + 1] =
{
    /* RUNNNING   RN */ { RNtoTG, NULL },
    /* TRIGGERED  TG */ { TGtoRS, NULL },
    /* RESETTING  RS */ { RStoPL, NULL },
    /* POLLING    PL */ { PLtoRN, NULL }
};

// Update the DIM trigger state for a single DIM

static void dimStateUpdateTrigger(uint32_t channel, uint8_t dim_num, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd)
{
    struct DIM_state &dim = dim_array[channel].state[dim_num];

    // Scan transitions for the current state, testing each condition in priority order

    StateFunc  * next_state = NULL;                                        // pointer to the next state function
    TransFunc  * transition;                                               // pointer to the next transition function to test
    TransFunc ** state_transitions = &transitions[dim.trigger_state][0];

    // Call transition condition functions for the current state, testing each condition in priority order

    while((transition = *(state_transitions++)) != NULL && (next_state = (*transition)(channel,dim_num)) == NULL)
    {
    }

    if(next_state != NULL)
    {
        dim.state_function = next_state;

        dim.trigger_state = next_state(channel, dim_num, status->critical.dim_log_index, ctrl_cmd, true);


#ifdef DEBUG
        logPrintf(channel, "dimStateUpdateTrigger: New state: %u\n", dim.trigger_state);
#endif

    }
    else
    {
        // If state_function was never defined, start in RUNNING state

        if(dim.state_function == NULL)
        {
            dim.state_function = stateDimRunning;
        }

        // No transition condition is true so stay in the current state and run the state function

        dim.state_function(channel, dim_num, status->critical.dim_log_index, ctrl_cmd, false);
    }

    // Check if we should set the FGC_LAT_DIM_TRIG_FLT flag

    struct Equipdev_channel &device = equipdev.device[channel];

    if(   dimStateIsUnlatched(channel, dim_num)                        == true      // If a DIM has an unlatched trigger and
       && digitalIOGetStatus(&device.dig,FGC_DIG_STAT_VS_RUN)          == true      // VSRUN is active and
       && getStatusBit(device.status.st_faults, FGC_FLT_VS_FAULT)      == false     // no VS_FAULT is present and
       && getStatusBit(device.status.st_faults, FGC_FLT_VS_EXTINTLOCK) == false     // no VS_EXTINTLOCK is present and
       && getStatusBit(device.status.st_faults, FGC_FLT_FAST_ABORT)    == false     // no FAST_ABORT is present and
       && getStatusBit(device.status.st_latched, FGC_LAT_DIM_TRIG_FLT) == false)    // the DIM_TRIG_FLT is not yet set
    {
        setStatusBit(&device.status.st_latched, FGC_LAT_DIM_TRIG_FLT, true);
    }
}



// External functions

void dimStateReset(uint32_t channel)
{
    // NOTE: This function is called from prop_equip.cpp, so concurrency is potentially an issue
    // (command may not always work)

    timingGetUserTime(0, &dim_array[channel].reset_time);
}



void dimStateInitActiveList(uint32_t channel, uint16_t dim_mask, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd)
{
    uint16_t &active_mask    = dim_array[channel].active_mask;
    uint16_t &dim_a_trig_unl = dim_array[channel].dim_a_trig_unl;
    uint16_t &dim_a_trig_lat = dim_array[channel].dim_a_trig_lat;

    // Treat DIMs that we do not log as if they are not present

    dim_mask &= (0xFFFF >> (16-DIM_BUS_MAX_LOG_DEVS));

    // Set the active mask to include all detected DIMs which do not have the latched or unlatched bits set.

    uint16_t running_mask = dim_mask & ~(dim_a_trig_lat | dim_a_trig_unl);

    active_mask &= dim_mask;
    active_mask |= running_mask;

    // Initialise the state of the DIM triggers for active DIMs to RUNNING, or RESETTING otherwise

    for(uint32_t dim_num = 0; dim_num < DIM_BUS_MAX_LOG_DEVS; ++dim_num)
    {
        struct DIM_state &dim = dim_array[channel].state[dim_num];

        if((1 << dim_num) & running_mask)
        {
            dim.trigger_state = stateDimRunning(channel, dim_num, status->critical.dim_log_index, ctrl_cmd, true);
        }
        else
        {
            dimStateReset(channel);

            dim.trigger_state = stateDimTriggered(channel, dim_num, status->critical.dim_log_index, ctrl_cmd, true);
        }
    }
}



void dimStateResetActiveList(uint32_t channel)
{
   dim_array[channel].active_mask = 0;
}



uint16_t dimStateGetActiveList(uint32_t channel)
{
    return dim_array[channel].active_mask;
}



void dimStateSetInactive(uint32_t channel, uint8_t dim_num)
{
    struct DIM_state &dim         = dim_array[channel].state[dim_num];
    uint16_t         &active_mask = dim_array[channel].active_mask;

    if(dim.trigger_state != DIM_RUNNING)
    {
        // Unset the active bit for this DIM

        active_mask &= ~(1 << dim_num);

#ifdef DEBUG
    logPrintf(channel, "Channel %d DIM %d set to INACTIVE: stopping DIM logging\n", channel, dim_num);
#endif
    }
}



bool dimStateIsLatched(uint32_t channel, uint8_t dim_num)
{
    struct DIM_state &dim      = dim_array[channel].state[dim_num];

    return (dim.trigger_state == DIM_TRIGGERED || dim.trigger_state == DIM_RESETTING);
}



bool dimStateIsUnlatched(uint32_t channel, uint8_t dim_num)
{
    struct DIM_state &dim = dim_array[channel].state[dim_num];

    return dim.trigger_state != DIM_RUNNING;
}



enum State_DIM_trigger dimStateCheckTrigger(uint32_t channel, uint8_t dim_num, uint16_t trigger_count, struct timeval *trigger_time)
{
    struct DIM_state &dim = dim_array[channel].state[dim_num];

    if(dim.trigger_state == DIM_TRIGGERED)
    {
        /*
         * The timestamp for events while in TRIGGERED state is the cycle timestamp when the trigger
         * occurred, adjusted by the trigger_count received from the DIM.
         *
         * When the external trigger is asserted, the DIM waits for 1μs before freezing the digital
         * input levels and its internal counter. The DIM counter has a resolution of 8μs and is
         * reset to zero at the start of each read-out cycle. In other words, the DIM counter is
         * used to calculate the event time of the fault as an offset from the start of the current
         * FIP cycle.
         *
         * To convert the trigger_count to μs, we multiply the counter by 8 and add 3 to
         * place it in the middle of the 8μs period. This gives an accuracy of +/- 4μs.
         *
         *
         * Behold this illustrative ASCII painting:
         *
         *
         * t:0                ~407 (3.2 ms)             2500 (20 ms; rolls over to 0)
         *   v                v                          v
         *   |------T---------ccc1-----------------------|-------------------------------------------|
         *   ^      ^         ^                          ^
         *   |      |         |                          information is transmitted to Front-End
         *   |      |         COUNTER + Latched Trigger are read out
         *   |      trigger arrives; counter is frozen
         *   start of bus cycle
         *
         *
         * The counter value, along with the latched trigger (LT) bit is sampled at the moment when
         * the FGClite starts to read the 1st bit of corresponding register ("t_reg6").
         * This occurs approximately 407 counts, or 3.2 ms, into the bus cycle; the exact time is
         * undefined, and on FGC2 it can jitter considerably as the read-out is triggered by software.
         *
         * If a trigger arrives after this moment, the FGClite will not see it until the following bus
         * cycle:
         *
         *
         *   |               (cycle N-1)                 |                (cycle N)                  |
         *   |                                           |                                           |
         *   |----------------ccc0--------------T--------|----------------ccc1-----------------------|
         *                    ^                 ^                         ^
         *                    |                 trigger arrives           |
         *                    COUNTER + Latched Trigger bit  are  read  out
         *
         *
         * Having only this information, to distinguish between cycles N-1 and N, it would be necessary
         * to precisely know t_reg6.
         * To relax this requirement, the DIM also transmits an unlatched trigger (UT) status in each
         * of the 6 registers preceding COUNTER.
         * FGC2 uses the value of the first of these (transmitted early in the bus cycle) to determine
         * if the trigger had already been active at the start of the cycle.
         *
         *   |-xxxU-xxxU-xxxU-xxxU-xxxU-xxxU-cccL----------------------------------------------------|
         *     ^    ^    ^    ^    ^    ^    ^
         *     UT sampled & sent  6 times    latched trigger sampled and transmitted once
         *     (along with registers 0..5)
         *
         *
         *   |-xxx0-T---------ccc1-----------------------|-------------------------------------------|
         *          ^
         *          |
         *          trigger arrives between sampling of 1st UT (0) and LT (1)
         *          we thus know for a fact that it occurred in the same cycle
         *
         *
         * Presumably due to a design oversight in FGClite, hovewer, only the 6th UT is captured and
         * transmitted to the front-end.
         * This allows us to detect when a trigger occured during the (512+32)μs time span between the
         * sampling of the registers 5 and 6.
         * Beyond that, we have to rely on approximately knowing t_reg6, to estimate, for any value of
         * COUNTER, whether it arrived before this interval (in cycle N), or after it (in cycle N-1).
         *
         *
         *   |---T-------xxx1-ccc1-----------------------|-------------------------------------------|
         *       ^
         *       trigger arrives before read-out of 6th UT
         *
         *
         *         critical interval
         *                 v
         *               *****
         *   |-----------xTx0-ccc1-----------------------|-------------------------------------------|
         *                ^
         *                |
         *                trigger arrives in the critical interval between sampling of UT (0) and LT (1)
         *                (during read-out of register 5 or in the subsequent inter-register gap)
         *
         *
         *   |-----------xxx0-ccc0--------------T--------|-----xxx1-------ccc1-----------------------|
         *                                      ^                 ^          ^
         *                                      |                 in the following cycle,
         *                                      |                 UT and LT both read as 1
         *                                      trigger arrives too late within cycle
         */

        *trigger_time = dim.timestamp;

        dim.timestamp_adjust_us = trigger_count * 8 + 3;

        if (dim.UT_value_at_trigger_time == false)
        {
            // If UNL = 0, trigger arrived during the critical interval. Thus, it must have occurred in
            // the same cycle and the timestamp is correct as-is.
            //
            // (Note that due to data paging on the bus, this code may be called several cycles after the
            //  trigger detection. It is therefore necessary to save the UNL value in stateDimTriggered)
        }
        else
        {
            // Subtract half or register read duration to put the threshold in the midpoint of the
            // critical interval
            if (trigger_count >= (FGC_DIM_COUNTER_TICKS_UNTIL_READ - FGC_DIM_REGISTER_READ_TIME / 2))
            {
                // Trigger occurred in the preceding cycle - apply correction
                dim.timestamp_adjust_us -= 20000;
            }
        }

        // Return the high-resolution time of the DIM trigger event

        struct timeval timestamp_adjust = { 0, dim.timestamp_adjust_us };

#ifdef DEBUG
        logPrintf(channel, "dimStateCheckTrigger: DIM %d trigger_time%ld.%06ld timestamp_adjust_us=%d  trigger_count=%d\n",
                  dim_num, trigger_time->tv_sec, trigger_time->tv_usec, dim.timestamp_adjust_us, trigger_count);
#endif

        timevalAdd(trigger_time, &timestamp_adjust);
    }
    else
    {
        // The timestamp for events while in POLLING state is the FIP cycle time

        timingGetUserTime(0, trigger_time);
    }

    return dim.trigger_state;
}



bool dimStateCheckOverrun(uint32_t channel, uint16_t dim_log_index, uint8_t *dim_num)
{
    for(*dim_num = 0; *dim_num < DIM_BUS_MAX_LOG_DEVS; (*dim_num)++)
    {
        uint16_t &active_mask = dim_array[channel].active_mask;

        if((1 << *dim_num) & active_mask)
        {
            struct DIM_state &dim = dim_array[channel].state[*dim_num];

            uint16_t log_index_relative = (dim_log_index + DIM_BUFFER_SIZE - dim.log_index) % DIM_BUFFER_SIZE;

            if(log_index_relative < 8)
            {
                // DIM log buffer has overrun. This should not happen.
#ifdef DEBUG
                logPrintf(channel, "dimStateCheckOverrun: DIM %d reading = %d, writing = %d\n", *dim_num, dim.log_index, dim_log_index);
#endif
                return true;
            }
        }
    }

    return false;
}



void dimStateResetLogIndices(uint32_t channel, uint32_t num_samples, uint16_t dim_log_index)
{
    uint16_t &active_mask = dim_array[channel].active_mask;

    for(uint32_t dim_num = 0; dim_num < DIM_BUS_MAX_LOG_DEVS; ++dim_num)
    {
        struct DIM_state &dim = dim_array[channel].state[dim_num];

        // For all DIMs in RUNNING state, reset the log index and ensure the DIM is in the active list

        if(dim.trigger_state == DIM_RUNNING)
        {
            active_mask |= (1 << dim_num);

            dim.log_index     = (dim_log_index + DIM_BUFFER_SIZE - 7) % DIM_BUFFER_SIZE;
            dim.log_index_end = (dim.log_index + num_samples     - 8) % DIM_BUFFER_SIZE;
#ifdef DEBUG
            logPrintf(channel, "dimStateResetLogIndices: DIM %d reset DIM_LOG_INDEX = [%d,%d]\n", dim_num, dim.log_index, dim.log_index_end);
#endif

            /*
             * Start the next DIM log
             *
             * Note: DIM_LOG_INDEX is the index of the last sample that was written, which completes on the
             *       20ms FIP cycle boundary. So we timestamp this sample with the current FIP cycle time.
             *       DIM samples are stored at 50 Hz and we are reading 8 samples at a time, so the timestamp
             *       of the first sample in the log is offset from the current FIP cycle time by 20 x 7 = 140ms.
             */

            ccLogDimStart(channel, dim_num, num_samples, -140);
        }
    }
}



uint16_t dimStateRequestPage(uint32_t channel, uint8_t dim_num, struct FGClite_command *ctrl_cmd)
{
    struct DIM_state &dim         = dim_array[channel].state[dim_num];
    uint16_t         &active_mask = dim_array[channel].active_mask;

    // Retrieve log index

    uint16_t requested_index = dim.log_index;

    // Request the next page of paged data:
    // Top 4 bits     => DIM number
    // Bottom 12 bits => sample address

    fgcliteCmdRequestCtrl(ctrl_cmd, channel, DIM_LOG, (dim_num << 11) | requested_index);

    // Increment the DIM log index

    dim.log_index = (dim.log_index + 8) % DIM_BUFFER_SIZE;

    // If we have requested the last page for this run, set the DIM log state to inactive

    if(requested_index == dim.log_index_end)
    {
        active_mask &= ~(1 << dim_num);
    }

    return requested_index;
}



void dimStateUpdate(uint32_t channel, uint16_t dim_mask, const struct FGClite_status *status, struct FGClite_command *ctrl_cmd)
{
    // Translate latch an unlatch masks from bus addresses to logical numbers

    dim_array[channel].dim_a_trig_unl = dimStateFromBusToLogicalMask(channel,status->critical.dim_a_trig_unl);
    dim_array[channel].dim_a_trig_lat = dimStateFromBusToLogicalMask(channel,status->critical.dim_a_trig_lat);

    // Update the trigger state for each DIM in the DIM present bitmask

    for(uint8_t dim_num = 0; dim_num < DIM_BUS_MAX_LOG_DEVS; ++dim_num)
    {
        if(dim_mask & 1)
        {
            dimStateUpdateTrigger(channel, dim_num, status, ctrl_cmd);
        }

        dim_mask >>= 1;
    }

    // If *any* DIM on this device needs reset, we must send the reset command

    if (dim_array[channel].requiring_reset_mask.any())
    {
        fgcliteCmdSetCtrl(ctrl_cmd, channel, DIM_RESET);
    }
    else
    {
        fgcliteCmdUnsetCtrl(ctrl_cmd, channel, DIM_RESET);
    }
}

// EOF
