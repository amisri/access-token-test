/*
 * @file   page_mgr.cpp
 * @brief  Manage FGClite paged memory
 * @author Michael Davis
 */

//#define DEBUG

#include <consts.h>
#include <timing.h>
#include <onewire.h>
#include <dim_bus.h>
#include <dim_state.h>
#include <log.h>
#include <ana_log.h>
#include <equip_liblog.h>
#include <equip_logger.h>
#include <equipdev.h>
#include <logging.h>
#include <page_mgr.h>

// Constants

const uint32_t TEMP_CYCLE              = 500;       // Scan the 1-wire temperatures once every 10 seconds (500 cycles)
const uint32_t DIM_BUS_CYCLE           = 50;        // Scan the DIM bus once per second (50 cycles)

// Page Manager States

enum State_PageMgr
{
    OFF           ,                                 // Off
    STARTING      ,                                 // Starting
    SCAN_ONE_WIRE ,                                 // Scan 1-wire bus
    SCAN_DIM_BUS  ,                                 // Scan DIM bus
    INIT_LOG      ,                                 // Initialise Logging
    READ_LOG      ,                                 // Read log data
    POSTMORTEM                                      // Read data for Postmortem
};

// Types for state and transition functions

typedef enum State_PageMgr StateFunc(uint32_t channel, struct FGClite_status const * status, struct FGClite_command * ctrl_cmd, bool first_call);
typedef StateFunc        * TransFunc(uint32_t channel);

// Structures

struct PageMgr
{
    uint32_t           cycle_counter;               // Cycle counter, counts 0..TEMP_CYCLE
    uint32_t           adc_log_index;               // Value of adc_log_index from the critical data for the current cycle
    uint32_t           pm_adc_log_index;            // Value of adc_log_index for the first sample in the Post Mortem 1 kHz log

    bool               do_reset;                    // Flag to reset the Page Manager state machine
    bool               do_pm;                       // Flag to trigger Post Mortem
    struct timeval     scan_dallas_time;            // Scheduled time to start scanning the Dallas bus

    bool               request_dim_bus;             // Set to TRUE to request a new DIM Bus scan
    bool               reset_log_index;             // Set to TRUE to restart logging from the latest ADC_LOG_INDEX/DIM_LOG_INDEX

    bool               is_invalid;                  // A FIP packed was lost, so page data is invalid
    bool               is_adc_log_frozen;           // ADC Post Mortem logging has completed

    enum State_PageMgr state;                       // Current page manager state
    StateFunc        * state_function;              // Current page manager state function pointer
};

// Global variables, zero initialised

static struct PageMgr page_mgrs[FGCD_MAX_DEVS] = {};

// Empty status variable, to be used if we miss a packet

static struct FGClite_status empty_status = {};

// Static function definitions

// Manage logging of paged data - This code is common to the LOGGING and POST_MORTEM states

static void pageMgrLogPagedData(uint32_t                      const channel,
                                struct FGClite_status const *       status,
                                struct FGClite_command      * const ctrl_cmd,
                                int32_t                       const time_offset_ms)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // ctrl_cmd_set is a flag to ensure that only one subsystem (1-wire, DIM Digital, DIM Analog, ADC)
    // can make a paged memory request per FIP cycle.

    bool ctrl_cmd_set = false;

    // If no valid status available, set status to all zeros and suppress new pages request for this cycle

    if(page_mgr.is_invalid)
    {
        status       = &empty_status;
        ctrl_cmd_set = true;
    }

    // Update 1-wire pages

    // Request a 1-wire temperature scan every 10 seconds

    if(page_mgr.cycle_counter == 0)
    {
        onewireStartTempScan(channel);
    }

    onewireUpdate(channel, status, &ctrl_cmd_set, ctrl_cmd);

    // Request DIM bus data once per second

    if((page_mgr.cycle_counter % DIM_BUS_CYCLE) == 0)
    {
        page_mgr.request_dim_bus = true;
    }

    dimBusScan(channel, status, &page_mgr.request_dim_bus, &ctrl_cmd_set, ctrl_cmd);

    // Retrieve a page of analogue data if one was requested in the previous cycle

    anaLogGetPage(channel, status);

    // Reset the ADC log index and DIM log index if required

    if(page_mgr.reset_log_index == true || anaLogIsOverrun(channel, status) == true)
    {
        // Reset the analogue log indices

        anaLogResetIndex(channel, status, page_mgr.is_adc_log_frozen, time_offset_ms);

        page_mgr.reset_log_index = false;
    }

    // If we have not yet set a command this cycle, request analog log data

    if(ctrl_cmd_set == false)
    {
        // If we have reached the end of all page requests, reset logging after the next call to anaLogGetPage()

        page_mgr.reset_log_index = anaLogRequestPage(channel, ctrl_cmd);
    }
}

// State function: STARTING


static enum State_PageMgr statePageMgrStarting(uint32_t                      const channel,
                                               struct FGClite_status const * const status,
                                               struct FGClite_command      * const ctrl_cmd,
                                               bool                          const first_call)
{
    onewireReset(channel);

    page_mgrs[channel].do_reset = false;

    return STARTING;
}


// State function: SCAN_ONE_WIRE


static enum State_PageMgr statePageMgrScanOneWire(uint32_t                      const channel,
                                                  struct FGClite_status const * const status,
                                                  struct FGClite_command      * const ctrl_cmd,
                                                  bool                          const first_call)
{
    // Start a new scan on first_call

    if(first_call == true)
    {
        onewireStartFullScan(channel);
    }

    // Scan the 1-wire bus

    onewireUpdate(channel, status, NULL, ctrl_cmd);

    return SCAN_ONE_WIRE;
}


// State function: SCAN_DIM


static enum State_PageMgr statePageMgrScanDimBus(uint32_t                      const channel,
                                                 struct FGClite_status const * const status,
                                                 struct FGClite_command      * const ctrl_cmd,
                                                 bool                          const first_call)
{
    // Scan the DIM bus. Start a new scan on first_call.

    dimBusFullScan(channel, status, first_call, ctrl_cmd);

    return SCAN_DIM_BUS;
}


// State function: INIT_LOG


static enum State_PageMgr statePageMgrInitLog(uint32_t                      const channel,
                                              struct FGClite_status const * const status,
                                              struct FGClite_command      * const ctrl_cmd,
                                              bool                          const first_call)
{
    // Initialise the analogue logging and the DIM active list with the bitmask of which DIM channels are in use

    anaLogInit(channel, dimBusActiveDimsBitmask(channel));

    dimStateInitActiveList(channel, dimBusActiveDimsBitmask(channel), status, ctrl_cmd);

    return INIT_LOG;
}


// State function: LOGGING


static enum State_PageMgr statePageMgrLogging(uint32_t                      const channel,
                                              struct FGClite_status const * const status,
                                              struct FGClite_command      * const ctrl_cmd,
                                              bool                          const first_call)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // Always log critical data

    ccLogCritical(channel, status);

    if(first_call)
    {
        // Ensure the FGClite paged data is not frozen

        anaLogUnfreezeAdc(channel, ctrl_cmd);

        // Reset log index and frozen flag for discontinuous logs

        page_mgr.reset_log_index   = true;
        page_mgr.is_adc_log_frozen = false;
    }

    // Log paged data

    pageMgrLogPagedData(channel, status, ctrl_cmd, -20);

    return READ_LOG;
}


// State function: POSTMORETM


static enum State_PageMgr statePageMgrPostmortem(uint32_t                      const channel,
                                                 struct FGClite_status const * const status,
                                                 struct FGClite_command      * const ctrl_cmd,
                                                 bool                          const first_call)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // Always log critical data

    ccLogCritical(channel, status);

    if(first_call)
    {
        // Reset log index for discontinuous logs

        page_mgr.reset_log_index = true;

        // Set flag to freeze continuous logs

        ccLogPMFreeze(channel);
    }

    // Log paged data

    pageMgrLogPagedData(channel, status, ctrl_cmd,
                        -(((int32_t)status->critical.adc_log_index + ADC_BUFFER_SIZE - page_mgr.pm_adc_log_index) % ADC_BUFFER_SIZE));

    // When discontinuous logging finishes, latch the frozen flag

    page_mgr.is_adc_log_frozen |= page_mgr.reset_log_index;

    // Freeze the FGClite paged data if it is about to overrun

    anaLogFreezeAdc(channel, status, ctrl_cmd);

    return POSTMORTEM;
}


// Transition functions


// Transition condition from any state to STARTING


static StateFunc * XXtoST(uint32_t const channel)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    if(page_mgr.do_reset || page_mgr.state == OFF)
    {
        return statePageMgrStarting;
    }

    return NULL;
}


// Condition for transition from STARTING to SCAN_ONE_WIRE


static StateFunc * STtoSO(uint32_t const channel)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // Check enough time has elapsed since the reset before starting a new scan

    struct timeval time_now;

    timingGetUserTime(0, &time_now);

    if(page_mgr.scan_dallas_time.tv_sec != 0 &&
       page_mgr.scan_dallas_time.tv_sec < time_now.tv_sec)
    {
        return statePageMgrScanOneWire;
    }

    return NULL;
}


// Condition for transition from SCAN_ONE_WIRE to SCAN_DIM_BUS


static StateFunc * SOtoSD(uint32_t const channel)
{
    if(onewireIsScanComplete(channel))
    {
        return statePageMgrScanDimBus;
    }

    return NULL;
}


// Condition for transition from SCAN_DIM_BUS to INIT_LOG


static StateFunc * SDtoIL(uint32_t const channel)
{
    if(dimBusIsScanComplete(channel))
    {
        return statePageMgrInitLog;
    }

    return NULL;
}


// Condition for transition from INIT_LOG to LOGGING


static StateFunc * ILtoLG(uint32_t const channel)
{
    return statePageMgrLogging;
}


// Condition for transition from LOGGING to POSTMORTEM


static StateFunc * LGtoPM(uint32_t const channel)
{
    if(page_mgrs[channel].do_pm == true)
    {
        return statePageMgrPostmortem;
    }

    return NULL;
}


// Condition for transition from POSTMORTEM to LOGGING


static StateFunc * PMtoLG(uint32_t const channel)
{
    if(page_mgrs[channel].do_pm == false)
    {
        return statePageMgrLogging;
    }

    return NULL;
}

// Transition functions : called in priority order, from left to right

#define MAX_TRANSITIONS     2      // Maximum transitions from a state

static TransFunc * transitions[][MAX_TRANSITIONS + 1] =
{
    /* OFF           OF */ { XXtoST,         NULL },
    /* STARTING      ST */ {         STtoSO, NULL },
    /* SCAN_ONE_WIRE SO */ { XXtoST, SOtoSD, NULL },
    /* SCAN_DIM_BUS  SD */ { XXtoST, SDtoIL, NULL },
    /* INIT_LOG      IL */ { XXtoST, ILtoLG, NULL },
    /* READ_LOG      LG */ { XXtoST, LGtoPM, NULL },
    /* POSTMORTEM    PM */ { XXtoST, PMtoLG, NULL },
};


void pageMgrUpdate(uint32_t const channel, struct FGClite_status const * const status, struct FGClite_command * const ctrl_cmd)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // Keep a local copy of adc_log_index

    page_mgr.adc_log_index = status->critical.adc_log_index;

    // Increment the cycle counter and reset it every 10 seconds

    page_mgr.cycle_counter = (page_mgr.cycle_counter + 1) % TEMP_CYCLE;

    // Update DIM trigger states

    dimStateUpdate(channel, dimBusActiveDimsBitmask(channel), status, ctrl_cmd);

    // Scan transitions for the current state, testing each condition in priority order

    StateFunc  * next_state = NULL;                                        // pointer to the next state function
    TransFunc  * transition;                                               // pointer to the next transition function to test
    TransFunc ** state_transitions = &transitions[page_mgr.state][0];

    // Call transition condition functions for the current state, testing each condition in priority order

    while((transition = *(state_transitions++)) != NULL && (next_state = (*transition)(channel)) == NULL)
    {
    }

    if(next_state != NULL)
    {
        page_mgr.state_function = next_state;

        page_mgr.state = next_state(channel, status, ctrl_cmd, true);

#ifdef DEBUG
        logPrintf(channel, "pageMgrUpdate: New state: %u\n", page_mgr.state);
#endif
    }
    else
    {
        // No transition condition is true so stay in the current state and run the state function

        page_mgr.state_function(channel, status, ctrl_cmd, false);
    }

    // Reset the invalidity flag for the status variable for the next cycle

    page_mgr.is_invalid = false;
}



void pageMgrReset(uint32_t const channel, struct timeval const * const start_time)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // Reset the page manager state to STARTING

    page_mgr.do_reset = true;

    // Initialise the time for the next Dallas ID scan to 2 seconds after the reset

    page_mgr.scan_dallas_time = *start_time;
    page_mgr.scan_dallas_time.tv_sec += 2;
}



void pageMgrInvalidate(uint32_t const channel)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // If we are still initialising everything, start again

    if(page_mgr.state == SCAN_ONE_WIRE || page_mgr.state == SCAN_DIM_BUS || page_mgr.state == INIT_LOG)
    {
        page_mgr.do_reset = true;

#ifdef DEBUG
        logPrintf(channel, "pageMgrInvalidate: State=%u. Reseting page manager sequence.\n");
#endif
    }
    else
    {
        // Otherwise, set a flag to say that the data arriving in the next cycle is invalid

        page_mgr.is_invalid = true;

#ifdef DEBUG
        logPrintf(channel, "pageMgrInvalidate: State=%u. Data for the next cycle is marked as invalid.\n");
#endif
    }
}



void pageMgrPMTrigger(uint32_t const channel)
{
    struct PageMgr &page_mgr = page_mgrs[channel];

    // Calculate the index of the first sample to be saved in the Postmortem log.
    // Split the log to be 50% before the trip and 50% after the trip.

    const uint32_t pm_adc_log_index_offset = equipdev.device[channel].log.buffers.priv[LOG_ACQ_1KHZ].num_postmortem_samples / 2;

    page_mgr.pm_adc_log_index = (page_mgr.adc_log_index + ADC_BUFFER_SIZE - pm_adc_log_index_offset) % ADC_BUFFER_SIZE;

    // Tell the Page Manager to switch to Post Mortem logging

    page_mgr.do_pm = true;

#ifdef DEBUG
    logPrintf(channel, "pageMgrPMTrigger: pm_adc_log_index=%u  Activating postmortem readout.\n", page_mgr.pm_adc_log_index);
#endif

}



void pageMgrPMReset(uint32_t const channel)
{
#ifdef DEBUG
    logPrintf(channel, "pageMgrPMTrigger: Activate postmortem readout.\n");
#endif

    page_mgrs[channel].do_pm = false;
}

// EOF
