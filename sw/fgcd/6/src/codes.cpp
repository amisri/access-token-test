//! @file   codes.cpp
//! @brief  Functions to access the codes databases
// #define DIM_WRN_DEBUG

#include <cstdio>
#include <cstring>
#include <cctype>

#include <classes/92/defconst.h>
#include <equip_libevtlog.h>
#include <equip_liblog.h>
#include <dim_state.h>
#include <equipdev.h>
#include <fgc/fgc_db.h>
#include <fgcddev.h>
#include <logging.h>
#include <codes.h>
#include <log.h>



// Constants

const char* const default_device_type = "RPAFL";
const uint32_t    DIM_NAME_MAX_LEN    = 30;


// Types

struct Dim
{
    uint32_t      logical_address;                 // DIM logical address
    uint16_t      type;                            // DIM type
    char          name     [DIM_NAME_MAX_LEN];     // Storage for DIAG.DIM_NAMES property
};


struct Codes
{
    uint16_t      sysdb_idx;                        // Index of the device type in the SysDb

    uint32_t      expected [FGC_N_COMP_GROUPS];     // Number of expected Dallas devices in each group
    uint32_t      detected [FGC_N_COMP_GROUPS];     // Number of detected Dallas devices in each group
    uint32_t      unknown  [FGC_MAX_UNKNOWN_COMPS]; // Number of unknown Dallas devices
    uint32_t      unknown_numels;

    struct Dim    dim      [FGC_MAX_DIMS];          // DIM definitions - index is dim def order in SysDb so no holes
};


// Global variables (zero-initialised)

struct Codes codes[FGCD_MAX_DEVS] = {};


// Static functions

// Check validity of the system type.
//
// @param[in,out]    type    System type to check
//
// @returns    The number of the entry in the SysDB, or zero if the entry is unknown

static uint8_t codesCheckSystemType(const char *type)
{
    for(uint8_t i = 0; i < SysDbLength(); ++i)
    {
        if(strncmp(type, SysDbType(i), 5) == 0) return i;
    }

    return 0;
}



// Return analogue data for the DIAG.ANA property

static int32_t codesDimsAnaData(uint32_t channel, uint32_t dim_no, uint16_t dim_channel, char *buffer, int32_t max_len)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct Dim              &dim    = codes[channel].dim[dim_no];

    return snprintf(buffer, max_len, "%u:%-15s:%10.3E:%s,",
                    dim_channel,                                       // DIM analogue channel 0..3
                    DimDbAnalogLabel(dim.type, dim_channel),           // Analogue channel label
                    device.diag.anasigs[dim_no + 20 * dim_channel],    // Analogue channel cooked value
                    DimDbAnalogUnits(dim.type, dim_channel));          // Analogue channel units
}



// Return digital data for the DIAG.DIG property

static int32_t codesDimsDigData(uint32_t channel, uint32_t dim_no, uint16_t dim_channel, uint32_t dim_dig_channel, char *buffer, int32_t max_len)
{
    struct Dim &dim          = codes[channel].dim[dim_no];
    uint16_t   &unl_dim_bits = equipdev.device[channel].diag.data[ dim_channel * 32 + dim_no];
    uint16_t   &lat_dim_bits = equipdev.device[channel].diag.data[(5 + dim_channel) * 32 + dim_no];

    int32_t len = 0;

    for(uint16_t dim_input = 0; dim_input < DimDbDigitalInputsLength(dim.type, dim_channel); ++dim_input)
    {
        if(DimDbIsDigitalInput(dim.type, dim_channel, dim_input))
        {
            int32_t l = snprintf(buffer + len, max_len - len, "%2u:%-3s|%-31s:%-7s:%-2s:%-2s,",
                                 dim_dig_channel * 12 + dim_input,                                           // DIM input logical channel 0..23
                                 DimDbDigitalIsFault(dim.type, dim_channel, dim_input) ? "TRG" : "STA",      // Input is TRIGGER or STATUS
                                 DimDbDigitalLabel  (dim.type, dim_channel, dim_input),                      // Input label

                                 unl_dim_bits & (1 << dim_input) ?
                                     DimDbDigitalLabelOne (dim.type, dim_channel, dim_input) :               // Text for bit set
                                     DimDbDigitalLabelZero(dim.type, dim_channel, dim_input),                // Text for bit unset

                                 DimDbDigitalIsFault(dim.type, dim_channel, dim_input) ?                     // Show UT flag just for triger inputs
                                    (dimStateIsUnlatched(channel, dim_no) ? "UT" : "") :
                                    "",
                                 DimDbDigitalIsFault(dim.type, dim_channel, dim_input) ?                     // Show LT flag just for trigger inputs
                                    ( (lat_dim_bits & (1 << dim_input)) ? "LT" : "") :
                                    "");

            len = (l < max_len - len) ? len + l : max_len - 1;
        }
    }

    return len;
}



// Return digital data for the DIAG.FAULTS property

static int32_t codesDimsDigFaults(uint32_t channel, uint32_t dim_no, uint16_t dim_channel, uint32_t dim_dig_channel, char *buffer, int32_t max_len)
{
    struct Dim &dim          = codes[channel].dim[dim_no];
    uint16_t   &unl_dim_bits = equipdev.device[channel].diag.data[ dim_channel * 32 + dim_no];
    uint16_t   &lat_dim_bits = equipdev.device[channel].diag.data[(5 + dim_channel) * 32 + dim_no];

    int32_t len = 0;

    for(uint16_t dim_input = 0; dim_input < DimDbDigitalInputsLength(dim.type, dim_channel); ++dim_input)
    {
        if(DimDbIsDigitalInput(dim.type, dim_channel, dim_input) &&                                          // Bit is a defined digital input
           DimDbDigitalIsFault(dim.type, dim_channel, dim_input) &&                                          // Bit is defined as TRIGGER (not STATUS)
           unl_dim_bits & (1 << dim_input))                                                                  // Bit is set
        {
            int32_t l = snprintf(buffer + len, max_len - len, "%-6s:%-31s:%-7s:%-2s:%-2s,",
                                 dim.name,                                                                   // DIM name
                                 DimDbDigitalLabel    (dim.type, dim_channel, dim_input),                    // Input label
                                 DimDbDigitalLabelOne (dim.type, dim_channel, dim_input),                    // Text for bit set
                                 dimStateIsUnlatched  (channel, dim_no) ? "UT" : "",                         // DIM is in unlatched fault state
                                 (lat_dim_bits & (1 << dim_input)) ? "LT" : "");                             // DIM is in latched fault state

            len = (l < max_len - len) ? len + l : max_len - 1;
        }
    }

    return len;
}



// Check for named diagnostic digital signals

static void codesFindDiagDigSig(uint32_t channel, char const *sig_name, uint16_t input_idx, int32_t dim_no, int16_t dim_channel, struct Equipdev_channel_diag_dig_sig *diag_dig_sig)
{
    if(strcmp(sig_name, diag_dig_sig->sig_name) == 0)
    {
        if(diag_dig_sig->num_sigs < DIAG_DIG_MAX_SIGS)
        {
            const uint32_t sig_idx = diag_dig_sig->num_sigs++;

            diag_dig_sig->dim_bits_ptr[sig_idx]  = &equipdev.device[channel].diag.data[dim_no + 0x20 * dim_channel];
            diag_dig_sig->dim_bits_mask[sig_idx] = 1 << input_idx;
            diag_dig_sig->state = FGC_VDI_NO_FAULT;

            logPrintf(channel, "CODES Connected to %s on DIM %u, channel %u, input %u\n", sig_name, dim_no, dim_channel, input_idx);
        }
        else
        {
            logPrintf(channel, "CODES Warning: too many %s signals\n", sig_name);
        }
    }
}



// Read definition for a single DIM from DimDb

static void codesDimsInitDim(uint32_t channel, uint32_t dim_no, uint16_t sysdb_idx, uint32_t dim_idx)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct Dim              &dim    = codes[channel].dim[dim_no];

    bool is_vs_dim;

    // Initialise DIM name and address

    dim.logical_address = dim_no;

    device.diag.bus_address[dim_no] = SysDbDimBusAddress(sysdb_idx, dim_idx);
    assert(device.diag.bus_address[dim_no] < FGC_MAX_REAL_DIMS);

    dim.type = SysDbDimDbType(sysdb_idx, dim_idx);

   // If the bus address is not virtual (and smaller than the maximum admisible value), then store the reverse lookup

    if(device.diag.bus_address[dim_no] < FGC_MAX_REAL_DIMS)
    {
        device.diag.dim_num[device.diag.bus_address[dim_no]] = dim_no;
    }

    // Increment DIAG.DIMS_EXPECTED if the DIM is real (not composite)

    if((device.diag.bus_address[dim_no] & CODES_COMPOSITE_BUS_ADDR_MASKS) == 0)
    {
        ++device.diag.dims_expected[0];
    }

    // Use DIM name for the log menu

    snprintf(dim.name, DIM_NAME_MAX_LEN, "%s", SysDbDimName(sysdb_idx, dim_idx));

    ccLogSetDimMenuName(channel, dim_no, dim.name);

    // Note which DIM is called VS. This must be the DIM returned in the critical data (i.e. bus address 1).

    if((is_vs_dim = (strcmp(dim.name, "VS") == 0)))
    {
        device.diag.vs_dim_idx = dim_no;
    }

    // Initialise gain and offset for the analogue channels

    for(uint16_t dim_channel = 0; dim_channel < DimDbNumberOfChannels(dim.type); ++dim_channel)
    {
        if(DimDbIsAnalogChannel(dim.type, dim_channel))
        {
            // Analog channel

            device.diag.gain  [dim_no + dim_channel * 20] = DimDbAnalogGain  (dim.type, dim_channel);
            device.diag.offset[dim_no + dim_channel * 20] = DimDbAnalogOffset(dim.type, dim_channel);

            // Find the location of U_LEADS and I_EARTH.

            float      *anasig_ptr   = &device.diag.anasigs[dim_no + dim_channel * 20];
            const char *anasig_label = DimDbAnalogLabel(dim.type, dim_channel);

            logStoreSigNameAndUnits(&device.log.structs.log[LOG_DIM + dim_no], dim_channel, anasig_label, DimDbAnalogUnits(dim.type, dim_channel));

                 if(strcmp(anasig_label, "U_LEAD_POS") == 0) device.meas.u_leads_ptr [U_LEAD_POS] = anasig_ptr;
            else if(strcmp(anasig_label, "U_LEAD_NEG") == 0) device.meas.u_leads_ptr [U_LEAD_NEG] = anasig_ptr;
            else if(strcmp(anasig_label, "I_EARTH")    == 0) device.meas.i_earth_ptr              = anasig_ptr;
        }
        else if(DimDbIsDigitalChannel(dim.type, dim_channel))
        {
            // Digital channel

            // Find DIM signals linked to FW_DIODE, FBAORT_UNSAFE and THYR_UNSAFE

            for(uint16_t input_idx=0; input_idx < DimDbDigitalInputsLength(dim.type, dim_channel); ++input_idx)
            {
                const char *channel_name = DimDbDigitalLabel(dim.type, dim_channel, input_idx);

                codesFindDiagDigSig(channel, channel_name, input_idx, dim_no, dim_channel, &device.diag.fw_diode);
                codesFindDiagDigSig(channel, channel_name, input_idx, dim_no, dim_channel, &device.diag.fabort_unsafe);
                codesFindDiagDigSig(channel, channel_name, input_idx, dim_no, dim_channel, &device.diag.thyr_unsafe);
                codesFindDiagDigSig(channel, channel_name, input_idx, dim_no, dim_channel, &device.diag.subconv_lost);

                // Check for warning signals

                if (strncmp(DimDbDigitalLabelOne(dim.type, dim_channel, input_idx), "WARNING", 7) == 0) {
                    // This magic number comes from DimDbIsDigitalChannel and happens to
                    // correspond to FGC_N_DIM_ANA_CHANS.
                    const int DIG_CHANNELS_OFFSET = 4;

                    int dig_channel_index = dim_channel - DIG_CHANNELS_OFFSET;

#ifdef DIM_WRN_DEBUG
                    logPrintf(channel, "CODES: treating signal '%s' (dim %d, dim channel %d / DIG channel %d, index %d) as WARNING\n", channel_name, dim_no, dim_channel, dig_channel_index, input_idx);
#endif

                    device.diag.warning_masks[dim_no][dig_channel_index] |= (1 << input_idx);
                }
            }
        }
    }
}



// External functions

int32_t codesInit(void)
{
    char *sysdb     = fgcddevGetCodePtr(FGC_CODE_90_SYSDB);
    char *compdb    = fgcddevGetCodePtr(FGC_CODE_90_COMPDB);
    char *dimdb     = fgcddevGetCodePtr(FGC_CODE_90_DIMDB);
    char *iddb      = fgcddevGetCodePtr(FGC_CODE_90_IDDB);
    char *ptdb      = fgcddevGetCodePtr(FGC_CODE_90_PTDB);

    if(sysdb == NULL || SysDbInit(sysdb) == -1)
    {
        fprintf(stderr, "ERROR: SysDB initialisation failed.\n");
        return 1;
    }
    if(compdb == NULL || CompDbInit(compdb) == -1)
    {
        fprintf(stderr, "ERROR: CompDB initialisation failed.\n");
        return 1;
    }
    if(dimdb == NULL || DimDbInit(dimdb) == -1)
    {
        fprintf(stderr, "ERROR: DIMDB initialisation failed.\n");
        return 1;
    }
    if(iddb == NULL || IdDbInit(iddb) == -1)
    {
        fprintf(stderr, "ERROR: IDDB initialisation failed.\n");
        return 1;
    }
    if(ptdb == NULL || PtDbInit(ptdb) == -1)
    {
        fprintf(stderr, "ERROR: PTDB initialisation failed.\n");
        return 1;
    }

    return 0;
}



void codesInitDevice(uint32_t channel, const char *device_name, char *type_prop)
{
    uint16_t &sysdb_idx = codes[channel].sysdb_idx;

    // Set the device type from the device name

    char device_type[6];

    // Extract device type from name. 5 chars max, truncate at first dot.

    uint32_t n_els;

    for(n_els = 0; n_els < 5; ++n_els)
    {
        if(device_name[n_els] == '\0' || device_name[n_els] == '.') break;
    }

    strncpy(device_type, device_name, n_els);

    device_type[n_els] = '\0';

    // Check if the device type is valid

    if(codesCheckSystemType(device_type) == 0)
    {
        // Use default device type

        strncpy(device_type, default_device_type, 6);
    }

    // If the DEVICE.TYPE property is set and the device type is not an operational device, override
    // the device type with the property value

    uint16_t const type_len = strnlen(type_prop, 5);

    if(type_len != 0 && strncmp(device_type, default_device_type, 6) == 0)
    {
        // DEVICE.TYPE is a char array, so convert it to upper case and ensure it is a
        // properly-terminated C string.

        for(n_els = 0; n_els < type_len; ++n_els) type_prop[n_els] = toupper(type_prop[n_els]);

        type_prop[n_els] = '\0';

        // If DEVICE.TYPE is valid and is different from device_type, override device_type

        if(codesCheckSystemType(type_prop)    != 0 &&
           strncmp(device_type, type_prop, 6) != 0)
        {
            // Warn if DEVICE.TYPE and the default device type are different

            logPrintf(channel, "CODES Warning: overriding default DEVICE.TYPE %s with %s.\n", device_type, type_prop);

            strncpy(device_type, type_prop, 5);
        }
    }

    // Now device_type is set correctly, ensure that the DEVICE.TYPE property is consistent

    strncpy(type_prop, device_type, 5);

    // Find the device in SysDb

    sysdb_idx = codesCheckSystemType(device_type);

    assert(sysdb_idx != 0);

    // Initialise the number of components expected and detected in each group

    for(uint32_t group = 0; group < FGC_N_COMP_GROUPS; ++group)
    {
        codes[channel].expected[group] = SysDbRequiredGroups(sysdb_idx, group);
        codes[channel].detected[group] = 0;
    }

    // Initialize the number of unknown components

    codes[channel].unknown_numels = 0;
    for(uint32_t i = 0; i < FGC_MAX_UNKNOWN_COMPS; ++i)
    {
        codes[channel].unknown[i] = 0;
    }

}



static void codesInitDiagDigSig(struct Equipdev_channel_diag_dig_sig *diag_dig_sig, uint32_t fault_mask, uint32_t warning_mask, uint16_t logic, const char *sig_name)
{
    // Reset the diagnostic digital signal structure

    memset(diag_dig_sig, 0, sizeof(struct Equipdev_channel_diag_dig_sig));

    // Initialise the main fields

    diag_dig_sig->sig_name     = sig_name;
    diag_dig_sig->fault_mask   = fault_mask;
    diag_dig_sig->warning_mask = warning_mask;
    diag_dig_sig->logic        = logic;
    diag_dig_sig->state        = FGC_VDI_NOT_PRESENT;
}



void codesDimsInit(uint32_t channel)
{
    struct Equipdev_channel &device    = equipdev.device[channel];
    uint16_t                &sysdb_idx = codes[channel].sysdb_idx;

    uint32_t max_dims = FGC_MAX_DIMS;

    device.sysdb_idx = sysdb_idx;

    // Find the default system type, used for the definition of the FGClite onboard DIM (DIM0)

    device.default_sysdb_idx = codesCheckSystemType(default_device_type);

    assert(device.default_sysdb_idx != 0);

    // Before initializing the DIM, prepare the diag digital signals that will be searched for in the DIM digital signal names

    codesInitDiagDigSig(&device.diag.fw_diode,      FGC_FLT_FW_DIODE, 0,      DIAG_DIG_SIG_LOGIC_OR,  "FREE WHEEL DIODE");
    codesInitDiagDigSig(&device.diag.fabort_unsafe, FGC_FLT_FABORT_UNSAFE, 0, DIAG_DIG_SIG_LOGIC_OR,  "FAST ABORT UNSAFE");
    codesInitDiagDigSig(&device.diag.thyr_unsafe,   FGC_FLT_THYR_UNSAFE, 0,   DIAG_DIG_SIG_LOGIC_OR,  "CROWBAR THYRISTOR UNSAFE");
    codesInitDiagDigSig(&device.diag.subconv_lost,  0, FGC_WRN_SUBCONVTR_FLT, DIAG_DIG_SIG_LOGIC_DIFF,"INPUT-PWR-FILTER_POWERED_STATE");

    // Initialise DIAG.DIMS_EXPECTED

    device.diag.dims_expected[0] = 0;

    // Initialize bus_addresses

    for(uint32_t dim_idx = 0; dim_idx < FGC_MAX_DIMS; ++dim_idx)
    {
        device.diag.bus_address[dim_idx] = 0xFF;
        device.diag.dim_num[dim_idx]      = dim_idx;
    }

    // If system type is not the default FGClite (RPAFL) then initialise DIM 0 using the RPAFL
    // DIM 0 definition, as this is the internal DIM in the FGClite which receives

    if(sysdb_idx != device.default_sysdb_idx)
    {
        codesDimsInitDim(channel, 0, device.default_sysdb_idx, 0);

        --max_dims;
    }

    // Ensure max_dims does not exceed the number of DIMs in the SysDb

    if(max_dims > SysDbDimsLength(sysdb_idx)) max_dims = SysDbDimsLength(sysdb_idx);

    // Before initializing the DIMs information set the U_LEADS and I_EARTH pointer to NULL

    device.meas.u_leads_ptr [U_LEAD_POS] = NULL;
    device.meas.u_leads_ptr [U_LEAD_NEG] = NULL;
    device.meas.i_earth_ptr              = NULL;

    // Read the DIM definitions from SysDb and DimDb

    for(uint32_t dim_idx = 0; dim_idx < max_dims; ++dim_idx)
    {
        if(SysDbDimEnabled(sysdb_idx, dim_idx))
        {
            uint32_t dim_no = SysDbDimLogicalAddress(sysdb_idx, dim_idx);

            // For all operational systems, increment logical DIM number by 1 to leave slot 0 free for onboard DIM

            if(sysdb_idx != device.default_sysdb_idx)
            {
                dim_no++;
            }

            codesDimsInitDim(channel, dim_no, sysdb_idx, dim_idx);
        }
    }
}



int32_t codesDimsList(uint32_t channel, bool is_triggered, int32_t from, int32_t to, int32_t step, char *buffer, int32_t max_len)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    int32_t len = 0;

    // Validate range

    if(from < 0 || abs(from) >= device.diag.dims_expected[0]) return 0;

    if(to == 0 || abs(to) >= device.diag.dims_expected[0])
    {
        to = device.diag.dims_expected[0] - 1;
    }

    if(to < 0) to = from;

    for(int32_t dim_no = from; dim_no <= to; dim_no += step)
    {
        struct Dim &dim      = codes[channel].dim   [dim_no];
        uint32_t   &bus_addr = device.diag.bus_address[dim_no];

        int32_t l;

        if(!is_triggered)
        {
            // If is_triggered is false, return a list of DIM names

            l = snprintf(buffer + len, max_len - len, "%s:%u:0x%02X,", dim.name, dim.logical_address, bus_addr);

            len = (l < max_len - len) ? len + l : max_len - 1;
        }
        else if(dimStateIsUnlatched(channel, dim.logical_address))
        {
            // If is_triggered is true, and still there is a fault present, then return a list of digital inputs which are in fault

            uint32_t dim_dig_channel = 0;

            for(uint16_t dim_channel = 0; dim_channel < DimDbNumberOfChannels(dim.type); ++dim_channel)
            {
                l = codesDimsDigFaults(channel, dim_no, dim_channel, dim_dig_channel++, buffer + len, max_len - len);

                len = (l < max_len - len) ? len + l : max_len - 1;
            }
        }
    }

    // Delete final ','

    if(len > 0) buffer[--len] = '\0';

    return len;
}



int32_t codesDimsData(uint32_t channel, bool get_ana, char *buffer, int32_t max_len)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    int32_t len = 0;

    for(uint32_t dim_no = 0; dim_no < device.diag.dims_expected[0]; ++dim_no)
    {
        struct Dim &dim = codes[channel].dim[dim_no];

        int32_t l;

        // Write the DIM name

        l = snprintf(buffer + len, max_len - len, "%s:", dim.name);

        len = (l < max_len - len) ? len + l : max_len - 1;

        // Write the DIM channels

        uint32_t dim_dig_channel = 0;

        for(uint16_t dim_channel = 0; dim_channel < DimDbNumberOfChannels(dim.type); ++dim_channel)
        {
            l = 0;

            if(get_ana && DimDbIsAnalogChannel(dim.type, dim_channel))
            {
                l = codesDimsAnaData(channel, dim_no, dim_channel, buffer + len, max_len - len);
            }
            else if(!get_ana && DimDbIsDigitalChannel(dim.type, dim_channel))
            {
                l = codesDimsDigData(channel, dim_no, dim_channel, dim_dig_channel++, buffer + len, max_len - len);
            }

            len = (l < max_len - len) ? len + l : max_len - 1;
        }

        buffer[len-1] = '\n';
    }

    return len;
}



const char *codesDimsSignalName(uint32_t channel, uint32_t dim_no, uint32_t dim_channel)
{
    struct Dim &dim = codes[channel].dim[dim_no];

    return DimDbAnalogLabel(dim.type, dim_channel);
}



const char *codesDimsSignalUnits(uint32_t channel, uint32_t dim_no, uint32_t dim_channel)
{
    struct Dim &dim = codes[channel].dim[dim_no];

    return DimDbAnalogUnits(dim.type, dim_channel);
}



void codesDimsEvtLog(uint32_t channel, uint32_t dim_no, bool is_triggered, const struct timeval *timestamp, const uint16_t digital_bank[2], uint16_t digital_mask[2])
{
    char dim_name[DIM_NAME_MAX_LEN+4];

    struct Dim &dim = codes[channel].dim[dim_no];

    snprintf(dim_name, DIM_NAME_MAX_LEN, "DIM.%s", dim.name);

    uint32_t digital_channel = 0;

    // If there is a DIM trigger and it is not yet displayed, set the DIM.<name> TRIGGER entry in the event log

    if(is_triggered && !digital_mask[0] && !digital_mask[1])
    {
        ccEvtlogStoreRecord(channel, timestamp, dim_name, "TRIGGER", "SET", EVTLOG_STATUS_NORMAL);
    }

    for(uint16_t dim_channel = 0; dim_channel < DimDbNumberOfChannels(dim.type) && digital_channel < 2; ++dim_channel)
    {
        if(DimDbIsAnalogChannel(dim.type, dim_channel)) continue;

        for(uint16_t dim_input = 0; dim_input < DimDbDigitalInputsLength(dim.type, dim_channel); ++dim_input)
        {
            if(DimDbIsDigitalInput(dim.type, dim_channel, dim_input) &&    // input is a digital input
               (1 << dim_input) & ~digital_mask[digital_channel])          // digital mask bit is unset for this input
            {
                bool is_input_set = (1 << dim_input) & digital_bank[digital_channel];

                if((is_input_set && DimDbDigitalIsFault(dim.type, dim_channel, dim_input))  ||  // If a fault is set                                                           // If the input was not logged already
                   (is_triggered && !DimDbDigitalIsFault(dim.type, dim_channel, dim_input)))    // or is a status during a DIM trigger                                                              // or in TRIGGERED state
                {
                    // Output the value of all FAULT bits which are set
                    // and all STATUS bits, whether they are set or not

                    ccEvtlogStoreRecord(channel, timestamp, dim_name,
                                        DimDbDigitalLabel(dim.type, dim_channel, dim_input),
                                        is_input_set ? DimDbDigitalLabelOne (dim.type, dim_channel, dim_input) :
                                                       DimDbDigitalLabelZero(dim.type, dim_channel, dim_input),
                                                       EVTLOG_STATUS_NORMAL);

                    // Keep a record of which set bits have been logged

                    digital_mask[digital_channel] |= (1 << dim_input);
                }
            }
        }

        ++digital_channel;
    }

    // If we didn't print anything in the event log (HW failure were latched bit does not correspond to DIM fault bits)
    // Then just set the mask to 0xFFFF to avoid spam

    if(is_triggered && !digital_mask[0] && !digital_mask[1])
    {
        digital_mask[0] = 0xFFFF;
        digital_mask[1] = 0xFFFF;
    }


}


uint16_t codesGetSysdbIdx(uint32_t channel)
{
    return codes[channel].sysdb_idx;
}

void codesGroupsAdd(uint32_t channel, uint16_t comp_idx)
{
    uint8_t group = CompDbGroupIndex(comp_idx, codes[channel].sysdb_idx);

    if(group)
    {
        ++codes[channel].detected[group];
    }
    else
    {
        // If the components do not belong to a group within  the system

        if(codes[channel].unknown_numels < FGC_MAX_UNKNOWN_COMPS)
        {
            codes[channel].unknown[codes[channel].unknown_numels] = comp_idx;
            ++codes[channel].unknown_numels;
        }
        else
        {
            logPrintf(channel,"CODES Number of unknown components bigger than maximum alllocated (%u)\n", FGC_MAX_UNKNOWN_COMPS);
        }
    }
}



bool codeGroupsCheckOK(uint32_t channel)
{
    for(uint32_t group = 0; group < FGC_N_COMP_GROUPS; ++group)
    {
        uint32_t &num_expected = codes[channel].expected[group];
        uint32_t &num_detected = codes[channel].detected[group];

        if(num_expected != num_detected) return false;
    }

    return true;
}



int32_t codesGroupsList(uint32_t channel, bool is_matched, char *buffer, int32_t max_len)
{
    int32_t len = 0;

    for(uint32_t group = 0; group < FGC_N_COMP_GROUPS; ++group)
    {
        uint32_t &num_expected = codes[channel].expected[group];
        uint32_t &num_detected = codes[channel].detected[group];

        if(( is_matched && num_expected == num_detected && num_expected != 0) ||
           (!is_matched && num_expected != num_detected))
        {
            int32_t l;

            l   = snprintf(buffer + len, max_len - len, "%02u/%02u|", num_detected, num_expected);
            len = (l < max_len - len) ? len + l : max_len - 1;

            for(uint32_t comp_idx = 0; comp_idx < CompDbLength(); ++comp_idx)
            {
                if(CompDbGroupIndex(comp_idx, codes[channel].sysdb_idx) == group)
                {
                    l   = snprintf(buffer + len, max_len - len, "%s=%s\\", CompDbType(comp_idx), CompDbLabel(comp_idx));
                    len = (l < max_len - len) ? len + l : max_len - 1;
                }

                // For group 0, just list UNKNOWN___ and ignore the rest

                if(group == 0) break;
            }
            --len;

            l   = snprintf(buffer + len, max_len - len, ",");
            len = (l < max_len - len) ? len + l : max_len - 1;
        }
    }

    if(!is_matched)
    {
        for(uint32_t i = 0; i < codes[channel].unknown_numels; ++i)
        {
            int32_t l;
            uint32_t comp_idx;

            comp_idx = codes[channel].unknown[i];
            l        = snprintf(buffer + len, max_len - len, "01/00|%s=%s,",CompDbType(comp_idx), CompDbLabel(comp_idx));
            len      = (l < max_len - len) ? len + l : max_len - 1;
        }
    }

    // Delete final ','

    if(len > 0) buffer[--len] = '\0';

    return len;
}



uint16_t codesDallasLookup(struct Dallas_id *dallas_id, char *barcode_text)
{
    enum Dallas_id_error  err;             // Error code if DB lookup fails
    uint16_t              part_no;         // Part number returned from IDDB to look up in PTDB
    int32_t               part_idx;        // Index of part_no in PTDB
    uint16_t              comp_idx = 0;    // Index of component in CompDb, initialised to 0 = Unknown component

    if((err = IdDbValidate(dallas_id))           != DLS_ID_OK  ||
       (err = IdDbLookup  (dallas_id, &part_no)) != DLS_ID_OK)
    {
        switch(err)
        {
            // Dallas ID is invalid or not found in IDDB.
            //
            // Note: error text must be a maximum of 20 characters (including terminating null),
            // to fit in the barcode buffer. It should also be in lower case, as the software uses
            // this as a hint to distinguish between valid barcodes and error codes.

            case DLS_ID_ERR_TYPE:         snprintf(barcode_text, 20, "unknown_dev_type");     break;
            case DLS_ID_ERR_NOT_FOUND:    snprintf(barcode_text, 20, "unknown_ID123_bytes");  break;
            case DLS_ID_ERR_TBL:          snprintf(barcode_text, 20, "unknown_ID4_byte");     break;
            case DLS_ID_ERR_ZEROS:        snprintf(barcode_text, 20, "non_zero_ID56_bytes");  break;
            case DLS_ID_ERR_CRC:          snprintf(barcode_text, 20, "crc_check_failed");     break;
            case DLS_ID_OK:               break;
        }
    }
    else if((part_idx = PtDbLookup(part_no)) < 0)
    {
        // Part number is not found in PTDB.
        //
        // This error indicates an inconsistency in the database, as all part numbers in IDDB
        // should also exist in PTDB.

        snprintf(barcode_text, 20, "unknown_part_no");
    }
    else
    {
        // Part number matched in PTDB

        PtDbBarcodeString(barcode_text, part_no, part_idx);

        comp_idx = CompDbLookup(PtDbTypeCode(part_idx));
    }

    return comp_idx;
}

// EOF
