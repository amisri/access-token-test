/*!
 * @file   timing_handlers.cpp
 * @brief  Functions for handling timing events
 * @author Stephen Page
 * @author Michael Davis
 */

#include <cstring>
#include <exception>
#include <iostream>
#include <stdlib.h>

#include <logging.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <equipdev.h>
#include <fieldbus.h>
#include <prop_equip.h>
#include <pm.h>

#include <timing.h>
#include <timing_handlers.h>



// Global variables

// Map of timing event handler names to functions

struct Timing_event_handler timing_event_handlers[]
= {
    { timing_handler_names[TIMING_HANDLER_POSTMORTEM],              &timingPostMortem           },
    { timing_handler_names[TIMING_HANDLER_STARTREF],                &timingStartRef             },
    { timing_handler_names[TIMING_HANDLER_ABORTREF],                &timingAbortRef             },
    { timing_handler_names[TIMING_HANDLER_TELEGRAM],                &timingTelegramReady        },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONCOMMIT],       &timingTransactionCommit    },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONROLLBACK],     &timingTransactionRollback  },
    { "",                                                           NULL                        },
};



void timingAddMultiPpmEvent(const char *mppm_condition)
{
    ; // Multi-PPM not supported in this class
}


// Static functions

static void timingSetTelegramBits(const char* field_name, uint8_t bit_number, Timing::EventValue* evt_value, uint32_t *telegram_value, bool first_telegram)
{
    static Timing::Value value;

    try
    {
        evt_value->getFieldValue(field_name, &value);

        if(value.getAsBool())
        {
            *telegram_value |= (1 << bit_number);
        }
        else
        {
            *telegram_value &= ~(1 << bit_number);
        }
    }
    catch(std::exception& e)
    {
        if (first_telegram)
        {
            // Print to std::cerr because logger is not ready at this point (during timing initialization)
            std::cerr << "Failed to set telegram bit " << field_name << " : " << e.what() <<". Exiting" << std::endl;
            exit(1);
        }
    }
}



/*
 * Handle telegram values for LHC
 */

static void timingTelegramLHC(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static bool first_telegram = true;
    uint16_t sector_mask = fgcd.device[0].omode_mask & 0x00FF;

    // PP60A (aka PC_PERMIT)

    timingSetTelegramBits("PP60A_ARC12", 0, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC23", 1, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC34", 2, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC45", 3, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC56", 4, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC67", 5, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC78", 6, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC81", 7, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field PP60A = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_PC_PERMIT]);
    }

    // Set PC_PERMIT

    if((timing.field_value[TIMING_FIELD_PC_PERMIT] & sector_mask) == sector_mask)
    {
      if(!fieldbus.pc_permit)
      {
           logPrintf(0, "TIMING PP60A (PC_PERMIT) set\n");
           fieldbus.pc_permit = 1;
      }
    }
    else if(fieldbus.pc_permit)
    {
      logPrintf(0, "TIMING PP60A (PC_PERMIT) cleared\n");
      fieldbus.pc_permit = 0;
    }

    // OMODE (aka OPMODE)

    timingSetTelegramBits("OMODE_BEAMOP", 8, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);
    timingSetTelegramBits("OMODE_CMS",    9, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);
    timingSetTelegramBits("OMODE_ATLAS",  10, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field OMODE = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_OP_MODE]);
    }

    // Set operational mode only if succeeded

    uint16_t op_mode = static_cast<uint16_t>(timing.field_value[TIMING_FIELD_OP_MODE] & 0x0000FFFF);

    if(fgcd.op_mode != op_mode)
    {
        if (first_telegram)
        {
            std::cout << "TIMING Operational mode mask changed from " << fgcd.op_mode << " to " << op_mode << std::endl;
        }

        logPrintf(0, "TIMING Operational mode mask changed from 0x%04hX to 0x%04hX\n", fgcd.op_mode, op_mode);
        fgcd.op_mode = op_mode;
    }

    // SECTACC (aka SECTORACCESS)

    timingSetTelegramBits("SECTACC_SEC12", 0, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC23", 1, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC34", 2, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC45", 3, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC56", 4, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC67", 5, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC78", 6, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC81", 7, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field SECTACC = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_SECTOR_ACCESS]);
    }

    // Set fieldbus SECTOR_ACCESS bit

    if(timing.field_value[TIMING_FIELD_SECTOR_ACCESS] & sector_mask)
    {
        if(!fieldbus.sector_access)
        {
           logPrintf(0, "TIMING SECTACC (SECTOR_ACCESS) set\n");
           fieldbus.sector_access = 1;
        }
    }
    else if(fieldbus.sector_access)
    {
      logPrintf(0, "TIMING SECTACC (SECTOR_ACCESS) cleared\n");
      fieldbus.sector_access = 0;
    }

    first_telegram = false;
}




// External functions

void timingTelegramReady(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Machine specific handling

    switch(fgcd.machine)
    {
        case MACHINE_LHC:
            timingTelegramLHC(handler, info, evt_time, evt_value);
            break;

        default:
            break;
    }
}



void timingPostMortem(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Log the timing event

    if(timing.log_events_flag)
    {
        try
        {
            static Timing::Value value;
            uint16_t             payload;

            evt_value->getFieldValue("EVENT_GROUP", &value);
            payload = static_cast<uint16_t>(value.getAsUshort());

            logPrintf(0, "Event %s Field EVENT_GROUP = %u\n", evt_value->getName().c_str(), payload);
        }
        catch(std::exception& e)
        {
            // Nothing to be done
        }
    }

    // Trigger an external Post Mortem in each of the devices

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        struct FGCD_device &device = fgcd.device[channel];

        if(device.name != NULL && device.online)
        {
            pmTriggerExternal(channel, evt_time.time.tv_sec, evt_time.time.tv_nsec);
        }
    }
}

// EOF
