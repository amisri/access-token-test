/*
 * @file   fgclite_cycle.cpp
 * @brief  Define the WorldFIP Fieldbus Cycle protocol
 * @author Michael Davis
 *
 * Implementation of the WorldFIP Fieldbus Cycle protocol as defined in:
 *
 * "FGClite: Fieldbus Cycle", EDMS 1523469
 */

//#define DEBUG_TIMING

#include <cstdio>
#include <cerrno>
#include <memory.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <unistd.h>

// FIP library includes

#include <fip.mfip.h>
#include <fip_watchdog.h>

// FGCD includes

#include <queue.h>
#include <timing.h>
#include <logging.h>
#include <fgcd_thread.h>
#include <fgclite_reg.h>
#include <fgclite_cycle.h>
#include <fip.h>
#include <fieldbus.h>
#include <fgcddev.h>
#include <equipdev.h>
#include <pm.h>
#include <pub.h>
#include <fgcd.h>
#include <serial.h>
#include <rt.h>

// Callbacks for FGClite cycle

static FipMfipVarCallback syncCommandSent;
static FipMfipVarCallback statusReceived;
static FipMfipVarCallback diagReceived;
static FipMfipVarCallback writeCommands;

// Other forward declarations of static functions

static void fgcliteCycleRoundRobin(uint32_t channel);



// Constants

const uint32_t FIP_DIAG_DEV_ID               = 31;                   // Internal ID used for the diagnostic device
const uint32_t NFIP_RESET_QUEUE_SIZE         = FGCD_MAX_EQP_DEVS;    // Size of queues for storing nanoFIP reset commands

const uint16_t MS_PERIOD_MIN                 = 38878;                // Minimum value for MS_PERIOD. Below this value, FGClite gets stuck in reset.
const uint16_t MS_PERIOD_DEFAULT             = 40000;                // Default value for MS_PERIOD. 40000 ticks x 25ns = 1ms
const uint16_t MS_PERIOD_MAX                 = 41122;                // Maximum value for MS_PERIOD. Above this value, nothing is communicated on the FIP bus.

const uint32_t CLOCK_CYCLE_FILTER_PERIOD     = 64;                   // Number of FIP cycles to include in the filter of CLOCK_CYCLE



/*
 * Calculate deadline within each cycle to receive all FGClite statuses and the response from the diagnostic device.
 *
 * The deadline is specified as an offset in nanoseconds from the start of cycle (transmission of the 50 Hz pulse from CTRI)
 *
 * The FGClite Fieldbus Cycle, p.5 (Elementary Cycle) and p.22 (Fig. 6: Worst Case STATUS Calculation Time) says that the first status
 * is sent at 1ms and each status has a bus-occupy time of 523us, given a maximum cable delay of 25us. The diagnostic device send-receive
 * cycle has a bus-occupy time of 140us. The measured times are slightly different.
 *
 * The times below were calculated by measuring when the callbacks fire on a full rack of 30 FGClites, with diagnostic device, with
 * both short and long FIP cables.
 */

const uint32_t FGCLITE_STATUS_START        =   808000;               // Time from the start of cycle to the start of STATUS_1
const uint32_t FGCLITE_AVG_DIAG_DURATION   =   113500;               // Average time from transmitting ID_DAT for the diag variable to receiving the diag callback on the gateway

const uint32_t FGCLITE_STATUS_DEADLINE     = 18000000;               // Deadline for the last status packet
const uint32_t FIP_DIAG_DEADLINE           = 18035000;               // Absolute deadline to receive all statuses and the response from the diagnostic device

const uint32_t FIP_BA_WAIT_NFIP_US         = 18035;                  // Deadline to wait to send NanoFIP reset command and write callback
const uint32_t FIP_SYN_WAIT_US             = 20000;                  // Deadline to wait for the next cycle trigger, which should occur at 20.0ms



// Types

/*
 * Frame IDs for the BA macrocycle program.
 *
 * These values specify ID_DAT for each frame. Each value must be unique and can be used at most once in
 * each cycle.
 *
 * The frame IDs recognised by FGClite are defined in the FGClite Fieldbus Cycle document, section 3.
 * See also the nanoFIP Functional Specification, section 3. The packing ID (0x9080) used for stuffing
 * is reserved.
 */

enum FGClite_macrocycle_frame_id
{
    FGCLITE_NOID,                            // Used for frames which do not correspond to a variable

    FGCLITE_STATUS_1            = 0x0601,    // FGClite device STATUS variable for device at FIP address 1
    FGCLITE_STATUS_2,                        // FGClite device STATUS variable for device at FIP address 2
    FGCLITE_STATUS_3,                        // FGClite device STATUS variable for device at FIP address 3
    FGCLITE_STATUS_4,                        // FGClite device STATUS variable for device at FIP address 4
    FGCLITE_STATUS_5,                        // FGClite device STATUS variable for device at FIP address 5
    FGCLITE_STATUS_6,                        // FGClite device STATUS variable for device at FIP address 6
    FGCLITE_STATUS_7,                        // FGClite device STATUS variable for device at FIP address 7
    FGCLITE_STATUS_8,                        // FGClite device STATUS variable for device at FIP address 8
    FGCLITE_STATUS_9,                        // FGClite device STATUS variable for device at FIP address 9
    FGCLITE_STATUS_10,                       // FGClite device STATUS variable for device at FIP address 10
    FGCLITE_STATUS_11,                       // FGClite device STATUS variable for device at FIP address 11
    FGCLITE_STATUS_12,                       // FGClite device STATUS variable for device at FIP address 12
    FGCLITE_STATUS_13,                       // FGClite device STATUS variable for device at FIP address 13
    FGCLITE_STATUS_14,                       // FGClite device STATUS variable for device at FIP address 14
    FGCLITE_STATUS_15,                       // FGClite device STATUS variable for device at FIP address 15
    FGCLITE_STATUS_16,                       // FGClite device STATUS variable for device at FIP address 16
    FGCLITE_STATUS_17,                       // FGClite device STATUS variable for device at FIP address 17
    FGCLITE_STATUS_18,                       // FGClite device STATUS variable for device at FIP address 18
    FGCLITE_STATUS_19,                       // FGClite device STATUS variable for device at FIP address 19
    FGCLITE_STATUS_20,                       // FGClite device STATUS variable for device at FIP address 20
    FGCLITE_STATUS_21,                       // FGClite device STATUS variable for device at FIP address 21
    FGCLITE_STATUS_22,                       // FGClite device STATUS variable for device at FIP address 22
    FGCLITE_STATUS_23,                       // FGClite device STATUS variable for device at FIP address 23
    FGCLITE_STATUS_24,                       // FGClite device STATUS variable for device at FIP address 24
    FGCLITE_STATUS_25,                       // FGClite device STATUS variable for device at FIP address 25
    FGCLITE_STATUS_26,                       // FGClite device STATUS variable for device at FIP address 26
    FGCLITE_STATUS_27,                       // FGClite device STATUS variable for device at FIP address 27
    FGCLITE_STATUS_28,                       // FGClite device STATUS variable for device at FIP address 28
    FGCLITE_STATUS_29,                       // FGClite device STATUS variable for device at FIP address 29
    FGCLITE_STATUS_30,                       // FGClite device STATUS variable for device at FIP address 30

    FGCLITE_DIAG_SEND           = 0x057F,    // FIELDBUS_DIAGNOSTIC (FD) produced variable
    FGCLITE_DIAG_RECV           = 0x067F,    // FIELDBUS_DIAGNOSTIC (FD) consumed variable

    FGCLITE_NANOFIP_RESET       = 0xE000,    // nanoFIP RESET (R) command

    // ID_DAT = 0x91nn specifies an FGClite-specific command. nanoFIP consumes ID_DAT but ignores the second
    // byte. As ID_DAT is not passed through to the FPGAs, the Command ID is passed through in the payload
    // (see FGClite_command::id below).

    FGCLITE_SYNC_CMD            = 0x9100,    // COMMAND0: Set the input of the SEFI detectors (for testing) and clock period adjustment
    FGCLITE_CONTROLLER_CMD      = 0x9102,    // COMMAND2: Command for the FGClite controllers
    FGCLITE_CONVERTER_CMD       = 0x9103,    // COMMAND3: Command to send to the power converter

    // For the serial commands, ID_DAT must be unique, but the commands have to share the same Command
    // ID (0x01). Therefore we use the bottom nybble of the last byte to store the Command ID and the
    // top nybble to store the number of the command within the cycle. We assign
    // FGClite_command::id = ID_DAT & 0x000F

    FGCLITE_SERIAL_CMD0         = 0x9101     // COMMAND1: Serial communications tunnelling over FGClite
};



/*
 * Struct to store status and statistics for a single FGClite
 */

struct FGClite_channel
{
    pthread_mutex_t               status_mutex;                       // Mutexes to protect access to FGClite status memory

    struct FGClite_status         status;                             // FGClite critical and paged status

    uint32_t                      pwr_time;                           // Time of the FGClite power on (from critical data)
    uint16_t                      seq_num;                            // Last seen sequence number
    uint32_t                      status_missed_count;                // Number of consecutive status messages missed

    float                         filtered_cycle_period;              // Filtered value of CYCLE_PERIOD from critical data
};



/*
 * Structure for FGClite data pointers and device statuses
 */

struct FGClite_data
{
    // FGClite Cycle thread and inter-process communication

    pthread_t                     thread;                             // FGClite cycle main thread

    uint64_t                      start_time;                         // Cycle start time for profiling
    sem_t                         cycle_sem;                          // Semaphore to synchronise the start of each 20ms cycle
    struct timespec               cycle_start_mono;                   // Internal reference time for the current 20ms cycle, to synchronise FGCD/FGClite events
    struct timeval                cycle_start_ctri;                   // External reference time for the current 20ms cycle, for publishing, logging, etc.

    uint32_t                      device_present;                     // Presence bits. Bits 1-30 are for FGClites, bit 31 for DIAG_DEV, to match the FGCD status property.
    uint32_t                      device_absent;                      // Absence bits. device_present & device_absent should be zero. Used as a consistency check in case a status arrives late.
    bool                          is_command_ready;                   // Check that the commands C0-C3 are ready to write to the FIP bus
    struct Queue                  nanofip_reset_queue[2];             // Queues to receive soft and hard nanoFIP reset commands

    // Pointers to FGClite variable buffers

    struct FGClite_command       *fip_command[FGCLITE_NUM_CMDS];      // Command variables
                                                                      // These pointers are never explicitly assigned in the code. Presumably they are allocated
                                                                      // by libmasterfip via fipMfipInitProtocol?
    struct FGClite_status        *fip_status[FGCD_MAX_EQP_DEVS];      // Status variables
    struct FGClite_diag          *fip_diag_recv;                      // Diagnostic device received variable
    struct FGClite_nanofip_reset *fip_nanofip_reset;                  // nanoFIP reset variable

    // FGClite status and statistics per channel

    struct FGClite_channel        channel[FGCD_MAX_EQP_DEVS];
};



// Global variables, zero-initialised

struct FIP_properties      fip           = {};    // FGC properties for FIP (extern)
struct FIP_BA_properties   fipba         = {};    // FGC properties for FIP Bus Arbitrator (extern)
struct FGClite_data        fgclite_cycle = {};    // Variable data pointers and IPC for FGClite cycle.



/*
 * Address reinterpret cast macro (to make the FIP_protocol_program table below more legible)
 */

#define ADDR(x) reinterpret_cast<void **>(&x)

/*
 * Definition of the FGClite Fieldbus Cycle.
 *
 * Each line defines one frame of the Bus Arbitrator macrocycle.
 */

struct fgc_fip_per_var_desc fgc_per_varlist1[] = {
    // send COMMAND0
    {FGCLITE_SYNC_CMD, FGC_FIP_PER_VAR_PROD, sizeof(FGClite_command), &syncCommandSent, ADDR(fgclite_cycle.fip_command[CMD_SYNC])},

    // Receive STATUS 1..30
    {FGCLITE_STATUS_1, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[0])},
    {FGCLITE_STATUS_2, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[1])},
    {FGCLITE_STATUS_3, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[2])},
    {FGCLITE_STATUS_4, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[3])},
    {FGCLITE_STATUS_5, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[4])},
    {FGCLITE_STATUS_6, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[5])},
    {FGCLITE_STATUS_7, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[6])},
    {FGCLITE_STATUS_8, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[7])},
    {FGCLITE_STATUS_9, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[8])},
    {FGCLITE_STATUS_10,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[9])},
    {FGCLITE_STATUS_11,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[10])},
    {FGCLITE_STATUS_12,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[11])},
    {FGCLITE_STATUS_13,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[12])},
    {FGCLITE_STATUS_14,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[13])},
    {FGCLITE_STATUS_15,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[14])},
    {FGCLITE_STATUS_16,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[15])},
    {FGCLITE_STATUS_17,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[16])},
    {FGCLITE_STATUS_18,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[17])},
    {FGCLITE_STATUS_19,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[18])},
    {FGCLITE_STATUS_20,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[19])},
    {FGCLITE_STATUS_21,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[20])},
    {FGCLITE_STATUS_22,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[21])},
    {FGCLITE_STATUS_23,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[22])},
    {FGCLITE_STATUS_24,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[23])},
    {FGCLITE_STATUS_25,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[24])},
    {FGCLITE_STATUS_26,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[25])},
    {FGCLITE_STATUS_27,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[26])},
    {FGCLITE_STATUS_28,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[27])},
    {FGCLITE_STATUS_29,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[28])},
    {FGCLITE_STATUS_30,FGC_FIP_PER_VAR_CONS, sizeof(FGClite_status), &statusReceived, ADDR(fgclite_cycle.fip_status[29])},

    // Receive FIELDBUS_DIAGNOSTIC variable
    {FGCLITE_DIAG_RECV, FGC_FIP_PER_VAR_CONS, sizeof(FGClite_diag), &diagReceived, ADDR(fgclite_cycle.fip_diag_recv)},
};
#define FGC_PER_VARLIST1_NVAR sizeof(fgc_per_varlist1) / sizeof(struct fgc_fip_per_var_desc)

struct fgc_fip_per_var_desc fgc_per_varlist2[] = {
    // Send nanoFIP RESET variable and write controller command and converter command to FDM
    {FGCLITE_NANOFIP_RESET, FGC_FIP_PER_VAR_PROD, sizeof(FGClite_nanofip_reset), &writeCommands, ADDR(fgclite_cycle.fip_nanofip_reset)},

    // We have a serial command here to allow time for the writeCommands callback to complete. This takes around 470-490us.
    // The serial command variable takes around 505us.
    {FGCLITE_SERIAL_CMD0, FGC_FIP_PER_VAR_PROD, sizeof(FGClite_command), NULL, ADDR(fgclite_cycle.fip_command[CMD_SER0])},

    // send controller command and converter command
    {FGCLITE_CONTROLLER_CMD, FGC_FIP_PER_VAR_PROD, sizeof(FGClite_command), NULL, ADDR(fgclite_cycle.fip_command[CMD_CTRL])},
    {FGCLITE_CONVERTER_CMD,  FGC_FIP_PER_VAR_PROD, sizeof(FGClite_command), NULL, ADDR(fgclite_cycle.fip_command[CMD_CONV])},
};
#define FGC_PER_VARLIST2_NVAR sizeof(fgc_per_varlist2) / sizeof(fgc_fip_per_var_desc)

struct fgc_fip_mcycle_desc *fgc_mcycle_desc = NULL;


// Callbacks



/*
 * Callback to allow FIP FDM library to use FGCD logging
 */

static int32_t fipMfipLogCallback(const char *error_string)
{
    return logPrintf(0, "FIP_MFIP %s", error_string);
}



/*
 * Callback called when FIP FDM library starts the BA macrocycle program
 */

static void fipMfipBaStartCallback(void)
{
    fipba.start_count++;

    // Clear fieldbus fault

    setStatusBit(&fgcddev.status.st_faults, FGC_FLT_FIELDBUS, false);
}



/*
 * Callback called when FIP FDM library stops the BA macrocycle program
 */

static void fipMfipBaStopCallback(void)
{
    // Set fieldbus fault

    setStatusBit(&fgcddev.status.st_faults, FGC_FLT_FIELDBUS, true);
}



/*
 * Callback when the sync command is sent
 */

static void syncCommandSent(struct mstrfip_dev *dev, struct mstrfip_data *data,
			    struct mstrfip_irq *irq)
{
    // Set the internal reference time for this FIP cycle

    if(clock_gettime(CLOCK_MONOTONIC, &fgclite_cycle.cycle_start_mono) != 0)
    {
        logPrintf(0, "FGCLITE_CYCLE clock_gettime() failed with error %d.\n", errno);
    }

    // Set the CTRI reference time for this FIP cycle
#ifdef DEBUG_TIMING
timeval last_time = fgclite_cycle.cycle_start_ctri;
last_time.tv_sec = -last_time.tv_sec;
last_time.tv_usec = -(last_time.tv_usec+20000);
#endif

    timingReadTime(&fgclite_cycle.cycle_start_ctri);

#ifdef DEBUG_TIMING
timeval current_time = fgclite_cycle.cycle_start_ctri;
fprintf(stderr,"cycle_start_ctri = %ld %ld, current_time = %ld %ld, last_time = %ld %ld\n", fgclite_cycle.cycle_start_ctri.tv_sec, fgclite_cycle.cycle_start_ctri.tv_usec, current_time.tv_sec, current_time.tv_usec, last_time.tv_sec, last_time.tv_usec);
timevalAdd(&current_time, &last_time);
fprintf(stderr,"SYNC_CMD ABS %ld %ld RELATIVE %ld %ld\n",fgclite_cycle.cycle_start_ctri.tv_sec, fgclite_cycle.cycle_start_ctri.tv_usec, current_time.tv_sec, current_time.tv_usec);
#endif
    // The value we receive from the timing library will be around 736us after the CTRI 50 Hz pulse, due to
    // stuffing/bourrage (~640us) and time for FIP FDM to process interrupt. So we round down to the nearest
    // 20 ms boundary.

    uint32_t usec_offset = fgclite_cycle.cycle_start_ctri.tv_usec % 20000;

    fgclite_cycle.cycle_start_ctri.tv_usec -= usec_offset;

    timingSetUserTime(0, &fgclite_cycle.cycle_start_ctri);

    // Similarly adjust the cycle start time for the monotonic clock (used for deadlines within the cycle)

    fgclite_cycle.cycle_start_mono.tv_nsec -= usec_offset * 1000;

    if(fgclite_cycle.cycle_start_mono.tv_nsec < 0)
    {
        fgclite_cycle.cycle_start_mono.tv_sec  -= 1;
        fgclite_cycle.cycle_start_mono.tv_nsec += 1000000000;
    }

    // Clear the presence and absence bits for all the devices

    fgclite_cycle.device_present = 1;    // bit 0 is set as it indicates the presence of the FGCD
    fgclite_cycle.device_absent  = 0;

    // Reset command ready flag

    fgclite_cycle.is_command_ready = false;

    // Unlock the sync semaphore

    if(sem_post(&fgclite_cycle.cycle_sem) == -1)
    {
        logPrintf(0, "FGCLITE_CYCLE Error %d when incrementing semaphore.\n", errno);
    }
}



/*
 * Callback when a status variable is received
 */

static void statusReceived(struct mstrfip_dev *dev, struct mstrfip_data *data,
			   struct mstrfip_irq *irq)
{
    uint32_t channel;

    // Process the received status variable

    channel = fipMfipGetDeviceId(data);

    if(channel > FGCD_MAX_EQP_DEVS)
    {
        logPrintf(0, "FGCLITE_CYCLE Status variable received from illegal WorldFIP bus address (%d)\n", channel);
        return;
    }

    // If we can't lock the mutex, we missed the deadline

    if(pthread_mutex_trylock(&fgclite_cycle.channel[channel-1].status_mutex) == 0)
    {
        // If we can lock the mutex, but the device_absent flag is set, we missed the deadline (by a large margin)

        if((fgclite_cycle.device_absent & (1 << channel)) == 0)
        {
            // Status was received in time
            if(fipMfipReadVar(data, &fip.channel[channel].fip_stats))
            {
		// Status is valid, copy it into the correct FGClite channel

                memcpy(&fgclite_cycle.channel[channel-1].status, fgclite_cycle.fip_status[channel-1], sizeof(struct FGClite_status));

                // The trigger for the onboard DIM0 is not attached, so force the unlatched status for DIM0 to zero

                fgclite_cycle.channel[channel-1].status.critical.dim_a_trig_unl &= 0xFFFE;

                // Interpret the Round Robin bytes

                fgcliteCycleRoundRobin(channel);

                // Set the status bit to present and notify Diamon

                fgclite_cycle.device_present |= 1 << channel;
                fipMfipSetDevicePresent(channel);
            }
        }

        pthread_mutex_unlock(&fgclite_cycle.channel[channel-1].status_mutex);
    }

    if((fgclite_cycle.device_present & (1 << channel)) == 0)
    {
        // If the status is late, increment the late counters

        fip.channel[channel].fip_stats.var_late_count++;
        fip.fieldbus_stats.errors.var_late_count++;
    }

#ifdef DEBUG_TIMING
timeval current_time;
timingReadTime(&current_time);
timeval abs_time = current_time;

timeval cycle_time = fgclite_cycle.cycle_start_ctri;
cycle_time.tv_sec = -cycle_time.tv_sec;
cycle_time.tv_usec = -cycle_time.tv_usec;
timevalAdd(&current_time, &cycle_time);
fprintf(stderr,"STATUS %d ABS %ld %ld RELATIVE %ld %ld\n", channel, abs_time.tv_sec, abs_time.tv_usec, current_time.tv_sec, current_time.tv_usec);
#endif
}



/*
 * Callback when the diagnostic device variable is received
 */

static void diagReceived(struct mstrfip_dev *dev, struct mstrfip_data *data,
			   struct mstrfip_irq *irq)
{
    uint32_t channel;

    // Process the received status variable

    channel = fipMfipGetDeviceId(data);

    // If the device_absent flag is set, we missed the deadline

    if((fgclite_cycle.device_absent & (1 << FIP_DIAG_DEV_ID)) == 0)
    {
        // Response was received in time

        if(fipMfipReadVar(data, &fip.diag_dev))
        {
            // Valid data received, set the status bit to present and notify Diamon

            fgclite_cycle.device_present |= 1 << FIP_DIAG_DEV_ID;    // store DIAG_DEV status in bit 31
            fipMfipSetDevicePresent(channel);                         // notify FDM/Diamon using the actual device address
        }
    }
    else
    {
        fip.diag_dev.var_late_count++;
        fip.fieldbus_stats.errors.var_late_count++;
    }

#ifdef DEBUG_TIMING
timeval current_time;
timingReadTime(&current_time);
timeval abs_time = current_time;

timeval cycle_time = fgclite_cycle.cycle_start_ctri;
cycle_time.tv_sec = -cycle_time.tv_sec;
cycle_time.tv_usec = -cycle_time.tv_usec;
timevalAdd(&current_time, &cycle_time);
fprintf(stderr,"STATUS 31 ABS %ld %ld RELATIVE %ld %ld\n", abs_time.tv_sec, abs_time.tv_usec, current_time.tv_sec, current_time.tv_usec);
#endif
}



/*
 * Callback to write commands at end of cycle
 */

static void writeCommands(struct mstrfip_dev *dev, struct mstrfip_data *data,
			   struct mstrfip_irq *irq)
{

#ifdef DEBUG_TIMING
timeval current_time;
timingReadTime(&current_time);
timeval abs_time = current_time;

timeval cycle_time = fgclite_cycle.cycle_start_ctri;
cycle_time.tv_sec = -cycle_time.tv_sec;
cycle_time.tv_usec = -cycle_time.tv_usec;
timevalAdd(&current_time, &cycle_time);
fprintf(stderr,"SEND_CMDS ABS %ld %ld RELATIVE %ld %ld\n", abs_time.tv_sec, abs_time.tv_usec, current_time.tv_sec, current_time.tv_usec);
#endif

    // Check the commands are ready to write

    if(!fgclite_cycle.is_command_ready)
    {
        logPrintf(0, "FGCLITE_CYCLE Missed deadline, command variables were not written.\n");
        return;
    }

    // Write the FGClite command variables

    fipMfipWriteVars();

#ifdef DEBUG_TIMING
timingReadTime(&current_time);
abs_time = current_time;

timevalAdd(&current_time, &cycle_time);
fprintf(stderr,"WRITE_VARS ABS %ld %ld RELATIVE %ld %ld\n", abs_time.tv_sec, abs_time.tv_usec, current_time.tv_sec, current_time.tv_usec);
#endif

}



// Static functions (not callbacks)

/*
 * Check that the macrocyle was specified correctly.
 *
 * Data structures in FIP_protocol_program are allocated by fipInitProtocol(). This is a sanity check that
 * FIP_protocol_program contains all the variables that need to be allocated (i.e. we don't have any NULL pointers).
 */

static int32_t protocolSanityCheck(void)
{
    uint32_t i;
    uint32_t count = 0;

    for(i = 0; i < FGCLITE_NUM_CMDS; ++i)
    {
        if(fgclite_cycle.fip_command[i] == NULL) ++count;
    }

    for(i = 0; i < FGCD_MAX_EQP_DEVS; ++i)
    {
        if(fgclite_cycle.fip_status[i] == NULL) ++count;
    }

    if(fgclite_cycle.fip_diag_recv == NULL) ++count;

    if(fgclite_cycle.fip_nanofip_reset == NULL) ++count;

    if(count > 0)
    {
        fprintf(stderr, "ERROR: FIP_protocol_program is missing %u FIP variables.\n", count);
        return -1;
    }

    return 0;
}



/*
 * Initialise the status deadlines for the FIP cycle
 */

static void fgcliteCycleInitDeadlines(void)
{
    uint32_t num_devices = 0;

    // Count the number of configured devices

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        if(fgcd.device[channel].name != NULL) ++num_devices;
    }

    uint32_t fgclite_status_duration = (FGCLITE_STATUS_DEADLINE - FGCLITE_STATUS_START) / FGCD_MAX_EQP_DEVS;

    for(uint32_t channel = 1, channel_count = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        if(fgcd.device[channel].name != NULL)
        {
            // Initialise deadline for receiving status packet for this device

            equipdev.deadline[channel] = FGCLITE_STATUS_DEADLINE - ((num_devices - channel_count) * fgclite_status_duration);
            ++channel_count;
        }
        else
        {
            // Don't wait for devices that are not configured

            equipdev.deadline[channel] = 0;
        }
    }

    // Initialise deadline for receiving status from diagnostic device

    equipdev.deadline[FIP_DIAG_DEV_ID] = FIP_DIAG_DEADLINE;
}



/*
 * Interpret the Round Robin bytes
 *
 * The meaning of bytes 40-41 changes each cycle. The interpretation of the bytes is set in byte 52.
 */

static void fgcliteCycleRoundRobin(uint32_t channel)
{
    struct Equipdev_channel &device  = equipdev.device[channel];
    uint16_t                payload  = fgclite_cycle.channel[channel-1].status.critical.roundrobin_register_payload;
    uint8_t                 tag      = fgclite_cycle.channel[channel-1].status.critical.roundrobin_register_tag;
    uint32_t                pwr_time = fgclite_cycle.channel[channel-1].pwr_time;
    struct timeval          time_now;

    switch(tag)
    {
        case UPTIME_COUNTER_LO:
            fgclite_cycle.channel[channel-1].pwr_time = payload;
            break;

        case UPTIME_COUNTER_HI:
            pwr_time = fgclite_cycle.channel[channel-1].pwr_time | (payload << 16);

            // The uptime value we received from the FGClite is in seconds. However, the DEVICE.PWR_TIME
            // property is of type ElapsedTime (and we can't change this as the property is shared with
            // FGC2). So we have to back-calculate the start time of the device from the elapsed time,
            // and the parser will convert it back to the elapsed time when the property is accessed.

            timingGetUserTime(0, &time_now);
            device.device.pwr_time = time_now.tv_sec - pwr_time;
            break;

        case PWRCYCLE_COUNTER:
            device.device.pwrcyc_count  = payload & 0xFF;
            device.device.pwrcyc_reason = payload >> 8;
            break;

        case FPGA_VERSIONS:
            device.device.fpga_version[0] = (payload >> PF_VERSION) & 0xF;
            device.device.fpga_version[1] = (payload >> XF_VERSION) & 0xF;
            device.device.fpga_version[2] = (payload >> CF_VERSION) & 0xF;
            device.device.fpga_version[3] = (payload >> NF_VERSION) & 0xF;
            break;

        case BACKPLANE_TYPE:
            equipdev.device[channel].crate_position = payload & 0x01; // bit 0 encodes the crate position (0=left, 1=right)
            equipdev.device[channel].crate_type     = payload >> 1;   // bits 1-5 encode the crate type
            break;
    }
}



/*
 * Take a FGClite device offline or online
 */

static void fgcliteCycleDeviceOnline(uint32_t channel, bool is_online)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    device.fgcd_device->online = is_online;
    device.fgcd_device->ready  = is_online;

    if(is_online)
    {
        logPrintf(channel, "FGCLITE_CYCLE Device online\n");
        setStatusBit(&device.status.st_faults, FGC_FLT_COMMS, false);
        fgcliteRegResetDevice(channel);
    }
    else
    {
        logPrintf(channel, "FGCLITE_CYCLE Device offline\n");
        fipMfipSetDeviceNotPresent(channel);
        setStatusBit(&device.status.st_faults, FGC_FLT_COMMS, true);
    }
}



/*
 * Reset FGClite devices.
 *
 * Checks the nanoFIP soft or hard reset queue and issues a reset command. If the reset queue is
 * empty, the reset command is set to zero, which is not consumed by any device on the FIP bus.
 *
 * Reset the software configuration of the device and set a flag to indicate to the Equipdev thread
 * that the device's configuration should be reloaded from non-volatile storage.
 */

static void fgcliteCycleReset(enum Nanofip_reset_type soft_hard)
{
    // Cast the void* in the queue to the FIP address. If the queue is empty, queuePop() returns a
    // NULL pointer, which is interpreted as 0 = no reset.

    uint32_t channel = reinterpret_cast<uint64_t>(queuePop(&fgclite_cycle.nanofip_reset_queue[soft_hard])) & 0xFF;

    fgclite_cycle.fip_nanofip_reset->addr[soft_hard] = channel;

    if(channel != 0)
    {
        logPrintf(channel, "FGClite %s reset request sent\n", soft_hard == RESET_HARD ? "hard" : "soft");

        if(soft_hard == RESET_SOFT)
        {
            // Take the device offline and reset the software state. We don't need to do this for a hard
            // reset (PWRCYC), because this takes place around ~10-15 seconds in the future and will be
            // detected when the device power cycles.

            fgcliteCycleDeviceOnline(channel, false);
        }
    }
}



/*
 * Convert a relative time in nanoseconds to an absolute time.
 *
 * @param[out] abs_time      Absolute time = start_time + nsecs
 * @param[in]  start_time    Absolute time to offset from
 * @param[in]  nsecs         Offset in nanoseconds
 */

static void fgcliteCycleRelativeToAbsTime(struct timespec *abs_time, struct timespec *start_time, uint32_t nsecs)
{
    nsecs += start_time->tv_nsec;

    abs_time->tv_sec  = start_time->tv_sec + (nsecs / 1000000000);
    abs_time->tv_nsec = nsecs % 1000000000;
}



/*
 * Sleep until the deadline for each device to report its status
 */

static void fgcliteCycleWaitForStatus(uint32_t channel)
{
    // If we already have a status for this device or any higher-numbered device, return immediately.
    // Note that bit 0 is always set in fgclite_cycle.device_present, so if we have received a status
    // for the current device, device_present > pending_status

    uint32_t pending_status = 1 << channel;

    if(fgclite_cycle.device_present > pending_status) return;

    // Wait for the deadline by which we expect a response from this device. The status callback for the
    // device must acquire the mutex before this deadline in order for the status to be used for this cycle.

    struct timespec deadline;

    fgcliteCycleRelativeToAbsTime(&deadline, &fgclite_cycle.cycle_start_mono, equipdev.deadline[channel]);

    while(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &deadline, NULL) != 0 && errno == EINTR)
    {
        // Make additional checks if nanosleep is interrupted

        if(fgclite_cycle.device_present > pending_status) return;
    }
}



/*
 * Get status, regulate and set commands for one FGClite device
 */

static void fgcliteRegulateDevice(uint32_t channel)
{
    struct FGClite_channel     &fgclite_channel = fgclite_cycle.channel[channel-1];
    struct Equipdev_channel    &device          = equipdev.device[channel];

    uint32_t                    channel_bit     = 1 << channel;

    // Sleep until the deadline for the device to report its status

    fgcliteCycleWaitForStatus(channel);

    // guarantees memory synchronisation as well as thread synchronisation

    pthread_mutex_lock(&fgclite_channel.status_mutex);

    if((fgclite_cycle.device_present & channel_bit) == 0)
    {
        // We didn't receive a status by the deadline

        // Update statistics

        fip.channel[channel].fip_stats.var_miss_count++;   // per-device error stats
        fip.fieldbus_stats.errors.var_miss_count++;        // total error stats for all devices
        fgclite_channel.status_missed_count++;             // count of consecutive missed statuses

        // Set the device absent bit

        fgclite_cycle.device_absent |= channel_bit;

        // If device was not already in fault, we may still be able to regulate

        if(!getStatusBit(device.status.st_faults, FGC_FLT_COMMS))
        {
            if(fgclite_channel.status_missed_count > 1)
            {
                setStatusBit(&device.status.st_faults, FGC_FLT_COMMS, true);
            }

            if(fgclite_channel.status_missed_count == 2)
            {
                // Status has been missed in 2 consecutive cycles, set status to fault in FGCD and report it to Diamon

                fgcliteCycleDeviceOnline(channel, false);
            }
            else
            {
                if(fgclite_channel.status_missed_count == 1)
                {
                    logPrintf(channel, "FGCLITE_CYCLE Single status packet miss the deadline\n");
                }

                fgcliteRegDeviceNotPresent(channel, &fgclite_cycle.channel[channel-1].status, fgclite_cycle.fip_command[CMD_SYNC], fgclite_cycle.fip_command[CMD_CTRL], fgclite_cycle.fip_command[CMD_CONV]);
            }
        }
    }
    else
    {
        // If the device present bit is set, then the status received callback has completed, so no need for mutex locking here

        uint16_t seq_num = fgclite_cycle.channel[channel-1].status.critical.controller_status & SEQUENCE_NUMBER;

        // If the device was offline, bring it online

        if(!device.fgcd_device->online)
        {
            fgcliteCycleDeviceOnline(channel, true);

            fgclite_channel.seq_num = seq_num;
            fgclite_channel.status_missed_count = 0;
        }
        else
        {
            if(seq_num != (fgclite_channel.seq_num + 1) % 4)
            {
                // The 2-bit sequence number was not incremented by 1

                if(seq_num == fgclite_channel.seq_num)
                {
                    logPrintf(channel, "FGCLITE_CYCLE status sequence number is different than expected (was %d instead of %d, seems stuck)\n", seq_num, (fgclite_channel.seq_num + 1) % 4);
                }
                else
                {
                    logPrintf(channel, "FGCLITE_CYCLE status sequence number is different than expected (was %d instead of %d)\n", seq_num, (fgclite_channel.seq_num + 1) % 4);
                }
            }

            fgclite_channel.seq_num = seq_num;
            fgclite_channel.status_missed_count = 0;

            // Regulate device

            fgcliteRegDevicePresent(channel, &fgclite_cycle.channel[channel-1].status, fgclite_cycle.fip_command[CMD_SYNC], fgclite_cycle.fip_command[CMD_CTRL], fgclite_cycle.fip_command[CMD_CONV]);

            // Filter CYCLE_PERIOD and set MS_PERIOD

            fgclite_channel.filtered_cycle_period *= CLOCK_CYCLE_FILTER_PERIOD-1;
            fgclite_channel.filtered_cycle_period += fgclite_channel.status.critical.cycle_period;
            fgclite_channel.filtered_cycle_period /= CLOCK_CYCLE_FILTER_PERIOD;

            device.fgc.cycle_period_raw      = fgclite_channel.status.critical.cycle_period;
            device.fgc.cycle_period_filtered = fgclite_channel.filtered_cycle_period;

            device.fgc.ms_period = device.fgc.ms_period_override ? device.fgc.ms_period_override : fgclite_channel.filtered_cycle_period / 20;

            // Clip value to min/max limits

            if(device.fgc.ms_period < MS_PERIOD_MIN) device.fgc.ms_period = MS_PERIOD_MIN;
            if(device.fgc.ms_period > MS_PERIOD_MAX) device.fgc.ms_period = MS_PERIOD_MAX;

            fgcliteCmdSetMsPeriod(fgclite_cycle.fip_command[CMD_SYNC], channel, device.fgc.ms_period);
        }
    }

    pthread_mutex_unlock(&fgclite_channel.status_mutex);

    // Execute tasks that take place after regulation (whether the device is present or not)

    fgcliteRegPost(channel, &fgclite_cycle.channel[channel-1].status, fgclite_cycle.fip_command[CMD_SYNC], fgclite_cycle.fip_command[CMD_CTRL], fgclite_cycle.fip_command[CMD_CONV]);
}



/*
 * Check for the presence of the FIP diagnostic device
 */

static void fgcliteCycleCheckDiag(void)
{
    fgcliteCycleWaitForStatus(FIP_DIAG_DEV_ID);

    if(fgclite_cycle.device_present & 1 << FIP_DIAG_DEV_ID)
    {
        // Diagnostic device OK, clear alarm

        if(getStatusBit(fgcddev.status.st_warnings, FGC_WRN_WORLDFIP_DIAG))
        {
            logPrintf(0, "FIP Diagnostic device online\n");
            setStatusBit(&fgcddev.status.st_warnings, FGC_WRN_WORLDFIP_DIAG, false);
        }
    }
    else
    {
        // No status received from diagnostic device

        // There is a race condition with the diagReceived() callback when setting the device_absent flag.
        // The only consequence is that potentially we could count the status as received and missed instead
        // of missed and late in the error statistics. Therefore we do not use mutex locks here.

        fgclite_cycle.device_absent |= 1 << FIP_DIAG_DEV_ID;    // store DIAG_DEV status in bit 31

        fip.diag_dev.var_miss_count++;

        if(!getStatusBit(fgcddev.status.st_warnings, FGC_WRN_WORLDFIP_DIAG))
        {
            logPrintf(0, "FIP Diagnostic device offline.\n");
            setStatusBit(&fgcddev.status.st_warnings, FGC_WRN_WORLDFIP_DIAG, true);
            fipMfipSetDeviceNotPresent(FIP_DIAG_DEV_ADDR);
        }
    }
}



/*
 * Check for loss of PC_PERMIT or SECTOR_ACCESS
 */

static void fgcliteCycleCheckInterlocks(void)
{
    // Update Gateway PC_PERMIT

    // Note: pc_permit_override takes value -1 (force false) | 0 (no override) | 1 (force true)

    if((fieldbus.pc_permit_override == 0 && fieldbus.pc_permit == FGC_CTRL_ENABLED) ||    // PC_PERMIT is enabled and not overridden
        fieldbus.pc_permit_override == 1)                                                 // PC_PERMIT_OVERRIDE is enabled and set to TRUE
    {
        fgcddev.status.st_unlatched |= FGC_UNL_PC_PERMIT_SET;
        equipdev.gw_pc_permit = true;
    }
    else
    {
        fgcddev.status.st_unlatched &= ~FGC_UNL_PC_PERMIT_SET;
        equipdev.gw_pc_permit = false;
    }

    // Update Gateway SECTOR_ACCESS

    // Note: sector_access_override has value -1 (force false) | 0 (no override) | 1 (force true)

    if((fieldbus.sector_access_override == 0 && fieldbus.sector_access == FGC_CTRL_ENABLED) ||
        fieldbus.sector_access_override == 1)
    {
        // Sector access is ENABLED

        if(!(fgcddev.status.st_unlatched & FGC_UNL_SECTOR_ACCESS))
        {
            fgcddev.status.st_unlatched |= FGC_UNL_SECTOR_ACCESS;
        }

        for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            // If the device is not simulating interlocks, force the device SECTOR_ACCESS to ENABLED

            if(!(device.status.state_op == FGC_OP_SIMULATION && device.vs.sim_intlks == FGC_CTRL_ENABLED))
            {
                device.sector_access = FGC_CTRL_ENABLED;
            }
        }
    }
    else if(fgcddev.status.st_unlatched & FGC_UNL_SECTOR_ACCESS)
    {
        // Sector access has changed from ENABLED to DISABLED

        fgcddev.status.st_unlatched &= ~FGC_UNL_SECTOR_ACCESS;

        // Set the device SECTOR_ACCESS to DISABLED, but this can be overridden by setting FGC.SECTOR_ACCESS

        for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            // If the device is not simulating interlocks, set the device SECTOR_ACCESS to DISABLED

            if(!(device.status.state_op == FGC_OP_SIMULATION && device.vs.sim_intlks == FGC_CTRL_ENABLED))
            {
                device.sector_access = FGC_CTRL_DISABLED;
            }
        }
    }
}


/*
 * FGClite Cycle thread
 */

static void *fgcliteCycleRun(void *unused)
{
    // It takes up to 30 cycles for the FIP timing to stabilise, so wait at least this long before starting the control loop

    for(uint32_t i = 0; i < 50; ++i)
    {
        if(sem_wait(&fgclite_cycle.cycle_sem) == -1)
        {
            logPrintf(0, "FGCLITE_CYCLE Error %d while waiting on semaphore.\n", errno);
        }

        // Suppress command not ready log messages

        fgclite_cycle.is_command_ready = true;
    }

    // Initialise FIELDBUS properties

    fieldbusInit();

    // Reset FIP error statistics

    fipMfipResetDeviceStats(&fip.fieldbus_stats.errors);

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        fipMfipResetDeviceStats(&fip.channel[channel].fip_stats);
    }

    // Start control loop

    while(true)
    {
        // Check PC_PERMIT and SECTOR_ACCESS interlocks

        fgcliteCycleCheckInterlocks();

        // Check the nanofip reset queues

        fgcliteCycleReset(RESET_SOFT);
        fgcliteCycleReset(RESET_HARD);

        // Set the SERIAL_CMD payloads for this cycle

        serialWriteRsp(reinterpret_cast<uint8_t*>(fgclite_cycle.fip_command[CMD_SER0]->payload));

        // Synchronise to start of cycle

        if(sem_wait(&fgclite_cycle.cycle_sem) == -1)
        {
            logPrintf(0, "FGCLITE_CYCLE Error %d while waiting on semaphore.\n", errno);
            continue;
        }

        // ***** Start of FIP cycle *****

        // Update cycle count and ping the watchdog. error_flag is set in fip_fdm.cpp when there is a fatal
        // error on the FIP interface or a failure to write a FIP variable.

        if(!fip.fieldbus_stats.error_flag) fieldbus.stats.cycle_count = fipMfipCycleTick();

        // Execute tasks that take place before regulation (whether the device is present or not)

        rt.data_start = (rt.data_start + 1) % RT_BUFFER_SIZE;

        for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            if(fipMfipIsDeviceConfigured(channel))
            {
                fgcliteRegPre(channel, &fgclite_cycle.channel[channel-1].status, fgclite_cycle.fip_command[CMD_SYNC], fgclite_cycle.fip_command[CMD_CTRL], fgclite_cycle.fip_command[CMD_CONV]);

                // Get the Real-Time corrections. Note that channel index goes from 0 to FGCD_MAX_EQP_DEVS-1

                if(rt.data[rt.data_start].active_channels & (1LL << (channel -1)))
                {
                    rt.used_data.channels[channel-1] = rt.data[rt.data_start].channels[channel-1];
                }
            }
        }

        // Clear active channels for RT reference

        rt.data[rt.data_start].active_channels = 0;

        // Receive statuses from each active device and regulate

        for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            if(fipMfipIsDeviceConfigured(channel) && fgclite_cycle.fip_nanofip_reset->addr[RESET_SOFT] != channel)
            {
                fgcliteRegulateDevice(channel);
            }
        }

        // Regulation done, set command ready flag

        __sync_synchronize();

        fgclite_cycle.is_command_ready = true;

        __sync_synchronize();
#ifdef DEBUG_TIMING
timeval current_time;
timingReadTime(&current_time);
timeval abs_time = current_time;

timeval cycle_time = fgclite_cycle.cycle_start_ctri;
cycle_time.tv_sec = -cycle_time.tv_sec;
cycle_time.tv_usec = -cycle_time.tv_usec;
timevalAdd(&current_time, &cycle_time);
fprintf(stderr,"COMMAND_READY ABS %ld %ld RELATIVE %ld %ld\n", abs_time.tv_sec, abs_time.tv_usec, current_time.tv_sec, current_time.tv_usec);
#endif

        // Check the status from the diagnostic device

        fgcliteCycleCheckDiag();

        // ***** End of cycle housekeeping *****

        // Update FGC statuses

        fgcddev.status.fgc_mask = fgclite_cycle.device_present;

        if(fgcddev.status.fgc_mask & 0x7FFFFFFE)
        {
            // At least one FGClite is present in the cycle

            setStatusBit(&fgcddev.status.st_faults, FGC_FLT_WORLDFIP_NOFGCS, false);
        }
        else
        {
            setStatusBit(&fgcddev.status.st_faults, FGC_FLT_WORLDFIP_NOFGCS, true);
        }

        // Update published data for FGCD and equipment devices

        pub.published_data.time_sec  = htonl(fgclite_cycle.cycle_start_ctri.tv_sec);
        pub.published_data.time_usec = htonl(fgclite_cycle.cycle_start_ctri.tv_usec);

        fgcddevPublish();
        equipdevPublish();
        pubTrigger();

        // Check status of Post Mortem buffers

        for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            if(fipMfipIsDeviceConfigured(channel)) pmBufferFreeze(channel);
        }
    }

    return 0;
}



// External functions

int32_t fgcliteCycleNanofipReset(uint32_t channel, enum Nanofip_reset_type soft_hard)
{
    // The data item we want to add is smaller than a void *, so just cast the integer to a pointer and add it to the queue.
    // This is simpler than having to allocate storage and dereference the pointer later.

    return queuePush(&fgclite_cycle.nanofip_reset_queue[soft_hard], reinterpret_cast<void*>(channel));
}



struct FGClite_command *fgcliteCycleGetCmdPtr(enum FGClite_protocol_command_id id)
{
    return fgclite_cycle.fip_command[id];
}



struct FGClite_status *fgcliteCycleGetStatusPtr(uint32_t channel)
{
    return &fgclite_cycle.channel[channel-1].status;
}



uint32_t fgcliteCycleGetUptimeCounter(uint32_t channel)
{
    return fgclite_cycle.channel[channel-1].pwr_time;
}



int32_t fgcliteCycleStart(void)
{
    // Initialise the semaphore for synchronising the start of each cycle

    if(sem_init(&fgclite_cycle.cycle_sem, 0, 0) == -1)
    {
        logPrintf(0, "FGCLITE_CYCLE Failed to initialise semaphore.\n");
        return 1;
    }

   // Initialise soft and hard reset request queues

    if(queueInit(&fgclite_cycle.nanofip_reset_queue[RESET_SOFT], NFIP_RESET_QUEUE_SIZE) != 0 ||
       queueInit(&fgclite_cycle.nanofip_reset_queue[RESET_HARD], NFIP_RESET_QUEUE_SIZE) != 0)
    {
        logPrintf(0, "FGCLITE_CYCLE Failed to initialise queue.\n");
        return 1;
    }

    return fgcd_thread_create(&fgclite_cycle.thread, fgcliteCycleRun, NULL, FGCLITE_CYCLE_THREAD_POLICY, FGCLITE_CYCLE_THREAD_PRIORITY, NO_AFFINITY);
}


int32_t fgcliteCycleFipStart(void)
{
    // Set fieldbus fault

    setStatusBit(&fgcddev.status.st_faults, FGC_FLT_FIELDBUS, true);

    // Get default FIP FDM library configuration

    struct FIP_config config;

    if(fipMfipGetConfig(&config) != 0) return -1;

    // Set library configuration options

    config.thread_priority = FIP_FDM_THREAD_PRIORITY;
    config.fieldbus_stats  = &fip.fieldbus_stats;

    // Set callbacks

    config.log_error_func = fipMfipLogCallback;
    config.ba_up_func     = fipMfipBaStartCallback;
    config.ba_down_func   = fipMfipBaStopCallback;

    // Write new configuration to FIP FDM library

    if(fipMfipSetConfig(&config) != 0) return -1;

    // Register configured devices with the FIP FDM library

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        if(fgcd.device[channel].name != NULL) fipMfipConfigureDevice(channel);
    }

    // Start the FIP interface

    return fipMfipStartInterface();
}

/*
 * In C++ there is no easy way to statically initialize an array of struct
 * containing union, because initializer ".member={" is not supoorted.
 * Therefore this function is meant to init the macrocycle description.
 * FGClite macro cycle is made of 4 fip windows:
 * 1 wind: periodic variables window scheduling the varlist1
 * 2 wind: wait window ending at fip_ba_wait_nfip_us in the macrocycle
 * 3 wind: periodic variable window scheduling the varlist2
 * 4 wind: wait window terminating and defining macrocycle duration.
 */
#define FGC_FIP_MCYCLE_DESC_NENTRIES 4
static void fgcliteBuildMacroCycleDesc()
{
    if (fgc_mcycle_desc != NULL)
    	return; // already initialized

    fgc_mcycle_desc = new fgc_fip_mcycle_desc[FGC_FIP_MCYCLE_DESC_NENTRIES];
    /* periodic window */
    fgc_mcycle_desc[0].type = FGC_FIP_PER_VAR_WIND;
    fgc_mcycle_desc[0].per_var_wind.nvar = FGC_PER_VARLIST1_NVAR;
    fgc_mcycle_desc[0].per_var_wind.varlist = fgc_per_varlist1;

    /* wait window */
    fgc_mcycle_desc[1].type = FGC_FIP_WAIT_WIND;
    fgc_mcycle_desc[1].wait_wind.end_ustime = FIP_BA_WAIT_NFIP_US;
    fgc_mcycle_desc[1].wait_wind.silent = 1;

    /* periodic window */
    fgc_mcycle_desc[2].type = FGC_FIP_PER_VAR_WIND;
    fgc_mcycle_desc[2].per_var_wind.nvar = FGC_PER_VARLIST2_NVAR;
    fgc_mcycle_desc[2].per_var_wind.varlist = fgc_per_varlist2;

    /* wait window */
    fgc_mcycle_desc[3].type = FGC_FIP_WAIT_WIND;
    fgc_mcycle_desc[3].wait_wind.end_ustime = FIP_SYN_WAIT_US;
    fgc_mcycle_desc[3].wait_wind.silent = 1;
}

int32_t fgcliteCycleProtocolStart(void)
{
    // Initialise the FIP BA macrocycle program

    fgcliteBuildMacroCycleDesc();
    fipMfipInitProtocol(fgc_mcycle_desc, FGC_FIP_MCYCLE_DESC_NENTRIES);

    if(protocolSanityCheck() != 0) return -1;

    // Set command IDs

    fgcliteCmdSetId(fgclite_cycle.fip_command[CMD_SYNC], FGCLITE_SYNC_CMD);
    fgcliteCmdSetId(fgclite_cycle.fip_command[CMD_CTRL], FGCLITE_CONTROLLER_CMD);
    fgcliteCmdSetId(fgclite_cycle.fip_command[CMD_CONV], FGCLITE_CONVERTER_CMD);
    fgcliteCmdSetId(fgclite_cycle.fip_command[CMD_SER0], FGCLITE_SERIAL_CMD0);

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        // Initialise the status mutex for this device

        if(fgcd_mutex_init(&fgclite_cycle.channel[channel-1].status_mutex, FGCLITE_MUTEX_POLICY) != 0) return -1;

        // Initialise command payloads

        // COMMAND 0: SYNC_CMD

        fgcliteCmdResetSEFI  (fgclite_cycle.fip_command[CMD_SYNC], channel);
        fgcliteCmdSetMsPeriod(fgclite_cycle.fip_command[CMD_SYNC], channel, MS_PERIOD_DEFAULT);
        fgclite_cycle.channel[channel-1].filtered_cycle_period = MS_PERIOD_DEFAULT * 20;

        // COMMAND 1: SERIAL

        fgclite_cycle.fip_command[CMD_SER0]->payload[channel-1] = 0;
        serialReset(channel);

        // COMMAND 2: CONTROLLER_CMD

        fgcliteCmdRequestCtrl(fgclite_cycle.fip_command[CMD_CTRL], channel, DIM_BUS, 0);
        fgcliteCmdResetCtrl  (fgclite_cycle.fip_command[CMD_CTRL], channel);

        // COMMAND 3: CONVERTER_CMD

        fgcliteCmdSetVRef  (fgclite_cycle.fip_command[CMD_CONV], channel, 0);
        fgcliteCmdResetConv(fgclite_cycle.fip_command[CMD_CONV], channel);

        // Start with a COMMS fault until communication is established

        setStatusBit(&equipdev.device[channel].status.st_faults, FGC_FLT_COMMS, true);
    }

    // Initialise the cycle deadlines

    fgcliteCycleInitDeadlines();

    // Initialise the nanoFIP reset bytes

    fgclite_cycle.fip_nanofip_reset->addr[RESET_SOFT] = 0;
    fgclite_cycle.fip_nanofip_reset->addr[RESET_HARD] = 0;

    // Start the FIP BA macrocycle program

    fipMfipStartProtocol();

    // Note: it is not possible to write to variables before AE/LE has started

    fipMfipWriteVars();

    return 0;
}

// EOF
