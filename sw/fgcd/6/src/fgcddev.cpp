/*!
 * @file   fgcddev.cpp
 * @brief  Functions for the FGCD device
 * @author Stephen Page
 * @author Michael Davis
 */

#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <cmdqmgr.h>
#include <consts.h>
#include <classes/6/definfo.h>
#include <classes/6/defprops.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <hash.h>
#include <logging.h>
#include <nonvol.h>
#include <parser.h>
#include <pub.h>
#include <rt.h>
#include <tcp.h>
#include <version.h>
#include <fgc_codes_gen.h>
#include <crc.h>
#include <codes.h>



// Constants

const uint32_t FGCDDEV_FGC_CODE_MIN_LEN = 32;



// Global variables

struct FGCDdev fgcddev;



// Static functions

/*
 * Reset the attributes for a code
 *
 * @retval    Returns the length of the code before the reset
 */

static uint32_t fgcddevResetCodeSlot(uint32_t code_slot)
{
    uint32_t length = fgcddev.fgc_code_length [code_slot];

    fgcddev.fgc_code_name        [code_slot][0] = '\0';
    fgcddev.fgc_code_class       [code_slot]    = 0;
    fgcddev.fgc_code_id          [code_slot]    = FGC_CODE_NULL;
    fgcddev.fgc_code_version     [code_slot]    = 0;
    fgcddev.fgc_code_old_version [code_slot]    = 0;
    fgcddev.fgc_code_chksum      [code_slot]    = 0;
    fgcddev.fgc_code_length      [code_slot]    = 0;

    return length;
}



/*
 * Set the attributes for a code
 */

static void fgcddevSetCodeSlot(uint32_t code_slot, ssize_t length)
{
    if(length >= FGCDDEV_FGC_CODE_MIN_LEN)
    {
        struct FGC_code_info code_info;

        memcpy(&code_info, &fgcddev.fgc_code[code_slot][length - sizeof(code_info)], sizeof(code_info));

        fgcddev.fgc_code_class       [code_slot] =       code_info.class_id;
        fgcddev.fgc_code_id          [code_slot] =       code_info.code_id;
        fgcddev.fgc_code_version     [code_slot] = ntohl(code_info.version);
        fgcddev.fgc_code_old_version [code_slot] = ntohs(code_info.old_version);
        fgcddev.fgc_code_chksum      [code_slot] = ntohs(code_info.crc);
        fgcddev.fgc_code_length      [code_slot] =       length;
    }
}



// External functions

int32_t fgcddevStart(void)
{
    uint32_t                    i;
    struct Parser_class_data    parser_data;

    // Set device, class and platform

    fgcddev.device            = &fgcd.device[0];
    fgcddev.fgc_class_name    = FGC_CLASS_NAME;
    fgcddev.fgc_platform      = FGC_PLATFORM_ID;
    fgcddev.fgc_platform_name = FGC_PLATFORM_NAME;

    // Set published compile time

    fgcddev.status.compile_time = version.time.tv_sec;

    // Set class in published data

    pub.published_data.status[0].class_id = FGC_CLASS_ID;

    // Initialise the property hash

    if(!(fgcddev.prop_hash = hashInit(PROP_HASH_SIZE)))
    {
        fprintf(stderr, "ERROR: Unable to initialise property hash\n");
        return 1;
    }

    // Populate the property hash

    for(i = 0 ; i < N_PROP_SYMBOLS ; i++)
    {
        hashInsert(fgcddev.prop_hash, &SYM_TAB_PROP[i]);
    }

    // Initialise the constant hash

    if((fgcddev.const_hash = hashInit(CONST_HASH_SIZE)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise constant hash\n");

        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Populate the const hash

    for(i = 0 ; i < N_CONST_SYMBOLS ; i++)
    {
        hashInsert(fgcddev.const_hash, &SYM_TAB_CONST[i]);
    }

    // Give values to test properties

    fgcddev.test_string[0] = const_cast<char*>("FIRST");
    fgcddev.test_string[1] = const_cast<char*>("SECOND");

    // Set code name pointers to allow names to be read through string property

    for(i = 0; i < FGC_CODE_NUM_SLOTS; ++i)
    {
        fgcddev.fgc_code_name_property[i] = fgcddev.fgc_code_name[i];
    }

    // Read code files

    if(fgcddevReadCodes() != 0)
    {
        fprintf(stderr, "ERROR: Failed to read FGC code files\n");

        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);

        return 1;
    }

    // Set parser data for class

    parser_data.class_id        = FGC_CLASS_ID;
    parser_data.sym_tab_const   = SYM_TAB_CONST;
    parser_data.const_hash      = fgcddev.const_hash;
    parser_data.sym_tab_prop    = SYM_TAB_PROP;
    parser_data.sym_tab_prop_len = sizeof(SYM_TAB_PROP)/sizeof(struct hash_entry);
    parser_data.prop_hash       = fgcddev.prop_hash;
    parser_data.get_func        = GET_FUNC;
    parser_data.pars_func       = PARS_FUNC;
    parser_data.num_pars_func   = NUM_PARS_FUNC_6;
    parser_data.set_func        = SET_FUNC;
    parser_data.setif_func      = SETIF_FUNC;
    parser_data.evtlog_func     = NULL;

    if(parserSetClassData(&parser_data) != 0)
    {
        fprintf(stderr, "ERROR: Failed to set parser class data\n");

        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Start the parser

    if(parserStart(&fgcddev.parser) != 0)
    {
        fprintf(stderr, "ERROR: Failed to start parser\n");

        parserClearClassData(FGC_CLASS_ID);
        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Set device command queue

    fgcddev.device->command_queue = &fgcddev.parser.command_queue;

    // Set device as online

    fgcddev.device->online  = 1;
    fgcddev.device->ready   = 1;

    // Load configuration

    nonvolLoad(fgcddev.device, 0);

    // Load properties that must be set last

    nonvolLoad(fgcddev.device, 1);

    return 0;
}



void fgcddevInitDevice(void)
{
    // Initialise the name database and code information to zero

    struct FGC_code_info    code_info = {};
    struct fgc_name_db      name_db   = {};

    // Set name of code

    snprintf(fgcddev.fgc_code_name[0], sizeof(fgcddev.fgc_code_name[0]), "name");

    // Insert name data for each device

    for(uint32_t i = 0; i < FGCD_MAX_DEVS && i < FGC_CODE_NAMEDB_MAX_DEVS; ++i)
    {
        name_db.dev[i].class_id     = htons(fgcd.device[i].fgc_class);
        name_db.dev[i].omode_mask   = htons(fgcd.device[i].omode_mask);

        // Check whether a name is defined for the device

        if(fgcd.device[i].name)
        {
            strncpy(name_db.dev[i].name, fgcd.device[i].name, FGC_MAX_DEV_LEN);
            name_db.dev[i].name[FGC_MAX_DEV_LEN] = '\0';
        }
        else // No name is defined
        {
            name_db.dev[i].name[0] = '\0';
        }
    }

    // Insert name data for each sub-device

    uint32_t sub_dev_num = 0;

    for(uint32_t i = 1; i < FGCD_MAX_DEVS && i < FGC_CODE_NAMEDB_MAX_DEVS; ++i)
    {
        for(uint32_t channel = 1; channel <= FGCD_MAX_SUB_DEVS_PER_DEV && sub_dev_num <  FGC_CODE_NAMEDB_MAX_SUBDEVS; ++channel)
        {
            if(devname.device_sub_devices[i][channel]) // A sub-device is present
            {
                // Increment number of sub_devs for device

                name_db.num_sub_devs[i]++;

                // Set sub-device index

                name_db.sub_dev[sub_dev_num].index = devname.device_sub_devices[i][channel]->index;

                // Set sub-device name

                strncpy(name_db.sub_dev[sub_dev_num].name, devname.device_sub_devices[i][channel]->key.c, sizeof(name_db.sub_dev[sub_dev_num].name));
                name_db.sub_dev[sub_dev_num].name[sizeof(name_db.sub_dev[sub_dev_num].name) - 1] = '\0';

                sub_dev_num++;
            }
        }
    }

    // Initialise the name code to zero

    memset(&fgcddev.fgc_code[0], 0, sizeof(fgcddev.fgc_code[0]));

    // Copy the name database into the code slot

    memcpy(&fgcddev.fgc_code[0], &name_db, sizeof(name_db));

    fgcddev.fgc_code_class  [0] = code_info.class_id                  = 0;
    fgcddev.fgc_code_id     [0] = code_info.code_id                   = 0;
    fgcddev.fgc_code_version[0] = fgcddev.fgc_code_old_version[0]     = 0;
    code_info.version           = htonl(fgcddev.fgc_code_version[0]);

    // Copy code information to the name code

    memcpy(&fgcddev.fgc_code[0][(FGC_CODE_NAMEDB_SIZE_BLKS * FGC_CODE_BLK_SIZE) - sizeof(code_info)], &code_info, sizeof(code_info));

    // Generate CRC

    fgcddev.fgc_code_length[0]  = FGC_CODE_NAMEDB_SIZE_BLKS * FGC_CODE_BLK_SIZE;
    fgcddev.fgc_code_chksum[0]  = crcCrc16((unsigned char *)&fgcddev.fgc_code[0], fgcddev.fgc_code_length[0] - sizeof(code_info.crc));
    code_info.crc               = htons(fgcddev.fgc_code_chksum[0]);

    // Copy CRC to the name code

    memcpy(&fgcddev.fgc_code[0][fgcddev.fgc_code_length[0] - sizeof(code_info.crc)], &code_info.crc, sizeof(code_info.crc));
}



void fgcddevPublish(void)
{
    uint32_t            pub_ms      = ntohl(pub.published_data.time_usec) / 1000;
    struct fgc6_stat    *pub_status = &pub.published_data.status[0].class_data.c6;

    // Check whether this is the first publication of the second

    if(!(pub_ms / FGCD_CYCLE_PERIOD_MS))
    {
        // Update per second TCP and UDP load statistics

        fgcddev.status.udp_pub_load = pub.udp_load;
        pub.udp_load                = 0;

        rt.udp_load = 0;

        fgcddev.status.tcp_load = tcp.load;
        tcp.load                = 0;
    }

    // Set unlatched statuses for RT_HOLD and RT_ZERO

    setStatusBit(&fgcddev.status.st_unlatched, FGC_UNL_RT_HOLD, rt.hold_values          == FGC_CTRL_ENABLED);
    setStatusBit(&fgcddev.status.st_unlatched, FGC_UNL_RT_ZERO, fieldbus.real_time_zero == FGC_CTRL_ENABLED);

    // Copy device status into published data

    memcpy(pub_status, &fgcddev.status, sizeof(fgcddev.status));

    // Byte-swap published data

    pub_status->cmw_subs        = htons(pub_status->cmw_subs);
    pub_status->compile_time    = htonl(pub_status->compile_time);
    pub_status->fgc_mask        = htonl(pub_status->fgc_mask);
    pub_status->st_faults       = htons(pub_status->st_faults);
    pub_status->st_unlatched    = htons(pub_status->st_unlatched);
    pub_status->st_warnings     = htons(pub_status->st_warnings);
    pub_status->tcp_load        = htonl(pub_status->tcp_load);
    pub_status->udp_pub_load    = htonl(pub_status->udp_pub_load);
}



int32_t fgcddevReadCodes(void)
{
    char        code_path[256];         // Path to directory containing codes
    struct stat stat_buf;
    uint32_t    code_slot;

    // Zero attributes of all codes. Ignore slot zero (name code), which is generated by devnameRead().

    for(code_slot = 1; code_slot < FGC_CODE_NUM_SLOTS; ++code_slot)
    {
        fgcddevResetCodeSlot(code_slot);
    }

    // Construct code group path for this host

    snprintf(code_path, sizeof(code_path), "%s/code_groups/%s", CONFIG_PATH, fgcd.group);

    // Read code directory in lexicographical order

    int32_t            namelist_entries;
    struct dirent    **namelist;

    if((namelist_entries = scandir(code_path, &namelist, NULL, alphasort)) < 0)
    {
        perror("scandir");
        return 1;
    }

    // Read each code file

    int32_t retval = 0;

    int32_t namelist_item;

    for(namelist_item = 0, code_slot = 1; code_slot < FGC_CODE_NUM_SLOTS && namelist_item < namelist_entries; ++namelist_item)
    {
        char        filename[256];
        int32_t     file;

        // Find the next regular file in the codes directory

        snprintf(filename, sizeof(filename), "%s/%s", code_path, namelist[namelist_item]->d_name);

        if(stat(filename, &stat_buf))
        {
            perror("stat");
            retval = 1;
            break;
        }

        if(!S_ISREG(stat_buf.st_mode)) continue;

        // Open code file

        if((file = open(filename, O_RDONLY)) < 0)
        {
            logPrintf(0, "FGCDDEV Error opening code file %s\n", filename);
            retval = 1;
            continue;
        }

        // Read code

        ssize_t length;

        if((length = read(file, reinterpret_cast<void*>(&fgcddev.fgc_code[code_slot]), FGC_CODE_SIZE)) < 0) // Error reading file
        {
            logPrintf(0, "FGCDDEV Error reading code file %s\n", filename);
            retval = 1;
            continue;
        }

        close(file);

        // Extract information from code header

        fgcddevSetCodeSlot(code_slot, length);

        // Set code name

        strncpy(fgcddev.fgc_code_name[code_slot], namelist[namelist_item]->d_name, sizeof(fgcddev.fgc_code_name[code_slot]));
        fgcddev.fgc_code_name[code_slot][sizeof(fgcddev.fgc_code_name[code_slot]) - 1] = '\0';

        logPrintf(0, "FGCDDEV Read %d bytes from code file %s into slot %u\n", length, filename, code_slot);

        ++code_slot;
    }

    // Free memory allocated by scandir()

    for(namelist_item = 0; namelist_item < namelist_entries; ++namelist_item)
    {
        free(namelist[namelist_item]);
    }
    free(namelist);

    // Check whether there are still code files remaining

    if(namelist_entries > FGC_CODE_NUM_SLOTS)
    {
        logPrintf(0, "FGCDDEV More code files than available slots\n");
        retval = 1;
    }

    // Initialise the FGC_DB

    if(codesInit() != 0)
    {
        logPrintf(0, "FGCDDEV Failed to read FGC codes\n");
        retval = 1;
    }

    return retval;
}



void fgcddevSetFGCCodeInfo(char *code)
{
    for(uint32_t code_slot = 0; code_slot < FGC_CODE_NUM_SLOTS; ++code_slot)
    {
        // Identify code slot

        if(code == &fgcddev.fgc_code[code_slot][0])
        {
            // Zero code attributes

            ssize_t length = fgcddevResetCodeSlot(code_slot);

            // Set code attributes

            fgcddevSetCodeSlot(code_slot, length);

            return;
        }
    }
}



char *fgcddevGetCodePtr(enum fgc_code_ids id)
{
    for(uint32_t code_slot = 0; code_slot < FGC_CODE_NUM_SLOTS; ++code_slot)
    {
        if(id == fgcddev.fgc_code_id[code_slot]) return &fgcddev.fgc_code[code_slot][0];
    }

    return NULL;
}

// EOF
