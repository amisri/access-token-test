/*!
 * @file   digital_io.cpp
 * @brief  Functions for I/O across the FGClite digital interface
 * @author Michael Davis
 */

//#define DEBUG
#ifdef DEBUG
#include <cstdio>
#endif

#include <libref.h>

#include <fgcd.h>
#include <fgclite_cmd.h>
#include <digital_io.h>



// Constants

const uint32_t DIG_CMD_PULSE_CYCLES = 2;    // Commands should be pulsed and remain high this many fieldbus cycles



/*
 * Lookup table for Voltage Source state from six direct digital inputs (VSFAULT and VSEXTINTLK are merged)
 */

static const uint8_t vs_state_lookup[] =
{                           //      IDX  VSRUN:VSPOWERON:VSREADY:FASTABORT:VSFAULT|VSEXTINTLK
    FGC_VS_OFF,             //       0      0       0       0       0       0
    FGC_VS_FLT_OFF,         //       1      0       0       0       0       1
    FGC_VS_FASTPA_OFF,      //       2      0       0       0       1       0
    FGC_VS_FLT_OFF,         //       3      0       0       0       1       1
    FGC_VS_INVALID,         //       4      0       0       1       0       0
    FGC_VS_INVALID,         //       5      0       0       1       0       1
    FGC_VS_INVALID,         //       6      0       0       1       1       0
    FGC_VS_INVALID,         //       7      0       0       1       1       1
    FGC_VS_STOPPING,        //       8      0       1       0       0       0
    FGC_VS_FAST_STOP,       //       9      0       1       0       0       1
    FGC_VS_FAST_STOP,       //      10      0       1       0       1       0
    FGC_VS_FAST_STOP,       //      11      0       1       0       1       1
    FGC_VS_INVALID,         //      12      0       1       1       0       0
    FGC_VS_INVALID,         //      13      0       1       1       0       1
    FGC_VS_INVALID,         //      14      0       1       1       1       0
    FGC_VS_INVALID,         //      15      0       1       1       1       1
    FGC_VS_STARTING,        //      16      1       0       0       0       0
    FGC_VS_FLT_OFF,         //      17      1       0       0       0       1
    FGC_VS_FASTPA_OFF,      //      18      1       0       0       1       0
    FGC_VS_FLT_OFF,         //      19      1       0       0       1       1
    FGC_VS_INVALID,         //      20      1       0       1       0       0
    FGC_VS_INVALID,         //      21      1       0       1       0       1
    FGC_VS_INVALID,         //      22      1       0       1       1       0
    FGC_VS_INVALID,         //      23      1       0       1       1       1
    FGC_VS_STARTING,        //      24      1       1       0       0       0
    FGC_VS_FAST_STOP,       //      25      1       1       0       0       1
    FGC_VS_FAST_STOP,       //      26      1       1       0       1       0
    FGC_VS_FAST_STOP,       //      27      1       1       0       1       1
    FGC_VS_READY,           //      28      1       1       1       0       0
    FGC_VS_INVALID,         //      29      1       1       1       0       1
    FGC_VS_INVALID,         //      30      1       1       1       1       0
    FGC_VS_INVALID          //      31      1       1       1       1       1
};

// External functions

uint8_t digitalIOVsGetState(const struct Digital_IO *dig, bool state_pc_is_starting)
{
    // Create state index from the five direct digital inputs

    uint32_t index;

    index  = digitalIOGetStatus(dig, (FGC_DIG_STAT_VS_FAULT | FGC_DIG_STAT_VS_EXTINTLK));
    index |= digitalIOGetStatus(dig, FGC_DIG_STAT_FAST_ABORT)  << 1;
    index |= digitalIOGetStatus(dig, FGC_DIG_STAT_VS_READY)    << 2;
    index |= digitalIOGetStatus(dig, FGC_DIG_STAT_VS_POWER_ON) << 3;
    index |= digitalIOGetStatus(dig, FGC_DIG_STAT_VS_RUN)      << 4;

    if(state_pc_is_starting == false && vs_state_lookup[index] == FGC_VS_STARTING)
    {
        // STATE is not in STARTING state, so the voltage source should not be in VS_STARTING.
        // This would imply that either VSREADY or VSPOWERON have been reset by the voltage source without a reason.
        // This was seen sometimes and its important to log this as an VS_INVALID state.

        return FGC_VS_INVALID;
    }
    else
    {
        return vs_state_lookup[index];
    }
}



void digitalIORequestCmd(struct Digital_IO *dig, enum Digital_IO_Request request)
{
    // Set the request mask that will be treated by digitalIOProcessRequests

    dig->request |= request;
}



void digitalIOProcessRequests(struct Digital_IO *dig)
{
    // Control VS_RUN command output - OFF request has priority if both OFF and ON are requested at the same time

    if(dig->request & DIG_IO_VS_OFF)
    {
        dig->commands &= ~FGC_DIG_CMDS_VS_RUN;
    }
    else if(dig->request & DIG_IO_VS_ON)
    {
        dig->commands |=  FGC_DIG_CMDS_VS_RUN;
    }

    // Control VS_RESET command output - this must be pulsed

    if(dig->request & DIG_IO_VS_RESET)
    {
        // VS_RESET requested - set the VS_RESET command bit and the down counter so that it will be reset after 40ms

        dig->commands      |= FGC_DIG_CMDS_VS_RESET;
        dig->vs_reset_timer = DIG_CMD_PULSE_CYCLES;
    }
    else
    {
        // VS_RESET is not requested - check if a VS_RESET pulse is in progress

        if(dig->vs_reset_timer > 0 && --dig->vs_reset_timer == 0)
        {
            // VS_RESET timer has expired so reset the VS_RESET command

            dig->commands &= ~FGC_DIG_CMDS_VS_RESET;
        }
    }

    // Clear the requests

    dig->request = 0;
}



void digitalIOPolSwitch(struct Digital_IO *dig, uint32_t pol_switch_state, uint32_t pol_switch_requested_state)
{
    // Control POLSWITCH commands - the requested state comes from libref which manages the polarity switch

    if(pol_switch_requested_state != dig->pol_switch_requested_state)
    {
        // Requested state has changed

        dig->pol_switch_requested_state = pol_switch_requested_state;

        // Only pulse a polarity switch command signal if we are now in the other state

        if(pol_switch_state == FGC_POL_POSITIVE && pol_switch_requested_state == FGC_POL_NEGATIVE)
        {
            dig->commands            |= FGC_DIG_CMDS_POL_TO_NEG;
            dig->pol_switch_cmd_timer = DIG_CMD_PULSE_CYCLES;
        }
        else if(pol_switch_state == FGC_POL_NEGATIVE && pol_switch_requested_state == FGC_POL_POSITIVE)
        {
            dig->commands            |= FGC_DIG_CMDS_POL_TO_POS;
            dig->pol_switch_cmd_timer = DIG_CMD_PULSE_CYCLES;
        }
    }

    // Check if a command is in progress and reset it when the timer expires

    if(dig->pol_switch_cmd_timer > 0 && --dig->pol_switch_cmd_timer == 0)
    {
        // POL_SWITCH command timer has expired so reset the command

        dig->commands &= ~(FGC_DIG_CMDS_POL_TO_POS | FGC_DIG_CMDS_POL_TO_NEG);
    }
}



void digitalIOSetCmd(struct Digital_IO *dig, uint16_t mask, bool value)
{
    if(value)
    {
        dig->commands |= mask;
    }
    else
    {
        dig->commands &= ~mask;
    }
}



uint8_t digitalIOGetCmd(struct Digital_IO *dig)
{
    return dig->commands & 0xFF;
}



void digitalIOSetStatus(struct Digital_IO *dig, uint16_t mask, bool value)
{
    if(value)
    {
        dig->status |= mask;
    }
    else
    {
        dig->status &= ~mask;
    }
}



bool digitalIOGetStatus(const struct Digital_IO *dig, uint16_t mask)
{
    return dig->status & mask;
}

// EOF
