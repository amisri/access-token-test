// @file   fgclite_reg.cpp
// @brief  FGClite regulation functions

#include <limits>

#include <cmwpub.h>
#include <logging.h>
#include <timing.h>
#include <rt.h>

#include <equipdev.h>
#include <log.h>
#include <dim_state.h>
#include <mode_pc.h>
#include <page_mgr.h>
#include <state_op.h>
#include <state_pc.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgclite_cmd.h>
#include <prop_equip.h>
#include <fieldbus.h>
#include <serial.h>
#include <cc_sig.h>
#include <equip_liblog.h>
#include <fgclite_reg.h>

// Import symbol definitions for CMW logging

#pragma GCC diagnostic ignored "-Wwrite-strings" // Disable warning about deprecated conversion from string constant to char*
#define DEFSYMS
#include <classes/92/defsyms.h>
#pragma GCC diagnostic warning "-Wwrite-strings"



// Constants

const uint32_t       NFIP_SW_INTERLOCK_DELAY   = 4;          // Delay time in secs for software interlock before a nanoFIP power cycle
const uint32_t       NFIP_VREF_RAMP_DOWN_DELAY = 4;          // Delay time in secs for V_REF ramp down before a nanoFIP power cycle
const uint32_t       NFIP_PWR_CYC_DELAY        = 5;          // Delay time in secs for a nanoFIP power cycle to take place
const uint32_t       NFIP_WFIP_DELAY           = 150000;     // Delay time in usecs based on device WorldFIP address

const float          ULEAD_WARN_SET_LIMIT      = 0.1;        // Limit of the lead voltage


// Static functions


// Log ADC values at 100Hz if the difference between I_A and I_B unfiltered is bigger than LIMITS.I.MEAS_DIFF
//

static void fgliteRegLogOnIMeas(int8_t channel)
{
   struct Equipdev_channel &device = equipdev.device[channel];
   struct FGCD_device &fgcd_device = fgcd.device[channel];

   float v_1_a,v_2_a,i_1_a,i_2_a;
   float v_1_b,v_2_b,i_1_b,i_2_b;

    // Channel A log the two 100Hz samples

    v_1_a = sigAdcRawToVoltsRT(device.sig_struct.adc.named.adc_a.cal.active,device.meas.raw[0].i_a);
    v_2_a = sigAdcRawToVoltsRT(device.sig_struct.adc.named.adc_a.cal.active,device.meas.raw[1].i_a);
    i_1_a = sigTransducerVadcToPhysUnitsRT(device.sig_struct.transducer.named.dcct_a.cal.active, v_1_a);
    i_2_a = sigTransducerVadcToPhysUnitsRT(device.sig_struct.transducer.named.dcct_a.cal.active, v_2_a);

    // Channel B log the two 100Hz samples

    v_1_b = sigAdcRawToVoltsRT(device.sig_struct.adc.named.adc_b.cal.active,device.meas.raw[0].i_b);
    v_2_b = sigAdcRawToVoltsRT(device.sig_struct.adc.named.adc_b.cal.active,device.meas.raw[1].i_b);
    i_1_b = sigTransducerVadcToPhysUnitsRT(device.sig_struct.transducer.named.dcct_b.cal.active, v_1_b);
    i_2_b = sigTransducerVadcToPhysUnitsRT(device.sig_struct.transducer.named.dcct_b.cal.active, v_2_b);

    // If the device is online in normal operation,
    // and production or running,
    // and in V mode,
    // and with the two DCCTs selected,
    // then check for I_DIFF warnings

    if(   fgcd_device.online
       && device.status.state_op == FGC_MODE_OP_NORMAL
       && (   (   fgcd_device.omode_mask
               && device.sig_struct.select.named.i_meas.selector == FGC_DCCT_SELECT_AB)
           || (   device.status.state_pc != FGC_PC_OFF
               && device.status.state_pc != FGC_PC_FLT_OFF))
       && refMgrParValue(&device.ref_mgr, MODE_REG_MODE) != FGC_REG_V
       && (   (fabs(i_1_a-i_1_b) > device.sig_struct.select.named.i_meas.diff_warn_limit)
           || (fabs(i_2_a-i_2_b) > device.sig_struct.select.named.i_meas.diff_warn_limit)))
    {
        logPrintf(channel,"FGCLITEREG CHANNEL %s: I_MEAS WARNING: raw_100Hz = %d (0X%X) & %d (0X%X), v_100Hz = %f & %f V, i_100Hz = %f & %f A\n",
            "A",
            device.meas.raw[0].i_a,
            device.meas.raw[0].i_a,
            device.meas.raw[1].i_a,
            device.meas.raw[1].i_a,
            v_1_a,
            v_2_a,
            i_1_a,
            i_2_a);

        logPrintf(channel,"FGCLITEREG CHANNEL %s: I_MEAS WARNING: raw_100Hz = %d (0X%X) & %d (0X%X), v_100Hz = %f & %f V, i_100Hz = %f & %f A\n",
            "B",
            device.meas.raw[0].i_b,
            device.meas.raw[0].i_b,
            device.meas.raw[1].i_b,
            device.meas.raw[1].i_b,
            v_1_b,
            v_2_b,
            i_1_b,
            i_2_b);
    }
}

// Get the cycle time for the non-PPM user and return it as a CCLIBS absolute time
//
// If T is the cycle time, the time for the measurements are (T-10ms, T), so we subtract 10ms here
//
// @param[out] timestamp    Cycle time for the current FIP cycle

static void fgcliteRegGetTimestamp(struct CC_us_time *timestamp, bool *is_200ms_boundary, bool *is_1s_boundary)
{
    const struct timeval MINUS_HALF_CYCLE = { 0, -10000 };    // -10 ms

    struct timeval user_time;

    timingGetUserTime(0, &user_time);
    timevalAdd(&user_time, &MINUS_HALF_CYCLE);

    timestamp->secs.abs = user_time.tv_sec;
    timestamp->us = user_time.tv_usec;

    // ms time is 10,30,50,... so time for this cycle will never lie on a 200ms or 1s boundary

    *is_200ms_boundary = false;
    *is_1s_boundary    = false;
}



// Increment a CCLIBS timestamp by half a cycle (10ms)
//
// @param[in,out] timestamp    timestamp to increment

static void fgcliteRegCycleIncTimestamp(struct CC_us_time *timestamp, bool *is_200ms_boundary, bool *is_1s_boundary)
{
    const int32_t US_10_MS     =   10000;
    const int32_t US_200_MS    =  200000;
    const int32_t US_1_SEC     = 1000000;

    timestamp->us += US_10_MS;

    if(timestamp->us >= US_1_SEC)
    {
        timestamp->secs.abs += 1;
        timestamp->us -= US_1_SEC;
    }

    // Set flags if the time for this cycle lies on a 200ms or 1s boundary

    *is_200ms_boundary = (timestamp->us % US_200_MS == 0);
    *is_1s_boundary    = (timestamp->us             == 0);
}


// Set polarity switch signals for  to use
/*
static void fgcliteRegPolSwitch(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Pass Polarity Switch position signals to libref and to ST_UNLATCHED

    device.polswitch.positive = digitalIOGetStatus(&device.dig, FGC_DIG_STAT_POL_SWI_POS) ? CC_ENABLED : CC_DISABLED;
    device.polswitch.negative = digitalIOGetStatus(&device.dig, FGC_DIG_STAT_POL_SWI_NEG) ? CC_ENABLED : CC_DISABLED;

    setStatusBit(&device.status.st_unlatched, FGC_UNL_POL_SWI_POS, device.polswitch.positive != CC_DISABLED);
    setStatusBit(&device.status.st_unlatched, FGC_UNL_POL_SWI_NEG, device.polswitch.negative != CC_DISABLED);

    // Set polarity switch simulation mode if STATE.OP is SIMULATION and VS.SIM_INTLKS is ENABLED

    device.polswitch.mode_sim = device.status.state_op == FGC_OP_SIMULATION && device.vs.sim_intlks == FGC_CTRL_ENABLED ? CC_ENABLED : CC_DISABLED;
}
*/


// Simulate the digital status registers and interlocks. There are three simulation modes,
// selected using MODE.OP and VS.SIM_INTLKS. In NORMAL mode (!SIMULATION), the status
// registers are set directly in fgcliteRegDevicePresent(). In simulation modes, the
// registers are set according to the following rules:
//
//      MODE.OP      VS.SIM_INTLKS          VSRUN/VSREADY               All other
//                                            VSPOWERON                  signals
//  --------------------------------------------------------------------------------------
//  1: !SIMULATION       N/A                    REAL                      REAL
//  2:  SIMULATION     DISABLED               SIMULATED                   REAL
//  3:  SIMULATION     ENABLED                SIMULATED                 SIMULATED
//  --------------------------------------------------------------------------------------
//
//               Input/Cmd         Mode 1          Mode 2          Mode 3
//                Signal           NORMAL        !SIM_INTLKS       SIM ALL
//              -----------------------------------------------------------------
//              DIG watchdog      TRIGGERED       TRIGGERED    NON-TRIGGERED
//              -----------------------------------------------------------------
//              VSNOCABLE           REAL            REAL            0
//              TIMEOUT             REAL            REAL            0
//              VSRUN               REAL            SIM             SIM
//              PWRFAILURE          REAL            REAL            SIM
//              INTLKSPARE          Software overlay based on PCPERMIT
//              PCPERMIT            REAL            REAL            1
//              PCDISCHRQ           REAL            REAL            0
//              FASTABORT           REAL            REAL            0
//              POLSWINEG           REAL            REAL            SIM
//              POLSWIPOS           REAL            REAL            SIM
//              DCCTAFLT            REAL            REAL            0
//              DCCTBFLT            REAL            REAL            0
//              VSEXTINTLK          REAL            REAL            0
//              VSFAULT             REAL            REAL            0
//              VSREADY             REAL            SIM             SIM
//              VSPOWERON           REAL            SIM             SIM
//              -----------------------------------------------------------------
//              FGCOKCMD            REAL            REAL            SIM
//              INTLKOUTCMD         REAL            REAL            0
//              AFRUNCMD            REAL            SIM             SIM
//
//              POLTONEGCMD         REAL            REAL            0
//              POLTOPOSCMD         REAL            REAL            0
//              VSRESETCMD          REAL            REAL            0
//              VSRUNCMD            REAL            REAL            SIM



// Set Active Filter command
//
// Used in mode 1

static void fgcliteRegSetAF(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(!(digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_AF_RUN)     &&    // AF_RUN is not active
       device.vs.active_filter == FGC_CTRL_ENABLED               &&    // Active filter is enabled
       device.status.state_vs  == FGC_VS_READY                   &&    // VS is running
       device.state_pc_time_ms  > 1000)                                // time in current state is >= 1s
    {
        digitalIOSetCmd(&device.dig, FGC_DIG_CMDS_AF_RUN, true);
    }
    else if((digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_AF_RUN) &&    // AF_RUN is active
            (device.vs.active_filter == FGC_CTRL_DISABLED        ||    // Active filter is disabled
            device.status.state_vs != FGC_VS_READY))                   // VS is not running
    {
        digitalIOSetCmd(&device.dig, FGC_DIG_CMDS_AF_RUN, false);
    }
}



// Simulate Voltage Source
//
// Used in modes 2 & 3

static void fgcliteRegSimulateVS(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // VS_RUN, VS_POWER_ON and VS_READY are not reset at the start of the iteration. This function
    // will set/reset them to simulate the behaviour of the power converter.

    // Simulate response to VS_RUN request

    if(digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_VS_RUN)
    {
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_POWER_ON | FGC_DIG_STAT_VS_RUN, true);

        // Simulate starting Voltage Source (3s)

        if(device.state_pc_time_ms > 3000L)
        {
            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_READY, true);
        }
    }
    else
    {
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_READY | FGC_DIG_STAT_VS_RUN, false);

        // Simulate stopping Voltage Source (5s)

        if(device.state_pc_time_ms > 5000L)
        {
            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_POWER_ON, false);
        }
    }
}


// Set digital status registers

static void fgcliteRegSetDigStatus(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.status.state_op == FGC_OP_SIMULATION)
    {
        if(device.vs.sim_intlks == FGC_CTRL_ENABLED)
        {
            // Simulate PC_PERMIT is true always

            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PC_PERMIT, true);

            // Simulate polarity switch signals

//            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_POL_SWI_POS, refMgrVarValue(&device.ref_mgr,POLSWITCH_STATE) == REF_POLSWITCH_POSITIVE);
//            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_POL_SWI_NEG, refMgrVarValue(&device.ref_mgr,POLSWITCH_STATE) == REF_POLSWITCH_NEGATIVE);
        }
        else // Do not simulate interlocks
        {
            // If any faults occurred or FAST_ABORT received, simulate digital interface by resetting VS_RUN and AF_RUN

            if(digitalIOGetStatus(&device.dig, FGC_DIG_STAT_PWR_FAILURE | FGC_DIG_STAT_FAST_ABORT))
            {
                digitalIOSetCmd(&device.dig, FGC_DIG_CMDS_VS_RUN | FGC_DIG_CMDS_AF_RUN, false);
            }
        }

        fgcliteRegSimulateVS(channel);
    }
    else // not in SIMULATION mode
    {
        // Drive real Active Filter command

        fgcliteRegSetAF(channel);
    }

    // Block discharge request signal, except for kA (RQ) and thyristor 2 bridge (RB) converters

    if (!(device.crate_type == FGC_CRATE_TYPE_PC_THYRISTOR2 || device.crate_type == FGC_CRATE_TYPE_PC_KA))
    {
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PC_DISCH_RQ, false);
    }
}



// Check the operational health of the system and set faults/warnings

static void fgcliteRegStatusCheck(uint32_t channel)
{
    // VS.STATE invalid timeout before setting a fault. In FGC2, this is set to 100 and is incremented every 5ms = 500ms.
    // In FGClite, we only increment once every 20ms cycle, so it is set to 25.

    const uint16_t VS_STATE_INVALID_TO = FGCD_CYCLES_PER_SEC / 2;

    struct Equipdev_channel &device = equipdev.device[channel];

     // During a VS reset, the VS_FAULT/VS_EXTINTLK/FAST_ABORT latch is disabled

    if(digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_VS_RESET)
    {
        setStatusBit  (&device.status.st_faults, FGC_FLT_VS_FAULT,      digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_FAULT));
        setStatusBit  (&device.status.st_faults, FGC_FLT_VS_EXTINTLOCK, digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_EXTINTLK));
        setStatusBit  (&device.status.st_faults, FGC_FLT_FAST_ABORT,    digitalIOGetStatus(&device.dig, FGC_DIG_STAT_FAST_ABORT));
    }
    else
    {
        latchStatusBit(&device.status.st_faults, FGC_FLT_VS_FAULT,      digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_FAULT));
        latchStatusBit(&device.status.st_faults, FGC_FLT_VS_EXTINTLOCK, digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_EXTINTLK));
        latchStatusBit(&device.status.st_faults, FGC_FLT_FAST_ABORT,    digitalIOGetStatus(&device.dig, FGC_DIG_STAT_FAST_ABORT));
    }

    // VS State invalid fault test

    if(device.status.state_vs == FGC_VS_INVALID && !getStatusBit(device.status.st_faults, FGC_FLT_VS_STATE))
    {
        device.vs.state_counter++;

        if(digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_RUN) && device.vs.state_counter >= VS_STATE_INVALID_TO)
        {
            logPrintf(channel, "FGCLITE_REG VS_STATE invalid (for %d consecutive cycles)\n", device.vs.state_counter);
            latchStatusBit(&device.status.st_faults, FGC_FLT_VS_STATE, true);
        }
    }
    else
    {
        device.vs.state_counter = 0;
    }

    // If the DAC calibration fails, set the latched flag. This should generate a fault.

    latchStatusBit(&device.status.st_latched, FGC_LAT_DAC_FLT, device.dac.cal_fault_bit_mask);
    latchStatusBit(&device.status.st_faults, FGC_FLT_FGC_HW , getStatusBit(device.status.st_latched, FGC_LAT_DAC_FLT));

    // Powering failure fault coming from the FGClite HW (i.e. CMD_7 (PWR_FAILURE) not sent by the FGClite SW)

    if(device.fgcd_device->online &&
       (digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_FGC_OK) &&
       digitalIOGetStatus(&device.dig, FGC_DIG_STAT_PWR_FAILURE))
    {
        latchStatusBit(&device.status.st_faults, FGC_FLT_COMMS, true);
    }

    // Warnings

    setStatusBit(&device.status.st_warnings, FGC_WRN_FGC_HW,
        getStatusBit(device.status.st_latched, FGC_LAT_DIM_SYNC_FLT | FGC_LAT_DIMS_EXP_FLT | FGC_LAT_DIM_TRIG_FLT | FGC_LAT_DALLAS_FLT | FGC_LAT_ANA_FLT | FGC_LAT_ID_FLT | FGC_LAT_MEM_FLT));

    setStatusBit(&device.status.st_warnings, FGC_WRN_FGC_PSU,
        getStatusBit(device.status.st_latched, FGC_LAT_FGC_PSU_FAIL | FGC_LAT_PSU_V_FAIL | FGC_LAT_VDC_FAIL));

    setStatusBit(&device.status.st_warnings, FGC_WRN_DCCT_PSU,
        getStatusBit(device.status.st_latched, FGC_LAT_DCCT_PSU_FAIL | FGC_LAT_DCCT_PSU_STOP));

    setStatusBit(&device.status.st_warnings, FGC_WRN_CONFIG,         device.config_state == FGC_CFG_STATE_UNSYNCED   ||
                                                                     device.config_mode  == FGC_CFG_MODE_SYNC_FAILED ||
                                                                     device.config_mode  == FGC_CFG_MODE_SYNC_DB_CAL);

    setStatusBit(&device.status.st_warnings, FGC_WRN_CURRENT_LEAD,
        (   (device.meas.u_leads_ptr[U_LEAD_POS] != NULL && fabsf(*device.meas.u_leads_ptr[U_LEAD_POS]) > ULEAD_WARN_SET_LIMIT)
         || (device.meas.u_leads_ptr[U_LEAD_NEG] != NULL && fabsf(*device.meas.u_leads_ptr[U_LEAD_NEG]) > ULEAD_WARN_SET_LIMIT)));

    // Unlatched status

    setStatusBit(&device.status.st_unlatched, FGC_UNL_NOMINAL_LOAD,  device.status.state_op == FGC_OP_NORMAL     &&
                                                                     device.cal.active      == FGC_CTRL_DISABLED &&
                                                                     regMgrParValue( &device.reg_mgr, LOAD_SELECT) == FGC_LOAD_MAGNET);

    setStatusBit(&device.status.st_unlatched, FGC_UNL_SYNC_PLEASE,   device.config_mode == FGC_CFG_MODE_SYNC_DB     ||
                                                                     device.config_mode == FGC_CFG_MODE_SYNC_DB_CAL ||
                                                                     device.config_mode == FGC_CFG_MODE_SYNC_FGC);

    setStatusBit(&device.status.st_unlatched, FGC_UNL_VS_POWER_ON,   digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_POWER_ON));

    setStatusBit(&device.status.st_unlatched, FGC_UNL_PWR_FAILURE,   digitalIOGetStatus(&device.dig, FGC_DIG_STAT_PWR_FAILURE));

    setStatusBit(&device.status.st_unlatched, FGC_UNL_PC_DISCH_RQ,   digitalIOGetStatus(&device.dig, FGC_DIG_STAT_PC_DISCH_RQ));

    setStatusBit(&device.status.st_unlatched, FGC_UNL_PC_PERMIT,     equipdevIsPcPermit(channel));

    // If mask_NO_VS_CABLE is set, force FGC_UNL_NO_VS_CABLE cleared. This is a work-around for issue EPCCCS-8395.
    bool vs_no_cable = digitalIOGetStatus(&device.dig, FGC_DIG_STAT_VS_NO_CABLE) && !device.mask_NO_VS_CABLE;
    setStatusBit(&device.status.st_unlatched, FGC_UNL_NO_VS_CABLE, vs_no_cable);

    setStatusBit(&device.status.st_unlatched, FGC_UNL_LOW_CURRENT,   device.reg_mgr.i.lim_meas.flags.low);

    setStatusBit(&device.status.st_unlatched, FGC_UNL_REF_RT_ACTIVE, refMgrParValue(&device.ref_mgr, MODE_RT_REF) == FGC_CTRL_ENABLED);

    // Set LOG.PM.STATE
    // FGClite logs data regardless of whether the converter is on, so we only implement two logging states: FROZEN and RUNNING

    if(ccLogPMIsFrozen(channel))
    {
        device.log.pm_state = FGC_LOG_FROZEN;
    }
    else
    {
        device.log.pm_state = FGC_LOG_RUNNING;
    }
}



// Latch STATUS.ST_FAULTS and STATUS.ST_WARNINGS status bits from the CCLIBS SELECT status

static void fgcliteRegSetSelectStatus(uint32_t channel)
{
    struct Equipdev_channel &device   = equipdev.device[channel];
    struct SIG_select       &select   = device.sig_struct.select.named.i_meas;

    const  uint32_t         &warnings = select.meas.status.warnings;

    // Ignore CCLIBS I_MEAS faults and warnings in SIMULATION mode

    if(device.status.state_op == FGC_OP_SIMULATION) return;

    // I_MEAS_DIFF unlatched. Note that it is latched because the bit is cleared every 20ms in fgcliteRegDevicePresent()

    latchStatusBit(&device.status.st_unlatched, FGC_UNL_I_MEAS_DIFF, warnings & SIG_SELECT_DIFF_WARN_BIT_MASK);

    // I_MEAS warning

    setStatusBit(&device.status.st_warnings, FGC_WRN_I_MEAS, warnings);

    // IN_USE bit

    switch(select.actual_selector)
    {
        case SIG_A:
            device.status.st_meas_a |= FGC_MEAS_IN_USE;
            break;
        case SIG_B:
            device.status.st_meas_b |= FGC_MEAS_IN_USE;
            break;
        case SIG_AB:
            device.status.st_meas_a |= FGC_MEAS_IN_USE;
            device.status.st_meas_b |= FGC_MEAS_IN_USE;
            break;
        default:
            break;
    }
}



// Update DCCT.{A,B}.STATUS from the CCLIBS status

static void fgcliteRegSetDcctStatus(uint32_t channel, uint8_t dcct_channel, uint8_t *dcct_status, uint16_t *adc_status)
{
    struct Equipdev_channel &device   = equipdev.device[channel];
    struct SIG_transducer   &dcct     = device.sig_struct.transducer.array[dcct_channel];
    const uint32_t          &faults   = dcct.meas.status.faults;
    const uint32_t          &warnings = dcct.meas.status.warnings;

    // Note: the following DCCT status bits appear in the symbol list but are not implemented in FGClite:
    //
    // MEAS_OK    Not implemented in class 51 or in class 92
    // ZERO_I     Not present in 60A/120A converters; 600A converters require implementation of Hitec 600A DIM
    // NO_HEAD    Not present in 60A/120A converters; 600A converters require implementation of Hitec 600A DIM
    // MCB_OFF    Not present in 60A/120A/600A converters
    // ID_FLT     Not implemented in class 51, 63, and 92

    if(dcct.fault_flag || faults)
    {
        *dcct_status = FGC_DCCT_FAULT;
    }
    else
    {
        // If there is no fault, then the current measurement should be OK

        *adc_status = FGC_MEAS_I_MEAS_OK;
    }

    // TEMP_FLT: There is no temperature fault from transducers in CCLIBS, only a warning. So we use
    //           the warning to set the fault bit.

    if(warnings & SIG_TRANSDUCER_TEMP_WARN_BIT_MASK)       *dcct_status |= FGC_DCCT_TEMP_FLT;

    // CAL_FLT

    if(faults & SIG_TRANSDUCER_CAL_FLT_BIT_MASK)           *dcct_status |= FGC_DCCT_CAL_FLT;

    // If there are no faults then the status should be ok

    if(!faults)
    {
        *dcct_status |= FGC_DCCT_MEAS_OK;
    }
}



// Update MEAS.STATUS.{A,B} from the CCLIBS ADC status

static void fgcliteRegSetAdcStatus(uint32_t channel, uint8_t adc_channel, uint16_t *adc_status)
{
    struct Equipdev_channel &device   = equipdev.device[channel];
    struct SIG_adc          &adc      = device.sig_struct.adc.array[adc_channel];
    const uint32_t          &faults   = adc.meas.status.faults;
    const uint32_t          &warnings = adc.meas.status.warnings;

    // I_MEAS_OK

    if(!faults)
    {
        *adc_status |= FGC_MEAS_V_MEAS_OK;
    }

    // SIGNAL_STUCK

    if(faults & SIG_ADC_STUCK_FLT_BIT_MASK)
    {
        *adc_status |= FGC_MEAS_SIGNAL_STUCK;
    }

    // Calibration faults and warnings only affect ADC A and B

    if((adc_channel == 0 || adc_channel == 1))
    {
        // CAL_ACTIVE

        if(ccSigCalChannelIsCalibrating(channel, adc_channel == 0 ? CAL_ADC_A : CAL_ADC_B))
        {
            *adc_status |= FGC_MEAS_CAL_ACTIVE;
        }

        // CAL_FAILED

        if(faults & SIG_ADC_CAL_FLT_BIT_MASK)
        {
            *adc_status |= FGC_MEAS_CAL_FAILED;
        }
    }

    // CAL_WARN

    if(warnings & SIG_ADC_CAL_WARN_BIT_MASK)
    {
        *adc_status |= FGC_MEAS_CAL_WARN;
    }

    // ADC_TEMP_WARN

    if(warnings & SIG_ADC_TEMP_WARN_BIT_MASK)
    {
        *adc_status |= FGC_MEAS_ADC_TEMP_WARN;
    }
}


// Latch CCLIBS warning and faults

static void fgcliteRegCclibs(uint32_t channel)
{
    struct Equipdev_channel &device       = equipdev.device[channel];
    uint16_t                *st_warnings  = &device.status.st_warnings;
    uint16_t                *st_faults    = &device.status.st_faults;
    uint32_t                 ref_warnings = refMgrVarValue(&device.ref_mgr,REF_WARNINGS);
    uint32_t                 ref_faults   = refMgrVarValue(&device.ref_mgr,REF_FAULTS_IN_OFF);

    // Update STAUTS.WARNINGS derived from CCLIBS

    setStatusBit(st_warnings, FGC_WRN_REG_ERROR,    ref_warnings & (REF_I_ERR_WARNING_BIT_MASK | REF_I_RST_WARNING_BIT_MASK));

    setStatusBit(st_warnings, FGC_WRN_REF_LIM,      ref_warnings & REF_REF_CLIPPED_BIT_MASK);

    setStatusBit(st_warnings, FGC_WRN_REF_RATE_LIM, ref_warnings & REF_REF_RATE_CLIPPED_BIT_MASK);

    setStatusBit(st_warnings, FGC_WRN_V_ERROR,      ref_warnings & REF_V_ERR_WARNING_BIT_MASK);

    // Update STAUTS.FAULTS derived from CCLIBS

    latchStatusBit(st_faults, FGC_FLT_I_MEAS,       ref_faults & REF_I_MEAS_FAULT_BIT_MASK);

    latchStatusBit(st_faults, FGC_FLT_REG_ERROR,    ref_faults & (REF_I_ERR_FAULT_BIT_MASK | REF_I_RST_FAULT_BIT_MASK));

//    latchStatusBit(st_faults, FGC_FLT_POL_SWITCH,   ref_faults & REF_POLSWITCH_FAULT_BIT_MASK);

    latchStatusBit(st_faults, FGC_FLT_LIMITS,      (ref_faults & REF_I_MEAS_TRIP_BIT_MASK) && (device.cal.active == FGC_CTRL_DISABLED));

    latchStatusBit(st_faults, FGC_FLT_V_ERROR,      ref_faults & REF_V_ERR_FAULT_BIT_MASK);
}



// Set the digital command register bits in CONVERTER_CMD for this cycle

static void fgcliteRegSetConv(uint32_t channel, struct FGClite_command *conv_cmd)
{
    struct Equipdev_channel &device  = equipdev.device[channel];

    uint16_t command;

    // Set V_REF and CONVERTER_CMD

    switch(device.status.state_op)
    {
        case FGC_OP_UNCONFIGURED:        // Don't set the DAC

            device.ref.dac_val = 0;

            // The PC should always be off in UNCONFIGURED mode. Set the POWERING_FAILURE interlock and force VS_RUN to OFF.

            digitalIOSetCmd(&device.dig, FGC_DIG_CMDS_FGC_OK, false);

            command = digitalIOGetCmd(&device.dig);
            break;

        case FGC_OP_TEST:                // Set the DAC directly. See REF.DAC property.

            if(device.ref.dac > 1000000)
            {
                // If REF.DAC is > 1000000 then increment the DAC value by (REF.DAC - 1000000) - this allows a up-going ramp on the DAC output

                device.ref.dac_val += device.ref.dac - 1000000;
            }
            else if(device.ref.dac < -1000000)
            {
                // If REF.DAC is < -1000000 then decrement the DAC value by (-1000000 - REF.DAC) - this allows a down-going ramp on the DAC output

                device.ref.dac_val -= -1000000 - device.ref.dac;
            }
            else
            {
                // REF.DAC is in the range from -1000000 to +1000000 so set the dac directly with this value

                device.ref.dac_val = device.ref.dac;
            }

            // Set the digital interface directly

            command = digitalIOGetCmd(&device.dig);

            // Loop the DAC back to the ADCs

            command |= CAL_SOURCE_DAC << 14 | CAL_IA_EN | CAL_IB_EN | CAL_VS_EN;

            break;

        case FGC_OP_SIMULATION:             // Set DAC using libref value
        case FGC_OP_NORMAL:

            device.ref.dac_val = sigDacRT(&device.dac, refMgrVarValue(&device.ref_mgr, REF_DAC), NULL);

            // Set FGC_OK according to the fault status and mask COMMS, PC_PERMIT and FAST_ABORT faults

            digitalIOSetCmd(&device.dig, FGC_DIG_CMDS_FGC_OK,
                            (digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_VS_RESET) ||
                            ((device.status.st_faults & ~(FGC_FLT_COMMS | FGC_FLT_NO_PC_PERMIT | FGC_FLT_FAST_ABORT)) == 0));

            command = digitalIOGetCmd(&device.dig);

            break;

        case FGC_OP_CALIBRATING:         // Set the DAC to the calibration value

            device.ref.dac_val = ccSigCalGetRawValue(channel);

            // Set FGC_OK according to the fault status and mask COMMS, PC_PERMIT and FAST_ABORT faults

            digitalIOSetCmd(&device.dig, FGC_DIG_CMDS_FGC_OK,
                           (digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_VS_RESET) ||
                           ((device.status.st_faults & ~(FGC_FLT_COMMS | FGC_FLT_NO_PC_PERMIT | FGC_FLT_FAST_ABORT)) == 0));

            command = digitalIOGetCmd(&device.dig);

            // Set the current calibration channel(s) and level

            command |= ccSigCalGetCmdBits(channel);
            break;

        default:                         // STATE.OP is invalid, we should never arrive here

            latchStatusBit(&device.status.st_faults, FGC_FLT_FGC_STATE, true);
            return;
    }

    // Reset VSRUN except in NORMAL operational state

    if(device.status.state_op != FGC_OP_NORMAL)
    {
        command &= ~FGC_DIG_CMDS_VS_RUN;
    }

    // Toggle FGC_OK command bit, but FGClite treats it as an FGC_FLT command

    command ^= FGC_DIG_CMDS_FGC_OK;

    // Enable the ADC channels, unless ADC RESET is active (ADC_RESET is active low)

    if(device.adc.reset_counter != 0)
    {
        --device.adc.reset_counter;

        device.adc.filter_state = FGC_FILTER_STATE_RESETTING;
    }
    else
    {
        command |= ADC_IA_RESET | ADC_IB_RESET | ADC_VS_RESET;

        device.adc.filter_state = FGC_FILTER_STATE_READY;
    }

    // Set the DAC

    fgcliteCmdSetVRef(conv_cmd, channel, device.ref.dac_val);

    // Set CONVERTER_CMD

    fgcliteCmdResetConv(conv_cmd, channel);
    fgcliteCmdSetConv(conv_cmd, channel, command);
}



// External functions

void fgcliteRegResetDevice(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set the device start time

    timingGetUserTime(0, &device.device.run_time);

    // Reset MS_PERIOD_OVERRIDE

    device.fgc.ms_period_override = 0;

    // Reset the serial buffers

    serialReset(channel);

    // Reset the calibration status

    ccSigCalReset(channel);

    // Reset the DIM logging state to INACTIVE for all DIMs

    dimStateResetActiveList(channel);

    // Reset the VS DIM to zero

    device.diag.vs_dim_idx = 0;

    // Reset the Paged Memory Manager and initiate a scan of the Dallas bus

    pageMgrReset(channel, &device.device.run_time);

    // Initialise the crate type and side as unknown

    device.crate_type     = 0x1F;
    device.crate_position = '?';

    // Set default requested operational state to NORMAL (it is non-volatile, so normally it will be set from NVS)

    device.mode_op = FGC_MODE_OP_NORMAL;

    // Set the Power Converter mode and state. The state stays at FAULT_OFF while the device is unconfigured.

    device.mode_pc  = FGC_PC_OFF;
    refMgrParValue(&device.ref_mgr, MODE_REF) = REF_OFF;

    statePcInit(channel);
    stateOpInit(channel);

    // Reset the voltage source

    digitalIORequestCmd(&device.dig, DIG_IO_VS_RESET);

    // Start with a PC state fault until the device is configured

    latchStatusBit(&device.status.st_faults, FGC_FLT_FGC_STATE, true);

    // Reset the latched status

    setStatusBit(&device.status.st_latched, 0xFFFF, false);

    // Request the device config to be reloaded from non-volatile storage

    device.config_state = FGC_CFG_STATE_UNSYNCED;
    device.config_mode  = FGC_CFG_MODE_SYNC_NONE;
    device.config_read  = true;                     // Tell equipdevRun() to read config from NVS files
}



void fgcliteRegPre(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // If ADC.INHIBIT_CAL is set, decrease inhibit calibration counter once per second

    if(device.adc.inhibit_cal > 0 && device.state_pc_time_ms % 1000 == 0)
    {
        --device.adc.inhibit_cal;
    }

    // Set the real-time reference

    if(fieldbus.real_time_zero == FGC_CTRL_ENABLED || isnan(rt.used_data.channels[channel-1]))
    {
        *refMgrParPointer(&device.ref_mgr, RT_REF) = 0;
    }
    else
    {
        *refMgrParPointer(&device.ref_mgr, RT_REF) = rt.used_data.channels[channel-1];
    }

    // Update the operational state machine

    stateOpRT(channel);
}



void fgcliteRegPost(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{
    struct CC_us_time        cycle_time;           // CTRI time for this cycle
    bool                     is_200ms_boundary;    // True if CTRI time is divisible by  200ms
    bool                     is_1s_boundary;       // True if CTRI time is divisible by 1000ms

    struct Equipdev_channel &device                = equipdev.device[channel];

    // If the device is unconfigured, we can't run the regulation code. Just perform some checks and set CONV_CMD.

    if(device.status.state_op == FGC_OP_UNCONFIGURED)
    {
        // Set system faults and warnings

        fgcliteRegStatusCheck(channel);

        // Set the CONVERTER_CMD for this cycle

        fgcliteRegSetConv(channel, conv_cmd);

        return;
    }

    // Set digital statuses and interlocks

    fgcliteRegSetDigStatus(channel);

    // Set system faults and warnings

    fgcliteRegStatusCheck(channel);

    // Update the Polarity Switch signals

//    fgcliteRegPolSwitch(channel);

    // Get the CTRI timestamp for this FIP cycle

    fgcliteRegGetTimestamp(&cycle_time, &is_200ms_boundary, &is_1s_boundary);

    // Update the STATE.VS and libref PC_STATE and REF_MODE as a function of PC_PERMIT

    modePcUpdate(channel);

    // Regulate the device : we use the 10ms averages of the measurements so we iterate twice

    for(uint32_t half_cycle = 0; half_cycle < 2; ++half_cycle)
    {
        struct CC_ns_time const cycle_ns_time = cctimeUsToNsRT(cycle_time);

        // Prepare the ADC channels

        // Pass the raw measurements into the libsig structures

        device.sig_struct.adc.named.adc_a.raw = device.meas.raw[half_cycle].i_a;
        device.sig_struct.adc.named.adc_b.raw = device.meas.raw[half_cycle].i_b;
        device.sig_struct.adc.named.adc_c.raw = device.meas.raw[half_cycle].v_meas;

        // Convert raw measurements. Restart the averaging filter on 200ms boundaries.

        sigMgrRT(&device.sig_struct.mgr, is_200ms_boundary, cycle_ns_time);

        // Background processing of all temperature filters, ADCs and their associated transducers

        if(is_200ms_boundary)
        {
            sigMgr(&device.sig_struct.mgr, is_1s_boundary);
        }

        // Pass current and voltage measurements into libreg

        regMgrMeasSetImeasRT(&device.reg_mgr, device.sig_struct.select.named.i_meas.meas.unfiltered);
        regMgrMeasSetVmeasRT(&device.reg_mgr, device.sig_struct.transducer.named.v_meas.meas.unfiltered);

        // Regulate and update the reference state machine

        refRtRegulationRT(&device.ref_mgr, cycle_ns_time);

        refRtStateRT(&device.ref_mgr);

        // Convert the raw values to floats for use by liblog

        device.meas.raw_f.i_a    = (cc_float)device.meas.raw[half_cycle].i_a;
        device.meas.raw_f.i_b    = (cc_float)device.meas.raw[half_cycle].i_b;
        device.meas.raw_f.v_meas = (cc_float)device.meas.raw[half_cycle].v_meas;

        // Log the reference and measurement data for this iteration

        ccLogIteration(channel, &cycle_time);

        // increment the timestamp by 10ms and reset the 200ms and 1s boundary flags

        fgcliteRegCycleIncTimestamp(&cycle_time, &is_200ms_boundary, &is_1s_boundary);

        // Update STATUS.ST_FAULTS and STATUS.ST_WARNINGS from the CCLIBS Select status

        fgcliteRegSetSelectStatus(channel);

        // Update DCCT.{A,B}.STATUS from the CCLIBS DCCT statuses

        fgcliteRegSetDcctStatus(channel, 0, &device.status.st_dcct_a, &device.status.st_meas_a);
        fgcliteRegSetDcctStatus(channel, 1, &device.status.st_dcct_b, &device.status.st_meas_b);

        // Update MEAS.STATUS.{A,B} from the CCLIBS ADC statuses

        fgcliteRegSetAdcStatus(channel, 0, &device.status.st_meas_a);
        fgcliteRegSetAdcStatus(channel, 1, &device.status.st_meas_b);
        fgcliteRegSetAdcStatus(channel, 2, &device.adc.st_meas_c);
    }

    // Update the Power Converter state machine

    statePcRT(channel);

    // Update REF.REMAINING property

    if(device.status.state_pc == FGC_PC_RUNNING)
    {
        float time_remaining = device.ref_mgr.ref_armed->fg_pars.meta.time.end - device.ref_mgr.fg.time_fp32;

        device.ref.remaining = time_remaining > 0.0
                             ? time_remaining
                             : 0.0;
    }
    else
    {
        device.ref.remaining = device.ref_mgr.ref_armed->fg_pars.meta.time.end;
    }

    // Update the commands on the digital interface for VS_RUN and VS_RESET

    digitalIOProcessRequests(&device.dig);

    // Update the commands on the digital interface for the polarity switch

//    digitalIOPolSwitch(&device.dig, refMgrVarValue(&device.ref_mgr,POLSWITCH_STATE), refMgrVarValue(&device.ref_mgr,POLSWITCH_REQ_STATE));

    // Set CCLIBS derived warnings and faults

    fgcliteRegCclibs(channel);

    // Set the CONVERTER_CMD for this cycle

    fgcliteRegSetConv(channel, conv_cmd);

    // Update the published data for this device

    // Convert MEAS.I.ERR_MA from from A to mA and cast to a signed 16-bit integer

    device.status.i_err_ma  = regMgrVarValue(&device.reg_mgr, REG_MILLI_REG_ERR);

    // Update MEAS.I.DIFF_MA

    device.status.i_diff_ma = sigVarValue(&device.sig_struct, select, i_meas, SELECT_MILLI_ABS_DIFF);

    // Update I_REF, I_MEAS, V_REF, V_MEAS from libreg

    device.status.i_ref     = device.reg_mgr.i.ref_limited;
    device.status.i_meas    = device.reg_mgr.i.meas.signal[REG_MEAS_EXTRAPOLATED];
    device.status.v_ref     = device.reg_mgr.v.ref;
    device.status.v_meas    = device.reg_mgr.v.unfiltered;

    // Update Event log

    ccEvtlogStoreProperties(channel);

    // LOG raw, volds, and amps values if there is a I_MEAS warning or fault (see APS-6746/EPCCCE-1095)

    fgliteRegLogOnIMeas(channel);
}



static void fgcliteRegSeuUpdate(uint32_t channel, const struct FGClite_status *status)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Return immediately if the device is offline

    if(!device.fgcd_device->online)
    {
        return;
    }

    // If the array have been zeroed, then the last entry should be zero

    if(device.fgc.seu.n_elements &&
       (!device.fgc.seu.type[device.fgc.seu.n_elements-1] || !device.fgc.seu.tv_sec[device.fgc.seu.n_elements-1]))
    {
        memset(&device.fgc.seu,0,sizeof(device.fgc.seu));
    }

    // If there is a SEU and the array is not full, then add a SEU event

    if(status->critical.seu_count && device.fgc.seu.n_elements < FGC_MAX_SEU_EVENTS)
    {
        struct timeval user_time;

        timingGetUserTime(0, &user_time);

        device.fgc.seu.type[device.fgc.seu.n_elements]   = status->critical.seu_count;
        device.fgc.seu.tv_sec[device.fgc.seu.n_elements] = static_cast<uint32_t>(user_time.tv_sec);

        ++device.fgc.seu.n_elements;
    }
}



void fgcliteRegDevicePresent(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize DCCT status. They are unlatched and set in fgcliteRegSetDcctStatus()

    device.status.st_dcct_a = 0;
    device.status.st_dcct_b = 0;

    // Initialize ADC status. They are unlatched and set in fgcliteRegSetAdcStatus()

    device.status.st_meas_a = 0;
    device.status.st_meas_b = 0;
    device.adc.st_meas_c    = 0;

    // Initialize I_MEAS warning and I_MEAS_DIFF unlatched status

    setStatusBit(&device.status.st_unlatched, FGC_UNL_I_MEAS_DIFF, 0);

    // Get the 10ms sum for each of the ADC channels

    device.meas.raw[0].i_a    = status->critical.i_a_0_9;
    device.meas.raw[1].i_a    = status->critical.i_a_10_19;
    device.meas.raw[0].i_b    = status->critical.i_b_0_9;
    device.meas.raw[1].i_b    = status->critical.i_b_10_19;
    device.meas.raw[0].v_meas = status->critical.v_meas_0_9;
    device.meas.raw[1].v_meas = status->critical.v_meas_10_19;

    // Remove the ADC fault flag by default (and check later)

    device.sig_struct.adc.named.adc_a.fault_flag  = false;
    device.sig_struct.adc.named.adc_b.fault_flag  = false;
    device.sig_struct.adc.named.adc_c.fault_flag  = false;

    // Read the digital status registers from the critical data

    if(device.status.state_op == FGC_OP_SIMULATION && device.vs.sim_intlks == FGC_CTRL_ENABLED)
    {
        // Reset most of the status bits - preserve VS_RUN, VS_POWER_ON, VS_READY, POL_SWI_POS and POL_SWI_NEG

        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_FAULT    | FGC_DIG_STAT_VS_EXTINTLK | FGC_DIG_STAT_FAST_ABORT |
                                        FGC_DIG_STAT_PC_DISCH_RQ | FGC_DIG_STAT_VS_NO_CABLE | FGC_DIG_STAT_DCCT_A_FLT |
                                        FGC_DIG_STAT_DCCT_B_FLT  | FGC_DIG_STAT_PC_PERMIT   | FGC_DIG_STAT_PWR_FAILURE, false);

        device.sig_struct.transducer.named.dcct_a.fault_flag = false;
        device.sig_struct.transducer.named.dcct_b.fault_flag = false;
    }
    else
    {
        // Status registers that we read in Mode 1 (NORMAL) & Mode 2 (SIMULATION with VS.SIM_INTLKS disabled)

        // Check the SEFI bits and set the ADC status accordingly

        if ((status->critical.controller_status & ADC_IA_M0_SEFI_DET_TEST) ||
            (status->critical.controller_status & ADC_IA_M1_SEFI_DET_TEST))
        {
            device.status.st_meas_a                      |= FGC_MEAS_SIGNAL_STUCK;
            device.sig_struct.adc.named.adc_a.fault_flag  = true;
        }

        if ((status->critical.controller_status & ADC_IB_M0_SEFI_DET_TEST) ||
            (status->critical.controller_status & ADC_IB_M1_SEFI_DET_TEST))
        {
            device.status.st_meas_b                      |= FGC_MEAS_SIGNAL_STUCK;
            device.sig_struct.adc.named.adc_b.fault_flag  = true;
        }

        if ((status->critical.controller_status & ADC_VS_M0_SEFI_DET_TEST) ||
            (status->critical.controller_status & ADC_VS_M1_SEFI_DET_TEST))
        {
            device.adc.st_meas_c                         |= FGC_MEAS_SIGNAL_STUCK;
            device.sig_struct.adc.named.adc_c.fault_flag  = true;
        }

        // Set the Transducer statuses.
        //
        // DCCT_A and DCCT_B statuses are set from the status bits in the critical data. We have no way to detect the status
        // of the voltage probe (V_MEAS transducer), so its fault flag is always FALSE.
        // The fault flag is latched, and it is reset after PC OFF

        device.sig_struct.transducer.named.dcct_a.fault_flag |= digitalIOGetStatus(&device.dig, FGC_DIG_STAT_DCCT_A_FLT);
        device.sig_struct.transducer.named.dcct_b.fault_flag |= digitalIOGetStatus(&device.dig, FGC_DIG_STAT_DCCT_B_FLT);

        // Bits of CONVERTER_INPUT which map to bits of DIG.STATUS

        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_FAULT,        status->critical.converter_input & VS_FAULT);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_EXTINTLK,     status->critical.converter_input & VS_EXTINTLK);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_FAST_ABORT,      status->critical.converter_input & FASTPA_MEM);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PC_DISCH_RQ,     status->critical.converter_input & SW_OP_REQ);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_NO_CABLE,     status->critical.converter_input & VS_NO_CABLE);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_DCCT_A_FLT,      status->critical.converter_input & DCCT_A_FLT);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_DCCT_B_FLT,      status->critical.converter_input & DCCT_B_FLT);

        // Polarity switch is not used in FGClite. Mask the flags so they do not spuriously appear (EPCCCS-8364).

        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_POL_SWI_POS,     false);
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_POL_SWI_NEG,     false);

        // Retrieve PC_PERMIT from FGClite and mask it use the GW.PC_PERMIT for the 60A
        // This makes equivalent the call to equipdevIsPcPermit() or checking FGC_DIG_STAT_PC_PERMIT

        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PC_PERMIT,       !(status->critical.controller_status & PC_PERMIT));
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PC_PERMIT,       equipdevIsPcPermit(channel));

        // Real interlock statuses from CONTROLLER_STATUS

        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PWR_FAILURE,     status->critical.controller_status & POWERING_FAILURE);

        // Some status registers are read in Mode 1 only

        if(device.status.state_op != FGC_OP_SIMULATION)
        {
            // Bits of CONVERTER_INPUT which map to bits of DIG.STATUS

            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_POWER_ON, status->critical.converter_input & POWER_ON);
            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_READY,    !(status->critical.converter_input & VLOOP_OK_NOT));

            // Read back VS_RUN from CONVERTER_OUTPUT

            digitalIOSetStatus(&device.dig, FGC_DIG_STAT_VS_RUN,      status->critical.converter_output & CMD_0_VS_RUN);
        }
    }

    // Manage the paged memory

    pageMgrUpdate(channel, status, ctrl_cmd);

    // Process serial data

    serialReadCmd(channel, status->critical.serial_data);

    // Check for SEUs

    fgcliteRegSeuUpdate(channel, status);
}



void fgcliteRegDeviceNotPresent(uint32_t channel, const struct FGClite_status *status, struct FGClite_command *sync_cmd, struct FGClite_command *ctrl_cmd, struct FGClite_command *conv_cmd)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set ADC statuses to invalid

    device.sig_struct.adc.named.adc_a.fault_flag = true;
    device.sig_struct.adc.named.adc_b.fault_flag = true;
    device.sig_struct.adc.named.adc_c.fault_flag = true;

    // Force PC_PERMIT status on, as we don't want to abort when we miss a cycle

    if(device.status.state_op != FGC_OP_SIMULATION || device.vs.sim_intlks == FGC_CTRL_DISABLED)
    {
        digitalIOSetStatus(&device.dig, FGC_DIG_STAT_PC_PERMIT, true);
    }

    // Inform the paged memory manager that we missed a page acquisition

    pageMgrInvalidate(channel);

    // Anything else to do here?
}

// EOF
