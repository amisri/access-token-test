/*!
 * @file   ana_log.cpp
 * @brief  FGClite analogue logging functions
 * @author Michael Davis
 */

//#define DEBUG

#include <bitset>
#include <logging.h>
#include <equipdev.h>
#include <onewire.h>
#include <equip_liblog.h>
#include <dim_state.h>
#include <ana_log.h>
#include <log.h>



// Constants

const uint32_t ADC_PAGES_PER_CHANNEL = 381;         // 381 pages x 21 samples per channel = 8001 samples @ 1000 Hz = 8s of ADC data
const uint32_t DIM_PAGES_PER_CHANNEL = 63;          // 63 pages x 8 samples per channel = 504 samples @ 50 Hz = 10s of DIM data

// Bitmask of which ADC channels to scan. Bit 2 = ADC V_MEAS log. Bit 3 = ADC I_A log. Bit 4 = ADC I_B log
// (see FGClite Fieldbus Specification Section 3.2). To log all 3 channels, the bitmask should be 11100b = 0x1C.

const uint16_t  ADC_CHANNEL_BITMASK   = 0x1C;
const uint32_t  ADC_NUM_CHANNELS      = 3;



// Types

/*
 * The type of analogue page data we are currently requesting
 */

enum Page_type
{
    ADC_LOG_PAGE,                                   // Log ADC channels
    DIM_LOG_PAGE                                    // Log DIM analogue channels
};



/*
 * Analogue log management structure
 */

struct Ana_log
{
    uint16_t           adc_log_index;               // Current index to read ADC pages
    bool               is_adc_log_frozen;           // Flag to log ADC pages or not

    uint32_t           num_dim_channels;            // Count of bits set in initial dim_mask

    enum Page_type     ana_page_type;               // Type of analogue pages we are requesting (ADC or DIM)
    uint32_t           ana_page_count;              // Count of how many pages of the current type have been requested

    uint32_t           adc_pages;                   // Number of ADC pages to read each 500 cycles
    uint32_t           dim_pages;                   // Number of DIM pages to read each 500 cycles
    uint32_t           adc_page_count;              // Number of ADC pages left to read
    uint32_t           dim_page_count;              // Number of DIM pages left to read

    uint8_t            curr_adc_channel;            // The ADC channel we are reading this cycle
    uint8_t            curr_dim_channel;            // The DIM channel we are reading this cycle

    bool               is_data_pending;             // Did we send a page request last cycle?
    enum Page_type     requested_page_type;         // Type of page requested in the last cycle
    uint8_t            requested_channel;           // Channel no. for page requested in the last cycle
    uint16_t           requested_index;             // Log index for page requested in the last cycle
};



// Global variables, zero initialised

struct Ana_log ana_log[FGCD_MAX_DEVS] = {};



// Static functions

/*
 * Search for the next active channel
 *
 * If all DIMs have triggered and have finished logging, it is possible that no active channel is
 * available. In this case, the *curr_channel will be set to 16, which is not a valid channel number.
 *
 * @param[in,out]    curr_channel    The current channel to log
 * @param[in]        mask            Bitmask of all active channels
 *
 * @retval           true            The new channel is the first channel in order
 * @retval           false           The new channel is not the first channel in order
 */

bool anaLogNextChannel(uint8_t *curr_channel, uint16_t mask)
{
    // Scan from curr_channel+1 to 15

    for(++(*curr_channel); *curr_channel < 16; ++(*curr_channel))
    {
        if((1 << *curr_channel) & mask) return false;
    }

    // No channel found, restart scan from 0

    for(*curr_channel = 0; *curr_channel < 16; ++(*curr_channel))
    {
        if((1 << *curr_channel) & mask) break;
    }

    return true;
}



/*
 * Request the next ADC analogue page
 */

static void anaLogRequestAdc(uint32_t channel, struct FGClite_command *ctrl_cmd)
{
    struct Ana_log &ana = ana_log[channel];

    // Request the next page

    fgcliteCmdRequestCtrl(ctrl_cmd, channel, static_cast<enum Ctrl_cmd_index_type>(ana.curr_adc_channel), ana.adc_log_index);

    ana.is_data_pending     = true;
    ana.requested_page_type = ADC_LOG_PAGE;
    ana.requested_channel   = ana.curr_adc_channel;
    ana.requested_index     = ana.adc_log_index;

    // Increment the channel and log index

    if(anaLogNextChannel(&ana.curr_adc_channel, ADC_CHANNEL_BITMASK))
    {
        ana.adc_log_index += 21;
        ana.adc_log_index %= ADC_BUFFER_SIZE;
    }
}



/*
 * Request the next DIM analogue page
 */

static void anaLogRequestDim(uint32_t channel, struct FGClite_command *ctrl_cmd)
{
    struct Ana_log &ana = ana_log[channel];

    // Increment the DIM channel

    anaLogNextChannel(&ana.curr_dim_channel, dimStateGetActiveList(channel));

    // If there is at least one active DIM channel, request the next page

    if(ana.curr_dim_channel < 16)
    {
        ana.is_data_pending     = true;
        ana.requested_page_type = DIM_LOG_PAGE;
        ana.requested_channel   = ana.curr_dim_channel;
        ana.requested_index     = dimStateRequestPage(channel, ana.curr_dim_channel, ctrl_cmd);
    }
}



// External functions

void anaLogInit(uint32_t channel, uint16_t dim_mask)
{
    struct Ana_log &ana         = ana_log[channel];

    uint32_t       &Da          = ana.num_dim_channels;                   // Number of DIM channels
    uint32_t       &a           = ana.adc_pages;                          // Number of ADC pages to read each 500 cycles
    uint32_t       &d           = ana.dim_pages;                          // Number of DIM pages to read each 500 cycles

    float const     Sa          = ADC_PAGES_PER_CHANNEL * 21;             // 21 ADC samples per page per channel
    float const     Sd          = DIM_PAGES_PER_CHANNEL * 8;              // 8 DIM samples per page per channel
    float const     Sa_max      = ADC_BUFFER_SIZE;                        // Max no. of ADC samples per channel (fixed in FGClite hardware)

    uint32_t        Dd;                                                   // Number of DIM digital pages
    uint32_t        W;                                                    // Number of 1-wire pages containing a temperature sensor
    uint32_t        F500;                                                 // Number of cycles per 500 cycles that are available for acquiring analogue data
    uint32_t        d_max;                                                // Limit on d to optimise the ADC read times
    float           A;                                                    // Number of ADC channels to log (as a float)
    float           Rda_max;                                              // Upper limit of the ratio between digital and analogue page reads

    // Set the number of channels

    A                    = ADC_NUM_CHANNELS;
    Da                   = std::bitset<16>(dim_mask).count();

    // Count the number of DIM pages and 1-wire pages we need to read in each 500 cycles

    Dd = 0;

    for(uint32_t i = 0; i < 4; ++i)
    {
        if((dim_mask >> (4*i)) & 0xF) ++Dd;
    }

    W = onewireTempPageCount(channel);

    // Calculate the ratios between the various types of page requests. See EPCCCS-3898 for an explanation of the maths.

    F500 = 500 - W - (10 * Dd);

    Rda_max = (((21.0 * static_cast<float>(F500) * (Sa_max + Sa)) / (10000.0 * A * Sa)) - 1.0) *
              (160.0 * A) / (21.0 * static_cast<float>(Da));

    d = (Rda_max * static_cast<float>(F500)) / (((160.0 * A) / (21.0 * Da)) + Rda_max);

#ifdef DEBUG
    logPrintf(channel, "anaLogInit: num_adc_channels=%f  Da=%d  Dd=%d  W=%d  F500=%d  Rda_max=%f  d=%d  a=%d\n",
                A, Da, Dd, W, F500, Rda_max, d, F500 - d);
#endif

    d_max = (Sd * Da) / 16.0;

    if(d > d_max)
    {
        d = d_max;
    }

    a = F500 - d;

#ifdef DEBUG
    logPrintf(channel, "anaLogInit: After clipping at d_max=%u  d=%u  a=%u\n", d_max, d, a);
#endif
}



void anaLogResetIndex(uint32_t channel, const struct FGClite_status *status, bool is_adc_log_frozen, int32_t time_offset_ms)
{
#ifdef DEBUG
    logPrintf(channel, "anaLogResetIndex: is_adc_log_frozen=%u  time_offset_ms=%d\n",
              is_adc_log_frozen, time_offset_ms);
#endif

    struct Ana_log &ana = ana_log[channel];

    /*
     * Check if ADC log is frozen. We need to maintain the same cadence for ADC/DIM logging while the
     * ADC log is frozen, otherwise the DIM log can overrun. Even though we don't log any ADC data,
     * we hold off logging DIM data until adc_page_count reaches zero. See also anaLogRequestPage().
     */

    ana.is_adc_log_frozen = is_adc_log_frozen;

    if(is_adc_log_frozen == false)
    {
        ccLogAdcStart(channel, time_offset_ms);
    }

    // Reset ADC logging

    ana.adc_page_count   = ADC_NUM_CHANNELS * ADC_PAGES_PER_CHANNEL;
    ana.curr_adc_channel = 16;

    anaLogNextChannel(&ana.curr_adc_channel, ADC_CHANNEL_BITMASK);

    ana.adc_log_index  = (status->critical.adc_log_index + ADC_BUFFER_SIZE + time_offset_ms) % ADC_BUFFER_SIZE;

    // Start logging the ADC pages

    ana.ana_page_count = (ana.adc_pages < ana.adc_page_count) ? ana.adc_pages : ana.adc_page_count;
    ana.ana_page_type  = ADC_LOG_PAGE;

    /*
     * Reset DIM logging
     *
     * Log indices for all DIMs in RUNNING state will be reset. Log indices for DIMs which are in
     * TRIGGERED/RESETTING/POLLING states will be reset until they return to RUNNING state.
     */

    ana.dim_page_count   = ana.num_dim_channels * DIM_PAGES_PER_CHANNEL;
    ana.curr_dim_channel = 16;

    anaLogNextChannel(&ana.curr_dim_channel, dimStateGetActiveList(channel));

    // Reset DIM log indices for all DIMs in RUNNING state and start logging

    dimStateResetLogIndices(channel, DIM_PAGES_PER_CHANNEL * 8, status->critical.dim_log_index);

#ifdef DEBUG
    logPrintf(channel, "anaLogResetIndex: critical.adc_log_index=%u  critical.dim_log_index=%u  ana.curr_adc_channel=%u  adc_log_index=%u  is_adc_log_frozen=%u\n",
                    status->critical.adc_log_index,
                    status->critical.dim_log_index,
                    ana.curr_adc_channel,
                    ana.adc_log_index,
                    ana.is_adc_log_frozen);
#endif
}



bool anaLogRequestPage(uint32_t channel, struct FGClite_command *ctrl_cmd)
{
    struct Ana_log &ana = ana_log[channel];
    uint32_t       &a   = ana.adc_pages;
    uint32_t       &d   = ana.dim_pages;

    switch(ana.ana_page_type)
    {
        case ADC_LOG_PAGE:
            if(!ana.is_adc_log_frozen) // See comment in anaLogResetIndex() above
            {
                anaLogRequestAdc(channel, ctrl_cmd);
            }
            --ana.adc_page_count;
            if(--ana.ana_page_count == 0 && ana.dim_page_count > 0)
            {
                ana.ana_page_count = (d < ana.dim_page_count) ? d : ana.dim_page_count;
                ana.ana_page_type  = DIM_LOG_PAGE;
            }
            break;

        case DIM_LOG_PAGE:
            anaLogRequestDim(channel, ctrl_cmd);
            --ana.dim_page_count;
            if(--ana.ana_page_count == 0 && ana.adc_page_count > 0)
            {
                ana.ana_page_count = (a < ana.adc_page_count) ? a : ana.adc_page_count;
                ana.ana_page_type  = ADC_LOG_PAGE;
            }
            break;
    }

    return ana.adc_page_count == 0 && ana.dim_page_count == 0;
}



void anaLogGetPage(uint32_t channel, const struct FGClite_status *status)
{
    struct Ana_log &ana = ana_log[channel];

    if(ana.is_data_pending)
    {
        ana.is_data_pending = false;

        if(ana.requested_page_type == ADC_LOG_PAGE)
        {
            // Handle ADC data requested in previous cycle

            ccLogAdc(channel, ana.requested_channel, status);
        }
        else // DIM logging
        {
            // Handle DIM data requested in previous cycle

            ccLogDim(channel, ana.requested_channel, ana.requested_index, status);
        }
    }
}



bool anaLogIsOverrun(uint32_t channel, const struct FGClite_status *status)
{
    struct Ana_log &ana = ana_log[channel];

    bool    is_overrun;
    uint8_t dim;

    // Check ADC_LOG_INDEX for overrunning

    uint16_t log_index_relative = (status->critical.adc_log_index + ADC_BUFFER_SIZE - ana.adc_log_index) % ADC_BUFFER_SIZE;

    // On the first cycle, the last address written is +20, and there are 20 samples written per cycle.
    // So to detect overruns, check if we are overwriting the 20 samples in the range (0,+19).

    if((is_overrun = ccLogAdcIsLogging(channel) && log_index_relative < 20))
    {
        logPrintf(channel, "ANA_LOG Overrun of ADC_LOG_INDEX on %s channel: (reading,writing,relative) = (%d,%d,%d). Resetting analogue logging.\n",
            ana.curr_adc_channel == ADC_I_A_LOG ? "I_A" :
            ana.curr_adc_channel == ADC_I_B_LOG ? "I_B" : "V_MEAS",
            ana.adc_log_index, status->critical.adc_log_index, log_index_relative);
    }
    // Check DIM_LOG_INDEX for overrunning by checking if we are overwriting the 8 samples in the range (0,+7).
    else if((is_overrun = dimStateCheckOverrun(channel, status->critical.dim_log_index, &dim)))
    {
        logPrintf(channel, "DIM_LOG Overrun of DIM_LOG_INDEX on channel %d, resetting analogue logging.\n", dim);
    }

    if(is_overrun)
    {
        // Cancel all discontinuous logs in progress

        ccLogEndDiscontinuous(channel, LOG_ACQ_1KHZ, -1, NULL);

        // Cancel all DIM logs in progress

        uint16_t dim_mask = dimStateGetActiveList(channel);

        for(dim = 0; dim < 16; ++dim)
        {
            if((1 << dim) & dim_mask) ccLogEndDiscontinuous(channel, static_cast<enum LOG_index>(LOG_DIM + dim), -1, NULL);
        }

        // Set all DIM logging channels to INACTIVE

        dimStateResetActiveList(channel);
    }

    return is_overrun;
}



void anaLogFreezeAdc(uint32_t channel, struct FGClite_status const * status,  struct FGClite_command *ctrl_cmd)
{
    struct Ana_log &ana = ana_log[channel];

    uint16_t log_index_relative = (status->critical.adc_log_index + ADC_BUFFER_SIZE - ana.adc_log_index) % ADC_BUFFER_SIZE;

    // If the FGClite write index is within 40ms (2 cycles) of the read index, freeze to avoid an overrrun

    if(log_index_relative > ADC_BUFFER_SIZE - 41)
    {
        fgcliteCmdSetCtrl(ctrl_cmd, channel, ADC_LOG_FREEZE);
#ifdef DEBUG
        logPrintf(channel, "anaLogFreezeAdc: ana.adc_log_index=%d  critical.adc_log_index=%d   command=0x%08X\n",
                  ana.adc_log_index, status->critical.adc_log_index, ctrl_cmd->payload[channel-1]);
#endif
    }
}



void anaLogUnfreezeAdc(uint32_t channel, struct FGClite_command * const ctrl_cmd)
{
    fgcliteCmdUnsetCtrl(ctrl_cmd, channel, ADC_LOG_FREEZE);

#ifdef DEBUG
    logPrintf(channel, "anaLogUnfreezeAdc: command=0x%08X\n", ctrl_cmd->payload[channel-1]);
#endif
}

// EOF
