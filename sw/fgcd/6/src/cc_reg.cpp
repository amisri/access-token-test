
/*!
 * @file   cc_reg.cpp
 * @brief  Initialise the CCLIBS regulation manager structures
 * @author Michael Davis
 */

#include <equipdev.h>
#include <cc_reg.h>
#include <fgcd_thread.h>

/*
 * FGClite iteration period (10ms) in us
 */

const uint32_t ITER_PERIOD_US = 10000;

/*
 * Actuation delay = 2 x 10ms iteration acquisition periods
 *
 * This defines the time from the nominal time of acquisition until the activation of the DAC.
 * This is a consequence of the WorldFIP cycle time.
 */

const float VS_ACT_DELAY_ITERS = 2.0;

/*
 * ADC_DELAY_ITERS is calculated as:
 *
 * low-pass analogue filter delay                = 0.3 ms
 * FPGA Sigma/Delta filter delay                 = 1.0 ms
 * FGClite FIR filter               = (10 - 1)/2 = 4.5 ms
 * Propagation delay                             = 1.0 ms
 *
 * Total = 6.8 ms, which is 0.68 * 10ms iters
 */

const float ADC_DELAY_ITERS = 0.68;

/*
 * First FIR filter is length 2. Second FIR filter is length 0 (deactivated) so we don't set it explicitly.
 */

const uint32_t I_FIR_LENGTH = 2;

/*
 * Size of measurement FIR buffer.
 *
 * The buffer is used for both FIR filter stages and the extrapolation history, so it must be long enough to cover all three requirements:
 * reg_meas_filter::fir_length[0] + reg_meas_filter::fir_length[1] + reg_meas_filter::extrapolation_len_iters
 */

const uint32_t CC_FILTER_BUF_LEN = (3 * FGC_MAX_PERIOD_ITERS);




// External functions

uint32_t ccRegInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check that FGC constants and CCLIBS constants are consistent

    CC_STATIC_ASSERT(FGC_N_LOADS      == REG_NUM_LOADS,FGC_N_LOADS_must_equal_REG_NUM_LOADS);
    CC_STATIC_ASSERT(FGC_N_RST_COEFFS == REG_NUM_RST_COEFFS,FGC_N_RST_COEFFS_must_equal_REG_NUM_RST_COEFFS);

    // Initialize reg_pars mutex

    if(fgcd_mutex_init(&device.reg_pars_mutex, EQUIPDEV_MUTEX_POLICY) != 0)
    {
        fprintf(stderr,"ERROR: regulationInit:fgcd_mutex_init(&device.reg_pars_mutex) failed for channel %u\n", channel);
        return 1;
    }

    // Initialize libreg - note: regMgrInit clears device.reg_mgr

    regMgrInit(&device.reg_mgr,
               &device.reg_pars,
               NULL,                    // Decoupling data - not required
               &device.reg_pars_mutex,
               (REG_mutex_callback *)pthread_mutex_lock,
               (REG_mutex_callback *)pthread_mutex_unlock,
               NULL,                    // v_ac_buf
               0,
               CC_DISABLED,             // Voltage regulation - not required
               CC_ENABLED,              // Current regulation - required
               CC_DISABLED);            // Field regulation   - not required

    // Initialize current measurement FIR buffer - this must come after regMgrInit because it resets device.reg_mgr

    regMeasFilterInitBuffer(&device.reg_mgr.i.meas, static_cast<int32_t*>(calloc(CC_FILTER_BUF_LEN, sizeof(int32_t))), CC_FILTER_BUF_LEN);

    // Set default parameter values

    regMgrParAppValue(&device.reg_pars, ITER_PERIOD_NS)          = 10000000;        // 10 m
    regMgrParAppValue(&device.reg_pars, VS_ACTUATION)            = REG_VOLTAGE_REF;
    regMgrParAppValue(&device.reg_pars, VS_ACT_DELAY_ITERS)      = VS_ACT_DELAY_ITERS;
    regMgrParAppValue(&device.reg_pars, MEAS_I_FIR_LENGTHS)[0]   = I_FIR_LENGTH;
    regMgrParAppValue(&device.reg_pars, MEAS_I_DELAY_ITERS)      = ADC_DELAY_ITERS;
    regMgrParAppValue(&device.reg_pars, MEAS_V_DELAY_ITERS)      = ADC_DELAY_ITERS;

    // Set default value for I_SAT_GAIN for all loads

    for(uint32_t load_select = 0 ; load_select < REG_NUM_LOADS ; load_select++)
    {
        regMgrParAppValue(&device.reg_pars, LOAD_I_SAT_GAIN)[load_select] = 1.0;
        regMgrParAppValue(&device.reg_pars, LOAD_SAT_SMOOTHING)[load_select] = 0.1;
    }

    // Link libreg to liblog to keep the current regulation log period correct

    regMgrInitRegPeriodPointers(&device.reg_mgr, NULL, &device.log.mgr.period[LOG_I_REG].ns);

    return 0;
}

// EOF
