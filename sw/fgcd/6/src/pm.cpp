//! @file   pm.cpp
//! @brief  Function definitions for submitting Post Mortem data


//#define CCPRINTF

#include <arpa/inet.h>
#include <cstdio>
#include <cstring>
#include <pm-dc-rda3-lib/PMClient.hpp>
#include <pm-dc-rda3-lib/PMData.hpp>
#include <pm-dc-rda3-lib/PMError.hpp>
#include <semaphore.h>
#include <unistd.h>

#include <cmdqmgr.h>
#include <consts.h>
#include <equip_libevtlog.h>
#include <equip_liblog.h>
#include <log.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <logging.h>
#include <page_mgr.h>
#include <pm.h>
#include <pmd_92.h>
#include <pub.h>
#include <rt.h>
#include <sub_cmwdata_gen.h>
#include <sub_utilities.h>

// Constants

const uint32_t    PM_WAIT_CYCLES_EXT_TRIG     = PM_EXT_BUF_LEN / 2;    //!< Number of cycles to wait before freezing Post Mortem buffer following an external trigger

// A self-triggered PM will contain a minimum of one minute of data before the trip and one minute after.
// One minute @ 50 Hz = 60 * 50 = 3000 samples.

// Upper bound on the number of cycles to wait before freezing Post Mortem buffer following a self-trigger

const uint32_t    PM_WAIT_CYCLES_SELF_MAX_92  = PM_SELF_BUF_LEN_92 - 3000;

// Lower bound on the number of cycles to wait before freezing Post Mortem buffer following a self-trigger

const uint32_t    PM_WAIT_CYCLES_SELF_MIN_92  = PM_WAIT_CYCLES_SELF_MAX_92 - 3000;


// Global variables

struct Pm pm_globals;


// Declare thread start function (to ensure type consistency)

static FGCD_thread_func pmRun;


// Static functions


// Post Mortem Dump for an externally-triggered PM event

static void pmDumpExternal(pm::Client &pmclient, uint32_t channel)
{
    struct FGCD_device &device  = fgcd.device[channel];
    struct PM_ext_acq  &ext_acq = device.pm_ext;

    logPrintf(channel, "PM Dump (external) starting\n");

    pm_globals.buffer.ext.trigger = ext_acq.buffer.status[ext_acq.acq_info.trigger_index].status;

    // Copy the frozen Post Mortem buffer

    uint32_t &status_index = ext_acq.acq_info.status_index;

    ccprintf("Filling EXT_PM status start = %u, trigger_index = %u, end = %u\n", status_index, ext_acq.acq_info.trigger_index, status_index-1);

    memset(&pm_globals.buffer.ext.status[0],                             0,                                    sizeof(struct PM_data)*PM_EXT_BUF_LEN);
    memcpy(&pm_globals.buffer.ext.status[0],                             &ext_acq.buffer.status[status_index], sizeof(struct PM_data)*(PM_EXT_BUF_LEN - status_index));
    memcpy(&pm_globals.buffer.ext.status[PM_EXT_BUF_LEN - status_index], &ext_acq.buffer.status[0],            sizeof(struct PM_data)*status_index);

    // Generate and send LHC PMD

    int64_t timestamp = ((int64_t)ext_acq.acq_info.time_sec * 1000000000) +
                        ((int64_t)ext_acq.acq_info.time_nsec);

    pmd_92GenerateAndSendExt(pmclient, &device, pm_globals.buffer.ext, timestamp);

    // Unfreeze Post Mortem buffer

    ext_acq.acq_info.frozen = false;

    __sync_synchronize();

    logPrintf(channel, "PM Dump (external) completed\n");
}


// Post Mortem Dump for a self-triggered PM event

static void pmDumpSelf(pm::Client &pmclient, uint32_t channel)
{
    struct FGCD_device    &device   = fgcd.device[channel];
    struct PM_self_acq    &self_acq = device.pm_self;

    logPrintf(channel, "PM Dump (self) started\n");

    // Copy the trigger sample

    pm_globals.buffer.self.trigger = self_acq.buffer.std.status[self_acq.acq_info.trigger_index].status;

    // Copy the frozen Post Mortem status buffer

    uint32_t &status_index = self_acq.acq_info.status_index;

    uint32_t  start_index  = (self_acq.acq_info.trigger_index + PM_SELF_BUF_LEN_92 - 3000) % PM_SELF_BUF_LEN_92;

    ccprintf("Filling SELF_PM status start = %u, trigger_index = %u, end = %u\n", start_index, self_acq.acq_info.trigger_index, status_index - 1);

    memset(&pm_globals.buffer.self.std.status[0],                                 0,                                         sizeof(struct PM_data)*PM_SELF_BUF_LEN_92);
    memcpy(&pm_globals.buffer.self.std.status[0],                                 &self_acq.buffer.std.status[status_index], sizeof(struct PM_data)*(PM_SELF_BUF_LEN_92 - status_index));
    memcpy(&pm_globals.buffer.self.std.status[PM_SELF_BUF_LEN_92 - status_index], &self_acq.buffer.std.status[0],            sizeof(struct PM_data)*status_index);
    memset(&pm_globals.buffer.self.std.fgc_buffers[0],                            0,                                         LOG_PM_BUF_SIZE);

    // Extract the PM_BUF, which includes the event log and the required signals from the liblog signal logs

    logGetPmBuf(channel, pm_globals.buffer.self.std.fgc_buffers);

    // Generate and send LHC PMD

    int64_t timestamp = ((int64_t)self_acq.acq_info.time_sec * 1000000000) +
                        ((int64_t)self_acq.acq_info.time_nsec);

    pmd_92GenerateAndSendSelf(pmclient, &device, pm_globals.buffer.self.std, timestamp);

    self_acq.acq_info.frozen = false;

    setStatusBit(&equipdev.device[channel].log.pm_trig, FGC_PM_TRIG_PM_SELF_TRIG, false);

    __sync_synchronize();

    logPrintf(channel, "PM Dump (self) completed\n");
}


// Thread to submit post-mortem data

static void *pmRun(void *unused)
{
    // Create post-mortem client

    pm::Client pmclient = pm::Client::createProClient(PM_SERVICE_NAME);

    // Handle post-mortem events

    while(1)
    {
        // Wait for a post-mortem event

        pthread_testcancel();
        sem_wait(&pm_globals.sem);
        pthread_testcancel();

        ccprintf("Semaphore received - checking all devices\n");

        // Scan devices for frozen post-mortem buffers

        for(uint32_t device_id = 1 ; device_id < FGCD_MAX_DEVS ; device_id++)
        {
            struct FGCD_device *device = &fgcd.device[device_id];

            // Check whether a post-mortem buffer for this device is ready for processing

            if(device->pm_ext.acq_info.frozen)
            {
                // Externally-triggered post mortem

                pmDumpExternal(pmclient, device_id);
            }

            if(device->pm_self.acq_info.frozen)
            {
                // Self-triggered post mortem

                pmDumpSelf(pmclient, device_id);
            }

            // Set or clear the POST_MORTEM unlatched status dependent on whether an external or a self-triggered PM is underway

            setStatusBit(&equipdev.device[device_id].status.st_unlatched, FGC_UNL_POST_MORTEM,
                         device->pm_ext.acq_info.frozen  || device->pm_ext.acq_info.wait_cycles > 0 ||
                         device->pm_self.acq_info.frozen || device->pm_self.acq_info.wait_cycles > 0);
        }
    }

    return 0;
}


// External functions

int32_t pmStart(void)
{
    // Initialise semaphore

    if(sem_init(&pm_globals.sem, 0, 0) < 0)
    {
        perror("sem_init pm sem");
        return 1;
    }

    // Start pm thread

    if(fgcd_thread_create(&pm_globals.thread, pmRun, NULL, PM_THREAD_POLICY, PM_THREAD_PRIORITY, NO_AFFINITY) != 0)
    {
        sem_destroy(&pm_globals.sem);
        return 1;
    }

    return 0;
}



void pmPubUpdate(uint32_t channel, struct fgc_stat *status, uint32_t time_sec, uint32_t time_usec)
{
    struct FGCD_device  *device = &fgcd.device[channel];
    union
    {
        float    f;
        uint32_t l;
    }                   pm_real_time_value = {};
    struct PM_data      *pm_sample;

    // Treat 50Hz published data

    // Store real-time value

    if(channel != 0) // Not the FGCD device
    {
        // Byte-swap real-time value

        pm_real_time_value.f = rt.used_data.channels[channel - 1];
        pm_real_time_value.l = htonl(pm_real_time_value.l);
    }

    // Add status data to external-trigger post-mortem buffer

    if(!device->pm_ext.acq_info.frozen) // Post-mortem buffer is not frozen
    {
        // Copy data to post-mortem buffer and increment index

        pm_sample = &device->pm_ext.buffer.status[device->pm_ext.acq_info.status_index];

        pm_sample->time_sec  = time_sec;
        pm_sample->time_usec = time_usec;
        pm_sample->real_time = pm_real_time_value.f;
        pm_sample->status    = *status;

        device->pm_ext.acq_info.status_index = (device->pm_ext.acq_info.status_index + 1) % PM_EXT_BUF_LEN;
    }

    // Add status data to self-trigger post-mortem buffer

    if(!device->pm_self.acq_info.frozen) // Post-mortem buffer is not frozen
    {
        // Copy data to post-mortem buffer and increment index

        pm_sample = &device->pm_self.buffer.std.status[device->pm_self.acq_info.status_index];

        pm_sample->time_sec  =  time_sec;
        pm_sample->time_usec =  time_usec;
        pm_sample->real_time =  pm_real_time_value.f;
        pm_sample->status    = *status;

        device->pm_self.acq_info.status_index = (device->pm_self.acq_info.status_index + 1) % PM_SELF_BUF_LEN_92;
    }
}



void pmTriggerExternal(uint32_t channel, uint32_t tv_sec, uint32_t tv_nsec)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct PM_ext_acq       &pm_ext = device.fgcd_device->pm_ext;

    // Ignore triggers if Post Mortem thread is not running, or an external PM is underway

    __sync_synchronize();

    ccprintf("pm_globals.thread=%lu  pm_ext.acq_info.wait_cycles=%d  pm_ext.acq_info.frozen=%d\n"
            , pm_globals.thread
            , pm_ext.acq_info.wait_cycles
            , pm_ext.acq_info.frozen
            );

    if(pm_globals.thread == 0 || pm_ext.acq_info.wait_cycles > 0 || pm_ext.acq_info.frozen) return;

    // Store timestamp for the external trigger and set no. of cycles to wait for completion

    logPrintf(channel, "PM Received external trigger event\n");

    setStatusBit(&equipdev.device[channel].status.st_unlatched, FGC_UNL_POST_MORTEM,     true);
    setStatusBit(&equipdev.device[channel].log.pm_trig,         FGC_PM_TRIG_PM_EXT_TRIG, true);

    pm_ext.acq_info.trigger_index = pm_ext.acq_info.status_index;
    pm_ext.acq_info.time_sec      = tv_sec;
    pm_ext.acq_info.time_nsec     = tv_nsec;
    pm_ext.acq_info.wait_cycles   = PM_WAIT_CYCLES_EXT_TRIG + 1;
}



void pmTriggerSelf(uint32_t channel, uint32_t tv_sec, uint32_t tv_nsec)
{
    struct Equipdev_channel &device  = equipdev.device[channel];
    struct PM_self_acq      &pm_self = device.fgcd_device->pm_self;

    // Ignore triggers if Post Mortem thread is not running, or a self PM is underway

    __sync_synchronize();

    ccprintf("pm_globals.thread=%lu  pm_self.acq_info.wait_cycles=%d  pm_self.acq_info.frozen=%d\n"
            , pm_globals.thread
            , pm_self.acq_info.wait_cycles
            , pm_self.acq_info.frozen
            );

    if(pm_globals.thread == 0 || pm_self.acq_info.wait_cycles > 0 || pm_self.acq_info.frozen) return;

    // Store timestamp for the self-trigger and set no. of cycles to wait for completion

    logPrintf(channel, "PM Received self-trigger event\n");

    setStatusBit(&equipdev.device[channel].status.st_unlatched, FGC_UNL_POST_MORTEM,      true);
    setStatusBit(&equipdev.device[channel].log.pm_trig,         FGC_PM_TRIG_PM_SELF_TRIG, true);

    pm_self.acq_info.trigger_index = pm_self.acq_info.status_index;
    pm_self.acq_info.time_sec      = tv_sec;
    pm_self.acq_info.time_nsec     = tv_nsec;
    pm_self.acq_info.wait_cycles   = PM_WAIT_CYCLES_SELF_MAX_92 + 2;

    ccprintf("pm_self.acq_info.trigger_index=%u   pm_self.acq_info.wait_cycles=%d\n"
            , pm_self.acq_info.trigger_index
            , pm_self.acq_info.wait_cycles
            );

    // Put the Page Manager into Post Mortem logging state

    pageMgrPMTrigger(channel);
}



void pmBufferFreeze(uint32_t channel)
{
    struct Equipdev_channel &device  = equipdev.device[channel];
    struct PM_ext_acq       &pm_ext  = device.fgcd_device->pm_ext;
    struct PM_self_acq      &pm_self = device.fgcd_device->pm_self;

    // Check whether an externally-triggered post-mortem acquisition is in progress for the device

    __sync_synchronize();

    if(  pm_ext.acq_info.wait_cycles >  0 &&
       --pm_ext.acq_info.wait_cycles == 0)
    {
        // Freeze post-mortem buffer

        pm_ext.acq_info.frozen = true;

        // Post semaphore to post-mortem thread

        ccprintf("freezing ext PM buffer - sending pm_globals semaphore\n");

        sem_post(&pm_globals.sem);
    }

    // Check whether a self-triggered post-mortem acquisition is in progress for the device
    //
    // The 50 Hz status buffer will contain a minimum of one minute of data before and one minute
    // after the trigger event. It will contain a maximum of one minute before and six minutes after
    // the trigger event. This depends on when the LOW_CURRENT flag becomes active.

    if(pm_self.acq_info.wait_cycles > 0)
    {
        // wait_cycles will count down to 1 and then wait until the liblog buffers are also frozen.
        // It is important that this never takes more than six minutes.

        if(pm_self.acq_info.wait_cycles > 1)
        {
            if(   pm_self.acq_info.wait_cycles < PM_WAIT_CYCLES_SELF_MIN_92
               && getStatusBit(device.status.st_unlatched, FGC_UNL_LOW_CURRENT))
            {
                pm_self.acq_info.wait_cycles = 1;
            }
            else
            {
                pm_self.acq_info.wait_cycles--;
            }
        }

        // Check if wait_cycles has reached 1 and if the liblog logs are now all frozen

        if(pm_self.acq_info.wait_cycles == 1 && ccLogPMIsFrozen(channel) == true)
        {
            // Freeze post-mortem buffer

            pm_self.acq_info.wait_cycles = 0;
            pm_self.acq_info.frozen      = true;

            // Post semaphore to post-mortem thread

            ccprintf("freezing self PM buffer - sending pm_globals semaphore\n");

            sem_post(&pm_globals.sem);
        }
    }
}



bool pmIsActive(uint32_t const channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    return (getStatusBit(device.status.st_unlatched, FGC_UNL_POST_MORTEM) != 0);
}


// EOF
