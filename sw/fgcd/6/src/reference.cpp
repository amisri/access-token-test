//!  @file  reference.cpp
//! @brief  Class specific function related to the use of libref

#include <equipdev.h>

uint32_t referenceInitClass(uint32_t channel)
{
    struct REF_mgr *ref_mgr = &equipdev.device[channel].ref_mgr;

    // Initialise default values

    refMgrParValue(ref_mgr, MODE_TO_OFF_FAULTS) = 0;
    refMgrParValue(ref_mgr, MODE_REF_CYC_SEL) = CC_ENABLED;
    refMgrParValue(ref_mgr, MODE_PRE_FUNC)    = REF_PRE_FUNC_RAMP;
    refMgrParValue(ref_mgr, MODE_POST_FUNC)   = REF_POST_FUNC_HOLD;

    return 0;
}

// EOF
