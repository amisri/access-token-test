//! @file   prop_equip.cpp
//! @brief  Equipment device-specific functions for getting and setting properties


#include <cstdlib>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include <fgc_errs.h>
#include <classes/92/defprops.h>

#include <equip_common.h>
#include <equip_libevtlog.h>
#include <equip_liblog.h>
#include <equip_logger.h>
#include <cc_sig.h>
#include <parser.h>
#include <cmd.h>
#include <cmwpub.h>
#include <codes.h>
#include <dim_bus.h>
#include <dim_state.h>
#include <equipdev.h>
#include <fgclite_cycle.h>
#include <logging.h>
#include <mode_pc.h>
#include <nonvol.h>
#include <onewire.h>
#include <state_pc.h>
#include <prop_equip.h>


// Calibration level symbol list

const struct sym_lst sym_lst_cal_ref[] =
{
    {STC_CALZERO, SIG_CAL_OFFSET},
    {STC_CALPOS,  SIG_CAL_POSITIVE},
    {STC_CALNEG,  SIG_CAL_NEGATIVE},
    {0}
};


// Static function definitions


// Scan non-volatile storage and report any properties which have been changed since the last reset

static int32_t propEquipScanForChanges(struct Cmd *command, char *dir_name)
{
    struct stat s;

    uint32_t channel = command->device->id;
    struct Equipdev_channel &device  = equipdev.device[channel];

    // Check directory exists and is readable

    DIR *dir;

    if(stat(dir_name, &s) == -1)                  // directory does not exist
    {
        // return silently unless there is a problem with the filesystem

        if(errno != ENOENT) logPrintf(channel, "PROP_EQUIP Can't stat %s: error %d\n", dir_name, errno);
        return 0;
    }
    else if(!S_ISDIR(s.st_mode))                  // it exists but it's not a directory
    {
        logPrintf(channel, "PROP_EQUIP %s is not a directory\n", dir_name);
        return 0;
    }
    else if((dir = opendir(dir_name)) == NULL)    // could not open directory for reading
    {
        logPrintf(channel, "PROP_EQUIP %s cannot be opened for reading: error %d\n", dir_name, errno);
        return 0;
    }

    // Scan all properties in this directory and add to the output

    int32_t         max_len = CMD_MAX_VAL_LENGTH - command->value_length - 1;
    char           *value   = command->value + command->value_length;
    struct dirent   dir_ent;
    struct dirent  *dir_ent_p;

    while(readdir_r(dir, &dir_ent, &dir_ent_p) == 0 && dir_ent_p != NULL)
    {
        int32_t  len;
        char     file_name[FILENAME_MAX + 1];
        FILE    *file;

        // Ignore anything except symbolic links and normal files

        if(dir_ent.d_type != DT_LNK && dir_ent.d_type != DT_REG) continue;

        // Ignore properties without the CONFIG flag or that does not exist

        struct prop* p = parserRetrieveProperty(command->parser, dir_ent.d_name);

        if(!p || !(p->flags & PF_CONFIG))
        {
            continue;
        }

        // Check whether the file was updated more recently than the last reset

        snprintf(file_name, sizeof(file_name), "%s/%s", dir_name, dir_ent.d_name);

        stat(file_name, &s);

        if(s.st_mtime <= device.config_reset_time) continue;

        // Open the file for reading

        if((file = fopen(file_name, "r")) == NULL) continue;

        // Add the filename to the property

        if((len = snprintf(value, max_len, "%s:", dir_ent.d_name)) < 0)
        {
            logPrintf(channel, "PROP_EQUIP snprintf encoding error\n");
            fclose(file);
            closedir(dir);
            return 1;
        }
        else if(len >= max_len)
        {
            // command buffer is full

            sprintf(command->value + CMD_MAX_VAL_LENGTH - 5, "...\n");
            command->value_length = CMD_MAX_VAL_LENGTH - 1;
            fclose(file);
            break;
        }

        max_len               -= len;
        command->value_length += len;
        value                 += len;

        // Add the file contents to the property

        len = fread(value, 1, max_len, file);

        if(ferror(file) != 0)
        {
           logPrintf(channel, "PROP_EQUIP error reading file %s\n", file_name);
           fclose(file);
           closedir(dir);
           return 1;
        }
        fclose(file);

        max_len               -= len;
        command->value_length += len;
        value                 += len;
    }

    closedir(dir);

    return 0;
}

// SETIF functions

int32_t SetifCalActiveOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return  device.status.state_op == FGC_OP_NORMAL                                           &&
           (device.status.state_pc == FGC_PC_OFF || device.status.state_pc == FGC_PC_FLT_OFF) &&
            device.config_mode     == FGC_CFG_MODE_SYNC_NONE                                  &&
            device.reg_mgr.i.lim_meas.flags.zero;    // Circuit current is below the Zero limit
}



int32_t SetifCalOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return   device.status.state_op == FGC_OP_NORMAL &&
            (device.status.state_pc == FGC_PC_OFF    || device.status.state_pc == FGC_PC_FLT_OFF);
}



int32_t SetifConfigModeOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_op != FGC_OP_CALIBRATING    &&
           device.config_mode == FGC_CFG_MODE_SYNC_NONE;
}



int32_t SetifFbsCmd(struct Cmd *command)
{
    // This function should return 0 in case the command was not sent through CMW or TCP. The serial
    // port terminal is not implemented in FGClite, so we always return 1.

    return 1;
}



int32_t SetifModeOpOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Check whether device is in a state in which the operational mode may be changed

    return    device.status.state_op != FGC_OP_NORMAL
           || device.crate_type == FGC_CRATE_TYPE_RECEPTION
           || (   device.status.state_pc <= FGC_PC_STOPPING
               && device.reg_mgr.i.lim_meas.flags.low == true
               && device.diag.fw_diode.state != FGC_VDI_FAULT
               && device.diag.fabort_unsafe.state != FGC_VDI_FAULT
               && device.diag.thyr_unsafe.state != FGC_VDI_FAULT);
}



int32_t SetifModeRtOk(struct Cmd *command)
{

    // managed by cclibs
    return 1;

}



int32_t SetifOpNormalSim(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return (device.status.state_op == FGC_OP_NORMAL || device.status.state_op == FGC_OP_SIMULATION) &&
           (device.config_mode != FGC_CFG_MODE_SYNC_FGC);
}



int32_t SetifOpNotCal(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_op != FGC_OP_CALIBRATING;
}



int32_t SetifOpTestingSim(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_op == FGC_OP_TEST || device.status.state_op == FGC_OP_SIMULATION;
}



int32_t SetifPcArmed(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_pc == FGC_PC_ARMED;
}



int32_t SetifPcOff(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_pc == FGC_PC_OFF || device.status.state_pc == FGC_PC_FLT_OFF;
}



int32_t SetifRefDac(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_op == FGC_OP_TEST;
}



int32_t SetifRegStateNone(struct Cmd *command)
{
    // libreg REG_MODE must be NONE for some properties

    return regMgrVarValue(&equipdev.device[command->device->id].reg_mgr, REG_MODE) == REG_NONE;
}



int32_t SetifSwitchOk(struct Cmd *command)
{
    logPrintf(command->device->id, "Called SetifSwitchOK for channel %d (incomplete)\n", command->device->id);

    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // FGClite does not have a load switch, so this function checks for polarity switch only

    // Attempts to change the state will be rejected (BAD STATE) if any of the following conditions are true:

    if((device.status.state_op != FGC_OP_NORMAL && device.status.state_op != FGC_OP_SIMULATION) ||   // STATE.OP is not NORMAL or SIMULATION
       (device.status.state_pc != FGC_PC_OFF && device.status.state_pc != FGC_PC_FLT_OFF)       ||   // STATE.PC not OFF or FLT_OFF
       (regMgrVarValue( &device.reg_mgr, FLAG_I_MEAS_ZERO) == false)                         ||   // ZERO flag is not set
       (device.polswitch_mgr.vars.state != FGC_POL_POSITIVE &&
        device.polswitch_mgr.vars.state != FGC_POL_NEGATIVE))                                        // POL_SWITCH.STATE is not POSITIVE or NEGATIVE
    {
        return 0;
    }
    else
    {
        return 1;
    }
}



// SET functions

int32_t SetCal(struct Cmd *command, struct prop *property)
{
    enum SIG_cal_level cal_level = SIG_CAL_NONE;
    uint8_t            cal_channel;
    char               delimiter;

    uint32_t &channel = command->device->id;

    // Get the calibration channel

    if(propEquipScanSymbolList(&cal_channel, &delimiter, command, property) != 0) return 1;

    // Get the calibration level

    if(cal_channel == FGC_CAL_TYPE_ADC_A  || cal_channel == FGC_CAL_TYPE_ADC_B  || cal_channel == FGC_CAL_TYPE_ADC_C ||
       cal_channel == FGC_CAL_TYPE_DCCT_A || cal_channel == FGC_CAL_TYPE_DCCT_B || cal_channel == FGC_CAL_TYPE_DCCTS )
    {
        // S CAL {ADC_A,ADC_B,ADC_C,DCCT_A,DCCT_B,DCCTS} takes the calibration level as a second parameter

        struct prop cal_prop;
        uint8_t     cl;

        cal_prop.flags = property->flags;
        cal_prop.range = &sym_lst_cal_ref;

        if(propEquipScanSymbolList(&cl, NULL, command, &cal_prop) != 0) return 1;

        cal_level = static_cast<enum SIG_cal_level>(cl);
    }
    else if(delimiter != '\0')
    {
        // S CAL {ADCS,DAC,DAC_A} don't take a second parameter

        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Request calibration of the specified channel(s)

    switch(cal_channel)
    {
        case FGC_CAL_TYPE_ADC_A:
            ccSigCalRequest(channel, CAL_ADC_A, cal_level);
            break;

        case FGC_CAL_TYPE_ADC_B:
            ccSigCalRequest(channel, CAL_ADC_B, cal_level);
            break;

        case FGC_CAL_TYPE_ADC_C:
            ccSigCalRequest(channel, CAL_ADC_C, cal_level);
            break;

        case FGC_CAL_TYPE_ADCS:
            ccSigCalRequest(channel, CAL_ADC_A, SIG_CAL_OFFSET);
            ccSigCalRequest(channel, CAL_ADC_A, SIG_CAL_POSITIVE);
            ccSigCalRequest(channel, CAL_ADC_A, SIG_CAL_NEGATIVE);
            ccSigCalRequest(channel, CAL_ADC_B, SIG_CAL_OFFSET);
            ccSigCalRequest(channel, CAL_ADC_B, SIG_CAL_POSITIVE);
            ccSigCalRequest(channel, CAL_ADC_B, SIG_CAL_NEGATIVE);
            ccSigCalRequest(channel, CAL_ADC_C, SIG_CAL_OFFSET);
            ccSigCalRequest(channel, CAL_ADC_C, SIG_CAL_POSITIVE);
            ccSigCalRequest(channel, CAL_ADC_C, SIG_CAL_NEGATIVE);

            // Reset the time of the next auto ADC calibration to 24 hours in the future

            ccSigScheduleAutoAdcsCal(channel, 86400);
            break;

        case FGC_CAL_TYPE_DCCT_A:
            ccSigCalRequest(channel, CAL_DCCT_A, cal_level);
            break;

        case FGC_CAL_TYPE_DCCT_B:
            ccSigCalRequest(channel, CAL_DCCT_B, cal_level);
            break;

        case FGC_CAL_TYPE_DCCTS:
            ccSigCalRequest(channel, CAL_DCCT_A, cal_level);
            ccSigCalRequest(channel, CAL_DCCT_B, cal_level);
            break;

        case FGC_CAL_TYPE_DAC:
            ccSigCalRequest(channel, CAL_DAC, SIG_CAL_OFFSET);
            ccSigCalRequest(channel, CAL_DAC, SIG_CAL_POSITIVE);
            ccSigCalRequest(channel, CAL_DAC, SIG_CAL_NEGATIVE);
            break;

        default:
            command->error = FGC_BAD_PARAMETER;
            return 1;
    }

    // Start the calibration sequence

    if(ccSigCalStart(channel) != 0)
    {
        command->error = FGC_BAD_STATE;
        return 1;
    }

    return 0;
}



int32_t SetCalDccts(struct Cmd *command, struct prop *property)
{
    struct prop        cal_prop;
    uint8_t            cl;
    enum SIG_cal_level cal_level;

    uint32_t &channel = command->device->id;

    cal_prop.flags = property->flags;
    cal_prop.range = &sym_lst_cal_ref;

    // Get the calibration level

    if(propEquipScanSymbolList(&cl, NULL, command, &cal_prop) != 0) return 1;

    cal_level = static_cast<enum SIG_cal_level>(cl);

    // Request calibration of the DCCTs at the level specified

    ccSigCalRequest(channel, CAL_DCCT_A, cal_level);
    ccSigCalRequest(channel, CAL_DCCT_B, cal_level);

    // Start the calibration sequence

    if(ccSigCalStart(channel) != 0)
    {
        command->error = FGC_BAD_STATE;
        return 1;
    }

    return 0;
}



int32_t SetCore(struct Cmd *command, struct prop *property)
{
    command->error = FGC_NOT_IMPL;

    return 1;
}



int32_t SetDefaults(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are set:
    //
    // REF.DEFAULTS.I.RESET
    //
    // This action will be executed automatically when CONFIG.STATE is reset following a synchronisation by the FGC configuration manager.

    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Set PLEP defaults

    uint32_t load_select = regMgrParValue(&device.reg_mgr, LOAD_SELECT);
    struct REF_default_ptrs * ref_defaults = &device.ref_mgr.ref_defaults[load_select][REG_CURRENT];

    device.ppm[0].transactional.plep.acceleration[0] = *ref_defaults->acceleration;
    device.ppm[0].transactional.plep.linear_rate [0] = *ref_defaults->linear_rate;

    // Note: REF.PLEP.EXP_FINAL is set to zero to request that it being set automatically, based on the voltage
    // limits, polarity switch positive, regulation mode and closedloop limit.

    device.ppm[0].transactional.plep.exp_final[0] = 0.0;

    // Note: REF.PLEP.EXP_TC is set to zero for 2Q and 4Q converters, which disables the exponential section of the PLEP. For a 1Q converter,
    // it is set to the time constant of the load + 15%. This factor reduces the risk of trips if the actual time constant is a bit longer
    // than the theoretical value due to cable heating.

    device.ppm[0].transactional.plep.exp_tc[0] = device.reg_mgr.v.lim_ref.flags.unipolar
                                               ? device.reg_mgr.load_pars.tc * 1.15
                                               : 0.0;

    // Set RAMP defaults (if we are in current regulation mode)

    if(refMgrParValue(&device.ref_mgr, MODE_REG_MODE) == FGC_REG_I)
    {
        device.ppm[0].transactional.ramp.acceleration[0] = *ref_defaults->acceleration;
        device.ppm[0].transactional.ramp.deceleration[0] = *ref_defaults->deceleration;
        device.ppm[0].transactional.ramp.linear_rate [0] = *ref_defaults->linear_rate;
    }

    // Set TRIM defaults

    device.ppm[0].transactional.trim.duration [0] = 0.0;

    return 0;
}



int32_t SetDevicePPM(struct Cmd *command, struct prop *property)
{
    uint8_t value;

    if(propEquipScanSymbolList(&value, NULL, command, property) != 0) return 1;

    // FGClite does not support PPM

    if(value == FGC_CTRL_ENABLED)
    {
        command->error = FGC_SYMBOL_IS_NOT_SETTABLE;
        return 1;
    }

    return SetInteger(command, property);
}



int32_t SetDirectCommand(struct Cmd *command, struct prop *property)
{
    uint32_t                &channel = command->device->id;
    struct Equipdev_channel &device  = equipdev.device[channel];

    // Check if input is octal or hexadecimal

    uint32_t base = 10;

    if(command->value[0] == '0')
    {
        base = (command->value[1] == 'x' || command->value[1] == 'X') ? 16 : 8;
    }

    // Convert string to int and check validity

    char *endptr = command->value;

    int64_t value = strtol(command->value, &endptr, base);

    if(command->value[0] == '\0' || *endptr != '\0' || errno == ERANGE)
    {
        command->error = FGC_BAD_INTEGER;
        return 0;
    }

    if(value < 0 || value > 0xFFFF)
    {
        command->error = FGC_OUT_OF_LIMITS;
        return 0;
    }

    // Extract set and unset bitmasks

    uint8_t dig_cmd_set   = value & 0xFF;
    uint8_t dig_cmd_unset = value >> 8;

    // Check that we are not setting and unsetting any of the same bits

    if(dig_cmd_set & dig_cmd_unset)
    {
        command->error = FGC_BAD_PARAMETER;
        return 0;
    }

    // Set digital commands. Normally, this is only done within the FGClite cycle (real-time) thread;
    // from the command thread we should use digitalIORequestCmd(). Here we make an exception for testing
    // and diagnostic purposes. This is safe because STATE.OP is TESTING or SIMULATION so the PC state
    // machine is not running.

    digitalIOSetCmd(&device.dig, dig_cmd_unset, false);
    digitalIOSetCmd(&device.dig, dig_cmd_set,   true);

    return 1;
}



int32_t SetFifo(struct Cmd *command, struct prop *property)
{
    // Access the ADC FIFO log (S ADC.FIFO)

    logPrintf(command->device->id, "Called SetFifo() NOT IMPLEMENTED\n");

    command->error = FGC_NOT_IMPL;

    return 1;
}



int32_t SetInternalAdcMpx(struct Cmd *command, struct prop *property)
{
    // Set the analogue input multiplexor selector (S ADC.INTERNAL.MPX)

    logPrintf(command->device->id, "Called SetInternalAdcMpx() NOT IMPLEMENTED\n");

    command->error = FGC_NOT_IMPL;

    return 1;
}



int32_t SetPC(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    uint8_t requested_state;

    if(propEquipScanSymbolList(&requested_state, NULL, command, property) != 0) return 1;

    command->error = modePcSet(channel, requested_state);

    return command->error != FGC_OK_NO_RSP;
}



int32_t SetPeriodIters(struct Cmd *command, struct prop *property)
{
    // In FGClite, the iteration period is 10 ms. On each iteration, the system monitors the
    // measurements of current and voltage. The regulation period is a multiple of the iteration
    // period which defines when the RST algorithm is run. As we can only set V_REF once per
    // 20 ms FIP cycle, the regulation period must be a multiple of 2.
    //
    // In FGC2, the regulation period was set (using REG.I.PERIOD_DIV) as a multiple of 1 ms V
    // regulation periods. The valid values for REG.I.PERIOD_DIV were 1-9, 10, 20, 30, 40, 50, 60,
    // 80, 100. The comparison between REG.I.PERIOD_DIV (FGC2) and i_period_iters (FGClite) is
    // shown in the following table:
    //
    // REG.I.PERIOD_DIV     CCLIBS i_period_iters     Regulation period (see regMgrSignalPrepareRT())
    //
    //     1-9                 n/a
    //      20                   2                     20 ms = 50 Hz     / 60 per 1.2s basic period
    //      40                   4                     40 ms = 25 Hz     / 30
    //      60                   6                     60 ms = 16.667 Hz / 20
    //      80                   8                     80 ms = 12.5 Hz   / 15
    //     100                  10                    100 ms = 10 Hz     / 12

    uint32_t                &channel = command->device->id;
    struct Equipdev_channel &device  = equipdev.device[channel];

    // Only valid values are 2, 4, 6, 8, 10.
    // See comment under ParsPeriods() for detailed explanation.

    // Parsing the value from the command in order to validate it is complicated. It's simpler to
    // set the value of the property from the command and validate it afterwards.

    // Store the previous value in case it fails validation

    uint32_t prev_i_period_iters[FGC_N_LOADS];

    for(uint32_t idx = 0; idx < FGC_N_LOADS; idx++)
    {
        prev_i_period_iters[idx] = regMgrParAppValue(&device.reg_pars,IREG_PERIOD_ITERS)[idx];
    }

    // Get pointer to number of elements and validate

    const uint32_t offset = sizeof(Equipdev_channel) * channel;

    uint32_t *num_elements = reinterpret_cast<uint32_t*>(reinterpret_cast<char*>(property->num_elements) + offset);

    uint32_t prev_num_elements = *num_elements;

    // Set and validate the value

    int32_t retval;

    if((retval = SetInteger(command, property)) == 0)
    {
        if(*num_elements != FGC_N_LOADS)
        {
            command->error = FGC_BAD_ARRAY_LEN;
            retval = 1;
        }
    }

    // Check if value is an odd number or out of range

    for(uint32_t idx = 0; retval == 0 && idx < FGC_N_LOADS; idx++)
    {
        uint32_t period_iters = regMgrParAppValue(&device.reg_pars,IREG_PERIOD_ITERS)[idx];

        if(period_iters & 1 || period_iters > 10)
        {
            command->error = FGC_OUT_OF_LIMITS;
            retval = 1;
        }
    }

    if(retval != 0)
    {
        // Validation failed, restore REG.I.PERIOD_ITERS to its original state

        *num_elements = prev_num_elements;

        for(uint32_t idx = 0; idx < FGC_N_LOADS; idx++)
        {
            regMgrParAppValue(&device.reg_pars,IREG_PERIOD_ITERS)[idx] = prev_i_period_iters[idx];
        }
    }
    else
    {
        for(uint32_t idx = 0; idx < FGC_N_LOADS; idx++)
        {
           device.reg.i_period[idx] = regMgrParAppValue(&device.reg_pars,IREG_PERIOD_ITERS)[idx] * device.reg_mgr.iter_period;
        }
    }

    return retval;
}



int32_t SetPolaritySwitch(struct Cmd *command, struct prop *property)
{
    command->error = FGC_NOT_IMPL;

    return 1;
}



int32_t SetReset(struct Cmd *command, struct prop *property)
{
    uint32_t                &channel = command->device->id;
    struct Equipdev_channel &device  = equipdev.device[channel];

    // Check the command is S {PROP} RESET

    if(strcasecmp(command->value, "reset") != 0)
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Property-specific handling

    switch(property->sym_idx)
    {
        case STP_ADC:                                       // S ADC RESET

            device.adc.reset_counter = 2;
            break;

        case STP_STATE:                                     // S CONFIG.STATE RESET

            // Reset CONFIG.MODE and CONFIG.STATE, and store the time of the last reset.
            //
            // This will normally not be done by a user but rather by the FGC configuration management program following a synchronisation.

            device.config_reset_time = nonvolGetModifiedTime();    // Set reset time to time the last nonvol property was written
            device.config_mode       = FGC_CFG_MODE_SYNC_NONE;
            device.config_state      = FGC_CFG_STATE_SYNCHRONISED;

            // Initialise the default non-PPM REG mode.
            // This is the SECOND COPY of the REG.MODE_INIT property to the non-PPM reg mode, after synchronising
            // with the config database. (The first copy happened after NVS initialisation).

            refMgrParValue(&device.ref_mgr, MODE_REG_MODE) = refMgrParValue(&device.ref_mgr, MODE_REG_MODE_CYC);

            // S REF.DEFAULTS.I.RESET

            SetDefaults(command, property);
            break;

        case STP_DIAG:                                      // S DIAG RESET

            // Reset diagnostic bus. This is done automatically following a DIM trigger.

            dimStateReset(channel);
            break;

        case STP_DLS:                                       // S FGC.DLS RESET

            // Reset latched DALLAS_FLT

            setStatusBit(&device.status.st_latched, FGC_LAT_DALLAS_FLT, false);
            break;

        case STP_LOG:                                       // S LOG RESET

            // LOG - Reset FGC logger event.

            loggerReset(channel);

            // The CERN post-mortem is reset automatically by the gateway once the PM logs have been read

            break;

        case STP_STATUS:                                    // S MEAS.STATUS RESET

            logPrintf(channel, "Called SetReset(MEAS.STATUS) [not implemented]\n");
            break;

        case STP_ST_LATCHED:                                // S STATUS.ST_LATCHED RESET

            // Reset the latched status bits

            setStatusBit(&device.status.st_latched, 0xFFFF, false);
            break;

        case STP_VS:                                        // S VS RESET

            digitalIORequestCmd(&device.dig, DIG_IO_VS_RESET);
            break;

        case STP_FW_DIODE:                                  // S VS.FW_DIODE RESET

            if(device.diag.fw_diode.state == FGC_VDI_FAULT)
            {
                device.diag.fw_diode.state = FGC_VDI_RESET;
            }

            setStatusBit(&device.status.st_faults, FGC_FLT_FW_DIODE, false);
            break;

        case STP_FABORT_UNSAFE:                             // S VS.FABORT_UNSAFE RESET

            if(device.diag.fabort_unsafe.state == FGC_VDI_FAULT)
            {
                device.diag.fabort_unsafe.state = FGC_VDI_RESET;
            }

            setStatusBit(&device.status.st_faults, FGC_FLT_FABORT_UNSAFE, false);
            break;

        case STP_THYR_UNSAFE:                               // S VS.THYR_UNSAFE RESET

            if(device.diag.thyr_unsafe.state == FGC_VDI_FAULT)
            {
                device.diag.thyr_unsafe.state = FGC_VDI_RESET;
            }

            setStatusBit(&device.status.st_faults, FGC_FLT_THYR_UNSAFE, false);
            break;

        default:

            // Function has been called for a property it does not handle

            command->error = FGC_NOT_IMPL;
            break;
    }

    return command->error != FGC_OK_NO_RSP;
}



int32_t SetResetFaults(struct Cmd *command, struct prop *property)
{
    return statePcResetFaults(command->device->id);
}



int32_t SetConfigMode(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel &device      = equipdev.device[command->device->id];
    uint16_t                 config_mode = device.config_mode;
    int32_t                  retval;

    if((retval = SetInteger(command, property)) == 0)
    {
        return (retval);
    }

    if (   device.status.state_pc != FGC_PC_OFF
        && device.status.state_pc != FGC_PC_FLT_OFF
        && device.config_mode     != FGC_CFG_MODE_SYNC_DB)
    {
        device.config_mode = config_mode;

        command->error = FGC_BAD_STATE;

        return 1;
    }

    return 0;
}



int32_t SetSimIntlks(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    if ( device.status.state_op != FGC_OP_SIMULATION)
    {
        command->error = FGC_BAD_STATE;
        return 1;
    }

    return SetInteger(command, property);
}



int32_t propEquipTerminate(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    // Send a nanofip reset command to the specified FGClite device

    if(property->sym_idx == STP_PWRCYC)
    {
        command->error = fgcliteCycleNanofipReset(channel, RESET_HARD) ? FGC_BUSY : FGC_OK_NO_RSP;
    }
    else if(property->sym_idx == STP_RESET)
    {
        command->error = fgcliteCycleNanofipReset(channel, RESET_SOFT) ? FGC_BUSY : FGC_OK_NO_RSP;
    }
    else
    {
        // Function has been called for a property it does not handle

        command->error = FGC_NOT_IMPL;
    }

    return command->error;
}



// GET functions

int32_t GetComponent(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are accessed:
    //
    // BARCODE.MATCHED
    // BARCODE.MISMATCHED

    uint32_t &channel = command->device->id;

    // Check that the 1-wire scan has completed

    if(!onewireIsDataReady(channel))
    {
        command->error = FGC_ID_NOT_READY;

        return 1;
    }

    command->value_length += codesGroupsList(channel,
                                             property->sym_idx == STP_MATCHED,
                                             command->value + command->value_length,
                                             CMD_MAX_VAL_LENGTH - command->value_length + 1);

    return 0;
}



int32_t GetConfigChanged(struct Cmd *command, struct prop *property)
{
    char dir_name[FILENAME_MAX + 1];

    // Scan the files in the non-volatile storage directory

    snprintf(dir_name, sizeof(dir_name), "%s/%s", NONVOL_PATH, command->device->name);

    return propEquipScanForChanges(command, dir_name);
}



int32_t GetDiagAna(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are accessed:
    //
    // DIAG.ANA.DATA

    uint32_t &channel = command->device->id;

    if(!dimBusIsScanComplete(channel))
    {
        command->error = FGC_DIM_BUS_NOT_READY;
        return 1;
    }

    command->value_length = codesDimsData(channel, true, command->value, CMD_MAX_VAL_LENGTH);

    return 0;
}



int32_t GetDiagDig(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are accessed:
    //
    // DIAG.DIG.DATA

    uint32_t &channel = command->device->id;

    if(!dimBusIsScanComplete(channel))
    {
        command->error = FGC_DIM_BUS_NOT_READY;
        return 1;
    }

    command->value_length = codesDimsData(channel, false, command->value, CMD_MAX_VAL_LENGTH);

    return 0;
}



int32_t GetDim(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are accessed:
    //
    // DIAG.DIM_NAMES
    // DIAG.FAULTS

    uint32_t &channel = command->device->id;

    if(!dimBusIsScanComplete(channel))
    {
        command->error = FGC_DIM_BUS_NOT_READY;
        return 1;
    }

    command->value_length += codesDimsList(channel,
                                           property->sym_idx == STP_FAULTS,
                                           command->parser->from,
                                           command->parser->to,
                                           command->parser->step,
                                           command->value + command->value_length,
                                           CMD_MAX_VAL_LENGTH - command->value_length + 1);

    return 0;
}



int32_t GetFifo(struct Cmd *command, struct prop *property)
{
    // Access the ADC FIFO log (G ADC.FIFO)

    command->error = FGC_NOT_IMPL;

    return 1;
}



int32_t GetId(struct Cmd *command, struct prop *property)
{
    // Check that the 1-wire scan has completed

    if(!onewireIsDataReady(command->device->id))
    {
        command->error = FGC_ID_NOT_READY;

        return 1;
    }

    return GetString(command, property);
}



int32_t GetLogPmBuf(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are accessed:
    //
    // LOG.PM

    uint32_t &channel = command->device->id;

    if(!(command->parser->getopt_flags & GET_OPT_BIN))
    {
        // BIN option was not specified

        command->error = FGC_BAD_GET_OPT;

        return 1;
    }

    // Get PM_BUF - no length check because the command response length is much larger than LOG_PM_BUF_SIZE

    command->value_length = LOG_PM_BUF_SIZE;

    logGetPmBuf(channel, (uint32_t *)command->value);

    return 0;
}



int32_t GetTemp(struct Cmd *command, struct prop *property)
{
    // Check that the 1-wire scan has completed

    if(!onewireIsDataReady(command->device->id))
    {
        command->error = FGC_ID_NOT_READY;

        return 1;
    }

    return GetFloat(command, property);
}



// PARS functions

void ParsRegPars(uint32_t channel)
{
    // Called when the and libreg properties are set.

    // Check libreg parameters for changes

    struct Equipdev_channel &device = equipdev.device[channel];

    regMgrParsCheck(&device.reg_mgr);

    // Recalculate LIMITS.OP.I.AVAILABLE in case LIMITS.I.POS or LIMITS.I.HARDWARE has changed

    device.limits.op_i_available = regMgrVarValue(&device.reg_mgr, LIMITS_I_POS) < device.limits.i_hardware
                                 ? regMgrVarValue(&device.reg_mgr, LIMITS_I_POS)
                                 : device.limits.i_hardware;

    // TEMPORARY - UNTIL YETS 2023/24 when V_PROBE.LOAD.GAIN will become a CONFIG property
    //
    // If still zero, then set V_MEAS nominal gain (V_PROVE.LOAD.GAIN) to VS.GAIN

    if(sigVarValue(&device.sig_struct, transducer, v_meas, TRANSDUCER_NOMINAL_GAIN) == 0.0F)
    {
        sigVarValue(&device.sig_struct, transducer, v_meas, TRANSDUCER_NOMINAL_GAIN) = regMgrParValue(&device.reg_mgr, VS_GAIN);
    }
}



void ParsCalDac(uint32_t channel)
{
    // Called when the following properties are set:
    //
    // CAL.A.DAC
}



void ParsCalFactors(uint32_t channel)
{
    // Called when the following properties are set:
    //
    // CAL.A.ADC.INTERNAL.DTC
    // CAL.A.ADC.INTERNAL.GAIN
    // CAL.A.ADC.INTERNAL.TC
    // CAL.A.DCCT.DTC
    // CAL.A.DCCT.HEADERR
    // CAL.A.DCCT.TC
    // CAL.B.ADC.INTERNAL.DTC
    // CAL.B.ADC.INTERNAL.GAIN
    // CAL.B.ADC.INTERNAL.TC
    // CAL.B.DCCT.DTC
    // CAL.B.DCCT.HEADERR
    // CAL.B.DCCT.TC
    // DCCT.A.GAIN
    // DCCT.B.GAIN
    // DCCT.PRIMARY_TURNS
}

// EOF
