/*!
 * @file   cc_sig.cpp
 * @brief  Interface to libsig and calibration functions
 * @author Michael Davis
 *
 * Initialise the CCLIBS analogue signal manager structures and provide an interface and state
 * machine for calibration
 */

// FGCD includes

#include <timing.h>
#include <logging.h>
#include <nonvol.h>
#include <equipdev.h>
#include <fgcd_thread.h>
#include <fgclite_cycle.h>
#include <cc_sig.h>

// Auto-generated libsig headers

#include "classes/92/sigStructsInit.h"



// Constants

const uint32_t          DAC_RESOLUTION  = 16;                // FGClite has a 16-bit DAC. DAC register signed with 0 raw -> 0 Volts.
const float             DAC_V_NOMINAL   = 12.5;              // Nominal DAC voltage for positive full scale. FGClite has a resolution of 381.5 uV per DAC value.
const float             DAC_MAX_V_ERROR = 0.5;               // Max voltage error when calibrating DAC

/*
 * Raw values for DAC calibration levels:
 *
 * OFFSET    =  0V
 * POSITIVE ~=  8V (8/12.5 x positive full-scale ~= 20971)
 * NEGATIVE ~= -8V (8/12.5 x negative full-scale ~= -20972)
 */

const int16_t cal_dac_raw[] = { 0, 20971, -20972 };

/*
 * The averaging period for measurements is 200ms. In order to ensure we have always completed the
 * averaging, we add 500ms to each of the calibration acquistion times.
 */

const struct timeval    CAL_TIME_DAC      = {  0, 700000 };    // Time to collect I_MEAS samples before calibrating the DAC   =  0s 200ms + 500ms
const struct timeval    CAL_TIME_ADC      = { 10, 500000 };    // Time to collect I_MEAS samples before calibrating the ADCs  = 10s       + 500ms
const struct timeval    CAL_TIME_DCCT     = { 10, 500000 };    // Time to collect I_MEAS samples before calibrating the DCCTs = 10s       + 500ms
const struct timeval    CAL_TIME_SETTLING = {  0, 100000 };    // Time to setle I_MEAS for normal operation                   =           + 100ms



// Types

/*
 * Calibration states
 */

enum State_Cal
{
    IL = 0,    // IDLE
    CS,        // CHANNEL_SELECT
    CL         // CALIBRATE
};



/*
 * Calibration state for one device
 */

struct Cal_Device
{
    pthread_mutex_t            mutex;                          // Allow multiple threads to access Cal_Device

    bool                       calibrating_request;            // Is a calibration sequence underway?

    struct timeval             auto_cal_time;                  // Time that the next automatic calibration sequence will take place

    enum State_Cal             state;                          // Current Calibration system state

    uint16_t                   request_mask [SIG_NUM_CALS];    // Channel mask for manual calibration requests
    uint16_t                   active_mask  [SIG_NUM_CALS];    // Channel mask for active calibrations

    enum SIG_cal_level         level;                          // Current calibration level
    uint16_t                   conv_cmd;                       // Converter command for this calibration
    int16_t                    dac;                            // Raw value for DAC calibration
    struct timeval             acq_time;                       // Time when we have collected enough samples to do the calibration
};



// Global variables, zero initialised

/*
 * State of the Calibration system
 */

struct Cal_Device cc_sig_cal_device[FGCD_MAX_EQP_DEVS] = {};    // Calibration state for each device



// Types for state and transition functions

typedef enum State_Cal       StateFunc(uint32_t channel, bool first_call);
typedef StateFunc*           TransFunc(uint32_t channel);



// Declare static state and transition functions (to ensure type consistency)

static StateFunc stateClIL;
static StateFunc stateClCS;
static StateFunc stateClCL;

static TransFunc ILtoCS;
static TransFunc CStoIL;
static TransFunc CStoCL;
static TransFunc CLtoCS;

// Other static functions

static const char* const ccSigCalText(enum Cal_Channel cal_channel);
static bool ccSigCalSetChannel(uint32_t channel, enum SIG_cal_level level);
static void ccSigCalCalibrate(uint32_t channel, enum Cal_Channel cal_channel);



// Constants

/*
 * Mapping of state enums to state functions
 */

static const StateFunc *run_state [] =
{
    /* IL */ stateClIL,
    /* CS */ stateClCS,
    /* CL */ stateClCL
};



/*
 * Transition functions : called in priority order, from left to right
 */

static const TransFunc* Transitions [][3] =
{
    /* IL */ {         ILtoCS,         NULL },
    /* CS */ { CStoIL,         CStoCL, NULL },
    /* CL */ {         CLtoCS,         NULL }
};



// Static functions

// State functions

static enum State_Cal stateClIL(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct Cal_Device       &cal    = cc_sig_cal_device[channel-1];

    // Check whether it is time for the next auto ADC calibration

    struct timeval timeNow;

    timingGetUserTime(0, &timeNow);

    if(timevalIsLessThan(&cal.auto_cal_time, &timeNow)                                          &&    // Time for auto-calibration
       device.adc.inhibit_cal == 0                                                              &&    // ADC.INTERNAL.INHIBIT_CAL counter is zero
       device.cal.active      == FGC_CTRL_DISABLED                                              &&    // CAL.ACTIVE is DISABLED
       device.config_state    == FGC_CFG_STATE_SYNCHRONISED                                     &&    // Synchronisation with DB is complete
       device.reg_mgr.i.lim_meas.flags.low                                                      &&    // Circuit current is below LIMITS.I.LOW
       (device.state_pc_bit_mask & (FGC_PC_OFF_BIT_MASK | FGC_PC_FLT_OFF_BIT_MASK)) != 0)             // Converter is OFF or FLT_OFF
    {
        // Select the channels to calibrate

        pthread_mutex_lock(&cal.mutex);

        cal.active_mask[SIG_CAL_OFFSET]   |= CAL_ADC_A | CAL_ADC_B | CAL_ADC_C;
        cal.active_mask[SIG_CAL_POSITIVE] |= CAL_ADC_A | CAL_ADC_B | CAL_ADC_C;
        cal.active_mask[SIG_CAL_NEGATIVE] |= CAL_ADC_A | CAL_ADC_B | CAL_ADC_C;

        pthread_mutex_unlock(&cal.mutex);

        // Schedule the next automatic ADC calibration 24 hours from now

        ccSigScheduleAutoAdcsCal(channel, 86400);
    }

    // Check whether there are pending calibration requests. These bitmasks can only be unset within
    // the Calibration state machine, so no memory synchronisation is required before testing them.

    if(cal.active_mask[SIG_CAL_OFFSET] != 0 || cal.active_mask[SIG_CAL_POSITIVE] != 0 || cal.active_mask[SIG_CAL_NEGATIVE] != 0)
    {
        // During the calibration process, notify CCLIBS
        // This allows the I_MEAS and associated log values to freeze to the latest value until the flag is unset

        sigVarValue(&device.sig_struct, adc, adc_a, ADC_CALIBRATING_FLAG) = true;
        sigVarValue(&device.sig_struct, adc, adc_b, ADC_CALIBRATING_FLAG) = true;
        sigVarValue(&device.sig_struct, adc, adc_c, ADC_CALIBRATING_FLAG) = true;

        pthread_mutex_lock(&cal.mutex);

        cal.calibrating_request = true;

        pthread_mutex_unlock(&cal.mutex);
    }

    return IL;
}

static enum State_Cal stateClCS(uint32_t channel, bool first_call)
{
    struct  Equipdev_channel &device = equipdev.device[channel];
    struct  Cal_Device &cal          = cc_sig_cal_device[channel-1];

    if(first_call)
    {
        // Select calibration channel and level

        if(!ccSigCalSetChannel(channel, SIG_CAL_OFFSET)    &&
           !ccSigCalSetChannel(channel, SIG_CAL_POSITIVE)  &&
           !ccSigCalSetChannel(channel, SIG_CAL_NEGATIVE))
        {
            // No further calibration requests: return to IDLE state

            pthread_mutex_lock(&cal.mutex);

            cal.calibrating_request = false;

            pthread_mutex_unlock(&cal.mutex);

            // If not in calibration, then remove the calibration flag from CCLIBS
            // This allows the I_MEAS and associated log values to unfroze 200ms after the change

            sigVarValue(&device.sig_struct, adc, adc_a, ADC_CALIBRATING_FLAG) = false;
            sigVarValue(&device.sig_struct, adc, adc_b, ADC_CALIBRATING_FLAG) = false;
            sigVarValue(&device.sig_struct, adc, adc_c, ADC_CALIBRATING_FLAG) = false;

            // Start the countdown to settle I_MEAS

            timingGetUserTime(0, &cal.acq_time);

            timevalAdd(&cal.acq_time, &CAL_TIME_SETTLING);
        }
    }

    // Wait in CHANNEL_SELECT state until one of the transition functions is activated

    return CS;
}

static enum State_Cal stateClCL(uint32_t channel, bool first_call)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    // Calibrate the devices in the active mask, one per FIP cycle

    for(uint8_t i = CAL_ADC_A; i <= CAL_DAC; i <<= 1)
    {
        if(cal.active_mask[cal.level] & i)
        {
            ccSigCalCalibrate(channel, static_cast<enum Cal_Channel>(i));
            break;
        }
    }

    return CL;
}



// Transition functions

static StateFunc* ILtoCS(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    if(cal.calibrating_request) return stateClCS;

    return NULL;
}

static StateFunc* CStoIL(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    struct timeval timeNow;

    timingGetUserTime(0, &timeNow);

    if(!cal.calibrating_request && timevalIsLessThan(&cal.acq_time, &timeNow))
    {
        return stateClIL;
    }

    return NULL;
}

static StateFunc* CStoCL(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    struct timeval timeNow;

    timingGetUserTime(0, &timeNow);

    if(cal.calibrating_request && timevalIsLessThan(&cal.acq_time, &timeNow))
    {
        return stateClCL;
    }

    return NULL;
}

static StateFunc* CLtoCS(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    if(cal.active_mask[cal.level] == 0)
    {
        return stateClCS;
    }

    return NULL;
}



// Other static functions

/*
 * Convert Calibration channel to text for error reporting
 */

static const char* const ccSigCalText(enum Cal_Channel cal_channel)
{
    switch(cal_channel)
    {
        case CAL_ADC_A:     return "ADC channel A";
        case CAL_ADC_B:     return "ADC channel B";
        case CAL_ADC_C:     return "ADC channel C";
        case CAL_DCCT_A:    return "DCCT channel A";
        case CAL_DCCT_B:    return "DCCT channel B";
        case CAL_DAC:       return "DAC";
    }

    return NULL;
}



/*
 * Schedule the next automatic calibration
 *
 * @param[in]    channel    FIP device to calibrate
 * @param[in]    secs       Number of seconds in the future to schedule the calibration
 */

void ccSigScheduleAutoAdcsCal(uint32_t channel, uint32_t secs)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    timingReadTime(&cal.auto_cal_time);

    cal.auto_cal_time.tv_sec += secs;
}



/*
 * Set the calibration channel for the next calibration.
 *
 * @param[in]        channel    FIP device to calibrate
 * @param[in,out]    bitmask    Bitmask of which channels should be calibrated
 *
 * @retval           true       the calibration level was set
 * @retval           false      no channels to calibrate at this level
 */

static bool ccSigCalSetChannel(uint32_t channel, enum SIG_cal_level level)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    // Initialise end time for acquiring calibration samples to current time

    timingGetUserTime(0, &cal.acq_time);

    // Set the calibration command

    if(cal.active_mask[level] & (CAL_ADC_A | CAL_ADC_B | CAL_ADC_C))
    {
        // Calibrate ADCs

        cal.level    = level;
        cal.dac      = 0;
        cal.conv_cmd = (level << 14)                                        |
                       (cal.active_mask[level] & CAL_ADC_A ? CAL_IA_EN : 0) |
                       (cal.active_mask[level] & CAL_ADC_B ? CAL_IB_EN : 0) |
                       (cal.active_mask[level] & CAL_ADC_C ? CAL_VS_EN : 0);

        timevalAdd(&cal.acq_time, &CAL_TIME_ADC);
    }
    else if(cal.active_mask[level] & (CAL_DCCT_A | CAL_DCCT_B))
    {
        // Calibrate DCCTs

        cal.level    = level;
        cal.dac      = 0;
        cal.conv_cmd = 0;

        timevalAdd(&cal.acq_time, &CAL_TIME_DCCT);
    }
    else if(cal.active_mask[level] & CAL_DAC)
    {
        // Calibrate DAC

        cal.level    = level;
        cal.dac      = cal_dac_raw[cal.level];
        cal.conv_cmd = CAL_SOURCE_DAC << 14 | CAL_IA_EN | CAL_IB_EN;

        timevalAdd(&cal.acq_time, &CAL_TIME_DAC);
    }
    else
    {
        // No channels to calibrate at this level

        cal.level    = SIG_CAL_NONE;
        cal.dac      = 0;
        cal.conv_cmd = 0;
    }

    return cal.level != SIG_CAL_NONE;
}



/*
 * Check that the returned calibration level is valid
 */

static bool ccSigCalLevelIsValid(uint32_t channel, enum Cal_Channel cal_channel, enum SIG_cal_level calibrated_level, enum SIG_cal_level expected_level)
{
    struct Cal_Device       &cal    = cc_sig_cal_device[channel-1];

    if(calibrated_level == SIG_CAL_FAULT)
    {
        logPrintf(channel, "Calibration failed for %s\n", ccSigCalText(cal_channel));
    }
    else if(calibrated_level != expected_level)
    {
        logPrintf(channel, "Calibration level mismatch for %s: expected %d received %d\n", ccSigCalText(cal_channel), expected_level, calibrated_level);
    }
    else
    {
        // Level is valid

        return true;
    }

    // If calibration failed, disable further calibrations of this channel until the next calibration cycle

    cal.active_mask[SIG_CAL_POSITIVE] &= ~cal_channel;
    cal.active_mask[SIG_CAL_NEGATIVE] &= ~cal_channel;

    return false;
}



static void ccSigCalCalibrate(uint32_t channel, enum Cal_Channel cal_channel)
{
    struct Equipdev_channel  &device = equipdev.device[channel];
    struct Cal_Device        &cal    = cc_sig_cal_device[channel-1];

    // Do nothing if calibration for this channel is not active

    if(!(cal.active_mask[cal.level] & cal_channel)) return;

    // Remove the channel from the active mask

    pthread_mutex_lock(&cal.mutex);

    cal.active_mask[cal.level] &= ~cal_channel;

    pthread_mutex_unlock(&cal.mutex);

    // Calibrate the channel

    enum SIG_cal_level calibrated_level = SIG_CAL_FAULT;

    switch(cal_channel)
    {
        case CAL_ADC_A:
            calibrated_level = sigAdcCal(&device.sig_struct.mgr, &device.sig_struct.adc.named.adc_a);
            if(ccSigCalLevelIsValid(channel, cal_channel, calibrated_level, cal.level))
            {
                device.cal.nonvol_mask |= (1 << CAL_A_ADC_INTERNAL_GAIN) | (1 << CAL_A_ADC_INTERNAL_ERR);
            }
            break;
        case CAL_ADC_B:
            calibrated_level = sigAdcCal(&device.sig_struct.mgr, &device.sig_struct.adc.named.adc_b);
            if(ccSigCalLevelIsValid(channel, cal_channel, calibrated_level, cal.level))
            {
                device.cal.nonvol_mask |= (1 << CAL_B_ADC_INTERNAL_GAIN) | (1 << CAL_B_ADC_INTERNAL_ERR);
            }
            break;
        case CAL_ADC_C:
            calibrated_level = sigAdcCal(&device.sig_struct.mgr, &device.sig_struct.adc.named.adc_c);
            if(ccSigCalLevelIsValid(channel, cal_channel, calibrated_level, cal.level))
            {
                device.cal.nonvol_mask |= (1 << CAL_C_ADC_INTERNAL_GAIN) | (1 << CAL_C_ADC_INTERNAL_ERR);
            }
            break;

        case CAL_DCCT_A:
            // If CAL.ACTIVE is set, the head error is forced to zero to allow automatic calibration using the calibration winding in the head

            device.sig_struct.transducer.named.dcct_a.gain_err_ppm = (device.cal.active == FGC_CTRL_ENABLED) ? 0 : device.cal.dcct_a_headerr;

            calibrated_level = sigTransducerCal(&device.sig_struct.mgr, &device.sig_struct.transducer.named.dcct_a);
            if(ccSigCalLevelIsValid(channel, cal_channel, calibrated_level, cal.level))
            {
                device.cal.nonvol_mask |= (1 << CAL_A_DCCT_ERR);
            }
            break;
        case CAL_DCCT_B:
            // If CAL.ACTIVE is set, the head error is forced to zero to allow automatic calibration using the calibration winding in the head

            device.sig_struct.transducer.named.dcct_b.gain_err_ppm = (device.cal.active == FGC_CTRL_ENABLED) ? 0 : device.cal.dcct_b_headerr;

            calibrated_level = sigTransducerCal(&device.sig_struct.mgr, &device.sig_struct.transducer.named.dcct_b);
            if(ccSigCalLevelIsValid(channel, cal_channel, calibrated_level, cal.level))
            {
                device.cal.nonvol_mask |= (1 << CAL_B_DCCT_ERR);
            }
            break;

        case CAL_DAC:
            calibrated_level = sigDacCal(&device.dac, cal_dac_raw[cal.level], device.sig_struct.adc.named.adc_a.meas.average1.mean);
            ccSigCalLevelIsValid(channel, cal_channel, calibrated_level, cal.level);
            break;
    }
}



// External functions

int32_t ccSigInit(uint32_t channel)
{
    struct Equipdev_channel  &device = equipdev.device[channel];
    struct Cal_Device        &cal    = cc_sig_cal_device[channel-1];

    // Check that FGC constants and CCLIBS constants are consistent

    assert(FGC_CAL_VRAW_LEN == SIG_NUM_CALS);

    // Initialise the calibration mutex

    if(fgcd_mutex_init(&cal.mutex, FGCLITE_MUTEX_POLICY) != 0) return 1;

    // Call auto-generated function to initialise sig_struct for this channel

    sigStructsInitDevice(&device.sig_struct,
                         &device.polswitch_mgr,
                         channel);

    // Reset the calibration state machine

    ccSigCalReset(channel);

    return 0;
}



void ccSigCalReset(uint32_t channel)
{
    struct Equipdev_channel  &device = equipdev.device[channel];
    struct Cal_Device        &cal    = cc_sig_cal_device[channel-1];

    pthread_mutex_lock(&cal.mutex);

    // Cancel active calibrations and return to IDLE state

    cal.calibrating_request = false;
    cal.state               = IL;

    cal.request_mask[SIG_CAL_OFFSET]   = 0;
    cal.request_mask[SIG_CAL_POSITIVE] = 0;
    cal.request_mask[SIG_CAL_NEGATIVE] = 0;
    cal.active_mask [SIG_CAL_OFFSET]   = 0;
    cal.active_mask [SIG_CAL_POSITIVE] = 0;
    cal.active_mask [SIG_CAL_NEGATIVE] = 0;

    pthread_mutex_unlock(&cal.mutex);

    // Initialise the DAC

    sigDacInit(&device.dac, DAC_RESOLUTION, DAC_V_NOMINAL, DAC_MAX_V_ERROR);

    // Schedule the first automatic calibration of the ADCs. We want to avoid starting this before
    // the first (self-actuated) FGClite reset, which takes place within the first 20 seconds after
    // FIP communication starts.

    ccSigScheduleAutoAdcsCal(channel, 20);
}



void ccSigInitCal(uint32_t channel)
{
    for(char bus = 'A'; bus <= 'C'; ++bus)
    {
        if(bus <= 'B')
        {
            equipdevPropSetNumEls(channel, bus, "CAL.?.DCCT.HEADERR",      1);
            equipdevPropSetNumEls(channel, bus, "CAL.?.DCCT.ERR",          6);
            equipdevPropSetNumEls(channel, bus, "CAL.?.DCCT.TC",           3);
            equipdevPropSetNumEls(channel, bus, "CAL.?.DCCT.DTC",          3);
        }

        equipdevPropSetNumEls    (channel, bus, "CAL.?.ADC.INTERNAL.GAIN", 1);
        equipdevPropSetNumEls    (channel, bus, "CAL.?.ADC.INTERNAL.ERR",  6);
        equipdevPropSetNumEls    (channel, bus, "CAL.?.ADC.INTERNAL.TC",   3);
        equipdevPropSetNumEls    (channel, bus, "CAL.?.ADC.INTERNAL.DTC",  3);
    }

    equipdevPropSetNumEls        (channel, '\0', "CAL.VREF.ERR",           6);
}



bool ccSigCalIsCalibrating(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    return cal.state != IL || cal.calibrating_request;
}



bool ccSigCalChannelIsCalibrating(uint32_t channel, enum Cal_Channel cal_channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    return cal.calibrating_request &&
          (cal_channel & (cal.active_mask[SIG_CAL_OFFSET]   |
                          cal.active_mask[SIG_CAL_POSITIVE] |
                          cal.active_mask[SIG_CAL_NEGATIVE])) != 0;
}



uint16_t ccSigCalGetCmdBits(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    return cal.conv_cmd;
}



int16_t ccSigCalGetRawValue(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    return cal.dac;
}



void ccSigCalRequest(uint32_t channel, enum Cal_Channel cal_channel, enum SIG_cal_level level)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    pthread_mutex_lock(&cal.mutex);

    cal.request_mask[level] |= cal_channel;

    pthread_mutex_unlock(&cal.mutex);
}



int32_t ccSigCalStart(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    int32_t retval = 0;

    pthread_mutex_lock(&cal.mutex);

    // If we are already calibrating, the request will be dropped and the user will get a BAD_STATE error

    if(ccSigCalIsCalibrating(channel))
    {
        retval = 1;
    }
    else
    {
        // Insert requested mask into the active mask

        cal.active_mask[SIG_CAL_OFFSET]   = cal.request_mask[SIG_CAL_OFFSET];
        cal.active_mask[SIG_CAL_POSITIVE] = cal.request_mask[SIG_CAL_POSITIVE];
        cal.active_mask[SIG_CAL_NEGATIVE] = cal.request_mask[SIG_CAL_NEGATIVE];
    }

    // Reset the request masks

    cal.request_mask[SIG_CAL_OFFSET]   = 0;
    cal.request_mask[SIG_CAL_POSITIVE] = 0;
    cal.request_mask[SIG_CAL_NEGATIVE] = 0;

    pthread_mutex_unlock(&cal.mutex);

    return retval;
}



void ccSigCalUpdate(uint32_t channel)
{
    struct Cal_Device &cal = cc_sig_cal_device[channel-1];

    StateFunc *target_state = NULL;    // pointer to the state to transition to
    TransFunc *trans_fptr;             // pointer to the next transition function to test

    // Scan transitions for the current state, testing each condition in priority order

    for(uint32_t i = 0; (trans_fptr = Transitions[cal.state][i]) != NULL; ++i)
    {
        target_state = (*trans_fptr)(channel);

        if(target_state != NULL)
        {
            // Change state

            cal.state = (*target_state)(channel, true);

            break;
        }
    }

    if(target_state == NULL)
    {
        // No transition : stay in current state and run the state function with the first_call parameter set to false

        (*(run_state[cal.state]))(channel, false);
    }
}

// EOF
