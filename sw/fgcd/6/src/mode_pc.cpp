//! @file   mode_pc.cpp
//! @brief  Manage Power Converter mode changes for FGClite devices


#include <cmwpub.h>
#include <cmdqmgr.h>
#include <logging.h>
#include <equipdev.h>
#include <state_pc.h>
#include <mode_pc.h>
#include <classes/92/defsyms.h>      // Import symbol definitions for CMW logging


void modePcUpdate(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Derive STATE.VS from digital inputs

    device.status.state_vs = digitalIOVsGetState(&device.dig, device.status.state_pc == FGC_PC_STARTING);

    if(device.status.state_vs == FGC_VS_READY)
    {
        // Voltage source is ready so activate libref PC_STATE

        refMgrParValue(&device.ref_mgr, PC_STATE) = REF_PC_ON;

        if(equipdevIsPcPermit(channel) == false)
        {
            // PC_PERMIT is inactive so force MODE_REF to OFF to trigger SLOW_ABORT

            refMgrParValue(&device.ref_mgr, MODE_REF) = REF_OFF;
        }
        else
        {
            // PC_PERMIT is active - check sector access limit when enabled

            const cc_float &meas_i = device.reg_mgr.i.meas.signal[REG_MEAS_FILTERED];

            if(device.sector_access == FGC_CTRL_ENABLED && fabs(meas_i) > device.limits.i_access)
            {
                // Access to sector is active and the current is too high, so go to standby

                refMgrParValue(&device.ref_mgr, MODE_REF) = REF_STANDBY;
            }
        }
    }
    else
    {
        // Voltage source is not ready so de-activate libref PC_STATE

        refMgrParValue(&device.ref_mgr, PC_STATE) = REF_PC_OFF;

        // Converter is stopping or in fault_off so de-activate libref MODE_REF

        if((device.state_pc_bit_mask & (FGC_PC_STOPPING_BIT_MASK | FGC_PC_FLT_STOPPING_BIT_MASK | FGC_PC_FLT_OFF_BIT_MASK)) != 0)
        {
            refMgrParValue(&device.ref_mgr, MODE_REF) = REF_OFF;
        }

        // Converter is off so latch FLT_NO_PC_PERMIT if PC_PERMIT is missing

        if(equipdevIsPcPermit(channel) == false)
        {
            latchStatusBit(&device.status.st_faults, FGC_FLT_NO_PC_PERMIT, true);
        }
    }
}



enum fgc_errno modePcSet(uint32_t channel, uint32_t mode_pc)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check that CAL.ACTIVE is not set (calibration of DCCTs on-going)

    if(   device.status.state_pc == FGC_PC_OFF
       && mode_pc != FGC_PC_OFF
       && device.cal.active == FGC_CTRL_ENABLED)
    {
        return FGC_CAL_ACTIVE;
    }

    // Do not allow power converter to be started if a self-triggered Post Mortem acquisition is in progress

    if(getStatusBit(device.status.st_unlatched, FGC_UNL_POST_MORTEM))
    {
        return FGC_PM_IN_PROGRESS;
    }

    switch(mode_pc)
    {
        case FGC_PC_OFF:

            if(device.status.state_pc == FGC_PC_FLT_OFF)
            {
                // Reset cclibs faults

                fgc_errno errnum = statePcResetFaults(channel);

                if( errnum != FGC_OK_NO_RSP)
                {
                    return errnum;
                }
            }
            else if(device.mode_pc == FGC_PC_OFF)
            {
                // Switch off immediately this is the second attempt to switch off

                digitalIORequestCmd(&device.dig, DIG_IO_VS_OFF);
            }

            break;

        case FGC_PC_IDLE:

            // Check if STATE.PC is an invalid state for IDLE request or if the polarity switch is moving or has a fault

            if((device.state_pc_bit_mask & (FGC_PC_FLT_OFF_BIT_MASK|FGC_PC_FLT_STOPPING_BIT_MASK|FGC_PC_STOPPING_BIT_MASK)) != 0)
            {
                return FGC_BAD_STATE;
            }

            // If state is ARMED and run event has not been received then return to IDLE by disarming the function

            if(device.status.state_pc == FGC_PC_ARMED && refMgrParPointer(&device.ref_mgr, REF_RUN)->secs.abs == 0)
            {
                // Reset reference function type to NONE

                refMgrFgParValue(&device.ref_mgr,REF_FG_TYPE) = FG_NONE;

                referenceArm(channel, 0, false, 0, NULL);
            }

            // If the converter is running, then IDLE aborts the function

            else if(device.status.state_pc == FGC_PC_RUNNING)
            {
                refEventAbort(&device.ref_mgr);
            }

            break;

        case FGC_PC_ON_STANDBY:

            // Check if STATE.PC is an invalid state for STANDBY request or if the polarity switch is moving or has a fault

            if((device.state_pc_bit_mask & (FGC_PC_FLT_OFF_BIT_MASK|FGC_PC_FLT_STOPPING_BIT_MASK|FGC_PC_STOPPING_BIT_MASK)) != 0)
            {
                return FGC_BAD_STATE;
            }

            break;

        case FGC_PC_SLOW_ABORT:

            // Check if STATE.PC is an invalid state for SLOW_ABORT request

            if((device.state_pc_bit_mask & (FGC_PC_OFF_BIT_MASK          |
                                            FGC_PC_FLT_OFF_BIT_MASK      |
                                            FGC_PC_FLT_STOPPING_BIT_MASK |
                                            FGC_PC_STOPPING_BIT_MASK)) != 0)
            {
                return FGC_BAD_STATE;
            }
            break;

        default:

            return FGC_BAD_STATE;
            break;
    }

    // Check if mode_pc has changed

    logPrintf(channel, "MODE_PC: %s => %s\n", sym_names_pc[device.mode_pc].label, sym_names_pc[mode_pc].label);

    if(mode_pc != device.mode_pc)
    {
        device.mode_pc = mode_pc;

        // Notify MODE.PC because it might be that PC was set

        cmwpubNotify(channel, "MODE.PC", 0, 0, 0, 1, 0);

    }

    // Set libref MODE_REF based on the new MODE.PC value

    refMgrParValue(&device.ref_mgr, MODE_REF) = translateModePctoRef(channel, mode_pc);

    return FGC_OK_NO_RSP;
}

// EOF
