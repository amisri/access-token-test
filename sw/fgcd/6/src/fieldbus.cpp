/*!
 * @file   fieldbus.cpp
 * @brief  Functions for FGCD fieldbus properties 
 * @author Michael Davis
 */

#include <fieldbus.h>

// Global variables, zero-initialised

struct Fieldbus_properties fieldbus = {};    // FGC properties for Fieldbus (extern)



// External functions

void fieldbusInit(void)
{
    // With the timdt library, we will not receive a PC_PERMIT timing event in the case that there is
    // no cable. So now we initialise PC_PERMIT to ENABLED on the Gateway. It can be explicitly
    // disabled by a timing event.

    fieldbus.pc_permit = 1;
}
