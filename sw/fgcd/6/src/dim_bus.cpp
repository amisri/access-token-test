/*
 * @file   dim_bus.cpp
 * @brief  FGClite DIM Bus functions
 * @author Michael Davis
 */

#define DEBUG
// #define DIM_WRN_DEBUG

#include <cstring>
#include <bitset>

#include <codes.h>
#include <equipdev.h>
#include <fgc/fgc_db.h>
#include <logging.h>
#include <dim_state.h>
#include <dim_bus.h>

// Constants

const uint32_t DIM_PAGE_MAX         = 3;                      // Maximum page number for DIM Bus Data (0..3)
const uint32_t DIM_DEVS_PER_PAGE    = 4;                      // 4 devices per page x 4 pages = maximum 16 DIMs per bus

// Types

// State of DIM Bus scan

struct DIM_state
{
    bool                    read_in_progress;                 // Are we reading out the DIM pages?
    bool                    is_scan_complete;                 // Have we completed a full scan of the DIM bus?
    bool                    is_data_pending;                  // Did we send a page request last cycle?
    uint8_t                 page_num;                         // Number of the page to read back
};

/*
 * Struct for reading DIM data from the DIM Bus.
 *
 * FGClite has only one DIM bus, with a maximum of 16 devices. Each DIM is allocated 16 bytes,
 * therefore there are 4 DIMs per page, and 4 pages per bus.
 *
 * For data format, see "FGClite Fieldbus Cycle", EDMS 1523469, section 4.2.1
 */

struct __attribute__((__packed__)) FGClite_dim
{
    uint16_t                digital[2];                       // Digital input banks A and B
    uint16_t                counter;                          // Trigger counter
    uint8_t                 reserved;                         // Not used
    uint8_t                 version;                          // Version
    uint16_t                analogue[4];                      // Analog channels 0..3
};

// Following code depends on this

static_assert(sizeof(FGClite_dim::digital) / sizeof(FGClite_dim::digital[0]) == FGC_N_DIM_DIG_BANKS);

// Struct to store responses from the DIM DIM Bus scan

struct __attribute__((__packed__)) FGClite_dim_bus
{
    struct FGClite_dim      dim[FGC_MAX_DIMS];               // Analog and digital data from all DIMs on a single DIM bus
};

// DIM Bus data

struct DIM_bus
{
    struct DIM_state        dim_state;                        // State of the DIM Bus scan
    uint16_t                device_mask;                      // Bitmask of which DIM addresses (0..15) contain a device
    struct FGClite_dim_bus  dim_bus;                          // Stored results of the DIM Bus scan
    uint16_t                digital_mask[FGC_MAX_DIMS][2];    // Mask of which digital signals have been logged in the Event log
};

// Global variables, zero initialised

struct DIM_bus dim_bus[FGCD_MAX_DEVS] = {};

// Static functions

static void dimBusCheckDiagDigSig(uint32_t channel, struct Equipdev_channel_diag_dig_sig * diag_dig_sig);
static bool dimBusGetDiagDigSig(struct Equipdev_channel_diag_dig_sig * diag_dig_sig);

/*
 * Convert a 12-bit little-endian unsigned integer into a 16-bit unsigned integer
 *
 * First byte is bits 0..7
 * Lower nibble in second byte is bits 8..11
 * Upper nibble in second byte is discarded
 */

static uint16_t getUint12(uint16_t const raw)
{
    return raw & 0x0FFF;
}


/*
 * Check if a DIM is present in the indicated slot, and set the bitmask.
 *
 * If there is no DIM in the slot, we get back:
 *
 * FF 0F FF 1F FF 2F FF 3F FF 4F FF 5F FF 6F FF 7F
 */

static void dimBusRegisterDevice(uint8_t const channel, uint8_t const dim_num)
{
    struct DIM_bus &dbus = dim_bus[channel];

    uint8_t *ptr = reinterpret_cast<uint8_t*>(&dbus.dim_bus.dim[dim_num]);
    uint8_t  val = 0x0F;

    for(uint32_t i = 0; i < 8; ++i)
    {
        if(*ptr++ != 0xFF || *ptr++ != val)
        {
            // A valid device was found in the slot

            dbus.device_mask |= 1 << dim_num;

            break;
        }

        val += 0x10;
    }
}



// Update DIAG properties following a scan of the DIM bus

static void dimBusUpdateProps(uint8_t const channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct DIM_bus          &dbus   = dim_bus[channel];

    // Set DIAG.DIMS_DETECTED for DIM Bus A. FGClite only has one bus so we don't need to set a count for Bus B.

    device.diag.dims_detected[0] = std::bitset<16>(dbus.device_mask).count();

    // Check that DIMS_DETECTED == DIMS_EXPECTED

    if(device.diag.dims_detected[0] != device.diag.dims_expected[0])
    {
        logPrintf(channel, "DIM_BUS DIMS_DETECTED (%d) does not match DIMS_EXPECTED (%d)\n", device.diag.dims_detected[0], device.diag.dims_expected[0]);

        device.diag.dims_exp_errs++;

        latchStatusBit(&device.status.st_latched, FGC_LAT_DIMS_EXP_FLT, true);
    }
}



// Update the DIAG.DATA and DIAG.ANASIGS properties for one DIM

static void dimBusUpdateData(uint8_t const channel, uint8_t const dim_num)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    struct DIM_bus          &dbus   = dim_bus[channel];
    struct FGClite_dim      &dim    = dbus.dim_bus.dim[dim_num];

    uint16_t * const data     = device.diag.data;
    float    * const anasigs  = device.diag.anasigs;
    float    * const gain     = device.diag.gain;
    float    * const offset   = device.diag.offset;

    uint32_t  data_idx;

    /*
     * Analog channels : ANA0..ANA3, 128 elements: data[0..0x7F]
     *
     * For DIM bus A (the only bus used with FGClite), the raw 12-bit integer ADC values for the
     * analog channels are stored in data[idx]:
     *
     *  idx(A) = dim_num + 0x20 * ana_chan
     *
     * where ana_chan = 0..3 and dim_address = 0..15.
     *
     * On FGC2/3, the bus B DIMs have an offset of 0x10:
     *
     *  idx(B) = 0x10 + dim_address + 0x20 * ana_chan
     *
     * Once the raw ADC values are scaled as floats, they are stored in anasigs[idx]:
     *
     *  idx = dim_num + 20 x ana_chan
     *
     * This allows 20 DIMs as a legacy alignment with the FGC2 software, which limited the number of
     * DIMs to 20 for memory reasons.
     *
     * On FGClite, the first DIM on Bus A is the DIM inside the FGC.
     * The first external DIM is usually the Voltage Source (VS) DIM. The four analog
     * channels from this DIM are exposed in the critical data, so we sample them at 50 Hz instead
     * in chunks many seconds later (as with the other DIMs). See ccLogCritical().
     *
     * vs_dim_idx is defined in the SysDb and should normally be 1 (DIM Bus A DIM 1). It cannot be
     * zero, as that is the address of the FGC_PSU on-board DIM.
     *
     * In any case the digital data for the VS DIM is updated at 1 Hz.
     */

    if(device.diag.vs_dim_idx == 0 || device.diag.vs_dim_idx != dim_num)
    {
        for(uint32_t dim_channel = 0; dim_channel < 4; ++dim_channel)
        {
            uint32_t ana_idx;

            data_idx = dim_num + dim_channel * 0x20;
            ana_idx  = dim_num + dim_channel * (FGC_N_DIAG_ANA_SIGS/FGC_N_DIM_ANA_CHANS);

            data[data_idx] = getUint12(dim.analogue[dim_channel]);

            // Convert raw analog signals to physical units for DIAG.ANASIGS property

            anasigs[ana_idx] = static_cast<float>(data[data_idx]) * gain[ana_idx] + offset[ana_idx];

            // Special magic for FGClite on-board DIM0 channel B (-15V)

            if(data_idx == 0x20)
            {
                // -15V channel depends on +15V channel, see EPCCCS-4799 and EPCCCS-4871 for details

                anasigs[ana_idx] -= 1.02280E-2 * static_cast<float>(data[0]);
            }
        }
    }

    // The internal FGC (DIM 0) measures the FGC_PSU voltages. Set the FGC.PSU_FGC property and check the limits.

    if(dim_num == 0)
    {
        device.fgc.psu[0] = anasigs[40];    //   +5V    The order for +5V, +/-15V is the same as for FGC2
        device.fgc.psu[1] = anasigs[ 0];    //  +15V
        device.fgc.psu[2] = anasigs[20];    //  -15V
        device.fgc.psu[3] = anasigs[60];    // +3.3V

        // Check limits according to FGC.PSU_FGC documentation

        if(   (device.fgc.psu[0] <   4.8 || device.fgc.psu[0] >   5.3)
           || (device.fgc.psu[1] <  14.7 || device.fgc.psu[1] >  15.5)
           || (device.fgc.psu[2] < -15.5 || device.fgc.psu[2] > -14.7)
           || (device.fgc.psu[3] <   3.1 || device.fgc.psu[3] >   3.5))
        {
            if(getStatusBit(device.status.st_latched,FGC_LAT_PSU_V_FAIL) == false)
            {
                logPrintf(channel, "FGC.PSU_FGC out of limits (4.8 < %.2f < 5.3, 14.7 < %.2f < 15.5, -15.5 < %.2f < -14.7, 3.1 < %.2f < 3.5)\n",
                            device.fgc.psu[0],
                            device.fgc.psu[1],
                            device.fgc.psu[2],
                            device.fgc.psu[3]);
            }

            latchStatusBit(&device.status.st_latched, FGC_LAT_PSU_V_FAIL, true);
        }
    }

    /*
     *  Digital channels : DIG0..DIG1, 64 elements: data[0x80..0xBF]
     *
     * For DIM bus A (the only bus used with FGClite), the 12-bit banks of digital inputs are stored in data[idx_A] where:
     *
     *  idx_A = 0x80 + dim_num + 0x20 * dig_chan
     *
     * where dig_chan = 0..1 and dim_address = 0..15.
     *
     * On FGC2/3, the bus B DIMs have an offset of 0x10:
     *
     *  idx_B = 0x90 + dim_num + 0x20 * dig_chan
     */

    for(uint32_t dig_channel = 0; dig_channel < 2; ++dig_channel)
    {
        data_idx = 0x80 + dim_num + dig_channel * 0x20;

        data[data_idx] = getUint12(dim.digital[dig_channel]);
    }

    // Software channels : 64 elements: data[0xC0..0xFF]
    //
    // SWTIMING1: idx = 0xC0 + data_index
    // SWTIMING2: idx = 0xE0 + data_index
    //
    // These channels are not related to DIMs. On FGC2, they provide debugging information
    // on the MCU CPU consumption. They are not used with FGClite.

    // Diagnostic channels : DIM COUNTERS, 32 elements: data[0x100..0x11F]

    data_idx = 0x100 + dim_num;

    data[data_idx] = getUint12(dim.counter);

    // Latched digital channels : LATCHED DIG1..LATCHED DIG2, 64 elements: data[0x120..0x15F]

    data_idx = 0x120 + dim_num;

    if(dimStateIsLatched(channel, dim_num))
    {
        // Store the latched digital data for DIG0 and DIG1 banks

        data[data_idx]        = data[data_idx - 0xA0];
        data[data_idx + 0x20] = data[data_idx - 0x80];
    }
    else if(!dimStateIsUnlatched(channel, dim_num))
    {
        // Clear the latched digital data

        data[data_idx]        = 0;
        data[data_idx + 0x20] = 0;
    }
}




// Read DIM bus data from FGClite paged memory

static void dimBusReadPage(uint8_t                        const channel,
                           uint8_t                        const dim_page_num,
                           bool                           const is_initial_scan,
                           struct FGClite_status const  * const status)
{
    struct DIM_bus &dbus            = dim_bus[channel];
    struct Equipdev_channel &device = equipdev.device[channel];

    if(dim_page_num >  DIM_PAGE_MAX)
    {
        fprintf(stderr,"dimBusReadPage: ch=%u dpn=%u iis=%u\n",
            channel, dim_page_num, is_initial_scan);
    }

    CC_STATIC_ASSERT(FGC_MAX_REAL_DIMS == DIM_DEVS_PER_PAGE*(DIM_PAGE_MAX+1),FGC_MAX_REAL_DIMS_mismatch);

    for(uint32_t i = 0; i < DIM_DEVS_PER_PAGE; ++i)
    {
        uint32_t dim_bus_addr = (dim_page_num * DIM_DEVS_PER_PAGE) + i;

        assert(dim_bus_addr <  FGC_MAX_DIMS);

        uint32_t const dim_num = device.diag.dim_num[dim_bus_addr];

        assert(dim_num < FGC_MAX_DIMS);

        // Copy the data page into memory

        memcpy(&dbus.dim_bus.dim[dim_num], status->paged + i*sizeof(FGClite_dim),  sizeof(FGClite_dim));

        // On the first scan, register detected DIMs

        if(is_initial_scan)
        {
            dimBusRegisterDevice(channel, dim_num);

#ifdef DEBUG
            char   buf[200];
            uint32_t len = sprintf(buf,"DIM ADDR %02u NUM %02u Bytes = ", dim_bus_addr, dim_num);

            for(uint32_t j=0; j < sizeof(FGClite_dim); ++j)
            {
                len += sprintf(&buf[len],"%02X ",*(status->paged + i*sizeof(FGClite_dim) + j));
            }

            buf[len++] = '\n';
            buf[len]   = '\0';

            logPrintf(channel, buf);

            logPrintf(channel, "DIM ADDR %02d NUM %02d DIGITAL %04X %04X ANALOGUE %04X %04X %04X %04X COUNTER %04X VERSION %02X -- %s",
                      dim_bus_addr, dim_num,
                      dbus.dim_bus.dim[dim_num].digital[0],
                      dbus.dim_bus.dim[dim_num].digital[1],
                      dbus.dim_bus.dim[dim_num].analogue[0],
                      dbus.dim_bus.dim[dim_num].analogue[1],
                      dbus.dim_bus.dim[dim_num].analogue[2],
                      dbus.dim_bus.dim[dim_num].analogue[3],
                      dbus.dim_bus.dim[dim_num].counter,
                      dbus.dim_bus.dim[dim_num].version,
                      dbus.device_mask & 1 << dim_num ? "PRESENT\n" : "ABSENT\n");
#endif
        }

        if(dbus.device_mask & (1 << dim_num))
        {
            State_DIM_trigger dim_state;
            struct timeval    timestamp;

            // Update properties for registered devices which have provided fresh data

            dimBusUpdateData(channel, dim_num);

            // Check if the DIM has triggered

            if((dim_state = dimStateCheckTrigger(channel, dim_num, getUint12(dbus.dim_bus.dim[dim_num].counter), &timestamp)) != DIM_RUNNING)
            {
                // Continue to log changes to DIM status as long as the DIM is in POLLING state

                codesDimsEvtLog(channel, dim_num, dim_state == DIM_TRIGGERED, &timestamp, dbus.dim_bus.dim[dim_num].digital, dbus.digital_mask[dim_num]);
            }
        }
    }
}



// Increment the page number to the next page that contains a DIM

static bool dimBusNextPage(uint32_t channel, bool increment)
{
    struct DIM_bus &dbus = dim_bus[channel];

    if(increment) dbus.dim_state.page_num++;

    while(!((dbus.device_mask >> (dbus.dim_state.page_num * DIM_DEVS_PER_PAGE)) & 0xF) &&
          dbus.dim_state.page_num <= DIM_PAGE_MAX)
    {
        dbus.dim_state.page_num++;
    }

    return dbus.dim_state.page_num <= DIM_PAGE_MAX;
}



static void dimBusWriteCompositeDims(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    for(uint8_t dim_num=0; dim_num != FGC_MAX_DIMS; ++dim_num)
    {
        uint32_t comp_bus_addr = device.diag.bus_address[dim_num];

        if(comp_bus_addr != 0xFF &&
           comp_bus_addr & CODES_COMPOSITE_BUS_ADDR_MASKS)
        {
            //! If the bus address is used and it is a composite DIMs

            uint32_t comp_dim_idx     = (comp_bus_addr & 0x1F);      // DIM index of first DIM to combine
            uint16_t comp_dim_channel = (comp_bus_addr >> 5) & 0x03; // Ana chan to combine from 4 consecutive DIMs

            for(uint32_t dim_channel=0; dim_channel < FGC_N_DIM_ANA_CHANS; dim_channel++, comp_dim_idx++)
            {
                uint32_t dim_num      = SysDbDimLogicalAddress(codesGetSysdbIdx(channel), comp_dim_idx);
                uint32_t comp_ana_idx = dim_num + comp_dim_channel * 20;
                uint32_t ana_idx      = dim_num + dim_channel * 20;

                if(comp_ana_idx < FGC_N_DIAG_ANA_CHANS &&
                   ana_idx      < FGC_N_DIAG_ANA_CHANS)
                {
                    device.diag.anasigs[comp_ana_idx] = device.diag.anasigs[ana_idx];
                }
                else
                {
                    logPrintf(channel, "DIMBUS Analog index out of bounds (comp_ana_idx=%u, ana_idx%u)\n", comp_ana_idx, ana_idx);
                }
            }
        }

    }
}


static bool dimBusHasDimWarnings(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    bool any_warning = false;

    // Go over all DIMs

    for (uint8_t dim_num = 0; dim_num != FGC_MAX_DIMS; ++dim_num)
    {
        struct FGClite_dim const &dim = dim_bus[channel].dim_bus.dim[dim_num];

        // Each DIM has 2 banks x 12 digital signals

        for (uint32_t dig_channel = 0; dig_channel < FGC_N_DIM_DIG_BANKS; ++dig_channel)
        {
            // We do not report _which_ warnings are set, only that _any_ are set
            // (unless DIM_WRN_DEBUG has been selected -- then we log all transitions)

            auto warnings = (getUint12(dim.digital[dig_channel]) &
                             device.diag.warning_masks[dim_num][dig_channel]);

            if (warnings != 0) {
                any_warning = true;
            }

#ifdef DIM_WRN_DEBUG
            auto new_warnings = warnings & ~device.diag.warning_state[dim_num][dig_channel];

            // If verbose output is required, iterate over all signals in the channel

            for (int input_idx = 0; input_idx < FGC_N_DIM_DIG_INPUTS; input_idx++) {
                auto prev_state = (device.diag.warning_state[dim_num][dig_channel] & (1 << input_idx)) != 0;
                auto new_state = (warnings & (1 << input_idx)) != 0;

                if (new_state != prev_state) {
                    logPrintf(channel, "DIM Warning change: dim %d, DIG chan %d, index %d, state %d->%d\n",
                              dim_num, dig_channel, input_idx, prev_state, new_state);
                }
            }
#endif

            equipdev.device[channel].diag.warning_state[dim_num][dig_channel] = warnings;
        }



    }

    return any_warning;
}

// External functions

uint16_t dimBusActiveDimsBitmask(uint32_t channel)
{
    return dim_bus[channel].device_mask;
}



void dimBusClearDigitalMask(uint32_t channel, uint8_t dim_num)
{
    struct DIM_bus &dbus = dim_bus[channel];

    dbus.digital_mask[dim_num][0] = dbus.digital_mask[dim_num][1] = 0;
}



bool dimBusIsScanComplete(uint32_t channel)
{
    return dim_bus[channel].dim_state.is_scan_complete;
}



void dimBusFullScan(uint32_t                      channel,
                    struct FGClite_status const * const status,
                    bool                          const request_scan,
                    struct FGClite_command      * const ctrl_cmd)
{
    struct DIM_bus &dbus = dim_bus[channel];

    // Read DIM data page if a read is in progress

    if(dbus.dim_state.read_in_progress == true)
    {
        dimBusReadPage(channel, dbus.dim_state.page_num++, true, status);
    }

    // If a new scan was requested, set state to start the scan

    if(request_scan == true)
    {
        logPrintf(channel, "DIM_BUS Starting full scan of DIM bus\n");

        dbus.dim_state.read_in_progress = true;
        dbus.dim_state.is_scan_complete = false;
        dbus.dim_state.page_num          = 0;
        dbus.device_mask                 = 0;
    }

    if(dbus.dim_state.page_num <= DIM_PAGE_MAX)
    {
        // Request the next page

        fgcliteCmdRequestCtrl(ctrl_cmd, channel, DIM_BUS, dbus.dim_state.page_num);
    }
    else
    {
        // All pages read - update the summary info for the DIM bus

        dimBusUpdateProps(channel);

        dbus.dim_state.read_in_progress = false;
        dbus.dim_state.is_scan_complete = true;

        logPrintf(channel, "DIM_BUS Full scan of DIM bus complete\n");
    }
}



void dimBusScan(uint32_t                      const channel,
                struct FGClite_status const * const status,
                bool                        * const request_scan,
                bool                        * const ctrl_cmd_set,
                struct FGClite_command      * const ctrl_cmd)
{
    struct DIM_bus &dbus = dim_bus[channel];

    // If a new scan was requested, set state to start the scan

    if(*ctrl_cmd_set == false && *request_scan == true)
    {
        *request_scan          = false;
        dbus.dim_state.page_num = 0;

        if(dimBusNextPage(channel, false) == false)
        {
            return;
        }

        // Request the first page

        dbus.dim_state.read_in_progress = true;
        dbus.dim_state.is_data_pending  = true;
        *ctrl_cmd_set                   = true;

        fgcliteCmdRequestCtrl(ctrl_cmd, channel, DIM_BUS, dbus.dim_state.page_num);

        return;
    }
    else if(dbus.dim_state.read_in_progress == false)
    {
        return;
    }

    if(dbus.dim_state.is_data_pending == true)
    {
        // Read out the page requested in the last cycle

        dimBusReadPage(channel, dbus.dim_state.page_num, false, status);

        dbus.dim_state.is_data_pending = false;
    }

    if(!*ctrl_cmd_set && dimBusNextPage(channel, true))
    {
        // Request the next page

        dbus.dim_state.is_data_pending = true;
        *ctrl_cmd_set                  = true;

        fgcliteCmdRequestCtrl(ctrl_cmd, channel, DIM_BUS, dbus.dim_state.page_num);

        return;
    }

    if(dimBusNextPage(channel, false) == false)
    {
        // There are no more pages to read, terminate the scan

        dbus.dim_state.read_in_progress = false;

        // If the scan is terminated update the composite DIMs

        dimBusWriteCompositeDims(channel);

        // Check Diag Digital Signals

        struct Equipdev_channel &device = equipdev.device[channel];

        dimBusCheckDiagDigSig(channel, &device.diag.fw_diode);
        dimBusCheckDiagDigSig(channel, &device.diag.fabort_unsafe);
        dimBusCheckDiagDigSig(channel, &device.diag.thyr_unsafe);
        dimBusCheckDiagDigSig(channel, &device.diag.subconv_lost);

        // Check warnings

        setStatusBit(&device.status.st_unlatched, FGC_UNL_DIM_WRN, dimBusHasDimWarnings(channel) == true);
    }
}



static void dimBusCheckDiagDigSig(uint32_t channel, struct Equipdev_channel_diag_dig_sig * diag_dig_sig)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(diag_dig_sig->fault_mask == 0)
    {
        // Input is not associated with a fault, so set or clear the warning according to the Diag Dig Sig state

        if(   device.status.state_op == FGC_OP_NORMAL
           && dimBusGetDiagDigSig(diag_dig_sig) == true)
        {
            setStatusBit(&device.status.st_warnings, diag_dig_sig->warning_mask, true);
        }
        else
        {
            setStatusBit(&device.status.st_warnings, diag_dig_sig->warning_mask, false);
        }

        return;
    }

    // Run the diag digital signal state machine for inputs associated with a fault

    switch(diag_dig_sig->state)
    {
        default:    // FGC_VDI_NOT_PRESENT or FGC_VDI_FAULT - do nothing

            return;

        case FGC_VDI_RESET:

            // Stay in RESET while not simulating and the signal is true

            if(   device.status.state_op != FGC_OP_SIMULATION
               && dimBusGetDiagDigSig(diag_dig_sig) == true)
            {
                return;
            }

            // Fall through to NO_FAULT

            diag_dig_sig->state = FGC_VDI_NO_FAULT;

        case FGC_VDI_NO_FAULT:

            if(device.status.state_op == FGC_OP_NORMAL)
            {
               if(dimBusGetDiagDigSig(diag_dig_sig) == true)
               {
                    if(++diag_dig_sig->filter_count > 5)
                    {
                        diag_dig_sig->filter_count = 0;
                        diag_dig_sig->state = FGC_VDI_FAULT;
                        setStatusBit(&device.status.st_faults, diag_dig_sig->fault_mask, true);
                        return;
                    }
               }
               else
               {
                    diag_dig_sig->filter_count = 0;
               }
            }

            // Reset the associated fault and warning

            setStatusBit(&device.status.st_faults, diag_dig_sig->fault_mask, false);

            break;
    }
}



static bool dimBusGetDiagDigSig(struct Equipdev_channel_diag_dig_sig * diag_dig_sig)
{
    const uint16_t num_sigs   = diag_dig_sig->num_sigs;
    uint16_t       true_count = 0;

    for(uint16_t sig_idx = 0 ; sig_idx < num_sigs ; sig_idx++)
    {
        true_count += (*diag_dig_sig->dim_bits_ptr[sig_idx] & diag_dig_sig->dim_bits_mask[sig_idx]) != 0;
    }

    return  diag_dig_sig->logic == DIAG_DIG_SIG_LOGIC_OR
          ? true_count > 0
          : true_count > 0 && true_count < num_sigs;
}

// EOF
