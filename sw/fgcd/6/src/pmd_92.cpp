//! @file   pmd_92.cpp
//! @brief  Handle FGC class 92 post-mortem data


#include <arpa/inet.h>
#include <cstdint>
#include <cstring>
#include <pm-dc-rda3-lib/PMClient.hpp>
#include <pm-dc-rda3-lib/PMData.hpp>
#include <pm-dc-rda3-lib/PMError.hpp>

#include <classes/92/evtlogStructs.h>
#include <classes/92/logMenus.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <logging.h>
#include <page_mgr.h>
#include <pm.h>
#include <classes/92/pm_buf_92.h>
#include <pub.h>
#include <rt.h>
#include <sub_cmwdata_gen.h>
#include <sub_utilities.h>


// Static asserts

static_assert(PM_BUF_92_SIZE < PM_BUF_MAX_SIZE);


// Global variables

const uint8_t     MAX_SUB_STRING_LENGTH = 255;                                                                   //!< Maximum length of a post-mortem string
const uint32_t    PM_STATUS_MAX_BUF_LEN = (PM_SELF_BUF_LEN_92>PM_EXT_BUF_LEN)?PM_SELF_BUF_LEN_92:PM_EXT_BUF_LEN; //!< Maximum size of the status buffer length


// PMD Ext Auxiliary arrays for the status arrays

struct pmd_92_status_arrays_t
{
    uint32_t  num_samples;
    int64_t   timestamps[PM_STATUS_MAX_BUF_LEN];
    int       class_id[PM_STATUS_MAX_BUF_LEN];
    char      data_status[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     data_status_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_faults[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_faults_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_warnings[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_warnings_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_latched[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_latched_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_unlatched[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_unlatched_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_op[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_op_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_vs[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_vs_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_pc[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_pc_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_meas_a[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_meas_a_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_meas_b[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_meas_b_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_dcct_a[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_dcct_a_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_dcct_b[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_dcct_b_ptr[PM_STATUS_MAX_BUF_LEN];
    int16_t   i_err_ma[PM_STATUS_MAX_BUF_LEN];
    int16_t   i_diff_ma[PM_STATUS_MAX_BUF_LEN];
    float     i_earth_pcnt[PM_STATUS_MAX_BUF_LEN];
    float     i_meas[PM_STATUS_MAX_BUF_LEN];
    float     i_ref[PM_STATUS_MAX_BUF_LEN];
    float     v_meas[PM_STATUS_MAX_BUF_LEN];
    float     v_ref[PM_STATUS_MAX_BUF_LEN];

} pm_status_arrays;


// Static functions

static void pmd_92AddStatus(PMData& pmdata, int64_t timestamp, const struct fgc_stat* trigger, const struct PM_data* status, uint32_t LEN)
{
    const fgc92_stat *c92 = &trigger->class_data.c92;

    // Parameters: Trigger data

    pmdata.registerParameter("TIMESTAMP", timestamp);

    subUtilitiesPmdBitmaskToText(trigger->data_status,sym_names_92_data_status,pm_status_arrays.data_status[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("DATA_STATUS",pm_status_arrays.data_status[0]);

    pmdata.registerParameter("CLASS_ID", (int32_t)trigger->class_id);

    subUtilitiesPmdBitmaskToText(ntohs(c92->st_faults),sym_names_92_flt,pm_status_arrays.st_faults[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_FAULTS",pm_status_arrays.st_faults[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c92->st_warnings),sym_names_92_wrn,pm_status_arrays.st_warnings[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_WARNINGS",pm_status_arrays.st_warnings[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c92->st_latched),sym_names_92_lat,pm_status_arrays.st_latched[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_LATCHED",pm_status_arrays.st_latched[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c92->st_unlatched),sym_names_92_unl,pm_status_arrays.st_unlatched[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_UNLATCHED",pm_status_arrays.st_unlatched[0]);

    subUtilitiesPmdEnumToText(c92->state_op,sym_names_92_op,pm_status_arrays.state_op[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_OP",pm_status_arrays.state_op[0]);

    subUtilitiesPmdEnumToText(c92->state_vs,sym_names_92_vs,pm_status_arrays.state_vs[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_VS",pm_status_arrays.state_vs[0]);

    subUtilitiesPmdEnumToText(c92->state_pc,sym_names_92_pc,pm_status_arrays.state_pc[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_PC",pm_status_arrays.state_pc[0]);

    subUtilitiesPmdBitmaskToText(c92->st_meas_a,sym_names_92_meas,pm_status_arrays.st_meas_a[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_MEAS_A",pm_status_arrays.st_meas_a[0]);

    subUtilitiesPmdBitmaskToText(c92->st_meas_b,sym_names_92_meas,pm_status_arrays.st_meas_b[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_MEAS_B",pm_status_arrays.st_meas_b[0]);

    subUtilitiesPmdBitmaskToText(c92->st_dcct_a,sym_names_92_dcct,pm_status_arrays.st_dcct_a[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_DCCT_A",pm_status_arrays.st_dcct_a[0]);

    subUtilitiesPmdBitmaskToText(c92->st_dcct_b,sym_names_92_dcct,pm_status_arrays.st_dcct_b[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_DCCT_B",pm_status_arrays.st_dcct_b[0]);

    pmdata.registerParameter("I_ERR_MA",subUtilitiesSignedNtohs(c92->i_err_ma));
    pmdata.addUnits("I_ERR_MA","mA");

    pmdata.registerParameter("I_DIFF_MA",subUtilitiesSignedNtohs(c92->i_diff_ma));
    pmdata.addUnits("I_DIFF_MA","mA");

    pmdata.registerParameter("I_EARTH_PCNT",0.01F * (float)subUtilitiesSignedNtohs(c92->i_earth_cpcnt));
    pmdata.addUnits("I_EARTH_PCNT","%");

    pmdata.registerParameter("I_MEAS",subUtilitiesNtohf(c92->i_meas));
    pmdata.addUnits("I_MEAS","A");

    pmdata.registerParameter("I_REF",subUtilitiesNtohf(c92->i_ref));
    pmdata.addUnits("I_REF","A");

    pmdata.registerParameter("V_MEAS",subUtilitiesNtohf(c92->v_meas));
    pmdata.addUnits("V_MEAS","V");

    pmdata.registerParameter("V_REF",subUtilitiesNtohf(c92->v_ref));
    pmdata.addUnits("V_REF","V");

    // STATUS Signal

    memset(&pm_status_arrays,0,sizeof(pm_status_arrays));

    uint32_t &num_samples = pm_status_arrays.num_samples;

    num_samples = 0;

    for(uint32_t i = 0; i < LEN; i++)
    {
        const PM_data *data = &status[i];
        c92 = &data->status.class_data.c92;

        // Ignore missing status data

        if(!data->time_sec) continue;

        // Fill arrays

        pm_status_arrays.timestamps[num_samples] = static_cast<int64_t>(ntohl(data->time_sec))*1000*1000*1000 + static_cast<int64_t>(ntohl(data->time_usec))*1000;

        subUtilitiesPmdBitmaskToText(data->status.data_status,sym_names_92_data_status,pm_status_arrays.data_status[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.data_status_ptr[num_samples] = pm_status_arrays.data_status[num_samples];

        pm_status_arrays.class_id[num_samples]  = data->status.class_id;

        subUtilitiesPmdBitmaskToText(ntohs(c92->st_faults),sym_names_92_flt,pm_status_arrays.st_faults[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_faults_ptr[num_samples] = pm_status_arrays.st_faults[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c92->st_warnings),sym_names_92_wrn,pm_status_arrays.st_warnings[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_warnings_ptr[num_samples] = pm_status_arrays.st_warnings[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c92->st_latched),sym_names_92_lat,pm_status_arrays.st_latched[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_latched_ptr[num_samples] = pm_status_arrays.st_latched[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c92->st_unlatched),sym_names_92_unl,pm_status_arrays.st_unlatched[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_unlatched_ptr[num_samples] = pm_status_arrays.st_unlatched[num_samples];

        subUtilitiesPmdEnumToText(c92->state_op,sym_names_92_op,pm_status_arrays.state_op[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_op_ptr[num_samples] = pm_status_arrays.state_op[num_samples];

        subUtilitiesPmdEnumToText(c92->state_vs,sym_names_92_vs,pm_status_arrays.state_vs[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_vs_ptr[num_samples] = pm_status_arrays.state_vs[num_samples];

        subUtilitiesPmdEnumToText(c92->state_pc,sym_names_92_pc,pm_status_arrays.state_pc[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_pc_ptr[num_samples] = pm_status_arrays.state_pc[num_samples];

        subUtilitiesPmdBitmaskToText(c92->st_meas_a,sym_names_92_meas,pm_status_arrays.st_meas_a[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_meas_a_ptr[num_samples] = pm_status_arrays.st_meas_a[num_samples];

        subUtilitiesPmdBitmaskToText(c92->st_meas_b,sym_names_92_meas,pm_status_arrays.st_meas_b[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_meas_b_ptr[num_samples] = pm_status_arrays.st_meas_b[num_samples];

        subUtilitiesPmdBitmaskToText(c92->st_dcct_a,sym_names_92_dcct,pm_status_arrays.st_dcct_a[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_dcct_a_ptr[num_samples] = pm_status_arrays.st_dcct_a[num_samples];

        subUtilitiesPmdBitmaskToText(c92->st_dcct_b,sym_names_92_dcct,pm_status_arrays.st_dcct_b[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_dcct_b_ptr[num_samples] = pm_status_arrays.st_dcct_b[num_samples];

        pm_status_arrays.i_err_ma[num_samples] = subUtilitiesSignedNtohs(c92->i_err_ma);
        pm_status_arrays.i_diff_ma[num_samples] = subUtilitiesSignedNtohs(c92->i_diff_ma);
        pm_status_arrays.i_earth_pcnt[num_samples] = 0.01F * (float)subUtilitiesSignedNtohs(c92->i_earth_cpcnt);
        pm_status_arrays.i_meas[num_samples] = subUtilitiesNtohf(c92->i_meas);
        pm_status_arrays.i_ref[num_samples] = subUtilitiesNtohf(c92->i_ref);
        pm_status_arrays.v_meas[num_samples] = subUtilitiesNtohf(c92->v_meas);
        pm_status_arrays.v_ref[num_samples] = subUtilitiesNtohf(c92->v_ref);

        // Increase number of samples counter counter

        num_samples++;
    }

    // Register timestamp

    pmdata.registerArray("STATUS.TIMESTAMP",pm_status_arrays.timestamps,num_samples);

    // Register arrays and associate them to the STATUS signal and the STATUS.TIMESTAMP

    pmdata.registerArray("STATUS.CLASS_ID",pm_status_arrays.class_id,num_samples);
    pmdata.addAttribute("STATUS.CLASS_ID","timescale","STATUS");
    pmdata.addAttribute("STATUS.CLASS_ID","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.DATA_STATUS",const_cast<const char **>(pm_status_arrays.data_status_ptr),num_samples);
    pmdata.addAttribute("STATUS.DATA_STATUS","timescale","STATUS");
    pmdata.addAttribute("STATUS.DATA_STATUS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_FAULTS",const_cast<const char **>(pm_status_arrays.st_faults_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_FAULTS","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_FAULTS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_WARNINGS",const_cast<const char **>(pm_status_arrays.st_warnings_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_WARNINGS","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_WARNINGS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_LATCHED",const_cast<const char **>(pm_status_arrays.st_latched_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_LATCHED","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_LATCHED","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_UNLATCHED",const_cast<const char **>(pm_status_arrays.st_unlatched_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_UNLATCHED","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_UNLATCHED","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_OP",const_cast<const char **>(pm_status_arrays.state_op_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_OP","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_OP","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_VS",const_cast<const char **>(pm_status_arrays.state_vs_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_VS","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_VS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_PC",const_cast<const char **>(pm_status_arrays.state_pc_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_PC","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_PC","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_MEAS_A",const_cast<const char **>(pm_status_arrays.st_meas_a_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_MEAS_A","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_MEAS_A","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_MEAS_B",const_cast<const char **>(pm_status_arrays.st_meas_b_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_MEAS_B","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_MEAS_B","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_DCCT_A",const_cast<const char **>(pm_status_arrays.st_dcct_a_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_DCCT_A","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_DCCT_A","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_DCCT_B",const_cast<const char **>(pm_status_arrays.st_dcct_b_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_DCCT_B","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_DCCT_B","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.I_ERR_MA",pm_status_arrays.i_err_ma,num_samples);
    pmdata.addAttribute("STATUS.I_ERR_MA","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_ERR_MA","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_ERR_MA","mA");

    pmdata.registerArray("STATUS.I_DIFF_MA",pm_status_arrays.i_diff_ma,num_samples);
    pmdata.addAttribute("STATUS.I_DIFF_MA","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_DIFF_MA","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_DIFF_MA","mA");

    pmdata.registerArray("STATUS.I_EARTH_PCNT",pm_status_arrays.i_earth_pcnt,num_samples);
    pmdata.addAttribute("STATUS.I_EARTH_PCNT","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_EARTH_PCNT","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_EARTH_PCNT","%");

    pmdata.registerArray("STATUS.I_MEAS",pm_status_arrays.i_meas,num_samples);
    pmdata.addAttribute("STATUS.I_MEAS","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_MEAS","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_MEAS","A");

    pmdata.registerArray("STATUS.I_REF",pm_status_arrays.i_ref,num_samples);
    pmdata.addAttribute("STATUS.I_REF","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_REF","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_REF","A");

    pmdata.registerArray("STATUS.V_MEAS",pm_status_arrays.v_meas,num_samples);
    pmdata.addAttribute("STATUS.V_MEAS","timescale","STATUS");
    pmdata.addAttribute("STATUS.V_MEAS","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.V_MEAS","V");

    pmdata.registerArray("STATUS.V_REF",pm_status_arrays.v_ref,num_samples);
    pmdata.addAttribute("STATUS.V_REF","timescale","STATUS");
    pmdata.addAttribute("STATUS.V_REF","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.V_REF","V");
}



void pmd_92GenerateAndSendExt(pm::Client &pmclient, struct FGCD_device *device, const struct PM_ext_buffer &buffer, int64_t timestamp)
{
    try
    {
        // Create PMData object

        PMData pmdata(PM_SYSTEM_NAME, "lhc_ext_pmd", device->name);

        // Add trigger and status data

        pmd_92AddStatus(pmdata, timestamp, &buffer.trigger, buffer.status, PM_EXT_BUF_LEN);

        // Send PMD Data

        logPrintf(device->id,
                  "PM Sending externally-triggered PM (system=%s, class=%s, source=%s, timestamp=%lld)...\n",
                  PM_SYSTEM_NAME, "lhc_self_pmd", device->name, timestamp);

        pmclient.send(pmdata, timestamp);
    }
    catch(PMError& e)
    {
       logPrintf(device->id, "PM PMD Generation and sending threw PMError: %s\n", e.what());
       fgcddev.status.st_faults |= FGC_FLT_PM;
    }
    catch(std::exception& e)
    {
        logPrintf(device->id, "PM PMD Generation and sending threw std::exception: %s\n", e.what());
        fgcddev.status.st_faults |= FGC_FLT_PM;
    }
}



void pmd_92GenerateAndSendSelf(pm::Client &pmclient, struct FGCD_device *device, const struct PM_self_buffer_std &buffer, int64_t timestamp)
{
    try
    {
        // Create PMData object

        PMData pmdata(PM_SYSTEM_NAME, "lhc_self_pmd", device->name);

        // Add trigger and status data

        pmd_92AddStatus(pmdata, timestamp, &buffer.trigger, buffer.status, PM_SELF_BUF_LEN_92);

        // Add FGC_92 buffers

        pmd_92AddFgc(pmdata);

        // Send PMD Data

        logPrintf(device->id,
                  "PM Sending self-triggered PM (system=%s, class=%s, source=%s, timestamp=%lld)...\n",
                  PM_SYSTEM_NAME, "lhc_self_pmd", device->name, timestamp);

        pmclient.send(pmdata, timestamp);
    }
    catch(PMError& e)
    {
       logPrintf(device->id, "PM PMD Generation and sending threw PMError: %s\n", e.what());
       fgcddev.status.st_faults |= FGC_FLT_PM;
    }
    catch(std::exception& e)
    {
        logPrintf(device->id, "PM PMD Generation and sending threw std::exception: %s\n", e.what());
        fgcddev.status.st_faults |= FGC_FLT_PM;
    }
}

// EOF
