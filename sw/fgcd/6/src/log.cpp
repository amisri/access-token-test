//! @file   cc_log.cpp
//! @brief  Interface to liblog
//! @author Michael Davis
//!
//! Initialise the CCLIBS log manager structures and instantiate LOG properties


//#define DEBUG

#include <cstring>
#include <limits>
#include <consts.h>
#include <codes.h>
#include <fgcd.h>
#include <parser.h>
#include <cmd.h>
#include <fgc/fgc_db.h>
#include <dim_state.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <fgclite_cmd.h>
#include <logging.h>
#include <timing.h>
#include <equip_liblog.h>

// Logging state for each device

static struct CC_log_state
{
    struct CC_us_time     adc_timestamp;                          // Timestamp for the last sample in the ADC log
    int32_t               adc_sample_count;                       // Countdown of the number of ADC samples to be logged

    struct CC_us_time     dim_timestamp    [LOG_DIM_NUM_LOGS];    // Timestamp for the last sample in the ADC log
    int32_t               dim_sample_count [LOG_DIM_NUM_LOGS];    // Countdown of the number of DIM samples to be logged for each DIM

} cc_log_state[FGCD_MAX_DEVS] = {};


// Static asserts

static_assert(LOG_DIM_NUM_LOGS == FGC_MAX_DIMS);

// Static functions

// Convert a 12-bit little-endian unsigned integer into a 16-bit unsigned integer

static uint16_t getInt12(uint8_t const * ptr)
{
    uint32_t result;

    result  = ptr[0] | (ptr[1] << 8);

    return (result & 0x0FFF);
}



// Convert a 24-bit little-endian signed integer into a 32-bit signed integer

static int32_t getInt24(uint8_t const * ptr)
{
    // Unpack three bytes from *ptr

    int32_t result = ptr[0] | (ptr[1] << 8) | (ptr[2] << 16);

    // Sign extend the 24-bit value to become a signed 32-bit value

    if((result & 0x00800000) != 0)
    {
        result |= 0xFF000000;
    }

    return result;
}


// Update MEAS.U_LEADS with the 1-second average, and MEAS.MAX.U_LEADS with the maximum absolute
// value. MEAS.MAX.U_LEADS are reset when the converter starts and when the property is read with
// the ZERO get option.

static void ccLogAverageULeads(uint32_t channel, float period)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    for(uint32_t i = 0; i < 2; ++i)
    {
        // Calculate the 1-second average and reset accumulator

        device.meas.u_leads[i] = device.meas.u_leads_acc[i] / period;

        device.meas.u_leads_acc[i] = 0;

        // Check if we exceeded MEAS.MAX.U_LEADS

        if(fabs(device.meas.u_leads[i]) > device.meas.max_u_leads[i])
        {
            device.meas.max_u_leads[i] = fabs(device.meas.u_leads[i]);
        }
    }
}



static void ccLogStartDiscontinuous(uint32_t channel, struct LOG_log *log, uint32_t sample_count, uint32_t ms_per_sample, struct CC_us_time *cc_timestamp, int32_t time_offset_ms)
{
    // Get timestamp for the first sample in the log

    struct timeval timestamp;

    timingGetUserTime(0, &timestamp);

    struct CC_us_time iter_us_time = { { timestamp.tv_sec }, timestamp.tv_usec };

    iter_us_time = cctimeUsAddOffsetRT(iter_us_time, time_offset_ms * 1000);

    // Start the log

    struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(iter_us_time);

    logStoreStartDiscontinuousRT(log, iter_ns_time, iter_ns_time, 0);

    // Store the timestamp of the last sample that will be logged

    *cc_timestamp = cctimeUsAddOffsetRT(iter_us_time, (sample_count-1) * 1000 * ms_per_sample);
}



// External functions

void logInitClass(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialise DIM log menu to be DISABLED and with empty names

    for(uint32_t dim_idx = 0; dim_idx < LOG_DIM_NUM_LOGS; ++dim_idx)
    {
        logMenuDisable   (&device.log.menus, LOG_MENU_DIM + dim_idx);
        logChangeMenuName(&device.log.menus, LOG_MENU_DIM + dim_idx, "");
    }

    // Initialise the log menu status

    logMenuUpdate(&device.log.mgr, &device.log.menus);
}



void ccLogSetDimMenuName(uint32_t channel, uint32_t dim_idx, const char * const menu_name)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Change the log menu name to be the name from the DIM_DB and enable the log menu

    logChangeMenuName(&device.log.menus, LOG_MENU_DIM + dim_idx, menu_name);
    logMenuEnable    (&device.log.menus, LOG_MENU_DIM + dim_idx);
}



void ccLogIteration(uint32_t channel, const struct CC_us_time *iter_us_time)
{
    // This function is called once after each REF iteration, i.e. at 100 Hz. Data which is logged at
    // a slower rate than this will not be logged every time the function is called. The 1 KHz log
    // data is handled elsewhere.

    struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(*iter_us_time);
    const uint32_t  US_20_MS    = 20000;                        // 20ms in units of microseconds

    struct Equipdev_channel &device = equipdev.device[channel];
    struct LOG_log * const   log    = device.log.structs.log;

    // Data to be logged at 100 Hz

    ccLogSetDebugSignals(channel, iter_us_time->us);

    logStoreContinuousRT(&log[LOG_ACQ],      iter_ns_time);
    logStoreContinuousRT(&log[LOG_I_MEAS],   iter_ns_time);
    logStoreContinuousRT(&log[LOG_V_REF],    iter_ns_time);
    logStoreContinuousRT(&log[LOG_FAULTS],   iter_ns_time);
    logStoreContinuousRT(&log[LOG_WARNINGS], iter_ns_time);
    logStoreContinuousRT(&log[LOG_FLAGS],    iter_ns_time);

    if(iter_us_time->us % US_20_MS == 0)
    {
        // Update the log menus

        logMenuUpdate(&device.log.mgr, &device.log.menus);

        // Data to be logged at 50 Hz

        logStoreContinuousRT(&log[LOG_I_EARTH], iter_ns_time);

        if(iter_us_time->us == 0)
        {
            // Data to be logged at 1 Hz

            ccLogAverageULeads(channel, 50);

            logStoreContinuousRT(&log[LOG_I_LEADS], iter_ns_time);

            if(iter_us_time->secs.abs % 10 == 0)
            {
                // Data to be logged at 0.1 Hz

                logStoreContinuousRT(&log[LOG_TEMP], iter_ns_time);
            }
        }
    }

    // The RST algorithm runs when the iteration counter is zero.

    if(regMgrVarValue(&device.reg_mgr,IREG_ITER_INDEX) == 0)
    {
        // Data to be logged at the regulation rate.

        logStoreContinuousRT(&log[LOG_I_REG], iter_ns_time);
    }
}



void ccLogCritical(uint32_t channel, const struct FGClite_status *status)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // On FGClite, the first DIM on Bus A is the DIM in the Voltage Source (VS). The four analogue
    // channels from this DIM are exposed in the critical data, so we sample them at 50 Hz instead
    // of 1 Hz (as with the other DIMs).
    //
    // vs_dim_idx is defined in the SysDb and should normally be 1 (DIM Bus A DIM 1). It cannot be
    // zero, as that is the address of the FGC_PSU onboard DIM.
    //
    // The raw values come from a 12-bit uncalibrated ADC (accurate to 8 bits), so we receive them
    // as 12-bit unsigned integers. Note that the byte ordering in the critical data is opposite to
    // the byte ordering in the paged data, see dimBusUpdateData().

    uint32_t &vs_dim_idx = device.diag.vs_dim_idx;

    if(vs_dim_idx != 0)
    {
        device.diag.data[vs_dim_idx + 0x00] = status->critical.dim_a_1_ana_0 & 0x0FFF;
        device.diag.data[vs_dim_idx + 0x20] = status->critical.dim_a_1_ana_1 & 0x0FFF;
        device.diag.data[vs_dim_idx + 0x40] = status->critical.dim_a_1_ana_2 & 0x0FFF;
        device.diag.data[vs_dim_idx + 0x60] = status->critical.dim_a_1_ana_3 & 0x0FFF;

        for(uint32_t dim_channel = 0; dim_channel < 4; ++dim_channel)
        {
            uint32_t raw_idx = vs_dim_idx + dim_channel * 0x20;
            uint32_t ana_idx = vs_dim_idx + dim_channel * 20;

            // Convert raw analogue signals to physical units

            device.diag.anasigs[ana_idx] = static_cast<float>(device.diag.data[raw_idx]) * device.diag.gain[ana_idx] + device.diag.offset[ana_idx];
        }
    }

    if(device.status.state_op != FGC_OP_SIMULATION)
    {
        // MEAS.U_LEADS
        //
        // For the 60A and 120A circuits, U_LEADS contains the measurement of the current lead voltages.
        // These are sampled by the VS DIM at 50Hz, but we publish the 1-second average. If the
        // measurement exceeds 150mV, the converter will trip.

        device.meas.u_leads_acc[U_LEAD_POS] += device.meas.u_leads_ptr[U_LEAD_POS] ? *device.meas.u_leads_ptr[U_LEAD_POS] : 0;
        device.meas.u_leads_acc[U_LEAD_NEG] += device.meas.u_leads_ptr[U_LEAD_NEG] ? *device.meas.u_leads_ptr[U_LEAD_NEG] : 0;

        // MEAS.I.EARTH
        //
        // Almost all power converters include an Earth Fault detection circuit that is monitored at 50Hz
        // by the first DIM on Bus A. The raw value appears in the critical data and is converted to amps
        // above. We set a pointer to the appropriate value in DIAG.ANASIGS in codesDimsInit().

        device.meas.i_earth = device.meas.i_earth_ptr ? *device.meas.i_earth_ptr : 0.0;
    }
    else
    {
        // Simulation mode - create u_leads and i_earth from i_meas signal

        device.meas.u_leads_acc[U_LEAD_POS] += device.status.i_meas * (1.3E-4 / 50.0);
        device.meas.u_leads_acc[U_LEAD_NEG] += device.status.i_meas * (1.5E-4 / 50.0);
        device.meas.i_earth = 0.8 * device.status.i_meas * device.limits.i_earth / regMgrVarValue(&device.reg_mgr,LIMITS_I_POS);
    }

    // MEAS.I.EARTH_PCNT
    //
    // The I_EARTH value is published as a signed short in centi-percent of the trip limit specified
    // in LIMITS.I.EARTH (clipped to -32768 to +32767)

    const int32_t i_earth_cpcnt = ccNearbyint(device.meas.i_earth * 10000.0 / device.limits.i_earth);

    using namespace std;

    device.status.i_earth_cpcnt = i_earth_cpcnt > numeric_limits<int16_t>::max() ? numeric_limits<int16_t>::max() :
                                  i_earth_cpcnt < numeric_limits<int16_t>::min() ? numeric_limits<int16_t>::min() :
                                  static_cast<int16_t>(i_earth_cpcnt);

    // MEAS.MAX.I_EARTH
    //
    // All LHC power converters include an Earth Fault detection circuit that is monitored by the FGC2. The value
    // can be read from MEAS.I_EARTH and this property records the maximum absolute value since the last time that
    // the property was read with the ZERO get option.

    if(fabs(device.meas.i_earth) > device.meas.max_i_earth) device.meas.max_i_earth = fabs(device.meas.i_earth);
}



bool ccLogAdcIsLogging(uint32_t channel)
{
    return cc_log_state[channel].adc_sample_count > 0;
}



void ccLogEndDiscontinuous(uint32_t           const channel,
                           enum LOG_index     const log_idx,
                           int32_t            const sample_count,
                           CC_us_time const * const timestamp)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(sample_count == 0)
    {
#ifdef DEBUG
        logPrintf(channel, "ccLogEndDiscontinuous: %s (%d) finsihed - timestamp=%u.%06u\n",
                log_idx == LOG_ACQ_1KHZ ? "ACQ_1KHZ" : "DIM",
                log_idx == LOG_ACQ_1KHZ ? LOG_ACQ_1KHZ : log_idx - LOG_DIM,
                timestamp->secs.abs, timestamp->us);
#endif
        // Finish the log

        logStoreEndDiscontinuousRT(&device.log.structs.log[log_idx], cctimeUsToNsRT(*timestamp));
    }
    else if(sample_count < 0)
    {
#ifdef DEBUG
        logPrintf(channel, "ccLogEndDiscontinuous: %s (%d) cancelled\n",
                log_idx == LOG_ACQ_1KHZ ? "ACQ_1KHZ" : "DIM",
                log_idx == LOG_ACQ_1KHZ ? LOG_ACQ_1KHZ : log_idx - LOG_DIM);
#endif
        // Something went wrong, abandon this log

        logStoreCancelDiscontinuousRT(&device.log.structs.log[log_idx]);
    }

    // If we are finishing a DIM log, suspend logging until the DIM returns to RUNNING state

    if(sample_count <= 0 && log_idx >= LOG_DIM)
    {
        dimStateSetInactive(channel, log_idx - LOG_DIM);
    }
}



void ccLogAdcStart(uint32_t channel, int32_t time_offset_ms)
{
#ifdef DEBUG
    logPrintf(channel, "ccLogAdcStart: time_offset_ms=%d\n", time_offset_ms);
#endif

    struct CC_log_state &log_state = cc_log_state[channel];
    struct LOG_log      &log       = equipdev.device[channel].log.structs.log[LOG_ACQ_1KHZ];

    // Set the number of samples to store.
    //
    // The number of samples is fixed at compile time in the header files auto-generated by liblog.
    // This must be a multiple of 21, as the FGClite returns 21 ADC samples per page.

    assert(log.priv->num_postmortem_samples % 21 == 0);

    log_state.adc_sample_count = log.priv->num_postmortem_samples;

    // Start discontinuous logging

    ccLogStartDiscontinuous(channel, &log, log_state.adc_sample_count, 1, &log_state.adc_timestamp, time_offset_ms);
}



void ccLogDimStart(uint32_t channel, uint32_t dim_no, uint32_t num_samples, int32_t time_offset_ms)
{
    struct CC_log_state &log_state = cc_log_state[channel];

    enum LOG_index log_idx = static_cast<enum LOG_index>(LOG_DIM + dim_no);

    struct LOG_log &log = equipdev.device[channel].log.structs.log[log_idx];

    assert(num_samples % 8 == 0);

    log_state.dim_sample_count[dim_no] = num_samples;

    // Start DIM logging

#ifdef DEBUG
        logPrintf(channel, "ccLogDimStart: DIM (%d) starting - time_offset_ms=%d\n",
                  log_idx - LOG_DIM, time_offset_ms);
#endif
    ccLogStartDiscontinuous(channel, &log, num_samples, 20, &log_state.dim_timestamp[dim_no], time_offset_ms);
}



void ccLogAdc(uint32_t channel, uint8_t adc_channel, const struct FGClite_status *status)
{
    struct  Equipdev_channel &device    = equipdev.device[channel];
    struct  CC_log_state     &log_state = cc_log_state[channel];
    struct  LOG_log * const   log       = device.log.structs.log;
    uint8_t transducer_idx;
    uint8_t adc_idx;

    const uint8_t *raw_value_ptr = status->paged;
    cc_float      *log_value_ptr;

    // Convert ADC channel to libsig channel

    switch(adc_channel)
    {
        case ADC_V_MEAS_LOG:    transducer_idx = SIG_TRANSDUCER_V_MEAS; adc_idx = SIG_ADC_ADC_C; log_value_ptr = device.log.adc_v_meas_1kHz; break;
        case ADC_I_A_LOG:       transducer_idx = SIG_TRANSDUCER_DCCT_A; adc_idx = SIG_ADC_ADC_A; log_value_ptr = device.log.adc_i_a_1kHz;    break;
        case ADC_I_B_LOG:       transducer_idx = SIG_TRANSDUCER_DCCT_B; adc_idx = SIG_ADC_ADC_B; log_value_ptr = device.log.adc_i_b_1kHz;    break;
        default:                assert(false); return;
    }

    // Store the current set of samples in the intermediate buffer

    SIG_cal_factors * const trans_factors = device.sig_struct.mgr.transducers[transducer_idx].cal.active;
    SIG_cal_factors * const adc_factors   = device.sig_struct.mgr.adcs[adc_idx].cal.active;

    for(uint32_t i = 0; i < 21; ++i, raw_value_ptr += 3)
    {
        // Get next raw value (24-bit signed integer)

        int32_t raw_value = getInt24(raw_value_ptr);

        // Convert raw value to volts. As we calibrate on the sum of 10 measurements, we need to
        // multiply the raw value by 10 here, too.

        float adc_v = sigAdcRawToVoltsRT(adc_factors, raw_value * 10);

        // For V_MEAS, log volts. For I_A and I_B, convert volts to amps.

        *(log_value_ptr++) = sigTransducerVadcToPhysUnitsRT(trans_factors, adc_v);
    }

    // Log all 3 channels after receiving the samples for the 3rd channel

    if(adc_channel == ADC_I_B_LOG)
    {
        // Log the 21 samples and close the log if it is full

        logStoreDiscontinuousRT(&log[LOG_ACQ_1KHZ], 21);

        log_state.adc_sample_count -= 21;

        ccLogEndDiscontinuous(channel, LOG_ACQ_1KHZ, log_state.adc_sample_count, &log_state.adc_timestamp);

        // Zero the sample buffers

        memset(device.log.adc_i_a_1kHz,    0, 21 * sizeof(float));
        memset(device.log.adc_i_b_1kHz,    0, 21 * sizeof(float));
        memset(device.log.adc_v_meas_1kHz, 0, 21 * sizeof(float));
    }
}



void ccLogDim(uint32_t channel, uint8_t dim_no, uint16_t log_idx, const struct FGClite_status *status)
{
    struct Equipdev_channel &device    = equipdev.device[channel];
    struct LOG_log * const   log       = device.log.structs.log;
    struct CC_log_state     &log_state = cc_log_state[channel];

    // Get DIM analogue raw values and cook them.

    // Each page contains 8 samples. Each sample consists of 4 analogue raw values (2 bytes per channel).

    const uint8_t *raw_value_ptr = status->paged;

    for(uint32_t sample_no = 0; sample_no < 8; ++sample_no)
    {
        uint16_t raw_value[4];

        for(uint32_t ana_channel = 0; ana_channel < 4; ++ana_channel, raw_value_ptr += 2)
        {
            float cooked_value;

            const uint32_t ana_idx = dim_no + ana_channel * 20;

            raw_value[ana_channel] = getInt12(raw_value_ptr);

            cooked_value = raw_value[ana_channel] * device.diag.gain[ana_idx] + device.diag.offset[ana_idx];

            // Special magic for FGClite onboard DIM0

            if(dim_no == 0 && ana_channel == 1)
            {
                // -15V channel depends on +15V channel, see EPCCCS-4799 and EPCCCS-4871 for details

                cooked_value -= 1.02280E-2 * raw_value[0];
            }

            device.log.dim[ana_channel][sample_no] = cooked_value;
        }
    }

    // Store the cooked values in liblog

    enum LOG_index dim_log_idx = static_cast<enum LOG_index>(LOG_DIM + dim_no);

    logStoreDiscontinuousRT(&log[dim_log_idx], 8);

    log_state.dim_sample_count[dim_no] -= 8;

    ccLogEndDiscontinuous(channel, dim_log_idx, log_state.dim_sample_count[dim_no], &log_state.dim_timestamp[dim_no]);
}


// EOF
