/*!
 * @file   state_op.cpp
 * @brief  Manage Operational state for FGClite devices
 * @author Michael Davis
 * @author Quentin King
 */

#include <cmwpub.h>
#include <fgcd.h>
#include <equipdev.h>
#include <cc_sig.h>
#include <logging.h>
#include <state_pc.h>
#include <state_op.h>


// --------------------------------------- STATE FUNCTIONS ----------------------------------------

// STATE: UNCONFIGURED (UC)

static uint32_t stateOpUC(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured == false && fgcd.device[channel].config_unset_count == 0)
    {
        // Ensure that CCLIBS parameters are valid before transitioning out of UNCONFIGURED state
        // Call set property pars functions

        parserCallPropSetParsFunctions(channel);

        // Process any pending changed libreg parameters

        regMgrParsProcess(&device.reg_mgr);

        device.is_configured = true;
    }
    else if(fgcd.device[channel].config_unset_count != 0)
    {
        // After a DEVICE.RESET, set configured mode back to FALSE

        device.is_configured = false;
    }

    return FGC_OP_UNCONFIGURED;
}



// STATE: NORMAL (NL)

static uint32_t stateOpNL(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Clear Simulation warning and FGC_STATE fault

        setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, false);

        setStatusBit(&device.status.st_faults, FGC_FLT_FGC_STATE, false);
    }

    return FGC_OP_NORMAL;
}



// STATE: SIMULATION (SM)

static uint32_t stateOpSM(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Entry function for SIMULATION state

    if(first_call)
    {
        // Enable interlock simulation

        device.vs.sim_intlks = FGC_CTRL_ENABLED;

        equipdevPropSetNumEls(channel, '?', "VS.SIM_INTLKS", 1);
    }

    // Set simulation warning

    setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, true);

    return FGC_OP_SIMULATION;
}



// STATE: CALIBRATING (CL)

static uint32_t stateOpCL(uint32_t channel, bool first_call)
{
     // Update the calibration state machine

    ccSigCalUpdate(channel);

    return FGC_OP_CALIBRATING;
}



// STATE: TEST (TT)

static uint32_t stateOpTT(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Clear Simulation warning

        setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, false);

        // Clear REF.DAC

        device.ref.dac_val = device.ref.dac = 0;
    }

    // Set FGC_STATE fault while in TEST

    setStatusBit(&device.status.st_faults, FGC_FLT_FGC_STATE, true);

    return FGC_OP_TEST;
}



// State machine initialization

void stateOpInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize channel state to be UNCONFIGURED

    device.status.state_op = FGC_OP_UNCONFIGURED;
    device.state_op_func   = stateOpUC;
}



// ------------------------------ TRANSITION CONDITION FUNCTIONS ----------------------------------

// TRANSITION: ANY STATE to NORMAL

static State* XXtoNL(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured && device.mode_op == FGC_OP_NORMAL)
    {
        return stateOpNL;
    }

    return NULL;
}



// TRANSITION: ANY STATE to SIMULATION

static State* XXtoSM(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured && device.mode_op == FGC_OP_SIMULATION)
    {
        return stateOpSM;
    }

    return NULL;
}



// TRANSITION: ANY STATE to TEST

static State* XXtoTT(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured && device.mode_op == FGC_OP_TEST)
    {
        return stateOpTT;
    }

    return NULL;
}



// TRANSITION: NORMAL to CALIBRATING

static State* NLtoCL(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if((device.state_pc_bit_mask & (FGC_PC_OFF_BIT_MASK | FGC_PC_FLT_OFF_BIT_MASK | FGC_PC_STARTING_BIT_MASK)) != 0 &&
       ccSigCalIsCalibrating(channel))
    {
        return stateOpCL;
    }

    return NULL;
}



// TRANSITION: CALIBRATING to NORMAL

static State* CLtoNL(uint32_t channel)
{
    if(!ccSigCalIsCalibrating(channel))
    {
        return stateOpNL;
    }

    return NULL;
}



// Transition functions : called in priority order, from left to right

CC_STATIC_ASSERT(FGC_OP_UNCONFIGURED ==  0, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_OP_NORMAL       ==  1, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_OP_SIMULATION   ==  2, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_OP_CALIBRATING  ==  3, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_OP_TEST         ==  4, Unexpected_STATE_PC_constant_value);

#define REF_MAX_TRANSITIONS     3      //!< Maximum transitions from any state

static  Transition * StateTransitions[][REF_MAX_TRANSITIONS + 1] =
{
    /* 0. UC */ { XXtoNL, XXtoSM, XXtoTT,         NULL },
    /* 1. NL */ {         XXtoSM, XXtoTT, NLtoCL, NULL },
    /* 2. SM */ { XXtoNL,         XXtoTT,         NULL },
    /* 3. CL */ {                         CLtoNL, NULL },
    /* 4. TT */ { XXtoNL, XXtoSM,                 NULL }
};



void stateOpRT(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Reset faults when reset command is active

    if((digitalIOGetCmd(&device.dig) & FGC_DIG_CMDS_VS_RESET) != 0)
    {
        // Clear latched faults, except FW_DIODE, FABORT_UNSAFE and THYR_UNSAFE, which can only be unset by an authorised person

        setStatusBit(&device.status.st_faults, ~(FGC_FLT_THYR_UNSAFE | FGC_FLT_FABORT_UNSAFE | FGC_FLT_FW_DIODE), false);

        // Clear the DCCT fault flag latches

        device.sig_struct.transducer.named.dcct_a.fault_flag = digitalIOGetStatus(&device.dig, FGC_DIG_STAT_DCCT_A_FLT);
        device.sig_struct.transducer.named.dcct_b.fault_flag = digitalIOGetStatus(&device.dig, FGC_DIG_STAT_DCCT_B_FLT);

        // Reset libreg I_MEAS fault as this won't be reset until the next regulation period, which can be up to 80ms later

        regMgrVarValue(&device.reg_mgr, FLAG_I_MEAS_FAULT) = false;

        // Reset ref_mgr faults

        refMgrResetFaultsRT(&device.ref_mgr);
    }

    // Scan transitions for the current operational state, testing each condition in priority order

    State       * next_state = NULL;
    Transition ** state_transitions = &StateTransitions[device.status.state_op][0];
    Transition  * transition;

    while((transition = *(state_transitions++)) != NULL &&      // there is another transition function, and
          (next_state = transition(channel)) == NULL)           // it return NULL (transition condition is false)
    {                                                           // then loop doing nothing
    }

    if(next_state != NULL)
    {
        device.state_op_func = next_state;

        // Transition condition is true so switch to new state

        uint8_t next_state_op = next_state(channel, true);

        if(device.status.state_op == FGC_OP_SIMULATION ||
           next_state_op          == FGC_OP_SIMULATION ||
           device.status.state_op == FGC_OP_TEST       ||
           next_state_op          == FGC_OP_TEST)
        {
            // Moving from/to SIMULATION or TEST - reset faults and warnings

            statePcResetFaults(channel);

            device.status.st_faults   = 0;
            device.status.st_warnings = 0;
        }

        device.status.state_op = next_state_op;

        // Manage libref MODE SIM_MEAS in case STATE.OP entered or exited from SIMULATION state
        // state_op must be set to the next_state_op before calling ParsMeasSim()

        ParsMeasSim(channel);
    }
    else
    {
        // No transition : stay in current state and run the state function with the first_call parameter set to false

        device.state_op_func(channel, false);
    }
}

// EOF
