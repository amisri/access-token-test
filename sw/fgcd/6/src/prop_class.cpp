/*!
 * @file   prop_class.cpp
 * @brief  Class-specific functions for getting and setting properties
 * @author Stephen Page
 * @author Michael Davis
 */

#include <cmd.h>
#include <classes/6/defconst.h>
#include <classes/6/defprops.h>
#include <fgc_errs.h>
#include <fgc_fip.h>
#include <fgcddev.h>
#include <fgclite_cycle.h>
#include <fieldbus.h>
#include <fip.h>
#include <logging.h>
#include <prop.h>
#include <timing_handlers.h>

#include <prop_class.h>
#include <prop_equip.h>
#include <classes/92/defconst_assert.h>


// Struct containing global variables

struct Prop_class
{
    uint16_t event_buf[sizeof(struct fgc_event)];    // Buffer for set of FGC FIP event
};



// Global variables

struct Prop_class prop_class;



// SETIF functions

int32_t SetifResetOk(struct Cmd *command)
{
    // As resetting the FGCD is always allowed and this resets all the devices, it does not make
    // sense to put restrictions on resetting the devices. Thus S DEVICE.RESET is always allowed.

    return 1;
}



/*
 * Set the time of a simulated timing event
 *
 * @param[out]    evt_time    Return value: event time
 * @param[in]     delay_ms    Specified delay (ms) to be added to the current time to get evt_time
 * @param[in]     evt_type    Event type
 */

static void propClassSetEventTime(struct timespec *evt_time, uint16_t delay_ms, Timing_event_info info)
{
    // Get the current cycle time

    struct timeval evt_time_tv;

    timingGetUserTime(0, &evt_time_tv);

    // The total delay in ms is equal to the delay specified in the parameter, less the delay specified
    // in the handler for this timing event (this is added back in the event handler function), plus
    // one cycle period to compensate for sending the event by a non-realtime thread.

    uint32_t evt_delay_ms = delay_ms - info.delay_ms + FGC_FIELDBUS_CYCLE_PERIOD_MS;

    struct timeval evt_delay_us = { 0, evt_delay_ms * 1000 };

    timevalAdd(&evt_time_tv, &evt_delay_us);

    // Convert timeval to timespec

    evt_time->tv_sec  = evt_time_tv.tv_sec;
    evt_time->tv_nsec = evt_time_tv.tv_usec * 1000;
}

// SET functions

int32_t SetEvent(struct Cmd *command, struct prop *property)
{
    if(SetInteger(command, property) != 0) return 1;

    uint16_t    &evt_type           = fgcddev.event_buf[EVENT_TYPE];
    uint16_t    &evt_trigger_type   = fgcddev.event_buf[EVENT_TRIGGER_TYPE];
    uint16_t    &evt_payload        = fgcddev.event_buf[EVENT_PAYLOAD];
    uint16_t    &delay_ms           = fgcddev.event_buf[EVENT_DELAY_MS];

    // Check that event type and payload are within accepted range

    if(evt_type > 0xFF || evt_trigger_type > 0xFF || evt_payload > 0xFF)
    {
        command->error = FGC_OUT_OF_LIMITS;
        return 1;
    }

    // Send the event

    tTimingTime           evt_time;
    Timing::EventValue    evt_value;    // No way to put the payload into the EventValue, so we can't set the EVENT_GROUP for START and ABORT events
    Timing_event_handler* handler;
    TimingHandlerFunc*    handler_function;

    switch(evt_type)
    {
        case FGC_EVT_PM:
            handler = &timing_event_handlers[TIMING_HANDLER_POSTMORTEM];
            handler_function = timingPostMortem;
            break;

        case FGC_EVT_START:
            handler = &timing_event_handlers[TIMING_HANDLER_STARTREF];
            handler_function = timingStartRef;
            break;

        case FGC_EVT_ABORT:
            handler = &timing_event_handlers[TIMING_HANDLER_ABORTREF];
            handler_function = timingAbortRef;
            break;

        default:
            command->error = FGC_NOT_IMPL;
            return 1;
    }

    // For the additional timing info we are going to use first one - if it exists - otherwise a default-constructed one
    Timing_event_info info;

    if (!handler->timing_events.empty())
    {
        info = handler->timing_events.begin()->second;
    }

    // Execute the set event time function and the timing event handler
    propClassSetEventTime(&evt_time.time, delay_ms, info);
    handler_function(handler, info, evt_time, &evt_value);

    return 0;
}



int32_t SetFGCCode(struct Cmd *command, struct prop *property)
{
    int32_t retval;

    if((retval = SetInteger(command, property)) == 0)
    {
        // If the property was set successfully, initialise the information fields

        fgcddevSetFGCCodeInfo(command->parser->value);
    }

    return retval;
}


int32_t SetReadCodes(struct Cmd *command, struct prop *property)
{
    command->error = fgcddevReadCodes();

    if(command->error)
    {
        command->error = FGC_ERROR_READING_FILE;
    }
    return command->error;
}



int32_t SetTerminate(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    // Decide if this command is for the FGCD or an equipment device and call the appropriate function

    if(channel == 0)
    {
        return Terminate(command, property);
    }
    else
    {
        return propEquipTerminate(command, property);
    }
}



int32_t SetGwTerminate(struct Cmd *command, struct prop *property)
{
    uint32_t fip_address;

    // Accepts one parameter which must be a valid FIP address

    if(command->value_length == 0)
    {
        command->error = FGC_BAD_PARAMETER;

        return 1;
    }

    fip_address = atoi(command->value);

    if(fip_address < 1 || fip_address > FGCD_MAX_EQP_DEVS)
    {
        command->error = FGC_BAD_PARAMETER;

        return 1;
    }

    if(property->sym_idx == STP_PWRCYC)
    {
        command->error = fgcliteCycleNanofipReset(fip_address, RESET_HARD) ? FGC_BUSY : FGC_OK_NO_RSP;
    }
    else if(property->sym_idx == STP_RESET)
    {
        command->error = fgcliteCycleNanofipReset(fip_address, RESET_SOFT) ? FGC_BUSY : FGC_OK_NO_RSP;
    }
    else
    {
        // Function has been called for a property it does not handle

        command->error = FGC_NOT_IMPL;
    }

    return command->error != FGC_OK_NO_RSP;
}

// EOF
