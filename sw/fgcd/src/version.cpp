/*!
 * @file   version.c
 * @brief  Include version information
 * @author Stephen Page
 */

#define VERSION_DEF

#include <version_def.h>

// EOF
