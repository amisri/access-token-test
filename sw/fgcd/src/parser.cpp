/*!
 * @file   parser.cpp
 * @brief  Functions for the parser
 * @author Stephen Page
 * @author Michael Davis
 */

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <algorithm>
#include <string>

#define FGC_PARSER_CONSTS

#include <cmd.h>
#include <cmdqmgr.h>
#include <cmwpub.h>
#include <consts.h>
#include <defconst.h>
#include <definfo.h>
#include <devname.h>
#include <equipdev.h>
#include <cmwpub.h>
#include <transaction.h>
#include <fgcd.h>
#include <fgc_consts_gen.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <hash.h>
#include <logging.h>
#include <nonvol.h>
#include <parser.h>
#include <prop.h>
#include <queue.h>
#include <tcp.h>
#include <timing.h>
#include <transaction.h>
#include <fgcddebug.h>



// Struct containing global variables

struct Parser_globals
{
    struct hash_table   *getopt_hash;   // Hash of get options
};



// Global variables

struct Parser_globals parser_globals;

// This array should be initialised with the IDs of classes to be parsed

const uint32_t MAX_PARSER_CLASS_DATA_LEN = 2;

struct Parser_class_data parser_class_data[MAX_PARSER_CLASS_DATA_LEN] = { {0}, {0} };

// Static functions

static FGCD_thread_func parserRun;

static void parserParse(struct Parser *parser);
static int32_t parserIdentifyArray(struct Parser *parser, struct prop *property, char *command_next_char);
static int32_t parserIdentifyGetOptions(struct Parser *parser);
static struct prop *parserIdentifyProperty(struct Parser *parser);
static int32_t parserIdentifyTransaction(struct Parser *parser, struct prop *property, char *command_next_char);
static int32_t parserIdentifyUser(struct Parser *parser, struct prop *property, char *command_next_char);

static void setPropParent(struct prop *property, struct prop *parent);
static void setPropParent(struct prop *property, struct prop *parent)
{
    struct prop *child;
    property->parent = parent;

    if(property->type == PT_PARENT)
    {
        uint32_t i;
        uint32_t num_children = property->num_elements;

        for(i=0; i<num_children; i++)
        {
            child = reinterpret_cast<struct prop*>(property->value) + i;
            setPropParent(child, property);
        }
    }
}


// External functions

int32_t parserInit(void)
{
    uint32_t i;

    // Initialise the getopt hash

    if((parser_globals.getopt_hash = hashInit(GETOPT_HASH_SIZE)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise getopt hash\n");
        return 1;
    }

    // Populate the getopt hash

    for(i = 0 ; i < N_GETOPT_SYMBOLS ; i++)
    {
        hashInsert(parser_globals.getopt_hash, &sym_tab_getopt[i]);
    }

    return 0;
}


struct Parser_class_data *parserGetClassData(uint32_t class_id)
{
    uint32_t i;

    // Find parser data for class

    for(i = 0 ; i < MAX_PARSER_CLASS_DATA_LEN && parser_class_data[i].class_id != class_id ; i++) ;

    // Check whether class data was found

    if(i == MAX_PARSER_CLASS_DATA_LEN) // Class data not found
    {
        return NULL;
    }

    return &parser_class_data[i];
}



uint32_t parserGetPropertyName(struct prop *property, uint32_t class_id, char *prop_name, uint32_t prop_max_len)
{
    Parser_class_data *class_data = parserGetClassData(class_id);

    // Stack property and its parents

    struct prop *parents[10];
    uint32_t     parent_idx = 0;

    do
    {
        parents[parent_idx++] = property;
        property = property->parent;
    } while (property != NULL);

    // Unwind stack, copying property names

    uint32_t name_len = 0;

    // Reduce length by 1 to keep one byte for the trailing nul

    const uint32_t max_len = prop_max_len - 1;

    while(name_len < max_len && parent_idx-- > 0)
    {
        char *from = class_data->sym_tab_prop[parents[parent_idx]->sym_idx].key.c;
        char  c;

        while(name_len < max_len && (c = *(from++)) != '\0')
        {
            prop_name[name_len++] = c;
        }

        prop_name[name_len++] = '.';
    }

    if(name_len >= max_len)
    {
        prop_name[max_len] = '\0';

        logPrintf(0,"parserGetPropertyName: property name exceeds length limit (%u):%s\n", prop_max_len, prop_name);
    }
    else
    {
        // Overwrite the final '.' with a nul

        prop_name[--name_len] = '\0';
    }

    // return length of string copied

    return name_len;
}


int32_t parserSetClassData(struct Parser_class_data *data)
{
    struct Parser_class_data *class_data;
    uint32_t                  i;

    // Search for empty slot

    for(i = 0 ; i < MAX_PARSER_CLASS_DATA_LEN && parser_class_data[i].class_id != 0 ; i++) ;

    // parser_class_data is already full

    if(i == MAX_PARSER_CLASS_DATA_LEN)
    {
        fprintf(stderr, "ERROR: parser_class_data array is full\n");
        return 1;
    }

    class_data = &parser_class_data[i];

    *class_data = *data;

    // Set parent properties

    struct prop *   root_property;

    // We start in the root properties (those without a parent) and setPropParent will
    // continue recursively
    for(i=0; i<data->sym_tab_prop_len; i++)
    {
        root_property = reinterpret_cast<struct prop*>(data->sym_tab_prop[i].value);

        if(root_property != NULL)
        {
            setPropParent(root_property, NULL);
        }
    }

    return 0;
}



void parserClearClassData(uint32_t class_id)
{
    struct Parser_class_data null_data;

    // Clear class data

    memset(&null_data, 0, sizeof(null_data));
    null_data.class_id = class_id;
    parserSetClassData(&null_data);
}



int32_t parserStart(struct Parser *parser)
{
    parser->current_command = NULL;
    parser->from            = 0;
    parser->to              = 0;

    // Initialise device command queue

    fgcddebug("Calling queueInit(command_queue,%u)\n",cmdqmgr.num_cmds);

    if(queueInit(&parser->command_queue, cmdqmgr.num_cmds))
    {
        fprintf(stderr, "ERROR: parserStart:Failed to initialise command queue:num_cmds=%u\n",cmdqmgr.num_cmds);
        return 1;
    }

    // Start the parser thread

    return fgcd_thread_create(&parser->thread, parserRun, parser, PARSER_THREAD_POLICY, PARSER_THREAD_PRIORITY ,NO_AFFINITY);
}

int32_t parserConfigProp(struct Parser *parser, struct prop *property)
{
    struct Cmd         *command = parser->current_command;
    struct FGCD_device *device  = parser->current_command->device;

    // Check type of access is allowed for property

    switch(command->type)
    {
        // Get command

        case CMD_GET:
        case CMD_GET_SUB:
        case CMD_SUB:
            if(!property->get_func_idx)
            {
                command->error = FGC_GET_NOT_PERM;
                return 1;
            }
            break;

        // Set command

        case CMD_SET:
        case CMD_SET_BIN:
            if(!property->set_func_idx)
            {
                command->error = FGC_SET_NOT_PERM;
                return 1;
            }

            // Steps other than 1 are not allowed for set commands

            if(parser->step != 1)
            {
                command->error = FGC_BAD_ARRAY_IDX;
                return 1;
            }
            break;

        // Subscription cancellation

        case CMD_UNSUB:

            // A subscription cancellation command should never be configured

            command->error = FGC_PROTO_ERROR;
            return 1;
    }

    // Set parser num_els pointer

    parserSetNumElsPtr(parser, property->flags & PF_INDIRECT_N_ELS ? reinterpret_cast<uintptr_t*>(property->num_elements) : &property->num_elements);

    // Set parser value pointer

    parser->value = static_cast<char*>(property->value);

    if(parser->num_elements_ptr == NULL)
    {
        logPrintf(device->id, "PROP Null num_elements_ptr for class %d symbol %s property %s\n",
                parser->class_data->class_id, parser->class_data->sym_tab_prop[property->sym_idx].key.c, property->name);

        parser->current_command->error = FGC_NULL_ADDRESS;

        return 1;
    }
    else if(property->type != PT_NULL && parser->value == NULL)
    {
        // Don't try to configure NULL properties

        return 0;
    }

    // Check property access rights related to transactions and transactional properties

    if(device->id != 0) // Device is not the FGCD device
    {
        if(transactionAccessCheck(command,
                                  (property->flags & PF_TRANSACTIONAL) != 0,
                                  equipdev.device[device->id].ppm[command->user].transaction_id))
        {
            return 1;
        }
    }

    // Configure property for equipment devices

    parser->is_ppm = equipdevConfigProperty(parser, property);

    // Zero command user if property is not PPM

    if(property->type != PT_PARENT && (property->flags & PF_PPM) == 0 && parser->is_ppm == false)
    {
        command->user = 0;
    }

    // Handle indirect numbers of elements

    if(property->flags & PF_INDIRECT_N_ELS)
    {
        // Return an error if pointer to number of elements is NULL

        if(parser->num_elements_ptr == NULL)
        {
            logPrintf(device->id, "PARSER Null address for number of elements for class %d symbol %s property %s\n",
                parser->class_data->class_id, parser->class_data->sym_tab_prop[property->sym_idx].key.c, property->name);
            parser->current_command->error = FGC_NULL_ADDRESS;
            return 1;
        }
    }

    // Handle indirect values

    if(property->flags & PF_INDIRECT)
    {
        // Return an error if pointer to value is NULL

        if(parser->value == NULL || (*(void **)parser->value) == NULL)
        {
            logPrintf(device->id, "PARSER Null address for class %d symbol %s property %s\n",
                parser->class_data->class_id, parser->class_data->sym_tab_prop[property->sym_idx].key.c, property->name);
            parser->current_command->error = FGC_NULL_ADDRESS;
            return 1;
        }

        // Dereference value pointer

        parser->value = *reinterpret_cast<char**>(parser->value);
    }

    return 0;
}



int32_t parserConfigGet(struct Parser *parser, struct prop *property)
{
    struct Cmd *command = parser->current_command;

    // Parent properties need no configuration

    if(property->type == PT_PARENT)
    {
        return 0;
    }

    // Print value prefixes

    // Check whether to print label

    if(parser->getopt_flags & GET_OPT_LBL && parserPrintLabel(command, property))
    {
        return 1;
    }

    // Check whether to print type

    if(parser->getopt_flags & GET_OPT_TYPE && parserPrintType(command, property))
    {
        return 1;
    }

    // Check whether to print range

    if(parser->getopt_flags & GET_OPT_RANGE && parserPrintRange(command, property))
    {
        return 1;
    }

    // Prepare array range

    // If PF_CLIENT flag is set and range is unspecified, set range to current client's property

    if((property->flags  & PF_CLIENT)   &&
       parser->from     == UNSPECIFIED  &&
       parser->to       == UNSPECIFIED  &&
       command->client  != NULL)
    {
        parser->from    = command->client->id;
        parser->to      = command->client->id;
    }

    // If range is unspecified, default to entire array

    if(parser->from == UNSPECIFIED)
    {
        parser->from = (property->flags & PF_FGC) ? 1 : 0;
    }
    else
    {
        // Check whether specified start index is within array range

        if(parser->from >= property->max_elements)
        {
            // Array range starts beyond possible range

            command->error = FGC_BAD_ARRAY_IDX;
            return 1;
        }
    }

    if(parser->to == UNSPECIFIED)
    {
        parser->to = parserGetNumEls(parser);

        if(parser->to > 0) parser->to--;
    }
    else
    {
        // Check whether specified end index is within array range

        if(parser->to >= property->max_elements)
        {
            // Array range is beyond possible range

            command->error = FGC_BAD_ARRAY_IDX;
            return 1;
        }

        if(parser->to >= parserGetNumEls(parser))
        {
            // Array range ends beyond end of array, truncate to end

            parser->to = parserGetNumEls(parser) - 1;
        }
    }

    return 0;
}



int32_t parserConfigSet(struct Parser *parser, struct prop *property)
{
    struct Cmd *command = parser->current_command;

    // Check setif function

    if(property->setif_func_idx &&
       !parser->class_data->setif_func[property->setif_func_idx](command))
    {
        // Set not allowed in this state

        command->error = FGC_BAD_STATE;
        return 1;
    }

    // Handle PF_CLIENT flag for client-context based properties

    if(property->flags & PF_CLIENT)
    {
        // If range is unspecified, set range to current client's property

        if(parser->from    == UNSPECIFIED &&
           parser->to      == UNSPECIFIED &&
           command->client != NULL)
        {
            parser->from   = command->client->id;
            parser->to     = command->client->id;
        }
        else if(command->client == NULL ||
                parser->from != static_cast<uint32_t>(command->client->id) ||
                parser->to   != static_cast<uint32_t>(command->client->id))
        {
            // Set of another client's property is not permitted, return error

            command->error = FGC_SET_NOT_PERM;
            return 1;
        }
    }

    // Handle array indices

    parser->num_array_specifiers = 0;

    // Check whether a 'from' index has been specified

    if(parser->from == UNSPECIFIED) // 'from' index was not specified
    {
        // Set 'from' index to the start of the array

        parser->from = 0;
    }
    else // 'from' index was specified
    {
        parser->num_array_specifiers++;
    }

    // Check whether a 'to' index has been specified

    if((parser->to == UNSPECIFIED)) // 'to' index was not specified
    {
        // Set 'to' index to maximum

        parser->to = property->max_elements - 1;
    }
    else // 'to' index was specified
    {
        parser->num_array_specifiers++;
    }

    // Check whether array range starts beyond end of existing data

    if(parser->from > parserGetNumEls(parser))
    {
        // Start of set is end of existing data, add pad elements

        parser->set_index    = parserGetNumEls(parser);
        parser->num_set_pads = parser->from - parserGetNumEls(parser);
    }
    else // Start of set is 'from' index, no padding necessary
    {
        parser->set_index    = parser->from;
        parser->num_set_pads = 0;
    }

    // Perform padding

    if(parser->num_set_pads)
    {
        memset(parser->set_buffer, 0, parser->num_set_pads * prop_type_size[property->type]);
    }

    // Set maximum number of elements to set

    parser->max_set_elements = (parser->to - parser->from) + 1;

    return 0;
}



int32_t parserGetParent(struct Cmd *command, struct prop *property, enum Parser_parent_filter filter)
{
    struct prop     *child;
    uint32_t        i;
    uint32_t        from, to;
    uint32_t        num_children;
    struct Parser   *parser = command->parser;
    uint32_t        user;

    // Return an error if BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN)
    {
        command->error = FGC_BAD_GET_OPT;
        return 1;
    }

    // If the RANGE option is set for PARENT properties which have a SYM_LIST defined, print the range of the parent property.
    // If there is no SYM_LIST, recurse through the child properties and print their range instead.

    if(property->flags & PF_SYM_LST &&
       parser->getopt_flags & GET_OPT_RANGE)
    {
        return parserPrintRange(command, property);
    }

    // Store initial array range and user

    from    = parser->from;
    to      = parser->to;
    user    = command->user;

    // Set flag to print labels of children

    parser->getopt_flags |= GET_OPT_LBL;

    // Process each child property

    num_children = parserGetNumEls(parser);
    for(i = 0, child = reinterpret_cast<struct prop*>(parser->value) ; i < num_children ; i++, child++)
    {
        // Skip child if it has no get function or is hidden

        if(!child->get_func_idx || (filter == PARSER_ALL && (child->flags & PF_HIDE)))
        {
            continue;
        }

        // Configure parser to access child

        if(parserConfigProp(parser, child))
        {
            return 1;
        }

        // Check whether child property is also a parent

        if(child->type == PT_PARENT)
        {
            // Check for exceeding maximum number of levels

            if(parser->parents_index == PARSER_MAX_PROP_LEVELS)
            {
                command->error = FGC_TOO_MANY_LEVELS;
                return 1;
            }

            // Add symbol index to parents

            parser->parents[parser->parents_index++] = child->sym_idx;
        }
        else if(filter != PARSER_ALL)
        {
            // Ignore properties that are not configuration properties or which do not correspond to the filter

            if(!(child->flags & PF_CONFIG) ||
                (filter == PARSER_CONFIG_SET   && parserGetNumEls(parser) == 0) ||
                (filter == PARSER_CONFIG_UNSET &&
                   (((child->flags & PF_DONT_SHRINK) == 0 && parserGetNumEls(parser) != 0) ||
                    ((child->flags & PF_DONT_SHRINK) != 0 && parserGetNumEls(parser) == child->max_elements))))
            {
                continue;
            }
        }

        // Configure parser to get child

        if(parserConfigGet(parser, child))
        {
            // Ignore bad array index error

            if(command->error != FGC_BAD_ARRAY_IDX)
            {
                return 1;
            }
            command->error = 0;
        }
        else // Parser configured successfully
        {
            if(parserGetNumEls(parser) > 0  &&
               (property->type == PT_PARENT || parser->from < parserGetNumEls(parser)))
            {
                // Call child's get function and check for error

                if(parser->class_data->get_func[child->get_func_idx] == GetParent)
                {
                    if(parserGetParent(command, child, filter))
                    {
                        return 1;
                    }
                }
                else if(parser->class_data->get_func[child->get_func_idx](command, child))
                {
                    return 1;
                }
            }
            else if(parser->value == NULL)
            {
                // Exception to call the accessor function for properties which do not have a data structure associated with
                // them, yet are not of type PT_NULL

                if(parser->class_data->get_func[child->get_func_idx](command, child))
                {
                    return 1;
                }
            }
        }

        if(child->type == PT_PARENT)
        {
            // Correct parents index

            parser->parents_index--;
        }
        else // Child is not a parent
        {
            // Append a new-line

            command->value[command->value_length++] = '\n';
        }

        // Restore original array range and user

        parser->from    = from;
        parser->to      = to;
        command->user   = user;
    }

    // Restore parser configuration

    parserConfigProp(parser, property);

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



uint32_t parserIdentifyNextSymbol(struct Parser     *parser,
                                  char              *c,
                                  uint32_t          *index,
                                  uint32_t          length,
                                  struct hash_table *hash_table,
                                  const char        *delimiters,
                                  int32_t           null_delim,
                                  char              *delim_match)
{
    struct Cmd          *command        = parser->current_command;
    uint32_t            d;
    struct hash_entry   *entry;
    uint32_t            i;
    uint32_t            match_found     = 0;
    size_t              num_delims      = strlen(delimiters);
    char                symbol[HASH_MAX_KEY_LEN + 1];

    // Check whether the end of the string has already been reached

    if(*index >= length)
    {
        return 0;
    }

    // Move to indexed point in string

    c += *index;

    // Add NULL to delimiters if requested

    if(null_delim) num_delims++;

    for(i = 0 ;
        !match_found && i < sizeof(symbol) && *index <= length ;
        (*index)++, i++)
    {
        // Copy and capitalise symbol

        symbol[i] = toupper(*c++);

        // Check for match to delimiters

        for(d = 0 ; !match_found && d < num_delims ; d++)
        {
            if(symbol[i] == delimiters[d])
            {
                // Symbol found

                if(delim_match)
                {
                    *delim_match = delimiters[d];
                }

                // Null terminate symbol

                symbol[i]   = '\0';
                match_found = 1;
            }
        }
    }

    // Check whether a delimiter was not matched

    if(!match_found) return 0;

    // Look up symbol in hash

    if(hash_table && (entry = hashFind(hash_table, symbol))) // Symbol found
    {
        return entry->index;
    }
    else // Symbol not in hash table
    {
        command->error = FGC_UNKNOWN_SYM;
        return 0;
    }
}



void parserScanInt(struct Parser    *parser,
                   char             *c,
                   uint32_t         *index,
                   uint32_t         length,
                   const char       *delims,
                   int32_t          null_delim,
                   char             *delim_match,
                   int32_t          allow_signed,
                   int32_t          check_min,
                   int32_t          check_max,
                   int32_t          min,
                   int32_t          max,
                   int32_t          *value)
{
    struct Cmd  *command    = parser->current_command;
    uint32_t    i;
    size_t      num_delims  = strlen(delims);
    char        *symbol;

    union
    {
        long             d;
        unsigned long    u;
    } buf;

    // Check whether the end of the string has already been reached

    if(*index >= length)
    {
        command->error = FGC_NO_SYMBOL;

        if(delim_match)
        {
            *delim_match = '\0';
        }
        return;
    }

    // Move to indexed point in string

    c += *index;

    // Check whether to include null as a delimiter

    if(null_delim) num_delims++; // Implicitly add null to delimiters

    // Skip spaces

    while(*c == ' ')
    {
        c++;
        (*index)++;
    }

    symbol = c;

    errno = 0;
    if(allow_signed)
    {
        buf.d = strtol(symbol, &c, 0);
    }
    else
    {
        // Check that value is not negative

        if(*c == '-')
        {
            command->error  = FGC_BAD_INTEGER;
            return;
        }

        buf.u = strtoul(symbol, &c, 0);

    }
    *index += (c - symbol) + 1;

    // Check that an allowed delimiter was reached

    for(i = 0 ; *c != delims[i] && i < num_delims ; i++);

    if(i == num_delims) // No delimiter was matched
    {
        command->error = FGC_BAD_INTEGER;
        return;
    }

    if(delim_match)
    {
        *delim_match = *c;
    }

    // Check whether scanned integer was valid

    if(c == symbol)
    {
        command->error  = FGC_NO_SYMBOL;
        return;
    }
    else if(errno)
    {
        command->error  = FGC_BAD_INTEGER;
        return;
    }

    // Set value

    if(allow_signed)
    {
        // Check limits

        if((check_min && buf.d < min) ||
           (check_max && buf.d > max))
        {
            command->error = FGC_OUT_OF_LIMITS;
            return;
        }

        *value = buf.d;
    }
    else // Value is unsigned
    {
        // Check limits

        if((check_min && buf.u < (uint32_t)min) ||
           (check_max && buf.u > (uint32_t)max))
        {
            command->error = FGC_OUT_OF_LIMITS;
            return;
        }

        *value = buf.u;
    }
}



void parserScanFloat(struct Parser  *parser,
                     char           *c,
                     uint32_t       *index,
                     uint32_t       length,
                     const char     *delims,
                     int32_t        null_delim,
                     char           *delim_match,
                     int32_t        check_min,
                     int32_t        check_max,
                     double         min,
                     double         max,
                     double         *value)
{
    double      buf;
    struct Cmd  *command    = parser->current_command;
    uint32_t    i;
    size_t      num_delims  = strlen(delims);
    char        *symbol;

    // Check whether the end of the string has already been reached

    if(*index >= length)
    {
        command->error  = FGC_NO_SYMBOL;

        if(delim_match)
        {
            *delim_match = '\0';
        }
        return;
    }

    // Move to indexed point in string

    c += *index;

    // Check whether to include null as a delimiter

    if(null_delim) num_delims++; // Implicitly add null to delimiters

    symbol  = c;
    errno   = 0;
    buf     = strtod(symbol, &c);
    *index += (c - symbol) + 1;

    // Check that an allowed delimiter was reached

    for(i = 0 ; *c != delims[i] && i < num_delims ; i++);

    if(i == num_delims) // No delimiter was matched
    {
        command->error = FGC_BAD_FLOAT;
        return;
    }

    if(delim_match)
    {
        *delim_match = *c;
    }

    // Check whether scanned float was valid

    if(c == symbol)
    {
        command->error = FGC_NO_SYMBOL;
        return;
    }
    else if(errno)
    {
        command->error = FGC_BAD_FLOAT;
        return;
    }

    // Check limits

    if( !isfinite(buf)          ||
       (check_min && buf < min) ||
       (check_max && buf > max))
    {
        command->error = FGC_OUT_OF_LIMITS;
        return;
    }

    // Set value

    *value = buf;

    return;
}



void parserScanPoint(struct Parser  *parser,
                     char           *c,
                     uint32_t       *index,
                     uint32_t       length,
                     const char     *delims,
                     int32_t        null_delim,
                     char           *delim_match,
                     int32_t        check_min,
                     int32_t        check_max,
                     double         min,
                     double         max,
                     double         *valuex,
                     double         *valuey)
{
    double      bufx;
    double      bufy;
    struct Cmd  *command    = parser->current_command;
    uint32_t    i;
    size_t      num_delims  = strlen(delims);
    int         consumed, num_valid;

    // Check whether the end of the string has already been reached

    if(*index >= length)
    {
        command->error  = FGC_NO_SYMBOL;

        if(delim_match)
        {
            *delim_match = '\0';
        }
        return;
    }

    // Move to indexed point in string

    c += *index;

    // Check whether to include null as a delimiter

    if(null_delim) num_delims++; // Implicitly add null to delimiters

    num_valid = sscanf(c,"%lf|%lf%n",&bufx,&bufy,&consumed);

    if(num_valid < 2)
    {
        for(i = 0 ; *c != delims[i] && i < num_delims ; i++);

        if(i != num_delims) // delimiter matched
        {
            if(delim_match)
            {
                *delim_match = *c;
            }

            command->error = FGC_NO_SYMBOL;
            return;
        }
        else
        {
            command->error = FGC_BAD_FLOAT;
            return;
        }
    }

    c += consumed;

    *index += consumed + 1;

    // Check that an allowed delimiter was reached

    for(i = 0 ; *c != delims[i] && i < num_delims ; i++);

    if(i == num_delims) // No delimiter was matched
    {
        command->error = FGC_BAD_FLOAT;
        return;
    }

    if(delim_match)
    {
        *delim_match = *c;
    }

    // Check limits

    if(!isfinite(bufx)           ||
       !isfinite(bufy)           ||
       (check_min && bufx < min) ||
       (check_max && bufx > max) ||
       (check_min && bufy < min) ||
       (check_max && bufy > max))
    {
        command->error = FGC_OUT_OF_LIMITS;
        return;
    }

    // Set value

    *valuex = bufx;
    *valuey = bufy;

    return;
}


int32_t parserPrintLabel(struct Cmd *command, struct prop *property)
{
    char            buffer[FGC_MAX_PROP_LEN + 2];
    uint32_t        i;
    uint32_t        label_length    = 0;
    uint32_t        length;
    struct Parser   *parser         = command->parser;

    // Add parents' symbols to value

    for(i = 0 ; i < parser->parents_index ; i++)
    {
        length           = sprintf(buffer, "%s.", parser->class_data->sym_tab_prop[parser->parents[i]].key.c);
        label_length    += length;

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }

        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Add this property's symbol to value

    if(parser->cmd_prop->type != PT_PARENT)
    {
        length           = sprintf(buffer, "value:");
    }
    else
    {
        length           = sprintf(buffer, "%s:", parser->class_data->sym_tab_prop[property->sym_idx].key.c);
    }
    label_length    += length;

    if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
    {
        command->error = FGC_RSP_BUF_FULL;
        return 1;
    }

    strcpy(&command->value[command->value_length], buffer);
    command->value_length += length;

    return 0;
}



int32_t parserPrintType(struct Cmd *command, struct prop *property)
{
    char                buffer[FGC_MAX_PROP_LEN + 1];
    uint32_t            length;
    enum prop_type_e    type;

    if(property->flags & PF_SYM_LST)
    {
        if(property->max_elements > 1)
        {
            type = PT_STRING;
        }
        else
        {
            type = PT_CHAR;
        }
    }
    else
    {
        type = static_cast<enum prop_type_e>(property->type);
    }

    // Add property's type to value

    length = sprintf(buffer, "%s:", prop_type_name[type]);

    if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
    {
        command->error = FGC_RSP_BUF_FULL;
        return 1;
    }

    // Check whether the value is scalar or an array

    if(property->max_elements == 1) // Value is scalar
    {
        // Copy the type in lowercase

        for(uint32_t i = 0 ; i < length ; i++)
        {
            command->value[command->value_length++] = tolower(buffer[i]);
        }
    }
    else // Value is an array
    {
        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    return 0;
}



int32_t parserPrintRange(struct Cmd *command, struct prop *property)
{
    char                  buffer[32];
    int32_t               delim_flag  = 0;
    uint32_t              length;
    struct Parser        *parser      = command->parser;
    const struct sym_lst *sym_list;

    if(property->flags & PF_SYM_LST)
    {
        // Get pointer to start of sym_list

        sym_list = static_cast<const sym_lst*>(property->range);

        // Add opening bracket to value

        command->value[command->value_length++] = '(';
        command->value[command->value_length]   = '\0';

        // Add each symbol to value

        for( ; sym_list->index ; sym_list++)
        {
            if(delim_flag)
            {
                // Add leading delimiter to value

                command->value[command->value_length++] = ' ';
                command->value[command->value_length]   = '\0';
            }
            else
            {
                delim_flag = 1;
            }

            // Add symbol to value

            length = sprintf(buffer, "%s", parser->class_data->sym_tab_const[sym_list->index & ~PROP_SYM_NOT_SETTABLE].key.c);

            if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }

            strcpy(&command->value[command->value_length], buffer);
            command->value_length += length;
        }
    }
    else if(property->flags & PF_LIMITS)
    {
        // Add limits in either decimal or hex to value

        length = 0;
        switch(property->type)
        {
            // Floating-point types

            case PT_FL_POINT:
            case PT_FLOAT:
            case PT_POINT:
                length = sprintf(buffer, "(%.5E %.5E",
                                         ((struct float_limits *)property->range)->min,
                                         ((struct float_limits *)property->range)->max);
                break;

            // Signed integer types

            case PT_INT8S:
            case PT_INT16S:
            case PT_INT32S:
                length = sprintf(buffer, (parser->getopt_flags & GET_OPT_HEX ? "(0x%08X 0x%08X" : "(%d %d"),
                                         ((struct int_limits *)property->range)->min,
                                         ((struct int_limits *)property->range)->max);
                break;

            // Unsigned integer types

            case PT_ABSTIME:
            case PT_ABSTIME8:
            case PT_INT8U:
            case PT_INT16U:
            case PT_INT32U:
                length = sprintf(buffer, (parser->getopt_flags & GET_OPT_HEX ? "(0x%08X 0x%08X" : "(%u %u"),
                                         ((struct int_limits *)property->range)->min,
                                         ((struct int_limits *)property->range)->max);
                break;
        }

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }

        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Add closing bracket and space to value

    if(command->value_length + 2 >= CMD_MAX_VAL_LENGTH)
    {
        command->error = FGC_RSP_BUF_FULL;
        return 1;
    }

    command->value[command->value_length++] = ')';
    command->value[command->value_length++] = ' ';
    command->value[command->value_length]   = '\0';

    return 0;
}


// Static functions

/*
 * Parser thread
 */

static void *parserRun(void *arg)
{
    struct Cmd *command;

    struct Parser *parser = static_cast<struct Parser*>(arg);

    parser->run = 1;

    // Process commands

    while(parser->run)
    {
        if((command = (struct Cmd *)queueBlockPop(&parser->command_queue)) == NULL)
        {
            logPrintf(0, "parserRun() could not read command queue, exiting.\n");
            break;
        }

        command->parser         = parser;
        parser->current_command = command;

        // Find parser data for class

        if(!(parser->class_data = parserGetClassData(command->device->fgc_class)))
        {
            // Class data not found

            command->error = FGC_DEV_NOT_READY;
            queuePush(command->response_queue, command);
            command = NULL;
            continue;
        }

        // Parse command

        parserParse(parser);
    }

    logPrintf(0, "PARSER parserRun(): parser->run = %d, exiting thread.\n", parser->run);

    // Send errors to any remaining commands

    while((command = (struct Cmd *)queuePop(&parser->command_queue)))
    {
        command->error = FGC_DEV_NOT_READY;
        queuePush(command->response_queue, command);
        command = NULL;
    }

    return 0;
}



/*
 * Parse a command
 */

static void parserParse(struct Parser *parser)
{
    struct Cmd  *command = parser->current_command;
    uint32_t    *command_value_bin_length = NULL;
    uint32_t    i;
    struct prop *property;
    uint32_t    old_num_elements;
    char        property_name[FGC_MAX_PROP_LEN + 1];
    uint32_t    parser_to;
    uint32_t    from_user = 0;
    uint32_t    to_user   = FGC_TIMING_NUM_USERS;
    bool        all_users = false;

    std::string cmd_upper_case;
    bool        cmd_is_powerspy = false;

    // Identify property

    if(!(property = parserIdentifyProperty(parser)))
    {
        // Property not found

        parser->current_command = NULL;
        queuePush(command->response_queue, command);
        command = NULL;
        return;
    }

    // Force user if it's set (it discards the user from the command string)
    if (command->force_user >= 0)
    {
        command->user = command->force_user;
    }

    // Set a pointer to the command property

    parser->cmd_prop = property;

    // Timestamp the start of command parsing

    timingGetUserTime(0, &parser->timestamp);

    // Process either get or set command

    switch(command->type)
    {
        // Get command

        case CMD_GET:
        case CMD_GET_SUB:
        case CMD_SUB:

            // Initialise variables

            command->value[0]       = '\0';
            command->value_length   = 0;

            // Zero parents index so that labels of child properties do not include those of their parents

            parser->parents_index = 0;

            // Identify get options

            if(parserIdentifyGetOptions(parser))
            {
                // Failed to identify get options

                break;
            }

            // Set acquisition timestamp
            timingGetUserTime(0, &command->acq_timestamp);
            timingGetUserTime(0, &command->req_timestamp);

            // If the property is a setting, then zero the cycle timestamp
            if (property->is_setting)
            {
                command->zero_cycle_timestamp = true;
            }

            // Don't set cycle timestamp if the property is not PPM
            if (!(property->flags & PF_PPM))
            {
                command->zero_cycle_timestamp = true;
            }

            // Check if the command is a PowerSpy get command (this needs different behaviour for user ALL)
            cmd_upper_case = std::string(command->command_string);
            std::transform(cmd_upper_case.begin(), cmd_upper_case.end(), cmd_upper_case.begin(), ::toupper);

            cmd_is_powerspy = !(strncmp(cmd_upper_case.c_str(), "LOG.SPY.DATA", strlen("LOG.SPY.DATA")));

            // Prepare to handle ALL USERS (only for PPM properties)
            if((command->user == ALL_USERS) && (property->flags & PF_PPM) && (!cmd_is_powerspy))
            {
                // All users - save initial value of array 'to' index
                parser_to = parser->to;
                all_users = true;
            }
            else if (property->flags & PF_PPM)
            {
                // Single user for a PPM property
                from_user = to_user = command->user;
            }
            else
            {
                // Not a PPM property
                from_user = to_user = 0;
            }

            for(uint32_t user = from_user ; user <= to_user ; user++)
            {
                command->user = user;

                if(parserConfigProp(parser, property))
                {
                    break;
                }

                if(all_users)
                {
                    if(command->user > 0 && command->value_length < CMD_MAX_VAL_LENGTH)
                    {
                        // When getting all users, add newline delimiter between responses

                        command->value[command->value_length++] = '\n';
                    }

                    // When getting all users, add the {user}: prefix before the content

                    command->value_length += snprintf(command->value + command->value_length, CMD_MAX_VAL_LENGTH - command->value_length,"%u:", command->user);

                    // Reset array 'to' index to initial value

                    parser->to = parser_to;
                }

                // Set cycle timestamp if a user was supplied

                if(command->user)
                {
                    timingGetUserTime(command->user, &command->cycle_timestamp);
                }

                // Do nothing if get of unspecified element of empty array
                // Otherwise get property

                if(!parserConfigGet(parser, property))
                {
                    if(property->type == PT_PARENT || parser->from < parserGetNumEls(parser))
                    {
                        // Check whether BIN get option is set

                        if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
                        {
                            // Prepend 0xFF (to indicate binary) and data length to value

                            command->value[command->value_length++]  = 0xFF;
                            command_value_bin_length                 = (uint32_t *)&command->value[command->value_length];
                            command->value_length                   += sizeof(*command_value_bin_length);
                            *command_value_bin_length                = command->value_length;
                        }

                        // Execute property's get function

                        parser->class_data->get_func[property->get_func_idx](command, property);

                        // Handle binary and ASCII values

                        if(parser->getopt_flags & GET_OPT_BIN)
                        {
                            // Set binary value length = the current value_length - the value_length before we imported the binary data

                            *command_value_bin_length = htonl(command->value_length - *command_value_bin_length);
                        }
                        else // Bin get option was not set
                        {
                            // Suppress trailing new-line if property was a parent

                            if(!(parser->getopt_flags & GET_OPT_BIN) &&
                               property->type == PT_PARENT           &&
                               command->value_length)
                            {
                                command->value[--command->value_length] = '\0';
                            }
                        }
                    }
                    else if(parser->value == NULL)
                    {
                        // Exception to call the accessor function for properties which do not have a data structure associated with
                        // them, yet are not of type PT_NULL

                        parser->class_data->get_func[property->get_func_idx](command, property);
                    }
                }
            }
            break;

        // Set command

        case CMD_SET:
        case CMD_SET_BIN:

            // Prepare to access the property

            if(parserConfigProp(parser, property))
            {
                break;
            }

            // Configure set command

            if(parserConfigSet(parser, property))
            {
                // Failed to configure set command

                break;
            }

            // Zero command index ready to parse value

            command->index = 0;

            // Check for attempt to send binary data to function other than SetBin or SetRBACToken

            if(command->type == CMD_SET_BIN &&
               (parser->class_data->set_func[property->set_func_idx] != SetBin &&
                parser->class_data->set_func[property->set_func_idx] != SetRBACToken))
            {
                command->error = FGC_UNEXPECTED_BIN_DATA;
                break;
            }

            // Store number of elements prior to the set

            old_num_elements = parserGetNumEls(parser);

            // Execute the set function

            parser->class_data->set_func[property->set_func_idx](command, property);

            // Call equipment device callback for transactional sets

            if(command->transaction_id != 0 && !command->error)
            {
                transactionSet(command, property);
            }

            // Log SET commands in the Event Log if a callback has been defined

            if(parser->class_data->evtlog_func != NULL)
            {
                parser->class_data->evtlog_func(command, property);
            }

            // Stop SET command processing if an error was reported

            if(command->error)
            {
                break;
            }

            // Check whether property has a parameter function

            if(property->pars_idx)
            {
                // Lock the device's parameter mutex

                pthread_mutex_lock(&command->device->parameter_mutex);

                // Set a bit within the device's parameter mask to indicate that a
                // real-time thread should call the corresponding parameter function

                command->device->parameter_mask |= 1 << (property->pars_idx - 1);

                // Unlock the device's parameter mutex

                pthread_mutex_unlock(&command->device->parameter_mutex);
            }

            // Check whether a configuration property has been set
            // This is not secure because an array config property without DONT_SHRINK could
            // be emptied and then set, and each time it would decrement config_unset_count.
            // It is simply hoped that this won't happen during the initialisation phase.
            // The correct implementation will require a dynamic property flag per device, which
            // is not yet supported.

            if((property->flags & PF_CONFIG) != 0 && command->device->config_unset_count > 0)
            {
                const uint32_t new_num_elements = parserGetNumEls(parser);

               if (   ((property->flags & PF_DONT_SHRINK) == 0 && old_num_elements == 0 && new_num_elements != 0)
                   || ((property->flags & PF_DONT_SHRINK) != 0 && old_num_elements != property->max_elements && new_num_elements == property->max_elements))
                {
                    command->device->config_unset_count--;
                }
            }

            // For non-transactional sets, trigger non-volatile storage and CMW publication

            if(command->transaction_id == 0) // Command is not transactional
            {
                // Construct property name

                property_name[0] = '\0';

                // Add parents' symbols to name

                for(i = 0 ; i < parser->parents_index ; i++)
                {
                    strncat(property_name,
                            parser->class_data->sym_tab_prop[parser->parents[i]].key.c,
                            FGC_MAX_PROP_LEN-strlen(property_name));
                    strncat(property_name, ".", FGC_MAX_PROP_LEN-strlen(property_name));
                }

                // Add property's symbol to name

                strncat(property_name,
                        parser->class_data->sym_tab_prop[property->sym_idx].key.c,
                        FGC_MAX_PROP_LEN-strlen(property_name));

                // Trigger non-volatile storage of property value if needed

                if(  command->client != CMD_FGCD_CLIENT &&
                     property->flags  & PF_NON_VOLATILE &&
                   !(property->flags  & PF_TRANSACTIONAL))
                {
                    // Use user 0 if it is non-ppm

                    nonvolStore(command->device,
                                property_name,
                                (property->flags & PF_PPM) ? command->user : 0,
                                (property->flags & PF_NON_VOL_LAST) ? 1 : 0,
                                (property->flags & PF_CONFIG) ? 1 : 0);
                }

                // Notify CMW publication that the value has been modified, if not a transactional set

                cmwpubNotify(parser->current_command->device->id,
                             property_name, command->user,
                             parser->from, parser->to,
                             1, CMWPUB_UPDATE_FLAGS_IMMEDIATE);
            }

            break;

        // Subscription cancellation

        case CMD_UNSUB:
            command->error = FGC_OK_NO_RSP;
            break;
    }

    // Push the command onto its response queue

    parser->current_command = NULL;
    queuePush(command->response_queue, command);
    command = NULL;
}



/*
 * Identify next property in command string
 */

static struct prop *parserIdentifyProperty(struct Parser *parser)
{
    struct Cmd  *command    = parser->current_command;
    char        delimiter   = '\0';
    uint32_t    i;
    int32_t     match;
    struct prop *property;
    struct prop *sub_property;
    uint32_t    sym_index;

    // Find first symbol (top-level property) in command string

    command->index = 0;

    if(!(sym_index = parserIdentifyNextSymbol(parser, command->command_string,
                                              &command->index, command->command_length,
                                              parser->class_data->prop_hash, ".<([ ", 1, &delimiter)))
    {
        // Command contained no symbols

        command->error = FGC_UNKNOWN_SYM;
        return NULL;
    }

    // Get pointer to top-level property

    property = (struct prop *)parser->class_data->sym_tab_prop[sym_index].value;

    // Check that symbol is a top-level property

    if(!property)
    {
        command->error = FGC_UNKNOWN_SYM;
        return NULL;
    }

    // Find remaining symbols (low-level properties) in command string

    parser->parents_index = 0;

    while(property->type == PT_PARENT && delimiter == '.' &&
          (sym_index = parserIdentifyNextSymbol(parser, command->command_string,
                                                &command->index, command->command_length,
                                                parser->class_data->prop_hash, ".<([ ", 1, &delimiter)))
    {
        // Check whether maximum number of parents has been reached

        if(parser->parents_index == PARSER_MAX_PROP_LEVELS)
        {
            command->error = FGC_UNKNOWN_SYM;
            return NULL;
        }

        // Add symbol index to parents

        parser->parents[parser->parents_index++] = property->sym_idx;

        // Search for the sub-property within the parent

        sub_property = (struct prop *)property->value;

        for(match = 0, i = 0 ; !match && i < property->num_elements ; i++)
        {
            if(sub_property->sym_idx == sym_index)
            {
                match = 1;
            }
            else
            {
                sub_property++;
            }
        }

        // Check whether the property was found

        if(match)
        {
            property = sub_property;
        }
        else
        {
            command->error = FGC_UNKNOWN_SYM;
            return NULL;
        }
    }

    // Check whether an error occurred

    if(command->error)
    {
        return NULL;
    }

    // Check if last symbol was unknown

    if(delimiter == '.')
    {
        command->error = FGC_UNKNOWN_SYM;
        return NULL;
    }

    // Set indices to defaults

    parser->from    = UNSPECIFIED;
    parser->to      = UNSPECIFIED;
    parser->step    = 1;
    parser->user    = 0;

    // Handle a tranaction id specifier: <ID>

    if(delimiter == '<')
    {
        if(parserIdentifyTransaction(parser, property, &delimiter))
        {
            return NULL;
        }
    }

    // Handle a user specifier: (USER)

    if(delimiter == '(')
    {
        if(parserIdentifyUser(parser, property, &delimiter))
        {
            return NULL;
        }
    }

    // Handle an array specifier: [FROM,TO,STEP]

    if(delimiter == '[')
    {
        if(parserIdentifyArray(parser, property, &delimiter))
        {
            return NULL;
        }
    }

    // Check for trailing junk

    if(delimiter != ' ' && delimiter != '\0')
    {
        command->error = FGC_UNKNOWN_SYM;
        return NULL;
    }

    // Success - return pointer to the property

    return property;
}



/*
 * Identify a transaction in the command string: <ID>
 */

static int32_t parserIdentifyTransaction(struct Parser *parser, struct prop *property, char *command_next_char)
{
    struct Cmd *command = parser->current_command;

    // Scan parameter which must be 0-65535 to be a valid transaction ID

    int32_t transaction_id;
    parserScanInt(parser, command->command_string,
                  &command->index, command->command_length,
                  const_cast<char*>(">"), 0, NULL, 0, 0, 1, 0, UINT16_MAX,
                  &transaction_id);

    if(command->error)
    {
        command->error = FGC_INVALID_TRANSACTION_ID;
        return 1;
    }

    command->transaction_id = transaction_id;

    // If a transaction ID was supplied, check that the property is transactional

    if(transaction_id != 0 && !(property->flags & PF_TRANSACTIONAL))
    {
        command->error = FGC_PROPERTY_NOT_TRANSACTIONAL;
        return 1;
    }

    // Set next command character to the character after '>'

    if(command->index < command->command_length)
    {
        *command_next_char = command->command_string[command->index++];
    }
    else
    {
        *command_next_char = '\0';
    }

    return 0;
}



/*
 * Identify a user in the command string: (USER)
 */

static int32_t parserIdentifyUser(struct Parser *parser, struct prop *property, char *command_next_char)
{
    struct Cmd *command = parser->current_command;

    // Check whether user is alphabetic (specified by name rather than number)

    if(command->command_string[command->index] == ')' &&
       command->type == CMD_GET &&
       property->type != PT_PARENT &&
       ((property->flags & PF_PPM) != 0 || parser->is_ppm))
    {
        // For get commands, allow "property()" to mean (ALL USERS)

        command->user = ALL_USERS;
        command->index++;
    }
    else if(isalpha(command->command_string[command->index]))
    {
        // Index is a user name, attempt to resolve it

        command->user = parserIdentifyNextSymbol(parser, command->command_string,
                                                 &command->index, command->command_length,
                                                 timing.user_line_hash, ")", 0, NULL);

        // Check whether an error has occurred

        if(!command->user || command->error || command->user > FGC_TIMING_NUM_USERS)
        {
            command->error = FGC_UNKNOWN_CYCLE;
            return 1;
        }
    }
    else // User is numeric
    {
        // Scan user number

        parserScanInt(parser, command->command_string,
                      &command->index, command->command_length,
                      const_cast<char*>(")"), 0, NULL, 0, 0, 1, 0, FGC_TIMING_NUM_USERS,
                      (int32_t *)&command->user);
        parser->user = command->user;

        // Check whether an error has occurred

        if(command->error)
        {
            command->error = FGC_BAD_CYCLE_SELECTOR;
            return 1;
        }
    }

    // Set next command character to the character after ')'

    if(command->index < command->command_length)
    {
        *command_next_char = command->command_string[command->index++];
    }
    else
    {
        *command_next_char = '\0';
    }

    return 0;
}



/*
 * Identify an array range in the command string: [FROM,TO,STEP]
 */

static int32_t parserIdentifyArray(struct Parser *parser, struct prop *property, char *command_next_char)
{
    struct Cmd  *command = parser->current_command;
    char        delimiter;

    // If the PF_FGC flag is set, then check for an alphabetic array index

    if((property->flags & PF_FGC) &&
       isalpha(command->command_string[command->index]))
    {
        // Index is an FGC name, attempt to resolve it

        parser->from = parserIdentifyNextSymbol(parser, command->command_string,
                                                &command->index, command->command_length,
                                                devname.name_hash, ",]", 0, &delimiter);

        // Check whether an error has occured, or the FGCD device was resolved (illegal)

        if(command->error || parser->from == 0)
        {
            command->error = FGC_BAD_ARRAY_IDX;
            return 1;
        }
    }
    else
    {
        // Scan for the first index

        parserScanInt(parser, command->command_string,
                      &command->index, command->command_length,
                      const_cast<char*>(",]"), 0, &delimiter, 0, 0, 0, 0, 0,
                      (int32_t *)&parser->from);

        switch(command->error)
        {
            case 0:  // Valid index found
                break;

            case FGC_NO_SYMBOL:
                command->error  = 0;
                parser->from    = 0;
                break;

            default: // An error has occured
                command->error = FGC_BAD_ARRAY_IDX;
                return 1;
        }
    }

    // Check whether a second index is expected

    if(delimiter == ',')
    {
        if((property->flags & PF_FGC) &&
           isalpha(command->command_string[command->index]))
        {
            // Index is an FGC name, attempt to resolve it

            parser->to = parserIdentifyNextSymbol(parser, command->command_string,
                                                  &command->index, command->command_length,
                                                  devname.name_hash, ",]", 0, &delimiter);

            // Check whether an error has occured, or the FGCD device was resolved (illegal)

            if(command->error || parser->to == 0)
            {
                command->error = FGC_BAD_ARRAY_IDX;
            }
        }
        else
        {
            // Scan the second index

            parserScanInt(parser, command->command_string,
                          &command->index, command->command_length,
                          const_cast<char*>(",]"), 0, &delimiter, 0, 1, 0, parser->from, 0,
                          (int32_t *)&parser->to);
        }

        switch(command->error)
        {
            case 0:  // Valid index found
                break;

            case FGC_NO_SYMBOL:
                command->error  = 0;
                parser->to      = UNSPECIFIED;
                break;

            default: // An error has occured
                command->error = FGC_BAD_ARRAY_IDX;
                return 1;
        }
    }
    else // A single array entry was requested
    {
        parser->to = parser->from;
    }

    // Check whether a step is expected

    if(delimiter == ',')
    {
        // Scan the step

        parserScanInt(parser, command->command_string,
                      &command->index, command->command_length,
                      const_cast<char*>("]"), 0, &delimiter, 0, 1, 0, 1, 0,
                      (int32_t *)&parser->step);

        switch(command->error)
        {
            case 0:  // Valid step found
                break;

            case FGC_NO_SYMBOL:
                command->error  = 0;
                parser->step    = 1;
                break;

            default: // An error has occured
                command->error = FGC_BAD_ARRAY_IDX;
                return 1;
        }
    }
    else // No step was specified
    {
        parser->step = 1;
    }

    // Set next command character to the character after ']'

    if(command->index < command->command_length)
    {
        *command_next_char = command->command_string[command->index++];
    }
    else
    {
        *command_next_char = '\0';
    }

    return 0;
}



/*
 * Identify get options from command string
 */

static int32_t parserIdentifyGetOptions(struct Parser *parser)
{
    struct Cmd  *command = parser->current_command;
    char        delimiter;
    uint32_t    getopt;
    uint32_t    getopt_idx;

    // Clear get option flags

    parser->getopt_flags = 0;

    // Clear DATA get option arguments

    parser->getopt_num_data_args = 0;
    memset(&parser->getopt_data_args, 0, sizeof(parser->getopt_data_args));

    // Process get options

    while((getopt_idx = parserIdentifyNextSymbol(parser, command->command_string,
                                                 &command->index, command->command_length,
                                                 parser_globals.getopt_hash, " |", 1, &delimiter)))
    {
        getopt = sym_tab_getopt[getopt_idx].value;

        // Check that the get option has not already been specified

        if(parser->getopt_flags & getopt)
        {
            command->error = FGC_BAD_GET_OPT;
            return 1;
        }

        // The get option is valid, so add it to the flags

        parser->getopt_flags |= getopt;

        // Check whether the get option has arguments

        if(delimiter == '|')
        {
            // Only the DATA get option should take arguments

            if(getopt != GET_OPT_DATA)
            {
                command->error = FGC_BAD_GET_OPT;
                return 1;
            }

            // Extract each DATA get option argument

            while(command->index < command->command_length &&
                  delimiter != ' ' && delimiter != '\0')
            {
                // Check whether too many arguments were supplied

                if(parser->getopt_num_data_args >= PARSER_DATA_GETOPT_MAX_ARGS)
                {
                    command->error = FGC_BAD_GET_OPT;
                    return 1;
                }

                // Extract the next argument

                char *c = &command->command_string[command->index];
                parser->getopt_data_args[parser->getopt_num_data_args] = c;

                for( ;
                    command->index < command->command_length && *c != ',' && *c != ' ' && *c != '\0' ;
                    c++, command->index++);

                // Store and move past the delimiter

                delimiter = *c;
                command->index++;

                // Null-terminate the argument

                *c = '\0';

                // Increment the count of DATA get option arguments

                parser->getopt_num_data_args++;
            }
        }
    }

    // Check whether an error occurred

    if(command->error)
    {
        command->error = FGC_BAD_GET_OPT;
        return 1;
    }

    // Check if unsupported options are specified

    if((parser->getopt_flags & GET_OPT_IDX)     ||
       (parser->getopt_flags & GET_OPT_INFO)    ||
       (parser->getopt_flags & GET_OPT_SIZE))
    {
        command->error = FGC_BAD_GET_OPT;
        return 1;
    }

    return 0;
}

void parserIncNumElsPtr(struct Parser *parser, size_t size, uint32_t index)
{
    parser->num_elements_ptr = reinterpret_cast<uintptr_t*>(reinterpret_cast<char*>(parser->num_elements_ptr) + (size * index));
}

static uint32_t parserIdentifyNextSymbolWithoutParser(const char *prop_name,
                                            uint32_t          *index,
                                            uint32_t          length,
                                            struct hash_table *hash_table,
                                            const char        *delimiters,
                                            int32_t           null_delim,
                                            char              *delim_match)
{
    char                *c = const_cast<char*>(prop_name);
    uint32_t            d;
    struct hash_entry   *entry;
    uint32_t            i;
    uint32_t            match_found     = 0;
    size_t              num_delims      = strlen(delimiters);
    char                symbol[HASH_MAX_KEY_LEN + 1];

    // Check whether the end of the string has already been reached

    if(*index >= length)
    {
        return 0;
    }

    // Move to indexed point in string

    c += *index;

    // Add NULL to delimiters if requested

    if(null_delim) num_delims++;

    for(i = 0 ;
        !match_found && i < sizeof(symbol) && *index <= length ;
        (*index)++, i++)
    {
        // Copy and capitalise symbol

        symbol[i] = toupper(*c++);

        // Check for match to delimiters

        for(d = 0 ; !match_found && d < num_delims ; d++)
        {
            if(symbol[i] == delimiters[d])
            {
                // Symbol found

                if(delim_match)
                {
                    *delim_match = delimiters[d];
                }

                // Null terminate symbol

                symbol[i]   = '\0';
                match_found = 1;
            }
        }
    }

    // Check whether a delimiter was not matched

    if(!match_found) return 0;

    // Look up symbol in hash

    if(hash_table && (entry = hashFind(hash_table, symbol))) // Symbol found
    {
        return entry->index;
    }
    else // Symbol not in hash table
    {
        return 0;
    }
}

/*
 * Retrieve the property structure given the property name
 */

struct prop* parserRetrieveProperty(struct Parser *parser, const char* prop_name)
{
    uint32_t    parse_index = 0;
    uint32_t    parse_parents_index = 0;
    char        delimiter   = '\0';
    uint32_t    i;
    int32_t     match;
    struct prop *property;
    struct prop *sub_property;
    uint32_t    sym_index;

    // Find first symbol (top-level property) in command string

    if(!(sym_index = parserIdentifyNextSymbolWithoutParser(prop_name,
                                                            &parse_index, strlen(prop_name),
                                                            parser->class_data->prop_hash, ".<([ ", 1, &delimiter)))
    {
        // Command contained no symbols
        return NULL;
    }

    // Get pointer to top-level property

    property = (struct prop *)parser->class_data->sym_tab_prop[sym_index].value;

    // Check that symbol is a top-level property

    if(!property)
    {
        return NULL;
    }

    // Find remaining symbols (low-level properties) in command string

    parse_parents_index = 0;

    while(property->type == PT_PARENT && delimiter == '.' &&
          (sym_index = parserIdentifyNextSymbolWithoutParser(prop_name,
                                                            &parse_index, strlen(prop_name),
                                                            parser->class_data->prop_hash, ".<([ ", 1, &delimiter)))
    {
        // Check whether maximum number of parents has been reached

        if(parse_parents_index == PARSER_MAX_PROP_LEVELS)
        {
            return NULL;
        }

        // Add symbol index to parents

        parse_parents_index++;

        // Search for the sub-property within the parent

        sub_property = (struct prop *)property->value;

        for(match = 0, i = 0 ; !match && i < property->num_elements ; i++)
        {
            if(sub_property->sym_idx == sym_index)
            {
                match = 1;
            }
            else
            {
                sub_property++;
            }
        }

        // Check whether the property was found

        if(match)
        {
            property = sub_property;
        }
        else
        {
            return NULL;
        }
    }

    if(delimiter == '.')
    {
        return NULL;
    }

    return property;

}


/*
 * Call property set parameter functions
 */

void parserCallPropSetParsFunctions(uint32_t channel)
{
    struct FGCD_device &device = fgcd.device[channel];

    // If the device parameter mask is non-zero then process the contents

    if(device.parameter_mask != 0)
    {
        // Get and clear the device parameter mask with mutex protection

        pthread_mutex_lock(&device.parameter_mutex);

        uint32_t parameter_mask = device.parameter_mask;

        device.parameter_mask = 0;

        pthread_mutex_unlock(&device.parameter_mutex);

        // Call the parameter-specific functions

        struct Parser_class_data *class_data = parserGetClassData(device.fgc_class);

        for(uint32_t pars_index = 0; parameter_mask != 0; parameter_mask >>= 1, pars_index++)
        {
            if((parameter_mask & 1) != 0)
            {
                class_data->pars_func[pars_index](channel);
            }
        }
    }
}

// EOF
