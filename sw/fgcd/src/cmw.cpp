/*!
 * @file   cmw.cpp
 * @brief  Functions for interfacing with the Controls Middleware (CMW)
 * @author Stephen Page
 */

#define __STDC_FORMAT_MACROS

#include <cstdio>
#include <cstring>
#include <inttypes.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <cmw-data/DataExceptions.h>
#include <cmw-data/DataFactory.h>
#include <cmw-fwk/server/service/FrameworkServerBuilder.h>
#include <cmw-rbac/authorization/AuthorizationReport.h>
#include <cmw-rbac/authorization/AuthorizationReportUtils.h>
#include <cmw-util/config/ConfigurationBuilder.h>

#include <cmd.h>
#include <cmdqmgr.h>
#include <cmw.h>
#include <cmwpub.h>
#include <cmwutil.h>
#include <consts.h>
#include <defconst.h>
#include <devname.h>
#include <fgcd.h>
#include <fgc_errs.h>
#include <fgcddev.h>
#include <logging.h>
#include <pub.h>
#include <queue.h>
#include <rbac.h>
#include <timing.h>
#include <fgcd_thread.h>

#include <sub_cmwdata_gen.h>

using namespace cmw;
using namespace cmw::data;
using namespace cmw::rda3::common;
using namespace cmw::util;



// Global variables

struct CMW_globals cmw_globals = {};



// Static functions

static FGCD_thread_func cmwRunServer;
static FGCD_thread_func cmwProcessResponses;

template <typename datatype>
static int32_t packBuffer(char *buffer, uint32_t &buf_len, uint32_t buf_max, const char *fmt, uint32_t num_vals, const datatype *vals);



/*
 * Constructor
 */

cmwFgcd::cmwFgcd(const char *name)
{
    using namespace std;

    // Initialise mutexes

    if(fgcd_mutex_init(&set_buffer_mutex, CMW_MUTEX_POLICY)           != 0 ||
       fgcd_mutex_init(&subscriber_mutex, CMW_MUTEX_POLICY)           != 0 ||
       fgcd_mutex_init(&cmwutil.response_buf_mutex, CMW_MUTEX_POLICY) != 0 )
    {
        throw(runtime_error("Failed to set mutex attribute protocol"));
    }

    // Create the CMW configuration

    unique_ptr<ConfigurationBuilder> configBuilder = ConfigurationBuilder::newInstance();

    char value[8];

    snprintf(value, sizeof(value), "%d", CMW_THREAD_PRIORITY_MIN);
    configBuilder->setProperty("cmw.rda3.server.thread.priority.min",    value);

    snprintf(value, sizeof(value), "%d", CMW_THREAD_PRIORITY_NORM);
    configBuilder->setProperty("cmw.rda3.server.thread.priority.normal", value);

    snprintf(value, sizeof(value), "%d", CMW_THREAD_PRIORITY_MAX);
    configBuilder->setProperty("cmw.rda3.server.thread.priority.max",    value);

    const unsigned CMW_SERVER_HBTIMEOUT_MS = 6000;
    snprintf(value, sizeof(value), "%u", CMW_SERVER_HBTIMEOUT_MS);
    configBuilder->setProperty("cmw.rda3.transport.server.hbTimeout",    value);

    ConfigurationSharedPtr config = configBuilder->build();

    // Create the CMW server

    unique_ptr<FrameworkServerBuilder> server_builder = FrameworkServerBuilder::newInstance();
    server_builder->setRbacEnabled(true);
    server_builder->setRbacAccessCheckEnabled(false);
    server_builder->setRbacMcsCheckEnabled(false);
    server_builder->setServerName(name);
    server_builder->setConfig(config);
    server_builder->setConnectionCallback(*this);
    server_builder->setRequestReplyCallback(*this);
    server_builder->setSubscriptionCallback(*this);
    server_builder->setDeviceInfoCallback(*this);
    server = server_builder->build();

    // Create RBAC factory. It is later used to get a validation callback to perform MCS check in rbac.cpp

    auto builder(cmw::util::ConfigurationBuilder::newInstance());
    builder->setProperty(cmw::RbacConfiguration::rbacEnabledProperty, "true");
    builder->setProperty(cmw::RbacConfiguration::rbacAccessCheckEnabledProperty, "false");
    builder->setProperty(cmw::RbacConfiguration::rbacMcsCheckEnabledProperty, "true");
    builder->setProperty(cmw::RbacConfiguration::rbacCheckPolicyProperty, "strict");
    std::unique_ptr<cmw::RbacConfiguration> rbacConfig(new cmw::RbacConfiguration(builder->build()));

    cmw_globals.rbacFactory = cmw::RbacCallbackFactory::create(std::move(rbacConfig), name, *this);
}



/*
 * Destructor
 */

cmwFgcd::~cmwFgcd(void)
{
    pthread_mutex_destroy(&set_buffer_mutex);
}



void cmwFgcd::runServer(void)
{
    server->start();
}



void cmwFgcd::stopServer(void)
{
    server->shutdown();
}



void cmwFgcd::clientConnected(const ServerConnection& connection)
{
    const RemoteHostInfo& client = connection.getClientInfo();

    logPrintf(0, "CMW Connect %s@%s:%d:%s\n",
              client.getUserName().c_str(),
              client.getAddress().getHost().c_str(),
              client.getProcessId(),
              client.getProcessName().c_str());

    fgcddev.status.cmw_clients++;
}



void cmwFgcd::clientDisconnected(const ServerConnection& connection)
{
    const RemoteHostInfo& client = connection.getClientInfo();

    logPrintf(0, "CMW Disconnect %s@%s:%d:%s\n",
              client.getUserName().c_str(),
              client.getAddress().getHost().c_str(),
              client.getProcessId(),
              client.getProcessName().c_str());

    fgcddev.status.cmw_clients--;
}



RdaPriority cmwFgcd::provideSessionPriority(const Session & session)
{
    return RP_NORMAL;
}



void cmwFgcd::get(std::unique_ptr<GetRequest> request)
{
    cmw_globals.cmds_received++;

    struct Cmd *command = prepareCommand(*request, CMD_GET, NULL);
    sendCommand(command, new cmw_command_data(request.release(), 1));
}



void cmwFgcd::set(std::unique_ptr<SetRequest> request)
{
    cmw_globals.cmds_received++;

    // Handle single and complex values

    Data& value = request->getData();

    if(value.size() == 0 || value.exists("value")) // Single value
    {
        struct Cmd *command = prepareCommand(*request, CMD_SET, value.getEntry("value"));
        sendCommand(command, new cmw_command_data(request.release(), 1));
    }
    else // Composite value
    {
        SetRequest          *request_p      = request.release();
        cmw_command_data    *command_data   = new cmw_command_data(request_p, value.size());

        // Send a command for each field in the composite value

        std::vector<const Entry *> entries = value.getEntries();
        for(std::vector<const Entry *>::const_iterator entry = entries.begin() ;
            entry != entries.end() ; entry++)
        {
            struct Cmd *command = prepareCommand(*request_p, CMD_SET, *entry);
            sendCommand(command, command_data);
        }
    }
}



void cmwFgcd::subscribe(SubscriptionRequest& request)
{
    uint32_t                            array_end;
    uint32_t                            array_start;
    uint32_t                            array_step;
    uint32_t                            channel;
    const RemoteHostInfo&               client                    = request.getSession().getConnection().getClientInfo();
    const char                          *device                   = request.getDeviceName().c_str();
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
    uint32_t                            getopt_bin;
#endif
    struct CMWpub_sub_params            params;
    const char                          *property                 = request.getPropertyName().c_str();
    char                                property_name[FGC_MAX_PROP_LEN+1];
    void                                *rbac_token               = NULL;
    uint32_t                            sub_device;
    uint16_t                            transaction_id;
    uint32_t                            user_num                  = 0;
    uint32_t                            error                     = 0;
    std::string                         error_str;

    // Check property length

    if(strlen(property) > FGC_MAX_PROP_LEN)
    {
        throw(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_SYNTAX_ERROR]));
    }

    // Copy and capitalise property

    for(uint32_t i = 0 ; i < sizeof(property_name) ; i++)
    {
        property_name[i] = (char)toupper(property[i]);

        if(property[i] == '\0')
        {
            break;
        }
    }
    property_name[sizeof(property_name) - 1] = '\0';

    // Extract command details

    try
    {
        cmwutilExtractCommandDetails(request.getContext(), device,
                                     &channel, &sub_device,
                                     &transaction_id,
                                     &user_num,
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
                                     &getopt_bin,
#endif
                                     &array_start, &array_end, &array_step);
    }
    catch(const RdaException& e)
    {
        logPrintf(0, "CMW Subscription to %s:%s from %s@%s:%d:%s failed with error (%s)\n",
                  device,
                  property_name,
                  client.getUserName().c_str(),
                  client.getAddress().getHost().c_str(),
                  client.getProcessId(),
                  client.getProcessName().c_str(),
                  e.what());
        throw(e);
    }

    logPrintf(channel, "CMW Subscribe %s(%u) received from %s@%s:%d:%s\n",
              property_name,
              user_num,
              client.getUserName().c_str(),
              client.getAddress().getHost().c_str(),
              client.getProcessId(),
              client.getProcessName().c_str());

    // Extract RBAC token

    if(fgcd.enable_rbac)
    {
        try
        {
            rbac_token = (void *)cmwutilExtractRBACToken(request.getSession().getContext().getData());
        }
        catch(...) // Failed to extract token
        {
            logPrintf(channel, "CMW Failed to extract RBAC token from subscription request\n");
            throw(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_BAD_RBAC_TOKEN]));
        }
    }

    // Check is a subscription to the status or to a generic property
    // special case: class 92 accepts subscriptions to SUB_51 (see EPCCCS-4035)
    if(  !strcmp("SUB", property_name) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == fgcd.device[channel].fgc_class)) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == 51 && fgcd.device[channel].fgc_class == 92)) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == 91 && fgcd.device[channel].fgc_class == 94)))
    {
            // Perform RBAC authorisation

        if(fgcd.enable_rbac)
        {
            // Initialise a command to use for authorisation

            struct Cmd command;
            char   cmd_value[CMD_MAX_VAL_LENGTH + 1];

            command.value = cmd_value;
            cmdqmgrInitCmd(&command);

            command.type            = CMD_SUB;
            command.command_length  = snprintf(command.command_string, sizeof(command.command_string),
                                               "SUB_%" PRIu8, fgcd.device[channel].fgc_class);
            command.device          = &fgcd.device[channel];
            command.rbac_token      = rbac_token;
            command.response_queue  = NULL;

            cmw_command_data command_data{&request, 0};
            cmw_command_data* raw_command_data = &command_data;

            // Store the address of the command data in the command's tag
            memcpy(command.tag, &raw_command_data, sizeof(raw_command_data));
            command.tag_length = sizeof(raw_command_data);

            // Perform RBAC authorisation

            if(rbacAuthorise(&command))
            {
                rbacClearCommandToken(&command);

                error_str = fgc_errmsg[command.error];
                if(command.error_in_value)
                {
                    error_str += std::string(" \n") + command.value;
                }

                logPrintf(channel, "CMW Request failed [%s]: %s\n", command.command_string, error_str.c_str());
                throw(ServerException(std::string("FGC Device: ") + error_str));
            }
            else
            {
                rbacClearCommandToken(&command);
            }
        }

        // Validate the subscription

        SubscriptionSourceSharedPtr source = request.accept().startPublishing();

        // Return an exception if the device is offline or not of the correct class

        struct fgc_stat *fgc_status = &pub.published_data.status[channel];

        if(!(fgc_status->data_status & FGC_DATA_VALID) ||
           !(fgc_status->data_status & FGC_CLASS_VALID))
        {
            source->notify(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_DEV_NOT_READY]));
        }
    }
    else // Not status data
    {
       // Configure subscription parameters

#ifdef MARKED_FOR_DELETION_EPCCCS_2623
       params.getopt_bin = getopt_bin;
#endif
        params.start_index = array_start;
        params.end_index = array_end;
        params.step = array_step;
        params.sub_device = sub_device;
        params.transaction_id = transaction_id;
        params.user = user_num;

        std::pair<uint32_t, Cmd*> subscribe_res = cmwpubSubscribe(channel, property_name, &params, request, rbac_token);

        error        = subscribe_res.first;
        Cmd* command = subscribe_res.second;

        if(error)
        {
            error_str = fgc_errmsg[error];

            if(command)
            {
                if(command->error_in_value)
                {
                    error_str += std::string(" \n") + command->value;
                }

                // Delete RBAC token

                rbacClearCommandToken(command);

                // Return the command structure

                queuePush(&cmdqmgr.free_cmd_q, command);
                command = nullptr;
            }

            logPrintf(channel, "CMW Subscription to %s(%u) from %s@%s:%d:%s failed\n",
                      property_name,
                      user_num,
                      client.getUserName().c_str(),
                      client.getAddress().getHost().c_str(),
                      client.getProcessId(),
                      client.getProcessName().c_str());
            throw(ServerException(std::string("FGC Device: ") + error_str));
        }
    }

    // Increment subscription count

    fgcddev.status.cmw_subs++;
}



void cmwFgcd::subscriptionSourceAdded(const SubscriptionSourceSharedPtr& source)
{
    using namespace std;

    uint32_t                                                 array_end;
    uint32_t                                                 array_start;
    uint32_t                                                 array_step;
    uint32_t                                                 channel;
    char                                                     command_string[CMD_MAX_LENGTH + 1];
    const char                                               *device                               = source->getDeviceName().c_str();
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
    uint32_t                                                 getopt_bin;
#endif
    struct CMWpub_sub_params                                 params;
    const char                                               *property                             = source->getPropertyName().c_str();
    char                                                     property_name[FGC_MAX_PROP_LEN+1];
    uint32_t                                                 sub_device;
    cmw_subscriber                                           *subscriber                           = NULL;
    std::set<cmw_subscriber*>::iterator                      subscriber_iterator;
    cmwpub_subscription                                      *subscription    = NULL;
    multimap<string, cmwpub_subscription *>::iterator        subscription_iter;
    pair<multimap<string, cmwpub_subscription *>::iterator,
         multimap<string, cmwpub_subscription *>::iterator>  subscription_range;
    uint16_t                                                 transaction_id;
    uint32_t                                                 user_num                             = 0;

    // Copy and capitalise property

    for(uint32_t i = 0 ; i < sizeof(property_name) ; i++)
    {
        property_name[i] = (char)toupper(property[i]);

        if(property[i] == '\0')
        {
            break;
        }
    }
    property_name[sizeof(property_name) - 1] = '\0';

    // Extract command details

    try
    {
        cmwutilExtractCommandDetails(source->getContext(), device,
                                     &channel, &sub_device,
                                     &transaction_id,
                                     &user_num,
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
                                     &getopt_bin,
#endif
                                     &array_start, &array_end, &array_step);
    }
    catch(const RdaException& e)
    {
        logPrintf(0, "CMW Failed to add subscription source for %s/%s: %s\n",
            device,
            property_name,
            e.what());
        return;
    }

    logPrintf(0, "CMW Subscription source addition received for %s/%s(%u)\n",
        device,
        property_name,
        user_num);


    // Check is a subscription to the status or to a generic property
    // special case: class 92 accepts subscriptions to SUB_51 (see EPCCCS-4035)
    if(  !strcmp("SUB",property_name) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == fgcd.device[channel].fgc_class)) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == 51 && fgcd.device[channel].fgc_class == 92)) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == 91 && fgcd.device[channel].fgc_class == 94)))
    {
        // Property name valid

        pthread_mutex_lock(&subscriber_mutex);

       for(subscriber_iterator  = subscribers[channel].begin() ; subscriber_iterator != subscribers[channel].end(); subscriber_iterator++)
       {
           subscriber = *subscriber_iterator;

           // Check if the current source exists

           if (subscriber->source->getId() == source->getId())
           {
               break;
           }
           subscriber = NULL;
       }

       // If the source does not exist create a new subscripotion

       if(subscriber == NULL)
       {
           cmw_subscriber *subscriber = new cmw_subscriber(source, transaction_id, user_num, sub_device);

           subscribers[channel].insert(subscriber);

           logPrintf(0, "CMW Subscriber source added for %s/%s(%u)\n",
               device,
               property_name,
               user_num);

       }

       pthread_mutex_unlock(&subscriber_mutex);

    }
    else // Not status data
    {
        // Search for a subscription matching the request

        pthread_mutex_lock(&cmwpub.subscription_mutex);

        subscription_range = cmwpub_subs[channel].equal_range(property_name);
        for(subscription_iter = subscription_range.first ; subscription_iter != subscription_range.second ; subscription_iter++)
        {
            subscription = subscription_iter->second;

            // Check whether the subscription matches the request

            if(subscription->source->getId() == source->getId())
            {
                break;
            }
            subscription = NULL;
        }

        // Check whether the subscription was found

        if(!subscription) // Subscription not found
        {
            // Configure subscription parameters

#ifdef MARKED_FOR_DELETION_EPCCCS_2623
            params.getopt_bin     = getopt_bin;
#endif
            params.start_index    = array_start;
            params.end_index      = array_end;
            params.step           = array_step;
            params.sub_device     = sub_device;
            params.transaction_id = transaction_id;
            params.user           = user_num;

            cmwpubBuildCommandString(property_name, &params, command_string);

            // Create a new subscription

            subscription = new cmwpub_subscription(&params, command_string, source);

            // Add the subscription to the subscription set

            cmwpub_subs[channel].insert
                (pair<string, cmwpub_subscription *>(property_name, subscription));

            logPrintf(0, "CMW Subscription source added for %s/%s(%u)\n",
                device,
                property_name,
                user_num);
        }

        pthread_mutex_unlock(&cmwpub.subscription_mutex);

    }
}



void cmwFgcd::subscriptionSourceRemoved(const SubscriptionSourceSharedPtr& subscription)
{
    uint32_t                  channel;
    const char               *cycle      = subscription->getContext().getSelector().c_str();
    const char               *device     = subscription->getDeviceName().c_str();
    struct CMWpub_sub_params  params;
    const char               *property   = subscription->getPropertyName().c_str();
    char                     property_name[FGC_MAX_PROP_LEN+1];
    uint32_t                  sub_device;
    uint16_t                  transaction_id;
    uint32_t                  user_num   = 0;

    // Copy and capitalise property

    for(uint32_t i = 0 ; i < sizeof(property_name) ; i++)
    {
        property_name[i] = (char)toupper(property[i]);

        if(property[i] == '\0')
        {
            break;
        }
    }
    property_name[sizeof(property_name) - 1] = '\0';

    // Set cycle to NULL if the selector is a zero-length string

    if(cycle[0] == '\0')
    {
        cycle = NULL;
    }

    // Extract command details

    try
    {
        cmwutilExtractCommandDetails(subscription->getContext(), device,
                                     &channel, &sub_device,
                                     &transaction_id,
                                     &user_num,
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
                                     NULL,
#endif
                                     NULL, NULL, NULL);
    }
    catch(const ServerException& e)
    {
        logPrintf(0, "CMW Subscription source removal from %s/%s(%u) failed with error (%s)\n",
                  device, property_name, user_num, e.what());
        return;
    }

    logPrintf(channel, "CMW Subscription source removal request for %s(%u) received\n",
              property_name,
              user_num);

    // Handle status data and properties
    // special case: class 92 accepts subscriptions to SUB_51 (see EPCCCS-4035)
    if(  !strcmp("SUB",property_name) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == fgcd.device[channel].fgc_class)) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == 51 && fgcd.device[channel].fgc_class == 92)) ||
        (!strncmp("SUB_", property_name, 4) && (atoi(&property_name[4]) == 91 && fgcd.device[channel].fgc_class == 94)))
    {
        // Unsubscribe from device status data

        if(remove_subscription(channel, *subscription))
        {
            // Failed to remove subscription from expected channel
            // Search all channels (device may have moved channels since subscription)

            for(channel = 0 ;
                channel < FGCD_MAX_DEVS &&
                remove_subscription(channel, *subscription) ;
                channel++);

            // Check whether subscriber was not found

            if(channel == FGCD_MAX_DEVS)
            {
                logPrintf(channel, "CMW Unsubscribe from unknown subscription source\n");
                return;
            }
        }
    }
    else // Not status data
    {
        uint32_t error = FGC_SUB_NOT_PERM;

        // Configure subscription parameters

#ifdef MARKED_FOR_DELETION_EPCCCS_2623
        params.getopt_bin       = 0;
#endif
        params.start_index      = 0;
        params.end_index        = UNSPECIFIED;
        params.step             = UNSPECIFIED;
        params.sub_device       = sub_device;
        params.transaction_id   = transaction_id;
        params.user             = user_num;

        error = cmwpubUnsubscribe(channel, property_name, &params, *subscription);

        if(error)
        {
            logPrintf(channel, "CMW Unsubscription from %s(%u) failed: %s\n",
                      property_name,
                      user_num,
                      fgc_errmsg[error]);
            return;
        }
    }
}



int32_t cmwFgcd::remove_subscription(uint32_t channel, const SubscriptionSource& subscription)
{
    std::set<cmw_subscriber*>::iterator subscriber_set_iterator;

    pthread_mutex_lock(&subscriber_mutex);

    // Find subscriber within subscriber set

    for(subscriber_set_iterator  = subscribers[channel].begin() ;
        subscriber_set_iterator != subscribers[channel].end() &&
        (*subscriber_set_iterator)->source->getId() != subscription.getId() ;
        subscriber_set_iterator++);

    // Check whether subscriber was found

    if(subscriber_set_iterator == subscribers[channel].end())
    {
        // Subscriber was not found

        pthread_mutex_unlock(&subscriber_mutex);
        return 1;
    }

    // Remove subscriber from subscriber set

    delete *subscriber_set_iterator;
    subscribers[channel].erase(subscriber_set_iterator);

    pthread_mutex_unlock(&subscriber_mutex);
    return 0;
}



void cmwFgcd::unsubscribe(const cmw::rda3::common::Request& request)
{
    // Decrement subscription count

    fgcddev.status.cmw_subs--;
}



struct Cmd *cmwFgcd::prepareCommand(const ServerRequest& request, enum Cmd_type type, const Entry *entry)
{
    // Get a command structure

    cmw_globals.num_cmds++;
    struct Cmd *command = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);

    command->type = type;

    // Insert the device name into the command structure

    const char *device_name = request.getDeviceName().c_str();

    // Check whether the device name exceeds the maximum length

    if(strlen(device_name) > FGC_MAX_DEV_LEN) // Device name too long
    {
        command->error = FGC_DEV_NAME_TOO_LONG;
        return command;
    }

    strncpy(command->device_name, device_name, FGC_MAX_DEV_LEN);
    command->device_name[FGC_MAX_DEV_LEN] = '\0';

    // Construct the FGC command

    if(constructCommandString(request, type, entry, command))
    {
        // An error occurred

        return command;
    }

    // Handle the command's RBAC token

    command->rbac_token = NULL;

        if(fgcd.enable_rbac)
        {
            // Get RBAC token

            try
            {
                command->rbac_token = (void *)cmwutilExtractRBACToken(request.getSession().getContext().getData());
            }
            catch(...) // Failed to extract token
            {
                logCommand(request.getSession().getConnection().getClientInfo(), command, CMD_GET);
                logPrintf(0, "CMW Failed to extract RBAC token from command\n");
                command->error = FGC_BAD_RBAC_TOKEN;
                return command;
            }
        }

    return command;
}



uint32_t cmwFgcd::constructCommandString(const ServerRequest&   request,
                                               enum Cmd_type    type,
                                         const Entry           *entry,
                                               struct Cmd      *command)
{
    uint32_t                array_end;
    uint32_t                array_start;
    uint32_t                array_step;
    const RemoteHostInfo&   client          = request.getSession().getConnection().getClientInfo();
    const RequestContext&   context         = request.getContext();
    uint32_t                device;
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
    uint32_t                getopt_bin;
#endif
    const char              *property       = request.getPropertyName().c_str();
    uint32_t                sub_device;

    // Extract command details

    try
    {
        cmwutilExtractCommandDetails(context, command->device_name,
                                     &device, &sub_device,
                                     &command->transaction_id,
                                     &command->user,
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
                                     &getopt_bin,
#endif
                                     &array_start, &array_end, &array_step);
    }
    catch(const RdaException& e)
    {
        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;
        throw(e);
    }

    command->device = &fgcd.device[device];

    // Add property to command

    uint32_t new_cmd_len = command->command_length + strlen(property);

    // Check whether the maximum command length would be exceeded

    if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
    {
        command->error = FGC_CMD_BUF_FULL;
        return 1;
    }

    strncpy(&command->command_string[command->command_length],
            property, CMD_MAX_LENGTH - command->command_length);
    command->command_string[CMD_MAX_LENGTH] = '\0';
    command->command_length                 = new_cmd_len;

    // Add field to command

    if(entry)
    {
        const char *entry_name = entry->getName();

        // Add the entry name if it is not "value"

        if(strcmp(entry_name, "value")) // entry_name is not "value"
        {
            new_cmd_len = command->command_length + 1 + strlen(entry_name);

            // Check whether the maximum command length would be exceeded

            if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
            {
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

            command->command_length += snprintf(&command->command_string[command->command_length],
                                                (CMD_MAX_LENGTH + 1) - command->command_length,
                                                ".%s", entry_name);
        }
    }

    // Add transaction ID if defined

    if(command->transaction_id)
    {
        new_cmd_len = command->command_length + 2 + 5; // 5 is the maximum string length of a uint16_t

        if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
        {
            logCommand(client, command, command->type);
            command->error = FGC_CMD_BUF_FULL;
            return 1;
        }

        command->command_length += snprintf(&command->command_string[command->command_length],
                                            (CMD_MAX_LENGTH + 1) - command->command_length,
                                            "<%hu>", command->transaction_id);
    }

    // Set user if defined

    if(command->user)
    {
        new_cmd_len = command->command_length + 2 + 10; // 10 is the maximum string length of a uint32_t

        if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
        {
            logCommand(client, command, command->type);
            command->error = FGC_CMD_BUF_FULL;
            return 1;
        }

        command->command_length += snprintf(&command->command_string[command->command_length],
                                            (CMD_MAX_LENGTH + 1) - command->command_length,
                                            "(%u)", command->user);
    }

    // Check whether array parameters are defined

    if(array_start  != UNSPECIFIED ||
       array_end    != UNSPECIFIED ||
       array_step   != UNSPECIFIED)
    {
        new_cmd_len = command->command_length + 1;

        if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
        {
            logCommand(client, command, command->type);
            command->error = FGC_CMD_BUF_FULL;
            return 1;
        }

        command->command_length += snprintf(&command->command_string[command->command_length],
                                            (CMD_MAX_LENGTH + 1) - command->command_length,
                                            "[");

        // Check whether an array start index is defined

        if(array_start != UNSPECIFIED)
        {
            new_cmd_len = command->command_length + 11 + 1; // 11 is the maximum string length of a long

            if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
            {
                logCommand(client, command, command->type);
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

            command->command_length += snprintf(&command->command_string[command->command_length],
                                                (CMD_MAX_LENGTH + 1) - command->command_length,
                                                "%u,", array_start);
        }
        else // No array start was specified
        {
            new_cmd_len = command->command_length + 1;

            if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
            {
                logCommand(client, command, command->type);
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

            command->command_length += snprintf(&command->command_string[command->command_length],
                                                (CMD_MAX_LENGTH + 1) - command->command_length,
                                                ",");
        }

        // Check whether an array end index is defined

        if(array_end != UNSPECIFIED)
        {
            new_cmd_len = command->command_length + 11; // 11 is the maximum string length of a long

            if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
            {
                logCommand(client, command, command->type);
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

           command->command_length += snprintf(&command->command_string[command->command_length],
                                                (CMD_MAX_LENGTH + 1) - command->command_length,
                                                "%u,", array_end);
        }
        else // No array end was specified
        {
            new_cmd_len = command->command_length + 1;

            if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
            {
                logCommand(client, command, command->type);
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

            command->command_length += snprintf(&command->command_string[command->command_length],
                                                (CMD_MAX_LENGTH + 1) - command->command_length,
                                                ",");
        }

        // Check whether an array step is defined

        if(array_step != UNSPECIFIED)
        {
            new_cmd_len = command->command_length + 1 + 11; // 11 is the maximum string length of a long

            if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
            {
                logCommand(client, command, command->type);
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

           command->command_length += snprintf(&command->command_string[command->command_length],
                                                (CMD_MAX_LENGTH + 1) - command->command_length,
                                                "%u", array_step);
        }

        new_cmd_len = command->command_length + 1;

        if(new_cmd_len > CMD_MAX_LENGTH) // Command too long
        {
            logCommand(client, command, command->type);
            command->error = FGC_CMD_BUF_FULL;
            return 1;
        }

        command->command_length += snprintf(&command->command_string[command->command_length],
                                            (CMD_MAX_LENGTH + 1) - command->command_length,
                                            "]");
    }

    // Handle different command types

    const Data& filters = context.getFilters();

    switch(type)
    {
        case CMD_GET:
            // Append type and label get options to command

            new_cmd_len = command->command_length + 9; // 9 is length of " type lbl"
            if(new_cmd_len < CMD_MAX_LENGTH)
            {
                command->command_length += snprintf(&command->command_string[command->command_length],
                                                    (CMD_MAX_LENGTH + 1) - command->command_length,
                                                    " type lbl");
            }
            else // Command too long
            {
                logCommand(client, command, CMD_GET);
                command->error = FGC_CMD_BUF_FULL;
                return 1;
            }

            // Get options from the filters.

            if(filters.exists("options"))
            {
                const char *options;

                try
                {
                    options = filters.getString("options");
                }
                catch(const DataException& e)
                {
                    logCommand(client, command, CMD_GET);
                    command->error = FGC_BAD_GET_OPT;
                    return 1;
                }

                size_t options_length   = strlen(options);
                new_cmd_len             = command->command_length + 1 + options_length;

                if(new_cmd_len <= CMD_MAX_LENGTH)
                {
                    command->command_length += snprintf(&command->command_string[command->command_length],
                                                        (CMD_MAX_LENGTH + 1) - command->command_length,
                                                        " %s",
                                                        options);
                }
                else // Command too long
                {
                    logCommand(client, command, CMD_GET);
                    command->error = FGC_CMD_BUF_FULL;
                    return 1;
                }
            }

            break;

        case CMD_SET:

            // Check whether a value was supplied

            if(entry)
            {
                // Add the value to the command

                if(dataEntryToCmd(*entry, command))
                {
                    logCommand(client, command, type);
                    return 1;
                }
            }
            break;

        default:
            command->error = FGC_NOT_IMPL;
            logPrintf(command->device->id,
                      "CMW Unhandled command type %d when constructing command string\n",
                      type);
            return 1;
    }

    logCommand(client, command, type);
    return 0;
}



/*
 * Generic function to pack a typed array into set_buffer.
 *
 * @param[out] buffer      Output buffer
 * @param[out] buf_len     Number of characters written to the output buffer, excluding the NULL terminator
 * @param[in]  buf_max     Size of the output buffer
 * @param[in]  fmt         Format string for snprintf
 * @param[in]  num_vals    Number of elements in vals
 * @param[in]  vals        Typed array of values
 */

template <typename datatype>
static int32_t packBuffer(char *buffer, uint32_t &buf_len, uint32_t buf_max, const char *fmt, uint32_t num_vals, const datatype *vals)
{
    buf_len = 0;

    for(uint32_t i = 0 ; i < num_vals ; i++)
    {
        int32_t value_len = snprintf(&buffer[buf_len], buf_max, fmt, vals[i]);

        // Check for an encoding error

        if(value_len < 0) return FGC_BAD_PARAMETER;

        // Check whether end of buffer has been reached

        if(static_cast<unsigned int>(value_len) > buf_max) return FGC_CMD_BUF_FULL;

        buf_len += value_len;
        buf_max -= value_len;
    }

    if(buf_len > 0) --buf_len;

    buffer[buf_len] = '\0';

    return 0;
}

static int32_t packDiscreteFunction(char *buffer, uint32_t &buf_len, uint32_t buf_max, const DiscreteFunction& function)
{
    const double *xarray   = function.getXArray();
    const double *yarray   = function.getYArray();
    uint32_t      num_vals = static_cast<uint32_t>(function.getSize());

    buf_len = 0;

    for(uint32_t i = 0 ; i < num_vals ; i++)
    {
        int32_t value_len = snprintf(&buffer[buf_len], buf_max, "%.7e|%.7e,", static_cast<float>(xarray[i]),static_cast<float>(yarray[i]));

        // Check for an encoding error

        if(value_len < 0) return FGC_BAD_PARAMETER;

        // Check whether end of buffer has been reached

        if(static_cast<unsigned int>(value_len) > buf_max) return FGC_CMD_BUF_FULL;

        buf_len += value_len;
        buf_max -= value_len;
    }

    // Add zero termination

    if(buf_len > 0) --buf_len;

    buffer[buf_len] = '\0';

    return 0;
}

static int32_t packDiscreteFunctionList(char *buffer, uint32_t &buf_len, uint32_t buf_max, const DiscreteFunctionList& flist)
{
    size_t nfuncs   = flist.getSize();
    double xoffset  = 0;

    // Ignore the first function for ADE since they add an artificial ramp of 0.5 ms
    // that cannot be handled (to be investigated why)

    size_t j = (fgcd.machine == MACHINE_ADE && nfuncs > 0 ? 1 : 0);

    for(; j != nfuncs ; j++)
    {
        const    DiscreteFunction& function = flist.getFunction(j);
        uint32_t num_vals                   = static_cast<uint32_t>(function.getSize());

        const double *xarray                = function.getXArray();
        const double *yarray                = function.getYArray();

        for(uint32_t i = 0 ; i < num_vals ; i++)
        {
            int32_t value_len = snprintf(&buffer[buf_len], buf_max, "%.7e|%.7e,", static_cast<float>(xarray[i]+xoffset),static_cast<float>(yarray[i]));

            // Check for an encoding error

            if(value_len < 0) return FGC_BAD_PARAMETER;

            // Check whether end of buffer has been reached

            if(static_cast<unsigned int>(value_len) > buf_max) return FGC_CMD_BUF_FULL;

            buf_len += value_len;
            buf_max -= value_len;
        }

        // Update time offset

        if(num_vals>0)
        {
          xoffset += xarray[num_vals-1];
        }
    }

    // Add zero termination

    if(buf_len > 0) --buf_len;

    buffer[buf_len] = '\0';

    return 0;
}

uint32_t cmwFgcd::dataEntryToCmd(const Entry& entry, struct Cmd *command)
{
    uint32_t        buf_len = 0;    // initialised to zero to suppress warning
    unsigned long   num_vals;

    const int8_t    *byte_vals;
    const double    *double_vals;
    const float     *float_vals;
    const int32_t   *int_vals;
    const int64_t   *long_vals;
    const int16_t   *short_vals;
    const char      **string_vals;
    const char      *value;

    pthread_mutex_lock(&set_buffer_mutex);

    // Handle possible data types

    switch(entry.getType())
    {
        case DT_BYTE:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%" PRId8, entry.getByte());
            break;

        case DT_BOOL:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%d", entry.getBool()?1:0);
            break;

        case DT_BYTE_ARRAY:
            byte_vals      = entry.getArrayByte(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%" PRId8 ",", num_vals, byte_vals);
            break;

        case DT_DOUBLE:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%.17e", entry.getDouble());
            break;

        case DT_DOUBLE_ARRAY:
            double_vals    = entry.getArrayDouble(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%.17e,", num_vals, double_vals);
            break;

        case DT_FLOAT:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%.7e", entry.getFloat());
            break;

        case DT_FLOAT_ARRAY:
            float_vals     = entry.getArrayFloat(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%.7e,", num_vals, float_vals);
            break;

        case DT_INT:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%" PRId32, entry.getInt());
            break;

        case DT_INT_ARRAY:
            int_vals       = entry.getArrayInt(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%" PRId32 ",", num_vals, int_vals);
            break;

        case DT_LONG:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%" PRId64, entry.getLong());
            break;

        case DT_LONG_ARRAY:
            long_vals      = entry.getArrayLong(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%" PRId64 ",", num_vals, long_vals);
            break;

        case DT_SHORT:
            buf_len = snprintf(set_buffer, sizeof(set_buffer), "%" PRId16, entry.getShort());
            break;

        case DT_SHORT_ARRAY:
            short_vals     = entry.getArrayShort(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%" PRId16 ",", num_vals, short_vals);
            break;

        case DT_STRING:
            value   = entry.getString();
            buf_len = strlen(value);

            if(buf_len <= sizeof(set_buffer) - 1)
            {
                strcpy(set_buffer, value);
            }
            else // Command too long, report error
            {
                command->error = FGC_CMD_BUF_FULL;
            }
            break;

        case DT_STRING_ARRAY:
            string_vals    = entry.getArrayString(num_vals);
            command->error = packBuffer(set_buffer, buf_len, sizeof(set_buffer), "%s,", num_vals, string_vals);
            break;

        case DT_DISCRETE_FUNCTION:
            command->error = packDiscreteFunction(set_buffer, buf_len, sizeof(set_buffer), entry.getDiscreteFunction());
            break;

        case DT_DISCRETE_FUNCTION_LIST:
            command->error = packDiscreteFunctionList(set_buffer, buf_len, sizeof(set_buffer), entry.getDiscreteFunctionList());
            break;

        default: // An unexpected type has been received
            command->error = FGC_NOT_IMPL;
            logPrintf(command->device->id, "CMW Unimplemented type %d for field \"%s\"\n",
                      entry.getType(), entry.getName());
            break;
    }

    // Copy buffer to command

    if(!command->error && buf_len > 0)
    {
        uint32_t new_val_len = command->value_length + 1 + buf_len;

        if(new_val_len <= CMD_MAX_VAL_LENGTH)
        {
            command->value_length += snprintf(&command->value[command->value_length],
                                              (CMD_MAX_VAL_LENGTH + 1) - command->value_length,
                                              "%s", set_buffer);
        }
        else // Command too long, report error
        {
            command->error = FGC_CMD_BUF_FULL;
        }
    }

    pthread_mutex_unlock(&set_buffer_mutex);
    return command->error > 0;
}



void cmwFgcd::sendCommand(struct Cmd *command, cmw_command_data *command_data)
{
    // Store the address of the command data in the command's rag

    memcpy(command->tag, &command_data, sizeof(command_data));
    command->tag_length = sizeof(command_data);

    // Check whether an error occurred preparing the command

    if(command->error)
    {
        // Push command directly onto response queue

        queuePush(&cmw_globals.response_queue, command);
    }
    else // No error has yet occurred
    {
        // Queue the command for the device

        command->response_queue = &cmw_globals.response_queue;
        cmdqmgrQueue(command);
    }
}



void cmwFgcd::logCommand(const RemoteHostInfo&  client,
                         const struct Cmd       *command,
                         const enum Cmd_type    cmd_type)
{
    if(cmd_type == CMD_GET)
    {
        logPrintf(command->device->id, "CMW Command from %s@%s:%d (G %s:%s %s)\n",
                  client.getUserName().c_str(),
                  client.getAddress().getHost().c_str(),
                  client.getProcessId(),
                  command->device_name,
                  command->command_string,
                  command->value);
    }
    else
    {
        logPrintf(command->device->id, "CMW Command from %s@%s:%d (S %s:%s %s)\n",
                  client.getUserName().c_str(),
                  client.getAddress().getHost().c_str(),
                  client.getProcessId(),
                  command->device_name,
                  command->command_string,
                  command->value);

    }
}



void cmwFgcd::publish(void)
{
    using namespace std;

    uint32_t                            channel;
    struct fgc_stat                     *fgc_status;
    char                                property_name[FGC_MAX_PROP_LEN + 1];
    std::set<cmw_subscriber*>::iterator subscriber_set_iterator;
    struct timespec                     timeout;

    // Check whether CMW server is running

    if(cmw_globals.server == NULL) return;

    // Attempt to lock the subscriber mutex
    // Time-out after half a cycle period

    timeout.tv_sec  = 0;
    timeout.tv_nsec = (FGCD_CYCLE_PERIOD_MS * 1000000) / 2;
    if(pthread_mutex_timedlock(&subscriber_mutex, &timeout))
    {
        logPrintf(0, "CMW Failed to lock subscriber mutex\n");
        return;
    }

    // Iterate through all listeners on all devices

    for(channel = 0 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        fgc_status = &pub.published_data.status[channel];

        // Send data to each subscriber

        for(subscriber_set_iterator  = subscribers[channel].begin()   ;
            subscriber_set_iterator != subscribers[channel].end()     ;
            subscriber_set_iterator++)
        {
            // Check whether data and its class are valid

            if((fgc_status->data_status & FGC_DATA_VALID) &&
               (fgc_status->data_status & FGC_CLASS_VALID))
            {
                // Check whether current user matches subscription

                if(!(*subscriber_set_iterator)->user ||
                    (*subscriber_set_iterator)->user == timing.user)
                {
                    // Populate value for device

                    strncpy(property_name, (*subscriber_set_iterator)->source->getPropertyName().c_str(), FGC_MAX_PROP_LEN+1); //convert to char* !

                    unique_ptr<Data> value = DataFactory::createData();

                    // Call auto-generated function to fill subscription data:

                    subcmwdataFillSubData(value.get(), fgc_status, (int32_t)(*subscriber_set_iterator)->sub_device);

                    // Create a context with the appropriate timestamp

                    unique_ptr<AcquiredContext> context
                        = AcquiredContextFactory::create(((int64_t)ntohl(pub.published_data.time_sec)  * 1000000000) +
                                                         ((int64_t)ntohl(pub.published_data.time_usec) * 1000));

                    // Publish the value

                    (*subscriber_set_iterator)->source->notify(AcquiredData(std::move(value), std::move(context)));
                    (*subscriber_set_iterator)->last_value_type = cmw_subscriber::LAST_VALUE_VALID;
                }
            }
            else if((*subscriber_set_iterator)->last_value_type == cmw_subscriber::LAST_VALUE_VALID)
            {
                // Indicate that data could not be updated

                (*subscriber_set_iterator)->source->notify(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_DEV_NOT_READY]));
                (*subscriber_set_iterator)->last_value_type = cmw_subscriber::LAST_VALUE_ERROR;
            }
        }
    }
    pthread_mutex_unlock(&subscriber_mutex);
}



std::string cmwFgcd::getDeviceClass(const std::string &deviceName)
{
    using namespace std;

    uint32_t channel;
    uint32_t sub_device;
    ostringstream class_name;

    if(devnameSubResolve((char *)deviceName.c_str(), &channel, &sub_device))
    {
        channel    = devnameResolve((char *)deviceName.c_str());
        sub_device = 0;
    }

    if(channel >= 0 && channel <= FGCD_MAX_EQP_DEVS)
    {
        class_name << "FGC_" << to_string(fgcd.device[channel].fgc_class);
        return class_name.str();
    }
    else
    {
        throw(ServerException(std::string("FGC Device: ") + "Cannot resolve device class for device '" + deviceName + "'"));
    }

}



int cmwStart(void)
{
    using namespace std;

    // Return error if FGCD device has no name as name is used for CMW server

    if(fgcd.device[0].name == NULL)
    {
        fprintf(stderr, "ERROR: FGCD device has no name\n");
        fgcddev.status.st_faults |= FGC_FLT_CMW;
        return 1;
    }

    // Initialise the command response queue

    if(queueInit(&cmw_globals.response_queue, cmdqmgr.num_cmds))
    {
        // Failed to initialise the queue

        throw(runtime_error("Failed to initialise the command response queue"));
    }

    // Start the server

    try
    {
        cmw_globals.server = new cmwFgcd(fgcd.server_name);
    }
    catch(const exception& ex)
    {
        cerr << "ERROR: Error creating CMW server: " << ex.what() << endl;
        queueFree(&cmw_globals.response_queue);
        return 1;
    }

    // Start the server thread

    if(fgcd_thread_create(&cmw_globals.server_thread, cmwRunServer, NULL, CMW_SERVER_THREAD_POLICY, CMW_THREAD_PRIORITY_MAX ,NO_AFFINITY) != 0)
    {
        queueFree(&cmw_globals.response_queue);
        throw(runtime_error("Failed to create CMW server thread"));
    }

    // Start response thread

    if(fgcd_thread_create(&cmw_globals.response_thread, cmwProcessResponses, NULL, CMW_PROCESS_RESPONSES_THREAD_POLICY, CMW_THREAD_PRIORITY_MAX ,NO_AFFINITY) != 0)
    {
        queueFree(&cmw_globals.response_queue);
        throw(runtime_error("Failed to create command response thread"));
    }

    return 0;
}



// Static functions



/*
 * CMW server thread
 */

static void *cmwRunServer(void *unused)
{
    using namespace std;

    // Delay to allow devices to come online before the server is started

    // This should be at least long enough to allow FGCs to lock their PLLs
    // so that they can respond when subscriptions are restarted

    sleep(5);

    // Start the CMW server

    try
    {
        cmw_globals.server->runServer();
    }
    catch(const exception& ex)
    {
        cerr << "Error running CMW server: " << ex.what() << endl;
    }

    logPrintf(0, "CMW cmwRunServer(): exiting thread.\n");

    return 0;
}

/*
 * Response processing thread
 */

static void *cmwProcessResponses(void *unused)
{
    struct Cmd *command;

    // Process each command from the response queue

    while(true)
    {
        if((command = reinterpret_cast<struct Cmd*>(queueBlockPop(&cmw_globals.response_queue))) == NULL)
        {
            logPrintf(0, "cmwProcessResponses() could not read response queue, exiting.\n");
            break;
        }

        // Extract the original request from the command's tag

        cmw_command_data *command_data;
        memcpy(&command_data, command->tag, sizeof(command_data));
        command_data->cmd_count--;

        // Handle different types of command

        switch(command->type)
        {
            case CMD_GET:
                {
                    // Get the original client request

                    GetRequest *request = (GetRequest *)command_data->rda_request;

                    // Return the response to the client

                    if(!command->error)
                    {
                        AcquiredData *value;

                        if((value = cmwutilResponseToRdaData(command)))
                        {
                            request->requestCompleted(*value);
                            delete value;
                        }
                        else // Failed to convert the response to RDA data
                        {
                            request->requestFailed(ServerException(std::string("FGC Device: ") + fgc_errmsg[command->error]));
                        }
                    }
                    else // The command returned an error
                    {
                        std::string error_msg = fgc_errmsg[command->error];

                        // Check if there's extra error message in value
                        if(command->error_in_value)
                        {
                            error_msg += std::string(" \n") + command->value;
                        }

                        logPrintf(command->device->id, "CMW Request failed [%s]: %s\n", command->command_string, error_msg.c_str());
                        request->requestFailed(ServerException(std::string("FGC Device: ") + error_msg));
                    }

                    delete command_data;
                    delete request;
                }
                break;

            case CMD_SET:
            case CMD_SET_BIN:
                {
                    // Check whether all FGC commands have been completed

                    if(!command_data->cmd_count)
                    {
                        // Extract the original request from the command's tag

                        SetRequest *request = (SetRequest *)command_data->rda_request;

                        // Check whether an error occurred

                        if(command->error)
                        {
                            std::string error_msg = fgc_errmsg[command->error];

                            // Check if there's extra error message in value
                            if(command->error_in_value)
                            {
                                error_msg += std::string(" \n") + command->value;
                            }

                            logPrintf(command->device->id, "CMW Request failed [%s]: %s\n", command->command_string, error_msg.c_str());
                            request->requestFailed(ServerException(std::string("FGC Device: ") + error_msg));
                        }
                        else // All FGC commands were successful
                        {
                            request->requestCompleted();
                        }

                        delete command_data;
                        delete request;
                    }
                }
                break;

            default:
                command->error = FGC_NOT_IMPL;
                logPrintf(command->device->id, "CMW Unhandled command type %d when processing response\n",
                          command->type);
                delete command_data;
                break;
        }

        // Delete the RBAc Token

        rbacClearCommandToken(command);

        // Return the command structure to the free queue

        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;
        cmw_globals.num_cmds--;
    }

    return 0;
}

// EOF
