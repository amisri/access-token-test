/*!
 * @file   logging.cpp
 * @brief  Functions for logging
 * @author Stephen Page
 */

#include <cstdio>
#include <cstring>
#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>

#include <defconst.h>
#include <definfo.h>
#include <fgcddev.h>
#include <fgcd.h>
#include <logging.h>
#include <queue.h>
#include <timing.h>
#include <version.h>
#include <fgcd_thread.h>

// Use CMW Logging only if CMW is being used

#include <cmw-log/LogManager.h>
#include <cmw-util/Properties.h>
#include <cmw-log/AppenderManager.h>
#include <cmw-log/stomp/StompAppenderFactory.h>

// Global variables

struct Logging logging = {};

// Static functions

static FGCD_thread_func logRun;
static int32_t logInit(void);
static void logWriteEntry(struct Log_entry *entry);
static void logGetTimestamp(struct timeval *timestamp);



int32_t logStart(void)
{
    // One-time initialisation

    if(logInit() != 0) return 1;

    // Create mutex

    if(fgcd_mutex_init(&logging.missed_messages_mutex, ALARMS_MUTEX_POLICY) != 0) return 1;

    // Start logging thread

    return fgcd_thread_create(&logging.thread, logRun, NULL, LOG_THREAD_POLICY, LOG_THREAD_PRIORITY ,NO_AFFINITY);
}

int32_t logInitCmwLog(void)
{
    if(logging.filename[0] != '\0')
    {
        try
        {
            cmw::log::stomp::StompAppenderFactory * factory = new cmw::log::stomp::StompAppenderFactory();
            factory->setSource(fgcd.server_name);

            // Manager takes ownership of factory instance

            cmw::log::AppenderManager::registerAppenderFactory("StompAppender", factory);

            cmw::log::LogManager::init(LOG_CMW_CONFIG_FILENAME);
            atexit(cmw::log::LogManager::finish);
        }
        catch (std::exception& ex)
        {
            fprintf(stderr,"ERROR: Can't initialize cmw-log: %s\n", ex.what());
            return 1;
        }
    }

    return 0;
}


int32_t logPrintf(uint32_t channel, const char *format, ...)
{
    va_list             args;
    struct Log_entry   *entry;
    int32_t             length;

    // If logging is not running, record a missed message and continue

    if(logging.thread == 0)
    {
        pthread_mutex_lock(&logging.missed_messages_mutex);
        logging.missed_messages++;
        pthread_mutex_unlock(&logging.missed_messages_mutex);
        return 0;
    }

    // In case of a wrong channel number, use channel 0

    if (channel < 0 || channel > FGCD_MAX_EQP_DEVS)
    {
        fprintf(stderr,"WARNING: LOGGING Using incorrect channel number (channel = %u)\n",channel);
        channel = 0;
    }

    // Get entry from free queue

    if((entry = reinterpret_cast<struct Log_entry*>(queuePop(&logging.free_queue))) == NULL)
    {
        // No free entries

        pthread_mutex_lock(&logging.missed_messages_mutex);
        logging.missed_messages++;
        pthread_mutex_unlock(&logging.missed_messages_mutex);
        return 0;
    }

    // Set channel, timestamp and log string

    entry->channel = channel;

    logGetTimestamp(&entry->timestamp);

    va_start(args, format);
    length = vsnprintf(entry->buffer, LOG_MESSAGE_LENGTH + 1, format, args);
    va_end(args);

    if(length >= LOG_MESSAGE_LENGTH)
    {
        // If the message did not fit within the buffer, replace final characters with ellipsis to indicate that it is incomplete

        strcpy(entry->buffer + LOG_MESSAGE_LENGTH - 4, "...\n");
    }

    // Append entry to output queue

    queuePush(&logging.queue, entry);

    return length;
}

// Static functions



/*
 * Initialise variables
 */

static int32_t logInit(void)
{
    // Initialise logging queues

    if(queueInit(&logging.free_queue, LOG_QUEUE_LENGTH))
    {
        fprintf(stderr, "ERROR: Failed to initialise logging free queue\n");
        return 1;
    }

    if(queueInit(&logging.queue, LOG_QUEUE_LENGTH))
    {
        fprintf(stderr, "ERROR: Failed to initialise logging queue\n");
        queueFree(&logging.free_queue);
        return 1;
    }

    // Add log entries to free queue

    for(uint32_t i = 0 ; i < LOG_QUEUE_LENGTH ; i++)
    {
        queuePush(&logging.free_queue, &logging.entries[i]);
    }

    return 0;
}

/*
 * Write a single log entry
 */

static void logWriteEntry(struct Log_entry *entry)
{
    struct tm time;
    int32_t   length;

    // Write entry to log

    localtime_r(&entry->timestamp.tv_sec, &time);
    while((length = fprintf(logging.file, "%04d-%02d-%02d %02d:%02d:%02d.%03lu %02u(%s) %s",
                            time.tm_year + 1900, time.tm_mon + 1, time.tm_mday,
                            time.tm_hour, time.tm_min, time.tm_sec, entry->timestamp.tv_usec / 1000,
                            entry->channel, fgcd.device[entry->channel].name,
                            entry->buffer)) < 0)
    {
        // An error occurred while writing entry to log

        fgcddev.status.st_warnings |= FGC_WRN_LOG;
        if(logging.file != LOG_OUTPUT_STREAM)
        {
            // Close and re-open log file

            fclose(logging.file);
            while(!(logging.file = fopen(logging.filename, "a")))
            {
                sleep(1);
            }
            fgcddev.status.st_warnings &= ~FGC_WRN_LOG;
        }
    }
    logging.length += length;
}



/*
 * Thread to run logging
 */

static void *logRun(void *unused)
{
    struct Log_entry   *entry;
    char                filename_old[LOG_MAX_FILENAME_LENGTH + 4 + 1]; // 4 represents ".old"
    struct stat         stat_buf;
    struct Log_entry    missed_entry;

    // Open log file

    if(logging.filename[0] == '\0')
    {
        // No filename specified, log to default output stream

        logging.file = LOG_OUTPUT_STREAM;
    }
    else // A filename for log was specified
    {
        // Create filename_old

        snprintf(filename_old, LOG_MAX_FILENAME_LENGTH + 4 + 1, "%s.old", logging.filename);

        while(!(logging.file = fopen(logging.filename, "a")))
        {
            fgcddev.status.st_warnings |= FGC_WRN_LOG;
            perror("fopen");
            sleep(1);
        }
        fgcddev.status.st_warnings &= ~FGC_WRN_LOG;

        // Stat log file

        fstat(fileno(logging.file), &stat_buf);
        logging.length = stat_buf.st_size;

        // Check file length

        if(LOG_MAX_LENGTH && logging.length >= LOG_MAX_LENGTH)
        {
            // Close log file and rename to <filename>.old

            fclose(logging.file);
            rename(logging.filename, filename_old);

            // Open new log file

            while(!(logging.file = fopen(logging.filename, "w")))
            {
                fgcddev.status.st_warnings |= FGC_WRN_LOG;
                perror("fopen");
                sleep(1);
            }
            fgcddev.status.st_warnings &= ~FGC_WRN_LOG;
            logging.length = 0;
        }
    }

    // Add entry for start of logging

    logPrintf(0, "LOG Start - FGCD class %s version %lu compiled %s\n", FGC_CLASS_ID_STRING, version.time.tv_sec,ctime(&version.time.tv_sec));

    while(true)
    {
        // Read an entry from the queue

        if((entry = reinterpret_cast<struct Log_entry*>(queueBlockPop(&logging.queue))) == NULL)
        {
            fprintf(stderr, "ERROR: logRun() could not read logging queue, exiting.\n");
            break;
        }

        // Check whether any messages were missed

        pthread_mutex_lock(&logging.missed_messages_mutex);
        uint32_t missed_messages = logging.missed_messages;
        pthread_mutex_unlock(&logging.missed_messages_mutex);

        if(missed_messages > 0)
        {
            logGetTimestamp(&missed_entry.timestamp);
            missed_entry.channel = 0;
            snprintf(missed_entry.buffer, LOG_MESSAGE_LENGTH, "LOGGING %u log messages missed (either too many logs per second and/or NFS is slow)\n", missed_messages);
            logWriteEntry(&missed_entry);

            pthread_mutex_lock(&logging.missed_messages_mutex);
            logging.missed_messages = 0;
            pthread_mutex_unlock(&logging.missed_messages_mutex);
        }

        // Write the log entry

        logWriteEntry(entry);

        // Return entry to free queue

        queuePush(&logging.free_queue, entry);

        // Flush the output stream

        fflush(logging.file);

        // Check whether log has reached maximum length

        if(logging.file   != LOG_OUTPUT_STREAM &&
           logging.length >= LOG_MAX_LENGTH    &&
           LOG_MAX_LENGTH >  0)
        {
            // Close log file and rename to <filename>.old

            fclose(logging.file);
            rename(logging.filename, filename_old);

            // Open new log file

            while(!(logging.file = fopen(logging.filename, "w")))
            {
                fgcddev.status.st_warnings |= FGC_WRN_LOG;
                sleep(1);
            }
            fgcddev.status.st_warnings &= ~FGC_WRN_LOG;
            logging.length = 0;
        }
    }

    return 0;
}



/*
 * Get timestamp from logging timing source
 */

static void logGetTimestamp(struct timeval *timestamp)
{
    if(logging.time_func != NULL)
    {
        logging.time_func(timestamp);
    }
    else
    {
        // No time function defined, use current FGCD iteration time

        timingGetUserTime(0, timestamp);
    }
}

// EOF
