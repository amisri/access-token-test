/*!
 * @file   devname.c
 * @brief  Functions for device name resolution
 * @author Stephen Page
 */

#include <assert.h>
#include <cstdio>
#include <cstring>
#include <cctype>
#include <cstdlib>

#include <string>
#include <map>
#include <regex>

#include <consts.h>
#include <defconst.h>
#include <devname.h>
#include <equipdev.h>
#include <fgcddev.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <timing_handlers.h>


// Constants

#define MULTI_PPM_CONDITION      200   //!< Length of the multi-PPM timing condition


// Global variables

struct Devname devname;

int32_t devnameInit(void)
{
    // Initialise name mutex

    if(fgcd_mutex_init(&devname.mutex, DEVNAME_MUTEX_POLICY) != 0) return 1;

    // Initialise name hash

    if(!(devname.name_hash = hashInit(FGCD_MAX_DEVS)))
    {
        fprintf(stderr, "ERROR: Unable to initialise name hash.\n");
        return 1;
    }

    // Initialise sub-device name hash

    if((devname.sub_name_hash = hashInit(FGCD_MAX_SUB_DEVS)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise sub-device name hash.\n");
        return 1;
    }

    // Read device names

    devnameRead();

    return 0;
}



int32_t devnameRead(void)
{
    int                 c               = EOF;
    uint32_t            class_number;
    struct hash_entry   *entry;
    uint32_t            entry_num       = 0;
    char                format[100];
    char                hostname[HOST_NAME_MAX + 1];
    uint32_t            i;
    uint32_t            items_assigned;
    uint32_t            line_number     = 0;
    FILE                *name_file;
    uint16_t            omode_mask;

    // Obtain exclusive access to name hash

    pthread_mutex_lock(&devname.mutex);

    // Clear NAMEFILE warning bit

    fgcddev.status.st_warnings &= ~FGC_WRN_NAMEFILE;

    // Open name file

    if(!(name_file = fopen(DEVNAME_NAME_FILE, "r")))
    {
        perror("Open name file " DEVNAME_NAME_FILE);
        pthread_mutex_unlock(&devname.mutex);
        fgcddev.status.st_faults |= FGC_FLT_CONFIG;
        return 1;
    }

    // Clear name hash

    hashEmpty(devname.name_hash);

    // Un-assign device names

    for(i = 0 ; i < FGCD_MAX_DEVS ; i++)
    {
        if(!fgcd.device[i].name) continue;

        fgcd.device[i].name             = NULL;
        fgcd.device[i].operational_name = NULL;
        fgcd.device[i].fgc_class        = FGC_CLASS_UNKNOWN;
        fgcd.device[i].omode_mask       = 0;
    }

    // Read name file entries

    // Create format from constants

    sprintf(format, "%%%d[^:]:%%u:%%u:%%%d[^:]:0x%%04hX",
            HOST_NAME_MAX, FGC_MAX_DEV_LEN);

    do
    {
        entry = &devname.name_entry[entry_num];

        // Zero the key

        memset(entry->key.c, 0, sizeof(entry->key.c));

        // Read entry

        items_assigned = fscanf(name_file, format, hostname,
                                                   &entry->index,
                                                   &class_number,
                                                   entry->key.c,
                                                   &omode_mask);
        line_number++;

        if(items_assigned == 5)
        {
            // Check for duplicate entry

            if(hashFind(devname.name_hash, entry->key.c))
            {
                fprintf(stderr, "WARNING: Duplicate entry of %s in name file %s at line %u\n",
                        entry->key.c, DEVNAME_NAME_FILE, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }

            // Check whether entry is for this host

            if(!strcmp(hostname, fgcd.hostname))
            {
                // Check that the device name does not begin with a numeric value

                if(isdigit(entry->key.c[0]))
                {
                    fprintf(stderr, "WARNING: Device name %s begins with a numeric value in name file %s at line %u\n",
                            entry->key.c, DEVNAME_NAME_FILE, line_number);
                    fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                    continue;
                }

                // Check that the entry is within the valid range of channels

                if(entry->index >= FGCD_MAX_DEVS)
                {
                    fprintf(stderr, "WARNING: Channel %u out of range (maximum %d) in name file %s at line %u\n",
                            entry->index, FGCD_MAX_DEVS, DEVNAME_NAME_FILE, line_number);
                    fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                    continue;
                }

                // Check that the class is within the valid range

                if(class_number >= FGC_CLASS_UNKNOWN)
                {
                    fprintf(stderr, "WARNING: Class %u out of range (maximum %d) in name file %s at line %u\n",
                            class_number, FGC_CLASS_UNKNOWN - 1, DEVNAME_NAME_FILE, line_number);
                    fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                    continue;
                }

                // Insert entry into name hash table

                hashInsert(devname.name_hash, entry);

                // Store name in device's structure

                // Check that device has not already been assigned a name

                if(fgcd.device[entry->index].name == NULL)
                {
                    fgcd.device[entry->index].name          = entry->key.c;
                    fgcd.device[entry->index].fgc_class     = class_number;
                    fgcd.device[entry->index].omode_mask    = omode_mask;
                }
                else
                {
                    // Channel has already been assigned a name, report error

                    fprintf(stderr, "WARNING: Device %u already has a name in name file %s at line %u\n",
                            entry->index, DEVNAME_NAME_FILE, line_number);
                    fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                    continue;
                }

                entry_num++;
            }

         }
        // Read until end of line or file

         while((c = fgetc(name_file)) != EOF && c != '\n');

    } while(c != EOF && entry_num < FGCD_MAX_DEVS);

    fclose(name_file);

    // Read sub-device names

    devnameSubRead();
    
    // Read MPPM conditions

    devnameMppmRead();

    // Initialise data for FGCD device

    fgcddevInitDevice();

    // Initialise data for equipment devices

    equipdevInitDevices();

    pthread_mutex_unlock(&devname.mutex);

    return 0;
}



int32_t devnameSubRead(void)
{
    int                 c                                  = EOF;
    struct hash_entry   *device_entry;
    char                device_name[FGC_MAX_DEV_LEN + 1];
    struct hash_entry   *entry;
    uint32_t            entry_num                           = 0;
    char                filename[FILENAME_MAX + 1];
    char                format[100];
    uint32_t            items_assigned;
    uint32_t            line_number                         = 0;
    FILE                *name_file;
    char                mppm_condition[MULTI_PPM_CONDITION];

    snprintf(filename, sizeof(filename), "%s/%s", DEVNAME_SUB_NAME_PATH, fgcd.hostname);

    // Attempt to open sub-device name file (may not exist)

    if((name_file = fopen(filename, "r")) == NULL)
    {
        return 0;
    }

    // Clear sub-device name hash

    hashEmpty(devname.sub_name_hash);

    // Clear sub-device pointers for devices

    memset(devname.device_sub_devices, 0, sizeof(devname.device_sub_devices));

    // Read name file entries

    // Create format from constants

    sprintf(format, "%%%d[^:]:%%u:%%%d[^:\n]:%%%d[^:\n]",
            FGC_MAX_DEV_LEN, FGC_MAX_DEV_LEN, MULTI_PPM_CONDITION);

    do
    {
        entry = &devname.sub_name_entry[entry_num];

        // Zero the key

        memset(entry->key.c, 0, sizeof(entry->key.c));

        // Read entry

        items_assigned = fscanf(name_file, format,  device_name,
                                                   &entry->index,
                                                    entry->key.c,
                                                    mppm_condition);
        line_number++;

        if(items_assigned == 3 || items_assigned == 4)
        {
            // Check for duplicate device entry

            if(hashFind(devname.name_hash, entry->key.c) != NULL)
            {
                fprintf(stderr, "WARNING: Sub-device %s in name file %s also exists as device at line %u\n",
                        entry->key.c, DEVNAME_NAME_FILE, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }

            // Check for duplicate sub-device entry

            if(hashFind(devname.sub_name_hash, entry->key.c) != NULL)
            {
                fprintf(stderr, "WARNING: Duplicate entry of %s in name file %s at line %u\n",
                        entry->key.c, DEVNAME_NAME_FILE, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }

            // Check that the device name does not begin with a numeric value

            if(isdigit(entry->key.c[0]))
            {
                fprintf(stderr, "WARNING: Sub-device name %s begins with a numeric value in name file %s at line %u\n",
                        entry->key.c, filename, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }

            // Check that the entry is within the valid range of channels

            if(entry->index < 0 || entry->index > FGCD_MAX_SUB_DEVS_PER_DEV)
            {
                fprintf(stderr, "WARNING: Channel %u out of range (minimum 0, maximum %d) in name file %s at line %u\n",
                        entry->index, FGCD_MAX_SUB_DEVS_PER_DEV, filename, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }

            // Find device owning sub-device

            if((device_entry = hashFind(devname.name_hash, device_name)) == NULL)
            {
                fprintf(stderr, "WARNING: Unknown device %s for sub-device %s in name file %s at line %u\n",
                        device_name, entry->key.c, DEVNAME_NAME_FILE, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }
            entry->value = device_entry->index;

            // Check for duplicate sub-device name

            if(devname.device_sub_devices[device_entry->index][entry->index])
            {
                fprintf(stderr, "WARNING: Device %s sub-device %u already has a name in name file %s at line %u\n",
                        device_entry->key.c, entry->index, filename, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }


            // Check for multi-PPM conditions

            // if(items_assigned == 4)
            // {
            //     timingAddMultiPpmEvent(device_entry->index, mppm_condition);

            //     // Zero the timdt info

            //     memset(mppm_condition,  0, sizeof(mppm_condition));
            // }

            // Set device sub-device pointer

            devname.device_sub_devices[device_entry->index][entry->index] = entry;

            // Insert entry into name hash table

            hashInsert(devname.sub_name_hash, entry);

            // Set operational name in FGCD device structure
            // Do it only once, so virtual devices don't overwrite it with their names 
            
            if(!fgcd.device[device_entry->index].operational_name)
            {
                fgcd.device[device_entry->index].operational_name = entry->key.c;
            }

            entry_num++;
        }

        // Read until end of line or file

        while((c = fgetc(name_file)) != EOF && c != '\n');

    } while(c != EOF && entry_num < FGCD_MAX_SUB_DEVS);

    // Go to either the end of the file, or a non-empty character

    while(isspace(c) && (c = fgetc(name_file)) != EOF);

    assert(c == EOF);

    fclose(name_file);

    return 0;
}

int32_t devnameMppmRead(void)
{
    int                 c                                  = EOF;
    char                filename[FILENAME_MAX + 1];
    char                format[100];
    uint32_t            items_assigned;
    uint32_t            line_number                         = 0;
    FILE                *mppm_file;
    uint32_t            i;
    char                mppm_condition[MULTI_PPM_CONDITION];
    std::map<uint32_t, std::string> mppm_conditions;


    snprintf(filename, sizeof(filename), "%s/%s", DEVNAME_MPPM_PATH, fgcd.hostname);

    // Attempt to open mppm name file (may not exist)

    if((mppm_file = fopen(filename, "r")) == NULL)
    {
        return 0;
    }

    // Read file entries

    // Create format from constants

    sprintf(format, "%%u:%%%d[^\n]", MULTI_PPM_CONDITION);

    do
    {
        // Read entry

        items_assigned = fscanf(mppm_file, format, &i, mppm_condition);
        line_number++;

        if(items_assigned == 2)
        {
            // Check if the entry is not a duplicate

            if(mppm_conditions.find(i) != mppm_conditions.end())
            {
                fprintf(stderr, "WARNING: Duplicate entry of %u in mppm file %s at line %u\n",
                        i, filename, line_number);
                fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                continue;
            }
            mppm_conditions[i] = mppm_condition;

            // Zero the timdt info

            memset(mppm_condition,  0, sizeof(mppm_condition));

        }
        // Read until end of line or file

        while((c = fgetc(mppm_file)) != EOF && c != '\n');
    } while(c != EOF);

        // Go to either the end of the file, or a non-empty character

    while(isspace(c) && (c = fgetc(mppm_file)) != EOF);

    assert(c == EOF);

    fclose(mppm_file);

    for(auto it = mppm_conditions.begin(); it != mppm_conditions.end(); ++it)
    {
        timingAddMultiPpmEvent(it->second.c_str());
    }
    return 0;
}




int32_t devnameResolve(char *name)
{
    int                 address;
    struct hash_entry   *entry;

    // Check whether the first character of the device name is numeric (absolute address)
    // otherwise attempt to resolve

    if(name[0] == '\0') // No name specified (implicitly FGCD device)
    {
        return 0;
    }
    else if(isdigit(name[0])) // Absolute address
    {
        address = atoi(name);

        // Check whether the address is invalid

        if(address < 0 || address > FGCD_MAX_EQP_DEVS)
        {
            return -1;
        }
        return address;
    }

    // Device is specified by name rather than address

    pthread_mutex_lock(&devname.mutex);

    if(!(entry = hashFind(devname.name_hash, name)))
    {
        // Name could not be resolved

        pthread_mutex_unlock(&devname.mutex);
        return -1;
    }
    pthread_mutex_unlock(&devname.mutex);

    // Return device number

    return entry->index;
}



int32_t devnameSubResolve(char *name, uint32_t *channel, uint32_t *sub_channel)
{
    struct hash_entry *entry;
    
    const std::string full_dev_name{name};
    
    std::regex r {"^([a-zA-Z.0-9_]+):(\\d+)$"};
    std::smatch match;

    // Check if name has ':'
    
    if(std::regex_match(full_dev_name, match, r))
    {
        // make sure everything has matched
        
        if(match.size() != 3)
        {
            return -1;
        }
        
        std::string dev_name         = match[1].str();
        std::string sub_channel_str  = match[2].str();
        
        int32_t device_number = devnameResolve(&dev_name[0]);

        if(device_number < 0)
            return -1;
        
        // Return device channel and sub-channel
        
        *channel = device_number;
        *sub_channel = std::stoi(sub_channel_str);

        return 0;
    }

    pthread_mutex_lock(&devname.mutex);

    if((entry = hashFind(devname.sub_name_hash, name)) == NULL)
    {
        // Name could not be resolved

        pthread_mutex_unlock(&devname.mutex);
        return -1;
    }
    pthread_mutex_unlock(&devname.mutex);

    // Return device channel and sub-channel

    *channel        = entry->value;
    *sub_channel    = entry->index;

    return 0;
}




int32_t devnameSubName(char *name, uint32_t size, uint32_t channel, uint32_t sub_channel)
{
    // Check whether requested channel and sub_channel are in range

    if(
        channel     < 0 || channel      > FGCD_MAX_DEVS - 1         ||
        sub_channel < 0 || sub_channel  > FGCD_MAX_SUB_DEVS_PER_DEV
      )
    {
        return 1;
    }

    pthread_mutex_lock(&devname.mutex);

    // Check whether sub-device exists

    if(devname.device_sub_devices[channel][sub_channel])
    {
        // Copy name

        strncpy(name, devname.device_sub_devices[channel][sub_channel]->key.c, size);
        name[size - 1] = '\0';
    }
    else // Sub-device does not exist
    {
        pthread_mutex_unlock(&devname.mutex);
        return 1;
    }
    pthread_mutex_unlock(&devname.mutex);

    return 0;
}

// EOF
