//! @file   equip_liblogger.cpp
//! @brief  This file handles FGC logger and Gateway post-mortem requests

#include <stdbool.h>
#include <stdint.h>

#include <equip_liblog.h>
#include <equip_logger.h>
#include <equipdev.h>
#include <fgcd.h>
#include <logging.h>



// Internal structures, unions and enumerations

//! Internal global variables

struct Logger
{
    bool         enabled;                         //!< FGC logger enabled flag
    uint32_t     synched_log_mask[FGCD_MAX_DEVS]; //!< Bitmaks with logs frozen by the FGC logger (DIM or VsRegDsp)
};



// Internal variable definitions

static struct Logger logger;



// External function definitions

int32_t loggerInit(void)
{
    memset(&logger, 0, sizeof(logger));

    // Initialize the FGC logger

    logger.enabled = fgcd.enable_fgc_logger != 0;

    return 0;
}



bool loggerIsEnabled(void)
{
    return logger.enabled;
}



bool loggerProcess(uint32_t const channel)
{
    // Process events

    struct Equipdev_channel &device = equipdev.device[channel];

    // FGC_UNL_LOG_PLEASE asserted if:
    //
    // * Post-mortem           : logs frozen     - device.log.post_mortem_time.tv_sec != 0
    // * DIM trigger           : logs frozen     - logger.synched_log_mask != 0
    // * VsReg DSP logs trigger: logs frozen     - logger.synched_log_mask != 0

    if(getStatusBit(device.status.st_unlatched, FGC_UNL_LOG_PLEASE))
    {
        if(device.log.post_mortem_time.tv_sec == 0 && logger.synched_log_mask[channel] == 0)
        {
            setStatusBit(&device.status.st_unlatched, FGC_UNL_LOG_PLEASE, false);

            logPrintf(channel, "FGC logger: event completed\n");
        }
    }

    if(    ccLogPMGetFreezeMask(channel) != 0
        && getStatusBit(device.status.st_unlatched, FGC_UNL_LOG_PLEASE) == 0
        && pmIsActive(channel) == false)
    {
        uint8_t menu_idx;

        for(menu_idx = 0; menu_idx < LOG_NUM_MENUS; menu_idx++)
        {
            logMenuStatusResetBitMask(&device.log.menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);
        }

        ccLogPMUnfreeze(channel);

        logPrintf(channel, "PM: logs cleared and unfrozen\n");

        return true;
    }

    return false;
}



void loggerReset(uint32_t const channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Reset the FGC logger event if active

    logPrintf(channel, "FGC logger: event reset\n");

    if(getStatusBit(device.status.st_unlatched, FGC_UNL_LOG_PLEASE))
    {
        device.log.post_mortem_time.tv_sec  = 0;
        device.log.post_mortem_time.tv_usec = 0;

        logger.synched_log_mask[channel] = 0;
    }
}



void loggerFgcLogSync(uint32_t const channel, uint8_t const log_idx)
{
    logPrintf(channel, "FGC logger: log index %u synched\n", log_idx);

    logger.synched_log_mask[channel] &= ~(1 << log_idx);
}



void loggerTriggerPm(uint32_t channel)
{
    if(logger.enabled == true)
    {
        logPrintf(channel, "FGC logger: post-mortem event\n");

        struct Equipdev_channel &device = equipdev.device[channel];

        // Freeze the logs and set LOG.MENU.PM_TIME.

        uint8_t menu_idx;

        for(menu_idx = 0; menu_idx < LOG_NUM_MENUS; menu_idx++)
        {
            if(   logMenuIsSave(&device.log.menus, menu_idx)  == true
               && logMenuIsAlias(&device.log.menus, menu_idx) == false)
            {
                logMenuStatusSetBitMask(&device.log.menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);

                if (menu_idx < LOG_NUM_LIBLOG_MENUS)
                {
                    uint32_t const log_idx = logMenuIndexToLogIndex(&device.log.menus, menu_idx);

                    if (logIsFreezable(&device.log.mgr, log_idx) == true)
                    {
                        ccLogPMFreezeLog(channel, log_idx);
                    }
                }
            }
        }

        timingGetUserTime(0, &device.log.post_mortem_time);

        setStatusBit(&device.status.st_unlatched, FGC_UNL_LOG_PLEASE, true);
    }
}



void loggerTriggerDim(uint32_t const channel, uint32_t const menu_idx)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Freeze the DIM log and set the SAVE flag

    if(logger.enabled == true)
    {
        if(logMenuIsSave(&device.log.menus, menu_idx) == true)
        {
            logMenuStatusSetBitMask(&device.log.menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);

            uint8_t const log_idx = logMenuIndexToLogIndex(&device.log.menus, menu_idx);

            logPrintf(channel, "FGC logger: DIM event for log index: %d\n", log_idx);

            if (logIsFreezable(&device.log.mgr, log_idx) == true)
            {
                ccLogPMFreezeLog(channel, log_idx);
            }

            logger.synched_log_mask[channel] |= (1 << log_idx);

            setStatusBit(&device.status.st_unlatched, FGC_UNL_LOG_PLEASE, true);
        }
    }
}


// EOF
