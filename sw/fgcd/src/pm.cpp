/*!
 * @file   pm.cpp
 * @brief  Functions for submitting Post Mortem data
 * @author Stephen Page
*/

#include <pm.h>

void pmPubUpdate(uint8_t channel, struct fgc_stat *status, uint32_t time_sec, uint32_t time_usec)
{
}


bool pmIsActive(uint32_t const channel)
{
	return false;
}

// EOF
