/*!
 * @file   alarms.c
 * @brief  Functions for submitting alarms to the alarm system
 * @author Stephen Page
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

#include <alarms.h>
#include <AlarmSource.h>
#include <AlarmUtils.h>
#include <devname.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <logging.h>

using namespace laserlw;

// Global variables

struct Alarms alarms;

// Static functions

static FGCD_thread_func alarmsRun;
static void alarmsReportAlarms(void);



int32_t alarmsStart(void)
{
    // Initialise alarm interface

    try
    {
        alarms.alarm_source = new AlarmSource(fgcd.device[0].name);

        // Disable LASER logs

        log::setLoglevel(log::LOG_FATAL);
    }
    catch(std::exception& e)
    {
        fprintf(stderr, "ERROR: AlarmSource instantiation failed: %s\n", e.what());
        return 1;
    }
    catch(...)
    {
        fprintf(stderr, "ERROR: AlarmSource instantiation failed: unknown exception\n");
        return 1;
    }


    // Start alarm source

    try
    {
        alarms.alarm_source->start();
    }
    catch(std::exception& e)
    {
        fprintf(stderr, "ERROR: AlarmSource start failed: %s\n", e.what());
        return 1;
    }
    catch(...)
    {
        fprintf(stderr, "ERROR: AlarmSource start failed: unknown exception\n");
        return 1;
    }


    // Initialise semaphore

    if(sem_init(&alarms.sem, 0, 0) < 0)
    {
        perror("sem_init alarms sem");
        return 1;
    }

    // Initialise mutex to protect copy of status data

    if(fgcd_mutex_init(&alarms.data_mutex, ALARMS_MUTEX_POLICY) != 0) return 1;

    alarms.cycles                   = 0;
    alarms.tick                     = 0;

    // Start alarm thread

    return fgcd_thread_create(&alarms.thread, alarmsRun, NULL, ALARMS_THREAD_POLICY, ALARMS_THREAD_PRIORITY ,NO_AFFINITY);
}



// Static functions



/*
 * Thread to submit alarms
 */

static void *alarmsRun(void *unused)
{
    while(1)
    {
        sem_wait(&alarms.sem);

        if(!(alarms.tick = (alarms.tick + 1) % ALARMS_PERIOD))
        {
            pthread_mutex_lock(&alarms.data_mutex);

            // Handle alarms

            alarmsReportAlarms();

            // Increment cycle count

            alarms.cycles++;

            pthread_mutex_unlock(&alarms.data_mutex);
        }
    }

    return 0;
}



static void alarmsReportAlarms(void)
{
    int                 e;
    char                fault_family[256];
    char                fault_member[256];

    // Iterate through devices in status data

    for(int device_index = 0 ; device_index < FGCD_MAX_DEVS ; device_index++)
    {
        // Ignore non-existant devices

        if(fgcd.device[device_index].fgc_class == FGC_CLASS_UNKNOWN)
        {
            continue;
        }

        if(!fgcd.device[device_index].name)
        {
            continue;
        }

        // Extract FAULT_FAMILY

        snprintf(fault_family, sizeof(fault_family),"FGC_%u", fgcd.device[device_index].fgc_class);

        // Extract FAULT_MEMBER (first try alias and then fully qualified name)

        if(devnameSubName(fault_member, sizeof(fault_member), device_index, 0))
        {
            strncpy(fault_member, fgcd.device[device_index].name, sizeof(fault_member));

            fault_member[sizeof(fault_member) - 1] = '\0';
        }
        
        // Check for alarms warnings and aalarms from publication data

        uint32_t device_alarms = ntohl(alarms.published_data.status[device_index].class_data.alarms);

        for(uint32_t i = 0, mask = 1 ; i < static_cast<int>(sizeof(device_alarms) * 8) ; i++, mask <<= 1)
        {
            
            AlarmFaultState alarm(fault_family, fault_member, i);

            // Check whether bit is set in device_alarms bit mask

            if(device_alarms & mask)
            {
                try
                {
                    e = alarms.alarm_source->activate(alarm);
                    if( (e != QUEUE_ACCEPTED) && (e != QUEUE_REDUNDANT_ELEM))
                    {
                        logPrintf(device_index,"ALARMS Activate failed (error = %d)\n", e);
                    }
                }
                catch(std::exception& e)
                {
                    logPrintf(device_index,"ALARMS Activate failed: %s\n", e.what());
                }
                catch(...)
                {
                    logPrintf(device_index,"ALARMS Activate failed: unknown exception\n");                    
                }
            }
            else
            {
                try
                {
                    e = alarms.alarm_source->terminate(alarm);
                    if( (e != QUEUE_ACCEPTED) && (e != QUEUE_REDUNDANT_ELEM))
                    {
                        logPrintf(device_index,"ALARMS Terminate failed (error = %d)\n", e);
                    }
                }
                catch(std::exception& e)
                {
                    logPrintf(device_index,"ALARMS Terminate failed: %s\n", e.what());
                }
                catch(...)
                {
                    logPrintf(device_index,"ALARMS Terminate failed: unknown exception\n");                    
                }
            }
        }

    }
}

// EOF
