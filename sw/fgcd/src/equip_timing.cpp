/*!
 * @file   equip_timing.cpp
 * @brief  Timing-related functions common to classes with equipment devices
 * @author Stephen Page
 */

#include <sys/time.h>
#include <timdt-lib-cpp/Timing.h>

#include <logging.h>
#include <timing.h>
#include <transaction.h>

void timingTransactionCommit(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    uint16_t                transaction_id;
    static Timing::Value    value;

    // Extract transaction ID timing event payload

    try
    {
        evt_value->getFieldValue("PAYLOAD", &value);
        transaction_id = static_cast<uint16_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field PAYLOAD = %u\n",
                      evt_value->getName().c_str(), transaction_id);
        }
    }
    catch(std::exception& e)
    {
        logPrintf(0, "Event %s failed to extract transaction_id payload\n", evt_value->getName().c_str());
        return;
    }

    // Get the event time

    struct timeval time;
    time.tv_sec  = evt_time.time.tv_sec;
    time.tv_usec = evt_time.time.tv_nsec / 1000;

    // Add the event delay to obtain the time at which the transaction should apply

    time.tv_sec  +=  info.delay_ms / 1000;
    time.tv_usec += (info.delay_ms % 1000) * 1000;

    if(time.tv_usec > 1000000)
    {
        time.tv_usec -= 1000000;
        time.tv_sec++;
    }

    // Put the transaction commit event in the queue for the transaction commit thread to process

    transactionCommitEvent(transaction_id, &time);
}



void timingTransactionRollback(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    uint16_t                transaction_id;
    static Timing::Value    value;

    // Extract transaction ID timing event payload

    try
    {
        evt_value->getFieldValue("PAYLOAD", &value);
        transaction_id = static_cast<uint16_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field PAYLOAD = %u\n",
                      evt_value->getName().c_str(), transaction_id);
        }
    }
    catch(std::exception& e)
    {
        logPrintf(0, "Event %s failed to extract transaction_id payload\n", evt_value->getName().c_str());
        return;
    }

    // Rollback the transaction for each equipment device

    for(uint32_t channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        transactionRollback(transaction_id, channel);
    }
}

// EOF
