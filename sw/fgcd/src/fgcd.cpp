/*!
 * @file   fgcd.cpp
 * @brief  The Function Generator Controller Daemon
 * @author Stephen Page
 */

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctype.h>
#include <sys/io.h>
#include <sys/mman.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>

#define DEF_VERSION
#define FGC_GLOBALS

#include <charq.h>
#include <consts.h>
#include <defconst.h>
#include <definfo.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <taskmgr.h>
#include <timing.h>
#include <version.h>
#include <fgcd_thread.h>

// Constants

const char *fgcd_machine_name[FGCD_NUM_MACHINES] = {
    "NONE",
    "ADE",
    "CPS",
    "LEI",
    "LHC",
    "LN4",
    "LNA",
    "PSB",
    "SPS",
};



// Global variables

struct Fgcd fgcd;



// Static function declarations

static int32_t fgcdInit(void);
static void fgcdSignalHandler(int signal);
static void fgcdPmWarning(void);
static void printAvlMem(void);

int main(int argc, char *argv[])
{
    int                 c;
    uint32_t            i;
    int32_t             pthread_error;
    pthread_t           thread_id;
    struct sched_param  thread_params;
    int                 thread_policy;
    time_t              t = time(NULL);
    struct tm           tm = *localtime(&t);

    fprintf(stderr, "%d-%02d-%02d %02d:%02d:%02d FGCD starting class %s version %lu compiled %s\n"
                  , tm.tm_year + 1900
                  , tm.tm_mon + 1
                  , tm.tm_mday
                  , tm.tm_hour
                  , tm.tm_min
                  , tm.tm_sec
                  , FGC_CLASS_ID_STRING
                  , version.time.tv_sec
                  , ctime(&version.time.tv_sec));

    // Initialise

    if(fgcdInit()) exit(1);

    // Process command-line options

    while((c = getopt(argc, argv, "?fhl:m:g:prstuv")) != -1)
    {
        switch(c)
        {
            case 'f':
                fgcd.enable_fgc_logger = 0;
                break;

            case '?':
            case 'h':
                printf("Usage: %s [-f] [-h] [-l<FILENAME>] [-m<MACHINE>] [-g<GROUP>] [-p] [-r] [-s] [-t] [-u] [-v]\n\n%s\n", argv[0],
                       "            -f              Disable FGC Logger\n"
                       "            -h              Help\n"
                       "            -l<FILENAME>    Set output file for log\n"
                       "            -m<MACHINE>     Set machine\n"
                       "            -g<GROUP>       Set group for codes\n"
                       "            -p              Disable post-mortem\n"
                       "            -r              Disable RBAC\n"
                       "            -s              Use system time as time source\n"
                       "            -t              Use timing library as time source\n"
                       "            -u              Do not force RBAC (allow CMW access without a token)\n"
                       "            -v              Display version information\n"
                      );
                exit(1);

            case 'l':

                // Check whether filename for log is too long

                if(strlen(optarg) > LOG_MAX_FILENAME_LENGTH)
                {
                    fprintf(stderr, "ERROR: Log filename too long\n");
                    exit(1);
                }
                strcpy(logging.filename, optarg);
                break;

            case 'm':

                // Search for matching machine name

                for(i = 0 ; i < FGCD_NUM_MACHINES ; i++)
                {
                    if(!strcasecmp(optarg, fgcd_machine_name[i]))
                    {
                        fgcd.machine = (enum FGCD_machine)i;
                        break;
                    }
                }

                if(i == MACHINE_NONE)
                {
                    timing.source = TIMING_SYS;
                }

                // Check whether no machine name was matched

                if(i == FGCD_NUM_MACHINES)
                {
                    fprintf(stderr, "ERROR: Unknown machine %s specified\n\n", optarg);
                    exit(1);
                }
                break;

            case 'g':

                // Check whether group name is too long

                if(strlen(optarg) > GROUP_NAME_MAX)
                {
                    fprintf(stderr, "ERROR: Group name too long (max %u characters)\n", GROUP_NAME_MAX);
                    exit(1);
                }
                strcpy(fgcd.group, optarg);
                break;

            case 'p':
                fgcd.enable_pm = 0;
                break;

            case 'r':
                fgcd.enable_rbac = 0;
                break;

            case 's':
                // Set time source to system time

                fgcd.machine  = MACHINE_NONE;
                timing.source = TIMING_SYS;
                break;

            case 't':
                // Set time source to timing library

                timing.source = TIMING_TIMLIB;
                break;

            case 'u':
                fgcd.force_rbac = 0;
                break;

            case 'v':
                fprintf(stderr, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
                                "Compile time: ", ctime(&version.time.tv_sec),
                                "Compile host: ", version.hostname,             "\n",
                                "Compile arch: ", version.architecture,         "\n",
                                "Compile CC:   ", version.cc_version,           "\n",
                                "Compile C++:  ", version.cxx_version,          "\n",
                                "Compile user: ", version.user_group,           "\n",
                                "Compile dir:  ", version.directory,            "\n"
                       );

                exit(0);
        }
    }

    // Set a PM warning if post-mortem is disabled in the LHC and gateway is for power converters

    fgcdPmWarning();

    // Initialize CMW log

    logInitCmwLog();

    // Lock process memory into RAM

#ifdef _POSIX_MEMLOCK
    if(mlockall(MCL_CURRENT))
    {
        perror("mlockall");
        exit(1);
    }
#else
#warning Memory locking not supported
#endif

    // Set I/O privilege level

#ifdef __linux__
    if(iopl(3))
    {
        perror("iopl");
        exit(1);
    }
#endif

    // Set scheduling priority

    thread_id = pthread_self();
    if((pthread_error = pthread_getschedparam(thread_id, &thread_policy, &thread_params)) != 0)
    {
        fprintf(stderr, "ERROR: pthread_getschedparam failed with error %d\n", pthread_error);
        exit(1);
    }

    thread_params.sched_priority = FGCD_THREAD_PRIORITY;
    if((pthread_error = pthread_setschedparam(thread_id, FGCD_THREAD_POLICY, &thread_params)) != 0)
    {
        fprintf(stderr, "ERROR: pthread_setschedparam failed with error %d\n", pthread_error);
        exit(1);
    }

    // Catch signals to clean-up

    signal(SIGHUP,  fgcdSignalHandler);
    signal(SIGINT,  fgcdSignalHandler);
    signal(SIGQUIT, fgcdSignalHandler);
    signal(SIGTERM, fgcdSignalHandler);

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Start tasks

    if(taskmgrStart())
    {
        exit(1);
    }
    fprintf(stderr, "\nINITIALISATION COMPLETE\n\n");

    // Report available memory to stdout

    printAvlMem();

    // Wait for a signal

    pause();

    // disable core dumps

    struct rlimit      core_limit;

    core_limit.rlim_cur = 0;
    core_limit.rlim_max = 0;
    setrlimit(RLIMIT_CORE,&core_limit);

    // Restart if requested

    if(fgcd.restart)
    {
        // Set all file descriptors except stdin, stdout and stderr to close on exec

        for(i = sysconf(_SC_OPEN_MAX) - 1 ; i >= 3 ; i--)
        {
            fcntl(i, F_SETFD, FD_CLOEXEC);
        }

        // Exec fgcd with original arguments

        execvp(argv[0], argv);
        perror("execvp");
        exit(1);
    }
    exit(0);
}



int32_t Terminate(struct Cmd *command, struct prop *property)
{
    struct hash_entry *sym_tab = command->parser->class_data->sym_tab_prop;
    char              *symbol  = sym_tab[property->sym_idx].key.c;
    struct rlimit      core_limit;

    if(!strcmp(symbol, "CRASH"))
    {
        // Generate a segmentation fault

        kill(fgcd.pid, SIGSEGV);
    }
    else if(!strcmp(symbol, "PWRCYC"))
    {
        // disable core dumps

        core_limit.rlim_cur = 0;
        core_limit.rlim_max = 0;
        setrlimit(RLIMIT_CORE,&core_limit);

        // Reboot system

        execl("/sbin/reboot", "/sbin/reboot", NULL);
        command->error = FGC_IO_ERROR;
    }
    else if(!strcmp(symbol, "RESET"))
    {
        // disable core dumps

        core_limit.rlim_cur = 0;
        core_limit.rlim_max = 0;
        setrlimit(RLIMIT_CORE,&core_limit);

        // Trigger a restart

        fgcd.restart = 1;
        kill(fgcd.pid, SIGTERM);
    }
    else // Function has been called for a property it does not handle
    {
        command->error = FGC_NOT_IMPL;
    }
    return command->error > 0;
}



// Static function definitions

/*
 * Initialise variables
 */

static int32_t fgcdInit(void)
{
    char               *c;
    struct FGCD_device *device;
    uint32_t            i;

    // Set default timing source

    timing.source = TIMING_DEFAULT_SOURCE;

    // Require RBAC tokens for commands by default

    fgcd.force_rbac = 1;

    // Set PID

    fgcd.pid = getpid();

    // Set machine

    fgcd.machine = MACHINE_DEFAULT;

    // Set hostname

    if(gethostname(fgcd.hostname, HOST_NAME_MAX + 1))
    {
        perror("gethostname");
        return 1;
    }
    fgcd.hostname_property = fgcd.hostname;

    // Remove domain name from hostname

    if((c = strchr(fgcd.hostname, '.')) != NULL)
    {
        *c  = '\0';
    }

    // Set server name

    strcpy(fgcd.server_name, FGCD_SERVER_PREFIX);
    c = &fgcd.server_name[sizeof(FGCD_SERVER_PREFIX) - 1];
    strcpy(c, fgcd.hostname);
    fgcd.server_name_property = fgcd.server_name;

    // Convert server name to upper case

    for( ; *c ; c++) *c = toupper(*c);

    // Store start time

    gettimeofday(&fgcd.time_start, NULL);

    // Enable FGC Logger

    fgcd.enable_fgc_logger = 1;

    // Enable post-mortem

    fgcd.enable_pm = 1;

    // Enable RBAC

    fgcd.enable_rbac = 1;

    // Initialise devices

    for(i = 0 ; i < FGCD_MAX_DEVS ; i++)
    {
        device = &fgcd.device[i];

        device->id                  = i;
        device->name                = NULL;
        device->operational_name    = NULL;
        device->fgc_class           = FGC_CLASS_UNKNOWN;
        device->online              = 0;
        device->ready               = 0;
        device->rterm_ready         = 0;

        // Initialise parameter mutex

        if(fgcd_mutex_init(&device->parameter_mutex, FGCD_MUTEX_POLICY) != 0) return 1;

        // Initialise remote terminal transmit queue

        if(charqInit(&device->rterm_tx_q, RTERM_Q_LEN))
        {
            fprintf(stderr, "ERROR: Unable to initialise remote terminal queue for device %u\n", i);
            return 1;
        }
    }

    return 0;
}



/*
 * Signal handler - Clean-up and exit
 */

static void fgcdSignalHandler(int signal)
{
    // Do nothing, but unblock pause() in main

    logPrintf(0, "FGCD Received signal %d\n", signal);
}

/*
 * Set Post Mortem warning if:
 *   - the FEC is in the LHC
 *   - Post Mortem collection is disable by the -p option
 *   - the FEC is controlling power converters as identified by the last part of name starting with "r"
 */

static void fgcdPmWarning(void)
{
    if(fgcd.enable_pm == 0 && fgcd.machine == MACHINE_LHC)
    {
        uint16_t wrn = 0;

        char hostname[64];
        int i = sizeof(hostname);
        int status = gethostname(hostname, i);

        if(status != 0)
        {
            fprintf(stderr,"WARNING: fgcdPmWarning: gethostname() failed: %s\n",strerror(errno));
        }

        while(i > 0)
        {
            if(hostname[--i] == '-')
            {
                if(hostname[i+1] == 'r')
                {
                    wrn = FGC_WRN_PM;
                    break;
                }
            }
        }

        fgcddev.status.st_warnings |= wrn;
    }
}

/*
 * Get available memory from /proc/meminfo and report to stdio
 */

static void printAvlMem(void)
{
    // Try to open /proc/meminfo

    FILE *fp = fopen("/proc/meminfo", "r");

    if (fp == NULL)
    {
        fprintf(stderr,"WARNING:Failed to fopen /proc/meminfo:%s\n",strerror(errno));
        puts("Available RAM (KB):unknown");
        return;
    }

    // Read /proc/meminfo line by line and look for MemAvailable:

    int    mem_avl_kb = 0;
    size_t size;
    char   *s = NULL;

    while(getline(&s, &size, fp) != EOF && mem_avl_kb == 0)
    {
        if(sscanf(s,"MemAvailable:%u",&mem_avl_kb) == 1)
        {
            printf("Available RAM (KB):%u\n", mem_avl_kb);
            break;
        }
    }

    free(s);
    fclose(fp);
    fflush(stdout);
}

// EOF
