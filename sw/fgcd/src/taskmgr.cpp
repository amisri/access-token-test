/*!
 * @file   taskmgr.cpp
 * @brief  Task management
 * @author Stephen Page
 */

#include <cstdio>

#include <consts.h>
#include <taskmgr.h>

// Struct containing global variables

struct Taskmgr
{
    int32_t task_idx;   // Index within tasks array
};



// Global variables

struct Taskmgr taskmgr;



int32_t taskmgrStart(void)
{
    struct Task *task;

    // Do nothing if tasks have already been started

    if(taskmgr.task_idx > 0) return 1;

    // Start tasks

    for(taskmgr.task_idx = 0 ;
        tasks[taskmgr.task_idx].start_func;
        taskmgr.task_idx++)
    {
        task = &tasks[taskmgr.task_idx];

        // Check whether task should start

        if((!task->flag || *task->flag))
        {
            // Check whether task has a start function

            fprintf(stderr, "Initializing %s...\n", task->label);

            if(task->start_func())
            {
                task->started = 0;

                // Check whether task is fatal

                if(task->fatal)
                {
                    fprintf(stderr, "Initialization %s %s\n",task->label,TASK_FAIL_FATAL_STRING);
                    return 1;
                }
                else // Task is not fatal
                {
                    fprintf(stderr, "Initialization %s %s\n",task->label,TASK_FAIL_NON_FATAL_STRING);
                }
            }
            else // Task was started successfully
            {
                task->started = 1;
                fprintf(stderr, "Initialization %s %s\n",task->label,TASK_OKAY_STRING);
            }
        }
    }
    return 0;
}


// EOF
