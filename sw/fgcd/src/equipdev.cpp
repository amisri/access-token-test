/*
 *  Filename: equipdev.cpp
 *
 *  Purpose:  Functions for equipment devices
 *
 *  Author:   Stephen Page
 */

#include <stdint.h>

#include <deftypes.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <parser.h>



// Global variables

struct Equipdev equipdev;



bool equipdevConfigProperty(struct Parser *parser, struct prop *property)
{
    return false;
}



void equipdevInitDevices(void)
{
}



void equipdevNonvolWarn(uint32_t channel, int32_t set)
{
}



void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property)
{
}



void equipdevTransactionNotify(uint32_t channel, uint32_t user)
{
}



fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time)
{
    return FGC_NOT_IMPL;
}



void equipdevTransactionRollback(uint32_t channel, uint32_t user)
{
    return;
}



fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user)
{
    return FGC_NOT_IMPL;
}

// EOF
