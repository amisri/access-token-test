/*!
 * @file   fieldbus.c
 * @brief  Functions for managing the fieldbus
 * @author Stephen Page
 */

#include <algorithm>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>

#include <charq.h>
#include <consts.h>
#include <fgcd.h>
#include <fgc_code.h>
#include <fgc_event.h>
#include <fgc_runlog.h>
#include <fgcddev.h>
#include <fieldbus.h>
#include <logging.h>
#include <timing.h>
#include <fgcd_thread.h>



// Global variables

struct Fieldbus fieldbus;



int32_t EventQueue::addEvent(const fgc_event &event)
{
    // Check if the queue is not full
    if (queue.size() >= FIELDBUS_EVENT_QUEUE_MAX_ITEMS)
    {
        logPrintf(0, "FIELDBUS Event queue full. Ignoring event: type=0x%02X trigger_type=0x%02X payload=%u delay_us=%u\n",
                  event.type, event.trigger_type, event.payload, event.delay_us);
        return 1;
    }

    // Find where to insert an event (so that the queue stays sorted by delay_us)
    auto pos = std::upper_bound(queue.begin(), queue.end(), event, [](const fgc_event& lhs, const fgc_event& rhs)
    {
        return lhs.delay_us < rhs.delay_us;
    });

    // Add event to the queue
    queue.emplace(pos, event);

    // Log event being queued
    if (timing.log_events_flag)
    {
        logPrintf(0, "FIELDBUS event added to queue type=0x%02X trigger_type=0x%02X payload=%u delay_us=%u\n",
                  event.type, event.trigger_type, event.payload, event.delay_us);
    }

    return 0;
}



void EventQueue::decreaseDelay(size_t milliseconds)
{
    for (auto& event : queue)
    {
        if (event.delay_us > milliseconds)
        {
            event.delay_us -= milliseconds;
        }
        else
        {
            event.delay_us = 0;
            logPrintf(0, "FIELDBUS Event delay became 0. Event: type=0x%02X trigger_type=0x%02X payload=%u delay_us=%u\n",
                      event.type, event.trigger_type, event.payload, event.delay_us);
        }
    }
}



bool EventQueue::hasEvents() const
{
    return !queue.empty();
}



fgc_event EventQueue::popEvent()
{
    if (!queue.empty())
    {
        // Get the first element (the one with the lowest delay)
        auto event = *queue.begin();

        // Remove it from the queue
        queue.pop_front();

        return event;
    }
    else
    {
        // This should not happen, as the code that does popEvent checks hasEvents first. But just in case.
        logPrintf(0, "FIELDBUS FATAL ERROR: Tried to pop event on empty queue");

        // Although something has to be returned
        return fgc_event{FGC_EVT_NONE, 0, 0, 0};
    }
}



int32_t fieldbusInit(void)
{
    // Set default PC_PERMIT

    fieldbus.pc_permit      = FGC_CTRL_ENABLED;

    // Set default sector access

    fieldbus.sector_access  = 0;
    
    // Reset runlog index

    fieldbus.fgc_runlog_idx = 0xFFFF;

    // Initialise event mutex

    return fgcd_mutex_init(&fieldbus.event_mutex, FIELDBUS_MUTEX_POLICY);
}



void fieldbusPrepareCode(struct fgc_fieldbus_time *time, uint8_t (*code_msg)[FGC_CODE_BLK_SIZE], uint32_t num_blocks)
{
    static uint32_t fgc_code_var_idx;   // Index within code that is currently being transmitted
    uint32_t        i;
    float           max_code_votes;

    // Set name DB checksum

    time->namedb_crc = htons(fgcddev.fgc_code_chksum[0]);

    // Check whether code transmission is disabled

    if(!fgcddev.send_fgc_code_flag)
    {
        time->adv_code_class_id         = 0;
        time->adv_code_id               = FGC_CODE_NULL;
        time->adv_code_len_blk          = 0;
        time->adv_code_crc              = 0;
        time->adv_code_version          = 0;
        time->old_adv_code_version      = 0;
        time->code_var_id               = FGC_CODE_NULL;
        fgcddev.fgc_code_tx_valid_flag  = 0;

        return;
    }

    // Check whether current code buffer is not valid or all of code has been sent

    if(!fgcddev.fgc_code_tx_valid_flag                                          ||
        fgc_code_var_idx >= fgcddev.fgc_code_length[fgcddev.fgc_code_tx_num]    ||
       !fgc_code_var_idx)
    {
        // Check whether code index is forced

        if(fgcddev.fgc_code_tx_force < FGC_CODE_NUM_SLOTS)
        {
            fgcddev.fgc_code_tx_num = fgcddev.fgc_code_tx_force;
        }
        else
        {
            // Find code with highest vote to send next

            fgcddev.fgc_code_tx_num = 0;
            max_code_votes          = 0;
            for(i = 0 ; i < FGC_CODE_NUM_SLOTS ; i++)
            { 
                if(fieldbus.fgc_code_votes[i] > max_code_votes)
                {
                    fgcddev.fgc_code_tx_num = i;
                    max_code_votes          = fieldbus.fgc_code_votes[i];
                }
            }
        }

        // Set code if valid

        if(fgcddev.fgc_code_id[fgcddev.fgc_code_tx_num] != FGC_CODE_NULL)
        {
            fgcddev.fgc_code_tx_valid_flag  = 1;
            fgc_code_var_idx                = 0;
            time->code_var_class_id         = fgcddev.fgc_code_class[fgcddev.fgc_code_tx_num];
            time->code_var_id               = fgcddev.fgc_code_id[fgcddev.fgc_code_tx_num];
        }
        else // No valid code
        {
            fgcddev.fgc_code_tx_valid_flag = 0; 
        }
    }

    // Send code if valid

    if(fgcddev.fgc_code_tx_valid_flag) // Code is valid
    {
        memcpy(code_msg,
               &fgcddev.fgc_code[fgcddev.fgc_code_tx_num][fgc_code_var_idx],
               FGC_CODE_BLK_SIZE * num_blocks);
        time->code_var_idx  = htons(fgc_code_var_idx / FGC_CODE_BLK_SIZE);
        fgc_code_var_idx   += (FGC_CODE_BLK_SIZE * num_blocks);
    }
    else // No valid code
    {
        time->code_var_id = FGC_CODE_NULL;
    }

    // Clear code slot votes

    memset(fieldbus.fgc_code_votes, 0, sizeof(fieldbus.fgc_code_votes));
}



void fieldbusPrepareTime(struct fgc_fieldbus_time *time)
{
    int32_t         c;
    static uint32_t fgc_code_num_adv;               // Number of code that is currently being advertised
    uint32_t        i;
    static uint32_t prev_rterm_channel_index = 1;   // Previous channel that was sent remote terminal data

    // Increment cycle count

    fieldbus.stats.cycle_count++;

    // Indicate whether the FGC Logger is enabled

    if(fgcd.enable_fgc_logger)
    {
        time->flags |= FGC_FLAGS_LOGGER_ENABLED;
    }
    else // FGC Logger is disabled
    {
        time->flags &= ~FGC_FLAGS_LOGGER_ENABLED;
    }

    // Indicate whether post-mortem is enabled

    if(fgcd.enable_pm)
    {
        time->flags |= FGC_FLAGS_PM_ENABLED;
    }
    else // Post-mortem is disabled
    {
        time->flags &= ~FGC_FLAGS_PM_ENABLED;
    }

    // Check whether the machine is in full economy mode

    if(timing.field_value[TIMING_FIELD_MACHINE_MODE] == TIMING_MACHINE_MODE_FULLECO)
    {
        time->flags |= FGC_FLAGS_ECONOMY_FULL;
    }
    else // The machine is not in full economy mode
    {
        time->flags &= ~FGC_FLAGS_ECONOMY_FULL;
    }

    // Set PC permit

    if((fieldbus.pc_permit && !fieldbus.pc_permit_override) ||
       fieldbus.pc_permit_override == 1) // PC permit is enabled
    {
        time->flags                 |=  FGC_FLAGS_PC_PERMIT;
        fgcddev.status.st_unlatched |=  FGC_UNL_PC_PERMIT_SET;
    }
    else // PC permit is disabled
    {
        time->flags                 &= ~FGC_FLAGS_PC_PERMIT;
        fgcddev.status.st_unlatched &= ~FGC_UNL_PC_PERMIT_SET;
    }

    // Set sector access

    if((fieldbus.sector_access && !fieldbus.sector_access_override) ||
        fieldbus.sector_access_override == 1) // Sector is in access
    {
        time->flags                 |=  FGC_FLAGS_SECTOR_ACCESS;
        fgcddev.status.st_unlatched |=  FGC_UNL_SECTOR_ACCESS;
    }
    else // Sector is not in access
    {
        time->flags                 &= ~FGC_FLAGS_SECTOR_ACCESS;
        fgcddev.status.st_unlatched &= ~FGC_UNL_SECTOR_ACCESS;
    }

    // Initialise fgc_id to 0 (send to no channel)

    time->fgc_id = 0;

    // Find next channel for remote terminal character

    i = prev_rterm_channel_index;
    c = -1;
    do
    {
        i = (i % FGCD_MAX_EQP_DEVS) + 1;

        // Check whether a character can be read from transmit queue

        if((c = charqPop(&fgcd.device[i].rterm_tx_q)) >= 0)
        {
            prev_rterm_channel_index    = i;
            time->fgc_id                = i;
            time->fgc_char              = c;
        }
    } while(c < 0 && i != prev_rterm_channel_index);

    // Advertise next code

    fgc_code_num_adv   = (fgc_code_num_adv + 1) % FGC_CODE_NUM_SLOTS;
    time->adv_list_idx = fgc_code_num_adv;
    time->adv_list_len = FGC_CODE_NUM_SLOTS;

    if(fgcddev.fgc_code_id[fgc_code_num_adv] != FGC_CODE_NULL)
    {
        time->adv_code_class_id    = fgcddev.fgc_code_class[fgc_code_num_adv];
        time->adv_code_id          = fgcddev.fgc_code_id[fgc_code_num_adv];
        time->adv_code_len_blk     = htons(fgcddev.fgc_code_length[fgc_code_num_adv] / FGC_CODE_BLK_SIZE);
        time->adv_code_crc         = htons(fgcddev.fgc_code_chksum[fgc_code_num_adv]);
        time->adv_code_version     = htonl(fgcddev.fgc_code_version[fgc_code_num_adv]);
        time->old_adv_code_version = htons(fgcddev.fgc_code_old_version[fgc_code_num_adv]);
    }
    else // No valid code to advertise
    {
        time->adv_code_class_id    = 0;
        time->adv_code_id          = FGC_CODE_NULL;
        time->adv_code_len_blk     = 0;
        time->adv_code_crc         = 0;
        time->adv_code_version     = 0;
        time->old_adv_code_version = 0;
    }

    // Set runlog index

    fieldbus.fgc_runlog_idx = (fieldbus.fgc_runlog_idx + 1) % FGC_RUNLOG_SIZE_BYTES;
    time->runlog_idx        = htons(fieldbus.fgc_runlog_idx);

    // Check whether to set FGC log synchronisation bit

    if(fieldbus.fgc_log_sync)
    {
        fieldbus.fgc_log_sync--;
        time->flags |=  FGC_FLAGS_SYNC_LOG;
    }
    else
    {
        time->flags &= ~FGC_FLAGS_SYNC_LOG;
    }
}



int32_t fieldbusSendEvent(struct fgc_event *event)
{
     if(event->delay_us == 0)
     {
        if(timing.log_events_flag)
        {
            logPrintf(0, "FIELDBUS invalid event delay type=0x%02X trigger_type=0x%02X payload=%u delay_us=%u\n",
                      event->type, event->trigger_type, event->payload, event->delay_us);
        }

        return 1;
     }

    // Lock event mutex
    pthread_mutex_lock(&fieldbus.event_mutex);

    // Add event to the queue
    auto result = fieldbus.eventQueue.addEvent(*event);

    // Unlock event mutex and return result
    pthread_mutex_unlock(&fieldbus.event_mutex);

    return result;
}



void fieldbusFillSlotsFromQueue(void)
{
    for (size_t i = 0 ; i < FIELDBUS_NUM_EVENT_SLOTS && fieldbus.eventQueue.hasEvents(); i++)
    {
        if (fieldbus.event[i].type == FGC_EVT_NONE) // Slot is free
        {
            // Get the next event from the queue
            auto event = fieldbus.eventQueue.popEvent();

            fieldbus.event[i].type          = event.type;
            fieldbus.event[i].trigger_type  = event.trigger_type;
            fieldbus.event[i].payload       = htons(event.payload);
            fieldbus.event[i].delay_us      = htonl(event.delay_us);

            // Set number of cycles for which to transmit the event
            // Event delay in fieldbus cycles (rounded-up)
            uint32_t event_delay_cycles = ((event.delay_us / 1000) + (FGCD_CYCLE_PERIOD_MS - 1)) / FGCD_CYCLE_PERIOD_MS;

            fieldbus.event_tx_cycles[i] = event_delay_cycles < FIELDBUS_EVENT_MAX_TX_CYCLES ?
                                          event_delay_cycles : FIELDBUS_EVENT_MAX_TX_CYCLES;

            // Log event transmission

            if(timing.log_events_flag)
            {
                logPrintf(0, "FIELDBUS sending event slot=%d type=0x%02X trigger_type=0x%02X payload=%u delay_us=%u\n",
                          i, event.type, event.trigger_type, event.payload, event.delay_us);
            }
        }
    }
}



void fieldbusUpdateEvents(void)
{
    struct fgc_event    *event;
    uint32_t            i;

    for(i = 0 ; i < FIELDBUS_NUM_EVENT_SLOTS ; i++)
    {
        event = &fieldbus.event[i];

        // Check whether slot is in use

        if(event->type != FGC_EVT_NONE)
        {
            // Check whether event has expired

            if(!--fieldbus.event_tx_cycles[i]) // Event has expired
            {
                // Check whether event was a global post-mortem

                if(event->type == FGC_EVT_PM)
                {
                    // Clear timestamp associated with the timing event

                    fieldbus.global_pm_time_sec  = 0;
                    fieldbus.global_pm_time_nsec = 0;
                }

                // Clear slot

                event->type = FGC_EVT_NONE;
            }
            else // Event is valid for next cycle
            {
                event->delay_us = htonl(ntohl(event->delay_us) - (FGCD_CYCLE_PERIOD_MS * 1000));
            }
        }
    }

    // Decrease the delay for all the other events that are waiting in the queue
    fieldbus.eventQueue.decreaseDelay(FGCD_CYCLE_PERIOD_MS * 1000);
}



void fieldbusEventTimeRelativeToNextCycle(tTimingTime evt_time, struct timeval *relative_time)
{
    struct timeval next_cycle;

    timingGetUserTime(0, &next_cycle);

    // Advance time to next cycle

    next_cycle.tv_usec += FGCD_CYCLE_PERIOD_MS * 1000;
    next_cycle.tv_usec -= (next_cycle.tv_usec % (FGCD_CYCLE_PERIOD_MS * 1000));

    if(next_cycle.tv_usec > 1000000)
    {
        next_cycle.tv_sec++;
        next_cycle.tv_usec -= 1000000;
    }

    // Calculate relative time

    relative_time->tv_sec    = next_cycle.tv_sec  - evt_time.time.tv_sec;
    relative_time->tv_usec   = next_cycle.tv_usec - (evt_time.time.tv_nsec / 1000);

    if(relative_time->tv_usec < 0)
    {
        relative_time->tv_sec   -= 1;
        relative_time->tv_usec  += 1000000;
    }
}

// EOF