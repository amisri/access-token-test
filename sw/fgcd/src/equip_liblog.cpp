//! @file   equip_liblog.cpp
//! @brief  Common fgcd equipment device liblog integration code

//#define CCPRINTF

#include <fgcd.h>
#include <equipdev.h>
#include <equip_liblog.h>
#include <equip_libevtlog.h>
#include <equip_logger.h>
#include <logInit_class.h>
#include <defprops_class.h>
#include <log.h>
#include <log_class.h>
#include <logging.h>
#include <timing.h>


//! Convert liblog status to FGC error number

static int32_t ccLogFGCStatus(uint32_t status)
{
    switch(status)
    {
        case LOG_READ_CONTROL_SUCCESS:            return FGC_OK_RSP;
        case LOG_READ_CONTROL_INVALID_INDEX:
        case LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES: return FGC_NO_DATA;
        case LOG_READ_CONTROL_NO_NEW_DATA:        return FGC_NO_NEW_DATA;
        case LOG_READ_CONTROL_LOG_OVERWRITTEN:    return FGC_LOG_OVERWRITTEN;
        case LOG_READ_CONTROL_BUFFER_TOO_SMALL:   return FGC_RSP_BUF_FULL;
        case LOG_READ_CONTROL_INVALID_CYC_SEL:    return FGC_BAD_CYCLE_SELECTOR;
        default:                                  return FGC_NOT_IMPL;              // LOG_READ_CONTROL_DISABLED or LOG_READ_CONTROL_NO_SIGNALS
    }
}



void ccLogInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialise liblog menus and structures

    logStructsInitDevice(&device.log.mgr, &device.log.structs, &device.log.buffers, channel);
    logReadInit(&device.log.read, &device.log.structs, &device.log.buffers);
    logMenusInit(&device.log.mgr, &device.log.menus, CMD_MAX_VAL_LENGTH);

    // Run any class specific log initialisation

    logInitClass(channel);

    ccprintf("\n  Logging initialised for channel %u\n", channel);
}



static uint32_t logMenuSignals(struct Cmd *command, enum LOG_read_action action)
{
    struct Equipdev_channel  &device = equipdev.device[command->device->id];
    struct Parser            *parser = command->parser;
    uint32_t                  menu_index;

    char *value = &command->value[command->value_length];

    for (menu_index = parser->from ; menu_index <= parser->to ; menu_index += parser->step)
    {
        // Add a comma delimiter between signal name strings for each menu

        if(menu_index > parser->from)
        {
            *(value++) = ',';
        }

        // Prepare request for the signal names for menu_index

        if(logPrepareReadMenuSigNamesRequest(&device.log.menus,
                                             menu_index,
                                             action,
                                             &device.log.read.request) == false)
        {
            // Continue if nothing to retrieve because action is LOG_READ_GET_ALL_SIG_NAMES and log menu is not MPX

            continue;
        }

        // Process the request in liblog

        logReadRequest(&device.log.read, &device.log.read_control);

        if(device.log.read_control.status == LOG_READ_CONTROL_SUCCESS)
        {
            // Transfer signal names to the command response buffer

            strncpy(value, device.log.read.header.sig_names, device.log.read_control.header_size);

            value += device.log.read_control.header_size;
        }
        else
        {
            // Report error from logReadRequest()

            return ccLogFGCStatus(device.log.read_control.status);
        }
    }

    command->value_length = value - command->value;

    return FGC_OK_RSP;
}



int32_t GetLogMenuSignals(struct Cmd *command, struct prop *property)
{
    struct Parser *parser = command->parser;

    // Check that the menu index is valid (between 0 and less than LOG_NUM_LIBLOG_MENUS)

    if (parser->to >= LOG_NUM_LIBLOG_MENUS)
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    switch (property->sym_idx)
    {
        case STP_MPX_SIG_NAMES:  command->error = logMenuSignals(command,LOG_READ_GET_ALL_SIG_NAMES); break;
        case STP_SIGNALS:        command->error = logMenuSignals(command,LOG_READ_GET_SIG_NAMES);     break;
        default:                 command->error = FGC_BAD_PARAMETER;                                     break;
    }

    return command->error == FGC_OK_RSP ? 0 : 1;
}



int32_t GetLogSpy(struct Cmd *command, struct prop *property)
{
    uint32_t                &channel = command->device->id;
    struct Equipdev_channel &device  = equipdev.device[channel];
    struct Parser           *parser  = command->parser;
    const uint32_t           bin_header_size = 1 + sizeof(uint32_t);
    const uint32_t           menu_idx        = command->parser->from;

    // Check that the menu index is valid (between 0 and less than LOG_NUM_LIBLOG_MENUS)

    if (menu_idx > LOG_NUM_LIBLOG_MENUS)
    {
        command->error = FGC_BAD_ARRAY_IDX;
        return 1;
    }

    // Unfreeze the log if SYNCHED get option is active

    if((parser->getopt_flags & GET_OPT_SYNCHED) != 0)
    {
        loggerFgcLogSync(channel, logMenuIndexToLogIndex(&device.log.menus, menu_idx));

        return 0;
    }

    // Check that BIN option is specified

    if(!(parser->getopt_flags & GET_OPT_BIN))
    {
        command->error = FGC_BAD_GET_OPT;
        return 1;
    }

    // Get the DATA values

    // There must always be 6 DATA values: time_origin_s, time_origin_ns, time_offset_ms, duration_ms, prev_first_sample_time_s, prev_first_sample_time_ns

    if (parser->getopt_num_data_args != 6)
    {
        command->error = FGC_BAD_GET_OPT;
        return 1;
    }

    // Note: time_origin_s and prev_first_sample_time_s are treated as signed 32-bit integers, so they will suffer from the 2038 Unixtime bug.
    // This is a quick hack for fgcd_v1, but should be done better in fgcd_v2, since 2038 is within the potential life span of the LHC.
    // In fgcd_v2, they should be 32-bit unsigned integers, which pushes the problem out to 2106, by which time, all 32-bit FGC3s should be retired
    // and Unixtime throughout cclibs can use 64-bit integers.

    int32_t max_dur_ms = (command->user == 0 ? 999999999 : 999999);
    int32_t data_value[6]; // time_origin_s, time_origin_ns, time_offset_ms, duration_ms, prev_first_sample_time_s, prev_first_sample_time_ns
    int32_t max_value[6] = {      INT32_MAX,      999999999,          99999,  max_dur_ms,                INT32_MAX,                 999999999 };
    int32_t min_value[6] = {              0,              0,         -99999,           0,                        0,                         0 };

    for(uint32_t i = 0 ; i < 6 ; i++)
    {
        uint32_t null_index = 0;

        parserScanInt(parser, parser->getopt_data_args[i], &null_index,
                    1,                      // length (must be greater than null_index)
                    "",                     // extra delimiters
                    1,                      // include null delimiter
                    NULL,                   // delim_match - not required so NULL
                    1,                      // allow signed
                    1,                      // Enable minimum limit check
                    1,                      // Enable maximum limit check
                    min_value[i],           // minimum limit
                    max_value[i],           // maximum limit
                    &data_value[i]);

        if(command->error != 0)
        {
            command->error = FGC_BAD_GET_OPT;
            return 1;
        }
    }

    // Transfer data values to read_timing

    struct LOG_read_timing read_timing;

    read_timing.time_origin.secs.rel            = data_value[0];
    read_timing.time_origin.ns                  = data_value[1];
    read_timing.time_offset_ms                  = data_value[2];
    read_timing.duration_ms                     = data_value[3];
    read_timing.prev_first_sample_time.secs.rel = data_value[4];
    read_timing.prev_first_sample_time.ns       = data_value[5];

    // Prepare log read request and then run the request

    logPrepareReadMenuRequest(&device.log.menus,
                              parser->from,
                              LOG_READ_GET_SPY,
                              command->user,
                              &read_timing,
                              0,                        // sub_sampling
                              &device.log.read.request);

    logReadRequest(&device.log.read, &device.log.read_control);

    command->error = ccLogFGCStatus(device.log.read_control.status);

    if(command->error != FGC_OK_RSP)
    {
        return 1;
    }

    // Copy header

    memcpy(command->value + bin_header_size, reinterpret_cast<const void*>(&device.log.read.header.spy), device.log.read_control.header_size);

    // Extract payload

    uint32_t value_offset = bin_header_size + device.log.read_control.header_size;

    logOutput(&device.log.read_control,
               device.log.read.buffers,
               (uint32_t*)&command->value[value_offset],
               (CMD_MAX_VAL_LENGTH - value_offset));

    // Success, create the FGC property BIN header

    *(command->value) = 0xFF;

    *reinterpret_cast<uint32_t*>(command->value + 1) = ntohl(device.log.read_control.total_size);

    command->value_length = bin_header_size + device.log.read_control.total_size;

    return 0;
}



void logGetPmBuf(uint8_t channel, uint32_t *pm_buffer)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Save pointer to next position after the end of the pm_buffer array

    uint32_t *end_of_pm_buffer = pm_buffer + LOG_PM_BUF_SIZE / sizeof(uint32_t);

    // Insert PM version number in network byte order at the start of the PM buffer

    *(pm_buffer++) = htonl(LOG_PM_BUF_VERSION);

    // Insert the event log in binary (true)

    pm_buffer += ccEvtlogGetLogEvt(channel, true, (char *)pm_buffer, LOG_PM_BUF_SIZE) / sizeof(uint32_t);

    // Insert liblog logs

    for(uint32_t log_menu_idx = 0; log_menu_idx < LOG_NUM_LIBLOG_MENUS; ++log_menu_idx)
    {
        if(logMenuIsInPmBuf(&device.log.menus, log_menu_idx))
        {
            // Prepare struct LOG_read_request. Setting time_origin to zero will instruct liblog
            // to set it to the log freeze request time, which is considered to be the post mortem time.

            struct LOG_read_timing read_timing;

            memset(&read_timing, 0, sizeof(read_timing));

            logPrepareReadPmBufRequest(&device.log.menus, log_menu_idx, &read_timing, &device.log.read.request);

            // Prepare read control structure and the PM buf header

            logReadRequest(&device.log.read, &device.log.read_control);

            // If successful, copy the header and signal data into the PM buffer

            if(device.log.read_control.status == LOG_READ_CONTROL_SUCCESS)
            {
                // Copy PM buf header

                memcpy(pm_buffer, &device.log.read.header, device.log.read_control.header_size);

                pm_buffer += device.log.read_control.header_size / sizeof(int32_t);

                // Write liblog signal data

                pm_buffer += logOutput(&device.log.read_control,
                                       device.log.read.buffers,
                                       pm_buffer,
                                       end_of_pm_buffer - pm_buffer);

                fprintf(stderr, "logGetPmBuf for %s: Adding %s to PM: data_size = %d, header = %d\n",
                          device.fgcd_device->name,
                          device.log.menus.names[log_menu_idx],
                          device.log.read_control.data_size,
                          device.log.read_control.header_size);
            }
            else
            {
                logPrintf(channel, "LOG Failed to write Post Mortem log %s (error %d)\n",
                            device.log.menus.names[log_menu_idx],
                            device.log.read_control.status);
            }
        }
    }
}



void ParsLogMpx(uint32_t channel)
{
    // This function is called when a LOG.MPX property is modified.
    // It needs to cache the pointers to the signals to be logged.

    logCacheSelectors(&equipdev.device[channel].log.structs);
}


// If DEBUG_SIG is defined in log_class.h then include logSetDebugSignals()

#ifdef DEBUG_SIG
void ccLogSetDebugSignals(uint32_t channel, uint32_t iter_time_us)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    device.log.debug.reg_mode       =  (cc_float)regMgrVarValue(&device.reg_mgr,REG_MODE ) / 3.0 - 8.0;                       // 0 ... 3  ->  -8.0  ... -7.0
    device.log.debug.ref_state      =  (cc_float)refMgrVarValue(&device.ref_mgr,REF_STATE) * 0.1;                             // 0 ... 14 ->   0.0  ...  1.4
    device.log.debug.cyc_sel        =  (cc_float)refMgrVarValue(&device.ref_mgr,EVENT_CYC_SEL) * 0.1;                         // 0 ... 31 ->   0.0  ...  3.1
    device.log.debug.fg_type        = -(cc_float)device.ref_mgr.fg.pars->meta.type * 0.1;                                     // 0 ... 13 ->   0.0  ... -1.3
    device.log.debug.fg_status      = -(cc_float)device.ref_mgr.fg.status          / 3.0 - 2.0;                               // 0 ... 2  ->  -2.0  ... -2.66
    device.log.debug.ramp_mgr_stat  = -(cc_float)device.ref_mgr.ramp_mgr.status    / 3.0 - 3.0;                               // 0 ... 2  ->  -3.0  ... -3.66
    device.log.debug.cyc_status     = -(cc_float)device.ref_mgr.fsm.cycling.status / 3.0 - 4.0;                               // 0 ... 2  ->  -4.0  ... -4.66
    device.log.debug.iter_time_us   =  (cc_float)iter_time_us * 1.0E-6;
    device.log.debug.iter_index_i   =  (cc_float)regMgrVarValue(&device.reg_mgr,IREG_ITER_INDEX);
}
#endif




void ccLogPMFreeze(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set flag to freeze continuous logs

    logFreezePostmortemLogs(&device.log.mgr);

#ifdef CCPRINTF
    logPrintf(channel, "ccLogPMFreeze: log_mgr->freeze_mask=0x%08X\n", device.log.mgr.freeze_mask);
#endif
}



void ccLogPMFreezeLog(uint32_t channel, uint8_t log_idx)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set flag to freeze logs

    logFreezeLog(&device.log.mgr, log_idx);

#ifdef CCPRINTF
    logPrintf(channel, "ccLogPMFreezeLog: log_mgr->freeze_mask=0x%08X\n", device.log.mgr.freeze_mask);
#endif
}



void ccLogPMUnfreeze(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    logClearFreezablePostmortemLogs(&device.log.buffers);

    logUnfreezePostmortemLogs(&device.log.mgr);

#ifdef CCPRINTF
    logPrintf(channel, "ccLogPMUnfreeze: log_mgr->freeze_mask=0x%08X\n", device.log.mgr.freeze_mask);
#endif
}



void ccLogPMUnfreezeLog(uint32_t channel, uint8_t log_idx)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set flag to freeze logs

    logUnfreezeLog(&device.log.mgr, log_idx);

#ifdef CCPRINTF
    logPrintf(channel, "ccLogPMUnfreezeLog: log_mgr->freeze_mask=0x%08X\n", device.log.mgr.freeze_mask);
#endif
}



bool ccLogPMIsFrozen(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Call to logPostmortemLogsFroze is essential even if CCPRINTF is not defined

    if(logPostmortemLogsFroze(&device.log.mgr))
    {
#ifdef CCPRINTF
        logPrintf(channel, "ccLogPMIsFrozen: PM logs just froze\n");
#endif
    }

    return logArePostmortemLogsFrozen(&device.log.mgr);
}



uint32_t ccLogPMGetFreezeMask(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    return logFreezeMaskGet(&device.log.mgr);
}



uint32_t ccLogPMGetPostmortemMask(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    return logPostmortemMaskGet(&device.log.mgr);
}


// EOF
