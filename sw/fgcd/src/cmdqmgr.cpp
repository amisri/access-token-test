/*!
 * @file   cmdqmgr.c
 * @brief  Functions for managing command queues
 * @author Stephen Page
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <errno.h>

#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <devname.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <logging.h>
#include <pm.h>
#include <queue.h>
#include <rbac.h>
#include <tcp.h>



// Global variables

struct Cmdqmgr cmdqmgr;



// Static functions

static void cmdqmgrFreeQueuePopCallback(struct Queue *queue, void *cmd);



int32_t cmdqmgrInit(void)
{
    uint32_t i;

    // Define the number of commands based on the free memory

    struct sysinfo info;

    if(sysinfo(&info) != 0)
    {
        // Failed to read system info

        fprintf(stderr, "ERROR: Failed read 1 of system information using sysinf():%s\n",strerror(errno));
        return 1;
    }

    printf("Total RAM (KB):%lu\n", info.totalram / 1024);

    // Calculate RAM needed for the max number of command value buffers

    size_t max_value_ram = (size_t)CMD_MAX_VAL_LENGTH * CMDQMGR_MAX_CMDS;

    // Calculate the actual space available for command value buffers which preserves
    // the required available RAMP (CMDQMGR_KEEP_AVL in Percent)

    size_t value_ram = (info.totalram * (100 - CMDQMGR_KEEP_AVL)) / 100 - CMDQMGR_RESERVED_MB*1024*1024;

    // Clip the value_ram to the maximum needed

    if(value_ram > max_value_ram)
    {
        value_ram = max_value_ram;
    }

    // Calculate the number of command structures whose value buffers can fit into the value ram

    cmdqmgr.num_cmds = value_ram / CMD_MAX_VAL_LENGTH;

    if(cmdqmgr.num_cmds < CMDQMGR_MIN_CMDS)
    {
        // System needs a minumum number of commands

        fprintf(stderr, "ERROR: System needs at least %d command buffers. Only %u available.\n",CMDQMGR_MIN_CMDS,cmdqmgr.num_cmds);
        return 1;
    }

    printf("CMD_MAX_VAL_LENGTH:%u\n",CMD_MAX_VAL_LENGTH);
    printf("Commands:%u\n", cmdqmgr.num_cmds);

    // Get memory for array of command structures

    cmdqmgr.command = (struct Cmd *)calloc(sizeof(struct Cmd), cmdqmgr.num_cmds);

    // Initialise free_cmd_q

    if(queueInit(&cmdqmgr.free_cmd_q, cmdqmgr.num_cmds))
    {
        // Failed to initialise free queue

        fprintf(stderr, "ERROR: Failed to initialise free queue\n");
        return 1;
    }

    cmdqmgr.free_cmd_q.pop_callback = &cmdqmgrFreeQueuePopCallback;

    // Push commands onto free_cmd_q

    for(i = 0 ; i < cmdqmgr.num_cmds ; i++)
    {
        // Malloc memory for the value buffer

        cmdqmgr.command[i].value = (char *)malloc(CMD_MAX_VAL_LENGTH+1);

        if(cmdqmgr.command[i].value == NULL)
        {
            // Failed to get value buffer

            fprintf(stderr, "ERROR: Failed to malloc %u bytes for command structure %u\n",CMD_MAX_VAL_LENGTH+1,i);
            return 1;
        }

        // Set non-zero value in the buffer to make the memory resident

        memset(cmdqmgr.command[i].value, 1, CMD_MAX_VAL_LENGTH+1);

        // Set struct type to CMD_STRUCT

        cmdqmgr.command[i].struct_type = CMD_STRUCT;

        // Push command structure onto free_cmd_q

        queuePush(&cmdqmgr.free_cmd_q, &cmdqmgr.command[i]);
    }

    // Initialise free_rterm_q

    if(queueInit(&cmdqmgr.free_rterm_q, RTERM_NUM_RESPS))
    {
        // Failed to initialise free rterm queue

        fprintf(stderr, "ERROR: Failed to initialise remote terminal queue\n");
        queueFree(&cmdqmgr.free_cmd_q);
        return 1;
    }

    // Push remote terminal responses onto free_rterm_q

    for(i = 0 ; i < RTERM_NUM_RESPS ; i++)
    {
        // Set struct type to RTERM_STRUCT

        cmdqmgr.rterm_resp[i].struct_type = RTERM_STRUCT;

        // Push remote terminal response structure onto free_rterm_q

        queuePush(&cmdqmgr.free_rterm_q, &cmdqmgr.rterm_resp[i]);
    }

    // Report free memory, command value length and number of commands to stdout and then flush stdout

    if(sysinfo(&info) != 0)
    {
        // Failed to read system info

        fprintf(stderr, "ERROR: Failed read 2 of system information using sysinf():%s\n",strerror(errno));
        return 1;
    }

    return 0;
}



void cmdqmgrInitCmd(struct Cmd *cmd)
{
    cmd->acq_timestamp.tv_sec       = 0;
    cmd->acq_timestamp.tv_usec      = 0;
    cmd->client                     = NULL;
    cmd->cmw_update_flags           = 0;
    cmd->command_length             = 0;
    cmd->command_string[0]          = '\0';
    //memset (cmd->command_string,0,sizeof(cmd->command_string));
    cmd->cycle_timestamp.tv_sec     = 0;
    cmd->cycle_timestamp.tv_usec    = 0;
    cmd->zero_cycle_timestamp       = false;
    cmd->device                     = NULL;
    cmd->device_name[0]             = '\0';
    //memset (cmd->device_name,0, sizeof(cmd->device_name));
    cmd->error                      = 0;
    cmd->parser                     = NULL;
    cmd->rbac_token                 = NULL;
    cmd->response_queue             = &cmdqmgr.free_cmd_q;
    cmd->sub_device                 = 0;
    cmd->tag[0]                     = '\0';
    //memset (cmd->tag,0,sizeof(cmd->tag));
    cmd->tag_length                 = 0;
    cmd->transaction_id             = 0;
    cmd->user                       = 0;
    cmd->force_user                 = -1;
    cmd->value[0]                   = '\0';
    //memset (cmd->value,0,CMD_MAX_VAL_LENGTH+1);
    cmd->value_length               = 0;
    cmd->error_in_value             = false;
}



void cmdqmgrQueue(struct Cmd *command)
{
    int32_t device_number;

    // Increment commands received

    cmdqmgr.cmds_received++;

    // Initialise command

    command->error                      = 0;
    command->acq_timestamp.tv_sec       = 0;
    command->acq_timestamp.tv_usec      = 0;
    command->cycle_timestamp.tv_sec     = 0;
    command->cycle_timestamp.tv_usec    = 0;

    // Clear value for get commands

    switch(command->type)
    {
        case CMD_GET:
        case CMD_GET_SUB:
        case CMD_SUB:
            command->value[0]     = '\0';
            command->value_length = 0;
            break;

        default:
            break;
    }

    // Check whether the device name is null and therefore implicitly the FGCD device

    if(command->device_name[0] == '\0')
    {
        command->device = &fgcd.device[0];

        if(fgcd.enable_rbac && rbacAuthorise(command))
        {
            queuePush(command->response_queue, command);
            return;
        }
    }
    else
    {
        // Check whether the first character of the device name is numeric (absolute address)
        // otherwise attempt to resolve

        if(isdigit(command->device_name[0])) // Channel number
        {
            device_number       = atoi(command->device_name);
            command->sub_device = 0;
        }
        else // Device name
        {
            // Resolve device name

            if(devnameSubResolve(command->device_name, (uint32_t *)&device_number, &command->sub_device))
            {
                device_number       = devnameResolve(command->device_name);
                command->sub_device = 0;
            }
        }

        // Check whether the device number is invalid

        if(device_number < 0 || device_number > FGCD_MAX_EQP_DEVS)
        {
            // Return unknown device error

            command->error = FGC_UNKNOWN_DEV;
            queuePush(command->response_queue, command);
            return;
        }

        // Set device

        command->device = &fgcd.device[device_number];

        // Perform RBAC authorisation

        if(fgcd.enable_rbac && rbacAuthorise(command))
        {
            queuePush(command->response_queue, command);
            return;
        }

        // Check whether the device is not ready

        if(!command->device->ready)
        {
            // Return device not ready error

            command->error = FGC_DEV_NOT_READY;
            queuePush(command->response_queue, command);
            return;
        }

#if(FGC_CLASS_ID == 1)

        // Reject commands at key points during self-triggered post-mortem

        if(command->client != CMD_FGCD_CLIENT &&                            // Client is not FGCD itself
           (command->type == CMD_SET || command->type == CMD_SET_BIN) &&    // Set command and
           (command->device->pm_self.acq_info.frozen || command->device->pm_self.cmds_queued))  // Self-triggered post-mortem buffer is frozen or in progress
        {
            logPrintf(command->device->id, "Command failed in FGCD %s: %s\n", command->command_string, fgc_errmsg[FGC_PM_IN_PROGRESS]);

            // Return post-mortem in progress error

            command->error = FGC_PM_IN_PROGRESS;
            queuePush(command->response_queue, command);
            return;
        }

#endif

    }

    // Push the command onto the device queue

    if(command->device->command_queue)
    {
        command->device->cmds_received++;

        int32_t status;

        if((status = queuePush(command->device->command_queue, command)) != 0)
        {
            // Failed to queue command for device (queue may be full)

            logPrintf(command->device->id, "CMDQMGR Failed to queue command for device:queuePush status=%d  q size=%d\n"
                                         ,status
                                         ,command->device->command_queue->buffer_size);
            command->error = FGC_BUSY;
            queuePush(command->response_queue, command);
        }
    }
    else // Device has no command queue
    {
        logPrintf(command->device->id, "CMDQMGR No command queue for device\n");
        command->error = FGC_DEV_NOT_READY;
        queuePush(command->response_queue, command);
    }
}



// Static functions



/*
 * Call-back to be called on pop action on the free command queue
 */

static void cmdqmgrFreeQueuePopCallback(struct Queue *queue, void *cmd)
{
    cmdqmgrInitCmd((struct Cmd *)cmd);
}

// EOF
