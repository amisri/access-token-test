/*!
 * @file   sub_utilities.cpp
 * @brief  SUB publication decoding utilities 
 * @author Miguel Hermo Serrans, Marc Magrans de Abril
 */

 #include <sub_utilities.h>

#include <arpa/inet.h>

#include <arpa/inet.h>

#include <logging.h>
#include <string.h>

float subUtilitiesNtohf(float value)
{
    union { float f; uint32_t l; }  f_l;
    f_l.f               = value;
    f_l.l               = ntohl(f_l.l);
    return f_l.f;
}

int16_t subUtilitiesSignedNtohs(int16_t value)
{
    return (int16_t) ntohs((uint16_t) value);
}

int32_t subUtilitiesSignedNtohs(int32_t value)
{
    return (int32_t) ntohl((uint32_t) value);
}

void subUtilitiesEnumToText(uint16_t state, CONST sym_name *status_map, char *message, uint8_t max_message_len)
{
    message[0]='\0';
    int i;  
    int written_chars;

    for(i = 0; status_map[i].label; ++i)
    {
        // If the bit is active, print bit label
        if (status_map[i].type == state)
        {
            written_chars = snprintf(message, max_message_len, "%s", status_map[i].label);

            if (written_chars < 0 || written_chars>max_message_len-1)
            {
                logPrintf(0,"SUBUTILITIES Could not convert FGC state %x to text: Text longer than maximum length = %u\n", state, max_message_len);
            }

            return;
        }
    }

    logPrintf(0,"SUBUTILITIES Could not find tag for state %x: state not found\n",state);
}

void subUtilitiesBitmaskToText(uint16_t status, CONST sym_name *status_map, char *message, uint8_t max_message_len)
{
    uint8_t pos = 0;
    int written_chars;
    int i;  

    message[0] = '\0';

    for(i = 0; status_map[i].label; ++i)
    {
        // If the bit is active, print bit label

        if (status_map[i].type & status)
        {
            written_chars = snprintf(message+pos, max_message_len-pos, "%s ", status_map[i].label);

            if (written_chars < 0 || written_chars+pos>max_message_len-1)
            {
                logPrintf(0,"SUBUTILITIES Could not convert FGC status %x to text: Text longer than maximum length = %u\n", status, max_message_len);
                return;
            }
            else
            {
                pos += written_chars;
            }
        }
    }

}

void subUtilitiesPmdEnumToText(uint16_t state, CONST sym_name *status_map, char *message, uint8_t max_message_len)
{
    int written_chars;
    int i;  

    message[0]='\0';

    for(i = 0; status_map[i].label; ++i)
    {
        // If the bit is active, print bit label

        if (status_map[i].type == state)
        {
            written_chars = snprintf(message, max_message_len, "%s (%u)", status_map[i].label, state);

            if (written_chars < 0 || written_chars>max_message_len-1)
            {
                logPrintf(0,"SUBUTILITIES Could not convert FGC state %x to text: Text longer than maximum length = %u\n", state, max_message_len);
            }

            return;
        }
    }

    logPrintf(0,"SUBUTILITIES Could not find tag for state %x\n",state);
}

void subUtilitiesPmdBitmaskToText(uint16_t status, CONST sym_name *status_map, char *message, uint8_t max_message_len)
{
    uint8_t pos = 0;
    int written_chars;
    int i;
    int first_time = 1;  

    message[0]='\0';

    for(i = 0; status_map[i].label; ++i)
    {
        // If the bit is active, print bit label

        if (status_map[i].type & status)
        {
            if (first_time)
            {
                first_time = 0;
                written_chars = snprintf(message+pos, max_message_len-pos, "%s", status_map[i].label);
            }
            else
            {
                written_chars = snprintf(message+pos, max_message_len-pos, "|%s", status_map[i].label);   
            }

            if (written_chars < 0 || written_chars+pos>max_message_len-1)
            {
                logPrintf(0,"SUBUTILITIES Could not convert FGC status %x to text: ext longer than maximum length = %u\n", status, max_message_len);
                message[0] = '\0';
                return;
            }
            else
            {
                pos += written_chars;
            }
        }
    }

    // Print the number

    written_chars = snprintf(message+pos, max_message_len-pos-1, " (%u)", status);

    if (written_chars < 0 || written_chars+pos>max_message_len-1)
    {
        logPrintf(0,"SUBUTILITIES Could not convert FGC status %x to text: ext longer than maximum length = %u\n", status, max_message_len);
        message[0] = '\0';
        return;
    }
}