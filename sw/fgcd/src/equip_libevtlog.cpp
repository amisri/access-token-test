//! @file   equip_libevtlog.cpp
//! @brief  Interface to libevtlog


#include <cmd.h>
#include <timing.h>
#include <cmwpub.h>
#include <fgcd_thread.h>
#include <equipdev.h>
#include <evtlogInit_class.h>
#include <fgcd.h>


// libevtlog callbacks

void ccEvtlogChangedFunction(uint32_t device_id, char const * const name, void * const lock_data)
{
    // Notify property and its parents

    cmwpubNotify(device_id, name, 0, 0, 0, 1, 0);
}



void ccEvtlogLockFunction(uint32_t device_id, void * const lock_data)
{
    pthread_mutex_lock(&equipdev.device[device_id].evtlog_mutex);
}



void ccEvtlogUnlockFunction(uint32_t device_id, void * const lock_data)
{
    pthread_mutex_unlock(&equipdev.device[device_id].evtlog_mutex);
}



// External functions

int32_t ccEvtlogInit(uint32_t device_id)
{
    struct Equipdev_channel &device = equipdev.device[device_id];

    // Initialise one mutex per device

    if(fgcd_mutex_init(&device.evtlog_mutex, FGCLITE_MUTEX_POLICY) != 0) return 1;

    // Call auto-generated function to initialise evtlog structs

    evtlogStructsInitDevice(&device.evtlog_data.device, 0, device_id);

    return 0;
}



void ccEvtlogStoreProperties(uint32_t device_id)
{
    // Retrieve time

    struct timeval timestamp;
    timingGetUserTime(0, &timestamp);

    // Match interfaces

    struct EVTLOG_device * evtlog_dev   = &equipdev.device[device_id].evtlog_data.device;
    struct CC_us_time      cc_timestamp = { timestamp.tv_sec, timestamp.tv_usec };

    // Scan event log properties

    evtlogStoreProperties(evtlog_dev, cc_timestamp);
}



void ccEvtlogStoreRecord(uint32_t device_id, const struct timeval *timestamp, const char *property, const char *value, const char *action, const char status)
{
    struct EVTLOG_device * evtlog_dev   = &equipdev.device[device_id].evtlog_data.device;
    struct CC_us_time      cc_timestamp = { timestamp->tv_sec, timestamp->tv_usec };

    evtlogStoreRecord(evtlog_dev, cc_timestamp, property, value, action, status);
}



void ccEvtLogStoreTimingEvent(uint32_t device_id, const struct timeval *timestamp, const char *event_name, uint32_t cycle_selector)
{
    char event_value[EVTLOG_VALUE_LEN];

    snprintf(event_value, EVTLOG_VALUE_LEN, "USER(%d)", cycle_selector);

    ccEvtlogStoreRecord(device_id, timestamp, event_name, event_value, "RECV", EVTLOG_STATUS_FREQ);
}



int32_t ccEvtlogStoreSetCmd(struct Cmd *command, struct prop *property)
{

    uint32_t &device_id = command->device->id;

    // FGC2 outputs NET.xxx or TRM.xxx in the response string, to distinguish between commands sent
    // through CMW or TCP and commands send through a remote terminal. As FGClite does not implement
    // the remote terminal, and the serial communication is only used during calibration, we don't
    // care about the distinction. So we always say NET.xxx.

    char response_string[8];

    snprintf(response_string, 8, "NET.%03u", command->error);

    // Match interfaces

    struct EVTLOG_device * evtlog_dev   = &equipdev.device[device_id].evtlog_data.device;
    struct CC_us_time      cc_timestamp = { command->parser->timestamp.tv_sec, command->parser->timestamp.tv_usec };

    evtlogStoreRecord(evtlog_dev, cc_timestamp, command->command_string, command->value, response_string, EVTLOG_STATUS_NORMAL);

    return 0;
}



size_t ccEvtlogGetLogEvt(uint32_t device_id, bool binary, char *buffer, size_t max_buflen)
{
    struct EVTLOG_device * evtlog_dev = &equipdev.device[device_id].evtlog_data.device;

    uint32_t reclen;
    uint32_t buflen       = 0;
    uint16_t record_index = 0;

    if(binary)
    {
        if(max_buflen >= EVTLOG_RECORD_LEN * EVTLOG_NUM_RECORDS)
        {
            // Return data in FGC binary format (Network byte order)

            while ((reclen = evtlogReadBin(evtlog_dev, &record_index, buffer + buflen, EVTLOG_NETWORK_BYTE_ORDER)) != 0)
            {
                buflen += reclen;
            }
        }
    }
    else
    {
        while((reclen = evtlogReadFgc(evtlog_dev, &record_index, buffer + buflen)) != 0)
        {
            // Return data in FGC ASCII format

            buflen += reclen;

            buffer[buflen++] = ',';

            // Check for possible buffer overflow on next iteration

            if(buflen + EVTLOG_READ_FGC_BUF_LEN >= max_buflen) return 0;
        }

        *(buffer + buflen - 1) = '\0';
    }

    return buflen;
}



int32_t GetLogEvt(struct Cmd *command, struct prop *property)
{
    const uint32_t bin_hdr_size = 1 + sizeof(uint32_t);

    uint32_t &channel = command->device->id;

    bool is_bin = command->parser->getopt_flags & GET_OPT_BIN;

    uint32_t evtlog_buf_size = ccEvtlogGetLogEvt(channel, is_bin, command->value + bin_hdr_size, CMD_MAX_VAL_LENGTH - bin_hdr_size);

    switch(evtlog_buf_size)
    {
        // Check for errors

        case -1: command->error = FGC_RSP_BUF_FULL;  break;    // Buffer full error
        case  0: command->error = FGC_LOG_NOT_READY; break;    // No records in Event log

        default:                                               // Success

            // Add Eventlog header

            *(command->value) = 0xFF;

            *reinterpret_cast<uint32_t*>(command->value + 1) = ntohl(evtlog_buf_size);

            command->value_length = bin_hdr_size + evtlog_buf_size;
            command->error        = FGC_OK_RSP;
    }

    return command->error != FGC_OK_RSP;
}
// EOF
