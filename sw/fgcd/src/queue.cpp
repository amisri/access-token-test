/*!
 * @file   queue.c
 * @brief  Functions for queues (FIFOs)
 * @author Stephen Page
 */

#include <stdio.h>
#include <stdlib.h>

#include <queue.h>
#include <fgcd_thread.h>
#include <fgcddebug.h>

// Static functions

static int32_t queueIsEmpty(struct Queue *q);
static int32_t queueIsFull(struct Queue *q);



int32_t queueInit(struct Queue *q, size_t buffer_size)
{
    fgcddebug("buffer_size %zu\n",buffer_size);

    // Allocate memory for queue buffer

    if(buffer_size == 0)
    {
        fprintf(stderr,"ERROR: queueInit:buffer_size is zero\n");
        return 1;
    }

    buffer_size++;
    if(!(q->buffer = (void **)calloc(buffer_size, sizeof(void *))))
    {
        return 1;
    }

    // Initialise mutex and condition variables

    if(fgcd_mutex_init(&q->mutex, QUEUE_MUTEX_POLICY) != 0)
    {
        free(q->buffer);
        q->buffer = NULL;
        return 1;
    }

    pthread_cond_init(&q->not_empty, NULL);
    pthread_cond_init(&q->not_full,  NULL);

    q->buffer_size      = buffer_size;
    q->back = q->front  = 0;
    q->pop_callback     = NULL;

    return 0;
}



void queueFree(struct Queue *q)
{
    // Free memory

    if(q->buffer != NULL)
    {
        free(q->buffer);
        q->buffer = NULL;
    }

    // Destroy mutex and condition variables

    pthread_mutex_destroy(&q->mutex);
    pthread_cond_destroy(&q->not_empty);
    pthread_cond_destroy(&q->not_full);
}



int32_t queuePush(struct Queue *q, void *member)
{
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return 2;
    }

    // Check that the queue is not full

    if(!queueIsFull(q)) // The queue is not full
    {
        int32_t queue_was_empty = queueIsEmpty(q);

        // Add the member to the queue

        q->buffer[q->front] = member;
        q->front            = (q->front + 1) % q->buffer_size;

        // Send signal that queue has content

        if(queue_was_empty)
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        pthread_mutex_unlock(&q->mutex);

        return 0;
    }
    else // The queue is full
    {
        pthread_mutex_unlock(&q->mutex);
        return 1;
    }
}



int32_t queueBlockPush(struct Queue *q, void *member)
{
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return 2;
    }

    // Check that the queue is not full

    while(queueIsFull(q))
    {
        // Wait for a member to leave the queue

        pthread_cond_wait(&q->not_full, &q->mutex);
    }

    int32_t queue_was_empty = queueIsEmpty(q);

    // Add the member to the queue

    q->buffer[q->front] = member;
    q->front            = (q->front + 1) % q->buffer_size;

    // Send signal that queue has content

    if(queue_was_empty)
    {
        pthread_cond_broadcast(&q->not_empty);
    }

    pthread_mutex_unlock(&q->mutex);

    return 0;
}



void *queuePop(struct Queue *q)
{
    void *member;

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return NULL;
    }

    if(queueIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        return NULL;
    }

    int32_t queue_was_full = queueIsFull(q);

    // Remove the member from the queue

    member  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    // Send signal that queue is not full

    if(queue_was_full)
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Call pop call-back if not NULL

    if(q->pop_callback)
    {
        q->pop_callback(q, member);
    }

    pthread_mutex_unlock(&q->mutex);

    return member;
}



void *queueBlockPop(struct Queue *q)
{
    void *member;

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return NULL;
    }

    while(queueIsEmpty(q))
    {
        // Wait for a member to enter the queue

        pthread_cond_wait(&q->not_empty, &q->mutex);
    }

    int32_t queue_was_full = queueIsFull(q);

    // Remove the member from the queue

    member  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    // Send signal that queue is not full

    if(queue_was_full)
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Call pop call-back if not NULL

    if(q->pop_callback)
    {
        q->pop_callback(q, member);
    }

    pthread_mutex_unlock(&q->mutex);

    return member;
}

// Static functions



/*
 * Is the queue empty?
 */

static int32_t queueIsEmpty(struct Queue *q)
{
    return q->back == q->front;
}



/*
 * Is the queue full?
 */

static int32_t queueIsFull(struct Queue *q)
{
    return (q->front + 1) % q->buffer_size == q->back;
}

// EOF
