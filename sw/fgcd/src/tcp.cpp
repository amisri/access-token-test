/*!
 * @file   tcp.c
 * @brief  Functions for the TCP server
 * @author Stephen Page
 */

#include <cstdlib>
#include <ctype.h>
#include <errno.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <charq.h>
#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <defconst.h>
#include <devname.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <logging.h>
#include <pub.h>
#include <queue.h>
#include <rbac.h>
#include <tcp.h>
#include <timing.h>

#define MAX_TCP_RETRIES 10

// Global variables

struct Tcp tcp;



// Static functions

static FGCD_thread_func tcpListen;
static FGCD_thread_func tcpSend;
static FGCD_thread_func tcpRecv;


static int32_t tcpReplaceUserNameByNumber(Cmd *recv_cmd);
static int32_t tcpInit(void);
static void tcpClearClient(struct TCP_client *client);
static void tcpDisconnect(struct TCP_client *client);
static void tcpProcessCmdBuffer(struct TCP_client *client, ssize_t buffer_length);

/*
 * Sends the contents of client->send_buffer though client->socket
 *
 * Returns non zero if not the whole buffer_length was successfully sent
 */

static int32_t tcpSendClientBuffer(TCP_client *client, uint32_t buffer_length)
{
    uint32_t nbytes_sent = 0;
    ssize_t  nwritten    = 0;

    if(sizeof(client->send_buffer) < buffer_length)
    {
        return -1;
    }

    while(nbytes_sent < buffer_length)
    {
        if((nwritten = send(client->sock, client->send_buffer + nbytes_sent, buffer_length - nbytes_sent, 0)) <= 0)
        {
            if(nwritten < 0 && errno == EINTR)
            {
                // EINTR means no data was sent before the error. We can try sending again
                nwritten = 0;
            }
            else
            {
                // error possibly ocurred after some data was sent. We can't recover
                logPrintf(0, "TCP %d (%s@%s) error sending response (nwritten=%d,errno=%d)\n",
                    client->id,
                    client->username,
                    client->hostname,
                    nwritten,
                    errno);
                return -2;
            }
        }

        nbytes_sent += nwritten;
        tcp.load    += nwritten;
    }

    return 0;
}


int32_t tcpReplaceUserNameByNumber(Cmd *recv_cmd)
{
    char *command_string = recv_cmd->command_string;
    char rewrite_buffer[CMD_MAX_LENGTH + 1];
    char user_prefix[CMD_MAX_LENGTH + 1];

    char *c;
    uint16_t i = 0;
    uint16_t j = 0;

    uint16_t username_begin;
    uint16_t selector_begin;
    uint16_t selector_end;

    uint16_t prefix_len;
    uint8_t usrid;
    uint16_t userid_string_len;

    // Find user selector

    while( command_string[i] != '(' )
    {
        // don't do anything if no username was provided

        if( command_string[i] == '\0' || i==CMD_MAX_LENGTH)
        {
            return 0;
        }
        i++;
    }
    while(command_string[++i] == ' ')
    {
        // ignore whitespaces before cycle selector, as does the FGC parser
    }
    selector_begin = i;

    while( command_string[i] !=')' )
    {
        // make username uppercase

        rewrite_buffer[i] = toupper(command_string[i]);

        // ERROR: missing matching ')'

        if( command_string[i] == '\0' || i==CMD_MAX_LENGTH)
        {
            logPrintf(recv_cmd->device->id, "TCP Missing matching ')' in command\n");
            recv_cmd->error = FGC_SYNTAX_ERROR;
            return 1;
        }
        i++;
    }
    // whitespaces after the selector are not allowed by the FGC parser, so we don't
    // take that into conideration

    selector_end = i;
    rewrite_buffer[selector_end] = '\0';

    // don't do anything if selector is already a number (possibly an hex or octal)

    strtol(&rewrite_buffer[selector_begin],&c,0);
    if(c == &rewrite_buffer[selector_end])
    {
        return 0;
    }

    // If the 'long' cycle selector was specified (ie:<machine>.USER.<username>)
    // we only consider the <username> part since that's what's provided by the timing library

    prefix_len = snprintf(user_prefix, CMD_MAX_LENGTH, "%s.USER.",fgcd_machine_name[fgcd.machine]);

    if(selector_begin + prefix_len < CMD_MAX_LENGTH && strncmp(user_prefix, &rewrite_buffer[selector_begin],prefix_len) == 0)
    {
        username_begin = selector_begin + prefix_len;
    }
    else
    {
        username_begin = selector_begin;
    }

    // find user number

    usrid = timingUserNumber( &rewrite_buffer[username_begin] );
    if(usrid == 0)
    {
        // Error code is very explicit, no need to print any additional log message

        recv_cmd->error = FGC_BAD_USERNAME;
        return 1;
    }

    // reconstruct command

    // add user number

    userid_string_len = snprintf(&rewrite_buffer[selector_begin], 4, "%d", usrid);

    // copy the rest of the command after the user number

    i = selector_end;
    j = selector_begin+userid_string_len;

    while(command_string[i]!='\0')
    {
        rewrite_buffer[j++] = command_string[i++];

        if( (i==CMD_MAX_LENGTH+1 || j==CMD_MAX_LENGTH+1) && command_string[i]!='\0')
        {
            logPrintf(recv_cmd->device->id, "TCP User number does not fit in the command\n");
            recv_cmd->error = FGC_UNKNOWN_ERROR_CODE;
            return 1;
        }
    }
    rewrite_buffer[j]='\0';

    // overwrite command_string with rewritten command

    strncpy( &command_string[selector_begin], &rewrite_buffer[selector_begin], CMD_MAX_LENGTH-selector_begin);
    recv_cmd->command_length = j;

    return 0;
}

int32_t tcpStart(void)
{
    struct sockaddr_in  sin;
    int                 sock_opt;           // Socket options

    // Check whether server is already started

    if(tcp.listen_sock > 0) return 1;

    if(tcpInit() != 0) return 1;

    if((tcp.listen_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("TCP server socket");
        return 1;
    }

    // Set bind options

    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(TCP_PORT_NUM);
    sin.sin_family      = AF_INET;

    // Allow socket re-use

    sock_opt = 1;

    if(setsockopt(tcp.listen_sock,
                  SOL_SOCKET,
                  SO_REUSEADDR,
                  (const void *)&sock_opt, sizeof(sock_opt)) < 0)
    {
        perror("setsockopt SO_REUSEADDR");
        close(tcp.listen_sock);
        tcp.listen_sock = -1;
        return 1;
    }

    // Bind to socket

    if(bind(tcp.listen_sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("TCP bind");
        close(tcp.listen_sock);
        tcp.listen_sock = -1;
        return 1;
    }

    // Start listening thread

    if(fgcd_thread_create(&tcp.listen_thread, tcpListen, NULL, TCP_LISTEN_THREAD_POLICY, TCP_LISTEN_THREAD_PRIORITY ,NO_AFFINITY) != 0)
    {
        close(tcp.listen_sock);
        tcp.listen_sock = -1;
        return 1;
    }

    return 0;
}

// Static functions



/*
 * Initialise variables
 */

static int32_t tcpInit(void)
{
    struct TCP_client   *client;
    uint32_t            i;

    tcp.listen_sock = -1;

    for(i = 0 ; i < FGC_TCP_MAX_CLIENTS ; i++)
    {
        client = &tcp.client[i];

        // Initialise mutex

        if(fgcd_mutex_init(&client->mutex, TCP_MUTEX_POLICY) != 0) return 1;

        tcpClearClient(client);

        client->id                = i;

        if (queueInit(&client->response_queue, cmdqmgr.num_cmds + RTERM_NUM_RESPS)) return 1;
    }

    return 0;
}



/*
 * Initialise or clear a client structure
 */

static void tcpClearClient(struct TCP_client *client)
{
    pthread_mutex_lock(&client->mutex);

    client->cmd_state                   = TCP_RECV_IDLE;
    client->hostname[0]                 = '\0';
    client->mode                        = TCP_MODE_CMD;
    client->num_cmds                    = 0;
    client->num_rterm_resps             = 0;
    client->recv_cmd                    = NULL;
    client->rterm_device                = NULL;
    client->rterm_locked                = 0;
    client->run                         = 0;
    client->udp_sub_id                  = 0;
    client->udp_sub_port_num            = 0;
    client->udp_sub_period              = PUB_UDP_DEFAULT_PERIOD;
    client->udp_sub_period_tick         = 0;
    client->udp_sub_sequence            = 0;
    client->username[0]                 = '\0';

    // Clear RBAC token

    rbacClearTCPClientToken(client);

    // Clear the flag indicating that the client structure is in use

    client->in_use = 0;

    pthread_mutex_unlock(&client->mutex);
}



/*
 * Listen on TCP port and spawn send and receive threads
 */

static void *tcpListen(void *unused)
{
    struct TCP_client   *client;
    struct sockaddr_in  from;
    uint32_t            i;
    socklen_t           length;
    int                 sock;
    int                 sock_opt;

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    listen(tcp.listen_sock, FGC_TCP_MAX_CLIENTS);

    while(tcp.listen_sock >= 0)
    {
        // Accept a connection

        length = sizeof(from);
        if((sock = accept(tcp.listen_sock, (struct sockaddr *)&from, &length)) < 0)
        {
            perror("accept");
            continue;
        }

        // Find a free client structure

        for(i = 0 ; i < FGC_TCP_MAX_CLIENTS && tcp.client[i].in_use ; i++);

        // Check whether all client structures are in use

        if(i >= FGC_TCP_MAX_CLIENTS)
        {
            // Send a '-' to indicate that the server is busy

            send(sock, "-", 1, 0);
            close(sock);

            logPrintf(0, "TCP Connection rejected, no free client structures\n");
            continue;
        }

        // A free client structure was found

        client = &tcp.client[i];
        tcpClearClient(client);

        client->address = from.sin_addr;
        client->in_use  = 1;
        client->run     = 1;
        client->sock    = sock;

        // Resolve client's address to a host name

        if(getnameinfo((struct sockaddr *)&from, sizeof(from),
                       client->hostname, sizeof(client->hostname), NULL, 0, 0))
        {
            // Resolution failed - disconnect the client

            logPrintf(0, "TCP %d Connection rejected, unable to resolve hostname\n", client->id);

            // Send a '-' to indicate that the server rejected the connection

            send(sock, "-", 1, 0);

            close(client->sock);
            tcpClearClient(client);
            continue;
        }

        // Increment published data client count

        fgcddev.status.tcp_clients++;

        // Check whether maximum number of clients has been reached

        if(fgcddev.status.tcp_clients == FGC_TCP_MAX_CLIENTS)
        {
            // Set warning

            fgcddev.status.st_warnings |= FGC_WRN_TCP_MAX_USERS;
        }

        // Send a '+' to indicate that the connection may proceed

        if(send(client->sock, "+", 1, 0) != 1)
        {
            // Close the socket

            logPrintf(0, "TCP %d (%s@%s) Send failed\n",
                      client->id, client->username, client->hostname);

            tcpDisconnect(client);
            continue;
        }

        // Enable keep-alive for the socket

        sock_opt = 1;
        if(setsockopt(client->sock,
                      SOL_SOCKET,
                      SO_KEEPALIVE,
                      (const void *)&sock_opt, sizeof(sock_opt)) < 0)
        {
            logPrintf(0, "TCP %d (%s@%s) Set keep-alive failed\n",
                      client->id, client->username, client->hostname);
            perror("setsockopt SO_KEEPALIVE");

            tcpDisconnect(client);
            continue;
        }

        // Disable Nagle's algorithm for the socket

        sock_opt = 1;
        if(setsockopt(client->sock,
                      IPPROTO_TCP,
                      TCP_NODELAY,
                      (const void *)&sock_opt, sizeof(sock_opt)) < 0)
        {
            logPrintf(0, "TCP %d (%s@%s) Disabling of Nagle's algorithm failed\n",
                      client->id, client->username, client->hostname);
            perror("setsockopt TCP_NODELAY");

            tcpDisconnect(client);
            continue;
        }

        // Spawn receive thread

        if(fgcd_thread_create(&client->recv_thread, tcpRecv, client, TCP_CLIENT_THREAD_POLICY, TCP_CLIENT_THREAD_PRIORITY ,NO_AFFINITY) != 0)
        {
            logPrintf(0, "TCP %d (%s@%s) Start receive thread failed\n", client->id, client->username, client->hostname);

            tcpDisconnect(client);
            continue;
        }
    }

    logPrintf(0, "TCP tcpListen(): tcp.listen_sock = %d, exiting thread.\n", tcp.listen_sock);

    return 0;
}

static void tcpDisconnect(struct TCP_client *client)
{
    close(client->sock);
    logPrintf(0, "TCP %d (%s@%s) Disconnect\n", client->id, client->username, client->hostname);

    tcpClearClient(client);

    // Decrement published data client count

    fgcddev.status.tcp_clients--;

    // Clear maximum users warning

    fgcddev.status.st_warnings &= ~FGC_WRN_TCP_MAX_USERS;
}



/*
 * Socket send thread
 */

static void *tcpSend(void *arg)
{
    int                 buffer_length;
    ssize_t             bytes_out;
    uint32_t            bytes_remaining;
    struct Cmd          *command;
    struct Rterm_resp   *rterm_resp;
    union cmdq_member   *q_member;

    struct TCP_client *client = reinterpret_cast<TCP_client *>(arg);

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Process responses

    while(client->run)
    {
        if((q_member = reinterpret_cast<union cmdq_member*>(queuePop(&client->response_queue))) == NULL)
        {
            if (!client->run)
            {
                // Client closed the connection.

                break;
            }
            else
            {
                // Wait one cycle and poll again

                usleep(20*1000);

                continue;
            }

        }

        // Handle different types of structure

        switch(q_member->struct_type)
        {
            case CMD_STRUCT: // Response is a command structure

                pthread_mutex_lock(&client->mutex);
                client->num_cmds--;
                pthread_mutex_unlock(&client->mutex);

                command = (struct Cmd *)q_member;

                // Process command if remote terminal not in progress

                if(client->rterm_device == NULL)
                {
                    // Switch client mode if necessary

                    if(client->mode != TCP_MODE_CMD)
                    {
                        // Enable telnet client line-mode and local echo, and clear screen

                        send(client->sock, "\xFF\xFC\x22\xFF\xFC\x01\33c", 8, 0);
                        client->mode = TCP_MODE_CMD;
                    }

                    // Fill the response buffer with the formatted response

                    if(command->error > FGC_OK_RSP) // Response is an error
                    {
                        // EPCCCS-2527: set device ID to sensible default if command->device is NULL

                        if(command->device == NULL) command->device = &fgcd.device[0];

                        logPrintf(command->device->id, "TCP %d Request failed [%s]: %s\n", client->id, command->command_string, fgc_errmsg[command->error]);

                        buffer_length = snprintf(client->send_buffer, sizeof(client->send_buffer),
                                                 "$%s !\n%u %s\n;", command->tag,
                                                 command->error, fgc_errmsg[command->error]);

                        // Send error response to client

                        tcpSendClientBuffer(client, buffer_length);
                    }
                    else // Response is data
                    {
                        // Send tag to client

                        buffer_length = snprintf(client->send_buffer, sizeof(client->send_buffer), "$%s .\n", command->tag);

                        tcpSendClientBuffer(client, buffer_length);

                        // Return value for get commands

                        if(command->type == CMD_GET)
                        {
                            command->index = 0;

                            while(command->index < command->value_length)
                            {
                                // Send value to client in chunks of sizeof(client->send_buffer)

                                bytes_remaining = command->value_length - command->index;
                                buffer_length   = bytes_remaining > sizeof(client->send_buffer) ?
                                                  sizeof(client->send_buffer) : bytes_remaining;

                                memcpy(client->send_buffer, &command->value[command->index], buffer_length);

                                if(tcpSendClientBuffer(client, buffer_length) != 0)
                                {
                                    break;
                                }

                                command->index += buffer_length;
                            }
                        }

                        // Send end of response to client

                        buffer_length = snprintf(client->send_buffer, sizeof(client->send_buffer), "\n;");

                        tcpSendClientBuffer(client, buffer_length);
                    }
                }

                // Return the command structure to the free queue

                queuePush(&cmdqmgr.free_cmd_q, q_member);
                command  =  NULL;
                q_member =  NULL;

                break;

            case RTERM_STRUCT: // Response is a remote terminal response structure

                pthread_mutex_lock(&client->mutex);
                client->num_rterm_resps--;
                pthread_mutex_unlock(&client->mutex);

                rterm_resp = (struct Rterm_resp *)q_member;

                // Process remote terminal response if remote terminal in progress

                if(client->rterm_device != NULL)
                {
                    // Switch client mode if necessary

                    if(client->mode != TCP_MODE_RTERM)
                    {
                        // Disable telnet client line-mode and local echo

                        send(client->sock, "\xFF\xFD\x22\xFF\xFB\x01", 6, 0);
                        client->mode = TCP_MODE_RTERM;

                        logPrintf(client->rterm_device->id, "TCP %d (%s@%s) rterm\n",
                                  client->id, client->username, client->hostname);
                    }

                    if((bytes_out = send(client->sock,
                                         rterm_resp->buffer,
                                         rterm_resp->buffer_length,
                                         0)) > 0)
                    {
                        tcp.load += bytes_out;
                    }
                }

                // Return the remote terminal response structure to the free_rterm_q

                queuePush(&cmdqmgr.free_rterm_q, q_member);
                rterm_resp = NULL;
                q_member   = NULL;

                break;
        }
    }

    // Return current receive command structure to the free queue

    if(client->recv_cmd)
    {
        queuePush(&cmdqmgr.free_cmd_q, client->recv_cmd);
        client->recv_cmd = NULL;
    }

    // Check whether a remote terminal is in progress for this client

    if(client->rterm_device != NULL)
    {
        // Disconnect remote terminal

        logPrintf(client->rterm_device->id, "TCP %d (%s@%s) Remote terminal disconnected\n",
                  client->id, client->username, client->hostname);
        client->rterm_device = NULL;
    }

    // Wait for the receive thread to be finsihed (i.e. no more TCP command are sent to the cmdqmgr)

    pthread_join(client->recv_thread, NULL);

    // Return structures to free queues.
    // Note that after this point we do not need to protect the num_cmds and num_rterm_resps

    while(client->num_cmds || client->num_rterm_resps)
    {
        if((q_member = reinterpret_cast<union cmdq_member*>(queueBlockPop(&client->response_queue))))
        {
            switch(q_member->struct_type)
            {
                case CMD_STRUCT:
                    queuePush(&cmdqmgr.free_cmd_q, q_member);
                    q_member = NULL;
                    client->num_cmds--;
                    break;

                case RTERM_STRUCT:
                    queuePush(&cmdqmgr.free_rterm_q, q_member);
                    q_member = NULL;
                    client->num_rterm_resps--;
                    break;
            }
        }
    }

    // Disconnect client

    tcpDisconnect(client);

    // Detach this thread

    pthread_detach(client->send_thread);

    return 0;
}



/*
 * Socket receive thread
 */

static void *tcpRecv(void *arg)
{
    ssize_t         bytes_in;
    ssize_t         i;

    struct TCP_client *client = reinterpret_cast<TCP_client*>(arg);

    logPrintf(0, "TCP %d Connect %s\n", client->id, client->hostname);

    // Receive a '+' to confirm that client is an FGC client

    if((bytes_in = recv(client->sock, client->recv_buffer, 1, 0)) > 0)
    {
        if(client->recv_buffer[0] != '+')
        {
            logPrintf(0, "TCP %d Protocol error\n", client->id);

            tcpDisconnect(client);
            return 0;    // return value is not checked
        }
    }
    else
    {
        tcpDisconnect(client);
        return 0;    // return value is not checked
    }

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Spawn send thread

    if(fgcd_thread_create(&client->send_thread, tcpSend, client, TCP_CLIENT_THREAD_POLICY, TCP_CLIENT_THREAD_PRIORITY ,NO_AFFINITY) != 0)
    {
        logPrintf(0, "TCP %d (%s@%s) Start send thread failed\n", client->id, client->username, client->hostname);

        tcpDisconnect(client);
        return 0;    // return value is not checked
    }

    // Get a command structure

    if((client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q)))
    {
        client->recv_cmd->response_queue = &client->response_queue;
        client->cmd_state = TCP_RECV_IDLE;

        // Handle commands

        while(client->run)
        {
            if((bytes_in = recv(client->sock,
                                client->recv_buffer,
                                client->mode == TCP_MODE_RTERM ? 1 : sizeof(client->recv_buffer) - 1,
                                0)) <= 0)
            {
                break;
            }
            client->recv_buffer[bytes_in] = '\0';

            // Add bytes to TCP load

            tcp.load += bytes_in;

            // Check client mode

            switch(client->mode)
            {
                case TCP_MODE_CMD:      // Client is in command mode

                    tcpProcessCmdBuffer(client, bytes_in);
                    break;

                case TCP_MODE_RTERM:    // Client is in remote terminal mode

                    // Add characters to device's queue

                    for(i = 0 ; i < bytes_in ; i++)
                    {
                        // Check for disconnect character if rterm not locked

                        if(!client->rterm_locked && client->recv_buffer[i] == RTERM_DISCONNECT_CHAR)
                        {
                            // Enable telnet client line-mode and local echo, and clear screen

                            send(client->sock, "\xFF\xFC\x22\xFF\xFC\x01\33c", 8, 0);
                            send(client->sock, "Remote terminal disconnected.\n", 30, 0);
                            client->mode = TCP_MODE_CMD;

                            logPrintf(client->rterm_device->id, "TCP %d (%s@%s) Remote terminal disconnected\n",
                                      client->id, client->username, client->hostname);
                            client->rterm_device = NULL;
                            break;
                        }
                        else // Character is not disconnect character
                        {
                            // Get current time

                            struct timeval  time;
                            timingGetUserTime(0, &time);

                            // Check whether client has an expired RBAC token

                            if(fgcd.enable_rbac     &&
                               client->rbac_token   &&
                               client->rbac_token_expiry < time.tv_sec)
                            {
                                logPrintf(client->rterm_device->id,
                                          "TCP %d (%s@%s) RBAC token expired - remote terminal disconnected\n",
                                          client->id, client->username, client->hostname);

                                client->rterm_device    = NULL;
                                client->mode            = TCP_MODE_CMD;

                                if(client->rterm_locked)
                                {
                                    // Disconnect client

                                    client->run = 0;
                                }
                                else // Remote terminal is not locked
                                {
                                    // Enable telnet client line-mode and local echo, and clear screen

                                    send(client->sock, "\xFF\xFC\x22\xFF\xFC\x01\33c", 8, 0);
                                    send(client->sock, "RBAC token expired - remote terminal disconnected.\n", 51, 0);
                                }
                                break;
                            }
                            else // RBAC token is valid
                            {
                                if(client->rterm_device->rterm_ready)
                                {
                                    charqBlockPush(&client->rterm_device->rterm_tx_q, client->recv_buffer[i]);
                                }
                            }
                        }
                    }
                    break;
            }
       }
    }
    else
    {
        logPrintf(0, "TCP %d (%s@%s) Unable to obtain command structure\n",
                  client->id, client->username, client->hostname);
    }

    // Stop the tcpSend thread by changing the run flag

    client->run = 0;

    // Sleep for a couple of 20 ms cycles (FGC_Ether and WFIP), so that there are no more remote terminal commands

    usleep(40*1000);

    return 0;
}



/*
 * Process buffer of incoming commands
 */

static void tcpProcessCmdBuffer(struct TCP_client *client, ssize_t buffer_length)
{
    unsigned char   *buffer = client->recv_buffer;
    int32_t         channel;
    ssize_t         i;

    for(i = 0 ; i < buffer_length ; i++)
    {
        switch(client->cmd_state)
        {
            case TCP_RECV_BIN_LENGTH:

                ((unsigned char *)&client->bin_length)[client->bin_index++] = buffer[i];

                // Check whether binary length has been completely received

                if(client->bin_index == sizeof(client->bin_length))
                {
                    client->bin_length  = ntohl(client->bin_length);
                    client->bin_index   = 0;

                    if(client->bin_length)
                    {
                        if(client->bin_length < (CMD_MAX_VAL_LENGTH - client->recv_cmd->value_length))
                        {
                            client->cmd_state = client->recv_cmd->error ?
                                                TCP_RECV_BIN_ERROR      :
                                                TCP_RECV_BIN_VALUE;
                        }
                        else // Data is too long
                        {
                            client->recv_cmd->error = FGC_CMD_BUF_FULL;
                            client->cmd_state       = TCP_RECV_BIN_ERROR;
                        }
                    }
                    else // Length is zero
                    {
                        // Command complete

                        // Remove trailing spaces

                        while(client->recv_cmd->command_length &&
                              client->recv_cmd->command_string[client->recv_cmd->command_length - 1] == ' ')
                        {
                            client->recv_cmd->command_length--;
                        }

                        // Null-terminate command

                        client->recv_cmd->command_string[client->recv_cmd->command_length] = '\0';

                        // Replace user name by number

                        if (tcpReplaceUserNameByNumber(client->recv_cmd)!=0)
                        {
                            client->cmd_state = TCP_RECV_BIN_ERROR;
                            break;
                        }

                        // Log binary set

                        if(client->recv_cmd->type == CMD_GET)
                        {
                            logPrintf(0, "TCP %d (%s@%s) cmd (G %s <0B empty binary data>)\n",
                                client->id, client->username, client->hostname,
                                client->recv_cmd->command_string);
                        }
                        else
                        {
                            logPrintf(0, "TCP %d (%s@%s) cmd (S %s <0B empty binary data>)\n",
                                client->id, client->username, client->hostname,
                                client->recv_cmd->command_string);
                        }

                        // Queue command

                        client->recv_cmd->rbac_token = client->rbac_token;
                        pthread_mutex_lock(&client->mutex);
                        client->num_cmds++;
                        pthread_mutex_unlock(&client->mutex);
                        cmdqmgrQueue(client->recv_cmd);
                        client->recv_cmd = NULL;

                        // Get a new command structure

                        client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);
                        assert(client->recv_cmd);
                        client->recv_cmd->response_queue = &client->response_queue;
                        client->cmd_state                = TCP_RECV_IDLE;
                    }
                }

                break;

            case TCP_RECV_BIN_VALUE:

                // Add byte to value

                client->recv_cmd->value[client->recv_cmd->value_length++] = buffer[i];

                // Check whether binary value has been completely received

                if(++client->bin_index == client->bin_length)
                {
                    // Command complete

                    // Remove trailing spaces

                    while(client->recv_cmd->command_length &&
                          client->recv_cmd->command_string[client->recv_cmd->command_length - 1] == ' ')
                    {
                        client->recv_cmd->command_length--;
                    }

                    // Null-terminate command

                    client->recv_cmd->command_string[client->recv_cmd->command_length] = '\0';

                    // Replace the user name by the number

                    if(tcpReplaceUserNameByNumber(client->recv_cmd)!=0)
                    {
                        client->cmd_state = TCP_RECV_BIN_ERROR;
                        break;
                    }

                    // Log binary set

                    if(client->recv_cmd->type == CMD_GET)
                    {
                        logPrintf(client->recv_cmd->device->id,
                            "TCP %d (%s@%s) cmd (G %s <%uB binary data>)\n",
                            client->id, client->username, client->hostname,
                            client->recv_cmd->command_string,
                            client->bin_length);
                    }
                    else
                    {
                        logPrintf(client->recv_cmd->device->id,
                            "TCP %d (%s@%s) cmd (S %s <%uB binary data>)\n",
                            client->id, client->username, client->hostname,
                            client->recv_cmd->command_string,
                            client->bin_length);
                    }

                    // Queue command

                    client->recv_cmd->rbac_token = client->rbac_token;
                    pthread_mutex_lock(&client->mutex);
                    client->num_cmds++;
                    pthread_mutex_unlock(&client->mutex);
                    cmdqmgrQueue(client->recv_cmd);
                    client->recv_cmd = NULL;

                    // Get a new command structure

                    client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);
                    assert(client->recv_cmd);
                    client->recv_cmd->response_queue = &client->response_queue;
                    client->cmd_state                = TCP_RECV_IDLE;
                }

                break;

            case TCP_RECV_BIN_ERROR:

                if(++client->bin_index == client->bin_length)
                {
                    // Command received
                    // Return error

                    // If a specific error message has not yet been set, default to FGC_UNKNOWN_CMD

                    if(!client->recv_cmd->error)
                    {
                        client->recv_cmd->error = FGC_UNKNOWN_CMD;
                    }

                    pthread_mutex_lock(&client->mutex);
                    client->num_cmds++;
                    pthread_mutex_unlock(&client->mutex);
                    queuePush(client->recv_cmd->response_queue, client->recv_cmd);
                    client->recv_cmd = NULL;

                    // Get a new command structure

                    client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);
                    assert(client->recv_cmd);
                    client->recv_cmd->response_queue = &client->response_queue;
                    client->cmd_state                = TCP_RECV_IDLE;
                }

                break;

            default:

                switch(buffer[i])
                {
                    case ';':
                        break;

                    case '!': // The start of a command

                        cmdqmgrInitCmd(client->recv_cmd);
                        client->recv_cmd->client            = client;
                        client->recv_cmd->response_queue    = &client->response_queue;

                        client->bin_index       = 0;
                        client->dev_index       = 0;
                        client->last_char_space = 0;
                        client->cmd_state       = TCP_RECV_TAG;

                        break;

                    case ' ':
                        switch(client->cmd_state)
                        {
                            case TCP_RECV_IDLE:
                            case TCP_RECV_TYPE:
                            case TCP_RECV_ERROR:
                                break;

                            case TCP_RECV_TAG:

                                // Tag is finished
                                // Null-terminate tag

                                client->recv_cmd->tag[client->recv_cmd->tag_length] = '\0';

                                // Change state to TCP_RECV_TYPE

                                client->cmd_state = TCP_RECV_TYPE;

                                break;

                            case TCP_RECV_DEV:

                                if(client->dev_index > 0)
                                {
                                    // Spaces are not allowed in the middle of a device, return error

                                    client->recv_cmd->error = FGC_NO_COLON;
                                    client->cmd_state       = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_PROP:

                                if(!client->last_char_space)
                                {
                                    // Add space to command string

                                    client->recv_cmd->command_string[client->recv_cmd->command_length++] = ' ';

                                    client->last_char_space = 1;

                                    if(client->recv_cmd->type == CMD_SET)
                                    {
                                        client->cmd_state = TCP_RECV_VALUE;
                                    }
                                }

                                break;

                            case TCP_RECV_VALUE:

                                // Add space to command string

                                client->recv_cmd->value[client->recv_cmd->value_length++] = ' ';

                                break;

                            default:
                                break;
                        }

                        break;

                    case ':':

                        if(client->cmd_state == TCP_RECV_DEV)
                        {
                            // Device is finished
                            // Null-terminate device name

                            client->recv_cmd->device_name[client->dev_index] = '\0';

                            // Resolve device so that log messages can be associated with it

                            if(!devnameSubResolve(client->recv_cmd->device_name, (uint32_t *)&channel, &client->recv_cmd->sub_device)) // Attempt to resolve a sub device
                            {
                                client->recv_cmd->device = &fgcd.device[channel];

                                // Change state to TCP_RECV_PROP

                                client->cmd_state = TCP_RECV_PROP;
                            }
                            else if((channel = devnameResolve(client->recv_cmd->device_name)) >= 0) // Attempt to resolve a device
                            {
                                client->recv_cmd->device = &fgcd.device[channel];

                                // Overwrite device name with address to prevent need for later resolution

                                sprintf(client->recv_cmd->device_name, "%d", channel);

                                // Change state to TCP_RECV_PROP

                                client->cmd_state = TCP_RECV_PROP;
                            }
                            else // Device name resolution failed
                            {
                                // Change state to TCP_RECV_ERROR

                                client->recv_cmd->error = FGC_UNKNOWN_DEV;
                                client->cmd_state       = TCP_RECV_ERROR;
                            }
                        }
                        else // Colon unexpected
                        {
                            // Change state to TCP_RECV_ERROR

                            client->recv_cmd->error = FGC_UNKNOWN_SYM;
                            client->cmd_state       = TCP_RECV_ERROR;
                        }

                        break;

                    case '\r':
                    case '\n':

                        if(client->cmd_state == TCP_RECV_PROP ||
                           client->cmd_state == TCP_RECV_VALUE)
                        {
                            // Command string is finished

                            // Remove trailing spaces

                            while(client->recv_cmd->command_length &&
                                  client->recv_cmd->command_string[client->recv_cmd->command_length - 1] == ' ')
                            {
                                client->recv_cmd->command_length--;
                            }

                            // Null-terminate command

                            client->recv_cmd->command_string[client->recv_cmd->command_length]  = '\0';

                            // Null-terminate the value

                            client->recv_cmd->value[client->recv_cmd->value_length]             = '\0';

                            // Check whether command length is zero
                            if(client->recv_cmd->command_length == 0)
                            {
                                // Change state to TCP_RECV_ERROR

                                client->recv_cmd->error = FGC_NO_SYMBOL;
                                client->cmd_state       = TCP_RECV_ERROR;
                            }
                            else if(tcpReplaceUserNameByNumber(client->recv_cmd)!=0)
                            {
                                client->cmd_state = TCP_RECV_ERROR;
                            }

                            // send command to FGC or error response back to client
                            if(client->cmd_state == TCP_RECV_ERROR)
                            {
                                pthread_mutex_lock(&client->mutex);
                                client->num_cmds++;
                                pthread_mutex_unlock(&client->mutex);

                                queuePush(client->recv_cmd->response_queue, client->recv_cmd);
                                client->recv_cmd = NULL;

                                // Get a new command structure

                                client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);
                                assert(client->recv_cmd);
                                client->recv_cmd->response_queue = &client->response_queue;
                                client->cmd_state                = TCP_RECV_IDLE;
                            }
                            else
                            {
                                // Command complete

                                // Log command

                                if(client->recv_cmd->type == CMD_GET)
                                {
                                    logPrintf(client->recv_cmd->device->id,
                                              "TCP %d (%s@%s) cmd (G %s)\n",
                                              client->id, client->username, client->hostname,
                                              client->recv_cmd->command_string);
                                }
                                else // Set command
                                {
                                    logPrintf(client->recv_cmd->device->id,
                                              "TCP %d (%s@%s) cmd (S %s %s)\n",
                                              client->id, client->username, client->hostname,
                                              client->recv_cmd->command_string,
                                              client->recv_cmd->value);
                                }

                                // Queue command

                                client->recv_cmd->rbac_token = client->rbac_token;
                                pthread_mutex_lock(&client->mutex);
                                client->num_cmds++;
                                pthread_mutex_unlock(&client->mutex);
                                cmdqmgrQueue(client->recv_cmd);
                                client->recv_cmd = NULL;

                                // Get a new command structure

                                client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);
                                assert(client->recv_cmd);
                                client->recv_cmd->response_queue = &client->response_queue;
                                client->cmd_state                = TCP_RECV_IDLE;

                            }
                        }
                        else if(client->cmd_state != TCP_RECV_IDLE)
                        {
                            // Return error

                            // If a specific error message has not yet been set, default to FGC_UNKNOWN_CMD

                            if(!client->recv_cmd->error)
                            {
                                client->recv_cmd->error = FGC_UNKNOWN_CMD;
                            }

                            pthread_mutex_lock(&client->mutex);
                            client->num_cmds++;
                            pthread_mutex_unlock(&client->mutex);
                            queuePush(client->recv_cmd->response_queue, client->recv_cmd);
                            client->recv_cmd = NULL;

                            // Get a new command structure

                            client->recv_cmd = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q);
                            assert(client->recv_cmd);
                            client->recv_cmd->response_queue = &client->response_queue;
                            client->cmd_state                = TCP_RECV_IDLE;
                        }

                        break;

                    case 0xFF: //  Binary value is being received

                        if(client->cmd_state != TCP_RECV_VALUE)
                        {
                            client->recv_cmd->error = FGC_UNEXPECTED_BIN_DATA;
                        }
                        client->recv_cmd->type  = CMD_SET_BIN;
                        client->cmd_state       = TCP_RECV_BIN_LENGTH;

                        break;

                    default:

                        switch(client->cmd_state)
                        {
                            case TCP_RECV_IDLE:
                                break;

                            case TCP_RECV_ERROR:

                                if(buffer[i] == 0xFF)
                                {
                                    client->recv_cmd->type  = CMD_SET_BIN;
                                    client->cmd_state       = TCP_RECV_BIN_LENGTH;
                                    break;
                                }

                                break;

                            case TCP_RECV_TAG:

                                // Check that the maximum tag length has not been exceeded

                                if(client->recv_cmd->tag_length < CMD_MAX_TAG_LENGTH)
                                {
                                    // Add character to tag

                                    client->recv_cmd->tag[client->recv_cmd->tag_length++] = buffer[i];
                                }
                                else // Maximum tag length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    client->recv_cmd->error = FGC_CMD_BUF_FULL;
                                    client->cmd_state       = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_TYPE:

                                // Check for set or get

                                if(toupper(buffer[i]) == 'S')
                                {
                                    client->recv_cmd->type  = CMD_SET;
                                    client->cmd_state       = TCP_RECV_DEV;
                                }
                                else if(toupper(buffer[i]) == 'G')
                                {
                                    client->recv_cmd->type  = CMD_GET;
                                    client->cmd_state       = TCP_RECV_DEV;
                                }
                                else // Invalid type character
                                {
                                    // Change state to TCP_RECV_ERROR

                                    client->cmd_state = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_DEV:

                                // Check that the maximum device length has not been exceeded

                                if(client->dev_index < FGC_MAX_DEV_LEN)
                                {
                                    // Add character to device name

                                    client->recv_cmd->device_name[client->dev_index++] = toupper(buffer[i]);
                                }
                                else // Maximum device length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    client->recv_cmd->error = FGC_UNKNOWN_DEV;
                                    client->cmd_state       = TCP_RECV_ERROR;
                                }

                                break;

                            case TCP_RECV_PROP:

                                // Check that the maximum command length has not been exceeded

                                if(client->recv_cmd->command_length < CMD_MAX_LENGTH)
                                {
                                    // Add character to command string

                                    client->recv_cmd->command_string[client->recv_cmd->command_length++] = buffer[i];
                                }
                                else // Maximum command length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    client->recv_cmd->error = FGC_CMD_BUF_FULL;
                                    client->cmd_state       = TCP_RECV_ERROR;
                                }

                                client->last_char_space = 0;

                                break;

                            case TCP_RECV_VALUE:

                                // Check that the maximum command length has not been exceeded

                                if(client->recv_cmd->value_length < CMD_MAX_VAL_LENGTH)
                                {
                                    // Add character to command string

                                    client->recv_cmd->value[client->recv_cmd->value_length++] = buffer[i];
                                }
                                else // Maximum value length exceeded
                                {
                                    // Change state to TCP_RECV_ERROR

                                    client->recv_cmd->error = FGC_CMD_BUF_FULL;
                                    client->cmd_state       = TCP_RECV_ERROR;
                                }

                                client->last_char_space = 0;

                                break;

                            default:
                                break;
                        }
                        break;
                }
        }
    }
}

// EOF
