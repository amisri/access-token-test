/*!
 * @file   fgcsm.c
 * @brief  Functions for state machines for communicating with FGCs over fieldbuses
 * @author Stephen Page
 */

#include <stdio.h>
#include <string.h>

// TODO: This module should be decoupled from class 51.
//       The dependencies could be pushed to pm.cpp clas modules by instantiating fgcsmPMAcq() and fgcsmPMTrigger() functions
#include <classes/51/defconst.h>

#include <algorithm>
#include <string>

#include <cmd.h>
#include <cmdqmgr.h>
#include <cmwpub.h>
#include <consts.h>
#include <defconst.h>
#include <fieldbus_class.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <fgcsm.h>
#include <fieldbus.h>
#include <logging.h>
#include <pm.h>
#include <pub.h>
#include <queue.h>
#include <tcp.h>
#include <timing.h>


// Global variables

struct FGCSM fgcsm;



// Static functions

static void fgcsmPMTrigger(uint32_t channel_num);
static void fgcsmPMAcq(uint32_t channel_num);

int32_t fgcsmInit(void)
{
    struct FGCSM_channel    *channel;
    static uint32_t         first_time = 1;
    uint32_t                i;

    // Initialise state of each FGC

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        channel = &fgcsm.channel[i];

        channel->current_command    = NULL;
        channel->current_pub_data   = NULL;

        channel->cmd_state          = FGC_FGC_STATE_OFFLINE;
        channel->pub_state          = FGC_PUB_START;
        channel->rsp_state          = FGC_RSP_START;

        channel->prev_rsp_seq       = 0;
        channel->status_received    = 0;

        fgcd.device[i].remote       = 1;
        fgcd.device[i].rterm_ready  = 1;

        // Perform actions the first time only

        if(first_time)
        {
            // Initialise device command queue

            if(queueInit(&channel->command_queue, cmdqmgr.num_cmds))
            {
                // Failed to initialise device command queue

                fprintf(stderr, "ERROR: fgcsmInit:Failed to initialise command queue for device %u\n", i);

                // Free device command queues created so far

                for(i-- ; i >= 1 ; i--)
                {
                    fgcd.device[i].command_queue = NULL;
                    queueFree(&channel->command_queue);
                }
                return 1;
            }
            fgcd.device[i].command_queue = &channel->command_queue;
        }
    }

    first_time = 0;

    return 0;
}



void fgcsmProcessStatus(uint32_t channel_num)
{
    struct FGCSM_channel        *channel            = &fgcsm.channel[channel_num];
    union fgc_pub_class_data    *class_data         = &fieldbus.channel[channel_num].status.fgc_stat.class_data;;
    uint32_t                    code_length;
    uint32_t                    code_request;
    struct Cmd                  *command;
    struct FGCD_device          *device             = &fgcd.device[channel_num];
    uint32_t                    i;
    uint32_t                    num_code_requests;
    struct Rterm_resp           *rterm_resp;
    uint32_t                    rterm_resp_len;
    union fgc_stat_var          *status             = &fieldbus.channel[channel_num].status;
    struct timeval              time;
    uint32_t                    value_length;

    // Increment variables received counter

    channel->count.status_rec++;

    // Check whether the status has already been received this cycle

    if(channel->status_received)
    {
        logPrintf(channel_num, "FGC Multiple statuses received within the same cycle\n");
    }
    channel->status_received = 1;

    // Check whether time was missed

    if(!(status->fieldbus_stat.ack & FGC_ACK_TIME))
    {
        channel->count.time_miss++;
        fgcsm.count.time_miss++;
    }

    // Reset sequential status missed counter

    channel->count.seq_status_miss = 0;

    // Store run log byte

    fieldbus.channel[channel_num].runlog[fieldbus.fgc_runlog_idx] = status->fieldbus_stat.runlog;

    // Handle class-specific requirements

    if(status->fgc_stat.class_id % 10) // Class is not a base class
    {
        // Check whether real-time variable was missed

        if(!(status->fieldbus_stat.ack & FGC_ACK_RD_PKT))
        {
            channel->count.rd_miss++;
            fgcsm.count.rd_miss++;
        }
    }
    else // Class is a base class
    {
        // Add code requests to slot votes

        if(class_data->boot.code_request)
        {
            code_request = ntohl(class_data->boot.code_request);

            // Count code requests

            i = code_request;
            for(num_code_requests = 0 ; i ; num_code_requests++)
            {
                // Clear least significant bit set

                i &= i - 1;
            }

            // Vote for requested codes

            for(i = 0 ; i < FGC_CODE_NUM_SLOTS ; i++)
            {
                code_length = fgcddev.fgc_code_length[i];

                if(code_length && code_request & (1 << i))
                {
                    fieldbus.fgc_code_votes[i] += 1000000. / (fgcddev.fgc_code_length[i] * num_code_requests);
                }
            }
        }

        // Process any remote terminal characters from base class

        class_data->boot.rterm[sizeof(class_data->boot.rterm) - 1] = '\0';
        rterm_resp_len = strlen(class_data->boot.rterm);

        if(rterm_resp_len)
        {
            // Check whether each client has a remote terminal on this channel

            for(i = 0 ; i < FGC_TCP_MAX_CLIENTS ; i++)
            {
                if(tcp.client[i].run                    &&
                   tcp.client[i].rterm_device == device &&
                   (rterm_resp = (struct Rterm_resp *)queuePop(&cmdqmgr.free_rterm_q)))
                {
                    strncpy(rterm_resp->buffer,
                            class_data->boot.rterm,
                            sizeof(class_data->boot.rterm));
                    rterm_resp->buffer[sizeof(class_data->boot.rterm) - 1] = '\0';
                    rterm_resp->buffer_length = rterm_resp_len;

                    pthread_mutex_lock(&tcp.client[i].mutex);
                    tcp.client[i].num_rterm_resps++;
                    pthread_mutex_unlock(&tcp.client[i].mutex);

                    queuePush(&tcp.client[i].response_queue, rterm_resp);
                    rterm_resp = NULL;
                }
            }
        }
    }

    // Trigger post-mortem acquisitions

    fgcsmPMTrigger(channel_num);

    // Check whether FGC PLL is locked

    device->online = 1;
    if((status->fieldbus_stat.ack & FGC_ACK_PLL)) // FGC PLL is locked
    {
        if(status->fgc_stat.class_id % 10) // Class is not a base class
        {
            switch(channel->cmd_state)
            {
                case FGC_FGC_STATE_OFFLINE:
                case FGC_FGC_STATE_NOLOCK:
                case FGC_FGC_STATE_INBOOT:

                    logPrintf(channel_num, "FGC Idle\n");

                    channel->cmd_state  = FGC_FGC_STATE_IDLE;
                    channel->pub_state  = FGC_PUB_START;
                    device->ready       = 1;

                    // Add FGC to mask of devices

                    fgcddev.status.fgc_mask |= 1 << device->id;

                    // Trigger recovery of CMW subscriptions

                    cmwpubNotifyRecoverSubs(device->id);

                    break;
            }
        }
        else // Class is a base class
        {
            // FGC is running the boot program

            if(channel->cmd_state != FGC_FGC_STATE_INBOOT)
            {
                logPrintf(channel_num, "FGC Boot\n");

                channel->cmd_state = FGC_FGC_STATE_INBOOT;
                device->ready      = 0;

                // Add FGC to mask of devices

                fgcddev.status.fgc_mask |= 1 << device->id;
            }
        }
    }
    else // FGC PLL is unlocked
    {
        if((channel->cmd_state != FGC_FGC_STATE_NOLOCK) &&
           (channel->cmd_state != FGC_FGC_STATE_WAITEXEC))
        {
            if(channel->cmd_state != FGC_FGC_STATE_OFFLINE)
            {
                logPrintf(channel_num, "FGC PLL unlocked\n");
            }

            // If there is a command queued, return an error to it

            if((command = channel->current_command))
            {
                command->error = FGC_PLL_NOT_LOCKED;
                channel->current_command = NULL;
                queuePush(command->response_queue, command);
                command = NULL;
            }

            channel->cmd_state          = FGC_FGC_STATE_NOLOCK;
            channel->prev_pub_seq       = 0;
            channel->pub_state          = FGC_PUB_START;
            fgcddev.status.fgc_mask    &= ~(1 << device->id);

            // Set device as not ready

            device->ready = 0;

            // Return published data command

            if(channel->current_pub_data)
            {
                struct Cmd* command = channel->current_pub_data;
                channel->current_pub_data = NULL;
                queuePush(&cmdqmgr.free_cmd_q, command);
            }

            // Increment unlock counts

            channel->count.unlock_transition++;
            fgcsm.count.unlock_transition++;
        }
    }

    // Process FGC's command state

    switch(channel->cmd_state)
    {
        case FGC_FGC_STATE_IDLE:

            // Send a command if there is one in the channel's queue

            if((channel->current_command = (struct Cmd *)queuePop(&channel->command_queue)))
            {
                command = channel->current_command;

                // Set remaining bytes to length of command (1 is a space character)

                command->remaining = command->command_length + 1 + command->value_length;

                if(fieldbusSendCommand(device->id, command))
                {
                    // Packet transmission failed, return error to client

                    command->error = FGC_IO_ERROR;
                    channel->current_command = NULL;
                    queuePush(command->response_queue, command);
                    command = NULL;

                }
            }
            break;

        case FGC_FGC_STATE_NOLOCK:

            // Return error to all queued commands

            while((command = (struct Cmd *)queuePop(&channel->command_queue)))
            {
                command->error = FGC_DEV_NOT_READY;
                queuePush(command->response_queue, command);
                command = NULL;
            }
            break;

        case FGC_FGC_STATE_WAITACK:

            // Check for toggle to acknowledge packet reception

            if((status->fieldbus_stat.ack & FGC_ACK_CMD_TOG) != channel->prev_toggle_bit)
            {
                // Packet has been acknowledged

                // If there is data, send next packet

                command = channel->current_command;
                if(command->remaining != 0)
                {
                    // Check FGC status

                    if(status->fieldbus_stat.cmd_stat == FGC_RECEIVING)
                    {
                        // Send next command packet

                        if(fieldbusSendCommand(device->id, command))
                        {
                            // Packet transmission failed, return error to client

                            command->error = FGC_IO_ERROR;
                        }
                    }
                    else // The FGC has returned an error
                    {
                        command->error = status->fieldbus_stat.cmd_stat < FGC_NUM_ERRS ?
                                         status->fieldbus_stat.cmd_stat : FGC_UNKNOWN_ERROR_CODE;

                        // Check that error is valid

                        if(command->error <= FGC_EXECUTING)
                        {
                            // Response is incomplete, return protocol error

                            logPrintf(channel_num,
                                      "FGC Protocol error - invalid cmd_stat (%hhu %s) from FGC while sending command\n",
                                      status->fieldbus_stat.cmd_stat, fgc_errmsg[status->fieldbus_stat.cmd_stat]);

                            channel->count.proto_error++;
                            command->error = FGC_PROTO_ERROR;
                        }
                    }

                    // Check whether an error has occurred

                    if(command->error)
                    {
                        // Push the command onto its response queue

                        channel->current_command = NULL;
                        queuePush(command->response_queue, command);
                        command = NULL;

                        // Change state to FGC_FGC_STATE_IDLE

                        channel->cmd_state = FGC_FGC_STATE_IDLE;
                    }

                    break;
                }
                else // Command has been fully transmitted
                {
                    // Change state to FGC_FGC_STATE_WAITEXEC

                    channel->cmd_state = FGC_FGC_STATE_WAITEXEC;
                    channel->rsp_state = FGC_RSP_START;

                    // Store the time at which the command started executing

                    timingGetUserTime(0, &channel->current_cmd_exec_time);

                    // Fall through to case FGC_FGC_STATE_WAITEXEC
                }
            }
            else // A toggle was missed
            {
                channel->count.ack_miss++;
                fgcsm.count.ack_miss++;
                channel->count.seq_toggle_bit_miss++;

                break;
            }

        case FGC_FGC_STATE_WAITEXEC:

            command = channel->current_command;

            switch(status->fieldbus_stat.cmd_stat)
            {
                case(FGC_EXECUTING):

                    // Check whether a timeout has occurred

                    timingGetUserTime(0, &time);
                    time = timingCompareTimevals(&time, &channel->current_cmd_exec_time);

                    if(time.tv_sec >= FGCSM_MAX_EXEC_TIME_SECS)
                    {
                        logPrintf(channel_num, "FGC Protocol error (FGC timeout in EXECUTING)\n");

                        channel->count.proto_error++;
                        command->error = FGC_PROTO_ERROR;

                        // Push the command onto its response queue

                        channel->current_command = NULL;
                        queuePush(command->response_queue, command);
                        command = NULL;

                        // Change state to FGC_FGC_STATE_IDLE

                        channel->cmd_state = FGC_FGC_STATE_IDLE;
                    }

                    break;

                case(FGC_OK_NO_RSP):

                    // Return okay to client

                    command->error = FGC_OK_NO_RSP;

                    // Push the command onto its response queue

                    channel->current_command = NULL;
                    queuePush(command->response_queue, command);
                    command = NULL;

                    // Change state to FGC_FGC_STATE_IDLE

                    channel->cmd_state = FGC_FGC_STATE_IDLE;

                    break;

                case(FGC_OK_RSP):

                    // Check that response has been fully received

                    if(channel->rsp_state != FGC_RSP_END &&
                       !command->error)
                    {
                        // Response is incomplete, return protocol error

                        logPrintf(channel_num, "FGC Protocol error (incomplete response)\n");

                        channel->count.proto_error++;
                        command->error = FGC_PROTO_ERROR;
                    }

                    // Check whether value is binary (first byte is 0xFF)

                    if(!command->error &&
                       (uint8_t)command->value[0] == 0xFF) // value is binary
                    {
                        // Check that value is long enough to include a length

                        memcpy(&value_length,&command->value[1],sizeof(uint32_t));
                        value_length = htonl(value_length);

                        if(command->value_length < 1 + sizeof(uint32_t)) // Value is too short
                        {
                            logPrintf(device->id, "FGC Protocol error (binary value too short to include header)\n");

                            channel->count.proto_error++;
                            command->error = FGC_PROTO_ERROR;
                        }
                        else if(command->value_length !=
                                (1 + sizeof(uint32_t) + value_length)) // Value does not match claimed length
                        {
                            logPrintf(channel_num, "FGC Protocol error (value length (%u) does not match expected (%u))\n",
                                      command->value_length - (1 + sizeof(uint32_t)),
                                      value_length);

                            channel->count.proto_error++;
                            command->error = FGC_PROTO_ERROR;
                        }
                    }

                    // Push the command onto its response queue

                    channel->current_command = NULL;
                    queuePush(command->response_queue, command);
                    command = NULL;

                    // Change state to FGC_FGC_STATE_IDLE

                    channel->cmd_state = FGC_FGC_STATE_IDLE;

                    break;

                case(FGC_RECEIVING):

                    // FGC device stuck in receiving

                    logPrintf(channel_num, "FGC Protocol error (FGC stuck in RECEIVING)\n");

                    channel->count.proto_error++;
                    command->error = FGC_PROTO_ERROR;

                    // Push the command onto its response queue

                    channel->current_command = NULL;
                    queuePush(command->response_queue, command);
                    command = NULL;

                    // Change state to FGC_FGC_STATE_IDLE

                    channel->cmd_state = FGC_FGC_STATE_IDLE;

                    break;

                default:

                    // FGC has returned an error

                    command->error = status->fieldbus_stat.cmd_stat < FGC_NUM_ERRS ?
                                     status->fieldbus_stat.cmd_stat : FGC_UNKNOWN_ERROR_CODE;

                    // Push the command onto its response queue

                    channel->current_command = NULL;
                    queuePush(command->response_queue, command);
                    command = NULL;

                    // Change state to FGC_FGC_STATE_IDLE

                    channel->cmd_state = FGC_FGC_STATE_IDLE;

                    break;
            }

            break;

        case FGC_FGC_STATE_INBOOT:
        case FGC_FGC_STATE_OFFLINE:

            // FGC in BOOT or OFFLINE, but we are waiting for a command

            if((command = channel->current_command))
            {
                logPrintf(channel_num, "FGC Protocol error (Waiting for response while FGC in BOOT or OFFLINE): %s\n",
                    command->command_string);

                command->error = FGC_PROTO_ERROR;

                channel->current_command = NULL;
                queuePush(command->response_queue, command);
                command = NULL;
            }

            break;

        case FGC_FGC_STATE_QUEUED:

            // Nothing to be done

            break;

        default:

            // Something went wrong and the command state had an incorrect value

            logPrintf(channel_num, "FGC Protocol error (Unknown command state = %u)\n",channel->cmd_state);

            if((command = channel->current_command))
            {

                command->error = FGC_PROTO_ERROR;

                // Push the command onto its response queue

                channel->current_command = NULL;
                queuePush(command->response_queue, command);
                command = NULL;
            }

            // Something went wrong, then reset the state

            channel->cmd_state = FGC_FGC_STATE_OFFLINE;

            break;
    }

    // Set previous toggle bit value

    channel->prev_toggle_bit = status->fieldbus_stat.ack & FGC_ACK_CMD_TOG;
}



uint32_t fgcsmPrepareCommand(uint32_t channel_num, struct Cmd *cmd, struct fgc_fieldbus_cmd *cmd_msg)
{
    uint32_t    cmd_msg_len = 0;
    int32_t     cmd_str_remaining;
    uint32_t    i;
    int32_t     val_remaining;

    // Construct packet header

    cmd_msg->header.flags = FGC_FIELDBUS_FLAGS_CMD_PKT;

    if(cmd->remaining == (cmd->command_length + 1 + cmd->value_length))
    {
        cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_FIRST_PKT;
    }

    // Set user

    cmd_msg->header.user = cmd->user;

    // Set command type

    switch(cmd->type)
    {
        case CMD_GET:
            cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_GET_CMD;
            break;

        case CMD_GET_SUB:
            cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_GET_SUB_CMD;
            break;

        case CMD_SET:
            cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_SET_CMD;
            break;

        case CMD_SET_BIN:
            cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_SET_BIN_CMD;
            break;

        case CMD_SUB:

            // Check that no publications currently have an error
            // New subscriptions must wait for errors to be returned before they can start

            for(i = 0 ; i < FGC_MAX_SUBS ; i++)
            {
                if(fgcsm.channel[channel_num].sub_data[i].error)
                {
                    return 0;
                }
            }

            cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_SUB_CMD;
            break;

        case CMD_UNSUB:
            cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_UNSUB_CMD;
            break;
    }

    // Add sub-device prefix if necessary

    if(cmd_msg->header.flags & FGC_FIELDBUS_FLAGS_FIRST_PKT && cmd->sub_device)
    {
        cmd_msg_len += sprintf(cmd_msg->cmd_pkt, "%u:", cmd->sub_device);
    }

    // Fill packet buffer

    // Calculate lengths of remaining parts of command string and value

    cmd_str_remaining = cmd->remaining - 1 - cmd->value_length;
    if(cmd_str_remaining < 0)
    {
        cmd_str_remaining = 0;
    }
    val_remaining = cmd->remaining - cmd_str_remaining;

     // Add command string to packet

    if(cmd_str_remaining)
    {
        strncpy(&cmd_msg->cmd_pkt[cmd_msg_len],
                &cmd->command_string[cmd->command_length - cmd_str_remaining],
                sizeof(cmd_msg->cmd_pkt) - cmd_msg_len);

        // Check whether remaining command data is longer than a packet

        if((cmd_str_remaining + cmd_msg_len) > sizeof(cmd_msg->cmd_pkt))
        {
            cmd->remaining -= sizeof(cmd_msg->cmd_pkt) - cmd_msg_len;
            cmd_msg_len     = sizeof(cmd_msg->cmd_pkt);
        }
        else // All command data fits within a packet
        {
            cmd->remaining -= cmd_str_remaining;
            cmd_msg_len    += cmd_str_remaining;
        }
    }

    // Add a space character between command and value if necessary

    if(cmd_msg_len   < sizeof(cmd_msg->cmd_pkt) &&
       val_remaining > static_cast<int32_t>(cmd->value_length))
    {
        cmd_msg->cmd_pkt[cmd_msg_len++] = ' ';
        cmd->remaining--;
        val_remaining--;
    }

    // Add value to packet

    if(cmd_msg_len < sizeof(cmd_msg->cmd_pkt) && val_remaining)
    {
        strncpy(&cmd_msg->cmd_pkt[cmd_msg_len],
                &cmd->value[cmd->value_length - val_remaining],
                sizeof(cmd_msg->cmd_pkt) - cmd_msg_len);

        // Check whether remaining value data is longer than a packet

        if((val_remaining + cmd_msg_len) > sizeof(cmd_msg->cmd_pkt))
        {
            cmd->remaining -= sizeof(cmd_msg->cmd_pkt) - cmd_msg_len;
            cmd_msg_len     = sizeof(cmd_msg->cmd_pkt);
        }
        else // All command data fits within a packet
        {
            cmd->remaining  = 0;
            cmd_msg_len    += val_remaining;
        }
    }

    if(!cmd->remaining)
    {
        cmd_msg->header.flags |= FGC_FIELDBUS_FLAGS_LAST_PKT;
    }

    // Add header size to cmd_msg_len

    cmd_msg_len += sizeof(cmd_msg->header);

    return cmd_msg_len;
}



void fgcsmProcessResponse(uint32_t                  channel_num,
                          struct fgc_fieldbus_rsp   *rsp_msg,
                          uint32_t                  msg_data_len)
{
    struct FGCSM_channel    *channel        = &fgcsm.channel[channel_num];
    struct Cmd              *command        = channel->current_command;
    uint32_t                expected_seq_num;
    uint32_t                i;
    uint32_t                rsp_msg_index   = rsp_msg->header.n_term_chs;
    uint32_t                sequence_number = rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_SEQ_MASK;
    uint32_t                size;

    // Check whether a response is expected

    if(channel->cmd_state != FGC_FGC_STATE_WAITEXEC)
    {
        logPrintf(channel_num, "FGCSM Protocol error - response received while not in the waitexec state\n");
        return;
    }

    // Check that the message is consistent with the response state

    if(channel->rsp_state == FGC_RSP_START)
    {
        command->index          = 0;
        command->value_length   = 0;

        // Check that the packet received was the first packet

        if(!(rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_FIRST_PKT))
        {
            logPrintf(channel_num,
                      "FGCSM Protocol error - first received packet did not have FGC_FIELDBUS_FLAGS_FIRST_PKT set\n");

            fgcsm.channel[channel_num].count.proto_error++;
            channel->current_command->error = FGC_PROTO_ERROR;
            channel->rsp_state              = FGC_RSP_ERROR;

            return;
        }

        // Handle the different types of command

        channel->new_sub_id = FGC_MAX_SUBS;

        switch(command->type)
        {
            case CMD_SUB:   // Subscription
            case CMD_UNSUB: // Unsubscription
                channel->rsp_state = FGC_RSP_SUB_ID;
                break;

            default:        // Normal command response
                channel->rsp_state = FGC_RSP_TIME;
                break;
        }

        // Set first sequence number

        channel->prev_rsp_seq = sequence_number;
    }
    else // A response is in the middle of being received
    {
        // Check whether sequence number matches expected

        expected_seq_num = (channel->prev_rsp_seq + 1) % (FGC_FIELDBUS_FLAGS_SEQ_MASK + 1);

        if(sequence_number != expected_seq_num) // Sequence number is not correct
        {
            logPrintf(channel_num,
                      "FGCSM Protocol error (incorrect response sequence number (expected %u, received %u))\n",
                      expected_seq_num, sequence_number);

            fgcsm.channel[channel_num].count.proto_error++;
            channel->current_command->error = FGC_PROTO_ERROR;
            channel->rsp_state              = FGC_RSP_ERROR;

            return;
        }

        // Store sequence number ready for next packet

        channel->prev_rsp_seq = sequence_number;
    }

    // Read ID for subscription responses

    if(msg_data_len && channel->rsp_state == FGC_RSP_SUB_ID)
    {
        channel->new_sub_id = rsp_msg->rsp_pkt[rsp_msg_index++];
        msg_data_len--;

        // Check whether the subscription ID is outside of the allowed range

        if(channel->new_sub_id >= FGC_MAX_SUBS)
        {
            logPrintf(channel_num,
                      "FGCSM Protocol error (subscription ID %hhu out of range)\n",
                      channel->new_sub_id);

            fgcsm.channel[channel_num].count.proto_error++;
            channel->current_command->error = FGC_PROTO_ERROR;
            channel->rsp_state              = FGC_RSP_ERROR;

            return;
        }

        // Handle the subscription or unsubscription

        command->value_length   = 0;
        channel->rsp_state      = FGC_RSP_END;

        switch(command->type)
        {
            case CMD_SUB: // Subscription

                logPrintf(channel_num, "FGCSM New subscription ID %hhu (%s)\n",
                          channel->new_sub_id, command->command_string);

                // Clear subscription error

                channel->sub_data[channel->new_sub_id].error = 0;

                // Store the property in the FGC subscription table

                for(i = 0 ; i <= FGC_MAX_PROP_LEN ; i++)
                {
                    switch(command->command_string[i])
                    {
                        case '\0':
                        case ' ':
                        case '<':
                        case '(':
                        case '[':
                            channel->sub_data[channel->new_sub_id].property[i] = '\0';
                            break;

                        default:
                            channel->sub_data[channel->new_sub_id].property[i] = command->command_string[i];
                            break;
                    }

                    if(channel->sub_data[channel->new_sub_id].property[i] == '\0')
                    {
                        break;
                    }
                }
                channel->sub_data[channel->new_sub_id].property[sizeof(channel->sub_data[channel->new_sub_id].property) - 1] = '\0';

                break;

            case CMD_UNSUB: // Unsubscription

                // Clear subscription command

                logPrintf(channel_num, "FGCSM Cancel subscription ID %hhu (%s)\n",
                          channel->new_sub_id, command->command_string);

                channel->sub_data[channel->new_sub_id].property[0] = '\0';

                break;

            default: // Unexpected command type
                logPrintf(channel_num, "FGCSM Protocol error (unexpected command type)\n");
                fgcsm.channel[channel_num].count.proto_error++;

                channel->current_command->error = FGC_PROTO_ERROR;
                channel->rsp_state              = FGC_RSP_ERROR;

                return;
        }
    }

    // Read timestamp

    if(msg_data_len && channel->rsp_state == FGC_RSP_TIME)
    {
        // Check whether message contains all of the remaining timestamp

        size = sizeof(channel->current_cmd_acq_time) - command->index;

        if(msg_data_len > size)
        {
            // Message contains all of the remaining timestamp

            memcpy(&((uint8_t *)&channel->current_cmd_acq_time)[command->index],
                   &rsp_msg->rsp_pkt[rsp_msg_index], size);

            msg_data_len    -= size;
            rsp_msg_index   += size;

            // Byte swap timestamp

            command->acq_timestamp.tv_sec   = ntohl(channel->current_cmd_acq_time.tv_sec);
            command->acq_timestamp.tv_usec  = ntohl(channel->current_cmd_acq_time.tv_usec);

            channel->rsp_state = FGC_RSP_VALUE;
        }
        else // Message contains only part of the remaining timestamp
        {
            memcpy(&((uint8_t *)&channel->current_cmd_acq_time)[command->index],
                   &rsp_msg->rsp_pkt[rsp_msg_index], msg_data_len);

            command->index += msg_data_len;
            msg_data_len    = 0;
        }
    }

    // Read value

    if(msg_data_len && channel->rsp_state == FGC_RSP_VALUE)
    {
        // Check that value_length + total response length will not exceed maximum

        if(msg_data_len + command->value_length > CMD_MAX_VAL_LENGTH)
        {
            channel->current_command->error = FGC_RSP_BUF_FULL;
            channel->rsp_state              = FGC_RSP_ERROR;

            logPrintf(channel_num, "FGCSM Command response buffer full\n");
        }
        else // Response data will fit within response buffer
        {
            // Append packet to response

            memcpy(&command->value[command->value_length],
                   &rsp_msg->rsp_pkt[rsp_msg_index],
                   msg_data_len);

            command->value_length += msg_data_len;

            // Check whether this is the end of the response

            if(rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_LAST_PKT)
            {
                channel->rsp_state = FGC_RSP_END;

                // Truncate response to remove semi-colon

                command->value[--command->value_length] = '\0';

                // [Hack] [APS-9259] : Special treatment for INTERLOCK.PPM.RESULT
                auto cmd_upper_case = std::string(command->command_string);
                std::transform(cmd_upper_case.begin(), cmd_upper_case.end(), cmd_upper_case.begin(), ::toupper);

                bool cmd_is_interlock = !(strncmp(cmd_upper_case.c_str(), "INTERLOCK.PPM.RESULT", strlen("INTERLOCK.PPM.RESULT")));

                // If the user received from the FGC is 0, then it means it's not a PPM property and we don't want
                // to send cycle timestamp in the CMW notification
                if (command->user == 0 && (!cmd_is_interlock))
                {
                    command->zero_cycle_timestamp = true;
                }
            }
        }
    }
}



void fgcsmProcessPublishMsg(uint32_t                channel_num,
                            struct fgc_fieldbus_rsp *rsp_msg,
                            uint32_t                msg_data_len)
{
    struct FGCSM_channel    *channel        = &fgcsm.channel[channel_num];
    struct Cmd              *command        = channel->current_pub_data;
    uint32_t                expected_seq_num;
    uint32_t                rsp_msg_index   = rsp_msg->header.n_term_chs;
    uint32_t                sequence_number = rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_SEQ_MASK;
    uint32_t                size;
    uint8_t                 sub_flags;

    // Calculate expected sequence number

    expected_seq_num = (channel->prev_pub_seq + 1) % (FGC_FIELDBUS_FLAGS_SEQ_MASK + 1);

    // Store sequence number ready for next packet

    channel->prev_pub_seq = sequence_number;

    // Check whether sequence number matches expected

    if(sequence_number != expected_seq_num)
    {
        logPrintf(channel_num,
                  "FGCSM Protocol error (incorrect published data sequence number (expected %u, received %u))\n",
                  expected_seq_num, sequence_number);
        fgcsm.channel[channel_num].count.proto_error++;

        // Return IO error to subscription

        if(command)
        {
            command->error = FGC_IO_ERROR;
            channel->current_pub_data = NULL;
            queuePush(command->response_queue, command);
            command = NULL;
        }

        channel->pub_state = FGC_PUB_START;
        return;
    }

    // Handle start of new published data

    if(msg_data_len && channel->pub_state == FGC_PUB_START)
    {
        // Ignore any packets except a first packet

        if(!(rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_FIRST_PKT))
        {
            return;
        }
        channel->pub_state = FGC_PUB_SUB_ID;
    }

    // Read pub_sub_id

    if(msg_data_len && channel->pub_state == FGC_PUB_SUB_ID)
    {
        channel->pub_sub_id = rsp_msg->rsp_pkt[rsp_msg_index++];
        msg_data_len--;

        // If there is some current command for publication data, then return it before getting a new one

        if(command)
        {
            logPrintf(channel_num,"FGCSM Returning zombie publication data command to the pool\n");
            command->error = FGC_IO_ERROR;
            channel->current_pub_data = NULL;
            queuePush(command->response_queue, command);
            command = NULL;
        }

        // Get a command structure from the free queue

        if(!(command = channel->current_pub_data = (struct Cmd *)queuePop(&cmdqmgr.free_cmd_q)))
        {
            logPrintf(channel_num, "FGCSM Failed to get command structure for publication\n");
            channel->pub_state = FGC_PUB_START;
            return;
        }

        // Initialise the command

        command->device = &fgcd.device[channel_num];
        command->type   = CMD_GET_SUB;

        // The remaining count for the command is overloaded here to indicate that the command
        // was not counted by cmwpub.fgcd_cmd_count

        // Published data responses from FGCD devices will have 1 in this field

        command->remaining = 0;

        // Prepare to receive subscription flags

        channel->pub_state = FGC_PUB_SUB_FLAGS;
    }

    // Read subscription flags

    if(msg_data_len && channel->pub_state == FGC_PUB_SUB_FLAGS)
    {

        sub_flags = rsp_msg->rsp_pkt[rsp_msg_index++];
        msg_data_len--;

        switch(sub_flags & FGC_FIELDBUS_PUB_TRIG_MASK)
        {
            case 0:
                break;

            case FGC_FIELDBUS_PUB_TRIG_NEW_SUB:
                command->cmw_update_flags = CMWPUB_UPDATE_FLAGS_FIRST;
                break;

            case FGC_FIELDBUS_PUB_TRIG_SET:
                command->cmw_update_flags = CMWPUB_UPDATE_FLAGS_IMMEDIATE;
                break;

            default:
                logPrintf(channel_num, "FGCSM Protocol error (invalid subscription flags 0x%02X)\n", sub_flags);
                fgcsm.channel[channel_num].count.proto_error++;

                // Return command structure

                channel->current_pub_data = NULL;
                queuePush(&cmdqmgr.free_cmd_q, command);
                command = NULL;

                channel->pub_state = FGC_PUB_START;
                return;
        }

        // Check whether the subscription ID is invalid

        if(channel->pub_sub_id >= FGC_MAX_SUBS)
        {
            logPrintf(channel_num,
                      "FGCSM Protocol error (subscription ID %hhu out of range)\n",
                      channel->pub_sub_id);
            fgcsm.channel[channel_num].count.proto_error++;

            // Return command structure

            channel->current_pub_data = NULL;
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;

            channel->pub_state = FGC_PUB_START;
            return;
        }

        // Check that the subscription ID refers to an active subscription

        if(!channel->sub_data[channel->pub_sub_id].property[0]) // Subscription is not active
        {
            logPrintf(channel_num,
                      "FGCSM Protocol error (published data received for inactive subscription ID %hhu)\n",
                      channel->pub_sub_id);
            fgcsm.channel[channel_num].count.proto_error++;

            // Return command structure

            channel->current_pub_data = NULL;
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;

            channel->pub_state = FGC_PUB_START;
            return;
        }

        // Retrieve the sub-device selector

        if (sub_flags & FGC_FIELDBUS_PUB_MULT_PPM)
        {
            command->sub_device = (sub_flags & FGC_FIELDBUS_PUB_SUB_DEV_SEL_MASK) >> FGC_FIELDBUS_PUB_SUB_DEV_SEL_SHIFT;
        }
        else
        {
            command->sub_device = CMWPUB_NOTIFY_ALL_SUB_DEVICES;
        }

        // Check whether the subscription is active

        if(channel->sub_data[channel->pub_sub_id].property[0])
        {
            // Copy the subscription property string to the command

            command->command_length = sprintf(command->command_string,
                                              channel->sub_data[channel->pub_sub_id].property);

            // Prepare to receive the published data value

            command->value_length   = 0;
            command->response_queue = &cmwpub.response_queue;
            channel->pub_state      = FGC_PUB_USER;
        }
        else // Subscription is inactive
        {
            // Return the command structure

            channel->current_pub_data = NULL;
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;

            channel->pub_state = FGC_PUB_START;

            return;
        }
    }

    // Read user

    if(msg_data_len && channel->pub_state == FGC_PUB_USER)
    {
        command->user = rsp_msg->rsp_pkt[rsp_msg_index++];
        msg_data_len--;

        // [Hack] [APS-9259] : Special treatment for INTERLOCK.PPM.RESULT
        auto cmd_upper_case = std::string(command->command_string);
        std::transform(cmd_upper_case.begin(), cmd_upper_case.end(), cmd_upper_case.begin(), ::toupper);

        bool cmd_is_interlock = !(strncmp(cmd_upper_case.c_str(), "INTERLOCK.PPM.RESULT", strlen("INTERLOCK.PPM.RESULT")));

        // If the user received from the FGC is 0, then it means it's not a PPM property and we don't want
        // to send cycle timestamp in the CMW notification
        if (command->user == 0 && (!cmd_is_interlock))
        {
            command->zero_cycle_timestamp = true;
        }

        // Append user to command

        command->command_length += sprintf(&command->command_string[command->command_length],
                                           "(%u) type lbl", command->user);

        // Check whether message is requesting a get to the FGC

        if((rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK) == FGC_FIELDBUS_FLAGS_PUB_GET_PKT)
        {
            // Queue command for processing by device

            command->client = CMD_FGCD_CLIENT;
            snprintf(command->device_name, FGC_MAX_DEV_LEN + 1, "%u", channel_num);
            channel->current_pub_data = NULL;
            cmdqmgrQueue(command);
            command = NULL;

            channel->pub_state = FGC_PUB_START;
            return;
        }
        else // Message includes value
        {
            command->index      = 0;
            channel->pub_state  = FGC_PUB_TIME;
        }
    }

    // Read timestamp

    if(msg_data_len && channel->pub_state == FGC_PUB_TIME)
    {
        // Check whether message contains all of the remaining timestamp

        size = sizeof(channel->current_pub_acq_time) - command->index;

        if(msg_data_len > size)
        {
            // Message contains all of the remaining timestamp

            memcpy(&((uint8_t *)&channel->current_pub_acq_time)[command->index],
                   &rsp_msg->rsp_pkt[rsp_msg_index], size);

            msg_data_len    -= size;
            rsp_msg_index   += size;

            // Byte swap timestamp

            command->acq_timestamp.tv_sec   = ntohl(channel->current_pub_acq_time.tv_sec);
            command->acq_timestamp.tv_usec  = ntohl(channel->current_pub_acq_time.tv_usec);

            channel->pub_state = FGC_PUB_VALUE;
        }
        else // Message contains only part of the remaining timestamp
        {
            memcpy(&((uint8_t *)&channel->current_pub_acq_time)[command->index],
                   &rsp_msg->rsp_pkt[rsp_msg_index], msg_data_len);

            command->index += msg_data_len;
            msg_data_len    = 0;
        }
    }

    // Read value

    if(msg_data_len && channel->pub_state == FGC_PUB_VALUE)
    {
        // Check that value_length + total response length will not exceed maximum

        if(msg_data_len + command->value_length > CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            channel->current_pub_data = NULL;
            queuePush(command->response_queue, command);
            command = NULL;

            logPrintf(channel_num, "FGCSM Command response buffer full\n");
            channel->pub_state = FGC_PUB_START;
            return;
        }
        else // Response data will fit within response buffer
        {
            // Append packet to response

            memcpy(&command->value[command->value_length],
                   &rsp_msg->rsp_pkt[rsp_msg_index],
                   msg_data_len);

            command->value_length += msg_data_len;
            msg_data_len = 0;

            // Check whether this is the end of the response

            if(rsp_msg->header.flags & FGC_FIELDBUS_FLAGS_LAST_PKT)
            {
                // Truncate response to remove semi-colon

                command->value[--command->value_length] = '\0';

                // Send command to response queue

                channel->current_pub_data = NULL;
                queuePush(command->response_queue, command);
                command = NULL;

                channel->pub_state = FGC_PUB_START;
                return;
            }
        }
    }
}



void fgcsmCmdQueued(uint32_t channel_num)
{
    // Change state to queued
    // Check that current state is idle or wait ack in case channel has gone offline

    switch(fgcsm.channel[channel_num].cmd_state)
    {
        case FGC_FGC_STATE_IDLE:
        case FGC_FGC_STATE_WAITACK:
            fgcsm.channel[channel_num].cmd_state = FGC_FGC_STATE_QUEUED;
            break;

        default:
            break;
    }
}



void fgcsmCmdSent(uint32_t channel_num)
{
    // Change state to wait_ack
    // Check that current state is queued in case channel has gone offline

    if(fgcsm.channel[channel_num].cmd_state == FGC_FGC_STATE_QUEUED)
    {
        fgcsm.channel[channel_num].cmd_state = FGC_FGC_STATE_WAITACK;
    }
}



void fgcsmCheckStatus(void)
{
    struct FGCSM_channel                    *channel;
    struct Cmd                              *command;
    uint32_t                                i;
    uint32_t                                sub_index;
    union fgc_stat_var                      status;

    // Check status of FGC devices

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        channel = &fgcsm.channel[i];

        // Copy the data from the fieldbus to avoid strict aliasing warnings

        memcpy(&status,&fieldbus.channel[i].status.fgc_stat,sizeof(struct fgc_stat));
        status.fieldbus_stat.ack       = 0;
        status.fieldbus_stat.cmd_stat  = 0;
        pub.published_data.status[i] = status.fgc_stat;

        // Check whether the channel's status been missed

        if((!channel->status_received) &&
            (fgcsm.channel[i].cmd_state != FGC_FGC_STATE_OFFLINE)) // Status variable missed
        {
            logPrintf(i, "FGC Status missed\n");

            channel->count.status_miss++;
            channel->count.seq_status_miss++;
            channel->count.seq_toggle_bit_miss++;

            if(channel->count.seq_status_miss == 2)
            {
                // Two consecutive missed status variables, therefore channel is offline

                channel->count.offline_transition++;
                fgcsm.count.offline_transition++;

                channel->cmd_state       = FGC_FGC_STATE_OFFLINE;
                fgcddev.status.fgc_mask &= ~(1 << i);

                // Set device as offline

                fgcd.device[i].ready  = 0;
                fgcd.device[i].online = 0;

                logPrintf(i, "FGC Offline\n");
            }
        }

        // Check whether a command packet acknowledgement has been missed for two sequential cycles

        if((channel->cmd_state == FGC_FGC_STATE_WAITACK) &&
           (channel->count.seq_toggle_bit_miss == 2)) // Acknowledgement missed
        {
            // 2 toggle bits missed, return protocol error

            logPrintf(i, "FGC Protocol error (2 toggles missed)\n");

            channel->count.proto_error++;
            channel->current_command->error = FGC_PROTO_ERROR;
            queuePush(channel->current_command->response_queue, channel->current_command);
            channel->current_command        = NULL;
            channel->cmd_state              = FGC_FGC_STATE_IDLE;
        }

        // Reset status received flag for next cycle

        channel->status_received = 0;

        if(channel->cmd_state == FGC_FGC_STATE_OFFLINE)
        {
            // Return error to all queued commands including current command

            if(channel->current_command != NULL)
            {
                channel->current_command->error = FGC_DEV_NOT_READY;
                queuePush(channel->current_command->response_queue, channel->current_command);
                channel->current_command        = NULL;
            }

            while((command = (struct Cmd *)queuePop(&channel->command_queue)))
            {
                command->error = FGC_DEV_NOT_READY;
                queuePush(command->response_queue, command);
                command = NULL;
            }

            // Clear subscription data and return error to all subscriptions

            for(sub_index = 0 ; sub_index < FGC_MAX_SUBS ; sub_index++)
            {
                if(channel->sub_data[sub_index].property[0] != '\0')
                {
                    channel->sub_data[sub_index].error = FGC_DEV_NOT_READY;
                }
            }
        }

        // Check for subscriptions with errors

        for(sub_index = 0 ; sub_index < FGC_MAX_SUBS ; sub_index++)
        {
            if(channel->sub_data[sub_index].error)
            {
                // Get a command structure

                if((command = (struct Cmd *)queuePop(&cmdqmgr.free_cmd_q)))
                {
                    command->device         = &fgcd.device[i];
                    command->type           = CMD_GET_SUB;
                    command->value_length   = 0;
                    command->error          = channel->sub_data[sub_index].error;

                    strcpy(command->command_string, channel->sub_data[sub_index].property);

                    // The remaining count for the command is overloaded here to indicate that the command
                    // was not counted by cmwpub.fgcd_cmd_count

                    // Published data responses from FGCD devices will have 1 in this field

                    command->remaining = 0;

                    // Push command onto publishing queue

                    queuePush(&cmwpub.response_queue, command);
                    command = NULL;

                    // Clear subscription data slot

                    channel->sub_data[sub_index].property[0] = '\0';
                    channel->sub_data[sub_index].error       = 0;
                }
            }
        }

        // Perform post-mortem acquisition

        fgcsmPMAcq(i);
    }
}



// Static functions



/*
 * Trigger post-mortem acquisitions
 */

static void fgcsmPMTrigger(uint32_t channel_num)
{
    struct FGCD_device  *device = &fgcd.device[channel_num];
    static int32_t      not_first_self_pm_flag[FGCD_MAX_DEVS];
    struct PM_ext_acq   *pm_ext;
    struct PM_self_acq  *pm_self;
    union fgc_stat_var  *status = &fieldbus.channel[channel_num].status;
    struct timeval      time;

    // Do nothing if post-mortem thread is not running

    if(pm_globals.thread == 0) return;

    // Handle externally-triggered post-mortems

    pm_ext = &fgcd.device[channel_num].pm_ext;

    // Check whether an external post-mortem trigger is requested

    if(!pm_ext->acq_info.wait_cycles    &&
       !pm_ext->acq_info.frozen         &&
       status->fieldbus_stat.ack & FGC_EXT_PM_REQ
      ) // External post-mortem requested
    {
        // Post-mortem externally-triggered

        logPrintf(channel_num, "PM external trigger\n");

        // Store event timestamp

        if(fieldbus.global_pm_time_sec) // Event was in response to a timing event
        {
            pm_ext->acq_info.time_sec   = fieldbus.global_pm_time_sec;
            pm_ext->acq_info.time_nsec  = fieldbus.global_pm_time_nsec;
        }
        else // Event was triggered by FGC
        {
            timingGetUserTime(0, &time);
            pm_ext->acq_info.time_sec   = time.tv_sec;
            pm_ext->acq_info.time_nsec  = time.tv_usec * 1000;
        }

        pm_ext->acq_info.wait_cycles    = PM_WAIT_CYCLES_EXT_TRIG;

        // Copy trigger status

        pm_ext->buffer.trigger = status->fgc_stat;
    }

    // Handle self-triggered post-mortems

    pm_self = &device->pm_self;

    // Check whether a self-triggered post-mortem acquisition is in progress for the device

    if(pm_self->acq_info.wait_cycles) // Post-mortem acquisition is in progress
    {
        // Check whether low current flag is being waited for and has been reached
        // and that more than the minimum number of cycles have been taken

        if(pm_self->wait_low_current                                                        &&
           ntohs(status->fgc_stat.class_data.c51.st_unlatched) & FGC_UNL_LOW_CURRENT        &&
           PM_WAIT_CYCLES_SELF_TRIG_51 - pm_self->acq_info.wait_cycles > PM_MIN_CYCLES_51
          )
        {
            // Freeze post-mortem buffer

            pm_self->acq_info.frozen        = 1;
            pm_self->acq_info.wait_cycles   = 0;
            pm_self->wait_low_current       = 0;

            // Post semaphore to post-mortem thread if not waiting for FGC logs

            if(!pm_self->wait_fgc_logs && !pm_self->cmds_queued)
            {
                sem_post(&pm_globals.sem);
            }
        }
    }
    else if(!pm_self->acq_info.frozen) // Self-triggered post-mortem is not in progress
    {
        // Check whether a self-triggered post-mortem is requested

        // Note that there are two conditions for triggering, depending whether it is the first trigger.
        // This is to ensure that data is recovered in the case that communication is broken between the gateway
        // and FGC during a post-mortem.

        if(device->ready &&
           ((not_first_self_pm_flag[channel_num]  && status->fieldbus_stat.ack & FGC_SELF_PM_REQ) ||
            (!not_first_self_pm_flag[channel_num] && ntohs(status->fgc_stat.class_data.common.st_unlatched) & FGC_POST_MORTEM))
          )
        {
            // Post-mortem self-triggered

            logPrintf(channel_num, "PM Self-trigger\n");
            not_first_self_pm_flag[channel_num] = 1;

            timingGetUserTime(0, &time);
            pm_self->acq_info.time_sec  = time.tv_sec;
            pm_self->acq_info.time_nsec = time.tv_usec * 1000;
            pm_self->fgc_logs_retries   = PM_FGC_LOGS_RETRIES;

            switch(fgcd.device[channel_num].fgc_class)
            {
                case 51:
                    pm_self->wait_fgc_logs          = PM_WAIT_CYCLES_FGC_LOGS_51;
                    pm_self->wait_low_current       = 1;
                    pm_self->acq_info.wait_cycles   = PM_WAIT_CYCLES_SELF_TRIG_51;
                    break;

                default:
                    pm_self->wait_fgc_logs          = PM_WAIT_CYCLES_FGC_LOGS_STD;
                    pm_self->wait_low_current       = 0;
                    pm_self->acq_info.wait_cycles   = PM_MIN_CYCLES_STD;
                    break;
            }

            // Copy trigger status

            pm_self->buffer.trigger = status->fgc_stat;
        }
    }
}



/*
 * Handle post-mortem acquisitions
 */

static void fgcsmPMAcq(uint32_t channel_num)
{
    struct Cmd          *command = NULL;
    struct FGCD_device  *device  = &fgcd.device[channel_num];

    // Do nothing if post-mortem thread is not running or device class is unknown

    if(pm_globals.thread == 0 || device->fgc_class == FGC_CLASS_UNKNOWN)
    {
        return;
    }

    // Check whether an externally-triggered post-mortem acquisition is in progress for the device

    if(device->pm_ext.acq_info.wait_cycles) // Post-mortem acquisition is in progress
    {
        // Check whether wait cycles have run out

        if(!--device->pm_ext.acq_info.wait_cycles) // Wait cycles are zero
        {
            // Freeze post-mortem buffer

            device->pm_ext.acq_info.frozen      = 1;
            device->pm_ext.acq_info.wait_cycles = 0;

            // Post semaphore to post-mortem thread

            sem_post(&pm_globals.sem);
        }
    }

    // Check whether self-triggered post-mortem is waiting for FGC logs

    if(device->pm_self.wait_fgc_logs)
    {
        device->pm_self.wait_fgc_logs--;

        // Check whether FGC logs are ready

        if(device->ready && !(fieldbus.channel[channel_num].status.fieldbus_stat.ack & FGC_SELF_PM_REQ))
        {
            // Get a new command structure

            if((command = (struct Cmd *)queuePop(&cmdqmgr.free_cmd_q)))
            {
                device->pm_self.wait_fgc_logs = 0;

                // Populate command structure

                command->client         = CMD_FGCD_CLIENT;
                command->command_length = snprintf(command->command_string, CMD_MAX_LENGTH + 1, "LOG.PM.BUF BIN");
                snprintf(command->device_name, FGC_MAX_DEV_LEN + 1, "%u", device->id);
                command->response_queue = &device->pm_self.cmd_response_queue;
                command->type           = CMD_GET;

                // Queue command

                logPrintf(device->id, "PM Sending command to extract logs\n");
                device->pm_self.cmds_queued++;
                cmdqmgrQueue(command);
                command = NULL;
            }
        }
        else if(!device->pm_self.wait_fgc_logs) // Time-out waiting for FGC logs to be ready
        {
            logPrintf(device->id, "PM Time-out waiting for POST_MORTEM\n");

            // Post semaphore to post-mortem thread if not waiting for acquisition to finish
            // (the processing in pm.cpp will run despite this time-out)

            if(!device->pm_self.acq_info.wait_cycles)
            {
                sem_post(&pm_globals.sem);
            }
        }
    }
    else if(device->pm_self.cmds_queued) // Commands are queued to extract post-mortem buffers from the device
    {
        // Attempt to obtain response

        if((command = (struct Cmd *)queuePop(&device->pm_self.cmd_response_queue)) != NULL)
        {
            device->pm_self.cmds_queued--;

            // Handle response error conditions

            switch(command->error)
            {
                case FGC_DEV_NOT_READY: // Device offline

                    // Wait for FGC to be ready again

                    if(device->pm_self.fgc_logs_retries)
                    {
                        device->pm_self.fgc_logs_retries--;

                        logPrintf(device->id, "PM Error [%d %s] retrieving FGC buffer, re-starting wait (%u retries remaining)\n",
                                  command->error, fgc_errmsg[command->error],
                                  device->pm_self.fgc_logs_retries);
                        device->pm_self.wait_fgc_logs = device->fgc_class == 51 ? PM_WAIT_CYCLES_FGC_LOGS_51 : PM_WAIT_CYCLES_FGC_LOGS_STD;

                        // Return command structure

                        queuePush(&cmdqmgr.free_cmd_q, command);
                        command = NULL;
                    }
                    else // No remaining retries
                    {
                        logPrintf(device->id, "PM Error [%d %s] retrieving FGC buffer, all retries used, abandoning\n",
                                  command->error, fgc_errmsg[command->error]);

                        // Re-use response command to reset FGC logs then return the structure to the free command queue

                        command->type           = CMD_SET;
                        command->command_length = snprintf(command->command_string, CMD_MAX_LENGTH + 1, "LOG RESET");
                        command->response_queue = &cmdqmgr.free_cmd_q;
                        cmdqmgrQueue(command);
                        command = NULL;

                        // Post semaphore to post-mortem thread if not waiting for acquisition to finish

                        if(!device->pm_self.acq_info.wait_cycles)
                        {
                            sem_post(&pm_globals.sem);
                        }
                    }

                    break;

                case FGC_PROTO_ERROR: // Communication error

                    // Re-submit command

                    if(device->pm_self.fgc_logs_retries)
                    {
                        device->pm_self.fgc_logs_retries--;
                        logPrintf(device->id, "PM Error [%d %s] retrieving FGC buffer, retrying (%u retries remaining)\n",
                                  command->error, fgc_errmsg[command->error],
                                  device->pm_self.fgc_logs_retries);
                        device->pm_self.cmds_queued++;
                        cmdqmgrQueue(command);
                        command = NULL;
                    }
                    else // No remaining retries
                    {
                        logPrintf(device->id, "PM Error [%d %s] retrieving FGC buffer, no retries remaining, abandoning\n",
                                  command->error, fgc_errmsg[command->error]);

                        // Re-use response command to reset FGC logs then return the structure to the free command queue

                        command->type           = CMD_SET;
                        command->command_length = snprintf(command->command_string, CMD_MAX_LENGTH + 1, "LOG RESET");
                        command->response_queue = &cmdqmgr.free_cmd_q;
                        cmdqmgrQueue(command);
                        command = NULL;

                        // Post semaphore to post-mortem thread if not waiting for acquisition to finish

                        if(!device->pm_self.acq_info.wait_cycles)
                        {
                            sem_post(&pm_globals.sem);
                        }
                    }
                    break;

                // Command was executed okay

                case FGC_OK_NO_RSP:
                case FGC_OK_RSP:

                    // Set post-mortem response

                    device->pm_self.response = command;

                    // Post semaphore to post-mortem thread if not waiting for acquisition to finish

                    if(!device->pm_self.acq_info.wait_cycles)
                    {
                        sem_post(&pm_globals.sem);
                    }
                    break;

                default: // Another error occurred

                    logPrintf(device->id, "PM Error [%d %s] retrieving FGC buffer, abandoning\n",
                              command->error, fgc_errmsg[command->error]);

                    // Re-use response command to reset FGC logs then return the structure to the free command queue

                    command->type           = CMD_SET;
                    command->command_length = snprintf(command->command_string, CMD_MAX_LENGTH + 1, "LOG RESET");
                    command->response_queue = &cmdqmgr.free_cmd_q;
                    cmdqmgrQueue(command);
                    command = NULL;

                    // Post semaphore to post-mortem thread if not waiting for acquisition to finish

                    if(!device->pm_self.acq_info.wait_cycles)
                    {
                        sem_post(&pm_globals.sem);
                    }
                    break;
            }
        }
    }

    // Check whether a self-triggered post-mortem acquisition is in progress for the device

    if(device->pm_self.acq_info.wait_cycles) // Post-mortem acquisition is in progress
    {
        // Check whether wait cycles have run out

        if(!--device->pm_self.acq_info.wait_cycles) // Wait cycles are zero
        {
            // Freeze post-mortem buffer

            device->pm_self.acq_info.frozen         = 1;
            device->pm_self.acq_info.wait_cycles    = 0;
            device->pm_self.wait_low_current        = 0;

            // Post semaphore to post-mortem thread if not waiting for FGC logs

            if(!device->pm_self.wait_fgc_logs &&
               !device->pm_self.cmds_queued)
            {
                sem_post(&pm_globals.sem);
            }
        }
    }
}

// EOF
