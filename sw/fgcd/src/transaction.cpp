/*
 *  Filename: transaction.cpp
 *
 *  Purpose:  Transaction handling
 *
 *  Author:   Stephen Page
 */

#include <stdint.h>
#include <sys/time.h>

#include <cmd.h>
#include <cmwpub.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <parser.h>
#include <prop.h>
#include <queue.h>
#include <timing.h>
#include <transaction.h>
#include <fgcd_thread.h>
#include <logging.h>



// Define transaction states if they are not already defined.
// This allows this file to compile for classes that do not yet implement transactions.
// Note that this works on the assumption that equipdev.h includes the class's defconsts.h

#ifndef FGC_TRANSACTION_STATE_NONE
enum dummy_transaction_states
{
    FGC_TRANSACTION_STATE_COMMIT_FAILED,
    FGC_TRANSACTION_STATE_NONE,
    FGC_TRANSACTION_STATE_NOT_TESTED,
    FGC_TRANSACTION_STATE_TEST_FAILED,
    FGC_TRANSACTION_STATE_TEST_OK,
};
#endif



// Structs containing global variables

struct TransactionCommitEvent
{
    uint16_t              transaction_id;
    const struct timeval *time;
};

struct TransactionCommit
{
    pthread_t                     thread;                                // Thread handle
    struct Queue                  event_queue;                           // Queue of transaction commit events to be processed
    struct Queue                  free_event_q;                          // Queue of free transaction commit events
    struct TransactionCommitEvent events[TRANSACTION_COMMIT_NUM_EVENTS]; // Commit event structures
};



// Global variables

struct TransactionCommit transaction_commit;



// Static functions

static void transactionSetState(uint32_t channel, uint32_t user, uint32_t state)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    device.ppm[user].transaction_state = state;
    cmwpubNotify(channel, "TRANSACTION.STATE", user, 0, 0, 1, 0);
}



static void *transactionCommitThread(void *unused)
{
    struct TransactionCommitEvent *event;

    // Process transaction commit events

    while(true)
    {
        if((event = (struct TransactionCommitEvent *)queueBlockPop(&transaction_commit.event_queue)) == NULL)
        {
            logPrintf(0, "TRANSACTION Could not pop transaction event from queue, exiting.\n");
            break;
        }

        // Process the transaction commit event

        for(uint32_t channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
        {
            fgc_errno status;
            if((status = transactionCommit(event->transaction_id, channel, event->time)) != FGC_OK_NO_RSP)
            {
                logPrintf(0, "TRANSACTION commit failed: %s.\n", fgc_errmsg[status]);
            }
        }

        // Return event structure to the free queue

        queuePush(&transaction_commit.free_event_q, event);
    }

    return 0;
}

// End of static functions



int32_t transactionCommitStart(void)
{
    // Initialise free_event_q

    if(queueInit(&transaction_commit.free_event_q, TRANSACTION_COMMIT_NUM_EVENTS))
    {
        // Failed to initialise free queue

        fprintf(stderr, "ERROR: transactionCommitStart:Failed to initialise free queue\n");
        return 1;
    }

    for(uint32_t i = 0 ; i < TRANSACTION_COMMIT_NUM_EVENTS ; i++)
    {
        queuePush(&transaction_commit.free_event_q, &transaction_commit.events[i]);
    }

    // Initialise transaction event processing queue

    if(queueInit(&transaction_commit.event_queue, TRANSACTION_COMMIT_NUM_EVENTS))
    {
        fprintf(stderr, "ERROR: transactionCommitStart:Failed to initialise command queue\n");
        return 1;
    }

    // Start transaction commit thread

    if(fgcd_thread_create(&transaction_commit.thread,
                          transactionCommitThread,
                          NULL,
                          TRANSACTION_COMMIT_THREAD_POLICY,
                          TRANSACTION_COMMIT_THREAD_PRIORITY,
                          NO_AFFINITY) != 0)
    {
        queueFree(&transaction_commit.free_event_q);
        queueFree(&transaction_commit.event_queue);
        return 1;
    }

    return 0;
}



uint32_t transactionAccessCheck(struct Cmd *command, bool prop_transactional, uint32_t user_transaction_id)
{
    switch(command->type)
    {
        // Get command

        case CMD_GET:

            if(prop_transactional && command->transaction_id != 0 && command->transaction_id != user_transaction_id)
            {
                if(user_transaction_id == 0)
                {
                    command->error = FGC_TRANSACTION_NOT_IN_PROGRESS;
                }
                else
                {
                    command->error = FGC_INCORRECT_TRANSACTION_ID;
                }
                return 1;
            }
            break;

        // Set command

        case CMD_SET:
        case CMD_SET_BIN:

            if(prop_transactional && command->transaction_id != user_transaction_id)
            {
                if(user_transaction_id == 0)
                {
                    command->error = FGC_TRANSACTION_NOT_IN_PROGRESS;
                }
                else if(command->transaction_id == 0)
                {
                    command->error = FGC_TRANSACTION_IN_PROGRESS;
                }
                else
                {
                    command->error = FGC_INCORRECT_TRANSACTION_ID;
                }
                return 1;
            }
            break;

        // Subscribe command

        default:    // SUB, GETSUB or UNSUB
            break;
    }

    // Property access granted

    return 0;
}



void transactionAckCommitFail(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Reset the transaction state if a commit failed

    if(device.ppm[command->user].transaction_state == FGC_TRANSACTION_STATE_COMMIT_FAILED)
    {
        transactionSetState(command->device->id, command->user, FGC_TRANSACTION_STATE_NONE);
    }
}



void transactionCommitEvent(uint16_t transaction_id, const struct timeval *time)
{
    struct TransactionCommitEvent *event;

    // Try to get a free event structure from the free event queue

    if((event = (struct TransactionCommitEvent *)queuePop(&transaction_commit.free_event_q)) == NULL)
    {
        // Not event structures available so ignore the COMMIT event but log a message

        logPrintf(0, "TIMING transactional commit ignored because no free transaction_commit events available.\n");
    }
    else
    {
        // Fill event and push to the transaction commit thread queue

        event->transaction_id = transaction_id;
        event->time = time;

        queuePush(&transaction_commit.event_queue, event);
    }
}



fgc_errno transactionCommit(uint16_t transaction_id, uint32_t channel, const struct timeval *time)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Apply the transactional settings for each of the transaction's users

    bool failed = false;
    bool id_matched = false;
    for(uint32_t user = 0 ; user < NUM_CYC_SELECTORS ; user++)
    {
        if(device.ppm[user].transaction_id == transaction_id)
        {
            id_matched = true;

            if(device.ppm[user].transaction_state == FGC_TRANSACTION_STATE_TEST_OK &&
               equipdevTransactionCommit(channel, user, time) == FGC_OK_NO_RSP)
            {
                transactionSetState(channel, user, FGC_TRANSACTION_STATE_NONE);
            }
            else // The commit wasn't attempted or failed for the user - roll back transactional settings
            {
                transactionSetState(channel, user, FGC_TRANSACTION_STATE_COMMIT_FAILED);
                device.ppm[user].transactional = device.ppm[user].transactional_backup;
                failed = true;
            }

            // Notify transactional properties for publication for this user

            equipdevTransactionNotify(channel, user);

            // End the transaction for this user

            device.ppm[user].transaction_id = 0;
        }
    }

    if(!id_matched)
    {
        return FGC_UNKNOWN_TRANSACTION_ID;
    }

    if(failed)
    {
        return FGC_TRANSACTION_COMMIT_FAILED;
    }

    return FGC_OK_NO_RSP;
}



uint16_t transactionGetIdFromCommand(struct Cmd *command)
{
    // Extract the transaction ID from the command

    int32_t transaction_id = 0;
    parserScanInt(command->parser, command->value,
                  &command->index, command->value_length,
                  const_cast<char*>(""), 1, NULL, 0, 1, 1, 1, UINT16_MAX,
                  &transaction_id);

    if(command->error)
    {
        command->error = FGC_INVALID_TRANSACTION_ID;
    }

    return (uint16_t)transaction_id;
}



fgc_errno transactionRollback(uint16_t transaction_id, uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check whether each user has a transaction open with the transaction ID

    fgc_errno error = FGC_UNKNOWN_TRANSACTION_ID;
    for(uint32_t user = 0 ; user < NUM_CYC_SELECTORS ; user++)
    {
        if(device.ppm[user].transaction_id == transaction_id)
        {
            error = FGC_OK_NO_RSP;

            // Restore transactional settings from backup

            device.ppm[user].transactional = device.ppm[user].transactional_backup;

            // Perform class-specific rollback

            equipdevTransactionRollback(channel, user);

            // Notify transactional properties for publication

            equipdevTransactionNotify(channel, user);

            // Reset the transaction state and end the transaction

            transactionSetState(channel, user, FGC_TRANSACTION_STATE_NONE);
            device.ppm[user].transaction_id = 0;
        }
    }

    return error;
}



void transactionSet(struct Cmd *command, struct prop *property)
{
    // Set transaction state to NOT_TESTED

    transactionSetState(command->device->id, command->user, FGC_TRANSACTION_STATE_NOT_TESTED);

    // Call equipment device callback

    equipdevTransactionalPropertySet(command, property);
}



void transactionSetId(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Scan parameter which must be 1-65535 to be a valid transaction ID

    uint16_t transaction_id = transactionGetIdFromCommand(command);

    if(command->error)
    {
        return;
    }

    // Check whether a transaction is already in progress for the user

    if(device.ppm[command->user].transaction_id != 0)
    {
        command->error = FGC_TRANSACTION_IN_PROGRESS;
        return;
    }

    // Start transaction for this user : backup transactional settings and update the transaction state

    device.ppm[command->user].transaction_id = transaction_id;
    device.ppm[command->user].transactional_backup = device.ppm[command->user].transactional;

    transactionSetState(command->device->id, command->user, FGC_TRANSACTION_STATE_NOT_TESTED);
}



fgc_errno transactionTest(uint16_t transaction_id, uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    fgc_errno error = FGC_UNKNOWN_TRANSACTION_ID;

    // Check whether each user has a transaction open with the transaction ID

    for(uint32_t user = 0 ; user < NUM_CYC_SELECTORS ; user++)
    {
        if(device.ppm[user].transaction_id == transaction_id)
        {
            // Test the transactional settings

            if(!(error = equipdevTransactionTest(channel, user)))

            {
                // The transaction test succeeded for the user

                transactionSetState(channel, user, FGC_TRANSACTION_STATE_TEST_OK);

                error = FGC_OK_NO_RSP;
            }
            else // The transaction test failed for the user
            {
                transactionSetState(channel, user, FGC_TRANSACTION_STATE_TEST_FAILED);

                // Stop and return the first error that occurred

                return error;
            }
        }
    }

    return error;
}

// EOF
