//! @file   equip_libref.cpp
//! @brief  Function that interact with libref


// Includes

#include <unistd.h>
#include <timing.h>
#include <timing_handlers.h>
#include <cmwpub.h>
#include <fgcd_thread.h>
#include <logging.h>
#include <equip_common.h>
#include <equipdev.h>
#include <reference.h>
#include <nonvol.h>
#include <defprops_class.h>
#include <libref/refFgParIdx.h>
#include <libref/refParsLinkStructs.h>
#include <fgc_errs.h>
#include <mode_pc.h>


// Constants

static const uint32_t MAX_PROP_NAME_LEN = 40;

const static struct timeval FGC_PC_ARM_TIMEOUT {.tv_sec = 0, .tv_usec = FGC_PC_ARM_TIMEOUT_MS * 1000};


// Static variables (file scope)

static struct prop *fg_pars_link[REF_NUM_FG_PARS];


// Static function declarations

static bool AwaitDeviceArmed(struct Equipdev_channel const &device,
                             struct timeval const& timeout);

// Static function definitions

static bool AwaitDeviceArmed(struct Equipdev_channel const &device,
                             struct timeval const& timeout)
{
    // Calculate wait deadline as time_start + timeout
    // The deadline ensures that we don't get stuck here if the arming fails (asynchronously) for an unexpected reason

    struct timeval time_start, time_limit, time_test;

    gettimeofday(&time_start, NULL);
    timeradd(&time_start, &timeout, &time_limit);

    while (device.status.state_pc != FGC_PC_ARMED) {
        gettimeofday(&time_test, NULL);

        if (timercmp(&time_test, &time_limit, >)) {
            break;
        }

        // Pause for 1 ms

        usleep(1000);
    }

    return (device.status.state_pc == FGC_PC_ARMED);
}

// External functions

uint32_t referenceInit(uint32_t device_index)
{
    struct Equipdev_channel &device  = equipdev.device[device_index];
    struct REF_mgr          *ref_mgr = &device.ref_mgr;

    // Set up mutex of libreg/ref

    if(fgcd_mutex_init(&device.arm_event_mutex, EQUIPDEV_MUTEX_POLICY) != 0)
    {
        fprintf(stderr, "ERROR: referenceInit(device_id=%u): fgcd_mutex_init(&device.arm_event_mutex)\n", device_index);
        return 1;
    }

    // Use helper functions for transaction last_fg_par_index and CTRL, REF, RAMP, PULSE, PLEP, PPPL, CUBEXP, TRIM, TEST and PRBS

    refMgrInitTransactionPars(ref_mgr, &device.ppm[0].transaction,          0, sizeof(device.ppm[0]));
    refMgrInitCtrlPars       (ref_mgr, &device.ppm[0].ctrl,                 0, sizeof(device.ppm[0]));
    refMgrInitRefPars        (ref_mgr, &device.ppm[0].transactional.ref,    0, sizeof(device.ppm[0]));
    refMgrInitRampPars       (ref_mgr, &device.ppm[0].transactional.ramp,   0, sizeof(device.ppm[0]));
    refMgrInitPulsePars      (ref_mgr, &device.ppm[0].transactional.pulse,  0, sizeof(device.ppm[0]));
    refMgrInitPlepPars       (ref_mgr, &device.ppm[0].transactional.plep,   0, sizeof(device.ppm[0]));
    refMgrInitPpplPars       (ref_mgr, &device.ppm[0].transactional.pppl,   0, sizeof(device.ppm[0]));
    refMgrInitCubexpPars     (ref_mgr, &device.ppm[0].transactional.cubexp, 0, sizeof(device.ppm[0]));
    refMgrInitTrimPars       (ref_mgr, &device.ppm[0].transactional.trim,   0, sizeof(device.ppm[0]));
    refMgrInitTestPars       (ref_mgr, &device.ppm[0].transactional.test,   0, sizeof(device.ppm[0]));
    refMgrInitPrbsPars       (ref_mgr, &device.ppm[0].transactional.prbs,   0, sizeof(device.ppm[0]));

    // TABLE must be initialised separately

    refMgrFgParInitPointer   (ref_mgr, table_function        , device.ppm[0].transactional.table.function);
    refMgrFgParInitCycSelStep(ref_mgr, table_function        , sizeof(device.ppm[0]));

    refMgrFgParInitPointer   (ref_mgr, table_function_num_els, &device.ppm[0].transactional.table.num_elements);
    refMgrFgParInitCycSelStep(ref_mgr, table_function_num_els, sizeof(device.ppm[0]));

    // Initialize the Interactive Learning Controller (ILC) array property num_elements

#ifdef PROP_REF_ILC_FUNCTION
    ref_mgr->pars.ilc_l_func_num_els[0] = (uintptr_t*)((char*)PROP_REF_ILC_FUNCTION.num_elements + sizeof(equipdev.device[0]) * device_index);
    ref_mgr->pars.ilc_q_func_num_els[0] = (uintptr_t*)((char*)PROP_REF_ILC_Q_FUNCTION.num_elements + sizeof(equipdev.device[0]) * device_index);
#endif

    // Initialise array of pointers to properties for the REF FG parameters

    fg_pars_link[REF_FG_PAR_REF_FG_TYPE         ] = &PROP_REF_FUNC_TYPE            ;  // Function type.
    fg_pars_link[REF_FG_PAR_RAMP_INITIAL_REF    ] = &PROP_REF_RAMP_INITIAL         ;  // Initial reference for a RAMP when cycling. Ignored when arming in IDLE state.
    fg_pars_link[REF_FG_PAR_RAMP_FINAL_REF      ] = &PROP_REF_RAMP_FINAL           ;  // Final reference for a RAMP.
    fg_pars_link[REF_FG_PAR_RAMP_ACCELERATION   ] = &PROP_REF_RAMP_ACCELERATION    ;  // RAMP acceleration.
    fg_pars_link[REF_FG_PAR_RAMP_DECELERATION   ] = &PROP_REF_RAMP_DECELERATION    ;  // RAMP deceleration.
    fg_pars_link[REF_FG_PAR_RAMP_LINEAR_RATE    ] = &PROP_REF_RAMP_LINEAR_RATE     ;  // Maximum linear rate during the RAMP.
    fg_pars_link[REF_FG_PAR_PULSE_REF           ] = &PROP_REF_PULSE_AMPLITUDE_VALUE;  // Maximum linear rate during the RAMP.
    fg_pars_link[REF_FG_PAR_PULSE_DURATION      ] = &PROP_REF_PULSE_DURATION       ;  // Maximum linear rate during the RAMP.
    fg_pars_link[REF_FG_PAR_PLEP_INITIAL_REF    ] = &PROP_REF_PLEP_INITIAL         ;  // Initial reference for a PLEP when cycling. Ignored when arming in IDLE state.
    fg_pars_link[REF_FG_PAR_PLEP_FINAL_REF      ] = &PROP_REF_PLEP_FINAL           ;  // Final reference for a PLEP.
    fg_pars_link[REF_FG_PAR_PLEP_ACCELERATION   ] = &PROP_REF_PLEP_ACCELERATION    ;  // PLEP acceleration.
    fg_pars_link[REF_FG_PAR_PLEP_LINEAR_RATE    ] = &PROP_REF_PLEP_LINEAR_RATE     ;  // Maximum linear rate during the PLEP.
    fg_pars_link[REF_FG_PAR_PLEP_EXP_TC         ] = &PROP_REF_PLEP_EXP_TC          ;  // PLEP exponential segment time constant.
    fg_pars_link[REF_FG_PAR_PLEP_EXP_FINAL      ] = &PROP_REF_PLEP_EXP_FINAL       ;  // PLEP exponential segment asymptote.
    fg_pars_link[REF_FG_PAR_PPPL_INITIAL_REF    ] = &PROP_REF_PPPL_INITIAL         ;  // Initial reference for a PPPL when cycling. Ignored when arming in IDLE state.
    fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION1  ] = &PROP_REF_PPPL_ACCELERATION1   ;  // Array of initial PPPL accelerations.
    fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION2  ] = &PROP_REF_PPPL_ACCELERATION2   ;  // Array of second PPPL accelerations.
    fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION3  ] = &PROP_REF_PPPL_ACCELERATION3   ;  // Array of third PPPL accelerations.
    fg_pars_link[REF_FG_PAR_PPPL_RATE2          ] = &PROP_REF_PPPL_RATE2           ;  // Array of rates at the start of the second PPPL accelerations.
    fg_pars_link[REF_FG_PAR_PPPL_RATE4          ] = &PROP_REF_PPPL_RATE4           ;  // Array of rates at the start of the linear PPPL segments.
    fg_pars_link[REF_FG_PAR_PPPL_REF4           ] = &PROP_REF_PPPL_REF4            ;  // Array of references at the start of the linear PPPL segments.
    fg_pars_link[REF_FG_PAR_PPPL_DURATION4      ] = &PROP_REF_PPPL_DURATION4       ;  // Array of durations of the fourth linear linear PPPL segments.
    fg_pars_link[REF_FG_PAR_CUBEXP_REF          ] = &PROP_REF_CUBEXP_REF           ;  // Array of cubexp start/end references.
    fg_pars_link[REF_FG_PAR_CUBEXP_RATE         ] = &PROP_REF_CUBEXP_RATE          ;  // Array of cubexp start/end rates.
    fg_pars_link[REF_FG_PAR_CUBEXP_TIME         ] = &PROP_REF_CUBEXP_TIME          ;  // Array of cubexp start/end times.
    fg_pars_link[REF_FG_PAR_TABLE_FUNCTION      ] = &PROP_REF_TABLE_FUNC_VALUE     ;  // TABLE function points array.
    fg_pars_link[REF_FG_PAR_TRIM_INITIAL_REF    ] = &PROP_REF_TRIM_INITIAL         ;  // Initial reference for a TRIM when cycling. Ignored when arming in IDLE state.
    fg_pars_link[REF_FG_PAR_TRIM_FINAL_REF      ] = &PROP_REF_TRIM_FINAL           ;  // Final reference for a TRIM.
    fg_pars_link[REF_FG_PAR_TRIM_DURATION       ] = &PROP_REF_TRIM_DURATION        ;  // Duration for a TRIM. Set to zero to be limited only by the rate of change limit.
    fg_pars_link[REF_FG_PAR_TEST_INITIAL_REF    ] = &PROP_REF_TEST_INITIAL         ;  // Initial reference for a TEST when cycling. Ignored when arming in IDLE state.
    fg_pars_link[REF_FG_PAR_TEST_AMPLITUDE_PP   ] = &PROP_REF_TEST_AMPLITUDE       ;  // TEST function peak-peak amplitude.
    fg_pars_link[REF_FG_PAR_TEST_PERIOD         ] = &PROP_REF_TEST_PERIOD          ;  // TEST function period.
    fg_pars_link[REF_FG_PAR_TEST_NUM_PERIODS    ] = &PROP_REF_TEST_NUM_PERIODS     ;  // Number of TEST function periods to play.
    fg_pars_link[REF_FG_PAR_TEST_WINDOW         ] = &PROP_REF_TEST_WINDOW          ;  // Control of TEST function half-period sine window.
    fg_pars_link[REF_FG_PAR_TEST_EXP_DECAY      ] = &PROP_REF_TEST_EXP_DECAY       ;  // Control of TEST function exponential amplitude delay.
    fg_pars_link[REF_FG_PAR_PRBS_INITIAL_REF    ] = &PROP_REF_PRBS_INITIAL         ;  // Initial reference for a PRBS when cycling. Ignored when arming in IDLE state.
    fg_pars_link[REF_FG_PAR_PRBS_AMPLITUDE_PP   ] = &PROP_REF_PRBS_AMPLITUDE_PP    ;  // PRBS peak-peak amplitude.
    fg_pars_link[REF_FG_PAR_PRBS_PERIOD_ITERS   ] = &PROP_REF_PRBS_PERIOD_ITERS    ;  // PRBS period in iterations.
    fg_pars_link[REF_FG_PAR_PRBS_NUM_SEQUENCES  ] = &PROP_REF_PRBS_NUM_SEQUENCES   ;  // Number of PRBS sequences to play.
    fg_pars_link[REF_FG_PAR_PRBS_K              ] = &PROP_REF_PRBS_K               ;  // PRBS K factor. Sequence length is 2^K - 1 periods.

    // Init Ref Manager

    const int32_t err_num = refMgrInit( ref_mgr,
                                        &device.polswitch_mgr,
                                        &device.reg_mgr,
                                        &device.arm_event_mutex,
                                        device.libref_vars.fg_error,
                                        device.libref_vars.cyc_status,
                                        device.libref_vars.ref_armed,
                                        device.libref_vars.armed_table_function,
                                        device.libref_vars.running_table_function,
                                        device.libref_vars.ilc_cyc_data_store,
                                        NUM_CYC_SELECTORS,
                                        FGC_TABLE_LEN,
                                        FGC_ILC_MAX_SAMPLES,
                                        0);                         // ref_to_tc_limit

    if(err_num < 0)
    {
        fprintf(stderr, "ERROR: Failed to initialise libref for device %d. Err_num: %d\n", device_index, err_num);
        return 1;
    }
    else if(err_num > 0)
    {
        fprintf(stderr, "ERROR: Failed to initialise libref for device %d. Parameter %d has a NULL pointer\n", device_index, err_num);
        return 1;
    }

    // Initialise default values

    refMgrParValue(ref_mgr, MODE_REG_MODE) = refMgrParValue(ref_mgr, MODE_REG_MODE_CYC);

    // Call class specific initialisation

    return(referenceInitClass(device_index));
}



void referenceArmAll(uint32_t device_index)
{
    struct Equipdev_channel &device  = equipdev.device[device_index];

    // Attempt to all all PPM functions

    for(uint32_t user = 1 ; user <= FGC_TIMING_NUM_USERS ; user++)
    {
        const char *func_type = fg_func_names[refMgrFgParValueSubCyc(&device.ref_mgr, 0, user, REF_FG_TYPE)];

        uint32_t errnum = referenceArm(device_index, user, false, 0, NULL);

        if(errnum != FGC_OK_NO_RSP)
        {
            fprintf(stderr, "WARNING: Failed to arm %u.%s(%u) with %s. Error %u %s\n",
                    device_index, device.fgcd_device->name, user, func_type, errnum, fgc_errmsg[errnum]);
        }
    }
}


// Map MODE.PC to REF_state

enum REF_state translateModePctoRef(uint32_t device_index, uint32_t mode)
{
    switch(mode)
    {
        case FGC_PC_OFF:           return REF_OFF;
        case FGC_PC_ON_STANDBY:    return REF_STANDBY;
        case FGC_PC_IDLE:          return REF_IDLE;
        case FGC_PC_CYCLING:       return REF_CYCLING;

        default:
            logPrintf(device_index, "MODE_PC MODE.PC=%d is invalid\n", mode);
            return REF_OFF;
    }
}


// Translates cclibs FG_errno to the equivalent FGC_* constant

static fgc_errno referenceFgcErrno(enum FG_errno fg_errno)
{
    switch(fg_errno)
    {
        case FG_OK:                 return  FGC_OK_NO_RSP;
        case FG_BAD_ARRAY_LEN:      return  FGC_BAD_ARRAY_LEN;
        case FG_BAD_PARAMETER:      return  FGC_BAD_PARAMETER;
        case FG_INVALID_TIME:       return  FGC_INVALID_TIME;
        case FG_OUT_OF_LIMITS:      return  FGC_OUT_OF_LIMITS;
        case FG_OUT_OF_RATE_LIMITS: return  FGC_OUT_OF_RATE_LIMITS;
        case FG_INIT_REF_MISMATCH:  return  FGC_REF_MISMATCH;
        case FG_INVALID_REG_MODE:   return  FGC_BAD_REG_MODE;
        case FG_INVALID_REF_STATE:  return  FGC_BAD_STATE;
        case FG_INVALID_CYC_SEL:    return  FGC_BAD_CYCLE_SELECTOR;

        case FG_INVALID_SUB_SEL:    // fall through
        case FG_NUM_ERRORS:         return FGC_NOT_IMPL;
    }

    return FGC_NOT_IMPL;
}



fgc_errno referenceArm(uint32_t device_index, uint32_t user, bool test_arm, uint32_t num_pars, cc_float *par_values)
{
    struct Equipdev_channel &device = equipdev.device[device_index];
    enum REF_fg_par_idx      fg_pars_idx[REF_FG_PAR_MAX_PER_GROUP];

    // Attempt to arm the reference function

    enum FG_errno fg_errno = refArm(&device.ref_mgr,
                                    0,                                // sub_sel
                                    user,
                                    test_arm == true ? CC_ENABLED : CC_DISABLED,
                                    num_pars,
                                    par_values,
                                    fg_pars_idx);

    // Store properties in non-volatile storage if a PPM function was successfully armed (not just tested)

    if(fg_errno == FG_OK && user > 0 && test_arm == false)
    {
        for(uint32_t idx = 0 ; idx < REF_FG_PAR_MAX_PER_GROUP && fg_pars_idx[idx] != REF_FG_PAR_NULL ; idx++)
        {
            nonvolStore(&fgcd.device[device_index],
                        fg_pars_link[fg_pars_idx[idx]]->name,
                        user,
                        false,          // last - store in last folder - not used by Classes 9294
                        false);         // config
        }
    }

    return(referenceFgcErrno(fg_errno));
}


// --- Setif functions


int32_t SetifRefNone(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Return whether function generator is armed for user

    return device.ref_mgr.ref_armed[command->user].fg_pars.meta.type == FGC_REF_NONE;
}



int32_t SetifRefOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    if(command->user == 0)
    {
        // Non-PPM command : reference can be set when device is in IDLE state

        return device.status.state_pc == FGC_PC_IDLE;
    }
    else
    {
        // PPM command : reference can be always set

        return 1;
    }
}



int32_t SetifRefUnlock(struct Cmd *command)
{

    return 1;
}



int32_t SetifTxIdOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // A transaction may not be started for non-PPM settings when in the ARMED state

    if(command->user == 0 && refMgrVarValue(&device.ref_mgr, REF_STATE) == REF_ARMED)
    {
        return 0;
    }

    return 1;
}


// --- Set functions


static int32_t propEquipRefArm(uint32_t fgc_ref_type, struct Cmd *command, uint32_t num_pars, cc_float *par_values)
{
    uint32_t                  channel = command->device->id;
    struct Equipdev_channel   &device = equipdev.device[channel];

    // REF NOW can only be set for non-PPM users

    if(fgc_ref_type == FGC_REF_NOW && command->user)
    {
        command->error = FGC_BAD_CYCLE_SELECTOR;
        return 1;
    }

    // Save FGC reference type in property variable, substituting RAMP for now

    device.ppm[command->user].transactional.ref.fg_type[0] = fgc_ref_type == FGC_REF_NOW
                                                           ? FG_RAMP
                                                           : (enum FG_type)fgc_ref_type;

    // Arming is not enabled: nothing else to do

    if(device.arming_enabled == false)
    {
        return 0;
    }

    // Try to arm reference

    command->error = referenceArm(channel, command->user, false, num_pars, par_values);

    if(command->error) return 1;

    // S REF NOW,... is equivalent to S REF RAMP,... followed by S REF.RUN

    if(fgc_ref_type == FGC_REF_NOW)
    {
        // Set run_now flag to tell statePcAr() to call refEventRun on entry to ARMED
        // state with zero time. This will run the RAMP after 1s.
        // The run_now flag is reset on entry into IDLE by statePcIl().

        device.ref.run_now = true;
    }

    return 0;
}



static int32_t propEquipScanRefParams(char delimiter, float float_buffer[REF_FG_PAR_MAX_ARM_PAR_VALUES], struct Cmd *command)
{
    // Scan float parameters from command string

    uint32_t float_count;

    for(float_count = 0; float_count < REF_FG_PAR_MAX_ARM_PAR_VALUES && delimiter != '\0' && command->error == 0; ++float_count)
    {
        double value;

        parserScanFloat(command->parser, command->value, &command->index, command->value_length, DELIM_COMMA, 1, &delimiter, 0, 0, 0, 0, &value);

        // If a parameter is missing, set NaN so that libref will substitute the default value for that parameter

        if(command->error == FGC_NO_SYMBOL)
        {
            float_buffer[float_count] = NAN;
            command->error = 0;
        }
        else
        {
            float_buffer[float_count] = value;
        }
    }

    // Handle scan errors

    if(command->error == 0)
    {
        // Check that the end of the command was reached

        command->error = (delimiter == '\0') ? 0 : FGC_BAD_PARAMETER;
    }

    if(command->error != 0) return -1;

    return float_count;
}



int32_t SetRef(struct Cmd *command, struct prop *property)
{
    uint8_t fgc_ref_type;
    char    delimiter;

    // Get the first parameter (the reference function type)

    if(propEquipScanSymbolList(&fgc_ref_type, &delimiter, command, property) != 0) return 1;

    // If we are setting REF.FUNC.TYPE, no additional parameters are permitted

    if(property->sym_idx == STP_TYPE && delimiter != '\0')
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // If we are setting REF directly, get the reference function parameters

    float   par_values[REF_FG_PAR_MAX_ARM_PAR_VALUES];
    int32_t num_pars = 0;

    if(property->sym_idx == STP_REF)
    {
        // S REF {FG_TYPE} can only be set for non-PPM users

        if(command->user)
        {
            command->error = FGC_BAD_CYCLE_SELECTOR;
            return 1;
        }

        num_pars = propEquipScanRefParams(delimiter, par_values, command);

        if(num_pars < 0) return 1;
    }

    // Arm the reference function

    if(propEquipRefArm(fgc_ref_type, command, num_pars, par_values) != 0) return 1;

    // If user is non-zero, we are done.
    // Otherwise, we must wait for PC state to be ARMED before returning.

    if (command->user != 0)
    {
        return 0;
    }

    struct Equipdev_channel &device = equipdev.device[command->device->id];

    if (!AwaitDeviceArmed(device, FGC_PC_ARM_TIMEOUT))
    {
        return 1;
    }

    return 0;
}



int32_t SetRefPulse(struct Cmd *command, struct prop *property)
{
    // Don't support autoarming of PULSE, since this feature will be dropped soon

    return SetFloat(command, property);
}



int32_t SetTableFunc(struct Cmd *command, struct prop *property)
{
    // Support auto-arming until this feature can be dropped

    struct Equipdev_channel &device = equipdev.device[command->device->id];
    bool transaction_in_progress = device.ppm[command->user].transaction_id != 0;

    // Setting an empty table is not permitted

    if (strlen(command->value) == 0)
    {
        command->error = FGC_BAD_ARRAY_LEN;
        return 1;
    }

    // Only allow setting REF.TABLE.FUNC.VALUE in IDLE state for user 0
    // if transaction not in progress  (see EPCCCS-4815)

    if(command->user == 0 && !transaction_in_progress && device.status.state_pc != FGC_PC_IDLE)
    {
        command->error = FGC_BAD_STATE;
        return 1;
    }

    // Set property value with the table

    if(SetPoint(command, property))
    {
        return 1;
    }

    // Return immediately if a transaction is in progress or device arming is disabled or arming a PPM user
    // This will support auto-arming of TABLES for FGClite only, pending adoption of transactional settings
    // in the LHC. This depends on Class 51 having transactional settings support and may not be done before
    // LS3.

    if(transaction_in_progress || !device.arming_enabled || command->user > 0)
    {
        return 0;
    }

    // Try to arm the table

    int32_t error = propEquipRefArm(FGC_REF_TABLE, command, 0, NULL);

    if (error != 0)
    {
        return error;
    }

    // Wait for PC state to reach ARMED before returning.

    if (!AwaitDeviceArmed(device, FGC_PC_ARM_TIMEOUT))
    {
        return 1;
    }

    return 0;
}


// --- Pars functions


void ParsMeasSim(uint32_t channel)
{
    // Called when the following properties are set:
    //
    // MEAS.SIM
    //
    // Tell the Reference state machine whether to use real or simulated measurements.
    //
    // Note: the measurement simulation mode can only change when STATE.PC is OFF or FAULT_OFF.

    struct Equipdev_channel &device = equipdev.device[channel];

    refMgrParValue(&device.ref_mgr, MODE_SIM_MEAS) =    device.status.state_op == FGC_OP_SIMULATION
                                                        && device.meas.sim == FGC_CTRL_ENABLED
                                                      ? CC_ENABLED
                                                      : CC_DISABLED;
}



void ParsRunTime(uint32_t channel)
{
    // Called when the following properties are set:
    //
    // REF.RUN

    refEventRun(&equipdev.device[channel].ref_mgr);
}



void ParsAbortTime(uint32_t channel)
{
    // Called when the following properties are set:
    //
    // REF.ABORT

    refEventAbort(&equipdev.device[channel].ref_mgr);
}


// -- Timing handlers


void timingAddEventDelay(tTimingTime *warning_evt_time, struct CC_us_time *evt_time, uint32_t delay_ms)
{
    // Initialise the event time to the warning time

    struct CC_us_time const evt_us_time = { { warning_evt_time->time.tv_sec}, warning_evt_time->time.tv_nsec / NS_PER_US };

    // Advance the event time by the event delay

    *evt_time = cctimeUsAddOffsetRT(evt_us_time, delay_ms * US_PER_MS);
}



void timingStartRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t             channel;
    uint32_t             event_group = 0;
    static Timing::Value value;

    // Get EVENT_GROUP from the timing event

    try
    {
        evt_value->getFieldValue("EVENT_GROUP", &value);
        event_group = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "LIBREF Event %s Field EVENT_GROUP = %u\n",
                evt_value->getName().c_str(),
                event_group);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Calculate run time from warning event time and the delay

    struct CC_us_time run_time;

    timingAddEventDelay(&evt_time, &run_time, info.delay_ms);

    // Run references for all devices that are armed for the received event group

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Check whether the payload is a broadcast or corresponds to the device's event group

        if(   device.fgcd_device->name
           && (event_group == 0 || event_group == device.ref.event_group))
        {
            // Register ramp run event with libref

            refMgrParValue(&device.ref_mgr, REF_RUN) = run_time;

            if(refEventRun(&device.ref_mgr) == true)
            {
                struct CC_us_time const ref_run = refMgrParValue(&device.ref_mgr, REF_RUN);
                struct timeval const timestamp = { ref_run.secs.abs, ref_run.us };

                ccEvtLogStoreTimingEvent(channel, &timestamp, "START_REF", timing.user);
            }
        }
    }
}



void timingAbortRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t             channel;
    uint32_t             event_group = 0;
    static Timing::Value value;
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Get EVENT_GROUP from the timing event

    try
    {
        evt_value->getFieldValue("EVENT_GROUP", &value);
        event_group = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "LIBREF Event %s Field EVENT_GROUP = %u\n",
                evt_value->getName().c_str(),
                event_group);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Abort references immediately for all devices for the received event group

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // If channel is in use

        if(device.fgcd_device->name)
        {
            // Check whether the payload is a broadcast or corresponds to the device's event group

            if(event_group == 0 || event_group == device.ref.event_group)
            {
                // Register ramp abort event with libref

                if(refEventAbort(&device.ref_mgr) == true)
                {
                    // if abort event accepted than update REF.ABORT property time and record it in the event log

                    device.ref.abort = timestamp;

                    ccEvtLogStoreTimingEvent(channel, &timestamp, "ABORT_REF", timing.user);
                }
            }
        }
    }
}



void timingCycleWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Get USER from the timing event

    try
    {
        evt_value->getFieldValue("USER", &value);

        uint32_t next_user = timingUserNumber(value.getAsString().c_str());
        if(!next_user) return;

        timing.next_user = next_user;

        if(timing.log_events_flag)
        {
            logPrintf(0, "LIBREF Event %s Field USER = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.next_user);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Calculate start function time from warning event time and the delay

    struct CC_us_time start_time;

    timingAddEventDelay(&evt_time, &start_time, info.delay_ms);

    // Start the countdown to the start of the cycle

    timing.next_cycle_countdown_ms = info.delay_ms;

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // If device is in use

        if(device.fgcd_device->name)
        {
            // Signal libRef that new cycle will start

            if(refEventStartFunc(&device.ref_mgr, &start_time, 0, timing.next_user, false) == true)
            {
                ccEvtLogStoreTimingEvent(channel, &timestamp, "START_CYCLE", timing.user);
            }
        }
    }
}



void timingCoast(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Calculate start function time from warning event time and the delay

    struct CC_us_time coast_time;

    timingAddEventDelay(&evt_time, &coast_time, info.delay_ms);

    // Set coast event for all device where REF.COAST is enabld

    for(uint32_t channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Register coast event if channel is in use and coast is enabled

        if(device.fgcd_device->name && device.coast)
        {
            if(refEventCoast(&device.ref_mgr, &coast_time))
            {
                // if event is accepted then record it in the event log

                ccEvtLogStoreTimingEvent(channel, &timestamp, "COAST", timing.user);
            }
        }
    }
}



void timingRecover(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Calculate start function time from warning event time and the delay

    struct CC_us_time recover_time;

    timingAddEventDelay(&evt_time, &recover_time, info.delay_ms);

    // Set coast event for all device where REF.COAST is enabld

    for(uint32_t channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Register recover event if channel is in use and coast is enabled

        if(device.fgcd_device->name && device.coast)
        {
            if(refEventRecover(&device.ref_mgr, &recover_time))
            {
                // if event is accepted then record it in the event log

                ccEvtLogStoreTimingEvent(channel, &timestamp, "RECOVER", timing.user);
            }
        }
    }
}



void timingEconomy(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Ignore economy request if this is the last millisecond of a cycle

    if(timing.next_ms_cycle_ms == 0)
    {
        return;
    }

    // Attempt to switch each device to an economy function for this cycle

    for(uint32_t channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Register dynamic economy event if channel is in use and economy is enabled

        if(device.fgcd_device->name && refMgrParValue(&device.ref_mgr,MODE_ECONOMY))
        {
            refMgrParValue(&device.ref_mgr,ECONOMY_DYNAMIC) = CC_ENABLED;

            ccEvtLogStoreTimingEvent(channel, &timestamp, "DYNAMIC_ECONOMY", timing.user);
        }
    }
}



void timingToCycling(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Put all devices that are in valid states to TO_CYCLING

    for(uint32_t channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Register ToCycling event if channel is in use and state is ON_STANDBY

        if(device.fgcd_device->name && device.status.state_pc == FGC_PC_ON_STANDBY)
        {
            modePcSet(channel, FGC_PC_CYCLING);

            ccEvtLogStoreTimingEvent(channel, &timestamp, "TO_CYCLING", timing.user);
        }
    }
}



void timingToStandby(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // Put all devices that are in valid states to TO_STANDBY

    for(uint32_t channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Register ToStandby event if channel is in use and state is CYCLING

        if(device.fgcd_device->name && device.status.state_pc == FGC_PC_CYCLING)
        {
            modePcSet(channel, FGC_PC_ON_STANDBY);

            ccEvtLogStoreTimingEvent(channel, &timestamp, "TO_STANDBY", timing.user);
        }
    }
}


// --- Transactional setting functions


void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property)
{
    uint32_t channel = command->device->id;
    struct REF_mgr *ref_mgr = &equipdev.device[channel].ref_mgr;

    // Disarm non-PPM user if it is armed

    if(command->user == 0 && refMgrVarValue(ref_mgr, REF_STATE) == REF_ARMED)
    {
        // Reset non-PPM reference function type to NONE

        refMgrFgParValue(ref_mgr,REF_FG_TYPE) = FG_NONE;

        referenceArm(channel, 0, false, 0, NULL);
    }

    // Linear search fg_pars_link array to find the parameter index for the transactional property being set

    for(uint32_t fg_par_idx = REF_FG_FIRST_TRANSACTIONAL_PAR; fg_par_idx < REF_NUM_FG_PARS ; fg_par_idx++)
    {
        if(property == fg_pars_link[fg_par_idx])
        {
            // Property identified so register fg parameter index with libref

            refMgrFgParValueSubCyc(ref_mgr, 0, command->user, TRANSACTION_LAST_FG_PAR_INDEX) = (enum REF_fg_par_idx)fg_par_idx;

            return;
        }
    }

    logPrintf(channel, "EQUIPDEV equipdevTransactionalPropertySet: failed to find fg_pars_link for property at %p (symbol=%s)\n",
            property, SYM_TAB_PROP[property->sym_idx].key.c);
}



void equipdevTransactionNotify(uint32_t channel, uint32_t user)
{
    // Publish all transactional properties for the user

    for(uint32_t fg_par_idx = REF_FG_FIRST_TRANSACTIONAL_PAR; fg_par_idx < REF_NUM_FG_PARS ; fg_par_idx++)
    {
        if(fg_pars_link[fg_par_idx] != NULL)
        {
            cmwpubNotify(channel, fg_pars_link[fg_par_idx]->name, user, 0, 0, 1, CMWPUB_UPDATE_FLAGS_IMMEDIATE);
        }
    }
}



fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const timeval *tv_run_time)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    fgc_errno error = FGC_OK_NO_RSP;

    // If REF_STATE is ARMED, start the reference in delay_ms ms

    if(refMgrVarValue(&device.ref_mgr, REF_STATE) == REF_ARMED)
    {
        // Convert timeval run_time to CC_us_time structure

        struct CC_us_time run_time = { { tv_run_time->tv_sec }, tv_run_time->tv_usec };

        // Register run event with libref

        refMgrParValue(&device.ref_mgr, REF_RUN) = run_time;
        refEventRun(&device.ref_mgr);
    }
    else
    {
        // Not in ARMED state so arm the reference immediately for use in CYCLING

        error = referenceArm(channel, user, false, 0, NULL);
    }

    return error;
}



void equipdevTransactionRollback(uint32_t channel, uint32_t user)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Disarm non-PPM user if it is armed

    if(user == 0 && refMgrVarValue(&device.ref_mgr, REF_STATE) == REF_ARMED)
    {
        // Reset non-PPM reference function type to NONE

        refMgrFgParValue(&device.ref_mgr,REF_FG_TYPE) = FG_NONE;

        referenceArm(channel, 0, false, 0, NULL);
    }

    // Reset last FG parameter index in libref

    refMgrFgParValueSubCyc(&device.ref_mgr, 0, user, TRANSACTION_LAST_FG_PAR_INDEX) = REF_FG_PAR_NULL;
}



fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    fgc_errno error = FGC_BAD_STATE;

    if(user == 0)
    {
        // Classes 92 & 94 only allow arming user 0 in IDLE state

        if (device.status.state_pc == FGC_PC_IDLE)
        {
            // Real arm

            error = referenceArm(channel, 0, false, 0, NULL);
        }
    }
    else
    {
        // Test arm

        error = referenceArm(channel, user, true, 0, NULL);
    }

    return error;
}

// EOF
