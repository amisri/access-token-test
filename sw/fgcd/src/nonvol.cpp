/*!
 * @file   nonvol.c
 * @brief  Functions for non-volatile property storage
 * @author Stephen Page
 */

#include <stdio.h>
#include <cstring>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>

#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <defconst.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <nonvol.h>
#include <queue.h>
#include <timing.h>
#include <fgcd_thread.h>



// Struct containing global variables

struct Nonvol
{
    pthread_t    thread;                // Thread handle
    struct Queue command_queue;         // Queue of commands for which to store values
    uint32_t     queued_cmds;           // Count of queued commands
    time_t       last_modified_time;    // Time of the last write to non-volatile storage
};



// Global variables

struct Nonvol nonvol;



// Static functions

static FGCD_thread_func nonvolStoreValues;

static int32_t nonvolReadDir(struct Queue *response_queue, struct FGCD_device *device, char *dir_name);
static int32_t nonvolReadPropFile(struct Queue *response_queue, struct FGCD_device *device, char *file_name);
static void nonvolWarn(struct FGCD_device *device, int32_t set);



int32_t nonvolStart(void)
{
    // Initialise time of last reset to current time

    time(&nonvol.last_modified_time);

    // Initialise command queue

    if(queueInit(&nonvol.command_queue, cmdqmgr.num_cmds))
    {
        fprintf(stderr, "ERROR: nonvolStart:Failed to initialise command queue\n");
        return 1;
    }

    // Start nonvol thread

    if(fgcd_thread_create(&nonvol.thread, nonvolStoreValues, NULL, NONVOL_THREAD_POLICY, NONVOL_THREAD_PRIORITY ,NO_AFFINITY) != 0)
    {
        queueFree(&nonvol.command_queue);
        return 1;
    }

    return 0;
}

int32_t nonvolStore(struct FGCD_device *device, const char *property, uint32_t user, int32_t last, int32_t is_config)
{
    struct Cmd *command;
    const char *user_name;

    // Overloaded function: set CONFIG.STATE to UNSYNCED in Class 6
    // At this point in time the SET command has been executed, and we just miss the non-volatile storage.

    if(is_config)
    {
        equipdevConfigUnsync(device->id);
    }

    // Get a command structure

    if(!(command = reinterpret_cast<struct Cmd*>(queueBlockPop(&cmdqmgr.free_cmd_q))))
    {
        nonvolWarn(device, 1);
        logPrintf(device->id, "NONVOL Failed to get a command structure for command \"g %s:%s(%u)\"\n",
                  device->name, property, user);
        return 1;
    }

    // Prepare command

    command->client         = CMD_FGCD_CLIENT;
    snprintf(command->device_name, FGC_MAX_DEV_LEN + 1, "%u", device->id);
    command->rbac_token     = NULL;
    command->response_queue = &nonvol.command_queue;
    command->struct_type    = CMD_STRUCT;
    command->tag[0]         = '\0';
    command->tag_length     = 0;
    command->type           = CMD_GET;

    // Note that command->remaining is overloaded here to indicate whether
    // command is a property needs to be set last during restoration

    command->remaining      = last;

    // Handle PPM and non-PPM commands

    if(user) // PPM command
    {
        // Get name of user

        user_name = timingUserName(user);

        if(!strlen(user_name))
        {
            nonvolWarn(device, 1);
            logPrintf(device->id, "NONVOL Failed to get name of user for command \"g %s:%s(%u)\"\n",
                      device->name, property, user);
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;
            return 1;
        }

        command->command_length = snprintf(command->command_string, CMD_MAX_LENGTH + 1,"%s(%s)", property, user_name);
    }
    else // Non-PPM command
    {
        command->command_length = snprintf(command->command_string, CMD_MAX_LENGTH + 1,
                                           "%s", property);
    }

    // Queue command

    nonvol.queued_cmds++;
    cmdqmgrQueue(command);
    command = NULL;

    return 0;
}



int32_t nonvolLoad(struct FGCD_device *device, int32_t last)
{
    char            dir_name[FILENAME_MAX + 1];
    struct Queue    response_queue;
    int32_t         status = 0;

    // Check that device has a name

    if(!device->name) return 1;

    // Initialise response queue

    queueInit(&response_queue, 1);

    // Set path to read

    if(last) // Configuration for properties that need to be set last
    {
        snprintf(dir_name, sizeof(dir_name), "%s/last/%s",
                 NONVOL_PATH, device->name);
    }
    else // Configuration for normal properties
    {
        snprintf(dir_name, sizeof(dir_name), "%s/%s",
                 NONVOL_PATH, device->name);
    }

    // Read configuration directory for device

    if(nonvolReadDir(&response_queue, device, dir_name))
    {
        status = 1;
    }

    // Free response queue

    queueFree(&response_queue);

    return status;
}



time_t nonvolGetModifiedTime(void)
{
    return nonvol.last_modified_time;
}



// Static functions



/*
 * Non-volatile storage thread
 */

static void *nonvolStoreValues(void *unused)
{
    struct Cmd  *command;
    static char  dir_name     [FILENAME_MAX + 1];
    static char  file_name    [FILENAME_MAX + 1];
    static char  file_name_tmp[FILENAME_MAX + 1];
    FILE        *file;
    struct stat  s;

    // Process commands

    while(true)
    {
        if((command = (struct Cmd *)queueBlockPop(&nonvol.command_queue)) == NULL)
        {
            logPrintf(0, "nonvolStoreValues() could not read command queue, exiting.\n");
            break;
        }

        nonvol.queued_cmds--;

        // Check whether an error occurred processing the command

        if(command->error)
        {
            nonvolWarn(command->device, 1);
            logPrintf(command->device->id,
                      "NONVOL Error \"%s\" processing command \"g %s:%s\" for non-volatile storage\n",
                      fgc_errmsg[command->error], command->device->name, command->command_string);

            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;
            continue;
        }

        // Construct configuration directory name

        // Note that command->remaining is overloaded here to indicate whether
        // command is a property that needs to be set last during restoration

        if(command->remaining) // Property that needs to be set last
        {
            snprintf(dir_name, sizeof(dir_name), "%s/last/%s",
                     NONVOL_PATH, command->device->name);
        }
        else // Normal property
        {
            snprintf(dir_name, sizeof(dir_name), "%s/%s",
                     NONVOL_PATH, command->device->name);
        }

        // Create configuration directory for device

        if((mkdir(NONVOL_PATH,         0755) != 0 && errno != EEXIST) ||
           (mkdir(NONVOL_PATH "/last", 0755) != 0 && errno != EEXIST) ||
           (mkdir(dir_name,            0755) != 0 && errno != EEXIST))
        {
            nonvolWarn(command->device, 1);
            logPrintf(command->device->id, "NONVOL Failed to create directory %s\n", dir_name);
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;
            continue;
        }

        // Construct temporary file name

        snprintf(file_name_tmp, sizeof(file_name_tmp), "%s/.new", dir_name);

        // Open file

        if(!(file = fopen(file_name_tmp, "w")))
        {
            nonvolWarn(command->device, 1);
            logPrintf(command->device->id, "NONVOL Failed to create file %s\n", file_name_tmp);
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;
            continue;
        }

        // Write value to file

        if(fwrite(command->value, 1, command->value_length, file) != command->value_length ||
           fwrite("\n", 1, 1, file) != 1) // Append new line
        {
            nonvolWarn(command->device, 1);
            logPrintf(command->device->id, "NONVOL Failed to write value to file %s\n", file_name_tmp);
            fclose(file);
            unlink(file_name_tmp);
            queuePush(&cmdqmgr.free_cmd_q, command);
            command = NULL;
            continue;
        }
        fclose(file);

        // Construct final file name

        snprintf(file_name, sizeof(file_name), "%s/%s", dir_name, command->command_string);

        // Rename file to final name

        if(rename(file_name_tmp, file_name))
        {
            nonvolWarn(command->device, 1);
            logPrintf(command->device->id, "NONVOL Failed to rename file %s to %s\n", file_name_tmp, file_name);
        }

        // Record the time of the last update to non-volatile storage

        stat(file_name, &s);

        nonvol.last_modified_time = s.st_mtime;

        // Return command structure

        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;
    }

    return 0;
}



/*
 * Read a configuration directory for a device
 */

static int32_t nonvolReadDir(struct Queue       *response_queue,
                             struct FGCD_device *device,
                             char               *dir_name)
{
    struct dirent   dir_ent;
    struct dirent   *dir_ent_p;
    DIR             *directory;
    char            file_name[FILENAME_MAX + 1];
    int32_t         status = 0;

    // Open configuration directory

    if(!(directory = opendir(dir_name)))
    {
        if(errno == ENOENT) // Directory does not exist
        {
            return 0;
        }
        else // Failed to open directory
        {
            nonvolWarn(device, 1);
            fprintf(stderr, "ERROR: Failed to open configuration directory %s\n", dir_name);
            return 1;
        }
    }

    // Read each file within the directory

    while(!readdir_r(directory, &dir_ent, &dir_ent_p) && dir_ent_p)
    {
        // Ignore anything except symbolic links and normal files

        if(!(dir_ent.d_type & (DT_LNK | DT_REG)) ||
           !strcmp(dir_ent.d_name, ".new")) // Incomplete configuration file
        {
            continue;
        }

        // Construct file name with full path

        snprintf(file_name, sizeof(file_name), "%s/%s",
                 dir_name, dir_ent.d_name);

        // Read property file

        if(nonvolReadPropFile(response_queue, device, file_name))
        {
            status = 1;
        }
    }
    closedir(directory);

    return status;
}

static int32_t nonvolRemovePropFile(const char *src_path)
{
    static const char   OLD_FOLDER[]   = "old/";
    static const size_t OLD_FOLDER_LEN = strlen(OLD_FOLDER);

    const char *filename;
    char        dest_path[FILENAME_MAX +1];
    int         src_dir_len;

    // We need room to insert 'old/' into the path

    if(strlen(src_path)>FILENAME_MAX-OLD_FOLDER_LEN)
    {
        fprintf(stderr, "ERROR: Can't move property file %s: path too long\n", src_path);
        return 1;
    }

    // get origin directory

    if((filename = strrchr(src_path, '/')))
    {
        // Advance past directory delimiter

        filename++;
    }
    else
    {
        // File is in current working directory

        filename = src_path;
    }

    src_dir_len = (int) (filename-src_path);

    // copy directory into destination path

    strncpy(dest_path, src_path, src_dir_len);

    // append 'old/' to destination path

    strcpy(dest_path + src_dir_len, OLD_FOLDER);

    // create destination directory if it doesn't exist

    if(mkdir(dest_path, 0755) != 0 && errno != EEXIST)
    {
        fprintf(stderr, "ERROR: Failed to create directory for obsolete properties: %s\n", dest_path);
        return 1;
    }

    // append property name to destination path

    strcpy(dest_path + src_dir_len + OLD_FOLDER_LEN, filename);

    // move the property file

    if(rename(src_path, dest_path))
    {
        fprintf(stderr, "ERROR: Failed to move file %s to %s\n", src_path, dest_path);
        return 1;
    }

    fprintf(stderr, "nonvolRemovePropFile: Moved file %s to %s\n", src_path, dest_path);
    return 0;
}

/*
 * Read device property file
 */

static int32_t nonvolReadPropFile(struct Queue       *response_queue,
                                  struct FGCD_device *device,
                                  char               *file_name
                                  )
{
    struct Cmd  *command;
    FILE        *file;
    char        *file_base;
    int32_t     status = 0;

    // Open file

    if(!(file = fopen(file_name, "r")))
    {
        nonvolWarn(device, 1);
        fprintf(stderr, "ERROR: Failed to open config file %s\n", file_name);
        return 1;
    }

    // Get a command structure

    if(!(command = reinterpret_cast<struct Cmd*>(queueBlockPop(&cmdqmgr.free_cmd_q))))
    {
        nonvolWarn(device, 1);
        fprintf(stderr, "ERROR: Failed to get a command structure\n");
        fclose(file);
        return 1;
    }

    // Initialise command structure

    sprintf(command->device_name, "%u", device->id);

    command->client          = CMD_FGCD_CLIENT;
    command->response_queue  = response_queue;
    command->type            = CMD_SET;

    // Get pointer to file basename
    // This is the name of the property to be set

    if((file_base = strrchr(file_name, '/')))
    {
        // Advance past directory delimiter

        file_base++;
    }
    else // File is in current working directory
    {
        file_base = file_name;
    }

    // Copy file basename to command string

    strncpy(command->command_string, file_base, CMD_MAX_LENGTH + 1);
    command->command_string[CMD_MAX_LENGTH] = '\0';

    // Read value from file

    command->value_length = fread(command->value, 1, CMD_MAX_VAL_LENGTH, file) - 1; // -1 is to remove new line
    command->value[command->value_length] = '\0';

    // Check whether all data was successfully read from the file

    if(feof(file)) // All data has been read from the file
    {
        fclose(file);

        command->command_length = strlen(command->command_string);
        command->value_length   = strlen((char *)command->value);

        // Queue command

        cmdqmgrQueue(command);

        command = NULL;

        if(response_queue != NULL)
        {

            // Wait for response

            command = (struct Cmd *)queueBlockPop(response_queue);

            if(command->error == FGC_UNKNOWN_SYM)
            {
                // Property does not exist anymore, so we move it to the "old" properties directory

                fprintf(stderr, "WARNING: File refers to unknown property: %s\n", file_name);

                if(nonvolRemovePropFile(file_name))
                {
                    nonvolWarn(device, 1);
                }
            }
            else if(command->error != FGC_OK_NO_RSP)
            {

                // There was an error processing the command

                status = 1;
                nonvolWarn(device, 1);

                fprintf(stderr, "WARNING: Command from file %s returned error \"%s\"\n",
                        file_name, fgc_errmsg[command->error]);
            }
        }
    }
    else // Failed to read value from the file
    {
        fclose(file);

        status = 1;
        nonvolWarn(device, 1);

        fprintf(stderr, "ERROR: Failed to read value from file %s\n", file_name);
    }

    // Return command structure to the free queue

    if(command!=NULL)
    {
        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;
    }

    return status;
}



/*
 * Set or clear non-volatile warning for a device
 */

static void nonvolWarn(struct FGCD_device *device, int32_t set)
{
    // Handle gateway and equipment devices

    if(device->id == 0) // FGCD device
    {
        fgcddev.status.st_warnings |= FGC_WRN_NON_VOLATILE;
    }
    else
    {
        equipdevNonvolWarn(device->id,set);
    }
}

// EOF
