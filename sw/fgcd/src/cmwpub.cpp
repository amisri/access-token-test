/*!
 * @file   cmwpub.cpp
 * @brief  Functions for publishing property data through CMW
 * @author Stephen Page
 */

#include <cstdio>
#include <cstring>
#include <sys/types.h>
#include <unistd.h>

#include <cmw-data/DataFactory.h>
#include <cmw-rda3/server/subscription/SubscriptionCreator.h>
using namespace cmw::data;
using namespace cmw::rda3::common;
using namespace cmw::rda3::server;

#include <cmd.h>
#include <cmdqmgr.h>
#include <cmw.h>
#include <cmwpub.h>
#include <cmwutil.h>
#include <defconst.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <logging.h>
#include <queue.h>
#include <rbac.h>
#include <timing.h>


// Global variables

struct CMWpub                                    cmwpub;
std::multimap<std::string, cmwpub_subscription*> cmwpub_subs[FGCD_MAX_DEVS];



// Static functions

static FGCD_thread_func cmwpubRunPublish;
static FGCD_thread_func cmwpubRunRetrieveData;
static FGCD_thread_func cmwpubRunRecoverSubs;



int32_t cmwpubStart(void)
{
    // Initialise subscription mutex

    if(fgcd_mutex_init(&cmwpub.subscription_mutex, CMW_MUTEX_POLICY) != 0) return 1;

    // Initialise notification and response queues

    if(queueInit(&cmwpub.notification_queue, CMWPUB_MAX_QUEUED_NOTIFICATIONS) != 0 ||
       queueInit(&cmwpub.response_queue, cmdqmgr.num_cmds) != 0)
    {
        // Failed to initialise queue

        fprintf(stderr, "ERROR: cmwpubStart():failed to initialise queues\n");
        fgcddev.status.st_faults |= FGC_FLT_CMW;
        return 1;
    }

    // Set the run flag (used by cmwpubNotify, cmwpubSubscribe and cmwpubUnsubscribe)

    cmwpub.run = 1;

    // Initialise subscription recovery semaphore

    if(sem_init(&cmwpub.recover_subs_sem, 0, 0) < 0)
    {
        perror("sem_init recover_subs sem");
        fgcddev.status.st_faults |= FGC_FLT_CMW;
        return 1;
    }

    // Start CMW publishing thread and CMW retrieve data thread

    if(fgcd_thread_create(&cmwpub.publish_thread,       cmwpubRunPublish,      NULL, CMW_PUBLISH_THREAD_POLICY,       CMW_THREAD_PRIORITY_NORM ,NO_AFFINITY) != 0 ||
       fgcd_thread_create(&cmwpub.retrieve_data_thread, cmwpubRunRetrieveData, NULL, CMW_RETRIEVE_DATA_THREAD_POLICY, CMW_THREAD_PRIORITY_NORM ,NO_AFFINITY) != 0 ||
       fgcd_thread_create(&cmwpub.recover_subs_thread,  cmwpubRunRecoverSubs,  NULL, CMW_RECOVER_SUBS_THREAD_POLICY,  CMW_THREAD_PRIORITY_MIN ,NO_AFFINITY)  != 0)
    {
        fprintf(stderr, "ERROR: cmwpubStart():failed to start threads\n");
        fgcddev.status.st_faults |= FGC_FLT_CMW;
        return 1;
    }

    return 0;
}



uint32_t cmwpubBuildCommandString(const char                 *property,
                                  struct CMWpub_sub_params   *params,
                                  char                       *command)
{
    uint32_t length;

    // Property name

    length = snprintf(command, FGC_MAX_PROP_LEN + 1, "%s", property);

    // Transaction ID

    if(params->transaction_id)
    {
        length += sprintf(&command[length], "<%hu>", params->transaction_id);
    }

    // User

    if(params->user)
    {
        length += sprintf(&command[length], "(%u)", params->user);
    }

    // Array indices and get options

    length += sprintf(&command[length], "[%u,", params->start_index != UNSPECIFIED ?
                                                                       params->start_index : 0);
    if(params->end_index != UNSPECIFIED)
    {
        length += sprintf(&command[length], "%u", params->end_index);
    }
    if(params->step != UNSPECIFIED)
    {
        length += sprintf(&command[length], ",%u", params->step);
    }
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
    length += sprintf(&command[length], "] %s", params->getopt_bin ? "bin" : "type lbl");
#else
    // bin option is forced for subscriptions so we don't need to specify it here.
    // type and lbl options are always required for CMW GETs and subscriptions, but are in fact forced by the device in any case.

    length += sprintf(&command[length], "] type lbl");
#endif

    return length;
}



void cmwpubNotify(uint32_t   channel,
                  const char *property,
                  uint32_t   user,
                  uint32_t   start_index,
                  uint32_t   end_index,
                  uint32_t   notify_parents,
                  int32_t    update_flags)
{
    int e;

    // Do nothing if publication is not running
    if(!cmwpub.run || !cmw_globals.server)
    {
        return;
    }

    cmwpub_notification *notification
            = new cmwpub_notification(channel, property,
                                      user, start_index, end_index,
                                      notify_parents, update_flags);

    // Queue notification

    if ((e = queuePush(&cmwpub.notification_queue, notification)))
    {
        // Free notification

        // Note that sometimes channel == CMWPUB_NOTIFY_ALL_EQUIPDEVS. So, logPrintf should handle this case

        logPrintf(channel, "CMWPUB Notification publication failed (error = %d)\n", e);
        delete notification;
        return;
    }
}



void cmwpubNotifyRecoverSubs(uint32_t channel)
{
    cmwpub.recover_subs[channel] = 1;
    sem_post(&cmwpub.recover_subs_sem);
}



std::pair<uint32_t, Cmd*> cmwpubSubscribe(uint32_t                   channel,
                                          const char                 *property_name,
                                          struct CMWpub_sub_params   *params,
                                          SubscriptionRequest&       request,
                                          void                       *rbac_token)
{
    using namespace std;

    struct Cmd                                              *command{nullptr};
    struct Queue                                            queue;

    // Do nothing if publication is not running

    if(!cmwpub.run) return {FGC_NOT_IMPL, command};

    // Reject transactional subscriptions

    if(params->transaction_id != 0)
    {
        return {FGC_TRANSACTIONAL_SUBSCRIPTIONS_FORBIDDEN, command};
    }

    // Initialise queue

    if(queueInit(&queue, 1))
    {
        // Failed to initialise queue

        logPrintf(0, "CMWPUB Failed to initialise queue\n");
        return {FGC_IO_ERROR, command};
    }

    // Check if the device is a remote one (e.g. FGC3) or FGCD internal device (virtual).
    // If channel is CMWPUB_NOTIFY_ALL_EQUIPDEVS then we check first device (id. 1 - as id 0 is FGCD).
    uint32_t channel_to_check = (channel == CMWPUB_NOTIFY_ALL_EQUIPDEVS ? 1 : channel);
    bool     device_is_remote = fgcd.device[channel_to_check].remote;

    // Calculate 'from' and 'to' to loop over all users (for user == 0) or to execute for a single user.
    // We want to iterate over all users only for internal (virtual) FGCD devices - the remote devices (e.g. FGC3)
    // handle this themselves internally.
    uint32_t loop_from = params->user;
    uint32_t loop_to   = params->user;

    if (params->user == 0 && !device_is_remote)
    {
        loop_from = 1;
        loop_to   = timingUsersCount();

        // The loop can still be broken after one iteration, if the property turns out to be non-PPM property.
        // Before that the command will be constructed with user == 1, but the parser will change that to 0 if the prop isn't PPM.
    }

    SubscriptionCreator* subscription_creator = NULL;

    bool run_only_one_time = false;

    for (uint32_t user = loop_from; user <= loop_to; ++user)
    {
        // Get a command structure from the free queue

        if (!(command = (struct Cmd *) queueBlockPop(&cmdqmgr.free_cmd_q)))
        {
            // Failed to get command structure

            logPrintf(0, "CMWPUB Failed to get command structure\n");
            queueFree(&queue);
            return {FGC_BUSY, command};
        }

        // Configure command

        sprintf(command->device_name, "%u", channel);
        command->command_length = cmwpubBuildCommandString(property_name, params, command->command_string);
        command->rbac_token     = rbac_token;
        command->response_queue = &queue;
        command->transaction_id = params->transaction_id;
        command->type           = CMD_SUB;
        command->user           = params->user;

        cmw_command_data cmd_data{&request, 0};
        cmw_command_data *raw_command_data = &cmd_data;
        // Store the address of the command data in the command's tag
        memcpy(command->tag, &raw_command_data, sizeof(raw_command_data));
        command->tag_length = sizeof(raw_command_data);

        // For internal (virtual) devices we force the user
        if (!device_is_remote)
        {
            command->force_user = user;
        }

        // Queue the command on the device queue
        cmdqmgrQueue(command);
        command = NULL;

        // Wait for the response
        command = (struct Cmd *) queueBlockPop(&queue);

        // Check whether an error occurred

        if (command->error > FGC_OK_RSP)
        {
            uint32_t error = command->error;

            queueFree(&queue);

            logPrintf(channel, "CMWPUB Subscription failed with error (%s)\n", fgc_errmsg[error]);
            return {error, command};
        }

        // Check if we should finish the loop after one run:
        // if the property is not PPM or if the device is a remote one
        if (device_is_remote)
        {
            run_only_one_time = true;
        }
        else
        {
            run_only_one_time = ((command->parser->cmd_prop->flags & PF_PPM) == 0);
        }

        // Check weather the subscription reply has the same user as the request (with exception to 0 (all users))
        // if not unsubscribe, and return an error

        if ((params->user != 0) && (params->user != command->user))
        {
            logPrintf(channel, "CMWPUB Subscription failed (request cycle selector(%u) != reply cycle selector (%u))\n",
                      params->user,
                      command->user);

            queueFree(&queue);
            return {FGC_BAD_CYCLE_SELECTOR, command};
        }

        // We do the below actions only one time (for the first iteration)
        if (user == loop_from)
        {
            // Get the subscription creator and accept the request
            subscription_creator = &request.accept();

            // Validate the publication
            subscription_creator->startPublishing();
        }

        // Send the first update to the client if it has content
        if (command->value_length && subscription_creator != NULL)
        {
            // Send first update
            AcquiredData *value;

            if ((value = cmwutilResponseToRdaData(command)))
            {
                subscription_creator->firstUpdate(*value);
                delete value;
            }
        }

        // Return the command structure
        queuePush(&cmdqmgr.free_cmd_q, command);
        command = nullptr;

        // Break the loop if the property was in fact not PPM or the device is a remote device
        if (run_only_one_time)
        {
            break;
        }
    }

    // Delete RBAC token
    if(rbac_token)
    {
        rbac::Token *token = static_cast<rbac::Token *>(rbac_token);
        delete token;
    }

    // Free the queue
    queueFree(&queue);

    return {0, nullptr};
}



uint32_t cmwpubUnsubscribe(uint32_t                     channel,
                           const char                   *property_name,
                           struct CMWpub_sub_params     *params,
                           const SubscriptionSource&    source)
{
    using namespace std;

    struct Cmd                                              *command;
    char                                                    command_to_cancel[CMD_MAX_LENGTH + 1] = "";
    pair<multimap<string, cmwpub_subscription *>::iterator,
         multimap<string, cmwpub_subscription *>::iterator> range;
    multimap<string, cmwpub_subscription *>::iterator       sub_iter;
    cmwpub_subscription                                     *subscription = NULL;

    // Do nothing if publication is not running

    if(!cmwpub.run) return 1;

    // Get a command structure

    if(!(command = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q)))
    {
        // Failed to get command structure

        logPrintf(0, "CMWPUB Failed to get command structure to cancel command [%u:%s]\n",
                  channel, command_to_cancel);
        return FGC_BUSY;
    }

    // Search for a subscription matching the request

    pthread_mutex_lock(&cmwpub.subscription_mutex);

    range = cmwpub_subs[channel].equal_range(property_name);
    for(sub_iter = range.first ; sub_iter != range.second ; sub_iter++)
    {
        subscription = sub_iter->second;

        // Check whether the source was found

        if(subscription->source->getId() == source.getId())
        {
            // Remove the subscription from the subscription set

            cmwpub_subs[channel].erase(sub_iter);

            // Check whether there are no remaining subscriptions to the property

            if(cmwpub_subs[channel].count(property_name) == 0)
            {
                // Store the command to cancel the subscription

                strcpy(command_to_cancel, subscription->command);
            }

            // Free the subscription

            delete subscription;

            // Stop searching as the subscription has been found

            // Stop searching as the subscription and listener have been found

            break;
        }
        else // The listener was not found within the subscription set
        {
            subscription = NULL;
        }
    }
    pthread_mutex_unlock(&cmwpub.subscription_mutex);

    // Check whether the subscription needs to be cancelled

    if(fgcd.device[channel].remote && command_to_cancel[0] != '\0')
    {
        // Configure the command

        sprintf(command->device_name, "%u", channel);
        command->command_length = sprintf(command->command_string, command_to_cancel);
        command->rbac_token     = CMD_FGCD_CLIENT;
        command->response_queue = &cmdqmgr.free_cmd_q;
        command->type           = CMD_UNSUB;

        // Queue the command

        cmdqmgrQueue(command);
        command = NULL;
    }
    else // No subscription to cancel
    {
        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;
    }

    return 0;
}



// Static functions

/*
 * Thread to publish data to subscribers
 */

static void *cmwpubRunPublish(void *unused)
{
    using namespace std;

    struct Cmd                                             *command;
    uint32_t                                                i;
    char                                                    property[FGC_MAX_PROP_LEN + 1];
    pair<multimap<string, cmwpub_subscription *>::iterator,
         multimap<string, cmwpub_subscription *>::iterator> range;
    multimap<string, cmwpub_subscription *>::iterator       sub_iterator;
    cmwpub_subscription                                    *subscription;
    UpdateType                                              update_type;

    while(true)
    {
        if((command = reinterpret_cast<struct Cmd*>(queueBlockPop(&cmwpub.response_queue))) == NULL)
        {
            logPrintf(0, "cmwpubRunPublish() could not read response queue, exiting.\n");
            break;
        }

        // The remaining count for the command is overloaded here to indicate that the command
        // was counted by cmwpub.fgcd_cmd_count

        // Published data responses from FGCs will have 0 in this field

        if(command->remaining)
        {
            // Decrement count of outstanding FGCD commands

            cmwpub.fgcd_cmd_count--;
        }

        // Extract the property name from the command

        for(i = 0 ; i <= FGC_MAX_PROP_LEN ; i++)
        {
            switch(command->command_string[i])
            {
                case '\0':
                case ' ':
                case '<':
                case '(':
                case '[':
                    property[i] = '\0';
                    break;

                default:
                    property[i] = command->command_string[i];
                    break;
            }

            if(property[i] == '\0')
            {
                break;
            }
        }
        property[sizeof(property) - 1] = '\0';

        // Lock subscriptions mutex

        pthread_mutex_lock(&cmwpub.subscription_mutex);

        // Search for subscriptions matching the request

        range = cmwpub_subs[command->device->id].equal_range(property);
        for(sub_iterator = range.first ; sub_iterator != range.second ; sub_iterator++)
        {
            subscription = sub_iterator->second;

            // Check whether the subscription matches the request

            if(command->device->remote) // Remote device (e.g. an FGC)
            {
                uint32_t user       = subscription->params.user;
                uint32_t sub_device = subscription->params.sub_device;

                if((command->user || command->error <= FGC_OK_RSP) &&                                    // Update is PPM or not an error
                   ((user                != 0                             && user       != command->user) ||      // Subscription is PPM and the user does not match the update
                    (command->sub_device != CMWPUB_NOTIFY_ALL_SUB_DEVICES && sub_device != command->sub_device))) // The sub-device does not match the update
                {
                    // Subscription does not match the request

                    continue;
                }
            }
            else // Device handled directly by FGCD
            {
                if(strcmp(subscription->command, command->command_string))
                {
                    // Subscription does not match the request

                    continue;
                }
            }

            // Check that the user coming from the device is meaningful (either 0 or an existing user on this timing network)

            if(command->user && strlen(timingUserName(command->user)) == 0)
            {
                continue;
            }

            // Set update type

            update_type = command->cmw_update_flags & CMWPUB_UPDATE_FLAGS_FIRST     ? UT_FIRST_UPDATE     :
                          command->cmw_update_flags & CMWPUB_UPDATE_FLAGS_IMMEDIATE ? UT_IMMEDIATE_UPDATE :
                          UT_NORMAL;

            // Send value to the subscribers

            // Check whether an error has occurred

            if(command->error > FGC_OK_RSP) // An error has occurred
            {
                subscription->source->notify(ServerException(std::string("FGC Device: ") + fgc_errmsg[command->error]), update_type);
            }
            else // No error has occurred
            {
                AcquiredData *value;

                if((value = cmwutilResponseToRdaData(command)))
                {
                    // Return value to subscribers
                    subscription->source->notify(*value, update_type);
                    delete value;
                }
                else // Failed to convert the response to RDA data
                {
                    // Return exception to subscribers if it is not an unknown cycle error

                    // cmwutilResponseToRdaData() will set an unknown cycle error for values returned by FGCs for
                    // user numbers that do not have corresponding names

                    if(command->error != FGC_UNKNOWN_CYCLE)
                    {
                        subscription->source->notify(ServerException(std::string("FGC Device: ") + fgc_errmsg[command->error]), update_type);
                    }
                }
            }
        }

        pthread_mutex_unlock(&cmwpub.subscription_mutex);

        // Return command structure to the free queue

        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;
    }

    return 0;
}



/*
 * Thread to process notifications and retrieve data for subscriptions
 */

static void *cmwpubRunRetrieveData(void *unused)
{
    using namespace std;

    uint32_t                                                channel;
    uint32_t                                                channel_end;
    uint32_t                                                channel_start;
    struct Cmd                                              *command;
    char                                                    *delimiter;
    multimap<string, cmwpub_subscription *>::iterator       iterator;
    cmwpub_notification                                     *notification;
    struct CMWpub_sub_params                                *params;
    char                                                    property[FGC_MAX_PROP_LEN + 1];
    pair<multimap<string, cmwpub_subscription *>::iterator,
         multimap<string, cmwpub_subscription *>::iterator> range;
    cmwpub_subscription                                     *subscription;

    while(true)
    {
        if((notification = reinterpret_cast<cmwpub_notification*>(queueBlockPop(&cmwpub.notification_queue))) == NULL)
        {
            logPrintf(0, "cmwpubRunRetrieveData() could not read notification queue, exiting.\n");
            break;
        }

        // Copy property string from notification

        strncpy(property, notification->property, sizeof(property));
        property[sizeof(property) - 1] = '\0';

        // Clear pointer to last '.' character in property

        delimiter = NULL;

        do
        {
            // Terminate property at last '.' delimiter if not NULL

            if(delimiter) *delimiter = '\0';

            // Set range of channels to notify

            if(notification->channel == CMWPUB_NOTIFY_ALL_EQUIPDEVS)
            {
                channel_start   = 1;
                channel_end     = FGCD_MAX_EQP_DEVS;
            }
            else // Notify only one device
            {
                channel_start = channel_end = notification->channel;
            }

            // Notify each channel

            for(channel = channel_start ; channel <= channel_end ; channel++)
            {
                // Skip channel if it is unused

                if(!fgcd.device[channel].name) continue;

                // Lock the subscription map

                pthread_mutex_lock(&cmwpub.subscription_mutex);

                // Update subscriptions matching the notification

                range = cmwpub_subs[channel].equal_range(property);
                for(iterator = range.first ; iterator != range.second ; iterator++)
                {
                    subscription    = iterator->second;
                    params          = &subscription->params;

                    // Check whether the subscription is affected by the update
                    // params->user == 0 - it means that the subscription is for all users

                    if ( (params->user        == notification->user      || params->user == 0)                   &&
                         (params->start_index <= notification->end_index || params->start_index == UNSPECIFIED)  &&
                          params->end_index   >= notification->start_index)
                    {
                        // Get a command structure from the free queue

                        if(!(command = (struct Cmd*)queuePop(&cmdqmgr.free_cmd_q)))
                        {
                            // Failed to get command structure

                            logPrintf(channel,
                                "CMWPUB Publication %s lost (Failed to retrieve free command structure)\n",
                                subscription->command);

                            continue;
                        }

                        // Configure command

                        sprintf(command->device_name, "%u", channel);
                        command->client             = CMD_FGCD_CLIENT;
                        command->command_length     = sprintf(command->command_string, subscription->command);
                        command->force_user         = notification->user;
                        command->response_queue     = &cmwpub.response_queue;
                        command->cmw_update_flags   = notification->update_flags;
                        command->type               = CMD_GET;

                        // The remaining count for the command is overloaded here to indicate that the command
                        // was counted by cmwpub.fgcd_cmd_count

                        // Published data responses from FGCs will have 0 in this field

                        command->remaining = 1;

                        // Queue command on device queue

                        cmwpub.fgcd_cmd_count++;
                        cmdqmgrQueue(command);
                        command = NULL;
                    }
                }

                // Unlock the subscription map

                pthread_mutex_unlock(&cmwpub.subscription_mutex);

            }

        } while(notification->notify_parents && (delimiter = strrchr(property, '.')));

        // Free notification

        delete notification;
    }

    return 0;
}



/*
 * Thread to recover subscriptions when a remote device comes back online
 */

static void *cmwpubRunRecoverSubs(void *unused)
{
    using namespace std;

    uint32_t                                            channel;
    struct Cmd                                          *command;
    multimap<string, cmwpub_subscription *>::iterator   iterator;
    struct Queue                                        queue;
    cmwpub_subscription                                 *subscription;

    while(true)
    {
        pthread_testcancel();
        sem_wait(&cmwpub.recover_subs_sem);
        pthread_testcancel();

        // Initialise queue

        if(queueInit(&queue, 1))
        {
            // Failed to initialise queue

            logPrintf(0, "CMWPUB Failed to initialise subscription recovery queue\n");
            return 0; // return value is not checked
        }

        // Get a command structure from the free queue

        if(!(command = (struct Cmd*)queueBlockPop(&cmdqmgr.free_cmd_q)))
        {
            // Failed to get command structure

            queueFree(&queue);
            return 0; // return value is not checked
        }
        command->response_queue = &queue;
        command->client         = CMD_FGCD_CLIENT;
        command->type           = CMD_SUB;

        // Find channels to recover

        // Note that the subscription mutex is deliberately unlocked between actions in order to
        // give the publication thread a chance to process updates

        for(channel = 0 ; channel < FGCD_MAX_DEVS ; channel++)
        {
            if(cmwpub.recover_subs[channel])
            {
                cmwpub.recover_subs[channel] = 0;

                // Flag all of the channel's subscriptions as requiring recovery

                pthread_mutex_lock(&cmwpub.subscription_mutex);

                for(iterator  = cmwpub_subs[channel].begin() ;
                    iterator != cmwpub_subs[channel].end()   ; iterator++)
                {
                    subscription = iterator->second;
                    subscription->recover = 1;
                }

                pthread_mutex_unlock(&cmwpub.subscription_mutex);

                // Recover each of the channel's subscriptions

                while(1)
                {
                    // Find next subscription to be recovered

                    pthread_mutex_lock(&cmwpub.subscription_mutex);

                    for(iterator  = cmwpub_subs[channel].begin() ;
                        iterator != cmwpub_subs[channel].end();
                        iterator++)
                    {
                        subscription = iterator->second;

                        if(subscription->recover)
                        {
                            break;
                        }

                        subscription = NULL;
                    }

                    // If there are more subscriptions to be recoverd for this channel, then go to the next one

                    if (subscription == NULL)
                    {
                        pthread_mutex_unlock(&cmwpub.subscription_mutex);

                        break;
                    }

                    // Start the recovery process for this specific subscription

                    logPrintf(channel, "CMWPUB Recovering subscription (%s)\n", subscription->command);

                    // Configure command

                    sprintf(command->device_name, "%u", channel);
                    command->command_length = sprintf(command->command_string, subscription->command);

                    // Queue command on device queue

                    cmdqmgrQueue(command);
                    command = NULL;

                    // Clear the subscription recover request and
                    // set the flag to indicate that a subscription was recovered

                    subscription->recover   = 0;

                    pthread_mutex_unlock(&cmwpub.subscription_mutex);

                    // Wait for the response

                    command = (struct Cmd *)queueBlockPop(&queue);

                    // Check if the subscription failed

                    if(command->error > FGC_OK_RSP)
                    {
                        uint32_t error = command->error;

                        logPrintf(channel, "CMWPUB Subscription recovery failed: %s\n", fgc_errmsg[error]);
                    }

                }

            }
        }

        // Return the command structure

        queuePush(&cmdqmgr.free_cmd_q, command);
        command = NULL;

        // Free the queue

        queueFree(&queue);
    }

    return 0;
}

// EOF
