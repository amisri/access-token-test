/*!
 * @file   timing.c
 * @brief  Functions for timing
 * @author Stephen Page
 */

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <set>
#include <vector>
#include <string>
#include <sys/types.h>
#include <unistd.h>

#include <consts.h>
#include <defconst.h>
#include <definfo.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <logging.h>
#include <timing.h>
#include <timing_handlers.h>
#include <transaction.h>
#include <fgcddebug.h>

// Global variables

struct TimingFgcd timing;


// Static functions

static FGCD_thread_func timingRun;

static int32_t timingReadUserLines(void);
static int32_t timingReadEventFile(const char * filename,FILE *file);
static void    timingMSTimerEvent(union sigval value);
static int32_t timingReadEventSubscriptions();
static int32_t timingStartPosixTimer(timer_t *timer, struct itimerspec *timer_spec, struct sigevent *event);
static int32_t timingWaitForOneTelegram();


int32_t timingInit(void)
{
    // Initialise time mutex

    if(fgcd_mutex_init(&timing.user_time_mutex, TIMING_MUTEX_POLICY) != 0) return 1;

    // Set the domain string

    timing.domain_str = fgcd_machine_name[fgcd.machine];

    // Initialise timing

    switch(timing.source)
    {
        case TIMING_SYS:

            break;

        case TIMING_TIMLIB:

            // Disable the logging of events by default

            timing.log_events_flag = 0;


            // Instantiate CTR handle

            timing.ctr_handle = ctr_open(NULL);

            if(timing.ctr_handle == reinterpret_cast<void*>(CTR_ERROR))
            {
                fprintf(stderr,"ERROR: Failed to open CTR (errno=%d): %s\n", errno, strerror(errno));
                fgcddev.status.st_faults |= FGC_FLT_TIMING;
                return 1;
            }

            // Get the list of existing devices (cross check)

            CtrDrvrPtimObjects ltims;

            if(ctr_list_ltim_objects(timing.ctr_handle, &ltims) < 0)
            {
                fprintf(stderr,"ERROR: TIMING Failed to retrieve ltim objects: ctr_list_ltim_objects() (errno=%d): %s\n", errno, strerror(errno));
                fgcddev.status.st_faults |= FGC_FLT_TIMING;
                return 1;
            }

            // Intialize timing

            try
            {
                // Set the machine ID for timing

                timing.connection = new Timing::ConnectionContext();

                // Set Queue

                timing.connection->setQueue(true);

                // Wait forever

                timing.connection->setTimeOut(0);
            }
            catch (std::exception& e)
            {
                fprintf(stderr,"ERROR: Timing exception: %s\n", e.what());
                timing.source = TIMING_SYS;
                fgcddev.status.st_faults |= FGC_FLT_TIMING;
                return 1;
            }

            // Instantiate timing domain

            try
            {
                timing.domain = new Timing::Domain(fgcd_machine_name[fgcd.machine]);

                // Only atempt to get the USER information if the timing.domain exists

                if(timingReadUserLines())
                {
                    return 1;
                }

            }
            catch (std::exception& e)
            {
                fprintf(stderr,"WARNING: Timing exception: %s\n", e.what());
                return 0;
            }

            // Read Event subscriptions

            if(timingReadEventSubscriptions())
            {
                return 1;
            }

            // Wait for the first telegram

            if(timingWaitForOneTelegram())
            {
                return 1;
            }

            break;

        default: // Invalid time source
            fprintf(stderr, "ERROR: Invalid time source\n");

            timing.source = TIMING_SYS;
            fgcddev.status.st_faults |= FGC_FLT_TIMING;
            return 1;
    }

    return 0;
}


static int32_t timingReadEventSubscriptions()
{
    char                        filename[FILENAME_MAX + 1];
    FILE *                      file = NULL;
    // Do nothing if timing source is not timing library

    if(timing.source != TIMING_TIMLIB)
    {
        return 0;
    }

    // Try to read the host specific file if exist

    snprintf(filename, sizeof(filename), "%s/%s",
             TIMING_HOST_EVENT_PATH, fgcd.hostname);

    if((file = fopen(filename, "r")) != NULL)
    {
        if(timingReadEventFile(filename,file))
        {
            fprintf(stderr, "ERROR: Failed to read host event file %s\n", filename);
            fclose(file);
            fgcddev.status.st_faults |= FGC_FLT_CONFIG;
            return 1;
        }
        else
        {
            fclose(file);
            return 0;
        }
    }

    // Try to read generic class/timing_domain file

    snprintf(filename, sizeof(filename), "%s/%s/%s",
             TIMING_CLASS_EVENT_PATH, FGC_CLASS_ID_STRING, fgcd_machine_name[fgcd.machine]);

    if((file = fopen(filename, "r")) != NULL)
    {
        if(timingReadEventFile(filename,file))
        {
            fprintf(stderr, "ERROR: Failed to read generic class/accelerator event file %s\n", filename);
            fclose(file);
            fgcddev.status.st_faults |= FGC_FLT_CONFIG;
            return 1;
        }
        else
        {
            fclose(file);
            return 0;
        }
    }

    fprintf(stderr, "ERROR: Configuration file for timing not found at %s/%s nor at %s/%s/%s\n",
        TIMING_HOST_EVENT_PATH, fgcd.hostname,
        TIMING_CLASS_EVENT_PATH, FGC_CLASS_ID_STRING, fgcd_machine_name[fgcd.machine]);
    return 1;
}

void timingTelegramInit(void)
{
    // Zero telegram values

    memset(timing.field_value, 0, sizeof(timing.field_value));

    switch(fgcd.machine)
    {
        case MACHINE_ADE:
        case MACHINE_CPS:
        case MACHINE_LEI:
        case MACHINE_LN4:
        case MACHINE_LNA:
        case MACHINE_PSB:
        case MACHINE_SPS:
            timing.field_value[TIMING_FIELD_CYCLE_LEN]        = 0xFFFFFFFF; // Set cycle length to indicate unknown
            timing.field_value[TIMING_FIELD_COMBINED_LINE]    = 0x00000000; // Set the number of injections to 0
            timing.field_value[TIMING_FIELD_NEXT_CYCLE_LEN]   = 0xFFFFFFFF; // Set next cycle length to indicate unknown
            timing.field_value[TIMING_FIELD_NEXT_DEST]        = 0xFFFFFFFF; // Set next destination to indicate unknown
            timing.field_value[TIMING_FIELD_NEXT_USER]        = 0xFFFFFFFF; // Set next user to indicate unknown
            timing.field_value[TIMING_FIELD_USER]             = 0xFFFFFFFF; // Set user to indicate unknown
            timing.field_value[TIMING_FIELD_BP_DURATION_MS]   = 1200;       // Set efault basic period
            timing.field_value[TIMING_FIELD_CYCLE_LEN]        = 1;          // Set cycle length to constant 1 basic period
            timing.field_value[TIMING_FIELD_NEXT_CYCLE_LEN]   = 1;          // Set next cycle length to constant 1 basic period
            timing.field_value[TIMING_FIELD_OP_MODE]          = 0;          // Non-operational by default
            timing.field_value[TIMING_FIELD_INJECTED_BATCHES] = 0;          // Number of injected batches et to 0
            timing.field_value[TIMING_FIELD_PC_PERMIT]        = 0x000000FF; // set default PC PERMIT
            timing.field_value[TIMING_FIELD_SECTOR_ACCESS]    = 0x0;        // No access to the machine by default
            break;

        case MACHINE_LHC:
            timing.field_value[TIMING_FIELD_USER]      = timingUserNumber("LHC"); // Set user to LHC
            timing.field_value[TIMING_FIELD_PC_PERMIT] = 0x000000FF;              // set default PC PERMIT
            break;

        default:
            fprintf(stderr, "WARNING: timingTelegramInit(\"%s\") not implemented\n",fgcd_machine_name[fgcd.machine]);
            break;
    }
}

static int32_t timingWaitForOneTelegram()
{
    struct Timing_event_handler *handler;
    uint32_t                    i;
    Timing::EventValue          value;

    // Initialize field values

    timingTelegramInit();

    // Search for the TELEGRAM handler

    for(i = 0 ;  timing_event_handlers[i].function && strcmp(timing_event_handlers[i].name,timing_handler_names[TIMING_HANDLER_TELEGRAM]); i++);

    if(!timing_event_handlers[i].function)
    {
        fprintf(stderr,"ERROR: Missing telegram handler\n");

        return 1;
    }

    handler = &timing_event_handlers[i];

    // If the handler is set

    if(!handler->timing_events.empty())
    {
        for (const auto& timing_event : handler->timing_events)
        {
            try
            {
                // Timing_event.first is the name of the timing event
                timing.connection->connect(timing_event.first);

                // Wait for 10 seconds maximum for the first telegram to arrive

                timing.connection->setTimeOut(10000);

                timing.connection->wait(&value);

                // Disconnect the telegram, it will be re-connected during the initialization of the subscriptions

                timing.connection->disconnect(timing_event.first);

                handler->function(handler, timing_event.second, value.getHwTimestamp(), &value);

            }
            catch (std::exception& e)
            {
                fprintf(stderr, "WARNING: Failed to connect to TELEGRAM '%s': %s\n", timing_event.first.c_str(),
                        e.what());
            }
        }
    }

    return 0;

}

int32_t timingStart(void)
{
    // Initialise timing

    if(timingInit() != 0) return 1;

    // Start timing thread

    return fgcd_thread_create(&timing.thread, timingRun, NULL, TIMING_THREAD_POLICY, TIMING_THREAD_PRIORITY, TIMING_THREAD_AFFINITY);
}

int32_t timingSubscribeEvents(void)
{
    struct Timing_event_handler *handler;
    uint32_t                    i;

    // Do nothing if timing source is not timing library

    if(timing.source != TIMING_TIMLIB)
    {
        if(timingStartMSTimer())
        {
            fprintf(stderr,"ERROR: Failed to start millisecond POSIX timer\n");
            return 1;
        }
        else
        {
            return 0;
        }
    }

    // Connect to events for timing handlers

    for(i = 0 ; timing_event_handlers[i].function ; i++)
    {
        handler = &timing_event_handlers[i];

        // Attach to event for handler if one is defined

        if(!handler->timing_events.empty())
        {
            for (const auto& timing_event : handler->timing_events)
            {
                try
                {
                    timing.connection->connect(timing_event.first);
                }
                catch(std::exception& e)
                {
                    fprintf(stderr, "WARNING: Failed to connect to timing event '%s': %s\n", timing_event.first.c_str(),e.what());
                    fgcddev.status.st_warnings |= FGC_WRN_TIMING;
                    //return 1;
                }
            }
        }
    }
    return 0;
}



void timingGetUserTime(uint32_t user, struct timeval *time)
{
    pthread_mutex_lock(&timing.user_time_mutex);
    *time = timing.user_time[user];
    pthread_mutex_unlock(&timing.user_time_mutex);
}



void timingSetUserTime(uint32_t user, const struct timeval *time)
{
    pthread_mutex_lock(&timing.user_time_mutex);
    timing.user_time[user] = *time;
    pthread_mutex_unlock(&timing.user_time_mutex);
}



void timingGetLastUserTime(uint32_t user, struct timeval *time)
{
    pthread_mutex_lock(&timing.user_time_mutex);
    *time = timing.last_user_time[user];
    pthread_mutex_unlock(&timing.user_time_mutex);
}



void timingSetLastUserTime(uint32_t user, const struct timeval *time)
{
    pthread_mutex_lock(&timing.user_time_mutex);
    timing.last_user_time[user] = *time;
    pthread_mutex_unlock(&timing.user_time_mutex);
}



int32_t timingReadTime(struct timeval *time)
{
    tTimingTime  timdt_time;
    static int   fault      = 0;

    switch(timing.source)
    {
        case TIMING_SYS: // Use system time

            gettimeofday(time, NULL);

            break;

        case TIMING_TIMLIB: // Use timing library
            try
            {
                timdt_time = timing.connection->getUTCTime();
                time->tv_sec    = timdt_time.time.tv_sec;
                time->tv_usec   = timdt_time.time.tv_nsec / 1000;

                if(fault)
                {
                    fault = 0;
                    fgcddev.status.st_faults &= ~FGC_FLT_TIMING;
                    logPrintf(0, "TIMING Fault cleared: ConnectionContext::getUTCTime() succeeded\n");
                }

                timingCheckStatus();
            }
            catch(std::exception& e)
            {
                if(!fault) // Timing fault was not yet set
                {
                    fault = 1;
                    fgcddev.status.st_faults |= FGC_FLT_TIMING;
                    logPrintf(0, "TIMING Fault reading time: ConnectionContext::getUTCTime() failed: %s\n", e.what());
                }
                return 1;

            }

            break;

        default: // Invalid time source

            logPrintf(0, "TIMING Invalid source\n");
            return 1;
    }
    return 0;
}



int32_t timingCheckStatus(void)
{
    static int     warning     = 0;
    static int     fault       = 0;
    CtrDrvrStatus  status;
    const uint32_t CTR_STATUS_OK =  CtrDrvrStatusGMT_OK |
                                    CtrDrvrStatusPLL_OK |
                                    CtrDrvrStatusSELF_TEST_OK |
                                    CtrDrvrStatusENABLED |
                                    CtrDrvrStatusNO_LOST_INTERRUPTS |
                                    CtrDrvrStatusNO_BUS_ERROR;

    // Do nothing if timing source is not timing library

    if(timing.source != TIMING_TIMLIB)
    {
        return 0;
    }

    // Check CTR status

    if(ctr_get_status(timing.ctr_handle,&status) < 0)
    {
        if(!fault)
        {
            fault = 1;
            fgcddev.status.st_faults |= FGC_FLT_TIMING;
            logPrintf(0, "TIMING Fault set: ctr_get_status() failed (errno =%d): %s",errno,strerror(errno));
        }
        return 1;
    }
    else if(fault)
    {
        fault = 0;
        fgcddev.status.st_faults &= ~FGC_FLT_TIMING;
        logPrintf(0,"TIMING Fault cleared: ctr_get_status() call succeeded\n");
    }

    // Check status word

    if((status & CTR_STATUS_OK) != CTR_STATUS_OK)
    {
        if(!warning || static_cast<uint32_t>(status) != timing.ctr_status)
        {
            warning = 1;
            fgcddev.status.st_warnings |= FGC_WRN_TIMING;
            logPrintf(0, "TIMING Warning set: ctr_get_status() = 0x%02X: %s %s %s %s %s %s\n",
                status,
                (CtrDrvrStatusGMT_OK & status)?"GMT_OK":"GMT_ERROR",
                (CtrDrvrStatusPLL_OK & status)?"PLL_OK":"PLL_ERROR",
                (CtrDrvrStatusSELF_TEST_OK & status)?"SELT_TEST_OK":"SELF_TEST_ERROR",
                (CtrDrvrStatusENABLED & status)?"ENABLED_OK":"ENABLED_ERROR",
                (CtrDrvrStatusNO_LOST_INTERRUPTS & status)?"LOST_INTERRUPTS_OK":"LOST_INTERRRUPTS_ERROR",
                (CtrDrvrStatusNO_BUS_ERROR & status)?"BUS_OK":"BUS_ERROR"
                );
        }

        timing.ctr_status = static_cast<uint32_t>(status);

        return 1;
    }
    else if(warning)
    {
        warning = 0;
        fgcddev.status.st_warnings &= ~FGC_WRN_TIMING;
        logPrintf(0,"TIMING Warning cleared: ctr_get_status() succeeded\n");
    }

    timing.ctr_status = static_cast<uint32_t>(status);

    return 0;
}



int32_t timingReceiveEvents(void)
{
    uint32_t num_evts;

    // Check that the timing source is timing library

    if(timing.source != TIMING_TIMLIB) return 1;

    // Process queued timing events

    for(num_evts = timing.connection->getQueueSize() ; num_evts ; num_evts--)
    {
        timingReceiveEvent();
    }
    return 0;
}

int32_t timingStartMSTimer(void)
{
    struct sigevent       event;          // Event configuration
    struct itimerspec     timer_spec;     // Timer specification
    int32_t               pthread_error;
    pthread_attr_t        thread_attr;
    struct sched_param    thread_param;
    uint32_t              handler_index;

    // Check that the timing source is the system time

    if(timing.source != TIMING_SYS)
    {
        fprintf(stderr, "ERROR: POSIX timer can only be used with system timing\n");
        return 1;
    }

    // Check that the POSIXMS handler is defined

    for(handler_index = 0 ;
        timing_event_handlers[handler_index].function                                                   &&
        strcmp(timing_handler_names[TIMING_HANDLER_POSIXMS],timing_event_handlers[handler_index].name)
        ; handler_index++);

    if(!timing_event_handlers[handler_index].function)
    {
        fprintf(stderr, "ERROR: TIMING Event \"POSIXEVENT\" has no associated action\n");
        return 1;
    }

    // Check that POSIX timer is not already in use

    if(timing.posix_timer_in_use)
    {
        fprintf(stderr, "ERROR: POSIX timer already in use\n");
        return 1;
    }

    // Configure thread attributes

    if((pthread_error = pthread_attr_init(&thread_attr)) != 0)
    {
        fprintf(stderr, "ERROR: pthread_attr_init failed with error %d\n", pthread_error);
        return 1;
    }

    pthread_attr_setinheritsched(   &thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(    &thread_attr, SCHED_FIFO);
    pthread_attr_setstacksize(      &thread_attr, PTHREAD_STACK_SIZE);

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = TIMING_THREAD_PRIORITY;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    // Configure event handler

    event.sigev_notify              = SIGEV_THREAD;
    event.sigev_notify_attributes   = &thread_attr;
    event.sigev_notify_function     = &timingMSTimerEvent;
    event.sigev_value.sival_ptr     = &timing.posix_timer;

    // Configure timer specification

    timer_spec.it_value.tv_sec      = 0;
    timer_spec.it_value.tv_nsec     = 1000000;  // 1ms
    timer_spec.it_interval.tv_sec   = 0;
    timer_spec.it_interval.tv_nsec  = 1000000;  // 1ms

    // Start the timer

    if(timingStartPosixTimer(&timing.posix_timer, &timer_spec, &event))
    {
        // Failed to start timer

        pthread_attr_destroy(&thread_attr);
        return 1;
    }
    pthread_attr_destroy(&thread_attr);

    // Flag POSIX timer as in use

    timing.posix_timer_in_use = 1;

    return 0;
}

int32_t timingReceiveEvent(void)
{
    static Timing::EventValue    evt_value;
    uint32_t                     handler_index;

    // Check the that timing source is the timing library

    if(timing.source != TIMING_TIMLIB) return 1;

    try
    {
        timing.connection->waitAll(&evt_value);
    }
    catch(std::exception& e)
    {
        // Wait a bit and try again

        usleep(100);

        return 1;
    }

    // Find corresponding handler index and extract the additional info about the timing event
    Timing_event_info event_info;

    for (handler_index = 0; timing_event_handlers[handler_index].function; handler_index++)
    {
        // Try to find the timing event in this handler
        auto found = timing_event_handlers[handler_index].timing_events.find(evt_value.getName());

        if (found != timing_event_handlers[handler_index].timing_events.end())
        {
            // Extract additional info
            event_info = found->second;
            break;
        }
    }

    // Check that we are not out of bounds

    if(!timing_event_handlers[handler_index].function)
    {
        logPrintf(0,"TIMING Event '%s' not found in timing handlers\n",evt_value.getName().c_str());
        return 1;
    }

    // Log reception of event

    if(timing.log_events_flag &&
       strcmp(timing_handler_names[TIMING_HANDLER_MS], timing_event_handlers[handler_index].name)) // Not millisecond event
    {
        try
        {
            Timing::Value value;

            evt_value.getFieldValue("USER", &value);

            logPrintf(0, "TIMING Event '%s', user = %s, timestamp %lu.%03lu, cycle time %lu ms\n",
                      evt_value.getName().c_str(),
                      value.getAsString().c_str(),
                      evt_value.getHwTimestamp().time.tv_sec,
                      evt_value.getHwTimestamp().time.tv_nsec / 1000000,
                      evt_value.getCycleTimestamp());
        }
        catch(...)
        {
            // If the field value USER and/or the cycle stamp are not present

            logPrintf(0, "TIMING Event '%s', user = NONE, timestamp %lu.%03lu, cycle time = NONE\n",
                      evt_value.getName().c_str(),
                      evt_value.getHwTimestamp().time.tv_sec,
                      evt_value.getHwTimestamp().time.tv_nsec / 1000000);
        }
    }

    // Get the event's timestamp

    tTimingTime evt_time = evt_value.getHwTimestamp();

    // Discard sub-millisecond precision from CTIM timestamps

    // It is only useful to timestamp CTIMs to the millisecond as the
    // sub-millisecond part comes from scheduling and transmission delays

    Timing::EventDescriptor *event_descriptor = evt_value.getEventDesc();
    if(event_descriptor->getClass() == kTIMING_EVENT_CTIM)
    {
        evt_time.time.tv_nsec = (evt_time.time.tv_nsec / 1000000) * 1000000;
    }
    delete(event_descriptor);

    // Set the last time that a user was seen for telegram ready events

    if(!strcmp(timing_handler_names[TIMING_HANDLER_TELEGRAM], timing_event_handlers[handler_index].name))
    {
        try
        {
            // Extract the USER field

            Timing::Value value;
            evt_value.getFieldValue("USER", &value);
            uint16_t user = value.getAsUshort();

            // Set the last time that the user was seen

            if(user <= FGC_TIMING_NUM_USERS)
            {
                struct timeval time;
                time.tv_sec  = evt_time.time.tv_sec;
                time.tv_usec = evt_time.time.tv_nsec / 1000;

                timingSetLastUserTime(user, &time);
            }
        }
        catch(...)
        {
        }
    }

    // Execute the event handler

    timing_event_handlers[handler_index].function(&timing_event_handlers[handler_index], event_info, evt_time, &evt_value);

    return 0;
}



struct timeval timingCompareTimevals(struct timeval *a, struct timeval *b)
{
    struct timeval result;

    result.tv_sec   = a->tv_sec  - b->tv_sec;
    result.tv_usec  = a->tv_usec - b->tv_usec;

    if(result.tv_usec < 0)
    {
        result.tv_sec   -= 1;
        result.tv_usec  += 1000000;
    }

    return result;
}

struct timespec timingCompareTimespecs(struct timespec *a, struct timespec *b)
{
    struct timespec result;

    result.tv_sec   = a->tv_sec  - b->tv_sec;
    result.tv_nsec  = a->tv_nsec - b->tv_nsec;

    if(result.tv_nsec < 0)
    {
        result.tv_sec   -= 1;
        result.tv_nsec  += 1000000000;
    }

    return result;
}


const char *timingUserName(uint32_t user_num)
{
    uint8_t i;
    static const char* EMPTY_STR = "";

    // Return 0 if timing source is not timing library or the user hash has not been initialized

    if(timing.source != TIMING_TIMLIB || !timing.user_line_entry)
    {
        return EMPTY_STR;
    }

    // Iterate while there are contents in the table

    for(i = 0; i != FGC_TIMING_NUM_USERS && timing.user_line_entry[i].key.c[0] != 0; ++i)
    {
        if(timing.user_line_entry[i].index == user_num)
        {
            return timing.user_line_entry[i].key.c;
        }
    }

    return EMPTY_STR;
}



uint32_t timingUserNumber(const char *user)
{
    struct hash_entry *entry;

    // Return 0 if timing source is not timing library

    if(timing.source != TIMING_TIMLIB || !timing.user_line_hash)
    {
        return 0;
    }

    if((entry = hashFind(timing.user_line_hash, (char *)user)))
    {
        if(entry->index > FGC_TIMING_NUM_USERS)
        {
            logPrintf(0, "Invalid user number for user \"%s\"\n", user);
            return 0;
        }
        else
        {
            return entry->index;
        }
    }
    else
    {
        logPrintf(0, "Invalid user name \"%s\"\n", user);
        return 0;
    }
}


uint8_t timingUsersCount()
{
    // Return 0 if timing source is not timing library or the user hash has not been initialized

    if(timing.source != TIMING_TIMLIB || !timing.user_line_entry)
    {
        return 0;
    }

    // Iterate while there are contents in the table

    uint8_t i;
    for(i = 0; i != FGC_TIMING_NUM_USERS && timing.user_line_entry[i].key.c[0] != 0; ++i);

    return i;
}


uint32_t timingGetCycleTimestamp(struct timeval *time, struct timeval *cycle_time)
{
    tTimingTime                    timdt_time, timdt_cycle_time;
    static Timing::FieldValueStore store;

    // Check that the timing source is the timing library

    if(timing.source != TIMING_TIMLIB)
    {
        return 1;
    }

    if(!timing.domain)
    {
        return 1;
    }

    // Do nothing if the cycle time is not supplied or already specified

    if(!cycle_time || cycle_time->tv_sec)
    {
        return 0;
    }

    timdt_time.time.tv_sec  = time->tv_sec;
    timdt_time.time.tv_nsec = time->tv_usec*1000;

    try
    {
        timing.domain->getFieldValueStore(timdt_time,&store);
        timdt_cycle_time = store.getCycleTimestamp();

        cycle_time->tv_sec  = timdt_cycle_time.time.tv_sec;
        cycle_time->tv_usec = timdt_cycle_time.time.tv_nsec / 1000;
    }
    catch(std::exception& e)
    {
        cycle_time->tv_sec  = 0;
        cycle_time->tv_usec = 0;
        return 1;
    }

    return 0;
}



// Static functions



/*
 * Read user line names
 */

static int32_t timingReadUserLines(void)
{
    Timing::Field       field;
    uint32_t            entry_num   = 0;

    if(!timing.domain)
    {
        return 1;
    }

    try
    {
        // Zero the contents of the hash

        memset(timing.user_line_entry, 0, sizeof(timing.user_line_entry));

        // Retrieve the labels and populate the hash

        timing.domain->getField("USER",&field);

        std::vector<Timing::Label*> v = field.getLabelList();

        // Initialise name hash

        if((timing.user_line_hash = hashInit(v.size())) == NULL)
        {
            fprintf(stderr, "ERROR: Unable to initialise user line hash\n");
            fgcddev.status.st_faults |= FGC_FLT_CONFIG;
            return 1;
        }

        for(std::vector<Timing::Label*>::iterator i = v.begin(); i != v.end(); ++i,++entry_num)
        {
            // Retrieve user

            Timing::Label* label = *i;

            if(entry_num > FGC_TIMING_NUM_USERS)
            {
                // Ignoring more users than FGC_TIMING_NUM_USERS

                fprintf(stderr, "WARNING: Ignoring user entry #%u (%s %hu): too many users\n", entry_num, label->getName().c_str(), label->getValueUShort());
                continue;
            }

            if(label->getValueUShort() > FGC_TIMING_NUM_USERS)
            {
                // Ignoring  users than FGC_TIMING_NUM_USERS

                fprintf(stderr, "WARNING: Ignoring user entry #%u (%s %hu): user number out of range\n", entry_num, label->getName().c_str(), label->getValueUShort());
                continue;
            }

            // Create hash entry

            timing.user_line_entry[entry_num].index = static_cast<uint32_t>(label->getValueUShort());

            strncpy(timing.user_line_entry[entry_num].key.c,label->getName().c_str(), HASH_MAX_KEY_LEN);

            // Insert entry into name hash table            -

            hashInsert(timing.user_line_hash, &timing.user_line_entry[entry_num]);
        }

        // Free the labels

        for(std::vector<Timing::Label*>::iterator i = v.begin(); i != v.end();++i)
        {
            delete *i;
        }
    }
    catch(std::exception& e)
    {
        fprintf(stderr, "ERROR: Unable to retrieve users for '%s': %s\n",fgcd_machine_name[fgcd.machine],e.what());
        fgcddev.status.st_faults |= FGC_FLT_CONFIG;
        return 1;
    }

    return 0;
}



/*
 * Read a timing event file
 */

static int32_t timingReadEventFile(const char * filename, FILE *file)
{
    struct Timing_event_handler *handler;
    uint32_t                    handler_index;
    char                        handler_name[20];
    char                        evt_name    [256];
    uint32_t                    delay_ms;
    int                         items_assigned;
    uint32_t                    line_number       = 0;
    uint32_t                    start;
    size_t                      llength           = 0;
    char                        *line             = NULL;
    bool                        iscomment         = false;

    // Set of all read timing events - used for logging and config validation
    std::set<std::string> timing_events;

    // Read events

    while(1)
    {
        if(getline(&line, &llength, file) < 0)
        {
            break;
        }

        line_number++;

        // Ignore leading white spaces and comments

        for(start = 0, iscomment=false; start != llength ; ++start)
        {
            if(iscomment)
            {
                continue;
            }
            else if(line[start] == '#')
            {
                iscomment = true;
                continue;
            }
            else if(isspace(line[start]))
            {
                continue;
            }
            else
            {
                break;
            }
        }

        // Scan line

        items_assigned = sscanf(&line[start], "%19[^:]:%255[^:]:%u", handler_name, evt_name,&delay_ms);

        if(items_assigned != 3)
        {
            // Try to parse an empty event name, which implies a disabled handler

            evt_name[0] = '\0';
            delay_ms    = 0;

            items_assigned = sscanf(&line[start], "%19[^:]::", handler_name);

            if(items_assigned != 1)
            {
                fprintf(stderr, "ERROR: %s:%u: Line does not follow the expected notation (i.e. HANLDER_NAME:EVENT_NAME:DELAY_MS or HANLDER_NAME::), items_assigned=%d\n",
                        filename,
                        line_number,
                        items_assigned);
                fgcddev.status.st_faults |= FGC_FLT_CONFIG;
                return 1;
            }
            else
            {
                fprintf(stderr, "WARNING: %s:%u: Disabling event handler '%s'\n", filename, line_number, handler_name);
            }
        }

        // Search for event handler with matching name

        for(handler_index = 0 ; timing_event_handlers[handler_index].function && strcmp(timing_event_handlers[handler_index].name, handler_name); handler_index++);

        // Check whether event handler was not found

        if(!timing_event_handlers[handler_index].function) // Event handler was not found
        {
            // Ignore unknwown event handlers
            // This is useful to allow backward compatible changes when new handlers are introduced

            fprintf(stderr, "WARNING: %s:%u: Event handler \"%s\" not used\n",
                filename,
                line_number,
                handler_name);
            continue;

        }

        handler = &timing_event_handlers[handler_index];

        // HACK: If the MS handler is needd, then we need to look for an appropiate event.
        // The evt_name must follow the convention "FGCD1KHZ-HOSTNAME"

        if(strcmp(handler->name,"MS") == 0)
        {
            uint32_t evt_length = strlen(evt_name);
            uint32_t host_length = strlen(fgcd.hostname);

            evt_name[evt_length] = '-';

            for(uint32_t i=0; i != host_length; ++i)
            {
                evt_name[evt_length+i+1] = toupper(fgcd.hostname[i]);
            }

            evt_name[evt_length+host_length+1] = '\0';
        }

        // Add timing event name to the list of names in the fieldbus event handler.
        handler->timing_events[evt_name] = Timing_event_info{delay_ms};

        // Validate that this timing event has not been assigned before
        if (timing_events.find(evt_name) != timing_events.end())
        {
            fprintf(stderr, "WARNING: %s:%u: Timing event \"%s\" has already been assigned to the event handler. Reassigning. \n",
                    filename,
                    line_number,
                    evt_name);
        }

        // Add the timing event to the set of all registered timing events.
        timing_events.emplace(evt_name);
    }

    free(line);

    // Log all registered timing events for debugging purposes.
    std::string registered_events;
    for (const auto& event: timing_events)
    {
        registered_events += event + " ";
    }

    fgcddebug("Based on file %s the following timing events were registered:\n%s\n",
              filename, registered_events.c_str());

    return 0;
}


/*
 * Timing thread
 */

static void *timingRun(void *unused)
{
    // Check the that timing source is the timing library

    if(timing.source != TIMING_TIMLIB) return 0;

    while(1)
    {
         timingReceiveEvent();
    }

    return 0;
}

/*
 * Event handler for use with POSIX timer to generate a millisecond tick
 */

static void timingMSTimerEvent(union sigval value)
{
    tTimingTime           evt_time;
    struct timespec       time;
    Timing_event_handler *handler;
    uint32_t              handler_index;

    // Get time and store in evt_time

    if(clock_gettime(CLOCK_REALTIME, &time))
    {
        logPrintf(0, "TIMING Failed to read real-time clock\n");
        return;
    }

    evt_time.time.tv_sec    = time.tv_sec;
    evt_time.time.tv_nsec   = time.tv_nsec;

    // Find corresponding handler index

    for(handler_index = 0 ;
        timing_event_handlers[handler_index].function                                                   &&
        strcmp(timing_handler_names[TIMING_HANDLER_POSIXMS],timing_event_handlers[handler_index].name)
        ; handler_index++);

    handler = &timing_event_handlers[handler_index];

    // Ignore event if no corresponding FGC event found

    if(!handler->function)
    {
        logPrintf(0, "TIMING Event \"POSIXEVENT\" has no associated action\n");
        return;
    }

    // For the additional timing info we are going to use first one - if it exists - otherwise a default-constructed one
    Timing_event_info info;

    if (!handler->timing_events.empty())
    {
        info = handler->timing_events.begin()->second;
    }

    // Call function for event.

    handler->function(handler, info, evt_time, NULL);
}



/*
 * Start a POSIX timer
 */

static int32_t timingStartPosixTimer(timer_t *timer, struct itimerspec *timer_spec, struct sigevent *event)
{
    // Check that the timing source is the system time

    if(timing.source != TIMING_SYS)
    {
        fprintf(stderr, "ERROR: POSIX timer can only be used with system timing\n");
        return 1;
    }

    // Create timer

    if(timer_create(CLOCK_REALTIME, event, timer))
    {
        perror("timer_create");
        return 1;
    }

    // Start the timer

    if(timer_settime(*timer, 0, timer_spec, NULL))
    {
        perror("timer_settime");
        timer_delete(*timer);
        return 1;
    }

    return 0;
}

void timingSetCtrOuput(uint32_t out,uint32_t value)
{
    const unsigned long CTRV_MODULE             = 1;
    static int          error                   = 0;
    int                 remote_flag;
    struct ctr_ccv_s    ctr_ccv;
    ctr_ccv_fields_t    ctr_ccv_fields          = static_cast<ctr_ccv_fields_t>(CTR_CCV_COUNTER_MASK | CTR_CCV_POLARITY);
    int                 polarity                = (value ? CtrDrvrPolarityTTL_BAR : CtrDrvrPolarityTTL);

    if(timing.log_events_flag)
    {
        logPrintf(0, "TIMING Triggering CTRV output CTR output 0x%X to %d\n",out,value?1:0);
    }

    // Retrieve the CTR module

    if(ctr_set_module(timing.ctr_handle, CTRV_MODULE) < 0)
    {
        if(!error)
        {
            error = 1;
            fgcddev.status.st_faults |= FGC_FLT_TIMING;
            logPrintf(0, "TIMING Failed to set CTR output 0x%X to %d (ctr_set_value failed)\n",out,value?1:0);
        }
    }
    else
    {
        // Retrieve the handle to the counter

        remote_flag = ctr_get_remote(timing.ctr_handle, static_cast<CtrDrvrCounter>(out), &ctr_ccv);
        ctr_ccv.counter_mask = static_cast<CtrDrvrCounterMask>(1<<out);
        ctr_ccv.polarity = static_cast<CtrDrvrPolarity>(polarity);

        // Set the appropiate value

        if(ctr_set_remote(timing.ctr_handle, 1, static_cast<CtrDrvrCounter>(out), CtrDrvrRemoteEXEC, &ctr_ccv, ctr_ccv_fields) < 0)
        {
            if(!error)
            {
                error = 1;
                fgcddev.status.st_faults |= FGC_FLT_TIMING;
                logPrintf(0, "TIMING Failed to set CTR output 0x%X to %d (ctr_set_remote failed)\n",out,value?1:0);
            }

        }
        else if(ctr_set_remote(timing.ctr_handle, remote_flag, static_cast<CtrDrvrCounter>(out), static_cast<CtrDrvrRemote>(0), NULL, static_cast<ctr_ccv_fields_t>(0)) < 0)
        {
            if(!error)
            {
                error = 1;
                fgcddev.status.st_faults |= FGC_FLT_TIMING;
                logPrintf(0, "TIMING Failed to set CTR output 0x%X to %d (ctr_set_remote failed)\n",out,value?1:0);
            }

        }
        else if(error)
        {
            error = 0;
            fgcddev.status.st_faults &= ~FGC_FLT_TIMING;
            logPrintf(0, "TIMING Fault cleared (set CTR output 0x%X to %d succeeded)\n",out,value?1:0);
        }
    }
}

// EOF
