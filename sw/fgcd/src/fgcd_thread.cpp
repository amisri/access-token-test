/*!
 * @file   fgcd_threads.cpp
 * @brief  Functions to start and stop FGCD threads
 * @author Michael Davis
 */

#include <cstdio>
#include <unistd.h>
#include <sched.h>
#include <string.h>

#include <fgcd_thread.h>



// External functions

int32_t fgcd_mutex_init(pthread_mutex_t *mutex, enum FGCD_mutex_policy policy)
{
    int32_t             pthread_error;
    pthread_mutexattr_t mutex_attr;

    // Initialise mutex attributes

    pthread_mutexattr_init(&mutex_attr);

    if((pthread_error = pthread_mutexattr_setprotocol(&mutex_attr, policy)) != 0)
    {
        fprintf(stderr, "ERROR: fgcd_mutex_init():pthread_mutexattr_setprotocol failed with error %d\n", pthread_error);
        pthread_mutexattr_destroy(&mutex_attr);
        return 1;
    }

    // Initialise mutex

    pthread_mutex_init(mutex, &mutex_attr);

    pthread_mutexattr_destroy(&mutex_attr);

    return 0;
}



int32_t fgcd_thread_create(pthread_t *thread_id, FGCD_thread_func *run_function, void *arg, enum FGCD_thread_policy policy, enum FGCD_priority priority, enum FGCD_affinity affinity)
{
    int32_t            thread_error;
    pthread_attr_t     thread_attr;
    struct sched_param thread_param;
    cpu_set_t          cpuset;

        // Configure thread attributes

    if((thread_error = pthread_attr_init(&thread_attr)) != 0)
    {
        fprintf(stderr, "ERROR: fgcd_thread_create():pthread_attr_init failed with error %d\n", thread_error);
        return 1;
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&thread_attr, policy);
    pthread_attr_setstacksize   (&thread_attr, PTHREAD_STACK_SIZE);

    // Configure thread parameters

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = priority;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    if((thread_error = pthread_create(thread_id, &thread_attr, run_function, arg)) != 0)
    {
        fprintf(stderr, "ERROR: fgcd_thread_create():pthread_create failed with error = %d: %s\n",
            thread_error,
            strerror(thread_error));
        pthread_attr_destroy(&thread_attr);
        return 1;
    }

    pthread_attr_destroy(&thread_attr);

    // Change the thread affinity if required

    if (affinity != NO_AFFINITY)
    {
        CPU_ZERO(&cpuset);
        CPU_SET(0,&cpuset);

        if((thread_error = pthread_setaffinity_np(*thread_id, sizeof(cpuset),&cpuset)))
        {
            fprintf(stderr, "ERROR: fgcd_thread_create():pthread_setaffinity_np failed with error = %d: %s\n",
                thread_error,
                strerror(thread_error));
            return 1;
        }
    }

    return 0;
}

// EOF
