//  Filename: equip_common.c
//  Purpose:  Common functions for equipment devices


#include <string.h>
#include <cmd.h>
#include <logging.h>
#include <equipdev.h>
#include <fgc_parser_consts.h>
#include <parser.h>
#include <fgcd.h>
#include <defprops_class.h>
#include <prop.h>
#include <prop_equip.h>


// Static functions

static uint32_t equipdevInitProperties(struct prop *property)
{
    // This recursive function redirects the number of elements for each equipment device class property
    // to allow an independent value for each device.  It also sets the config_unset_count for
    // each device.

    uint32_t        i;
    static uint32_t num_els_idx = 0;

    // Create property name string and store it in malloced memory, linked from prop.name

    char prop_name[FGC_MAX_PROP_LEN];

    size_t name_len = 1 + parserGetPropertyName(property, EQUIPDEV_CLASS_ID, prop_name, FGC_MAX_PROP_LEN);

    property->name = (char *)malloc(name_len);

    strncpy(property->name, prop_name, name_len);

    // Treat parents and data properties separately

    if(property->type == PT_PARENT)
    {
        // Recurse for each of property's children

        struct prop *child = (struct prop*)property->value;

        for(i = 0 ; i < property->num_elements ; i++)
        {
            if(equipdevInitProperties(child))
            {
                return 1;
            }

            // If the child property (after going through equipdevInitProperties) is a setting, mark the parent as a setting as well
            if (child->is_setting)
            {
                property->is_setting = true;
            }

            // Increment to the next child
            child++;
        }
    }
    else // Property is not a parent
    {
        // Report properties if value pointer is NULL and a Set or Get function is associated with the property

        if(   property->value == NULL
           && (property->set_func_idx || property->get_func_idx))
        {
            fprintf(stderr, "Note: property %s value is NULL\n", property->name);
        }

        // Mark the property as setting if it has a set function
        if (property->set_func_idx)
        {
            property->is_setting = true;
        }

        // Check whether property has the CONFIG flag set

        if(property->flags & PF_CONFIG)
        {
            // Check that NON_VOLATILE flag is set

            if(!(property->flags & PF_NON_VOLATILE))
            {
                fprintf(stderr, "ERROR: Property %s has CONFIG flag set, but not NON_VOLATILE flag\n", property->name);
                return 1;
            }

            // Check that initial number of elements is zero

            if(((property->flags & PF_INDIRECT_N_ELS) && *(uintptr_t *)property->num_elements != 0) ||
                 property->num_elements != 0)
            {
                fprintf(stderr, "ERROR: Property %s has CONFIG flag set, but initial number of elements is not zero\n", property->name);
                return 1;
            }

            // Increment count of unset configuration properties for each equipment device

            for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
            {
                fgcd.device[i].config_unset_count++;
            }
        }

        // Return if property already has an indirect number of elements

        if(property->flags & PF_INDIRECT_N_ELS)
        {
            return 0;
        }

        // Check whether all num_els elements have been taken

        if(num_els_idx >= EQUIPDEV_MAX_PROPS)
        {
            fprintf(stderr, "ERROR: EQUIPDEV_MAX_PROPS is too low (%u)\n", EQUIPDEV_MAX_PROPS);
            return 1;
        }

        // Set device specific numbers of elements to default value for property

        for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
        {
            for(uint32_t user = 0 ; user < NUM_CYC_SELECTORS ; user++)
            {
                equipdev.device[i].ppm[user].prop_num_els[num_els_idx] = property->num_elements;
            }
        }

        // Redirect property number of elements to device specific value

        property->flags       |= PF_INDIRECT_N_ELS;
        property->num_elements = (uintptr_t)&equipdev.device[0].ppm[0].prop_num_els[num_els_idx++];
    }

    return 0;
}



int32_t equipdevInit(uint32_t num_pars_func, Prop_callback evtlog_func)
{
    // Set platform and class

    equipdev.fgc_platform      = FGC_PLATFORM_ID;
    equipdev.fgc_platform_name = FGC_PLATFORM_NAME;
    equipdev.fgc_class_name    = FGC_CLASS_NAME;

    // Initialise the property hash

    if(!(equipdev.prop_hash = hashInit(PROP_HASH_SIZE)))
    {
        fprintf(stderr, "ERROR: Unable to initialise property hash\n");
        return 1;
    }

    // Populate the property hash

    for(uint32_t i = 0 ; i < N_PROP_SYMBOLS ; i++)
    {
        hashInsert(equipdev.prop_hash, &SYM_TAB_PROP[i]);
    }

    // Initialise the constant hash

    if((equipdev.const_hash = hashInit(CONST_HASH_SIZE)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise constant hash\n");
        return 1;
    }

    // Populate the const hash

    for(uint32_t i = 0 ; i < N_CONST_SYMBOLS ; i++)
    {
        hashInsert(equipdev.const_hash, &SYM_TAB_CONST[i]);
    }

    // Set parser data for class

    struct Parser_class_data parser_data;

    parser_data.class_id         = EQUIPDEV_CLASS_ID;
    parser_data.sym_tab_const    = SYM_TAB_CONST;
    parser_data.const_hash       = equipdev.const_hash;
    parser_data.sym_tab_prop     = SYM_TAB_PROP;
    parser_data.sym_tab_prop_len = N_PROP_SYMBOLS;
    parser_data.prop_hash        = equipdev.prop_hash;
    parser_data.get_func         = GET_FUNC;
    parser_data.pars_func        = PARS_FUNC;
    parser_data.num_pars_func    = num_pars_func;
    parser_data.set_func         = SET_FUNC;
    parser_data.setif_func       = SETIF_FUNC;
    parser_data.evtlog_func      = evtlog_func;

    if(parserSetClassData(&parser_data) != 0)
    {
        fprintf(stderr, "ERROR: Failed to set parser class data\n");
        return 1;
    }

    // Initialise equipment device properties

    for(uint32_t i = 0 ; i < FGC_N_TLPS ; i++)
    {
        if(equipdevInitProperties(&PROPS[i]))
        {
            fprintf(stderr, "ERROR: Failed to initialise equipment device properties\n");
            return 1;
        }
    }

    return 0;
}



bool equipdevConfigProperty(struct Parser *parser, struct prop* property)
{
    bool        is_ppm    = false;                                   // Flag to indicate that property access is PPM
    struct Cmd *command   = parser->current_command;
    uint32_t    device_id = command->device->id;

    // If the value is in the fgcd.device or equipdev.device structures, set the value to array element for the device

    // Value

    if(property->value >= (void *)&fgcd.device[0] &&
       property->value <  (void *)&fgcd.device[1])
    {
        // Value is in fgcd.device structure

        parser->value += sizeof(fgcd.device[0]) * device_id;
    }
    else if(property->value >= (void *)&equipdev.device[0] &&
            property->value <  (void *)&equipdev.device[1])
    {
        // Value is in equipdev.device structure

        // Check whether value is PPM

        if(parser->value >= (void *)&equipdev.device[0].ppm[0] &&
           parser->value <  (void *)&equipdev.device[0].ppm[1])
        {
            // Value is PPM

            is_ppm = true;
            parser->value  += sizeof(equipdev.device[0].ppm[0]) * command->user;
        }

        // Advance value to correct equipment device structure

        parser->value += sizeof(equipdev.device[0]) * device_id;
    }
    else if(property->value >= (void *)&equipdev.ppm[0] &&
            property->value <  (void *)&equipdev.ppm[1])
    {
        // Value is PPM

        is_ppm = true;
        parser->value  += sizeof(equipdev.ppm[0]) * command->user;
    }

    // Zero command user if command is not PPM

    if(property->type != PT_PARENT && !(property->flags & PF_PPM) && !is_ppm)
    {
        command->user = 0;
    }

    // Number of elements

    if(property->flags & PF_INDIRECT_N_ELS)
    {
        if((void *)parser->num_elements_ptr >= (void *)&fgcd.device[0] &&
           (void *)parser->num_elements_ptr <  (void *)&fgcd.device[1])
        {
            // Number of elements is in fgcd.device structure

            parserIncNumElsPtr(parser, sizeof(struct FGCD_device), device_id);
        }
        else if((void *)parser->num_elements_ptr >= (void *)&equipdev.device[0] &&
                (void *)parser->num_elements_ptr <  (void *)&equipdev.device[1])
        {
            // Number of elements is in equipdev.device structure

            // Check whether number of elements is PPM

            if((void *)parser->num_elements_ptr >= (void *)&equipdev.device[0].ppm[0] &&
               (void *)parser->num_elements_ptr <  (void *)&equipdev.device[0].ppm[1])
            {
                // Number of elements is PPM

                parserIncNumElsPtr(parser, sizeof(equipdev.device[0].ppm[0]), command->user);
            }

            // Advance number of elements to correct equipment device structure

            parserIncNumElsPtr(parser, sizeof(equipdev.device[0]), device_id);


        }
        else if((void *)parser->num_elements_ptr >= (void *)&equipdev.ppm[0] &&
                (void *)parser->num_elements_ptr <  (void *)&equipdev.ppm[1])
        {
            // Number of elements is PPM

            parserIncNumElsPtr(parser, sizeof(equipdev.ppm[0]), command->user);
        }
    }

    // If libref is being used then map value and num_element_ptr if in the fg_error, cyc_status and ref_armed zones

#ifdef LIBREF
    refMgrVarPointerFunc(&equipdev.device[command->device->id].ref_mgr,
                         (uint8_t*) parser->value,
                         0, command->user, 0, 0,
                         (uint8_t **) &parser->value);

    refMgrVarPointerFunc(&equipdev.device[command->device->id].ref_mgr,
                         (uint8_t*) parser->num_elements_ptr,
                         0, command->user, 0, 0,
                         (uint8_t **) &parser->num_elements_ptr);
#endif

    return is_ppm;
}


// Scan the symbol list for a command

int32_t propEquipScanSymbolList(uint8_t *value, char *delimiter, struct Cmd *command, struct prop *property)
{
    struct Parser        *parser = command->parser;
    uint32_t              sym_index;
    const struct sym_lst *sym_list;

    // Check that symbol list is defined

    if(!(property->flags & PF_SYM_LST))
    {
        command->error = FGC_NOT_IMPL;
        return 1;
    }
    sym_list = static_cast<const struct sym_lst*>(property->range);

    // Scan symbol from command string

    sym_index = parserIdentifyNextSymbol(parser, command->value, &command->index, command->value_length, parser->class_data->const_hash,
                                         delimiter == NULL ? DELIM_NONE : DELIM_COMMA, 1, delimiter);

    if(sym_index == 0)
    {
        // No symbol was supplied or the symbol does not exist

        command->error = FGC_NO_SYMBOL;
        return 1;
    }

    // Scan property's symbol list for a match

    for( ; sym_list->index && sym_list->index != sym_index ; sym_list++);

    // Check whether no match was found

    if(!sym_list->index)
    {
        // Symbol was not valid, return error

        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Set return value

    *value = sym_list->value;

    return 0;
}



struct prop *equipdevLookupProp(uint32_t channel, char bus_id, const char *prop_label, char *prop_label_expand)
{
    struct prop *property;

    // Copy the property label, replace '?' with bus_id and split it into tokens

    strncpy(prop_label_expand, prop_label, FGC_MAX_PROP_LEN);

    for(uint32_t i = 0; i < strlen(prop_label_expand); ++i)
    {
        if(prop_label_expand[i] == '?') prop_label_expand[i] = bus_id;
    }

    char prop_tokens[FGC_MAX_PROP_LEN];

    strncpy(prop_tokens, prop_label_expand, FGC_MAX_PROP_LEN);

    char *next_part = strtok(prop_tokens, ".");

    // Find the top-level property

    struct hash_entry *prop_ptr = hashFind(equipdev.prop_hash, next_part);

    if( prop_ptr                                                    == NULL ||
       (property = reinterpret_cast<struct prop*>(prop_ptr->value)) == NULL)
    {
        logPrintf(channel, "EQUIPDEV %s is not a top-level property.\n", next_part);
        return NULL;
    }

    next_part = strtok(NULL, ".");

    // Descend the property tree to find the leaf property

    while(next_part != NULL && property != NULL && property->type == PT_PARENT)
    {
        uint32_t     i;
        struct prop *child;

        for(i = 0, child = static_cast<struct prop*>(property->value); i < property->num_elements; ++i, ++child)
        {
            if(strcmp(SYM_TAB_PROP[child->sym_idx].key.c, next_part) == 0)
            {
                next_part = strtok(NULL, ".");
                property  = child;
                break;
            }
        }

        if(property != child)
        {
            logPrintf(channel, "EQUIPDEV Element %s not found in the property tree.\n", next_part);
            return NULL;
        }
    }

    // As we have reached the leaf property, next_part should be NULL

    if(next_part != NULL)
    {
        logPrintf(channel, "EQUIPDEV Element %s is not found in the property tree.\n", next_part);
        return NULL;
    }

    return property;
}



void equipdevPropSetNumEls(uint32_t channel, char bus_id, const char *prop_label, uint32_t num_els)
{
    struct prop    *property;                               // Pointer to the property
    char            prop_label_expand[FGC_MAX_PROP_LEN];    // Expanded label for the property, with ? converted to A or B

    // Do nothing if prop name is unset

    if(prop_label[0] == '\0') return;

    if((property = equipdevLookupProp(channel, bus_id, prop_label, prop_label_expand)) == NULL)
    {
        logPrintf(channel, "EQUIPDEV Property %s not found, cannot set num_els to %d.\n", prop_label_expand, num_els);
        return;
    }
    else if(!(property->flags & PF_INDIRECT_N_ELS))
    {
        // This function should only be called for properties with an indirect number of elements

        logPrintf(channel, "EQUIPDEV Property %s PF_INDIRECT_N_ELS flag is not set, cannot set num_els to %d.\n", prop_label_expand, num_els);
        return;
    }

    // Set the number of elements to the requested value

    const uint32_t offset = sizeof(Equipdev_channel) * channel;

    uint32_t *num_elements = reinterpret_cast<uint32_t*>(reinterpret_cast<char*>(property->num_elements) + offset);

    // Check whether a configuration property has been set
    // This is not secure because an array config property without DONT_SHRINK could
    // be emptied and then set, and each time it would decrement config_unset_count.
    // It is simply hoped that this won't happen during the initialisation phase.
    // The correct implementation will require a dynamic property flag per device, which
    // is not yet supported.

    if(   (property->flags & PF_CONFIG) != 0
       && fgcd.device[channel].config_unset_count > 0
       && (   ((property->flags & PF_DONT_SHRINK) == 0 && *num_elements == 0 && num_els != 0)
           || ((property->flags & PF_DONT_SHRINK) != 0 && *num_elements != property->max_elements && num_els == property->max_elements)))
    {
        fgcd.device[channel].config_unset_count--;
    }

    *num_elements = num_els;
}



void equipdevNonvolWarn(uint32_t channel, int32_t set)
{
    // Called from nonvol.cpp to set/clear the CONFIG warning

    setStatusBit(&equipdev.device[channel].status.st_warnings, FGC_WRN_CONFIG, set);
}

// EOF
