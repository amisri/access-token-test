/*!
 * @file   charq.c
 * @brief  Functions for queues (FIFOs) of characters
 * @author Stephen Page
 */

#include <stdio.h>
#include <stdlib.h>

#include <charq.h>
#include <fgcd_thread.h>



// Static functions

static int charqIsEmpty(struct Charq *q);
static int charqIsFull(struct Charq *q);



int32_t charqInit(struct Charq *q, uint32_t buffer_size)
{
    // Allocate memory for queue buffer

    buffer_size++;
    if(!(q->buffer = (unsigned char *)malloc(buffer_size)))
    {
        return 1;
    }

    // Initialise mutex and condition variables

    if(fgcd_mutex_init(&q->mutex, CHARQ_MUTEX_POLICY) != 0)
    {
        free(q->buffer);
        q->buffer = NULL;
        return 1;
    }

    pthread_cond_init(&q->not_empty, NULL);
    pthread_cond_init(&q->not_full,  NULL);

    q->block           = 1;
    q->buffer_size     = buffer_size;
    q->back = q->front = 0;

    return 0;
}



void charqFree(struct Charq *q)
{
    // Free memory

    if(q->buffer != NULL)
    {
        free(q->buffer);
        q->buffer = NULL;
    }

    // Destroy mutex and condition variables

    pthread_mutex_destroy(&q->mutex);
    pthread_cond_destroy(&q->not_empty);
    pthread_cond_destroy(&q->not_full);
}



int32_t charqPush(struct Charq *q, unsigned char member)
{
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return 2;
    }

    // Check that the queue is not full

    if(!charqIsFull(q)) // The queue is not full
    {
        // Send signal that queue has content

        if(charqIsEmpty(q))
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        // Add the member to the queue

        q->buffer[q->front] = member;
        q->front            = (q->front + 1) % q->buffer_size;

        pthread_mutex_unlock(&q->mutex);
        return 0;
    }
    else // The queue is full
    {
        pthread_mutex_unlock(&q->mutex);
        return 1;
    }
}



int32_t charqBlockPush(struct Charq *q, unsigned char member)
{
    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return 2;
    }

    // Check that the queue is not full

    while(q->block && charqIsFull(q))
    {
        // Wait for a member to leave the queue

        pthread_cond_wait(&q->not_full, &q->mutex);
    }

    // Check whether the queue was unblocked

    if(q->block) // The block was not removed
    {
        // Send signal that queue has content

        if(charqIsEmpty(q))
        {
            pthread_cond_broadcast(&q->not_empty);
        }

        // Add the member to the queue

        q->buffer[q->front] = member;
        q->front            = (q->front + 1) % q->buffer_size;

        pthread_mutex_unlock(&q->mutex);
        return 0;
    }
    else // The queue was unblocked
    {
        pthread_mutex_unlock(&q->mutex);
        return 1;
    }
}



int32_t charqPop(struct Charq *q)
{
    unsigned char   member;

    // Prevent thread from dying while holding mutex

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    if(charqIsEmpty(q))
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    // Send signal that queue is not full

    if(charqIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the member from the queue

    member  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    return member;
}



int32_t charqBlockPop(struct Charq *q)
{
    unsigned char   member;

    pthread_mutex_lock(&q->mutex);

    // Check whether queue has been properly initialised

    if(q->buffer == NULL)
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    while(q->block && charqIsEmpty(q))
    {
        // Wait for a member to enter the queue

        pthread_cond_wait(&q->not_empty, &q->mutex);
    }

    // Check whether the queue was unblocked

    if(!q->block)
    {
        pthread_mutex_unlock(&q->mutex);
        return -1;
    }

    // Send signal that queue is not full

    if(charqIsFull(q))
    {
        pthread_cond_broadcast(&q->not_full);
    }

    // Remove the member from the queue

    member  = q->buffer[q->back];
    q->back = (q->back + 1) % q->buffer_size;

    pthread_mutex_unlock(&q->mutex);
    return member;
}



void charqSetBlock(struct Charq *q, int32_t state)
{
    pthread_mutex_lock(&q->mutex);

    if(!(q->block = state))
    {
        pthread_cond_broadcast(&q->not_empty);
        pthread_cond_broadcast(&q->not_full);
    }

    pthread_mutex_unlock(&q->mutex);
}



// Static functions



/*
 * Is the queue empty?
 */

static int charqIsEmpty(struct Charq *q)
{
    return q->back == q->front;
}



/*
 * Is the queue full?
 */

static int charqIsFull(struct Charq *q)
{
    return (q->front + 1) % q->buffer_size == q->back;
}

// EOF
