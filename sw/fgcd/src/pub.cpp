/*!
 * @file   pub.cpp
 * @brief  Functions for publishing data
 * @author Stephen Page
 * @author Michael Davis
 */

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/ip.h>

#include <consts.h>
#include <defconst.h>
#include <fgcd.h>
#include <fgc_consts_gen.h>
#include <pub.h>

#include <alarms.h>

#include <cmw.h>

#include <pm.h>

#include <definfo.h>
#include <fgc_pubdata.h>
#include <fgcddev.h>
#include <tcp.h>
#include <timing.h>
#include <logging.h>
#include <fgcd_thread.h>



// Global variables

struct Pub pub = {};



// Declare publication thread function

static FGCD_thread_func pubSend;



// Static functions



/*
 * Read UDP only subscriptions from files
 */

static void pubReadSubs(FILE *subs_file, const char *filename)
{
    const uint32_t   MAX_USER_NAME = 64;

    char     user   [MAX_USER_NAME + 1];
    char     format [100];

    // Prepare address_hints

    struct addrinfo address_hints;
    memset(&address_hints, 0, sizeof(struct addrinfo));
    address_hints.ai_family = AF_INET;

    // Read each subscription

    int32_t  items_assigned;
    uint32_t line_number     = 0;

    do
    {
        struct udp_client *client = &pub.client[pub.num_udp_only_clients];

        client->user = 0;

        // Create format from constants

        sprintf(format, "%%%d[^:]:%%4u:%%5d", HOST_NAME_MAX);

        items_assigned = fscanf(subs_file, format, client->hostname, &client->port_num, &client->period);
        line_number++;

        if(items_assigned == 3)
        {
            // Attempt to read optional user-defined ID

            items_assigned = fscanf(subs_file, ":%x", &client->id);
            if(items_assigned != 1)
            {
                client->id = 0;
            }
            else
            {
                // Attempt to read optional user

                sprintf(format, ":%%%u[^:\n]", MAX_USER_NAME);
                items_assigned = fscanf(subs_file, format, user);

                if(items_assigned == 1)
                {
                    uint32_t user_num;

                    if((user_num = timingUserNumber(user)))
                    {
                        client->user = user_num;
                    }
                    else
                    {
                        fprintf(stderr, "WARNING: Unknown user %s in subscription file %s line %u.\n",
                                user, filename, line_number);
                        continue;
                    }
                }
            }
            items_assigned = 3;

            // Resolve client's address to a host entry

            int32_t          error;
            struct addrinfo *address_info;

            if((error = getaddrinfo(client->hostname, NULL, &address_hints, &address_info)))
            {
                // Resolution failed - report error and continue with next entry

                fprintf(stderr, "WARNING: Unable to resolve address for hostname %s in UDP subscription file %s line %u: %s\n",
                        client->hostname, filename, line_number, gai_strerror(error));

                continue;
            }

            // Store client address

            client->address = reinterpret_cast<struct sockaddr_in*>(address_info[0].ai_addr)->sin_addr;
            freeaddrinfo(address_info);

            // Check whether a negative value was specified for an aligned subscription

            if(client->period < 0)
            {
                if(abs(client->period) > FGCD_CYCLES_PER_SEC)
                {
                    // Check that period is a multiple of seconds

                    if((FGCD_CYCLE_PERIOD_MS * abs(client->period)) % 1000)
                    {
                        fprintf(stderr, "WARNING: Invalid period %d in subscription file %s line %u.\n", client->period, filename, line_number);
                        continue;
                    }
                }
                else
                {
                    // Check that period is a factor of 1 second

                    if(1000. / (FGCD_CYCLE_PERIOD_MS * abs(client->period)) != 1000 / (FGCD_CYCLE_PERIOD_MS * abs(client->period)))
                    {
                        fprintf(stderr, "WARNING: Invalid period %d in subscription file %s line %u.\n", client->period, filename, line_number);
                        continue;
                    }
                }
            }

            pub.num_udp_only_clients++;

            // Read until end of line or file

            int32_t c;

            while((c = fgetc(subs_file)) != EOF && c != '\n');
        }
    } while(items_assigned == 3 && pub.num_udp_only_clients < PUB_UDP_MAX_NON_TCP_CLIENTS);

    if(items_assigned != EOF)
    {
        // Check whether too many clients were specified

        if(pub.num_udp_only_clients == PUB_UDP_MAX_NON_TCP_CLIENTS)
        {
            fprintf(stderr, "WARNING: Too many UDP subscriptions in file %s at line %u\n",
                    filename, line_number);
        }
        else if(items_assigned != 3)  // Syntax error
        {
            fprintf(stderr, "WARNING: Syntax error in UDP subscription file %s at line %u\n",
                    filename, line_number);
        }
        fgcddev.status.st_faults |= FGC_FLT_CONFIG;
    }
}



/*
 * Scan UDP only clients for subscriptions
 */

static void pubScanUDPClients(struct timeval *pub_time)
{
    // Configure socket address structure

    struct sockaddr_in sin;

    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;

    for(uint32_t i = 0 ; i < pub.num_udp_only_clients ; i++)
    {
        struct udp_client *udp_client  = &pub.client[i];
        uint32_t           port_number = udp_client->port_num;

        // Take no action if port number or period is 0 or subscription is for a different user

        if(port_number && udp_client->period &&
           (!udp_client->user || udp_client->user == timing.user))
        {
            // Align subscription if period is negative

            if(udp_client->period < 0)
            {
                if(abs(udp_client->period) > (1000 / FGCD_CYCLE_PERIOD_MS)) // Align to a multiple of seconds
                {
                    udp_client->period_tick = ((pub_time->tv_sec % ((pub.client[i].period * FGCD_CYCLE_PERIOD_MS) / 1000)) * FGCD_CYCLES_PER_SEC) +
                                              (pub_time->tv_usec / (FGCD_CYCLE_PERIOD_MS * 1000));
                }
                else // Align within a second
                {
                    udp_client->period_tick = ((pub_time->tv_usec / (FGCD_CYCLE_PERIOD_MS * 1000)) % udp_client->period);
                }
            }

            ++udp_client->period_tick;

            // Send data

            if(udp_client->period_tick * FGCD_CYCLE_PERIOD_MS >= abs(udp_client->period * PUB_UDP_PERIOD_MS))
            {
                // Re-Initialize tick to zero

                udp_client->period_tick = 0;

                // Configure addressing

                sin.sin_addr = udp_client->address;
                sin.sin_port = htons(port_number);

                // Set time of data send

                struct timeval            send_time;
                struct fgc_udp_header    *header;

                timingReadTime(&send_time);

                header = &pub.published_data.header;
                header->id              = htonl(udp_client->id);
                header->sequence        = htonl(udp_client->sequence++);
                header->send_time_sec   = htonl(send_time.tv_sec);
                header->send_time_usec  = htonl(send_time.tv_usec);

                // Send data

                pub.udp_load += sendto(pub.sock, &pub.published_data, sizeof(struct Pub_data), 0,
                                       reinterpret_cast<struct sockaddr*>(&sin), sizeof(struct sockaddr_in));
            }
        }
    }
}



/*
 * Scan TCP clients for UDP subscriptions
 */

static void pubScanTCPClientsUDP(struct timeval *pub_time)
{
    // Configure socket address structure

    struct sockaddr_in sin;

    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;

    for(uint32_t i = 0 ; i < FGC_TCP_MAX_CLIENTS ; i++)
    {
        struct TCP_client *tcp_client  = &tcp.client[i];
        uint32_t           port_number = tcp_client->udp_sub_port_num;

        // Take no action if port number or period is 0 or subscription is for a different user

        if(port_number && tcp_client->udp_sub_period)
        {

            // Align subscription if period is negative

            if(tcp_client->udp_sub_period < 0)
            {
                if(abs(tcp_client->udp_sub_period) > (1000 / FGCD_CYCLE_PERIOD_MS)) // Align to a multiple of seconds
                {
                    tcp_client->udp_sub_period_tick = ((pub_time->tv_sec % ((tcp_client->udp_sub_period * FGCD_CYCLE_PERIOD_MS) / 1000)) * FGCD_CYCLES_PER_SEC) +
                                                      (pub_time->tv_usec / (FGCD_CYCLE_PERIOD_MS * 1000));
                }
                else // Align within a second
                {
                    tcp_client->udp_sub_period_tick = ((pub_time->tv_usec / (FGCD_CYCLE_PERIOD_MS * 1000)) % tcp_client->udp_sub_period);
                }
            }

            ++tcp_client->udp_sub_period_tick;

            // Send data

            if(tcp_client->udp_sub_period_tick * FGCD_CYCLE_PERIOD_MS >= abs(tcp_client->udp_sub_period * PUB_UDP_TCP_PERIOD_MS))
            {
                // Re-Initialize tick to zero

                tcp_client->udp_sub_period_tick = 0;

                // Configure addressing

                sin.sin_addr = tcp_client->address;
                sin.sin_port = htons(port_number);

                // Set time of data send

                struct timeval            send_time;
                struct fgc_udp_header    *header;

                timingReadTime(&send_time);

                header = &pub.published_data.header;
                header->id              = htonl(tcp_client->udp_sub_id);
                header->sequence        = htonl(tcp_client->udp_sub_sequence++);
                header->send_time_sec   = htonl(send_time.tv_sec);
                header->send_time_usec  = htonl(send_time.tv_usec);

                // Send data

                pub.udp_load += sendto(pub.sock, &pub.published_data, sizeof(struct Pub_data), 0,
                                       reinterpret_cast<struct sockaddr*>(&sin), sizeof(struct sockaddr_in));
            }
        }
    }
}

/*
 * Publish CMW middleware data
 */

static void pubCMW(void)
{
    static uint32_t cmw_period_tick = 0;

    cmw_period_tick += FGCD_CYCLE_PERIOD_MS;

    if(cmw_period_tick >= PUB_CMW_PERIOD_MS)
    {
        // Re-initalize tick

        cmw_period_tick = 0;

        pub.cmw_sequence_number++;

        // Publish data via CMW

        cmw_globals.server->publish();
    }
}



/*
 * Copy buffer for alarms processing
 */

static void pubAlarms(void)
{
    // Check whether alarms mutex can be obtained (the alarms thread is not in the middle of processing)

    if(!pthread_mutex_trylock(&alarms.data_mutex))
    {
        memcpy(&alarms.published_data, &pub.published_data, sizeof(struct Pub_data));
        pthread_mutex_unlock(&alarms.data_mutex);

        // Trigger alarm processing

        sem_post(&alarms.sem);
    }
}



/*
 * Thread to publish data
 */

static void *pubSend(void *unused)
{
    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    // Main loop

    while(true)
    {
        pthread_testcancel();
        sem_wait(&pub.send_data_sem);
        pthread_testcancel();

        struct timeval pub_time;

        pub_time.tv_sec  = ntohl(pub.published_data.time_sec);
        pub_time.tv_usec = ntohl(pub.published_data.time_usec);

        // Do nothing if time not set

        if(pub_time.tv_sec == 0) continue;

        // Set class and data status for each device, and update the PM buffers

        for(uint32_t channel = 0; channel < FGCD_MAX_DEVS; channel++)
        {
            struct FGCD_device *device = &fgcd.device[channel];
            struct fgc_stat    *status = &pub.published_data.status[channel];

            if(device->fgc_class == FGC_CLASS_UNKNOWN)
            {
                // Device not in name file

                status->data_status &= ~FGC_DEVICE_IN_DB;
                status->data_status &= ~FGC_CLASS_VALID;
            }
            else if(status->class_id != device->fgc_class)
            {
                // Device is in name file, but class is unexpected

                status->data_status |= FGC_DEVICE_IN_DB;
                status->data_status &= ~FGC_CLASS_VALID;
            }
            else
            {
                // Device is in name file and class is valid

                status->data_status |= FGC_DEVICE_IN_DB;
                status->data_status |= FGC_CLASS_VALID;
            }

            // Check device online status

            if(device->online)
            {
                status->data_status |= FGC_DATA_VALID;
            }
            else
            {
                status->data_status &= ~FGC_DATA_VALID;
            }

            // If device is valid and online, update the Post Mortem buffers

            if(status->data_status & FGC_CLASS_VALID &&
               status->data_status & FGC_DATA_VALID)
            {
                pmPubUpdate(channel, &pub.published_data.status[channel], pub.published_data.time_sec, pub.published_data.time_usec);
            }
        }

        // Scan UDP only clients for subscriptions

        pubScanUDPClients(&pub_time);

        // Scan TCP clients for UDP subscriptions

        pubScanTCPClientsUDP(&pub_time);

        // Publish CMW middleware data

        pubCMW();

        // Copy buffer for alarms processing

        pubAlarms();
    }

    return 0;
}



// External functions

int32_t pubStart(void)
{
    FILE       *file;
    int32_t     sock_tos;                           // Socket type of service
    char        sub_file_name[FILENAME_MAX + 1];

    // Initialise semaphore

    if(sem_init(&pub.send_data_sem, 0, 0) < 0)
    {
        perror("sem_init send_data_sem");
        return 1;
    }

    // Create socket

    if((pub.sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        return 1;
    }

    // Make socket non-blocking

    if(fcntl(pub.sock, F_SETFL, fcntl(pub.sock, F_GETFL, 0) | O_NONBLOCK))
    {
        perror("fcntl");
        close(pub.sock);
        return 1;
    }

    // Set IP Type-Of-Service field for socket

    sock_tos = IPTOS_LOWDELAY;
    if(setsockopt(pub.sock, IPPROTO_IP, IP_TOS, &sock_tos, sizeof(int32_t)) < 0)
    {
        perror("setsockopt");
        close(pub.sock);
        return 1;
    }

    // Read UDP only subscriptions

    // Read global subscriptions

    if((file = fopen(PUB_UDP_GLOBAL_SUBS_FILE, "r")) != NULL)
    {
        pubReadSubs(file, PUB_UDP_GLOBAL_SUBS_FILE);
        fclose(file);
    }
    else
    {
        fprintf(stderr, "WARNING: Unable to open UDP subscription file %s\n", PUB_UDP_GLOBAL_SUBS_FILE);
    }

    // Read host subscriptions

    snprintf(sub_file_name, FILENAME_MAX + 1, "%s/%s", PUB_UDP_HOST_SUBS_PATH, fgcd.hostname);

    if((file = fopen(sub_file_name, "r")) != NULL)
    {
        pubReadSubs(file, sub_file_name);
        fclose(file);
    }

    // Start send thread

    if(fgcd_thread_create(&pub.thread, pubSend, NULL, PUB_THREAD_POLICY, PUB_THREAD_PRIORITY ,NO_AFFINITY) != 0)
    {
        close(pub.sock);
        return 1;
    }

    return 0;
}



// Only one thread should call this function

int32_t pubTrigger(void)
{
    int32_t sem_value;

    // Do nothing if semaphore has a non-zero value

    if(sem_getvalue(&pub.send_data_sem, &sem_value) != 0 || sem_value != 0) return 1;

    // Post semaphore to trigger sending of published data

    sem_post(&pub.send_data_sem);

    return 0;
}

// EOF
