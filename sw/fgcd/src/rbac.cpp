/*!
 * @file   rbac.cpp
 * @brief  Functions for interfacing with the Role-Based Access Control system
 * @author Stephen Page
 */

#include <cstdio>
#include <cstring>
#include <ctype.h>
#include <stdlib.h>

#include <cmw-rbac/authentication/Token.h>
#include <cmw-rbac/authorization/AccessManager.h>
#include <cmw-rbac/authorization/AuthorizationReport.h>
#include <cmw-rbac/authorization/AuthorizationReportUtils.h>
#include <cmw-rbac/authorization/Transaction.h>
#include <cmw-rbac/Exception.h>

#include <cmd.h>
#include <cmw.h>
#include <defconst.h>
#include <fgcd.h>
#include <fgc_errs.h>
#include <logging.h>
#include <rbac.h>
#include <tcp.h>
#include <fgcd_thread.h>
#include <devname.h>



// Global variables

struct Rbac rbac_globals;

FgcdServerRequest::FgcdServerRequest(SetRequest &originalRequest, std::string propName)
    : request(originalRequest), name(std::move(propName))
{
}

int64_t FgcdServerRequest::getId() const
{
    return request.getId();
}

Session &FgcdServerRequest::getSession() const
{
    return request.getSession();
}

const std::string FgcdServerRequest::getAccessPointName() const
{
    return request.getAccessPointName();
}

const std::string &FgcdServerRequest::getDeviceName() const
{
    return request.getDeviceName();
}

const std::string &FgcdServerRequest::getPropertyName() const
{
    return name;
}

const cmw::rda3::common::RequestContext &FgcdServerRequest::getContext() const
{
    return request.getContext();
}

cmw::data::Data &FgcdServerRequest::getData()
{
    return request.getData();
}

void FgcdServerRequest::requestCompleted()
{
    request.requestCompleted();
}

void FgcdServerRequest::requestFailed(const ServerException &exception)
{
    request.requestFailed(exception);
}

int32_t rbacInit(void)
{
    // Do nothing if already initialised

    if(rbac_globals.run) return 1;
    rbac_globals.run = 1;

    // Create RBAC access manager for RBAC

    rbac_globals.rbacOnlyAccess = new rbac::AccessManager("/dsc/local/data/", fgcd.server_name);
    rbac_globals.rbacOnlyAccess->setAccessCheckEnabled(true);
    rbac_globals.rbacOnlyAccess->setMcsCheckEnabled(false);
    rbac_globals.rbacOnlyAccess->setCheckingPolicy(rbac::CP_STRICT);

    // Create RBAC access manager for MCS

    rbac_globals.mcsOnlyAccess = new rbac::AccessManager("/dsc/local/data/", fgcd.server_name);
    rbac_globals.mcsOnlyAccess->setAccessCheckEnabled(false);
    rbac_globals.mcsOnlyAccess->setMcsCheckEnabled(true);
    rbac_globals.mcsOnlyAccess->setCheckingPolicy(rbac::CP_STRICT);

    // Initialise mutex

    if(fgcd_mutex_init(&rbac_globals.mutex, RBAC_MUTEX_POLICY) != 0) return 1;

    // Read access Map

    if(rbacReadAccessMap())
    {
        rbac_globals.run = 0;
        pthread_mutex_destroy(&rbac_globals.mutex);
        return 1;
    }

    return 0;
}



int32_t rbacReadAccessMap(void)
{
    rbac::LoadResultList results;
    rbac::LoadResultList::const_iterator i;

    if(!rbac_globals.run && rbacInit())
    {
        return 1;
    }

    // Load RBAC public key and access map

    pthread_mutex_lock(&rbac_globals.mutex);

    results = rbac_globals.rbacOnlyAccess->loadAccessMaps();

    for(i=results.begin(); i != results.end(); ++i)
    {
       // If the RBAC access map is not load

       if (i->getStatus() != rbac::LOADED)
       {
           fprintf(stderr,"WARNING: RBAC Failed to load RBAC Access Map: %s",i->getException().c_str());
           logPrintf(0,"RBAC Failed to load RBAC Access Map: %s",i->getException().c_str());
           pthread_mutex_unlock(&rbac_globals.mutex);
           return 1;
       }
    }

    results = rbac_globals.mcsOnlyAccess->loadAccessMaps();

    for(i=results.begin(); i != results.end(); ++i)
    {
        // If the MCS access map is not load

       if (i->getStatus() == rbac::NO_MAP_FILE)
       {
           logPrintf(0,"RBAC MCS Access Map does not exist. Skipping...\n");
       }
       else if (i->getStatus() != rbac::LOADED)
       {
           fprintf(stderr,"WARNING: Failed to load MCS Access Map: %s",i->getException().c_str());
           logPrintf(0,"RBAC Failed to load MCS Access Map: %s",i->getException().c_str());
           pthread_mutex_unlock(&rbac_globals.mutex);
           return 1;
       }
    }

    pthread_mutex_unlock(&rbac_globals.mutex);
    return 0;
}

static std::string cmwRbacErrorMessage(rbac::Transaction* transaction, rbac::Token *token, rbac::AuthorizationReport* report, struct Cmd* command)
{
    // Retrieve CMW request from command tag

    cmw_command_data *command_data;
    memcpy(&command_data, command->tag, sizeof(command_data));
    cmw::rda3::server::ServerRequest* request = NULL;

    switch(command->type)
    {
        case CMD_GET:
            request = static_cast<cmw::rda3::server::GetRequest*>          (command_data->rda_request);
            break;
        case CMD_SET:
            request = static_cast<cmw::rda3::server::SetRequest*>          (command_data->rda_request);
            break;
        case CMD_SUB:
            request = static_cast<cmw::rda3::server::SubscriptionRequest*> (command_data->rda_request);
            break;
        default:
            return "Failed to retrieve cmw::rda3::ServerRequest from FGCD Cmd";
    }

    // Retrieve RBAC report

    try
    {

        std::string user_name = request->getSession().getConnection().getClientInfo().getUserName();
        std::string hostname  = request->getSession().getConnection().getClientInfo().getAddress().getHost();
        int32_t     pid       = request->getSession().getConnection().getClientInfo().getProcessId();
        rbac::AuthorizationReportUtils::ExecutionContext exec(hostname, user_name, pid, fgcd.server_name);

        return rbac::AuthorizationReportUtils::buildReport(*transaction, token, exec, *report);
    }
    catch(std::exception& e)
    {
        return std::string("Failed to retrieve RBAC authorization report: ") + e.what();
    }

}

void rbacValidateMcs(struct Cmd *command, const std::string& property)
{
    if  ( (!command->client) && command->tag_length > 0 &&
          (command->type == CMD_SET || command->type == CMD_SET_BIN) )
    {
        // Retrieve the command_data
        cmw_command_data *command_data;
        memcpy(&command_data, command->tag, sizeof(command_data));

        // Cast command_data into CMW set request
        auto request = reinterpret_cast<cmw::rda3::server::SetRequest*>(command_data->rda_request);

        // Create the FgcdServerRequest instance, that implements CMW SetRequest
        FgcdServerRequest fgcdRequest(*request, property);

        // This will throw an exception on error
        cmw_globals.rbacFactory->getValidationCallback().validate(fgcdRequest, ValidationType::VALIDATE_SET);
    }
}

int32_t rbacAuthorise(struct Cmd *command)
{
    using namespace std;

    string                  class_name = "FGC_";
    char                    class_number[4];
    struct FGCD_device      *device = command->device;
    char                    device_name[FGC_MAX_DEV_LEN + 1];
    uint32_t                device_number;
    uint32_t                i;
    rbac::OperationalMode   mode;
    rbac::OperationType     op_type;
    string                  property;
    rbac::Token             *token = static_cast<rbac::Token *>(command->rbac_token);

    // Check whether RBAC has been initialised

    if(!rbac_globals.run)
    {
        command->error = FGC_NO_RBAC_RULE_FOR_PROPERTY;
        return 1;
    }

    // Set operation type

    switch(command->type)
    {
        case CMD_GET:
        case CMD_GET_SUB:
        case CMD_SUB:
            op_type         = rbac::OT_GET;
            break;

        case CMD_SET:
        case CMD_SET_BIN:
            op_type         = rbac::OT_SET;
            break;

        case CMD_UNSUB:
            return 0;

        default:
            logPrintf(device->id, "RBAC Unknown command type %u\n", command->type);
            command->error = FGC_PROTO_ERROR;
            return 1;
    }

    // Extract the property name from command

    for(i = 0 ; command->command_string[i] != '[' &&
                command->command_string[i] != '(' &&
                command->command_string[i] != '<' &&
                command->command_string[i] != ' ' &&
                command->command_string[i] != '\0' ; i++)
    {
        property.append(1, (char)toupper(command->command_string[i]));
    }

    // Allow access if property is CLIENT.TOKEN

    if(device->id == 0 &&
       !property.compare("CLIENT.TOKEN")) return 0;

    // Special handling for sets of CLIENT.RTERM
    // Authorise against remote terminal device rather than FGCD device

    if( device->id      == 0                &&
        command->type   == CMD_SET          &&
        (!property.compare("CLIENT.RTERM")  ||
         !property.compare("CLIENT.RTERMLOCK")))
    {
        // Get channel number from command value

        if(isdigit(command->value[0]))
        {
            // Device number is given as an integer

            device_number = atoi(command->value);

            if(device_number < 1 || device_number > FGCD_MAX_EQP_DEVS)
            {
                command->error = FGC_OUT_OF_LIMITS;
                return 1;
            }
        }
        else // Channel is not numeric
        {
            command->error = FGC_UNKNOWN_DEV;
            return 1;
        }

        // Override device if channel corresponds to a known device

        if(fgcd.device[device_number].fgc_class != FGC_CLASS_UNKNOWN)
        {
            device = &fgcd.device[device_number];
        }
    }

    // Set device name

    // Check whether device has a name

    if(device->name)
    {
        // If it has operational name (alias) - use it

        if(device->operational_name)
        {
            // It might be a subdevice (virtual device). Use its name in that case.
            
            if(devnameSubName(device_name, FGC_MAX_DEV_LEN + 1, device->id, command->sub_device))
            {
                // devnameSubName failed - use device's operational name.
                strncpy(device_name, device->operational_name, FGC_MAX_DEV_LEN + 1);
            }
        }
        else
        {
            strncpy(device_name, device->name, FGC_MAX_DEV_LEN + 1);
        }
        device_name[FGC_MAX_DEV_LEN] = '\0';
    }
    else // Device has no name
    {
        device_name[0] = '\0';
    }

    // Set mode

    mode = device->omode_mask & fgcd.op_mode ?
           rbac::OM_OPERATIONAL : rbac::OM_NON_OPERATIONAL;

    // Generate class name

    snprintf(class_number, sizeof(class_number), "%hhu", device->fgc_class);
    class_name.append(class_number);

    // Obtain exclusive access to access map

    pthread_mutex_lock(&rbac_globals.mutex);

    // Attempt to authorise command

    if((!command->client && (token || fgcd.force_rbac)) ||      // Not an FGC TCP client and has a token or RBAC is forced
       ( command->client && command->client != CMD_FGCD_CLIENT) // FGC TCP client and not internal FGCD command
      )
    {
        // Missing token

        if (!token)
        {
            logPrintf(device->id,"RBAC Authorization denied (%s): Missing token\n",property.c_str());
            command->error = FGC_NO_RBAC_TOKEN;
            pthread_mutex_unlock(&rbac_globals.mutex);
            return 1;
        }

        // For CMW SET commands, validate the MCS

        try
        {
            rbacValidateMcs(command, property);
        }
        catch (std::runtime_error& error)
        {
            const std::string error_msg = error.what();

            logPrintf(device->id,"RBAC Authorization denied: %s\n", error_msg.c_str());
            command->error = FGC_UNKNOWN_PROPERTY_OR_ACCESS_DENIED_BY_RBAC;
            pthread_mutex_unlock(&rbac_globals.mutex);

            // store error message in the value field
            command->value_length = snprintf(command->value, CMD_MAX_VAL_LENGTH + 1, "%s", error_msg.c_str());
            command->error_in_value = true;

            return 1;
        }

        // Create RBAC transaction

        std::unique_ptr<rbac::Transaction> transaction (new rbac::Transaction(class_name, property, device_name, op_type));

        std::unique_ptr<rbac::AuthorizationContext> context (new rbac::AuthorizationContext(token, mode));

        // Perform RBAC authorisation

        std::unique_ptr<rbac::AuthorizationReport> report_rbac
            = rbac_globals.rbacOnlyAccess->authorization(*transaction, *context);

        if (report_rbac->getAuthResult() != rbac::AR_ACCESS_GRANTED)
        {
            if(command->client || command->tag_length == 0)
            {
                // Report error for TCP clients or for commands without a tag

                logPrintf(device->id,"RBAC Authorization denied (%s %s@%s): %s: token SerialID=%d, ExpirationTime=%d, AuthenticationTime=%d\n",
                    property.c_str(),
                    token->getUserName().c_str(),
                    token->getLocationName().c_str(),
                    report_rbac->getMessage().c_str(),
                    token->getSerialID(),
                    token->getExpirationTime(),
                    token->getAuthenticationTime());
            }
            else
            {
                // Report error for CMW clients
                const std::string error_msg = cmwRbacErrorMessage(transaction.get(), token, report_rbac.get(), command);
                logPrintf(device->id,"RBAC Authorization denied: %s\n", error_msg.c_str());

                // store error message in the value field
                command->value_length = snprintf(command->value, CMD_MAX_VAL_LENGTH + 1, "%s", error_msg.c_str());
                command->error_in_value = true;
            }

            switch(report_rbac.get()->getAuthResult())
            {
                case rbac::AR_TOKEN_NOT_VERIFIED:   // Token signature is not valid
                    command->error = FGC_BAD_RBAC_TOKEN;
                    break;

                case rbac::AR_TOKEN_EXPIRED:        // Token has expired
                    command->error = FGC_EXPIRED_RBAC_TOKEN;
                    break;

                case rbac::AR_NO_TOKEN:             // No token
                    command->error = FGC_NO_RBAC_TOKEN;
                    break;

                case rbac::AR_ACCESS_DENIED:        // No access rule allows the access
                    command->error = FGC_UNKNOWN_PROPERTY_OR_ACCESS_DENIED_BY_RBAC;
                    break;

                case rbac::AR_ACCESS_IMPLICIT:      // Property is not protected
                    command->error = FGC_NO_RBAC_RULE_FOR_PROPERTY;
                    break;

                default:
                    logPrintf(0,"RBAC Fatal authorization error: This case should never happen\n");
                    command->error = FGC_SYNTAX_ERROR;
                    break;
            }
            pthread_mutex_unlock(&rbac_globals.mutex);
            return 1;
        }
    }

    //Check MCS for the tcp client

    if(command->client && command->client != CMD_FGCD_CLIENT) // FGC TCP client and not internal FGCD command
    {
        // Missing token

        if (!token)
        {
            logPrintf(device->id,"RBAC MCS Authorization denied (%s): Missing token\n",property.c_str());
            command->error = FGC_NO_RBAC_TOKEN;
            pthread_mutex_unlock(&rbac_globals.mutex);
            return 1;
        }

        // Create RBAC transaction

        std::unique_ptr<rbac::Transaction> transaction (new rbac::Transaction(class_name, property, device_name, op_type));

        std::unique_ptr<rbac::AuthorizationContext> context (new rbac::AuthorizationContext(token, mode));

        // If there is an MCS rule, then the access should be denied through the TCP interface

        std::unique_ptr<rbac::AuthorizationReport> report_mcs
            = rbac_globals.mcsOnlyAccess->authorization(*(transaction.get()), *(context.get()));

        // In the case of MCS if the file is not there there is an implicit right to access

        if ((report_mcs.get()->getAuthResult() != rbac::AR_ACCESS_GRANTED) &&
            (report_mcs.get()->getAuthResult() != rbac::AR_ACCESS_IMPLICIT))
        {
            logPrintf(device->id,"RBAC MCS Authorization denied (%s %s@%s): %s\n",
                property.c_str(),
                token->getUserName().c_str(),
                token->getLocationName().c_str(),
                report_mcs.get()->getMessage().c_str());
            command->error = FGC_UNKNOWN_PROPERTY_OR_ACCESS_DENIED_BY_RBAC;
            pthread_mutex_unlock(&rbac_globals.mutex);
            return 1;
        }

    }

    pthread_mutex_unlock(&rbac_globals.mutex);
    return 0;
}



int32_t rbacSetTCPClientToken(struct TCP_client *client, void *token_bin, long token_size)
{
    rbac::Token *token = NULL;

    // Check whether a token is already defined for the client

    rbacClearTCPClientToken(client);

    // Check token size

    if(token_size == 0) return 0;

    // Check whether access manager is initialised

    if(!rbac_globals.rbacOnlyAccess)
    {
        logPrintf(0, "RBAC Error creating token: access manager not initialised\n");
        return 1;
    }

    try
    {
        token = new rbac::Token((signed char *)token_bin, token_size);
    }
    catch(const rbac::ExceptionRBAC& ex)
    {
        logPrintf(0, "RBAC Error creating token: %s\n", ex.what());
        return 1;
    }
    catch(...)
    {
        logPrintf(0, "RBAC Unknown exception setting TCP client %d token\n", client->id);
        return 1;
    }

    // Check whether token is signed correctly

    if(!token->isSignatureVerified())
    {
        logPrintf(0, "RBAC Bad token signature setting TCP client %d token\n", client->id);
        delete token;
        return 1;
    }

    client->rbac_token          = token;
    client->rbac_token_expiry   = token->getExpirationTime();

    strncpy(client->username, token->getUserName().c_str(), TCP_AUTH_USER_SIZE);
    client->username[TCP_AUTH_USER_SIZE - 1] = '\0';

    logPrintf(0, "RBAC TCP client %d new token %s@%s\n",
              client->id,
              token->getUserName().c_str(),
              token->getLocationName().c_str());

    return 0;
}



void rbacClearTCPClientToken(struct TCP_client *client)
{
    // Check whether a token is defined for the client

    if(client->rbac_token)
    {
        rbac::Token *token          = static_cast<rbac::Token *>(client->rbac_token);
        client->rbac_token          = NULL;
        client->rbac_token_expiry   = 0;
        delete token;
    }
}

void rbacClearCommandToken(struct Cmd *cmd)
{
    // Check whether a token is defined for the client

    if(cmd->rbac_token)
    {
        rbac::Token *token = static_cast<rbac::Token *>(cmd->rbac_token);
        cmd->rbac_token          = NULL;
        delete token;
    }
}

// EOF
