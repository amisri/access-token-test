/*!
 * @file   cmwutil.cpp
 * @brief  Utility functions for CMW
 * @author Stephen Page
 */

#include <arpa/inet.h>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <ctype.h>
#include <memory>

#include <cmw-data/Data.h>
#include <cmw-data/DiscreteFunctionListBuilder.h>
#include <cmw-rbac/authentication/Token.h>
#include <cmw-rbac/Exception.h>
#include <cmw-rda3/common/exception/ServerException.h>
#include <cmw-rda3/server/service/ServerBuilder.h>
using namespace cmw::data;
using namespace cmw::rda3::common;
using namespace cmw::rda3::server;

#include <cmd.h>
#include <cmwutil.h>
#include <consts.h>
#include <devname.h>
#include <fgcd.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <logging.h>
#include <parser.h>
#include <rbac.h>
#include <timing.h>
#include <fgcd_thread.h>

struct Cmwutil cmwutil;

// Static functions

template <typename datatype>
static datatype getTypedDataItem(const char *item_ptr, bool is_host_order, int32_t &num_bytes);

static int32_t cmwutilProcessItem(uint8_t *response_buf_type, uint32_t el_idx, const char *item_ptr, const char *delim_ptr, uint32_t type, bool is_binary, bool is_host_order);



int32_t cmwutilInit(void)
{
    return 0;
}



void cmwutilExtractCommandDetails(const RequestContext& context,
                                  const char            *device,
                                  uint32_t              *channel,
                                  uint32_t              *sub_device,
                                  uint16_t              *transaction_id,
                                  uint32_t              *user_num,
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
                                  uint32_t              *getopt_bin,
#endif
                                  uint32_t              *array_start,
                                  uint32_t              *array_end,
                                  uint32_t              *array_step)
{
    const Data& filters = context.getFilters();

    // Extract channel and sub_device

    if(device && channel && sub_device)
    {
        // Resolve device name to channel number

        if(devnameSubResolve((char *)device, channel, sub_device))
        {
            *channel    = devnameResolve((char *)device);
            *sub_device = 0;
        }

        // Check whether channel is invalid

        if(*channel < 0 || *channel > FGCD_MAX_EQP_DEVS)
        {
            throw(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_UNKNOWN_DEV]));
        }
    }

    // Extract transaction ID

    if(transaction_id)
    {
        *transaction_id = filters.exists("transactionId") ?
                          filters.getInt("transactionId") : 0;
    }

    // Extract user number

    if(user_num)
    {
        const char *selector = context.getSelector().c_str();

        // Set user if defined

        if(selector[0] != '\0') // Selector is not a zero-length string
        {
            // Translate selector string to user number

            if(cmwutilParseSelector(selector, user_num))
            {
                logPrintf(0, "CMW Unknown cycle [%s]\n", selector);
                throw(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_UNKNOWN_CYCLE]));
            }
        }
        else // No selector was defined
        {
            *user_num = 0;
        }
    }

    // Check whether array indices have been specified

    try
    {
        if(array_start)
        {
            *array_start    = filters.exists("filterStartIndex") ?
                              filters.getInt("filterStartIndex") :
                              UNSPECIFIED;
        }

        if(array_end)
        {
            *array_end      = filters.exists("filterEndIndex") ?
                              filters.getInt("filterEndIndex") :
                              UNSPECIFIED;
        }

        if(array_step)
        {
            *array_step     = filters.exists("filterStepIndex") ?
                              filters.getInt("filterStepIndex") :
                              UNSPECIFIED;
        }
    }
    catch(std::exception& e)
    {
        logPrintf(*channel,"CMW Extracting filer indices from command failed: %s\n",e.what());
        throw(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_BAD_ARRAY_IDX]));
    }
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
    // Check whether get options have been specified

    try
    {
        if(getopt_bin)
        {
            *getopt_bin = filters.exists( "getopt_bin")         ?
                          filters.getBool("getopt_bin") ? 1 : 0 :
                          0;
        }
    }
    catch(std::exception& e)
    {
        logPrintf(*channel,"CMW Extracting getopt_bin option filter form command failed: %s\n",e.what());
        throw(ServerException(std::string("FGC Device: ") + fgc_errmsg[FGC_BAD_GET_OPT]));
    }
#endif
}



rbac::Token *cmwutilExtractRBACToken(const Data& context_data)
{

    // Return NULL if RBAC is not enabled

    if(!fgcd.enable_rbac || !rbac_globals.rbacOnlyAccess)
    {
        return NULL;
    }

    // Check for the existance of a token

    if(!context_data.exists("rbacToken"))
    {
        return NULL;
    }

    // Extract the token

    size_t          token_len = 0;
    const int8_t *  token_buf = context_data.getArrayByte("rbacToken", token_len);

    try
    {
        return new rbac::Token(token_buf, token_len);
    }
    catch(const rbac::ExceptionRBAC& e)
    {
        logPrintf(0, "RBAC error creating token %s\n", e.what());
    }
    catch(...)
    {
        logPrintf(0, "RBAC unknown exception creating CMW token\n");
    }

    return NULL;
}



uint32_t cmwutilParseSelector(const char *selector, uint32_t *user_num)
{
    char    machine[4];
    char    type[16];
    char    value[16];

    sscanf(selector, "%3[^.].%15[^.].%15s", machine, type, value);

    // Check that machine matches the current machine

    if(strcmp(machine, fgcd_machine_name[fgcd.machine]))
    {
        return 1;
    }

    // Handle type USER

    if(!strcmp(type, "USER"))
    {
        if(!strcmp(value, "ALL"))
        {
            *user_num = 0;
            return 0;
        }
        else if((*user_num = timingUserNumber(value)))
        {
            return 0;
        }
    }

    // Failed to interpret selector

    return 1;
}

static void cmwutilAppendFunctionsToBuilder(std::unique_ptr<DiscreteFunctionListBuilder>& builder, double* xarray, double* yarray, uint32_t n)
{
    double   xoffset = 0;
    uint32_t flength = 1;
    uint32_t fstart  = 0;

    for(uint32_t i = 0; i != n; ++i, ++flength)
    {
        // Change the time offset

        if( (i == (n-1)) ||
            (xarray[i] == xarray[i+1]))
        {
            // Begining of new function, or end of the last function

            // Retrieve the new offset

            double next_xoffset = xarray[i];

            // Remove the current time offset

            xarray[i] -= xoffset;

            // Build and append function to discrete Function List builder

            std::unique_ptr<const cmw::data::DiscreteFunction> function =
                cmw::data::DataFactory::createDiscreteFunction(&xarray[fstart],
                                                               &yarray[fstart],
                                                               flength,
                                                               DF_COPY_POLICY);
            builder->append(std::move(function));

            // Reset function length

            flength = 0;

            // Set the new offset

            xoffset = next_xoffset;

            // Set the new function start index

            fstart = i+1;
        }
        else
        {
            // Remove the current time offset

            xarray[i] -= xoffset;
        }
    }
}

std::unique_ptr<const cmw::data::DiscreteFunction> cmwutilCreateFunction(double* xarray, double* yarray, uint32_t n)
{
    // Check that the function does no have repeated time coordinates

    for(uint32_t i = 0; i < n-1; ++i)
    {
        if(xarray[i] == xarray[i+1])
        {
            // HACK
            // Instead of throwing an error in this case, we return an empty discrete function.
            // This will avoid protocol errors when a client requests REF.TABLE or REF.TABLE.FUNCTION
            // on an FGC driven by a REF.TABLE.FUNCLIST

            return cmw::data::DataFactory::createDiscreteFunction(cmwutil.response_xarray,cmwutil.response_yarray,0,DF_COPY_POLICY);
        }
    }

    return cmw::data::DataFactory::createDiscreteFunction(cmwutil.response_xarray,cmwutil.response_yarray,n,DF_COPY_POLICY);
}

AcquiredData *cmwutilResponseToRdaData(struct Cmd *command)
{
    using namespace std;

    const char *c;
    char        field[FGC_MAX_PROP_LEN + 1];
    uint32_t    field_idx;
    uint32_t    type;
    char        user[32] = "";
    std::string tag      = "";

    // Lock the mutex for the response_buf, response_buf_type, and the timingGetCycleTimestamp static variable

    pthread_mutex_lock(&cmwutil.response_buf_mutex);

    // Set cycle name if command is PPM

    if(command->user)
    {
        c = timingUserName(command->user);
        if(strlen(c) != 0)
        {
            snprintf(user, sizeof(user), "%s.USER.%s", fgcd_machine_name[fgcd.machine], c);
        }
        else // User was not known
        {
            command->error = FGC_UNKNOWN_CYCLE;
            pthread_mutex_unlock(&cmwutil.response_buf_mutex);
            return NULL;
        }
    }

    int64_t cycle_stamp = 0;

    // Set cycle timestamp if acquisition timestamp is supplied
    if(command->acq_timestamp.tv_sec)
    {
        // We set cycle timestamp if the property is PPM or is coming from a remote device (e.g. FGC3)
        if (!command->zero_cycle_timestamp)
        {
            timingGetCycleTimestamp(&command->acq_timestamp, &command->cycle_timestamp);

            cycle_stamp = ((int64_t) command->cycle_timestamp.tv_sec * 1000000000) +
                          ((int64_t) command->cycle_timestamp.tv_usec * 1000);
        }
    }
    else
    {
        // If the acquisition timestamp is 0, we add it here (it will be zero from FGC3 if the property is a setting -
        // in that case acq timestamp of 0 is used to distinguish between a setting and acquisition (to know whether
        // to include the cycle timestamp).
        timingGetUserTime(0, &command->acq_timestamp);
    }

    // Calculate acquisition timestamp
    int64_t acq_stamp = ((int64_t)command->acq_timestamp.tv_sec  * 1000000000) +
            ((int64_t)command->acq_timestamp.tv_usec * 1000);

    // Prepare context
    auto context = AcquiredContextFactory::create(acq_stamp, user, cycle_stamp);


    // Prepare value

    unique_ptr<Data> value = DataFactory::createData();

    // If the command came from the FGCD device, binary data items are in host order (little-endian).
    // Otherwise, they came from an FGC device and are in network order (big-endian).

    bool is_host_order = (command->device->id == 0);

    // copy command buffer so we can overwrite delimiters with NULLs

//    for (int i = 0; i < command->value_length; ++i)
//    {
//        printf("%c", command->value[i]);
//    }


    memcpy(cmwutil.response_buf, command->value, command->value_length);
    cmwutil.response_buf[command->value_length] = '\0';

    // Process each line (field) of response:
    // Outer loop: process data one field at-a-time

    char *item_ptr  = cmwutil.response_buf;    // pointer to start of next data item
    char *delim_ptr = item_ptr;                // pointer to next item delimiter in the ASCII buffer

    for(field_idx = 0; delim_ptr < cmwutil.response_buf + command->value_length; ++field_idx)
    {
        // Read field label

        if((delim_ptr = strchr(item_ptr, ':')) == NULL || delim_ptr - item_ptr > FGC_MAX_PROP_LEN)
        {
            command->error = FGC_PROTO_ERROR;
            logPrintf(command->device->id, "CMW FGC protocol error - field %u missing label (i = %d)\n", field_idx, item_ptr - cmwutil.response_buf);
            logPrintf(command->device->id, "CMW rsp %d [%s]\n", command->value_length, command->value);
            pthread_mutex_unlock(&cmwutil.response_buf_mutex);
            return NULL;
        }
        *delim_ptr = '\0';
        strcpy(field, item_ptr);

        // check for empty property label

        if(strlen(field) == 0)
        {
            command->error = FGC_PROTO_ERROR;
            logPrintf(command->device->id, "CMW FGC protocol error - property label is missing for field %u (i = %d)\n", field_idx, item_ptr - cmwutil.response_buf);
            logPrintf(command->device->id, "CMW rsp %d [%s]\n", command->value_length, command->value);
            pthread_mutex_unlock(&cmwutil.response_buf_mutex);
            return NULL;
        }

        // check for same label

        if(!strcmp(field,tag.c_str()))
        {
            command->error = FGC_PROTO_ERROR;
            logPrintf(command->device->id, "CMW FGC protocol error - property label '%s' repeated for field %u (i = %d)\n", field, field_idx, item_ptr - cmwutil.response_buf);
            logPrintf(command->device->id, "CMW rsp %d [%s]\n", command->value_length, command->value);
            pthread_mutex_unlock(&cmwutil.response_buf_mutex);
            return NULL;
        }

        tag = field;
        item_ptr = delim_ptr + 1;

        // Read type string

        if((delim_ptr = strchr(item_ptr, ':')) == NULL)
        {
            command->error = FGC_PROTO_ERROR;
            logPrintf(command->device->id, "CMW FGC protocol error - field %u [%s] missing type (i = %d)\n", field_idx, field, item_ptr - cmwutil.response_buf);
            logPrintf(command->device->id, "CMW rsp %d [%s]\n", command->value_length, command->value);
            pthread_mutex_unlock(&cmwutil.response_buf_mutex);
            return NULL;
        }
        *delim_ptr = '\0';

        // Check whether the first character of the type string is lowercase, meaning that the value is scalar

        bool type_is_scalar = islower(item_ptr[0]);

        // Find type in prop_type_e

        for(type = 0 ; type < NUM_PROP_TYPE && strcasecmp(item_ptr, prop_type_name[type]) ; type++);

        // Default to string for unknown types

        if(type == NUM_PROP_TYPE) type = PT_STRING;

        // #### TEMPORARY ####
        //
        // Identify the property REF.FAULT_USER and force type to be PT_CYCSEL. This can
        // be removed once Class 63 has been updated everywhere at the end of 2022.

        if(strncasecmp(command->command_string,"REF.FAULT_USER",14) == 0) type = PT_CYCSEL;

        // in case we have to return a char/string which has not been set

        cmwutil.response_buf_type[0] = '\0';

        // Inner loop: Find each item delimiter and process data one item at-a-time

        uint32_t el_idx = 0;

        for(bool is_end_of_field = false; !is_end_of_field; )
        {
            bool    is_binary = false;
            int32_t length    = 1;

            item_ptr = delim_ptr + 1;

            if(*(reinterpret_cast<uint8_t*>(item_ptr)) == 0xFF)
            {
                // special handling for binary data

                is_binary = true;
                length = *(reinterpret_cast<uint32_t*>(++item_ptr));

                // the binary blob length is always stored in network order, even for properties in the FGCD

                length = ntohl(length);
                item_ptr += sizeof(length);

                // check length is valid

                if(static_cast<uint32_t>(item_ptr - cmwutil.response_buf) + length > CMD_MAX_VAL_LENGTH)
                {
                    command->error = FGC_PROTO_ERROR;
                    logPrintf(command->device->id, "CMW FGC protocol error - invalid binary data length %d (i = %d)\n", length, item_ptr - cmwutil.response_buf);
                    pthread_mutex_unlock(&cmwutil.response_buf_mutex);
                    return NULL;
                }
                delim_ptr = item_ptr + length;

                if(*delim_ptr != ',' && *delim_ptr != '\n' && *delim_ptr != '\0')
                {
                    command->error = FGC_PROTO_ERROR;
                    logPrintf(command->device->id, "CMW FGC protocol error - invalid delimiter \'%c\' to (i = %d)\n", *delim_ptr, item_ptr - cmwutil.response_buf);
                    pthread_mutex_unlock(&cmwutil.response_buf_mutex);
                    return NULL;
                }
            }
            else if((delim_ptr = strpbrk(item_ptr, ",\n")) == NULL) // find the next item or field delimiter
            {
                // if we didn't find a delimiter, this must be the last item of the last field

                delim_ptr = cmwutil.response_buf + command->value_length;
            }

            // are there more items in this field?

            if(*delim_ptr != ',') is_end_of_field = true;

            // delimit the current data item with \0. This is only strictly necessary in the case of char/string data.

            *delim_ptr = '\0';

            // skip adjacent delimiters with no data between them

            if(delim_ptr - item_ptr == 0) continue;

            while(length > 0)
            {
                int32_t num_chars;

                // process the current item into the typed buffer

                num_chars = cmwutilProcessItem(cmwutil.response_buf_type, el_idx, item_ptr, delim_ptr, type, is_binary, is_host_order);

                length   -= num_chars;
                item_ptr += num_chars;

                ++el_idx;

                if(num_chars < 0 || (is_binary && length < 0))
                {
                    command->error = FGC_PROTO_ERROR;
                    if(is_binary)
                    {
                        logPrintf(command->device->id, "CMW FGC protocol error - failed to convert binary data to %s (i = %d)\n", prop_type_name[type], item_ptr - cmwutil.response_buf);
                    }
                    else
                    {
                        logPrintf(command->device->id, "CMW FGC protocol error - failed to convert \"%s\" to %s\n", item_ptr, prop_type_name[type]);
                    }
                    pthread_mutex_unlock(&cmwutil.response_buf_mutex);
                    return NULL;
                }
            }
        }

        // Advance to next field for the next iteration

        item_ptr = delim_ptr + 1;

        // Check whether value has a scalar type, but zero length

        if(type_is_scalar && el_idx == 0 &&
           type != PT_CHAR && type != PT_STRING) // CHAR and STRING scalars may have zero length
        {
            command->error = FGC_EMPTY_SCALAR_VALUE;
            pthread_mutex_unlock(&cmwutil.response_buf_mutex);
            return NULL;
        }

        // Insert typed data into rdaData

        switch(type)
        {
            case PT_ABSTIME:
            case PT_ABSTIME8:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<double *>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<double *>(cmwutil.response_buf_type), el_idx);
                }
                break;

            case PT_CHAR:
                value->append(tag, reinterpret_cast<char *>(cmwutil.response_buf_type));
                break;

            case PT_FLOAT:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<float *>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<float *>(cmwutil.response_buf_type), el_idx);
                }
                break;

            case PT_INT8S:
            case PT_LOG:
            case PT_RUNLOG:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<int8_t *>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<int8_t *>(cmwutil.response_buf_type), el_idx);
                }
                break;

            case PT_INT8U:
            case PT_INT16S:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<int16_t *>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<int16_t *>(cmwutil.response_buf_type), el_idx);
                }
                break;

            case PT_INT16U:
            case PT_INT32S:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<int32_t *>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<int32_t *>(cmwutil.response_buf_type), el_idx);
                }
                break;

            case PT_INT32U:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<int64_t *>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<int64_t *>(cmwutil.response_buf_type), el_idx);
                }
                break;

            case PT_POINT:
                try
                {
                    value->appendMove(tag, cmwutilCreateFunction(cmwutil.response_xarray,cmwutil.response_yarray,el_idx));
                }
                catch(std::exception& e)
                {
                    command->error = FGC_PROTO_ERROR;
                    logPrintf(command->device->id, "CMW FGC protocol error - Failed to create DiscreteFunction: %s\n", e.what());
                    pthread_mutex_unlock(&cmwutil.response_buf_mutex);
                    return NULL;
                }
                break;

            case PT_FL_POINT:
                try
                {
                    unique_ptr<DiscreteFunctionListBuilder> builder = DataFactory::createDiscreteFunctionListBuilder();
                    cmwutilAppendFunctionsToBuilder(builder, cmwutil.response_xarray, cmwutil.response_yarray,el_idx);
                    value->appendMove(tag, builder->build());
                }
                catch(std::exception& e)
                {
                    command->error = FGC_PROTO_ERROR;
                    logPrintf(command->device->id, "CMW FGC protocol error - Failed to create DiscreteFunctionList: %s\n", e.what());
                    pthread_mutex_unlock(&cmwutil.response_buf_mutex);
                    return NULL;
                }
                break;

            case PT_CYCSEL:
            case PT_STRING:
            case PT_UNIXTIME:
            default:
                if(type_is_scalar)
                {
                    value->append(tag, reinterpret_cast<const char **>(cmwutil.response_buf_type)[0]);
                }
                else // Type is an array
                {
                    value->appendArray(tag, reinterpret_cast<const char **>(cmwutil.response_buf_type), el_idx);
                }
                break;
        }
    }

    pthread_mutex_unlock(&cmwutil.response_buf_mutex);

    AcquiredData *return_object = new AcquiredData(std::move(value), std::move(context));

    return return_object;
}



// Static functions



/*
 * Generic function to convert untyped bytes to typed data in the correct order.
 * Reads one binary item from the buffer, performs network to host order conversion
 * if necessary, and returns a typed pointer to the data item. The size of the item
 * is returned in num_bytes.
 */

template <typename datatype>
static datatype getTypedDataItem(const char *item_ptr, bool is_host_order, int32_t &num_bytes)
{
    num_bytes = sizeof(datatype);

    char buf[num_bytes];

    if(!is_host_order)
    {
        for(int32_t i = 0; i < num_bytes; ++i)
        {
            buf[i] = *(item_ptr + num_bytes - (i+1));
        }

        item_ptr = buf;
    }

    return *(reinterpret_cast<const datatype*>(item_ptr));
}



/*
 * Process one item from ASCII buffer (response_buf) into typed buffer (response_buf_type)
 */

static int32_t cmwutilProcessItem(uint8_t *response_buf_type, uint32_t el_idx, const char *item_ptr, const char *delim_ptr, uint32_t type, bool is_binary, bool is_host_order)
{
    char    *end       = const_cast<char *>(delim_ptr);
    int32_t  num_chars = 1;
    int32_t  user;

    errno = 0;

    switch(type)
    {
        case PT_ABSTIME:
        case PT_ABSTIME8:
            reinterpret_cast<double *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<double>(item_ptr, is_host_order, num_chars) : strtod(const_cast<char *>(item_ptr), &end);
            break;

        case PT_CHAR:
            strcpy(reinterpret_cast<char *>(&response_buf_type[el_idx]), item_ptr);
            break;

        case PT_FLOAT:
            reinterpret_cast<float *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<float>(item_ptr, is_host_order, num_chars) : static_cast<float>(strtod(item_ptr, &end));
            break;

        case PT_INT8S:
        case PT_LOG:
        case PT_RUNLOG:
            reinterpret_cast<int8_t *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<int8_t>(item_ptr, is_host_order, num_chars) : static_cast<int8_t>(strtol(item_ptr, &end, 0));
            break;

        case PT_INT8U: // no unsigned type so we need to go up to the next size type (16 bits)
            reinterpret_cast<int16_t *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<uint8_t>(item_ptr, is_host_order, num_chars) : static_cast<int16_t>(strtol(item_ptr, &end, 0));
            break;

        case PT_INT16S:
            reinterpret_cast<int16_t *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<int16_t>(item_ptr, is_host_order, num_chars) : static_cast<int16_t>(strtol(item_ptr, &end, 0));
            break;

        case PT_INT16U: // no unsigned type so we need 32 bits
            reinterpret_cast<int32_t *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<uint16_t>(item_ptr, is_host_order, num_chars) : strtol(item_ptr, &end, 0);
            break;

        case PT_INT32S:
            reinterpret_cast<int32_t *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<int32_t>(item_ptr, is_host_order, num_chars) : strtol(item_ptr, &end, 0);
            break;

        case PT_INT32U: // no unsigned type so we need 64 bits
            reinterpret_cast<uint64_t *>(response_buf_type)[el_idx] =
                is_binary ? getTypedDataItem<uint32_t>(item_ptr, is_host_order, num_chars) : strtoul(item_ptr, &end, 0);
            break;

        case PT_CYCSEL:
            user = is_binary ? getTypedDataItem<int32_t>(item_ptr, is_host_order, num_chars) : strtol(item_ptr, &end, 0);
            reinterpret_cast<const char **>(response_buf_type)[el_idx] = timingUserName(user);
            break;

        case PT_POINT:
        case PT_FL_POINT:
            if(is_binary)
            {
                cmwutil.response_xarray[el_idx] = static_cast<double>(getTypedDataItem<float>(item_ptr, is_host_order, num_chars));
                cmwutil.response_yarray[el_idx] = static_cast<double>(getTypedDataItem<float>(item_ptr + sizeof(float), is_host_order, num_chars));
                num_chars *= 2;
            }
            else
            {
                sscanf(item_ptr, "%lf|%lf", &cmwutil.response_xarray[el_idx], &cmwutil.response_yarray[el_idx]);
            }
            break;

        case PT_STRING:
        case PT_UNIXTIME:
        default:
            reinterpret_cast<const char **>(response_buf_type)[el_idx] = item_ptr;
            break;
    }

    // Check for error conditions: ASCII conversion failed, or binary length is not an integer multiple of the data size

    if(errno || *end != '\0')
    {
        return -1;
    }

    // If we are processing ASCII items, return 1. If we are processing binary items, return the number of chars processed for the current item.

    return num_chars;
}

// EOF
