/*!
 * @file   hash.c
 * @brief  Functions for hashes
 * @author Stephen Page
 */

#include <string.h>
#include <stdlib.h>

#include <hash.h>

// Static functions

static uint32_t hashHash(union hash_key *key, size_t size);
static void hashNextPrime(size_t *target);



struct hash_table* hashInit(size_t size)
{
    struct hash_table *table;

    // Adjust size to next prime number

    hashNextPrime(&size);

    // Allocate memory for table structure

    if(!(table = (struct hash_table *)malloc(sizeof(struct hash_table))))
    {
        // Unable to allocate memory

        return NULL;
    }

    // Allocate memory for table data

    if(!(table->data = (struct hash_entry **)calloc(size, sizeof(struct hash_entry *))))
    {
        // Free previously allocated data

        free(table);

        // Unable to allocate memory

        return NULL;
    }

    table->size = size;

    return table;
}



void hashEmpty(struct hash_table *table)
{
    size_t i;

    for(i = 0 ; i < table->size ; i++)
    {
        table->data[i] = NULL;
    }
}



void hashFree(struct hash_table *table)
{
    if(table != NULL)
    {
        if(table->data != NULL) free(table->data);
        free(table);
    }
}



void hashInsert(struct hash_table *table, struct hash_entry *entry)
{
    size_t              index;
    struct hash_entry   *node;

    entry->next = NULL;

    // Perform the hash

    index = hashHash(&entry->key, table->size);

    // Insert the entry, checking for collisions

    if(table->data[index] == NULL)
    {
        // Insert the entry at the start

        table->data[index] = entry;
    }
    else
    {
        // Follow the chain until the end

        for(node = table->data[index] ; node->next != NULL ; node = node->next);

        // Add the entry onto the end

        node->next = entry;
    }
}



struct hash_entry *hashFind(struct hash_table *table, char *key_string)
{
    uint32_t            i;
    size_t              index;
    union hash_key      key;
    struct hash_entry   *node;

    // Zero fill the key

    memset(key.c, 0, HASH_MAX_KEY_LEN);

    // Copy the key string into the key

    strncpy(key.c, key_string, HASH_MAX_KEY_LEN);
    key.c[HASH_MAX_KEY_LEN] = '\0';

    // Perform the hash

    index = hashHash(&key, table->size);

    // Check whether there are no matches for the hash

    if(table->data[index] == NULL)
    {
        return NULL;
    }

    // Follow the chain of nodes checking each for a match

    for(node = table->data[index] ; node != NULL ; node = node->next)
    {
        // Check whether the values match

        for(i = 0 ; (i < ((HASH_MAX_KEY_LEN + 1) / sizeof(key.i[0]))) &&
                    (node->key.i[i] == key.i[i]) ; i++)
        {
            // Is this a match?

            if(i == ((HASH_MAX_KEY_LEN + 1) / sizeof(key.i[0])) - 1)
            {
                // Match found, return value

                return node;
            }
        }
    }

    // No match was found, return NULL

    return NULL;
}



struct hash_entry *hashRemove(struct hash_table *table, char *key_string)
{
    uint32_t            i;
    size_t              index;
    union hash_key      key;
    struct hash_entry   *node;
    struct hash_entry   *parent;

    // Zero fill the key

    memset(key.c, 0, HASH_MAX_KEY_LEN);

    // Copy the key string into the key

    strncpy(key.c, key_string, HASH_MAX_KEY_LEN);
    key.c[HASH_MAX_KEY_LEN] = '\0';

    // Perform the hash

    index = hashHash(&key, table->size);

    // Check whether there are no matches for the hash

    if(table->data[index] == NULL)
    {
        return NULL;
    }

    // Follow the chain of nodes checking each for a match

    for(parent = NULL, node = table->data[index] ; node != NULL ; parent = node, node = node->next)
    {
        // Check whether the values match

        for(i = 0 ; (i < ((HASH_MAX_KEY_LEN + 1) / sizeof(key.i[0]))) &&
                    (node->key.i[i] == key.i[i]) ; i++)
        {
            // Is this a match?

            if(i == ((HASH_MAX_KEY_LEN + 1) / sizeof(key.i[0])) - 1)
            {
                // Match found

                // Check whether node has a parent or is a root node

                if(parent) // Node has a parent
                {
                    // Set parent's child to node's child

                    parent->next = node->next;
                }
                else // Node is a root node
                {
                    // Remove reference to node

                    table->data[index] = NULL;
                }

                return node;
            }
        }
    }

    // No match was found, return NULL

    return NULL;
}



// Static functions



/*
 * Hash a key
 */

static uint32_t hashHash(union hash_key *key, size_t size)
{
    uint32_t hash = 0;
    uint32_t i;

    for(i = 0 ; i < ((HASH_MAX_KEY_LEN + 1) / sizeof(key->i[0])) ; i++)
    {
        hash += key->i[i];
    }
    hash %= size;

    return hash;
}



/*
 * Find the next prime number
 */

static void hashNextPrime(size_t *target)
{
    size_t  i;
    int32_t is_prime = 0;

    for( ; !is_prime ; (*target)++)
    {
        // Set is_prime to 1 initially

        is_prime = 1;

        // Check for factors

        for(i = 2 ; is_prime && i < (*target / 2) ; i++)
        {
            if(*target % i == 0)
            {
                is_prime = 0;
            }
        }
    }
    (*target)--;
}

// EOF
