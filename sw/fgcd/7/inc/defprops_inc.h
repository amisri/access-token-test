/*!
 * @file   defprops_inc.h
 * @brief  Includes header files needed by defprops.h
 * @author Marc Magrans de Abril
 */

#ifndef DEFPROPS_INC_H
#define DEFPROPS_INC_H

#include <alarms.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <hash.h>
#include <prop.h>
#include <prop_class.h>
#include <tcp.h>
#include <timing.h>
#include <version.h>

#endif

// EOF
