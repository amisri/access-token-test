//! @file  logOutput.h
//! @brief Converter Control Logging library functions for outputting a read request into a buffer.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

// Global functions declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Process the read_control structure to return up to one block of log data.
//!
//! This function uses the contents of read_control returned by a call to logReadRequest() to extract
//! up to a block of data from the log. It can be called repeatedly to read all the blocks.
//! The size of the block is up to the application.
//!
//! @param[in]   read_control   Pointer to read_control structure
//! @param[out]  buffers        Pointer to the start of all the liblog circular buffers
//! @param[out]  blk_buf        Pointer to start of the block buffer to receive the log data
//! @param[in]   blk_len        Length of the block buffer in elements of uint32_t
//!
//! @returns     data_len - The number of elements of log data copied into the block buffer

uint32_t logOutput(struct LOG_read_control * read_control,
                   uint32_t                * buffers,
                   uint32_t                * blk_buf,
                   uint32_t                  blk_len);

#ifdef __cplusplus
}
#endif

// EOF
