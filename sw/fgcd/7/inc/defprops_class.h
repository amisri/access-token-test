//! @file   defprops_class.h
//! @brief  Includes defconst, definfo and defprops for the class 93


#ifndef DEFPROPS_CLASS_H
#define DEFPROPS_CLASS_H

#include <classes/93/defconst.h>
#include <classes/93/definfo.h>
#include <classes/93/defprops.h>

#endif

// EOF
