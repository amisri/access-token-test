/*!
 * @file   state_op.h
 * @brief  Manage Operational state for ISEG devices
 * @author Jose M. de Paco
 */

#ifndef STATE_OP_H
#define STATE_OP_H

#include <stdint.h>

#include <classes/93/defconst.h>

// Operational states

enum OP_state
{
    OP_UC        = FGC_OP_UNCONFIGURED,
    OP_NL        = FGC_OP_NORMAL,
    OP_SM        = FGC_OP_SIMULATION,
};


//! Initialize STATE.OP for a channel to be UNCONFIGURED
//!
//! @param[in]    channel    ISEG device address

void stateOpInit(uint32_t channel);




/*!
 * State and transition function typedefs
 */

typedef enum OP_state  OpState     (uint8_t channel, bool first_call);  //!< Type for state function
typedef OpState *      OpTransition(uint8_t channel);                   //!< Type for transition condition function



/*!
 * Operational state machine
 *
 * This function will drive the OP state machine
 */

void stateOpUpdate(uint8_t channel);

#endif

// EOF
