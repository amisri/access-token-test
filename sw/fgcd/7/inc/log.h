/*!
 * @file   equipdev.h
 * @brief  Declarations for signal logging
 * @author Miguel Hermo Serans
 */
#include <inttypes.h>
#include <consts.h>
#include <cclibs.h>

#ifndef CC_LOG_H
#define CC_LOG_H

/*!
 * Bind CCLIBS analogue and digital logging parameters to class data structures and initialise values.
 *
 * @param[in]    channel    address of the ISEG device
 *
 */
void logInitClass(uint8_t channel);

/*!
 * Update the CCLIB logs
 */
uint32_t logSignals(uint8_t channel, const struct CC_us_time * const us_time);



#endif
