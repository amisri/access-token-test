/*!
 * @file   equipdev.h
 * @brief  Declarations for equipment devices for the ISEG FGC
 * @author Marc Magrans de Abril, JM de Paco
 */

#ifndef EQUIPDEV_H
#define EQUIPDEV_H

#include <stdbool.h>
#include <sys/time.h>
#include <fgc_stat.h>
#include <fgc_errs.h>
#include <consts.h>
#include <timing.h>
#include <state_pc.h>
#include <state_op.h>
#include <classes/93/defconst.h>

#include <equip_liblog.h>
#include <equip_libevtlog.h>
#include <equip_common_93.h>
//#include <equip_common.h>
#include "classes/93/logMenus.h"   //!< Auto-generated liblog header
#include "classes/93/logStructs.h" //!< Auto-generated liblog header




// Forward declarations

struct Parser;
struct prop;

// Types and Constants

enum { EQUIPDEV_CLASS_ID  = 93 };                           //!< FGC class ID for equipment devices
enum { EQUIPDEV_MAX_PROPS = 800 };                          //!< Maximum number of properties < N_PROP_SYMBOLS



/*!
 * Bitmask values for Equipdev_channel::state_flags field
 */

enum Equipdev_device_state
{
    DEVICE_STATE_RESET_CONFIG = 1,                          //!< Reload device configuration from non-volatile storage after a reset.
    DEVICE_STATE_SLOW_ABORT   = 2                           //!< Latched SLOW_ABORT state. This can be cleared by restoring PC_PERMIT (not ISEG) and setting MODE.OP.
};



/*!
 * Struct containing PPM variables for each device
 */

struct Equipdev_channel_ppm                                 //!< PPM variables
{
    uintptr_t       prop_num_els[EQUIPDEV_MAX_PROPS];       //!< Numbers of elements for device properties

    float           pulse_v_ref;                            //!< REF.PULSE.REF.VALUE property
    float           pulse_v_ref_prev;                       //!< Previous value of REF used by state function CYCLING. Used as not PPM -> [0]
    float           pulse_duration;                         //!< REF.PULSE.DURATION property

    uint32_t        pulse_units;                            //!< REF.PULSE.REF.VALUE_UNITS property

    float           cycling_v_ref;                          //!< Value of the voltage to be applied in cycling. Used as not PPM -> [0]

    float           acq_value;                              //!< Last value of the voltage measurement at the moment of the event.


    // Transactional settings support

    uint16_t                        transaction_id;                 //!< Transaction ID for transactional settings

};


/*!
 * LOG properties
 */

struct Equipdev_channel_log
{
    struct LOG_menus                    menus;              //!< liblog menus
    struct LOG_mgr                      mgr;                //!< liblog manager
    struct LOG_read                     read;               //!< liblog readout
    struct LOG_read_control             read_control;       //!< liblog read control
    struct LOG_structs                  structs;            //!< liblog structures
    struct LOG_buffers                  buffers;            //!< liglog buffers
    struct timeval                      post_mortem_time;   //!< Post mortem time

    uint16_t     pm_state;                                  //!< LOG.PM.STATE
    uint16_t     pm_trig;                                   //!< LOG.PM.TRIG
};



/*!
 * MEAS properties
 */

struct Equipdev_channel_meas
{
    uint32_t                            sim;                //!< MEAS.SIM property
    uint32_t                            mode_sim_meas;      //!< Set to MODE.OP == SIMULATION && MEAS.SIM == ENABLED

    float                               i_meas;             //!< Current measurement signal
    float                               v_meas;             //!< Voltage measurement signal
};

struct Equipdev_channel_ref                                 //!< References
{
    float           vcv;                                    //!< Voltage reference set by operators.
    float           vcv_prev;                               //!< Voltage reference previous value

    uint32_t        event_cyc;                              //!< Timing event type associated to the CYCLING
};


/*!
 * LOAD properties
 */

struct Equipdev_channel_load_limits_v
{
    float       min;                    //!< Stand-by minimum voltage: property not instantiated (not used). Value is always 0.
    float       neg;                    //!< Voltage limit negative
    float       pos;                    //!< Voltage limit positive
};

struct Equipdev_channel_load_limits_i
{
    float       min;                    //!< Stand-by minimum current
    float       neg;                    //!< Current limit negative
    float       pos;                    //!< Current limit positive
};

struct Equipdev_channel_load_limits
{
    struct Equipdev_channel_load_limits_v  v;               //!< Voltage Limits
    struct Equipdev_channel_load_limits_i  i;               //!< Current Limits
};

struct Equipdev_channel_load
{
    struct Equipdev_channel_load_limits limits;                  //!< Limits
};


/*!
 * STATE PC management
 */

struct Equipdev_channel_PC_state_mgr
{
    uint32_t                    pc_state_counter;          //!< Counter incremented after each execution of the PC state machine
    enum PC_state               pc_state;                  //!< PC state enum
    enum PC_state               pc_state_prev;             //!< Previous PC state enum
    PcState                     *pc_state_func;            //!< Pointer to PC state function
    uint32_t                    pc_state_mask;             //!< PC state bit mask (1 bit only will be active)
};


/*!
 * STATE OP management
 */

struct Equipdev_channel_OP_state_mgr
{
    uint32_t                    op_state_counter;          //!< Counter incremented after each execution of the OP state machine
    enum OP_state               op_state;                  //!< OP state enum
    enum OP_state               op_state_prev;             //!< Previous OP state enum
    OpState                     *op_state_func;            //!< Pointer to OP state function
    uint32_t                    op_state_mask;             //!< OP state bit mask (1 bit only will be active)
};


/*!
 * VS properties
 */

struct Equipdev_channel_vs
{
    uint16_t                            state_counter;      //!< VS invalid state test counter
    uint16_t                            sim_intlks;         //!< VS.SIM_INTLKS property
};


/*!
 * ISEG address
 */

struct Equipdev_channel_address
{
    uint8_t                             module;             //!< Channel's module address
    uint8_t                             channel;            //!< Channel's address
};

/*!
 * ISEG property parameters
 */

struct Equipdev_channel_params
{
    float                               voltage_bounds;              //!< Voltage bounds
    float                               voltage_bounds_prev;        //!< Voltage bounds prev
    float                               current_trip_set;           //!< Current trip set
    float                               current_trip_set_prev;      //!< Current trip set prev
    float                               delayed_trip_time;          //!< Delayed trip action time
    float                               delayed_trip_time_prev;     //!< Delayed trip action time prev
    uint16_t                            delayed_trip_action;        //!< Delayed trip action
    uint16_t                            delayed_trip_action_prev;   //!< Delayed trip action prev
};



/*!
 * ISEG properties
 */

struct Equipdev_iseg_channel
{
    uint32_t                            status;                 //!< ISEG Status
    uint32_t                            event;                  //!< ISEG Event Status
    uint32_t                            eventMask;              //!< ISEG Event Mask
    uint16_t                            control;                //!< ISEG Control
    uint16_t                            external_inhibit;       //!< ISEG External Inhibit Action (status)
    uint16_t                            delayed_trip_action;    //!< Delayed trip action

    float                               voltage_meas;           //!< ISEG Voltage Measurement
    float                               voltage_nominal;        //!< ISEG Voltage Nominal
    float                               current_meas;           //!< ISEG Voltage Measurement
    float                               voltage_set;            //!< ISEG Voltage Reference
    float                               voltage_bounds;         //!< Voltage bounds
    float                               current_trip_set;       //!< Current trip set
    float                               delayed_trip_time;      //!< Delayed trip action time

    struct Equipdev_channel_address     address;                //!< ISEG Address
    struct Equipdev_channel_params      params;                 //!< ISEG Parameters
};

struct Equipdev_iseg
{
    struct Equipdev_iseg_channel        channel;           //!< ISEG Channel properties
};





/*!
 * Data for an equipment device channel
 */

struct Equipdev_channel
{
    struct FGCD_device                  *fgcd_device;                //!< Pointer to FGCD device structure

    uint16_t                             state_flags;                //!< Flags to indicate that the device is not in a normal operating state and is awaiting some action to be performed

    // Device interlocks

    uint16_t                             sector_access;              //!< Sector access flag copied from the GW

    // CCLIBS data structures`

    struct EVTLOG_data                  evtlog_data;        //!< Allocation of memory for device data, prop_data, last_evt and records
    pthread_mutex_t                     evtlog_mutex;       //!< Mutex to protect event log callbacks and to lock resources

    // Parser data structures

    uintptr_t    prop_num_els    [EQUIPDEV_MAX_PROPS];               //!< Numbers of elements for device properties

    // Publication

    struct fgc93_stat                    status;                     //!< Published status. Used by STATUS, STATE and PC properties.
    struct fgc93_stat                    status_prev;                //!< Previous status. Used to notify CMW when published values have changed.

    // MEAS properties

    struct Equipdev_channel_meas         meas;                       //!< MEAS properties for this device

    // MODE properties

    uint16_t                             mode_op;                    //!< MODE.OP. Operational mode requested by client
    uint16_t                             mode_pc;                    //!< MODE.PC. Power Converter mode requested by client
    uint16_t                             mode_pc_prev;               //!< Previous MODE.PC. Used to notify CMW when MODE.PC changes.
    uint64_t                             mode_time_ms;               //!< Time since the last state change request (ms). Used to simulate VS behaviour.
    uint64_t                             mode_pc_simplified;         //!< MODE.PC_SIMPLIFIED. Power Converter mode requested by client
    uint64_t                             mode_pc_on;                 //!< MODE.PC_ON. Power Converter simplified state on

    // STATE PC management

    struct Equipdev_channel_PC_state_mgr pc_state_mgr;               //!< State PC management

    // STATE simplified

    uint64_t                             state_pc_simplified;        //!< STATE.PC_SIMPLIFIED. Power Converter simplified state
    uint16_t                             state_pc_simplified_prev;   //!< Previous STATE.PC_SIMPLIFIED. Used to notify CMW when STATE.PC_SIMPLIFIED changes.


    // STATE OP management

    struct Equipdev_channel_OP_state_mgr op_state_mgr;               //!< State OP management

    // REF properties

    struct Equipdev_channel_ref          ref;                        //!< REF properties for this device

    // ISEG properties
    struct Equipdev_iseg                 iseg;                       //!< ISEG properties for this device

    // LOAD properties

    uint32_t                             load_select;                //!< Index of selected load

    struct Equipdev_channel_load         load [FGC_N_LOADS];         //!< Load properties

    // TEST properties

    struct Equipdev_channel_test test;                               //!< Standard TEST property values

    // VS properties

    struct Equipdev_channel_vs           vs;                         //!< VS properties for this device
    float                                start_timeout;              //!< VS.VSRUN_TIMEOUT property
    float                                remaining_cycles_timeout;   //!< Remaining cycles for the VS RUN TIMEOUT

    // DEVICE properties

    struct timeval                       time_start;                 //!< Time at which process was started

    uint32_t                             ppm_enabled;                //!< DEVICE.PPM property.

    // LOG properties

    struct Equipdev_channel_log          log;                        //!< LOG properties for this device

    // PPM variables must be within the following structure

    struct Equipdev_channel_ppm          ppm[NUM_CYC_SELECTORS];

    // Economy state

    uint8_t                              economy_mode;              //!< ECONOMY mode: no pulsing if no beam
};


/*!
 * Struct containing global variables
 */

struct Equipdev
{
    pthread_t                thread;                        //!< Thread ID for Equipdev thread

    uint8_t                  fgc_platform;                  //!< Platform number
    const char              *fgc_platform_name;             //!< Platform name
    const char              *fgc_class_name;                //!< Class name
    struct hash_table       *prop_hash;                     //!< Hash of properties
    struct hash_table       *const_hash;                    //!< Hash of constants

    struct Equipdev_channel  device[FGCD_MAX_DEVS];         //!< Data for equipment devices

    struct Equipdev_ppm                                         //!< PPM variables
    {
        struct Equipdev_ppm_log
        {
            uint32_t        index;                              //!< Indices within log (element 0 is the index of the latest sample)
            uintptr_t       num_elements;                       //!< Numbers of elements in the log
            struct timeval  timestamp;                          //!< Timestamp of cycle within log
        } log;

    } ppm[NUM_CYC_SELECTORS];
};

extern struct Equipdev equipdev;


// Static functions

//! Called from nonvol.cpp if a config property is changed.

static inline void equipdevConfigUnsync(uint32_t channel)
{
}


// External functions

/*!
 * Initialise equipment devices and start the "non-real time" equipment device thread
 */

int32_t equipdevStart(void);

/*!
 * Initialise data for devices.
 *
 * This function is called each time the device names are read.
 */

void equipdevInitDevices(void);

/*!
 * Publish data for equipment devices
 */

void equipdevPublish(void);

/*!
 * Look up a property and set the number of elements.
 *
 * This function allows us to set the number of elements for the FGC.ID.* properties.
 * This cannot be done in the normal way as these properties have no SET function, so
 * this function is a bit of a hack to get around this limitation of the property model.
 *
 * It is also used to initialise Calibration properties on start-up, if they have no
 * value set in non-volatile storage.
 *
 * @param[in]        channel         Address of the DIGMUGEF device
 * @param[in]        bus_id          If the property name contains a '?', it will be replaced
 *                                   with this parameter. (Usually set to 'A' or 'B').
 * @param[in]        prop_label      Name of the property to update
 * @param[in]        num_els         Number of elements to set
 */

void equipdevPropSetNumEls(uint32_t channel, char bus_id, const char *prop_label, uint32_t num_els);

/*!
 * Callback from parser to manually configure properties.
 *
 * @param[in] parser Parser data structure used to process a given command
 *
 * @return    true if the property is ppm, false otherwise
 *
 */


void equipdevNonvolWarn(uint32_t channel, int32_t set);

#endif

// EOF
