/*!
 * @file   state_pc.h
 * @brief  Manage Power Converter state for ISEG devices
 * @author Jose M. de Paco
 */

#ifndef STATE_PC_H
#define STATE_PC_H

#include <classes/93/defconst.h>
#include "timing_handlers.h"

// Constants

#define PC_INVALID_STATE   (0xFF)


/*!
 * Power Converter states
 */

enum PC_state
{
    PC_FO = FGC_PC_FLT_OFF,
    PC_OF = FGC_PC_OFF,
    PC_FS = FGC_PC_FLT_STOPPING,
    PC_SP = FGC_PC_STOPPING,
    PC_ST = FGC_PC_STARTING,
    PC_SA = FGC_PC_SLOW_ABORT,
    PC_TS = FGC_PC_TO_STANDBY,
    PC_SB = FGC_PC_ON_STANDBY,
    PC_IL = FGC_PC_IDLE,
    PC_TC = FGC_PC_TO_CYCLING,
    PC_AR = FGC_PC_ARMED,
    PC_RN = FGC_PC_RUNNING,
    PC_AB = FGC_PC_ABORTING,
    PC_CY = FGC_PC_CYCLING,
    PC_PL = FGC_PC_POL_SWITCHING,
    PC_BK = FGC_PC_BLOCKING,
    PC_EC = FGC_PC_ECONOMY,
    PC_DT = FGC_PC_DIRECT
};


/*!
 * State and transition function typedefs
 */

typedef enum PC_state  PcState     (uint8_t channel, bool first_call); //!< Type for state function
typedef PcState *      PcTransition(uint8_t channel);                  //!< Type for transition condition function



// External functions

/*!
 * PC state machine
 *
 * This function will drive the PC state machine
 */

void statePcUpdate(uint8_t channel);

/*!
 * Converts a simplified state into a normal state.
 *
 * @param state Simplified state
 *
 * @retval Normal state
 */

uint8_t statePcSimplifiedToNormal(uint8_t state, uint8_t channel);

/*!
 * Converts a normal state into a simplified state.
 *
 * @param state Normal state
 *
 * @retval Simplified state
 */

uint8_t statePcNormalToSimplified(uint8_t state, uint8_t channel);

//! Initialize STATE.PC for a channel to be FLT_OFF
//!
//! @param[in]    channel    Address of the ISEG device

void statePcInit(uint32_t channel);

#endif

// EOF
