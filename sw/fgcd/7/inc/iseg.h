/*!
 * @file iseg.h
 * @brief  Functions to Get and Set items in the ISEG Hardware using the ISEGHAL API
 * @author Jose M. de Paco
 */

#ifndef FGC_iseg_functions_H
#define FGC_iseg_functions_H

#include <stdint.h>

extern "C" {
    #include "isegapi.h"
}

// Constants

#define FGCD_NUMBER_ISEG_MODULES 10

// External structures, unions and enumerations



/*
 * Structures for data driven get of iseg items
 */

enum Iseg_channel_item_type
{
    ISEG_ITEM_FLOAT,
    ISEG_ITEM_INT16,
    ISEG_ITEM_INT32
};

enum Iseg_channel_item_level
{
    ISEG_CHANNEL,
    ISEG_MODULE,
    ISEG_CRATE,
    ISEG_LINE,
    ISEG_SYSTEM
};


struct Iseg_items
{
    const char * const                  name;
    const enum Iseg_channel_item_type   type;
    const enum Iseg_channel_item_level  level;
    void * const                        destination;
    const uint32_t                      element_step_size;
};

// External function declarations


/*!
 * Disconnect with ISEG crate using the API ISEGHAL
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegDisconnectCrate(void);



/*!
 * Function to get iseg items defined in the struct type Iseg_items
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegGetIsegItems(const uint8_t index, struct Iseg_items *iseg_items, const uint8_t num_items);



/*!
 *  Function to reset (clear events) the iseg power converter's module
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegResetModuleOfChannel(const uint8_t channel);



/*!
 *  Function to reset (clear events) the iseg power converter's channel
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegResetChannel(const uint8_t channel);



/*!
 *  Function to reset (clear events) the iseg crate and power modules
 */

void isegResetAll(void);



/*!
 *  Function to switch ON an iseg power converter
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegSwitchOnPowerConverter(const uint8_t channel);



/*!
 *  Function to switch OFF an iseg power converter
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegSwitchOffPowerConverter(const uint8_t channel);



/*!
 *  Function to do an emergency stop of the iseg power converter
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegEmergencyStopPowerConverter(const uint8_t channel);



/*!
 * Function to turn ON the crate HV Power
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegHVPowerOn(void);



/*!
 * Function to turn OFF the crate HV Power
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegHVPowerOff(void);



/*!
 *  Function to set the voltage reference to the iseg power converter
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegSetVoltageReference(const uint8_t channel, const float voltage_ref);



/*!
 *  Function to prepare a string with the iseg channel address: crate.module.channel
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegGetChannelAddress(const uint8_t channel, char *iseg_address, size_t size);



/*!
 *  Function to set the module's config parameters
 * @retval  0    Success
 * @retval  1    Failed
 */

int32_t isegSetModuleParams(const uint8_t module, struct Iseg_items *iseg_items, const uint32_t num_items);



/*
 * Function to set all the params to the hardware
 */

void isegSetAllParameters(void);


/*
 * Function to set the params to the hardware only if modified
 */

void isegSetModifiedParameters(void);



/*
 * Function to check the connection to the crate controller
 * @retval  false    Not connected
 * @retval  true     Connected
 */

bool isegIsConnected(void);



/*!
 * Function to connect (TCP/IP socket) with the crate controller
 */
void isegConnectCrate(void);



/*
 * Function to mark the not configured/used modules, i.e. those modules
 * with no device in the configuration.
 */

void isegMarkUsedModules(void);



#endif  // FGC_iseg_functions_H end of header encapsulation

// EOF
