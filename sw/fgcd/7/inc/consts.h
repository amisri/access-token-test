/*!
 * @file   consts.h
 * @brief  Global constants
 * @author Stephen Page
 */

#ifndef CONSTS_H
#define CONSTS_H

#include <consts_base.h>
//#include <defconst.h>
#include <classes/7/defconst.h>
#include <fgc_consts_gen.h>

#define FGCD_CYCLE_PERIOD_MS            200                                               //!< FGCD cycle period in milliseconds
#define FGCD_CYCLES_PER_SEC             (1000 / FGCD_CYCLE_PERIOD_MS)                     //!< FGCD cycles per second
#define FGCD_MAX_EQP_DEVS               FGC_MAX_EQP_DEVS_PER_GW                           //!< Maximum number of equipment devices (not including fgcddev)
#define FGCD_MAX_DEVS                   FGC_MAX_DEVS_PER_GW                               //!< Total maximum number of devices
#define FGCD_MAX_SUB_DEVS_PER_DEV       0                                                 //!< Maximum number of sub-devices per device
#define FGCD_MAX_SUB_DEVS             ((1 + FGCD_MAX_SUB_DEVS_PER_DEV) * FGCD_MAX_DEVS)   //!< Maximum number of sub-devices
#define CMD_MAX_VAL_LENGTH              5230000                                           //!< https://wikis.cern.ch/display/TEEPCCCS/MAX_VAL_LEN

#endif

// EOF
