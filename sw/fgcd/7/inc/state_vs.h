/*!
 * @file state_vs.h
 * @brief  Manage voltage source state
 * @author Jose M. de Paco
 */

#ifndef FGC_prueba2_H
#define FGC_prueba2_H

// Includes

#include <stdint.h>
#include <stdbool.h>
#include <classes/93/defconst.h>

// External structures, unions and enumerations

/*!
 * Voltage source states
 */

enum State_VS
{
    VS_INVALID    = FGC_VS_INVALID,
    VS_FLT_OFF    = FGC_VS_FLT_OFF,
    VS_FASTPA_OFF = FGC_VS_FASTPA_OFF,
    VS_OFF        = FGC_VS_OFF,
    VS_FAST_STOP  = FGC_VS_FAST_STOP,
    VS_STOPPING   = FGC_VS_STOPPING,
    VS_STARTING   = FGC_VS_STARTING,
    VS_READY      = FGC_VS_READY,
    VS_BLOCKED    = FGC_VS_BLOCKED
};

// External function declarations

/*!
 * Get the Voltage Source state.
 *
 * @param[in] channel    Number of channel
 *
 */

void GetVsState(uint8_t channel);

#endif  // FGC_state_vs_H end of header encapsulation

// EOF
