/*!
 * @file   prop_class.h
 * @brief  Declarations for class-specific properties for ISEG_"HV
 * @author Marc Magrans de Abril
 */

#ifndef PROP_CLASS_H
#define PROP_CLASS_H

// Forward declarations

struct prop;
struct Cmd;

// External functions

/*!
 * Check whether a reset is currently permitted.
 *
 * @retval 0    Reset is not permitted (A power converter is not off)
 * @retval 1    Reset permitted (All power converters are off)
*/

int32_t SetifResetOk(struct Cmd *command);

/*!
 * Callback for the DEVICe.RESET property for the ISEG FGCD and FGC
 */

int32_t SetTerminate(struct Cmd *command, struct prop *property);

#endif

// EOF
