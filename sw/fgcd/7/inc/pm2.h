//! @file   pm.h
//! @brief  Declarations for submitting post-mortem data

#pragma once

#include <semaphore.h>
#include <stdint.h>

#include <fgc_stat.h>

// Constants

#define PM_SERVICE_NAME                 "pm-dc"     //!< Name of the PM service to which post-mortem dumps are to be sent
#define PM_SYSTEM_NAME                  "FGC"       //!< System name to be reported with post-mortem dumps

// There are separate buffers for external (1 minute) and self-triggered (7 minutes) Post Mortem
// events. Simultaneous external and self-triggered events are possible.
//
// Unlike FGC classes which work only with fast converters, the class 93 self-triggered buffer holds
// 21,000 samples (7 minutes). Like class 51, FGClite has a longer status buffer to allow recording
// of the decay of the superconducting circuits.


const uint32_t PM_EXT_BUF_LEN      =   3000;         //!< Number of PM samples to be stored after an external trigger
const uint32_t PM_SELF_BUF_LEN_93  =  21000;         //!< Number of PM samples to be stores following a self-trigger
const uint32_t PM_BUF_MAX_SIZE     = 200000;         //!< Max size of the Class 93 FGC pm_buf data
const uint32_t PM_RETRY_PERIOD     = 20;             //!< Time in seconds between retries of post-mortem submission


//! Structure for post-mortem status buffer sample

struct PM_data
{
    uint32_t        time_sec;   //!< Unix time in seconds
    uint32_t        time_usec;  //!< Unix time microseconds
    float           real_time;  //!< Real-time value
    struct fgc_stat status;     //!< Device status
};

//! Structure for post-mortem acquisition information

struct PM_acq_info
{
    bool     frozen;        //!< Flag to indicate whether buffer is frozen
    uint32_t status_index;  //!< Index within status buffer for acquisition
    uint32_t trigger_index; //!< Index of the status at the time of the PM event
    uint32_t time_sec;      //!< Timestamp seconds
    uint32_t time_nsec;     //!< Timestamp nanoseconds
    uint32_t wait_cycles;   //!< Number of cycles to wait before freezing buffer
};

//! Structure for externally triggered post-mortem buffer

struct PM_ext_buffer
{
    struct fgc_stat trigger;                //!< FGC status at time of trigger
    struct PM_data  status[PM_EXT_BUF_LEN]; //!< FGC status buffer
};

//! Structure for externally-triggered post-mortem acquisitions

struct PM_ext_acq
{
    struct PM_acq_info      acq_info;   //!< Acquisition information
    struct PM_ext_buffer    buffer;     //!< Post-mortem buffer
};



// Structures for SELF_PM (Self-triggered post mortem)

//! Structure for SELF_PM buffer for class 93 devices

struct PM_self_buffer_std
{
    struct fgc_stat trigger;                              //!< FGC status at time of trigger
    struct PM_data  status     [PM_SELF_BUF_LEN_93];      //!< FGC status buffer
    uint32_t        fgc_buffers[PM_BUF_MAX_SIZE/4];       //!< Buffers from FGC
};

//! Union for SELF_PM buffer from all devices

union PM_self_buffer
{
    struct fgc_stat              trigger;
    struct PM_self_buffer_std    std;
};

//! Structure for SELF_PM acquisitions

struct PM_self_acq
{
    struct PM_acq_info           acq_info;                             //!< Acquisition information
    union PM_self_buffer         buffer;                               //!< Post-mortem buffer
};

//! Struct containing global variables

struct Pm
{
    sem_t           sem;    //!< Semaphore to trigger the sending of post-mortem data
    pthread_t       thread; //!< Thread handle
    union
    {
        struct PM_ext_buffer    ext;
        union  PM_self_buffer   self;
    } buffer;                                  //!< Buffer for sending Post Mortem
};

extern struct Pm pm_globals;

// External functions

//! Start the post-mortem thread

int32_t pmStart(void);


//! Trigger a self PM

void pmTriggerSelf(uint32_t channel, uint32_t tv_sec, uint32_t tv_nsec);


//! Trigger an external PM

void pmTriggerExternal(uint32_t channel, uint32_t tv_sec, uint32_t tv_nsec);


//! Freeze buffers

void pmBufferFreeze(uint32_t channel);


//! Update Post Mortem status data for one FGC device
//!
//! This function is called from the publication thread (fgcd/src/pub.cpp) once per 20ms (fieldbus) cycle for devices
//! which implement Post Mortem.
//!
//! @param[in]    channel      Address of the FGC device
//! @param[in]    status       Pointer to the published status for the device
//! @param[in]    time_sec     Timestamp for the status (seconds)
//! @param[in]    time_usec    Timestamp for the status (microseconds)

void pmPubUpdate(uint32_t channel, struct fgc_stat *status, uint32_t time_sec, uint32_t time_usec);


//! Returns if a post mortem event is active
//!
//! @param[in]    channel      Address of the FGC device
//! retval        True         A post mortem event is active
//! retval        False        A post mortem event is not active

bool pmIsActive(uint32_t channel);


// EOF
