//! @file   log_class.h
//! @brief  Includes log structure header files for the class 93


#ifndef LOG_CLASS_H
#define LOG_CLASS_H


// Include class specific generated header files for liblog

#include <classes/93/logStructs.h>
#include <classes/93/logMenus.h>

#endif

// EOF
