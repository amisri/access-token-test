//! @file   evtlogInit_class.h
//! @brief  Includes evtlog structure init header files for the class 93


#ifndef EVTLOGINIT_CLASS_H
#define EVTLOGINIT_CLASS_H

#include "fgc_fieldbus.h"
#include <classes/93/defprops.h>
#include <classes/93/evtlogStructsInit.h>

#endif

// EOF
