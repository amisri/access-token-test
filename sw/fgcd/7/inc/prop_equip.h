/*!
 * @file   prop_equip.h
 * @brief  Declarations for equipment device-specific properties
 * @author Marc Magrans de Abril, Jose M. de Paco
 */

#ifndef PROP_EQUIP_H
#define PROP_EQUIP_H

// Forward declarations

struct prop;
struct Cmd;

// Variables

extern bool vs_reset_flag [FGCD_MAX_EQP_DEVS + 1];      //  true if VS reset requested


/*!
 * Reset a property
 *
 */

int32_t SetReset(struct Cmd *command, struct prop *property);

/*!
 * Reset the Voltage Source.
 *
 * This is identical to doing S VS RESET.
 */

int32_t SetResetFaults(struct Cmd *command, struct prop *property);

/*!
 * Set PPM user.
 *
 */

int32_t SetDevicePPM(struct Cmd *command, struct prop *property);

/*!
 * Set the reference control value.
 *
 */

int32_t SetRefControlValue(struct Cmd * command, struct prop * property);


/*!
 * Set the reference pulse duration.
 *
 */

int32_t SetRefPulseDuration(struct Cmd * command, struct prop * property);

/*!
 * Set the pulse reference value.
 *
 */

int32_t SetRefPulse(struct Cmd * command, struct prop * property);



// SETIF functions

/*!
 * Check whether the operational mode can be set
 */

int32_t SetifModeOpOk(struct Cmd *command);

/*!
 * Check whether power converter is off
 */

int32_t SetifRegStateNone(struct Cmd *command);

/*!
 * Check whether power converter is off
 */

int32_t SetifPcOff(struct Cmd *command);

/*!
 * Check whether operational state is normal or simulation
 */

int32_t SetifOpNormalSim(struct Cmd *command);

/*!
 * Check whether a new reference can be accepted
 */

int32_t SetifRefOk(struct Cmd *command);

/*!
 * This function is required only for cycling operation.
 * Set If Condition:     Is REF function lock DISABLED?
 */

int32_t SetifRefUnlock(struct Cmd *command);

/*!
 * Set If Condition:     State PC is not Cycling
 */

int32_t SetifPcNotCycling(struct Cmd *command);



// SET functions

/*!
 * Set a power converter state
 */

int32_t SetPC(struct Cmd *command, struct prop *property);

/*!
 * Set a power converter state (simplified)
 */

int32_t SetPCSimplified(struct Cmd *command, struct prop *property);

/*!
 * Set a reference
 */

int32_t SetRef(struct Cmd *command, struct prop *property);



/*!
 * Set load select parameters
 */

void ParsLoadSelect(uint32_t channel);

/*!
 * Set limit parameters
 */

void ParsLimits(uint32_t channel);

/*!
 * Set parameter for simulated measurements
 */

void ParsMeasSim(uint32_t channel);

// GET function declarations

//! Retrieve LOG.PM.BUF buffer

int32_t GetLogPmBuf(struct Cmd *command, struct prop *property);

#endif

// EOF
