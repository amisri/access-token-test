/*!
 * @file   mode_pc.h
 * @brief  Manage Power Converter mode changes for ISEG devices
 * @author Michael Davis, Jose M. de Paco
 */

#ifndef MODE_PC_H
#define MODE_PC_H

#include <stdint.h>
#include <fgc_errs.h>


// Variables


// External functions

/*!
 * Set the Power Converter State Machine requested state.
 *
 * Sets MODE.PC and sends the appropriate command (START/STOP/RESET) to the Power Converter.
 */

enum fgc_errno modePcSet(uint8_t channel, uint8_t mode);

#endif

// EOF
