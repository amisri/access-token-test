/*!
 * @file   fgcddev.h
 * @brief  Declarations for the ISEG FGCD device
 * @author Marc Magrans de Abril
 */

#ifndef FGCDDEV_H
#define FGCDDEV_H

#include <stdint.h>

#include <fgc_stat.h>
#include <parser.h>

// Forward declarations

struct FGCD_device;
struct hash_table;


/*!
 * ISEG properties
 */

struct FGCDdev_iseg_system
{
    uint32_t                            status;             //!< ISEG System Status
    uint16_t                            cycle_counter;      //!< ISEG System Cycle Counter
};

struct FGCDdev_iseg_line
{
    uint32_t                            status;             //!< ISEG Line Status
};

struct FGCDdev_iseg_crate
{
    uint32_t                            status;             //!< ISEG Status
    uint32_t                            event;              //!< ISEG Event Status
    uint32_t                            event_mask;         //!< ISEG Event Mask
    uint16_t                            connected;          //!< ISEG crate connected
    uint16_t                            communication;      //!< ISEG crate communication is enabled
    uint16_t                            auto_power_on;      //!< ISEG crate auto HV power on is enabled
    uint16_t                            power_on_command;   //!< ISEG crate power_on command
    uint16_t                            power_off_command;  //!< ISEG crate power_off command
    uint16_t                            power_on_status;    //!< ISEG crate power_on status
    float                               fan_speed;          //!< ISEG crate fan speed
};

/*!
 * ISEG property parameters
 */

struct FGCDdev_iseg_module_params
{
    float                               voltage_ramp_speed[10];      //!< ISEG module voltage ramp speed
    float                               voltage_ramp_speed_prev[10]; //!< ISEG module voltage ramp speed previous
    uint16_t                            kill_enable[10];             //!< ISEG module kill enable
    uint16_t                            kill_enable_prev[10];        //!< ISEG module kill enable previous
    uint16_t                            fine_adjustment[10];         //!< ISEG module kill enabled
    uint16_t                            fine_adjustment_prev[10];    //!< ISEG module kill enabled previous
};

struct FGCDdev_iseg_module
{
    uint32_t                            status[10];                  //!< ISEG Status
    uint32_t                            event[10];                   //!< ISEG Event Status
    uint32_t                            event_mask[10];              //!< ISEG Event Mask
    uint16_t                            control[10];                 //!< ISEG Control
    uint16_t                            connected[10];               //!< ISEG module connected
    uint16_t                            sample_rate[10];             //!< ISEG module sample rate
    uint16_t                            digital_filter[10];          //!< ISEG module digital filter
    uint16_t                            high_voltage_ok[10];         //!< ISEG module high voltage OK
    float                               voltage_limit[10];           //!< ISEG module voltage limit
    float                               current_limit[10];           //!< ISEG module voltage limit
    float                               temperature[10];             //!< ISEG module temperature
    float                               voltage_ramp_speed[10];      //!< ISEG module voltage ramp speed

    struct FGCDdev_iseg_module_params   params;                      //!< ISEG module parameters
};

struct FGCDdev_iseg
{
    uint16_t                            reset;              //!< ISEG Reset of HW faults

    struct FGCDdev_iseg_module          module;             //!< ISEG Module properties
    struct FGCDdev_iseg_crate           crate;              //!< ISEG Crate properties
    struct FGCDdev_iseg_line            line;               //!< ISEG Line properties
    struct FGCDdev_iseg_system          system;             //!< ISEG System properties
};


/*!
 * Struct containing global variables
 */

struct FGCDdev
{
    struct FGCD_device *device;                             //!< Pointer to device structure
    struct fgc7_stat    status;                             //!< Published data
    struct Parser       parser;                             //!< Parser
    struct hash_table  *const_hash;                         //!< Hash of constants
    struct hash_table  *prop_hash;                          //!< Hash of properties
    uint8_t             test_bin[64];                       //!< Test binary property
    char                test_char[64];                      //!< Test character property
    float               test_float[16];                     //!< Test float property
    int8_t              test_int8s[64];                     //!< Test integer property
    uint8_t             test_int8u[64];                     //!< Test integer property
    int16_t             test_int16s[32];                    //!< Test integer property
    uint16_t            test_int16u[32];                    //!< Test integer property
    int32_t             test_int32s[16];                    //!< Test integer property
    uint32_t            test_int32u[16];                    //!< Test integer property
    const char         *test_string[2];                     //!< Test string property
    const char         *fgc_class_name;                     //!< Class name
    const char         *fgc_platform_name;                  //!< Platform name
    uint8_t             fgc_platform;                       //!< Platform

    // iseg property
    struct FGCDdev_iseg iseg;                               //!< ISEG properties for FGCD device
};

extern struct FGCDdev fgcddev;

// External functions

/*!
 * Start the device
 */

int32_t fgcddevStart(void);

/*!
 * Initialise data for device
 *
 * This function does not do anything
 */

void fgcddevInitDevice(void);

/*!
 * Publish data for FGCD device
 */

void fgcddevPublish(void);

#endif

// EOF
