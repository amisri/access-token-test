//! @file   logInit_class.h
//! @brief  Includes log structure initialisation header file for the class 93


#ifndef LOGINIT_CLASS_H
#define LOGINIT_CLASS_H

#include <classes/93/logStructsInit.h>
#include <classes/93/logMenusInit.h>

#endif

// EOF
