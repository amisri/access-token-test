/*!
 * @file iseg_cycle.h
 * @brief Implementation of the cycle for the ISEG equipment
 * @author Jose M. de Paco
 */

#ifndef FGC_iseg_cycle_H
#define FGC_iseg_cycle_H


// Includes

#include <stdint.h>

// Constants


// External structures, unions and enumerations


// External function declarations

/*!
 * Initialise and start the ISEG Cycle thread.
 *
 * The ISEG Cycle thread handles all the commands and statuses in each cycle.
 *
 * @retval  0    Initialisation and startup was successful
 * @retval  1    Initialisation and startup failed
 */

int32_t isegCycleStart(void);

// External variable definitions


#endif  // FGC_iseg_cycle_H end of header encapsulation

// EOF
