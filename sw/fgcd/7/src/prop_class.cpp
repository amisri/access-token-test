/*!
 * @file   prop_class.cpp
 * @author Stephen Page
 * @brief  ISEG HV FGCD specific properties
 *
 * Class-specific functions for getting and setting properties
 */

#include <cmd.h>
#include <defconst.h>
#include <defprops.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <prop.h>
#include <prop_class.h>
#include <logging.h>
#include <classes/93/defconst.h>
#include <classes/93/defconst_assert.h>



int32_t SetifResetOk(struct Cmd *command)
{
    struct Equipdev_channel *device;
    uint32_t                i;

    // Check whether any power converters are ON

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        device = &equipdev.device[i];

        if(device->fgcd_device->name != NULL)
        {
            // The device is configured

            switch(device->status.state_pc)
            {
                // States in which power converter is considered to be off

                case FGC_PC_FLT_OFF:
                case FGC_PC_FLT_STOPPING:
                case FGC_PC_OFF:
                case FGC_PC_STOPPING:
                    break;

                default: // Power converter is not off
                    return 0;
            }
        }
    }

    // All power converters are OFF

    return 1;
}

int32_t SetTerminate(struct Cmd *command, struct prop *property)
{
    uint32_t channel = command->device->id;

    // Terminate the FGCD?

    if(channel == 0)
    {
        return Terminate(command, property);
    }

    // Otherwise, send a reset to the ISEG converter (not implemented)

    logPrintf(channel, "PROP_CLASS Warning: DEVICE.RESET not implemented for ISEG HV devices \n");

    return command->error;
}

// EOF
