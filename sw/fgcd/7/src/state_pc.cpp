/*!
 * @file   state_pc.cpp
 * @brief  Manage Power Converter state for ISEG devices
 * @author Jose M. de Paco
 */

#include <stdint.h>
#include <unistd.h>
#include <math.h>

#include <classes/93/defconst.h>
#include <logging.h>
#include <pm.h>
#include <timing.h>
#include <equipdev.h>
#include <state_pc.h>
#include <iseg.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <equip_common_93.h>

/*!
 * State functions declarations
 */

static PcState statePcFO;
static PcState statePcOF;
static PcState statePcFS;
static PcState statePcSP;
static PcState statePcST;
static PcState statePcSA;
static PcState statePcTS;
static PcState statePcSB;
static PcState statePcTC;
static PcState statePcCY;
static PcState statePcBK;
static PcState statePcDT;


// State machine initialization

void statePcInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize channel state to be OFF

    device.status.state_pc   = FGC_PC_FLT_OFF;

}



// Transition functions

static PcState* pcTransXXtoFS(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Detect faults that bring the converter to FAULT STOPPING state

    if (device.status.st_faults != 0)
    {
        // At least one fault has appeared

        return statePcFS;
    }

    // Detect unexpected switch off of the power converter

    if (device.mode_pc != FGC_PC_OFF    && !getStatusBit(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON) &&
        device.status.state_pc != PC_ST && device.status.state_pc != PC_SP)
    {
        logPrintf(channel, "STATE_PC Power converter switched off unexpectedly \n");
        latchStatusBit(&device.status.st_faults, FGC_FLT_VS_STATE, true);

        return statePcFS;
    }

    return NULL;
}

static PcState* pcTransXXtoSA(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if( device.mode_pc == FGC_PC_SLOW_ABORT  ||  device.mode_pc == FGC_PC_OFF)
    {
        // Explicit request to switch the PC off

        setStatusBit(&device.state_flags, DEVICE_STATE_SLOW_ABORT, true);

        return statePcSA;
    }

    return NULL;
}

static PcState* pcTransBKtoSP(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.mode_pc == FGC_PC_OFF                              ||
       device.mode_pc == FGC_PC_SLOW_ABORT                       ||
       getStatusBit(device.state_flags, DEVICE_STATE_SLOW_ABORT) )
    {
        return statePcSP;
    }

    return NULL;
}

static PcState* pcTransBKtoTS(uint8_t channel)
{
    // We always transition through BLOCKING immediately. If we didn't transition
    // to STOPPING, we must transition to TO_STANDBY.

    return statePcTS;
}

static PcState* pcTransFOtoOF(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.status.st_faults == 0)
    {
        // No faults asserted

        return statePcOF;
    }

    return NULL;
}

static PcState* pcTransFStoSP(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];


    if(device.status.st_faults == 0)
    {
        // No faults asserted

        return statePcSP;
    }

    return NULL;
}

static PcState* pcTransFStoFO(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.status.state_vs == FGC_VS_FLT_OFF || device.status.state_vs == FGC_VS_OFF)
    {
        // Power converter is OFF

        return statePcFO;
    }

    return NULL;
}

static PcState* pcTransOFtoFO(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check that power converter is OFF

    if(getStatusBit(device.status.st_unlatched, FGC_UNL_HIGH_VOLTAGE_ON))
    {
        logPrintf(channel, "STATE_PC Power converter switched on unexpectedly \n");
        latchStatusBit(&device.status.st_faults, FGC_FLT_VS_STATE, true);

        // Switch off power converter

        isegSwitchOffPowerConverter(channel);

        return statePcFO;
    }

    if(device.status.st_faults != 0)
    {
        // Fault asserted

        return statePcFO;
    }

    return NULL;
}

static PcState* pcTransOFtoST(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // MODE.PC == [BK, SB, CY, DT] and no faults asserted

    if((device.mode_pc == FGC_PC_BLOCKING ||
        device.mode_pc == FGC_PC_ON_STANDBY ||
        device.mode_pc == FGC_PC_CYCLING ||
        device.mode_pc == FGC_PC_DIRECT) &&
        device.status.state_vs != FGC_VS_INVALID &&
        device.status.st_faults == 0)
    {
        return statePcST;
    }

    return NULL;
}

static PcState* pcTransSAtoBK(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.mode_pc == FGC_PC_OFF || fabs(device.meas.v_meas) <= fabs(device.load[device.load_select].limits.v.min + device.iseg.channel.voltage_nominal * 0.0015))
    {
        // MODE.PC OFF or reference ramped down to a minimum

        return statePcBK;
    }

    return NULL;
}

static PcState* pcTransSAtoTS(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Allow transition if permits are asserted (not applicable for iseg) AND the state_flags latch
    // has been cleared by explicitly setting MODE.OP

    if(!getStatusBit(device.state_flags, DEVICE_STATE_SLOW_ABORT) && device.mode_pc == FGC_PC_ON_STANDBY)
    {
        return statePcTS;
    }

    return NULL;
}

static PcState* pcTransSPtoOF(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check if converter stopped


    if(device.status.state_vs == FGC_VS_OFF)
    {
        // Clean the control flag

        isegSwitchOffPowerConverter(channel);

        return statePcOF;
    }


    return NULL;
}

static PcState* pcTransSTtoBK(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.status.state_vs == FGC_VS_READY)
    {
        // Converter started

        return statePcBK;
    }

    return NULL;
}

static PcState* pcTransSTtoSP(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.mode_pc == FGC_PC_OFF)
    {
        // PC OFF requested

        return statePcSP;
    }

    return NULL;
}

static PcState* pcTransTStoSB(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(fabs(device.meas.v_meas * 1.05) >= device.load[device.load_select].limits.v.min)
    {
        // Reference ramped to a minimum value

        return statePcSB;
    }

    if(device.status.state_op == FGC_OP_SIMULATION)
    {
        // Direct transition if simulation

        return statePcSB;
    }

    return NULL;
}

static PcState* pcTransSBtoDT(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_DIRECT)
    {
        // Mode PC direct requested

        return statePcDT;
    }

    return NULL;
}

static PcState* pcTransSBtoTC(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_CYCLING)
    {
        // Mode PC cycling requested

        return statePcTC;
    }

    return NULL;
}

static PcState* pcTransDTtoTS(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_ON_STANDBY)
    {
        // Mode PC stand-by requested

        return statePcTS;
    }

    return NULL;
}

static PcState* pcTransTCtoCY(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_CYCLING)
    {
        // Mode PC cycling requested

        sleep(1);
        return statePcCY;
    }

    return NULL;
}

static PcState* pcTransTCtoTS(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_ON_STANDBY)
    {
        // Mode PC stand-by requested

        return statePcTS;
    }

    return NULL;
}

static PcState* pcTransCYtoTS(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_ON_STANDBY)
    {
        // Mode PC stand-by requested

        return statePcTS;
    }

    return NULL;
}

static PcState* pcTransCYtoDT(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (device.mode_pc == FGC_PC_DIRECT)
    {
        // Mode PC direct requested

        return statePcDT;
    }

    return NULL;
}



/*
 * Transition condition table
 */

#define PC_MAX_TRANSITIONS     4        //!< Maximum transitions from a state

static PcTransition * transitions[][PC_MAX_TRANSITIONS + 1] =
{
    ////////// States where the Power Converter is OFF ////////////////////////////////////////////////////

    /* PC_FO */ {                               pcTransFOtoOF,                NULL },

    /* PC_OF */ {                               pcTransOFtoFO, pcTransOFtoST, NULL },

    ////////// States where the Power Converter is transitioning to ON or OFF /////////////////////////////

    /* PC_FS */ {                               pcTransFStoFO, pcTransFStoSP, NULL },

    /* PC_SP */ { pcTransXXtoFS,                pcTransSPtoOF,                NULL },

    /* PC_ST */ { pcTransXXtoFS,                pcTransSTtoSP, pcTransSTtoBK, NULL },

    /* PC_SA */ { pcTransXXtoFS,                pcTransSAtoTS, pcTransSAtoBK, NULL },

    ////////// States where the Power Converter is ON /////////////////////////////////////////////////////

    /* PC_TS */ { pcTransXXtoFS, pcTransXXtoSA, pcTransTStoSB,                NULL },

    /* PC_SB */ { pcTransXXtoFS, pcTransXXtoSA, pcTransSBtoDT, pcTransSBtoTC, NULL },

    /* PC_IL */ {                                                             NULL },  //not implemented

    /* PC_TC */ { pcTransXXtoFS, pcTransXXtoSA, pcTransTCtoCY, pcTransTCtoTS, NULL },

    /* PC_AR */ {                                                             NULL },  //not implemented

    /* PC_RN */ {                                                             NULL },  //not implemented

    /* PC_AB */ {                                                             NULL },  //not implemented

    /* PC_CY */ { pcTransXXtoFS, pcTransXXtoSA, pcTransCYtoTS, pcTransCYtoDT, NULL },

    /* PC_PL */ {                                                             NULL },  //not implemented

    /* PC_BK */ { pcTransXXtoFS,                pcTransBKtoSP, pcTransBKtoTS, NULL },

    /* PC_EC */ {                                                             NULL },  //not implemented

    /* PC_DT */ { pcTransXXtoFS, pcTransXXtoSA, pcTransDTtoTS,                NULL },

    /* PC_PA */ {                                                             NULL }   //not implemented
};


/*
 * PC State Machine Function
 */

void statePcUpdate(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Transition condition functions for each state are called in priority order, from left to right

    PcState        * next_state = NULL;
    PcTransition   * transition;
    PcTransition  ** state_transitions = &transitions[device.pc_state_mgr.pc_state][0];

    // Scan transitions for the current state, testing each condition in priority order

    while((transition = *(state_transitions++)) != NULL && (next_state = (*transition)(channel)) == NULL);

    // If a condition is true then switch to the new state

    if(next_state != NULL)
    {
        // Run new state function with first_call set to true

        device.pc_state_mgr.pc_state_prev = device.pc_state_mgr.pc_state;
        device.pc_state_mgr.pc_state      = next_state(channel, true);

        device.pc_state_mgr.pc_state_mask = 1 << device.pc_state_mgr.pc_state;
        device.pc_state_mgr.pc_state_func = next_state;

        device.status.state_pc = device.pc_state_mgr.pc_state;

        // Reset the time since last state change

        device.mode_time_ms = 0;
    }
    else
    {
        // Stay in current state and run the state function with the first_call parameter set to false

        device.pc_state_mgr.pc_state_func(channel, false);
    }

    // Increment the state counter - this is a service to the application to allow a background thread to wait for the state machine to run

    device.pc_state_mgr.pc_state_counter++;

    // Update the simplified PC state

    uint8_t simplified_state = statePcNormalToSimplified(device.status.state_pc, channel);

    if (simplified_state != PC_INVALID_STATE)
    {
        device.state_pc_simplified = simplified_state;
    }
}

/*
 * State Functions
 */

static enum PC_state statePcFO(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Change the mode PC to OFF

        device.mode_pc  = FGC_PC_OFF;

        // Switch OFF the converter

        isegSwitchOffPowerConverter(channel);
    }

    else if(getStatusBit(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON))
    {
        // Converter is ON

        struct timeval time;
        timingGetUserTime(0, &time);

        // Send switch off command every 10 seconds and log

        if(time.tv_sec % 10 == 0)
        {
            // Switch OFF the converter

            isegSwitchOffPowerConverter(channel);

            logPrintf(channel, "STATE_PC Power converter switched on unexpectedly \n");
        }
     }

    return PC_FO;
}

static enum PC_state statePcOF(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Initialize iseg Voltage Set to 0V

        isegSetVoltageReference(channel, 0.0);

        device.status.v_ref = 0.0;

        // Reset the power converter

        isegResetChannel(channel);

        // Clear the SLOW_ABORT flag

        setStatusBit(&device.state_flags, DEVICE_STATE_SLOW_ABORT, false);

        // Action if previous state is FLT_OFF

        if (device.pc_state_mgr.pc_state_prev == FGC_PC_FLT_OFF)
        {
            // Switch off the power converter: clean control flag in case of previous emergency stop

            isegSwitchOffPowerConverter(channel);
        }
    }

    return PC_OF;
}

static enum PC_state statePcFS(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    //struct timeval time;

    float remaining_cycles_timeout = 500.0;

    if(first_call)
    {
        // Trigger a Post Mortem (SELF)

        //struct timeval  event_tv;

        //timingGetUserTime(0, &event_tv);

        //pmTriggerSelf(channel, event_tv.tv_sec,event_tv.tv_usec*1000);

        // Set Voltage Reference to 0V

        isegSetVoltageReference(channel, 0.0);

        device.status.v_ref = 0.0;

        // Switch off the power converter

        isegSwitchOffPowerConverter(channel);

        // Initialize timeout

        device.remaining_cycles_timeout = remaining_cycles_timeout;
    }

    else if(getStatusBit(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON))
    {
        // If converter did not turn OFF before the time out

        if(device.remaining_cycles_timeout <= 1.0)
        {
            static bool unblock_control = false;

            if(unblock_control == false)
            {
                isegSwitchOnPowerConverter(channel);  // Converter is already ON, so this command has no effect (only cleaning the register)

                logPrintf(channel, "STATE_PC Power Converter stopping: unblocking Control register. Ignore previous ON command \n");

                unblock_control = true;
            }
            else
            {
                // Switch OFF the converter

                isegEmergencyStopPowerConverter(channel);

                logPrintf(channel, "STATE_PC Power Converter stopping: unblocking Control register n");
                unblock_control = false;
            }

            device.remaining_cycles_timeout = remaining_cycles_timeout;;;;
        }
        else
        {
            device.remaining_cycles_timeout--;
        }
    }

    return PC_FS;
}

static enum PC_state statePcSP(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    float remaining_cycles_timeout = 100.0;

    if(first_call)
    {
        isegSwitchOffPowerConverter(channel);

        // Initialize timeout

        device.remaining_cycles_timeout = remaining_cycles_timeout;
    }

    if(getStatusBit(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON))
    {
        // Converter is still ON

        // Start counting for the time-out

        if(device.remaining_cycles_timeout <= 1.0)
        {
            static bool unblock_control = false;

            if(unblock_control == false)
            {
                isegSwitchOnPowerConverter(channel);  // Converter is already ON, so this command has no effect (only cleaning the register)

                logPrintf(channel,
                        "STATE_PC Power Converter stopping: unblocking Control register. Ignore previous ON command \n");

                unblock_control = true;

            }
            else
            {
                // Switch OFF the converter

                isegEmergencyStopPowerConverter(channel);

                logPrintf(channel, "STATE_PC Power Converter stopping: unblocking Control register \n");

                unblock_control = false;
            }

            device.remaining_cycles_timeout = remaining_cycles_timeout;

        }
        else
        {
            device.remaining_cycles_timeout--;
        }
    }

    return PC_SP;
}

static enum PC_state statePcST(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Start the power converter

        isegSwitchOnPowerConverter(channel);

        // Start counting for the time-out

        device.remaining_cycles_timeout = device.start_timeout / (FGCD_CYCLE_PERIOD_MS / 1000.0);

        if(device.remaining_cycles_timeout < 10)
        {
            // Minimum number of cycles to refresh the state of the PC is 10

            device.remaining_cycles_timeout = 10;
        }
    }
    else
    {
        if(device.status.state_op != FGC_OP_SIMULATION)
        {
            if (device.remaining_cycles_timeout == 5)
            {
                // Retry: Start the power converter

                isegSwitchOnPowerConverter(channel);

                device.remaining_cycles_timeout--;
            }
            else if (device.remaining_cycles_timeout < 1)
            {
                // Time out

                latchStatusBit(&device.status.st_faults, FGC_FLT_VS_RUN_TO, true);
            }
            else
            {
                // Continue counting cycles

                device.remaining_cycles_timeout--;
            }
        }
    }

    return PC_ST;
}

static enum PC_state statePcSA(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    float voltage_ref_slow_abort = 0.0;

    if(first_call)
    {
        // Set the voltage reference to 0V

        isegSetVoltageReference(channel, voltage_ref_slow_abort);
    }

    device.status.v_ref = voltage_ref_slow_abort;

    return PC_SA;
}

static enum PC_state statePcTS(uint8_t channel, bool first_call)
{
    if (first_call)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        float reference;

        // Set the voltage reference to the value configured in LIMITS.V.MIN

        if (device.load[device.load_select].limits.v.pos == 0 && device.load[device.load_select].limits.v.neg < 0) // negative
        {
            reference = - device.load[device.load_select].limits.v.min;

            isegSetVoltageReference(channel, reference);

            device.status.v_ref = reference;
        }
        else
        {
            // Positive or bipolar

            reference = device.load[device.load_select].limits.v.min;

            isegSetVoltageReference(channel, reference);

            device.status.v_ref = reference;
        }
    }

    return PC_TS;
}

static enum PC_state statePcSB(uint8_t channel, bool first_call)
{
    return PC_SB;
}

static enum PC_state statePcTC(uint8_t channel, bool first_call)
{
    return PC_TC;
}

static enum PC_state statePcCY(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if (first_call)
    {
        // Initialize cycling reference to the current voltage

        device.ppm[0].cycling_v_ref    = device.status.v_ref;

        // Initialize previous value

        device.ppm[0].pulse_v_ref_prev = device.status.v_ref;
    }
    else
    {
        float reference = device.ppm[0].cycling_v_ref;

        if (device.ppm[0].pulse_v_ref_prev != reference)
        {
            isegSetVoltageReference(channel, reference);

            device.status.v_ref            = reference;
            device.ppm[0].pulse_v_ref_prev = reference;
        }
    }

    return PC_CY;
}

static enum PC_state statePcBK(uint8_t channel, bool first_call)
{
    // Not implemented in ISEG

    return PC_BK;
}

static enum PC_state statePcDT(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    float reference = 0.0;

    // Set the voltage reference to the power converter on first call or if it has been changed

    if ((first_call || (device.ref.vcv != device.ref.vcv_prev)))
    {
        reference = device.ref.vcv;

        // Clip the voltage reference if out of limits

        if (reference > device.load[device.load_select].limits.v.pos)
        {
            reference = device.load[device.load_select].limits.v.pos;
        }
        else if(reference < device.load[device.load_select].limits.v.neg)
        {
            reference = device.load[device.load_select].limits.v.neg;
        }

        // Set the voltage reference

        isegSetVoltageReference(channel, reference);

        device.ref.vcv_prev = device.ref.vcv;

        device.status.v_ref = device.ref.vcv;
    }

    return PC_DT;
}




// External functions

uint8_t statePcSimplifiedToNormal(uint8_t state, uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    static const uint8_t state_logical_order[] =
    {
        0,   // FGC_PC_FLT_OFF
        1,   // FGC_PC_OFF
        2,   // FGC_PC_FLT_STOPPING
        3,   // FGC_PC_STOPPING
        4,   // FGC_PC_STARTING
        6,   // FGC_PC_SLOW_ABORT
        7,   // FGC_PC_TO_STANDBY
        8,   // FGC_PC_ON_STANDBY
        9,   // FGC_PC_IDLE
        10,  // FGC_PC_TO_CYCLING
        11,  // FGC_PC_ARMED
        12,  // FGC_PC_RUNNING
        13,  // FGC_PC_ABORTING
        14,  // FGC_PC_CYCLING
        15,  // FGC_PC_POL_SWITCHING
        5,   // FGC_PC_BLOCKING
        16,  // FGC_PC_ECONOMY
        17   // FGC_PC_DIRECT
    };

    uint8_t normal_state;

    switch (state)
    {
        case FGC_PC_SIMPLIFIED_OFF:
            if(state_logical_order[device.status.state_pc] >= state_logical_order[FGC_PC_TO_STANDBY])
            {
                normal_state = FGC_PC_SLOW_ABORT;
            }
            else
            {
                normal_state = FGC_PC_OFF;
            }
            break;

        case FGC_PC_SIMPLIFIED_ON:

            // The value of MODE.PC_ON is a symlist (def/symlists/pc_on.xml): IDLE = 0, CYCLING = FGC_PC_CYCLING, DIRECT = FGC_PC_DIRECT. Therefore if
            // S MODE.PC_SIMPLIFIED ON is requested, the target normal state is directly the value of the variable of property MODE.PC_ON (device.mode_pc_on).
            // No conversion is needed

            normal_state = device.mode_pc_on;
            break;

        case FGC_PC_SIMPLIFIED_BLOCKING:
            normal_state = FGC_PC_BLOCKING;
            break;

        default:
            normal_state = PC_INVALID_STATE;
            break;
    }

    return (normal_state);
}



uint8_t statePcNormalToSimplified(uint8_t state, uint8_t channel)
{
    // pc_simplifed maps the current operational state (index of array)
    // to the corresponding simplified state (value in the array).

    static uint8_t const pc_simplified[] =
    {
        FGC_PC_SIMPLIFIED_FAULT      ,     // FO
        FGC_PC_SIMPLIFIED_OFF        ,     // OF
        FGC_PC_SIMPLIFIED_FAULT      ,     // FS
        PC_INVALID_STATE             ,     // SP
        PC_INVALID_STATE             ,     // ST
        PC_INVALID_STATE             ,     // SA
        PC_INVALID_STATE             ,     // TS
        FGC_PC_SIMPLIFIED_ON         ,     // SB
        PC_INVALID_STATE             ,     // IL
        PC_INVALID_STATE             ,     // TC
        PC_INVALID_STATE             ,     // AR
        PC_INVALID_STATE             ,     // RN
        PC_INVALID_STATE             ,     // AB
        FGC_PC_SIMPLIFIED_ON         ,     // CY
        PC_INVALID_STATE             ,     // PL
        FGC_PC_SIMPLIFIED_BLOCKING   ,     // BK
        PC_INVALID_STATE             ,     // EC
        FGC_PC_SIMPLIFIED_ON         ,     // DT
    };

    return (pc_simplified[state]);
}


// EOF
