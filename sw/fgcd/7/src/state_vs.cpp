/*!
 * @file state_vs.cpp
 * @brief  Manage voltage source state
 * @author Jose M. de Paco
 */



// Includes

#include <state_vs.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <equipdev.h>
#include <equip_common_93.h>

// Constants

/*
 * Lookup table for Voltage Source state from XXXX
 */

static const enum State_VS vs_state_lookup[] =
{
                        //     IDX      CRATE_ON        MODULE_ON       VSREADY         VSFAULT
    VS_OFF,             //      0           0               0              0               0
    VS_FLT_OFF,         //      1           0               0              0               1
    VS_INVALID,         //      2           0               0              1               0
    VS_INVALID,         //      3           0               0              1               1
    VS_INVALID,         //      4           0               1              0               0
    VS_INVALID,         //      5           0               1              0               1
    VS_INVALID,         //      6           0               1              1               0
    VS_INVALID,         //      7           0               1              1               1
    VS_OFF,             //      8           1               0              0               0
    VS_FLT_OFF,         //      9           1               0              0               1
    VS_INVALID,         //     10           1               0              1               0
    VS_INVALID,         //     11           1               0              1               1
    VS_OFF,             //     12           1               1              0               0
    VS_FLT_OFF,         //     13           1               1              0               1
    VS_READY,           //     14           1               1              1               0
    VS_FAST_STOP,       //     15           1               1              1               1
};


// Internal structures, unions and enumerations



// Internal function declarations



// External function definitions

void GetVsState(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Create state index from the four entries: CRATE_ON, MODULE_ON, VSREADY, VSFAULT

    uint8_t index = 0;

    uint8_t module_address = device.iseg.channel.address.module;

    if (device.status.state_op != FGC_OP_SIMULATION) // not simulation
    {
        index = (device.status.st_faults != 0);
        index |= getStatusBit(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON) << 1;
        index |= (fgcddev.iseg.module.connected[module_address] == 1)  << 2 ;
        index |= ((fgcddev.iseg.crate.connected == 1) && (fgcddev.iseg.crate.power_on_status))  << 3;
    }
    else  // simulation mode
    {
        index = (device.status.st_faults != 0);
        index |= getStatusBit(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON) << 1;
        index |= 1 << 2;  // module is OK
        index |= 1 << 3;
    }

    device.status.state_vs = vs_state_lookup[index];
}




// Internal function definitions


// EOF
