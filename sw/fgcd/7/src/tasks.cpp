/*!
 * @file   tasks.cpp
 * @author Stephen Page
 *
 * Define tasks to be started and stopped
 */

// FGCD includes

#include <alarms.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <cmw.h>
#include <cmwpub.h>
#include <cmwutil.h>
#include <devname.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <nonvol.h>
#include <parser.h>
#include <pm.h>
#include <pub.h>
#include <rbac.h>
#include <rt.h>
#include <taskmgr.h>
#include <tcp.h>
#include <timing.h>
#include <iseg_cycle.h>


struct Task tasks[]
=
{
    // label                        start_func                   fatal   flag

    { "RBAC",                       &rbacInit,                   1,  &fgcd.enable_rbac  },
    { "parser",                     &parserInit,                 1,  NULL               },
    { "command queue manager",      &cmdqmgrInit,                1,  NULL               },
    { "device naming",              &devnameInit,                1,  NULL               },
    { "non-volatile storage",       &nonvolStart,                1,  NULL               },
    { "FGCD device",                &fgcddevStart,               1,  NULL               },
    { "logging",                    &logStart,                   1,  NULL               },
    { "equipment devices",          &equipdevStart,              1,  NULL               },
    { "CMW utilities",              &cmwutilInit,                1,  NULL               },
    { "CMW publication",            &cmwpubStart,                1,  NULL               },
    { "ISEG Cycle",                 &isegCycleStart,             1,  NULL               },
    { "timing",                     &timingStart,                0,  NULL               },
    { "timing events",              &timingSubscribeEvents,      1,  NULL               },
    { "CMW server",                 &cmwStart,                   1,  NULL               },
    { "alarm surveillance",         &alarmsStart,                1,  NULL               },
    { "publishing",                 &pubStart,                   1,  NULL               },
    { "TCP server",                 &tcpStart,                   1,  NULL               },
//    { "post-mortem",                &pmStart,                    1,  &fgcd.enable_pm    },
    { "",                           NULL,                        0,  NULL               },
};

// EOF
