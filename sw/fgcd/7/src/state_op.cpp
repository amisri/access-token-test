/*!
 * @file   state_op.cpp
 * @brief  Manage Operational state for ISEG devices
 * @author Jose M. de Paco
 */

#include <fgcd.h>
#include <equipdev.h>
#include <state_op.h>
#include <equip_common_93.h>


// Static function declarations

static bool stateOpIsConfigured(uint8_t channel);

// State functions

static OpState stateOpUC;
static OpState stateOpNL;
static OpState stateOpSM;


// State machine initialization

void stateOpInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize channel state to be UNCONFIGURED

    device.status.state_op = FGC_OP_UNCONFIGURED;
}



// Transition functions

static OpState* UCtoNL(uint8_t channel)
{
    if(equipdev.device[channel].mode_op == OP_NL && stateOpIsConfigured(channel))
    {
        return stateOpNL;
    }

    return NULL;
}

static OpState* UCtoSM(uint8_t channel)
{
    if(equipdev.device[channel].mode_op == OP_SM && stateOpIsConfigured(channel))
    {
        return stateOpSM;
    }

    return NULL;
}

static OpState* SMtoNL(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(equipdev.device[channel].mode_op == OP_NL)
    {
        // Clear Simulation warning

        setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, false);

        // Disable measurement simulation

        device.meas.mode_sim_meas = 0;

        return stateOpNL;
    }

    return NULL;
}

static OpState* NLtoSM(uint8_t channel)
{
    if(equipdev.device[channel].mode_op == OP_SM)
    {
        return stateOpSM;
    }

    return NULL;
}

static OpState* XXtoUC(uint8_t channel)
{
    if (!stateOpIsConfigured(channel))
    {
        return stateOpUC;
    }

    return NULL;
}



/*
 * Transition condition table
 */

#define OP_MAX_TRANSITIONS     2        //!< Maximum transitions from a state

static OpTransition* transitions [][OP_MAX_TRANSITIONS + 1] =
{
    /* OP_UC */ {         UCtoNL, UCtoSM, NULL },
    /* OP_NL */ { XXtoUC, NLtoSM,         NULL },
    /* OP_SM */ { XXtoUC, SMtoNL,         NULL }
};



/*
 * OP State Machine Function
 */

void stateOpUpdate(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Transition condition functions for each state are called in priority order, from left to right

    OpState        * next_state = NULL;
    OpTransition   * transition;
    OpTransition  ** state_transitions = &transitions[device.op_state_mgr.op_state][0];

    // Scan transitions for the current state, testing each condition in priority order

    while((transition = *(state_transitions++)) != NULL && (next_state = (*transition)(channel)) == NULL);

    // If a condition is true then switch to the new state

    if(next_state != NULL)
    {
        // Run new state function with first_call set to true

        device.op_state_mgr.op_state_prev = device.op_state_mgr.op_state;
        device.op_state_mgr.op_state      = next_state(channel, true);

        device.op_state_mgr.op_state_mask = 1 << device.op_state_mgr.op_state;
        device.op_state_mgr.op_state_func = next_state;

        device.status.state_op = device.op_state_mgr.op_state;
    }
    else
    {
        if (device.op_state_mgr.op_state_func == NULL)
        {
            // If the device initializes unconfigured, then execute first the function stateOpUC

            device.op_state_mgr.op_state_func = *stateOpUC;
        }

        // Stay in current state and run the state function with the first_call parameter set to false

        device.op_state_mgr.op_state_func(channel, false);
    }

    // Increment the state counter - this is a service to the application to allow a background thread to wait for the state machine to run

    device.op_state_mgr.op_state_counter++;
}



// State functions

static enum OP_state stateOpUC(uint8_t channel, bool first_call)
{
    return OP_UC;
}

static enum OP_state stateOpNL(uint8_t channel, bool first_call)
{
    return OP_NL;
}

static enum OP_state stateOpSM(uint8_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Enable measurement simulation if MEAS.SIM is ENABLED

    device.meas.mode_sim_meas = (device.meas.sim == FGC_CTRL_ENABLED);

    if(first_call)
    {
        // Reset latched faults and warnings

        device.status.st_faults   = 0;
        device.status.st_warnings = 0;
    }

    // Set Simulation warning

    setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, true);

    return OP_SM;
}



// Static functions definitions

/*
 * Check if we can transition out of Unconfigured state
 */

static bool stateOpIsConfigured(uint8_t channel)
{
    if(fgcd.device[channel].config_unset_count != 0)
    {
        return false;
    }

    return true;
}


// EOF
