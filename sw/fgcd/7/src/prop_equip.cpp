/*!
 * @file   prop_equip.cpp

 * @author Marc Magrans de Abril, Jose M. de Paco
 * @brief  ISEG HV specific properties
 *
 * Class-specific functions for getting and setting properties
 */

#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <cmd.h>
#include <equip_common.h>
#include <prop_equip.h>
#include <prop.h>
#include <fgc_log.h>
#include <fgc_errs.h>
#include <logging.h>
#include <nonvol.h>
#include <equipdev.h>
#include <mode_pc.h>
#include <state_pc.h>

#include <classes/93/defprops.h>
#include <classes/93/defconst.h>
#include <equip_libevtlog.h>
#include <equip_liblog.h>
#include <equip_logger.h>


// Static functions



// SET functions

int32_t SetReset(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    const char *symbol = command->parser->class_data->sym_tab_prop[property->sym_idx].key.c;

    // Check the command is S {PROP} RESET

    if(strcasecmp(command->value, "reset") != 0)
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Property-specific handling

    if(strcmp(symbol, "STATE") == 0)                                            // S CONFIG.STATE RESET
    {
        /*
         * Reset CONFIG.MODE and CONFIG.STATE, and store the time of the last reset.
         *
         * This will normally not be done by a user but rather by the FGC configuration management program following a synchronisation.
         */
    }
    else if(strcmp(symbol, "LOG") == 0)                                         // S LOG RESET
    {
        // LOG - Reset POST_MORTEM bit in unlatched status. This is done automatically by the gateway once the PM logs have been read

        logPrintf(channel, "Called SetReset(LOG) [not implemented]\n");
    }
    else if(strcmp(symbol, "STATUS") == 0)                                      // S MEAS.STATUS RESET
    {
        logPrintf(channel, "Called SetReset(MEAS.STATUS) [not implemented]\n");
    }
    else if(strcmp(symbol, "SPY") == 0)                                         // S SPY RESET
    {
        // SPY - Reset SPY.MPX to the default values
       /*
        * The FGC transmits analogue signals to the diagnostic interface at 1kHz. The user can select which signals are sent using this property.
        * For Class 51 six signals are sent. The property is initialised to the following settings on reset or by the command "S SPY RESET":
        *
        * IREF - the current reference (A)
        * IMEAS - the validated current measurement (A)
        * VREF - the voltage reference (V)
        * VMEAS - the measured voltage (V)
        * IA - the unfiltered 1kHz measurement of the current from DCCT A (A)
        * IB - the unfiltered 1kHz measurement of the current from DCCT B (A)
        */

        logPrintf(channel, "Called SetReset(SPY) [not implemented]\n");
    }
    else if(strcmp(symbol, "ST_LATCHED") == 0)                                  // S STATUS.ST_LATCHED RESET
    {
        // Reset the latched status bits

        // Not needed: no ST_LATCHED in ISEG
    }
    else if(strcmp(symbol, "VS") == 0)                                          // S VS RESET
    {
        /* Request the reset signal to be sent to the VS. It sets a flag, and the cycle thread will check the flag
         * and request the reset of the channel's module.
         */

        struct Equipdev_channel &device = equipdev.device[command->device->id];
        device.status.st_faults = 0;

        vs_reset_flag[channel] = 1;
    }
    else
    {
        // Function has been called for a property it does not handle

        command->error = FGC_NOT_IMPL;
    }

    return command->error != FGC_OK_NO_RSP;
}



int32_t SetResetFaults(struct Cmd *command, struct prop *property)
{
    uint32_t &channel               = command->device->id;
    struct Equipdev_channel &device = equipdev.device[channel];

    // Clear latched faults

    device.status.st_faults = 0;

    /* Request the reset signal to be sent to the VS. It sets a flag, and the cycle thread will check the flag
     * and request the reset of the channel's module.
     */

    vs_reset_flag[channel] = 1;

    return 1;
}



int32_t SetDevicePPM(struct Cmd *command, struct prop *property)
{
    return SetInteger(command, property);
}



int32_t SetRefControlValue(struct Cmd * command, struct prop * property)
{
    uint32_t &channel               = command->device->id;
    struct Equipdev_channel &device = equipdev.device[channel];

    static bool not_first_call[64];

    // Read the value

    char *end;
    errno = 0;

    float value = strtof(command->value, &end);

    if (errno || end == command->value || *end != '\0')
    {
        command->error = FGC_OUT_OF_LIMITS;
        return 1;
    }

    // Check the limits of REF.DIRECT.VALUE (or REF.PULSE.REF.VALUE)

    bool vcv_off_limits =  value > device.load[device.load_select].limits.v.pos ||
                           value < device.load[device.load_select].limits.v.neg;


    if (not_first_call[channel] && vcv_off_limits)
    {
        // Limits violated and not first call (to avoid checking unconfigured limits at startup)

        command->error = FGC_OUT_OF_LIMITS;

        not_first_call[channel] = true;
        return 1;
    }
    else
    {
        // OK

        SetFloat(command, property);
        command->error = FGC_OK_NO_RSP;

        not_first_call[channel] = true;
        return 0;
    }
}

int32_t SetRefPulseDuration(struct Cmd * command, struct prop * property)
{
    // Read the value

    char *end;
    errno = 0;

    float value = strtof(command->value, &end);

    if (errno || end == command->value || *end != '\0')
    {
        command->error = FGC_OUT_OF_LIMITS;
        return 1;
    }


    if (value < 0)
    {
        // Limits violated

        command->error = FGC_OUT_OF_LIMITS;

        return 1;
    }
    else
    {
        // OK

        SetFloat(command, property);
        command->error = FGC_OK_NO_RSP;

        return 0;

    }
}

int32_t SetRefPulse(struct Cmd * command, struct prop * property)
{
    if (property == &PROP_REF_PULSE_DURATION)
    {
        return SetRefPulseDuration(command, property);
    }
    else
    {
        return SetRefControlValue(command, property);
    }
}



int32_t SetPC(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    uint8_t requested_state;

    if(propEquipScanSymbolList(&requested_state, NULL, command, property) != 0)
    {
        return 1;
    }

    command->error = modePcSet(channel, requested_state);

    return command->error != FGC_OK_NO_RSP;
}



int32_t SetRef(struct Cmd *command, struct prop *property)
{
    // Nothing to do in iseg, because the reference is managed by the state machine.

    return 1;
}



int32_t SetPCSimplified(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    uint8_t requested_simplified_state;
    uint8_t normal_state;

    if(propEquipScanSymbolList(&requested_simplified_state, NULL, command, property) != 0)
    {
        return 1;
    }

    normal_state = statePcSimplifiedToNormal(requested_simplified_state, channel);

    command->error = modePcSet(channel, normal_state);

    // Update value of mode.pc_simplified

    if (command->error == FGC_OK_NO_RSP)
    {
        equipdev.device[channel].mode_pc_simplified = requested_simplified_state;
    }
    else
    {
        equipdev.device[channel].mode_pc_simplified = FGC_PC_SIMPLIFIED_FAULT;
    }

    return command->error != FGC_OK_NO_RSP;
}



// SETIF functions


int32_t SetifModeOpOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Check whether device is in a state in which the operational mode may be changed

    switch(device.status.state_pc)
    {
        // States in which the operational mode may be changed

        case FGC_PC_FLT_OFF:
        case FGC_PC_OFF:
        case FGC_PC_FLT_STOPPING:
        case FGC_PC_STOPPING:
            return 1;

        default: // Operational mode may not be changed
            return 0;
    }
}



int32_t SetifRegStateNone(struct Cmd *command)
{
    return SetifPcOff(command);
}



int32_t SetifPcOff(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_pc == FGC_PC_OFF || device.status.state_pc == FGC_PC_FLT_OFF;
}



int32_t SetifOpNormalSim(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    return device.status.state_op == FGC_OP_NORMAL || device.status.state_op == FGC_OP_SIMULATION;
}



int32_t SetifRefOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    if(command->user == 0)
    {
        // Non-PPM command: reference can be set when device is in DIRECT state

        return device.status.state_pc == FGC_PC_DIRECT;
    }
    else
    {
        // PPM commands: reference can be set when device is in CYCLING state

        return device.status.state_pc == FGC_PC_CYCLING;
    }
}



int32_t SetifRefUnlock(struct Cmd *command)
{
    // This function is required only for cycling operation.
    // For the ISEG devices, the condition can always be OK because reference is never locked

    return 1;
}



int32_t SetifPcNotCycling(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    if (device.status.state_pc != FGC_PC_CYCLING)
    {
        return 1;
    }
    else
    {
        return FGC_BAD_STATE;
    }
}



// GET Functions

int32_t GetLogPmBuf(struct Cmd *command, struct prop *property)
{
    // Called when the following properties are accessed:
    //
    // LOG.PM

    uint32_t &channel = command->device->id;

    if(!(command->parser->getopt_flags & GET_OPT_BIN))
    {
        // BIN option was not specified

        command->error = FGC_BAD_GET_OPT;

        return 1;
    }

    // Get PM_BUF - no length check because the command response length is much larger than LOG_PM_BUF_SIZE

    command->value_length = LOG_PM_BUF_SIZE;

    logGetPmBuf(channel, (uint32_t *)command->value);

    return 0;
}



// PARS functions

void ParsLoadSelect(uint32_t channel)
{
    /*
     * Called when the following properties are set:
     *
     * LOAD.SELECT
     */

    // For ISEG there is nothing to do.

    return;
}



void ParsLimits(uint32_t channel)
{
    /*
     * Called when the following properties are set:
     *
     * LIMITS.V.NEG
     * LIMITS.V.POS
     */

    //For ISEG theres is nothing to do.

    return;
}



void ParsMeasSim(uint32_t channel)
{
    /*
     * Called when the following properties are set:
     *
     * MEAS.SIM
     *
     */

    //For ISEG theres is nothing to do.

    return;
}



// EOF
