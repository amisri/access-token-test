/*!
 * @file   mode_pc.cpp
 * @brief  Manage Power Converter mode changes for ISEG devices
 * @author Jose M. de Paco
 */

#include <logging.h>
#include <consts.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <mode_pc.h>



// Type for mode transition functions

typedef bool ModeFunc(uint8_t channel, uint8_t mode);



// Static function declarations



// Global variables

extern bool vs_reset_flag [FGCD_MAX_EQP_DEVS + 1];

bool mode_pc_mask[][18] =
{
    /* [Initial][Target]     PC_FO      PC_OF       PC_FS       PC_SP       PC_ST       PC_SA       PC_TS       PC_SB       PC_IL       PC_TC       PC_AR       PC_RN       PC_AB       PC_CY       PC_PL       PC_BK       PC_EC       PC_DT    */

    /* PC_FO */            { false,     true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_OF */            { false,     false,      false,      false,      false,      true,       false,      true,       false,      false,      false,      false,      false,      true,       false,      false,      false,      true      },

    /* PC_FS */            { false,     true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_SP */            { false,     true,       false,      false,      false,      true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_ST */            { false,     true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_SA */            { false,     true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_TS */            { false,     true,       false,      false,      false,      true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_SB */            { false,     true,       false,      false,      false,      true,       false,      false,      false,      false,      false,      false,      false,      true,       false,      false,      false,      true      },

    /* PC_IL */            { false,     false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_TC */            { false,     true,       false,      false,      false,      true,       false,      true,       false,      false,      false,      false,      false,      true,       false,      false,      false,      false     },

    /* PC_AR */            { false,     false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_RN */            { false,     false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_AB */            { false,     false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_CY */            { false,     true,       false,      false,      false,      true,       false,      true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      true      },

    /* PC_PL */            { false,     false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_BK */            { false,     true,       false,      false,      false,      true,       false,      true,       false,      false,      false,      false,      false,      true,       false,      false,      false,      true      },

    /* PC_EC */            { false,     false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

    /* PC_DT */            { false,     true,       false,      false,      false,      true,       false,      true,       false,      false,      false,      false,      false,      false,      false,      false,      false,      false     },

};



// Static function definitions



// External functions


enum fgc_errno modePcSet(uint8_t channel, uint8_t mode)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Do nothing if device is already in requested state

    if(device.status.state_pc == mode)
    {
        if(device.mode_pc != mode)
        {
            device.mode_pc  = mode;
        }

        return FGC_OK_NO_RSP;
    }

    // Do not allow power converter to be started if a post-mortem acquisition is in progress
    /* PM removed
    if(mode != FGC_PC_OFF && device.fgcd_device->pm_self.acq_info.wait_cycles)
    {
        return FGC_PM_IN_PROGRESS;
    }
    */

    /*
     * Handle requested mode
     */

    // Check if the requested target state is permitted

    if(mode_pc_mask[device.pc_state_mgr.pc_state][mode] == true)
    {
        // Set the mode_pc to the requested mode

        device.mode_pc = mode;

        // Reset the time since last state change request

        device.mode_time_ms = 0;

        // Clear the SLOW_ABORT flag when we change mode

        setStatusBit(&device.state_flags, DEVICE_STATE_SLOW_ABORT, false);

        // Special case. If mode pc is requested perform a reset

        if (mode == FGC_PC_OFF)
        {
            // Reset faults

            vs_reset_flag[channel] = 1;
            device.status.st_faults = 0;
        }

        return FGC_OK_NO_RSP;
    }
    else
    {
        return FGC_BAD_STATE;
    }
}

// EOF
