//! @file  logOutput.c
//! @brief Converter Control Logging library functions for outputting a read request into a buffer.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Include header files

#include "cclibs.h"

#include "liblog.h"

// Local structure declaration

struct LOG_output_control
{
    uint32_t * log_buf;                      //!< Pointer to start of circular buffer the log being readout
    uint32_t * blk_buf;                      //!< Pointer to next free value in the block buffer
    uint32_t   sig_index;                    //!< Element index for current signal
    uint32_t   value_index;                  //!< Index of next value to return for all signals
    uint32_t   blk_els_remaining;            //!< Number of elements remaining to fill in the output block
};


//! Copy log data to destination buffer
//!
//! This function is treated like a real-time function to be as fast as possible.
//!
//! @param[in,out]  dest             Pointer to destination buffer
//! @param[in]      src              Pointer to source buffer
//! @param[in]      step             Step size in the source buffer (in elements)
//! @param[in]      num_elements     Number of elements to copy
//!
//! @returns        Pointer of the next free element in the dest buffer

static uint32_t * logOutputCopy(uint32_t * dest, uint32_t * src, uint32_t const step, uint32_t const num_elements)
{
    uint32_t i;

    for(i = 0 ; i < num_elements ; i++, src += step)
    {
        *(dest++) = *src;
    }

    return dest;
}


//! Transfer up to one block of signal data
//!
//! NON-RT  logOutputSignal
//!
//! This function is optimized to run as fast as possible by avoiding comparisons within the inner copy loops.
//!
//! @param[in]      read_control   Pointer to read_control structure
//! @param[in,out]  read_control   Pointer to read_control structure
//!
//! @returns     values_returned - The number of values of log data copied into the block buffer

static uint32_t logOutputSignal(struct LOG_read_control   * const read_control,
                                struct LOG_output_control * const output_control)
{
    // Clip the number of values to return to the space remaining in the block

    uint32_t values_to_return = read_control->num_samples_from_log - output_control->sig_index;
    uint32_t next_signal;

    if(values_to_return > output_control->blk_els_remaining)
    {
        // More values than space - clip the number of values

        values_to_return = output_control->blk_els_remaining;
        next_signal      = 0;
    }
    else
    {
        // There is enough space in the block for all remaining values so calculate the number of the next signal

        next_signal = 1 + output_control->value_index / read_control->num_samples;
    }

    // Prepare to copy the values in one or two loops, depending on whether the data spans the end of the circular buffer

    uint32_t       element_offset  = read_control->output.element_offset;
    uint32_t const step            = read_control->output.step;

    // Calculate loop 1 length - this is the number of samples to copy before the end of the circular buffer

    uint32_t const last_sample_idx = element_offset + values_to_return * step;

    uint32_t const loop_1_len = last_sample_idx >= read_control->buf_len
                              ? (read_control->buf_len + step - 1 - element_offset) / step
                              : values_to_return;

    // Calculate loop 2 length - this is the number of samples to copy after wrapping back to the beginning of the circular buffer

    uint32_t const loop_2_len = values_to_return - loop_1_len;

    // Run copy loop 1 always

    output_control->blk_buf = logOutputCopy(output_control->blk_buf, &output_control->log_buf[element_offset], step, loop_1_len);

    // Adjust element offset

    element_offset += loop_1_len * step;

    int32_t const next_element_offset = element_offset - read_control->buf_len;

    if(next_element_offset >= 0)
    {
        element_offset = next_element_offset;
    }

    // Run optional second copy loop if the data spans the end of the circular buffer

    if(loop_2_len > 0)
    {
        output_control->blk_buf = logOutputCopy(output_control->blk_buf, &output_control->log_buf[element_offset], step, loop_2_len);

        // Adjust element offset - readout will never cross the end of the buffer twice

        element_offset += loop_2_len * step;
    }

    // Update element_offset according to whether all the values for the current signal have been copied

    read_control->output.element_offset = next_signal > 0
                                        ? read_control->output.start_offset[next_signal]
                                        : element_offset;

    // Return the number of values copied into the block

    return values_to_return;
}


//! Transfer up to one block of zero padding
//!
//! NON-RT  logOutputZeroPadding
//!
//! @param[in]      read_control   Pointer to read_control structure
//! @param[in,out]  read_control   Pointer to read_control structure
//!
//! @returns        The number of values of log data copied into the block buffer

static uint32_t logOutputZeroPadding(struct LOG_read_control   * const read_control,
                                     struct LOG_output_control * const output_control)
{
    // Clip the number of zero padding values to return to the space remaining in the block

    uint32_t zeros_to_return = read_control->num_samples - output_control->sig_index;

    if(zeros_to_return > output_control->blk_els_remaining)
    {
        zeros_to_return = output_control->blk_els_remaining;
    }

    // Set padding zeros in the block buffer

    memset(output_control->blk_buf, 0, zeros_to_return * LOG_VALUE_SIZE);

    output_control->blk_buf += zeros_to_return;

    // Return the number of values set to zero

    return zeros_to_return;
}


// NON-RT  logOutput

uint32_t logOutput(struct LOG_read_control * const read_control,
                   uint32_t                * const buffers,
                   uint32_t                *       blk_buf,
                   uint32_t                  const blk_len)
{
    // Initialise the output control structure

    struct LOG_output_control output_control;

    output_control.log_buf           = &buffers[read_control->buf_offset];
    output_control.blk_buf           = blk_buf;
    output_control.value_index       = read_control->output.total_values_returned;
    output_control.blk_els_remaining = blk_len;

    // Loop until all the log values have been returned or the block is full

    uint32_t values_returned;

    while(output_control.value_index < read_control->data_len && output_control.blk_els_remaining > 0)
    {
        // Calculate the index of the next sample in the current signal from the index for all signals

        output_control.sig_index = output_control.value_index % read_control->num_samples;

        // Transfer log data or zero padding to the output block

        values_returned = output_control.sig_index < read_control->num_samples_from_log
                        ? logOutputSignal     (read_control, &output_control)
                        : logOutputZeroPadding(read_control, &output_control);

        // Increment the value index and decrement the number of elements remaining in the block

        output_control.value_index += values_returned;

        output_control.blk_els_remaining -= values_returned;
    }

    // Adjust the total values_returned

    values_returned = output_control.value_index - read_control->output.total_values_returned;

    read_control->output.total_values_returned = output_control.value_index;

    // Return number of values returned in blk_buf

    return values_returned;
}

// EOF
