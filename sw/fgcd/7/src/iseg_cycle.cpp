/*!
 * @file iseg_cycle.cpp
 * @brief Implementation of the cycle for the ISEG equipment
 * @author Jose M. de Paco
 */


// Includes

#include <stdlib.h>
#include <unistd.h>
#include <string.h>


// FGCD includes

#include "fgc_fieldbus.h"
#include <classes/93/defconst.h>
#include <iseg_cycle.h>
#include <iseg.h>
#include <queue.h>
#include <timing.h>
#include <logging.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <equipdev.h>
#include <pub.h>
#include <cmwpub.h>
#include <fgcd.h>
#include <state_pc.h>
#include <prop_equip.h>
#include <state_op.h>
#include <state_vs.h>

#include <equip_common_93.h>

// Import symbol definitions

#pragma GCC diagnostic ignored "-Wwrite-strings" // Disable warning about deprecated conversion from string constant to char*
#define DEFSYMS
#include <classes/93/defsyms.h>
#pragma GCC diagnostic warning "-Wwrite-strings"


extern struct Iseg_connection iseg_connection;


// Constants



// Internal structures, unions and enumerations

/*!
 *  Structures for data-driven access to the ISEG cyclic items
 */

static struct Iseg_items iseg_channel_fast_items[] =
{  // Name                      Type               Level             Pointer to destination
    {"VoltageMeasure",          ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.voltage_meas,           sizeof(equipdev.device[0])                               },
    {"CurrentMeasure",          ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.current_meas,           sizeof(equipdev.device[0])                               },
    {"Status",                  ISEG_ITEM_INT32,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.status,                 sizeof(equipdev.device[0])                               },
    {"EventStatus",             ISEG_ITEM_INT32,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.event,                  sizeof(equipdev.device[0])                               },
};

static struct Iseg_items iseg_module_fast_items[] =
{  // Name                      Type               Level             Pointer to destination
    {"Connected",               ISEG_ITEM_INT16,     ISEG_MODULE,     &fgcddev.iseg.module.connected[0],                       sizeof(fgcddev.iseg.module.connected[0])                 },
    {"Status",                  ISEG_ITEM_INT32,     ISEG_MODULE,     &fgcddev.iseg.module.status[0],                          sizeof(fgcddev.iseg.module.status[0])                    },
    {"EventStatus",             ISEG_ITEM_INT32,     ISEG_MODULE,     &fgcddev.iseg.module.event[0],                           sizeof(fgcddev.iseg.module.event[0])                     },
};

static struct Iseg_items iseg_crate_fast_items[] =
{  // Name                      Type               Level             Pointer to result
    {"Status",                  ISEG_ITEM_INT32,     ISEG_CRATE,      &fgcddev.iseg.crate.status,                              0                                                        },
    {"EventStatus",             ISEG_ITEM_INT32,     ISEG_CRATE,      &fgcddev.iseg.crate.event,                               0                                                        },
    {"Status",                  ISEG_ITEM_INT32,     ISEG_LINE,       &fgcddev.iseg.line.status,                               0                                                        },
    {"Status",                  ISEG_ITEM_INT32,     ISEG_SYSTEM,     &fgcddev.iseg.system.status,                             0                                                        },
};

static struct Iseg_items iseg_channel_slow_items[] =
{  // Name                      Type               Level             Pointer to destination
    {"VoltageSet",              ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.voltage_set,            sizeof(equipdev.device[0])                               },
    {"Control",                 ISEG_ITEM_INT16,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.control,                sizeof(equipdev.device[0])                               },
    {"DelayedTripAction",       ISEG_ITEM_INT16,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.delayed_trip_action,    sizeof(equipdev.device[0])                               },
    {"DelayedTripTime",         ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.delayed_trip_time,      sizeof(equipdev.device[0])                               },
    {"VoltageBounds",           ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.voltage_bounds,         sizeof(equipdev.device[0])                               },
    {"CurrentSet",              ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.current_trip_set,       sizeof(equipdev.device[0])                               },
    {"VoltageNominal",          ISEG_ITEM_FLOAT,     ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.voltage_nominal,        sizeof(equipdev.device[0])                               },
};

static struct Iseg_items iseg_module_slow_items[] =
{  // Name                      Type               Level             Pointer to destination
    {"Temperature",             ISEG_ITEM_FLOAT,     ISEG_MODULE,     &fgcddev.iseg.module.temperature[0],                     sizeof(fgcddev.iseg.module.temperature[0])               },
    {"VoltageRampSpeed",        ISEG_ITEM_FLOAT,     ISEG_MODULE,     &fgcddev.iseg.module.voltage_ramp_speed[0],              sizeof(fgcddev.iseg.module.params.voltage_ramp_speed[0]) },
};

static struct Iseg_items iseg_crate_slow_items[] =
{  // Name                      Type               Level             Pointer to result
    {"Connected",               ISEG_ITEM_INT16,     ISEG_CRATE,      &fgcddev.iseg.crate.connected,                           0                                                        },
    {"PowerOn",                 ISEG_ITEM_INT16,     ISEG_CRATE,      &fgcddev.iseg.crate.power_on_status,                     0                                                        },
    {"FanSpeed",                ISEG_ITEM_FLOAT,     ISEG_CRATE,      &fgcddev.iseg.crate.fan_speed,                           0                                                        },
    {"CycleCounter",            ISEG_ITEM_INT32,     ISEG_SYSTEM,     &fgcddev.iseg.system.cycle_counter,                      0                                                        },
};

/*
 * Structure for ISEG data pointers and device statuses
 */

struct IsegCycle_data
{
    // ISEG Cycle thread and inter-process communication

    pthread_t   thread;         // ISEG cycle main thread
    uint64_t    start_time;     // Cycle start time for profiling
};

/*
 * Structure for cycle information
 */

struct IsegSlowCycle
{
    bool    slow_cycle_is_on;
    uint8_t number_cycles;
    uint8_t counter;
}iseg_slow_cycle;



// Global variables, zero-initialised

struct IsegCycle_data   iseg_cycle = {};    // Variable data pointers and IPC for ISEG cycle.

bool vs_reset_flag [FGCD_MAX_EQP_DEVS + 1];



// Internal function declarations

/*
 * Function to handle the TCP/IP connection with the iseg crate controller
 */
static void isegCycleHandleConnection(void);

/*!
 * Update channel's fast STATUS
 */
static void isegCycleUpdateChannelStatus(const uint8_t channel);

/*!
 * Update Module and Crate STATUS
 */
static void isegCycleUpdateModuleAndCrateStatus(void);

/*!
 * Get the value of a bit from ISEG.CRATE.STATUS/EVENT symbol lists: 32 bits
 */
static bool getStatusBit32(uint32_t status, uint32_t mask);

/*!
 * Initialize Slow Cycle Management Structure
 */
static void isegInitializeSlowCycle(void);

/*!
 * Cycle for equipment devices in simulation OP mode
 */
static void isegCycleSimulation(const uint8_t channel);

/*!
 * Cycle for equipment devices in normal OP mode
 */
static void isegCycleEquipDevices(const uint8_t channel);

/*!
 * Simulate the items of an ISEG channel
 */
static void isegCycleSimulateChannel(const uint8_t channel);

/*!
 * Cycle for the modules
 */
static void isegCycleModules(void);

/*!
 * Cycle for the crate
 */
static void isegCycleCrate(void);

/*!
 * Log the fault when the Power Converter transitions to a fault state
 */
static void isegCycleLogStatePcFault(uint8_t channel);

/*!
 * Notify CMW of any changes in ISEG state or status
 */
static void isegCycleNotifyCMW(uint32_t channel);

/*!
 * ISEG Cycle thread
 */
static void *isegCycleRun(void *unused);




// External function definitions

int32_t isegCycleStart(void)
{
    return fgcd_thread_create(&iseg_cycle.thread, isegCycleRun, NULL, ISEG_CYCLE_THREAD_POLICY, ISEG_CYCLE_THREAD_PRIORITY, NO_AFFINITY);
}



// Internal function definitions

static void isegCycleHandleConnection(void)
{
    if (isegIsConnected() == false)
    {
        // Connect

        isegConnectCrate();

        if (isegIsConnected()) // Connection successful
        {
            // Declare devices as online and reset the comm. warning

            uint8_t channel;

            for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
            {
                struct Equipdev_channel device = equipdev.device[channel];

                fgcd.device[channel].online = 1;
                fgcd.device[channel].ready  = 1;

                setStatusBit(&device.status.st_warnings, FGC_WRN_COMMS, false);
             }

            setStatusBit(&fgcddev.status.st_warnings, FGC_WRN_COMMS_ERRORS, false);

            // Set all the config params to the ISEG hardware

            isegSetAllParameters();
        }

        else
        {
            // Mark device as offline and set communication warning

            uint8_t channel;

            for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
            {
                struct Equipdev_channel device = equipdev.device[channel];

                if(equipdev.device[channel].status.state_op != FGC_OP_SIMULATION) // Not in simulation
                {
                    fgcd.device[channel].online = 0;

                    setStatusBit(&device.status.st_warnings,  FGC_WRN_COMMS,         true);
                    setStatusBit(&fgcddev.status.st_warnings, FGC_WRN_COMMS_ERRORS,  true);
                }
                else // Simulation
                {
                    fgcd.device[channel].online = 1;

                    setStatusBit(&device.status.st_warnings,  FGC_WRN_COMMS,         false);
                }
            }
        }
    }
}



static void isegCycleUpdateChannelStatus(const uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set MEAS.V.VALUE (Float)

    device.meas.v_meas = device.iseg.channel.voltage_meas;

    // Set MEAS.I.VALUE (Float)

    device.meas.i_meas = device.iseg.channel.current_meas;


    if (device.mode_op != OP_SM)  // Do not update iseg related status if in simulation mode
    {
        // STATUS.UNLATCHED

        setStatusBit(&device.status.st_unlatched,  FGC_UNL_LOW_CURRENT_RANGE,   getStatusBit32(device.iseg.channel.status, FGC_ISG_ST_IS_LOW_CURRENT_RANGE));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_CONSTANT_VOLTAGE,    getStatusBit32(device.iseg.channel.status, FGC_ISG_ST_IS_CONSTANT_VOLTAGE));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_CONSTANT_CURRENT,    getStatusBit32(device.iseg.channel.status, FGC_ISG_ST_IS_CONSTANT_CURRENT));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_RAMPING,             getStatusBit32(device.iseg.channel.status, FGC_ISG_ST_IS_RAMPING));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_HIGH_VOLTAGE_ON,     getStatusBit32(device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_END_OF_RAMP,         getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_END_OF_RAMP));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_ON_TO_OFF,           getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_ON_TO_OFF));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_VOLTAGE_BOUNDS,      getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_VOLTAGE_BOUNDS));
        setStatusBit(&device.status.st_unlatched,  FGC_UNL_CURRENT_BOUNDS,      getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_CURRENT_BOUNDS));

        // STATUS.WARNINGS

        bool vs_warning = getStatusBit(device.status.st_warnings, FGC_WRN_TEMPERATURE)    ||
                          getStatusBit(device.status.st_warnings, FGC_WRN_INPUT_ERROR)    ||
                          getStatusBit(device.status.st_warnings, FGC_WRN_MODULE_WARNING);

        setStatusBit(&device.status.st_warnings,   FGC_WRN_VS_WARNING,          vs_warning);
        // not needed setStatusBit(&device.status.st_warnings,   FGC_WRN_INPUT_ERROR,         getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_INPUT_ERROR));
        setStatusBit(&device.status.st_warnings,   FGC_WRN_TEMPERATURE,         getStatusBit32(fgcddev.iseg.module.event[device.iseg.channel.address.module], FGC_ISG_M_EV_TEMPERATURE_FAULT));


        // STATUS.FAULTS

        latchStatusBit(&device.status.st_faults,   FGC_FLT_VS_FAULT,            getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_TRIP_EXCEEDED));
        latchStatusBit(&device.status.st_faults,   FGC_FLT_ARC_OR_REGULATION,  (getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_ARC_OR_REGULATION)
                                                                                     || getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_ARC_ERROR)));
        latchStatusBit(&device.status.st_faults,   FGC_FLT_REG_ERROR,           getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_ARC_OR_REGULATION));
        latchStatusBit(&device.status.st_faults,   FGC_FLT_HW_V_LIMITS,         getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_VOLTAGE_LIMIT));



        // Temporary for ELECTRON COOLER RC1 ELENA (EPCCCS-9187)

        if (!strcmp(fgcd.device[channel].name, "RPAEZ.193.LNR.RC1.0427") || !strcmp(fgcd.device[channel].name, "RPAEZ.193.LNR.RC2.0427"))
        {
            latchStatusBit(&device.status.st_faults,   FGC_FLT_HW_I_LIMITS, 0);
        }
        else
        {
            latchStatusBit(&device.status.st_faults,   FGC_FLT_HW_I_LIMITS,         getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_CURRENT_LIMIT));
        }


        latchStatusBit(&device.status.st_faults,   FGC_FLT_VS_HW_EXT_INTLOCK,   getStatusBit32(device.iseg.channel.event,  FGC_ISG_EV_HW_EXT_INTLOCK)
                                                                                     || getStatusBit32(fgcddev.iseg.module.event[device.iseg.channel.address.module], FGC_ISG_M_EV_SAFETY_LOOP_OPEN));

    }


    bool v_limits = (device.meas.v_meas > ( 1 + device.load[device.load_select].limits.v.pos * 1.01)) ||
                    (device.meas.v_meas < (-1 + device.load[device.load_select].limits.v.neg * 1.01));

    latchStatusBit(&device.status.st_faults, FGC_FLT_V_LIMITS, v_limits);

    bool i_limits = (device.meas.i_meas > ( 0.00001 + device.load[device.load_select].limits.i.pos * 1.01)) ||
                    (device.meas.i_meas < (-0.00001 + device.load[device.load_select].limits.i.neg * 1.01));

    latchStatusBit(&device.status.st_faults, FGC_FLT_I_LIMITS, i_limits);
}




static void isegCycleUpdateModuleAndCrateStatus(void)
{
    // STATUS.FAULTS

    bool crate_psu   = getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_LOW_24V_BATTERY)          ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_HIGH_24V_BATTERY)         ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_LOW_5V_BACKPLANE)         ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_HIGH_5V_BACKPLANE)        ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_LOW_24V_BACKPLANE)        ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_HIGH_24V_BACKPLANE)       ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_LOW_5V_CRATE)             ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_HIGH_5V_CRATE)            ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_LOW_3V_CRATE)             ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_HIGH_3V_CRATE);

    bool crate_fault = crate_psu ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_SERVICE_NEEDED)           ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_POWER_FAIL)               ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_HIGH_TEMPERATURE)         ||
                       getStatusBit32(fgcddev.iseg.crate.event,      FGC_ISG_CR_EV_SHUTDOWN);

    bool module_fault = false;
    uint8_t i = 0;

    while ((i < 8) && (module_fault == false))
    {
        module_fault = getStatusBit32  (fgcddev.iseg.module.event[i],  FGC_ISG_M_EV_SERVICE_NEEDED)            ||
                       getStatusBit32  (fgcddev.iseg.module.event[i],  FGC_ISG_M_EV_HW_VOLTAGE_LIMIT_FAULT)    ||
                       getStatusBit32  (fgcddev.iseg.module.event[i],  FGC_ISG_M_EV_SUPPLIES_FAULT)            ||
                       getStatusBit32  (fgcddev.iseg.module.event[i],  FGC_ISG_M_EV_TEMPERATURE_FAULT);

        ++i;
    }

    setStatusBit(&fgcddev.status.st_faults,     FGC_FLT_CRATE_PSU,              crate_psu);
    setStatusBit(&fgcddev.status.st_faults,     FGC_FLT_CRATE,                  crate_fault);
    setStatusBit(&fgcddev.status.st_faults,     FGC_FLT_MODULES,                module_fault);
    setStatusBit(&fgcddev.status.st_faults,     FGC_FLT_POWER_FAIL,             getStatusBit32(fgcddev.iseg.crate.event,  FGC_ISG_CR_EV_POWER_FAIL));
    setStatusBit(&fgcddev.status.st_faults,     FGC_FLT_CRATE_DISABLED,        !getStatusBit32(fgcddev.iseg.crate.status, FGC_ISG_CR_ST_IS_CRATE_ENABLED));
    setStatusBit(&fgcddev.status.st_faults,     FGC_FLT_POWER_OFF,             !getStatusBit32(fgcddev.iseg.crate.status, FGC_ISG_CR_ST_IS_POWER_ON));

    // STATUS.WARNINGS

    bool module_safety_loop = false;
    i = 0;

    while ((i < 8) && (module_safety_loop == false))
    {
        module_safety_loop = getStatusBit32(fgcddev.iseg.module.event[i],  FGC_ISG_M_EV_SAFETY_LOOP_OPEN);
        ++i;
    }

    setStatusBit(&fgcddev.status.st_warnings,   FGC_WRN_SAFETY_LOOP,            module_safety_loop);
    setStatusBit(&fgcddev.status.st_warnings,   FGC_WRN_CRATE_SERVICE_NEEDED,   getStatusBit32(fgcddev.iseg.crate.event, FGC_ISG_CR_EV_SERVICE_NEEDED));
    setStatusBit(&fgcddev.status.st_warnings,   FGC_WRN_CRATE_TEMPERATURE,      getStatusBit32(fgcddev.iseg.crate.event, FGC_ISG_CR_EV_HIGH_TEMPERATURE));
}



static void isegCycleSimulateChannel(const uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set the device as online

    fgcd.device[channel].online = 1;

    // Simulate status of power converter

    device.iseg.channel.event  = 0;
    device.iseg.channel.status = 0;
    device.meas.i_meas         = 0.0;

    if (device.status.state_pc == FGC_PC_OFF || device.status.state_pc == FGC_PC_STOPPING || device.status.state_pc == FGC_PC_FLT_OFF ||device.status.state_pc == FGC_PC_FLT_STOPPING)
    {
        setStatusBit32(&device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON, false);
    }
    else
    {
        setStatusBit32(&device.iseg.channel.status, FGC_ISG_ST_IS_HIGH_VOLTAGE_ON, true);
    }

    // Simulate measurement = reference

    if (device.meas.sim)
    {
        if (device.mode_pc == FGC_PC_DIRECT && device.status.state_vs == FGC_VS_READY) // direct mode AND VS ON
        {
            device.iseg.channel.voltage_meas = device.ref.vcv;
        }
        else if (device.mode_pc == FGC_PC_CYCLING && device.status.state_vs == FGC_VS_READY) // cycling state AND VS ON
        {
            device.iseg.channel.voltage_meas = device.ppm[0].cycling_v_ref;
        }
        else if((device.mode_pc == FGC_PC_ON_STANDBY || device.mode_pc == FGC_PC_TO_STANDBY) && device.status.state_vs == FGC_VS_READY)
        {
            device.iseg.channel.voltage_meas = device.load[device.load_select].limits.v.min;
        }
        else
        {
            device.iseg.channel.voltage_meas = 0.0;
        }
    }
    else
    {
        device.iseg.channel.voltage_meas = 0.0;
    }
}



static void isegInitializeSlowCycle(void)
{
    // Count number of configured devices

    uint8_t number_configured_devices = 0;
    uint8_t n;

    for(n = 1; n <= FGCD_MAX_EQP_DEVS; n++)
    {
        if((&equipdev.device[n])->fgcd_device->name)
        {
            number_configured_devices++;
        }
    }

    if (number_configured_devices > 0)
    {
        // Compute number of cycles to get the slow items

        iseg_slow_cycle.number_cycles = 40 * FGCD_MAX_EQP_DEVS / number_configured_devices;

        // Initialize the cycle counter

        iseg_slow_cycle.counter = iseg_slow_cycle.number_cycles;
    }
    else
    {
        logPrintf(0, "ISEG_CYCLE ERROR Number of configured devices is ZERO  \n");
    }

}



static void isegCycleSimulation(const uint8_t channel)
{
    // Simulate channel items

    isegCycleSimulateChannel(channel);

    // Update STATUS property

    isegCycleUpdateChannelStatus(channel);

    // Update the VS state machine

    GetVsState(channel);

    // Update the PC state machine

    statePcUpdate(channel);
}



static void isegCycleEquipDevices(const uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Update the VS state machine

    GetVsState(channel);

    // Update the PC state machine

    statePcUpdate(channel);

    // Skip device if not connected

    if(isegIsConnected())
    {
        // Get channel fast items

        isegGetIsegItems(channel, iseg_channel_fast_items, sizeof(iseg_channel_fast_items) / sizeof(iseg_channel_fast_items[0]));

        // Get channel slow items

        if(iseg_slow_cycle.slow_cycle_is_on)
        {
            isegGetIsegItems(channel, iseg_channel_slow_items, sizeof(iseg_channel_slow_items) / sizeof(iseg_channel_slow_items[0]));
        }

        // Update STATUS property

        isegCycleUpdateChannelStatus(channel);

        // Reset if flag is active

        if (vs_reset_flag[channel])
        {
            static bool reset_done_prev_cycle = false;

            if (device.status.state_op != FGC_OP_SIMULATION && reset_done_prev_cycle == false)
            {
                isegResetChannel(channel);
                reset_done_prev_cycle = true;
            }

            else if (reset_done_prev_cycle)
            {
                /*
                 * Clear st_faults in the next cycle after a reset.
                 * Motivation: the iseg event status needs time to be cleared after the reset -> give one
                 * additional cycle to clear the latched faults.
                 */

                device.status.st_faults = 0;

                // Clear the flags

                vs_reset_flag[channel] = false;
                reset_done_prev_cycle  = false;
            }
        }
    }
}



static void isegCycleModules(void)
{
    uint8_t module_index;

    for (module_index = 0; module_index < FGCD_NUMBER_ISEG_MODULES; module_index++)
    {
        // Fast data

        isegGetIsegItems(module_index, iseg_module_fast_items, sizeof(iseg_module_fast_items) / sizeof(iseg_module_fast_items[0]));

        // Slow data

        if (iseg_slow_cycle.slow_cycle_is_on)
        {
            isegGetIsegItems(module_index, iseg_module_slow_items, sizeof(iseg_module_slow_items) / sizeof(iseg_module_slow_items[0]));
        }
    }
}



static void isegCycleCrate(void)
{
    // Read system, line and crate data

    // Fast data

    isegGetIsegItems(0, iseg_crate_fast_items, sizeof(iseg_crate_fast_items) / sizeof(iseg_crate_fast_items[0]));

    // Slow data

    if (iseg_slow_cycle.slow_cycle_is_on)
    {
        isegGetIsegItems(0, iseg_crate_slow_items, sizeof(iseg_crate_slow_items) / sizeof(iseg_crate_slow_items[0]));
    }

    // Update the status of the modules and crate

    isegCycleUpdateModuleAndCrateStatus();

    // Check if Power is ON and turn it ON if it is not

    if (!getStatusBit32(fgcddev.iseg.crate.status,  FGC_ISG_CR_ST_IS_POWER_ON) && fgcddev.iseg.crate.auto_power_on == FGC_CTRL_ENABLED)
    {
        isegHVPowerOn();
    }

    // Reset the ISEG crate controller and modules if requested (ISEG.RESET has been set)

    if (fgcddev.iseg.reset == 1)
    {
        isegResetAll();
    }

    // Switch ON/OFF the crate if requested (ISEG.CRATE.POWER.COMMAND)

    if (fgcddev.iseg.crate.power_on_command == 1 && !getStatusBit32(fgcddev.iseg.crate.status,  FGC_ISG_CR_ST_IS_POWER_ON))
    {
        isegHVPowerOn();
        fgcddev.iseg.crate.power_on_command = 0;

    }
    if (fgcddev.iseg.crate.power_off_command == 1 && getStatusBit32(fgcddev.iseg.crate.status,  FGC_ISG_CR_ST_IS_POWER_ON))
    {
        isegHVPowerOff();
        fgcddev.iseg.crate.power_off_command = 0;
    }
}



static void isegCycleLogStatePcFault(uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    char buffer[LOG_MESSAGE_LENGTH + 1];

    size_t pos = snprintf(buffer, sizeof(buffer), "STATE STATE_PC=%s: st_faults=0x%04hX ", sym_names_pc[device.status.state_pc].label, device.status.st_faults);

    // Print the labels for all the fault bits that are set

    for(uint32_t i = 0; sym_names_flt[i].label; ++i)
    {
        if (sym_names_flt[i].type & device.status.st_faults)
        {
            pos += snprintf(buffer+pos, LOG_MESSAGE_LENGTH-10-pos, "%c%s", i == 0 ? '(' : ',', sym_names_flt[i].label);
        }
    }

    strcpy(buffer+pos, ")\n");

    logPrintf(channel, buffer);
}



static void isegCycleNotifyCMW(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    bool is_status_changed = false;

    if(device.status.st_faults != device.status_prev.st_faults)
    {
        cmwpubNotify(channel, "STATUS.FAULTS", 0, 0, 0, 0, 0);

        device.status_prev.st_faults = device.status.st_faults;
        is_status_changed = true;
    }

    if(device.status.st_warnings != device.status_prev.st_warnings)
    {
        cmwpubNotify(channel, "STATUS.WARNINGS", 0, 0, 0, 0, 0);

        device.status_prev.st_warnings = device.status.st_warnings;
        is_status_changed = true;
    }

    if(device.status.st_unlatched != device.status_prev.st_unlatched)
    {
        cmwpubNotify(channel, "STATUS.ST_UNLATCHED", 0, 0, 0, 0, 0);

        device.status_prev.st_unlatched = device.status.st_unlatched;
        is_status_changed = true;
    }

    if(is_status_changed)
    {
        cmwpubNotify(channel, "STATUS", 0, 0, 0, 0, 0);
    }

    if(device.status.state_op != device.status_prev.state_op)
    {
        cmwpubNotify(channel, "STATE.OP", 0, 0, 0, 1, 0);

        device.status_prev.state_op = device.status.state_op;
    }

    if(device.status.state_vs != device.status_prev.state_vs)
    {
        logPrintf(channel, "ISEG_CYCLE STATE_VS=%s \n", sym_names_vs[device.status.state_vs].label);
        cmwpubNotify(channel, "VS.STATE", 0, 0, 0, 1, 0);

        device.status_prev.state_vs = device.status.state_vs;
    }

    if(device.mode_pc != device.mode_pc_prev)
    {
        cmwpubNotify(channel, "MODE.PC", 0, 0, 0, 1, 0);

        device.mode_pc_prev = device.mode_pc;
    }

    if(device.state_pc_simplified != device.state_pc_simplified_prev)
    {
        cmwpubNotify(channel, "STATE.PC_SIMPLIFIED", 0, 0, 0, 1, 0);

        device.state_pc_simplified_prev = device.state_pc_simplified;
    }

    if(device.status.state_pc != device.status_prev.state_pc)
    {
        // Log the state change

        if(device.status.state_pc == FGC_PC_FLT_OFF || device.status.state_pc == FGC_PC_FLT_STOPPING)
        {
            isegCycleLogStatePcFault(channel);
        }

        cmwpubNotify(channel, "PC", 0, 0, 0, 0, 0);
        cmwpubNotify(channel, "STATE.PC", 0, 0, 0, 1, 0);

        device.status_prev.state_pc = device.status.state_pc;
    }
}



static void *isegCycleRun(void *unused)
{
    timeval cycle_start;

    // Initialized used modules

    isegMarkUsedModules();

    // Initialize slow cycle management structure

    isegInitializeSlowCycle();

    // Start cycle

    while(true)
    {
        // Handle the TCP/IP connection with the crate controller

        isegCycleHandleConnection();

        // Read cycle time

        timingGetUserTime(0, &cycle_start);

        // Update cycle counter for slow items

        iseg_slow_cycle.counter--;

        if(iseg_slow_cycle.counter == 0)
        {
            // Activate slow cycle

            iseg_slow_cycle.slow_cycle_is_on = true;

            // Re-initialize

            iseg_slow_cycle.counter = iseg_slow_cycle.number_cycles;
        }

        // Run the cycle for each equipment device

        uint8_t channel;

        for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
        {
            // Skip device if not configured

            if(!equipdev.device[channel].fgcd_device->name)
            {
                continue;
            }

            // Update the operational state machine

            stateOpUpdate(channel);

            // Cycle for devices in normal mode

            if(equipdev.device[channel].status.state_op == FGC_OP_NORMAL)
            {
                isegCycleEquipDevices(channel);
            }

            // Cycle for devices in simulation mode

            else if(equipdev.device[channel].status.state_op == FGC_OP_SIMULATION)
            {
                isegCycleSimulation(channel);
            }

            // Notify CMW

            isegCycleNotifyCMW(channel);
        }

        if(isegIsConnected())
        {
            // Modules

            isegCycleModules();

            // Crate

            isegCycleCrate();

            // Set configuration parameters only if changed

            isegSetModifiedParameters();
        }

        // Reset slow cycle if it was active

        iseg_slow_cycle.slow_cycle_is_on = false;
    }
    return 0;
}



// EOF

