/*!
 * @file   prop.cpp
 * @brief  Functions for getting and setting properties
 * @author Stephen Page
 * @author Michael Davis
 */

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <defconst.h>
#include <devname.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <hash.h>
#include <logging.h>
#include <parser.h>
#include <prop.h>
#include <rbac.h>
#include <tcp.h>
#include <timing.h>


// Global variables

struct Prop_globals prop;



int32_t GetAbsTime(struct Cmd *command, struct prop *property)
{
    char            buffer[32];
    const char     *format;
    uint32_t        i;
    uint32_t        length;
    struct Parser  *parser = command->parser;
    struct timeval  value;
    struct timeval *data;
    int32_t         zero_flag;

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    // Set zero flag

    zero_flag = (property->flags        & PF_GET_ZERO) &&
                (parser->getopt_flags   & GET_OPT_ZERO);

    // Format each value and add to response

    format = parser->getopt_flags & GET_OPT_HEX ? "0x%lX.%06X," : "%lu.%06u,";
    data   = reinterpret_cast<struct timeval*>(parser->value + (parser->from * property->step));

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        value  = *data;

        if(zero_flag)
        {
            data->tv_sec  = 0;
            data->tv_usec = 0;
        }

        data += (property->step / prop_type_size[property->type]);

        length = sprintf(buffer, format, value.tv_sec, (uint32_t)value.tv_usec);

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }

        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Decrement value length to overwrite trailing comma

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



int32_t SetAbsTime(struct Cmd *command, struct prop *property)
{
    struct timeval *buffer;
    uint32_t        buffer_length = 0;
    struct timeval *data;
    char            delimiter;
    uint32_t        i;
    struct Parser  *parser        = command->parser;
    uint32_t        size          = prop_type_size[property->type];
    struct timeval  value;
    double          value_d;

    buffer = reinterpret_cast<struct timeval*>(&parser->set_buffer) + parser->num_set_pads;
    data   = reinterpret_cast<struct timeval*>(parser->value + (parser->from * property->step));

    // Loop for each parameter

    do
    {
        // Get default value

        if(buffer_length < parserGetNumEls(parser))
        {
            value = *data;
        }
        else
        {
            value.tv_sec    = 0;
            value.tv_usec   = 0;
        }

        // Check whether property is in network byte order

        if(property->flags & PF_NET_ORDER)
        {
            value.tv_sec    = ntohl(value.tv_sec);
            value.tv_usec   = ntohl(value.tv_usec);
        }

        data += property->step / size;

        // Scan float from value

        parserScanFloat(parser, command->value,
                        &command->index, command->value_length,
                        ",", 1, &delimiter,
                        1, 1,                                                   // enable min & max limits
                        (double)(unsigned int)((struct int_limits *)property->range)->min,      // TODO: this is super fragile, property->range is weakly typed
                        (double)(unsigned int)((struct int_limits *)property->range)->max,      //       with no way to check validity of the cast
                        &value_d);

        switch(command->error)
        {
            case 0: // Float was successfully read from command

                // Convert floating point value to time value

                value.tv_sec    = (uint32_t)value_d;
                value.tv_usec   = ((value_d - value.tv_sec) * 1000000) + .49999999; // .49999999 is to round to nearest integer
                break;

            case FGC_NO_SYMBOL: // No symbol, take default value

                // Take default value if delimiter is comma or this is not first element

                if(delimiter == ',' || buffer_length)
                {
                    command->error = 0;
                }
                break;

            default: // An error has occurred
                return 1;
        }

        if(!command->error)
        {
            // Check whether the maximum number of elements for set has been exceeded or the set buffer is full

            if(buffer_length == parser->max_set_elements ||
               ((buffer_length * size) >= PARSER_BUF_LEN))
            {
                command->error = FGC_SET_BUF_FULL;
                return 1;
            }

            // Check whether property is in network byte order

            if(property->flags & PF_NET_ORDER)
            {
                value.tv_sec    = htonl(value.tv_sec);
                value.tv_usec   = htonl(value.tv_usec);
            }

            // Store value in buffer

            *(buffer++) = value;
            buffer_length++;
        }
        else if(command->error == FGC_NO_SYMBOL)
        {
            command->error = 0;
        }
    } while(!command->error && delimiter == ',');

    // Check that the buffer will not overrun the maximum length of the property

    if(parser->set_index + buffer_length > property->max_elements)
    {
        command->error = FGC_BAD_ARRAY_LEN;
        return 1;
    }

    // Copy the buffer to the property's value

    for(i = 0 ; i < (buffer_length + parser->num_set_pads) ; i++)
    {
        *reinterpret_cast<struct timeval*>(parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<struct timeval*>(parser->set_buffer + (i * size));
    }

    // Set the number of elements in the property

    if(!((parser->set_index + parser->num_set_pads + buffer_length) < parserGetNumEls(parser) &&
         ((property->flags & PF_DONT_SHRINK) ||
          (parser->num_array_specifiers == 2 && buffer_length == parser->max_set_elements))
      ))
    {
        parserSetNumEls(parser, parser->set_index + parser->num_set_pads + buffer_length);
    }

    return 0;
}



int32_t GetBin(struct Cmd *command, struct prop *property)
{
    uint32_t        i;
    void            *p;
    struct Parser   *parser = command->parser;

    // Return an error if invalid get options were specified or property is a parent

    if(!(parser->getopt_flags & GET_OPT_BIN)    ||
        (parser->getopt_flags & GET_OPT_ZERO)   ||
        property->type == PT_PARENT)
    {
        command->error = FGC_BAD_GET_OPT;
        return 1;
    }

    // Check that value buffer is large enough to contain value

    if(command->value_length + 1 + sizeof(uint32_t) +
      (prop_type_size[property->type] * (parser->to - parser->from) + 1) >= CMD_MAX_VAL_LENGTH)
    {
        command->error = FGC_RSP_BUF_FULL;
        return 1;
    }

    // Copy data to value

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        p = parser->value + (i * property->step);

        // Copy element to value

        memcpy(&command->value[command->value_length], p, prop_type_size[property->type]);
        command->value_length += prop_type_size[property->type];
    }

    return 0;
}



int32_t SetBin(struct Cmd *command, struct prop *property)
{
    uint32_t        i;
    uint32_t        length;
    void            *p;
    struct Parser   *parser = command->parser;

    // Check that number of pad elements is zero

    if(parser->num_set_pads)
    {
        command->error = FGC_BAD_ARRAY_IDX;
        return 1;
    }

    // Set length

    length = command->value_length - command->index;

    // Check that property is large enough to contain value

    if(length > property->max_elements * prop_type_size[property->type])
    {
        command->error = FGC_BAD_ARRAY_LEN;
        return 1;
    }

    // Check that length is a multiple of property type size

    if(length % prop_type_size[property->type])
    {
        command->error = FGC_NO_SYMBOL;
        return 1;
    }

    // Copy the buffer to the property's value

    for(i = 0 ; i < (length / prop_type_size[property->type]) ; i++)
    {
        p = parser->value + ((i + parser->set_index) * property->step);

        // Copy element to value

        memcpy(p, &command->value[command->index], prop_type_size[property->type]);
        command->index += prop_type_size[property->type];
    }

    // Set the number of elements in the property

    if(!((parser->set_index + length) < parserGetNumEls(parser) &&
         ((property->flags & PF_DONT_SHRINK) ||
          (parser->num_array_specifiers == 2 && length == parser->max_set_elements))
      ))
    {
        parserSetNumEls(parser, parser->set_index + (length / prop_type_size[property->type]));
    }

    return 0;
}



int32_t GetChar(struct Cmd *command, struct prop *property)
{
    char            buffer[32];
    char            *c;
    uint32_t        i;
    uint32_t        length;
    struct Parser   *parser = command->parser;

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    // Return empty value if property has no elements

    if(parserGetNumEls(parser) == 0)
    {
        // Null terminate value

        command->value[command->value_length] = '\0';
        return 0;
    }

    // Add each value to response

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        c = parser->value + (i * property->step);

        if(parser->getopt_flags & GET_OPT_HEX)
        {
            // Add hex value of character to value

            length = sprintf(buffer, "0x%02hhX", (uint8_t)*c);

            if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }
            strcpy(&command->value[command->value_length], buffer);
            command->value_length += length;
        }
        else
        {
            // Add character to value

            if(command->value_length + 1 >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }
            command->value[command->value_length++] = *c;
        }

        // Add comma to value

        if(command->value_length + 1 >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }
        command->value[command->value_length++] = ',';
    }

    // Decrement value length so that trailing comma is overwritten by null

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



int32_t SetChar(struct Cmd *command, struct prop *property)
{
    uint32_t        buffer_length   = 0;
    char            c;
    uint32_t        i;
    struct Parser   *parser         = command->parser;
    char            previous_char   = '\0';

    // Check that number of pad elements is zero

    if(parser->num_set_pads)
    {
        command->error = FGC_BAD_ARRAY_IDX;
        return 1;
    }

    while((command->index < command->value_length) &&
          (c = command->value[command->index++]))
    {
        // Check that character is not a disallowed character

        switch(c)
        {
            case ' ':
            case '$':
            case ';':
                command->error = FGC_BAD_PARAMETER;
                return 1;

            case ',':
                // Return an error if there are multiple commas in succession

                if(previous_char == ',')
                {
                    command->error = FGC_NO_SYMBOL;
                    return 1;
                }
                else // Single comma
                {
                    // Ignore comma

                    previous_char = c;
                    continue;
                }
        }

        // Check whether the maximum number of elements for set has been exceeded or the set buffer is full

        if(buffer_length >= parser->max_set_elements ||
           buffer_length >= PARSER_BUF_LEN)
        {
            command->error = FGC_SET_BUF_FULL;
            return 1;
        }

        // Copy the character into the buffer

        parser->set_buffer[buffer_length++] = c;
        previous_char = c;
    }

    // Copy the buffer to the property's value

    for(i = 0 ; i < buffer_length ; i++)
    {
        *(parser->value + (i + parser->set_index) * property->step) = parser->set_buffer[i];
    }

    // Set the number of elements in the property

    if(!((parser->set_index + buffer_length) < parserGetNumEls(parser) &&
         ((property->flags & PF_DONT_SHRINK) ||
          (parser->num_array_specifiers == 2 && buffer_length == parser->max_set_elements))
      ))
    {
        parserSetNumEls(parser, parser->set_index + buffer_length);
    }

    return 0;
}



int32_t GetConfigSet(struct Cmd *command, struct prop *property)
{
    // Override parser number of elements to use maximum number of elements for this property
    // This should be the number of top-level properties

    parserSetNumElsPtr(command->parser, &property->max_elements);

    // Get set configuration properties

    return parserGetParent(command, property, PARSER_CONFIG_SET);
}



int32_t GetConfigUnset(struct Cmd *command, struct prop *property)
{
    // Override parser number of elements to use maximum number of elements for this property
    // This should be the number of top-level properties

    parserSetNumElsPtr(command->parser, &property->max_elements);

    // Get unset configuration properties

    return parserGetParent(command, property, PARSER_CONFIG_UNSET);
}



int32_t SetConfig(struct Cmd *command, struct prop *property)
{
    // SetConfig is used to prepare a FGC to erase its configuration in the case where it has become corrupted,
    // or to cancel the erase request. It is not implemented for FGCD devices.

    command->error = FGC_NOT_IMPL;
    return 1;
}


int32_t GetElapsedTime(struct Cmd *command, struct prop *property)
{
    char            buffer[32];
    const char     *format;
    uint32_t        i;
    uint32_t        length;
    struct Parser  *parser = command->parser;
    struct timeval  time;
    uint32_t       *value;

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        command->error = FGC_NOT_IMPL;
        return 1;
    }

    // Get current FGCD iteration time

    timingGetUserTime(0, &time);

    // Set value format

    format = parser->getopt_flags & GET_OPT_HEX ? "0x%lX," : "%ld,";

    // Add each element to value

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        value   = reinterpret_cast<uint32_t*>(parser->value + (i * property->step));
        length  = sprintf(buffer, format, time.tv_sec - (property->flags & PF_NET_ORDER ? ntohl(*value) : *value));

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }
        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Decrement value length to overwrite trailing comma

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



int32_t GetFloat(struct Cmd *command, struct prop *property)
{
    char            buffer[32];
    float          *data;
    uint32_t        i;
    uint32_t        length;
    struct Parser  *parser = command->parser;
    union
    {
        float       f;
        uint32_t    l;
    }               value;
    int32_t         zero_flag; // Flag to indicate to zero data after get

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    // Set zero flag

    zero_flag = (property->flags        & PF_GET_ZERO) &&
                (parser->getopt_flags   & GET_OPT_ZERO);

    // Prepare data pointer

    data = reinterpret_cast<float*>(parser->value + (parser->from * property->step));

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        value.f = *data;

        // Check whether property is in network byte order

        if(property->flags & PF_NET_ORDER)
        {
            value.l = ntohl(value.l);
        }

        if(zero_flag) *data = 0;

        data += (property->step / prop_type_size[property->type]);

        if(parser->getopt_flags & GET_OPT_HEX)
        {
            length = sprintf(buffer, "0x%08X,", value.l);
        }
        else
        {
            length = sprintf(buffer, "%.7E,",   value.f);
        }

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }
        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Decrement value length so that trailing comma is overwritten by null

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



int32_t SetFloat(struct Cmd *command, struct prop *property)
{
    float          *buffer;
    uint32_t        buffer_length   = 0;
    float          *data;
    char            delimiter;
    uint32_t        i;
    int32_t         limits_flag     = 0;
    float           max             = 0;
    float           min             = 0;
    struct Parser  *parser          = command->parser;
    uint32_t        size            = prop_type_size[property->type];
    union
    {
        float       f;
        uint32_t    l;
    }               value;
    double          value_d;

    buffer  = reinterpret_cast<float*>(&parser->set_buffer) + parser->num_set_pads;
    data    = reinterpret_cast<float*>(parser->value + (parser->from * property->step));

    // Loop for each parameter

    do
    {
        // Get default value

        value.f = buffer_length < parserGetNumEls(parser) ? *data : 0;

        // Check whether property is in network byte order

        if(property->flags & PF_NET_ORDER)
        {
            value.l = ntohl(value.l);
        }

        data += property->step / size;

        if((limits_flag = property->flags & PF_LIMITS))
        {
            min = (static_cast<const struct float_limits*>(property->range))->min;
            max = (static_cast<const struct float_limits*>(property->range))->max;
        }

        // Scan float from value

        parserScanFloat(parser, command->value,
                        &command->index, command->value_length,
                        ",", 1, &delimiter, limits_flag, limits_flag, min, max,
                        &value_d);

        switch(command->error)
        {
            case 0: // Float was successfully read from command
                value.f = value_d;
                break;

            case FGC_NO_SYMBOL: // No symbol, take default value

                // Take default value if delimiter is comma or this is not first element

                if(delimiter == ',' || buffer_length)
                {
                    command->error = 0;
                }
                break;

            default: // An error has occurred
                return 1;
        }

        if(!command->error)
        {
            // Check whether the maximum number of elements for set has been exceeded or the set buffer is full

            if(buffer_length == parser->max_set_elements)
            {
                command->error = FGC_BAD_ARRAY_LEN;
                return 1;
            }
            else if (buffer_length * size >= PARSER_BUF_LEN)
            {
                command->error = FGC_SET_BUF_FULL;
                return 1;
            }

            // Check whether property is in network byte order

            if(property->flags & PF_NET_ORDER)
            {
                value.l = htonl(value.l);
            }

            // Store value in buffer

            *(buffer++) = value.f;
            buffer_length++;
        }
        else if(command->error == FGC_NO_SYMBOL)
        {
            command->error = 0;
        }
    } while(!command->error && delimiter == ',');

    // Check that the buffer will not overrun the maximum length of the property

    if(parser->set_index + buffer_length > property->max_elements)
    {
        command->error = FGC_BAD_ARRAY_LEN;
        return 1;
    }

    // Copy the buffer to the property's value

    for(i = 0 ; i < (buffer_length + parser->num_set_pads) ; i++)
    {
        *reinterpret_cast<float*>(parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<float*>(parser->set_buffer + (i * size));
    }

    // Set the number of elements in the property

    if(!((parser->set_index + parser->num_set_pads + buffer_length) < parserGetNumEls(parser) &&
         ((property->flags & PF_DONT_SHRINK) ||
          (parser->num_array_specifiers == 2 &&
           buffer_length == parser->max_set_elements))
      ))
    {
        parserSetNumEls(parser, parser->set_index + parser->num_set_pads + buffer_length);
    }

    return 0;
}



int32_t GetInteger(struct Cmd *command, struct prop *property)
{
    int32_t               bit_mask_flag;                                      // Flag to indicate that this is a bit mask
    char                  buffer[32];
    const char           *format;                                             // Pointer to format string
    uint32_t              i;
    uint32_t              length;
    int8_t               *p8              = NULL;                             // Pointer to 8 bit data
    int16_t              *p16             = NULL;                             // Pointer to 16 bit data
    int32_t              *p32             = NULL;                             // Pointer to 32 bit data
    struct Parser        *parser          = command->parser;
    uint32_t              size            = prop_type_size[property->type];   // Element size
    uint32_t              size_index;                                         // Size index
    const struct sym_lst *sym_list        = NULL;                             // Pointer to symbol list
    int32_t               sym_list_flag;                                      // Flag to indicate that this is a symbol list
    uint32_t              type            = 0;                                // Type index
    uint8_t              *up8             = NULL;                             // Pointer to 8 bit data
    uint16_t             *up16            = NULL;                             // Pointer to 16 bit data
    uint32_t             *up32            = NULL;                             // Pointer to 32 bit data
    uint8_t               uv8             = 0;                                // unsigned 8 bit value
    uint16_t              uv16            = 0;                                // unsigned 16 bit value
    uint32_t              uv32            = 0;                                // unsigned 32 bit value
    int8_t                v8              = 0;                                // 8 bit value
    int16_t               v16             = 0;                                // 16 bit value
    int32_t               v32             = 0;                                // 32 bit value
    int32_t               zero_flag;                                          // Flag to indicate to zero data after get

    const char *hex_format[] = { "0x%02X,", "0x%04hX,", "0x%08X," };
    const char *dec_format[] = { "%u,", "%d,", "%hu,", "%hd,", "%u,", "%d," };

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    // Set sym_list flag

    sym_list_flag = (property->flags & PF_SYM_LST) && !(parser->getopt_flags & GET_OPT_HEX);

    // Set zero flag

    zero_flag = (property->flags & PF_GET_ZERO) && (parser->getopt_flags & GET_OPT_ZERO);

    // Set bit mask flag

    bit_mask_flag = (property->flags & PF_BIT_MASK) && sym_list_flag;

    // Set integer type index and prepare data pointer

    switch(property->type)
    {
        case PT_INT8U:
            type    = 0;
            up8     = reinterpret_cast<uint8_t*>(parser->value + (parser->from * property->step));
            break;

        case PT_INT8S:
            type    = 1;
            p8      = reinterpret_cast<int8_t*>(parser->value + (parser->from * property->step));
            break;

        case PT_INT16U:
            type    = 2;
            up16    = reinterpret_cast<uint16_t*>(parser->value + (parser->from * property->step));
            break;

        case PT_INT16S:
            type    = 3;
            p16     = reinterpret_cast<int16_t*>(parser->value + (parser->from * property->step));
            break;

        case PT_INT32U:
            type    = 4;
            up32    = reinterpret_cast<uint32_t*>(parser->value + (parser->from * property->step));
            break;

        case PT_INT32S:
            type    = 5;
            p32     = reinterpret_cast<int32_t*>(parser->value + (parser->from * property->step));
            break;
    }

    // Set integer size index

    size_index = type / 2;

    // Check whether property uses a symbol list

    if(sym_list_flag)
    {
        sym_list = static_cast<const sym_lst*>(property->range);
        format   = "%s,";
    }
    else if(parser->getopt_flags & GET_OPT_HEX)
    {
        format = hex_format[size_index];
    }
    else
    {
        format = dec_format[type];
    }

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        switch(property->type)
        {
            case PT_INT8U:  // 8 bit unsigned
                uv8 = *up8;

                if(zero_flag) *up8 = 0;

                up8 += (property->step / size);

                break;

            case PT_INT8S:  // 8 bit signed
                v8 = *p8;

                if(zero_flag) *p8 = 0;

                p8 += (property->step / size);

                break;

            case PT_INT16U: // 16 bit unsigned
                uv16 = property->flags & PF_NET_ORDER ? ntohs(*up16) : *up16;

                if(zero_flag) *up16 = 0;

                up16 += (property->step / size);
                break;

            case PT_INT16S: // 16 bit signed
                v16 = property->flags & PF_NET_ORDER ? ntohs(*p16) : *p16;

                if(zero_flag) *p16 = 0;

                p16 += (property->step / size);
                break;

            case PT_INT32U: // 32 bit unsigned
                uv32 = property->flags & PF_NET_ORDER ? ntohl(*up32) : *up32;

                if(zero_flag) *up32 = 0;

                up32 += (property->step / size);

                break;

            case PT_INT32S: // 32 bit signed
                v32 = property->flags & PF_NET_ORDER ? ntohl(*p32) : *p32;

                if(zero_flag) *p32 = 0;

                p32 += (property->step / size);

                break;
        }

        // Check whether this is a symbol list

        if(sym_list_flag)
        {
            switch(property->type)
            {
                case PT_INT8U:  uv32 = uv8;     break;
                case PT_INT8S:  uv32 = v8;      break;
                case PT_INT16U: uv32 = uv16;    break;
                case PT_INT16S: uv32 = v16;     break;
                case PT_INT32S: uv32 = v32;     break;
            }

            if(bit_mask_flag)
            {
                // Add bit mask to value

                if(propPrintBitMask(command, const_cast<sym_lst*>(sym_list), uv32))
                {
                    return 1;
                }
            }
            else // Property is not a bit mask
            {
                // Add symbol to value

                length = sprintf(buffer, format, propGetSymString(parser, const_cast<sym_lst*>(sym_list), uv32));

                if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
                {
                    command->error = FGC_RSP_BUF_FULL;
                    return 1;
                }
                strcpy(&command->value[command->value_length], buffer);
                command->value_length += length;
            }
        }
        else // Plain integer data
        {
            switch(property->type)
            {
                case PT_INT8U:  length = sprintf(buffer, format, uv8);  break;
                case PT_INT8S:  length = sprintf(buffer, format, v8);   break;
                case PT_INT16U: length = sprintf(buffer, format, uv16); break;
                case PT_INT16S: length = sprintf(buffer, format, v16);  break;
                case PT_INT32U: length = sprintf(buffer, format, uv32); break;
                case PT_INT32S: length = sprintf(buffer, format, v32);  break;
                default:        length = 0; break;
            }

            if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }
            strcpy(&command->value[command->value_length], buffer);
            command->value_length += length;
        }
    }

    // Decrement value length so that trailing comma is overwritten by null

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



int32_t SetInteger(struct Cmd *command, struct prop *property)
{
    uint32_t              buffer_length   = 0;
    char                  delimiter;
    uint32_t              i;
    int32_t               limits_flag     = 0;
    long                  max             = 0;
    long                  min             = 0;
    struct Parser        *parser          = command->parser;
    int32_t               signed_flag     = 0;
    uint32_t              size            = prop_type_size[property->type];
    uint32_t              sym_index;
    const struct sym_lst *sym_list;
    int32_t               sym_list_flag   = 0;
    int32_t               value;

    union
    {
        int8_t      *i8s;
        uint8_t     *i8u;
        int16_t     *i16s;
        uint16_t    *i16u;
        int32_t     *i32s;
        uint32_t    *i32u;
    } buffer, data;

    buffer.i8s  = reinterpret_cast<signed char*>(&parser->set_buffer) + (parser->num_set_pads * size);
    data.i8s    = reinterpret_cast<signed char*>(parser->value)       + (parser->from * property->step);

    // Check whether property is a symbol list

    sym_list_flag = (property->flags & PF_SYM_LST);

    // Check whether property is a signed type

    if(property->type == PT_INT8S  ||
       property->type == PT_INT16S ||
       property->type == PT_INT32S)
    {
        signed_flag = 1;
    }

    // Loop for each parameter

    do
    {
        // Set default value

        switch(property->type)
        {
            case PT_INT8S:
                value       = buffer_length < parserGetNumEls(parser) ? *data.i8s : 0;
                data.i8s   += (property->step / size);
                break;

            case PT_INT8U:
                value       = buffer_length < parserGetNumEls(parser) ? *data.i8u : 0;
                data.i8u   += (property->step / size);
                break;

            case PT_INT16S:
                value       = buffer_length < parserGetNumEls(parser) ? property->flags & PF_NET_ORDER ? ntohs(*data.i16s) : *data.i16s : 0;
                data.i16s  += (property->step / size);
                break;

            case PT_INT16U:
                value       = buffer_length < parserGetNumEls(parser) ? property->flags & PF_NET_ORDER ? ntohs(*data.i16u) : *data.i16u : 0;
                data.i16u  += (property->step / size);
                break;

            case PT_INT32S:
                value       = buffer_length < parserGetNumEls(parser) ? property->flags & PF_NET_ORDER ? ntohl(*data.i32s) : *data.i32s : 0;
                data.i32s  += (property->step / size);
                break;

            case PT_INT32U:
                value       = buffer_length < parserGetNumEls(parser) ? property->flags & PF_NET_ORDER ? ntohl(*data.i32u) : *data.i32u : 0;
                data.i32u  += (property->step / size);
                break;
        }

        // Check whether property has a symbol list

        if(!sym_list_flag) // Property does not have a symbol list
        {
            if((limits_flag = property->flags & PF_LIMITS))
            {
                min = (static_cast<const struct int_limits*>(property->range))->min;
                max = (static_cast<const struct int_limits*>(property->range))->max;
            }

            // Scan integer from value

            parserScanInt(parser, command->value,
                          &command->index, command->value_length,
                          const_cast<char*>(","), 1, &delimiter, signed_flag, limits_flag, limits_flag, min, max,
                          &value);

            switch(command->error)
            {
                case 0:
                    break;

                case FGC_NO_SYMBOL:

                    // Take default value if delimiter is comma or this is not first element

                    if(delimiter == ',' || buffer_length)
                    {
                        command->error = 0;
                    }
                    break;

                default: // An error has occurred
                    return 1;
            }
        }
        else // Property has a symbol list
        {
            sym_list = static_cast<const sym_lst*>(property->range);

            // Scan symbol from value

            if((sym_index = parserIdentifyNextSymbol(parser, command->value,
                                                     &command->index, command->value_length,
                                                     parser->class_data->const_hash, ",", 1, &delimiter)))
            {
                // Scan property's symbol list for a match

                for( ; sym_list->index && sym_list->index != sym_index ; sym_list++);

                // Check whether no match was found

                if(!sym_list->index)
                {
                    // Symbol was not valid, return error

                    command->error = FGC_BAD_PARAMETER;
                    return 1;
                }

                // Set value

                value = signed_flag ? (int32_t)sym_list->value : (uint32_t)sym_list->value;
            }
            else if(command->error) // An error has occurred
            {
                return 1;
            }
        }

        if(!command->error)
        {
            // Check whether the maximum number of elements for set has been exceeded or if the set buffer is full

            if(buffer_length == parser->max_set_elements ||
               ((buffer_length * size) >= PARSER_BUF_LEN))
            {
                command->error = FGC_SET_BUF_FULL;
                return 1;
            }

            // Store value in buffer

            switch(property->type)
            {
                case PT_INT8S:  *(buffer.i8s++)  = (signed char)    value;  break;
                case PT_INT8U:  *(buffer.i8u++)  = (unsigned char)  value;  break;
                case PT_INT16S: *(buffer.i16s++) = property->flags & PF_NET_ORDER ? htons((int16_t )value) : (int16_t )value; break;
                case PT_INT16U: *(buffer.i16u++) = property->flags & PF_NET_ORDER ? htons((uint16_t)value) : (uint16_t)value; break;
                case PT_INT32S: *(buffer.i32s++) = property->flags & PF_NET_ORDER ? htonl((int32_t )value) : (int32_t )value; break;
                case PT_INT32U: *(buffer.i32u++) = property->flags & PF_NET_ORDER ? htonl((uint32_t)value) : (uint32_t)value; break;
            }
            buffer_length++;
        }
        else if(command->error == FGC_NO_SYMBOL)
        {
            command->error = 0;
        }
    } while(!command->error && delimiter == ',');

    // Check that the buffer will not overrun the maximum length of the property

    if(parser->set_index + buffer_length > property->max_elements)
    {
        command->error = FGC_BAD_ARRAY_LEN;
        return 1;
    }

    // Copy the buffer to the property's value

    for(i = 0 ; i < (buffer_length + parser->num_set_pads) ; i++)
    {
        switch(property->type)
        {
            case PT_INT8S:
                *reinterpret_cast<int8_t*>  (parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<int8_t*>  (parser->set_buffer + (i * size));
                break;

            case PT_INT8U:
                *reinterpret_cast<uint8_t*> (parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<uint8_t*> (parser->set_buffer + (i * size));
                break;

            case PT_INT16S:
                *reinterpret_cast<int16_t*> (parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<int16_t*> (parser->set_buffer + (i * size));
                break;

            case PT_INT16U:
                *reinterpret_cast<uint16_t*>(parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<uint16_t*>(parser->set_buffer + (i * size));
                break;

            case PT_INT32S:
                *reinterpret_cast<int32_t*> (parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<int32_t*> (parser->set_buffer + (i * size));
                break;

            case PT_INT32U:
                *reinterpret_cast<uint32_t*>(parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<uint32_t*>(parser->set_buffer + (i * size));
                break;
        }
    }

    // Set the number of elements in the property

    if(!((parser->set_index + parser->num_set_pads + buffer_length) < parserGetNumEls(parser) &&
         ((property->flags & PF_DONT_SHRINK) ||
          (parser->num_array_specifiers == 2 && buffer_length == parser->max_set_elements))
      ))
    {
        parserSetNumEls(parser, parser->set_index + parser->num_set_pads + buffer_length);
    }

    return 0;
}



int32_t GetPoint(struct Cmd *command, struct prop *property)
{
    union point_t
    {
        struct
        {
            float       x;
            float       y;
        } point;
        struct
        {
            uint32_t    x;
            uint32_t    y;
        } uint32;
    };
    union point_t   value;
    union point_t  *data;
    char            buffer[32];
    uint32_t        i;
    uint32_t        length;
    struct Parser  *parser = command->parser;

    int32_t         zero_flag; // Flag to indicate to zero data after get

    // Arrays of points can not be in big endian as it will conflict with libfg

    if(property->flags & PF_NET_ORDER)
    {
        return FGC_BAD_FLOAT;
    }

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    // Set zero flag

    zero_flag = (property->flags        & PF_GET_ZERO) &&
                (parser->getopt_flags   & GET_OPT_ZERO);

    // Prepare data pointer

    data = reinterpret_cast<union point_t*>(parser->value + (parser->from * property->step));

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        value = *data;

        if(zero_flag) *data = (union point_t){{0}};

        data += (property->step / prop_type_size[property->type]);

        if(parser->getopt_flags & GET_OPT_HEX)
        {
            length = sprintf(buffer, "0x%08X|0x%08X,", value.uint32.x, value.uint32.y);
        }
        else
        {
            length = sprintf(buffer, "%.4f|%.7E,",   value.point.x, value.point.y);
        }

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }
        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Decrement value length so that trailing comma is overwritten by null

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}



int32_t SetPoint(struct Cmd *command, struct prop *property)
{
    struct point_t
    {
        float       x;
        float       y;
    };
    point_t         value;
    point_t        *buffer;
    uint32_t        buffer_length   = 0;
    point_t        *data;
    char            delimiter;
    uint32_t        i;
    struct Parser  *parser          = command->parser;
    uint32_t        size            = prop_type_size[property->type];
    double          value_dx;
    double          value_dy;

    // Arrays of points can not be in big endian as it will conflict with libfg

    if(property->flags & PF_NET_ORDER)
    {
        return FGC_BAD_FLOAT;
    }

    buffer  = reinterpret_cast<struct point_t*>(&parser->set_buffer) + parser->num_set_pads;
    data    = reinterpret_cast<struct point_t*>(parser->value + (parser->from * property->step));

    // Loop for each parameter

    do
    {
        // Get default value

        value = buffer_length < parserGetNumEls(parser) ? *data : (struct point_t){0};

        data += property->step / size;

        // Scan x coordinate

        parserScanPoint(parser, command->value,
                        &command->index, command->value_length,
                        ",", 1, &delimiter, 0, 0, 0, 0,
                        &value_dx,&value_dy);

        switch(command->error)
        {
            case 0: // Float was successfully read from command
                value.x = value_dx;
                value.y = value_dy;
                break;

            case FGC_NO_SYMBOL: // No symbol, take default value

                // Take default value if delimiter is comma or this is not first element

                if(delimiter == ',' || buffer_length)
                {
                    command->error = 0;
                }
                break;

            default: // An error has occurred
                return 1;
        }

        if(!command->error)
        {
            // Check whether the maximum number of elements for set has been exceeded or the set buffer is full

            if(buffer_length == parser->max_set_elements ||
               ((buffer_length * size) >= PARSER_BUF_LEN))
            {
                command->error = FGC_SET_BUF_FULL;
                return 1;
            }

            // Store value in buffer

            *(buffer++) = value;
            buffer_length++;
        }
        else if(command->error == FGC_NO_SYMBOL)
        {
            command->error = 0;
        }
    } while(!command->error && delimiter == ',');

    // Check that the buffer will not overrun the maximum length of the property

    if(parser->set_index + buffer_length > property->max_elements)
    {
        command->error = FGC_BAD_ARRAY_LEN;
        return 1;
    }

    // Copy the buffer to the property's value

    for(i = 0 ; i < (buffer_length + parser->num_set_pads) ; i++)
    {
        *reinterpret_cast<point_t*>(parser->value + ((parser->set_index + i) * property->step)) = *reinterpret_cast<point_t*>(parser->set_buffer + (i * size));
    }

    // Set the number of elements in the property

    if(!((parser->set_index + parser->num_set_pads + buffer_length) < parserGetNumEls(parser) &&
         ((property->flags & PF_DONT_SHRINK) ||
          (parser->num_array_specifiers == 2 &&
           buffer_length == parser->max_set_elements))
      ))
    {
        parserSetNumEls(parser, parser->set_index + parser->num_set_pads + buffer_length);
    }

    return 0;
}

int32_t GetParent(struct Cmd *command, struct prop *property)
{
    return parserGetParent(command, property, PARSER_ALL);
}



int32_t SetRBACToken(struct Cmd *command, struct prop *property)
{
    // Check that client field is defined in command structure

    if(command->client == NULL)
    {
        // Return bad connection type error

        command->error = FGC_BAD_CONN_TYPE;
        return 1;
    }

    // Perform binary set

    if(SetBin(command, property))
    {
        return 1;
    }

    // Create token object for TCP client
    if(fgcd.enable_rbac &&
       rbacSetTCPClientToken(command->client, prop.rbac_token_buf, property->num_elements) != 0)
    {
        command->error = FGC_BAD_RBAC_TOKEN;
        return 1;
    }

    return 0;
}



int32_t SetReadNames(struct Cmd *command, struct prop *property)
{
    command->error = devnameRead();

    if(command->error)
    {
        command->error = FGC_ERROR_READING_FILE;
    }
    return command->error;
}



int32_t SetReadRBAC(struct Cmd *command, struct prop *property)
{
    command->error = rbacReadAccessMap();

    if(command->error)
    {
        command->error = FGC_ERROR_READING_FILE;
    }

    return command->error;
}

int32_t SetRterm(struct Cmd *command, struct prop *property)
{
    struct TCP_client  *client = command->client;
    char                delimiter;
    int32_t             device_number;
    struct Rterm_resp  *rterm_resp;

    // Check that client field is defined in command structure

    if(client == NULL)
    {
        // Return bad connection type error

        command->error = FGC_BAD_CONN_TYPE;
        return 1;
    }

    // Do nothing if client already has an rterm

    if(client->rterm_device != NULL)
    {
        return 1;
    }

    // Get device number from value

    if(isdigit(command->value[command->index]))
    {
        parserScanInt(command->parser, command->value,
                      &command->index, command->value_length,
                      const_cast<char*>(""), 1, &delimiter, 0, 1, 1, 1, FGCD_MAX_DEVS - 1,
                      &device_number);
    }
    else
    {
        command->error = FGC_UNKNOWN_DEV;
    }

    if(command->error)
    {
        return 1;
    }

    // Start remote terminal for client

    client->rterm_locked    = 0;
    client->rterm_device    = &fgcd.device[device_number];
    client->mode            = TCP_MODE_RTERM;

    // Change client terminal settings for remote terminal
    // Disable telnet client line-mode and local echo

    if((rterm_resp = static_cast<struct Rterm_resp*>(queueBlockPop(&cmdqmgr.free_rterm_q))) != NULL)
    {
        rterm_resp->buffer_length = sprintf(rterm_resp->buffer,
                                            "$%s .\n\n;\xFF\xFD\x22\xFF\xFB\x01",
                                            command->tag);

        pthread_mutex_lock(&client->mutex);
        client->num_rterm_resps++;
        pthread_mutex_unlock(&client->mutex);

        queuePush(&client->response_queue, rterm_resp);
        rterm_resp = NULL;
    }

    logPrintf(client->rterm_device->id, "TCP %d (%s@%s) rterm\n",
              command->client->id, client->username, client->hostname);

    return 0;
}



int32_t SetRtermLock(struct Cmd *command, struct prop *property)
{
    unsigned char return_val;

    if(!(return_val = SetRterm(command, property))) // Rterm set okay
    {
        command->client->rterm_locked = 1;
    }

    return return_val;
}



int32_t GetString(struct Cmd *command, struct prop *property)
{
    char            *c;
    uint32_t        char_index;
    struct Parser   *parser = command->parser;
    uint32_t        string_index;

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    // Return empty value if property has no elements

    if(parserGetNumEls(parser) == 0)
    {
        // Null terminate value

        command->value[command->value_length] = '\0';
        return 0;
    }

    // Add each value to response

    for(string_index  = parser->from    ;
        string_index <= parser->to      ;
        string_index += parser->step)
    {
        c = *reinterpret_cast<char**>(parser->value + (string_index * property->step));

        // Add string to value

        for(char_index = 0 ; c != NULL && *c != '\0' ; char_index++)
        {
            if(command->value_length >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }

            command->value[command->value_length++] = *(c++);
        }
        command->value[command->value_length++] = ',';
    }

    // Decrement value length so that trailing comma is overwritten by null

    if(string_index != parser->from) // Check that at least one element has been returned
    {
        command->value_length--;
    }

    // Null terminate value

    command->value[command->value_length] = '\0';
    return 0;
}



int32_t SetSubPeriod(struct Cmd *command, struct prop *property)
{
    struct TCP_client   *client = command->client;
    int32_t             previous_value;

    // Check that client field is defined in command structure

    if(client == NULL)
    {
        // Return bad connection type error

        command->error = FGC_BAD_CONN_TYPE;
        return 1;
    }

    previous_value              = client->udp_sub_period;
    client->udp_sub_period      = 0;
    client->udp_sub_period_tick = 0;

    // Perform integer set

    if(SetInteger(command, property))
    {
        client->udp_sub_period = previous_value;
        return 1;
    }

    // Check whether a negative value was specified for an aligned subscription

    if(client->udp_sub_period < 0)
    {
        if(abs(client->udp_sub_period) > FGCD_CYCLES_PER_SEC)
        {
            // Check that period is a multiple of seconds

            if((FGCD_CYCLE_PERIOD_MS * abs(client->udp_sub_period)) % 1000)
            {
                client->udp_sub_period  = previous_value;
                command->error          = FGC_OUT_OF_LIMITS;
                return 1;
            }
        }
        else
        {
            // Check that period is a factor of 1 second

            if(1000. / (FGCD_CYCLE_PERIOD_MS * abs(client->udp_sub_period)) !=
               1000  / (FGCD_CYCLE_PERIOD_MS * abs(client->udp_sub_period)))
            {
                client->udp_sub_period  = previous_value;
                command->error          = FGC_OUT_OF_LIMITS;
                return 1;
            }
        }
    }
    return 0;
}



int32_t GetUnixTime(struct Cmd *command, struct prop *property)
{
    char            buffer[32];
    const char     *format;
    uint32_t        i;
    uint32_t        length;
    struct Parser  *parser = command->parser;
    struct tm       time;
    struct timeval  value;

    // Check whether BIN get option is set

    if(parser->getopt_flags & GET_OPT_BIN) // BIN get option is set
    {
        // Execute binary get function

        return GetBin(command, property);
    }

    format = parser->getopt_flags & GET_OPT_HEX ?  "0x%llX," : "%02d/%02d/%04d %02d:%02d:%02d,";

    // Format each value and add to response

    for(i = parser->from ; i <= parser->to ; i += parser->step)
    {
        value = *reinterpret_cast<struct timeval*>(parser->value + (i * property->step));

        if(parser->getopt_flags & GET_OPT_HEX) // Return value as hex
        {
            length = sprintf(buffer, format, (long long)value.tv_sec);
        }
        else // Return value as date string
        {
            localtime_r(&value.tv_sec, &time);
            length = sprintf(buffer,
                             format,
                             time.tm_mday,
                             time.tm_mon  + 1,
                             time.tm_year + 1900,
                             time.tm_hour,
                             time.tm_min,
                             time.tm_sec);
        }

        if(command->value_length + length >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }
        strcpy(&command->value[command->value_length], buffer);
        command->value_length += length;
    }

    // Decrement value length to overwrite trailing comma

    command->value_length--;

    // Null terminate value

    command->value[command->value_length] = '\0';

    return 0;
}

int32_t GetTimingUsers(struct Cmd *command, struct prop *property)
{
    char           *buffer = command->value;
    struct timeval  cutoff_time;
    uint32_t        len    = 0;

    // Create the timeval for now minus 10 minutes

    timingReadTime(&cutoff_time);
    cutoff_time.tv_sec -= 10 * 60;

    // Include user 0 (ALL)

    len = snprintf(buffer,CMD_MAX_VAL_LENGTH,"%s.USER.ALL|0|ENABLED", timing.domain_str);

    // Include the remaining users

    for(uint32_t user = 1; user <= FGC_TIMING_NUM_USERS && len < CMD_MAX_VAL_LENGTH; ++user)
    {
        const char * const user_name = timingUserName(user);

        // Check whether the user is defined

        if(user_name[0] != '\0')
        {
            struct timeval last_user_time;
            timingGetLastUserTime(user, &last_user_time);

            int32_t l = snprintf(buffer + len,
                                 CMD_MAX_VAL_LENGTH - len,
                                 ",%s.USER.%s|%u|%s",
                                 timing.domain_str,
                                 user_name,
                                 user,
                                 timevalIsLessThan(&last_user_time, &cutoff_time) ? "DISABLED" : "ENABLED");

            if(l > 0)
            {
                // sprintf successful - update length of string in buffer

                len += l;
            }
        }
    }

    *(buffer + len) = '\0';

    command->value_length = len;

    return 0;
}

int32_t propPrintBitMask(struct Cmd *command, struct sym_lst *sym_list, uint32_t value)
{
    uint32_t        bit_mask;
    char            buffer[32];
    int32_t         delim_flag  = 0;
    uint32_t        length;
    struct Parser   *parser     = command->parser;

    // Loop through symbols in list

    for( ; sym_list->index ; sym_list++)
    {
        // Get mask constant for symbol

        bit_mask = sym_list->value;

        // Check whether symbol is set in mask

        if((value & bit_mask) == bit_mask)
        {
            // Add symbol to value

            length = sprintf(buffer, "%s", parser->class_data->sym_tab_const[sym_list->index & ~PROP_SYM_NOT_SETTABLE].key.c);

            if(command->value_length + delim_flag + length >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }

            // Add delimiter to value if required

            if(delim_flag)
            {
                command->value[command->value_length++] = ' ';
            }
            else
            {
                delim_flag = 1;
            }

            // Copy buffer to value

            strcpy(&command->value[command->value_length], buffer);
            command->value_length += length;
        }
    }
    command->value[command->value_length++] = ',';

    return 0;
}



const char *propGetSymString(struct Parser *parser, struct sym_lst *sym_list, int32_t value)
{
    // Loop through symbols in list

    for( ; sym_list->index ; sym_list++)
    {
        // Check whether symbol constant has been matched

        if(value == sym_list->value)
        {
            // Return pointer to symbol string

            return parser->class_data->sym_tab_const[sym_list->index & ~PROP_SYM_NOT_SETTABLE].key.c;
        }
    }

    // No match was found

    return "!invalid.";
}

// EOF
