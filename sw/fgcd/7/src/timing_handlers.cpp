/*!
 * @file   timing_handlers.cpp
 * @author Jose M. de Paco
 *
 * Functions for handling timing events
 */

// __STDC_LIMIT_MACROS must be defined in C++ to make stdint.h define macros such as UINT32_MAX

#define __STDC_LIMIT_MACROS

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <logging.h>
#include <fgcd.h>
#include <equipdev.h>
#include <fgcddev.h>
#include <cmwpub.h>
#include <pub.h>

#include <timing.h>
#include <timing_handlers.h>

#include <log.h>
#include <equip_common_93.h>



// Global variables

bool timing_pulse_on[FGCD_MAX_DEVS];
uint32_t timing_pulse_user;
uint32_t timing_pulse_timer[FGCD_MAX_DEVS];
uint32_t timing_new_pulse_timer[FGCD_MAX_DEVS];
float    timing_cycling_reference[FGCD_MAX_DEVS];

uint32_t timing_extraction_timer;
bool     timing_extraction_event;



void timingAddMultiPpmEvent(const char *mppm_condition)
{
    ; // Multi-PPM not supported in this class
}


// Internal functions declarations

/*
 * Handle cycling devices for each device
 */
static void timingProcessCyclingDevices();

/*
 * Handle the received cycling event
 */
static void timingHandleCyclingEvent(uint8_t channel, float time_to_event_ms);

/*
 * Signals logging library of start of a cycle
 */
static void timingStartCycle(struct CC_us_time * iter_time);
/*
 * Calculate cycle timing
 */
static void timingCalcCycle(tTimingTime evt_time, struct CC_us_time * iter_time, bool * is_200ms_boundary, bool * is_1s_boundary);

/*
 * Log timing event
 */
static void timingLogEventTiming(const char *event_name, const uint32_t event_delay);

/*
 * Log acquisition for cycling devices
 */
static void timingLogAcquisitionCycling(uint32_t channel);


// External functions declarations

void timingMillisecond(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    struct timeval  time;

    uint32_t ms = evt_time.time.tv_nsec / 1000000;

    // Set time

    time.tv_sec  = evt_time.time.tv_sec;
    time.tv_usec = ms * 1000;

    // Calculate cycle timing

    struct CC_us_time iter_time;
    bool is_200ms_boundary;
    bool is_1s_boundary;

    timingCalcCycle(evt_time, &iter_time, &is_200ms_boundary, &is_1s_boundary);

    // Check timing status and set fault if necessary

    timingCheckStatus();

    // Increment millisecond within cycle if user is set

    if(timing.user)
    {
        timing.cycle_ms++;
    }

    // Actions to be done every 200 ms

    if(ms % FGCD_CYCLE_PERIOD_MS == 0)
    {
        // Timestamp published data

        pub.published_data.time_sec  = htonl(time.tv_sec);
        pub.published_data.time_usec = htonl(time.tv_usec);

        // Update published data for FGCD and equipment devices

        fgcddevPublish();
        equipdevPublish();
        pubTrigger();

        // log signals and check status of post mortem buffers

        uint8_t channel;

        for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
        {
            if(fgcd.device[channel].name != NULL)
            {
               // Update Log

               logSignals(channel, &iter_time);

               // Update Event log

               ccEvtlogStoreProperties(channel);

               // removed post-mortem pmBufferFreeze(channel);
            }
        }
    }

    // Process CYCLING devices

    timingProcessCyclingDevices();
}

void timingTelegramReady(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    // CYCLE_NB (aka CYCLENUM)

    try
    {
        evt_value->getFieldValue("CYCLE_NB", &value);
        timing.field_value[TIMING_FIELD_CYCLE_NUM] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field CYCLE_NB (aka CYCLENUM) = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_CYCLE_NUM]);
        }

        // Set cycle number

        timing.cycle_num = timing.field_value[TIMING_FIELD_CYCLE_NUM] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }


    // CMW notifications

    uint8_t channel;

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {

        // Skip device if not configured

        if(!equipdev.device[channel].fgcd_device->name)
        {
            continue;
        }

        /*
         *  Notify measurement and reference properties
         *  Working sets refresh every basic period (1.2 s)
         */

        cmwpubNotify(channel, "MEAS",                0, 0, 0, 0, 0);
        cmwpubNotify(channel, "MEAS.I",              0, 0, 0, 0, 0);
        cmwpubNotify(channel, "MEAS.I.VALUE",        0, 0, 0, 0, 0);
        cmwpubNotify(channel, "MEAS.V",              0, 0, 0, 0, 0);
        cmwpubNotify(channel, "MEAS.V.VALUE",        0, 0, 0, 0, 0);
        cmwpubNotify(channel, "MEAS.DIRECT.V.VALUE", 0, 0, 0, 1, 0);
        cmwpubNotify(channel, "MEAS.REF",            0, 0, 0, 0, 0);

        // Notify REF.DIRECT.V.VALUE (requested by Medicis OP)
        cmwpubNotify(channel, "REF.DIRECT.V",       0, 0, 0, 0, 0);
        cmwpubNotify(channel, "REF.DIRECT.V.VALUE", 0, 0, 0, 0, 0);

    }


}



void timingInjectionWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    try
    {
        // Get the USER in which the injection will happen

        evt_value->getFieldValue("USER", &value);

        uint32_t user = timingUserNumber(value.getAsString().c_str());
        if(!user) return;

        timing_pulse_user = user;


        if(timing.log_events_flag)
        {
            logPrintf(0, "TIMING Injection warning (delay %d ms) , user %s (%u)\n", info.delay_ms, value.getAsString().c_str(), timing_pulse_user);
        }

        // Get whether there is beam

        static Timing::Value cycle_without_beam;
        evt_value->getFieldValue("CYCLE_WITHOUT_BEAM", &cycle_without_beam);

        // For ELENA, check that the injection mode is the ion source (it should not ramp up if the injection mode is AD)

        static Timing::Value injection_mode;
        evt_value->getFieldValue("INJECTION_MODE", &injection_mode);

        // Process the cycling pulse

        uint8_t channel;

        for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            bool economy_conditions =  device.economy_mode == FGC_CTRL_DISABLED ||
                                      (device.economy_mode == FGC_CTRL_ENABLED &&
                                      cycle_without_beam.getAsBool() == false &&                                                        // beam present
                                      (!strcmp(timing.domain_str, "LNA") && !strcmp(injection_mode.getAsString().c_str(), "LSOURCE"))); // injection mode is source for ELENA

            if(device.status.state_pc == FGC_PC_CYCLING && device.ref.event_cyc == FGC_EVENT_CYC_INJECTION && economy_conditions)
            {
                // The device is in CYCLING, listening to the InjectionWarning event and there is beam (or economy disabled).
                // If machine is LNA, the injection mode is the source

                timingHandleCyclingEvent(channel, info.delay_ms);
            }

            else // publish measurements anyway (MEAS is cycle bounded)
            {
                cmwpubNotify(channel, "MEAS.PULSE",       timing.user, 0, 0, 0, 0);
                cmwpubNotify(channel, "MEAS.PULSE.VALUE", timing.user, 0, 0, 0, 0);
            }
        }

        // Log timing event only for CYCLING devices

        timingLogEventTiming("INJECTWARNING", info.delay_ms);

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }
}



void timingExtractionWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    try
    {
        static Timing::Value value;
        evt_value->getFieldValue("USER", &value);

        if(timing.log_events_flag)
        {
            logPrintf(0, "TIMING Extraction warning (delay %d ms) , user %s \n", info.delay_ms, value.getAsString().c_str());
        }

        // Start timer to extraction event

        timing_extraction_timer = info.delay_ms;
        timing_extraction_event = true;

        // Log timing event only for CYCLING devices

        timingLogEventTiming("EXTRACTWARNING", info.delay_ms);
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }
}



void timingCycleWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    // Start the countdown to the start of the cycle

    timing.next_cycle_countdown_ms  = info.delay_ms;

    // USER

    try
    {
        evt_value->getFieldValue("USER", &value);
        uint32_t next_user = timingUserNumber(value.getAsString().c_str());

        if(!next_user) return;

        timing.next_user = next_user;

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field USER = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.next_user);
        }

        // Log timing event only for CYCLING devices

        timingLogEventTiming("STARTCYCLE", info.delay_ms);
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }
}



// Internal functions definitions

static void timingProcessCyclingDevices()
{
    uint8_t channel;

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        if(device.status.state_pc == FGC_PC_CYCLING)
        {
            if (timing_pulse_on[channel] == true)
            {
                // Decrease the timers

                if (timing_pulse_timer[channel] != 0)
                {
                    timing_pulse_timer[channel]--;
                }

                if (timing_new_pulse_timer[channel] != 0)
                {
                    timing_new_pulse_timer[channel]--;
                }

                // If the current pulse timer has finished and:
                // 1. No new pulse is armed: reset the pulse
                // 2. New pulse is armed: transfer the new pulse timer to the current pulse timer and set the voltage


                if (timing_pulse_timer[channel] == 0)
                {
                    if(timing_new_pulse_timer[channel] == 0)  // 1
                    {
                        timing_pulse_on[channel] = false;
                    }
                    else                                      // 2
                    {
                        timing_pulse_timer    [channel] = timing_new_pulse_timer[channel];
                        timing_new_pulse_timer[channel] = 0;

                        // Set the reference of the next pulse

                        if (device.ppm_enabled == FGC_CTRL_ENABLED)
                        {
                            device.ppm[0].cycling_v_ref = device.ppm[timing_pulse_user].pulse_v_ref;  // PPM device
                        }
                        else
                        {
                            device.ppm[0].cycling_v_ref = device.ppm[0].pulse_v_ref;                  // Not PPM device
                        }
                    }


                }

                // Event time (pulse time after the warning delay passed): notify and log acquisitions

                if ((  device.ppm_enabled == FGC_CTRL_ENABLED   && timing_pulse_timer[channel] == device.ppm[timing_pulse_user].pulse_duration * 1000)
                   || (device.ppm_enabled == FGC_CTRL_DISABLED  && timing_pulse_timer[channel] == device.ppm[0                ].pulse_duration * 1000))
                {
                    /*
                     *  Notify measurement and reference properties
                     *  Working sets CMW subscription in event REF.EVENT_CYCLE for cycling devices
                     */

                    device.ppm[timing_pulse_user].acq_value = device.meas.v_meas;

                    cmwpubNotify(channel, "MEAS",             timing.user, 0, 0, 0, 0);
                    cmwpubNotify(channel, "MEAS.PULSE",       timing.user, 0, 0, 0, 0);
                    cmwpubNotify(channel, "MEAS.PULSE.VALUE", timing.user, 0, 0, 0, 0);
                    cmwpubNotify(channel, "MEAS.REF",         timing.user, 0, 0, 1, 0);


                    // Log acquisition in event log

                    timingLogAcquisitionCycling(channel);
                }
            }
            else
            {
                timing_pulse_timer    [channel] = 0;
                timing_new_pulse_timer[channel] = 0;

                // Set the reference to V MIN

                if (device.load[device.load_select].limits.v.pos == 0 && device.load[device.load_select].limits.v.neg < 0) // negative
                {
                    device.ppm[0].cycling_v_ref = - device.load[device.load_select].limits.v.min;
                }
                else  // positive or bipolar
                {
                    device.ppm[0].cycling_v_ref =   device.load[device.load_select].limits.v.min;
                }
            }
        }
        else
        {
            // Reset pulse ON flag and timers

            timing_pulse_on       [channel] = false;
            timing_pulse_timer    [channel] = 0;
            timing_new_pulse_timer[channel] = 0;

            // Reset the timing warning because it is not latched

            setStatusBit(&device.status.st_warnings, FGC_WRN_TIMING_EVT, false);
        }
    }


    // Handle CMW notifications for cycling devices on EXTRACTION (required for ELENA's ion switch)

    if(timing_extraction_timer == 0 && timing_extraction_event)
    {
        uint8_t channel;

        for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            if(device.status.state_pc == FGC_PC_CYCLING && device.ref.event_cyc == FGC_EVENT_CYC_INJECTION)
            {
                // Notify at extraction for devices in cycling and listening to injection

                device.ppm[timing_pulse_user].acq_value = device.meas.v_meas;

                cmwpubNotify(channel, "MEAS",             timing.user, 0, 0, 0, 0);
                cmwpubNotify(channel, "MEAS.PULSE",       timing.user, 0, 0, 0, 0);
                cmwpubNotify(channel, "MEAS.PULSE.VALUE", timing.user, 0, 0, 0, 0);
                cmwpubNotify(channel, "MEAS.REF",         timing.user, 0, 0, 1, 0);

                // Log acquisition in event log

                timingLogAcquisitionCycling(channel);
            }
        }

        timing_extraction_event = false;
    }
    else
    {
        // Decrease timer

        timing_extraction_timer--;
    }
}



static void timingHandleCyclingEvent(uint8_t channel, float time_to_event_ms)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    /*
     * 1. Check if the power converter is able to arrive in time in order to rise a warning
     */

    float voltage_rate = fgcddev.iseg.module.params.voltage_ramp_speed[device.iseg.channel.address.module] / 100.0 * device.iseg.channel.voltage_nominal; // [V/s]

    float target_voltage = 0.0;
    float pulse_duration = 0.0;

    if (device.ppm_enabled == FGC_CTRL_ENABLED)
    {
        // PPM device

        target_voltage = device.ppm[timing_pulse_user].pulse_v_ref;
        pulse_duration = device.ppm[timing_pulse_user].pulse_duration;
    }
    else
    {
        // Not PPM device

        target_voltage = device.ppm[0].pulse_v_ref;
        pulse_duration = device.ppm[0].pulse_duration;
    }

    float time_needed_to_pulse = 0.0; // Time to reach next pulse's voltage

    if ((fabs(device.meas.v_meas - target_voltage) > (device.iseg.channel.voltage_nominal * 0.01) && equipdev.device[channel].status.state_op == FGC_OP_NORMAL))
    {
        // The target voltage is different to the current one and mode op normal

        // The voltage is kept until the end of the pulse duration and brought to the target_voltage afterwards

        time_needed_to_pulse  = (float)(timing_pulse_timer[channel]) / 1000.0 + fabs(device.meas.v_meas - target_voltage) / voltage_rate;
    }
    else
    {
        time_needed_to_pulse = 0.0;
    }

    if (time_needed_to_pulse >= (time_to_event_ms / 1000.0))
    {
        setStatusBit(&device.status.st_warnings, FGC_WRN_TIMING_EVT, true);

        logPrintf(channel, "TIMING WARNING Not able to bring voltage to the required setting in time: injection warning (delay %d ms) , user (%d) \n",
                  time_to_event_ms, timing_pulse_user);
    }
    else
    {
        setStatusBit(&device.status.st_warnings, FGC_WRN_TIMING_EVT, false);
    }


    /*
     * 2. Set the timers depending if it is the first pulse or a new pulse arrived during the previous one
     */

    if (timing_pulse_on[channel] == false)
    {
        // First pulse then initialize both timers, set flag pulse ON, and set the voltage

        timing_pulse_timer    [channel] = (uint32_t)(time_to_event_ms + pulse_duration * 1000.0);
        timing_new_pulse_timer[channel] = 0;
        timing_pulse_on       [channel] = true;

        if (device.ppm_enabled == FGC_CTRL_ENABLED)
        {
            device.ppm[0].cycling_v_ref = device.ppm[timing_pulse_user].pulse_v_ref;  // PPM device
        }
        else
        {
            device.ppm[0].cycling_v_ref = device.ppm[0].pulse_v_ref;                  // Not PPM device
        }
    }
    else
    {
        // New pulse arrived then set timer for the new pulse

        timing_new_pulse_timer[channel] = time_to_event_ms + pulse_duration * 1000.0;
    }
}



static void timingStartCycle(struct CC_us_time * iter_time)
{
    uint32_t channel;
    struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(*iter_time);

    for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel ++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        if(device.fgcd_device->name == NULL) continue;

        // Signals the logging library that a new cycle has started

        if(timing.user && device.fgcd_device->name != NULL)
        {
            logStoreStartContinuousRT(device.log.read.log, iter_ns_time, timing.user);
        }

    }
}



static void timingCalcCycle(tTimingTime evt_time, struct CC_us_time * iter_time, bool * is_200ms_boundary, bool * is_1s_boundary)
{
    // set time for the current FGCD iteration

    const uint32_t ms = evt_time.time.tv_nsec / NS_PER_MS;   // Millisecond within UTC second

    struct timeval time;
    time.tv_sec  = evt_time.time.tv_sec;
    time.tv_usec = ms * US_PER_MS;
    timingSetUserTime(0, &time);

    // Set cycle time

    iter_time->secs.abs = time.tv_sec;
    iter_time->us = time.tv_usec;

    // Calculate timing boundaries

    *is_200ms_boundary = (ms % 200 == 0);
    *is_1s_boundary    = (ms       == 0);

    // Decrement time until the next cycle

    if(timing.next_cycle_countdown_ms != UINT32_MAX)
    {
        timing.next_cycle_countdown_ms--;
    }

    // Check whether this is millisecond zero of a new cycle

    if(!timing.next_cycle_countdown_ms) // New cycle
    {
        timing.cycle_ms = 0;

        // Set user

        timing.prev_user    = timing.user;
        timing.user         = timing.next_user;
        timing.next_user    = 0;

        timing.next_ms_cycle_ms = timing.user ? 1 : 0; // Fix next cycle ms at 0 until the user is known
        timing.next_ms_user     = timing.user;

        // Store the time at which the user started

        if(timing.user)
        {
            timingSetUserTime(timing.user,  &time);
        }

        // Launch the start cycle event

        timingStartCycle(iter_time);

        // Log the start of the cycle if logging of timing events is enabled

        if(timing.log_events_flag)
        {
            logPrintf(0, "TIMING Expected Start cycle, user %s (%u)\n", timingUserName(timing.user), timing.user);
        }

    }
    else // This is not the first millisecond of a new cycle
    {
        // Increment millisecond within cycle if user is set

        if(timing.user)
        {
            timing.cycle_ms++;
        }

        // Set millisecond and user for next millisecond

        if(timing.next_cycle_countdown_ms == 1) // Last millisecond of a cycle
        {
            // Log the end of the cycle if logging of timing events is enabled

            if(timing.log_events_flag)
            {
                logPrintf(0, "TIMING Expected Cycle last ms, user %hhu, ms %u\n", timing.user, timing.cycle_ms);
            }

            timing.next_ms_cycle_ms = 0;
            timing.next_ms_user     = timing.next_user;
        }
        else // This is not the last millisecond of a cycle
        {
            timing.next_ms_cycle_ms = timing.user ? timing.cycle_ms + 1 : 0;
            timing.next_ms_user     = timing.user;
        }
    }
}



static void timingLogEventTiming(const char *event_name, const uint32_t event_delay)
{
    uint32_t channel;

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        if (device.mode_pc_on == 1) // cycling
        {
            // Retrieve time

            struct timeval    timestamp;
            timingGetUserTime(0, &timestamp);

            // Log event

            ccEvtLogStoreTimingEvent(channel, &timestamp, event_name, timing.user);
        }
    }
}



static void timingLogAcquisitionCycling(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Retrieve time

    struct timeval    timestamp;
    timingGetUserTime(0, &timestamp);

    // Log acquisition in event log

    static char log_prop[FGC_LOG_EVT_PROP_LEN];

    snprintf(log_prop, FGC_LOG_EVT_PROP_LEN, "ACQUISITION (%2u)   %3s", timing.user, "");

    static char log_val[FGC_LOG_EVT_VAL_LEN];

    snprintf(log_val, FGC_LOG_EVT_VAL_LEN, "%-10s %11.3f %11.3f", "ENABLED", device.status.v_ref, device.meas.v_meas);

    ccEvtlogStoreRecord(channel, &timestamp, log_prop, log_val, "NOW", EVTLOG_STATUS_FREQ);
}

struct Timing_event_handler timing_event_handlers[]=
{
    { timing_handler_names[TIMING_HANDLER_POSIXMS],          &timingMillisecond       },
    { timing_handler_names[TIMING_HANDLER_MS],               &timingMillisecond       },
    { timing_handler_names[TIMING_HANDLER_CYCLEWARNING],     &timingCycleWarning      },
    { timing_handler_names[TIMING_HANDLER_INJECTWARNING],    &timingInjectionWarning  },
    { timing_handler_names[TIMING_HANDLER_EXTRACTWARNING],   &timingExtractionWarning },
    { timing_handler_names[TIMING_HANDLER_TELEGRAM],         &timingTelegramReady     },
    { "",                                                    NULL                     },
};


// EOF
