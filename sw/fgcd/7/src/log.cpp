#include <cstdlib>
#include <equipdev.h>
#include <classes/93/logMenus.h>
#include <classes/93/logMenusInit.h>
#include <classes/93/logStructsInit.h>
#include <logging.h>

#include <cmwpub.h>
#include <timing.h>

#include <equip_liblog.h>

#include <log.h>


void logInitClass(uint8_t channel)
{
    // Class specific action to initalize logs

    //struct Equipdev_channel &device = equipdev.device[channel];

    //logMenusInit(&device.log.mgr, &device.log.menus, LOG_BUFFERS_LEN);

}



uint32_t logSignals(uint8_t channel, const struct CC_us_time * const us_time)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(*us_time);

    // Update the log menus

    logMenuUpdate(&device.log.mgr, &device.log.menus);

    // Log

    logStoreContinuousRT(&device.log.structs.log[LOG_ACQ], iter_ns_time);

    return 0;
}



