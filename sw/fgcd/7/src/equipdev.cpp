/*
 *  Filename: equipdev.cpp
 *
 *  Purpose:  Functions for equipment devices
 *
 *  Author:   Stephen Page, J.M. de Paco
 */

#include <stdio.h>
#include <string.h>


#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <nonvol.h>
#include <logging.h>
#include <prop_equip.h>
#include <cmwpub.h>
#include <timing.h>
#include "fgc_fieldbus.h"
#include <classes/93/defconst.h>
#include <classes/93/definfo.h>
#include <classes/93/defprops.h>

#include <equipdev.h>
#include <log.h>
#include <equip_liblog.h>



#if N_PROP_SYMBOLS > EQUIPDEV_MAX_PROPS
#error EQUIPDEV_MAX_PROPS must be greater than or equal to N_PROP_SYMBOLS
#endif

#define LOG_MEAS_DELAY 10

// Global variables

struct Equipdev equipdev;

// Static functions

// Initialise the configuration of an equipment device

static uint32_t equipdevInitDevice(uint32_t channel)
{
    fprintf(stderr,"equipdevInitDevice: channel %u %s\n",channel, fgcd.device[channel].name);

    struct Equipdev_channel &device = equipdev.device[channel];

    device.fgcd_device = &fgcd.device[channel];

    // Set test strings

    device.test.strings[0] = "FIRST";
    device.test.strings[1] = "SECOND";

    // Initialize libevtlog

    if(ccEvtlogInit(channel) != 0) return 1;

    // Initialise logging : This must be done AFTER initialising libref & libreg to make sure pointers

    ccLogInit(channel);

    // Give values to the reference units

    device.ppm[0].pulse_units = 0; // 0 value for V units according to def/src/symlists/reg_mode_units.xml

    // Set start time to be same as FGCD start time

    device.time_start = fgcd.time_start;

    // Initialise STATE.PC to FLT_OFF and STATE.OP to UNCONFIGURED

    statePcInit(channel);
    stateOpInit(channel);

    // Initialize MODE.PC_SIMPLIFIED to OFF

    device.mode_pc_simplified = FGC_PC_OFF;

    // Load device configuration

    nonvolLoad(&fgcd.device[channel], 0);

    // Load properties that must be set last

    nonvolLoad(&fgcd.device[channel], 1);

    return 0;
}



int32_t equipdevStart(void)
{
    fprintf(stderr,"equipdevStart\n");

    // Initialise the standard equipdev services (hash tables, parser data, ...)

     equipdevInit(NUM_PARS_FUNC_93, ccEvtlogStoreSetCmd);

    // Initialise each device

    for(uint32_t channel = 1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
    {
        equipdev.device[channel].fgcd_device = &fgcd.device[channel];

        if(fgcd.device[channel].name != NULL)
        {
            if(equipdevInitDevice(channel) != 0) return 1;
        }
    }

    return 0;
}

// External functions

void equipdevInitDevices(void)
{
    struct FGCD_device  *fgcd_device;
    uint32_t            i;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        fgcd_device = &fgcd.device[i];

        if(fgcd_device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set command queue

            fgcd_device->command_queue = &fgcddev.parser.command_queue;

            // Set device as online

            fgcd_device->online  = 1;
            fgcd_device->ready   = 1;
        }
    }
}


void equipdevPublish(void)
{
    for(uint32_t i = 1; i <= FGCD_MAX_EQP_DEVS; ++i)
    {
        struct Equipdev_channel &device     = equipdev.device[i];
        struct fgc_stat         &pub_data   = pub.published_data.status[i];
        struct fgc93_stat       &pub_status = pub_data.class_data.c93;

        if(fgcd.device[i].fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set class in published data

            pub_data.class_id = EQUIPDEV_CLASS_ID;

            // Swap byte order and copy device status into published data

            pub_status.st_faults    = htons(device.status.st_faults);
            pub_status.st_warnings  = htons(device.status.st_warnings);
            pub_status.st_unlatched = htons(device.status.st_unlatched);
            pub_status.state_op     = device.status.state_op;
            pub_status.state_vs     = device.status.state_vs;
            pub_status.state_pc     = device.status.state_pc;

            device.status.i_meas    = device.meas.i_meas;
            pub_status.i_meas       = htonf(device.status.i_meas);

            pub_status.v_ref        = htonf(device.status.v_ref);

            device.status.v_meas    = device.meas.v_meas;
            pub_status.v_meas       = htonf(device.status.v_meas);
        }
    }
}



// EOF
