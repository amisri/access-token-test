/*!
 * @file iseg.cpp
 * @brief  Functions to Get and Set items in the ISEG Hardware using the ISEGHAL API
 * @author Jose M. de Paco
 */

#define iseg_functions_GLOBALS


// Includes

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

// FGCD includes

#include <iseg.h>
#include <logging.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcddev.h>



// Constants

#define ISEG_CRATE_INTERFACE "hal://192.168.0.2:1454/can0,user,pass"


// Iseg constants (taken from isegHAL documentation)

#define SIZE_FULL_ISEG_ITEM         120
#define SIZE_ISEG_ADDRESS           10
#define SIZE_ISEG_MODULE_ADDRESS    5
#define ISEG_CRATE_ADDRESS          "0.1000."

#define VALUE_SETCHANNEL_ON         "8"
#define VALUE_SETCHANNEL_OFF        "0"
#define VALUE_EMERGENCY_STOP        "32"
#define VALUE_CLEAR_CHANNEL         "65535"
#define ITEM_RESET_CHANNEL          "Control:6"


// Internal structures, unions and enumerations

/*!
 *  Struct for the iseg TCP/IP connection
 */

struct Iseg_connection
{
    char                                name[VALUE_SIZE];
    bool                                connected;
    uint32_t                            number_of_attempt;
};

static struct Iseg_connection iseg_connection = {"", false, 0};

/*!
 *  Structures for data-driven access to the ISEG param items
 */

static struct Iseg_items iseg_channel_param_items[] =
{  // Name                      Type               Level            Pointer to param value
    {"DelayedTripTime",         ISEG_ITEM_FLOAT,   ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.params.delayed_trip_time,   sizeof(equipdev.device[0]) },
    {"DelayedTripAction",       ISEG_ITEM_INT16,   ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.params.delayed_trip_action, sizeof(equipdev.device[0]) },
    {"VoltageBounds",           ISEG_ITEM_FLOAT,   ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.params.voltage_bounds,      sizeof(equipdev.device[0]) },
    {"CurrentSet",              ISEG_ITEM_FLOAT,   ISEG_CHANNEL,    &equipdev.device[0].iseg.channel.params.current_trip_set,    sizeof(equipdev.device[0]) },
};

static struct Iseg_items iseg_module_param_items[] =
{  // Name                      Type               Level            Pointer to param value
    {"VoltageRampSpeed",        ISEG_ITEM_FLOAT,   ISEG_MODULE,     &fgcddev.iseg.module.params.voltage_ramp_speed[0],           sizeof(fgcddev.iseg.module.params.voltage_ramp_speed[0])},
    {"KillEnable",              ISEG_ITEM_INT16,   ISEG_MODULE,     &fgcddev.iseg.module.params.kill_enable[0],                  sizeof(fgcddev.iseg.module.params.kill_enable[0])},
    {"FineAdjustment",          ISEG_ITEM_INT16,   ISEG_MODULE,     &fgcddev.iseg.module.params.fine_adjustment[0],              sizeof(fgcddev.iseg.module.params.fine_adjustment[0])},
};



/*!
 *  Array lists with the addresses (module and channel) of the iseg equipment device.
 */

static const uint8_t iseg_modules [FGCD_MAX_DEVS] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4,
                                                     6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9};

static const uint8_t iseg_channels[FGCD_MAX_DEVS] = {0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7,
                                                     0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7};


/*
 * Array with the used modules
 */
static bool iseg_module_is_used     [FGCD_NUMBER_ISEG_MODULES] = {false, false, false, false, false, false, false, false, false};

/*
 * Array with the offline or not ready modules
 */
static bool iseg_module_is_available[FGCD_NUMBER_ISEG_MODULES] = {false, false, false, false, false, false, false, false, false};



// Internal function declarations


/*!
 * Get an iseg item value using the API ISEGHAL
 */
static int32_t isegGetItem(const uint8_t log_index, const char *iseg_address, const char *item_name, IsegItem *iseg_get_result);

/*!
 * Set an iseg item value using the API ISEGHAL
 */
static int32_t isegSetItem(const uint8_t index, const char *iseg_address, const char *item_name, const char *set_value);

/*!
 * Function to set the parameters: all or only modified
 */
static void isegSetParameters(bool all_parameters);

/*!
 * Check if config parameter has been modified and set it
 */
static int32_t isegCheckAndSetParams(uint8_t index, struct Iseg_items *iseg_items, const uint32_t num_items, bool all_params);

/*!
 * Switch ON/OFF the power converter
 */
static int32_t isegSwitchPowerConverter(const uint8_t channel, const char* value);

/*
 * Function to convert string to uint16_t
 */
static int32_t strToUint16(const char *str, uint16_t *res);

/*
 * Function to convert string to uint32_t
 */
static int32_t strToUint32(const char *str, uint32_t *res);

/*
 * Function to convert string to float
 */
static int32_t strToFloat(const char *str, float *res);



// External function definitions


int32_t isegDisconnectCrate(void)
{
    // Call the iseg API to disconnect

    IsegResult result = iseg_disconnect(iseg_connection.name);

    // Check the result of the disconnection

    if (result == ISEG_OK)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}



int32_t isegGetIsegItems(const uint8_t index, struct Iseg_items *iseg_items, const uint8_t num_items)
{
    char iseg_address [SIZE_ISEG_ADDRESS];

    // Check if communication is alive

    if (isegIsConnected() == false)
    {
        return 1;
    }

    // Skip the channels (except the address 0) if the module has been marked as unavailable

    if (equipdev.device[index].iseg.channel.address.channel != 0 && !iseg_module_is_available[equipdev.device[index].iseg.channel.address.module])
    {
        return 1;
    }

    // For each item in the array of structs -> get the value of the iseg item

    uint8_t i;

    for (i = 0; i < num_items; i++)
    {
        // Get the address of the item depending if it is a channel, module, crate, line or system item

        // log_index is 0 for FGCD device (crate, module, line or system), and index (channel) for equipment devices

        uint32_t log_index = 0;

        if (iseg_items[i].level == ISEG_CHANNEL)
        {
            log_index = index;

            if(isegGetChannelAddress(index, iseg_address, sizeof(iseg_address)))
            {
                // Error getting address for the channel

                return 1;
            }
        }
        else if (iseg_items[i].level == ISEG_MODULE)
        {
            log_index = 0;

            // Skip if the module[i] is not used or not available

            if (iseg_module_is_used[index] == false || iseg_module_is_available[index] == false)
            {
                return 1;
            }

            snprintf(iseg_address, SIZE_ISEG_ADDRESS, "0.%d.", index);
        }
        else if (iseg_items[i].level == ISEG_CRATE)
        {
            log_index = 0;

            snprintf(iseg_address, SIZE_ISEG_ADDRESS, ISEG_CRATE_ADDRESS);
        }
        else if (iseg_items[i].level == ISEG_LINE)
        {
            log_index = 0;

            snprintf(iseg_address, SIZE_ISEG_ADDRESS, "0.");
        }
        else if (iseg_items[i].level == ISEG_SYSTEM)
        {
            log_index = 0;
            strcpy(iseg_address,"");
        }
        else
        {
            logPrintf(log_index, "ISEG Item type is not one of the defined: channel, module, crate or system \n");

            return 1;
        }

        // Call isegGetItem and put the got value in get_value

        IsegItem get_value;
        int8_t error = isegGetItem(log_index, iseg_address, iseg_items[i].name, &get_value);

        // If OK, convert the value to its type (float or integer) and save it into its destination

        if (!error)
        {
            // Declare devices as online if they have been recovered

            if (iseg_items[i].level == ISEG_CHANNEL)
            {
                struct Equipdev_channel &device = equipdev.device[index];

                // Module has been recovered

                if (iseg_module_is_available[device.iseg.channel.address.module] == false)
                {
                    // Mark the module as available

                    iseg_module_is_available[device.iseg.channel.address.module] = 1;

                    // Set the all devices in the same module as online and ready

                    uint8_t module_number = device.iseg.channel.address.module;
                    uint8_t ch_number;

                    for (ch_number = 1; ch_number <= FGCD_MAX_EQP_DEVS; ch_number++)
                    {
                        struct Equipdev_channel &dev = equipdev.device[ch_number];

                        if (dev.iseg.channel.address.module == module_number)
                        {
                            fgcd.device[ch_number].online = 1;

                            // Comms warning

                            setStatusBit(&dev.status.st_warnings, FGC_WRN_COMMS, false);
                        }
                    }
                }
            }

            // Check the type (float or int)

            if (iseg_items[i].type == ISEG_ITEM_FLOAT)
            {
                // Convert to float

                bool conversion_ok = false;
                float float_value = 0.0;

                if (!strToFloat(get_value.value, &float_value))
                {
                    void *ptr = iseg_items[i].destination;

                    // Cast pointer to char (byte) and increased byte size

                    ptr = (char*)ptr + index * iseg_items[i].element_step_size;

                    // Cast pointer to float and assign float value

                    *((float*)ptr) = float_value;

                    conversion_ok = true;
                }
                else
                {
                    // Conversion error

                    logPrintf(log_index, "ISEG Status update (%s) failed: bad conversion string to float \n", iseg_items[i].name);
                    conversion_ok = false;
                }

                if (!strcmp(iseg_items[i].name, "VoltageMeasure"))
                {
                    struct Equipdev_channel &device = equipdev.device[index];
                    setStatusBit(&device.status.st_warnings, FGC_WRN_V_MEAS, !conversion_ok);
                }
            }
            else if (iseg_items[i].type == ISEG_ITEM_INT16)
            {
                // Convert to integer

                uint16_t int_value = 0;

                if (!strToUint16(get_value.value, &int_value))
                {
                    void *ptr = iseg_items[i].destination;

                    // Cast pointer to char (byte) and increased byte size

                    ptr = (char*)ptr + index * iseg_items[i].element_step_size;

                    // Cast pointer to int and assign int value

                    *((uint16_t*)ptr) = int_value;
                }
                else
                {
                    // Conversion error

                    logPrintf(log_index, "ISEG Status update (%s) failed: bad conversion string to UINT16 \n", iseg_items[i].name);
                }
            }
            else if (iseg_items[i].type == ISEG_ITEM_INT32)
            {
                // Convert to integer

                uint32_t int_value;

                if (!strToUint32(get_value.value, &int_value))
                {
                    void *ptr = iseg_items[i].destination;

                    // Cast pointer to char (byte) and increased byte size

                    ptr = (char*)ptr + index * iseg_items[i].element_step_size;

                    // Cast pointer to int and assign int value

                    *((uint32_t*)ptr) = int_value;
                }
                else
                {
                    // Conversion error

                    logPrintf(log_index, "ISEG Status update (%s) failed: bad conversion string to UINT32 \n", iseg_items[i].name);
                }
            }
            else
            {
                logPrintf(log_index, "ISEG Invalid type \n", iseg_items[i].name);
            }
        }
        else
        {
            // Error

            if (iseg_items[i].level == ISEG_CHANNEL)
            {
                struct Equipdev_channel &device = equipdev.device[index];

                // Mark the module as not available

                iseg_module_is_available[device.iseg.channel.address.module] = 0;

                // Set the all devices in the same module as offline and not ready

                uint8_t module_number = device.iseg.channel.address.module;
                uint8_t ch_number;

                for (ch_number = 1; ch_number <= FGCD_MAX_EQP_DEVS; ch_number++)
                {
                    struct Equipdev_channel &dev = equipdev.device[ch_number];

                    if (dev.iseg.channel.address.module == module_number)
                    {
                        if(equipdev.device[ch_number].status.state_op != FGC_OP_SIMULATION)
                        {
                            fgcd.device[ch_number].online = 0;
                        }

                        // Comms warning

                        setStatusBit(&dev.status.st_warnings, FGC_WRN_COMMS, true);
                    }
                }


                // Check if the communication was lost

                IsegItem get_value;

                if (isegGetItem(log_index, ISEG_CRATE_ADDRESS, "Connected", &get_value))
                {
                    iseg_connection.connected = false;

                    // Close the socket

                    isegDisconnectCrate();
                }
                return 1;
            }
            return 1;
        }
    }
    return 0;
}



int32_t isegResetModuleOfChannel(const uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Get the module address of the channel

    char iseg_address[SIZE_ISEG_MODULE_ADDRESS];
    snprintf(iseg_address, SIZE_ISEG_MODULE_ADDRESS, "0.%d.", device.iseg.channel.address.module);

    // Call iseg function

    if (isegSetItem(channel, iseg_address, ITEM_RESET_CHANNEL, "1") == false)  // OK
    {
        logPrintf(channel, "ISEG Module's reset done \n");
        return 0;
    }
    else // error
    {
        logPrintf(channel, "ISEG Request to reset the module failed \n");
        return 1;
    }
}



int32_t isegResetChannel(const uint8_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Get the module number of the channel

    uint8_t module = device.iseg.channel.address.module;

    // Get the address of the item

    char iseg_address[SIZE_ISEG_ADDRESS];

    if(isegGetChannelAddress(channel, iseg_address, sizeof(iseg_address)))
    {
        // Error getting address for the channel

        return 1;
    }

    // Call iseg function: write 65535 to EventStatus to clear all channel events

    if (isegSetItem(channel, iseg_address, "EventStatus", VALUE_CLEAR_CHANNEL) == false) // OK
    {
        if (getStatusBit(fgcddev.iseg.module.event[module],  FGC_ISG_M_EV_SAFETY_LOOP_OPEN))
        {
            // Reset the module to remove the safety loop open event

            if(isegResetModuleOfChannel(channel))
            {
                logPrintf(channel, "ISEG Request to reset the channel failed \n");
                return 1;
            }
        }

        logPrintf(channel, "ISEG Channel's reset done \n");

        // Clear the latched status

        device.status.st_faults = 0;

        return 0;
    }
    else
    {
        // Error

        logPrintf(channel, "ISEG Request to reset the channel failed \n");
        return 1;
    }
}



void isegResetAll(void)
{
    // Reset the crate

    char iseg_address[10];
    snprintf(iseg_address, SIZE_ISEG_ADDRESS, ISEG_CRATE_ADDRESS);
    isegSetItem(0, iseg_address, "Control:0", "1");
    sleep(1);
    isegSetItem(0, iseg_address, "Control:1", "1");
    logPrintf(0, "ISEG Reset crate controller \n");
    sleep(1);

    /*
     *  Reset all modules
     */

    uint8_t module;

    for (module = 0; module < FGCD_NUMBER_ISEG_MODULES; module++)
    {
        // Reset module if it is used and available

        if (iseg_module_is_used[module] == true && iseg_module_is_available[module] == true)
        {
            logPrintf(0, "ISEG Reset module %d \n", module);

            char iseg_module_address[SIZE_ISEG_MODULE_ADDRESS];
            snprintf(iseg_module_address, SIZE_ISEG_MODULE_ADDRESS, "0.%d.", module);

            isegSetItem(0, iseg_module_address, ITEM_RESET_CHANNEL, "1");
        }

    }

    // Clear the property to request the reset

    fgcddev.iseg.reset = 0;

    /*
     *  Reset all channels
     */

    uint8_t channel;

    for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Clear warnings and faults

        device.status.st_faults = 0;
    }
}



int32_t isegSwitchOnPowerConverter(const uint8_t channel)
{
    return isegSwitchPowerConverter(channel, VALUE_SETCHANNEL_ON);
}



int32_t isegSwitchOffPowerConverter(const uint8_t channel)
{
    return isegSwitchPowerConverter(channel, VALUE_SETCHANNEL_OFF);
}



int32_t isegEmergencyStopPowerConverter(const uint8_t channel)
{
    return isegSwitchPowerConverter(channel, VALUE_EMERGENCY_STOP);
}



int32_t isegHVPowerOn(void)
{
    if (isegSetItem(0, "0.1000.", "PowerOn" , "1") == false)  // OK
    {
        // Wait until HV power is ON

        logPrintf(0, "ISEG Crate HV Power is starting \n");

        sleep(10);

        logPrintf(0, "ISEG Crate HV Power successfully turned ON \n");

        // General reset after restart

        isegResetAll();

        return 0;
    }
    else // error
    {
        logPrintf(0, "ISEG Error turning ON the crate HV Power \n");
        return 1;
    }
}



int32_t isegHVPowerOff(void)
{
    if (isegSetItem(0, "0.1000.", "PowerOn" , "0") == false)  // OK
    {
        // Wait until HV power is OFF

        logPrintf(0, "ISEG Crate HV Power is stopping \n");

        sleep(10);

        logPrintf(0, "ISEG Crate HV Power successfully turned OFF \n");

        // General reset

        isegResetAll();

        return 0;
    }
    else // error
    {
        logPrintf(0, "ISEG Error turning OFF the crate HV Power \n");
        return 1;
    }
}



int32_t isegSetVoltageReference(const uint8_t channel, const float voltage_ref)
{
    // Get the iseg_address for the channel and call the set function

    char iseg_address[SIZE_ISEG_ADDRESS];

    if (isegGetChannelAddress(channel, iseg_address, sizeof(iseg_address)) == false)
    {
        // Convert voltage_ref (float) to string

        char voltage_ref_str[50];
        snprintf(voltage_ref_str, 50, "%f", voltage_ref);

        // Set the item

        int32_t error = isegSetItem(channel, iseg_address, "VoltageSet" , voltage_ref_str);

        if (!error)  // OK
        {
            logPrintf(channel, "ISEG Voltage reference %s set to the power converter \n", voltage_ref_str);
            return 0;
        }
        else
        {
            // Error

            logPrintf(channel, "ISEG Voltage reference %s set request failed \n", voltage_ref_str);
            return 1;
        }
    }
    else
    {
        // Error getting address

        logPrintf(channel, "ISEG Voltage reference set request failed. Invalid channel address \n");
        return 1;
    }
}



int32_t isegGetChannelAddress(const uint8_t channel, char *iseg_address, size_t size)
{
    if (channel > FGCD_MAX_EQP_DEVS)
    {
        logPrintf(channel, "ISEG Invalid channel number %d > %d \n", channel, FGCD_MAX_EQP_DEVS);
        return 1;
    }

    // Module number

    uint8_t module_number = iseg_modules[channel-1];

    // Channel number

    uint8_t channel_number = iseg_channels[channel-1];

    // Save the information in the property iseg.channel

    struct Equipdev_channel &device     = equipdev.device[channel];
    device.iseg.channel.address.module  = module_number;
    device.iseg.channel.address.channel = channel_number;

    // Get string with the form of crate.module.channel (crate is always zero for iseg FGCD class)

    snprintf(iseg_address, size, "0.%d.%d.", module_number, channel_number);

    return 0;
}



bool isegIsConnected(void)
{
    return iseg_connection.connected;
}



void isegConnectCrate(void)
{
    if (fgcddev.iseg.crate.communication == FGC_CTRL_ENABLED)
    {
        // Communication is enabled

        // Number of attempt

        ++iseg_connection.number_of_attempt;

        if (iseg_connection.number_of_attempt > 100)
        {
            iseg_connection.number_of_attempt = 1;
        }

        // Set the NetworkTimeout

        iseg_setItem(iseg_connection.name, "NetworkTimeout", "0.5");

        // Connection name

        snprintf(iseg_connection.name, sizeof(iseg_connection.name), "eth_%u", iseg_connection.number_of_attempt + 1);

        // Call the iseg API to connect

        IsegResult result = iseg_connect(iseg_connection.name, ISEG_CRATE_INTERFACE, NULL);

        // Check the result

        switch(result)
        {
            case ISEG_OK                 : logPrintf(0, "ISEG INFO %s CONNECT successful: ISEG_OK \n", iseg_connection.name);              iseg_connection.connected = true;      break;

            case ISEG_ERROR              : logPrintf(0, "ISEG ERROR %s CONNECT failed: ISEG_ERROR \n", iseg_connection.name);              iseg_connection.connected = false;     break;

            case ISEG_WRONG_SESSION_NAME : logPrintf(0, "ISEG ERROR %s CONNECT failed: ISEG_WRONG_SESSION_NAME \n", iseg_connection.name); iseg_connection.connected = false;     break;

            case ISEG_WRONG_USER         : logPrintf(0, "ISEG ERROR %s CONNECT failed: ISEG_WRONG_USER \n", iseg_connection.name);         iseg_connection.connected = false;     break;

            case ISEG_WRONG_PASSWORD     : logPrintf(0, "ISEG ERROR %s CONNECT failed: ISEG_WRONG_PASSWORD \n", iseg_connection.name);     iseg_connection.connected = false;     break;

            case ISEG_NOT_AUTHORIZED     : logPrintf(0, "ISEG ERROR %s CONNECT failed: ISEG_NOT_AUTHORIZED \n", iseg_connection.name);     iseg_connection.connected = false;     break;

            default                      : logPrintf(0, "ISEG ERROR %s CONNECT failed \n", iseg_connection.name);                          iseg_connection.connected = false;     break;
        }
    }
    else
    {
        // Communication is disabled

        iseg_connection.connected = false;

        // Wait 200 ms to simulate the time of an iseg cycle

        usleep(200000);
    }
    return;
}



void isegMarkUsedModules(void)
{
    uint8_t channel;

    for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
    {
        if((&equipdev.device[channel])->fgcd_device->name != NULL)
        {
            // Mark module as used

            iseg_module_is_used     [iseg_modules[channel-1]] = true;

            // Initialize all used modules as available

            iseg_module_is_available[iseg_modules[channel-1]] = true;
        }
    }
}



void isegSetAllParameters(void)
{
    isegSetParameters(true);
}



void isegSetModifiedParameters(void)
{
    isegSetParameters(false);
}



// Internal function definitions


static int32_t isegGetItem(const uint8_t log_index, const char *iseg_address, const char *item_name, IsegItem *iseg_get_value)
{
    // Get the fully qualified name of the ISEG channel property

    char iseg_item_full_name[SIZE_FULL_ISEG_ITEM];
    snprintf(iseg_item_full_name, sizeof(iseg_item_full_name), "%s%s", iseg_address, item_name);

    // getItem: call to API

    *iseg_get_value =  iseg_getItem(iseg_connection.name, iseg_item_full_name);

    // GET successful

    if (!strcmp(iseg_get_value->quality, ISEG_ITEM_QUALITY_OK))
    {
        return 0;
    }

    // Otherwise check the result of the getItem request for errors

    else if (!strcmp(iseg_get_value->quality, ISEG_ITEM_QUALITY_INVALID))
    {
        //logPrintf(log_index, "ISEG ERROR %s iseg GET %s failed: ISEG_ITEM_QUALITY_INVALID \n", iseg_connection.name, iseg_item_full_name);
        return 1;
    }
    else if (!strcmp(iseg_get_value->quality, ISEG_ITEM_QUALITY_INITIALIZE))
    {
        //logPrintf(log_index, "ISEG ERROR %s GET %s RESULT Value = %s Quality = INITIALIZE \n", iseg_connection.name, iseg_item_full_name, iseg_get_value->value);
        return 1;
    }
    else if (!strcmp(iseg_get_value->quality, ISEG_ITEM_QUALITY_COMMUNICATION_BAD))
    {
        logPrintf(log_index, "ISEG ERROR %s iseg GET %s failed: ISEG_ITEM_QUALITY_COMMUNICATION_BAD code %s \n", iseg_connection.name, iseg_item_full_name, iseg_get_value->quality);
        return 1;
    }
    else if (!strcmp(iseg_get_value->quality, ISEG_ITEM_QUALITY_ERROR))
    {
        logPrintf(log_index, "ISEG ERROR %s iseg GET %s failed: ISEG_ITEM_QUALITY_ERROR code \n",  iseg_connection.name, iseg_item_full_name);
        return 1;
    }
    else
    {
        logPrintf(log_index, "ISEG ERROR %s iseg GET %s failed: ISEG ERROR unknown \n", iseg_connection.name, iseg_item_full_name);
        return 1;
    }
}



static int32_t isegSetItem(const uint8_t index, const char *iseg_address, const char *item_name, const char *set_value)
{
    // Skip calling the API for devices in simulation

    struct Equipdev_channel &device = equipdev.device[index];

    if(device.status.state_op != FGC_OP_SIMULATION)
    {
        // Get the fully qualified name of the ISEG channel property

        char iseg_item_full_name[SIZE_FULL_ISEG_ITEM];
        snprintf(iseg_item_full_name, sizeof(iseg_item_full_name), "%s%s", iseg_address, item_name);

        // setItem: call to API

        IsegResult result =  iseg_setItem(iseg_connection.name, iseg_item_full_name, set_value);

        // Check the result

        if (result == ISEG_OK)
        {
            return 0;
        }
        else //switch the error
        {
            switch (result)
            {
                case ISEG_ERROR:              logPrintf(index, "ISEG ERROR %s iseg SET %s = %s failed: ISEG_ERROR              \n", iseg_connection.name, iseg_item_full_name, set_value);  return 1;    break;

                case ISEG_WRONG_SESSION_NAME: logPrintf(index, "ISEG ERROR %s iseg SET %s = %s failed: ISEG_WRONG_SESSION_NAME \n", iseg_connection.name, iseg_item_full_name, set_value);  return 1;    break;

                case ISEG_WRONG_USER:         logPrintf(index, "ISEG ERROR %s iseg SET %s = %s failed: ISEG_WRONG_USER         \n", iseg_connection.name, iseg_item_full_name, set_value);  return 1;    break;

                case ISEG_WRONG_PASSWORD:     logPrintf(index, "ISEG ERROR %s iseg SET %s = %s failed: ISEG_WRONG_PASSWORD     \n", iseg_connection.name, iseg_item_full_name, set_value);  return 1;    break;

                case ISEG_NOT_AUTHORIZED:     logPrintf(index, "ISEG ERROR %s iseg SET %s = %s failed: ISEG_NOT_AUTHORIZED     \n", iseg_connection.name, iseg_item_full_name, set_value);  return 1;    break;

                default:                      logPrintf(index, "ISEG ERROR %s iseg SET %s = %s failed: Unknown error           \n", iseg_connection.name, iseg_item_full_name, set_value);  return 1;    break;
            }
        }
    }
    else
    {
        return 0;
    }
}


static void isegSetParameters(bool all_parameters)
{
    // Channel parameters

    uint8_t channel;

    for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Set parameters if devices is configured and module is available

        if((&equipdev.device[channel])->fgcd_device->name != NULL && iseg_module_is_available[device.iseg.channel.address.module] == true)
        {
            isegCheckAndSetParams(channel, iseg_channel_param_items, sizeof(iseg_channel_param_items) / sizeof(iseg_channel_param_items[0]), all_parameters);
        }
    }

    // Module parameters

    uint8_t module;

    for (module = 0; module < 10; module++)
    {
        // If the module[i] is used and available then set the params

        if (iseg_module_is_used[module] && iseg_module_is_available[module])
        {
            // Check if parameters have been modified and set them

            isegCheckAndSetParams(module, iseg_module_param_items, sizeof(iseg_module_param_items) / sizeof(iseg_module_param_items[0]), all_parameters);
        }
    }
}



static int32_t isegCheckAndSetParams(uint8_t index, struct Iseg_items *iseg_items, const uint32_t num_items, bool all_parameters)
{
    uint8_t log_index = 0;
    char    level_str[20];
    uint8_t offset = 0;

    // Get the address of the channel or module

    char iseg_address[SIZE_ISEG_ADDRESS];

    if (iseg_items[0].level == ISEG_CHANNEL)
    {
        isegGetChannelAddress(index, iseg_address, sizeof(iseg_address));

        log_index = index;
        strcpy(level_str, "Channel");

    }
    else if (iseg_items[0].level == ISEG_MODULE)
    {
        snprintf(iseg_address, SIZE_ISEG_ADDRESS, "0.%d.", index);

        log_index = 0;
        strcpy(level_str, "Module");
    }

    // For each item in the array of structs -> check if the property parameter has been modified

    uint32_t i;

    for (i = 0; i < num_items; i++)
    {
        // Compute the offset depending on ISEG level

        if (iseg_items[0].level == ISEG_CHANNEL)
        {
            offset = 1; // The offset is one type (for example one float)
        }
        else if (iseg_items[0].level == ISEG_MODULE)
        {
            offset = FGCD_NUMBER_ISEG_MODULES; // The variables for module are arrays
        }

        // Pointer to item

        void *ptr = iseg_items[i].destination;
        ptr = (char*)ptr + index * iseg_items[i].element_step_size;   // cast pointer to char (byte) and increased byte size


        // Check the type

        if (iseg_items[i].type == ISEG_ITEM_FLOAT)
        {
            float param_value = *((float*)ptr); // cast pointer to float and deref
            float param_prev_value = *(((float*)ptr + offset));

            if (param_value != param_prev_value || all_parameters)
            {
                // Assign the new value to the previous

                *(((float*)ptr + offset)) = param_value;

                // Set the new value to the hardware

                // Convert to string

                char param_value_str[50];
                snprintf(param_value_str, sizeof(param_value_str), "%f", param_value);

                // Item name

                char item_name_str[50];

                // Special case for killEnable and fineAdjustment

                if (!strcmp(iseg_items[i].name, "KillEnable"))
                {
                    strcpy(item_name_str, "Control:14");
                }
                else if (!strcmp(iseg_items[i].name, "FineAdjustment"))
                {
                    strcpy(item_name_str, "Control:12");
                }

                // Rest of cases

                else
                {
                    strcpy(item_name_str, iseg_items[i].name);
                }

                // Set iseg item

                if(isegSetItem(log_index, iseg_address, item_name_str, param_value_str) == false)
                {
                    logPrintf(log_index, "ISEG The parameter %s %s %d has been set to %s \n", iseg_items[i].name, level_str, index, param_value_str);
                }
                else
                {
                    logPrintf(log_index, "ISEG Error setting the parameter %s %s %d \n", iseg_items[i].name, level_str, index);
                    return 1;
                }
            }
        }
        else if (iseg_items[i].type == ISEG_ITEM_INT16)
        {
            uint16_t param_value      = *((uint16_t*)ptr);
            uint16_t param_prev_value = *(((uint16_t*)ptr + offset));

            if (param_value != param_prev_value || all_parameters)
            {
                // Assign the new value to the previous

                *(((uint16_t*)ptr + offset)) = param_value;

                // Set the new value to the hardware

                // Convert to string

                char param_value_str[50];
                snprintf(param_value_str, sizeof(param_value_str), "%d", param_value);


                // Set iseg item

                if(isegSetItem(log_index, iseg_address, iseg_items[i].name, param_value_str) == false)
                {
                    logPrintf(log_index, "ISEG The parameter %s %s %d has been set to %s \n", iseg_items[i].name, level_str, index, param_value_str);
                }
                else
                {
                    logPrintf(log_index, "ISEG Error setting the parameter %s %s %d \n", iseg_items[i].name, level_str, index);
                    return 1;
                }
            }
        }
    }
    return 0;
}



static int32_t isegSwitchPowerConverter(const uint8_t channel, const char* value)
{
    // Get the iseg_address for the channel and call the set function

    char iseg_address[SIZE_ISEG_ADDRESS];

    if (isegGetChannelAddress(channel, iseg_address, sizeof(iseg_address)) == false)
    {
        // Call iseg function

        int32_t error = isegSetItem(channel, iseg_address, "Control" , value);

        if (!error)
        {
            if (!strcmp(value, VALUE_SETCHANNEL_ON))
            {
                logPrintf(channel, "ISEG Power Converter switch ON command \n");
            }
            else if (!strcmp(value, VALUE_SETCHANNEL_OFF))
            {
                logPrintf(channel, "ISEG Power Converter switch OFF command \n");
            }
            else if (!strcmp(value, VALUE_EMERGENCY_STOP))
            {
                logPrintf(channel, "ISEG Emergency Stop of the Power Converter \n");
            }

            return 0;
        }
        else // error
        {
            if (!strcmp(value, VALUE_SETCHANNEL_ON))
            {
                logPrintf(channel, "ISEG Request to switch ON the power converter failed \n");
            }
            else if (!strcmp(value, VALUE_SETCHANNEL_OFF))
            {
                logPrintf(channel, "ISEG Request to switch OFF the power converter failed \n");
            }
            else if (!strcmp(value, VALUE_EMERGENCY_STOP))
            {
                logPrintf(channel, "ISEG Request for emergency stop of the power converter failed \n");
            }

            return 1;
        }
    }
    else // error getting address
    {
        logPrintf(0, "Invalid channel address \n");
        return 1;
    }
}



static int32_t strToUint16(const char *str, uint16_t *res)
{
    char *end;
    errno = 0;

    long val = strtol(str, &end, 10);

    if (errno || end == str || *end != '\0' || val < 0 || val >= 0xffff)
    {
        return 1;
    }

    *res = (uint16_t)val;

    return 0;
}

static int32_t strToUint32(const char *str, uint32_t *res)
{
    char *end;
    errno = 0;

    long val = strtol(str, &end, 10);

    if (errno || end == str || *end != '\0' || val < 0 || val >= 0xffffffff)
    {
        return 1;
    }

    *res = (uint32_t)val;

    return 0;
}

static int32_t strToFloat(const char *str, float *res)
{
    char *end;
    errno = 0;

    float val = strtof(str, &end);

    if (errno || end == str || *end != '\0')
    {
        return 1;
    }

    *res = val;

    return 0;
}


// EOF
