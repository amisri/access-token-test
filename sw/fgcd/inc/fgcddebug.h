/*!
 * @file   fgcddebug.h
 * @brief  Macros for debug printing
 * @author Quentin King
 * Source  https://stackoverflow.com/questions/1644868/define-macro-for-debug-printing-in-c
 *
 * Make with FGCDDEBUG=1 to include fgcddebug print statements in the compiled code.
 */

#ifndef FGCDDEBUG_H
#define FGCDDEBUG_H

#include <stdio.h>

#define fgcddebug(fmt, ...) \
        do { if (FGCDDEBUG) fprintf(stderr, "fgcddebug:%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while (0)

#endif

// EOF
