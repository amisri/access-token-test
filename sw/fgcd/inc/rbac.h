/*!
 * @file   rbac.h
 * @brief  Declarations for interfacing with the Role-Based Access Control system
 * @author Stephen Page
 */

#ifndef RBAC_H
#define RBAC_H

#include <pthread.h>
#include <stdint.h>

// Forward declarations

struct Cmd;
struct TCP_client;

#undef Boolean
#include <cmw-rbac/authentication/Token.h>
#include <cmw-rbac/authorization/AccessManager.h>
#include <cmw-rbac/authorization/AccessMap.h>
#include <cmw-rda3/server/core/SetRequest.h>


/*!
 * Class inheriting from SetRequest.
 * This subclass exists to perform a custom MCS checks.
 */

class FgcdServerRequest : public cmw::rda3::server::SetRequest
{
    public:
        FgcdServerRequest(cmw::rda3::server::SetRequest& originalRequest, std::string propName);

        int64_t getId() const override;
        cmw::rda3::server::Session & getSession() const override;
        const std::string getAccessPointName() const override;
        const std::string & getDeviceName() const override;
        const std::string & getPropertyName() const override;
        const cmw::rda3::common::RequestContext & getContext() const override;

        cmw::data::Data & getData() override;
        void requestCompleted() override;
        void requestFailed(const cmw::rda3::common::ServerException & exception) override;

    private:
        cmw::rda3::server::SetRequest& request;
        std::string name;
};

/*!
 * Struct containing global variables
 */

struct Rbac
{
    int32_t             run;                //!< Flag to indicate that RBAC is initialised
    rbac::AccessManager *rbacOnlyAccess;    //!< RBAC access manager for checking RBAC
    rbac::AccessManager *mcsOnlyAccess;     //!< RBAC access manager for checking MCS
    pthread_mutex_t     mutex;              //!< Mutex to protect access to access map
};

extern struct Rbac rbac_globals;
// External functions

/*!
 * Initialise RBAC
 */

int32_t rbacInit(void);

/*!
 * Read access map
 */

int32_t rbacReadAccessMap(void);

/*!
 * Perform MCS check. It throws RBAC exception on failure.
 */

void rbacValidateMcs(struct Cmd *command, const std::string& property);

/*!
 * Authorise a command
 */

int32_t rbacAuthorise(struct Cmd *command);

/*!
 * Set token for a TCP client
 */

int32_t rbacSetTCPClientToken(struct TCP_client *client, void *token_bin, long token_size);

/*!
 * Clear token for a TCP client
 */

void rbacClearTCPClientToken(struct TCP_client *client);

/*!
 * Clear token for a Command
 */

void rbacClearCommandToken(struct Cmd *cmd);

#endif

// EOF
