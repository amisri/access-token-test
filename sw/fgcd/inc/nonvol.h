/*!
 * @file   nonvol.h
 * @brief  Declarations for non-volatile property storage
 * @author Stephen Page
 */

#ifndef NONVOL_H
#define NONVOL_H

#include <stdint.h>
#include <time.h>

// Constants

#define NONVOL_PATH     FRONT_END_DATA_PATH"/device_config" //!< Path to device configuration files

// Forward declaration

struct FGCD_device;

// External functions

/*!
 * Start non-volatile configuration storage
 */

int32_t nonvolStart(void);

/*!
 * Store a property's value in non-volatile storage
 */

int32_t nonvolStore(struct FGCD_device *device, const char *property, uint32_t user, int32_t last, int32_t is_config);


/*!
 * Load a device configuration
 */

int32_t nonvolLoad(struct FGCD_device *device, int32_t last);

/*!
 * Return the time of the last modification to non-volatile storage.
 *
 * This is required for S CONFIG.STATE RESET, as we set the time of the last reset = time of the last nonvol write.
 * This time is read back from the filesystem rather than the system clock.
 */

time_t nonvolGetModifiedTime(void);

#endif

// EOF
