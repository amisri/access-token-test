/*!
 * @file   transaction.h
 * @brief  Transaction handling
 * @author Stephen Page
 */

#pragma once

#include <stdint.h>
#include <sys/time.h>

#include <cmd.h>
#include <deftypes.h>
#include <fgc_errs.h>

#define TRANSACTION_COMMIT_NUM_EVENTS    128    //!< Number of transaction commit events to pre-allocate

//! Start transaction commit thread

int32_t transactionCommitStart(void);

//! Check property access rules for transactions

uint32_t transactionAccessCheck(struct Cmd *command, bool prop_transactional, uint32_t user_transaction_id);

//! Acknowledge the failure of a transaction commit and reset the transaction state

void transactionAckCommitFail(struct Cmd *command);

//! Send transaction commit event to the transaction commit thread via a queue

void transactionCommitEvent(uint16_t transaction_id, const struct timeval *time);

//! Commit a transaction

fgc_errno transactionCommit(uint16_t transaction_id, uint32_t channel, const struct timeval *time);

//! Get a transaction ID from a command

uint16_t transactionGetIdFromCommand(struct Cmd *command);

//! Rollback a transaction

fgc_errno transactionRollback(uint16_t transaction_id, uint32_t channel);

//! Callback when a transactional set has been performed

void transactionSet(struct Cmd *command, struct prop *property);

//! Set a transaction ID

void transactionSetId(struct Cmd *command, struct prop *property);

//! Test a transaction

fgc_errno transactionTest(uint16_t transaction_id, uint32_t channel);

// EOF
