/*!
 * @file   version.h
 * @brief  Declarations for version information
 * @author Stephen Page
 */

#ifndef VERSION_H
#define VERSION_H

#include <sys/time.h>

/*!
 * Struct containing version information
 */

struct version
{
    struct timeval  time;           //!< Build timestamp
    const char      *hostname;      //!< Host name of machine used for the build
    const char      *architecture;  //!< Architecture of the machine used for the build
    const char      *cc_version;    //!< C compiler version
    const char      *cxx_version;   //!< C++ compiler version
    const char      *user_group;    //!< User and group that performed the build
    const char      *directory;     //!< Build directory
    const char      *ident_info;    //!< Information for ident
};

/*!
 * Declare version information.
 *
 * Defined in version_def.h
 */

extern struct version version;

#endif

// EOF
