/*!
 * @file   parser.h
 * @brief  Declarations for the parser
 * @author Stephen Page
 * @author Michael Davis
 */

#ifndef PARSER_H
#define PARSER_H

#include <stdint.h>

#include <consts.h>
#include <deftypes.h>
#include <hash.h>
#include <queue.h>

// Forward declarations

struct Cmd;
struct prop;

// Constants

#define PARSER_BUF_LEN                      CMD_MAX_VAL_LENGTH * sizeof(uint32_t)   //!< Length of the set buffer
#define PARSER_MAX_PROP_LEVELS              16                                      //!< Maximum number of property levels
#define PARSER_DATA_GETOPT_MAX_ARGS         8                                       //!< Maximum number of DATA get option arguments
#define UNSPECIFIED                         0xFFFFFFFF                              //!< Value to indicate value is unspecified
#define ALL_USERS                           0xFF                                    //!< Value to indicate that all users should be shown

// Types

/*!
 * Callback function type
 */

typedef int32_t Prop_callback(struct Cmd *command, struct prop *property);

/*!
 * Enumeration for use with parserGetParent()
 */

enum Parser_parent_filter
{
    PARSER_ALL,
    PARSER_CONFIG_SET,
    PARSER_CONFIG_UNSET,
};

/*!
 * Structure to contain parser data for a class
 */

struct Parser_class_data
{
    uint32_t             class_id;          //!< FGC class ID
    struct hash_entry   *sym_tab_prop;      //!< Property hash entries
    uint32_t            sym_tab_prop_len;   //!< Property hash lenght
    struct hash_table   *prop_hash;         //!< Hash of properties
    struct hash_entry   *sym_tab_const;     //!< Constant hash entries
    struct hash_table   *const_hash;        //!< Hash of constants
    DEFPROPS_FUNC_GET((**get_func));        //!< Get functions
    DEFPROPS_FUNC_SET((**set_func));        //!< Set functions
    DEFPROPS_FUNC_SETIF((**setif_func));    //!< Setif functions
    DEFPROPS_FUNC_PARS((**pars_func));      //!< Parameter functions
    uint32_t             num_pars_func;     //!< Number of parameter functions
    Prop_callback       *evtlog_func;       //!< Callback hook to log SET events in the Event Log
};

/*!
 * Structure to contain data for a parser instance
 */

struct Parser
{
    int32_t                    run;                                             //!< Flag to indicate that the parser thread should run
    pthread_t                  thread;                                          //!< Handle for the parser thread
    struct Queue               command_queue;                                   //!< Queue of commands for the parser to process
    struct Cmd                 *current_command;                                //!< Command currently being processed
    struct Parser_class_data   *class_data;                                     //!< Parser data for class of destination device
    uint32_t                   getopt_flags;                                    //!< Flags for get options of current get command
    char                       *getopt_data_args[PARSER_DATA_GETOPT_MAX_ARGS];  //!< Arguments for the data get option
    uint32_t                   getopt_num_data_args;                            //!< Number of arguments for the data get option
    uint32_t                   from;                                            //!< From index
    uint32_t                   to;                                              //!< To index
    uint32_t                   step;                                            //!< Array step
    uint32_t                   user;                                            //!< User number for PPM properties
    bool                       is_ppm;                                          //!< True if property is ppm
    char                       set_buffer[PARSER_BUF_LEN];
    uint32_t                   set_index;                                       //!< Index of current element in set range
    uint32_t                   max_set_elements;                                //!< Maximum number of elements to set during a set operation
    uint32_t                   num_set_pads;                                    //!< Number of elements to pad during a set operation
    uint32_t                   num_array_specifiers;                            //!< Number array index specifiers
    uint32_t                   parents[PARSER_MAX_PROP_LEVELS];                 //!< Array of parent property indices
    uint32_t                   parents_index;                                   //!< Index within parents array
    uintptr_t                  *num_elements_ptr;                               //!< Pointer to number of elements of property for command
    char                       *value;                                          //!< Pointer to value of property for command
    struct prop                *cmd_prop;                                       //!< Command property. This is needed to set the label to "value", when the GET command is on a leaf property
    struct timeval             timestamp;                                       //!< Timestamp of the start of command parsing
};



// Inline functions

/*!
 * Set pointer to number of elements for the property of a command
 */

static inline void parserSetNumElsPtr(struct Parser *parser, uintptr_t *num_elements_ptr)
{
    parser->num_elements_ptr = num_elements_ptr;
}



/*!
 * Set number of elements for the property of a command, with protection against emptying a scalar property.
 * This function assumes little-endian CPU, this allows use numels variables with uint32_t or uintptr_t.
 */

static inline void parserSetNumEls(struct Parser *parser, uint32_t num_elements)
{
    if(parser->cmd_prop->max_elements != 1 || num_elements > 0)
    {
        *reinterpret_cast<uint32_t*>(parser->num_elements_ptr) = num_elements;
    }
}



/*!
 * Get number of elements for the property of a command
 * This function assumes little-endian CPU, this allows use numels variables with uint32_t or uintptr_t.
*/

static inline uint32_t parserGetNumEls(struct Parser *parser)
{
    return *reinterpret_cast<uint32_t*>(parser->num_elements_ptr);
}

// External functions

/*!
 * Initialise parser globals
 */

int32_t parserInit(void);

/*!
 * Get parser data for a class
 */

struct Parser_class_data *parserGetClassData(uint32_t class_id);

/*!
 * Gets the full name of a property
 *
 * If the property name is too long for the buffer, a warning will be written to the log
 * and the name will be truncated with a terminating nul in the last element of the
 * prop_name array.
 *
 * @param[in]  property     pointer to the property we want to read
 * @param[in]  class_id
 * @param[out] prop_name    pointer to buffer where property name will be written
 * @param[in]  prop_max_len maximum length of the property name including terminating nul
 *
 * returns length of property name string
 */
uint32_t parserGetPropertyName(struct prop *property, uint32_t class_id, char *prop_name, uint32_t prop_max_len);

/*!
 * Set parser data for a class
 */

int32_t parserSetClassData(struct Parser_class_data *data);

/*!
 * Clear parser data for a class
 */

void parserClearClassData(uint32_t class_id);

/*!
 * Start parser
 */

int32_t parserStart(struct Parser *parser);

/*!
 * Configure parser to access property
 */

int32_t parserConfigProp(struct Parser *parser, struct prop *property);

/*!
 * Prepare for get operation
 */

int32_t parserConfigGet(struct Parser *parser, struct prop *property);

/*!
 * Prepare for set operation
 */

int32_t parserConfigSet(struct Parser *parser, struct prop *property);

/*!
 * Get a parent property while applying a filter.
 *
 * This function is used by get functions that need to descend the property tree
 */

int32_t parserGetParent(struct Cmd *command, struct prop *property, enum Parser_parent_filter filter);

/*!
 * Identify next symbol from command string
 */

uint32_t parserIdentifyNextSymbol(struct Parser *parser, char *c, uint32_t *index, uint32_t length, struct hash_table *hash_table, const char *delimiters, int32_t null_delim, char *delim_match);

/*!
 * Scan command string for an integer fitting defined criteria
 */

void parserScanInt(struct Parser *parser, char *c, uint32_t *index, uint32_t length, const char *delims, int32_t null_delim, char *delim_match, int32_t allow_signed, int32_t check_min, int32_t check_max, int32_t min, int32_t max, int32_t *value);

/*!
 * Scan command string for a float fitting defined criteria
 */

void parserScanFloat(struct Parser *parser, char *c, uint32_t *index, uint32_t length, const char *delims, int32_t null_delim, char *delim_match, int32_t check_min, int32_t check_max, double min, double max, double *value);

/*!
 * Scan command string for a point fitting defined criteria
 */

void parserScanPoint(struct Parser *parser, char *c, uint32_t *index, uint32_t length, const char *delims, int32_t null_delim, char *delim_match, int32_t check_min, int32_t check_max, double min, double max, double *valuex,double *valuey);

/*!
 * Print a property's label
 */

int32_t parserPrintLabel(struct Cmd *command, struct prop *property);

/*!
 * Print a property's type
 */

int32_t parserPrintType(struct Cmd *command, struct prop *property);

/*!
 * Print a property's range
 */

int32_t parserPrintRange(struct Cmd *command, struct prop *property);


/*
 * Increment pointer to number of elements. Required for equipment devices and PPM.
 */

void parserIncNumElsPtr(struct Parser *parser, size_t size, uint32_t index);

/*
 * Retrieve the property structure given the property name
 */

struct prop* parserRetrieveProperty(struct Parser *parser, const char* prop_name);

/*
 * Call property set parameter functions
 */

void parserCallPropSetParsFunctions(uint32_t channel);

#endif

// EOF
