/*!
 * @file   fgcd_threads.h
 * @brief  Functions to start and stop FGCD threads
 * @author Michael Davis
 *
 * Notes from Stephen Page on thread policies and priorities:
 *
 * * cmwRunServer and cmwProcessResponses / cmwpubRunPublish and cmwpubRunRetrieveData: For CMW threads which run at the same
 *   priority and are scheduled with SCHED_FIFO, you could consider whether SCHED_RR is more appropriate, if it is advantageous
 *   that they should be scheduled in parallel.
 *
 * * parserRun: there is potential to have multiple parser threads in some cases, particularly in Class 3 (Mugef) if multiple
 *   equipment device parsers need to run commands in parallel. parserRun is set to SCHED_RR so that it will behave correctly
 *   if needed later.
 *
 * * (Class 1) fipEmptySendFifo and fipEmptyRecvFifo: these two functions trigger the flushing of the FIFOs for WorldFIP
 *   messaging when a new message is sent or received. They are equivalent in priority and it is likely to be better to
 *   let one finish before scheduling the other (on uni-core), though switching them to RR may not have negative consequences.
 *
 * * (Class 2) serialRun: a serialRun thread is started for each connected serial device. Since they are independent and
 *   parallel, SCHED_RR is used.
 *
 * * (Class 2) equipdevRun and parserRun: equipdevRun is a little special for FGCD Class 2. It performs a role near identical
 *   to the parser for the special status equipment device with class 98. It is therefore given equivalent priority.
 */

#ifndef __FGCD_THREAD_H
#define __FGCD_THREAD_H

#include <stdint.h>
#include <pthread.h>



// Constants

static const uint32_t PTHREAD_STACK_SIZE = 1048576;         //!< Stack size for pthread_attr_setstacksize()



// Types

typedef void*(FGCD_thread_func)(void*);                     //!< Type for FGCD thread run functions



/*!
 * Policies for FGCD mutexes.
 *
 * PTHREAD_PRIO_NONE means that thread priority and scheduling are not affected by mutex ownership.
 *
 * PTHREAD_PRIO_INHERIT means that when a thread is blocking a higher-priority thread which is waiting on a lock on this
 * mutex, the thread priority is temporarily raised to the priority of the blocked thread.
 */

enum FGCD_mutex_policy
{
    ALARMS_MUTEX_POLICY   = PTHREAD_PRIO_NONE,               //!< Policy for alarms mutex
    DEVNAME_MUTEX_POLICY  = PTHREAD_PRIO_NONE,               //!< Policy for devname mutex
    RBAC_MUTEX_POLICY     = PTHREAD_PRIO_NONE,               //!< Policy for RBAC mutex
    TCP_MUTEX_POLICY      = PTHREAD_PRIO_NONE,               //!< Policy for TCP mutex

    CHARQ_MUTEX_POLICY    = PTHREAD_PRIO_INHERIT,            //!< Policy for character queue mutexes
    QUEUE_MUTEX_POLICY    = PTHREAD_PRIO_INHERIT,            //!< Policy for queue mutexes
    FIELDBUS_MUTEX_POLICY = PTHREAD_PRIO_INHERIT,            //!< Policy for fieldbus event mutex
    CMW_MUTEX_POLICY      = PTHREAD_PRIO_INHERIT,            //!< Policy for CMW mutexes
    FGCD_MUTEX_POLICY     = PTHREAD_PRIO_INHERIT,            //!< Policy for FGCD device parameter mutexes
    RT_MUTEX_POLICY       = PTHREAD_PRIO_INHERIT,            //!< Policy for real-time data mutex
    TIMING_MUTEX_POLICY   = PTHREAD_PRIO_INHERIT,            //!< Policy for timing mutex

    // Class 1 only

    FIP_MUTEX_POLICY      = PTHREAD_PRIO_NONE,               //!< Policy for FIP mutexes

    // Class 2 only

    SUB_MUTEX_POLICY      = PTHREAD_PRIO_NONE,               //!< Policy for data subscription mutex

    // Class 3 & Class 8

    EQUIPDEV_MUTEX_POLICY = PTHREAD_PRIO_INHERIT,            //!< Policy for equipdev armed function generator parameter mutex
    STATE_MUTEX_POLICY    = PTHREAD_PRIO_INHERIT,            //!< Policy for state command request mutex

    // Class 6 only

    FGCLITE_MUTEX_POLICY  = PTHREAD_PRIO_INHERIT             //!< Policy for FGClite status mutexes
};



/*!
 * Policies for FGCD threads.
 *
 * SCHED_FIFO is the real-time FIFO scheduler. Each thread runs until it is blocked, then it yields to the next
 * thread at the same priority. If there are no runnable threads at that priority, it yields to a lower-priority
 * thread.
 *
 * SCHED_RR is the real-time Round Robin scheduler. Each thread runs until it is blocked or reaches the end of
 * its time-slice before yielding as for SCHED_FIFO.
 *
 * A runnable higher-priority thread will always pre-empt a thread running at lower priority.
 *
 * Where there is only one thread running at a given priority (most threads in FGCD), SCHED_FIFO and SCHED_RR are
 * equivalent. Therefore, the policy is important only for the following threads:
 *
 * * CMW publishing and CMW retrieve data run at the same priority
 * * CMW server and CMW process responses run at the same priority
 * * TCP send/receive/listen threads run at the same priority
 * * The equipdev thread and parser threads run at the same priority, and there can be multiple concurrent parser threads
 * * (Class 1 only) The threads for emptying the FIP Send and Receive FIFOs run at the same priority
 * * (Class 5 only) There can be multiple concurrent serial threads
 */

enum FGCD_thread_policy
{
    LOG_THREAD_POLICY                   = SCHED_FIFO,        //!< Policy for logging thread
    WATCHDOG_THREAD_POLICY              = SCHED_FIFO,        //!< Policy for watchdog thread
    FGCD_THREAD_POLICY                  = SCHED_FIFO,        //!< Policy for FGCD thread
    CMW_RECOVER_SUBS_THREAD_POLICY      = SCHED_FIFO,        //!< Policy for CMW subscription recovery thread
    CMW_PUBLISH_THREAD_POLICY           = SCHED_FIFO,        //!< Policy for CMW publishing thread
    CMW_RETRIEVE_DATA_THREAD_POLICY     = SCHED_FIFO,        //!< Policy for CMW retrieve data thread
    CMW_SERVER_THREAD_POLICY            = SCHED_FIFO,        //!< Policy for CMW server thread
    CMW_PROCESS_RESPONSES_THREAD_POLICY = SCHED_FIFO,        //!< Policy for CMW process responses thread
    TCP_CLIENT_THREAD_POLICY            = SCHED_RR,          //!< Policy for TCP client (send/receive) threads
    TCP_LISTEN_THREAD_POLICY            = SCHED_RR,          //!< Policy for TCP listen thread
    NONVOL_THREAD_POLICY                = SCHED_FIFO,        //!< Policy for non-volatile storage thread
    TRANSACTION_COMMIT_THREAD_POLICY    = SCHED_FIFO,        //!< Policy for transaction commit thread
    PARSER_THREAD_POLICY                = SCHED_RR,          //!< Policy for parser threads.
    EQUIPDEV_THREAD_POLICY              = SCHED_RR,          //!< Policy for equipment device thread
    ALARMS_THREAD_POLICY                = SCHED_FIFO,        //!< Policy for alarms thread
    SUB_THREAD_POLICY                   = SCHED_FIFO,        //!< Policy for data subscription thread
    PM_THREAD_POLICY                    = SCHED_FIFO,        //!< Policy for post-mortem thread
    PUB_THREAD_POLICY                   = SCHED_FIFO,        //!< Policy for data publishing thread
    RT_THREAD_POLICY                    = SCHED_FIFO,        //!< Policy for real-time data thread
    ETHER_RECEIVE_THREAD_POLICY         = SCHED_FIFO,        //!< Policy for ether receive thread
    SERIAL_THREAD_POLICY                = SCHED_RR,          //!< Policy for serial communications threads
    FGCLITE_CYCLE_THREAD_POLICY         = SCHED_FIFO,        //!< Policy for the FGClite cycle handling thread
    TIMING_THREAD_POLICY                = SCHED_FIFO,        //!< Policy for timing thread
    FIP_FIFO_EMPTY_THREAD_POLICY        = SCHED_FIFO,        //!< Policy for FIP FIFO emptying threads
    ISEG_CYCLE_THREAD_POLICY            = SCHED_FIFO,        //!< Policy for the ISEG cycle handling thread
};



/*!
 * Priorities for FGCD threads.
 *
 * Normal user-space threads run with priority 0. Real-time threads have priority 1-99.
 * A runnable higher-priority thread will always pre-empt a thread running at lower priority.
 *
 * Additional information from Nicolas De Metz-Noblat <Nicolas.de.Metz-Noblat@cern.ch> (15 April 2015):
 *
 * System  threads servicing  Interrupt Service Routines are created at priority 50 using FIFO scheduling
 * and do not use any priority tracking. With some kernels, I also observed some system threads running at
 * priority 49.
 *
 * ps command to see priorities of all threads:
 *
 * # ps -eLo user,pid,tid,cls,rtprio,vsz,rss,tty,stat,start,bsdtime,args
 *
 * This is rather long, so there is an alias "psx"
 */

enum FGCD_priority
{
    LOG_THREAD_PRIORITY             = 36,          //!< Priority of logging thread
    WATCHDOG_THREAD_PRIORITY        = 37,          //!< Priority of watchdog thread
    FGCD_THREAD_PRIORITY            = 38,          //!< Priority of the FGCD process
    CMW_THREAD_PRIORITY_MIN         = 39,          //!< Minimum CMW priority. Used by CMW subscription recovery thread.
    CMW_THREAD_PRIORITY_NORM        = 40,          //!<  Normal CMW priority. Used by CMW publishing thread and CMW retrieve data thread.
    CMW_THREAD_PRIORITY_MAX         = 41,          //!< Maximum CMW priority. Used by CMW server thread and CMW process responses thread.
    TCP_CLIENT_THREAD_PRIORITY      = 42,          //!< Priority of TCP client (send/receive) threads
    TCP_LISTEN_THREAD_PRIORITY      = 42,          //!< Priority of TCP listen thread
    NONVOL_THREAD_PRIORITY          = 43,          //!< Priority of non-volatile property storage thread
    PARSER_THREAD_PRIORITY          = 44,          //!< Priority of parser threads
    EQUIPDEV_THREAD_PRIORITY        = 44,          //!< Priority of equipment device thread
    ALARMS_THREAD_PRIORITY          = 45,          //!< Priority of alarm submission thread
    SUB_THREAD_PRIORITY             = 46,          //!< Priority of data subscription thread
    PM_THREAD_PRIORITY              = 47,          //!< Priority of post-mortem thread
    PUB_THREAD_PRIORITY             = 48,          //!< Priority of data publishing thread
    RT_THREAD_PRIORITY              = 49,          //!< Priority of real-time data thread

    // Note: The Linux kernel uses priority 50 for kernel tasklets and interrupt handler by default.
    //       We use the Linux RT patches, so threads with priority > 50 can pre-empt kernel threads.

    ETHER_RECEIVE_THREAD_PRIORITY   = 57,          //!< Priority of ether receive thread
    SERIAL_THREAD_PRIORITY          = 57,          //!< Priority for serial communications threads
    ISEG_CYCLE_THREAD_PRIORITY      = 57,          //!< Priority for the ISEG cycle handling thread
    TRANSACTION_COMMIT_THREAD_PRIORITY = 57,       //!< Priority for the transaction commit handling thread

    TIMING_THREAD_PRIORITY          = 58,          //!< Priority of timing thread

    FGCLITE_CYCLE_THREAD_PRIORITY   = 59,          //!< Priority of the FGClite cycle handling thread

    FIP_FIFO_EMPTY_THREAD_PRIORITY  = 64,          //!< Priority of FIP FIFO emptying threads
    FIP_FDM_THREAD_PRIORITY         = 65           //!< Base priority for real-time FIP FDM threads
};

/*!
 *  Afinities of the threads.
 *
 *  By default the affinity is not set
 */

enum FGCD_affinity
{
    TIMING_THREAD_AFFINITY          = 0,          //!< Affinity of the timing thread (only needed here)
    FDM_THREAD_AFFINITY             = 0,          //!< Affinity of the FDM thread
    NO_AFFINITY                     = 64
};

/*!
 * Initialise a mutex with the specified policy.
 *
 * @param[out]  mutex    The newly initialised mutex
 * @param[in]   policy   The policy for thread priorities to use for mutex initialisation
 *
 * @retval 0    The mutex was initialised successfully
 * @retval 1    Mutex initialisation failed
 */

int32_t fgcd_mutex_init(pthread_mutex_t *mutex, enum FGCD_mutex_policy policy);

/*!
 * Set thread policy and priority, then launch the thread.
 *
 * @param[out]  thread_id       On success, set to the thread ID of the newly created thread
 * @param[in]   run_function    Pointer to the function to run in this thread
 * @param[in]   arg             Pointer to the argument for the run function (set to NULL for no argument)
 * @param[in]   policy          Thread policy, #SCHED_RR or #SCHED_FIFO
 * @param[in]   priority        Thread priority
 *
 * @retval 0    The thread was created successfully
 * @retval 1    Thread creation failed
 */

int32_t fgcd_thread_create(pthread_t *thread_id, FGCD_thread_func *run_function, void *arg, enum FGCD_thread_policy policy, enum FGCD_priority priority, enum FGCD_affinity affinity);

#endif

// EOF
