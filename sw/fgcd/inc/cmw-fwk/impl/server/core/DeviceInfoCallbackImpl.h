#ifndef CMW_FWK_DEVICE_INFO_CALLBACK_H_INCLUDED
#define CMW_FWK_DEVICE_INFO_CALLBACK_H_INCLUDED

#include <cmw-rda3/server/core/DeviceInfoCallback.h>
#include <cmw-rda3/common/util/AccessPointUtils.h>

#include <string>
 
namespace cmw
{

/**
 * Callback provides class name for the requested device name. It returns the RDA3 admin class
 * for the RDA3 admin device. A valid user callback is mandatory and must be provided.
 *
 * @author Joel Lauener
 */
class DeviceInfoCallbackImpl : public rda3::server::DeviceInfoCallback
{
public:

    static std::string ADMIN_CLASS;

    DeviceInfoCallbackImpl(rda3::server::DeviceInfoCallback &callback) :
                    callback_(callback)
    {
    }

    /**
     * Return class name for given device name;
     *
     * @param deviceName
     * @return className
     */
    std::string getDeviceClass(const std::string & deviceName) override
    {
        if (rda3::common::AccessPointUtils::isAdminDevice(deviceName))
        {
            return ADMIN_CLASS;
        }
        else
        {
            return callback_.getDeviceClass(deviceName);
        }
    }

    virtual ~DeviceInfoCallbackImpl()
    {
    }

private:
    /** Hidden assignment operator */
    void operator =(const DeviceInfoCallback &);

    rda3::server::DeviceInfoCallback & callback_;
};

}
#endif

