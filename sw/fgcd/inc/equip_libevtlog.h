//! @file   equip_libevtlog.h
//! @brief  Interface to libevtlog
//!
//! Function declarations for initialising the CCLIBS evtlog manager structures


#ifndef EQUIP_LIBEVTLOG_H
#define EQUIP_LIBEVTLOG_H

#include <stdint.h>
#include <sys/time.h>
#include <libevtlog.h>
#include <evtlog_class.h>

// Forward declarations

struct Cmd;
struct prop;

//! Initialize CCLIBS event log structures and their value.
//!
//! @param[in]    channel    FIP address of the FGClite device (1 to 30)
//!
//! @returns      0 on success, non-zero on failure

int32_t ccEvtlogInit(uint32_t channel);


//! Store property changes in the event log
//!
//! @param[in]    channel    FIP address of the FGClite device (1 to 30)

void ccEvtlogStoreProperties(uint32_t channel);


//! Manually store a record in the event log
//!
//! @param[in]    channel      Address of the device
//! @param[in]    timestamp    Timestamp of the event, with us resolution
//! @param[in]    property     Property name
//! @param[in]    value        Property value
//! @param[in]    action       In the case of a SET command, this takes the form CMW.nnn, TCP.nnn or
//!                            SERIAL.nnn, where nnn is the error code returned by the SET command (0
//!                            for success).
//! @param[in]    status       Record status character

void ccEvtlogStoreRecord(uint32_t channel, const struct timeval *timestamp, const char *property, const char *value, const char *action, const char status);


//! Manually store a timing event
//! @param[in]    channel        Address of the device
//! @param[in]    timestamp      Timestamp of the event, with us resolution
//! @param[in]    event_name     Name of the timing event
//! @param[in]    cycle_selector cycle selector when the event happened

void ccEvtLogStoreTimingEvent(uint32_t channel, const struct timeval *timestamp, const char *event_name, uint32_t cycle_selector);


//! Manually store a record in the event log
//!
//! @param[in]    command     The FGCD command structure received by CMW or TCP
//! @param[in]    property    Not used
//!
//! @retval       0           This function always returns 0

int32_t ccEvtlogStoreSetCmd(struct Cmd *command, struct prop *property);


//! Get the contents of the Event log in FGC format
//!
//! @param[in]    channel       Address of the device
//! @param[in]    is_bin        If true, return data in binary format. If false, return data in ASCII format.
//! @param[in]    buffer        Buffer to write the Event log to
//! @param[in]    max_buflen    Size of buffer in bytes
//!
//! @returns      Number of bytes written to buffer (0 in case max_buflen is too short)

size_t ccEvtlogGetLogEvt(uint32_t channel, bool is_bin, char *buffer, size_t max_buflen);


//! Get function of the event log

int32_t GetLogEvt(struct Cmd *command, struct prop *property);


//! Event log callback function declarations

EVTLOG_changed_callback ccEvtlogChangedFunction;
EVTLOG_lock_callback    ccEvtlogLockFunction;
EVTLOG_lock_callback    ccEvtlogUnlockFunction;

#endif
