#pragma once

#include <chrono>

using namespace std::chrono_literals;

template <class T = std::chrono::high_resolution_clock>
class Timer
{
    public:
        //! Starts time measurement.
        //!
        //! @remarks Not thread-safe.
        void start()
        {
            m_start_point = T::now();
            m_is_started  = true;
        }

        // **********************************************************

        //! Stops time measurement.
        //!
        //! @remarks Not thread-safe.
        void stop()
        {
            if (m_is_started)
            {
                m_stop_point = T::now();
                m_is_started = false;
            }
        }

        // **********************************************************

        //! Returns measured duration as nanoseconds. If the timer has been started and stopped the duration will
        //! represent time between start() and stop(). If the timer has been started and not yet stopped the duration
        //! will represent time between start() and the current moment. If the timer has never been started, the duration
        //! will be equal to 0.
        //!
        //! @return Duration measured by the timer.
        //!
        //! @remarks Thread-safe.
        std::chrono::nanoseconds get()
        {
            if (m_is_started)
            {
                return T::now() - m_start_point;
            }
            else
            {
                return m_stop_point - m_start_point;
            }
        }

    protected:
        bool                   m_is_started = false;           //!< Flag that marks whether the timer is started but not stopped.
        typename T::time_point m_start_point;                  //!< Time point representing start() moment.
        typename T::time_point m_stop_point;                   //!< Time point representing stop() moment.
};