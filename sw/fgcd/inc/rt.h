/*!
 * @file   rt.h
 * @brief  Declarations for receiving real-time data
 * @author Stephen Page
 */

#ifndef RT_H
#define RT_H

#include <pthread.h>
#include <stdint.h>

#include <defconst.h>
#include <fgc_consts_gen.h>

// Constants

#define RT_BUFFER_SIZE      (FGC_RT_BUFFER_MS / FGCD_CYCLE_PERIOD_MS)   //!< Number of elements in RT buffer
#define RT_MAX_VALUE        30000                                       //!< Maximum value to accept for real-time reference values
#define RT_UDP_PORT_NUM     FGC_GW_RT_PORT                              //!< UDP port for real-time data thread

#include <fgc_rtdata.h>

/*!
 * Struct containing global variables
 */

struct Rt
{
    pthread_t               thread;                         //!< Thread handle
    int                     sock;                           //!< Socket to receive data
    uint32_t                udp_load;                       //!< Incoming UDP load in bytes per second
    uint32_t                bad_packet_count;               //!< Count of bad packets received
    uint32_t                bad_size_packet_count;          //!< Count of packets received of wrong size
    uint32_t                early_packet_count;             //!< Count of packets received too early to apply
    uint32_t                good_packet_count;              //!< Count of good packets received
    uint32_t                late_packet_count;              //!< Count of packets received too late to apply
    uint32_t                packet_count;                   //!< Count of packets received
    uint32_t                bad_value_count;                //!< Count of bad values received
    uint32_t                net_latency[FGC_RT_BUFFER_MS];  //!< Histogram of network latency in milliseconds
    uint32_t                hold_values;                    //!< Flag to indicate that existing values should be held and new values ignored
    pthread_mutex_t         mutex;                          //!< Mutex to protect real-time data
    struct fgc_udp_rt_data  data[RT_BUFFER_SIZE];           //!< Real-time data
    uint32_t                data_start;                     //!< Real-time data start index
    struct fgc_udp_rt_data  used_data;                      //!< Last real-time data that was used
};

extern struct Rt rt;

// External functions

/*!
 * Start the real-time reception thread
 */

int32_t rtStart(void);

#endif

// EOF
