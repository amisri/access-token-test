/*!
 * @file   timing.h
 * @brief  Declarations for timing using the timdt library
 * @author Marc Magrans de Abril
 */

#ifndef TIMING_H
#define TIMING_H

#undef Boolean
#include <err/err.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>

#include <timdt-lib-cpp/Timing.h>

extern "C"
{
    #include <ctr/libctr.h>
}

#include <consts.h>
#include <fgc_consts_gen.h>
#include <hash.h>
#include <timing_handlers.h>

// Constants

#ifndef TIMING_DEFAULT_SOURCE
#define TIMING_DEFAULT_SOURCE           TIMING_TIMLIB                           //!< Default timing source
#endif

#define NUM_CYC_SELECTORS               FGC_TIMING_NUM_USERS+1                  //!< Cycle selectors: 1 per user + slot for user 0

#define TIMING_CLASS_EVENT_PATH         CONFIG_PATH"/timing/timdt/class"        //!< Path containing a file per machine per class for class-specific timing events
#define TIMING_HOST_EVENT_PATH          CONFIG_PATH"/timing/timdt/host"         //!< Path containing file with local timing events

#define TIMING_MACHINE_MODE_FULLECO     2                                       //!< Interpretation of the MACHINE_MODE timing field

#define US_PER_S                        1000000
#define NS_PER_MS                       1000000
#define NS_PER_US                       1000
#define US_PER_MS                       1000

/*!
 * Enumeration to indicate the active timing source
 */

enum Timing_source
{
    TIMING_SYS,
    TIMING_TIMLIB
};


/*!
 * Struct containing global variables
 */

struct TimingFgcd
{
    pthread_t                        thread;                                             //!< Thread handle
    void                            *ctr_handle;                                         //!< Handle for the CTR
    Timing::ConnectionContext       *connection;                                         //!< Timing connection
    Timing::Domain                  *domain;                                             //!< Timing domain
    const char                      *domain_str;                                         //!< Timing domain string
    uint32_t                         telegram_group[TIMING_NUM_FIELDS];                  //!< Mapping between telegram groups and internal group codes
    uint32_t                         field_value[TIMING_NUM_FIELDS];                     //!< Telegram group values
    enum Timing_source               source;                                             //!< Source of timing
    uint32_t                         ctr_status;                                         //!< Status of timing receiver as reported by timlib
    uint32_t                         cycle_ms;                                           //!< Millisecond number within current cycle
    uint32_t                         user;                                               //!< Current user
    uint32_t                         next_user;                                          //!< Next user
    uint32_t                         prev_user;                                          //!< Previous user
    uint8_t                          next_ms_user;                                       //!< User (for next millisecond)
    uint8_t                          cycle_num;                                          //!< Number of the cycle within the supercycle
    uint32_t                         next_ms_cycle_ms;                                   //!< Millisecond number within cycle (for next millisecond)
    uint32_t                         next_cycle_countdown_ms;                            //!< Number of milliseconds until next cycle
    uint32_t                         next_fei_countdown_ms;                              //!< Number of milliseconds until next FEI event
    struct hash_table               *user_line_hash;                                     //!< Hash of user lines
    struct hash_entry                user_line_entry[FGC_TIMING_NUM_USERS];              //!< User line hash entries
    struct timeval                   user_time[1 + FGC_TIMING_NUM_USERS];                //!< Timestamp of last cycle occurrence of each user
    struct timeval                   last_user_time[1 + FGC_TIMING_NUM_USERS];           //!< Timestamp of last occurrence of each user
    pthread_mutex_t                  user_time_mutex;                                    //!< Mutex protecting access to time
    uint32_t                         log_events_flag;                                    //!< Flag to indicate whether timing event reception should be logged
    timer_t                          posix_timer;                                        //!< Posix timer to use to generate events when no other time source is present
    uint32_t                         posix_timer_in_use;                                 //!< Flag to indicate that POSIX timer is in use
};

extern struct TimingFgcd timing;

// External functions

/*!
 * Initialise timing
 */

int32_t timingInit(void);

/*!
 * Start the timing thread
 */

int32_t timingStart(void);

/*!
 * Read timing event files
 */

int32_t timingSubscribeEvents(void);

/*!
 * Get the time for a user.
 *
 * User 0 is the current FGCD iteration time.
 */

void timingGetUserTime(uint32_t user, struct timeval *time);

/*!
 * Set the time for a user.
 *
 * User 0 is the current FGCD iteration time.
 */

void timingSetUserTime(uint32_t user, const struct timeval *time);

/*!
 * Get the last seen time for a user.
 */

void timingGetLastUserTime(uint32_t user, struct timeval *time);

/*!
 * Set the last seen time for a user.
 */

void timingSetLastUserTime(uint32_t user, const struct timeval *time);

/*!
 * Read the current time from the time source
 */

int32_t timingReadTime(struct timeval *time);

/*!
 * Check status of timing receiver
 */

int32_t timingCheckStatus(void);

/*!
 * Receive timing events from timing receiver
 */

int32_t timingReceiveEvents(void);

/*!
 * FGCD cycle generator for use when there is no other hardware time source
 */

int32_t timingStartFGCDCycleTimer(void);

/*!
 * Millisecond tick generator for use when there is no other hardware time source
 */

int32_t timingStartMSTimer(void);

/*!
 * Stop timer used to generate millisecond tick
 */

void timingStopMSTimer(void);

/*!
 * Receive a single timing event
 */

int32_t timingReceiveEvent(void);

/*!
 * Compare two timevals
 */

struct timeval timingCompareTimevals(struct timeval *a, struct timeval *b);

/*!
 * Compare two timespec
 */

struct timespec timingCompareTimespecs(struct timespec *a, struct timespec *b);

/*!
 * Get user name for number.
 *
 * @returns User name. Note: the string returned must be freed by the caller.
 */

const char *timingUserName(uint32_t user_num);

/*!
 * Get user number for string
 */

uint32_t timingUserNumber(const char *user);

/*!
 * Get number of users.
 */

uint8_t timingUsersCount();

/*!
 * Get cycle stamp from time using timing library's telegram history.
 *
 * This differs from timingGetUserTime() which returns the timestamp of the last instance of a user from
 * FGCD's internal cycle calculations.
 */

uint32_t timingGetCycleTimestamp(struct timeval *time, struct timeval *cycle_time);

/*!
 * Set a CTR output to either 0 or 1
 *
 * @params out   the output number of the CTR card (TimLibLemoOUT_1, TimLibLemoOUT_2, etc.)
 * @params value the TTL output (either 0 or 1)
 */

void timingSetCtrOuput(uint32_t out,uint32_t value);

// Inline functions

/*!
 * Increment or decrement a timeval by another timeval.
 *
 * This function is declared inline as it is used within FIP callbacks, which can pre-empt the
 * kernel, so we would like to keep the function in the same compilation unit to avoid the overhead
 * of a function call.
 *
 * tv_second is added to tv_first, adjusting tv_sec and tv_usec if we cross a second boundary. If
 * tv_second is negative, it is subtracted; however, tv_first->tv_sec must be greater than
 * -(tv_second->tv_sec), as the number of seconds is unsigned. The number of microseconds is
 * signed.
 *
 * @param[in,out]    tv_first     timeval to increment or decrement
 * @param[in]        tv_second    timeval to add to tv_first. Can be negative.
 */

inline void timevalAdd(struct timeval *tv_first, const struct timeval* const tv_second)
{
    tv_first->tv_sec  += tv_second->tv_sec;
    tv_first->tv_usec += tv_second->tv_usec;

    // Check if we crossed a second boundary

    if(tv_first->tv_usec > 999999)
    {
        tv_first->tv_sec  += tv_first->tv_usec / 1000000;
        tv_first->tv_usec %= 1000000;
    }

    // Ensure usec field is positive after a subtraction

    while(tv_first->tv_usec < 0)
    {
        tv_first->tv_sec  -= 1;
        tv_first->tv_usec += 1000000;
    }
}

/*!
 * Compare two timevals and return true if the first one is less than the second one.
 *
 * @param[in,out]    tv_first     first timeval to compare
 * @param[in]        tv_second    second timeval to compare
 *
 * @returns true if tv_first < tv_second
 */

inline bool timevalIsLessThan(const struct timeval* const tv_first, const struct timeval* const tv_second)
{
    return tv_first->tv_sec < tv_second->tv_sec ||
          (tv_first->tv_sec == tv_second->tv_sec && tv_first->tv_usec < tv_second->tv_usec);
}


/*!
 * Convert the event time to a timeval with round down to the nearest millisecond
 *
 * @param[in]   evt_time                event time structure
 *
 * @returns     timeval structure containing event time rounded to the millisecond
 */

inline struct timeval evtTimeToTimeval(tTimingTime evt_time)
{
    struct timeval time =
    {
        .tv_sec  = evt_time.time.tv_sec,
        .tv_usec = (evt_time.time.tv_nsec / NS_PER_MS) * US_PER_MS
    };

    return time;
}

#endif

// EOF
