/*!
 * @file   fgcsm.h
 * @brief  Declarations for state machines for communicating with FGCs over fieldbuses
 * @author Stephen Page
 */

#ifndef FGCSM_H
#define FGCSM_H

#include <stdint.h>
#include <sys/time.h>

#include <consts.h>
#include <defconst.h>
#include <queue.h>

// Forward declarations

struct Cmd;
struct fgc_fieldbus_rsp;
struct fgc_fieldbus_cmd;

// Constants

#define FGCSM_MAX_EXEC_TIME_SECS    120  //!< Maximum amount of time to wait for a command to execute


/*!
 * States of command responses
 */

enum FGC_rsp_state
{
    FGC_RSP_START,      //!< Start of command response reception
    FGC_RSP_SUB_ID,     //!< Receive subscription ID for subscribe commands
    FGC_RSP_TIME,       //!< Receive timestamp
    FGC_RSP_VALUE,      //!< Receive value
    FGC_RSP_END,        //!< End command reception
    FGC_RSP_ERROR,      //!< An error occurred receiving a command response
};

/*!
 * States of published data reception
 */

enum FGC_pub_state
{   
    FGC_PUB_START,      //!< Start of published data reception
    FGC_PUB_SUB_ID,     //!< Receive subscription ID
    FGC_PUB_SUB_FLAGS,  //!< Receive subscription flags
    FGC_PUB_USER,       //!< Receive user 
    FGC_PUB_TIME,       //!< Receive timestamp
    FGC_PUB_VALUE,      //!< Receive value
    FGC_PUB_END,        //!< End published data reception
};

/*!
 * FGC subscription data
 */

struct FGC_sub_data
{   
    char        property[FGC_MAX_PROP_LEN + 1];         //!< Property string for the subscription
    uint32_t    error;                                  //!< FGC error to be returned to subscription
};

/*!
 * FGC time structure
 */

struct FGC_time
{
    uint32_t    tv_sec;
    uint32_t    tv_usec;
};

//! Channel data

struct FGCSM_channel
{
    //! Counts

    struct FGCSM_channel_count
    {
        uint32_t    ack_miss;                       //!< Number of message acknowledgements missed
        uint32_t    offline_transition;             //!< Number of times device has gone offline
        uint32_t    proto_error;                    //!< Number of protocol errors
        uint32_t    rd_miss;                        //!< Number of real-time messages missed
        uint32_t    seq_status_miss;                //!< Number of sequential status messages missed
        uint32_t    seq_toggle_bit_miss;            //!< Number of sequential toggle bits missed
        uint32_t    status_rec;                     //!< Number of status messages received
        uint32_t    status_miss;                    //!< Number of status messages missed
        uint32_t    time_miss;                      //!< Number of time messages missed
        uint32_t    unlock_transition;              //!< Number of times device has unlocked its PLL
    } count;

    struct Queue            command_queue;          //!< Command queue

    struct Cmd              *current_command;       //!< Command currently being received
    struct timeval          current_cmd_exec_time;  //!< Time at which the current command started executing
    struct FGC_time         current_cmd_acq_time;   //!< Acquisition time of command currently being received
    struct Cmd              *current_pub_data;      //!< Published data command currently being received
    struct FGC_time         current_pub_acq_time;   //!< Acquisition time of published data currently being received

    uint32_t                cmd_state;              //!< State of FGC command state machine
    enum FGC_pub_state      pub_state;              //!< State of published data reception
    enum FGC_rsp_state      rsp_state;              //!< State of response reception
    struct FGC_sub_data     sub_data[FGC_MAX_SUBS]; //!< Subscription data

    uint8_t                 new_sub_id;             //!< Buffer for receiving IDs for new subscriptions
    uint8_t                 pub_sub_id;             //!< Buffer for receiving IDs for existing subscriptions
    uint32_t                prev_pub_seq;           //!< Sequence number of previous publish message received
    uint32_t                prev_rsp_seq;           //!< Sequence number of previous response received
    int32_t                 prev_status_seq;        //!< Sequence number of previous status variable received
    int32_t                 prev_toggle_bit;        //!< Previous toggle bit status
    int32_t                 status_received;        //!< Flag for status variable reception
};

/*!
 * Struct containing global variables
 */

struct FGCSM
{
    //! Counts

    struct FGCSM_count
    {
        uint32_t    ack_miss;                           //!< Message acknowledgements missed
        uint32_t    offline_transition;                 //!< Number of times devices have gone offline
        uint32_t    rd_miss;                            //!< Real-time messages missed
        uint32_t    time_miss;                          //!< Time messages missed
        uint32_t    unlock_transition;                  //!< Number of times devices have unlocked their PLLs
    } count;

    struct FGCSM_channel channel[FGCD_MAX_DEVS];        //!< Channel data
};

extern struct FGCSM fgcsm;

// External functions

/*!
 * Initialise FGC state machine
 */

int32_t fgcsmInit(void);

/*!
 * Process received status
 */

void fgcsmProcessStatus(uint32_t channel_num);

/*!
 * Prepare a command message to be sent
 */

uint32_t fgcsmPrepareCommand(uint32_t channel_num, struct Cmd *cmd, struct fgc_fieldbus_cmd *cmd_msg);

/*!
 * Handle the reception of a command response message
 */

void fgcsmProcessResponse(  uint32_t channel_num, struct fgc_fieldbus_rsp *rsp_msg, uint32_t msg_data_len);

/*!
 * Handle the reception of a published data message
 */

void fgcsmProcessPublishMsg(uint32_t channel_num, struct fgc_fieldbus_rsp *rsp_msg, uint32_t msg_data_len);

/*!
 * Callback for when a command packet has been queued for fieldbus transmission
 */

void fgcsmCmdQueued(uint32_t channel_num);

/*!
 * Callback for when a command packet has been sent on the fieldbus
 */

void fgcsmCmdSent(uint32_t channel_num);

/*!
 * Check status of all FGCs after they should have been received
 */

void fgcsmCheckStatus(void);

#endif

// EOF
