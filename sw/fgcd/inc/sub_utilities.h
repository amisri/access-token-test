/*!
 * @file   sub_utilities.h
 * @brief  SUB property decoding utitlities
 * @author Miguel Hermo Serrans, Marc Magarns de Abril
 */

#ifndef SUB_UTILITIES_H
#define SUB_UTILITIES_H

#include <deftypes.h>
#include <stdint.h>
#include <label.h>

/*
 * Network to host order for floats
 */

float subUtilitiesNtohf(float value);

/*
 * Network to host order for int16_t
 */

int16_t subUtilitiesSignedNtohs(int16_t value);

/*
 * Network to host order for int32_t
 */

int32_t subUtilitiesSignedNtohs(int32_t value);

/*
 * Translates FGC enumeration properties to text using the mapping provided by 'status_map' 
 */

void  subUtilitiesEnumToText(uint16_t state, CONST sym_name *status_map, char *message, uint8_t max_message_len);

/*
 * Translates FGC bit mask properties to PMD compliant text using the mapping provided by 'status_map' 
 */

void subUtilitiesPmdBitmaskToText(uint16_t status, CONST sym_name *status_map, char *message, uint8_t max_message_len);

/*
 * Translates FGC enumeration properties to PMD compliant text using the mapping provided by 'status_map' 
 */

void  subUtilitiesPmdEnumToText(uint16_t state, CONST sym_name *status_map, char *message, uint8_t max_message_len);

/*
 * Translates FGC bit mask properties to text using the mapping provided by 'status_map' 
 */

void subUtilitiesBitmaskToText(uint16_t status, CONST sym_name *status_map, char *message, uint8_t max_message_len);

#endif