/*!
 * @file   prop.h
 * @brief  Declarations for fgcd properties
 * @author Stephen Page
 */

#ifndef PROP_H
#define PROP_H

#include <stdint.h>

#include <defconst.h>

// Forward declarations

struct Cmd;

/*!
 * Struct containing global variables
 */

struct Prop_globals
{
    uint8_t rbac_token_buf[FGC_RBAC_MAX_TOKEN_SIZE];    //!< Buffer for binary set of RBAC token (note that this must be aligned to a long word boundary)
};

extern struct Prop_globals prop;

// External functions

/*!
 * Get an absolute time property
 */

int32_t GetAbsTime(struct Cmd *command, struct prop *property);

/*!
 * Set an absolute time
 */

int32_t SetAbsTime(struct Cmd *command, struct prop *property);

/*!
 * Get a binary property
 */

int32_t GetBin(struct Cmd *command, struct prop *property);

/*!
 * Set a binary property
 */

int32_t SetBin(struct Cmd *command, struct prop *property);

/*!
 * Get a character property
 */

int32_t GetChar(struct Cmd *command, struct prop *property);

/*!
 * Set a character property
 */

int32_t SetChar(struct Cmd *command, struct prop *property);

/*!
 * Get a list of configuration properties that have been set
 */

int32_t GetConfigSet(struct Cmd *command, struct prop *property);

/*!
 * Get a list of configuration properties that have not been set
 */

int32_t GetConfigUnset(struct Cmd *command, struct prop *property);

/*!
 * Set configuration
 */

int32_t SetConfig(struct Cmd *command, struct prop *property);

/*!
 * Get an elapsed_time property
 */

int32_t GetElapsedTime(struct Cmd *command, struct prop *property);

/*!
 * Get a floating point property
 */

int32_t GetFloat(struct Cmd *command, struct prop *property);

/*!
 * Set a floating point property
 */

int32_t SetFloat(struct Cmd *command, struct prop *property);

/*!
 * Get an integer property
 */

int32_t GetInteger(struct Cmd *command, struct prop *property);

/*!
 * Set an integer property
 */

int32_t SetInteger(struct Cmd *command, struct prop *property);

/*!
 * Get a point property
 */

int32_t GetPoint(struct Cmd *command, struct prop *property);

/*!
 * Get a point property
 */

int32_t GetCcPoint(struct Cmd *command, struct prop *property);

/*!
 * Set an point property
 */

int32_t SetPoint(struct Cmd *command, struct prop *property);

/*!
 * Get a parent property
 */

int32_t GetParent(struct Cmd *command, struct prop *property);

/*!
 * Set an RBAC token
 */

int32_t SetRBACToken(struct Cmd *command, struct prop *property);

/*!
 * Set a read of device names
 */

int32_t SetReadNames(struct Cmd *command, struct prop *property);

/*!
 * Set a read of RBAC access map
 */

int32_t SetReadRBAC(struct Cmd *command, struct prop *property);

/*!
 * Switch a client into a remote terminal session
 */

int32_t SetRterm(struct Cmd *command, struct prop *property);

/*!
 * Switch a client into a locked remote terminal session
 */

int32_t SetRtermLock(struct Cmd *command, struct prop *property);

/*!
 * Get a string property
 */

int32_t GetString(struct Cmd *command, struct prop *property);

/*!
 * Set a subscription period
 */

int32_t SetSubPeriod(struct Cmd *command, struct prop *property);

/*
 * Acknowledge failure of a transaction commit
 */

int32_t SetTransactionAckCommitFail(struct Cmd *command, struct prop *property);

/*
 * Commit a transaction
 */

int32_t SetTransactionCommit(struct Cmd *command, struct prop *property);

/*
 * Test the commit a transaction by simulating the timing event
 */

int32_t SetTransactionCommitTest(struct Cmd *command, struct prop *property);

/*
 * Set a transaction ID
 */

int32_t SetTransactionId(struct Cmd *command, struct prop *property);

/*
 * Rollback a transaction
 */

int32_t SetTransactionRollback(struct Cmd *command, struct prop *property);

/*
 * Test a transaction
 */

int32_t SetTransactionTest(struct Cmd *command, struct prop *property);

/*!
 * Get the list of enabled timig users in the last 10 minutes
 */

int32_t GetTimingUsers(struct Cmd *command, struct prop *property);

/*!
 * Get UNIX time
 */

int32_t GetUnixTime(struct Cmd *command, struct prop *property);

/*!
 * Add symbols set in bit mask to current command's value
 */

int32_t propPrintBitMask(struct Cmd *command, struct sym_lst *sym_list, uint32_t value);

/*!
 * Get the string for a symbol
 */

const char *propGetSymString(struct Parser *parser, struct sym_lst *sym_list, int32_t value);

#endif

// EOF
