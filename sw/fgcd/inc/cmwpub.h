/*!
 * @file   cmwpub.h
 * @brief  Declarations for publishing property data through CMW
 * @author Stephen Page
 */

#ifndef CMWPUB_H
#define CMWPUB_H

#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <string.h>

#include <consts.h>

// Constants

#define CMWPUB_MAX_QUEUED_NOTIFICATIONS     (8 * FGCD_MAX_DEVS) //!< Maximum number of queued notifications at any time
#define CMWPUB_NOTIFY_ALL_EQUIPDEVS         0xFFFFFFFF          //!< Value to pass as channel to cmwNotify() to notify all equipment device channels
#define CMWPUB_NOTIFY_ALL_SUB_DEVICES       0xFFFFFFFF          //!< Publication is notified to all the sub-devices

#define CMWPUB_UPDATE_FLAGS_FIRST           1                   //!< Initial value sent upon acceptance of a subscription
#define CMWPUB_UPDATE_FLAGS_IMMEDIATE       2                   //!< New value triggered by a set command

#include <cmd.h>
#include <queue.h>

#include <cmw-rda3/server/subscription/SubscriptionRequest.h>
#include <map>
#include <set>

/*!
 * Structure for subscription parameters
 */

struct CMWpub_sub_params
{
    uint32_t    sub_device;
    uint16_t    transaction_id;
    uint32_t    user;
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
    uint32_t    getopt_bin;
#endif
    uint32_t    start_index;
    uint32_t    end_index;
    uint32_t    step;
};

/*!
 * Class for notification parameters
 */

class cmwpub_notification
{
    public:

        uint32_t    channel;
        char        property[FGC_MAX_PROP_LEN + 1];
        uint32_t    user;
        uint32_t    start_index;
        uint32_t    end_index;
        uint32_t    notify_parents;
        int32_t     update_flags;

        cmwpub_notification(uint32_t    channel,
                            const char  *property,
                            uint32_t    user,
                            uint32_t    start_index,
                            uint32_t    end_index,
                            uint32_t    notify_parents,
                            int32_t     update_flags)
        {
            this->channel           = channel;

            strncpy(this->property, property, sizeof(this->property));
            this->property[sizeof(this->property) - 1] = '\0';

            this->user              = user;
            this->start_index       = start_index;
            this->end_index         = end_index;
            this->notify_parents    = notify_parents;
            this->update_flags      = update_flags;
        }
};

/*!
 * Class for subscription
 */

class cmwpub_subscription
{
    public:
        char                                           command[CMD_MAX_LENGTH + 1];    //!< Command string for the subscription
        struct CMWpub_sub_params                       params;                         //!< Parameters of the subscription
        cmw::rda3::server::SubscriptionSourceSharedPtr source;                         //!< Subscription source
        uint32_t                                       recover;                        //!< Flag to indicate that this subscription requires recovery

        cmwpub_subscription(struct CMWpub_sub_params *params, const char *command_string, const cmw::rda3::server::SubscriptionSourceSharedPtr& source)
        {
            this->params = *params;
            this->source = source;

            // Clear the recovery flag

            recover = 0;

            // Copy command string

            strncpy(this->command, command_string, sizeof(this->command));
            this->command[sizeof(this->command) - 1] = '\0';

            // Clear the recovery flag

            recover = 0;
        }
};

/*!
 * Struct containing global variables
 */

struct CMWpub
{
    int32_t         run;                            //!< Flag to indicate that the thread should run
    pthread_t       publish_thread;                 //!< Publishing thread handle
    pthread_t       retrieve_data_thread;           //!< Data retrieval thread handle
    struct Queue    notification_queue;             //!< Queue of notification data
    struct Queue    response_queue;                 //!< Queue of command responses with data to publish
    uint32_t        fgcd_cmd_count;                 //!< Count of FGCD commands queued for subscriptions
    pthread_mutex_t subscription_mutex;             //!< Mutex to protect subscription map

    // Subscription recovery

    uint32_t        recover_subs[FGCD_MAX_DEVS];    //!< Flags to indicate whether subscriptions for channels should be recovered
    pthread_t       recover_subs_thread;            //!< Subscription recovery thread handle
    sem_t           recover_subs_sem;               //!< Semaphore to request the recovery of subscriptions
};

extern struct CMWpub cmwpub;

/*!
 * Subscriptions for each device by property name
 */

extern std::multimap<std::string, cmwpub_subscription*> cmwpub_subs[FGCD_MAX_DEVS];

// External functions with C linkage

/*!
 * Start the CMW publishing thread
 */

int32_t cmwpubStart(void);

/*!
 * Notification that a property value has been updated
 */

void cmwpubNotify(uint32_t channel, const char *property, uint32_t user, uint32_t start_index, uint32_t end_index, uint32_t notify_parents, int32_t update_flags);

/*!
 * Notification that all subscriptions should be recovered for a channel.
 *
 * This is used to recover subscriptions after an FGC has re-locked its PLL.
 */

void cmwpubNotifyRecoverSubs(uint32_t channel);

/*!
 * Build a subscription command string
 */

uint32_t cmwpubBuildCommandString(const char *property, struct CMWpub_sub_params *params, char *command);

/*!
 * Start a subscription
 */
std::pair<uint32_t, Cmd*> cmwpubSubscribe(  uint32_t channel, const char *property, struct CMWpub_sub_params *params, cmw::rda3::server::SubscriptionRequest& request, void *rbac_token);

/*!
 * Stop a subscription
 */

uint32_t cmwpubUnsubscribe(uint32_t channel, const char *property, struct CMWpub_sub_params *params, const cmw::rda3::server::SubscriptionSource& source);

#endif

// EOF
