/*!
 * @file   logging.h
 * @brief  Declarations for logging
 * @author Stephen Page
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>

#include <consts.h>

// Constants

#define LOG_MAX_FILENAME_LENGTH     255                                         //!< Maximum length of log file name
#define LOG_MAX_LENGTH              64000000                                    //!< Maximum length of log file in bytes
#define LOG_MESSAGE_LENGTH          511                                         //!< Length of each log message string
#define LOG_OUTPUT_STREAM           stdout                                      //!< Output stream for log messages
#define LOG_QUEUE_LENGTH            1024                                        //!< Length of queue of messages to be logged
#define LOG_CMW_CONFIG_FILENAME     "/user/pclhc/etc/fgcd/cmw/fgcd-log.cfg"     //!< Path to CMW log config file

#include <queue.h>

/*!
 * Log entry structure
 */

struct Log_entry
{
    uint32_t        channel;
    struct timeval  timestamp;
    char            buffer[LOG_MESSAGE_LENGTH + 1];
};

/*!
 * Struct containing global variables
 */

struct Logging
{
    FILE               *file;                                   //!< File handle for log
    char                filename[LOG_MAX_FILENAME_LENGTH + 1];  //!< Filename for log
    pthread_t           thread;                                 //!< Thread handle
    struct Queue        queue;                                  //!< Queue of messages to be logged
    struct Queue        free_queue;                             //!< Queue of free buffers for messages
    void                (*time_func)(struct timeval *);         //!< Function to read time
    uint32_t            length;                                 //!< Current length of log
    uint32_t            missed_messages;                        //!< Count of missed messages
    struct Log_entry    entries[LOG_QUEUE_LENGTH];              //!< Buffers for log messages
    pthread_mutex_t     missed_messages_mutex;                  //! Protects missed_messages variable
};

extern struct Logging logging;

// External functions

/*!
 * Start the logging thread
 */

int32_t logStart(void);

/*!
 * Initialize the CMW logging
 */

int32_t logInitCmwLog(void);

/*!
 * Append string to log queue
 */

int32_t logPrintf(uint32_t channel, const char *format, ...);

#endif

// EOF
