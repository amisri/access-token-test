//! @file   equip_libref.h
//! @brief  Header file for shared functions that use libref

#ifndef EQUIP_LIBREF_H
#define EQUIP_LIBREF_H

#include <stdint.h>
#include <timing.h>
#include <fgc_errs.h>
#include <defconst.h>

// STATE.PC state bit masks - these can be used with device.state_pc_bit_mask to
// quickly to check if a state is part of a group of states.

enum State_pc_bit_mask
{
    FGC_PC_FLT_OFF_BIT_MASK       = (1 << FGC_PC_FLT_OFF      ),
    FGC_PC_OFF_BIT_MASK           = (1 << FGC_PC_OFF          ),
    FGC_PC_FLT_STOPPING_BIT_MASK  = (1 << FGC_PC_FLT_STOPPING ),
    FGC_PC_STOPPING_BIT_MASK      = (1 << FGC_PC_STOPPING     ),
    FGC_PC_STARTING_BIT_MASK      = (1 << FGC_PC_STARTING     ),
    FGC_PC_SLOW_ABORT_BIT_MASK    = (1 << FGC_PC_SLOW_ABORT   ),
    FGC_PC_TO_STANDBY_BIT_MASK    = (1 << FGC_PC_TO_STANDBY   ),
    FGC_PC_ON_STANDBY_BIT_MASK    = (1 << FGC_PC_ON_STANDBY   ),
    FGC_PC_IDLE_BIT_MASK          = (1 << FGC_PC_IDLE         ),
    FGC_PC_TO_CYCLING_BIT_MASK    = (1 << FGC_PC_TO_CYCLING   ),
    FGC_PC_ARMED_BIT_MASK         = (1 << FGC_PC_ARMED        ),
    FGC_PC_RUNNING_BIT_MASK       = (1 << FGC_PC_RUNNING      ),
    FGC_PC_ABORTING_BIT_MASK      = (1 << FGC_PC_ABORTING     ),
    FGC_PC_CYCLING_BIT_MASK       = (1 << FGC_PC_CYCLING      ),
    FGC_PC_POL_SWITCHING_BIT_MASK = (1 << FGC_PC_POL_SWITCHING),
    FGC_PC_BLOCKING_BIT_MASK      = (1 << FGC_PC_BLOCKING     ),
    FGC_PC_ECONOMY_BIT_MASK       = (1 << FGC_PC_ECONOMY      ),
    FGC_PC_DIRECT_BIT_MASK        = (1 << FGC_PC_DIRECT       ),
    FGC_PC_PAUSED_BIT_MASK        = (1 << FGC_PC_PAUSED       ),
};

// Structures

struct LIBREF_vars
{
    // Cycling status

    struct FG_error        fg_error  [NUM_CYC_SELECTORS];         // Function errors (PPM)
    struct REF_cyc_status  cyc_status[NUM_CYC_SELECTORS];         // Cycle status (PPM)
    struct REF_armed       ref_armed [NUM_CYC_SELECTORS];         // Armed functions (PPM)

    // TABLE function data

    struct FG_point        armed_table_function  [REF_ARMED_TABLE_FUNCTION_LEN(NUM_CYC_SELECTORS, FGC_MAX_SUB_DEVS, FGC_TABLE_LEN)];
    struct FG_point        running_table_function[REF_RUNNING_TABLE_FUNCTION_LEN(FGC_TABLE_LEN)];

    // ILC cycle data store

   struct REF_ilc_cyc_data ilc_cyc_data_store[REF_ILC_CYC_DATA_STORE_NUM_ELS(FGC_ILC_MAX_SAMPLES,FGC_TIMING_NUM_USERS)];
};


//! Bind CCLIBS reference management parameters to DIGMUGEF data structures
//!
//! @param[in] device_index  Device ID
//!
//! @retval 0 on success
//! @retval 1 if any parameter pointers are not set

uint32_t referenceInit(uint32_t device_index);


//! Attempt to arm reference function all users
//!
//! @param[in] device_index  Device ID

void referenceArmAll(uint32_t device_index);


//! Map MODE.PC to REF_state

enum REF_state translateModePctoRef(uint32_t device_index, uint32_t mode);


//! Asks cclibs to arm a function.
//!
//! @param[in] device_index Device ID
//! @param[in] event_time   Time of the event
//! @param[in] cyc_sel      Cycle selector of the event
//! @param[in] test_arm     True to simply test arm
//! @param[in] num_pars     Number of parameters defined in par_values array
//! @param[in] par_values   Parameter values array
//!
//! @retval    fgc_errno    Error status after attempting to arm the reference

enum fgc_errno referenceArm(uint32_t device_index, uint32_t cycle_sel, bool test_arm, uint32_t num_pars, cc_float *par_values);


//! Check whether reference type is none

int32_t SetifRefNone(struct Cmd *command);


//! Check whether a new reference can be accepted

int32_t SetifRefOk(struct Cmd *command);


//! Check whether reference parameters are unlocked.
//!
//! This condition never fails. If the reference is locked, then a copy of the
//! settings will be taken, then the reference will be unlocked.

int32_t SetifRefUnlock(struct Cmd *command);

// Reject non-PPM transactions when in ARMED state

int32_t SetifTxIdOk(struct Cmd *command);

//! Set a reference

int32_t SetRef(struct Cmd *command, struct prop *property);


//! Set pulse

int32_t SetRefPulse(struct Cmd *command, struct prop *property);


//!Set table function time/ref points

int32_t SetTableFunc(struct Cmd *command, struct prop *property);


//! Control the MODE_SIM_MEAS parameter

void ParsMeasSim(uint32_t channel);


//! Control reference function run time parameters

void ParsRunTime(uint32_t channel);


//! Control reference abort time parameters

void ParsAbortTime(uint32_t channel);


//! Callback from parser to indicate that a transactional property was set

void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property);


//! Callback to notify transactional properties for publication

void equipdevTransactionNotify(uint32_t channel, uint32_t user);


//! Apply transactional settings for a user on a device

fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time);


//! Rollback transactional settings for a user on a device

void equipdevTransactionRollback(uint32_t channel, uint32_t user);


//! Test transactional settings for a user on a device

fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user);

#endif

// EOF
