//! @file   equip_common.h
//! @brief  Common functions and structures for all equipment device classes


#ifndef EQUIP_COMMON_H
#define EQUIP_COMMON_H

// Delimiters for functions defined in parser.c. We can't define these as pointer to const because
// the parameters in the parser functions are not declared const.

char * const DELIM_NONE  = const_cast<char*>("");        // Null delimiter for parser functions
char * const DELIM_COMMA = const_cast<char*>(",");       // Comma delimiter for parser functions


// Includes

#include <stdint.h>
#include <arpa/inet.h>
#include <parser.h>

//! Structure for standard TEST property values

struct Equipdev_channel_test
{
    uint8_t     bin[64];                                       //!< Test binary property
    uint8_t     data[64];                                      //!< Test bit mask
    char        chars[64];                                     //!< Test character property
    float       floats[16];                                    //!< Test float property
    int8_t      int8s[64];                                     //!< Test integer property
    uint8_t     int8u[64];                                     //!< Test integer property
    int16_t     int16s[32];                                    //!< Test integer property
    uint16_t    int16u[32];                                    //!< Test integer property
    int32_t     int32s[16];                                    //!< Test integer property
    uint32_t    int32u[16];                                    //!< Test integer property
    const char *strings[2];                                    //!< Test string property
};

// Inline functions

//! Set or clear a bit in one of the status registers.
//!
//! There are 4 FGC status registers (STATUS.FAULTS, STATUS.WARNINGS, STATUS.ST_LATCHED, STATUS.ST_UNLATCHED).
//! They are all 16-bit unsigned integers.
//!
//! @param[in,out] status    Which status register to update, e.g. equipdev.device[0].status.st_faults
//! @param[in]     mask      Bitmask indicating which bit(s) to set/unset
//! @param[in]     value     Value to set the bits to (0/1)

static inline void setStatusBit(uint16_t *status, uint16_t mask, bool value)
{
    if(value)
    {
        *status |= mask;
    }
    else
    {
        *status &= ~mask;
    }
}


//! Set a bit in one of the latched status registers.
//!
//! Like setStatusBit(), but for latched statuses only.
//!
//! @param[in,out] status    Which status register to update, e.g. equipdev.device[0].status.st_faults
//! @param[in]     mask      Bitmask indicating which bit(s) to latch
//! @param[in]     value     If true, the bit is set. If false, nothing happens.

static inline void latchStatusBit(uint16_t *status, uint16_t mask, bool value)
{
    if(value) *status |= mask;
}


//! Get the value of a bit in one of the status registers.
//!
//! There are 4 FGC status registers (STATUS.FAULTS, STATUS.WARNINGS, STATUS.ST_LATCHED, STATUS.ST_UNLATCHED). They are
//! all 16-bit unsigned integers.
//!
//! @param[in] status    Which status register to query, e.g. equipdev.device[0].status.st_faults
//! @param[in] mask      Bitmask indicating which bit to query
//!
//! @returns         The value of the status bit (0/1)

static inline bool getStatusBit(uint16_t status, uint16_t mask)
{
    return status & mask;
}


//! Convert floats to network byte-order (for publication)
//!
//! @param[in] f      Float in host byte order
//!
//! @returns          Float in network byte order

static inline float htonf(float f)
{
    uint32_t buf = htonl(*(uint32_t *)&f);

    return *(float*)&buf;
}


// External functions


//! The function can be called from equipdevStart to set up some of the standard equipdev services
//!
//! @param[in] num_pars_func    Number of property pars functions (NUM_PARS_FUNC_{CLASS_ID})
//! @param[in] evtlog_func      Callback function to store set commands in the event long
//!
//! @retval    0                Success
//! @retval    1                Failure

int32_t equipdevInit(uint32_t num_pars_func, Prop_callback evtlog_func);


//! Callback from parser to manually configure access to equipdev properties.
//!
//! @param[in] parser Parser data structure used to process a given command
//!
//! @return    true if the property is ppm, false otherwise

bool equipdevConfigProperty(struct Parser *parser, struct prop *property);


// Check the next command token against the symbol list for the property

int32_t propEquipScanSymbolList(uint8_t *value, char *delimiter, struct Cmd *command, struct prop *property);


// Look up a property and return a pointer to it, or NULL if it is not found
//
// @param[in]     prop_label           Property label
// @param[in]     bus_id               Bus ID char ('A' or 'B'). Only used if prop_label needs to be expanded (i.e., it contains a '?')
// @param[out]    prop_label_expand    Expanded version of prop_label, with '?' replaced by bus_id
//
// @returns       Pointer to the property, or NULL if there is an error or the property is not found

struct prop *equipdevLookupProp(uint32_t channel, char bus_id, const char *prop_label, char *prop_label_expand);


// Set the number of elements for the named property (after possible expansion of '?' using bus_id character
//
// @param[in]     channel              channel address
// @param[in]     bus_id               Bus ID char ('A' or 'B'). Only used if prop_label needs to be expanded (i.e., it contains a '?')
// @param[in]     prop_label           Property label
// @param[in]     num_els              Number of elements to set for the named property

void equipdevPropSetNumEls(uint32_t channel, char bus_id, const char *prop_label, uint32_t num_els);


//! This function is called in nonvol.c. It triggers/clears a CONFIG warning in the equipment device.
//!
//! @param[in]    channel     channel address
//! @param[in]    set         Set the CONFIG warning if different than 0, otherwise clears the CONFIG warning.

void equipdevNonvolWarn(uint32_t channel, int32_t set);

#endif

// EOF
