/*!
 * @file   devname.h
 * @brief  Declarations for device name resolution
 * @author Stephen Page
 */

#ifndef DEVNAME_H
#define DEVNAME_H

#include <pthread.h>
#include <stdint.h>

#include <consts.h>
#include <fgc_consts_gen.h>

// Constants

#define DEVNAME_NAME_FILE       FGC_NAME_FILE                       //!< FGC name file
#define DEVNAME_SUB_NAME_PATH   CONFIG_PATH"/sub_devices"           //!< Path containing sub-device name files
#define DEVNAME_MPPM_PATH       CONFIG_PATH"/multippm_conditions"   //<! Path contatining multi PPM conditions

#include <hash.h>

// Struct containing global variables

struct Devname
{
    pthread_mutex_t     mutex;
    struct hash_table   *name_hash;                                                         //!< Hash of devices by name
    struct hash_entry   name_entry[FGCD_MAX_DEVS];                                          //!< Name hash entries
    struct hash_table   *sub_name_hash;                                                     //!< Hash of sub-devices
    struct hash_entry   sub_name_entry[FGCD_MAX_SUB_DEVS];                                  //!< Sub-device name hash entries
    struct hash_entry   *device_sub_devices[FGCD_MAX_DEVS][1 + FGCD_MAX_SUB_DEVS_PER_DEV];  //!< Sub-device hash entries by device
};

extern struct Devname devname;

// External functions


/*!
 * Initialise device naming
 */

int32_t devnameInit(void);

/*!
 * Read name file
 */

int32_t devnameRead(void);

/*!
 * Read sub-device name file
 */

int32_t devnameSubRead(void);

/*!
 * Read MPPM conditions file
 */

int32_t devnameMppmRead(void);

/*!
 * Resolve a device name
 */

int32_t devnameResolve(char *name);

/*!
 * Resolve a sub-device name
 */

int32_t devnameSubResolve(char *name, uint32_t *channel, uint32_t *sub_channel);

/*!
 * Get the name of a sub-device name
 */

int32_t devnameSubName(char *name, uint32_t size, uint32_t channel, uint32_t sub_channel);

#endif

// EOF
