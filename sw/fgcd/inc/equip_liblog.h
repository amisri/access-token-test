//! @file   equip_liblog.h
//! @brief  Header file for the common fgcd equipment device liblog integration code

#ifndef EQUIP_LIBLOG_H
#define EQUIP_LIBLOG_H

#include <stdbool.h>
#include <stdint.h>

#include <cclibs.h>
#include <consts.h>
#include <log_class.h>



//! Bind CCLIBS analogue and digital logging parameters to DIGMUGEF data structures and initialise values.
//!
//! @param[in]    channel    address of the DIGMUGEF device

void ccLogInit(uint32_t channel);


//! Return signal names for a menu
//!
//! This is called from GetLogMenuSignals() which is the Get function for LOG.MENU.SIGNALS and LOG.MENU.MPX_SIG_NAMES.
//! For LOG.MENU.SIGNALS, the header type should be LOG_READ_GET_SIG_NAMES, which returns the selected signal names for the menus.
//! For LOG.MENU.MPX_SIG_NAMES, the header type should be LOG_READ_GET_ALL_SIG_NAMES, which returns the all the signal names
//! for multiplexed log menus only. For non-multiplexed menus, the returned elements are empty.
//!
//! @param[in,out]    command        Pointer to command structure for G LOG.MENU.SIGNALS
//! @param[in]        action         LOG_READ_GET_SIG_NAMES or LOG_READ_GET_ALL_SIG_NAMES
//!
//! @returns FGC error number

int32_t GetLogMenuSignals(struct Cmd *command, struct prop *property);


//! Get log data with a SPY header

int32_t GetLogSpy(struct Cmd *command, struct prop *property);


//! Get PM_BUF data (includes the event log)

void logGetPmBuf(uint8_t channel, uint32_t *pm_buffer);


//! Cache liblog selectors after a change to a LOG MPX property.

void ParsLogMpx(uint32_t channel);


// If DEBUG_SIG is defined in log_class.h then include logSetDebugSignals()

#ifdef DEBUG_SIG

//! Set the values in the debug structure that are liblog signals

void ccLogSetDebugSignals(uint32_t channel, uint32_t iter_time_us);

#endif


//! Set the freeze flag for all continuous logs which form part of the Post Mortem.
//!
//! liblog will continue logging until the number of samples specified in the log definition spreadsheet have been collected
//!
//! @param[in]    channel        FIP address of the FGClite device

void ccLogPMFreeze(uint32_t channel);


//! Freeze and individual log.
//!
//! liblog will continue logging until the number of samples specified in the log definition spreadsheet have been collected
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    log_idx        Log index to freeze

void ccLogPMFreezeLog(uint32_t channel, uint8_t log_idx);


//! Unfreeze the Post Mortem logs.
//!
//! This should be called when the Post Mortem dump has completed, to return logging to normal
//! operation.
//!
//! @param[in]    channel        FIP address of the FGClite device

void ccLogPMUnfreeze(uint32_t channel);


//! Unfreeze and individual log.
//!
//! liblog will continue logging until the number of samples specified in the log definition spreadsheet have been collected
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    log_idx        Log index to freeze

void ccLogPMUnfreezeLog(uint32_t channel, uint8_t log_idx);


//! Check if Post Mortem logs have just frozen.
//!
//! Continuous logs are managed by liblog and are frozen when they have stored the requisite number
//! of samples after calling ccLogPMFreeze. Discontinuous logs are managed by the FGCD application
//! and are considered frozen if no logging is active.
//!
//! Note: This function returns true only on the iteration when the last postmortem log transitions
//! from not frozen to frozen. This can be used as a signal to trigger the postmortem log readout.
//!
//! @param[in]    channel        FIP address of the FGClite device
//!
//! @retval       true           All PM logs are frozen
//! @retval       false          Some PM logs are not yet frozen

bool ccLogPMIsFrozen(uint32_t channel);


//! Get mask of frozen logs.
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @return                      Mask of frozen logs

uint32_t ccLogPMGetFreezeMask(uint32_t channel);


//! Get mask of post mortem logs.
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @return                      Post mortem log mask

uint32_t ccLogPMGetPostmortemMask(uint32_t channel);


#endif // CC_LOG_H

// EOF
