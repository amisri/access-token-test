/*!
 * @file   pub.h
 * @brief  Declarations for publishing data
 * @author Stephen Page
 */

#ifndef PUB_H
#define PUB_H

#include <limits.h>
#include <netinet/in.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>

#include <consts.h>

// Constants

#define PUB_CMW_PERIOD_MS               500                                     //!< Period of CMW status publication in ms

#define PUB_UDP_DEFAULT_PERIOD          50                                      //!< Default period for UDP published data in PUB_UDP_TCP_PERIOD_UNITS steps
#define PUB_UDP_TCP_PERIOD_MS           20                                      //!< Default period units for UDP TCP published data in ms
#define PUB_UDP_PERIOD_MS               10                                      //!< Default period units for published data in ms
#define PUB_UDP_GLOBAL_SUBS_FILE        CONFIG_PATH"/udp_subscriptions/global"  //!< File containing global subscription data
#define PUB_UDP_HOST_SUBS_PATH          CONFIG_PATH"/udp_subscriptions/host"    //!< Path containing files with host subscription data
#define PUB_UDP_MAX_NON_TCP_CLIENTS     10                                      //!< Maximum number of clients without associated TCP connections
#define PUB_UDP_MAX_PERIOD              500                                     //!< Maximum period for UDP published data subscription in FGCD_CYCLE_PERIOD steps

#include <fgc_pubdata.h>

/*!
 * Published data structure.
 */

struct Pub_data
{
    struct fgc_udp_header   header;
    uint32_t                time_sec;
    uint32_t                time_usec;
    struct fgc_stat         status[FGCD_MAX_DEVS];
};

/*!
 * UDP client structure
 */

struct udp_client
{
    char            hostname[HOST_NAME_MAX + 1];    //!< Hostname
    struct in_addr  address;                        //!< Host address
    uint32_t        port_num;                       //!< Port number for UDP published data subscription
    int32_t         period;                         //!< Period for UDP published data subscription (in 10ms units)
    uint32_t        period_tick;                    //!< Tick within period for UDP published data subscription
    uint32_t        sequence;                       //!< Sequence number for UDP published data subscription
    uint32_t        id;                             //!< User-defined ID number to send in packet headers
    uint32_t        user;                           //!< User selector
};

/*!
 * Struct containing global variables
 */

struct Pub
{
    uint32_t                cmw_sequence_number;                    //!< Sequence number for CMW publishing
    uint32_t                udp_load;                               //!< Outgoing UDP load in bytes per second
    pthread_t               thread;                                 //!< Thread handle
    int                     sock;                                   //!< Socket to send data
    sem_t                   send_data_sem;                          //!< Semaphore to trigger sending of data
    struct Pub_data         published_data;                         //!< Published data
    struct udp_client       client[PUB_UDP_MAX_NON_TCP_CLIENTS];    //!< UDP clients that are not associated with TCP clients
    uint32_t                num_udp_only_clients;                   //!< Number of UDP only clients
};

extern struct Pub pub;

// External functions

/*!
 * Start the publishing thread
 */

int32_t pubStart(void);

/*!
 * Trigger publication
 */

int32_t pubTrigger(void);

#endif

// EOF
