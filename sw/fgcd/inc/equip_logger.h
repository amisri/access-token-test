//! @file   equip_liblogger.h
//! @brief  Header file for the handling of FGC logger and Gateway post-mortem requests

#ifndef EQUIP_LOGGER_H
#define EQUIP_LOGGER_H

#include <stdint.h>


// External function definitions

//! Initializes the FGC logger

int32_t loggerInit(void);


//! Returns if the FGC logger is enabled
//!
//! @retval       True           FGC logger enabled
//! @retval       False          FGC logger disabled

bool loggerIsEnabled(void);


//! Processes logger events
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @retval       true           Logs unfrozen
//! @retval       false          Logs not unfrozen

bool loggerProcess(uint32_t channel);


//! Synchronizes the log and clears the corresponding bit in the bit-mask
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    log_idx        Log index

void loggerFgcLogSync(uint32_t channel, uint8_t log_idx);


//! Reset the FGC logger event
//!
//! @param[in]    channel        FIP address of the FGClite device

void loggerReset(uint32_t channel);


//! Trigger a post-mortem FGC logger event
//!
//! @param[in]    channel        FIP address of the FGClite device

void loggerTriggerPm(uint32_t channel);


//! Trigger a DIM FGC logger event
//!
//! @param[in]    channel        FIP address of the FGClite device
//! @param[in]    menu_idx       Menu index

void loggerTriggerDim(uint32_t channel, uint32_t menu_idx);


#endif // EQUIP_LOGGER_H

// EOF
