/*!
 * @file   cmd.h
 * @brief  Declarations for commands
 * @author Stephen Page
 */

#ifndef CMD_H
#define CMD_H

#include <stdint.h>
#include <sys/time.h>

#include <consts.h>
#include <fgc_consts_gen.h>

// Forward declarations

struct Parser;
struct FGCD_device;
struct TCP_client;
struct Queue;

// Constants

#define CMD_FGCD_CLIENT                 ((struct TCP_client *)0xFFFFFFFF)    //!< Value of cmd.client to indicate that client is FGCD itself
#define CMD_MAX_LENGTH                  FGC_MAX_CMD_LEN                      //!< Maximum length of a command (S/G PROP - does not include values being set)
#define CMD_MAX_RTERM_RESP_LENGTH       64                                   //!< Maximum length of a remote terminal response
#define CMD_MAX_TAG_LENGTH              FGC_MAX_TAG_LEN                      //!< Maximum length of a command tag

enum Cmd_type
{
    CMD_GET,        //!< Get command
    CMD_GET_SUB,    //!< Get command for subscription
    CMD_SET,        //!< Set command
    CMD_SET_BIN,    //!< Set command with binary value
    CMD_SUB,        //!< Subscription request
    CMD_UNSUB,      //!< Unsubscription request
};

enum Struct_type
{
    CMD_STRUCT,     //!< Command structure
    RTERM_STRUCT,   //!< Remote terminal structure
};

/*!
 * Command structure
 */

struct Cmd
{
    enum Struct_type    struct_type;                            //!< Type of structure
    struct Parser       *parser;                                //!< Pointer to parser handling the command
    uint32_t            remaining;                              //!< Number of remaining characters to send
    enum Cmd_type       type;                                   //!< Type (set or get)
    char                command_string[CMD_MAX_LENGTH + 1];     //!< Command string
    uint32_t            command_length;                         //!< Length of command string
    uint32_t            index;                                  //!< Index within command while parsing
    char                tag[CMD_MAX_TAG_LENGTH + 1];            //!< Command tag
    char                device_name[FGC_MAX_DEV_LEN + 1];       //!< Channel number or name of device
    struct FGCD_device  *device;                                //!< Target device
    uint32_t            sub_device;                             //!< Number of target sub-device (or 0 if no sub-device)
    uint16_t            transaction_id;                         //!< Transaction ID
    uint32_t            user;                                   //!< Target PPM user
    int32_t             force_user;                             //!< Override PPM user from command string or user field and use this one if its' >-1
    char                *value;                                 //!< Value - calloced in cmdqmgrInit() to buffer of length CMD_MAX_VAL_LENGTH bytes
    uint32_t            tag_length;                             //!< Length of tag
    uint32_t            value_length;                           //!< Length of value
    uint32_t            error;                                  //!< Error status
    struct timeval      acq_timestamp;                          //!< Acquisition timestamp
    struct timeval      req_timestamp;                          //!< Acquisition timestamp
    struct timeval      cycle_timestamp;                        //!< Cycle timestamp
    bool                zero_cycle_timestamp;                   //!< If set to true, then cycle timestamp is set to 0
    struct TCP_client   *client;                                //!< Pointer to client structure for client that submitted the command
    struct Queue        *response_queue;                        //!< Pointer to response queue
    void                *rbac_token;                            //!< Pointer to RBAC token
    int32_t             cmw_update_flags;                       //!< Flags for CMW publication
    bool                error_in_value;                         //!< If set, error message will be in the value
};

/*!
 * Remote terminal response structure
 */

struct Rterm_resp
{
    enum Struct_type    struct_type;                            //!< Type of structure (to distinguish from cmd within queues)
    char                buffer[CMD_MAX_RTERM_RESP_LENGTH];      //!< Buffer for received rterm characters
    uint8_t             buffer_length;                          //!< Length of buffer
};

/*!
 * Queue member union
 */

union cmdq_member
{
    enum Struct_type    struct_type;                            //!< Type of structure
    struct Cmd          command;
    struct Rterm_resp   rterm;
};

#endif

// EOF
