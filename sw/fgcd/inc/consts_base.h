/*!
 * @file   consts_base.h
 * @brief  Basic global constants
 * @author Stephen Page
 */

#ifndef CONSTS_BASE_H
#define CONSTS_BASE_H

#define CONFIG_PATH                     "/user/pclhc/etc/fgcd"                          //!< Directory containing fgcd configuration files
#define FRONT_END_DATA_PATH             "/dsc/local/data/fgcd"                          //!< Directory containing front-end specific data

#endif

// EOF
