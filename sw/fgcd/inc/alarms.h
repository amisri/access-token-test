/*!
 * @file   alarms.h
 * @brief  Declarations for submitting alarms to the alarm system
 * @author Stephen Page
 */

#ifndef ALARMS_H
#define ALARMS_H

#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>

#include <consts.h>
#include <pub.h>

// Forward declarations


namespace laserlw
{
    class AlarmSource;
}

// Constants

#define ALARMS_MAX_PER_DEVICE       32                  //!< Maximum number of alarms per device
#define ALARMS_PERIOD               FGCD_CYCLES_PER_SEC //!< Alarm surveillance period in steps of FGCD_CYCLE_PERIOD
#define ALARMS_PUSH_ACTIVE_PERIOD   240                 //!< Maximum period between pushing active alarm list in steps of ALARMS_PERIOD
#define ALARMS_SELECT_TIMEOUT       1                   //!< Timeout for select() used within alarm interface library

/*!
 * Struct containing global variables
 */

struct Alarms
{
    sem_t                       sem;                                            //!< Semaphore to trigger the sending of alarms
    uint32_t                    cycles;                                         //!< Count of alarm scan cycles
    pthread_t                   thread;                                         //!< Thread handle
    struct Pub_data             published_data;                                 //!< Published data to check for alarms
    pthread_mutex_t             data_mutex;                                     //!< Mutex to protect data being checked for alarms
    laserlw::AlarmSource       *alarm_source;
    uint32_t                    tick;                                           //!< Rolling counter for checking alarms at required frequency
};

extern struct Alarms alarms;

// External functions

/*!
 * Start the alarm thread
 */

int32_t alarmsStart(void);

#endif

// EOF
