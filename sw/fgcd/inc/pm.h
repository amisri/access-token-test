/*!
 * @file   pm.h
 * @brief  Declarations for submitting post-mortem data
 * @author Marc Magrans de Abril
 */

#pragma once

#include <stdint.h>

#include <fgc_stat.h>

// Dummy declarations

struct PM_ext_acq  {};
struct PM_self_acq {};

//! Update the PM data

void pmPubUpdate(uint8_t channel, struct fgc_stat *status, uint32_t time_sec, uint32_t time_usec);

bool pmIsActive(uint32_t channel);

// EOF
