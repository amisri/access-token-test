/*!
 * @file   equipdev.h
 * @brief  Declarations for equipment devices.  Note that this version of the file is a dummy skeleton for classes without equipment devices
 * @author Stephen Page
 */

#pragma once

#include <stdint.h>
#include <sys/time.h>

#include <consts.h>
#include <fgc_errs.h>
#include <parser.h>
#include <timing.h>

//! Data for an equipment device channel

struct Equipdev_channel
{
    // PPM variables must be within the following structure

    struct Equipdev_channel_ppm                     //!< PPM variables
    {
        uint16_t transaction_id;                    //!< Transaction ID for transactional settings
        uint16_t transaction_state;                 //!< State of a transaction

        // Transactional settings

        struct Equipdev_ppm_transactional
        {
        } transactional, transactional_backup;      //!< PPM transactional settings and a backup to allow rollback
    } ppm[NUM_CYC_SELECTORS];
};

//! Struct containing global variables

struct Equipdev
{
    struct Equipdev_channel device[FGCD_MAX_DEVS];  //!< Data for equipment devices
};

extern struct Equipdev equipdev;


// Static functions

//! Called from nonvol.cpp if a config property is changed.

static inline void equipdevConfigUnsync(uint32_t channel)
{
}


// External functions
// Note that these functions are empty dummies in this file

//! Callback from parser to manually configure properties

bool equipdevConfigProperty(struct Parser *parser, struct prop *property);

/*!
 * This function is called in nonvol.c. It triggers/clears a CONFIG warning in the equipment device.
 *
 * @param[in] channel     channel address
 * @param[in] set         Set the CONFIG warning if different than 0, otherwise clears the CONFIG warning.
 */

void equipdevNonvolWarn(uint32_t channel, int32_t set);

/*!
 * Initialise data for devices.
 *
 * This function is called each time the device names are read.
 */

void equipdevInitDevices(void);

//! Callback from parser to indicate that a transactional property was set

void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property);

//! Callback to notify transactional properties for publication

void equipdevTransactionNotify(uint32_t channel, uint32_t user);

//! Apply transactional settings for a user on a device

fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time);

//! Rollback transactional settings for a user on a device

void equipdevTransactionRollback(uint32_t channel, uint32_t user);

//! Test transactional settings for a user on a device

fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user);

// EOF
