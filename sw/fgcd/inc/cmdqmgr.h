/*!
 * @file   cmdqmgr.h
 * @brief  Declarations for managing the gateway's command queues
 * @author Stephen Page
 */

#ifndef CMDQMGR_H
#define CMDQMGR_H

#include <stdint.h>

#include <cmd.h>
#include <consts.h>
#include <queue.h>

// Constants

#define CMDQMGR_MIN_CMDS    100     //!< Minimum number of command blocks
#define CMDQMGR_MAX_CMDS    1000    //!< Maximum number of command blocks
#define CMDQMGR_RESERVED_MB 1000    //!< Memory (in MB) to reserve for other uses
#define CMDQMGR_KEEP_AVL    15      //!< Memory to keep available in percent of total RAM
#define RTERM_NUM_RESPS     200     //!< Number of remote terminal responses to pre-allocate


/*!
 * Struct containing global variables
 */

struct Cmdqmgr
{
    uint32_t          num_cmds;                       //!< Number of commands based on free memory
    uint32_t          cmds_received;                  //!< Number of commands received
    struct Cmd       *command;                        //!< Command structures array (length = num_cmds)
    struct Queue      free_cmd_q;                     //!< Queue of free commands
    struct Rterm_resp rterm_resp[RTERM_NUM_RESPS];    //!< Remote terminal responses
    struct Queue      free_rterm_q;                   //!< Queue of free remote terminal responses
};

extern struct Cmdqmgr cmdqmgr;

// External functions

/*!
 * Initialise variables
 */

int32_t cmdqmgrInit(void);

/*!
 * Initialise a command structure
 */

void    cmdqmgrInitCmd(struct Cmd *cmd);

/*!
 * Queue command
 */

void    cmdqmgrQueue(struct Cmd *command);

#endif

// EOF
