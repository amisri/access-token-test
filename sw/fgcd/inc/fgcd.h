/*!
 * @file   fgcd.h
 * @brief  Declarations for the Function Generator Controller Daemon
 * @author Stephen Page
 */

#ifndef FGCD_H
#define FGCD_H

#include <limits.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>

#include <consts.h>
#include <fgc_consts_gen.h>

// Constants

#define FGCD_SERVER_PREFIX          "FGC_"                                              //!< Prefix for server name
#define RTERM_Q_LEN                 1024                                                //!< Length of character queue for characters sent to FGCs in rterms
#define GROUP_NAME_MAX              31                                                  //!< Max group name length

#include <charq.h>
#include <cmd.h>
#include <pm.h>
#include <deftypes.h>

// forward declarations

struct TCP_client;

/*!
 * Machines
 */

enum FGCD_machine
{
    MACHINE_NONE,
    MACHINE_ADE,
    MACHINE_CPS,
    MACHINE_LEI,
    MACHINE_LHC,
    MACHINE_LN4,
    MACHINE_LNA,
    MACHINE_PSB,
    MACHINE_SPS,
    FGCD_NUM_MACHINES
};
#define MACHINE_DEFAULT MACHINE_LHC //!< Default machine

/*!
 * Machine names
 */

extern const char *fgcd_machine_name[FGCD_NUM_MACHINES];

/*!
 * Data for each device
 */

struct FGCD_device
{
    uint32_t           id;                    //!< Device ID
    char              *name;                  //!< Device name
    char              *operational_name;      //!< Device operational name
    uint8_t            fgc_class;             //!< FGC class number
    uint16_t           omode_mask;            //!< Operational mode mask
    int32_t            remote;                //!< Flag to indicate whether the device is handled remotely (e.g. an FGC)
    int32_t            online;                //!< Flag to indicate that device is online
    int32_t            ready;                 //!< Flag to indicate that device is ready to receive commands
    uint32_t           parameter_mask;        //!< Mask of parameter function indices to be called following property sets
    pthread_mutex_t    parameter_mutex;       //!< Mutex to protect access to the parameter_mask
    uint32_t           config_unset_count;    //!< Count of properties with the CONFIG flag that have not yet been set
    struct Queue      *command_queue;         //!< Command queue
    uint32_t           cmds_received;         //!< Number of commands received
    struct Charq       rterm_tx_q;            //!< Remote terminal transmit queue
    int32_t            rterm_ready;           //!< Flag to indicate that device is ready to receive remote terminal data
    struct PM_ext_acq  pm_ext;                //!< Post-mortem acquisition for externally-triggered events
    struct PM_self_acq pm_self;               //!< Post-mortem acquisition for self-triggered events
};

/*!
 * Struct containing global variables
 */

struct Fgcd
{
    char                             hostname[HOST_NAME_MAX + 1];                                //!< Hostname
    char                             group[GROUP_NAME_MAX+1];                                    //!< Group name
    char                            *hostname_property;                                          //!< Pointer to access hostname as a string property
    char                             server_name[sizeof(FGCD_SERVER_PREFIX) + HOST_NAME_MAX];    //!< Server name
    char                            *server_name_property;                                       //!< Pointer to access server name as a string property
    struct FGCD_device               device[FGCD_MAX_DEVS];                                      //!< Device data
    int32_t                          enable_fgc_logger;                                          //!< Flag to enable FGC Logger
    int32_t                          enable_pm;                                                  //!< Flag to enable post-mortem
    int32_t                          enable_rbac;                                                //!< Flag to enable RBAC
    int32_t                          force_rbac;                                                 //!< Flag to force clients to supply RBAC tokens with commands
    enum FGCD_machine                machine;                                                    //!< Machine to which devices belong
    pid_t                            pid;                                                        //!< Process ID
    int32_t                          restart;                                                    //!< Flag to indicate whether to restart when shutting down
    struct timeval                   time_start;                                                 //!< Time at which process was started
    uint16_t                         op_mode;                                                    //!< Operational mode
};

extern struct Fgcd fgcd;

/*!
 * Terminate the FGCD.
 *
 * Terminate this process, either by using a signal (SIGTERM, SIGSEGV) or by an external call to /sbin/reboot.
 *
 * @param[in] command     Pointer to the SET command which initiated the terminate function
 * @param[in] property    Pointer to the property which indicates how to terminate (CRASH, PWRCYC or RESET)
 *
 * @retval 0    The terminate call completed successfully. The process will probably terminate before the function returns.
 * @retval 1    The terminate call was unsuccessful. The reason is set in command->error.
 */

int32_t Terminate(struct Cmd *command, struct prop *property);

#endif

// EOF
