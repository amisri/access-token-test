/*!
 * @file   tcp.h
 * @brief  Declarations for the TCP server
 * @author Stephen Page
 */

#ifndef TCP_H
#define TCP_H

#include <limits.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/time.h>

#include <consts.h>
#include <defconst.h>
#include <fgc_consts_gen.h>
#include <fgc_pubdata.h>
#include <queue.h>

// Forward declarations

struct Cmd;

// Constants

#define RTERM_DISCONNECT_CHAR   24              //!< Character to signal disconnect of remote terminal
#define TCP_AUTH_USER_SIZE      16              //!< Size of string for username
#define TCP_MODE_CMD            0               //!< TCP client is in command mode
#define TCP_MODE_RTERM          1               //!< TCP client is in remote terminal mode
#define TCP_PORT_NUM            FGC_GW_PORT     //!< TCP port number on which to listen


/*!
 * Enumeration for command reception state
 */

enum TCP_recv_state
{
    TCP_RECV_IDLE,
    TCP_RECV_TAG,
    TCP_RECV_TYPE,
    TCP_RECV_DEV,
    TCP_RECV_PROP,
    TCP_RECV_VALUE,
    TCP_RECV_BIN_LENGTH,
    TCP_RECV_BIN_VALUE,
    TCP_RECV_BIN_ERROR,
    TCP_RECV_ERROR,
};

/*!
 * Client structure
 */

struct TCP_client
{
    int32_t                     id;                                 //!< ID of client structure
    int32_t                     in_use;                             //!< Flag to indicate that this client handle is in use
    int                         sock;                               //!< Socket
    unsigned char               recv_buffer[1500];                  //!< Receive buffer
    char                        send_buffer[1500];                  //!< Send buffer
    char                        hostname[HOST_NAME_MAX + 1];        //!< Hostname
    char                        *hostname_property;                 //!< Hostname property
    char                        username[TCP_AUTH_USER_SIZE];       //!< Username
    char                        *username_property;                 //!< Username property
    struct in_addr              address;                            //!< Host address
    uint32_t                    mode;                               //!< Mode (command or rterm)
    uint32_t                    num_cmds;                           //!< Number of commands in progress
    uint32_t                    num_rterm_resps;                    //!< Number of remote terminal responses in progress
    pthread_mutex_t             mutex;                              //!< Mutex to protect command structure counts
    enum TCP_recv_state         cmd_state;                          //!< State of the command currently being received
    uint32_t                    dev_index;                          //!< Index within device name or address while receiving command
    uint32_t                    bin_index;                          //!< Index within received binary field
    uint32_t                    bin_length;                         //!< Length of received binary value
    uint32_t                    last_char_space;                    //!< Flag to indicate last command character received was a space
    struct Queue                response_queue;                     //!< Response queue
    pthread_t                   send_thread;                        //!< Send thread
    pthread_t                   recv_thread;                        //!< Receive thread
    struct Cmd                  *recv_cmd;                          //!< Pointer to current command being filled by receive thread
    struct FGCD_device          *rterm_device;                      //!< Pointer to device to which client holds a remote terminal
    uint32_t                    rterm_locked;                       //!< Flag to indicate whether the rterm is locked (ignores close character)
    struct FGCD_device          *diag_device;                       //!< Pointer to device to which client holds a UDP diagnostic subscription
    uint32_t                    udp_sub_port_num;                   //!< Port number for UDP published data subscription
    int32_t                     udp_sub_period;                     //!< Period for UDP published data subscription (in 20ms units)
    uint32_t                    udp_sub_period_tick;                //!< Tick within period for UDP published data subscription
    uint32_t                    udp_sub_sequence;                   //!< Sequence number for UDP published data subscription
    uint32_t                    udp_sub_id;                         //!< User-defined ID number to send in packet headers
    uint32_t                    run;                                //!< Flag to indicate that this client should run
    void                        *rbac_token;                        //!< Pointer to RBAC token
    long                        rbac_token_expiry;                  //!< Expiry time of RBAC token
};

/*!
 * Struct containing global variables
 */

struct Tcp
{
    int                     listen_sock;                    //!< Socket handle to listen on
    uint32_t                load;                           //!< TCP load in bytes for this second
    struct TCP_client       client[FGC_TCP_MAX_CLIENTS];    //!< Client handles
    pthread_t               listen_thread;                  //!< Listening thread handle
};

extern struct Tcp tcp;

// External functions

/*!
 * Start the TCP server
 */

int32_t tcpStart(void);

#endif

// EOF
