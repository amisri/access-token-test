/*!
 * @file   timing_handlers.h
 * @brief  Declarations for timing event handlers when using the timdt library
 * @author Marc Magrans de Abril
 */

#ifndef TIMING_HANDLERS_H
#define TIMING_HANDLERS_H

#include <stdint.h>
#include <map>
#include <sys/time.h>

#include <consts.h>

#include <timdt-lib-cpp/Timing.h>

/*!
 * Timing handler enum
 */

enum Timing_handler_types
{
    TIMING_HANDLER_POSIXMS,
    TIMING_HANDLER_POSTMORTEM,
    TIMING_HANDLER_STARTREF,
    TIMING_HANDLER_ABORTREF,
    TIMING_HANDLER_TELEGRAM,
    TIMING_HANDLER_CYCLEWARNING,
    TIMING_HANDLER_SSC,
    TIMING_HANDLER_MS,
    TIMING_HANDLER_STARTCYCLE,
    TIMING_HANDLER_FEI,
    TIMING_HANDLER_COAST,
    TIMING_HANDLER_RECOVER,
    TIMING_HANDLER_STARTHARMONICS,
    TIMING_HANDLER_STOPHARMONICS,
    TIMING_HANDLER_ECONOMY,
    TIMING_HANDLER_ABORTECONOMY,
    TIMING_HANDLER_FGCDSTARTCYCLE,
    TIMING_HANDLER_INJECTWARNING,
    TIMING_HANDLER_STARTREF1WARNING,
    TIMING_HANDLER_STARTREF2WARNING,
    TIMING_HANDLER_STARTREF3WARNING,
    TIMING_HANDLER_STARTREF4WARNING,
    TIMING_HANDLER_STARTREF5WARNING,    
    TIMING_HANDLER_TOCYCLING,
    TIMING_HANDLER_TOSTANDBY,
    TIMING_HANDLER_DYNDEST,
    TIMING_HANDLER_PAUSE,
    TIMING_HANDLER_RESUME,
    TIMING_HANDLER_ACQUISITION,
    TIMING_HANDLER_TRANSACTIONCOMMIT,
    TIMING_HANDLER_TRANSACTIONROLLBACK,
    TIMING_HANDLER_EXTRACTWARNING,
    TIMING_NUM_HANDLERS
};

/*!
 * Timing handler names
 */

const char * const timing_handler_names[TIMING_NUM_HANDLERS] =
{
    "POSIXMS",
    "POSTMORTEM",
    "STARTREF",
    "ABORTREF",
    "TELEGRAM",
    "CYCLEWARNING",
    "SSC",
    "MS",
    "STARTCYCLE",
    "FEI",
    "COAST",
    "RECOVER",
    "STARTHARMONICS",
    "STOPHARMONICS",
    "ECONOMY",
    "ABORTECONOMY",
    "FGCDSTARTCYCLE",
    "INJECTWARNING",
    "STARTREF1WARNING",
    "STARTREF2WARNING",
    "STARTREF3WARNING",
    "STARTREF4WARNING",
    "STARTREF5WARNING",    
    "TOCYCLING",
    "TOSTANDBY",
    "DYNDEST",
    "PAUSE",
    "RESUME",
    "ACQUISITION",
    "TRANSACTIONCOMMIT",
    "TRANSACTIONROLLBACK",
    "EXTRACTWARNING"
};

/*!
 * Timing event handlers.
 *
 * Note that the names in this array are used in the following configuration files:
 *    TIMING_GLOBAL_EVENT_PATH/<machine>
 *    TIMING_CLASS_EVENT_PATH/<class>/<machine>
 *    TIMING_HOST_EVENT_PATH/<hostname>
 *
 * Note that the indices of the handlers are used within xml/properties/fgcd.xml
 * for the profiling properties.
 */

extern struct Timing_event_handler timing_event_handlers[];

/*!
 * Telegram value indices (for timing.field_value).
 *
 * Note that the names are used in the following configuration files:
 *    TIMING_GLOBAL_TELEGRAM_PATH/<machine>
 *    TIMING_CLASS_TELEGRAM_PATH/<class>/<machine>
 *    TIMING_HOST_TELEGRAM_PATH/<hostname>
 */

enum Timing_field_indices
{
    TIMING_FIELD_PC_PERMIT,
    TIMING_FIELD_NEXT_USER,
    TIMING_FIELD_NEXT_CYCLE_LEN,
    TIMING_FIELD_OP_MODE,
    TIMING_FIELD_USER,
    TIMING_FIELD_CYCLE_LEN,
    TIMING_FIELD_SECTOR_ACCESS,
    TIMING_FIELD_NEXT_DEST,
    TIMING_FIELD_NEXT_DYN_DEST,
    TIMING_FIELD_CYCLE_TAG,
    TIMING_FIELD_CYCLE_NUM,
    TIMING_FIELD_COMBINED_LINE,
    TIMING_FIELD_BP_DURATION_MS,
    TIMING_FIELD_INJECTED_BATCHES,
    TIMING_FIELD_MACHINE_MODE,
    TIMING_NUM_FIELDS
};

/*!
 * Names for the above telegram groups
 */

const char * const timing_telegram_names[TIMING_NUM_FIELDS]
= {
    "PCPERMIT",
    "NEXTUSER",
    "NEXTCYCLELEN",
    "OPMODE",
    "USER",
    "CYCLELEN",
    "SECTORACCESS",
    "NEXTDEST",
    "NEXTDYNDEST",
    "CYCLETAG",
    "CYCLENUM",
    "COMBINEDLINE",
    "BPDURATIONMS",
    "INJECTEDBATCHES",
    "MACHINEMODE",
  };

/*!
 * Timing event additional information structure
 */

struct Timing_event_info
{
    uint32_t delay_ms = 0;   //!< Delay for warning events in milliseconds
};

/*!
 * Timing event handler callback
 */

typedef void TimingHandlerFunc(Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value);

/*!
 * Map that maps timing (not fieldbus) event with additional timing information
 */

typedef std::map<std::string, Timing_event_info> TimingEvents;

/*!
 * Timing event handler structure
 */

struct Timing_event_handler
{
    char const          *name;           //!< Name of FGCD event
    TimingHandlerFunc   *function;       //!< Function to call when event occurs
    TimingEvents         timing_events;  //!< List of timing events that trigger a fieldbus event represented by this handler
};


// External functions

/*!
 * Sets the multi-PPM condition
 *
 * @param[in] mppm_condition multi-PPM condition
 */
void timingAddMultiPpmEvent(const char *mppm_condition);


// Declarations for timing event handlers

TimingHandlerFunc timingAbortEconomy;
TimingHandlerFunc timingAbortRef;
TimingHandlerFunc timingAcquisition;
TimingHandlerFunc timingCoast;
TimingHandlerFunc timingCycleWarning;
TimingHandlerFunc timingDynamicDestination;
TimingHandlerFunc timingEconomy;
TimingHandlerFunc timingFEI;
TimingHandlerFunc timingInjectionWarning;
TimingHandlerFunc timingMillisecond;
TimingHandlerFunc timingPostMortem;
TimingHandlerFunc timingRecover;
TimingHandlerFunc timingSSC;
TimingHandlerFunc timingStartCycle;
TimingHandlerFunc timingStartRef;
TimingHandlerFunc timingStartRef1Warning;
TimingHandlerFunc timingStartRef2Warning;
TimingHandlerFunc timingStartRef3Warning;
TimingHandlerFunc timingStartRef4Warning;
TimingHandlerFunc timingTelegramReady;
TimingHandlerFunc timingToCycling;
TimingHandlerFunc timingToStandby;
TimingHandlerFunc timingTransactionCommit;
TimingHandlerFunc timingTransactionRollback;
TimingHandlerFunc timingPause;
TimingHandlerFunc timingResume;

#endif

// EOF
