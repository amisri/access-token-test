/*!
 * @file   cmwutil.h
 * @brief  Declarations for CMW utility functions
 * @author Stephen Page
 */

#ifndef CMWUTIL_H
#define CMWUTIL_H

#include <cmw-data/Data.h>
#include <cmw-rbac/authentication/Token.h>
#include <cmw-rda3/server/service/ServerBuilder.h>
#include <stdint.h>

#include <cmd.h>

using namespace cmw::data;
using namespace cmw::rda3::common;
using namespace cmw::rda3::server;

struct Cmwutil
{
    char              response_buf[CMD_MAX_VAL_LENGTH + 1];
    uint8_t           response_buf_type[(CMD_MAX_VAL_LENGTH / 2) * sizeof(int64_t)];
    double            response_xarray[(CMD_MAX_VAL_LENGTH / 2)];
    double            response_yarray[(CMD_MAX_VAL_LENGTH / 2)];
    pthread_mutex_t   response_buf_mutex;
};

extern struct Cmwutil cmwutil;

// External functions

/*!
 * Initialise CMW utility functions
 */

int32_t cmwutilInit(void);

/*!
 * Extra details of a CMW command
 */

void cmwutilExtractCommandDetails(const RequestContext&  context,
                                  const char            *device,
                                  uint32_t              *channel,
                                  uint32_t              *sub_device,
                                  uint16_t              *transaction_id,
                                  uint32_t              *user_num,
#ifdef MARKED_FOR_DELETION_EPCCCS_2623
                                  uint32_t              *getopt_bin,
#endif
                                  uint32_t              *array_start,
                                  uint32_t              *array_end,
                                  uint32_t              *array_step);

/*!
 * Extract an RBAC token from a CMW context if one exists
 */

rbac::Token *cmwutilExtractRBACToken(const Data& context_data);

/*!
 * Parse a selector
 */

uint32_t cmwutilParseSelector(const char *cycle, uint32_t *user_num);

/*!
 * Store a response in an rdaData object.
 *
 * The FGC can send data in ASCII or binary format. The default is ASCII, and the non-ASCII character 0xFF is
 * used as a magic number to switch to binary.
 *
 * The protocol is:
 *
 * response    ::= <field>{field}
 * field       ::= <ASCII field label>:<ASCII type>:<data_items>
 * data_items  ::= <data_item>{,<data_item>}<terminator>
 * data_item   ::= <ASCII item>|<binary_item>
 * binary_item ::= <byte:0xFF><uint32:length><byte:data>{<byte:data>}
 * terminator  ::= '\n'
 * 
 * If we receive a 0xFF character, the next item is in binary format. Multiple values can be sent in the same binary_item;
 * the number of items is equal to the length divided by the size of the type.
 *
 * If the item was sent by the FGCD (ID 0), it is in host order (little-endian for Intel Linux). If it was sent by an FGC
 * (ID > 0), it is in network order (big-endian) and needs to be converted.
 *
 * Note that for non-numeric types, the ASCII and binary representations are the same.
 */

AcquiredData *cmwutilResponseToRdaData(struct Cmd *command);

#endif

// EOF
