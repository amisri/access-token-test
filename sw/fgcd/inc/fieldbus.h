/*!
 * @file   fieldbus.timdt.h
 * @brief  Declarations for the fieldbus when using the timdt library
 * @author Marc Magrans de Abril
 */

#ifndef FIELDBUS_H
#define FIELDBUS_H

#include <pthread.h>
#include <stdint.h>
#include <list>

#include <timdt-lib-cpp/Timing.h>

// Constants

#define FIELDBUS_EVENT_MAX_TX_CYCLES    2                           //!< Maximum number of cycles in which an event will be transmitted
#define FIELDBUS_NUM_EVENT_SLOTS        (FGC_NUM_EVENT_SLOTS)       //!< Number of slots for events to send over the fieldbus
#define FIELDBUS_EVENT_QUEUE_MAX_ITEMS  1000                        //!< Maximum item count in the event queue

#include <consts.h>
#include <fgc_code.h>
#include <fgc_event.h>
#include <fgc_runlog.h>
#include <fgc_stat.h>

// Forward declarations

struct fgc_fieldbus_time;

/*!
 * Channel data
 */

struct Fieldbus_channel
{
    union fgc_stat_var  status;                             //!< Device status data
    uint8_t             runlog[FGC_RUNLOG_SIZE_BYTES];      //!< FGC run log
};

/*!
 * Timing events queue. It's not thread-safe, but it will be protected by fieldbus.event_mutex.
 */

class EventQueue
{
    public:
        //! Adds the event to the queue in a sorted way (sorts by delay)
        int32_t addEvent(const fgc_event& event);

        //! Iterates over each event and decrease its delay by milliseconds
        void decreaseDelay(size_t milliseconds);

        //! Returns true if the queue contains any events.
        bool hasEvents() const;

        //! Removes next event from the queue and returns it. Use hasEvents() before this function.
        fgc_event popEvent();

    private:
        std::list<fgc_event> queue;
};

/*!
 * Struct containing global variables
 */

struct Fieldbus
{
    //! Statistics

    struct Fieldbus_stats
    {
        // General

        uint32_t        cycle_count;                            //!< Count of fieldbus cycles

        //! Reception

        struct Fieldbus_stats_rx
        {
            uint32_t    load;                                   //!< Load in bytes per second
        } rx;

        //! Transmission

        struct Fieldbus_stats_tx
        {
            uint32_t    load;                                   //!< Load in bytes per second
        } tx;
    } stats;

    // Values

    int32_t             fgc_log_sync;                               //!< Counter to trigger sending of synchronisation signal for FGC logs
    uint32_t            fgc_power_sig_count;                        //!< Count of FGC power cycle triggers to send
    uint16_t            fgc_runlog_idx;                             //!< Index within FGC run logs
    uint32_t            global_pm_time_sec;                         //!< Timestamp of last global post-mortem event (seconds)
    uint32_t            global_pm_time_nsec;                        //!< Timestamp of last global post-mortem event (nanoseconds)
    uint32_t            pc_permit;                                  //!< pc_permit software interlock state
    int32_t             pc_permit_override;                         //!< pc_permit override (-1=force false, 1=force true, 0=no override)
    uint32_t            real_time_zero;                             //!< Flag to indicate whether to force all real-time values to zero
    uint32_t            sector_access;                              //!< sector_access software interlock state
    int32_t             sector_access_override;                     //!< sector_access override (-1=force false, 1=force true, 0=no override)

    struct fgc_event    event[FIELDBUS_NUM_EVENT_SLOTS];            //!< Slots for events to be sent over the fieldbus
    uint32_t            event_tx_cycles[FIELDBUS_NUM_EVENT_SLOTS];  //!< Number of cycles remaining during which events should be transmitted
    EventQueue          eventQueue;                                 //!< Queue for events buffering
    pthread_mutex_t     event_mutex;                                //!< Mutex protecting access to events

    float               fgc_code_votes[FGC_CODE_NUM_SLOTS];         //!< Vote values for devices requesting codes within the current cycle

    struct Fieldbus_channel channel[FGCD_MAX_DEVS];                 //!< Channel data
};

extern struct Fieldbus fieldbus;

// External functions

/*!
 * Initialise fieldbus
 */

int32_t fieldbusInit(void);

/*!
 * Prepare code message
 */

void fieldbusPrepareCode(struct fgc_fieldbus_time *time, uint8_t (*code_msg)[FGC_CODE_BLK_SIZE], uint32_t num_blocks);

/*!
 * Prepare time message
 */

void fieldbusPrepareTime(struct fgc_fieldbus_time *time);

/*!
 * Send an FGC event
 */

int32_t fieldbusSendEvent(struct fgc_event *event);

/*!
 * Transfers events from the event queue to the slots
 */

void fieldbusFillSlotsFromQueue(void);

/*!
 * Update events ready for next cycle
 */

void fieldbusUpdateEvents(void);

/*!
 * Calculate event time relative to start of next cycle
 */

void fieldbusEventTimeRelativeToNextCycle(tTimingTime evt_time, struct timeval *relative_time);

#endif

// EOF
