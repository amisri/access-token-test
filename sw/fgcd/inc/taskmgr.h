/*!
 * @file   taskmgr.h
 * @brief  Task management
 * @author Stephen Page
 */

#ifndef TASKMGR_H
#define TASKMGR_H

#include <stdint.h>

// Constants

// Status strings

#define TASK_FAIL_FATAL_STRING      "\033[31mFAIL\033[0m\n" //!< "FAIL" in red at current line column 50
#define TASK_FAIL_NON_FATAL_STRING  "\033[33mFAIL\033[0m\n" //!< "FAIL" in cyan at current line column 50
#define TASK_OKAY_STRING            "\033[32mDONE\033[0m\n" //!< "DONE" in green at current line column 50

/*!
 * Structure to describe a task
 */

struct Task
{
    const char  *label;
    int32_t     (*start_func)(void);
    int32_t     fatal;
    int32_t     *flag;
    int32_t     started;
};

extern struct Task tasks[];

// External functions

/*!
 * Start tasks
 */

int32_t taskmgrStart(void);

#endif

// EOF
