/*!
 * @file   queue.h
 * @brief  Declarations for queues (FIFOs)
 * @author Stephen Page
 */

#ifndef QUEUE_H
#define QUEUE_H

#include <pthread.h>
#include <stddef.h>
#include <stdint.h>

/*!
 * Queue structure
 */

struct Queue
{
    void            **buffer;                                   //!< Queue buffer
    size_t          buffer_size;                                //!< Buffer size
    uint32_t        back;                                       //!< Back of queue index
    uint32_t        front;                                      //!< Front of queue index
    pthread_mutex_t mutex;                                      //!< Mutex protecting access to queue
    pthread_cond_t  not_empty;                                  //!< Condition variable indicating that queue has content
    pthread_cond_t  not_full;                                   //!< Condition variable indicating that queue is not full
    void            (*pop_callback)(struct Queue *, void *);    //!< Call-back to be called when an element is popped
};

// External functions

/*!
 * Initialise the queue
 */

int32_t queueInit(struct Queue *q, size_t buffer_size);

/*!
 * Free the queue
 *
 * This function destroys the queue, so the calling function must guarantee that it is called not more than once.
 * Calling this function a second time on the same queue results in undefined behaviour.
 */

void queueFree(struct Queue *q);

/*!
 * Insert member into the queue
 */

int32_t queuePush(struct Queue *q, void *member);

/*!
 * Insert member into the queue (block if queue is full)
 */

int32_t queueBlockPush(struct Queue *q, void *member);

/*!
 * Extract member from the queue if present, without blocking
 */

void *queuePop(struct Queue *q);

/*!
 * Extract member from the queue (block if queue is empty)
 */

void *queueBlockPop(struct Queue *q);

#endif

// EOF
