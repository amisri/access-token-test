/*!
 * @file   crc.h
 * @brief  CRC checks
 * @author Stephen Page
 */

#ifndef CRC_H
#define CRC_H

#include <stdint.h>

// External functions

/*!
 * Perform 16 bit CRC
 */

uint16_t crcCrc16(uint8_t *data_p, uint32_t length);

#endif

// EOF
