/*!
 * @file   cmw.h
 * @brief  Declarations for interfacing with the Controls Middleware (CMW)
 * @author Stephen Page
 */

#ifndef CMW_H
#define CMW_H

#include <cmw-rda3/server/connection/ConnectionCallback.h>
#include <cmw-rda3/server/core/RequestReplyCallback.h>
#include <cmw-rda3/server/core/Server.h>
#include <cmw-rda3/server/core/DeviceInfoCallback.h>
#include <cmw-rda3/server/subscription/SubscriptionCallback.h>
#include <cmw-fwk/server/rbac/RbacCallbackFactory.h>
#include <pthread.h>
#include <set>
#include <stdint.h>

#include <consts.h>

#include <cmd.h>
#include <queue.h>

using namespace cmw::data;
using namespace cmw::rda3::common;
using namespace cmw::rda3::server;

/*!
 * Class to contain command data
 */

class cmw_command_data
{
    public:
        void        *rda_request;
        uint32_t    cmd_count;

        cmw_command_data(void *rda_request, uint32_t cmd_count)
        {
            this->rda_request   = rda_request;
            this->cmd_count     = cmd_count;
        }
};

/*!
 * Class to contain subscriber data
 */

class cmw_subscriber
{
    public:

        //! Enumeration to indicate the last type of value sent to the subscription

        enum
        {
            LAST_VALUE_NONE,
            LAST_VALUE_ERROR,
            LAST_VALUE_VALID,
        } last_value_type;

        SubscriptionSourceSharedPtr source;
        uint32_t                    sub_device;
        uint16_t                    transaction_id;
        uint32_t                    user;

        cmw_subscriber(SubscriptionSourceSharedPtr source, uint16_t transaction_id, uint32_t user, unsigned int sub_device)
        {
            this->source            = source;
            this->transaction_id    = transaction_id;
            this->user              = user;
            this->sub_device        = sub_device;
            last_value_type         = LAST_VALUE_NONE;
        }
};

class cmwFgcd : public ConnectionCallback, RequestReplyCallback, SubscriptionCallback, DeviceInfoCallback

{
    public:
        cmwFgcd(const char *name);
        virtual ~cmwFgcd(void);

        /*!
         * Run the CMW server
         */

        void runServer(void);

        /*!
         * Stop the CMW server
         */

        void stopServer(void);

        /*!
         * Connection callback
         */

        virtual void clientConnected(const ServerConnection& connection);

        /*!
         * Disconnection callback
         */

        virtual void clientDisconnected(const ServerConnection& connection);

        /*!
         * Callback to set a session priority
         */

        virtual RdaPriority provideSessionPriority(const Session & session);

        /*!
         * Get callback
         */

        virtual void get(std::unique_ptr<GetRequest> request);

        /*!
         * Set callback
         */

        virtual void set(std::unique_ptr<SetRequest> request);

        /*!
         * Subscribe callback
         */

        virtual void subscribe(SubscriptionRequest& request);

        /*!
         * Subscription source added callback
         */

        virtual void subscriptionSourceAdded(const SubscriptionSourceSharedPtr& subscription);

        /*!
         * Subscription source removed callback
         */

        virtual void subscriptionSourceRemoved(const SubscriptionSourceSharedPtr& subscription);

        /*!
         * Unsubscribe callback
         */

        virtual void unsubscribe(const cmw::rda3::common::Request& request);

        /*!
         * Publish data
         */

        void publish(void);

        /*!
         * CMW Helper callback to retrieve the class name for a given device name
         */

        virtual std::string getDeviceClass(const std::string &deviceName);

    private:
        std::unique_ptr<Server>     server;

        char                        set_buffer[CMD_MAX_VAL_LENGTH + 1];
        pthread_mutex_t             set_buffer_mutex;

        std::set<cmw_subscriber *>  subscribers[FGCD_MAX_DEVS];
        pthread_mutex_t             subscriber_mutex;

        /*!
         * Remove a subscription from a channel
         */

        int32_t remove_subscription(uint32_t channel, const SubscriptionSource& subscription);

        /*!
         * Prepare an FGC command from an RDA request
         */

        struct Cmd *prepareCommand(const ServerRequest& request, enum Cmd_type type, const Entry *entry);

        /*!
         * Construct an FGC command string from an RDA request
         */

        uint32_t constructCommandString(const ServerRequest& request, enum Cmd_type type, const Entry *entry, struct Cmd *command);

        /*!
         * Append data within a Data Entry to command string
         */

        uint32_t dataEntryToCmd(const Entry& entry, struct Cmd *command);

        /*!
         * Send a command
         */

        void sendCommand(struct Cmd *command, cmw_command_data *command_data);

        /*!
         * Write command details to log
         */

        void logCommand(const RemoteHostInfo& client, const struct Cmd *command, const enum Cmd_type cmd_type);

    private:
        /** Hidden assignment operator */
        void operator =(const DeviceInfoCallback &);
};



// External functions

/*!
 * Start the CMW server
 */

int  cmwStart(void);

/*!
 * Stop the CMW server
 */

void cmwStop(void);



/*!
 * Struct containing global variables
 */

struct CMW_globals
{
    uint32_t                                  cmds_received;      //!< A count of the total number of received commands
    uint32_t                                  num_cmds;           //!< The number of commands currently being processed
    struct Queue                              response_queue;     //!< A queue of responses to return to clients
    pthread_t                                 response_thread;    //!< A thread to process responses and return them to clients
    cmwFgcd*                                  server;             //!< The CMW server
    pthread_t                                 server_thread;      //!< A thread for the CMW server
    std::unique_ptr<cmw::RbacCallbackFactory> rbacFactory;        //!< Used to get MCS validation callback
};

extern struct CMW_globals cmw_globals;

#endif

// EOF
