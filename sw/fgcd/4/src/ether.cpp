/*!
 * @file   ether.c
 * @author Stephen Page
 *
 * Functions for managing the FGC_Ether fieldbus
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <poll.h>

extern "C"
{
    unsigned int if_nametoindex(__const char *__ifname);
}

#include <cmdqmgr.h>
#include <consts.h>
#include <ether.h>
#include <fgc_ether.h>
#include <fgcd.h>
#include <fgcsm.h>
#include <fieldbus.h>
#include <logging.h>
#include <queue.h>
#include <tcp.h>
#include <timing.h>
#include <rt.h>
#include <fgcd_thread.h>



// Global variables

struct Ether ether = {};



// Static functions

static FGCD_thread_func etherRunReception;

static void etherHandlePub(uint32_t channel_num, struct fgc_ether_pub *frame, ssize_t size);
static void etherHandleRsp(uint32_t channel_num, struct fgc_ether_rsp *frame, ssize_t size);
static void etherHandleStatus(uint32_t channel_num, struct fgc_ether_status *frame, ssize_t size);



int32_t etherStart(void)
{
    struct ifreq        ifreq;          // Interface data
    struct sockaddr_ll  sockaddr;       // Socket configuration
    struct tpacket_req  tp;

    // Initialise the fieldbus

    if(fieldbusInit() != 0) return 1;

    // Initialise FGC state machine

    fgcsmInit();

    // Open socket

    if((ether.socket = socket(AF_PACKET, SOCK_RAW, 0)) < 0)
    {
        perror("socket");
        return 1;
    }

    // Configure the reception ring buffer

    tp.tp_block_size    = ETHER_RX_RING_SMEM_SIZE;
    tp.tp_block_nr      = 1;
    tp.tp_frame_size    = getpagesize();
    tp.tp_frame_nr      = ETHER_RX_RING_FRAMES;

    if(setsockopt(ether.socket, SOL_PACKET, PACKET_RX_RING, (void *)&tp, sizeof(tp)))
    {
        perror("setsockopt");
        close(ether.socket);
        return 1;
    }

    // Memory map the reception ring buffer

    ether.rx_ring_buffer = reinterpret_cast<char*>(mmap(0, ETHER_RX_RING_SMEM_SIZE,PROT_READ | PROT_WRITE, MAP_SHARED, ether.socket, 0));
    if(!ether.rx_ring_buffer)
    {
        perror("mmap");
        close(ether.socket);
        return 1;
    }

    // Get the MAC address of the network interface

    strncpy(ifreq.ifr_name, ETHER_NETWORK_INTERFACE, sizeof(ifreq.ifr_name) - 1);
    ifreq.ifr_name[sizeof(ifreq.ifr_name) - 1] = '\0';

    if(ioctl(ether.socket, SIOCGIFHWADDR, &ifreq))
    {
        perror("ioctl(SIOCGIFHWADDR)");
        munmap(ether.rx_ring_buffer, ETHER_RX_RING_SMEM_SIZE);
        close(ether.socket);
        return 1;
    }
    memcpy(ether.interface_address, ifreq.ifr_hwaddr.sa_data, ETH_ALEN);

    // Configure sockaddr

    memset((void *)&sockaddr, 0, sizeof(sockaddr));
    sockaddr.sll_family     = AF_PACKET;
    sockaddr.sll_halen      = ETH_ALEN;
    sockaddr.sll_hatype     = ARPHRD_ETHER;
    sockaddr.sll_pkttype    = PACKET_OTHERHOST;
    sockaddr.sll_protocol   = htons(FGC_ETHER_TYPE);

    if(!(ether.interface_index = if_nametoindex(ETHER_NETWORK_INTERFACE)))
    {
        fprintf(stderr, "ERROR: Failed to get index for interface %s\n", ETHER_NETWORK_INTERFACE);
        munmap(ether.rx_ring_buffer, ETHER_RX_RING_SMEM_SIZE);
        close(ether.socket);
        return 1;
    }
    sockaddr.sll_ifindex = ether.interface_index;

    // Bind to socket

    if(bind(ether.socket, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) < 0)
    {
        perror("bind");
        munmap(ether.rx_ring_buffer, ETHER_RX_RING_SMEM_SIZE);
        close(ether.socket);
        return 1;
    }

    // Create reception thread

    if(fgcd_thread_create(&ether.reception_thread, etherRunReception, NULL, ETHER_RECEIVE_THREAD_POLICY, ETHER_RECEIVE_THREAD_PRIORITY, NO_AFFINITY) != 0)
    {
        munmap(ether.rx_ring_buffer, ETHER_RX_RING_SMEM_SIZE);
        close(ether.socket);
        return 1;
    }

    return 0;
}



/*
 * Receive data from the fieldbus
 */

static void *etherRunReception(void *unused)
{
    uint8_t             addr_start[] = { 0x02, 0x00, 0x00, 0x00, 0x00 };    // Expected start of FGC address
    uint32_t            channel_num;                                        // Number of channel that sent the frame
    volatile union frame_t
    {
        struct fgc_ether_frame  generic;

        struct fgc_ether_pub    pub;
        struct fgc_ether_rsp    rsp;
        struct fgc_ether_status status;
    }                            *frame;                                    // Buffer to receive FGC_Ether frame
    volatile struct tpacket_hdr  *header;                                   // Pointer to a packet header within the receive ring buffer
    struct pollfd       poll_set;                                           // Set to allow frame reception to be awaited with poll()
    ssize_t             size;                                               // Size of received frame

    while(1)
    {
        // Set the header pointer to the next element in the ring buffer

        header = reinterpret_cast<tpacket_hdr*>(ether.rx_ring_buffer + (ether.rx_ring_offset * getpagesize()));

        // Wait for the frame to be ready to read in user space

        while(!(header->tp_status & TP_STATUS_USER))
        {
            // Wait for the socket to be readable

            poll_set.fd      = ether.socket;
            poll_set.events  = POLLIN;
            poll_set.revents = 0;

            if(poll(&poll_set, 1, -1) == -1 && errno != EINTR)
            {
                perror("poll");
                continue;
            }
        }

        // Check whether frames are being lost

        if(header->tp_status & TP_STATUS_LOSING)
        {
            logPrintf(0, "ETHER Losing frames\n");
        }

        // Set the frame pointer and size

        frame = reinterpret_cast<volatile frame_t*>(reinterpret_cast<volatile char*>(header) + header->tp_mac);
        size  = header->tp_len;

        fieldbus.stats.rx.load += size;
        ether.stats.rx.frames++;

        // Validate and process the frame

        if(size < static_cast<ssize_t>(sizeof(frame->generic.header)))
        {
            // The frame is too small to contain a header

            ether.stats.rx.bad_frames++;
            ether.stats.rx.bad_size++;
        }
        else if(memcmp(const_cast<uint8_t*>(frame->generic.header.ethernet.src_addr), addr_start, sizeof(addr_start)) ||
                frame->generic.header.ethernet.src_addr[5] >= FGCD_MAX_DEVS)
        {
            // The frame did not come from a valid FGC address

            ether.stats.rx.bad_frames++;
            ether.stats.rx.bad_address++;
        }
        else
        {
            // The frame is valid, handle its payload

            channel_num = frame->generic.header.ethernet.src_addr[5];

            if(frame->generic.header.fgc.payload_type < FGC_ETHER_PAYLOAD_NUM_TYPES)
            {
                switch(frame->generic.header.fgc.payload_type)
                {
                    case FGC_ETHER_PAYLOAD_PUB:
                        etherHandlePub(channel_num, const_cast<struct fgc_ether_pub*>(&frame->pub), size);
                        break;

                    case FGC_ETHER_PAYLOAD_RSP:
                        etherHandleRsp(channel_num, const_cast<struct fgc_ether_rsp*>(&frame->rsp), size);
                        break;

                    case FGC_ETHER_PAYLOAD_STATUS:
                        etherHandleStatus(channel_num, const_cast<struct fgc_ether_status*>(&frame->status), size);
                        break;

                    default: // Unhandled payload type
                        break;
                }
            }
            else // The payload type is invalid
            {
                ether.stats.rx.bad_frames++;
                logPrintf(channel_num, "ETHER Unexpected payload type %hhu\n",
                          frame->generic.header.fgc.payload_type);
            }
        }

        // Hand the frame buffer back to the kernel and advance to the next element within the ring buffer

        header->tp_status    = TP_STATUS_KERNEL;
        ether.rx_ring_offset = (ether.rx_ring_offset + 1) % ETHER_RX_RING_FRAMES;
    }

    return 0;
}



/*
 * Handle a received publication frame
 */

static void etherHandlePub(uint32_t channel_num, struct fgc_ether_pub *frame, ssize_t size)
{
    // Check that no terminal characters were returned as they are not supported

    if(frame->payload.data.header.n_term_chs)
    {
        ether.stats.rx.bad_frames++;
        logPrintf(channel_num, "ETHER %hhu terminal characters returned in publication frame\n",
                  frame->payload.data.header.n_term_chs);
        return;
    }

    // Byte swap payload length

    frame->payload.length = ntohs(frame->payload.length);

    // Process published data

    fgcsmProcessPublishMsg(channel_num, &frame->payload.data,
                           frame->payload.length - sizeof(frame->payload.data.header));
}



/*
 * Handle a received command response frame
 */

static void etherHandleRsp(uint32_t channel_num, struct fgc_ether_rsp *frame, ssize_t size)
{
    // Check that no terminal characters were returned as they are not supported

    if(frame->payload.data.header.n_term_chs)
    {
        ether.stats.rx.bad_frames++;
        logPrintf(channel_num, "ETHER %hhu terminal characters returned in response frame\n",
                  frame->payload.data.header.n_term_chs);
        return;
    }

    // Byte swap payload length

    frame->payload.length = ntohs(frame->payload.length);

    // Process response

    fgcsmProcessResponse(channel_num, &frame->payload.data,
                         frame->payload.length - sizeof(frame->payload.data.header));
}



/*
 * Handle a received status frame
 */

static void etherHandleStatus(uint32_t channel_num, struct fgc_ether_status *frame, ssize_t size)
{
    uint32_t            i;
    struct Rterm_resp   *rterm_resp;    // Remote terminal response
    uint32_t            rterm_resp_len; // Length of remote terminal response

    // Check that frame is the expected size

    if(size != sizeof(*frame))
    {
        ether.stats.rx.bad_frames++;
        ether.stats.rx.bad_size++;
        return;
    }

    // Copy FGC status structure

    memcpy(&fieldbus.channel[channel_num].status,
           &frame->payload.status, sizeof(frame->payload.status));

    // Process status

    fgcsmProcessStatus(frame->header.ethernet.src_addr[5]);

    // Process any remote terminal data

    frame->payload.rterm[sizeof(frame->payload.rterm) - 1] = '\0';
    rterm_resp_len = strlen(frame->payload.rterm);

    if(frame->payload.rterm[0] != '\0') // There is at least 1 character of remote terminal data
    {
        // Check whether each client has a remote terminal on this channel

        for(i = 0 ; i < FGC_TCP_MAX_CLIENTS ; i++)
        {
            if(tcp.client[i].run                                       &&
               tcp.client[i].rterm_device == &fgcd.device[channel_num] &&
               (rterm_resp = (struct Rterm_resp *)queuePop(&cmdqmgr.free_rterm_q)))
            {
                strncpy(rterm_resp->buffer, frame->payload.rterm, sizeof(frame->payload.rterm));
                rterm_resp->buffer[rterm_resp_len]  = '\0';
                rterm_resp->buffer_length           = rterm_resp_len;

                pthread_mutex_lock(&tcp.client[i].mutex);
                tcp.client[i].num_rterm_resps++;
                pthread_mutex_unlock(&tcp.client[i].mutex);

                queuePush(&tcp.client[i].response_queue, rterm_resp);
                rterm_resp = NULL;
            }
        }
    }
}



ssize_t etherSend(uint32_t channel_num, struct fgc_ether_frame *frame, uint32_t size)
{
    ssize_t             sent_size;
    struct sockaddr_ll  sockaddr;

    // Increment count of frames sent

    ether.stats.tx.frames++;

    // Set source address

    memcpy(&frame->header.ethernet.src_addr,
           ether.interface_address, ETH_ALEN);

    // Set destination address

    if(channel_num == 0) // Broadcast
    {
        // Set destination address to broadcast address

        memset(frame->header.ethernet.dest_addr, 0xFF, ETH_ALEN);
    }
    else
    {
        // Check that channel is valid

        if(channel_num >= FGCD_MAX_DEVS)
        {
            logPrintf(0, "ETHER Attempt to send to channel out of range (%u)\n", channel_num);
            ether.stats.tx.failed++;
            return -1;
        }

        // Set destination address
        // Address is (02:00:00:00:00:channel_num)

        memset(frame->header.ethernet.dest_addr, 0x00, ETH_ALEN);
        frame->header.ethernet.dest_addr[0] = 0x02;     // Indicates a locally administered address
        frame->header.ethernet.dest_addr[5] = channel_num;
    }

    // Set ether type in Ethernet header

    frame->header.ethernet.ether_type = htons(FGC_ETHER_TYPE);

    // Configure sockaddr

    sockaddr.sll_family     = AF_PACKET;
    sockaddr.sll_halen      = ETH_ALEN;
    sockaddr.sll_hatype     = ARPHRD_ETHER;
    sockaddr.sll_ifindex    = ether.interface_index;
    sockaddr.sll_pkttype    = PACKET_OTHERHOST;
    sockaddr.sll_protocol   = htons(FGC_ETHER_TYPE);

    // Send frame

    sent_size = sendto(ether.socket, frame, size, 0,
                       (struct sockaddr *)&sockaddr, sizeof(sockaddr));

    if(sent_size == size) // Send succeeded
    {
        // Add bytes to load

        fieldbus.stats.tx.load += sent_size;
    }
    else // Send failed
    {
        ether.stats.tx.failed++;
    }

    return sent_size;
}



int32_t etherSendTime(void)
{
    struct fgc_ether_time   *frame = &ether.frames.time;
    struct timeval          time;

    // Set payload type

    frame->header.fgc.payload_type = FGC_ETHER_PAYLOAD_TIME;

    // Set time within frame

    timingGetUserTime(0, &time);
    frame->payload.time.unix_time   = htonl(time.tv_sec);
    frame->payload.time.ms_time     = htons(time.tv_usec / 1000);

    // Send frame

    if(etherSend(0, (struct fgc_ether_frame *)frame, sizeof(*frame)) != sizeof(*frame))
    {
        return 1;
    }
    return 0;
}



int32_t etherSendCommand(uint32_t channel_num, struct Cmd *cmd)
{
    struct fgc_ether_cmd    *frame = &ether.frames.command;
    uint32_t                frame_len;

    // Set payload type

    frame->header.fgc.payload_type = FGC_ETHER_PAYLOAD_CMD;

    // Prepare command to be sent

    if(!(frame->payload.length = fgcsmPrepareCommand(channel_num, cmd, &frame->payload.data)))
    {
        // No data can be sent

        return 0;
    }

    // Ensure that frame_len is at least the minimum size

    if(frame->payload.length >= FGC_FIELDBUS_MIN_CMD_LEN)
    {
        frame_len = sizeof(frame->header) + sizeof(frame->payload.length) + frame->payload.length;
    }
    else // Frame content is below the minimum size of an Ethernet frame
    {
        frame_len = sizeof(frame->header) + FGC_ETHER_PAYLOAD_MIN;
    }

    // Byte swap payload length

    frame->payload.length = htons(frame->payload.length);

    // Send command frame

    if(etherSend(channel_num, (struct fgc_ether_frame *)frame, frame_len) != frame_len)
    {
        return 1;
    }

    // Reset sequential toggle bit miss count

    fgcsm.channel[channel_num].count.seq_toggle_bit_miss = 0;

    return 0;
}



void etherPrepareRT(void)
{
    union { float f; uint32_t l; }  f_l;
    uint32_t                        i;

    // Send real-time references

    pthread_mutex_lock(&rt.mutex);

    for(i = 0 ; i < FGCD_MAX_EQP_DEVS ; i++)
    {
        if(rt.data[rt.data_start].active_channels & (1LLU << i)) // Channel is active
        {
            // Set new value

            f_l.f = rt.used_data.channels[i]    = rt.data[rt.data_start].channels[i];
            f_l.l                               = htonl(f_l.l);
            ether.frames.time.payload.rt_ref[i] = f_l.f;
        }
        else // Channel is inactive
        {
            // Retain previous value

            f_l.f                               = rt.used_data.channels[i];
            f_l.l                               = htonl(f_l.l);
            ether.frames.time.payload.rt_ref[i] = f_l.f;
        }
    }

    // Clear active channels for this reference and advance data start

    rt.data[rt.data_start].active_channels  = 0;
    rt.data_start                           = (rt.data_start + 1) % RT_BUFFER_SIZE;

    pthread_mutex_unlock(&rt.mutex);

    // Send zero reference if real_time_zero flag set

    if(fieldbus.real_time_zero)
    {
        memset(ether.frames.time.payload.rt_ref, 0,
               sizeof(ether.frames.time.payload.rt_ref));
    }
}

// EOF
