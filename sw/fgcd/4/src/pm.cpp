/*!
 * @file   pm.cpp
 * @author Stephen Page
 * @author Michael Davis
 *
 * Functions for submitting Post Mortem data
 */

#include <arpa/inet.h>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <pm-dc-rda3-lib/PMClient.hpp>
#include <pm-dc-rda3-lib/PMData.hpp>
#include <pm-dc-rda3-lib/PMError.hpp>
#include <sys/types.h>
#include <unistd.h>

#include <cmd.h>
#include <cmdqmgr.h>
#include <defconst.h>
#include <fgcd.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <logging.h>
#include <pm.h>
#include <pmd_63.h>
#include <queue.h>
#include <rt.h>



// Global variables

struct Pm pm_globals;


// Static functions

// Thread to submit post-mortem data

static void *pmRun(void *unused)
{
    // Create post-mortem client

    pm::Client pmclient = pm::Client::createProClient(PM_SERVICE_NAME);

    // Create queue for FGC responses

    struct Queue response_queue;

    // Initialise queue

    if(queueInit(&response_queue, 1) != 0)
    {
        logPrintf(0, "PM Failed to initialise queue\n");
        return nullptr;
    }

    // Handle post-mortem events

    while(1)
    {
        // Wait for a post-mortem event

        pthread_testcancel();
        sem_wait(&pm_globals.sem);
        pthread_testcancel();

        // Scan devices for frozen post-mortem buffers

        for(uint32_t device_id = 1 ; device_id < FGCD_MAX_DEVS ; device_id++)
        {
            struct FGCD_device *device = &fgcd.device[device_id];

            // Check whether a post-mortem buffer for this device is ready for processing

            if(device->pm_ext.acq_info.frozen) // Externally-triggered post-mortem
            {
                struct PM_ext_acq *ext_acq = &device->pm_ext;

                logPrintf(device_id, "PM Dump (external) started\n");

                // Copy frozen post-mortem buffer

                pm_globals.buffer.ext.trigger = ext_acq->buffer.trigger;
                uint32_t pm_index = 0;
                uint32_t device_pm_index = ext_acq->acq_info.status_index;

                do
                {
                    pm_globals.buffer.ext.status[pm_index++] = ext_acq->buffer.status[device_pm_index];
                    device_pm_index = (device_pm_index + 1) % PM_EXT_BUF_LEN;
                } while(device_pm_index != ext_acq->acq_info.status_index);

                // Generate and send post-mortem data

                int64_t timestamp = ((int64_t)ext_acq->acq_info.time_sec * 1000000000) +
                                    ((int64_t)ext_acq->acq_info.time_nsec);

                pmd_63GenerateAndSendExt(pmclient, device, pm_globals.buffer.ext, timestamp);

                // Un-freeze device post-mortem buffer

                ext_acq->acq_info.frozen = 0;
                logPrintf(device_id, "PM Dump (external) completed\n");
            }

            if( device->pm_self.acq_info.frozen &&
               !device->pm_self.wait_fgc_logs   &&
               !device->pm_self.cmds_queued) // Self-triggered post-mortem
            {
                struct PM_self_acq *self_acq = &device->pm_self;

                logPrintf(device_id, "PM Dump (self) started\n");

                // Copy frozen post-mortem buffer

                pm_globals.buffer.self.trigger = self_acq->buffer.trigger;
                uint32_t pm_index = 0;
                uint32_t device_pm_index = self_acq->acq_info.status_index;

                switch(device->fgc_class)
                {
                    default:
                        do
                        {
                            pm_globals.buffer.self.std.status[pm_index++] = self_acq->buffer.std.status[device_pm_index];
                            device_pm_index = (device_pm_index + 1) % PM_SELF_BUF_LEN_STD;
                        } while(device_pm_index != self_acq->acq_info.status_index);

                        break;
                }

                // Check whether a post-mortem response has been returned from the FGC

                if(self_acq->response)
                {
                    // Check whether FGC response length is valid

                    if(self_acq->response->value_length - FGC_BIN_HEADER_LEN < PM_FGC_LOG_MAX_LEN) // Length is valid
                    {
                        logPrintf(device_id, "PM Logs retrieved, length %u bytes\n",
                                  self_acq->response->value_length);

                        switch(device->fgc_class)
                        {
                            default:
                                memcpy(&pm_globals.buffer.self.std.fgc_buffers,
                                       &self_acq->response->value[FGC_BIN_HEADER_LEN],
                                       self_acq->response->value_length - FGC_BIN_HEADER_LEN);
                                break;
                        }
                    }
                    else // FGC response is too large
                    {
                        logPrintf(device_id, "PM Log length %u bytes is too large (max %u)\n",
                                  self_acq->response->value_length - FGC_BIN_HEADER_LEN,
                                  PM_FGC_LOG_MAX_LEN);

                        // Zero FGC data in post-mortem buffer

                        switch(device->fgc_class)
                        {
                            default:
                                memset(&pm_globals.buffer.self.std.fgc_buffers, 0, PM_FGC_LOG_MAX_LEN);
                                break;
                        }
                    }
                }
                else // No post-mortem response from the FGC
                {
                    // Zero FGC data in post-mortem buffer
                    // FIXME: This just causes an exception in the subsequent code ("FGC's LOG.PM.BUF data version is not compatible with FGCD (1 expected)")

                    switch(device->fgc_class)
                    {
                        default:
                            memset(&pm_globals.buffer.self.std.fgc_buffers, 0, PM_FGC_LOG_MAX_LEN);
                            break;
                    }
                }

                // Generate and send post-mortem data

                int64_t timestamp = ((int64_t)self_acq->acq_info.time_sec * 1000000000) +
                                    ((int64_t)self_acq->acq_info.time_nsec);

                pmd_63GenerateAndSendSelf(pmclient, device, pm_globals.buffer.self.std, timestamp);

                // Return the self acquisition command to the free command queue

                if(self_acq->response)
                {
                    queuePush(&cmdqmgr.free_cmd_q, self_acq->response);
                }

                // Reset the log buffer

                struct Cmd *command;

                if(!(command = (struct Cmd *)queueBlockPop(&cmdqmgr.free_cmd_q)))
                {
                   // Failed to get command structure

                   logPrintf(0, "PM Failed to get command structure. Cannot reset PM LOG buffer\n");
                }
                else
                {
                    command->type            = CMD_SET;
                    command->client          = CMD_FGCD_CLIENT;
                    snprintf(command->device_name, FGC_MAX_DEV_LEN + 1, "%u", device->id);
                    command->command_length  = snprintf(command->command_string, CMD_MAX_LENGTH + 1, "LOG");
                    command->value_length    = snprintf(command->value, CMD_MAX_VAL_LENGTH + 1, "RESET");
                    command->response_queue  = &response_queue;
                    cmdqmgrQueue(command);

                    // Wait for a response. This is to ensure that the updated device status has propagated back to FGCD before we
                    // unlock the post-mortem buffer and allow other threads look at FGC_SELF_PM_REQ/FGC_POST_MORTEM flags again.
                    // Finally return the command structure.

                    queueBlockPop(&response_queue);
                    queuePush(&cmdqmgr.free_cmd_q, command);
                }

                // Un-freeze device post-mortem buffer

                self_acq->acq_info.frozen = 0;
                logPrintf(device_id, "PM Dump (self) completed\n");
            }
        }
    }

    queueFree(&response_queue);
    return 0;
}



// External functions

int32_t pmStart(void)
{
    // Initialise semaphore

    if(sem_init(&pm_globals.sem, 0, 0) < 0)
    {
        perror("sem_init pm sem");
        return 1;
    }

    // Initialise command response queues

    for(uint32_t i = 0 ; i < FGCD_MAX_DEVS ; i++)
    {
        queueInit(&fgcd.device[i].pm_self.cmd_response_queue, PM_NUM_CMDS_PER_DEV);
    }

    // Start pm thread

    if(fgcd_thread_create(&pm_globals.thread, pmRun, NULL, PM_THREAD_POLICY, PM_THREAD_PRIORITY, NO_AFFINITY) != 0)
    {
        sem_destroy(&pm_globals.sem);
        return 1;
    }

    return 0;
}



void pmPubUpdate(uint8_t channel, struct fgc_stat *status, uint32_t time_sec, uint32_t time_usec)
{
    struct FGCD_device  *device = &fgcd.device[channel];
    union
    {
        float    f;
        uint32_t l;
    }                   pm_real_time_value = {};
    struct PM_data      *pm_sample;

    // Store real-time value

    if(channel != 0) // Not the FGCD device
    {
        // Byte-swap real-time value

        pm_real_time_value.f = rt.used_data.channels[channel - 1];
        pm_real_time_value.l = htonl(pm_real_time_value.l);
    }

    // Add status data to external-trigger post-mortem buffer

    if(!device->pm_ext.acq_info.frozen) // Post-mortem buffer is not frozen
    {
        // Copy data to post-mortem buffer and increment index

        pm_sample = &device->pm_ext.buffer.status[device->pm_ext.acq_info.status_index];

        pm_sample->time_sec  = time_sec;
        pm_sample->time_usec = time_usec;
        pm_sample->real_time = pm_real_time_value.f;
        pm_sample->status    = *status;

        device->pm_ext.acq_info.status_index = (device->pm_ext.acq_info.status_index + 1) % PM_EXT_BUF_LEN;
    }

    // Add status data to self-trigger post-mortem buffer

    if(!device->pm_self.acq_info.frozen) // Post-mortem buffer is not frozen
    {
        // Copy data to post-mortem buffer and increment index

        pm_sample = &device->pm_self.buffer.std.status[device->pm_self.acq_info.status_index];

        pm_sample->time_sec  =  time_sec;
        pm_sample->time_usec =  time_usec;
        pm_sample->real_time =  pm_real_time_value.f;
        pm_sample->status    = *status;

        device->pm_self.acq_info.status_index = (device->pm_self.acq_info.status_index + 1) % PM_SELF_BUF_LEN_STD;
    }
}

// EOF
