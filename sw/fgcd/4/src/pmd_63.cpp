/*!
 * @file   pmd_63.cpp
 * @brief  Handle FGC class 63 post-mortem data
 * @author Stephen Page
 * @author Michael Davis
 * @author Quentin King
 */

#include <arpa/inet.h>
#include <cstdint>
#include <cstring>
#include <pm-dc-rda3-lib/PMClient.hpp>
#include <pm-dc-rda3-lib/PMData.hpp>
#include <pm-dc-rda3-lib/PMError.hpp>

#include <classes/63/sub_defsyms.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <classes/63/pm_buf_63.h>
#include <sub_utilities.h>


// Global variables

const uint8_t  MAX_SUB_STRING_LENGTH = 255;
const uint32_t PM_STATUS_MAX_BUF_LEN = (PM_SELF_BUF_LEN_STD>PM_EXT_BUF_LEN)?PM_SELF_BUF_LEN_STD:PM_EXT_BUF_LEN;

//! Data structure to store the intermediate status arrays to produce the PMD data

struct PM_63_status_arrays
{
    uint32_t  num_samples;
    int64_t   timestamps[PM_STATUS_MAX_BUF_LEN];
    int       class_id[PM_STATUS_MAX_BUF_LEN];
    char      data_status[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     data_status_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_faults[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_faults_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_warnings[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_warnings_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_latched[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_latched_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_unlatched[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_unlatched_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_op[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_op_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_vs[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_vs_ptr[PM_STATUS_MAX_BUF_LEN];
    char      state_pc[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     state_pc_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_meas_a[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_meas_a_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_meas_b[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_meas_b_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_dcct_a[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_dcct_a_ptr[PM_STATUS_MAX_BUF_LEN];
    char      st_dcct_b[PM_STATUS_MAX_BUF_LEN][MAX_SUB_STRING_LENGTH+1];
    char*     st_dcct_b_ptr[PM_STATUS_MAX_BUF_LEN];
    int16_t   i_err_ma[PM_STATUS_MAX_BUF_LEN];
    int16_t   i_diff_ma[PM_STATUS_MAX_BUF_LEN];
    float     i_earth_cpcnt[PM_STATUS_MAX_BUF_LEN];
    float     i_meas[PM_STATUS_MAX_BUF_LEN];
    float     i_ref[PM_STATUS_MAX_BUF_LEN];
    float     v_meas[PM_STATUS_MAX_BUF_LEN];
    float     v_ref[PM_STATUS_MAX_BUF_LEN];

} pm_status_arrays;



static void pmd_63AddStatus(PMData& pmdata, int64_t timestamp, const struct fgc_stat* trigger, const struct PM_data* status, uint32_t LEN)
{
    const fgc63_stat *c63 = &trigger->class_data.c63;

    // Parameters: Trigger data

    pmdata.registerParameter("TIMESTAMP", timestamp);

    subUtilitiesPmdBitmaskToText(trigger->data_status,sym_names_63_data_status,pm_status_arrays.data_status[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("DATA_STATUS",pm_status_arrays.data_status[0]);

    pmdata.registerParameter("CLASS_ID", (int32_t)trigger->class_id);

    subUtilitiesPmdBitmaskToText(ntohs(c63->st_faults),sym_names_63_flt,pm_status_arrays.st_faults[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_FAULTS",pm_status_arrays.st_faults[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c63->st_warnings),sym_names_63_wrn,pm_status_arrays.st_warnings[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_WARNINGS",pm_status_arrays.st_warnings[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c63->st_latched),sym_names_63_lat,pm_status_arrays.st_latched[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_LATCHED",pm_status_arrays.st_latched[0]);

    subUtilitiesPmdBitmaskToText(ntohs(c63->st_unlatched),sym_names_63_unl,pm_status_arrays.st_unlatched[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_UNLATCHED",pm_status_arrays.st_unlatched[0]);

    subUtilitiesPmdEnumToText(c63->state_op,sym_names_63_op,pm_status_arrays.state_op[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_OP",pm_status_arrays.state_op[0]);

    subUtilitiesPmdEnumToText(c63->state_vs,sym_names_63_vs,pm_status_arrays.state_vs[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_VS",pm_status_arrays.state_vs[0]);

    subUtilitiesPmdEnumToText(c63->state_pc,sym_names_63_pc,pm_status_arrays.state_pc[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("STATE_PC",pm_status_arrays.state_pc[0]);

    subUtilitiesPmdBitmaskToText(c63->st_adc_a,sym_names_63_adc_status,pm_status_arrays.st_meas_a[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_MEAS_A",pm_status_arrays.st_meas_a[0]);

    subUtilitiesPmdBitmaskToText(c63->st_adc_b,sym_names_63_adc_status,pm_status_arrays.st_meas_b[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_MEAS_B",pm_status_arrays.st_meas_b[0]);

    subUtilitiesPmdBitmaskToText(c63->st_dcct_a,sym_names_63_dcct,pm_status_arrays.st_dcct_a[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_DCCT_A",pm_status_arrays.st_dcct_a[0]);

    subUtilitiesPmdBitmaskToText(c63->st_dcct_b,sym_names_63_dcct,pm_status_arrays.st_dcct_b[0],MAX_SUB_STRING_LENGTH);
    pmdata.registerParameter("ST_DCCT_B",pm_status_arrays.st_dcct_b[0]);

    pmdata.registerParameter("I_ERR_MA",subUtilitiesSignedNtohs(c63->i_err_ma));
    pmdata.addUnits("I_ERR_MA","mA");

    pmdata.registerParameter("I_DIFF_MA",subUtilitiesSignedNtohs(c63->i_diff_ma));
    pmdata.addUnits("I_DIFF_MA","mA");

    pmdata.registerParameter("I_EARTH_CPCNT",(float)c63->i_earth_cpcnt);
    pmdata.addUnits("I_EARTH_CPCNT","%");

    pmdata.registerParameter("I_MEAS",subUtilitiesNtohf(c63->i_meas));
    pmdata.addUnits("I_MEAS","A");

    pmdata.registerParameter("I_REF",subUtilitiesNtohf(c63->i_ref));
    pmdata.addUnits("I_REF","A");

    pmdata.registerParameter("V_MEAS",subUtilitiesNtohf(c63->v_meas));
    pmdata.addUnits("V_MEAS","V");

    pmdata.registerParameter("V_REF",subUtilitiesNtohf(c63->v_ref));
    pmdata.addUnits("V_REF","V");

    // STATUS Signal

    memset(&pm_status_arrays,0,sizeof(pm_status_arrays));

    uint32_t &num_samples = pm_status_arrays.num_samples;

    for(uint32_t i = 0; i <  LEN; i++)
    {
        const PM_data *data = &status[i];
        c63 = &data->status.class_data.c63;

        // Ignore missing status data

        if(!data->time_sec) continue;

        // Fill arrays

        pm_status_arrays.timestamps[num_samples] = static_cast<int64_t>(ntohl(data->time_sec))*1000*1000*1000 + static_cast<int64_t>(ntohl(data->time_usec))*1000;

        subUtilitiesPmdBitmaskToText(data->status.data_status,sym_names_63_data_status,pm_status_arrays.data_status[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.data_status_ptr[num_samples] = pm_status_arrays.data_status[num_samples];

        pm_status_arrays.class_id[num_samples] = data->status.class_id;

        subUtilitiesPmdBitmaskToText(ntohs(c63->st_faults),sym_names_63_flt,pm_status_arrays.st_faults[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_faults_ptr[num_samples] = pm_status_arrays.st_faults[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c63->st_warnings),sym_names_63_wrn,pm_status_arrays.st_warnings[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_warnings_ptr[num_samples] = pm_status_arrays.st_warnings[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c63->st_latched),sym_names_63_lat,pm_status_arrays.st_latched[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_latched_ptr[num_samples] = pm_status_arrays.st_latched[num_samples];

        subUtilitiesPmdBitmaskToText(ntohs(c63->st_unlatched),sym_names_63_unl,pm_status_arrays.st_unlatched[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_unlatched_ptr[num_samples] = pm_status_arrays.st_unlatched[num_samples];

        subUtilitiesPmdEnumToText(c63->state_op,sym_names_63_op,pm_status_arrays.state_op[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_op_ptr[num_samples] = pm_status_arrays.state_op[num_samples];

        subUtilitiesPmdEnumToText(c63->state_vs,sym_names_63_vs,pm_status_arrays.state_vs[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_vs_ptr[num_samples] = pm_status_arrays.state_vs[num_samples];

        subUtilitiesPmdEnumToText(c63->state_pc,sym_names_63_pc,pm_status_arrays.state_pc[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.state_pc_ptr[num_samples] = pm_status_arrays.state_pc[num_samples];

        subUtilitiesPmdBitmaskToText(c63->st_adc_a,sym_names_63_adc_status,pm_status_arrays.st_meas_a[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_meas_a_ptr[num_samples] = pm_status_arrays.st_meas_a[num_samples];

        subUtilitiesPmdBitmaskToText(c63->st_adc_b,sym_names_63_adc_status,pm_status_arrays.st_meas_b[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_meas_b_ptr[num_samples] = pm_status_arrays.st_meas_b[num_samples];

        subUtilitiesPmdBitmaskToText(c63->st_dcct_a,sym_names_63_dcct,pm_status_arrays.st_dcct_a[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_dcct_a_ptr[num_samples] = pm_status_arrays.st_dcct_a[num_samples];

        subUtilitiesPmdBitmaskToText(c63->st_dcct_b,sym_names_63_dcct,pm_status_arrays.st_dcct_b[num_samples],MAX_SUB_STRING_LENGTH);
        pm_status_arrays.st_dcct_b_ptr[num_samples] = pm_status_arrays.st_dcct_b[num_samples];

        pm_status_arrays.i_err_ma[num_samples] = subUtilitiesSignedNtohs(c63->i_err_ma);
        pm_status_arrays.i_diff_ma[num_samples] = subUtilitiesSignedNtohs(c63->i_diff_ma);
        pm_status_arrays.i_earth_cpcnt[num_samples] = (float)c63->i_earth_cpcnt;
        pm_status_arrays.i_meas[num_samples] = subUtilitiesNtohf(c63->i_meas);
        pm_status_arrays.i_ref[num_samples] = subUtilitiesNtohf(c63->i_ref);
        pm_status_arrays.v_meas[num_samples] = subUtilitiesNtohf(c63->v_meas);
        pm_status_arrays.v_ref[num_samples] = subUtilitiesNtohf(c63->v_ref);

        // Increase number of samples counter counter

        num_samples++;
    }

    // Register timestamp

    pmdata.registerArray("STATUS.TIMESTAMP",pm_status_arrays.timestamps,num_samples);

    // Register arrays and associate them to the STATUS signal and the STATUS.TIMESTAMP

    pmdata.registerArray("STATUS.CLASS_ID",pm_status_arrays.class_id,num_samples);
    pmdata.addAttribute("STATUS.CLASS_ID","timescale","STATUS");
    pmdata.addAttribute("STATUS.CLASS_ID","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.DATA_STATUS",const_cast<const char **>(pm_status_arrays.data_status_ptr),num_samples);
    pmdata.addAttribute("STATUS.DATA_STATUS","timescale","STATUS");
    pmdata.addAttribute("STATUS.DATA_STATUS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_FAULTS",const_cast<const char **>(pm_status_arrays.st_faults_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_FAULTS","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_FAULTS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_WARNINGS",const_cast<const char **>(pm_status_arrays.st_warnings_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_WARNINGS","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_WARNINGS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_LATCHED",const_cast<const char **>(pm_status_arrays.st_latched_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_LATCHED","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_LATCHED","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_UNLATCHED",const_cast<const char **>(pm_status_arrays.st_unlatched_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_UNLATCHED","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_UNLATCHED","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_OP",const_cast<const char **>(pm_status_arrays.state_op_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_OP","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_OP","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_VS",const_cast<const char **>(pm_status_arrays.state_vs_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_VS","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_VS","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.STATE_PC",const_cast<const char **>(pm_status_arrays.state_pc_ptr),num_samples);
    pmdata.addAttribute("STATUS.STATE_PC","timescale","STATUS");
    pmdata.addAttribute("STATUS.STATE_PC","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_MEAS_A",const_cast<const char **>(pm_status_arrays.st_meas_a_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_MEAS_A","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_MEAS_A","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_MEAS_B",const_cast<const char **>(pm_status_arrays.st_meas_b_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_MEAS_B","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_MEAS_B","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_DCCT_A",const_cast<const char **>(pm_status_arrays.st_dcct_a_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_DCCT_A","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_DCCT_A","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.ST_DCCT_B",const_cast<const char **>(pm_status_arrays.st_dcct_b_ptr),num_samples);
    pmdata.addAttribute("STATUS.ST_DCCT_B","timescale","STATUS");
    pmdata.addAttribute("STATUS.ST_DCCT_B","timestamps","STATUS.TIMESTAMP",true);

    pmdata.registerArray("STATUS.I_ERR_MA",pm_status_arrays.i_err_ma,num_samples);
    pmdata.addAttribute("STATUS.I_ERR_MA","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_ERR_MA","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_ERR_MA","mA");

    pmdata.registerArray("STATUS.I_DIFF_MA",pm_status_arrays.i_diff_ma,num_samples);
    pmdata.addAttribute("STATUS.I_DIFF_MA","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_DIFF_MA","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_DIFF_MA","mA");

    pmdata.registerArray("STATUS.I_EARTH_CPCNT",pm_status_arrays.i_earth_cpcnt,num_samples);
    pmdata.addAttribute("STATUS.I_EARTH_CPCNT","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_EARTH_CPCNT","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_EARTH_CPCNT","%");

    pmdata.registerArray("STATUS.I_MEAS",pm_status_arrays.i_meas,num_samples);
    pmdata.addAttribute("STATUS.I_MEAS","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_MEAS","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_MEAS","A");

    pmdata.registerArray("STATUS.I_REF",pm_status_arrays.i_ref,num_samples);
    pmdata.addAttribute("STATUS.I_REF","timescale","STATUS");
    pmdata.addAttribute("STATUS.I_REF","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.I_REF","A");

    pmdata.registerArray("STATUS.V_MEAS",pm_status_arrays.v_meas,num_samples);
    pmdata.addAttribute("STATUS.V_MEAS","timescale","STATUS");
    pmdata.addAttribute("STATUS.V_MEAS","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.V_MEAS","V");

    pmdata.registerArray("STATUS.V_REF",pm_status_arrays.v_ref,num_samples);
    pmdata.addAttribute("STATUS.V_REF","timescale","STATUS");
    pmdata.addAttribute("STATUS.V_REF","timestamps","STATUS.TIMESTAMP",true);
    pmdata.addUnits("STATUS.V_REF","V");
}



static void pmd_63AddIleads(PMData &pmdata)
{
    // Use ILOOP log time stamps for the ILEADS log.

    pmdata.registerArray("ILEADS.TIMESTAMP",pm_buf_63_converted.iloop_timestamps_ns,PM_BUF_63_ILOOP_LEN);

    // I_MEAS and V_MEAS are also taken from the ILOOP log because the post mortem browser
    // will merge I_MEAS from the ILEADS and ILOOP logs.

    // Use a union to circumvent strict-aliasing warning to cast to the PM_buf_63 structure pointer

    union  PM_buf_63_union * const self_union = (union PM_buf_63_union *)pm_globals.buffer.self.std.fgc_buffers;
    struct PM_buf_63       * const self       = &self_union->self;

    pmdata.registerArray("ILEADS.I_MEAS",self->iloop.signals.i_meas,PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute("ILEADS.I_MEAS","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.I_MEAS","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.I_MEAS","A");

    pmdata.registerArray("ILEADS.V_MEAS",self->iloop.signals.v_meas,PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute("ILEADS.V_MEAS","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.V_MEAS","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.V_MEAS","V");

    // U_LEAD_POS and U_LEAD_NEG signals will contain zeros.

    static float ilead_dummy_data[PM_BUF_63_ILOOP_LEN];

    pmdata.registerArray("ILEADS.U_LEAD_POS",ilead_dummy_data,PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute("ILEADS.U_LEAD_POS","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.U_LEAD_POS","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.U_LEAD_POS","V");

    pmdata.registerArray("ILEADS.U_LEAD_NEG",ilead_dummy_data,PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute("ILEADS.U_LEAD_NEG","timescale","ILEADS");
    pmdata.addAttribute("ILEADS.U_LEAD_NEG","timestamps","ILEADS.TIMESTAMP",true);
    pmdata.addUnits("ILEADS.U_LEAD_NEG","V");
}



void pmd_63GenerateAndSendExt(pm::Client &pmclient, struct FGCD_device *device, const struct PM_ext_buffer &buffer, int64_t timestamp)
{
    try
    {
        // Create PMData object

        PMData pmdata(PM_SYSTEM_NAME, "lhc_ext_pmd", device->name);

        // Add trigger and status data

        pmd_63AddStatus(pmdata, timestamp, &buffer.trigger, buffer.status, PM_EXT_BUF_LEN);

        // Send PMD Data

        logPrintf(device->id,
                  "PM Sending externally-triggered PM (system=%s, class=%s, source=%s, timestamp=%lld)...\n",
                  PM_SYSTEM_NAME, "lhc_self_pmd", device->name, timestamp);

        pmclient.send(pmdata, timestamp);
    }
    catch(PMError& e)
    {
       logPrintf(device->id, "PM PMD Generation and sending threw PMError: %s\n", e.what());
       fgcddev.status.st_faults |= FGC_FLT_PM;
    }
    catch(std::exception& e)
    {
        logPrintf(device->id, "PM PMD Generation and sending threw std::exception: %s\n", e.what());
        fgcddev.status.st_faults |= FGC_FLT_PM;
    }
}



void pmd_63GenerateAndSendSelf(pm::Client &pmclient, struct FGCD_device *device, const struct PM_self_buffer_std &buffer, int64_t timestamp)
{
    try
    {
        // Create PMData object

        PMData pmdata(PM_SYSTEM_NAME, "lhc_self_pmd", device->name);

        // Add trigger and status data

        pmd_63AddStatus(pmdata, timestamp, &buffer.trigger, buffer.status, PM_SELF_BUF_LEN_STD);

        // Add FGC_63 buffers using auto-generated function from pm_buf_63.h

        pmd_63AddFgc(pmdata);

        // Add ILEADS buffer containing zeros for compatibility with Class 51

        pmd_63AddIleads(pmdata);

        // Send PMD Data

        logPrintf(device->id,
                  "PM Sending self-triggered PM (system=%s, class=%s, source=%s, timestamp=%lld)...\n",
                  PM_SYSTEM_NAME, "lhc_self_pmd", device->name, timestamp);

        pmclient.send(pmdata, timestamp);
    }
    catch(PMError& e)
    {
       logPrintf(device->id, "PM PMD Generation and sending threw PMError: %s\n", e.what());
       fgcddev.status.st_faults |= FGC_FLT_PM;
    }
    catch(std::exception& e)
    {
        logPrintf(device->id, "PM PMD Generation and sending threw std::exception: %s\n", e.what());
        fgcddev.status.st_faults |= FGC_FLT_PM;
    }
}

// EOF
