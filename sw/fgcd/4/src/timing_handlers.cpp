/*!
 * @file   timing_handlers.c
 * @author Stephen Page
 *
 * Functions for handling timing events
 */

#include <arpa/inet.h>
#include <exception>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <map>
#include <memory>

#include <accsoft-commons-matcher/Matcher.h>
#include <accsoft-commons-matcher-timdt/TimdtConditionVerifierWithTgm.h>
#include <accsoft-commons-matcher-timdt/TimdtMatcher.h>
#include <accsoft-commons-matcher-timdt/TimdtValuesMapWithTgm.h>

#include <consts.h>
#include <ether.h>
#include <fgc_event.h>
#include <fgcsm.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <fieldbus_class.h>
#include <fieldbus.h>
#include <logging.h>
#include <pub.h>
#include <iostream>
#include <thread>

#include <timing.h>
#include <timing_handlers.h>
#include <profilingTimer.h>


// Counter for diagnostics for APS-9428
uint32_t msHandlerNextMs       = 9999;      //! Next expected ms
auto     lastMsHandlerDuration = 0ns;       //! The time it took for the last ms handler to execute

std::unique_ptr<accsoft::TimdtMatcher> mppm_matcher;

void timingAddMultiPpmEvent(const char *mppm_condition)
{
    try
    {
        // Use the same TimdtConditionVerifierWithTgm for all devices

        accsoft::TimdtConditionVerifierWithTgm conditionVerifier;

        // Create a new Matcher if it has not been already initialized

        if(!mppm_matcher)
        {
            mppm_matcher = std::unique_ptr<accsoft::TimdtMatcher>(new accsoft::TimdtMatcher());
        }

        // Register the timing condition

        mppm_matcher->registerCondition(mppm_condition, conditionVerifier);
    }
    catch(std::exception& e)
    {
        fprintf(stderr, "WARNING: Failed to register multi-PPM condition %s: %s\n", mppm_condition, e.what());
    }
}


static void timingSetTelegramBits(const char* field_name,uint8_t bit_number, Timing::EventValue* evt_value, uint32_t *telegram_value, bool first_telegram)
{
    static Timing::Value value;

    try
    {
        evt_value->getFieldValue(field_name, &value);

        if(value.getAsBool())
        {
            *telegram_value |= (1 << bit_number);
        }
        else
        {
            *telegram_value &= ~(1 << bit_number);
        }
    }
    catch(std::exception& e)
    {
        if (first_telegram)
        {
            // Print to std::cerr because logger is not ready at this point (during timing initialization)
            std::cerr << "Failed to set telegram bit " << field_name << " : " << e.what() <<". Exiting" << std::endl;
            exit(1);
        }
    }
}

/*
 * Function to perform actions on telegram reception for cycling machines
 */

static void timingTelegramCyclingMachines(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    // USER (aka USER)

    try
    {
        evt_value->getFieldValue("USER", &value);

        uint32_t user = timingUserNumber(value.getAsString().c_str());
        if(!user) return;

        timing.field_value[TIMING_FIELD_USER] = user;

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field USER = %s (%u)\n",
                      evt_value->getName().c_str(),
                      value.getAsString().c_str(),
                      timing.field_value[TIMING_FIELD_USER]);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Set user

    timing.prev_user = timing.user;
    timing.user      = timing.field_value[TIMING_FIELD_USER] & 0x000000FF;

    // BP_DURATION_MS

    try
    {
        evt_value->getFieldValue("BP_DURATION_MS", &value);

        timing.field_value[TIMING_FIELD_BP_DURATION_MS] = static_cast<uint32_t>(value.getAsLong32());
     }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // CYCLE_DURATION_MS (aka CYCLELEN)

    try
    {
        evt_value->getFieldValue("CYCLE_DURATION_MS", &value);
        timing.field_value[TIMING_FIELD_CYCLE_LEN] = static_cast<uint32_t>(value.getAsLong32()/timing.field_value[TIMING_FIELD_BP_DURATION_MS]);


        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field CYCLE_DURATION_MS = %u ms, CYCLE_LEN = %u bps, BP_DURATION_MS = %u ms\n",
                      evt_value->getName().c_str(),
                      static_cast<uint32_t>(value.getAsLong32()),
                      timing.field_value[TIMING_FIELD_CYCLE_LEN],
                      timing.field_value[TIMING_FIELD_BP_DURATION_MS]);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // CYCLE_TAG (aka CYCLETAG)

    try
    {
        evt_value->getFieldValue("CYCLE_TAG", &value);
        timing.field_value[TIMING_FIELD_CYCLE_TAG] = static_cast<uint32_t>(value.getAsLong32());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field CYCLE_TAG = %u\n",
                      evt_value->getName().c_str(),
                      timing.field_value[TIMING_FIELD_CYCLE_TAG]);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // CYCLE_NB (aka CYCLENUM)

    try
    {
        evt_value->getFieldValue("CYCLE_NB", &value);
        timing.field_value[TIMING_FIELD_CYCLE_NUM] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field CYCLE_NB = %u\n",
                      evt_value->getName().c_str(),
                      timing.field_value[TIMING_FIELD_CYCLE_NUM]);
        }

        // Set cycle number

        timing.cycle_num = timing.field_value[TIMING_FIELD_CYCLE_NUM] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // FGC EVENTS

    uint32_t             basic_period = ((evt_value->getCycleTime() / timing.field_value[TIMING_FIELD_BP_DURATION_MS]) %
                                        timing.field_value[TIMING_FIELD_CYCLE_LEN]) + 1;
    struct fgc_event     fgc_event;

    // Take actions in the first basic period of the cycle

    if(basic_period == 1)
    {
        // Send the cycle tag fieldbus event

        fgc_event.type          = FGC_EVT_CYCLE_TAG;
        fgc_event.trigger_type  = 0;
        fgc_event.payload       = timing.field_value[TIMING_FIELD_CYCLE_TAG];
        fgc_event.delay_us      = FGC_EVT_MIN_CYCLES * FGCD_CYCLE_PERIOD_MS * 1000;
        fieldbusSendEvent(&fgc_event);
    }

    // SPS MACHINE_MODE

    if(fgcd.machine == MACHINE_SPS)
    {
        try
        {
            evt_value->getFieldValue("MACHINE_MODE", &value);
            timing.field_value[TIMING_FIELD_MACHINE_MODE] = static_cast<uint32_t>(value.getAsUshort());

            if(timing.log_events_flag)
            {
                logPrintf(0,"TIMING Event %s Field MACHINE_MODE = %s (%u)\n",
                    evt_value->getName().c_str(),
                    value.getAsString().c_str(),
                    timing.field_value[TIMING_FIELD_MACHINE_MODE]);
            }

        }
        catch(std::exception& e)
        {
            logPrintf(0, "TIMING Failed to extract MACHINE_MODE field\n");
        }
    }
}

static void timingTelegramLHC(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static bool first_telegram = true;
    uint16_t sector_mask = fgcd.device[0].omode_mask & 0x00FF;
    static Timing::Value value;

    // PP60A (aka PC_PERMIT)

    timingSetTelegramBits("PP60A_ARC12",0, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC23",1, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC34",2, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC45",3, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC56",4, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC67",5, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC78",6, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);
    timingSetTelegramBits("PP60A_ARC81",7, evt_value, &timing.field_value[TIMING_FIELD_PC_PERMIT], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field PP60A = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_PC_PERMIT]);
    }

    // Set PC_PERMIT

    if((timing.field_value[TIMING_FIELD_PC_PERMIT] & sector_mask) == sector_mask)
    {
      if(!fieldbus.pc_permit)
      {
           logPrintf(0, "TIMING PP60A (PC_PERMIT) set\n");
           fieldbus.pc_permit = 1;
      }
    }
    else if(fieldbus.pc_permit)
    {
      logPrintf(0, "TIMING PP60A (PC_PERMIT) cleared\n");
      fieldbus.pc_permit = 0;
    }

    // OMODE (aka OPMODE)

    timingSetTelegramBits("OMODE_BEAMOP", 8, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);
    timingSetTelegramBits("OMODE_CMS",    9, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);
    timingSetTelegramBits("OMODE_ATLAS",  10, evt_value, &timing.field_value[TIMING_FIELD_OP_MODE], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field OMODE = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_OP_MODE]);
    }

    // Set operational mode only if succeeded

    uint16_t op_mode = static_cast<uint16_t>(timing.field_value[TIMING_FIELD_OP_MODE] & 0x0000FFFF);

    if(fgcd.op_mode != op_mode)
    {
        if (first_telegram)
        {
            std::cout << "TIMING Operational mode mask changed from " << fgcd.op_mode << " to " << op_mode << std::endl;
        }

        logPrintf(0, "TIMING Operational mode mask changed from 0x%04hX to 0x%04hX\n", fgcd.op_mode, op_mode);
        fgcd.op_mode = op_mode;
    }

    // SECTACC (aka SECTORACCESS)

    timingSetTelegramBits("SECTACC_SEC12", 0, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC23", 1, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC34", 2, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC45", 3, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC56", 4, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC67", 5, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC78", 6, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);
    timingSetTelegramBits("SECTACC_SEC81", 7, evt_value, &timing.field_value[TIMING_FIELD_SECTOR_ACCESS], first_telegram);

    if(timing.log_events_flag)
    {
        logPrintf(0, "Event %s Field SECTACC = 0x%08X\n",
                  evt_value->getName().c_str(), timing.field_value[TIMING_FIELD_SECTOR_ACCESS]);
    }

    // Set fieldbus SECTOR_ACCESS bit

    if(timing.field_value[TIMING_FIELD_SECTOR_ACCESS] & sector_mask)
    {
        if(!fieldbus.sector_access)
        {
           logPrintf(0, "TIMING SECTACC (SECTOR_ACCESS) set\n");
           fieldbus.sector_access = 1;
        }
    }
    else if(fieldbus.sector_access)
    {
      logPrintf(0, "TIMING SECTACC (SECTOR_ACCESS) cleared\n");
      fieldbus.sector_access = 0;
    }

    first_telegram = false;
}

void timingTelegramReady(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Machine-specific handling

    switch(fgcd.machine)
    {
        case MACHINE_ADE:
        case MACHINE_CPS:
        case MACHINE_LEI:
        case MACHINE_LN4:
        case MACHINE_LNA:
        case MACHINE_PSB:
        case MACHINE_SPS:
            timingTelegramCyclingMachines(handler, info, evt_time, evt_value);
            break;

        case MACHINE_LHC:
            timingTelegramLHC(handler, info, evt_time, evt_value);
            break;

        default:
            break;
    }
}

static void timingGenericWarningWithUser(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value, uint8_t warning_event_type)
{
    uint32_t             delay_us;
    struct fgc_event     fgc_event;
    struct timeval       relative_time;
    static Timing::Value value;

    // Retrieve next USER

    try
    {
        evt_value->getFieldValue("USER", &value);

        uint32_t next_user = timingUserNumber(value.getAsString().c_str());
        if(!next_user) return;

        timing.field_value[TIMING_FIELD_NEXT_USER] = next_user;

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field USER = %s (%u)\n",

            evt_value->getName().c_str(),
            value.getAsString().c_str(),
            timing.field_value[TIMING_FIELD_NEXT_USER]);
        }

        timing.next_user = timing.field_value[TIMING_FIELD_NEXT_USER] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Get the time relative to the start of the next fieldbus cycle

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    delay_us = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);

    // Send multi-ppm condition event if a condition exists

    if(mppm_matcher)
    {
        try
        {
            accsoft::Result match_result = mppm_matcher->matchFirst(accsoft::TimdtValuesMapWithTgm(*evt_value));

            switch(warning_event_type)
            {
                case FGC_EVT_CYCLE_START: fgc_event.type = FGC_EVT_SUB_DEVICE_CYCLE_START; break;
                case FGC_EVT_START_REF_1: fgc_event.type = FGC_EVT_SUB_DEVICE_START_REF_1; break;
                case FGC_EVT_START_REF_2: fgc_event.type = FGC_EVT_SUB_DEVICE_START_REF_2; break;
                case FGC_EVT_START_REF_3: fgc_event.type = FGC_EVT_SUB_DEVICE_START_REF_3; break;
                case FGC_EVT_START_REF_4: fgc_event.type = FGC_EVT_SUB_DEVICE_START_REF_4; break;
                case FGC_EVT_START_REF_5: fgc_event.type = FGC_EVT_SUB_DEVICE_START_REF_5; break;
                default:                  fgc_event.type = FGC_EVT_NONE;                   break;
            }

            if(match_result.idx >= 0) // Multi-PPM condition was matched
            {
                // Set the payload to a bitwise or of the sub-device index and the multi-PPM user

                fgc_event.payload  = ((match_result.idx + 1) << FGC_MULTI_PPM_SUB_SEL_SHIFT) |
                                     (match_result.selectorIndex & FGC_MULTI_PPM_USER_MASK);
            }
            else // Multi-PPM condition was not matched
            {
                fgc_event.payload  = (timing.next_user & FGC_MULTI_PPM_USER_MASK);
            }

            fgc_event.delay_us = delay_us;
            fgc_event.trigger_type = 0;

            // Send the event

            fieldbusSendEvent(&fgc_event);
        }
        catch(std::exception& e)
        {
            logPrintf(0, "Multi-PPM matcher failed for event %s\n", evt_value->getName().c_str());
        }
    }

    // Send fieldbus events

    // Next user

    fgc_event.delay_us      = delay_us;
    fgc_event.type          = warning_event_type;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = timing.field_value[TIMING_FIELD_NEXT_USER];

    fieldbusSendEvent(&fgc_event);

    // Next destination

    fgc_event.type          = FGC_EVT_NEXT_DEST;
    fgc_event.trigger_type  = warning_event_type;
    fgc_event.payload       = 0;

    // Attempt to get the destination from the timing event, if available.
    // Note that a payload of 0 is deliberately sent, if not.

    try
    {
        // Machine-specific handling

        uint16_t field_value;
        switch(fgcd.machine)
        {
            // The following machines use a mask in a field called DYN_DEST,
            // which can be sent directly as the payload of the NEXT_DEST event

            case MACHINE_CPS:
                evt_value->getFieldValue("DYN_DEST", &value);
                fgc_event.payload = value.getAsUshort();
                break;

            // LEIR has no dynamic destination, so the programmed destination enumeration is used instead.
            // It is converted into a mask with the corresponding bit set.
            // Note that the enumeration starts at 1, so the shift is the field value - 1.

            case MACHINE_LEI:
                evt_value->getFieldValue("PROG_DEST", &value);
                field_value = value.getAsUshort();
                fgc_event.payload = 0x1 << (field_value - 1);
                break;

            // The following machines use an enumeration in a field called DYN_DEST,
            // which should be converted into a mask with the corresponding bit set.
            // Note that the enumeration starts at 1, so the shift is the field value - 1.

            case MACHINE_PSB:
            case MACHINE_SPS:
                evt_value->getFieldValue("DYN_DEST", &value);
                field_value = value.getAsUshort();
                fgc_event.payload = 0x1 << (field_value - 1);
                break;

            default:
                break;
        }
    }
    catch(std::exception& e)
    {
        logPrintf(0, "TIMING Failed to get next destination for event %s\n", evt_value->getName().c_str());
    }

    fieldbusSendEvent(&fgc_event);
}

static void timingGenericWarningWithoutUser(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value, uint8_t warning_event_type)
{
    struct fgc_event     fgc_event;
    struct timeval       relative_time;

    // Prepare FGC fieldbus event

    fgc_event.type          = warning_event_type;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = 0;

    // Get the time relative to the start of the next fieldbus cycle

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    fgc_event.delay_us = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);

    // Send fieldbus event

    fieldbusSendEvent(&fgc_event);
}

// Timing handler functions

void timingMillisecond(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    ctr_ccv_s               ccv;
    uint32_t                ms = evt_time.time.tv_nsec / 1000000;   // Millisecond within UTC second
    static uint32_t         prev_power_sig_count = 0;
    struct timeval          time;
    Timer<>                 profiling_timer;

    profiling_timer.start();

    // Set time
    time.tv_sec  = evt_time.time.tv_sec;
    time.tv_usec = ms * 1000;
    timingSetUserTime(0, &time);

    // Issue a warning if time.tv_sec less than 2022.01.01 (this should not happen normally)
    if (time.tv_sec < 1640995200)
    {
        logPrintf(0, "TIMING: UTC seconds (time.tv_sec == %lld) is less than 2022.01.01.\n", (long long) time.tv_sec);
    }

    // If msHandlerNextMs was set and it's different from ms
    if (msHandlerNextMs != 9999 && msHandlerNextMs != ms)
    {
        logPrintf(0, "TIMING: Millisecond should be %d but it is %d (%lu). Previous ms handler took %lu ns. \n",
                  msHandlerNextMs, ms, evt_time.time.tv_nsec, lastMsHandlerDuration.count());
    }

    // Update the expected ms
    msHandlerNextMs = (ms == 999 ? 0 : ms + 1);

    // Perform actions on specific milliseconds within FGCD cycle

    switch(ms % FGCD_CYCLE_PERIOD_MS)
    {
        case 0:

            // Copy fieldbus events to time frame

            pthread_mutex_lock(&fieldbus.event_mutex);

            // Fill the events slots with events from the queue

            fieldbusFillSlotsFromQueue();

            // Copy the events into the Ethernet frame

            memcpy(ether.frames.time.payload.time.event, fieldbus.event, sizeof(ether.frames.time.payload.time.event));

            // Update fieldbus event slots

            fieldbusUpdateEvents();

            pthread_mutex_unlock(&fieldbus.event_mutex);

            break;

        case FGC_ETHER_STATUS_WIN_END_MS + 1:   // Millisecond after status reception closed

            // Process status of all FGCs

            fgcsmCheckStatus();

            // Timestamp published data

            timingGetUserTime(0, &time);
            pub.published_data.time_sec     = htonl(time.tv_sec);
            pub.published_data.time_usec    = htonl(time.tv_usec - ((ms % FGCD_CYCLE_PERIOD_MS) * 1000));

            // Publish FGCD device data

            fgcddevPublish();

            // Trigger sending of published data

            pubTrigger();

            break;

        case FGC_ETHER_TIME_1_MS - 1:

            // Prepare time frame

            fieldbusPrepareTime(&ether.frames.time.payload.time);

            // Prepare FGC code

            fieldbusPrepareCode(&ether.frames.time.payload.time,
                                ether.frames.time.payload.code,
                                FGC_FIELDBUS_CODE_BLOCKS_PER_MSG);

            // Prepare real-time references

            etherPrepareRT();

            break;

        case FGC_ETHER_TIME_1_MS:

            // Send time frame

            etherSendTime();
            break;

        case FGC_ETHER_TIME_2_MS:

            // Send time frame

            etherSendTime();

            // Check whether to send long pulses for FGC power-cycle

            if(timing.source == TIMING_TIMLIB)
            {
                // Check whether the FGC power signal is requested

                if(fieldbus.fgc_power_sig_count)
                {
                    // Check whether the signal needs to be enabled

                    if(!prev_power_sig_count) // The signal is not being sent
                    {
                        // Enable the output in the last ms of the second as the start is triggered by the PPS

                        if(ms == 999) // This is the last ms of the second
                        {
                            if (ctr_get_remote(timing.ctr_handle, static_cast<CtrDrvrCounter>(FIELDBUS_TIMING_COUNTER_LONG), &ccv) > 0)
                            {

                                // Enable the long pulse counter

                                ccv.enable |= CtrDrvrCounterOnZeroOUT;

                                if (ctr_set_remote(timing.ctr_handle, 1, static_cast<CtrDrvrCounter>(FIELDBUS_TIMING_COUNTER_LONG), CtrDrvrRemoteLOAD, &ccv, CTR_CCV_ENABLE) < 0)
                                {
                                    logPrintf(0, "TIMING Failed to enable long pulse: ctr_set_remote() < 0\n");
                                }
                            }
                            else
                            {
                                fprintf(stderr, "WARNING: Failed to get CTR counter configuration: ctr_get_remote() < 0\n");
                            }

                            prev_power_sig_count = fieldbus.fgc_power_sig_count;
                        }
                    }
                    else // The power signal is being sent
                    {
                        prev_power_sig_count = fieldbus.fgc_power_sig_count;

                        fieldbus.fgc_power_sig_count--;
                        logPrintf(0, "TIMING Long pulse, %d remaining\n", fieldbus.fgc_power_sig_count);
                    }
                }
                else if(prev_power_sig_count) // The last power signal was sent in the previous cycle
                {
                    prev_power_sig_count = 0;

                    if (ctr_get_remote(timing.ctr_handle, static_cast<CtrDrvrCounter>(FIELDBUS_TIMING_COUNTER_LONG), &ccv) > 0)
                    {

                        // Enable the long pulse counter

                        ccv.enable &= ~CtrDrvrCounterOnZeroOUT;

                        if (ctr_set_remote(timing.ctr_handle, 1, static_cast<CtrDrvrCounter>(FIELDBUS_TIMING_COUNTER_LONG), CtrDrvrRemoteLOAD, &ccv, CTR_CCV_ENABLE) < 0)
                        {
                            logPrintf(0, "TIMING Failed to disable long pulse: ctr_set_remote() < 0\n");
                        }
                    }
                    else
                    {
                        fprintf(stderr, "WARNING: Failed to get CTR counter configuration: ctr_get_remote() < 0\n");
                    }
                }
            }

            break;
    }

    profiling_timer.stop();
    lastMsHandlerDuration = profiling_timer.get();

    if (lastMsHandlerDuration > 1ms)
    {
        logPrintf(0, "TIMING: The ms (%d) handler took longer than 1ms (%lu ns)\n", ms, lastMsHandlerDuration.count());
    }
}

void timingPostMortem(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event fgc_event;

    fieldbus.global_pm_time_sec     = evt_time.time.tv_sec;
    fieldbus.global_pm_time_nsec    = evt_time.time.tv_nsec;

    fgc_event.type          = FGC_EVT_PM;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = 0;
    fgc_event.delay_us      = FGC_EVT_MIN_CYCLES * FGCD_CYCLE_PERIOD_MS * 1000;
    fieldbusSendEvent(&fgc_event);
}

void timingAbortRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event    fgc_event;
    struct timeval      relative_time;
    static Timing::Value value;

    fgc_event.payload = 0;
    if(fgcd.machine == MACHINE_LHC || fgcd.machine == MACHINE_SPS)
    {
        // EVENT_GROUP

        try
        {
            evt_value->getFieldValue("EVENT_GROUP",&value);
            fgc_event.payload = static_cast<uint16_t>(value.getAsUshort());

            if(timing.log_events_flag)
            {
                logPrintf(0, "Event %s Field EVENT_GROUP = %u\n",
                          evt_value->getName().c_str(),
                          fgc_event.payload);
            }
        }
        catch(std::exception& e)
        {
            logPrintf(0, "TIMING Failed to extract EVENT_GROUP field for AbortRef\n");
            return;
        }
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    fgc_event.type          = FGC_EVT_ABORT;
    fgc_event.trigger_type  = 0;
    fgc_event.delay_us      = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);
    fieldbusSendEvent(&fgc_event);
}

void timingStartRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event     fgc_event;
    struct timeval       relative_time;
    static Timing::Value value;

    fgc_event.payload = 0;
    if(fgcd.machine == MACHINE_LHC || fgcd.machine == MACHINE_SPS)
    {
        // EVENT_GROUP

        try
        {
            evt_value->getFieldValue("EVENT_GROUP",&value);
            fgc_event.payload = static_cast<uint16_t>(value.getAsUshort());

            if(timing.log_events_flag)
            {
                logPrintf(0, "Event %s Field EVENT_GROUP = %u\n",
                          evt_value->getEventDesc()->getName().c_str(),
                          fgc_event.payload);
            }
        }
        catch(std::exception& e)
        {
            logPrintf(0, "TIMING Failed to extract EVENT_GROUP field for StartRef\n");
            return;
        }
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    fgc_event.type          = FGC_EVT_START;
    fgc_event.trigger_type  = 0;
    fgc_event.delay_us      = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);
    fieldbusSendEvent(&fgc_event);
}

void timingSSC(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event fgc_event;

    fgc_event.type          = FGC_EVT_SSC;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = 0;
    fgc_event.delay_us      = FGC_EVT_MIN_CYCLES * FGCD_CYCLE_PERIOD_MS * 1000;
    fieldbusSendEvent(&fgc_event);
}

void timingCycleWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithUser(handler, info, evt_time, evt_value, FGC_EVT_CYCLE_START);
}

void timingPause(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_PAUSE);
}

void timingResume(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_RESUME);
}

void timingStartRef1Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithUser(handler, info, evt_time, evt_value, FGC_EVT_START_REF_1);
}

void timingStartRef2Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithUser(handler, info, evt_time, evt_value, FGC_EVT_START_REF_2);
}

void timingStartRef3Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithUser(handler, info, evt_time, evt_value, FGC_EVT_START_REF_3);
}

void timingStartRef4Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithUser(handler, info, evt_time, evt_value, FGC_EVT_START_REF_4);
}

void timingStartRef5Warning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithUser(handler, info, evt_time, evt_value, FGC_EVT_START_REF_5);
}

void timingCoast(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_COAST);
}

void timingRecover(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_RECOVER);
}

void timingStartHarmonics(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_START_HARMONICS);
}

void timingStopHarmonics(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_STOP_HARMONICS);
}

void timingEconomy(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    timingGenericWarningWithoutUser(handler, info, evt_time, evt_value, FGC_EVT_ECONOMY_DYNAMIC);
}

void timingAcquisition(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event     fgc_event;
    struct timeval       relative_time;
    static Timing::Value value;
    uint32_t             next_user = 0;

    // Retrieve next USER

    try
    {
        evt_value->getFieldValue("USER", &value);

        next_user = timingUserNumber(value.getAsString().c_str());
        if(!next_user) return;

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field USER = %s (%u)\n",
            evt_value->getName().c_str(),
            value.getAsString().c_str(),
            next_user);
        }
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    // Send fieldbus event

    fgc_event.type          = FGC_EVT_ACQUISITION;
    fgc_event.trigger_type  = 0;
    fgc_event.payload       = next_user;
    fgc_event.delay_us      = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);

    fieldbusSendEvent(&fgc_event);
}

void timingTransactionCommit(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event        fgc_event;
    struct timeval          relative_time;
    uint16_t                transaction_id;
    static Timing::Value    value;

    // Extract transaction ID timing event payload

    try
    {
        evt_value->getFieldValue("PAYLOAD", &value);
        transaction_id = static_cast<uint16_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field PAYLOAD = %u\n",
                      evt_value->getName().c_str(), transaction_id);
        }
    }
    catch(std::exception& e)
    {
        logPrintf(0, "Event %s failed to extract transaction_id payload\n", evt_value->getName().c_str());
        return;
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    // Send fieldbus event

    fgc_event.type          = FGC_EVT_TRANSACTION_COMMIT;
    fgc_event.trigger_type  = 0;
    fgc_event.delay_us      = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);
    fgc_event.payload       = transaction_id;
    fieldbusSendEvent(&fgc_event);
}

void timingTransactionRollback(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue* evt_value)
{
    struct fgc_event        fgc_event;
    struct timeval          relative_time;
    uint16_t                transaction_id;
    static Timing::Value    value;

    // Extract transaction ID timing event payload

    try
    {
        evt_value->getFieldValue("PAYLOAD", &value);
        transaction_id = static_cast<uint16_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0, "Event %s Field PAYLOAD = %u\n",
                      evt_value->getName().c_str(), transaction_id);
        }
    }
    catch(std::exception& e)
    {
        logPrintf(0, "Event %s failed to extract transaction_id payload\n", evt_value->getName().c_str());
        return;
    }

    fieldbusEventTimeRelativeToNextCycle(evt_time, &relative_time);

    // Send fieldbus event

    fgc_event.type          = FGC_EVT_TRANSACTION_ROLLBACK;
    fgc_event.trigger_type  = 0;
    fgc_event.delay_us      = (1000 * info.delay_ms) - ((relative_time.tv_sec * 1000000) + relative_time.tv_usec);
    fgc_event.payload       = transaction_id;
    fieldbusSendEvent(&fgc_event);
}

struct Timing_event_handler timing_event_handlers[]
= {
    { timing_handler_names[TIMING_HANDLER_ABORTREF],                &timingAbortRef             },
    { timing_handler_names[TIMING_HANDLER_ACQUISITION],             &timingAcquisition          },
    { timing_handler_names[TIMING_HANDLER_CYCLEWARNING],            &timingCycleWarning         },
    { timing_handler_names[TIMING_HANDLER_COAST],                   &timingCoast                },
    { timing_handler_names[TIMING_HANDLER_RECOVER],                 &timingRecover              },
    { timing_handler_names[TIMING_HANDLER_STARTHARMONICS],          &timingStartHarmonics       },
    { timing_handler_names[TIMING_HANDLER_STOPHARMONICS],           &timingStopHarmonics        },
    { timing_handler_names[TIMING_HANDLER_ECONOMY],                 &timingEconomy              },
    { timing_handler_names[TIMING_HANDLER_MS],                      &timingMillisecond          },
    { timing_handler_names[TIMING_HANDLER_PAUSE],                   &timingPause                },
    { timing_handler_names[TIMING_HANDLER_POSTMORTEM],              &timingPostMortem           },
    { timing_handler_names[TIMING_HANDLER_RESUME],                  &timingResume               },
    { timing_handler_names[TIMING_HANDLER_SSC],                     &timingSSC                  },
    { timing_handler_names[TIMING_HANDLER_STARTREF1WARNING],        &timingStartRef1Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF2WARNING],        &timingStartRef2Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF3WARNING],        &timingStartRef3Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF4WARNING],        &timingStartRef4Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF5WARNING],        &timingStartRef5Warning     },
    { timing_handler_names[TIMING_HANDLER_STARTREF],                &timingStartRef             },
    { timing_handler_names[TIMING_HANDLER_TELEGRAM],                &timingTelegramReady        },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONCOMMIT],       &timingTransactionCommit    },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONROLLBACK],     &timingTransactionRollback  },
    { "",                                                           NULL                        }
};

// EOF
