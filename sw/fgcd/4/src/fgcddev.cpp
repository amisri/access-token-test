/*!
 * @file   fgcddev.c
 * @author Stephen Page
 *
 * Functions for the FGCD device
 *
 * Notes: Name code support added for class 1
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <crc.h>
#include <definfo.h>
#include <defprops.h>
#include <devname.h>
#include <fgc_code.h>
#include <fgc_codes_gen.h>
#include <fgc_errs.h>
#include <fgc_stat.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <fieldbus.h>
#include <hash.h>
#include <logging.h>
#include <nonvol.h>
#include <parser.h>
#include <pub.h>
#include <rt.h>
#include <tcp.h>
#include <timing.h>
#include <version.h>



// Global variables

struct FGCDdev fgcddev;



// Static functions

static void fgcddevGenerateNameCode(void);



int32_t fgcddevStart(void)
{
    uint32_t                    i;
    struct Parser_class_data    parser_data;

    // Set device

    fgcddev.device = &fgcd.device[0];

    // Set class

    fgcddev.fgc_class_name = FGC_CLASS_NAME;

    // Set platform

    fgcddev.fgc_platform        = FGC_PLATFORM_ID;
    fgcddev.fgc_platform_name   = FGC_PLATFORM_NAME;

    // Set published compile time

    fgcddev.status.compile_time = version.time.tv_sec;

    // Set class in published data

    pub.published_data.status[0].class_id = FGC_CLASS_ID;

    // Initialise the property hash

    if(!(fgcddev.prop_hash = hashInit(PROP_HASH_SIZE)))
    {
        fprintf(stderr, "ERROR: Unable to initialise property hash\n");
        return 1;
    }

    // Populate the property hash

    for(i = 0 ; i < N_PROP_SYMBOLS ; i++)
    {
        hashInsert(fgcddev.prop_hash, &SYM_TAB_PROP[i]);
    }

    // Initialise the constant hash

    if((fgcddev.const_hash = hashInit(CONST_HASH_SIZE)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise constant hash\n");

        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Populate the const hash

    for(i = 0 ; i < N_CONST_SYMBOLS ; i++)
    {
        hashInsert(fgcddev.const_hash, &SYM_TAB_CONST[i]);
    }

    // Give values to test properties

    fgcddev.test_string[0] = "FIRST";
    fgcddev.test_string[1] = "SECOND";

    // Set code name pointers to allow names to be read through string property

    for(i = 0 ; i < FGC_CODE_NUM_SLOTS ; i++)
    {
        fgcddev.fgc_code_name_property[i] = fgcddev.fgc_code_name[i];
    }

    // Read code files

    if(fgcddevReadCodes())
    {
        fprintf(stderr, "ERROR: failed to read FGC code files\n");

        hashFree(fgcddev.prop_hash);
        hashFree(fgcddev.const_hash);

        return 1;
    }

    // Set parser data for class

    parser_data.class_id        = FGC_CLASS_ID;
    parser_data.sym_tab_const   = SYM_TAB_CONST;
    parser_data.const_hash      = fgcddev.const_hash;
    parser_data.sym_tab_prop    = SYM_TAB_PROP;
    parser_data.sym_tab_prop_len = sizeof(SYM_TAB_PROP)/sizeof(struct hash_entry);
    parser_data.prop_hash       = fgcddev.prop_hash;
    parser_data.get_func        = GET_FUNC;
    parser_data.pars_func       = PARS_FUNC;
    parser_data.num_pars_func   = NUM_PARS_FUNC_4;
    parser_data.set_func        = SET_FUNC;
    parser_data.setif_func      = SETIF_FUNC;
    parser_data.evtlog_func     = NULL;

    if(parserSetClassData(&parser_data))
    {
        fprintf(stderr, "ERROR: Failed to set parser class data\n");

        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Start the parser

    if(parserStart(&fgcddev.parser))
    {
        fprintf(stderr, "ERROR: Failed to start parser\n");

        parserClearClassData(FGC_CLASS_ID);
        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Set device command queue

    fgcddev.device->command_queue = &fgcddev.parser.command_queue;

    // Set device as online

    fgcddev.device->online  = 1;
    fgcddev.device->ready   = 1;

    // Load configuration

    nonvolLoad(fgcddev.device, 0);

    // Load properties that must be set last

    nonvolLoad(fgcddev.device, 1);

    return 0;
}

void fgcddevInitDevice(void)
{
    fgcddevGenerateNameCode();
}



void fgcddevPublish(void)
{
    uint32_t            pub_ms      = ntohl(pub.published_data.time_usec) / 1000;
    struct fgc1_stat    *pub_status = &pub.published_data.status[0].class_data.c1;

    // Check whether this is the first publication of the second

    if(!(pub_ms / FGCD_CYCLE_PERIOD_MS))
    {
        // Update per second TCP, UDP and fieldbus load statistics

        fgcddev.status.udp_pub_load = pub.udp_load;
        pub.udp_load                = 0;

        fgcddev.status.fieldbus_rx_load = fieldbus.stats.rx.load;
        fgcddev.status.fieldbus_tx_load = fieldbus.stats.tx.load;

        fieldbus.stats.rx.load = 0;
        fieldbus.stats.tx.load = 0;

        rt.udp_load = 0;

        fgcddev.status.tcp_load = tcp.load;
        tcp.load                = 0;
    }

    // Set unlatched status

    if(rt.hold_values)
    {
        fgcddev.status.st_unlatched |=  FGC_UNL_RT_HOLD;
    }
    else // RT values are not held
    {
        fgcddev.status.st_unlatched &= ~FGC_UNL_RT_HOLD;
    }

    if(fieldbus.real_time_zero)
    {
        fgcddev.status.st_unlatched |=  FGC_UNL_RT_ZERO;
    }
    else // RT values are not zeroed
    {
        fgcddev.status.st_unlatched &= ~FGC_UNL_RT_ZERO;
    }

    // Update cycle information

    fgcddev.status.cycle_num    = timing.cycle_num;
    fgcddev.status.cycle_user   = timing.user;

    // Copy device status into published data

    memcpy(pub_status, &fgcddev.status, sizeof(fgcddev.status));

    // Byte-swap published data

    pub_status->cmw_subs            = htons(pub_status->cmw_subs);
    pub_status->compile_time        = htonl(pub_status->compile_time);
    pub_status->fgc_mask            = htonl(pub_status->fgc_mask);
    pub_status->fieldbus_rx_load    = htonl(pub_status->fieldbus_rx_load);
    pub_status->fieldbus_tx_load    = htonl(pub_status->fieldbus_tx_load);
    pub_status->st_faults           = htons(pub_status->st_faults);
    pub_status->st_unlatched        = htons(pub_status->st_unlatched);
    pub_status->st_warnings         = htons(pub_status->st_warnings);
    pub_status->tcp_load            = htonl(pub_status->tcp_load);
    pub_status->udp_pub_load        = htonl(pub_status->udp_pub_load);
}



/*
 * Generate name code
 */

static void fgcddevGenerateNameCode(void)
{
    uint32_t                channel;
    struct fgc_code_info    code_info;
    uint32_t                i;
    struct fgc_name_db      name_db;
    uint32_t                sub_dev_num;

    // Initialise the name database and code information to zero

    memset(&name_db,    0, sizeof(name_db));
    memset(&code_info,  0, sizeof(code_info));

    // Disable sending of code

    fgcddev.send_fgc_code_flag = 0;

    // Set name of code

    snprintf(fgcddev.fgc_code_name[0], sizeof(fgcddev.fgc_code_name[0]), "name");

    // Invalidate code currently being sent

    fgcddev.fgc_code_tx_valid_flag = 0;

    // Insert name data for each device

    for(i = 0 ; i < FGCD_MAX_DEVS && i < FGC_CODE_NAMEDB_MAX_DEVS ; i++)
    {
        name_db.dev[i].class_id     = htons(fgcd.device[i].fgc_class);
        name_db.dev[i].omode_mask   = htons(fgcd.device[i].omode_mask);

        // Check whether a name is defined for the device

        if(fgcd.device[i].name)
        {
            strncpy(name_db.dev[i].name, fgcd.device[i].name, FGC_MAX_DEV_LEN);
            name_db.dev[i].name[FGC_MAX_DEV_LEN] = '\0';
        }
        else // No name is defined
        {
            name_db.dev[i].name[0] = '\0';
        }
    }

    // Insert name data for each sub-device

    sub_dev_num = 0;
    for(i = 1 ; i < FGCD_MAX_DEVS && i < FGC_CODE_NAMEDB_MAX_DEVS ; i++)
    {
        for(channel = 1 ; channel     <= FGCD_MAX_SUB_DEVS_PER_DEV &&
                          sub_dev_num <  FGC_CODE_NAMEDB_MAX_SUBDEVS ; channel++)
        {
            if(devname.device_sub_devices[i][channel]) // A sub-device is present
            {
                // Increment number of sub_devs for device

                name_db.num_sub_devs[i]++;

                // Set sub-device index

                name_db.sub_dev[sub_dev_num].index = devname.device_sub_devices[i][channel]->index;

                // Set sub-device name

                strncpy(name_db.sub_dev[sub_dev_num].name,
                        devname.device_sub_devices[i][channel]->key.c,
                        sizeof(name_db.sub_dev[sub_dev_num].name));
                name_db.sub_dev[sub_dev_num].name[sizeof(name_db.sub_dev[sub_dev_num].name) - 1] = '\0';

                sub_dev_num++;
            }
        }
    }

    // Initialise the name code to zero

    memset(&fgcddev.fgc_code[0], 0, sizeof(fgcddev.fgc_code[0]));

    // Copy the name database into the code slot

    memcpy(&fgcddev.fgc_code[0], &name_db, sizeof(name_db));

    fgcddev.fgc_code_class[0]   = code_info.class_id = 0;
    fgcddev.fgc_code_id[0]      = code_info.code_id  = 0;
    fgcddev.fgc_code_version[0] = fgcddev.fgc_code_old_version[0] = 0;
    code_info.version           = htonl(fgcddev.fgc_code_version[0]);

    // Copy code information to the name code

    memcpy(&fgcddev.fgc_code[0][(FGC_CODE_NAMEDB_SIZE_BLKS * FGC_CODE_BLK_SIZE) - sizeof(code_info)],
           &code_info, sizeof(code_info));

    // Generate CRC

    fgcddev.fgc_code_length[0]  = FGC_CODE_NAMEDB_SIZE_BLKS * FGC_CODE_BLK_SIZE;
    fgcddev.fgc_code_chksum[0]  = crcCrc16((unsigned char *)&fgcddev.fgc_code[0],
                                           fgcddev.fgc_code_length[0] - sizeof(code_info.crc));
    code_info.crc               = htons(fgcddev.fgc_code_chksum[0]);

    // Copy CRC to the name code

    memcpy(&fgcddev.fgc_code[0][fgcddev.fgc_code_length[0] - sizeof(code_info.crc)],
           &code_info.crc, sizeof(code_info.crc));

    // Enable sending of code

    fgcddev.send_fgc_code_flag = 1;
}



int32_t fgcddevReadCodes(void)
{
    struct fgc_code_info    code_info;
    static char             code_path[256];
    int32_t                 error = 0;
    int                     file;
    char                    filename[256];
    int                     i,j;
    ssize_t                 length;
    struct stat             stat_buf;
    struct dirent 			**namelist;
    int n;

    // Clear forced code index

    fgcddev.fgc_code_tx_force = FGC_CODE_NUM_SLOTS;

    // Disable sending of code

    fgcddev.send_fgc_code_flag = 0;

    // Zero attributes of all codes

    for(i = 1 ; i < FGC_CODE_NUM_SLOTS ; i++) // i = 1 to skip name code which is generated by devnameRead()
    {
        fgcddev.fgc_code_name[i][0]     = '\0';
        fgcddev.fgc_code_length[i]      = 0;
        fgcddev.fgc_code_class[i]       = 0;
        fgcddev.fgc_code_id[i]          = FGC_CODE_NULL;
        fgcddev.fgc_code_version[i]     = 0;
        fgcddev.fgc_code_old_version[i] = 0;
        fgcddev.fgc_code_chksum[i]      = 0;
    }

    // Construct code group path for this host

    snprintf(code_path, sizeof(code_path), "%s/%s",
             FGCDDEV_CODE_GROUP_PATH, fgcd.group);

    // Read code directory in lexicographical order

    if((n = scandir(code_path, &namelist, NULL, alphasort)) < 0)
    {
        perror("scandir");
        return (1);
    }

    // Read each code file

    i = 1; // i = 1 to skip name code which is generated by devnameRead()
    j = 0;  // iterator of the namelist
    while(i < FGC_CODE_NUM_SLOTS && j < n)
    {
        // Find next code file

        snprintf(filename, sizeof(filename), "%s/%s", code_path, namelist[j]->d_name);

        j++;

        // Can not stat file

        if(stat(filename, &stat_buf))
        {
            perror("stat");
            error = 1;
            break;
        }

        // Is it a regular file?

        if(!S_ISREG(stat_buf.st_mode))
        {
            continue;
        }

        // Open code file

        if((file = open(filename, O_RDONLY)) < 0)
        {
            logPrintf(0, "FGCDDEV Error opening code file %s\n", filename);
            error = 1;
            continue;
        }

        // Read code

        if((length = read(file, fgcddev.fgc_code[i], FGC_CODE_SIZE)) < 0) // Error reading file
        {
            logPrintf(0, "FGCDDEV Error reading code file %s\n", filename);
            error = 1;
            continue;
        }
        close(file);

        // Extract code information

        if(length >= FGCDDEV_FGC_CODE_MIN_LEN) // Code is greater than or equal to minimum length
        {
            memcpy(&code_info,&fgcddev.fgc_code[i][length - FGC_CODE_INFO_SIZE],sizeof(struct fgc_code_info));

            fgcddev.fgc_code_class[i]       = code_info.class_id;
            fgcddev.fgc_code_id[i]          = code_info.code_id;
            fgcddev.fgc_code_version[i]     = ntohl(code_info.version);
            fgcddev.fgc_code_old_version[i] = ntohs(code_info.old_version);
            fgcddev.fgc_code_chksum[i]      = ntohs(code_info.crc);
            fgcddev.fgc_code_length[i]      = length;
        }

        // Set code name

        strncpy(fgcddev.fgc_code_name[i], namelist[j-1]->d_name, sizeof(fgcddev.fgc_code_name[i]));
        fgcddev.fgc_code_name[i][sizeof(fgcddev.fgc_code_name[i]) - 1] = '\0';

        logPrintf(0, "FGCDDEV Read %d bytes from code file %s into slot %u\n", length, filename, i);
        i++;
    }

    // Free scandir memory

    for(i=0; i != n; ++i)
    {
        free(namelist[i]);
    }
    free(namelist);

    // Check whether there are still code files remaining

    if(n > FGC_CODE_NUM_SLOTS)
    {
        logPrintf(0, "FGCDDEV More code files than available slots\n");
        error = 1;
    }

    // Enable sending of code

    fgcddev.fgc_code_tx_num     = 0;
    fgcddev.send_fgc_code_flag  = 1;

    return error;
}

// EOF
