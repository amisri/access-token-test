/*!
 * @file   fieldbus_class.c
 * @author Stephen Page
 *
 * Class-specific functions for the fieldbus
 */

#include <cmd.h>
#include <ether.h>
#include <fieldbus_class.h>
#include <fgcsm.h>

int32_t fieldbusSendCommand(uint32_t channel_index, struct Cmd *cmd)
{
    int32_t status;

    if((status = etherSendCommand(channel_index, cmd)))
    {
        return status;
    }
    else // Command was sent successfully
    {
        fgcsmCmdQueued(channel_index);
        fgcsmCmdSent(channel_index);
        return 0;
    }
}

// EOF
