/*!
 * @file   prop_class.c
 * @author Stephen Page
 *
 * Class-specific functions for getting and setting properties
 */

#include <string.h>

#include <cmd.h>
#include <defconst.h>
#include <defprops.h>
#include <fgc_code.h>
#include <fgc_errs.h>
#include <fgc_event.h>
#include <fgcddev.h>
#include <fieldbus.h>
#include <prop.h>
#include <prop_class.h>



// Global variables

struct Prop_class prop_class;



int32_t SetifResetOk(struct Cmd *command)
{
    return 1;
}



int32_t SetEvent(struct Cmd *command, struct prop *property)
{
    struct fgc_event    fgc_event;
    int32_t             return_val;

    if(!(return_val = SetInteger(command, property))) // Property set successfully
    {
        // Check that event type and payload are within accepted range

        if(prop_class.event_buf[0] > 0xFF || // Event type
           prop_class.event_buf[1] > 0xFF || // Trigger type
           prop_class.event_buf[2] > 0xFFFF) // Payload
        {
            return FGC_OUT_OF_LIMITS;
        }

        fgc_event.type          = prop_class.event_buf[0];
        fgc_event.trigger_type  = prop_class.event_buf[1];
        fgc_event.payload       = prop_class.event_buf[2];

        // Note that one cycle period is added to the delay.  This is to compensate
        // for the sending of the event by a non-real-time thread.  The extra cycle
        // will be subtracted by fieldbusUpdateEvents().

        fgc_event.delay_us = prop_class.event_buf[3] + (FGC_FIELDBUS_CYCLE_PERIOD_MS * 1000);

        if(fieldbusSendEvent(&fgc_event))
        {
            return FGC_BUSY;
        }
    }
    return return_val;
}



int32_t SetFGCCode(struct Cmd *command, struct prop *property)
{
    struct fgc_code_info    code_info;
    uint32_t                i;
    uint32_t                length;
    int32_t                 return_val;

    // Disable sending of code

    fgcddev.send_fgc_code_flag = 0;

    if(!(return_val = SetInteger(command, property))) // Property set successfully
    {
        // Invalidate code currently being sent

        fgcddev.fgc_code_tx_valid_flag = 0;

        // Identify code slot

        for(i = 0 ; i < FGC_CODE_NUM_SLOTS ; i++)
        {
            if(command->parser->value == (char*)&fgcddev.fgc_code[i]) // Code slot matches
            {
                // Clear code name

                fgcddev.fgc_code_name[i][0] = '\0';

                // Zero code attributes to prevent it being sent

                fgcddev.fgc_code_class[i]       = 0;
                fgcddev.fgc_code_id[i]          = FGC_CODE_NULL;
                fgcddev.fgc_code_version[i]     = 0;
                fgcddev.fgc_code_old_version[i] = 0;
                fgcddev.fgc_code_chksum[i]      = 0;

                // Check that code is larger than minimum length

                length = fgcddev.fgc_code_length[i];

                if(length >= FGCDDEV_FGC_CODE_MIN_LEN)
                {
                    // Set code attributes

                    memcpy(&code_info,&fgcddev.fgc_code[i][length - FGC_CODE_INFO_SIZE],sizeof(struct fgc_code_info));

                    fgcddev.fgc_code_class[i]       = code_info.class_id;
                    fgcddev.fgc_code_id[i]          = code_info.code_id;
                    fgcddev.fgc_code_version[i]     = ntohl(code_info.version);
                    fgcddev.fgc_code_old_version[i] = ntohs(code_info.old_version);
                    fgcddev.fgc_code_chksum[i]      = ntohs(code_info.crc);
                }

                break;
            }
        }
    }

    // Enable sending of code

    fgcddev.send_fgc_code_flag = 1;

    return return_val;
}



int32_t SetFGCLogSync(struct Cmd *command, struct prop *property)
{
    fieldbus.fgc_log_sync = FGC_LOG_SYNC_BITS;
    return 0;
}



int32_t SetReadCodes(struct Cmd *command, struct prop *property)
{
    command->error = fgcddevReadCodes();

    if(command->error)
    {
        command->error = FGC_ERROR_READING_FILE;
    }
    return command->error;
}



int32_t SetTerminate(struct Cmd *command, struct prop *property)
{
    return Terminate(command, property);
}

// EOF
