/*!
 * @file   pm.h
 * @brief  Declarations for submitting post-mortem data
 * @author Stephen Page
 */

#pragma once

#include <semaphore.h>
#include <stdint.h>

#include <cmd.h>
#include <consts.h>
#include <fgc_pubdata.h>
#include <fgc_stat.h>
#include <queue.h>

// Constants

#define PM_EXT_BUF_LEN                  3000        //!< Number of entries in post-mortem buffers per device following an external trigger
#define PM_FGC_LOG_MAX_LEN              650000      //!< Maximum length of FGC post-mortem log in bytes
#define PM_FGC_LOGS_RETRIES             5           //!< Number of times to retry extracting FGC logs following self-trigger
#define PM_SELF_BUF_LEN_STD             3500        //!< Number of entries in post-mortem buffers per device following a self-trigger
#define PM_SERVICE_NAME                 "pm-dc"     //!< Name of the PM service to which post-mortem dumps are to be sent
#define PM_SYSTEM_NAME                  "FGC"       //!< System name to be reported with post-mortem dumps
#define PM_MIN_CYCLES_51                3000        //!< Minimum number of cycles of post-mortem data to take following a self-trigger
#define PM_MIN_CYCLES_STD               500         //!< Minimum number of cycles of post-mortem data to take following a self-trigger
#define PM_NUM_CMDS_PER_DEV             10          //!< Number of commands that may be used for post-mortem per device
#define PM_RETRY_PERIOD                 20          //!< Time in seconds between retries of post-mortem submission
#define PM_WAIT_CYCLES_EXT_TRIG         1500        //!< Number of cycles to wait before freezing post-mortem buffer following external-trigger
#define PM_WAIT_CYCLES_FGC_LOGS_51      15000       //!< Number of cycles to wait for FGC logs to be ready and extracted following self-trigger (class 51 only)
#define PM_WAIT_CYCLES_FGC_LOGS_STD     750         //!< Number of cycles to wait for FGC logs to be ready and extracted following self-trigger
#define PM_WAIT_CYCLES_SELF_TRIG_51     18000       //!< Number of cycles to wait before freezing post-mortem buffer following self-trigger (unless LOW_CURRENT reached) (class 51 only)
#define PM_WAIT_CYCLES_SELF_TRIG_STD    500         //!< Number of cycles to wait before freezing post-mortem buffer following self-trigger

//! Structure for post-mortem status buffer sample

struct PM_data
{
    uint32_t        time_sec;   //!< Unix time in seconds
    uint32_t        time_usec;  //!< Unix time microseconds
    float           real_time;  //!< Real-time value
    struct fgc_stat status;     //!< Device status
};

//! Structure for post-mortem acquisition information

struct PM_acq_info
{
    uint32_t frozen;        //!< Flag to indicate whether buffer is frozen
    uint32_t status_index;  //!< Index within status buffer for acquisition
    uint32_t time_sec;      //!< Timestamp seconds
    uint32_t time_nsec;     //!< Timestamp nanoseconds
    uint32_t wait_cycles;   //!< Number of cycles to wait before freezing buffer
};

//! Structure for externally triggered post-mortem buffer

struct PM_ext_buffer
{
    struct fgc_stat trigger;                //!< FGC status at time of trigger
    struct PM_data  status[PM_EXT_BUF_LEN]; //!< FGC status buffer
};

//! Structure for externally-triggered post-mortem acquisitions

struct PM_ext_acq
{
    struct PM_acq_info      acq_info;   //!< Acquisition information
    struct PM_ext_buffer    buffer;     //!< Post-mortem buffer
};

//! Structure for self-triggered post-mortem buffer

struct PM_self_buffer_std
{
    struct fgc_stat trigger;                            //!< FGC status at time of trigger
    struct PM_data  status[PM_SELF_BUF_LEN_STD];        //!< FGC status buffer
    uint8_t         fgc_buffers[PM_FGC_LOG_MAX_LEN];    //!< Buffers from FGC
};

//! Union for self-triggered post-mortem buffer from all devices

union pm_self_buffer
{
    struct fgc_stat             trigger;
    struct PM_self_buffer_std   std;
};

//! Structure for self-triggered post-mortem acquisitions

struct PM_self_acq
{
    struct PM_acq_info      acq_info;           //!< Acquisition information
    union pm_self_buffer    buffer;             //!< Post-mortem buffer
    struct Queue            cmd_response_queue; //!< Response queue for post-mortem commands
    uint32_t                cmds_queued;        //!< Number of post-mortem commands queued for device
    struct Cmd              *response;          //!< Response to post-mortem extraction command
    uint32_t                wait_fgc_logs;      //!< Flag to indicate whether waiting to send command to retrieve device post-mortem buffers
    uint32_t                fgc_logs_retries;   //!< Number of retries remaining to extract device logs
    uint32_t                wait_low_current;   //!< Flag to indicate whether to wait for FGC low current flag before freezing buffer
};

//! Union of external and self-triggered post-mortem buffers

union pm_buffer
{
    struct PM_ext_buffer    ext;
    union pm_self_buffer    self;
};

//! Struct containing global variables

struct Pm
{
    sem_t           sem;    //!< Semaphore to trigger the sending of post-mortem data
    pthread_t       thread; //!< Thread handle
    union pm_buffer buffer; //!< Buffer for sending post-mortem
};

extern struct Pm pm_globals;

// External functions

//! Start the post-mortem thread

int32_t pmStart(void);

/*!
 * Update Post Mortem data for one FGC device
 *
 * This function is called from the publication thread (fgcd/src/pub.cpp) once per cycle for devices
 * which implement Post Mortem .
 *
 * @param[in]    channel      Address of the FGC device
 * @param[in]    status       Pointer to the published status for the device
 * @param[in]    time_sec     Timestamp for the status (seconds)
 * @param[in]    time_usec    Timestamp for the status (microseconds)
 */

void pmPubUpdate(uint8_t channel, struct fgc_stat *status, uint32_t time_sec, uint32_t time_usec);

// EOF
