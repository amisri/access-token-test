/*!
 * @file   defprops_inc.h
 * @brief  Includes header files needed by defprops.h
 * @author Stephen Page
 */

#ifndef DEFPROPS_INC_H
#define DEFPROPS_INC_H

#include <alarms.h>
#include <ether.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <fgcsm.h>
#include <fieldbus.h>
#include <hash.h>
#include <prop.h>
#include <prop_class.h>
#include <rt.h>
#include <tcp.h>
#include <timing.h>
#include <version.h>

#endif

// EOF
