/*!
 * @file   pmd_63.h
 * @brief  Declaration of the PMX binary format for class 63
 * @author Marc Magrans de Abril
 */

#pragma once

#include <cstdint>
#include <pm-dc-rda3-lib/PMClient.hpp>

#include <fgcd.h>
#include <pm.h>

void pmd_63GenerateAndSendExt(pm::Client &pmclient, struct FGCD_device *device, const struct PM_ext_buffer &buffer, int64_t timestamp);
void pmd_63GenerateAndSendSelf(pm::Client &pmclient, struct FGCD_device *device, const struct PM_self_buffer_std &buffer, int64_t timestamp);

// EOF
