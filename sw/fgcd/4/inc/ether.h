/*!
 * @file   ether.h
 * @brief  Declarations for the FGC_Ether fieldbus
 * @author Stephen Page
 */

#ifndef ETHER_H
#define ETHER_H

#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>

#include <consts.h>
#include <fgc_ether.h>

// Forward declarations

 struct Cmd;

// Constants

#define ETHER_NETWORK_INTERFACE     "eth1"                                  //!< Name of the FGC_Ether interface
#define ETHER_RX_RING_FRAMES        512                                     //!< Size of reception ring buffer in frames
#define ETHER_RX_RING_SMEM_SIZE     (ETHER_RX_RING_FRAMES * getpagesize())  //!< Size of shared memory for reception ring buffer


/*!
 * Struct containing global variables
 */

struct Ether
{
    pthread_t       reception_thread;               //!< Reception thread
    unsigned int    interface_index;                //!< Index of network interface
    uint8_t         interface_address[8];           //!< MAC address of network interface
    char           *rx_ring_buffer;                 //!< Reception ring buffer
    uint32_t        rx_ring_offset;                 //!< Offset within reception ring buffer in pages
    int             socket;                         //!< Network socket

    // Statistics

    struct Ether_stats
    {
        // Reception

        struct Ether_stats_rx
        {
            uint32_t    frames;                     //!< Count of number of frames received
            uint32_t    bad_address;                //!< Count of number of frames received with an invalid source address
            uint32_t    bad_frames;                 //!< Count of total number of bad frames received
            uint32_t    bad_size;                   //!< Count of number of frames received with an incorrect size
            uint32_t    bad_time;                   //!< Count of number of frames received at an invalid time within the fieldbus cycle
        } rx;

        // Transmission

        struct Ether_stats_tx
        {
            uint32_t    frames;                     //!< Count of number of frames sent
            uint32_t    failed;                     //!< Count of number of frames that failed to send
        } tx;
    } stats;

    // Frames

    struct Ether_frames
    {
        struct fgc_ether_cmd    command;            //!< FGC_Ether command frame
        struct fgc_ether_time   time;               //!< FGC_Ether time frame
    } frames;
};

extern struct Ether ether;

// External functions

/*!
 * Start the fieldbus
 */

int32_t etherStart(void);

/*!
 * Send a frame on the fieldbus.
 *
 * If channel is 0 then a broadcast will be sent
 */

ssize_t etherSend(uint32_t channel_num, struct fgc_ether_frame *frame, uint32_t size);

/*!
 * Send time frame
 */

int32_t etherSendTime(void);

/*!
 * Send a command frame
 */

int32_t etherSendCommand(uint32_t channel_num, struct Cmd *cmd);

/*!
 * Prepare real-time references within time frame
 */

void etherPrepareRT(void);

#endif

// EOF
