/*!
 * @file   fieldbus_class.h
 * @brief  Include header for fieldbus type used by a class
 * @author Stephen Page
 */

#ifndef FIELDBUS_CLASS_H
#define FIELDBUS_CLASS_H

#include <stdint.h>

// Forward delcarations

struct Cmd;

// Constants

#define FIELDBUS_TIMING_COUNTER_LONG    4   //!< Timing counter used for long fieldbus cycle triggers

// External functions

/*!
 * Set an event
 */

int32_t fieldbusSendCommand(uint32_t channel_index, struct Cmd *cmd);


#endif

// EOF
