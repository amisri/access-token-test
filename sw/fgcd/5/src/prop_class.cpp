/*!
 * @file   prop_class.c
 * @author Stephen Page
 *
 * Class-specific functions for getting and setting properties
 */

#include <cmd.h>
#include <defconst.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <prop.h>
#include <prop_class.h>



int32_t SetifResetOk(struct Cmd *command)
{
    return 1;
}



int32_t SetTerminate(struct Cmd *command, struct prop *property)
{
    return Terminate(command, property);
}

// EOF
