/*!
 * @file   timing_handlers.c
 * @author Stephen Page
 *
 * Functions for handling timing events
 */


#include <stdio.h>
#include <string.h>

#include <consts.h>
#include <fgcddev.h>
#include <logging.h>
#include <pub.h>
#include <timing.h>
#include <timing_handlers.h>


void timingAddMultiPpmEvent(const char *mppm_condition)
{
    ; // Multi-PPM not supported in this class
}

void timingMillisecond(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t        ms = evt_time.time.tv_nsec / 1000000;
    struct timeval  time;

    if (ms % FGCD_CYCLE_PERIOD_MS)
    {

        // Set time

        time.tv_sec  = evt_time.time.tv_sec;
        time.tv_usec = ms * 1000;
        timingSetUserTime(0, &time);

        // Check timing status and set fault if necessary

        timingCheckStatus();

        // Timestamp published data

        pub.published_data.time_sec     = htonl(time.tv_sec);
        pub.published_data.time_usec    = htonl(time.tv_usec);

        // Update published data for FGCD device

        fgcddevPublish();

        // Trigger sending of published data

        pubTrigger();
    }
}

struct Timing_event_handler timing_event_handlers[]
= {
    { timing_handler_names[TIMING_HANDLER_POSIXMS],        &timingMillisecond          },
    { "",                                                  NULL                        },
};


// EOF
