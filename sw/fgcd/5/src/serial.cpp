/*!
 * @file   serial.c
 * @author Stephen Page
 *
 * Functions for serial communications
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <fgc_errs.h>
#include <fgc_stat.h>
#include <fgcd.h>
#include <logging.h>
#include <pub.h>
#include <scrp.h>
#include <serial.h>
#include <queue.h>
#include <fgcd_thread.h>



// Struct containing global variables

struct Serial
{
    pthread_t    thread[FGCD_MAX_EQP_DEVS];          // Thread handles for serial devices
    struct Queue command_queue[FGCD_MAX_EQP_DEVS];   // Queues of commands for serial devices
};



// Global variables

struct Serial serial;



// Static functions

static FGCD_thread_func serialRun;



int32_t serialStart(void)
{
    struct FGCD_device *device;
    int32_t             i;

    // Initialise a command queue and start a thread for each equipment device

    for(i = 0 ; i < FGCD_MAX_EQP_DEVS ; i++)
    {
        // Set remote device flag

        fgcd.device[i + 1].remote = 1;

        if(fgcd.device[i + 1].fgc_class != FGC_CLASS_UNKNOWN)
        {
            // Initialise serial command queue

            if(queueInit(&serial.command_queue[i], cmdqmgr.num_cmds))
            {
                // Failed to initialise command queue

                fprintf(stderr, "ERROR: Failed to initialise serial command queue for device %d\n", i + 1);

                // Free device command queues created so far

                for(i-- ; i >= 0 ; i--)
                {
                    queueFree(&serial.command_queue[i]);
                }
                return 1;
            }

            // Start serial thread

            if(fgcd_thread_create(&serial.thread[i], serialRun, &fgcd.device[i + 1], SERIAL_THREAD_POLICY, SERIAL_THREAD_PRIORITY, NO_AFFINITY) != 0)
            {
                fprintf(stderr, "ERROR: Can not start serialRun thread %d\n",i);
                return 1;
            }
        }
    }

    // Set devices as online

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        device = &fgcd.device[i];

        if(device->fgc_class != FGC_CLASS_UNKNOWN)
        {
            device->command_queue   = &serial.command_queue[i - 1];
            device->online          = 1;
            device->ready           = 1;

            // Set FGC class in published data for device

            pub.published_data.status[i].class_id = device->fgc_class;
        }
    }

    return 0;
}


/*
 * Serial thread
 */

static void *serialRun(void *arg)
{
    struct Cmd      *command;
    struct fgc_diag diag_data        = {0};
    char            serial_device[16];
    FILE            *serial_port;
    struct timespec sleep_time;

    struct FGCD_device *device = reinterpret_cast<struct FGCD_device *>(arg);

    // Initialise serial device name

    snprintf(serial_device, sizeof(serial_device), "%s%u", SERIAL_DEVICE_BASE, device->id - 1);

    // Handle commands and receive diagnostic data

    while(1)
    {
        if((command = reinterpret_cast<struct Cmd*>(queuePop(&serial.command_queue[device->id - 1]))))
        {
            // Reject unhandled command types

            switch(command->type)
            {
                // Allowed command types

                case CMD_GET:
                case CMD_SET:
                    break;

                default:
                    command->error = FGC_NOT_IMPL;
                    queueBlockPush(command->response_queue, command);
                    continue;
            }

            // Open the serial port for the device

            if(!(serial_port = scrpOpen(serial_device)))
            {
                logPrintf(device->id, "SERIAL Failed to open serial device %s\n", serial_device);

                // Set device as offline

                device->online = 0;

                command->error = FGC_DEV_NOT_READY;
                queueBlockPush(command->response_queue, command);
                continue;
            }

            // Check whether the device has returned to the online state

            if(!device->online)
            {
                logPrintf(device->id, "SERIAL Online\n");
                device->online = 1;
            }

            // Send the command to the serial device

            switch(scrpCmd(serial_port, command->type == CMD_GET ? 'G' : 'S',
                           command->command_string, command->value, CMD_MAX_VAL_LENGTH,
                           &command->value_length))
            {
                case 1: // Command returned an error

                    // Read error number from response

                    errno = 0;
                    command->error = strtoul(command->value, NULL, 0);
                    if(errno || !command->error || command->error >= FGC_NUM_ERRS)
                    {
                        logPrintf(device->id, "SERIAL Bad error code for command \"%s\".  Response was \"%s\"\n",
                                  command->command_string, command->value);
                        command->error = FGC_UNKNOWN_ERROR_CODE;
                    }
                    break;

                case 0: // Command succeeded
                    break;

                case -1: // A communication error occurred
                    logPrintf(device->id, "SERIAL Communication error for command \"%s\" for %s\n",
                              command->command_string, serial_device);
                    command->error = FGC_DEV_NOT_READY;
                    break;

                default: // An unexpected value was returned
                    logPrintf(device->id,
                              "SERIAL Unexpected return value from scrpCmd() for command \"%s\" for %s\n",
                              command->command_string, serial_device);
                    command->error = FGC_PROTO_ERROR;
                    break;
            }
            scrpClose(serial_port);

            // Return the command to the response queue

            queueBlockPush(command->response_queue, command);
        }
        else // No command is pending
        {
            // Open the serial port for the device

            if(!(serial_port = scrpOpen(serial_device)))
            {
                // Set device as offline

                if(device->online)
                {
                    logPrintf(device->id, "SERIAL Failed to open serial device %s\n", serial_device);
                    device->online = 0;
                }

                // Sleep to limit continuous failure of port opening

                sleep_time.tv_sec   = 0;
                sleep_time.tv_nsec  = 100000000; // 100ms
                while(nanosleep(&sleep_time, &sleep_time) == EINTR);

                continue;
            }

            // Check whether the device has returned to the online state

            if(!device->online)
            {
                logPrintf(device->id, "SERIAL Online\n");
                device->online = 1;
            }

            // Get diagnostic data

            if(!scrpDiag(serial_port, &diag_data))
            {
                // Set published data

                pub.published_data.status[device->id].class_id   = diag_data.class_id;
                pub.published_data.status[device->id].class_data = diag_data.class_data;
            }
            else // Failed to read diagnostic data
            {
                // Sleep to limit continuous failure of diagnostic read

                sleep_time.tv_sec   = 0;
                sleep_time.tv_nsec  = 100000000; // 100ms
                while(nanosleep(&sleep_time, &sleep_time) == EINTR);
            }
            scrpClose(serial_port);
        }
    }

    return 0;
}

// EOF
