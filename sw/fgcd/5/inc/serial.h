/*!
 * @file   serial.h
 * @brief  Declarations for serial communications
 * @author Stephen Page
 */

#ifndef SERIAL_H
#define SERIAL_H

#include <stdint.h>

// Constants

#define SERIAL_DEVICE_BASE      "/dev/ttyUSB"   //!< Base of serial device file names


// External functions

/*!
 * Start the serial threads
 */

int32_t serialStart(void);

#endif

// EOF
