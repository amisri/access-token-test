//! @file   defprops_class.h
//! @brief  Includes defconst, definfo and defprops for the class 91


#ifndef DEFPROPS_CLASS_H
#define DEFPROPS_CLASS_H

#include <classes/91/defconst.h>
#include <classes/91/definfo.h>
#include <classes/91/defprops.h>

#endif

// EOF
