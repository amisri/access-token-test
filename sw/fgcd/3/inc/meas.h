/*!
 * @file   meas.h
 * @brief  Declarations for measurement
 * @author Stephen Page
 */

#ifndef MEAS_H
#define MEAS_H

#include <stdint.h>

#include <mugefhw/mpv915-21.h>

#define MEAS_MAX_DELAY_MS   MPV915_MEAS_DELAY_MS

#include <mugef/consts.h>

/*!
 * Measurement channel structure
 */

struct Meas_chan
{
    float       i_a;        //!< Measured current from power converter channel A
    float       i_b;        //!< Measured current from power converter channel B

    float       i_meas;     //!< Measured current
    float       ref_meas;   //!< Measured reference output

    uint32_t    sim;        //!< Flag to indicate whether to simulate measurements when simulating the voltage source
};

/*!
 * Struct containing global variables
 */

struct Meas
{
    uint32_t                delay_ms;                               //!< Delay in measurement in milliseconds
    struct mpv915_card      mpv915[MPV915_NUM_ADDRS];               //!< MPV915 cards
    uint32_t                mpv915_bad_values[MPV915_NUM_ADDRS];    //!< Counts of the numbers of bad values read from MPV915 cards
    struct Meas_chan        chan[1 + FGCD_MAX_EQP_DEVS];            //!< Measurement channels
};

extern struct Meas meas;

// External functions

/*!
 * Initialise measurement
 */

int32_t measInit(void);

/*!
 * Acquire measurement
 */

void measAcquire(void);

#endif

// EOF
