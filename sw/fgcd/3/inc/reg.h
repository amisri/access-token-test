/*!
 * @file   reg.h
 * @brief  Declarations for regulation
 * @author Stephen Page
 */

#ifndef REG_H
#define REG_H

#include <stdint.h>

#include <libreg1.h>

#include <consts.h>

#include <equipdev.h>
#include <funcgen.h>

/*!
 * Struct containing global variables
 */

struct Reg
{
    struct reg_converter        converter[1 + FGCD_MAX_EQP_DEVS];                   //!< Power converter regulation configuration
    struct reg_converter_pars   pars     [1 + FGCD_MAX_EQP_DEVS];                   //!< Power converter regulation parameters

    // Note that the size of the second dimension of the following array
    // assumes that the maximum regulation pure delay is 1 second

    float                       i_err_buffer[1 + FGCD_MAX_EQP_DEVS][FUNCGEN_FREQ];  //!< Buffer for I_ERR calculation
    float                       v_err_buffer[1 + FGCD_MAX_EQP_DEVS][FUNCGEN_FREQ];  //!< Buffer for V_ERR calculation
};

extern struct Reg reg;

// External functions

/*!
 * Regulate each channel.
 *
 * This function is called every millisecond
 */

void regRegulate(void);

#endif

// EOF
