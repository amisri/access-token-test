/*!
 * @file   funcgen.h
 * @brief  Declarations for function generation
 * @author Stephen Page
 */

#ifndef FUNCGEN_H
#define FUNCGEN_H

#include <stdint.h>

#include <libfg1.h>
#include <libfg/plep.h>
#include <libfg/table.h>
#include <libfg/test.h>
#include <libfg/trim.h>

#include <consts.h>
#include <classes/91/defconst.h>

// Constants

#define FUNCGEN_FREQ            1000                //!< Function generation frequency
#define FUNCGEN_PERIOD          (1. / FUNCGEN_FREQ) //!< Function generation period in seconds

#define FUNCGEN_ECO_PERIOD      0.250               //!< Economy ramp period in seconds

// Types

/*!
 * Parameters for function generation
 */

union funcgen_pars
{
    struct fg_plep_pars     plep;   //!< Parameters for PLEP functions
    struct fg_table_pars    table;  //!< Parameters for table functions
    struct fg_test_pars     test;   //!< Parameters for test functions
    struct fg_trim_pars     trim;   //!< Parameters for trim functions
};

/*!
 * Table reference for function generation
 */

struct Funcgen_table
{
    struct FG_point    function[FGC_TABLE_LEN];     //!< Table points
};

/*!
 * Struct containing global variables
 */

struct Funcgen
{
    uint32_t    previous_state_pc       [1 + FGCD_MAX_EQP_DEVS];    //!< State during previous iteration
    uint32_t    to_cycling_plep_running [1 + FGCD_MAX_EQP_DEVS];    //!< Flag to indicate that a PLEP is running in the TO_CYCLING state
};

extern struct Funcgen funcgen;

// External functions

/*!
 * Initialise function generation
 */

int32_t funcgenInit(void);

/*!
 * Calculate references for next millisecond
 */

void funcgenCalcRefs(void);

/*!
 * Arm the function used in the STARTING state
 */

int32_t funcgenArmStartFunction(uint32_t channel);

/*!
 * Launch a system-triggered PLEP (i.e. one configured by the system rather than a user)
 */

void funcgenSystemPlep(uint32_t channel, float final, float final_rate);

/*!
 * Launch a system-triggered PLEP for the Slow Abort transition
 */

void funcgenSlowAbortPlep(uint32_t channel, float linear_rate);

/*!
 * Start dynamic economy for a channel for this cycle
 */

int32_t funcgenStartDynamicEconomy(uint32_t channel);

#endif

// EOF
