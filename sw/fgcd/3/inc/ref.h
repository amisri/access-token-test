/*!
 * @file   ref.h
 * @brief  Declarations for reference generation
 * @author Stephen Page
 */

#ifndef REF_H
#define REF_H

#include <stdint.h>

#include <mugef/consts.h>
#include <mugefhw/newave3.h>

/*!
 * Struct containing global variables
 */

struct Ref
{
    struct newave3_card     newave[NEWAVE3_NUM_ADDRS];      //!< Newave cards
    int32_t                 ref_set[1 + FGCD_MAX_EQP_DEVS]; //!< Reference that has been set on DAC
};

extern struct Ref ref;

// External functions

/*!
 * Initialise reference generation
 */

int32_t refInit(void);

/*!
 * Update references
 */

void    refUpdateRefs(void);

#endif

// EOF
