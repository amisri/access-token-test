/*!
 * @file   prop_equip.h
 * @brief  Declarations for equipment device-specific properties
 * @author Stephen Page
 */

#pragma once

// Forward declarations

struct prop;
struct Cmd;

// External functions

/*!
 * Check whether the operational mode can be set
 */

int32_t SetifModeOpOk(struct Cmd *command);

/*!
 * Check whether the real-time mode can be set
 */

int32_t SetifModeRtOk(struct Cmd *command);

/*!
 * Check whether operational state is normal or simulation
 */

int32_t SetifOpNormalSim(struct Cmd *command);

/*!
 * Check whether operational state is not calibrating
 */

int32_t SetifOpNotCal(struct Cmd *command);

/*!
 * Check whether power converter is armed
 */

int32_t SetifPcArmed(struct Cmd *command);

/*!
 * Check whether power converter is off
 */

int32_t SetifPcOff(struct Cmd *command);

/*!
 * Check whether power converter is off
 */

int32_t SetifRegStateNone(struct Cmd *command);

/*!
 * Check whether reference type is none
 */

int32_t SetifRefNone(struct Cmd *command);

/*!
 * Check whether a new reference can be accepted
 */

int32_t SetifRefOk(struct Cmd *command);

/*!
 * Check whether reference parameters are unlocked.
 *
 * This condition never fails. If the reference is locked, then a copy of the
 * settings will be taken, then the reference will be unlocked.
 */

int32_t SetifRefUnlock(struct Cmd *command);

/*!
 * Check whether it is okay to move switches
 */

int32_t SetifSwitchOk(struct Cmd *command);

/*!
 * Check whether it is okay to move set a transaction ID
 */

int32_t SetifTxIdOk(struct Cmd *command);

/*!
 * Get a log
 */

int32_t GetLog(struct Cmd *command, struct prop *property);

/*!
 * Get a Spy log.
 *
 * This function adds a log with FGC log headers to the value
 */

int32_t GetLogSpy(struct Cmd *command, struct prop *property);

/*!
 * Set load switch
 */

int32_t SetLoadSwitch(struct Cmd *command, struct prop *property);

/*!
 * Set a power converter state
 */

int32_t SetPC(struct Cmd *command, struct prop *property);

/*!
 * Set polarity
 */

int32_t SetPolaritySwitch(struct Cmd *command, struct prop *property);

/*!
 * Set a reference
 */

int32_t SetRef(struct Cmd *command, struct prop *property);

/*!
 * Sets the control current value. In the Mugef case is an alias for REF NOW,xyz
 */

int32_t SetRefControlValue(struct Cmd *command, struct prop *property);

/*!
 * Reset voltage source state
 */

int32_t SetReset(struct Cmd *command, struct prop *property);

/*
 * Set table function time/ref points
 */
int32_t SetTableFunc(struct Cmd *command, struct prop *property);

/*!
 * Call parameter functions to set parameters for each device
 */

void SetParameters(void);

/*!
 * Set limit parameters
 */

void ParsLimits(uint32_t channel);

/*!
 * Set load parameters
 */

void ParsLoad(uint32_t channel);

/*!
 * Set load saturation parameters
 */

void ParsLoadSaturation(uint32_t channel);

/*!
 * Set parameters to select a new load
 */

void ParsLoadSelect(uint32_t channel);

/*!
 * Set reference function run time parameters (not used).
 *
 * This function is required by the XML but is not used for Mugef.
 */

void ParsMeasSim(uint32_t channel);

/*!
 * Set reference function run time parameters (not used).
 *
 * This function is required by the XML but is not used for Mugef.
 */

void ParsRunTime(uint32_t channel);

/*!
 * Set reference abort time parameters (not used)
 *
 * This function is required by the XML but is not used for Mugef.
 */

void ParsAbortTime(uint32_t channel);

// EOF
