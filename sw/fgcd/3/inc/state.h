/*!
 * @file   state.h
 * @brief  Declarations for managing power converter states
 * @author Stephen Page
 */

#ifndef STATE_H
#define STATE_H

#include <stdint.h>

#include <mugef/pc_families/1.h>
#include <mugef/pc_families/4.h>
#include <mugef/pc_families/6.h>
#include <mugefhw/mpc.h>
#include <mugefhw/vmodttl.h>

/*!
 * Supported commands
 */

enum State_cmd
{
    STATE_CMD_LOAD_SWITCH1,
    STATE_CMD_LOAD_SWITCH2,
    STATE_CMD_POLARITY,
    STATE_CMD_RESET,
    STATE_CMD_START,
    STATE_CMD_STOP,
    STATE_CMD_NUM_CMDS
};

struct Status_to_fault
{
    const uint16_t  status_mask;
    const uint16_t  inverted;
    const uint16_t  vs_fault;
    const uint16_t  fault_mask;
};

struct Status_to_string
{
    const uint16_t  mask;
    const char      *str;
};

/*!
 * Struct containing global variables
 */

struct State
{
    struct mpc_card         mpc;                                //!< MPC card
    struct vmodttl_card     vmod;                               //!< VMOD TTL card

    uint64_t                cmd_request[STATE_CMD_NUM_CMDS];    //!< Masks of channels for command requests
    pthread_mutex_t         cmd_request_mutex;                  //!< Mutex to protect command requests
    uint16_t                command[1 + MUGEF_MAX_CHANNELS];    //!< Commands being processed

    uint16_t                status[1 + MUGEF_MAX_CHANNELS];     //!< Power converter status
    uint32_t                latch[1 + MUGEF_MAX_CHANNELS];      //!< Latch status flag

    /*!
     * Pointers to MPC registers for property access
     */

    struct State_prop
    {
        volatile const uint16_t *version_firmware;
        volatile const uint16_t *version_pld;
        volatile       uint32_t *time;
        volatile       uint16_t *link;
        volatile       uint16_t *cmd;
        volatile const uint16_t *cmd_state;
        volatile const uint16_t *status;
    } prop;
};

/*!
 * Global variables
 */

extern struct State state;

// External function declarations

/*!
 * Initialise state management
 */

int32_t  stateInit(void);

/*!
 * Process state commands
 */

void stateProcessCmds(void);

/*!
 * Request movement of the load switch 1
 */

uint32_t stateRequestLoadSwitch1(uint16_t channel, int mode);

/*!
 * Request movement of the load switch 2 (aka polarity switch)
 */

uint32_t stateRequestLoadSwitch2(uint16_t channel, int polarity);

/*!
 * Request movement of polarity switch
 */

uint32_t stateRequestPolarity(uint16_t channel, int polarity);

/*!
 * Start power converter
 */

uint32_t stateRequestStart(uint16_t channel);

/*!
 * Stop power converter
 */

uint32_t stateRequestStop(uint16_t channel);

/*!
 * Reset power converter faults
 */

uint32_t stateRequestReset(uint16_t channel);

/*!
 * Survey power converter states
 */

void stateSurvey(uint32_t start_channel, uint32_t end_channel);

/*!
 * Set power converter state
 */

void stateSetPC(uint16_t channel, uint8_t state, const char *state_name);

/*!
 * Set voltage source state
 */

void stateSetVS(uint16_t channel, uint8_t state, const char *state_name);

#endif

// EOF
