/*!
 * @file   consts.h
 * @brief  Global constants
 * @author Stephen Page
 */

#ifndef CONSTS_H
#define CONSTS_H

#include <fgc_consts_gen.h>
#include <consts_base.h>
#include <mugef/consts.h>

#define FGCD_CYCLE_PERIOD_MS            20                                                //!< FGCD cycle period in milliseconds
#define FGCD_CYCLES_PER_SEC             (1000 / FGCD_CYCLE_PERIOD_MS)                     //!< FGCD cycles per second
#define FGCD_MAX_EQP_DEVS               MUGEF_MAX_CHANNELS                                //!< Maximum number of equipment devices (not including fgcddev)
#define FGCD_MAX_DEVS                   (MUGEF_MAX_CHANNELS+1)                            //!< Total maximum number of devices (including fgcddev)
#define FGCD_MAX_SUB_DEVS_PER_DEV       0                                                 //!< Maximum number of sub-devices per device
#define FGCD_MAX_SUB_DEVS             ((1 + FGCD_MAX_SUB_DEVS_PER_DEV) * FGCD_MAX_DEVS)   //!< Maximum number of sub-devices
#define CMD_MAX_VAL_LENGTH             1800000                                            //!< https://wikis.cern.ch/display/TEEPCCCS/MAX_VAL_LEN

#endif

// EOF
