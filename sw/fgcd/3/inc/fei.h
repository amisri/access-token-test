/*!
 * @file   fei.h
 * @brief  Declarations for fast extraction interlocks
 * @author Stephen Page
 */

#ifndef FEI_H
#define FEI_H

#include <stdint.h>

#include <mugefhw/fei.h>

/*!
 * Struct containing global variables
 */

struct Fei
{
    struct fei_card card;   //!< FEI card

    // Pointers to FEI registers for property access

    struct
    {
        volatile const  uint16_t *version;
        volatile        uint16_t *beam_dump;
        volatile        uint16_t *fei;
    } prop;
};

extern struct Fei fei;

// External functions

/*!
 * Initialise FEI
 */

int32_t feiInit(void);

/*!
 * Perform FEI check
 */

void feiCheck(void);

#endif

// EOF
