/*!
 * @file   equipdev.h
 * @brief  Declarations for equipment devices
 * @author Stephen Page
 */

#pragma once

#include <libfg1.h>
#include <libfg/plep.h>
#include <libfg/table.h>
#include <libfg/test.h>
#include <libfg/trim.h>
//#include <libreg1.h>
#include <stdint.h>
#include <sys/time.h>

// Constants

#define EQUIPDEV_CLASS_ID       91      //!< FGC class ID for equipment devices
#define EQUIPDEV_MAX_PROPS      150     //!< Maximum number of properties supported by an equipment device

#include <classes/91/defconst.h>
#include <equip_common.h>
#include <consts.h>
#include <devname.h>
#include <fgc_errs.h>
#include <fgc_stat.h>
#include <funcgen.h>
#include <hash.h>
#include <meas.h>
#include <mugefhw/fei.h>
#include <timing.h>

// Forward declarations

struct Parser;
struct prop;

// Duration (ms) of the timing pulse between Mugef and the RBI.81607 MCCF thyristor switch control electonics

#define RBI81607_PULSE_MS 10

// CTR output 2 to trigger an LHC pulse in RBI.81607

#define RBI81607_OUT_LHC  2

// CTR output 3 to trigger an AWAKE pulse in RBI.81607

#define RBI81607_OUT_AWAKE 3

// Types

/*!
 * Function generator reference configuration
 */

struct Equipdev_fg_ref
{
    union funcgen_pars      pars;           //!< Function generation parameters
    struct fg_meta          meta_data;      //!< Meta data for armed function
    uint32_t                ref_type;       //!< Reference type
    uint32_t                reg_mode;       //!< Regulation mode
    float                   run_delay;      //!< Time to delay before start of function
    struct fg_table_config  table;          //!< Configuration of table functions
    struct Funcgen_table    table_arrays;   //!< Configuration table reference and time arrays
    struct fg_trim_config   trim;           //!< Configuration of trim function
};

struct Equipdev_channel_load_fg
{
    struct Equipdev_channel_load_fg_defaults            //!< Defaults
    {
        struct Equipdev_channel_load_fg_defaults_i      //!< Current defaults
        {
            float   acceleration;                       //!< Default acceleration
            float   linear_rate;                        //!< Default rate of change
        } i;
    } defaults;
};

/*!
 * Load Limits
 */

struct Equipdev_channel_load_limits                     //!< Limits
{
    struct fg_limits    i;                              //!< Current limits
    float               i_err_fault;                    //!< Current error fault limit
    float               i_err_warning;                  //!< Current error warning limit
};

/*!
 * Load configuration
 */

struct Equipdev_channel_load
{
    float           henrys;                                 //!< Inductance

    float           henrys_sat;                             //!< Saturated circuit inductance
    float           i_sat_start;                            //!< Current for start of magnet saturation
    float           i_sat_end;                              //!< Current for end of magnet saturation

    float           ohms_mag;                               //!< Magnetic resistance
    float           ohms_par;                               //!< Parallel resistance
    float           ohms_ser;                               //!< Serial resistance

    // Function generation

    struct Equipdev_channel_load_fg fg;                        //!< Load-specific function generation parameters

    // Limits

    struct Equipdev_channel_load_limits  limits;            //!< Limits
};

/*!
 * Fast Extraction Interlocks (FEI)
 */

struct Equipdev_channel_ppm_fei
{
    float       i_meas[FGC_FEI_MAX_OCCURRENCES];        //!< Measured current for each FEI occurrence
    float       i_ref;                                  //!< Reference current
    float       i_tol;                                  //!< Tolerance relative to reference current
    uint32_t    output;                                 //!< FEI output number (0 = no output, 1-4 select output)
    uint32_t    result[FGC_FEI_MAX_OCCURRENCES];        //!< Flag indicating whether FEI check was successful for each occurrence
};

/*!
 * State of automatic polarity switching
 */

enum pol_state_t
{
    POL_STATE_MOVING,
    POL_STATE_TO_MIN,
};

/*!
 * Data for an equipment device channel
 */

struct Equipdev_channel
{
    struct FGCD_device *fgcd_device;                            //!< Pointer to FGCD device structure
    struct fgc91_stat   status;                                 //!< Published status
    struct timeval      time_start;                             //!< Time that the FGCD was started
    uint16_t            family;                                 //!< Family of power converter
    uint16_t            mode_op;                                //!< Operational mode requested by client
    uint8_t             mode_pc;                                //!< Power converter mode requested by client
    uint32_t            mode_rt;                                //!< Real-time mode
    float               start_timeout;                          //!< Time-out for power converter to respond to start command
    float               start_timeout_remaining;                //!< Countdown for start time-out
    int32_t             load_select;                            //!< Index of selected load
    uint32_t            load_switch1_state;                     //!< Mode switch state
    uint32_t            load_switch1_request;                   //!< Mode switch requested state
    uint32_t            load_switch1_timeout;                   //!< Mode switch time-out (in seconds)
    uint32_t            load_switch1_timeout_remaining;         //!< Countdown for mode switch time-out (in milliseconds)
    uint32_t            load_switch2_state;                     //!< Polarity switch (used as a load switch) state
    uint32_t            load_switch2_request;                   //!< Polarity switch (used as a load switch) requested state
    uint32_t            load_switch2_timeout;                   //!< Polarity switch (used as a load switch) time-out (in seconds)
    uint32_t            load_switch2_timeout_remaining;         //!< Polarity switch (used as a load switch) countdown time-out (in milliseconds)
    uint32_t            pol_switch_state;                       //!< Polarity switch state
    uint32_t            pol_switch_request;                     //!< Polarity switch requested state
    uint32_t            pol_switch_timeout;                     //!< Polarity switch time-out (in seconds)
    uint32_t            pol_switch_timeout_remaining;           //!< Countdown for polarity switch time-out (in milliseconds)
    uint32_t            pol_switch_auto;                        //!< Automatic polarity switch flag
    uint32_t            beam_dump;                              //!< Flag to indicate whether a beam dump will be requested when STATE.PC is not an operational state
    uint32_t            coast;                                  //!< Flag to indicate whether the device will respond to coast and recover timing events
    struct {float x,y;} test_point[64];
    int32_t             autotune_time;                          //!< Time reference of the automatic tune change reference function
    uint32_t            rbi81607_cycling;                       //!< RBI.81607 is enabled for the current cycle
    uint32_t            rbi81607_enable;                        //!< RBI.81607 automatic thyristor switch firing enable/disable
    uint32_t            rbi81607_force;                         //!< RBI.81607 forced destination when the dynamic destination is the SPS DUMP
    uint32_t            rbi81607_timeout_remaining;             //!< RBI.81607 pulse countdone time-out

    bool                arming_enabled;                         //!< (not mapped to a property) Arming is disabled until all config is loaded from nonvol

    // References

    struct Equipdev_channel_ref                                 //!< References
    {
        float           fg;                                     //!< Function generator output reference
        float           rt;                                     //!< Real-time input reference

        float           i;                                      //!< Current reference

        float           reg;                                    //!< Regulation reference
        float           reg_rate;                               //!< Rate of change of regulation reference

        float           output;                                 //!< Reference sent to output
    } ref;

    struct Equipdev_channel_ref ref_buffer[MEAS_MAX_DELAY_MS];  //!< Buffered reference to handle delay taking measurements

    uint32_t            dcct_select;                            //!< DCCT selection
    struct Meas_chan    meas;                                   //!< Measurements
    uint32_t            meas_bad_values;                        //!< Count of the number of bad measurement values read for the device

    // Reference output

    struct Equipdev_channel_ref_output
    {
        struct fg_limits *limits;                               //!< Pointer to limits currently in use
    } ref_output;

    // Function generation

    struct Equipdev_channel_fg                                  //!< Non-PPM function generation data
    {
        float           ref_remaining;                          //!< Remaining reference time in seconds
        double          time;                                   //!< Time within reference function in seconds
        struct timeval  ref_run;                                //!< Absolute time at which to start a reference
        uint16_t        ref_event_group;                        //!< Event group to trigger the start of a reference

        struct Equipdev_fg_ref  running_cyc;                    //!< Data for cycling running functions
        struct Equipdev_fg_ref  running_non_cyc;                //!< Data for non-cycling running functions

        pol_state_t pol_state;                                  //!< Polarity state

    } fg;

    // Economy

    struct Equipdev_channel_economy
    {
        double          dyn_time_offset;                        //!< Dynamic economy function time offset from the cycle time
        uint32_t        state;                                  //!< Flag to indicate whether the device will react to economy triggers

        struct Equipdev_channel_economy_dest
        {
            uint32_t    mask;                                   //!< Mask of destinations for which the device should pulse
            uint32_t    state;                                  //!< Flag to indicate whether the device will react to destination economy triggers
        } dest;
    } economy;

    // Load configuration

    struct Equipdev_channel_load  load[FGC_N_LOADS];            //!< Load configuration

    // Values logged every millisecond should be within the following structure

    struct Equipdev_channel_log                                 //!< Logged data
    {
        float           i_ref;                                  //!< Logged current reference
        float           i_meas;                                 //!< Logged measured current value from power converter
        float           ref_meas;                               //!< Logged measured reference output
    } log[FGC_LOG_LEN];

    // Test properties

    struct Equipdev_channel_test test;                          //!< Standard TEST property values

    // PPM variables must be within the following structure

    struct Equipdev_channel_ppm                                 //!< PPM variables
    {
        uintptr_t       prop_num_els[EQUIPDEV_MAX_PROPS];       //!< Numbers of elements for device properties

        uint16_t        transaction_id;                         //!< Transaction ID for transactional settings
        uint32_t        transaction_state;                      //!< State of a transaction
        uint32_t        transaction_ref_type;                   //!< Reference type for a transaction, set automatically based upon the last reference property set (FGC_REF_NONE until known)

        // Transactional settings

        struct Equipdev_ppm_transactional
        {
            struct Equipdev_fg_ref fg_config;                   //!< Function generation configuration for property access
            struct fg_plep_config  plep;                        //!< Configuration of PLEP function
            struct fg_test_config  test;                        //!< Configuration of test function
        } transactional, transactional_backup;                  //!< PPM transactional settings and a backup to allow rollback

        // Function generation

        struct Equipdev_ppm_fg                                  //!< PPM function generation
        {
            uint32_t    play;                                   //!< Flag indicating whether the function for the user should be played

            struct Equipdev_fg_ref armed;                       //!< Armed function data
        } fg;                                                   //!< Function generation

        uint32_t        fg_play;                                //!< Flag indicating whether the function for the user should be played

        float           max_abs_err;                            //!< Maximum absolute error between reference and measurement

        // Fast Extraction Interlocks (FEI)

        struct Equipdev_channel_ppm_fei fei;                    //!< Fast Extraction Interlocks (FEI)

        // Economy

        struct Equipdev_ppm_economy
        {
            float   dyn_end_time;                               //!< Cycle time at which the dynamic economy function must rejoin the operational function
        } economy;

        // SPS Mains automatic tune change

        struct Equipdev_channel_ppm_autotune
        {
            uint32_t enable;                                    //!< Enable/disable the automatic tune change real-time reference
            float    steps[FGC_AUTOTUNE_MAX_INJ];               //!< Delta current per injection of the automatic tune setting
        } autotune;

    } ppm[NUM_CYC_SELECTORS];
};

/*!
 * Data for LOG.MENU.NAMES property
 */

extern const char *equipdev_log_menu_names[];

/*!
 * Data for LOG.MENU.PROPS property
 */

extern const char *equipdev_log_menu_props[];

/*!
 * Equipdev FEI output per user
 */

struct Equipdev_ppm_fei_output
{
    uint32_t    active;                             //!< Flag indicating whether output is active for the user
    uint16_t    pulse_width_100us;                  //!< Width of the output pulse in steps of 100us
    uint32_t    result[FGC_FEI_MAX_OCCURRENCES];    //!< Flag indicating whether FEI check was successful for each occurrence
};

/*!
 * Struct containing global variables
 */

struct Equipdev
{
    uint64_t                beam_dump_request;                  //!< Bit mask of beam dump requests
    struct hash_table       *const_hash;                        //!< Hash of constants
    struct hash_table       *prop_hash;                         //!< Hash of properties
    struct Equipdev_channel device[FGCD_MAX_DEVS];              //!< Data for equipment devices
    const char              *fgc_class_name;                    //!< Class name
    const char              *fgc_platform_name;                 //!< Platform name
    uint8_t                 fgc_platform;                       //!< Platform number
    uint32_t                ref_buf_idx;                        //!< Index within ref buffer to handle delay taking measurements
    pthread_mutex_t         armed_fg_pars_mutex;                //!< Mutex to protect the armed function parameters

    struct Equipdev_oasis                                       //!< OASIS variables
    {
        uint32_t            i_meas_interval_ns;                 //!< I_MEAS sampling interval (period) in nanoseconds
        uint32_t            i_ref_interval_ns;                  //!< I_REF sampling interval (period) in nanoseconds
    } oasis;

    struct Equipdev_ppm                                         //!< PPM variables
    {
        struct Equipdev_ppm_log
        {
            uint32_t        index;                              //!< Indices within log (element 0 is the index of the latest sample)
            uintptr_t       num_elements;                       //!< Numbers of elements in the log
            struct timeval  timestamp;                          //!< Timestamp of acquired cycle within log (note that this may be offset from the actual timing cycle, e.g. in TT10 for SPS injection)
            struct timeval  cycle_timestamp;                    //!< Timestamp of real timing cycle within log which will become the cycleStamp
        } log;

        struct Equipdev_ppm_fei
        {
            uintptr_t       num_occurrences;                    //!< Number of FEI events that have occurred

            struct Equipdev_ppm_fei_output output[FEI_NUM_FEI_OUTPUTS];

        } fei;
    } ppm[NUM_CYC_SELECTORS];
};

extern struct Equipdev equipdev;

// Static functions

//! Called from nonvol.cpp if a config property is changed.

static inline void equipdevConfigUnsync(uint32_t channel)
{
}


// External functions

/*!
 * Initialise equipment devices
 */

int32_t equipdevStart(void);

/*!
 * Clean-up equipment devices
 */

void equipdevCleanUp(void);

/*!
 * Initialise data for devices.
 *
 * This function is called each time the device names are read.
 */

void equipdevInitDevices(void);

/*!
 * Publish data for equipment devices
 */

void equipdevPublish(void);

/*!
 * Apply autotune
 */

void equipdevRtAutotune(bool log_events_flag);

/*!
 * Check limits
 */

void equipdevCheckLimits(void);

/*!
 * Initialise an equipment device after it has been configured.
 *
 * This function should be called for a device when STATE.OP is changed from UNCONFIGURED.
 */

void equipdevConfigured(struct Equipdev_channel *device);

/*!
 * Update logged data
 */

void equipdevUpdateLog(void);

/*!
 * Arm the reference for a user
 */

fgc_errno equipdevArmCycle(uint32_t channel, uint32_t cycle_sel, bool test_only);

/*!
 * Recover reference settings
 */

void equipdevRecoverRefSettings(struct Equipdev_channel *device, uint32_t user);

//! Callback from parser to indicate that a transactional property was set

void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property);

//! Callback to notify transactional properties for publication

void equipdevTransactionNotify(uint32_t channel, uint32_t user);

//! Apply transactional settings for a user on a device

fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time);

//! Rollback transactional settings for a user on a device

void equipdevTransactionRollback(uint32_t channel, uint32_t user);

//! Test transactional settings for a user on a device

fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user);

// EOF
