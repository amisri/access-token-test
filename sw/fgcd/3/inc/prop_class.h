/*!
 * @file   prop_class.h
 * @brief  Declarations for class-specific properties.
 * @author Stephen Page
 */

#ifndef PROP_CLASS_H
#define PROP_CLASS_H

// Forward declarations

struct prop;
struct Cmd;

// External functions

/*!
 * Check whether a reset is currently permitted.
 *
 * @retval 0    Reset is not permitted (A power converter is not off)
 * @retval 1    Reset permitted (All power converters are off)
 */

int32_t SetifResetOk(struct Cmd *command);

/*!
 * Terminate the FGCD.
 *
 * @param[in] command     Pointer to the SET command which initiated the terminate function
 * @param[in] property    Pointer to the property which indicates the type of termination
 *
 * @returns If the function is successful, it does not return. It returns 1 on failure, i.e. it was called
 *          with a property it does not handle.
 */

int32_t SetTerminate(struct Cmd *command, struct prop *property);

#endif

// EOF
