/*
 *  Filename: reg.c
 *
 *  Purpose:  Functions for regulation
 *
 *  Author:   Stephen Page
 */

#include <consts.h>
#include <equipdev.h>
#include <fgcd.h>
#include <funcgen.h>
#include <logging.h>
#include <reg.h>



// Global variables

struct Reg reg;



void regRegulate(void)
{
    uint32_t                channel;
    struct Equipdev_channel *equip_device;
    float                   reg_ref_prev;   // Regulation reference for previous millisecond

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        equip_device = &equipdev.device[channel];

        // Skip channel if it is unused

        if(!equip_device->fgcd_device->name) continue;

        // Store previous regulation reference

        reg_ref_prev = equip_device->ref.reg;

        // Set regulation reference

        switch(equip_device->status.state_pc)
        {
            // States where real-time reference should be applied

            case FGC_PC_ARMED:
            case FGC_PC_CYCLING:
            case FGC_PC_IDLE:
            case FGC_PC_RUNNING:
            case FGC_PC_TO_CYCLING:
                equip_device->ref.reg = equip_device->ref.fg + equip_device->ref.rt;
                break;

            // Real-time reference should not be applied

            default:
                equip_device->ref.reg = equip_device->ref.fg;
                break;
        }

        // RBI.81607 should not set the reference when the conditions are not met

        if (equip_device->rbi81607_enable  == FGC_CTRL_ENABLED &&
            equip_device->rbi81607_cycling == FGC_CTRL_DISABLED)
        {
            equip_device->ref.reg = equip_device->ref_output.limits->min;
        }

        // Set current reference to regulation reference

        equip_device->ref.i = equip_device->ref.reg;

        // Calculate rate of change of regulation reference

        equip_device->ref.reg_rate = (equip_device->ref.reg - reg_ref_prev) * FUNCGEN_FREQ;

        // Set reference parameters to type accepted by voltage source

        equip_device->ref_output.limits = &equip_device->load[equip_device->load_select].limits.i;
        equip_device->ref.output        = equip_device->ref.i;

        // Copy ref to buffer

        equip_device->ref_buffer[equipdev.ref_buf_idx] = equip_device->ref;
    }
    equipdev.ref_buf_idx = (equipdev.ref_buf_idx + 1) % meas.delay_ms;
}

// EOF
