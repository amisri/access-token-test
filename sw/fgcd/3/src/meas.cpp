/*
 *  Filename: meas.c
 *
 *  Purpose:  Measurement
 *
 *  Author:   Stephen Page
 */

#include <arpa/inet.h>

#include <equipdev.h>
#include <fgcd.h>
#include <logging.h>
#include <meas.h>
#include <mugef/consts.h>
#include <mugefhw/mpv915-21.h>
#include <timing.h>



// Global variables

struct Meas meas;



// Static functions

static int32_t measInitMpv915(void);
static void measAcquireMpv915(void);



int32_t measInit(void)
{
    // Initialise MPV915 cards

    if(measInitMpv915())
    {
        return 1;
    }

    return 0;
}



/*
 * Initialise measurement using MPV915 cards
 */

static int32_t measInitMpv915(void)
{
    uint32_t i;

    // Map MPV915 cards

    for(i = 0 ; i < MPV915_NUM_ADDRS ; i++)
    {
        // Attempt to map card

        if(mpv915Map(&meas.mpv915[i], i))
        {
            continue;
        }
    }

    // Set measurement delay

    meas.delay_ms = MPV915_MEAS_DELAY_MS;

    return 0;
}

void measAcquire(void)
{
    struct Equipdev_channel *device;
    uint32_t                i;

    // Perform acquisition

    measAcquireMpv915();

    // Set I_MEAS

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        device = &equipdev.device[i];

        // Check whether measurements are being simulated for the device

        if(device->status.state_op == FGC_OP_SIMULATION && device->meas.sim)
        {
            // Set the measurements to match the reference

            device->meas.ref_meas   = device->ref_buffer[equipdev.ref_buf_idx].i;
            device->meas.i_a        = device->ref_buffer[equipdev.ref_buf_idx].i;
            device->meas.i_b        = device->ref_buffer[equipdev.ref_buf_idx].i;
        }
        else // Use real measurements
        {
            // Invert measurements if device has a polarity switch in the negative position

            if(device->pol_switch_state == FGC_POL_NEGATIVE)
            {
                device->meas.ref_meas   = -device->meas.ref_meas;
                device->meas.i_a        = -device->meas.i_a;
                device->meas.i_b        = -device->meas.i_b;
            }
        }

        switch(device->dcct_select)
        {
            case FGC_DCCT_SELECT_A:     // Use measurement channel A
                device->meas.i_meas = device->meas.i_a;
                break;

            case FGC_DCCT_SELECT_B:     // Use measurement channel B
                device->meas.i_meas = device->meas.i_b;
                break;

            case FGC_DCCT_SELECT_AB:    // Use average of measurement channels A and B
                device->meas.i_meas = (device->meas.i_a + device->meas.i_b) / 2;
                break;

            default:
                logPrintf(i, "MEAS Unexpected value of dcct_select %u\n", device->dcct_select);
                break;
        }
    }
}



/*
 * Acquire measurement using MPV915 cards
 */

static void measAcquireMpv915(void)
{
    struct mpv915_card      *card;
    uint32_t                card_num;
    uint32_t                channel;
    struct Equipdev_channel *device;
    uint32_t                device_id;
    uint16_t                mpv_page[MPV915_NUM_ADDRS];
    uint16_t                mpv_status[MPV915_NUM_ADDRS];
    uint16_t                raw_value;
    static uint16_t         raw_value_previous[MPV915_NUM_ADDRS][MPV915_NUM_CHANS];
    uint16_t                sample_index;
    float                   scale_factor;
    struct timeval          time;
    float                   value;

    // Read card statuses

    for(card_num = 0 ; card_num < MPV915_NUM_ADDRS ; card_num++)
    {
        card = &meas.mpv915[card_num];

        // Do nothing if card not mapped

        if(!card->regs) continue;

        // Read acquisition status

        mpv_status[card_num]    = ntohs(card->regs->status);
        mpv_page[card_num]      = mpv_status[card_num] & MPV915_STATUS_PAGE_MASK;
    }

    // The sample index given by the MPV915 is missing the least significant bit,
    // so the UTC millisecond is used to calculate the index

    // Note that this relies upon the cards' SSC inputs being connected to a UTC-aligned PPS

    timingGetUserTime(0, &time);
    sample_index = (((time.tv_usec / 1000) + 1000) - 1) % 1000;

    // Read measurement values
    // Note that values read from MPV915 cards are 2ms in the past
    // (see section 2.4.3 of the manual)

    for(channel = 0 ; channel < MPV915_NUM_CHANS ; channel++)
    {
        // Select pages on cards

        for(card_num = 0 ; card_num < MPV915_NUM_ADDRS ; card_num++)
        {
            card = &meas.mpv915[card_num];

            // Do nothing if card not mapped

            if(!card->regs) continue;

            device_id   = 1 + (((card_num * MPV915_NUM_CHANS) + channel) / 2);
            device      = &equipdev.device[device_id];

            // Skip channel if it is unused

            if(!device->fgcd_device->name) continue;

            // Select page on card

            card->regs->window = htons(channel | (mpv_page[card_num] << MPV915_WINDOW_PAGE_SHIFT));
        }

        // Delay to allow page selections to take effect

        mpv915DelayNs(MPV915_DELAY_REG_WRITE_NS);

        // Read channels

        for(card_num = 0 ; card_num < MPV915_NUM_ADDRS ; card_num++)
        {
            card = &meas.mpv915[card_num];

            // Do nothing if card not mapped

            if(!card->regs) continue;

            device_id   = 1 + (((card_num * MPV915_NUM_CHANS) + channel) / 2);
            device      = &equipdev.device[device_id];

            // Skip channel if it is unused

            if(!device->fgcd_device->name) continue;

            // Calculate scale factor for device

            scale_factor = -device->load[device->load_select].limits.i.pos /(-32767);

            // Read value

            raw_value = ntohs(card->ram[sample_index]);

            // Invalid values of 0xFFFF are sometimes reported by the MPV915
            // Keep the previous value in that case unless it was also 0xFFFF

            if(raw_value == 0xFFFF && raw_value_previous[card_num][channel] != 0xFFFF)
            {
                // Increment the counts of bad values for the card and the device

                meas.mpv915_bad_values[card_num]++;
                device->meas_bad_values++;

                // Keep the previous value

                raw_value = raw_value_previous[card_num][channel];
            }
            raw_value_previous[card_num][channel] = raw_value;

            // Scale value

            value = (raw_value - MPV915_CAL_ZERO) * scale_factor;

            // Assign value to device

            if(channel & 1) // Odd channels are I_A
            {
                device->meas.i_a = value;
            }
            else // Even channels are REF_MEAS
            {
                device->meas.ref_meas = value;
            }
        }
    }
}

// EOF
