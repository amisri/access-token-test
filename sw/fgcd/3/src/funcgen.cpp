/*
 *  Filename: funcgen.c
 *
 *  Purpose:  Function generation
 *
 *  Author:   Stephen Page
 */

#include <classes/91/defconst.h>
#include <equipdev.h>
#include <fgcd.h>
#include <libfg1.h>
#include <libfg/plep.h>
#include <libfg/table.h>
#include <libfg/test.h>
#include <libfg/trim.h>
#include <logging.h>
#include <prop_equip.h>
#include <reg.h>
#include <state.h>
#include <timing.h>



// Global variables

struct Funcgen funcgen;

typedef  uint32_t (*funcgen_t)(union funcgen_pars*, const double*, float*);

funcgen_t funcgen_gen_func[] = {
    NULL,                                        // NONE
    NULL,                                        // STARTING
    NULL,                                        // STOPPING
    NULL,                                        // TO_STANDBY
    NULL,                                        // ARMED
    NULL,                                        // ABORTING
    reinterpret_cast<funcgen_t>(fgPlepGen),      // NOW
    reinterpret_cast<funcgen_t>(fgTestGen),      // STEPS
    reinterpret_cast<funcgen_t>(fgTestGen),      // SQUARE
    reinterpret_cast<funcgen_t>(fgTestGen),      // SINE
    reinterpret_cast<funcgen_t>(fgTestGen),      // COSINE
    NULL,                                        // PLP
    reinterpret_cast<funcgen_t>(fgPlepGen),      // PLEP
    NULL,                                        // PPPL
    reinterpret_cast<funcgen_t>(fgTrimGen),      // LTRIM
    reinterpret_cast<funcgen_t>(fgTrimGen),      // CTRIM
    reinterpret_cast<funcgen_t>(fgTableGen)      // TABLE
};



// Static functions

static void funcgenCalcRefArmed(uint32_t channel);
static void funcgenCalcRefPolSwitching(uint32_t channel);
static void funcgenCalcRefCycling(uint32_t channel);
static void funcgenCalcRefEconomy(uint32_t channel);
static void funcgenCalcRefRunning(uint32_t channel);
static void funcgenCalcRefSlowAbort(uint32_t channel);
static void funcgenCalcRefStarting(uint32_t channel);
static void funcgenCalcRefToCycling(uint32_t channel);
static void funcgenCalcRefToStandby(uint32_t channel);
static void funcgenAutoPolarity(uint32_t channel, struct fg_meta *function_meta);



int32_t funcgenInit(void)
{
    return 0;
}



void funcgenCalcRefs(void)
{
    struct Equipdev_channel *device;
    uint32_t                i;
    uint32_t                state_pc;

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        device      = &equipdev.device[i];
        state_pc    = device->status.state_pc;

        // Skip device if not configured

        if(!device->fgcd_device->name) continue;

        // Clear TO_CYCLING PLEP running flag if state is not TO_CYCLING

        if(state_pc != FGC_PC_TO_CYCLING)
        {
            funcgen.to_cycling_plep_running[i] = 0;
        }

        // Generate reference for next millisecond

        switch(state_pc)
        {
            case FGC_PC_FLT_OFF:
            case FGC_PC_OFF:
                device->ppm[0].fg.armed.ref_type    = FGC_REF_NONE;
                device->ref.fg                      = 0;
                break;

            case FGC_PC_ARMED:          funcgenCalcRefArmed(i);         break;
            case FGC_PC_CYCLING:        funcgenCalcRefCycling(i);       break;
            case FGC_PC_ECONOMY:        funcgenCalcRefEconomy(i);       break;
            case FGC_PC_POL_SWITCHING:  funcgenCalcRefPolSwitching(i);  break;
            case FGC_PC_RUNNING:        funcgenCalcRefRunning(i);       break;
            case FGC_PC_SLOW_ABORT:     funcgenCalcRefSlowAbort(i);     break;
            case FGC_PC_STARTING:       funcgenCalcRefStarting(i);      break;
            case FGC_PC_TO_CYCLING:     funcgenCalcRefToCycling(i);     break;
            case FGC_PC_TO_STANDBY:     funcgenCalcRefToStandby(i);     break;
        }

        // Store state

        funcgen.previous_state_pc[i] = state_pc;
    }
}



/*
 * Calculate a reference point for a device in the ARMED state
 */

static void funcgenCalcRefArmed(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];
    struct timeval          time;
    struct timeval          time_diff;

    // Do nothing if the run time has not been set

    if(equipdev.device[channel].fg.ref_run.tv_sec == LONG_MAX)
    {
        return;
    }

    // Compare current time to time set for ref run

    timingGetUserTime(0, &time);
    time_diff = timingCompareTimevals(&time, &equipdev.device[channel].fg.ref_run);

    if(time_diff.tv_sec >= 0) // At or after ref_run
    {
        // Set the function generator reference to the current reference

        device->ref.fg = device->ref.i;

        // Start reference

        device->fg.time = 0;
        stateSetPC(channel, FGC_PC_RUNNING, "RUNNING");

        // Calculate reference for running state

        funcgenCalcRefRunning(channel);
    }
}



/*
 * Calculate a reference point for a device in the POL_SWITCHING state
 */

static void funcgenCalcRefPolSwitching(uint32_t channel)
{
    struct Equipdev_channel  *device = &equipdev.device[channel];
    int                      new_polarity;
    union funcgen_pars       *fg_pars;

    // Check whether entering state

    if(funcgen.previous_state_pc[channel] != FGC_PC_POL_SWITCHING)
    {
        // Set the function generator reference to the current reference

        device->ref.fg = device->ref.i;

        // Calculate PLEP to I_MIN

        funcgenSystemPlep(channel,
                          device->pol_switch_state == FGC_POL_NEGATIVE    ?
                          -device->load[device->load_select].limits.i.min :
                           device->load[device->load_select].limits.i.min,
                          0);

        device->fg.time         = FUNCGEN_PERIOD;
        device->fg.pol_state    = POL_STATE_TO_MIN;
    }

    switch(device->fg.pol_state)
    {
        case POL_STATE_MOVING:

            // Check whether the switch has moved to the requested position

            if(device->pol_switch_state == device->pol_switch_request)
            {
                // Initialise limits

                ParsLimits(channel);

                // Set reference to MIN

                device->ref.fg = device->pol_switch_state == FGC_POL_NEGATIVE ?
                                -device->load[device->load_select].limits.i.min : device->load[device->load_select].limits.i.min;

                // Change to the TO_CYCLING state

                stateSetPC(channel, FGC_PC_TO_CYCLING, "TO_CYCLING");
                funcgenCalcRefToCycling(channel);
            }
            break;

        case POL_STATE_TO_MIN:

            // Generate reference point

            fg_pars = &device->fg.running_non_cyc.pars;

            if(funcgen_gen_func[FGC_REF_PLEP] &&
               funcgen_gen_func[FGC_REF_PLEP]
                    (fg_pars,
                     &device->fg.time,
                     &device->ref.fg))
            {
                device->fg.time += FUNCGEN_PERIOD;
            }
            else // Function has completed
            {
                // Set reference to zero

                device->ref.fg = 0;

                // Switch polarity

                new_polarity = device->pol_switch_state == FGC_POL_POSITIVE ?
                               FGC_POL_NEGATIVE : FGC_POL_POSITIVE;

                if(stateRequestPolarity(channel, new_polarity))
                {
                    // Polarity switch request failed

                    stateSetPC(channel, FGC_PC_FLT_OFF, "FLT_OFF");
                    return;
                }

                device->fg.pol_state = POL_STATE_MOVING;
            }

            break;
    }
}



/*
 * Calculate a reference point for a device in the CYCLING state
 */

static void funcgenCalcRefCycling(uint32_t channel)
{
    struct Equipdev_channel *device            = &equipdev.device[channel];

    // Set pointers to armed and running functions

    struct Equipdev_fg_ref  *armed      = &device->ppm[timing.next_ms_user].fg.armed;
    struct Equipdev_fg_ref  *running    = &device->fg.running_cyc;

    // Copy reference parameters in cycle millisecond 0

    if(timing.next_ms_cycle_ms  == 0)
    {
        if(armed->ref_type != FGC_REF_NONE)
        {
            // Check whether playing of the function for the next user is disabled

            if(!device->ppm[timing.next_ms_user].fg.play) // The next user's function is disabled
            {
                // Change to the TO_CYCLING state in order to meet the start of the next enabled function

                stateSetPC(channel, FGC_PC_TO_CYCLING, "TO_CYCLING");
                funcgenCalcRefToCycling(channel);
            }
            else if(device->economy.state && // Economy is enabled
                    (timing.field_value[TIMING_FIELD_MACHINE_MODE]  == TIMING_MACHINE_MODE_FULLECO ||           // Full economy
                    (device->economy.dest.state && !(device->economy.dest.mask & (1 << (timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] - 1)))))) // Destination economy
            {
                stateSetPC(channel, FGC_PC_ECONOMY, "ECONOMY");
                device->economy.dyn_time_offset = 0;
                return;
            }
            else // The next user's function is enabled
            {
                // Check whether the polarity of armed reference matches the current polarity

                if(device->pol_switch_timeout) // Device has a polarity switch
                {
                    funcgenAutoPolarity(channel, &armed->meta_data);

                    // Return if device is no longer in this state

                    if(device->status.state_pc != FGC_PC_CYCLING)
                    {
                        return;
                    }
                }

                // Copy the armed reference to the running reference

                pthread_mutex_lock(&equipdev.armed_fg_pars_mutex);

                *running                    = *armed;

                // Set table reference array pointers

                if(running->ref_type == FGC_REF_TABLE)
                {
                    running->pars.table.function     = running->table_arrays.function;
                }

                pthread_mutex_unlock(&equipdev.armed_fg_pars_mutex);

                // Set the function generator reference to the current reference

                device->ref.fg = device->ref.i;

                // Set the remaining reference time

                device->fg.ref_remaining = running->meta_data.duration;
            }
        }
        else // No reference is armed
        {
            running->ref_type           = FGC_REF_NONE;
            device->fg.ref_remaining    = 0;
        }
    }

    // Generate reference point

    device->fg.time = timing.next_ms_cycle_ms * FUNCGEN_PERIOD;

    if(funcgen_gen_func[running->ref_type])
    {
        funcgen_gen_func[running->ref_type](&running->pars, &device->fg.time, &device->ref.fg);
    }

    // Reduce remaining reference time

    if(device->fg.ref_remaining > FUNCGEN_PERIOD)
    {
        device->fg.ref_remaining -= FUNCGEN_PERIOD;
    }
    else // Remaining time is less than one function generator iteration
    {
        device->fg.ref_remaining = 0;
    }
}



static void funcgenCalcRefEconomy(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];
    double                  time;

    device->fg.time = timing.next_ms_cycle_ms * FUNCGEN_PERIOD;

    // Handle dynamic and full economy

    if(device->economy.dyn_time_offset) // Dynamic economy
    {
        // Check whether the end of the cycle has been reached

        if(timing.next_ms_cycle_ms == 0)
        {
            // The end of the cycle has been reached before the economy function finished

            // Switch back to CYCLING

            device->economy.dyn_time_offset = 0;
            stateSetPC(channel, FGC_PC_CYCLING, "CYCLING");
            return funcgenCalcRefCycling(channel);
        }

        // Generate reference point

        if(funcgen_gen_func[FGC_REF_PLEP]) // A function exists to handle PLEP references
        {
            time = device->fg.time - device->economy.dyn_time_offset;

            if(!funcgen_gen_func[FGC_REF_PLEP](&device->fg.running_non_cyc.pars,
                                               &time, &device->ref.fg))
            {
                // Economy function has finished, switch back to the CYCLING state

                device->economy.dyn_time_offset = 0;
                stateSetPC(channel, FGC_PC_CYCLING, "CYCLING");
            }
        }

        // Reduce remaining reference time

        if(device->fg.ref_remaining > FUNCGEN_PERIOD)
        {
            device->fg.ref_remaining -= FUNCGEN_PERIOD;
        }
        else // Remaining time is less than one function generator iteration
        {
            device->fg.ref_remaining = 0;
        }
    }
    else // Full economy
    {
        // Check whether the end of the cycle has been reached

        if(timing.next_ms_cycle_ms == 0)
        {
            // Check whether full and destination economy conditions have been cleared

            if(!device->economy.state || // Economy is disabled
               (!(timing.field_value[TIMING_FIELD_MACHINE_MODE] == TIMING_MACHINE_MODE_FULLECO) &&           // Full economy conditions not met
               (!(device->economy.dest.state && !(device->economy.dest.mask & (1 << (timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] - 1))))))) // Destination economy conditions not met
            {
                // Switch back to CYCLING

                device->economy.dyn_time_offset = 0;
                stateSetPC(channel, FGC_PC_CYCLING, "CYCLING");
                return funcgenCalcRefCycling(channel);
            }
        }
    }
}



/*
 * Calculate a reference point for a device in the RUNNING state
 */

static void funcgenCalcRefRunning(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];

    // Generate reference point

    if(funcgen_gen_func[device->ppm[0].fg.armed.ref_type] &&
       funcgen_gen_func[device->ppm[0].fg.armed.ref_type](&device->ppm[0].fg.armed.pars,
                                                          &device->fg.time,
                                                          &device->ref.fg))
    {
        // Reduce remaining reference time

        if(device->fg.ref_remaining >= FUNCGEN_PERIOD)
        {
            device->fg.ref_remaining -= FUNCGEN_PERIOD;
        }
        else // Remaining time is less than one function generator iteration
        {
            device->fg.ref_remaining = 0;
        }
        device->fg.time += FUNCGEN_PERIOD;
    }
    else // Function has completed
    {
        device->fg.ref_remaining            = 0;
        device->fg.time                     = 0;
        device->ppm[0].fg.armed.ref_type    = FGC_REF_NONE;
        stateSetPC(channel, FGC_PC_IDLE, "IDLE");
    }
}



/*
 * Calculate a reference point for a device in the SLOW_ABORT state
 */

static void funcgenCalcRefSlowAbort(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];
    float linear_rate               = device->load[device->load_select].fg.defaults.i.linear_rate/2;
    float post_delay                = 1;

    // Check whether entering state

    if(funcgen.previous_state_pc[channel] != FGC_PC_SLOW_ABORT)
    {
        // Set the function generator reference to the regulation reference to absorb RT reference

        device->ref.fg = device->ref.reg;

        // Calculate PLEP for SlowAbort

        funcgenSlowAbortPlep(channel, linear_rate);

        device->fg.time = FUNCGEN_PERIOD;
    }

    // Generate reference point

    if(funcgen_gen_func[FGC_REF_PLEP] &&
       funcgen_gen_func[FGC_REF_PLEP]
            (&device->fg.running_non_cyc.pars,
             &device->fg.time,
             &device->ref.fg))
    {
        device->fg.time += FUNCGEN_PERIOD;
    }
    else if(device->fg.time < (device->fg.running_non_cyc.meta_data.duration + post_delay))   // Post function delay
    {
        device->fg.time += FUNCGEN_PERIOD;
    }
    else // Function has completed
    {
        // Stop the voltage source

        stateRequestStop(channel);
    }
}



/*
 * Calculate a reference point for a device in the STARTING state
 */

static void funcgenCalcRefStarting(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];

    //Start to generate the start function just after receiving the ready from the converter (SPS Mains requirment)

    if(device->status.state_vs == FGC_VS_READY)
    {
        if(funcgen_gen_func[FGC_REF_CTRIM] &&
           funcgen_gen_func[FGC_REF_CTRIM]
                 (&device->fg.running_non_cyc.pars,
                  &device->fg.time,
                  &device->ref.fg))
         {
             device->fg.time += FUNCGEN_PERIOD;
         }
         else // Function has completed
         {
             // Check whether power converter has started

             if(device->status.state_vs == FGC_VS_READY)
             {
                 // Update power converter state

                 switch(device->mode_pc)
                 {
                     case FGC_PC_CYCLING:    stateSetPC(channel, FGC_PC_TO_CYCLING,  "TO_CYCLING");  break;
                     case FGC_PC_IDLE:       stateSetPC(channel, FGC_PC_IDLE,        "IDLE");        break;
                     default:                stateSetPC(channel, FGC_PC_ON_STANDBY,  "ON_STANDBY");  break;
                 }
             }
         }

    }

    // Handle start time-out

    if(device->start_timeout_remaining >= 0)
    {
        device->start_timeout_remaining -= FUNCGEN_PERIOD;
    }
    else // Timed-out
    {
        device->status.st_faults |= FGC_FLT_VS_RUN_TO;
        stateSetPC(channel, FGC_PC_FLT_OFF, "FLT_OFF");
        stateRequestStop(channel);
    }
}



/*
 * Calculate a reference point for a device in the TO_CYCLING state
 */

static void funcgenCalcRefToCycling(uint32_t channel)
{
    struct Equipdev_channel *device             = &equipdev.device[channel];
    struct Equipdev_fg_ref  *next_user_armed    = &device->ppm[timing.next_user].fg.armed;

    // Check whether a TO_CYCLING function is already running

    if(funcgen.to_cycling_plep_running[channel]) // A function is running
    {
        // Check whether next user is known and has a function

        if(timing.next_user &&
           device->ppm[timing.next_user].fg.play &&
           next_user_armed->ref_type != FGC_REF_NONE)
        {
            // Check whether current function is destined for start of function for next user

            if(device->fg.running_non_cyc.meta_data.range.end != next_user_armed->meta_data.range.start)
            {
                // Check whether polarity of function for next user matches the current polarity

                if(device->pol_switch_timeout) // Device has a polarity switch
                {
                    funcgenAutoPolarity(channel, &next_user_armed->meta_data);

                    // Return if device is no longer in this state

                    if(device->status.state_pc != FGC_PC_TO_CYCLING)
                    {
                        return;
                    }
                }

                // Set the function generator reference to the current reference

                device->ref.fg = device->ref.i;

                // Ramp to start of function for next user

                funcgenSystemPlep(channel, next_user_armed->meta_data.range.start, 0);

                device->fg.time = FUNCGEN_PERIOD;
            }
        }
    }
    else // No TO_CYCLING function is running
    {
        // Check whether next user is known and has a function

        if(timing.next_user &&
           device->ppm[timing.next_user].fg.play &&
           next_user_armed->ref_type != FGC_REF_NONE)
        {
            // Check whether reference matches start of function for next user

            if(device->ref.fg == next_user_armed->meta_data.range.start)
            {
                // Check whether next millisecond is the start of the next cycle

                if(timing.next_ms_cycle_ms == 0)
                {
                    // Enter cycling

                    stateSetPC(channel, FGC_PC_CYCLING, "CYCLING");
                    funcgenCalcRefCycling(channel);
                }
            }
            else // Reference does not match start of function for next user
            {
                // Check whether polarity of function for next user matches the current polarity

                if(device->pol_switch_timeout) // Device has a polarity switch
                {
                    funcgenAutoPolarity(channel, &next_user_armed->meta_data);

                    // Return if device is no longer in this state

                    if(device->status.state_pc != FGC_PC_TO_CYCLING)
                    {
                        return;
                    }
                }

                // Set the function generator reference to the current reference

                device->ref.fg = device->ref.i;

                // Ramp to start of function for next user

                funcgenSystemPlep(channel, next_user_armed->meta_data.range.start, 0);

                device->fg.time = FUNCGEN_PERIOD;
                funcgen.to_cycling_plep_running[channel] = 1;
            }
        }
    }

    // Generate reference point if function running

    if(funcgen.to_cycling_plep_running[channel])
    {
        // Generate reference point

        if(funcgen_gen_func[FGC_REF_PLEP] &&
           funcgen_gen_func[FGC_REF_PLEP]
                (&device->fg.running_non_cyc.pars,
                 &device->fg.time,
                 &device->ref.fg))
        {
            device->fg.time += FUNCGEN_PERIOD;
        }
        else // Function has completed
        {
            funcgen.to_cycling_plep_running[channel] = 0;
        }
    }
}



/*
 * Calculate a reference point for a device in the TO_STANDBY state
 */

static void funcgenCalcRefToStandby(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];

    // Check whether entering state

    if(funcgen.previous_state_pc[channel] != FGC_PC_TO_STANDBY)
    {
        // Set the function generator reference to the regulation reference to absorb RT reference

        device->ref.fg = device->ref.reg;

        // Calculate PLEP

        funcgenSystemPlep(channel,
                          device->pol_switch_state == FGC_POL_NEGATIVE ?
                          -device->load[device->load_select].limits.i.min : device->load[device->load_select].limits.i.min,
                          0);

        device->fg.time = FUNCGEN_PERIOD;
    }

    // Generate reference point

    if(funcgen_gen_func[FGC_REF_PLEP] &&
       funcgen_gen_func[FGC_REF_PLEP]
            (&device->fg.running_non_cyc.pars,
             &device->fg.time,
             &device->ref.fg))
    {
        device->fg.time += FUNCGEN_PERIOD;
    }
    else // Function has completed
    {
        stateSetPC(channel, FGC_PC_ON_STANDBY, "ON_STANDBY");
    }
}



int32_t funcgenArmStartFunction(uint32_t channel)
{
    struct fg_trim_config   config;
    struct Equipdev_channel *device = &equipdev.device[channel];
    struct fg_limits        limits;
    enum fg_limits_polarity limits_polarity;
    struct fg_trim_pars     *pars   = &device->fg.running_non_cyc.pars.trim;

    // Copy the device's current limits and override I_MIN to zero

    limits      = device->load[device->load_select].limits.i;
    limits.min  = 0;
    limits.user_check_limits = NULL;

    // Configure trim

    config.type     = FG_TRIM_CUBIC;
    config.duration = 0.2;
    config.final    = device->load[device->load_select].limits.i.min;

    // Set limits polarity

    if(!device->pol_switch_timeout || device->pol_switch_state == FGC_POL_POSITIVE)
    {
        limits_polarity = FG_LIMITS_POL_NORMAL;
    }
    else if(device->pol_switch_state == FGC_POL_NEGATIVE) // Device has a polarity switch with a negative polarity
    {
        limits_polarity = FG_LIMITS_POL_NEGATIVE;

        // Invert the final value

        config.final = -config.final;
    }
    else // Device has a polarity switch that is neither positive nor negative
    {
        return 1;
    }

    // Initialise the trim

    if(fgTrimArm(&limits, limits_polarity, &config, 0.6, 0, pars, // The 0.6 is 600ms run delay to allow phaseback to be removed by the CIS
                  &device->fg.running_non_cyc.meta_data) != FG_OK)
    {
        return 1;
    }

    // Zero function generator time

    device->fg.time = 0;

    return 0;
}



void funcgenSystemPlep(uint32_t channel, float final, float final_rate)
{
    struct fg_plep_config   config;
    struct Equipdev_channel *device = &equipdev.device[channel];
    struct fg_plep_pars     *pars   = &device->fg.running_non_cyc.pars.plep;

    // Configure PLEP

    config.acceleration = device->load[device->load_select].fg.defaults.i.acceleration;
    config.exp_final    = device->load[device->load_select].limits.i.min;
    config.exp_tc       = 0;
    config.final        = final;
    config.final_rate   = final_rate;
    config.linear_rate  = device->load[device->load_select].fg.defaults.i.linear_rate;

    // Calculate PLEP parameters

    fgPlepCalc(&config, pars, 0, device->ref.fg, device->ref.reg_rate,
               &device->fg.running_non_cyc.meta_data);
}



void funcgenSlowAbortPlep(uint32_t channel, float linear_rate)
{
    struct fg_plep_config   config;
    struct Equipdev_channel *device = &equipdev.device[channel];
    struct fg_plep_pars     *pars   = &device->fg.running_non_cyc.pars.plep;

    // Configure PLEP

    config.acceleration = device->load[device->load_select].fg.defaults.i.acceleration;
    config.exp_final    = 0;
    config.exp_tc       = 0;
    config.final        = 0;
    config.final_rate   = 0;
    config.linear_rate  = linear_rate;

    // Calculate PLEP parameters

    fgPlepCalc(&config, pars, 0, device->ref.fg, device->ref.reg_rate,
               &device->fg.running_non_cyc.meta_data);
}



int32_t funcgenStartDynamicEconomy(uint32_t channel)
{
    struct Equipdev_channel *device = &equipdev.device[channel];
    double                  duration;
    double                  dyn_end_time = device->ppm[timing.user].economy.dyn_end_time;
    double                  dyn_end_time_minus_period = dyn_end_time - FUNCGEN_PERIOD;
    float                   rate;
    float                   ref;
    float                   ref_prev_period;
    struct Equipdev_fg_ref  *running;

    // Do nothing if the device is not in the CYCLING state with a low rate of change

    if(device->status.state_pc    != FGC_PC_CYCLING ||  // State is not CYCLING
       fabs(device->ref.reg_rate) >= 2)                 // The rate of change is greater than 2 units per second
    {
        return 1;
    }

    // Set pointer to running function data

    running = &device->fg.running_cyc;

    // Skip device if there is no function for the reference type

    if(!funcgen_gen_func[running->ref_type])
    {
        return 1;
    }

    // Calculate the reference for the end of the economy function and the rate at that point

    funcgen_gen_func[running->ref_type](&running->pars, &dyn_end_time,              &ref);
    funcgen_gen_func[running->ref_type](&running->pars, &dyn_end_time_minus_period, &ref_prev_period);
    rate = (ref - ref_prev_period) / FUNCGEN_PERIOD;

    // Calculate the economy function

    funcgenSystemPlep(channel, ref, rate);
    duration = device->fg.running_non_cyc.meta_data.duration;

    // Calculate the economy function time offset relative to the operational cycle

    device->economy.dyn_time_offset = dyn_end_time - duration;

    // Check whether it is too late to start the economy function

    if(device->economy.dyn_time_offset < device->fg.time)
    {
        device->economy.dyn_time_offset = 0;
        return 1;
    }

    // Switch to the ECONOMY state

    stateSetPC(channel, FGC_PC_ECONOMY, "ECONOMY");

    return 0;
}



/*
 * Check whether polarity needs to be changed to play an armed function
 */

static void funcgenAutoPolarity(uint32_t channel, struct fg_meta *function_meta)
{
    struct Equipdev_channel *device = &equipdev.device[channel];

    if(function_meta->range.min < 0 ||
       function_meta->range.max < 0) // Reference is negative
    {
        if(device->pol_switch_state != FGC_POL_NEGATIVE)
        {
            if(device->pol_switch_auto)
            {
                // Automatically move polarity switch

                logPrintf(channel,
                          "FUNCGEN Automatically switching to negative polarity for user %u\n",
                          timing.next_ms_user);

                // Change to the POL state

                stateSetPC(channel, FGC_PC_POL_SWITCHING, "POL_SWITCHING");
                funcgenCalcRefPolSwitching(channel);
                return;
            }
            else // Function for cycle does not match the polarity
            {
                logPrintf(channel,
                          "FUNCGEN Function for user %u requires negative polarity\n",
                          timing.next_ms_user);

                // Trip to ON_STANDBY

                stateSetPC(channel, FGC_PC_TO_STANDBY, "TO_STANDBY");
                funcgenCalcRefToStandby(channel);
                return;
            }
        }
    }
    else // Reference is positive
    {
        if(device->pol_switch_state != FGC_POL_POSITIVE)
        {
            if(device->pol_switch_auto)
            {
                // Automatically move polarity switch

                logPrintf(channel,
                          "FUNCGEN Automatically switching to positive polarity for user %u\n",
                          timing.next_ms_user);

                // Change to the POL state

                stateSetPC(channel, FGC_PC_POL_SWITCHING, "POL_SWITCHING");
                funcgenCalcRefPolSwitching(channel);
                return;
            }
            else // Function for cycle does not match the polarity
            {
                logPrintf(channel,
                          "FUNCGEN Function for user %u requires positive polarity\n",
                          timing.next_ms_user);

                // Trip to ON_STANDBY

                stateSetPC(channel, FGC_PC_TO_STANDBY, "TO_STANDBY");
                funcgenCalcRefToStandby(channel);
                return;
            }
        }
    }
}

// EOF
