/*!
 * @file   tasks.cpp
 * @brief  Define tasks to be started and stopped
 * @author Stephen Page
 */

#include <alarms.h>
#include <cmdqmgr.h>
#include <consts.h>

#include <cmw.h>
#include <cmwpub.h>
#include <cmwutil.h>

#include <devname.h>
#include <fei.h>
#include <fgcddev.h>
#include <funcgen.h>
#include <logging.h>
#include <equipdev.h>
#include <fgcd.h>
#include <meas.h>
#include <nonvol.h>
#include <parser.h>
#include <pub.h>
#include <rbac.h>
#include <ref.h>
#include <rt.h>
#include <state.h>
#include <taskmgr.h>
#include <tcp.h>
#include <transaction.h>
#include <timing.h>

struct Task tasks[]
=
{
    // label                        start_func              fatal   flag

    { "device naming",              &devnameInit,           1,      NULL                },
    { "fast extraction interlocks", &feiInit,               0,      NULL                },
    { "measurement",                &measInit,              1,      NULL                },
    { "function generation",        &funcgenInit,           1,      NULL                },
    { "reference generation",       &refInit,               1,      NULL                },
    { "state management",           &stateInit,             1,      NULL                },
    { "RBAC",                       &rbacInit,              1,      &fgcd.enable_rbac   },
    { "parser",                     &parserInit,            1,      NULL                },
    { "command queue manager",      &cmdqmgrInit,           1,      NULL                },
    { "non-volatile storage",       &nonvolStart,           1,      NULL                },
    { "transaction commit",         &transactionCommitStart,1,      NULL                },
    { "timing",                     &timingStart,           1,      NULL                },
    { "FGCD device",                &fgcddevStart,          1,      NULL                },
    { "logging",                    &logStart,              1,      NULL                },
    { "equipment devices",          &equipdevStart,         1,      NULL                },
    { "CMW utilities",              &cmwutilInit,           1,      NULL                },
    { "CMW publication",            &cmwpubStart,           1,      NULL                },
    { "timing events",              &timingSubscribeEvents, 1,      NULL                },
    { "real-time data reception",   &rtStart,               1,      NULL                },
    { "CMW server",                 &cmwStart,              1,      NULL                },
    { "alarm surveillance",         &alarmsStart,           1,      NULL                },
    { "publishing",                 &pubStart,              1,      NULL                },
    { "TCP server",                 &tcpStart,              1,      NULL                },
    { "",                           NULL,                   0,      NULL                },
};

// EOF
