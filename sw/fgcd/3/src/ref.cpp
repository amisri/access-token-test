/*
 *  Filename: ref.c
 *
 *  Purpose:  Functions for reference generation
 *
 *  Author:   Stephen Page
 */

// __STDC_LIMIT_MACROS must be definied in C++ to make stdint.h define macros such as UINT32_MAX

#define __STDC_LIMIT_MACROS

#include <arpa/inet.h>
#include <stdint.h>
#include <string.h>

#include <equipdev.h>
#include <fgcd.h>
#include <mugef/consts.h>
#include <mugefhw/newave3.h>
#include <ref.h>

// Global variables

struct Ref ref;

int32_t refInit(void)
{
    uint32_t i;

    // Map Newave cards

    for(i = 0 ; i < NEWAVE3_NUM_ADDRS ; i++)
    {
        // Attempt to map card

        if(newave3Map(&ref.newave[i], i))
        {
            continue;
        }
    }

    // Initialise channel data

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        // Force setting of reference to zero

        ref.ref_set[i] = UINT32_MAX;
    }

    return 0;
}

void refUpdateRefs(void)
{
    struct newave3_card     *card;
    uint32_t                card_chan;                      // Channel on card
    uint32_t                card_num;                       // Card number
    struct Equipdev_channel *device;
    uint32_t                i;
    double                  prop_max;                       // Proportion of maximum value
    double                  max_prop_neg;                   // Maximum allowed negative proportion of maximum value
    double                  min_prop_max;                   // Minimum allowed proportion of maximum value (for unipolar references)
    uint32_t                ref_clip_flag;                  // Flag to indicate whether reference was clipped
    float                   ref_value_f;                    // Floating point reference
    int32_t                 ref_value_i;                    // Integer reference
    uint16_t                update_mask[NEWAVE3_NUM_ADDRS]; // Mask for each card of channels with updated values

    // Clear all update masks

    memset(update_mask, 0, sizeof(update_mask));

    // Write reference values

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        card_num    = (i - 1) / NEWAVE3_NUM_CHANS;
        card        = &ref.newave[card_num];
        device      = &equipdev.device[i];

        // Skip channel if there is no card for the channel
        // or the device is not configured

        if(!card->regs || !device->fgcd_device->name) continue;

        card_chan = (i - 1) % NEWAVE3_NUM_CHANS;

        // Calculate reference

        // Absolute value of reference is used if device has a polarity switch

        ref_value_f     = device->pol_switch_timeout ? fabs(device->ref.output) : device->ref.output;
        prop_max        = (double)ref_value_f                       / (double)device->ref_output.limits->pos;
        max_prop_neg    = (double)device->ref_output.limits->neg    / (double)device->ref_output.limits->pos;
        ref_value_i     = -prop_max * INT32_MIN;

        // Set minimum proportion of maximum current depending upon power converter's state

        if(device->status.state_pc <= FGC_PC_OFF)
        {
            min_prop_max = 0;
        }
        else // Power converter is not off
        {
            min_prop_max = (double)device->ref_output.limits->min   / (double)device->ref_output.limits->pos;
        }

        // Clip at maximum range
        //
        // Note that range is asymmetric as INT32_MAX < -INT32_MIN

        ref_clip_flag = 0;
        if(prop_max > 1 || (prop_max > .9 && ref_value_i < (INT32_MAX / 2))) // Reference is above positive limit
        {
            // Set reference to positive limit

            ref_value_i = INT32_MAX;

            if(prop_max > 1)
            {
                ref_clip_flag = 1;
            }
        }
        else if(device->ref_output.limits->neg == 0) // Reference is unipolar
        {
            // Check whether reference is below minimum allowed proportion of maximum

            if(prop_max < min_prop_max)
            {
                // Set reference to minimum allowed value

                ref_value_i     = min_prop_max * INT32_MAX;
                ref_clip_flag   = 1;
            }
        }
        else if(prop_max < max_prop_neg) // Reference is below negative limit
        {
            // Set reference to negative limit

            ref_value_i     = -max_prop_neg * INT32_MIN;
            ref_clip_flag   = 1;
        }

        // Set warning if reference was clipped

        if(ref_clip_flag)
        {
            device->status.st_warnings |=  FGC_WRN_REF_LIM;
        }
        else // Reference was not clipped
        {
            device->status.st_warnings &= ~FGC_WRN_REF_LIM;
        }

        // Skip channel if reference has not changed

        if(ref_value_i == ref.ref_set[i]) continue;

        // Set reference

        card->ref.value[card_chan]  = htonl(ref_value_i);
        ref.ref_set[i]              = ref_value_i;
        update_mask[card_num]      |= 1 << card_chan;
    }

    // Write update masks

    for(card_num = 0 ; card_num < NEWAVE3_NUM_ADDRS ; card_num++)
    {
        card = &ref.newave[card_num];

        // Skip card if not mapped

        if(!card->regs) continue;

        if(update_mask[card_num])
        {
            *card->ref.update_mask = htons(update_mask[card_num]);
        }
    }
}

// EOF
