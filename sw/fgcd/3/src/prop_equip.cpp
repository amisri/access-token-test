/*
 *  Filename: prop_equip.c
 *
 *  Purpose:  Equipment device-specific functions for getting and setting properties
 *
 *  Author:   Stephen Page
 */

#include <stdbool.h>
#include <string.h>

#include <classes/91/defconst.h>
#include <classes/91/defprops.h>
#include <cmd.h>
#include <cmwpub.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <fgc_log.h>
#include <fgc_parser_consts.h>
#include <funcgen.h>
#include <hash.h>
#include <libfg1.h>
#include <libfg/plep.h>
#include <libfg/table.h>
#include <libfg/test.h>
#include <libfg/trim.h>
#include <logging.h>
#include <mugef/cmd.h>
#include <nonvol.h>
#include <parser.h>
#include <prop.h>
#include <prop_equip.h>
#include <equip_common.h>
#include <reg.h>
#include <state.h>


// Static functions

static uint32_t SetPCToIdle(struct Equipdev_channel *device);
static uint32_t SetPCToOff(struct Equipdev_channel *device);
static uint32_t SetPCToOnStandby(struct Equipdev_channel *device);
static uint32_t SetPCToCycling(struct Equipdev_channel *device);

/*
 * Add log signal headers to command value
 */

static uint32_t propAddLogSignalHeaders(struct Cmd *command, struct prop *property)
{
    uint32_t                    i;
    uint32_t                    n_signals;      // Number of signals
    struct fgc_log_sig_header   *sig_header;    // Pointer to signal header within value

    // Set property-specific log header fields

    switch(property->sym_idx)
    {
        case STP_MEAS:
        case STP_REF:
            n_signals = 1;
            break;

        default: // Function has been called for a property it does not handle
            command->error = FGC_NOT_IMPL;
            return 0;
    }

    // Add signal headers

    for(i = 0 ; i < n_signals ; i++)
    {
        // Check that value buffer has space for log header

        if(command->value_length + sizeof(*sig_header) >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 0;
        }

        // Add signal header to value

        sig_header = (struct fgc_log_sig_header *)&command->value[command->value_length];
        command->value_length += sizeof(*sig_header);

        // Initialise signal header

        memset(sig_header, 0, sizeof(*sig_header));

        sig_header->gain    = 1;
        sig_header->type    = FGC_LOG_TYPE_FLOAT;

        // Set property-specific signal header fields

        switch(property->sym_idx)
        {
            case STP_MEAS:
                sig_header->label_len = sizeof("MEAS") - 1;
                strncpy(sig_header->label, "MEAS", sig_header->label_len);

                sig_header->units_len = sizeof("amps") - 1;
                strncpy(sig_header->units, "amps", sig_header->units_len);

                break;

            case STP_REF:
                sig_header->info = FGC_LOG_SIG_INFO_STEPS;

                sig_header->label_len = sizeof("REF") - 1;
                strncpy(sig_header->label, "REF", sig_header->label_len);

                sig_header->units_len = sizeof("amps") - 1;
                strncpy(sig_header->units, "amps", sig_header->units_len);

                break;

            default: // Function has been called for a property it does not handle
                command->error = FGC_NOT_IMPL;
                return 0;
        }
    }

    return n_signals;
}

static int32_t propEquipScanRefParams(uint8_t ref_type, char delimiter, struct Cmd *command)
{
    uint32_t float_count;
    double   float_buffer[8];

    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Restore user number for command

    command->user = command->parser->user;

    // Scan float parameters from command string

    for(float_count = 0 ; !command->error && delimiter != '\0' && float_count < 8 ; float_count++)
    {
        parserScanFloat(command->parser, command->value,
                        &command->index, command->value_length,
                        ",", 1, &delimiter, 0, 0, 0, 0, &float_buffer[float_count]);
    }

    // Override NO_SYMBOL error

    if(command->error == FGC_NO_SYMBOL)
    {
        command->error = 0;
    }

    // Check whether an error has occurred

    if(command->error)
    {
        return 1;
    }

    // Check that the end of the command was reached

    if(delimiter != '\0')
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Copy floats to parameters for reference type

    switch(ref_type)
    {
        case FGC_REF_CTRIM:
        case FGC_REF_LTRIM:

            // Check whether an invalid number of floats has been supplied

            if(!float_count || float_count > 2)
            {
                command->error = FGC_BAD_PARAMETER;
                return 1;
            }

            // Set reference configuration

            device.ppm[command->user].transactional.fg_config.trim.final    =                   float_buffer[0];
            device.ppm[command->user].transactional.fg_config.trim.duration = float_count > 1 ? float_buffer[1] : 0;

            break;

        case FGC_REF_COSINE:
        case FGC_REF_SINE:
        case FGC_REF_SQUARE:
        case FGC_REF_STEPS:

            // Check whether an invalid number of floats has been supplied

            if(!float_count || float_count > 3)
            {
                command->error = FGC_BAD_PARAMETER;
                return 1;
            }

            device.ppm[command->user].transactional.test.amplitude_pp =                   float_buffer[0];
            device.ppm[command->user].transactional.test.num_cycles   = float_count > 1 ? float_buffer[1] : 1;
            device.ppm[command->user].transactional.test.period       = float_count > 2 ? float_buffer[2] : 1;

            break;

        case FGC_REF_NONE:

            // No floats are allowed

            if(float_count)
            {
                command->error = FGC_BAD_PARAMETER;
            }
            break;

        case FGC_REF_NOW:
        case FGC_REF_PLEP:

            // Check whether an invalid number of floats has been supplied

            if(!float_count || float_count > 5)
            {
                command->error = FGC_BAD_PARAMETER;
                return 1;
            }

            // Set reference configuration

            device.ppm[command->user].transactional.plep.final        =                   float_buffer[0];
            device.ppm[command->user].transactional.plep.acceleration = float_count > 1 ? float_buffer[1] : device.load[device.load_select].fg.defaults.i.acceleration;
            device.ppm[command->user].transactional.plep.linear_rate  = float_count > 2 ? float_buffer[2] : device.load[device.load_select].fg.defaults.i.linear_rate;
            device.ppm[command->user].transactional.plep.exp_tc       = float_count > 3 ? float_buffer[3] : 0;
            device.ppm[command->user].transactional.plep.exp_final    = float_count > 4 ? float_buffer[4] : 0;

            break;

        default:
            command->error = FGC_NOT_IMPL;
            return 1;
    }

    return 0;
}



static uint32_t propEquipRefArm(uint8_t ref_type, struct Cmd *command)
{
    struct Equipdev_channel &device     = equipdev.device[command->device->id];
    struct Equipdev_fg_ref  *fg_config  = &device.ppm[command->user].transactional.fg_config;

    fg_config->ref_type = ref_type;

    if(!device.arming_enabled)
    {
        // Arming is not enabled: nothing to do

        return 0;
    }
    else
    {
        // Arm function

        return (command->error = equipdevArmCycle(command->device->id, command->user, false));
    }
}



int32_t SetifModeOpOk(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    // Check whether device is in a state in which the operational mode may be changed

    switch(device->status.state_pc)
    {
        // States in which the operational mode may be changed

        case FGC_PC_FLT_OFF:
        case FGC_PC_FLT_STOPPING:
        case FGC_PC_OFF:
        case FGC_PC_STOPPING:
            return 1;

        default: // Operational mode may not be changed
            return 0;
    }
}



int32_t SetifModeRtOk(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_pc < FGC_PC_IDLE;
}



int32_t SetifOpNormalSim(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_op == FGC_OP_NORMAL ||
           device->status.state_op == FGC_OP_SIMULATION;
}



int32_t SetifOpNotCal(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_op != FGC_OP_CALIBRATING;
}



int32_t SetifPcArmed(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_pc == FGC_PC_ARMED;
}



int32_t SetifPcOff(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_pc == FGC_PC_OFF ||
           device->status.state_pc == FGC_PC_FLT_OFF;
}



int32_t SetifRegStateNone(struct Cmd *command)
{
    return SetifPcOff(command);
}



int32_t SetifRefNone(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    // Return whether function generator is armed for user

    return device->ppm[command->user].fg.armed.ref_type == FGC_REF_NONE;
}



int32_t SetifRefOk(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    // Handle PPM and non-PPM commands

    if(command->user) // PPM command
    {
        return 1; // PPM references are always allowed
    }
    else // Non-PPM command
    {
        return device->status.state_pc == FGC_PC_IDLE; // Device is in the IDLE state
    }
}



int32_t SetifRefUnlock(struct Cmd *command)
{
    return 1;
}



int32_t SetifSwitchOk(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return fabs(device->meas.i_meas) < (device->load[device->load_select].limits.i.pos * 0.001) &&  // Measured current is less than 0.1% of I_POS
           device->status.state_vs   < FGC_VS_STOPPING;                                            // Voltage source is off
}



int32_t SetifTxIdOk(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    // A transaction may not be started for non-PPM settings when in the ARMED state

    if(command->user == 0 && device->status.state_pc == FGC_PC_ARMED)
    {
        return 0;
    }

    return 1;
}



int32_t GetLog(struct Cmd *command, struct prop *property)
{
    struct Parser   *parser             = command->parser;

    uint32_t        from, to;           // Range within array
    int32_t         i;                  // Index within log circular buffer
    int32_t         log_element_size    = sizeof(equipdev.device[0].log[0]);
    int32_t         log_element         = equipdev.ppm[command->user].log.index;
    uint32_t        log_from;           // Starting index within log circular buffer
    uint32_t        n;                  // Element index
    char            *value;

    // Set cycle timestamp

    command->cycle_timestamp = equipdev.ppm[command->user].log.cycle_timestamp;

    // Advance value to point to data for user within buffer

    parser->value += log_element * log_element_size;

    // Store parser values to allow them to be restored after manipulation

    from    = parser->from;
    to      = parser->to;
    value   = parser->value;

    // Calculate index within log

    log_from = (log_element + from) % property->max_elements;

    // Get each log element

    parser->from    = 0;
    parser->to      = 0;

    for(i = log_from, n = from ;
        n <= to ;
        i = (i + parser->step) % property->max_elements, n += parser->step)
    {
        parser->value = value + ((i - log_element) * log_element_size);

        switch(property->type)
        {
            case PT_FLOAT:
                if(parser->getopt_flags & GET_OPT_BIN)
                {
                    GetBin(command, property);
                }
                else
                {
                    GetFloat(command, property);
                }
                break;

            case PT_INT8S:
            case PT_INT8U:
            case PT_INT16S:
            case PT_INT16U:
            case PT_INT32S:
            case PT_INT32U:
                if(parser->getopt_flags & GET_OPT_BIN)
                {
                    GetBin(command, property);
                }
                else
                {
                    GetInteger(command, property);
                }
                break;

            default:
                command->error = FGC_NOT_IMPL;
                break;
        }

        // Stop if an error has occurred

        if(command->error)
        {
            return 1;
        }
        // Add comma to value

        if(!(parser->getopt_flags & GET_OPT_BIN))
        {
            if(command->value_length + 1 >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }
            command->value[command->value_length++] = ',';
        }
    }

    // Restore parser values

    parser->from    = from;
    parser->to      = to;
    parser->value   = value;

    // Decrement value length to remove trailing comma

    if(!(parser->getopt_flags & GET_OPT_BIN))
    {
        command->value_length--;
    }

    // Set acquisition timestamp

    if(command->user) // PPM command
    {
        // Set acquisition timestamp

        const struct timeval *user_log_time = &equipdev.ppm[command->user].log.timestamp;

        // Timestamp is cycle time + 'from' milliseconds

        command->acq_timestamp.tv_sec  = user_log_time->tv_sec  + (from / 1000);
        command->acq_timestamp.tv_usec = user_log_time->tv_usec + ((from % 1000) * 1000);

        // Check whether tv_usec is greater than a second (roll over a second)

        if(command->acq_timestamp.tv_usec > 1000000)
        {
            command->acq_timestamp.tv_sec++;
            command->acq_timestamp.tv_usec -= 1000000;
        }
    }
    else // Non-PPM command
    {
        // Timestamp is acquisition time - (log length - 'from') milliseconds

        command->acq_timestamp.tv_sec  = command->req_timestamp.tv_sec - ((property->max_elements - 1) - from) / 1000;
        command->acq_timestamp.tv_usec = command->req_timestamp.tv_usec - (((property->max_elements - 1) - from) % 1000) * 1000;

        // Check whether tv_usec is less than 0 (roll back a second)

        if(command->acq_timestamp.tv_usec < 0)
        {
            command->acq_timestamp.tv_sec--;
            command->acq_timestamp.tv_usec += 1000000;
        }
    }

    return 0;
}



int32_t GetLogSpy(struct Cmd *command, struct prop *property)
{
    struct prop             *child;
    uint32_t                from, to;
    uint32_t                i;
    struct fgc_log_header   *log_header = NULL;
    uint32_t                num_children;
    uint32_t                num_elements;
    struct Parser           *parser     = command->parser;
    char                    *value;

    // Use GetParent() if property is a parent and binary get option is not set

    if(property->type == PT_PARENT &&
       !(parser->getopt_flags & GET_OPT_BIN))
    {
        return GetParent(command, property);
    }

    // Get number of elements in log for user

    num_elements = equipdev.ppm[command->user].log.num_elements;

    // Return if no data is available

    if(num_elements == 0)
    {
        return 0;
    }

    // Validate array indices if property is a parent

    if(property->type == PT_PARENT)
    {
        // Check whether start of array range was specified

        if(parser->from == UNSPECIFIED)
        {
            parser->from = 0;
        }

        // Override index if maximum number of elements is less that actual number of elements

        if(property->max_elements < num_elements)
        {
            parser->from += num_elements - property->max_elements;
        }

        // Check whether end of array range was specified

        if(parser->to == UNSPECIFIED)
        {
            parser->to = num_elements - 1;
        }
        else // to index was specified
        {
            // Override index if maximum number of elements is less that actual number of elements

            if(property->max_elements < num_elements)
            {
                parser->to += num_elements - property->max_elements;
            }

            // Check whether maximum number of elements has been exceeded

            if((parser->to - parser->from) + 1 > property->max_elements)
            {
                command->error = FGC_BAD_ARRAY_IDX;
                return 1;
            }
        }
    }

    // Add log header if binary get option is set

    if(parser->getopt_flags & GET_OPT_BIN)
    {
        // Check that value buffer has space for log header

        if(command->value_length + sizeof(*log_header) >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }

        // Add log header to value

        log_header = (struct fgc_log_header *)&command->value[command->value_length];
        command->value_length += sizeof(*log_header);

        // Initialise log header

        memset(log_header, 0, sizeof(*log_header));

        log_header->info        = htons(1) == 1 ? 0 : FGC_LOG_BUF_INFO_LITTLE_ENDIAN;
        log_header->n_samples   = ((parser->to - parser->from) + 1) / parser->step;
        log_header->period_us   = 1000;
        log_header->version     = FGC_LOG_VERSION;
    }

    // Handle parent or child properties

    if(property->type == PT_PARENT)
    {
        // Process each child property

        num_children = parserGetNumEls(parser);

        // Add signal headers if a log header is present

        if(log_header)
        {
            for(i = 0, child = (struct prop *)parser->value ; i < num_children ; i++, child++)
            {
                log_header->n_signals += propAddLogSignalHeaders(command, child);
                if(command->error) return 1;
            }
        }

        // Add log signals to value

        from        = parser->from;
        to          = parser->to;
        parser->to  = from;
        value       = parser->value;

        child = (struct prop *)parser->value;

        for( ; parser->to <= to ; parser->from += parser->step,
                                  parser->to    = parser->from)
        {
            for(i = 0, child = (struct prop *)parser->value ; i < num_children ; i++, child++)
            {
                if(!parserConfigProp(parser, child))
                {
                    parserConfigGet( parser, child);
                }

                // Ignore bad array index error as indices have already been validated

                if(command->error == FGC_BAD_ARRAY_IDX)
                {
                    command->error = 0;
                }

                // Add signal to value

                if(command->error || GetLog(command, child))
                {
                    return 1;
                }

                // Restore value to point to parent value

                parser->value = value;
            }
        }

        // Restore array range

        parser->from    = from;
        parser->to      = to;

        // Set acquisition timestamp for parents

        if(command->user) // PPM command
        {
            // Timestamp is cycle time + 'from' milliseconds

            command->acq_timestamp.tv_sec   = command->cycle_timestamp.tv_sec  +  (from / 1000);
            command->acq_timestamp.tv_usec  = command->cycle_timestamp.tv_usec + ((from % 1000) * 1000);

            // Check whether tv_usec is greater than a second (roll over a second)

            if(command->acq_timestamp.tv_usec > 1000000)
            {
                command->acq_timestamp.tv_sec++;
                command->acq_timestamp.tv_usec -= 1000000;
            }
        }
        else // Non-PPM command
        {
            // Recover pointer to one child

            child--;

            // Timestamp is acquisition time - (log length - 'from') milliseconds

            command->acq_timestamp.tv_sec  =  command->req_timestamp.tv_sec - ((child->max_elements - 1) - from) / 1000;
            command->acq_timestamp.tv_usec =  command->req_timestamp.tv_usec - (((child->max_elements - 1) - from) % 1000) * 1000;

            // Check whether tv_usec is less than 0 (roll back a second)

            if(command->acq_timestamp.tv_usec < 0)
            {
                command->acq_timestamp.tv_sec--;
                command->acq_timestamp.tv_usec += 1000000;
            }
        }

    }
    else // Property is not a parent
    {
        // Add signal headers if a log header is present

        if(log_header)
        {
            log_header->n_signals = propAddLogSignalHeaders(command, property);
            if(command->error) return 1;
        }

        // Add log signal to value

        if(GetLog(command, property))
        {
            return 1;
        }
    }

    // Set timestamp in log header if one exists

    if(log_header)
    {
        log_header->unix_time   = command->acq_timestamp.tv_sec;
        log_header->us_time     = command->acq_timestamp.tv_usec;
    }

    return 0;
}



int32_t SetLoadSwitch(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];
    struct Parser           *parser = command->parser;

    // Override parser value to use switch request rather than state

    parser->value = (char *)&device->load_switch1_request;

    // Perform integer set

    if(SetInteger(command, property))
    {
        return 1;
    }

    // Request switch movement if the switch is not already in the requested state

    if(device->load_switch1_state != *(uint32_t *)parser->value)
    {
        if(stateRequestLoadSwitch1(device->fgcd_device->id, *(uint32_t *)parser->value))
        {
            command->error = FGC_BAD_STATE;
            return 1;
        }
    }

    // Succesful request

    return 0;
}



int32_t SetPC(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];
    struct Parser           *parser = command->parser;
    uint8_t                 requested_state;
    uint32_t                sym_index;
    const struct sym_lst   *sym_list;

    // Check that symbol list is defined

    if(!(property->flags & PF_SYM_LST))
    {
        command->error = FGC_NOT_IMPL;
        return 1;
    }
    sym_list = reinterpret_cast<const sym_lst*>(property->range);

    // Scan symbol from command string

    if((sym_index = parserIdentifyNextSymbol(parser, command->value,
                                             &command->index, command->value_length,
                                             parser->class_data->const_hash, "", 1, NULL)))
    {
        // Scan property's symbol list for a match

        for( ; sym_list->index && sym_list->index != sym_index ; sym_list++);

        // Check whether no match was found

        if(!sym_list->index)
        {
            // Symbol was not valid, return error

            command->error = FGC_BAD_PARAMETER;
            return 1;
        }

        // Set requested state

        requested_state = sym_list->value;
    }
    else // An error has occurred
    {
        return 1;
    }

    // Do nothing if device is already in requested state

    if(device->status.state_pc == requested_state)
    {
        device->mode_pc = requested_state;
        return 0;
    }

    // Handle requested state

    switch(requested_state)
    {
        case FGC_PC_CYCLING:    command->error = SetPCToCycling(    device);    break;
        case FGC_PC_IDLE:       command->error = SetPCToIdle(       device);    break;
        case FGC_PC_OFF:        command->error = SetPCToOff(        device);    break;
        case FGC_PC_ON_STANDBY: command->error = SetPCToOnStandby(  device);    break;

        default:                command->error = FGC_BAD_PARAMETER;             return 1;
    }

    if(command->error)
    {
        return 1;
    }

    device->mode_pc = requested_state;
    cmwpubNotify(device->fgcd_device->id, "MODE.PC", 0, 0, 0, 1, 0);
    return 0;
}



/*
 * Set STATE.PC to IDLE state
 */

static uint32_t SetPCToIdle(struct Equipdev_channel *device)
{
    switch(device->status.state_pc)
    {
        case FGC_PC_OFF:

            // Start the power converter

            if(stateRequestStart(device->fgcd_device->id))
            {
                return FGC_BAD_STATE;
            }
            break;

        // Disallowed states

        case FGC_PC_STARTING:
        case FGC_PC_STOPPING:
            return FGC_BAD_STATE;

        default:
            stateSetPC(device->fgcd_device->id, FGC_PC_IDLE, "IDLE");
            break;
    }
    return 0;
}



/*
 * Set STATE.PC to OFF state
 */

static uint32_t SetPCToOff(struct Equipdev_channel *device)
{
    switch(device->status.state_pc)
    {
        // States from which a slow abort should be performed

        case FGC_PC_ABORTING:
        case FGC_PC_ARMED:
        case FGC_PC_CYCLING:
        case FGC_PC_ECONOMY:
        case FGC_PC_IDLE:
        case FGC_PC_RUNNING:
        case FGC_PC_TO_CYCLING:
        case FGC_PC_TO_STANDBY:
            stateSetPC(device->fgcd_device->id, FGC_PC_SLOW_ABORT, "SLOW_ABORT");
            break;

        // Reset power converter faults

        case FGC_PC_FLT_OFF:
            if(stateRequestReset(device->fgcd_device->id))
            {
                return FGC_BAD_STATE;
            }
            break;

        // States in which there should be no effect

        case FGC_PC_STOPPING:
            break;

        // Stop the power converter by default

        default:
            if(device->status.state_vs == FGC_VS_READY &&
               stateRequestStop(device->fgcd_device->id))
            {
                return FGC_BAD_STATE;
            }
            break;
    }
    return 0;
}



/*
 * Set STATE.PC to ON_STANDBY state
 */

static uint32_t SetPCToOnStandby(struct Equipdev_channel *device)
{
    switch(device->status.state_pc)
    {
        case FGC_PC_ABORTING:
        case FGC_PC_ARMED:
        case FGC_PC_CYCLING:
        case FGC_PC_ECONOMY:
        case FGC_PC_IDLE:
        case FGC_PC_RUNNING:
        case FGC_PC_TO_CYCLING:
            stateSetPC(device->fgcd_device->id, FGC_PC_TO_STANDBY, "TO_STANDBY");
            break;

        case FGC_PC_OFF:

            // Start the power converter

            if(stateRequestStart(device->fgcd_device->id))
            {
                return FGC_BAD_STATE;
            }
            break;

        default:
            return FGC_BAD_STATE;
    }
    return 0;
}






/*
 * Set STATE.PC to TO_CYCLING state
 */

static uint32_t SetPCToCycling(struct Equipdev_channel *device)
{
    switch(device->status.state_pc)
    {
        case FGC_PC_OFF:

            // Start the power converter

            if(stateRequestStart(device->fgcd_device->id))
            {
                return FGC_BAD_STATE;
            }
            break;

        case FGC_PC_ARMED:
        case FGC_PC_IDLE:
        case FGC_PC_ON_STANDBY:
            stateSetPC(device->fgcd_device->id, FGC_PC_TO_CYCLING, "TO_CYCLING");
            break;

        default:
            return FGC_BAD_STATE;
    }
    return 0;
}



int32_t SetPolaritySwitch(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];
    struct Parser           *parser = command->parser;
    uint32_t                *switch_request;
    uint32_t                *switch_state;
    bool                    ispolarity;

    // Check if is a the seocnd load switch or the polarity switch

    if (!strncasecmp("switch.polarity.state",command->command_string,command->command_length) ||
        !strncasecmp("switch.polarity.mode",command->command_string,command->command_length))
    {
        // Check second load switch is disabled

        if (device->load_switch2_timeout != 0)
        {
            logPrintf(command->device->id,"PROP_EQUIP Trying to set polarity switch while SWITCH.LOAD.TIMOUT != 0\n");

            return FGC_BAD_STATE;
        }

        ispolarity     = true;
        switch_request = &(device->pol_switch_request);
        switch_state   = &(device->pol_switch_state);


    }
    else if (!strncasecmp("switch.load2.state",command->command_string,command->command_length))
    {
        // Check polarity switch is disabled

        if (device->pol_switch_timeout != 0)
        {
            logPrintf(command->device->id,"PROP_EQUIP Trying to set second load switch while SWITCH.POLARITY.TIMOUT != 0\n");

            return FGC_BAD_STATE;
        }

        ispolarity     = false;
        switch_request = &(device->load_switch2_request);
        switch_state   = &(device->load_switch2_state);

    }
    else
    {
        return FGC_SYNTAX_ERROR;
    }

    // Override parser value to use switch request rather than state

    parser->value = (char *)switch_request;

    // Perform integer set

    if(SetInteger(command, property))
    {
        return 1;
    }

    // Request switch movement if the switch is not already in the requested state

    if(*switch_state != *switch_request)
    {
        if (ispolarity)
        {
            if(stateRequestPolarity(device->fgcd_device->id, *switch_request))
            {
                command->error = FGC_BAD_STATE;
                return 1;
            }
        }
        else
        {
            if(stateRequestLoadSwitch2(device->fgcd_device->id, *switch_request))
            {
                command->error = FGC_BAD_STATE;
                return 1;
            }

        }
    }

    return 0;
}



int32_t SetRef(struct Cmd *command, struct prop *property)
{
    char                    delimiter;
    struct Equipdev_channel &device = equipdev.device[command->device->id];
    uint8_t                 ref_type;

    // Check that the state is IDLE if the command is not PPM

    if(!command->user && device.status.state_pc != FGC_PC_IDLE)
    {
        equipdevRecoverRefSettings( &device, command->user);
        return 1;
    }

    // Get the first parameter (the reference function type)

    if(propEquipScanSymbolList(&ref_type, &delimiter, command, property) != 0)
    {
        equipdevRecoverRefSettings( &device, command->user);
        return 1;
    }

    switch(property->sym_idx)
    {
        case STP_REF:
            if(propEquipScanRefParams(ref_type, delimiter, command) != 0) return 1;
            break;

        case STP_TYPE:
            if(delimiter != '\0') command->error = FGC_BAD_PARAMETER;
            break;

        default:
            command->error = FGC_NOT_IMPL;
            break;
    }

    // Check whether an error has occurred

    if(command->error)
    {
        equipdevRecoverRefSettings(&device, command->user);
        return 1;
    }

    // Arm function, if the command is not transactional

    if(command->transaction_id == 0 && propEquipRefArm(ref_type, command) != 0)
    {
        equipdevRecoverRefSettings(&device, command->user);
        return 1;
    }

    return 0;
}

int32_t SetRefControlValue(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel &device              = equipdev.device[command->device->id];

    // Check no user sent

    if(command->user)
    {
        command->error = FGC_BAD_CYCLE_SELECTOR;
        return 1;
    }

    // Check state in IDLE after initialitzation

    if(device.arming_enabled && (device.status.state_pc != FGC_PC_IDLE))
    {
        command->error = FGC_BAD_STATE;
        return 1;
    }

    // Read PLEP params (usually just reference)

    if(propEquipScanRefParams(FGC_REF_NOW, ',', command) != 0)
    {
        return 1;
    }

    // Arm function

    if(propEquipRefArm(FGC_REF_NOW, command) != 0)
    {
        equipdevRecoverRefSettings( &device, command->user);
        return 1;
    }

    cmwpubNotify(device.fgcd_device->id, "REF.ARMED.TABLE",          0, 0, 0, 0, 0);

    return 0;
}

int32_t SetReset(struct Cmd *command, struct prop *property)
{
    uint32_t            channel     = command->device->id;
    struct hash_entry   *sym_tab    = command->parser->class_data->sym_tab_prop;
    char                *symbol     = sym_tab[property->sym_idx].key.c;

    // Property-specific handling

    if(!strcmp(symbol, "VS"))
    {
        // Request a reset of the device

        stateRequestReset(channel);
    }
    else // Function has been called for a property it does not handle
    {
        command->error = FGC_NOT_IMPL;
        return 1;
    }

    return 1;
}

int32_t SetTableFunc(struct Cmd *command, struct prop *property)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    uint32_t result;

    // We only allow changing user 0 in IDLE

    if(command->user == 0 && device.status.state_pc != FGC_PC_IDLE)
    {
        command->error = FGC_BAD_STATE;
        return 1;
    }
    else if((result = SetPoint(command, property)) == 0 && command->transaction_id == 0)
    {
        result = propEquipRefArm(FGC_REF_TABLE, command);
    }

    if(result != 0)
    {
        equipdevRecoverRefSettings(&device, command->user);
    }

    return result;
}



void SetParameters(void)
{
    // Call property set parameter functions for all active devices

    for(uint32_t channel = 0 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        if(fgcd.device[channel].name != NULL)
        {
            parserCallPropSetParsFunctions(channel);
        }
    }
}



void ParsLimits(uint32_t channel)
{
    struct Equipdev_channel             *device = &equipdev.device[channel];
    uint32_t                            invert_limits;
    struct Equipdev_channel_load_limits *limits = &device->load[device->load_select].limits;

    // Set a flag to indicate whether the limits should be inverted

    invert_limits = !device->pol_switch_timeout || device->pol_switch_state == FGC_POL_POSITIVE ? 0 : 1;

    // Initialise measurement limits for current

    regLimMeasInit(&reg.converter[channel].lim_i_meas,
                   limits->i.pos, limits->i.neg,
                   limits->i.pos * 0.1,
                   limits->i.pos * 0.01,
                   invert_limits);
}



void ParsLoad(uint32_t channel)
{
    struct Equipdev_channel         *device = &equipdev.device[channel];
    struct Equipdev_channel_load    *load   = &device->load[device->load_select];

    // Initialise load for regulation

    regLoadInit(&reg.pars[channel].load,
                load->ohms_ser, load->ohms_par,
                load->ohms_mag, load->henrys, 0);
}



void ParsLoadSaturation(uint32_t channel)
{
    struct Equipdev_channel         *device = &equipdev.device[channel];
    struct Equipdev_channel_load    *load   = &device->load[device->load_select];

    // Initialise load saturation parameters for regulation

    regLoadInitSat(&reg.pars[channel].load, load->henrys_sat,
                   load->i_sat_start, load->i_sat_end);
}



void ParsLoadSelect(uint32_t channel)
{
    ParsLimits(channel);
    ParsLoad(channel);
    ParsLoadSaturation(channel);
}



void ParsMeasSim(uint32_t channel)
{
}



void ParsRunTime(uint32_t channel)
{
}



void ParsAbortTime(uint32_t channel)
{
}

// EOF
