/*
 *  Filename: fei.c
 *
 *  Purpose:  Functions for fast extraction interlocks
 *
 *  Author:   Stephen Page
 */

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>

#include <cmwpub.h>
#include <equipdev.h>
#include <fei.h>
#include <logging.h>
#include <mugefhw/fei.h>
#include <timing.h>


// Global variables

struct Fei fei;



int32_t feiInit(void)
{
    // Check whether FEI has already been initialised

    if(fei.card.regs) return 1;

    // Map FEI card

    if(feiMap(&fei.card))
    {
        fprintf(stderr, "Note: Failed to map FEI card.\n");
        return 1;
    }

    // Check that firmware version matches expected

    if(ntohs(fei.card.regs->version) != FEI_EXPECTED_VERSION)
    {
        fprintf(stderr, "WARNING: FEI firmware version does not match expected.\n");
        feiUnmap(&fei.card);
        return 1;
    }

    // Set pointers for property access

    fei.prop.version    = &fei.card.regs->version;
    fei.prop.beam_dump  = fei.card.regs->beam_dump;
    fei.prop.fei        = fei.card.regs->fei;

    return 0;
}

void feiCheck(void)
{
    uint32_t                        channel;
    struct Equipdev_channel         *device;
    struct Equipdev_channel_ppm_fei *device_fei;
    uint32_t                        fei_occurrence = equipdev.ppm[timing.user].fei.num_occurrences;
    struct Equipdev_ppm_fei_output  *fei_output;
    uint32_t                        output_num;
    uint32_t                        output_result[FEI_NUM_FEI_OUTPUTS];

    // Initialise result for each output to success

    for(output_num = 0 ; output_num < FEI_NUM_FEI_OUTPUTS ; output_num++)
    {
        output_result[output_num] = 1;
    }

    // Check FEI condition for each device

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device      = &equipdev.device[channel];
        device_fei  = &equipdev.device[channel].ppm[timing.user].fei;

        // Skip device if it does not use an FEI output

        output_num = device_fei->output;
        if(!output_num) continue;

        device_fei->i_meas[fei_occurrence] = device->meas.i_meas;
        if(fabs(device->meas.i_meas - device_fei->i_ref) > device_fei->i_tol)
        {
            // Measured current or reference is out of tolerance

            device_fei->result[fei_occurrence]  = 0;
            output_result[output_num - 1]       = 0;
        }
        else if (device->status.state_op == FGC_OP_SIMULATION)
        {
            // Device is in SIMULATION mode

            device_fei->result[fei_occurrence]  = 0;
            output_result[output_num - 1]       = 0;
        }
        else // FEI check for device succeeded
        {
            device_fei->result[fei_occurrence] = 1;
        }

        // Notify CMW of result and measurement changes

        cmwpubNotify(channel, "INTERLOCK.FEI.RESULT", timing.user, 0, 0, 0, 0);
        cmwpubNotify(channel, "INTERLOCK.FEI.I.MEAS", timing.user, 0, 0, 1, 0); // Parent properties are notified by this line
    }

    // Set result of FEI check

    for(output_num = 0 ; output_num < FEI_NUM_FEI_OUTPUTS ; output_num++)
    {
        fei_output = &equipdev.ppm[timing.user].fei.output[output_num];

        if(fei_output->active && output_result[output_num]) // FEI check for output succeeded
        {
            fei_output->result[fei_occurrence] = 1;

            // Set FEI output counter to the configured number of 100us ticks

            if(fei.card.regs)
            {
                fei.card.regs->fei[output_num] = htons(fei_output->pulse_width_100us);
            }
        }
        else // Output is inactive for user or FEI check for output failed
        {
            fei_output->result[fei_occurrence] = 0;

            // Set FEI output counter to 0

            if(fei.card.regs)
            {
                fei.card.regs->fei[output_num] = 0;
            }
        }
    }

    // Notify CMW of output result changes

    cmwpubNotify(0, "INTERLOCK.FEI.OUTPUT1.RESULT", timing.user, 0, 0, 1, 0);
    cmwpubNotify(0, "INTERLOCK.FEI.OUTPUT2.RESULT", timing.user, 0, 0, 1, 0);
    cmwpubNotify(0, "INTERLOCK.FEI.OUTPUT3.RESULT", timing.user, 0, 0, 1, 0);
    cmwpubNotify(0, "INTERLOCK.FEI.OUTPUT4.RESULT", timing.user, 0, 0, 1, 0);
}

// EOF
