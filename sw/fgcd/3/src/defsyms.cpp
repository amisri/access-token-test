/*!
 * @file   defsyms.c
 * @brief  File use to intialize the constants at class/91/defsyms.h
 * @author Marc Magrans de Abril
 */

#define DEFSYMS

#include "classes/91/defsyms.h"
