/*
 *  Filename: equipdev.c
 *
 *  Purpose:  Functions for equipment devices
 *
 *  Author:   Stephen Page
 */

// __STDC_LIMIT_MACROS must be definied in C++ to make stdint.h define macros such as UINT32_MAX

#define __STDC_LIMIT_MACROS

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <classes/91/defconst.h>
#include <classes/91/definfo.h>
#include <classes/91/defprops.h>
#include <cmwpub.h>
#include <consts.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <fgc_stat.h>
#include <fgcddev.h>
#include <fgcd_thread.h>
#include <funcgen.h>
#include <hash.h>
#include <meas.h>
#include <logging.h>
#include <libfg1.h>
#include <nonvol.h>
#include <parser.h>
#include <prop.h>
#include <pub.h>
#include <ref.h>
#include <state.h>
#include <timing.h>

#if N_PROP_SYMBOLS > EQUIPDEV_MAX_PROPS
#error EQUIPDEV_MAX_PROPS must be greater than or equal to N_PROP_SYMBOLS
#endif

// Global variables

struct Equipdev equipdev;

const char *equipdev_log_menu_names[FGC_MAX_LOG_MENUS] = {
    "I",
    "I_MEAS",
    "I_REF",
    "DAC",
    "REF_MEAS",
};

const char *equipdev_log_menu_props[FGC_MAX_LOG_MENUS] = {
    "LOG.MUGEF.I",
    "LOG.MUGEF.I.MEAS",
    "LOG.MUGEF.I.REF",
    "LOG.MUGEF.DAC",
    "LOG.MUGEF.DAC.MEAS",
};



// Mapping of properties to their corresponding reference types

// This is used to automatically determine the reference type for transactions based upon the last property set.
// Properties with a matching reference type or a reference type of NONE will be notified for publication and non-volatile storage when a transaction is ended.

struct property_ref_type_mapping
{
    const char *        property_name;
    const struct prop * property;
    const uint32_t      ref_type;
} property_ref_type_mapping[] =
{
    { "REF.FUNC.TYPE",          &PROP_REF_FUNC_TYPE,            FGC_REF_NONE    },
    { "REF.PLEP.ACCELERATION",  &PROP_REF_PLEP_ACCELERATION,    FGC_REF_PLEP    },
    { "REF.PLEP.EXP.FINAL",     &PROP_REF_PLEP_EXP_FINAL,       FGC_REF_PLEP    },
    { "REF.PLEP.EXP.TC",        &PROP_REF_PLEP_EXP_TC,          FGC_REF_PLEP    },
    { "REF.PLEP.FINAL",         &PROP_REF_PLEP_FINAL,           FGC_REF_PLEP    },
    { "REF.PLEP.LINEAR_RATE",   &PROP_REF_PLEP_LINEAR_RATE,     FGC_REF_PLEP    },
    { "REF.TABLE.FUNC.VALUE",   &PROP_REF_TABLE_FUNC_VALUE,     FGC_REF_TABLE   },
    { "REF.TEST.AMPLITUDE",     &PROP_REF_TEST_AMPLITUDE,       FGC_REF_SINE    },
    { "REF.TEST.NUM_CYCLES",    &PROP_REF_TEST_NUM_CYCLES,      FGC_REF_SINE    },
    { "REF.TEST.PERIOD",        &PROP_REF_TEST_PERIOD,          FGC_REF_SINE    },
    { "REF.TEST.WINDOW",        &PROP_REF_TEST_WINDOW,          FGC_REF_SINE    },
    { "REF.TRIM.DURATION",      &PROP_REF_TRIM_DURATION,        FGC_REF_CTRIM   },
    { "REF.TRIM.FINAL",         &PROP_REF_TRIM_FINAL,           FGC_REF_CTRIM   },
};


// Static functions

int32_t equipdevStart(void)
{
    struct Equipdev_channel    *device;
    uint32_t                    channel;
    uint32_t                    user;

    // Initialise the standard equipdev services (hash tables, parser data, ...)

    equipdevInit(NUM_PARS_FUNC_91, NULL);

    // Initialise armed function generator parameter mutex

    if(fgcd_mutex_init(&equipdev.armed_fg_pars_mutex, EQUIPDEV_MUTEX_POLICY) != 0) return 1;

    // Set the number of elements in the non-PPM log

    equipdev.ppm[0].log.num_elements = FGC_LOG_LEN;

    // Initialise OASIS sampling intervals

    equipdev.oasis.i_meas_interval_ns = 1000000;
    equipdev.oasis.i_ref_interval_ns  = 1000000;

    // Initialise each device

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device              = &equipdev.device[channel];
        device->fgcd_device = &fgcd.device[channel];

        // Give values to test properties

        device->test.strings[0] = "FIRST";
        device->test.strings[1] = "SECOND";

        // Set start time to be same as FGCD start time

        device->time_start = fgcd.time_start;

        // Set default values

        if(device->fgcd_device->name)
        {
            device->beam_dump = FGC_CTRL_ENABLED;
        }
        else
        {
            device->beam_dump = FGC_CTRL_DISABLED;
        }

        device->economy.dest.mask   = UINT32_MAX;
        device->mode_op             = FGC_MODE_OP_NORMAL;
        device->meas.sim            = FGC_CTRL_ENABLED;

        // Set STATE.PC to OFF by default

        device->status.state_pc = FGC_PC_OFF;

        // Set reference limits

        device->ref_output.limits = &device->load[device->load_select].limits.i;

        // Initialise PPM data

        for(user = 0 ; user < NUM_CYC_SELECTORS ; user++)
        {
            // Initialise transaction data

            device->ppm[user].transaction_id        = 0;
            device->ppm[user].transaction_ref_type  = FGC_REF_NONE;
            device->ppm[user].transaction_state     = FGC_TRANSACTION_STATE_NONE;

            // Set table reference pointers

            device->ppm[user].transactional.fg_config.table.function = device->ppm[user].transactional.fg_config.table_arrays.function;
        }

        // Initialize RBI.81607 forced destination

        device->rbi81607_force = FGC_RBI81607_FORCE_NONE;
        device->rbi81607_timeout_remaining = 0;
        device->rbi81607_cycling = FGC_CTRL_DISABLED;

        if(device->fgcd_device->name && strcasestr(device->fgcd_device->name,"RBI.81607") != NULL)
        {
            device->rbi81607_enable = FGC_CTRL_ENABLED;
        }
        else
        {
            device->rbi81607_enable = FGC_CTRL_DISABLED;
        }

        // Load device configuration
        // We desable arming until the full configuration has been retrieved
        device->arming_enabled = false;

        nonvolLoad(&fgcd.device[channel], 0);
        nonvolLoad(&fgcd.device[channel], 1);

        device->arming_enabled = true;

        // Arm functions

        for(user = 1 ; user < NUM_CYC_SELECTORS ; user++)
        {
            if(equipdevArmCycle(channel, user, false) != 0 )
            {
                logPrintf(channel, "Failed to arm user %d\n",user);
            }
        }
    }

    // Survey power converter states

    stateSurvey(1, FGCD_MAX_EQP_DEVS);

    return 0;
}



void equipdevCleanUp(void)
{
    // Clear parser data

    parserClearClassData(EQUIPDEV_CLASS_ID);

    // Clean-up

    hashFree(equipdev.const_hash);
    hashFree(equipdev.prop_hash);

    // Destroy armed function generator parameter mutex

    pthread_mutex_destroy(&equipdev.armed_fg_pars_mutex);
}



void equipdevInitDevices(void)
{
    struct FGCD_device  *fgcd_device;
    uint32_t            i;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        fgcd_device = &fgcd.device[i];

        if(fgcd_device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set command queue

            fgcd_device->command_queue = &fgcddev.parser.command_queue;

            // Set device as online

            fgcd_device->online  = 1;
            fgcd_device->ready   = 1;
        }
    }
}



void equipdevPublish(void)
{
    struct Equipdev_channel         *equip_device;
    struct FGCD_device              *fgcd_device;
    uint32_t                        i;
    struct fgc_stat                 *pub_data;
    struct fgc91_stat               *pub_status;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        equip_device    = &equipdev.device[i];
        fgcd_device     = &fgcd.device[i];
        pub_data        = &pub.published_data.status[i];
        pub_status      = &pub_data->class_data.c91;

        if(fgcd_device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set class in published data

            pub_data->class_id = EQUIPDEV_CLASS_ID;

            // I_REF and V_REF are set in timingMillisecond() to ensure that
            // the values are taken for this millisecond rather than the next one

            // Set measurements in status

            equip_device->status.i_meas     = equip_device->meas.i_meas;
            equip_device->status.ref_meas   = equip_device->meas.ref_meas;

            // Copy device status into published data

            memcpy(pub_status,
                   &equip_device->status,
                   sizeof(equip_device->status));

            // Byte-swap statuses

            pub_status->st_faults       = htons(pub_status->st_faults);
            pub_status->st_warnings     = htons(pub_status->st_warnings);
            pub_status->st_unlatched    = htons(pub_status->st_unlatched);
            pub_status->i_ref           = htonf(pub_status->i_ref);
            pub_status->i_meas          = htonf(pub_status->i_meas);
            pub_status->v_ref           = htonf(pub_status->v_ref);
            pub_status->ref_meas        = htonf(pub_status->ref_meas);
        }
    }
}



void equipdevRtAutotune(bool log_events_flag)
{
    // Scan every device to see if autotune is enabled

    for(uint32_t channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        if (device.ppm[timing.user].autotune.enable == FGC_CTRL_ENABLED)
        {
            // Process autotune if MUGEF.AUTO_TUNE.ENABLE(user) is ENABLED

            // Increment autotune_time once the first injection occurs and until the start of the next cycle
            // or max injections have occured

            if ( fgcddev.count_injections > 0  &&
                 fgcddev.count_injections <= FGC_AUTOTUNE_MAX_INJ)
            {
                // Set delta current reference from MUGEF.AUTO_TUNE.STEPS property

                float delta = device.ppm[timing.user].autotune.steps[fgcddev.count_injections-1];

                if (device.autotune_time == 0)
                {
                    // When time is zero, apply the full delta in one jump

                    device.ref.rt += delta;

                    if(log_events_flag)
                    {
                        logPrintf(channel,"TIMING autotune %u for user %u. Added %.5f to ref.rt %.5f\n"
                               , fgcddev.count_injections
                               , timing.user
                               , delta
                               , device.ref.rt);
                    }
                }
            }

            // Increment the autotune time

            device.autotune_time++;
        }
    }
}



void equipdevCheckLimits(void)
{
    float                   absolute_error;
    struct Equipdev_channel *device;
    uint32_t                i;

    // Set maximum absolute error for each device

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        device = &equipdev.device[i];

        // Set maximum absolute error

        if(device->status.state_pc >= FGC_PC_ON_STANDBY)
        {
            absolute_error = fabs(device->ref_buffer[equipdev.ref_buf_idx].i - device->meas.i_meas);

            // Set non-PPM error

            if(absolute_error > device->ppm[0].max_abs_err)
            {
                device->ppm[0].max_abs_err = absolute_error;
            }

            // Set PPM error

            if(timing.cycle_ms == meas.delay_ms) // Start of new measurement cycle
            {
                if(timing.user)
                {
                    device->ppm[timing.user].max_abs_err = absolute_error;
                }
            }
            else if(timing.cycle_ms < meas.delay_ms) // Measurement is from previous cycle
            {
                if(timing.prev_user && absolute_error > device->ppm[timing.prev_user].max_abs_err)
                {
                    device->ppm[timing.prev_user].max_abs_err = absolute_error;
                }
            }
            else // (timing.cycle_ms > meas.delay_ms) // Measurement is from current cycle
            {
                if(timing.user && absolute_error > device->ppm[timing.user].max_abs_err)
                {
                    device->ppm[timing.user].max_abs_err = absolute_error;
                }
            }
        }

        // Check whether measured current is outside of allowed operating range

        if(device->status.state_pc >= FGC_PC_ON_STANDBY &&
           !(device->status.st_faults & FGC_FLT_LIMITS))
        {
            // Check limits appropriately

            if(device->pol_switch_state == FGC_POL_NEGATIVE) // Device has a polarity switch that is negative
            {
                if(-device->meas.i_meas > (device->load[device->load_select].limits.i.pos * 1.01))
                {
                    device->status.st_faults |= FGC_FLT_LIMITS;
                }
            }
            else if(device->meas.i_meas > (device->load[device->load_select].limits.i.pos * 1.01) ||
                    (device->load[device->load_select].limits.i.neg < 0 &&
                     device->meas.i_meas < (device->load[device->load_select].limits.i.neg * 1.01)))
            {
                device->status.st_faults |= FGC_FLT_LIMITS;
            }

            // Stop the converter if there is a limits fault

            if(device->status.st_faults & FGC_FLT_LIMITS)
            {
                logPrintf(i, "EQUIPDEV Limits fault set (I_REF=%.7E, I_MEAS=%.7E)\n",
                          device->ref.i, device->meas.i_meas);
                stateRequestStop(i);
            }
        }
    }
}



void equipdevConfigured(struct Equipdev_channel *device)
{
    // Initialise PLEP function parameters to defaults

    struct fg_plep_config *plep_config = &device->ppm[0].transactional.plep;

    plep_config->acceleration = device->load[device->load_select].fg.defaults.i.acceleration;
    plep_config->linear_rate  = device->load[device->load_select].fg.defaults.i.linear_rate;
}



void equipdevUpdateLog(void)
{
    struct Equipdev_channel *device;
    uint32_t                i;
    static uint32_t         log_cycle_index         = 0;    // Index within log buffers of start of cycle that is currently being logged
    static uint32_t         log_cycle_num_elements  = 0;    // Number of elements for the cycle that is currently being logged
    static struct timeval   log_new_cycle_acqstamp;         // Timestamp of the start of the acquisition for the new cycle to be logged after the measurement delay
    static struct timeval   log_cycle_acqstamp;             // Timestamp of the start of the acquisition for the cycle that is currently being logged
    static uint32_t         log_user                = 0;    // User for logged data (this may be delayed with respect to the timing user)
    uint32_t                user;

    // Store log acquisition timestamp at the beginning of the calculated cycle

    if(timing.cycle_ms == 0)
    {
        timingGetUserTime(0, &log_new_cycle_acqstamp);
    }

    // Check whether this is the start of a new logging cycle

    if(timing.cycle_ms == meas.delay_ms) // Start of a new cycle for logging
    {
        // Set parameters for completed cycle

        if(log_user)
        {
            // Check whether start of cycle is in log

            if(log_cycle_num_elements <= FGC_LOG_LEN) // Full cycle is in log
            {
                equipdev.ppm[log_user].log.index            = log_cycle_index;
                equipdev.ppm[log_user].log.num_elements     = log_cycle_num_elements;
            }
            else // Start of cycle was before beginning of log
            {
                // Set number of elements to zero to indicate no data

                equipdev.ppm[log_user].log.num_elements = 0;
            }

            // Set timestamps

            equipdev.ppm[log_user].log.timestamp = log_cycle_acqstamp;
            timingGetUserTime(log_user, &equipdev.ppm[log_user].log.cycle_timestamp);

            // Send notifications that properties have been updated

            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.DAC.MEAS", log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.DAC.REF",  log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.DAC",      log_user, 0, equipdev.ppm[log_user].log.num_elements, 1, 0);

            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.I.MEAS",   log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.I.REF",    log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.I",        log_user, 0, equipdev.ppm[log_user].log.num_elements, 1, 0);

            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_MEAS.DATA",  log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_MEAS",       log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_REF.DATA",   log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_REF",        log_user, 0, equipdev.ppm[log_user].log.num_elements, 0, 0);
        }

        // Set parameters to start logging new cycle

        log_cycle_acqstamp      = log_new_cycle_acqstamp;
        log_user                = timing.user;
        log_cycle_index         = equipdev.ppm[0].log.index;
        log_cycle_num_elements  = 0;
    }

    // Check whether the start of a cycle is about to be overwritten

    for(user = 1 ; user < NUM_CYC_SELECTORS ; user++)
    {
        if(equipdev.ppm[user].log.index == equipdev.ppm[0].log.index)
        {
            // Set number of elements to zero to indicate no data

            equipdev.ppm[user].log.num_elements = 0;

            // Only a single user can become older than the last log entry
            // each millisecond, so stop searching

            break;
        }
    }

    // Update logged data for each device

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        device = &equipdev.device[i];

        device->log[equipdev.ppm[0].log.index].i_ref    = device->ref_buffer[equipdev.ref_buf_idx].i;
        device->log[equipdev.ppm[0].log.index].i_meas   = device->meas.i_meas;
        device->log[equipdev.ppm[0].log.index].ref_meas = device->meas.ref_meas;
    }

    // Increment number of elements in cycle currently being acquired

    log_cycle_num_elements++;

    // Set non-PPM index to oldest entry in log buffer

    equipdev.ppm[0].log.index = (equipdev.ppm[0].log.index + 1) % FGC_LOG_LEN;
}



void equipdevRecoverRefSettings(struct Equipdev_channel *device, uint32_t user)
{
    struct Equipdev_fg_ref *config = &device->ppm[user].transactional.fg_config;

    // We don't keep errors that might have happened during mounting of reference, so that
    // we can se what went wrong after recovering the previous settings

    struct fg_meta_error fg_error = config->meta_data.error;

    // Recover reference settings

    *config = device->ppm[user].fg.armed;

    // Recover errors

    config->meta_data.error = fg_error;

    // Set table reference array pointers

    config->table.function = config->table_arrays.function;

    if(config->ref_type == FGC_REF_TABLE)
    {
        config->pars.table.function = config->table_arrays.function;
    }
}



fgc_errno equipdevArmCycle(uint32_t channel, uint32_t cycle_sel, bool test_only)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    struct Equipdev_fg_ref  *fg_config           = &device.ppm[cycle_sel].transactional.fg_config;
    struct Equipdev_fg_ref  *fg_armed            = &device.ppm[cycle_sel].fg.armed;
    enum fg_error            fg_error            = FG_OK;

    enum fg_limits_polarity  limits_polarity;

    // Set polarity in limits

    if(device.pol_switch_timeout && cycle_sel)
    {
        limits_polarity = FG_LIMITS_POL_AUTO;
    }
    else if(!device.pol_switch_timeout || device.pol_switch_state == FGC_POL_POSITIVE)
    {
        limits_polarity = FG_LIMITS_POL_NORMAL;
    }
    else if(device.pol_switch_state == FGC_POL_NEGATIVE) // Device has a polarity switch with a negative polarity
    {
        limits_polarity = FG_LIMITS_POL_NEGATIVE;
    }
    else // Device has a polarity switch that is neither positive nor negative
    {
        return FGC_BAD_STATE;
    }

    // Initialise reference

    switch(fg_config->ref_type)
    {
        case FGC_REF_CTRIM:
        case FGC_REF_LTRIM:

            // Reference type can only be set for non-PPM users

            if(cycle_sel) // Command is PPM
            {
                return FGC_BAD_PARAMETER;
            }

            // Set trim type

            fg_config->trim.type = fg_config->ref_type == FGC_REF_CTRIM ? FG_TRIM_CUBIC : FG_TRIM_LINEAR;

            fg_error = fgTrimArm(&device.load[device.load_select].limits.i,
                                 limits_polarity,
                                 &fg_config->trim,
                                 fg_config->run_delay,
                                 device.ref.fg,
                                 &fg_config->pars.trim,
                                 &fg_config->meta_data);
            break;

        case FGC_REF_COSINE:
        case FGC_REF_SINE:
        case FGC_REF_SQUARE:
        case FGC_REF_STEPS:

            // Reference type can only be set for non-PPM users

            if(cycle_sel) // Command is PPM
            {
                return FGC_BAD_PARAMETER;
            }

            switch(fg_config->ref_type)
            {
                case FGC_REF_COSINE:
                    device.ppm[0].transactional.test.type = FG_TEST_COSINE;
                    break;

                case FGC_REF_SINE:
                    device.ppm[0].transactional.test.type = FG_TEST_SINE;
                    break;

                case FGC_REF_SQUARE:
                    device.ppm[0].transactional.test.type = FG_TEST_SQUARE;
                    break;

                case FGC_REF_STEPS:
                    device.ppm[0].transactional.test.type = FG_TEST_STEPS;
                    break;
            }

            fg_error = fgTestArm(&device.load[device.load_select].limits.i,
                                 limits_polarity,
                                 &device.ppm[0].transactional.test,
                                 fg_config->run_delay,
                                 device.ref.fg,
                                 &fg_config->pars.test,
                                 &fg_config->meta_data);
            break;

        case FGC_REF_NONE:

            // Reference type can only be set for PPM users

            if(!cycle_sel) // Command is non-PPM
            {
                return FGC_BAD_PARAMETER;
            }
            break;

        case FGC_REF_NOW:
        case FGC_REF_PLEP:

            // Reference type can only be set for non-PPM users

            if(cycle_sel) // Command is PPM
            {
                return FGC_BAD_PARAMETER;
            }

            fg_error = fgPlepArm(&device.load[device.load_select].limits.i,
                                 limits_polarity,
                                 &device.ppm[0].transactional.plep,
                                 fg_config->run_delay,
                                 device.ref.fg,
                                 &fg_config->pars.plep,
                                 &fg_config->meta_data);
            break;

        case FGC_REF_TABLE:

            fg_config->pars.table.function = fg_config->table_arrays.function;

            fg_error = fgTableArm(&device.load[device.load_select].limits.i,
                                  limits_polarity,
                                  &fg_config->table,
                                  fg_config->run_delay,
                                  FUNCGEN_PERIOD,
                                  &fg_config->pars.table,
                                  &fg_config->meta_data);

            break;

        default:
            return FGC_NOT_IMPL;
    }

    // Handle errors from function generation library

    switch(fg_error)
    {
        case FG_OK:                         break;
        case FG_BAD_ARRAY_LEN:              return FGC_BAD_ARRAY_LEN;
        case FG_BAD_PARAMETER:              return FGC_BAD_PARAMETER;
        case FG_INVALID_TIME:               return FGC_INVALID_TIME;
        case FG_OUT_OF_ACCELERATION_LIMITS: return FGC_OUT_OF_ACCELERATION_LIMITS;
        case FG_OUT_OF_LIMITS:              return FGC_OUT_OF_LIMITS;
        case FG_OUT_OF_RATE_LIMITS:         return FGC_OUT_OF_RATE_LIMITS;
        case FG_OUT_OF_VOLTAGE_LIMITS:      return FGC_OUT_OF_VOLTAGE_LIMITS;
        default:                            return FGC_UNKNOWN_ERROR_CODE;
    }

    // Stop if this is just a test

    if(test_only)
    {
        return FGC_OK_NO_RSP;
    }

    // Arm reference

    pthread_mutex_lock(&equipdev.armed_fg_pars_mutex);

    *fg_armed = *fg_config;

    // Set table reference array pointers and store in non-volatile

    if(fg_config->ref_type == FGC_REF_TABLE)
    {
        fg_armed->pars.table.function = fg_armed->table_arrays.function;

    }
    else
    {
        // Reset the length of REF.ARMED.TABLE.FUNCTION

        fg_armed->table.n_elements = 0;
    }

    pthread_mutex_unlock(&equipdev.armed_fg_pars_mutex);

    // Notify the properties for the armed reference

    cmwpubNotify(device.fgcd_device->id, "REF.ARMED.TABLE.FUNCTION", cycle_sel, 0, 0, 0, 0);
    cmwpubNotify(device.fgcd_device->id, "REF.ARMED.TABLE",          cycle_sel, 0, 0, 0, 0);

    // Store PPM reference properties in non-volatile storage

    if(cycle_sel)
    {
        for(uint32_t i = 0 ; i < (sizeof(property_ref_type_mapping) / sizeof(property_ref_type_mapping[0])) ; i++)
        {
            auto mapping = &property_ref_type_mapping[i];

            // Store the property's value in non-volatile storage if property is NON_VOLATILE and
            // its reference type matches that of the armed function or is NONE (non-type specific)

            if((mapping->property->flags & PF_NON_VOLATILE) &&
               (mapping->ref_type == FGC_REF_NONE || mapping->ref_type == fg_config->ref_type))
            {
                // Store the value in non-volatile storage

                nonvolStore(&fgcd.device[channel], mapping->property_name, cycle_sel,
                            (mapping->property->flags & PF_NON_VOL_LAST) ? 1 : 0,
                            (mapping->property->flags & PF_CONFIG) ? 1 : 0);
            }
        }
    }
    else
    {
        // Change state to ARMED if command is non-ppm

        device.fg.running_non_cyc.meta_data    = fg_armed->meta_data;
        device.fg.ref_run.tv_sec               = LONG_MAX;
        device.fg.ref_remaining                = device.fg.running_non_cyc.meta_data.duration;

        stateSetPC(device.fgcd_device->id, FGC_PC_ARMED, "ARMED");

        // Start reference immediately if it is of type NOW

        if(fg_config->ref_type == FGC_REF_NOW)
        {
            device.fg.ref_run.tv_sec = 0;
        }
    }

    return FGC_OK_NO_RSP;
}



void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property)
{
    uint32_t channel = command->device->id;
    Equipdev_channel *device = &equipdev.device[channel];

    // Disarm the reference if the command is non-PPM

    if(command->user == 0 && device->status.state_pc == FGC_PC_ARMED)
    {
        stateSetPC(command->device->id, FGC_PC_IDLE, "IDLE");
    }

    // Search for a matching property

    for(uint32_t i = 0 ; i < (sizeof(property_ref_type_mapping) / sizeof(property_ref_type_mapping[0])) ; i++)
    {
        if(property_ref_type_mapping[i].property == property)
        {
            // Set the transaction reference type to that of the matched property

            device->ppm[command->user].transaction_ref_type = property_ref_type_mapping[i].ref_type;
            return;
        }
    }

    logPrintf(channel, "EQUIPDEV equipdevTransactionalPropertySet: failed to find mapping for property at %p (symbol=%s)\n",
              property, SYM_TAB_PROP[property->sym_idx].key.c);
}



void equipdevTransactionNotify(uint32_t channel, uint32_t user)
{
    // Publish all transactional properties for the user

    for(uint32_t i = 0 ; i < (sizeof(property_ref_type_mapping) / sizeof(property_ref_type_mapping[0])) ; i++)
    {
        cmwpubNotify(channel, property_ref_type_mapping[i].property_name, user, 0, 0, 1, CMWPUB_UPDATE_FLAGS_IMMEDIATE);
    }
}



fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time)
{
    Equipdev_channel *device = &equipdev.device[channel];
    fgc_errno error = FGC_OK_NO_RSP;

    // Set the reference type

    if(device->ppm[user].transaction_ref_type != FGC_REF_NONE)
    {
        device->ppm[user].transactional.fg_config.ref_type = device->ppm[user].transaction_ref_type;
        device->ppm[user].transaction_ref_type = FGC_REF_NONE;
    }

    // Handle PPM and non-PPM commits

    // PPM commits arm a reference.
    // Non-PPM commits start an already armed reference.

    if(user)
    {
        // Arm the cycle

        error = equipdevArmCycle(channel, user, false);
    }
    else // Non-PPM
    {
        // If the device is in the ARMED state, start the reference at the specified time

        if(device->status.state_pc == FGC_PC_ARMED)
        {
            // Trigger the start of the reference at the appropriate time

            device->fg.ref_run = *time;
        }
        else
        {
            // Not in ARMED state so return BAD STATE

            error = FGC_BAD_STATE;
        }
    }

    return error;
}



//! Rollback transactional settings for a user on a device

void equipdevTransactionRollback(uint32_t channel, uint32_t user)
{
    Equipdev_channel *device = &equipdev.device[channel];

    // Clear the transaction reference type

    device->ppm[user].transaction_ref_type = FGC_REF_NONE;

    // If non-PPM and ARMED, then disarm

    if(user == 0 && device->status.state_pc == FGC_PC_ARMED)
    {
        stateSetPC(channel, FGC_PC_IDLE, "IDLE");
    }
}



fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user)
{
    Equipdev_channel *device = &equipdev.device[channel];

    // Set the reference type

    if(device->ppm[user].transaction_ref_type != FGC_REF_NONE)
    {
        device->ppm[user].transactional.fg_config.ref_type = device->ppm[user].transaction_ref_type;
    }

    // Non-PPM test is only allowed in IDLE state

    if(user == 0 && device->status.state_pc != FGC_PC_IDLE)
    {
        return FGC_BAD_STATE;
    }

    // Test arm the cycle or actually arm it if non-PPM

    return equipdevArmCycle(channel, user, user == 0 ? false : true);
}

// EOF
