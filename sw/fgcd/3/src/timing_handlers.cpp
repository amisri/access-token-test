/*
 *  Filename: timing_handlers.c
 *
 *  Purpose:  Functions for handling timing events
 */

// __STDC_LIMIT_MACROS must be defined in C++ to make stdint.h define macros such as UINT32_MAX

#define __STDC_LIMIT_MACROS

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <consts.h>
#include <equipdev.h>
#include <fgcddev.h>

#include <classes/91/defconst.h>
#include <cmwpub.h>
#include <fei.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <funcgen.h>
#include <logging.h>
#include <prop_equip.h>
#include <meas.h>
#include <pub.h>
#include <ref.h>
#include <reg.h>
#include <rt.h>
#include <state.h>
#include <timing.h>
#include <timing_handlers.h>



void timingAddMultiPpmEvent(const char *mppm_condition)
{
    ; // Multi-PPM not supported in this class
}



void timingTelegramReady(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static uint32_t old_cyclenum = timing.field_value[TIMING_FIELD_CYCLE_NUM];
    static Timing::Value value;

    // Only SPS timing allowed for class 3

    if (fgcd.machine != MACHINE_SPS)
    {
        return;
    }

    // Read all the relevant fields for the basic period event

    // CYCLE_NB (aka CYCLENUM)

    try
    {
        evt_value->getFieldValue("CYCLE_NB", &value);
        timing.field_value[TIMING_FIELD_CYCLE_NUM] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag && old_cyclenum != timing.field_value[TIMING_FIELD_CYCLE_NUM])
        {
            logPrintf(0,"Event %s Field CYCLE_NB (aka CYCLENUM) = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_CYCLE_NUM]);

            old_cyclenum = timing.field_value[TIMING_FIELD_CYCLE_NUM];
        }

        // Set cycle number

        timing.cycle_num = timing.field_value[TIMING_FIELD_CYCLE_NUM] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // INJECTED_BATCHES

    try
    {
        evt_value->getFieldValue("INJECTED_BATCHES", &value);

        timing.field_value[TIMING_FIELD_INJECTED_BATCHES] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag && old_cyclenum != timing.field_value[TIMING_FIELD_CYCLE_NUM])
        {
            logPrintf(0,"Event %s Field INJECTED_BATCHES (aka NUMBER INJECTIONS) = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_INJECTED_BATCHES]);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // MACHINE_MODE

    try
    {
        evt_value->getFieldValue("MACHINE_MODE", &value);
        timing.field_value[TIMING_FIELD_MACHINE_MODE] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field MACHINE_MODE = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.field_value[TIMING_FIELD_MACHINE_MODE]);
        }

    }
    catch(std::exception& e)
    {
        //Nothing to be done
    }
}

static void timingStartCycle(void)
{
    uint32_t                 channel;
    struct Equipdev_channel *device;

    // Reset the injection counter and number every start of the cycle

    fgcddev.count_injections = 0;

    // Reset the real time reference

    pthread_mutex_lock(&rt.mutex);

    for(channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        equipdev.device[channel].ref.rt = 0;
    }

    pthread_mutex_unlock(&rt.mutex);

    // Reset FEI counter

    equipdev.ppm[timing.user].fei.num_occurrences = 0;

    // RBI.81607 Thyristor switch selection

    for(channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        device = &(equipdev.device[channel]);

        // Check if the control of the Thyristor switches if enabled and converter in cycling or idle

        if(device->rbi81607_enable == FGC_CTRL_ENABLED)
        {
            if(device->load_switch2_state == FGC_POL_NEGATIVE &&
                (timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] == 7 || timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] == 5))
            {
                // Load switch 2 to the charge, and destination to LHC2_TI8 or TI8_DUMP

                device->rbi81607_cycling           = FGC_CTRL_ENABLED;
                device->rbi81607_timeout_remaining = RBI81607_PULSE_MS;

                timingSetCtrOuput(RBI81607_OUT_LHC,1);
                timingSetCtrOuput(RBI81607_OUT_AWAKE,0);
            }
            else if (device->load_switch1_state == FGC_LOAD_SWITCH_LOAD_1 &&
                     timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] == 2)
            {
                // Load switch 1 to the charge, and dstination to AWAKE

                device->rbi81607_cycling           = FGC_CTRL_ENABLED;
                device->rbi81607_timeout_remaining = RBI81607_PULSE_MS;

                timingSetCtrOuput(RBI81607_OUT_LHC,0);
                timingSetCtrOuput(RBI81607_OUT_AWAKE,1);
            }
            else if (device->load_switch2_state == FGC_POL_NEGATIVE &&
                timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] == 1 &&
                strcasestr(timingUserName(timing.user),"LHC") != NULL &&
                device->rbi81607_force     == FGC_RBI81607_FORCE_LHC)
            {
                // Load switch 2 to the charge, dynamic destination to SPS DUMP, destination to LHC, and output forced to LHC

                device->rbi81607_cycling           = FGC_CTRL_ENABLED;
                device->rbi81607_timeout_remaining = RBI81607_PULSE_MS;

                timingSetCtrOuput(RBI81607_OUT_LHC,1);
                timingSetCtrOuput(RBI81607_OUT_AWAKE,0);

            }
            else if (device->load_switch1_state == FGC_LOAD_SWITCH_LOAD_1 &&
                timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] == 1 &&
                strcasestr(timingUserName(timing.user),"AWAKE") != NULL &&
                device->rbi81607_force     == FGC_RBI81607_FORCE_AWAKE)
            {
                // Load switch 2 to the charge, dynamic destination to SPS DUMP, destination AWAKE, and output forced to AWAKE

                device->rbi81607_cycling           = FGC_CTRL_ENABLED;
                device->rbi81607_timeout_remaining = RBI81607_PULSE_MS;

                timingSetCtrOuput(RBI81607_OUT_LHC,0);
                timingSetCtrOuput(RBI81607_OUT_AWAKE,1);
            }
            else
            {
                // Otherwise force outputs to 0

                device->rbi81607_cycling           = FGC_CTRL_DISABLED;
                device->rbi81607_timeout_remaining = 0;

                timingSetCtrOuput(RBI81607_OUT_LHC,0);
                timingSetCtrOuput(RBI81607_OUT_AWAKE,0);

            }
        }
        else if (device->rbi81607_enable == FGC_CTRL_ENABLED)
        {
            // The device is th eRBI.81607 but is not cycling or idle

            device->rbi81607_cycling           = FGC_CTRL_DISABLED;
            device->rbi81607_timeout_remaining = 0;

            timingSetCtrOuput(RBI81607_OUT_LHC,0);
            timingSetCtrOuput(RBI81607_OUT_AWAKE,0);
        }
    }
}


/*
 * Calculate cycle timing
 */

static void timingCalcCycle(void)
{
    struct timeval time;

    // Decrement time until the next cycle

    if(timing.next_cycle_countdown_ms != UINT32_MAX)
    {
        timing.next_cycle_countdown_ms--;
    }

    // Check whether this is millisecond zero of a new cycle

    if(!timing.next_cycle_countdown_ms) // New cycle
    {
        timing.cycle_ms = 0;

        // Set user

        timing.prev_user    = timing.user;
        timing.user         = timing.next_user;
        timing.next_user    = 0;

        timing.next_ms_cycle_ms = timing.user ? 1 : 0; // Fix next cycle ms at 0 until the user is known
        timing.next_ms_user     = timing.user;

        // Zero the time for the user

        time.tv_sec = 0;
        time.tv_usec = 0;
        timingSetUserTime(timing.user, &time);

        // Handle the start of the cycle

        timingStartCycle();

        // Log the start of the cycle if logging of timing events is enabled

        if(timing.log_events_flag)
        {
            logPrintf(0, "TIMING Expected Start cycle, user %s (%u)\n", timingUserName(timing.user), timing.user);
        }

    }
    else // This is not the first millisecond of a new cycle
    {
        // Increment millisecond within cycle if user is set

        if(timing.user)
        {
            timing.cycle_ms++;
        }

        // Set millisecond and user for next millisecond

        if(timing.next_cycle_countdown_ms == 1) // Last millisecond of a cycle
        {
            // Log the end of the cycle if logging of timing events is enabled

            if(timing.log_events_flag)
            {
                logPrintf(0, "TIMING Expected Cycle last ms, user %hhu, ms %u\n", timing.user, timing.cycle_ms);
            }

            timing.next_ms_cycle_ms = 0;
            timing.next_ms_user     = timing.next_user;
        }
        else // This is not the last millisecond of a cycle
        {
            timing.next_ms_cycle_ms = timing.user ? timing.cycle_ms + 1 : 0;
            timing.next_ms_user     = timing.user;
        }
    }
}


/*
 * Calculate FEI timing
 */

static void timingCalcFei(void)
{
    // Decrement time until the next cycle

    if(timing.next_fei_countdown_ms != UINT32_MAX)
    {
        timing.next_fei_countdown_ms--;
    }

    // Check whether the FEI event has expried

    if(!timing.next_fei_countdown_ms)
    {
        // Do nothing if the maximum number of FEI events have already occurred for this cycle

        if(equipdev.ppm[timing.user].fei.num_occurrences == FGC_FEI_MAX_OCCURRENCES)
        {
            logPrintf(0, "TIMING Maximum number of FEI events %u exceeded for user %u\n",
                      FGC_FEI_MAX_OCCURRENCES, timing.user);
            return;
        }

        // Perform FEI check

        feiCheck();

        // Increment number of FEI occurrences for this cycle

        equipdev.ppm[timing.user].fei.num_occurrences++;
    }
}


void timingMillisecond(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;
    uint32_t                ms = evt_time.time.tv_nsec / 1000000;   // Millisecond within UTC second
    struct timeval          time;

    // Set time

    time.tv_sec  = evt_time.time.tv_sec;
    time.tv_usec = ms * 1000;
    timingSetUserTime(0, &time);

    // Calculate cycle timing

    timingCalcCycle();

    // Calculate FEI timing

    timingCalcFei();

    // Call parameter functions for each device

    SetParameters();

    // Acquire measurements

    measAcquire();

    // Update real-time reference for autotune

    pthread_mutex_lock(&rt.mutex);
    equipdevRtAutotune(timing.log_events_flag);
    pthread_mutex_unlock(&rt.mutex);

    // Check limits

    equipdevCheckLimits();

    // Update logged data

    equipdevUpdateLog();

    // In the second millisecond of the real accelerator cycle, store the real timestamp of the current cycle.
    // Note that if the system uses an offset cycle (e.g. in TT10), then there may be a period at the start of
    // the cycle when the cyclestamp is unavailable.

    if(evt_value->getCycleTime() == 2)
    {
        tTimingTime cycle_timestamp = evt_value->getCycleTimestamp();
        struct timeval user_time;

        user_time.tv_sec  = cycle_timestamp.time.tv_sec;
        user_time.tv_usec = cycle_timestamp.time.tv_nsec / 1000;

        // Store the time at which the user started

        if(timing.user)
        {
            timingSetUserTime(timing.user, &user_time);
        }
    }

    // Set published status before it is updated for next millisecond

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Update published references

        device->status.i_ref = device->ref_buffer[equipdev.ref_buf_idx].i;
    }

    // Calculate function generator references for next millisecond

    funcgenCalcRefs();

    // Update real-time references if this is the millisecond before a FGCD_CYCLE_PERIOD_MS boundary

    if(ms % FGCD_CYCLE_PERIOD_MS == (FGCD_CYCLE_PERIOD_MS - 1))
    {
        pthread_mutex_lock(&rt.mutex);

        // Set real-time reference for each device

        for(channel = 0 ; channel < FGCD_MAX_EQP_DEVS ; channel++)
        {
            // Check whether there is a new RT reference for the device

            if(rt.data[rt.data_start].active_channels & (1LLU << channel)) // Channel is active
            {
                device = &equipdev.device[channel + 1];

                // Set new value

                device->ref.rt = rt.used_data.channels[channel] = rt.data[rt.data_start].channels[channel];
            }
        }

        // Clear active channels for this reference and advance data start

        rt.data[rt.data_start].active_channels  = 0;
        rt.data_start                           = (rt.data_start + 1) % RT_BUFFER_SIZE;

        pthread_mutex_unlock(&rt.mutex);
    }

    // Set regulation reference and dref/dt for each device

    regRegulate();

    // Update references to be applied in the next millisecond

    refUpdateRefs();

    // Check timing status and set fault if necessary

    timingCheckStatus();

    // Check whether this is the first millisecond of a new FGCD cycle

    if(!(ms % FGCD_CYCLE_PERIOD_MS))
    {
        // Timestamp published data

        pub.published_data.time_sec  = htonl(time.tv_sec);
        pub.published_data.time_usec = htonl(time.tv_usec);

        // Survey power converter states and process state commands in chunks of 4 devices per millisecond to avoid overrunning

        // channels_per_group must be a multiple integer of FGCD_MAX_EQP_DEVS
        static uint32_t const channels_per_group = 4;
        static uint32_t       channel_group      = 0;

        uint32_t ms_group      = channel_group++ % (FGCD_MAX_EQP_DEVS / channels_per_group);
        uint32_t start_channel = (ms_group * channels_per_group) + 1;
        uint32_t end_channel   = ((ms_group + 1) * channels_per_group);

        // Survey power converter states

        stateSurvey(start_channel, end_channel);

        // Process state commands

        stateProcessCmds();
    }

    // Check whether an FEI card is present

    if(fei.card.regs)
    {
        // Set FEI beam dump counter

        if(!(fgcddev.status.st_faults   & FGC_FLT_TIMING) && // No timing fault
           !(fgcddev.status.st_warnings & FGC_WRN_TIMING) && // No timing warning
           !equipdev.beam_dump_request) // No beam dump request
        {
            fei.card.regs->beam_dump[0] = htons(20); // 2ms
        }
        else // Request beam dump
        {
            fei.card.regs->beam_dump[0] = 0;
        }
    }

    // Check switch time-outs

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Check whether a load switch 1 time-out is active

        if(device->load_switch1_timeout_remaining)
        {
            // Check whether mode matches requested

            if(device->load_switch1_state == device->load_switch1_request)
            {
                device->load_switch1_timeout_remaining = 0;
            }
            else // Switch is not in requested position
            {
                if(!--device->load_switch1_timeout_remaining)
                {
                    logPrintf(channel, "STATE load switch 1 time-out\n");
                    device->status.st_faults |= FGC_FLT_SWITCH;
                    cmwpubNotify(channel, "STATUS.FAULTS", 0, 0, 0, 1, 0);
                }
            }
        }

        // Check whether the load switch 2 time-out is active

        if(device->load_switch2_timeout_remaining)
        {
            // Check whether mode matches requested

            if(device->load_switch2_state == device->load_switch2_request)
            {
                device->load_switch2_timeout_remaining = 0;
            }
            else // Switch is not in requested position
            {
                if(!--device->load_switch2_timeout_remaining)
                {
                    logPrintf(channel, "STATE load switch 2 time-out\n");
                    device->status.st_faults |= FGC_FLT_SWITCH;
                    cmwpubNotify(channel, "STATUS.FAULTS", 0, 0, 0, 1, 0);
                }
            }
        }

        // Check whether a polarity switch time-out is active

        if(device->pol_switch_timeout_remaining)
        {
            // Check whether polarity matches requested

            if(device->pol_switch_state == device->pol_switch_request)
            {
                device->pol_switch_timeout_remaining = 0;
            }
            else // Switch is not in requested position
            {
                if(!--device->pol_switch_timeout_remaining)
                {
                    logPrintf(channel, "STATE polarity switch time-out\n");
                    device->status.st_faults |= FGC_FLT_SWITCH;
                    cmwpubNotify(channel, "STATUS.FAULTS", 0, 0, 0, 1, 0);
                }
            }
        }
    }

    // Check RBI.81607 CTR output pulse timeouts

    for(channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        device = &(equipdev.device[channel]);

        // Reset the CTR outputs if the thrystor switch is enabled

        if(device->rbi81607_enable == FGC_CTRL_ENABLED && device->rbi81607_timeout_remaining)
        {
            if(!--device->rbi81607_timeout_remaining)
            {
                timingSetCtrOuput(RBI81607_OUT_LHC,0);
                timingSetCtrOuput(RBI81607_OUT_AWAKE,0);
            }
        }
    }

    // Publish data meas.delay_ms after start of FGCD cycle

    if(ms % FGCD_CYCLE_PERIOD_MS == meas.delay_ms)
    {
        // Update published data for FGCD device

        fgcddevPublish();

        // Update published data for equipment devices

        equipdevPublish();

        // Trigger sending of published data

        pubTrigger();
    }

    // Notify that measurement and reference properties have been updated

    if(ms % 200 == meas.delay_ms) // Every 200ms
    {
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS.I.VALUE",   0, 0, 0, 0, 0);
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS.REF",       0, 0, 0, 0, 0);
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS",           0, 0, 0, 0, 0);
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "REF.I",          0, 0, 0, 1, 0);
    }
}

void timingStartRef(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;
    struct timeval          *ref_run;
    uint32_t                event_group = 0;
    static Timing::Value    value;


    // EVENT_GROUP

    try
    {
        evt_value->getFieldValue("EVENT_GROUP", &value);
        event_group = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field EVENT_GROUP = %u\n",
                evt_value->getName().c_str(),
                event_group);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // Run references for all devices that are armed for the received event group

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Check whether the device is in the ARMED state

        if(device->status.state_pc == FGC_PC_ARMED)
        {
            // Check whether the event_group is a broadcast or corresponds to the device's event group

            if(event_group == 0 || event_group == device->fg.ref_event_group)
            {
                ref_run = &device->fg.ref_run;

                // Initialise the run time to the event time

                ref_run->tv_sec  = evt_time.time.tv_sec;
                ref_run->tv_usec = evt_time.time.tv_nsec / 1000;

                // Advance the run time by the event delay

                ref_run->tv_usec += info.delay_ms * 1000;

                // Check whether a second boundary was passed

                if(ref_run->tv_usec >= 1000000)
                {
                    ref_run->tv_sec++;
                    ref_run->tv_usec -= 1000000;
                }
            }
        }
    }
}



void timingFEI(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Start the countdown to the start of the cycle
    // If the delay is 0, add 1 ms for timingCalcFei() to work

    timing.next_fei_countdown_ms = info.delay_ms == 0 ? 1 : info.delay_ms;
}



void timingCycleWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    // Start the countdown to the start of the cycle

    timing.next_cycle_countdown_ms = info.delay_ms;

    // USER

    try
    {
        evt_value->getFieldValue("USER", &value);

        uint32_t next_user = static_cast<uint32_t>(value.getAsUshort());
        if(!next_user) return;

        timing.next_user = next_user;

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field USER = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.next_user);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // PROG_DEST

    try
    {
        evt_value->getFieldValue("PROG_DEST", &value);
        timing.field_value[TIMING_FIELD_NEXT_DEST] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field PROG_DEST = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.field_value[TIMING_FIELD_NEXT_DEST]);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }
}



void timingInjectionWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t channel;

    // If TEST.UINT8[0] is non-zero or INJECTED BATCHES from timing telegram is greater than
    // the number of injections so far this cycle

    if (   fgcddev.test_int8u[0]
        || timing.field_value[TIMING_FIELD_INJECTED_BATCHES] > fgcddev.count_injections)
    {
        // increase the injection count

        ++fgcddev.count_injections;

        if (timing.log_events_flag)
        {
            logPrintf(0,"TIMING Event '%s' injection number %u/%u USER %s (%u)\n"
                , evt_value->getName().c_str()
                , fgcddev.count_injections
                , timing.field_value[TIMING_FIELD_INJECTED_BATCHES]
                , timingUserName(timing.user)
                , timing.user);
        }

        // Check for all equipment devices if the automatic tuning is enabled

        for(channel=1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            if (device.ppm[timing.user].autotune.enable == FGC_CTRL_ENABLED)
            {
                // Initialise the autotune time to zero to trigger step immediately

                device.autotune_time = 0;

                if(timing.log_events_flag)
                {
                    logPrintf(channel,"TIMING autotune_time set to %d\n",device.autotune_time);
                }
            }
        }
    }
    else if(timing.log_events_flag)
    {
        logPrintf(0,"TIMING Event '%s' ignored (%u/%u) USER %s (%u)\n"
            , evt_value->getName().c_str()
            , fgcddev.count_injections
            , timing.field_value[TIMING_FIELD_INJECTED_BATCHES]
            , timingUserName(timing.user)
            , timing.user);
    }
}

void timingDynamicDestination(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static Timing::Value value;

    try
    {
        evt_value->getFieldValue("DYN_DEST", &value);
        timing.field_value[TIMING_FIELD_NEXT_DYN_DEST] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field DYN_DEST = %s (%u)\n",
                      evt_value->getName().c_str(),
                      value.getAsString().c_str(),
                      timing.field_value[TIMING_FIELD_NEXT_DYN_DEST]);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }
}



void timingCoast(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;

    // Put all devices that are in valid states to IDLE

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Check whether coast is enabled for the device

        if(device->coast)
        {
            // Check whether the device is in a valid state to enter coast

            switch(device->status.state_pc)
            {
                case FGC_PC_CYCLING:
                    stateSetPC(channel, FGC_PC_IDLE, "IDLE");
                    break;
            }
        }
    }
}



void timingRecover(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;

    // Put all devices that are in valid states to TO_CYCLING

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Check whether coast is enabled for the device

        if(device->coast)
        {
            // Check whether the device is in a valid state to recover

            switch(device->status.state_pc)
            {
                case FGC_PC_ARMED:
                case FGC_PC_IDLE:
                    stateSetPC(channel, FGC_PC_TO_CYCLING, "TO_CYCLING");
                    break;
            }
        }
    }
}



void timingEconomy(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;

    // Ignore economy request if this is the last millisecond of a cycle

    if(timing.next_ms_cycle_ms == 0)
    {
        return;
    }

    // Attempt to switch each device to an economy function for this cycle

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Attempt to put the channel into economy mode

        if(device->economy.state)
        {
            funcgenStartDynamicEconomy(channel);
        }
    }
}



void timingAbortEconomy(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;

    // Return immediately to CYCLING for any device in the ECONOMY state

    // Note that this can cause a discontinuity in the reference, which may cause devices to trip,
    // but is required only for a very specific case with RPPCU.BA1.MDSH.11971 (See EDMS SPS-R-ES-0003)

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Immediately change state to CYCLING, if currently in ECONOMY

        if(device->status.state_pc == FGC_PC_ECONOMY)
        {
            stateSetPC(channel, FGC_PC_CYCLING, "CYCLING");
        }
    }
}



void timingToCycling(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;

    // Put all devices that are in valid states to TO_CYCLING

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Check whether the device is in a valid state to go to TO_CYCLING

        switch(device->status.state_pc)
        {
            case FGC_PC_ON_STANDBY:
                stateSetPC(channel, FGC_PC_TO_CYCLING, "TO_CYCLING");
                break;
        }
    }
}



void timingToStandby(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t                channel;
    struct Equipdev_channel *device;

    // Put all devices that are in valid states to TO_STANDBY

    for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        device = &equipdev.device[channel];

        // Skip the channel if it is unused

        if(!device->fgcd_device->name) continue;

        // Check whether the device is in a valid state to go to TO_STANDBY

        switch(device->status.state_pc)
        {
            case FGC_PC_CYCLING:
                stateSetPC(channel, FGC_PC_TO_STANDBY, "TO_STANDBY");
                break;
        }
    }
}



struct Timing_event_handler timing_event_handlers[]
= {
    { timing_handler_names[TIMING_HANDLER_MS],                      &timingMillisecond          },
    { timing_handler_names[TIMING_HANDLER_STARTREF],                &timingStartRef             },
    { timing_handler_names[TIMING_HANDLER_TELEGRAM],                &timingTelegramReady        },
    { timing_handler_names[TIMING_HANDLER_CYCLEWARNING],            &timingCycleWarning         },
    { timing_handler_names[TIMING_HANDLER_INJECTWARNING],           &timingInjectionWarning     },
    { timing_handler_names[TIMING_HANDLER_DYNDEST],                 &timingDynamicDestination   },
    { timing_handler_names[TIMING_HANDLER_RECOVER],                 &timingRecover              },
    { timing_handler_names[TIMING_HANDLER_COAST],                   &timingCoast                },
    { timing_handler_names[TIMING_HANDLER_ECONOMY],                 &timingEconomy              },
    { timing_handler_names[TIMING_HANDLER_ABORTECONOMY],            &timingAbortEconomy         },
    { timing_handler_names[TIMING_HANDLER_TOCYCLING],               &timingToCycling            },
    { timing_handler_names[TIMING_HANDLER_TOSTANDBY],               &timingToStandby            },
    { timing_handler_names[TIMING_HANDLER_FEI],                     &timingFEI                  },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONCOMMIT],       &timingTransactionCommit    },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONROLLBACK],     &timingTransactionRollback  },
    { "",                                                           NULL                        },
};

// EOF
