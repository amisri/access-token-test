/*!
 * @file   consts.h
 * @brief  Global constants
 * @author Stephen Page
 */

#ifndef CONSTS_H
#define CONSTS_H

#include <consts_base.h>
#include <defconst.h>
#include <fgc_consts_gen.h>

#define FGCD_CYCLE_PERIOD_MS            20                            //!< FGCD cycle period in milliseconds
#define FGCD_CYCLES_PER_SEC             (1000 / FGCD_CYCLE_PERIOD_MS) //!< FGCD cycles per second
#define FGCD_MAX_EQP_DEVS               64                            //!< Maximum number of equipment devices (not including fgcddev)
#define FGCD_MAX_DEVS                   65                            //!< Total maximum number of devices (including fgcddev)
#define FGCD_MAX_SUB_DEVS_PER_DEV       16                                                //!< Maximum number of sub-devices per device
#define FGCD_MAX_SUB_DEVS             ((1 + FGCD_MAX_SUB_DEVS_PER_DEV) * FGCD_MAX_DEVS)   //!< Maximum number of sub-devices

#define TIMING_DEFAULT_SOURCE           TIMING_SYS                    //!< Default timing source

// Override maximum value length and number of commands to allow data for all FGCDs to fit

#include <sub.h>

#define CMD_MAX_VAL_LENGTH              ((FGC_MAX_FGCDS+1)*(uint32_t)sizeof(struct fgcd_data))      //!< https://wikis.cern.ch/display/TEEPCCCS/MAX_VAL_LEN


#endif

// EOF
