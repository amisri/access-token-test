/*!
 * @file   prop_class.h
 * @brief  Declarations for class-specific properties.
 * @author Stephen Page
 */

#ifndef PROP_CLASS_H
#define PROP_CLASS_H

// Forward declarations

struct Cmd;
struct prop;

// External functions

/*!
 * Check whether a reset is currently permitted.
 *
 * @returns Always returns 1; reset is always permitted.
 */

int32_t SetifResetOk(struct Cmd *command);

/*!
 * Terminate the FGCD.
 *
 * @param[in] command     Pointer to the SET command which initiated the terminate function
 * @param[in] property    Pointer to the property which indicates the type of termination
 *
 * @returns If the function is successful, it does not return. It returns 1 on failure, i.e. it was called
 *          with a property it does not handle.
 */

int32_t SetTerminate(struct Cmd *command, struct prop *property);

#endif

// EOF
