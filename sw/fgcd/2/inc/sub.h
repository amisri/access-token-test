/*!
 * @file   sub.h
 * @brief  Declarations for subscribing to data
 * @author Stephen Page
 */

#ifndef SUB_H
#define SUB_H

#include <stdint.h>
#include <pthread.h>

#include <consts.h>
#include <defconst.h>
#include <pub.h>
#include <fgc_pubdata.h>
#include <hash.h>

// Constants

#define SUB_UDP_PORT_NUM    2000        //!< UDP port for data subscription thread


/*!
 * Structure to contain data for each subscribed FGCD instance
 */

struct fgcd_data
{
    char                        names[FGCD_MAX_DEVS][FGC_MAX_DEV_LEN + 1];   //!< Name of each device attached to the FGCD
    char                        hostname[HOST_NAME_MAX + 1];                    //!< FGCD host name
    uint32_t                    recv_time_sec;                                  //!< Unix time of data reception in seconds
    uint32_t                    recv_time_usec;                                 //!< Unix time of data reception microseconds
    struct Pub_data             pub_data;                                       //!< Published data
};

/*!
 * Struct containing global variables
 */

struct Sub
{
    pthread_mutex_t     mutex;                                          //!< Mutex to protect access to status data
    pthread_t           thread;                                         //!< Thread handle
    int                 sock;                                           //!< Socket to receive data
    struct fgcd_data    fgcd_data[FGC_MAX_FGCDS];                       //!< Data published
    struct hash_table   *address_fgcd_hash;                             //!< Hash mapping of addresses to FGCD data
    struct hash_entry   address_fgcd_entry[FGC_MAX_FGCDS];              //!< Hash entries
    uint32_t            address_fgcd_entrynum;                          //!< Index of current entry being added to the hash
    struct hash_table   *name_fgcd_hash;                                //!< Hash mapping of names to FGCD data
    struct hash_entry   name_fgcd_entry[FGC_MAX_FGCDS * FGCD_MAX_DEVS]; //!< Hash entries
    uint32_t            name_fgcd_entrynum;                             //!< Index of current entry being added to the hash
};

extern struct Sub sub;

// External functions

/*!
 * Start the subscription thread
 */

int32_t subStart(void);

#endif

// EOF
