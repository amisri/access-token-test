/*!
 * @file   equipdev.h
 * @brief  Declarations for equipment devices
 * @author Stephen Page
 */

#pragma once

#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>

#include <classes/98/defconst.h>
#include <consts.h>
#include <fgc_errs.h>
#include <fgc_stat.h>
#include <hash.h>
#include <queue.h>
#include <timing.h>

// Forward declarations

struct Parser;
struct prop;

// Constants

#define EQUIPDEV_CLASS_ID   98      //!< FGC class ID for equipment devices
#define EQUIPDEV_MAX_PROPS  100     //!< Maximum number of properties supported by an equipment device

/*!
 * Data for an equipment device channel
 */

struct Equipdev_channel
{
    struct FGCD_device  *fgcd_device;                       //!< Pointer to FGCD device structure
    struct fgc98_stat   status;                             //!< Published status
    uintptr_t           prop_num_els[EQUIPDEV_MAX_PROPS];   //!< Numbers of elements for device properties

    // PPM variables must be within the following structure

    struct Equipdev_channel_ppm                     //!< PPM variables
    {
        uint16_t transaction_id;                    //!< Transaction ID for transactional settings
        uint16_t transaction_state;                 //!< State of a transaction

        // Transactional settings

        struct Equipdev_ppm_transactional
        {
        } transactional, transactional_backup;      //!< PPM transactional settings and a backup to allow rollback
    } ppm[NUM_CYC_SELECTORS];
};

/*!
 * Struct containing global variables
 */

struct Equipdev
{
    struct Hash_table       *const_hash;            //!< Hash of constants
    struct Hash_table       *prop_hash;             //!< Hash of properties
    struct Equipdev_channel device[FGCD_MAX_DEVS];  //!< Data for equipment devices
    const char              *fgc_class_name;        //!< Class name
    const char              *fgc_platform_name;     //!< Platform name
    uint8_t                 fgc_platform;           //!< Platform number

    pthread_t               thread;                 //!< Thread handle
    struct Queue            command_queue;          //!< Queue of commands
};

extern struct Equipdev equipdev;


// Static functions

//! Called from nonvol.cpp if a config property is changed.

static inline void equipdevConfigUnsync(uint32_t channel)
{
}

// External functions

/*!
 * Initialise data for devices.
 *
 * This function is called each time the device names are read.
 */

void equipdevInitDevices(void);

/*!
 * Publish data for equipment devices
 */

void equipdevPublish(void);

/*!
 * Start the equipment device thread
 */

int32_t equipdevStart(void);

/*!
 * Callback from parser to manually configure properties.
 *
 * @param[in] parser Parser data structure used to process a given command
 *
 * @return    true if the property is ppm, false otherwise
 *
 */

bool equipdevConfigProperty(struct Parser *parser, struct prop *property);
/*!
 * This function is called in nonvol.c. It triggers/clears a CONFIG warning in the equipment device.
 *
 * @param[in] channel     channel address
 * @param[in] set         Set the CONFIG warning if different than 0, otherwise clears the CONFIG warning.
 */

void equipdevNonvolWarn(uint32_t channel, int32_t set);

//! Callback from parser to indicate that a transactional property was set

void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property);

//! Callback to notify transactional properties for publication

void equipdevTransactionNotify(uint32_t channel, uint32_t user);

//! Apply transactional settings for a user on a device

fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time);

//! Rollback transactional settings for a user on a device

void equipdevTransactionRollback(uint32_t channel, uint32_t user);

//! Test transactional settings for a user on a device

fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user);

// EOF
