/*!
 * @file   tasks.cpp
 * @brief  Define tasks to be started and stopped
 * @author Stephen Page
 */

#include <cmdqmgr.h>
#include <consts.h>

#include <devname.h>
#include <equipdev.h>
#include <fgcddev.h>
#include <logging.h>
#include <nonvol.h>
#include <parser.h>
#include <pub.h>
#include <rt.h>
#include <sub.h>
#include <taskmgr.h>
#include <tcp.h>
#include <timing.h>

struct Task tasks[]
=
{
    // label                        start_func                  fatal   flag

    { "parser",                     &parserInit,                1,      NULL                },
    { "command queue manager",      &cmdqmgrInit,               1,      NULL                },
    { "device naming",              &devnameInit,               1,      NULL                },
    { "FGCD device",                &fgcddevStart,              1,      NULL                },
    { "logging",                    &logStart,                  1,      NULL                },
    { "equipment devices",          &equipdevStart,             1,      NULL                },
    { "timing",                     &timingInit,                0,      NULL                },
    { "timing events",              &timingSubscribeEvents,     1,      NULL                },
    { "publishing",                 &pubStart,                  1,      NULL                },
    { "subscription",               &subStart,                  1,      NULL                },
    { "TCP server",                 &tcpStart,                  1,      NULL                },
    { "",                           NULL,                       0,      NULL                },
};

// EOF
