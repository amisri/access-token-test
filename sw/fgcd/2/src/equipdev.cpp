/*!
 * @file   equipdev.c
 * @author Stephen Page
 *
 * Functions for equipment devices
 */

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <classes/98/defconst.h>
#include <classes/98/definfo.h>
#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <fgc_stat.h>
#include <fgcd.h>
#include <hash.h>
#include <logging.h>
#include <parser.h>
#include <prop.h>
#include <pub.h>
#include <queue.h>
#include <sub.h>
#include <fgcd_thread.h>



// Global variables

struct Equipdev equipdev = {};



// Static functions

static FGCD_thread_func equipdevRun;

static int32_t equipdevInit(void);



/*
 * Initialise equipment devices
 */

static int32_t equipdevInit(void)
{
    uint32_t i;

    // Set class

    equipdev.fgc_class_name    = FGC_CLASS_NAME;

    // Set platform

    equipdev.fgc_platform      = FGC_PLATFORM_ID;
    equipdev.fgc_platform_name = FGC_PLATFORM_NAME;

    // Initialise each device

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        equipdev.device[i].fgcd_device = &fgcd.device[i];
    }

    // Initialise command queue

    if(queueInit(&equipdev.command_queue, cmdqmgr.num_cmds))
    {
        // Failed to initialise command queue

        fprintf(stderr, "ERROR: Failed to initialise command queue\n");
        return 1;
    }

    return 0;
}



void equipdevInitDevices(void)
{
    struct FGCD_device  *fgcd_device;
    uint32_t            i;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        fgcd_device = &fgcd.device[i];

        if(fgcd_device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set command queue

            fgcd_device->command_queue = &equipdev.command_queue;

            // Set device as online

            fgcd_device->online  = 1;
            fgcd_device->ready   = 1;
        }
    }
}



void equipdevPublish(void)
{
    struct FGCD_device  *device;
    uint32_t            i;
    struct fgc_stat     *pub_data;
    struct fgc98_stat   *pub_status;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        device      = &fgcd.device[i];
        pub_data    = &pub.published_data.status[i];
        pub_status  = &pub_data->class_data.c98;

        if(device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set class in published data

            pub_data->class_id = EQUIPDEV_CLASS_ID;

            // Copy device status into published data

            memcpy(pub_status,
                   &equipdev.device[i].status,
                   sizeof(equipdev.device[i].status));

            // Byte-swap published data

            pub_status->st_faults   = htons(pub_status->st_faults);
            pub_status->st_warnings = htons(pub_status->st_warnings);
        }
    }
}



int32_t equipdevStart(void)
{
    // Initialise equipment devices

    if(equipdevInit() != 0) return 1;

    // Start equipment device thread

    if(fgcd_thread_create(&equipdev.thread, equipdevRun, NULL, EQUIPDEV_THREAD_POLICY, EQUIPDEV_THREAD_PRIORITY, NO_AFFINITY) != 0)
    {
        // Free command queue

        queueFree(&equipdev.command_queue);

        return 1;
    }

    return 0;
}



/*
 * Equipment device thread
 */

static void *equipdevRun(void *unused)
{
    struct Cmd         *command;       // Pointer to received command
    void               *data;          // Pointer to data to return as response
    uint32_t            data_size;      // Size of data to return as response
    uint32_t            data_size_net;  // Size of data to return as response in network byte order
    struct hash_entry  *gw_entry;      // Gateway data hash entry

    // Process received commands

    while(1)
    {
        if((command = reinterpret_cast<struct Cmd*>(queueBlockPop(&equipdev.command_queue))) == NULL)
        {
            logPrintf(0, "EQUIPDEV could not read command queue, exiting.\n");
            break;
        }

        // Reject unhandled command types

        if(command->type != CMD_GET)
        {
            command->error = FGC_NOT_IMPL;
            queueBlockPush(command->response_queue, command);
            continue;
        }

        // Attempt to find device in hash

        if((gw_entry = hashFind(sub.name_fgcd_hash, command->command_string)))
        {
            data        = (void *)gw_entry->value;
            data_size   = sizeof(struct fgcd_data);
        }
        else // Command does not match an FGCD device name
        {
            if(!strcmp(command->command_string, "ALL")) // Request was for all data
            {
                data        = &sub.fgcd_data;
                data_size   = sub.address_fgcd_entrynum * sizeof(struct fgcd_data);
            }
            else // Request was for unknown data
            {
                command->error = FGC_UNKNOWN_SYM;
                queueBlockPush(command->response_queue, command);
                continue;
            }
        }

        // Check that data will fit within value buffer

        if(1 + sizeof(uint32_t) + data_size > CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            queueBlockPush(command->response_queue, command);
            continue;
        }

        // Add binary response prefix (0xFF and length) to value

        data_size_net       = htonl(data_size);
        command->value[0]   = 0xFF;
        memcpy(&command->value[1], &data_size_net, sizeof(data_size_net));
        command->value_length = 1 + sizeof(uint32_t);

        // Copy data to value buffer

        pthread_mutex_lock(&sub.mutex);
        memcpy(&command->value[command->value_length], data, data_size);
        pthread_mutex_unlock(&sub.mutex);
        command->value_length += data_size;

        // Return response

        queueBlockPush(command->response_queue, command);

    }

    return 0;
}



bool equipdevConfigProperty(struct Parser *parser, struct prop *property)
{
    bool        is_ppm    = false;                                   // Flag to indicate that property access is PPM

    struct Cmd *command   = parser->current_command;
    uint32_t    device_id = command->device->id;

    // If the value is in the fgcd.device or equipdev.device structures, set the value to array element for the device

    // Value

    if(property->value >= (void *)&fgcd.device[0] &&
       property->value <  (void *)&fgcd.device[1])
    {
        // Value is in fgcd.device structure

        parser->value += sizeof(fgcd.device[0]) * device_id;
    }
    else if(property->value >= (void *)&equipdev.device[0] &&
            property->value <  (void *)&equipdev.device[1])
    {
        // Advance value to correct equipment device structure

        parser->value += sizeof(equipdev.device[0]) * device_id;
    }

    // Zero command user if command is not PPM

    if(property->type != PT_PARENT && !(property->flags & PF_PPM) && !is_ppm)
    {
        command->user = 0;
    }

    // Number of elements

    if(property->flags & PF_INDIRECT_N_ELS)
    {
        if((void *)parser->num_elements_ptr >= (void *)&fgcd.device[0] &&
           (void *)parser->num_elements_ptr <  (void *)&fgcd.device[1])
        {
            // Number of elements is in fgcd.device structure

            parserIncNumElsPtr(parser, sizeof(struct FGCD_device), device_id);
        }
        else if((void *)parser->num_elements_ptr >= (void *)&equipdev.device[0] &&
                (void *)parser->num_elements_ptr <  (void *)&equipdev.device[1])
        {
            // Number of elements is in equipdev.device structure

            // Advance number of elements to correct equipment device structure

            parserIncNumElsPtr(parser, sizeof(equipdev.device[0]), device_id);


        }
    }

    return is_ppm;
}



void equipdevNonvolWarn(uint32_t channel, int32_t set)
{
}



void equipdevTransactionalPropertySet(struct Cmd *command, struct prop *property)
{
}



void equipdevTransactionNotify(uint32_t channel, uint32_t user)
{
}



fgc_errno equipdevTransactionCommit(uint32_t channel, uint32_t user, const struct timeval *time)
{
    return FGC_NOT_IMPL;
}



void equipdevTransactionRollback(uint32_t channel, uint32_t user)
{
    return;
}



fgc_errno equipdevTransactionTest(uint32_t channel, uint32_t user)
{
    return FGC_NOT_IMPL;
}

// EOF
