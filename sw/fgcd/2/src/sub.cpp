/*!
 * @file   sub.cpp
 * @author Stephen Page
 *
 * Functions for subscribing to data
 */

#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <consts.h>
#include <devname.h>
#include <fgc_pubdata.h>
#include <fgcddev.h>
#include <hash.h>
#include <logging.h>
#include <sub.h>
#include <timing.h>
#include <fgcd_thread.h>



// Global variables

struct Sub sub;



// Static functions

static FGCD_thread_func subRecv;

static int32_t subReadNameFile(void);
static int32_t subReadNameFileChannels(struct fgcd_data *fgcd_data, FILE *name_file);



int32_t subStart(void)
{
    struct sockaddr_in sin;

    if(fgcd_mutex_init(&sub.mutex, SUB_MUTEX_POLICY) != 0) return 1;

    // Create address->FGCD instance data hash

    if((sub.address_fgcd_hash = hashInit(FGC_MAX_FGCDS)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise address->FGCD hash.\n");
        return 1;
    }

    // Initialise name->fgcd hash

    if((sub.name_fgcd_hash = hashInit(FGC_MAX_FGCDS * FGCD_MAX_DEVS)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise name->gw hash.\n");
        return 1;
    }

    // Read name file

    if(subReadNameFile())
    {
        return 1;
    }

    // Create socket

    if((sub.sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        return 1;
    }

    // Set bind options

    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(SUB_UDP_PORT_NUM);
    sin.sin_family      = AF_INET;

    // Bind to socket

    if(bind(sub.sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("bind");
        return 1;
    }

    // Start receive thread

    return fgcd_thread_create(&sub.thread, subRecv, NULL, SUB_THREAD_POLICY, SUB_THREAD_PRIORITY, NO_AFFINITY);
}



/*
 * Read details of all devices from the name file
 */

static int32_t subReadNameFile(void)
{
    int                 c                      = EOF;
    uint32_t            channel;
    uint8_t             class_number;
    struct hash_entry   *entry;
    char                format[100];
    struct hostent      *hostent;
    char                hostname[HOST_NAME_MAX + 1];
    int                 items_assigned;
    uint32_t            line_number     = 0;
    char                name[FGC_MAX_DEV_LEN + 1];
    FILE                *name_file;
    uint16_t            sector_mask;

    sub.address_fgcd_entrynum   = 0;
    sub.name_fgcd_entrynum      = 0;

    // Open name file

    if((name_file = fopen(FGC_NAME_FILE, "r")) == NULL)
    {
        perror("Open name file " FGC_NAME_FILE);
        return 1;
    }

    // Read name file entries

    // Create format from constants

    sprintf(format, "%%%d[^:]:%%u:%%hhu:%%%d[^:]:0x%%04hX",
            HOST_NAME_MAX, FGC_MAX_DEV_LEN);

    do
    {
        entry = &sub.address_fgcd_entry[sub.address_fgcd_entrynum];

        items_assigned = fscanf(name_file, format, hostname,
                                                   &channel,
                                                   &class_number,
                                                   name,
                                                   &sector_mask);

        line_number++;

        // Check that the correct number of fields were read
        // and whether the entry is an FGCD device (the channel is 0)

        if(items_assigned == 5)
        {
            if(channel == 0) // The entry is for an FGCD device
            {
                if(sub.address_fgcd_entrynum < FGC_MAX_FGCDS)
                {
                    // Resolve address for hostname

                    if((hostent = gethostbyname(hostname)) == NULL)
                    {
                        // Resolution failed

                        fprintf(stderr, "WARNING: Unable to resolve FGCD host %s in name file %s at line %u\n",
                                hostname, FGC_NAME_FILE, line_number);
                        fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                        continue;
                    }

                    // Copy address from host entry to hash key

                    memcpy(&entry->key, hostent->h_addr, hostent->h_length);

                    // Set hash entry value to pointer to FGCD's data entry

                    entry->value = (HASH_VALUE)&sub.fgcd_data[sub.address_fgcd_entrynum];

                    // Store hostname in FGCD's data entry

                    strcpy(sub.fgcd_data[sub.address_fgcd_entrynum].hostname, hostname);

                    // Store name in FGCD's data entry

                    strcpy(sub.fgcd_data[sub.address_fgcd_entrynum].names[0], name);

                    // Get names of all channels

                    subReadNameFileChannels(&sub.fgcd_data[sub.address_fgcd_entrynum], name_file);

                    // Check for duplicate entry

                    if(hashFind(sub.address_fgcd_hash, entry->key.c))
                    {
                        fprintf(stderr, "WARNING: Duplicate entry of %s in name file %s at line %u\n",
                                entry->key.c, FGC_NAME_FILE, line_number);
                        fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                        continue;
                    }

                    // Insert entry into address->FGCD hash table

                    hashInsert(sub.address_fgcd_hash, entry);

                    // Clean the warning

                    fgcddev.status.st_warnings &= ~FGC_WRN_NAMEFILE;
                }
                else
                {
                    fprintf(stderr, "WARNING: Number of FGCDs present in the name file is bigger than FGC_MAX_FGCDS\n");
                    fgcddev.status.st_warnings &= ~FGC_WRN_NAMEFILE;
                }

                sub.address_fgcd_entrynum++;
            }
        }

        // Read until end of line or file

        while((c = fgetc(name_file)) != EOF && c != '\n');

    } while(c != EOF);

    // Name file read successfully
    // Close it

    fclose(name_file);
    return 0;
}



/*
 * Read details of all of an FGCD's channels from the name file
 */

static int32_t subReadNameFileChannels(struct fgcd_data *fgcd_data, FILE *name_file)
{
    int                 c;
    uint32_t            channel;
    uint8_t             class_number;
    struct hash_entry   *entry;
    char                format[100];
    char                hostname[HOST_NAME_MAX + 1];
    int                 items_assigned;
    uint32_t            line_number     = 0;
    char                name[FGC_MAX_DEV_LEN + 1];
    fpos_t              position;
    uint16_t            sector_mask;

    // Get names of all channels

    // Store file position

    fgetpos(name_file, &position);

    // Return to start of file

    rewind(name_file);

    // Read name file entries

    // Create format from constants

    sprintf(format, "%%%d[^:]:%%u:%%hhu:%%%d[^:]:0x%%04hX",
            HOST_NAME_MAX, FGC_MAX_DEV_LEN);

    do
    {
        entry = &sub.name_fgcd_entry[sub.name_fgcd_entrynum];

        items_assigned = fscanf(name_file, format, hostname,
                                                   &channel,
                                                   &class_number,
                                                   name,
                                                   &sector_mask);

        line_number++;

        // Check that the correct number of fields were read

        if(items_assigned == 5)
        {
            // Check whether the entry is for the current FGCD instance

            if(!strcmp(hostname, fgcd_data->hostname))
            {
                // Check that channel number is valid

                if(channel < FGCD_MAX_DEVS)
                {
                    // Store channel name

                    strcpy(fgcd_data->names[channel], name);

                    // Copy name to hash key

                    strcpy(entry->key.c, name);

                    // Set hash entry value to pointer to FGCD data

                    entry->value = (HASH_VALUE)&sub.fgcd_data[sub.address_fgcd_entrynum];

                    // Check for duplicate entry

                    if(hashFind(sub.name_fgcd_hash, entry->key.c))
                    {
                        fprintf(stderr, "WARNING: Duplicate entry of %s in name file %s at line %u\n",
                                entry->key.c, FGC_NAME_FILE, line_number);
                        fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                        continue;
                    }

                    // Insert entry into name->FGCD hash table

                    hashInsert(sub.name_fgcd_hash, entry);
                    sub.name_fgcd_entrynum++;
                }
                else
                {
                    // Channel number is invalid, return error

                    fprintf(stderr, "WARNING: Invalid channel number %u in name file %s at line %u\n",
                            channel, FGC_NAME_FILE, line_number);
                    fgcddev.status.st_warnings |= FGC_WRN_NAMEFILE;
                    continue;
                }
            }
        }
        // Read until end of line or file

        while((c = fgetc(name_file)) != EOF && c != '\n');

    } while(c != EOF);

    // Restore file position

    fsetpos(name_file, &position);

    return 0;
}



/*
 * Thread to receive data
 */

static void *subRecv(void *unused)
{
    char                     address[32];
    struct Pub_data          buffer;
    ssize_t                  bytes_in;
    struct fgcd_data        *fgcd_data;
    struct sockaddr_in       from;
    socklen_t                from_len;
    struct hash_entry       *hash_entry;
    struct timeval           recv_time;

    // Ignore SIGPIPE

    signal(SIGPIPE, SIG_IGN);

    while(1)
    {
        // Receive data

        from_len = sizeof(from);
        bytes_in = recvfrom(sub.sock,
                            &buffer, sizeof(buffer),
                            0,
                            (struct sockaddr *)&from, &from_len);
        gettimeofday(&recv_time, 0);

        timingReadTime(&timing.user_time[0]);

        // Check that packet size minus header and timestamp is a multiple of sizeof(struct fgc_stat)

        if((bytes_in - (sizeof(struct fgc_udp_header) + (sizeof(int32_t) * 2))) % sizeof(struct fgc_stat))
        {
            // Ignore packets of incorrect size

            if(inet_ntop(AF_INET, &from.sin_addr, address, sizeof(address)))
            {
                logPrintf(0, "SUB Packet of incorrect size %zd received from %s\n", bytes_in, address);
            }

            // Wait for next packet

            continue;
        }

        // Resolve FGCD and copy data

        if((hash_entry = hashFind(sub.address_fgcd_hash, (char *)&from.sin_addr.s_addr)))
        {
            fgcd_data = (struct fgcd_data *)hash_entry->value;

            // Copy buffer data to FGCD's entry

            pthread_mutex_lock(&sub.mutex);
            fgcd_data->recv_time_sec    = htonl(recv_time.tv_sec);
            fgcd_data->recv_time_usec   = htonl(recv_time.tv_usec);
            memset(&fgcd_data->pub_data, 0, sizeof(fgcd_data->pub_data));
            memcpy(&fgcd_data->pub_data, &buffer, bytes_in);
            pthread_mutex_unlock(&sub.mutex);
        }
        else // Packet could not be resolved to a FGCD
        {
            if(inet_ntop(AF_INET, &from.sin_addr, address, sizeof(address)))
            {
                logPrintf(0, "SUB Unresolvable packet received from %s\n", address);
            }
        }
    }

    return 0;
}

// EOF
