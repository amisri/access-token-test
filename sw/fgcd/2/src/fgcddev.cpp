/*!
 * @file   fgcddev.c
 * @author Stephen Page
 *
 * Functions for the FGCD device
 */

#include <stdio.h>
#include <string.h>

#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <cmd.h>
#include <cmdqmgr.h>
#include <consts.h>
#include <definfo.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <hash.h>
#include <nonvol.h>
#include <parser.h>
#include <pub.h>
#include <rt.h>
#include <tcp.h>
#include <version.h>



// Global variables

struct FGCDdev fgcddev;



int32_t fgcddevStart(void)
{
    uint32_t                    i;
    struct Parser_class_data    parser_data;

    // Set device

    fgcddev.device = &fgcd.device[0];

    // Set class

    fgcddev.fgc_class_name = FGC_CLASS_NAME;

    // Set platform

    fgcddev.fgc_platform        = FGC_PLATFORM_ID;
    fgcddev.fgc_platform_name   = FGC_PLATFORM_NAME;

    // Set published compile time

    fgcddev.status.compile_time = version.time.tv_sec;

    // Set class in published data

    pub.published_data.status[0].class_id = FGC_CLASS_ID;

    // Initialise the property hash

    if(!(fgcddev.prop_hash = hashInit(PROP_HASH_SIZE)))
    {
        fprintf(stderr, "ERROR: Unable to initialise property hash\n");
        return 1;
    }

    // Populate the property hash

    for(i = 0 ; i < N_PROP_SYMBOLS ; i++)
    {
        hashInsert(fgcddev.prop_hash, &SYM_TAB_PROP[i]);
    }

    // Initialise the constant hash

    if((fgcddev.const_hash = hashInit(CONST_HASH_SIZE)) == NULL)
    {
        fprintf(stderr, "ERROR: Unable to initialise constant hash\n");

        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Populate the const hash

    for(i = 0 ; i < N_CONST_SYMBOLS ; i++)
    {
        hashInsert(fgcddev.const_hash, &SYM_TAB_CONST[i]);
    }

    // Give values to test properties

    fgcddev.test_string[0] = "FIRST";
    fgcddev.test_string[1] = "SECOND";

    // Set parser data for class

    parser_data.class_id        = FGC_CLASS_ID;
    parser_data.sym_tab_const   = SYM_TAB_CONST;
    parser_data.const_hash      = fgcddev.const_hash;
    parser_data.sym_tab_prop    = SYM_TAB_PROP;
    parser_data.sym_tab_prop_len = sizeof(SYM_TAB_PROP)/sizeof(struct hash_entry);
    parser_data.prop_hash       = fgcddev.prop_hash;
    parser_data.get_func        = GET_FUNC;
    parser_data.pars_func       = PARS_FUNC;
    parser_data.num_pars_func   = NUM_PARS_FUNC_2;
    parser_data.set_func        = SET_FUNC;
    parser_data.setif_func      = SETIF_FUNC;
    parser_data.evtlog_func     = NULL;

    if(parserSetClassData(&parser_data))
    {
        fprintf(stderr, "ERROR: Failed to set parser class data\n");

        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Start the parser

    if(parserStart(&fgcddev.parser))
    {
        fprintf(stderr, "ERROR: Failed to start parser\n");

        parserClearClassData(FGC_CLASS_ID);
        hashFree(fgcddev.const_hash);
        hashFree(fgcddev.prop_hash);
        return 1;
    }

    // Set device command queue

    fgcddev.device->command_queue = &fgcddev.parser.command_queue;

    // Set device as online

    fgcddev.device->online  = 1;
    fgcddev.device->ready   = 1;

    return 0;
}

void fgcddevInitDevice(void)
{
}



void fgcddevPublish(void)
{
    uint32_t            pub_ms      = ntohl(pub.published_data.time_usec) / 1000;
    struct fgc2_stat    *pub_status = &pub.published_data.status[0].class_data.c2;

    // Check whether this is the first publication of the second

    if(!(pub_ms / FGCD_CYCLE_PERIOD_MS))
    {
        // Update per second TCP and UDP load statistics

        fgcddev.status.tcp_load = tcp.load;
        tcp.load                = 0;
    }

    // Copy device status into published data

    memcpy(pub_status, &fgcddev.status, sizeof(fgcddev.status));

    // Byte-swap published data

    pub_status->cmw_subs        = htons(pub_status->cmw_subs);
    pub_status->compile_time    = htonl(pub_status->compile_time);
    pub_status->st_faults       = htons(pub_status->st_faults);
    pub_status->st_unlatched    = htons(pub_status->st_unlatched);
    pub_status->st_warnings     = htons(pub_status->st_warnings);
    pub_status->tcp_load        = htonl(pub_status->tcp_load);
}

// EOF
