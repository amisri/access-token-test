#! /usr/bin/bash

# reset fgc6 running on all FECs

cd $(dirname $0)
root=$(pwd)
fecs=$(gawk -F : '//{if($3==8)print $1}' ~pclhc/etc/fgcd/name | sort)

for host in $fecs
do
    echo Restarting $host
    lumensctl -S fgcd -H $host restart

done

exit 0

# EOF
