/*!
 * @file   defprops_inc.h
 * @brief  Includes header files needed by defprops.h
 * @author Stephen Page
 */

#ifndef DEFPROPS_INC_H
#define DEFPROPS_INC_H

#include <alarms.h>
#include <equipdev.h>
#include <fei.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <equip_libref.h>
#include <hash.h>
#include <meas.h>
#include <mugef/cmd.h>
#include <mugef/pc_families/1.h>
#include <mugef/pc_families/4.h>
#include <mugef/pc_families/6.h>
#include <mugefhw/fei.h>
#include <mugefhw/mpc.h>
#include <mugefhw/vmodttl.h>
#include <prop.h>
#include <prop_class.h>
#include <prop_equip.h>
#include <rt.h>
#include <tcp.h>
#include <timing.h>
#include <version.h>

#endif

// EOF
