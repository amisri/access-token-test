//! @file   equipdev.h
//! @brief  Declarations for equipment devices


#ifndef EQUIPDEV_H
#define EQUIPDEV_H

// Includes

#include <sys/time.h>
#include <libreg.h>
#include <libref.h>
#include <classes/94/sigStructs.h>
#include <classes/94/logMenus.h>
#include <classes/94/logStructs.h>
#include <classes/94/defconst.h>
#include <equip_common.h>
#include <equip_libevtlog.h>
#include <devname.h>
#include <fgc_errs.h>
#include <fgc_stat.h>
#include <meas.h>
#include <mugefhw/fei.h>
#include <equip_libref.h>


// Constants

const uint32_t  EQUIPDEV_CLASS_ID  = 94;     //!< FGC class ID for equipment devices
const uint32_t  EQUIPDEV_MAX_PROPS = 400;    //!< Maximum number of properties supported by an equipment device

const uint32_t  QD  = 1;
const uint32_t  QF  = 2;
const uint32_t  MBI = 3;

const uint32_t REF_MBI_QFD_INTLK_BIT_MASK = 0x8000; // Bit mask in reg_faults to trigger SLOW_ABORT

// Forward declarations

struct Parser;
struct prop;


//! State machine typedefs

typedef uint32_t  State(uint32_t channel, bool first_call);  //!< Type for state machine state function returning state number
typedef State   * Transition(uint32_t channel);              //!< Type for pointer to transition function returning pointer to state function


//! ADC

struct Equipdev_channel_adc
{
    float       raw_float;                          //!< Raw measurement (used by liblog)

    uint32_t    num_frame_err;                      //!< This is reported through ADC.EXTERNAL.NUM_FRAME_ERR
    uint32_t    num_timing_err;                     //!< This is reported through ADC.EXTERNAL.NUM_NO_ERR     <--- This is to avoid creating a new property for now

    bool        frame_err;                          //!< True if there was an ADC frame error on this iteration
    bool        timing_err;                         //!< True if there was an ADC timing error on this iteration
};


//! AUTOTUNE

struct Equipdev_channel_ppm_autotune
{
    uint32_t    enable;                             //!< Enable/disable the automatic tune change real-tiem reference
    float       steps[FGC_AUTOTUNE_MAX_INJ];        //!< Delta current per injection of the automatic tune setting
};


//! DCCT

struct Equipdev_channel_dcct
{
    uint32_t    primary_turns;                      //!< DCCT.PRIMARY_TURNS property. No. of primary turns for both DCCT channels. There is only one
                                                    //!< property for both channels; the value is propagated to the DCCTs by ParsCalFactors()
    uint8_t     st_dcct_a;
    uint8_t     st_dcct_b;
};


//! Fast Extraction Interlocks (FEI)

struct Equipdev_channel_ppm_fei
{
    float       i_meas[FGC_FEI_MAX_OCCURRENCES];        //!< Measured current for each FEI occurrence
    float       i_ref;                                  //!< Reference current
    float       i_tol;                                  //!< Tolerance relative to reference current
    uint32_t    output;                                 //!< FEI output number (0 = no output, 1-4 select output)
    uint32_t    result[FGC_FEI_MAX_OCCURRENCES];        //!< Flag indicating whether FEI check was successful for each occurrence
};


//! LOG (Legacy)

struct Equipdev_channel_log
{
    struct LOG_menus                menus;              //!< liblog menus
    struct LOG_mgr                  mgr;                //!< liblog manager
    struct LOG_read                 read;               //!< libLog read out structure
    struct LOG_read_control         read_control;       //!< liblog read control structure
    struct LOG_structs              structs;            //!< liblog structs - this is automatically generated
    struct LOG_buffers              buffers;            //!< liblog buffers pointer
    struct timeval                  post_mortem_time;   //!< Post mortem time

    bool                            is_frozen;          //!< Flag latched when the continuous and discontinuous logs are frozen

    struct Equipdev_channel_debug_log                   //!< CCLIBS debug log
    {
        float reg_mode;
        float ref_state;
        float fg_type;
        float fg_status;
        float ramp_mgr_stat;
        float cyc_status;
        float cyc_sel;
        float iter_time_us;
        float iter_index_i;
        float debug_sig;
    } debug;
};

struct Equipdev_channel_legacy_log
{
    float       i_ref;                                              //!< Logged current reference
    float       i_meas;                                             //!< Logged measured current value from power converter
    float       ref_meas;                                           //!< Logged measured reference output
};


//! MEAS properties

struct Equipdev_channel_meas
{
    uint32_t    sim;                                                //!< MEAS.SIM property
};


//! REF

struct Equipdev_channel_ref
{
    struct timeval abort;                                           //!< REF.ABORT property. Absolute time at which to abort the running change.

    uint16_t       event_group;                                     //!< REF.EVENT_GROUP
    bool           run_now;                                         //!< Set for REF NOW to trigger execution 1s after entering ARMED state
};


//! PPM Channel properties

struct Equipdev_channel_ppm                                         //!< PPM variables
{
    uintptr_t prop_num_els[EQUIPDEV_MAX_PROPS];                     //!< Numbers of elements for device properties

    struct REF_ctrl_pars ctrl;                                      //!< REF.FUNC.PLAY and REF.ECONOMY.DYN_END_TIME - these not transactional properties

    // Fast Extraction Interlocks (FEI)

    struct Equipdev_channel_ppm_fei fei;                            //!< Fast Extraction Interlocks (FEI)

    // SPS Mains automatic tune change

    struct Equipdev_channel_ppm_autotune autotune;                  //!< Autotune variables

    // Transactional settings support

    uint16_t                        transaction_id;                 //!< Transaction ID for transactional settings
    uint16_t                        transaction_state;              //!< State of a transaction
    struct REF_transaction_pars     transaction;                    //!< TRANSACTION LAST_FG_PAR_INDEX - this is not a property but is used to support transactions

    struct Equipdev_ppm_transactional
    {
        struct REF_ref_pars         ref;                            //!< REF.FUNC.TYPE
        struct REF_ramp_pars        ramp;                           //!< REF.RAMP.*
        struct REF_pulse_pars       pulse;                          //!< REF.PULSE.*
        struct REF_plep_pars        plep;                           //!< REF.PLEP.*
        struct REF_pppl_pars        pppl;                           //!< REF.PPPL.*
        struct REF_cubexp_pars      cubexp;                         //!< REF.CUBEXP.*
        struct REF_trim_pars        trim;                           //!< REF.TRIM.*
        struct REF_test_pars        test;                           //!< REF.TEST.*
        struct REF_prbs_pars        prbs;                           //!< REF.PRBS.*

        struct REF_table_pars
        {
            FG_point             function[FGC_TABLE_LEN];           //!< REF.TABLE.FUNC.VALUE
            uintptr_t            num_elements;                      //!< Number of elements in REF.TABLE.FUNC.VALUE
        } table;
    } transactional, transactional_backup;                          //!< PPM transactional settings and a backup to allow rollback
};


//! Data for an equipment device channel

struct Equipdev_channel
{
    struct FGCD_device                * fgcd_device;                //!< Pointer to FGCD device structure

    // Configured flag

    bool                                is_configured;              //!< Device is configured flag

    // CCLIBS data structures

    struct REG_mgr                      reg_mgr;                    //!< Regulation manager data for this device
    struct REG_pars                     reg_pars;                   //!< Regulation manager parameters for this device
    struct REF_mgr                      ref_mgr;                    //!< Reference manager data for this device
    struct POLSWITCH_mgr                polswitch_mgr;              //!< Polarity switch manager data - required by libref even though it is not used
    struct SIG_struct                   sig_struct;                 //!< Signal data for this device
    struct EVTLOG_data                  evtlog_data;                //!< Allocation of memory for device data, prop_data, last_evt and records
    struct LIBREF_vars                  libref_vars;                //!< Variables associated with libref integration
    pthread_mutex_t                     reg_pars_mutex;             //!< Mutex to protect regParsCheck() and regParsProcess() from being run simultaneously
    pthread_mutex_t                     arm_event_mutex;            //!< Mutex to protect refArm() and refEventStartFunc() from being run simultaneously
    pthread_mutex_t                     evtlog_mutex;               //!< Mutex to protect event log callbacks and to lock resources

    // I/O

    struct Equipdev_channel_meas        meas;
    struct Equipdev_channel_dcct        dcct;
    struct Equipdev_channel_adc         adc[FGC_N_ADCS];            //!< ADC variables
    enum CC_enabled_disabled            qs_dac;                     //!< MUGEF.QS_DAC property

    // Unlatched faults

    uint16_t                            unlatched_faults;

    // Publication

    struct fgc94_stat                   status;                     //!< Published status

    // Device configuration

    uint16_t                            mode_op;                    //!< Operational mode requested by client
    uint16_t                            mode_pc;                    //!< Power converter mode requested by client
    uint16_t                            mode_pc_on;                 //!< Default power converter mode when on

    bool                                arming_enabled;             //!< Arming is disabled until all config is loaded from nonvol

    // States

    uint64_t                            state_pc_time_ms;           //!< Time since the last STATE.PC change (ms)
    uint32_t                            state_pc_bit_mask;          //!< STATE.PC as a bit mask (only 1 bit set at a time)
    State                             * state_pc_func;              //!< Pointer to STATE.PC state machine function
    State                             * state_op_func;              //!< Pointer to STATE.OP state machine function
    uint32_t                            vsrun_timeout;              //!< VS_RUN timeout timer

    // Logging

    struct Equipdev_channel_log         log;                        //!< Logging
    struct Equipdev_channel_legacy_log  legacy_log[FGC_LOG_LEN];    //!< MUGEF legacy logs (pre-cclibs)

    // Voltage Source

    struct Equipdev_channel_vs
    {
        float vsrun_timeout;                                        //!< Time-out for power converter to respond to start command

        struct Equipdev_channel_vs_state
        {
            bool comm_error;                                        // indicates a communication error with the vmod ttl card
            bool vs_power_on;                                       // indicates that the VS is on
            bool vs_fault;                                          // indicates if there's a fault in the VS
        } state;

        bool vsrun;                                                 // signals the VS to turn on and off

    } vs;

    // Timing

    struct timeval            time_start;                           //!< Time that the FGCD was started

    // RT properties

    uint32_t                  beam_dump;                            //!< Flag to indicate whether a beam dump will be requested when STATE.PC is not an operational state
    uint32_t                  coast;                                //!< Flag to indicate whether the device will respond to coast and recover timing events
    int32_t                   autotune_time;                        //!< Time reference of the automatic tune change reference function

    // References

    struct Equipdev_channel_ref    ref;

    // Test properties

    struct Equipdev_channel_test test;

    // PPM variables must be within the following structure

    struct Equipdev_channel_ppm ppm[NUM_CYC_SELECTORS];
};


//! Equipdev FEI output per user

struct Equipdev_ppm_fei_output
{
    uint32_t    active;                                             //!< Flag indicating whether output is active for the user
    uint16_t    pulse_width_100us;                                  //!< Width of the output pulse in steps of 100us
    uint32_t    result[FGC_FEI_MAX_OCCURRENCES];                    //!< Flag indicating whether FEI check was successful for each occurrence
};


//! Struct containing global variables

struct Equipdev
{
    pthread_t               thread;                                 //!< Thread ID for Equipdev thread
    uint64_t                beam_dump_request;                      //!< Bit mask of beam dump requests
    struct hash_table       *const_hash;                            //!< Hash of constants
    struct hash_table       *prop_hash;                             //!< Hash of properties
    struct Equipdev_channel device[FGCD_MAX_DEVS];                  //!< Data for equipment devices
    const char              *fgc_class_name;                        //!< Class name
    const char              *fgc_platform_name;                     //!< Platform name
    uint8_t                 fgc_platform;                           //!< Platform number

    struct Equipdev_oasis                                           //!< OASIS variables
    {
        uint32_t            i_meas_interval_ns;                     //!< I_MEAS sampling interval (period) in nanoseconds
        uint32_t            i_ref_interval_ns;                      //!< I_REF sampling interval (period) in nanoseconds
    } oasis;

    struct Equipdev_ppm                                             //!< PPM variables
    {
        struct Equipdev_ppm_legacy_log                              // Old MUGEF logs
        {
            uint32_t        index;                                  //!< Indices within log (element 0 is the index of the latest sample)
            uintptr_t       num_elements;                           //!< Numbers of elements in the log
            struct timeval  timestamp;                              //!< Timestamp of cycle within log
        } legacy_log;

        struct Equipdev_ppm_fei
        {
            uintptr_t       num_occurrences;                        //!< Number of FEI events that have occurred

            struct Equipdev_ppm_fei_output output[FEI_NUM_FEI_OUTPUTS];

        } fei;
    } ppm[NUM_CYC_SELECTORS];
};

extern struct Equipdev equipdev;

// Static functions

//! Called from nonvol.cpp if a config property is changed.

static inline void equipdevConfigUnsync(uint32_t channel)
{
}


// External functions

//! Initialise equipment devices

int32_t equipdevStart(void);


//! Check status of RST regulation

void equipdevCheckRstStatus(uint32_t channel);


//! Clean-up equipment devices

void equipdevCleanUp(void);


//! Initialise data for devices.
//!
//! This function is called each time the device names are read.

void equipdevInitDevices(void);


//! Publish data for equipment devices

void equipdevPublish(void);


//! Initialise an equipment device after it has been configured.
//!
//! This function should be called for a device when STATE.OP is changed from UNCONFIGURED.


void equipdevConfigure(uint32_t channel);


//! Update the automatic tune real-time reference

void equipdevRtAutotune(bool log_events_flag);

//! Update the real time reference

void equipdevRtUpdate(void);


//! Look up a property and set the number of elements.
//!
//! This function allows us to set the number of elements for the FGC.ID.* properties.
//! This cannot be done in the normal way as these properties have no SET function, so
//! this function is a bit of a hack to get around this limitation of the property model.
//!
//! It is also used to initialise Calibration properties on start-up, if they have no
//! value set in non-volatile storage.
//!
//! @param[in]        channel         Address of the DIGMUGEF device
//! @param[in]        bus_id          If the property name contains a '?', it will be replaced
//!                                   with this parameter. (Usually set to 'A' or 'B').
//! @param[in]        prop_label      Name of the property to update
//! @param[in]        num_els         Number of elements to set

void equipdevPropSetNumEls(uint32_t channel, char bus_id, const char *prop_label, uint32_t num_els);


//! Update unlatched faults

void equipdevUpdateFaults(uint32_t device_index);

#endif

// EOF
