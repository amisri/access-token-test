//! @file   defprops_class.h
//! @brief  Includes defconst, definfo and defprops for the class 94


#ifndef DEFPROPS_CLASS_H
#define DEFPROPS_CLASS_H

#include <classes/94/defconst.h>
#include <classes/94/definfo.h>
#include <classes/94/defprops.h>

#endif

// EOF
