/*!
 * @file   mode_pc.h
 * @brief  Manage Power Converter mode changes for FGClite devices
 * @author Michael Davis
 */

#ifndef MODE_PC_H
#define MODE_PC_H

#include <stdint.h>
#include <fgc_errs.h>

/*!
 * Set the Power Converter State Machine requested state.
 *
 * Sets MODE.PC and requests the appropriate Voltage Source command (START/STOP/RESET) from the Power Converter State Machine.
 */

enum fgc_errno modePcSet(uint32_t device_index, uint32_t mode);

#endif

// EOF
