/*!
 * @file   signals.h
 * @brief  Interface to libsig and calibration functions
 * @author Michael Davis
 * @author Miguel Hermo Serans
*/

// FGCD includes

#ifndef CC_SIG_H
#define CC_SIG_H

#include <stdint.h>

/*!
 * DIGMUG calibration channel bitmasks
 */

enum Cal_Channel
{
    CAL_ADC_A  =  1,
    CAL_ADC_B  =  2,
};


/*!
 * DIGMUG calibration non-volatile properties
 */

enum Cal_Nonvol_Prop
{
    CAL_A_ADC_EXTERNAL_GAIN,
    CAL_A_ADC_EXTERNAL_ERR,
    CAL_B_ADC_EXTERNAL_GAIN,
    CAL_B_ADC_EXTERNAL_ERR,
    NUM_CAL_NONVOL_PROPS
};


/*!
 * DIGMUG calibration non-volatile property names
 */

const char* const Cal_nonvol_prop_string[] =
{
    "CAL.A.ADC.EXTERNAL.GAIN",
    "CAL.A.ADC.EXTERNAL.ERR",
    "CAL.B.ADC.EXTERNAL.GAIN",
    "CAL.B.ADC.EXTERNAL.ERR",
};


// External functions

/*!
 * Bind CCLIBS analogue signal parameters to DIGMUGEF data structures and initialise values.
 *
 * @param[in]    device_index    Address of the DIGMUGEF device
 *
 */

void signalsInit(uint32_t device_index);


/*!
 * Process calibration requests for a given device
 * This should be called from the MS task.
 *
 * @param[in]    device_index   Address of the DIGMUGEF device
 * @param[in]    cal_channel    Channel to be calibrated (CAL_ADC_A or CAL_ADC_B)
 *
 * @returns      0 if the calibration succeeds, non-zero otherwise.
 */

uint32_t signalsCalibrate(uint32_t device_index, enum Cal_Channel cal_channel);

#endif

// EOF
