/*!
 * @file   state_op.h
 * @brief  Manage power converter state for DIGMUGEF devices
 */

#ifndef STATE_PC_H
#define STATE_PC_H

#include <stdint.h>
#include <defconst.h>

/*!
 *  Initialize the PC state machine for a channel to FLT_OFF
 *
 * @param[in]    channel    Device channel address
 *
 */
void statePcInit(uint32_t channel);

/*!
 * Update the power converter State Machine.
 *
 * Transition from current PC state to requested PC state.
 *
 * @param[in]    channel    Device channel address
 *
 */
void statePcRT(uint32_t channel);

/*!
 * Implements interlock between Slow Abort of the quads and optionally the MBI
 */

void statePcSlowAbortInterlock(void);

/*!
 * Implements interlock between Fast Abort of the quads and optionally the MBI
 */

void statePcFastAbortInterlock(void);

#endif
