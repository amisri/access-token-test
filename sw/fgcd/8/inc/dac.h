/*!
 * @file   ref.h
 * @brief  Declarations for reference generation
 * @author Stephen Page
 */

#ifndef DAC_H
#define DAC_H

#include <stdint.h>

// External functions

/*!
 * Initialise reference generation
 */

int32_t dacInit(void);

/*!
 * Calculate the reference to be applied in a given channel.
 * Note that the reference won't be applied until dacApplyReferences is executed
 *
 * @param[in] device_index   MUGEF channel
 */

void dacCalculateReference(uint32_t device_index);

/*!
 * Update the values of the DACs with the values obtained with dacCalculateReference()
 */
void dacApplyReferences(void);

#endif

// EOF
