//! @file   log_class.h
//! @brief  Includes log structure header files for the class 94


#ifndef LOG_CLASS_H
#define LOG_CLASS_H

// Define DEBUG_SIG - this will enable the inclusion of ccLogSetDebugSignals() in equip_liblog.cpp/h

#define DEBUG_SIG   device.log.debug.debug_sig

// Include class specific generated header files for liblog

#include <classes/94/logStructs.h>
#include <classes/94/logMenus.h>

#endif

// EOF
