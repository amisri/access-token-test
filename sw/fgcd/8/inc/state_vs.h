/*
 * Manages the voltage source of DIGMUGEF devices throught the VMOD TTL card
 * @author Miguel Hermo Serans
 */
#ifndef STATE_VS_H
#define STATE_VS_H

#include <stdint.h>


/*
 * Initialises VMOD card. This must be called before any other function
 */
int32_t stateVsInit(void);

/*
 * Read the status of a converter through the VMOD TTL card (or simulate it when in simulation mode)
 *
 * @param device_index      index of mugef device
 */
void stateVsRead(uint32_t device_index);

/*
 * Send commands to the converter through
 *
 * @param device_index      index of mugef device
 */
void stateVsSend(uint32_t device_index);

#endif
