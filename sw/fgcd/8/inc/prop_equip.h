/*!
 * @file   prop_equip.h
 * @brief  Declarations for equipment device-specific properties
 * @author Stephen Page
 */

#ifndef PROP_EQUIP_H
#define PROP_EQUIP_H

// Forward declarations

struct prop;
struct Cmd;

// External functions

/*!
 * Check whether auto-calibration is permitted
 */

int32_t SetifCalOk(struct Cmd *command);

/*!
 * Check whether the operational mode can be set
 */

int32_t SetifModeOpOk(struct Cmd *command);

/*!
 * Check whether the real-time mode can be set
 */

int32_t SetifModeRtOk(struct Cmd *command);

/*!
 * Check whether operational state is normal or simulation
 */

int32_t SetifOpNormalSim(struct Cmd *command);

/*!
 * Check whether operational state is not calibrating
 */

int32_t SetifOpNotCal(struct Cmd *command);

/*!
 * Check whether power converter is armed
 */

int32_t SetifPcArmed(struct Cmd *command);

/*!
 * Check whether power converter is off
 */

int32_t SetifPcOff(struct Cmd *command);

/*!
 * Check whether regulation state is NONE
 */

int32_t SetifRegStateNone(struct Cmd *command);

/*!
 * Check whether it is okay to move switches
 */

int32_t SetifSwitchOk(struct Cmd *command);

/*!
 * Get a log
 */

int32_t GetLog(struct Cmd *command, struct prop *property);


/*!
 * Get a log from mugef's legacy log.
 *
 * This function adds a log with FGC log headers to the value
 */

int32_t GetLogMugef(struct Cmd *command, struct prop *property);

// SET functions

/*!
 * Set DAC, ADC and DCCT calibration error properties
 */

int32_t SetCal(struct Cmd *command, struct prop *property);

/*!
 * Set load switch
 */

int32_t SetLoadSwitch(struct Cmd *command, struct prop *property);

/*!
 * Validate and set i_period_iters
 */

int32_t SetPeriodIters(struct Cmd *command, struct prop *property);

/*!
 * Set a power converter state
 */

int32_t SetPC(struct Cmd *command, struct prop *property);

/*!
 * Set polarity
 */

int32_t SetPolaritySwitch(struct Cmd *command, struct prop *property);

/*!
 * Reset voltage source state
 */

int32_t SetReset(struct Cmd *command, struct prop *property);

/*!
 * Set Process libreg parameters
 */
void ParsRegPars(uint32_t channel);

/*!
 * Set limit parameters
 */

void ParsLimits(uint32_t channel);

/*!
 * Set load parameters
 */

void ParsLoad(uint32_t channel);

/*!
 * Set load saturation parameters
 */

void ParsLoadSaturation(uint32_t channel);

/*!
 * Set parameters to select a new period
 */

void ParsPeriods(uint32_t channel);

/*!
 * Set voltage regulation parameters
 */

void ParsRegV(uint32_t channel);

/*!
 * Set ADC and DCCT calibration parameters
 *
 * @param[in] channel    Equipment device to set
 */

void ParsCalFactors(uint32_t channel);

/*!
 * Set LIMITS.I.MEAS_DIFF
 *
 * @param[in] channel    Equipment device to set
 */

void ParsSigParams(uint32_t channel);

#endif

// EOF
