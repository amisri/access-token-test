/*!
 * @file   consts.h
 * @brief  Global constants
 * @author Stephen Page
 */

#ifndef CONSTS_H
#define CONSTS_H

#include <consts_base.h>
#include <timing.h>

#define FGCD_CYCLE_PERIOD_MS            20                                                //!< FGCD cycle period in milliseconds
#define FGCD_CYCLES_PER_SEC             (1000 / FGCD_CYCLE_PERIOD_MS)                     //!< FGCD cycles per second
#define FGCD_MAX_EQP_DEVS               3                                                 //!< Maximum number of equipment devices (not including fgcddev)
#define FGCD_MAX_DEVS                   (FGCD_MAX_EQP_DEVS+1)                             //!< Total maximum number of devices (including fgcddev)
#define FGCD_MAX_SUB_DEVS_PER_DEV       0                                                 //!< Maximum number of sub-devices per device
#define FGCD_MAX_SUB_DEVS             ((1 + FGCD_MAX_SUB_DEVS_PER_DEV) * FGCD_MAX_DEVS)   //!< Maximum number of sub-devices

#define CMD_MAX_VAL_LENGTH              3850000                                           //!< https://wikis.cern.ch/display/TEEPCCCS/MAX_VAL_LEN

#define TIMING_COMLN_SPS_FULL_ECO_MASK  0x0008                      //!< Mask for the SPS telegram combined line group to get the full economy request
                                                                    // (This should ideally come from the timing library)

#endif

// EOF
