/*!
 * @file   cc_reg.h
 * @brief  Initialise the regulation manager
 * @author Michael Davis
 *
 * Functions to initialise the CCLIBS regulation manager structure
 */

#ifndef CC_REG_H
#define CC_REG_H

#include <stdint.h>

#include <cclibs.h>

/*!
 * Bind CCLIBS regulation parameters to DIGMUGEF data structures and initialise regulation values.
 *
 * @param[in] channel    DIGMUGEF address
 *
 * @retval    0          Success
 * @retval    1          Failure
 */
uint32_t regulationInit(uint32_t channel);

/*
 * Perform regulation on the given channel
 */
void regulationRegChannel(uint32_t channel, struct CC_us_time * iter_time, bool is_200ms_boundary);

#endif

// EOF
