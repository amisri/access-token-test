//! @file   evtlogInit_class.h
//! @brief  Includes evtlog structure init header files for the class 94


#ifndef EVTLOGINIT_CLASS_H
#define EVTLOGINIT_CLASS_H

#include <classes/94/defprops.h>
#include <classes/94/evtlogStructsInit.h>

#endif

// EOF
