/*!
 * @file   meas.h
 * @brief  Declarations for measurement
 * @author Stephen Page
 */

#ifndef MEAS_H
#define MEAS_H

#include <stdint.h>

#include <mugefhw/svec.h>

#define MEAS_DELAY_MS	1

/*!
 * Struct containing global variables
 */

struct Meas
{
    struct svec_card    svec;
    uint32_t            modulator_error_mask;
    uint32_t            timing_error;
    uint32_t            raw_adc[SVEC_NUM_CHANNELS];     // In network byte order
};

extern struct Meas meas;

// External functions

/*!
 * Initialise measurement
 */

int32_t measSvecInit(void);

/*!
 * Acquire measurement
 */

void measSvecAcquire(void);


#endif

// EOF
