/*!
 * @file   fgcddev.h
 * @brief  Declarations for the FGCD device
 * @author Stephen Page
 */

#ifndef FGCDDEV_H
#define FGCDDEV_H

#include <stdint.h>

#include <fgc_stat.h>
#include <parser.h>

// Forward declarations

struct FGCD_device;
struct hash_table;

/*!
 * Struct containing global variables
 */

struct FGCDdev
{
    struct FGCD_device  *device;                            //!< Pointer to device structure
    struct fgc8_stat    status;                             //!< Published data
    struct Parser       parser;                             //!< Parser
    struct hash_table   *const_hash;                        //!< Hash of constants
    struct hash_table   *prop_hash;                         //!< Hash of properties
    uint8_t             test_bin[64];                       //!< Test binary property
    char                test_char[64];                      //!< Test character property
    float               test_float[16];                     //!< Test float property
    int8_t              test_int8s[64];                     //!< Test integer property
    uint8_t             test_int8u[64];                     //!< Test integer property
    int16_t             test_int16s[32];                    //!< Test integer property
    uint16_t            test_int16u[32];                    //!< Test integer property
    int32_t             test_int32s[16];                    //!< Test integer property
    uint32_t            test_int32u[16];                    //!< Test integer property
    const char          *test_string[2];                    //!< Test string property
    const char          *fgc_class_name;                    //!< Class name
    const char          *fgc_platform_name;                 //!< Platform name
    uint8_t             fgc_platform;                       //!< Platform
    uint32_t            number_injections;                  //!< Number of injections retrieved from the telegram (LHC or HIRADMAT)
    uint32_t            count_injections;                   //!< Number of injections so far
    uint32_t            simulate_evt_mask;                  //!< Timing events to be simulated on the next ms
    uint32_t            mbi_qfd_intlk;                      //!< MUGEF.MBI_QFD_INTLK property
};

extern struct FGCDdev fgcddev;

// External functions

/*!
 * Initialise the device
 */

int32_t fgcddevInit(void);

/*!
 * unused, but called by devname.cpp
 */

void fgcddevInitDevice(void);

/*!
 * Publish data for FGCD device
 */

void fgcddevPublish(void);

#endif

// EOF
