//! @file   logInit_class.h
//! @brief  Includes log structure initialisation header file for the class 94


#ifndef LOGINIT_CLASS_H
#define LOGINIT_CLASS_H

#include <classes/94/logStructsInit.h>
#include <classes/94/logMenusInit.h>

#endif

// EOF
