//! @file   8/inc/log.h
//! @brief  Declarations for signal logging

#ifndef LOG_H
#define LOG_H

#include <stdbool.h>
#include <stdint.h>
#include <cclibs.h>

//! Class specific log initialisation
//!
//! @param[in]    channel    address of the DIGMUGEF device

void logInitClass(uint32_t channel);

//! Update the CCLIB logs for a given DIGMUGEF device

uint32_t logSignals(uint32_t channel, const struct CC_us_time * us_time, bool is_200ms_boundary);

//! Update the legacy logs for all DIGMUGEF devices.
//! These logs are the ones used before CCLIBS and are kept for backwards compatibility

void logUpdateLegacyLogs(void);

#endif
