//! @file   state_pc.cpp
//! @brief  Manage Power Converter state and Reference state for DIGMUGEF devices


#include <stdint.h>

#include <classes/94/defsyms.h>
#include <state_pc.h>
#include <equipdev.h>
#include <equip_logger.h>
#include <fgcd.h>
#include <logging.h>
#include <mugefhw/vmodttl.h>
#include <cmwpub.h>
#include <state_vs.h>
#include <mode_pc.h>
#include <reference.h>
#include <timing.h>
#include <fgcddev.h>

// --------------------------------------- STATE FUNCTIONS ----------------------------------------

// STATE: FLT_OFF (FO)

static uint32_t statePcFO(uint32_t channel, bool first_call)
{
    // Force VS_RUN to be off - just in case

    equipdev.device[channel].vs.vsrun = 0;

    return FGC_PC_FLT_OFF;
}



// STATE: OFF (OF)

static uint32_t statePcOF(uint32_t channel, bool first_call)
{
    return FGC_PC_OFF;
}



// STATE: FLT_STOPPING (FS)

static uint32_t statePcFS(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        // Remove VS_RUN to stop the converter

        equipdev.device[channel].vs.vsrun = 0;

        // Trigger an FGC logger post-mortem event

        loggerTriggerPm(channel);
    }

    return FGC_PC_FLT_STOPPING;
}



// STATE: STOPPING (SP)

static uint32_t statePcSP(uint32_t channel, bool first_call)
{
    return FGC_PC_STOPPING;
}



// STATE: STARTING (ST)

static uint32_t statePcST(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Entering state - Set VS_RUN timeout down counter from VS.VSRUN_TIMEOUT property (seconds as a float)

        device.vsrun_timeout = (uint32_t)(device.vs.vsrun_timeout * 1000.0);
        device.vs.vsrun      = 1;
    }
    else if(device.vsrun_timeout > 0 && --device.vsrun_timeout == 0)
    {
        // VS_RUN timeout expired so set VS_RUN_TO fault

        device.status.st_faults |= FGC_FLT_VS_RUN_TO;
    }

    return FGC_PC_STARTING;
}



// STATE: SLOW_ABORT (SA) - LIBREF STATE: TO_OFF


static uint32_t statePcSA(uint32_t channel, bool first_call)
{
    return FGC_PC_SLOW_ABORT;
}



// STATE: TO_STANDBY (TS) - LIBREF STATE: TO_STANDBY

static uint32_t statePcTS(uint32_t channel, bool first_call)
{
    return FGC_PC_TO_STANDBY;
}



// STATE: ON_STANDBY (SB) - LIBREF STATE: STANDBY

static uint32_t statePcSB(uint32_t channel, bool first_call)
{
    return FGC_PC_ON_STANDBY;
}



// STATE: IDLE (IL) - LIBREF STATE: IDLE

static uint32_t statePcIL(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        equipdev.device[channel].ref.run_now = false;
    }

    return FGC_PC_IDLE;
}



// STATE: TO_CYCLING (TC) - LIBREF STATE: TO_CYCLING

static uint32_t statePcTC(uint32_t channel, bool first_call)
{
    return FGC_PC_TO_CYCLING;
}



// STATE: ARMED (AR) - LIBREF STATE: ARMED

static uint32_t statePcAR(uint32_t channel, bool first_call)
{
    if(first_call)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        // Start running if run_now flag is set

        if(device.ref.run_now == true)
        {
            // Pass a run_time of zero to request event in 1s

            struct CC_us_time run_time = { 0 };

            refMgrParValue(&device.ref_mgr, REF_RUN) = run_time;

            refEventRun(&device.ref_mgr);
        }
    }

    return FGC_PC_ARMED;
}



// STATE: RUNNING (RN) - LIBREF STATE: RUNNING

static uint32_t statePcRN(uint32_t channel, bool first_call)
{
    return FGC_PC_RUNNING;
}



// STATE: ABORTING (AB) - LIBREF STATE: TO_IDLE

static uint32_t statePcAB(uint32_t channel, bool first_call)
{
    return FGC_PC_ABORTING;
}



// STATE: CYCLING (CY) - LIBREF STATE: CYCLING

static uint32_t statePcCY(uint32_t channel, bool first_call)
{
    return FGC_PC_CYCLING;
}



// STATE: BLOCKING (BK) - SPS Mains converters do not use blocking

static uint32_t statePcBK(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call && refMgrParValue(&device.ref_mgr,MODE_REF) == REF_OFF)
    {
        // Remove VS_RUN to turn off the converter

        device.vs.vsrun = 0;
    }

    return FGC_PC_BLOCKING;
}



// STATE: ECONOMY (EC) - LIBREG STATES: DYN_ECO and FULL_ECO

static uint32_t statePcEC(uint32_t channel, bool first_call)
{
    return FGC_PC_ECONOMY;
}



// State machine initialization

void statePcInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize channel state to be OFF

    device.status.state_pc   = FGC_PC_FLT_OFF;
    device.state_pc_bit_mask = FGC_PC_FLT_OFF_BIT_MASK;
    device.state_pc_func     = statePcFO;

    // Set default value for LOG.MENUS.MODE_PC_ON (CYCLING for Class 94)

    device.mode_pc_on = FGC_PC_ON_CYCLING;
}



// ------------------------------ TRANSITION CONDITION FUNCTIONS ----------------------------------

// TRANSITION: ANY LIBREF STATE to ANY LIBREF STATE

CC_STATIC_ASSERT(REF_OFF              ==  0, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_POL_SWITCHING    ==  1, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_OFF           ==  2, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_STANDBY       ==  3, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_CYCLING       ==  4, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_TO_IDLE          ==  5, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_DIRECT           ==  6, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_STANDBY          ==  7, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_CYCLING          ==  8, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_DYN_ECO          ==  9, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_FULL_ECO         == 10, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_PAUSED           == 11, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_IDLE             == 12, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_ARMED            == 13, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_RUNNING          == 14, Unexpected_REF_STATE_constant_value);
CC_STATIC_ASSERT(REF_NUM_STATES       == 15, Unexpected_REF_STATE_constant_value);

static State * XXtoXX(uint32_t channel)
{
    // Mapping between libref ref_state and state_pc - this must match the order of the ref state
    // constants, so they are checked above with static assert in case libref is changed in future.

    static State * ref_state_to_state_pc[] =
    {
        statePcBK ,     //   0. [REF_OFF          ]
        statePcFS ,     //   1. [REF_POL_SWITCHING] - not implemented to jump to FLT_STOPPING
        statePcSA ,     //   2. [REF_TO_OFF       ]
        statePcTS ,     //   3. [REF_TO_STANDBY   ]
        statePcTC ,     //   4. [REF_TO_CYCLING   ]
        statePcAB ,     //   5. [REF_TO_IDLE      ]
        statePcFS ,     //   6. [REF_DIRECT       ] - not implemented to jump to FLT_STOPPING
        statePcSB ,     //   7. [REF_STANDBY      ]
        statePcCY ,     //   8. [REF_CYCLING      ]
        statePcEC ,     //   9. [REF_DYN_ECO      ]
        statePcEC ,     //  10. [REF_FULL_ECO     ]
        statePcFS ,     //  11. [REF_PAUSED       ] - not implemented to jump to FLT_STOPPING
        statePcIL ,     //  12. [REF_IDLE         ]
        statePcAR ,     //  13. [REF_ARMED        ]
        statePcRN       //  14. [REF_RUNNING      ]
    };

    struct Equipdev_channel &device = equipdev.device[channel];

    // Look up STATE.PC state function that corresponds to the current libref ref_state

    const State * next_state_pc_func = ref_state_to_state_pc[refMgrVarValue(&device.ref_mgr, REF_STATE)];

    if(next_state_pc_func == statePcFS)
    {
        // libref has returned an unimplemented state for FGClite - jump to FLT_STOPPING

        logPrintf(channel, "libref REF state machine returned unimplemented state %u\n", refMgrVarValue(&device.ref_mgr, REF_STATE));
    }

    // Return pointer to the next state function only if it has changed

    if(next_state_pc_func != device.state_pc_func)
    {
        return next_state_pc_func;
    }

    // Return NULL to indicate that state has not changed

    return NULL;
}



// TRANSITION: ANY STATE to FLT_STOPPING

static State * XXtoFS(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults != 0)
    {
        return statePcFS;
    }

    return NULL;
}



// TRANSITION: FLT_STOPPING to FLT_OFF

static State * FStoFO(uint32_t channel)
{
    if(equipdev.device[channel].vs.state.vs_power_on == false)
    {
        return statePcFO;
    }

    return NULL;
}



// TRANSITION: FLT_STOPPING to STOPPING

static State * FStoSP(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults == 0)
    {
        return statePcSP;
    }

    return NULL;
}



// TRANSITION: STOPPING to OFF

static State * SPtoOF(uint32_t channel)
{
    if(equipdev.device[channel].vs.state.vs_power_on == false)
    {
        return statePcOF;
    }

    return NULL;
}



// TRANSITION: FLT_OFF to OFF

static State * FOtoOF(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults == 0)
    {
        return statePcOF;
    }

    return NULL;
}



// TRANSITION: OFF to FLT_OFF

static State * OFtoFO(uint32_t channel)
{
    if(equipdev.device[channel].status.st_faults != 0)
    {
        return statePcFO;
    }

    return NULL;
}



// TRANSITION: OFF to STARTING

static State * OFtoST(uint32_t channel)
{
    if(refMgrParValue(&equipdev.device[channel].ref_mgr,MODE_REF) != REF_OFF)
    {
        return statePcST;
    }

    return NULL;
}



// TRANSITION: STARTING to STOPPING

static State * STtoSP(uint32_t channel)
{
    if(refMgrParValue(&equipdev.device[channel].ref_mgr,MODE_REF) == REF_OFF)
    {
        return statePcSP;
    }

    return NULL;
}



// TRANSITION: STARTING to BLOCKING

static State * STtoBK(uint32_t channel)
{
    if(equipdev.device[channel].status.state_vs == FGC_VS_READY)
    {
        return statePcBK;
    }

    return NULL;
}



// TRANSITION: BLOCKING to STOPPING

static State * BKtoSP(uint32_t channel)
{
    if(equipdev.device[channel].vs.vsrun == false)
    {
        return statePcSP;
    }

    return NULL;
}



// TRANSITION: ARMED to XX

static State * ARtoXX(uint32_t channel)
{
    State * next_state = XXtoXX(channel);

    if(   next_state == statePcRN
       && refMgrVarValue(&equipdev.device[channel].ref_mgr, REF_FG_STATUS) == FG_PRE_FUNC)
    {
        next_state = NULL;
    }

    return next_state;
}



// State transition functions : called in priority order, from left to right, return the state function.
//
// The initialization order of StateTransitions must match the STATE.PC constant values from the XML symlist.
// This is checked with static assertions.

CC_STATIC_ASSERT(FGC_PC_FLT_OFF       ==  0, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_OFF           ==  1, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_FLT_STOPPING  ==  2, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_STOPPING      ==  3, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_STARTING      ==  4, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_SLOW_ABORT    ==  5, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_TO_STANDBY    ==  6, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ON_STANDBY    ==  7, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_IDLE          ==  8, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_TO_CYCLING    ==  9, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ARMED         == 10, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_RUNNING       == 11, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ABORTING      == 12, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_CYCLING       == 13, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_POL_SWITCHING == 14, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_BLOCKING      == 15, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_ECONOMY       == 16, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_DIRECT        == 17, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_PC_PAUSED        == 18, Unexpected_STATE_PC_constant_value);

#define REF_MAX_TRANSITIONS     3      //!< Maximum transitions from any state

static  Transition * StateTransitions[][REF_MAX_TRANSITIONS + 1] =
{                                                            // LIBREF STATE    Used in Class 94
    /*  0.FO */ {                 FOtoOF,         NULL },    //      NO               YES
    /*  1.OF */ {                 OFtoFO, OFtoST, NULL },    //      NO               YES
    /*  2.FS */ {                 FStoFO, FStoSP, NULL },    //      NO               YES
    /*  3.SP */ { XXtoFS,         SPtoOF,         NULL },    //      NO               YES
    /*  4.ST */ { XXtoFS,         STtoSP, STtoBK, NULL },    //      NO               YES
    /*  5.SA */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  6.TS */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  7.SB */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  8.IL */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /*  9.TC */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 10.AR */ { XXtoFS, ARtoXX,                 NULL },    //      YES              YES
    /* 11.RN */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 12.AB */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 13.CY */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 14.PL */ {                                 NULL },    //      YES              NO
    /* 15.BK */ { XXtoFS, XXtoXX, BKtoSP,         NULL },    //      NO               YES
    /* 16.EC */ { XXtoFS, XXtoXX,                 NULL },    //      YES              YES
    /* 17.DT */ {                                 NULL },    //      YES              NO
    /* 18.PA */ {                                 NULL }     //      YES              NO
};



void statePcRT(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];
    State       * next_state        = NULL;
    Transition ** state_transitions = &StateTransitions[device.status.state_pc][0];
    Transition  * transition;

    // Scan transitions for the current state, testing each condition in priority order

    while((transition = *(state_transitions++)) != NULL &&      // there is another transition function, and
          (next_state = transition(channel)) == NULL)           // it return NULL (transistion condition is false)
    {                                                           // then loop doing nothing
    }

    // If a transition condition was true then switch to the new state

    if(next_state != NULL)
    {
        // Run new state function with first_call flag set to true

        device.status.state_pc = next_state(channel, true);     // State function returns the state enum

        // Cache the state function and bit mask

        device.state_pc_func     = next_state;
        device.state_pc_bit_mask = 1 << device.status.state_pc;

        // Reset the time since last state change

        device.state_pc_time_ms = 0;

        // Log state change

        char   buffer[LOG_MESSAGE_LENGTH + 1];
        size_t pos = 0;

//      pos = sprintf(buffer, "STATE_PC = %s",sym_names_pc[device.status.state_pc].label);

        if((device.state_pc_bit_mask & (FGC_PC_FLT_OFF_BIT_MASK | FGC_PC_FLT_STOPPING_BIT_MASK)) != 0)
        {
            pos += sprintf(buffer+pos, "   st_faults=0x%04hX :",device.status.st_faults);

            // Print the labels for all the fault bits that are set

            for(uint32_t i = 0; sym_names_flt[i].label; ++i)
            {
                if (sym_names_flt[i].type & device.status.st_faults)
                {
                    pos += snprintf(buffer+pos, LOG_MESSAGE_LENGTH-10-pos, " %s", sym_names_flt[i].label);
                }
            }
        }

        if(pos > 0)
        {
            sprintf(buffer+pos, "\n");
            logPrintf(channel, buffer);
        }
    }
    else
    {
        // Increment time in state and run the state function with the first_call flag reset

        device.state_pc_time_ms++;

        device.state_pc_func(channel, false);
    }
}



void statePcSlowAbortInterlock(void)
{
    struct Equipdev_channel &mbi_device = equipdev.device[MBI];
    struct Equipdev_channel &qf_device  = equipdev.device[QF];
    struct Equipdev_channel &qd_device  = equipdev.device[QD];

    bool const mbi_slow_abort =    mbi_device.ref_mgr.latched_faults != 0
                                && (mbi_device.ref_mgr.latched_faults & ~refMgrParValue(&mbi_device.ref_mgr, MODE_TO_OFF_FAULTS)) == 0;

    bool const qf_slow_abort  =    qf_device.ref_mgr.latched_faults != 0
                                && (qf_device.ref_mgr.latched_faults & ~refMgrParValue(&qf_device.ref_mgr, MODE_TO_OFF_FAULTS)) == 0;

    bool const qd_slow_abort  =    qd_device.ref_mgr.latched_faults != 0
                                && (qd_device.ref_mgr.latched_faults & ~refMgrParValue(&qd_device.ref_mgr, MODE_TO_OFF_FAULTS)) == 0;

    // Interlock between the QF and QD is always active

    if(qf_slow_abort == true && qd_slow_abort == false)
    {
        qd_device.ref_mgr.latched_faults |= REF_MBI_QFD_INTLK_BIT_MASK;
    }

    if(qd_slow_abort == true && qf_slow_abort == false)
    {
        qf_device.ref_mgr.latched_faults |= REF_MBI_QFD_INTLK_BIT_MASK;
    }

    // Interlock between the quads and MBI and MBI and the quads if enabled by MUGEF.MBI_QFD_INTLK

    if(fgcddev.mbi_qfd_intlk == CC_ENABLED)
    {
        if(mbi_slow_abort == true)
        {
            // MBI is going to SLOW_ABORT

            if(qd_slow_abort == false)
            {
                qd_device.ref_mgr.latched_faults |= REF_MBI_QFD_INTLK_BIT_MASK;
            }

            if(qf_slow_abort == false)
            {
                qf_device.ref_mgr.latched_faults |= REF_MBI_QFD_INTLK_BIT_MASK;
            }
        }
        else
        {
            // MBI is not going to SLOW_ABORT - request a SLOW_ABORT if either QF or QD are going to SLOW_ABORT

            if(qd_slow_abort == true || qf_slow_abort == true)
            {
                mbi_device.ref_mgr.latched_faults |= REF_MBI_QFD_INTLK_BIT_MASK;
            }
        }
    }
}



void statePcFastAbortInterlock(void)
{
    struct Equipdev_channel &mbi_device = equipdev.device[MBI];
    struct Equipdev_channel &qf_device  = equipdev.device[QF];
    struct Equipdev_channel &qd_device  = equipdev.device[QD];

    bool const mbi_fast_abort = (mbi_device.ref_mgr.latched_faults & ~refMgrParValue(&mbi_device.ref_mgr, MODE_TO_OFF_FAULTS)) != 0;

    bool const qf_fast_abort  = (qf_device.ref_mgr.latched_faults & ~refMgrParValue(&qf_device.ref_mgr, MODE_TO_OFF_FAULTS)) != 0;

    bool const qd_fast_abort  = (qd_device.ref_mgr.latched_faults & ~refMgrParValue(&qd_device.ref_mgr, MODE_TO_OFF_FAULTS)) != 0;

    // Interlock between the QF and QD is always active

    if(qf_fast_abort == true && qd_fast_abort == false)
    {
        qd_device.status.st_faults |= FGC_FLT_VS_EXTINTLOCK;
    }

    if(qd_fast_abort == true && qf_fast_abort == false)
    {
        qf_device.status.st_faults |= FGC_FLT_VS_EXTINTLOCK;
    }

    // Interlock between the quads and MBI and MBI and the quads if enabled by MUGEF.MBI_QFD_INTLK

    if(fgcddev.mbi_qfd_intlk == CC_ENABLED)
    {
        if(mbi_fast_abort == true)
        {
            // MBI is going to FAST_ABORT

            if(qd_fast_abort == false)
            {
                qd_device.status.st_faults |= FGC_FLT_VS_EXTINTLOCK;
            }

            if(qf_fast_abort == false)
            {
                qf_device.status.st_faults |= FGC_FLT_VS_EXTINTLOCK;
            }
        }
        else
        {
            // MBI is not going to FAST_ABORT - request a FAST_ABORT if either QF or QD are going to FAST_ABORT

            if(qd_fast_abort == true || qf_fast_abort == true)
            {
                mbi_device.status.st_faults |= FGC_FLT_VS_EXTINTLOCK;
            }
        }
    }
}

// EOF
