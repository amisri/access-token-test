/*!
 * @file   defsyms.c
 * @brief  File use to intialize the constants at class/94/defsyms.h
 * @author Marc Magrans de Abril
 */

#define DEFSYMS

#include <libfg/fgConsts.h>
#include <libsig.h>
#include "classes/94/defsyms.h"
