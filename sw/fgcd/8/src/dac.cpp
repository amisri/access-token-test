/*
 *  Filename: ref.c
 *
 *  Purpose:  Functions for reference generation
 *
 *  Author:   Stephen Page
 */

#include <string.h>

#include <classes/94/defconst.h>
#include <equipdev.h>
#include <libref.h>
#include <mugef/consts.h>
#include <mugefhw/newave3.h>
#include <dac.h>

#define NEWAVE_SLOT          5
#define DAC_RESOLUTION_BITS  16
#define DAC_V_NOMINAL        9.9997  // see https://edms.cern.ch/document/1727068/1
#define DAC_MAX_V_ERROR      0.5
#define DAC_QS               0
#define DAC_NUM_CHANS        4

/*!
 * Struct containing global variables
 */

static struct Newave_configuration
{
    struct newave3_card     card;                             //!< Newave card
    uint16_t                update_mask;                      //!< Mask for each card of channels with updated values
    int16_t                 raw_reference[NEWAVE3_NUM_CHANS]; //!< Reference that has been set on DAC
} newave;

// All DACs have the same scaling and are not calibrated in software so we can share one sig_dac structure

static struct SIG_dac sig_dac;

int32_t dacInit(void)
{
    // Map Newave card

    if(newave3Map(&newave.card, NEWAVE_SLOT))
    {
        fprintf(stderr, "ERROR: could not initialise Newave DAC card.\n");
        return 1;
    }

    // Initialise sig_dac structure

    sigDacInit(&sig_dac, DAC_RESOLUTION_BITS, DAC_V_NOMINAL, DAC_MAX_V_ERROR);

    // force all 8 newave channels to be initialised to 0

    uint32_t adc_channel;

    for(adc_channel=0; adc_channel<NEWAVE3_NUM_CHANS; adc_channel++)
    {
        newave.raw_reference[adc_channel] = 0;
        newave.update_mask |= 1 << adc_channel;
    }

    // update DACs

    dacApplyReferences();

    return 0;
}



void dacCalculateReference(uint32_t device_index)
{
    // +---------+-------------+--------------+
    // |CONVERTER|MUGEF CHANNEL|NEWAVE_CHANNEL|
    // +---------+-------------+--------------+
    // |      QS |             |            0 |
    // |      QD |           1 |            1 |
    // |      QF |           2 |            2 |
    // |     MBI |           3 |            3 |
    // +---------+-------------+--------------+

    int32_t raw_value = sigDacRT(&sig_dac, refMgrVarValue(&equipdev.device[device_index].ref_mgr, REF_DAC), NULL);

    if(raw_value != newave.raw_reference[device_index])
    {
        newave.raw_reference[device_index] = raw_value;
        newave.update_mask |= 1 << device_index;
    }
}



void dacApplyReferences(void)
{
    // Copy QD or QF raw value to QS DAC when required - QD his priority

    int32_t raw_qs_dac = 0;

    if(equipdev.device[QD].qs_dac == CC_ENABLED)
    {
        raw_qs_dac = newave.raw_reference[QD];
    }
    else if(equipdev.device[QF].qs_dac == CC_ENABLED)
    {
        raw_qs_dac = newave.raw_reference[QF];
    }

    if(raw_qs_dac != newave.raw_reference[DAC_QS])
    {
        // Only update the QS dac when the value changes

        newave.raw_reference[DAC_QS] = raw_qs_dac;
        newave.update_mask |= 1 << DAC_QS;
    }

    // Write raw DAC values to the Newave board

    uint32_t newave_channel;

    for(newave_channel = 0; newave_channel<DAC_NUM_CHANS; newave_channel++)
    {
        // Update DAC if the value changed

        if((newave.update_mask & (1<<newave_channel)) != 0)
        {
            newave.card.ref.value[newave_channel] = htonl((int32_t)newave.raw_reference[newave_channel] << 16);
        }
    }

    // Apply changes

    *newave.card.ref.update_mask = htons(newave.update_mask);
    newave.update_mask = 0;
}

// EOF
