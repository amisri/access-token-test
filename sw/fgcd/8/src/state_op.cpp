/*!
 * @file   state_op.cpp
 * @brief  Manage Operational state for DIGMUGEF devices
 * @author Michael Davis
 * @author Miguel Hermo Serans
 * @author Quentin King
 */

#include <state_op.h>
#include <equipdev.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <cmwpub.h>
#include <libreg.h>


// --------------------------------------- STATE FUNCTIONS ----------------------------------------

// STATE: UNCONFIGURED (UC)

static uint32_t stateOpUC(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured == false && fgcd.device[channel].config_unset_count == 0)
    {
        // Ensure that CCLIBS parameters are valid before transitioning out of UNCONFIGURED state
        // Call set property pars functions

        parserCallPropSetParsFunctions(channel);

        // Process any pending changed libreg parameters

        regMgrParsProcess(&device.reg_mgr);

        device.is_configured = true;
    }
    else if(fgcd.device[channel].config_unset_count != 0)
    {
        // After a DEVICE.RESET, set configured mode back to FALSE

        device.is_configured = false;
    }

    return FGC_OP_UNCONFIGURED;
}



// STATE: NORMAL (NL)

static uint32_t stateOpNL(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Clear latched faults

        device.status.st_faults = 0;

        // Clear Simulation warning

        setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, false);

        // Disable measurement simulation

        refMgrParValue(&device.ref_mgr, MODE_SIM_MEAS) = CC_DISABLED;

        // Reset I_RMS variables in case there is a lingering RMS current from simulation

        regLimRmsResetRT(&device.reg_mgr.lim_i_rms);
        regLimRmsResetRT(&device.reg_mgr.lim_i_rms_load);
    }

    return FGC_OP_NORMAL;
}



// STATE: SIMULATION (SM)

static uint32_t stateOpSM(uint32_t channel, bool first_call)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(first_call)
    {
        // Reset all faults except for FGC_FLT_REG

        device.status.st_faults = (device.unlatched_faults & FGC_FLT_REG);

        // Reset I_RMS variables in case there is a lingering RMS current from normal operation

        regLimRmsResetRT(&device.reg_mgr.lim_i_rms);
        regLimRmsResetRT(&device.reg_mgr.lim_i_rms_load);
    }

    // set simulation warning

    setStatusBit(&device.status.st_warnings, FGC_WRN_SIMULATION, true);

    return FGC_OP_SIMULATION;
}



// State machine initialization

void stateOpInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Initialize channel state to be UNCONFIGURED

    device.status.state_op = FGC_OP_UNCONFIGURED;
    device.state_op_func   = stateOpUC;
}


// ------------------------------ TRANSITION CONDITION FUNCTIONS ----------------------------------

// TRANSITION: ANY STATE to NORMAL

static State* XXtoNL(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured && device.mode_op == FGC_OP_NORMAL)
    {
        return stateOpNL;
    }

    return NULL;
}



// TRANSITION: ANY STATE to SIMULATION

static State* XXtoSM(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    if(device.is_configured && device.mode_op == FGC_OP_SIMULATION)
    {
        return stateOpSM;
    }

    return NULL;
}


// Transition functions : called in priority order, from left to right

CC_STATIC_ASSERT(FGC_OP_UNCONFIGURED ==  0, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_OP_NORMAL       ==  1, Unexpected_STATE_PC_constant_value);
CC_STATIC_ASSERT(FGC_OP_SIMULATION   ==  2, Unexpected_STATE_PC_constant_value);

#define REF_MAX_TRANSITIONS     2      //!< Maximum transitions from any state

static  Transition * StateTransitions[][REF_MAX_TRANSITIONS + 1] =
{
    /* 0. UC */ { XXtoNL, XXtoSM, NULL },
    /* 1. NL */ {         XXtoSM, NULL },
    /* 2. SM */ { XXtoNL,         NULL },
};



void stateOpRT(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Scan transitions for the current operational state, testing each condition in priority order

    State       * next_state = NULL;
    Transition ** state_transitions = &StateTransitions[device.status.state_op][0];
    Transition  * transition;

    while((transition = *(state_transitions++)) != NULL &&      // there is another transition function, and
          (next_state = transition(channel)) == NULL)           // it return NULL (transition condition is false)
    {                                                           // then loop doing nothing
    }

    if(next_state != NULL)
    {
        device.state_op_func = next_state;

        // Transition condition is true to switch to new state

        device.status.state_op = next_state(channel, true);

        // Manage libref MODE SIM_MEAS in case STATE.OP entered or exited from SIMULATION state

        ParsMeasSim(channel);
    }
    else
    {
        // No transition : stay in current state and run the state function with the first_call parameter set to false

        device.state_op_func(channel, false);
    }
}

// EOF
