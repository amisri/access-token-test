#include <unistd.h>

#include <signals.h>
#include <equipdev.h>
#include <logging.h>
#include <nonvol.h>
#include <fgcd_thread.h>

#include "classes/94/sigStructsInit.h"


void signalsInit(uint32_t device_index)
{
    struct Equipdev_channel  &device = equipdev.device[device_index];

    // Check that FGC constants and CCLIBS constants are consistent

    static_assert(FGC_CAL_VRAW_LEN == SIG_NUM_CALS);

    // Call auto-generated function to initialise sig_struct for the device

    sigStructsInitDevice(&device.sig_struct,
                         &device.polswitch_mgr,
                         device_index);

    /*
    * Calibration properties are usually set from non-volatile storage on start-up. Properties which
    * have no value stored are initialised to a default value (usually zero), to allow us to
    * transition from UNCONFIGURED to NORMAL operational state. This is required because calibration
    * cannot take place in the UNCONFIGURED state, because we only start calling refIterationRT()
    * after the device is configured.
    */

    equipdevPropSetNumEls(device_index, 'A', "CAL.?.ADC.EXTERNAL.ERR",       6);
    equipdevPropSetNumEls(device_index, 'B', "CAL.?.ADC.EXTERNAL.ERR",       6);
    equipdevPropSetNumEls(device_index, 'A', "CAL.?.ADC.EXTERNAL.GAIN",      1);
    equipdevPropSetNumEls(device_index, 'B', "CAL.?.ADC.EXTERNAL.GAIN",      1);

}

// Called when user requests a channel to be calibrated

uint32_t signalsCalibrate(uint32_t device_index, enum Cal_Channel cal_channel)
{
    SIG_cal_level calibrated_level;

    struct Equipdev_channel  &device = equipdev.device[device_index];

    struct SIG_adc *adc = NULL;
    uint32_t modified_cal_mask = 0;

    // Select channel and indicate which properties should be updated after calibration
    // depending on the selected cal_channel
    if(cal_channel == CAL_ADC_A)
    {
        adc = &device.sig_struct.adc.named.adc_a;
        modified_cal_mask = (1 << CAL_A_ADC_EXTERNAL_GAIN) | (1 << CAL_A_ADC_EXTERNAL_ERR);
    }
    else if(cal_channel == CAL_ADC_B)
    {
        adc = &device.sig_struct.adc.named.adc_b;
        modified_cal_mask = (1 << CAL_B_ADC_EXTERNAL_GAIN) | (1 << CAL_B_ADC_EXTERNAL_ERR);
    }
    else
    {
        logPrintf(device_index, "Calibration not supported for calibration channel %d", cal_channel);
        return 1;
    }

    // Perform calibration

    calibrated_level = sigAdcCal(&device.sig_struct.mgr, adc);

    if(calibrated_level == SIG_CAL_FAULT)
    {
        logPrintf(device_index, "Calibration failed for ADC_B\n");
        return 1;
    }

    // Store nonvolatile properties

    for(uint32_t i = 0; i < NUM_CAL_NONVOL_PROPS; ++i)
    {
        if(modified_cal_mask & (1 << i))
        {
            nonvolStore(device.fgcd_device, Cal_nonvol_prop_string[i], 0, 0, 1);
        }
    }

    return 0;

}
