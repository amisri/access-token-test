/*!
 * @file   state_vs.cpp
 * @brief  Manage Voltage Source state for DIGMUGEF devices
 * @author Miguel Hermo
 */
#include <state_vs.h>
#include <stdio.h>
#include <unistd.h>

#include <fgcd.h>

#include <consts.h>
#include <classes/94/defsyms.h>      // Import symbol definitions for CMW logging

#include <cclibs.h>
#include <cmwpub.h>
#include <equipdev.h>
#include <mugefhw/vmodttl.h>
#include <logging.h>

enum VS_Channel
{
    VS_INVALID =  0,
    VS_QD      =  1,
    VS_QF      = 17,
    VS_MBI     = 49
};

static bool prev_vsrun[FGCD_MAX_DEVS];

enum State_VS
{
    STATE_VS_INVALID    = FGC_VS_INVALID,
    STATE_VS_FLT_OFF    = FGC_VS_FLT_OFF,
    STATE_VS_FASTPA_OFF = FGC_VS_FASTPA_OFF,  // UNUSED
    STATE_VS_OFF        = FGC_VS_OFF,
    STATE_VS_FAST_STOP  = FGC_VS_FAST_STOP,
    STATE_VS_STOPPING   = FGC_VS_STOPPING,
    STATE_VS_STARTING   = FGC_VS_STARTING,
    STATE_VS_READY      = FGC_VS_READY
};

static enum State_VS vs_state_mapping[] =
{
  //--------------------//-------------//----------//--------------//-----------//
  // STATE.VS           // COMM_ERROR  //   VSRUN  // VS_POWER_ON  // VS_FAULT  //
  //--------------------//-------------//----------//--------------//-----------//
    STATE_VS_OFF,       //          0  //       0  //           0  //        0  //
    STATE_VS_FLT_OFF,   //          0  //       0  //           0  //        1  //
    STATE_VS_STOPPING,  //          0  //       0  //           1  //        0  //
    STATE_VS_FAST_STOP, //          0  //       0  //           1  //        1  //
    STATE_VS_STARTING,  //          0  //       1  //           0  //        0  //
    STATE_VS_INVALID,   //          0  //       1  //           0  //        1  //  <= should not request VSRUN when in fault
    STATE_VS_READY,     //          0  //       1  //           1  //        0  //
    STATE_VS_INVALID,   //          0  //       1  //           1  //        1  //  <= should not request VSRUN when in fault
    STATE_VS_INVALID,   //          1  //       0  //           0  //        0  //
    STATE_VS_INVALID,   //          1  //       0  //           0  //        1  //
    STATE_VS_INVALID,   //          1  //       0  //           1  //        0  //
    STATE_VS_INVALID,   //          1  //       0  //           1  //        1  //
    STATE_VS_INVALID,   //          1  //       1  //           0  //        0  //
    STATE_VS_INVALID,   //          1  //       1  //           0  //        1  //
    STATE_VS_INVALID,   //          1  //       1  //           1  //        0  //
    STATE_VS_INVALID    //          1  //       1  //           1  //        1  //
  //--------------------//-------------//----------//--------------//-----------//
};


static struct vmodttl_card vmod;

static int getVmodAddr(uint32_t device_index)
{
    switch(device_index)
    {
        case(QD):
            return VS_QD;
        case(QF):
            return VS_QF;
        case(MBI):
            return VS_MBI;
        default:
            return VS_INVALID;
    }
}

static enum State_VS getVsState(uint32_t device_index)
{
    Equipdev_channel * device = &equipdev.device[device_index];

    uint32_t state = 0;

    state |= ( device->vs.state.comm_error  << 3 );
    state |= ( device->vs.vsrun             << 2 );
    state |= ( device->vs.state.vs_power_on << 1 );
    state |= ( device->vs.state.vs_fault    << 0 );

    return vs_state_mapping[state];
}

/*
 * Reads the converter status from the VMOD TTL card
 */

static void stateVsReadVmod(uint32_t device_index)
{
    Equipdev_channel * device = &equipdev.device[device_index];

    int error;
    int result;

    device->vs.state.comm_error = 0;

    int vmod_addr = getVmodAddr(device_index);

    if(vmod_addr == VS_INVALID)
    {
        device->vs.state.vs_fault = 1;
        return;
    }

    // Read VMOD faults

    if((error = vmodttlGetNoFault(&vmod, vmod_addr, &result)))
    {
        device->vs.state.comm_error = 1;
        logPrintf(device_index, "STATE Error reading the VMOD TTL (addr %u) fault signals (error = %d)\n", vmod_addr, error);
    }
    else
    {
        device->vs.state.vs_fault = ( result == 0);
    }

    // Read VMOD state

    if((error = vmodttlGetState(&vmod, vmod_addr, &result)))
    {
        device->vs.state.comm_error = 1;
        logPrintf(device_index, "STATE Error reading the VMOD TTL (addr %u) state signals (error = %d)\n", vmod_addr, error);
    }
    else
    {
        device->vs.state.vs_power_on = (result != 0);
    }

}


static void stateVsSimulateRead(uint32_t device_index)
{
    Equipdev_channel * device = &equipdev.device[device_index];

    device->vs.state.vs_fault    = 0;

    device->vs.state.vs_power_on = device->vs.vsrun;
}

/*
 * Sets the vsrun signal through the VMOD TTL card
 *
 * @param device_index    index of mugef device
 * @param vsrun           value of VS_RUN
 *
 * @retval 0 on success, 1 otherwise
 */
static void stateVsSendVmod(uint32_t device_index)
{
    uint32_t error;

    Equipdev_channel * device = &equipdev.device[device_index];

    int vmod_addr = getVmodAddr(device_index);

    if(vmod_addr == VS_INVALID)
    {
        device->vs.state.vs_fault = 1;
        return;
    }

    bool vsrun = device->vs.vsrun;

    if( (error = vmodttlSetState(&vmod, vmod_addr, vsrun)) )
    {
        logPrintf(device_index, "VMOD TTL Command %d to VMOD TTL (addr %u) failed (error = %d)\n", vsrun, vmod_addr, error);
        device->vs.state.comm_error = 1;
    }

    logPrintf(device_index, "VMOD TTL (addr %u) Command sent : vsrun=%d\n", vmod_addr, vsrun);
}


// External Functions

int32_t stateVsInit(void)
{
    uint32_t            device_index;
    Equipdev_channel * device;

    // initialise VMOD TTL card

    if(vmodttlMap(&vmod)!=0 || vmodttlInit(&vmod)!=0 )
    {
        fprintf(stderr, "ERROR: Failed to initialise the VMOD TTL card.\n");
        vmodttlUnmap(&vmod);
        return 1;
    }

    // If VS is on when FGCD starts:
    //
    //   1) Wait 5s for the current to ramp down (DACs are alwas 0 initialized, so the
    //    converter should be already ramping down)
    //
    //   2) Turn off the converter
    //

    uint64_t vs_on_device_mask = 0x0;

    for(device_index=1; device_index<FGCD_MAX_DEVS; device_index++)
    {

        if(fgcd.device[device_index].name == NULL) continue; // skip unused devices

        device = &equipdev.device[device_index];
        device->vs.vsrun = 0;

        stateVsReadVmod(device_index);

        if( device->vs.state.vs_power_on)
        {
            fprintf(stderr, "VS WARNING: (device %d ) converter is not OFF!.\n", device_index);
            vs_on_device_mask |= ((uint64_t) 1 << device_index);
        }
    }

    if(vs_on_device_mask)
    {
        fprintf(stderr, "VS WARNING: Waiting 15s to ramp down before turning converter(s) OFF.\n");

        sleep(15);

        for(device_index=1; device_index<FGCD_MAX_DEVS; device_index++)
        {
            if(vs_on_device_mask & ((uint64_t) 1 << device_index))
            {
                fprintf(stderr, "VS WARNING: (device %d ) Turning converter OFF.\n", device_index );
                stateVsSendVmod(device_index);
            }
        }
    }

    return 0;
}

void stateVsRead(uint32_t device_index)
{
    struct Equipdev_channel * device = &equipdev.device[device_index];

    if(device->mode_op == FGC_OP_SIMULATION)
    {
        stateVsSimulateRead(device_index);
    }
    else
    {
        stateVsReadVmod(device_index);
    }

    device->status.state_vs = getVsState(device_index);;

    // set libref PC STATE based on state_vs

    refMgrParValue(&device->ref_mgr,PC_STATE) = device->status.state_vs == STATE_VS_READY
                                                 ? REF_PC_ON
                                                 : REF_PC_OFF;
}

void stateVsSend(uint32_t device_index)
{
    struct Equipdev_channel * device = &equipdev.device[device_index];

    // send commands to converter

    if(prev_vsrun[device_index] != device->vs.vsrun)
    {
        prev_vsrun[device_index] = device->vs.vsrun;

        if(device->mode_op == FGC_OP_NORMAL)
        {
            stateVsSendVmod(device_index);
        }
        else
        {
            logPrintf(device_index, "[Simulate] VMOD TTL Command : vsrun=%d\n", device->vs.vsrun);
        }
    }
}


// EOF
