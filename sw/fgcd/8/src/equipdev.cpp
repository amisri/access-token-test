//! @file   equipdev.cpp
//! @brief  Functions for equipment devices

#define DEFPROPS
#define DEFPROPS_INC_ALL

#include <equipdev.h>
#include <equip_liblog.h>

#include <classes/94/definfo.h>
#include <classes/94/defprops.h>

#include <cmwpub.h>
#include <fgcd_thread.h>
#include <fgcddev.h>
#include <timing.h>
#include <hash.h>
#include <logging.h>

#include <nonvol.h>
#include <parser.h>
#include <prop.h>
#include <pub.h>

#include <state_pc.h>
#include <state_op.h>
#include <state_vs.h>
#include <mode_pc.h>

#include <fei.h>
#include <dac.h>

#include <log.h>
#include <signals.h>
#include <regulation.h>
#include <reference.h>


#if N_PROP_SYMBOLS > EQUIPDEV_MAX_PROPS
    #error EQUIPDEV_MAX_PROPS must be greater than or equal to N_PROP_SYMBOLS
#endif

// Global variables, zero initialised

struct Equipdev equipdev;



// Initialise the configuration of an equipment device

static uint32_t equipdevInitDevice(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Set start time to be same as FGCD start time

    device.time_start = fgcd.time_start;

    // Set default values

    device.test.strings[0] = "FIRST";
    device.test.strings[1] = "SECOND";

    device.beam_dump = FGC_CTRL_ENABLED;

    // Set default requested operational state to NORMAL (it is non-volatile, so normally it will be set from NVS)

    device.mode_op = FGC_MODE_OP_NORMAL;

    // Initialize libevtlog

    if(ccEvtlogInit(channel) != 0) return 1;

    // Initialize libsig
    //
    // This must be done before reading from non-volatile storage because otherwise
    // defaults would overwrite non-volatile values

    signalsInit(channel);

    // Initilise libreg

    if(regulationInit(channel) != 0) return 1;

    // Initialize libref

    if(referenceInit(channel) != 0) return 1;

    // Initialise logging : This must be done AFTER initialising libref & libreg to make sure pointers are initialised

    ccLogInit(channel);

    // Load device configuration - disable arming until the full configuration has been retrieved

    device.arming_enabled = false;

    nonvolLoad(&fgcd.device[channel], 0);

    device.arming_enabled = true;

    // Check for changes in all libreg parameters

    regMgrParsCheck(&device.reg_mgr);

    // Enable MODE_RT_REF so that AUTO_TUNE will work when enabled

    refMgrParValue(&device.ref_mgr, MODE_RT_REF) = CC_ENABLED;

    // Arm all users from nonvol values

    referenceArmAll(channel);

    // Initialize state machines

    stateOpInit(channel);
    statePcInit(channel);

    // Set the Power Converter mode and state. The state stays at FAULT_OFF while the device is unconfigured.

    device.mode_pc = FGC_PC_OFF;
    refMgrParValue(&device.ref_mgr,MODE_REF) = REF_OFF;

    return 0;
}



int32_t equipdevStart(void)
{
    // Initialise the standard equipdev services (hash tables, parser data, ...)

    equipdevInit(NUM_PARS_FUNC_94, ccEvtlogStoreSetCmd);

    // Set the number of elements in the legacy log

    equipdev.ppm[0].legacy_log.num_elements = FGC_LOG_LEN;

    // Initialise OASIS sampling intervals (1ms)

    equipdev.oasis.i_meas_interval_ns = 1000000;
    equipdev.oasis.i_ref_interval_ns  = 1000000;

    // Initialise Hardware

    if(feiInit() != 0)
    {
        fprintf(stderr, "WARNING: Failed to initialise Fast Extraction Interlock\n");
    }

    if(measSvecInit() != 0)
    {
        fprintf(stderr, "ERROR: Failed to initialise SVEC acquisition board\n");
        return 1;
    }

    if(dacInit() != 0)
    {
        fprintf(stderr, "ERROR: Failed to initialise DACs (check Newave card?)\n");
        return 1;
    }

    // NOTE: VS initialization MUST go after DACs initialisation to make sure that if a
    //       converter is ON, it will be ramping down

    if(stateVsInit() != 0)
    {
        return 1;
    }

    // Initialise each device

    for(uint32_t channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
    {
        equipdev.device[channel].fgcd_device = &fgcd.device[channel];

        if(fgcd.device[channel].name != NULL)
        {
            if(equipdevInitDevice(channel) != 0) return 1;
        }
    }

    return 0;
}



void equipdevCleanUp(void)
{
    // Clear parser data

    parserClearClassData(EQUIPDEV_CLASS_ID);

    // Clean-up

    hashFree(equipdev.const_hash);
    hashFree(equipdev.prop_hash);
}



void equipdevInitDevices(void)
{
    struct FGCD_device  *fgcd_device;
    uint32_t            i;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        fgcd_device = &fgcd.device[i];

        if(fgcd_device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set command queue

            fgcd_device->command_queue = &fgcddev.parser.command_queue;

            // Set device as online

            fgcd_device->online  = 1;
            fgcd_device->ready   = 1;
        }
    }
}

void equipdevPublish(void)
{
    struct Equipdev_channel         *equip_device;
    struct FGCD_device              *fgcd_device;
    uint32_t                        i;
    struct fgc_stat                 *pub_data;
    struct fgc94_stat               *pub_status;

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        equip_device    = &equipdev.device[i];
        fgcd_device     = &fgcd.device[i];
        pub_data        = &pub.published_data.status[i];
        pub_status      = &pub_data->class_data.c94;

        if(fgcd_device->fgc_class == EQUIPDEV_CLASS_ID)
        {
            // Set class in published data

            pub_data->class_id = EQUIPDEV_CLASS_ID;

            // I_REF and V_REF are set in timingMillisecond() to ensure that
            // the values are taken for this millisecond rather than the next one

            // Set measurements in status

            equip_device->status.i_ref    = (float) equip_device->reg_mgr.i.ref_limited;
            equip_device->status.v_ref    = (float) equip_device->reg_mgr.v.ref;
            equip_device->status.ref_meas = (float) equip_device->reg_mgr.v.ref;
            equip_device->status.i_meas   = (float) equip_device->reg_mgr.i.meas.signal[REG_MEAS_EXTRAPOLATED];

            // Copy device status into published data

            memcpy(pub_status, &equip_device->status, sizeof(equip_device->status));

            // Byte-swap statuses

            pub_status->st_faults       = htons(pub_status->st_faults);
            pub_status->st_warnings     = htons(pub_status->st_warnings);
            pub_status->st_unlatched    = htons(pub_status->st_unlatched);

            pub_status->i_ref           = htonf(pub_status->i_ref);
            pub_status->i_meas          = htonf(pub_status->i_meas);
            pub_status->v_ref           = htonf(pub_status->v_ref);
            pub_status->ref_meas        = htonf(pub_status->ref_meas);

        }
    }
}



void equipdevRtAutotune(bool log_events_flag)
{
    // Scan every device to see if autotune is enabled

    for(uint32_t channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        if (device.ppm[timing.user].autotune.enable == FGC_CTRL_ENABLED)
        {
            // Process autotune if MUGEF.AUTO_TUNE.ENABLE(user) is ENABLED

            // Increment autotune_time once the first injection occurs and until the start of the next cycle
            // or max injections have occured

            if ( fgcddev.count_injections > 0  &&
                 fgcddev.count_injections <= FGC_AUTOTUNE_MAX_INJ)
            {
                // Set delta current reference from MUGEF.AUTO_TUNE.STEPS property

                float delta = device.ppm[timing.user].autotune.steps[fgcddev.count_injections-1];

                if (device.autotune_time == 0)
                {
                    // When time is zero, apply the full delta in one jump

                    *refMgrParPointer(&device.ref_mgr, RT_REF) += delta;

                    if(log_events_flag)
                    {
                        logPrintf(channel,"TIMING autotune %u for user %u  MODE_RT_REF=%d   Added %.5f to RT_REF %.5f\n"
                               , fgcddev.count_injections
                               , timing.user
                               , refMgrParValue(&device.ref_mgr, MODE_RT_REF)
                               , delta
                               , *refMgrParPointer(&device.ref_mgr, RT_REF));
                    }
                }
            }

            // Increment the autotune time

            device.autotune_time++;
        }
    }
}



void equipdevRtUpdate(void)
{
    uint32_t channel;
    struct Equipdev_channel *device;

    if(pthread_mutex_trylock(&rt.mutex) == 0)
    {
        for(channel = 0 ; channel < FGCD_MAX_EQP_DEVS ; channel++)
        {
            // Check whether there is a new RT reference for the device

            if(rt.data[rt.data_start].active_channels & (1LL << channel)) // Channel is active
            {
                device = &equipdev.device[channel + 1];

                // Set new value

                *refMgrParPointer(&device->ref_mgr, RT_REF) = rt.used_data.channels[channel] = rt.data[rt.data_start].channels[channel];
            }
        }

        // Clear active channels for this reference and advance data start

        rt.data[rt.data_start].active_channels  = 0;
        rt.data_start = (rt.data_start + 1) % RT_BUFFER_SIZE;

        pthread_mutex_unlock(&rt.mutex);
    }
}


// Add faults to unlatched_flags which are not set directly by the application

void equipdevUpdateFaults(uint32_t device_index)
{

    Equipdev_channel * device = &equipdev.device[device_index];

    uint32_t unlatched_faults = 0;

    // Unconfigured fault

    if(device->status.state_op == FGC_OP_UNCONFIGURED)
    {
        unlatched_faults |= FGC_FLT_FGC_STATE;
    }

    // VS faults

    if(device->status.state_op == FGC_OP_NORMAL)
    {
        if(device->vs.state.comm_error)
        {
            unlatched_faults |= FGC_FLT_COMMS;
        }

        if(device->vs.state.vs_fault)
        {
            unlatched_faults |= FGC_FLT_VS;
        }
    }

    // Regulation and reference errors

    if((refMgrVarValue(&device->ref_mgr,REF_FAULTS_IN_OFF) & REF_I_MEAS_FAULT_BIT_MASK) != 0x0)
    {
        unlatched_faults |= FGC_FLT_MEAS;
    }

    if((refMgrVarValue(&device->ref_mgr, REF_FAULTS_IN_OFF) & ~REF_I_MEAS_FAULT_BIT_MASK) != 0)
    {
        unlatched_faults |= FGC_FLT_REG;
    }

    device->unlatched_faults = unlatched_faults;

    device->status.st_faults |= device->unlatched_faults;
}

// EOF
