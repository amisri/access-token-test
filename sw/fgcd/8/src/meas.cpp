/*
 *  Filename: meas.c
 *
 *  Purpose:  Measurement
 *
 *  Author:   Stephen Page
 */

#include <arpa/inet.h>

#include <equipdev.h>
#include <fgcd.h>
#include <logging.h>
#include <meas.h>
#include <timing.h>
#include <mugef/consts.h>
#include <mugefhw/svec.h>

#include <cclibs.h>

// |Device|Chan|SVEC index|
// |------|----|----------|
// |  MBI |  A |    0     |
// |      |  B |    1     |
// |------|----|----------|
// |  QD  |  A |    2     |
// |      |  B |    3     |
// |------|----|----------|
// |  QF  |  A |    4     |
// |      |  B |    5     |
// |------|----|----------|

#define SVEC_MBI 0
#define SVEC_QD  2
#define SVEC_QF  4

enum ADC_index
{
    ADC_A = 0,
    ADC_B = 1
};

// Global variables

struct Meas meas;

/*
 * Initialise svec acquisition card
 */

int32_t measSvecInit(void)
{
    if (svecMap(&meas.svec))
    {
        return 1;
    }

    return 0;
}



static void measChannel(uint32_t device_index, uint32_t base_svec_chan, ADC_index adc_index)
{
    struct Equipdev_channel &device      = equipdev.device[device_index];
    SIG_adc                 &libsig_adc  = device.sig_struct.adc.array[adc_index];
    uint32_t                svec_channel = base_svec_chan + adc_index;

    // Convert raw_adc value to host byte order

    libsig_adc.raw = (int32_t)ntohl(meas.raw_adc[svec_channel]);

    // liblog's analog signals must be float, so we store
    // a float cast of the raw value into equipdev to be used by liblog

    device.adc[adc_index].raw_float = (float)libsig_adc.raw;

    // Check errors for this channel and increment the error counters

    bool timing_err = meas.timing_error != 0;

    device.adc[adc_index].timing_err      = timing_err;
    device.adc[adc_index].num_timing_err += timing_err;

    bool frame_err = (meas.modulator_error_mask & (1 << svec_channel)) != 0;

    device.adc[adc_index].frame_err      = frame_err;
    device.adc[adc_index].num_frame_err += frame_err;

    // Set the signal fault lag for libsig

    libsig_adc.fault_flag = timing_err || frame_err;
}



void measSvecAcquire(void)
{
    // Read error data from SVEC

    meas.modulator_error_mask = ntohl(meas.svec.regs->modulator_error);
    meas.timing_error         = ntohl(meas.svec.regs->timing_error);

    // Read all the channel values from the SVEC board in one transfer (raw_adc stays in network byte order)

    memcpy(meas.raw_adc, (const void *)meas.svec.regs->ch, sizeof(meas.raw_adc));

    // Process the raw adc and status for each channel of each device

    measChannel(MBI, SVEC_MBI, ADC_A);
    measChannel(MBI, SVEC_MBI, ADC_B);

    measChannel(QD,  SVEC_QD,  ADC_A);
    measChannel(QD,  SVEC_QD,  ADC_B);

    measChannel(QF,  SVEC_QF,  ADC_A);
    measChannel(QF,  SVEC_QF,  ADC_B);
}

// EOF
