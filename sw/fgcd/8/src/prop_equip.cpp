//  Filename: prop_equip.c
//  Purpose:  Equipment device-specific functions for getting and setting properties


#include <classes/94/defprops.h>
#include <fgc_log.h>
#include <equip_logger.h>
#include <signals.h>
#include <mode_pc.h>


// SetIf functions


int32_t SetifCalOk(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return   device->status.state_op == FGC_OP_NORMAL &&
            (device->status.state_pc == FGC_PC_OFF    || device->status.state_pc == FGC_PC_FLT_OFF);
    return 0;
}



int32_t SetifModeOpOk(struct Cmd *command)
{
    struct Equipdev_channel &device = equipdev.device[command->device->id];

    // Check whether device is in a state in which the operational mode may be changed

    return    device.status.state_op != FGC_OP_NORMAL
           || (   device.status.state_pc <= FGC_PC_STOPPING
               && regMgrVarValue(&device.reg_mgr,FLAG_I_MEAS_LOW) == true);
}



int32_t SetifModeRtOk(struct Cmd *command)
{
    // taken care of by cclibs
    return 1;
}



int32_t SetifOpNormalSim(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_op == FGC_OP_NORMAL ||
           device->status.state_op == FGC_OP_SIMULATION;
}



int32_t SetifOpNotCal(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_op != FGC_OP_CALIBRATING;
}



int32_t SetifPcArmed(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_pc == FGC_PC_ARMED;
}



int32_t SetifPcOff(struct Cmd *command)
{
    struct Equipdev_channel *device = &equipdev.device[command->device->id];

    return device->status.state_pc == FGC_PC_OFF ||
           device->status.state_pc == FGC_PC_FLT_OFF;
}



int32_t SetifRegStateNone(struct Cmd *command)
{
    // libreg REG_MODE must be NONE for some properties

    return regMgrVarValue(&equipdev.device[command->device->id].reg_mgr, REG_MODE) == REG_NONE;
}



int32_t SetifSwitchOk(struct Cmd *command)
{
    //struct Equipdev_channel *device = &equipdev.device[command->device->id];
    struct Equipdev_channel &device = equipdev.device[command->device->id];
    //cc_float                &meas_i = device.reg_mgr.i.meas.signal[REG_MEAS_EXTRAPOLATED];

    //TO-DO: setif switch
    //return fabs(*meas_i) * 1000 > (device.load[device.load_select].limits.i.pos) &&  // Measured current is less than 0.1% of I_POS
    return       device.status.state_vs   < FGC_VS_STOPPING;                                            // Voltage source is off
}

// Add log signal headers to command value

static uint32_t propAddLogSignalHeaders(struct Cmd *command, struct prop *property)
{
    uint32_t                    i;
    uint32_t                    n_signals;      // Number of signals
    struct fgc_log_sig_header   *sig_header;    // Pointer to signal header within value

    // Set property-specific log header fields

    switch(property->sym_idx)
    {
        case STP_MEAS:
        case STP_REF:
            n_signals = 1;
            break;

        default: // Function has been called for a property it does not handle
            command->error = FGC_NOT_IMPL;
            return 0;
    }

    // Add signal headers

    for(i = 0 ; i < n_signals ; i++)
    {
        // Check that value buffer has space for log header

        if(command->value_length + sizeof(*sig_header) >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 0;
        }

        // Add signal header to value

        sig_header = (struct fgc_log_sig_header *)&command->value[command->value_length];
        command->value_length += sizeof(*sig_header);

        // Initialise signal header

        memset(sig_header, 0, sizeof(*sig_header));

        sig_header->gain    = 1;
        sig_header->type    = FGC_LOG_TYPE_FLOAT;

        // Set property-specific signal header fields

        switch(property->sym_idx)
        {
            case STP_MEAS:
                sig_header->label_len = sizeof("MEAS") - 1;
                strncpy(sig_header->label, "MEAS", sig_header->label_len);

                sig_header->units_len = sizeof("amps") - 1;
                strncpy(sig_header->units, "amps", sig_header->units_len);

                break;

            case STP_REF:
                sig_header->info = FGC_LOG_SIG_INFO_STEPS;

                sig_header->label_len = sizeof("REF") - 1;
                strncpy(sig_header->label, "REF", sig_header->label_len);

                sig_header->units_len = sizeof("amps") - 1;
                strncpy(sig_header->units, "amps", sig_header->units_len);

                break;

            default: // Function has been called for a property it does not handle
                command->error = FGC_NOT_IMPL;
                return 0;
        }
    }

    return n_signals;
}



int32_t GetLogMugef(struct Cmd *command, struct prop *property)
{
    struct prop             *child;
    uint32_t                from, to;
    uint32_t                i;
    struct fgc_log_header   *log_header = NULL;
    uint32_t                num_children;
    uint32_t                num_elements;
    struct Parser           *parser     = command->parser;
    char                    *value;

    // Use GetParent() if property is a parent and binary get option is not set

    if(property->type == PT_PARENT &&
       !(parser->getopt_flags & GET_OPT_BIN))
    {
        return GetParent(command, property);
    }

    // Get number of elements in log for user

    num_elements = equipdev.ppm[command->user].legacy_log.num_elements;

    // Return if no data is available

    if(num_elements == 0)
    {
        return 0;
    }

    // Validate array indices if property is a parent

    if(property->type == PT_PARENT)
    {
        // Check whether start of array range was specified

        if(parser->from == UNSPECIFIED)
        {
            parser->from = 0;
        }

        // Override index if maximum number of elements is less that actual number of elements

        if(property->max_elements < num_elements)
        {
            parser->from += num_elements - property->max_elements;
        }

        // Check whether end of array range was specified

        if(parser->to == UNSPECIFIED)
        {
            parser->to = num_elements - 1;
        }
        else // to index was specified
        {
            // Override index if maximum number of elements is less that actual number of elements

            if(property->max_elements < num_elements)
            {
                parser->to += num_elements - property->max_elements;
            }

            // Check whether maximum number of elements has been exceeded

            if((parser->to - parser->from) + 1 > property->max_elements)
            {
                command->error = FGC_BAD_ARRAY_IDX;
                return 1;
            }
        }
    }

    // Add log header if binary get option is set

    if(parser->getopt_flags & GET_OPT_BIN)
    {
        // Check that value buffer has space for log header

        if(command->value_length + sizeof(*log_header) >= CMD_MAX_VAL_LENGTH)
        {
            command->error = FGC_RSP_BUF_FULL;
            return 1;
        }

        // Add log header to value

        log_header = (struct fgc_log_header *)&command->value[command->value_length];
        command->value_length += sizeof(*log_header);

        // Initialise log header

        memset(log_header, 0, sizeof(*log_header));

        log_header->info        = htons(1) == 1 ? 0 : FGC_LOG_BUF_INFO_LITTLE_ENDIAN;
        log_header->n_samples   = ((parser->to - parser->from) + 1) / parser->step;
        log_header->period_us   = 1000;
        log_header->version     = FGC_LOG_VERSION;
    }

    // Handle parent or child properties

    if(property->type == PT_PARENT)
    {
        // Process each child property

        num_children = parserGetNumEls(parser);

        // Add signal headers if a log header is present

        if(log_header)
        {
            for(i = 0, child = (struct prop *)parser->value ; i < num_children ; i++, child++)
            {
                log_header->n_signals += propAddLogSignalHeaders(command, child);
                if(command->error) return 1;
            }
        }

        // Add log signals to value

        from        = parser->from;
        to          = parser->to;
        parser->to  = from;
        value       = parser->value;

        for( ; parser->to <= to ; parser->from += parser->step,
                                  parser->to    = parser->from)
        {
            for(i = 0, child = (struct prop *)parser->value ; i < num_children ; i++, child++)
            {
                if(!parserConfigProp(parser, child))
                {
                    parserConfigGet( parser, child);
                }

                // Ignore bad array index error as indices have already been validated

                if(command->error == FGC_BAD_ARRAY_IDX)
                {
                    command->error = 0;
                }

                // Add signal to value

                if(command->error || GetLog(command, child))
                {
                    return 1;
                }

                // Restore value to point to parent value

                parser->value = value;
            }
        }

        // Restore array range

        parser->from    = from;
        parser->to      = to;
    }
    else // Property is not a parent
    {
        // Add signal headers if a log header is present

        if(log_header)
        {
            log_header->n_signals = propAddLogSignalHeaders(command, property);
            if(command->error) return 1;
        }

        // Add log signal to value

        if(GetLog(command, property))
        {
            return 1;
        }
    }

    // Set timestamp in log header if one exists

    if(log_header)
    {
        log_header->unix_time   = command->acq_timestamp.tv_sec;
        log_header->us_time     = command->acq_timestamp.tv_usec;
    }

    return 0;
}


int32_t GetLog(struct Cmd *command, struct prop *property)
{

    struct Parser   *parser             = command->parser;

    uint32_t        from, to;           // Range within array
    int32_t         i;                  // Index within log circular buffer
    int32_t         log_element_size    = sizeof(equipdev.device[0].legacy_log[0]);
    int32_t         log_element         = equipdev.ppm[command->user].legacy_log.index;
    uint32_t        log_from;           // Starting index within log circular buffer
    uint32_t        n;                  // Element index
    char            *value;

    // Set cycle timestamp

    command->cycle_timestamp = equipdev.ppm[command->user].legacy_log.timestamp;

    // Advance value to point to data for user within buffer

    parser->value += log_element * log_element_size;

    // Store parser values to allow them to be restored after manipulation

    from    = parser->from;
    to      = parser->to;
    value   = parser->value;

    // Calculate index within log

    log_from = (log_element + from) % property->max_elements;

    // Get each log element

    parser->from    = 0;
    parser->to      = 0;

    for(i = log_from, n = from ;
        n <= to ;
        i = (i + parser->step) % property->max_elements, n += parser->step)
    {
        parser->value = value + ((i - log_element) * log_element_size);

        switch(property->type)
        {
            case PT_FLOAT:
                GetFloat(command, property);
                break;

            case PT_INT8S:
            case PT_INT8U:
            case PT_INT16S:
            case PT_INT16U:
            case PT_INT32S:
            case PT_INT32U:
                GetInteger(command, property);
                break;

            default:
                command->error = FGC_NOT_IMPL;
                break;
        }

        // Stop if an error has occurred

        if(command->error) break;

        // Add comma to value

        if(!(parser->getopt_flags & GET_OPT_BIN))
        {
            if(command->value_length + 1 >= CMD_MAX_VAL_LENGTH)
            {
                command->error = FGC_RSP_BUF_FULL;
                return 1;
            }
            command->value[command->value_length++] = ',';
        }
    }

    // Restore parser values

    parser->from    = from;
    parser->to      = to;
    parser->value   = value;

    // Decrement value length to remove trailing comma

    if(!(parser->getopt_flags & GET_OPT_BIN))
    {
        command->value_length--;
    }

    // Set acquisition timestamp

    if(command->user) // PPM command
    {
        // Timestamp is cycle time + 'from' milliseconds

        command->acq_timestamp.tv_sec   = command->cycle_timestamp.tv_sec  +  (from / 1000);
        command->acq_timestamp.tv_usec  = command->cycle_timestamp.tv_usec + ((from % 1000) * 1000);

        // Check whether tv_usec is greater than a second (roll over a second)

        if(command->acq_timestamp.tv_usec > 1000000)
        {
            command->acq_timestamp.tv_sec++;
            command->acq_timestamp.tv_usec -= 1000000;
        }
    }
    else // Non-PPM command
    {
        // Timestamp is acquisition time - (log length - 'from') milliseconds

        command->acq_timestamp.tv_sec  -=  ((property->max_elements - 1) - from) / 1000;
        command->acq_timestamp.tv_usec -= (((property->max_elements - 1) - from) % 1000) * 1000;

        // Check whether tv_usec is less than 0 (roll back a second)

        if(command->acq_timestamp.tv_usec < 0)
        {
            command->acq_timestamp.tv_sec--;
            command->acq_timestamp.tv_usec += 1000000;
        }
    }

    return 0;
}


// SET Properties


int32_t SetCal(struct Cmd *command, struct prop *property)
{

    uint8_t            cal_channel;
    char               delimiter;

    uint32_t &channel = command->device->id;

    // Get the calibration channel

    if(propEquipScanSymbolList(&cal_channel, &delimiter, command, property) != 0) return 1;

    // S CAL {ADC_A|ADC_B} don't take a second parameter
    if(delimiter != '\0')
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Request calibration of the specified channel(s)

    switch(cal_channel)
    {
        case FGC_CAL_TYPE_ADC_A:
            signalsCalibrate(channel, CAL_ADC_A);
            break;
        case FGC_CAL_TYPE_ADC_B:
            signalsCalibrate(channel, CAL_ADC_B);
            break;
        case FGC_CAL_TYPE_ADCS:
            signalsCalibrate(channel, CAL_ADC_A);
            signalsCalibrate(channel, CAL_ADC_B);
            break;
        default:
            command->error = FGC_BAD_PARAMETER;
            return 1;
    }

    // calibrate requested channels

    return 0;
}



int32_t SetPeriodIters(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    // Get pointer to number of elements and validate

    const uint32_t offset = sizeof(Equipdev_channel) * channel;

    uint32_t *num_elements = reinterpret_cast<uint32_t*>(reinterpret_cast<char*>(property->num_elements) + offset);

    if((SetInteger(command, property)) == 0)
    {
        if(*num_elements != FGC_N_LOADS)
        {
            command->error = FGC_BAD_ARRAY_LEN;
            return 1;
        }
    }

    return 0;
}



int32_t SetPolaritySwitch(struct Cmd *command, struct prop *property)
{
    command->error = FGC_NOT_IMPL;

    return 1;
}



int32_t SetPC(struct Cmd *command, struct prop *property)
{
    uint32_t &channel = command->device->id;

    uint8_t requested_state;

    if(propEquipScanSymbolList(&requested_state, NULL, command, property) != 0) return 1;

    command->error = modePcSet(channel, requested_state);

    return command->error != FGC_OK_NO_RSP;
}



int32_t SetReset(struct Cmd *command, struct prop *property)
{
    uint32_t                &channel = command->device->id;
    struct Equipdev_channel &device  = equipdev.device[command->device->id];

    // Check the command is S {PROP} RESET

    if(strcasecmp(command->value, "reset") != 0)
    {
        command->error = FGC_BAD_PARAMETER;
        return 1;
    }

    // Property-specific handling

    switch (property->sym_idx)
    {
        case STP_VS:        // VS

            // Clear VS faults

            device.status.st_faults &= ~(FGC_FLT_VS_RUN_TO|FGC_FLT_COMMS);
            break;

        case STP_LOG:       // LOG

            // LOG - Reset FGC logger event.

            loggerReset(channel);
            break;

        case STP_ILC:       // REF.ILC

            refIlcReset(&device.ref_mgr);
            break;

        default:

            // Function has been called for a property it does not handle RESET

            command->error = FGC_NOT_IMPL;
            return 1;
    }

    return 0;
}



void ParsRegPars(uint32_t channel)
{
    // Called when any property associated with a libreg parameter is set

    regMgrParsCheck(&equipdev.device[channel].reg_mgr);
}



void ParsCalFactors(uint32_t channel)
{
    // Called when the following properties are set:
    //
    // CAL.A.ADC.EXTERNAL.GAIN
    // CAL.A.ADC.EXTERNAL.GAIN

}

// EOF
