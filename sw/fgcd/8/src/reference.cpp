//!  @file  reference.cpp
//! @brief  Class specific function related to the use of libref

#include <equipdev.h>


uint32_t referenceInitClass(uint32_t device_index)
{
    struct REF_mgr *ref_mgr = &equipdev.device[device_index].ref_mgr;

    // Initialise default values

    refMgrParValue(ref_mgr, MODE_TO_OFF_FAULTS) = REF_I_MEAS_FAULT_BIT_MASK     |
                                                     REF_I_MEAS_TRIP_BIT_MASK      |
                                                     REF_I_ERR_FAULT_BIT_MASK      |
                                                     REF_I_RMS_FAULT_BIT_MASK      |
                                                     REF_I_RMS_LOAD_FAULT_BIT_MASK |
                                                     REF_V_MEAS_FAULT_BIT_MASK     |
                                                     REF_V_MEAS_TRIP_BIT_MASK      |
                                                     REF_V_ERR_FAULT_BIT_MASK      |
                                                     REF_V_RATE_RMS_FAULT_BIT_MASK |
                                                     REF_MBI_QFD_INTLK_BIT_MASK;

    refMgrParValue(ref_mgr, MODE_REF_CYC_SEL) = CC_ENABLED;
    refMgrParValue(ref_mgr, MODE_PRE_FUNC)    = REF_PRE_FUNC_RAMP;
    refMgrParValue(ref_mgr, MODE_POST_FUNC)   = REF_POST_FUNC_HOLD;

    return 0;
}

// EOF
