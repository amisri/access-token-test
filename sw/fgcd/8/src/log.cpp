//! @file   8/inc/log.cpp
//! @brief  Class 8/94 liblog integration

#include <fgcd.h>
#include <parser.h>
#include <fgc_errs.h>
#include <equipdev.h>
#include <equip_liblog.h>
#include <logging.h>
#include <cmwpub.h>
#include <timing.h>

#include <classes/94/defconst.h>

void logInitClass(uint32_t channel)
{
}



uint32_t logSignals(uint32_t channel, const struct CC_us_time * const us_time, bool is_200ms_boundary)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(*us_time);

    ccLogSetDebugSignals(channel, us_time->us);

    logStoreContinuousRT(&device.log.structs.log[LOG_ACQ],      iter_ns_time);
    logStoreContinuousRT(&device.log.structs.log[LOG_I_MEAS],   iter_ns_time);
    logStoreContinuousRT(&device.log.structs.log[LOG_V_REF],    iter_ns_time);

    logStoreContinuousRT(&device.log.structs.log[LOG_FAULTS],   iter_ns_time);
    logStoreContinuousRT(&device.log.structs.log[LOG_WARNINGS], iter_ns_time);
    logStoreContinuousRT(&device.log.structs.log[LOG_FLAGS],    iter_ns_time);

    // I_REG is logged every regulation period

    if(regMgrVarValue(&device.reg_mgr,IREG_ITER_INDEX) == 0)
    {
        logStoreContinuousRT(&device.log.structs.log[LOG_I_REG], iter_ns_time);
    }

    if(is_200ms_boundary)
    {
        // Store limits values at 5Hz

        logStoreContinuousRT(&device.log.structs.log[LOG_LIMITS], iter_ns_time);

        // Update log menus to get changes in status and reg period

        logMenuUpdate(&device.log.mgr, &device.log.menus);
    }

    // Store ILC RMS error signals when requested by refIlcStateToCycling()

    if(refMgrVarValue(&device.ref_mgr,ILC_LOG_TIME_STAMP).secs.abs != 0.0)
    {
        logStoreContinuousRT(&device.log.structs.log[LOG_ILC_CYC], refMgrVarValue(&device.ref_mgr,ILC_LOG_TIME_STAMP));

        // Acknowledge by zeroing the time stamp

        refMgrVarValue(&device.ref_mgr,ILC_LOG_TIME_STAMP).secs.abs = 0;
    }

    return 0;
}



void logUpdateLegacyLogs(void)
{
    struct Equipdev_channel *device;
    uint32_t                i;
    static uint32_t         log_cycle_index         = 0;    // Index within log buffers of start of cycle that is currently being logged
    static uint32_t         log_cycle_num_elements  = 0;    // Index within log buffers of start of cycle that is currently being logged
    static struct timeval   log_cycle_timestamp;            // Timestamp of start of cycle that is currently being logged
    static uint32_t         log_user                = 0;    // User for logged data (this may be delayed with respect to the timing user)
    uint32_t                user;
    // Check whether this is the start of a new logging cycle

    if(timing.cycle_ms ==  MEAS_DELAY_MS) // Start of a new cycle for logging
    {
        // Set parameters for completed cycle

        if(log_user)
        {
            // Check whether start of cycle is in log

            if(log_cycle_num_elements <= FGC_LOG_LEN) // Full cycle is in log
            {
                equipdev.ppm[log_user].legacy_log.index        = log_cycle_index;
                equipdev.ppm[log_user].legacy_log.num_elements = log_cycle_num_elements;
                equipdev.ppm[log_user].legacy_log.timestamp    = log_cycle_timestamp;
            }
            else // Start of cycle was before beginning of log
            {
                // Set number of elements to zero to indicate no data

                equipdev.ppm[log_user].legacy_log.num_elements = 0;
                equipdev.ppm[log_user].legacy_log.timestamp    = log_cycle_timestamp;
            }

            // Send notifications that properties have been updated

            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.DAC.MEAS", log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.DAC.REF",  log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.DAC",      log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 1, 0);

            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.I.MEAS",   log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.I.REF",    log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.I",        log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 1, 0);

            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_MEAS.DATA",  log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_MEAS",       log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_REF.DATA",   log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);
            cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "LOG.OASIS.I_REF",        log_user, 0, equipdev.ppm[log_user].legacy_log.num_elements, 0, 0);

        }

        // Set parameters to start logging new cycle

        log_user                = timing.user;
        log_cycle_index         = equipdev.ppm[0].legacy_log.index;
        log_cycle_num_elements  = 0;
        timingGetUserTime(log_user, &log_cycle_timestamp);
    }

    // Check whether the start of a cycle is about to be overwritten

    for(user = 1 ; user < NUM_CYC_SELECTORS ; user++)
    {
        if(equipdev.ppm[user].legacy_log.index == equipdev.ppm[0].legacy_log.index)
        {
            // Set number of elements to zero to indicate no data

            equipdev.ppm[user].legacy_log.num_elements = 0;

            // Only a single user can become older than the last log entry
            // each millisecond, so stop searching

            break;
        }
    }

    // Update logged data for each device

    for(i = 1 ; i <= FGCD_MAX_EQP_DEVS ; i++)
    {
        device = &equipdev.device[i];
        device->legacy_log[equipdev.ppm[0].legacy_log.index].i_ref    = (float) regMgrVarValue(&device->reg_mgr, I_REF_DELAYED);
        device->legacy_log[equipdev.ppm[0].legacy_log.index].ref_meas = (float) regMgrVarValue(&device->reg_mgr, I_REF_DELAYED);
        device->legacy_log[equipdev.ppm[0].legacy_log.index].i_meas   = (float) regMgrVarValue(&device->reg_mgr, MEAS_I_UNFILTERED);
    }

    // Increment number of elements in cycle currently being acquired

    log_cycle_num_elements++;

    // Set non-PPM index to oldest entry in log buffer

    equipdev.ppm[0].legacy_log.index = (equipdev.ppm[0].legacy_log.index + 1) % FGC_LOG_LEN;
}

// EOF
