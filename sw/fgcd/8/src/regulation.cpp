/*!
 * @file   cc_reg.cpp
 * @brief  Initialise the CCLIBS regulation manager structures
 * @author Michael Davis
 * @author Miguel Hermo Serans
 */

#include <pthread.h>
#include <equipdev.h>
#include <regulation.h>
#include <dac.h>
#include <fgcd_thread.h>
#include <logging.h>

/*
 * DIGMUGEF iteration period is 1ms
 */

const uint32_t  CC_FILTER_BUF_LEN  = (3 * FGC_MAX_PERIOD_ITERS); // reg_meas_filter::fir_length[0] + reg_meas_filter::fir_length[1] + reg_meas_filter::extrapolation_len_iters
const uint32_t  ITER_PERIOD_US     = 1000;                       // 1kHz iteration rate
const float     MEAS_DELAY_ITERS   = 1.05;                       // ADC22 bit delay (50us for analogue LPF, 1ms for FPGA filter)



// External functions

uint32_t regulationInit(uint32_t channel)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Check that FGC constants and CCLIBS constants are consistent

    CC_STATIC_ASSERT(FGC_N_LOADS      == REG_NUM_LOADS,FGC_N_LOADS_must_equal_REG_NUM_LOADS);
    CC_STATIC_ASSERT(FGC_N_RST_COEFFS == REG_NUM_RST_COEFFS,FGC_N_RST_COEFFS_must_equal_REG_NUM_RST_COEFFS);

    // Initialize reg_pars mutex

    if(fgcd_mutex_init(&device.reg_pars_mutex, EQUIPDEV_MUTEX_POLICY) != 0)
    {
        fprintf(stderr,"ERROR: regulationInit:fgcd_mutex_init(&device.reg_pars_mutex) failed for channel %u\n", channel);
        return 1;
    }

    // Set default parameter values

    regMgrInit( &device.reg_mgr,
                &device.reg_pars,
                NULL,                   // deco_shared
                &device.reg_pars_mutex,
                (REG_mutex_callback *)pthread_mutex_lock,
                (REG_mutex_callback *)pthread_mutex_unlock,
                NULL,                   // v_ac_buf
                0,
                CC_DISABLED,            // Voltage regulation
                CC_ENABLED,             // Current regulation
                CC_DISABLED);           // Field regulation

    // Initialise current measurement FIR buffer

    regMeasFilterInitBuffer(&device.reg_mgr.i.meas, static_cast<int32_t*>(calloc(CC_FILTER_BUF_LEN, sizeof(int32_t))), CC_FILTER_BUF_LEN);

    // Initialise constant parameter values (must be done after call to regMgrInit)

    regMgrParAppValue(&device.reg_pars, VS_ACTUATION)       = REG_VOLTAGE_REF;
    regMgrParAppValue(&device.reg_pars, MEAS_V_DELAY_ITERS) = MEAS_DELAY_ITERS;

    // Set default value for I_SAT_GAIN for all loads

    for(uint32_t load_select = 0 ; load_select < REG_NUM_LOADS ; load_select++)
    {
        regMgrParAppValue(&device.reg_pars, LOAD_I_SAT_GAIN)[load_select] = 1.0;
    }

    // Link libreg to liblog to keep the current regulation log period correct

    regMgrInitRegPeriodPointers(&device.reg_mgr, NULL, &device.log.mgr.period[LOG_I_REG].ns);

    return 0;
}



static void regulationSetDcctStatus(uint32_t channel, uint32_t dcct_channel, uint8_t *dcct_status)
{
    struct Equipdev_channel &device   = equipdev.device[channel];
    struct SIG_transducer   &dcct     = device.sig_struct.transducer.array[dcct_channel];
    const uint32_t          &faults   = dcct.meas.status.faults;
    const uint32_t          &warnings = dcct.meas.status.warnings;

    if(dcct.fault_flag || faults)
    {
        *dcct_status |= FGC_DCCT_FAULT;
    }

    // CAL_FLT

    if(faults & SIG_TRANSDUCER_CAL_FLT_BIT_MASK)           *dcct_status |= FGC_DCCT_CAL_FLT;

    // If there are not warnings and no faults then the status should be ok

    if(!warnings && !faults)
    {
        *dcct_status = FGC_DCCT_MEAS_OK;
    }

}



void regulationRegChannel(uint32_t channel, struct CC_us_time * iter_time, bool is_200ms_boundary)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // set DCCT status

    regulationSetDcctStatus(channel, 0, &device.dcct.st_dcct_a);
    regulationSetDcctStatus(channel, 1, &device.dcct.st_dcct_b);

    // RT cclibs signal management

    if(device.status.state_op != FGC_OP_UNCONFIGURED)
    {
        struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(*iter_time);

        sigMgrRT(&device.sig_struct.mgr, is_200ms_boundary, iter_ns_time);

        // Set new measurements in libReg

        regMgrMeasSetImeasRT(&device.reg_mgr, device.sig_struct.select.named.i_meas.meas.unfiltered);
        regMgrMeasSetVmeasRT(&device.reg_mgr, device.sig_struct.transducer.named.v_meas.meas.unfiltered);

        // Run CCLIBS real-time activity for this iteration

        refRtRegulationRT(&device.ref_mgr, iter_ns_time);

        refRtStateRT(&device.ref_mgr);

        // Run the ILC state machine - this can be run in the background, but there is enough
        // processing power to run it in the real-time thread

        refIlcState(&device.ref_mgr);
    }

    // calculate DAC reference to be applied next cycle

    dacCalculateReference(channel);
}

// EOF
