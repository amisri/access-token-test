/*
 *  Filename: timing_handlers.c
 *
 *  Purpose:  Functions for handling timing events
 *
 *  Author:   Stephen Page
 *  Author:   Miguel Hermo
 */

// __STDC_LIMIT_MACROS must be definied in C++ to make stdint.h define macros such as UINT32_MAX

#define __STDC_FORMAT_MACROS

#define __STDC_LIMIT_MACROS

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <timing.h>
#include <timing_handlers.h>

#include <consts.h>
#include <equipdev.h>
#include <equip_logger.h>
#include <fgcddev.h>

#include <classes/94/defconst.h>
#include <cmwpub.h>
#include <fgcd.h>
#include <fgcddev.h>
#include <logging.h>
#include <prop_equip.h>
#include <pm.h>
#include <pub.h>
#include <rt.h>

#include <fei.h>
#include <meas.h>
#include <dac.h>

#include <libreg/regVars.h>

#include <mode_pc.h>
#include <state_op.h>
#include <state_pc.h>
#include <state_vs.h>
#include <signals.h>
#include <log.h>
#include <reference.h>
#include <regulation.h>



void timingAddMultiPpmEvent(const char *mppm_condition)
{
    ; // Multi-PPM not supported in this class
}



void timingTelegramReady(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    static uint32_t old_cyclenum = timing.field_value[TIMING_FIELD_CYCLE_NUM];
    static Timing::Value value;

    // Only SPS timing allowed for class 8

    if (fgcd.machine != MACHINE_SPS)
    {
        return;
    }

    // Read all the relevant fields for the basic period event

    // CYCLE_NB (aka CYCLENUM)

    try
    {
        static uint32_t old_cyclenum = timing.field_value[TIMING_FIELD_CYCLE_NUM];

        evt_value->getFieldValue("CYCLE_NB", &value);
        timing.field_value[TIMING_FIELD_CYCLE_NUM] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag && old_cyclenum != timing.field_value[TIMING_FIELD_CYCLE_NUM])
        {
            logPrintf(0,"Event %s Field CYCLE_NB (aka CYCLENUM) = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_CYCLE_NUM]);

            old_cyclenum = timing.field_value[TIMING_FIELD_CYCLE_NUM];
        }

        // Set cycle number

        timing.cycle_num = timing.field_value[TIMING_FIELD_CYCLE_NUM] & 0x000000FF;
    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // INJECTED_BATCHES

    try
    {
        evt_value->getFieldValue("INJECTED_BATCHES", &value);

        timing.field_value[TIMING_FIELD_INJECTED_BATCHES] = static_cast<uint32_t>(value.getAsUshort());

        if(timing.log_events_flag && old_cyclenum != timing.field_value[TIMING_FIELD_CYCLE_NUM])
        {
            logPrintf(0,"Event %s Field INJECTED_BATCHES (aka NUMBER INJECTIONS) = %u\n",
                evt_value->getName().c_str(),
                timing.field_value[TIMING_FIELD_INJECTED_BATCHES]);
        }

    }
    catch(std::exception& e)
    {
        // Nothing to be done
    }

    // MACHINE_MODE

    try
    {
        evt_value->getFieldValue("MACHINE_MODE", &value);
        timing.field_value[TIMING_FIELD_MACHINE_MODE] = static_cast<uint32_t>(value.getAsUshort());

        uint32_t                channel;
        struct Equipdev_channel *device;

        // Put all devices that are in valid states to TO_CYCLING
        for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
        {
            device = &equipdev.device[channel];

            if(timing.field_value[TIMING_FIELD_MACHINE_MODE] == TIMING_MACHINE_MODE_FULLECO)
            {
                refMgrParValue(&device->ref_mgr,ECONOMY_FULL) = CC_ENABLED;
            }
            else
            {
                refMgrParValue(&device->ref_mgr,ECONOMY_FULL) = CC_DISABLED;
            }
        }

        if(timing.log_events_flag)
        {
            logPrintf(0,"Event %s Field MACHINE_MODE = %s (%u)\n",
                evt_value->getName().c_str(),
                value.getAsString().c_str(),
                timing.field_value[TIMING_FIELD_MACHINE_MODE]);
        }

    }
    catch(std::exception& e)
    {
        //Nothing to be done
    }
}

static void timingStartCycle(struct CC_us_time * iter_time)
{
    uint32_t        channel;
    struct CC_ns_time const iter_ns_time = cctimeUsToNsRT(*iter_time);

    // Reset the injection counter and number every start of the cycle

    fgcddev.count_injections = 0;

    // Reset the real time reference

    pthread_mutex_lock(&rt.mutex);

    for(channel = 1 ; channel <= FGCD_MAX_EQP_DEVS ; channel++)
    {
        refMgrParValue(&equipdev.device[channel].ref_mgr,RT_REF) = 0.0;
    }

    pthread_mutex_unlock(&rt.mutex);

    for(channel = 1; channel <= FGCD_MAX_EQP_DEVS; channel ++)
    {
        struct Equipdev_channel &device = equipdev.device[channel];

        if(timing.user && device.fgcd_device->name != NULL)
        {
            logStoreStartContinuousRT(device.log.read.log, iter_ns_time, timing.user);
        }
    }

    // The number of FEI per cycle are limited, so we restart the counter
    // at the beginning of the cycle

    equipdev.ppm[timing.user].fei.num_occurrences = 0;
}

/*
 * Calculate cycle timing
 */

static void timingCalcCycle(tTimingTime evt_time,
                            struct CC_us_time * iter_time,
                            bool * is_200ms_boundary,
                            bool * is_1s_boundary)
{
    // set time for the current FGCD iteration

    const uint32_t ms = evt_time.time.tv_nsec / NS_PER_MS;   // Millisecond within UTC second

    struct timeval time = evtTimeToTimeval(evt_time);
    timingSetUserTime(0, &time);

    // Set cycle time

    iter_time->secs.abs = time.tv_sec;
    iter_time->us = time.tv_usec;

    // Calculate timing boundaries

    *is_200ms_boundary = (ms % 200 == 0);
    *is_1s_boundary    = (ms       == 0);

    // Decrement time until the next cycle

    if(timing.next_cycle_countdown_ms != UINT32_MAX)
    {
        timing.next_cycle_countdown_ms--;
    }

    // Check whether this is millisecond zero of a new cycle

    if(!timing.next_cycle_countdown_ms) // New cycle
    {
        timing.cycle_ms = 0;

        // Set user

        timing.prev_user    = timing.user;
        timing.user         = timing.next_user;
        timing.next_user    = 0;

        timing.next_ms_cycle_ms = timing.user ? 1 : 0; // Fix next cycle ms at 0 until the user is known
        timing.next_ms_user     = timing.user;

        // Store the time at which the user started

        if(timing.user)
        {
            timingSetUserTime(timing.user,  &time);
        }

        // Launch the start cycle event

        timingStartCycle(iter_time);

        // Log the start of the cycle if logging of timing events is enabled

        if(timing.log_events_flag)
        {
            logPrintf(0, "TIMING Expected Start cycle, user %s (%u)\n", timingUserName(timing.user), timing.user);
        }

    }
    else // This is not the first millisecond of a new cycle
    {
        // Increment millisecond within cycle if user is set

        if(timing.user)
        {
            timing.cycle_ms++;
        }

        // Set millisecond and user for next millisecond

        if(timing.next_cycle_countdown_ms == 1) // Last millisecond of a cycle
        {
            // Log the end of the cycle if logging of timing events is enabled

            if(timing.log_events_flag)
            {
                logPrintf(0, "TIMING Expected Cycle last ms, user %hhu, ms %u\n", timing.user, timing.cycle_ms);
            }

            timing.next_ms_cycle_ms = 0;
            timing.next_ms_user     = timing.next_user;
        }
        else // This is not the last millisecond of a cycle
        {
            timing.next_ms_cycle_ms = timing.user ? timing.cycle_ms + 1 : 0;
            timing.next_ms_user     = timing.user;
        }
    }
}

static void updatePublishedData()
{
    struct timeval time;
    timingGetUserTime(0,  &time);

    const uint32_t ms = time.tv_usec / US_PER_MS;   // Millisecond within UTC second


    if(!(ms % FGCD_CYCLE_PERIOD_MS))
    {
        // Timestamp published data

        pub.published_data.time_sec     = htonl(time.tv_sec);
        pub.published_data.time_usec    = htonl(time.tv_usec);
    }

    // Publish data meas.delay_ms after start of FGCD cycle
    // (1 ms after FGCD cycle start)
    if(ms % FGCD_CYCLE_PERIOD_MS == 1)
    {
        // Update published data for FGCD device

        fgcddevPublish();

        // Update published data for equipment devices

        equipdevPublish();

        // Trigger sending of published data

        pubTrigger();
/*
        // Handle post-mortem acquisition for each device

        if(fgcd.enable_pm)
        {
            uint32_t  channel;

            for(channel = 1 ; channel < FGCD_MAX_DEVS ; channel++)
            {
                struct PM_acq_info *pm_info = &fgcd.device[channel].pm_self.acq_info;

                // Check whether post-mortem acquisition is in progress

                if(pm_info->wait_cycles)
                {
                    // Check whether wait cycles have run out

                    if(!--pm_info->wait_cycles) // Wait cycles are zero
                    {
                        // Freeze post-mortem buffer

                        pm_info->frozen         = 1;
                        pm_info->wait_cycles    = 0;

                        // Post semaphore to post-mortem thread

                        sem_post(&pm.sem);
                    }
                }
            }
        }
*/
    }

    // Notify change in measurement

    if(ms % 200 == 1)
    {
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS",           0, 0, 0, 0, 0);
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS.I",         0, 0, 0, 0, 0);
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS.I.VALUE",   0, 0, 0, 0, 0);
        cmwpubNotify(CMWPUB_NOTIFY_ALL_EQUIPDEVS, "MEAS.I.REF",     0, 0, 0, 0, 0);
    }
}


static void timingStateManagement(uint32_t channel, struct CC_us_time * iter_time, bool is_200ms_boundary, bool is_1s_boundary)
{
    struct Equipdev_channel &device = equipdev.device[channel];

    // Call pars functions to update parameters

    parserCallPropSetParsFunctions(channel);

    // Check converter state and request dump if necessary

    feiCheckState(channel);

    // RT cclibs signal management

    if(is_200ms_boundary)
    {
        // Non-RT signal management
        // This should normally be called from another thread, but as was the case in
        // FGCLite the workload is low enough that we don't bother with the extra complexity

        sigMgr(&device.sig_struct.mgr, is_1s_boundary);
    }

    stateVsRead(channel);

    equipdevUpdateFaults(channel);

    stateOpRT(channel);

    statePcRT(channel);

    stateVsSend(channel);

    // Process any pending changed libreg parameters

    regMgrParsProcess(&device.reg_mgr);

    // log signals

    logSignals(channel, iter_time, is_200ms_boundary);

    // Process logger events

    loggerProcess(channel);

    ccEvtlogStoreProperties(channel);
}

static void timingSimulateEvents(tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t    evt_index, handler_index;
    char const *evt_name;

    // If we receive a simulate event command between the following two lines we'll miss it.
    // however that's both unlikely and non critical so we don't bother with semaphores here

    uint32_t evt_mask = fgcddev.simulate_evt_mask;

    fgcddev.simulate_evt_mask = 0;

    for(evt_index = 0; evt_index < TIMING_NUM_HANDLERS; evt_index++)
    {
        if(evt_mask & (1 << evt_index))
        {
            // flag is set for this event, search corresponding handler
            evt_name = timing_handler_names[evt_index];

            for(handler_index = 0 ;
            timing_event_handlers[handler_index].function         &&
            timing_event_handlers[handler_index].name != evt_name
            ; handler_index++);

            // Check that we are not out of bounds

            if(!timing_event_handlers[handler_index].function)
            {
                logPrintf(0,"TIMING Event '%s' not found in timing handlers\n", evt_name);
                continue;
            }

            auto handler = &timing_event_handlers[handler_index];

            // For the additional timing info we are going to use first one - if it exists - otherwise a default-constructed one
            Timing_event_info info;

            if (!handler->timing_events.empty())
            {
                info = handler->timing_events.begin()->second;
            }

            // Call event handler
            timing_event_handlers[handler_index].function(handler, info, evt_time, evt_value);

            logPrintf(0,"Simulating timing event: %s\n", evt_name);
        }
    }

}

void timingMillisecond(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Calculate cycle timing
    struct CC_us_time iter_time;
    bool is_200ms_boundary;
    bool is_1s_boundary;

    timingSimulateEvents(evt_time, evt_value);

    timingCalcCycle(evt_time, &iter_time, &is_200ms_boundary, &is_1s_boundary);

    // Check timing status and set fault if necessary

    timingCheckStatus();

    // Acquire measurements

    measSvecAcquire();

    // Update real-time reference

    equipdevRtAutotune(timing.log_events_flag);

//    equipdevRtUpdate();

    regulationRegChannel(MBI, &iter_time, is_200ms_boundary);
    regulationRegChannel(QF,  &iter_time, is_200ms_boundary);
    regulationRegChannel(QD,  &iter_time, is_200ms_boundary);

    dacApplyReferences();

    statePcSlowAbortInterlock();
    statePcFastAbortInterlock();

    timingStateManagement(MBI, &iter_time, is_200ms_boundary, is_1s_boundary);
    timingStateManagement(QF,  &iter_time, is_200ms_boundary, is_1s_boundary);
    timingStateManagement(QD,  &iter_time, is_200ms_boundary, is_1s_boundary);

    feiCheckDumpRequest(); // FEI request beam dump if necessary

    logUpdateLegacyLogs();

    updatePublishedData();
}



void timingFEI(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    // Do nothing if the maximum number of FEI events have already occurred for this cycle

    if(equipdev.ppm[timing.user].fei.num_occurrences == FGC_FEI_MAX_OCCURRENCES)
    {
        logPrintf(0, "TIMING Maximum number of FEI events %u exceeded for user %u\n",
                  FGC_FEI_MAX_OCCURRENCES, timing.user);
        return;
    }

    // Perform FEI check

    feiCheckValues();

    // Increment number of FEI occurrences for this cycle

    equipdev.ppm[timing.user].fei.num_occurrences++;
}



void timingInjectionWarning(struct Timing_event_handler *handler, Timing_event_info info, tTimingTime evt_time, Timing::EventValue *evt_value)
{
    uint32_t channel;
    const struct timeval timestamp = evtTimeToTimeval(evt_time);

    // If TEST.UINT8[0] is non-zero or INJECTED BATCHES from timing telegram is greater than
    // the number of injections so far this cycle

    if (   fgcddev.test_int8u[0]
        || timing.field_value[TIMING_FIELD_INJECTED_BATCHES] > fgcddev.count_injections)
    {
        // increase the injection count

        ++fgcddev.count_injections;

        if (timing.log_events_flag)
        {
            logPrintf(0,"TIMING Event '%s' injection number %u/%u USER %s (%u)\n"
                , evt_value->getName().c_str()
                , fgcddev.count_injections
                , timing.field_value[TIMING_FIELD_INJECTED_BATCHES]
                , timingUserName(timing.user)
                , timing.user);
        }

        // Check for all equipment devices if the automatic tuning is enabled

        for(channel=1; channel <= FGCD_MAX_EQP_DEVS; ++channel)
        {
            struct Equipdev_channel &device = equipdev.device[channel];

            ccEvtLogStoreTimingEvent(channel, &timestamp, "INJECTION_WARNING", timing.user);

            if (device.ppm[timing.user].autotune.enable == FGC_CTRL_ENABLED)
            {
                // Initialise the autotune time to zero to trigger step immediately

                device.autotune_time = 0;

                if(timing.log_events_flag)
                {
                    logPrintf(channel,"TIMING autotune_time set to %d\n",device.autotune_time);
                }
            }
        }
    }
    else if(timing.log_events_flag)
    {
        logPrintf(0,"TIMING Event '%s' ignored (%u/%u) USER %s (%u)\n"
            , evt_value->getName().c_str()
            , fgcddev.count_injections
            , timing.field_value[TIMING_FIELD_INJECTED_BATCHES]
            , timingUserName(timing.user)
            , timing.user);
    }
}


// Define table with all the timing handlers supported by the class

struct Timing_event_handler timing_event_handlers[]
= {
    { timing_handler_names[TIMING_HANDLER_MS],                      &timingMillisecond          },
    { timing_handler_names[TIMING_HANDLER_FEI],                     &timingFEI                  },
    { timing_handler_names[TIMING_HANDLER_STARTREF],                &timingStartRef             },
    { timing_handler_names[TIMING_HANDLER_TELEGRAM],                &timingTelegramReady        },
    { timing_handler_names[TIMING_HANDLER_CYCLEWARNING],            &timingCycleWarning         },
    { timing_handler_names[TIMING_HANDLER_INJECTWARNING],           &timingInjectionWarning     },
    { timing_handler_names[TIMING_HANDLER_RECOVER],                 &timingRecover              },
    { timing_handler_names[TIMING_HANDLER_COAST],                   &timingCoast                },
    { timing_handler_names[TIMING_HANDLER_ECONOMY],                 &timingEconomy              },
    { timing_handler_names[TIMING_HANDLER_TOCYCLING],               &timingToCycling            },
    { timing_handler_names[TIMING_HANDLER_TOSTANDBY],               &timingToStandby            },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONCOMMIT],       &timingTransactionCommit    },
    { timing_handler_names[TIMING_HANDLER_TRANSACTIONROLLBACK],     &timingTransactionRollback  },
    { "",                                                           NULL                        },
};

// EOF
