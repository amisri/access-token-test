/*!
 * @file   prop_class.c
 * @author Stephen Page
 *
 * Class-specific functions for getting and setting properties
 */

#include <cmd.h>
#include <defconst.h>
#include <defprops.h>
#include <equipdev.h>
#include <fgc_errs.h>
#include <prop.h>
#include <prop_class.h>
#include <classes/94/defconst_assert.h>


int32_t SetifResetOk(struct Cmd *command)
{
    struct Equipdev_channel *device;
    uint32_t                i;

    // Check whether any power converters are on

    for(i = 1 ; i < FGCD_MAX_DEVS ; i++)
    {
        device = &equipdev.device[i];

        // Skip device if not configured

        if(!device->fgcd_device->name) continue;

        // Check whether device is in a state in which the software is permitted to restart

        switch(device->status.state_pc)
        {
            // States in which power converter is considered to be off

            case FGC_PC_FLT_OFF:
            case FGC_PC_FLT_STOPPING:
            case FGC_PC_OFF:
            case FGC_PC_STOPPING:
                break;

            default: // Power converter is not off
                return 0;
        }
    }
    return 1; // All power converters are off
}



int32_t SetTerminate(struct Cmd *command, struct prop *property)
{
    return Terminate(command, property);
}

// EOF
