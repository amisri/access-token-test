/*!
 * @file   mode_pc.cpp
 * @brief  Manage Power Converter mode changes for DIGMUGEF devices
 * @author Michael Davis
 * @author Miguel Hermo
 */

#include <cmwpub.h>
#include <cmdqmgr.h>
#include <logging.h>
#include <equipdev.h>
#include <fgcd.h>
#include <state_pc.h>
#include <state_vs.h>
#include <reference.h>
#include <mode_pc.h>
#include <classes/94/defsyms.h>      // Import symbol definitions for CMW logging


enum fgc_errno modePcSet(uint32_t device_index, uint32_t mode)
{
    Equipdev_channel &device = equipdev.device[device_index];

    // Do not allow power converter to be started if a self-triggered Post Mortem acquisition is in progress
/*
    if(mode != FGC_PC_OFF && device.fgcd_device->pm_self.acq_info.wait_cycles)
    {
        return FGC_PM_IN_PROGRESS;
    }
*/
    switch(mode)
    {
        case FGC_PC_OFF:

            if(device.status.state_pc == FGC_PC_FLT_OFF)
            {
                // reset cclibs faults

                refMgrResetFaultsRT(&device.ref_mgr);

                // reconstruct unlatched_faults

                equipdevUpdateFaults(device_index);

                // reset faults

                device.status.st_faults = device.unlatched_faults;

	    }
            else if(device.mode_pc == FGC_PC_OFF && device.vs.vsrun == 1)
            {
                // user requests PC OFF again before the vsrun was set to 0 by STATE PC: we force it

                logPrintf(device_index, "Forcing vsrun=0\n");

                device.vs.vsrun = 0;
            }

	    break;

        case FGC_PC_IDLE:

            if((device.state_pc_bit_mask & (FGC_PC_FLT_OFF_BIT_MASK|FGC_PC_FLT_STOPPING_BIT_MASK|FGC_PC_STOPPING_BIT_MASK)) != 0)
            {
                return FGC_BAD_STATE;
            }

            // Allow going from ARMED to IDLE if the run time has not been set

            if(device.status.state_pc == FGC_PC_ARMED && refMgrParPointer(&device.ref_mgr, REF_RUN)->secs.abs == 0)
            {
                // Reset reference function type to NONE

                refMgrFgParValue(&device.ref_mgr,REF_FG_TYPE) = FG_NONE;

                referenceArm(device_index, 0, false, 0, NULL);
            }

            // Abort if we ask for IDLE while RUNNING

            else if(device.status.state_pc == FGC_PC_RUNNING)
            {
                refEventAbort(&device.ref_mgr);
            }

            break;

        case FGC_PC_CYCLING:
        case FGC_PC_ON_STANDBY:

            if((device.state_pc_bit_mask & (FGC_PC_FLT_OFF_BIT_MASK|FGC_PC_FLT_STOPPING_BIT_MASK|FGC_PC_STOPPING_BIT_MASK)) != 0)
            {
                return FGC_BAD_STATE;
            }
            break;

        default:

            return FGC_BAD_PARAMETER;
            break;
    }

    // change of mode.pc succeeded

    if(mode != device.mode_pc)
    {
        device.mode_pc = mode;

        refMgrParValue(&device.ref_mgr, MODE_REF) = translateModePctoRef(device_index, mode);

        cmwpubNotify(device_index, "MODE.PC", 0, 0, 0, 1, 0);

        logPrintf(device_index, "MODE_PC = %s\n", sym_names_pc[device.mode_pc].label);
    }

    return FGC_OK_NO_RSP;
}

// EOF
