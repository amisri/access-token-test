#!/bin/bash
#
# Filename: fgcd_common.sh
# Purpose:  Common variables and functions for FGCD scripts
# Author:   Stephen Page

# CPU architecture and OS of the build host

cpu=$(uname -m)
os=$(uname -s)

# Operational user

op_user=pclhc

# Host for operational deployment

op_host="cs-ccr-teepc2"

# Path to operational executables

op_path="/user/$op_user/bin/${os}/${cpu}"

# Path to operational configuration

op_config_path="/user/$op_user/etc/fgcd"

# FGC group and name files

op_group_file="$op_config_path/group"
op_name_file="$op_config_path/name"

# Print information about an FGCD executable

print_fgcd_executable_info()
{
    executable=$1

    # Get the build information for the executable

    build_ident_data=$(ident "$executable")

    build_unixtime=$(echo "$build_ident_data" | awk -F: '/\$fgcdBuild/{ split($2,a," ");print a[1] }')
    build_user=$(echo "$build_ident_data" | awk -F: '/\$fgcdUser/{ print $2 }')
    build_time=$(date -d @"$build_unixtime")

    # Print information about the executable for the group

    printf "%d  %-30s  %-10s" "$build_unixtime" "$build_time" "$build_user"
}

# EOF
