#!/bin/bash
#
# Name:    class_install_groups.sh
# Purpose: Get a list of installation groups for a class
# Author:  Stephen Page

if [ $# -ne 1 ]; then
    echo >&2 "Usage: $0 <class>"
    exit 1
fi

class=$1

# Import common variables and functions

script_path=$(dirname $0)
. "$script_path/fgcd_common.sh" || exit 1

# Get a list of gateways for the class

class_gateways=$("$script_path/class_gateways.sh" $class)

# Get a list of unique installation groups for the class's gateways from the group file

egrep "$(echo ^$class_gateways: | sed 's/ /:|^/g')" "$op_group_file" | cut -d: -f3 | cut -d/ -f1 | sort -u

# EOF
