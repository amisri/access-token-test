#!/bin/bash
#
# Name:    fgcd_class_check_installed.sh
# Purpose: Check installed executables for an FGCD class
# Author:  Stephen Page

if [ $# -ne 1 ]; then
    echo >&2 "Usage: $0 <class>"
    exit 1
fi

class=$1
exit_status=0

# Import common variables and functions

script_path=$(dirname $0)
. "$script_path/fgcd_common.sh" || exit 1

groups=$($script_path/class_install_groups.sh $class)
hosts=$($script_path/class_gateways.sh $class)

# Check the executable for each group

echo -e "Groups:\n"
for group in $groups
do
    executable=$op_path/fgcd/${class}/group/$group

    # Check whether the executable for the group exists

    if ! [ -e "$executable" ]; then
        printf "%-15s  %s\n" "$group" "$executable does not exist"
        exit_status=1
        continue
    fi

    printf "%-20s  " $group
    print_fgcd_executable_info "$executable"
    echo
done

# Check for host-specific executables

echo -e "\nHost-specific executables:\n"
for host in $hosts
do
    executable=$op_path/fgcd/${class}/host/$host

    if [ -e "$executable" ]; then
        printf "%-20s  " $host
        print_fgcd_executable_info "$executable"
        echo
    fi
done

exit $exit_status

# EOF
