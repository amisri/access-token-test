#! /usr/bin/bash

# Provide common functions to config file management scripts for each class
#
# The class scripts need to provide migrate().

# migration function

script=$(basename $0)
usage="usage: $script show|last|old|backup|restore|clean|migrate [fec]"

if [[ $# < 1 || $# > 2 ]]; then

    echo $usage
    exit 1
fi

cmd=$1

if [[ $# == 2 ]]; then

    fecs=$2
fi



show()
{
    ls -l $fgcd/ | grep device_config
}

last()
{
    ls -lR $cfg/last
}

old()
{
    ls -lR $cfg/*/old
}

backup()
{
    rm -rf $cfg.backup
    cp -ra $cfg $cfg.backup
}

restore()
{
    rm -rf $cfg.old
    mv -f $cfg $cfg.old
    cp -ra $cfg.backup $cfg
}

clean()
{
    rm -rf $cfg/last $cfg/*/old
}

# Main

count=0

for device in $fecs
do
    let count++
    fgcd=/user/pclhc/var/log/fgcd/$device

    echo
    echo ">>> " $count. $cmd $device $fgcd
    echo

    cfg=$fgcd/device_config

    case $cmd in

    show)
        show
        ;;

    last)
        last
        ;;

    old)
        old
        ;;

    backup)
        backup
        ;;

    restore)
        restore
        ;;

    clean)
        clean
        ;;

    migrate)
        migrate
        ;;

    *)
        echo $usage
        exit -1
        ;;
    esac
done

# EOF
