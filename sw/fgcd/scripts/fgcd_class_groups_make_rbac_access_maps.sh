#!/bin/bash
#
# Name:    class_groups_make_rbac_access_maps.sh
# Purpose: Generate RBAC access maps for a class's specified groups
# Author:  Stephen Page

if [ $# -lt 2 ]; then
    echo >&2 "Usage: $0 <class> [<base group 1> [<base group 2>...]]"
    exit 1
fi

class=$1
shift

groups=$@

# Import common variables and functions

script_path=$(dirname $0)
. "$script_path/fgcd_common.sh" || exit 1

hosts=$("$script_path/class_gateways.sh" "$class" $groups)

# Check whether no hosts were matched

if [ -z "$hosts" ]; then
    echo >&2 No hosts for class $class in groups: $groups
    exit 1
fi

# Build a comma-delimited list of device server names

device_servers=$(echo $(echo "$hosts" | sed 's/.*/FGC_\U&/'))

# Generate RBAC access maps

/user/rbac/tools/rbac-maps-server/bin/RBAC-MAPS-SERVER.jvm --login=explicit $device_servers

# EOF
