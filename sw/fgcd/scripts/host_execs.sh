#!/bin/bash
#
# Name:    host_execs.sh
# Purpose: Output a list of hosts and their FGCD executables
# Author:  Stephen Page

# Import common variables and functions

script_path=$(dirname $0)
. "$script_path/fgcd_common.sh" || exit 1

exit_status=0

# Read each host name and its class from the name file

egrep '^[^:]+:0:' "$op_name_file" | sort | cut -d: -f1,3 | sed 's/:/ /g' | while read host_name class
do
    # Check for host or group executable

    exec=$op_path/fgcd/${class}/host/$host_name
    if [ -e "$exec" ]; then
        # Host-specific executable

        echo $host_name ${exec##*/}
    else # Group executable
        # Read host entry from the group file

        host_entry=$(grep -m1 "^$host_name:" "$op_group_file")

        if [ $? -ne 0 ]; then
            echo >&2 Error: host $host_name is not in the FGC group file $group_file
            exit 1
        fi

        base_group=$(echo $host_entry | cut -d: -f3 | cut -d/ -f1)
        exec=$op_path/fgcd/${class}/group/$base_group

        echo -n $host_name ${exec##*/}
        if [ -e "$exec" ]; then
            echo
        else # The executable for the group does not exist
            echo "(MISSING)"
            exit_status=1
        fi
    fi
done

exit $exit_status

# EOF
