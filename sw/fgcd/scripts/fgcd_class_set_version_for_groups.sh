#!/bin/bash
#
# Name:    class_set_version_for_groups.sh
# Purpose: Set the installed version of a class that groups should run
# Author:  Stephen Page

if [ $# -eq 0 ]; then
    echo >&2 "Usage: $0 <class> [<version>] [<group 1> [<group 2> ...]] [ALL]"
    exit 1
fi

class=$1
shift

target_version=$1
shift

target_groups=$@

# Import common variables and functions

script_path=$(dirname $0)
. "$script_path/fgcd_common.sh" || exit 1

# Allow an empty result to the wildcard/glob *

shopt -s nullglob

# Check that the specified class is valid

if [[ ! "$class" =~ ^[0-9]+$ ]]; then
    echo >&2 "\"$class\" is not a valid class"
    exit 1
fi

# Prompt the user to select a version if it was not specified as an argument

if [ -z "$target_version" ]; then
    installed_executables=$(echo $op_path/fgcd/${class}/released/*)

    # Check whether no versions of the class are installed

    if [ -z "$installed_executables" ]; then
        echo >&2 "No versions of class $class are installed"
        exit 1
    fi

    # Print installed versions

    echo -e "Installed versions of class $class are:\n"
    for executable in $installed_executables
    do
        print_fgcd_executable_info $executable
        echo
    done
    echo

    # Prompt the user

    read -p "Enter an installed version from the list above or \"latest\" for the most recent version: "

    if [ -z "$REPLY" ]; then
        exit 1
    fi
    target_version="$REPLY"
fi

# Check that the entered version exists

if [ "$target_version" = "latest" ]; then
    target_versions=($(echo $op_path/fgcd/${class}/released/*))
    target_version=$(basename ${target_versions[-1]})

    if [ -z "$target_version" ]; then
        echo >&2 "No versions of class $class are installed"
        exit 1
    fi

    echo -e "\nUsing latest version of class $class:\n"
    print_fgcd_executable_info "$op_path/fgcd/${class}/released/$target_version"
    echo
elif [[ ! $target_version =~ ^[0-9]+$ ]] || [ ! -r "$op_path/fgcd/${class}/released/$target_version" ]; then
    echo >&2 "$op_path/fgcd/${class}/released/$target_version is not an executable for an installed version of class $class"
    exit 1
fi

# Get a list of groups for the class

groups=$($script_path/class_install_groups.sh $class)

# Prompt the user to select groups if they were not specified as arguments

if [ -z "$target_groups" ]; then
    # Print the class's groups and information about their currently installed executables

    echo -e "\nGroups for class $class are:\n"

    for group in $groups
    do
        group_executable=$op_path/fgcd/${class}/group/$group

        printf "%-20s  " $group
        if [ -e "$group_executable" ]; then
            print_fgcd_executable_info $group_executable
        else
            echo -n No version installed
        fi
        echo
    done
    echo

    # Prompt the user

    read -p "Enter one or more space-separated groups from the list above or \"ALL\" to install to all groups: "

    if [ -z "$REPLY" ]; then
        exit 1
    fi
    target_groups=$REPLY
fi

# Check that the entered groups exist for the class

if [ "$target_groups" = "ALL" ]; then
    target_groups=${groups//$'\n'/ }

    echo -e "\nInstalling to all groups for $class"
    echo
else
    for group in $target_groups
    do
        if ! echo -e "$groups" | grep -q ^$group$; then
            echo >&2 "$group is not a valid group for class $class"
            exit 1
        fi
    done
fi

# Set the version for the selected groups

echo -e "\nSetting version $target_version for class $class groups:\n"

ssh $op_user@$op_host "for group in $target_groups; do ln -fv \"$op_path/fgcd/${class}/released/$target_version\" \"$op_path/fgcd/${class}/group/\$group\"; done"

# EOF
