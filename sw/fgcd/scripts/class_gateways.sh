#!/bin/bash
#
# Name:    class_gateways.sh
# Purpose: Get a list of gateways for a class and optionally from only specified groups
# Author:  Stephen Page

if [ $# -lt 1 ]; then
    echo >&2 "Usage: $0 <class> [<base group 1> [<base group 2>...]]"
    exit 1
fi

class=$1
shift

groups=$@

# Import common variables and functions

script_path=$(dirname $0)
. "$script_path/fgcd_common.sh" || exit 1

# Get gateways for the class from the name file

hosts=$(egrep "^[^:]+:[0-9]+:$class:" "$op_name_file" | cut -d: -f1 | sort -u)

# Filter hosts by group if one was specified

if [ -n "$groups" ]; then
    group_data=$(cat $op_group_file)

    for host in $hosts
    do
        # Read host entry from the group file

        host_entry=$(grep -m1 "^$host:" "$op_group_file")

        if [ $? -ne 0 ]; then
            echo >&2 Error: host $host is not in the FGC group file $group_file
            exit 1
        fi

        # Check whether the base group for the host is in the specified list of groups

        base_group=$(echo $host_entry | cut -d: -f3 | cut -d/ -f1)

        for group in $groups
        do
            if [ "$base_group" = "$group" ]; then
                echo "$host"
                break
            fi
        done
    done
else # No group was specified
    # Print all of the class's hosts

    echo "$hosts"
fi

# EOF
