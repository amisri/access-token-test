#!/bin/sh
#
# Name:     fgcd.sh
# Purpose:  FGCD start-up script
# NOTE:     This is edited in the fgc repo at fgc/sw/fgcd/scripts/ - DO NOT EDIT in pclhc/bin/

op_user=pclhc
config_path="/user/$op_user/etc/fgcd"
group_file="$config_path/group"
options_path="$config_path/options"
name_file="$config_path/name"
startup_path="$config_path/startup"

max_cores=3 # The maximum number of core files that may be created

# Wait for FGCD process to terminate

function wait_for_fgcd
{
    # Record start unixtime

    start_time=$(date +%s)

    # Wait for FGCD process to terminate

    wait

    # Get unixtime at the moment FGCD stopped

    end_time=$(date +%s)

    # Extract the last lines of the log to $end_time.log

    if [ -r log ]; then
        tail -100 log >$end_time.log
    fi

    # Check whether there is a core file and give it a standard name

    core_name=$end_time.core

    if [ -f core.$fgcd_pid ]; then
        mv core.$fgcd_pid   "$core_name"
    elif [ -f core ]; then
        mv core             "$core_name"
    fi

    if [ -f "$core_name" ]; then
        # In case of a core dump, save a copy of stdout and stderr

        if [ -r stdout ]; then
            cp stdout ${end_time}.stdout
        fi
        if [ -r stderr ]; then
            cp stderr ${end_time}.stderr
        fi

        # Allow the core file to be read by members of the dscdev group

        chgrp dscdev "$core_name" && chmod g+r "$core_name"

        # Compress the core file

        gzip "$core_name"

        # Check whether there are already more than the maximum number of core files

        num_cores=$(/bin/ls *.core* | wc -l)

        if [ $num_cores -gt $max_cores ]; then
            # Remove data relating to the oldest core file

            oldest_core_time=$(/bin/ls -td *.core* | tail -1 | cut -d. -f1)
            rm -f ${oldest_core_time}.*
        fi
    fi

    # Check whether FGCD died before the minimum lifetime

    let lifetime=$end_time-$start_time
    if [ $lifetime -lt 60 ]; then
        echo Process died after $lifetime seconds which is less than the minimum lifetime >>stderr
        exit 1
    fi
}

# End of functions

# Read host entry from the name file

host_name=$(hostname -s | sed 's/\..*//')
host_entry=$(grep -m1 "^$host_name:0:" "$name_file")

if [ $? -ne 0 ]; then
    echo ERROR: host $host_name is not in the FGC name file $name_file >>stderr
    exit 1
fi

class=$(echo $host_entry | cut -d: -f3)

# Read host entry from the group file

host_entry=$(grep -m1 "^$host_name:" "$group_file")

if [ $? -ne 0 ]; then
    echo ERROR: host $host_name is not in the FGC group file $group_file >>stderr
    exit 1
fi

timing_domain=$(echo $host_entry | cut -d: -f2)
group=$(echo $host_entry | cut -d: -f3)
base_group=$(echo $group | cut -d/ -f1)

# Set path to the executable

cpu=$(uname -m)
os=$(uname -s)

op_path="/user/$op_user/bin/$os/$cpu"

# Use a host-specific executable, if one exists,
# otherwise use the one for the base group

host_exec="$op_path/fgcd/${class}/host/$host_name"

if [ -e "$host_exec" ]; then
    exec=$host_exec
else # No host-specific executable exists so use the base group
    exec="$op_path/fgcd/${class}/group/$base_group"
fi

# Change directory to the log directory

log_dir="/user/$op_user/var/log/fgcd/$host_name"
if ! cd "$log_dir"; then
    echo ERROR: Failed to change directory to $log_dir >>stderr
    exit 1
fi

# Remove standard out and standard error files
# Also clean up startup_global - it is no longer needed - this can be removed once all startup_global files have gone

rm -f stdout stderr startup_global

# Run the group start-up script

if [ -x "$startup_path/group/$base_group" ]; then
    if ! "$startup_path/group/$base_group" >startup_group 2>&1; then
        exit 1
    fi
fi

# Run the class start-up script

if [ -x "$startup_path/class/$class" ]; then
    if ! "$startup_path/class/$class" >startup_class 2>&1; then
        exit 1
    fi
fi

# Run the host start-up script

if [ -r "$startup_path/host/$host_name" ]; then
    if ! source "$startup_path/host/$host_name"  >startup_host 2>&1; then
        exit 1
    fi
fi

# Configure timing

if [ "$timing_domain" = "SYSTEM" ]; then
    fgcd_timing_options="-s"
else # Hardware timing
    if ! "/user/$op_user/bin/fgcd_timing.sh" $class $timing_domain >"fgcd_timing" 2>&1; then
        exit 1
    fi

    fgcd_timing_options="-m$timing_domain"
fi

# Get options
# A host-specific file is used if it exists,
# otherwise a base-group-specific file if that exists

options=
if [ -r "$options_path/host/$host_name" ]; then
    options=$(echo -n $(cat "$options_path/host/$host_name"))
elif [ -r "$options_path/group/$base_group" ]; then
    options=$(echo -n $(cat "$options_path/group/$base_group"))
fi

# Set maximum core file size

ulimit -c unlimited

# Run ethtool to get the eth0 link speed and save it to a file

ethtool eth0 2>/dev/null > eth0_info

# Run FGCD

while :
do
    # Check that $exec is executable

    if [ ! -x "$exec" ]; then
        echo ERROR: $exec is not executable >>stderr
        exit 1
    fi

    # Run executable

    printf "Starting FGCD class %s group %s from %s at %s\n\n" $class $base_group $exec "$(date)" >>stderr

    "$exec" $options $fgcd_timing_options -g$base_group -llog $@ >>stdout 2>>stderr &
    fgcd_pid=$!

    # Terminate FGCD on exit

    trap "kill $fgcd_pid; wait $fgcd_pid" EXIT

    # Wait for FGCD

    wait_for_fgcd
done

# EOF
