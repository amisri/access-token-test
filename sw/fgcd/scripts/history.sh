#!/bin/sh
#
# Filename: history.sh
#
# Purpose:  Add information to history files when a new version is installed
#
# Author:   Stephen Page

if [ $# -ne 3 ]; then
    echo "Usage: $0 <version file> <platform history file> <class history file>"
    exit 1
fi

version_file=$1
platform_file=$2
class_file=$3

class=$(basename $PWD)
version_unixtime=$(cat "$version_file")
tag=${class}/$version_unixtime

# Check that the tag does not already exist in the repository

if [ -n "$(git tag -l $tag)" ]; then
    echo >&2 Tag $tag already exists in the repository
    exit 1
fi

# Exit if an error occurs

set -e

# Read existing histories

class_history=$(cat "$class_file")
platform_history=$(cat "$platform_file")

# Restore the history if killed

trap "echo -e \"$class_history\" >\"$class_file\"; echo -e \"$platform_history\" >\"$platform_file\"; exit" 1 2 15

# Add a prefix of newly installed versions of classes to the history

echo "********************************************************************************" >"$class_file"
date +"%a %b %d %H:%M:%S %Y" >>"$class_file"
echo >>"$class_file"

# Construct the class line

cpu=$(uname -m)
os=$(uname -s)

class_line=$(awk "BEGIN  { printf(\"CLASS $class: $os/$cpu %s ($version_unixtime)\", strftime(\"%a %b %d %H:%M:%S %Y\", $version_unixtime)) }")
echo -e "$class_line\n" >>"$class_file"

# Write new platform entries to the class history

new_platform_entries=$(grep -m1 -B9999 "^CLASS $class:" "$platform_file" | grep -v "^CLASS" | grep -A1 "^-" | grep -v "\-\-" | sed 's/^-/=/')
if [ -n "$new_platform_entries" ]; then
    echo -e "$new_platform_entries\n" >>"$class_file"
fi

# Append the original class history

if [ -n "$class_history" ]; then
    echo -e "$class_history" >>"$class_file"
fi

# Write the class line to the platform file

echo -e "$class_line\n" >"$platform_file"

# Append the original platform history

echo -e "$platform_history" >>"$platform_file"

# Commit and tag the new release

git add "$class_file" "$platform_file"
git commit -m "Release: $class_line"
git tag "$tag"

# Update the remote repository with commits and tags created during the install process

git push origin
git push origin --tags

# EOF
