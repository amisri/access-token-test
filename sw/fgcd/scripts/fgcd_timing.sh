#!/bin/sh
#
# Name:     fgcd_timing.sh
# Purpose:  Load CTR timing configurations for FGCD
# Author:   Stephen Page

# Arguments

if [ -r "/dsc/local/data/LTIM_DU.$(hostname -s).instance" ]; then
    echo "LTIM configuration at /dsc/local/data/LTIM_DU.$(hostname -s).instance. Skipping custom timing configuration..."
    exit 0;
fi

if [ $# -ne 2 ]; then
    echo "Usage $0: <class> <machine>"
    exit 1
fi

class=$1
machine=$2

# Path to timload

timload="/usr/local/bin/timload"

# Base path to CTR configuration files

ctr="/user/pclhc/etc/fgcd/timing/ctr"

# Load class-specific configurations

if [ -n "$class" ]; then
    # Load class global configuration

    if [ -r "$ctr/class/$class/global" ]; then
        "$timload" -rf "$ctr/class/$class/global"
    fi
fi

# Load host-specific configuration

host_name=$(hostname -s | sed 's/\..*//')
if [ -r "$ctr/host/$host_name" ]; then
    "$timload" -rf "$ctr/host/$host_name"
fi

# EOF
