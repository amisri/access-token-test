#!/bin/bash

# Change directory to a script location
cd "$(dirname "$0")"

./../utilities/cpp/projectControl/projectControl.py --config . "$@"

# EOF