# FGCD 2 Framework: Main file of the building system.

# **********************************************************
# Set some basics variables
    set(FGCD_ROOT          "${CMAKE_CURRENT_LIST_DIR}/..")
    set(FGCD_MODULES_ROOT  "${FGCD_ROOT}/source/modules")
    set(FGCD_PROFILES_ROOT "${FGCD_ROOT}/cmake/profiles")
    set(FGCD_LIBS_ROOT     "${FGCD_ROOT}/cmake/libs")
    if(NOT CMAKE_BUILD_TYPE OR CMAKE_BUILD_TYPE STREQUAL "")
      set(CMAKE_BUILD_TYPE Release)
    endif()

# **********************************************************
# Create an executable
    add_executable(FGCD)

# **********************************************************
# Include all *.cmake files within 'commands' subdirectory
    file(GLOB FGCD_COMMANDS_FILES "${FGCD_ROOT}/cmake/commands/*.cmake")

    foreach(commandFile ${FGCD_COMMANDS_FILES})
        include("${commandFile}")
    endforeach(commandFile)

# **********************************************************
# Simplify paths
    fgcdSimplifyPaths()

# **********************************************************
# Set common paths to default values (or user-provided values if environment variables with the same name exist)
    fgcdSetToEnvOrDefault(FGCD_GCC_X86_64     "")
    fgcdSetToEnvOrDefault(FGCD_GCC_AARCH64    "/user/poccdev/fgcd2/bin/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu")
    fgcdSetToEnvOrDefault(FGCD_GCC_AARCH64_BM "/user/poccdev/fgcd2/bin/gcc-arm-10.3-2021.07-x86_64-aarch64-none-elf")

# EOF
