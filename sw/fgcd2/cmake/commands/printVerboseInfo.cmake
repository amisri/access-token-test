# FGCD 2 Framework: Macro to print verbose informations

macro(printVerboseInfo)
    # Retrieve some target's information
    get_target_property(verboseOutputName FGCD OUTPUT_NAME)
    get_target_property(verboseIncludes   FGCD INCLUDE_DIRECTORIES)
    get_target_property(verboseSource     FGCD SOURCES)
    get_target_property(verboseLibs       FGCD LINK_LIBRARIES)

    # Print verbose header message
    message(VERBOSE "FGCD framework compilation summary:")

    # Generic information
    message(VERBOSE "   Target       = ${verboseOutputName}")

    # Profile information
    message(VERBOSE "   gcc path     = ${FGCD_PROFILE_C_PATH}")
    message(VERBOSE "   g++ path     = ${FGCD_PROFILE_CXX_PATH}")
    message(VERBOSE "   objdump path = ${FGCD_PROFILE_OBJDUMP_PATH}")
    message(VERBOSE "   objcopy path = ${FGCD_PROFILE_OBJCOPY_PATH}")
    message(VERBOSE "   size path    = ${FGCD_PROFILE_SIZE_PATH}")
    message(VERBOSE "   C flags      = ${FGCD_PROFILE_FLAGS} ${FGCD_PROFILE_C_FLAGS}")
    message(VERBOSE "   C++ flags    = ${FGCD_PROFILE_FLAGS} ${FGCD_PROFILE_CXX_FLAGS}")

    # Modules information
    message(VERBOSE "Loaded modules:")
    foreach(module IN LISTS FGCD_LOADED_MODULES)
        message(VERBOSE "   ${module}")
    endforeach()

    # Library information
    message(VERBOSE "Loaded libraries:")
    foreach(lib IN LISTS FGCD_LOADED_LIBS)
        message(VERBOSE "   ${lib}")
    endforeach()
    foreach(lib IN LISTS verboseLibs)
        message(VERBOSE "   ${lib}")
    endforeach()

    # Source information
    message(VERBOSE "Include directories:")
    message(VERBOSE "   ${verboseIncludes}")

    message(VERBOSE "Source:")
    message(VERBOSE "   ${verboseSource}")

endmacro()

# EOF