# FGCD 2 Framework: Macro to declare dependency on a module / path
# **********************************************************

macro(fgcdDependOnModules)
    fgcdDependOnModulesBase(${ARGN})
endmacro()

# EOF