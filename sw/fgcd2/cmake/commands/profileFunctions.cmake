# FGCD 2 Framework: Macros to set compilation options.

macro(fgcdProfileFunctionsGuard name)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use ${name}() before fgcdDeclareTarget(), not after.")
endmacro()

# **********************************************************

macro(fgcdProfileSetCPath value)
    fgcdProfileFunctionsGuard("fgcdProfileSetCPath")
    set(FGCD_PROFILE_C_PATH "${value}")
endmacro()

# **********************************************************

macro(fgcdProfileSetCxxPath value)
    fgcdProfileFunctionsGuard("fgcdProfileSetCxxPath")
    set(FGCD_PROFILE_CXX_PATH "${value}")
endmacro()

# **********************************************************

macro(fgcdProfileSetObjdumpPath value)
    fgcdProfileFunctionsGuard("fgcdProfileSetObjdumpPath")
    set(FGCD_PROFILE_OBJDUMP_PATH "${value}")
endmacro()

# **********************************************************

macro(fgcdProfileSetObjcopyPath value)
    fgcdProfileFunctionsGuard("fgcdProfileSetObjcopyPath")
    set(FGCD_PROFILE_OBJCOPY_PATH "${value}")
endmacro()

# **********************************************************

macro(fgcdProfileSetSizePath value)
    fgcdProfileFunctionsGuard("fgcdProfileSetSizePath")
    set(FGCD_PROFILE_SIZE_PATH "${value}")
endmacro()

# **********************************************************

macro(fgcdProfileAddFlag value)
    fgcdProfileFunctionsGuard("fgcdProfileAddFlag")
    set(FGCD_PROFILE_FLAGS "${FGCD_PROFILE_FLAGS} ${value}")
endmacro()

# **********************************************************

macro(fgcdProfileAddCFlag value)
    fgcdProfileFunctionsGuard("fgcdProfileAddCFlag")
    set(FGCD_PROFILE_C_FLAGS "${FGCD_PROFILE_C_FLAGS} ${value}")
endmacro()

# **********************************************************

macro(fgcdProfileAddCxxFlag value)
    fgcdProfileFunctionsGuard("fgcdProfileAddCxxFlag")
    set(FGCD_PROFILE_CXX_FLAGS "${FGCD_PROFILE_CXX_FLAGS} ${value}")
endmacro()

# **********************************************************

macro(fgcdProfileAddLinkFlag value)
    fgcdProfileFunctionsGuard("fgcdProfileAddLinkFlag")
    set(FGCD_PROFILE_LINK_FLAGS "${FGCD_PROFILE_LINK_FLAGS} ${value}")
endmacro()

# EOF