# FGCD 2 Framework: Macro to declare target (executable).

macro(fgcdDeclareTarget targetName)
    fgcdRequireVar(FGCD_ROOT           "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireVar(FGCD_PROFILE_LOADED "You need to invoke fgcdLoadProfile() before fgcdDeclareTarget().")

    # Add the files in target directory to sources
    file(GLOB_RECURSE sourcesC   CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/*.c")
    file(GLOB_RECURSE sourcesCxx CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

    # Add sources to the executable
    target_sources(FGCD PRIVATE ${FGCD_SOURCES} ${sourcesC} ${sourcesCxx})

    # Set the executable name
    set_target_properties(FGCD PROPERTIES OUTPUT_NAME "${targetName}")

    # Apply information from the loaded profile

    # Set compilers paths
    set(CMAKE_C_COMPILER    ${FGCD_PROFILE_C_PATH})
    set(CMAKE_ASM_COMPILER  ${FGCD_PROFILE_C_PATH})
    set(CMAKE_CXX_COMPILER  ${FGCD_PROFILE_CXX_PATH})

    # Set additional flags
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   ${FGCD_PROFILE_FLAGS} ${FGCD_PROFILE_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FGCD_PROFILE_FLAGS} ${FGCD_PROFILE_CXX_FLAGS}")
    set(CMAKE_CXX_LINK_FLAGS "${CMAKE_LINKER_FLAGS} ${FGCD_PROFILE_LINK_FLAGS}")

    # Add source folder to the include directories
    include_directories(${FGCD_ROOT}/source)

    # Print verbose information
    printVerboseInfo()

    # Add information about generated executable
    get_target_property(exeName FGCD OUTPUT_NAME)
    add_custom_command(TARGET FGCD POST_BUILD
                       COMMAND echo "${CMAKE_CURRENT_BINARY_DIR}/${exeName}" > ${CMAKE_CURRENT_BINARY_DIR}/exe.txt
    )


    # Mark the finish of the declareTarget()
    set(FGCD_TARGET_DECLARED TRUE)
endmacro()

# EOF
