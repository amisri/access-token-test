# FGCD 2 Framework: Miscellaneous macros

# **********************************************************
# Checks if the given variable is defined

macro(fgcdRequireVar var hint)
    if (NOT DEFINED ${var})
        message(FATAL_ERROR "FGCD error: The variable '${var}' is not defined. ${hint}")
    endif()
endmacro()


# **********************************************************
# Checks if the given variable is undefined

macro(fgcdRequireNoVar var hint)
    if (DEFINED ${var})
        message(FATAL_ERROR "FGCD error: The variable '${var}' is defined. ${hint}")
    endif()
endmacro()


# **********************************************************
# If env variable 'envVar' is defined, then it sets 'var' to the value of 'envVar' or to 'defaultValue' otherwise.

macro(fgcdSetToEnvOrDefault var defaultValue)
    if (DEFINED ENV{${var}})
        set(${var} $ENV{${var}})
    elseif (NOT DEFINED ${var})
        set(${var} ${defaultValue})
    endif()
endmacro()


# **********************************************************
# resultVar - name of the variable to set
# name      - name of the thing we try to load (profile, library, etc.)
# value     - file name (or full path)
# path      - path to the file that we should check first

macro(fgcdUsePathsIfExist resultVar name value path)
    if (EXISTS "${path}")
        set(${resultVar} "${path}")
    elseif (EXISTS "${value}")
        set(${resultVar} "${value}")
    else()
        message(FATAL_ERROR "FGCD error: The ${name} '${value}' is not a valid ${name} name or path.")
    endif()
endmacro()


# **********************************************************
# Resolves absolute path (removes symlink and dots . ..)

macro(fgcdSimplifyPath pathVar)
    get_filename_component(${pathVar} "${${pathVar}}" ABSOLUTE)
endmacro()


# **********************************************************
# Resolves absolute path for common paths

macro(fgcdSimplifyPaths)
    fgcdSimplifyPath(FGCD_ROOT)
    fgcdSimplifyPath(FGCD_MODULES_ROOT)
    fgcdSimplifyPath(FGCD_PROFILES_ROOT)
    fgcdSimplifyPath(FGCD_LIBS_ROOT)
endmacro()



# EOF