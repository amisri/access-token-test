# FGCD 2 Framework: Macro to declare dependency on a module's tests
# **********************************************************

macro(fgcdDependOnModuesTests)
    fgcdDependOnModulesBase(${ARGN} INCLUDE_TEST_DIR)
endmacro()

# EOF