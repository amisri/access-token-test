# FGCD 2 Framework: Macro to add a file to the list of sources.

macro(fgcdAddSource path)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdAddSource() before fgcdDeclareTarget(), not after.")

    list(APPEND FGCD_SOURCES ${path})
endmacro()

# EOF