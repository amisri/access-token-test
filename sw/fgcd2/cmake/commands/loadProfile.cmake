# FGCD 2 Framework: Macro to load compilation profile.

macro(fgcdLoadProfile profileName)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdLoadProfile() before fgcdDeclareTarget(), not after.")

    # First we will try to load profile from profiles directory.
    # Otherwise, we treat profileName as a full path to the profile file.
    fgcdUsePathsIfExist(profilePath "profile" "${profileName}" "${FGCD_PROFILES_ROOT}/${profileName}.cmake")

    # Include profile
    include("${profilePath}")

    # Validate that every required profile information is provided
    fgcdRequireVar(FGCD_PROFILE_C_PATH       "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_CXX_PATH     "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_OBJDUMP_PATH "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_OBJCOPY_PATH "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_SIZE_PATH    "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_FLAGS        "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_C_FLAGS      "Check if your profile file is complete.")
    fgcdRequireVar(FGCD_PROFILE_CXX_FLAGS    "Check if your profile file is complete.")

    set(FGCD_PROFILE_LOADED TRUE)
endmacro()

# EOF