# FGCD 2 Framework: Macro to declare dependency on a module / path

macro(fgcdDependOnModuleBase moduleName includeTestDir)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdDependOnModule() before fgcdDeclareTarget(), not after.")

    # First we will try to load module from modules directory.
    # Otherwise, treat moduleName as a full path to the module directory.
    fgcdUsePathsIfExist(modulePath "module" "${moduleName}" "${FGCD_MODULES_ROOT}/${moduleName}")

    # Get all the modules.cmake files in the path
    file(GLOB_RECURSE moduleMainFiles CONFIGURE_DEPENDS "${modulePath}/module.cmake")

    # Check if the path contains any modules
    if(NOT moduleMainFiles)
        message(FATAL_ERROR "FGCD error: The path '${moduleName}' does not contain any modules (no 'module.cmake' files found).")
    endif()

    # Iterate over the list of found modules
    foreach (moduleMainFile IN LISTS moduleMainFiles)
        # Simplify path
        fgcdSimplifyPath(moduleMainFile)

        # Make sure that the module has not been added before
        if (NOT moduleMainFile IN_LIST FGCD_LOADED_MODULES)
            # Add the module to the list of loaded modules (this has to be executed before include below)
            list(APPEND FGCD_LOADED_MODULES "${moduleMainFile}")

            # Get module directory path
            get_filename_component(moduleDir "${moduleMainFile}" DIRECTORY)

            # Add source files inside this module
            fgcdAddSourceDir("${moduleDir}/src")
            
            if(${includeTestDir})
                fgcdAddSourceDir("${moduleDir}/tests")
            endif(${includeTestDir})

            # Include the module.cmake file
            include("${moduleMainFile}")
        endif()
    endforeach()

endmacro()

# **********************************************************

macro(fgcdDependOnModulesBase)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdDependOnModules() before fgcdDeclareTarget(), not after.")

    set(options INCLUDE_TEST_DIR)
    cmake_parse_arguments(
        DEPEND_ON_MODULE_BASE
        "${options}"
        ""
        "" ${ARGN})

    foreach(module IN ITEMS ${DEPEND_ON_MODULE_BASE_UNPARSED_ARGUMENTS})
        fgcdDependOnModuleBase("${module}" ${DEPEND_ON_MODULE_BASE_INCLUDE_TEST_DIR})
    endforeach()
endmacro()

# EOF