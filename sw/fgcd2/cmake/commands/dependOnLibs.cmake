# FGCD 2 Framework: Macro to declare dependency on a module / path

set(LIBRARIES_HOME "/user/poccdev/fgcd2/libs/" CACHE PATH "Path to libraries")

macro(fgcdDependOnLib libName)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdDependOnLib() before fgcdDeclareTarget(), not after.")

    # First we will try to load library from libs directory.
    # Otherwise, treat libName as a full path to the file with library definition.
    fgcdUsePathsIfExist(libPath "library" "${libName}" "${FGCD_LIBS_ROOT}/${libName}.cmake")

    # Simplify path
    fgcdSimplifyPath(libPath)

    # Make sure that the library has not been added before
    if (NOT libPath IN_LIST FGCD_LOADED_LIBS)
        # Add the module to the list of loaded modules (this has to be executed before include below)
        list(APPEND FGCD_LOADED_LIBS "${libPath}")

        # Include the library definition
        include("${libPath}")
    endif()

endmacro()

# **********************************************************

macro(fgcdDependOnLibs)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdDependOnLibs() before fgcdDeclareTarget(), not after.")

    foreach(lib IN ITEMS ${ARGN})
        fgcdDependOnLib("${lib}")
    endforeach()
endmacro()

# EOF