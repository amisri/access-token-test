# FGCD 2 Framework: Macro to add C and C++ source files in a given directory.

macro(fgcdAddSourceDir path)
      fgcdRequireVar(FGCD_ROOT            "Include fgcd.cmake as the first thing in the CMakeList.txt.")
    fgcdRequireNoVar(FGCD_TARGET_DECLARED "Use fgcdAddSourceDir() before fgcdDeclareTarget(), not after.")

    file(GLOB_RECURSE foundSourcesC   CONFIGURE_DEPENDS "${path}/*.c")
    file(GLOB_RECURSE foundSourcesCpp CONFIGURE_DEPENDS "${path}/*.cpp")
      file(GLOB_RECURSE foundSourcesHpp CONFIGURE_DEPENDS "${path}/../inc/*.h")

    list(APPEND FGCD_SOURCES ${foundSourcesC})
    list(APPEND FGCD_SOURCES ${foundSourcesCpp})
      list(APPEND FGCD_SOURCES ${foundSourcesHpp})


endmacro()

# EOF