# FGCD 2 Framework: Compilation profile for devices running on the aarch64 architecture.

# Set path to the GCC C compiler
fgcdProfileSetCPath("${FGCD_GCC_AARCH64}/bin/aarch64-none-linux-gnu-gcc")

# Set path to the GCC C++ compiler
fgcdProfileSetCxxPath("${FGCD_GCC_AARCH64}/bin/aarch64-none-linux-gnu-g++")

# Set path to the objdump
fgcdProfileSetObjdumpPath("${FGCD_GCC_AARCH64}/bin/aarch64-none-linux-gnu-objdump")

# Set path to the objcopy
fgcdProfileSetObjcopyPath("${FGCD_GCC_AARCH64}/bin/aarch64-none-linux-gnu-objcopy")

# Set path to the size
fgcdProfileSetSizePath("${FGCD_GCC_AARCH64}/bin/aarch64-none-linux-gnu-size")

# Set additional C/C++ flags
fgcdProfileAddFlag("-D_GNU_SOURCE")
fgcdProfileAddFlag("-O2")
fgcdProfileAddFlag("-g")
fgcdProfileAddFlag("-Wall")
fgcdProfileAddFlag("-fmessage-length=0")
fgcdProfileAddFlag("-fsigned-char")
fgcdProfileAddFlag("-ffunction-sections")
fgcdProfileAddFlag("-fdata-sections")
fgcdProfileAddFlag("-ftree-vectorize")

# Set additional C flags
fgcdProfileAddCFlag("-std=c99")

# Set additional C++ flags
fgcdProfileAddCxxFlag("-std=c++17")

# Link libc, libgcc and libstdc++ statically, since our toolchain doesn't match the FGC4 Linux runtime
fgcdProfileAddCxxFlag("-static")
fgcdProfileAddCxxFlag("-static-libgcc")
fgcdProfileAddCxxFlag("-static-libstdc++")

fgcdProfileAddLinkFlag("-static")
fgcdProfileAddLinkFlag("-static-libgcc")
fgcdProfileAddLinkFlag("-static-libstdc++")

# Necessary when linking statically, see https://stackoverflow.com/a/45271521
fgcdProfileAddLinkFlag("-Wl,--whole-archive -lpthread -Wl,--no-whole-archive")

# Add base modules implicitly
fgcdDependOnModules(base)

# Necessary for cross-compiling, see https://stackoverflow.com/q/53633705
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")

# EOF