# FGCD 2 Framework: Compilation profile for tests.

# Set path to the GCC C compiler
fgcdProfileSetCPath("${FGCD_GCC_X86_64}/bin/gcc")

# Set path to the GCC C++ compiler
fgcdProfileSetCxxPath("${FGCD_GCC_X86_64}/bin/g++")

# Set path to the objdump
fgcdProfileSetObjdumpPath("objdump")

# Set path to the objcopy
fgcdProfileSetObjcopyPath("objcopy")

# Set additional C/C++ flags
fgcdProfileAddFlag("-D_GNU_SOURCE")
fgcdProfileAddFlag("-O0")
fgcdProfileAddFlag("-g")
fgcdProfileAddFlag("-fno-strict-aliasing")
fgcdProfileAddFlag("-Wall")
fgcdProfileAddFlag("-Wno-narrowing")

# Set additional C flags
fgcdProfileAddCFlag("-std=c99")

# Set additional C++ flags
fgcdProfileAddCxxFlag("-std=c++20")
# EOF