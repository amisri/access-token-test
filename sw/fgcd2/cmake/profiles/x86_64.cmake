# FGCD 2 Framework: Compilation profile for FECs running on the x86_64 architecture.

# Set path to the GCC C compiler
fgcdProfileSetCPath("gcc")

# Set path to the GCC C++ compiler
fgcdProfileSetCxxPath("g++")

# Set path to the objdump
fgcdProfileSetObjdumpPath("objdump")

# Set path to the objcopy
fgcdProfileSetObjcopyPath("objcopy")

# Set path to the size
fgcdProfileSetSizePath("size")

# Set additional C/C++ flags
fgcdProfileAddFlag("-D_GNU_SOURCE")
fgcdProfileAddFlag("-O0")
fgcdProfileAddFlag("-g")
fgcdProfileAddFlag("-Wall")
fgcdProfileAddFlag("-Wno-narrowing")
fgcdProfileAddFlag("-static-libstdc++")
fgcdProfileAddFlag("-DMASTERFIP")

# Set additional C flags
fgcdProfileAddCFlag("-std=c99")

# Set additional C++ flags
fgcdProfileAddCxxFlag("-std=c++20")
# EOF