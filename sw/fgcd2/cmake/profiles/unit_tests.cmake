# FGCD 2 Framework: Compilation profile for unit tests.

# Set path to the GCC C compiler
fgcdProfileSetCPath("gcc")

# Set path to the GCC C++ compiler
fgcdProfileSetCxxPath("g++")

# Set path to the objdump
fgcdProfileSetObjdumpPath("objdump")

# Set path to the objcopy
fgcdProfileSetObjcopyPath("objcopy")

# Set path to the size
fgcdProfileSetSizePath("size")

# Set additional C/C++ flags
# fgcdProfileAddFlag("-Og")
# fgcdProfileAddFlag("-ggdb3")
# fgcdProfileAddFlag("-fsanitize=address,undefined")

fgcdProfileAddFlag("-Wfatal-errors")

# Set additional C flags
fgcdProfileAddCFlag("-std=c99")

# Set additional C++ flags
fgcdProfileAddCxxFlag("-std=c++20")

# EOF
