# FGCD 2 Framework: Compilation profile for CERN FECs.

# Inherit from a generic FEC profile
fgcdLoadProfile(x86_64)

# The below options come from /acc/local/Linux/x86_64-linux-gcc/gcc71/Make.variables
# TODO Maybe we can add CMake.variables to the path above to support CMakes?

set(fec_toolchain_libs_path "/acc/sys/Linux/toolchain_libs")
set(linux_toolchain_libs_path "/acc/local/Linux/x86_64-linux-gcc/toolchain_libs")
set(linker_rpath "-rpath=${fec_toolchain_libs_path}:${linux_toolchain_libs_path}")
set(gcc_rpath "-Wl,${linker_rpath}")

# Extend additional C flags
fgcdProfileAddCFlag("-Wno-format-truncation")

# Extend additional C++ flags
fgcdProfileAddCxxFlag("-fpermissive ")
fgcdProfileAddCxxFlag("-Wno-deprecated-declarations")
fgcdProfileAddCxxFlag("-Wno-format-truncation")
fgcdProfileAddCxxFlag("-D_GLIBCXX_USE_CXX11_ABI=0")
fgcdProfileAddCxxFlag("${gcc_rpath}")

# EOF