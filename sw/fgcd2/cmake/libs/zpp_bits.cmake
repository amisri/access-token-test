# FGCD 2 Framework: Library definition for the zpp_bits library.
# https://github.com/eyalz800/zpp_bits
#
# Include by #include <zpp_bits.h>

include_directories("${LIBRARIES_HOME}/zpp_bits-4.4.16")

# EOF
