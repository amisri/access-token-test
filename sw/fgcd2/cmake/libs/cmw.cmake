set(CMW_FWK_VERSION 3.3.0)
set(CMW_FWK_HOME /acc/local/L867/cmw/cmw-fwk/${CMW_FWK_VERSION})

# Create a virtual target to attach all include/link dependencies onto

add_library(CMW::RDA INTERFACE IMPORTED)

target_include_directories(CMW::RDA
  INTERFACE
    ${CMW_FWK_HOME}/include
  )

target_link_libraries(CMW::RDA
  INTERFACE
    ${CMW_FWK_HOME}/lib/libcmw-fwk.a
    ${CMW_FWK_HOME}/lib/libcmw-rda3.a
    ${CMW_FWK_HOME}/lib/libcmw-rbac.a
    ${CMW_FWK_HOME}/lib/libcmw-directory-client.a
    ${CMW_FWK_HOME}/lib/libcmw-data.a
    ${CMW_FWK_HOME}/lib/libcmw-log.a
    ${CMW_FWK_HOME}/lib/libcmw-util.a
    ${CMW_FWK_HOME}/lib/libzmq.a

    ${CMW_FWK_HOME}/lib/libboost_1_69_0_filesystem.a
    ${CMW_FWK_HOME}/lib/libboost_1_69_0_thread.a
    ${CMW_FWK_HOME}/lib/libboost_1_69_0_chrono.a
    ${CMW_FWK_HOME}/lib/libboost_1_69_0_atomic.a
    curl
    gssapi_krb5
    ssl
    crypto
    rt
  )

set(CMW_FOUND TRUE)

# EOF