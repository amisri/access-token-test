
# FGCD 2 Framework: Library definition for the websockets++ library.
# https://github.com/zaphoyd/websocketpp
#
# Include by #include <websockets/...>

include_directories("${LIBRARIES_HOME}/websocketpp-0.8.2-cpp20")

# EOF
