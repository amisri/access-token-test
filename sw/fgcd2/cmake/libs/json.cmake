# FGCD 2 Framework: Library definition for the JSON library.
# https://github.com/nlohmann/json/
#
# Include by #include <json/json.hpp>

include_directories("${LIBRARIES_HOME}/json-3.11.2")

# EOF