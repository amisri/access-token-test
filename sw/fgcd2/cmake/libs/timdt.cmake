# FGCD 2 Framework: Library definition for the Timdt library.
# https://wikis.cern.ch/pages/viewpage.action?pageId=112366428
#
# Include <timdt-lib-cpp/Timing.h>

set(TIMDT_PATH "/acc/local/L867/timdt/timdt-fwk/PRO")

include_directories("${TIMDT_PATH}/include")
find_library(TimdtLibCpp NAMES libtimdt-lib-cpp.a PATHS "${TIMDT_PATH}/lib")
find_library(TimdtLib NAMES libtimdt-lib.a PATHS "${TIMDT_PATH}/lib")
find_library(TimdtLLLib NAMES libtimdt-LLlib.a PATHS "${TIMDT_PATH}/lib")
find_library(TimdtCore NAMES libtimdt-core.a PATHS "${TIMDT_PATH}/lib")
find_library(Ctr NAMES libctr.a PATHS "${TIMDT_PATH}/lib")
find_library(TimdtMetrics NAMES libtimdt-metrics.a PATHS "${TIMDT_PATH}/lib")

target_link_libraries(FGCD ${TimdtLibCpp} ${TimdtLib} ${TimdtLLLib} ${TimdtCore} ${Ctr} ${TimdtMetrics})

# EOF
