# FGCD 2 Framework: Library definition for the C++ Utils library.

include_directories("/user/poccdev/fgcd2/libs/masterfip-1.1.4/include")
target_link_libraries(FGCD "/user/poccdev/fgcd2/libs/masterfip-1.1.4/lib/libmasterfip.a" "/user/poccdev/fgcd2/libs/masterfip-1.1.4/lib/libmockturtle.a" pthread)

# EOF