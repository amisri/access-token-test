# FGCD 2 Framework: Library definition for the fmt library.

add_subdirectory(${LIBRARIES_HOME}/fmt-8.0.1 "${CMAKE_CURRENT_BINARY_DIR}/lib/fmt")
target_link_libraries(FGCD fmt::fmt)

# EOF