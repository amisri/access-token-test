# FGCD 2 Framework: Library definition for the ASIO library.
# https://think-async.com/Asio/
#
# Include by #include <asio/asio.hpp>

include_directories("${LIBRARIES_HOME}/asio-1.24.0")

# EOF