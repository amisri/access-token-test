# FGCD 2 Framework: Library definition for the fmt library.
# FGCD 2 Framework: Library definition for the gTest and gMock library.
# http://google.github.io/googletest/
#
# Include by #include <gtest/gtest.h> and #include <gmock/gmock.h>

add_subdirectory("/user/poccdev/fgcd2/libs/googletest-1.11.0" "${CMAKE_CURRENT_BINARY_DIR}/lib/googletest")
target_link_libraries(FGCD GTest::gtest GTest::gmock)

# EOF