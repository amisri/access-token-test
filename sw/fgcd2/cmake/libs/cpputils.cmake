# FGCD 2 Framework: Library definition for the C++ Utils library.

include_directories("${FGCD_ROOT}/../lib/cpp_utils/include")
fgcdAddSourceDir("${FGCD_ROOT}/../lib/cpp_utils/src")

# EOF