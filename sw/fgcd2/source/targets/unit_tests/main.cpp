#include <gtest/gtest.h>

#include <modules/base/component/inc/Component.h>
#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/core/inc/Application.h>

using namespace fgcd;

RootComponentPtr Application::createRootComponent(int argc, char *argv[]) { return {}; }

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
