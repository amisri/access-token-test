//! @file
//! @brief  Root component for the generic FEC class at CERN.
//! @author Dariusz Zielinski

#include <modules/targets/cern_base/inc/CernBaseTarget.h>
#include <modules/base/core/inc/Application.h>
#include "inc/Fgc4Mainloop.h"

using namespace fgcd;

// **********************************************************

RootComponentPtr Application::createRootComponent(int argc, char *argv[])
{
    auto root = std::make_shared<CernBaseTarget>();

    root->addComponent<Fgc4Mainloop>();

    return root;
}

