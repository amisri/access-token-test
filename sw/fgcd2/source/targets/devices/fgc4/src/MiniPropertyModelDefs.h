#ifndef FGC4_FGCD_MINIPROPERTYMODELDEFS_H
#define FGC4_FGCD_MINIPROPERTYMODELDEFS_H

#include <stdint.h>

enum CC_pars_type
{
    PAR_SIGNED    ,
    PAR_UNSIGNED  ,
    PAR_UINTPTR  ,
    PAR_USHORT    ,
    PAR_HEX       ,
    PAR_FLOAT     ,
    PAR_DOUBLE    ,
    PAR_STRING    ,
    PAR_ENUM      ,
    PAR_BOOL      ,
    PAR_ABS_TIME  ,
    PAR_US_TIME   ,
    PAR_BITMASK   ,
    PAR_POINT     ,
    PAR_POINTER   ,
    PAR_FGIDX ,
    PAR_NUM_TYPES ,
};

union CC_value_p
{
    uint8_t                *c;    //!< Single character
    int32_t                *i;    //!< Signed integer   (32 bits)
    uint32_t               *u;    //!< Unsigned integer (32 bits)
    uintptr_t              *r;    //!< Integer that can hold an pointer (64 bits)
    uint16_t               *h;    //!< Unsigned short integer (16 bits)
    bool                   *b;    //!< Boolean
    float                  *f;    //!< cclibs float
//    cc_double              *d;    //!< cclibs double
    struct CC_us_time     *us;    //!< cclibs microsecond time
    struct FG_point        *p;    //!< libfg point (contains two floats: time & ref)
    char                  **s;    //!< String
//    enum REF_fg_par_idx    *x;    //!< Function Generation parameter index
    void                  **t;    //!< Pointer
};

struct CC_pars
{
    const char                * name;                              //!< Parameter name
    enum CC_pars_type           type;                              //!< Type of value held by parameter
    uint32_t                    max_num_elements;                  //!< Max number of elements (values) that parameter can hold
    const struct CC_pars_enum * ccpars_enum;                       //!< Pointer to ccrt enum structure when parameter type is PAR_ENUM, NULL otherwise
    union CC_value_p            value_p;                           //!< Pointer to parameter value(s)
    uint32_t                    num_default_elements;              //!< Number of elements on program startup
    uint32_t                    flags;                             //!< Mask holding various info about parameter
//    uint32_t                    size_of_struct;                    //!< Size of struct if variable is part of a structure
//    uint32_t                    array_step;                        //!< Array step in bytes used for accessing parameter values in arrays
//    uint32_t                    sub_sel_step;                      //!< Sub-device selector step in bytes used for accessing sub_sel parameters in arrays
//    uint32_t                    cyc_sel_step;                      //!< Cycle selector step in bytes used for accessing cyc_sel parameters in arrays
//
//    // Don't move around the values above!
//
//    char                        full_name[CC_PATH_LEN];            //!< Command name + parameter name, separated with a single space
//    uint32_t                    name_len;                          //!< Length of parameter name
//    uintptr_t                 * num_elements;                      //!< Actual number of elements (values) that parameter holds. For cyc sel parameters
//    //!< it's an array with a separate counter for each selector
//    const struct CC_cmds      * parent_cmd;                        //!< Pointer to parent command structure
};

struct CC_pars_enum
{
    uint32_t     value;
    const char * string;
    uint32_t     flags;
};

// struct ccpars flags

#define PARS_FIXLEN                 0x0001   //!< Fixed length parameter
#define PARS_RO                     0x0002   //!< Read-only parameter
#define PARS_RW                     0x0004   //!< Read/write parameter
#define PARS_CFG                    0x0008   //!< Config parameter must be saved in file to be non-volatile
#define PARS_REG                    0x0010   //!< Libreg must be notified using regMgrPars() when this parameter is changed
#define PARS_CYC_SEL                0x0020   //!< Parameter is a SUB_SEL array
#define PARS_SUB_SEL                0x0040   //!< Parameter is a SUB_SEL array
#define PARS_STAT                   0x0080   //!< Parameter is a status flag on the status display (see ccStatusFlags())
#define PARS_LOG_MPX                0x0100   //!< Parameter is a log multiplexor
#define PARS_TRANS                  0x0200   //!< Parameter is transactional

// struct ccpars_enum flags

#define CC_ENUM_READ_ONLY           0x0001   //!< Enum value is read-only - user cannot request this value explicitly
#define CC_ENUM_RED                 0x0002   //!< Enum value will be displayed in red on the terminal
#define CC_ENUM_YELLOW              0x0004   //!< Enum value will be displayed in yellow on the terminal
#define CC_ENUM_GREEN               0x0008   //!< Enum value will be displayed in green on the terminal
#define CC_ENUM_CYAN                0x0010   //!< Enum value will be displayed in cyan on the terminal

#endif //FGC4_FGCD_MINIPROPERTYMODELDEFS_H
