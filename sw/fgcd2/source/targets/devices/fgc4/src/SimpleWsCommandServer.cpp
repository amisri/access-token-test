//! @file
//! @brief  Playground for development / debug.
//! @author Dariusz Zielinski

#include <string>
#include <unistd.h>

#include "../inc/FgcErrno.h"
#include "../inc/SimpleWsCommandServer.h"
#include <modules/base/thread/inc/threads.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/logging/inc/StdOutLogger.h>

#include "reg_loop_shmem.h"

using namespace fgcd;

// TODO: thread safety??
std::optional<std::string> GetPropertyValueByName(std::string_view property_name_uppercase);
bool SetPropertyValueByName(std::string_view property_name_uppercase, std::string_view value, nlohmann::json& data);

// **********************************************************

SimpleWsCommandServer::SimpleWsCommandServer(Component& parent) : Component(parent, "SimpleWsCommandServer")
{
    m_ws = addComponent<Websocket>(9002);

    m_ws->setMessageHandler("testCommand", [this](WebsocketClient client, nlohmann::json data)
    {
        logInfo("testCommand handler: {} {}", data["text"].get<std::string>(), data["num"].get<int>());
    });

    m_ws->setMessageHandler("G", [this](WebsocketClient client, nlohmann::json data)
    {
        std::string property_name = data["property"];
        std::transform(property_name.begin(), property_name.end(), property_name.begin(), ::toupper);

        auto res = GetPropertyValueByName(property_name);

        m_ws->sendMessage(client, "G_resp", nlohmann::json { { "resp", res.value_or("ERROR: Unknown property") } });
    });

    m_ws->setMessageHandler("S", [this](WebsocketClient client, nlohmann::json data)
    {
        std::string property_name = data["property"];
        std::string value = data["value"];
        std::transform(property_name.begin(), property_name.end(), property_name.begin(), ::toupper);

        auto res = SetPropertyValueByName(property_name, value, data);

        m_ws->sendMessage(client, "S_resp", nlohmann::json { { "rc", res ? FGC_OK_NO_RSP : FGC_UNKNOWN_PROPERTY_OR_ACCESS_DENIED_BY_RBAC } });
    });
}

// **********************************************************

void SimpleWsCommandServer::onStart()
{
    m_thread.start(threads::debug, [&](std::atomic_bool& keep_running)
    {
        threadFunction(keep_running);
    });
}

// **********************************************************

void SimpleWsCommandServer::onStop()
{
    logInfo("SimpleWsCommandServer::onStop");
    m_thread.stop();
    m_thread.forceJoin();
}

// **********************************************************

void SimpleWsCommandServer::threadFunction(std::atomic_bool& keep_running)
{
    usleep(3'000'000);      // wait for RT init

    while (keep_running) {
        auto& tenant = SHMEM_NONVOL.tenants[0];
        auto p_i_meas = regMgrVarPointer(&tenant.reg_mgr, I_MEAS_REG);
        auto p_i_ref = regMgrVarPointer(&tenant.reg_mgr, I_REF_LIMITED);
        auto p_i_err = regMgrVarPointer(&tenant.reg_mgr, I_ERR);
        auto p_v_ref = regMgrVarPointer(&tenant.reg_mgr, V_REF);

        constexpr int PERF_PERIOD_NS = 20;          // nanoseconds
        auto const prof = SHMEM_NONVOL.rt_core_last_profile;        // copy, not a reference!

        auto data = nlohmann::json {
            {"I_MEAS", *p_i_meas},
            {"I_REF", *p_i_ref},
            {"I_ERR", *p_i_err},
            {"V_REF", *p_v_ref},
            {"pb1_gpio0", tenant.pb1_gpio0_pir},
            {"ADC0", {tenant.adc0_data[0], tenant.adc0_data[1], tenant.adc0_data[2], tenant.adc0_data[3]}},
            {"ADC0_raw", {tenant.adc0_raw[0], tenant.adc0_raw[1], tenant.adc0_raw[2], tenant.adc0_raw[3]}},
            {"missed_cycles", SHMEM_NONVOL.missed_cycles},
            {"ref_state", (int)*(refMgrVarPointer(&tenant.ref_mgr, REF_STATE))},
            {"pc_state", (int)refMgrParValue(&tenant.ref_mgr, PC_STATE)},
            {"debug", (int)tenant.ref_mgr.latched_faults},

            {"profile", nlohmann::json {
                    {"adc",    (prof.adc -         prof.start) *    PERF_PERIOD_NS},
                    {"cclibs", (prof.cclibs -      prof.adc) *      PERF_PERIOD_NS},
                    {"dac",    (prof.dac -         prof.cclibs) *   PERF_PERIOD_NS},
                    {"cmd",    (prof.after_cmd -   prof.dac) *      PERF_PERIOD_NS},
                    {"total",  (prof.after_cmd -   prof.start) *    PERF_PERIOD_NS},
            }}
        };
        m_ws->broadcastMessage("SUB", data);

        usleep(100'000);
    }
}

// EOF