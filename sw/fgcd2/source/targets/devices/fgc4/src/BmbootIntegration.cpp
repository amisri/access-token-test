//! @file
//! @brief  Bumbleboot integration for FGC4
//! @author Martin Cejp

#include "../inc/crc32.h"
#include "../inc/BmbootIntegration.h"
#include <modules/base/logging/inc/Logger.h>

#include "bmboot/domain.hpp"

using namespace fgcd;
using bmboot::DomainState;

// ************************************************************



// ************************************************************

static void check_return(bmboot::MaybeError err, const char* function)
{
    if (err.has_value())
    {
        // FIXME: needs to use FGCD2 error reporting facilities once available
        throw std::runtime_error(fmt::format( "bmboot error in {}: {}", function, bmboot::toString(*err)));
    }
}

// ************************************************************

void BmbootIntegration::bootRegloopFromFile(bmboot::IDomain& domain, std::filesystem::path const& binary_path)
{
    std::ifstream file(binary_path.c_str(), std::ios::binary);

    if (!file)
    {
        throw std::runtime_error(fmt::format("Failed to open {}", binary_path));
    }

    std::vector<uint8_t> program((std::istreambuf_iterator<char>(file)),
                                 std::istreambuf_iterator<char>());

    fmt::print("reg_loop status initial: {}\n", BmbootIntegration::getStatusAsString(domain));

    fmt::print("IDomain::ensureReadyToLoadPayload\n");
    check_return(domain.ensureReadyToLoadPayload(), "ensureReadyToLoadPayload");

    fmt::print("IDomain::loadAndStartPayload\n");
    auto checksum = crc32(0, program.data(), program.size());
    check_return(domain.loadAndStartPayload(program, checksum), "loadAndStartPayload");
}

std::string BmbootIntegration::getStatusAsString(bmboot::IDomain& domain)
{
    return bmboot::toString(domain.getState());
}

std::unique_ptr<bmboot::IDomain> BmbootIntegration::openDomain(bmboot::DomainIndex which)
{
    auto maybe_domain = bmboot::IDomain::open(which);

    if (!std::holds_alternative<std::unique_ptr<bmboot::IDomain>>(maybe_domain)) {
        throw std::runtime_error(fmt::format("bmboot error in open_domain: {}",
                                             bmboot::toString(std::get<bmboot::ErrorCode>(maybe_domain))));
    }

    return std::move(std::get<std::unique_ptr<bmboot::IDomain>>(maybe_domain));
}