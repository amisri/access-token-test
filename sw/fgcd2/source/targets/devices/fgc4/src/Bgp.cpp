#define DEVICE_INDEX 0

#include "libcctime.h"
#include "libintlk.h"
#include "libpolswitch.h"
#include "libref.h"
#include "libref/refParsLinkStructs.h"

#include <unistd.h>

#include <algorithm>
#include <array>
#include <numeric>
#include <sstream>
#include <stdarg.h>
#include <vector>

#include "logMenus.h"
#include "logStructs.h"
#include "sigStructs.h"

#include "cctest_runtime.h"

#include "targets/devices/fgc4/inc/Fgc4Shmem.h"

#include "../inc/Equipdev.h"

// should never end up referenced by this core
extern float dummy_float;
extern float sig_cpu_usage_us;
extern uint32_t dummy_bitfield;

#include "logMenusInit.h"
#include "logStructsInit.h"
#include "sigStructsInit.h"

#include "bmboot/domain.hpp"

LOG_menus log_menus;
LOG_read log_read;

#define CC_ILC_MAX_SAMPLES          1000                // Max length of ILC reference buffer

REG_pars reg_pars;
//SIG_struct sig_struct;

CC_pars_mode ccpars_mode;
CC_sim_vars ccsim;

struct REF_ref_pars ccpars_ref[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS];

//struct REF_test_pars ccpars_test[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS] = { {
//    /*[0][0] =*/ { .initial_ref   = { 0.0         },      // TEST INITIAL_REF
//               .amplitude_pp  = { 1.0         },      // TEST AMPLITUDE_PP
//               .num_periods   = { 1           },      // TEST NUM_PERIODS
//               .period_iters  = { 0           },      // TEST PERIOD_ITERS
//               .period        = { 0.8         },      // TEST PERIOD
//               .window        = { CC_ENABLED  },      // TEST WINDOW
//               .exp_decay     = { CC_DISABLED }, }    // TEST EXP_DECAY
//} };

static enum REF_fg_par_idx             static_fg_pars_idx[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS][REF_FG_PAR_MAX_PER_GROUP]; // Null terminal array of libref fg parameter indexes of armed or restored parameters

struct CC_pars_log ccpars_log
        = {
                .time_origin          = { { 0 }, 0 },   // LOG TIME_ORIGIN
                .cyc_sel              = 0,              // LOG CYC_SEL
                .sub_sampling_period  = 0,              // LOG SUB_SAMPLING_PERIOD
//                .prev_cycle           = CC_DISABLED,    // LOG PREV_CYCLE
//                .overwrite            = CC_ENABLED,     // LOG OVERWRITE
                .csv_spy              = CC_ENABLED,     // LOG CSV_SPY
                .binary_spy           = CC_DISABLED,    // LOG BINARY_SPY
//                .pm_buf               = CC_DISABLED,    // LOG PM_BUF
//                .internal_tests       = CC_DISABLED,    // LOG INTERNAL_TESTS
        };

enum CC_enabled_disabled ccpars_logmenu[LOG_NUM_MENUS];

static std::stringstream stdout_accum;

static void ccSimPcStateRT(ShmemTenant& tenant);

static void ccBenchmarkSingle(CCRT2C_script const& script, bool save_log, bmboot::IDomain& domain, int deviceIndex);

void ccRtPrintError(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
}


static void ccRtPrintErrorAndExit(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

//    exit(EXIT_FAILURE);
}

static void ccInitLibreg(ShmemTenant& tenant)
{
    // Set default parameter values

    auto err_num = regMgrInit(
            &tenant.reg_mgr,
            &reg_pars,
            nullptr,
            nullptr,
            nullptr,
            nullptr,
            nullptr,
            0,
            CC_ENABLED,
            CC_ENABLED,
            CC_ENABLED);

    if(err_num != 0)
    {
        // Report error code

        ccRtPrintErrorAndExit("regMgrInit() reports error number %d", err_num);
    }

    // Prepare measurement FIR buffers

    regMeasFilterInitBuffer(&tenant.reg_mgr.b.meas, tenant.i_meas_filter_buffer, CC_FILTER_BUF_LEN);
    regMeasFilterInitBuffer(&tenant.reg_mgr.i.meas, tenant.b_meas_filter_buffer, CC_FILTER_BUF_LEN);

    // Link libreg to liblog to keep the current and field regulation log periods correct

    regMgrInitRegPeriodPointers(&tenant.reg_mgr, &tenant.log_mgr.period[LOG_B_REG].ns, &tenant.log_mgr.period[LOG_I_REG].ns);

    printf("Success regMgrInit\n");
}

static void ccInitLibref(int deviceIndex)
{
    auto& tenant = SHMEM_NONVOL.tenants[deviceIndex];

    // Zero-initialization is assumed for many/all CClibs structures

    memset(&tenant.the_polswitch_mgr, 0, sizeof(tenant.the_polswitch_mgr));
    memset(&tenant.ref_mgr, 0, sizeof(tenant.ref_mgr));

    // Initialize libref parameter pointers to ccrt variables

     // ILC - Link to function parameter array lengths

    //  refMgrParSetValue(&ref_mgr, ILC_L_FUNC_NUM_ELS, ilc_pars[ILC_L_FUNC].num_elements);
    //  refMgrParSetValue(&ref_mgr, ILC_Q_FUNC_NUM_ELS, ilc_pars[ILC_Q_FUNC].num_elements);

     // TRANSACTION

    static struct REF_transaction_pars transaction;

     refMgrInitTransactionPars(&tenant.ref_mgr,
                               &transaction,
                               0,
                               sizeof(transaction));

     // CTRL - this shares ref_pars with REF FG_TYPE

    struct Equipdev_channel &device  = equipdev.device[deviceIndex];

    refMgrInitCtrlPars       (&tenant.ref_mgr, &device.ppm[0].ctrl,                 0, sizeof(device.ppm[0]));

     // REF - this shares ref_pars with REF PLAY and REF DYN_ECO_END_TIME

     refMgrInitRefPars(&tenant.ref_mgr,
                       &ccpars_ref[0][0],
                        0,
                        sizeof(struct REF_ref_pars));

    // RAMP

    refMgrInitRampPars(&tenant.ref_mgr,
                       &tenant.transactional.ramp,
                        0,
                        sizeof(tenant.transactional));

    // PULSE

    refMgrInitPulsePars(&tenant.ref_mgr,
                        &tenant.transactional.pulse,
                        0,
                        sizeof(tenant.transactional));

    // PLEP

    refMgrInitPlepPars(&tenant.ref_mgr,
                       &tenant.transactional.plep,
                        0,
                        sizeof(tenant.transactional));

    // ref_mgr.test_plep_initial_rate = &ccpars_global.test_plep_initial_rate;   // Test inital_rate parameter for PLEP function testing
    // ref_mgr.test_plep_final_rate   = &ccpars_global.test_plep_final_rate;     // Test final_rate parameter for PLEP function testing

    // PPPL

    refMgrInitPpplPars(&tenant.ref_mgr,
                       &tenant.transactional.pppl,
                        0,
                        sizeof(tenant.transactional));

    // PPPL num_els parameters are stored in a contiguous array 2D, hence the sub_sel_step and cyc_sel_step values need to be overridden

    // refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration1_num_els, pppl_pars[PPPL_ACCELERATION1].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration1_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration1_num_els, sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration2_num_els, pppl_pars[PPPL_ACCELERATION2].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration2_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration2_num_els, sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration3_num_els, pppl_pars[PPPL_ACCELERATION3].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration3_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration3_num_els, sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_rate2_num_els        , pppl_pars[PPPL_RATE2].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_rate2_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_rate2_num_els        , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_rate4_num_els        , pppl_pars[PPPL_RATE4].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_rate4_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_rate4_num_els        , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_ref4_num_els         , pppl_pars[PPPL_REF4].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_ref4_num_els         , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_ref4_num_els         , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_duration4_num_els    , pppl_pars[PPPL_DURATION4].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_duration4_num_els    , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_duration4_num_els    , sizeof(uintptr_t));

    // CUBEXP

    refMgrInitCubexpPars(&tenant.ref_mgr,
                         &tenant.transactional.cubexp,
                        0,
                        sizeof(tenant.transactional));

    // CUBEXP num_els parameters are stored in a contiguous array 2D, hence the sub_sel_step and cyc_sel_step values need to be overridden

    // refMgrFgParInitPointer   (&ref_mgr, cubexp_ref_num_els        , cubexp_pars[CUBEXP_REF].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, cubexp_ref_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, cubexp_ref_num_els        , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, cubexp_rate_num_els       , cubexp_pars[CUBEXP_RATE].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, cubexp_rate_num_els       , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, cubexp_rate_num_els       , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, cubexp_time_num_els       , cubexp_pars[CUBEXP_TIME].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, cubexp_time_num_els       , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, cubexp_time_num_els       , sizeof(uintptr_t));

    // TABLE - Not a sub_sel function

    refMgrFgParInitPointer   (&tenant.ref_mgr, table_function            , tenant.transactional.table.function);
    // refMgrFgParInitCycSelStep(&ref_mgr, table_function            , table_pars[TABLE_FUNCTION].cyc_sel_step);

    refMgrFgParInitPointer   (&tenant.ref_mgr, table_function_num_els    , &tenant.transactional.table.num_elements);
    // refMgrFgParInitCycSelStep(&ref_mgr, table_function_num_els    , sizeof(uintptr_t));

    // TRIM

    refMgrInitTrimPars(&tenant.ref_mgr,
                       &tenant.transactional.trim,
                        0,
                        sizeof(tenant.transactional));

    // TEST

    tenant.transactional.test.initial_ref[0] = 0.0f;
    tenant.transactional.test.amplitude_pp[0] = 1.0f;
    tenant.transactional.test.num_periods[0] = 1;
    tenant.transactional.test.period_iters[0] = 0;
    tenant.transactional.test.period[0] = 0.8f;
    tenant.transactional.test.window[0] = CC_ENABLED;
    tenant.transactional.test.exp_decay[0] = CC_DISABLED;

    refMgrInitTestPars(&tenant.ref_mgr,
                       &tenant.transactional.test,
                        0,
                        sizeof(struct REF_test_pars));

    // PRBS

    refMgrInitPrbsPars(&tenant.ref_mgr,
                       &tenant.transactional.prbs,
                        0,
                        sizeof(tenant.transactional));

    // Initialize links from libref function generator parameter indexes to ccrt parameter structures

    // ccrun.fg_pars_link[REF_FG_PAR_REF_FG_TYPE          ] = &ref_pars[REF_PAR_FG_TYPE]         ;  // Function type.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_INITIAL_REF     ] = &ramp_pars[RAMP_INITIAL_REF]       ;  // Initial reference for a RAMP when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_FINAL_REF       ] = &ramp_pars[RAMP_FINAL_REF]         ;  // Final reference for a RAMP.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_ACCELERATION    ] = &ramp_pars[RAMP_ACCELERATION]      ;  // RAMP acceleration.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_DECELERATION    ] = &ramp_pars[RAMP_DECELERATION]      ;  // RAMP deceleration.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_LINEAR_RATE     ] = &ramp_pars[RAMP_LINEAR_RATE]       ;  // Maximum linear rate during the RAMP.
    // ccrun.fg_pars_link[REF_FG_PAR_PULSE_REF            ] = &pulse_pars[PULSE_REF]             ;  // PULSE reference.
    // ccrun.fg_pars_link[REF_FG_PAR_PULSE_DURATION       ] = &pulse_pars[PULSE_DURATION]        ;  // PULSE duration.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_INITIAL_REF     ] = &plep_pars[PLEP_INITIAL_REF]       ;  // Initial reference for a PLEP when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_FINAL_REF       ] = &plep_pars[PLEP_FINAL_REF]         ;  // Final reference for a PLEP.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_ACCELERATION    ] = &plep_pars[PLEP_ACCELERATION]      ;  // PLEP acceleration.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_LINEAR_RATE     ] = &plep_pars[PLEP_LINEAR_RATE]       ;  // Maximum linear rate during the PLEP.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_EXP_TC          ] = &plep_pars[PLEP_EXP_TC]            ;  // PLEP exponential segment time constant.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_EXP_FINAL       ] = &plep_pars[PLEP_EXP_FINAL]         ;  // PLEP exponential segment asymptote.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_INITIAL_REF     ] = &pppl_pars[PPPL_INITIAL_REF]       ;  // Initial reference for a PPPL when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION1   ] = &pppl_pars[PPPL_ACCELERATION1]     ;  // Array of initial PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION2   ] = &pppl_pars[PPPL_ACCELERATION2]     ;  // Array of second PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION3   ] = &pppl_pars[PPPL_ACCELERATION3]     ;  // Array of third PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_RATE2           ] = &pppl_pars[PPPL_RATE2]             ;  // Array of rates at the start of the second PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_RATE4           ] = &pppl_pars[PPPL_RATE4]             ;  // Array of rates at the start of the linear PPPL segments.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_REF4            ] = &pppl_pars[PPPL_REF4]              ;  // Array of references at the start of the linear PPPL segments.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_DURATION4       ] = &pppl_pars[PPPL_DURATION4]         ;  // Array of durations of the fourth linear linear PPPL segments.
    // ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_REF           ] = &cubexp_pars[CUBEXP_REF]           ;  // Array of cubexp start/end references.
    // ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_RATE          ] = &cubexp_pars[CUBEXP_RATE]          ;  // Array of cubexp start/end rates.
    // ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_TIME          ] = &cubexp_pars[CUBEXP_TIME]          ;  // Array of cubexp start/end times.
    // ccrun.fg_pars_link[REF_FG_PAR_TABLE_FUNCTION       ] = &table_pars[TABLE_FUNCTION]        ;  // TABLE function points array.
    // ccrun.fg_pars_link[REF_FG_PAR_TRIM_INITIAL_REF     ] = &trim_pars[TRIM_INITIAL_REF]       ;  // Initial reference for a TRIM when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_TRIM_FINAL_REF       ] = &trim_pars[TRIM_FINAL_REF]         ;  // Final reference for a TRIM.
    // ccrun.fg_pars_link[REF_FG_PAR_TRIM_DURATION        ] = &trim_pars[TRIM_DURATION]          ;  // Duration for a TRIM. Set to zero to be limited only by the rate of change limit.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_INITIAL_REF     ] = &test_pars[TEST_INITIAL_REF]       ;  // Initial reference for a TEST when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_AMPLITUDE_PP    ] = &test_pars[TEST_AMPLITUDE_PP]      ;  // TEST function peak-peak amplitude.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_PERIOD          ] = &test_pars[TEST_PERIOD]            ;  // TEST function period.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_NUM_PERIODS     ] = &test_pars[TEST_NUM_PERIODS]       ;  // Number of TEST function periods to play.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_WINDOW          ] = &test_pars[TEST_WINDOW]            ;  // Control of TEST function half-period sine window.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_EXP_DECAY       ] = &test_pars[TEST_EXP_DECAY]         ;  // Control of TEST function exponential amplitude delay.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_INITIAL_REF     ] = &prbs_pars[PRBS_INITIAL_REF]       ;  // Initial reference for a PRBS when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_AMPLITUDE_PP    ] = &prbs_pars[PRBS_AMPLITUDE_PP]      ;  // PRBS peak-peak amplitude.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_PERIOD_ITERS    ] = &prbs_pars[PRBS_PERIOD_ITERS]      ;  // PRBS period in iterations.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_NUM_SEQUENCES   ] = &prbs_pars[PRBS_NUM_SEQUENCES]     ;  // Number of PRBS sequences to play.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_K               ] = &prbs_pars[PRBS_K]                 ;  // PRBS K factor. Sequence length is 2^K - 1 periods.

    // Initialize memory for error, status and armed structures

//    struct FG_error       * const fg_error   = (struct FG_error       *) calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct FG_error      ));
    struct FG_error       * const fg_error = &tenant.fg_error[0];
    struct REF_cyc_status * const cyc_status = (struct REF_cyc_status *) calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct REF_cyc_status));
//    struct REF_armed      * const ref_armed  = (struct REF_armed      *) calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct REF_armed     ));
    struct REF_armed      * const ref_armed = &tenant.ref_armed[0];

    // TABLE function does not support sub_sel in ccrt, so get memory only for CC_NUM_CYC_SELS - note that libref can support [sub_sel][cyc_sel] tables

    struct FG_point * const armed_table_function = (struct FG_point *) calloc(REF_ARMED_TABLE_FUNCTION_LEN(CC_NUM_CYC_SELS, 1, TABLE_LEN), sizeof(struct FG_point));

    // ILC cycle data store

    struct REF_ilc_cyc_data * const ilc_cyc_data_store = (struct REF_ilc_cyc_data *) calloc(REF_ILC_CYC_DATA_STORE_NUM_ELS(CC_ILC_MAX_SAMPLES,CC_MAX_CYC_SEL),sizeof(struct REF_ilc_cyc_data));

    // Running functions are triple buffered (two nexts and active) so allocated three more table buffers

    struct FG_point * const running_table_function = (struct FG_point *) calloc(REF_RUNNING_TABLE_FUNCTION_LEN(TABLE_LEN), sizeof(struct FG_point));

    if(fg_error               == NULL ||
       cyc_status             == NULL ||
       ref_armed              == NULL ||
       armed_table_function   == NULL ||
       running_table_function == NULL ||
       ilc_cyc_data_store     == NULL)
    {
        ccRtPrintErrorAndExit("Memory allocation failure during libref initialization");
    }

    // Initialize the ref manager

    int32_t err_num =  refMgrInit(&tenant.ref_mgr,
                                  &tenant.the_polswitch_mgr,
                                  &tenant.reg_mgr,
                                  nullptr,
                                  fg_error,
                                  cyc_status,
                                  ref_armed,
                                  armed_table_function,
                                  running_table_function,
                                  ilc_cyc_data_store,
                                  CC_NUM_CYC_SELS,
                                  TABLE_LEN,
                                  CC_ILC_MAX_SAMPLES,
                                  4);                           // ref_to_tc_limit

    if(err_num < 0)
    {
        // Report error code

        ccRtPrintErrorAndExit("refMgrInit() reports error number %d", err_num);
    }
    else if(err_num > 0)
    {
        // Report NULL parameter pointer

        ccRtPrintErrorAndExit("refMgrInit() reports FG parameter %d has a NULL pointer", err_num);
    }

    // Initialize fg_par_idx array

    enum REF_fg_par_idx * fg_pars_idx = &static_fg_pars_idx[0][0][0];
    uint32_t              len = sizeof(static_fg_pars_idx) / sizeof(enum REF_fg_par_idx);

    while(len-- > 0)
    {
        *(fg_pars_idx++) = REF_FG_PAR_NULL;
    }
}

static CC_ns_time state_start_time;

extern "C" bool cctestTimeInStateElapsed(double time_sec)
{
    auto iter_time = cpu0_get_iter_time();
    return cctimeNsRelTimeToDoubleRT(iter_time) - cctimeNsRelTimeToDoubleRT(state_start_time) >= time_sec;
}

#define SCRIPT_int(s) script_##s
#define SCRIPT(s) SCRIPT_int(s)
#define SCRIPT_NAME_int(s) #s
#define SCRIPT_NAME(s) SCRIPT_NAME_int(s)

void Fgc4BgpMain(fgcd::Fgc4Shmem& shmem, bmboot::IDomain& domain) {
    puts("enter Fgc4BgpMain");

//    __builtin___clear_cache((char*) 0x7A000000, (char*) 0x7BFFFFFF);

    auto start = clock();

    // wait for CPU1 to come up, or time out
    while (SHMEM_VOL.cpu1_state == CPU1_STATE_NOTREADY
           && clock() < start + 3 * CLOCKS_PER_SEC) {
        // busy loop
    }

    printf("cpu1_state = %d, expect %d\n", SHMEM_NONVOL.cpu1_state, CPU1_STATE_REG_STOPPED);

    if (SHMEM_NONVOL.cpu1_state != CPU1_STATE_REG_STOPPED) {
        exit(1);
    }

    bool seen_cpu1_up = false;

    while (clock() < start + 3 * CLOCKS_PER_SEC) {
        if (!seen_cpu1_up) {
            if (SHMEM_VOL.cpu1_state != CPU1_STATE_NOTREADY) {
                puts("-> cpu1_up!!");
                seen_cpu1_up = true;
                break;
            }
        }
    }

    printf("INFO built=%s %s use_liblog=%d script=%s\r\n", __DATE__, __TIME__, USE_LIBLOG, SCRIPT_NAME(USE_SCRIPT));

    auto& script = SCRIPT(USE_SCRIPT);

    // do a dry run
    int deviceIndex = 0;
    ccBenchmarkSingle(script, false, domain, deviceIndex);
    SHMEM_NONVOL.missed_cycles = 0;         // forgive any performance issues caused by cold caches on the first run
    SHMEM_NONVOL.missed_cycle_goal = 0;
    SHMEM_NONVOL.missed_cycle_actual = 0;

    for (int i = 0; i < 3; i++) {
        ccBenchmarkSingle(script, false, domain, deviceIndex);
    }

    printf("missed iterations: %d (%lu - %lu = %d)\n", SHMEM_NONVOL.missed_cycles,
           SHMEM_NONVOL.missed_cycle_actual, SHMEM_NONVOL.missed_cycle_goal, (int)(SHMEM_NONVOL.missed_cycle_actual - SHMEM_NONVOL.missed_cycle_goal));
    printf("\nEND\n");

    puts("kickoff cpu1 post-test");
    cpu0_send_cmd(CMD_START_REG);
    cpu0_await_cpu1_state(CPU1_STATE_REG_RUNNING);
    puts("success kickoff cpu1 post-test");
}

static void ccBenchmarkSingle(CCRT2C_script const& script, bool save_log, bmboot::IDomain& domain, int deviceIndex)
{
    // before proceeding, we need cpu1 to init liblog structures
    cpu0_await_cpu1_state(CPU1_STATE_REG_STOPPED);

    auto& tenant = SHMEM_NONVOL.tenants[deviceIndex];

    regMgrParAppValue(&reg_pars, ITER_PERIOD_NS) = script.iter_period_us * 1000;

    ccInitLibreg(tenant);

#if 0
    // Initialise constant parameter values (must be done after call to regMgrInit)

    regMgrParAppValue(&reg_pars, VS_ACTUATION)       = REG_VOLTAGE_REF;
    regMgrParAppValue(&reg_pars, VS_GAIN)            = 0.1f;
    regMgrParAppValue(&reg_pars, MEAS_V_DELAY_ITERS) = 0;

    // Set default value for I_SAT_GAIN for all loads

    for(uint32_t load_select = 0 ; load_select < REG_NUM_LOADS ; load_select++)
    {
        regMgrParAppValue(&reg_pars, LOAD_I_SAT_GAIN)[load_select] = 1.0;
    }

    // regMgrParAppValue(&reg_pars, LIMITS_POS)[0] = 5.0f;

    regMgrParAppValue(&reg_pars, LIMITS_V_POS)[0] = 5.0f;
    regMgrParAppValue(&reg_pars, LIMITS_V_NEG)[0] = -5.0f;
    regMgrParAppValue(&reg_pars, LIMITS_V_RATE)   = 20.0f;      // what units?

    // firing reg configuration
    regMgrParAppValue(&reg_pars, VFILTER_V_MEAS_SOURCE) = REG_MEASUREMENT;
    // regMgrParAppValue(&reg_pars, VREG_EXT_K_P) = 1.0f; DOESN'T WORK
    regMgrParGetValue(&reg_mgr, VREG_EXT_K_P) = 1.0f;

    // reg_mgr.par_groups_mask |= REG_PAR_GROUP_V_LIMITS_REF;
    reg_mgr.par_groups_mask |= REG_PAR_GROUP_VFILTER;           // force initialization of k_w
#endif

    // Set default parameter values

    // regMgrParAppValue(&reg_pars, HARMONICS_AC_PERIOD_ITERS) = ac_period_iters;

    regMgrParsCheck(&tenant.reg_mgr);
#if 0
    regMgrModeSetRT(&reg_mgr, REG_VOLTAGE, false);
#endif
    fprintf(stderr, "initialized reg_mgr; reg_mode: %d\n", tenant.reg_mgr.reg_mode);

    ccInitLibref(deviceIndex);

    // Now that libref parameters have been initialized, we can initialize the signal structures

    sigStructsInit(&tenant.sig_struct, &tenant.the_polswitch_mgr);

    // Initialize log_read structure

    memset(&log_read, 0x55, sizeof(log_read));

    logReadInit(&log_read, &tenant.log_structs, &tenant.log_buffers);                  // logReadInit() is generated automatically from def/log.csv

    // Initialize liblog menus (log_menus) for this application

    logMenusInit(&tenant.log_mgr, &log_menus, 0);     // logMenusInit() is generated automatically from def/log.csv

    // Change the name of two logs to test the logChangeMenuName()
    // which is generated automatically from def/log.csv

//    logChangeMenuName(&log_menus, LOG_MENU_CODIM, "CODIM1");
//    logChangeMenuName(&log_menus, LOG_MENU_DCDIM, "DCDIM1");

    // Change the name and/or units of signals in the TEMP log to test logPrepareSetSigNameAndUnitsRequest()

//    ccLogSetSigNamesAndUnits(LOG_TEMP, LOG_TEMP_EXT_3,       "INTERNAL_TEMP", "C");
//    ccLogSetSigNamesAndUnits(LOG_TEMP, LOG_TEMP_EXT_4,       "DCCT_A_TEMP",    "");
//    ccLogSetSigNamesAndUnits(LOG_TEMP, LOG_TEMP_DCCT_B_TEMP, "",              "C");

    // Libintlk
    struct INTLK_mgr intlk_mgr;
    struct INTLK_pars intlk_pars;
    struct INTLK_latch_values intlk_latch_values;

    memset(&intlk_mgr, 0, sizeof(struct INTLK_mgr));
    memset(&intlk_pars, 0, sizeof(intlk_pars));
    memset(&intlk_latch_values, 0, sizeof(intlk_latch_values));

    // Initialize the libintlk library

    uint32_t interlock_sub_sel = 0;
    uint32_t interlock_cyc_sel = 0;

    uint32_t dummy_state_mask = 1;

    intlkInit(&intlk_mgr,
              &interlock_sub_sel,
              &interlock_cyc_sel,
              &dummy_state_mask,
              regMgrVarPointer(&tenant.reg_mgr, REG_ABS_ERR),
              regMgrVarPointer(&tenant.reg_mgr, MEAS_V_UNFILTERED),
              regMgrVarPointer(&tenant.reg_mgr, MEAS_I_UNFILTERED),
              regMgrVarValue(&tenant.reg_mgr, ITER_PERIOD),
              &intlk_pars,
              0,
              &intlk_latch_values,
              0,
              dummy_state_mask);

    intlkChanParValue(&intlk_pars, CHAN_CONTROL, 0) = INTLK_CHAN_CONDITIONAL;
    intlkChanParValue(&intlk_pars, CHAN_CONTROL, 1) = INTLK_CHAN_CONDITIONAL;
    intlkChanParValue(&intlk_pars, CHAN_CONTROL, 2) = INTLK_CHAN_CONDITIONAL;
    intlkChanParValue(&intlk_pars, CHAN_CONTROL, 3) = INTLK_CHAN_CONDITIONAL;

    intlkRT(&intlk_mgr, true, false);

    // Init state machine
    auto state = script.init();

    // these must be triggered, with valid configuration, before CPU1 kickoff;
    // otherwise we risk crashing it
    regMgrParsCheck(&tenant.reg_mgr);
    logCacheSelectors(&tenant.log_structs);

    state_start_time = {};

//    refMgrParSetValue(&ref_mgr, MODE_REF,     REF_IDLE);
//    refMgrParSetValue(&ref_mgr, PC_STATE,     REF_PC_ON);
//    refMgrParSetValue(&ref_mgr, DIRECT_V_REF, 1.23f);

    // TODO sig.csv: assign stuck_limit_p -> NULL
    // TODO sig.csv: assign cal_num_samples_p -> NULL
    // TODO sig.csv: assign primary_turns_p -> NULL

    const bool verbose = false;
    const bool ascii_plot = false;

    [[maybe_unused]] bool worth_reporting = true;

#ifndef __aarch64__
    std::ofstream logf("log.csv");
    logf << "time,I_ref\n";
#endif

    int i;
    int last_print = 0;
    int last_iter_cpu1 = -1;
    bool cpu1_hung = false;

    // CPU1 KICK-OFF
    puts("kickoff cpu1");
    cpu0_send_cmd(CMD_START_REG);
    cpu0_await_cpu1_state(CPU1_STATE_REG_RUNNING);
    puts("success kickoff cpu1");

    for (i = 0; state != nullptr; i++) {
        /*
         * ccrt order of operations:
         *  - (sync real-time threads)
         *  - increase iter_time
         *  - (simulate super-cycle)
         *  - (simulate circuit)
         *  - (simulate ADC)
         *  - call sigMgrRT
         *  - Set measurements in libreg
         *  - Run CCLIBS real-time activity for this iteration
         *  - (Run the ILC state machine)
         *  - Store all relevant signals in logs
         *  - Simulate PC state
         *  - Wake up the event log thread at the specified rate
         */

        auto iter_cpu1 = SHMEM_VOL.iter_cpu1;

        if (iter_cpu1 == last_iter_cpu1) {
            // This will mis-fire if CPU1 is slowed down by debug printfs; we should give a serious time-out to this
            if (!cpu1_hung) {
                printf("Warning: CPU1 seems to have hung @ %u\n", iter_cpu1);
                cpu1_hung = true;
            }
        }

        last_iter_cpu1 = iter_cpu1;

        auto new_state = (StateFunc_t) state();

        if (i == 0 || new_state != state) {
            state_start_time = cpu0_get_iter_time();
            state = new_state;
//            if (!ascii_plot) {
                printf("NEW STATE @ %d.%d / %d\n", (int)state_start_time.secs.abs, (int)state_start_time.ns, SHMEM_VOL.iter_cpu1);
//                bmboot::dump_debug_info(bmboot::Domain::cpu1);
//            }
            worth_reporting = true;

            // conservatively do these after every state update
            regMgrParsCheck(&tenant.reg_mgr);
            logCacheSelectors(&tenant.log_structs);
        }

        if (ascii_plot) {
            int epoch = SHMEM_VOL.iter_cpu1 / (100'000 / script.iter_period_us);

            if (epoch > last_print) {
                int offset = 80;
                float scale = 80.0f / 10.0f;

                auto val = offset + (int)floor(tenant.reg_mgr.i.ref_limited * scale);
                for (int j = 0; j < val; j++) {
                    printf(" ");
                }
                printf("@\n");
                last_print = epoch;
            }
        }
        else {
//            if (worth_reporting || i % (1'000'000 / ITER_PERIOD_US) == 0) {
//                printf("Iteration %6d\ttime [%5u.%06u]\tref.PC_STATE=%d\tref_mode=%u\treg-mode=%u\tref_state=%u\treg_mgr.i.ref_limited=%9f\n",
//                       i, iter_time.s, iter_time.us,
//                       refMgrParGetValue(&ref_mgr, PC_STATE),
//                       ref_mgr.iter_cache.ref_mode,
//                       ref_mgr.iter_cache.reg_mode,
//                       ref_mgr.ref_state,
//                       reg_mgr.i.ref_limited
//                );
//                worth_reporting = false;
//            }
        }

        if (verbose) {
            printf("\n");
        }

        assert(200'000 >= script.iter_period_us);
        assert(200'000 % script.iter_period_us == 0);

        // Simulate PC state
        // (this is suffixed -RT, but will be done by hardware in practice)

        ccSimPcStateRT(tenant);

        if (verbose) {
//        printf("ref state: %d\n", refMgrParGetValue(&ref_mgr, ...));
            printf("PC state: %d\n", refMgrParValue(&tenant.ref_mgr, PC_STATE));
            printf("REF state: %d\n", tenant.ref_mgr.ref_state);
//        printf("actuation mode: %d (expect REG_FIRING_REF aka 2)\n", regMgrParGetValue(&reg_mgr, VS_ACTUATION));
            printf("ref_reg: %f\n", tenant.reg_mgr.v.ref_reg);
            printf("ref_rate: %f\n", tenant.reg_mgr.ref_rate);
            printf("v.ref: %f\n", tenant.reg_mgr.v.ref); // (rate-limited reference)
            printf("v_reg_err: %f\n", tenant.reg_mgr.v.reg_vars.v_reg_err);
            printf("ref_actuation: %f\n", tenant.reg_mgr.ref_actuation);
            printf("ref_dac: %f\n", tenant.ref_mgr.ref_dac);
        }

        int c = domain.getchar();
        if (c >= 0) {
            if (c == '\n') {
                printf("reg_loop: %s\n", stdout_accum.str().c_str());
                std::stringstream().swap(stdout_accum);         // https://stackoverflow.com/a/23266418
            }
            else {
                stdout_accum << (char)c;
            }
        }

        // FIXME: a hack to get the timing right?
        // what are we aiming for here? we do not need 10us resolution for the slow processes anyway
        usleep(100);
    }

    puts("end loop");
    cpu0_send_cmd(CMD_STOP_REG);
    cpu0_await_cpu1_state(CPU1_STATE_REG_STOPPED);          // patiently wait, otherwise we could mess up some structures and crash the CPU1 program
    printf("longest iter: %ld x 20ns = %.3f us\n", SHMEM_NONVOL.longest_iter_20ns, SHMEM_NONVOL.longest_iter_20ns * 0.02);
}

static void ccSimPcStateRT(ShmemTenant& tenant)
{
    // Fault state if polarity switch fault is latched or When ref_state is OFF and a libref fault is active

    if(   polswitchVarValue(&tenant.the_polswitch_mgr,LATCHED_FAULTS) != 0
          || (   refMgrVarValue(&tenant.ref_mgr, REF_FAULTS_IN_OFF)  != 0
                 && refMgrVarValue(&tenant.ref_mgr, REF_STATE) == REF_OFF))
    {
        refMgrParValue(&tenant.ref_mgr, PC_STATE) = REF_PC_FAULT;
    }
    else
    {
        // Delay switching on but not switching off

        if(ccpars_mode.pc != refMgrParValue(&tenant.ref_mgr,PC_STATE))
        {
            ccsim.pcstate.timer += regMgrVarValue(&tenant.reg_mgr, ITER_PERIOD);

            if(ccpars_mode.pc == REF_PC_OFF || ccsim.pcstate.timer > 1.5F)
            {
                refMgrParValue(&tenant.ref_mgr, PC_STATE) = ccpars_mode.pc;
                ccsim.pcstate.timer = 0.0;
            }
        }
    }
}

extern "C"
void ccRtArmRef(uint32_t sub_sel, uint32_t cyc_sel, uint32_t const num_pars, cc_float const * const par_values)
{
    auto& tenant = SHMEM_NONVOL.tenants[DEVICE_INDEX];

    // Lock mutex before arming to avoid a concurrent call to refEventStartFunc() from the Super Cycle thread

#ifdef HAVE_RUNLOG
    if(ccRtStateIsInitialised() && refMgrParGetValue(&ref_mgr,MODE_USE_ARM_NOW) == CC_ENABLED)
    {
        ccRunLogEventStart(REF_EVENT_USE_ARM_NOW);
    }
#endif

    // Adjust sub_sel/cyc_sel according to MODE REF_SUB_SEL and MODE REF_CYC_SEL

    sub_sel = refMgrSubSel(&tenant.ref_mgr, sub_sel);
    cyc_sel = refMgrCycSel(&tenant.ref_mgr, cyc_sel);

    // Try to arm the reference

    auto fg_errno = refArm(&tenant.ref_mgr, sub_sel, cyc_sel, CC_DISABLED, num_pars, par_values, static_fg_pars_idx[sub_sel][cyc_sel]);

    // Check error response

    if(fg_errno != FG_OK)
    {
        struct FG_error * const fg_error = &tenant.ref_mgr.fg_error[cyc_sel + sub_sel * tenant.ref_mgr.num_cyc_sels];

        if(fg_errno == fg_error->fg_errno)
        {
            ccRtPrintErrorAndExit("Failed to arm XX for sub_sel %u cyc_sel %u: ERR=%u (%u) %g, %g, %g, %g",
//                           ccParsEnumValueToString(enum_fg_type, fg_error->fg_type),
                           sub_sel,
                           cyc_sel,
//                           ccParsEnumValueToString(enum_fgerror_errno,fg_error->fg_errno),
                           fg_error->fg_errno,
                           fg_error->index,
                           fg_error->data[0],
                           fg_error->data[1],
                           fg_error->data[2],
                           fg_error->data[3]);
        }
        else
        {
//            ccRtPrintError(ccParsEnumValueToString(enum_fgerror_errno,ccpars_global.fg_errno));
            ccRtPrintErrorAndExit("unexpected error %d in ccRtArmRef", (int) fg_errno);
        }
    }
}

extern "C"
uint32_t ccCmdsRun()
{
    auto& tenant = SHMEM_NONVOL.tenants[DEVICE_INDEX];

    auto iter_time = SHMEM_NONVOL.iter_time_cpu1[SHMEM_NONVOL.iter_time_rdptr_cpu1];

    // Calculate run time

    struct CC_ns_time run_time;

    run_time = cc_zero_ns;

    // Bail out if the calculated run time is in the past

    struct CC_ns_time delta_time = cc_zero_ns;

    if(run_time.secs.abs > 0)
    {
        delta_time = cctimeNsSubRT(iter_time, run_time);

        if(cctimeNsCmpRelTimesRT(delta_time, cc_zero_ns) > 0)
        {
            char strbuf[22];

            ccRtPrintError("Run time is already %s s in the past", cctimeNsPrintRelTime(delta_time, strbuf));
        }
    }

    // Issue Run event

#ifdef HAVE_RUNLOG
    ccRunLogEventStart(REF_EVENT_RUN);
#endif

    refMgrParValue(&tenant.ref_mgr, REF_RUN) = cctimeNsToUsRT(run_time);

    if(refEventRun(&tenant.ref_mgr) == false)
    {
        ccRtPrintErrorAndExit("No armed reference function is ready to run in the current state");
    }

    return EXIT_SUCCESS;
}
