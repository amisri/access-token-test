#ifndef FGC4_FGCD_MINIPROPERTYMODEL_H
#define FGC4_FGCD_MINIPROPERTYMODEL_H

#include "MiniPropertyModelDefs.h"
#include "MiniPropertyModelCclibs.h"

#define CCPARS_DIRECT_EXT
#define CCPARS_STATE_EXT
#define GLOBALS

CCPARS_STATE_EXT struct CC_pars_enum enum_reg_mode[]
#ifdef GLOBALS
= {
    { REG_NONE    , "NONE",    CC_ENUM_READ_ONLY },
    { REG_VOLTAGE , "VOLTAGE", 0                 },
    { REG_CURRENT , "CURRENT", 0                 },
    { REG_FIELD   , "FIELD",   0                 },
    { 0           , NULL                         },
}
#endif
;

CCPARS_STATE_EXT struct CC_pars_enum enum_state_pc[]
#ifdef GLOBALS
= {
    { REF_PC_OFF   , "OFF"   , 0                             },
    { REF_PC_ON    , "ON"    , CC_ENUM_GREEN                 },
    { REF_PC_FAULT , "FAULT" , CC_ENUM_RED|CC_ENUM_READ_ONLY },
    { 0            , NULL    , 0                             },
}
#endif // GLOBALS
;

//! Used for:
//! * MODE REF
//! * STATE REF
//! parameters value decoding

CCPARS_STATE_EXT struct CC_pars_enum enum_state_ref[]
#ifdef GLOBALS
= {
    { REF_OFF           , "OFF"           , 0                              },
    { REF_POL_SWITCHING , "POL_SWITCHING" , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_OFF        , "TO_OFF"        , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_STANDBY    , "TO_STANDBY"    , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_CYCLING    , "TO_CYCLING"    , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_IDLE       , "TO_IDLE"       , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_DIRECT        , "DIRECT"        , CC_ENUM_GREEN                  },
    { REF_STANDBY       , "STANDBY"       , CC_ENUM_GREEN                  },
    { REF_CYCLING       , "CYCLING"       , CC_ENUM_GREEN                  },
    { REF_DYN_ECO       , "DYN_ECO"       , CC_ENUM_READ_ONLY              },
    { REF_FULL_ECO      , "FULL_ECO"      , CC_ENUM_READ_ONLY              },
    { REF_IDLE          , "IDLE"          , CC_ENUM_GREEN                  },
    { REF_ARMED         , "ARMED"         , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_RUNNING       , "RUNNING"       , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_PAUSED        , "PAUSED"        , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { 0                 , NULL            , 0                              },
}
#endif // GLOBALS
;

CCPARS_STATE_EXT struct CC_pars mode_pars[]
#ifdef GLOBALS
= {// name                 type       max_n_els                 *enum                         *value                                                n_els                   flags
    { "MODE.PC"                , PAR_ENUM,     1,                     enum_state_pc,         { .u = &ccpars_mode_pc                                     }, 1,                    PARS_RW          },
    { "MODE.REF"               , PAR_ENUM,     1,                     enum_state_ref,        { .u = (uint32_t*)refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,MODE_REF             ) }, 1,                    PARS_RW          },
//    { "REF_SUB_SEL"       , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_REF_SUB_SEL     ) }, 1,                    PARS_RW|PARS_CFG },
//    { "REF_CYC_SEL"       , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_REF_CYC_SEL     ) }, 1,                    PARS_RW|PARS_CFG },
//    { "TEST_CYC_SEL"      , PAR_UNSIGNED, 1,                     NULL,                  { .u = refMgrParGetPointer(&ref_mgr,MODE_TEST_CYC_SEL    ) }, 1,                    PARS_RW          },
//    { "TEST_REF_CYC_SEL"  , PAR_UNSIGNED, 1,                     NULL,                  { .u = refMgrParGetPointer(&ref_mgr,MODE_TEST_REF_CYC_SEL) }, 1,                    PARS_RW          },
//    { "TEST_REG_MODE"     , PAR_ENUM,     1,                     enum_reg_mode,         { .u = refMgrParGetPointer(&ref_mgr,MODE_TEST_REG_MODE   ) }, 1,                    PARS_RW          },
//    { "EVENT_OFFSET_US"   , PAR_SIGNED,   1,                     NULL,                  { .i = refMgrParGetPointer(&ref_mgr,MODE_EVENT_OFFSET_US ) }, 1,                    PARS_RW          },
    { "REG.MODE"          , PAR_ENUM,     1,                     enum_reg_mode,         { .u = (uint32_t*)refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,MODE_REG_MODE        ) }, 1,                    PARS_RW          },
//    { "REG_MODE_CYC"      , PAR_ENUM,     1,                     enum_reg_mode,         { .u = refMgrParGetPointer(&ref_mgr,MODE_REG_MODE_CYC    ) }, 1,                    PARS_RW|PARS_CFG },
//    { "PRE_FUNC"          , PAR_ENUM,     1,                     enum_pre_func_mode,    { .u = refMgrParGetPointer(&ref_mgr,MODE_PRE_FUNC        ) }, 1,                    PARS_RW|PARS_CFG },
//    { "POST_FUNC"         , PAR_ENUM,     1,                     enum_post_func_mode,   { .u = refMgrParGetPointer(&ref_mgr,MODE_POST_FUNC       ) }, 1,                    PARS_RW|PARS_CFG },
//    { "SIM_MEAS"          , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_SIM_MEAS        ) }, 1,                    PARS_RW|PARS_CFG },
//    { "RT_REF"            , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_RT_REF          ) }, 1,                    PARS_RW|PARS_CFG },
//    { "RT_NUM_STEPS"      , PAR_UNSIGNED, 1,                     NULL,                  { .u = refMgrParGetPointer(&ref_mgr,RT_NUM_STEPS         ) }, 1,                    PARS_RW|PARS_CFG },
//    { "ECONOMY"           , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_ECONOMY         ) }, 1,                    PARS_RW|PARS_CFG },
//    { "USE_ARM_NOW"       , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_USE_ARM_NOW     ) }, 1,                    PARS_RW|PARS_CFG },
//    { "TO_OFF_FAULTS"     , PAR_BITMASK,  1,                     enum_ref_faults,       { .u = refMgrParGetPointer(&ref_mgr,MODE_TO_OFF_FAULTS   ) }, 1,                    PARS_RW|PARS_CFG },
//    { "ILC"               , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_ILC             ) }, 1,                    PARS_RW|PARS_CFG },
//    { "HARMONICS"         , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParGetPointer(&ref_mgr,MODE_HARMONICS       ) }, 1,                    PARS_RW          },
    { NULL }
}
#endif
;

CCPARS_DIRECT_EXT struct CC_pars direct_pars[]
#ifdef GLOBALS
= {// name               type     max_n_els *enum           *value                                                n_els  flags
        { "V_REF"          , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,DIRECT_V_REF          ) }, 1,   PARS_RW|PARS_CFG },
        { "REF.DIRECT.I.VALUE", PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,DIRECT_I_REF          ) }, 1,   PARS_RW|PARS_CFG },
        { "B_REF"          , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,DIRECT_B_REF          ) }, 1,   PARS_RW|PARS_CFG },
        { "DEGAUSS_AMP_PP" , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,DIRECT_DEGAUSS_AMP_PP ) }, 1,   PARS_RW|PARS_CFG },
        { "DEGAUSS_PERIOD" , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&SHMEM_NONVOL.tenants[0].ref_mgr,DIRECT_DEGAUSS_PERIOD ) }, 1,   PARS_RW|PARS_CFG },

        // JUST OTHERS don't ask
//        { "ADC0.0", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.adc0_data[0] }, 1, PARS_RO },
//        { "ADC0.1", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.adc0_data[1] }, 1, PARS_RO },
//        { "ADC0.2", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.adc0_data[2] }, 1, PARS_RO },
//        { "ADC0.3", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.adc0_data[3] }, 1, PARS_RO },
        { "ADC0.Z", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.tenants[0].adc0_zero }, 1, PARS_RO },
        { "ADC0.P", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.tenants[0].adc0_pos10v }, 1, PARS_RO },
        { "ADC0.N", PAR_SIGNED, 1, NULL, {.i = &SHMEM_NONVOL.tenants[0].adc0_neg10v }, 1, PARS_RO },


        { NULL }
}
#endif
;

#endif //FGC4_FGCD_MINIPROPERTYMODEL_H
