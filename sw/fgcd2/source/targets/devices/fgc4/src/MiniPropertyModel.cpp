#include "MiniPropertyModel.h"

#include "../inc/cctest_runtime.h"

#include <charconv>
#include <optional>
#include <string>
#include <unistd.h>

#include <json/json.hpp>

uint32_t ccpars_mode_pc;            // TBD what to do about this

// https://stackoverflow.com/a/4119881
bool iequals(std::string_view a, std::string_view b)
{
    return std::equal(a.begin(), a.end(),
                      b.begin(), b.end(),
                      [](char a, char b) {
                          return tolower(a) == tolower(b);
                      });
}

CC_pars* FindParsByName(std::string_view property_name_uppercase) {
    for (auto pars = &mode_pars[0]; pars->name != nullptr; pars++) {
        if (property_name_uppercase == pars->name) {
            return pars;
        }
    }

    for (auto pars = &direct_pars[0]; pars->name != nullptr; pars++) {
        if (property_name_uppercase == pars->name) {
            return pars;
        }
    }

    return nullptr;
}

std::optional<std::string> EnumValueToString(uint32_t value, const struct CC_pars_enum * the_enum) {
    for (; the_enum->string != nullptr; the_enum++) {
        if (the_enum->value == value) {
            return the_enum->string;
        }
    }

    return {};
}

std::optional<uint32_t> StringToEnumValue(std::string_view str, const struct CC_pars_enum * the_enum) {
    for (; the_enum->string != nullptr; the_enum++) {
        if (iequals(the_enum->string, str)) {
            return the_enum->value;
        }
    }

    return {};
}

std::optional<std::string> GetPropertyValueByName(std::string_view property_name_uppercase) {
    auto maybe_pars = FindParsByName(property_name_uppercase);

    if (!maybe_pars) {
        return {};
    }

    auto& pars = *maybe_pars;

    switch (pars.type) {
        case PAR_ENUM: {
            auto as_str = EnumValueToString(*pars.value_p.u, pars.ccpars_enum);
            return as_str;
        }

        case PAR_FLOAT:
            return std::to_string(*pars.value_p.f);

        case PAR_SIGNED:
            return std::to_string(*pars.value_p.i);

        default:
            return "Unhandled type";
    }
}

bool SetPropertyValueByName(std::string_view property_name_uppercase, std::string_view value,
                            nlohmann::json& data) {
    auto& tenant = SHMEM_NONVOL.tenants[0];

    if (property_name_uppercase == "DAC0" || property_name_uppercase == "DAC1") {
        tenant.dac_data = std::stoi(std::string(value), nullptr, 0);
        tenant.dac_value = std::stod(std::string(value), nullptr);
        cpu0_send_cmd(property_name_uppercase == "DAC0" ? CMD_SET_DAC0 : CMD_SET_DAC1);
        return true;
    }

    if (property_name_uppercase == "VS_CMD") {
        tenant.vs_cmd = std::stoi(std::string(value), nullptr, 0);
        if (tenant.vs_cmd != 1) {
            // off
            refMgrParValue(&tenant.ref_mgr,PC_STATE) = REF_PC_OFF;
        }
        else {
            // on! (not quite correct... should wait for converter confirmation)
            refMgrParValue(&tenant.ref_mgr,PC_STATE) = REF_PC_ON;
        }
        cpu0_send_cmd(CMD_SET_VS_CMD);
        return true;
    }

    if (property_name_uppercase == "ADC_INPUT") {
        if (value == "EXT") {
            cpu0_send_cmd(CMD_SET_ADC_INPUT_EXT);
        }
        else if (value == "DAC") {
            cpu0_send_cmd(CMD_SET_ADC_INPUT_DAC);
        }
        return true;
    }

    if (property_name_uppercase == "REF.MODE") {
        if (value == "IDLE") {
            *(refMgrParPointer(&tenant.ref_mgr, MODE_REF)) = REF_IDLE;
        }
        else if (value == "OFF") {
            *(refMgrParPointer(&tenant.ref_mgr, MODE_REF)) = REF_OFF;
        }
        return true;
    }

    auto arm_and_run = []() {
        printf("ref_state pre arm: %d\n", *(refMgrVarPointer(&tenant.ref_mgr, REF_STATE)));
        ccRtArmRef(0, 0, 0, NULL);

        for (int i = 0; i < 100; i++) {
            if (*(refMgrVarPointer(&tenant.ref_mgr, REF_STATE)) == REF_ARMED) {
                break;
            }

            usleep(10'000);
        }

        printf("ref_state post arm: %d\n", *(refMgrVarPointer(&tenant.ref_mgr, REF_STATE)));

        ccCmdsRun();
        usleep(10'000);
        printf("ref_state post run: %d\n", *(refMgrVarPointer(&tenant.ref_mgr, REF_STATE)));
    };

    if (property_name_uppercase == "REF_NOW") {
        // Initiate a ramp transition
        *(ccpars_ref[0][0].fg_type) = FG_RAMP;
        tenant.transactional.ramp.initial_ref[0] = 1.0f;  // will be presumably ignored in IDLE,
        // but taken into account if starting from STANDBY
        tenant.transactional.ramp.final_ref[0] = std::stod(std::string(value), nullptr);

        tenant.transactional.ramp.acceleration[0] = 5.0f;
        tenant.transactional.ramp.deceleration[0] = 5.0f;
        tenant.transactional.ramp.linear_rate[0] = 5.0f;

        arm_and_run();
        return true;
    }

    if (property_name_uppercase == "REF_STEP") {
        // Initiate a ramp transition
        *(ccpars_ref[0][0].fg_type) = FG_RAMP;
        tenant.transactional.ramp.initial_ref[0] = 1.0f;  // will be presumably ignored in IDLE,
        // but taken into account if starting from STANDBY
        tenant.transactional.ramp.final_ref[0] = std::stod(std::string(value), nullptr);

        tenant.transactional.ramp.acceleration[0] = 2e5f;
        tenant.transactional.ramp.deceleration[0] = 2e5f;
        tenant.transactional.ramp.linear_rate[0] = 2e5f;

//        SHMEM_NONVOL.transactional.test.

        arm_and_run();
        return true;
    }

    if (property_name_uppercase == "REF_SINE") {
        *(ccpars_ref[0][0].fg_type) = FG_SINE;
        tenant.transactional.test.initial_ref[0] = 0.0f;
        tenant.transactional.test.amplitude_pp[0] = 2.0f * std::stod(std::string(value), nullptr);
        tenant.transactional.test.num_periods[0] = 5;
        tenant.transactional.test.period_iters[0] = 0;    // ignored?
        tenant.transactional.test.period[0] = std::stod((std::string) data["period"]);
        tenant.transactional.test.window[0] = CC_DISABLED;
        tenant.transactional.test.exp_decay[0] = CC_DISABLED;

        arm_and_run();
        return true;
    }

    if (property_name_uppercase == "DEV.SAVE_LOGS") {
        void ccLogWriteSpy(bool csv, bool binary);

        ccLogWriteSpy(true, false);
        return true;
    }

    auto maybe_pars = FindParsByName(property_name_uppercase);

    if (!maybe_pars) {
        return false;
    }

    auto& pars = *maybe_pars;

    switch (pars.type) {
        case PAR_ENUM: {
            auto as_int = StringToEnumValue(value, pars.ccpars_enum);
            if (as_int.has_value()) {
                *pars.value_p.u = *as_int;

//                if (pars.value_p.u == &ccpars_mode_pc)
//                {
//                    switch (ccpars_mode_pc)
//                    {
//                        case FGC_PC_ON_STANDBY:  refMgrParSetValue  (&ref_mgr, MODE_REF, REF_STANDBY);  break;
//                        case FGC_PC_IDLE:        refMgrParSetValue  (&ref_mgr, MODE_REF, REF_IDLE   );  break;
//                        case FGC_PC_CYCLING:     refMgrParSetValue  (&ref_mgr, MODE_REF, REF_CYCLING);  break;
//                        case FGC_PC_DIRECT:      refMgrParSetValue  (&ref_mgr, MODE_REF, REF_DIRECT );  break;
//                        default:                 refMgrParSetValue  (&ref_mgr, MODE_REF, REF_OFF    );  break;
//                    }
//                }

                return true;
            }
            else {
                return false;
            }
        }

        case PAR_FLOAT: {
            // our version of gcc doesn't implement std::from_chars for float
            *pars.value_p.f = std::stof(std::string(value));
            return true;
        }

        default:
            return false;
    }
}
