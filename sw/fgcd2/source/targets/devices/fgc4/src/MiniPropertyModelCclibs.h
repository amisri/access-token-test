#ifndef FGC4_FGCD_MINIPROPERTYMODELCCLIBS_H
#define FGC4_FGCD_MINIPROPERTYMODELCCLIBS_H

#include <libreg.h>
#include <libref.h>

#include <reg_loop_shmem.h>

extern uint32_t ccpars_mode_pc;

#endif //FGC4_FGCD_MINIPROPERTYMODELCCLIBS_H
