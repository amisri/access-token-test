//! @file
//! @brief  Playground for development / debug.
//! @author Dariusz Zielinski

#include "../inc/BmbootIntegration.h"
#include "../inc/Fgc4Mainloop.h"
#include "../inc/Fgc4Shmem.h"
#include "inc/SimpleWsCommandServer.h"
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>

void Fgc4BgpMain(fgcd::Fgc4Shmem& shmem, bmboot::IDomain& domain);

using namespace fgcd;

// **********************************************************

Fgc4Mainloop::Fgc4Mainloop(Component& parent) : Component(parent, "Fgc4Mainloop")
{
    addComponent<SimpleWsCommandServer>();

    m_stats.all = 15;

    m_stats.bad.reasons.failed++;
    m_stats.bad.reasons.failed++;
    m_stats.bad.reasons.rejected = 10;

    m_stats_root.print();


}

// **********************************************************

void Fgc4Mainloop::onStart()
{
    m_thread.start(threads::debug, [&](std::atomic_bool& keep_running)
    {
        threadFunction(keep_running);
    });
}

// **********************************************************

void Fgc4Mainloop::onStop()
{
    m_thread.stop();
    m_thread.forceJoin();

    // park CPU1
    *(uint64_t volatile*)0x7fff'ffe8 = 0x7fff'ffe0;

//    throw 5;
}

// **********************************************************

void Fgc4Mainloop::threadFunction(std::atomic_bool& keep_running)
{
    /*while (keep_running)
    {
        logInfo("My message is {}.", 42);
        std::this_thread::sleep_for(1s);

    }*/

    // Before booting regloop, set up IPC region
    // TODO: must ensure CPU1 parked already before doing this!
    Fgc4Shmem shmem;
    shmem.initIpcDataStructures();

    // Initialize bare-metal CPU domain
    auto domain = BmbootIntegration::openDomain(bmboot::DomainIndex::cpu1);
    BmbootIntegration::bootRegloopFromFile(*domain, "reg_loop.bin");
    logInfo("reg_loop status: {}", BmbootIntegration::getStatusAsString(*domain));

    Fgc4BgpMain(shmem, *domain);

//    puts("All ok, exiting");
//    _Exit(0);           // C standard version of _exit
}

// EOF