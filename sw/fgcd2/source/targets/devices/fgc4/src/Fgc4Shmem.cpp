//! @file
//! @brief  FGC4 shared memory routines
//! @author Martin Cejp

#include "../inc/Fgc4Shmem.h"
//#include <modules/base/logging/inc/Logger.h>

#include "reg_loop_shmem.h"

using namespace fgcd;

// ************************************************************

Fgc4Shmem::Fgc4Shmem()
    : dev_mem("/dev/mem"),
      base_0x7A000000(dev_mem, 0x7A000000, 96*1024*1024, PROT_READ | PROT_WRITE, (void*) 0x7A000000)
{
}

// ************************************************************

void Fgc4Shmem::initIpcDataStructures()
{
    // Cache concerns during initialization
    // we need these writes to be cache coherent -- 3 ways to tackle this
    // a) we let EL1 initialize before writing any of this
    // b) we do not let EL1 touch cache and init it in EL3
    // c) have bmboot provide a memcpy_to_executor function
    //
    // (Though it does seem to work for the moment)

    memset(&SHMEM_NONVOL, 0, sizeof(SHMEM_NONVOL));

    SHMEM_NONVOL.cpu1_state = CPU1_STATE_NOTREADY;

//    SHMEM_NONVOL.cpu0_to_cpu1.data = &SHMEM_NONVOL.cpu0_to_cpu1_buffer[0];
//    SHMEM_NONVOL.cpu0_to_cpu1.size = sizeof(SHMEM_NONVOL.cpu0_to_cpu1_buffer);
//    SHMEM_NONVOL.cpu0_to_cpu1.readpos = 0;
//    SHMEM_NONVOL.cpu0_to_cpu1.writepos = 0;
//
//    SHMEM_NONVOL.cpu1_to_cpu0.data = &SHMEM_NONVOL.cpu1_to_cpu0_buffer[0];
//    SHMEM_NONVOL.cpu1_to_cpu0.size = sizeof(SHMEM_NONVOL.cpu1_to_cpu0_buffer);
//    SHMEM_NONVOL.cpu1_to_cpu0.readpos = 0;
//    SHMEM_NONVOL.cpu1_to_cpu0.writepos = 0;

    SHMEM_NONVOL.iter_cpu1 = 0;

    SHMEM_NONVOL.iter_time_rdptr_cpu1 = 0;
    memset(&SHMEM_NONVOL.iter_time_cpu1, 0, sizeof(SHMEM_NONVOL.iter_time_cpu1));

    SHMEM_NONVOL.missed_cycles = 0;

    SHMEM_NONVOL.cpu0_cmd = 0;
    SHMEM_NONVOL.cpu0_cmd_seq = 0;
}
