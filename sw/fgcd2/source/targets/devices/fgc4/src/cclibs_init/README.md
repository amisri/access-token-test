### How to re-generate

```
cd /opt/mcejp/projects/fgc/sw/lib/cclibs/ccrt/
make clean
make     # (OK if compilation/linking fails)

/opt/mcejp/projects/ccrt2c
. ~pclhc/bin/python/ccs_venv/bin/activate.fish
python ccrt_extract_pars.py /opt/mcejp/projects/fgc/sw/lib/cclibs/ccrt

set CONV_DIR /opt/mcejp/projects/fgc/sw/lib/cclibs/ccrt/converters/fgc4
set OUT_DIR /opt/mcejp/projects/fgc/sw/fgcd2/source/targets/devices/fgc4/src/cclibs_init
python ccrt2c.py $CONV_DIR/tests/idle/idle.cct $OUT_DIR/script_idle.c
```
