// GENERATED AUTOMATICALLY FROM tests/direct/direct.cct
// DO NOT EDIT BY HAND

#include "cctest_runtime.h"

#define DEVICE_INDEX 0
#define ref_mgr (SHMEM_NONVOL.tenants[DEVICE_INDEX].ref_mgr)
#define reg_mgr (SHMEM_NONVOL.tenants[DEVICE_INDEX].reg_mgr)
#define sig_struct (SHMEM_NONVOL.tenants[DEVICE_INDEX].sig_struct)

static StateFunc_t WAIT_5_0_None_None_None_None(void);
static StateFunc_t WAIT_4_0_None_None_None_None(void);
static StateFunc_t WAIT_0_8_None_None_None_None(void);
static StateFunc_t WAIT_1_0_None_None_None_None(void);

static StateFunc_t INIT(void)
{
    // BREG            PERIOD_ITERS                  10  10  10  10
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[0] = 10;
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[1] = 10;
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[2] = 10;
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[3] = 10;
    // BREG            EXTERNAL_ALG                  DISABLED  DISABLED  DISABLED  DISABLED
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[0] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[1] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[2] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[3] = CC_DISABLED;
    // BREG            INT_MEAS_SELECT               EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // BREG            INT_PURE_DELAY_PERIODS        0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[3] = 0.000000E+00;
    // BREG            INT_AUXPOLE1_HZ               1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[3] = 1.000000E+01;
    // BREG            INT_AUXPOLES2_HZ              1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[3] = 1.000000E+01;
    // BREG            INT_AUXPOLES2_Z               5.000000E-01  5.000000E-01  5.000000E-01  5.000000E-01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[0] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[1] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[2] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[3] = 5.000000E-01;
    // BREG            INT_AUXPOLE4_HZ               0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[3] = 0.000000E+00;
    // BREG            INT_AUXPOLE5_HZ               0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[3] = 0.000000E+00;
    // BREG            EXT_MEAS_SELECT               EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // BREG            EXT_TRACK_DELAY_PERIODS       0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[3] = 0.000000E+00;
    // BREG            EXT_OP_R                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[15] = 0.000000E+00;
    // BREG            EXT_OP_S                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[15] = 0.000000E+00;
    // BREG            EXT_OP_T                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[15] = 0.000000E+00;
    // CAL             DCCT_PRIMARY_TURNS            1
    // (skipped write to &ccpars_cal.dcct_primary_turns)
    // CAL             NUM_SAMPLES                   10000
    // (skipped write to &ccpars_cal.num_samples)
    // CAL             DCCT_A_POSITION               CONVERTER
    *(sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_POSITION              )) = SIG_TRANSDUCER_POSITION_CONVERTER;
    // CAL             DCCT_A_GAIN                   1.000000E+01
    *(sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_NOMINAL_GAIN          )) = 1.000000E+01;
    // CAL             DCCT_B_POSITION               LOAD
    *(sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_POSITION              )) = SIG_TRANSDUCER_POSITION_LOAD;
    // CAL             DCCT_B_GAIN                   1.000000E+01
    *(sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_NOMINAL_GAIN          )) = 1.000000E+01;
    // CAL             V_PROBE_GAIN                  1.000000E+01
    *(sigVarPointer(&sig_struct, transducer, v_probe , TRANSDUCER_NOMINAL_GAIN          )) = 1.000000E+01;
    // CAL             B_PROBE_GAIN                  1.000000E+00
    *(sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_NOMINAL_GAIN          )) = 1.000000E+00;
    // CAL             I_PROBE_GAIN                  1.000000E+00
    *(sigVarPointer(&sig_struct, transducer, i_probe , TRANSDUCER_NOMINAL_GAIN          )) = 1.000000E+00;
    // DECO            PHASE                         0
    *(regMgrParAppPointer(&reg_pars, DECO_PHASE)) = 0;
    // DECO            INDEX                         0
    *(regMgrParAppPointer(&reg_pars, DECO_INDEX)) = 0;
    // DECO            K                             0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_DECO), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[3] = 0.000000E+00;
    // DECO            D                             0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_DECO), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[3] = 0.000000E+00;
    // DEFAULT         V_ACCELERATION                1.000000E+02  1.010000E+02  1.020000E+02  1.030000E+02
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[0] = 1.000000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[1] = 1.010000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[2] = 1.020000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[3] = 1.030000E+02;
    // DEFAULT         V_DECELERATION                7.000000E+01  7.100000E+01  7.200000E+01  7.300000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[0] = 7.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[1] = 7.100000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[2] = 7.200000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[3] = 7.300000E+01;
    // DEFAULT         V_LINEAR_RATE                 9.000000E+01  9.100000E+01  9.200000E+01  9.300000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[0] = 9.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[1] = 9.100000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[2] = 9.200000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[3] = 9.300000E+01;
    // DEFAULT         V_PRE_FUNC_MAX                1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[0] = 1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[1] = 1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[2] = 1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[3] = 1.000000E+01;
    // DEFAULT         V_PRE_FUNC_MIN               -1.000000E+01  0.000000E+00  0.000000E+00 -1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[0] = -1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[1] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[2] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[3] = -1.000000E+01;
    // DEFAULT         V_MINRMS                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[0] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[1] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[2] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[3] = 0.000000E+00;
    // DEFAULT         I_ACCELERATION                1.003000E+02  1.013000E+02  1.023000E+02  1.033000E+02
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[0] = 1.003000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[1] = 1.013000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[2] = 1.023000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[3] = 1.033000E+02;
    // DEFAULT         I_DECELERATION                7.030000E+01  7.130000E+01  7.230000E+01  7.330000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[0] = 7.030000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[1] = 7.130000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[2] = 7.230000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[3] = 7.330000E+01;
    // DEFAULT         I_LINEAR_RATE                 9.030000E+01  9.130000E+01  9.230000E+01  9.330000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[0] = 9.030000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[1] = 9.130000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[2] = 9.230000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[3] = 9.330000E+01;
    // DEFAULT         I_PRE_FUNC_MAX                9.000000E+00  9.000000E+00  9.000000E+00  9.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[0] = 9.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[1] = 9.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[2] = 9.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[3] = 9.000000E+00;
    // DEFAULT         I_PRE_FUNC_MIN               -9.000000E+00  0.000000E+00  3.000000E-01 -9.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[0] = -9.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[1] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[2] = 3.000000E-01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[3] = -9.000000E+00;
    // DEFAULT         I_MINRMS                      0.000000E+00  1.000000E-01  1.000000E-01  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[0] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[1] = 1.000000E-01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[2] = 1.000000E-01;
    (refMgrParPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[3] = 0.000000E+00;
    // DEFAULT         B_ACCELERATION                1.006000E+02  1.016000E+02  1.026000E+02  1.036000E+02
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[0] = 1.006000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[1] = 1.016000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[2] = 1.026000E+02;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[3] = 1.036000E+02;
    // DEFAULT         B_DECELERATION                7.060000E+01  7.160000E+01  7.260000E+01  7.360000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[0] = 7.060000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[1] = 7.160000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[2] = 7.260000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[3] = 7.360000E+01;
    // DEFAULT         B_LINEAR_RATE                 9.060000E+01  9.160000E+01  9.260000E+01  9.360000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[0] = 9.060000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[1] = 9.160000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[2] = 9.260000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[3] = 9.360000E+01;
    // DEFAULT         B_PRE_FUNC_MAX                1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[0] = 1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[1] = 1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[2] = 1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[3] = 1.000000E+01;
    // DEFAULT         B_PRE_FUNC_MIN               -1.000000E+01  0.000000E+00  3.000000E-01 -1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[0] = -1.000000E+01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[1] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[2] = 3.000000E-01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[3] = -1.000000E+01;
    // DEFAULT         B_MINRMS                      0.000000E+00  1.000000E-01  1.000000E-01  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[0] = 0.000000E+00;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[1] = 1.000000E-01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[2] = 1.000000E-01;
    (refMgrParPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[3] = 0.000000E+00;
    // DEFAULT         PLATEAU_DURATION              2.000000E-02  5.000000E-02
    _Static_assert(2 <= (2), "List of values does not fit!");
    (refMgrParPointer(&ref_mgr,DEFAULT_PLATEAU_DURATION))[0] = 2.000000E-02;
    (refMgrParPointer(&ref_mgr,DEFAULT_PLATEAU_DURATION))[1] = 5.000000E-02;
    // DIRECT          V_REF                         0.000000E+00
    *(refMgrParPointer(&ref_mgr,DIRECT_V_REF          )) = 0.000000E+00;
    // DIRECT          I_REF                         0.000000E+00
    *(refMgrParPointer(&ref_mgr,DIRECT_I_REF          )) = 0.000000E+00;
    // DIRECT          B_REF                         0.000000E+00
    *(refMgrParPointer(&ref_mgr,DIRECT_B_REF          )) = 0.000000E+00;
    // DIRECT          DEGAUSS_AMP_PP                0.2
    *(refMgrParPointer(&ref_mgr,DIRECT_DEGAUSS_AMP_PP )) = 0.2;
    // DIRECT          DEGAUSS_PERIOD                0.1
    *(refMgrParPointer(&ref_mgr,DIRECT_DEGAUSS_PERIOD )) = 0.1;
    // GLOBAL          SUPER_CYCLE                   101  201  301  401  501  601  701  801  901  1001
    // (skipped write to ccpars_global.super_cycle)
    // GLOBAL          BASIC_PERIOD_MS               1000
    // (skipped write to &ccpars_global.basic_period_ms)
    // GLOBAL          EVENT_ADVANCE_MS              900
    // (skipped write to &ccpars_global.event_advance_ms)
    // GLOBAL          FLOAT_TOLERANCE               1.000000E-04
    // (skipped write to &ccpars_global.float_tolerance)
    // GLOBAL          INC_FLOAT_TOLERANCE           1.000000E-02
    // (skipped write to &ccpars_global.inc_float_tolerance)
    // ILC             START_TIME                    0.000000E+00
    *(refMgrParPointer(&ref_mgr,ILC_START_TIME    )) = 0.000000E+00;
    // ILC             STOP_REF                      0.000000E+00
    *(refMgrParPointer(&ref_mgr,ILC_STOP_REF      )) = 0.000000E+00;
    // ILC             L_ORDER                       0
    *(refMgrParPointer(&ref_mgr,ILC_L_ORDER       )) = 0;
    // IREG            PERIOD_ITERS                  10  10  10  10
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[0] = 10;
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[1] = 10;
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[2] = 10;
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[3] = 10;
    // IREG            EXTERNAL_ALG                  DISABLED  DISABLED  DISABLED  DISABLED
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[0] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[1] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[2] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[3] = CC_DISABLED;
    // IREG            INT_MEAS_SELECT               EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // IREG            INT_PURE_DELAY_PERIODS        0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[3] = 0.000000E+00;
    // IREG            INT_AUXPOLE1_HZ               1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[3] = 1.000000E+01;
    // IREG            INT_AUXPOLES2_HZ              1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[3] = 1.000000E+01;
    // IREG            INT_AUXPOLES2_Z               5.000000E-01  5.000000E-01  5.000000E-01  5.000000E-01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[0] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[1] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[2] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[3] = 5.000000E-01;
    // IREG            INT_AUXPOLE4_HZ               0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[3] = 0.000000E+00;
    // IREG            INT_AUXPOLE5_HZ               0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[3] = 0.000000E+00;
    // IREG            EXT_MEAS_SELECT               EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED  EXTRAPOLATED
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // IREG            EXT_TRACK_DELAY_PERIODS       0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[3] = 0.000000E+00;
    // IREG            EXT_OP_R                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[15] = 0.000000E+00;
    // IREG            EXT_OP_S                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[15] = 0.000000E+00;
    // IREG            EXT_OP_T                      0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[15] = 0.000000E+00;
    // LIMITS          B_POS                         1.200000E+01  1.200000E+01  1.200000E+01  1.200000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[0] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[1] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[2] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[3] = 1.200000E+01;
    // LIMITS          B_STANDBY                     0.000000E+00  1.000000E+00  1.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[3] = 0.000000E+00;
    // LIMITS          B_NEG                        -1.200000E+01  0.000000E+00  0.000000E+00 -1.200000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[0] = -1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[3] = -1.200000E+01;
    // LIMITS          B_RATE                        1.200000E+01  1.200000E+01  1.200000E+01  1.200000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[0] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[1] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[2] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[3] = 1.200000E+01;
    // LIMITS          B_CLOSELOOP                   0.000000E+00  5.000000E-01  5.000000E-01  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[1] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[2] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[3] = 0.000000E+00;
    // LIMITS          B_LOW                         0.000000E+00  1.000000E-01  1.000000E-01  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[1] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[2] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[3] = 0.000000E+00;
    // LIMITS          B_ZERO                        0.000000E+00  1.000000E-02  1.000000E-02  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[1] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[2] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[3] = 0.000000E+00;
    // LIMITS          B_ERR_WARNING                 0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[3] = 0.000000E+00;
    // LIMITS          B_ERR_FAULT                   0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[3] = 0.000000E+00;
    // LIMITS          I_POS                         1.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[0] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[1] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[2] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[3] = 1.000000E+01;
    // LIMITS          I_STANDBY                     0.000000E+00  1.000000E+00  1.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[3] = 0.000000E+00;
    // LIMITS          I_NEG                        -1.000000E+01  0.000000E+00  0.000000E+00 -1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[0] = -1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[3] = -1.000000E+01;
    // LIMITS          I_RATE                        2.000000E+01  1.000000E+01  1.000000E+01  1.000000E+01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[0] = 2.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[1] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[2] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[3] = 1.000000E+01;
    // LIMITS          I_CLOSELOOP                   0.000000E+00  5.000000E-01  5.000000E-01  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[1] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[2] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[3] = 0.000000E+00;
    // LIMITS          I_LOW                         0.000000E+00  1.000000E-01  1.000000E-01  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[1] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[2] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[3] = 0.000000E+00;
    // LIMITS          I_ZERO                        0.000000E+00  1.000000E-02  1.000000E-02  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[1] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[2] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[3] = 0.000000E+00;
    // LIMITS          I_ERR_WARNING                 0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[3] = 0.000000E+00;
    // LIMITS          I_ERR_FAULT                   0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[3] = 0.000000E+00;
    // LIMITS          I_QUADRANTS41                 0.000000E+00  0.000000E+00
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_QUADRANTS41                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_QUADRANTS41                       ))[1] = 0.000000E+00;
    // LIMITS          I_RMS_TC                      0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_TC                            )) = 0.000000E+00;
    // LIMITS          I_RMS_WARNING                 0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_WARNING                       )) = 0.000000E+00;
    // LIMITS          I_RMS_FAULT                   0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_FAULT                         )) = 0.000000E+00;
    // LIMITS          I_RMS_LOAD_TC                 0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[3] = 0.000000E+00;
    // LIMITS          I_RMS_LOAD_WARNING            0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[3] = 0.000000E+00;
    // LIMITS          I_RMS_LOAD_FAULT              0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[3] = 0.000000E+00;
    // LIMITS          V_POS                         1.000000E+02  1.000000E+02  1.000000E+02  1.000000E+02
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[0] = 1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[1] = 1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[2] = 1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[3] = 1.000000E+02;
    // LIMITS          V_NEG                        -1.000000E+02 -1.000000E+02  0.000000E+00 -1.000000E+02
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[0] = -1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[1] = -1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[3] = -1.000000E+02;
    // LIMITS          V_RATE                        1.000000E+03
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_RATE                              )) = 1.000000E+03;
    // LIMITS          V_RATE_RMS_TC                 0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_RATE_RMS_TC                       )) = 0.000000E+00;
    // LIMITS          V_RATE_RMS_FAULT              0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_RATE_RMS_FAULT                    )) = 0.000000E+00;
    // LIMITS          V_ERR_WARNING                 0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_ERR_WARNING                       )) = 0.000000E+00;
    // LIMITS          V_ERR_FAULT                   0.000000E+00
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_ERR_FAULT                         )) = 0.000000E+00;
    // LIMITS          V_QUADRANTS41                 0.000000E+00  0.000000E+00
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_V_QUADRANTS41                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_QUADRANTS41                       ))[1] = 0.000000E+00;
    // LIMITS          P_POS                         0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[3] = 0.000000E+00;
    // LIMITS          P_NEG                         0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[3] = 0.000000E+00;
    // LOAD            SELECT                        0
    *(regMgrParAppPointer(&reg_pars, LOAD_SELECT        )) = 0;
    // LOAD            OHMS_SER                      5.000000E-01  5.000000E-01  5.000000E-01  5.000000E-01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[0] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[1] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[2] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[3] = 5.000000E-01;
    // LOAD            OHMS_PAR                      1.000000E+09  1.000000E+09  1.000000E+09  1.000000E+02
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[0] = 1.000000E+09;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[1] = 1.000000E+09;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[2] = 1.000000E+09;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[3] = 1.000000E+02;
    // LOAD            OHMS_MAG                      6.000000E-01  6.000000E-01  6.000000E-01  6.000000E-01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[0] = 6.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[1] = 6.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[2] = 6.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[3] = 6.000000E-01;
    // LOAD            HENRYS                        1.000000E+00  1.000000E+00  1.000000E+00  1.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[3] = 1.000000E+00;
    // LOAD            HENRYS_SAT                    1.000000E+00  1.000000E+00  1.000000E+00  1.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[3] = 1.000000E+00;
    // LOAD            I_SAT_GAIN                    1.000000E+00  1.000000E+00  1.000000E+00  1.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[3] = 1.000000E+00;
    // LOAD            I_SAT_START                   0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[3] = 0.000000E+00;
    // LOAD            I_SAT_END                     0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[3] = 0.000000E+00;
    // LOAD            SAT_SMOOTHING                 3.000000E-01  3.000000E-01  3.000000E-01  3.000000E-01
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[0] = 3.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[1] = 3.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[2] = 3.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[3] = 3.000000E-01;
    // LOAD            GAUSS_PER_AMP                 1.200000E+00  1.200000E+00  1.200000E+00  1.200000E+00
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[0] = 1.200000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[1] = 1.200000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[2] = 1.200000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[3] = 1.200000E+00;
    // MEAS            DCCT_SELECT                   AB
    *(sigVarPointer(&sig_struct, select, i_meas, SELECT_SELECTOR               )) = SIG_AB;
    // MEAS            B_DELAY_ITERS                 1.300000E+00
    *(regMgrParAppPointer(&reg_pars, MEAS_B_DELAY_ITERS                         )) = 1.300000E+00;
    // MEAS            I_DELAY_ITERS                 1.300000E+00
    *(regMgrParAppPointer(&reg_pars, MEAS_I_DELAY_ITERS                         )) = 1.300000E+00;
    // MEAS            V_DELAY_ITERS                 1.300000E+00
    *(regMgrParAppPointer(&reg_pars, MEAS_V_DELAY_ITERS                         )) = 1.300000E+00;
    // MEAS            B_FIR_LENGTHS                 0  0
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, MEAS_B_FIR_LENGTHS                         ))[0] = 0;
    (regMgrParAppValue(  &reg_pars, MEAS_B_FIR_LENGTHS                         ))[1] = 0;
    // MEAS            I_FIR_LENGTHS                 0  0
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, MEAS_I_FIR_LENGTHS                         ))[0] = 0;
    (regMgrParAppValue(  &reg_pars, MEAS_I_FIR_LENGTHS                         ))[1] = 0;
    // MODE            REF_SUB_SEL                   ENABLED
    *(refMgrParPointer(&ref_mgr,MODE_REF_SUB_SEL     )) = CC_ENABLED;
    // MODE            REF_CYC_SEL                   ENABLED
    *(refMgrParPointer(&ref_mgr,MODE_REF_CYC_SEL     )) = CC_ENABLED;
    // MODE            REG_MODE_CYC                  CURRENT
    *(refMgrParPointer(&ref_mgr,MODE_REG_MODE_CYC    )) = REG_CURRENT;
    // MODE            PRE_FUNC                      UPMINMAX
    *(refMgrParPointer(&ref_mgr,MODE_PRE_FUNC        )) = REF_PRE_FUNC_UPMINMAX;
    // MODE            POST_FUNC                     MINRMS
    *(refMgrParPointer(&ref_mgr,MODE_POST_FUNC       )) = REF_POST_FUNC_MINRMS;
    // MODE            SIM_MEAS                      ENABLED
    *(refMgrParPointer(&ref_mgr,MODE_SIM_MEAS        )) = CC_ENABLED;
    // MODE            RT_REF                        DISABLED
    *(refMgrParPointer(&ref_mgr,MODE_RT_REF          )) = CC_DISABLED;
    // MODE            RT_NUM_STEPS                  0
    *(refMgrParPointer(&ref_mgr,RT_NUM_STEPS         )) = 0;
    // MODE            ECONOMY                       DISABLED
    *(refMgrParPointer(&ref_mgr,MODE_ECONOMY         )) = CC_DISABLED;
    // MODE            USE_ARM_NOW                   DISABLED
    *(refMgrParPointer(&ref_mgr,MODE_USE_ARM_NOW     )) = CC_DISABLED;
    // MODE            TO_OFF_FAULTS                 0x0
    *(refMgrParPointer(&ref_mgr,MODE_TO_OFF_FAULTS   )) = 0x0;
    // MODE            ILC                           DISABLED
    *(refMgrParPointer(&ref_mgr,MODE_ILC             )) = CC_DISABLED;
    // VS              ACTUATION                     VOLTAGE_REF
    *(regMgrParAppPointer(&reg_pars, VS_ACTUATION      )) = REG_VOLTAGE_REF;
    // VS              ACT_DELAY_ITERS               1.000000E+00
    *(regMgrParAppPointer(&reg_pars, VS_ACT_DELAY_ITERS)) = 1.000000E+00;
    // VS              NUM_VSOURCES                  1
    *(regMgrParAppPointer(&reg_pars, VS_NUM_VSOURCES   )) = 1;
    // VS              GAIN                          1.000000E+01
    *(regMgrParAppPointer(&reg_pars, VS_GAIN           )) = 1.000000E+01;
    // VS              OFFSET                        0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VS_OFFSET         )) = 0.000000E+00;
    // VS              DAC_CLAMP                     0.000000E+00
    *(refMgrParPointer(   &ref_mgr,  DAC_CLAMP         )) = 0.000000E+00;
    // VS              FIRING_MODE                   CASSEL
    *(regMgrParAppPointer(&reg_pars, VS_FIRING_MODE    )) = REG_CASSEL;
    // VS              FIRING_DELAY                  1.000000E-03
    *(regMgrParAppPointer(&reg_pars, VS_FIRING_DELAY   )) = 1.000000E-03;
    // VS              FIRING_LUT                    0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(101 <= (REG_FIRING_LUT_LEN), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[3] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[4] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[5] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[6] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[7] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[8] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[9] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[10] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[11] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[12] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[13] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[14] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[15] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[16] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[17] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[18] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[19] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[20] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[21] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[22] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[23] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[24] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[25] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[26] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[27] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[28] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[29] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[30] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[31] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[32] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[33] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[34] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[35] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[36] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[37] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[38] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[39] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[40] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[41] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[42] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[43] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[44] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[45] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[46] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[47] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[48] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[49] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[50] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[51] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[52] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[53] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[54] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[55] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[56] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[57] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[58] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[59] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[60] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[61] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[62] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[63] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[64] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[65] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[66] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[67] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[68] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[69] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[70] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[71] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[72] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[73] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[74] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[75] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[76] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[77] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[78] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[79] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[80] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[81] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[82] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[83] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[84] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[85] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[86] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[87] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[88] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[89] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[90] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[91] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[92] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[93] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[94] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[95] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[96] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[97] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[98] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[99] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ))[100] = 0.000000E+00;
    // VS              SIM_BANDWIDTH                 2.000000E+02
    *(regMgrParAppPointer(&reg_pars, VS_SIM_BANDWIDTH  )) = 2.000000E+02;
    // VS              SIM_Z                         9.000000E-01
    *(regMgrParAppPointer(&reg_pars, VS_SIM_Z          )) = 9.000000E-01;
    // VS              SIM_TAU_ZERO                  0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VS_SIM_TAU_ZERO   )) = 0.000000E+00;
    // VS              SIM_NUM                       1.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_SIM_NUM_DEN_LEN), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, VS_SIM_NUM        ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_SIM_NUM        ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_SIM_NUM        ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_SIM_NUM        ))[3] = 0.000000E+00;
    // VS              SIM_DEN                       1.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_SIM_NUM_DEN_LEN), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, VS_SIM_DEN        ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_SIM_DEN        ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_SIM_DEN        ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, VS_SIM_DEN        ))[3] = 0.000000E+00;
    // POLSWITCH       TIMEOUT                       0
    // (skipped write to polswitchParPointer(&polswitch_mgr,TIMEOUT      ))
    // POLSWITCH       AUTOMATIC                     DISABLED
    // (skipped write to polswitchParPointer(&polswitch_mgr,AUTOMATIC    ))
    // POLSWITCH       MOVEMENT_TIME                 0.000000E+00
    // (skipped write to polswitchParPointer(&polswitch_mgr,MOVEMENT_TIME))
    // RTREF           SAMPLING_ITERS                20
    // (skipped write to &ccpars_rtref.sampling_iters)
    // RTREF           OFFSET                        0.000000E+00
    // (skipped write to &ccpars_rtref.offset)
    // RTREF           AMPLITUDE                     0.000000E+00
    // (skipped write to &ccpars_rtref.amplitude)
    // RTREF           FREQUENCY                     1.000000E+00
    // (skipped write to &ccpars_rtref.frequency)
    // SIM             ADC_SIGNAL                    I_DCCT_A  I_DCCT_B  V_MEAS  B_PROBE_A
    // (skipped write to ccpars_sim.adc_signal)
    // SIM             VS_QUANTIZATION               0.000000E+00
    // (skipped write to &ccpars_sim.vs_quantization)
    // SIM             VS_TONE_PP                    0.000000E+00
    // (skipped write to &ccpars_sim.tone_pp[TONE_VS])
    // SIM             MEAS_TONE_PERIOD_ITERS        2.000000E+01
    // (skipped write to &ccpars_sim.meas_tone_period_iters)
    // SIM             DCCT_A_TONE_PP                0.000000E+00
    // (skipped write to &ccpars_sim.tone_pp[TONE_DCCT_A])
    // SIM             DCCT_B_TONE_PP                0.000000E+00
    // (skipped write to &ccpars_sim.tone_pp[TONE_DCCT_B])
    // SIM             B_PROBE_TONE_PP               0.000000E+00
    // (skipped write to &ccpars_sim.tone_pp[TONE_B_PROBE])
    // SIM             I_PROBE_TONE_PP               0.000000E+00
    // (skipped write to &ccpars_sim.tone_pp[TONE_I_PROBE])
    // SIM             NOISE_VS                      0.000000E+00
    // (skipped write to &ccpars_sim.noise_vs)
    // SIM             NOISE_DCCT_A                  0.000000E+00
    // (skipped write to &ccpars_sim.noise_dcct_a)
    // SIM             NOISE_DCCT_B                  0.000000E+00
    // (skipped write to &ccpars_sim.noise_dcct_b)
    // SIM             NOISE_V_PROBE                 0.000000E+00
    // (skipped write to &ccpars_sim.noise_v_probe)
    // SIM             NOISE_B_PROBE                 0.000000E+00
    // (skipped write to &ccpars_sim.noise_b_probe)
    // SIM             NOISE_I_PROBE                 0.000000E+00
    // (skipped write to &ccpars_sim.noise_i_probe)
    // SIM             NOISE_ADCS                    0.000000E+00
    // (skipped write to &ccpars_sim.noise_adcs)
    // VFEEDFWD        GAIN                          0.000000E+00
    *(refMgrParPointer(&ref_mgr, VFEEDFWD_GAIN)) = 0.000000E+00;
    // VFEEDFWD        DELAY_PERIODS                 0.000000E+00
    *(refMgrParPointer(&ref_mgr, VFEEDFWD_DELAY_PERIODS)) = 0.000000E+00;
    // VFEEDFWD        NUM                           0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_SIM_NUM_DEN_LEN), "List of values does not fit!");
    (regMgrParAppValue(&reg_pars, VFEEDFWD_NUM))[0] = 0.000000E+00;
    (regMgrParAppValue(&reg_pars, VFEEDFWD_NUM))[1] = 0.000000E+00;
    (regMgrParAppValue(&reg_pars, VFEEDFWD_NUM))[2] = 0.000000E+00;
    (regMgrParAppValue(&reg_pars, VFEEDFWD_NUM))[3] = 0.000000E+00;
    // VFEEDFWD        DEN                           0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00
    _Static_assert(4 <= (REG_SIM_NUM_DEN_LEN), "List of values does not fit!");
    (regMgrParAppValue(&reg_pars, VFEEDFWD_DEN))[0] = 0.000000E+00;
    (regMgrParAppValue(&reg_pars, VFEEDFWD_DEN))[1] = 0.000000E+00;
    (regMgrParAppValue(&reg_pars, VFEEDFWD_DEN))[2] = 0.000000E+00;
    (regMgrParAppValue(&reg_pars, VFEEDFWD_DEN))[3] = 0.000000E+00;
    // VFILTER         V_MEAS_SOURCE                 MEASUREMENT
    *(regMgrParAppPointer(&reg_pars, VFILTER_V_MEAS_SOURCE)) = REG_MEASUREMENT;
    // VFILTER         I_CAPA_SOURCE                 MEASUREMENT
    *(regMgrParAppPointer(&reg_pars, VFILTER_I_CAPA_SOURCE)) = REG_MEASUREMENT;
    // VFILTER         I_CAPA_FILTER                 1.000000E+00  0.000000E+00
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue  (&reg_pars, VFILTER_I_CAPA_FILTER))[0] = 1.000000E+00;
    (regMgrParAppValue  (&reg_pars, VFILTER_I_CAPA_FILTER))[1] = 0.000000E+00;
    // VFILTER         K_U                           0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_U      )) = 0.000000E+00;
    // VFILTER         K_I                           0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_I      )) = 0.000000E+00;
    // VFILTER         K_D                           0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_D      )) = 0.000000E+00;
    // VREG            K_INT                         0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_K_INT)) = 0.000000E+00;
    // VREG            K_P                           0.000000E+00
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_K_P  )) = 0.000000E+00;
    // VREG            K_FF                          1.000000E+00
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_K_FF )) = 1.000000E+00;
    // LOG  DURATION               7000
    *(&ccpars_log.read_timing.duration_ms) = 7000;
    // LOGMENU I_REG               ENABLED
    *(&ccpars_logmenu[LOG_MENU_I_REG               ]) = CC_ENABLED;
    // MODE REG_MODE               CURRENT
    *(refMgrParPointer(&ref_mgr,MODE_REG_MODE        )) = REG_CURRENT;
    // MODE REF                    DIRECT
    *(refMgrParPointer(&ref_mgr,MODE_REF             )) = REF_DIRECT;
    // MODE PC            ON
    *(&ccpars_mode.pc) = REF_PC_ON;

    // return new state
    return (StateFunc_t) &WAIT_5_0_None_None_None_None;
}

static StateFunc_t from_WAIT_5_0_None_None_None_None(void)
{
    // DIRECT I_REF          5
    *(refMgrParPointer(&ref_mgr,DIRECT_I_REF          )) = 5;

    // return new state
    return (StateFunc_t) &WAIT_4_0_None_None_None_None;
}

static StateFunc_t from_WAIT_4_0_None_None_None_None(void)
{
    // DIRECT I_REF          0
    *(refMgrParPointer(&ref_mgr,DIRECT_I_REF          )) = 0;

    // return new state
    return (StateFunc_t) &WAIT_0_8_None_None_None_None;
}

static StateFunc_t from_WAIT_0_8_None_None_None_None(void)
{
    // MODE REF STANDBY
    *(refMgrParPointer(&ref_mgr,MODE_REF             )) = REF_STANDBY;

    // return new state
    return (StateFunc_t) &WAIT_1_0_None_None_None_None;
}

static StateFunc_t from_WAIT_1_0_None_None_None_None(void)
{
    // LOG TIME_ORIGIN    T
    // (skipped write to &ccpars_log.time_origin)
    // WRITELOGS
    // TODO: WRITELOGS

    // end of script
    return NULL;
}

// WAIT 5
static StateFunc_t WAIT_5_0_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(5.0))
    {
        // call transition function and propagate returned new state
        return from_WAIT_5_0_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_5_0_None_None_None_None;
    }
}

// WAIT 4
static StateFunc_t WAIT_4_0_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(4.0))
    {
        // call transition function and propagate returned new state
        return from_WAIT_4_0_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_4_0_None_None_None_None;
    }
}

// WAIT 0.8
static StateFunc_t WAIT_0_8_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(0.8))
    {
        // call transition function and propagate returned new state
        return from_WAIT_0_8_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_0_8_None_None_None_None;
    }
}

// WAIT 1
static StateFunc_t WAIT_1_0_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(1.0))
    {
        // call transition function and propagate returned new state
        return from_WAIT_1_0_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_1_0_None_None_None_None;
    }
}

const struct CCRT2C_script script_direct = {
    .iter_period_us         = 100,
    .event_log_period_iters = 50,

    .init                   = &INIT,
};
