//! @file
//! @brief  FGC4 shared memory routines
//! @author Martin Cejp

#pragma once

#include <filesystem>
#include <string>

#include "Util_Mmap.h"

namespace fgcd
{
    class Fgc4Shmem
    {
    public:
        Fgc4Shmem();

        void initIpcDataStructures();
//        static std::string getStatusAsString();

    private:
        File dev_mem;
        Mmap base_0x7A000000;
    };
}
