//! @file
//! @brief  Playground for development / debug.
//! @author Dariusz Zielinski

#pragma once

#include <cpp_utils/enumStr.h>

#include <modules/base/thread/inc/Thread.h>
#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/component/inc/Component.h>

namespace fgcd
{
    class Fgc4Mainloop : public Component
    {
        public:
            explicit Fgc4Mainloop(Component& parent);

            void onStart() override;

            void onStop() override;

        private:
            void threadFunction(std::atomic_bool& keep_running);

            Thread m_thread;

            struct : StatsGroup
            {
                using StatsGroup::StatsGroup;

                StatsField all{*this, "all"};
                StatsField allBad{*this, "all bad"};

                struct : StatsGroup
                {
                    using StatsGroup::StatsGroup;

                    StatsField rx{*this, "rx"};
                    StatsField tx{*this, "tx"};
                } good{*this, "good"};

                struct : StatsGroup
                {
                    using StatsGroup::StatsGroup;

                    StatsField rx{*this, "rx"};
                    StatsField tx{*this, "tx"};

                    struct : StatsGroup
                    {
                        using StatsGroup::StatsGroup;

                        StatsField failed{*this, "failed"};
                        StatsField rejected{*this, "rejected"};
                    } reasons{*this, "reasons"};

                } bad{*this, "bad"};

            } m_stats{m_stats_root, "ethernet"};
    };
}

// EOF