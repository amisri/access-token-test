#pragma once

#include "reg_loop_shmem.h"

struct Equipdev_channel_ppm
{
    REF_ctrl_pars ctrl;
};

struct Equipdev_channel
{
    Equipdev_channel_ppm ppm[CC_NUM_CYC_SELS];
};

struct Equipdev
{
    Equipdev_channel device[CC_NUM_SUB_SELS];
};

extern Equipdev equipdev;
