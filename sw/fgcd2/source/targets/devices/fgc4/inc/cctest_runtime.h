#pragma once

#include "liblog.h"
#include "libref.h"

#include "logMenus.h"
#include "logStructs.h"

#include "reg_loop_shmem.h"

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

//! MODE parameters structure

struct CC_pars_mode
{
    enum REF_pc_state           pc;                  //!< MODE PC - desired PC STATE
};

struct CC_sim_vars
{
    struct CC_sim_pc_state
    {
        cc_float                timer;                          //!< Converter on/off timer (s)
    } pcstate;
};

struct CC_pars_log
{
    struct CC_ns_time        time_origin;         //!< Log time origin (zero for most recent data)
    uint32_t                 cyc_sel;             //!< Cycle selector to log automatically
    uint32_t                 sub_sampling_period; //!< Sub-sampling period in units of the log period (0 = automatic, 1-N = required sub-sampling period)
    struct LOG_read_timing   read_timing;         //!< Read request timing
//    enum CC_enabled_disabled prev_cycle;          //!< Enabled:Log previous cycle.  Disabled:use LOG CYC_SEL to set cycle to be logged.
//    enum CC_enabled_disabled overwrite;           //!< Enabled:each log is to a separate folder. Disabled:all logs written to top level log folder.
    enum CC_enabled_disabled csv_spy;             //!< Enables/disables creation of CSV Spy file on each writelog
    enum CC_enabled_disabled binary_spy;          //!< Enables/disables creation of binary Spy file on each writelog
//    enum CC_enabled_disabled pm_buf;              //!< Enables/disables PM_BUF generation when converter goes OFF
//    enum CC_enabled_disabled internal_tests;      //!< Enables/disables internal tests of log read control and header or PM_BUF
};


typedef void (*OpaqueFunc_t)(void);
// must return a new StateFunc pointer or NULL
typedef OpaqueFunc_t (*StateFunc_t)(void);

struct CCRT2C_script
{
    int iter_period_us;
    int event_log_period_iters;

    StateFunc_t (*init)(void);
};

//extern struct LOG_buffers log_buffers;
//extern struct REF_mgr ref_mgr;
//extern struct REG_mgr reg_mgr;          // referred to by logs
extern struct REG_pars reg_pars;
//extern struct SIG_struct sig_struct;

extern struct CC_pars_log ccpars_log;
extern enum CC_enabled_disabled ccpars_logmenu[LOG_NUM_MENUS];
extern struct CC_pars_mode ccpars_mode;
extern struct CC_sim_vars ccsim;

extern struct REF_ref_pars ccpars_ref[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS];

bool cctestTimeInStateElapsed(double time_sec);
uint32_t ccCmdsRun(void);
void ccRtArmRef(uint32_t sub_sel, uint32_t cyc_sel, uint32_t const num_pars, cc_float const * const par_values);

// defined in generated code
[[maybe_unused]] extern const struct CCRT2C_script script_direct;
[[maybe_unused]] extern const struct CCRT2C_script script_idle;

#ifdef __cplusplus
}
#endif
