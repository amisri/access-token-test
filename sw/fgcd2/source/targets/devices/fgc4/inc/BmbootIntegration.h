//! @file
//! @brief  Bumbleboot integration for FGC4
//! @author Martin Cejp

#pragma once

#include <filesystem>
#include <string>

#include "bmboot/domain.hpp"

namespace fgcd
{
    class BmbootIntegration
    {
    public:
        static std::unique_ptr<bmboot::IDomain> openDomain(bmboot::DomainIndex which);

        static void bootRegloopFromFile(bmboot::IDomain& domain, std::filesystem::path const& binary_path);
        static std::string getStatusAsString(bmboot::IDomain& domain);
    };
}
