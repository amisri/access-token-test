//! @file
//! @brief  Playground for development / debug.
//! @author Dariusz Zielinski

#pragma once

#include <cpp_utils/enumStr.h>

#include <modules/base/thread/inc/Thread.h>
#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/component/inc/Component.h>
#include <modules/net/websocket/inc/Websocket.h>

namespace fgcd
{
    class SimpleWsCommandServer : public Component
    {
        public:
            explicit SimpleWsCommandServer(Component& parent);

            void onStart() override;

            void onStop() override;

        private:
            void threadFunction(std::atomic_bool& keep_running);

            Thread m_thread;
            std::shared_ptr<Websocket> m_ws;
    };
}

// EOF