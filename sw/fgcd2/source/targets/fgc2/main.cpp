#include <filesystem>
#include <memory>
#include <string_view>

#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/core/inc/Application.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/entryPoint.h>
#include <modules/devices/base/inc/NameFileReader.h>
#include <modules/devices/base/inc/PlatformManagerInventory.h>
#include <modules/devices/fgc2/inc/PlatformManager.h>
#include <modules/net/websocket/inc/FecServer.h>
#include <modules/targets/fgcd2/inc/Fgcd2.h>

using namespace fgcd;

// **********************************************************

int main(int argc, char *argv[])
{
    entryPoint(argc, argv);
}

// **********************************************************

RootComponentPtr Application::createRootComponent(int argc, char* argv[])   // NOLINT(*)
{
    if (argc != 4)
    {
        throw Exception("Missing arguments");
    }

    std::string_view hostname       = argv[1];
    std::string_view class_id       = argv[2];
    std::string_view timing_name    = argv[3];


    // **********************************************************
    // Create components
    // **********************************************************
    // clang-format off
    auto root                  = std::make_shared<Fgcd2>();
    auto platform_inventory    = root->addComponent<PlatformManagerInventory>();
    auto device_inventory      = root->addComponent<DeviceInventory>();
    auto reprogramming_manager = root->addComponent<ReprogrammingManager>(*device_inventory);
    auto server                = root->addComponent<FecServer>(device_inventory);

    auto namefile_reader       = platform_inventory->addComponent<NameFileReader>(*platform_inventory, *reprogramming_manager, *device_inventory);
    auto platform              = platform_inventory->addPlatformManager<fgc2::PlatformManager>(root->getTiming(), reprogramming_manager);
    // clang-format on

    // **********************************************************
    // Load event file
    // **********************************************************
    std::filesystem::path config_path{"/user/pclhc/etc/fgcd"};
    std::filesystem::path event_file = config_path / "timing/timdt/class" / class_id / timing_name;
    root->loadEventFile(event_file, hostname);

    return root;
}
