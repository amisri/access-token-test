cmake_minimum_required(VERSION 3.17)
project(FIP_PROJ)

include("${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/fgcd.cmake")

set(FGCD_GCC_X86_64  "/usr")
set(CMAKE_CXX_STANDARD 20)

fgcdDependOnModules(targets devices/base devices/fgc2 net/websocket)
fgcdLoadProfile(x86_64)
fgcdProfileAddFlag("-g")

fgcdDeclareTarget("fgc2")
# EOF
