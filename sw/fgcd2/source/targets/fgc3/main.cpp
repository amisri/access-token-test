#include <filesystem>
#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <utility>

#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/core/inc/Application.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/entryPoint.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/NameFileReader.h>
#include <modules/devices/base/inc/PlatformManagerInventory.h>
#include <modules/devices/fgc3/inc/Fgc3PlatformManager.h>
#include <modules/net/websocket/inc/Websocket.h>
#include <modules/net/websocket/inc/WefServer.h>
#include <modules/targets/fgcd2/inc/Fgcd2.h>

using namespace fgcd;

// **********************************************************

int main(int argc, char* argv[])
{
    entryPoint(argc, argv);
}

// **********************************************************

RootComponentPtr Application::createRootComponent(int argc, char* argv[])   // NOLINT(*)
{
    if (argc != 3)
    {
        throw Exception("Missing arguments");
    }
    std::string_view interface_name = argv[1];
    std::string_view hostname       = argv[2];

    auto root = std::make_shared<Fgcd2>();

    auto platform_inv    = root->addComponent<PlatformManagerInventory>();
    auto device_inv      = root->addComponent<DeviceInventory>();
    auto reprogrammer    = root->addComponent<ReprogrammingManager>(*device_inv);
    auto distributor     = root->addComponent<CommandDistributor>(device_inv);
    auto websocket       = root->addComponent<Websocket>(5566);
    auto server          = root->addComponent<WefServer>(websocket, distributor);
    auto namefile_reader = root->addComponent<NameFileReader>(*platform_inv, *reprogrammer, *device_inv);

    // **********************************************************
    // Load event file
    // **********************************************************
    std::filesystem::path config_path{"/user/pclhc/etc/fgcd"};
    std::filesystem::path event_file = config_path / "timing/timdt/class" / std::to_string(4) / "PSB";
    root->loadEventFile(event_file, hostname);

    // **********************************************************
    // Crate platform
    // **********************************************************

    auto platform = platform_inv->addPlatformManager<Fgc3PlatformManager>(interface_name, *reprogrammer);

    return root;
}
