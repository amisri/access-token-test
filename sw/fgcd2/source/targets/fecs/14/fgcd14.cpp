//! @file
//! @brief  Root component for the generic FEC class at CERN.
//! @author Dariusz Zielinski

#include <modules/base/core/inc/Application.h>
#include <modules/base/debugging/inc/Playground.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/targets/inc/CernBaseTarget.h>

using namespace fgcd;

// **********************************************************

RootComponentPtr Application::createRootComponent(int argc, char *argv[])
{
    auto root = std::make_shared<CernBaseTarget>();

    root->addComponent<Playground>();

    return root;
}

