#include <memory>

#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/core/inc/Application.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/entryPoint.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/CommandDistributor.h>
#include <modules/devices/base/inc/NameFileReader.h>
#include <modules/devices/base/inc/PlatformManagerInventory.h>
#include <modules/devices/fgc4/inc/PlatformManager.h>
#include <modules/net/websocket/inc/Websocket.h>
#include <modules/net/websocket/inc/WefServer.h>
#include <modules/targets/fgcd2/inc/Fgcd2.h>

using namespace fgcd;

// **********************************************************

int main(int argc, char* argv[])
{
    entryPoint(argc, argv);
}

// **********************************************************

RootComponentPtr Application::createRootComponent(int argc, char* argv[])
{
    auto root = std::make_shared<Fgcd2>();

    auto platform_inv    = root->addComponent<PlatformManagerInventory>();
    auto device_inv      = root->addComponent<DeviceInventory>();
    auto reprogrammer    = root->addComponent<ReprogrammingManager>(*device_inv);
    auto distributor     = root->addComponent<CommandDistributor>(device_inv);
    auto websocket       = root->addComponent<Websocket>(5566);
    auto server          = root->addComponent<WefServer>(websocket, distributor);
    auto namefile_reader = root->addComponent<NameFileReader>(*platform_inv, *reprogrammer, *device_inv);

    // **********************************************************
    // Create platform
    // **********************************************************

    auto platform = platform_inv->addPlatformManager<fgc4::PlatformManager>();

    return root;
}
