//! @file
//! @brief  Interface of a class taking LogEntry and doing something with it (e.g. printing to a file).
//! @author Dariusz Zielinski

#pragma once

#include <memory>
#include <string>

#include <modules/base/logging/inc/LogEntry.h>

namespace fgcd
{
    struct ILogConsumer
    {
        virtual ~ILogConsumer() = default;

        virtual void consumeLog(LogEntryPtr log_entry, std::string_view message) = 0;
    };

    using ILogConsumerPtr = std::shared_ptr<ILogConsumer>;
}


// EOF