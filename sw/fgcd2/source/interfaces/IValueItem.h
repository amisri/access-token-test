
//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <fmt/format.h>
#include <map>
#include <memory>
#include <string>
#include <variant>
#include <vector>

#include <modules/devices/base/inc/Point.h>

namespace fgcd
{
    using ValueType = std::variant<
        std::monostate,                              //
        std::string, std::vector<std::string>,       //
        Point, std::vector<Point>,                   //
        float, std::vector<float>,                   //
        std::int32_t, std::vector<std::int32_t>,     //
        std::uint32_t, std::vector<std::uint32_t>,   //
        std::int16_t, std::vector<std::int16_t>,     //
        std::uint16_t, std::vector<std::uint16_t>,   //
        std::int8_t, std::vector<std::int8_t>,       //
        std::uint8_t, std::vector<std::uint8_t>      //
        >;

    struct IValueItem;
    using IValueItemPtr  = std::shared_ptr<IValueItem>;
    using IValueVector   = std::vector<IValueItemPtr>;
    using IValueChildren = std::map<std::string, IValueItemPtr, std::less<>>;
    //!                                                             ^
    //!                                         To lookup keys using std::string_view

    struct IValueItem
    {
        virtual ~IValueItem()                        = default;
        IValueItem& operator=(IValueItem&&) noexcept = delete;

        [[nodiscard]] virtual ValueType   get() const                                      = 0;
        virtual void                      add(std::string_view label, IValueItemPtr child) = 0;
        virtual const IValueChildren&     getChildren()                                    = 0;
        [[nodiscard]] virtual bool        isParent() const                                 = 0;
        [[nodiscard]] virtual std::string toString() const                                 = 0;
    };
}

// **********************************************************

template<>
struct fmt::formatter<fgcd::IValueVector>
{
    template<typename FormatContext>
    auto format(const fgcd::IValueVector& values, FormatContext& ctx)
    {
        std::string text;
        for(auto&& value : values)
        {
            text += value->toString();
        }

        return fmt::format_to(ctx.out(), "{}", text);
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// EOF
