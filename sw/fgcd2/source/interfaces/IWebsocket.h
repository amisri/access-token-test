//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <functional>
#include <memory>
#include <set>

#include <cpp_utils/misc.h>
#include <modules/base/core/inc/types.h>

namespace fgcd
{
    using ConnectionHandle = std::weak_ptr<void>;

    struct IWebsocket;
    using IWebsocketPtr = std::shared_ptr<IWebsocket>;

    struct IWebsocket : utils::NoncopyableNonmovable
    {
        using MessageHandler  = std::function<void(ConnectionHandle, const Json&)>;
        using StateHandler    = std::function<void(ConnectionHandle)>;
        using ValidateHandler = std::function<bool(ConnectionHandle)>;
        using ClientSet       = std::set<ConnectionHandle, std::owner_less<ConnectionHandle>>;

        virtual ~IWebsocket() = default;

        virtual void sendMessage(ConnectionHandle handle, const Json& payload) = 0;
        virtual void setMessageHandler(MessageHandler handler) = 0;
        virtual void setOpenHandler(const StateHandler& handler) = 0;
        virtual void setCloseHandler(const StateHandler& handler) = 0;
        virtual void setValidateHandler(const ValidateHandler& handler) = 0;
        virtual void broadcastMessage(const Json& data) = 0;
    };
}

// EOF
