//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <cpp_utils/misc.h>
#include <modules/devices/base/inc/Command.h>

namespace fgcd
{
    struct ICommandDistributor : utils::NoncopyableNonmovable
    {
        virtual ~ICommandDistributor() = default;

        virtual bool pushCommand(CommandPtr command) = 0;
    };

    using ICommandDistributorPtr = std::shared_ptr<ICommandDistributor>;
}

// EOF
