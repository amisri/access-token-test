
//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <modules/devices/base/inc/Device.h>

namespace fgcd
{
    struct IPlatformManager
    {
        virtual ~IPlatformManager()                                                        = default;
        virtual DevicePtr createDevice(DeviceName name, int class_id, Json json_data = {}) = 0;
        virtual bool      manages(int class_id)                                            = 0;
    };

    using IPlatformManagerPtr = std::shared_ptr<IPlatformManager>;
}

// EOF
