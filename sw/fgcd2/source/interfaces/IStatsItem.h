//! @file
//! @brief  Interface to a statistics element (group or field)
//! @author Dariusz Zielinski

#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace fgcd
{
    struct IStatsItem;
    using  IStatsItemPtr  = IStatsItem*;
    using  IStatsItemList = std::vector<IStatsItemPtr>;

    // **********************************************************

    struct IStatsItem
    {
        virtual ~IStatsItem() = default;

        virtual std::string_view getName() const = 0;

        virtual std::string_view getHint() const = 0;

        virtual uint64_t getValue() const = 0;

        virtual bool isParent() const = 0;

        virtual IStatsItemList& getChildren() = 0;
    };
}

// EOF