//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <map>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/thread/inc/Thread.h>

namespace fgcd
{

    class TimerManager : public Component
    {
        using MillisecondTask                = std::function<void(unsigned int millisecond)>;
        static constexpr auto max_milisecond = 20;

      public:
        explicit TimerManager(Component& parent);

        template<int millisecond>
        void registerTask(const MillisecondTask& task)
        {
            // This check ensures that no tasks are registered for 18ms and 19ms, as they are used for synchronization.
            // It could be made possible, but it's not needed and this limitation simplifies the synchronization.
            static_assert(
                millisecond >= 0 && millisecond < max_milisecond,
                "A millisecond task can only be registered for 0-19 ms."
            );

            // Register the task
            m_tasks.emplace(millisecond, task);
        }

        void onStart() override;

        void onStop() override;

      private:
        void millisecondFunc(std::atomic_bool& keep_running);

        // TODO steady clock
        std::multimap<unsigned int, MillisecondTask> m_tasks;              //!< Maps seconds for tasks
        Thread                                       m_ms_thread;          //!< Millisecond thread
    };

    using TimerManagerPtr = std::shared_ptr<TimerManager>;
}

// EOF
