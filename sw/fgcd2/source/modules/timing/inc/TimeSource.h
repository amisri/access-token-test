//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <ctime>
#include <functional>
#include <memory>
#include <timdt-lib-cpp/Timing.h>
#include <unordered_map>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/thread/inc/Thread.h>

namespace fgcd
{
    class TimeSource;
    using TimeSourcePtr = std::shared_ptr<TimeSource>;

    class TimeSource : public Component
    {
        static auto constexpr kTIMING_ERROR_TIMOUT = 3;

        using Callback  = std::function<void(std::timespec)>;
        using Callbacks = std::unordered_map<std::string, Callback>;

      public:
        TimeSource(Component& parent);

        void          registerCallback(std::string event_name, Callback callback);
        std::timespec getTime();

      private:
        void onStart() override;
        void pollEvents(std::atomic_bool& keep_running);

        Callbacks                 m_callbacks;
        Timing::ConnectionContext m_context;
        Thread                    m_thread;
    };
}

// EOF
