//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#include <atomic>

#include <modules/base/clock/inc/utils.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>
#include "../inc/TimerManager.h"

using namespace fgcd;
using namespace std::chrono;

// **********************************************************

TimerManager::TimerManager(Component& parent)
    : Component(parent, "Timer")
{
}

// **********************************************************

void TimerManager::onStart()
{
    m_ms_thread.start(
        threads::timer,
        [this](std::atomic_bool& keep_running)
        {
            millisecondFunc(keep_running);
        }
    );
}

// **********************************************************

void TimerManager::onStop()
{
    m_ms_thread.stop();
    m_ms_thread.forceJoin();
}

// **********************************************************

void TimerManager::millisecondFunc(std::atomic_bool& keep_running)
{
    while (keep_running)
    {
        m_clock.waitForTick();

        // Get all tasks for the current millisecond
        auto millisecond = getMillisecond(m_clock.now()) % max_milisecond;
        auto range       = m_tasks.equal_range(millisecond);

        // And then execute them
        for (auto i = range.first; i != range.second; ++i)
        {
            i->second(i->first);
        }
    }
}

// EOF
