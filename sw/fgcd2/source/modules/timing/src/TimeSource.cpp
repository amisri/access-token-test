//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <ctime>
#include <timdt-lib-cpp/Timing.h>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>
#include "../inc/TimeSource.h"

using namespace fgcd;

// **********************************************************

TimeSource::TimeSource(Component& parent)
    : Component(parent, "TimeSource")
{
}

// **********************************************************

void TimeSource::registerCallback(std::string event_name, Callback callback)
{
    m_callbacks.emplace(event_name, callback);
    m_context.connect(event_name);
}

// **********************************************************

std::timespec TimeSource::getTime()
{
    auto          timdt_time = m_context.getUTCTime();
    std::timespec time_spec  = timdt_time.time;

    // TODO
    // if (fault)
    // {
    //     fault                    = 0;
    //     fgcddev.status.st_faults &= ~FGC_FLT_TIMING;
    //     logPrintf(0, "TIMING Fault cleared: ConnectionContext::getUTCTime() succeeded\n");
    // }

    // timingCheckStatus();

    return time_spec;
}

// **********************************************************

void TimeSource::onStart()
{
    m_thread.start(
        threads::timing,
        [this](std::atomic_bool& keep_running)
        {
            pollEvents(keep_running);
        }
    );
}

// **********************************************************

void TimeSource::pollEvents(std::atomic_bool& keep_running)
{
    m_context.setTimeOut(0);
    while (keep_running)
    {
        Timing::EventValue event_value;

        try
        {
            m_context.wait(&event_value);

            for(auto&& [name, function] : m_callbacks)
            {
                if(name == event_value.getName())
                {
                    function(event_value.getHwTimestamp().time);
                }
            }
        }
        catch (Timing::Exception& e)
        {
                if (e.getCode() != kTIMING_ERROR_TIMOUT)
                {
                    std::cout << "Real problem... " << e.getCode() << " " << e.what() << std::endl;
                }

                break;
        }
    }
}

// EOF
