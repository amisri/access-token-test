#include <ctime>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <thread>

#include <utils/test/mocks/ClockMock.h>
#include <utils/test/mocks/RootMock.h>
#include "../inc/TimerManager.h"

using namespace fgcd;
using namespace fgcd::testing;

// **********************************************************
// Setup
// **********************************************************

struct TimerManagerTests : public ::testing::Test
{
    RootMockPtr      root;
    TimerManagerPtr  object;
    std::vector<int> tasks_logger{};
    MockClockPtr     clock;

    void SetUp() override
    {
        clock  = std::make_shared<MockClock>();
        root   = std::make_unique<RootMock>(clock);
        root->enableLogging();
        object = root->addComponent<TimerManager>();

        object->registerTask<5>(
            [this](auto ms)
            {
                tasks_logger.push_back(ms);
            }
        );
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(TimerManagerTests, TimerManager_test1)
{
    // **********************************************************
    // Preparation
    root->start();
    EXPECT_EQ(tasks_logger.size(), 0);


    // **********************************************************
    // Execution
    clock->add(5ms);
    std::this_thread::sleep_for(1ms);


    // **********************************************************
    // Verification
    ASSERT_EQ(tasks_logger.size(), 1);
    EXPECT_EQ(tasks_logger[0], 5);


    // **********************************************************
    // Execution
    clock->add(20ms);
    std::this_thread::sleep_for(1ms);


    // **********************************************************
    // Verification
    ASSERT_EQ(tasks_logger.size(), 2);
    EXPECT_EQ(tasks_logger[1], 5);
    root->stop();
}

// EOF
