#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/TimeSource.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct TimeSourceTests : public ::testing::Test
{
    RootMockPtr root;
    TimeSourcePtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<TimeSource>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(TimeSourceTests, TimeSource_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(false, false);
}

// EOF
