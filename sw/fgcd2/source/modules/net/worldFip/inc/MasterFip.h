//! @file
//! @brief  MasterFip class that enables to send and receive Ethernet frames.
//! @author Adam Solawa

#pragma once

#include <array>
#include <cstdint>
#include <functional>
#include <map>
#include <masterfip/libmasterfip.h>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryWriter.h>
#include <modules/devices/fieldbus/inc/PODs.h>
#include "Frame.h"

namespace fgcd
{
    class MasterFip : public Component
    {
        static constexpr auto fgcd_max_dev    = 30;
        static constexpr auto extra_variables = 10;

        static constexpr auto fip_bus_speed = MSTRFIP_BITRATE_2500;

        static constexpr auto fip_time_id         = 0x3000;
        static constexpr auto fip_periodic_end_id = 0x3001;
        static constexpr auto fip_real_time_id_1  = 0x3002;
        static constexpr auto fip_real_time_id_2  = 0x3003;
        static constexpr auto fip_code_id         = 0x3004;
        static constexpr auto fip_extra_base_id   = 0x3005;
        static constexpr auto fip_status_base_id  = 0x0601;

        using DeviceArray = std::array<struct mstrfip_data*, fgcd_max_dev>;

        using PrepareCallback = std::function<void(fip::Frame time_frame, fip::Frame code_frame)>;
        using PacketCallback  = std::function<void(fip::Frame frame, std::uint8_t device_id)>;

      public:
        MasterFip(Component& parent, std::uint32_t lun);

        fip::Frame getCommandFrame(std::uint8_t device_id);
        void       sendCommand(fip::Frame frame);
        void       setListener(fieldbus::PacketType type, PacketCallback listener);
        void       setPrepareNextCycle(PrepareCallback callback);

      private:
        //! It is called on component's start.
        //! Starts reception thread.
        void onStart() override;

        //! It is called on component's finish.
        //! Stops reception thread, closes the socket and ring buffer memory.
        void onStop() override;

        void setup();

        void openDevice();
        void checkBusSpeed();
        void createVariables();
        void createMessages();
        void scheduleObjects();

        void handleStatus(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* irq) const;
        void handleTime(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* irq) const;
        void handleCycleFinish(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* irq);
        void handleResponse(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* irq) const;
        void handleInterfaceError(mstrfip_dev* fip, mstrfip_error_list error);

        void createCallback();

        PrepareCallback     m_prepare_cycle_callback;
        std::uint32_t       m_logical_number_unit;
        mstrfip_dev*        m_fip        = nullptr;
        mstrfip_macrocycle* m_macrocycle = nullptr;

        mstrfip_data* m_time_variable        = nullptr;
        mstrfip_data* m_code_variable        = nullptr;
        mstrfip_data* m_real_time_variable_1 = nullptr;
        mstrfip_data* m_real_time_variable_2 = nullptr;
        mstrfip_data* m_periodic_end         = nullptr;

        std::array<mstrfip_data*, fgcd_max_dev> m_status_variables{};
        std::array<mstrfip_data*, extra_variables> m_extra_variables{};
        std::array<mstrfip_data*, fgcd_max_dev> m_command_messages{};
        std::array<mstrfip_data*, fgcd_max_dev> m_response_messages{};

        std::map<fieldbus::PacketType, PacketCallback> m_listeners;
    };
}

// EOF
