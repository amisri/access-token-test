//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <masterfip/libmasterfip.h>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/base/misc/inc/BinaryWriter.h>

namespace fgcd::fip
{
    class Frame
    {
      public:
        Frame(mstrfip_data* handle, std::size_t buffer_size);

        mstrfip_data*              getHandle();

        utils::Result<void, std::string> write(auto&&... elements)
        {
            auto result = m_writer.write(elements...);
            if (result.hasError())
            {
                return utils::Failure{result.error()};
            }

            m_handle->bsz = m_writer.position();
            return {};
        }

        template<typename T>
        auto read()
        {
            return m_reader.read<T>();
        }

        [[nodiscard]] ByteStreamView unreadData() const;

      private:
        ByteSpan      m_buffer;
        BigWriter     m_writer;
        BigReader     m_reader;
        mstrfip_data* m_handle;
    };

}

// EOF
