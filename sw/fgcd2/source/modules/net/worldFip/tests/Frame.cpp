#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/Frame.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct FrameTests : public ::testing::Test
{
    RootMockPtr root;
    FramePtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<Frame>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FrameTests, Frame_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
