//! @file
//! @brief
//! @author Adam Solawa
// https://ohwr.org/project/masterfip-sw/wikis/home

#include <cstdint>
#include <functional>
#include <masterfip/libmasterfip.h>
#include <utility>
#include <vector>

#include <modules/base/core/inc/Exception.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>
#include <modules/devices/fgc2/inc/Config.h>
#include <modules/devices/fgc2/inc/RealTimePacket.h>
#include <modules/devices/fgc2/inc/TimePacket.h>
#include <modules/devices/fieldbus/inc/PODs.h>
#include "../inc/Frame.h"
#include "../inc/MasterFip.h"

using namespace fgcd;

// ************************************************************
// C style callback adapter
// ************************************************************

enum class FipPurpose
{
    status,
    time,
    cycle_finish,
    response,
    error
};

using HandlerFunction = std::function<void(mstrfip_dev*, mstrfip_data*, mstrfip_irq*)>;
using ErrorFunction   = std::function<void(mstrfip_dev*, mstrfip_error_list)>;

static std::map<FipPurpose, HandlerFunction> handler_mapper;   // NOLINT
static ErrorFunction                         error_handler;    // NOLINT

// **********************************************************

template<FipPurpose purpose>
static void handler(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* irq)
{
    auto function = handler_mapper.at(purpose);
    function(fip, data, irq);
}

// **********************************************************

static void errorHandlerWrapper(mstrfip_dev* dev, mstrfip_error_list error)
{
    error_handler(dev, error);
}

// **********************************************************
// Master fip class
// ************************************************************

MasterFip::MasterFip(Component& parent, std::uint32_t lun)
    : Component(parent, "FIP"),
      m_logical_number_unit(lun)
{
    setup();

    using enum FipPurpose;
    handler_mapper[time]         = std::bind_front(&MasterFip::handleTime, this);
    handler_mapper[status]       = std::bind_front(&MasterFip::handleStatus, this);
    handler_mapper[cycle_finish] = std::bind_front(&MasterFip::handleCycleFinish, this);
    handler_mapper[response]     = std::bind_front(&MasterFip::handleResponse, this);
    error_handler                = std::bind_front(&MasterFip::handleInterfaceError, this);
}

// **********************************************************

fip::Frame MasterFip::getCommandFrame(std::uint8_t device_id)
{
    auto* variable = m_command_messages.at(device_id);

    return {variable, fgc2::config::max_command_length};
}

// **********************************************************

void MasterFip::sendCommand(fip::Frame frame)
{
    if (mstrfip_msg_write(m_fip, frame.getHandle()) < 0)
    {
        logError("Variable sent faliure: {}", mstrfip_strerror(errno));
    }
    else
    {
        logInfo("Send frame");
    }
}

// **********************************************************

void MasterFip::setListener(fieldbus::PacketType type, PacketCallback listener)
{
    m_listeners.insert_or_assign(type, listener);
}

// **********************************************************

void MasterFip::setPrepareNextCycle(PrepareCallback callback)
{
    m_prepare_cycle_callback = std::move(callback);
}

// **********************************************************

void MasterFip::onStart()
{
    if (mstrfip_ba_load(m_fip, m_macrocycle) < 0)
    {
        mstrfip_ba_reset(m_fip);
        throw Exception("Failed to load macrocycle: {}", mstrfip_strerror(errno));
    }

    if (mstrfip_ba_start(m_fip) < 0)
    {
        mstrfip_ba_reset(m_fip);
        throw Exception("Failed to start BA: {}", mstrfip_strerror(errno));
    }
}

// ************************************************************

void MasterFip::onStop()
{
    // TODO
}

// ************************************************************

void MasterFip::setup()
{
    openDevice();
    checkBusSpeed();

    // ************************************************************
    // Software config
    // ************************************************************
    mstrfip_sw_cfg software_config{
        .irq_thread_prio       = threads::socket.priority(),
        .mstrfip_error_handler = errorHandlerWrapper,
    };
    if (mstrfip_sw_cfg_set(m_fip, &software_config) < 0)
    {
        throw Exception("Cannot configure SW: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Hardware config
    // ************************************************************
    mstrfip_hw_cfg hardware_config{
        hardware_config.enable_ext_trig      = 1,
        hardware_config.enable_ext_trig_term = 0,
        hardware_config.enable_ext_trig_term = 1,
    };
    if (mstrfip_hw_cfg_set(m_fip, &hardware_config) < 0)
    {
        throw Exception("Cannot configure HW: {}", mstrfip_strerror(errno));
    }

    createVariables();
    createMessages();
    scheduleObjects();
}

// ************************************************************

void MasterFip::openDevice()
{
    if (mstrfip_init() < 0)
    {
        throw Exception("Cannot init fip library: {}", mstrfip_strerror(errno));
    }

    m_fip = mstrfip_open_by_lun(m_logical_number_unit);
    if (m_fip == nullptr)
    {
        throw Exception("Cannot open masterfip dev: {}", mstrfip_strerror(errno));
    }

    if (mstrfip_rtapp_reset(m_fip) < 0)
    {
        throw Exception("Cannot reset rt app: {}", mstrfip_strerror(errno));
    }

    m_macrocycle = mstrfip_macrocycle_create(m_fip);
    if (m_macrocycle == nullptr)
    {
        throw Exception("Create macrocycle failed: {}", mstrfip_strerror(errno));
    }
}

// ************************************************************

void MasterFip::checkBusSpeed()
{
    mstrfip_bitrate  speed{};
    std::string_view speed_string;

    if (mstrfip_hw_speed_get(m_fip, &speed) < 0)
    {
        throw Exception("Can't get FIP speed: {}", mstrfip_strerror(errno));
    }

    if (speed != fip_bus_speed)
    {
        switch (speed)
        {
            case MSTRFIP_BITRATE_31:
                speed_string = "31.25 kb/s";
                break;
            case MSTRFIP_BITRATE_1000:
                speed_string = "1 Mb/s";
                break;
            case MSTRFIP_BITRATE_2500:
                speed_string = "2.5 Mb/s";
                break;
            case MSTRFIP_BITRATE_UNDEFINED:
                speed_string = "Undefined speed";
                break;
        }

        throw Exception("Expected FIP bus speed 2.5 Mb/s and got: {}", speed_string);
    }
}

// ************************************************************

void MasterFip::createVariables()
{
    // ************************************************************
    // Time variable
    // ************************************************************
    mstrfip_data_cfg config{
        .id                   = fip_time_id,
        .flags                = MSTRFIP_DATA_FLAGS_PROD,
        .max_bsz              = fgc2::TimePacket::size,
        .mstrfip_data_handler = handler<FipPurpose::time>,
    };

    m_time_variable = mstrfip_var_create(m_macrocycle, &config);
    if (m_time_variable == nullptr)
    {
        throw Exception("Time var creation failed: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Code variable
    // ************************************************************
    config = {
        .id                   = fip_code_id,
        .flags                = MSTRFIP_DATA_FLAGS_PROD,
        .max_bsz              = fgc2::config::code_blocks_per_message * fgc2::config::code_block_size,
        .mstrfip_data_handler = nullptr,
    };

    m_code_variable = mstrfip_var_create(m_macrocycle, &config);
    if (m_code_variable == nullptr)
    {
        throw Exception("Code var creation failed: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Real-time variable 1
    // ************************************************************
    config = {
        .id                   = fip_real_time_id_1,
        .flags                = MSTRFIP_DATA_FLAGS_PROD,
        .max_bsz              = sizeof(fgc2::RtData),
        .mstrfip_data_handler = nullptr,
    };

    m_real_time_variable_1 = mstrfip_var_create(m_macrocycle, &config);
    if (m_real_time_variable_1 == nullptr)
    {
        throw Exception("Real time 1 var creation failed: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Real-time variable 2
    // ************************************************************
    config = {
        .id                   = fip_real_time_id_2,
        .flags                = MSTRFIP_DATA_FLAGS_PROD,
        .max_bsz              = sizeof(fgc2::RtData),
        .mstrfip_data_handler = nullptr,
    };

    m_real_time_variable_2 = mstrfip_var_create(m_macrocycle, &config);
    if (m_real_time_variable_2 == nullptr)
    {
        throw Exception("Real time 2 var creation failed: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Periodic end variable
    // ************************************************************
    config = {
        .id                   = fip_periodic_end_id,
        .flags                = MSTRFIP_DATA_FLAGS_PROD,
        .max_bsz              = sizeof(uint16_t),
        .mstrfip_data_handler = handler<FipPurpose::cycle_finish>,
    };

    m_periodic_end = mstrfip_var_create(m_macrocycle, &config);
    if (m_periodic_end == nullptr)
    {
        throw Exception("PerEnd var creation failed: {}", mstrfip_strerror(errno));
    }

    // Its really important but unused variable.
    // Without this variable, slave device can just break at some random moment.
    for (auto i = 0; i < extra_variables; i++)
    {
        config = {
            .id                   = fip_extra_base_id + i,
            .flags                = MSTRFIP_DATA_FLAGS_PROD,
            .max_bsz              = sizeof(std::uint16_t),
            .mstrfip_data_handler = nullptr,
        };

        auto* extra_var = mstrfip_var_create(m_macrocycle, &config);
        if (extra_var == nullptr)
        {
            throw Exception("Extra var creation failed: {}", mstrfip_strerror(errno));
        }

        m_extra_variables[i] = extra_var;
    }

    // ************************************************************
    // Status variable
    // ************************************************************
    for (auto i = 0; i < fgcd_max_dev; i++)
    {
        config = {
            .id                   = fip_status_base_id + i,
            .flags                = MSTRFIP_DATA_FLAGS_CONS,
            .max_bsz              = sizeof(fieldbus::StatusPacketBarePOD),
            .mstrfip_data_handler = handler<FipPurpose::status>,
        };

        auto* status_var = mstrfip_var_create(m_macrocycle, &config);
        if (status_var == nullptr)
        {
            throw Exception("Status var creation failed: {}", mstrfip_strerror(errno));
        }

        m_status_variables[i] = status_var;
    }
}

// ************************************************************

void MasterFip::createMessages()
{

    for (auto i = 1; i < fgcd_max_dev; i++)
    {
        // ************************************************************
        // Command message
        // ************************************************************
        mstrfip_data_cfg config{
            .id                   = i,
            .flags                = MSTRFIP_DATA_FLAGS_PROD,
            .max_bsz              = fgc2::config::max_command_length,
            .mstrfip_data_handler = nullptr,   // TODO
        };

        auto* command_message = mstrfip_msg_create(m_macrocycle, &config);
        if (command_message == nullptr)
        {
            throw Exception("Cannot create command message: {}", mstrfip_strerror(errno));
        }
        m_command_messages[i] = command_message;

        // ************************************************************
        // Response message
        // ************************************************************
        config = {
            .id                   = i,
            .flags                = MSTRFIP_DATA_FLAGS_CONS,
            .max_bsz              = fgc2::config::max_command_length,
            .mstrfip_data_handler = handler<FipPurpose::response>,
        };

        auto *response_message = mstrfip_msg_create(m_macrocycle, &config);
        if (response_message == nullptr)
        {
            throw Exception("Cannot create response message: {}", mstrfip_strerror(errno));
        }
        m_response_messages[i] = response_message;
    }
}

// ************************************************************

void MasterFip::scheduleObjects()
{

    // ************************************************************
    // Schedule first periodic window
    // ************************************************************

    std::vector<mstrfip_data*> periodic_window;
    periodic_window.push_back(m_time_variable);
    periodic_window += m_extra_variables;
    periodic_window.push_back(m_code_variable);
    periodic_window += m_status_variables;
    periodic_window.push_back(m_periodic_end);

    mstrfip_per_var_wind_cfg periodic_window_config{
        .varlist   = periodic_window.data(),
        .var_count = periodic_window.size(),
    };

    if (mstrfip_per_var_wind_append(m_macrocycle, &periodic_window_config) < 0)
    {
        throw Exception("Appending periodic window failed: {}", mstrfip_strerror(errno));
    }

    if (mstrfip_wait_wind_append(m_macrocycle, 1, 11'000) < 0)
    {
        throw Exception("Appending silent window failed: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Schedule aperiodic window
    // ************************************************************

    mstrfip_aper_msg_wind_cfg aperiodic_window_config{
        .end_ustime               = 18'000,
        .prod_msg_fifo_size       = fgcd_max_dev,
        .cons_msg_fifo_size       = fgcd_max_dev,
        .mstrfip_cons_msg_handler = nullptr,
        .mstrfip_prod_msg_handler = nullptr,
    };

    if (mstrfip_aper_msg_wind_append(m_macrocycle, &aperiodic_window_config) < 0)
    {
        throw Exception("Appending aperiodic window failed: {}", mstrfip_strerror(errno));
    }

    if (mstrfip_wait_wind_append(m_macrocycle, 1, 19'000) < 0)
    {
        throw Exception("Appending silent window failed: {}", mstrfip_strerror(errno));
    }

    // ************************************************************
    // Schedule second periodic window
    // ************************************************************

    periodic_window.clear();
    periodic_window.push_back(m_real_time_variable_1);
    periodic_window.push_back(m_real_time_variable_2);

    periodic_window_config = {
        .varlist   = periodic_window.data(),
        .var_count = periodic_window.size(),
    };

    if (mstrfip_per_var_wind_append(m_macrocycle, &periodic_window_config) < 0)
    {
        throw Exception("Appending periodic window failed: {}", mstrfip_strerror(errno));
    }

    if (mstrfip_wait_wind_append(m_macrocycle, 1, 20'000) < 0)
    {
        throw Exception("Appending silent window failed: {}", mstrfip_strerror(errno));
    }
}

// ************************************************************

void MasterFip::handleStatus(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* /*unused*/) const
{
    mstrfip_var_update(fip, data);

    auto device_id = std::uint8_t(data->id);

    switch (data->status)
    {
        case MSTRFIP_DATA_OK:
            if (m_listeners.contains(fieldbus::PacketType::status))
            {
                fip::Frame frame{data, data->bsz};
                m_listeners.at(fieldbus::PacketType::status)(frame, device_id);
            }
            break;

        case MSTRFIP_DATA_PAYLOAD_ERROR:
            logTrace("ERROR: MSTRFIP_DATA_PAYLOAD_ERROR {}", data->payload_error);
            break;
        case MSTRFIP_DATA_FRAME_ERROR:
            logTrace("ERROR: MSTRFIP_DATA_FRAME_ERROR");
            break;
        case MSTRFIP_DATA_NOT_RECEIVED:
            logTrace("ERROR: MSTRFIP_DATA_NOT_RECEIVED");
            break;
    }
}

// ************************************************************

void MasterFip::handleCycleFinish(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* irq)
{
    fip::Frame time_frame{m_time_variable, m_time_variable->bsz};
    fip::Frame code_frame{m_code_variable, m_code_variable->bsz};

    // ************************************************************

    if(m_prepare_cycle_callback)
    {
        m_prepare_cycle_callback(time_frame, code_frame);
    }

    // ************************************************************

    if (mstrfip_var_write(m_fip, m_code_variable) != 0)
    {
        logError("Code var sent faliure: '{}'", mstrfip_strerror(errno));
    }

    if (mstrfip_var_write(m_fip, m_time_variable) != 0)
    {
        logError("Time var sent faliure: '{}'", mstrfip_strerror(errno));
    }

    if (mstrfip_var_write(m_fip, m_real_time_variable_1) < 0)
    {
        logError("Real time var1 sent faliure: '{}'", mstrfip_strerror(errno));
    }

    if (mstrfip_var_write(m_fip, m_real_time_variable_2) < 0)
    {
        logError("Real time var2 sent faliure: '{}'", mstrfip_strerror(errno));
    }
}


// ************************************************************

void MasterFip::handleTime(mstrfip_dev* /*unused*/, mstrfip_data* /*unused*/, mstrfip_irq* /*unused*/) const
{
    // TODO
}

// ************************************************************

void MasterFip::handleResponse(mstrfip_dev* fip, mstrfip_data* data, mstrfip_irq* /*unused*/) const
{
    mstrfip_msg_update(fip, data);

    auto device_id = std::uint8_t(data->id);

    switch (data->status)
    {
        case MSTRFIP_DATA_OK:
        {
            if (m_listeners.contains(fieldbus::PacketType::response))
            {
                fip::Frame frame{data, data->bsz};
                m_listeners.at(fieldbus::PacketType::response)(frame, device_id);
            }
            break;
        }

        case MSTRFIP_DATA_PAYLOAD_ERROR:
            logTrace("ERROR: MSTRFIP_DATA_PAYLOAD_ERROR {}", data->payload_error);
            break;
        case MSTRFIP_DATA_FRAME_ERROR:
            logTrace("ERROR: MSTRFIP_DATA_FRAME_ERROR");
            break;
        case MSTRFIP_DATA_NOT_RECEIVED:
            logTrace("ERROR: MSTRFIP_DATA_NOT_RECEIVED");
            break;
    }
}

// ************************************************************

void MasterFip::handleInterfaceError(mstrfip_dev* fip, mstrfip_error_list error)
{
    logError("Interface error");
}

// EOF
