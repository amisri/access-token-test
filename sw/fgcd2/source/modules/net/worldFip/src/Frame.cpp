//! @file
//! @brief  
//! @author Adam Solawa

#include <cstddef>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include "../inc/Frame.h"

using namespace fgcd::fip;

// **********************************************************

Frame::Frame(mstrfip_data* handle, std::size_t buffer_size)
    : m_buffer(reinterpret_cast<std::byte*>(handle->buffer), buffer_size),
      m_writer(m_buffer),
      m_reader(m_buffer),
      m_handle(handle)
{
}

// **********************************************************

mstrfip_data* Frame::getHandle()
{
    return m_handle;
}

// **********************************************************

fgcd::ByteStreamView Frame::unreadData() const
{
    return m_reader.unreadData();
}

// EOF
