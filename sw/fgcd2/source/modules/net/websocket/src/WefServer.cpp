//! @file
//! @brief
//! @author Adam Solawa

#include <any>
#include <exception>
#include <fmt/format.h>
#include <functional>
#include <memory>
#include <optional>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>

#include <cpp_utils/misc.h>
#include <cpp_utils/typeTraits.h>
#include <interfaces/ICommandDistributor.h>
#include <interfaces/IWebsocket.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>
#include <modules/devices/base/inc/Response.h>
#include "../inc/CommandParser.h"
#include "../inc/ContextManager.h"
#include "../inc/WefServer.h"

using namespace fgcd;
using namespace fgcd::wefserver;

// **********************************************************

WefServer::WefServer(Component& parent, IWebsocketPtr websocket, ICommandDistributorPtr distributor)
    : Component(parent, "WefServer"),
      m_websocket(std::move(websocket)),
      m_distributor(std::move(distributor)),
      m_response_queue(std::make_shared<ResponseQueue>())
{
    m_websocket->setMessageHandler(std::bind_front(&WefServer::processMessage, this));
}

// **********************************************************

void WefServer::onStart()
{
    m_response_thread.start(
        threads::websocket_server,
        [this](std::atomic_bool& keep_running)
        {
            responseThread(keep_running);
        }
    );
}

// **********************************************************

void WefServer::responseThread(std::atomic_bool& keep_running)
{
    while (keep_running)
    {
        auto result = m_response_queue->popBlock();
        if (not result)
        {
            logWarning("Received empty response");
            continue;
        }

        auto&& command  = **result;

        auto connection = std::any_cast<wefserver::Connection>(command.specific());
        if (not m_context.hasConnection(connection))
        {
            logDebug("Context doesn't contian given connection");
            return;
        }

        auto payload = serializeResponse(command);
        if (payload.empty())
        {
            logError("Received empty response");
            return;
        }

        
        m_websocket->sendMessage(connection.handle, payload);
    }
}

// **********************************************************

bool WefServer::validateMessage(ConnectionHandle handle, const Json& payload)
{
    if (not payload.contains("type"))
    {
        m_websocket->sendMessage(
            handle,
            {
                {"type", "INIT"},
                {"status", Status::BAD_REQUEST},
                {"error", "Missing field type"},
                {"version", version},
            }
        );
        return false;
    }

    if (payload.at("type") != "INIT")
    {
        if (not m_context.hasConnection(handle))
        {
            logWarning("Received connection from not initialzied connection");
            sendErrorResponse(handle, "", "Connection not initalized", Status::UNAUTHORIZED);
            return false;
        }
    }

    return true;
}

// **********************************************************

void WefServer::processMessage(ConnectionHandle handle, const Json& payload)
{
    try
    {
        if (not validateMessage(handle, payload))
        {
            return;
        }

        auto&& type = payload.at("type").get<MessageType>();
        switch (type)
        {
            case MessageType::init:
                processInit(handle, payload);
                break;

            case MessageType::get:
                processGet(handle, payload);
                break;

            case MessageType::getex:
                processGetEx(handle, payload);
                break;

            case MessageType::set:
                processSet(handle, payload);
                break;

            case MessageType::setex:
                processGetEx(handle, payload);
                break;

            case MessageType::sub:
                processSub(handle, payload);
                break;

            case MessageType::unsub:
                processUnsub(handle, payload);
                break;

            case MessageType::clear:
                processClear(handle, payload);
                break;

            case MessageType::term:
                processTerm(handle, payload);
                break;

            default:
                logError("Invalid command type {}", payload.at("type"));
        }
    }
    catch (std::exception& e)
    {
        // clang-format off
        m_websocket->sendMessage(
            handle,
            {
                {"type",            "RESPONSE"},
                {"value",           ""},
                {"timestamp",       {{"sec", 0}, {"nano", 0}}},
                {"cycle_timestamp", {{"sec", 0}, {"nano", 0}}},
                {"status",          Status::BAD_REQUEST},
                {"code",            0},
                {"error",           e.what()},
                {"tag",             ""},
            }
        );
        // clang-format on
    }
}

// **********************************************************

void WefServer::processInit(ConnectionHandle handle, const Json& payload)
{
    logInfo("Process init");

    try
    {
        auto request = payload.get<InitRequest>();

        m_context.createConnection(handle);
        logInfo("Connection created");

        m_websocket->sendMessage(
            handle,
            {
                {"type", "INIT"},
                {"status", Status::OK},
                {"error", ""},
                {"version", version},
            }
        );
    }
    // Catch JSON exceptions
    catch (std::exception& e)
    {
        m_websocket->sendMessage(
            handle,
            {
                {"type", "INIT"},
                {"status", Status::BAD_REQUEST},
                {"error", e.what()},
                {"version", version},
            }
        );
    }
}

// **********************************************************

void WefServer::processGet(ConnectionHandle handle, const Json& payload)
{
    logInfo("Process get");

    auto request = payload.get<GetRequest>();

    auto fields = CommandParser::parse(request.command);
    if (fields.hasError())
    {
        sendErrorResponse(handle, request.tag, fields.error(), Status::BAD_REQUEST);
        return;
    }

    auto command = createCommand(handle, CommandType::GET, request.device, request.tag, *fields);
    m_distributor->pushCommand(std::move(command));
}

// **********************************************************

void WefServer::processGetEx(ConnectionHandle handle, const Json& payload)
{
    logInfo("Process get ex");

    auto request = payload.get<GetExRequest>();

    auto command = createCommand(handle, CommandType::GET, request.device, request.tag, request.fields);
    m_distributor->pushCommand(std::move(command));
}

// **********************************************************

void WefServer::processSet(ConnectionHandle handle, const Json& payload)
{
    logInfo("Process set");

    auto request = payload.get<SetRequest>();

    auto fields = CommandParser::parse(request.command);
    if (fields.hasError())
    {
        sendErrorResponse(handle, request.tag, fields.error(), Status::BAD_REQUEST);
        return;
    }

    auto command = createCommand(handle, CommandType::SET, request.device, request.tag, *fields);
    m_distributor->pushCommand(std::move(command));
}

// **********************************************************

void WefServer::processSetEx(ConnectionHandle handle, const Json& payload)
{
    logInfo("Process setex");

    auto request = payload.get<SetExRequest>();

    auto command = createCommand(handle, CommandType::SET, request.device, request.tag, request.fields);
    m_distributor->pushCommand(std::move(command));
}

// **********************************************************

void WefServer::processSub(ConnectionHandle handle, const Json& payload)
{
    logInfo("Process sub");

    auto request = payload.get<SubRequest>();

    auto fields = CommandParser::parse(request.command);
    if (fields.hasError())
    {
        sendErrorResponse(handle, request.tag, fields.error(), Status::BAD_REQUEST);
        return;
    }

    auto command = createCommand(handle, CommandType::SUB, request.device, request.tag, *fields);
    m_distributor->pushCommand(std::move(command));

    auto closure = createCommand(handle, CommandType::UNSUB, request.device, request.tag, *fields);
    m_context.pushClosure(handle, std::move(closure));
}

// **********************************************************

void WefServer::processUnsub(ConnectionHandle handle, const Json& payload)
{
    logDebug("Process unsub");

    auto request = payload.get<UnsubRequest>();

    auto closure = m_context.popClosure(handle, request.tag);
    if (not closure)
    {
        // Code error
        sendErrorResponse(handle, request.tag, "Internal server error", Status::BAD_REQUEST);
        return;
    }

    m_distributor->pushCommand(std::move(closure));
}

// **********************************************************

void WefServer::processClear(ConnectionHandle handle, const Json& payload)
{
    logDebug("Process clear");

    if (payload.contains("credentials"))
    {
        // clang-format off
        m_websocket->sendMessage(
            handle,
            {
                {"type",   "CLEAR"},
                {"status", Status::BAD_REQUEST},
                {"error",  "Missing fields"},
            }
        );
        // clang-format on
        return;
    }

    closeConnection(handle);
    m_context.createConnection(handle);
}

// **********************************************************

void WefServer::processTerm(ConnectionHandle handle, const Json& payload)
{
    logDebug("Process term");

    auto request = payload.get<TermRequest>();
}

// **********************************************************

void WefServer::sendErrorResponse(ConnectionHandle handle, std::string_view tag, std::string_view error, Status status)
{
    // clang-format off
    m_websocket->sendMessage(
        handle,
        {
            {"type",            "RESPONSE"},
            {"value",           ""},
            {"timestamp",       {{"sec", 0}, {"nano", 0}}},
            {"cycle_timestamp", {{"sec", 0}, {"nano", 0}}},
            {"user",            0},
            {"status",          status},
            {"code",            0},
            {"error",           error},
            {"tag",             tag},
        }
    );
    // clang-format on
}

// **********************************************************

void WefServer::closeConnection(ConnectionHandle handle)
{
    auto* context = m_context.getContext(handle);
    if (not context)
    {
        return;
    }

    for (auto&& command : context->closures)
    {
        m_distributor->pushCommand(std::move(command));
    }

    m_context.closeConnection(context->connection.user);
}

// **********************************************************

Json WefServer::serializeResponse(const Command& command)
{
    auto&& response = command.response();

    auto serializer = utils::overloaded{
        [&](const fgcd::CommandResponse& content) -> Json
        {
            // clang-format off
            return {
                {"type",        "RESPONSE"},
                {"value",       fmt::to_string(content.values)},
                {"timestamp",
                    {
                        {"sec",     content.timestamp.second},
                        {"nano",    content.timestamp.microsecond}
                    }
                },
                {"cycle_timestamp",
                    {
                        {"sec",     content.timestamp.second},
                        {"nano",    content.timestamp.microsecond}
                    }
                },
                {"user_name",   command.selector().value_or("")},
                {"status",      Status::OK},
                {"code",        0},
                {"error",       ""},
                {"tag",         command.tag()},
            };
            // clang-format on
        },
        [&](const fgcd::PublicationResponse& content) -> Json
        {
            // clang-format off
            return {
                {"type",            "NOTIFICATION"},
                {"value",           fmt::to_string(content.values)},
                {"timestamp",
                    {
                        {"sec",     content.timestamp.second},
                        {"nano",    content.timestamp.microsecond}
                    }
                },
                {"cycle_timestamp",
                    {
                        {"sec",     content.timestamp.second},
                        {"nano",    content.timestamp.microsecond}
                    }
                },
                {"user_name",       command.selector().value_or("")},
                {"tag",             command.tag()},
            };
            // clang-format on
        },
        [&](const fgcd::ErrorResponse& content) -> Json
        {
            // clang-format off
            return {
                {"type",            "RESPONSE"},
                {"value",           ""},
                {"timestamp",
                    {
                        {"sec",     0},
                        {"nano",    0}
                    }
                },
                {"cycle_timestamp",
                    {
                        {"sec",     0},
                        {"nano",    0}
                    }
                },
                {"user_name",       command.selector().value_or("")},
                {"status",          Status::OK},
                {"code",            content.error_code},
                {"error",           content.message},
                {"tag",             command.tag()},
            };
            // clang-format on
        },
        [&](const std::monostate& content) -> Json
        {
            return {};
        },
    };

    return std::visit(serializer, response);
}

// **********************************************************

CommandPtr WefServer::createCommand(
    const ConnectionHandle& handle, CommandType type, const std::string& device, const std::string& tag,
    const CommandFields& fields
)
{
    const auto* context = m_context.getContext(handle);
    bool        parse   = false;

    return std::make_unique<Command>(
        m_response_queue, context->connection, type, device, tag, parse,
        fields.property,      //
        fields.selector,      //
        fields.transaction,   //
        fields.filter,        //
        fields.rest           //
    );
}

// EOF
