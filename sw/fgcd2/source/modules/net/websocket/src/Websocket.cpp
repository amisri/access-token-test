//! @file
//! @brief  A wrapper-class (around Websocketpp library) to create a Websocket server.
//! @author Dariusz Zielinski

#include <exception>
#include <iostream>

#define ASIO_STANDALONE
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include <modules/base/thread/inc/threads.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/Websocket.h"

using namespace fgcd;
using ConnectionHandle = websocketpp::connection_hdl;

// ************************************************************

Websocket::Websocket(Component& parent, uint16_t port)
    : Component(parent, "Websocket"), m_port(port)
{
    m_server = std::make_unique<websocketpp::server<websocketpp::config::asio>>();

    m_server->init_asio();

    // Adjust log levels
    // m_server.set_access_channels(websocketpp::log::alevel::all);
    m_server->clear_access_channels(websocketpp::log::alevel::frame_header | websocketpp::log::alevel::frame_payload);

    m_server->set_open_handler([this](ConnectionHandle handle)
    {
        logInfo("New connection");  // TODO

        {
            std::scoped_lock lock(m_handlers_mutex);
            m_clients.insert(handle);
        }

        if (m_open_handler)
        {
            m_open_handler(handle);
        }
    });

    m_server->set_close_handler([this](ConnectionHandle handle)
    {
        logInfo("Connection closed");  // TODO

        {
            std::scoped_lock lock(m_handlers_mutex);
            m_clients.erase(handle);
        }

        if (m_close_handler)
        {
            m_close_handler(handle);
        }
    });

    m_server->set_message_handler([this](ConnectionHandle handle, Server::message_ptr msg)
    {
        onMessage(handle, msg->get_payload());
    });

    logInfo("Websockets initialized");  // TODO
}

// ************************************************************

// This is needed to have an unique_ptr to an incomplete type (m_server)
Websocket::~Websocket() = default;

// ************************************************************

void Websocket::setOpenHandler(const Websocket::StateHandler& handler)
{
    if (!m_is_running)
    {
        m_open_handler = handler;
    }
}

// ************************************************************

void Websocket::setCloseHandler(const Websocket::StateHandler& handler)
{
    if (!m_is_running)
    {
        m_close_handler = handler;
    }
}

// ************************************************************

void Websocket::setValidateHandler(const Websocket::ValidateHandler& handler)
{
    if (!m_is_running)
    {
        m_server->set_validate_handler([=](ConnectionHandle handle)
        {
            return handler(std::move(handle));
        });
    }
}

// ************************************************************

void Websocket::setMessageHandler(MessageHandler handler)
{
     std::scoped_lock lock(m_handlers_mutex);
     m_handler = handler;
}

// ************************************************************

void Websocket::sendMessage(ConnectionHandle client, const Json& data)
{
    try
    {
        logInfo("Sending message: {}", data);   // TODO
        m_server->send(std::move(client), data.dump(), websocketpp::frame::opcode::text);
    }
    catch (std::exception& e)
    {
        logError("Websocket error: {}", e.what());
        // TODO return an error
    }

    // TODO return a success
}

// ************************************************************

void Websocket::broadcastMessage(const Json& data)
{
    std::scoped_lock lock(m_handlers_mutex);
    for (const auto& client: m_clients)
    {
        sendMessage(client, data);
    }
}

// ************************************************************

void Websocket::onStart()
{
    m_thread.start(threads::websockets, [this](std::atomic_bool& keep_running)
    {
        m_is_running = true;
        m_server->set_reuse_addr(true);
        m_server->listen(m_port);
        m_server->start_accept();
        m_server->run();
        m_is_running = false;
    });

}

// ************************************************************

void Websocket::onStop()
{
    // Stop the server
    m_server->stop_listening();

    {
        // Close all the connections
        std::scoped_lock locK(m_handlers_mutex);
        for (const auto& client: m_clients)
        {
            m_server->close(client, websocketpp::close::status::normal, "");
        }
    }

    // Wait a bit
    utils::setTimeout([&]() { return !m_is_running; }, 50ms);

    // Stop and join the thread
    m_thread.stop();
    m_thread.forceJoin();
}

// ************************************************************

void Websocket::onMessage(WebsocketClient client, std::string_view message)
{
    try
    {
        logInfo("Received message: {}", message);   // TODO

        // Parse the message and get command
        Json parsed_message = Json::parse(message);
        auto command = parsed_message["command"];

        // Try to find a handler for this command
        if(m_handler)
        {
            m_handler(client, parsed_message);
        }
        else
        {
            logError("No handler provided");
        }
    }
    // Catch JSON exceptions
    catch (std::exception& e)
    {
        logError("Error during processing of Websocket message: '{}'", e.what());
    }
}



// EOF
