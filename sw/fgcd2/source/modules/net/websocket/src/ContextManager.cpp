//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <cstdint>
#include <utility>
#include "../inc/ContextManager.h"

using namespace fgcd::wefserver;
using namespace fgcd;

// **********************************************************

void ContextManager::createConnection(ConnectionHandle handle)
{
    auto user = getId();

    Connection connection{std::move(handle), user};
    m_connections.emplace_back(connection);
}

// **********************************************************

bool ContextManager::pushClosure(const ConnectionHandle& handle, CommandPtr command)
{
    auto* context = getContext(handle);
    if (not context)
    {
        return false;
    }

    context->closures.push_back(std::move(command));
    return true;
}

// **********************************************************

CommandPtr ContextManager::popClosure(const ConnectionHandle& handle, const std::string& tag)
{
    auto* context = getContext(handle);
    if (not context)
    {
        return nullptr;
    }

    auto&& closures = context->closures;

    auto result = std::find_if(
        closures.begin(), closures.end(),
        [&](const CommandPtr& command)
        {
            return command->tag() == tag;
        }
    );
    if (result == closures.end())
    {
        return nullptr;
    }

    auto closure = std::move(*result);
    closures.erase(result);
    return closure;
}

// **********************************************************

void ContextManager::closeConnection(std::uint32_t user)
{
    m_active_users.erase(user);
}

// **********************************************************

ConnectionContext* ContextManager::getContext(const ConnectionHandle& handle)
{
    for (auto&& context : m_connections)
    {
        if (handleEquals(context.connection.handle, handle))
        {
            return &context;
        }
    }

    return nullptr;
}

// **********************************************************

[[nodiscard]] bool ContextManager::hasConnection(const Connection& connection) const
{
    for (auto&& context : m_connections)
    {
        if (handleEquals(context.connection.handle, connection.handle))
        {
            if (context.connection.user == connection.user)
            {
                return true;
            }
        }
    }

    return false;
}

// **********************************************************

[[nodiscard]] bool ContextManager::hasConnection(const ConnectionHandle& handle) const
{
    for (auto&& context : m_connections)
    {
        if ( not context.connection.handle.owner_before(handle))
        {
            return true;
        }
    }

    return false;
}

// **********************************************************

std::uint32_t ContextManager::getId()
{
    std::uint32_t user = m_last_id;

    do
    {
        user      = user + 1;
        user      %= std::numeric_limits<std::uint32_t>::max();
    } while (m_active_users.contains(user));

    m_active_users.insert(user);
    m_last_id = user;

    return user;
}

// **********************************************************

bool ContextManager::handleEquals(const ConnectionHandle& h1, const ConnectionHandle& h2)
{
    return not h1.owner_before(h2) && not h2.owner_before(h1);
}

// EOF
