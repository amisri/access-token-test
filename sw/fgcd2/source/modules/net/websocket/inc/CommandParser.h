//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <array>
#include <cctype>
#include <fmt/core.h>
#include <fmt/format.h>
#include <string_view>

#include <cpp_utils/Result.h>
#include "ParserCombinator.h"

namespace fgcd
{
    struct CommandFields
    {
        std::string                property;
        std::optional<std::string> selector;
        std::optional<std::string> transaction;
        std::optional<std::string> filter;
        std::string                rest;

        bool operator==(const CommandFields& rhs) const = default;
    };

    class CommandParser
    {
        static constexpr auto is_property = [](char c)
        {
            return std::isalnum(c) || c == '.' || c == '_';
        };
        static constexpr auto is_user = [](char c)
        {
            return std::isalnum(c) || c == '.' || c == '_';
        };
        static constexpr auto is_filter = [](char c)
        {
            return c != ']';
        };
        static constexpr auto is_value = [](char c)
        {
            return not std::isspace(c);
        };
        static constexpr auto is_rest = [](char c)
        {
            return true;
        };

        static constexpr CParser auto property_parser    = many1P(is_property, "alnum, '.' or '_'");
        static constexpr CParser auto user_parser        = charP('(') >> manyP(is_user) << charP(')');
        static constexpr CParser auto transaction_parser = charP('<') >> many1P(::isdigit, "digit") << charP('>');
        static constexpr CParser auto filter_parser      = charP('[') >> manyP(is_filter) << charP(']');
        static constexpr CParser auto rest_parser        = (many1P(::isspace, "' '") >> manyP(is_rest)) | emptyP;

      public:
        static utils::Result<CommandFields, std::string> parse(std::string_view text)
        {
            static auto parser = property_parser & parseOptions & rest_parser;

            auto result = parser(text, 0);
            if (result.hasError())
            {
                return utils::Failure{fmt::to_string(result.error())};
            }

            auto&& [property, tuf_options, rest] = result->value;
            auto&& [transaction, user, filter]   = tuf_options;

            CommandFields fields;
            fields.property    = property;
            fields.transaction = transaction;
            fields.selector    = user;
            fields.filter      = filter;
            fields.rest        = rest;
            return fields;
        }

      private:
        using Options = std::array<std::optional<std::string>, 3>;

        static ResultP<Options> parseOptions(std::string_view input, Position position)
        {
            Options results{};
            int     values{0};
            bool    run{true};

            while (not input.empty() && run)
            {
                switch (input.front())
                {
                    case '<':
                        if (results[0])
                        {
                            run = false;
                            break;
                        }

                        if (auto result = transaction_parser(input, position))
                        {
                            results[0] = result->value;
                            input      = result->input;
                            position   = result->position;
                            values++;
                            run = values != 3;
                        }
                        else
                        {
                            return utils::Failure(result.error());
                        }
                        break;

                    case '(':
                        if (results[1])
                        {
                            run = false;
                            break;
                        }

                        if (auto result = user_parser(input, position))
                        {
                            results[1] = result->value;
                            input      = result->input;
                            position   = result->position;
                            values++;
                            run = values != 3;
                        }
                        else
                        {
                            return utils::Failure(result.error());
                        }
                        break;

                    case '[':
                        if (results[2])
                        {
                            run = false;
                            break;
                        }

                        if (auto result = filter_parser(input, position))
                        {
                            results[2] = result->value;
                            input      = result->input;
                            position   = result->position;
                            values++;
                            run = values != 3;
                        }
                        else
                        {
                            return utils::Failure(result.error());
                        }
                        break;

                    default:
                    {
                        run = false;
                    }
                }
            }

            return {{
                results,
                input,
                position,
            }};
        };
    };
}

// EOF
