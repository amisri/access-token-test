//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <atomic>
#include <cstdint>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <string_view>
#include <utility>

#include <cpp_utils/circularBuffer.h>
#include <interfaces/ICommandDistributor.h>
#include <interfaces/IWebsocket.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/Thread.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>
#include <modules/devices/base/inc/Response.h>
#include "CommandParser.h"
#include "ContextManager.h"

// **********************************************************

namespace fgcd::wefserver
{
    enum class MessageType
    {
        init,
        get,
        getex,
        set,
        setex,
        sub,
        unsub,
        clear,
        term,
        invalid
    };

    // clang-format off
    NLOHMANN_JSON_SERIALIZE_ENUM(
        MessageType,
        {
            {MessageType::invalid, "INVALID"},
            {MessageType::init,    "INIT"},
            {MessageType::get,     "GET"},
            {MessageType::getex,   "GETEX"},
            {MessageType::set,     "SET"},
            {MessageType::setex,   "SETEX"},
            {MessageType::sub,     "SUB"},
            {MessageType::clear,   "CLEAR"},
            {MessageType::unsub,   "UNSUB"},
            {MessageType::term,    "TERM"},
        }
    )
    // clang-format on

    struct InitRequest
    {
        std::string credentials;
        std::string options;
        std::string version;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(InitRequest, credentials, options, version);

    struct GetRequest
    {
        std::string device;
        std::string command;
        std::string tag;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(GetRequest, device, command, tag);

    struct GetExRequest
    {
        std::string   device;
        CommandFields fields;
        std::string   tag;
    };

    inline void from_json(const Json& data, GetExRequest& request)
    {
        request.device             = data.at("device");
        request.fields.property    = data.at("property");
        request.fields.selector    = data.at("selector");
        request.fields.transaction = data.at("transaction");
        request.fields.filter      = data.at("filter");
        request.fields.rest        = data.at("rest");
        request.tag                = data.at("tag");
    }

    struct SetRequest
    {
        std::string device;
        std::string command;
        std::string tag;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetRequest, device, command, tag);

    struct SetExRequest
    {
        std::string   device;
        CommandFields fields;
        std::string   tag;
    };

    inline void from_json(const Json& data, SetExRequest& request)
    {
        request.device             = data.at("device");
        request.fields.property    = data.at("property");
        request.fields.selector    = data.at("selector");
        request.fields.transaction = data.at("transaction");
        request.fields.filter      = data.at("filter");
        request.fields.rest        = data.at("rest");
        request.tag                = data.at("tag");
    }

    struct SubRequest
    {
        std::string device;
        std::string command;
        std::string tag;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SubRequest, device, command, tag);

    struct UnsubRequest
    {
        std::string tag;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(UnsubRequest, tag);

    struct TermRequest
    {
        std::string device;
        std::string term_char;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TermRequest, device, term_char);
}

namespace fgcd
{
    class WefServer;
    using WefServerPtr = std::shared_ptr<WefServer>;

    class WefServer : public Component
    {
        enum class Status
        {
            OK           = 200,
            BAD_REQUEST  = 400,
            UNAUTHORIZED = 401,
            FORBIDDEN    = 403,
            NOT_FOUND    = 404,
        };

        using CommandSpecific = wefserver::Connection;

        static constexpr auto version        = "0.9.1";
        static constexpr auto command_source = "wefserver";

      public:
        WefServer(Component& parent, IWebsocketPtr websocket, ICommandDistributorPtr distributor);

      private:
        void onStart() override;
        void responseThread(std::atomic_bool& keep_running);

        bool validateMessage(ConnectionHandle handle, const Json& payload);
        void processMessage(ConnectionHandle handle, const Json& payload);
        void processInit(ConnectionHandle handle, const Json& payload);
        void processGet(ConnectionHandle handle, const Json& payload);
        void processGetEx(ConnectionHandle handle, const Json& payload);
        void processSet(ConnectionHandle handle, const Json& payload);
        void processSetEx(ConnectionHandle handle, const Json& payload);
        void processSub(ConnectionHandle handle, const Json& payload);
        void processUnsub(ConnectionHandle handle, const Json& payload);
        void processClear(ConnectionHandle handle, const Json& payload);
        void processTerm(ConnectionHandle handle, const Json& payload);

        void sendErrorResponse(ConnectionHandle handle, std::string_view tag, std::string_view error, Status status);
        void closeConnection(ConnectionHandle handle);

        Json serializeResponse(const Command& command);

        CommandPtr createCommand(
            const ConnectionHandle& handle, CommandType type, const std::string& device, const std::string& tag,
            const CommandFields& fields
        );

        IWebsocketPtr          m_websocket;
        ICommandDistributorPtr m_distributor;
        Thread                 m_response_thread;

        wefserver::ContextManager m_context;
        ResponseQueuePtr          m_response_queue;
    };
}

// EOF
