//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <fmt/chrono.h>
#include <fmt/ostream.h>
#include <functional>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <cpp_utils/Result.h>
#include <cpp_utils/typeTraits.h>
#include <modules/base/core/inc/types.h>

template<typename T>
class WWW;

namespace fgcd
{

    // **********************************************************
    // Types Definitions
    // **********************************************************

    using Position = std::size_t;

    struct ParserError
    {
        Position                 position;
        std::string              unexpected;
        std::vector<std::string> expected;

        bool operator==(const ParserError&) const = default;
    };

    template<typename T>
    struct ParserState
    {
        using value_type = T;

        value_type       value;
        std::string_view input;
        Position         position{};

        bool operator==(const ParserState&) const = default;
    };

    template<typename T>
    ParserState(T, std::string_view, Position) -> ParserState<T>;

    template<typename T>
    using ResultP = utils::Result<ParserState<T>, ParserError>;

    template<typename T>
    using Parser = std::function<ResultP<T>(std::string_view, Position)>;

    namespace details
    {
        template <typename T>
        struct IsResultP : std::false_type {};

        template <typename T>
        struct IsResultP<ResultP<T>> : std::true_type {};

        template <typename T>
        concept CIsResultP  = IsResultP<T>::value;
    }

    // clang-format off
    template<typename P>
    concept CParser = std::regular_invocable<P, std::string_view, Position>
        && requires(P parser, std::string_view input, Position postion)
        {
               { parser(input, postion) } -> details::CIsResultP;
        };
    // clang-format on

    template<CParser P>
    using ParserR = typename std::invoke_result_t<P, std::string_view, Position>;

    template<CParser P>
    using ParserV = typename ParserR<P>::value_type::value_type;

    template<typename F, typename... Args>
    concept ParserCombinator = std::regular_invocable<F, Args...> &&   //
        CParser<std::invoke_result_t<F, Args...>>;

    template<typename F, typename... Args>
        requires ParserCombinator<F, Args...>
    using ParserCombinatorValue = std::invoke_result_t<F, Args...>;
}

// **********************************************************

template<>
struct fmt::formatter<fgcd::ParserError>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
    template<typename FormatContext>
    auto format(const fgcd::ParserError error, FormatContext& ctx)
    {
        return fmt::format_to(
            ctx.out(),                                                                            //
            "(position: {}, unexpected: '{}', expected: {})", error.position, error.unexpected,   //
            fmt::join(error.expected, " or ")                                                     //
        );
    }
};

// **********************************************************

template<typename T>
struct fmt::formatter<fgcd::ParserState<T>>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
    template<typename FormatContext>
    auto format(const fgcd::ParserState<T> state, FormatContext& ctx)
    {
        return fmt::format_to(
            ctx.out(), "(parsed = '{}', unparsed = '{}', position = {})", state.value, state.input, state.position
        );
    }
};


namespace fgcd
{
    // **********************************************************
    // Core parsers
    // **********************************************************

    template<typename T>
    constexpr CParser auto pure(T const& value)
    {
        return [value](std::string_view input, Position position) -> ResultP<T>
        {
            return {{value, input, position}};
        };
    }

    inline ResultP<std::string_view> emptyP(std::string_view input, Position position)
    {
        if (input.empty())
        {
            return {{
                "",
                input,
                position,
            }};
        }

        return utils::Failure{ParserError{position, std::string(input), {"eof"}}};
    }

    constexpr CParser auto stringP(std::string_view match)
    {
        return [match](std::string_view input, Position position) -> ResultP<std::string_view>
        {
            if (input.starts_with(match))
            {
                return {{
                    input.substr(0, match.size()),
                    input.substr(match.size()),
                    position + match.size(),
                }};
            }

            return utils::Failure{ParserError{position, std::string(input), {fmt::format("'{}'", match)}}};
        };
    }

    constexpr CParser auto charP(char c)
    {
        return [c](std::string_view input, Position position) -> ResultP<char>
        {
            if (input.empty())
            {
                return utils::Failure{ParserError{position, std::string(input), {fmt::format("'{}'", c)}}};
            }

            if (input.front() == c)
            {
                return {{
                    input.front(),
                    input.substr(1),
                    position + 1,
                }};
            }

            return utils::Failure{ParserError{position, std::string(input.data(), 1), {fmt::format("'{}'", c)}}};
        };
    }

    constexpr CParser auto manyP(std::predicate<char> auto predicate)
    {
        return [predicate](std::string_view input, Position position) -> ResultP<std::string_view>
        {
            auto i = 0U;
            for (; i < input.size(); i++)
            {
                if (not predicate(input[i]))
                {
                    break;
                }
            }

            return {{
                input.substr(0, i),
                input.substr(i),
                position + i,
            }};
        };
    }

    constexpr CParser auto many1P(std::predicate<char> auto p, std::string_view label)
    {
        return [p, label](std::string_view input, Position position) -> ResultP<std::string_view>
        {
            auto i = 0U;
            for (; i < input.size(); i++)
            {
                if (not p(input[i]))
                {
                    break;
                }
            }

            if (i == 0)
            {
                return utils::Failure{ParserError{
                    position,
                    std::string(input.data() + i, 1),
                    {std::string(label)},
                }};
            }

            return {{
                input.substr(0, i),
                input.substr(i),
                position + i,
            }};
        };
    }

    template<CParser P, CParser Q>
        requires std::convertible_to<ParserV<P>, ParserV<Q>>
    constexpr CParser auto operator|(P p, Q q)
    {
        return [=](std::string_view input, Position position) -> ParserR<Q>
        {
            auto r1 = p(input, position);
            if (r1.hasError())
            {
                auto r2 = q(input, position);
                if (r2.hasError())
                {
                    return utils::Failure{ParserError{
                        position,
                        std::string(input),
                        r1.error().expected + r2.error().expected,
                    }};
                }

                return r2;
            }
            return r1;
        };
    }

    template<CParser P, CParser Q>
    constexpr CParser auto operator>>(P p, Q q)
    {
        return [=](std::string_view input, Position position) -> ParserR<Q>
        {
            auto result = p(input, position);
            if (result.hasValue())
            {
                return q(result->input, result->position);
            }

            return utils::Failure{result.error()};
        };
    }

    template<CParser P, CParser Q>
    constexpr CParser auto operator<<(P p, Q q)
    {
        return [=](std::string_view input, Position position) -> ParserR<P>
        {
            auto r1 = p(input, position);
            if (r1.hasValue())
            {
                auto r2 = q(r1->input, r1->position);
                if (r2.hasValue())
                {
                    return {{r1->value, r2->input, r2->position}};
                }

                return utils::Failure{r2.error()};
            }

            return utils::Failure{r1.error()};
        };
    }

    template <typename... Ts, typename U>
    decltype(auto) joinValues(const std::tuple<Ts...>& ts, const U& u)
    {
        return std::tuple_cat(ts, std::tuple(u));
    }

    template <typename... Ts, typename U>
    decltype(auto) joinValues(const U& u, const std::tuple<Ts...>& ts)
    {
        return std::tuple_cat(ts, std::tuple(u));
    }

    template <typename T, typename U>
    decltype(auto) joinValues(const T& t, const U& u)
    {
        return std::tuple(t, u);
    }

    template<CParser P, CParser Q>
    constexpr CParser auto operator&(P p, Q q)
    {
        using R1         = ParserV<P>;
        using R2         = ParserV<Q>;
        using ResultType = decltype(joinValues(std::declval<R1>(), std::declval<R2>()));
        return [=](std::string_view input, Position position) -> ResultP<ResultType>
        {
            auto pr = p(input, position);
            if (pr.hasValue())
            {
                auto qr = q(pr->input, pr->position);
                if (qr.hasValue())
                {
                    return {{
                        joinValues(pr->value, qr->value),
                        qr->input,
                        qr->position,
                    }};
                }

                return utils::Failure{qr.error()};
            }

            return utils::Failure{pr.error()};
        };
    }
}

// EOF
