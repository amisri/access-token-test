//! @file
//! @brief  A wrapper-class (around Websocketpp library) to create a Websocket server.
//! @author Dariusz Zielinski

#pragma once

#include <atomic>
#include <functional>
#include <json/json.hpp>
#include <map>
#include <memory>
#include <mutex>

#include <modules/base/component/inc/Component.h>
#include <modules/base/thread/inc/Thread.h>
#include <interfaces/IWebsocket.h>

namespace websocketpp
{
    // Forward declarations
    template<typename T>
    class server;

    namespace config
    {
        struct asio;
    }
};

namespace fgcd
{
    using WebsocketClient = std::weak_ptr<void>;

    class Websocket : public Component, public IWebsocket
    {
        using Server    = websocketpp::server<websocketpp::config::asio>;

      public:
        Websocket(Component& parent, uint16_t port);

        ~Websocket() override;

        void sendMessage(ConnectionHandle handle, const Json& payload) override;
        void setMessageHandler(MessageHandler handler) override;
        void setOpenHandler(const StateHandler& handler) override;
        void setCloseHandler(const StateHandler& handler) override;
        void setValidateHandler(const ValidateHandler& handler) override;
        void broadcastMessage(const Json& data) override;

      protected:
        void onStart() override;
        void onStop() override;

      private:
        void onMessage(WebsocketClient client, std::string_view message);

        Thread           m_thread;
        std::atomic_bool m_is_running = false;
        uint16_t         m_port       = 0;

        StateHandler    m_open_handler     = nullptr;
        StateHandler    m_close_handler    = nullptr;
        ValidateHandler m_validate_handler = nullptr;

        std::mutex                            m_handlers_mutex;
        MessageHandler                        m_handler;
        ClientSet                             m_clients;

        std::unique_ptr<Server> m_server;
    };
}

// EOF
