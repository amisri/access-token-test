//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <set>
#include <string>
#include <vector>

#include <cpp_utils/Result.h>
#include <interfaces/ICommandDistributor.h>
#include <interfaces/IWebsocket.h>
#include <modules/base/component/inc/Component.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>

namespace fgcd::wefserver
{
    struct Connection
    {
        ConnectionHandle handle;
        std::uint32_t    user;
    };

    struct ConnectionContext
    {
        Connection              connection;
        std::vector<CommandPtr> closures;
    };

    class ContextManager
    {
      public:
        void               createConnection(ConnectionHandle handle);
        void               closeConnection(std::uint32_t user);
        bool               pushClosure(const ConnectionHandle& handle, CommandPtr command);
        CommandPtr         popClosure(const ConnectionHandle& handle, const std::string& tag);
        ConnectionContext* getContext(const ConnectionHandle& handle);
        [[nodiscard]] bool hasConnection(const Connection& connection) const;
        [[nodiscard]] bool hasConnection(const ConnectionHandle& handle) const;

      private:
        std::uint32_t getId();

        static bool handleEquals(const ConnectionHandle& h1, const ConnectionHandle& h2);

        std::vector<ConnectionContext> m_connections;
        std::set<std::uint32_t>        m_active_users;
        std::uint32_t                  m_last_id = 0;
    };
}

// EOF
