#include <algorithm>
#include <fmt/ranges.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <optional>

#include <modules/base/misc/inc/FmtFormatters.h>
#include <utils/test/printers/Printers.h>
#include "../inc/CommandParser.h"
#include "../inc/ParserCombinator.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

using ParserResult = utils::Result<CommandFields, std::string>;
using Parameters = std::tuple<std::string, ParserResult>;

struct CommandParserTests : public ::testing::TestWithParam<Parameters>
{
    CommandParserTests()
    {
        std::tie(input, expected) = GetParam();
    }

    std::string  input;
    ParserResult expected;
};

// **********************************************************
// Tests
// **********************************************************

TEST_P(CommandParserTests, test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution
    auto result = CommandParser::parse(input);


    // **********************************************************
    // Verification
    EXPECT_EQ(result, expected);
}

// clang-format off
std::vector<Parameters> test_paramters = {
    {
        "FOO<1>[2](3) BAR",
        CommandFields{.property = "FOO", .selector = "3", .transaction = "1", .filter = "2", .rest = "BAR"}
    },
    {
        "<1>[2](3) BAR",
        utils::Failure{"(position: 0, unexpected: '<', expected: alnum, '.' or '_')"}
    },
    {
        ".<1>[2](3) BAR",
        CommandFields{.property = ".", .selector = "3", .transaction = "1", .filter = "2", .rest = "BAR"}
    },
    {
        "FOO[2](3) BAR",
        CommandFields{.property = "FOO", .selector = "3", .transaction = std::nullopt, .filter = "2", .rest = "BAR"}
    },
    {
        "FOO%[2](3) BAR",
        utils::Failure{"(position: 3, unexpected: '%[2](3) BAR', expected: ' ' or eof)"}
    },
    {
        "FOO[2][2](3) BAR",
        utils::Failure{"(position: 6, unexpected: '[2](3) BAR', expected: ' ' or eof)"}
    },
    {
        "FOO BAR",
        CommandFields{.property = "FOO", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = "BAR"}
    },
    {
        "FOO.FIZ BAR",
        CommandFields{.property = "FOO.FIZ", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = "BAR"}
    },
    {
        "FOO.FIZ() BAR",
        CommandFields{.property = "FOO.FIZ", .selector = "", .transaction = std::nullopt, .filter = std::nullopt, .rest = "BAR"}
    },
    {
        "FOO",
        CommandFields{.property = "FOO", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "ref.pulse.amplitude",
        CommandFields{.property = "ref.pulse.amplitude", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF_PULSE_AMPLITUDE",
        CommandFields{.property = "REF_PULSE_AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "Abc.XYZ_1..._QWE_",
        CommandFields{.property = "Abc.XYZ_1..._QWE_", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE(10)",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "10", .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE(99999)",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "99999", .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE(SPS.USER.MD5)",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.USER.MD5", .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE(SPS.___.MD5)",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE()",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "", .transaction = std::nullopt, .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE<10>",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = "10", .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE<99999>",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = "99999", .filter = std::nullopt, .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "  Abc_._123!@#$%^&()<>  ", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[<10>]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "<10>", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[<>]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "<>", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[(10)]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "(10)", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[(SPS.USER.MD5)]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "(SPS.USER.MD5)", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE[[[[]",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = "[[[", .rest = ""}
    },
    {
        "REF.PULSE.AMPLITUDE BIN",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = "BIN"}
    },
    {
        "REF.PULSE.AMPLITUDE BIN INFO TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = std::nullopt, .transaction = std::nullopt, .filter = std::nullopt, .rest = "BIN INFO TYPE"}
    },
    {
        "REF.PULSE.AMPLITUDE(SPS.___.MD5)<99999>[  Abc_._123!@#$%^&()<>  ]  BIN TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = "99999", .filter = "  Abc_._123!@#$%^&()<>  ", .rest = "BIN TYPE"}
    },
    {
        "REF.PULSE.AMPLITUDE(SPS.___.MD5)[  Abc_._123!@#$%^&()<>  ]<99999>  BIN TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = "99999", .filter = "  Abc_._123!@#$%^&()<>  ", .rest = "BIN TYPE"}
    },
    {
        "REF.PULSE.AMPLITUDE<99999>(SPS.___.MD5)[  Abc_._123!@#$%^&()<>  ]  BIN TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = "99999", .filter = "  Abc_._123!@#$%^&()<>  ", .rest = "BIN TYPE"}
    },
    {
        "REF.PULSE.AMPLITUDE<99999>[  Abc_._123!@#$%^&()<>  ](SPS.___.MD5)  BIN TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = "99999", .filter = "  Abc_._123!@#$%^&()<>  ", .rest = "BIN TYPE"}
    },
    {
        "REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ]<99999>(SPS.___.MD5)  BIN TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = "99999", .filter = "  Abc_._123!@#$%^&()<>  ", .rest = "BIN TYPE"}
    },
    {
        "REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ](SPS.___.MD5)<99999>  BIN TYPE",
        CommandFields{.property = "REF.PULSE.AMPLITUDE", .selector = "SPS.___.MD5", .transaction = "99999", .filter = "  Abc_._123!@#$%^&()<>  ", .rest = "BIN TYPE"}
    },
};

INSTANTIATE_TEST_SUITE_P(
    CommandParserTestsSuite,
    CommandParserTests,
    testing::ValuesIn(test_paramters)
);
// clang-format on

// EOF
