// #include <gtest/gtest.h>
// #include <memory>
// #include <thread>
// #include <vector>

// #include <modules/base/core/inc/types.h>
// #include <utils/test/mocks/RootMock.h>
// #include "../inc/WefServer.h"

// using namespace fgcd;

// // **********************************************************
// // Setup
// // **********************************************************

// struct WebsocketMock : public IWebsocket
// {
//     MessageHandler    m_handler;
//     std::vector<Json> m_responses;

//     void sendMessage(Json payload) override
//     {
//         m_responses.push_back(std::move(payload));
//     }

//     void setMessageHandler(MessageHandler handler) override
//     {
//         m_handler = std::move(handler);
//     }

//     void receiveMessage(const Json& payload)
//     {
//         if (m_handler)
//         {
//             m_handler({}, payload);
//         }
//     }
// };

// struct WefServerTests : public ::testing::Test
// {
//     RootMockPtr root;
//     WefServerPtr server;
//     std::shared_ptr<WebsocketMock> websocket;


//     void SetUp() override
//     {
//         root = std::make_unique<RootMock>();
//         websocket = std::make_shared<WebsocketMock>();
//         server = root->addComponent<WefServer>(websocket);

//         root->enableLogging();
//         root->start();
//     }

//     void TearDown() override
//     {
//         root->stop();
//     }
// };

// // **********************************************************
// // Tests
// // **********************************************************

// TEST_F(WefServerTests, badInitRequest)
// {
//     // **********************************************************
//     // Preparation


//     // **********************************************************
//     // Execution
//     websocket->receiveMessage({
//         {"type", "INIT"}   //
//     });


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(websocket->m_responses.size(), 1);
//     EXPECT_EQ(websocket->m_responses[0]["type"], "INIT");
//     EXPECT_NE(websocket->m_responses[0]["error"], "");
//     EXPECT_EQ(websocket->m_responses[0]["status"], 400);
// }

// // **********************************************************

// TEST_F(WefServerTests, goodInitRequest)
// {
//     // **********************************************************
//     // Preparation


//     // **********************************************************
//     // Execution
//     websocket->receiveMessage({
//         {"type", "INIT"}   //
//     });


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(websocket->m_responses.size(), 1);
//     EXPECT_EQ(websocket->m_responses[0]["type"], "INIT");
//     EXPECT_NE(websocket->m_responses[0]["error"], "");
//     EXPECT_EQ(websocket->m_responses[0]["status"], 400);
// }

// // **********************************************************

// TEST_F(WefServerTests, unathorizedGet)
// {
//     // **********************************************************
//     // Preparation


//     // **********************************************************
//     // Execution
//     websocket->receiveMessage({
//         {"type", "GET"}   //
//     });


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(websocket->m_responses.size(), 1);
//     EXPECT_EQ(websocket->m_responses[0]["type"], "RESPONSE");
//     EXPECT_EQ(websocket->m_responses[0]["error"], "");
//     EXPECT_EQ(websocket->m_responses[0]["status"], 401);
// }

// // **********************************************************

// TEST_F(WefServerTests, badGetRequest)
// {
//     // **********************************************************
//     // Preparation


//     // **********************************************************
//     // Execution
//     websocket->receiveMessage({
//         {"type", "INIT"},
//         {"credentials", ""},
//         {"options", {}},
//         {"version", ""},

//     });
//     websocket->receiveMessage({
//         {"type", "GET"}   //
//     });


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(websocket->m_responses.size(), 2);
//     EXPECT_EQ(websocket->m_responses[1]["type"], "RESPONSE");
//     // EXPECT_EQ(websocket->m_responses[1]["error"], ""s);
//     EXPECT_EQ(websocket->m_responses[1]["status"], 400);
// }

// // EOF
