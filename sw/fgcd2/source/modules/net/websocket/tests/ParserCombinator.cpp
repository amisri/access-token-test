#include <cctype>
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>
#include <gtest/gtest.h>
#include <string_view>

#include <utils/test/printers/Printers.h>
#include "../inc/ParserCombinator.h"

using namespace fgcd;
using namespace std::literals;

// **********************************************************
// Setup
// **********************************************************

// **********************************************************
// Tests
// **********************************************************

TEST(ParserCombinatorTests, stringP)
{
    auto parser = stringP("FOO");

    {
        auto result   = parser("FOOO", 0);
        auto expected = ParserState{"FOO"sv, "O"sv, 3};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("TEST", 0);
        auto expected = utils::Failure{ParserError{0, "TEST", {"'FOO'"}}};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, charP)
{
    auto parser = charP('!');

    {
        auto result   = parser("!GET", 0);
        auto expected = ParserState{'!', "GET"sv, 1};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("GET", 0);
        auto expected = utils::Failure{ParserError{0, "G", {"'!'"}}};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, parseAlternative)
{
    auto parser = stringP("FOO") | stringP("BAR");

    {
        auto result   = parser("FOOA", 0);
        auto expected = ParserState{"FOO"sv, "A"sv, 3};


        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("BARA", 0);
        auto expected = ParserState{"BAR"sv, "A"sv, 3};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("FOBAR", 0);
        auto expected = utils::Failure{ParserError{0, "FOBAR", {"'FOO'", "'BAR'"}}};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, manyP)
{
    auto is_plus = [](char c)
    {
        return c == '+';
    };
    auto parser = manyP(is_plus);

    {
        auto result   = parser("+++---", 0);
        auto expected = ParserState{"+++"sv, "---"sv, 3};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("---", 0);
        auto expected = ParserState{""sv, "---"sv, 0};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, many1P)
{
    auto is_plus = [](char c)
    {
        return c == '+';
    };
    auto parser = many1P(is_plus, "isPlus");

    {
        auto result   = parser("+++---", 0);
        auto expected = ParserState{"+++"sv, "---"sv, 3};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("----", 0);
        auto expected = utils::Failure{ParserError{0, "-", {"isPlus"}}};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, discardLeft)
{
    auto parser = charP('!') >> stringP("foo");

    {
        auto result   = parser("!foobaz", 0);
        auto expected = ParserState{"foo"sv, "baz"sv, 4};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("!foobaz", 0);
        auto expected = ParserState{"foo"sv, "baz"sv, 4};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("foo", 0);
        auto expected = utils::Failure{ParserError{0, "f", {"'!'"}}};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("! foo", 0);
        auto expected = utils::Failure{ParserError{1, " foo", {"'foo'"}}};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, discardRight)
{
    auto parser = stringP("foo") << charP(';');

    {
        auto result   = parser("foo;baz", 0);
        auto expected = ParserState{"foo"sv, "baz"sv, 4};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("fo;", 0);
        auto expected = utils::Failure{ParserError{0, "fo;", {"'foo'"}}};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("foo", 0);
        auto expected = utils::Failure{ParserError{3, "", {"';'"}}};

        EXPECT_EQ(result, expected);
    }
}

// **********************************************************

TEST(ParserCombinatorTests, parseSum)
{
    auto parser = many1P(::isdigit, "digit") & many1P(::isalpha, "alphabetic") & many1P(::isdigit, "digit");

    {
        auto result   = parser("123test123", 0);
        auto expected = ParserState{std::make_tuple("123"sv, "test"sv, "123"sv), ""sv, 10};

        EXPECT_EQ(result, expected);
    }

    {
        auto result   = parser("123test<", 0);
        auto expected = utils::Failure{ParserError{7, "<", {"digit"}}};

        EXPECT_EQ(result, expected);
    }
}

// EOF
