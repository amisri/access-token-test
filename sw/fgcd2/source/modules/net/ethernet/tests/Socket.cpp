#include <chrono>
#include <cstdint>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

#include <modules/base/core/inc/ErrnoException.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <utils/test/mocks/RootMock.h>
#include <utils/test/utils/utils.h>
#include "../inc/EthernetHeader.h"
#include "../inc/Socket.h"

using namespace fgcd;
using namespace std::literals::chrono_literals;

using ::testing::ElementsAreArray;

// **********************************************************
// Setup
// **********************************************************

struct Result
{
    ByteStreamView payload;
    MacAddress     source;
};

struct SocketTestsFixture : public ::testing::Test
{
    static constexpr std::uint16_t ethernet_type = 0x1122;

    RootMockPtr         root;
    SocketPtr           socket_send;
    SocketPtr           socket_recv;
    MacAddress          socket_send_mac = MacAddress({02, 00, 00, 00, 00, 02});
    MacAddress          socket_recv_mac = MacAddress({02, 00, 00, 00, 00, 03});
    std::vector<Result> received_packets;

    void SetUp() override
    {
        root        = std::make_unique<RootMock>();
        socket_send = root->addComponent<Socket>("veth0", ethernet_type);
        socket_recv = root->addComponent<Socket>("veth1", ethernet_type);

        socket_recv->setListener(
            [this](BigReader payload, MacAddress source)
            {
                received_packets.emplace_back(payload.buffer(), source);
            }
        );

        root->enableLogging();
        root->start();
    }

    void TearDown() override
    {
        root->stop();
    }

    static void SetUpTestSuite()
    {
        std::system("ip link delete veth0");

        // Create virtual interfaces
        std::system("ip link add veth0 type veth peer name veth1");

        std::system("ip link set dev veth0 address 02:00:00:00:00:02");
        std::system("ip link set dev veth1 address 02:00:00:00:00:03");

        // Bring interfaces up
        std::system("ip link set dev veth0 up");
        std::system("ip link set dev veth1 up");
    }

    static void TearDownTestSuite()
    {
        std::system("ip link delete veth0");
    }

    void joinThreads()
    {
        std::this_thread::sleep_for(1ms);
        root->stop();
    }
};


// **********************************************************
// TESTS

TEST_F(SocketTestsFixture, noPacktSent)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_EQ(received_packets.size(), 0);
}

// **********************************************************

TEST_F(SocketTestsFixture, getFrameBroadcast)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution
    auto frame = socket_send->getFrame();
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_BINARY_EQ(
        frame.buffer(),
        ByteStream({
            0xFF_b, 0xFF_b, 0xFF_b, 0xFF_b, 0xFF_b, 0xFF_b,   //
            0x02_b, 0x00_b, 0x00_b, 0x00_b, 0x00_b, 0x02_b,   //
            0x11_b, 0x22_b                                    //
        })
    );
}

// **********************************************************

TEST_F(SocketTestsFixture, getFrameMacAddress)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution
    auto frame = socket_send->getFrame(socket_recv_mac);
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_BINARY_EQ(
        frame.buffer(),
        ByteStream({
            0x02_b, 0x00_b, 0x00_b, 0x00_b, 0x00_b, 0x03_b,   //
            0x02_b, 0x00_b, 0x00_b, 0x00_b, 0x00_b, 0x02_b,   //
            0x11_b, 0x22_b                                    //
        })
    );
}

// **********************************************************

TEST_F(SocketTestsFixture, skipWrongAddressPacket)
{
    // **********************************************************
    // Preparation
    auto frame = socket_send->getFrame(MacAddress({1, 2, 3, 4, 5, 6}));

    // **********************************************************
    // Execution
    socket_send->send(frame);
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_EQ(received_packets.size(), 0);
}

// **********************************************************

TEST_F(SocketTestsFixture, sendProperMacAddress)
{
    // **********************************************************
    // Preparation
    auto frame = socket_send->getFrame(socket_recv_mac);

    // **********************************************************
    // Execution
    socket_send->send(frame);
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_EQ(received_packets.size(), 1);
}

// **********************************************************

TEST_F(SocketTestsFixture, sendWithPayload)
{
    // **********************************************************
    // Preparation
    auto payload = ByteStream(50, 0x77_b);
    auto frame   = socket_send->getFrame(socket_recv_mac);


    // **********************************************************
    // Execution
    frame.write(payload);
    socket_send->send(frame);
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_EQ(received_packets.size(), 1);
    EXPECT_EQ(received_packets[0].source, socket_send_mac);
    EXPECT_BINARY_EQ(received_packets[0].payload, payload);
}

// **********************************************************

TEST_F(SocketTestsFixture, sendMultiplePackets)
{
    // **********************************************************
    // Preparation
    auto payload_0 = ByteStream(50, 0x77_b);
    auto payload_n = ByteStream(1000, 0xAA_b);
    auto frame_0   = socket_send->getFrame(socket_recv_mac);
    auto frame_n   = socket_send->getFrame(socket_recv_mac);
    frame_0.write(payload_0);
    frame_n.write(payload_n);


    // **********************************************************
    // Execution
    1000_times(
        [&]()
        {
            socket_send->send(frame_0);
        }
    );
    socket_send->send(frame_n);
    joinThreads();


    // **********************************************************
    // Verification
    ASSERT_EQ(received_packets.size(), 1001);
    EXPECT_BINARY_EQ(received_packets[402].payload, payload_0);
    EXPECT_BINARY_EQ(received_packets[1000].payload, payload_n);
}

// **********************************************************

TEST_F(SocketTestsFixture, wrongInterfaceName)
{
    // **********************************************************
    // Verification
    EXPECT_THROW(root->addComponent<Socket>("test123", 0x2233), fgcd::ErrnoException);
}

// // EOF
