//! @file
//! @brief  
//! @author Adam Solawa

#include <memory>
#include <utility>

#include <cpp_utils/bits.h>
#include <modules/base/core/inc/types.h>
#include "../inc/RawFrame.h"

using namespace fgcd;

// **********************************************************

RawFrame::RawFrame(ByteSpan bytes)
    : m_buffer(bytes),
      // Create object, to access underlying element without undefined behavior
      m_header(new(m_buffer.data()) tpacket_hdr)
{
}

// **********************************************************

RawFrame::RawFrame(RawFrame&& other) noexcept
    : m_buffer(std::exchange(other.m_buffer, ByteSpan())),
      m_header(std::exchange(other.m_header, nullptr))
{
}

// **********************************************************

RawFrame::~RawFrame()
{
    // Explicit deletion of object because it was manually created
    std::destroy_at(m_header);
}

// **********************************************************

bool RawFrame::ready() const
{
    return utils::bit::vIsOne(m_header->tp_status, TP_STATUS_USER);
}

// **********************************************************

bool RawFrame::losing() const
{
    return utils::bit::vIsOne(m_header->tp_status, TP_STATUS_LOSING);
}

// **********************************************************

void RawFrame::release()
{
    m_header->tp_status = TP_STATUS_KERNEL;
}

// **********************************************************

ByteStreamView RawFrame::getPayload() const
{
    return {m_buffer.data() + m_header->tp_mac, m_header->tp_len};
}

// EOF
