//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#include <array>
#include <unistd.h>
#include <fmt/format.h>

#include <modules/base/core/inc/Exception.h>
#include "../inc/Pipe.h"

using namespace fgcd;


// // **********************************************************

Pipe::Pipe()
{
    std::array<int, 2> fds = {};

    if (pipe(fds.data()) != 0)
    {
        throw Exception(fmt::format("Pipe creation failed: '{}'", strerror(errno)));
    }

    m_read_fd  = fds[0];
    m_write_fd = fds[1];
}

Pipe::~Pipe()
{
    close(m_read_fd);
    close(m_write_fd);
}

bool Pipe::write(std::string_view message) const
{
    return ::write(m_write_fd, message.data(), message.size()) >= 0;
}

int Pipe::readFd() const
{
    return m_read_fd; 
}

// EOF
