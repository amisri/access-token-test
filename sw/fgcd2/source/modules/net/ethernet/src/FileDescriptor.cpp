//! @file
//! @brief  
//! @author Adam Solawa

#include <unistd.h>

#include "../inc/FileDescriptor.h"
#include "modules/base/core/inc/Exception.h"

using namespace fgcd;

// **********************************************************

FileDescriptor::FileDescriptor(int handle)
    : m_handle(handle)
{
    if(m_handle == null_value)
    {
        throw Exception("Fail to create file descriptor: provided null value");
    }
}

// **********************************************************

FileDescriptor::FileDescriptor(FileDescriptor&& descriptor) noexcept
    : m_handle(descriptor.m_handle)
{
    descriptor.m_handle = null_value;
}

// **********************************************************

FileDescriptor& FileDescriptor::operator=(FileDescriptor&& descriptor) noexcept
{
    m_handle = descriptor.m_handle;
    descriptor.m_handle = null_value;
    return *this;
}

// **********************************************************

FileDescriptor::~FileDescriptor()
{
    if(m_handle != null_value)
    {
        close(m_handle);
    }
}

// **********************************************************

FileDescriptor::value_type FileDescriptor::get() const
{
    return m_handle;
}

// **********************************************************

FileDescriptor::operator bool() const
{
    return m_handle != null_value;
}


// EOF
