//! @file
//! @brief  
//! @author Adam Solawa

#include <cerrno>
#include <linux/if_packet.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <modules/base/core/inc/ErrnoException.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/RingBuffer.h"

using namespace fgcd;

// **********************************************************

RingBuffer::RingBuffer(Component& parent, std::size_t frame_size, std::size_t frames_length, SocketFD socket_handle)
    : Component(parent, "RingBuffer"),
      m_socket_fd(socket_handle)
{
    auto buffer_size = frame_size * frames_length;

    tpacket_req req   = {};
    req.tp_frame_size = frame_size;      // Size of data
    req.tp_frame_nr   = frames_length;   // Total number of frames
    req.tp_block_size = buffer_size;     // Minimal size of contiguous block
    req.tp_block_nr   = 1;               // Number of blocks

    if (setsockopt(m_socket_fd, SOL_PACKET, PACKET_RX_RING, &req, sizeof(req)) != 0)
    {
        throw ErrnoException("setsockopt");
    }

    // frame size > tpacket
    auto* ptr = mmap(nullptr, buffer_size, PROT_READ | PROT_WRITE, MAP_SHARED, m_socket_fd, 0);
    if (ptr == MAP_FAILED)
    {
        throw ErrnoException("mmap");
    }

    m_buffer = ByteSpan(reinterpret_cast<std::byte*>(ptr), buffer_size);

    for (auto offset = 0U; offset < buffer_size; offset += frame_size)
    {
        m_frames.emplace_back(m_buffer.subspan(offset, frame_size));
    }

    m_current = m_frames.begin();
}

// **********************************************************

RingBuffer::~RingBuffer()
{
    munmap(m_buffer.data(), m_buffer.size());
}

// **********************************************************

ByteStreamView RingBuffer::getPayload() const
{
    pollfd poll_socket{};

    // Configure structure with options used by poll function
    poll_socket.fd      = m_socket_fd;
    poll_socket.events  = POLLIN;
    poll_socket.revents = 0;

    while (!m_current->ready())
    {
        if (poll(&poll_socket, 1, -1) == -1 && errno != EINTR)
        {
            logError("Poll: {}", strerror(errno));
            continue;
        }
    }

    if (m_current->losing())
    {
        logWarning("Losing Ethernet frames.");
    }

    return m_current->getPayload();
}

// **********************************************************

void RingBuffer::next()
{
    m_current->release();
    m_current++;
    if (m_current == m_frames.end())
    {
        m_current = m_frames.begin();
    }
}

// EOF
