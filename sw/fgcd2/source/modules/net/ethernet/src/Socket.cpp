//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#include <arpa/inet.h>
#include <array>
#include <cstddef>
#include <cstdint>
#include <fmt/format.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <poll.h>
#include <string_view>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cpp_utils/bits.h>
#include <modules/base/core/inc/ErrnoException.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/base/thread/inc/threads.h>
#include "../inc/EthernetHeader.h"
#include "../inc/FileDescriptor.h"
#include "../inc/Socket.h"

using namespace fgcd;

// ************************************************************

extern "C" unsigned int if_nametoindex(const char*);

// ************************************************************

Socket::Socket(Component& parent, std::string_view interface_name, std::uint16_t ethernet_type)
    : Component(parent, "Socket"),
      m_socket_fd(socket(AF_PACKET, SOCK_RAW, 0)),
      m_buffer(addComponent<RingBuffer>(getpagesize(), buffer_frames_length, m_socket_fd.get())),
      m_ethernet_type(ethernet_type)
{
    bindSocket(interface_name);
    setMac(interface_name);
}

// **********************************************************

void Socket::setListener(SocketListener listener)
{
    m_listener = std::move(listener);
}

// ************************************************************

void Socket::send(const EthernetFrame& frame)
{
    logTrace("Socket sends packet");

    auto buffer = frame.buffer();

    auto send_size = sendto(
        m_socket_fd.get(),                         //
        buffer.data(),                             //
        buffer.size(),                             //
        0,                                         //
        reinterpret_cast<sockaddr*>(&m_address),   //
        sizeof(m_address)
    );


    if (static_cast<std::size_t>(send_size) != buffer.size())
    {
        logError("Unable to send data: '{}'", strerror(errno));
    }
}

// **********************************************************

EthernetFrame Socket::getFrame(MacAddress destination)
{
    EthernetFrame frame{};

    auto result = frame.write(   //
        EthernetHeader{
            .destination_mac = destination,
            .source_mac      = m_mac,
            .ethernet_type   = m_ethernet_type,
        }
    );

    if (!result.hasValue())
    {
        throw Exception(fmt::format("Cannot get frame: {}", result.error()));
    }

    return frame;
}

// ************************************************************

void Socket::onStart()
{
    m_thread.start(
        threads::socket,
        [this](std::atomic_bool& keep_running)
        {
            receptionThread(keep_running);
        }
    );
}

// ************************************************************

void Socket::onStop()
{
    // Gracefully finish receiveMsg thread, without it thread would be canceled
    m_thread.stop();

    // If thread has not finished yet, forcefully stop it
    m_thread.forceJoin();
}

// ************************************************************

void Socket::receptionThread(std::atomic_bool& keep_running)
{
    while (keep_running)
    {
        auto payload = m_buffer->getPayload();
        auto frame   = BigReader(payload);
        auto header  = frame.read<EthernetHeader>();

        if (header.hasError())
        {
            logWarning("Cannot deserialize header: {}", header.error());
        }
        else if (header->destination_mac != m_mac)
        {
            logInfo(
                "Skipping ethernet frame for recipient: {:x}",   //
                fmt::join(                                       //
                    header->destination_mac.begin(),             //
                    header->destination_mac.end(),               //
                    std::string_view(" ")                        //
                )
            );
        }
        else if (m_listener)
        {
            m_listener(frame, header->source_mac);
        }
        else
        {
            logError("No listener function provided");
        }

        m_buffer->next();
    }
}

// **********************************************************

void Socket::bindSocket(std::string_view interface_name)
{
    // Get index of Ethernet interface
    auto eth_index = if_nametoindex(interface_name.data());

    if (eth_index == 0)
    {
        throw ErrnoException("if_nametoindex");
    }

    // ************************************************************
    // * Binding Ethernet interface index to the socket using link-layer socket descriptor
    // ************************************************************
    m_address.sll_family   = AF_PACKET;
    m_address.sll_halen    = ETH_ALEN;
    m_address.sll_hatype   = ARPHRD_ETHER;
    m_address.sll_pkttype  = PACKET_OTHERHOST;
    m_address.sll_protocol = utils::bit::bs(m_ethernet_type);
    m_address.sll_ifindex  = static_cast<int>(eth_index);

    if (bind(m_socket_fd.get(), reinterpret_cast<sockaddr*>(&m_address), sizeof(m_address)) < 0)
    {
        throw ErrnoException("bind");
    }
}

// **********************************************************

void Socket::setMac(std::string_view interface_name)
{
    ifreq ifreq{};

    strncpy(ifreq.ifr_name, interface_name.data(), interface_name.size());
    ifreq.ifr_name[sizeof(ifreq.ifr_name) - 1] = '\0';

    if (ioctl(m_socket_fd.get(), SIOCGIFHWADDR, &ifreq) != 0)
    {
        throw ErrnoException("ioctl");
    }

    // Save Ethernet port (self) address

    std::memcpy(&m_mac, ifreq.ifr_hwaddr.sa_data, sizeof(MacAddress));
}

// EOF
