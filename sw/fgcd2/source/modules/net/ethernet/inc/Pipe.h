//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#pragma once

#include <linux/if_packet.h>
#include <string_view>

namespace fgcd
{
    class Pipe
    {
      public:
        Pipe();
        ~Pipe();

        [[nodiscard]] bool write(std::string_view message) const;
        [[nodiscard]] int  readFd() const;

      private:
        int m_read_fd  = 0;
        int m_write_fd = 0;
    };
}

// EOF
