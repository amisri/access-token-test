//! @file
//! @brief  
//! @author Adam Solawa

#pragma once

#include <array>
#include <cstdint>

#include <modules/base/core/inc/types.h>

namespace fgcd
{
    using MacAddress                           = std::array<std::uint8_t, 6>;
    inline static auto constexpr broadcast_mac = MacAddress({0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF});

    struct EthernetHeader
    {
        MacAddress    destination_mac;
        MacAddress    source_mac;
        std::uint16_t ethernet_type;
    };
}

// EOF
