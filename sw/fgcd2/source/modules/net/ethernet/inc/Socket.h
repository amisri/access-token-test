// !@file
//! @brief  Socket class that enables to send and receive Ethernet frames.
//! @author Adam Solawa

#pragma once

#include <array>
#include <cstdint>
#include <linux/if_packet.h>
#include <memory>
#include <string_view>
#include <sys/socket.h>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/base/thread/inc/Thread.h>
#include "EthernetFrame.h"
#include "EthernetHeader.h"
#include "FileDescriptor.h"
#include "Pipe.h"
#include "RingBuffer.h"

// **********************************************************

namespace fgcd
{
    class Socket;
    using SocketPtr = std::shared_ptr<Socket>;

    class Socket : public Component
    {
        //!< Size of reception ring buffer in frames.
        static constexpr std::uint32_t buffer_frames_length = 128;

      public:
        using SocketListener = std::function<void(BigReader frame, MacAddress source)>;

        static constexpr std::uint16_t protocol_type = 0x88B5;

        //! Constructor initializes the component and opens a socket and sets up ring buffer.
        //!
        //! @param parent a parent component.
        //! @param interface_name name of the interface used for communication.
        Socket(Component& parent, std::string_view interface_name, std::uint16_t ethernet_type);

        void setListener(SocketListener listener);

        void send(const EthernetFrame& frame);

        EthernetFrame getFrame(MacAddress destination = broadcast_mac);

      private:
        //! It is called on component's start.
        //! Starts reception thread.
        void onStart() override;

        //! It is called on component's finish.
        //! Stops reception thread, closes the socket and ring buffer memory.
        void onStop() override;

        //! Handles frames receptions.
        //!
        //! @param keep_running flag that indicate if thread should run any longer.
        void receptionThread(std::atomic_bool& keep_running);

        void bindSocket(std::string_view interface_name);
        void setMac(std::string_view interface_name);

        SocketListener m_listener;         //!< Function that handles received frames.
        Thread         m_thread;           //!< Thread that handles frames reception.
        Pipe           m_pipe;             //!< Pipe that helps in graceful socket closing.
        MacAddress     m_mac       = {};   //!< Mac address of the interface.
        sockaddr_ll    m_address   = {};   //!< Address struct binding socket with an interface.
        FileDescriptor m_socket_fd = {};   //!< File descriptor of the socket.
        RingBufferPtr  m_buffer;
        std::uint16_t  m_ethernet_type;
    };
}

// EOF
