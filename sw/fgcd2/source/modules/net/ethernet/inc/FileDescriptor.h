//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include "FileDescriptor.h"

// **********************************************************

namespace fgcd
{
    class FileDescriptor
    {
        static constexpr auto null_value = -1;

      public:
        using value_type = int;

        FileDescriptor(int handle = null_value);
        FileDescriptor(const FileDescriptor&)            = delete;
        FileDescriptor& operator=(const FileDescriptor&) = delete;
        FileDescriptor(FileDescriptor&&) noexcept;
        FileDescriptor& operator=(FileDescriptor&&) noexcept;
        ~FileDescriptor();

        operator bool() const;

        [[nodiscard]] value_type get() const;

      private:
        value_type m_handle = null_value;
    };
}

// EOF
