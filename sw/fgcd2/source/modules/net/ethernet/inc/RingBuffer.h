//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include "FileDescriptor.h"
#include "RawFrame.h"

namespace fgcd
{
    class RingBuffer;
    using RingBufferPtr = std::shared_ptr<RingBuffer>;

    class RingBuffer : public Component
    {
        using SocketFD = FileDescriptor::value_type;

      public:
        RingBuffer(Component& parent, std::size_t frame_size, std::size_t frames_length, SocketFD socket_handle);
        ~RingBuffer() override;

        [[nodiscard]] ByteStreamView getPayload() const;
        void                         next();

      private:
        ByteSpan                        m_buffer;
        std::vector<RawFrame>           m_frames;
        std::vector<RawFrame>::iterator m_current = m_frames.begin();
        FileDescriptor::value_type      m_socket_fd;
    };
}

// EOF
