//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <linux/if_packet.h>

#include <modules/base/core/inc/types.h>

namespace fgcd
{
    class RawFrame
    {
      public:
        RawFrame(ByteSpan bytes);
        ~RawFrame();
        RawFrame(RawFrame&& other) noexcept;
        RawFrame(const RawFrame&)            = delete;
        RawFrame& operator=(const RawFrame&) = delete;
        RawFrame& operator=(RawFrame&&)      = delete;

        [[nodiscard]] bool           ready() const;
        [[nodiscard]] bool           losing() const;
        [[nodiscard]] ByteStreamView getPayload() const;
        void                         release();

      private:
        ByteSpan     m_buffer;
        tpacket_hdr* m_header;
    };
}

// EOF
