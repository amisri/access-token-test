//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryWriter.h>

namespace fgcd
{
    struct EthernetFrame
    {
        static constexpr std::uint32_t frame_size = 1514;

        using Buffer = ByteArray<frame_size>;

        EthernetFrame()
            : m_buffer(std::make_unique<Buffer>()),
              m_writer(*m_buffer)
        {
        }

        utils::Result<void, std::string> write(auto&&... elements)
        {
            auto result = m_writer.write(elements...);
            if (result.hasError())
            {
                return utils::Failure{result.error()};
            }

            return {};
        }

        [[nodiscard]] ByteStreamView buffer() const
        {
            return {m_buffer->data(), m_writer.position()};
        }

        std::unique_ptr<Buffer> m_buffer;
        BigWriter               m_writer;
    };
}

// EOF
