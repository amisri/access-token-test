#include <string>
#include <random>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <cpp_utils/enumStr.h>

#include <modules/properties/inc/PropertyGroup.h>
#include <modules/properties/inc/PropertyRoot.h>
#include <modules/properties/inc/PropertyLeaf.h>


using namespace fgcd;
using namespace std::literals;

using VectorTypes = testing::Types<
        float,
        std::int32_t,
        std::uint32_t,
        std::int16_t,
        std::uint16_t,
        std::int8_t,
        std::uint8_t,
        Point
>;

using BasicTypes = testing::Types<
        float,
        std::int32_t,
        std::uint32_t,
        std::int16_t,
        std::uint16_t,
        std::int8_t,
        std::uint8_t
>;

// some helper functions

template <typename T>
T getRandomValue(T min, T max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution distribution(min, max);
    return distribution(gen);
}
template <>
float getRandomValue(float min, float max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution distribution(min, max);
    return distribution(gen);
}

TEST(propertiesTests, testRoot)
{
    PropertyRoot root;
    PropertyGroup test(root, "test");

    ASSERT_EQ(&root, &test.root());
    ASSERT_EQ(test.parent(), nullptr);
}

TEST(propertiesTests, testRootAddAndFind)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    PropertyLeaf<int, true> amplitude{ref_prop, "amplitude", PropertyConfig{}};
    auto res = root.find("ReF.amplitude");

    ASSERT_NE(nullptr, res);
    if (res)
    {
        ASSERT_EQ(res->fullName().toString(), amplitude.fullName().toString());
    }
}

TEST(propertiesTests, testPPMSimpleTree)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    PropertyLeaf<int, true> amplitude{ref_prop, "amplitude", PropertyConfig{}};

    // test for parents
    ASSERT_TRUE(ref_prop.isParent());
    ASSERT_FALSE(amplitude.isParent());

    ASSERT_EQ(amplitude.fullName().toString(), "REF.AMPLITUDE");

    root.forEach([](auto& property)
                 {
                     ASSERT_EQ(property.fullName().toString(), "REF.AMPLITUDE");
                 });
    auto val = 10;
    Selector selector {15};
    auto res = amplitude.set(val, selector);
    ASSERT_FALSE(res.error);
    if(res.error)
    {
        std::cout << "Error:" << res.error->error_str << std::endl;
    }

    ASSERT_EQ(val, amplitude.get(selector).value);

    // try with string
    std::string string_value = "66";
    res = amplitude.setGeneric(string_value, selector);
    ASSERT_FALSE(res.error);

    ASSERT_EQ(66, amplitude.get(selector).value);

    // Try with different selector & val
    auto val2 = 66;
    res = amplitude.set(val2, Selector{0});
    ASSERT_FALSE(res.error);

    ASSERT_EQ(val2, amplitude.get(Selector{0}).value);
    ASSERT_EQ(66, amplitude.get(Selector{15}).value);
}

TEST(propertiesTests, testNonPPMSimpleTree)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    PropertyLeaf<int, false> amplitude{ref_prop, "amplitude", PropertyConfig{}};
    ASSERT_EQ(amplitude.fullName().toString(), "REF.AMPLITUDE");

    root.forEach([](auto& property)
                 {
                     ASSERT_EQ(property.fullName().toString(), "REF.AMPLITUDE");
                 });
    auto val = 10;
    auto res = amplitude.set(val);
    ASSERT_FALSE(res.error);

    ASSERT_EQ(10, amplitude.get().value);
}

TEST(propertiesTests, testPPMPtr)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    // prepare array of pointers
    std::array<int, Selector::MAX_SELECTOR_ARRAY_SIZE> ptr_array;
    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        ptr_array[i] = i * 10;
    }

    // create property
    PropertyLeaf<int *, true> test_ptr{ref_prop, "amplitude", PropertyConfig{}, 0, PropertyLimits<int>()};

    test_ptr.setPointerBaseAddress(ptr_array.data());

    // get the values

    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        auto val = test_ptr.get(Selector{i});
        ASSERT_EQ(val.value, i * 10);
    }

    // set the values - externally

    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        auto new_val = i * 100;
        ptr_array[i] = new_val;

        test_ptr.notify(Selector{i});

        auto val = test_ptr.get(Selector{i});
        ASSERT_EQ(val.value, new_val);
        ASSERT_EQ(ptr_array[i], new_val);
    }

    // set the values - through the property
    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        auto new_val = i * 1000;
        test_ptr.notify(Selector{i});

        auto res = test_ptr.set(new_val, Selector{i});
        ASSERT_FALSE(res.error);

        auto val = test_ptr.get(Selector{i});
        ASSERT_EQ(val.value, new_val);
        ASSERT_EQ(ptr_array[i], new_val);
    }
}

TEST(propertiesTests, testNonPPMPtr)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    std::unique_ptr<int> ptr = std::make_unique<int>();
    *ptr = 10;
    // create property
    PropertyLeaf<int *, false> test_ptr{ref_prop, "amplitude", PropertyConfig{}, 0, PropertyLimits<int>()};

    test_ptr.setPointerBaseAddress(ptr.get());

    // get the values
    auto val = test_ptr.get();
    ASSERT_EQ(val.value, 10);


    // set the values - externally
    auto new_val = 100;
    *ptr = new_val;

    test_ptr.notify();

    val = test_ptr.get();
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(*ptr, new_val);

    // set the values - through the property

    new_val = 1000;

    test_ptr.notify();

    auto res = test_ptr.set(new_val);
    ASSERT_FALSE(res.error);

    val = test_ptr.get();
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(*ptr, new_val);
}

TEST(propertiesTests, testPPMPtrStruct)
{
    struct TestStruct
    {
        int a;
        bool b;
        std::string c;
        int d;
        double e;
        float f;
    };

    std::array<TestStruct, Selector::MAX_SELECTOR_ARRAY_SIZE> ptr_array;
    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        ptr_array[i].a = i * 10;
        ptr_array[i].b = i%2;
        ptr_array[i].c = std::to_string(i*10);
        ptr_array[i].d = i * 20;
        ptr_array[i].e = i * 0.1;
        ptr_array[i].f = i * 0.2;
    }


    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    auto offset_to_d = offsetof(TestStruct, d);

    // create property
    PropertyLeaf<int *, true> test_ptr{ref_prop, "amplitude", PropertyConfig{}, offset_to_d, PropertyLimits<int>()};

    test_ptr.setPointerBaseAddress(ptr_array.data());
    test_ptr.setPointerPPMOffset(sizeof(TestStruct));

    // get the values
    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        auto val = test_ptr.get(Selector{i});
        EXPECT_EQ(val.value, ptr_array[i].d);
    }

    // set the values - externally
    auto new_val = 666;
    auto selector = Selector{10};
    ptr_array[selector.asNumber()].d = new_val;

    test_ptr.notify(selector);

    auto val = test_ptr.get(selector);
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(ptr_array[selector.asNumber()].d, new_val);

    // set the values - through the property

    new_val = 666000;
    selector = Selector{15};
    auto res = test_ptr.set(new_val, selector);
    val = test_ptr.get(selector);
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(ptr_array[selector.asNumber()].d, new_val);
}

TEST(propertiesTests, testNonPPMPtrStruct)
{
    struct TestStruct
    {
        int a;
        bool b;
        std::string c;
        int d;
        double e;
        float f;
    };

    auto my_struct = TestStruct{};

    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    my_struct.d = 10;
    auto ptr = &(my_struct.d);
    auto my_struct_ptr = &my_struct;
    auto offset_to_d = offsetof(TestStruct, d);

    // create property
    PropertyLeaf<int *, false> test_ptr{ref_prop, "amplitude", PropertyConfig{}, offset_to_d, PropertyLimits<int>()};

    test_ptr.setPointerBaseAddress(my_struct_ptr);

    // get the values
    auto val = test_ptr.get();
    ASSERT_EQ(val.value, my_struct.d);


    // set the values - externally
    auto new_val = 100;
    my_struct.d = new_val;

    test_ptr.notify();

    val = test_ptr.get();
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(*ptr, new_val);

    // set the values - through the property

    new_val = 1000;
    auto res = test_ptr.set(new_val);
    val = test_ptr.get();
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(*ptr, new_val);
}

TEST(propertiesTests, testPPMSubDevPtrStruct)
{
    constexpr auto MAX_SUBDEV = 16;
    constexpr auto SUBDEV_ID = 1;
    struct TestStruct
    {
        int a;
        bool b;
        std::string c;
        int d;
        struct FuncPulse
        {
            float ref [MAX_SUBDEV];
            float duration[MAX_SUBDEV];
        } pulse;
        double e;
        float f;
    };

    std::array<TestStruct, Selector::MAX_SELECTOR_ARRAY_SIZE> ptr_array;
    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        ptr_array[i].a = i * 10;
        ptr_array[i].b = i%2;
        ptr_array[i].c = std::to_string(i*10);
        ptr_array[i].d = i * 20;
        for(auto j=0; j<MAX_SUBDEV; ++j)
        {
            ptr_array[i].pulse.ref[j] = i*10+j;
            ptr_array[i].pulse.duration[j] = i*20+j;
        }
        ptr_array[i].e = i * 0.1;
        ptr_array[i].f = i * 0.2;
    }


    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    auto offset_to_pulse_ref = offsetof(TestStruct, pulse.ref);

    // create property
    PropertyLeaf<float *, true> test_ptr{ref_prop, "amplitude", PropertyConfig{}, offset_to_pulse_ref, PropertyLimits<float>()};

    test_ptr.setPointerBaseAddress(ptr_array.data());
    test_ptr.setPointerPPMOffset(sizeof(TestStruct));
    test_ptr.setPointerSubDeviceOffset(sizeof(float));
    test_ptr.setSubDeviceID(SUBDEV_ID);

    // get the values
    for (auto i = 0; i < ptr_array.size(); ++i)
    {
        //EXPECT_EQ(&ptr_array[0].pulse.ref[SUBDEV_ID], test_ptr.getReference(Selector{i}));
        auto val = test_ptr.get(Selector{i});
        EXPECT_EQ(val.value, ptr_array[i].pulse.ref[SUBDEV_ID]);
    }

    // set the values - externally
    auto new_val = 666;
    auto selector = Selector{10};
    ptr_array[selector.asNumber()].pulse.ref[SUBDEV_ID] = new_val;

    test_ptr.notify(selector);

    auto val = test_ptr.get(selector);
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(ptr_array[selector.asNumber()].pulse.ref[SUBDEV_ID], new_val);

    // set the values - through the property

    new_val = 666000;
    selector = Selector{15};
    auto res = test_ptr.set(new_val, selector);
    val = test_ptr.get(selector);
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(ptr_array[selector.asNumber()].pulse.ref[SUBDEV_ID], new_val);
}

TEST(propertiesTests, testNonPPMSubDevPtrStruct)
{
    constexpr auto MAX_SUBDEV = 16;
    constexpr auto SUBDEV_ID = 1;
    struct TestStruct
    {
        int a;
        bool b;
        std::string c;
        int d;
        struct FuncPulse
        {
            float ref [MAX_SUBDEV];
            float duration[MAX_SUBDEV];
        } pulse;
        double e;
        float f;
    };

    TestStruct test_str;

    test_str.a = 10;
    test_str.b = true;
    test_str.c = std::to_string(10);
    test_str.d = 20;
    for(auto j=0; j<MAX_SUBDEV; ++j)
    {
        test_str.pulse.ref[j] = 10+j;
        test_str.pulse.duration[j] = 20+j;
    }
    test_str.e = 0.1;
    test_str.f = 0.2;


    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    auto offset_to_pulse_ref = offsetof(TestStruct, pulse.ref);

    // create property
    PropertyLeaf<float *, false> test_ptr{ref_prop, "amplitude", PropertyConfig{}, offset_to_pulse_ref, PropertyLimits<float>()};

    test_ptr.setPointerBaseAddress(&test_str);
    //test_ptr.setPointerPPMOffset(sizeof(TestStruct));
    test_ptr.setPointerSubDeviceOffset(sizeof(float));
    test_ptr.setSubDeviceID(SUBDEV_ID);

    // get the values
    auto val = test_ptr.get();
    EXPECT_EQ(val.value, test_str.pulse.ref[SUBDEV_ID]);

    // set the values - externally
    auto new_val = 666;
    auto selector = Selector{10};
    test_str.pulse.ref[SUBDEV_ID] = new_val;

    test_ptr.notify(selector);

    auto val2 = test_ptr.get();
    ASSERT_EQ(val2.value, new_val);
    ASSERT_EQ(test_str.pulse.ref[SUBDEV_ID], new_val);

    // set the values - through the property

    new_val = 666000;
    auto res = test_ptr.set(new_val);
    val = test_ptr.get();
    ASSERT_EQ(val.value, new_val);
    ASSERT_EQ(test_str.pulse.ref[SUBDEV_ID], new_val);
}

TEST(propertiesTests, testPtrStructArr)
{
    constexpr auto FGC_TABLE_LEN = 32;

    struct TestStruct
    {
        int a;
        bool b;
        std::string c;
        int d;
        struct property_ppm_func_table
        {
            struct FG_point            function[FGC_TABLE_LEN];          //!< REF.TABLE.FUNCTION      Table function
            uintptr_t                  function_num_elements;            //!<                         Number of elements in table function
        } table;
        double e;
        float f;
    };

    auto my_struct = TestStruct{
        .a = 1,
        .b = true,
        .c = "10",
        .d = 1,
        .e = 1.2,
        .f = 3.4
    };

    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    auto nb_elements = 10;
    for(auto i=0; i<nb_elements; ++i)
    {
        my_struct.table.function[i] = {i*0.1f, i*0.2f};
    }
    auto* my_struct_ptr = &my_struct;
    auto offset_to_table_function = offsetof(TestStruct, table.function);

    // create property
    PropertyLeaf<FG_point*, false, FGC_TABLE_LEN> test_ptr{ref_prop, "amplitude", PropertyConfig{}, offset_to_table_function, std::nullopt};

    test_ptr.setPointerBaseAddress(my_struct_ptr);

    // get the values
    auto val = test_ptr.get();

    for (auto i=0; i<nb_elements; ++i)
    {
        ASSERT_EQ(val.value[i], my_struct.table.function[i]);
    }



    // set the values - externally
    auto new_val = FG_point{10.0f, 10.0f};
    my_struct.table.function[5] = new_val;

    test_ptr.notify();

    val = test_ptr.get();
    ASSERT_EQ(val.value[5], new_val);
    ASSERT_EQ(my_struct.table.function[5], new_val);

    // set the values - through the property

    std::vector<FG_point> vec;
    vec.push_back(FG_point{100.0f, 100.0f});
    auto res = test_ptr.set(vec);
    val = test_ptr.get();
    ASSERT_EQ(val.value[0], vec[0]);
    ASSERT_EQ(my_struct.table.function[0], vec[0]);
}

TEST(propertiesTests, testPPMPtrStructArr)
{
    constexpr auto FGC_TABLE_LEN = 32;

    struct TestStruct
    {
        int a;
        bool b;
        std::string c;
        int d;
        struct property_ppm_func_table
        {
            struct FG_point            function[FGC_TABLE_LEN];          //!< REF.TABLE.FUNCTION      Table function
            uintptr_t                  function_num_elements;            //!<                         Number of elements in table function
        } table;
        double e;
        float f;
    };

    // PPM

    std::array<TestStruct, Selector::MAX_SELECTOR_ARRAY_SIZE> test_struct_array;
    auto nb_elements = 10;

    for (int i=0; i<test_struct_array.size(); ++i)
    {
        test_struct_array[i] = TestStruct{
                .a = 1 * i,
                .b = true,
                .c = "10",
                .d = 1 * i,
                .e = 1.2 * i,
                .f = 3.4f * i
        };

        for(auto j=0; j<nb_elements; ++j)
        {
            test_struct_array[i].table.function[j] = {i*j*0.1f, i*j*0.2f};
        }
    }

    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    auto offset_to_table_function = offsetof(TestStruct, table.function);

    // create property
    PropertyLeaf<FG_point*, true, FGC_TABLE_LEN> test_ptr{ref_prop, "amplitude", PropertyConfig{}, offset_to_table_function, std::nullopt};

    test_ptr.setPointerBaseAddress(test_struct_array.data());
    test_ptr.setPointerPPMOffset(sizeof(TestStruct));

    // get the values
    for (int i=0; i<test_struct_array.size(); ++i)
    {
        auto val = test_ptr.get(Selector{i});

        for (auto j=0; j<nb_elements; ++j)
        {
            ASSERT_EQ(val.value[j], test_struct_array[i].table.function[j]);
        }
    }

    // set the values - externally
    Selector selector{5};
    auto new_val = FG_point{10.0f, 10.0f};
    test_struct_array[selector.asNumber()].table.function[5] = new_val;

    test_ptr.notify(selector);

    auto val = test_ptr.get(selector);
    ASSERT_EQ(val.value[5], new_val);
    ASSERT_EQ(test_struct_array[selector.asNumber()].table.function[5], new_val);

    // set the values - through the property

    std::vector<FG_point> vec;
    vec.push_back(FG_point{100.0f, 100.0f});
    auto res = test_ptr.set(vec, selector);
    val = test_ptr.get(selector);
    ASSERT_EQ(val.value[0], vec[0]);
    ASSERT_EQ(test_struct_array[selector.asNumber()].table.function[0], vec[0]);
}

TEST(propertiesTests, testNonPPMPtrArrStruct)
{
    constexpr auto nb_devices = 32;
    struct Device
    {
        int a;
        bool b;
        std::string name;
        int d;
        double e;
        float f;
    };

    std::array<Device, nb_devices> devices;
    std::vector<std::string> expected_device_names{nb_devices};
    for(auto i=0; i<nb_devices; ++i)
    {
        std::string device_name = "Device_"s + std::to_string(i);

        devices[i].a = i;
        devices[i].b = true;
        devices[i].name = device_name;
        devices[i].d = i+1;
        devices[i].e = i;
        devices[i].f = i+1.0f;

        expected_device_names[i] = device_name;
    }

    PropertyRoot root;
    PropertyGroup ref_prop{root, "DEVICE"};

    // create property
    PropertyLeaf<std::string*, false, nb_devices> test_ptr_arr{ref_prop, "NAME", PropertyConfig{}, offsetof(Device, name)};
    test_ptr_arr.setPointerBaseAddress(devices.data());
    test_ptr_arr.setStep(sizeof(Device));

    auto val = test_ptr_arr.get();

    EXPECT_EQ(val.value, expected_device_names);

    expected_device_names[0] = "testDevice";
    expected_device_names[1] = "TestDevice2";
    auto res = test_ptr_arr.setGeneric(std::vector<std::string>{"testDevice", "TestDevice2"});
    EXPECT_FALSE(res.hasError()) << res.error->error_str;
    val = test_ptr_arr.get();
    EXPECT_FALSE(val.hasError());
    EXPECT_EQ(expected_device_names, val.value);
}

TEST(propertiesTests, testNonPPMPropSizeZeroWithType)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "DEVICE"};

    // Property config
    auto config = PropertyConfig{
        .type = PropertyType::method,
        .get_func = PropertyFunctionSource::warning,
        .set_func = PropertyFunctionSource::user,

    };
    // output string
    std::string output;
    // create property
    PropertyLeaf<std::string, false> test_prop{ref_prop, "RESET", config};


    auto setFunc = [&output](PropertyLeaf<std::string, false>& prop, const std::string& value, const Selector&, const Filters&, const Options&) -> Result<void>
    {
        output = value;
        return {};
    };

    test_prop.setSetFunctionExact(setFunc);

    // get shouldn't work
    auto val = test_prop.get();
    EXPECT_TRUE(val.error);

    // set
    auto res = test_prop.set("DUPA");
    EXPECT_FALSE(res.error);

    EXPECT_EQ("DUPA", output);

}

TEST(propertiesTests, testNonPPMPropSizeZeroWithoutType)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "DEVICE"};

    // Property config
    auto config = PropertyConfig{
            .type = PropertyType::method,
            .get_func = PropertyFunctionSource::warning,
            .set_func = PropertyFunctionSource::user,

    };
    // output string
    std::string output;
    // create property
    using PropType = PropertyLeaf<void, false>;
    PropType test_prop{ref_prop, "RESET", config};


    auto setFunc = [&output](PropType& prop, const Selector&, const Filters&, const Options&) -> Result<void>
    {
        output = "DUPA";
        return {};
    };
    test_prop.setSetFunction(setFunc);
    test_prop.setSetFunctionExact(setFunc);

    // get shouldn't work
    auto val = test_prop.get();
    EXPECT_TRUE(val.error);

    // set
    auto res = test_prop.set();
    EXPECT_FALSE(res.error);

    // setGeneric
    auto res2 = test_prop.setGeneric(std::monostate());
    EXPECT_FALSE(res2.hasError());

    EXPECT_EQ("DUPA", output);

}

TEST(propertiesTests, testPPMArrAndLimits)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    std::array<int, 10> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    PropertyLimits<decltype(arr)> limits(10, 20);
    auto config = PropertyConfig{
        .set_func = fgcd::PropertyFunctionSource::limits
    };

    PropertyLeaf<decltype(arr), true> test_arr{ref_prop, "value", config, arr, limits};

    // Check default value works
    auto selector = Selector{6};
    for (auto i = 0u; i < arr.size(); ++i)
    {
        auto& val = arr[i];
        EXPECT_EQ(test_arr.get(selector).value[i], val);
    }

    // check limits

    // set value above limits
    std::array<int, 10> arr_above_limits = {100, 110, 120, 130, 140, 150, 160, 170, 180, 190};
    auto res = test_arr.set(arr_above_limits, selector);
    EXPECT_TRUE(res.error);

}

TEST(propertiesTests, testPPMArrSetVector)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    std::array<int, 10> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::vector<int> vec = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

    auto config = PropertyConfig{
            //.set_func = fgcd::PropertyFunctionSource::limits
    };
    PropertyLeaf<decltype(arr), true> test_arr{ref_prop, "value", config, arr};

    // set vector
    auto selector = Selector{6};

    auto res = test_arr.setGeneric(vec, selector);
    if(res.error)
    {
        std::cout << "Error:" << res.error->error_str << std::endl;
    }
    ASSERT_FALSE(res.error);

    // check it works
    for (auto i = 0u; i < vec.size(); ++i)
    {
        auto& val = vec[i];
        ASSERT_EQ(test_arr.get(selector).value[i], val);
    }
}

TEST(propertiesTests, testHidePropertyLeafConfig)
{
    // HIDE config should work both on PARENT properties and LEAF properties
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    PropertyLeaf<std::uint32_t, false> hidden_property{ref_prop, "HIDDEN", PropertyConfig{.flags=PropertyFlags::hide}};
    PropertyLeaf<std::uint32_t, false> not_hidden_property{ref_prop, "NOT_HIDDEN", PropertyConfig{}};
    {
        auto res = hidden_property.set(5);
        EXPECT_FALSE(res.hasError());
    }
    {
        auto res = not_hidden_property.set(10);
        EXPECT_FALSE(res.hasError());
    }

    // get value
    {
        auto res = ref_prop.getGeneric(NoSelector);
        ASSERT_FALSE(res.hasError());
        auto value_parent = res.value;
        EXPECT_TRUE(value_parent.isParent());
        EXPECT_EQ(value_parent.getChildren().size(), 1);

        // find children
        auto not_hidden_child_iter = value_parent.getChildren().find("NOT_HIDDEN");
        auto hidden_child_iter = value_parent.getChildren().find("HIDDEN");

        auto end_iter = value_parent.getChildren().end();

        EXPECT_NE(not_hidden_child_iter, end_iter);
        EXPECT_EQ(hidden_child_iter, end_iter); // not found

        // check value
        auto not_hidden_child = not_hidden_child_iter->second;
        EXPECT_TRUE(not_hidden_child->isParent());
        auto not_hidden_child_value = not_hidden_child->getChildren().at("value");
        EXPECT_EQ(std::get<std::uint32_t>(not_hidden_child_value->get()), 10);
    }
}

TEST(propertiesTests, testSeResettOnParentProperty)
{
    PropertyRoot root;
    struct VS : public PropertyGroup
    {
        using PropertyGroup::PropertyGroup;
        PropertyLeaf<void, false> reset{*this, "reset", PropertyConfig{
                .type       = PropertyType::method,
                .get_func   = PropertyFunctionSource::warning,
                .set_func   = PropertyFunctionSource::user,
        }};
    };
    VS vs{root, "VS",
              PropertyConfig {
                      .set_func = PropertyFunctionSource::user,
              }};

    bool reset_triggered = false;
    bool notified = false;

    vs.reset.setSetFunction([&reset_triggered](auto& property, auto selector, auto filters, auto options) -> decltype(VS::reset)::SetFuncResult
    {
        reset_triggered = true;
        property.notify(selector);
        property.callOnChange(selector);
        return {};
    });

    root.addNotificationCallback([&notified, &reset_triggered](auto& property, auto selector) -> void
    {
        if (property.fullName() == "VS.RESET")
        {
            notified = true;
        }
    });

    vs.setSetFunction(
            [](PropertyGroup& property, auto value, auto selector, auto filters, auto options) -> Result<void>
    {
        auto* vs_prop = dynamic_cast<VS*>(&property);

        if(vs_prop)
        {
            return vs_prop->reset.set(filters, options);
        }
        return Result<void>{Error{"Set function used on a wrong property!"}};
    });

    // let's try...
    {
        auto result = vs.setGeneric(true, NoSelector);
        EXPECT_FALSE(result.hasError());
        EXPECT_TRUE(reset_triggered);
        EXPECT_TRUE(notified);
    }


    // clear reset flag and notified flag
    reset_triggered = false;
    notified = false;

    // try via reset
    {
        auto result = vs.reset.set();
        EXPECT_FALSE(result.hasError());
        EXPECT_TRUE(reset_triggered);
        EXPECT_TRUE(notified);
    }
}

TEST(propertiesTests, testSetREFOnParentProperty)
{
    PropertyRoot root;
    struct PLEP : public PropertyGroup
    {
        using PropertyGroup::PropertyGroup;
        PropertyLeaf<float, true> final{*this, "final", PropertyConfig{
                .type       = PropertyType::setting,
                .get_func   = PropertyFunctionSource::automatic,
                .set_func   = PropertyFunctionSource::automatic,
        }};
        PropertyLeaf<float, true> acceleration{*this, "acceleration", PropertyConfig{
                .type       = PropertyType::setting,
                .get_func   = PropertyFunctionSource::automatic,
                .set_func   = PropertyFunctionSource::automatic,
        }};
        PropertyLeaf<float, true> linear_rate{*this, "linear_rate", PropertyConfig{
                .type       = PropertyType::setting,
                .get_func   = PropertyFunctionSource::automatic,
                .set_func   = PropertyFunctionSource::automatic,
        }};
        PropertyLeaf<float, true> exp_tc{*this, "exp_tc", PropertyConfig{
                .type       = PropertyType::setting,
                .get_func   = PropertyFunctionSource::automatic,
                .set_func   = PropertyFunctionSource::automatic,
        }};
        PropertyLeaf<float, true> exp_final{*this, "exp_final", PropertyConfig{
                .type       = PropertyType::setting,
                .get_func   = PropertyFunctionSource::automatic,
                .set_func   = PropertyFunctionSource::automatic,
        }};
    };
    struct REF : public PropertyGroup
    {
        using PropertyGroup::PropertyGroup;
        PLEP plep {*this, "plep", PropertyConfig {
                .flags = PropertyFlags{fgcd::PropertyFlags::hide},
                .set_func = PropertyFunctionSource::user,
        }};
    };

    REF ref{root, "ref", PropertyConfig{
        .set_func = PropertyFunctionSource::user,
    }};

    std::vector<std::pair<PropertyName, Selector>> notified_properties;

    ref.setSetFunction([](auto& property, auto value, auto selector, auto filters, auto options)
                       {
                           auto* ref = dynamic_cast<REF*>(&property);
                           const auto plep_prefix = "PLEP"sv;
                           if (ref)
                           {
                                if (std::holds_alternative<std::string>(value))
                                {
                                    auto command = std::get<std::string>(value);

                                    if (command.starts_with(plep_prefix))
                                    {
                                        // remove prefix
                                        auto new_command = command.substr(plep_prefix.size()+1); // with space
                                        return ref->plep.setGeneric(new_command, selector, filters, options);
                                    }
                                }
                           }
                           return Result<void>{Error{"Set function used on a wrong property!"}};
                       });

    ref.plep.setSetFunction([](auto& property, auto value, auto selector, auto filters, auto options)
                            {
                                auto* ref_plep = dynamic_cast<PLEP*>(&property);
                                if (ref_plep)
                                {
                                    if (std::holds_alternative<std::string>(value))
                                    {
                                        auto command = std::get<std::string>(value);
                                        std::vector<float> data;
                                        std::istringstream iss{command};
                                        iss >> data;

                                        // check size
                                        if (data.size() != 5)
                                        {
                                            return Result<void>{Error{"Size too big!"}};
                                        }

                                        std::size_t counter = 0;
                                        for(auto item: ref_plep->items())
                                        {
                                            auto& child_property = item.get();
                                            auto result = child_property.setGeneric(data[counter++], selector, filters, options);
                                            if (result.hasError())
                                            {
                                                return Result<void>{Error{"Set failed for " + child_property.fullName().toString()}};
                                                break;
                                            }
                                        }
                                        return Result<void>{};

                                    }
                                }
                                return Result<void>{Error{"Set function used on a wrong property!"}};
                            });

    root.addNotificationCallback([&notified_properties](auto& property, auto selector) -> void
                                 {
                                    notified_properties.push_back({property.fullName(), selector});
                                 });

    // let's try...
    std::array data_to_set = {
            1.0f,
            2.0f,
            3.0f,
            4.0f,
            5.0f
    };

    std::ostringstream ss;
    ss << data_to_set;
    std::string command = "PLEP " + ss.str();
    {
        auto selector = Selector{5};
        auto result = ref.setGeneric(command, selector);
        EXPECT_FALSE(result.hasError());

        std::size_t counter = 0;
        for(auto& item: ref.plep.items())
        {
            auto& child_property = item.get();
            auto child_get_result = child_property.getGeneric(selector);
            EXPECT_FALSE(child_get_result.hasError());

            auto value = child_get_result.value.getChildren().at("value")->get();

            EXPECT_THAT(value, ::testing::VariantWith<float>(data_to_set[counter++]));
        }
    }
}

TEST(propertiesTests, testArrayToStringAndFromString)
{
    // to string
    std::array<int, 6> arr = {0, 1, 2, 3, 4, 5};
    auto str_repr = "[0,1,2,3,4,5]"sv;
    std::ostringstream out;
    out << arr;
    ASSERT_EQ(out.str(), str_repr);

    // from string
    std::array<int, 6> arr2;
    std::istringstream iss{std::string(str_repr)};
    iss >> arr2;
    ASSERT_EQ(arr, arr2);
}

TEST(propertiesTests, testFromStringArray)
{
    auto arr_repr = "[0,1,2,3,4,5]"s;
    std::array<int, 6> target_arr;
    std::array<int, 6> expected_array = {0,1,2,3,4,5};
    auto res = ValueParser::fromString(arr_repr, target_arr);
    ASSERT_EQ(target_arr, expected_array);
}

template <typename T>
  struct SimplePropertyTestFixture : public ::testing::Test {};
TYPED_TEST_SUITE_P(SimplePropertyTestFixture);

TYPED_TEST_P(SimplePropertyTestFixture, doPropertySetAndGetWithLimits)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    auto config = PropertyConfig{
        // test also with limits
            .set_func = fgcd::PropertyFunctionSource::limits
    };

    // let's set the limits a bit different to the min/max value
    constexpr TypeParam limit_offset { 10 };
    PropertyLimits<TypeParam> limits;
    limits.setMin(0 + limit_offset);
    limits.setMax(limits.getMin() + limit_offset);
//    limits.setMin(std::numeric_limits<TypeParam>::min()+limit_offset);
//    limits.setMax(std::numeric_limits<TypeParam>::max()-limit_offset);

    // create property
    PropertyLeaf<TypeParam , true> property{ref_prop, "value", config, {}, limits};

    // get some selectors
    std::array selectors = {Selector{0}, Selector{2}, Selector{10}};

    // get some storage
    std::map<Selector, TypeParam> storage;

    // fill storage with random values within limits and set if for given selectors

    for(auto selector: selectors)
    {
        auto random_value = getRandomValue(limits.getMin(), limits.getMax());
        storage.insert({selector, random_value});
    }

    // set
    for(auto [selector, value]: storage)
    {
        auto result = property.set(value, selector);
        EXPECT_FALSE(result.error) << result.error->error_str;
    }
    // get
    for(auto [selector, value]: storage)
    {
        auto result = property.get(selector);
        EXPECT_FALSE(result.error) << result.error->error_str;
        EXPECT_EQ(value, result.value);
    }

    // set outside of limit
    for(auto [selector, value]: storage)
    {
        auto min_val = property.getLimit()->getMin() - 1;
        auto max_val = property.getLimit()->getMax() + 1;

        auto result1 = property.set(min_val, selector);
        auto result2 = property.set(max_val, selector);
        auto result3 = property.get(selector);
        EXPECT_TRUE(result1.error);
        EXPECT_TRUE(result2.error);
        EXPECT_FALSE(result3.error);

        // value shouldn't be changed
        EXPECT_EQ(value, result3.value);
    }
    // set the values as string and compare if okay
    for(auto [selector, value]: storage)
    {
        std::ostringstream out;
        if constexpr(std::is_same_v<TypeParam, signed char> ||
                std::is_same_v<TypeParam, unsigned char>)
        {
            // pass char as an int to the stream;
            out << static_cast<int>(value);
        }
        else
        {
            out << value;
        }
        auto result = property.setGeneric(out.str(), selector);
        EXPECT_FALSE(result.error) << result.error->error_str << "Value: " << out.str();

        // get
        auto result2 = property.get(selector);
        EXPECT_FALSE(result2.error) << result2.error->error_str;
        EXPECT_NEAR(result2.value, value, 0.001);
    }

    // Set wrong string value
    const auto random_string = "dupa.8"s;

    auto result = property.setGeneric(random_string, Selector{5});

    EXPECT_TRUE(result.error) << result.error->error_str;

    // test base class set
    ref_prop.forEach([](auto& property){
        auto result = property.setGeneric("11", Selector{5});
        EXPECT_FALSE(result.error) << result.error->error_str;

    });

}

REGISTER_TYPED_TEST_SUITE_P(SimplePropertyTestFixture, doPropertySetAndGetWithLimits);

INSTANTIATE_TYPED_TEST_SUITE_P(propertyTests, SimplePropertyTestFixture, BasicTypes);

template <typename T>
  struct ArrayPropertyWithVectorFixture : public ::testing::Test {};

TYPED_TEST_SUITE_P(ArrayPropertyWithVectorFixture);

TYPED_TEST_P(ArrayPropertyWithVectorFixture, doArrayPropertyWithVector)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};

    std::array<TypeParam, 5> arr;
    if constexpr (std::is_same_v<TypeParam, Point>)
    {
        arr = {
                Point{0, 0},
                Point{0, 1},
                Point{0, 2},
                Point{1, 1},
                Point{5, 5}
            };
    }
    else
    {
        arr = {0, 1, 2, 3, 4};
    }

    std::vector<typename decltype(arr)::value_type> vec;
    for(const auto& v: arr)
    {
        vec.push_back(v);
    }

    auto config = PropertyConfig{
            //.set_func = fgcd::PropertyFunctionSource::limits
    };

    PropertyLeaf<decltype(arr), true> test_arr{ref_prop, "value", config, arr};

    // set vector
    auto selector = Selector{6};

    auto res = test_arr.setGeneric(vec, selector);
    if(res.error)
    {
        std::cout << "Error:" << res.error->error_str << std::endl;
    }
    ASSERT_FALSE(res.error);

    // check it works
    for (auto i = 0u; i < vec.size(); ++i)
    {
        auto& val = vec[i];
        EXPECT_EQ(test_arr.get(selector).value[i], val);
    }
}

REGISTER_TYPED_TEST_SUITE_P(ArrayPropertyWithVectorFixture, doArrayPropertyWithVector);

INSTANTIATE_TYPED_TEST_SUITE_P(propertiesTests, ArrayPropertyWithVectorFixture, VectorTypes);

TEST(propertiesTests, SimpleGetOfParentProperty)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    auto config = PropertyConfig{};
    PropertyGroup i_prop{ref_prop, "I"};
    PropertyGroup v_prop{ref_prop, "V"};
    PropertyLeaf<int, false> i_value{i_prop, "current", config};
    PropertyLeaf<int, false> v_value{v_prop, "voltage", config};

    // set some values
    {
        auto res = i_value.set(123);
        EXPECT_FALSE(res.error);
    }
    {
        auto res = v_value.set(321);
        EXPECT_FALSE(res.error);
    }

    auto ref_res = ref_prop.getGeneric(NoSelector);
    EXPECT_FALSE(ref_res.error) << ref_res.error->error_str;
    EXPECT_TRUE(ref_res.value.isParent());

    auto I_val = ref_res.value.getChildren().at("I");
    EXPECT_TRUE(I_val->isParent());

    auto current_val = I_val->getChildren().at("CURRENT");
    EXPECT_TRUE(current_val->isParent());

    auto value = current_val->getChildren().at("value");
    EXPECT_FALSE(value->isParent());
    EXPECT_THAT(value->get(), ::testing::VariantWith<int>(123));

}

enum class SampleEnum
{
    ON,
    OFF,
    STANDBY
};

MAKE_ENUM_STR(SampleEnumStr, SampleEnum,
              ENUM_STR(SampleEnum, ON),
              ENUM_STR(SampleEnum, OFF),
              ENUM_STR(SampleEnum, STANDBY),
);

TEST(propertiesTests, GetAndSetEnumProperty)
{
    PropertyRoot root;
    PropertyGroup ref_prop{root, "REF"};
    auto config = PropertyConfig{};
    PropertyLeaf<SampleEnum, false> state{ref_prop, "state", config};

    // try with enum itself
    {
        auto res = state.set(SampleEnum::STANDBY);
        EXPECT_FALSE(res.error);
    }
    {
        auto res = state.get();
        EXPECT_FALSE(res.error);
        EXPECT_EQ(res.value, SampleEnum::STANDBY);
    }

    // try with string
    {
        auto res = state.setGeneric("ON");
        EXPECT_FALSE(res.error);
    }
    {
        auto res = state.get();
        EXPECT_FALSE(res.error);
        EXPECT_EQ(res.value, SampleEnum::ON);
    }

    // try with string that's not valid
    {
        auto res = state.setGeneric("DUPA");
        EXPECT_TRUE(res.error);
    }
    {
        auto res = state.get();
        EXPECT_FALSE(res.error);
        EXPECT_EQ(res.value, SampleEnum::ON);
    }
    // try with int that's not valid
    {
        auto res = state.setGeneric(666);
        EXPECT_TRUE(res.error);
    }
    {
        auto res = state.get();
        EXPECT_FALSE(res.error);
        EXPECT_EQ(res.value, SampleEnum::ON);
    }
}