#include <gtest/gtest.h>

#include <modules/properties/inc/PropertyName.h>
#include <cpp_utils/text.h>
#include <string>
using namespace fgcd;
using namespace std::literals;

TEST(propertiesTests, testExplicitInstantiation)
{
    PropertyName pn1{};
    PropertyName pn2{"TEst"s};
    PropertyName pn3{"test"};
    PropertyName pn4{"Test"sv};
}

//TEST(propertiesTests, testImplicitInstantiation)
//{
//    PropertyName pn1{};
//    PropertyName pn2 = "TEst"s;
//    PropertyName pn3 = "test";
//    PropertyName pn4 = "Test"sv;
//}

TEST(propertiesTests, testEmptyIsEqualEmpty)
{
    PropertyName pn1, pn2;

    ASSERT_EQ(pn1, pn2);
}
TEST(propertiesTests, testEmptyIsEquality)
{
    PropertyName pn1;
    std::string empty_s = "";
    std::string_view empty_sv = "";
    const char * empty_cstr = "";

    ASSERT_EQ(pn1, empty_s);
    ASSERT_EQ(empty_s, pn1);

    ASSERT_EQ(pn1, empty_sv);
    ASSERT_EQ(empty_sv, pn1);

    ASSERT_EQ(pn1, empty_cstr);
    ASSERT_EQ(empty_cstr, pn1);
}

TEST(propertiesTests, testEmptyIsNotEqual)
{
    PropertyName pn1;
    std::string empty_s = "a";
    std::string_view empty_sv = "b";
    const char * empty_cstr = "c";

    ASSERT_NE(pn1, empty_s);
    ASSERT_NE(empty_s, pn1);

    ASSERT_NE(pn1, empty_sv);
    ASSERT_NE(empty_sv, pn1);

    ASSERT_NE(pn1, empty_cstr);
    ASSERT_NE(empty_cstr, pn1);
}

TEST(propertiesTests, testLowerNameIsEqualString)
{
    const std::string_view name = "test.name.of.property";
    const std::string_view name_upper = "TEST.NAME.OF.PROPERTY";

    PropertyName pn1{name};

    ASSERT_EQ(pn1, name_upper);
    ASSERT_EQ(name_upper, pn1);
}

bool test_string_equal_pn(const std::string& s1, const std::string& s2)
{
    return s1 == s2;
}

bool test_string_view_equal_pn(std::string_view s1, std::string_view s2)
{
    return s1 == s2;
}

TEST(propertiesTests, testPropertyNameAsString)
{
    const std::string_view name = "test.name.of.property";

    PropertyName pn1{name};

    ASSERT_FALSE(test_string_equal_pn(static_cast<std::string>(pn1), std::string{name}));
}

TEST(propertiesTests, testPropertyNameAsStringView)
{
    const std::string_view name = "test.name.of.property";

    PropertyName pn1{name};

    ASSERT_FALSE(test_string_view_equal_pn(static_cast<std::string>(pn1), name));
}

TEST(propertiesTests, testPropertyNameToString)
{
    const std::string_view name = "test.name.of.property";

    PropertyName pn1{name};

    ASSERT_EQ(pn1.toString(), utils::toUpperCopy(name));

}