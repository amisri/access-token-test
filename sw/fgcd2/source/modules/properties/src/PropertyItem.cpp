#include <cpp_utils/forEach.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/PropertyItem.h"
#include "../inc/PropertyRoot.h"
#include "../inc/PropertyGroup.h"

using namespace fgcd;
using namespace utils;

PropertyItem::PropertyItem(PropertyRoot& root, std::string_view name, PropertyConfig config): m_config{std::move(config)}, m_root{root}, m_name{name}, m_full_name{m_name}
{
    m_root.addItem(*this);
}

PropertyItem::PropertyItem(PropertyGroup& group, std::string_view name, PropertyConfig config): m_config{std::move(config)}, m_parent{&group}, m_root{group.root()}, m_name{name}
{
    m_full_name = parent()->fullName() + m_name;
    parent()->addItem(*this);
}
