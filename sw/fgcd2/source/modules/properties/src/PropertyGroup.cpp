//
// Created by ppluteck on 1/26/23.
//

#include <cpp_utils/forEach.h>
#include "../inc/PropertyGroup.h"
#include "../inc/PropertyRoot.h"


using namespace fgcd;
using namespace utils;


void PropertyGroup::forEach(const PropertyCallback& callback)
{
    for(auto it : m_items)
    {
        it.get().forEach(callback);
    }
}

bool PropertyGroup::isParent() const
{
    return true;
}

void PropertyGroup::addItem(PropertyItem& property_item)
{
    m_items.emplace_back(property_item);
    root().addItem(property_item);
}