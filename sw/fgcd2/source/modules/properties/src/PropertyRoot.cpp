//
// Created by ppluteck on 1/26/23.
//

#include <cpp_utils/forEach.h>
#include "../inc/PropertyRoot.h"


using namespace fgcd;
using namespace utils;


PropertyItem* PropertyRoot::find(const PropertyName& full_name) const
{
    if(auto it = m_all_items.find(full_name); it != m_all_items.end())
    {
        return &(it->second.get());
    }
    return nullptr;
}

PropertyItem* PropertyRoot::find(std::string_view property_full_name) const
{
    return find(PropertyName{property_full_name});
}

void PropertyRoot::forEach(const PropertyCallback& callback)
{
    for(auto it : m_items)
    {
        it.get().forEach(callback);
    }
}

void PropertyRoot::addItem(PropertyItem& property_item)
{
    m_items.emplace_back(property_item);
    m_all_items.insert({property_item.fullName(), property_item});
}
