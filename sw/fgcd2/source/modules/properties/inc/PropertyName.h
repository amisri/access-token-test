//! @file
//! @brief  Class that represents a property name.
//! @author Przemyslaw Plutecki


#pragma once

#include <string>
#include <string_view>
#include <cpp_utils/text.h>

namespace fgcd
{
    struct PropertyName
    {
            PropertyName() = default;
            explicit PropertyName(std::string_view property_name): m_property_name{utils::toUpperCopy(property_name)} {}

            explicit operator std::string() const
            {
                return m_property_name;
            }

            std::string toString() const
            {
                return static_cast<std::string>(*this);
            }

            PropertyName operator + (const PropertyName& rhs) const;

            bool operator == (const PropertyName& rhs) const;
            bool operator != (const PropertyName& rhs) const;
            bool operator <  (const PropertyName& rhs) const;

            friend bool operator ==(const PropertyName& lhs, std::string_view rhs);
            friend bool operator !=(const PropertyName& lhs, std::string_view rhs);
            friend bool operator ==(std::string_view lhs, const PropertyName& rhs);
            friend bool operator !=(std::string_view lhs, const PropertyName& rhs);

        private:
            std::string m_property_name;
    };


    inline bool PropertyName::operator==(const PropertyName &rhs) const
    {
        return m_property_name == rhs.m_property_name;
    }

    inline bool PropertyName::operator!=(const PropertyName &rhs) const
    {
        return !(*this == rhs);
    }

    inline bool PropertyName::operator<(const PropertyName &rhs) const
    {
        return m_property_name < rhs.m_property_name;
    }

    inline bool operator==(std::string_view lhs, const PropertyName& rhs)
    {
        return rhs.operator==(PropertyName{lhs});
    }

    inline bool operator!=(std::string_view lhs, const PropertyName& rhs)
    {
        return rhs.operator!=(PropertyName{lhs});
    }

    inline bool operator==(const PropertyName& lhs, std::string_view rhs)
    {
        return lhs.operator==(PropertyName{rhs});
    }

    inline bool operator!=(const PropertyName& lhs, std::string_view rhs)
    {
        return lhs.operator!=(PropertyName{rhs});
    }

    inline PropertyName PropertyName::operator+(const PropertyName& rhs) const
    {
        return PropertyName{m_property_name + "." + rhs.m_property_name};
    }

}

// EOF