//! @file
//! @brief  Class that represents property configuration.
//! @author Przemyslaw Plutecki


#pragma once

#include <cpp_utils/flagSet.h>
#include "PropertyType.h"
#include "PropertyFunctionSource.h"

namespace fgcd
{

    enum class PropertyFlags
    {
        non_volatile,
        config_file,        //< The property comes from a config file
        require_limits,     //< The property requires limits (min/max value)
        hide,               //< The property will not be shown when the get function of the parent is performed.
        sub_dev,            //< The property belongs to a sub-device.
        COUNT               //< Has to be the last item.
    };

    MAKE_ENUM_STR(PropertyFlagsStr, PropertyFlags,
      ENUM_STR(PropertyFlags, non_volatile),
      ENUM_STR(PropertyFlags, config_file),
      ENUM_STR(PropertyFlags, require_limits),
      ENUM_STR(PropertyFlags, hide),
      ENUM_STR(PropertyFlags, sub_dev)
    );

    struct PropertyConfig
    {
        PropertyType type = PropertyType::setting;
        utils::FlagSet<PropertyFlags> flags;
        PropertyFunctionSource get_func = PropertyFunctionSource::automatic;
        PropertyFunctionSource set_func = PropertyFunctionSource::automatic;
    };
}