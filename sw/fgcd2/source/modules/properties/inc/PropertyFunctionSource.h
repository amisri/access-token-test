//! @file
//! @brief  Class that represents property types.
//! @author Przemyslaw Plutecki


#pragma once

namespace fgcd
{
    enum class PropertyFunctionSource
    {
        automatic,
        limits,
        user,
        warning
    };
}