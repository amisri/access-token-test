//
// Created by ppluteck on 2/2/23.
//

#pragma once

#include <type_traits>
#include <variant>

#include <cpp_utils/Result.h>

#include "PropertyItem.h"
#include "PropertyGroup.h"
#include "PropertyConfig.h"
#include "PropertyLimits.h"
#include "PropertyRoot.h"
#include "PropertyUtils.h"
#include "ValueParser.h"
#include "modules/common/inc/ValueLeaf.h"

namespace fgcd
{
    template<
            typename T,
            bool PPM,
            std::size_t MAX_SIZE = ArraySizeTrait<T>::value
    >
    class PropertyLeaf : public PropertyItem
    {
      public:
        using UpdatedT = ReplaceVoidWithEmptyType<T>::type;

        // Type of the PropertyLeaf, used as argument in many methods
        using PropertyLeafType = PropertyLeaf<T, PPM, MAX_SIZE>;

        // Special case = MAX_SIZE == 0; No value stored, it's obtained from a get function
        static constexpr bool no_storage = MAX_SIZE == 0;

        // More special case - no type of value to store. Set function should have no value params. Get should return nothing
        static constexpr bool no_value = std::is_same_v<T, void>;

        static_assert(no_value ? no_storage: true, "MAX_SIZE must be 0 when using NoValue");

        // Property storage. If T is a pointer, then it's external
        static constexpr bool external_storage = std::is_pointer_v<UpdatedT>;

        // Is the property holding multiple values and has external storage
        static constexpr bool is_external_array = MAX_SIZE > 1 && external_storage;

        // Is the property holding an array of values
        static constexpr bool is_array = is_std_array<UpdatedT>::value || is_std_vector<UpdatedT>::value || is_external_array;

        // If there's no property storage, use PropertiesEmptyType (std::monostate or like);
        // If the property storage is external, then it stores just a pointer (T); If it's internal and PPM, then use array
        using StorageDataType = std::conditional_t<no_storage,
                                        PropertiesEmptyType,
                                        std::conditional_t<external_storage,
                                            UpdatedT,
                                            std::conditional_t<PPM,
                                                std::array<UpdatedT, Selector::MAX_SELECTOR_ARRAY_SIZE>,
                                                UpdatedT>
                                        >
                                    >;


        // for get and set function, the data type for pointer type should be the type it points to
        using RawValueType = std::conditional_t<external_storage,
                                std::conditional_t<is_external_array,
                                    std::vector<std::remove_pointer_t<UpdatedT>>,
                                    std::remove_pointer_t<UpdatedT>>,
                                UpdatedT>;

        // For arrays, get the value_type. For external arrays (and external storage) it's done by removing pointer.
        using UnderlyingValueType = std::conditional_t<is_array,
                std::conditional_t<is_std_array<UpdatedT>::value || is_std_vector<UpdatedT>::value,
                        typename get_value_type<UpdatedT>::value_type,
                        std::remove_pointer_t<UpdatedT>>,
                std::conditional_t<external_storage,
                    std::remove_pointer_t<UpdatedT>,
                    UpdatedT>>;

        using LimitsDataType = UnderlyingValueType;


        using SetFuncValueType = ValueType;

        using GetFuncResult         = std::conditional_t<no_value, Result<void>, Result<RawValueType>>;
        using SetFuncResult         = Result<void>;

        using GetFunc       =   std::function<GetFuncResult       (PropertyLeafType&, const Selector&, const Filters&, const Options&)>;

        using SetFunc       = std::conditional_t<no_value,
                                std::function<SetFuncResult       (PropertyLeafType&, const Selector&, const Filters&, const Options&)>,
                                std::function<SetFuncResult       (PropertyLeafType&, SetFuncValueType, const Selector&, const Filters&, const Options&)>
                            >;

        using SetFuncExact  = std::conditional_t<no_value,
                                std::function<SetFuncResult       (PropertyLeafType&, const Selector&, const Filters&, const Options&)>,
                                std::function<SetFuncResult       (PropertyLeafType&, RawValueType, const Selector&, const Filters&, const Options&)>
                            >;
        using OnChangeFunc  =   std::function<void                (PropertyLeafType&, const Selector&)>;

        using PropertySizeType  = uint32_t;

        // For external storage, it is a pointer; Otherwise, it's a reference
        using ReferenceType = std::conditional_t<external_storage, UpdatedT, UpdatedT&>;

        //! Construct PropertyLeaf used for properties with internal storage (and value type different than void)
        //! \param parent PropertyGroup parent
        //! \param name property name
        //! \param config PropertyConfig configuration
        //! \param default_value optional default value of type T
        //! \param limits optional limits
        PropertyLeaf(PropertyGroup& parent,
                     std::string_view name,
                     PropertyConfig config,
                     std::optional<UpdatedT> default_value = std::nullopt,
                     std::optional<PropertyLimits<LimitsDataType>> limits = std::nullopt) requires(!external_storage && !no_storage) :
                PropertyItem(parent, name, config), m_limits{std::move(limits)}
        {
            // set the default
            if (default_value)
            {
                if constexpr (PPM)
                {
                    for (auto& val: m_value)
                    {
                        val = default_value.value();
                    }
                }
                else
                {
                    m_value = std::move(default_value.value());
                }
            }
        }

        //! Construct PropertyLeaf used for properties with external storage (and value type different than void)
        //! \param parent PropertyGroup parent
        //! \param name property name
        //! \param config PropertyConfig configuration
        //! \param field_offset offset in bytes to given field in a structure
        //! \param limits optional limits
        PropertyLeaf(PropertyGroup& parent,
                     std::string_view name,
                     PropertyConfig config,
                     std::size_t field_offset,
                     std::optional<PropertyLimits<LimitsDataType>> limits = std::nullopt) requires(external_storage && !no_storage) :
                PropertyItem(parent, name, config), m_limits{std::move(limits)}, m_field_offset{field_offset}
        {

        }

        //! Construct PropertyLeaf used for properties without storage (value type is void)
        //! \param parent PropertyGroup parent
        //! \param name property name
        //! \param config PropertyConfig configuration
        //! \param limits optional limits
        PropertyLeaf(PropertyGroup& parent,
                     std::string_view name,
                     PropertyConfig config,
                     std::optional<PropertyLimits<LimitsDataType>> limits = std::nullopt) requires(no_storage) :
                PropertyItem(parent, name, config), m_limits{std::move(limits)}
        {

        }

        //! Get a reference to the data stored in a property
        //! \return ReferenceType to the stored data
        [[ nodiscard ]] ReferenceType getReference() requires(!no_storage)
        {
            if constexpr (external_storage)
            {
                return getPointer(0, NoSelector);
            }
            else
            {
                return m_value;
            }
        }

        //! Get a reference to the data stored in a property (for non-PPM properties and externally stored arrays)
        //! \param idx index of element in array
        //! \return ReferenceType to the stored data
        [[ nodiscard ]] ReferenceType getReference(std::size_t idx) requires(!PPM && is_external_array && !no_storage)
        {
            return getPointer(idx, NoSelector);
        }

        //! Get a reference to the data stored in a property (for PPM properties)
        //! \param selector Selector for PPM data
        //! \return ReferenceType to the stored data
        [[ nodiscard ]] ReferenceType getReference(const Selector& selector) requires (PPM && !no_storage)
        {
            if constexpr (external_storage)
            {
                return getPointer(0, selector);
            }
            else
            {
                return m_value[selector.asNumber()];
            }
        }

        //! Get a reference to the data stored in a property (for PPM properties with externally stored arrays)
        //! \param idx index of element in array
        //! \param selector Selector for PPM data
        //! \return ReferenceType to the stored data
        [[ nodiscard ]] ReferenceType getReference(std::size_t idx, const Selector& selector) requires (PPM && is_external_array && !no_storage)
        {
            return getPointer(idx, selector);
        }

        //! Get a copy of property's data. If property has external storage, it returns a vector with data in case of array type or a single value.
        //! Otherwise, getReference is used.
        //! \return copy of property's data
        [[ nodiscard ]] RawValueType getData() requires (!no_storage)
        {
            if constexpr (external_storage)
            {
                if constexpr (is_external_array)
                {
                    RawValueType ret_value;
                    for(auto i=0; i<size(); ++i)
                    {
                        ret_value.push_back(*getReference(i));
                    }
                    return ret_value;
                }
                else
                {
                    return *getReference();
                }
            }
            else
            {
                return getReference();
            }
        }

        //! Get a copy of property's data. If property has external storage, it returns a vector with data in case of array type or a single value.
        //! Otherwise, getReference is used.
        //! \param selector Selector for PPM data
        //! \return copy of property's data
        [[ nodiscard ]] RawValueType getData(const Selector& selector) requires(PPM && !no_storage)
        {
            if constexpr (external_storage)
            {
                if constexpr (is_external_array)
                {
                    RawValueType ret_value;
                    for(auto i=0; i<size(); ++i)
                    {
                        ret_value.push_back(*getReference(i, selector));
                    }
                    return ret_value;
                }
                else
                {
                    return *getReference(selector);
                }
            }
            else
            {
                return getReference(selector);
            }
        }

        //! For properties with external storage, set address to the structure where the property's data sits.
        //! Internally, passed value is advanced by the field offset set in constructor
        //! \tparam V any structure type
        //! \param ptr pointer to structure
        template <typename V>
        void setPointerBaseAddress(V* ptr) requires(external_storage)
        {
            m_value = reinterpret_cast<UpdatedT>(offsetPointer(ptr));
        }

        //! For properties with external storage of array, set the number of bytes which separate two values in an array.
        //! By default, it's a size of single element
        //! \param step offset between two values in an array in bytes
        void setStep(std::size_t step) requires(is_external_array)
        {
            m_step = step;
        }

        //! For PPM properties with external storage, set the number of bytes which separate two values for different users.
        //! By default, it's a size of single element
        //! \param ppm_offset offset between two values for PPM
        void setPointerPPMOffset(std::size_t ppm_offset) requires(external_storage && PPM)
        {
            m_ppm_step = ppm_offset;
        }

        //! For properties with external storage, set the number of bytes which separate two values for subdevices.
        //! By default, it's a size of single element
        //! \param subdevice_offset offset between two subdevice values
        void setPointerSubDeviceOffset(std::size_t subdevice_offset) requires(external_storage)
        {
            m_subdevice_step = subdevice_offset;
        }

        //! For properties with external storage, set a subdevice ID for current property instance. By default it's 0.
        //! \param subdevice_id subdevice ID
        void setSubDeviceID(std::size_t subdevice_id) requires(external_storage)
        {
            m_subdevice_id = subdevice_id;
        }

        //! Get size of a property
        //! \return size of property (number of elements)
        PropertySizeType size() const
        {
            if constexpr (is_external_array)
            {
                if (m_size_ptr != nullptr)
                {
                    return *m_size_ptr;
                }
            }
            return m_size;
        }

        //! Set size of a property
        //! \param size new size of a property (number of elements)
        void setSize(PropertySizeType size)
        {
            if constexpr (is_external_array)
            {
                if (m_size_ptr != nullptr)
                {
                    *m_size_ptr = size;
                }
            }
            else
            {
                m_size = size;
            }
        }

        //! For properties with external storage, its size might be stored outside of a property.
        //! \param size_ptr pointer to external size of property
        void setSizePointer(PropertySizeType* size_ptr) requires(is_external_array)
        {
            m_size_ptr = size_ptr;
        }

        //! Execute a callback on this property
        //! \param callback callback
        void forEach(const PropertyCallback& callback) override
        {
            callback(*this);
        }

        //! Check if this property is a parent
        //! \return false
        bool isParent() const override
        {
            return false;
        }

        //! Set a get function
        //! \param func GetFunction
        void setGetFunction(GetFunc func)
        {
            m_get_function = std::move(func);
        }

        //! Set a set function
        //! \param func SetFunction
        void setSetFunction(SetFunc func)
        {
            m_set_function = std::move(func);
        }

        //! Set a set function which takes exactly the type of the property's value
        //! \param func SetFunction
        void setSetFunctionExact(SetFuncExact func)
        {
            m_set_function_exact = std::move(func);
        }

        //! Set a function that will be called when the property's value change. Useful when automatic (default) set function is sufficient.
        //! \param func OnChangeFunction
        void setOnChangeFunction(OnChangeFunc func)
        {
            m_on_change_function = std::move(func);
        }

        //! Set limits of a property
        //! \param limits property's limits
        void setLimit(PropertyLimits<LimitsDataType> limits)
        {
            m_limits = std::move(limits);
        }

        //! Get limits of a property
        //! \return optional with property limits
        const std::optional<PropertyLimits<LimitsDataType>>& getLimit() const
        {
            return m_limits;
        }

        //! For non-PPM properties, notify about a value change (for example when the change occurred externally of property - external storage)
        void notify() requires (!PPM)
        {
            root().notify(*this, NoSelector);
        }

        //! For PPM properties, notify about a value change (for example when the change occurred externally of property - external storage)
        //! \param selector Selector
        void notify(const Selector& selector)
        {
            root().notify(*this, selector);
        }

        //! For non-PPM properties, call on change function without a selector.
        void callOnChange() requires (!PPM)
        {
            callOnChange(NoSelector);
        }

        //! For PPM properties, call on change function with a selector.
        //! \param selector Selector
        void callOnChange(const Selector& selector)
        {
            if (m_on_change_function)
            {
                m_on_change_function(*this, selector);
            }
        }

        //! For PPM properties, get a value from a property (no variant)
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return GetFuncResults (Result object)
        [[ nodiscard ]] GetFuncResult get(const Selector& selector, const Filters& filters = {}, const Options& options = {}) requires(PPM)
        {
            return dispatchGetOnFunctionSource(selector, filters, options);
        }

        //! For non-PPM properties, get a value from a property (no variant)
        //! \param filters Filters
        //! \param options Options
        //! \return GetFuncResults (Result object)
        [[ nodiscard ]] GetFuncResult get(const Filters& filters = {}, const Options& options = {}) requires(!PPM)
        {
            return dispatchGetOnFunctionSource(NoSelector, filters, options);
        }

        //! For PPM properties, set a value (no variant)
        //! \param value value to set
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult set(RawValueType value, const Selector& selector, const Filters& filters = {}, const Options& options = {}) requires (PPM && !no_value)
        {
            return dispatchSetOnFunctionSource(std::move(value), selector, filters, options);
        }

        //! For non-PPM properties, set a value (no variant)
        //! \param value value to set
        //! \param filters Filters
        //! \param options Options
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult set(RawValueType value, const Filters& filters = {}, const Options& options = {}) requires (!PPM && !no_value)
        {
            return dispatchSetOnFunctionSource(std::move(value), NoSelector, filters, options);
        }

        //! For non-PPM properties, set a value (with variant)
        //! \param value value to set (variant)
        //! \param filters Filters
        //! \param options Options
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult setGeneric(SetFuncValueType value, const Filters& filters = {}, const Options& options = {}) requires (!PPM)
        {
            return setGeneric(std::move(value), NoSelector, filters, options);
        }

        //! For PPM properties, set for properties without stored value
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult set(const Selector& selector, const Filters& filters = {}, const Options& options = {}) requires (PPM && no_value)
        {
            return setGeneric(std::monostate(), selector, filters, options);
        }

        //! For non-PPM properties, set for properties without stored value
        //! \param filters Filters
        //! \param options Options
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult set(const Filters& filters = {}, const Options& options = {}) requires (!PPM && no_value)
        {
            return setGeneric(std::monostate(), filters, options);
        }

        //! Generic set for properties (override from PropertyItem)
        //! \param value value to set (variant)
        //! \param selector Selector
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult setGeneric(SetFuncValueType value, const Selector& selector) override
        {
            return setGeneric(std::move(value), selector, {}, {});
        }

        //! Generic set for properties (override from PropertyItem)
        //! \param value value to set (variant)
        //! \param selector Selector
        //! \param filters Filters
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult setGeneric(SetFuncValueType value, const Selector& selector, const Filters& filters) override
        {
            return setGeneric(std::move(value), selector, filters, {});
        }

        //! Generic set for properties (override from PropertyItem)
        //! \param value value to set (variant)
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return SetFuncResults (Result object)
        [[ nodiscard ]] SetFuncResult setGeneric(SetFuncValueType value, const Selector& selector, const Filters& filters, const Options& options) override
        {
            if constexpr (PPM)
            {
                return dispatchSetOnFunctionSource(std::move(value), selector, filters, options);
            }
            else
            {
                return dispatchSetOnFunctionSource(std::move(value), NoSelector, filters, options);
            }
        }

        //! Generic get for properties (override from PropertyItem)
        //! \param selector Selector
        //! \return Result<ValueParent> (Result object)
        [[ nodiscard ]] Result<ValueParent> getGeneric(const Selector& selector) override
        {
            return getGeneric(selector, {}, {});
        }

        //! Generic get for properties (override from PropertyItem)
        //! \param selector Selector
        //! \param filters Filters
        //! \return Result<ValueParent> (Result object)
        [[ nodiscard ]] Result<ValueParent> getGeneric(const Selector& selector, const Filters& filters) override
        {
            return getGeneric(selector, filters, {});
        }

        //! Generic get for properties (override from PropertyItem)
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return Result<ValueParent> (Result object)
        [[ nodiscard ]] Result<ValueParent> getGeneric(const Selector& selector, const Filters& filters, const Options& options) override
        {
            auto result = dispatchGetOnFunctionSource(selector, filters, options);
            if (result.error)
            {
                return Result<ValueParent>{std::move(result.error)};
            }
            ValueParent value_parent;
            if constexpr (no_value)
            {
                IValueItemPtr value = std::make_shared<ValueLeaf>();
                value_parent.add("value", value);
            }
            else
            {
                IValueItemPtr value = std::make_shared<ValueLeaf>(convertValueToVariant(result.value));
                value_parent.add("value", value);
            }
            return Result<ValueParent>{std::move(value_parent)};
        }

        //! Automatic get function (default one)
        //! \param property property to get the data from
        //! \param selector PPM selector
        //! \param filters filters
        //! \param options options
        //! \return Result object with the data
        static GetFuncResult automaticGet(PropertyLeafType& property, const Selector& selector, const Filters& filters, const Options& options)
        {
            if constexpr (no_value)
            {
                return GetFuncResult{};
            }
            else if constexpr (no_storage)
            {
                return GetFuncResult{Error{"Property has no storage and automatic get function was called"}};
            }
            else
            {
                if constexpr (PPM)
                {
                    return GetFuncResult{property.getData(selector)};
                }
                else
                {
                    return GetFuncResult{property.getData()};
                }
            }
        }

        //! Automatic set function (default one) with variant
        //! \param property property to get the data from
        //! \param value value to set (variant)
        //! \param selector PPM selector
        //! \param filters filters
        //! \param options options
        //! \return Result object
        static SetFuncResult automaticSet(PropertyLeafType& property, SetFuncValueType value, const Selector& selector, const Filters& filters, const Options& options) requires(!no_value)
        {
            auto result = getValueFromVariant(value);
            if (result.error)
            {
                return SetFuncResult{std::move(result)};
            }
            return automaticSet(property, std::move(result.value), selector, filters, options);
        }

        //! Automatic set function (default one) with exact type
        //! \param property property to get the data from
        //! \param value value to set (exact type)
        //! \param selector PPM selector
        //! \param filters filters
        //! \param options options
        //! \return Result object
        static SetFuncResult automaticSet(PropertyLeafType& property, RawValueType value, const Selector& selector, const Filters& filters, const Options& options) requires(!no_value)
        {
            // apply filter and options
            auto processed_results = applyFiltersAndOptions(std::move(value), filters, options);
            if (processed_results.error)
            {
                return SetFuncResult{std::move(processed_results.error)};
            }

            // check limits
            const auto& propertyLimits = property.getLimit();
            if (propertyLimits)
            {
                // if constexpr(is_array):
                // checkAndGetIndexes method can be used to get (some or all) indexes of values from processed_results that aren't within the limits
                if (not propertyLimits->check(processed_results.value))
                {
                    return SetFuncResult{Error("Property value outside limits")};
                }
            }
            // check size
            if constexpr (is_external_array)
            {
                if (processed_results.value.size() > MAX_SIZE)
                {
                    return SetFuncResult{Error("Size of data exceeds maximum size of the property")};
                }
                // set size
                property.setSize(processed_results.value.size());
            }

            setData(property, std::move(processed_results.value), selector);

            property.notify(selector);
            property.callOnChange(selector);
            return {};
        }

        //! Automatic set function (default one) for properties without a value
        //! \param property property to get the data from
        //! \param selector PPM selector
        //! \param filters filters
        //! \param options options
        //! \return Result object
        static SetFuncResult automaticSet(PropertyLeafType& property, const Selector& selector, const Filters& filters, const Options& options) requires(no_value)
        {
            // apply filter and options
            // ...
            property.notify(selector);
            property.callOnChange(selector);
            return {};
        }

        static void setData(PropertyLeafType& property, RawValueType value, const Selector& selector) requires(!no_storage)
        {
            if constexpr (PPM)
            {
                if constexpr (external_storage)
                {
                    if constexpr (is_external_array)
                    {
                        for(auto i=0; i<value.size(); ++i)
                        {
                            *property.getReference(i, selector) = std::move(value[i]);
                        }
                    }
                    else
                    {
                        *property.getReference(selector) = std::move(value);
                    }
                }
                else
                {
                    property.getReference(selector) = std::move(value);
                }
            }
            else
            {
                if constexpr (external_storage)
                {
                    if constexpr (is_external_array)
                    {
                        for(auto i=0; i<value.size(); ++i)
                        {
                            *property.getReference(i) = std::move(value[i]);
                        }
                    }
                    else
                    {
                        *property.getReference() = std::move(value);
                    }
                }
                else
                {
                    property.getReference() = std::move(value);
                }
            }
        }


      private:
        GetFuncResult dispatchGetOnFunctionSource(const Selector& selector, const Filters& filters, const Options& options)
        {
            switch (config().get_func)
            {
                case PropertyFunctionSource::automatic:
                {
                    return automaticGet(*this, selector, filters, options);
                }
                case PropertyFunctionSource::limits:
                {
                    // doesn't make quite sense. TODO: warning in sanity check?
                    return GetFuncResult{Error("PropertyFunctionSource::limits set for get method")};
                }
                case PropertyFunctionSource::user:
                {
                    return m_get_function(*this, selector, filters, options);
                }
                case PropertyFunctionSource::warning:
                {
                    return GetFuncResult{Error("PropertyFunctionSource::warning set for get method")};
                }
                default:
                    return GetFuncResult{Error("Unknown PropertyFunctionSource set for get method")};
            }
        }

        template <typename ValueT>
        SetFuncResult dispatchSetOnFunctionSource(ValueT value, const Selector& selector, const Filters& filters, const Options& options)
        requires(std::is_same_v<ValueT, SetFuncValueType> || std::is_same_v<ValueT, RawValueType>)
        {
            switch (config().set_func)
            {
                case PropertyFunctionSource::automatic:
                case PropertyFunctionSource::limits:
                {
                    if constexpr (no_storage)
                    {
                        return SetFuncResult{Error("PropertyFunctionSource::automatic/limits set for set method, but property doesn't have storage!")};
                    }
                    else if constexpr (no_value)
                    {
                        return automaticSet(*this, selector, filters, options);
                    }
                    else
                    {
                        return automaticSet(*this, std::move(value), selector, filters, options);
                    }
                }
                case PropertyFunctionSource::user:
                {
                    // 3 scenarios:
                    //  1. Property doesn't have a value
                    //  2. ValueT is of type SetFuncValueType (variant) so it needs to be converted first
                    //  3. ValueT is of type RawValueType (~T of property) so it doesn't need to be converted
                    if constexpr (no_value)
                    {
                        if (m_set_function)
                        {
                            return m_set_function(*this, selector, filters, options);
                        }
                        if (m_set_function_exact)
                        {
                            return m_set_function_exact(*this, selector, filters, options);
                        }
                        return SetFuncResult{Error("PropertyFunctionSource::user set for set method, but no set/setExact function")};
                    }
                    else if constexpr (std::is_same_v<ValueT, SetFuncValueType>)
                    {
                        if (m_set_function)
                        {
                            return m_set_function(*this, std::move(value), selector, filters, options);
                        }
                        if (m_set_function_exact)
                        {
                            const auto& result = getValueFromVariant(value);
                            if (result.hasError())
                            {
                                return SetFuncResult{std::move(result.error)};
                            }
                            return m_set_function_exact(*this, std::move(result.value), selector, filters, options);
                        }
                        return SetFuncResult{Error("PropertyFunctionSource::user set for set method, but no set function")};
                    }
                    else // ValueT == RawValueType
                    {
                        if (m_set_function_exact)
                        {
                            return m_set_function_exact(*this, std::move(value), selector, filters, options);
                        }
                        return SetFuncResult{Error("PropertyFunctionSource::user set for set method, but no set exact function")};
                    }
                }
                case PropertyFunctionSource::warning:
                {
                    return SetFuncResult{Error("PropertyFunctionSource::warning set for set method")};
                }
                default:
                {
                    return SetFuncResult{Error("Unknown PropertyFunctionSource set for set method")};
                }
            }
        }
        template<typename V>
        V* offsetPointer(V* ptr) requires (external_storage)
        {
            // we have offset in bytes
            char* byte_ptr = reinterpret_cast<char *>(ptr);
            byte_ptr += m_field_offset;
            ptr = reinterpret_cast<V*>(byte_ptr);
            return ptr;
        }

        [[nodiscard]] static GetFuncResult getValueFromVariant(SetFuncValueType variant)
        {
            return std::visit
            (
                overload
                {
                    [](const auto& value)
                    {
                        using LambdaValueType  = std::remove_cvref_t<decltype(value)>;
                        if constexpr (std::is_same_v<RawValueType, LambdaValueType>)
                        {
                            // types match and there's no better overload
                            return Result<RawValueType>{std::move(value)};
                        }
                        else if constexpr (is_std_vector<LambdaValueType>::value)
                        {
                            // argument is a vector
                            if constexpr (is_std_array<RawValueType>::value) // Property holds an array and...
                            {
                                using VecValueType  = typename LambdaValueType::value_type;
                                using Array1DimType = typename RawValueType::value_type;

                                if constexpr (std::is_same_v<VecValueType, Array1DimType>)
                                {
                                    RawValueType arr;
                                    auto result = VectorToArrayConverter(std::move(value), arr);
                                    if(result.error)
                                    {
                                        return Result<RawValueType>{std::move(result.error)};
                                    }
                                    return Result<RawValueType>{std::move(arr)};
                                }
//                                    else if constexpr (is_std_array<Array1DimType>::value)
//                                    {
//                                        // 2D array
//
//                                    }
                            }
                        }
                        return Result<RawValueType>{Error("Templated version failure")};
                    },
                    [](const std::monostate&)
                    {
                        return Result<RawValueType>{Error("Empty value passed to set function")};
                    },
                    [](const std::string& value)
                    {
                        RawValueType dest;
                        auto result = ValueParser::fromString(value, dest);
                        if (result.error)
                        {
                            return Result<RawValueType>(std::move(result.error));
                        }
                        return Result<RawValueType>(std::move(dest));
                    },
                },
                variant
            );
        }

        [[nodiscard]] static ValueType convertValueToVariant(const RawValueType& value)
        {
            if constexpr(is_one_of<RawValueType, ValueType>{})
            {
                return value;
            }
            if constexpr(is_std_array<UpdatedT>::value)
            {
                std::vector<element_type_t<UpdatedT>> vec;
                ArrayToVectorConverter(vec, value);
                return vec;
            }
            if constexpr(requires{value.toString();})
            {
                return value.toString();
            }
            if constexpr(std::is_enum_v<UpdatedT>)
            {
                std::ostringstream oss;
                oss << value;
                return oss.str();
            }
            return ValueType{};
        }

        [[nodiscard]] static GetFuncResult applyFiltersAndOptions(RawValueType value, const Filters& filters, const Options& options)
        {
            return GetFuncResult {std::move(value)};
        }


        [[ nodiscard ]] ReferenceType getPointer(std::size_t idx, const Selector& selector) requires (!no_storage)
        {
            char * ptr = reinterpret_cast<char*>(m_value);
            ptr += m_ppm_step * selector.asNumber();
            ptr += m_subdevice_step * m_subdevice_id;
            ptr += m_step * idx;
            return reinterpret_cast<UpdatedT>(ptr);
        }
        GetFunc m_get_function;
        SetFunc m_set_function;
        SetFuncExact m_set_function_exact;
        OnChangeFunc m_on_change_function;

        std::optional<PropertyLimits<LimitsDataType>> m_limits;
        std::size_t m_field_offset          = 0;
        std::size_t m_step                  = sizeof(std::remove_pointer_t<UpdatedT>);
        std::size_t m_ppm_step              = sizeof(std::remove_pointer_t<UpdatedT>);
        std::size_t m_subdevice_step        = sizeof(std::remove_pointer_t<UpdatedT>);
        std::size_t m_subdevice_id          = 0;
        PropertySizeType m_size             = MAX_SIZE;
        PropertySizeType* m_size_ptr        = nullptr;
        StorageDataType m_value;
    };
}
