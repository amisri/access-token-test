//
// Created by ppluteck on 5/12/23.
//

#pragma once

namespace fgcd
{
    template <typename V>
    struct is_std_array: public std::false_type {};

    template <typename V, std::size_t N>
    struct is_std_array<std::array<V, N>> : public std::true_type {};

    template <typename V>
    struct is_std_vector: public std::false_type {};

    template <typename V>
    struct is_std_vector<std::vector<V>> : public std::true_type {};

    template <typename V>
    using element_type_t = std::remove_reference_t<decltype(*std::begin(std::declval<V&>()))>;

    template <typename T, typename /* unused */ = void>
    struct get_value_type
    {
        using value_type = T;
    };

    template <typename T>
    struct get_value_type<T, std::void_t<typename T::value_type>>
    {
        using value_type = T::value_type;
    };

//
//    template <typename V>
//    constexpr bool is_std_array_or_carray = std::is_array_v<V> || is_std_array<V>::value;

    template<class... Ts> struct overload : Ts... { using Ts::operator()...; };
    template<class... Ts> overload(Ts...) -> overload<Ts...>;

    template <class T, class U> struct is_one_of;

    template <class T, class... Ts>
    struct is_one_of<T, std::variant<Ts...>>
            : std::bool_constant<(std::is_same_v<T, Ts> || ...)>
    { };


    template <typename T, std::size_t N>
    Result<void> VectorToArrayConverter(const std::vector<T>& vec, std::array<T, N>& arr)
    {
        if (vec.size() != N)
        {
            return Result<void>{Error("Size of array and vector doesn't match")};
        }
        for(std::size_t i=0; i<arr.size(); ++i)
        {
            arr[i] = vec[i];
        }
        return {};
    }
    template <typename T, std::size_t N>
    void ArrayToVectorConverter(std::vector<T>& vec, const std::array<T, N>& arr)
    {
        for(const auto& val: arr)
        {
            vec.push_back(val);
        }
    }

    template <typename T>
    struct ArraySizeTrait : std::integral_constant<std::size_t, 1> {};

    template <typename T, std::size_t N>
    struct ArraySizeTrait<std::array<T, N>> : std::integral_constant<std::size_t, N> {};

    template <>
    struct ArraySizeTrait<void> : std::integral_constant<std::size_t, 0> {};

    using PropertiesEmptyType = std::monostate;

    template <typename T>
    struct ReplaceVoidWithEmptyType
    {
        using type = T;
    };

    template <>
    struct ReplaceVoidWithEmptyType<void>
    {
        using type = PropertiesEmptyType;
    };
}
