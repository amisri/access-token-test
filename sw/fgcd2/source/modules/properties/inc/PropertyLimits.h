//! @file
//! @brief  Class that represents property limits.
//! @author Przemyslaw Plutecki


#pragma once

#include <array>
#include <limits>
#include <algorithm>

namespace fgcd
{
    template<typename T>
    class PropertyLimits
    {
      public:
        constexpr PropertyLimits() = default;

        constexpr explicit PropertyLimits(T limit_min, T limit_max) requires std::is_arithmetic_v<T>
        {
            setLimits(limit_min, limit_max);
        }

        constexpr void setLimits(T limit_min, T limit_max)
        {
            if (limit_min > limit_max)
            {
                throw std::invalid_argument("Invalid limits: min > max");
            }
            m_min = limit_min;
            m_max = limit_max;
        }

        constexpr void setMin(T limit_min)
        {
            setLimits(limit_min, m_max);
        }

        constexpr void setMax(T limit_max)
        {
            setLimits(m_min, limit_max);
        }

        constexpr T getMin() const
        {
            return m_min;
        }

        constexpr T getMax() const
        {
            return m_max;
        }

        constexpr auto clamp(const T& value) const
        {
            return std::clamp(value, m_min, m_max);
        }

        [[nodiscard]] constexpr auto check(const T& value) const
        {
            if constexpr (std::is_floating_point_v<T>)
            {
                if (std::isnan(value) || std::isinf(value))
                {
                    return false;
                }
                // Tolerances?
            }
            return value >= m_min && value <= m_max;
        }

        [[nodiscard]] constexpr auto check(std::span<T> values) const
        {
            return std::all_of(
                    values.begin(),
                    values.end(),
                    [this](auto val)
                    {
                        return check(val);
                    });
        }

        template <std::size_t MaxWrongIndexes>
        [[nodiscard]] constexpr auto checkAndGetIndexes(std::span<T> values) const
        {
            std::array<std::size_t, MaxWrongIndexes> indexes;
            std::size_t count = 0;

            for(std::size_t i=0; i<values.size(); ++i)
            {
                if(not check(values[i]))
                {
                    indexes[count++] = i;
                    if (count == MaxWrongIndexes)
                    {
                        break;
                    }
                }
            }
            return indexes;
        }

      private:
        T m_min = std::numeric_limits<T>::min();
        T m_max = std::numeric_limits<T>::max();
    };

    // specialization for std::array

    template<typename T, std::size_t N>
    class PropertyLimits<std::array<T, N>> : public PropertyLimits<T>
    {
      public:
        using PropertyLimits<T>::PropertyLimits;
    };

    // specialization for std::vector

    template<typename T>
    class PropertyLimits<std::vector<T>> : public PropertyLimits<T>
    {
      public:
        using PropertyLimits<T>::PropertyLimits;
    };
}

// EOF
