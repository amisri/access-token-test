//
// Created by ppluteck on 1/26/23.
//

#pragma once

#include <vector>
#include <map>
#include <optional>

#include "PropertyItem.h"


namespace fgcd
{
    class PropertyGroup : public PropertyItem
    {
      public:
        using SetFuncResult         = Result<void>;
        using SetFuncValueType      = ValueType;
        using SetFunc               = std::function<SetFuncResult       (PropertyGroup&, SetFuncValueType, const Selector&, const Filters&, const Options&)>;

        using ItemsStorage       = std::vector<std::reference_wrapper<PropertyItem>>;

        using PropertyItem::PropertyItem;

        //! Execute a callback on this property (on its children)
        //! \param callback callback
        void forEach(const PropertyCallback& callback) override;

        //! Check if this property is a parent
        //! \return true
        bool isParent() const override;

        //! Add PropertyItem (parent or leaf) as a child of this property
        //! \param property_item property (parent or leaf)
        void addItem(PropertyItem& property_item);

        //! For non-PPM properties, set a value (with variant)
        //! \param value value to set (variant)
        //! \param selector Selector
        //! \return Result<void> (empty Result object)
        [[ nodiscard ]] Result<void> setGeneric(ValueType value, const Selector& selector) override
        {
            return setGeneric(std::move(value), selector, {}, {});
        }

        //! For non-PPM properties, set a value (with variant)
        //! \param value value to set (variant)
        //! \param selector Selector
        //! \param filters Filters
        //! \return Result<void> (empty Result object)
        [[ nodiscard ]] Result<void> setGeneric(ValueType value, const Selector& selector, const Filters& filters) override
        {
            return setGeneric(std::move(value), selector, filters, {});
        }

        //! For non-PPM properties, set a value (with variant)
        //! \param value value to set (variant)
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return Result<void> (empty Result object)
        [[ nodiscard ]] Result<void> setGeneric(ValueType value, const Selector& selector, const Filters& filters, const Options& options) override
        {
            if (m_set_function)
            {
                return m_set_function(*this, std::move(value), selector, filters, options);
            }
            return Result<void>{Error("Can't set value for parent property")};
        }

        //! Generic get for properties (override from PropertyItem). Will get from all its children.
        //! \param selector Selector
        //! \return Result<ValueParent> (Result object)
        [[ nodiscard ]] Result<ValueParent> getGeneric(const Selector& selector) override
        {
            return getGeneric(selector, {}, {});
        };

        //! Generic get for properties (override from PropertyItem). Will get from all its children.
        //! \param selector Selector
        //! \param filters Filters
        //! \return Result<ValueParent> (Result object)
        [[ nodiscard ]] Result<ValueParent> getGeneric(const Selector& selector, const Filters& filters) override
        {
            return getGeneric(selector, filters, {});
        };

        //! Generic get for properties (override from PropertyItem). Will get from all its children.
        //! \param selector Selector
        //! \param filters Filters
        //! \param options Options
        //! \return Result<ValueParent> (Result object)
        [[ nodiscard ]] Result<ValueParent> getGeneric(const Selector& selector, const Filters& filters, const Options& options) override
        {
            ValueParent parent_value;
            for(auto& child: m_items)
            {
                auto& child_property = child.get();

                // check for hide flag - if present, skip the property (and its children)
                if (child_property.config().flags.has(PropertyFlags::hide))
                {
                    continue;
                }

                Result<ValueParent> result = child_property.getGeneric(selector, filters, options);
                if (result.error)
                {
                    return Result<ValueParent>{std::move(result.error)};
                }
                parent_value.add(child_property.name().toString(),
                                 std::make_shared<ValueParent>(std::move(result.value)));
            }
            return Result<ValueParent>{std::move(parent_value)};
        }

        //! Set a set function
        //! \param func SetFunction
        void setSetFunction(SetFunc func)
        {
            m_set_function = std::move(func);
        }


        //! Get child properties of this property
        //! \return child properties
        [[ nodiscard ]] const ItemsStorage& items() const
        {
            return m_items;
        }

      private:
        ItemsStorage m_items;
        SetFunc m_set_function;
    };

}




