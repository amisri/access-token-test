//! @file
//! @brief  Class that represents property types.
//! @author Przemyslaw Plutecki


#pragma once

#include <cpp_utils/enumStr.h>

namespace fgcd
{
    enum class PropertyType
    {
        setting,            //< Read-write property
        derived_setting,    //< Read-only property (by clients), its value depend on other setting or derived setting property
        acquisition,        //< Read-only property
        configuration,      //< Read-only property, cannot be modified when the process is running
        method,             //< Write-only property, aka command
    };

    MAKE_ENUM_STR(PropertyTypeStr, PropertyType,
      ENUM_STR(PropertyType, method),
      ENUM_STR(PropertyType, setting),
      ENUM_STR(PropertyType, derived_setting),
      ENUM_STR(PropertyType, acquisition),
      ENUM_STR(PropertyType, configuration)
    );
}