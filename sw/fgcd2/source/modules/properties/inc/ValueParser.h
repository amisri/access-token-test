
#pragma once

#include <functional>
#include <sstream>

#include <cpp_utils/Result.h>


namespace fgcd
{
    struct FG_point
    {
        float                time;               //!< Time since start of the table (first point must have time 0.0)
        float                ref;                //!< Reference value at the specified time
        friend auto operator<=>(const FG_point&, const FG_point&) = default;
    };

    // std::array
    template <typename T, size_t N>
    std::istream& operator>>(std::istream& is, std::array<T, N>& arr)
    {
        if (std::string line{}; std::getline(is, line))
        {
            std::vector<std::string> tokens {};
            std::istringstream iss {line};
            iss.ignore(line.size(), '[');

            std::string token{};
            while (std::getline(iss, token, ','))
            {
                tokens.emplace_back(token);
            }
            if (tokens.size() != N)
            {
                is.setstate(std::ios_base::failbit);
                return is;
            }
            for (std::size_t i = 0; i<tokens.size(); ++i)
            {
                std::istringstream iss_token{tokens[i]};
                iss_token >> arr[i];
            }
        }
        return is;
    }

    template <typename T, size_t N>
    std::ostream& operator<<(std::ostream& os, const std::array<T, N>& arr)
    {
        os << '[';
        auto begin = arr.begin();
        auto end = arr.end();
        if (begin != end)
        {
            os << *begin;
        }
        while (++begin != end)
        {
            os << ',' << *begin;
        }
        os << ']';
        return os;
    }

    // std::vector
    template <typename T>
    std::istream& operator>>(std::istream& is, std::vector<T>& vec)
    {
        if (std::string line{}; std::getline(is, line))
        {
            std::vector<std::string> tokens {};
            std::istringstream iss {line};
            iss.ignore(line.size(), '[');

            std::string token{};
            while (std::getline(iss, token, ','))
            {
                tokens.emplace_back(token);
            }
            vec.reserve(tokens.size());
            for (std::size_t i = 0; i<tokens.size(); ++i)
            {
                std::istringstream iss_token{tokens[i]};
                T val;
                iss_token >> val;
                vec.push_back(std::move(val));
            }
        }
        return is;
    }

    // Point
    std::istream& operator>>(std::istream& is, Point& point)
    {
        std::array<float, 2> arr_float {};
        is >> arr_float;
        point.first     = arr_float[0];
        point.second    = arr_float[1];
        return is;
    }
    std::ostream& operator<<(std::ostream& os, const Point& point)
    {
        os << '[' << point.first << ',' << point.second << ']';
        return os;
    }
    // FG_point
    std::istream& operator>>(std::istream& is, FG_point& point)
    {
        std::array<float, 2> arr_float {};
        is >> arr_float;
        point.time      = arr_float[0];
        point.ref       = arr_float[1];
        return is;
    }
    std::ostream& operator<<(std::ostream& os, const FG_point& point)
    {
        os << '[' << point.time << ',' << point.ref << ']';
        return os;
    }


    struct ValueParser
    {
        template <typename T>
        constexpr static std::function<T(const std::string&)> get_conversion_function()
        {
            if constexpr (std::is_same_v<T, signed char> || std::is_same_v<T, unsigned char>)
            {
                // get same fn as for int.
                return [](const std::string& s){ return std::stoi(s); };
            }
            if constexpr (std::is_same_v<T, int>)
            {
                return [](const std::string& s){ return std::stoi(s); };
            }
            else if constexpr (std::is_same_v<T, long>)
            {
                return [](const std::string& s){ return std::stol(s); };
            }
            else if constexpr (std::is_same_v<T, long long>)
            {
                return [](const std::string& s){ return std::stoll(s); };
            }
            else if constexpr (std::is_same_v<T, unsigned long>)
            {
                return [](const std::string& s){ return std::stoul(s); };
            }
            else if constexpr (std::is_same_v<T, unsigned long long>)
            {
                return [](const std::string& s){ return std::stoull(s); };
            }
            else if constexpr(std::is_same_v<T, float>)
            {
                return [](const std::string& s){ return std::stof(s); };
            }
            else if constexpr (std::is_same_v<T, double>)
            {
                return [](const std::string& s){ return std::stod(s); };
            }
            else if constexpr (std::is_same_v<T, long double>)
            {
                return [](const std::string& s){ return std::stold(s); };
            }
            // no direct match
            else
            {
                return [](const std::string& str) -> T
                {
                    std::istringstream stream(str);
                    T val;
                    stream >> val;
                    if (stream.fail())
                    {
                        throw std::out_of_range("istringstream fail");
                    }
                    return val;
                };
            }
        }

        template <typename T>
        static Result<void> fromString(std::string src, T& dest)
        {
            try
            {
                auto conversion_fn = get_conversion_function<T>();
                dest = conversion_fn(std::move(src));
            }
            catch (const std::exception& exc)
            {
                return Result<void>{Error(exc.what())};
            }
            return {};
        }

    };
}

