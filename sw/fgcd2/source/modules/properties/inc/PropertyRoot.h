//
// Created by ppluteck on 1/26/23.
//

#pragma once

#include <vector>
#include <map>
#include <optional>

#include "PropertyItem.h"


namespace fgcd
{
    class PropertyRoot
    {
      public:
        using NotificationCallback = std::function<void(const PropertyItem&, Selector)>;

        void forEach(const PropertyCallback& callback);

        [[nodiscard]] PropertyItem *find(std::string_view property_full_name) const;

        [[nodiscard]] PropertyItem *find(const PropertyName& property_full_name) const;

        void addItem(PropertyItem& property_item);

        void notify(const PropertyItem& caller, Selector selector) const
        {
            std::for_each(m_notification_callbacks.begin(),
                          m_notification_callbacks.end(),
                          [&caller, &selector](auto& callback)
                          {
                              callback(caller, selector);
                          });
        }

        void notify(const PropertyItem& caller) const
        {
            notify(caller, NoSelector);
        }

        void addNotificationCallback(NotificationCallback callback)
        {
            m_notification_callbacks.push_back(std::move(callback));
        }

      private:
        std::vector<std::reference_wrapper<PropertyItem>> m_items;
        std::map<PropertyName, std::reference_wrapper<PropertyItem>> m_all_items;
        std::vector<NotificationCallback> m_notification_callbacks;
    };
}




