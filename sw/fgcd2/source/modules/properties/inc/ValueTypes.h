
#pragma once

#include <variant>
#include <cpp_utils/enumStr.h>
#include "ValueParser.h"

namespace fgcd
{
    enum class BasicType
    {
        UINT8,
        UINT16,
        UINT32,
        UINT64,
        BOOL,
        INT8,
        INT16,
        INT32,
        INT64,
        FLOAT,
        DOUBLE,
        STRING,
    };

    MAKE_ENUM_STR(BasicTypeStr, BasicType,
                  ENUM_STR(BasicType, UINT8),
                  ENUM_STR(BasicType, UINT16),
                  ENUM_STR(BasicType, UINT32),
                  ENUM_STR(BasicType, UINT64),
                  ENUM_STR(BasicType, BOOL),
                  ENUM_STR(BasicType, INT8),
                  ENUM_STR(BasicType, INT16),
                  ENUM_STR(BasicType, INT32),
                  ENUM_STR(BasicType, INT64),
                  ENUM_STR(BasicType, FLOAT),
                  ENUM_STR(BasicType, DOUBLE),
                  ENUM_STR(BasicType, STRING),
    );

//    using BasicTypeVariant = std::variant<
//            std::monostate,
//            uint8_t,
//            uint16_t,
//            uint32_t,
//            uint64_t,
//            bool,
//            int8_t,
//            int16_t,
//            int32_t,
//            int64_t,
//            float,
//            double,
//            std::string
//    >;
//
//    struct ScalarValue
//    {
//        BasicTypeVariant data;
//    };
//
//    template<typename T>
//    using ArrayTypeContainer = const std::vector<T>;
//
//    template<typename T>
//    using ArrayTypeStorage = std::unique_ptr<ArrayTypeContainer<T>>;
//
//    using ArrayTypeVariant = std::variant<
//            ArrayTypeStorage<uint8_t>,
//            ArrayTypeStorage<uint16_t>,
//            ArrayTypeStorage<uint32_t>,
//            ArrayTypeStorage<uint64_t>,
//            ArrayTypeStorage<bool>,
//            ArrayTypeStorage<int8_t>,
//            ArrayTypeStorage<int16_t>,
//            ArrayTypeStorage<int32_t>,
//            ArrayTypeStorage<int64_t>,
//            ArrayTypeStorage<float>,
//            ArrayTypeStorage<double>,
//            ArrayTypeStorage<std::string>
//    >;
//
//    struct ArrayValue
//    {
//        ArrayTypeVariant data;
//    };
//
//    using AllVariant = std::variant<ScalarValue, ArrayValue>;
//
//    class Data
//    {
//      public:
//        enum class DataType
//        {
//            DUNNO,
//            BASIC,
//            ARRAY,
//            ENUM,
//            COMPLEX,
//            OTHER,
//        };
//
//        explicit Data(DataType kind, AllVariant data, std::string_view complex_data_type = ""):
//        m_kind(kind), m_data(std::move(data)), m_complex_data_type{complex_data_type}
//        {}
//
//        Data() = default;
//
//      private:
//        DataType m_kind = DataType::DUNNO;
//        AllVariant m_data;
//        std::string m_complex_data_type;
//    };
}
