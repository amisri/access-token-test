//! @file
//! @brief  Class that represents a property item.
//! @author Przemyslaw Plutecki


#pragma once

#include <functional>
#include <optional>

#include <cpp_utils/Result.h>

#include <modules/common/inc/ValueParent.h>

#include "PropertyName.h"
#include "PropertyConfig.h"


namespace fgcd
{
    template <typename T>
    using Result = utils::Result<T>;

    using Error = utils::Error;

    //! forward declarations

    class PropertyItem;
    class PropertyRoot;
    class PropertyGroup;

    //! typedefs
    using PropertyCallback = std::function<void(PropertyItem&)>;

    // define some types that will be improved later.

    // filters type
    using Filters = std::string; // TODO: perhaps ds::AnyData?

    // options type
    using Options = std::string; // TODO: perhaps ds::AnyData?

    // selector type
    struct Selector
    {
        using InternalType = int32_t;
        constexpr static InternalType MAX_SELECTOR_ARRAY_SIZE = 32;
        constexpr static InternalType NO_SELECTOR_ID = 0; // TODO: Would be nice to have NO_SELECTOR_ID == 0; Is it possible?
        constexpr Selector() = default;
        constexpr explicit Selector(InternalType selector): m_selector(selector){}

        [[ nodiscard ]] constexpr InternalType asNumber() const
        {
            return m_selector;
        }
        auto operator<=>(const Selector&) const = default;
      private:
        InternalType m_selector = NO_SELECTOR_ID;
    };

    constexpr Selector NoSelector;

    //! Base class representing property item (group or property leaf)
    class PropertyItem
    {
      public:
        //! Create PropertyItem
        //! \param root root property
        //! \param name name of the property
        //! \param config PropertyConfig with configuration of a property
        explicit PropertyItem(PropertyRoot& root, std::string_view name, PropertyConfig config = {});


        //! Create PropertyItem
        //! \param group parent property
        //! \param name name of the property
        //! \param config PropertyConfig with configuration of a property
        explicit PropertyItem(PropertyGroup& group, std::string_view name, PropertyConfig config = {});

        virtual ~PropertyItem()                         = default;

        PropertyItem(const PropertyItem&)               = default;
        PropertyItem(PropertyItem&&)                    = default;

        PropertyItem& operator=(const PropertyItem&)    = delete;
        PropertyItem& operator=(PropertyItem&&)         = delete;

        //! Ger property name (relative to its parent)
        //! \return name
        PropertyName name() const
        {
            return m_name;
        }

        //! Get full property name (absolute name)
        //! \return full property name
        PropertyName fullName() const
        {
            return m_full_name;
        }

        //! Get property root
        //! \return root
        PropertyRoot& root() const
        {
            return m_root;
        }

        //! Get property parent; can be nullptr for parent properties
        //! \return parent
        PropertyGroup* parent() const
        {
            return m_parent;
        }

        //! Get property config
        //! \return config
        const PropertyConfig& config() const
        {
            return m_config;
        }

        //! Generic set function, to be used in callbacks
        //! \param value Value to be set (variant)
        //! \param selector Selector (special type for empty selector)
        //! \return empty Result object
        [[ nodiscard ]] virtual Result<void>        setGeneric(ValueType    value,   const Selector& selector)                                                       = 0;

        //! Generic set function, to be used in callbacks
        //! \param value Value to be set (variant)
        //! \param selector Selector (special type for empty selector)
        //! \param filters Property filters
        //! \return empty Result object
        [[ nodiscard ]] virtual Result<void>        setGeneric(ValueType    value,   const Selector& selector, const Filters& filters)                               = 0;

        //! Generic set function, to be used in callbacks
        //! \param value Value to be set (variant)
        //! \param selector Selector (special type for empty selector)
        //! \param filters Property filters
        //! \param options Property options
        //! \return empty Result object
        [[ nodiscard ]] virtual Result<void>        setGeneric(ValueType    value,   const Selector& selector, const Filters& filters, const Options& options)       = 0;

        //! Generic get function, to be used in callbacks
        //! \param selector Selector (special type for empty selector)
        //! \return Result object with ValueParent (tree structure with variant)
        [[ nodiscard ]] virtual Result<ValueParent> getGeneric(const Selector& selector)                                                                                    = 0;

        //! Generic get function, to be used in callbacks
        //! \param selector Selector (special type for empty selector)
        //! \param filters Property filters
        //! \return Result object with ValueParent (tree structure with variant)
        [[ nodiscard ]] virtual Result<ValueParent> getGeneric(const Selector& selector, const Filters& filters)                                                            = 0;

        //! Generic get function, to be used in callbacks
        //! \param selector Selector (special type for empty selector)
        //! \param filters Property filters
        //! \param options Property options
        //! \return Result object with ValueParent (tree structure with variant)
        [[ nodiscard ]] virtual Result<ValueParent> getGeneric(const Selector& selector, const Filters& filters, const Options& options)                                    = 0;

        //! Function to execute a callback on every child element
        //! \param callback PropertyCallback
        virtual void forEach(const PropertyCallback& callback) = 0;

        //! Function to check if PropertyItem is a parent property
        //! \return bool true if is parent
        virtual bool isParent() const = 0;

      private:
        PropertyConfig  m_config;
        PropertyGroup*  m_parent    = nullptr;
        PropertyRoot&   m_root;

        PropertyName    m_name      = PropertyName{};
        PropertyName    m_full_name = PropertyName{};
    };
}

// EOF
