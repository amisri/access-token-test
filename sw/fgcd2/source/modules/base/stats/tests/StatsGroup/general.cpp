#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/component/inc/RootComponent.h>

#include <gtest/gtest.h>

using namespace fgcd;

class ComponentProxy : public Component
{
public:
    explicit ComponentProxy(Component& parent, std::string name) 
        : Component(parent, name) {}

    void print(std::ostream& os) {
        m_stats_root.print(os);
    }

    StatsGroup& getStats() {
        return m_stats_root;
    }
};

TEST(StatsGroupGeneralTests, emptyStatsGroup)
{
    RootComponent root;
    std::stringstream oss;

    struct : ComponentProxy
    {
        using ComponentProxy::ComponentProxy;

        struct : StatsGroup
        {
            using StatsGroup::StatsGroup;
        } m_stats{m_stats_root, "ethernet"};
    } component(root, "main");

    component.print(oss);

    EXPECT_EQ(
        "Stats: \n"
        "   ethernet\n",
        oss.str());
}


TEST(StatsGroupGeneralTests, groupWithFields)
{
    RootComponent root;
    std::stringstream oss;

    struct : ComponentProxy
    {
        using ComponentProxy::ComponentProxy;

        struct : StatsGroup
        {
            using StatsGroup::StatsGroup;

            StatsField all{*this, "all"};
            StatsField allBad{*this, "all bad"};
        } m_stats{m_stats_root, "ethernet"};
    } component(root, "main");

    component.print(oss);

    EXPECT_EQ(
        "Stats: \n"
        "   ethernet\n"
        "      all = 0\n"
        "      all bad = 0\n",
        oss.str());
}

TEST(StatsGroupGeneralTests, nestedStatsGroups)
{
    RootComponent root;
    std::stringstream oss;

    struct : public ComponentProxy
    {
        using ComponentProxy::ComponentProxy;

        struct : StatsGroup
        {
            using StatsGroup::StatsGroup;

            StatsField all{*this, "all"};
            StatsField allBad{*this, "all bad"};

            struct : StatsGroup
            {
                using StatsGroup::StatsGroup;

                StatsField rx{*this, "rx"};
                StatsField tx{*this, "tx"};
            } good{*this, "good"};

            struct : StatsGroup
            {
                using StatsGroup::StatsGroup;

                StatsField rx{*this, "rx"};
                StatsField tx{*this, "tx"};

                struct : StatsGroup
                {
                    using StatsGroup::StatsGroup;

                    StatsField failed{*this, "failed"};
                    StatsField rejected{*this, "rejected"};
                } reasons{*this, "reasons"};

            } bad{*this, "bad"};

        } m_stats{m_stats_root, "ethernet"};
    } component(root, "main");

    component.print(oss);

    EXPECT_EQ(
        "Stats: \n"
        "   ethernet\n"
        "      all = 0\n"
        "      all bad = 0\n"
        "      good\n"
        "         rx = 0\n"
        "         tx = 0\n"
        "      bad\n"
        "         rx = 0\n"
        "         tx = 0\n"
        "         reasons\n"
        "            failed = 0\n"
        "            rejected = 0\n",
        oss.str());
}

// EOF