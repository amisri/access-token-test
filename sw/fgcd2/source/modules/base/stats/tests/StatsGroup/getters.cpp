#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/component/inc/RootComponent.h>

#include <gtest/gtest.h>

using namespace fgcd;

class ComponentProxy : public Component
{
public:
    explicit ComponentProxy(Component& parent, std::string name)
        : Component(parent, name) {}

    void print(std::ostream& os) {
        m_stats_root.print(os);
    }

    StatsGroup& getStats() {
        return m_stats_root;
    }
};

TEST(StatsGroupGettersTests, emptyRootStatsGroup)
{
    RootComponent root;
    struct : ComponentProxy
    {
        using ComponentProxy::ComponentProxy;
    } component(root, "main");

    auto &stats = component.getStats();

    EXPECT_EQ("",       stats.getName());
    EXPECT_EQ(0,        stats.getChildren().size());
    EXPECT_EQ("",       stats.getHint());
    EXPECT_EQ(true,     stats.isParent());
    EXPECT_EQ(0,        stats.getValue());
}

TEST(StatsGroupGettersTests, emptyStatsGroup)
{
    RootComponent root;
    struct : ComponentProxy
    {
        using ComponentProxy::ComponentProxy;

        struct : StatsGroup
        {
            using StatsGroup::StatsGroup;
        } m_stats{m_stats_root, "ethernet"};
    } component(root, "main");

    auto &stats = component.getStats();

    EXPECT_EQ("",               stats.getName());
    EXPECT_EQ("",               stats.getHint());
    EXPECT_EQ(true,             stats.isParent());
    EXPECT_EQ(0,                stats.getValue());
    ASSERT_EQ(1,                stats.getChildren().size());

    auto childStats = stats.getChildren()[0];
    EXPECT_EQ("ethernet",       childStats->getName());
    EXPECT_EQ(0,                childStats->getChildren().size());
    EXPECT_EQ("",               childStats->getHint());
    EXPECT_EQ(true,             childStats->isParent());
    EXPECT_EQ(0,                childStats->getValue());
}

// EOF