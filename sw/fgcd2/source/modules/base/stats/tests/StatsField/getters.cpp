#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/component/inc/RootComponent.h>

#include <gtest/gtest.h>

using namespace fgcd;

class ComponentProxy : public Component
{
public:
    explicit ComponentProxy(Component& parent) : Component(parent, "test-StatsFieldcomponent"){}

    void print(std::ostream& os) {
        m_stats_root.print(os);
    }

    StatsGroup& getStats() {
        return m_stats_root;
    }
};

TEST(StatsFieldGettersTests, StatsFieldfieldCreation)
{
    RootComponent root;
    ComponentProxy component(root);

    auto &stats = component.getStats();
    StatsField field(stats, "loss");

    EXPECT_EQ("loss",   field.getName());
    EXPECT_EQ(0,        field.getChildren().size());
    EXPECT_EQ("",       field.getHint());
    EXPECT_EQ(false,    field.isParent());
    EXPECT_EQ(0,        field.getValue());
}

TEST(StatsFieldGettersTests, StatsFieldfieldCreationWithHint)
{
    RootComponent root;
    ComponentProxy component(root);
    auto &stats = component.getStats();

    StatsField field(stats, "loss", "important");

    EXPECT_EQ("loss",       field.getName());
    EXPECT_EQ(0,            field.getChildren().size());
    EXPECT_EQ("important",  field.getHint());
    EXPECT_EQ(false,        field.isParent());
    EXPECT_EQ(0,            field.getValue());
}

TEST(StatsFieldGettersTests, StatsFieldassignment)
{
    RootComponent root;
    ComponentProxy component(root);
    auto &stats = component.getStats();

    StatsField field(stats, "loss", "important");
    field = 159;

    EXPECT_EQ("loss",       field.getName());
    EXPECT_EQ(0,            field.getChildren().size());
    EXPECT_EQ("important",  field.getHint());
    EXPECT_EQ(false,        field.isParent());
    EXPECT_EQ(159,          field.getValue());
}

TEST(StatsFieldGettersTests, StatsFieldadditionAssignment)
{
    RootComponent root;
    ComponentProxy component(root);
    auto &stats = component.getStats();

    StatsField field(stats, "loss", "important");
    field = 159;
    field += 100;

    EXPECT_EQ("loss",       field.getName());
    EXPECT_EQ(0,            field.getChildren().size());
    EXPECT_EQ("important",  field.getHint());
    EXPECT_EQ(false,        field.isParent());
    EXPECT_EQ(259,          field.getValue());
}

TEST(StatsFieldGettersTests, StatsFieldpreIncrement)
{
    RootComponent root;
    ComponentProxy component(root);
    auto &stats = component.getStats();

    StatsField field(stats, "loss", "important");
    field = 159;
    StatsField field2 = ++field;

    EXPECT_EQ("loss",       field.getName());
    EXPECT_EQ(0,            field.getChildren().size());
    EXPECT_EQ("important",  field.getHint());
    EXPECT_EQ(false,        field.isParent());
    EXPECT_EQ(160,          field.getValue());

    EXPECT_EQ("loss",       field2.getName());
    EXPECT_EQ(0,            field2.getChildren().size());
    EXPECT_EQ("important",  field2.getHint());
    EXPECT_EQ(false,        field2.isParent());
    EXPECT_EQ(160,          field2.getValue());
}

TEST(StatsFieldGettersTests, StatsFieldpostIncrement)
{
    RootComponent root;
    ComponentProxy component(root);
    auto &stats = component.getStats();

    StatsField field(stats, "loss", "important");
    field = 159;
    StatsField field2 = field++;

    EXPECT_EQ("loss",       field.getName());
    EXPECT_EQ(0,            field.getChildren().size());
    EXPECT_EQ("important",  field.getHint());
    EXPECT_EQ(false,        field.isParent());
    EXPECT_EQ(160,          field.getValue());

    EXPECT_EQ("loss",       field2.getName());
    EXPECT_EQ(0,            field2.getChildren().size());
    EXPECT_EQ("important",  field2.getHint());
    EXPECT_EQ(false,        field2.isParent());
    EXPECT_EQ(159,          field2.getValue());
}

TEST(StatsFieldGettersTests, StatsFielduint64Convertion)
{
    RootComponent root;
    ComponentProxy component(root);
    auto &stats = component.getStats();

    StatsField field(stats, "loss", "important");
    field = 159;

    EXPECT_EQ(159,          static_cast<uint64_t>(field));
}
// EOF