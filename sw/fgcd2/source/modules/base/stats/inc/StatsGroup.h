//! @file
//! @brief  Class that represents a group of counters for statistics.
//! @author Dariusz Zielinski

#pragma once

#include <interfaces/IStatsItem.h>
#include <iostream>
#include "StatsField.h"

namespace fgcd
{
    class RootComponent;

    class StatsGroup: public IStatsItem
    {
        friend RootComponent;
        friend StatsField;

        public:
            std::string_view getName() const override;

            std::string_view getHint() const override;

            uint64_t getValue() const override;

            bool isParent() const override;

            IStatsItemList& getChildren() override;

            StatsGroup(StatsGroup& parent, std::string_view name);

            void print(std::ostream& os = std::cout);

        private:
            StatsGroup();

            void addField(StatsField& field);

            const StatsGroup*    m_parent;
            const std::string    m_name;
                  IStatsItemList m_children;
    };
}

// EOF