//! @file
//! @brief  Class that represents a counter for statistics.
//! @author Dariusz Zielinski

#pragma once

#include <interfaces/IStatsItem.h>

namespace fgcd
{
    class StatsGroup;

    class StatsField : public IStatsItem
    {
        public:
            std::string_view getName() const override;

            std::string_view getHint() const override;

            uint64_t getValue() const override;

            bool isParent() const override;

            IStatsItemList& getChildren() override;

            StatsField(StatsGroup& group, std::string_view name, std::string_view hint = "");

            StatsField& operator=(const uint64_t& value);

            StatsField& operator+=(const uint64_t& amount);

            //! Pre-increment operator
            StatsField& operator++();

            //! Post-increment operator
            StatsField operator++(int) &;

            operator uint64_t() const;

        private:
            const std::string      m_name;
            const std::string_view m_hint;
                  uint64_t         m_counter  = 0;
    };
}

// EOF