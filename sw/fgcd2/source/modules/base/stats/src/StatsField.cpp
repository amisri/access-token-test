//! @file
//! @brief  Class that represents a counter for statistics.
//! @author Dariusz Zielinski

#include "../inc/StatsField.h"
#include "../inc/StatsGroup.h"

using namespace fgcd;

// **********************************************************

std::string_view StatsField::getName() const
{
    return m_name;
}

// **********************************************************

std::string_view StatsField::getHint() const
{
    return m_hint;
}

// **********************************************************

uint64_t StatsField::getValue() const
{
    return m_counter;
}

// **********************************************************

bool StatsField::isParent() const
{
    return false;
}

// **********************************************************

IStatsItemList& StatsField::getChildren()
{
    static IStatsItemList dummy_list;
    return dummy_list;
}

// **********************************************************

StatsField::StatsField(StatsGroup& group, std::string_view name, std::string_view hint)
    : m_name(name), m_hint(hint)
{
    group.addField(*this);
}

// **********************************************************

StatsField& StatsField::operator=(const uint64_t& value)
{
    m_counter = value;
    return *this;
}

// **********************************************************

StatsField& StatsField::operator+=(const uint64_t& amount)
{
    m_counter += amount;
    return *this;
}

// **********************************************************

StatsField& StatsField::operator++()
{
    ++m_counter;
    return *this;
}

// **********************************************************

StatsField StatsField::operator++(int) &
{
    auto copy = *this;
    ++m_counter;
    return copy;
}

// **********************************************************

StatsField::operator uint64_t() const
{
    return m_counter;
}


// EOF