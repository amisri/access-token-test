//! @file
//! @brief  Class that represents a group of counters for statistics.
//! @author Dariusz Zielinski

#include <iostream>

#include <cpp_utils/forEach.h>
#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/core/inc/types.h>

#include "../inc/StatsField.h"

using namespace fgcd;
using namespace utils;

// **********************************************************

std::string_view StatsGroup::getName() const
{
    return m_name;
}

// **********************************************************

std::string_view StatsGroup::getHint() const
{
    return "";
}

// **********************************************************

uint64_t StatsGroup::getValue() const
{
    return 0;
}

// **********************************************************

bool StatsGroup::isParent() const
{
    return true;
}

// **********************************************************

IStatsItemList& StatsGroup::getChildren()
{
    return m_children;
}

// **********************************************************

StatsGroup::StatsGroup(StatsGroup& parent, std::string_view name)
    : m_parent(&parent), m_name(name)
{
    parent.m_children.emplace_back(this);
}

// **********************************************************

StatsGroup::StatsGroup()
    : m_parent(this)
{
}

// **********************************************************

void StatsGroup::addField(StatsField& field)
{
    m_children.emplace_back(&field);
}

void StatsGroup::print(std::ostream& os)
{
    os << "Stats: " ;

    forEach(*this, {}, &IStatsItem::getChildren, [&](IStatsItem* field, std::string indent)
    {
        if (field->isParent())
        {
            os << indent << field->getName() << std::endl;
        }
        else
        {
            os << indent << field->getName() << " = " << field->getValue();

            if (!field->getHint().empty())
            {
                os << " (" << field->getHint() << ")";
            }

            os << std::endl;
        }

        return indent + "   ";

    }, ""s);
}


// EOF