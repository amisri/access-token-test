//! @file
//! @brief  A class representing the root component for all others FGCD components.
//! @author Dariusz Zielinski

#pragma once

#include <memory>

#include <modules/base/stats/inc/StatsGroup.h>
#include <modules/base/clock/inc/Clock.h>
#include "Component.h"

namespace fgcd
{
    class RootComponent : public Component
    {
        friend Component;

        public:
            //! Default constructor
            explicit RootComponent();

            //! A recursive method that performs onStart() for each sub-component in an order in which they were added
            //! and then for this component.
            void start();

            //! A recursive method that performs onStop() for this component and then for each sub-component
            //! in a reverse order that they were added.
            void stop();

            //! This method will log signal reception using logger
            void acknowledgeSignal(int signal_number, std::string_view signal_name);

          protected:
            Clock      m_clock;

          private:
            StatsGroup m_stats_root;
    };

    using RootComponentPtr = std::shared_ptr<RootComponent>;
}

// EOF
