//! @file
//! @brief  A base class for any other FGCD components.
//! @author Dariusz Zielinski

#pragma once

#include <fmt/core.h>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include <cpp_utils/misc.h>
#include <modules/base/clock/inc/Clock.h>

namespace fgcd
{
    //! Forward declarations
    class Component;
    class RootComponent;
    class Logger;
    class StatsGroup;

    using ComponentPtr = std::shared_ptr<Component>;

    // ************************************************************

    class Component : utils::NoncopyableNonmovable
    {
        friend RootComponent;

      public:
        virtual ~Component() = default;

        template<typename SubComponent, typename... Args>
        std::shared_ptr<SubComponent> addComponent(Args&&... args)
        {
            // Check that inserted class has Component class as a base class
            static_assert(
                std::is_base_of_v<Component, SubComponent>,
                "A sub-component class has to derive from the Component class"
            );

            // Create a new sub-component
            auto component = std::make_shared<SubComponent>(*this, std::forward<Args>(args)...);

            // Add this sub-component to the parent's list of sub-components
            m_sub_components.emplace_back(component);

            return component;
        }

        //! @return name of the component
        std::string_view name() const;

        RootComponent& root();

      protected:
        Component(Component& parent, const std::string& name);

        template<typename Format, typename... Args>
        Component(Component& parent, const Format& format, Args&&... args)
            : Component(parent, fmt::vformat(format, fmt::make_format_args(args...)))
        {
        }

        virtual void onStart();

        virtual void onStop();

        Component&                m_parent;
        Clock&                    m_clock;
        std::string               m_name;
        std::vector<ComponentPtr> m_sub_components;
        Logger&                   m_logger;
        StatsGroup&               m_stats_root;
        void*                     m_options    = nullptr;   // TODO void*
        bool                      m_is_running = false;

      private:
        //! This constructor will only be executed by the RootComponent (which is a friend of Component class).
        //! It will create default subcomponents (like Logger) that are used by all other components.
        explicit Component(RootComponent& root);

        // Returns m_sub_components for the purpose of forEach algorithm
        std::vector<ComponentPtr>& getComponents();

        RootComponent& m_root;
    };
}

// EOF
