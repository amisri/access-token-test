#include <modules/base/component/inc/Component.h>
#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/logging/inc/Logger.h>

#include <gtest/gtest.h>
#include <algorithm>

using namespace fgcd;

class StreamStringLogger : public Component, public ILogConsumer
{
    public:
        explicit StreamStringLogger(Component& parent) : Component(parent, "Consumer") {};

        void consumeLog(LogEntryPtr log_entry, std::string_view message) override
        {
            logger.emplace_back(std::string(message)); 
        }

        std::vector<std::string>& getLogs() {
            return logger;
        }

    private:
        std::vector<std::string> logger;
};

class MockComponent : public Component
{
public:
    MockComponent(Component& parent, std::string name) 
    : Component(parent, name) { };
};

class MockRoot : public RootComponent
{
private:
    std::shared_ptr<StreamStringLogger> consumer;
public:
    MockRoot()
    {
        consumer = m_logger.addComponent<StreamStringLogger>();
        m_logger.registerConsumer(consumer);
    };

    std::vector<std::string>& getLogs() {
        return consumer->getLogs();
    }
};

// **********************************************************
// Tests

TEST(componentGeneralTests, createOnStack)
{
    EXPECT_NO_THROW(
        RootComponent root;
    );
}

TEST(componentGeneralTests, createOnHeap)
{
    std::make_shared<RootComponent>();
}

TEST(componentGeneralTests, getName)
{
    RootComponent root;

    EXPECT_EQ("Root", root.name());
}

TEST(componentGeneralTests, getRoot)
{
    RootComponent root;

    EXPECT_EQ(&root, &root.root());
}

TEST(componentGeneralTests, getChildsRoot)
{
    RootComponent root;
    
    auto child1 = root.addComponent<MockComponent>("c1");
    auto child2 = child1->addComponent<MockComponent>("c2");

    EXPECT_EQ(&root, &child2->root());
}

TEST(componentGeneralTests, addChild)
{
    RootComponent root;

    auto child = root.addComponent<MockComponent>("component");

    EXPECT_EQ("component", child->name());
    EXPECT_EQ(&root, &child->root());
}

TEST(componentGeneralTests, acknowledgeSignal)
{
    MockRoot root;

    root.start();
    root.acknowledgeSignal(10, "Test signal log message"); 
    root.stop();
    
    auto &logs = root.getLogs();

    EXPECT_TRUE(std::any_of(logs.begin(), logs.end(), [](const std::string &record) {
        return record.find("Test signal log message") != std::string::npos;
    }));
}

// EOF
