#include <modules/base/component/inc/Component.h>
#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/logging/inc/Logger.h>

#include <gtest/gtest.h>
#include <cpp_utils/enumStr.h>

using namespace fgcd;

// **********************************************************
// Classes to log order of function calls
enum class CallType
{
    onStart,
    onStop
};

struct Calltrace
{
    CallType type;
    std::string_view name;

    bool operator==(const Calltrace& other) const
    {
        return name == other.name && type == other.type;
    }
};

// TODO wrap it into fixture
std::vector<Calltrace> callLogger;

// **********************************************************
// Utilities to provide pretty printing for error messages
MAKE_ENUM_STR(CallTypeStr, CallType,
    ENUM_STR(CallType, onStart),
    ENUM_STR(CallType, onStop),
);

std::ostream& operator<<(std::ostream& os, const Calltrace& object) {
    return os << object.name << ", " << CallTypeStr::toString(object.type);
}


// **********************************************************
// Mocks
class StreamStringLogger : public Component, public ILogConsumer
{
    public:
        explicit StreamStringLogger(Component& parent) : Component(parent, "Consumer") {};

        void consumeLog(LogEntryPtr log_entry, std::string_view message) override
        {
            logger.emplace_back(std::string(message)); 
        }

        std::vector<std::string>& getLogs() {
            return logger;
        }

    private:
        std::vector<std::string> logger;
};

class MockComponent : public Component
{
public:
    MockComponent(Component& parent, std::string name) 
    : Component(parent, name) { };
};

void Component::onStart()
{
    callLogger.push_back({CallType::onStart, m_name});
}

void Component::onStop()
{
    callLogger.push_back({CallType::onStop, m_name});
}

// **********************************************************
// Tests

// Component's tree used for the tests 
//
//                  +-------+                                                                   
//  0 level         | Root  |                                                                   
//                  +---+---+                                                                   
//               +------+-------+                                                             
//               |              |                                                             
//            +--v--+        +--v--+                                                           
//  1 level   | C1  |        | C2  |                                                           
//            +-----+        +-----+                                                           
//               |                                                                            
//            +--v--+                                                                          
//  2 level   | C3  |                                                                   
//            +-----+                                                                          

TEST(componentTests, stopWithoutStart)
{
    RootComponent root;

    auto c1 = root.addComponent<MockComponent>("C1");
    root.addComponent<MockComponent>("C2");
    c1->addComponent<MockComponent>("C3");

    root.stop();

    ASSERT_EQ(0, callLogger.size());
}

TEST(componentTests, startChildren)
{
    RootComponent root;

    auto c1 = root.addComponent<MockComponent>("C1");
    root.addComponent<MockComponent>("C2");
    c1->addComponent<MockComponent>("C3");

    root.start();
    root.stop();

    ASSERT_EQ(8, callLogger.size());
    EXPECT_EQ((Calltrace{CallType::onStart, "C3"}),     callLogger[0]);
    EXPECT_EQ((Calltrace{CallType::onStart, "C1"}),     callLogger[1]);
    EXPECT_EQ((Calltrace{CallType::onStart, "C2"}),     callLogger[2]);
    EXPECT_EQ((Calltrace{CallType::onStart, "Root"}),   callLogger[3]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "C2"}),     callLogger[4]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "C3"}),     callLogger[5]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "C1"}),     callLogger[6]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "Root"}),   callLogger[7]);
}

TEST(componentTests, addComponentAfterStart)
{
    RootComponent root;
    callLogger.clear();

    auto c1 = root.addComponent<MockComponent>("C1");
    root.addComponent<MockComponent>("C2");
    c1->addComponent<MockComponent>("C3");

    root.start();
    root.addComponent<MockComponent>("C4");
    root.stop();

    ASSERT_EQ(8, callLogger.size());
    EXPECT_EQ((Calltrace{CallType::onStart, "C3"}),     callLogger[0]);
    EXPECT_EQ((Calltrace{CallType::onStart, "C1"}),     callLogger[1]);
    EXPECT_EQ((Calltrace{CallType::onStart, "C2"}),     callLogger[2]);
    EXPECT_EQ((Calltrace{CallType::onStart, "Root"}),   callLogger[3]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "C2"}),     callLogger[4]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "C3"}),     callLogger[5]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "C1"}),     callLogger[6]);
    EXPECT_EQ((Calltrace{CallType::onStop,  "Root"}),   callLogger[7]);
}

// EOF