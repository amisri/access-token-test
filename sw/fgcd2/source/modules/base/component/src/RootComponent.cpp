//! @file
//! @brief  A class representing the root component for all others FGCD components.
//! @author Dariusz Zielinski

#include <cpp_utils/forEach.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/RootComponent.h"

using namespace fgcd;
using namespace utils;

// TODO Check why RootComponent is crashing when you create it on stack (instead of dynamic allocation)

// **********************************************************

RootComponent::RootComponent() : Component(*this)
{
    logInfo("Root component created");

    // TODO print info about version
}

// **********************************************************

void RootComponent::start()
{
    if (!m_is_running)
    {
        logInfo("Starting all components");

        // Get a fresh buffer to combine the logs of starting components
        std::string started_components;
        started_components.reserve(1_kB);

        std::string_view starting_component;

        try
        {
            Finally printLog([&]()
            {
                logInfo("Started components: {}", started_components);
            });

            forEach(*this, ForEach::parent_after, &Component::getComponents, [&](Component* component)
            {
                if (!component->m_is_running)
                {
                    starting_component = component->m_name;

                    // Start the component
                    component->onStart();
                    component->m_is_running = true;

                    // Add name of the component to the list of started components
                    fmt::format_to(std::back_inserter(started_components), FMT_STRING(" {} "), component->m_name);
                }
            });
        }
        catch (std::exception& e)
        {
            logFatal("Failed to start the component '{}' with error: {}", starting_component, e.what());

            // Every component is considered critical and any exception will terminate the app (TODO ?)
            throw;
        }
        catch (...)
        {
            logFatal("Failed to start the component '{}'", starting_component);
            throw;
        }
    }
}

// **********************************************************

void RootComponent::stop()
{
    m_clock.unlockAndDisable();

    if (m_is_running)
    {
        logInfo("Stopping all components");

        forEach(*this, ForEach::parent_after | ForEach::reverse, &Component::getComponents, [&](Component* component)
        {
            std::string_view stopping_component;

            try
            {
                if (component->m_is_running)
                {
                    stopping_component = component->m_name;

                    // Stop the component
                    component->onStop();
                    component->m_is_running = false;
                }
            }
            catch (std::exception& e)
            {
                logFatal("Failed to stop the component '{}' with error: {}", stopping_component, e.what());
                // TODO add to std::cerr
            }
            catch (...)
            {
                logFatal("Failed to stop the component '{}'", stopping_component);
                // TODO add to std::cerr
            }
        });
    }
}

// **********************************************************

void RootComponent::acknowledgeSignal(int signal_number, std::string_view signal_name)
{
    logInfo("FGCD application received signal {} ({})", signal_number, signal_name);
}

// EOF
