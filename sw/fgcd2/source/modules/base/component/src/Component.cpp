//! @file
//! @brief  A base class for any other FGCD components.
//! @author Dariusz Zielinski

#include <vector>
#include <iostream>

#include <cpp_utils/forEach.h>
#include <cpp_utils/misc.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/Component.h"
#include "../inc/RootComponent.h"

using namespace fgcd;

// **********************************************************

std::string_view Component::name() const
{
    return m_name;
}

// **********************************************************

RootComponent& Component::root()
{
    return m_root;
}

// **********************************************************


std::vector<ComponentPtr>& Component::getComponents()
{
    return m_sub_components;
}

// **********************************************************

Component::Component(Component& parent, const std::string& name)
    : m_parent(parent), m_clock(parent.m_clock), m_name(name),m_logger(parent.m_logger),
    m_stats_root(parent.m_stats_root), m_root(parent.m_root)
{
}

// **********************************************************

void Component::onStart()
{
    // Empty implementation - to be overridden by a subclasses if needed
}

// **********************************************************

void Component::onStop()
{
    // Empty implementation - to be overridden by a subclasses if needed
}

// **********************************************************

Component::Component(RootComponent &root)
    : m_parent(*this), m_clock(root.m_clock), m_name("Root"), m_logger(*addComponent<Logger>()),
    m_stats_root(root.m_stats_root), m_root(root)
{
}




// EOF
