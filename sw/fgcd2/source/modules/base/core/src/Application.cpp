//! @file
//! @brief  Main FGCD class used to create and store an instance of the root component and to manage the application
//! @author Dariusz Zielinski

#include <iostream>
#include <csignal>

#include "../inc/Application.h"
#include <modules/base/component/inc/RootComponent.h>

using namespace fgcd;

// ************************************************************

RootComponentPtr     Application::m_root_component = nullptr;
ApplicationResetType Application::m_reset_type     = ApplicationResetType::none;
std::mutex           Application::m_root_component_mutex;

// ************************************************************

void Application::initialize(int argc, char *argv[])
{
    std::scoped_lock lock(m_root_component_mutex);

    if (m_root_component == nullptr)
    {
        std::cerr << "Starting the FGCD application" << std::endl;

        m_root_component = createRootComponent(argc, argv);
        m_root_component->start();
    }
}

// ************************************************************

void Application::terminate()
{
    std::scoped_lock lock(m_root_component_mutex);

    if (m_root_component != nullptr)
    {
        m_root_component->stop();
        m_root_component = nullptr;

        std::cerr << "FGCD application terminated" << std::endl;
    }
}

// ************************************************************

void Application::reset(const ApplicationResetType &reset_type)
{
    m_reset_type = reset_type;
    raise(SIGTERM);
}

// ************************************************************

ApplicationResetType Application::resetType()
{
    return m_reset_type;
}

// ************************************************************

void Application::acknowledgeSignal(int signal_number, std::string_view signal_name)
{
    std::scoped_lock lock(m_root_component_mutex);

    if (m_root_component)
    {
        m_root_component->acknowledgeSignal(signal_number, signal_name);
    }
}

// EOF