//! @file
//! @brief  Entry point for the Function Generator Controller Daemon 2 (FGCD2)
//! @author Dariusz Zielinski

#include <algorithm>
#include <csignal>
#include <cstdlib>
#include <exception>
#include <fcntl.h>
#include <iostream>
#include <sys/mman.h>
#include <unistd.h>

// <sys/io.h> is only supported on x86*, Alpha, IA64, and 32-bit ARM
#ifndef __aarch64__
    #include <sys/io.h>
#endif

#include <modules/base/thread/inc/Thread.h>
#include "../inc/Application.h"
#include "../inc/Version.h"
#include "../inc/signals.h"


// ************************************************************
// * Signals handlers
// ************************************************************

//! This handler doesn't do anything, but it unblocks pause() in main function.
static void signalHandler(int signal)
{
    // Convert signal to signal name
    auto found = std::find_if(begin(signals_names), end(signals_names), [&](auto p) { return p.first == signal; });

    auto signal_name = (found != std::end(signals_names) ? found->second : "");

    // Print the received signal number and name to the std output
    std::cerr << "\nFGCD application received signal " << signal << " (" << signal_name << ")" << std::endl;

    // If root component is available, print the received signal using logger
    fgcd::Application::acknowledgeSignal(signal, signal_name);
}

// ************************************************************

//! Register the custom handler to some of the signals
static void registerSignalHandlers()
{
    // Catch signals terminating the application to perform graceful stopping and clean-up
    signal(SIGHUP,  signalHandler);
    signal(SIGINT,  signalHandler);
    signal(SIGQUIT, signalHandler);
    signal(SIGTERM, signalHandler);

    // Ignore SIGPIPE signal
    signal(SIGPIPE, SIG_IGN);
}

// ************************************************************
// * System-wide operations
// ************************************************************

//! Check if the user that runs this application has root privileges and if not - terminate the application.
static void ensureRootUser()
{
    if (geteuid() != 0)
    {
        std::cerr << "Error: FGCD has to be started with root privileges." << std::endl;
        std::exit(1);
    }
}

// ************************************************************

//! Lock memory pages (current and future) in RAM
static void lockMemory()
{
    #ifdef _POSIX_MEMLOCK
        if (mlockall(MCL_CURRENT | MCL_FUTURE))
        {
            perror("mlockall");
            std::exit(1);
        }
    #else
        #warning "Memory locking not supported"
    #endif
}

// ************************************************************

//! Set IO privilege level
static void setIoPrivilegeLevel()
{
    // <sys/io.h> is only supported on x86*, Alpha, IA64, and 32-bit ARM
    #ifndef __aarch64__
        if (iopl(3))
        {
            perror("iopl");
            std::exit(1);
        }
    #endif
}

// ************************************************************

//! Register clean-up function at exit of the program
static void registerAtExit()
{
    if (std::atexit(fgcd::Application::terminate))
    {
        std::cerr << "Failed to register clean-up function with std::atexit()" << std::endl;
        std::exit(1);
    }
}

// ************************************************************
// * Entry point
// ************************************************************

int entryPoint(int argc, char *argv[])
{
    // TODO re-enable this:
    // std::cout << "Starting FGCD 2 version " << fgcd::Version::build_time.tv_sec
    //           << " (" << fgcd::Version::build_time_str << ")" << std::endl;

    // Set thread name of the main thread
    fgcd::CurrentThread::name = "MAIN";

    // Program-wide initialization
    ensureRootUser();
    lockMemory();
    setIoPrivilegeLevel();
    registerSignalHandlers();
    registerAtExit();
    // TODO Anything to do to configure core dumps? setrlimit?

    // Create the FGCD application and the root component instance
    try
    {
        fgcd::Application::initialize(argc, argv);

        // Wait for a signal
        pause();

        // Terminate the application
        fgcd::Application::terminate();

        // Check if this is a result of calling Application::reset()
        if (fgcd::Application::resetType() == fgcd::ApplicationResetType::reset)
        {
            // Set all file descriptors except stdin, stdout and stderr to close on exec
            for (long i = sysconf(_SC_OPEN_MAX) - 1 ; i >= 3 ; i--)
            {
                fcntl(static_cast<int>(i), F_SETFD, FD_CLOEXEC);
            }

            // Execute the FGCD with the original arguments
            execvp(argv[0], argv);

            // This code should not be reachable (after call to execvp)
            perror("execvp");
            std::exit(1);
        }

        if (fgcd::Application::resetType() == fgcd::ApplicationResetType::power_cycle)
        {
            execl("/sbin/reboot", "/sbin/reboot", NULL);
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        std::exit(1);
    }
    catch(...)
    {
        std::exit(1);
    }

    std::exit(0);
}

// EOF
