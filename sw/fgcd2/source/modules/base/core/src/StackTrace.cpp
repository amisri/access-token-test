//! @file
//! @brief  A static class that prints stack trace.
//! @author Dariusz Zielinski

#include <cstdio>
#include <cstdlib>
#include <cxxabi.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <iostream>

#include "../inc/StackTrace.h"

using namespace fgcd;

// **********************************************************

void StackTrace::print(std::ostream& output_stream, const int& skip_firsts)
{
    // This code is based on the library: http://stacktrace.sourceforge.net/

    void* trace[stack_trace_max_depth];
    int   stack_depth = backtrace(trace, stack_trace_max_depth);

    output_stream << "Stack trace:\n";

    for (int i = skip_firsts; i < stack_depth; i++)
    {
        Dl_info dlinfo;

        if (!dladdr(trace[i], &dlinfo))
        {
            break;
        }

        const char* symname = dlinfo.dli_sname;

        int   status;
        char* demangled = abi::__cxa_demangle(symname, nullptr, nullptr, &status);

        if (status == 0 && demangled)
        {
            symname = demangled;
        }

        if (dlinfo.dli_fname && symname)
        {
            output_stream << "   " << (i - skip_firsts) << " : " << dlinfo.dli_fname << " : " << symname << '\n';
        }
        else
        {
            // That probably means that functions symbols were not exported
            break;
        }

        if (demangled)
        {
            free(demangled);
        }
    }
}

// EOF