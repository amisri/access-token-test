//! @file
//! @brief  Main FGCD class used to create and store an instance of the root component and to manage the application.
//! @author Dariusz Zielinski

#pragma once

#include <memory>
#include <mutex>

namespace fgcd
{
    //! Forward declaration of a class for the root component.
    class RootComponent;
    using RootComponentPtr = std::shared_ptr<RootComponent>;

    // ************************************************************

    enum class ApplicationResetType
    {
        none,
        reset,
        power_cycle
    };

    // ************************************************************

    //! Main FGCD class representing the application and holding an instance of the root component.
    //! Root component will be created using createRootComponent() that is not defined by the Application class
    //! and should be defined by each target.
    class Application
    {
        public:
            //! It is OK to execute this function multiple times.
            //! @threadSafe
            static void initialize(int argc, char *argv[]);

            //! It is OK to execute this function multiple times.
            //! @threadSafe
            static void terminate();

            //! After the call to this function, a signal will be raised.
            //! Then the pause() in the main() function will be unblocked and the program will perform requested reset.
            static void reset(const ApplicationResetType& reset_type);

            //! @return a type of reset, selected by the method reset().
            static ApplicationResetType resetType();

            //! This method will try to log signal reception using logger of the root component (only if the root
            //! component is created - otherwise the function will have no effect).
            static void acknowledgeSignal(int signal_number, std::string_view signal_name);

        private:
            static RootComponentPtr createRootComponent(int argc, char *argv[]);

            static std::mutex           m_root_component_mutex;
            static RootComponentPtr     m_root_component;
            static ApplicationResetType m_reset_type;
    };
}

// EOF