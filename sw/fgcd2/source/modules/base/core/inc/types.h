//! @file
//! @brief  Declarations / definitions of a few simple types used across the FGCD framework and related functionalities.
//! @author Dariusz Zielinski

#pragma once

#include <array>
#include <chrono>
#include <cstddef>
#define JSON_DIAGNOSTICS 1
#include <json/json.hpp>
#include <span>
#include <string>
#include <string_view>
#include <vector>

// Enable use of standard C++ literals suffixes
using namespace std::string_literals;
using namespace std::chrono_literals;

// **********************************************************

namespace fgcd
{
    // User-defined literals for memory sizes

    constexpr std::size_t operator""_kB(unsigned long long bytes)
    {
        return 1024u * bytes;
    }

    constexpr std::size_t operator""_MB(unsigned long long bytes)
    {
        return 1024u * 1024u * bytes;
    }

    constexpr std::size_t operator""_GB(unsigned long long bytes)
    {
        return 1024u * 1024u * 1024u * bytes;
    }

    // User-defined literals byte literals

    constexpr std::byte operator""_b(unsigned long long item)
    {
        return std::byte(item);
    }

    constexpr std::byte operator""_b(char item)
    {
        return std::byte(item);
    }

    // **********************************************************

    //! Define default duration type to be standard nanoseconds
    using ClockType = std::chrono::system_clock;
    using TimePoint = ClockType::time_point;
    using Duration  = ClockType::duration;

    //! Define default byte stream types
    using ByteStream     = std::vector<std::byte>;
    using ByteSpan       = std::span<std::byte>;
    using ByteStreamView = std::span<const std::byte>;
    template<std::size_t N>
    using ByteArray = std::array<std::byte, N>;

    //! Define json type
    using Json = nlohmann::json;

    // **********************************************************

    // For some silly, implementation-related reason (see N3512), they omitted operator+ for string + string_view.
    // Before it makes into standard, we add it here.

    inline std::string operator+(const std::string& a, std::string_view b)
    {
        return std::string(a).append(b);
    }

    // **********************************************************

    template<typename T, typename V>
    std::vector<T>& operator+=(std::vector<T>& left, const V& right)
    {
        left.insert(left.end(), right.begin(), right.end());
        return left;
    }

    // **********************************************************

    template<typename T, typename V>
    std::vector<T> operator+(std::vector<T> left, const V& right)
    {
        return left += right;
    }
}

// EOF
