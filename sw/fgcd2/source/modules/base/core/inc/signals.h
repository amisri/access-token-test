//! @file
//! @brief  Function to convert signal number to description - because sigdescr_np() and sigabbrev_np() are not present in current glibc.
//! @author Dariusz Zielinski

#pragma once

#include <csignal>
#include <string>
#include <tuple>

#ifndef SIGUNUSED
    #define SIGUNUSED SIGSYS
#endif

constexpr std::pair<int, std::string_view> signals_names[] =
{
    { SIGABRT,   "SIGABRT"   },
    { SIGALRM,   "SIGALRM"   },
    { SIGBUS,    "SIGBUS"    },
    { SIGCHLD,   "SIGCHLD"   },
    { SIGCLD,    "SIGCLD"    },
    { SIGCONT,   "SIGCONT"   },
    { SIGFPE,    "SIGFPE"    },
    { SIGHUP,    "SIGHUP"    },
    { SIGILL,    "SIGILL"    },
    { SIGINT,    "SIGINT"    },
    { SIGIO,     "SIGIO"     },
    { SIGIOT,    "SIGIOT"    },
    { SIGKILL,   "SIGKILL"   },
    { SIGPIPE,   "SIGPIPE"   },
    { SIGPOLL,   "SIGPOLL"   },
    { SIGPROF,   "SIGPROF"   },
    { SIGPWR,    "SIGPWR"    },
    { SIGQUIT,   "SIGQUIT"   },
    { SIGSEGV,   "SIGSEGV"   },
    { SIGSTKFLT, "SIGSTKFLT" },
    { SIGSTOP,   "SIGSTOP"   },
    { SIGTSTP,   "SIGTSTP"   },
    { SIGSYS,    "SIGSYS"    },
    { SIGTERM,   "SIGTERM"   },
    { SIGTRAP,   "SIGTRAP"   },
    { SIGTTIN,   "SIGTTIN"   },
    { SIGTTOU,   "SIGTTOU"   },
    { SIGUNUSED, "SIGUNUSED" },
    { SIGURG,    "SIGURG"    },
    { SIGUSR1,   "SIGUSR1"   },
    { SIGUSR2,   "SIGUSR2"   },
    { SIGVTALRM, "SIGVTALRM" },
    { SIGXCPU,   "SIGXCPU"   },
    { SIGXFSZ,   "SIGXFSZ"   },
    { SIGWINCH,  "SIGWINCH"  },
};


// EOF