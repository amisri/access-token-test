//! @file
//! @brief  Version class used to get meta-data about this executable (like compilation time etc.)
//! @author Dariusz Zielinski

#pragma once

#include <ctime>

namespace fgcd
{
    //! Struct containing version information
    struct Version
    {
        static const timeval build_time;        //!< Build timestamp
        static const char*   build_time_str;    //!< Human readable build timestamp as a string
        static const char*   hostname;          //!< Host name of machine used for the build
        static const char*   architecture;      //!< Architecture of the machine used for the build
        static const char*   gcc_version;       //!< GCC compiler version
        static const char*   user_group;        //!< User and group that performed the build
        static const char*   directory;         //!< Build directory
        static const char*   ident_info;        //!< Information for ident
    };
}

// EOF