//! @file
//! @brief  A static class that prints stack trace.
//! @author Dariusz Zielinski

#pragma once

#include <iostream>

namespace fgcd
{
    class StackTrace
    {
        static constexpr auto stack_trace_max_depth = 32;

        public:
            static void print(std::ostream& output_stream, const int& skip_firsts = 1);
    };
}

// EOF