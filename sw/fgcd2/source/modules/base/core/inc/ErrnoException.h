//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string>
#include <system_error>

namespace fgcd
{
    class ErrnoException : public std::system_error
    {
        public:
            explicit ErrnoException(const std::string& message)
                : std::system_error(errno, std::generic_category(), message)
            {
            }
    };
}

// EOF
