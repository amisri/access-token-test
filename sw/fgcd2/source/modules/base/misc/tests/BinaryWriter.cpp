#include <gtest/gtest.h>

#include <modules/base/core/inc/types.h>
#include <utils/test/utils/utils.h>
#include "../inc/BinaryWriter.h"

using namespace fgcd;

// **********************************************************
// Tests
// **********************************************************

TEST(BinaryWriterTests, BinaryWriter_test1)
{
    // **********************************************************
    // Preparation
    ByteStream buffer;
    auto writer = BigWriter(buffer);


    // **********************************************************
    // Execution
    auto r = writer.write('a', 'b', 'c');


    // **********************************************************
    // Verification
    EXPECT_EQ(r.hasValue(), false);
    EXPECT_EQ(r.error().error_str, "Error");
}

// **********************************************************

TEST(BinaryWriterTests, BinaryWriter_test2)
{
    // **********************************************************
    // Preparation
    ByteStream buffer(10);
    ByteSpan span = buffer;
    auto writer = BigWriter(span);


    // **********************************************************
    // Execution
    writer.write('a', 'b', 'c');


    // **********************************************************
    // Verification
    EXPECT_EQ(writer.position(), 3);
    EXPECT_BINARY_EQ(span, ByteStream({1_b, 2_b, 1_b}));
}

// EOF
