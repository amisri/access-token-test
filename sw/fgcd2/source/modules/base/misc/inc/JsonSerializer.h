//! @file
//! @brief  Serializer for the JSON library.
//! @author Dariusz Zielinski

#pragma once

#include <chrono>

namespace fgcd
{
    class JsonSerializer
    {
        public:
            template<typename... Args>
            static json& serialize(json& data, Args&&... args)
            {
                ((data[args.name] = printJson(args.obj) ), ...);

                return data;
            }

        private:
            template<typename T>
            auto printJson(const T& object)
            {
                if constexpr(is_serializable_v<T>)
                {
                    json data;
                    object.serialize<JsonSerializer>(data);
                    return data;
                }
                else
                {
                    return object;
                }
            }
    };
}

// EOF