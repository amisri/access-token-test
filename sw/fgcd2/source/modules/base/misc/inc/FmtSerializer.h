//! @file
//! @brief  Serializer for the FMT library.
//! @author Dariusz Zielinski

#pragma once

#include <chrono>
#include <fmt/format.h>


#include <cpp_utils/GenericSerializer.h>

namespace fgcd
{
    class FmtSerializer : GenericSerializer<FmtSerializer>
    {
        public:
            template<typename OutputObject, typename... Args>
            static auto& serialize(OutputObject& out, Args&&... args)
            {
                size_t index = 0;

                fmt::format_to(out, "{{");
                (( format_to(out, "{} = ", args.name), serializeHelper(out, args.obj, (++index) == sizeof...(args)) ), ...);
                fmt::format_to(out, "}}");

                return out;
            }

            template<typename OutputObject, typename T>
            static void print(OutputObject& out, const T& object)
            {
                fmt::format_to(out, "{}", object);
            }
    };
}

template <typename T, typename Char>
struct fmt::formatter<T, Char, enable_if_t<utils::is_serializable_v<T>> > : fmt::formatter<string_view>
{
    template <typename FormatContext>
    auto format(T obj, FormatContext& ctx)
    {
        auto out = ctx.out();
        return obj.serialize<fgcd::FmtSerializer>(out);
    }
};

// EOF