//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <system_error>
#include <type_traits>
#include <zpp_bits.h>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include "BinarySerializationUtils.h"

namespace fgcd
{
    template<std::endian Endiannes>
    class BinaryReader
    {
      public:
        BinaryReader(ByteStreamView bytes)
            : m_reader(serialization::make_deserializer<Endiannes>(bytes)),
              m_buffer(bytes)
        {
        }

        template<typename T>
        utils::Result<T, std::string> read()
        {
            T    object;
            auto result = m_reader(object);

            if(result != std::errc{})
            {
                return utils::Failure(serialization::getError(result));
            }

            return object;
        }

        template<typename T>
            requires requires(T object) { object.reserve(1); }
        utils::Result<T, std::string> readContainer(std::size_t count)
        {
            T object;
            object.resize(count);

            auto result = m_reader(object);

            if(result != std::errc{})
            {
                return utils::Failure(serialization::getError(result));
            }

            return object;
        }

        [[nodiscard]] ByteStreamView unreadData() const
        {
            return {m_buffer.data() + m_reader.position(), m_buffer.data() + m_buffer.size()};
        }


      private:
        serialization::Deserializer<Endiannes> m_reader;
        ByteStreamView                         m_buffer;
    };

    using BigReader    = BinaryReader<std::endian::big>;
    using LittleReader = BinaryReader<std::endian::little>;
}

// EOF
