//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <chrono>
#include <fmt/format.h>
#include <optional>
#include <variant>
#include <vector>

#include <cpp_utils/Result.h>
#include <modules/devices/base/inc/Point.h>

// **********************************************************

template<>
struct fmt::formatter<std::chrono::seconds>
{
    template<typename FormatContext>
    auto format(const std::chrono::seconds& second, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{}s", second.count());
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<>
struct fmt::formatter<std::chrono::milliseconds>
{
    template<typename FormatContext>
    auto format(const std::chrono::milliseconds& millisecond, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{}ms", millisecond.count());
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<>
struct fmt::formatter<std::chrono::microseconds>
{
    template<typename FormatContext>
    auto format(const std::chrono::microseconds& microseconds, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{}µs", microseconds.count());
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<>
struct fmt::formatter<fgcd::Point>
{
    template<typename FormatContext>
    auto format(const fgcd::Point& value, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{}|{}", value.time, value.value);
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<>
struct fmt::formatter<std::monostate>
{
    template<typename FormatContext>
    auto format(const std::monostate& value, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "None");
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<typename... Ts>
struct fmt::formatter<std::variant<Ts...>>
{
    template<typename FormatContext>
    auto format(const std::variant<Ts...>& var, FormatContext& ctx)
    {
        return std::visit(
            [&ctx](const auto& value)
            {
                return fmt::format_to(ctx.out(), "{}", value);
            },
            var
        );
    }

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<typename T>
struct fmt::formatter<std::vector<T>>
{
    template<typename FormatContext>
    auto format(const std::vector<T>& vec, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{}", fmt::join(vec, ","));
    }

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<typename T>
struct fmt::formatter<std::optional<T>>
{
    template<typename FormatContext>
    auto format(const std::optional<T>& var, FormatContext& ctx)
    {
        if(var.has_value())
        {
            return fmt::format_to(ctx.out(), "{}", *var);
        }
        return fmt::format_to(ctx.out(), "None");
    }

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// **********************************************************

template<typename T, typename Q>
struct fmt::formatter<std::pair<T, Q>>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }
    template <typename FormatContext>
    auto format(const std::pair<T, Q> pair, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{},{}", pair.first, pair.second);
    }
};

// **********************************************************

template<typename T>
std::string toString(const T& value)
{
    return fmt::to_string(value);
}

// EOF
