//! @file
//! @brief  Defines serializers to serialize any class into string in different formats.
//! @author Dariusz Zielinski

#pragma once

#include <ostream>

#include <cpp_utils/StreamSerializer.h>

namespace fgcd
{
    // TODO is inline here needed? as this is template anyway?

    template<typename T>
    inline enable_if_t<utils::is_serializable_v<T>, std::ostream&>
    operator<<(std::ostream& stream, const T& obj)
    {
        return obj.serialize<StreamSerializer>(stream);
    }
}

// EOF