//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string>
#include <system_error>
#include <zpp_bits.h>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include "BinarySerializationUtils.h"

namespace fgcd
{
    template<std::endian Endiannes>
    class BinaryWriter
    {
      public:
        BinaryWriter(ByteSpan buffer)
            : m_serializer(serialization::make_serializer<Endiannes>(buffer))
        {
        }

        utils::Result<void, std::string> write(auto&&... elements)
        {
            auto result = m_serializer(elements...);
            if (result != std::errc{})
            {
                return utils::Failure{serialization::getError(result)};
            }

            return {};
        }

        [[nodiscard]] std::size_t position() const
        {
            return m_serializer.position();
        }

      private:
        serialization::Serializer<ByteSpan, Endiannes> m_serializer;
    };

    using BigWriter    = BinaryWriter<std::endian::big>;
    using LittleWriter = BinaryWriter<std::endian::little>;
}

// EOF
