//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <bit>
#include <system_error>
#include <utility>
#include <zpp_bits.h>

#include "fmt/core.h"
#include <modules/base/core/inc/types.h>

namespace fgcd::serialization
{
    template<std::endian Endiannes>
    struct Endian
    {
        using Type = void;
    };

    template<>
    struct Endian<std::endian::big>
    {
        using Type = zpp::bits::endian::big;
    };

    template<>
    struct Endian<std::endian::little>
    {
        using Type = zpp::bits::endian::little;
    };

    template<std::endian Endiannes>
    auto make_deserializer(ByteStreamView view)
    {
        return zpp::bits::in(view, zpp::bits::no_size{}, typename Endian<Endiannes>::Type{});
    }

    template<std::endian Endiannes>
    auto make_serializer(auto&& buffer)
    {
        return zpp::bits::out(buffer, zpp::bits::no_size{}, typename Endian<Endiannes>::Type{});
    }

    template<std::endian Endiannes>
    using Deserializer = decltype(make_deserializer<Endiannes>(ByteStreamView()));

    template<typename T, std::endian Endiannes>
    using Serializer = decltype(make_serializer<Endiannes>(std::declval<T>()));

    inline std::string getError(std::errc error)
    {
        if(error == std::errc{})
        {
            return "No error.";
        }

        switch (error)
        {
            case std::errc::result_out_of_range:
                return "Attempting to write or read from a too short buffer.";
            case std::errc::no_buffer_space:
                return "Growing buffer would grow beyond the allocation limits or overflow.";
            case std::errc::value_too_large:
                return "Varinat (variable length integer) encoding is beyond the representation limits.";
            case std::errc::message_size:
                return "Message size is beyond the user defined allocation limits.";
            case std::errc::not_supported:
                return "Attempt to call an RPC that is not listed as supported.";
            case std::errc::bad_message:
                return "Attempt to read a variant of unrecognized type.";
            case std::errc::invalid_argument:
                return "Attempting to serialize null pointer or a value-less variant.";
            case std::errc::protocol_error:
                return "Attempt to deserialize an invalid protocol message.";
            default:
                return fmt::format("Unexpected error code: '{}'.", error);
        }
    }
}

// EOF
