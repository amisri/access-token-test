//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <condition_variable>
#include <ctime>
#include <mutex>

#include <modules/base/core/inc/types.h>

namespace fgcd
{
    class Clock
    {
      public:
        ~Clock();
        TimePoint now();
        void      set(std::timespec time);
        void      waitFor(Duration duration);
        void      waitForTick();
        void      unlockAndDisable();

      private:
        std::condition_variable m_time_set;
        std::mutex              m_mutex;
        TimePoint               m_time{};
        bool                    m_disabled{false};
    };
}

// EOF
