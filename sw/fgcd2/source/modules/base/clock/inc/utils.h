//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <chrono>
#include <ctime>

#include <modules/base/core/inc/types.h>

namespace fgcd
{
    TimePoint     toTimePoint(std::timespec time);
    std::timespec toTimespec(TimePoint timepoint);
    long          getMillisecond(TimePoint timepoint);
    TimePoint     roundDownTimepoint(TimePoint timepoint, std::chrono::milliseconds round_to);
}

// EOF
