#include <future>
#include <gtest/gtest.h>

#include <modules/base/core/inc/types.h>
#include <utils/test/printers/Printers.h>
#include "../inc/utils.h"

using namespace fgcd;
using namespace std::chrono_literals;

// **********************************************************
// Tests
// **********************************************************

TEST(clockUtilsTests, test1)
{
    // **********************************************************
    // Preparation
    TimePoint timepoint{3333ms + 10ns};
    

    // **********************************************************
    // Execution
    auto rounded_timepoint = roundDownTimepoint(timepoint, 20ms);


    // **********************************************************
    // Verification
    EXPECT_EQ(rounded_timepoint, TimePoint{3320ms});
}

// EOF
