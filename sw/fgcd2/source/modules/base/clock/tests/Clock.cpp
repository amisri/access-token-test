#include <atomic>
#include <chrono>
#include <gtest/gtest.h>
#include <iostream>
#include <optional>
#include <ostream>
#include <thread>
#include <type_traits>

#include "../inc/Clock.h"
#include "../inc/utils.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct ClockTests : public ::testing::Test
{
    Clock clock{};
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(ClockTests, testDefaultGet)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution
    auto timepoint = clock.now();


    // **********************************************************
    // Verification
    EXPECT_EQ(timepoint, TimePoint());
}

// **********************************************************

TEST_F(ClockTests, setAndGetTime)
{
    // **********************************************************
    // Preparation
    using namespace std::chrono_literals;
    TimePoint set_timepoint{2001317502s};


    // **********************************************************
    // Execution
    clock.set(toTimespec(set_timepoint));
    auto timepoint = clock.now();


    // **********************************************************
    // Verification
    EXPECT_EQ(timepoint, set_timepoint);
}

// **********************************************************

TEST_F(ClockTests, unlock)
{
    // **********************************************************
    // Preparation
    using namespace std::chrono_literals;
    std::atomic_bool finished = false;
    std::jthread     wait_thread(
        [this, &finished]()
        {
            clock.waitFor(1ms);
            finished = true;
        }
    );


    // **********************************************************
    // Execution
    std::this_thread::sleep_for(10ms);


    // **********************************************************
    // Verification
    EXPECT_EQ(finished, false);


    // **********************************************************
    // Execution
    clock.unlockAndDisable();
    std::this_thread::sleep_for(10ms);


    // **********************************************************
    // Verification
    EXPECT_EQ(finished, true);
}

// **********************************************************

TEST_F(ClockTests, waitFor)
{
    // **********************************************************
    // Preparation
    using namespace std::chrono_literals;
    auto             start    = clock.now();
    std::atomic_bool finished = false;
    std::jthread     wait_thread(
        [this, &finished]()
        {
            clock.waitFor(1ms);
            finished = true;
        }
    );


    // **********************************************************
    // Execution
    std::this_thread::sleep_for(10ms);
    clock.set(toTimespec(start + 500us));


    // **********************************************************
    // Verification
    EXPECT_EQ(finished, false);


    // **********************************************************
    // Execution
    clock.set(toTimespec(start + 1ms));
    std::this_thread::sleep_for(10ms);


    // **********************************************************
    // Verification
    EXPECT_EQ(finished, true);
}

// **********************************************************

TEST_F(ClockTests, waitForTick)
{
    // **********************************************************
    // Preparation
    using namespace std::chrono_literals;
    auto             start    = clock.now();
    std::atomic_bool finished = false;
    std::jthread     wait_thread(
        [this, &finished]()
        {
            clock.waitForTick();
            finished = true;
        }
    );


    // **********************************************************
    // Execution
    std::this_thread::sleep_for(10ms);
    clock.set(toTimespec(start + 10us));
    std::this_thread::sleep_for(10ms);


    // **********************************************************
    // Verification
    EXPECT_EQ(finished, true);
}

// EOF
