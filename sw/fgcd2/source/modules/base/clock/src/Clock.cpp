//! @file
//! @brief
//! @author Adam Solawa

#include <ctime>
#include <mutex>

#include "../inc/Clock.h"
#include "../inc/utils.h"

using namespace fgcd;

// **********************************************************

Clock::~Clock()
{
    unlockAndDisable();
}

// **********************************************************

TimePoint Clock::now()
{
    std::scoped_lock lock(m_mutex);
    return m_time;
}

// **********************************************************

void Clock::set(std::timespec time)
{
    std::scoped_lock lock(m_mutex);
    m_time = toTimePoint(time);
    m_time_set.notify_all();
}

// **********************************************************

void Clock::waitFor(Duration duration)
{
    std::unique_lock lock(m_mutex);

    auto wait_finish = m_time + duration;
    m_time_set.wait(
        lock,
        [this, &wait_finish]
        {
            return m_disabled || m_time >= wait_finish;
        }
    );
}

// **********************************************************

void Clock::waitForTick()
{
    std::unique_lock lock(m_mutex);

    auto wait_finish = m_time;
    m_time_set.wait(
        lock,
        [this, wait_finish]
        {
            return m_disabled || m_time != wait_finish;
        }
    );
}

// **********************************************************

void Clock::unlockAndDisable()
{
    m_disabled = true;
    m_time_set.notify_all();
}

// EOF
