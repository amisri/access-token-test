//! @file
//! @brief
//! @author Adam Solawa

#include <chrono>
#include <ctime>

#include <modules/base/core/inc/types.h>
#include "../inc/utils.h"

using namespace fgcd;

// **********************************************************

TimePoint fgcd::toTimePoint(std::timespec time)
{
    return TimePoint(std::chrono::seconds(time.tv_sec) + std::chrono::nanoseconds(time.tv_nsec));
}

// **********************************************************

std::timespec fgcd::toTimespec(TimePoint timepoint)
{
    auto seconds     = std::chrono::time_point_cast<std::chrono::seconds>(timepoint);
    auto nanoseconds = std::chrono::time_point_cast<std::chrono::nanoseconds>(timepoint) - seconds;

    return {seconds.time_since_epoch().count(), nanoseconds.count()};
}

// **********************************************************

long fgcd::getMillisecond(TimePoint timepoint)
{
    auto duration = timepoint.time_since_epoch();
    return duration_cast<std::chrono::milliseconds>(duration % std::chrono::seconds(1)).count();
}

// **********************************************************

TimePoint fgcd::roundDownTimepoint(TimePoint timepoint, std::chrono::milliseconds round_to)
{
    timepoint = std::chrono::floor<std::chrono::milliseconds>(timepoint);
    auto milliseconds = getMillisecond(timepoint);

    auto reminder = milliseconds % round_to.count();
    if (reminder != 0)
    {
        timepoint -= std::chrono::milliseconds(reminder);
    }

    return timepoint;
}

// EOF
