//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#pragma once

#include <interfaces/ILogConsumer.h>
#include <modules/base/component/inc/Component.h>

#include <fstream>
#include <string>
#include <filesystem>
#include <functional>

namespace fs = std::filesystem;

namespace fgcd
{
    class FileLogger : public Component, public ILogConsumer
    {
        using Predicate = std::function<bool(LogEntryPtr log_entry)>;
        public:
        
            //! Constructor initializes the component and opens log file in append mode.
            //!
            //! @param parent a parent component.
            //! @param path to a directory to store log files.
            //! @param name base of a filename for log files.
            //! @param max_size specifies a maximum size of log file in bytes.
            //! @param num_files specifies a maximum number of log files.
            FileLogger(Component& parent, std::filesystem::path path, const std::string& name, std::size_t max_size, std::size_t num_files=1);

            //! Consumes a received log entry. Provided a log entry is filtered base on predicate and then stored in log file.
            //! Log messages are appended to the log file until the size of a file is smaller than max_size.
            //! If max_size would be exceeded, log files are rotated and a log message is added to the new log file.
            //! If max_size would be exceeded and number_files is one, the old log file is overwritten by the new one.
            //!
            //! @param log_entry a pointer to a struct containing log's infomrations.
            //! @param message a formated log's string.
            void consumeLog(LogEntryPtr log_entry, std::string_view message) override;

            //! Sets a predicate determining if a log entry should be added to log file.
            //!
            //! @param predicate a function that base on LogEntryPtr returns bool specifying if the log entry should be used.
            void setFilter(Predicate predicate);

        private:
            //! Rotates logs files. Change the main log file into '<base_name>.1'.
            //! Following log fileswith names '<base_name>.<number>' are renamed into '<base_name>.<number+1>'.
            //! The last log file according to provided num_files value is removed.
            void doRollover();

            //! Writes message to the log file. A message is written to currently open file, stored in file_handle.
            //! In case of an error during writing to the file, log file is reopened and writing is retried.
            //! After a successful write, log_size is updated accordingly.
            //!
            //! @param message a string to be written.
            void doWrite(std::string_view message);

            //! Opens file in m_log_path in selected mode and saves it to file_handle.
            //! In case of an error during opening, file descriptor is closed and opening is retried after 1s pause.
            //! After successful opening, log_size is initialized with the file's size.
            //!
            //! @param mode specifies file open mode.
            void openLogFile(std::ios_base::openmode mode = std::ios_base::out);


            std::ofstream               m_file_handle;          //!< File handle to the current log file.
            std::size_t                 m_log_size;             //!< Current size of the log file.
            Predicate                   m_predicate;            //!< Filter criteria for received log entry.
            const std::string           m_base_name;            //!< Base filename for log files.
            const fs::path              m_log_dir;              //!< Path to directory for log files.
            const fs::path              m_log_path;             //!< Path to the log file.
            const std::size_t           m_max_size;             //!< Maximum size of log files.
            const std::size_t           m_num_files;            //!< Maximum number of log files.
            static const int            m_error_retries = 5;    //!< Maximum number of retries of I/O operations.
    };
}



// EOF