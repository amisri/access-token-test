//! @file
//! @brief  Class that represents a log entry.
//! @author Dariusz Zielinski

#pragma once

#include <fmt/format.h>
#include <string>

#include <cpp_utils/enumStr.h>
#include <cpp_utils/flagSet.h>
#include <modules/base/core/inc/types.h>

namespace fgcd
{
    enum class LogType
    {
        trace,
        debug,
        info,
        fail,
        warning,
        error,
        fatal,
        exception,
        COUNT
    };

    // clang-format off
    MAKE_ENUM_STR(LogTypeStr, LogType,
        { LogType::trace,     "TRACE"     },
        { LogType::debug,     "DEBUG"     },
        { LogType::info,      "INFO"      },
        { LogType::fail,      "FAIL"      },
        { LogType::warning,   "WARNING"   },
        { LogType::error,     "ERROR"     },
        { LogType::fatal,     "FATAL"     },
        { LogType::exception, "EXCEPTION" }
    );
    // clang-format on

    // **********************************************************

    using LogMemoryBuffer = fmt::basic_memory_buffer<char, 500>;

    // **********************************************************

    struct LogEntry
    {
        LogEntry(
            TimePoint timestamp, LogType log_type, std::string_view file_name, int line, std::string_view component_name
        );

        TimePoint        timestamp;
        LogType          log_type;
        std::string_view file_name;
        int              line;
        std::string_view thread_name;
        std::string_view component_name;
        LogMemoryBuffer  message;
    };

    // **********************************************************

    using LogEntryPtr = std::shared_ptr<LogEntry>;
}

// EOF
