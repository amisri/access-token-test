//! @file
//! @brief  Main logger class used to register logs and manage logging.
//! @author Dariusz Zielinski

#pragma once

#include <fmt/chrono.h>
#include <fmt/ostream.h>
#include <regex>
#include <vector>

#include <cpp_utils/circularBuffer.h>
#include <cpp_utils/templateMap.h>
#include <interfaces/ILogConsumer.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/FmtFormatters.h>
#include <modules/base/thread/inc/Thread.h>
#include <modules/base/thread/inc/threads.h>
#include "LogEntry.h"

// **********************************************************

// Logging macros. They assume compile-time format string and to be used from code of a Component.
#define FGCD_LOG(type, format, ...) \
    m_logger.log<(type)>(__FILE__, __LINE__, m_name, FMT_STRING(format) __VA_OPT__(,) __VA_ARGS__)

#define   logTrace(format, ...)     FGCD_LOG(LogType::trace,   format  __VA_OPT__(,) __VA_ARGS__)
#define   logDebug(format, ...)     FGCD_LOG(LogType::debug,   format  __VA_OPT__(,) __VA_ARGS__)
#define    logInfo(format, ...)     FGCD_LOG(LogType::info,    format  __VA_OPT__(,) __VA_ARGS__)
#define    logFail(format, ...)     FGCD_LOG(LogType::fail,    format  __VA_OPT__(,) __VA_ARGS__)
#define logWarning(format, ...)     FGCD_LOG(LogType::warning, format  __VA_OPT__(,) __VA_ARGS__)
#define   logError(format, ...)     FGCD_LOG(LogType::error,   format  __VA_OPT__(,) __VA_ARGS__)
#define   logFatal(format, ...)     FGCD_LOG(LogType::fatal,   format  __VA_OPT__(,) __VA_ARGS__)

// **********************************************************

namespace fgcd
{
    class Logger : public Component
    {
        public:
            //! Constructor initializing the component
            explicit Logger(Component& parent) : Component(parent, "Logger")
            {
                // Set which log types are enabled by default
                m_enabled_type.value<LogType::trace>()     = false;
                m_enabled_type.value<LogType::debug>()     = false;
                m_enabled_type.value<LogType::info>()      = true;
                m_enabled_type.value<LogType::fail>()      = true;
                m_enabled_type.value<LogType::warning>()   = true;
                m_enabled_type.value<LogType::error>()     = true;
                m_enabled_type.value<LogType::fatal>()     = true;
                m_enabled_type.value<LogType::exception>() = true;
            }

            //! This method will start the log consuming thread
            void onStart() override
            {
                m_thread.start(threads::logger, [this](std::atomic_bool& keep_running)
                {
                    threadFunction(keep_running);
                });
            }

            //! This method will wait some fixed timeout for the thread to consume existing log entries and
            //! then it will stop the thread and join it.
            void onStop() override
            {
                // Let's give some timeout to wait for the queue to flush any log entries that might still be in the queue
                utils::setTimeout([&]() { return m_log_queue.isEmpty(); }, 10ms);

                // Stop the thread, disable the log queue and then join the thread
                m_thread.stop();
                m_log_queue.unblockAndDisable();
                m_thread.forceJoin();
            }

            //! Adds the consumer to the list of log consumers. Each log entry is passed to each log consumer.
            //! This method can only be invoked before the Logger component is started - that is during creation
            //! of components in the RootComponent. Call to this method after the thread is started are ignored.
            //!
            //! @param consumer a log entry consumer that will be added to the list of consumers.
            //! @return true if the consumers was registered, false otherwise (i.e. if invoked after start()).
            bool registerConsumer(const ILogConsumerPtr& consumer)
            {
                // TODO check for null
                if (!m_thread.isRunning())
                {

                    m_consumers.emplace_back(consumer);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // **********************************************************

            template<LogType type>
            void enableLogType(const bool state)
            {
                m_enabled_type.value<type>() = state;
            }

            // **********************************************************

            template<LogType type, typename Format, typename... Args>
            void log(const std::string_view file_name, const int line, const std::string_view component_name,
                     const Format& format, Args&&... args)
            {
                // If the current log type is enabled
                if (m_enabled_type.value<type>())
                {
                    // Get a new entry object
                    // TODO Dynamic allocation control
                    auto entry = std::make_shared<LogEntry>(m_clock.now(), type, file_name, line, component_name);

                    // Try to format the message into entry object or report an error on failure
                    try
                    {
                        fmt::vformat_to(std::back_inserter(entry->message), format, fmt::make_args_checked<Args...>(format, args...));
                    }
                    catch (fmt::format_error& e)
                    {
                        // TODO When there is an error, the "Invalid log format" message is appended at the end of the
                        // entry->message, leaving partial log that failed. If the log format is incorrect, there should only
                        // be an error about invalid log format, without the original message. 
                        entry->log_type = LogType::exception;
                        fmt::format_to(std::back_inserter(entry->message), FMT_STRING("Invalid log format: {}"), e.what());
                    }

                    // Push the entry object into log queue for the logging thread to pass it to log consumers
                    if (!m_log_queue.push(entry))
                    {
                        // TODO add stats that we missed a log message because queue is full
                    }
                }
            }

        private:
            static std::string_view shortenFileName(std::string_view file_name)
            {
                constexpr auto key        = "/modules";
                constexpr auto key_length = std::char_traits<char>::length(key);

                auto index = file_name.find(key);

                return index != std::string::npos ? file_name.substr(index + key_length) : file_name;
            }


            void threadFunction(std::atomic_bool& keep_running)
            {
                LogMemoryBuffer message;
                message.reserve(25_kB);

                while (keep_running)
                {
                    auto optionaL_entry = m_log_queue.popBlock();

                    // Check if entry is in the std::optional
                    if (optionaL_entry)
                    {
                        auto entry = *optionaL_entry;

                        // Clear message buffer
                        message.clear();

                        // Format entry into a message prefix
                        fmt::format_to(std::back_inserter(message), FMT_STRING("[{:%Y-%m-%d %H:%M:}{:%S}] [{}] [{}:{}] [{}::{}] "),
                                    entry->timestamp, entry->timestamp.time_since_epoch(), entry->log_type,
                                    shortenFileName(entry->file_name), entry->line,
                                    entry->thread_name, entry->component_name);

                        // Append actual message to the prefix and a new line character
                        message.append(entry->message);
                        message.push_back('\n');

                        // Consume the log entry by each registered consumer
                        for (auto& consumer : m_consumers)
                        {
                            consumer->consumeLog(entry, { message.data(), message.size() });
                        }
                    }
                }
            }

            Thread                              m_thread;               //!< Thread that pops log entries from log queue.
            utils::CircularBuffer<LogEntryPtr>  m_log_queue;            //!< Circular buffer with log entries to be consumed.
            std::vector<ILogConsumerPtr>        m_consumers;            //!< List of log consumers.
            utils::TemplateMap<LogType, bool>   m_enabled_type{false};  //!< Map that stores which log types are enabled.
    };

}

// EOF
