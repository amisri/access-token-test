//! @file
//! @brief  Log consumer that takes logs and print them to the standard output.
//! @author Dariusz Zielinski

#pragma once

#include <interfaces/ILogConsumer.h>
#include <modules/base/component/inc/Component.h>
#include "../inc/Logger.h"

namespace fgcd
{
    class StdOutLogger : public Component, public ILogConsumer
    {
        public:
            explicit StdOutLogger(Component& parent);

            void consumeLog(LogEntryPtr log_entry, std::string_view message) override;
    };
}

// EOF
