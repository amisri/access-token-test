#include <cerrno>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <gtest/gtest.h>
#include <memory>
#include <string>
#include <sys/mount.h>
#include <vector>

#include <modules/base/component/inc/RootComponent.h>
#include <utils/test/mocks/RootMock.h>
#include "../inc/FileLogger.h"
#include "../inc/Logger.h"

using namespace utils;
using namespace fgcd;
using namespace std::chrono_literals;
namespace fs = std::filesystem;

// **********************************************************
// Setup
// **********************************************************

using LogVector = std::vector<std::string>;

std::optional<LogVector> loadLogs(fs::path const& path)
{
    LogVector     logs;
    std::string   line;
    std::ifstream stream(path);

    if (!stream.is_open())
    {
        return {};
    }

    while (std::getline(stream, line))
    {
        logs.push_back(line + '\n');
    }

    return logs;
}

struct FileLoggingTestsFixture : public ::testing::Test
{
    RootMockPtr                 root;
    std::shared_ptr<Logger>     logger;
    std::shared_ptr<FileLogger> consumer;
    static const fs::path       test_log_dir;

    void SetUp() override
    {
        root   = std::make_unique<RootMock>();
        logger = root->addComponent<Logger>();

        fs::create_directory(test_log_dir);
    }

    void TearDown() override
    {
        fs::remove_all(test_log_dir);
    }

    static void SetUpTestSuite()
    {
        if (fs::exists(test_log_dir))
        {
            fs::remove_all(test_log_dir);
        }

        mount("tmpfs", test_log_dir.c_str(), "tmpfs", 0, "size=4k");
    }

    static void TearDownTestSuite()
    {
        std::cout << "Clearing \n";
        for (int i = 0; i < 4; ++i)
        {
            if (umount(test_log_dir.c_str()) != 0)
            {
                std::this_thread::sleep_for(1s);
                break;
            }
        }
    }
};

const fs::path FileLoggingTestsFixture::test_log_dir = "test_logs";

// **********************************************************
// Tests
// **********************************************************

TEST_F(FileLoggingTestsFixture, singleLog_CheckFileCrated)
{
    fs::path logPath = test_log_dir / "logging";



    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 1000);
    logger->registerConsumer(consumer);

    logger->onStart();
    logger->log<LogType::info>("file.cpp", 666, "component1", "Example message");
    logger->onStop();



    EXPECT_TRUE(fs::exists(logPath));
}


TEST_F(FileLoggingTestsFixture, singleLog_CheckFileContent)
{
    fs::path logPath = test_log_dir / "logging";



    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 1000);
    logger->registerConsumer(consumer);

    logger->onStart();
    logger->log<LogType::info>("file.cpp", 123, "component1", "Example message");
    logger->onStop();



    // LOADING LOGS
    auto logs = loadLogs(logPath).value();

    EXPECT_EQ(1, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:123] [::component1] Example message\n", logs.at(0));
}


TEST_F(FileLoggingTestsFixture, multipleLogs_CheckContent)
{
    fs::path logPath = test_log_dir / "logging";



    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 1000);
    logger->registerConsumer(consumer);

    logger->onStart();
    logger->log<LogType::info>("file.cpp", 1, "component:1", "Example message");
    logger->log<LogType::info>("file.cpp", 2, "component:2", "Example message");
    logger->log<LogType::info>("file.cpp", 3, "component:3", "Example message");
    logger->log<LogType::info>("file.cpp", 4, "component:4", "Example message");
    logger->onStop();



    // LOADING LOGS
    auto logs = loadLogs(logPath).value();

    EXPECT_EQ(4, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:1] [::component:1] Example message\n", logs.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:2] [::component:2] Example message\n", logs.at(1));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:3] [::component:3] Example message\n", logs.at(2));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:4] [::component:4] Example message\n", logs.at(3));
}


TEST_F(FileLoggingTestsFixture, twoLogFiles_CheckFilesCreated)
{
    fs::path logPath = test_log_dir / "logging";
    fs::path logPath2 = test_log_dir / "logging.1";



    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 100, 2);
    logger->registerConsumer(consumer);

    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component:2", "Example message2");
    logger->onStop();



    EXPECT_TRUE(fs::exists(logPath));
    EXPECT_TRUE(fs::exists(logPath2));
}


TEST_F(FileLoggingTestsFixture, twoLogFiles_CheckFilesContent)
{
    fs::path logPath = test_log_dir / "logging";



    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 100, 2);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component:2", "Example message2");
    logger->onStop();



    // LOADING LOGS
    auto logs0 = loadLogs(logPath).value();
    auto logs1 = loadLogs(test_log_dir / "logging.1").value();

    EXPECT_EQ(1, logs0.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file2.cpp:2] [::component:2] Example message2\n", logs0.at(0));
    EXPECT_EQ(1, logs1.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file1.cpp:1] [::component:1] Example message1\n", logs1.at(0));
}


TEST_F(FileLoggingTestsFixture, rotateLog_CheckFilesContent)
{
    fs::path logPath = test_log_dir / "logging";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 200, 2);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component:2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component:3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component:4", "Example message4");
    logger->log<LogType::info>("file5.cpp", 5, "component:5", "Example message5");
    logger->log<LogType::info>("file6.cpp", 6, "component:6", "Example message6");
    logger->onStop();



    // LOADING LOGS
    auto logs0 = loadLogs(logPath).value();
    auto logs1 = loadLogs(test_log_dir / "logging.1").value();

    EXPECT_EQ(2, logs0.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file5.cpp:5] [::component:5] Example message5\n", logs0.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file6.cpp:6] [::component:6] Example message6\n", logs0.at(1));
    EXPECT_EQ(2, logs1.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file3.cpp:3] [::component:3] Example message3\n", logs1.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file4.cpp:4] [::component:4] Example message4\n", logs1.at(1));
}


TEST_F(FileLoggingTestsFixture, multipleLog_CheckFilesCreated)
{
    fs::path logPath0 = test_log_dir / "logging";
    fs::path logPath1 = test_log_dir / "logging.1";
    fs::path logPath2 = test_log_dir / "logging.2";
    fs::path logPath3 = test_log_dir / "logging.3";
    fs::path logPath4 = test_log_dir / "logging.4";
    fs::path logPath5 = test_log_dir / "logging.5";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath0.filename(), 100, 6);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component4", "Example message4");
    logger->log<LogType::info>("file5.cpp", 5, "component5", "Example message5");
    logger->log<LogType::info>("file6.cpp", 6, "component6", "Example message6");
    logger->onStop();



    EXPECT_TRUE(fs::exists(logPath0));
    EXPECT_TRUE(fs::exists(logPath1));
    EXPECT_TRUE(fs::exists(logPath2));
    EXPECT_TRUE(fs::exists(logPath3));
    EXPECT_TRUE(fs::exists(logPath4));
    EXPECT_TRUE(fs::exists(logPath5));
}


TEST_F(FileLoggingTestsFixture, multipleLog_CheckFilesContent)
{
    fs::path logPath0 = test_log_dir / "logging";
    fs::path logPath1 = test_log_dir / "logging.1";
    fs::path logPath2 = test_log_dir / "logging.2";
    fs::path logPath3 = test_log_dir / "logging.3";
    fs::path logPath4 = test_log_dir / "logging.4";
    fs::path logPath5 = test_log_dir / "logging.5";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath0.filename(), 100, 6);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component:2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component:3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component:4", "Example message4");
    logger->log<LogType::info>("file5.cpp", 5, "component:5", "Example message5");
    logger->log<LogType::info>("file6.cpp", 6, "component:6", "Example message6");
    logger->onStop();



    // LOADING LOGS
    auto logs0 = loadLogs(logPath0).value();
    auto logs1 = loadLogs(logPath1).value();
    auto logs2 = loadLogs(logPath2).value();
    auto logs3 = loadLogs(logPath3).value();
    auto logs4 = loadLogs(logPath4).value();
    auto logs5 = loadLogs(logPath5).value();

    EXPECT_EQ(1, logs0.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file6.cpp:6] [::component:6] Example message6\n", logs0.at(0));
    EXPECT_EQ(1, logs1.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file5.cpp:5] [::component:5] Example message5\n", logs1.at(0));
    EXPECT_EQ(1, logs2.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file4.cpp:4] [::component:4] Example message4\n", logs2.at(0));
    EXPECT_EQ(1, logs3.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file3.cpp:3] [::component:3] Example message3\n", logs3.at(0));
    EXPECT_EQ(1, logs4.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file2.cpp:2] [::component:2] Example message2\n", logs4.at(0));
    EXPECT_EQ(1, logs5.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file1.cpp:1] [::component:1] Example message1\n", logs5.at(0));
}


TEST_F(FileLoggingTestsFixture, rotateMultipleLog_CheckFilesCreated)
{
    fs::path logPath0 = test_log_dir / "logging";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath0.filename(), 200, 3);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component4", "Example message4");
    logger->log<LogType::info>("file5.cpp", 5, "component5", "Example message5");
    logger->log<LogType::info>("file6.cpp", 6, "component6", "Example message6");
    logger->log<LogType::info>("file7.cpp", 7, "component7", "Example message7");
    logger->log<LogType::info>("file8.cpp", 8, "component8", "Example message8");
    logger->onStop();



    // Number of files in the log directory
    int num_of_files = std::distance(fs::directory_iterator(test_log_dir), {});

    EXPECT_EQ(3, num_of_files);
    EXPECT_TRUE(fs::exists(test_log_dir / "logging"));
    EXPECT_TRUE(fs::exists(test_log_dir / "logging.1"));
    EXPECT_TRUE(fs::exists(test_log_dir / "logging.2"));
    EXPECT_FALSE(fs::exists(test_log_dir / "logging.3"));
}


TEST_F(FileLoggingTestsFixture, rotateMultipleLog_CheckFilesContent)
{
    fs::path logPath0 = test_log_dir / "logging";
    fs::path logPath1 = test_log_dir / "logging.1";
    fs::path logPath2 = test_log_dir / "logging.2";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath0.filename(), 200, 3);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component:2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component:3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component:4", "Example message4");
    logger->log<LogType::info>("file5.cpp", 5, "component:5", "Example message5");
    logger->log<LogType::info>("file6.cpp", 6, "component:6", "Example message6");
    logger->log<LogType::info>("file7.cpp", 7, "component:7", "Example message7");
    logger->log<LogType::info>("file8.cpp", 8, "component:8", "Example message8");
    logger->onStop();

    // LOADING LOGS
    auto logs0 = loadLogs(logPath0).value();
    auto logs1 = loadLogs(logPath1).value();
    auto logs2 = loadLogs(logPath2).value();



    EXPECT_EQ(2, logs0.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file7.cpp:7] [::component:7] Example message7\n", logs0.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file8.cpp:8] [::component:8] Example message8\n", logs0.at(1));
    EXPECT_EQ(2, logs1.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file5.cpp:5] [::component:5] Example message5\n", logs1.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file6.cpp:6] [::component:6] Example message6\n", logs1.at(1));
    EXPECT_EQ(2, logs2.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file3.cpp:3] [::component:3] Example message3\n", logs2.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file4.cpp:4] [::component:4] Example message4\n", logs2.at(1));
}


TEST_F(FileLoggingTestsFixture, logToExistingFile)
{
    fs::path logPath = test_log_dir / "logging";
    std::ofstream log_file(logPath);
    log_file << "[2000-01-01 10:00:00] [INFO] [file.cpp:1] [::component:1] Example message\n";
    log_file << "[2000-01-01 10:00:00] [INFO] [file.cpp:2] [::component:2] Example message\n";
    log_file.close();



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 400, 3);
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file3.cpp", 3, "component:3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component:4", "Example message4");
    logger->onStop();



    // LOADING LOGS
    auto logs = loadLogs(logPath).value();

    EXPECT_EQ(4, logs.size());
    EXPECT_EQ("[2000-01-01 10:00:00] [INFO] [file.cpp:1] [::component:1] Example message\n", logs.at(0));
    EXPECT_EQ("[2000-01-01 10:00:00] [INFO] [file.cpp:2] [::component:2] Example message\n", logs.at(1));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file3.cpp:3] [::component:3] Example message3\n", logs.at(2));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file4.cpp:4] [::component:4] Example message4\n", logs.at(3));
}


TEST_F(FileLoggingTestsFixture, setFilterAllPredicate)
{
    fs::path logPath = test_log_dir / "logging";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 400, 3);
    consumer->setFilter([](LogEntryPtr log_entry) {
        return false;
    });
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component4", "Example message4");
    logger->onStop();



    // LOADING LOGS
    auto logs = loadLogs(logPath).value();

    EXPECT_EQ(0, logs.size());
}


TEST_F(FileLoggingTestsFixture, consumeOnlyErrorLogs)
{
    fs::path logPath = test_log_dir / "logging";



    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 400, 3);
    consumer->setFilter([](LogEntryPtr log_entry) {
        return log_entry->log_type == LogType::error;
    });
    logger->registerConsumer(consumer);
    
    logger->onStart();
    logger->log<LogType::info>("file1.cpp",     1, "component:1", "Example message1");
    logger->log<LogType::error>("file2.cpp",    2, "component:2", "Example message2");
    logger->log<LogType::warning>("file3.cpp",  3, "component:3", "Example message3");
    logger->log<LogType::debug>("file4.cpp",    4, "component:4", "Example message4");
    logger->log<LogType::error>("file5.cpp",    5, "component:5", "Example message5");
    logger->onStop();



    // LOADING LOGS
    auto logs = loadLogs(logPath).value();

    EXPECT_EQ(2, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [ERROR] [file2.cpp:2] [::component:2] Example message2\n", logs.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [ERROR] [file5.cpp:5] [::component:5] Example message5\n", logs.at(1));
}


TEST_F(FileLoggingTestsFixture, overflowOneLogfile_CheckFilesCreated)
{
    fs::path logPath = test_log_dir / "logging";
    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 100, 1);
    logger->registerConsumer(consumer);



    logger->onStart();
    logger->log<LogType::info>("file.cpp", 1, "component", "Example message");
    logger->log<LogType::info>("file.cpp", 2, "component", "Example message");
    logger->log<LogType::info>("file.cpp", 3, "component", "Example message");
    logger->log<LogType::info>("file.cpp", 4, "component", "Example message");
    logger->onStop();



    // Number of files in the log directory
    int num_of_files = std::distance(fs::directory_iterator(test_log_dir), {});


    EXPECT_EQ(1, num_of_files);
    EXPECT_TRUE(fs::exists(test_log_dir / "logging"));
    EXPECT_FALSE(fs::exists(test_log_dir / "logging.1"));
    EXPECT_FALSE(fs::exists(test_log_dir / "logging.2"));
}


TEST_F(FileLoggingTestsFixture, overflowOneLogfile_CheckFilesContent)
{
    fs::path logPath = test_log_dir / "logging";
    //      Size of log row = 79
    consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 100, 1);
    logger->registerConsumer(consumer);



    logger->onStart();
    logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1");
    logger->log<LogType::info>("file2.cpp", 2, "component:2", "Example message2");
    logger->log<LogType::info>("file3.cpp", 3, "component:3", "Example message3");
    logger->log<LogType::info>("file4.cpp", 4, "component:4", "Example message4");
    logger->onStop();



    // LOADING LOGS
    auto logs = loadLogs(logPath).value();

    EXPECT_EQ(1, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file4.cpp:4] [::component:4] Example message4\n", logs.at(0));
}


TEST_F(FileLoggingTestsFixture, testOpenFaliure)
{
    fs::path logDirectory = test_log_dir / "dir";
    fs::path logPath = logDirectory / "log";



    ASSERT_THROW(std::make_shared<FileLogger>(*root, logDirectory, logPath.filename(), 100, 1), std::ios_base::failure);
}

TEST_F(FileLoggingTestsFixture, testWriteFaliure)
{
    fs::path logPath = test_log_dir / "logging";

    std::ofstream file(logPath);
    std::fill_n(std::ostream_iterator<int>(file), 4_kB, 0);
    file.exceptions(std::ofstream::badbit | std::ofstream::failbit);
    file.close();




    auto consumer = std::make_shared<FileLogger>(*root, test_log_dir, logPath.filename(), 5_kB, 1);
    logger->registerConsumer(consumer);

    logger->onStart();
    // TODO should check warning status, but warning module is not implemented yet
    EXPECT_THROW(logger->log<LogType::info>("file1.cpp", 1, "component:1", "Example message1"), std::ios_base::failure);
    logger->onStop();
}

// EOF
