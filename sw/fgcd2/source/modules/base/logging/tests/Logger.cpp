#include <gtest/gtest.h>
#include <memory>
#include <string>
#include <vector>

#include <modules/base/component/inc/RootComponent.h>
#include <utils/test/mocks/RootMock.h>
#include "../inc/Logger.h"

using namespace utils;
using namespace fgcd;

struct MockLogConsumer : public Component, public ILogConsumer
{
    explicit MockLogConsumer(Component& parent)
        : Component(parent, "consumer"){};

    void consumeLog(LogEntryPtr log_entry, std::string_view message) override
    {
        logs.emplace_back(message);
    }

    std::vector<std::string> logs;
};


struct LoggingTestsFixture : public ::testing::Test
{
    RootMockPtr                      root;
    std::unique_ptr<Logger>          logger;
    std::shared_ptr<MockLogConsumer> consumer;

    void SetUp() override
    {
        root     = std::make_unique<RootMock>();
        logger   = std::make_unique<Logger>(*root);
        consumer = std::make_shared<MockLogConsumer>(*root);

        logger->registerConsumer(consumer);
    }

    void TearDown() override
    {
        logger->onStop();
    }
};


// TESTS


TEST(LoggingTests, registerConsumerSucceed)
{
    auto root     = std::make_unique<RootComponent>();
    auto logger   = std::make_unique<Logger>(*root);
    auto consumer = std::make_shared<MockLogConsumer>(*root);


    EXPECT_TRUE(logger->registerConsumer(consumer));
}


TEST(LoggingTests, registerConsumerAfterStart)
{
    auto root     = std::make_unique<RootComponent>();
    auto logger   = std::make_unique<Logger>(*root);
    auto consumer = std::make_shared<MockLogConsumer>(*root);


    logger->onStart();


    EXPECT_FALSE(logger->registerConsumer(consumer));

    logger->onStop();
}


TEST(LoggingTests, assignNullConsumer)
{
    auto                             root   = std::make_unique<RootComponent>();
    auto                             logger = std::make_unique<Logger>(*root);
    std::shared_ptr<MockLogConsumer> consumer;


    // TODO not working, needs to add implementation
    EXPECT_FALSE(logger->registerConsumer(consumer));
}


TEST_F(LoggingTestsFixture, makeSingleLog)
{
    logger->onStart();
    logger->log<LogType::info>("file.cpp", 123, "component1", "Example message");
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(1, logs.size());
}


TEST_F(LoggingTestsFixture, logAfterStop)
{
    logger->onStart();
    logger->onStop();
    logger->log<LogType::info>("file.cpp", 123, "component1", "Example message");


    auto& logs = consumer->logs;

    EXPECT_EQ(0, logs.size());
}


TEST_F(LoggingTestsFixture, logBeforeStart)
{
    logger->log<LogType::info>("file.cpp", 123, "component1", "Example message");
    logger->onStart();
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(1, logs.size());
}


TEST_F(LoggingTestsFixture, checkLogContent)
{
    logger->onStart();
    logger->log<LogType::info>("logger.cpp", 123, "logComponent", "Testing message");
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(1, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [logger.cpp:123] [::logComponent] Testing message\n", logs.at(0));
}


TEST_F(LoggingTestsFixture, notEnabledLogType)
{
    logger->onStart();
    logger->log<LogType::trace>("file.cpp", 123, "component1", "Example message");
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(0, logs.size());
}


TEST_F(LoggingTestsFixture, enableLogtypeNotIntefereOtherType)
{
    logger->enableLogType<LogType::debug>(false);
    logger->enableLogType<LogType::trace>(true);
    logger->enableLogType<LogType::warning>(true);
    logger->enableLogType<LogType::info>(true);
    logger->enableLogType<LogType::fatal>(true);
    logger->enableLogType<LogType::fail>(true);
    logger->enableLogType<LogType::exception>(true);
    logger->enableLogType<LogType::error>(true);

    logger->onStart();
    logger->log<LogType::debug>("file.cpp", 123, "component1", "Example message");
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(0, logs.size());
}


TEST_F(LoggingTestsFixture, logWithParameter)
{
    logger->onStart();
    logger->log<LogType::info>("param.cpp", 0, "param", "Example message {}", 21);
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(1, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [param.cpp:0] [::param] Example message 21\n", logs.at(0));
}


TEST_F(LoggingTestsFixture, manyLogRecords)
{
    logger->onStart();
    for (int i = 0; i < 200; i++)
        logger->log<LogType::info>("file.cpp", i, "component", "Example message.{}", i);
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(200, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:0] [::component] Example message.0\n", logs.at(0));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:3] [::component] Example message.3\n", logs.at(3));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:60] [::component] Example message.60\n", logs.at(60));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:112] [::component] Example message.112\n", logs.at(112));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:199] [::component] Example message.199\n", logs.at(199));
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [file.cpp:199] [::component] Example message.199\n", logs.at(199));
}


TEST_F(LoggingTestsFixture, invalidFormatLog)
{
    logger->onStart();
    logger->log<LogType::info>("file.cpp", 0, "component", "Example message {", 21);
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(1, logs.size());
    EXPECT_EQ(
        "[1970-01-01 01:00:00] [EXCEPTION] [file.cpp:0] [::component] Invalid log format: invalid format string\n",
        logs.at(0)
    );
}


TEST_F(LoggingTestsFixture, checkEmptyString)
{
    logger->onStart();
    logger->log<LogType::info>("", 123, "", "");
    logger->onStop();


    auto& logs = consumer->logs;

    EXPECT_EQ(1, logs.size());
    EXPECT_EQ("[1970-01-01 01:00:00] [INFO] [:123] [::] \n", logs.at(0));
}


TEST_F(LoggingTestsFixture, multithreadedLogging)
{
    std::atomic_bool         start           = false;
    const int                threads_num     = 4;
    const int                logs_per_thread = 100;
    std::vector<std::thread> threads;


    logger->onStart();

    for (int i = 0; i < threads_num; i++)
    {
        threads.emplace_back((
            [this, i, &start, logs_per_thread]()
            {
                while (!start)
                {
                }

                for (int j = 0; j < logs_per_thread; j++)
                {
                    logger->log<LogType::info>("f.cpp", 0, "c", "Log nr: {:03}", i * logs_per_thread + j);
                }
            }
        ));
    }

    // Wait to setup threads and run them simultaneously
    std::this_thread::sleep_for(5ms);
    start = true;
    // Wait to consume all logs before stopping
    std::this_thread::sleep_for(10ms);
    logger->onStop();

    for (std::size_t i = 0; i < threads.size(); i++)
    {
        if (threads.at(i).joinable())
        {
            threads.at(i).join();
        }
    }


    std::vector<std::string> expected_logs;
    for (int i = 0; i < threads_num; i++)
    {
        for (int j = 0; j < logs_per_thread; j++)
        {
            expected_logs.emplace_back(
                fmt::format("[1970-01-01 01:00:00] [INFO] [f.cpp:0] [::c] Log nr: {:03}\n", i * logs_per_thread + j)
            );
        }
    }

    auto& logs = consumer->logs;

    EXPECT_EQ(threads_num * logs_per_thread, logs.size());
    EXPECT_TRUE(std::is_permutation(begin(logs), end(logs), begin(expected_logs), end(expected_logs)));
}


// EOF
