//! @file
//! @brief  Class that represents a log entry.
//! @author Dariusz Zielinski

#include <modules/base/thread/inc/Thread.h>
#include "../inc/LogEntry.h"

using namespace fgcd;

// **********************************************************

LogEntry::LogEntry(TimePoint timestamp, LogType log_type, std::string_view file_name, int line, std::string_view component_name)
    : timestamp(timestamp), log_type(log_type), file_name(file_name), line(line),
      thread_name(CurrentThread::name), component_name(component_name)
{
}

// EOF