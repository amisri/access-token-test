//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#include <chrono>
#include <fmt/format.h>

#include "../inc/FileLogger.h"

using namespace fgcd;

// **********************************************************

FileLogger::FileLogger(Component& parent, fs::path path, const std::string& name, std::size_t max_size, std::size_t m_num_files)
    : Component(parent, "FileLogger"), m_base_name(name), m_log_dir(path), m_log_path(m_log_dir / m_base_name), m_max_size(max_size), m_num_files(m_num_files)
{
    openLogFile(std::ios_base::app);
}

// **********************************************************

void FileLogger::consumeLog(LogEntryPtr log_entry, std::string_view message)
{
    if (m_predicate)
    {
        if (!m_predicate(log_entry)) return;
    } 

    if (m_log_size + message.length() <= m_max_size)
    {
        doWrite(message);
    }
    else
    {
        if (m_num_files > 1)
        {
            doRollover();
        }

        m_file_handle.close();
        openLogFile();

        doWrite(message);
    }
}

// **********************************************************

void FileLogger::openLogFile(std::ios_base::openmode mode)
{
    using namespace std::chrono_literals;
    int open_tries = 0;

    m_file_handle.open(m_log_path, mode);
    
    while (++open_tries < m_error_retries && m_file_handle.rdstate() == std::ofstream::failbit)
    {
        m_file_handle.close();
        std::this_thread::sleep_for(1s);
        m_file_handle.open(m_log_path, mode);
    }

    if (open_tries == m_error_retries)
    {
        m_file_handle.exceptions(std::ofstream::failbit);
        // TODO log failures to open the file
        // fgcddev.status.st_warnings &= ~FGC_WRN_LOG;
    }

    m_log_size = fs::file_size(m_log_path);
}

// **********************************************************

void FileLogger::doWrite(std::string_view message)
{
    int write_tries = 0;

    m_file_handle.write(message.data(), message.length());

    while (++write_tries < m_error_retries && m_file_handle.rdstate() == std::ofstream::badbit)
    {
        // TODO log failures to write
        // fgcddev.status.st_warnings |= FGC_WRN_LOG;
        m_file_handle.close();
        openLogFile(std::ios_base::app);
        m_file_handle.write(message.data(), message.length());
    }
    if (write_tries == m_error_retries)
    {
        // TODO Issue warning
        // m_file_handle.exceptions(std::ofstream::badbit);
    }

    m_log_size += message.length();
    m_file_handle.flush();
}

// **********************************************************

void FileLogger::doRollover()
{
    for (std::size_t i = m_num_files-2; i > 0; --i)
    {
        fs::path source_file      = m_log_dir / fmt::format("{}.{}", m_base_name, i);
        fs::path destination_file = m_log_dir / fmt::format("{}.{}", m_base_name, i+1);

        if (fs::exists(source_file))
        {
            fs::rename(source_file, destination_file);
        }
    }


    fs::path source_file = m_log_dir / (m_base_name);
    fs::path destination_file = m_log_dir / (m_base_name + ".1");

    if(fs::exists(source_file)) {
        fs::rename(source_file, destination_file);
    }
}

// **********************************************************

void FileLogger::setFilter(Predicate predicate)
{
    this->m_predicate = predicate;
}

// EOF