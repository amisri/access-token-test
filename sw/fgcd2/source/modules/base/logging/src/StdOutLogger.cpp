//! @file
//! @brief  Log consumer that takes logs and print them to the standard output.
//! @author Dariusz Zielinski

#include <cstdio>

#include "../inc/StdOutLogger.h"

using namespace fgcd;

// **********************************************************

StdOutLogger::StdOutLogger(Component& parent) : Component(parent, "StdOutLogger")
{}

// **********************************************************

void StdOutLogger::consumeLog(LogEntryPtr log_entry, std::string_view message)
{
    fwrite(message.data(), 1, message.length(), stdout);
}

// EOF