//! @file
//! @brief  A class to represent exceptions generated by the FGCD.
//! @author Dariusz Zielinski

#include <pthread.h>
#include <sched.h>

#include <cpp_utils/misc.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/types.h>
#include "../inc/Thread.h"
#include "../inc/threads.h"

using namespace fgcd;

// **********************************************************

thread_local std::string_view CurrentThread::name;

// **********************************************************

void Thread::stop()
{
    // This can be used by the thread to stop itself
    m_is_running = false;
}

// **********************************************************

void Thread::forceJoin(const Duration& grace_period)
{
    // First wait for a given grace period to give a chance for the thread to finish by itself
    // (method stop() is intended to be used before forceJoin())
    utils::setTimeout([&](){ return m_finished.load(); }, grace_period);

    // If the tasks haven't finished, cancel it
    if (!m_finished)
    {
        pthread_cancel(m_thread.native_handle());
    }

    // If it's still not joined
    if (m_thread.joinable())
    {
        m_thread.join();
    }
}

// **********************************************************

void Thread::configureAffinity(const ThreadInfo &thread_info)
{
    if (thread_info.affinity() != Affinity::none)
    {
        // Build cpu set
        cpu_set_t cpu_set;
        CPU_ZERO(&cpu_set);
        CPU_SET(static_cast<size_t>(thread_info.affinity()), &cpu_set);

        // Set affinity
        if (pthread_setaffinity_np(m_thread.native_handle(), sizeof(cpu_set), &cpu_set))
        {
            throw Exception("Failed to set thread affinity for thread with name: "s + thread_info.name());
        }
    }
}

// **********************************************************

void Thread::configurePriority(const ThreadInfo &thread_info)
{
    // Fill the sched_param structure with the current data
    int         policy;
    sched_param params;

    pthread_getschedparam(m_thread.native_handle(), &policy, &params);

    // Set proper priority and scheduling policy
    params.sched_priority = thread_info.priority();

    switch (thread_info.policy())
    {
        case SchedulingPolicy::rr:   policy = SCHED_RR;   break;
        case SchedulingPolicy::fifo: policy = SCHED_FIFO; break;
    }

    // Apply changes
    if (pthread_setschedparam(m_thread.native_handle(), policy, &params))
    {
        throw Exception("Failed to set thread priority for thread with name: "s + thread_info.name());
    }
}

// **********************************************************

bool Thread::isRunning() const
{
    return m_is_running;
}

std::optional<Affinity> Thread::getAffinity()
{
    // Build cpu set
    cpu_set_t cpu_set;

    // Get affinity
    if (pthread_getaffinity_np(m_thread.native_handle(), sizeof(cpu_set), &cpu_set))
    {
        return std::nullopt;
    }

    // Map data to Affinity enum
    for (int j = 0; j < CPU_SETSIZE; j++)
        if (CPU_ISSET(j, &cpu_set))
            return std::optional<Affinity>(static_cast<Affinity>(j));
    return std::nullopt;
}

std::optional<SchedulingPair> Thread::getPriority()
{
    // Fill the sched_param structure with the current data
    int         policy;
    sched_param param;

    if (pthread_getschedparam(m_thread.native_handle(), &policy, &param))
    {
        return std::nullopt;
    }

    switch (policy)
    {
        case SCHED_RR:   return std::optional<SchedulingPair>({SchedulingPolicy::rr,   param.sched_priority}); break;
        case SCHED_FIFO: return std::optional<SchedulingPair>({SchedulingPolicy::fifo, param.sched_priority}); break;
        default:
            return std::nullopt;
    }
}

// EOF