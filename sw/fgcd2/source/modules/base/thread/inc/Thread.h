//! @file
//! @brief  Class that represents an FGCD thread and is used to start and manage it.
//! @author Dariusz Zielinski

#pragma once

#include <atomic>
#include <functional>
#include <iostream>
#include <mutex>
#include <optional>
#include <string>
#include <thread>

#include <modules/base/core/inc/types.h>
#include "ThreadInfo.h"

namespace fgcd
{
    struct CurrentThread
    {
        static thread_local std::string_view name;
    };

    // **********************************************************

    class Thread
    {
        public:
            // TODO to decide if there should be any cleaning routine, for example to call forceJoin
            // ~Thread();
            // TODO Should we set pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0); for every thread?
            
            template<typename Function, typename... Args>
            void start(const ThreadInfo& thread_info, Function&& function, Args&&... args)
            {
                m_is_running = true;

                // Notice that it is needed to copy function by value as it is parameter of this function and will be
                // destroyed after the function exits (which may be before std::invoke).
                m_thread = std::thread([this, thread_info, function, args...]()
                {
                    // Set the name for the newly created thread
                    CurrentThread::name = thread_info.name();

                    // Execute the thread function
                    std::invoke(function, m_is_running, args...);
                    m_finished = true;
                });

                // TODO pass empty function and add a delay here of say 1s to set affinity and priority of already joined thread to see it that's not a problem

                // Configure thread parameters
                configureAffinity(thread_info);
                configurePriority(thread_info);
            }

            //! This method sets the m_is_running flag to false. Whether the thread will actually stop depends on
            //! the thread function making use of that flag.
            void stop();

            //! This method will wait a given timeout for the thread to finish itself (e.g. after invoking stop()).
            //! If the thread is not stopped after the timeout, it will cancel the thread and then join it.
            void forceJoin(const Duration& grace_period = 10ms);

            //! @return true if the thread was started (i.e. is running), false otherwise.
            bool isRunning() const;

            //! @return Affinity configuration of underlying thread.
            std::optional<Affinity> getAffinity();

            //! @return Policy and priority configuration of underlying thread.
            std::optional<SchedulingPair> getPriority();

        private:
            //! Configures CPU affinity of the thread
            void configureAffinity(const ThreadInfo& thread_info);

            //! Configures scheduling policy and priority of the thread
            void configurePriority(const ThreadInfo& thread_info);

            std::thread      m_thread;
            std::atomic_bool m_is_running = false;
            std::atomic_bool m_finished   = false;
    };
}

// EOF