//! @file
//! @brief  Definitions of threads parameters. It is globally put here, so we can have an overview of all threads.
//! @author Dariusz Zielinski

#pragma once

#include "ThreadInfo.h"

namespace fgcd
{
    //! Configuration of threads parameters (affinity, scheduling and priority) of all the threads used in the FGCD.
    //! Priority ranges:   1 - 99   (1 - lowest, 99 - highest)
    //!
    //! For readability, define the threads info in the order of priority.
    namespace threads
    {
        // clang-format off
        constexpr ThreadInfo debug            {"DEBUG",              Affinity::none, Scheduling::fifo<1>()};
        constexpr ThreadInfo logger           {"LOGGER",             Affinity::none, Scheduling::fifo<35>()};
        constexpr ThreadInfo timing           {"TIMING",             Affinity::none, Scheduling::fifo<58>()};
        constexpr ThreadInfo timer            {"TIMER",              Affinity::none, Scheduling::fifo<58>()};
        constexpr ThreadInfo socket           {"SOCKET",             Affinity::none, Scheduling::fifo<65>()};
        constexpr ThreadInfo device           {"DEVICE",             Affinity::none, Scheduling::fifo<65>()};
        constexpr ThreadInfo websockets       {"WS",                 Affinity::none, Scheduling::fifo<40>()};
        constexpr ThreadInfo websocket_server {"WEBSOCKET_RESPONSE", Affinity::none, Scheduling::fifo<65>()};
        constexpr ThreadInfo status_server    {"STATUS_SERVER",      Affinity::none, Scheduling::fifo<40>()};
        // clang-format on
    };
}

// EOF
