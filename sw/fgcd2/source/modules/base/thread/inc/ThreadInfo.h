//! @file
//! @brief  Helper classes to define thread parameters (priority, affinity, scheduling) of the threads.
//! @author Dariusz Zielinski

#pragma once

#include <tuple>

namespace fgcd
{
    //! Defines affinity of a thread to a CPU core
    enum class Affinity : std::size_t
    {
        core_0 = 0,          //!< Affinity to core with index 0.
        core_1 = 1,          //!< Affinity to core with index 1.
        core_2 = 2,          //!< Affinity to core with index 2.
        core_3 = 3,          //!< Affinity to core with index 3.
        core_4 = 4,          //!< Affinity to core with index 4.
        core_5 = 5,          //!< Affinity to core with index 5.
        core_6 = 6,          //!< Affinity to core with index 6.
        core_7 = 7,          //!< Affinity to core with index 7.

        none  = 99          //!< No affinity to a specific core. Default behavior.
    };

    // ************************************************************

    //! Defines scheduling policy (and thus range of priorities)
    enum class SchedulingPolicy
    {
        rr,
        fifo
    };

    // ************************************************************

    //! Aggregates scheduling policy and priority. It is introduced only to perform static_assert check of priority.
    struct Scheduling
    {
        template <int priority>
        constexpr static std::pair<SchedulingPolicy, int> rr()
        {
            static_assert(priority >=1 && priority <= 99, "A realtime thread priority has to be in range of 1-99");
            return {SchedulingPolicy::rr, priority};
        }

        template <int priority>
        constexpr static std::pair<SchedulingPolicy, int> fifo()
        {
            static_assert(priority >=1 && priority <= 99, "A realtime thread priority has to be in range of 1-99");
            return {SchedulingPolicy::fifo, priority};
        }
    };
    using SchedulingPair = std::pair<SchedulingPolicy, int>;

    // ************************************************************

    //! This class holds affinity, scheduling policy, and priority and validates the data
    class ThreadInfo
    {
        public:
            constexpr ThreadInfo(const char* name, Affinity affinity, std::pair<SchedulingPolicy, int> scheduling)
                : m_name(name), m_affinity(affinity), m_policy(scheduling.first), m_priority(scheduling.second)
            {
            }

            // ************************************************************

            constexpr const char* name() const
            {
                return m_name;
            }

            // ************************************************************

            constexpr Affinity affinity() const
            {
                return m_affinity;
            }

            // ************************************************************

            constexpr SchedulingPolicy policy() const
            {
                return m_policy;
            }

            // ************************************************************

            constexpr int priority() const
            {
                return m_priority;
            }

        private:
            const char*      m_name;
            Affinity         m_affinity;
            SchedulingPolicy m_policy;
            int              m_priority;
    };
}

// EOF
