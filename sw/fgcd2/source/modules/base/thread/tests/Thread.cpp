#include <grp.h>

#include "../inc/Thread.h"
#include <modules/base/core/inc/Exception.h>

#include <gtest/gtest.h>
#include <cpp_utils/enumStr.h>

using namespace fgcd;

// **********************************************************
//! Macros to provide pretty printing
namespace fgcd {
    MAKE_ENUM_STR(SchedulingPolicyStr, SchedulingPolicy,
        ENUM_STR(SchedulingPolicy, fifo),
        ENUM_STR(SchedulingPolicy, rr),
    );
}

// **********************************************************

constexpr ThreadInfo info{"TEST_THREAD", Affinity::core_0, Scheduling::fifo<10>()};

TEST(ThreadTests, threadNotStarted)
{
    Thread t;
    
    EXPECT_FALSE(t.isRunning());
}

TEST(ThreadTests, threadCalled)
{
    Thread t;
    bool called = false;
    
    t.start(info, [&called](std::atomic_bool& keep_running)
    {
        called = true;
    });
    t.forceJoin();
    EXPECT_TRUE(called);
}

TEST(ThreadTests, shortTask)
{
    Thread t;

    EXPECT_NO_THROW(
        t.start(info, [](std::atomic_bool& keep_running) { });
    );
    
    t.forceJoin();
}

TEST(ThreadTests, stopThread)
{
    Thread t;
    bool end = false;
    
    t.start(info, [&end](std::atomic_bool& keep_running)
    {
        while (keep_running) {}
        end = true;
    });

    EXPECT_FALSE(end);
    t.stop();
    std::this_thread::sleep_for(10ms);
    EXPECT_TRUE(end);
    t.forceJoin();
}

TEST(ThreadTests, forceJoinEndlessLoop)
{
    Thread t;
    bool end = false;
    
    t.start(info, [&end](std::atomic_bool& keep_running)
    {
        while (keep_running)
        {
            std::this_thread::sleep_for(1ms);
            // Call to function which is a cancellation point.
            // Required to cancel a thread in deferred mode.
            getgrent();
        }
        end = true;
    });

    t.forceJoin();
    EXPECT_FALSE(end);
    EXPECT_TRUE(t.isRunning());
}

TEST(ThreadTests, threadConfiguration)
{
    Thread t;
    constexpr ThreadInfo params{"TEST_THREAD", Affinity::core_4 , Scheduling::rr<2>()};

    t.start(params, [](std::atomic_bool& keep_running)
    {
        while (keep_running) {}
    });

    auto param    = t.getPriority();
    auto affinity = t.getAffinity();
    t.stop();
    t.forceJoin();

    ASSERT_TRUE(param.has_value());
    ASSERT_TRUE(affinity.has_value());
    EXPECT_EQ(SchedulingPolicy::rr, param->first);
    EXPECT_EQ(2, param->second);
    EXPECT_EQ(Affinity::core_4, *affinity);
}

// EOF
