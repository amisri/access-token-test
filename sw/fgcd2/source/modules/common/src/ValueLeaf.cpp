//! @file
//! @brief
//! @author Adam Solawa

#include <utility>

#include <interfaces/IValueItem.h>
#include <modules/base/misc/inc/FmtFormatters.h>
#include "../inc/ValueLeaf.h"

using namespace fgcd;

// **********************************************************

ValueLeaf::ValueLeaf(ValueType value) : m_value(std::move(value))
{
}

// **********************************************************

[[nodiscard]] ValueType ValueLeaf::get() const
{
    return m_value;
}

// **********************************************************

void ValueLeaf::add(std::string_view label, IValueItemPtr child)
{
}

// **********************************************************

const IValueChildren& ValueLeaf::getChildren()
{
    static IValueChildren dummy_vector;
    return dummy_vector;
}

// **********************************************************

bool ValueLeaf::isParent() const
{
    return false;
}

// **********************************************************

std::string ValueLeaf::toString() const
{
    return ::toString(m_value);
}

// EOF
