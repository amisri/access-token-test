//! @file
//! @brief
//! @author Adam Solawa

#include <memory>
#include <string_view>
#include <variant>

#include <cpp_utils/text.h>
#include <interfaces/IValueItem.h>
#include "../inc/ValueParent.h"

using namespace fgcd;

// **********************************************************

ValueType ValueParent::get() const
{
    return std::monostate();
}

// **********************************************************

void ValueParent::add(std::string_view label, IValueItemPtr new_node)
{
    auto [label_head, rest] = utils::splitFirst(label, '.');

    if (m_children.contains(label_head))
    {
        auto [_, child] = *m_children.find(label_head);

        child->add(rest, new_node);
    }
    else if (!rest.empty())
    {
        IValueItemPtr item = std::make_shared<ValueParent>();

        item->add(rest, new_node);

        m_children.emplace(label_head, item);
    }
    else
    {
        m_children.emplace(label_head, new_node);
    }
}

// **********************************************************

const IValueChildren& ValueParent::getChildren()
{
    return m_children;
}

// **********************************************************

[[nodiscard]] bool ValueParent::isParent() const
{
    return true;
}

// **********************************************************

std::string ValueParent::toString() const
{
    return "TODO";
}

// EOF
