//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string_view>
#include <variant>

#include <interfaces/IValueItem.h>

namespace fgcd
{
    class ValueLeaf : public IValueItem
    {
      public:
        ValueLeaf() = default;
        ValueLeaf(ValueType value);

        [[nodiscard]] ValueType get() const override;
        void                    add(std::string_view label, IValueItemPtr child) override;
        const IValueChildren&    getChildren() override;
        [[nodiscard]] bool      isParent() const override;
        [[nodiscard]] std::string toString() const override;

      private:
        ValueType m_value = std::monostate();
    };
}

// EOF
