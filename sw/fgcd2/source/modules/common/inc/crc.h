//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>

#include <modules/base/core/inc/types.h>

namespace fgcd
{
    std::uint16_t calculateCrc16(ByteStreamView bytes);
}

// EOF
