//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string_view>

#include <interfaces/IValueItem.h>

namespace fgcd
{
    class ValueParent : public IValueItem
    {
      public:
        [[nodiscard]] ValueType get() const override;
        void                    add(std::string_view label, IValueItemPtr new_node) override;
        const IValueChildren&    getChildren() override;
        [[nodiscard]] bool      isParent() const override;
        [[nodiscard]] std::string toString() const override;

      private:
        IValueChildren m_children;
    };
}

// EOF
