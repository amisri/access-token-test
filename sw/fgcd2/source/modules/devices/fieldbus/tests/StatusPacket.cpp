#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/StatusPacket.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct FieldbusStatusPacketTests : public ::testing::Test
{
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FieldbusStatusPacketTests, FieldbusStatusPacket_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
}

// EOF
