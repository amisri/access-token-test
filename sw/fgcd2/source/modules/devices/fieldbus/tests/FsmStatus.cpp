#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include <utils/test/printers/Printers.h>
#include "../inc/Enums.h"
#include "../inc/FsmStatus.h"
#include "../inc/StatusPacket.h"

using namespace utils;
using namespace fgcd;
using namespace fgcd::fieldbus;

// **********************************************************
// Setup
// **********************************************************

class FsmStatusTests : public ::testing::Test
{
  public:
    RootMockPtr  root;
    FsmStatusPtr status_fsm;

    void SetUp() override
    {
        root       = std::make_unique<RootMock>();
        status_fsm = root->addComponent<FsmStatus>();
        root->enableLogging();
        root->start();
    }

    void TearDown() override
    {
        root->stop();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FsmStatusTests, noSignalAfterProcess)
{
    // **********************************************************
    // Preparation
    auto packet = fieldbus::StatusPacket().setSequence(1);


    // **********************************************************
    // Execution
    auto signal = status_fsm->process(packet, false);


    // **********************************************************
    // Verification
    EXPECT_EQ(signal, DeviceSignals());
}

// **********************************************************

TEST_F(FsmStatusTests, noSignalAfterTick)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution
    auto signal = status_fsm->tick();


    // **********************************************************
    // Verification
    EXPECT_EQ(signal, DeviceSignals());
}

// **********************************************************

TEST_F(FsmStatusTests, sendCommand)
{
    // **********************************************************
    // Preparation
    auto packet = fieldbus::StatusPacket()   //
                      .setPllLocked()
                      .setClass(64);


    // **********************************************************
    // Execution
    auto signal = status_fsm->process(packet, true);


    // **********************************************************
    // Verification
    EXPECT_EQ(signal, DeviceSignal::send_command);
}

// **********************************************************

TEST_F(FsmStatusTests, deviceGoOfflineDuringCommunication)
{
    // **********************************************************
    // Preparation
    auto packet = fieldbus::StatusPacket()   //
                      .setPllLocked()
                      .setClass(64);
    std::vector<DeviceSignals> signals{};


    // **********************************************************
    // Execution
    signals.push_back(status_fsm->process(packet, true));
    signals.push_back(status_fsm->tick());
    signals.push_back(status_fsm->tick());
    signals.push_back(status_fsm->tick());


    // **********************************************************
    // Verification
    ASSERT_EQ(signals[0], DeviceSignal::send_command);
    ASSERT_EQ(signals[1].none(), true);
    ASSERT_EQ(signals[2].none(), true);
    ASSERT_EQ(signals[3], DeviceSignal::command_finished);
    ASSERT_EQ(status_fsm->getStatus(), FgcStatus::dev_not_ready);
}

// **********************************************************

TEST_F(FsmStatusTests, properCommandFinished)
{
    // **********************************************************
    // Preparation
    auto packet = fieldbus::StatusPacket()   //
                      .setPllLocked()
                      .setClass(64)
                      .setSequence(1)
                      .setCommandToggle(true);


    // **********************************************************
    // Execution
    status_fsm->process(packet, true);
    status_fsm->tick();


    // **********************************************************
    // Preparation
    packet = fieldbus::StatusPacket()   //
                      .setPllLocked()
                      .setClass(64)
                      .setSequence(2)
                      .setCommandToggle(false)
                      .setStatus(FgcStatus::ok_no_rsp);


    // **********************************************************
    // Execution
    auto signal = status_fsm->process(packet, false);


    // **********************************************************
    // Verification
    EXPECT_EQ(signal, DeviceSignal::command_finished);
}

// **********************************************************

TEST_F(FsmStatusTests, commandFinishedAfterExecuting)
{
    // **********************************************************
    // Preparation
    auto packet = fieldbus::StatusPacket()   //
                      .setPllLocked()
                      .setClass(64)
                      .setSequence(1)
                      .setCommandToggle(true);


    // **********************************************************
    // Execution
    status_fsm->tick();
    status_fsm->process(packet, true);


    // **********************************************************
    // Preparation
    packet = fieldbus::StatusPacket()   //
                 .setPllLocked()
                 .setClass(64)
                 .setSequence(2)
                 .setCommandToggle(false)
                 .setStatus(FgcStatus::executing);


    // **********************************************************
    // Execution
    status_fsm->tick();
    status_fsm->process(packet, false);


    // **********************************************************
    // Preparation
    packet = fieldbus::StatusPacket()   //
                 .setPllLocked()
                 .setClass(64)
                 .setSequence(3)
                 .setCommandToggle(false)
                 .setStatus(FgcStatus::ok_no_rsp);


    // **********************************************************
    // Execution
    status_fsm->tick();
    auto signal = status_fsm->process(packet, false);


    // **********************************************************
    // Verification
    EXPECT_EQ(signal, DeviceSignal::command_finished);
}

// EOF
