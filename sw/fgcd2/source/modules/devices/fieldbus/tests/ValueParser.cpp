#include <array>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <span>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <interfaces/IValueItem.h>
#include <modules/base/core/inc/types.h>
#include <utils/test/mocks/RootMock.h>
#include "../inc/ValueParser.h"

using namespace fgcd;
using namespace std::literals;

// **********************************************************
// Setup
// **********************************************************

struct ValueParserTests : public ::testing::Test
{
    ValueParser parser;
};

// **********************************************************
// Tests
// **********************************************************

using Parameters = std::tuple<std::string, ValueType, std::string>;

struct TextParseTests : public ::testing::TestWithParam<Parameters>
{
    TextParseTests()
    {
        std::tie(input, output, std::ignore) = GetParam();
    }

    std::string input;
    ValueType   output;
};

TEST_P(TextParseTests, parseText)
{
    // **********************************************************
    // Preparation
    auto stream      = this->input;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = ValueParser().parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto value = *result;


    // **********************************************************
    // Verification
    EXPECT_EQ(value->isParent(), false);
    EXPECT_EQ(value->get(), this->output);
}

std::string generate_test_name(const ::testing::TestParamInfo<Parameters>& info)
{
    auto [_1, _2, name] = info.param;
    return name;
}

// clang-format off

std::vector<Parameters> test_paramters = {
    {
        "value:char:TEST"s,
        std::string("TEST"),
        "Test2"
    },
    {
        "value:string:TEST"s,
        std::string("TEST"),
        "Test3"
    },
    {
        "value:string:TEST1,TEST2,TEST3"s,
        std::vector({"TEST1"s, "TEST2"s, "TEST3"s}),
        "Test5"
    },
    {
        "value:POINT:2.0000|3.0000000E+00"s,
        Point({2, 3}),
        "Test6"
    },
    {
        "value:POINT:1.0000|1.0000000E+00,2.0000|2.0000000E+00,3.0000|3.0000000E+00"s,
        std::vector<Point>({{1, 1}, {2, 2}, {3, 3}}),
        "Test7"
    },
    {
        "value:float:1.1234000E+01"s,
        float(11.234F),
        "Test8"
    },
    {
        "value:float:1.1000000E+01,2.1000000E+01,3.1000000E+01,4.1000000E+01"s,
        std::vector<float>({11.F, 21.F, 31.F, 41.F}),
        "Test9"
    },
    {
        "value:int32s:-1156"s,
        std::int32_t(-1156),
        "Test10"
    },
    {
        "value:INT32S:-1156,2198,-1229,2221,0,0"s,
        std::vector<std::int32_t>({-1156, 2198, -1229, 2221, 0, 0}),
        "Test11"
    },
    {
        "value:int16s:-1156"s,
        std::int16_t(-1156),
        "Test12"
    },
    {
        "value:int16s:-1156,-1,100,30000"s,
        std::vector<std::int16_t>({-1156, -1, 100, 30000}),
        "Test13"
    },
    {
        "value:int8s:-115"s,
        std::int8_t(-115),
        "Test14"
    },
    {
        "value:int8s:-115,115,0,-1"s,
        std::vector<std::int8_t>({-115, 115, 0, -1}),
        "Test15"
    },
    {
        "value:int32u:1156"s,
        std::uint32_t(1156),
        "Test16"
    },
    {
        "value:INT32U:1156,2198,1229,2221,0,0"s,
        std::vector<std::uint32_t>({1156, 2198, 1229, 2221, 0, 0}),
        "Test17"
    },
    {
        "value:int16u:1156"s,
        std::uint16_t(1156),
        "Test18"
    },
    {
        "value:int16u:1156,1,100,30000"s,
        std::vector<std::uint16_t>({1156, 1, 100, 30000}),
        "Test19"
    },
    {
        "value:int8u:115"s,
        std::uint8_t(115),
        "Test20"
    },
    {
        "value:int8u:115,115,0,1"s,
        std::vector<std::uint8_t>({115, 115, 0, 1}),
        "Test21"
    },

    // **********************************************************
    // Binary input
    // **********************************************************

    {
        "value:float:"
        "\xFF"                // flag
        "\x00\x00\x00\x04"    // length
        "\x3F\xE0\x00\x00"s,
        1.75F,
        "Test22"
    },
    {
        "value:float:"
        "\xFF"               // flag
        "\x00\x00\x00\x10"   // length
        "\xC0\x00\x00\x00"
        "\xC0\x80\x00\x00"
        "\xC1\x00\x00\x00"
        "\xC1\xC0\x00\x00"s,
        std::vector<float>({-2.F, -4.F, -8.F, -24.F}),
        "Test23"
    },
    {
        "value:int32s:"
        "\xFF"                // flag
        "\x00\x00\x00\x04"    // length
        "\xFF\xFF\xFB\x77"s,
        std::int32_t(-1161),
        "Test24"
    },
    {
        "value:int32s:"
        "\xFF"                // flag
        "\x00\x00\x00\x18"    // length
        "\xFF\xFF\xFB\x77"
        "\x00\x00\x08\x92"
        "\xFF\xFF\xFB\x32"
        "\x00\x00\x08\xB3"
        "\x00\x00\x00\x00"
        "\x00\x00\x00\x00"s,
        std::vector<std::int32_t>({-1161, 2194, -1230, 2227, 0, 0}),
        "Test25"
    },
    {
        "value:int16s:"
        "\xFF"               // flag
        "\x00\x00\x00\x02"   // length
        "\x12\x34"s,
        std::int16_t(4660),
        "Test26"
    },
    {
        "value:int16s:"
        "\xFF"               // flag
        "\x00\x00\x00\x08"   // length
        "\x80\x34"
        "\x00\x34"
        "\x80\x80"
        "\x00\x80"s,
        std::vector<std::int16_t>({-32716, 52, -32640, 128}),
        "Test27"
    },
    {
        "value:int8s:"
        "\xFF"               // flag
        "\x00\x00\x00\x01"   // length
        "\xF2"s,
        std::int8_t(-14),
        "Test28"
    },
    {
        "value:int8s:"
        "\xFF"               // flag
        "\x00\x00\x00\x04"   // length
        "\x02"
        "\x82"
        "\xFF"
        "\x7F"s,
        std::vector<std::int8_t>({2, -126, -1, 127}),
        "Test29"
    },
    {
        "value:int32u:"
        "\xFF"                // flag
        "\x00\x00\x00\x04"    // length
        "\xFF\xFF\xFB\x77"s,
        std::uint32_t(4294966135),
        "Test30"
    },
    {
        "value:int32u:"
        "\xFF"                // flag
        "\x00\x00\x00\x18"    // length
        "\xFF\xFF\xFB\x77"
        "\x00\x00\x08\x92"
        "\xFF\xFF\xFB\x32"
        "\x00\x00\x08\xB3"
        "\x00\x00\x00\x00"
        "\x00\x00\x00\x00"s,
        std::vector<std::uint32_t>({4294966135, 2194, 4294966066, 2227, 0, 0}),
        "Test31"
    },
    {
        "value:int16u:"
        "\xFF"               // flag
        "\x00\x00\x00\x02"   // length
        "\x12\x34"s,
        std::uint16_t(4660),
        "Test32"
    },
    {
        "value:int16u:"
        "\xFF"               // flag
        "\x00\x00\x00\x08"   // length
        "\x80\x34"
        "\x00\x34"
        "\x80\x80"
        "\x00\x80"s,
        std::vector<std::uint16_t>({32820, 52, 32896, 128}),
        "Test33"
    },
    {
        "value:int8u:"
        "\xFF"               // flag
        "\x00\x00\x00\x01"   // length
        "\xF2"s,
        std::uint8_t(242),
        "Test34"
    },
    {
        "value:int8u:"
        "\xFF"               // flag
        "\x00\x00\x00\x04"   // length
        "\x02"
        "\x82"
        "\xFF"
        "\x7F"s,
        std::vector<std::uint8_t>({2, 130, 255, 127}),
        "Test35"
    },
    {
        "value:POINT:"
        "\xFF"                                // flag
        "\x00\x00\x00\x18"                    // length
        "\x3F\xE0\x00\x00\x41\x28\x00\x00"    // 4B time + 4B value
        "\x40\x00\x00\x00\x00\x00\x00\x00"
        "\x40\x40\x00\x00\x00\x00\x00\x00"s,
        std::vector<Point>({{1.75, 10.5}, {2, 0}, {3, 0}}),
        "Test36"
    }
};

INSTANTIATE_TEST_SUITE_P(
    ValueParserTests,
    TextParseTests,
    testing::ValuesIn(test_paramters),
    generate_test_name
);

// clang-format on

// **********************************************************

TEST_F(ValueParserTests, parseEmpty)
{
    // **********************************************************
    // Preparation
    auto stream      = ""s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);


    // **********************************************************
    // Verification
    ASSERT_EQ(result.hasError(), true) << result.error().error_str;
}

// **********************************************************

TEST_F(ValueParserTests, parseEmptyPayload)
{
    // **********************************************************
    // Preparation
    auto stream      = "value:int16u:"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream, true);


    // **********************************************************
    // Verification
    ASSERT_EQ(result.hasError(), true) << result.error().error_str;
}

// **********************************************************

TEST_F(ValueParserTests, parseParentOneChild)
{
    // **********************************************************
    // Preparation
    auto stream      = "NOW:float:0.1000000E+02"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto root = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(root->isParent(), true);
    ASSERT_EQ(root->getChildren().contains("NOW"), true);
    auto direct = root->getChildren().at("NOW");
    ASSERT_EQ(direct->isParent(), false);
    ASSERT_THAT(direct->get(), ::testing::VariantWith<float>(10.0f));
}

// **********************************************************

TEST_F(ValueParserTests, parseParentNestedChild)
{
    // **********************************************************
    // Preparation
    auto stream      = "DIRECT.B.VALUE:float:1.2345000E+04"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto root = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(root->isParent(), true);
    ASSERT_EQ(root->getChildren().contains("DIRECT"), true);
    auto direct = root->getChildren().at("DIRECT");
    ASSERT_EQ(direct->isParent(), true);
    ASSERT_EQ(direct->getChildren().contains("B"), true);
    auto b = direct->getChildren().at("B");
    ASSERT_EQ(b->isParent(), true);
    ASSERT_EQ(b->getChildren().contains("VALUE"), true);
    auto value = b->getChildren().at("VALUE");
    ASSERT_EQ(value->isParent(), false);
    ASSERT_THAT(value->get(), ::testing::VariantWith<float>(12345.0f));
}

// **********************************************************

TEST_F(ValueParserTests, parseRepeatedChild)
{
    // **********************************************************
    // Preparation
    auto stream      = "DIRECT.B.VALUE:float:1.2345000E+04\n"
                       "DIRECT.B.VALUE:float:0.1000000E+04"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto root = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(root->isParent(), true);
    ASSERT_EQ(root->getChildren().contains("DIRECT"), true);
    auto direct = root->getChildren().at("DIRECT");
    ASSERT_EQ(direct->isParent(), true);
    ASSERT_EQ(direct->getChildren().contains("B"), true);
    auto b = direct->getChildren().at("B");
    ASSERT_EQ(b->isParent(), true);
    ASSERT_EQ(b->getChildren().contains("VALUE"), true);
    auto value = b->getChildren().at("VALUE");
    ASSERT_EQ(value->isParent(), false);
    ASSERT_THAT(value->get(), ::testing::VariantWith<float>(12345.0f));
}

// **********************************************************

TEST_F(ValueParserTests, parseParentMultipleNestedChild)
{
    // **********************************************************
    // Preparation
    auto stream      = "NOW:float:0.1000000E+02\n"
                       "DIRECT.B.VALUE:float:2.2000000E+02\n"
                       "DIRECT.B.VALUE_MAX:float:3.3000000E+01\n"
                       "DIRECT.B.VALUE_MIN:float:0.0333000E+03"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto root = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(root->isParent(), true);

    ASSERT_EQ(root->getChildren().contains("NOW"), true);
    auto now = root->getChildren().at("NOW");
    ASSERT_EQ(now->isParent(), false);
    ASSERT_THAT(now->get(), ::testing::VariantWith<float>(10.f));

    ASSERT_EQ(root->getChildren().contains("DIRECT"), true);
    auto direct = root->getChildren().at("DIRECT");
    ASSERT_EQ(direct->isParent(), true);

    ASSERT_EQ(direct->getChildren().contains("B"), true);
    auto b = direct->getChildren().at("B");
    ASSERT_EQ(b->isParent(), true);

    ASSERT_EQ(b->getChildren().contains("VALUE"), true);
    auto value = b->getChildren().at("VALUE");
    ASSERT_EQ(value->isParent(), false);
    ASSERT_THAT(value->get(), ::testing::VariantWith<float>(220.f));

    ASSERT_EQ(b->getChildren().contains("VALUE_MAX"), true);
    auto value_max = b->getChildren().at("VALUE_MAX");
    ASSERT_EQ(value_max->isParent(), false);
    ASSERT_THAT(value_max->get(), ::testing::VariantWith<float>(33.f));

    ASSERT_EQ(b->getChildren().contains("VALUE_MIN"), true);
    auto value_min = b->getChildren().at("VALUE_MIN");
    ASSERT_EQ(value_min->isParent(), false);
    ASSERT_THAT(value_min->get(), ::testing::VariantWith<float>(33.3f));
}

// **********************************************************

TEST_F(ValueParserTests, parseMultipleTheSameChildren)
{
    // **********************************************************
    // Preparation
    auto stream      = "NOW:float:0.1000000E+02\n"
                       "DIRECT.B.VALUE:float:2.2000000E+02\n"
                       "DIRECT.B.VALUE:float:3.3000000E+01"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto root = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(root->isParent(), true);

    ASSERT_EQ(root->getChildren().contains("NOW"), true);
    auto now = root->getChildren().at("NOW");
    ASSERT_EQ(now->isParent(), false);
    ASSERT_THAT(now->get(), ::testing::VariantWith<float>(10.f));

    ASSERT_EQ(root->getChildren().contains("DIRECT"), true);
    auto direct = root->getChildren().at("DIRECT");
    ASSERT_EQ(direct->isParent(), true);

    ASSERT_EQ(direct->getChildren().contains("B"), true);
    auto b = direct->getChildren().at("B");
    ASSERT_EQ(b->isParent(), true);

    ASSERT_EQ(b->getChildren().contains("VALUE"), true);
    auto value = b->getChildren().at("VALUE");
    ASSERT_EQ(value->isParent(), false);
    ASSERT_THAT(value->get(), ::testing::VariantWith<float>(220.f));
}

// **********************************************************

TEST_F(ValueParserTests, parseMultipleTheSameChildren2)
{
    // **********************************************************
    // Preparation
    auto stream      = "NOW:float:0.1000000E+02\n"
                       "NOW:float:0.1230000E+02"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto root = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(root->isParent(), true);

    ASSERT_EQ(root->getChildren().contains("NOW"), true);
    auto now = root->getChildren().at("NOW");
    ASSERT_EQ(now->isParent(), false);
    ASSERT_THAT(now->get(), ::testing::VariantWith<float>(10.f));
}

// **********************************************************

TEST_F(ValueParserTests, wrongHeader)
{
    // **********************************************************
    // Preparation
    auto stream      = "0.1000000E+02"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);


    // **********************************************************
    // Verification
    ASSERT_EQ(result.hasError(), true);
}

// **********************************************************

TEST_F(ValueParserTests, wrongBinaryHeader)
{
    // **********************************************************
    // Preparation
    auto stream      = "0.1000000E+02"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parse(byte_stream);


    // **********************************************************
    // Verification
    ASSERT_EQ(result.hasError(), true);
}

// **********************************************************

TEST_F(ValueParserTests, multirowGetBody)
{
    // **********************************************************
    // Preparation
    auto stream      = "0:float:0.0000000E+00\n"
                       "1:float:1.0000000E+00\n"
                       "2:float:2.0000000E+00\n"
                       "3:float:3.0000000E+00\n"
                       "4:float:4.0000000E+00\n"
                       "5:float:5.0000000E+00"s;
    auto byte_stream = std::as_bytes(std::span(stream));


    // **********************************************************
    // Execution
    auto result = parser.parsePPM(byte_stream);
    ASSERT_EQ(result.hasError(), false) << result.error().error_str;
    auto test_paramters = *result;


    // **********************************************************
    // Verification
    ASSERT_EQ(test_paramters.size(), 6);
    EXPECT_EQ(test_paramters[0]->isParent(), false);
    EXPECT_THAT(test_paramters[0]->get(), ::testing::VariantWith<float>(0.00f));
    EXPECT_EQ(test_paramters[1]->isParent(), false);
    EXPECT_THAT(test_paramters[1]->get(), ::testing::VariantWith<float>(1.00f));
    EXPECT_EQ(test_paramters[2]->isParent(), false);
    EXPECT_THAT(test_paramters[2]->get(), ::testing::VariantWith<float>(2.00f));
    EXPECT_EQ(test_paramters[3]->isParent(), false);
    EXPECT_THAT(test_paramters[3]->get(), ::testing::VariantWith<float>(3.00f));
    EXPECT_EQ(test_paramters[4]->isParent(), false);
    EXPECT_THAT(test_paramters[4]->get(), ::testing::VariantWith<float>(4.00f));
    EXPECT_EQ(test_paramters[5]->isParent(), false);
    EXPECT_THAT(test_paramters[5]->get(), ::testing::VariantWith<float>(5.00f));
}

// EOF
