#include <gtest/gtest.h>

#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryWriter.h>
#include "../inc/FgcCode.h"

using namespace fgcd::fieldbus;
using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct FgcCodeTests : public ::testing::Test
{
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FgcCodeTests, defaultInitializedNotValid)
{
    // **********************************************************
    // Preparation
    FgcCode code{};


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(code.valid(), false);
}

// **********************************************************

TEST_F(FgcCodeTests, invalidCallGetter)
{
    // **********************************************************
    // Preparation
    FgcCode code{};


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    // clang-format off
    EXPECT_EQ(code.getCodeBlocks().empty(), true);
    EXPECT_EQ(code.getBlocksLength(),       0);
    EXPECT_EQ(code.getClassId(),            0);
    EXPECT_EQ(code.getCrc(),                0);
    EXPECT_EQ(code.getVersion(),            0);
    // clang-format on
}

// **********************************************************

TEST_F(FgcCodeTests, fromBytesOk)
{
    // **********************************************************
    // Preparation
    FgcCodeInfo header{
        .version  = 0x64197FA5,   //
        .class_id = 63,           //
        .code_id  = 24,           //
        .crc      = 0x2E73        //
    };
    // clang-format off
    auto header_bytes = ByteStream(20, 'A'_b) +
        ByteStream({
            0x64_b, 0x19_b, 0x7F_b, 0xA5_b,
            63_b,
            24_b,
            0xFF_b, 0xFF_b,
            0x2E_b, 0x73_b
        });
    auto buffer = ByteStream(30) + header_bytes;
    // clang-format on


    // **********************************************************
    // Execution
    auto code = FgcCode::fromBytes(buffer);


    // **********************************************************
    // Verification
    // clang-format off
    ASSERT_EQ(code.hasValue(),        true);
    EXPECT_EQ(code->valid(),           true);
    EXPECT_EQ(code->getBlocksLength(), 2);
    EXPECT_EQ(code->getClassId(),      63);
    EXPECT_EQ(code->getCodeId(),       24);
    EXPECT_EQ(code->getCrc(),          0x2E73);
    EXPECT_EQ(code->getVersion(),      0x64197FA5);
    // clang-format on
}

// **********************************************************

TEST_F(FgcCodeTests, blocksLengthRounding)
{
    // **********************************************************
    // Preparation
    ByteStream buffer1 = fgcd::ByteStream(32 * 10 + 1);
    ByteStream buffer2 = fgcd::ByteStream(32 * 10);


    // **********************************************************
    // Execution
    auto code1 = FgcCode::fromBytes(buffer1);
    auto code2 = FgcCode::fromBytes(buffer2);


    // **********************************************************
    // Verification
    ASSERT_EQ(code1.hasValue(), true);
    EXPECT_EQ(code1->getBlocksLength(), 11);
    ASSERT_EQ(code2.hasValue(), true);
    EXPECT_EQ(code2->getBlocksLength(), 10);
}

// **********************************************************

TEST_F(FgcCodeTests, getCodeBlocks)
{
    // **********************************************************
    // Preparation
    ByteStream buffer = fgcd::ByteStream(32 * 10 + 1);


    // **********************************************************
    // Execution
    auto code = FgcCode::fromBytes(buffer);


    // **********************************************************
    // Verification
    // clang-format off
    ASSERT_EQ(code.hasValue(),                true);
    EXPECT_EQ(code->getCodeBlocks().size(),    321);
    EXPECT_EQ(code->getCodeBlocks(1).size(),   289);
    EXPECT_EQ(code->getCodeBlocks(1,1).size(), 32);
    EXPECT_EQ(code->getCodeBlocks(9).size(),   33);
    EXPECT_EQ(code->getCodeBlocks(9,4).size(), 33);
    EXPECT_EQ(code->getCodeBlocks(10).size(),  1);
    EXPECT_EQ(code->getCodeBlocks(11).empty(), true);
    // clang-format on
}

// **********************************************************

TEST_F(FgcCodeTests, tooShortBuffer)
{
    // **********************************************************
    // Preparation
    ByteStream buffer = fgcd::ByteStream(25);


    // **********************************************************
    // Execution
    auto code = FgcCode::fromBytes(buffer);


    // **********************************************************
    // Verification
    ASSERT_EQ(code.hasValue(), false);
}
// EOF
