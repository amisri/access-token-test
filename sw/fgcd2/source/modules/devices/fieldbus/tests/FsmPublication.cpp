#include <cassert>
#include <cstddef>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <optional>
#include <string>
#include <variant>
#include <vector>

#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>
#include <modules/devices/base/inc/Response.h>
#include <modules/devices/base/inc/Subscription.h>
#include <modules/devices/base/inc/Timestamp.h>
#include <utils/test/mocks/RootMock.h>
#include <utils/test/printers/Printers.h>
#include <utils/test/utils/CommandBuilder.h>
#include "../inc/FsmPublication.h"
#include "../inc/PODs.h"
#include "../inc/ResponsePacket.h"

using namespace fgcd::fieldbus;

// **********************************************************
// Setup
// **********************************************************

class FsmPublicationTests : public ::testing::Test
{
  public:
    RootMockPtr       root;
    FsmPublicationPtr handler;
    CommandBuilder    command_builder;
    ResponseQueue&    queue = command_builder.queue();
    const int         device_id = 10;

    void SetUp() override
    {
        root    = std::make_unique<RootMock>();
        handler = root->addComponent<FsmPublication>(device_id);

        root->enableLogging();
        root->start();
    }

    void TearDown() override
    {
        root->stop();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FsmPublicationTests, validPublication)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setProperty("TIME")
                       .setSelector("2")
                       .build();
    auto subscription = ResponsePacket()   //
                            .setType(FieldbusResponseType::command_response)
                            .setBody({0x50_b, ';'_b});
    auto publication = ResponsePacket()            //
                           .setFirstPacket(true)   //
                           .setLastPacket(true)    //
                           .setBody({
                               0x50_b, 0b1010'0001_b, 0x02_b,                         // subscription_id, flags, user
                               0x00_b, 0x00_b,        0x01_b, 0x05_b,                 // timestamp seconds
                               0x00_b, 0x00_b,        0x0A_b, 0x0D_b,                 // timestamp microseconds
                               'v'_b,  'a'_b,         'l'_b,  'u'_b,  'e'_b, ':'_b,   // label
                               'c'_b,  'h'_b,         'a'_b,  'r'_b,  ':'_b,          // type
                               'A'_b,  'B'_b,         'C'_b,  'D'_b,                  // body
                               ';'_b                                                  // termination
                           });


    // **********************************************************
    // Execution
    handler->processSub(subscription, command);
    handler->processPublication(publication);


    // **********************************************************
    // Verification
    auto result = queue.pop();

    ASSERT_EQ(queue.isEmpty(), true);
    ASSERT_EQ(result.has_value(), true);

    auto response = (*result)->response();

    ASSERT_EQ(std::holds_alternative<PublicationResponse>(response), true);

    auto publication_response = std::get<PublicationResponse>(response);

    EXPECT_EQ(publication_response.timestamp, Timestamp({.second = 0x0105, .microsecond = 0x0A0D}));
    EXPECT_EQ(publication_response.values.size(), 1);

    EXPECT_THAT(publication_response.values[0]->get(), ::testing::VariantWith<std::string>("ABCD"));
}

// **********************************************************

TEST_F(FsmPublicationTests, missValueType)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setProperty("TIME")
                       .setSelector("2")
                       .build();
    auto subscription = ResponsePacket()   //
                            .setType(FieldbusResponseType::command_response)
                            .setBody({0x50_b, ';'_b});
    auto publication = ResponsePacket()            //
                           .setFirstPacket(true)   //
                           .setLastPacket(true)    //
                           .setBody({
                               0x50_b, 0b1010'0001_b, 0x02_b,    // subscription_id, flags, user
                               0x00_b, 0x00_b, 0x01_b, 0x05_b,   // timestamp seconds
                               0x00_b, 0x00_b, 0x0A_b, 0x0D_b,   // timestamp microseconds
                               'A'_b, 'B'_b, 'C'_b, 'D'_b,       // body
                               ';'_b                             // termination
                           });


    // **********************************************************
    // Execution
    handler->processSub(subscription, command);
    handler->processPublication(publication);


    // **********************************************************
    // Verification
    ASSERT_EQ(queue.isEmpty(), true);
}

// **********************************************************

TEST_F(FsmPublicationTests, multipacketPublication)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setProperty("TIME")
                       .setSelector("2")
                       .build();
    auto subscription = ResponsePacket()   //
                            .setType(FieldbusResponseType::command_response)
                            .setBody({0x50_b, ';'_b});
    auto publication_1 = ResponsePacket()            //
                             .setFirstPacket(true)   //
                             .setLastPacket(false)   //
                             .setSequence(1)         //
                             .setBody({
                                 0x50_b, 0b1010'0001_b, 0x02_b,                         // subscription_id, flags, user
                                 0x00_b, 0x00_b,        0x01_b, 0x05_b,                 // timestamp seconds
                                 0x00_b, 0x00_b,        0x0A_b, 0x0D_b,                 // timestamp microseconds
                                 'v'_b,  'a'_b,         'l'_b,  'u'_b,  'e'_b, ':'_b,   // label
                                 'c'_b,  'h'_b,         'a'_b,  'r'_b,  ':'_b,          // type
                                 'A'_b,  'B'_b,         'C'_b,  'D'_b,                  // body
                             });
    auto publication_2 = ResponsePacket()             //
                             .setFirstPacket(false)   //
                             .setLastPacket(true)     //
                             .setSequence(2)          //
                             .setBody({
                                 'A'_b, 'B'_b, 'C'_b, 'D'_b,   // body
                                 ';'_b                         // termination
                             });


    // **********************************************************
    // Execution
    handler->processSub(subscription, command);
    handler->processPublication(publication_1);
    handler->processPublication(publication_2);


    // **********************************************************
    // Verification
    auto result = queue.pop();

    ASSERT_EQ(queue.isEmpty(), true);
    ASSERT_EQ(result.has_value(), true);

    auto response = (*result)->response();

    ASSERT_EQ(std::holds_alternative<PublicationResponse>(response), true);

    auto publication_response = std::get<PublicationResponse>(response);

    EXPECT_EQ(publication_response.timestamp, Timestamp({.second = 0x0105, .microsecond = 0x0A0D}));
    EXPECT_EQ(publication_response.values.size(), 1);
    // EXPECT_THAT(publication_response.values[0]->get(), ::testing::VariantWith<std::string>("ABCDABCD"));
}

// **********************************************************

TEST_F(FsmPublicationTests, unsubscribe)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setProperty("TIME")
                       .setSelector("2")
                       .build();
    auto subscription = ResponsePacket()   //
                            .setType(FieldbusResponseType::command_response)
                            .setBody({0x50_b, ';'_b});
    auto publication = ResponsePacket()            //
                           .setFirstPacket(true)   //
                           .setLastPacket(true)    //
                           .setBody({
                               0x50_b, 0b1010'0001_b, 0x02_b,                         // subscription_id, flags, user
                               0x00_b, 0x00_b,        0x01_b, 0x05_b,                 // timestamp seconds
                               0x00_b, 0x00_b,        0x0A_b, 0x0D_b,                 // timestamp microseconds
                               'v'_b,  'a'_b,         'l'_b,  'u'_b,  'e'_b, ':'_b,   // label
                               'c'_b,  'h'_b,         'a'_b,  'r'_b,  ':'_b,          // type
                               'A'_b,  'B'_b,         'C'_b,  'D'_b,                  // body
                               ';'_b                                                  // termination
                           });


    // **********************************************************
    // Execution
    handler->processSub(subscription, command);
    handler->processPublication(publication);
    handler->processUnsub(subscription);
    handler->processPublication(publication);


    // **********************************************************
    // Verification
    auto result = queue.pop();

    ASSERT_EQ(queue.isEmpty(), true);
    ASSERT_EQ(result.has_value(), true);
}

// **********************************************************

TEST_F(FsmPublicationTests, differentUserPublication)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setProperty("TIME")
                       .setSelector("2")
                       .build();
    auto subscription = ResponsePacket()   //
                            .setType(FieldbusResponseType::command_response)
                            .setBody({0x50_b, ';'_b});
    auto publication = ResponsePacket()            //
                           .setFirstPacket(true)   //
                           .setLastPacket(true)    //
                           .setBody({
                               0x50_b, 0b1010'0001_b, 0x03_b,                         // subscription_id, flags, user
                               0x00_b, 0x00_b,        0x01_b, 0x05_b,                 // timestamp seconds
                               0x00_b, 0x00_b,        0x0A_b, 0x0D_b,                 // timestamp microseconds
                               'v'_b,  'a'_b,         'l'_b,  'u'_b,  'e'_b, ':'_b,   // label
                               'c'_b,  'h'_b,         'a'_b,  'r'_b,  ':'_b,          // type
                               'A'_b,  'B'_b,         'C'_b,  'D'_b,                  // body
                               ';'_b                                                  // termination
                           });


    // **********************************************************
    // Execution
    handler->processSub(subscription, command);
    handler->processPublication(publication);


    // **********************************************************
    // Verification
    ASSERT_EQ(queue.isEmpty(), true);
}

// **********************************************************

TEST_F(FsmPublicationTests, ppmSubscription)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setProperty("TIME")
                       .setSelector("")
                       .build();
    auto subscription = ResponsePacket()   //
                            .setType(FieldbusResponseType::command_response)
                            .setBody({0x50_b, ';'_b});
    auto publication_1 = ResponsePacket()            //
                             .setFirstPacket(true)   //
                             .setLastPacket(true)    //
                             .setBody({
                                 0x50_b, 0b1010'0001_b, 0x02_b,                         // subscription_id, flags, user
                                 0x00_b, 0x00_b,        0x01_b, 0x05_b,                 // timestamp seconds
                                 0x00_b, 0x00_b,        0x0A_b, 0x0D_b,                 // timestamp microseconds
                                 'v'_b,  'a'_b,         'l'_b,  'u'_b,  'e'_b, ':'_b,   // label
                                 'c'_b,  'h'_b,         'a'_b,  'r'_b,  ':'_b,          // type
                                 'A'_b,  'B'_b,         'C'_b,  'D'_b,                  // body
                                 ';'_b                                                  // termination
                             });
    auto publication_2 = ResponsePacket()            //
                             .setFirstPacket(true)   //
                             .setLastPacket(true)    //
                             .setBody({
                                 0x50_b, 0b1010'0001_b, 0x03_b,                         // subscription_id, flags, user
                                 0x00_b, 0x00_b,        0x01_b, 0x05_b,                 // timestamp seconds
                                 0x00_b, 0x00_b,        0x0A_b, 0x0D_b,                 // timestamp microseconds
                                 'v'_b,  'a'_b,         'l'_b,  'u'_b,  'e'_b, ':'_b,   // label
                                 'c'_b,  'h'_b,         'a'_b,  'r'_b,  ':'_b,          // type
                                 'E'_b,  'F'_b,         'G'_b,  'H'_b,                  // body
                                 ';'_b                                                  // termination
                             });


    // **********************************************************
    // Execution
    handler->processSub(subscription, command);
    handler->processPublication(publication_1);
    handler->processPublication(publication_2);


    // **********************************************************
    // Verification
    auto result1 = queue.pop();
    auto result2 = queue.pop();

    ASSERT_EQ(queue.isEmpty(), true);
    ASSERT_EQ(result1.has_value(), true);
    ASSERT_EQ(result2.has_value(), true);

    auto response1 = (*result1)->response();
    auto response2 = (*result2)->response();

    ASSERT_EQ(std::holds_alternative<PublicationResponse>(response1), true);
    ASSERT_EQ(std::holds_alternative<PublicationResponse>(response2), true);

    auto publication_response1 = std::get<PublicationResponse>(response1);
    auto publication_response2 = std::get<PublicationResponse>(response2);

    EXPECT_EQ(publication_response1.values.size(), 1);
    EXPECT_THAT(publication_response1.values[0]->get(), ::testing::VariantWith<std::string>("ABCD"));

    EXPECT_EQ(publication_response2.values.size(), 1);
    EXPECT_THAT(publication_response2.values[0]->get(), ::testing::VariantWith<std::string>("EFGH"));
}

// EOF
