// #include <cstddef>
// #include <cstdint>
// #include <gtest/gtest.h>

// #include <cpp_utils/misc.h>
// #include <modules/base/core/inc/types.h>
// #include <modules/devices/programming/inc/types.h>
// #include <utils/test/mocks/RootMock.h>
// #include <utils/test/utils/utils.h>
// #include "../inc/CodeDistributor.h"
// #include "../inc/FgcCode.h"
// #include "../inc/TimePacket.h"

// using namespace fgcd::fieldbus;
// using namespace fgcd;

// // **********************************************************
// // Setup
// // **********************************************************

// struct VarCodeArgs
// {
//     FgcCode      code;
//     std::uint8_t block_index{};
// };

// struct AdvCodeArgs
// {
//     FgcCode      code;
//     std::uint8_t code_index{};
//     std::uint8_t list_length{};
// };

// class CodeDistributorTests : public ::testing::Test
// {
//   public:
//     RootMockPtr        root;
//     CodeDistributorPtr distributor;
//     std::size_t block_size{32};

//     void SetUp() override
//     {
//         root        = std::make_unique<RootMock>();
//         distributor = root->addComponent<CodeDistributor>();
//     }
// };

// // **********************************************************
// // Tests
// // **********************************************************

// TEST_F(CodeDistributorTests, publishDefaultCode)
// {
//     // **********************************************************
//     // Preparation
//     TimePacket timePacket = {};


//     // **********************************************************
//     // Execution
//     distributor->publishCode();


//     // **********************************************************
//     // Verification
//     // clang-format off
//     ASSERT_EQ(timepacket.adv_codes.size(),         1);
//     EXPECT_EQ(timepacket.adv_codes[0].code,        FgcCode());
//     EXPECT_EQ(timepacket.adv_codes[0].code_index,  0);
//     EXPECT_EQ(timepacket.adv_codes[0].list_length, 32);
//     ASSERT_EQ(timepacket.var_codes.size(),         1);
//     EXPECT_EQ(timepacket.var_codes[0].block_index, 0);
//     EXPECT_EQ(timepacket.var_codes[0].code,        FgcCode());
//     // clang-format on
// }

// // **********************************************************

// TEST_F(CodeDistributorTests, setCode_wrongEndpoint)
// {
//     // **********************************************************
//     // Preparation
//     ReprogramData data{};


//     // **********************************************************
//     // Execution
//     auto success = distributor->setCode(data);


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(success, false);
// }

// // **********************************************************

// TEST_F(CodeDistributorTests, advertiseCode)
// {
//     // **********************************************************
//     // Preparation
//     FgcCodeInfo header{
//         .version  = 0x11223344,   //
//         .class_id = 0x55,         //
//         .code_id  = 0x66,         //
//         .crc      = 0x7788        //
//     };
//     auto          header_bytes = serializeCodeHeader(header);
//     auto          binary       = ByteStream(1000, 0xAA_b) + header_bytes;
//     ReprogramData data{
//         .endpoint = {.subsystem_id = "TEST:24"},   //
//         .binary   = binary                         //
//     };
//     auto expected_code = createFgcCode(binary);


//     // **********************************************************
//     // Execution
//     auto success = distributor->setCode(data);
//     distributor->publishCode();
//     distributor->publishCode();


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(success, true);
//     ASSERT_EQ(timepacket.adv_codes.size(), 2);
//     EXPECT_EQ(timepacket.adv_codes[0].code_index, 0);
//     EXPECT_EQ(timepacket.adv_codes[0].code, FgcCode());
//     EXPECT_EQ(timepacket.adv_codes[1].code_index, 1);
//     EXPECT_EQ(timepacket.adv_codes[1].code, expected_code);
// }

// // **********************************************************

// TEST_F(CodeDistributorTests, distributingCode)
// {
//     // **********************************************************
//     // Preparation
//     FgcCodeInfo header{
//         .version  = 0x11223344,   //
//         .class_id = 0x55,         //
//         .code_id  = 0x66,         //
//         .crc      = 0x7788        //
//     };
//     auto          header_bytes = serializeCodeHeader(header);
//     auto          binary       = ByteStream(2 * 10 * block_size, 0xAA_b) + header_bytes;
//     ReprogramData data{
//         .endpoint = {.subsystem_id = "TEST:24"},   //
//         .binary   = binary                         //
//     };
//     auto expected_code = createFgcCode(binary);

//     // **********************************************************
//     // Execution
//     auto success = distributor->setCode(data);
//     distributor->vote(0x0000'0001 << 1);
//     4_times(
//         [this]()
//         {
//             distributor->publishCode();
//         }
//     );


//     // **********************************************************
//     // Verification
//     ASSERT_EQ(success, true);
//     ASSERT_EQ(timepacket.var_codes.size(), 4);
//     EXPECT_EQ(timepacket.var_codes[0].block_index, 0);
//     EXPECT_EQ(timepacket.var_codes[0].code, expected_code);
//     EXPECT_EQ(timepacket.var_codes[1].block_index, 10);
//     EXPECT_EQ(timepacket.var_codes[1].code, expected_code);
//     EXPECT_EQ(timepacket.var_codes[2].block_index, 20);
//     EXPECT_EQ(timepacket.var_codes[2].code, expected_code);
//     EXPECT_EQ(timepacket.var_codes[3].block_index, 0);
//     EXPECT_EQ(timepacket.var_codes[3].code, FgcCode());
// }

// // EOF
