#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/StatusServer.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct StatusProvideTests : public ::testing::Test
{
    RootMockPtr root;
    StatusProvidePtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<StatusServer>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(StatusProvideTests, test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF

