#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <optional>
#include <string>
#include <utility>

#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>
#include <utils/test/mocks/RootMock.h>
#include <utils/test/printers/Printers.h>
#include <utils/test/utils/CommandBuilder.h>
#include <utils/test/utils/utils.h>
#include "../inc/CommandPacket.h"
#include "../inc/FsmCommand.h"

using namespace utils;
using namespace fgcd;


// **********************************************************
// Setup
// **********************************************************

struct TestCommandPacket : public fieldbus::CommandPacket
{
    static constexpr auto buffer_size = 10;
    std::string           payload;

    std::size_t doSetCommandBuffer(std::string_view content) override
    {
        payload = content;
        return buffer_size;
    };
};

struct FsmCommandTests : public ::testing::Test
{
    static constexpr auto buffer_size = 1494;
    RootMockPtr           root;
    FsmCommandPtr         command_fsm;
    TestCommandPacket     command_packet;
    CommandBuilder        command_builder;

    void SetUp() override
    {
        root        = std::make_unique<RootMock>();
        command_fsm = root->addComponent<FsmCommand>(command_packet);

        root->enableLogging();
        root->start();
    }

    void TearDown() override
    {
        root->stop();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FsmCommandTests, onePacketCheck)
{
    // **********************************************************
    // Preparation
    CommandPtr cmd = command_builder   //
                      .setType(CommandType::SET)
                      .setProperty("TEST")
                      .setValue("VALUE")
                      .setSelector("11")
                      .build();


    // **********************************************************
    // Execution
    command_fsm->setCommand(*cmd);
    command_fsm->publish();
    auto has_frame = command_fsm->hasFrameToSend();


    // **********************************************************
    // Verification
    EXPECT_EQ(command_packet.payload, "TEST VALUE");
    EXPECT_EQ(command_packet.isLastPacket(), true);
    EXPECT_EQ(command_packet.isFirstPacket(), true);
    EXPECT_EQ(command_packet.getUser(), 11);
    EXPECT_EQ(command_packet.getType(), fieldbus::FieldbusCommandType::set);
}

// **********************************************************

TEST_F(FsmCommandTests, multiPacketPacket)
{
    // **********************************************************
    // Preparation
    CommandPtr cmd = command_builder   //
                      .setType(CommandType::GET)
                      .setProperty(std::string(25, 'c'))
                      .setSelector("11")
                      .build();
    command_fsm->setCommand(*cmd);

    // **********************************************************
    // Execution
    auto has_frame_0 = command_fsm->hasFrameToSend();
    command_fsm->publish();


    // **********************************************************
    // Verification
    ASSERT_EQ(has_frame_0, true);
    EXPECT_EQ(command_packet.payload, std::string(25 - 0 * TestCommandPacket::buffer_size, 'c'));
    EXPECT_EQ(command_packet.isLastPacket(), false);
    EXPECT_EQ(command_packet.isFirstPacket(), true);


    // **********************************************************
    // Execution
    auto has_frame_1 = command_fsm->hasFrameToSend();
    command_fsm->publish();


    // **********************************************************
    // Verification
    ASSERT_EQ(has_frame_1, true);
    EXPECT_EQ(command_packet.payload, std::string(25 - 1 * TestCommandPacket::buffer_size, 'c'));
    EXPECT_EQ(command_packet.isLastPacket(), false);
    EXPECT_EQ(command_packet.isFirstPacket(), false);


    // **********************************************************
    // Execution
    auto has_frame_2 = command_fsm->hasFrameToSend();
    command_fsm->publish();


    // **********************************************************
    // Verification
    ASSERT_EQ(has_frame_2, true);
    EXPECT_EQ(command_packet.payload, std::string(25 - 2 * TestCommandPacket::buffer_size, 'c'));
    EXPECT_EQ(command_packet.isLastPacket(), true);
    EXPECT_EQ(command_packet.isFirstPacket(), false);


    // **********************************************************
    // Execution
    auto has_frame_3 = command_fsm->hasFrameToSend();


    // **********************************************************
    // Verification
    EXPECT_EQ(has_frame_3, false);
}

// // EOF
