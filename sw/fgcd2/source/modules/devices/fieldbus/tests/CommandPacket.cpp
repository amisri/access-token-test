#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/CommandPacket.h"

using namespace fgcd::fieldbus;

// **********************************************************
// Setup
// **********************************************************

struct FieldbusCommandPacketTests : public ::testing::Test
{
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FieldbusCommandPacketTests, test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
}

// EOF
