#include <cstddef>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cpp_utils/bits.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>
#include <modules/devices/base/inc/Timestamp.h>
#include <utils/test/mocks/RootMock.h>
#include <utils/test/printers/Printers.h>
#include <utils/test/utils/CommandBuilder.h>
#include "../inc/FsmResponse.h"
#include "../inc/ResponsePacket.h"

using namespace utils;
using namespace fgcd;
using namespace fgcd::fieldbus;

// **********************************************************
// Setup
// **********************************************************

class FsmResponseTests : public ::testing::Test
{
  public:
    RootMockPtr    root;
    FsmResponsePtr response_fsm;
    CommandBuilder command_builder;

    void SetUp() override
    {
        root         = std::make_unique<RootMock>();
        response_fsm = root->addComponent<FsmResponse>();
        root->enableLogging();
        root->start();
    }

    void TearDown() override
    {
        root->stop();
    }

    // Command createCommand(CommandType type, bool parse, bool ppm)
    // {
    //     std::optional<std::string> selector = std::nullopt;
    //     if(ppm)
    //     {
    //         selector = "";
    //     }

    //     return {
    //         type,         "", std::ignore,
    //         "",             //
    //         selector,       //
    //         std::nullopt,   //
    //         std::nullopt,   //
    //         "",             //
    //     };
    // }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(FsmResponseTests, properResponse)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet = ResponsePacket()   //
                      .setFirstPacket(true)
                      .setLastPacket(true)
                      .setBody({
                          0x01_b, 0x02_b, 0x03_b, 0x04_b,     //
                          0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,     //
                          'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                      });


    // **********************************************************
    // Execution
    auto response = response_fsm->process(packet, *command);


    // **********************************************************
    // Verification
    ASSERT_EQ(response.has_value(), true);
    EXPECT_EQ(response->timestamp, Timestamp({.second = 0x0102'0304, .microsecond = 0x0A0B'0C0D}));
    EXPECT_THAT(response->values.size(), 1);
    EXPECT_THAT(response->values[0]->get(), ::testing::VariantWith<std::string>("TEST"));
}

// **********************************************************

TEST_F(FsmResponseTests, unterminatedResponse)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet = ResponsePacket()   //
                      .setFirstPacket(true)
                      .setLastPacket(true)
                      .setBody({
                          0x01_b, 0x02_b, 0x03_b, 0x04_b,   //
                          0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,   //
                          'T'_b, 'E'_b, 'S'_b, 'T'_b        //
                      });


    // **********************************************************
    // Execution
    auto response = response_fsm->process(packet, *command);


    // **********************************************************
    // Verification
    ASSERT_EQ(response.has_value(), false);
}

// **********************************************************

TEST_F(FsmResponseTests, twoPacketResponse)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet_1 = ResponsePacket()   //
                        .setFirstPacket(true)
                        .setLastPacket(false)
                        .setSequence(1)
                        .setBody({
                            0x01_b, 0x02_b, 0x03_b, 0x04_b,   //
                            0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,   //
                            'T'_b, 'E'_b, 'S'_b, 'T'_b        //
                        });
    auto packet_2 = ResponsePacket()   //
                        .setFirstPacket(false)
                        .setLastPacket(true)
                        .setSequence(2)
                        .setBody({
                            'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                        });


    // **********************************************************
    // Execution
    auto response_1 = response_fsm->process(packet_1, *command);
    ASSERT_EQ(response_1.has_value(), false);
    auto response_2 = response_fsm->process(packet_2, *command);
    ASSERT_EQ(response_2.has_value(), true);


    // **********************************************************
    // Verification
    EXPECT_THAT(response_2->values.size(), 1);
    EXPECT_THAT(response_2->values[0]->get(), ::testing::VariantWith<std::string>("TESTTEST"));
}

// **********************************************************

TEST_F(FsmResponseTests, threePacketResponse)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet_1 = ResponsePacket()   //
                        .setFirstPacket(true)
                        .setLastPacket(false)
                        .setSequence(1)
                        .setBody({
                            0x01_b, 0x02_b, 0x03_b, 0x04_b,   //
                            0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,   //
                            'T'_b, 'E'_b, 'S'_b, 'T'_b        //
                        });
    auto packet_2 = ResponsePacket()   //
                        .setFirstPacket(false)
                        .setLastPacket(false)
                        .setSequence(2)
                        .setBody({
                            'T'_b, 'E'_b, 'S'_b, 'T'_b,   //
                        });
    auto packet_3 = ResponsePacket()   //
                        .setFirstPacket(false)
                        .setLastPacket(true)
                        .setSequence(3)
                        .setBody({
                            'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                        });


    // **********************************************************
    // Execution
    auto response_1 = response_fsm->process(packet_1, *command);
    ASSERT_EQ(response_1.has_value(), false);
    auto response_2 = response_fsm->process(packet_2, *command);
    ASSERT_EQ(response_2.has_value(), false);
    auto response_3 = response_fsm->process(packet_3, *command);
    ASSERT_EQ(response_3.has_value(), true);


    // **********************************************************
    // Verification
    EXPECT_THAT(response_3->values.size(), 1);
    EXPECT_THAT(response_3->values[0]->get(), ::testing::VariantWith<std::string>("TESTTESTTEST"));
}

// **********************************************************

TEST_F(FsmResponseTests, noFirstPackets)
{
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet = ResponsePacket()   //
                      .setFirstPacket(false)
                      .setLastPacket(true)
                      .setBody({
                          0x01_b, 0x02_b, 0x03_b, 0x04_b,     //
                          0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,     //
                          'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                      });


    // **********************************************************
    // Execution
    auto response = response_fsm->process(packet, *command);


    // **********************************************************
    // Verification
    ASSERT_EQ(response.has_value(), false);
}

// **********************************************************

TEST_F(FsmResponseTests, wrongSequenceNumber)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet_1 = ResponsePacket()   //
                        .setFirstPacket(true)
                        .setLastPacket(false)
                        .setSequence(1)
                        .setBody({
                            0x01_b, 0x02_b, 0x03_b, 0x04_b,   //
                            0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,   //
                            'T'_b, 'E'_b, 'S'_b, 'T'_b        //
                        });
    auto packet_2 = ResponsePacket()   //
                        .setFirstPacket(false)
                        .setLastPacket(true)
                        .setSequence(3)
                        .setBody({
                            'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                        });


    // **********************************************************
    // Execution
    auto response_1 = response_fsm->process(packet_1, *command);
    ASSERT_EQ(response_1.has_value(), false);
    auto response_2 = response_fsm->process(packet_2, *command);


    // **********************************************************
    // Verification
    ASSERT_EQ(response_2.has_value(), false);
}

// **********************************************************

TEST_F(FsmResponseTests, secondResponse)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet = ResponsePacket()   //
                      .setFirstPacket(true)
                      .setLastPacket(true)
                      .setBody({
                          0x01_b, 0x02_b, 0x03_b, 0x04_b,     //
                          0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,     //
                          'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                      });


    // **********************************************************
    // Execution
    auto response_1 = response_fsm->process(packet, *command);
    ASSERT_EQ(response_1.has_value(), true);
    response_fsm->nextCycle();
    auto response_2 = response_fsm->process(packet, *command);


    // **********************************************************
    // Verification
    ASSERT_EQ(response_2.has_value(), true);
}

// **********************************************************

TEST_F(FsmResponseTests, secondResponseWithoutReset)
{
    // **********************************************************
    // Preparation
    auto command = command_builder   //
                       .setType(CommandType::GET)
                       .setSelector("0")
                       .build();
    auto packet = ResponsePacket()   //
                      .setFirstPacket(true)
                      .setLastPacket(true)
                      .setBody({
                          0x01_b, 0x02_b, 0x03_b, 0x04_b,     //
                          0x0A_b, 0x0B_b, 0x0C_b, 0x0D_b,     //
                          'T'_b, 'E'_b, 'S'_b, 'T'_b, ';'_b   //
                      });


    // **********************************************************
    // Execution
    auto response_1 = response_fsm->process(packet, *command);
    ASSERT_EQ(response_1.has_value(), true);
    auto response_2 = response_fsm->process(packet, *command);


    // **********************************************************
    // Verification
    ASSERT_EQ(response_2.has_value(), false);
}

// EOF
