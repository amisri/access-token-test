//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string_view>

#include <cpp_utils/Result.h>
#include <cpp_utils/enumStr.h>
#include <cpp_utils/text.h>
#include <interfaces/IValueItem.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/common/inc/ValueLeaf.h>

namespace fgcd
{
    namespace value_parser
    {
        enum class PropertyType
        {
            float_type,
            char_type,
            point,
            int32s,
            int16s,
            int8s,
            int32u,
            int16u,
            int8u,
            string
        };

        // **********************************************************

        MAKE_ENUM_STR(                                       // NOLINT
            PropertyTypeStr, PropertyType,                   //
            {PropertyType::float_type, "float", "FLOAT"},    //
            {PropertyType::char_type, "char", "CHAR"},       //
            {PropertyType::point, "point", "POINT"},         //
            {PropertyType::int32s, "int32s", "INT32S"},      //
            {PropertyType::int16s, "int16s", "INT16S"},      //
            {PropertyType::int8s, "int8s", "INT8S"},         //
            {PropertyType::int32u, "int32u", "INT32U"},      //
            {PropertyType::int16u, "int16u", "INT16U"},      //
            {PropertyType::int8u, "int8u", "INT8U"},         //
            {PropertyType::string, "string", "STRING"},      //
        );

    }

    class ValueParser
    {
        static constexpr auto             binary_data_flag = 0xff_b;
        static constexpr std::string_view leaf_label       = "value";

      public:
        static utils::Result<IValueItemPtr, std::string> parse(ByteStreamView bytes, bool parse = true);
        static utils::Result<IValueVector, std::string>  parsePPM(ByteStreamView bytes, bool parse = true);

      private:
        static utils::Result<IValueItemPtr, std::string> parsePayload(std::string_view stream, value_parser::PropertyType type);
        static utils::Result<IValueItemPtr, std::string> deserializePayload(ByteStreamView bytes, value_parser::PropertyType type);

        static std::pair<std::string_view, ByteStreamView> asStringUntil(ByteStreamView bytes, std::byte sentinel);

        // **********************************************************

        template<typename T>
        static utils::Result<IValueItemPtr, std::string> parsePayloadTyped(std::string_view stream)
        {
            std::vector<T> elements;
            elements.reserve(100);

            for (auto&& [column, rest] = utils::splitFirst(stream, ',');   //
                 not column.empty();                                       //
                 std::tie(column, rest) = utils::splitFirst(rest, ','))
            {
                auto value = parseColumn<T>(column);
                if (value.hasError())
                {
                    return utils::Failure{value.error()};
                }
                elements.push_back(*value);
            }

            if (elements.empty())
            {
                return utils::Failure{"No elements parsed"};
            }

            if (elements.size() == 1)
            {
                return {std::make_shared<ValueLeaf>(elements.front())};
            }

            return {std::make_shared<ValueLeaf>(elements)};
        }

        // **********************************************************

        template<typename T>
        static utils::Result<T, std::string> parseColumn(std::string_view stream)
        {
            if constexpr (utils::numeric<T>)
            {
                return utils::parseNumber<T>(stream);
            }
            else if constexpr (std::is_same_v<T, Point>)
            {
                auto [time_string, value_string] = utils::splitFirst(stream, '|');

                auto time  = utils::parseNumber<float>(time_string);
                auto value = utils::parseNumber<float>(value_string);

                if (!time or !value)
                {
                    return utils::Failure{"Cannot parse Point"};
                }

                return Point({*time, *value});
            }
            else if constexpr (std::is_same_v<T, std::string>)
            {
                return std::string(stream);
            }
            else
            {
                static_assert(utils::dependent_false_v<T>, "Not implemented");
            }
        }

        // **********************************************************

        template<typename T>
        static utils::Result<IValueItemPtr, std::string> deserializePayloadTyped(BigReader& reader)
        {
            auto data_length = reader.unreadData().size() / sizeof(T);
            auto result      = reader.template readContainer<std::vector<T>>(data_length);

            return result.map(
                [](auto&& value) -> IValueItemPtr
                {
                    if (value.size() == 1)
                    {
                        return std::make_shared<ValueLeaf>(value.front());
                    }

                    return std::make_shared<ValueLeaf>(value);
                }
            );
        }
    };
}

// EOF
