//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>

#include <modules/base/core/inc/types.h>
#include "FgcCode.h"
#include "PODs.h"

namespace fgcd::fieldbus
{
    class TimePacket
    {
        static constexpr auto max_runlog_index = 500;

      public:
        TimePacket()                             = default;
        TimePacket(const TimePacket&)            = delete;
        TimePacket(TimePacket&&)                 = delete;
        TimePacket& operator=(const TimePacket&) = delete;
        TimePacket& operator=(TimePacket&&)      = delete;
        virtual ~TimePacket()                    = default;

        void setTime(fgcd::TimePoint time);
        void tick();
        void advertiseCode(const FgcCode& code, std::uint8_t code_index, std::uint8_t list_length);
        void distributCode(const FgcCode& code, std::uint16_t block_index);
        FieldbusTime getTime();

      private:
        FieldbusTime m_time;
    };
}

// EOF
