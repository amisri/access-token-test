//! @file
//! @brief  Command FSM class used to send FGC Ether commands.
//! @author Adam Solawa

#pragma once

#include <memory>

#include <cpp_utils/fsm.h>
#include <modules/base/component/inc/Component.h>
#include <modules/devices/base/inc/Command.h>
#include "CommandPacket.h"
#include "PODs.h"

namespace fgcd
{
    class FsmCommand;
    using FsmCommandPtr = std::shared_ptr<FsmCommand>;

    class FsmCommand : public Component
    {
        //! Enum defining FGC Ether command's sending states.
        enum class State
        {
            first_packet,
            middle_packet,
            last_packet
        };

        using TransRes     = utils::FsmTransitionResult<State>;
        using StateMachine = utils::Fsm<State, FsmCommand>;

      public:
        FsmCommand(Component& parent, fieldbus::CommandPacket& command_packet);

        bool setCommand(const Command& command);

        void publish();

        [[nodiscard]] bool hasFrameToSend() const;

      private:
        bool saveCommand(const Command& command);

        //! Copy appropriate chunk of command's buffer into frame to be sent.
        void publishPayload();
        void publishHeader();

        // **********************************************************
        // FMS state function
        // **********************************************************

        void firstPacketState();
        void middlePacketState();
        void lastPacketState();

        // **********************************************************
        // FMS transition function
        // **********************************************************

        TransRes xxToLastPacket();
        TransRes firstToNextPacket();
        TransRes lastPacketToFirst();


        //! Dependencies
        StateMachine             m_fsm;              //!< Class handling finite state machine logic.
        fieldbus::CommandPacket& m_command_packet;   //!< TODO

        //! State variables
        std::uint8_t                  m_selector;
        fieldbus::FieldbusCommandType m_type;
        bool                          m_command_finished = true;   //!< Flag determines if entire command was sent.
        std::string                   m_buffer;                    //!< Buffer storing entire command in textual form.
        std::string::size_type        m_buffer_offset = 0;         //!< Offset in sending buffer.
    };
}

// EOF
