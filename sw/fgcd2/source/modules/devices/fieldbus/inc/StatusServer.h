//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <array>
#include <functional>
#include <memory>
#include <semaphore>
#include <vector>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/thread/inc/Thread.h>

// **********************************************************

namespace fgcd
{
    class StatusServer;
    using StatusProvidePtr = std::shared_ptr<StatusServer>;

    class StatusServer : public Component
    {
        static constexpr auto max_devices = 60;
        using StatusCallback = std::function<void(const std::array<ByteStream, max_devices>&)>;

      public:
        StatusServer(Component& parent);

        void push(int device_id, ByteStream data)
        {
            m_statuses.at(device_id) = std::move(data);
        }

        void registerCallback(StatusCallback callback)
        {
            m_callbacks.push_back(std::move(callback));
        }

        void worker(std::atomic_bool& keep_running)
        {
            while (keep_running)
            {
                m_guard.acquire();

                for (auto&& callback : m_callbacks)
                {
                    callback(m_statuses);
                }

                m_statuses = {};
            }
        }

        void publish()
        {
            m_guard.release();
        }

      private:
        Thread                              m_thread;
        std::array<ByteStream, max_devices> m_statuses;
        std::binary_semaphore               m_guard{0};
        std::vector<StatusCallback>         m_callbacks;
    };
}

// EOF
