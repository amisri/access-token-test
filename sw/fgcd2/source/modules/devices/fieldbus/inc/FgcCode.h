//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>

namespace fgcd::fieldbus
{
    struct FgcCodeInfo
    {
        static constexpr auto empty_code_id = 0xFF;

        std::array<char, 20> version_string = {};
        std::uint32_t        version        = 0;
        std::uint8_t         class_id       = 0;
        std::uint8_t         code_id        = empty_code_id;
        std::uint16_t        old_version    = 0;
        std::uint16_t        crc            = 0;

        auto operator<=>(const FgcCodeInfo& other) const = default;
    };

    class FgcCode
    {
        static constexpr std::size_t header_size = 30;
        static constexpr std::size_t npos        = -1;

      public:
        static constexpr std::size_t block_size = 32;

        FgcCode()                                    = default;
        auto operator<=>(const FgcCode& other) const = default;

        static utils::Result<FgcCode, std::string> fromBytes(ByteStreamView bytes);

        [[nodiscard]] bool           valid() const;
        [[nodiscard]] ByteStreamView getCodeBlocks(std::size_t offset = 0, std::size_t count = npos) const;
        [[nodiscard]] std::uint16_t  getBlocksLength() const;
        [[nodiscard]] std::uint8_t   getClassId() const;
        [[nodiscard]] std::uint8_t   getCodeId() const;
        [[nodiscard]] std::uint16_t  getCrc() const;
        [[nodiscard]] std::uint32_t  getVersion() const;


      private:
        FgcCode(FgcCodeInfo header, ByteStream body);

        FgcCodeInfo   m_header        = {};
        ByteStream    m_body          = {};
        std::uint16_t m_blocks_length = 0;
    };
};

// EOF
