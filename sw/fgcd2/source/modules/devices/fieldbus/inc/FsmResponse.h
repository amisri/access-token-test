//! @file
//! @brief  Response FSM class used to handle reception of FGC Ether commands's responses.
//! @author Adam Solawa

#pragma once

#include <memory>
#include <source_location>

#include <cpp_utils/Result.h>
#include <cpp_utils/fsm.h>
#include <cpp_utils/misc.h>
#include <cpp_utils/typeTraits.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/LogEntry.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Response.h>
#include "ResponsePacket.h"
#include "ValueParser.h"

namespace fgcd
{
    class FsmResponse;
    using FsmResponsePtr = std::shared_ptr<FsmResponse>;

    //! This class is used to store state of currently processed response.
    //! Note: After a response is processed, class needs to be rested to process a next response.
    class FsmResponse : public Component
    {
        //! Enum defining FGC Ether response's states.
        enum class State
        {
            start,
            first,
            next,
            end,
            error
        };

        using TransRes     = utils::FsmTransitionResult<State>;
        using StateMachine = utils::Fsm<State, FsmResponse>;

      public:
        //! Constructor initializes the component and setup fsm.
        //!
        //! @param parent a parent component.
        //! @param device a FGC Ether device object.
        explicit FsmResponse(Component& parent);

        //! Updates reception finite state machine.
        std::optional<CommandResponse> process(const fieldbus::ResponsePacket& packet, const Command& command);

        //! Resets the response state.
        //! The method should be used before processing next response.
        void nextCycle();

      private:
        void parseGet();

        //! FMS state function
        void startState();
        //! FMS state function
        void firstState();
        //! FMS state function
        void nextState();
        //! FMS state function
        void endState();
        //! FMS state function
        void errorState();

        //! FMS transition function
        TransRes startToXx();
        //! FMS transition function
        TransRes startToSubscription();
        //! FMS transition function
        TransRes firstPktToNextPkt();
        //! FMS transition function
        TransRes xxToEnd();
        //! FMS transition function.
        TransRes nextToError();

        //! Call arguments
        const fieldbus::ResponsePacket* m_packet = nullptr;   //!< Pointer to currently processed response packet.

        //! State variables
        CommandType          m_type;
        bool                 m_parse;
        bool                 m_is_ppm;
        bool                 m_error        = true;   //!< Indicates if an error has occurred during response processing
        bool                 m_subscription = false;      //!< Indicates if processed packet is subscription.
        utils::CyclicInt<16> m_next_sequence      = 0;    //!< Stores expected value for next packet sequence number.
        std::optional<CommandResponse> m_response = {};   //!< Content of currently processed response.
        ByteStream                     m_buffer   = {};   //!< Buffer aggregating responses bodies.

        //! Dependencies
        StateMachine m_fsm;   //!< Class handling finite state machine logic.
        ValueParser  m_value_parser;
    };
}

// EOF
