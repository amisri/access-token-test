//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <optional>
#include <string>

#include <modules/devices/base/inc/Command.h>

namespace fgcd
{
    inline std::optional<std::uint8_t> getFieldbusSelector(std::optional<std::string> selector)
    {
        if (not selector.has_value())
        {
            return 0;
        }

        if (selector->empty())
        {
            return 0;
        }

        auto number = utils::parseNumber<std::uint8_t>(*selector);
        if (number.hasError())
        {
            return std::nullopt;
        }

        return *number;
    }
}

// EOF
