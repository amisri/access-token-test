//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <optional>

#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Timestamp.h>
#include "PODs.h"

namespace fgcd::fieldbus
{
    class PublicationPacket
    {
      public:
        PublicationPacket() = default;
        PublicationPacket(PublicationPacketPOD pod);

        static utils::Result<PublicationPacket, std::string> fromBytes(BigReader& frame);

        [[nodiscard]] bool               isNewSubscription() const;
        [[nodiscard]] bool               isSetTrigger() const;
        [[nodiscard]] std::optional<int> getSubDevice() const;
        [[nodiscard]] std::uint8_t       getId() const;
        [[nodiscard]] std::uint8_t       getUser() const;
        [[nodiscard]] Timestamp          getTimestamp() const;

      private:
        PublicationPacketPOD m_pod = {};
    };
}

// EOF
