//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <map>
#include <memory>

#include <cpp_utils/fsm.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Subscription.h>
#include <modules/devices/base/inc/Timestamp.h>
#include "Enums.h"
#include "PublicationPacket.h"
#include "ResponsePacket.h"

namespace fgcd
{
    class FsmPublication;
    using FsmPublicationPtr = std::shared_ptr<FsmPublication>;

    class FsmPublication : public Component
    {
        //! Enum defining FGC subscriptions states.
        enum class State
        {
            start,
            first,
            next,
            end,
            error,
        };

        //! Finite state machine utils types
        using TransRes     = utils::FsmTransitionResult<State>;
        using StateMachine = utils::Fsm<State, FsmPublication>;

        using Subscriptions = std::map<std::uint8_t, CommandPtr>;

        static constexpr auto max_sequence = 0x0F;
        //! All user id

      public:
        FsmPublication(Component& parent, int device_id);
        void      processPublication(const fieldbus::ResponsePacket& packet);
        FgcStatus processSub(const fieldbus::ResponsePacket& packet, const CommandPtr& command);
        FgcStatus processUnsub(const fieldbus::ResponsePacket& packet);

      private:
        //! FMS state function.
        void startState();
        //! FMS state function.
        void firstState();
        //! FMS state function.
        void nextState();
        //! FMS state function.
        void endState();
        //! FMS state function.
        void errorState();

        //! FMS transition function.
        TransRes checkFirstPacket();
        //! FMS transition function.
        TransRes checkLastPacket();
        //! FMS transition function.
        TransRes reset();
        //! FMS transition function.
        TransRes checkError();
        //! FMS transition function.
        //! Checks if sequence number has correct value.
        TransRes checkSequence();

        StateMachine                    m_fsm;                         //! State machine object.
        int                             m_device_id       = 0;         //! Device id.
        Subscriptions                   m_subscriptions   = {};        //! Maps subscription id into publication object.
        const fieldbus::ResponsePacket* m_response_packet = nullptr;   //! Pointer to currently processed packet.
        fieldbus::PublicationPacket     m_publication_packet;          //!
        std::uint8_t                    m_current_subscription_id = 0;   //!
        std::uint8_t                    m_next_sequence : 4       = 0;   //! Expected value of next sequence number.
        bool           m_error           = false;     //! Indicates if currently processed publication has any errors.
        ByteStream     m_buffer          = {};        //!< Buffer aggregating publication bodies.
        const Command* m_current_command = nullptr;   //!< Pointer to currently executed command,
    };
}

// EOF
