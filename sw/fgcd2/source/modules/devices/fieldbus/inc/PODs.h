//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstddef>
#include <cstdint>

#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Timestamp.h>
#include "Enums.h"

namespace fgcd::fieldbus
{
    // **********************************************************
    // Common
    // **********************************************************

    enum class PacketType
    {
        status,
        response,
        notification
    };

    enum class FieldbusCommandType : std::uint8_t
    {
        set     = 0x01,
        get     = 0x02,
        set_bin = 0x03,
        sub     = 0x04,
        unsub   = 0x05
    };

    // **********************************************************
    // Status packet
    // **********************************************************

    struct StatusAcknowlegment
    {
        std::uint8_t status               : 2 = {};
        bool         time_received        : 1 = false;
        bool         rt_received          : 1 = false;
        bool         command_toggle       : 1 = false;
        bool         pll_locked           : 1 = false;
        bool         post_mortem          : 1 = false;
        bool         external_post_mortem : 1 = false;
    };
    static_assert(sizeof(StatusAcknowlegment) == 1, "Unexpected size");

    struct StatusPacketPOD
    {
        std::byte                    acknowlegment       = {};
        std::uint8_t                 class_id            = 0;
        FgcStatus                    command_status      = FgcStatus::unknown_error_code;
        std::uint8_t                 runlog_index        = 0;
        std::array<std::uint8_t, 4>  diagnostic_channels = {};
        std::array<std::uint16_t, 4> diagnostic_datas    = {};
        std::array<std::byte, 40>    class_specific_data = {};
    };

    struct StatusPacketBarePOD
    {
        std::byte                 data_status         = {};
        std::uint8_t              class_id            = 0;
        std::byte                 reserved0           = {};
        std::uint8_t              runlog_index        = 0;
        std::array<std::byte, 12> reserved1           = {};
        std::array<std::byte, 40> class_specific_data = {};
    };

    struct BootSpecificData
    {
        ByteArray<10>        reserved0;
        ByteArray<2>         reserved1;
        std::uint32_t        code_request;   // Code slot requests (slots 0-31)
        ByteArray<2>         reserved2;
        std::array<char, 21> remote_terminal;
        ByteArray<1>         reserved3;
    };

    // **********************************************************
    // Response pods
    // **********************************************************

    enum class FieldbusResponseType : std::uint8_t
    {
        command_response        = 0b00,
        direct_publication      = 0b01,
        publication_get_request = 0b10,
    };

    struct ResponseFlags
    {
        std::uint8_t         sequence     : 4 = {};
        bool                 first_packet : 1 = false;
        bool                 last_packet  : 1 = false;
        FieldbusResponseType type         : 2 = {};
    };

    struct ResponsePacketHeaderPOD
    {
        std::uint16_t length;
        std::byte     flags;
        std::uint8_t  user;
    };

    struct ResponseHeader
    {
        std::byte     flags;
        std::uint8_t  n_rterm_chars;
    };

    // **********************************************************
    // Publication pods
    // **********************************************************

    struct PublicationFlags
    {
        bool          new_subscription_trigger : 1 = false;
        bool          set_trigger              : 1 = false;
        bool          multi_ppm                : 1 = false;
        unsigned char reserved                 : 2 = {};
        unsigned char sub_device_selector      : 3 = 0;
    };

    static_assert(sizeof(PublicationFlags) == 1, "Struct occupy more space than expected");

    struct PublicationHeaderPOD
    {
        std::uint8_t id        = {};
        std::byte    flags     = {};
        std::uint8_t user      = {};
        Timestamp    timestamp = {};
    };

    struct PublicationPacketPOD
    {
        PublicationHeaderPOD header = {};
        ByteStream           body   = {};
    };

    // **********************************************************
    // Command
    // **********************************************************

    struct CommandFlags
    {
        FieldbusCommandType type         : 4 = {};
        bool                first_packet : 1 = {};
        bool                last_packet  : 1 = {};
        std::byte           reserved     : 2 = {};
    };

    static_assert(sizeof(CommandFlags) == 1, "Struct occupy more space than expected");

    struct FieldbusCommandHeader
    {
        std::byte    flags{};
        std::uint8_t user{};
    };

    // **********************************************************
    // Time
    // **********************************************************

    struct FieldbusEvent
    {
        FgcEventType  type;           //!< Event type (FGC_EVT_XXXX)
        std::uint8_t  trigger_type;   //!< Event type of triggering event
        std::uint16_t payload;        //!< Event payload
        std::uint32_t delay_us;       //!< Event delay in microseconds
    };

    struct FieldbusTime
    {
        static constexpr auto fgc_event_slots = 4;
        using EventArray                      = std::array<FieldbusEvent, fgc_event_slots>;

        std::uint8_t  fgc_id               = {};   // [0x00] FGC ID: 0-FGC_MAX_DEVS_PER_GW
        std::uint8_t  fgc_char             = {};   // [0x01] FGC rterm character
        std::uint16_t ms_time              = {};   // [0x02] ms time: 0,20,40,...,960,980,0,20,...      on FIP
        std::uint32_t unix_time            = {};   // [0x04] UTC as seconds since start of 1970
        std::uint16_t namedb_crc           = {};   // [0x08] NameDB (Code 0) checksum
        std::uint16_t old_adv_code_version = {};   // [0x0A] Old advertised code version
        std::uint8_t  code_var_class_id    = {};   // [0x0C] Code variable class
        std::uint8_t  code_var_id          = {};   // [0x0D] Code variable ID
        std::uint16_t code_var_idx         = {};   // [0x0E] Code variable block index (32-byte blocks)

        std::uint8_t  adv_list_idx         = {};   // [0x10] Advertised code list index
        std::uint8_t  adv_list_len         = {};   // [0x11] Advertised code list length
        std::uint8_t  adv_code_class_id    = {};   // [0x12] Advertised code class ID
        std::uint8_t  adv_code_id          = {};   // [0x13] Advertised code ID
        std::uint16_t adv_code_crc         = {};   // [0x14] Advertised code CRC16
        std::uint16_t adv_code_len_blk     = {};   // [0x16] Advertised code length (32-byte blocks)

        std::uint16_t runlog_idx           = {};   // [0x18] Run log index
        std::uint8_t  flags                = {};   // [0x1A] Flags
        std::uint8_t  reserved1            = {};   // [0x1B] Reserved

        std::uint32_t adv_code_version     = {};   // [0x1C] Advertised code version

        EventArray    events               = {};   // [0x20] FGC event slots
                                                   // [0x40] End
    };

    // **********************************************************
    // Code
    // **********************************************************

    using CodeBlock = std::array<std::byte, 32>;
}

// EOF
