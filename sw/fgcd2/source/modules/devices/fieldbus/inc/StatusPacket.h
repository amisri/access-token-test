//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cpp_utils/Result.h>
#include <cpp_utils/bits.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include "Enums.h"
#include "PODs.h"

// **********************************************************

namespace fgcd::fieldbus
{
    class StatusPacket
    {
      public:
        StatusPacket()                               = default;
        StatusPacket(const StatusPacket&)            = default;
        StatusPacket(StatusPacket&&)                 = default;
        StatusPacket& operator=(const StatusPacket&) = default;
        StatusPacket& operator=(StatusPacket&&)      = default;
        virtual ~StatusPacket()                      = default;

        static utils::Result<StatusPacket, std::string> fromBytes(auto& frame)
        {
            auto status = frame.template read<StatusPacketPOD>();
            if (!status)
            {
                return status.error();
            }

            return StatusPacket(*status);
        }

        // **********************************************************
        // Getters
        // **********************************************************

        [[nodiscard]] int         getSequence() const;
        [[nodiscard]] bool        isTimeReceived() const;
        [[nodiscard]] bool        getCommandToggle() const;
        [[nodiscard]] bool        isPllLocked() const;
        [[nodiscard]] bool        isBaseClass() const;
        [[nodiscard]] FgcStatus   getStatus() const;
        [[nodiscard]] std::string getRemoteTerminal() const;

        template<typename POD>
        [[nodiscard]] auto getClassSpecificData() const
        {
            BigReader reader{m_pod.class_specific_data};
            return reader.read<POD>();
        }

        // **********************************************************
        // Setter
        // **********************************************************

        StatusPacket& setPllLocked();
        StatusPacket& setClass(std::uint8_t class_id);
        StatusPacket& setSequence(std::size_t sequence);
        StatusPacket& setCommandToggle(bool value);
        StatusPacket& setStatus(FgcStatus status);

      protected:
        StatusPacket(StatusPacketPOD pod);

      private:
        [[nodiscard]] virtual std::string doGetRemoteTerminal() const;

        StatusPacketPOD m_pod;
    };
}

// EOF
