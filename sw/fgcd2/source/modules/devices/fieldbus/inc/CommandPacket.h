//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <string_view>

#include <modules/devices/base/inc/Command.h>
#include "PODs.h"

// **********************************************************

namespace fgcd::fieldbus
{
    class CommandPacket
    {
      public:
        CommandPacket()                                = default;
        CommandPacket(const CommandPacket&)            = delete;
        CommandPacket(CommandPacket&&)                 = delete;
        CommandPacket& operator=(const CommandPacket&) = delete;
        CommandPacket& operator=(CommandPacket&&)      = delete;
        virtual ~CommandPacket()                       = default;

        std::size_t    setPayload(std::string_view payload);
        CommandPacket& setFirstPacket(bool first);
        CommandPacket& setLastPacket(bool last);
        CommandPacket& setUser(std::uint8_t user);
        CommandPacket& setType(FieldbusCommandType type);

        [[nodiscard]] bool                isFirstPacket() const;
        [[nodiscard]] bool                isLastPacket() const;
        [[nodiscard]] std::uint8_t        getUser() const;
        [[nodiscard]] FieldbusCommandType getType() const;

      protected:
        FieldbusCommandHeader m_header;

      private:
        virtual std::size_t doSetCommandBuffer(std::string_view payload) = 0;
    };
}

// EOF
