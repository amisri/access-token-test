//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#pragma once

#include <string_view>

namespace fgcd
{

    struct ProtocolConfig
    {
        const std::string_view name;
        const unsigned int     max_packet_length;
        const unsigned int     min_packet_length;
        const unsigned int     max_payload_length;
        const unsigned int     min_payload_length;
    };

    namespace configs
    {
        inline constexpr auto user_count = 32;

        constexpr ProtocolConfig fgc_ether = 
        {
            .name               = "FGC_ETHER",
            .max_packet_length  = 1514,
            .min_packet_length  = 64,
            .max_payload_length = 1496,
            .min_payload_length = 48
        };

        constexpr ProtocolConfig world_fip = 
        {
            .name               = "WOLRD_FIP",
            .max_payload_length = 120,
            .min_payload_length = 3
        };
    };

    inline std::ostream& operator<<(std::ostream& os, const ProtocolConfig& config)
    {
        return os << config.name;
    }
}


// EOF
