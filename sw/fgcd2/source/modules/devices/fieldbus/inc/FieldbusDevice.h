//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#pragma once

#include <memory>
#include <mutex>

#include <modules/base/component/inc/Component.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/programming/inc/types.h>
#include "CodeDistributor.h"
#include "CommandPacket.h"
#include "Enums.h"
#include "FsmCommand.h"
#include "FsmPublication.h"
#include "FsmResponse.h"
#include "FsmStatus.h"
#include "ResponsePacket.h"
#include "StatusPacket.h"

namespace fgcd
{
    class FieldbusDevice;
    using FieldbusDevicePtr = std::shared_ptr<FieldbusDevice>;

    class FieldbusDevice : public Device
    {
      public:
        FieldbusDevice(
            Component& parent, const DeviceData& data, fieldbus::CodeDistributor& code_manager,
            fieldbus::CommandPacket& command_packet
        );


        [[nodiscard]] int getDongleId() const;
        void              handle(const fieldbus::StatusPacket& status_packet);
        void              handle(const fieldbus::ResponsePacket& response_packet);
        void              tick();

      private:
        virtual void doSendCommand() = 0;

        void doExecuteCommand(CommandPtr command) override;
        void doReprogram(const ReprogramData& data) override;

        void setStatus(std::optional<FgcStatus> status);
        void handleCommandResponse(const fieldbus::ResponsePacket& packet);

        std::mutex m_mutex;
        // **********************************************************
        int        m_dongle_id;
        CommandPtr m_command;
        Response   m_response;
        // **********************************************************
        fieldbus::CodeDistributor& m_code_distributor;
        FsmResponse                m_fsm_response;
        FsmCommand                 m_fsm_command;
        FsmStatus                  m_fsm_status;
        FsmPublication             m_publication_handler;
    };
}

// EOF
