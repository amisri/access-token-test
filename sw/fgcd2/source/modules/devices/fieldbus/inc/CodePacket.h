//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>

#include "FgcCode.h"

namespace fgcd::fieldbus
{
    class CodePacket
    {
      public:
        virtual ~CodePacket() = default;
        CodePacket()          = default;

        std::size_t distributeCode(const FgcCode& code, std::uint16_t block_index);

      private:
        virtual std::size_t doDistributeCode(const FgcCode& code, std::uint16_t block_index) = 0;
    };
}

// EOF
