//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <array>
#include <memory>
#include <unordered_map>

#include <modules/base/component/inc/Component.h>
#include <modules/devices/programming/inc/types.h>
#include "CodePacket.h"
#include "FgcCode.h"
#include "TimePacket.h"

namespace fgcd::fieldbus
{
    enum class CodeType : std::uint8_t
    {
        namedb          = 0,
        main_program_51 = 8,
        main_program_63 = 24,
        unknown,
        count
    };

    class CodeDistributor;
    using CodeDistributorPtr = std::shared_ptr<CodeDistributor>;

    class CodeDistributor : public Component
    {
        using Slots = std::unordered_map<CodeType, int>;

        static const Slots    slots;
        static constexpr auto slots_number = 32;

      public:
        CodeDistributor(Component& parent);

        void publishCode(CodePacket& code_packet, TimePacket& time_packet);

        bool setCode(const ReprogramData& data);
        void vote(std::uint32_t request);

      private:
        void pickCode();
        void advertiseCode(TimePacket& time_packet);
        void distributeCode(TimePacket& time_packet, CodePacket& code_packet);

        std::array<FgcCode, slots_number>     m_codes_slots             = {};
        std::array<std::size_t, slots_number> m_votes                   = {};
        utils::CyclicInt<slots_number>        m_advertised_index        = {};
        std::size_t                           m_active_code_index       = 0;
        std::uint16_t                         m_active_code_block_index = 0;
    };
}

// EOF
