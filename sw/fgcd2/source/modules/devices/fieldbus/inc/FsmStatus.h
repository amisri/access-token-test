//! @file
//! @brief  Status FSM class used to process FGC Ether status packets.
//! @author Adam Solawa

#pragma once

#include <chrono>
#include <cstdint>
#include <memory>
#include <source_location>

#include <cpp_utils/flagSet.h>
#include <cpp_utils/fsm.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/stats/inc/StatsGroup.h>
#include "Enums.h"
#include "StatusPacket.h"

namespace fgcd
{
    class FsmStatus;
    using FsmStatusPtr = std::shared_ptr<FsmStatus>;

    enum class DeviceSignal
    {
        send_command,
        command_finished,
        vote,
        error,
        COUNT
    };
    using DeviceSignals = utils::FlagSet<DeviceSignal>;

    class FsmStatus : public Component
    {
        //! Enum defining FGC Ether device's states.
        enum class State
        {
            offline,
            inBoot,
            idle,
            queued,
            waitAck,
            waitExe,
            finish,
            noLock,
        };

        using TransRes                           = utils::FsmTransitionResult<State>;
        using StateMachine                       = utils::Fsm<State, FsmStatus>;
        static constexpr int  base_class_factor  = 10;
        static constexpr auto max_execution_time = std::chrono::seconds(120);

      public:
        FsmStatus(Component& parent);

        //! Processes the received status frame.
        //! The method resets status missed counter, updates current status packet and keeps the
        //! last one. Subsequently it updates device's state machine.
        //!
        //! @param frame a pointer to status frame to be handled.
        DeviceSignals process(const fieldbus::StatusPacket& packet, bool has_frame_to_send);
        DeviceSignals tick();

        std::optional<FgcStatus>     getStatus();
        std::optional<std::uint32_t> getVote();

      private:
        void setStatus(FgcStatus status, std::source_location location = std::source_location::current());

        // **********************************************************
        // FMS state function
        // **********************************************************

        void offlineState();
        void inBootState();
        void idleState();
        void queuedState();
        void waitAckState();
        void waitExeState();
        void finishState();
        void noLockState();

        // **********************************************************
        // FMS transition function
        // **********************************************************

        TransRes nonBaseClass();
        TransRes baseClass();
        TransRes commandHasFrame();
        TransRes toWaitAck();
        TransRes waitExeToXxx();
        TransRes pllUnlock();
        TransRes pllLocked();
        TransRes waitAckToXxx();

        StateMachine m_fsm;   //!< Class handling finite state machine logic.

        int  m_last_sequence                   = {};      //!< TODO
        bool m_last_toggle                     = {};      //!< TODO
        bool m_status_received                 = false;   //!< Indicates if status was already received in given cycle.
        bool m_ongoing_communication           = false;   //!< Bool indicating if communication is ongoing.
        bool m_last_command_toggle             = {};
        const fieldbus::StatusPacket* m_packet = nullptr;     //!< Stores current status packet.
        TimePoint                     m_command_start = {};   //!< Stores start of command execution by fgc device.
        DeviceSignals                 m_signal        = {};   //!< TODO
        std::optional<FgcStatus>      m_status        = {};   //!< Status of the currently handled command.
        std::optional<std::uint32_t>  m_vote;
        bool                          m_has_frame_to_send = false;
        bool                          m_device_alive      = false;

        struct : StatsGroup
        {
            using StatsGroup::StatsGroup;

            StatsField status_miss{*this, "status_miss"};
            StatsField seq_stats_miss{*this, "seq_stats_miss"};
            StatsField seq_toggle_bit_miss{*this, "seq_toggle_bit_miss"};

            StatsField ack_miss{*this, "ack_miss"};
            StatsField status_rec{*this, "status_rec"};

            StatsField offline_transition{*this, "offline_transition"};
            StatsField protocol_error{*this, "protocol_error"};
        } m_stats{m_stats_root, "count"};

        int m_device_id = 0;
    };
}

// EOF
