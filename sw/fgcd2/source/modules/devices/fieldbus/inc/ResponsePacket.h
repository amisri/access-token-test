//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cpp_utils/Result.h>
#include <cpp_utils/bits.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include "PODs.h"

namespace fgcd::fieldbus
{
    class ResponsePacket
    {
      public:
        ResponsePacket() = default;
        ResponsePacket(ResponseHeader header, ByteStreamView payload);

        static utils::Result<ResponsePacket, std::string> fromBytes(auto& frame)
        {
            auto header = frame.template read<ResponseHeader>();
            if (!header)
            {
                return header.error();
            }

            return ResponsePacket(*header, frame.unreadData());
        }

        [[nodiscard]] std::string          getRemoteTerminal() const;
        [[nodiscard]] ByteStreamView       getBody() const;
        [[nodiscard]] bool                 isFirstPacket() const;
        [[nodiscard]] bool                 isLastPacket() const;
        [[nodiscard]] FieldbusResponseType getType() const;
        [[nodiscard]] std::size_t          getSequence() const;

        ResponsePacket& setBody(ByteStream stream);
        ResponsePacket& setFirstPacket(bool first);
        ResponsePacket& setLastPacket(bool last);
        ResponsePacket& setType(FieldbusResponseType type);
        ResponsePacket& setSequence(std::size_t sequence);

      private:
        ResponseHeader m_header{};
        ByteStream     m_payload{};
    };
}

// EOF
