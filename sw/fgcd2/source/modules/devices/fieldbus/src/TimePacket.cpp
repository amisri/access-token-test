//! @file
//! @brief
//! @author Adam Solawa

#include <cstdint>

#include "../inc/PODs.h"
#include "../inc/TimePacket.h"

using namespace fgcd::fieldbus;

// **********************************************************

void TimePacket::setTime(fgcd::TimePoint time)
{
    using namespace std::chrono;

    auto duration = time.time_since_epoch();

    auto unix_time   = duration_cast<seconds>(duration);
    auto millisecond = duration_cast<milliseconds>(duration % seconds(1));

    // // REMARK: It would overflow at February 7, 2106 at 6:28:15 AM UTC
    m_time.unix_time = static_cast<std::uint32_t>(unix_time.count());
    m_time.ms_time   = static_cast<std::uint16_t>(millisecond.count());
}

// **********************************************************

void TimePacket::tick()
{
    ++m_time.runlog_idx;

    m_time.runlog_idx %= max_runlog_index;
}

// **********************************************************

void TimePacket::advertiseCode(const fieldbus::FgcCode& code, std::uint8_t index, std::uint8_t list_length)
{
    m_time.adv_list_idx = index;
    m_time.adv_list_len = list_length;

    m_time.adv_code_class_id = code.getClassId();
    m_time.adv_code_id       = code.getCodeId();
    m_time.adv_code_crc      = code.getCrc();
    m_time.adv_code_len_blk  = code.getBlocksLength();
    m_time.adv_code_version  = code.getVersion();
}

// **********************************************************

void TimePacket::distributCode(const FgcCode& code, std::uint16_t block_index)
{
    m_time.code_var_idx      = block_index;
    m_time.code_var_id       = code.getCodeId();
    m_time.code_var_class_id = code.getClassId();

}

// **********************************************************

FieldbusTime TimePacket::getTime()
{
    return m_time;
}

// EOF
