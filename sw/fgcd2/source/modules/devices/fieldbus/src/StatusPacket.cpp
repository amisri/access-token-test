//! @file
//! @brief
//! @author Adam Solawa

#include <bit>
#include <cstring>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include "../inc/PODs.h"
#include "../inc/StatusPacket.h"

using namespace fgcd::fieldbus;

// **********************************************************

int StatusPacket::getSequence() const
{
    return std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment).status;
}

// **********************************************************

bool StatusPacket::isTimeReceived() const
{
    return std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment).time_received;
}

// **********************************************************

bool StatusPacket::getCommandToggle() const
{
    return std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment).command_toggle;
}

// **********************************************************

bool StatusPacket::isPllLocked() const
{
    return std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment).pll_locked;
}

// **********************************************************

fgcd::FgcStatus StatusPacket::getStatus() const
{
    return m_pod.command_status;
}

// **********************************************************

bool StatusPacket::isBaseClass() const
{
    return m_pod.class_id % 10 == 0;
}

// **********************************************************

std::string StatusPacket::getRemoteTerminal() const
{
    return doGetRemoteTerminal();
}

// **********************************************************

StatusPacket& StatusPacket::setPllLocked()
{
    auto flags          = std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment);
    flags.pll_locked    = true;
    m_pod.acknowlegment = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

StatusPacket& StatusPacket::setClass(std::uint8_t class_id)
{
    m_pod.class_id = class_id;
    return *this;
}

// **********************************************************

StatusPacket& StatusPacket::setSequence(std::size_t sequence)
{
    auto flags          = std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment);
    flags.status        = sequence;
    m_pod.acknowlegment = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************


StatusPacket& StatusPacket::setCommandToggle(bool value)
{
    auto flags           = std::bit_cast<StatusAcknowlegment>(m_pod.acknowlegment);
    flags.command_toggle = value;
    m_pod.acknowlegment  = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************


StatusPacket& StatusPacket::setStatus(FgcStatus status)
{
    m_pod.command_status = status;
    return *this;
}

// **********************************************************

StatusPacket::StatusPacket(StatusPacketPOD pod)
    : m_pod(pod)
{
}

// **********************************************************

std::string StatusPacket::doGetRemoteTerminal() const
{
    if(isBaseClass())
    {
        BigReader reader(m_pod.class_specific_data);
        auto data = reader.read<BootSpecificData>();

        if(data.hasError())
        {
            return "Remote terminal error";
        }

        auto length = std::strlen(data->remote_terminal.data());
        return {data->remote_terminal.data(), length};
    }

    return "";
}


// EOF
