//! @file
//! @brief
//! @author Adam Solawa

#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>
#include <string_view>
#include <tuple>
#include <utility>

#include <cpp_utils/Result.h>
#include <cpp_utils/rangesUtils.h>
#include <cpp_utils/text.h>
#include <interfaces/IValueItem.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/common/inc/ValueLeaf.h>
#include <modules/common/inc/ValueParent.h>
#include "../inc/ProtocolConfig.h"
#include "../inc/ValueParser.h"

using namespace fgcd;
using namespace fgcd::value_parser;

// **********************************************************

utils::Result<IValueItemPtr, std::string> ValueParser::parse(ByteStreamView bytes, bool parse)
{
    if (!parse)
    {
        auto text = std::string(utils::asStringView(bytes));
        return {std::make_shared<ValueLeaf>(std::move(text))};
    }

    // **********************************************************
    // Parse first line
    // **********************************************************

    std::string_view label;
    std::tie(label, bytes) = asStringUntil(bytes, ':'_b);

    std::string_view type_string;
    std::tie(type_string, bytes) = asStringUntil(bytes, ':'_b);

    auto type = PropertyTypeStr::fromString(type_string);
    if (not type.has_value())
    {
        return utils::Failure{"Cannot parse first line type"};
    }

    // Check if it is binary payload
    if (not bytes.empty() && bytes.front() == binary_data_flag)
    {
        bytes = bytes.subspan(1);
        return deserializePayload(bytes, *type);
    }

    std::string_view value;
    std::tie(value, bytes) = asStringUntil(bytes, '\n'_b);

    auto node = parsePayload(value, *type);
    if (node.hasError())
    {
        return utils::Failure{node.error()};
    }
    if (label == leaf_label)
    {
        return *node;
    }

    IValueItemPtr parent_value = std::make_shared<ValueParent>();
    parent_value->add(label, *node);

    // **********************************************************
    // Parse following lines
    // **********************************************************

    for (auto line : utils::asStringView(bytes) | utils::split_string('\n'))
    {
        std::tie(label, line)       = utils::splitFirst(line, ':');
        std::tie(type_string, line) = utils::splitFirst(line, ':');

        auto type = PropertyTypeStr::fromString(type_string);
        if (not type.has_value())
        {
            return utils::Failure{"Cannot parse record type"};
        }

        node = parsePayload(line, *type);
        if (node.hasError())
        {
            return utils::Failure{node.error()};
        }

        parent_value->add(label, *node);
    }

    return parent_value;
}

// **********************************************************

utils::Result<IValueVector, std::string> ValueParser::parsePPM(ByteStreamView bytes, bool parse)
{
    if (!parse)
    {
        auto         text = utils::asStringView(bytes);
        IValueVector value{};
        value.push_back(std::make_shared<ValueLeaf>(std::string(text)));
        return value;
    }


    IValueVector values;
    values.reserve(configs::user_count);

    for (auto line : utils::asStringView(bytes) | utils::split_string('\n'))
    {
        auto [label, rest1]       = utils::splitFirst(line, ':');
        auto [type_string, rest2] = utils::splitFirst(rest1, ':');

        auto type = PropertyTypeStr::fromString(type_string);
        if (not type.has_value())
        {
            return utils::Failure{"Cannot parse record type"};
        }

        auto result = parsePayload(rest2, *type);
        if (result.hasError())
        {
            return utils::Failure{result.error()};
        }

        values.push_back(*result);
    }

    return values;
}

// **********************************************************

utils::Result<IValueItemPtr, std::string> ValueParser::parsePayload(std::string_view stream, PropertyType type)
{
    switch (type)
    {
        case PropertyType::float_type:
            return parsePayloadTyped<float>(stream);

        case PropertyType::int32s:
            return parsePayloadTyped<std::int32_t>(stream);

        case PropertyType::int16s:
            return parsePayloadTyped<std::int16_t>(stream);

        case PropertyType::int8s:
            return parsePayloadTyped<std::int8_t>(stream);

        case PropertyType::int32u:
            return parsePayloadTyped<std::uint32_t>(stream);

        case PropertyType::int16u:
            return parsePayloadTyped<std::uint16_t>(stream);

        case PropertyType::int8u:
            return parsePayloadTyped<std::uint8_t>(stream);

        case PropertyType::string:
            return parsePayloadTyped<std::string>(stream);

        case PropertyType::point:
            return parsePayloadTyped<Point>(stream);

        case PropertyType::char_type:
            return {std::make_shared<ValueLeaf>(std::string(stream))};
    }

    return utils::Failure{"Unexpected type"};
}

// **********************************************************

utils::Result<IValueItemPtr, std::string> ValueParser::deserializePayload(ByteStreamView bytes, PropertyType type)
{
    BigReader reader(bytes);

    auto byte_size = reader.read<std::uint32_t>();
    if (byte_size.hasError())
    {
        return utils::Failure{byte_size.error()};
    }

    if (auto left_size = reader.unreadData().size(); *byte_size != left_size)
    {
        return utils::Failure{"Get wrong binary length"};
    }

    switch (type)
    {
        case PropertyType::float_type:
            return deserializePayloadTyped<float>(reader);

        case PropertyType::point:
            return deserializePayloadTyped<Point>(reader);

        case PropertyType::int32s:
            return deserializePayloadTyped<std::int32_t>(reader);

        case PropertyType::int16s:
            return deserializePayloadTyped<std::int16_t>(reader);

        case PropertyType::int8s:
            return deserializePayloadTyped<std::int8_t>(reader);

        case PropertyType::int32u:
            return deserializePayloadTyped<std::uint32_t>(reader);

        case PropertyType::int16u:
            return deserializePayloadTyped<std::uint16_t>(reader);

        case PropertyType::int8u:
            return deserializePayloadTyped<std::uint8_t>(reader);

        default:
            return utils::Failure{"Unknown type"};
    }
}

// **********************************************************

std::pair<std::string_view, ByteStreamView> ValueParser::asStringUntil(ByteStreamView bytes, std::byte sentinel)
{
    auto result = std::ranges::find(bytes, sentinel);
    auto index  = std::distance(bytes.begin(), result);

    auto string = utils::asStringView(bytes.first(index));
    auto rest   = bytes.subspan(index);

    if (static_cast<std::size_t>(index) != bytes.size())
    {
        rest = bytes.subspan(index + 1);
    }

    return {string, rest};
}

// EOF
