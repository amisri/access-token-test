//! @file
//! @brief  
//! @author Adam Solawa

#include "../inc/CodePacket.h"
#include "../inc/FgcCode.h"

using namespace fgcd::fieldbus;

// **********************************************************

std::size_t CodePacket::distributeCode(const FgcCode& code, std::uint16_t block_index)
{
    return doDistributeCode(code, block_index);
}

// EOF
