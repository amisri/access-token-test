//! @file
//! @brief  
//! @author Adam Solawa

#include <cstdint>

#include <cpp_utils/Result.h>
#include <cpp_utils/misc.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include "../inc/FgcCode.h"

using namespace fgcd::fieldbus;

// **********************************************************

utils::Result<FgcCode, std::string> FgcCode::fromBytes(fgcd::ByteStreamView bytes)
{
    if (bytes.size() < header_size)
    {
        return utils::Failure("Too short bytes");
    }
    auto      header_bytes = bytes.last(header_size);

    BigReader reader(header_bytes);
    auto      header = reader.read<FgcCodeInfo>();
    if (header.hasError())
    {
        return utils::Failure{header.error()};
    }

    return FgcCode(*header, {bytes.begin(), bytes.end()});
}

// **********************************************************

bool FgcCode::valid() const
{
    return m_header.code_id != FgcCodeInfo::empty_code_id;
}

// **********************************************************

fgcd::ByteStreamView FgcCode::getCodeBlocks(std::size_t offset, std::size_t count) const
{
    auto view = ByteStreamView(m_body);

    auto offset_bytes = offset * block_size;
    auto count_bytes  = count == npos ? view.size() : count * block_size;

    if (offset_bytes >= view.size())
    {
        return {};
    }

    if (offset_bytes + count_bytes >= view.size())
    {
        count_bytes = view.size() - offset_bytes;
    }

    return view.subspan(offset_bytes, count_bytes);
}

// **********************************************************

std::uint16_t FgcCode::getBlocksLength() const
{
    return m_blocks_length;
}

// **********************************************************

std::uint8_t FgcCode::getClassId() const
{
    return m_header.class_id;
}

// **********************************************************

std::uint8_t FgcCode::getCodeId() const
{
    return m_header.code_id;
}

// **********************************************************

std::uint16_t FgcCode::getCrc() const
{
    return m_header.crc;
}

// **********************************************************

std::uint32_t FgcCode::getVersion() const
{
    return m_header.version;
}

// **********************************************************

FgcCode::FgcCode(FgcCodeInfo header, ByteStream body)
    : m_header(header),
      m_body(std::move(body)),
      m_blocks_length(utils::ceil(m_body.size(), block_size))
{
}

// EOF
