//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#include <fmt/format.h>
#include <mutex>
#include <optional>
#include <string>
#include <utility>

#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/base/inc/Enums.h>
#include <modules/devices/base/inc/Response.h>
#include <modules/devices/programming/inc/types.h>
#include "../inc/CodeDistributor.h"
#include "../inc/CommandPacket.h"
#include "../inc/Enums.h"
#include "../inc/FieldbusDevice.h"
#include "../inc/FsmPublication.h"
#include "../inc/PODs.h"
#include "../inc/ResponsePacket.h"
#include "../inc/StatusPacket.h"

using namespace fgcd;

// **********************************************************

FieldbusDevice::FieldbusDevice(
    Component& parent, const DeviceData& data, fieldbus::CodeDistributor& code_manager,
    fieldbus::CommandPacket& command_packet
)
    : Device(parent, data),
      m_dongle_id(data.class_specific.at("dongle_id")),
      m_code_distributor(code_manager),
      m_fsm_response(*this),
      m_fsm_command(*this, command_packet),
      m_fsm_status(*this),
      m_publication_handler(*this, m_dongle_id)
{
}

// **********************************************************

int FieldbusDevice::getDongleId() const
{
    return m_dongle_id;
}

// **********************************************************

void FieldbusDevice::doExecuteCommand(CommandPtr command)
{
    std::scoped_lock lock{m_mutex};

    logInfo("Set new command");
    m_command  = std::move(command);
    m_response = std::monostate{};
    m_fsm_command.setCommand(*m_command);
}

// **********************************************************

void FieldbusDevice::doReprogram(const ReprogramData& data)
{
    std::scoped_lock lock{m_mutex};

    m_code_distributor.setCode(data);
}

// **********************************************************

void FieldbusDevice::tick()
{
    std::scoped_lock lock{m_mutex};

    auto events = m_fsm_status.tick();

    if (events.has(DeviceSignal::command_finished))
    {
        logDebug("Tick promise set");

        setStatus(m_fsm_status.getStatus());
        if (not Command::setResponse(std::move(m_command), m_response))
        {
            logError("Cannot push response");
        }
        finishCommand();
    }
}

// **********************************************************

void FieldbusDevice::handle(const fieldbus::StatusPacket& status_packet)
{
    std::scoped_lock lock{m_mutex};

    auto has_frame = m_fsm_command.hasFrameToSend();
    auto events    = m_fsm_status.process(status_packet, has_frame);

    if (events.has(DeviceSignal::error))
    {
        return;
    }

    pushRterm(status_packet.getRemoteTerminal());

    if (events.none())
    {
        return;
    }

    if (events.has(DeviceSignal::send_command))
    {
        // Prepare reception SM for new responses
        m_fsm_response.nextCycle();

        // Populate command frame with data
        m_fsm_command.publish();

        // Send frame
        doSendCommand();
    }

    if (events.has(DeviceSignal::vote))
    {
        if (auto vote = m_fsm_status.getVote())
        {
            m_code_distributor.vote(*vote);
        }
        else
        {
            logError("Logical error");
        }
    }

    if (events.has(DeviceSignal::command_finished))
    {
        setStatus(m_fsm_status.getStatus());
        if (not Command::setResponse(std::move(m_command), m_response))
        {
            logError("Cannot push response");
        }
        finishCommand();
    }
}

// **********************************************************

void FieldbusDevice::handle(const fieldbus::ResponsePacket& response_packet)
{
    std::scoped_lock lock{m_mutex};

    using namespace fieldbus;

    switch (response_packet.getType())
    {
        case FieldbusResponseType::command_response:
            handleCommandResponse(response_packet);
            break;

        case FieldbusResponseType::publication_get_request:
            logInfo("Received direct publication response");
            break;

        case FieldbusResponseType::direct_publication:
            m_publication_handler.processPublication(response_packet);
            break;
    }
}

// **********************************************************

void FieldbusDevice::setStatus(std::optional<FgcStatus> status)
{
    if (status)
    {
        auto error_code = static_cast<int>(*status);
        if (error_code > 1)
        {
            m_response = ErrorResponse{.error_code = error_code, .message = "Error"};
        }
        else if(m_response.index() == 0)
        {
            logInfo("Set empty response");
            m_response = CommandResponse{};
        }
    }
    else
    {
        logError("Logical error");
    }
}

// **********************************************************

void FieldbusDevice::handleCommandResponse(const fieldbus::ResponsePacket& packet)
{
    switch (m_command->type())
    {
        case CommandType::GET:
        case CommandType::SET:
            if (auto response = m_fsm_response.process(packet, *m_command);   //
                response.has_value())
            {
                logDebug("Set response content");
                m_response = *response;
            }
            break;

        case CommandType::SUB:
            setStatus(m_publication_handler.processSub(packet, m_command));
            break;

        case CommandType::UNSUB:
            setStatus(m_publication_handler.processUnsub(packet));
            break;

        default:
            logWarning("Unexpected command type");
    }
}

// EOF
