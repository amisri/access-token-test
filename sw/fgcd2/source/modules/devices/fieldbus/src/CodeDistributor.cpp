//! @file
//! @brief
//! @author Adam Solawa

#include <bitset>
#include <utility>

#include <cpp_utils/misc.h>
#include <cpp_utils/text.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/CodeDistributor.h"
#include "../inc/CodePacket.h"
#include "../inc/FgcCode.h"
#include "../inc/TimePacket.h"

using namespace fgcd::fieldbus;

// **********************************************************

const CodeDistributor::Slots CodeDistributor::slots = {
    {CodeType::namedb, 0},
    {CodeType::main_program_63, 1},
    {CodeType::main_program_51, 2},
};

// **********************************************************

CodeDistributor::CodeDistributor(Component& parent)
    : Component(parent, "CodeDistributor")
{
}

// **********************************************************

void CodeDistributor::publishCode(CodePacket& code_packet, TimePacket& time_packet)
{
    advertiseCode(time_packet);
    pickCode();
    distributeCode(time_packet, code_packet);
    m_votes                   = {};
}

// **********************************************************

void CodeDistributor::distributeCode(TimePacket& time_packet, CodePacket& code_packet)
{
    auto&& code = m_codes_slots[m_active_code_index];

    time_packet.distributCode(code, m_active_code_block_index);
    auto copied_blocks = code_packet.distributeCode(code, m_active_code_block_index);

    m_active_code_block_index += copied_blocks;
}

// **********************************************************


void CodeDistributor::advertiseCode(TimePacket& time_packet)
{
    time_packet.advertiseCode(m_codes_slots[m_advertised_index], m_advertised_index, slots_number);

    ++m_advertised_index;
}

// **********************************************************

bool CodeDistributor::setCode(const ReprogramData& data)
{
    auto [lhs, rhs] = utils::splitFirst(data.endpoint.subsystem_id, ':');

    auto class_id = utils::parseNumber<int>(lhs);
    auto type_id  = utils::parseEnum<CodeType>(rhs);
    if (!type_id.has_value())
    {
        logWarning("Cannot parse code id: {}", rhs);
        return false;
    }

    if(!slots.contains(*type_id))
    {
        logError("Code distributor doent support code type: {}", *type_id);
        return false;
    }
    auto slot           = slots.at(*type_id);
    auto code = FgcCode::fromBytes(data.binary);
    if(code.hasError())
    {
        logError("Cannot set code: {}", code.error());
        return false;
    }

    m_codes_slots[slot] = *code;
    logInfo("Setting code for slot number {}", slot);
    return true;
}

// **********************************************************

void CodeDistributor::vote(std::uint32_t request)
{
    auto bits = std::bitset<slots_number>(request);

    for (auto i = 0U; i < slots_number; ++i)
    {
        if (bits.test(i))
        {
            ++m_votes[i];   // NOLINT
        }
    }
}

// **********************************************************

void CodeDistributor::pickCode()
{
    auto&& active_code = m_codes_slots[m_active_code_index];

    if (active_code.valid())
    {
        if (bool code_transmited = active_code.getBlocksLength() <= m_active_code_block_index;   //
            !code_transmited)
        {
            return;
        }
    }

    auto* result = std::ranges::max_element(m_votes);
    auto  index  = std::distance(m_votes.begin(), result);

    if (!m_codes_slots[index].valid())
    {
        index = 0;
    }

    logTrace("New code - {} {}", index, fmt::join(m_votes, "-"));
    m_active_code_index = index;
    m_active_code_block_index = 0;
}

// EOF
