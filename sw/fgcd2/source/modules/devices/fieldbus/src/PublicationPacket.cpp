//! @file
//! @brief  
//! @author Adam Solawa

#include <optional>
#include <utility>

#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Timestamp.h>
#include "../inc/PublicationPacket.h"

using namespace fgcd::fieldbus;

// **********************************************************

utils::Result<PublicationPacket, std::string> PublicationPacket::fromBytes(BigReader& frame)
{
    auto result = frame.template read<PublicationPacketPOD>();
    if (result.hasError())
    {
        return utils::Failure{result.error()};
    }

    return PublicationPacket(*result);
}

// **********************************************************

PublicationPacket::PublicationPacket(PublicationPacketPOD pod)
    : m_pod(std::move(pod))
{
}

// **********************************************************

bool PublicationPacket::isNewSubscription() const
{
    return std::bit_cast<PublicationFlags>(m_pod.header.flags).new_subscription_trigger;
}

// **********************************************************

bool PublicationPacket::isSetTrigger() const
{
    return std::bit_cast<PublicationFlags>(m_pod.header.flags).set_trigger;
}

// **********************************************************

std::optional<int> PublicationPacket::getSubDevice() const
{
    const auto flags = std::bit_cast<PublicationFlags>(m_pod.header.flags);

    if (!flags.multi_ppm)
    {
        return std::nullopt;
    }

    return flags.sub_device_selector;
}

// **********************************************************

std::uint8_t PublicationPacket::getId() const
{
    return m_pod.header.id;
}

// **********************************************************


std::uint8_t PublicationPacket::getUser() const
{
    return m_pod.header.user;
}

// **********************************************************

fgcd::Timestamp PublicationPacket::getTimestamp() const
{
    return m_pod.header.timestamp;
}

// EOF
