//! @file
//! @brief  
//! @author Adam Solawa

#include <atomic>

#include <modules/base/component/inc/Component.h>
#include <modules/base/thread/inc/threads.h>
#include "../inc/StatusServer.h"

using namespace fgcd;

// **********************************************************

StatusServer::StatusServer(Component& parent)
    : Component(parent, "StatusServer")
{
    m_callbacks.reserve(10);

    m_thread.start(
        threads::status_server,
        [this](std::atomic_bool& keep_running)
        {
            worker(keep_running);
        }
    );
}

// EOF
