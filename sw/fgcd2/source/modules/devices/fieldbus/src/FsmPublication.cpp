//! @file
//! @brief
//! @author Adam Solawa

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <fmt/format.h>
#include <memory>
#include <optional>
#include <string>
#include <utility>

#include <cpp_utils/fsm.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Response.h>
#include <modules/devices/base/inc/Subscription.h>
#include "../inc/Command.h"
#include "../inc/Enums.h"
#include "../inc/FsmPublication.h"
#include "../inc/PublicationPacket.h"
#include "../inc/ResponsePacket.h"
#include "../inc/ValueParser.h"

using namespace fgcd;
using namespace utils;

// **********************************************************

FsmPublication::FsmPublication(Component& parent, int device_id)
    : Component(parent, "FsmPublication{}", device_id),
      m_fsm(*this, State::start)
{
    using enum State;

    // clang-format off
    m_fsm.addState(start, &FsmPublication::startState, {&FsmPublication::checkFirstPacket                                });
    m_fsm.addState(first, &FsmPublication::firstState, {&FsmPublication::checkError, &FsmPublication::checkLastPacket    });
    m_fsm.addState(next,  &FsmPublication::nextState,  {&FsmPublication::checkError, &FsmPublication::checkLastPacket    });
    m_fsm.addState(end,   &FsmPublication::endState,   {&FsmPublication::reset                                           });
    m_fsm.addState(error, &FsmPublication::errorState, {&FsmPublication::reset                                           });
    // clang-format on

    // TODO
    m_buffer.reserve(1024);
}

// **********************************************************

void FsmPublication::processPublication(const fieldbus::ResponsePacket& packet)
{
    logInfo("Publication received");

    m_response_packet = &packet;
    m_fsm.update();
    ++m_next_sequence;
}

// **********************************************************

fgcd::FgcStatus FsmPublication::processSub(const fieldbus::ResponsePacket& packet, const CommandPtr& command)
{
    logInfo("Process sub");
    auto body = packet.getBody();
    if (body.size() != 2)
    {
        logError("Invalid body");
        return FgcStatus::proto_error;
    }

    auto subscription_id = std::to_integer<std::uint8_t>(body.front());

    auto selector = getFieldbusSelector(command->selector());
    if (not selector.has_value())
    {
        logError("Wrong selector: {}", selector);
        return FgcStatus::proto_error;
    }

    auto publication_command = std::make_unique<Command>(*command.get());
    m_subscriptions.emplace(subscription_id, std::move(publication_command));
    return FgcStatus::ok_no_rsp;
}

// **********************************************************

fgcd::FgcStatus FsmPublication::processUnsub(const fieldbus::ResponsePacket& packet)
{
    auto body = packet.getBody();
    if (body.size() != 2)
    {
        logError("Invalid body");
        return FgcStatus::proto_error;
    }

    auto subscription_id = std::to_integer<std::uint8_t>(body.front());

    m_subscriptions.erase(subscription_id);
    return FgcStatus::ok_no_rsp;
}

// **********************************************************

void FsmPublication::startState()
{
    logTrace("Start state");
}

// **********************************************************

void FsmPublication::firstState()
{
    logTrace("First state");

    m_error         = false;
    m_next_sequence = m_response_packet->getSequence();

    auto reader = BigReader(m_response_packet->getBody());

    auto packet = fieldbus::PublicationPacket::fromBytes(reader);
    if (not packet)
    {
        logError("Cannot deserialize publication packet: {}", packet.error());
        m_error = true;
        return;
    }
    m_publication_packet = *packet;

    m_current_subscription_id = m_publication_packet.getId();
    if (not m_subscriptions.contains(m_current_subscription_id))
    {
        logError("Published data received for inactive publication ID");
        m_error = true;
        return;
    }

    auto&& command  = m_subscriptions[m_current_subscription_id];
    auto   selector = getFieldbusSelector(command->selector());
    if (not selector.has_value())
    {
        logError("Wrong selector: {}", selector);
        m_error = true;
        return;
    }

    if (selector != m_publication_packet.getUser())
    {
        if (not command->ppm())
        {
            logDebug("Received publication for different user");
            m_error = true;
            return;
        }
    }

    auto buffer = reader.unreadData();
    m_buffer.assign(buffer.begin(), buffer.end());
}

// **********************************************************

void FsmPublication::nextState()
{
    logTrace("Next state");

    // TODO
    auto buffer = m_response_packet->getBody();
    m_buffer.insert(m_buffer.end(), buffer.begin(), buffer.end());
}

// **********************************************************

void FsmPublication::endState()
{
    logTrace("End state");

    // Pop termination sign
    m_buffer.pop_back();

    auto&& command     = m_subscriptions[m_current_subscription_id];
    auto   publication = std::make_unique<Command>(*command.get());

    auto result = ValueParser::parse(m_buffer, true);
    if (result.hasError())
    {
        logError("Parsing error: {}", result.error());
        m_error = true;
    }
    else
    {
        logDebug("Set response on publication command");

        auto success = Command::setResponse(                                                             //
            std::move(publication),                                                                      //
            PublicationResponse{.timestamp = m_publication_packet.getTimestamp(), .values = {*result}}   //
        );

        if (not success)
        {
            logError("Cannot push response");
        }
    }
}

// **********************************************************

void FsmPublication::errorState()
{
    logError("Error state");
}

// **********************************************************

FsmPublication::TransRes FsmPublication::checkFirstPacket()
{
    if (!m_response_packet->isFirstPacket())
    {
        return {};
    }

    return {State::first, utils::FsmCascade};
}

// **********************************************************

FsmPublication::TransRes FsmPublication::checkLastPacket()
{
    if (!m_response_packet->isLastPacket())
    {
        return {State::next};
    }

    if (m_buffer.empty())
    {
        logError("Empty response body");
        return {State::error, utils::FsmCascade};
    }

    if (m_buffer.back() != ';'_b)
    {
        logError("Wrong response termination character");
        return {State::error, utils::FsmCascade};
    }

    return {State::end, utils::FsmCascade};
}

// **********************************************************

FsmPublication::TransRes FsmPublication::reset()
{
    logDebug("Reset");
    return {State::start};
}

// **********************************************************

FsmPublication::TransRes FsmPublication::checkError()
{
    if (m_error)
    {
        return {State::error, utils::FsmCascade};
    }

    if (m_next_sequence != m_response_packet->getSequence())
    {
        logError("Incorrect sequence number: got {} expected {}", m_response_packet->getSequence(), +m_next_sequence);
        return {State::error, utils::FsmCascade};
    }

    return {};
}

// EOF
