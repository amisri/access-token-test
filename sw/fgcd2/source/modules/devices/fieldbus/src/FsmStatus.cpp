//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#include <cstdint>
#include <optional>
#include <set>

#include <cpp_utils/fsm.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include "../inc/Enums.h"
#include "../inc/FsmStatus.h"
#include "../inc/PODs.h"
#include "../inc/StatusPacket.h"

using namespace fgcd;

// ***************************************************

FsmStatus::FsmStatus(Component& parent)
    : Component(parent, "FsmStatus::{}", parent.name()),
      m_fsm(*this, State::offline)
{
    // CAUTION: The order of transition method matters
    using enum State;
    // clang-format off
    m_fsm.addState(offline, &FsmStatus::offlineState, { &FsmStatus::pllUnlock, &FsmStatus::baseClass,     &FsmStatus::nonBaseClass    });
    m_fsm.addState(inBoot,  &FsmStatus::inBootState,  { &FsmStatus::pllUnlock, &FsmStatus::nonBaseClass                               });
    m_fsm.addState(idle,    &FsmStatus::idleState,    { &FsmStatus::pllUnlock, &FsmStatus::baseClass,     &FsmStatus::commandHasFrame });
    m_fsm.addState(queued,  &FsmStatus::queuedState,  { &FsmStatus::pllUnlock, &FsmStatus::toWaitAck                                  });
    m_fsm.addState(waitAck, &FsmStatus::waitAckState, { &FsmStatus::pllUnlock, &FsmStatus::waitAckToXxx                               });
    m_fsm.addState(waitExe, &FsmStatus::waitExeState, { &FsmStatus::waitExeToXxx                                                      });
    m_fsm.addState(noLock,  &FsmStatus::noLockState,  { &FsmStatus::pllLocked                                                         });
    // clang-format on
}

// ***************************************************

DeviceSignals FsmStatus::tick()
{
    m_signal.clear();

    if (not m_status_received && m_fsm.getState() != State::offline)
    {
        logWarning("Status missed");

        m_stats.status_miss++;
        m_stats.seq_stats_miss++;
        m_stats.seq_toggle_bit_miss++;

        if (m_stats.seq_stats_miss == 2)
        {
            m_stats.offline_transition++;

            m_fsm.reset();
            logInfo("FGC Offline");

            if (m_ongoing_communication)
            {
                m_ongoing_communication = false;
                setStatus(FgcStatus::dev_not_ready);
            }
        }
    }

    m_status_received = false;

    return m_signal;
}

// ***************************************************

DeviceSignals FsmStatus::process(const fieldbus::StatusPacket& packet, bool has_frame_to_send)
{
    logTrace("Processing status packet");

    m_stats.status_rec++;

    if (m_status_received)
    {
        logWarning("FGC Multiple statuses received within the same cycle");
    }
    m_status_received = true;


    m_stats.seq_stats_miss = 0;

    // **********************************
    // Store parameters for state methods
    // **********************************
    if (m_packet != nullptr)
    {
        if (packet.getSequence() == m_last_sequence)
        {
            logWarning("Received the same sequence");
            return DeviceSignal::error;
        }
    }
    m_packet            = &packet;
    m_has_frame_to_send = has_frame_to_send;

    // tick method
    m_status = std::nullopt;
    m_vote   = std::nullopt;
    m_signal.clear();
    m_fsm.update();

    m_last_sequence = m_packet->getSequence();
    m_last_toggle   = m_packet->getCommandToggle();

    return m_signal;
}

// **********************************************************

std::optional<FgcStatus> FsmStatus::getStatus()
{
    return m_status;
}

// **********************************************************

std::optional<std::uint32_t> FsmStatus::getVote()
{
    return m_vote;
}

// **********************************************************

void FsmStatus::setStatus(FgcStatus status, std::source_location location)
{
    m_logger.log<LogType::debug>(
        location.file_name(), location.line(), m_name, "Setting status: {}", FgcStatusStr::toString(status)
    );
    m_status                = status;
    m_ongoing_communication = false;
    m_signal                += DeviceSignal::command_finished;
}

// ***************************************************

void FsmStatus::offlineState()
{
    if (m_device_alive)
    {
        logInfo("FGC offline");
        m_device_alive = false;
    }
}

// ***************************************************

void FsmStatus::inBootState()
{
    if (m_device_alive)
    {
        logInfo("FGC Boot");
        m_device_alive = false;
    }

    auto data = m_packet->getClassSpecificData<fieldbus::BootSpecificData>();
    if (data.hasValue())
    {
        m_vote   = data->code_request;
        m_signal += DeviceSignal::vote;
    }
    else
    {
        logError("Cannot decode class specific data {}", data.error());
    }
}

// ***************************************************

void FsmStatus::idleState()
{
    if (!m_device_alive)
    {
        logInfo("FGC Idle");
        m_device_alive = true;
    }

    if (m_ongoing_communication)
    {
        m_stats.protocol_error++;
        setStatus(FgcStatus::proto_error);
    }
}

// ***************************************************

void FsmStatus::queuedState()
{
    logDebug("Sending command");

    // Reset toggle missed counter
    m_stats.seq_toggle_bit_miss = 0;
    m_ongoing_communication = true;

    m_signal += DeviceSignal::send_command;
}

// ***************************************************

void FsmStatus::waitAckState()
{
    // If command acknowledgment bit was missed then increase counter
    if (auto toggle = m_packet->getCommandToggle();   //
        m_last_toggle == toggle)
    {
        // Increase toggle missed counter
        m_stats.seq_toggle_bit_miss++;
        m_stats.ack_miss++;

        // If there was no acknowledge bit change two times then this is an warning
        if (m_stats.seq_toggle_bit_miss >= 2)
        {
            logError("Protocol error: 2 toggles missed");
            m_stats.protocol_error++;
            setStatus(FgcStatus::proto_error);
        }

        return;
    }

    // Otherwise reset the counter
    m_stats.seq_toggle_bit_miss = 0;

    // If there are more messages to send FGC state should be RECEIVING - if not it is an error
    if (m_has_frame_to_send && m_packet->getStatus() != FgcStatus::receiving)
    {
        logError("Wrong FGC state while sending command messages");

        if (std::set invalid_status = {FgcStatus::ok_rsp, FgcStatus::ok_no_rsp, FgcStatus::executing};
            invalid_status.contains(m_packet->getStatus()))
        {
            logError(
                "Invalid command status {} from FGC while sending command",
                FgcStatusStr::toString(m_packet->getStatus())
            );
            setStatus(FgcStatus::proto_error);
        }
        else
        {
            setStatus(m_packet->getStatus());
        }

        return;
    }

    // Store the start of command's execution
    m_command_start = m_clock.now();
}

// ***************************************************

void FsmStatus::waitExeState()
{
    switch (m_packet->getStatus())
    {
        case FgcStatus::executing:
            if (auto elapsed_time = m_clock.now() - m_command_start;   //
                elapsed_time >= max_execution_time)
            {
                logError("FGC timeout in 'EXECUTING' {}", elapsed_time);
                setStatus(FgcStatus::proto_error);
            }
            break;

        case FgcStatus::receiving:
            logError("FGC state is 'RECEIVING' which is not expected state after entire command was sent");
            setStatus(FgcStatus::proto_error);
            break;

        case FgcStatus::ok_rsp:
        case FgcStatus::ok_no_rsp:
            logDebug("Command status set");
            setStatus(m_packet->getStatus());
            break;

        default:
            logWarning("FGC has returned error: {}", FgcStatusStr::toString(m_packet->getStatus()));
            setStatus(m_packet->getStatus());
    }
}

// ***************************************************

void FsmStatus::finishState()
{
    m_ongoing_communication = false;
}

// ***************************************************

void FsmStatus::noLockState()
{
    if (m_device_alive)
    {
        logWarning("No lock state");
        m_device_alive = false;
    }

    if (m_ongoing_communication)
    {
        m_ongoing_communication = false;

        logDebug("Setting response status as FGC_PLL_NOT_LOCKED");
        setStatus(FgcStatus::pll_not_locked);
    }
}

// ***************************************************

FsmStatus::TransRes FsmStatus::nonBaseClass()
{
    if (!m_packet->isBaseClass())
    {
        return {State::idle, utils::FsmCascade};
    }

    return {};
}

// ***************************************************

FsmStatus::TransRes FsmStatus::baseClass()
{
    if (m_packet->isBaseClass())
    {
        return {State::inBoot};
    }

    return {};
}

// ***************************************************

FsmStatus::TransRes FsmStatus::commandHasFrame()
{
    if (m_has_frame_to_send)
    {
        logDebug("Transition to queued state");
        return {State::queued, utils::FsmCascade};
    }

    return {};
}

// ***************************************************

FsmStatus::TransRes FsmStatus::toWaitAck()
{
    logDebug("Transition to waitAck state");
    return {State::waitAck};
}

// ***************************************************

FsmStatus::TransRes FsmStatus::waitExeToXxx()
{
    if (m_status.has_value())
    {
        return {State::idle};
    }

    return {};
}

// ***************************************************

FsmStatus::TransRes FsmStatus::pllUnlock()
{
    if (!m_packet->isPllLocked())
    {
        return {State::noLock, utils::FsmCascade};
    }

    return {};
}

// ***************************************************

FsmStatus::TransRes FsmStatus::pllLocked()
{
    if (m_packet->isPllLocked())
    {
        return {State::idle, utils::FsmCascade};
    }

    return {};
}

// **********************************************************

FsmStatus::TransRes FsmStatus::waitAckToXxx()
{
    if (m_stats.seq_toggle_bit_miss >= 2)
    {
        return {State::idle};
    }

    if (m_stats.seq_toggle_bit_miss != 0)
    {
        return {};
    }

    if (m_has_frame_to_send)
    {
        return {State::queued, utils::FsmCascade};
    }

    return {State::waitExe, utils::FsmCascade};
}

// EOF
