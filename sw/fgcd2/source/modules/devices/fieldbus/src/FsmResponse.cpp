//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <iterator>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <sys/time.h>

#include <cpp_utils/Result.h>
#include <cpp_utils/fsm.h>
#include <interfaces/IValueItem.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/common/inc/ValueLeaf.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Message.h>
#include <modules/devices/base/inc/Response.h>
#include <modules/devices/base/inc/Timestamp.h>
#include "../inc/FsmResponse.h"
#include "../inc/ResponsePacket.h"
#include "../inc/ValueParser.h"

using namespace fgcd;

FsmResponse::FsmResponse(Component& parent)
    : Component(parent, "FsmResponse::{}", parent.name()),
      m_fsm(*this, State::start)
{
    using enum State;
    // clang-format off
    m_fsm.addState(start, &FsmResponse::startState, {&FsmResponse::startToXx                         });
    m_fsm.addState(first, &FsmResponse::firstState, {&FsmResponse::xxToEnd                           });
    m_fsm.addState(next,  &FsmResponse::nextState,  {&FsmResponse::nextToError, &FsmResponse::xxToEnd});
    m_fsm.addState(end,   &FsmResponse::endState,   {                                                });
    m_fsm.addState(error, &FsmResponse::errorState, {                                                });
    // clang-format on
}

// ***************************************************

std::optional<CommandResponse> FsmResponse::process(const fieldbus::ResponsePacket& packet, const Command& command)
{
    logTrace("Response received");

    if (m_fsm.getState() == State::end)
    {
        logWarning("Logical error: class should be reseted");
        return std::nullopt;
    }

    m_type   = command.type();
    m_parse  = command.parse();
    m_is_ppm = command.ppm();
    m_packet = &packet;
    m_fsm.update();
    ++m_next_sequence;


    if (m_error || m_fsm.getState() != State::end)
    {
        return std::nullopt;
    }

    return m_response;
}

// **********************************************************

void FsmResponse::nextCycle()
{
    logDebug("FsmResponse reset");
    m_fsm.reset();
}

// **********************************************************

void FsmResponse::parseGet()
{
    logInfo("Parsing get");

    auto reader = BigReader{m_buffer};

    auto timestamp = reader.read<Timestamp>();
    if (timestamp.hasError())
    {
        logError("Timestamp error: {}", timestamp.error());
        m_error = true;
        return;
    }

    auto result = m_value_parser.parsePPM(reader.unreadData(), m_parse);
    if (result.hasError())
    {
        logError("Parsing error: {}", result.error());
        m_error = true;
    }
    m_response = CommandResponse{*timestamp, *result};
}

// **********************************************************

void FsmResponse::startState()
{
    logTrace("Start state");

    m_error = false;
}

// ***************************************************

void FsmResponse::firstState()
{
    logTrace("FirstPacket state");

    m_next_sequence = m_packet->getSequence();

    m_buffer.clear();
    std::ranges::copy(m_packet->getBody(), std::back_inserter(m_buffer));
}

// **********************************************************

void FsmResponse::nextState()
{
    logTrace("Next state");
    std::ranges::copy(m_packet->getBody(), std::back_inserter(m_buffer));
}

// **********************************************************

void FsmResponse::endState()
{
    logTrace("LastPacket state");

    // Pop termination charater
    m_buffer.pop_back();

    parseGet();
}

// **********************************************************

void FsmResponse::errorState()
{
    logError("Error state");

    m_error = true;
}

// **********************************************************

FsmResponse::TransRes FsmResponse::startToXx()
{
    if (!m_packet->isFirstPacket())
    {
        logTrace("Not first packet");
        return {};
    }

    return {State::first, utils::FsmCascade};
}

// **********************************************************

FsmResponse::TransRes FsmResponse::startToSubscription()
{
    if (m_packet->isFirstPacket())
    {
        return {State::first, utils::FsmCascade};
    }

    return {};
}

// **********************************************************

FsmResponse::TransRes FsmResponse::firstPktToNextPkt()
{
    if (!m_packet->isFirstPacket())
    {
        return {State::next};
    }

    return {};
}

// **********************************************************

FsmResponse::TransRes FsmResponse::xxToEnd()
{
    if (!m_packet->isLastPacket())
    {
        return {State::next};
    }

    // TODO
    if (m_buffer.empty())
    {
        logError("Empty response body");
        return {State::error, utils::FsmCascade};
    }

    if (m_buffer.back() != ';'_b)
    {
        logError("Missing termination character");
        return {State::error, utils::FsmCascade};
    }

    return {State::end, utils::FsmCascade};
}

// **********************************************************

FsmResponse::TransRes FsmResponse::nextToError()
{
    if (m_packet->isFirstPacket())
    {
        logError("First packet set for next packet");
        return {State::error, utils::FsmCascade};
    }

    if (m_packet->getSequence() != m_next_sequence)
    {
        logError("Wrong sequence number: {}, expected: {}", m_packet->getSequence(), +m_next_sequence);
        return {State::error, utils::FsmCascade};
    }

    return {};
}

// EOF
