//! @file
//! @brief
//! @author Adam Solawa

#include <cstddef>

#include "../inc/CommandPacket.h"
#include "../inc/PODs.h"

using namespace fgcd::fieldbus;

// **********************************************************

CommandPacket& CommandPacket::setFirstPacket(bool first)
{
    auto flags         = std::bit_cast<CommandFlags>(m_header.flags);
    flags.first_packet = first;
    m_header.flags     = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

CommandPacket& CommandPacket::setLastPacket(bool last)
{
    auto flags        = std::bit_cast<CommandFlags>(m_header.flags);
    flags.last_packet = last;
    m_header.flags    = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

CommandPacket& CommandPacket::setUser(std::uint8_t user)
{
    m_header.user = user;
    return *this;
}

// **********************************************************

CommandPacket& CommandPacket::setType(FieldbusCommandType type)
{
    auto flags     = std::bit_cast<CommandFlags>(m_header.flags);
    flags.type     = type;
    m_header.flags = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

std::size_t CommandPacket::setPayload(std::string_view payload)
{
    return doSetCommandBuffer(payload);
}

// **********************************************************

bool CommandPacket::isFirstPacket() const
{
    return std::bit_cast<CommandFlags>(m_header.flags).first_packet;
}

// **********************************************************

bool CommandPacket::isLastPacket() const
{
    return std::bit_cast<CommandFlags>(m_header.flags).last_packet;
}

// **********************************************************

std::uint8_t CommandPacket::getUser() const
{
    return m_header.user;
}

// **********************************************************

FieldbusCommandType CommandPacket::getType() const
{
    return std::bit_cast<CommandFlags>(m_header.flags).type;
}

// EOF
