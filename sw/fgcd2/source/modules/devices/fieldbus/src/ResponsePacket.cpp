//! @file
//! @brief
//! @author Adam Solawa

#include <string>
#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include "../inc/PODs.h"
#include "../inc/ResponsePacket.h"

using namespace fgcd::fieldbus;
using namespace fgcd;

// **********************************************************

ResponsePacket::ResponsePacket(ResponseHeader header, ByteStreamView payload)
    : m_header(header),
      m_payload(ByteStream{payload.begin(), payload.end()})
{
}

// **********************************************************

std::string ResponsePacket::getRemoteTerminal() const
{
    BigReader reader(m_payload);
    auto rterm = reader.readContainer<std::string>(m_header.n_rterm_chars);
    if(rterm.hasError())
    {
        return {};
    }

    return *rterm;
}

// **********************************************************

[[nodiscard]] ByteStreamView ResponsePacket::getBody() const
{
    return m_payload;
}

// **********************************************************

[[nodiscard]] bool ResponsePacket::isFirstPacket() const
{
    return std::bit_cast<ResponseFlags>(m_header.flags).first_packet;
}

// **********************************************************

[[nodiscard]] bool ResponsePacket::isLastPacket() const
{
    return std::bit_cast<ResponseFlags>(m_header.flags).last_packet;
}

// **********************************************************

[[nodiscard]] FieldbusResponseType ResponsePacket::getType() const
{
    return std::bit_cast<ResponseFlags>(m_header.flags).type;
}

// **********************************************************

[[nodiscard]] std::size_t ResponsePacket::getSequence() const
{
    return std::bit_cast<ResponseFlags>(m_header.flags).sequence;
}

// **********************************************************

ResponsePacket& ResponsePacket::setBody(ByteStream stream)
{
    m_payload = std::move(stream);
    return *this;
}

// **********************************************************

ResponsePacket& ResponsePacket::setFirstPacket(bool first)
{
    auto flags         = std::bit_cast<ResponseFlags>(m_header.flags);
    flags.first_packet = first;
    m_header.flags     = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

ResponsePacket& ResponsePacket::setLastPacket(bool last)
{
    auto flags        = std::bit_cast<ResponseFlags>(m_header.flags);
    flags.last_packet = last;
    m_header.flags    = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

ResponsePacket& ResponsePacket::setType(FieldbusResponseType type)
{
    auto flags     = std::bit_cast<ResponseFlags>(m_header.flags);
    flags.type     = type;
    m_header.flags = std::bit_cast<std::byte>(flags);
    return *this;
}

// **********************************************************

ResponsePacket& ResponsePacket::setSequence(std::size_t sequence)
{
    auto flags     = std::bit_cast<ResponseFlags>(m_header.flags);
    flags.sequence = sequence;
    m_header.flags = std::bit_cast<std::byte>(flags);
    return *this;
}

// EOF
