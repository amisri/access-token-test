//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <utility>

#include <cpp_utils/text.h>
#include <modules/base/logging/inc/Logger.h>
#include "../inc/Command.h"
#include "../inc/CommandPacket.h"
#include "../inc/FsmCommand.h"
#include "../inc/PODs.h"

using namespace fgcd;

// **********************************************************

FsmCommand::FsmCommand(Component& parent, fieldbus::CommandPacket& command_packet)
    : Component(parent, "FsmCommand::{}", parent.name()),
      m_fsm(*this, State::first_packet),
      m_command_packet(command_packet)
{
    using enum State;
    // clang-format off
    m_fsm.addState(first_packet,  &FsmCommand::firstPacketState,  {&FsmCommand::xxToLastPacket, &FsmCommand::firstToNextPacket});
    m_fsm.addState(middle_packet, &FsmCommand::middlePacketState, {&FsmCommand::xxToLastPacket});
    m_fsm.addState(last_packet,   &FsmCommand::lastPacketState,   {&FsmCommand::lastPacketToFirst});
    // clang-format on
}

// **********************************************************

void FsmCommand::publish()
{
    if (m_command_finished)
    {
        return;
    }

    m_fsm.update();
}

// **********************************************************

bool FsmCommand::setCommand(const Command& command)
{
    if (m_command_finished)
    {
        logInfo("Saving command {}", command);

        return saveCommand(command);
    }

    return false;
}

// **********************************************************

bool FsmCommand::hasFrameToSend() const
{
    return !m_command_finished;
}

// **********************************************************

bool FsmCommand::saveCommand(const Command& command)
{
    // Save selector
    auto selector = getFieldbusSelector(command.selector());
    if (not selector.has_value())
    {
        logError("Wrong selector of command {}", command);
        return false;
    }
    m_selector = *selector;

    // Save type

    m_buffer.assign(command.property());
    if(command.ppm())
    {
        m_buffer.append("()");
    }
    switch(command.type())
    {
        case CommandType::GET:
            m_type = fieldbus::FieldbusCommandType::get;
            break;

        case CommandType::SET:
            m_type = fieldbus::FieldbusCommandType::set;
            m_buffer.append(' ' + command.value().value_or(" "));
            break;

        case CommandType::SUB:
            m_type = fieldbus::FieldbusCommandType::sub;
            break;

        case CommandType::UNSUB:
            m_type = fieldbus::FieldbusCommandType::unsub;
            break;
    }

    // Save command string
    logDebug("Set command buffer '{}'", m_buffer);

    // Reset command state
    m_command_finished = false;

    return true;
}

// **********************************************************

void FsmCommand::publishPayload()
{
    logTrace("Copying data into packet");

    auto copied_length = m_command_packet.setPayload({m_buffer.begin() + m_buffer_offset, m_buffer.end()});

    // Advance the iterator
    m_buffer_offset += copied_length;

    // Check if there is still more to be sent
    if (m_buffer_offset >= m_buffer.size())
    {
        m_command_finished = true;
    }
}

// **********************************************************

void FsmCommand::firstPacketState()
{
    logDebug("First packet state");

    // Set state variables
    m_command_finished = false;
    m_buffer_offset    = 0;

    // Set up header
    m_command_packet.setFirstPacket(true);
    m_command_packet.setLastPacket(false);
    m_command_packet.setUser(m_selector);
    m_command_packet.setType(m_type);

    publishPayload();
}

// **********************************************************

void FsmCommand::middlePacketState()
{
    logDebug("Middle packet state");

    m_command_packet.setFirstPacket(false);
    m_command_packet.setLastPacket(false);

    publishPayload();
}

// **********************************************************

void FsmCommand::lastPacketState()
{
    logDebug("Last packet state");

    m_command_packet.setLastPacket(true);
}

// **********************************************************

FsmCommand::TransRes FsmCommand::xxToLastPacket()
{
    if (m_command_finished)
    {
        logTrace("Transition to lastPacket state");
        return { State::last_packet, utils::FsmCascade };
    }

    return {};
}

// **********************************************************

FsmCommand::TransRes FsmCommand::firstToNextPacket()
{
    if (!m_command_finished)
    {
        logTrace("Transition from firstPacket state to nextPacket");
        return { State::middle_packet };
    }

    return {};
}

// **********************************************************

FsmCommand::TransRes FsmCommand::lastPacketToFirst()
{
    logTrace("Transition from lastPacket state to firstPacket");
    return { State::first_packet };
}

// EOF
