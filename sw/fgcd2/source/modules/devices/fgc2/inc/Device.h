//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/fieldbus/inc/CodeDistributor.h>
#include <modules/devices/fieldbus/inc/FieldbusDevice.h>
#include <modules/net/worldFip/inc/MasterFip.h>
#include "CommandPacket.h"

namespace fgcd::fgc2
{
    class Device;
    using DevicePtr = std::shared_ptr<Device>;

    class Device : public FieldbusDevice
    {
      public:
        Device(
            Component& parent, fieldbus::CodeDistributor& code_distributor, CommandPacket& command_packet,
            MasterFip& socket, DeviceName name, int class_id, Json class_specific_data
        );

      private:
        void doSendCommand() override;

        MasterFip&     m_socket;
        CommandPacket& m_command_packet;
    };
}

// EOF
