//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string_view>

#include <cpp_utils/Result.h>
#include <modules/devices/fieldbus/inc/CommandPacket.h>
#include <modules/devices/fieldbus/inc/PODs.h>

namespace fgcd::fgc2
{
    class CommandPacket : public fieldbus::CommandPacket
    {
        static constexpr auto max_payload_size = 122UL;
        static constexpr auto min_payload_size = 3UL;

      public:
        auto serialize(auto&& frame)
        {
            return frame.write(m_header, m_payload);
        }

        void clear();

      private:
        std::size_t doSetCommandBuffer(std::string_view payload) override;

        std::string m_payload;
    };
}

// EOF
