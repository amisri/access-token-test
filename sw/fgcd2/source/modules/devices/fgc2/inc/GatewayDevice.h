//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <future>
#include <memory>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Device.h>

// **********************************************************

namespace fgcd::fgc2
{
    class GatewayDevice;
    using GatewayDevicePtr = std::shared_ptr<GatewayDevice>;

    class GatewayDevice : public Component, public fgcd::Device
    {
      public:
        GatewayDevice(Component& parent, DeviceName name, int class_id, Json class_specific_data);

        std::future<Response> executeCommand(Command command) override;
        void                  reprogram(const ReprogramData& data) override;
        DeviceName            name() override;
        int                   classId() override;

        int getId();

      private:
        DeviceName m_device_name;
        int        m_class_id;
    };
}

// EOF
