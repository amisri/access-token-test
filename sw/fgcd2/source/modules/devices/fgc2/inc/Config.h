//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <modules/devices/fieldbus/inc/FgcCode.h>
#include <modules/devices/fieldbus/inc/ProtocolConfig.h>

namespace fgcd::fgc2::config
{
    constexpr std::size_t code_blocks_per_message = 2;
    constexpr std::size_t code_block_size         = fieldbus::FgcCode::block_size;

    constexpr std::size_t max_command_length = 122;
    constexpr std::size_t min_command_length = 3;


    constexpr std::size_t max_device_name_length = 32;
    constexpr std::size_t max_devices            = 65;
    constexpr std::size_t max_subdevices         = 160;

    constexpr std::size_t namedb_block_length = 256;
}

// EOF
