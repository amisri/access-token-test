//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/fieldbus/inc/FgcCode.h>
#include <modules/devices/fieldbus/inc/TimePacket.h>

namespace fgcd::fgc2
{
    struct TimePacket : public fieldbus::TimePacket
    {
        static constexpr std::size_t size = sizeof(fieldbus::FieldbusTime);

        auto serialize(auto&& archive)
        {
            return archive.write(getTime());
        }
    };
}

// EOF
