//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <array>
#include <cstddef>
#include <cstdint>

namespace fgcd::fgc2
{
    struct RtData
    {
        std::uint8_t             sequence_number;
        std::array<std::byte, 3> reserved;
        std::array<float, 15>    rt_data;
    };
}

// EOF
