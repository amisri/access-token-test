//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <json/json.hpp>
#include <memory>
#include <vector>

#include <interfaces/IPlatformManager.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/misc/inc/BinaryWriter.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/base/inc/DeviceInventory.h>
#include <modules/devices/fgc3/inc/Device.h>
#include <modules/devices/fieldbus/inc/CodeDistributor.h>
#include <modules/devices/programming/inc/ReprogrammingManager.h>
#include <modules/net/worldFip/inc/Frame.h>
#include <modules/net/worldFip/inc/MasterFip.h>
#include <modules/timing/inc/TimerManager.h>
#include <modules/timing/inc/TimeSource.h>
#include "CodePacket.h"
#include "CommandPacket.h"
#include "Device.h"
#include "GatewayDevice.h"
#include "TimePacket.h"

namespace fgcd::fgc2
{
    class PlatformManager : public Component, public IPlatformManager
    {
        static auto constexpr cycle_duration = 20ms;

      public:
        PlatformManager(Component& parent, TimeSourcePtr timing, ReprogrammingManagerPtr reprogram_manager);

        bool            manages(int class_id) override;
        fgcd::DevicePtr createDevice(DeviceName name, int class_id, Json json_data) override;

      private:
        void onStart() override;

        void prepareCycle(fip::Frame time_frame, fip::Frame code_frame);
        void handleStatus(fip::Frame frame, std::uint8_t device_id);
        void handleResponse(fip::Frame frame, std::uint8_t device_id);
        void setTime();
        void setNamedb();

        GatewayDevicePtr       m_gateway;
        std::vector<DevicePtr> m_devices;

        TimerManagerPtr            m_timer;
        MasterFip&                 m_socket;
        TimePacket                 m_time_packet{};
        CodePacket                 m_code_packet{};
        CommandPacket              m_command_packet{};
        fieldbus::CodeDistributor& m_code_distributor;
        TimeSourcePtr           m_timing;
        ReprogrammingManagerPtr    m_reprogram_manager;
    };
}

// EOF
