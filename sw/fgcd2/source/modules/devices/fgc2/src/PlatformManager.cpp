//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <array>
#include <chrono>
#include <cstdint>
#include <utility>

#include <modules/base/clock/inc/utils.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/base/misc/inc/BinaryWriter.h>
#include <modules/common/inc/crc.h>
#include <modules/devices/fieldbus/inc/CodeDistributor.h>
#include <modules/devices/fieldbus/inc/FgcCode.h>
#include <modules/devices/fieldbus/inc/PODs.h>
#include <modules/devices/fieldbus/inc/StatusPacket.h>
#include <modules/net/worldFip/inc/Frame.h>
#include <modules/net/worldFip/inc/MasterFip.h>
#include <modules/timing/inc/TimeSource.h>
#include "../inc/Config.h"
#include "../inc/Device.h"
#include "../inc/GatewayDevice.h"
#include "../inc/PlatformManager.h"

using namespace fgcd::fgc2;

// **********************************************************

PlatformManager::PlatformManager(Component& parent, TimeSourcePtr timing, ReprogrammingManagerPtr reprogram_manager)
    : Component(parent, "PlatformManager"),
      m_socket(*addComponent<MasterFip>(0)),
      m_code_distributor(*addComponent<fieldbus::CodeDistributor>()),
      m_timing(std::move(timing)),
      m_reprogram_manager(std::move(reprogram_manager))
{
    m_socket.setPrepareNextCycle(std::bind_front(&PlatformManager::prepareCycle, this));
    m_socket.setListener(fieldbus::PacketType::status, std::bind_front(&PlatformManager::handleStatus, this));
    m_socket.setListener(fieldbus::PacketType::response, std::bind_front(&PlatformManager::handleResponse, this));
}

// **********************************************************

fgcd::DevicePtr PlatformManager::createDevice(DeviceName name, int class_id, Json json_data)
{
    logDebug("Adding device {}", name);

    try
    {
        if (class_id == 1)
        {
            m_gateway = addComponent<GatewayDevice>(name, class_id, json_data);
            return m_gateway;
        }
        else
        {
            auto device
                = addComponent<Device>(m_code_distributor, m_command_packet, m_socket, name, class_id, json_data);
            m_devices.push_back(device);
            return device;
        }
    }
    // Catch JSON access field exceptions
    catch (...)
    {
        logError("Cannot create device");
        throw;
    }
}

// **********************************************************

bool PlatformManager::manages(int class_id)
{
    if (class_id == 1)
    {
        return true;
    }

    if (50 <= class_id && class_id < 60)
    {
        return true;
    }

    return false;
}

// **********************************************************

void PlatformManager::onStart()
{
    setNamedb();
}

// **********************************************************

void PlatformManager::prepareCycle(fip::Frame time_frame, fip::Frame code_frame)
{
    // Tick devices
    std::ranges::for_each(m_devices, &Device::tick);

    // Set time
    setTime();

    m_reprogram_manager->execute();

    // Prepare packets
    m_code_distributor.publishCode(m_code_packet, m_time_packet);
    m_time_packet.setTime(m_clock.now());
    m_time_packet.tick();

    // Serialize packets
    if (auto result = m_time_packet.serialize(time_frame);   //
        result.hasError())
    {
        logError("Failure serializing timepacket: {}", result.error());
    }

    if (auto result = m_code_packet.serialize(code_frame);   //
        result.hasError())
    {
        logError("Failure serializing code: {}", result.error());
    }

    m_code_packet.clear();
}

// **********************************************************

void PlatformManager::handleStatus(fip::Frame frame, std::uint8_t device_id)
{
    auto device = std::ranges::find(m_devices, device_id, &Device::getDongleId);
    if (device == m_devices.end())
    {
        logWarning("Get status for not avaiable device {}", device_id);
        return;
    }

    auto status = fieldbus::StatusPacket::fromBytes(frame);
    if (!status.hasValue())
    {
        logError("Cannot deserialize status packet: {}", status.error());
        return;
    }

    (*device)->handle(*status);
}

// **********************************************************

void PlatformManager::handleResponse(fip::Frame frame, std::uint8_t device_id)
{
    auto device = std::ranges::find(m_devices, device_id, &Device::getDongleId);
    if (device == m_devices.end())
    {
        logWarning("Get response for not avaiable device {}", device_id);
        return;
    }

    auto packet = fieldbus::ResponsePacket::fromBytes(frame);
    if (!packet.hasValue())
    {
        logError("Cannot deserialize response packet: {}", packet.error());
        return;
    }

    (*device)->pushRterm(packet->getRemoteTerminal());
    (*device)->handle(*packet);
}

// **********************************************************

void PlatformManager::setTime()
{
    using namespace std::chrono_literals;

    auto time = toTimePoint(m_timing->getTime());
    time      += cycle_duration;
    time      = roundDownTimepoint(time, cycle_duration);

    m_clock.set(toTimespec(time));
}

// **********************************************************

void PlatformManager::setNamedb()
{
    struct DeviceRecord
    {
        std::uint16_t        class_id;     // [2]  Device class
        std::uint16_t        omode_mask;   // [2]  Operational mode mask
        std::array<char, 32> name;         // [32] Device name
    };

    ReprogramData data{};
    data.endpoint.version      = "0";
    data.endpoint.subsystem_id = "0:0";
    data.binary.reserve(fieldbus::FgcCode::block_size * config::namedb_block_length);
    data.binary.resize(fieldbus::FgcCode::block_size * config::namedb_block_length - 2);

    BigWriter writer(data.binary);

    // **********************************************************
    // Set gateway record
    // **********************************************************
    DeviceRecord record{};
    auto         name = m_gateway->name();

    record.class_id   = m_gateway->classId();
    record.omode_mask = 0;
    name.device_name.copy(record.name.data(), std::string::npos);
    record.name.back() = '\0';

    writer.write(record);


    // **********************************************************
    // Set devices record
    // **********************************************************
    for (auto&& device : m_devices)
    {
        DeviceRecord record{};
        auto         name = device->name();

        record.class_id   = device->classId();
        record.omode_mask = 0;
        name.device_name.copy(record.name.data(), std::string::npos);
        record.name.back() = '\0';

        writer.write(record);
    }

    auto crc = calculateCrc16(data.binary);
    writer.write(crc);
    logDebug("Set name db code with crc: {:x}", crc);

    m_code_distributor.setCode(data);
}

// EOF
