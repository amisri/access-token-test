//! @file
//! @brief  
//! @author Adam Solawa

#include <future>
#include <utility>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Device.h>
#include "../inc/GatewayDevice.h"

using namespace fgcd::fgc2;

// **********************************************************

GatewayDevice::GatewayDevice(Component& parent, DeviceName name, int class_id, Json class_specific_data)
    : Component(parent, "GatewayDevice"),
      m_device_name(std::move(name)),
      m_class_id(class_id)
{
}

// **********************************************************

int GatewayDevice::getId()
{
    return 0;
}

// **********************************************************

std::future<fgcd::Response> GatewayDevice::executeCommand(Command command)
{
    throw Exception("Not implemented");
}

// **********************************************************

void GatewayDevice::reprogram(const ReprogramData& data)
{
    throw Exception("Not implemented");
}

// **********************************************************

fgcd::DeviceName GatewayDevice::name()
{
    return m_device_name;
}

// **********************************************************

int GatewayDevice::classId()
{
    return m_class_id;
}

// EOF
