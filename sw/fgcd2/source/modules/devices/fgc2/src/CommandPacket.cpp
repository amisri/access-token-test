//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <iterator>
#include <string_view>

#include <modules/base/core/inc/Exception.h>
#include <modules/devices/fieldbus/inc/CommandPacket.h>
#include "../inc/CommandPacket.h"

using namespace fgcd::fgc2;

// **********************************************************

std::size_t CommandPacket::doSetCommandBuffer(std::string_view payload)
{
    auto copy_bytes = std::min(max_payload_size, payload.size());

    m_payload.reserve(max_payload_size);
    std::copy_n(payload.begin(), copy_bytes, std::back_inserter(m_payload));

    if (copy_bytes < min_payload_size)
    {
        m_payload.resize(min_payload_size);
    }

    return copy_bytes;
}

// **********************************************************

void CommandPacket::clear()
{
    m_payload.clear();
}

// EOF
