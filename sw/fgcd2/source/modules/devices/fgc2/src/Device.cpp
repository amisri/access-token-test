//! @file
//! @brief  
//! @author Adam Solawa

#include <cstdint>
#include <utility>

#include <cpp_utils/misc.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/fieldbus/inc/CodeDistributor.h>
#include <modules/devices/fieldbus/inc/FieldbusDevice.h>
#include <modules/net/worldFip/inc/MasterFip.h>
#include "../inc/CommandPacket.h"
#include "../inc/Device.h"

using namespace fgcd::fgc2;

// **********************************************************

Device::Device(
    Component& parent, fieldbus::CodeDistributor& code_distributor, CommandPacket& command_packet, MasterFip& socket,
    DeviceName name, int class_id, Json class_specific_data
)
    : FieldbusDevice(parent, code_distributor, {}, command_packet, std::move(name), class_id, class_specific_data.at("dongle_id")),
      m_socket(socket),
      m_command_packet(command_packet)
{
}

// **********************************************************

void Device::doSendCommand()
{
    utils::Finally cleanup(
        [this]()
        {
            m_command_packet.clear();
        }
    );

    auto frame = m_socket.getCommandFrame(getDongleId());
    if (auto result = m_command_packet.serialize(frame);   //
        result.hasError())
    {
        logWarning("Cannot serialize command packet: {}", result.error());
        return;
    }

    m_socket.sendCommand(frame);
}

// EOF
