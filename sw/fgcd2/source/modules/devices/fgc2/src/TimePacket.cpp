//! @file
//! @brief  
//! @author Adam Solawa

#include <cstddef>
#include <cstdint>

#include <modules/base/core/inc/Exception.h>
#include <modules/devices/fieldbus/inc/FgcCode.h>
#include "../inc/TimePacket.h"

using namespace fgcd::fgc2;

// **********************************************************

// EOF
