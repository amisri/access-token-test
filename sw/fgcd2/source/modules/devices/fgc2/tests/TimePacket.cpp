#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/TimePacket.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct TimePacketTests : public ::testing::Test
{
    RootMockPtr root;
    TimePacketPtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<TimePacket>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(TimePacketTests, TimePacket_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
