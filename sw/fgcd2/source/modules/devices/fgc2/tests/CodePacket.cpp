#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/CodePacket.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct CodePacketTests : public ::testing::Test
{
    RootMockPtr root;
    CodePacketPtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<CodePacket>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(CodePacketTests, CodePacket_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
