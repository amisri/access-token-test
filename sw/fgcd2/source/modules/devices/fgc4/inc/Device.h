//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <modules/base/component/inc/Component.h>
#include <modules/devices/base/inc/Device.h>

// **********************************************************

namespace fgcd::fgc4
{
    class Device;
    using DevicePtr = std::shared_ptr<Device>;

    class Device : public fgcd::Device
    {
      public:
        Device(Component& parent, const DeviceData& data);

      private:
        void doExecuteCommand(CommandPtr command) override;
        void doReprogram(const ReprogramData& data) override;
    };
}

// EOF
