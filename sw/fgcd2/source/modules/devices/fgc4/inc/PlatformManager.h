//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <array>

#include <interfaces/IPlatformManager.h>
#include <modules/base/component/inc/Component.h>
#include "Device.h"

namespace fgcd::fgc4
{
    class PlatformManager : public Component, public IPlatformManager
    {
        static std::array constexpr managed_classes = {123, 14};

      public:
        PlatformManager(Component& parent);
        fgcd::DevicePtr createDevice(DeviceName name, int class_id, Json json_data) override;
        bool            manages(int class_id) override;

      private:
        std::vector<DevicePtr> m_devices{};
    };
}

// EOF
