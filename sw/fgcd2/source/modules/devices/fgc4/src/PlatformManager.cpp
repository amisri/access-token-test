//! @file
//! @brief
//! @author Adam Solawa

#include <utility>

#include <modules/base/component/inc/Component.h>
#include "../inc/Device.h"
#include "../inc/PlatformManager.h"

using namespace fgcd::fgc4;

// **********************************************************

PlatformManager::PlatformManager(Component& parent)
    : Component(parent, "PlatformManager")
{
}

// **********************************************************

bool PlatformManager::manages(int class_id)
{
    for (auto&& id : managed_classes)
    {
        if (id == class_id)
        {
            return true;
        }
    }

    return false;
}

// **********************************************************

fgcd::DevicePtr PlatformManager::createDevice(DeviceName name, int class_id, Json json_data)
{
    DeviceData data{
        .name           = name,
        .class_id       = class_id,
        .class_specific = json_data,
    };

    auto device = addComponent<Device>(data);

    m_devices.push_back(device);

    return std::move(device);
}

// EOF
