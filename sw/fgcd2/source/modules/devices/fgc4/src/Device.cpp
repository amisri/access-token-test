//! @file
//! @brief  
//! @author Adam Solawa

#include <utility>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/base/inc/Response.h>
#include "../inc/Device.h"

using namespace fgcd::fgc4;

// **********************************************************

Device::Device(Component& parent, const DeviceData& data)
    : fgcd::Device(parent, data)
{
}

// **********************************************************

void Device::doExecuteCommand(CommandPtr command)
{
    logInfo("Execute command {}", *command);

    auto response = ErrorResponse{.error_code = -1, .message = "Not implemented"};
    bool success  = Command::setResponse(std::move(command), std::move(response));
    if (not success)
    {
        logError("Cannot push response");
    }

    finishCommand();
}

// **********************************************************

void Device::doReprogram(const ReprogramData& data)
{
    logInfo("Do reprogram");
}

// EOF
