//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <filesystem>
#include <memory>
#include <vector>

#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/programming/inc/types.h>

namespace fgcd
{
    class ReprogramTask;

    using ReprogramTaskPtr = std::shared_ptr<ReprogramTask>;

    class ReprogramTask
    {
      public:
        ReprogramTask(std::filesystem::path path, ReprogramEndpoint endpoint);
        void addDevice(DevicePtr device);
        void execute();

      private:
        ReprogramData          m_reprogram_data;
        std::vector<DevicePtr> m_devices = {};
    };
}

// EOF
