//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <cpp_utils/circularBuffer.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/base/inc/DeviceInventory.h>
#include "ReprogramTask.h"
#include "ReprogrammingManager.h"

namespace fgcd
{
    class ReprogrammingManager;
    using ReprogrammingManagerPtr = std::shared_ptr<ReprogrammingManager>;

    class ReprogrammingManager : public Component
    {
        static constexpr auto max_tasks = 10U;
        using Buffer                    = utils::CircularBuffer<ReprogramTaskPtr, max_tasks>;
        using Tasks                     = std::map<ReprogramEndpoint, ReprogramTaskPtr>;

      public:
        ReprogrammingManager(Component& parent, DeviceInventory& inventory);

        void execute()
        {
            auto task = m_tasks.pop();

            if (!task.has_value())
            {
                return;
            }

            (*task)->execute();
        }

        void push(const ReprogramTaskPtr& task)
        {
            logInfo("Pushing tasks");
            m_tasks.push(task);
        }

      private:
        DeviceInventory& m_inventory;
        Buffer           m_tasks;
    };
}

// EOF
