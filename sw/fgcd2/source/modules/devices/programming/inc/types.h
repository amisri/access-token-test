//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string>

#include <modules/base/core/inc/types.h>

namespace fgcd
{
    struct ReprogramEndpoint
    {
        std::string subsystem_id = {};
        std::string version      = {};

        auto operator<=>(const ReprogramEndpoint&) const = default;
    };

    inline std::ostream& operator<<(std::ostream& os, const ReprogramEndpoint& endpoint)
    {
        return os << endpoint.subsystem_id << "," << endpoint.version;
    }

    struct ReprogramData
    {
        ReprogramEndpoint endpoint = {};
        ByteStream        binary   = {};
    };
}

// EOF
