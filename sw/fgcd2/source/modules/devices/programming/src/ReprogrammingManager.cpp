//! @file
//! @brief  
//! @author Adam Solawa

#include <fstream>
#include <memory>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/DeviceInventory.h>
#include "../inc/ReprogramTask.h"
#include "../inc/ReprogrammingManager.h"
#include "../inc/types.h"

using namespace fgcd;

// **********************************************************

ReprogrammingManager::ReprogrammingManager(Component& parent, DeviceInventory& inventory)
    : Component(parent, "ReprogrammingManager"),
      m_inventory(inventory)
{
}

// EOF
