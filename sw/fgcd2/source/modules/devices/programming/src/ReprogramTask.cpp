//! @file
//! @brief  
//! @author Adam Solawa

#include <algorithm>
#include <cassert>
#include <filesystem>
#include <fmt/core.h>
#include <fstream>
#include <ios>
#include <memory>
#include <stdexcept>
#include <utility>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Device.h>
#include "../inc/ReprogramTask.h"
#include "../inc/types.h"

using namespace fgcd;

// **********************************************************

ReprogramTask::ReprogramTask(std::filesystem::path path, ReprogramEndpoint endpoint)
{
    if (!std::filesystem::exists(path))
    {
        throw Exception(fmt::format("Path {} doesnt exists", path.string()));
    }

    auto file      = std::ifstream(path, std::ios::binary | std::ios::in);
    auto file_size = std::filesystem::file_size(path);

    ByteStream binary = {};
    binary.resize(file_size);

    file.read(reinterpret_cast<char*>(binary.data()), static_cast<long>(file_size));   // NOLINT

    m_reprogram_data = ReprogramData({std::move(endpoint), std::move(binary)});
}

// **********************************************************

void ReprogramTask::addDevice(DevicePtr device)
{
    m_devices.push_back(std::move(device));
}

// **********************************************************

void ReprogramTask::execute()
{
    std::ranges::for_each(
        m_devices,
        [this](auto&& device)
        {
            device->reprogram(m_reprogram_data);
        }
    );
}

// EOF
