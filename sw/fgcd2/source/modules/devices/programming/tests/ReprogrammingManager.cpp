#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/ReprogrammingManager.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

class ReprogrammingManagerTests : public ::testing::Test
{
  public:
    RootMockPtr root;
    ReprogrammingManagerPtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<ReprogrammingManager>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(ReprogrammingManagerTests, ReprogrammingManager_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
