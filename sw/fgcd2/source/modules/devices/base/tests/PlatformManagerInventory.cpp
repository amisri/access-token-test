#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/PlatformManagerInventory.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct PlatformManagerInventoryTests : public ::testing::Test
{
    RootMockPtr root;
    PlatformManagerInventoryPtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<PlatformManagerInventory>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(PlatformManagerInventoryTests, PlatformManagerInventory_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
