#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/DeviceInventory.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct DeviceInventoryTests : public ::testing::Test
{
    RootMockPtr root;
    DeviceInventoryPtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<DeviceInventory>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(DeviceInventoryTests, DeviceInventory_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
