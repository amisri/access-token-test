//! @file
//! @brief
//! @author Adam Solawa

#include <fstream>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/programming/inc/types.h>
#include "../inc/DeviceInventory.h"
#include "../inc/NameFileReader.h"
#include "../inc/PlatformManagerInventory.h"

using namespace fgcd;

// **********************************************************

NameFileReader::NameFileReader(
    Component& parent, PlatformManagerInventory& platform_inventory, ReprogrammingManager& reprogramming,
    DeviceInventory& device_inventory
)
    : Component(parent, "NameFileReader"),
      m_platform_inventory(platform_inventory),
      m_reprogramming(reprogramming),
      m_device_inventory(device_inventory)
{
}

// **********************************************************

void NameFileReader::loadConfig()
{
    std::string   filename = "namefile.json";
    std::ifstream file(filename);
    if (!file.is_open())
    {
        logError("Cannot open namefile");
        return;
    }

    Json data = Json::parse(file);

    // **********************************************************
    // Load binaries information
    // **********************************************************

    for (auto&& binary : data.at("binaries"))
    {
        ReprogramEndpoint endpoint;
        endpoint.subsystem_id = binary.at("id");
        endpoint.version      = binary.at("version");
        std::filesystem::path path(binary.at("path"));
        if(!std::filesystem::exists(path))
        {
            logError("Path {} doen't exists", path.string());
            continue;
        }
        m_binaries[endpoint]  = path;
    }


    for (auto&& device_data : data.at("devices"))
    {
        // **********************************************************
        // Create device
        // **********************************************************
        DeviceName name{device_data.at("name"), device_data.at("alias")};
        auto platform = m_platform_inventory.getPlatformManager(device_data.at("class_id"));
        if(!platform.has_value())
        {
            logError("Cannot find platform for class {}", device_data.at("class_id"));
            continue;
        }

        auto device = (*platform)->createDevice(name, device_data.at("class_id"), device_data.at("class_specific"));
        m_device_inventory.addDevice(name, device);

        // **********************************************************
        // Create reprogramming tasks
        // **********************************************************

        for (auto&& system : device_data.at("systems"))
        {
            ReprogramEndpoint endpoint;
            endpoint.subsystem_id = system.at("id");
            endpoint.version      = system.at("version");

            if (!m_tasks.contains(endpoint))
            {
                if (!m_binaries.contains(endpoint))
                {
                    logError("Cannot find endpoint {}", endpoint);
                    continue;
                }

                auto path = m_binaries.at(endpoint);
                auto task = ReprogramTask(path, endpoint);
                m_tasks[endpoint] = std::make_shared<ReprogramTask>(task);
            }

            m_tasks[endpoint]->addDevice(device);
        }
    }

    for(auto&& [endpoint, task] : m_tasks)
    {
        m_reprogramming.push(task);
    }
}

// **********************************************************

void NameFileReader::onStart()
{
    loadConfig();
}

// EOF
