//! @file
//! @brief  
//! @author Adam Solawa

#include <atomic>
#include <utility>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>
#include "../inc/Command.h"
#include "../inc/Device.h"

using namespace fgcd;

// **********************************************************

Device::Device(Component& parent, const DeviceData& data)
    : Component(parent, "Device"),
      m_device_name(data.name),
      m_class_id(data.class_id)
{
    m_thread.start(
        threads::device,
        [this](std::atomic_bool& keep_running)
        {
            worker(keep_running);
        }
    );
}

// **********************************************************

DeviceName Device::name() const
{
    return m_device_name;
}

// **********************************************************

int Device::classId() const
{
    return m_class_id;
}

// **********************************************************

void Device::reprogram(const ReprogramData& data)
{
    doReprogram(data);
}

// **********************************************************

void Device::pushCommand(CommandPtr command)
{
    logInfo("Push Command");
    m_commands.pushBlock(std::move(command));
}

// **********************************************************

void Device::worker(std::atomic_bool& keep_running)
{
    while (keep_running)
    {
        auto command = m_commands.popBlock();

        if (not command)
        {
            logWarning("Commands queue disabled");
            break;
        }

        m_busy.acquire();
        doExecuteCommand(std::move(*command));
    }
}

// **********************************************************

void Device::finishCommand()
{
    m_busy.release();
}

// **********************************************************

void Device::pushRterm(const std::string& message)
{
    m_rterm_messages.push(message);
}

// EOF
