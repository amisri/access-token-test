//! @file
//! @brief
//! @author Adam Solawa

#include <interfaces/IValueItem.h>
#include "../inc/Message.h"

// **********************************************************

std::string fgcd::messages::toString(const MaybeMessage& message)
{
    if (!message.has_value())
    {
        return "";
    }

    return std::visit(
        [](auto message) -> std::string
        {
            return fgcd::messages::toString(message);
        },
        *message
    );
}

// **********************************************************

std::string fgcd::messages::toString(const Get& message)
{
    return fgcd::messages::toString(message.value);
}

// **********************************************************

std::string fgcd::messages::toString(const GetPPM& message)
{
    std::string result;
    for (auto&& value : message.values)
    {
        result += fgcd::messages::toString(value);
    }
    return result;
}

// **********************************************************

std::string fgcd::messages::toString(const Subscription& message)
{
    return "SUB none";
}

// **********************************************************

std::string fgcd::messages::toString(const IValueItemPtr& item)
{
    return item->toString();
}

// EOF
