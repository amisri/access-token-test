//! @file
//! @brief  
//! @author Adam Solawa

#include <optional>
#include <string>
#include <string_view>
#include <utility>
#include <modules/base/component/inc/Component.h>
#include "../inc/DeviceInventory.h"

using namespace fgcd;

// **********************************************************

DeviceInventory::DeviceInventory(Component& parent)
    : Component(parent, "DeviceInventory")
{
}

// **********************************************************

void DeviceInventory::addDevice(DeviceName name, DevicePtr device)
{
    m_list[name] = std::move(device);
}

// **********************************************************

std::optional<DevicePtr> DeviceInventory::find(DeviceName name)
{
    if(m_list.contains(name))
    {
        return m_list.find(name)->second;
    }

    return std::nullopt;
}

// **********************************************************

std::optional<DevicePtr> DeviceInventory::find(std::string_view name)
{
    for(auto&&[device_name, device] : m_list)
    {
        if(device_name.alias == name or device_name.device_name == name)
        {
            return device;
        }
    }

    return std::nullopt;
}

// EOF
