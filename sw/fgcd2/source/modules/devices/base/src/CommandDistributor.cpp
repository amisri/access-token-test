//! @file
//! @brief  
//! @author Adam Solawa

#include <fmt/format.h>
#include <optional>
#include <string>
#include <utility>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/thread/inc/threads.h>
#include "../inc/Command.h"
#include "../inc/CommandDistributor.h"
#include "../inc/DeviceInventory.h"
#include "../inc/Response.h"

using namespace fgcd;

// **********************************************************

CommandDistributor::CommandDistributor(Component& parent, DeviceInventoryPtr inventory)
    : Component(parent, "CommandDistributor"),
      m_inventory(std::move(inventory))
{
}

// **********************************************************

bool CommandDistributor::pushCommand(CommandPtr command)
{
    auto result = m_inventory->find(command->device());
    if (not result)
    {
        logWarning("Received command for not avaiable device: {}", command->device());

        if (not Command::setResponse(
                std::move(command),                                                            //
                ErrorResponse{0, fmt::format("Cannot find device: '{}'", command->device())}   //
            ))
        {
            logError("Cannot push response");
        }

        return false;
    }
    auto&& device = std::move(*result);

    // TODOOOO Run all validators


    device->pushCommand(std::move(command));

    return true;
}

// EOF
