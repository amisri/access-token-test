//! @file
//! @brief  
//! @author Adam Solawa

#include <algorithm>
#include <optional>

#include <interfaces/IPlatformManager.h>
#include <modules/base/component/inc/Component.h>
#include "../inc/PlatformManagerInventory.h"

using namespace fgcd;

// **********************************************************

PlatformManagerInventory::PlatformManagerInventory(Component& parent)
    : Component(parent, "PlatformManagerInventory")
{
}

// **********************************************************


std::optional<IPlatformManagerPtr> PlatformManagerInventory::getPlatformManager(int class_id)
{
    for(auto && manager : m_platform_managers)
    {
        if(manager->manages(class_id))
        {
            return manager;
        }
    }

    return std::nullopt;
}

// EOF
