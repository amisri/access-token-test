//! @file
//! @brief  
//! @author Adam Solawa

#include <utility>
#include "../inc/Command.h"

using namespace fgcd;

// **********************************************************

Command::Command(
    ResponseQueuePtr queue, std::any specific, CommandType type, std::string device, std::string tag, bool parse,
    std::string                property,      //
    std::optional<std::string> selector,      //
    std::optional<std::string> transaction,   //
    std::optional<std::string> filter,        //
    std::string                rest           //
)
    : m_type(type),
      m_device(std::move(device)),
      m_tag(std::move(tag)),
      m_specific(std::move(specific)),
      m_parse(parse),
      m_queue(std::move(queue))
{
    m_fields.property    = std::move(property);
    m_fields.selector    = std::move(selector);
    m_fields.transaction = std::move(transaction);
    m_fields.filter      = std::move(filter);

    switch (m_type)
    {
        case CommandType::GET:
            m_fields.option = std::move(rest);
            break;

        case CommandType::SET:
            m_fields.value = std::move(rest);
            break;

        case CommandType::SUB:
        case CommandType::UNSUB:
            break;
    }
}

// **********************************************************

std::string Command::property() const
{
    return m_fields.property;
}

// **********************************************************

std::optional<std::string> Command::value() const
{
    return m_fields.value;
}

// **********************************************************

std::optional<std::string> Command::selector() const
{
    // TODOOOOO
    return m_fields.selector;
}

// **********************************************************

std::string Command::tag() const
{
    return m_tag;
}

// **********************************************************

std::string Command::device() const
{
    return m_device;
}

// **********************************************************

std::string Command::source() const
{
    return m_source;
}

// **********************************************************

std::any Command::specific() const
{
    return m_specific;
}

// **********************************************************

bool Command::parse() const
{
    return m_parse;
}

// **********************************************************

CommandType Command::type() const
{
    return m_type;
}

// **********************************************************

bool Command::ppm() const
{
    if (m_fields.selector.has_value())
    {
        return m_fields.selector->empty();
    }
    return false;
}

// **********************************************************

const Response& Command::response() const
{
    return m_response;
}

// **********************************************************

bool Command::setResponse(CommandPtr command, Response response)
{
    command->m_response = std::move(response);

    auto& queue = command->m_queue;
    return queue->push(std::move(command));
}

// EOF
