//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include <interfaces/IPlatformManager.h>
#include <modules/base/component/inc/Component.h>

// **********************************************************

namespace fgcd
{
    class PlatformManagerInventory;
    using PlatformManagerInventoryPtr = std::shared_ptr<PlatformManagerInventory>;

    class PlatformManagerInventory : public Component
    {
      public:
        PlatformManagerInventory(Component& parent);

        template<typename T, typename... Args>
        std::shared_ptr<T> addPlatformManager(Args&&... args)
        {
            auto manager = addComponent<T>(std::forward<Args>(args)...);
            m_platform_managers.push_back(manager);

            return manager;
        }

        std::optional<IPlatformManagerPtr> getPlatformManager(int class_id);

      private:
        std::vector<IPlatformManagerPtr> m_platform_managers{};
    };
}

// EOF
