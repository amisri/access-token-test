//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <fmt/core.h>

namespace fgcd
{
    struct Point
    {
        float time;
        float value;

        auto operator<=>(const Point&) const = default;
    };
}

// EOF
