//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <chrono>
#include <cstdint>

#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/BinaryReader.h>

namespace fgcd
{
    struct Timestamp
    {
        std::uint32_t second      = {};
        std::uint32_t microsecond = {};

        auto operator<=>(const Timestamp&) const = default;

        [[nodiscard]] TimePoint toTimepoint() const
        {
            return TimePoint(std::chrono::seconds(second) + std::chrono::microseconds(microsecond));
        }
    };
}

// EOF
