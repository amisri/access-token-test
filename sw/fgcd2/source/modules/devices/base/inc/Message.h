//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>
#include <optional>
#include <string>
#include <variant>

#include <interfaces/IValueItem.h>
#include <modules/common/inc/ValueLeaf.h>
#include "Timestamp.h"

namespace fgcd::messages
{
    struct Get
    {
        Timestamp     timestamp = {};
        IValueItemPtr value     = std::make_shared<ValueLeaf>(std::monostate());
    };

    struct GetPPM
    {
        Timestamp   timestamp = {};
        IValueVector values    = {};
    };

    struct Subscription
    {
        std::uint8_t subscription_id = 0;
    };

    using Message      = std::variant<Subscription, Get, GetPPM>;
    using MaybeMessage = std::optional<Message>;

    std::string toString(const MaybeMessage& message);
    std::string toString(const Get& message);
    std::string toString(const GetPPM& message);
    std::string toString(const Subscription& message);
    std::string toString(const IValueItemPtr& item);
}

// EOF
