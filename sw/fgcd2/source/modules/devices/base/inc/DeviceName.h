//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <fmt/core.h>
#include <string>

namespace fgcd
{
    struct DeviceName
    {
        std::string device_name;
        std::string alias;

        auto operator<=>(const DeviceName&) const = default;
    };
}

template<>
struct fmt::formatter<fgcd::DeviceName>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
    template<typename FormatContext>
    auto format(const fgcd::DeviceName& name, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "{}::{}", name.device_name, name.alias);
    }
};

// EOF
