//! @file
//! @brief  Log consumer that takes logs and print them to the given file.
//! @author Adam Solawa

#pragma once

#include <any>
#include <fmt/core.h>
#include <memory>
#include <optional>
#include <string>

#include <cpp_utils/circularBuffer.h>
#include <cpp_utils/enumStr.h>
#include <cpp_utils/misc.h>
#include <interfaces/IValueItem.h>
#include <modules/devices/fieldbus/inc/Enums.h>
#include "Enums.h"
#include "Response.h"

namespace fgcd
{
    class Command;
    using CommandPtr = std::unique_ptr<Command>;

    using ResponseQueue    = utils::CircularBuffer<CommandPtr>;
    using ResponseQueuePtr = std::shared_ptr<ResponseQueue>;

    class Command
    {
      public:
        Command(
            ResponseQueuePtr queue, std::any specific, CommandType type, std::string device, std::string tag, bool parse,
            std::string                property,      //
            std::optional<std::string> selector,      //
            std::optional<std::string> transaction,   //
            std::optional<std::string> filter,        //
            std::string                rest           //
        );

        [[nodiscard]] std::string                property() const;
        [[nodiscard]] std::optional<std::string> value() const;
        [[nodiscard]] std::optional<std::string> selector() const;
        [[nodiscard]] std::string                tag() const;
        [[nodiscard]] std::string                device() const;
        [[nodiscard]] std::string                source() const;
        [[nodiscard]] std::any                   specific() const;
        [[nodiscard]] bool                       parse() const;
        [[nodiscard]] CommandType                type() const;
        [[nodiscard]] bool                       ppm() const;
        [[nodiscard]] const Response&            response() const;

        static bool setResponse(CommandPtr command, Response response);

      private:
        struct
        {
            std::string                property;
            std::optional<std::string> selector;
            std::optional<std::string> transaction;
            std::optional<std::string> filter;
            std::optional<std::string> option;
            std::optional<std::string> value;
        } m_fields;
        Response         m_response;
        CommandType      m_type{};
        std::string      m_device;
        std::string      m_tag;
        std::string      m_source;
        std::any         m_specific;
        bool             m_parse = false;
        ResponseQueuePtr m_queue;
    };
}


template<>
struct fmt::formatter<fgcd::Command>
{
    template<typename FormatContext>
    auto format(const fgcd::Command& command, FormatContext& ctx)
    {
        // clang-format off
        return fmt::format_to(
                ctx.out(),
                "(tag: '{}', ppm: {}, parse: {}, type: {}, device: {}, property: '{}', selector: '{}', value: '{}')",
                command.tag(), command.ppm(), command.parse(), command.type(), command.device(), command.property(), command.selector(), command.value()
                );
        // clang-format on
    };

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// EOF
