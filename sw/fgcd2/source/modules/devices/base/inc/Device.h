//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <functional>
#include <memory>
#include <semaphore>

#include <cpp_utils/circularBuffer.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/thread/inc/Thread.h>
#include <modules/devices/programming/inc/types.h>
#include "Command.h"
#include "DeviceName.h"

namespace fgcd
{
    struct DeviceData
    {
        DeviceName name;
        int        class_id;
        Json       class_specific;
    };

    class Device;
    using DevicePtr = std::shared_ptr<Device>;

    using FinishCommand = std::function<void(CommandPtr command)>;

    class Device : public Component
    {
      public:
        Device(Component& parent, const DeviceData& data);

        void reprogram(const ReprogramData& data);
        void pushRterm(const std::string& message);
        void pushCommand(CommandPtr command);

        [[nodiscard]] DeviceName name() const;
        [[nodiscard]] int        classId() const;

      protected:
        void finishCommand();

      private:
        virtual void doExecuteCommand(CommandPtr command)   = 0;
        virtual void doReprogram(const ReprogramData& data) = 0;

        void worker(std::atomic_bool& keep_running);

        Thread                             m_thread;
        utils::CircularBuffer<CommandPtr>  m_commands;
        utils::CircularBuffer<std::string> m_rterm_messages;
        DeviceName                         m_device_name;
        int                                m_class_id;
        std::binary_semaphore              m_busy{1};
    };
}

// EOF
