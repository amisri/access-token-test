//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cpp_utils/enumStr.h>

namespace fgcd
{
    enum class CommandType
    {
        GET,
        SET,
        SUB,
        UNSUB
    };

    MAKE_ENUM_STR(
        CommandTypeStr, CommandType,              // NOLINT
        {CommandType::GET, "get", "get"},         //
        {CommandType::SET, "set", "set"},         //
        {CommandType::SUB, "sub", "sub"},         //
        {CommandType::UNSUB, "unsub", "unsub"},   //
    );
}
// EOF
