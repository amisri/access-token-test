//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>
#include <optional>
#include <string_view>

#include <modules/base/component/inc/Component.h>
#include "Device.h"

namespace fgcd
{
    class DeviceInventory;
    using DeviceInventoryPtr = std::shared_ptr<DeviceInventory>;

    class DeviceInventory : public Component
    {
      public:
        DeviceInventory(Component& parent);
        void                     addDevice(DeviceName name, DevicePtr device);
        std::optional<DevicePtr> find(DeviceName name);
        std::optional<DevicePtr> find(std::string_view name);

      private:
        std::map<DeviceName, DevicePtr> m_list{};
    };
}


// EOF
