//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <interfaces/ICommandDistributor.h>
#include <modules/base/component/inc/Component.h>
#include "Command.h"
#include "DeviceInventory.h"

namespace fgcd
{
    class CommandDistributor;
    using CommandDistributorPtr = std::shared_ptr<CommandDistributor>;

    class CommandDistributor : public Component, public ICommandDistributor
    {
        static auto constexpr timeout = 15s;

      public:
        CommandDistributor(Component& parent, DeviceInventoryPtr inventory);

        bool pushCommand(CommandPtr command) override;

      private:
        DeviceInventoryPtr m_inventory;
    };
}

// EOF
