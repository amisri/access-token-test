//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <filesystem>
#include <json/json.hpp>
#include <map>
#include <memory>

#include <cpp_utils/circularBuffer.h>
#include <modules/base/component/inc/Component.h>
#include <modules/devices/programming/inc/ReprogramTask.h>
#include <modules/devices/programming/inc/ReprogrammingManager.h>
#include <modules/devices/programming/inc/types.h>
#include "DeviceInventory.h"
#include "PlatformManagerInventory.h"

namespace fgcd
{
    class NameFileReader;
    using NameFileReaderPtr = std::shared_ptr<NameFileReader>;

    class NameFileReader : public Component
    {
        using Binaries = std::map<ReprogramEndpoint, std::filesystem::path>;
        using Tasks    = std::map<ReprogramEndpoint, ReprogramTaskPtr>;
        using Json     = nlohmann::json;

      public:
        NameFileReader(
            Component& parent, PlatformManagerInventory& platform_inventory, ReprogrammingManager& reprogramming,
            DeviceInventory& device_inventory
        );
        void loadConfig();

      private:
        void onStart() override;

        PlatformManagerInventory& m_platform_inventory;
        ReprogrammingManager&     m_reprogramming;
        DeviceInventory&          m_device_inventory;
        Binaries                  m_binaries{};
        Tasks                     m_tasks{};
    };
}

// EOF
