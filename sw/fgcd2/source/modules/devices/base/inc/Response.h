//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <fmt/format.h>
#include <variant>

#include <cpp_utils/enumStr.h>
#include <cpp_utils/misc.h>
#include <cpp_utils/typeTraits.h>
#include <interfaces/IValueItem.h>
#include "Timestamp.h"

namespace fgcd
{
    struct CommandResponse
    {
        Timestamp    timestamp;
        IValueVector values;
    };

    struct PublicationResponse
    {
        Timestamp    timestamp;
        IValueVector values;
    };

    struct ErrorResponse
    {
        int         error_code;
        std::string message;
    };

    using Response = std::variant<std::monostate, CommandResponse, PublicationResponse, ErrorResponse>;
}

// EOF
