//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <functional>
#include <string>

#include <interfaces/IValueItem.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/fieldbus/inc/PublicationPacket.h>

namespace fgcd
{
    struct Subscription
    {
        int         device_id = 0;
        std::string selector;
        std::string property;

        auto operator<=>(const Subscription&) const = default;
    };

    struct Publication
    {
        const Subscription          subscription = {};
        IValueItemPtr               value        = {};
        fieldbus::PublicationPacket publication  = {};
    };
}

// EOF
