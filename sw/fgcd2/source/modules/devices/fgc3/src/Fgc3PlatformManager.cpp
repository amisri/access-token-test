//! @file
//! @brief
//! @author Adam Solawa

#include <cstdint>
#include <functional>
#include <utility>

#include <cpp_utils/text.h>
#include <modules/base/core/inc/Exception.h>
#include <modules/base/logging/inc/LogEntry.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/logging/inc/StdOutLogger.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/CommandDistributor.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/base/inc/DeviceInventory.h>
#include <modules/devices/base/inc/Subscription.h>
#include <modules/devices/programming/inc/ReprogrammingManager.h>
#include <modules/net/ethernet/inc/EthernetHeader.h>
#include "../inc/Device.h"
#include "../inc/Fgc3Address.h"
#include "../inc/Fgc3PlatformManager.h"
#include "../inc/Fgc3Socket.h"
#include "../inc/PODs.h"
#include "../inc/ResponsePacket.h"
#include "../inc/StatusPacket.h"

using namespace fgcd;

// **********************************************************

Fgc3PlatformManager::Fgc3PlatformManager(
    Component& parent, std::string_view interface_name, ReprogrammingManager& reporgram
)
    : Component(parent, "Fgc3PlatformManager"),
      m_socket(*addComponent<Fgc3Socket>(interface_name)),
      m_timer(*addComponent<TimerManager>()),
      m_code_distributor(*addComponent<fieldbus::CodeDistributor>()),
      m_reprogram(reporgram)
{
    m_socket.setListener(std::bind_front(&Fgc3PlatformManager::routePacket, this));

    m_timer.registerTask<0>(
        [this](auto)
        {
            // m_time_factory->next();
            statusPing();
        }
    );

    m_timer.registerTask<ms_reprogram>(
        [this](auto)
        {
            m_reprogram.execute();
        }
    );

    m_timer.registerTask<ms_timepacket_1 - 1>(
        [this](auto)
        {
            m_code_distributor.publishCode(m_time_packet, m_time_packet);
            m_time_packet.tick();
        }
    );

    m_timer.registerTask<ms_timepacket_1>(
        [this](auto)
        {
            m_time_packet.setTime(m_clock.now());

            auto frame = m_socket.getFrame(fgc3::PayloadType::time);
            m_time_packet.serialize(frame);

            m_socket.send(frame);
        }
    );

    m_timer.registerTask<ms_timepacket_2>(
        [this](auto)
        {
            m_time_packet.setTime(m_clock.now());

            auto frame = m_socket.getFrame(fgc3::PayloadType::time);
            m_time_packet.serialize(frame);

            m_socket.send(frame);
        }
    );
}

// **********************************************************

DevicePtr Fgc3PlatformManager::createDevice(DeviceName name, int class_id, Json json_data)
{
    DeviceData data;
    data.class_id       = class_id;
    data.name           = name;
    data.class_specific = json_data;

    auto device = addComponent<fgc3::Device>(data, m_code_distributor, m_command_packet, m_socket);

    m_devices.push_back(device);

    return device;
}

// **********************************************************

bool Fgc3PlatformManager::manages(int class_id)
{
    return class_id == 63;
}

// **********************************************************

void Fgc3PlatformManager::statusPing()
{
    for (auto& device : m_devices)
    {
        device->tick();
    }
}

// **********************************************************

void Fgc3PlatformManager::routePacket(BigReader frame, MacAddress source)
{
    auto device_id = Fgc3Address::getDeviceId(source);

    auto device = std::ranges::find(m_devices, device_id, get_device_id);
    if (device == m_devices.end())
    {
        logTrace("Get packet for not avaiable device {}", device_id);
        return;
    }

    auto header = frame.read<fgc3::FgcEtherHeader>();
    if (!header.hasValue())
    {
        logError("Cannot deserialize packet header: '{}'", header.error());
        return;
    }

    switch (header->type)
    {
        case fgc3::PayloadType::status:
        {
            auto packet = fgc3::StatusPacket::fromBytes(frame);
            if (packet.hasError())
            {
                logError("Cannot deserialize status packet: '{}'", packet.error());
                return;
            }

            (*device)->handle(*packet);
        }
        break;

        case fgc3::PayloadType::response:
        case fgc3::PayloadType::publication:
        {
            auto packet = fgc3::ResponsePacket::fromBytes(frame);
            if (packet.hasError())
            {
                logError("Cannot deserialize response packet: {}", packet.error());
                return;
            }

            (*device)->handle(*packet);
        }
        break;

        default:
            logError("Recevied unknown payload type: '{}'", header->type);
    }
}

// EOF
