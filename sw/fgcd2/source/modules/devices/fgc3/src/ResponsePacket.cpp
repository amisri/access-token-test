//! @file
//! @brief  
//! @author Adam Solawa

#include <cstdint>
#include <utility>
#include <fmt/core.h>

#include <cpp_utils/Result.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/fieldbus/inc/PODs.h>
#include <modules/devices/fieldbus/inc/ResponsePacket.h>
#include "../inc/ResponsePacket.h"

using namespace fgcd::fgc3;

// **********************************************************

ResponsePacket::ResponsePacket(fieldbus::ResponsePacket packet)
    : fieldbus::ResponsePacket(std::move(packet))
{
}

// EOF
