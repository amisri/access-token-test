//! @file
//! @brief  
//! @author Adam Solawa

#include <string_view>

#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/net/ethernet/inc/EthernetFrame.h>
#include <modules/net/ethernet/inc/EthernetHeader.h>
#include <modules/net/ethernet/inc/Socket.h>
#include "../inc/Fgc3Socket.h"
#include "../inc/PODs.h"

using namespace fgcd;

// **********************************************************

Fgc3Socket::Fgc3Socket(Component& parent, std::string_view interface_name)
    : Component(parent, "Fgc3Socket"),
      m_socket(*addComponent<Socket>(interface_name, ethernet_type))
{
}

// **********************************************************

void Fgc3Socket::setListener(Socket::SocketListener listener)
{
    m_socket.setListener(std::move(listener));
}

// **********************************************************

EthernetFrame Fgc3Socket::getFrame(fgc3::PayloadType type, std::uint8_t device_id)
{
    EthernetFrame frame;

    if(device_id == 0)
    {
        frame = m_socket.getFrame();
    }
    else
    {
        frame = m_socket.getFrame(MacAddress({2, 0, 0, 0, 0, device_id}));
    }

    auto result = frame.write(   //
        fgc3::FgcEtherHeader{.type = type}
    );

    if (!result)
    {
        logError("Cannot write FgcEther header into the frame");
    }

    return frame;
}

// **********************************************************

void Fgc3Socket::send(const EthernetFrame& frame)
{
    m_socket.send(frame);
}

// EOF
