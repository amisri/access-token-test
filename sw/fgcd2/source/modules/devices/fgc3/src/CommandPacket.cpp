//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <cstdint>
#include <iterator>

#include "../inc/CommandPacket.h"

using namespace fgcd::fgc3;

// **********************************************************

void CommandPacket::clear()
{
    m_payload.clear();
}

// **********************************************************

std::size_t CommandPacket::doSetCommandBuffer(std::string_view payload)
{
    auto copy_bytes = std::min(max_payload_size, payload.size());

    m_payload.reserve(max_payload_size);
    std::copy_n(payload.begin(), copy_bytes, std::back_inserter(m_payload));


    m_length = sizeof(m_header) + m_payload.size();

    if (copy_bytes < min_payload_size)
    {
        m_payload.resize(min_payload_size);
    }

    return copy_bytes;
}

// **********************************************************

std::uint16_t CommandPacket::getLength() const
{
    return m_length;
}

// EOF
