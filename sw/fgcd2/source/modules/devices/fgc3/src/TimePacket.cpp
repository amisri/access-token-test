//! @file
//! @brief
//! @author Adam Solawa

#include <algorithm>
#include <cstdint>
#include <cstring>

#include <cpp_utils/misc.h>
#include <modules/devices/fieldbus/inc/FgcCode.h>
#include "../inc/TimePacket.h"

using namespace fgcd::fgc3;

// **********************************************************

std::size_t TimePacket::doDistributeCode(const fieldbus::FgcCode& code, std::uint16_t block_index)
{
    auto body  = code.getCodeBlocks(block_index, code_blocks_per_message);
    if(body.empty())
    {
        return 0;
    }

    codes = {};
    std::memcpy(codes.data(), body.data(), body.size());

    return code_blocks_per_message;
}

// EOF
