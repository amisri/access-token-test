//! @file
//! @brief  
//! @author Adam Solawa

#include <cstdint>

#include "../inc/Fgc3Address.h"

using namespace fgcd;

// **********************************************************

MacAddress Fgc3Address::getDeviceAddress(std::uint8_t device_id)
{
    return MacAddress({2, 0, 0, 0, 0, device_id});
}

// **********************************************************

std::uint8_t Fgc3Address::getDeviceId(MacAddress mac)
{
    return mac[5];
}

// EOF
