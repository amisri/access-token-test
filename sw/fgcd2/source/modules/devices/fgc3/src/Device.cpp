//! @file
//! @brief  
//! @author Adam Solawa

#include <cstdint>

#include <cpp_utils/misc.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/fieldbus/inc/CodeDistributor.h>
#include <modules/devices/fieldbus/inc/FieldbusDevice.h>
#include "../inc/CommandPacket.h"
#include "../inc/Device.h"
#include "../inc/Fgc3Socket.h"

using namespace fgcd::fgc3;

// **********************************************************

Device::Device(
    Component& parent, const DeviceData& data, fieldbus::CodeDistributor& code_distributor,
    CommandPacket& command_packet, Fgc3Socket& socket
)
    : FieldbusDevice(parent, data, code_distributor, command_packet),
      m_socket(socket),
      m_command_packet(command_packet)
{
}

// **********************************************************

void Device::doSendCommand()
{
    utils::Finally cleanup(
        [this]()
        {
            m_command_packet.clear();
        }
    );

    auto frame = m_socket.getFrame(PayloadType::command, getDongleId());
    if (auto result = m_command_packet.serialize(frame);   //
        !result.hasValue())
    {
        logWarning("Fail to serialize command packet: {}", result.error());
        return;
    }

    m_socket.send(frame);
}

// EOF
