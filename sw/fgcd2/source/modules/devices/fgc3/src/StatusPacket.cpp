//! @file
//! @brief  
//! @author Adam Solawa

#include <string>

#include <cpp_utils/Result.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/fieldbus/inc/PODs.h>
#include <modules/devices/fieldbus/inc/StatusPacket.h>
#include "../inc/StatusPacket.h"

using namespace fgcd::fgc3;

// **********************************************************

utils::Result<StatusPacket, std::string> StatusPacket::fromBytes(BigReader& frame)
{
    auto status = frame.read<fieldbus::StatusPacketPOD>();

    if (!status)
    {
        return utils::Failure{status.error()};
    }

    auto remote_terminal = frame.read<RTerm>();
    if (!remote_terminal)
    {
        return utils::Failure{remote_terminal.error()};
    }

    auto reserved = frame.readContainer<std::vector<std::byte>>(3);
    if (!reserved)
    {
        return utils::Failure{"Cannot deserialize reserved"};
    }

    return StatusPacket(*status, *remote_terminal);
}

// **********************************************************

std::string StatusPacket::doGetRemoteTerminal() const
{
    auto len = std::strlen(m_remote_terminal.data());
    return {m_remote_terminal.data(), len};
}

// **********************************************************

StatusPacket::StatusPacket(fieldbus::StatusPacketPOD pod, RTerm rterm)
    : fieldbus::StatusPacket(pod),
      m_remote_terminal(rterm)
{
}

// EOF
