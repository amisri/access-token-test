//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/fieldbus/inc/CodePacket.h>
#include <modules/devices/fieldbus/inc/FgcCode.h>
#include <modules/devices/fieldbus/inc/TimePacket.h>
#include "PODs.h"

namespace fgcd::fgc3
{
    class TimePacket : public fieldbus::TimePacket, public fieldbus::CodePacket
    {
        static constexpr auto code_blocks_per_message = 32;
        static constexpr auto block_size              = 32;
        static constexpr auto codes_size              = code_blocks_per_message * block_size;

        using CodesArray = std::array<std::byte, codes_size>;

      public:
        auto serialize(auto&& archive)
        {
            return archive.write(getTime(), realtime, codes);
        }

      private:
        std::size_t doDistributeCode(const fieldbus::FgcCode& code, std::uint16_t block_index) override;

        RealTimePOD realtime;
        CodesArray  codes{};
    };
}

// EOF
