//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <string_view>

#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Command.h>
#include <modules/devices/fieldbus/inc/CommandPacket.h>

namespace fgcd::fgc3
{
    class CommandPacket : public fieldbus::CommandPacket
    {
        static constexpr auto max_payload_size = 1494UL;
        static constexpr auto min_payload_size = 44UL;

      public:
        auto serialize(auto&& frame)
        {
            return frame.write(m_length, m_header, m_payload);
        }

        void clear();

        [[nodiscard]] std::uint16_t getLength() const;

      private:
        std::size_t doSetCommandBuffer(std::string_view payload) override;

        std::uint16_t m_length;
        std::string   m_payload;
    };
}

// EOF

