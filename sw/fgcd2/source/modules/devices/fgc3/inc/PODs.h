//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstddef>

#include <cpp_utils/misc.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Command.h>

namespace fgcd::fgc3
{
    enum class PayloadType : std::uint8_t
    {
        time = 0,
        status,
        command,
        response,
        publication,
        inter_fgc,
        unknown_type,
        count
    };

    struct FgcEtherHeader
    {
        PayloadType type    = PayloadType::time;
        std::byte   padding = 0xFF_b;
    };

    // **********************************************************

    struct RealTimePOD
    {
        static constexpr auto references_count = 64;
        static constexpr auto default_value    = std::bit_cast<float>(0xFFFF'FFFF);
        using RealTimeArray                    = std::array<float, 64>;

        RealTimeArray references = {};
    };

}

// EOF
