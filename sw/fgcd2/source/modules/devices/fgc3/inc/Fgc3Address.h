//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>

#include <modules/net/ethernet/inc/EthernetHeader.h>


// **********************************************************

namespace fgcd
{
    struct Fgc3Address
    {
        static MacAddress   getDeviceAddress(std::uint8_t device_id);
        static std::uint8_t getDeviceId(MacAddress mac);
    };
}

// EOF
