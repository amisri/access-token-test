//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>

#include <modules/base/component/inc/Component.h>
#include <modules/base/core/inc/types.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/fieldbus/inc/CodeDistributor.h>
#include <modules/devices/fieldbus/inc/FieldbusDevice.h>
#include "CommandPacket.h"
#include "Fgc3Socket.h"

namespace fgcd::fgc3
{
    class Device;
    using DevicePtr = std::shared_ptr<Device>;

    class Device : public FieldbusDevice
    {
      public:
        Device(
            Component& parent, const DeviceData& data, fieldbus::CodeDistributor& code_distributor,
            CommandPacket& command_packet, Fgc3Socket& socket
        );

      private:
        void doSendCommand() override;

        Fgc3Socket&    m_socket;
        CommandPacket& m_command_packet;
    };
}

// EOF
