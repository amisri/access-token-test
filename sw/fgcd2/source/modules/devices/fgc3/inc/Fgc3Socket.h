//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <memory>
#include <string_view>

#include <modules/base/component/inc/Component.h>
#include <modules/net/ethernet/inc/EthernetFrame.h>
#include <modules/net/ethernet/inc/Socket.h>
#include "PODs.h"

namespace fgcd
{
    class Fgc3Socket;
    using Fgc3SocketPtr = std::shared_ptr<Fgc3Socket>;

    class Fgc3Socket : public Component
    {
        static constexpr std::uint16_t ethernet_type = 0x88B5;

      public:
        Fgc3Socket(Component& parent, std::string_view inteface_name);

        void          setListener(Socket::SocketListener listener);
        EthernetFrame getFrame(fgc3::PayloadType type, std::uint8_t device_id = 0);
        void          send(const EthernetFrame& frame);

      private:
        Socket& m_socket;
    };
}

// EOF
