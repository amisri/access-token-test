//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <cstdint>
#include <cpp_utils/Result.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/fieldbus/inc/ResponsePacket.h>

namespace fgcd::fgc3
{
    class ResponsePacket : public fieldbus::ResponsePacket
    {
        struct Header
        {
            std::uint16_t            length;
            fieldbus::ResponseHeader header;
        };

      public:
        static utils::Result<ResponsePacket, std::string> fromBytes(auto& frame)
        {
            auto fgc3_header = frame.template read<Header>();
            if (!fgc3_header)
            {
                return utils::Failure{"Header read error"};
            }

            auto payload        = frame.unreadData();
            auto payload_length = fgc3_header->length - sizeof(fieldbus::ResponseHeader);

            if (payload_length > payload.size())
            {
                return utils::Failure{
                    fmt::format("Payload length:{} is shorter than packet's length field :{}", payload.size(), payload_length)
                };
            }

            payload = payload.first(payload_length);

            return ResponsePacket({fgc3_header->header, payload});
        }


      private:
        ResponsePacket(fieldbus::ResponsePacket packet);
    };
}

// EOF
