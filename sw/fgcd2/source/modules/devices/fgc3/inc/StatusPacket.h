//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string>

#include <cpp_utils/Result.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/fieldbus/inc/PODs.h>
#include <modules/devices/fieldbus/inc/StatusPacket.h>

namespace fgcd::fgc3
{
    class StatusPacket : public fieldbus::StatusPacket
    {
        using RTerm = std::array<char, 21>;

      public:
        static utils::Result<StatusPacket, std::string> fromBytes(BigReader& frame);

      private:
        StatusPacket(fieldbus::StatusPacketPOD pod, RTerm rterm);

        [[nodiscard]]  std::string doGetRemoteTerminal() const override;

        RTerm m_remote_terminal;
    };
}

// EOF
