//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <json/json.hpp>
#include <string_view>
#include <vector>

#include <interfaces/IPlatformManager.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/misc/inc/BinaryReader.h>
#include <modules/devices/base/inc/CommandDistributor.h>
#include <modules/devices/base/inc/Device.h>
#include <modules/devices/base/inc/DeviceInventory.h>
#include <modules/devices/base/inc/Subscription.h>
#include <modules/devices/programming/inc/ReprogrammingManager.h>
#include <modules/net/ethernet/inc/Socket.h>
#include <modules/timing/inc/TimerManager.h>
#include "CommandPacket.h"
#include "Device.h"
#include "Fgc3Socket.h"
#include "TimePacket.h"

namespace fgcd
{
    class Fgc3PlatformManager : public Component, public IPlatformManager
    {
        static constexpr auto ms_reprogram    = 12;
        static constexpr auto ms_timepacket_1 = 18;
        static constexpr auto ms_timepacket_2 = 19;
        static constexpr auto get_device_id   = [](auto&& device)
        {
            return device->getDongleId();
        };

      public:
        Fgc3PlatformManager(Component& parent, std::string_view interface_name, ReprogrammingManager& reporgram);

        DevicePtr createDevice(DeviceName name, int class_id, Json json_data) override;
        bool      manages(int class_id) override;

      private:
        void statusPing();
        void routePacket(BigReader frame, MacAddress source);

        std::vector<fgc3::DevicePtr> m_devices  = {};

        Fgc3Socket&                m_socket;
        TimerManager&              m_timer;
        fieldbus::CodeDistributor& m_code_distributor;
        fgc3::TimePacket           m_time_packet;
        fgc3::CommandPacket        m_command_packet;
        ReprogrammingManager&      m_reprogram;
    };
}

// EOF
