#include <gtest/gtest.h>

#include "../inc/CommandPacket.h"
#include "cpp_utils/typeTraits.h"
#include "modules/devices/fgc3/inc/PODs.h"
#include "modules/devices/fieldbus/inc/ProtocolConfig.h"

using namespace fgcd::fgc3;

// **********************************************************
// Setup
// **********************************************************

struct TestWriter
{
    template<typename T>
    bool write(T&& object)
    {
        if constexpr(utils::is_iterable_v<T>)
        {
            size += object.size();
        }
        else
        {
            size += sizeof(object);
        }
        return true;
    }

    std::size_t size{0};
};

struct Fgc3CommandPacketTests : public ::testing::Test
{
    TestWriter writer{};
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(Fgc3CommandPacketTests, setPayload)
{
    // **********************************************************
    // Preparation
    CommandPacket packet{};
    std::string payload = "TEST.CHAR TEST1";


    // **********************************************************
    // Execution
    auto copied = packet.setPayload(payload);


    // **********************************************************
    // Verification
    EXPECT_EQ(copied, 15);
    EXPECT_EQ(packet.getLength(), 17);
}

// **********************************************************

TEST_F(Fgc3CommandPacketTests, serialize)
{
    // **********************************************************
    // Preparation
    CommandPacket packet{};
    std::string payload = "TEST.CHAR TEST1";


    // **********************************************************
    // Execution
    packet.setPayload(payload);
    packet.serialize(writer);


    // **********************************************************
    // Verification
    EXPECT_EQ(writer.size, fgcd::configs::fgc_ether.min_payload_length);
}

// EOF
