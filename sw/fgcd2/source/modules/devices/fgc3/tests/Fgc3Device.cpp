#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/Fgc3Device.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

class Fgc3DeviceTests : public ::testing::Test
{
  public:
    RootMockPtr root;
    Fgc3DevicePtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        // object = root->addComponent<Fgc3Device>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(Fgc3DeviceTests, Fgc3Device_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
