#include <gtest/gtest.h>

#include <modules/base/core/inc/types.h>
#include <modules/devices/fieldbus/inc/FgcCode.h>
#include <utils/test/utils/FgcCode.h>
#include <utils/test/utils/utils.h>
#include "../inc/TimePacket.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct TimePacketTests : public ::testing::Test
{
    fgc3::TimePacket packet{};
};

auto createExpectedPacket(ByteStreamView code_buffer)
{
    auto buffer = ByteStream(code_buffer.begin(), code_buffer.end());
    buffer.resize(1024);
    return buffer;
}

// **********************************************************
// Tests
// **********************************************************

// **********************************************************

TEST_F(TimePacketTests, setVarCodeReturnValue)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
}

// EOF
