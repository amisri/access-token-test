#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <utils/test/mocks/RootMock.h>
#include "../inc/StatusPacket.h"

using namespace fgcd;

// **********************************************************
// Setup
// **********************************************************

struct StatusPacketTests : public ::testing::Test
{
    RootMockPtr root;
    StatusPacketPtr object;

    void SetUp() override
    {
        root = std::make_unique<RootMock>();
        object = root->addComponent<StatusPacket>();
    }
};

// **********************************************************
// Tests
// **********************************************************

TEST_F(StatusPacketTests, StatusPacket_test1)
{
    // **********************************************************
    // Preparation


    // **********************************************************
    // Execution


    // **********************************************************
    // Verification
    EXPECT_EQ(true, false);
}

// EOF
