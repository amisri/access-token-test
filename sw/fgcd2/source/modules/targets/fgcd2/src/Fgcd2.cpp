//! @file
//! @brief
//! @author

#include <ctime>
#include <filesystem>
#include <fstream>
#include <regex>
#include <string>
#include <string_view>

#include <cpp_utils/text.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/logging/inc/StdOutLogger.h>
#include <modules/timing/inc/TimeSource.h>
#include "../inc/Fgcd2.h"

using namespace fgcd;

// **********************************************************

Fgcd2::Fgcd2()
    : m_timing(addComponent<TimeSource>())
{
    m_logger.registerConsumer(m_logger.addComponent<StdOutLogger>());
    m_logger.enableLogType<LogType::debug>(true);
}

// **********************************************************

void Fgcd2::loadEventFile(std::filesystem::path path, std::string_view hostname)
{
    if (!std::filesystem::exists(path))
    {
        logError("Event config path does not exist: {}", path);
        return;
    }

    std::ifstream file{path};
    if (file.is_open())
    {
        logInfo("File opened {}", path);
        std::string line;
        while (std::getline(file, line))
        {
            std::string regex_string = R"(^([^:]{1,19}):([^:]{1,255}):(\d+)$)";
            std::regex  format(regex_string);
            std::smatch matches;
            if (std::regex_search(line, matches, format))
            {

                auto&& name  = matches[1];
                auto&& event = matches[2];
                auto&& delay = matches[3];

                if (name == "MS")
                {
                    std::string event_name = event + "-" + utils::toUpperCopy(hostname);
                    logInfo("name {}, event {}, delay {}", name, event_name, delay);
                    m_timing->registerCallback(
                        event_name,
                        [this](std::timespec time)
                        {
                            m_clock.set(time);
                        }
                    );
                }
            }
            else
            {
                logWarning("Wrong format of line: {}", line);
            }
        }
    }
    else
    {
        logError("Cannot open file {}", path);
    }
}

// **********************************************************

TimeSourcePtr Fgcd2::getTiming()
{
    return m_timing;
}

// EOF
