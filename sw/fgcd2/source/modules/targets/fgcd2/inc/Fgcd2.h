//! @file
//! @brief
//! @author

#pragma once

#include <filesystem>
#include <string_view>

#include <modules/base/component/inc/RootComponent.h>
#include <modules/timing/inc/TimeSource.h>

namespace fgcd
{
    class Fgcd2 : public RootComponent
    {
      public:
        Fgcd2();
        void          loadEventFile(std::filesystem::path path, std::string_view hostname);
        TimeSourcePtr getTiming();

      private:
        TimeSourcePtr m_timing;
    };
}

// EOF
