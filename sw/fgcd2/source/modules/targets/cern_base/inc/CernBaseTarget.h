//! @file
//! @brief  Root component for the generic FEC class at CERN.
//! @author Dariusz Zielinski

#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <modules/base/component/inc/RootComponent.h>

namespace fgcd
{
    class CernBaseTarget : public RootComponent
    {
        public:
            CernBaseTarget();

    };

}

// EOF