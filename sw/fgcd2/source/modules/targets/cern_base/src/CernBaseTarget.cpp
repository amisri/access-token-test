//! @file
//! @brief  Root component for the generic FEC class at CERN.
//! @author Dariusz Zielinski

#include <modules/base/logging/inc/StdOutLogger.h>
#include "../inc/CernBaseTarget.h"


using namespace fgcd;

// **********************************************************

CernBaseTarget::CernBaseTarget() : RootComponent()
{
    m_logger.registerConsumer(m_logger.addComponent<StdOutLogger>());
}

// EOF
