#pragma once

#include <chrono>
#include <cstddef>
#include <fmt/core.h>
#include <fmt/format.h>
#include <future>
#include <gtest/gtest.h>
#include <ostream>
#include <utility>

#include "modules/net/websocket/inc/CommandParser.h"
#include <cpp_utils/Result.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/misc/inc/FmtFormatters.h>
#include <modules/net/websocket/inc/ParserCombinator.h>

using namespace fgcd;

// **********************************************************

// This is base on https://github.com/google/googletest/discussions/4121

// NOTE: This needs to be added to first test printing given type.
// Otherwise, gtest instantiate generic printer, which shadows this printers.

namespace testing::internal
{
    template<>
    class UniversalPrinter<TimePoint>
    {
      public:
        static void Print(const TimePoint& value, ::std::ostream* stream)
        {
            using namespace std::chrono;

            auto duration    = value.time_since_epoch();

            auto second      = duration_cast<seconds>(duration % minutes(1));
            auto millisecond = duration_cast<milliseconds>(duration % seconds(1));
            auto microsecond = duration_cast<microseconds>(duration % milliseconds(1));

            *stream << fmt::format("{} {} {}", second, millisecond, microsecond);
        }
    };

    // **********************************************************

    template<>
    class UniversalPrinter<std::future_status>
    {
      public:
        static void Print(const std::future_status& value, ::std::ostream* stream)
        {
            if (value == std::future_status::ready)
            {
                *stream << "ready";
            }
            else if (value == std::future_status::timeout)
            {
                *stream << "timeout";
            }
            else if (value == std::future_status::deferred)
            {
                *stream << "deferred";
            }
        }
    };

    // **********************************************************

    template<>
    class UniversalPrinter<std::byte>
    {
      public:
        static void Print(const std::byte& value, ::std::ostream* stream)
        {
            *stream << fmt::format("{:#04x}", value);
        }
    };

    // **********************************************************

    template<>
    class UniversalPrinter<std::chrono::milliseconds>
    {
      public:
        static void Print(const std::chrono::milliseconds& value, ::std::ostream* stream)
        {
            *stream << fmt::format("{}", value.count());
        }
    };

    // **********************************************************

    template<typename T, typename U>
    class UniversalPrinter<std::pair<T, U>>
    {
      public:
        static void Print(const std::pair<T, U>& value, ::std::ostream* stream)
        {
            *stream << fmt::format("{},{}", value.first, value.second);
        }
    };

    // **********************************************************

    template<typename T, typename E>
    class UniversalPrinter<utils::Result<T, E>>
    {
      public:
        static void Print(const utils::Result<T, E>& value, ::std::ostream* stream)
        {
            if(value.hasValue())
            {
                *stream << fmt::format("Value = {}", value.value());
            }
            else
            {
                *stream << fmt::format("Error = {}", value.error());
            }
        }
    };

    // **********************************************************

    template<>
    class UniversalPrinter<fgcd::ParserError>
    {
      public:
        static void Print(const fgcd::ParserError& value, ::std::ostream* stream)
        {
            *stream << fmt::format("{}", value);
        }
    };

    // **********************************************************

    template<typename T>
    class UniversalPrinter<fgcd::ParserState<T>>
    {
      public:
        static void Print(const fgcd::ParserState<T>& value, ::std::ostream* stream)
        {
            *stream << fmt::format("{}", value);
        }
    };
    
    // **********************************************************

    template<typename E>
    class UniversalPrinter<utils::Failure<E>>
    {
      public:
        static void Print(const utils::Failure<E>& value, ::std::ostream* stream)
        {
            *stream << fmt::format("Failure = {}", value.value());
        }
    };
}

// **********************************************************

template<>
struct fmt::formatter<fgcd::CommandFields>
{
    template<typename FormatContext>
    auto format(const fgcd::CommandFields& object, FormatContext& ctx)
    {
        return fmt::format_to(
            ctx.out(),                                                                         //
            "(property: '{}', selector: '{}', transaction: '{}', filter: '{}', rest: '{}')",   //
            object.property, object.selector, object.transaction, object.filter, object.rest
        );
    }

    template<typename ParseContext>
    auto parse(ParseContext& ctx)
    {
        return ctx.begin();
    }
};

// EOF
