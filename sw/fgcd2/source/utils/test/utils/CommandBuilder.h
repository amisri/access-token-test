#pragma once

#include <modules/devices/base/inc/Command.h>
#include <modules/devices/base/inc/Enums.h>

#include <memory>
#include <utility>

using namespace fgcd;

struct CommandBuilder
{
  private:
    ResponseQueue              m_queue;
    CommandType                m_type = CommandType::GET;
    std::string                m_property;
    std::optional<std::string> m_selector;
    std::string                m_value;
    bool                       m_parse = false;

  public:
    CommandBuilder& setType(CommandType type) &
    {
        m_type = type;
        return *this;
    }

    CommandBuilder& setProperty(std::string property) &
    {
        m_property = std::move(property);
        return *this;
    }

    CommandBuilder& setSelector(std::optional<std::string> selector) &
    {
        m_selector = std::move(selector);
        return *this;
    }

    CommandBuilder& setValue(std::string value) &
    {
        m_value = std::move(value);
        return *this;
    }

    CommandBuilder& setParse(bool parse) &
    {
        m_parse = parse;
        return *this;
    }

    CommandPtr build() &
    {
        std::string specific;
        std::string device;
        std::string tag;
        std::string transaction;
        std::string filter;

        return std::make_unique<Command>(
            m_queue, specific, m_type, device, tag, m_parse,       //
            m_property, m_selector, transaction, filter, m_value   //
        );
    }

    ResponseQueue& queue() &
    {
        return m_queue;
    }
};
