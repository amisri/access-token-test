#pragma once

#include <cassert>
#include <cstddef>
#include <fmt/color.h>
#include <map>

#include <modules/base/core/inc/types.h>

// **********************************************************

using SparseDefinition = std::map<fgcd::ByteStream::size_type, std::byte>;

template<std::size_t length, std::byte init = std::byte(0)>
fgcd::ByteStream createBytesVector(const SparseDefinition& definition)
{
    auto vector = fgcd::ByteStream(length, init);

    for (auto&& [index, value] : definition)
    {
        assert(index < vector.size());
        vector[index] = value;
    }

    for (auto i = 1; i < length; i++)
    {
        if (definition.contains(i))
        {
            vector[i] = definition.at(i);
        }
        else
        {
            vector[i] = vector[i - 1];
        }
    }

    return vector;
}

// **********************************************************

// Used this implementation: https://brevzin.github.io/c++/2020/03/30/span-comparisons

#include <algorithm>
#include <compare>
#include <concepts>
#include <ranges>
#include <span>

namespace span_ext
{
    using std::convertible_to;
    using std::equality_comparable;
    using std::invocable;
    using std::same_as;
    using std::three_way_comparable;

    using std::contiguous_iterator;
    using std::ranges::begin;
    using std::ranges::contiguous_range;
    using std::ranges::end;
    using std::ranges::range_value_t;

    inline constexpr auto synth_three_way = []<class T>(const T& t, const T& u)
        requires requires {
                     {
                         t < u
                         } -> convertible_to<bool>;
                     {
                         u < t
                         } -> convertible_to<bool>;
                 }
    {
        if constexpr (three_way_comparable<T>)
        {
            return t <=> u;
        }
        else
        {
            if (t < u)
                return std::weak_ordering::less;
            if (u < t)
                return std::weak_ordering::greater;
            return std::weak_ordering::equivalent;
        }
    };

    template<typename T>
    concept synth_comparable = invocable<decltype(synth_three_way), T, T>;

    template<typename T, typename U>
    concept sameish = same_as<std::remove_cvref_t<T>, std::remove_cvref_t<U>>;

    template<typename R, typename T>
    concept contiguous_range_of = contiguous_range<R const> && sameish<T, range_value_t<R const>>;

    template<typename T>
    concept memcmp_friendly = sameish<T, std::byte> || sameish<T, unsigned char>;

    template<typename InputIter1, typename InputIter2>
    auto lexicographical_compare_three_way(InputIter1 first1, InputIter1 last1, InputIter2 first2, InputIter2 last2)
        -> decltype(synth_three_way(*first1, *first2))
    {
        return std::lexicographical_compare_three_way(first1, last1, first2, last2, synth_three_way);
    }
}

namespace std
{
    template<span_ext::equality_comparable T, size_t E, span_ext::contiguous_range_of<T> R>
    constexpr bool operator==(span<T, E> lhs, R const& rhs)
    {
        return ::std::equal(lhs.begin(), lhs.end(), span_ext::begin(rhs), span_ext::end(rhs));
    }

    template<span_ext::synth_comparable T, size_t E, span_ext::contiguous_range_of<T> R>
    constexpr auto operator<=>(span<T, E> lhs, R const& rhs)
    {
        return span_ext::lexicographical_compare_three_way(
            lhs.begin(), lhs.end(), span_ext::begin(rhs), span_ext::end(rhs)
        );
    }
}

// **********************************************************

inline ::testing::AssertionResult
vectors_match(const char* lhs_expr, const char* rhs_expr, fgcd::ByteStreamView lhs, fgcd::ByteStreamView rhs)
{

    std::ostringstream os;
    os << "Legend:\n";
    os << fmt::format(fg(fmt::color::white), "lhs[i] ; lhs[i] == rhs[i]\n");
    os << fmt::format(fg(fmt::color::red), "lhs[i] ; lhs[i] != rhs[i]\n");
    os << fmt::format(fg(fmt::color::gray), "lhs[i] ; i >= rhs.size()\n");
    os << fmt::format(fg(fmt::color::yellow), "rhs[i] ; i >= lhs.size()\n");
    os << "\n";
    os << "Vectors are not equal\n";
    os << fmt::format("\t {} != {}\n", lhs_expr, rhs_expr);
    os << "Vector sizes are:\n";
    os << fmt::format("\t {}    {}\n", lhs.size(), rhs.size());
    bool match = true;

    for (std::size_t i = 0; i < lhs.size(); i++)
    {
        if (i % 16 == 0)
        {
            os << fmt::format("\n0x{:x}\t", i);
        }
        else if (i % 8 == 0)
        {
            os << " ";
        }

        if (i >= rhs.size())
        {
            match = false;
            os << fmt::format(fg(fmt::color::gray), "{:02x} ", lhs[i]);
        }
        else if (lhs[i] != rhs[i])
        {
            match = false;
            os << fmt::format(fg(fmt::color::red), "{:02x} ", lhs[i]);
        }
        else
        {
            os << fmt::format(fg(fmt::color::white), "{:02x} ", lhs[i]);
        }
    }

    for (auto i = lhs.size(); i < rhs.size(); i++)
    {
        match = false;

        if (i % 16 == 0)
        {
            os << fmt::format("\n0x{:x}\t", i);
        }
        else if (i % 8 == 0)
        {
            os << " ";
        }

        os << fmt::format(fg(fmt::color::yellow), "{:02x} ", rhs[i]);
    }

    if (!match)
    {
        return ::testing::AssertionFailure() << os.str();
    }

    return ::testing::AssertionSuccess() << "koala";
}

#define EXPECT_BINARY_EQ(val1, val2) EXPECT_PRED_FORMAT2(vectors_match, val1, val2)
#define ASSERT_BINARY_EQ(val1, val2) ASSERT_PRED_FORMAT2(vectors_match, val1, val2)

// **********************************************************

constexpr auto operator"" _times(unsigned long long n)
{
    return [n](auto f)
    {
        for (auto i = 0ull; i < n; i++)
        {
            f();
        }
    };
}


// EOF
