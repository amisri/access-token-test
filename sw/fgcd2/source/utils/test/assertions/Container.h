#include <algorithm>
#include <cstddef>
#include <gmock/gmock.h>

MATCHER_P(IsContainerEqual, expected, "") {
    auto result = (expected == arg);
    if (not result)
    {
        auto [arg_mismatch, expected_mismatch] = std::mismatch(arg.begin(), arg.end(), expected.begin());
        auto mismatch_index = arg_mismatch - arg.begin();
        *result_listener << "actual size " << arg.size() << ", ";
        if (arg_mismatch == arg.end())
        {
            *result_listener << "actual container is to short";
        }
        else
        {
            *result_listener << "element #" << mismatch_index << " - " << ::testing::PrintToString(*arg_mismatch) << " doesn't match";
        }
    }

    return result;
}
