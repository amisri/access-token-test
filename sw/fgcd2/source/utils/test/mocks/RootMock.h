#pragma once

#include <memory>

#include <modules/base/clock/inc/utils.h>
#include <modules/base/component/inc/RootComponent.h>
#include <modules/base/core/inc/types.h>
#include <modules/base/logging/inc/Logger.h>
#include <modules/base/logging/inc/StdOutLogger.h>

struct RootMock : public fgcd::RootComponent
{
    using fgcd::RootComponent::RootComponent;

    void enableLogging()
    {
        m_logger.registerConsumer(m_logger.addComponent<fgcd::StdOutLogger>());
        m_logger.enableLogType<fgcd::LogType::debug>(true);
        m_logger.enableLogType<fgcd::LogType::trace>(true);
    }
};

using RootMockPtr = std::shared_ptr<RootMock>;

// EOF
