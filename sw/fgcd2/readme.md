# FGCDv2

### Directory structure

- cmake - cmake macros, compilation profiles, external libraries definitions,
          and other building system configuration files.
- docs - additional documentation for the project, including C++ code style document.
- scripts - scripts and commands used to manage, deploy, develop and test the code.
- source - C++ source of the project
  - interfaces - inter-module interfaces declarations.
  - modules - source code of modules and their unit tests.
  - targets - definitions of executables crated based on the project modules.


### Building and managing the project

There are dedicated commands you can execute to build, rebuild, clean, unit test,
deploy, generate documentation, perform static analysis and more.  

To run a command type in the terminal *"./fgcd.sh"* - this will display help with the list
of available commands and their description. You can always add "-h" to read help
for a given command, e.g. *"./fgcd.sh build -h"*.
