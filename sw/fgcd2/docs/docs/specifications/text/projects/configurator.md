# FGC4 Configurator specification

The purpose of this document is to describe the working of the FGC4 Configurator as well as listing all the features it provides.

## Current version

0.1

### Purpose

This document specifies the scope and most important features of the FGC4 Configurator project. The configurator is 
used by the EPC experts to set valus of configuration parameters and define the composition of their systems. 
It work at the level of types (that act like templates) and at the level of specific instances (devices).

### Scope

TBD

### Features

TBD

## API

The FGC4 Configurator API is a RESTful API that allows the user to interact with the FGC4 Configurator.

### API Endpoints

TBD

### API Methods

TBD

### API Responses

TBD

## Examples

TBD
 
