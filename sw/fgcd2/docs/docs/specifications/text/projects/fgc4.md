# FGC4 features specification

## Current version

0.1


## Purpose

This document describes, constrains and specifies some of the most fundamental features provided by the FGC4 device.


## Hardware architecture

CIB, SIB, other cards, FMS, local and remote cards, cards addressing/indexing
backpanels


## Infrastructure integration

what cables are connected and where, commands, timing, events, PLCs


## System architecture

core assignment, types of payloads


## Booting sequence

data storage, partitions, kernel, patches, device tree descriptors, user space / distribution


## Logical architecture

FEC-like main device, tenants, subdevices, reprogrammable subsystems


## Multitenancy

constrains and scope



