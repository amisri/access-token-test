# WEF (Websockets for EPC FEC) protocol

:::{admonition} This is a working version of the protocol.
:class: danger

A certain aspects require further clarification and design:

- What should be the format of the STATUS response data?

- How does remote terminal work? Does it need to be opened (/closed)?

- Is RBAC authorisation required to access the remote terminal?

:::


## Current version

0.9.2


## Purpose

The purpose of this protocol is to provide a communication channel with the EPC
Front-End Computers, mainly from EPC scripts and web tools (either directly or via 
the FGC API), as opposed to the RDA3 protocol which is used to communicate with the 
CERN control system. Having a dedicated protocol allows to incorporate all custom and
specific features, independently from what is supported by the more generic CERN control
system. Basing the protocol on Websocket allows easy use from the web browsers and 
scripts. This protocol replaces previously used TCP-based protocol.


## Protocol features

- Stateful
- Messaged-based with Request-Response and Pub-Sub types of communication 
- JSON-encoded payload
- Authentication support
- Asynchronous

## Messages

### Message-based protocol

Every communication to or from server is based on messages, with the format specified below.
No other data (e.g., binary) is sent.

Messages can be divided into two types:
- **Request** - any message from a client to the server.
- **Response** - any message from the server to a client. A response message may be sent to clients
even without corresponding request message from a client (e.g., subscriptions).

Communication is fully asynchronous, meaning:
- Any number of requests may be sent, before any response is received.
- The responses may arrive in any order (not necessarily matching the requests order - tag-based matching is needed).
- The responses may arrive even without client requesting them.



### Types of request messages

INIT
: This message has to be sent by a client as the first message after the connection is established. 
Sending any other request before this one will be rejected. With this message, the client informs the
server about the protocol version it implements and any selected options.\
Optionally, the client may provide user's credentials (e.g., RBAC token) for authentication.
Authentication is required to use `GET`, `GETEX`, `SET`, `SETEX`, `SUB`, and `SUBEX` requests.\
This request is matched with a corresponding response from the server confirming successful initialization.
Before the confirmation is received by the client, it should not send any other requests. 

CLEAR
: This message is used to change the user, while reusing the already opened and initialized connection. 
As the result of this request all active subscriptions are removed and any other protocol state is restarted, 
as if it was just initialized. The client may provide new user's credentials for authentication.\
This request is matched with a corresponding response from the server confirming the clearing and optional 
authentication.  

GET
: This message is used to send a *GET* command to a device using the compact [command string](#command-string).
A *RESPONS* response will be sent by the server when the command is executed.

GETEX
: This message offers the same functionality and behaviour as `GET` request, but allows to send 
data (e.g., property, cycle selector, filters, transaction ID, etc.) explicitly in the payload,
instead of using the compact [command string](#command-string).

SET
: This message is used to send a *SET* command to a device using the compact [command string](#command-string).
A *RESPONSE* response will be sent by the server when the command is executed.

SETEX
: This message offers the same functionality and behaviour as `SET` request, but allows to send 
data (e.g., property, cycle selector, filters, transaction ID, etc.) explicitly in the payload,
instead of using the compact [command string](#command-string).

SUB
: This message is used to send a subscription command to a device  using a compact [command string](#command-string). 
A `RESPONSE` response will be sent by the server when the command is executed. If the execution 
is successful (i.e., there is no error) the server will also send a `NOTIFICATION` response, 
just after subscribing and on every subsequent property value change. There will be one `NOTIFICATION`
response per each cycle selector. 

SUBEX
: This message offers the same functionality and behaviour as `SUB` request, but allows to send 
data (e.g., property, cycle selector, filters, transaction ID, etc.) explicitly in the payload,
instead of using the compact [command string](#command-string).

UNSUB
: This message is used to unsubscribe. If the subscription tag is empty, it will unsubscribe from all
active subscriptions at once. A matching `NOTIFICATION` response will be sent by the server as a confirmation. 

TERM
: This message is used to send a character to a device to implement the *remote terminal* functionality.


### Types of response messages


INIT
: Sent to the client after corresponding `INIT` request. It informs client about a successful 
initialization or an error. If the client provided user's credentials, this response will also
serve as a conformation of successful authentication or an error.

CLEAR
: Sent to the client after corresponding `CLEAR` request. It informs client about a successful 
authentication or an error. 

RESPONSE
: Sent to the client as a response to `GET`, `SET`, and `SUB` requests, and their explicit 
versions `GETEX`, `SETEX`, and `SUBEX`. It may contain a value (i.e., response from the FGC) 
or just server as a confirmation of command execution. If there is a problem with a request,
the error will be provided in this message. 

NOTIFICATION
: Sent to the client who subscribed to a property. It is sent just after subscribing (*first update*)
and on every subsequent property value change. During *first update*, a separate response will be 
sent for each cycle selector.

UNSUB
: Sent to the client to confirm unsubscription. 

TERM
: This message is used to receive a character from a device to implement the *remote terminal* functionality.

STATUS
: This message is sent to the client and contains devices' statuses as sent periodically from the devices.
It provides the same functionality as the UDP subscriptions. 
	

## Generic payload structure

Every message shares the common payload:

:::{admonition} Common payload
:class: note

```{code-block} json
{
  "type": "<TYPE>"
}
```

Where:

- `<TYPE>` may be one of these strings: `"INIT"`, `"CLEAR"`, `"GET"`, `"GETEX"`, `"SET"`, `"SETEX"`,
`"SUB"`, `"SUBEX"`, `"UNSUB"`, `"TERM"`, `"RESPONSE"`, `"NOTIFICATION"` or `"STATUS"`.\
The field is case insensitive.

:::





(status-codes)=
## Errors handling and format

There are two types of errors: the ones related to the protocol itself and the ones that
come from the devices as a result of command execution. 

To distinguish between them, the protocol introduces two concepts related to error handling:
- *Status* - this is an integer that relates to the protocol status. Meaning of this number 
is the same as the [HTTP Status Codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes).
- *Error code* - this is a number returned by the device and its meaning is described in the
[FGC documentation](https://accwww.cern.ch/proj-fgc/proj-fgc/gendoc/def/Errors.htm). If the 
*error code* is different from 0 or 1 (success codes), the *status* has to be set to 400 (*Bad Request*)

Additionally, a field *error* is present in the payloads to provide a string with the error details.
For the 400 (*Bad Request*) status, the error message is coming from the device. 

The *status* code can be any of the HTTP Status Codes, but several codes have defined meaning:

:::{admonition} 200 OK
:class: tip

The request was correctly formulated and successfully executed. If the response is a result
of command execution, the *error code* has to be set to 0 or 1. *Error* message should be empty.
:::


:::{admonition} 400 Bad Request
:class: danger

There are two reasons the server might return this status:
- The request payload was incorrect (e.g., missing required fields, invalid command string, etc.) 
or any other protocol error occurred. \
In this case, the *error code* will be set to -1 and the *error* message will provide details.
- The request payload was correct, but the device returned an error.\
In that case the *error code* and the *error* message are set by the device. *Error code* will be greater than 1. 
:::


:::{admonition} 401 Unauthorized
:class: danger

Any request other than *INIT* was sent before the client was successfully initialized or 
the authentication (i.e., processing of the `INIT` or `CLEAR` request) failed.\
In this case, the *error code* will be set to -1 and the *error* message will provide details.
:::


:::{admonition} 403 Forbidden
:class: danger

The command was rejected during authorisation (e.g., RBAC or MCS).\
In this case, the *error code* will be set to -1 and the *error* message will provide details.
:::


:::{admonition} 404 Not Found
:class: danger

The request contained an incorrect value for the *type* field.\
In this case, the *error code* will be set to -1 and the *error* message will provide details.
:::
    

:::{admonition} 500 Internal Server Error
:class: danger

Returned by the server in case the request processing failed due to server problems
(e.g., an exception occurred during processing).\
In this case, the *error code* will be set to -1 and the *error* message will provide details.
:::


(command-string)=
## Command string specification

The format of the command string that is sent in *GET*, *SET*, and *SUB* is described
in a separate specification, [TODO TODO TODO available here TODO TODO TODO](http://www.google.pl)



## Specific payload structures

### Requests

:::{admonition} **INIT** request
:class: note

```{code-block} json
{
	"type":       "INIT",
	"credentials": {},
	"options":     [],
	"version":      ""
}
```

Where:

- `"credentials"` is an object containing user's credentials (e.g., at CERN this object 
will contain an RBAC token). If unused, it may be set to an empty object. 
- `"options"` is an array containing zero or more strings with options. The supported options are:
  - `"term"` which enables *remote terminal* functionality.
  - `"status"` which enables sending by the server the periodic status update.
- `"version"` is a string indicating the version of the protocol implemented by the client.

:::


:::{admonition} **CLEAR** request
:class: note

```{code-block} json
{
	"type":       "CLEAR",
	"credentials": {},
}
```

Where:

- `"credentials"` is an object containing user's credentials (e.g., at CERN this object 
will contain an RBAC token). If unused, it may be set to an empty object.

:::



:::{admonition} **GET** request
:class: note

```{code-block} json
{
	"type":    "GET",
	"device":  "",
	"command": "",
	"tag":     ""
}
```

Where:

- `"device"` is a name of the device to which the GET command is sent.
- `"command"` is a command string to be executed, as described [here](#command-string).
- `"tag"` is a client-generated, preferably unique, string. It has no meaning and will be simply passed back
to the client in the response. The client may use it to match the response to the request.   

:::



:::{admonition} **GETEX** request
:class: note

```{code-block} json
{
	"type":        "GETEX",
	"device":      "",
	"property":    "",
	"selector":    "",
	"transaction": "",
	"filter":      "",
	"option":      "",
	"tag":         ""
}
```

Where:

- `"device"` is a name of the device to which the GET command is sent.
- `"property"` is a name of the property to get.
- `"selector"` is a cycle selector.
- `"transaction"` is a transaction ID.
- `"filter"` is a filter to apply.
- `"option"` is a GET option.
- `"tag"` is a client-generated, preferably unique, string. It has no meaning and will be simply passed back
to the client in the response. The client may use it to match the response to the request.   

:::



:::{admonition} **SET** request
:class: note

```{code-block} json
{
	"type":    "SET",
	"device":  "",
	"command": "",
	"tag":     ""
}
```
\
Where fields (except `type`) are the same as in the `GET` request.
:::




:::{admonition} **SETEX** request
:class: note

```{code-block} json
{
	"type":        "SETEX",
	"device":      "",
	"property":    "",
	"value":       "",
	"selector":    "",
	"transaction": "",
	"filter":      "",
	"option":      "",
	"tag":         ""
}
```

Where:

- `"value"` is a value of the property to set.

The remaining fields (except `type`) are the same as in the `GETEX` request.

:::



:::{admonition} **SUB** request
:class: note

```{code-block} json
{
	"type":    "SUB",
	"device":  "",
	"command": "",
	"tag":     ""
}
```
\
Where fields (except `type`) are the same as in the `GET` request.
:::




:::{admonition} **SUBEX** request
:class: note

```{code-block} json
{
	"type":        "SETEX",
	"device":      "",
	"property":    "",
	"selector":    "",
	"transaction": "",
	"filter":      "",
	"option":      "",
	"tag":         ""
}
```

Where fields (except `type`) are the same as in the `GETEX` request.

:::



:::{admonition} **UNSUB** request
:class: note

```{code-block} json
{
	"type": "UNSUB",
	"tag":  ""
}
```

Where:

- `"tag"` should be the set to the same `tag` that was passed to the server in the `SUB` request.

:::



:::{admonition} **TERM** request (------ **MAY CHANGE** ------)
:class: warning

```{code-block} json
{
	"type":   "TERM",
	"device": "",
	"char":   ""
}
```

Where:

- `"device"` is a name of the device to which the terminal character is sent.
- `"char"` is a single-byte character sent to the device's terminal.    

:::


### Responses

:::{admonition} **INIT** response
:class: note

```{code-block} json
{
	"type":    "INIT",
	"status":  0,
	"error":   "",
    "version": ""
}
```

Where:

- `"status"` is an HTTP Status Code, as described [here](#status-codes).
- `"error"` is an error message, describing the problem. May be empty. 
- `"version"` is a string indicating the version of the protocol implemented by the server.

:::



:::{admonition} **CLEAR** response
:class: note

```{code-block} json
{
	"type":    "CLEAR",
	"status":  0,
	"error":   ""
}
```

Where:

- `"status"` is an HTTP Status Code, as described [here](#status-codes).
- `"error"` is an error message, describing the problem. May be empty. 

:::



:::{admonition} **RESPONSE** response
:class: note

```{code-block} json
{
	"type":   "RESPONSE",
    "value":  "",
    "timestamp":       { "sec": 0, "nano": 0 },
    "cycle_timestamp": { "sec": 0, "nano": 0 },
    "user":            0,
    "user_name":       "",
    "status": 0,
    "code":   0,
    "error":  "",
    "tag":    ""
}
```

Where:

- `"value"` is a value returned by the device. May be empty.
- `"timestamp"` is an acquisition timestamp.
- `"cycle_timestamp"` is a cycle timestamp.
- `"user"` is a number indicating the timing user (cycle selector).
- `"user_name"` is a textual name of the timing user (cycle selector).
- `"status"` is an HTTP Status Code, as described [here](#status-codes).
- `"code"` is a device error code, as described [here](#status-codes).
- `"error"` is an error message, describing the problem. May be empty.
- `"tag"` is a tag that was passed in the corresponding `GET`, `GETEX`, `SET`, 
`SETEX`, `SUB`, or `SUBEX` request.

:::



:::{admonition} **NOTIFICATION** response
:class: note

```{code-block} json
{
	"type":            "NOTIFICATION",
    "value":           "",
    "timestamp":       { "sec": 0, "nano": 0 },
    "cycle_timestamp": { "sec": 0, "nano": 0 },
    "user":            0,
    "user_name":       "",
    "tag":             "" 
}
```

Where:

- `"value"` is a new value returned by the device.
- `"timestamp"` is an acquisition timestamp.
- `"cycle_timestamp"` is a cycle timestamp.
- `"user"` is a number indicating the timing user (cycle selector).
- `"user_name"` is a textual name of the timing user (cycle selector).
- `"tag"` is a tag that was passed in the corresponding `SUB` request.

:::



:::{admonition} **UNSUB** response
:class: note

```{code-block} json
{
	"type": "UNSUB",
    "status": 0
    "error":  "" 
    "tag":  "" 
}
```

Where:

- `"status"` is an HTTP Status Code, as described [here](#status-codes).
- `"error"` is an error message, describing the problem. May be empty.
- `"tag"` is a tag that was passed in the corresponding `UNSUB` request.

:::



:::{admonition} **TERM** response (------ **MAY CHANGE** ------)
:class: warning

```{code-block} json
{
	"type":   "TERM",
	"device": "",
	"char":   ""
}
```

Where:

- `"device"` is a name of the device from which the terminal character is sent.
- `"char"` is a single-byte character sent from the device's terminal.

:::



:::{admonition} **STATUS** response (------ **MAY CHANGE** ------)
:class: warning

```{code-block} json
{
	"type":   "STATUS",
	"status": [ { "device": "", "data": "" } ]
}
```

Where:

- `"status"` is an array of objects. Each object contains fields, where:
  - `"device"` is a name of the device for which the `data` field is sent.
  - `"data"` is status data sent from the device (*TODO decide on the format*)

:::




## Possible communication scenarios

### Initializing client 

- Client sends an `INIT` request.
- Server responds with an `INIT` response. 
  - If the response indicates success, the client is considered initialized.
  - Otherwise, the client may retry sending an `INIT` request or break the connection.


### Sending get or set commands

- Client sends a `GET`, `GETEX`, `SET`, or `SETEX` request.
- Server responds with a `RESPONSE` response. 


### Subscribing

- Client sends a `SUB` or `SUBEX` request.
- Server responds with a `RESPONSE` response. 
  - If the response indicates a success:
    - The server will send a `NOTIFICATION` response immediately with the first update. If the
subscription is to all timing users, then the server will send one `NOTIFICATION` response for each
timing user.
    - The server will send a `NOTIFICATION` response on each subsequent change of the property value
for any timing user.
  - If the request failed, no `NOTIFICATION` response is sent.


### Unsubscribing

- Client sends an `UNSUB` request.
- Server responds with an `UNSUB` response, confirming subscription removal or otherwise indicating an error.


### Clearing and changing user's credentials

- Client sends a `CLEAR` request, that optionally contains new user's credentials.
- If the user's credentials were provided:
    - Server attempts to authenticate the new user using its credentials.
    - If that succeeds:
        - Server clears all active subscriptions and restarts the protocol state to the one after initialization. 
        - Server then responds with a `CLEAR` response, confirming clearing.
    - If that fails:
        - Server responds with a `CLEAR` response, containing error message. The active subscriptions
        are preserved.
- If the user's credentials were not provided:
    - Server clears the current user's credentials, clears all active subscriptions 
    and restarts the protocol state to the one after initialization.
    - Server then responds with a `CLEAR` response, confirming clearing.

### Remote terminal 


:::{admonition} Warning
:class: warning
Work in progress. May change.   
:::

Remote terminal works fully asynchronously. The responses may be even send without requests.

- Client may send a `TERM` request with a character that will be passed to the device indicated in the request.
- Server may send a `TERM` response with a character coming from the device indicated in the response.


### Periodic status update 

There is only response for this functionality. No request is needed.

If enabled by the options during initialization, the server will periodically send `STATUS` responses
that contain status data for each device on the server (FEC). 






