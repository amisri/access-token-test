# FGC4 Profinet protocol

:::{admonition} This is just a protocol proposal.
:class: danger

The final shape depends on relevant experts' opinions. 
:::



## Current version

0.2


## Purpose

The purpose of this protocol is to define a two-way communication channel with 
PLC devices using the Profinet protocol, which is natively supported by the PLCs.


## Data specification

- Data is memory-mapped, i.e. it is similar to registers in any MCU.
- Data is not serial/streamed, i.e. the maximum size is constant.
- Data is divided into 256 registers, each containing 8 bytes.
- Data is divided into:
  - Registers written by PLC and read by FGC4 (*PLC registers*)
  - Registers written by FGC4 and read by PLC (*FGC4 registers*)
- Data is addressable using 2 bytes. There are two address spaces: one for registers written by the PLC 
and one for register written by the FGC4. 

{fgcd}`<img src="../../_static/img/profinet_data.png" width="70%" center>`


## Hardware architecture

{fgcd}`<img src="../../_static/img/profinet_hw.png" center>`


## Basic communication workflow

The goal is to exchange registry-like data between a CPU of the FGC4 and a CPU of a PLC, in both ways. 

### PLC to DI/OT 

Starting from the PLC side, for data that the PLC writes

- The PLC sends the data using Profinet protocol over Ethernet link into the Profinet
  FMC carrier board plugged into a generic DI/OT peripheral board.
- The processor on this board decodes Profinet communication and exposes received data over SPI channel,
  as described below.
- The FPGA on the peripheral board consumes this SPI data and stores the data in its copy of registers.
- These registers are memory-mapped and are made available to be read by the system board's CPU using standard
memory-map access method.


### DI/OT to PLC

For data coming from the DI/OT, the flow is reversed:

- System board's CPU writes data into itw own FPGA memory space.
- This is then transmitted to the FPGA on the peripheral board, using standard memory-map access method.
- The FPGA then exposes this data over SPI channel, using the same data format, as described.
- This is received by the ARM CPU on the Profinet FMC board and then transmitted over Profinet protocol to the PLC.

  
## SPI channel

:::{admonition} SPI.
:class: hint

The use of SPI is a proposal - it could be, in fact, any other basic protocol.
:::

### The protocol

- The SPI channel works in 2 directions, both with the same data format as described below.
- Each request consists of a single-byte header, then two bytes of address, followed a byte of data and then a 8-bit CRC. 
The header contains status data.
- After each request, there is a single-byte response from the receiver, which acknowledges (or not) the request.
- Response is awaited for a fixed timeout. 
- If there is no response or the response does not acknowledge the request, the request is retransmitted one more time.
- If, again, there is no response or the response does not acknowledge the request, the senders gives up on sending this
request. 

### Request format

{fgcd}`<colorbox blue bold width="18%" text="Byte 4 (MSB)">`
{fgcd}`<colorbox blue bold width="18%" text="Byte 3">`
{fgcd}`<colorbox blue bold width="18%" text="Byte 2">`
{fgcd}`<colorbox blue bold width="18%" text="Byte 1">`
{fgcd}`<colorbox blue bold width="18%" text="Byte 0 (LSB)">`

{fgcd}`<colorbox green width="18%" text="CRC">`
{fgcd}`<colorbox green width="18%" text="Data value">`
{fgcd}`<colorbox green width="18%" text="Byte addres">`
{fgcd}`<colorbox green width="18%" text="Register addres">`
{fgcd}`<colorbox green width="18%" text="Header">`

#### Request's header format

{fgcd}`<colorbox blue bold width="10.5%" text="Bit 7">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 6">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 5">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 4">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 3">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 2">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 1">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 0">`

{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="State">`
{fgcd}`<colorbox green width="10.5%" text="Link">`
{fgcd}`<colorbox green width="10.5%" text="Data">`

Data
: If this bit is 1, then the request contains address and data to be set. If the bit is 0, then the address and data
bytes are ignored - in this case the request serves only as a mean to transmit the state of the link and Profinet.\
&nbsp;\
From the direction FPGA -> FMC, this bit is always 1.

Link
: 1 means that the Ethernet link is detected on the FMC Profinet board, 0 means otherwise.\
&nbsp;\
From the direction FPGA -> FMC, this bit is always 1.

State
: 1 means that the state of Profinet protocol is correct (**TODO assuming there is such thing at all**).\
&nbsp;\
From the direction FPGA -> FMC, this bit is always 1.


### Response format

{fgcd}`<colorbox blue bold width="10.5%" text="Bit 7">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 6">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 5">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 4">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 3">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 2">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 1">`
{fgcd}`<colorbox blue bold width="10.5%" text="Bit 0">`

{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="X">`
{fgcd}`<colorbox green width="10.5%" text="N/ACK">`

N/ACK
: One means ACK, 0 means NACK.



## Memory-mapped access

The data should be visible in the FPGA memory map as a continuous block of 2048 bytes (256 x 8), available at the given
base address. The software will then read correct byte from a given register by calculating the absolute address: 
`base_address + (register_index*8) + byte_index`.