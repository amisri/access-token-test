# VSLib configuration commands format

## Current version

0.1


## Purpose

This document describes the JSON format for configuration commands sent from the 
FGC4 Linux application into the VSLib-based bare-metal FGC4 application in order to
set values of required parameters. 

The data in this format will be prepared by the FGC Configurator.


