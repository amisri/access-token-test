# FGCDv2 Configuration data format

## Current version

0.1


## Purpose

This document describes the configuration JSON file format used by the FGCDv2 framework 
to load its configuration and a list of devices, along with their configuration.

## Using JSON

:::{include} ../common/usingJson.txt
:::

## Schema
