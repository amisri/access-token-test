# VSLib configuration manifest format

## Current version

0.1


## Purpose

This document describes the JSON format for configuration manifest generated
by the VSLib-based bare-metal application in order to inform FGC4 Configurator
about what components and parameters are used in a given voltage loop application.

The data in this format will be produced by running the bare-metal application in 
a special mode on a regular x86_64 machine, during deployment process. The resulting
JSON data is then attached as meta-data shipped together with the binary. 
It is then consumed by the FGC Configurator to dynamically generate configuration UI.


