# Binary distribution packages 

## Current version

0.1


## Purpose

This document describes the contents and format of the binary packages based 
on the Debian package format.

The packages contains the binary and related metadata. It is produced by the
building system and consumed by the FGCDv2 application to reprogram themselves,
the systems they are on or the devices they manage. 
The binary may be anything reprogrammable, from the FGC2 main program to FGC4 FPGA bitstreams.
The metadata may contain information about the version and compilation and any associated 
files (e.g., VSLib configuration manifest, as described [here](./vslibConfigManifest.md)).


