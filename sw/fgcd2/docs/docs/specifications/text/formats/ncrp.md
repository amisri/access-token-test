# Network Command Response Protocol

:::{admonition} This format specification is not finished.
:class: danger

This file was copied from Command String specification and some things were not changed yet.
Once they are changed, this warning may be removed.
:::

:::{admonition} FGC2 format
:class: danger
Is it the same for FGC2? Or should FGC2 have a separate document?
:::
 
## Current version

1.0


## Purpose

The Network Command Response Protocol (NCRP) is a string format that it used to send commands to devices 
from the FGC2 and FGC3 platforms. 

A very similar string format for commands have been specified based on this format, called simply Command String, 
specified [here](commandString.md). The Command String format is used to type in commands by humans, e.g., in the 
web tools like FGC Commander, which are then sent to a FEC for processing. On the other hand, this NCRP format is used
to send a processed command to FGC2 or FGC3 devices.  

It is worth noting that this specification has been reversed-engineered based on the current implementation 
in FGC2 and FGC3 devices. 


## Format specification 

{fgcd}`<colorbox blue bold text="property">` 
{fgcd}`<colorbox green bold text="( cycle selector )">` 
{fgcd}`<colorbox green bold text="&lt; transaction id &gt;">` 
{fgcd}`<colorbox green bold text="[ filter ]">` 
{fgcd}`<colorbox text="spaces">` 
{fgcd}`<colorbox green bold text="get options">`
{fgcd}`<colorbox text="or">`
{fgcd}`<colorbox green bold text="value">` 


{fgcd}`<div mb=60px">`


:::{admonition} Property &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Mandatory`
:class: note
{fgcd}`<div mb="30px">`

A name of the property for which the command is executed.


Allowed format:
: A string of any length, composed of only lowercase letters, uppercase letters, digits, dots, and underscores.
:::



:::{admonition} Cycle selector &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

A timing cycle selector. It may be a number or a textual name of the timing cycle.\
If left empty, it selects all users. 

Allowed format:
: A string of any length, composed of only lowercase letters, uppercase letters, digits, dot, and underscores.

:::



:::{admonition} Transaction ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

A positive integer ID of a transaction. If the angle brackets are provided, the ID cannot be left empty. 

Allowed format:
: A string of any length, composed of only digits.

:::



:::{admonition} Filter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

A filter. Its meaning depends on given property. For arrays it is used to select a sub-range. \
Even if the square brackets are provided, the filter may be left empty.

Allowed format:
: A string of any length, composed of any characters other than a closing square bracket `]`.

:::



:::{admonition} Get options &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

One or more of the predefined GET options. 

Allowed format:
: A string of any length, composed of only lowercase letters, uppercase letters, digits, and underscores.

Remarks:
: Only valid for `GET` and `SUBSCRIBE` commands.

:::



:::{admonition} Value &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

Value of a property to set. If value is not provided it defaults to an empty string. The value starts at the first
non-whitespace character.

Allowed format:
: A string of any length composed of any characters. 

Remarks:
: Only valid for `SET` commands.

:::



### Remarks

TODO if any

## Regex representation

TODO





## Examples

:::{admonition} Valid `GET`/`SUBSCRIBE` examples.  
:class: hint
All of the following lines are valid command strings (which does not mean that all of them make sense).
:::

```TEXT

```


:::{admonition} Valid `SET` examples.  
:class: hint
All of the following lines are valid command strings (which does not mean that all of them make sense).
:::

```TEXT

```



:::{admonition} Invalid `GET`/`SUBSCRIBE` examples.  
:class: danger
All of the following lines are invalid command strings.
:::

```TEXT

```


:::{admonition} Invalid `SET` examples.  
:class: danger
All of the following lines are invalid command strings.
:::

```TEXT

```