# FEC Command String format



## Current version

0.3.0


## Purpose

The FEC Command String is a human-readable command format used in the tools (e.g., FGC Commander) to send commands
to the FEC and eventually to the devices. This specification can be considered as an appendix to the
[WEF (Websockets for EPC FEC) protocol](../protocols/wef.md) specification.

This Command String format should not be confused with a very similar format used to send commands to the FGC2 and FGC3
devices, which is called *NCRP (Network Command Response Protocol)* and it is specified [here](./ncrp.md).

The Command String format is used between a WEF (WebSocket) client and a FEC. It is then parsed by the FEC software
into a command structure and executed. For FGC2 and FGC3, this execution will involve constructing another command string
in the aforementioned NCRP protocol format.


## Format specification

{fgcd}`<colorbox blue bold text="property">`
{fgcd}`<colorbox green bold text="( cycle selector )">`
{fgcd}`<colorbox green bold text="&lt; transaction id &gt;">`
{fgcd}`<colorbox green bold text="[ filter ]">`
{fgcd}`<colorbox text="spaces">`
{fgcd}`<colorbox green bold text="get options">`
{fgcd}`<colorbox text="or">`
{fgcd}`<colorbox green bold text="value">`


{fgcd}`<div mb=60px">`


:::{admonition} Property &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Mandatory`
:class: note
{fgcd}`<div mb="30px">`

A name of the property for which the command is executed.


Allowed format:
: A string of any length, composed of only lowercase letters, uppercase letters, digits, dots, and underscores.
:::



:::{admonition} Cycle selector &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

A timing cycle selector. It may be a number or a textual name of the timing cycle.\
If left empty, it selects all users.

Allowed format:
: A string of any length, composed of only lowercase letters, uppercase letters, digits, dot, and underscores.

:::



:::{admonition} Transaction ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

A positive integer ID of a transaction. If the angle brackets are provided, the ID cannot be left empty.

Allowed format:
: A string of any length, composed of only digits.

:::



:::{admonition} Filter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

A filter. Its meaning depends on given property. For arrays it is used to select a sub-range. \
Even if the square brackets are provided, the filter may be left empty.

Allowed format:
: A string of any length, composed of any characters other than a closing square bracket `]`.

:::



:::{admonition} Get options &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

One or more of the predefined GET options.

Allowed format:
: A string of any length, composed of only lowercase letters, uppercase letters, digits, and underscores.

Remarks:
: Only valid for `GET` and `SUBSCRIBE` commands.

:::



:::{admonition} Value &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Optional`
:class: hint
{fgcd}`<div mb="30px">`

Value of a property to set. If value is not provided it defaults to an empty string. The value starts at the first
non-whitespace character.

Allowed format:
: A string of any length composed of any characters.

Remarks:
: Only valid for `SET` commands.

:::



### Remarks

- Type of the command (`GET`, `SET` or `SUBSCRIBE`) is already determined by the type of the WEF request.
- The `cycle selector`, `transaction`, and `filter` may be specified in any order.
- The `get option ` and `value` have to be seperated by at least one whitespace character.
- There might be multiple consecutive whitespace characters in the `spaces` boxes, as well as at the beginning
and at the end of the command string.
- For `SET` commands with no value or `GET`/`SUBSCRIBE` commands with not *get options*, there does not have to be any whitespace character at the end
of the command.


## Regex representation

### Complete `GET`/`SUBSCRIBE` regex.
It does not contain matching groups for every field - it is meant to validate if the string is a valid Command String.

```text
^\s*[\w\.]+(?:(?=(\(|<|\[))(\([\w\.]*\)|<\d+>|\[[^\]]*\]))?(?!\1)(?:(?=(\(|<|\[))(\([\w\.]*\)|<\d+>|\[[^\]]*\]))?(?!\1|\3)(\([\w\.]*\)|<\d+>|\[[^\]]*\])?(?:\s|$)[\s\w\.]*$
```

### Step by step explanation

- `^\s*[\w\.]+` Selects a valid and mandatory property name at the beginning of the string (with optional whitespace characters at the beginning).
- `(?:...)?` Crates a non-capturing group that is optional. Inside this group:
  - `(?=(\(|<|\[))` Creates a positive look-ahead to assert that `(` or `<` or `[` character follows and captures this character in the capture group 1.
- `(\([\w\.]*\)|<\d+>|\[[^\]]*\])` Creates a capture group 2 that matches either `(cycle selector)`, `<transaction ID>`, or `[filter]`.
- `(?!\1)` Creates a negative look-ahead to assert that the contents of the capture group 1 (`(` or `<` or `[`) does not appear again.
- `(?:...)?` Is identical to the first non-capturing group. The next `(` or `<` or `[` is captured in the capture group 3.
- `(?!\1|\3)` Creates a negative look-ahead to assert that the contents of the capture group 1 or 3 (`(` or `<` or `[`) does not appear again.
- `(\([\w\.]*\)|<\d+>|\[[^\]]*\])?` Creates an optional capture group that matches either `(cycle selector)`, `<transaction ID>`, or `[filter]` for the third time.
- `(?:\s|$)` Selects a whitespace character or the end of the line.
- `[\s\w\.]*$` Selects optional get options.

### Complete `SET` regex.
Is almost identical to the `GET` regex - the only difference is at the very end: instead of capturing get options it captures value to set.

```text
^\s*[\w\.]+(?:(?=(\(|<|\[))(\([\w\.]*\)|<\d+>|\[[^\]]*\]))?(?!\1)(?:(?=(\(|<|\[))(\([\w\.]*\)|<\d+>|\[[^\]]*\]))?(?!\1|\3)(\([\w\.]*\)|<\d+>|\[[^\]]*\])?(?:\s|$).*$
```

### Regexes for individual fields

Regex for a property name:
: `[\w\.]+`

Regex for a cycle selector:
: `\([\w\.]*\)`

Regex for a transaction ID:
: `<\d+>`

Regex for a filter:
: `\[[^\]]*\]`

Regex for get options:
: `[\s\w\.]*`

Regex for a value:
: `.*`








## Examples

:::{admonition} Valid `GET`/`SUBSCRIBE` examples.
:class: hint
All of the following lines are valid command strings (which does not mean that all of them make sense).
:::

```TEXT
REF.PULSE.AMPLITUDE
ref.pulse.amplitude
REF_PULSE_AMPLITUDE
Abc.XYZ_1..._QWE_

REF.PULSE.AMPLITUDE(10)
REF.PULSE.AMPLITUDE(99999)
REF.PULSE.AMPLITUDE(SPS.USER.MD5)
REF.PULSE.AMPLITUDE(SPS.___.MD5)
REF.PULSE.AMPLITUDE()

REF.PULSE.AMPLITUDE<10>
REF.PULSE.AMPLITUDE<99999>

REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ]
REF.PULSE.AMPLITUDE[<10>]
REF.PULSE.AMPLITUDE[<>]
REF.PULSE.AMPLITUDE[(10)]
REF.PULSE.AMPLITUDE[(SPS.USER.MD5)]
REF.PULSE.AMPLITUDE[]
REF.PULSE.AMPLITUDE[[[[]

REF.PULSE.AMPLITUDE BIN
REF.PULSE.AMPLITUDE BIN INFO TYPE

REF.PULSE.AMPLITUDE(SPS.___.MD5)<99999>[  Abc_._123!@#$%^&()<>  ]  BIN TYPE
REF.PULSE.AMPLITUDE(SPS.___.MD5)[  Abc_._123!@#$%^&()<>  ]<99999>  BIN TYPE
REF.PULSE.AMPLITUDE<99999>(SPS.___.MD5)[  Abc_._123!@#$%^&()<>  ]  BIN TYPE
REF.PULSE.AMPLITUDE<99999>[  Abc_._123!@#$%^&()<>  ](SPS.___.MD5)  BIN TYPE
REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ]<99999>(SPS.___.MD5)  BIN TYPE
REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ](SPS.___.MD5)<99999>  BIN TYPE
```


:::{admonition} Valid `SET` examples.
:class: hint
All of the following lines are valid command strings (which does not mean that all of them make sense).
:::

```TEXT
REF.PULSE.AMPLITUDE
ref.pulse.amplitude
REF_PULSE_AMPLITUDE () <> Abc_._  123 !@#$%^& () <>
Abc.XYZ_1..._QWE_       () <> Abc_._  123 !@#$%^& () <>

REF.PULSE.AMPLITUDE(10) Abc_._123!@#$%^&()<>
REF.PULSE.AMPLITUDE(99999) Abc_._123!@#$%^&()<>
REF.PULSE.AMPLITUDE(SPS.USER.MD5) Abc_._123!@#$%^&()<>
REF.PULSE.AMPLITUDE(SPS.___.MD5)
REF.PULSE.AMPLITUDE()

REF.PULSE.AMPLITUDE<10> () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE<99999> () <> Abc_._  123 !@#$%^& () <>

REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ] () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[<10>] () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[<>] () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[(10)] () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[(SPS.USER.MD5)] () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[] () <> Abc_._  123 !@#$%^& () <>

REF.PULSE.AMPLITUDE BIN INFO TYPE () <> Abc_._  123 !@#$%^& () <>

REF.PULSE.AMPLITUDE(SPS.___.MD5)<99999>[  Abc_._123!@#$%^&()<>  ]  () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE(SPS.___.MD5)[  Abc_._123!@#$%^&()<>  ]<99999>  () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE(SPS.___.MD5)<99999>[  Abc_._123!@#$%^&()<>  ]  () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE<99999>(SPS.___.MD5)[  Abc_._123!@#$%^&()<>  ]  () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE<99999>[  Abc_._123!@#$%^&()<>  ](SPS.___.MD5)  () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ]<99999>(SPS.___.MD5)  () <> Abc_._  123 !@#$%^& () <>
REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ](SPS.___.MD5)<99999>  () <> Abc_._  123 !@#$%^& () <>

REF.PULSE.AMPLITUDE (10)
REF.PULSE.AMPLITUDE ( 10 )
REF.PULSE.AMPLITUDE (-99999)
REF.PULSE.AMPLITUDE (SPS!USER!MD5)
REF.PULSE.AMPLITUDE (
REF.PULSE.AMPLITUDE <10>
REF.PULSE.AMPLITUDE <>
REF.PULSE.AMPLITUDE <
REF.PULSE.AMPLITUDE [
REF.PULSE.AMPLITUDE [[[[[
REF.PULSE.AMPLITUDE [ ABC! ]
```



:::{admonition} Invalid `GET`/`SUBSCRIBE` examples.
:class: danger
All of the following lines are invalid command strings.
:::

```TEXT
REF!PULSE!AMPLITUDE
(10)

REF.PULSE.AMPLITUDE (10)
REF.PULSE.AMPLITUDE( 10 )
REF.PULSE.AMPLITUDE(-99999)
REF.PULSE.AMPLITUDE(SPS!USER!MD5)
REF.PULSE.AMPLITUDE( )
REF.PULSE.AMPLITUDE(

REF.PULSE.AMPLITUDE <10>
REF.PULSE.AMPLITUDE< 10 >
REF.PULSE.AMPLITUDE<abc>
REF.PULSE.AMPLITUDE<>
REF.PULSE.AMPLITUDE<

REF.PULSE.AMPLITUDE [  Abc_._123!@#$%^&()<>  ]
REF.PULSE.AMPLITUDE[
REF.PULSE.AMPLITUDE[]]

REF.PULSE.AMPLITUDE[ <> ()
REF.PULSE.AMPLITUDE< [] ()
REF.PULSE.AMPLITUDE(<10>)

REF.PULSE.AMPLITUDE<10><10>
REF.PULSE.AMPLITUDE(10)(10)
REF.PULSE.AMPLITUDE<10><15>
REF.PULSE.AMPLITUDE(10)(15)
REF.PULSE.AMPLITUDE[  Abc_._123!@#$%^&()<>  ][  Abc_._123!@#$%^&()<>  ]
REF.PULSE.AMPLITUDE[10][16]
REF.PULSE.AMPLITUDE<10>(10)(10)[ABC]

REF.PULSE.AMPLITUDE BIN!
REF.PULSE.AMPLITUDE BIN INFO TYPE!
REF.PULSE.AMPLITUDE BIN(10)
REF.PULSE.AMPLITUDE BIN (10)
REF.PULSE.AMPLITUDE(10)BIN
```


:::{admonition} Invalid `SET` examples.
:class: danger
All of the following lines are invalid command strings.
:::

```TEXT
REF!PULSE!AMPLITUDE
(10) ABC

REF.PULSE.AMPLITUDE( 10 ) ABC
REF.PULSE.AMPLITUDE( ABC

REF.PULSE.AMPLITUDE<10>ABC

REF.PULSE.AMPLITUDE[
```
