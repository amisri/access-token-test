# Specifications

This website contains different specifications related to the FGC4 and FGCDv2 projects.


```{toctree}
:caption: Projects
:maxdepth: 1

text/projects/fgc4
text/projects/configurator
```

```{toctree}
:caption: Protocols
:maxdepth: 1

text/protocols/wef
text/protocols/profinet
```

```{toctree}
:caption: Formats
:maxdepth: 1

text/formats/fgcdv2Config
text/formats/binaryPackages
text/formats/commandString
text/formats/ncrp
text/formats/vslibConfigManifest
text/formats/vslibConfigCommands
```


