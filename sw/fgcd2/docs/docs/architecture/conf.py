# FGCDv2 configuration file for the Sphinx documentation builder.
#
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

project = 'FGCDv2'
html_title = "FGCDv2 Architecture"
copyright = '2022, EPC-CCS, CERN.'
author = 'FGCD2 Authors'

# -- General configuration ---------------------------------------------------

# Extensions path
import os
import sys
sys.path.append(os.path.abspath("../../config/extensions"))

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# Add extensions
extensions = ["sphinxawesome_theme", "myst_parser", "fgcd"]

# Add myST extensions
myst_enable_extensions = [
    "colon_fence",
    "deflist",
    "fieldlist",
    "html_admonition",
    "html_image"
]

# Paths are relative to this directory
templates_path = ['_templates']

# Selec theme
html_theme = "sphinxawesome_theme"

# Paths are relative to this directory
html_static_path = ['static', '../../config/css', '../../config/js']

# These paths are either relative to html_static_path or fully qualified paths (eg. https://...)
html_css_files = [
	"fgcdCommon.css",
	"sphinxTheme.css",
	"https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600&family=Raleway:wght@400;500;600&display=swap",
] 

# These paths are either relative to html_static_path or fully qualified paths (eg. https://...)
html_js_files = [
	"sphinxHooks.js",
	"panzoom.min.js"
] 


html_permalinks_icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" class="shareIcon" viewBox="0 0 16 16"> <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.499 2.499 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5zm-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3z" fill="#000000"></path> </svg>'

html_theme_options = {
	"show_prev_next": True,
	"show_scrolltop": True,
	"navigation_with_keys": True,
}