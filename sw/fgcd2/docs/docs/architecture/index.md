# FGCDv2 Architecture

This is the main document describing the architecture of the FGCDv2 Framework and its use-cases.

```{toctree}

text/symbols
text/nomenclature
text/overview
```

```{toctree}
:caption: Modules

text/modules/base
text/modules/status
text/modules/properties
text/modules/devices
text/modules/commands
text/modules/subscriptions
text/modules/events
text/modules/regulation
```