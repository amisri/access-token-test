# Nomenclature

The following terms are used across the FGCDv2 project.

FGCDv2, FGCD
: A software framework used to build FGC FEC and FGC4 software. 

Target
: It is a pair of source files (usually just *main.cpp*) and modules it depends on. It contains glue logic that connects
all components together. A target becomes an executable (so each executable is a separate target). 

Module
: A collection of C++ classes (including components) and other functionalities grouped together logically or functionally. 
This is just a logical entity and a compilation unit - it has no direct translation into C++. It is however the smallest 
unit of code that can be compiled independently. Modules can depend on other modules and libraries. The dependencies are 
automatically compiled.

Component
: A C++ class whose base class is `fgcd::Component`. Components are main building blocks of the FGCD application. 
They are organized in a tree hierarchy - they can have subcomponents and always have a parent component (except `RootComponent`).
They can be created, started (during initialization) and stopped (during termination). 

Hardware device
: A device that exists physically and is responsible for its properties and regulation functions (e.g. FGC3, FGC4).

Software device
: A device that has its properties and functions managed by the FEC (e.g. FGC Lite). 

Virtual device, sub-device
: A virtual device that is "attached" to a software or hardware device and has different settings (properties values)
depending on the timing events.

FGC event
: An event inside the FGCDv2 framework (e.g. STARTREF1WARNING)

Timing event
: A strictly CERN-specific event that is received from the timing network (e.g. BIX.F900-CT)  



