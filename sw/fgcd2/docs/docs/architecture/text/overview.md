# Overview

If we were to group different modules into some "*super-modules*" or "*functionalities*", it could look like that:  

{fgcd}`<img src="../_static/img/overview.png" width="70%" center>`


## Overview of modules

The following overview diagram contains all modules designed so far. 

*The modules are described in later chapters, so don't spend time analyzing the drawing here.*

{fgcd}`<zoomable src="../_static/img/overview_all.svg">`

