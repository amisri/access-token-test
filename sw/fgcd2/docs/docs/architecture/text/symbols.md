# Symbols

The following symbols are used across the architecture diagrams:
{fgcd}`<clear my="30px">`


{fgcd}`<img src="../_static/img/symbol_module.png" width="250px" wrap-left mr="30px" mb="50px">`
{fgcd}`<div mb="3rem">`

Module
: Represents a collection of C++ classes and other functionalities grouped together logically or functionally.

{fgcd}`<clear mt="40px">`



{fgcd}`<img src="../_static/img/symbol_fwk_class.png" width="250px" wrap-left mr="30px">`
{fgcd}`<div mb="3rem">`

FGCDv2 Framework class
: Used to represent a C++ class that is part of the framework and is not part of any particular target.
If you think about the FGCDv2 project as a combination of *a library* + *use of the library*, this symbol represents a class that
belongs to the library.

{fgcd}`<clear mt="40px">`
 


{fgcd}`<img src="../_static/img/symbol_user_class.png" width="250px" wrap-left mr="30px">`
{fgcd}`<div mb="3rem">`

User class
: Used to represent a C++ class that belongs to a target (i.e. a program using the FGCDv2 framework).

{fgcd}`<clear mt="40px">`



{fgcd}`<img src="../_static/img/symbol_interface.png" width="250px" wrap-left mr="30px">`
{fgcd}`<div mb="3rem">`

Interface
: Represents an interface (i.e. an abstract class).

{fgcd}`<clear mt="40px">`



{fgcd}`<img src="../_static/img/symbol_other.png" width="250px" wrap-left mr="30px" mb="50px">`
{fgcd}`<div mb="3rem">`

Other functionalities
: Represents all other functionalities, not necessarily implemented as C++ classes, including simple structures (POD). 

{fgcd}`<clear mt="40px">`
















