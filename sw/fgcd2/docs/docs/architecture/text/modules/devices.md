# Devices

## Modules overview

{fgcd}`<zoomable src="../../_static/img/devices_modules_all.svg">`



## Base module

{fgcd}`<img src="../../_static/img/devices_modules_base.png" width="100%" center>`

### DeviceName class

Helper struct that stores device's name and alias. it provides custom comparison operator, so that we can easily
compare devices' names - it's case insensitive and will return true if either name or alias matches. It can compare 
another `DeviceName` or `std::string`. Operator less (<) is provided so it can be used as a key in a `std::map`.
It should implement `Serializable` to print name&alias pair or have stringify function for the name reason.


### Device class

This class abstracts any device, both software or hardware ones and is supposed to serve as a base class for all other
devices. The only common attributes among the devices are their name and class id. A configuration JSON data can be passed,
that can contain additional data (for example dongle ID).

The `Device` class offers `executeCommand` method that is a last step in the command processing flow. The abstract 
method `processCommand` should be implemented by the derived classes and will be called by the `Device` class when 
it's time to execute the command.

**Note:** The design of device class is not yet fully finished and some functionalities are still missing 
(for example to notify subscription manager or to receive events).


### Command queuing and distributing 

{fgcd}`<img src="../../_static/img/devices_distributor.png" width="70%" center>`

There is an `ICommandDistributor` interface that is implemented by its default implementation `CommandDistributor`.
Distributor class provides means to queue commands - by design choice the `Device` class is guaranteed to process only
one command at the time - which simplifies `Device` subclasses (as they don't need to be thread-safe). 

The queueing implementation depends on the `ICommandDistributor` implementation and one could implement it to have no 
queuing at all, have one queue and one thread or use one queue and a thread pool. The default implementation in 
`CommandDistributor` uses a configurable-size thread pool to distribute queued commands to devices. 
The commands are queued using `DistributedCommand` helper struct which is a pair device-command.

{fgcd}`<img src="../../_static/img/devices_execute_flow.png" width="40%" center>`

The `processDistributedCommand` method takes `DistributedCommand` as an argument, even though it would be sufficient 
to pass just the command. This is done to avoid confusing this method with the `executeCommand` method.


### DeviceInventory class

The inventory holds all the devices in a map, to which they are inserted with `addDevice` method by the platform 
manager implementations (described later). It has two `find` methods that return an `std::optional` with the `Device`
pointer, which evaluates to false if no device with given name or alias is found.
Additionally, it has `forEach` method that can execute a functor for each device in the inventory. 


### Platform managers and device creation 

{fgcd}`<img src="../../_static/img/devices_platform.png" width="90%" center>`

The platform manager is supposed to manage a platform (e.g. FGC3, FGCLite, FGC4, etc.) and there should be only one
instance of a manager for a given platform. Managers will be responsible for communication stack with the devices they
manage (for example, FGC3 platform manager will be responsible for sending FGCEther's timing packets).

Platform managers are abstracted with `IPlatformManager` interface. They are then stored in `PlatformManagerInventory`
which has a vector of platform managers and is also responsible for creating and adding them. It is also used to find
and return an appropriate platform manager based on a device-to-be-created's class ID.

{fgcd}`<clear mb="50px">`

{fgcd}`<img src="../../_static/img/devices_creation.png" width="50%" wrap-right ml="30px">`

The creation flow of a device creation is described with the flow chart on the right. Each platform manager needs to
implement `createDevice` method that will constructs the device instance, hold it internally in the platform manager
object (if needed by the manager) and add the device to the main device inventory. It should also implement `manages`
method that returns true if given device class is managed by this manager (`PlatformManagerInventory` uses it to find
a suitable platform manager). 

Any class can instantiate devices (e.g. based on the name file) using `PlatformManagerInventory`, provided they have
device name, class ID and required JSON configuration data.

Platform manager or device creation can fail with an exception, which should be signalled with a fault in the FGCD,
but will not prevent the FGCD process from starting and operating. 

{fgcd}`<clear mb="50px">`

With this architecture, a single FEC can interface multiple platforms at the same time - we can have a FEC that manages
FGC3 and FGC4 devices without any changes (because they both use Ethernet for communication) or one could even image
a FEC that has FGC2 and FGC3 devices, provided required hardware is installed (MasterFIP and NIC). 

An example of platform managers and devices adaptors to handle FGC2 and FGC3 devices:

{fgcd}`<img src="../../_static/img/devices_modules_fgc2and3.png" width="100%">`

```{note}
This bit has been designed (and already implemented) by Adam and it is here for now, just as an example - it will be 
described on a separate page.
```


### Adaptor devices and subdevices

When it comes to software devices (e.g. FGCLite) there is only one device class, because the entire device is inside
the FEC. However, for FGCD-based hardware devices (e.g. FGC4) there is a need for two classes representing the same device - 
one class on the FEC and one class inside the device. To distinguished them (using FGC4 as an example), the class inside
the device can be simply called `Fgc4Device` and the class on the FEC can be called `Fgc4DeviceAdaptor` - as it adapts
FGC4 device to the common `Device` interface. 

The `Fgc4Device` class will be similar to other software device classes, e.g. `FgcLite`. 

{fgcd}`<img src="../../_static/img/devices_adaptor.png" width="100%">`

As on the above drawing, the same device infrastructure can be used inside the FGC4 itself.
Then the subdevices can be simply expressed by creating additional devices of the same type as the `Fgc4Device` inside
the FGC4 device - the FGC4 will become its own "FEC" hosting its subdevices. This doesn't require any change to the
device classes and infrastructure - but requires special class for the property tree (that will use pointers to the 
main property tree for all non-subdevice properties). By doing so, the concept of subdevices requires almost no 
additional classes and is transparent in the command-processing and device-matching flows.
 


## Reprogramming

{fgcd}`<img src="../../_static/img/devices_modules_programming.png" width="80%" center>`

TODO: To be described.


