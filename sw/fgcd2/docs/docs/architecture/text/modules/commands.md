# Commands

{fgcd}`<img src="../../_static/img/commands_simple.png" width=100%">`

```{warning}
This module is not design yet, so I just describe command processing ideas, without spcific components / modulus yet.
```

## Description

### Command object

Similarly to the FGCDv1, there will be a command object. Any module that wants to issue a command is will be called
command provider and it has to parse its own request to generate a valid, common command object. So, for example, the
TCP module will parse the string command, CMW will extract data from the request, etc. 

The command object will store command provider's request in `std::any` which is a strongly-typed C++ version of `void*`.

When creating the command object, the user will choose if they want to receive the result by placing the command object
onto some reception queue or if they want to receive `std::future` object (from the future-promise idiom). Both approaches
might be useful - reception queue for when you don't care about the order (e.g. CMW, TCP) and future object when you want
to wait for all commands to finish (e.g. reprogramming). 


### Errors and responses

The command object will provide (at least) two methods to finish the processing of the command. One that sets the error
and the other one that sets the response. The error will include a textual error message and/or error code. 
Finishing the command by providing the response will automatically place the command object on the response queue or 
it will set the future (depending on the selected type).


### Extension points

The command processing module will register extension points that will be stored in a vector and then executed one after
the other, until one of them fails or all are executed. Failure will cause the command to be finished with an error.  

Extension points will be classes implementing some common interface consisting of one function that takes command object
and returns an error (or lack of it).

You can think of each extension point like a next step in command processing flow.
They will be used, for example, to authorize commands (e.g. check RBAC, check MCS) or to implement special 
functionalities (e.g. transactions or some hack-ish processing of certain commands). 
As the name suggests, they could also be used to implement hot-fixes during operational period, by injecting command 
processing code into the processing flow.

Each target (during the initialization) will define what extension points it is using, by registering them in the 
command processing module. So, for example, FEC classes will register RBAC and MCS extension points, but FGC4 will not 
use them.

The extension points need to know who was the command provider as their behaviour might be different depending on the
source of the command (e.g. MCS component needs to know if the command arrived from the CMW as, as a safety measure, 
it will reject all set attempts on MCS properties if they are not done via CMW).


### Device handling

After the command processing executes all extension points, it will try to find a matching device (or now I'm thinking
it should actually do at the beginning, there's no point running anything if the device doesn't exist). In any case,
after processing extension points, the command will be passed to the device object, via `executeCommand` method, it will
then be queued and distributed back to the device where particular `Device` implementation will truly execute the 
command - as described in the *Devices* chapter.


### Response object

Response can come as a string (e.g. from the FGC2 or 3) or may come as a C++ object (e.g. from software devices, perhaps
from the FGC4 as well). Independently of how it comes, it might need to be converted to a string (e.g. for TCP) or
to the C++ type (e.g. for CMW). Even more tricky, if the value comes as C++ type from the software device - it 
may come as any C++ type (as described in *Properties*), for example enum. But CMW (and DSF as well) supports only limited
number of types. So, while float or int will stay the same, enum needs to be converter to either int or string. 

As a command result, the response object will contain in itself information whether the value is in the string or is a
C++ type. If it is in string, it also needs to provide information what type is represented by this string. 
Besides, the response object will have `toString` method that will be able to convert any value to a string. 

For the more complicated case of converting the value into CMW types, we can have an `std::variant` (C++ union) or 
`std::tuple` that holds all the possible types in CMW/DSF and store it in the command object as "expected value type".
Then we can have some class that does the conversion to CMW type, depending on the expected type. 
The details should be clearer during the implementation. 


