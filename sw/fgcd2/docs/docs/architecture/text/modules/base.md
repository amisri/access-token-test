# Base

## Modules overview

{fgcd}`<zoomable src="../../_static/img/base_modules_all.svg">`




## Core module

{fgcd}`<img src="../../_static/img/base_modules_core.png" width="50%" wrap-right ml="20px">`

The core module provides a few very fundamental features. 

`EntryPoint` class is doing what normally would be executed in the `main()` function. It is extracted to a separate class
to avoid implementing `main()` within the FGCDv2 framework. The implementation of the `main()` is left to each target,
which is useful for the unit test target. 

`Application` is a singleton class that has only static methods. It is used to initialize and terminate the application.
This allows to terminate the application from any place in the code. Similarly, it allows to reset (or power cycle) the 
whole program. Its main function during initialization is to create the root component (this is done by target's code) and
then verify integrity and start each component.  

`Exception` is used to define FGCDv2 exception class, inheriting from the standard exception. If the logger is already
available, throwing this exception will print the stack trace with demangled names, using `StackTrace` class.

The `Signals` file defines Linux signals names (which was lacking in the standard headers), the `types` file defines
a few basic types (usually aliases), and the `version` file is filled by the building system and is used to store
data about compilation (date, user, etc.).

### Start-up sequence 

{fgcd}`<img src="../../_static/img/base_startup.png" width="100%">`

There is only one root component and it is stored in the `Application` class. It is created by the `createRootComponent`
function, that is not implemented in the FGCDv2 and needs to be implemented by each target.

From the target perspective, the initialization is composed of three main steps: creating components, integrity check 
and starting components. 

In the `createRootComponent` the target chooses properties root, creates context (described later), loads configuration
and then creates all components. After that, the `Application` code performs integrity check - it will try to verify 
that all required setup was performed and check for all the things that cannot be checked during compilation, for 
example: if all the custom get/set callbacks for properties were set, if every required configuration option was provided
in the config file, etc. 
The last step is to start the components. The `Application` iterates over every component and executes `start()` function
on each component. It is not required and a component may not implement it. 

The idea behind this division (creating & starting) is that during creation everything is set up: memory is allocated, 
properties' callbacks are set, configuration options are checked and the glue logic is provided, by feeding one component
to another. After everything is set up and initialized, the components are started. The main purpose of the `start()` 
method is to create and run any threads the component may have. Similarly, during `stop()` the component has to stop its
threads and free any other resources (that are not automatically managed).



## Component module

{fgcd}`<img src="../../_static/img/base_modules_component.png" width="80%" center>`

The `Component` is a base class for all other components. Every component can have subcomponents and subcomponents have
to inherit from the `Component` class to be registered as subcomponents of a parent component using `addComponent` method.

Every component contains a reference to the parent (except `RootComponent` which points to itself), textual name for logging,
textual logging group (to be able to enable/disable entire logging group), and a reference to a context object. 

The `Context` object holds a few variables that should be accessible from every part of the code. The context can be 
accessed by `m_context` field inside of components or passed by reference to other code. These variables are stored 
inside the `Context` object, so a different context can be passed for mocking purposes in unit tests. 

The `Context` object is created by the `ContextBuilder`. The user first create the `ContextBuilder` objects, set it up and 
then passes it to the root component. The root component will construct `Context` object using the builder. This is 
necessary because `Context` holds the logger and logger itself is a component. This creates a *chicken and egg* problem - 
the context is needed to create a component, but a component (logger) is needed to constructs the context. The `ContextBuilder`
solves this problem with its Factory design pattern. 

The `RootComponent` is used to start and stop components - as all of them are its children. The instance of this class
is created in target's code and is then stored by the `Application` singleton.  



## Logging module

{fgcd}`<img src="../../_static/img/base_modules_logging.png" >`

The `Logger` component is a special case because it is created by the `RootComponent` and not by target's code.
It contains the `log` method that takes several parameters (like a file name, a line, the log message, etc.). The 
`registerConsumer` is used to add a log consumer that will take the log entry with a log message already formatted by 
the `Logger` and do something with this message (print to a terminal, save to a file, etc.). 
Furthermore, the logger allows to set the current log level and enable / disable logging of different logging groups. 



## Thread module

{fgcd}`<img src="../../_static/img/base_modules_thread.png" >`

The thread module defines a few helper types: `Affinity` (to represent CPU affinity), `Scheduling` to represent a 
scheduling policy and thread priority and the `ThreadInfo` class, that stores these information along with a thread's
textual name (for logging).

Then, the `ThreadInfo` class is used to define configuration information about every thread inside the `threads` file. 
It is one of few places (if not the only one) when the modularity of the design and independence of modules is
not strictly respected - the `threads` file contains information about all types of threads used inside FGCDv2 application,
so that it is easy (and possible) to have an overview over thread's priorities. 

The `ThreadInfo` defines a type of a thread (i.e. its configuration), not its instance. In most case there will be only
one thread of a given type, but it is not a design requirement.

In the `CurrentThread` namespace, there is a global variable `name` that is local to the thread and it is used to store
the name of a each thread for logging (which can then be accessed from any place in the code).

Finally, the `Thread` class is there to extend functionality of the `std::thread`, which it uses underneath. The OS thread
is created in the `start()` method, which also takes the thread configuration (`ThreadInfo` object) and sets all 
required options (affinity, policy, priority and thread name). 

The thread's function is executed in the `start()` method and it needs to take at least one boolean argument - a
*is running* variable. This atomic variable is used in the `stop()` methods and gives the thread function a chance to
exit "politely". Later, the `forceJoin()` method should be executed. It will wait for a given grace period (letting the
thread finish by itself) and then use `pthread_cancel` method to forcefully stop the thread and join it. 

It is designed that the `forceJoin()` method should be preceded by the `stop()` method. Both of which should be used in the 
`stop()` method of the `Component` class, during the application termination. 



## Stats module

{fgcd}`<img src="../../_static/img/base_modules_stats.png" width="90%" center>`

This module is used to provide statistics (counters). Each counter is wrapped in the `StatsField` class, which also stores
the name of the counter and provides basic arithmetic and casting operators, so that the `StatsField` objects can be used
as if they were integers. 

Each field has to belong a `StatsGroup`. Groups can also belong to other groups thus allowing to create a tree-like structure.
The context object contains the root group of all statistics, so each component can add its statistics to it. A component
can also pass its main group to its subcomponents and so on. 

The `IStatsItem` interface is there to make it possible to iterate over `StatsGroup` and `StatsField` in the same way.
The `StatsGroup` has a print method to serialize the tree into a string. This string shall also be accessible 
via a dedicated property. 




## Config module

{fgcd}`<img src="../../_static/img/base_modules_config.png" width="90%" center>`

The config module is responsible for setting up the configuration options of each module that requires configuration
(for example to set things like timing domain, timing events, disable RBAC, etc.).
Each configuration option is a property, defined in the same way as regular properties and gathered together in a 
single parent property (`configProp` in the drawing). That way we can reuse parsing capabilities of properties 
and at the same time it will be possible to see every config option the FGCDv2 was started with by getting this property.
Moreover, by iterating over this parent property, FGCDv2 will know which configuration options are allowed (available).
Each configuration property has a flag which says whether it is required. If it's not, it may have a default value.
If a required option is not provided, the integrity check will fail. 

There are two main ways to set these config properties - via a file and from a command line, handled respectively by
the `ConfigFileReader` and `CommandLineParser` classes. The command line takes precedence over the config file, 
overwriting any config option defined in the config file.

The config file format is JSON, so no custom parsing is needed. The config file itself might be generated by our
configuration tools (e.g. DeviceManager). It is read from a default path, which can be overridden with a command
line option (which itself is a config property as well).

The command line format is just a name of a property, followed by an equal sign and then a value. That way there is no 
need to define anywhere the available command line options. 
Ideally, the FGCDv2 should be started with no command line options - everything should be read from the config file.
The designed purpose of the command line options is debugging and developing.  





## Debugging module

{fgcd}`<img src="../../_static/img/base_modules_debugging.png" width="50%" wrap-right ml="20px">`

The debugging module contains some features that can be useful in finding errors or understanding an unexpected
behaviour. One of such feature is `trace` which provides macros to log entry and exit of the function, which allows
to follow the execution of the code. `profiling` on the other hand provides wrappers over timers from the *cpp_utils* 
library, to measure code execution time. 

Additionally, there is a class `DebugInfoCollector` that is a part of the context object. The idea of this class
is to collect different debugging information (like trace and debug logs) in a circular buffers. Then, either when 
some serious error happens or on demand, the collected data can be dumped into the log or be read via a property.
One cannot have trace and debug logs enabled all the time - they pollute the log file and it takes time to write to a 
file (especially on a network file system). Instead, this class provides a short, in-memory "log" of debug information 
that can be printed when needed. It's a bit like a stack trace or core dump, but with debug logs. 





## Misc module

{fgcd}`<img src="../../_static/img/base_modules_misc.png" width="50%" wrap-right ml="20px">`

This module contains miscellaneous features that are too simple to justify being in a separate modules.
It contains serialization functions (implementation of the serializers to work with the serializer from the *cpp_utils* 
library). The `clock` is there to provide current time to be used with the `std::chrono` methods. 






