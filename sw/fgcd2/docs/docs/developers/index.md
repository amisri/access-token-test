# FGCDv2 Developer Docs

This is the main document describing the architecture of the FGCDv2 Framework and its use-cases.


```{toctree}
:caption: Documentation
:maxdepth: 1

text/docs/writing
text/docs/syntax
text/docs/commands
```

```{toctree}
:caption: Build system
:maxdepth: 1

text/build/index
```

```{toctree}
:caption: Modules
:maxdepth: 1

text/modules/index
```