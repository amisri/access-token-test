# Syntax

We use an extended Markdown syntax in Sphinx, via MyST project, which provides reStructuredText (RST)
features with Markdown syntax.

MyST documentation can be found here: <https://myst-parser.readthedocs.io>


## Standard Markdown syntax

Standard Markdown syntax can be used to add basic elements, like headings and links.  

[Basic syntax guide](https://www.markdownguide.org/basic-syntax/)\
[Syntax cheat sheet](https://www.markdownguide.org/cheat-sheet/)


## Extended Markdown syntax

MyST adds more directives to the standard Markdown so it can be used in Sphinx.

[Cheat sheet](https://jupyterbook.org/en/stable/reference/cheatsheet.html)

[Typography](https://myst-parser.readthedocs.io/en/latest/syntax/typography.html)\
[Images](https://myst-parser.readthedocs.io/en/latest/syntax/images_and_figures.html)\
[Admonitions](https://myst-parser.readthedocs.io/en/latest/syntax/admonitions.html)\
[Tables](https://myst-parser.readthedocs.io/en/latest/syntax/tables.html)\
[Code blocks](https://myst-parser.readthedocs.io/en/latest/syntax/code_and_apis.html)\
[Cross-reference](https://myst-parser.readthedocs.io/en/latest/syntax/cross-referencing.html)\
[Math](https://myst-parser.readthedocs.io/en/latest/syntax/math.html)


[Table of content](https://myst-parser.readthedocs.io/en/latest/syntax/organising_content.html)\
[Extensions](https://myst-parser.readthedocs.io/en/latest/syntax/optional.html)


## Custom FGCDv2 syntax commands

Sphinx/MyST allows to add custom directives and roles written in Python that can later be used
within Markdown source. A *directive* is a block command, while a *role* is inline.

FGCDv2 adds one new role, called *fgcd*, which then have multiple commands written with HTML syntax.

If you want to add another command, see [](./commands) and **then extend this page to document your new directive**.



:::{admonition} Common options
:class: tip

mx, my, ml, mr, mt, mb
: Sets margin 

px, py, pl, pr, pt, pb
: Sets padding

width
: Sets width

height
: Sets height

:::


:::{admonition} **img** command
:class: note

```{code-block} html
:caption: Example

{fgcd}`<img src="../../_static/img/base_startup.png" width="100%">`
```
\
Inserts an image into text, with additional options as compared to the built-in image options, like wrapping
the images around the text and opening the image in the modal view after clicking on it.

src
: A path to the image.

wrap-left
: Places the image on the left and wraps the text around it.

wrap-right
: Places the image on the right and wraps the text around it.

center
: Centers the image and clears the text around it.

*common options*
: See above.

:::


:::{admonition} **zoomable** command
:class: note

```{code-block} html
:caption: Example

{fgcd}`<zoomable src="../../_static/img/devices_modules_all.svg">`
```
\
Inserts an image and enables a user to zoom and pan it. 

src
: A path to the image.

zoom
: Initial zoom value.

x
: Initial pan (x value).

y
: Initial pan (y value).

*common options*
: See above.

:::


:::{admonition} **div** command
:class: note

```{code-block} html
:caption: Example

{fgcd}`<div mb="3rem">`
```
\
Inserts an empty space in the document. 

*common options*
: See above.

:::


:::{admonition} **clear** command
:class: note

```{code-block} html
:caption: Example

{fgcd}`<clear mt="40px">`
```
\
Similarly to the *div* command, it inserts an empty space in the document, but it clears the text before itself, 
so that the text is not wrapped around it.
 

*common options*
: See above.

:::




:::{admonition} **colorbox** command
:class: note

```{code-block} html
:caption: Example

{fgcd}`<colorbox blue bold text="property">`
```
\
Inserts a colored box with text centered inside. Without specifying any color option, the box is white.

text
: A string to place inside the box.

bold
: Make the text bolded.

blue
: Use blue color scheme.

green
: Use green color scheme.

yellow
: Use yellow color scheme.

red
: Use red color scheme.

*common options*
: See above.

:::