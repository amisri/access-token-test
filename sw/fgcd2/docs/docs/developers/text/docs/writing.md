# Writing documentation

## Introduction

* Documentation is generated using [Sphinx](https://www.sphinx-doc.org/en/master/) project.
* The syntax used is MyST, [see here](./syntax) for more.
* The commands available in MyST are extended by custom FGCDv2 commands, [see here](./commands) for more.


## Where to add your files

* Main path of the documentation is `fgcd2\docs`.
* The `fgcd2\docs\config` folder contains custom CSS and JS files. It also contains custom Python extension
that is used to add custom [FGCDv2 commands](./commands).
* The documentation text is placed under `fgcd2\docs\docs`, where each documentation has its own folder, mainly 
`architecture`, `developers` (this docs), `specifications`, and `standard`.


## File structure 

Folder for each documentation (e.g., architecture) may contain folders:

* `diagrams` folder with source files for diagrams (e.g., drawIO files).
* `static` folder from which every file is copied to the output directory and may contain, for example, images
used in the documentation.
* `text` folder with documentation text written in Markdown files (*.md).

Additionally, it should contain 2 files:

* `conf.py` - Sphinx configuration file for this documentation.
* `index.md` - main entry-point Markdown file, that includes all files in the `text` directory, creating the table of contents.