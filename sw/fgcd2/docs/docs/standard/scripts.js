$(document).ready(function ()
{
    // Create dynamic navigation menu
    let nav = $("nav");
    let index = 0;

    $("h1,h2,h3").each(function ()
    {
        index++;
        $(this).attr("id", `item-${index}`);

        let className = "nav-"+$(this).prop("tagName");
        nav.append(`<div class="${className}"> <a class="nav-link" href="#item-${index}">${$(this).text()}</a></div>`);
    });

    // Insert code snippets
    $("[data-code]").each(function ()
    {
        let code = snippets[$(this).data("code")].trim().replaceAll('<','&lt;').replaceAll('>','&gt;');
        $(this).html(`<code class="language-cpp">${code}</code>`);
    });

    // Highlight code
    hljs.highlightAll();
});