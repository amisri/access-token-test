const snippets =
{

    "headerFile":
`
//! @file
//! @brief  Short description of the class or content of the file.
//! @author Your Name

#pragma once

#include <cpp_std_headers>
#include <and_linux_headers>
#include <vector>

#include <global_project_headers>
#include <each_group_sorted_alphabetically>
#include "../inc/module_or_project_headers.h"

namespace fgcd
{
    class SomeClass
    {
    }
}
`,


    "headerFullFile":
`
//! @file
//! @brief  Short description of the class or content of the file.
//! @author Your Name

#pragma once

#include <cpp_std_headers>
#include <and_linux_headers>
#include <vector>

#include <global_project_headers>
#include <each_group_sorted_alphabetically>
#include "../inc/module_or_project_headers.h"

// ************************************************************

#define macro definitions
#define macro functions definitions

// ************************************************************

namespace fgcd
{
    enum class SomeEnums
    {
    }

    // ************************************************************

    struct SomeSimpleStructs
    {
    }

    // ************************************************************

    class SomeClasses
    {
    }
}
`,


    "sourceFile":
`
//! @file
//! @brief  Short description of the class or content of the file.
//! @author Your Name

#include <same_rules_for_includes_as_in_header_files>;

using namespace fgcd;

// ************************************************************

void SomeClass::someMethod()
{
}

// ************************************************************

void SomeClass::someOtherMethod()
{
}
`,


    "separator":
`
// ************************************************************
`,


    "strongSeparator":
`
// ************************************************************
// * Entry point
// ************************************************************
`,


    "comments":
`
class Logger
{
    //! A description of what this function does.
    //! It can continue for multiple lines if needed.
    //!
    //! @param factor description of the parameter.
    //! @return true if succeeded, false otherwise.
    bool privateMethod(int factor)
    {
        // Check if the factor is correct
        if (factor < 10)
        {
            callOtherMethod(factor);
            return true;
        }          
        
        // Otherwise indicate error
        return false;
    }

    Thread                 m_thread;        //!< Thread that pops log entries from the log queue
    utils::CircularBuffer  m_log_queue;     //!< Circular buffer with log entries to be consumed
};
`,


    "spaces":
`
if (flag == SomeEnum::ready)
{
    result[idx] = (value1 * (array[4] + value3)) / value4;  
    result      = (condition < 5 ? 0 : 1);
      
    method<int>(result[idx], result);
}    
`,


    "alignment":
`
class Logger
{
    void method()
    {
        constexpr auto key        = "/modules";
        constexpr auto key_length = std::char_traits<char>::length(key);
        int            param      = 10;
    }

    private:
        Thread                              m_thread;                         //!< Thread that pops log entries from log queue.
        utils::CircularBuffer<LogEntryPtr>  m_log_queue;                      //!< Circular buffer with log entries to be consumed.
        LogType                             m_consumer     = LogType::full;   //!< Type of logs.
        utils::TemplateMap<LogType, bool>   m_enabled_type = false;           //!< Map that stores which log types are enabled.
};   
`,


    "params":
`
setDeviceStatus(someVeryLongNamespace::getGlobalId(), (flag ? 0 : 10), parameter1,  
                parameter2, parameter3, true, false);  
`,


    "initList":
`
Component::Component(RootComponent &root)
    : m_parent(*this), m_name("Root"), m_logger(getComponent<Logger>()),
      m_stats_root(root.m_stats_root), m_root(root)
{
}  
`,


    "consts":
`
const int  var;      // const int
const int& var;      // const reference to int

// Raw pointers should not be used anyway, but for completeness or integration with C language:
const int        var;      // const int
const int*       var;      // pointer to int that is const 
const int* const var;      // const pointer to int that is const
      int* const var;      // const pointer to int 
`,



    "funcQualifiers":
`
const int& func(const SomeType& param) const noexcept override
{
}
`,



    "simpleCondition":
`
if (param > 0 && param < 10)
{
    doSomething(param);
}

if (flag_1)
{
}
else if ((!flag_2) && some_other_flag)
{
}
else
{
}
`,



    "condition":
`
if ( ((param >= 0 && param <= 10) || (param >= 50 && param <= 100)) &&
     thisQuiteLongNameSpace::someChecker(param) )
{
    doSomething(param);
}
`,



    "loops":
`
for (int i = 0; i < 10; ++i)
{
}

for (const auto& element : vector)
{
}

while (!is_disabled)
{
}
`,



    "switchCase":
`  
switch (state)  
{  
    case State::idle:  
        processIdle();  
        break;  

    case State::cycling:  
        [[fallthrough]]
  
    case State::running:  
        processRunning();  
        break;  
  
    default:  
        break;  
}  

switch (state)  
{  
    case State::idle:    processIdle();    break;  
    case State::cycling: [[fallthrough]]      
    case State::running: processRunning(); break; 
    default:                               break;   
}  
`,


    "lambda":
`  
// Regular lambda
thread.start(params, [this](std::atomic_bool& keep_running)
{
    while (keep_running) 
    {
        this->threadFunc();
    }
});  

// Very short lambda
auto found = std::find_if(begin_i, end_i, [&](auto p) { return p.first == signal; });
`,



    "templates":
`  
template<LogType log_type, typename Format, typename... Args>
void log(const std::string_view file_name, const int line, const std::string_view component_name,
         const Format& format, Args&&... args)
{
    genericLog<log_type>(file_name);
}
`,



    "classStructure":
`  
class SomeClass : public someBaseClass
{
    // Declarations of constexpr values
    // Declaration of internal types
    // Declaration of type aliases (using)
    // Declarations of friendship
    
  public:
    // Constructors
    // Special constructors (copy, move)
    // Destructor
    // Operators
    // Override methods and interface implementations
    // All other methods 
    
  protected:
    // Methods in the same order as above
    
  private:
    // Methods in the same order as above
    
    // Private fields (variables)
};
`,




    "classExample":
`  
template<typename T>
class SomeClass : public SomeBaseClass<T>
{
    static constexpr auto some_value = 5.0;
    static constexpr auto more_value = 5.01;

    struct Point
    {
        int x = 0;
        int y = 0;
    };

    using Iterator = SomeBaseClass<T>::iterator;

    friend SomeOtherClass;

  public:
    //! Constructs the class with a default second parameter.
    //!
    //! @param param Description of param 1.
    explicit SomeClass(const int param = 0)
        : SomeClass(param, 0)
    {}

    // ************************************************************

    //! Constructs the class with given parameters.
    //!
    //! @param param_1 Description of param 1
    //! @param param_2 Description of param 2
    SomeClass(const int param_1, const int param_2)
        : m_param_1(param_1), m_param_2(param_2)
    {}

    // ************************************************************

    //! Implementation of SomeBaseClass method.
    //!
    //! @return Parameter considered to be the main one.
    int getMainParam() const override
    {
        return m_param_1;
    }

    // ************************************************************

    //! Returns parameters.
    //!
    //! @return SomeClass parameters.
    std::tuple<int, int> getParams() const
    {
        return {m_param_1, m_param_2};
    }

  private:
    //! The method that does nothing.
    void doNothing()
    {
    }

    int m_param_1 = 0;      //!< Some param 1 description
    int m_param_2 = 0;      //!< Some param 2 description
};
`,


    "fullExample":
`  
//! @file
//! @brief  A complete example for the CCS C++ code style.
//! @author Dariusz Zielinski

#pragma once

#include <fmt/ostream.h>
#include <vector>

#include <cpp_utils/circularBuffer.h>
#include <cpp_utils/templateMap.h>
#include <interfaces/ILogConsumer.h>
#include <modules/base/component/inc/Component.h>
#include <modules/base/thread/inc/Thread.h>
#include "LogEntry.h"

// ************************************************************

// Logging macros
#define    logInfo(format, ...)     FGCD_LOG(LogType::info,    format  __VA_OPT__(,) __VA_ARGS__)
#define   logError(format, ...)     FGCD_LOG(LogType::error,   format, __VA_OPT__(,) __VA_ARGS__)

// ************************************************************

namespace fgcd
{
    constexpr int max_index = 10;

    // ************************************************************

    enum class LogType
    {
        debug,
        info,
        error,
    };

    // ************************************************************

    //! Some class that does some things in some way.
    //!
    //! @tparam index magic value.
    //! @tparam T underlying type of SomeClass.
    template<int index, typename T>
    class SomeClass : public SomeBaseClass<T>
    {
        static_assert(index <= max_index, "Index of SomeClass is out of valid range.");

        static constexpr auto some_value = 5.0;
        static constexpr auto more_value = 5.01;

        struct Point
        {
            int x = 0;
            int y = 0;
        };

        using Iterator = SomeBaseClass<T>::iterator;

        friend SomeOtherClass;

      public:
        //! Constructs the class with a default second parameter.
        //!
        //! @param param Description of param 1.
        explicit SomeClass(const int param = 0)
                : SomeClass(param, 0)
        {}

        // ************************************************************

        //! Constructs the class with given parameters.
        //!
        //! @param param_1 Description of param 1.
        //! @param param_2 Description of param 2.
        SomeClass(const int param_1, const int param_2)
                : m_param_1(param_1), m_param_2(param_2)
        {}

        // ************************************************************

        //! Implementation of SomeBaseClass method.
        //!
        //! @return Parameter considered to be the main one.
        int getMainParam() const override
        {
            return m_param_1;
        }

        // ************************************************************

        //! Find parameter in the given container.
        //!
        //! @param container container in which to look for the parameter.
        //! @return Search result or container::end() if parameter has not been found.
        auto findParam(const std::vector<int>& container) const override
        {
            return std::find_if(container.begin(), container.end(), [&](auto e) { return e >= m_param_1; });
        }

        // ************************************************************

        //! Returns parameters.
        //!
        //! @return SomeClass parameters.
        std::tuple<int, int> getParams() const
        {
            return {m_param_1, m_param_2};
        }

        // ************************************************************

        //! Returns string representation of the type if the given index is withing the range of the class's index.
        //!
        //! @tparam someParam some magic value.
        //! @param type type of the log to convert to string.
        //! @param type_index another some magic value.
        //! @return string representation of the type.
        template<int someParam>
        std::optional<std::string> processType(const LogType type, const int type_index) const
        {
            if ( (type_index >= (-index) && type_index < index) || (someParam > 0) )
            {
                switch (type)
                {
                    case LogType::debug  return "debug";
                    case LogType::info   return "info";
                    case LogType::error  return "error";
                    default:             return "";
                }
            }
            else if (someParam == 0)
            {
                return "";
            }

            return {};
        }

      private:
        //! The method that does nothing.
        void doNothing()
        {
        }

        int m_param_1 = 0;      //!< Some param 1 description.
        int m_param_2 = 0;      //!< Some param 2 description.
    };
}
`,


    "numberFormatting":
`  
uint32_t      cpu_frequency = 1’500’000’000;         // Formatting of long numbers
unsigned auto binary_value  = 0b10101010;        // Binary format
`,


    "lazyEval":
`  
// Relying on lazy evaluation and putting side effect in the if
if (ptr != nullptr && funcThatHasSideEffect(ptr))
{
    doSomething();
}

// "if" shuld be there just to check condition not to declare expression (it should have no side affects).
// Relying on lazy evaluation (when ptr is nullptr) of the second condition is not elegant.
// Instead write:
if (ptr != nullptr)
{
    bool result = funcThatHasSideEffect(ptr);
    
    if (result)
    {
        doSomething()
    }
}

// It may be more verbose, but it is explicit and unambiguous
`,


};