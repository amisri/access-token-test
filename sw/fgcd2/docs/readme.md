# FGCDv2 documentation

### How to generate documentation

Use *"fgcd.sh"* script with a command "docs" to generate (and deploy) documentation.

### Directory structure

- config - common files to use across documentations

- docs - documentations
	- main - landing page for the documentation
	- codeStandard - a document describing C++ code style and some rules and guidelines.
	- architecture - description of the FGCDv2 architecture 
	- developers - documentation for FGCDv2 developers
