// Zoom library: https://github.com/anvaka/panzoom


window.addEventListener("load", function()
{
    prevNextButtons("Go to next page");
    prevNextButtons("Go to previous page");


    document.querySelector("header > div").innerHTML += '<a href="https://accwww.cern.ch/fgcd2" title="Go back to the main page"><div id="fgcdDocsButton">FGCDv2 Docs</div></a>';

    document.querySelectorAll(".zoomable").forEach(item =>
    {
        imgZoom(item);
    });

    document.querySelectorAll(".fgcd-img").forEach(item =>
    {
        regularImg(item);
    });

    let imgView = document.createElement("div");
    imgView.id = 'fgcd-imgView';
    imgView.addEventListener('click', (event) => { imgView.style.visibility = "hidden"; });
    document.body.appendChild(imgView);

    document.addEventListener('keydown', (event) =>
    {
        if (event.key === 'Escape')
        {
            imgView.style.visibility = "hidden";
            document.querySelector(".zoomedFull").classList.remove("zoomedFull");
            document.querySelector(".zoomableFull").innerHTML("See in full size");
        }
    });
});


function prevNextButtons(title)
{
    document.querySelectorAll(("a[title='" + title + "'")).forEach(link =>
    {
        link.parentElement.style.cursor = "pointer";
        link.parentElement.addEventListener('click', function (e){ window.location = link.getAttribute("href")});
    });
}


function imgZoom(element)
{
    let ctrlFull  = element.getElementsByClassName('zoomableFull')[0];
    let infoZoom  = element.getElementsByClassName('zoomableZoom')[0];
    let infoX     = element.getElementsByClassName('zoomableX')[0];
    let infoY     = element.getElementsByClassName('zoomableY')[0];

    let initZoom = parseFloat(element.dataset.zoom);
    let initX = parseInt(element.dataset.x);
    let initY = parseInt(element.dataset.y);

    let instance = panzoom(element.getElementsByTagName('img')[0],
    {
        bounds: true,
        smoothScroll: false,
        zoomDoubleClickSpeed: 1,
        initialZoom: initZoom,
        initialX: initX,
        initialY: initY,

        onDoubleClick: () => { return false; },

        beforeWheel: (e) => { return !(e.ctrlKey || e.shiftKey || e.altKey || element.classList.contains("zoomedFull")); },
    });

    instance.on('transform', function(e)
    {
        infoZoom.innerHTML = "Zoom: " + (Math.round(e.getTransform().scale * 100)/100);
        infoX.innerHTML = "X: " + Math.round(e.getTransform().x);
        infoY.innerHTML = "Y: " + Math.round(e.getTransform().y);
    });

    infoZoom.onclick = () => { instance.moveTo(0, 0); instance.smoothZoomAbs(0, 0, 1); };
    infoX.onclick     = () => { instance.smoothMoveTo(0, 0); };
    infoY.onclick     = () => { instance.smoothMoveTo(0, 0); };

    element.ondblclick = () => { instance.moveTo(0, 0); instance.smoothZoomAbs(0, 0, 1);}

    ctrlFull.onclick = () =>
    {
        element.classList.toggle("zoomedFull");
        updateZoomButtons(element, ctrlFull);
    }

    element.onclick = (e) =>
    {
        if (e.target === element)
        {
            element.classList.remove("zoomedFull");
            updateZoomButtons(element, ctrlFull);
        }
    }
}

function updateZoomButtons(element, button)
{
    button.innerHTML = element.classList.contains("zoomedFull") ? "Close full size view" :  "See in full size";
}

function regularImg(element)
{
    element.onclick = (e) =>
    {
        let url = element.getAttribute("src");
        let imgView = document.querySelector("#fgcd-imgView");
        imgView.style.backgroundImage = 'url("'+url+'")';
        imgView.style.visibility = "visible";
    };
}