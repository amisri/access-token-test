from html.parser import HTMLParser
from docutils import nodes
from docutils.parsers.rst import Directive
from sphinx.util.docutils import SphinxDirective


class FgcdHtmlParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.tag = ""
        self.data = {}

    def get(self, key, default_value=""):
        if key in self.data:
            return self.data[key]
        else:
            return default_value

    def html(self, key, default_value=""):
        if key in self.data:
            return f'{key}="{self.data[key]}" '
        elif default_value:
            return f'{key}="{default_value}" '
        else:
            return ""

    def css(self, key, default_value=""):
        if key in self.data:
            return f'{key}: {self.data[key]}; '
        elif default_value:
            return f'{key}: {default_value}; '
        else:
            return ""

    def css_name(self, key, name):
        if key in self.data:
            return f'{name}: {self.data[key]}; '
        else:
            return ""

    def has(self, key):
        return key in self.data

    def handle_starttag(self, tag, attrs):
        self.tag = tag
        for attr in attrs:
            self.data[attr[0]] = attr[1]


def spacing_helper(args, key, name):
    style = args.css_name(key, name)

    style += args.css_name(f"{key}x", f"{name}-left")
    style += args.css_name(f"{key}x", f"{name}-right")
    style += args.css_name(f"{key}y", f"{name}-top")
    style += args.css_name(f"{key}y", f"{name}-bottom")
    style += args.css_name(f"{key}l", f"{name}-left")
    style += args.css_name(f"{key}r", f"{name}-right")
    style += args.css_name(f"{key}t", f"{name}-top")
    style += args.css_name(f"{key}b", f"{name}-bottom")

    return style


def common_css(args):
    return spacing_helper(args, "m", "margin") + spacing_helper(args, "p", "padding") \
           + args.css("width") + args.css("height")


class Zoomable:
    fgcd_tag = "zoomable"

    @staticmethod
    def run(args):
        style = "width: 100%;"
        style += common_css(args)

        zoom = args.get("zoom", "1.0")
        x = args.get("x", "0")
        y = args.get("y", "0")

        html = f"""
        <div class="zoomable" style="{style}" data-zoom="{zoom}" data-x="{x}" data-y="{y}">
            <div class="zoomableImg">
                <img  src="{args.get("src")}">
            </div>
            <div class="zoomableCtrl">
                <div class="zoomableFull">See in full size</div>
                <div class="zoomableHint">Use CTRL+Scroll to zoom</div>
                <div class="zoomableInfo">
                    <span class="zoomableZoom" title="Reset zoom">Zoom: 1.0</span>
                    <span class="zoomableX" title="Reset pan">X: 0</span>
                    <span class="zoomableY" title="Reset pan">Y: 0</span>
                </div>
            </div>
        </div>"""

        return [nodes.raw('', html, format='html')]


class Img:
    fgcd_tag = "img"

    @staticmethod
    def run(args):
        attributes = ""
        style = common_css(args)

        # Text wrapping
        if args.has("wrap-left"):
            style += "float: left; "
        elif args.has("wrap-right"):
            style += "float: right; "
        elif args.has("center"):
            style += "margin-left: auto; margin-right: auto; clear: both;"

        html = f"""
        <img class="fgcd-img {args.get("class")}" src="{args.get("src")}" {attributes} style="{style}">
        """

        return [nodes.raw('', html, format='html')]


class Clear:
    fgcd_tag = "clear"

    @staticmethod
    def run(args):
        style = common_css(args)

        return [nodes.raw('', f'<div style="clear: both"></div><div style="{style}"></div>', format='html')]


class Div:
    fgcd_tag = "div"

    @staticmethod
    def run(args):
        style = common_css(args)

        return [nodes.raw('', f'<div style="{style}"></div>', format='html')]


class ColorBox:
    fgcd_tag = "colorbox"

    @staticmethod
    def run(args):
        classes = args.get("class")
        style = common_css(args)

        # Text wrapping
        if args.has("blue"):
            classes += " fgcd-colorbox-blue"
        elif args.has("green"):
            classes += " fgcd-colorbox-green"
        elif args.has("yellow"):
            classes += " fgcd-colorbox-yellow"
        elif args.has("red"):
            classes += " fgcd-colorbox-red"

        if args.has("bold"):
            classes += " fgcd-colorbox-bold"

        html = f"""
        <div class="fgcd-colorbox {classes}" style="{style}">{args.get("text")}</div>
        """

        return [nodes.raw('', html, format='html')]


def fgcd_role(name, rawtext, text, lineno, inliner):
    parser = FgcdHtmlParser()
    parser.feed(text)

    commands = [Zoomable(), Img(), Clear(), Div(), ColorBox()]

    for command in commands:
        if command.fgcd_tag.lower() == parser.tag.lower():
            return command.run(parser), []

    return [nodes.raw('', f"<b>FGCD role error: Unrecognized tag {text}</b>", format='html')], []


def setup(app):
    app.add_role("fgcd", fgcd_role)

    return {
        'version': '1.0',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }


