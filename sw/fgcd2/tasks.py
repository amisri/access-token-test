from fabric import Connection
from invoke import Collection, task, Context
import os
import re
import sys
import time

build_dir      = "build"
control_script = "fgcd.sh"

# Utils Functions

def sync_file(c, dir, event, filename):
    relative_path = os.path.relpath(dir, c.root_path)
    remote_path   = os.path.join(c.remote_root_path, relative_path)
    remote_pc     = c.builder_pc

    if event == "CREATE":
        local_path = os.path.join(dir, filename)
    elif event == "MOVED_FROM":
        local_path = os.path.join(dir, filename)
    elif event == "DELETE":
        local_path = dir
    else:
        print(f"Unhandled event '{event}'")
        return

    try:
        c.run(f"rsync -azh --delete {local_path} {remote_pc}:{remote_path}", pty=True, echo=True)
    except:
        print("Error while transfering files")

#####################################################
## PyInvoke support prefix
#####################################################
        
def run_with_prefix(c, command, prefix):
    modified_command = f"{prefix} {command}"
    c.run(modified_command)


def add_prefix_to_run(original_run):
    def wrapped_run(c, *args, **kwargs):
        prefix = getattr(c, 'prefix', '')  # Get the prefix from the Context object
        if prefix:
            args = [f"{prefix} '{arg}'" for arg in args]
        original_run(c, *args, **kwargs)
    return wrapped_run

def run_wrapper(original_run):
    tabs_align = "tabs 10,30,40,100,130,160"
    color_logs = """
sed \
-e "s/FATAL/"$'\e[31m'"&"$'\e[m'"/" \
-e "s/ERROR/"$'\e[31m'"&"$'\e[m'"/" \
-e "s/WARNING/"$'\e[33m'"&"$'\e[m'"/" \
-e "s/INFO/"$'\e[32m'"&"$'\e[m'"/" \
-e "s/DEBUG/"$'\e[34m'"&"$'\e[m'"/" \
-e "s/] /]\t/g" 
"""
    def wrapped_run(c, *args, **kwargs):
        cmd = args[0]
        if "format" in kwargs and kwargs["format"] == True:
            cmd = f"{tabs_align}; {cmd} | {color_logs}"
            del kwargs["format"]
        return original_run(c, cmd, **kwargs)
    return wrapped_run

Connection.run = add_prefix_to_run(Connection.run)
Context.run = run_wrapper(Context.run)

# Tasks


@task
def sync(c):
    local_path  = f"{c.repo_root_path}"
    remote_path = f"{c.builder_pc}:{c.remote_repo_root_path}/.."
    exclude_opt = "{__pycache__,build,venv,doc,hw,sw/clients,4913,.*,def}"

    c.run(f"rsync -av --delete --exclude={exclude_opt} {local_path} {remote_path}", pty=True, echo=True)


@task
def load(c, target):
    compile_commands = "compile_commands.json"
    path = os.path.join(os.getcwd(), "build", target, compile_commands)
    c.run(f"ln -sf {path} {compile_commands}", pty=True)


@task
def watch(c):
    sync(c)

    exclude_opt = "(__pycache__|build|venv|doc|hw|sw/clients|4913|.*\/\.\S*)"
    while True:
        command_result = c.run(f"inotifywait -qre modify,create,delete,move {c.repo_root_path} --exclude='{exclude_opt}'", hide=True)

        time.sleep(1)

        result = re.search("(\\S*) (\\S*) (\\S*)", command_result.stdout)
        sync_file(c, *result.groups())


@task
def build(c, target, remote=False, clean=False, config_opts={}, color=True):
    if remote == True:
        target_path      = os.path.join(c.remote_root_path, "source", "targets", target)
        build_path       = os.path.join(c.remote_root_path, build_dir, target)
        cmake            = "cmake3"

        remote_root_path = c.remote_root_path

        c = Connection(c.builder_pc)
        c.prefix = "scl enable devtoolset-11"
    else:
        target_path = os.path.join(c.root_path, "source", "targets", target)
        build_path  = os.path.join(c.root_path, build_dir, target)
        cmake       = "cmake"


    config_opts["CMAKE_EXPORT_COMPILE_COMMANDS"] = 1
    config_opts_cmd = ' '.join([f"-D{x}={y}" for x, y in config_opts.items()])

    if clean:
        c.run(f"rm -r {build_path}")
    c.run(f"{cmake} -S {target_path} -B {build_path} {config_opts_cmd}", pty=color)
    c.run(f"{cmake} --build {build_path} --parallel", pty=color)

    load(c, target)


@task
def test(c, module, test, repeat=1, stop=0, color=True):
    build_opts = {"CMAKE_BUILD_TYPE": "Debug", "SELECTED_MODULE": module}
    build(c, "unit_tests", config_opts=build_opts, color=color)

    test_opts = {
            "gtest_color":            "yes",
            "gtest_filter":           test,
            "gtest_repeat":           repeat,
            "gtest_break_on_failure": stop
    }
    test_opts_cmd = ' '.join([f"--{x}={y}" for x, y in test_opts.items()])
    test_cmd      = f"sudo ./build/unit_tests/unit_tests {test_opts_cmd}"
    c.run(test_cmd, pty=color, format=color)
