from utils.executeForTargets import *
from projectControl.utils import *


@execute_for_targets
class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("build", description="Builds the project.", help="Builds the project.")
        cls.subparser.add_argument("-c", "--compile", action="store_true", help="Do not run cmake generation and only compile.")
        cls.subparser.add_argument("-v", "--verbose", action="store_true", help="Show make commands.")

    @classmethod
    def run(cls, args, cfg, project_args):
        logging.info(f"Building target '{args.target}':\n")

        result = build_project(args.cmake, args.target_build_path, args.target_path, f"Failed to build target {args.target}",
                               only_compile=args.compile, verbose=args.verbose)

    @classmethod
    def after(cls, targets):
        logging.success(f"All targets ({', '.join(targets)}) built successfully")
