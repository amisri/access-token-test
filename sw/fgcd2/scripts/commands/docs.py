import subprocess

from projectControl.utils import *


class Config:
    DEPLOY_USER = "pclhc"
    DEPLOY_HOST = "cs-ccr-teepc2.cern.ch"
    DEPLOY_PATH = "/user/pclhc/public_html/fgcd2"


class Command:

    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("docs", help="Generates and deploys documentation.")

        cls.subparser.add_argument("-s", "--show", action="store_true",
                                   help="Open a web browser with the locally generated documentation.")

        cls.subparser.add_argument("-f", "--full", action="store_true",
                                   help="Build and deploy the documentation as a website after generation.")

        cls.subparser.add_argument("-d", "--deploy", action="store_true",
                                   help="Don't build, only deploy the already built documentation.")

        cls.subparser.add_argument("-b", "--build", action="store",
                                   help="Build only one documentation. "
                                        "Valid options: index, standard, arch, specs, dev")

        cls.subparser.add_argument("-l", "--live", action="store",
                                   help="Use sphinx autobuild to have an auto-updated, local documentation. "
                                        "Valid options: arch, specs, dev")

    @classmethod
    def run(cls, args, cfg, project_args):
        docs_path = project_args["docs_path"]
        build_dir = os.path.join(cfg.build_dir, "docs")
        fatal_if_missing(docs_path, f"Path to the documentation ({docs_path}) does not exist.")

        if args.build and (args.full or args.deploy):
            fatal("Cannot use -b with -d or -f options.")

        if args.deploy:
            cls.deploy(build_dir)
            return

        # If live auto-update is requested, run it and quit
        if args.live:
            if args.live.lower() == "arch":
                Arch(docs_path, build_dir).live()
                exit(0)
            elif args.live.lower() == "specs":
                Specs(docs_path, build_dir).live()
                exit(0)
            elif args.live.lower() == "dev":
                DevDocs(docs_path, build_dir).live()
                exit(0)
            else:
                fatal(f"Invalid documentation name ('{args.live}') for live update option.")

        # Define different documentations to generate / deploy
        docs = [Index(docs_path, build_dir),
                CodeStandard(docs_path, build_dir),
                Arch(docs_path, build_dir),
                Specs(docs_path, build_dir),
                DevDocs(docs_path, build_dir)]

        # Check source path, create output path and generate each (or one) documentation
        for doc in docs:
            if not args.build or args.build.lower() == doc.id:
                fatal_if_missing(doc.source_path, f"Path ({doc.source_path}) to the source of ({doc.name}) does not exist.")
                pathlib.Path(doc.gen_path).mkdir(parents=True, exist_ok=True)

                doc.generate()
                logging.success(f"Successfully generated {doc.name}")

        # Deploy if requested
        if args.full:
            cls.deploy(build_dir)

        # Open in a browser
        if args.show:
            index_path = os.path.join(build_dir, "index.html")
            if args.build:
                index_path = os.path.join(build_dir, args.build, "index.html")

            logging.info(f"Opening: {index_path}")
            open_by_system(index_path)

    @classmethod
    def deploy(cls, build_dir):
        fatal_if_missing(build_dir, f"Path to the generated documentation ({build_dir}) does not exist.")
        runCommand(["rsync", "--progress", "-avzh", f"{build_dir}/",
                    f"{Config.DEPLOY_USER}@{Config.DEPLOY_HOST}:{Config.DEPLOY_PATH}"])

        logging.success(f"Documentation deployed successfully")


class Index:

    def __init__(self, docs_path, build_dir):
        self.id = "index"
        self.name = "Index (landing page)"
        self.common_path = os.path.join(docs_path, "config", "css", "fgcdCommon.css")
        self.source_path = os.path.join(docs_path, "docs", "index")
        self.gen_path = os.path.join(build_dir)

    def generate(self):
        runCommand(["rsync", "-avzh", f"{self.common_path}", self.gen_path])
        runCommand(["rsync", "-avzh", f"{self.source_path}/", self.gen_path])


class CodeStandard:

    def __init__(self, docs_path, build_dir):
        self.id = "standard"
        self.name = "C++ Code Standard document"
        self.source_path = os.path.join(docs_path, "docs", "standard")
        self.gen_path = os.path.join(build_dir, "standard")

    def generate(self):
        runCommand(["rsync", "-avzh", f"{self.source_path}/", self.gen_path])


class SphinxDocs:
    def __init__(self, docs_path, source_path, gen_path):
        self.docs_path = docs_path
        self.source_path = source_path
        self.gen_path = gen_path

    def before_generate(self):
        pass

    def generate(self):
        self.before_generate()

        # Required Python dependencies: sphinx, myst-parser, sphinxawesome-theme

        # Build docs
        runCommand(["sphinx-build", "-a", "-j", "auto", "-b", "html", self.source_path, self.gen_path])

    def live(self):
        # Required Python dependencies: sphinx, myst-parser, sphinxawesome-theme, sphinx-autobuild

        # Run autobuild
        # -a "--watch", os.path.join(self.docs_path, "config")
        runCommand(["sphinx-autobuild", "-a", "-j", "auto", "-E", "--port", "0", "--watch", os.path.join(self.docs_path, "config"), self.source_path, self.gen_path])


class Arch(SphinxDocs):
    def __init__(self, docs_path, build_dir):
        self.id = "arch"
        self.name = "FGCDv2 Architecture"
        super().__init__(docs_path,
                         os.path.join(docs_path, "docs", "architecture"), os.path.join(build_dir, "arch"))


class Specs(SphinxDocs):
    def __init__(self, docs_path, build_dir):
        self.id = "specs"
        self.name = "Specifications"
        super().__init__(docs_path,
                         os.path.join(docs_path, "docs", "specifications"), os.path.join(build_dir, "specs"))


class DevDocs(SphinxDocs):
    def __init__(self, docs_path, build_dir):
        self.id = "dev"
        self.name = "FGCDv2 Developer Docs"
        super().__init__(docs_path,
                         os.path.join(docs_path, "docs", "developers"), os.path.join(build_dir, "dev"))


