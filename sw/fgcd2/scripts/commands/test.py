import subprocess

from utils.findTargetExe import *
from projectControl.unitTests import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("test", help="Runs unit tests.")

        UnitTests.add_arguments(cls.subparser)

        cls.subparser.add_argument("modules", action="store", nargs="*",
                                   help="Runs tests only for the selected modules.")

        cls.subparser.add_argument("--build_only", action="store_true", 
                                   help="Only builds the test target.")

    @classmethod
    def run(cls, args, cfg, project_args):
        # Prepare target paths
        target_path = os.path.join(project_args['target_dir'], project_args['test_target'])
        target_build_path = os.path.join(cfg.build_dir, project_args['test_target'])

        # Handle modules selection
        config_args = f" -DSELECTED_MODULE=\"{ ' '.join(args.modules) if args.modules else '.' }\""

        # Configure and build test target
        UnitTests.build(target_path, target_build_path, args, config_args=config_args)

        if args.build_only:
            logging.success(f"Test target built")
            return

        # Run tests
        exe = find_target_exe(cfg, project_args, project_args["test_target"])
        result = UnitTests.run(exe, args.filter, args.number, args.stop_on_failure, args.leaks, args.args)

        if result.returncode == 0:
            logging.success(f"All unit tests passed")
        else:
            fatal("Unit tests failed")
    