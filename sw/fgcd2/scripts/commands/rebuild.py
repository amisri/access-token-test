from utils.executeForTargets import *
from projectControl.utils import *


@execute_for_targets
class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("rebuild", help="Rebuilds the project.",
                                          description="Cleans build directory and rebuilds the project.")
        cls.subparser.add_argument("-v", "--verbose", action="store_true", help="Show make command.")

    @classmethod
    def run(cls, args, cfg, project_args):
        logging.info(f"Rebuilding target '{args.target}':\n")

        config_args = "--log-level=VERBOSE" if args.verbose else ""
        build_args = "-- VERBOSE=1" if args.verbose else ""

        remove_dir(args.target_build_path)
        result = build_project(args.cmake, args.target_build_path, args.target_path, f"Failed to rebuild target {args.target}",
                               config_args=config_args, build_args=build_args)

    @classmethod
    def after(cls, targets):
        logging.success(f"All targets ({', '.join(targets)}) rebuilt successfully")

