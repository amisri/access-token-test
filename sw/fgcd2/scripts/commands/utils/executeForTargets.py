import sys
import os
import glob

from projectControl.utils import *


def execute_for_targets(cls):
    class DecoratedCommand(cls):
        def init(parser):
            cls.init(parser)
            cls.subparser.add_argument("targets", nargs="*", help="List of targets. Leave empty to select all available targets.")

        def run(args, cfg, project_args):
            # If no target was given, find targets in project_args['target_dir']
            if len(args.targets) == 0:
                targets = [ os.path.dirname(os.path.relpath(target, project_args['target_dir']))
                            for target in glob.glob(f"{project_args['target_dir']}/**/CMakeLists.txt", recursive=True)]
                updateArgsNamespace(args, 'targets', targets)

            # For every target, find its path and run decorated function
            for target in args.targets:
                target_path = os.path.join(project_args['target_dir'], target)
                target_build_path = os.path.join(cfg.build_dir, target)

                fatal_if_missing(target_path, f"Target at '{target_path}' does not exists")
                fatal_if_missing(os.path.join(target_path, "CMakeLists.txt"), f"{target_path} does not contain CMakeLists.txt")

                updateArgsNamespace(args, 'target_path', target_path)
                updateArgsNamespace(args, 'target_build_path', target_build_path)
                updateArgsNamespace(args, 'target', target)

                cls.run(args, cfg, project_args)

            if hasattr(cls, "after"):
                cls.after(args.targets)

    return DecoratedCommand


