import sys
import os
import glob

from projectControl.utils import *


def find_target_exe(cfg, project_args, target):
    target_path = os.path.join(project_args['target_dir'], target)
    target_build_path = os.path.join(cfg.build_dir, target)

    fatal_if_missing(target_path, f"Target at '{target_path}' does not exists")
    fatal_if_missing(target_build_path, f"Target at '{target_build_path}' does not exists. Have you built project?")

    target_build_path_info = os.path.join(target_build_path, "exe.txt")
    fatal_if_missing(target_build_path_info, f"Cannot find information about target (exe.txt). Have you built project?")

    with open(target_build_path_info) as f:
        target_exe = f.read().strip()

    fatal_if_missing(target_exe, f"Target executable ({target_exe}) does not exist. Have you built project?")

    return target_exe







