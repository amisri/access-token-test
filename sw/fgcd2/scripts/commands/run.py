from utils.findTargetExe import *
from projectControl.utils import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("run", description="Runs target's binary.",
                                          help="Runs target's binary.")

        cls.subparser.add_argument("target", help="Target to run.")
        cls.subparser.add_argument("arguments", nargs="*", help="Optional arguments to pass to the target.")

    @classmethod
    def run(cls, args, cfg, project_args):
        target_exe = find_target_exe(cfg, project_args, args.target)

        runCommand([target_exe] + args.arguments, sudo=True)




