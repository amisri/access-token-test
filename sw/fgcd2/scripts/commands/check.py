import subprocess
import os

from utils.findTargetExe import *
from projectControl.staticCheck import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("check", help="Run static checks for the target.")

        cls.subparser.add_argument("target", help="Target to run.")

        cls.staticChecks = StaticCheck(cls.subparser)

    @classmethod
    def run(cls, args, cfg, project_args):
        root_fgcd_dir = os.getcwd()

        # Set config file path
        config_dir = os.path.join(cfg.project_args['target_dir'], args.target)
        config_file = os.path.join(config_dir, "config.json")
        config_path = config_dir if os.path.exists(config_file) else root_fgcd_dir

        # Set compilation db path
        db_dir = os.path.join(cfg.build_dir, args.target)
        db_path = db_dir if os.path.exists(db_dir) else None

        cls.staticChecks.run(args=args, db_path=db_path, config_path=config_path)
