from utils.executeForTargets import *
from projectControl.utils import *


@execute_for_targets
class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("clean", description="Cleans build directory for selected targets.",
                                          help="Cleans build directory for selected targets.")
        cls.subparser.add_argument("-a", "--all", action="store_true", help="Clean the whole 'build' directory.")
        cls.subparser.add_argument("-d", "--docs", action="store_true", help="Remove generated documentation.")

        cls.first_target = True
        cls.removed = "targets: "

    @classmethod
    def run(cls, args, cfg, project_args):
        if args.all:
            if cls.first_target:
                cls.removed = f"directory {cfg.build_dir}"
                remove_dir(cfg.build_dir)
        if args.docs:
            if cls.first_target:
                docs_path = os.path.join(cfg.build_dir, "docs")
                cls.removed = f"docs directory {docs_path}"
                remove_dir(docs_path)
        else:
            cls.removed += f"{args.target} "
            remove_dir(args.target_build_path)

        cls.first_target = False

    @classmethod
    def after(cls, targets):
        logging.success(f"Removed {cls.removed}")
