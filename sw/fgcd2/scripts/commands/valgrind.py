import subprocess

from utils.findTargetExe import *
from projectControl.valgrind import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("valgrind", help="Run valgrind for the target.")

        cls.subparser.add_argument("target", help="Target to run.")

        cls.subparser.add_argument("arguments", nargs="*", help="Optional arguments to pass to the target.")

        cls.subparser.add_argument("-a", "--args", action="store",
                                   help="Use these valgrind arguments instead of the default ones.")

        cls.subparser.add_argument("-q", "--quiet", action="store_true",
                                   help="Do not show valgrind output if there is no error.")

    @classmethod
    def run(cls, args, cfg, project_args):
        target_exe = find_target_exe(cfg, project_args, args.target)

        valgrind = Valgrind(args.args)
        valgrind.run(args.target, target_exe, args.arguments)


    