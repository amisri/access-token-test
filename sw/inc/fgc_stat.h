/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc_stat.h

  Contents:     This file contains the generic real-time status structure definition for FGC devices.
                It also contains the diagnostic structure transmitted by the FGC via the RS232 serial
                port when in diagnostic mode.
\*---------------------------------------------------------------------------------------------------------*\

 All FGC devices produce 56 byte real-time status blocks.  Sixteen bytes are common to all classes and forty
 bytes are specific, but the first four bytes of the class specific data must be an FGC alarms structure
 (faults and warnings).  This file defines the generic structure for the status block, which is
 discriminated for each device class by separate automatically generated header files, included below.

 The common part of the structure is 16 bytes long, though only two bytes are meaningful for client
 applications.  The other fourteen bytes are used for FGC diagnostics communications between an FGC and
 the Gateway.  The meaningful bytes are:

         80    7 40    6 20    5 10    4 08    3 04    2 02    1 01    0
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | ExtPM |SelfPM |PllLock|       |       | Class | Data  |In Data|
        |       |       |       |       |       | valid | valid |  base | -+-- .status
        +-------+-------+-------+-------+-------+-------+-------+-------+
        |                            Class ID                           |
        |                             (0-99)                            | -+-- .class
        +-------+-------+-------+-------+-------+-------+-------+-------+

 The status nibble contains four status bits, with the following significance when the bit is 1:

   Bit value    Field           Constant                Meaning when 1

     0x01       In Database     TYPE_FGC_DEVICE_IN_DB   The device is defined in the database.
     0x02       Data valid      FGC_DATA_VALID          The data is fresh.
     0x04       Class valid     FGC_CLASS_VALID         The device class agrees with the database.

     0x20       Pll locked      FGC_ACK_PLL             [KT gateway only] The PLL is locked.
     0x40       Self PM         FGC_SELF_PM_REQ         [KT gateway only] Self-triggered post-mortem (GW+FGC data) requested.
     0x80       External PM     FGC_EXT_PM_REQ          [KT gateway only] External post-mortem (GW data only) requested.

 When everything is working correctly, the status byte will have the value 0x0F.

 The second byte defines the dataset device class.  The second word is used by FGCs to export their
 runlog to the Gateway.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_STAT_H
#define FGC_STAT_H

#include <stdint.h>

#include "fgc_consts_gen.h"

/*----- Include class status definitions -----*/

#include "classes/0/0_stat.h"                   // Class 0 - FGCD base
#include "classes/1/1_stat.h"                   // Class 1 - FGC WorldFIP gateway
#include "classes/2/2_stat.h"                   // Class 2 - FGC status server
#include "classes/3/3_stat.h"                   // Class 3 - Mugef FGCD
#include "classes/4/4_stat.h"                   // Class 4 - FGC_Ether gateway
#include "classes/5/5_stat.h"                   // Class 5 - FGC serial gateway
#include "classes/6/6_stat.h"                   // Class 6 - FGClite gateway
#include "classes/7/7_stat.h"                   // Class 7 - ISEG HV gateway
#include "classes/8/8_stat.h"                   // Class 7 - Mugef FGCD (digital regulation)

#include "classes/50/50_stat.h"                 // Class 50 - FGC2 base class (BOOT)
#include "classes/51/51_stat.h"                 // Class 51 - FGC2 for LHC power converters
#include "classes/53/53_stat.h"                 // Class 53 - FGC2 for PS POPS main power converter
#include "classes/59/59_stat.h"                 // Class 59 - FGC2 for RF function generation
#include "classes/60/60_stat.h"                 // Class 60 - FGC3 base class (BOOT)
#include "classes/62/62_stat.h"                 // Class 62 - FGC3 for fast-pulse power converters
#include "classes/63/63_stat.h"                 // Class 63 - FGC3 for power converters with cclibs
#include "classes/64/64_stat.h"                 // Class 64 - FGC3 for SVC applications
#include "classes/65/65_stat.h"                 // Class 65 - FGC3 for EPIC diagnostics
#include "classes/90/90_stat.h"                 // Class 90 - FGCD generic equipment
#include "classes/91/91_stat.h"                 // Class 91 - FGCD Mugef power converter
#include "classes/92/92_stat.h"                 // Class 92 - FGClite power converter
#include "classes/93/93_stat.h"                 // Class 93 - ISEG HV power converter
#include "classes/94/94_stat.h"                 // Class 94 - FGCD Mugef power converter (digital regulation)
#include "classes/98/98_stat.h"                 // Class 98 - FGC status server equipment device
#include "classes/150/150_stat.h"               // Class 150 - CERN DCCT Calibrator
#include "classes/151/151_stat.h"               // Class 151 - Switching Matrix

/*----- Constants -----*/

#define FGC_DEVICE_IN_DB                0x01    // Device is defined in the database
#define FGC_DATA_VALID                  0x02    // Data is fresh
#define FGC_CLASS_VALID                 0x04    // Device class agrees with database

// Following flags used by KT GW are already defined in fgc_fieldbus.h
// #define FGC_ACK_PLL                     0x20    // [KT gateway only] PLL is locked
// #define FGC_SELF_PM_REQ                 0x40    // [KT gateway only] Self-triggered post-mortem (GW+FGC data) requested
// #define FGC_EXT_PM_REQ                  0x80    // [KT gateway only] External post-mortem (GW data only) requested

/*----- Boot structure (40 bytes) -----*/

struct boot_stat
{
    uint8_t     reserved0[10];                  // Union with fgc50_stat
    uint8_t     reserved1[2];
    uint32_t    code_request;                   // Code slot requests (slots 0-31)
    uint8_t     reserved2[2];
    char        rterm[21];                      // Remote terminal data
    uint8_t     reserved3;
};

/*----- Common structure (40 bytes) -----*/

#define FGC_POST_MORTEM         256

struct common_stat
{
    uint16_t    st_faults;                      // Faults
    uint16_t    st_warnings;                    // Warnings
    uint16_t    st_latched;                     // Latched status
    uint16_t    st_unlatched;                   // Unlatched status
};

/*----- Class specific data (40 bytes) -----*/

union fgc_pub_class_data
{
    uint32_t            alarms;                 // Note that we're inside a union, so this field aliases
                                                // with the concatenation of st_faults + st_warnings.
    struct boot_stat    boot;
    struct common_stat  common;
    struct fgc0_stat    c0;
    struct fgc1_stat    c1;
    struct fgc2_stat    c2;
    struct fgc3_stat    c3;
    struct fgc4_stat    c4;
    struct fgc5_stat    c5;
    struct fgc6_stat    c6;
    struct fgc7_stat    c7;
    struct fgc8_stat    c8;
    struct fgc50_stat   c50;
    struct fgc51_stat   c51;
    struct fgc53_stat   c53;
    struct fgc59_stat   c59;
    struct fgc60_stat   c60;
    struct fgc62_stat   c62;
    struct fgc63_stat   c63;
    struct fgc64_stat   c64;
    struct fgc65_stat   c65;
    struct fgc90_stat   c90;
    struct fgc91_stat   c91;
    struct fgc92_stat   c92;
    struct fgc93_stat   c93;
    struct fgc94_stat   c94;
    struct fgc98_stat   c98;
    struct fgc150_stat  c150;
    struct fgc151_stat  c151;
    uint8_t             reserved[40];
};

/*----- Published status structure declaration -----*/

struct fgc_stat
{
    uint8_t     data_status;                    // [0x00] Class data status
    uint8_t     class_id;                       // [0x01] Class ID for the device
    uint8_t     reserved0;                      // [0x02] FGC command status byte
    uint8_t     runlog;                         // [0x03] FGC runlog data byte

    uint8_t     reserved1[12];                  // [0x04] Padding

    union fgc_pub_class_data class_data;        // [0x10] Class specific data
};

/*----- FGC diagnostic structure declaration (172 bytes) -----*/

struct fgc_diag
{
    uint32_t    time_sec;                       // [0x00] Unix time (s)
    uint32_t    time_usec;                      // [0x04] ms time (0, 200, 400, 600, 800)
    uint8_t     data_status;                    // [0x08] Class data status
    uint8_t     class_id;                       // [0x09] Class ID for the device
    uint8_t     reserved[2];                    // [0x0A] Padding

    union fgc_pub_class_data class_data;        // [0x0C] Class specific data

    uint8_t     chan[FGC_DIAG_PERIOD][FGC_DIAG_N_LISTS];        // [0x34] Diag channel numbers
    uint16_t    data[FGC_DIAG_PERIOD][FGC_DIAG_N_LISTS];        // [0x5C] Diag data
};

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc_stat.h
\*---------------------------------------------------------------------------------------------------------*/
