/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc_ether.h

  Contents:     Structures and constants for communication via the FGC_Ether fieldbus.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_ETHER_H
#define FGC_ETHER_H

#include <stdint.h>
// #include <defconst.h>

#include "fgc_code.h"

// General constants

#define FGC_ETHER_ADDR_SIZE             6           // Size of an Ethernet address
#define FGC_ETHER_MAX_FGCS              64          // Maximum number of FGCs per FGC_Ether fieldbus
#define FGC_ETHER_TYPE                  0x88B5      // Ether type for FGC_Ether frames

// Cycle timings

#define FGC_ETHER_STATUS_WIN_START_MS   0           // Millisecond within cycle for start of status window
#define FGC_ETHER_STATUS_WIN_END_MS     15          // Millisecond within cycle for end of status window
#define FGC_ETHER_PUB_FLUSH_MS           3          // Millisecond within cycle for publication flush
#define FGC_ETHER_RSP_FLUSH_MS          16          // Millisecond within cycle for response command flush
#define FGC_ETHER_TIME_1_MS             18          // Millisecond within cycle for first time frame transmission
#define FGC_ETHER_TIME_2_MS             19          // Millisecond within cycle for second time frame transmission

#include "fgc_stat.h"

// Standard Ethernet header (14 bytes)

struct fgc_ether_header_ethernet
{
    uint8_t     dest_addr[FGC_ETHER_ADDR_SIZE];     // Destination MAC address
    uint8_t     src_addr[FGC_ETHER_ADDR_SIZE];      // Source MAC address
    uint16_t    ether_type;                         // Ether type (must be set to FGC_ETHER_TYPE)
};

// FGC header fields

struct fgc_ether_header_fgc
{
    uint8_t     payload_type;                       // Type of payload (see enum fgc_ether_payload_type)
    uint8_t     padding;
};

// FGC_Ether frame header structure

typedef struct fgc_ether_header
{
    struct fgc_ether_header_ethernet    ethernet;   // Standard Ethernet header
    struct fgc_ether_header_fgc         fgc;        // FGC header
} fgc_ether_header_t;

// Note that 1500 and 46 in the following lines are the maximum and minimum Ethernet payload sizes respectively

#define FGC_ETHER_PAYLOAD_MAX               (1500 - sizeof(struct fgc_ether_header_fgc))    // Maximum length of an FGC_Ether payload
#define FGC_ETHER_PAYLOAD_MIN               (  46 - sizeof(struct fgc_ether_header_fgc))    // Minimum length of an FGC_Ether payload

#define FGC_ETHER_CODE_BLOCKS_PER_MSG       32                                              // Number of code blocks transmitted per message
#define FGC_ETHER_MAX_CMD_LEN               (FGC_ETHER_PAYLOAD_MAX - sizeof(uint16_t))      // Maximum command message length
#define FGC_ETHER_MIN_CMD_LEN               (FGC_ETHER_PAYLOAD_MIN - sizeof(uint16_t))      // Minimum command message length
#define FGC_ETHER_MAX_RSP_LEN               (FGC_ETHER_PAYLOAD_MAX - sizeof(uint16_t))      // Maximum response message length
#define FGC_ETHER_MIN_RSP_LEN               (FGC_ETHER_PAYLOAD_MIN - sizeof(uint16_t))      // Minimum response message length

#ifndef FGC_FIELDBUS_CODE_BLOCKS_PER_MSG
#define FGC_FIELDBUS_CODE_BLOCKS_PER_MSG    FGC_ETHER_CODE_BLOCKS_PER_MSG                   // Number of code blocks transmitted per message
#define FGC_FIELDBUS_MAX_CMD_LEN            FGC_ETHER_MAX_CMD_LEN                           // Maximum command message length
#define FGC_FIELDBUS_MIN_CMD_LEN            FGC_ETHER_MIN_CMD_LEN                           // Minimum command message length
#define FGC_FIELDBUS_MAX_RSP_LEN            FGC_ETHER_MAX_RSP_LEN                           // Maximum response message length
#define FGC_FIELDBUS_MIN_RSP_LEN            FGC_ETHER_MIN_RSP_LEN                           // Minimum response message length
#endif

#include "fgc_fieldbus.h"
#define FGC_ETHER_RSP_PKT_LEN               (FGC_ETHER_MAX_RSP_LEN - sizeof(struct fgc_fieldbus_rsp_header))
#define FGC_ETHER_MAX_RTERM_CHARS       21          // Max number of remote terminal chars in a status packet (eth)

// FGC_Ether payload types

enum fgc_ether_payload_type
{
    FGC_ETHER_PAYLOAD_TIME,
    FGC_ETHER_PAYLOAD_STATUS,
    FGC_ETHER_PAYLOAD_CMD,
    FGC_ETHER_PAYLOAD_RSP,
    FGC_ETHER_PAYLOAD_PUB,
    FGC_ETHER_PAYLOAD_INTER_FGC,
    FGC_ETHER_PAYLOAD_NUM_TYPES
};

// Basic FGC_Ether frame structure

struct fgc_ether_frame
{
    fgc_ether_header_t     header;
    uint8_t                     payload[FGC_ETHER_PAYLOAD_MAX];
};

// Command frame
// Identified by FGC_ETHER_PAYLOAD_CMD payload type in header

typedef struct fgc_ether_cmd_payload
{
    uint16_t                length;
    struct fgc_fieldbus_cmd data;
} fgc_ether_cmd_payload_t;

typedef struct fgc_ether_pub_payload
{
    uint16_t                length;
    struct fgc_fieldbus_rsp data;
} fgc_ether_pub_payload_t;

typedef struct fgc_ether_rsp_payload
{
    uint16_t                length;
    struct fgc_fieldbus_rsp data;
} fgc_ether_rsp_payload_t;

typedef struct fgc_ether_status_payload
{
    union fgc_stat_var      status;         // Status
    char                    rterm[21];      // Remote terminal data
    uint8_t                 reserved[3];    // Pad to 32 bit boundary
} fgc_ether_status_payload_t;

struct fgc_ether_cmd
{
    fgc_ether_header_t      header;
    fgc_ether_cmd_payload_t payload;
};

// Published data frame
// Identified by FGC_ETHER_PAYLOAD_PUB payload type in header

struct fgc_ether_pub
{
    fgc_ether_header_t      header;
    fgc_ether_pub_payload_t payload;
};

// Response frame
// Identified by FGC_ETHER_PAYLOAD_RSP payload type in header

struct fgc_ether_rsp
{
    fgc_ether_header_t      header;
    fgc_ether_rsp_payload_t payload;
};

// Status frame
// Identified by FGC_ETHER_PAYLOAD_STATUS payload type in header
// Status sent to the Gateway every 20-millisecond cycle
struct fgc_ether_status
{
    fgc_ether_header_t          header;
    fgc_ether_status_payload_t  payload;

};

//Status sent from one FGC3 to another each millisecond

typedef struct fgc_ether_metadata
{
    uint32_t version;
    uint16_t size;
    uint16_t reserved;
} fgc_ether_metadata_t;

// Time, real-time reference and code frame
// Identified by FGC_ETHER_PAYLOAD_TIME payload type in header

struct fgc_ether_time
{
    fgc_ether_header_t     header;

    struct fgc_ether_time_payload
    {
        struct fgc_fieldbus_time    time;                                                   // Time variable
        float                       rt_ref[64];                                             // Real-time references
        uint8_t                     code[FGC_FIELDBUS_CODE_BLOCKS_PER_MSG][FGC_CODE_BLK_SIZE];  // Code blocks
    } payload;
};

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc_ether.h
\*---------------------------------------------------------------------------------------------------------*/
