//! @file  fgc_log.h
//! @brief FGC Spy version 1 structures and constants
//!
//! This file defines the original FGC Spy Version 1 data format
//! The data has a buffer header followed by signal headers, followed by the signal data.

#ifndef FGC_FGC_LOG_H
#define FGC_FGC_LOG_H

#include <stdint.h>

// Constants

#define FGC_LOG_VERSION                 1               //!< Spy data version

#define FGC_LOG_SIG_LABEL_LEN           15              //!< Signal name length
#define FGC_LOG_SIG_UNITS_LEN           7               //!< Signal units length

#define FGC_LOG_TYPE_FLOAT              0               //!< Signal data are 32-bit floats
#define FGC_LOG_TYPE_INT16S             1               //!< Signal data are 16-bit signed integers
#define FGC_LOG_TYPE_INT16U             2               //!< Signal data are 16-bit unsigned integers
#define FGC_LOG_TYPE_INT32S             3               //!< Signal data are 32-bit signed integers
#define FGC_LOG_TYPE_INT32U             4               //!< Signal data are 32-bit unsigned integers

#define FGC_LOG_BUF_INFO_LITTLE_ENDIAN  0x01            //!< Data is little endian order
#define FGC_LOG_BUF_INFO_STOPPED        0x80            //!< Buffer has stopped filling

#define FGC_LOG_SIG_INFO_STEPS          0x01            //!< Specify step interpolation for signal

struct fgc_log_header                                   //!< Buffer header (20 bytes)
{
    uint8_t     version;                                //!<  0: Buffer version
    uint8_t     info;                                   //!<  1: FGC_LOG_BUF_INFO_XXXX
    uint16_t    n_signals;                              //!<  2: Number of signals
    uint32_t    n_samples;                              //!<  4: Number of samples
    uint32_t    period_us;                              //!<  8: Sampling period (us)
    uint32_t    unix_time;                              //!< 12: Time of first sample (s)
    uint32_t    us_time;                                //!< 16: Time of first sampe (us)
};

struct fgc_log_sig_header                               //!< Signal header (40 bytes)
{
    uint16_t    type;                                   //!< Data type:  FGC_LOG_TYPE_XXXX
    uint16_t    info;                                   //!< Data flags: FGC_LOG_SIG_INFO_XXXX
    int32_t     us_time_offset;                         //!< Signal time offset (us)
    float       gain;                                   //!< Signal gain
    float       offset;                                 //!< Sig = gain * raw_value + offset
    uint8_t     label_len;                              //!< Label length in bytes
    char        label[FGC_LOG_SIG_LABEL_LEN];           //!< Label (not null terminated)
    uint8_t     units_len;                              //!< Units length in bytes
    char        units[FGC_LOG_SIG_UNITS_LEN];           //!< Units (not null terminated)
};

// FGC Spy version 1 structure

struct fgc_log
{
    struct fgc_log_header       buf_header;             //!< Buffer header (20 bytes)

    struct fgc_log_sig_header   sig_header[1];          //!< Signal headers array (40 bytes per signal)
};

#endif

// EOF
