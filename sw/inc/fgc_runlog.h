/*---------------------------------------------------------------------------------------------------------*\
  File:     fgc_runlog.h

  Contents: This file contains the structures associated with the FGC runlog
\*---------------------------------------------------------------------------------------------------------*\

 The run log record has a fixed length 6-byte structure as follows:

                +0      +1      +2      +3      +4      +5
            +-------+-------+-------+-------+-------+-------+
            |RL SEQ | RL ID |   RL DATA[0]  |   RL DATA[1]  |
            +-------+-------+-------+-------+-------+-------+


 Each record has a run log ID which identifies the run log event, and a payload of 0, 1 or 2 words.
 The format is big endian and the payload length is specified in the XML file when the run log ID
 is defined.  The FGC contains a circular buffer with FGC_RUNLOG_N_ELS entries.  The FGC exports this
 buffer via one byte in the status variable published over the WorldFIP by each FGC. The address of
 the next byte to send is defined in the runlog_idx field of the time variable, broadcast by the gateway.

\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_RUNLOG_H
#define FGC_RUNLOG_H

#include <stdint.h>

// Constants

#define FGC_RUNLOG_N_ELS            128                                     // Run log buffer length in elements
#define FGC_RUNLOG_MAX_DATA_LEN     2                                       // Max number of data words per element
#define FGC_RUNLOG_EL_SIZE          6                                       // Size of element in bytes
#define FGC_RUNLOG_SIZE_BYTES       (FGC_RUNLOG_N_ELS * FGC_RUNLOG_EL_SIZE) // Size of runlog in bytes

// Runlog structure

struct fgc_runlog
{
    uint8_t     seq;                            // Record sequence number
    uint8_t     id;                             // Run log ID number
    uint16_t    data[FGC_RUNLOG_MAX_DATA_LEN];  // Run log data words
};

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc_runlog.h
\*---------------------------------------------------------------------------------------------------------*/
