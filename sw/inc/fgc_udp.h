/*
 *  Filename: fgc_udp.h
 *
 *  Purpose:  Declarations for UDP data
 *
 *  Author:   Stephen Page
 */

#ifndef FGC_UDP_H
#define FGC_UDP_H

#include <stdint.h>

// UDP data header

struct fgc_udp_header
{
    uint32_t    id;                 // Context dependent ID
    uint32_t    sequence;           // Sequence number
    int32_t     send_time_sec;      // Timestamp of data send (sec)
    int32_t     send_time_usec;     // Timestamp of data send (usec)
};

#endif

// EOF
