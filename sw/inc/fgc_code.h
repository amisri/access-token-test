/*---------------------------------------------------------------------------------------------------------*\
  File:     fgc_code.h

  Contents:     Structures and constants for FGC codes
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_CODE_H
#define FGC_CODE_H

#include <stdint.h>

#include <fgc_consts_gen.h>

// Constants

#define FGC_CODE_BLK_SIZE           32          // Size of a code block
#define FGC_CODE_NULL               0xFF        // Value to indicate that a code slot is empty
#define FGC_CODE_NAMEDB_BLKS        256         // 8 KB
#define FGC_CODE_NAMEDB_ID          0           // ID for NameDB code
#define FGC_CODE_NAMEDB_MAX_DEVS    65          // Max devices in NameDB
#define FGC_CODE_NAMEDB_MAX_SUBDEVS 160         // Limited by 8KB name code

// Structure containing code meta data
// This structure is found at the end of each FGC code

#define FGC_CODE_VERSION_STRING_LEN 20          // Nul terminated 19 chars: "DD/MM/YYYY HH:MM:SS\0"
#define FGC_CODE_INFO_SIZE          30          // Unpadded size of struct fgc_code_info
#define FGC_CODE_MAX_SLOTS          32          // Max number of code slots in the gateway (limit is 32-bit mask)

struct fgc_code_info
{
    char        version_string[FGC_CODE_VERSION_STRING_LEN]; // Version local time string
    uint32_t    version;                                     // Version (unixtime)
    uint8_t     class_id;                                    // FGC class ID
    uint8_t     code_id;                                     // Code ID
    uint16_t    old_version;                                 // Version used for the old versioning system
    uint16_t    crc;                                         // CRC16
};

/*----- NameDB union definition -----*\

  The gateway will dedicate code slot 0 to the NameDB code (Code 0).  This contains the part of the
  FGC name file that applies to the gateway.  It is only 8KB is size. The size of the code is
  calculated in fgc/doc/development/NameDBsize.xlsx.

  The code version is always zero.  The FGCs will rely on the checksum to recognise a change of NameDB.

  FGC_CODE_NAMEDB_MAX_SUBDEVS must be set to keep the length of the fgc_name_db structure below the
  length of the 8KB NameDB code.
*/

struct fgc_name_db
{
    struct fgc_dev_name
    {
        uint16_t    class_id;                   // [2]  Device class
        uint16_t    omode_mask;                 // [2]  Operational mode mask
        char        name[FGC_MAX_DEV_LEN + 1];  // [24] Device name
    } dev[FGC_CODE_NAMEDB_MAX_DEVS];

    uint8_t num_sub_devs[FGC_CODE_NAMEDB_MAX_DEVS];  // Number of sub-devices per device
    uint8_t pad;

    struct fgc_sub_dev_name
    {
        uint8_t index;                          // [1]  Sub device index (1-16)
        uint8_t padding[3];                     // [3]
        char    name[FGC_MAX_DEV_LEN + 1];      // [24] Sub-device names
    } sub_dev[FGC_CODE_NAMEDB_MAX_SUBDEVS];
};

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc_code.h
\*---------------------------------------------------------------------------------------------------------*/
