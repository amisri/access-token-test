//! @file  fgc_event.h
//! @brief Structures and constants for FGC events.

#ifndef FGC_EVENT_H
#define FGC_EVENT_H

#ifdef FGC_EVENT_GLOBALS
#define FGC_EVENT_VARS_EXT
#else
#define FGC_EVENT_VARS_EXT extern
#endif


// ---------- Includes

#include <stdint.h>



// ---------- Constants

#define FGC_EVT_MIN_CYCLES              3       //!< Minimum number of fieldbus cycles in which to transmit an event
#define FGC_MULTI_PPM_SUB_SEL_SHIFT     8
#define FGC_MULTI_PPM_USER_MASK         0x00FF



// ---------- External structures, unions and enumerations

//! FGC event structure

struct fgc_event
{
    uint8_t  type;              //!< Event type (FGC_EVT_XXXX)
    uint8_t  trigger_type;      //!< Event type of triggering event
    uint16_t payload;           //!< Event payload
    uint32_t delay_us;          //!< Event delay in microseconds
};


//! Event types

enum fgc_events
{
    FGC_EVT_NONE                  ,  // Colour: off
    FGC_EVT_PM                    ,  // Colour: off
    FGC_EVT_START                 ,  // Colour:   1
    FGC_EVT_ABORT                 ,  // Colour:   1
    FGC_EVT_SSC                   ,  // Colour: off
    FGC_EVT_NEXT_DEST             ,  // Colour: off
    FGC_EVT_CYCLE_TAG             ,  // Colour:   2
    FGC_EVT_CYCLE_START           ,  // Colour:   3
    FGC_EVT_START_REF_1           ,  // Colour:   3
    FGC_EVT_START_REF_2           ,  // Colour:   3
    FGC_EVT_START_REF_3           ,  // Colour:   3
    FGC_EVT_START_REF_4           ,  // Colour:   3
    FGC_EVT_SUB_DEVICE_CYCLE_START,  // Colour:   2
    FGC_EVT_SUB_DEVICE_START_REF_1,  // Colour:   3
    FGC_EVT_SUB_DEVICE_START_REF_2,  // Colour:   3
    FGC_EVT_SUB_DEVICE_START_REF_3,  // Colour:   3
    FGC_EVT_SUB_DEVICE_START_REF_4,  // Colour:   3
    FGC_EVT_PAUSE                 ,  // Colour:   4
    FGC_EVT_RESUME                ,  // Colour:   4
    FGC_EVT_ECONOMY_DYNAMIC       ,  // Colour:   5
    FGC_EVT_ACQUISITION           ,  // Colour:   6
    FGC_EVT_TRANSACTION_COMMIT    ,  // Colour:   7
    FGC_EVT_TRANSACTION_ROLLBACK  ,  // Colour: off
    FGC_EVT_COAST                 ,  // Colour:   9
    FGC_EVT_RECOVER               ,  // Colour:   9
    FGC_EVT_START_HARMONICS       ,  // Colour:  10
    FGC_EVT_STOP_HARMONICS        ,  // Colour:  10
    FGC_EVT_START_REF_5           ,  // Colour:   3
    FGC_EVT_SUB_DEVICE_START_REF_5,  // Colour:   3
    FGC_NUM_EVENT_TYPES
};



FGC_EVENT_VARS_EXT char const * fgc_event_names[FGC_NUM_EVENT_TYPES]
#ifdef FGC_EVENT_GLOBALS
=
{
    "NONE",
    "PM",
    "START",
    "ABORT",
    "SSC",
    "NEXT_DEST",
    "CYCLE_TAG",
    "CYCLE_START",
    "START_REF_1",
    "START_REF_2",
    "START_REF_3",
    "START_REF_4",
    "SUB_DEV_CYCLE_START",
    "SUB_DEV_START_REF_1",
    "SUB_DEV_START_REF_2",
    "SUB_DEV_START_REF_3",
    "SUB_DEV_START_REF_4",
    "PAUSE",
    "RESUME",
    "ECONOMY_DYNAMIC",
    "ACQUISITION",
    "TRANSACTION_COMMIT",
    "TRANSACTION_ROLLBACK",
    "COAST",
    "RECOVER",
    "START_HARMONICS",
    "STOP_HARMONICS",
    "START_REF_5",
    "SUB_DEV_START_REF_5",
}
#endif
;

#endif


// EOF
