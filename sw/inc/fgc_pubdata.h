/*
 *  Filename: fgc_pubdata.h
 *
 *  Purpose:  Declarations for UDP published data
 *
 *  Author:   Stephen Page
 */

#ifndef FGC_PUBDATA_H
#define FGC_PUBDATA_H

#include <stdint.h>

#include "fgc_consts_gen.h"
#include "fgc_stat.h"
#include "fgc_udp.h"

// Diagnostic data structure

struct fgc_udp_diag_data
{
    struct fgc_udp_header   header;
    struct fgc_diag         diag;
};

// Published data structure. Used by LHC OFC (see EPCCCS-5018). Do not remove w/o askign OFC team!

#ifndef FGCD_MAX_DEVS
#warning "FGCD_MAX_DEVS not defined. Settign the value to its defautl value of 31"
#define FGCD_MAX_DEVS 31
#endif

struct fgc_udp_pub_data
{
    struct fgc_udp_header   header;
    int32_t                 time_sec;
    int32_t                 time_usec;
    struct fgc_stat         status[FGCD_MAX_DEVS];
};

#endif

// EOF
