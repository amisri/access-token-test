/*
 *  Filename: fgc_rtdata.h
 *
 *  Purpose:  Declarations for real-time data
 *
 *  Author:   Stephen Page
 */

#ifndef FGC_RTDATA_H
#define FGC_RTDATA_H

#include <stdint.h>

#include "fgc_udp.h"

// Real-time data structure

#define FGC_MAX_RTDEVS_PER_GW 64

struct fgc_udp_rt_data
{
    struct fgc_udp_header   header;
    int32_t                 apply_time_sec;                        // Timestamp of data application (sec)
    int32_t                 apply_time_usec;                       // Timestamp of data application (usec)
    uint64_t                active_channels;                       // Mask of channels to which to apply RT data
    float                   channels[FGC_MAX_RTDEVS_PER_GW];       // RT data for channels
};

#endif

// EOF
