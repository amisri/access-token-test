/*!
 * @file   logPostmortem.h
 * @brief  Declaration of the PM_BUF binary format for class 51
 * @author Marc Magrans de Abril
 */

#ifndef PM_51_H
#define PM_51_H

#include <stdint.h>

// Constants for the different buffer sizes for the FGC_51 self pm buffer

const uint32_t  PM_FGC_51_EVENTS_LEN           = 646;
const uint32_t  PM_FGC_51_EVENTS_PROPERTY_LEN  = 24;
const uint32_t  PM_FGC_51_EVENTS_SYMBOL_LEN    = 36;
const uint32_t  PM_FGC_51_EVENTS_ACTION_LEN    = 8;

const uint32_t  PM_FGC_51_IAB_LEN            = 8192;
const long long PM_FGC_51_IAB_SAMPLING_NS    = 1000*1000;           // 1kHz
const uint32_t  PM_FGC_51_IREG_LEN           = 3840;
const long long PM_FGC_51_IREG_SAMPLING_NS   = 1000*1000*10;        // 100Hz
const uint32_t  PM_FGC_51_ILEADS_LEN         = 768;
const long long PM_FGC_51_ILEADS_SAMPLING_NS = 1000*1000*1000;      // 1Hz
const uint32_t  PM_FGC_51_IEARTH_LEN         = 1024;
const long long PM_FGC_51_IEARTH_SAMPLING_NS = 1000*1000*20;        // 50Hz

//! Data structure that maps the data returned in LOG.PM.BUF. All data is in network byte order.

struct pm_51_self_t
{
    struct pm_51_events_t
    {
        uint32_t time_sec;
        uint32_t time_usec;
        char     property[PM_FGC_51_EVENTS_PROPERTY_LEN];
        char     symbol[PM_FGC_51_EVENTS_SYMBOL_LEN];
        char     action[PM_FGC_51_EVENTS_ACTION_LEN];
    } events[PM_FGC_51_EVENTS_LEN];

    struct pm_51_iab_t
    {
        float i_a;
        float i_b;
    } iab[PM_FGC_51_IAB_LEN];

    uint32_t iab_last_time_sec;
    uint32_t iab_last_time_usec;

    struct pm_51_ireg_t
    {
        float i_ref_rst;
        float i_meas;
        float v_ref;
        float v_meas;
    } ireg[PM_FGC_51_IREG_LEN];

    uint32_t ireg_last_time_sec;
    uint32_t ireg_last_time_usec;

    struct pm_51_ileads_t
    {
        float i_meas;
        float v_meas;
        float u_lead_pos;
        float u_lead_neg;
    } ileads[PM_FGC_51_ILEADS_LEN];

    uint32_t ileads_last_time_sec;
    uint32_t ileads_last_time_usec;

    struct pm_51_iearth_t
    {
        float i_earth;
    } iearth[PM_FGC_51_IEARTH_LEN];

    uint32_t iearth_last_time_sec;
    uint32_t iearth_last_time_usec;
};

#endif
