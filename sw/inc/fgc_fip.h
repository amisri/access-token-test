/*---------------------------------------------------------------------------------------------------------*\
  File:     fgc_fip.h

  Contents:     Structures, constants and descriptions for communication between the Gateway and FGCs via
                the WorldFIP fieldbus.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_FIP_H
#define FGC_FIP_H

#include <stdint.h>

#include "fgc_code.h"
#include "fgc_consts_gen.h"
#include "fgc_event.h"
#include "fgc_stat.h"

#define FGC_FIP_CODE_BLOCKS_PER_MSG         2                           // Number of code blocks transmitted per message
#define FGC_FIP_MAX_CMD_LEN                 122                         // Maximum command message length
#define FGC_FIP_MIN_CMD_LEN                 3                           // Minimum command message length
#define FGC_FIP_MAX_RSP_LEN                 122                         // Maximum response message length
#define FGC_FIP_MIN_RSP_LEN                 3                           // Minimum response message length

#ifndef FGC_FIELDBUS_CODE_BLOCKS_PER_MSG
#define FGC_FIELDBUS_CODE_BLOCKS_PER_MSG    FGC_FIP_CODE_BLOCKS_PER_MSG // Number of code blocks transmitted per message
#define FGC_FIELDBUS_MAX_CMD_LEN            FGC_FIP_MAX_CMD_LEN         // Maximum command message length
#define FGC_FIELDBUS_MIN_CMD_LEN            FGC_FIP_MIN_CMD_LEN         // Minimum command message length
#define FGC_FIELDBUS_MAX_RSP_LEN            FGC_FIP_MAX_RSP_LEN         // Maximum response message length
#define FGC_FIELDBUS_MIN_RSP_LEN            FGC_FIP_MIN_RSP_LEN         // Minimum response message length
#endif

#include "fgc_fieldbus.h"
#define FGC_FIP_RSP_PKT_LEN                 (FGC_FIP_MAX_RSP_LEN - sizeof(struct fgc_fieldbus_rsp_header))

/*----- Code variable - ID: 0x3004 - Length: 64 bytes -----*\

            +0      +1      +2      +3      +4      +5      +6      +7
        +-------+-------+-------+-------+-------+-------+-------+-------+
 0:0x00 |                   Code block 0 bytes 0-7                      |
        +-------+-------+-------+-------+-------+-------+-------+-------+
 8:0x08 |                   Code block 0 bytes 8-15                     |
        +-------+-------+-------+-------+-------+-------+-------+-------+
16:0x10 |                   Code block 0 bytes 16-23                    |
        +-------+-------+-------+-------+-------+-------+-------+-------+
24:0x18 |                   Code block 0 bytes 24-31                    |
        +-------+-------+-------+-------+-------+-------+-------+-------+
32:0x20 |                   Code block 1 bytes 0-7                      |
        +-------+-------+-------+-------+-------+-------+-------+-------+
40:0x28 |                   Code block 1 bytes 8-15                     |
        +-------+-------+-------+-------+-------+-------+-------+-------+
48:0x30 |                   Code block 1 bytes 16-23                    |
        +-------+-------+-------+-------+-------+-------+-------+-------+
56:0x38 |                   Code block 1 bytes 24-31                    |
        +-------+-------+-------+-------+-------+-------+-------+-------+

The code variable is sent shortly after the time variable, and contains two 32-byte code blocks.
*/

struct fgc_fip_code
{
    uint8_t code[FGC_FIELDBUS_CODE_BLOCKS_PER_MSG][FGC_CODE_BLK_SIZE];  // [0x00] Code blocks
};

/*----- Real-time data variable - ID: 0x3002 or 0x3003 - Length: 64 bytes -----*\

        +0      +1      +2      +3      +4      +5      +6      +7
        +-------+-------+-------+-------+-------+-------+-------+-------+
 0:0x00 |seq_num|       Reserved        | Float value for FGC  1 or 16  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
 8:0x08 | Float value for FGC  2 or 17  | Float value for FGC  3 or 18  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
16:0x10 | Float value for FGC  4 or 19  | Float value for FGC  5 or 20  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
24:0x18 | Float value for FGC  6 or 21  | Float value for FGC  7 or 22  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
32:0x20 | Float value for FGC  8 or 23  | Float value for FGC  9 or 24  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
40:0x28 | Float value for FGC 10 or 25  | Float value for FGC 11 or 26  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
48:0x30 | Float value for FGC 12 or 27  | Float value for FGC 13 or 29  |
        +-------+-------+-------+-------+-------+-------+-------+-------+
56:0x38 | Float value for FGC 14 or 29  | Float value for FGC 15 or 30  |
        +-------+-------+-------+-------+-------+-------+-------+-------+

 Notes: The gateway must be able to send a separate 32-bit IEEE float
    to each FGC on the bus, every 20ms.  To do this it uses two 64-byte
    variables (the maximum size that the uFIP can received).   The
    variables are numbered 3002 (FGCs 1-15) and 3003 (FGCs 16-30).
    The FGCs receive only the variable that applies to them, and they
    extract only the four bytes that is relevent for their ID number.

    If the gateway has never received an RT value for a channel it will
    send NaN (0xFFFFFFFF).
*/

struct fgc_fip_rt_data
{
    uint8_t seq_num;        // [0x00] 0-255 - transmission sequence number
    uint8_t reserved[3];    // [0x01] Reserved
    float   rt_data[15];    // [0x04] RT data for FGCs 1-15 or 16-30
};

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc_fip.h
\*---------------------------------------------------------------------------------------------------------*/
