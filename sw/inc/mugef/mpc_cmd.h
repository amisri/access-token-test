/*
 *  Filename: mpc_cmd.h
 *
 *  Purpose:  Constants for mpc commands
 *
 *  Author:
 */


#include <mugef/consts.h>


#define MPC_NUM_CHANNELS            (MUGEF_MAX_CHANNELS + 2)                        // Number of channels supported by MPC card

#define MPC_DATA_SIZE               0x000A00                                        // Size of MPC channel data
#define MPC_NUM_PARMS               5                                               // Number of parameters for MPC commands

// Global channel commands

#define MPC_GLOBAL_CHANNEL          64      // Global channel number
#define MPC_CMD_SET_1_LINK          0x0001  // Set link type for single channel                 (channel)
#define MPC_CMD_SET_ALL_LINK        0x0002  // Set link type for all channels
#define MPC_CMD_SET_1_LINK_T        0x0003  // Set link type for single channel                 (channel, position)
#define MPC_CMD_GET_1_LINK          0x0004  // Get link type for single channel
#define MPC_CMD_GET_ALL_LINK        0x0005  // Get link type for all channels
#define MPC_CMD_GET_ALL_DAISY       0x0014
#define MPC_CMD_GET_ONE_DAISY       0x0015  //                                                  (channel)
#define MPC_CMD_CLEAR_RESP_BUF      0x001E  // Clean response data buffer                       (channel)
#define MPC_CMD_SEND_VME_BUF        0x0028  // Send contents of VME buffer                      (channel, length)

// System channel commands

#define MPC_SYSTEM_CHANNEL          65      // System channel number
#define MPC_CMD_FPGA_VERSION        0x0001  // Get FPGA version                                 (uarts, daisy, vme)
#define MPC_CMD_FPGA_RESET          0x0002  // Reset FPGA                                       (uarts, daisy)
#define MPC_CMD_GET_ANA_DAISY       0x0003  // Get analogue data over daisy chain               (number of samples)
#define MPC_CMD_HELP_LIST           0x0004  // Retrieve help list
#define MPC_CMD_GET_STATE           0x0005  // Get channel state                                (channel)


// MPC link register (link) constants

#define MPC_LINK_UNUSED             0x0000
#define MPC_LINK_SERIAL             0x0001
#define MPC_LINK_DAISY              0x0002

// MPC command state register (cmd_state) constants

#define MPC_CMD_STATE_READY         0x0000
#define MPC_CMD_STATE_PROC          0x0001
#define MPC_CMD_STATE_ERROR         0x0002
#define MPC_CMD_STATE_TIMEOUT       0x0003

// Constants for communication protocol

#define MPC_NETWORK_TMO_MS          100

