/*
 *  Filename: pc_families/4.h
 *
 *  Purpose:  Constants for family 4 power converters
 *
 *  Author:   Stephen Page
 */

#ifndef PC_FAMILY_4_H
#define PC_FAMILY_4_H

// State bit mask definitions

#define PC_FAMILY_4_STATE_380V_FLT          0x0001
#define PC_FAMILY_4_STATE_NO_INT_INTLOCK    0x0002
#define PC_FAMILY_4_STATE_LOCAL_CTRL        0x0004
#define PC_FAMILY_4_STATE_OFF               0x0008
#define PC_FAMILY_4_STATE_NOT_POL_POS       0x0010
#define PC_FAMILY_4_STATE_NOT_POL_NEG       0x0020
#define PC_FAMILY_4_STATE_FILTER_FLT        0x0040
#define PC_FAMILY_4_STATE_NOT_MODE_3        0x0080
#define PC_FAMILY_4_STATE_MAGNET_OK         0x0100
#define PC_FAMILY_4_STATE_NO_EXT_INTLOCK    0x0200
#define PC_FAMILY_4_STATE_NOT_MODE_2        0x0400
#define PC_FAMILY_4_STATE_NOT_MODE_1        0x0800

// Fault bits with value 1 when in fault

#define PC_FAMILY_4_FAULTS_ONE             (PC_FAMILY_4_STATE_380V_FLT          | \
                                            PC_FAMILY_4_STATE_LOCAL_CTRL        | \
                                            PC_FAMILY_4_STATE_FILTER_FLT)

// Fault bits with value 0 when in fault

#define PC_FAMILY_4_FAULTS_ZERO            (PC_FAMILY_4_STATE_NO_INT_INTLOCK    | \
                                            PC_FAMILY_4_STATE_MAGNET_OK         | \
                                            PC_FAMILY_4_STATE_NO_EXT_INTLOCK)

// Faults 1 bit mask definitions

#define PC_FAMILY_4_FAULTS_1_FAST_STOP      0x0001
#define PC_FAMILY_4_FAULTS_1_DOORS          0x0002
#define PC_FAMILY_4_FAULTS_1_WATER          0x0004
#define PC_FAMILY_4_FAULTS_1_FILTER_CAPA    0x0008
#define PC_FAMILY_4_FAULTS_1_SUPPRESS       0x0010
#define PC_FAMILY_4_FAULTS_1_CHOKE_TEMP     0x0020
#define PC_FAMILY_4_FAULTS_1_BRIDGE_TEMP    0x0040
#define PC_FAMILY_4_FAULTS_1_TRANSFO_TEMP   0x0080
#define PC_FAMILY_4_FAULTS_1_THYRISTOR_FUSE 0x0100
#define PC_FAMILY_4_FAULTS_1_AUX_FUSE       0x0200
#define PC_FAMILY_4_FAULTS_1_DCCT           0x0400
#define PC_FAMILY_4_FAULTS_1_5_15V          0x0800
#define PC_FAMILY_4_FAULTS_1_MCB            0x1000

// Faults 2 bit mask definitions

#define PC_FAMILY_4_FAULTS_2_OVER_U_DC      0x0001
#define PC_FAMILY_4_FAULTS_2_OVER_I_AC      0x0002
#define PC_FAMILY_4_FAULTS_2_OVER_RIPPLE    0x0004
#define PC_FAMILY_4_FAULTS_2_OVER_I_RMS     0x0008
#define PC_FAMILY_4_FAULTS_2_OVER_I_DC      0x0010
#define PC_FAMILY_4_FAULTS_2_EARTH_LEAK     0x0020
#define PC_FAMILY_4_FAULTS_2_EXT_SPARE      0x0040
#define PC_FAMILY_4_FAULTS_2_MAGNET         0x0100
#define PC_FAMILY_4_FAULTS_2_EXT_CHAIN      0x0200
#define PC_FAMILY_4_FAULTS_2_EXT_SUM        0x0400
#define PC_FAMILY_4_FAULTS_2_MCB_WARNING    0x2000
#define PC_FAMILY_4_FAULTS_2_DCCT_WARNING   0x4000
#define PC_FAMILY_4_FAULTS_2_POL_WARNING    0x8000

#endif

// EOF
