/*
 *  Filename: pc_families/4.h
 *
 *  Purpose:  Constants for family 4 power converters
 *
 *  Author:   Stephen Page
 */

#ifndef PC_FAMILY_6_H
#define PC_FAMILY_6_H

// State bit mask definitions

#define PC_FAMILY_6_STATE_ON                0x0001
#define PC_FAMILY_6_STATE_VS_FLT            0x0002
#define PC_FAMILY_6_STATE_VMOD_FLT          0x0004

// Fault bits with value 1 when in fault

#define PC_FAMILY_6_FAULTS_ONE             (PC_FAMILY_6_STATE_VS_FLT | \
											PC_FAMILY_6_STATE_VMOD_FLT )

#endif

// EOF
