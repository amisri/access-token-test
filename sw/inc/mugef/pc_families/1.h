/*
 *  Filename: pc_families/1.h
 *
 *  Purpose:  Constants for family 1 power converters
 *
 *  Author:   Stephen Page
 */

#ifndef PC_FAMILY_1_H
#define PC_FAMILY_1_H

// State bit mask definitions

#define PC_FAMILY_1_STATE_OFF               0x0001
#define PC_FAMILY_1_STATE_LOCAL_CTRL        0x0002
#define PC_FAMILY_1_STATE_FAULT             0x0004
#define PC_FAMILY_1_STATE_OVERLOAD_FLT      0x0008
#define PC_FAMILY_1_STATE_EARTH_FLT         0x0010
#define PC_FAMILY_1_STATE_380V_FLT          0x0020
#define PC_FAMILY_1_STATE_AUX_SUPPLY_FLT    0x0040
#define PC_FAMILY_1_STATE_OVER_TEMP_FLT     0x0080

// Fault bits with value 1 when in fault

#define PC_FAMILY_1_FAULTS_ONE             (PC_FAMILY_1_STATE_LOCAL_CTRL        | \
                                            PC_FAMILY_1_STATE_FAULT             | \
                                            PC_FAMILY_1_STATE_OVERLOAD_FLT      | \
                                            PC_FAMILY_1_STATE_EARTH_FLT         | \
                                            PC_FAMILY_1_STATE_380V_FLT          | \
                                            PC_FAMILY_1_STATE_AUX_SUPPLY_FLT    | \
                                            PC_FAMILY_1_STATE_OVER_TEMP_FLT)

#endif

// EOF
