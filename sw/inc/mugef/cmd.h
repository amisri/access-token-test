/*
 *  Filename: mugef/cmd.h
 *
 *  Purpose:  Constants for power converter commands
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEF_CMD_H
#define MUGEF_CMD_H

// Command flags

#define MUGEF_CMD_GET               0x8000                              // Command reads data
#define MUGEF_CMD_SET               0x4000                              // Command supplies data
#define MUGEF_CMD_DIR               (MUGEF_CMD_GET | MUGEF_CMD_SET)     // Bits giving direction
#define MUGEF_CMD_MASK              0x0FFF                              // Bits used for the command
#define MUGEF_CMD_DAISY             0x00FF                              // Bits used for a daisy (or serial) link
#define MUGEF_CMD_SERIAL            0x0F00                              // Bits used for serial link only
#define MUGEF_CMD_PRIO              0x2000                              // Bit used for command priority
#define MUGEF_CMD_LOW_PRIO          0x0000                              // Low priority
#define MUGEF_CMD_HIGH_PRIO         0x2000                              // High priority

// Only the following commands are available on old AuxPS converters

#define MUGEF_CMD_START             (MUGEF_CMD_SET | 0x0001)                    // Start the power converter
#define MUGEF_CMD_STOP              (MUGEF_CMD_SET | 0x0002)                    // Stop the power converter
#define MUGEF_CMD_POL_POS           (MUGEF_CMD_SET | 0x0008)                    // Polarity switch positive
#define MUGEF_CMD_POL_NEG           (MUGEF_CMD_SET | 0x0010)                    // Polarity switch negative
#define MUGEF_CMD_MODE1             (MUGEF_CMD_SET | 0x0040)                    // Power converter mode 1
#define MUGEF_CMD_MODE2             (MUGEF_CMD_SET | 0x0020)                    // Power converter mode 2
#define MUGEF_CMD_MODE3             (MUGEF_CMD_SET | 0x0080)                    // Power converter mode 3

// The following commands are handled by the PLC card (a MPC card is required)

#define MUGEF_CMD_RESET             (MUGEF_CMD_SET | 0x0003)                    // Reset faults
#define MUGEF_CMD_STANDBY           (MUGEF_CMD_SET | 0x0004)                    // Power converter to standby

// All the following commands intend to reply with a word. Deprecated as the implementation is currently unsafe

#define MUGEF_CMD_DEFAULT_STATUS    (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B0)    // Read default status
#define MUGEF_CMD_FLT1              (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B1)    // Read faults 1
#define MUGEF_CMD_FLT2              (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B2)    // Read faults 2
#define MUGEF_CMD_FIRST_FLT1        (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B3)    // Read first fault 1
#define MUGEF_CMD_FIRST_FLT2        (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B4)    // Read first fault 2
#define MUGEF_CMD_ANA_CHAN          (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B5)    // Read analogue channel number
#define MUGEF_CMD_INCOH             (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B6)    // Read incoherent states
#define MUGEF_CMD_TIMEOUTS          (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B7)    // Read converter timeouts
#define MUGEF_CMD_ACTIVE_FLT1       (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B8)    // Read active faults 1
#define MUGEF_CMD_ACTIVE_FLT2       (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00B9)    // Read active faults 2
#define MUGEF_CMD_LHC_STATUS        (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00BA)    // Read status unique to converters in the LHC
#define MUGEF_CMD_VER_CPU           (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00C0)    // Read version of HC16 program
#define MUGEF_CMD_VER_FPGA          (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00C1)    // Read version of FPGA programming
#define MUGEF_CMD_PC_TYPE           (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00C2)    // Read type of power converter
#define MUGEF_CMD_VER_TYPE          (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00C3)    // Read version of type configuration
#define MUGEF_CMD_ZERO_PLC_COUNTER  (MUGEF_CMD_SET | 0x00C4)                    // Zero PLC counter
#define MUGEF_CMD_INC_PLC_COUNTER   (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00C5)    // Increment and read PLC counter
#define MUGEF_CMD_TEST_BUS_5555     (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x0055)    // Pattern test status with 0x5555
#define MUGEF_CMD_TEST_BUS_AAAA     (MUGEF_CMD_GET | MUGEF_CMD_SET | 0x00AA)    // Pattern test status with 0xAAAA

#endif

// EOF
