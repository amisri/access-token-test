/*
 *  Filename: mugef_consts.h
 *
 *  Purpose:  Constants for the Mugef project
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEF_CONSTS_H
#define MUGEF_CONSTS_H

//#define DEBUG

#define MUGEF_MAX_CHANNELS      64      // Maximum number of channels per Mugef crate

#endif

// EOF
