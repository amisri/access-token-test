/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc_fieldbus.h

  Contents:     Structures, constants and descriptions for communication between the Gateway and FGCs via
                a fieldbus.

  Notes:        There are two fieldbuses used by the FGCs: WorldFIP (2.5Mbps) and FGC_Ether
                (100Mbps Ethernet plus 50Hz sync signal).  The same FGC protocols apply to both
                fieldbuses however the packeting is different because Ethernet packets are larger:

                                                            WorldFIP               FGC_Ether

                  Max number of FGCs per gateway             30                        64
                  Cycle time                                 20 ms                     20 ms

                  Gateway broadcast packet size              64 bytes                1344 bytes
                  FGC status packet size                     56 bytes                  78 bytes
                  Gateway command packet size             3-122 bytes             46-1500 bytes
                  FGC remote terminal characters packet   3- 66 bytes             Included in status packet
                  FGC command response packet             3-122 bytes             46-1500 bytes
                  FGC publication packet                  3-122 bytes             46-1500 bytes

                                                          Sizes exclude FIP or Ethernet headers

                Periodic broadcast from the Gateway to all FGCs:

                   The gateway broadcasts three sets of information every cycle:
                                                                     FIP        FGC_Ether
                     1. Time/Advertise Codes/Events structure      64 bytes      64 bytes
                     2. Code (in blocks of 32 bytes)               64 bytes    1024 bytes
                     3. Real-time data (4 bytes/FGC)              120 bytes     256 bytes

                   For WorldFIP, all broadcasts must be in 64-byte "WorldFIP variables".
                   Four are sent per cycle:

                        ms  0: Time/Advertise Codes/Events structure
                        ms  1: Code (2 x 32 byte blocks)
                        ms 19: Real-time data for FGCs  1-15
                               Real-time data for FGCs 16-30

                   The FGCs only receive the Code variable when running in the boot and they only
                   receive one of the two real-time data variables (according to their ID) when they
                   are running the main program.

                   For FGC_Ether, all the broadcast data is sent in one Ethernet packet and this
                   packet is broadcast twice; at the start of ms 18 and ms 19.  The content is the
                   same except for the millisecond time.  It is sent twice for redundancy reasons.
                   Only one needs to be received.

                Periodic status from the FGCs to the Gateway:

                   The FGC sends 56 bytes of status every 20ms to the gateway.  This is sent
                   between ms 2 and ms 10 for WorldFIP and will be sent on ms 2 for FGC_Ether.
                   On FGC_Ether, the packet will contain 21 bytes of remote terminal data
                   for a total length of 78 bytes.

                Remote terminal characters

                   The FGC can produce up to 960 remote terminal characters per second.  These
                   are sent in different ways depending upon the network:

                        FIP:    The boot sends the rterm data in the the periodic 56 byte status packet
                                The main program sents the rterm data in the beginning of aperiodic messages.

                        FGC_Ether: The boot and main program send the rterm data in the periodic 78 byte
                                   status packet.

                Aperiodic command packets from the Gateway to the FGCs:

                   The same protocol is used for FIP and FGC_Ether (described below) however there are
                   differences.  The FIP message payload can be from 1 to 122 bytes long, while the
                   FGC_Ether payload can be from 46 to 1500 bytes long.  For this reason the FGC_Ether
                   command packet includes a length word to say how many bytes of real data there are.

                   With WorldFIP, the gateway can only send a command packet every other cycle because
                   the request to send a packet must be queued with the WorldFIP bus arbitrator on one
                   cycle and it will then be requested the next cycle.

                   With FGC_Ether, the gateway can send a command packet every cycle so the maximum
                   throughput will be 75000 bytes/s compared to 3000 bytes/s for WorldFIP.

                Aperiodic command response packets from the FGCs to the Gateway:

                   The same protocol is used for FIP and FGC_Ether (described below) with the same
                   packet size conditions described above for the command packets.  Further more,
                   with WorldFIP the FGC can only send at most one packet per cycle (6000 bytes/s)
                   while for FGC_Ether, there isn't a fixed limit.  The actual limit will probably be
                   three packets (220 KB/s) - the bottleneck will be the MCU processing time to
                   prepare the data to be sent.

                Aperiodic publication packets from the FGCs to the Gateway:

                   The same protocol is used for FIP and FGC_Ether (described below) with the same
                   packet size conditions described above for the command packets.  Further more,
                   with WorldFIP the FGC can only send at most one packet per cycle (6000 bytes/s)
                   while for FGC_Ether, there isn't a fixed limit.  The actual limit will probably be
                   three packets (220 KB/s) - the bottleneck will be the MCU processing time to
                   prepare the data to be sent.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_FIELDBUS_H
#define FGC_FIELDBUS_H

#include <stdint.h>

#include "fgc_consts_gen.h"
#include "fgc_event.h"
#include "fgc_stat.h"

// General constants

#define FGC_FIELDBUS_CYCLE_PERIOD_MS    20                                      // Fieldbus cycle period in ms
#define FGC_FIELDBUS_CYCLE_PERIOD_S     0.02                                    // Fieldbus cycle period in s
#define FGC_FIELDBUS_CYCLE_FREQ         (1000 / FGC_FIELDBUS_CYCLE_PERIOD_MS)   // Fieldbus cycles per second

/*----- Time/Events/Code Info structure - Length: 64 bytes - (FIP VAR:0x3000) -----*\

            +0      +1      +2      +3      +4      +5      +6      +7
        +-------+-------+-------+-------+-------+-------+-------+-------+
 0:0x00 |FGC Id |TermCh |    ms time    |        UTC (Unix Time)        |
        +-------+-------+-------+-------+-------+-------+-------+-------+
 8:0x08 |  NameDB CRC   |  Old adv ver  |Cde cls|Code id|Code block idx |
        +-------+-------+-------+-------+-------+-------+-------+-------+
16:0x10 |Adv Idx|Adv Len|Adv Cls|Adv Cde| Adv code CRC16|Adv code blklen|
        +-------+-------+-------+-------+-------+-------+-------+-------+
24:0x18 |  runlog_idx   | flags |Reservd|       Adv code version        |
        +-------+-------+-------+-------+-------+-------+-------+-------+
32:0x20 |                         Event slot 0                          |       4 x 8 Byte events
        +-------+-------+-------+-------+-------+-------+-------+-------+
40:0x28 |                         Event slot 1                          |
        +-------+-------+-------+-------+-------+-------+-------+-------+
48:0x30 |                         Event slot 2                          |
        +-------+-------+-------+-------+-------+-------+-------+-------+
56:0x38 |                         Event slot 3                          |
        +-------+-------+-------+-------+-------+-------+-------+-------+

The time structure provides nine services in total:

1. Transmission of date/time
2. Synchronisation of PLL (FIP and FGC_Ether when sync pulse is not available)
3. Transmission of remote terminal keyboard characters
4. Transmission of FGC timing events
5. Transmission of the run log byte index - this specifies which byte of the FGC's runlog it should publish
6. Transmission of synchronisation flags
7. Transmission of the advertised code table
8. Transmission of the code block header for the code that will be sent in the code variable
9. Transmission of the CRC of the NameDB code

Notes:  .fgc_id can be from 0-FGC_MAX_DEVS_PER_GW.  0 means that .fgc_char is not used.

        .fgc_char will be used by the FGC identified in .fgc_id as a remote terminal keyboard character

        .ms_time is the time of the millisecond. e.g. 0, 20, 40, ..., 960, 980, 0, 20, ...
        This value must change for the time variable to be acknowledged in the status variable
        response from the FGC.

        See http://cern.ch/fgc/static/Project/Software/CodeDistribution.htm
        for information about the code related fields.
*/

#define FGC_NUM_EVENT_SLOTS         4    // Number of FGC event slots in the time variable
#define FGC_FLAGS_SYNC_LOG       0x01    // Flag to synchronise log data readout
#define FGC_FLAGS_PC_PERMIT      0x02    // PC permit flag
#define FGC_FLAGS_SECTOR_ACCESS  0x04    // Sector Access flag
#define FGC_FLAGS_ECONOMY_FULL   0x08    // Full economy flag
#define FGC_FLAGS_LOGGER_ENABLED 0x40    // FGC logger enabled flag
#define FGC_FLAGS_PM_ENABLED     0x80    // PM enabled flag

struct fgc_fieldbus_time
{
    uint8_t             fgc_id;                     // [0x00] FGC ID: 0-FGC_MAX_DEVS_PER_GW
    uint8_t             fgc_char;                   // [0x01] FGC rterm character
    uint16_t            ms_time;                    // [0x03] ms time: 0,20,40,...,960,980,0,20,...      on FIP
    //                 18,19,38,39,...,998,999,18,19,... on ETH
    uint32_t            unix_time;                  // [0x04] UTC as seconds since start of 1970
    uint16_t            namedb_crc;                 // [0x08] NameDB (Code 0) checksum
    uint16_t            old_adv_code_version;       // [0x0A] Old advertised code version
    uint8_t             code_var_class_id;          // [0x0C] Code variable class
    uint8_t             code_var_id;                // [0x0D] Code variable ID
    uint16_t            code_var_idx;               // [0x0E] Code variable block index (32-byte blocks)
    uint8_t             adv_list_idx;               // [0x10] Advertised code list index
    uint8_t             adv_list_len;               // [0x11] Advertised code list length
    uint8_t             adv_code_class_id;          // [0x12] Advertised code class ID
    uint8_t             adv_code_id;                // [0x13] Advertised code ID
    uint16_t            adv_code_crc;               // [0x14] Advertised code CRC16
    uint16_t            adv_code_len_blk;           // [0x16] Advertised code length (32-byte blocks)
    uint16_t            runlog_idx;                 // [0x18] Run log index
    uint8_t             flags;                      // [0x1A] Flags
    uint8_t             reserved1;                  // [0x1B] Reserved
    uint32_t            adv_code_version;           // [0x1C] Advertised code version
    struct fgc_event    event[FGC_NUM_EVENT_SLOTS]; // [0x20] FGC event slots
};

/*----- FGC Status structure - Length: 56 bytes - (FIP VAR: 0x06{fgc_id}) -----*\

The FGC status structure starts with 16 bytes that are common to all classes:

         80    7 40    6 20    5 10    4 08    3 04    2 02    1 01    0
        +-------+-------+-------+-------+-------+-------+-------+-------+
    0   | ExtPM |SelfPM |PllLock|CMD_TOG|  RDP  |  Time |    Status     | -+-- .ack
        +-------+-------+-------+-------+-------+-------+-------+-------+
    1   |                       CMD Status Number                       | -+-- .cmd_stat
        +-------+-------+-------+-------+-------+-------+-------+-------+

 Protocol bits (.ack) notes:

    FGC_ACK_STATUS     Unchanged        Status variable not refreshed
                      Incremented       Status variable refreshed

    FGC_ACK_TIME          0x00          New time_v not received
                          0x04          New time_v received with different .ten_ms_time

    FGC_ACK_RD_PKT        0x00          RT Data packet not received
                          0x08          RT Data packet received

    FGC_ACK_CMD_TOG    Unchanged        Command packet not received
                      Toggled 0x10      Command packet received and buffered

    FGC_ACK_PLL           0x00          PLL not locked
                          0x20          PLL locked

    FGC_SELF_PM_REQ       0x00          Log data ready for readout
                          0x40          Self-triggered post-mortem (GW+FGC data) requested

    FGC_EXT_PM_REQ        0x00          No external post-mortem requested
                          0x80          External post-mortem (GW data only) requested

 Note: FGC_EXT_PM_REQ will be set when entering the state SLOW_PA and will be reset after 100ms.
       FGC_SELF_PM_REQ will be set when entering the FLT_STOPPING state if any fault is present
       except NO_PC_PERMIT.  The bit will be reset when logging has stopped and the buffers are
       ready to read out.

 See fgc_errors.h for cmd_stat codes.
*/

#define FGC_ACK_STATUS      0x03
#define FGC_ACK_TIME        0x04
#define FGC_ACK_RD_PKT      0x08
#define FGC_ACK_CMD_TOG     0x10
#define FGC_ACK_PLL         0x20
#define FGC_SELF_PM_REQ     0x40
#define FGC_EXT_PM_REQ      0x80

struct fgc_fieldbus_stat
{
    uint8_t     ack;                            // [0] FGC acknowledgement byte
    uint8_t     class_id;                       // [1] FGC device class ID
    uint8_t     cmd_stat;                       // [2] FGC command status byte
    uint8_t     runlog;                         // [3] FGC runlog data byte (index from runlog_idx in time)
    uint8_t     diag_chan[FGC_DIAG_N_LISTS];    // [4] Diagnostic channels 0x00-0xFE (0xFF = chan not in use)
    uint16_t    diag_data[FGC_DIAG_N_LISTS];    // [8] Diagnostic data
};

union fgc_stat_var
{
    struct fgc_fieldbus_stat    fieldbus_stat;  // Used for comms from FGC to GW
    struct fgc_stat             fgc_stat;       // Used for client programs receiving data forwarded by GW
};

/*----- Command, Response and Publication structures -----*\

Commands, Responses and Publications use point-to-point transmissions between the Gateways
and the FGCs. Gateway commands have a two byte header: [Flags][User].  Response and publication
also have a two byte header: [Flags][n_rterm_chs].  The n_rterm_chs will be zero always for
FGC_Ether.

For WorldFIP, up to 64 characters at the beginning of the packet can be dedicated
to remote terminal characters.  The rest of the packet can contain:

  A. Nothing if no reponse or publication is in progress
  B. Response characters
  C. Publication characters

Note that Publication packets take priority over Response packets.

For FGC_Ether, remote terminal characters are always returned in periodic status packet and
multiple Response and Publication packets can be sent during the same 20 ms cycle.

1. The Header for all types of aperiodic packets contains a flags byte which is interpretted as follows:

                         7  6  5  4  3  2  1  0
  Gateway messages:     [-][-][L][F][ CMD TYPE ]
  FGC messages:         [RPT ][L][F][    SEQ   ]

Where RPT is the Response Packet Type and SEQ is the sequence number.  There is a
separate sequence number for Response packets and Publication packets.  If a FIP packet
contains only remote terminal characters then SEQ is 0xF.

2. Payload: The payload following the header is of variable length.  The format depends upon the message type.

   Gateway packets:

        Command packets contain ASCII characters (not NUL terminated).

            char        cmd_pkt[FGC_FIELDBUS_MAX_CMD_LEN];

    FGC packets:

     1. Command response packets         (RPT = 00 = 0x00)

        A. SUB command:

            [SUB_IDX][;]

        B. UNSUB command

            [SUB_IDX][;]

        C. GET command or GET_SUB command

            [TIMESTAMP][Get function response][;]

     2. Direct Publication packets       (RPT = 01 = 0x40)

        [SUB_IDX][FLAGS][USER][TIMESTAMP][Get function response][;]

     3. Publication get request packets  (RPT = 10 = 0x80)

        [SUB_IDX][FLAGS][USER][;]

     Where:

        [TIMESTAMP] = two big-endien long words (UNIX time and microseconds within the second)

Publication:

Because the throughput for WorldFIP is so limited, only small properties will be published through
the publication channel.  This channel has priority above responses.  For large properties (>100 bytes)
the FGC sends a short PUB_GET request via the publication channel and the gateway queues a GET_SUB
command to recover the data.  This allows small publications to get through ahead of get commands
and big publications.

With FGC_Ether, there is no contention between publication and get responses so all PUB_GETs will not
be used.
*/

// Flag constants

// General

#define FGC_FIELDBUS_FLAGS_FIRST_PKT        0x10    // First packet of a command/response
#define FGC_FIELDBUS_FLAGS_LAST_PKT         0x20    // Last packet of a command/response

// Command from a gateway

#define FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK    0x0F    // Mask for bottom nibble containing type of command
#define FGC_FIELDBUS_FLAGS_SET_CMD          0x01    // Set command packet
#define FGC_FIELDBUS_FLAGS_GET_CMD          0x02    // Get command packet
#define FGC_FIELDBUS_FLAGS_SET_BIN_CMD      0x03    // Binary set command packet
#define FGC_FIELDBUS_FLAGS_SUB_CMD          0x04    // Subscribe command packet
#define FGC_FIELDBUS_FLAGS_UNSUB_CMD        0x05    // Unsubscribe command packet
#define FGC_FIELDBUS_FLAGS_GET_SUB_CMD      0x06    // Get for sub command packet

// Response from an FGC

#define FGC_FIELDBUS_FLAGS_SEQ_MASK         0x0F    // Response sequence number mask
#define FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK    0xC0    // Response type mask
#define FGC_FIELDBUS_FLAGS_CMD_PKT          0x00    // Any packet of a command/response
#define FGC_FIELDBUS_FLAGS_PUB_DIRECT_PKT   0x40    // Packet contains publication data
#define FGC_FIELDBUS_FLAGS_PUB_GET_PKT      0x80    // Packet contains publication get request

// Published data flags

#define FGC_FIELDBUS_PUB_TRIG_MASK          0x03    // Mask of publication trigger type bits
#define FGC_FIELDBUS_PUB_TRIG_NEW_SUB       0x01    // The publication was triggered by the start of a subscription
#define FGC_FIELDBUS_PUB_TRIG_SET           0x02    // The publication was triggered by a set command
#define FGC_FIELDBUS_PUB_MULT_PPM           0x04    // The publication is multi-PPM. The sub-device selector is in the 3 msb
#define FGC_FIELDBUS_PUB_SUB_DEV_SEL_MASK   0xE0    // Sub-device selector mask
#define FGC_FIELDBUS_PUB_SUB_DEV_SEL_SHIFT  0x05    // Sub-device selector bit shift

/*----- Gateway command packet message -----*\

  Notes:

    1.  Command packet messages are always acknowledged on the following cycle, so packets
        for a given FGC are never sent more often than every 40 ms.

    2.  Commands longer than a single packet will be broken into pieces.

    3.  All command packet messages have the FGC_FIELDBUS_CMD_PKT bit set in the header byte.

    4.  There are four types of command packet, identified by the FGC_FIELDBUS_FLAGS_LAST_PKT and
        FGC_FIELDBUS_FLAGS_FIRST_PKT bits in the header word:

        Type    FIRST_CMD_PKT   LAST_CMD_PKT    Length                      Contents
        --------------------------------------------------------------------------------------------
         0          0               0             FGC_FIELDBUS_MAX_CMD_LEN  Middle packet of command
         1          0               1           1-FGC_FIELDBUS_MAX_CMD_LEN  Last packet of command
         2          1               0             FGC_FIELDBUS_MAX_CMD_LEN  First packet of command
         3          1               1           1-FGC_FIELDBUS_MAX_CMD_LEN  Only packet of command
        --------------------------------------------------------------------------------------------

    5.  Command packets are acknowledged by the FGC via the .ack and .cmd_stat
        fields of the status variable.  If the packet is correctly received and buffered, then
        the FGC will toggle the FGC_ACK_CMD_TOG bit in .ack.

    6.  The state of command processing is reported via the .cmd_stat field of the status variable.
        The values which can be in these fields are defined in fgc_errs.h.  In the normal situation
        the state will go through the following changes:

            FGC_OK_NO_RSP|FGC_OK_RSP -> FGC_RECEIVING -> FGC_EXECUTING -> FGC_OK_NO_RSP|FGC_OK_RSP

        At any stage it may change to an error state if there is a problem.  Not all stages of this
        chain of states will necessarily be reported to the gateway.
*/

struct fgc_fieldbus_cmd_header
{
    uint8_t flags;                                          // [0] Flags
    uint8_t user;                                           // [1] User number for PPM
};

#ifdef FGC_FIELDBUS_MAX_CMD_LEN
struct fgc_fieldbus_cmd
{
    struct fgc_fieldbus_cmd_header header;                  // [0] Header

    char cmd_pkt[FGC_FIELDBUS_MAX_CMD_LEN -
                 sizeof(struct fgc_fieldbus_cmd_header)];   // [2] Command data
};
#endif

/*----- FGC response packet message -----*\

  Notes:

    1.  A response message will be queued if there is at least one remote terminal
        character, or one command response character, or one publication character to be sent.

    2.  If the packet contains command response characters, the FGC_FIELDBUS_CMD_PKT bit of the header
        byte will be set.  The bits FGC_FIELDBUS_FLAGS_FIRST_PKT and FGC_FIELDBUS_FLAGS_LAST_PKT will be set
        if the packet is the first or last of a command response.

    3.  The remote terminal is limited to 64 characters per message.  The number of characters will be set
        in n_term_chs.

    4.  The low nibble of the flags byte will be incremented before each message is sent.

*/

#define FGC_FIELDBUS_MAX_RTERM_CHARS        64      // Max number of remote terminal chars in a rsp packet

struct fgc_fieldbus_rsp_header
{
    uint8_t flags;                                              // [0] Packet flags:Sequence number
    uint8_t n_term_chs;                                         // [1] Number of remote term chars
};

#ifdef FGC_FIELDBUS_MAX_RSP_LEN
struct fgc_fieldbus_rsp
{
    struct fgc_fieldbus_rsp_header header;                      // [0] Header
    uint8_t rsp_pkt[FGC_FIELDBUS_MAX_RSP_LEN -
                    sizeof(struct fgc_fieldbus_rsp_header)];    // [2] Response data
};
#endif

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc_fieldbus.h
\*---------------------------------------------------------------------------------------------------------*/
