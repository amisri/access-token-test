#!/usr/local/bin/perl -w
#

# Author:  Ph. Fraboulet

use File::stat;
use Digest::CRC qw(crc16 crcccitt);
use strict;
use Config;

if(@ARGV != 2)
{
    print "USAGE: perl crc.pl <type name> <size>\n";
    exit;
}
my ($type_name, $size)= @ARGV;

# checksum of package file

open (PACK, "+<$type_name.pack") || die("cannot open package file for checksum");
binmode(PACK);

my $ch_cs    = 0;
my $ch_0     = 0;
my $ch_1     = 0;
my $crc_str  = "";
my $nb_skip  = 0;
my $checksum = 0;
my $c_low;
my $c_high;
my $c;
my $index = 0;
while(read(PACK,$ch_0,1))
{
    $nb_skip += 2;
    read(PACK,$ch_1,1);
    if($nb_skip >= 10)
    {
        $crc_str .= $ch_0.$ch_1;
    }

    if($nb_skip == $size)
    {
        last;
    }

#    $c_low = ord($ch_cs);
#    if(!read(PACK,$ch_cs,1))
#    {
#	print "ERROR: odd number of bytes\n";
#    }
#    $c_high = ord($ch_cs);
#    $c = $c_high*0x100 + $c_low;
#    $checksum ^= $c;
#    printf " = 0x%04X - CS 0x%04X\n", $c, $checksum;
}

$checksum = Digest::CRC->new(width=>16, init=>0x0000, xorout=>0x0000, poly=>0x1021, refin=>1, refout=>1)->add($crc_str)->digest;
printf("%i - 0x%04X\n",$nb_skip,$checksum);

printf PACK (pack('v', $checksum));
close(PACK);

#EOF
