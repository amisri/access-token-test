loadProjectFile -file "Z:/projects/fgc/hw/fq120A10V/pld/fq120A10V.cdf"

setPreference -pref StartupClock:AUTO_CORRECTION
setPreference -pref AutoSignature:FALSE
setPreference -pref KeepSVF:TRUE
setPreference -pref ConcurrentMode:FALSE
setPreference -pref UseHighz:FALSE
setPreference -pref ConfigOnFailure:STOP
setPreference -pref UserLevel:NOVICE
setPreference -pref MessageLevel:DETAILED
setPreference -pref svfUseTime:FALSE
setPreference -pref SpiByteSwap:AUTO_CORRECTION
setCable -port xsvf -file "Z:/projects/fgc/sw/pldprog/c87/P100-Boot/TypeFileMaker/FQ120A10V/id.xsvf"
ReadIdcode -p 1 

setCable -port xsvf -file "Z:/projects/fgc/sw/pldprog/c87/P100-Boot/TypeFileMaker/FQ120A10V/0/0v.xsvf"
Verify -p 1 
setCable -port xsvf -file "Z:/projects/fgc/sw/pldprog/c87/P100-Boot/TypeFileMaker/FQ120A10V/0/0p.xsvf"
Program -p 1 -e -defaultVersion 0

exit
