#!/usr/local/bin/perl -w

use strict;
use Config;

open (PACK, ">test.pack") || die("cannot create package file");
binmode(PACK);

my $i = 0;

for($i = 0; $i < 10000 ; $i++)
{
    print PACK (pack('C', $i));
}