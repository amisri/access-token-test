#!/usr/local/bin/perl -w
#
# Name:    make_type.pl
# Purpose: make pld prog type file to be downloaded into flash memory
#
#		Create the package file for a type of entity to reprogram (ex. FGC50, FGC60, ...). This package
#		file will be loaded into PldProg flash memory so that it can check and reprogramm the pld chain.
#		The package structure is the following:
#
#		header : 0xDEADBEEF (synchronization)
#		0x00 : Flash address of package 							- 32 bits
#		0x04 : Package total size	 							- 32 bits
#		0x08 : type number of entity (0: FGC50, ..., cf types.txt) 				- 16 bits
#		0x0A : Number of jtag devices in chain							- 16 bits
#		0x0C : Id(0)										- 16 bits
#                    : Id(1)										- 16 bits
#                    : ...										- 16 bits
#		0x?? : Flash address of ID file (Address of file size, followed by the xsv)		- 32 bits
#		0x?? : Flash address of VERIFY file 0 (Address of file size, followed by the xsv)	- 32 bits
#		0x?? : Flash address of VERIFY file 1 (Address of file size, followed by the xsv)	- 32 bits
#                    : ...										- 32 bits
#		0x?? : Flash address of PROG file 0 (Address of file size, followed by the xsv)		- 32 bits
#		0x?? : Flash address of PROG file 1 (Address of file size, followed by the xsv)		- 32 bits
#                    : ...										- 32 bits
#		0x?? : Size of ID file									- 32 bits
#		0x?? : ID file (xsv)									- ?? bits
#		0x?? : Size of VERIFY file 0								- 32 bits
#		0x?? : VERIFY file 0 (xsv)								- ?? bits
#		0x?? : Size of VERIFY file 1								- 32 bits
#		0x?? : VERIFY file 1 (xsv)								- ?? bits
#		...
#		0x?? : Size of PROG file 0								- 32 bits
#		0x?? : PROG file 0 (xsv)								- ?? bits
#		0x?? : Size of PROG file 1								- 32 bits
#		0x?? : PROG file 1 (xsv)								- ?? bits
#		...
#		0x?? : CHECKSUM										- 16 bits
#
#
# Author:  Ph. Fraboulet

use Digest::CRC qw(crc16 crcccitt);
use strict;
use Config;

if(!$ARGV[0])
{
    print "USAGE: perl make_type.pl <type name>\n";
    exit;
}

my $print_file = 0;	# 1 to print the file in ascii

my $type_n;		# type index
my $type_start_addr;	# start addr in flash
my $type_size;		# in nb of pages (0x10000)
my $device_nb;		# number of device in jtag chain
my @id_list;		# list of device IDs

my $i;
my $address;
my $address_log;
my $total_size;

my $type_name = $ARGV[0];

chomp($type_name);

open (DEF, "PldProgTypes.txt") || die("cannot open types.txt");

while(<DEF>)
{
    if(/NAME:$type_name/)
    {
        $_ = <DEF>;
	chomp();
	$type_n = $_;
        $_ = <DEF>;
	chomp();
	$type_start_addr = hex($_);
        $_ = <DEF>;
	chomp();
	$device_nb = $_;
        $_ = <DEF>;
	chomp();
        @id_list         = split(/,/,$_);
    }
}
close(DEF);

binmode(STDOUT);


if(!scalar(@id_list))
{
    print "Type name not found in definition file\n";
    exit;
}

if(scalar(@id_list) != $device_nb)
{
    print "Incoherance between device number and ID list\n";
    exit;
}

my @bin_file = ();

#--- get id file size ---#

my $size_id  = -s "$type_name/id.xsvf";
my $id_align = ($size_id%2 == 1);			# odd nb of bytes

#--- Get prog & verif file sizes ---#

my @files_prog_sizes  = ();
my @files_prog_align  = ();
my @files_verif_sizes = ();
my @files_verif_align = ();

for($i=0 ; $i<$device_nb ; $i++)
{
    $files_verif_sizes[$i] = (defined(-s "$type_name/$i/".$i."v.xsvf") ? -s "$type_name/$i/".$i."v.xsvf" : 0);
    $files_verif_align[$i] = ($files_verif_sizes[$i]%2 == 1);	# odd size -> align

    $files_prog_sizes[$i] = (defined(-s "$type_name/$i/".$i."p.xsvf") ? -s "$type_name/$i/".$i."p.xsvf" : 0);
    $files_prog_align[$i]  = ($files_prog_sizes[$i]%2 == 1);	# odd size -> align
}

#----------- PRINT PACKAGE FILE ----------#

open (LOG, ">$type_name.log") || die("cannot create log.txt");

open (PACK, ">$type_name.pack") || die("cannot create package file");
binmode(PACK);


$address_log = $type_start_addr;

# Print synchro word

my $synchro_word = 3735928559;				# DEADBEEF
print PACK (pack('V',$synchro_word));

# print start address

print PACK (pack('V', $type_start_addr));		# addr in flash - 32 bits - V for unsigned long, little endian

printf LOG "0x%08X:\t0x%08X\n", $address_log, $type_start_addr;
$address_log += 4;

# print package size

$total_size = 	4 + 4 + 2 + 2 + $device_nb*2 +
		(2*$device_nb +	1)*4 +		# Files addresses
		$size_id + $id_align + 4	# Id file stuff (size, align & size storage)
		+ 2;				# Checksum
for($i=0; $i<$device_nb; $i++)
{
    $total_size += ($files_verif_sizes[$i] + 4 +			# file size + 4 bytes for file size storage
    		    $files_verif_align[$i] +
		    $files_prog_sizes[$i]  + 4 +
		    $files_prog_align[$i]);
}
print PACK (pack('V', $total_size));			# size of package - 32 bits

printf LOG "0x%08X:\t%i - 0x%08X(package size)\n", $address_log, $total_size, $total_size;
$address_log += 4;

print PACK (pack('v', $type_n));			# type          - 16 bits - v for unsigned short, little endian

printf LOG "0x%08X:\t%i\n", $address_log, $type_n;
$address_log += 2;

print PACK (pack('v', $device_nb));			# nb of devices - 16 bits

printf LOG "0x%08X:\t%i\n", $address_log, $device_nb;
$address_log += 2;

#--- print PACK ids ---#

for($i = 0 ; $i < $device_nb ; $i++)
{
    my $id_code;

    if($id_list[$i] =~ /xcs/)
    {
	$id_code = 0;
    }
    elsif($id_list[$i] eq 9536)
    {
	$id_code = 1;
    }
    elsif($id_list[$i] eq 9572)
    {
	$id_code = 2;
    }
    elsif($id_list[$i] eq 95108)
    {
	$id_code = 3;
    }
    elsif($id_list[$i] eq 95144)
    {
	$id_code = 4;
    }
    elsif($id_list[$i] =~ /18V/)
    {
	$id_code = 5;
    }
    else
    {
	print "ERROR: unknown ID type [$id_list[$i]]\n";
	exit;
    }

    print PACK (pack('v', $id_code));				# ids - 16 bits

    printf LOG "0x%08X:\t%i (ID %s)\n", $address_log, $id_code, $id_list[$i];
    $address_log += 2;
}

# print ID file start addr

# start of file part in package file (type_start + address + size + type + nb + ids + files addresses (prog & verif))
my $files_address = $type_start_addr + 4 + 4 + 2 + 2 + 2*$device_nb + 4 + 4*$device_nb + 4*$device_nb;


print PACK (pack('V', $files_address));				# ID file start addr - 32 bits

printf LOG "0x%08X:\t0x%08X\n", $address_log, $files_address;
$address_log += 4;

# print Verif files address

$address = $files_address + 4 + $size_id + $id_align;		# files address + ID file size + 4 bytes for file size long word

print PACK (pack('V',$address));				# 1st Verif file address - 32 bits

printf LOG "0x%08X:\t0x%08X\n", $address_log, $address;
$address_log += 4;

for($i=1 ; $i<$device_nb ; $i++)
{
    $address += 4 + $files_verif_sizes[$i-1] + $files_verif_align[$i-1];		# add previous file size + 4 bytes for file size long word
    print PACK (pack('V',$address));			 				# Verif file address - 32 bits

    printf LOG "0x%08X:\t0x%08X\n", $address_log, $address;
    $address_log += 4;
}

# print Prog files address

$address +=  4 + $files_verif_sizes[$device_nb-1] + $files_verif_align[$device_nb-1];	# files address + last verif file size + 4 bytes for file size

print PACK (pack('V',$address));			 		# 1st Verif file address - 32 bits

printf LOG "0x%08X:\t0x%08X\n", $address_log, $address;
$address_log += 4;

for($i=1 ; $i<$device_nb ; $i++)
{
    $address += 4 + $files_prog_sizes[$i-1] + $files_prog_align[$i-1];
    print PACK (pack('V',$address));			 		# Verif file address - 32 bits

    printf LOG "0x%08X:\t0x%08X\n", $address_log, $address;
    $address_log += 4;
}

#----- print PACK XSV files with their size on 4byte words at their beginnings

# print ID file

print PACK (pack('V', $size_id + $id_align));						# ID file size - 32 bits

printf LOG "0x%08X:\t%i (size ID)\n", $address_log, $size_id + $id_align;
$address_log += 4;

open (BIN, "<$type_name/id.xsvf") || die("cannot open $type_name/id.xsvf file");
binmode(BIN);

my $ch;
while(read(BIN,$ch,1))
{
    print PACK $ch;
}
close(BIN);

# Align ID file

if($id_align)		# align data
{
    print PACK 0;
}

printf LOG "0x%08X:\t#ID file (%i - 0x%08X)\n", $address_log, $size_id + $id_align, $size_id + $id_align;
$address_log += $size_id + $id_align;

# print verif files

for($i=0 ; $i<$device_nb ; $i++)
{
    print PACK (pack('V', $files_verif_sizes[$i] + $files_verif_align[$i]));						# file size - 32 bits

    printf LOG "0x%08X:\t%i (size VERIF)\n", $address_log, $files_verif_sizes[$i] + $files_verif_align[$i];
    $address_log += 4;

    if(open (BIN, "<$type_name/$i/".$i."v.xsvf"))		# file exists
    {
	binmode(BIN);

	while(read(BIN,$ch,1))
	{
	    print PACK $ch;
	}
	close(BIN);

	if($files_verif_align[$i])		# align data
	{
	    print PACK 0;
	}
    }

    printf LOG "0x%08X:\t#Verif file (%i - 0x%08X)\n", $address_log, $files_verif_sizes[$i] + $files_verif_align[$i], $files_verif_sizes[$i] + $files_verif_align[$i];
    $address_log += $files_verif_sizes[$i] + $files_verif_align[$i];
}

# print prog files

for($i=0 ; $i<$device_nb ; $i++)
{
    print PACK (pack('V', $files_prog_sizes[$i] + $files_prog_align[$i]));						# file size - 32 bits

    printf LOG "0x%08X:\t%i (size PROG)\n", $address_log, $files_prog_sizes[$i] + $files_prog_align[$i];
    $address_log += 4;

    if(open (BIN, "<$type_name/$i/".$i."p.xsvf"))		# file exists
    {
	binmode(BIN);

	while(read(BIN,$ch,1))
	{
	    print PACK $ch;
	}
	close(BIN);

	if($files_prog_align[$i])		# align data
	{
	    print PACK 0;
	}
    }

    printf LOG "0x%08X:\t#Prog file (%i - 0x%08X)\n", $address_log, $files_prog_sizes[$i] + $files_prog_align[$i], $files_prog_sizes[$i] + $files_prog_align[$i];
    $address_log += $files_prog_sizes[$i] + $files_prog_align[$i];
}

close PACK;

# checksum of package file

open (PACK, "+<$type_name.pack") || die("cannot open package file for checksum");
binmode(PACK);

my $ch_cs    = 0;
my $crc_str  = "";
my $checksum = 0;
my $c_low;
my $c_high;
my $c;
my $index = 0;

# Read syncro words (not in CRC)

read(PACK,$ch_cs,1);
read(PACK,$ch_cs,1);
read(PACK,$ch_cs,1);
read(PACK,$ch_cs,1);

# CRC calc

while(read(PACK,$ch_cs,1))
{
    $crc_str .= $ch_cs;
}

$checksum = Digest::CRC->new(width=>16, init=>0x0000, xorout=>0x0000, poly=>0x1021, refin=>1, refout=>1)->add($crc_str)->digest;
printf PACK (pack('v', $checksum));			# 16 bits

close(PACK);

printf LOG "0x%08X:\t0x%04X (Checksum)\n", $address_log, $checksum;

$total_size = $address_log - $type_start_addr + 2;			# 2  bytes for checksum

printf LOG "\nTOTAL SIZE OF PACKAGE = %i (0x%08X)\n", $total_size, $total_size;
close LOG;

printf "\nCHECKSUM:  0x%04X\n", $checksum;
printf "\nPACK SIZE: 0x%04X (%i)", $total_size, $total_size;


if($print_file)
{
    open (PACK, "<$type_name.pack") || die("cannot open package file...");
    binmode(PACK);
    $i = 0 + $type_start_addr;
    while(read(PACK,$ch_cs,1))
    {
	$c_low = ord($ch_cs);
	if(!read(PACK,$ch_cs,1))
	{
	    print "ERROR: odd number of bytes\n";
	}
	$c_high = ord($ch_cs);
	$c = $c_high*0x100 + $c_low;

	printf("0x%08X - 0x%04X\n", $i, $c);
	$i += 2;
    }
    close PACK;
}


#EOF
