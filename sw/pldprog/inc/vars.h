/*---------------------------------------------------------------------------------------------------------*\
  File:		vars.h

  Contents:	pldprog
  		this file declares/defines all the global structures and variables used
		by the program.

  History:

    20/04/05	pfr	Created
    20/10/10	psmz	Modified to include the Power Converter 120A 10V
\*---------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------*/

VARS_EXT INT16U	isrdummy;
VARS_EXT INT16U	u3_tx;


/*----- test -----*/

VARS_EXT INT32U  times_var[16];
VARS_EXT INT32U  free_run[16];

VARS_EXT INT16U  tick_20Hz;

/*--- type ---*/

struct position
{
    INT16U		status;				// status of the position (1:ok - 0:to be reprogrammed)
    INT16U		*verif_p;			// pointer to first data of verify file
    INT16U		*prog_p;			// pointer to first data of program file
};

struct chain_type
{
    INT16U		nb;				// number of device for this type
    INT16U		id_list[MAX_PLD_DEVICES];	// list of ID codes of devices in the chain
    INT16U		*id_file_p;			// points to ID file
    struct position	list[MAX_PLD_DEVICES];		// list of verify & program files address
};

VARS_EXT struct chain_type chain_type;

/*--- display ---*/

struct dspl
{
    INT8U		chars[4];			// chars of display
    INT16U		flash;				// Flash flag register: 1:flash, 0:don't flash
    INT16U		type;				// type given by coding wheel
    INT16U		mode;				// 1: jtag - 0: test, download, ...
    INT16U		pos;				// position of the device under operation
    INT16U		progress;			// progress of the operation (0-9)
    INT16U		status;				// status of the operation
    INT8U		op;				// operation
};

VARS_EXT struct dspl dspl;

VARS_EXT INT8U *dspl_str[NB_TYPES]
#ifdef FGC_GLOBALS
= {
"50",
"60",
"70",
"80",
//"120A",		// for Four Quadrant 120A 10V
//"600A",		// for Four Quadrant 600A 10V
//"51",		// for FGC51
}
#endif
;

/* Package file download */

struct pack_file_header
{
    INT32U		start_addr;			// Address where to load the code
    INT32U		size;				// size of file
    INT16U		type;				// type of FGC 0=FGC50, 1=FGC60, 2=FGC70, 3=FGC80, 4=FQ120A10V, 5=FQ600A10V
    INT16U		device_nb;			// nb of jtag devices
};

VARS_EXT INT32U type_map[NB_TYPES]
#ifdef FGC_GLOBALS
= {
FLASH_PACK0_32,
FLASH_PACK1_32,
FLASH_PACK2_32,
FLASH_PACK3_32,
//FLASH_PACK4_32,
//FLASH_PACK5_32,
}
#endif
;

VARS_EXT INT8U *dwnld_err[]
#ifdef FGC_GLOBALS
= {
"OK",
"Start address mismatch",
"No synchro words",
}
#endif
;

struct download
{
    INT16U		synchro_f;			// synchro_flag
    INT32U		ct;				// download counter
    INT32U		size;				// download file size
    INT32U		address;			// download address in flash
    INT16U		type;				// jtag chain type (fgc50, ...)
    INT16U		parity;				// byte parity (odd-even)
    INT16U		timeout;			// timeout (abort if no char received)
    INT16U		*ptr;				// pointer to flash location to be written
    INT16U		err;
    union val
    {
	INT32U		val_32;
	INT16U		val_16[2];
	INT8U		val_8[4];
    } val;
    INT16U		checksum;			// 16 bit rolling checksum
};

VARS_EXT struct download download;

/*--- General boot variables ---*/

struct dev_vars
{
    INT16U		state;				// Boot state
    INT16U		fip_id;				// Fip address
    INT16U		run_menu_f;			// Run menu flag
    INT16U		n_devs_to_update;		// Number of flash devices to update
    INT16U		n_devs_corrupted;		// Number of corrupted codes detected
    INT16U		abort_f;			// Abort operation flag (Ctrl-C pressed)
    INT16U		mem_sbe_f;			// Memory single bit error flag
    INT16U		enable_slow_wd;			// Enable triggering of slow watchdog
    INT16U		enable_fast_wd;			// Enable triggering of fast watchdog
    INT16U		leds_read;			// Read of LEDs register
    INT16U		slot5;				// Slot5 info register
    INT16U		slot5_type;			// Slot5 interface type
    INT16U		bus_type;			// Bus type
    INT16U		pld_version;			// PLD version read from slot 5 register
    INT8U		device;				// 'A', 'B' or 'R'
    INT8U		reg_r;				// Register access faults (read)
    INT8U		reg_w;				// Register access faults (write)
    INT8U		rst_src[20];			// Reset source string
};

VARS_EXT struct dev_vars 	dev;

/*--- Terminal variables ---*/

struct term_buf						// 256 byte circular buffer
{
    INT16U		n_ch;				// Number of characters in the buffer
    INT8U		in;				// Buffer input index
    INT8U		out;				// Buffer output index
    INT8U 		buf[TERM_BUF_SIZE];		// 256 character buffer
};

struct term
{
    FILE 		f;				// Pointer to buffered IO stream
    struct term_buf	tx;				// Transmission buffer
    struct term_buf	rx;				// Reception buffer
    INT16U		xoff_timeout;			// Xoff timeout flag (ms)
    INT16U		recv_args_f;			// Terminal input is active
    INT16U		recv_cmd_f;			// Start of comamnd ($) seen
    INT16U              edit_state;
    INT8U               linebuf[TERM_LINE_SIZE];
    INT16U              line_end;
    INT16U              line_idx;
};

VARS_EXT struct term	term;

/*----- Uart Tx Tasks Variables -----*/

struct tx_vars						// Tx task variables that are initialised to zero.
{
    INT8U *		buf;				// Pointer to transmission buffer
    INT16U		in;				// Buffer input index
    FILE		f;				// Buffered IO stream
};

VARS_EXT struct tx_vars	tx[N_UARTS];			// Tick Task variables structure

struct txi_vars						// Tx task variables initialised in uart.c
{
    INT16U *		uart_tx;			// Pointer to UART transmit register
    INT16U		(*out_func)(INT16U);		// Pointer to byte output function
    INT16U		len;				// Buffer length
};

extern struct txi_vars	txi[N_UARTS];			// Tick Task variables structure (init in uart.c)

/*--- Menu variables ---*/

struct menu_node
{
    char * 		name;
    char * 		args_help;
    INT8U		argc;
    INT8U		confirm_f;
    INT8U		fatal_f;
    INT8U		recurse_f;
    void		(*function)(INT16U argc, INT8U **argv);
    INT16U		n_children;
    struct menu_node **	children;
};

struct menu
{
    struct menu_node *	node;
    INT16U		depth;
    INT16U		up_lvls;
    INT8U		node_id[MENU_MAX_DEPTH];
    INT16U		argc;
    INT8U *		argv[CMD_MAX_ARGS];
    INT16U		response_buf_pos;
    INT8U		response_buf[TERM_LINE_SIZE];
    INT8U		error_buf[TERM_LINE_SIZE];
};

VARS_EXT struct menu	menu;

/*--- JTAG port ---*/

typedef union outPortUnion {
    INT8U value;
    struct opBitsStr {
        INT8U tdi:1;
        INT8U tck:1;
        INT8U tms:1;
        INT8U zero:1;
        INT8U one:1;
        INT8U bit5:1;
        INT8U bit6:1;
        INT8U bit7:1;
    } bits;
} outPortType;

typedef union inPortUnion {
    INT8U value;
    struct ipBitsStr {
        INT8U bit0:1;
        INT8U bit1:1;
        INT8U bit2:1;
        INT8U tdo:1;
        INT8U power:1;
        INT8U bit5:1;
        INT8U bit6:1;
        INT8U bit7:1;
    } bits;
} inPortType;

VARS_EXT  inPortType 	in_word;
VARS_EXT  outPortType 	out_word;

/* jtag vars */

struct jtag_vars
{
    INT16U	state;				// Sate in the JTAG TAP
    INT16U	tdi;				// value of TDI
    INT16U	tms;				// value of TMS
};

VARS_EXT	struct jtag_vars jtag;

struct xsv_vars
{
    INT16U		start_f;			// start flag
    INT16U		start_timeout;			// start timeout
    INT16U		stat_run;			// 1:running, 0:finished or waiting
    INT16U		mode;				// sig, infra, inter, ...
    INT32U		size;
    INT32U		bytes_read;
    INT8U		*data;				// pointer to the current data
    INT16U    		id_index;
    INT32U   		ids[MAX_PLD_DEVICES];
    INT16U		step_flag;				// step by step option
};

VARS_EXT struct	xsv_vars xsv;

enum {normal_mode, id_mode, sig_mode, infra_mode, inter_mode, checksum_mode};

VARS_EXT	INT16U	xsv_iDebugLevel;

VARS_EXT	INT16U	bscan_error_count;

struct bscan_err_vars
{
    INT16U	pos;		// position of the failing bit in bscan vect
    INT16U	rec_val;	// received value of the failing bit (0,1 or 2 for unknown2)
};

VARS_EXT  struct bscan_err_vars	bscan_err[MAX_FAIL_PINS];

VARS_EXT    INT16U	bscan_history_idx;
VARS_EXT    INT16U	bscan_history[MAX_FAIL_PINS];


/*----- Test/response arguments structure -----*/

struct args
{
    INT16U		argc;      			// Number of args found in string
    INT8U *		argv[MAX_ARGS];    	 	// Pointers to arguments in string
    INT8U		buf[TERM_LINE_SIZE];		// String buffer
};

/*--- LENVAL ---*/
#define LENVAL_MAX 600


typedef struct var_len_byte
{
	INT16S len;   /* number of chars in this value */
	INT8U val[LENVAL_MAX+1];  /* bytes of data */
} lenVal;

/*--- XSVF Type Declarations ---*/

typedef struct tagSXsvfInfo
{
    /* XSVF status information */
    INT8U   ucComplete;         /* 0 = running; 1 = complete */
    INT8U   ucCommand;          /* Current XSVF command byte */
    long     lCommandCount;      /* Number of commands processed */
    INT16U   iErrorCode;         /* An error code. 0 = no error. */

    /* TAP state/sequencing information */
    INT8U   ucTapState;         /* Current TAP state */
    INT8U   ucEndIR;            /* ENDIR TAP state (See SVF) */
    INT8U   ucEndDR;            /* ENDDR TAP state (See SVF) */

    /* RUNTEST information */
    INT8U   ucMaxRepeat;        /* Max repeat loops (for xc9500/xl) */
    long     lRunTestTime;       /* Pre-specified RUNTEST time (usec) */

    /* Shift Data Info and Buffers */
    long            lShiftLengthBits;   /* Len. current shift data in bits */
    INT16S           sShiftLengthBytes;  /* Len. current shift data in bytes */

    lenVal          lvTdi;              /* Current TDI shift data */
    lenVal          lvTdoExpected;      /* Expected TDO shift data */
    lenVal          lvTdoCaptured;      /* Captured TDO shift data */
    lenVal          lvTdoMask;          /* TDO mask: 0=dontcare; 1=compare */
    lenVal          lvXOR;              /* XOR of all the Captured vectors */
    lenVal          lvPrevCap;          /* Previous Captured vector */

    lenVal          lvAddressMask;      /* Address mask for XSDRINC */
    lenVal          lvDataMask;         /* Data mask for XSDRINC */
    lenVal          lvNextData;         /* Next data for XSDRINC */
} SXsvfInfo;

VARS_EXT SXsvfInfo	xsvfInfo;

/* Declare pointer to functions that perform XSVF commands */
typedef INT16U (*TXsvfDoCmdFuncPtr)( SXsvfInfo* );

/*--- former testing.h vars ---*/
typedef struct xsvfTestInformation
{
	FILE *resultsFile;          // File where the results from the execution of the xsvf for test are stored.
	FILE *xdiagnosisFile;       // XDiagnosis file.
	FILE *jtagErrorFile;        // JTAG error file.
	FILE *jtagErrorTemp;        // Temporary jtag error file. It is necessary to write the total number of errors
										 // in the JTAG error file before writing the errors. This temporary file stores the
										 // errors until they are accounted and ready to be written in the JTAG error file.


	char  xdiagnosisFileName[FILE_MAX_CHARS];   // Must be: design_name.XDA.
	char  jtagErrorFileName[FILE_MAX_CHARS];    // Must be: test_name.ERR (inter.err or infra.err for example).
	char  resultsFileName[FILE_MAX_CHARS];      // Must be: test_name.RST.
	char  GENfileName[FILE_MAX_CHARS];
	char  APLfileName[FILE_MAX_CHARS];

	char designName[MAX_CHARS];   // Must be known from the beginning
	char testName[MAX_CHARS];

	// Information about the errors
	INT16U scanFailBit;         /* Bit that is failing within a data shift.
										 Goes from 1 to chain[BSRegister] (Inter test) or chain[IDRegister] (Infra test).
										 (sum of registers in all devices in the chain */
	INT16U scanFailCell;        /* Order of the cell, within a device, that is failing in a data shift.
										 Goes from 0 to device[BSRegister] or device[IDRegister]. */
	INT16U deviceIndex;         /* Device in which a cell is failing (Index within the array of devices). */
	INT16U cellIndex;           /* Input cell which is failing (Index within the array of cells). */

	INT16U interVectorNumber;   /* Vector number in an interconnection test. */

	INT16U pass_fail;           /* Indicates if the test passed or not (PASS or FAIL). */

	INT16U totalErrors;         /* Total number of errors. */

	INT16U receivedBit;         /* Bit actually received in a cell. */

}xsvfTInfo;

VARS_EXT xsvfTInfo xsvfTestInfo;

typedef struct failPins
{
  INT16U deviceIndex;             // Index of device where the error occurred, within the array of devices.
  INT16U errorType;               // ERROR_STA0, ERROR_STA1 or ERROR_UNKNOWN.
										 // In an INFRA test these have the meaning:
										 //  -  ERROR_STA0: Bit expected 1 /received 0
										 //  -  ERROR_STA1: Bit expected 0 /received 1
  INT16U scanFailBit;             // Bit that is failing within a data shift (INTER test) or within a device (INFRA test).
  char pinDes[MAX_CHARS];      // Pin designator 1, 20, A23, F40, etc..
										 // Not used in INFRA test.
} failPns;

VARS_EXT failPns failingPins[MAX_FAIL_PINS + 1];  // Array of errors;

/*--- Individual global variables ---*/

VARS_EXT INT16U			timeout_ms;			// Millisecond timeout down counter

/*---------------------------------------------------------------------------------------------------------*\
  End of file: vars.h
\*---------------------------------------------------------------------------------------------------------*/


