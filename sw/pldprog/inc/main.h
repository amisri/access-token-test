/*---------------------------------------------------------------------------------------------------------*\
  File:		main.h

  Contents:	Mitsubishi-G64 controller card under test
  		this is the principle header file for a tester program

  History:

    20/04/05	mc	Created
    28/11/06    pfr	Modified for pld prog
\*---------------------------------------------------------------------------------------------------------*/

//#include <fgclib.h>


#ifdef FGC_GLOBALS
#define VARS_EXT
#else
#define VARS_EXT extern
#endif


/*----- Include all other header files -----*/

#include <stdarg.h>
#include <stdio.h>				// ANSI C header files
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <os_cpu.h>			// Include ucos header
//#include <fgc_consts.h>			// Global FGC constans
//#include <fgc_stat.h>			// FGC published/diag data structures

// z:\projects\inc\renesas\m32c83
#include <iodefines.h>		// M32C/87

#include <platforms\pldprog\memmap.h>
#include <consts.h>				// constants
#include <vars.h>				// variables
#include <funcs.h>				// functions

#include <version.h>				// version

#include "fpga.h"
#include "xilinx.h"

/*----- DAC special definitions to prevent errors -----*/

#ifdef __DAC__
#define	__BUILTIN_strcpy(s1,s2)
#define	__BUILTIN_memcpy(s1,s2,n)
#define	__BUILTIN_strcmp(s1,s2)		(0)
#define __BUILTIN_memset(s,c,n)
#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
