/*---------------------------------------------------------------------------------------------------------*\
  File:		funcs.h

  Contents:	Mitsubishi-G64 controller card under test
  		this file declares all the functions and macros contained in the program

  History:

    20/04/05	mc	Created
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

/*
This is already define in
z:\projects\lib\ucos\m32c87\inc\os_cpu.h

#define  OS_CRITICAL_METHOD    2

#define  OS_ENTER_CRITICAL()  	asm("PUSHC FLG"); asm("FCLR I")		// Disable interrupts
#define  OS_EXIT_CRITICAL()   	asm("POPC FLG")				// Enable  interrupts

*/
/*--- Macros ---*/

#define BUS_ERROR_CHECK_OFF	bus_error_check = 0
#define BUS_ERROR_CHECK_ON	bus_error_check = 1
#define BUS_ERROR_RESET		bus_error_flag  = 0

//#define SET_DEV_LEDS(mask)	SIM_PORTF_P=((~mask<<1)&0x1E)

#define	USLEEP(usecs)		{INT16U t0=ta1,tn=(INT16U)(usecs);while((ta1-t0)<tn);}
#define	MSLEEP(ms)		{timeout_ms=ms;while(timeout_ms);}

//#define CODING_WHEEL		(INT16U)(~p13 & 0x0F)
#define CODING_WHEEL		(INT16U)((!p13_0 << 2) | (!p13_1 << 1) | (!p13_2))

/*---- main.c functions -----*/

void	Init		(void);
void 	RunMenus	(void);
void	EnableIsr	(void);
INT16U	SendChUart0	(INT16U ch);
INT8U 	UartTermGetCh	(INT16U to_ms);
void    ReportBoot      (void);
void 	SetProcMode	(void);
void	PldProg		(void);

/*----- flash.c function declarations -----*/

void 	flashEnable		(void);
void 	flashProtect		(void);
INT16U 	flashWrite		(INT16U *address, INT16U data);
void 	flashRead		(INT16U *adress, INT16U* data);
INT16U 	flashErase		(INT16U *block_addr);
INT16U  flashFullStatCheck	(INT16U status);
INT16U 	flashNoAccess		(INT32U address);
INT16U 	flashEraseAll		(void);
INT16U  flashCrc		(INT32U address, INT32U nb_bytes, INT16U *file_crc);

/*--- display.c functions ---*/

void 	display			(INT8U position, INT8U ch);
void    displayProgress         (INT8U op, INT16U block);
void 	displayString		(INT8U *s);
void 	displayErase		(void);
void 	displayBlank		(void);
INT16U 	displaySelPos		(INT8U pos);
void	displayDwnld		(INT16U percent);
void 	displayJtagProgress	(void);

/*--- download.c ---*/

void	initDwnld		(void);

/*--- Assembler function in RAM ---*/

void	FlashCopiedFunc	(void);

/*----- term.c function declarations -----*/

#define	IFTERM			if(term.edit_state)

void	TermRx			(INT8U);
void	TermGetMenuOption	(struct menu_node *node);
void	TermRunFuncWithArgs	(struct menu_node *node);
void    TermEnter		(void);
void	TermInteractive		(void);
void	TermDirect		(void);
INT16U  TermLE0                 (INT8U ch);
INT16U  TermLE1                 (INT8U ch);
INT16U  TermLE2                 (INT8U ch);
INT16U  TermLE3                 (INT8U ch);
INT16U  TermLE4                 (INT8U ch);
INT16U  TermLE5                 (INT8U ch);
INT16U	TermInsertChar          (INT8U ch);
void    TermCursorLeft          (void);
void    TermCursorRight         (void);
void    TermStartOfLine		(void);
void    TermEndOfLine           (void);
void    TermDeleteLeft          (void);
void    TermDeleteRight         (void);
void    TermShiftRemains	(void);

/*---- isr.c functions -----*/
void	IsrDummy1	(void);
void	IsrTick20Hz	(void);
void    Uart0RxISR	(void);
void    Uart2RxISR	(void);
void    Uart3RxISR	(void);

/*--- mem.c functions ---*/

void 	MemTest		(INT32U start_addr, INT32U size);
void 	MemTest64	(INT32U start_addr, INT32U size);

/*--- UART ---*/

void 	UartRxTsk	(void *unused);
void 	UartSendCh	(INT16U uart, INT8U ch);
INT16U 	UartTermRxFlush	(void);
INT16U 	UartTxTerm	(INT16U ch);
INT8U 	UartTermGetCh	(INT16U to_ms);

/*--- menu.c ---*/

void	MenuMenu		(struct menu_node *node);
void	MenuDisplay		(struct menu_node *node);
INT16U  MenuFollow		(struct menu_node *node);
INT16U  MenuRunFunc		(struct menu_node *node, INT8U *args_buf);
INT16U	MenuConfirm		(struct menu_node *node);
void	MenuRspArg		(char *format, ...);
void	MenuRspError		(char *format, ...);
INT16U  MenuPrepareArgs         (INT8U *buff, INT16U argc_exp);
INT16U	MenuRspProgress		(INT16U allow_abort_f, INT16U level, INT16U total);
INT16U  MenuGetInt16U           (INT8U *cp, INT16U min, INT16U max, INT16U *result);
INT16U  MenuGetInt32U           (INT8U *cp, INT32U min, INT32U max, INT32U *result);
INT16U  MenuGetInt32S           (INT8U *cp, INT32S min, INT32S max, INT32S *result);

/*---- jtag.c functions -----*/
INT16U 	jtagRun		(INT16U* file);
INT16U 	jtagTap		(void);
void	jtagReset	(void);
void	jtagIdle	(void);
void	jtagSelect	(void);
void	jtagCapture	(void);
void	jtagShift	(void);
void	jtagExit	(void);
void	jtagPause	(void);
void	jtagUpdate	(void);

/*---- jtag_ports.c functions -----*/

INT8U 	portInp		(void);
INT8U 	readTDOBit	(void);			/* read the TDO bit and store it in val */
INT16U	jtag_run	(INT16U iArgc, char** ppzArgv);
void 	zero_outputs	(void);
void 	pulseClock	(void);			/* make clock go down->up->down*/
void 	readByte	(INT8U *data);		/* read the next byte of data from the xsvf file */
void 	portOutp	(INT8U value);
void 	setPort		(INT16S p, INT16S val);	/* set the port "p" (TCK, TMS, or TDI) to val (0 or 1) */
void 	waitTime	(long microsec);

/* jtag_seq.c functions */
INT16U 	xsvfExecute		(void);

/*--- XSVF Function Prototypes ---*/

INT16U  xsvfDoIllegalCmd	( SXsvfInfo* pXsvfInfo );   /* Illegal command function */
INT16U  xsvfDoXCOMPLETE		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXTDOMASK		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSIR		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSIR2		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSDR		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXRUNTEST		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXREPEAT		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSDRSIZE		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSDRTDO		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSETSDRMASKS	( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSDRINC		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSDRBCE		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSDRTDOBCE	( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXSTATE		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXENDXR		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXCOMMENT		( SXsvfInfo* pXsvfInfo );
INT16U  xsvfDoXWAIT		( SXsvfInfo* pXsvfInfo );
INT16U	xsvfDoXCHECKSUM		( SXsvfInfo* pXsvfInfo );

/*--- jtag_utils.c functions ---*/

INT32S	value			(lenVal* plvValue);
INT16S 	EqualLenVal		(lenVal *expected, lenVal *actual, lenVal *mask);
INT16S 	RetBit			(lenVal *lv, INT16U byte, INT16U bit);
void	initLenVal		(lenVal* ptr_lv, INT32S lValue);
void 	addVal			(lenVal *resVal, lenVal *val1, lenVal *val2);
void 	SetBit			(lenVal *lv, INT16U byte, INT16U bit, INT16S val);
void  	readVal			(lenVal *x, INT16S numBytes);

/*---------------------------------------------------------------------------------------------------------*\
  End of file: funcs.h
\*---------------------------------------------------------------------------------------------------------*/

