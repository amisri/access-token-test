/*---------------------------------------------------------------------------------------------------------*\
  File:		consts.h

  Contents:	Mitsubishi-G64 controller card under test
  		this file defines the global constants for the tester program.

  History:

    20/04/05	mc	Created
\*---------------------------------------------------------------------------------------------------------*/

#define  NB_TYPES		4				// nb of types

/*------------ TEST -------------*/

#define  SET_TP3		p9_3   = 0x01
#define  RESET_TP3		p9_3   = 0x00
#define  SET_TP4       		p9_4   = 0x01
#define  RESET_TP4		p9_4   = 0x00

/*----- Miscellaneous -----*/

#define  SYNCHRO_WORD		0xDEADBEEF

#define  MAX_PLD_DEVICES	50				// Max number of pld devices for 1 type

#define  LENVAL_MAX		600

#define  TERM_LINE_SIZE		80
#define  MAX_ARGS		10

/* STATUS */

#define  STAT_PASS		0
#define  STAT_FAIL		1
#define  STAT_ERR		0xFFFF

/*----- Miscellaneous constants -----*/

#define CMD_MAX_ARGS			(TERM_LINE_SIZE/2)	// Max command args
#define	MENU_MAX_DEPTH			8			// Length of menu.node_id;
#define ALLOW_ABORT			1			// Used by MenuRspProgress()
#define NO_ABORT			0			// Used by MenuRspProgress()

/* Download errors */

#define DWNLD_OK			0
#define DWNLD_AD_MISM                   1
#define DWNLD_NO_SYNC                   2

/* Download timeout */

#define  DWNLD_TIMEOUT			5

/*----- Boot response header constants -----*/

#define	RSP_ID_NAME			"$0,%s,%s"	// $0,{NODE_ID},{NODE_NAME}
#define	RSP_PROGRESS			"$1"		// $1,{% completed}
#define	RSP_DATA			"$2"		// $2,{DATA},{DATA},...
#define	RSP_OK				"$3"		// $3
#define	RSP_ERROR			"$4"		// $4,{ERROR MSG}

/*--- Menu ---*/

#define CMD_MAX_ARGS			(TERM_LINE_SIZE/2)	// Max command args
#define	MENU_MAX_DEPTH			8			// Length of menu.node_id;

/*----- Uart constants -----*/

#define N_UARTS				1
#define	TX_LEN_TERM			128
#define	TX_LEN_FGC			40
#define	TX_LEN_DIS			40
#define TX_LEN_GCPU			40
#define	TX_LEN_PC			256
#define	RX_MSGQ_SIZE			128
#define	TX_TERM				&tx[0].f
//#define	TX_FGC				&tx[1].f
//#define	TX_DIS				&tx[2].f
//#define	TX_PC				&tx[3].f
//#define	TX_GCPU				&tx[4].f

/*----- Terminal constants -----*/

#define TERM_RESET		"\r      \33c"
#define TERM_INIT		"\33[2J  \a \33[?7h \r"
#define TERM_BOLD		"\33[1m"
#define TERM_REVERSE		"\33[7m"
#define TERM_UL			"\33[4m"
#define TERM_NORMAL		"\33[0m"
#define TERM_CLR_LINE		"\33[2K"

#define TERM_LINE_SIZE  	        80		// Length of terminal line for line editor
#define	TERM_BUF_SIZE			256		// Terminal rx/tx buffer sizes
#define TERM_XOFF_TIMEOUT_MS		60000		// Xoff timeout in milliseconds (60s)
#define TERM_COL1			1		// First column
#define TERM_COL2			21		// Second column
#define TERM_COL3			41		// Third column
#define TERM_COL4			61		// Fourth column

/* Jtag port */

#define  JTAG_PORT		p9
#define  JTAG_PORT_TDI		p9_3
#define  JTAG_PORT_TMS		p9_4
#define  JTAG_PORT_TCK		p9_5

/*----- Serial Communications Interface related constants -----*/

#define  JTAG_RESET		0
#define  JTAG_IDLE		1
#define  JTAG_SELECT_DR		2
#define  JTAG_CAPTURE_DR	3
#define  JTAG_SHIFT_DR		4
#define  JTAG_EXIT1_DR		5
#define  JTAG_PAUSE_DR		6
#define  JTAG_EXIT2_DR		7
#define  JTAG_UPDATE_DR		8
#define  JTAG_SELECT_IR		9
#define  JTAG_CAPTURE_IR	10
#define  JTAG_SHIFT_IR		11
#define  JTAG_EXIT1_IR		12
#define  JTAG_PAUSE_IR		13
#define  JTAG_EXIT2_IR		14
#define  JTAG_UPDATE_IR		15

#define	 TDI_MASK		0x80	// a verifier
#define	 TCK_MASK		0x40
#define	 TMS_MASK		0x20

/*------------------ XSVF Command Bytes ---------------------*/

/* encodings of xsvf instructions */
#define  XCOMPLETE        0
#define  XTDOMASK         1
#define  XSIR             2
#define  XSDR             3
#define  XRUNTEST         4
/* Reserved              5 */
/* Reserved              6 */
#define  XREPEAT          7
#define  XSDRSIZE         8
#define  XSDRTDO          9
#define  XSETSDRMASKS     10
#define  XSDRINC          11
#define  XSDRB            12
#define  XSDRC            13
#define  XSDRE            14
#define  XSDRTDOB         15
#define  XSDRTDOC         16
#define  XSDRTDOE         17
#define  XSTATE           18         /* 4.00 */
#define  XENDIR           19         /* 4.04 */
#define  XENDDR           20         /* 4.04 */
#define  XSIR2            21         /* 4.10 */
#define  XCOMMENT         22         /* 4.14 */
#define  XWAIT            23         /* 5.00 */
#define  XCHECKSUM	  24	     /* PFR added */
/* Insert new commands here */
/* and add corresponding xsvfDoCmd function to xsvf_pfDoCmd below. */
#define  XLASTCMD         25         /* Last command marker */


/*-------------- XSVF Command Parameter Values ----------------------*/

#define  XSTATE_RESET     0          /* 4.00 parameter for XSTATE */
#define  XSTATE_RUNTEST   1          /* 4.00 parameter for XSTATE */

#define  XENDXR_RUNTEST   0          /* 4.04 parameter for XENDIR/DR */
#define  XENDXR_PAUSE     1          /* 4.04 parameter for XENDIR/DR */

/* TAP states */
#define  XTAPSTATE_RESET     0x00
#define  XTAPSTATE_RUNTEST   0x01    /* a.k.a. IDLE */
#define  XTAPSTATE_SELECTDR  0x02
#define  XTAPSTATE_CAPTUREDR 0x03
#define  XTAPSTATE_SHIFTDR   0x04
#define  XTAPSTATE_EXIT1DR   0x05
#define  XTAPSTATE_PAUSEDR   0x06
#define  XTAPSTATE_EXIT2DR   0x07
#define  XTAPSTATE_UPDATEDR  0x08
#define  XTAPSTATE_IRSTATES  0x09    /* All IR states begin here */
#define  XTAPSTATE_SELECTIR  0x09
#define  XTAPSTATE_CAPTUREIR 0x0A
#define  XTAPSTATE_SHIFTIR   0x0B
#define  XTAPSTATE_EXIT1IR   0x0C
#define  XTAPSTATE_PAUSEIR   0x0D
#define  XTAPSTATE_EXIT2IR   0x0E
#define  XTAPSTATE_UPDATEIR  0x0F

/*--- Ports consts ---*/

#define  POWER_CHECK		1
#define  TDO			p9_6
#define  TCK			(INT16S) 0
#define  TMS			(INT16S) 1
#define  TDI			(INT16S) 2


/*--- former testing.h consts ---*/

#define  XSV_FILE_MAX_NB	4
#define  XSV_FILE_NAME_SIZE	8
#define  BSCAN_ERR_PRINT	20
#define  XSV_CS_LOOP_SIZE	4	// Number of shift in checksum loop

#define MAX_BSDEVICES   25          // Maximum number of boundary scan (BS) devices in chain.
#define MAX_BSDL_TYPES  8           // Maximum number of BS types (BSDLs)in a JTAG chain.
#define MAX_CELLS       300         // Maximum number of testable cells (input type cells) in each BS device type.
#define MAX_FAIL_PINS   50          // Maximum number of different pins failing in a test (-1).

#define MAX_CHARS       20          // Maximum number of chars in a string (bsdl entity names, etc).
#define FILE_MAX_CHARS  32          // Maximum number of chars in a filename string (filenames, bsdl entity names, etc).

#define XSVF_XILINX_TYPE 0          // Type of xsvf file.

#define BSDL_ENTITY_UNKNOWN -1

#define WRITE_START              0  // Constants used in functions write?????File
#define WRITE_ERROR_JTAG         1
#define WRITE_FINISH             2
#define WRITE_ERROR_RESULT_INFRA 3
#define WRITE_ERROR_RESULT_INTER 4

#define FAIL 0                      // test status: PASS or FAIL.
#define PASS 1

#define ENDOF_ALL_DEVICES -1        // Mark, respectively, the end of devices, cells and errors in each array.
#define ENDOF_ALL_CELLS   -1
#define ENDOF_ALL_ERRORS  -1

// Do NOT change the following constants:
#define ERROR_STA0        0         // Stuck-at-0 error (allways expected 1/received 0).
#define ERROR_STA1        1         // Stuck-at-1 error (allways expected 0/received 1).
#define ERROR_UNKNOWN     2         // Unknown error (both expected 1/received 0 and expected 0/received 1).


/* was in micro.h */

#ifndef XSVF_MICRO_H
#define XSVF_MICRO_H

#ifndef DEBUG_MODE
#define DEBUG_MODE 1
#endif

/* Legacy error codes for xsvfExecute from original XSVF player v2.0 */
#define XSVF_LEGACY_SUCCESS 1
#define XSVF_LEGACY_ERROR   0

/* 4.04 [NEW] Error codes for xsvfExecute. */
/* Must #define XSVF_SUPPORT_ERRORCODES in jtag_seq.c to get these codes */
#define XSVF_ERROR_NONE         	0
#define XSVF_ERROR_UNKNOWN      	1
#define XSVF_ERROR_TDOMISMATCH  	2
#define XSVF_ERROR_MAXRETRIES   	3   /* TDO mismatch after max retries */
#define XSVF_ERROR_ILLEGALCMD   	4
#define XSVF_ERROR_ILLEGALSTATE 	5
#define XSVF_ERROR_DATAOVERFLOW 	6   /* Data > lenVal LENVAL_MAX buffer size*/
#define XSVF_ERROR_ILLEGALJUMP_CS 	7	/* should not jump to xsvfDoXCHECKSUM */
/* Insert new errors here */
#define XSVF_NO_POWER	         	8
#define XSVF_ERROR_LAST         	9
#define XSVF_NO_FILE	         	10


#endif  /* XSVF_MICRO_H */

/* FLASH constants */

#define  FLASH_PROTECT_N		p12_2
#define  FLASH_ENABLE_N                 p12_4
#define  FLASH_START			0x010000
#define  FLASH_END			0xEFFFFE
#define  FLASH_TIMEOUT_W		175             // 175 us
#define  FLASH_TIMEOUT_E		4000            // 4s
/* modes */
#define  FLASH_READ_ARRAY		0xA955		// 0x00FF
#define  FLASH_READ_STAT		0xA100		// 0x0070
#define  FLASH_READ_DEV_INFO		0x0900		// 0x0090
#define  FLASH_CLEAR_STAT		0x2100		// 0x0050
#define  FLASH_PROG_ECR			0xA000		// 0x0060
#define  FLASH_WRITE_W			0x2000		// 0x0040
#define  FLASH_WRITE_B			0x0100		// 0x0010
#define  FLASH_BLK_ERASE              	0x8000		// 0x0020
#define  FLASH_ERASE_CFM              	0x2900		// 0x00D0

/* Display constants */

#define  DISP_WR_N			p9_0
#define  DISP_BUS			p14 		// p14 has only 7 signals, like the display

/* XSV start timeout */

#define  STATR_TIMEOUT			2

/*---------------------------------------------------------------------------------------------------------*\
  End of file: consts.h
\*---------------------------------------------------------------------------------------------------------*/

