Mise en route des programmateurs FGC2 JTAG Standalone

1. Programmer le chip USB FTDI (port DEV sur COM 2, port TERM sur port 16)

2. Programmer le C87 (pldprog.mot)

3. ************** TESTER COMPLETEMENT LA FLASH (Menu 15) *********************

4. EFFACER LA FLASH AVANT DE DOWNLOADER LES CODES

5. Envoyer CTRL-Z au terminal

6. Telecharger les codes (Terminal com 16, baudrate=38400)
