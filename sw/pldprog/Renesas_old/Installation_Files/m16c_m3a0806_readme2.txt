M16C Flash Starter (M3A-0806)
Release Notes No.13

RENESAS SOLUTIONS CORP
April.01.2005

* Last time, a difference from the version
   A kind of M16C/62P group (512KB version) was added.
   A kind of M32C/84,85,86 group was added.
   The C_E.P.R command was added.

1. Overview
   M16C Flash Starter enables you to rewrite your MCU with internal
   flash memory from com connector of a personal computer.
   For detailed information on the software, see the user's manual that is
   included with the product.
   These Release Notes contains notes on usage and explain how to install and
   uninstall the product.
   Before using the product, read these notes carefully.

* Notes on Usage
   M16C Flash Starter is supported only by the following MCUs.

   M16C Family
    /62A
      M30624FGAFP,M30624FGAGP,M30625FGMGP,
      M30620FCMFP,M30620FCMGP,M30621FCMGP
      M30620FCTFP,M30620FCUFP,M30621FCTFP,M30621FCUFP,
      M30624FGTFP,M30624FGUFP,M30625FGTFP,M30625FGUFP
    /62M
      M30624FGMFP,M30624FGMGP,M30625FGMGP,
      M30620FCMFP,M30620FCMGP,M30621FCMGP
    /62N
      M3062GF8NFP,M3062GF8NGP,M30620FCNFP,M30620FCNGP,
      M30621FCNGP,M30624FGNFP,M30624FGNGP,M30625FGNGP
    /62P
      M30626FHPFP,M30626FHPGP,M30627FHPGP,M30624FGPFP,
      M30624FGPGP,M30625FGPGP,M30626FJPFP,M30626FJPGP,
      M30627FJPGP
    /20
      M30201F6SP,M30201F6FP,M30201F6TFP,M30220FCGP
      M30220FCRP
    /80
      M30800FCFP,M30800FCGP,M30802FCGP,M30803FGFP,
      M30803FGGP,M30805FGGP
      M30800SFP-BL,M30800SGP-BL,M30802SGP-BL,M30803SFP-BL,
      M30803SGP-BL,M30805SGP-BL
    /6N
      M306NAFGTFP,M306NBFGTFP,M306N4FCTFP,M306N4FGTFP
      M306N5FCTFP
    /6S
      M306S0FAGP
    /10
      M30100F3FP,M30100F3TFP,M30102F3FP,M30102F3TFP
    /1N
      M301N2F8TFP
    /26
      M30262F3GP,M30262F4GP,M30262F6GP,M30262F8GP
    /26A
      M30260F3AGP,M30260F4AGP,M30260F6AGP,M30260F8AGP,
      M30263F3AGP,M30263F4AGP,M30263F6AGP,M30263F8AGP
      *Used to T-Version
    /28
      M30280F6HP,M30280F8HP,M30280FAHP,
      M30281F6HP,M30281F8HP,M30281FAHP
      *Used to T-Version and V-Version
    /29
      M30290F8HP,M30290FAHP,M30290FCHP,
      M30291F8HP,M30291FAHP,M30291FCHP
      *Used to T-Version and V-Version
   M32C series
    /83
      M30833FJFP,M30833FJGP,M30835FJGP
    /84
      M30843FHFP,M30843FHGP,M30843FJFP,M30843FJGP,
      M30843FWFP,M30843FWGP,M30845FHGP,M30845FJGP,
      M30845FWGP
    /85
      M30853FHFP,M30853FHGP,M30853FJFP,M30853FJGP,
      M30853FWFP,M30853FWGP,M30855FHGP,M30855FJGP,
      M30855FWGP
    /86
      M30865FJGP

   38000/740 series
      M37516F8HP
      M37542F8SP,M37542F8FP,M37542F8GP
      M37542F8TFP,M37542F8TGP
      M37542F8VFP,M37542F8VGP
      M37542F8HP(For development. Only ES product. MP:no plan)
      M38039FFHSP,M38039FFHFP,M38039FFHHP,M38039FFHKP
      M38049FFHSP,M38049FFHFP,M38049FFHHP,M38049FFHKP
      M38507F8SP,M38507F8FP
      M38507F8ASP,M38507F8AFP
      M38517F8SP,M38517F8FP 
      M38C29FFAFP,M38C29FFAHP

   R8C/Tiny series
    /10
      R5F21102FP,R5F21103FP,R5F21104FP
    /11
      R5F21112FP,R5F21113FP,R5F21114FP
    /12
      R5F21122FP,R5F21123FP,R5F21124FP
    /13
      R5F21132FP,R5F21133FP,R5F21134FP
    /14
      R5F21142SP,R5F21143SP,R5F21144SP
    /15
      R5F21152SP,R5F21153SP,R5F21154SP
    /16
      R5F21162SP,R5F21163SP,R5F21164SP
    /17
      R5F21172SP,R5F21173SP,R5F21174SP

( Warning )
  Be careful with MCU except that it is mentioned in the 
  user manual because it can't be used.
  As for the correspondence of other MCUs, it will make contact
  one after another on a support homepage to mention later.

  And can't support function of boot-loader for M16C/80.

2. CD-ROM Content

   The CD-ROM included in the package contains the following files.

      \
      |-Flashsta.exe:M16C  Flash  Starter main
      |-FlaSta10.exe:M16C  Flash  Starter supplement 1
      |-firm.mot    :M16C  Flash  Starter supplement 2
      |-HEXTOS2.exe :HEX format
      |-Manual_j.pdf:M16C Flash Starter User's Manual (Japanese)
      |-HEXTOS2j.pdf:HEX format Manual (Japanese)
      |-Readme_j.TXT:Release Notes (Japanese)
      |-Readmej2.TXT:Release Notes2 (Japanese)
      |-Manual_e.pdf:M16C Flash Starter User's Manual (English)
      |-HEXTOS2e.pdf:HEX format Manual (English)
      |-Readme_e.TXT:Release Notes (English)
      |-Readmee2.TXT:Release Notes2 (English)
      |-smp1662.id  :M16C/62 ID file
      |-smp1680.id  :M16C/80 ID file
      |-smp3800.id  :38000 ID file
      |-smpR8C.id   :R8C ID file

3. Installation

   Copy Flashsta.exe (M16C Flash Starter main),FlaSta10.exe(Starter supplement 1)
   and firm.mot(Starter supplement 2) from the included CD-ROM into a folder of
   your choice. If necessary, copy the user's manual (Manual_E.pdf), too.
   If you do not have a PDF reader, select "Acro30.exe" from [Reader] - [English]
   folders, click on [Start] - [Run], and run the program.

4. Startup

   Click on [Start] - [Run]. Then, select and run Flashsta.exe which you copied
   in "3. Installation".
   Perform the operations as instructed by screen prompts.
   (For details on operation, see the user's manual.)

5. Uninstallation

   Delete Flashsta.exe,FlaSta10.exe and firm.mot which you copied in "3. Installation".

6. Connector Specifications of include serial cable

   Pin assignments of serial cable ( MF_Ten_Nine cable ) is below.
   When use this cable, please connect pin assigments.

  (Pin assignments)    Pin No.      Signal of user
     2    1 <             1              Vcc
     4    3               2               -
     6    5               3               -
     8    7               4               RxD1
     10   9               5               -
                          6               -
                          7               GND
                          8               -
                          9               -
                          10              TxD1

7. Product Information

   The latest information on this product will be announced in the below
   Web site. Support for new MCUs will be included. Have a look.

   (Web site below)
   http://www.renesas.com/en/m16c

----------------------------------------------------------------<End of File>-
