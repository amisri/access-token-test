M16C Flash Starter (M3A-0806)  Ver.2.0.0.46

RENESAS SOLUTIONS CORP
April.1.2005

The files included in an archive are the following files.
Please use the target file for every microcomputer to be used.

Files for M16C/62,20 series user are as follows

Flashsta.exe  : M16C Flash Starter
Smp1662.id    : Sample ID file for M16C/62,20 series

Files for M16C/80 series user are as follows

Flashsta.exe  : M16C Flash Starter
Smp1680.id    : Sample ID file for M16C/80 series

Files for 38000series user are as follows

Flashsta.exe  : M16C Flash Starter
Smp3800.id    : Sample ID file for 38000series
hextos2.exe   : HEXTOS2 execution file
                Convert the Intel HEX format machine language file i
                (extension: .hex) into the Motorola S format machine 
                language file (extension: .s2).

Files for R8C/Tiny series user are as follows

Flashsta.exe  : M16C Flash Starter
SmpR8C.id    : Sample ID file for R8C/Tiny series

Files for M16C/10 series user are as follows

Flashsta.exe  : M16C Flash Starter
FlaSta10.exe  : M16C/10 Only Flash Starter
firm.mot      : M16C/10 mot file
Smp1662.id    : Sample ID file for M16C/62,20 series


ID file

 ID file name is the same as the Motorola S format file name written
 in the flash built-in MCU. An extension is ".id."

 ID file will be read when flash starter choosing a file.
 Serial ID to read is the last 2 digits of the 2nd line to the 8th line
 of ID file. Please set up ID checked by a customer.
