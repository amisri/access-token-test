/*---------------------------------------------------------------------------------------------------------*\
  File:         menuFlash.c

  Purpose:      flash test functions

  Author:       philippe.fraboulet@cern.ch

  Notes:

  History:

    16/04/07    pfr	Created
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashWrite(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests flash writing.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U  address;
    INT16U  data;
    INT16U  data_r;

    dspl.mode	       = 0;		// download display mode

    if(!(MenuGetInt32U(argv[0], FLASH_START, FLASH_END, &address) ||	// address
    	 MenuGetInt16U(argv[1], 0x0, 0xFFFF, &data)) )			// data

    {
        /* If address accessible */
        if(flashNoAccess((INT32U)address))
        {
            MenuRspError("Flash Block address 0x%06lX not accessible by C87", address);
            return;
        }

        flashEnable();                          // Unprotect flash

	flashWrite((INT16U*)address, data);
	flashRead((INT16U*)address, &data_r);

	MenuRspArg("AD 0x%06lX - D 0x%04X - DR 0x%04X", address & 0xFFFFFF, data, data_r);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashRead(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests flash writing.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U  address;
    INT16U  i,nb;
    INT16U  data_r;

    dspl.mode	       = 0;		// download display mode

    if(!(MenuGetInt32U(argv[0], FLASH_START, FLASH_END, &address) ||
    	 MenuGetInt16U(argv[1], 0, 20, &nb)) )
    {
        flashEnable();                          // Unprotect flash

	for(i=0 ; i<nb ; i++)
	{
	    flashRead((INT16U*)address, &data_r);
	    fprintf(TX_TERM, "AD 0x%06lX - D 0x%04X", address & 0xFFFFFF, data_r);
	    fprintf(TX_TERM, "\r\n");
            address += 2;
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashErase(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests flash erase.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U  address;

    dspl.mode	       = 0;		// download display mode

    if(!MenuGetInt32U(argv[0], FLASH_START, FLASH_END, &address))		// address
    {
        /* If address accessible */
        if(flashNoAccess(address))
        {
            MenuRspError("Flash Block address 0x%06lX not accessible by C87", address);
            return;
        }

        flashEnable();                          // Unprotect flash

	flashErase((INT16U*)address);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashTest(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests the flash. Writes and readback all the memory locations
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U  address;
    INT16U  data, data_r;
    INT16U  ch;

    dspl.mode	       = 0;		// download display mode

    /* Enable Flash */
    flashEnable();

    /* Erase flash */
    if(flashEraseAll())				// Erase fails
    {
	return;
    }

    /* Write all locations */
    data = 0x00;
    for(address = FLASH_START ; address < FLASH_END; address += 2)
    {
        if(!flashNoAccess(address))             // if address accessible
        {
            flashWrite((INT16U*)address, data++);
        }

        /* Display */
        if(!(address & 0xFFFF))
        {
            displayProgress('W',(INT16U)(address>>16));
            MenuRspProgress(1, (INT16U)(address>>16), 0x200);
        }

    }

    /* read back locations */
    data = 0x00;
    flashRead((INT16U*)FLASH_START, &data_r);             // first read to initialise flash mode
    for(address = FLASH_START ; address < FLASH_END; address += 2)
    {
        if(!flashNoAccess(address))             // if address accessible
        {
            if(*(INT16U*)address != data++)
            {
                MenuRspError("Readback error: AD: 0x%06lX D: 0x%04X DR: 0x%04X", address, data--, *(INT16U*)address);
                return;
            }
        }

        /* Display */
        if(!(address & 0xFFFF))
        {
            displayProgress('R',(INT16U)(address>>16));
            MenuRspProgress(1, 0x100 + (INT16U)(address>>16), 0x200);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashEraseType(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases one type.
  Each type shall begin at an even page because blocks are 128kb long.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  type;
    INT32U  block_start,block_stop;

    if(MenuGetInt16U(argv[0], 0, NB_TYPES-1, &type))		// Get type
    {
	return;
    }

    block_start = type_map[type];
    block_stop  = ((type == NB_TYPES-1) ? FLASH_END : type_map[type+1]);

    /* Display */

    displayString("    ");		// clear display
    dspl.mode = 0;			// download display mode
    fprintf(TX_TERM,RSP_DATA ", Erasing type FGC%s...\n",dspl_str[type]);

    /* Unprotect flash */

    flashEnable();

    /* Erase all block between beginning of type and next type (or end of flash) */

    while(block_start<block_stop)
    {
        /* Erase block */

	if(flashErase((INT16U*)block_start))
	{
            return;		// Exit id flash erasing failled
	}

        /* Display progress */

	displayProgress('E',block_start>>16);

        /* prepare for next block */

        block_start += 0x20000;			// One blok is 128kb
    }
    fprintf(TX_TERM,RSP_DATA ", Type FGC%s erased\n",dspl_str[type]);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashEraseAll(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases all the flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    dspl.mode	       = 0;		// download display mode

    flashEraseAll();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashChecksum(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the checksums.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  crc, crc_exp ,i;
    struct pack_file_header *pack_p;

    *(INT16U*)type_map[0] = FLASH_READ_ARRAY;	// go in read array mode (write a 16b word)

    fprintf(TX_TERM,RSP_DATA " | i |  Type |  CRC   |  file  |\n");

    /* For all types */

    for(i=0;i<NB_TYPES;i++)
    {
	/* Check type and address */

	pack_p = (struct pack_file_header*)(type_map[i]);
	if((type_map[i] != pack_p->start_addr) || (i != pack_p->type))
	{
	    fprintf(TX_TERM,RSP_DATA " | %i | FGC%s |   Not avail.    |\n",i,dspl_str[i]);
	}
	else
	{
	    crc = flashCrc(pack_p->start_addr, pack_p->size, &crc_exp);
	    fprintf(TX_TERM,RSP_DATA " | %i | FGC%s | 0x%04X | 0x%04X |\n",i,dspl_str[i],crc,crc_exp);
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFlashDownload(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepare flash for download.
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(TX_TERM,"Not ready for type 51\n");
    fprintf(TX_TERM,"Is flash erase? If yes, CTRL-Z and send file\n");
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuFlash.c
\*---------------------------------------------------------------------------------------------------------*/
