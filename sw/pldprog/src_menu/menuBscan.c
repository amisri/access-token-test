/*---------------------------------------------------------------------------------------------------------*\
  File:         menuBscan.c

  Purpose:      Bscan test functions

  Author:       philippe.fraboulet@cern.ch

  Notes:

  History:

    22/01/07    pfr	Created
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

void MenuBscanSig		(INT16U argc, INT8U **argv)	{MenuRspError("Not implemented");}
void MenuBscanInfra		(INT16U argc, INT8U **argv)	{MenuRspError("Not implemented");}
void MenuBscanInter		(INT16U argc, INT8U **argv)	{MenuRspError("Not implemented");}

/*---------------------------------------------------------------------------------------------------------*/
void MenuBscanRun(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
 This function runs the pldprog routine.
\*---------------------------------------------------------------------------------------------------------*/
{
    PldProg();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuBscan.c
\*---------------------------------------------------------------------------------------------------------*/
