/*---------------------------------------------------------------------------------------------------------*\
 File:		jtag_utils.c

 Purpose:	FGC3 boot

 Abstract:	This file contains routines for using
		the lenVal data structure.

 Author:	Philippe Fraboulet

 History:
    12/05/06	pfr	Adapted from xilinx player lenval.c file
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
INT32S	value(lenVal* plvValue)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     value
  Description:  Extract the long value from the lenval array.
  Parameters:   plvValue    - ptr to lenval.
  Returns:      long        - the extracted value.
\*---------------------------------------------------------------------------------------------------------*/
{
    long    lValue;         /* result to hold the accumulated result */
    INT16S   sIndex;
    lValue  = 0;
    for ( sIndex = 0; sIndex < plvValue->len ; ++sIndex )
    {
	    lValue <<= 8;                       /* shift the accumulated result */
	    lValue |= plvValue->val[ sIndex];   /* get the last byte first */
    }
    return( lValue );
}
/*---------------------------------------------------------------------------------------------------------*/
void initLenVal(lenVal* ptr_lv, INT32S lValue)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     initLenVal
  Description:  Initialize the lenval array with the given value.
                Assumes lValue is less than 256.
  Parameters:   ptr_lv         - ptr to lenval.
                lValue      - the value to set.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr_lv->len    = 1;
    ptr_lv->val[0] = (INT16U)lValue;
}
/*---------------------------------------------------------------------------------------------------------*/
INT16S EqualLenVal(lenVal* plvTdoExpected, lenVal* plvTdoCaptured, lenVal* plvTdoMask)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     EqualLenVal
  Description:  Compare two lenval arrays with an optional mask.
  Parameters:   plvTdoExpected  - ptr to lenval #1.
                plvTdoCaptured  - ptr to lenval #2.
                plvTdoMask      - optional ptr to mask (=0 if no mask).
  Returns:      INT16S   - 0 = mismatch; 1 = equal.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16S           sEqual;
    INT16S           sIndex;
    INT16U   ucByteVal1;
    INT16U   ucByteVal2;
    INT16U   ucByteMask;

    sEqual  = 1;
    sIndex  = plvTdoExpected->len;

    while ( sEqual && sIndex-- )
    {
	ucByteVal1  = plvTdoExpected->val[ sIndex ];
	ucByteVal2  = plvTdoCaptured->val[ sIndex ];
	if ( plvTdoMask )
	{
	    ucByteMask   = plvTdoMask->val[ sIndex ];
	    ucByteVal1  &= ucByteMask;
	    ucByteVal2  &= ucByteMask;
	}
	if ( ucByteVal1 != ucByteVal2 )
	{
	    sEqual  = 0;
	}
    }

    return( sEqual );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16S RetBit(lenVal* ptr_lv, INT16U iByte, INT16U iBit)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     RetBit
  Description:  return the (byte, bit) of lv (reading from left to right).
  Parameters:   ptr_lv     - ptr to lenval.
                iByte   - the byte to get the bit from.
                iBit    - the bit number (0=msb)
  Returns:      INT16S   - the bit value.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* assert( ( iByte >= 0 ) && ( iByte < ptr_lv->len ) ); */
    /* assert( ( iBit >= 0 ) && ( iBit < 8 ) ); */
    return( (INT16S)( ( ptr_lv->val[ iByte ] >> ( 7 - iBit ) ) & 0x1 ) );
}
/*---------------------------------------------------------------------------------------------------------*/
void SetBit(lenVal* ptr_lv, INT16U iByte, INT16U iBit, INT16S sVal)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     SetBit
  Description:  set the (byte, bit) of lv equal to val
  Example:      SetBit("00000000",byte, 1) equals "01000000".
  Parameters:   ptr_lv     - ptr to lenval.
                iByte   - the byte to get the bit from.
                iBit    - the bit number (0=msb).
                sVal    - the bitto set.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U   ucByteVal;
    INT16U   ucBitMask;

    ucBitMask   = (INT16U)(1 << ( 7 - iBit ));
    ucByteVal   = (INT16U)(ptr_lv->val[ iByte ] & (~ucBitMask));

    if ( sVal )
    {
        ucByteVal   |= ucBitMask;
    }
    ptr_lv->val[ iByte ]   = ucByteVal;
}
/*---------------------------------------------------------------------------------------------------------*/
void addVal(lenVal* plvResVal, lenVal* plvVal1, lenVal* plvVal2)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     AddVal
  Description:  add val1 to val2 and store in resVal;
                assumes val1 and val2  are of equal length.
  Parameters:   plvResVal   - ptr to result.
                plvVal1     - ptr of addendum.
                plvVal2     - ptr of addendum.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U   ucCarry;
    INT16U  usSum;
    INT16U  usVal1;
    INT16U  usVal2;
    INT16S  sIndex;

    plvResVal->len  = plvVal1->len;         /* set up length of result */

    /* start at least significant bit and add bytes    */
    ucCarry = 0;
    sIndex  = plvVal1->len;
    while ( sIndex-- )
    {
	usVal1  = plvVal1->val[ sIndex ];   /* i'th byte of val1 */
	usVal2  = plvVal2->val[ sIndex ];   /* i'th byte of val2 */

	/* add the two bytes plus carry from previous addition */
	usSum   = (INT16U)( usVal1 + usVal2 + ucCarry );

	/* set up carry for next byte */
	ucCarry = (INT16U)( ( usSum > 255 ) ? 1 : 0 );

	/* set the i'th byte of the result */
	plvResVal->val[ sIndex ]    = (INT16U)usSum;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void readVal(lenVal* ptr_lv, INT16S sNumBytes)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     readVal
  Description:  read from XSVF numBytes bytes of data into x.
  Parameters:   ptr_lv         - ptr to lenval in which to put the bytes read.
                sNumBytes   - the number of bytes to read.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U*  pucVal;

    ptr_lv->len    = sNumBytes;        /* set the length of the lenVal        */
    for ( pucVal = ptr_lv->val; sNumBytes; --sNumBytes, ++pucVal )
    {
        /* read a byte of data into the lenVal */
		readByte( pucVal );
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: jtag_utils.c
\*---------------------------------------------------------------------------------------------------------*/

