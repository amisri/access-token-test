/*---------------------------------------------------------------------------------------------------------*\
  File:		uart.c

  Purpose:	FGC3 boot - UART Interface Functions

  Author:	Quentin King

  Notes:	This file contains the functions for communications via the C83 UARTS.  The C83 has
		five dedicated UARTs (0-4) and two intelligent I/O groups (0 and 1) that can be configured
		as UARTs.  UART1 is assigned to the monitor program for debugging or the boot for flashing
		code.  Five UARTS are required for the tester operation, so one intelligent I/O group (0)
		is used as a sixth UART and is considered to be UART1 for the purposes of the tester
		library.  This allows a contiguous array of five structures to map all the active serial
		connections:

			STREAM	   TX		   RX		Speed	Notes
			--------------------------------------------------------------------------------
			  0	Terminal	Keyboard	 9600	RS232 - UART 0
			  1	FGC/Flasher cmd	FGC/Flasher rsp	 9600	RS232 - Intelligent I/O group 0
			  2	Display		Barcode reader	19200   Display: TTL, Barcode: RS232 (UART 2)
			  3	PC cmd		PC rsp          19200	RS232 - to PC COM1 (UART 3)
			  4	GCPU cmd 	GCPU rsp	 9600	RS232 - Intelligent I/O group 1
			  5	Dallas ID bus	Dallas ID bus	 9600	TTL (no TX task - direct writes only - UART 4)

		All UARTS are configured to use 8-bits, 1 stop bit, no parity.

		There is a transmission task for each STREAM 0-4.  They wait on a counting semaphore that tells
		how many characters are waiting to be sent in a circular transmission buffer.
  		To send each character it must take a synchronising semaphore that indicates that
		the UART is ready to send.  The TxISR functions post these RTS semaphores each time
		a transmission completes.  Each TX task also has a buffer-free semaphore that can
		block the task that is attempting to write to the UART if the buffer is full.

		STREAM 5 is special because the ID task must write directly to the UART and there is no
		TX interrupt.

		There is a single reception task for all STREAMs.  It waits on a message queue where each
		entry in the queue is one long word in which the high word is the STREAM number (0-5) and the
		low word is the character received on that STREAM.  There is one ISR per STREAM that is
		responsible for queuing the characters as they are received, setting the high word to
		inform the RX task which UART it was from.  The task calls a handler function for the
		appropriate STREAM to process the character received.
  History:

    04/08/03	qak	Created
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/* Initialised Global Variables */
				//  UART Tx reg,  Stream Tx callback, Tx buffer length
struct txi_vars	txi[N_UARTS]= {	{	&u0tb,		UartTxTerm,	TX_LEN_TERM	}};


/*---------------------------------------------------------------------------------------------------------*/
void UartRxTsk(void *unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the UART reception task.  It uses a single message queue to receive characters from the
  ISRs for all five STREAMS.  Each message is one lomg word, where the bottom word is the received character
  plus serial status, and the top word is the STREAM number (0-4).  The task calls the reception function for
  the appropriate UART to handle the received character. This function never exits, so "local" variables are
  static to save stack space.
\*---------------------------------------------------------------------------------------------------------*/
{
    static INT32U	data;
    static INT16U	uart;
    static INT16U	ch;
    static void 	(*uart_func[])(INT8U) =		// UART reception functions
    {
	TermRx,						// STREAM 0 - Terminal keyboard
    };

    /*--- Task loop ---*/

    for(;;)
    {
	uart = data >> 16;				// Extract UART number from high word
	ch   = (data & 0xFF);				// Extract 8-bit character from low word

	uart_func[uart](ch);				// Call handler function for this UART
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void UartSendCh(INT16U uart, INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to submit a character to a transmission queue.  If the queue is full, the function
  will block forever on a semaphore until the queue has space.
\*---------------------------------------------------------------------------------------------------------*/
{
    *txi[uart].uart_tx = ch;			// Write character to UART Tx register
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SendChUart0(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function will execute the commande waiting in the buffer transmit via UART 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    while(!ti_u0c1);		// Wait for transmit buffer to be empty

    u0tb  = ch;			// Send character

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U UartTxTerm(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the terminal output stream.  It only transmits characters when the
  terminal has been activated.  It will add a CR to each LF.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.edit_state)
    {
	if(ch == '\n')
	{
	    //UartSendCh(0,'\r');
	    SendChUart0('\r');
	}
	//UartSendCh(0,(INT8U)ch);
	SendChUart0((INT8U)ch);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U UartTermRxFlush(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will flush the character reception buffer while checking for CTRl-Z.  If at least is
  included, it returns 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U	ctrl_z_f = 0;

    OS_ENTER_CRITICAL();			// Disable interrupts

    while(term.rx.n_ch)				// For all characters waiting in the buffer
    {
	if(term.rx.buf[term.rx.out++] == 0x1A)		// If character is CTRL-Z
	{
	    ctrl_z_f = 1;					// Set the ctrl-Z flag
	}
	term.rx.n_ch--;					// Decrement the buffer occupancy counter
    }

    OS_EXIT_CRITICAL();				// Enable interrupts

    return(ctrl_z_f);
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U UartTermGetCh(INT16U to_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the specified timeout period for a keyboard character from the TermIsr or
  FipIsr functions.  If the timeout expires, it will return zero, otherwise it returns the received
  character forced to lower case.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U	ch = 0;				// Initialise character to zero in case of timeout

    //timeout_ms = to_ms;

//    while((timeout_ms || !to_ms) &&		// Wait for timeout to expire if set or
    while((to_ms != 1) &&
           !term.rx.n_ch)			// for new kbd character
    {
	PldProg();
	if(to_ms)
	{
	    to_ms--;
	}
	USLEEP(1000);					// Sleep for 1 ms
    }

    if(term.rx.n_ch)				// If character received
    {
	ch = term.rx.buf[term.rx.out++];		// Get character from the rx buffer
	term.rx.n_ch--;					// Decrement the buffer occupancy counter
    }

    if(ch >= 'A' && ch <= 'Z')			// If upper case character
    {
	ch |= 0x20;					// force to lower case
    }

    return(ch);					// Return character or zero if timeout occured
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: uart.c
\*---------------------------------------------------------------------------------------------------------*/

