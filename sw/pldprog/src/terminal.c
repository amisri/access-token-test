/*---------------------------------------------------------------------------------------------------------*\
  File:		terminal.c

  Purpose:	FGC3 boot - VT100/ANSI terminal support functions

  Author:	Quentin King & Martin Christophe

  Notes:	This file contains the functions for communications via UART0 with an ANSI type terminal.

  History:

    31/10/00	qak	Port from HC16 version (was called sci.c)
    12/08/03	qak	TermSendCmd() adjusted to send unprocessed command line to Tst task
    21/11/06	pfr	Adapt for FGC3 maquette
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>					// Include all header files

static INT16U 	(* const edit_func[])(INT8U) =		// Line editor functions
{
    TermLE0,						// State 0 function (Idle)
    TermLE1,						// State 1 function (Line editor)
    TermLE2,						// State 2 function (ESC pressed)
    TermLE3,						// State 3 function (Cursor key or function key)
    TermLE4,						// State 4 function (Function key)
    TermLE5,						// State 5 function (PF1-PF4)
};

/*---------------------------------------------------------------------------------------------------------*/
void TermRx(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the terminal keyboard character handler function.  It is called by UartRxTsk() when
  a character is received from UART0, which is used with a VT100/ANSI standard terminal.  It maintains
  a state machine to interpret the function/cursor keys from the terminal, which produce a sequence of
  characters.
\*---------------------------------------------------------------------------------------------------------*/
{
    term.edit_state = edit_func[term.edit_state](ch);
}
/*---------------------------------------------------------------------------------------------------------*/
void TermGetMenuOption(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a menu branch is being processed.  The user needs to enter the menu option
  they want, or various control keys (e.g. backspace to return up one level).  When in direct mode, there
  is no visual echo of the selected command and command sequences must start with a $.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U	ch;
    INT16U	menu_option;

    for(;;)			// Loop until function returns
    {
        if(term.edit_state)			// If interactive - if direct don't care
	{
	    ch = UartTermGetCh(0);		// Get keyboard character

	    switch(ch)					// Switch according to character
	    {
	    case 0x08:						// [Backspace]
	    case 0x7F:	menu.up_lvls = 1;	return;		// [Delete]	Return up on level

	    case '!':						// [$]		Switch to direct mode
	    case 0x1A:	TermDirect();		return;		// [CTRL-Z]	Switch to direct mode

	    case 0x1B:	TermInteractive();	return;		// [ESC] 	Reset in interactive

	    case 0x0A:						// [Enter]
	    case 0x0D:	MenuFollow(node);	return;		// [Return]	Follow children

	    default:						// [Any]	Menu option character

		menu_option = ch - '0';

		if(menu_option < node->n_children)		// If option is valid
		{
		    menu.node_id[menu.depth++] = ch;
		    MenuMenu(node->children[menu_option]);
		    menu.depth--;
		    return;
		}
		else                                	// invalid option
		{
		    UartTxTerm('\a');				// Just beep
		}
	    }
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermRunFuncWithArgs(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  Run the terminal.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U ch;

    term.recv_args_f	= 1;			// Enable receiving of args
    term.line_idx       = 0;			// Reset line buffer cursor index
    term.line_end       = 0;			// Reset line buffer end index
    menu.node           = node;			// Save node for use by TermEnter

    if(term.edit_state)				// If interactive mode
    {
	fprintf(TX_TERM,"%c,",menu.node_id[menu.depth-1]);	// Prompt user for arguments
    }

    while(term.recv_args_f)			// While args reception is active
    {
    	ch = UartTermGetCh(0);					// Get a character
        term.edit_state = edit_func[term.edit_state](ch);	// Process character according to edit state
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermEnter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE0() or TermLE1() if Enter or Return been received.  It will run the
  function of the current leaf node using the line buffer for the arguments.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.edit_state)				// If interactive
    {
	UartTxTerm('\n');				// Move cursor to begining of next line
    }

    term.linebuf[term.line_end] = '\0';		// Nul terminate line buffer

    MenuRunFunc(menu.node, term.linebuf);	// Run command with arguments

    term.recv_args_f = 0;			// Terminate character input
}
/*---------------------------------------------------------------------------------------------------------*/
void TermInteractive(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will enable interactive mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    term.recv_args_f = 0;			// Terminate character input
    menu.up_lvls = menu.depth+1;		// Request return to top level of menu
    ReportBoot();				// Enable interactive mode and report state
}
/*---------------------------------------------------------------------------------------------------------*/
void TermDirect(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will enable direct mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    term.recv_args_f = 0;			// Terminate character input
    menu.up_lvls     = menu.depth+1;		// Request return to top level of menu
    term.edit_state  = 0;			// Enable direct mode
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE0(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 0.  This state applies to direct mode in
  which characters are entered directly in the buffer and are not echoed to the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)					// Switch according to the key pressed
    {
    case 0x0A:						// [Enter]
    case 0x0D:	TermEnter();		break;		// [Return]	Run node function

    case 0x1B:	TermInteractive();	return(1);	// [ESC] 	Switch to interactive operation

    default:	term.recv_args_f = TermInsertChar(ch);	// [others]	Insert character in buffer
    }

    return(0);					// Continue with edit state 0
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE1(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 1. This is interactive mode in which characters
  are echoed to the terminal and simple line editing functions are supported. In state 1, the character
  is analysed directly.  If the ESC code (0x1B) is received, the state changes to 2, otherwise the
  character is processed and the state remains 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)					// Switch according to the key pressed
    {
    case 0x01:	TermStartOfLine();	break;		// [CTRL-A]	Move to start of line

    case 0x05:	TermEndOfLine();	break;		// [CTRL-E]	Move to end of line

    case 0x08:						// [Backspace]
    case 0x7F:	TermDeleteLeft();	break;		// [Delete]	Delete left

    case 0x04:	TermDeleteRight();	break;		// [CTRL-D]	Delete right

    case 0x1A:	TermDirect();		return(0);	// [CTRL-Z]	Switch to Direct mode

    case 0x0A:						// [Enter]
    case 0x0D:	TermEnter();		return(1);	// [Return]	Send command

    case 0x1B:				return(2);	// [ESC] 	Start escape sequence

    default:	term.recv_args_f = TermInsertChar(ch);	// [others]	Insert character
    }

    return(1);					// Continue with edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE2(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 2. The previous character was [ESC].
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)					// Switch according to next code
    {
    case 0x5B:				return(3);	// [Cursor/Fxx]	Change state to analyse

    case 0x4F:				return(5);	// [PF1-4]	Change state to ignore
    }

    return(1);					// All other characters - return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE3(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 3. The key was either a cursor key or a function
  key.  Cursors keys have the code sequence "ESC[A" to "ESC[D", while function keys have the code
  sequence "ESC[???~" where ??? is a variable number of alternative codes.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)					// Switch according to next code
    {
    case 0x41:	                        return(1);	// [Up]

    case 0x42:	                        return(1);	// [Down]

    case 0x43:	TermCursorRight();	return(1);	// [Right]	Move cursor right

    case 0x44:	TermCursorLeft();	return(1);	// [Left]	Move cursor left
    }

    return(4);					// Function key - change to edit state 4
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE4(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 4.  Orignial Key was a Function with sequence
  terminated by 0x7E (~).  However, the function will also accept a new [ESC] to allow an escape route
  in case of corrupted reception.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)					// Switch according to next code
    {
    case 0x1B:				return(1);	// [ESC]	Escape to edit state 1

    case 0x7E:				return(1);	// [~]		Sequence complete - return to state 1
    }

    return(4);				 	// No change to edit state
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE5(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 5.  The original key was PF1-4 and one more
  character must be ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(1);					// Return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermInsertChar(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE0() or TermLE1() if a standard character has been received.
  It returns the new value for term.recv_args_f (0=stop 1=continue).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U	i;

    /*--- Direct mode ---*/

    if(!term.edit_state)		// If direct mode
    {
	if(ch >= 0x20 && ch < 0x80) 		// If character is printable 7-bit ASCII symbol
	{
	    if(term.line_end < (TERM_LINE_SIZE-2))	// If line buffer is NOT full...
	    {
		term.linebuf[term.line_end++] = ch;	// Save new character in line buffer
		return(1);				// Return 1 to continue line entry
	    }

	    fputs(RSP_ERROR ",Argument buffer overflow\n",TX_TERM);
	}
	else					// else charcter not printable and if non-interactive
	{
	    fprintf(TX_TERM,RSP_ERROR ",Invalid character: 0x%02X\n",ch);
	}
	return(0);				// Return zero to end line entry
    }

    /*---Interactive mode ---*/

    if(ch >= 0x20 && ch < 0x80) 		// If character is printable 7-bit ASCII symbol
    {
	if(term.line_end < (TERM_LINE_SIZE-3))		// If line buffer is NOT full...
	{
	    for(i=term.line_end++;i > term.line_idx;i--)// For the rest of the line (backwards)
	    {
		term.linebuf[i] = term.linebuf[i-1];		// Shift characters one column in buffer
	    }

	    term.linebuf[term.line_idx] = ch; 		// Save new character in line buffer

	    for(i=term.line_idx++;i < term.line_end;i++)// For the rest of the line (forwards)
	    {
		UartTxTerm(term.linebuf[i]);		// Output chars to screen
	    }

	    if((i = term.line_end - term.line_idx))	// If cursor is now offset from true position
	    {
		fprintf(TX_TERM, "\33[%uD",i);		// Move cursor back
	    }
	}
	else					// else buffer is full
	{
	    UartTxTerm('\a');			// Just beep
	}
    }

    return(1);					// Return 1 to continue line entry
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor left key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!term.line_idx)			// If cursor is already at the start of the line
    {
	UartTxTerm('\a'); 			 // Just beep
    }
    else				// else
    {
	UartTxTerm('\b');			// Move cursor left one character
	term.line_idx--;			// Decrement cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor right key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.line_idx == term.line_end)		// If cursor is already at the end of the line
    {
	UartTxTerm('\a');				// Just beep
    }
    else					// else
    {
	fputs("\33[C",TX_TERM);				// Move cursor right one character
	term.line_idx++;				// Increment cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermStartOfLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the Ctrl-A key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.line_idx)				// If cursor is not already at the start of the line
    {
	fprintf(TX_TERM ,"\33[%uD", term.line_idx);	// Move cursor the required number of columns left
	term.line_idx = 0;				// Reset cursor position index
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermEndOfLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the Ctrl-E key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U	i;

    if((i = term.line_end - term.line_idx))	// If cursor is not already at the end of the line
    {
	fprintf(TX_TERM, "\33[%uC", i);	// Move cursor the required number of columns right
	term.line_idx = term.line_end;			// Set cursor position index to end of line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermDeleteLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the backspace or delete keys have been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!term.line_idx)				// If cursor is already at the start of the line
    {
	term.recv_args_f = 0;				// Leave terminal mode;
    }
    else					// else
    {
	UartTxTerm('\b');				// Cursor left
	term.line_idx--;				// Adjust cursor position index
	TermShiftRemains();				// Shift remains of line left
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermDeleteRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the Ctrl-D key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.line_idx == term.line_end)		// If cursor is already at the end of the line
    {
	UartTxTerm('\a');				// Just beep
    }
    else					// else
    {
	TermShiftRemains();				// Shift remains of line left
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermShiftRemains(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermDeleteLeft() and TermDeleteRight() to shift the remains of the line
  one character to the left.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U	ch;
    INT16U	i;

    term.line_end--;				// Adjust end of line index

    for(i=term.line_idx;i < term.line_end;i++)	// For the remainder of the line
    {
	UartTxTerm((term.linebuf[i]=term.linebuf[i+1]));// Shift character in buffer and display it
    }

    UartTxTerm(' ');			// Clear last character

    fprintf(TX_TERM,"\33[%uD",			// Move cursor the required number of columns left
	   (1+term.line_end-term.line_idx));
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: terminal.c
\*---------------------------------------------------------------------------------------------------------*/

