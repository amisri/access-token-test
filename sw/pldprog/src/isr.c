/*---------------------------------------------------------------------------------------------------------*\
 File:		isr.c

 Purpose:	ISRs for FGC3 boot

 Author:	philippe.fraboulet@cern.ch

 History:
    20/04/05	mc	Created
    28/11/06    pfr	Modified for FGC3
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>
#include <platforms\pldprog\memmap.h>

/* Declare interrupt functions */

#pragma INTERRUPT  IsrDummy1
#pragma INTERRUPT  IsrTick20Hz
#pragma INTERRUPT  Uart0RxISR
#pragma INTERRUPT  Uart2RxISR
#pragma INTERRUPT  Uart3RxISR
#pragma INTERRUPT  IsrStartButton

/*---------------------------------------------------------------------------------------------------------*/
void IsrDummy1(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    isrdummy++;
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrTick20Hz(void)
/*---------------------------------------------------------------------------------------------------------*\
 This function use the timer A4 underflow @20Hz to manage the display.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U crc;

    if(tick_20Hz++ == 19)
    {
        tick_20Hz = 0;
    }

    /* Actions: disply, ... */
    if(term.edit_state)			// Interactive mode
    {
	/* if coding wheel activated */
	if(dspl.type != CODING_WHEEL)
	{
	    dspl.mode = 1;			// "jtag run" display mode

	    /* Display selected type */
	    dspl.type = CODING_WHEEL;

            if(dspl.type > (NB_TYPES-1))
	    {
		displayString("xx");
	    }
	    else
	    {
	        displayString(dspl_str[dspl.type]);
	    }
	}

        /* display for jtag opertions */

	if((tick_20Hz == 1) && dspl.mode)
	{
	    if(xsv.stat_run)			// Jtag operation running
	    {
	        displayJtagProgress();
	    }
	}

	/* Display */
	if(dspl.mode)
	{
	    if(tick_20Hz == 0)
	    {
		if(xsv.stat_run)			// Jtag operation running
		{
		    displayJtagProgress();
		}
		else
		{
		    displayString(dspl.chars);
		}
	    }

	    if((tick_20Hz  == 10) && dspl.flash)
	    {
		displayBlank();
	    }
	}
    }
    else				// Direct mode - Download
    {
	/* Display download progress */
	if((tick_20Hz == 1) && download.size)
	{
	    displayDwnld((INT16U)(100*(download.ct)/(download.size)));
	}

	/* Download finished */
	if((tick_20Hz == 2) && (download.ct == download.size) && download.ct)		// download finished
	{
	    /* Verify checksum */
	    crc = flashCrc(download.address, download.size, &download.checksum);
	    if(download.checksum != crc)		// Wrong Checksum
	    {
		OS_ENTER_CRITICAL();
		flashWrite((INT16U *)(FLASH_STATUS_32 + 2*download.type), 0);
		term.edit_state  = 1;			// go to interactive mode
		displayString("D --");
		OS_EXIT_CRITICAL();
		fprintf(TX_TERM, RSP_ERROR ", Download pack %u failed (ct %lu - cs 0x%04X)\r", download.type, download.ct, crc);
	    }
	    else				// CheckSum OK
	    {
		OS_ENTER_CRITICAL();
		flashWrite((INT16U *)(FLASH_STATUS_32 + 2*download.type), 1);
		term.edit_state  = 1;			// go to interactive mode
		displayDwnld(100);
		OS_EXIT_CRITICAL();
		fprintf(TX_TERM, RSP_DATA ", Download pack %u succeeded (0x%04X)\r", download.type,crc);
	    }
	    initDwnld();			// Init download struct
	}

	/* 1s decrement timeout */
	if((tick_20Hz == 3) && download.ct)
	{
	    if(download.err)
	    {
		OS_ENTER_CRITICAL();
		term.edit_state  = 1;			// go to interactive mode
		flashWrite((INT16U *)(FLASH_STATUS_32 + 2*download.type), 2);			// Report timeout expiration in status
		fprintf(TX_TERM, RSP_ERROR ", Download error (%s)\r", dwnld_err[download.err]);
		initDwnld();
		OS_EXIT_CRITICAL();
	    }

	    if(download.timeout)
	    {
		download.timeout--;								// decrement every 1s
	    }
	    else										// when download timeout reaches 0
	    {
		OS_ENTER_CRITICAL();
		term.edit_state  = 1;			// go to interactive mode
		flashWrite((INT16U *)(FLASH_STATUS_32 + 2*download.type), 2);			// Report timeout expiration in status
		fprintf(TX_TERM, RSP_ERROR ", Download timeout expired (ct %lu)\r", download.ct);
		initDwnld();
		OS_EXIT_CRITICAL();
	    }
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void Uart0RxISR(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for the UART0 Rx-buffer-full interrupt.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U  ch;
    INT32U address;
    struct pack_file_header *pack_p;

    ch = u0rbl;

    SET_TP4;

    if(!term.edit_state)	        			// Direct mode - used for downloading package files
    {
	download.timeout = DWNLD_TIMEOUT;			// reset timeout

        if(!download.synchro_f)					// Before reception of syn words
	{
	    download.val.val_8[download.ct % 4] = ch;		// little endian
	    download.ct++;					// increments counter
	    if(download.ct == 4)				// SYNC words
	    {
                if(download.val.val_32 != SYNCHRO_WORD)
		{
		    term.edit_state = 1;			// Stop download
		    download.err    = DWNLD_NO_SYNC;		// No synchro words
		}
		else						// Synchro OK
		{
		    download.synchro_f = 1;			// Set synchro flag
		    download.ct = 0;				// Reset reception counter
		}
	    }
	}
	else		// Reception is synchronized
	{
	    /* copy incomming data to flash */

	    if(download.ct <= 10 || download.ct <= download.size)
	    {
		if(download.ct < 4)						// getting the flash address for storage
		{
		    download.val.val_8[download.ct % 4] = ch;			// little endian
		    if(download.ct == 3)					// address where package must be written (in flash)
		    {
			download.address  = download.val.val_32;
			download.ptr 	  = (INT16U*)download.val.val_32;	// Initialise flash pointer

			/* Write start address in Flash */

			flashWrite(download.ptr++, download.val.val_16[0]);	// LSB
			flashWrite(download.ptr++, download.val.val_16[1]);     // MSB
		    }
		}
		else
		{
		    /* header file received - copy file information to download struct */

		    if(download.ct == 10)
		    {
			*(INT16U*)download.address = FLASH_READ_ARRAY;	// go in read array mode (write a 16b word)

			pack_p = (struct pack_file_header*)download.address;
			download.size = pack_p->size;
			download.type = pack_p->type;

			/* Check start address */

			if(download.address != type_map[pack_p->type])
			{
			    download.err = DWNLD_AD_MISM;
			}
		    }

		    /* Write Flash */

		    download.val.val_8[download.parity] = ch;
		    if(download.parity)						// even byte
		    {
			flashWrite(download.ptr, download.val.val_16[0]);
			download.ptr++;
		    }
		    download.parity = !download.parity;				// mark bytes parity: parity = 0 if even byte
		}
		download.ct++;							// increment download counter
	    }
	}
    }
    else								// normal mode (interactive)
    {
        if(ch == '\032')				// ctrl-Z
	{
	    download.err	= 0;			// reset download error
	    download.timeout	= DWNLD_TIMEOUT;	// set Timeout
	    term.recv_args_f 	= 1;                    // clear command flag
	    term.edit_state	= 1;			// go to interactive mode
	    initDwnld();				// init download structure
	    dspl.mode           = 0;			// download display mode
	}

	if(!ch || ch > 127)			/* Ignore null/8-bit characters */
	{
	    return;
	}
	if(ch == '\3')				/* CTRL-C = Abort */
	{
	    dev.abort_f = 3;
	    return;
	}
	if(ch != 19 && ch != 17)
	{
	    if(term.rx.n_ch < TERM_BUF_SIZE)
	    {
		term.rx.buf[term.rx.in++] = ch;
		term.rx.n_ch++;
	    }
	}
	else
	{
	    if(ch == 19)		/* CTRL-S = XOFF */
	    {
		term.xoff_timeout = TERM_XOFF_TIMEOUT_MS;
	    }
	    if(ch == 17)		/* CTRL-Q = XON */
	    {
		term.xoff_timeout = 0;
	    }
	}
    }

    RESET_TP4;
}
/*---------------------------------------------------------------------------------------------------------*/
void Uart2RxISR(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for the UART2 Rx-buffer-full interrupt.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U ch;

    ch = u2rbl;				// flush buffer

    u2tb    =   0x00;					// enable receive interupts
}
/*---------------------------------------------------------------------------------------------------------*/
void Uart3RxISR(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for the UART0 Rx-buffer-full interrupt.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U ch;

    ch = u3rbl;				// flush buffer
    //u3tb    =   0x00;			// enable receive interupts
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrStartButton(void)
/*---------------------------------------------------------------------------------------------------------*\
 This function is called when there is an INT2 interrupt.
\*---------------------------------------------------------------------------------------------------------*/
{
    xsv.start_f = 1;			// set start flag
    dspl.mode   = 1;			// "jtag run" display mode
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/

