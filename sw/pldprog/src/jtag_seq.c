/*---------------------------------------------------------------------------------------------------------*\
 File:		jtag_seq.c

 Purpose:	FGC3 boot

 Abstract:	This file contains the routines to interpret the XSVF commands

 Author:	Philippe Fraboulet

 History:
    Functions added by NMVC:    CheckFScanf
				strCompare
				initializeXSVFtestInfo
				ReadXDiagnosisFile
				writeResultsFile
				writeJTAGErrorFile
				goThroughBSDLEntitys
				findDeviceAndCell
				identifyFailingBits
				findDataShiftErrors
				updateFailingINTER
    12/05/06	pfr	Adapted from xilinx player micro.c file
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>
#include "xda.h"

#define XSVF_VERSION    "5.00"

/*---------------------------------------------------------------------------------------------------------*\
  Define:       XSVF_SUPPORT_COMPRESSION
  Description:  Define this to support the XC9500/XL XSVF data compression
                scheme.
                Code size can be reduced by NOT supporting this feature.
                However, you must use the -nc (no compress) option when
                translating SVF to XSVF using the SVF2XSVF translator.
                Corresponding, uncompressed XSVF may be larger.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef XSVF_SUPPORT_COMPRESSION
    #define XSVF_SUPPORT_COMPRESSION    1
#endif

/*---------------------------------------------------------------------------------------------------------*\
  Define:       XSVF_SUPPORT_ERRORCODES
  Description:  Define this to support the new XSVF error codes.
                (The original XSVF player just returned 1 for success and
                0 for an unspecified failure.)
\*---------------------------------------------------------------------------------------------------------*/

#ifndef XSVF_SUPPORT_ERRORCODES
    #define XSVF_SUPPORT_ERRORCODES     1
#endif

#ifdef  XSVF_SUPPORT_ERRORCODES
    #define XSVF_ERRORCODE(errorCode)   errorCode
#else   /* Use legacy error code */
    #define XSVF_ERRORCODE(errorCode)   ((errorCode==XSVF_ERROR_NONE)?1:0)
#endif  /* XSVF_SUPPORT_ERRORCODES */

/*---------------------------------------------------------------------------------------------------------*\
  Define:       XSVF_MAIN
  Description:  Define this to compile with a main function for standalone
                debugging.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef XSVF_MAIN
    #ifdef DEBUG_MODE
		  #define XSVF_MAIN   1
    #endif  /* DEBUG_MODE */
#endif  /* XSVF_MAIN */

/*---------------------------------------------------------------------------------------------------------*\
  DEBUG_MODE #define
\*---------------------------------------------------------------------------------------------------------*/

#ifdef  DEBUG_MODE
    #define XSVFDBG_PRINTF(iDebugLevel,pzFormat) \
                { if ( xsv_iDebugLevel >= iDebugLevel ) \
                    printf(pzFormat ); }
    #define XSVFDBG_PRINTF1(iDebugLevel,pzFormat,arg1) \
                { if ( xsv_iDebugLevel >= iDebugLevel ) \
                    printf(pzFormat, arg1 ); }
	 #define XSVFDBG_PRINTF2(iDebugLevel,pzFormat,arg1,arg2) \
                { if ( xsv_iDebugLevel >= iDebugLevel ) \
                    printf(pzFormat, arg1, arg2 ); }
    #define XSVFDBG_PRINTF3(iDebugLevel,pzFormat,arg1,arg2,arg3) \
                { if ( xsv_iDebugLevel >= iDebugLevel ) \
                    printf(pzFormat, arg1, arg2, arg3 ); }
    #define XSVFDBG_PRINTLENVAL(iDebugLevel,plenVal) \
                { if ( xsv_iDebugLevel >= iDebugLevel ) \
                    xsvfPrintLenVal(plenVal); }
#else   /* !DEBUG_MODE */
    #define XSVFDBG_PRINTF(iDebugLevel,pzFormat)
    #define XSVFDBG_PRINTF1(iDebugLevel,pzFormat,arg1)
    #define XSVFDBG_PRINTF2(iDebugLevel,pzFormat,arg1,arg2)
    #define XSVFDBG_PRINTF3(iDebugLevel,pzFormat,arg1,arg2,arg3)
    #define XSVFDBG_PRINTLENVAL(iDebugLevel,plenVal)
#endif  /* DEBUG_MODE */



/* Array of XSVF command functions.  Must follow command byte value order! */
/* If your compiler cannot take this form, then convert to a switch statement*/

TXsvfDoCmdFuncPtr   xsvf_pfDoCmd[]  =
{
    xsvfDoXCOMPLETE,        /*  0 */
    xsvfDoXTDOMASK,         /*  1 */
    xsvfDoXSIR,             /*  2 */
    xsvfDoXSDR,             /*  3 */
    xsvfDoXRUNTEST,         /*  4 */
    xsvfDoIllegalCmd,       /*  5 */
    xsvfDoIllegalCmd,       /*  6 */
    xsvfDoXREPEAT,          /*  7 */
    xsvfDoXSDRSIZE,         /*  8 */
    xsvfDoXSDRTDO,          /*  9 */
#ifdef  XSVF_SUPPORT_COMPRESSION
    xsvfDoXSETSDRMASKS,     /* 10 */
    xsvfDoXSDRINC,          /* 11 */
#else
    xsvfDoIllegalCmd,       /* 10 */
    xsvfDoIllegalCmd,       /* 11 */
#endif  /* XSVF_SUPPORT_COMPRESSION */
    xsvfDoXSDRBCE,          /* 12 */
    xsvfDoXSDRBCE,          /* 13 */
    xsvfDoXSDRBCE,          /* 14 */
    xsvfDoXSDRTDOBCE,       /* 15 */
    xsvfDoXSDRTDOBCE,       /* 16 */
    xsvfDoXSDRTDOBCE,       /* 17 */
    xsvfDoXSTATE,           /* 18 */
    xsvfDoXENDXR,           /* 19 */
    xsvfDoXENDXR,           /* 20 */
    xsvfDoXSIR2,            /* 21 */
    xsvfDoXCOMMENT,         /* 22 */
    xsvfDoXWAIT,             /* 23 */
    xsvfDoXCHECKSUM,	    /* 24 */
/* Insert new command functions here */
};

#ifdef  DEBUG_MODE
    char* xsvf_pzCommandName[]  =
    {
        "XCOMPLETE",
        "XTDOMASK",
        "XSIR",
        "XSDR",
        "XRUNTEST",
        "Reserved5",
        "Reserved6",
        "XREPEAT",
        "XSDRSIZE",
        "XSDRTDO",
        "XSETSDRMASKS",
	"XSDRINC",
        "XSDRB",
        "XSDRC",
        "XSDRE",
        "XSDRTDOB",
        "XSDRTDOC",
        "XSDRTDOE",
        "XSTATE",
	"XENDIR",
        "XENDDR",
        "XSIR2",
        "XCOMMENT",
        "XWAIT",
	"XCHECKSUM",
    };

    char*   xsvf_pzErrorName[]  =
    {
        "No error",
        "Unknown",
        "TDO mismatch",
        "Data error or timeout",	//"ERROR:  TDO mismatch and exceeded max retries",
        "Unsupported XSVF command",
        "Illegal state specification",
        "Data overflows allocated LENVAL_MAX buffer size",
        "Illegal jump",
        "No power",
	 };

    char*   xsvf_pzTapState[] =
    {
        "RESET",        /* 0x00 */
        "RUNTEST/IDLE", /* 0x01 */
        "DRSELECT",     /* 0x02 */
        "DRCAPTURE",    /* 0x03 */
        "DRSHIFT",      /* 0x04 */
        "DREXIT1",      /* 0x05 */
        "DRPAUSE",      /* 0x06 */
        "DREXIT2",      /* 0x07 */
        "DRUPDATE",     /* 0x08 */
        "IRSELECT",     /* 0x09 */
        "IRCAPTURE",    /* 0x0A */
        "IRSHIFT",      /* 0x0B */
        "IREXIT1",      /* 0x0C */
        "IRPAUSE",      /* 0x0D */
        "IREXIT2",      /* 0x0E */
        "IRUPDATE"      /* 0x0F */
    };
#endif  /* DEBUG_MODE */

//FILE            *debug_file;
//char            drive_letter;
//INT8U            xda_file[32];

/*---------------------------------------------------------------------------------------------------------*\
  Utility Functions
\*---------------------------------------------------------------------------------------------------------*/

#ifdef  DEBUG_MODE
/*---------------------------------------------------------------------------------------------------------*/
void xsvfPrintLenVal(lenVal *ptr_lv)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfPrintLenVal
  Description:  Print the lenval value in hex.
  Parameters:   ptr_lv     - ptr to lenval.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U i;

    if(ptr_lv)
    {
        printf("0x" );

        for ( i = 0; i < ptr_lv->len; ++i )
        {
            printf("%02x", (INT16U)ptr_lv->val[ i ]);
        }
        printf("\r\n");
    }
}
#endif  /* DEBUG_MODE */
/*---------------------------------------------------------------------------------------------------------*/
void write_tdo_debug(lenVal *mask, lenVal *expected, lenVal *received)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     write_tdo_debug
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U i;
    if(expected)
    {
        //XSVFDBG_PRINTF(1,"Expected = 0x");

        for ( i = 0; i < expected->len; ++i )
        {
            //XSVFDBG_PRINTF1(1,"%02X", (INT16U)(mask->val[i] & expected->val[i]));
        }
        //XSVFDBG_PRINTF(1,"\r\n");
    }

    if(received)
    {
        //XSVFDBG_PRINTF(1,"Received = 0x");

        for ( i = 0; i < received->len; ++i )
        {
            //XSVFDBG_PRINTF1(1,"%02X", (INT16U)(mask->val[i] & received->val[i]));
        }
        //XSVFDBG_PRINTF(1,"\r\n");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfInfoInit( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfInfoInit
  Description:  Initialize the xsvfInfo data.
  Parameters:   pXsvfInfo   - ptr to the XSVF info structure.
  Returns:      INT16U         - 0 = success; otherwise error.
\*---------------------------------------------------------------------------------------------------------*/
{
    //XSVFDBG_PRINTF1(1,"    sizeof( SXsvfInfo ) = %d bytes\r\n",
    //	                     sizeof( SXsvfInfo ) );

    pXsvfInfo->ucComplete       = 0;
    pXsvfInfo->ucCommand        = XCOMPLETE;
    pXsvfInfo->lCommandCount    = 0;
    pXsvfInfo->iErrorCode       = XSVF_ERROR_NONE;
    pXsvfInfo->ucMaxRepeat      = 0;
    pXsvfInfo->ucTapState       = XTAPSTATE_RESET;
    pXsvfInfo->ucEndIR          = XTAPSTATE_RUNTEST;
    pXsvfInfo->ucEndDR          = XTAPSTATE_RUNTEST;
    pXsvfInfo->lShiftLengthBits = 0L;
	 pXsvfInfo->sShiftLengthBytes= 0;
    pXsvfInfo->lRunTestTime     = 0L;

    return( 0 );
}
/*---------------------------------------------------------------------------------------------------------*/
void xsvfInfoCleanup( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfInfoCleanup
  Description:  Cleanup the xsvfInfo data.
  Parameters:   pXsvfInfo   - ptr to the XSVF info structure.
  Returns:      INT8U.
\*---------------------------------------------------------------------------------------------------------*/
{
}

/*---------------------------------------------------------------------------------------------------------*/
INT16S xsvfGetAsNumBytes( long lNumBits )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfGetAsNumBytes
  Description:  Calculate the number of bytes the given number of bits
                consumes.
  Parameters:   lNumBits    - the number of bits.
  Returns:      INT16S       - the number of bytes to store the number of bits.
\*---------------------------------------------------------------------------------------------------------*/
{
    return( (INT16S)( ( lNumBits + 7L ) / 8L ) );
}
/*---------------------------------------------------------------------------------------------------------*/
void xsvfTmsTransition( INT16S sTms )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfTmsTransition
  Description:  Apply TMS and transition TAP controller by applying one TCK
                cycle.
  Parameters:   sTms    - new TMS value.
  Returns:      INT8U.
\*---------------------------------------------------------------------------------------------------------*/
{
    setPort( TMS, sTms );
    setPort( TCK, 0 );
    setPort( TCK, 1 );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfGotoTapState( INT8U* pucTapState, INT8U ucTargetState )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfGotoTapState
  Description:  From the current TAP state, go to the named TAP state.
                A target state of RESET ALWAYS causes TMS reset sequence.
                All SVF standard stable state paths are supported.
                All state transitions are supported except for the following
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U i;
    INT16U iErrorCode;

    iErrorCode  = XSVF_ERROR_NONE;
    if ( ucTargetState == XTAPSTATE_RESET )
    {
        /* If RESET, always perform TMS reset sequence to reset/sync TAPs */
        xsvfTmsTransition( 1 );
        for ( i = 0; i < 5; ++i )
        {
            setPort( TCK, 0 );
            setPort( TCK, 1 );
        }
        *pucTapState    = XTAPSTATE_RESET;
        //XSVFDBG_PRINTF( 5, "   TMS Reset Sequence -> Test-Logic-Reset\r\n" );
        //XSVFDBG_PRINTF1( 5, "   TAP State = %s\n", xsvf_pzTapState[ *pucTapState ] );
    }
    else
    {
	if ( ( ucTargetState != *pucTapState ) &&
		  ( ( ( ucTargetState == XTAPSTATE_EXIT2DR ) && ( *pucTapState != XTAPSTATE_PAUSEDR ) ) ||
		    ( ( ucTargetState == XTAPSTATE_EXIT2IR ) && ( *pucTapState != XTAPSTATE_PAUSEIR ) ) ) )
	{
	    /* Trap illegal TAP state path specification */
	    iErrorCode      = XSVF_ERROR_ILLEGALSTATE;
	}
	else
	{
	    if ( ucTargetState == *pucTapState )
	    {
		/* Already in target state.  Do nothing except when in DRPAUSE
		   or in IRPAUSE to comply with SVF standard */
		if ( ucTargetState == XTAPSTATE_PAUSEDR )
		{
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_EXIT2DR;
		    //XSVFDBG_PRINTF1( 5, "   TAP State = %s\r\n", xsvf_pzTapState[ *pucTapState ] );
		}
		else if ( ucTargetState == XTAPSTATE_PAUSEIR )
		{
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_EXIT2IR;
		    //XSVFDBG_PRINTF1( 5, "   TAP State = %s\r\n", xsvf_pzTapState[ *pucTapState ] );
		}
	    }

	    /* Perform TAP state transitions to get to the target state */
	    while ( ucTargetState != *pucTapState )
	    {
		switch ( *pucTapState )
		{
		case XTAPSTATE_RESET:
		    xsvfTmsTransition( 0 );
		    *pucTapState    = XTAPSTATE_RUNTEST;
		    break;
		case XTAPSTATE_RUNTEST:
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_SELECTDR;
		    break;
		case XTAPSTATE_SELECTDR:
		    if ( ucTargetState >= XTAPSTATE_IRSTATES )
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_SELECTIR;
		    }
		    else
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_CAPTUREDR;
		    }
		    break;
		case XTAPSTATE_CAPTUREDR:
		    if ( ucTargetState == XTAPSTATE_SHIFTDR )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_SHIFTDR;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_EXIT1DR;
		    }
		    break;
		case XTAPSTATE_SHIFTDR:
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_EXIT1DR;
		    break;
		case XTAPSTATE_EXIT1DR:
		    if ( ucTargetState == XTAPSTATE_PAUSEDR )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_PAUSEDR;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_UPDATEDR;
		    }
		    break;
		case XTAPSTATE_PAUSEDR:
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_EXIT2DR;
		    break;
		case XTAPSTATE_EXIT2DR:
		    if ( ucTargetState == XTAPSTATE_SHIFTDR )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_SHIFTDR;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_UPDATEDR;
		    }
		    break;
		case XTAPSTATE_UPDATEDR:
		    if ( ucTargetState == XTAPSTATE_RUNTEST )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_RUNTEST;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_SELECTDR;
		    }
		    break;
		case XTAPSTATE_SELECTIR:
		    xsvfTmsTransition( 0 );
		    *pucTapState    = XTAPSTATE_CAPTUREIR;
		    break;
		case XTAPSTATE_CAPTUREIR:
		    if ( ucTargetState == XTAPSTATE_SHIFTIR )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_SHIFTIR;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_EXIT1IR;
		    }
		    break;
		case XTAPSTATE_SHIFTIR:
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_EXIT1IR;
		    break;
		case XTAPSTATE_EXIT1IR:
		    if ( ucTargetState == XTAPSTATE_PAUSEIR )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_PAUSEIR;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_UPDATEIR;
		    }
		    break;
		case XTAPSTATE_PAUSEIR:
		    xsvfTmsTransition( 1 );
		    *pucTapState    = XTAPSTATE_EXIT2IR;
		    break;
		case XTAPSTATE_EXIT2IR:
		    if ( ucTargetState == XTAPSTATE_SHIFTIR )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_SHIFTIR;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_UPDATEIR;
		    }
		    break;
		case XTAPSTATE_UPDATEIR:
		    if ( ucTargetState == XTAPSTATE_RUNTEST )
		    {
			xsvfTmsTransition( 0 );
			*pucTapState    = XTAPSTATE_RUNTEST;
		    }
		    else
		    {
			xsvfTmsTransition( 1 );
			*pucTapState    = XTAPSTATE_SELECTDR;
		    }
		    break;
		default:
		    iErrorCode      = XSVF_ERROR_ILLEGALSTATE;
		    *pucTapState    = ucTargetState;    /* Exit while loop */
		    break;
		}
		//XSVFDBG_PRINTF1( 5, "   TAP State = %s\r\n",
		//		 xsvf_pzTapState[ *pucTapState ] );
	    }
	}
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
void xsvfShiftOnly( long lNumBits, lenVal* plvTdi, lenVal* plvTdoCaptured, INT16U iExitShift )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfShiftOnly
  Description:  Assumes that starting TAP state is SHIFT-DR or SHIFT-IR.
                Shift the given TDI data into the JTAG scan chain.
                Optionally, save the TDO data shifted out of the scan chain.
                Last shift cycle is special:  capture last TDO, set last TDI,
                but does not pulse TCK.  Caller must pulse TCK and optionally
                set TMS=1 to exit shift state.
  Parameters:   lNumBits        - number of bits to shift.
                plvTdi          - ptr to lenval for TDI data.
                plvTdoCaptured  - ptr to lenval for storing captured TDO data.
                iExitShift      - 1=exit at end of shift; 0=stay in Shift-DR.
  Returns:      INT8U.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U*  pucTdi;
    INT8U*  pucTdo;
    INT16U   ucTdiByte;
    INT16U   ucTdoByte;
    INT16U   ucTdoBit;
    INT16U   i;

    INT32U   j;

    /* assert( ( ( lNumBits + 7 ) / 8 ) == plvTdi->len ); */

    /* Initialize TDO storage len == TDI len */
	 pucTdo  = 0;
    if ( plvTdoCaptured )
    {
        plvTdoCaptured->len = plvTdi->len;
        pucTdo              = plvTdoCaptured->val + plvTdi->len;
    }

    /* Shift LSB first.  val[N-1] == LSB.  val[0] == MSB. */
    pucTdi  = plvTdi->val + plvTdi->len;
    while ( lNumBits )
    {
        /* Process on a byte-basis */
        ucTdiByte   = (*(--pucTdi));
        ucTdoByte   = 0;
        for ( i = 0; ( lNumBits && ( i < 8 ) ); ++i )
        {
	    --lNumBits;
            if ( iExitShift && !lNumBits )
            {
                /* Exit Shift-DR state */
                setPort( TMS, 1 );
            }

            /* Set the new TDI value */
	    setPort( TDI, (INT16S)(ucTdiByte & 1) );
            ucTdiByte   >>= 1;

            /* Set TCK low */
            setPort( TCK, 0 );

            if ( pucTdo )
            {
                /* Save the TDO value */
                ucTdoBit    = readTDOBit();
                ucTdoByte   |= ( ucTdoBit << i );
            }

	    /* Set TCK high */
            setPort( TCK, 1 );
        }

        /* Save the TDO byte value */
        if ( pucTdo )
        {
            (*(--pucTdo))   = ucTdoByte;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U CheckFScanf(INT16U temp)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     CheckFScanf
\*---------------------------------------------------------------------------------------------------------*/
{
    if (temp == 0 || temp == EOF)
    {
        return (-1);
    }
    else
    {
        return (0);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U strCompare(char *csta, char *buff)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     strCompare
\*---------------------------------------------------------------------------------------------------------*/
{
    //return(strcmpi(csta, buff));
    return(strcmp(csta, buff));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U initializeXSVFtestInfo(void)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     initializeXSVFtestInfo
\*---------------------------------------------------------------------------------------------------------*/
{
    xsvfTestInfo.interVectorNumber = -1;
    xsvfTestInfo.totalErrors       = 0;
    xsvfTestInfo.pass_fail         = PASS;
    failingPins[0].deviceIndex     = ENDOF_ALL_ERRORS;
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
void resultsSummary ( lenVal* plvXOR, lenVal* plvTDOCaptured )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     resultsSummary
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;
    INT16U  errors;
    INT8U   const *diag[3] = {"stuck at 0","stuck at 1","unknown"};

    /* Number of bits in each plvXOR->val[ index ] */
    INT16U bitsPer2Chars = 8;

    // Maximum number of errors to print
    errors = (bscan_error_count < BSCAN_ERR_PRINT) ? bscan_error_count : BSCAN_ERR_PRINT;

    if ( (xsv.mode == inter_mode) )
    {
	if(bscan_error_count)
	{
	    printf("$2,boundary scan failed %i errors\r\n", bscan_error_count);
	    for(i=0;i<errors;i++)
	    {
		if(!xda[bscan_err[i].pos])
		{
		    printf("$2,Unknown error (bscan pos %d)\r\n", bscan_err[i].pos);
		}
		printf("$2,pin %i %s\r\n", xda[bscan_err[i].pos],
							  diag[bscan_err[i].rec_val]);
	    }
	}
	else
	{
	    printf("$0,boundary scan completed\r\n", bscan_error_count);
	}
    }
    return;
}
#ifdef PORTING_XLX_PLAYER
/*---------------------------------------------------------------------------------------------------------*/
INT16U updateFailingINFRA(void)
/*---------------------------------------------------------------------------------------------------------*\
   updateFailingINFRA
   Go through the array of errors to find out if an INFRA error already exists. \
   If not add it to the array.
\*---------------------------------------------------------------------------------------------------------*/
{
        INT16U error_index = 0;
        bscan_error_count++;
        // Go through the array of errors. The error could not exist before because there is
        // only 1 vector in an INFRA test
        while (failingPins[error_index].deviceIndex != ENDOF_ALL_ERRORS)
                ++error_index;
        if (error_index == MAX_FAIL_PINS)
        // We got too many errors.
        {
                //XSVFDBG_PRINTF( 5, "\n[TEST] - Too many unique errors. No more errors will be stored.\n");
        }
        else
        {
                // Now add it to the array
                failingPins[error_index].deviceIndex         = xsvfTestInfo.deviceIndex;

                failingPins[error_index].errorType           = xsvfTestInfo.receivedBit;

                // Position within the device where the error occurred
                failingPins[error_index].scanFailBit         = xsvfTestInfo.scanFailCell;

                // Mark end of errors
                failingPins[error_index+1].deviceIndex       = ENDOF_ALL_ERRORS;
        }
        return(2);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U updateFailingINTER(void)
/*---------------------------------------------------------------------------------------------------------*\
   updateFailingINTER
   Go through the array of errors to find out if an INTER error already exists.
   If not add it to the array.
\*---------------------------------------------------------------------------------------------------------*/
{
        INT16U error_index;
        error_index = 0;
        // Go through the array of errors to find out if it already exists
        while (failingPins[error_index].deviceIndex != ENDOF_ALL_ERRORS)
        {
	    if ( (xsvfTestInfo.deviceIndex == failingPins[error_index].deviceIndex) &&
		      (strCompare(failingPins[error_index].pinDes,
		      bsdlMapping[AllDevices [xsvfTestInfo.deviceIndex].bsdlIndex].cells[xsvfTestInfo.cellIndex].pinDes) == 0) )
	    //Found the same device and pin
	    {
		// Now check the type of error we had until now
		if (xsvfTestInfo.receivedBit == 0)
		{
		    if (failingPins[error_index].errorType == ERROR_STA1)
			    failingPins[error_index].errorType  = ERROR_UNKNOWN;
		} else
		{
		    if (failingPins[error_index].errorType == ERROR_STA0)
			    failingPins[error_index].errorType = ERROR_UNKNOWN;
		}
		return (1);
	    }
	    ++error_index;
        }
        if (error_index == MAX_FAIL_PINS)
        // We got too many errors.
        {
	    //XSVFDBG_PRINTF( 5, "\n[TEST] - Too many unique errors. No more errors will be stored.\n");
        }
        else
        {
	    // Pin was not found: add it to the array
	    failingPins[error_index].deviceIndex         = xsvfTestInfo.deviceIndex;

	    strcpy ( failingPins[error_index].pinDes,
				    bsdlMapping[AllDevices [xsvfTestInfo.deviceIndex].bsdlIndex].cells[xsvfTestInfo.cellIndex].pinDes );
	    // Assume we have a STUCK-AT type of error
	    failingPins[error_index].errorType           = xsvfTestInfo.receivedBit;
	    // Position within the chain where the error occurred
	    failingPins[error_index].scanFailBit         = xsvfTestInfo.scanFailBit;
	    // Mark end of errors
	    failingPins[error_index+1].deviceIndex       = ENDOF_ALL_ERRORS;
        }
        bscan_error_count++;
        return (2);
}
#endif
/*---------------------------------------------------------------------------------------------------------*/
INT16U identifyFailingBits (INT16U testChar, INT16U testCap, INT16U byteIndex)
/*---------------------------------------------------------------------------------------------------------*\
  identifyFailingBits
  Identifies each bit at 1 in bitPattern and indentify which bits are at 1 in the testChar -> Error.
  For each error it determines: xsvfTestInfo.scanFailBit, xsvfTestInfo.pass_fail
  and xsvfTestInfo.receivedBit (INTER test).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U 	i, pos;
    INT16U	err_idx = 0;
    INT16U	err_exists = 0;

    /* history of failing vectors */

    if(bscan_history_idx < MAX_FAIL_PINS)
    {
	bscan_history[bscan_history_idx++] = xsvfTestInfo.interVectorNumber;
    }
    for (i = 1, pos = 0; i <= 128; ++pos, i=i<<1) // Check 8 bits
    {
	if (( testChar & i) != 0)
	// Found a cell with an error
	{
	    xsvfTestInfo.pass_fail   = FAIL;
	    xsvfTestInfo.scanFailBit = pos + byteIndex;   // starts at 0

	    //XSVFDBG_PRINTF1( 5, "[TEST] - xsvfTestInfo.scanFailBit: %d\r\n", xsvfTestInfo.scanFailBit);

	    //if (findDeviceAndCell() < 0) return (-1);

	    /* Check if the error exists */
	    while(bscan_err[err_idx].pos)
	    {
	        /* If Yes */
		if(bscan_err[err_idx].pos == xsvfTestInfo.scanFailBit)
		{
		    err_exists = 1;
		    if(xsv.mode == inter_mode &&
		        bscan_err[err_idx].rec_val < 2)		// not unknown err
		    {
		        /* received value diff from previous */
			if( bscan_err[err_idx].rec_val != (INT16U)(!!(testCap & i)) )
			{
			    bscan_err[err_idx].rec_val = 2;	// unknown error
			}
		    }
		}
		err_idx++;
	    }

	    /* new error */
	    if(!err_exists)
	    {
		if(bscan_error_count != err_idx)
		{
		    printf("$2,Inconsistency in error count\r\n");
		    return(STAT_ERR);
		}
		if(err_idx >= MAX_FAIL_PINS)
		{
		    printf("$2,Too many failing pins\r\n");
		    return(STAT_ERR);
		}
		else
		{
		    bscan_err[err_idx].pos     = xsvfTestInfo.scanFailBit;
		    bscan_err[err_idx].rec_val = (INT16U)(!!(testCap & i));
		    bscan_error_count++;
		}
	    }
	}
    }
    return (STAT_PASS);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16S findDataShiftErrors(  lenVal* plvTdoExpected,
			      lenVal* plvTdoCaptured,
			      lenVal* plvTdoMask,
			      lenVal* plvXOR,
			      lenVal* plvPrevCap )
/*---------------------------------------------------------------------------------------------------------*\
  findDataShiftErrors
  For each Byte (8 bits) of data:
   - find out if an error occurred (bitPattern != 0).
   - calculates the XOR vector.
\*---------------------------------------------------------------------------------------------------------*/
{
     INT16U    bitPattern, bitPos;
     INT16S    sIndex;
     INT16U    error_flag = 0;

     sIndex  = plvTdoExpected->len;

     bitPos  = 0;

     while (sIndex--)
     {
	// PASS means -> ((exp XOR rst) AND msk) = 0
	//      FAIL means -> ((exp XOR rst) AND msk) != 0
	bitPattern  = (INT16U) ( (plvTdoExpected->val[ sIndex ] ^ plvTdoCaptured->val[ sIndex ])
				& plvTdoMask->val[ sIndex ] );

	// Initialize Cap vector with the first captured vector and XOR with 0
	if (xsvfTestInfo.interVectorNumber == 1)
	{
	    plvPrevCap->val[ sIndex ] = plvTdoCaptured->val[ sIndex ];
	    plvXOR->val[ sIndex ]     = 0;
	    if(xsv.mode == checksum_mode)
	    {
		plvXOR->val[ sIndex ] = plvTdoCaptured->val[ sIndex ] & plvTdoMask->val[ sIndex ];
	    }
	}
	else
	{
            if(xsv.mode != checksum_mode)
	    {
		// OR with the XOR vector so we don't loose the bits that are at 1
		// Only the bits that are not masked (MASK = 1) should be taken into account
		//plvXOR->val[ sIndex ]      = ( ( plvTdoCaptured->val[ sIndex ] ^ plvPrevCap->val[ sIndex ] )| plvXOR->val[ sIndex ] );

		//plvXOR->val[ sIndex ]     = ( ( (plvTdoCaptured->val[ sIndex ] & plvTdoMask->val[ sIndex ])
		//			      ^ (plvPrevCap->val[ sIndex ] & plvTdoMask->val[ sIndex ]))
		//			      | plvXOR->val[ sIndex ]);
		plvXOR->val[ sIndex ]     = ( ( (plvTdoCaptured->val[ sIndex ] ^ plvPrevCap->val[ sIndex ])
					       & plvTdoMask->val[ sIndex ] )
					     | plvXOR->val[ sIndex ] );

	    }
	    else		// checsum mode: XOR of all captured vectors
	    {
		plvXOR->val[ sIndex ] ^= plvTdoCaptured->val[sIndex] & plvTdoMask->val[sIndex];
	    }
	    // Previous = current
	    plvPrevCap->val[ sIndex ]  = plvTdoCaptured->val[ sIndex ];
	}

	//XSVFDBG_PRINTF ( 5, "\n[TEST] - Going through Captured Vector:\r\n");
	//XSVFDBG_PRINTF1( 5, "[TEST] - Vector                   : %d\r\n", xsvfTestInfo.interVectorNumber);
	//XSVFDBG_PRINTF1( 5, "[TEST] - sIndex                   : %d\r\n", sIndex);
	//XSVFDBG_PRINTF1( 5, "[TEST] - Captured Vector [sIndex] : %02x\r\n", ((INT16U)(plvTdoCaptured->val[ sIndex ])) );
	//XSVFDBG_PRINTF1( 5, "[TEST] - XOR Vector [sIndex]      : %02x\r\n", ((INT16U)(plvXOR->val[ sIndex ])) );
	//XSVFDBG_PRINTF1( 5, "[TEST] - bitPattern               : %d\r\n", bitPattern);

	if ( (xsv.mode != checksum_mode) && (bitPattern != 0))
	// Errors occurred
	{
	    error_flag = 1;

	    if (identifyFailingBits ( bitPattern, (INT16U) ( plvTdoCaptured->val[ sIndex ]), bitPos) != STAT_PASS)
	    {
		printf("$2,Error analysing errors\r\n" );
		exit(1);
	    }
	}

	bitPos += 8; // Next byte
     }

     // Debug test (temporary)

/*
     if(error_flag)
     {
	write_tdo_debug(debug_file, plvTdoMask, plvTdoExpected, plvTdoCaptured);
     }
*/

     return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfShift( INT8U*   	pucTapState,
               INT8U    	ucStartState,
               long             lNumBits,
	       lenVal*          plvTdi,
               lenVal*          plvTdoCaptured,
               lenVal*          plvTdoExpected,
               lenVal*          plvTdoMask,
               lenVal*          plvXOR,
               lenVal*          plvPrevCap,
               INT8U    	ucEndState,
               long             lRunTestTime,
               INT8U    	ucMaxRepeat )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfShift
  Description:  Goes to the given starting TAP state.
                Calls xsvfShiftOnly to shift in the given TDI data and
                optionally capture the TDO data.
                Compares the TDO captured data against the TDO expected
                data.
                If a data mismatch occurs, then executes the exception
                handling loop upto ucMaxRepeat times.
  Parameters:   pucTapState     - Ptr to current TAP state.
                ucStartState    - Starting shift state: Shift-DR or Shift-IR.
                lNumBits        - number of bits to shift.
                plvTdi          - ptr to lenval for TDI data.
                plvTdoCaptured  - ptr to lenval for storing TDO data.
                plvTdoExpected  - ptr to expected TDO data.
                plvTdoMask      - ptr to TDO mask.
                ucEndState      - state in which to end the shift.
                lRunTestTime    - amount of time to wait after the shift.
                ucMaxRepeat     - Maximum number of retries on TDO mismatch.
  Returns:      INT16U             - 0 = success; otherwise TDO mismatch.
  Notes:        XC9500XL-only Optimization:
                Skip the waitTime() if plvTdoMask->val[0:plvTdoMask->len-1]
                is NOT all zeros and sMatch==1.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U		iErrorCode;
    INT16U		iMismatch;
    INT16U		ucRepeat;
    INT16U		iExitShift;

    iErrorCode  = XSVF_ERROR_NONE;
    iMismatch   = 0;
    ucRepeat    = 0;
    iExitShift  = ( ucStartState != ucEndState );

    //XSVFDBG_PRINTF1(2, "   Shift Length = %ld\r\n", lNumBits );
    //XSVFDBG_PRINTF(2, "    TDI          = ");
    //XSVFDBG_PRINTLENVAL( 2, plvTdi );
    //XSVFDBG_PRINTF(2,"\r\n");
    //XSVFDBG_PRINTF(2, "    TDO Expected = ");
    //XSVFDBG_PRINTLENVAL( 2, plvTdoExpected );
    //XSVFDBG_PRINTF(2,"\r\n");

    if ( !lNumBits )
    {
        /* Compatibility with XSVF2.00:  XSDR 0 = no shift, but wait in RTI */
        if ( lRunTestTime )
        {
            /* Wait for prespecified XRUNTEST time */
            xsvfGotoTapState( pucTapState, XTAPSTATE_RUNTEST );
            //XSVFDBG_PRINTF1(2, "   Wait = %ld usec\r\n", lRunTestTime );
            waitTime( lRunTestTime );
        }
    }
    else
    {
        do
        {
            /* Goto Shift-DR or Shift-IR */
            xsvfGotoTapState( pucTapState, ucStartState );

            /* Shift TDI and capture TDO */
            xsvfShiftOnly( lNumBits, plvTdi, plvTdoCaptured, iExitShift );

            if ( plvTdoExpected && (xsv.mode != checksum_mode))
            {
                /* Compare TDO data to expected TDO data */
                iMismatch   = !EqualLenVal( plvTdoExpected,
                                            plvTdoCaptured,
                                            plvTdoMask );
            }

            if ( iExitShift )
            {
                /* Update TAP state:  Shift->Exit */
                ++(*pucTapState);
                //XSVFDBG_PRINTF1(1,"   TAP State = %s\n", xsvf_pzTapState[ *pucTapState ] );

                if ( iMismatch && lRunTestTime && ( ucRepeat < ucMaxRepeat ) )
                {
                    //XSVFDBG_PRINTF(3, "    TDO Expected = ");
                    //XSVFDBG_PRINTLENVAL( 3, plvTdoExpected );
                    //XSVFDBG_PRINTF(3, "\n");
                    //XSVFDBG_PRINTF(3, "    TDO Captured = ");
                    //XSVFDBG_PRINTLENVAL( 3, plvTdoCaptured );
                    //XSVFDBG_PRINTF(3, "\n");
                    //XSVFDBG_PRINTF(3, "    TDO Mask     = ");
                    //XSVFDBG_PRINTLENVAL( 3, plvTdoMask );
                    //XSVFDBG_PRINTF(3, "\n");
                    //XSVFDBG_PRINTF1(3, "   Retry #%d\n", ( ucRepeat + 1 ) );
                    /* Do exception handling retry - ShiftDR only */
                    xsvfGotoTapState( pucTapState, XTAPSTATE_PAUSEDR );
						  /* Shift 1 extra bit */
                    xsvfGotoTapState( pucTapState, XTAPSTATE_SHIFTDR );
                    /* Increment RUNTEST time by an additional 25% */
                    lRunTestTime    += ( lRunTestTime >> 2 );
                }
                else
                {
                    /* Do normal exit from Shift-XR */
                    xsvfGotoTapState( pucTapState, ucEndState );
                }

                if ( lRunTestTime )
                {
                    /* Wait for prespecified XRUNTEST time */
                    xsvfGotoTapState( pucTapState, XTAPSTATE_RUNTEST );
                    //XSVFDBG_PRINTF1(1, "   Wait = %ld usec\n", lRunTestTime );
                    waitTime( lRunTestTime );
                }
            }
        } while ( (xsv.mode != infra_mode && xsv.mode != inter_mode) && iMismatch && ( ucRepeat++ < ucMaxRepeat ) );
    }

    // SPTEST
    //write_tdo_debug(debug_file, plvTdoMask, plvTdoExpected, plvTdoCaptured);

    // To create the XOR mask it is necessary to go through all the vectors, even if there are no errors
    if ( ((xsv.mode == inter_mode) || (xsv.mode == checksum_mode))
         && (xsvfTestInfo.interVectorNumber >= 1) )
    // Start in Vector 1 (when the actual test starts)
    {
        if (findDataShiftErrors( plvTdoExpected, plvTdoCaptured, plvTdoMask, plvXOR, plvPrevCap ) < 0)
	{
            XSVFDBG_PRINTF( 3, "Error analyzing errors\r\n");
	}
    }

    if ( xsv.mode != id_mode && xsv.mode != sig_mode && iMismatch )
    {
        if (xsv.mode == infra_mode)
        {
            if (findDataShiftErrors( plvTdoExpected, plvTdoCaptured, plvTdoMask, plvXOR, plvPrevCap ) < 0)
	    {
                XSVFDBG_PRINTF( 3, "Error analyzing errors\r\n");
	    }
        }

        //XSVFDBG_PRINTF( 1, " TDO Expected = ");
        //XSVFDBG_PRINTLENVAL( 1, plvTdoExpected );
        //XSVFDBG_PRINTF( 1, "\r\n");
        //XSVFDBG_PRINTF( 1, " TDO Captured = ");
        //XSVFDBG_PRINTLENVAL( 1, plvTdoCaptured );
        //XSVFDBG_PRINTF( 1, "\r\n");
        //TEST //XSVFDBG_PRINTF( 1, " TDO Mask     = ");
        ////XSVFDBG_PRINTLENVAL( 1, plvTdoMask );
        ////XSVFDBG_PRINTF( 1, "\n");
        if ( ucMaxRepeat && ( ucRepeat > ucMaxRepeat ) )
        {
            iErrorCode  = XSVF_ERROR_MAXRETRIES;
        }
        else
        {
            iErrorCode  = XSVF_ERROR_TDOMISMATCH;
        }

        // Proceed with the test

        if ( (xsv.mode == inter_mode) || (xsv.mode == infra_mode) )
            iErrorCode = XSVF_ERROR_NONE;
    }

    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfBasicXSDRTDO( INT8U*    pucTapState,
                      long              lShiftLengthBits,
                      INT16S            sShiftLengthBytes,
                      lenVal*           plvTdi,
                      lenVal*           plvTdoCaptured,
                      lenVal*           plvTdoExpected,
                      lenVal*           plvTdoMask,
                      lenVal*           plvXOR,
                      lenVal*           plvPrevCap,
                      INT8U		ucEndState,
                      long              lRunTestTime,
                      INT8U     	ucMaxRepeat )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfBasicXSDRTDO
  Description:  Get the XSDRTDO parameters and execute the XSDRTDO command.
                This is the common function for all XSDRTDO commands.
  Parameters:   pucTapState         - Current TAP state.
                lShiftLengthBits    - number of bits to shift.
                sShiftLengthBytes   - number of bytes to read.
                plvTdi              - ptr to lenval for TDI data.
                lvTdoCaptured       - ptr to lenval for storing TDO data.
                iEndState           - state in which to end the shift.
                lRunTestTime        - amount of time to wait after the shift.
                ucMaxRepeat         - maximum xc9500/xl retries.
  Returns:      INT16U                 - 0 = success; otherwise TDO mismatch.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U*  pucVal;
    INT16S  byte_idx;

    readVal( plvTdi, sShiftLengthBytes );

    if ( plvTdoExpected )
    {
        readVal( plvTdoExpected, sShiftLengthBytes );
    }
	 return( xsvfShift( pucTapState, XTAPSTATE_SHIFTDR, lShiftLengthBits,
                       plvTdi, plvTdoCaptured, plvTdoExpected, plvTdoMask, plvXOR, plvPrevCap,
                       ucEndState, lRunTestTime, ucMaxRepeat ) );
}
/*---------------------------------------------------------------------------------------------------------*/
#ifdef  XSVF_SUPPORT_COMPRESSION
void xsvfDoSDRMasking( lenVal*  plvTdi,
                       lenVal*  plvNextData,
                       lenVal*  plvAddressMask,
                       lenVal*  plvDataMask )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoSDRMasking
  Description:  Update the data value with the next XSDRINC data and address.
  Example:      dataVal=0x01ff, nextData=0xab, addressMask=0x0100,
                dataMask=0x00ff, should set dataVal to 0x02ab
  Parameters:   plvTdi          - The current TDI value.
                plvNextData     - the next data value.
                plvAddressMask  - the address mask.
                plvDataMask     - the data mask.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16S  i;
    INT8U   ucTdi;
    INT8U   ucTdiMask;
    INT8U   ucDataMask;
    INT8U   ucNextData;
    INT8U   ucNextMask;
    INT16S    sNextData;

    /* add the address Mask to dataVal and return as a new dataVal */
    addVal( plvTdi, plvTdi, plvAddressMask );

    ucNextData  = 0;
    ucNextMask  = 0;
    sNextData   = plvNextData->len;
    for ( i = plvDataMask->len - 1; i >= 0; --i )
    {
		  /* Go through data mask in reverse order looking for mask (1) bits */
        ucDataMask  = plvDataMask->val[ i ];
        if ( ucDataMask )
        {
            /* Retrieve the corresponding TDI byte value */
            ucTdi       = plvTdi->val[ i ];

            /* For each bit in the data mask byte, look for 1's */
            ucTdiMask   = 1;
            while ( ucDataMask )
            {
                if ( ucDataMask & 1 )
                {
                    if ( !ucNextMask )
                    {
                        /* Get the next data byte */
								ucNextData  = plvNextData->val[ --sNextData ];
								ucNextMask  = 1;
                    }

                    /* Set or clear the data bit according to the next data */
                    if ( ucNextData & ucNextMask )
                    {
                        ucTdi   |= ucTdiMask;       /* Set bit */
                    }
                    else
                    {
                        ucTdi   &= ( ~ucTdiMask );  /* Clear bit */
                    }

                    /* Update the next data */
                    ucNextMask  <<= 1;
                }
                ucTdiMask   <<= 1;
                ucDataMask  >>= 1;
            }

            /* Update the TDI value */
				plvTdi->val[ i ]    = ucTdi;
        }
    }
}
#endif  /* XSVF_SUPPORT_COMPRESSION */

/***********************************************************************************************************\
    XSVF Command Functions (type = TXsvfDoCmdFuncPtr)
    These functions update pXsvfInfo->iErrorCode only on an error.
    Otherwise, the error code is left alone.
    The function returns the error code from the function.
\***********************************************************************************************************/

/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoIllegalCmd( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoIllegalCmd
  Description:  Function place holder for illegal/unsupported commands.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    //XSVFDBG_PRINTF2( 0, "ERROR:  Encountered unsupported command #%d (%s)\n",
    //XSVFDBG_PRINTF2( 0, "$2,Encountered unsupported command #%d (%s)\r\n",
    //                 ((INT16U)(pXsvfInfo->ucCommand)),
    //                 ((pXsvfInfo->ucCommand < XLASTCMD)
    //                  ? (xsvf_pzCommandName[pXsvfInfo->ucCommand])
    //                  : "Unknown") );
    pXsvfInfo->iErrorCode   = XSVF_ERROR_ILLEGALCMD;
    return( pXsvfInfo->iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXCOMPLETE( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXCOMPLETE
  Description:  XCOMPLETE (no parameters)
                Update complete status for XSVF player.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    pXsvfInfo->ucComplete   = 1;

    if ( (xsv.mode == inter_mode) || (xsv.mode == infra_mode) )
    {
        resultsSummary  ( &(pXsvfInfo->lvXOR), &(pXsvfInfo->lvTdoCaptured) );
    }

    return( XSVF_ERROR_NONE );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXTDOMASK( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXTDOMASK
  Description:  XTDOMASK <lenVal.TdoMask[XSDRSIZE]>
                Prespecify the TDO compare mask.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    readVal( &(pXsvfInfo->lvTdoMask), pXsvfInfo->sShiftLengthBytes );
	 //XSVFDBG_PRINTF(1,"    TDO Mask     = ");
    //XSVFDBG_PRINTLENVAL( 4, &(pXsvfInfo->lvTdoMask) );
    //XSVFDBG_PRINTF(1,"\r\n");
    return( XSVF_ERROR_NONE );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSIR( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSIR
  Description:  XSIR <(byte)shiftlen> <lenVal.TDI[shiftlen]>
                Get the instruction and shift the instruction into the TAP.
                If prespecified XRUNTEST!=0, goto RUNTEST and wait after
                the shift for XRUNTEST usec.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U   ucShiftIrBits;
    INT16S   sShiftIrBytes;
    INT16U   iErrorCode;

    /* Get the shift length and store */
    readByte( &ucShiftIrBits );
	 sShiftIrBytes   = xsvfGetAsNumBytes( ucShiftIrBits );
    //XSVFDBG_PRINTF1(1,"   XSIR length = %d\n",
    //                ((INT16U)ucShiftIrBits) );

    if ( sShiftIrBytes > LENVAL_MAX )
    {
        iErrorCode  = XSVF_ERROR_DATAOVERFLOW;
    }
    else
    {
        /* Get and store instruction to shift in */
        readVal( &(pXsvfInfo->lvTdi), xsvfGetAsNumBytes( ucShiftIrBits ) );

        /* Shift the data */
        iErrorCode  = xsvfShift( &(pXsvfInfo->ucTapState), XTAPSTATE_SHIFTIR,
				    ucShiftIrBits, &(pXsvfInfo->lvTdi),
				    /*plvTdoCaptured*/0, /*plvTdoExpected*/0,
				    /*plvTdoMask*/0, /*plvXOR*/0, /*plvPrevCap*/0, pXsvfInfo->ucEndIR,
				    pXsvfInfo->lRunTestTime, /*ucMaxRepeat*/0 );
    }

    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSIR2( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSIR2
  Description:  XSIR <(2-byte)shiftlen> <lenVal.TDI[shiftlen]>
                Get the instruction and shift the instruction into the TAP.
                If prespecified XRUNTEST!=0, goto RUNTEST and wait after
                the shift for XRUNTEST usec.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    long            lShiftIrBits;
    INT16S           sShiftIrBytes;
    INT16U             iErrorCode;

    /* Get the shift length and store */
    readVal( &(pXsvfInfo->lvTdi), 2 );
    lShiftIrBits    = value( &(pXsvfInfo->lvTdi) );
    sShiftIrBytes   = xsvfGetAsNumBytes( lShiftIrBits );
    //XSVFDBG_PRINTF1(1,"   XSIR2 length = %d\n", lShiftIrBits);

    if ( sShiftIrBytes > LENVAL_MAX )
    {
        iErrorCode  = XSVF_ERROR_DATAOVERFLOW;
    }
	 else
    {
	/* Get and store instruction to shift in */
	readVal( &(pXsvfInfo->lvTdi), xsvfGetAsNumBytes( lShiftIrBits ) );

	/* Shift the data */
	iErrorCode  = xsvfShift( &(pXsvfInfo->ucTapState), XTAPSTATE_SHIFTIR,
				lShiftIrBits, &(pXsvfInfo->lvTdi),
				/*plvTdoCaptured*/0, /*plvTdoExpected*/0,
				/*plvTdoMask*/0, /*plvXOR*/0, /*plvPrevCap*/0, pXsvfInfo->ucEndIR,
				pXsvfInfo->lRunTestTime, /*ucMaxRepeat*/0 );
    }

    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSDR( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSDR
  Description:  XSDR <lenVal.TDI[XSDRSIZE]>
                Shift the given TDI data into the JTAG scan chain.
                Compare the captured TDO with the expected TDO from the
                previous XSDRTDO command using the previously specified
                XTDOMASK.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U iErrorCode;
    readVal( &(pXsvfInfo->lvTdi), pXsvfInfo->sShiftLengthBytes );
    /* use TDOExpected from last XSDRTDO instruction */
    iErrorCode  = xsvfShift( &(pXsvfInfo->ucTapState), XTAPSTATE_SHIFTDR,
                             pXsvfInfo->lShiftLengthBits, &(pXsvfInfo->lvTdi),
                             &(pXsvfInfo->lvTdoCaptured),
                             &(pXsvfInfo->lvTdoExpected),
                             &(pXsvfInfo->lvTdoMask), &(pXsvfInfo->lvXOR), &(pXsvfInfo->lvPrevCap), pXsvfInfo->ucEndDR,
                             pXsvfInfo->lRunTestTime, pXsvfInfo->ucMaxRepeat );
    if ( iErrorCode != XSVF_ERROR_NONE )
	 {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXRUNTEST( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXRUNTEST
  Description:  XRUNTEST <uint32>
                Prespecify the XRUNTEST wait time for shift operations.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    readVal( &(pXsvfInfo->lvTdi), 4 );
    pXsvfInfo->lRunTestTime = value( &(pXsvfInfo->lvTdi) );
    //XSVFDBG_PRINTF1(1,"   XRUNTEST = %ld\r\n", pXsvfInfo->lRunTestTime );
    return( XSVF_ERROR_NONE );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXREPEAT( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXREPEAT
  Description:  XREPEAT <byte>
                Prespecify the maximum number of XC9500/XL retries.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    readByte( &(pXsvfInfo->ucMaxRepeat) );
    //XSVFDBG_PRINTF1(1,"   XREPEAT = %d\r\n",
    //                 ((INT16U)(pXsvfInfo->ucMaxRepeat)) );
    return( XSVF_ERROR_NONE );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSDRSIZE( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSDRSIZE
  Description:  XSDRSIZE <uint32>
                Prespecify the XRUNTEST wait time for shift operations.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U iErrorCode;
    iErrorCode  = XSVF_ERROR_NONE;
    readVal( &(pXsvfInfo->lvTdi), 4 );
    pXsvfInfo->lShiftLengthBits = value( &(pXsvfInfo->lvTdi) );
    pXsvfInfo->sShiftLengthBytes= xsvfGetAsNumBytes( pXsvfInfo->lShiftLengthBits );
    //XSVFDBG_PRINTF1(1,"   XSDRSIZE = %ld\r\n", pXsvfInfo->lShiftLengthBits );
    if ( pXsvfInfo->sShiftLengthBytes > LENVAL_MAX )
    {
        iErrorCode  = XSVF_ERROR_DATAOVERFLOW;
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSDRTDO( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSDRTDO
  Description:  XSDRTDO <lenVal.TDI[XSDRSIZE]> <lenVal.TDO[XSDRSIZE]>
                Get the TDI and expected TDO values.  Then, shift.
                Compare the expected TDO with the captured TDO using the
                prespecified XTDOMASK.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16S		byte;
    INT16U		chip = 0;
    INT16U		chipfound;
    INT16S		i;
    INT32U		id;
    INT16U		iErrorCode;
    static INT16U	ignore      = 0;
    INT32U		high, low;

    iErrorCode  = xsvfBasicXSDRTDO( &(pXsvfInfo->ucTapState),
                                    pXsvfInfo->lShiftLengthBits,
                                    pXsvfInfo->sShiftLengthBytes,
                                    &(pXsvfInfo->lvTdi),
                                    &(pXsvfInfo->lvTdoCaptured),
                                    &(pXsvfInfo->lvTdoExpected),
                                    &(pXsvfInfo->lvTdoMask),
                                    &(pXsvfInfo->lvXOR),
                                    &(pXsvfInfo->lvPrevCap),
                                    pXsvfInfo->ucEndDR,
                                    pXsvfInfo->lRunTestTime,
                                    pXsvfInfo->ucMaxRepeat );

    if((xsv.mode == id_mode || xsv.mode == sig_mode) && pXsvfInfo->lvTdoCaptured.len >= 4)
    {
        // Check whether to ignore this value
        // IDs are read twice, so ignore every other one

//        if(xsv.mode == id_mode)
//        {
//            if(ignore)
//            {
//                ignore = 0;
//                return( iErrorCode );
//            }
//            else
//            {
//                ignore = 1;
//            }
//        }
//        else // sig_mode (ignore becomes active-low!)
//        {
//            if(!ignore)
//            {
//                ignore = 1;
//                return( iErrorCode );
//            }
//            else
//            {
//                ignore = 0;
//            }
//        }

        // Construct low long word

        for(i = (pXsvfInfo->lvTdoCaptured.len - 1), low = 0 ; i >= (pXsvfInfo->lvTdoCaptured.len - 4) && i >= 0 ; i--)
        {
            low += (long)pXsvfInfo->lvTdoCaptured.val[i] << (((pXsvfInfo->lvTdoCaptured.len - 1) - i) * 8);
        }

        // Construct high long word

        for(i = (pXsvfInfo->lvTdoCaptured.len - 5), high = 0 ; i >= 0 ; i--)
        {
            high += (long)pXsvfInfo->lvTdoCaptured.val[i] << (((pXsvfInfo->lvTdoCaptured.len - 5) - i) * 8);
        }

        // Find chip number from mask

        for(byte = (pXsvfInfo->lvTdoMask.len - 1), chipfound = 0; byte >= 0 && !chipfound ; byte--)
        {
            for(i = 0 ; i < 8 && !chipfound ; i++)
            {
                if(pXsvfInfo->lvTdoMask.val[byte] & (1 << i))
                {
                    chip        = i + (((pXsvfInfo->lvTdoCaptured.len - 1) - byte) * 8);
                    chipfound   = 1;
                }
            }
        }

        // Shift words according to chip number

        low  >>= chip;
        high <<= (32 - chip);

        // Construct id from low and high words

        id = low | high;

	if(xsv.id_index < chain_type.nb)		// if space in the buffer, fill it
	{
	    if(xsv.mode == id_mode)
	    {
		// Check which Xilinx type the ID corresponds to and store result in IDs array

		switch(id)
		{
		    case XILINX36:
			xsv.ids[xsv.id_index++] = 1;
			break;

		    case XILINX72:
			xsv.ids[xsv.id_index++] = 2;
			break;

		    case XILINX108:
			xsv.ids[xsv.id_index++] = 3;
			break;

		    case XILINX144:
			xsv.ids[xsv.id_index++] = 4;
			break;

		    case XILINX18V:
		    case XILINX18V01:
		    case XILINX18V01_0:
			xsv.ids[xsv.id_index++] = 5;
			break;

		    default:
			printf("Unknown Xilinx ID 0x%08lX\r\n", id);
			xsv.ids[xsv.id_index++] = 0;
			break;
		}
	    }
	    else
	    {
		if(xsv.mode == sig_mode)
		{
		    xsv.ids[xsv.id_index++] = id;
		}
	    }
	}
    }

    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
#ifdef  XSVF_SUPPORT_COMPRESSION
INT16U xsvfDoXSETSDRMASKS( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSETSDRMASKS
  Description:  XSETSDRMASKS <lenVal.AddressMask[XSDRSIZE]>
                             <lenVal.DataMask[XSDRSIZE]>
                Get the prespecified address and data mask for the XSDRINC
                command.
                Used for xc9500/xl compressed XSVF data.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* read the addressMask */
    readVal( &(pXsvfInfo->lvAddressMask), pXsvfInfo->sShiftLengthBytes );
    /* read the dataMask    */
    readVal( &(pXsvfInfo->lvDataMask), pXsvfInfo->sShiftLengthBytes );

    //XSVFDBG_PRINTF(1,"    Address Mask = " );
    //XSVFDBG_PRINTLENVAL( 4, &(pXsvfInfo->lvAddressMask) );
    //XSVFDBG_PRINTF(1,"\n" );
    //XSVFDBG_PRINTF(1,"    Data Mask    = " );
	 //XSVFDBG_PRINTLENVAL( 4, &(pXsvfInfo->lvDataMask) );
	 //XSVFDBG_PRINTF(1,"\n" );

    return( XSVF_ERROR_NONE );
}
#endif  /* XSVF_SUPPORT_COMPRESSION */
/*---------------------------------------------------------------------------------------------------------*/
#ifdef  XSVF_SUPPORT_COMPRESSION
INT16U xsvfDoXSDRINC( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSDRINC
  Description:  XSDRINC <lenVal.firstTDI[XSDRSIZE]> <byte(numTimes)>
                        <lenVal.data[XSETSDRMASKS.dataMask.len]> ...
                Get the XSDRINC parameters and execute the XSDRINC command.
                XSDRINC starts by loading the first TDI shift value.
                Then, for numTimes, XSDRINC gets the next piece of data,
                replaces the bits from the starting TDI as defined by the
                XSETSDRMASKS.dataMask, adds the address mask from
                XSETSDRMASKS.addressMask, shifts the new TDI value,
                and compares the TDO to the expected TDO from the previous
                XSDRTDO command using the XTDOMASK.
                Used for xc9500/xl compressed XSVF data.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U   iErrorCode;
    INT16S   iDataMaskLen;
    INT8U    ucDataMask;
    INT8U    ucNumTimes;
    INT16S   i;

    readVal( &(pXsvfInfo->lvTdi), pXsvfInfo->sShiftLengthBytes );
    iErrorCode  = xsvfShift( &(pXsvfInfo->ucTapState), XTAPSTATE_SHIFTDR,
                             pXsvfInfo->lShiftLengthBits,
                             &(pXsvfInfo->lvTdi), &(pXsvfInfo->lvTdoCaptured),
                             &(pXsvfInfo->lvTdoExpected),
                             &(pXsvfInfo->lvTdoMask), &(pXsvfInfo->lvXOR), &(pXsvfInfo->lvPrevCap), pXsvfInfo->ucEndDR,
                             pXsvfInfo->lRunTestTime, pXsvfInfo->ucMaxRepeat );
    if ( !iErrorCode )
    {
        /* Calculate number of data mask bits */
        iDataMaskLen    = 0;
        for ( i = 0; i < pXsvfInfo->lvDataMask.len; ++i )
        {
	    ucDataMask  = pXsvfInfo->lvDataMask.val[ i ];
            while ( ucDataMask )
            {
                iDataMaskLen    += ( ucDataMask & 1 );
                ucDataMask      >>= 1;
            }
        }

        /* Get the number of data pieces, i.e. number of times to shift */
        readByte( &ucNumTimes );

        /* For numTimes, get data, fix TDI, and shift */
        for ( i = 0; !iErrorCode && ( i < ucNumTimes ); ++i )
        {
            readVal( &(pXsvfInfo->lvNextData),
                     xsvfGetAsNumBytes( iDataMaskLen ) );
				xsvfDoSDRMasking( &(pXsvfInfo->lvTdi),
                              &(pXsvfInfo->lvNextData),
                              &(pXsvfInfo->lvAddressMask),
                              &(pXsvfInfo->lvDataMask) );
            iErrorCode  = xsvfShift( &(pXsvfInfo->ucTapState),
                                     XTAPSTATE_SHIFTDR,
                                     pXsvfInfo->lShiftLengthBits,
                                     &(pXsvfInfo->lvTdi),
                                     &(pXsvfInfo->lvTdoCaptured),
                                     &(pXsvfInfo->lvTdoExpected),
                                     &(pXsvfInfo->lvTdoMask),
                                     &(pXsvfInfo->lvXOR),
                                     &(pXsvfInfo->lvPrevCap),
                                     pXsvfInfo->ucEndDR,
                                     pXsvfInfo->lRunTestTime,
                                     pXsvfInfo->ucMaxRepeat );
        }
	 }
    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
#endif  /* XSVF_SUPPORT_COMPRESSION */
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSDRBCE( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSDRBCE
  Description:  XSDRB/XSDRC/XSDRE <lenVal.TDI[XSDRSIZE]>
                If not already in SHIFTDR, goto SHIFTDR.
                Shift the given TDI data into the JTAG scan chain.
                Ignore TDO.
                If cmd==XSDRE, then goto ENDDR.  Otherwise, stay in ShiftDR.
                XSDRB, XSDRC, and XSDRE are the same implementation.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U   ucEndDR;
    INT16U   iErrorCode;

    ucEndDR = (INT16U)(( pXsvfInfo->ucCommand == XSDRE ) ?
                                pXsvfInfo->ucEndDR : XTAPSTATE_SHIFTDR);
    iErrorCode  = xsvfBasicXSDRTDO( &(pXsvfInfo->ucTapState),
                                    pXsvfInfo->lShiftLengthBits,
                                    pXsvfInfo->sShiftLengthBytes,
                                    &(pXsvfInfo->lvTdi),
                                    /*plvTdoCaptured*/0, /*plvTdoExpected*/0,
                                    /*plvTdoMask*/0, /*plvXOR*/0, /*plvPrevCap*/0, ucEndDR,
                                    /*lRunTestTime*/0, /*ucMaxRepeat*/0 );
    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSDRTDOBCE( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSDRTDOBCE
  Description:  XSDRB/XSDRC/XSDRE <lenVal.TDI[XSDRSIZE]> <lenVal.TDO[XSDRSIZE]>
                If not already in SHIFTDR, goto SHIFTDR.
                Shift the given TDI data into the JTAG scan chain.
                Compare TDO, but do NOT use XTDOMASK.
                If cmd==XSDRTDOE, then goto ENDDR.  Otherwise, stay in ShiftDR.
                XSDRTDOB, XSDRTDOC, and XSDRTDOE are the same implementation.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U   ucEndDR;
    INT16U             iErrorCode;
    ucEndDR = (INT16U)(( pXsvfInfo->ucCommand == XSDRTDOE ) ?
                                pXsvfInfo->ucEndDR : XTAPSTATE_SHIFTDR);
    iErrorCode  = xsvfBasicXSDRTDO( &(pXsvfInfo->ucTapState),
                                    pXsvfInfo->lShiftLengthBits,
                                    pXsvfInfo->sShiftLengthBytes,
                                    &(pXsvfInfo->lvTdi),
                                    &(pXsvfInfo->lvTdoCaptured),
                                    &(pXsvfInfo->lvTdoExpected),
                                    /*plvTdoMask*/0, /*plvXOR*/0, /*plvPrevCap*/0, ucEndDR,
                                    /*lRunTestTime*/0, /*ucMaxRepeat*/0 );
    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXSTATE( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXSTATE
  Description:  XSTATE <byte>
                <byte> == XTAPSTATE;
                Get the state parameter and transition the TAP to that state.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U   ucNextState;
    INT16U   iErrorCode;

    readByte( &ucNextState );
    iErrorCode  = xsvfGotoTapState( &(pXsvfInfo->ucTapState), ucNextState );
    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXENDXR( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXENDXR
  Description:  XENDIR/XENDDR <byte>
                <byte>:  0 = RUNTEST;  1 = PAUSE.
                Get the prespecified XENDIR or XENDDR.
                Both XENDIR and XENDDR use the same implementation.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  iErrorCode;
    INT8U   ucEndState;

    iErrorCode  = XSVF_ERROR_NONE;
    readByte( &ucEndState );

    if ( ( ucEndState != XENDXR_RUNTEST ) && ( ucEndState != XENDXR_PAUSE ) )
    {
        iErrorCode  = XSVF_ERROR_ILLEGALSTATE;
    }
    else
    {
    if ( pXsvfInfo->ucCommand == XENDIR )
    {
	if ( ucEndState == XENDXR_RUNTEST )
	{
	    pXsvfInfo->ucEndIR  = XTAPSTATE_RUNTEST;
			    }
	else
	{
	    pXsvfInfo->ucEndIR  = XTAPSTATE_PAUSEIR;
	}
	//XSVFDBG_PRINTF1(1,"   ENDIR State = %s\n",
	//		 xsvf_pzTapState[ pXsvfInfo->ucEndIR ] );
        }
	else    /* XENDDR */
	{
	    if ( ucEndState == XENDXR_RUNTEST )
	    {
		pXsvfInfo->ucEndDR  = XTAPSTATE_RUNTEST;
	    }
	    else
	    {
		 pXsvfInfo->ucEndDR  = XTAPSTATE_PAUSEDR;
	    }
	    //XSVFDBG_PRINTF1(1,"   ENDDR State = %s\n",
	    //			 xsvf_pzTapState[ pXsvfInfo->ucEndDR ] );
        }
    }

    if ( iErrorCode != XSVF_ERROR_NONE )
    {
        pXsvfInfo->iErrorCode   = iErrorCode;
    }
    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXCOMMENT( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXCOMMENT
  Description:  XCOMMENT <text string ending in \0>
                <text string ending in \0> == text comment;
                Arbitrary comment embedded in the XSVF.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Use the comment for debugging */
    /* Otherwise, read through the comment to the end '\0' and ignore */
    INT8U   ucText;

    if ( xsv_iDebugLevel > 0 )
    {
        putchar( ' ' );
    }
    do
    {
        readByte( &ucText );
        if ( xsv_iDebugLevel > 0 )
        {
	    putchar( ucText ? ucText : '\n' );
        }
    } while ( ucText );
    pXsvfInfo->iErrorCode   = XSVF_ERROR_NONE;
    return( pXsvfInfo->iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXWAIT( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXWAIT
  Description:  XWAIT <wait_state> <end_state> <wait_time>
                If not already in <wait_state>, then go to <wait_state>.
                Wait in <wait_state> for <wait_time> microseconds.
                Finally, if not already in <end_state>, then goto <end_state>.
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      INT16U         - 0 = success;  non-zero = error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U   ucWaitState;
    INT16U   ucEndState;
    long     lWaitTime;

    /* Get Parameters */
    /* <wait_state> */
    readVal( &(pXsvfInfo->lvTdi), 1 );
    ucWaitState = pXsvfInfo->lvTdi.val[0];

    /* <end_state> */
    readVal( &(pXsvfInfo->lvTdi), 1 );
    ucEndState = pXsvfInfo->lvTdi.val[0];

    /* <wait_time> */
    readVal( &(pXsvfInfo->lvTdi), 4 );
    lWaitTime = value( &(pXsvfInfo->lvTdi) );
    //XSVFDBG_PRINTF2( 3, "   XWAIT:  state = %s; time = %ld\n",
    //                 xsvf_pzTapState[ ucWaitState ], lWaitTime );

    /* If not already in <wait_state>, go to <wait_state> */
    if ( pXsvfInfo->ucTapState != ucWaitState )
    {
        xsvfGotoTapState( &(pXsvfInfo->ucTapState), ucWaitState );
    }

    /* Wait for <wait_time> microseconds */
    waitTime( lWaitTime );

    /* If not already in <end_state>, go to <end_state> */
    if ( pXsvfInfo->ucTapState != ucEndState )
    {
        xsvfGotoTapState( &(pXsvfInfo->ucTapState), ucEndState );
    }

    return( XSVF_ERROR_NONE );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfDoXCHECKSUM( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfDoXchecksum
  Description:  Dummy function - Program should not arrive there
                XCHECKSUM is performed in a loop in xsvRun()
  Parameters:   pXsvfInfo   - XSVF information pointer.
  Returns:      error.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(XSVF_ERROR_ILLEGALJUMP_CS);
}

/***********************************************************************************************************\
    Execution Control Functions
\***********************************************************************************************************/

/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfInitialize( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfInitialize
  Description:  Initialize the xsvf player.
                Call this before running the player to initialize the data
                in the SXsvfInfo struct.
                xsvfCleanup is called to clean up the data in SXsvfInfo
                after the XSVF is played.
  Parameters:   pXsvfInfo   - ptr to the XSVF information.
  Returns:      INT16U - 0 = success; otherwise error.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Initialize values */
    pXsvfInfo->iErrorCode   = xsvfInfoInit( pXsvfInfo );

    if ( !pXsvfInfo->iErrorCode )
    {
        /* Initialize the TAPs */
        pXsvfInfo->iErrorCode   = xsvfGotoTapState( &(pXsvfInfo->ucTapState),
                                                    XTAPSTATE_RESET );
    }
    return( pXsvfInfo->iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfRun( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfRun
  Description:  Run the xsvf player for a single command and return.
                First, call xsvfInitialize.
                Then, repeatedly call this function until an error is detected
                or until the pXsvfInfo->ucComplete variable is non-zero.
                Finally, call xsvfCleanup to cleanup any remnants.
  Parameters:   pXsvfInfo   - ptr to the XSVF information.
  Returns:      INT16U         - 0 = success; otherwise error.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U 	loop_size;
    INT16U	byte_idx;
    INT16S	temp_len;

    /* Process the XSVF commands */
    if ( (!pXsvfInfo->iErrorCode) && (!pXsvfInfo->ucComplete) )
    {
	/* read 1 byte for the instruction */
	readByte( &(pXsvfInfo->ucCommand) );
	++(pXsvfInfo->lCommandCount);

        if ( pXsvfInfo->ucCommand < XLASTCMD )
        {
            /* Execute the command.  Func sets error code. */

            /* We must count the vector number during an Interconnection test.
                    In a typical interconnection test the order of XSIR and XSDRTDO is:

                            XSIR
                            XSDRTDO
                            XSIR
                            XSDRTDO - Vector 1
                            XSDRTDO - Vector 2
                            XSDRTDO - Vector 3
                            XSDRTDO - Vector 4
                            XSDRTDO - Vector 5
                            XSDRTDO - Vector 6
                                    . . .

	     	for checksum files:
			    XSIR
			    XSDRTDO
			    XSIR
			    XCHECKSUM
			    Val3
			    Val2
			    Val1
			    Val0
			    ...

            */
            if ( xsv.mode == inter_mode )
            {
		if ( !stricmp( xsvf_pzCommandName[pXsvfInfo->ucCommand], "XSDRTDO" ) )
		{
		    ++xsvfTestInfo.interVectorNumber;
		}
            }

	    /* If checksum command, loop on given number of TDO shifts (next 4 bytes) */
            if ( !stricmp( xsvf_pzCommandName[pXsvfInfo->ucCommand], "XCHECKSUM" ) )
	    {
		/* Get and store number of TDO shifts - 4 bytes - use Tdi vector as temp storage */
		temp_len = pXsvfInfo->lvTdi.len;
		pXsvfInfo->lvTdi.len = (INT16S)XSV_CS_LOOP_SIZE;

		readVal(&(pXsvfInfo->lvTdi), XSV_CS_LOOP_SIZE );
		loop_size = value(&(pXsvfInfo->lvTdi));
                pXsvfInfo->lvTdi.len = temp_len;	// restore real Tdi length

		/* Sets TdoExpected length */
		pXsvfInfo->lvTdoExpected.len = pXsvfInfo->lvTdi.len;

		/* when checksum cmd, write 0x00 in Tdi vector */
		for ( byte_idx = 0; byte_idx < pXsvfInfo->sShiftLengthBytes; byte_idx++)
		{
		    pXsvfInfo->lvTdi.val[byte_idx] = 0x00;
		}

		xsvfTestInfo.interVectorNumber = 0;
		/* Repeat XSDTDO loop_size times */
		for(;loop_size;loop_size--)
		{
		    ++xsvfTestInfo.interVectorNumber;
		    pXsvfInfo->iErrorCode  = xsvfShift(&(pXsvfInfo->ucTapState), XTAPSTATE_SHIFTDR,
							pXsvfInfo->lShiftLengthBits,
							&(pXsvfInfo->lvTdi),
							&(pXsvfInfo->lvTdoCaptured),
							&(pXsvfInfo->lvTdoExpected),
							&(pXsvfInfo->lvTdoMask),
							&(pXsvfInfo->lvXOR),
							&(pXsvfInfo->lvPrevCap),
							pXsvfInfo->ucEndDR,
							pXsvfInfo->lRunTestTime,
							pXsvfInfo->ucMaxRepeat );
		    if(pXsvfInfo->iErrorCode)
		    {
		        return( pXsvfInfo->iErrorCode );
		    }
		}
	    }


            /* If your compiler cannot take this form,
               then convert to a switch statement */
            xsvf_pfDoCmd[ pXsvfInfo->ucCommand ]( pXsvfInfo );

            if ( (xsv.mode == inter_mode) )
            {
		if ( !stricmp( xsvf_pzCommandName[pXsvfInfo->ucCommand], "XSDRTDO" ) )
		{
		    // If in stepping mode, wait for key press and display vector number

		    if(xsv.step_flag)
		    {
                        /* no step option yet */
		    }
		}
            }
        }
        else
        {
            /* Illegal command value.  Func sets error code. */
            xsvfDoIllegalCmd( pXsvfInfo );
        }
    }
    //XSVFDBG_PRINTF1(1,"CMD: %s\r\n", xsvf_pzCommandName[pXsvfInfo->ucCommand]);
    //XSVFDBG_PRINTF1(1,"- Err: %i\r\n",pXsvfInfo->iErrorCode);
    return( pXsvfInfo->iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*/
void xsvfCleanup( SXsvfInfo* pXsvfInfo )
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfCleanup
  Description:  cleanup remnants of the xsvf player.
  Parameters:   pXsvfInfo   - ptr to the XSVF information.
  Returns:      void.
\*---------------------------------------------------------------------------------------------------------*/
{
    xsvfInfoCleanup( pXsvfInfo );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U xsvfExecute(void)
/*---------------------------------------------------------------------------------------------------------*\
  Function:     xsvfExecute
  Description:  Process, interpret, and apply the XSVF commands.
                See port.c:readByte for source of XSVF data.
  Parameters:   none.
  Returns:      INT16U - Legacy result values:  1 == success;  0 == failed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U	c = 0;
    INT16U	char_index;
    INT16S	i;
    INT8U	sig[5];

    //static SXsvfInfo	xsvfInfo;

    xsvfInitialize( &xsvfInfo );

    while ( !xsvfInfo.iErrorCode && (!xsvfInfo.ucComplete) )
    {
        xsvfRun( &xsvfInfo );
    }
    if ( xsvfInfo.iErrorCode)
    {
	//XSVFDBG_PRINTF3( 1, "CMD: %s - Err: %u - XSV Byte: %u\r\n", xsvf_pzCommandName[xsvfInfo.ucCommand]
	//							,xsvfInfo.iErrorCode, xsv.bytes_read);
        printf("$2,%s\r\n", xsvf_pzErrorName[ ( xsvfInfo.iErrorCode < XSVF_ERROR_LAST ) ? xsvfInfo.iErrorCode : XSVF_ERROR_UNKNOWN ] );
    }
    else
    {
        switch(xsv.mode)
        {
            case id_mode:
            case sig_mode:

            //printf("%s", "$1");
	    printf("signature:");

            for(i = 0 ; i < xsv.id_index ; i++)
            {
                if(xsv.mode == id_mode)
                {
                    printf(",%i", xsv.ids[i] );
                }
                else // sig_mode
                {
                    // Extract signature

                    for(char_index = 0 ; char_index < 4 ; char_index++)
                    {
                        c = ((xsv.ids[i] >> (char_index * 8)) & 0xFF);

                        if(c < '0' || c > 'z')
                        {
                            c = ' ';
                            break;
                        }

                        sig[3 - char_index] = c;
                    }

                    if(c != ' ')
                    {
                        sig[4] = '\0';

                        printf(",%s", sig );
                    }
                    else
                    {
                        printf(",%c", '?' );
                    }
                }
            }

            if(xsv.id_index == 0)
            {
                printf(",");
            }

            printf("\r\n" );
            break;

            case infra_mode:
            case inter_mode:

                if(bscan_error_count)
                {
                    printf("$2,%u bscan errors\r\n", bscan_error_count );
                }
                else
                {
                    printf("$1\r\n" );
                }
                break;

            case checksum_mode:
	    	printf("$1,");
		xsvfInfo.lvXOR.len = 4;
		xsvfPrintLenVal(&(xsvfInfo.lvXOR));
		printf("\r\n");

	    default:
                ////XSVFDBG_PRINTF( 0, "SUCCESS - Completed XSVF execution.\n" );
                printf("$1\r\n" );
                break;
        }
    }

    xsvfCleanup( &xsvfInfo );

    return( XSVF_ERRORCODE(xsvfInfo.iErrorCode) );
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U jtagRun(INT16U* file)
/*---------------------------------------------------------------------------------------------------------*\
  Function:	jtagRun
  Description:  jtag function.
                Specified here for creating stand-alone debug executable.
                Embedded users should call xsvfExecute() directly.
  Parameters:   iArgc    - number of command-line arguments.
                ppzArgv  - array of ptrs to strings (command-line arguments).
  Returns:      INT16U   - Legacy return value
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U	*c;
    INT16U	card_set_flag;
    INT16U	xda_set_flag;
    INT16U	iErrorCode;
    INT16S	i;

    //clock_t     startClock;
    //clock_t     endClock;

    iErrorCode          = XSVF_ERRORCODE( XSVF_ERROR_NONE );
    xsv.bytes_read      = 0;
    card_set_flag   	= 0;
    xda_set_flag    	= 0;

    /* Debug level display */
    xsv_iDebugLevel    = 0;

    /* Step by step option */
    xsv.step_flag = 0;

    /* get file size, type and start_addr */

    //xsv.size	= ((INT32U)*file) && (((INT32U)*file++) << 16);         // little endian
    xsv.size	= *(INT32U*)file;

    if(!xsv.size)		// if file size = 0
    {
	return(XSVF_NO_FILE);
    }

    file 	+= 2;							// skip size
    xsv.data 	= (INT8U *)file;

    if ( (xsv.mode == inter_mode) || (xsv.mode == infra_mode) )
    {
	//XSVFDBG_PRINTF (4, "Initializing test data structure ..\n");
	if (initializeXSVFtestInfo() < 0)
	{
	    //XSVFDBG_PRINTF (4, "Error initializing test data structure\n");
	    printf("$2,Error initializing test data structure\n");
	    return (-1);
	}
	else
	{
	    //XSVFDBG_PRINTF (4, "OK\n");
	}

	/* Initialize error struct */
	bscan_error_count = 0;
	bscan_err[0].pos  = 0;
    }

    /* Initialize the I/O */
    setPort( TMS, 1 );

    /* init history fail index */
    bscan_history_idx = 0;

    /* Execute the XSVF in the file */
    //startClock  = clock();
    iErrorCode  = xsvfExecute();
    //endClock    = clock();

    zero_outputs();

    return( iErrorCode );
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: jtag_seq.c
\*---------------------------------------------------------------------------------------------------------*/
