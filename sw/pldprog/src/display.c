/*---------------------------------------------------------------------------------------------------------*\
  File:		flashdisplay
  Contents:	FGC2 PLD porgrammer display functions

  Notes:	The component displays 4 ascii characters.

  History:

    16/04/07	pfr	Created
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>
#include <platforms\pldprog\memmap.h>

/*---------------------------------------------------------------------------------------------------------*/
void display(INT8U position, INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function prints the given character at the given position.
\*---------------------------------------------------------------------------------------------------------*/
{
    if((ch > 0x60) && (ch < 0x7B))          // miniscule to capital
    {
        ch -= 0x20;
    }

    /* If character is not printable by dislay */
    if( (ch < ' ') || (ch > '_') )
    {
	MenuRspError("Char to display (%c) out of range", ch);
    }
    else
    {

    	if(displaySelPos(position))
	{
	    return;
	}

	DISP_WR_N  = 0x00;			// write enable
	USLEEP(1);				// 20 ns should be waited (Twd)
	DISP_BUS   = ch &0x7F;			// apply char to display
	USLEEP(1);				// 50 ns should be waited (Tah Tdh)
	DISP_WR_N  = 0x01;			// write disable
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void displayString(INT8U *s)
/*---------------------------------------------------------------------------------------------------------*\
  This function displays the first 4 chars of this string.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  idx = 0;

    /* display string - left justified */
    while(*s && (idx < 4))
    {
        dspl.chars[idx] = *s;                   // memorise chars
        display(idx++, *s++);                   // Dislay
    }

    while(idx < 4)                              // add spaces at the end of string
    {
        dspl.chars[idx] = ' ';                   // memorise chars
        display(idx++, ' ');                     // Dislay
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void displayErase(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases the display.
\*---------------------------------------------------------------------------------------------------------*/
{
    displayString("    ");
}
/*---------------------------------------------------------------------------------------------------------*/
void displayBlank(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function clears the display but not the memory (uses for flashing).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  idx = 4;
    INT16U  mask;

    mask = dspl.flash;

    /* display string - right justified */
    while(idx--)
    {
        if(mask & 0x08)
	{
	    display(idx,' ');	// display
	}
	mask <<= 1;		// shift mask
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U displaySelPos(INT8U pos)
/*---------------------------------------------------------------------------------------------------------*\
  This function selects the position.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(pos > 3)
    {
	MenuRspError("Wrong display position %i", pos);
	return(STAT_ERR);
    }
    else
    {
        pos = 3 - pos;                       // Invert positions to be the same as in a string
	p9  = (p9 & 0xF9) | (pos << 1);
	return(STAT_PASS);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void displayProgress(INT8U op, INT16U block)
/*---------------------------------------------------------------------------------------------------------*\
  This function displays progress.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U  ch;

    dspl.chars[0] = op;
    display(0,op);
    ch = (block>>4);
    if(ch<=9)
    {
        ch += 0x30;             // 0-9
    }
    else
    {
        ch += 0x37;             // A-F
    }
    display(1,ch);
    dspl.chars[1] = ch;

    ch = block & 0xF;
    if(ch<=9)
    {
        ch += 0x30;             // 0-9
    }
    else
    {
        ch += 0x37;             // A-F
    }
    display(2,ch);
    dspl.chars[2] = ch;
}
/*---------------------------------------------------------------------------------------------------------*/
void displayDwnld(INT16U percent)
/*---------------------------------------------------------------------------------------------------------*\
  This function displays download progress.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  digit;
    INT8U   ch;
    static  INT8U  alive_toggle;

    dspl.chars[0] = 'D';
    display(0,'D');

    /* if more than 100% */
    if(percent >= 100)
    {
	dspl.chars[1] = ' ';
	display(1,' ');
	dspl.chars[2] = 'O';
	display(2,'O');
	dspl.chars[3] = 'K';
	display(3,'K');
    }
    else
    {
        /* toggle digit 1 to say I'm alive */
	alive_toggle = !alive_toggle;
        if(alive_toggle)
	{
	    dspl.chars[1] = ' ';
	    display(1,' ');
	}
	else
	{
	    dspl.chars[1] = '-';
	    display(1,'-');
	}

	/* dizaines */
	digit = percent/10;
	ch    = (INT8U)digit + 0x30;
	dspl.chars[2] = ch;
	display(2,ch);

	/* unites */
	digit = (INT16U)(percent - digit*10);
	ch    = (INT8U)digit + 0x30;
	dspl.chars[3] = ch;
	display(3,ch);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void displayJtagProgress(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function displays jtag operation progress.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  progress;
    INT16U  temp;

    /* position */
    if(dspl.pos < 100)
    {
	temp = '0' + dspl.pos/10;
	display(0,temp);
	dspl.chars[0] = temp;

	temp = '0' + (dspl.pos - (dspl.pos/10)*10);
	display(1,temp);
	dspl.chars[1] = temp;
    }

    /* operation */
    display(2, dspl.op);
    dspl.chars[2] = dspl.op;


    /* progress */
    progress = (INT16U)(((float)xsv.bytes_read/xsv.size)*10);
    if(progress < 10)
    {
        temp = '0' + progress;
	display(3,temp);
	dspl.chars[3] = temp;
    }
    else
    {
	display(3,'-');
	dspl.chars[3] = '-';
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: display.c
\*---------------------------------------------------------------------------------------------------------*/

