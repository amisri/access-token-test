/*---------------------------------------------------------------------------------------------------------*\
 File:		jtag.c

 Purpose:	Jtag TAP functions

 Author:	Philippe Fraboulet

 History:
    22/05/05	pfr	Created
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
INT16U jtagTap(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a risin edge is detected on TCK (falling edge is treated in interrpt function).
  It emulates the JTAG TAP controller.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* State machine */

    switch(jtag.state)
    {
    case JTAG_RESET :
    	jtagReset();
	jtag.state = (jtag.tms ? jtag.state : JTAG_IDLE);
	break;

    case JTAG_IDLE :
	//jtagIdle();
	jtag.state = (jtag.tms ? JTAG_SELECT_DR : jtag.state);
	break;

    case JTAG_SELECT_DR :
	jtagSelect();
	jtag.state = (jtag.tms ? JTAG_SELECT_IR : JTAG_CAPTURE_DR);
	break;

    case JTAG_CAPTURE_DR :
	jtagCapture();
	jtag.state = (jtag.tms ? JTAG_EXIT1_DR : JTAG_SHIFT_DR);
	break;

    case JTAG_SHIFT_DR :
	jtagShift();
	jtag.state = (jtag.tms ? JTAG_EXIT1_DR : jtag.state);
	break;

    case JTAG_EXIT1_DR :
	jtagExit();
	jtag.state = (jtag.tms ? JTAG_UPDATE_DR : JTAG_PAUSE_DR);
	break;

    case JTAG_PAUSE_DR :
	jtagPause();
	jtag.state = (jtag.tms ? JTAG_EXIT2_DR : jtag.state);
	break;

    case JTAG_EXIT2_DR :
	jtagExit();
	jtag.state = (jtag.tms ? JTAG_UPDATE_DR : JTAG_SHIFT_DR);
	break;

    case JTAG_UPDATE_DR :
	jtagUpdate();
	jtag.state = (jtag.tms ? JTAG_SELECT_DR : JTAG_IDLE);
	break;

    case JTAG_SELECT_IR :
	jtagSelect();
	jtag.state = (jtag.tms ? JTAG_SELECT_DR : JTAG_IDLE);
	break;

    case JTAG_CAPTURE_IR :
    case JTAG_SHIFT_IR :
    case JTAG_EXIT1_IR :
    case JTAG_PAUSE_IR :
    case JTAG_EXIT2_IR :
    case JTAG_UPDATE_IR :
    default :
    	break;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/
