/*---------------------------------------------------------------------------------------------------------*\
  File:		flash.c

  Contents:	FGC2 PLD porgrammer flash functions

  Notes:	On the FGC2 PLD Programmer, a flash memory of 16MBytes is available. From the C87 only
  		15MBytes are accessible (memory expansion mode). This area is from 0x10000 to 0xEFFFF.
  History:

    16/04/07	pfr	Created
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void flashEnable(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function enables the flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    FLASH_ENABLE_N = 0x00;
    USLEEP(1);
    FLASH_PROTECT_N = 0x01;
    USLEEP(1);
}
/*---------------------------------------------------------------------------------------------------------*/
void flashProtect(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function disables writing & erasing the flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    FLASH_PROTECT_N = 0x00;
    USLEEP(1);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U flashWrite(INT16U *address, INT16U data)
/*---------------------------------------------------------------------------------------------------------*\
  This function writes data into flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  timeout = 0;
    INT16U  status;

    *address = (INT16U)FLASH_WRITE_W;		// sends write command
    *address = data;				// write data - flash in read status mode

    for(;;)
    {
        USLEEP(1);                              // wait for 1us
	status = *address;
	if(status & 0x800)			// SR7 = 1, flash is ready
	{
            if(flashFullStatCheck(status))
            {
                return(1);
            }
            else
            {
                return(0);
            }
	}
	else
	{
            if(timeout++ >= FLASH_TIMEOUT_W)
	    {
		fprintf(TX_TERM, RSP_ERROR ", Writing timeout exceeded\r\n");
		return(1);
	    }
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void flashRead(INT16U *address, INT16U* data)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads data from flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    *address = FLASH_READ_ARRAY;		// go in read array mode
    *data    = *address;			// Get data

}
/*---------------------------------------------------------------------------------------------------------*/
INT16U flashEraseAll(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases all flash locations.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  block;
    INT32U  address;

    displayString("    ");			// clear display

    /* Erase flash */
    for(block = 0x00 ; block < 0xFF ; block += 2)
    {
        if(!block)				// skip 1st page (not accessible)
	{
	    block = 0x01;
	}

        address = (INT32U)block << 16;
        if(!flashNoAccess(address))             // if block accessible, erase
        {
            displayProgress('E',block);

            if(flashErase((INT16U*)address))             // if Erase failed
            {
                return(1);
            }
        }

	if(block == 0x01)			// remove 1 page to scan even pages
	{
            block = 0x00;
	}
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U flashErase(INT16U *block_addr)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases block in flash. Some blocks of the flash are not accessible in the C87 memory expansion
  mode : block 0, blocks 0xF0 - 0xFE (9 blocks on the whole).
  One block is 128 kbytes.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  timeout = 0;
    INT16U  status;
    INT16U  block_nb;

    block_addr = (INT16U*)((INT32U)block_addr & 0xFF0000);		// block address base

    if(flashNoAccess((INT32U)block_addr))
    {
        fprintf(TX_TERM, RSP_ERROR ", Flash Block address 0x%06lX not accessible by C87", (INT32U)block_addr);
        return(1);
    }

    *block_addr = FLASH_BLK_ERASE;		// sends erase command
    *block_addr = FLASH_ERASE_CFM;		// sends erase confirm command - flash in status read mode

    for(;;)
    {
        USLEEP(1000);
	status = *block_addr;
	if(status & 0x800)		// SR7 = 1, flash is ready
	{
            if(flashFullStatCheck(status))
            {
                return(1);
            }
            else
            {
                return(0);
            }
	}
	else
	{
            if(timeout++ >= FLASH_TIMEOUT_E)
	    {
		fprintf(TX_TERM, RSP_ERROR ", Erase timeout exceeded, Flash still busy");
                return(1);
	    }
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U flashNoAccess(INT32U address)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks wether address is in FLASH range.
  0x000000 - 0x00FFFF : zone not accessible by C87 expanded memory
  0xF00000 - 0xFFFFFF : zone not accessible by C87 expanded memory
\*---------------------------------------------------------------------------------------------------------*/
{
    return( (address < FLASH_START) || (address > FLASH_END));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U flashFullStatCheck(INT16U status)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the full status of the flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Put status bits in order */

    status = (status & 0x01) | (status & 0x04)>>1 | (status & 0x10)>>2 | (status & 0x40)>>3 | (status & 0x100)>>4 |
             (status & 0x8000)>>10 | (status & 0x2000)>>7 | (status & 0x800)>>4;

    if(status & 0x7E)
    {
	fprintf(TX_TERM, RSP_ERROR ", Status error: 0x%04X", status);
	return(1);
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U flashCrc(INT32U address, INT32U nb_bytes, INT16U *file_crc)
/*---------------------------------------------------------------------------------------------------------*\
  Perform CRC-CCITT calculation on the specified zone of words in flash. The resulting crc is the return
  value for the function.
  nb_bytes is the size of file plus its CRC. the function writes in file_crc ptr the file crc.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U *data_in;			// pointer to data for CRC input

    /* Set Flash in array mode */

    *(INT16U*)address = FLASH_READ_ARRAY;	// go in read array mode (write a 16b word)

    crcd     = 0x00;			// Reset & init CRC
    data_in  = (INT8U*)address;		// init pointer to data

    nb_bytes -= 2;			// remove file CRC from calculation
    while(nb_bytes--)
    {
	crcin = *data_in++;
    }

    *file_crc = (INT16U)*data_in++;		// save file CRC
    *file_crc |= ((INT16U)*data_in)<<8;		// save file CRC

    asm("NOP");
    asm("NOP");
    asm("NOP");

    return(crcd);			// CRCD needs 3 cycles to be updated
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: flash.c
\*---------------------------------------------------------------------------------------------------------*/

