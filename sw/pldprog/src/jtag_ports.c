/*---------------------------------------------------------------------------------------------------------*\
 File:		jtag_ports.c

 Purpose:	FGC3 boot

 Abstract:	This file contains the routines to
            	output values on the JTAG ports, to read
            	the TDO bit, and to read a byte of data
            	from the prom

 Author:	Philippe Fraboulet

 History:
    12/05/06	pfr	Adapted from xilinx player ports.c file
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

//static inPortType in_word;
//static outPortType out_word;
static INT16U base_port = 0x378;
static INT16U once = 0;


//#define DATA_OFFSET    (unsigned INT16S) 0
//#define STATUS_OFFSET  (unsigned INT16S) 1
//#define CONTROL_OFFSET (unsigned INT16S) 2

/*---------------------------------------------------------------------------------------------------------*/
void setPort(INT16S p,INT16S val)
/*---------------------------------------------------------------------------------------------------------*\
  This function sets JTAG port
\*---------------------------------------------------------------------------------------------------------*/
{
    if (once == 0)
    {
        out_word.bits.one = 1;
        out_word.bits.zero = 0;
        once = 1;
    }
    if (p==TMS)
    {
        out_word.bits.tms = (INT8U) val;
    }
    if (p==TDI)
    {
        out_word.bits.tdi = (INT8U) val;
    }
    if (p==TCK)
    {
        out_word.bits.tck = (INT8U) val;
	(void) portOutp( out_word.value );
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void pulseClock(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function toggles TCK LH
\*---------------------------------------------------------------------------------------------------------*/
{
     setPort(TCK,0);  /* set the TCK port to low  */
     setPort(TCK,1);  /* set the TCK port to high */
}
/*---------------------------------------------------------------------------------------------------------*/
void readByte(INT8U *data)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads in a byte of data from the prom
\*---------------------------------------------------------------------------------------------------------*/
{
        *data = *xsv.data++;
        xsv.bytes_read++;

        // Display progress (fewer steps for small files)

        if(xsv.size < 50000)
        {
            // Display progress in 10% steps

            if(!(xsv.bytes_read % (xsv.size / 10)))
            {
		printf("$0,%u\r\n", ((100 * xsv.bytes_read) / xsv.size));
		if(!POWER_CHECK)
		{
		    xsvfInfo.iErrorCode = XSVF_NO_POWER;
		    zero_outputs();
		}
            }
        }
        else
        {
            // Display progress in 1% steps

            if(!(xsv.bytes_read % (xsv.size / 100)))
            {
                    printf("$0,%u\r\n", ((100 * xsv.bytes_read) / xsv.size));

                    // Check whether DUT is powered (in case over-current protection has removed power)

                    //in_word.value = (INT8U)portInp((unsigned INT16S)(base_port + STATUS_OFFSET));

                    //if(!in_word.bits.power) // DUT is not powered
                    if(!POWER_CHECK)
		    {
                        xsvfInfo.iErrorCode = XSVF_NO_POWER;
                        zero_outputs();
                    }
            }
        }
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U readTDOBit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads the TDO bit from port
\*---------------------------------------------------------------------------------------------------------*/
{
     in_word.value = (INT8U) portInp();
     if (in_word.bits.tdo == 0x1) {
	      return( (INT8U) 1 );
     }
     return( (INT8U) 0 );
}
/*---------------------------------------------------------------------------------------------------------*/
void portOutp(INT8U value)
/*---------------------------------------------------------------------------------------------------------*\
  This function outputs JTAG value
\*---------------------------------------------------------------------------------------------------------*/
{
    /* value:     TDI: bit0 - TCK: bit1 - TMS: bit2 */
    /* JTAG_PORT: TDI: bit3 - TMS: bit4 - TCK: bit 5 */

    //p0 = (p0 & 0xE0) | (value & 0x07);
    JTAG_PORT_TCK = !!(value&0x02);			// TCK
    JTAG_PORT_TMS = !!(value&0x04);			// TMS
    JTAG_PORT_TDI = !!(value&0x01);			// TDI

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void zero_outputs(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function outputs zero on JTAG bus
\*---------------------------------------------------------------------------------------------------------*/
{
    (void)portOutp(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U portInp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets input JTAG value
\*---------------------------------------------------------------------------------------------------------*/
{
    return( ((JTAG_PORT & 0x40) >> 3) |		// TDO
    	    (0x10) );				// Power check
}
/*---------------------------------------------------------------------------------------------------------*/
void waitTime(long microsec)
/*---------------------------------------------------------------------------------------------------------*\
  Wait at least the specified number of microsec.
  Use a timer if possible; otherwise estimate the number of instructions
  necessary to be run based on the microcontroller speed.  For this example
  we pulse the TCK port a number of times based on the processor speed.
\*---------------------------------------------------------------------------------------------------------*/
{
    static INT32S         tckCyclesPerMicrosec    = 1;
    //long                tckCycles   = (microsec * tckCyclesPerMicrosec) / 1.625; // For 200MHz Pentium
    INT32S                tckCycles   = (microsec * tckCyclesPerMicrosec) / 2.3; // For 133Mhz AMD
    //long                tckCycles   = (microsec * tckCyclesPerMicrosec) * 5;
    //long                tckCycles   = microsec * tckCyclesPerMicrosec;
    INT32S                i;
    static INT16U progress_tick = 0;

    if(!POWER_CHECK) // DUT is not powered
    {
	xsvfInfo.iErrorCode = XSVF_NO_POWER;
	zero_outputs();
	exit(1);
    }
    /* For systems with TCK rates >= 1 MHz;  This implementation is fine. */

    for ( i = 0; i < tckCycles; ++i )
    {
	/* Display progress after every 50000 iterations */

	if(!(progress_tick++ % 50000))
	{
	    printf("$0,%u\r\n", ((100 * xsv.bytes_read) / xsv.size));
	}
	out_word.bits.tck = (INT8U) 0;
	(void) portOutp( out_word.value );
	out_word.bits.tck = (INT8U) 1;
	(void) portOutp( out_word.value );
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: jtag_ports.c
\*---------------------------------------------------------------------------------------------------------*/

