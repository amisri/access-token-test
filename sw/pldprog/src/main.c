/*---------------------------------------------------------------------------------------------------------*\
 File:		main.c

 Purpose:	PLDPROG boot

 Author:	Philippe Fraboulet

 History:
    12/05/06	pfr	Created
    28/04/07    pfr	Modified for PLDPROG
\*---------------------------------------------------------------------------------------------------------*/
#define GLOBAL_VARS
#define FGC_GLOBALS
#define MENUTREE

#include <main.h>
#include <platforms\pldprog\boot\menutree.h>
#include <platforms\pldprog\memmap.h>
//#include "xda.h"

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  MAIN
\*---------------------------------------------------------------------------------------------------------*/
{
    unsigned short prog_ver;
    unsigned char  str1[4];
    unsigned char  str2[4];

    Init();
    EnableIsr();
 
    /* Display version */

	DisplayReset();
	DisplayPrintf("Starting Pilot XSVF PLAYER");

   // RunMenus();
    while(1)					// Run the menus indefinitely
    {
	term.recv_cmd_f = 0;			// Reset receiving command flag
	MenuMenu(&root_menu);			// Run menus
    }
}
/*---------------------------------------------------------------------------------------------------------
void RunMenus(void)
---------------------------------------------------------------------------------------------------------
  This function is called by main() to start the menu system if required.
---------------------------------------------------------------------------------------------------------
{
    unsigned short	i;
    unsigned short	t;
    unsigned short	test_leds;
    unsigned char	ch = 0;
    unsigned char	node_id[32] = "";


}*/
/*---------------------------------------------------------------------------------------------------------*/
void PldProg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function runs the PLD prorammation
\*---------------------------------------------------------------------------------------------------------*/
{
    unsigned short  i, temp, crc, crc_exp;
    unsigned short  *type_p;
    struct  pack_file_header *pack;

    unsigned short  get_val_16;				// buffer value to read 16 bit values in flash
    INT32U  get_val_32;				// buffer value to read 32 bit values in flash

    if(!xsv.start_f)
    {
        return;
    }

    /* If start button pressed */

    xsv.stat_run	=  1;			// jtag running
    dspl.mode 		=  1;			// jump into display mode
    term.edit_state 	=  1;			// Interactive mode

    /* Init display */
    dspl.pos        =  0;
    dspl.flash 	=  0x00;
    displayString("I   ");

    /* get type struct address (in flash) */

    if(dspl.type > (NB_TYPES-1))
    {
	displayString("ERR");
	fprintf(TX_TERM, RSP_ERROR ", Unknown type %i\n\r", dspl.type);
	xsv.stat_run =  0;			// jtag operation finished
	xsv.start_f  =  0;			// clear start flag
	return;
    }

    /* Initialise Flash reading - put flash in read array mode */

    type_p   = (unsigned short*)type_map[dspl.type];

    pack = (struct pack_file_header*)type_p;
    flashRead((unsigned short*)type_p,&i);				// dummy read to put Flash in read_array mode

    /* Check flash is not empty */
    if(((INT32U)type_p != (INT32U)pack->start_addr) || (dspl.type != pack->type))
    {
	displayString("----");
	fprintf(TX_TERM, RSP_ERROR ", Flash not programmed\n\r");
	xsv.stat_run =  0;			// jtag operation finished
	xsv.start_f  =  0;			// clear start flag
	return;
    }

    /* Check download status for the given type */
    if(*((unsigned short *)FLASH_STATUS_32 + dspl.type) != 1)		// Status register for type is not 0
    {
	displayString("FLAS");
	fprintf(TX_TERM, RSP_ERROR ", Bad package status 0x%04X for type %i\n\r", *((unsigned short *)FLASH_STATUS_32 + dspl.type), dspl.type);
	xsv.stat_run =  0;			// jtag operation finished
	xsv.start_f  =  0;			// clear start flag
	return;
    }

    /* check file crc */
    crc = flashCrc(pack->start_addr, pack->size, &crc_exp);
    if(crc != crc_exp)
    {
	displayString("CRC");
	fprintf(TX_TERM, RSP_ERROR ", bad CRC (0x%04X)\n\r", crc);
	xsv.stat_run =  0;			// jtag operation finished
	xsv.start_f  =  0;			// clear start flag
	return;
    }

    /*--- Fill C structure with data stored in external Flash ---*/

    /* Get and verify type field */

    type_p += 4;			// skip start_addr & file total size
    get_val_16     = *type_p;
    if(get_val_16 !=  dspl.type)
    {
	displayString("ERR");
	fprintf(TX_TERM, RSP_ERROR ", Err in package struct - type [0x%04X]\n\r", get_val_16);
	xsv.stat_run =  0;			// jtag operation finished
	xsv.start_f  =  0;			// clear start flag
	return;
    }
    type_p++;						// shift to next field: nb of devices

    /* Get nb of devices in jtag chain */
    chain_type.nb  = *type_p;

    if(chain_type.nb > MAX_PLD_DEVICES)
    {
	displayString("-NB-");
	fprintf(TX_TERM, RSP_ERROR ", Nb of devices too high (%u)\n\r", chain_type.nb);
	return;
    }

    for(i=0 ; i<chain_type.nb ; i++)
    {
	chain_type.id_list[i] = *(++type_p);				// fill device list
    }

    /* Get ID file address */
    chain_type.id_file_p = (unsigned short*)(*(INT32U*)(++type_p));		// ID file address

    /* Get VERIFY file address */
    for(i=0 ; i<chain_type.nb ; i++)
    {
	type_p += 2;						// skip previous file address
	chain_type.list[i].verif_p = (unsigned short*)(*(INT32U*)type_p);	// VERIFY file address
    }

    /* Get PROG file address */
    for(i=0 ; i<chain_type.nb ; i++)
    {
	type_p += 2;						// skip previous file address
	chain_type.list[i].prog_p = (unsigned short*)(*(INT32U*)type_p);	// PROG file address
    }

    /*--- C structure is now ready ---*/

    /* Verify type structure with ID codes */
    xsv.mode = id_mode;					// set player in ID mode
    dspl.op  = 'I';

    xsv.id_index = 0;					// init id index
    if(jtagRun(chain_type.id_file_p))			// if ID failed
    {
	displayString("-ID-");
	fprintf(TX_TERM, RSP_ERROR ", Chain detection failed\n\r");
	return;
    }

    for(i=0 ; i<chain_type.nb ; i++)
    {
	if(xsv.ids[i] != chain_type.id_list[i])
	{
	    displayString("TYPE");
	    fprintf(TX_TERM, RSP_ERROR ", Wrond ID chain: pos %i - ID: g:%i exp:%i\n\r", i, xsv.ids[i], chain_type.id_list[i]);
	    xsv.stat_run =  0;			// jtag operation finished
	    xsv.start_f  =  0;			// clear start flag
	    return;
	}
    }

    xsv.mode = normal_mode;					// set player in prog & verify mode
    /* Verify all devices of this type */
    dspl.flash   = 0x4;
    dspl.op      = 'V';

    for(i=0 ; i<chain_type.nb ; i++)
    {
	dspl.pos = i+1;
	chain_type.list[i].status = 0;			// set position status to 0 (to be updated)
	if(!jtagRun(chain_type.list[i].verif_p))		// Verify ok
	{
	    chain_type.list[i].status = 1;
	}
    }

    /* Program to-be-updated position */
    dspl.op  = 'P';

    for(i=0 ; i<chain_type.nb ; i++)
    {
	dspl.pos = i+1;
	if(!chain_type.list[i].status)			// Status not ok
	{
	    if(!jtagRun(chain_type.list[i].prog_p))		// program ok
	    {
		chain_type.list[i].status = 1;
	    }
	}
    }

    /* Same thing, second run */
    for(i=0 ; i<chain_type.nb ; i++)
    {
	dspl.pos = i+1;
	if(!chain_type.list[i].status)			// Status not ok
	{
	    if(!jtagRun(chain_type.list[i].prog_p))		// program ok
	    {
		chain_type.list[i].status = 1;
	    }
	}
    }

    /* Check all status */
    for(i=0 ; i<chain_type.nb ; i++)
    {
	if(!chain_type.list[i].status)	// first fail
	{
	    /* display error */
	    dspl.chars[0] = 'P';
	    dspl.chars[1] = '-';

	    i++;
	    temp = '0' + i/10;		// dizaine
	    display(2,temp);
	    dspl.chars[2] = temp;

	    temp = '0' + (i - (i/10)*10);	// units
	    display(3,temp);
	    dspl.chars[3] = temp;

	    dspl.flash    = 0xF;

	    xsv.stat_run =  0;			// jtag operation finished
	    xsv.start_f  =  0;			// clear start flag

	    fprintf(TX_TERM, RSP_ERROR", position %u failed\n\r", i);
	    return;
	}
    }

    dspl.flash    = 0;
    displayString("GOOD");
    xsv.stat_run =  0;			// jtag operation finished
    xsv.start_f  =  0;			// clear start flag
}
/*---------------------------------------------------------------------------------------------------------*/
void EnableIsr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will enable needed ISR for the program
\*---------------------------------------------------------------------------------------------------------*/
{
    unsigned short  dummy_ch;

    //ta0ic   =   0x05;				// Timer A0 source for USLEEP, Level 5
    ta4ic   =   0x04;				// Timer A4 source for display and download sequencing, Level 4

    /* UART0 */
 //   s0ric   =   0x06;				//  6 : UART0 Rx (terminal keyboard)
 //   s0tic  =   0x03;				//  3 : UART0 Tx buffer empty (terminal display)

    /* UART2 */
    //s2ric   =   0x07;				//  7 : UART2 Rx
    //u2tb    =   0x00;                           // enable receive interupts

    /* UART3 */
    //s3ric   =   0x00;				//  4 : UART3 Rx
    //u3tb    =   0x00;                           // enable receive interupts

    /* UART4 */
    //s4ric   =   0x00;				//  5 : UART4 Rx
    //u4tb    =   0x00;                           // enable receive interupts

    /* INT2 */
    int2ic  =   0x03;				// Priority level 3, falling edge
}

/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare all the task for execution under uC/OS-II.
\*---------------------------------------------------------------------------------------------------------*/
{
    isrdummy = 0;

   /*--- Timers - configs ---*/

    udf	        =   0x02;	// Increment counter for Timer A1
    trgsr	=   0xC2;	// Trigger select register: Timer A1 uses TA0 overflow
				// Trigger select register: Timer A4 uses TA0 overflow
   /*--- Timer A0 - 1 Mhz clock (f1/24) ---*/

    ta0		=     23;	// 1MHz free running timer using 24MHz clock
    ta0s        =   0x01;	// Start timer A0

    /*--- Timer A1 - Free running counter at 1 Mhz (uses Timer A0 as input) - used by USLEEP() macro ---*/

    ta1mr	=   0x41;	// Timer mode register: event counter mode, free running
    ta1s        =   0x01;	// Start timer A1

    /*--- Timer A4 - display timer - underflow calls isr ---*/

    ta4mr	=   0x01;	// Timer mode register: event counter mode, reload
    ta4		=   50000-1;	// 20Hz timer using Timer 1
    ta4s        =   0x01;	// Start timer A4


}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/

