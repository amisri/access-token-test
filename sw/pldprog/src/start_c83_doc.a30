;---------------------------------------------------------------------
; File:     start.a30
;
; Purpose:  PS G64/C83 program - start up file
;
; Author:   Daniel Calcoen
;
; History:
;   14/01/05    qak Created from ncrt0.a30 and sect30.inc combined
;   07/03/05	doc Added reserved memory for FlashAsmCode()
;   09/05/05	pfr Modified for G64 tester program
;   15/06/05	mc  Add some interrupt vectors
;   08/08/06    PFR Modified for jtag player porting
;---------------------------------------------------------------------

RAM_USER	.equ	0400h			; begin of usable RAM
AFTER_FLASH_PRG	.equ	RAM_USER + 120h		; 200h reserved for FlashAsmCode()
;---------------------------------------------------------------
USER_ADR	.equ	00f80000h		; user decision to put the code from here

PRG_ADR         .equ    USER_ADR + 100h		; first 0x100 bytes are for vectors
;IsrReset_ADR	.equ
;---------------------------------------------------------------
; fixed areas for M32C83
;SPECIAL_PRG	.equ	0ff0000h
MonitorEntry	.equ	0fffd00h
FVECS_ADR	.equ	0ffffdch		; Fixed vector table at top of memory
;---------------------------------------------------------------
STACKSIZE	.equ	0300h
ISTACKSIZE	.equ	0300h
;---------------------------------------------------------------
prcr_addr	.equ	0ah			; Protect register
pm0_addr	.equ	04h			; Processor mode register 0
pm1_addr	.equ	05h			; Processor mode register 1
mcd_addr	.equ	0ch			; Main clock division register

;===============================================================
; Section allocation
;===============================================================
; Near RAM data area
;===============================================================
;000400h reserved for flash burner code that must reside in RAM
;0004ffh
;---------------------------------------------------------------
; SBDATA, at start up the section data_SEI in ROM is copied here
;---------------------------------------------------------------
	.section	data_SE,DATA
	.org		AFTER_FLASH_PRG
data_SE_top:					; SBDATA is redirected here
;---------------------------------------------------------------
; SBDATA variables without init value, Even aligned, zeroed at start up
;---------------------------------------------------------------
	.section	bss_SE,DATA,ALIGN
bss_SE_top:
;---------------------------------------------------------------
; SBDATA, at start up the section data_SOI in ROM is copied here
;---------------------------------------------------------------
	.section	data_SO,DATA
data_SO_top:
;---------------------------------------------------------------
; SBDATA variables without init value, Odd aligned, zeroed at start up
;---------------------------------------------------------------
	.section	bss_SO,DATA
bss_SO_top:
;---------------------------------------------------------------
; C variables, that goes to RAM, have initial value, Even aligned,
; Near, are stored in section data_NEI in ROM
; and at start up copied here
;---------------------------------------------------------------
	.section	data_NE,DATA,ALIGN
data_NE_top:
;---------------------------------------------------------------
; C variables without init value, Near, Even aligned, zeroed at start up
;---------------------------------------------------------------
	.section	bss_NE,DATA,ALIGN
bss_NE_top:
;---------------------------------------------------------------
; C variables, that goes to RAM, have initial value, Odd aligned,
; Near, are stored in section data_NOI in ROM
; and at start up copied here
;---------------------------------------------------------------
	.section	data_NO,DATA
data_NO_top:
;---------------------------------------------------------------
; C variables without init value, Near, Odd aligned, zeroed at start up
;---------------------------------------------------------------
	.section	bss_NO,DATA
bss_NO_top:
;---------------------------------------------------------------
; Stack area
;---------------------------------------------------------------
	.section	stack,DATA,ALIGN
	.blkb		STACKSIZE
	.align
stack_top:

	.blkb		ISTACKSIZE
	.align
istack_top:
;---------------------------------------------------------------
; Near ROM data area
;---------------------------------------------------------------
; C variables defined with "const", that are Even, Odd aligned,
; and are Near, are stored here
;---------------------------------------------------------------
; not defined
;===============================================================
; all the NEAR can go up to 0xFFFF, page0 ends here
;===============================================================
; Far RAM data area,  in case yo have external RAM
;===============================================================
; not defined
;---------------------------------------------------------------
; Code area
;---------------------------------------------------------------

; we decide to put the vectors just before the program code

	.section    	vector
	.org    	USER_ADR
vector_top:

	.glb    _IsrDummy1
        .glb    _IsrTick
	.glb    _Uart0TxISR
	.glb    _Uart0RxISR
	.glb    _Uart2RxISR
	.glb    _Uart3RxISR

                            ; NM = Non mascarable, the I flag don't mask the interrupt

	.lword  _IsrDummy1      ;NM INT16U  0, ISP for stack, BRK instruction
	.lword  _IsrDummy1      ;NM INT16U  1, ISP for stack, pure software, hardware reserved, RTOS context switch, called with the macro OS_TASK_SW()
	.lword  _IsrDummy1      ;NM INT16U  2, ISP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U  3, ISP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U  4, ISP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U  5, ISP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U  6, ISP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;   INT16U  7, ISP for stack, A/D1 Converter
	.lword  _IsrDummy1      ;   INT16U  8, ISP for stack, DMA0
	.lword  _IsrDummy1      ;   INT16U  9, ISP for stack, DMA1
	.lword  _IsrDummy1      ;   INT16U 10, ISP for stack, DMA2
	.lword  _IsrDummy1      ;   INT16U 11, ISP for stack, DMA3
	.lword  _IsrTick      	;   INT16U 12, ISP for stack, TIMER A0, systeme tick
	.lword  _IsrDummy1      ;   INT16U 13, ISP for stack, TIMER A1
	.lword  _IsrDummy1      ;   INT16U 14, ISP for stack, TIMER A2
	.lword  _IsrDummy1      ;   INT16U 15, ISP for stack, TIMER A3
	.lword  _IsrDummy1      ;   INT16U 16, ISP for stack, TIMER A4
	.lword  _IsrDummy1      ;   INT16U 17, ISP for stack, UART0 Transmission, NACK
	.lword  _Uart0RxISR     ;   INT16U 18, ISP for stack, UART0 Reception, ACK
	.lword  MonitorEntry    ;   INT16U 19, ISP for stack, UART1 Transmission, NACK,Debug Monitor
	.lword  MonitorEntry    ;   INT16U 20, ISP for stack, UART1 Reception, ACK, Debug Monitor
	.lword  _IsrDummy1      ;   INT16U 21, ISP for stack, TIMER B0
	.lword  _IsrDummy1      ;   INT16U 22, ISP for stack, TIMER B1
	.lword  _IsrDummy1      ;   INT16U 23, ISP for stack, TIMER B2
	.lword  _IsrDummy1      ;   INT16U 24, ISP for stack, TIMER B3
	.lword  _IsrDummy1      ;   INT16U 25, ISP for stack, TIMER B4
	.lword  _IsrDummy1      ;   INT16U 26, ISP for stack, INT5
	.lword  _IsrDummy1      ;   INT16U 27, ISP for stack, INT4
	.lword  _IsrDummy1      ;   INT16U 28, ISP for stack, INT3
	.lword  _IsrDummy1      ;   INT16U 29, ISP for stack, INT2
	.lword  _IsrDummy1      ;   INT16U 30, ISP for stack, INT1
	.lword  _IsrDummy1      ;   INT16U 31, ISP for stack, INT0, used by the Xilinx Bridge to drive the M32C83 with G64 bus Interrupts
	.lword  _IsrDummy1      ;   INT16U 32, USP for stack, TIMER B5
	.lword  _IsrDummy1      ;   INT16U 33, USP for stack, UART2 Transmission, NACK
	.lword  _Uart2RxISR     ;   INT16U 34, USP for stack, UART2 Reception, ACK
	.lword  _IsrDummy1      ;   INT16U 35, USP for stack, UART3 Transmission, NACK
	.lword  _Uart3RxISR     ;   INT16U 36, USP for stack, UART3 Reception, ACK
	.lword  _IsrDummy1      ;   INT16U 37, USP for stack, UART4 Transmission, NACK
	.lword  _IsrDummy1      ;   INT16U 38, USP for stack, UART4 Reception, ACK
	.lword  _IsrDummy1      ;   INT16U 39, USP for stack, Bus conflict detect, Start condition detect, Stop condition detect, (UART2), Fault Error
	.lword  _IsrDummy1      ;   INT16U 40, USP for stack, Bus conflict detect, Start condition detect, Stop condition detect, (UART3/UART0), Fault Error
	.lword  _IsrDummy1      ;   INT16U 41, USP for stack, Bus conflict detect, Start condition detect, Stop condition detect, (UART4/UART1), Fault Error
	.lword  _IsrDummy1      ;   INT16U 42, USP for stack, A/D0 Converter
	.lword  _IsrDummy1      ;   INT16U 43, USP for stack, Key input
	.lword  _IsrDummy1      ;   INT16U 44, USP for stack, Intelligent I/O Interrupt 0
	.lword  _IsrDummy1      ;   INT16U 45, USP for stack, Intelligent I/O Interrupt 1
	.lword  _IsrDummy1      ;   INT16U 46, USP for stack, Intelligent I/O Interrupt 2
	.lword  _IsrDummy1      ;   INT16U 47, USP for stack, Intelligent I/O Interrupt 3
	.lword  _IsrDummy1      ;   INT16U 48, USP for stack, Intelligent I/O Interrupt 4
	.lword  _IsrDummy1      ;   INT16U 49, USP for stack, Intelligent I/O Interrupt 5
	.lword  _IsrDummy1      ;   INT16U 50, USP for stack, Intelligent I/O Interrupt 6
	.lword  _IsrDummy1      ;   INT16U 51, USP for stack, Intelligent I/O Interrupt 7
	.lword  _IsrDummy1      ;   INT16U 52, USP for stack, Intelligent I/O Interrupt 8
	.lword  _IsrDummy1      ;   INT16U 53, USP for stack, Intelligent I/O Interrupt  9, CAN 0
	.lword  _IsrDummy1      ;   INT16U 54, USP for stack, Intelligent I/O Interrupt 10, CAN 1
	.lword  _IsrDummy1      ;NM INT16U 55, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U 56, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;   INT16U 57, USP for stack, Intelligent I/O Interrupt 11, CAN 2
	.lword  _IsrDummy1      ;NM INT16U 58, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U 59, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U 60, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U 61, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U 62, USP for stack, pure software, hardware reserved
	.lword  _IsrDummy1      ;NM INT16U 63, USP for stack, pure software, INT instruction

	; Interrupt with fixed vectors

	; FFFFDC          ;NM UND     ??? for stack, Undefined instruction
	; FFFFE0          ;NM INTO    ??? for stack, Overflow
	; FFFFE4          ;NM BRK     ??? for stack, BRK (can be redirected to INT16U 0)
	; FFFFE8          ;NM         ??? for stack, Address Match
	; FFFFEC          ;NM         ??? for stack, reserved
	; FFFFF0          ;NM         ??? for stack, Watchdog timer
	; FFFFF4          ;NM         ??? for stack, reserved
	; FFFFF8          ;NM         ??? for stack, NMI
	; FFFFFC          ;NM         ??? for stack, RESET

;---------------------------------------------------------------
; the code generated from the C compiler goes here
;---------------------------------------------------------------
	.section program
;	.org		PRG_ADR
program_top:
;---------------------------------------------------------------
; the assemble routine coded in this file,
; to serve the RESET interrupt, goes here
;---------------------------------------------------------------
	.section	interrupt,ALIGN
;	.org		IsrReset_ADR
interrupt_top:
;---------------------------------------------------------------
; Far ROM data area
;---------------------------------------------------------------
; C variables defined with "const", that are Even aligned,
; and are Far, are stored here
;---------------------------------------------------------------
	.section	rom_FE,ROMDATA,ALIGN
;	.org		0ff0000h
rom_FE_top:
;---------------------------------------------------------------
; C variables defined with "const", that are Odd aligned,
; and are Far, are stored here
;---------------------------------------------------------------
	.section	rom_FO,ROMDATA
rom_FO_top:
;---------------------------------------------------------------
; SBDATA with initial value
;---------------------------------------------------------------
; at start up copied to section data_SE in RAM
;---------------------------------------------------------------
	.section	data_SEI,ROMDATA,ALIGN
data_SEI_top:
;---------------------------------------------------------------
; at start up copied to section data_SO in RAM
;---------------------------------------------------------------
	.section	data_SOI,ROMDATA
data_SOI_top:
;---------------------------------------------------------------
; (how the compiler behaves with this Near and Far variables ??? which is the difference??)
;---------------------------------------------------------------
; C variables, that goes to RAM, have initial value, are Even aligned,
; and are Near, are stored here and at start up copied to section data_NE in RAM
;---------------------------------------------------------------
	.section	data_NEI,ROMDATA,ALIGN
data_NEI_top:
;---------------------------------------------------------------
; C variables, that goes to RAM, have initial value, are Odd aligned,
; and are Near, are stored here and at start up copied to section data_NO in RAM
;---------------------------------------------------------------
	.section	data_NOI,ROMDATA
data_NOI_top:
;---------------------------------------------------------------
; C variables, that goes to RAM, have initial value, are Even aligned,
; and are Far, are stored here ( and at start should be copied to Far RAM ???)
;---------------------------------------------------------------
	.section	data_FEI,ROMDATA,ALIGN
data_FEI_top:
;---------------------------------------------------------------
; C variables, that goes to RAM, have initial value, are Odd aligned,
; and are Far, are stored here ( and at start should be copied to Far RAM ???)
;---------------------------------------------------------------
	.section	data_FOI,ROMDATA
data_FOI_top:
;---------------------------------------------------------------
; Special Code area
;---------------------------------------------------------------
	.section	program_S
;	.org		SPECIAL_PRG
program_S_top:
;---------------------------------------------------------------
; Fixed Vector section
;---------------------------------------------------------------
	.section	fvector
	.org		FVECS_ADR
UDI:
	.lword		_IsrDummy1
OVER_FLOW:
	.lword		_IsrDummy1
BRKI:
	.lword		_IsrDummy1
ADDRESS_MATCH:
	.lword		_IsrDummy1
SINGLE_STEP:
	.lword		_IsrDummy1
WDT:
	.lword		_IsrDummy1
DBC:
	.lword		_IsrDummy1
NMI:
	.lword		_IsrDummy1
RESET:
	.lword		IsrReset

;---------------------------------------------------------------------
; SBDATA area definition
;---------------------------------------------------------------------
    .glb    __SB__
__SB__  .equ    data_SE_top
;---------------------------------------------------------------------
; Initialize Macro declaration
;---------------------------------------------------------------------

BZERO	.macro	TOP_ ,SECT_
	mov.b	#00H, R0L
	mov.l	#TOP_, A1
	mov.w	#sizeof SECT_ , R3
	sstr.b
	.endm

BCOPY	.macro	FROM_,TO_,SECT_
	mov.l	#FROM_ ,A0
	mov.l	#TO_ ,A1
	mov.w	#sizeof SECT_ , R3
	smovf.b
	.endm

;---------------------------------------------------------------------
; this code (IsrReset) goes to interrupt section
; serves the RESET of the uP
;---------------------------------------------------------------------

	.insf		IsrReset,S,0
	.glb		IsrReset
	.section	interrupt
IsrReset:

	mov.b	#03h,prcr_addr  ;protect register
				;bit0 unlock CM0, CM1, CM2, MCD, PLC0, PLC1
				;bit1 unlock PM0, PM1, INVC0, INVC1

	;set processer mode,
	;the selection for processer mode is explained at the end of the file

;	mov.b	#21h,pm0_addr
;	mov.b	#21h,pm1_addr

	mov.b	#00h,pm0_addr
	mov.b	#00h,pm1_addr
				;main clock is 32MHz
	mov.b	#12h,mcd_addr	;set main clock division to NO DIVISION

	mov.b	#00h,prcr_addr	;protect register (lock all)

;	ldc #0080h,	flg	;explicity says to start using USP
	ldc #0000h,	flg	;explicity says to start using ISP

	ldc #stack_top,	sp	;set stack pointer

	ldc #istack_top, isp	;set ustack pointer

	ldc #data_SE_top, sb	;set sb register
	ldc #USER_ADR, intb	;set intb register
	fset	i

;---------------------------------------------------------------------
; NEAR area initialize.
;--------------------------------------------------------------------
; bss zero clear
;--------------------------------------------------------------------

    BZERO   bss_SE_top,bss_SE
    BZERO   bss_SO_top,bss_SO
    BZERO   bss_NE_top,bss_NE
    BZERO   bss_NO_top,bss_NO

;---------------------------------------------------------------------
; initialize data section
;---------------------------------------------------------------------

    BCOPY   data_SEI_top,data_SE_top,data_SE
    BCOPY   data_SOI_top,data_SO_top,data_SO
    BCOPY   data_NEI_top,data_NE_top,data_NE
    BCOPY   data_NOI_top,data_NO_top,data_NO

;---------------------------------------------------------------------
; Call main() function
;---------------------------------------------------------------------

    ldc #stack_top,sp		;this is done several line above, needed due to the use of BZERO, BCOPY ???
    ldc #0h, fb			;for debuger (?????)

    .glb    _main
;   jsr.a   _main		;why calling a subrutine, it takes 4 bytes from the stack!!!!!!!
				;and we never return from that
    jmp.a   _main               ;this is better

;====================================================================
; exit() function
;---------------------------------------------------------------------

    .glb    _exit
    .glb    $exit
_exit:             ; End program
$exit:
    jmp _exit
    .einsf

;====================================================================
    .end

;---------------------------------------------------------------------
; End of start.a30 (C83)
;---------------------------------------------------------------------

;---------------------------------------------------------------------
; Processor mode Register 0
;
;  b7   b6   b5   b4   b3   b2   b1   b0
; PM07 (b6) PM05 PM04 PM03 PM02 PM01 PM00
;   |    |    |    |    |    |    |---|----- Processor mode
;   |    |    |    |    |    |
;   |    |    |    |    |    |-------------- R/W mode select bit
;   |    |    |    |    |
;   |    |    |    |    |--------------------Software reset bit
;   |    |    |    |
;   |    |    |----|-------------------------Multiplexed bus space select bit
;   |    |
;   |    |-----------------------------------Reserved
;   |
;   |----------------------------------------BCLK output disable bit
;
; our selection
;
;   0    0    1   0    0    0    0    1
;
;   Processor mode = Memory Expansion mode
;   /RD /BHE /WR
;   not reseted
;   10, Access the CS1 area with the bus ??? is not well explained in the manual ??!!! (Multiplex bus space allocated to /CS1)
;   0
;   BCLK is output
;---------------------------------------------------------------------
;---------------------------------------------------------------------
; Processor mode Register 1
;
;  b7   b6   b5   b4   b3   b2   b1   b0
; PM17 (b6) PM15 PM14 PM13 PM12 PM11 PM10
;   |    |    |    |    |    |    |---|----- External memory space mode
;   |    |    |    |    |    |
;   |    |    |    |    |    |-------------- Internal memory wait states bit
;   |    |    |    |    |
;   |    |    |    |    |--------------------SFR area wait states bit
;   |    |    |    |
;   |    |    |----|-------------------------ALE pin select bit
;   |    |
;   |    |-----------------------------------Reserved
;   |
;   |----------------------------------------BCLK output disable bit, 0 is utput, 1 is not output
;
; our selection
;
;   0    0    1   0    0    0    0    1
;
;   External memory space in Mode 1, CS2 CS1 CS0 to P4, (Two Chip selects & 21 bit addresses)
;   Internal mem = No wait state
;   SFR area = 1 wait state
;   P5/RAS
;
;   01 x and reserved bit ???, or ALE pin is P56 (where is this explained in the manual ?)
;
;---------------------------------------------------------------------

