/*---------------------------------------------------------------------------------------------------------*\
  File:         menu.c

  Purpose:      Functions to support the boot menu

  Author:       Stephen.Page@cern.ch

  Notes:

  History:

    19/08/03    stp     Created
    21/11/06    pfr     Taken from FGC2 boot for FGC3 maquette
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>
//#include <platforms\fgc3\boot\menutree.h>
#include <platforms\pldprog\boot\menutree.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuMenu(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  Move through the menu tree.  This function is called recursively for each level.  It is called
  recursively by TermGetMenuOption().
\*---------------------------------------------------------------------------------------------------------*/
{
    while(!menu.up_lvls)		// While no request to move up a level
    {
	menu.node_id[menu.depth] = '\0';	// Null terminate node ID at this depth

	if(node->function)			// If node is a LEAF
	{
	    if(node->argc)				// If function expect args
	    {
		TermRunFuncWithArgs(node);			// Get args then run function
	    }
	    else					// else no args
	    {
		MenuRunFunc(node, NULL);			// Run function directly
	    }

	    menu.up_lvls = (term.edit_state ? 1 : menu.depth+1); // Set up levels
	}
	else					// else node is a BRANCH
	{
	    if(term.edit_state)				// If interactive mode
	    {
		MenuDisplay(node);				// Print the menu
		TermGetMenuOption(node);			// Get user option and call MenuMenu() recursively
	    }
	}
    }

    menu.up_lvls--;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDisplay(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called in interactive mode to display the menu for the current branch node.  It
  indicates if the branch is recursive and for each child, whether it is a leaf or a sub-branch.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U	i;
    INT16U	n;

    /* Display menu title */

    if(!menu.depth)						// If root menu
    {
	n = fprintf(TX_TERM, "\n" TERM_BOLD "%s\n",node->name) - 2;	// Print node name underlined
    }
    else							// else lower level menu
    {
	n = fprintf(TX_TERM, "\n" TERM_BOLD "%s%c %s\n",menu.node_id,	// Print node name underlined
		    (node->recurse_f ? '*' : ' '),node->name) - 2;
    }

    /* Display system status */

//    CheckStatus();						// Refresh status values

//    fprintf(TX_TERM, TERM_NORMAL TERM_UL "B:%c G:%u T:%08lX F:%04X W:%04X L:%04X\n" TERM_NORMAL,
//			dev.device,				// Boot device
//			fip.gw_f,				// Gateway status
//			SM_UXTIME_P,				// Unix time
//			FAULTS,					// Faults
//			WARNINGS,				// Warnings
//			ST_LATCHED);				// Unlatched status

    /* Display list of menu options */

    n = node->n_children;

    for(i=0;i < n;i++)			// Show list of menu options
    {
	if((i+1) == n)				// If this is the last option
	{
	    fputs("\33[4m",TX_TERM);			// Enable underlining
	}

	fprintf(TX_TERM, "%i%c %s", i,
		(node->children[i]->function ? '*' : ' '),	// Mark leaves with a '*'
		 node->children[i]->name);

	if(node->children[i]->argc)			// If node has arguments
	{
	    fprintf(TX_TERM, " (%u,%s)",i,node->children[i]->args_help);	// Display help with args
	}

	fputs("\33[0m\n",TX_TERM);		// Disable attributes (underlining) and newline
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuFollow(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  Follow all children of node and run their functions.  If a child fails then result depends upon whether
  the node is flagged as fatal or not.  If it is, the execution on that branch terminates and the error
  is reported to the level above.  Note that all leaves on a recursive branch must not require arguments.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    /*--- Check recursive flag for leaves and branches ---*/

    if(!node->recurse_f)			// If node is not recursive
    {
	return(0);					// Return immediately without error
    }

    menu.node_id[menu.depth]   = '\0';

    /*--- Node is a LEAF ---*/

    if(node->function)				// If node is a leaf
    {
	return(MenuRunFunc(node, NULL));		// Run leaf's function (there can be no args)
    }

    /*--- Node is a BRANCH ---*/

    for(i=0;node->children[i];i++)			// For each child of branch
    {
	menu.node_id[menu.depth++] = '0' + i;		// Set node ID for child

	if(MenuFollow(node->children[i]) &&			// If execution of child failed and
	   node->children[i]->fatal_f)				// child node is flagged as fatal
	{
	    menu.depth--;
	    return(1);							// Report failure to level above
	}
	menu.depth--;
    }

    return(0);					// Return no error
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuRunFunc(struct menu_node *node, INT8U *args_buf)
/*---------------------------------------------------------------------------------------------------------*\
  This function will run the function for the LEAF node.
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(TX_TERM,RSP_ID_NAME, 			// Report that command is starting execution
	    menu.node_id,node->name);

    if(MenuPrepareArgs(args_buf, node->argc))		// If arguments are not valid
    {
	return(1);						// Report failure
    }

    menu.response_buf_pos = 0;				// Clear response and error buffers
    menu.error_buf[0] = '\0';

    if(MenuConfirm(node))				// If request is cancelled by user
    {
	fprintf(TX_TERM,RSP_ERROR ",Execution cancelled\n");	// Write error report
	return(1);
    }

    dev.abort_f = 0;					// Clear abort flag

    node->function(menu.argc, (INT8U**)&menu.argv);		// Run function

    if(menu.response_buf_pos)				// If rsp buffer is not empty
    {
	fprintf(TX_TERM,RSP_DATA "%s\n",menu.response_buf);	// Write last response buffer
    }

    if(!*menu.error_buf)				// If no error reported
    {
	fputs(RSP_OK "\n",TX_TERM);				// Report success
	return(0);
    }

    fprintf(TX_TERM,RSP_ERROR ",%s\n",menu.error_buf);	// Write error report
    return(1);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuConfirm(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  This function will get confirmation from the user if required for the node and in interactive mode.
  The function returns 0 if confirmation is given or not required (function will be called) and 1 if
  confirmation was not given (function will not be called).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U	ch;

    if(!term.edit_state || !node->confirm_f)	// If direct mode or node does not require confirmation
    {
	UartTxTerm('\n');				// Newline
	return(0);					// Run function
    }

    fputs(": Y/[N] ",TX_TERM);

    while(ch = UartTermGetCh(30000))		// While characters are entered with 30s timeout period
    {
	switch(ch)
	{
	case 'y':					// Y = Run command

	    fputs("Y\n",TX_TERM);
	    return(0);

	case '\n':					// Newline, Carriage return or N = cancel command
	case '\r':
	case 'n':

	    fputs("N\n",TX_TERM);
	    return(1);

	default:					// otherwise ring bell and get another character

	    UartTxTerm('\a');
	    break;
	}
    }

    fputs("- Time out\a\n",TX_TERM);		// Report timeout and cancel execution
    return(1);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuPrepareArgs(INT8U *buff, INT16U argc_exp)
/*---------------------------------------------------------------------------------------------------------*\
  Prepare arguments for function.  The argv buffer should be big enough to hold the maximum number of
  args that the buffer can contain (~40).  The function chops up the arguments in the buffer and sets
  menu.argc and menu.argv.  White space is removed from the start and end of arguments.  Missing arguments
  are set to point to a null string.  If number of arguments expected doesn't equal the number found,
  an error message is written and the function returns 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U		delimiter;
    INT8U *		end;
    INT8U *		cp     = buff;
    INT8U **		argv   = menu.argv;
    static INT8U	null[] = "";		    // Blank string for missing arguments

    menu.argc = 0;			// Reset number of arguments

    if(buff)				// If arguments buffer supplied
    {
	do					// Loop for each argument (comma separated)
	{
	    for(;*cp == ' ';cp++);			// Skip leading white space

	    if(*cp == '\0' || *cp == ',')		// If argument is empty
	    {
		end  = cp - 1;					// Prepare end pointer
		*argv = null;					// Set arg to null string
	    }
	    else					// else argument exists
	    {
		for(*argv = cp;*cp && *cp != ',';cp++);		// Find argument separater (, or null)
		for(end   = cp;*(--end) == ' ';);		// Find end of argument
	    }

	    delimiter = *(cp++);		// Remember delimiter character
	    argv++;				// Increment argument pointer
	    menu.argc++;			// Increment argument count
	    *(++end) = '\0';			// Terminate the current argument
	}
	while(delimiter);		// Loop while more arguments are waiting
    }

    if(menu.argc != argc_exp)		// If number of arguments is invalid
    {
	fprintf(TX_TERM,"\n" RSP_ERROR ",Invalid number of arguments (exp:%u got:%u)\n",argc_exp,menu.argc);
	return(1);
    }

    return(0);				// Return 0 = success
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRspArg(char *format, ...)
/*---------------------------------------------------------------------------------------------------------*\
  Append argument to response.
\*---------------------------------------------------------------------------------------------------------*/
{
    va_list	argv;
    INT16U	buf_len;
    INT8U	buf[TERM_LINE_SIZE];

    if(!format)							// If format is NULL
    {
        fprintf(TX_TERM, RSP_DATA "%s\n", menu.response_buf);		// Send response buffer
        menu.response_buf_pos = 0;					// Reset response buffer
	return;
    }

    va_start(argv, format);

//  int       vsprintf(char _far *, const char _far *, __va_list);
  buf_len = vsprintf(buf, format, argv);
//    buf_len = vsprintf( (char _far *) buf, (const char _far *) format, argv);

    va_end(argv);

    if(menu.response_buf_pos + buf_len >= (TERM_LINE_SIZE-3))	// If end of line exceeded
    {
        fprintf(TX_TERM, RSP_DATA "%s\n", menu.response_buf);		// Send response buffer
        menu.response_buf_pos = 0;					// Reset response buffer
    }

    menu.response_buf_pos += sprintf(&menu.response_buf[menu.response_buf_pos],",%s",buf);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuRspProgress(INT16U allow_abort_f, INT16U level, INT16U total)
/*---------------------------------------------------------------------------------------------------------*\
  This will report the progress in percent (level/total * 100).  If allow_abort_f is set, it will also check
  if the user has aborted the test, and returns 1 if so.  Otherwise it will return 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U level32 = (INT32U) level * 100L;

    if ( allow_abort_f && dev.abort_f )
    {
	MenuRspError("Aborted by user");
	return(1);
    }

    fprintf(TX_TERM,RSP_PROGRESS ",%u\r",(INT16U)(level32/total));

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRspError(char *format, ...)
/*---------------------------------------------------------------------------------------------------------*\
  Write error report to error buffer.
\*---------------------------------------------------------------------------------------------------------*/
{
    va_list	argv;

    va_start(argv, format);
    vsprintf(menu.error_buf, format, argv);
    va_end(argv);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuGetInt16U(INT8U *arg, INT16U min, INT16U max, INT16U *result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a 16-bit unsigned integer value.  If the ASCII value in *arg
  starts "0x", the value will be interpretted in hex.  If a valid integer is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U	value;
    INT16U	base  = 10;	// 16:Hex  10:Decimal (default)
    INT8U *	cp = arg;
    INT8U *	remains;

    if(!*cp)					// If argument is empty
    {
	MenuRspError("Missing argument");		// Report error
	return(1);
    }

    if(cp[0] == '0' && cp[1] == 'x')		// If prefix is "0x"
    {
	cp  += 2;					// Skip prefix
	base = 16;					// Interpret as hex
    }

    value = strtoul(cp,&remains,base);		// Convert arg to unsigned long

    if(*remains)				// If any characters remain after conversion
    {
	MenuRspError("Invalid integer:%s",arg);
	return(1);
    }

    if((value < (INT32U)min) ||			// If value is outside of min/max range
       (value > (INT32U)max))
    {
	MenuRspError("%s is outside limits (%u:%u)",arg,min,max);
	return(1);
    }

    *result = value;				// Return value via *result

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuGetInt32U(INT8U *arg, INT32U min, INT32U max, INT32U *result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a 32-bit unsigned integer value.  If the ASCII value in *arg
  starts "0x", the value will be interpretted in hex.  If a valid integer is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U      value;
    INT16U      base = 10;      // 16:Hex  10:Decimal (default)
    INT8U *	cp = arg;
    INT8U *	remains;

    if(!*cp)					// If argument is empty
    {
	MenuRspError("Missing argument");		// Report error
	return(1);
    }

    if(cp[0] == '0' && cp[1] == 'x')            // If prefix is "0x"
    {
        cp  += 2;                                       // Skip prefix
        base = 16;                                      // Interpret as hex
    }

    value = strtoul(cp,&remains,base);          // Convert arg to unsigned long

    if(*remains)                                // If any characters remain after conversion
    {
	MenuRspError("Invalid integer:%s",arg);
        return(1);
    }

    if((value < min) || (value > max))          // If value is outside of min/max range
    {
	MenuRspError("%s is outside limits (%lu:%lu)",arg,min,max);
        return(1);
    }

    *result = value;                            // Return value via *result

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U MenuGetInt32S(INT8U *arg, INT32S min, INT32S max, INT32S *result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a 32-bit signed integer value.  If the ASCII value in *arg
  starts "0x", the value will be interpretted in hex.  If a valid integer is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32S      value;
    INT16U      base = 10;      // 16:Hex  10:Decimal (default)
    INT8U *	cp = arg;
    INT8U *	remains;

    if(!*cp)					// If argument is empty
    {
	MenuRspError("Missing argument");		// Report error
	return(1);
    }

    if(cp[0] == '0' && cp[1] == 'x')            // If prefix is "0x"
    {
        cp  += 2;                                       // Skip prefix
        base = 16;                                      // Interpret as hex
    }

    value = strtol(cp,&remains,base);		// Convert arg to signed long

    if(*remains)                                // If any characters remain after conversion
    {
	MenuRspError("Invalid integer:%s",arg);
        return(1);
    }

    if((value < min) || (value > max))          // If value is outside of min/max range
    {
	MenuRspError("%s is outside limits (%ld:%ld)",arg,min,max);
        return(1);
    }

    *result = value;                            // Return value via *result

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void DUMMY(INT16U argc, INT8U **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Dummy boot function.
\*---------------------------------------------------------------------------------------------------------*/
{
   MenuRspError("Dummy function");
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menu.c
\*---------------------------------------------------------------------------------------------------------*/
