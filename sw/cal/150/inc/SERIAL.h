#ifndef __SERIAL_h__
#define __SERIAL_h__


#ifdef EXTERN
  #undef EXTERN
#endif

#ifdef __SERIAL_c__
  #define EXTERN
#else
  #define EXTERN extern
#endif

#define SERIAL_BUFLEN 512

EXTERN char serial_rxbuf[SERIAL_BUFLEN];
EXTERN char serial_txbuf[SERIAL_BUFLEN];
EXTERN unsigned int  serial_pitx;
EXTERN unsigned int  serial_potx;
EXTERN unsigned int  serial_pirx;
EXTERN unsigned int  serial_porx;
EXTERN unsigned int  serial_rxcnt;
EXTERN unsigned char serial_wcnt;
EXTERN unsigned char serial_state;
EXTERN unsigned char serial_txbuffull;
#define SERIAL_ST_WAITING    0
#define SERIAL_ST_TXB       10

EXTERN char serial_rxbuf0[SERIAL_BUFLEN];
EXTERN char serial_txbuf0[SERIAL_BUFLEN];
EXTERN unsigned int  serial_pitx0;
EXTERN unsigned int  serial_potx0;
EXTERN unsigned int  serial_pirx0;
EXTERN unsigned int  serial_porx0;
EXTERN unsigned int  serial_rxcnt0;
EXTERN unsigned char serial_wcnt0;
EXTERN unsigned char serial_state0;
EXTERN unsigned char serial_txbuffull0;

void serialComInit0(void);
void serialTimer0(void);
int  serialDataAvailable0(void);
int  serialTx0(char far *p, int l);
int  serialRx0(char far *p, int n);
char serialFlushTimer0(void);
char serialFlushInterrupt0(void);
#define SERIAL_FLUSH0 serialFlushTimer0()

#endif


