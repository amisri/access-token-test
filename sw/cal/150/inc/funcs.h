/*---------------------------------------------------------------------------------------------------------*\
  File:		funcs.h

  Contents:	Mitsubishi-G64 controller card under test
  		this file declares all the functions and macros contained in the program

  History:

    20/04/05	mc	Created
    28/11/06    pfr	Modified for FGC3
    11/12/07    ac  modified for USB DCM
\*---------------------------------------------------------------------------------------------------------*/

/*--- Macros ---*/

#define BUS_ERROR_CHECK_OFF	bus_error_check = 0
#define BUS_ERROR_CHECK_ON	bus_error_check = 1
#define BUS_ERROR_RESET		bus_error_flag  = 0

#define SINGLE_CHIP_MODE()	{OS_ENTER_CRITICAL();prc1=1;pm01=0;pm00=0;OS_EXIT_CRITICAL();prc1=0;}
#define MEM_EXP_MODE()		{OS_ENTER_CRITICAL();prc1=1;pm01=0;pm00=1;OS_EXIT_CRITICAL();prc1=0;}

//#define SET_DEV_LEDS(mask)	SIM_PORTF_P=((~mask<<1)&0x1E)

#define CURSOR(x,y)		fprintf(TERM_TX,"\33[%u;%uH",y,x)
#define SET_FP_LEDS(mask)	if(dev.reg_w&REG_LEDS)LEDS_FP=mask

/* Timer macros */

#define	USLEEP(usecs)		{INT16U t0=ta4,tn=(INT16U)(usecs);while((ta4-t0)<tn);}
#define	MSLEEP(ms)		{INT16U t0=ta2,tn=(INT16U)(ms);while((ta2-t0)<tn);}

/*---- Bscan mode functions ----*/

#define BSCAN_ENABLE()	{TRST_LOW;EMU0_HIGH;EMU1_LOW;TRST_HIGH;}
#define BSCAN_DISABLE()	{TRST_LOW;}
#define	TRST_HIGH	p0_5 = 0x01
#define	TRST_LOW	p0_5 = 0x00
#define	EMU0_HIGH	p0_6 = 0x01
#define	EMU0_LOW	p0_6 = 0x00
#define	EMU1_HIGH	p0_7 = 0x01
#define	EMU1_LOW	p0_7 = 0x00

/*---- FIP ----*/

#define	FIP_RESET	p14_1 = 0x00
#define FIP_SET		p14_1 = 0x01

/*---- DIG interface ----*/

#define ENA_DIG		{pd14_4=1; p14_4=1;}
#define DIS_DIG		{pd14_4=1; p14_4=0;}

/*---- DIG interface ----*/

#define ENA_ANA		{pd14_5=1; p14_5=1;}
#define DIS_ANA		{pd14_5=1; p14_5=0;}


/*--- term.c ---*/

#define	IFTERM			if(term.edit_state)

void 	InitTerm		(FILE *f);
void	TermGetMenuOption	(struct menu_node *node);
void	TermRunFuncWithArgs	(struct menu_node *node);
void    TermEnter		(void);
void	TermInteractive		(void);
void	TermDirect		(void);
INT16U  TermLE0                 (INT8U ch);
INT16U  TermLE1                 (INT8U ch);
INT16U  TermLE2                 (INT8U ch);
INT16U  TermLE3                 (INT8U ch);
INT16U  TermLE4                 (INT8U ch);
INT16U  TermLE5                 (INT8U ch);
INT16U	TermInsertChar          (INT8U ch);
void    TermCursorLeft          (void);
void    TermCursorRight         (void);
void    TermStartOfLine		(void);
void    TermEndOfLine           (void);
void    TermDeleteLeft          (void);
void    TermDeleteRight         (void);
void    TermShiftRemains	(void);

/*--- isr.c ---*/

void	IsrDummy1	(void);
void	IsrTA0   	(void);
void	IsrUART0   	(void);
void	IsrTick		(void);
void	IsrReset	(void);

/*--- sci.c ---*/

void	SciInit			(void);
void	SciRxCh			(INT8U ch);
INT16U	SciRxFlush		(void);
INT8U	SciGetCh		(INT16U to_ms);
void	SciPutc			(INT8S ch, FILE *f);
void	SciIsrMst		(void);
void	SciTxCh			(INT8U ch);

/*--- UART ---*/

void 	UartRxTsk	(void *unused);
void 	UartSendCh	(INT16U uart, INT8U ch);
INT16U 	UartTermRxFlush	(void);
INT16U 	UartTxTerm	(INT16U ch);
INT8U 	UartTermGetCh	(INT16U to_ms);

/*--- menu.c ---*/

void	MenuMenu		(struct menu_node *node);
void	MenuDisplay		(struct menu_node *node);
INT16U  MenuFollow		(struct menu_node *node);
INT16U  MenuRunFunc		(struct menu_node *node, INT8U *args_buf);
INT16U	MenuConfirm		(struct menu_node *node);
void	MenuRspArg		(INT8S *format, ...);
void	MenuRspError		(INT8S *format, ...);
INT16U  MenuPrepareArgs         (INT8U *buff, INT16U argc_exp);
INT16U	MenuRspProgress		(INT16U allow_abort_f, INT16U level, INT16U total);
INT16U  MenuGetInt16U           (INT8U *cp, INT16U min, INT16U max, INT16U *result);
INT16U  MenuGetInt32U           (INT8U *cp, INT32U min, INT32U max, INT32U *result);
INT16U  MenuGetInt32S           (INT8U *cp, INT32S min, INT32S max, INT32S *result);
INT16U	MenuGetFloat		(INT8U *arg, FP32 min, FP32 max, FP32 *result);

/*--- main.c ---*/

void	Init			(void);
void 	RunMenus		(void);
void	EnableIsr		(void);


/*---------------------------------------------------------------------------------------------------------*\
  End of file: funcs.h
\*---------------------------------------------------------------------------------------------------------*/

