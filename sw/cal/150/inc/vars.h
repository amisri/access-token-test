/*---------------------------------------------------------------------------------------------------------*\
  File:		vars.h

  Contents:	FGC3 maquette test prog
  		this file declares/defines all the global structures and variables used
		by the program.

  History:

    20/04/05	pfr	Created
\*---------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------*/

VARS_EXT INT16U	isrdummy;

VARS_EXT INT16U  timeout_ms;

/*--- General boot variables ---*/

struct dev_vars
{
    INT16U		state;				// Boot state
    INT16U		fip_id;				// Fip address
    INT16U		run_menu_f;			// Run menu flag
    INT16U		n_devs_to_update;		// Number of flash devices to update
    INT16U		n_devs_corrupted;		// Number of corrupted codes detected
    INT16U		abort_f;			// Abort operation flag (Ctrl-C pressed)
    INT16U		mem_sbe_f;			// Memory single bit error flag
    INT16U		enable_slow_wd;			// Enable triggering of slow watchdog
    INT16U		enable_fast_wd;			// Enable triggering of fast watchdog
    INT16U		leds_read;			// Read of LEDs register
    INT16U		slot5;				// Slot5 info register
    INT16U		slot5_type;			// Slot5 interface type
    INT16U		bus_type;			// Bus type
    INT16U		pld_version;			// PLD version read from slot 5 register
    INT8U		device;				// 'A', 'B' or 'R'
    INT8U		reg_r;				// Register access faults (read)
    INT8U		reg_w;				// Register access faults (write)
    INT8U		rst_src[20];			// Reset source string
};

VARS_EXT struct dev_vars 	dev;

/*--- Menu variables ---*/

struct menu_node
{
    char * 		name;
    char * 		args_help;
    INT8U		argc;
    INT8U		confirm_f;
    INT8U		fatal_f;
    INT8U		recurse_f;
    void		(*function)(INT16U argc, INT8U **argv);
    INT16U		n_children;
    struct menu_node **	children;
};

struct menu
{
    struct menu_node *	node;
    INT16U		depth;
    INT16U		up_lvls;
    INT8U		node_id[MENU_MAX_DEPTH];
    INT16U		argc;
    INT8U *		argv[CMD_MAX_ARGS];
    INT16U		response_buf_pos;
    INT8U		response_buf[TERM_LINE_SIZE];
    INT8U		error_buf[TERM_LINE_SIZE];
};

VARS_EXT struct menu	menu;

/*--- Terminal variables ---*/

struct term_buf						// 256 byte circular buffer
{
    INT16U		n_ch;				// Number of characters in the buffer
    INT8U		in;				// Buffer input index
    INT8U		out;				// Buffer output index
    INT8U 		buf[TERM_BUF_SIZE];		// 256 character buffer
};

struct term
{
    FILE 		f;				// Pointer to buffered IO stream
    struct term_buf	tx;				// Transmission buffer
    struct term_buf	rx;				// Reception buffer
    INT16U		xoff_timeout;			// Xoff timeout flag (ms)
    INT16U		recv_args_f;			// Terminal input is active
    INT16U		recv_cmd_f;			// Start of comamnd ($) seen
    INT16U              edit_state;
    INT8U               linebuf[TERM_LINE_SIZE];
    INT16U              line_end;
    INT16U              line_idx;
};

VARS_EXT struct term	term;

/*---------------------------------------------------------------------------------------------------------*\
  End of file: vars.h
\*---------------------------------------------------------------------------------------------------------*/


