#ifndef __SIMPLESHELL_h__
#define __SIMPLESHELL_h__

#include <deftypes.h>

#ifdef EXTERN
  #undef EXTERN
#endif

#ifndef __SIMPLESHELL_c__
   #define EXTERN extern
#else
  #define EXTERN
#endif

#define SIMPLESHELL_OUTLEN 201
#define DIAG_OUTLEN 512

EXTERN char simpleshell_outstr[SIMPLESHELL_OUTLEN];
EXTERN char direct_outstr[SIMPLESHELL_OUTLEN];
EXTERN char far *diag_outstr;

#define SIMPLESHELL_PROMPT "\n\rUSB DCM#"
#define SIMPLESHELL_STRLEN 40
#define SIMPLESHELL_CMDLEN 100  //changed from 80 to 100 on 03/2012 miguel

EXTERN char simpleshell_str[SIMPLESHELL_STRLEN];
EXTERN char simpleshell_strtmp[SIMPLESHELL_STRLEN];
EXTERN char simpleshell_strCmd[SIMPLESHELL_CMDLEN];
EXTERN char simpleshell_lCmd;
EXTERN char direct_strCmd[SIMPLESHELL_CMDLEN];
EXTERN char direct_lCmd;
EXTERN char simpleshell_echo;
EXTERN char simpleshell_state;

#define SIMPLESHELL_ST_OUT      0
#define SIMPLESHELL_ST_OKLOGIN  10
#define SIMPLESHELL_ST_WAITING  20
#define SIMPLESHELL_ST_EXECMD   30

EXTERN char direct_st;
#define DIRECT_CMDSTART 10
#define DIRECT_CMDDFN   20
#define DIRECT_CMDRCV   30

EXTERN char term_mode;
#define DIRECT_MODE 1
#define EDITOR_MODE 2
#define DIAG_MODE   3

EXTERN char simpleshell_st;
#define SIMPLESHELL_ST_NORM     0
#define SIMPLESHELL_ST_ESC0     10
#define SIMPLESHELL_ST_ESC1     11
#define SIMPLESHELL_ST_ESC2     12
#define SIMPLESHELL_ST_CHKHOME  20
#define SIMPLESHELL_ST_CHKEND   30
#define SIMPLESHELL_ST_CHKDEL   40

#define SIMPLESHELL_cntESC  5000  // 500ms

#define SIMPLESHELL_LF  0x0A
#define SIMPLESHELL_CR  0x0D
#define SIMPLESHELL_SC  0x3B
#define SIMPLESHELL_ESC 0x1B
#define SIMPLESHELL_BS  0x08

#define SIMPLESHELL_CMDHIST_SIZE    10

EXTERN char simpleshell_cmdhistory[SIMPLESHELL_CMDHIST_SIZE][SIMPLESHELL_CMDLEN];
EXTERN int simpleshell_pcmdhist;
EXTERN int simpleshell_cursor;
EXTERN int direct_cursor;
EXTERN char direct_space;
EXTERN char set_cmd;

EXTERN char simpleshell_matrixstring[20];
EXTERN unsigned int simpleshell_secrelays;

EXTERN unsigned int simpleshell_cnt;	// decremented each 100us in SimpleShellTimer() if >0

EXTERN unsigned int cdc_faults_msk;
EXTERN unsigned int sm_faults_msk;
EXTERN unsigned int cdc_warns_msk;

EXTERN int begin_array;
EXTERN int end_array;
EXTERN unsigned int ival_array[7];
EXTERN float fval_array[10];

EXTERN struct fgc_diag diagdata;
EXTERN struct fgc150_stat cdc_pubdata;
EXTERN struct fgc151_stat sm_pubdata;
EXTERN struct DIAG_data
{
    char start_seq[FGC_DIAG_N_SYNC_BYTES];
    struct fgc_diag   diagdata0;
} DIAG_data0;

void simpleshellVarInit(void);
void simpleshellRun(const char far *c);
void simpleshellTimer(void);
void simpleshellExecCommand(void);
char simpleshellParse(char far *strCmd, char lCmd);
void simpleshellCleanLine(void);
void simpleshellCursorLeft(void);
void simpleshellCursorRight(void);
void simpleshellCharIns(char c);
void simpleshellCharDel(void);

void simpleshellDirectInit(void);
void simpleshellDirectRun(const char far *c);
void simpleshellDirectCharIns(char c);

void simpleshellDiagInit(void);
void simpleshellDiagRun(void);

void simpleshellDiagCheckCdcStatus(void);
void simpleshellDiagCheckSmStatus(void);
void simpleshellDiagCheckCdcWarn(void);
void simpleshellTermCheckCdcStatus(void);
void simpleshellTermCheckSmStatus(void);
void simpleshellTermCheckCdcWarn(void);
void simpleshellReadAllStatus(void);
void simpleshellReadAllAdcs(void);

int simpleshellCheckOptions(char far *tk1, char far *tk2, char far *rsp1, char far *rsp2, char far *outstr);

char simpleshellCheckArrayIdx(char far *array_idx, int array_sz);
char simpleshellCheckArrayEnb(char far *array_val);
char simpleshellCheckArrayFlt(char far *array_val);
char simpleshellSetCdc(int cdc_on);
char simpleshellSetSmChannel(int k);
char simpleshellUnsetSmChannel(int k);

enum rangeno {
/*0*/ OFF,
/*1*/ RANGE_1A,
/*2*/ RANGE_2A5,
/*3*/ RANGE_5A,
/*4*/ RANGE_10A,
/*5*/ INVALID_RANGE
};

enum relstate {
/*0*/ ENABLED,
/*1*/ DISABLED,
/*2*/ INVALID_STATE
};

enum smrelay {
/*0*/ SEL_CH1,
/*1*/ BP_CH1_OPEN,
/*2*/ SEL_CH2,
/*3*/ BP_CH2_OPEN,
/*4*/ SEL_CH3,
/*5*/ BP_CH3_OPEN,
/*6*/ SEL_CH4,
/*7*/ BP_CH4_OPEN,
/*8*/ SEL_CH5,
/*9*/ BP_CH5_OPEN,
/*10*/ SEL_CH6,
/*11*/ BP_CH6_OPEN,
/*12*/ VB_DUAL,
/*13*/ SEL_CH7,
/*14*/ BP_CH7_OPEN,
/*15*/ BP_ALL_OPEN
};

#define SM_INDEX_TABLE_TYPE int

extern def_const_string cdc_out_range[];
extern def_const_string cdc_relay_status[];
extern def_const_string sm_relay_status[];
extern SM_INDEX_TABLE_TYPE sm_idx[];

#ifdef FGC_GLOBALS

SM_INDEX_TABLE_TYPE sm_idx[] = {                                                          //this table adapts the SM bitmap in order
/*0*/   SEL_CH1,                                                                          //to have all channels in a sequence
/*1*/   BP_CH1_OPEN,                                                                      //in the sm bit map. The original bitmap
/*2*/   SEL_CH2,                                                                          //implemented in the SM puts channel 7
/*3*/   BP_CH2_OPEN,                                                                      //on bits 13 and 14 whilst with this table
/*4*/   SEL_CH3,                                                                          //we get them on bits 12 and 13
/*5*/   BP_CH3_OPEN,
/*6*/   SEL_CH4,
/*7*/   BP_CH4_OPEN,
/*8*/   SEL_CH5,
/*9*/   BP_CH5_OPEN,
/*10*/  SEL_CH6,
/*11*/  BP_CH6_OPEN,
/*12*/  SEL_CH7,
/*13*/  BP_CH7_OPEN,
/*14*/  VB_DUAL,
/*15*/  BP_ALL_OPEN
};

def_const_string cdc_out_range[] = {
/*0*/    "OFF",
/*1*/    "RANGE_1A",
/*2*/    "RANGE_2A5",
/*3*/    "RANGE_5A",
/*4*/    "RANGE_10A",
/*5*/    "!invalid"
};

def_const_string cdc_relay_status[] = {
/*0*/   "INV2048T",
/*1*/   "ON2048T",
/*2*/   "INV1024T",
/*3*/   "ON1024T",
/*4*/   "INV512T",
/*5*/   "ON512T",
/*6*/   "INV256T",
/*7*/   "ON256T",
/*8*/   "INV128T",
/*9*/   "ON128T",
/*10*/  "INV64T",
/*11*/  "ON64T",
/*12*/  "INV32T",
/*13*/  "ON32T",
/*14*/  "INV16T",
/*15*/  "ON16T",
/*16*/  "INV8T",
/*17*/  "ON8T",
/*18*/  "INV4T",
/*19*/  "ON4T",
/*20*/  "INV2T",
/*21*/  "ON2T",
/*22*/  "INV1T",
/*23*/  "ON1T",
/*24*/  "INV1TCAL",
/*25*/  "ON1TCAL",
/*26*/  "INV1TDAC",
/*27*/  "ON1TDAC",
/*28*/  "ONS4T",
/*29*/  "ONS8T",
/*30*/  "ONS16T",
/*31*/  "ONS40T"
};

def_const_string sm_relay_status[] = {
/*0*/ "SEL_CH1",
/*1*/ "BP_CH1_OPEN",
/*2*/ "SEL_CH2",
/*3*/ "BP_CH2_OPEN",
/*4*/ "SEL_CH3",
/*5*/ "BP_CH3_OPEN",
/*6*/ "SEL_CH4",
/*7*/ "BP_CH4_OPEN",
/*8*/ "SEL_CH5",
/*9*/ "BP_CH5_OPEN",
/*10*/ "SEL_CH6",
/*11*/ "BP_CH6_OPEN",
/*12*/ "VB_DUAL",
/*13*/ "SEL_CH7",
/*14*/ "BP_CH7_OPEN",
/*15*/ "BP_ALL_OPEN"
};

#endif

#endif

#define NO_VB_CLP_ON        0x374           //VB disconnected, clamp on,
#define NO_VB_CLP_ON_TWARN  0x3F4           //VB disconnected, clamp on, temp warning on
#define VB_ON_CLP_ON        0x176          //VB connected, clamp on
#define VB_ON_CLP_ON_TWARN  0x1F6          //VB connected, clamp on, temp warning on
#define NO_VB_CLP_OFF       0x375          //no VB, Clamp off
#define NO_VB_CLP_OFF_TWARN 0x3F5          //no VB, Clamp off, temp warning on
#define VB_ON_CLP_OFF       0x177          //VB on, Clamp off
#define VB_ON_CLP_OFF_TWARN 0x1F7          //VB on  Clamp off, temp warning on





