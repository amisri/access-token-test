/*---------------------------------------------------------------------------------------------------------*\
  File:		main.h

  Contents:	Mitsubishi-G64 controller card under test
  		this is the principle header file for a tester program

  History:

    20/04/05	mc	Created
    28/11/06    pfr	Modified for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#ifdef FGC_GLOBALS
#define VARS_EXT
#else
#define VARS_EXT extern
#endif

/*--- Include files ---*/

#include <sfr32c87_144.h>

#include <os_cpu.h>				// functions
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <version.h>				// version
#include <consts.h>				// constants
#include <vars.h>				   // variables
#include <funcs.h>				// functions

#include <fgc_stat.h>
#include <fgc_errs.h>
#include <SERIAL.h>
#include <SIMPLESHELL.h>
#include <USBDCM.h>

/*--- FGC Library macros ---*/

#define	Test(v,b)		(v&(b))
#define	Set(v,b)		(v|=(b))
#define	Clr(v,b)		(v&=~(b))

/*----- usbdcm_dac special definitions to prevent errors -----*/

#ifdef __DAC__
#define	__BUILTIN_strcpy(s1,s2)
#define	__BUILTIN_memcpy(s1,s2,n)
#define	__BUILTIN_strcmp(s1,s2)		(0)
#define __BUILTIN_memset(s,c,n)
#endif

/*----- TIMER related macros and constants -----*/
#define FCLK 24000000L
#define CALC_TMR(ftmr) (((FCLK / (unsigned long)(ftmr))) - 1)


/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
