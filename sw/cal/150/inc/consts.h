/*---------------------------------------------------------------------------------------------------------*\
  File:		consts.h

  Contents:	Mitsubishi-G64 controller card under test
  		this file defines the global constants for the tester program.

  History:

    20/07/07	pfr	Created
\*---------------------------------------------------------------------------------------------------------*/

/* Temporary consts to make the proj compile */

#define TERM_RESET 0


/* A definir ds la mem map*/
#define CPU_RESET_POWER_MASK16              0x0080                // Reset source: Power cycle
#define CPU_RESET_PROGRAM_MASK16            0x0040                // Reset source: HC16 program
#define CPU_RESET_MANUAL_MASK16             0x0020                // Reset source: reset button on front panel
#define CPU_RESET_WDOG1MS_MASK16            0x0010                // Reset source: 1 millisecond watchdog timeout
#define CPU_RESET_WDOG6S_MASK16             0x0008                // Reset source: 6 second watchdog timeout
#define CPU_RESET_C87RAM_MASK16             0x0004                // Reset source: multiple bit error in HC16 RAM
#define CPU_RESET_C67RAM_MASK16             0x0002
#define CPU_MODEH_C67IRQ_3_MASK8            0x0008                // C32 Interrupt request 3
#define CPU_MODEH_C67IRQ_2_MASK8            0x0004                // C32 Interrupt request 2
#define CPU_MODEH_C67IRQ_1_MASK8            0x0002                // C32 Interrupt request 1
#define CPU_MODEH_C67IRQ_0_MASK8            0x0001

#define TRUE  1
#define FALSE 0

/*------------ TEST POINTS -------------*/

#define  SET_TP1		p10_1 = 0x01
#define  CLEAR_TP1		p10_1 = 0x00
#define  TP1			p10_1
#define  SET_TP2		p10_2 = 0x01
#define  CLEAR_TP2		p10_2 = 0x00
#define  TP2			p10_2
#define  SET_TP6		p10_6 = 0x01
#define  CLEAR_TP6		p10_6 = 0x00
#define  TP6			p10_6
#define  SET_TP7		p10_7 = 0x01
#define  CLEAR_TP7		p10_7 = 0x00
#define  TP7			p10_7

/*---------- ms counter --------*/

#define  MS_CT			ta2
#define  US_CT			ta4

/*----- Miscellaneous constants -----*/

#define CMD_MAX_ARGS			(TERM_LINE_SIZE/2)	// Max command args
#define	MENU_MAX_DEPTH			8			// Length of menu.node_id;
#define	LEDS_UPDATING_CODE		LEDS_DCCT_RED_MASK16
#define ALLOW_ABORT			1			// Used by MenuRspProgress()
#define NO_ABORT			0			// Used by MenuRspProgress()

/*----- Miscellaneous -----*/

#define	 LEDS_UPDATING_CODE		LEDS_DCCT_RED_MASK16
#define  LENVAL_MAX		600
//#define  TERM_LINE_SIZE		80
//#define  MAX_ARGS		10

/*--- STATUS ---*/

//#define  STAT_PASS		0
//#define  STAT_FAIL		1
//#define  STAT_ERR		0xFFFF

/*----- States/Faults/warnings/status variable shortcuts -----*/

#define	OP_STATE			stat_var.fgc_stat.class_data.c50.state_op
#define	PLL_STATE			stat_var.fgc_stat.class_data.c50.state_pll
#define	FAULTS				stat_var.fgc_stat.class_data.c50.st_faults
#define	WARNINGS			stat_var.fgc_stat.class_data.c50.st_warnings
#define ST_LATCHED			stat_var.fgc_stat.class_data.c50.st_latched
#define ST_UNLATCHED			stat_var.fgc_stat.class_data.c50.st_unlatched

/*----- Update state constants -----*/

#define	ADV_CODE_STATE_NOT_IN_USE	0
#define	ADV_CODE_STATE_OK		1
#define	ADV_CODE_STATE_BAD_CODE		2
#define	ADV_CODE_STATE_BAD_LEN		3

/*----- Boot state constants -----*/

#define BOOT_STATE_SELF_TEST		LEDS_VS_GREEN_MASK16
#define BOOT_STATE_CODE_UPDATE		LEDS_DCCT_GREEN_MASK16
#define BOOT_STATE_WAIT_USER		LEDS_PIC_GREEN_MASK16|LEDS_PIC_RED_MASK16
#define BOOT_STATE_RUN_MENU		LEDS_PIC_GREEN_MASK16

/*----- Boot type constants -----*/

#define	BOOT_PWR			0		// Power start - do full self test and update codes
#define	BOOT_SLOW			1		// Slow start  - do partial test and update codes
#define	BOOT_FAST			2		// Fast start  - do minimual test and run main program

/*----- C62 constants -----*/

#define	C62_OFF				0		// C62Pwr() constants
#define	C62_ON				1
#define	C62_CHECK			2

/*----- Code constants -----*/

#define CODE_TIMEOUT_INIT		20000		// Initial timeout: 20 seconds
#define CODE_TIMEOUT_RUN		2000		// Running timeout: 2 seconds
#define CODE_SIZEOF_BLOCK		36		// Used in asm so must be numeric (sizeof doesn't work)
#define	CODE_RX_NONE			0 		// code.rx_mode
#define	CODE_RX_FIP			1
#define	CODE_RX_SERIAL			2

#define CODE_BOOT_BLKS			4096		// 128 KB
#define CODE_MAINPROG_BLKS		8192		// 256 KB
#define CODE_DSP_BLKS			1177		// 37 KB
#define CODE_IDPROG_BLKS		1024		//  32 KB
#define CODE_IDDB_BLKS			6144		// 192 KB
#define CODE_PTDB_BLKS			256		//   8 KB
#define CODE_SYSDB_BLKS			512		//  16 KB
#define CODE_COMPDB_BLKS		1024		//  32 KB
#define CODE_DIMDB_BLKS			2048		//  64 KB
#define CODE_FGENDB_BLKS		256		//  8  KB
#define CODE_FGC3_XLX_BLKS		2592		//

#define FPGA_CODE_SIZE			82896		// FPGA(s) code size in bytes
							// not multiple of 32 bytes

/*----- Code state constants -----*/

#define	CODE_STATE_UNKNOWN		0
#define	CODE_STATE_DEV_FAILED		1
#define	CODE_STATE_CORRUPTED		2
#define	CODE_STATE_NOT_AVL		3
#define	CODE_STATE_VALID		4

/*----- Diag constants -----*/

#define	DIAG_BUF_LEN			13		// Max number of all channels
#define	DIAG_TRIGGER			0x8000		// Trigger scan bit in  QSM_SPCR1
#define	DIAG_MAX_RESETS			10		// Max resets before state changes to FAILED
#define	DIAG_N_ANA			8		// Number of analogue channels
#define	DIAG_OFF			0		// No scan
#define	DIAG_RESET			1		// Diag Reset
#define	DIAG_START			2		// End of reset
#define	DIAG_SCAN			3		// Start scan
#define DIAG_RUN			4		// Scanning
#define DIAG_FAILED			5		// Failed
#define	DIAG_VMEAS1			0		// Diag analogue channels indexes...
#define	DIAG_VMEAS10			1
#define	DIAG_PSU5			2
#define	DIAG_PSU15P			3
#define	DIAG_PSU15N			4
#define	DIAG_ANA5			5
#define	DIAG_ANA15P			6
#define	DIAG_ANA15N			7
#define	DIAG_STATUS_A			8
#define	DIAG_STATUS_B			9
#define	DIAG_DATA_A			11
#define	DIAG_DATA_B			12

/*----- Dig constants -----*/

#define	DIG_TIMEOUT_MAX			300		// Dig Timeout max in ms
#define	DIG_TIMEOUT_MIN			100		// Dig Timeout min in ms

/*----- New class_id constants -----*/

#define	FGC_BASE			0
#define	FGC2_BASE			50
#define	FGC2_LHC_PC			51
#define	FGC2_PS_MPS			52
#define	FGC2_RF_FGEN			59
#define	FGC3_BASE			60

/*----- Flash device constants -----*/

#define FLS_BL_ERASE_MS			1000		// ms Timeout of C87 internal flash block erase (typ. 800 ms)
#define FLS_PROG_US			50		// us Timeout of C87 internal flash word prog (typ. 25 us)
#define	FLS_DEV_MP_13_10		0		// C87ROM blocks 13-10	256KB	(Main programs)
#define	FLS_DEV_BA_7_6			1		// C87ROM blocks 7-6	128KB	(Boot for operation)
#define	FLS_DEV_16			2		// C87ROM block  16	64KB	(DimDB)
#define	FLS_DEV_5			3		// C87ROM blocks 5	32KB	(CompDB)
#define	FLS_DEV_2			4		// C87ROM block	 2	8KB	(NameDB)
#define	FLS_DEV_NDB_4_3			5		// C87ROM block  4-3	16KB	(SysDB)
#define	FLS_DEV_DSP_9_8			6		// C87ROM blocks 9-8	128KB	(DSP)
#define	FLS_DEV_C62_1			7		// C62_1 		8KB	(PTDB)
#define	FLS_DEV_C62_3			8		// C62_3 		32KB	(IdProg)
#define	FLS_DEV_C62_456			9		// C62_456 		192KB	(IDDB)
#define	FLS_DEV_XLX_15_14		10		// C87ROM blocks 15-14	128KB	(xlx prog)

#define	FLS_DEV_MP_13_10_SIZE		0x40000		// C87 MP (13-10)    size	256KB
#define	FLS_DEV_BA_7_6_SIZE		0x20000		// C87 BootA (7-6)   size	128KB
#define	FLS_DEV_16_SIZE			0x10000		// C87 Blk  16	     size	64KB
#define	FLS_DEV_5_SIZE			0x8000		// C87 Blk  5	     size	32KB
#define	FLS_DEV_2_SIZE			0x2000		// C87 Blks 2	     size	8KB
#define	FLS_DEV_NDB_4_3_SIZE		0x4000		// C87 Blks 4-3	     size	16KB
#define	FLS_DEV_DSP_9_8_SIZE		0x20000		// C87 Blks 9-8 (DSP)size	128KB
#define	FLS_DEV_C62_1_SIZE		0x2000		// C62_1	     size	32KB
#define	FLS_DEV_C62_3_SIZE		0x10000		// C62_3	     size	64KB
#define	FLS_DEV_C62_456_SIZE		0x30000		// C62_456	     size	192KB
#define	FLS_DEV_XLX_15_14_SIZE		0x20000		// C87 Blks 15-14    size	128KB

#define	FLS_BOOT_BLOCK_SIZE		0x10000		// Size of blocks used for boot (64KB)
							// Flash funcs must be changed if different blocks are used
/*----- FIFO test constants -----*/

#define	FIFO_PORT_CTRL			0x00000222
#define	FIFO_DX				0x00000040
#define	FIFO_FSX			0x00000400
#define	FIFO_CLX			0x00000004

/*----- Fip constants -----*/

#define FIP_GW_TIMEOUT_MS		45		// 45ms (allow one missed packed before change of state)
#define FIP_INIT_TIMEOUT		2000		// FipInit period when not connected (ms)
#define FIP_PLL_LOCKED_LIMIT		800		// +/-20us
#define FIP_PLL_LOCKED_DETECT		50		// Number of cycle below limit for locked state

/*----- Register access constants -----*/

#define	REG_LEDS			0x0001		// LEDs register access
#define	REG_C62				0x0002		// C62 register access
#define	REG_FIP				0x0004		// FIP register access
#define	REG_DIG				0x0008		// DIG register access
#define	REG_SLOT5			0x0010		// Slot5 (ANA/RFC) register access

/*----- Boot response header constants -----*/

#define	RSP_ID_NAME			"$0,%s,%s"	// $0,{NODE_ID},{NODE_NAME}
#define	RSP_PROGRESS			"$1"		// $1,{% completed}
#define	RSP_DATA			"$2"		// $2,{DATA},{DATA},...
#define	RSP_OK				"$3"		// $3
#define	RSP_ERROR			"$4"		// $4,{ERROR MSG}
#define RSP_LABEL			"$5"		// $5,{LABEL}

/*----- Terminal constants -----*/

#define	TERM_TX				&term.f		// Pointer to FILE stream
#define TERM_LINE_SIZE  	        80		// Length of terminal line for line editor
#define	TERM_BUF_SIZE			256		// Terminal rx/tx buffer sizes
#define TERM_XOFF_TIMEOUT_MS		60000		// Xoff timeout in milliseconds (60s)
#define TERM_COL1			1		// First column
#define TERM_COL2			21		// Second column
#define TERM_COL3			41		// Third column
#define TERM_COL4			61		// Fourth column

/*----- Update state constants -----*/

#define	UPDATE_STATE_UNKNOWN		0
#define	UPDATE_STATE_UPDATE		1
#define	UPDATE_STATE_OK			2
#define	UPDATE_STATE_LOCKED		3

/*----- C67 constants from Test program -----*/

#define C67BOOT_STAT_PASS		0
#define C67BOOT_NO_ACK			1
#define C67BOOT_NOT_RDY			2

#define C67_PING			1
#define C67_INV				2
#define C67_INT_CT			3
#define C67_FPU				4
#define C67_SCI				5
#define C67_RESET_CT			6

#define	C67_ALIVE			0x01		// Mask for top byte only (used with BRSET/BRCLR)
#define	C67_STATUS_C87_SBE		0x0010
#define	C67_STATUS_C67_SBE		0x0020
#define	C67_STATUS_PATTERN_FAULT	0x0040
#define	C67_STATUS_ADDRESS_FAULT	0x0080

/*----- C67 constants from Boot -----*/

#define	C67_STATUS_FAILED_TO_START	0x0100
#define	C67_STATUS_BG_FAILED		0x0200
#define	C67_STATUS_MEM_FAULT		0x0400
#define	C67_STATUS_STANDALONE		0x0800

/*-------------- C67 -------------*/

#define  C67_RESET			p14_3
#define  C67_BL_CS			p15_3
#define  C67_BL_MAGIC			0x41504954
#define  C67_BL_SWS			0x00005853
#define  C67_BL_SWS_R			0x00005253
#define  C67_BL_PING_NB			10
#define  C67_BL_SECTION_LOAD		0x58535901
#define  C67_BL_SECTION_LOAD_R		0x52535901
#define  C67_BL_REQUEST_CRC		0x58535902
#define  C67_BL_REQUEST_CRC_R		0x52535902
#define  C67_BL_ENABLE_CRC		0x58535903
#define  C67_BL_ENABLE_CRC_R		0x52535903
#define  C67_BL_DISABLE_CRC		0x58535904
#define  C67_BL_DISABLE_CRC_R		0x52535904
#define  C67_BL_JUMP			0x58535905
#define  C67_BL_JUMP_R			0x58535905
#define  C67_BL_JUMP_CLOSE		0x58535906
#define  C67_BL_JUMP_CLOSE_R		0x52535906
#define  C67_BL_SET			0x58535907
#define  C67_BL_SET_R			0x52535907
#define  C67_BL_START_OVER		0x58535908
#define  C67_BL_START_OVER_R		0x52535908
#define  C67_BL_RESERVED		0x58535909

#define  C67_BL_SECTION_FILL		0x5853590A
#define  C67_BL_SECTION_FILL_R		0x5253590A
#define  C67_BL_PING			0x5853590B
#define  C67_BL_PING_R			0x5253590B

#define  C67_BL_EOF_AIS			0x00000000

#define  C67_DSP_FINISH			0

/*------------- C62 -------------*/

#define  C62_POWER_ON		p14_0

/*--- heater ---*/

#define  HEAT_DAC		da1

/*------------- FPGA -------------*/

#define	 FPGA_PGM		p15_7
#define  FPGA_DONE		p15_5
#define  FPGA_CCLK		p15_4
#define  FPGA_INIT		p15_6

#define FPGA_PULSE_CCLK		p15_4 = 1; asm("NOP"); p15_4 = 0; asm("NOP"); p15_4 = 1
#define FPGA_INIT_IN()		pd15_6 = 0x00;
#define FPGA_INIT_OUT		pd15_6 = 0x01;
#define FPGA_DATA		p0

/*--- PLL conctants ---*/

#define PLL_PHASE       	pll_phase  //0x4600   // 556*40
#define PLL_P_GAIN        	pll_p_gain //500
#define PLL_I_GAIN        	pll_i_gain //2
#define PLL_D_GAIN        	pll_d_gain //0
#define TICK_PERIOD		pll_tick_period //40000L

/*----- MEM MAP constants -----*/

#define	SRAM_SIZE	0x100000
#define	FRAM_SIZE	0x010000

/*----- Terminal constants -----*/

#define TERM_LINE_SIZE  	        80		// Length of terminal line for line editor
#define	TERM_BUF_SIZE			256		// Terminal rx/tx buffer sizes
#define TERM_XOFF_TIMEOUT_MS		60000		// Xoff timeout in milliseconds (60s)
#define TERM_COL1			1		// First column
#define TERM_COL2			21		// Second column
#define TERM_COL3			41		// Third column
#define TERM_COL4			61		// Fourth column


/*----- Serial Communications Interface related constants -----*/

#define  JTAG_RESET		0
#define  JTAG_IDLE		1
#define  JTAG_SELECT_DR		2
#define  JTAG_CAPTURE_DR	3
#define  JTAG_SHIFT_DR		4
#define  JTAG_EXIT1_DR		5
#define  JTAG_PAUSE_DR		6
#define  JTAG_EXIT2_DR		7
#define  JTAG_UPDATE_DR		8
#define  JTAG_SELECT_IR		9
#define  JTAG_CAPTURE_IR	10
#define  JTAG_SHIFT_IR		11
#define  JTAG_EXIT1_IR		12
#define  JTAG_PAUSE_IR		13
#define  JTAG_EXIT2_IR		14
#define  JTAG_UPDATE_IR		15

#define	 TDI_MASK		0x80	// a verifier
#define	 TCK_MASK		0x40
#define	 TMS_MASK		0x20

/*------------------ XSVF Command Bytes ---------------------*/

#define  XILINX36		0x29502093
#define  XILINX72		0x29504093
#define  XILINX108		0x29506093
#define  XILINX144		0x29508093
#define  XILINX18V		0x05024093
#define  XILINX18V01		0x05034093

/* encodings of xsvf instructions */
#define  XCOMPLETE        0
#define  XTDOMASK         1
#define  XSIR             2
#define  XSDR             3
#define  XRUNTEST         4
/* Reserved              5 */
/* Reserved              6 */
#define  XREPEAT          7
#define  XSDRSIZE         8
#define  XSDRTDO          9
#define  XSETSDRMASKS     10
#define  XSDRINC          11
#define  XSDRB            12
#define  XSDRC            13
#define  XSDRE            14
#define  XSDRTDOB         15
#define  XSDRTDOC         16
#define  XSDRTDOE         17
#define  XSTATE           18         /* 4.00 */
#define  XENDIR           19         /* 4.04 */
#define  XENDDR           20         /* 4.04 */
#define  XSIR2            21         /* 4.10 */
#define  XCOMMENT         22         /* 4.14 */
#define  XWAIT            23         /* 5.00 */
#define  XCHECKSUM	  24	     /* PFR added */
/* Insert new commands here */
/* and add corresponding xsvfDoCmd function to xsvf_pfDoCmd below. */
#define  XLASTCMD         25         /* Last command marker */


/*-------------- XSVF Command Parameter Values ----------------------*/

#define  XSTATE_RESET     0          /* 4.00 parameter for XSTATE */
#define  XSTATE_RUNTEST   1          /* 4.00 parameter for XSTATE */

#define  XENDXR_RUNTEST   0          /* 4.04 parameter for XENDIR/DR */
#define  XENDXR_PAUSE     1          /* 4.04 parameter for XENDIR/DR */

/* TAP states */
#define  XTAPSTATE_RESET     0x00
#define  XTAPSTATE_RUNTEST   0x01    /* a.k.a. IDLE */
#define  XTAPSTATE_SELECTDR  0x02
#define  XTAPSTATE_CAPTUREDR 0x03
#define  XTAPSTATE_SHIFTDR   0x04
#define  XTAPSTATE_EXIT1DR   0x05
#define  XTAPSTATE_PAUSEDR   0x06
#define  XTAPSTATE_EXIT2DR   0x07
#define  XTAPSTATE_UPDATEDR  0x08
#define  XTAPSTATE_IRSTATES  0x09    /* All IR states begin here */
#define  XTAPSTATE_SELECTIR  0x09
#define  XTAPSTATE_CAPTUREIR 0x0A
#define  XTAPSTATE_SHIFTIR   0x0B
#define  XTAPSTATE_EXIT1IR   0x0C
#define  XTAPSTATE_PAUSEIR   0x0D
#define  XTAPSTATE_EXIT2IR   0x0E
#define  XTAPSTATE_UPDATEIR  0x0F

/*--- Ports consts ---*/

#define  POWER_CHECK		p0_4
#define  TDO			p0_3
#define  TCK			(INT16S) 0
#define  TMS			(INT16S) 1
#define  TDI			(INT16S) 2


/*--- former testing.h consts ---*/

#define  XSV_FILE_MAX_NB	4
#define  XSV_FILE_NAME_SIZE	8
#define  BSCAN_ERR_PRINT	20
#define  XSV_CS_LOOP_SIZE	4	// Number of shift in checksum loop

#define MAX_BSDEVICES   	25          // Maximum number of boundary scan (BS) devices in chain.
#define MAX_BSDL_TYPES  	8           // Maximum number of BS types (BSDLs)in a JTAG chain.
#define MAX_CELLS       	300         // Maximum number of testable cells (input type cells) in each BS device type.
#define MAX_FAIL_PINS   	50          // Maximum number of different pins failing in a test (-1).

#define MAX_CHARS       	20          // Maximum number of chars in a string (bsdl entity names, etc).
#define FILE_MAX_CHARS  	32          // Maximum number of chars in a filename string (filenames, bsdl entity names, etc).

#define XSVF_XILINX_TYPE 	0          // Type of xsvf file.

#define BSDL_ENTITY_UNKNOWN 	-1

#define WRITE_START              0  // Constants used in functions write?????File
#define WRITE_ERROR_JTAG         1
#define WRITE_FINISH             2
#define WRITE_ERROR_RESULT_INFRA 3
#define WRITE_ERROR_RESULT_INTER 4

#define FAIL 0                      // test status: PASS or FAIL.
#define PASS 1

#define ENDOF_ALL_DEVICES 	-1        // Mark, respectively, the end of devices, cells and errors in each array.
#define ENDOF_ALL_CELLS   	-1
#define ENDOF_ALL_ERRORS  	-1

// Do NOT change the following constants:
#define ERROR_STA0        	0         // Stuck-at-0 error (allways expected 1/received 0).
#define ERROR_STA1        	1         // Stuck-at-1 error (allways expected 0/received 1).
#define ERROR_UNKNOWN     	2         // Unknown error (both expected 1/received 0 and expected 0/received 1).


/* PLD consts */

#define PLD_NAME_LEN		16	// PLD name register length

/* was in micro.h */

#ifndef XSVF_MICRO_H
#define XSVF_MICRO_H

#ifndef DEBUG_MODE
#define DEBUG_MODE 1
#endif

/* Legacy error codes for xsvfExecute from original XSVF player v2.0 */
#define XSVF_LEGACY_SUCCESS 		1
#define XSVF_LEGACY_ERROR   		0

/* 4.04 [NEW] Error codes for xsvfExecute. */
/* Must #define XSVF_SUPPORT_ERRORCODES in jtag_seq.c to get these codes */
#define XSVF_ERROR_NONE         	0
#define XSVF_ERROR_UNKNOWN      	1
#define XSVF_ERROR_TDOMISMATCH  	2
#define XSVF_ERROR_MAXRETRIES   	3   /* TDO mismatch after max retries */
#define XSVF_ERROR_ILLEGALCMD   	4
#define XSVF_ERROR_ILLEGALSTATE 	5
#define XSVF_ERROR_DATAOVERFLOW 	6   /* Data > lenVal LENVAL_MAX buffer size*/
#define XSVF_ERROR_ILLEGALJUMP_CS 	7	/* should not jump to xsvfDoXCHECKSUM */
/* Insert new errors here */
#define XSVF_NO_POWER	         	8
#define XSVF_ERROR_LAST         	9

#endif  /* XSVF_MICRO_H */
/*---------------------------------------------------------------------------------------------------------*\
  End of file: consts.h
\*---------------------------------------------------------------------------------------------------------*/

