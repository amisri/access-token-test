#ifndef __USBDCM_h__
#define __USBDCM_h__


#ifdef EXTERN
  #undef EXTERN
#endif

#ifdef __USBDCM_c__
  #define EXTERN
#else
  #define EXTERN extern
#endif

//--------------------------- usbdcm_dac --------------------------

#define USBDCM_DAC1CS   p2_0
#define USBDCM_DAC1SCLK p2_1
#define USBDCM_DAC1DIN  p2_2
#define USBDCM_DAC2CS   p2_4
#define USBDCM_DAC2SCLK p2_5
#define USBDCM_DAC2DIN  p2_6

#define USBDCM_DAC_0_DNFS_NOM   0L
#define USBDCM_DAC_0_D0_NOM     32768L
#define USBDCM_DAC_0_DPFS_NOM   65535L
#define USBDCM_DAC_0_VNFS_NOM   -12.5												//the usbdcm_dac Full scale is 12.5V and not 10V
#define USBDCM_DAC_0_V0_NOM     0
#define USBDCM_DAC_0_VPFS_NOM   12.5                                                //This means 65535 corresponds to 12.5V and not 10V


#define USBDCM_DAC_1_DNFS_NOM   0L
#define USBDCM_DAC_1_D0_NOM     32768L
#define USBDCM_DAC_1_DPFS_NOM   65535L
#define USBDCM_DAC_1_VNFS_NOM   -12.5
#define USBDCM_DAC_1_V0_NOM     0
#define USBDCM_DAC_1_VPFS_NOM   12.5

#define GAIN_ADCINT_P05 		0.0097656											//gains from raw value read on the internal ADCS
#define GAIN_ADCINT_P15 		0.029248											//to real voltage on the power supplies
#define GAIN_ADCINT_NT15 		0.0048828											//takes in consideration the voltage dividers used to measure the voltage
#define GAIN_ADCINT_N15 		1.3988
EXTERN float usbdcm_dac0_0mat;
EXTERN float usbdcm_dac0_10mat;
EXTERN float usbdcm_dac0_slope_mat;
EXTERN float usbdcm_dac[2];
EXTERN int usbdcm_dac_raw[2];
//EXTERN unsigned long int usbdcm_dac_raw[2];

void usbdcmDacOut(unsigned char ch, unsigned val);
void usbdcmDacOutV(unsigned char ch, float val);
char usbdcmAdcCal(void);
void usbdcmAdcCfg(unsigned char val, unsigned char addr);
int usbdcmAdcIn(void);
float usbdcmAdcInV(void);
void usbdcmInternalAdcInit(void);

//--------------------------- ADC --------------------------
#define USBDCM_ADCCLK   p11_2
#define USBDCM_ADCCS    p11_1       // old: p11_0
#define USBDCM_ADCDOUT  p11_0       // old: p11_1

#define USBDCM_ADCCFG   p1
#define USBDCM_DIFFON   p1_0
#define USBDCM_SEON     p1_1
#define USBDCM_EN_DIFF  p1_2
#define USBDCM_EN_SE    p1_3
#define USBDCM_ADDRSH   4
#define USBDCM_ADDRMSK  0xF0

#define USBDCM_DIFF     0
#define USBDCM_SE       1
#define USBDCM_ADC_CALCYCLES    100

EXTERN unsigned char usbdcm_cfg;
EXTERN unsigned char usbdcm_cfgp;
EXTERN unsigned int usbdcm_wcnt;
EXTERN unsigned int status_cnt;
EXTERN unsigned int diag_cnt;

EXTERN float usbdcm_adc_v0;
EXTERN long usbdcm_adc_d0;                                                       	//ADC offset, used to calculate the ADC calibrated reading
EXTERN float usbdcm_adc_slope;                                                   	//ADC gain, used to calculate the ADC calibrated reading
EXTERN float usbdcm_adc_vref;                                                  		//real value of Vref voltage as measured by external DVM
EXTERN long usbdcm_adc_dref;                                                  		//used to calculate: usbdcm_adc_slope
EXTERN unsigned int mp_version;


//--------------------------- internal ADC------------------

#define USBDCM_ADSTART  0x40
#define USBDCM_AD0CON0  0x10    													// single sweep mode
#define USBDCM_AD0CON1  0x39
#define USBDCM_AD0CON2  0x01
#define USBDCM_AD0CON3  0x00
#define USBDCM_AD0CON4  0x00

#define USBDCM_CNT_IADC_RELOAD  19


EXTERN unsigned int usbdcm_cnt_iadc;
EXTERN volatile int usbdcm_raw_p05;
EXTERN volatile int usbdcm_raw_p15;
EXTERN volatile int usbdcm_raw_m15;
EXTERN float usbdcm_volts_p05;
EXTERN float usbdcm_volts_p15;
EXTERN float usbdcm_volts_m15;
EXTERN float calp05;
EXTERN float calp15;
EXTERN float caln15;
EXTERN float cdc_iout;
EXTERN float cdc_vout;
EXTERN float pbc_temp;
EXTERN float sm_inull;
EXTERN float sm_vout;
EXTERN float sm_temp[8];

EXTERN volatile char usbdcm_flag_iadc;

//--------------------------- DIG IO -----------------------
#define USBDCM_DIGOUT(byte)  p3 = byte
#define USBDCM_DIGIN         p4

//--------------------------- CDC --------------------------
#define USBDCM_WRITELATCH 0x10
#define USBDCM_WRITECLOCK 0x20
#define USBDCM_READLATCH  0x40
#define USBDCM_READCLOCK  0x80

#define USBDCM_READSTATUS 0
#define USBDCM_READRELAYS 8
#define USBDCM_READALARMS 3

void usbdcmResetCpld(void);
int usbdcmReadStatus(char far *st);
void usbdcmWriteStatus(char far *st);
void usbdcmSetCurrent(double current);
void usbdcmSetCurrentStep(double current);
void usbdcmAutonullPid(void);
char usbdcmCalDac(void);
float usbdcmAutonullDac1t(int offset);
void usbdcmInit(void);
void usbdcmTimer(void);
void usbdcmInitFlashStorage(void);
void usbdcmGetFlash(void);

#define USBDCM_DEFAULT_LIMSTEPSIZE      10											//changed step size to 5%, mcb 29-06-2016 and back to 10% on the 2017-04-21 because of timing problems in the LHC
#define USBDCM_DEFAULT_RELAYDELAY 		5000
#define USBDCM_DEFAULT_AUTONULLDELAY    2500
#define USBDCM_DEFAULT_ADCDELAY         500											//50ms
#define DIAG_DELAY                      2000

EXTERN int usbdcm_limStepSize;
EXTERN unsigned int usbdcm_relaydelay;
EXTERN unsigned int usbdcm_autonulldelay;
EXTERN unsigned int usbdcm_adcdelay;

#define USBDCM_KP_DEFAULT 1.0
#define USBDCM_KI_DEFAULT 5.0


EXTERN float usbdcm_kp;
EXTERN float usbdcm_ki;
EXTERN unsigned int usbdcm_nautonull;
EXTERN float i_meas1a;

EXTERN float last_autonull;
EXTERN char usbdcm_autonull;
EXTERN char usbdcm_autonull_sat;
EXTERN char clamp_on;
EXTERN char pbc_cal_ok;
EXTERN char pbc_cmpl_ok;
EXTERN char pbc_pwr_ok;
EXTERN char pbc_chr_ok;
EXTERN char pbc_temp_warn;
EXTERN char pbc_temp_fault;
EXTERN char cdc_pwr_ok;
EXTERN char vb_pwr_ok;
EXTERN char vb_con_ok;
EXTERN char vb_temp_ok;

EXTERN char vb_mode;

EXTERN unsigned int usbdcm_dac_cal;
EXTERN unsigned int usbdcm_adc_cal;

EXTERN double usbdcm_prevcurrent;
EXTERN double usbdcm_current;

EXTERN float usbdcm_pat;
EXTERN float usbdcm_at;
EXTERN float usbdcm_vdac_cal;
EXTERN float usbdcm_fs;
EXTERN unsigned int usbdcm_nsec;
EXTERN unsigned int usbdcm_npri;
EXTERN unsigned short usbdcm_csec;

EXTERN char USBDCM_longop;

EXTERN char USBDCM_matrixstring[20];

//--------------------------- SM --------------------------
#define USBDCM_SM_2FPGA_ON      0x01
#define USBDCM_SM_2FPGA_OFF     0xFE
#define USBDCM_SM_WRITELATCH    0x02
#define USBDCM_SM_WRITECLOCK    0x04
#define USBDCM_SM_READLATCH     0x08
#define USBDCM_SM_READCLOCK     0x10

#define USBDCM_SM_2UC   0x01
#define USBDCM_SM_5V    0x02

#define USBDCM_SM_IDLE  0x0000

#define USBSM_V2T   6.57778
#define USBSM_V2V   7.0
#define USBSM_V2N   10.0
#define USBCDC_V2I  10				//current measurement scaling factor: the current measurement is done with a 0.1R resistor
#define USBCDC_V2V  1				//compliance voltage conversion factor: compliance voltage never above 10V so a direct measurement is done
#define USBCDC_V2N  20				//null meter scaling factor: on the 1kR resistor 1mV = 1uA, the gain of the null meter is 50 so 50mV = 1uA, which means 1V is 20uA so a factor of 20 from V to uA
#define USBCDC_V2T  100				//temperature scaling factor: the PBC temp reading gives 0.01V/C

EXTERN unsigned int usbdcm_sm_5v_present;
EXTERN unsigned int usbdcm_smch;
EXTERN int usbdcm_sm_relays;
EXTERN int usbdcm_smbits;

#define CDC_RELAY_MAP_LEN   32
#define SM_RELAY_MAP_LEN    16
#define SM_CHANNELS         7

EXTERN unsigned int cdc_relay[CDC_RELAY_MAP_LEN];
EXTERN unsigned int sm_relay[SM_RELAY_MAP_LEN];

void usbdcmSmWriteStatus(unsigned int st);
unsigned int usbdcmSmReadStatus(void);

//--------------------------- FLASH -----------------------

#define USBDCM_FLASH_CS     p9_3    // flash chip select
#define USBDCM_FLASH_DIN    p9_2    // uc to flash
#define USBDCM_FLASH_DOUT   p9_1    // flash to uc
#define USBDCM_FLASH_SCLK   p9_0    // flash serial clock

void usbdcmInitFlash(void);
void usbdcmWriteEnableFlash(void);
void usbdcmWriteDisableFlash(void);
void usbdcmWriteFlash8(unsigned char addr, unsigned char b);
unsigned char usbdcmReadFlash8(unsigned char addr);
void usbdcmWriteFlashFloat(unsigned char addr, float f);
float usbdcmReadFlashFloat(unsigned char addr);

void usbdcmSwapbytes(void *object, size_t size);

typedef union {
  float f;
  unsigned long l;
  struct {
    unsigned char b0;
    unsigned char b1;
    unsigned char b2;
    unsigned char b3;
  } bytes;
} usbdcm_flb;

EXTERN unsigned char usbdcm_readyness;
EXTERN unsigned char usbdcm_sm;


#define USBDCM_JUSTMANUFACTURED 255
#define USBDCM_READYFORUSE      0
#define USBDCM_TYPE_CDC         0
#define USBDCM_TYPE_SM          1
#define USBDCM_UNCAL            0
#define USBDCM_CALOK            255

#define USBDCM_ADDR_READYNESS   0                                                 	//addresses defined according to variable size
#define USBDCM_ADDR_TYPE        1
#define USBDCM_ADDR_DAC0_0mAT   2
#define USBDCM_ADDR_DAC0_10mAT  6
#define USBDCM_ADDR_CAL_SARPID  10
#define USBDCM_ADDR_CAL_KP      11
#define USBDCM_ADDR_CAL_KI      15
#define USBDCM_ADDR_ADC_D0      19
#define USBDCM_ADDR_ADC_VREF    23
#define USBDCM_ADDR_ADC_DREF    27
#define USBDCM_ADDR_DAC_CAL     31
#define USBDCM_ADDR_ADC_CAL     32
#define USBDCM_ADDR_SM          33
#define USBDCM_ADDR_CAL_IMEAS1A 34
#define USBDCM_ADDR_CAL_P05     38
#define USBDCM_ADDR_CAL_P15     42
#define USBDCM_ADDR_CAL_N15     46
#define USBDCM_ADDR_DAC_NULL	50

#endif


