/*---------------------------------------------------------------------------------------------------------*\
 File:		isr.c

 Purpose:	ISRs for USBDCM boot

 Author:	andrea.cantone@cern.ch

 History:
    20/04/05	mc	Created
    28/11/06    pfr	Modified for FGC3
    11/12/07    ac  modified for USBDCM
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/* Declare interrupt functions */

#pragma INTERRUPT  IsrTA0
#pragma INTERRUPT  IsrUART0
#pragma INTERRUPT  IsrADC

#pragma INTERRUPT  IsrTick
#pragma INTERRUPT  IsrFip
#pragma INTERRUPT  IsrDummy1

/*---------------------------------------------------------------------------------------------------------*/
void IsrTA0(void)																			//this interrupt happens every 100us
{
    static unsigned int i = 0;

    p15_0 = 1;																				//test signal

    serialTimer0();																			//decrements different timers
    simpleshellTimer();
    usbdcmTimer();

    if (usbdcm_cnt_iadc > 0)
    {
        usbdcm_cnt_iadc--;
    }
    else
    {
        ad0con0 |= USBDCM_ADSTART;															//trigger internal ADCs
        usbdcm_cnt_iadc = USBDCM_CNT_IADC_RELOAD;
    }
    p15_0 = 0;
    p15_7 = usbdcm_smch ? 0 : 1;       														//cal active alarm, optocoupler is normally active and cuts when a channel is activated, logic inverted the 30/07
}

/*---------------------------------------------------------------------------------------------------------*/
void IsrUART0(void)
{
    static char ch;

    ch = u0rb;                                                                          	//save byte from receive register

	p11_2 = 1;                                                                          	//toggle ADC clock
	p11_2 = 0;

	serial_rxbuf0[serial_pirx0] = ch;                                                   	//save the received byte in the rx circular buffer

	if (serial_rxcnt0 == 0)
	{
		serial_rxcnt0 = 1;                                                              	//First byte received
	}
	else
	{
		if (serial_pirx0 == serial_porx0)													//? not sure when this happens? from previous versions
		{
			serial_porx0++;																	//update the first received byte pointer
			if (serial_porx0 >= SERIAL_BUFLEN)
			{
				serial_porx0 = 0;
			}
		}
		else
		{
			serial_rxcnt0++;                                                            	//bytes received so we increment the byte counter
		}
	}
	serial_pirx0++;                                                                     	//increment the last received byte pointer
	if (serial_pirx0 >= SERIAL_BUFLEN)
	{                                                                                   	//got to end of circular buffer
		serial_pirx0 = 0;
	}

    if (USBDCM_longop)
    {
        u0tb = 'B';																			//indicates the software is busy executing a long command
        serial_pirx0 = 0;         															//added 01/2012 miguel
        serial_porx0 = 0;
        serial_rxcnt0 = 0;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void IsrADC(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    p15_1 = 1;
    usbdcm_raw_p05 = ad01;																	//read internal ADCs
    usbdcm_raw_p15 = ad02;
    usbdcm_raw_m15 = ad03;
    usbdcm_flag_iadc = 1;
    p15_1 = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrTick(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{

}
/*---------------------------------------------------------------------------------------------------------*/
void IsrFip(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrDummy1(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/

