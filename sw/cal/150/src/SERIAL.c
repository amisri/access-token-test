#define __SERIAL_c__

#include <main.h>


void serialComInit0(void)
{
    static int i;

    serial_state0 = SERIAL_ST_WAITING;														//initialise state machine
    serial_wcnt0  = 0;
    for (i=0; i<SERIAL_BUFLEN; i++)
    {
        serial_rxbuf0[i] = 0;
        serial_txbuf0[i] = 0;
    }
    serial_pitx0 = 0;																		// last byte to transmit pointer
    serial_potx0 = 0;																		// first byte to transmit pointer
    serial_pirx0 = 0;																		// last byte to receive pointer
    serial_porx0 = 0;																		// first byte to receive pointer
    serial_txbuffull0 = 0;
    serial_rxcnt0     = 0;
}

void serialTimer0(void)
{
    if (serial_wcnt0 > 0)
    {
        serial_wcnt0--;
    }

    if (diag_cnt > 0)
    {
        diag_cnt--;
    }
}

int serialDataAvailable0(void)
{
    return serial_rxcnt0;																				//returns number of received bytes
}

int serialTx0(char far *p, int l)																		//l is -1 for strings otherwise gives the lenght
{
    static int i,n;

    i = 0;
    if (!serial_txbuffull0)
    {
        do
        {
            serial_txbuf0[serial_pitx0] = p[i];															//puts a byte in transmit buffer
            serial_pitx0++;																				//updates last byte to transmit pointer
            i++;
            if (serial_pitx0 >= SERIAL_BUFLEN)															//circular buffer
            {
                serial_pitx0 = 0;
            }
            if (serial_pitx0 == serial_potx0)															//buffer is full
            {
                serial_txbuffull0 = 1;
            }
            if (l >= 0)
            {
                if (i == l) break;																		//finished filling the buffer
            }
            else
            {
                if (!p[i]) break;
            }
        }
        while (1);
    }
    return i;
}

int serialRx0(char far *p, int n)
{
    int i;

    for (i=0; i<n; i++)
    {
        p[i] = serial_rxbuf0[serial_porx0];																//gets data from receive buffer
        p[i+1] = 0;
        serial_rxcnt0--;
        serial_porx0++;																					//updates first byte pointer as it gets the data from buffer
        if (serial_porx0 >= SERIAL_BUFLEN)
        {
            serial_porx0 = 0;
        }
        if (serial_porx0 == serial_pirx0)																//breaks when it reaches the last byte to receive
        {
            break;
        }
    }
    return i;
}

char serialFlushTimer0(void)
{

  switch (serial_state0)
  {
    case SERIAL_ST_WAITING:																				//if in waiting state
        if (serial_potx0 != serial_pitx0)
        {                                                                                   			//if there are bytes to be transmitted
            serial_state0 = SERIAL_ST_TXB;                                                  			//then move to transmit state
        }
        break;
    case SERIAL_ST_TXB:                                                                     			//if in transmit state
        u0tb = serial_txbuf0[serial_potx0];                                                 			//then transmit byte
        while (ti_u0c1 == 0);		                                                        			//wait until the buffer is empty
        serial_potx0++;                                                                     			//update pointer
        if (serial_potx0 >= SERIAL_BUFLEN)                                                  			//manage circular buffer
        {
            serial_potx0 = 0;
        }
        if (serial_potx0 == serial_pitx0)
        {                                                                                   			//no more bytes to transmit
            serial_state0 = SERIAL_ST_WAITING;
        }
        break;
    }
}

