/*---------------------------------------------------------------------------------------------------------*\
 File:		main.c

 Purpose:	USBDCM main programs

 Authors:	andrea.cantone@cern.ch, miguel.cerqueira.bastos@cern.ch

 History:
    12/05/06	 pfr	Created
    28/11/06    pfr	Modified for FGC3 maquette
    11/12/07    ac  modified for USBDCM
    19/05/2011  mcb modified for USBDCM
\*---------------------------------------------------------------------------------------------------------*/
/*
 copyright CERN 2014. The software is licensed under the terms copied verbatim in the file: LICENCE.TXT provided in every distribution
 of the software. In applying this licence, CERN does not waive the privileges and immunities  granted to it by virtue of its status
 as an Intergovernmental Organization or submit itself to any jurisdiction
 */

#define GLOBAL_VARS
#define FGC_GLOBALS

#define __main_c__

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  MAIN
\*---------------------------------------------------------------------------------------------------------*/
{

    float t, av_p05 = 0.0, av_p15 = 0.0, av_n15 = 0.0;
    static int d;
    static int  n;
    static char c[1];

    Init();                                                                                	//initialises configuration registers: I/O, adc, uart, timers
    serialComInit0();                                                                      	//initialise buffers and pointers for serial com
    simpleshellVarInit();                                                                  	//initialise state machine variables
    simpleshellDirectInit();																//initialise state machine that analyses the syntax of the commands and triggers the parsing
    usbdcmInitFlash();																		//initialise flash memory
    EnableIsr();
    usbdcmInit();                                                                          	//initialise most control variables, send a reset to the CPLD
    term_mode = DIRECT_MODE;																//start in direct mode - see fgc serial protocol

    for(;;) {
        if (!status_cnt && (term_mode == DIAG_MODE))
        {
            status_cnt = 20000;                                                             //count 2s - uses the usbdcmtimer function executed in TA0
            simpleshellReadAllStatus();                                                     //read all alarms
            simpleshellReadAllAdcs();														//read all ADCs
        }

        if ((term_mode == DIAG_MODE) && (serial_potx0 == serial_pitx0) && !diag_cnt)		//if it is in diagnostics mode and it has finished transmitting data
        {
            simpleshellDiagRun();                                                           //re-fill the buffer with latest published data
            diag_cnt = 2000;																//count 200ms - uses the serialTimer0 function executed in TA0
        }
        SERIAL_FLUSH0;                                                                      //sends 1 byte at a time from the serial output buffer

        n = serialDataAvailable0();                                                        	//returns number of unread bytes in the receive buffer
        if (n)
        {																					//data is available: process it
            serialRx0((const char far *)c, 1);                                              //c is pointer to first byte from receive buffer
            switch(c[0])																	//determine mode
            {
                case '!':				                                                    // '!'    : Direct Mode
	            case 0x1A:				                                                    // Ctrl-Z : Direct Mode
	                if(term_mode != DIRECT_MODE)		                                    // If not in direct mode
	                {
		                term_mode = DIRECT_MODE;			                                // Switch to direct mode
                        simpleshellDirectInit();
	                }
	                break;
	            case 0x1B:				                                                    // ESC : Editor Mode
	                if(term_mode != EDITOR_MODE)		                                    // If not in editor mode
	                {
		                term_mode = EDITOR_MODE;			                                // Switch to editor mode
                        simpleshellVarInit();
		                continue;
	                }
	                break;
                case 0x19:				                                                    // Ctrl-Y : Direct Mode
	                if(term_mode != DIAG_MODE)		                                        // If not in diag mode
                    {
		                term_mode = DIAG_MODE;			                                    // Switch to diag mode
                        simpleshellDiagInit();
	                }
	                break;
            }

    	    switch(term_mode)																//depending on mode run the appropriate routines
            {
	            case DIRECT_MODE:
	                simpleshellDirectRun(c);
	                break;
	            case EDITOR_MODE:
                    simpleshellRun(c);
	                break;
                case DIAG_MODE:
	                break;
	        }

        }

        if (usbdcm_flag_iadc)
        {                                                                                   //TRUE when there is a new ADC reading - from IsrADC
        	for (d=0; d<5; d++)
        	{
        		usbdcm_volts_p05 = usbdcm_raw_p05 * GAIN_ADCINT_P05;						//the power supply voltages are read and scaled to 0..5V
        		usbdcm_volts_p15 = usbdcm_raw_p15 * GAIN_ADCINT_P15;
        		t = usbdcm_raw_m15 * GAIN_ADCINT_NT15;
        		usbdcm_volts_m15 = t - (usbdcm_volts_p15 - t) * GAIN_ADCINT_N15;
        		av_p05 = av_p05 + usbdcm_volts_p05;											//average 5 readings
        		av_p15 = av_p15 + usbdcm_volts_p15;
        		av_n15 = av_n15 + usbdcm_volts_m15;
        	}
        	usbdcm_volts_p05 = av_p05 / 5;
        	usbdcm_volts_p15 = av_p15 / 5;
        	usbdcm_volts_m15 = av_n15 / 5;
        	av_p05 = 0;
        	av_p15 = 0;
        	av_n15 = 0;
        	usbdcm_flag_iadc = 0;
            if (usbdcm_volts_p05 < 4)														//if power goes below 4V, then the CDC is unpowered
            {
               usbdcm_csec = 0;																//initialize all relevants variables
               usbdcm_npri  = 0;															//describing the CDC and SM state
               usbdcm_nsec  = 0;
               usbdcm_fs    = 0.0;
               usbdcm_sm_relays = 0;
               usbdcm_smch = 0;
               for (d=0; d<16; d++) sm_relay[d] = 0;
            }
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void EnableIsr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will enable needed ISR for the program
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  dummy_ch;

    ta0ic   =   0x04;				                                                        // Timer A0 source for OS tick interrupt, Level 4

    /* UART0 */
    s0ric   =   0x05;				                                                        //  5 : UART0 Receive interrupt, level 5
    //u0tb    =   0x00;
    //s0tic  =   0x03;				                                                        //  3 : UART0 Tx buffer empty (terminal display)

    /* UART2 */
    //s2ric   =   0x07;				                                                        //  7 : UART2 Rx
    //u2tb    =   0x00;                                                                     // enable receive interrupts

    /* adc */
    ad0ic   = 0x03;																			// enable internal ADC interrupt, level 3

}
/*---------------------------------------------------------------------------------------------------------*/
void Init(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare all the task for execution under uC/OS-II.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* ---  Port 0 --- NOT USED*/
    pd0		=   0x00;																		//port p0 all inputs

    /*  --- Port 1 --- CONTROL ADC*/
    pd1		=   0xFF;	                                                                    // Port p1 all outputs
    p1      =   0x00;																		//initialise

    /* ---  Port 2 --- CONTROL DAC*/
    pd2		=   0x77;	                                                                    // MSB -> LSB : -,dac2in,dac2sclk,dac2scs,-,dac1in,dac1sclk,dac1cs,
    p2      =   0x11;                                                                       // CS = 1 -> DACs not selected

    /* ---  Port 3 --- DOUT */
    pd3		=   0xFF;	                                                                    // Port p3 all outputs - DOUT
    p3      =   0x00;																		// Initialise

    /* --- Port 4 ---  DIN */
    pd4		=   0x00;	                                                                    // Port p4 all inputs - DIN

    /* --- Port 5 --- NOT USED */
    pd5		=   0x00;	                                                                    // Port p5 all inputs

    /*  --- Port 6 ---  */
//    pd6_2 	=   0x00;	                                                                    // Term RX: input
//    pd6_3	=   0x01;	                                                                    // Term TX: output
//    pd6_6 	=   0x00;	                                                                    // Dev RX
//    pd6_7	=   0x01;	                                                                    // Dev TX
//    p6_3	=   0x00;																		// Initialise
    /*--- UART0 - communication with terminal ---*/
    ps0_2	=   0x00;                                                                       // P6 bit 2 is dedicated to the UART0 Rx
    ps0_3	=   0x01;	                                                                    // P6 bit 3 is dedicated to the UART0 Tx

    u0mr	=   0x05;	                                                                	// UART, Int clk, 1 stop bit, no parity
    u0c0	=   0x10;	                                                                	// Use f1, CTS/RTS disabled, transfer LSB first
    u0brg	=   0x96;	                                                                    // Set u0brg = f1/(16*baud) - 1   (0x9C = 9600 when f1=24MHz)
    u0c1	=   0x05;	                                                                	// Enable Rx and Tx
    //ifsr6   =   1;
    /*--- UART1 - programming port, not used in RUN mode ---*/

    /*  --- Port 7 ---  */
    //pd7 	=   0x00;
    /* --- UART2 - communication with ID chip --- */
    ps1_1	=   0x00;                                                                   	// P7 bit 1 is dedicated to the UART2 Rx
    ps1_0	=   0x01;	                                                                    // P7 bit 0 is dedicated to the UART2 Tx
    u2mr	=   0x05;	                                                                	// UART, Int clk, 1 stop bit, no parity
    u2c0	=   0x10;	                                                                	// Use f1, CTS/RTS disabled, transfer LSB first
    u2brg	=   0x96;	                                                                    // Set u0brg = f1/(16*baud) - 1   (0x9C = 9600 when f1=24MHz)
    u2c1	=   0x05;	                                                                	// Enable Rx and Tx

    /*--- Port 8 --- INTERRUPTS ---*/
    pd8		=   0x00;	                                                                    // Port p8 all inputs

    /*  --- Port 9 --- */
    prc2	= 	0x01;
    pd9 	= 	0x0d;                                                                       // p90: out clk
                                                                                       	   	// p91: in  dout (from flash to uC)
                                                                                            // p92: out din  (from uc to flash)
                                                                                            // p93: out cs
    /* --- Port 10 --- Test points ---*/
    pd10	= 	0x00;                                                                       // Port p10 all inputs
    p10		= 	0x00;                                                                       // Port p10 initialise
    /*--- internal ADC ---*/
    usbdcmInternalAdcInit();																// Set internal ADCs

    /*--- Port 11 --- CONTROL ADC  ---*/
    pd11	=   0x06;	                                                                    // P11_2: DLCK for ADC, P11_0 CS for ADC
    p11     =   0x06;                                                                       // ADC not selected, clk high

    /*--- Port 12 --- NOTUSED ---*/
    pd12	=   0x00;	        															// Port 12 all inputs

    /*--- Port 13 --- NOTUSED ---*/
	pd13	=   0x00;	        															// Port 13 all inputs

    /*--- Port 14 --- NOTUSED ---*/
    pd14	=   0x00;																		// Port 14 All inputs

    /*--- Port 15 --- TEST POINTS ---*/
    pd15	=   0xFF;																		// Port 15 All outputs

    /*--- Timer A4 - Free running counter at 1 Mhz (uses Timer A3 as input) - used by usleep() macro ---*/

    ta4mr	=   0x41;	                                                                    // Timer mode register: event counter mode, free running
    trgsr	=   0x80;	                                                                    // Trigger select register: Timer A4 uses TA3 overflow
    udf	        =   0x10;	                                                                // Increment counter for Timer A4
    ta4s        =   0x01;	                                                                // Start timer A4

    /*--- Timer A0 -  10Khz clock ---*/

    ta0mr   =   0x00;
    ta0		=   CALC_TMR(10000);															//Timer A0 set to 100us
    ta0s    =   0x01;	                                                                    // Start timer A0

}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/

