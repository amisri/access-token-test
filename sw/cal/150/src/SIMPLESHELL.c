#define __SIMPLESHELL_c__

#include <main.h>

/*
Diagnostic mode is activated on reception of a byte with value 0x19
In diagnostic mode a structure fgc_diag is transmitted every 200ms
fgc_diag is defined in the file z:\projects\fgc\sw\inc\fcg_stat.h

struct fgc_diag
{
    uint32_t    time_sec;                       // [0x00] Unix time (s)
    uint32_t    time_usec;                      // [0x04] ms time (0, 200, 400, 600, 800)
    uint8_t     data_status;                    // [0x08] Class data status
    uint8_t     class_id;                       // [0x09] Class ID for the device
    uint8_t     reserved[2];                    // [0x0A] Padding

    union fgc_pub_class_data class_data;        // [0x0C] Class specific data

    uint8_t     chan[FGC_DIAG_PERIOD][FGC_DIAG_N_LISTS];        // [0x34] Diag channel numbers
    uint16_t    data[FGC_DIAG_PERIOD][FGC_DIAG_N_LISTS];        // [0x5C] Diag data
};

Where the class data is, for the CDC and SM, defined in
z:\projects\fgc\sw\inc\classes\150\150_stat.h
z:\projects\fgc\sw\inc\classes\151\151_stat.h

struct fgc150_stat
{
    uint16_t st_faults       ;  // Faults
    uint16_t st_warnings     ;  // Warnings
    float    i_ref           ;  // Current reference (I)
    float    i_meas          ;  // Current measurement (I)
    float    v_meas          ;  // Voltage measurement (V)
    uint32_t output          ;  // Output mode
    uint8_t  reserved0[20];
};

struct fgc151_stat
{
    uint16_t st_faults       ;  // Faults
    uint16_t st_warnings     ;  // Warnings
    float    v_meas          ;  // Voltage measurement (V)
    float    i_null_ua       ;  // Current measurement (I) in uA
    uint8_t  reserved0[28];
};

All fields must be in big endian order

*/

void simpleshellDiagInit(void)
{
    static int i,j;

    serial_potx0 = serial_pitx0;                                                      //re-initialise serial
    serial_state0 = SERIAL_ST_WAITING;
    for (i = 0; i < FGC_DIAG_N_SYNC_BYTES; i++)                                       //Prepare the start sequence for the diag data
    {
        DIAG_data0.start_seq[i] = FGC_DIAG_SYNC;                                      //prepare the struct that contains the diag data
    }                                                                                 //this struct is composed by the start sequence                                                                                    //plus the diag structure defined in fgc_stat.h
    diagdata.time_sec = 0;
    diagdata.time_usec = 0;
    diagdata.data_status = 0;
    diagdata.class_id = usbdcm_sm ? 151 : 150;
    diagdata.reserved[0] = 0;
    diagdata.reserved[1] = 0;
    for (i = 0; i < (FGC_DIAG_PERIOD); i++)
    {
        for (j = 0; j < (FGC_DIAG_N_LISTS); j++)
        {
            diagdata.chan[i][j] = 0;
            diagdata.data[i][j] = 0;
        }
    }
}

void simpleshellDiagRun(void)
{
   static int i;
                                                                                        //prepare the sm struct with the diag data
    if (usbdcm_sm)
    {
        sm_pubdata.st_faults = sm_faults_msk;
        usbdcmSwapbytes(&(sm_pubdata.st_faults), sizeof (sm_pubdata.st_faults));
        sm_pubdata.st_warnings = 0;
        sm_pubdata.v_meas = sm_vout;
        usbdcmSwapbytes(&(sm_pubdata.v_meas), sizeof (sm_pubdata.v_meas));
        sm_pubdata.i_null_ua = sm_inull;
        usbdcmSwapbytes(&(sm_pubdata.i_null_ua), sizeof (sm_pubdata.i_null_ua));
        for (i = 0; i < 28; i++)
        {
            sm_pubdata.reserved0[i] = 0;
        }
        diagdata.class_data.c151 = sm_pubdata;
    }
    else
    {                                                                                	//prepare the cdc struct with the diag data
        cdc_pubdata.st_faults = cdc_faults_msk;
        usbdcmSwapbytes(&(cdc_pubdata.st_faults), sizeof (cdc_pubdata.st_faults));   	//FGCRUN+ is big endian, so we need to swap bytes
        cdc_pubdata.st_warnings = cdc_warns_msk;
        usbdcmSwapbytes(&(cdc_pubdata.st_warnings), sizeof (cdc_pubdata.st_warnings));
        cdc_pubdata.i_ref = (float)usbdcm_current;
        usbdcmSwapbytes(&(cdc_pubdata.i_ref), sizeof (cdc_pubdata.i_ref));
        cdc_pubdata.i_meas = cdc_iout;
        usbdcmSwapbytes(&(cdc_pubdata.i_meas), sizeof (cdc_pubdata.i_meas));
        cdc_pubdata.v_meas = cdc_vout;
        usbdcmSwapbytes(&(cdc_pubdata.v_meas), sizeof (cdc_pubdata.v_meas));
        cdc_pubdata.output = usbdcm_nsec;  //052014MIGUEL it was csec but that variable is actually the desired range and not the real range
        usbdcmSwapbytes(&(cdc_pubdata.output), sizeof (cdc_pubdata.output));
        for (i = 0; i < 20; i++)
        {
            cdc_pubdata.reserved0[i] = 0;
        }
        diagdata.class_data.c150 = cdc_pubdata;
    }

    DIAG_data0.diagdata0 = diagdata;
    diag_outstr = (char far *) &DIAG_data0;												//point to the structure
    serialTx0(diag_outstr, (FGC_DIAG_N_SYNC_BYTES + 172));                             	//fill in the send buffer

}

void simpleshellDirectInit(void)
{
    static int i;

    direct_strCmd[0] = 0;                                                               //command read from the terminal
    direct_lCmd      = 0;                                                               //composed in SIMPLESHELL_inchar
    direct_cursor    = 0;
    direct_st = 0;
    direct_space = 0;
    serial_potx0 = serial_pitx0;                                                        //re-initialise serial
    serial_state0 = SERIAL_ST_WAITING;
}

/*------------------simpleshellDirectRun----------------------------------------*/
/* This is the state machine that analyses the syntax of the commands
and triggers the parsing. This state machine is used in both DIRECT and
EDITOR modes.The difference is that in DIRECT mode characters are sent
directly here, while in EDITOR mode they pass first through the function
simpleshellRun, which implements the terminal -> verifies if it is a special
character or a normal character and if it is a normal character builds a
string which is then sent to simpleshellDirectRun (char by char) to be checked
for syntax and parsed.
*/

void simpleshellDirectRun(const char far *c)
{
    static char ch;
    static char cmok;
    static char far *point;
    char auxch[2];

    if (c[0] >= 'a' && c[0] <= 'z')
    {
        c[0] = c[0] - 32;                                                               //turns lower case into upper case
    }                                                                                   //making it case unsensitive
    ch = c[0];

    switch (ch)
    {
        case '}':
        case '{':
        case '$':
            return;
        case ' ':                                                                       //space
            direct_space = 1;                                                           //set space flag
            return;
        case '!':                                                                       //new command
            direct_st = DIRECT_CMDSTART;                                                //change state to CMD START
            return;                                                                     //start new command
        case '\n':                                                                      //newline
        case '\r':
        case ';':                                                                       //carriage return
            ch = ';';
            break;                                                                      //Replace with execute character ';' and fall through
        default:                                                                        //All other characters
            if(ch < 0x20)
            {                                                                           //Ignore all control characters
                return;
            }
            break;
    }

    switch (direct_st)
    {                                                                                   //declare state variable and states
        case 0:
            if (term_mode == DIRECT_MODE)
            {                                                                           //before the "!" we just ignore in direct mode
                if(ch == ';')
                {                                                                       //End of command
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_NO_SYMBOL, fgc_errmsg[FGC_NO_SYMBOL]);
                    break;
                }
                else
                {
                    return;
                }
            }
            else
            {                                                                           //in editor mode the commands always get here with a "!"
                break;
            }
        case DIRECT_CMDSTART:                                                           //because it is internally generated
            if (ch == 'S' || ch == 'G')
            {                                                                           //so we only get to state zero when there was an error
                //simpleshellDirectCharIns(ch);                                                   //during this state machine
                set_cmd = 0;                                                            //in that case we need to wait for the semi colon
                if (ch == 'S') set_cmd = 1;                                             //to print the error message
                direct_st = DIRECT_CMDDFN;
                direct_space = 0;
                return;
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                "%i %s", FGC_UNKNOWN_CMD, fgc_errmsg[FGC_UNKNOWN_CMD]);
            }
            break;
        case DIRECT_CMDDFN:
            if (!direct_space)
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_UNKNOWN_CMD, fgc_errmsg[FGC_UNKNOWN_CMD]);
                break;
            }
            if((ch < 'A' || ch > 'Z') && (ch!=';'))
            {                                                                               //if first char is not valid and not a semi colon
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_UNKNOWN_SYM, fgc_errmsg[FGC_UNKNOWN_SYM]);                                                // Report unknown symbol
                break;
            }                                                                               //if semi colon or valid char, go to next state and parse
            direct_st = DIRECT_CMDRCV;
            direct_space = 0;                                                               //no break -> fall through

        case DIRECT_CMDRCV:
            if(ch == ';')
            {                                                                               // End of command
                if (direct_lCmd)
                {
                    simpleshellDirectCharIns('\0');                                                   //terminate the string
                    cmok = simpleshellParse(direct_strCmd, direct_lCmd);                   //parse the commands
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_NO_SYMBOL, fgc_errmsg[FGC_NO_SYMBOL]);
                }
                break;                                                                      //state machine can go back to beginning
            }
            if (direct_space)
            {
                simpleshellDirectCharIns(' ');
                direct_space = 0;                                                           //insert space in string to parse
            }
            simpleshellDirectCharIns(ch);                                                             //insert character in string to parse
            return;
    }

   if (term_mode == DIRECT_MODE)
   {                                                                                        //direct mode
      if (!cmok)
      {                                                                                     //let's build the error message
         sprintf((const char far *)direct_outstr,
            "$$");
         strcat(direct_outstr, simpleshell_outstr);
         strcat(direct_outstr, "\n!");
      }
      else
      {                                                                                     //let's build the response message
         sprintf((const char far *)direct_outstr,
            "$");
         strcat(direct_outstr, simpleshell_outstr);
         strcat(direct_outstr, "\n;");
      }
   }
   else
   {                                                                                        //editor mode
      sprintf((const char far *)direct_outstr,
            "\n\r");
      point = simpleshell_outstr;                                                           //pointer to beginning of response string
      while (*point)
      {                                                                                     //analyse character by character
         if (*point == '\n')
         {                                                                                  //replace the \n per \n\r
            strcat(direct_outstr, "\n\r");                                                  //only happens in case of multiple lines
         }
         else
         {
            auxch[0] = *point;                                                              //for normal characters
            auxch [1] = '\0';
            strcat(direct_outstr, auxch);
         }
         point++;
      }
   }                                                                                        //ensures the err mess is only print once
   if (ch == ';') serialTx0(direct_outstr, -1);                                            	//when the semi colon is received
   direct_strCmd[0] = 0;                                                                    //command read from the terminal
   direct_lCmd      = 0;                                                                    //(composed in SIMPLESHELL_inchar)
   direct_cursor    = 0;                                                                    //reinitialise state
   direct_st = 0;
   direct_space = 0;

}

void simpleshellVarInit(void)
{
    static int i;

    serial_potx0 = serial_pitx0;                                                            //re-initilise serial
    serial_state0 = SERIAL_ST_WAITING;
    simpleshell_strCmd[0] = 0;                                                              //command read from the terminal
    simpleshell_lCmd      = 0;                                                              //(composed in SIMPLESHELL_inchar)
    simpleshell_cursor    = 0;
    simpleshell_strtmp[0] = 0;
    simpleshell_state     = SIMPLESHELL_ST_OUT;
    simpleshell_echo      = 1;
    simpleshell_cnt       = 0;
    simpleshell_secrelays = 0;
    simpleshell_st        = SIMPLESHELL_ST_NORM;
    for (i=0; i<SIMPLESHELL_CMDHIST_SIZE; i++)
    {
        simpleshell_cmdhistory[i][0] = 0;
    }
    simpleshell_pcmdhist = 0;
    strcpy(simpleshell_str, SIMPLESHELL_PROMPT);
    if (simpleshell_echo)
    {
        serialTx0(simpleshell_str, -1);
    }
}

/*-----------------------simpleshellRun-----------------------------------------*/
/*This is the state machine that implements the terminal function in EDITOR mode
if the recceived is not a special character, then it is added to simpleshell_strCmd
until LF, CR or SC (;) are received, in case which the string is added the delimiter
characters and sent char by char to the function simpleshellDirectRun to be syntax analysed and
parsed */

void simpleshellRun(const char far *c)
{
    static char r;
    static int  i;
    static char far *chr;
    static char processed;

    switch (simpleshell_st)
    {                                                                                       //state machine
        case SIMPLESHELL_ST_NORM:                                                           //editor mode normal state
            processed = 0;
            if ((c[0] == SIMPLESHELL_LF) || (c[0] == SIMPLESHELL_CR) || (c[0] == SIMPLESHELL_SC))
            {
                simpleshell_st = SIMPLESHELL_ST_NORM;                                       //if linefeed or carriage return then go to normal state
                if (simpleshell_lCmd > 0)
                {                                                                           //check if there is text to parse
                    for (i=SIMPLESHELL_CMDHIST_SIZE-1; i>0; i--)
                    {                                                                       //store the commands in the history buffer
                        strcpy(simpleshell_cmdhistory[i], simpleshell_cmdhistory[i-1]);
                    }
                    strcpy(simpleshell_cmdhistory[i], simpleshell_strCmd);

                    chr = simpleshell_strCmd - 1;
                    *chr = '!';                                                             //Submit character to direct mode state machine
                    simpleshellDirectRun(chr);                                                        //which will do the parsing
                    chr++;                                                                  //but we stay in editor mode !
                    while(*chr)
                    {                                                                       //loop while characters remain
                        simpleshellDirectRun(chr);
                        chr++;
                    }
                    *chr = ';';
                    simpleshellDirectRun(chr);

                    simpleshell_lCmd      = 0;
                    simpleshell_cursor    = 0;
                    simpleshell_strCmd[0] = 0;
                    simpleshell_pcmdhist  = -1;
                    if (simpleshell_echo) {
                        serialTx0((const char far *)SIMPLESHELL_PROMPT, -1);               //prompt for next line
                    }
                }
                else
                {
                    if (simpleshell_echo)
                    {                                                                       //if no text to parse then just echo NO COMMAND
                        strcpy((const char far *)simpleshell_str, "\r\nno command");
                        serialTx0((const char far *)simpleshell_str, -1);
                        serialTx0((const char far *)SIMPLESHELL_PROMPT, -1);
                    }
                }
                processed = 1;
            }                                                                               //if we receive an
            if (c[0] == SIMPLESHELL_ESC)
            {                                                                               //ESC we change STATE to check if it is the
                simpleshell_st  = SIMPLESHELL_ST_ESC0;                                       //beginning of an escape sequence or not
                simpleshell_cnt = SIMPLESHELL_cntESC;
                processed = 1;
            }
            if (c[0] == SIMPLESHELL_BS)
            {                                                                               //backspace - we stay in the same STATE
                simpleshell_st  = SIMPLESHELL_ST_NORM;                                      //and just delete
                if (simpleshell_cursor > 0)
                {
                    simpleshell_cursor--;
                    simpleshellCursorLeft();
                    simpleshellCharDel();
                    serialTx0((const char far *)&simpleshell_strCmd[simpleshell_cursor], -1);
                    simpleshell_str[0] = ' ';
                    serialTx0((const char far *)simpleshell_str, 1);
                    for (i=simpleshell_lCmd; i>=simpleshell_cursor; i--)
                    {
                        simpleshellCursorLeft();
                    }
                }
                processed = 1;
            }
            if (!processed)
            {
                simpleshellCharIns(c[0]);                                                  //not special character, then it is part of the command
                if (simpleshell_echo && (c[0] != SIMPLESHELL_ESC))
                {
                    serialTx0((const char far *)&simpleshell_strCmd[simpleshell_cursor - 1], -1);
                    for (i=simpleshell_lCmd-1; i>=simpleshell_cursor; i--)
                    {
                        simpleshellCursorLeft();
                    }
                }
            }
            break;

        case SIMPLESHELL_ST_ESC0:                                                            //check if it is an escape sequence
            processed = 0;
            if (c[0] == '[')
            {                                                                               //if we receive "[" this means it is
                simpleshell_st = SIMPLESHELL_ST_ESC1;                                       //an escape sequence for cursor or function keys
                processed = 1;                                                              //let's go to stESC1 to check
            }
            if (c[0] == SIMPLESHELL_ESC)
            {                                                                               //another ESC brings us back to normal state
                if (simpleshell_cnt > 0)
                {
                    simpleshell_lCmd      = 0;
                    simpleshell_cursor    = 0;
                    simpleshell_strCmd[0] = 0;
                    simpleshell_pcmdhist  = -1;
                    simpleshellCleanLine();
                    simpleshell_st = SIMPLESHELL_ST_NORM;
                }
                processed = 1;
            }
            if (c[0] == 'O')
            {                                                                               //if we receive "O" this means it is
                simpleshell_st = SIMPLESHELL_ST_ESC2;                                       //an escape sequence for PF1-PF4
                processed = 1;                                                              //let's go to stESC2 to ignore
            }
            if (!processed)
            {
                simpleshellCharIns(c[0]);
                simpleshell_st = SIMPLESHELL_ST_NORM;
                if (simpleshell_echo && (c[0] != SIMPLESHELL_ESC))
                {
                    serialTx0((const char far *)&simpleshell_strCmd[simpleshell_cursor - 1], -1);
                    for (i=simpleshell_lCmd-1; i>=simpleshell_cursor; i--)
                    {
                        simpleshellCursorLeft();
                    }
                }
            }
            break;

        case SIMPLESHELL_ST_ESC1:
            if (c[0] == 'A')                                                                // Cursor up
            {                                                                               // cursor keys have the escape sequence
                simpleshell_st = SIMPLESHELL_ST_NORM;                                       // "ESC[A" to "ESC[D"
                if (simpleshell_pcmdhist < SIMPLESHELL_CMDHIST_SIZE - 1)
                {
                    simpleshell_pcmdhist++;
                }
                strcpy(simpleshell_strCmd, simpleshell_cmdhistory[simpleshell_pcmdhist]);
                simpleshell_lCmd = strlen(simpleshell_strCmd);
                simpleshell_cursor = simpleshell_lCmd;
                simpleshellCleanLine();
                if (simpleshell_echo)
                {
                    serialTx0((const char far *)simpleshell_strCmd, -1);
                }
            }
            if (c[0] == 'B')
            {
            																				// Cursor down
                simpleshell_st = SIMPLESHELL_ST_NORM;
                if (simpleshell_pcmdhist > 0)
                {
                    simpleshell_pcmdhist--;
                }
                if (simpleshell_pcmdhist == -1)
                {
                    simpleshell_pcmdhist = 0;
                }
                strcpy(simpleshell_strCmd, simpleshell_cmdhistory[simpleshell_pcmdhist]);
                simpleshell_lCmd = strlen(simpleshell_strCmd);
                simpleshell_cursor = simpleshell_lCmd;
                simpleshellCleanLine();
                if (simpleshell_echo)
                {
                    serialTx0((const char far *)simpleshell_strCmd, -1);
                }
                //simpleshell_st = SIMPLESHELL_ST_NORM;
            }
            if (c[0] == 'C')                                                                // Cursor right
            {
                simpleshell_st = SIMPLESHELL_ST_NORM;
                if (simpleshell_cursor < simpleshell_lCmd)
                {
                    simpleshell_cursor++;
                    simpleshellCursorRight();
                }
            }
            if (c[0] == 'D')
            {                                                                               // Cursor left
                simpleshell_st = SIMPLESHELL_ST_NORM;
                if (simpleshell_cursor > 0)
                {
                    simpleshell_cursor--;
                    simpleshellCursorLeft();
                }
            }
            if (c[0] == '2')
            {                                                                               //this is supposed to be the home key but                                                                                    //on the VT100 documentation I find "ESC[1~" instead???
                simpleshell_st = SIMPLESHELL_ST_CHKHOME;                                    //also differences for the end and delete keys
                simpleshell_cnt = SIMPLESHELL_cntESC;                                       //but it seems to work !
            }
            if (c[0] == '5')
            {
                simpleshell_st = SIMPLESHELL_ST_CHKEND;
                simpleshell_cnt = SIMPLESHELL_cntESC;
            }
            if (c[0] == '4')
            {
                simpleshell_st = SIMPLESHELL_ST_CHKDEL;
                simpleshell_cnt = SIMPLESHELL_cntESC;
            }
            break;

        case SIMPLESHELL_ST_ESC2:
            simpleshell_st = SIMPLESHELL_ST_NORM;
            break;

        case SIMPLESHELL_ST_CHKHOME:
            if (c[0] == '~')
            {                                                                               // Cursor Home
                simpleshell_st = SIMPLESHELL_ST_NORM;
                while (simpleshell_cursor > 0)
                {
                    simpleshell_cursor--;
                    simpleshellCursorLeft();
                }
            }
            if (simpleshell_cnt == 0)
            {
                simpleshell_st = SIMPLESHELL_ST_NORM;
            }
            break;
        case SIMPLESHELL_ST_CHKEND:
            if (c[0] == '~')
            {                                                                               // Cursor End
                simpleshell_st = SIMPLESHELL_ST_NORM;
                while (simpleshell_cursor < simpleshell_lCmd)
                {
                    simpleshell_cursor++;
                    simpleshellCursorRight();
                }
            }
            if (simpleshell_cnt == 0)
            {
                simpleshell_st = SIMPLESHELL_ST_NORM;
            }
            break;
        case SIMPLESHELL_ST_CHKDEL:
            if (c[0] == '~') {
                if (simpleshell_cursor < simpleshell_lCmd) {                                // Delete character at cursor
                    simpleshellCharDel();
                    serialTx0((const char far *)&simpleshell_strCmd[simpleshell_cursor], -1);
                    simpleshell_str[0] = ' ';
                    serialTx0((const char far *)simpleshell_str, 1);
                    for (i=simpleshell_lCmd; i>=simpleshell_cursor; i--) {
                        simpleshellCursorLeft();
                    }
                }
                simpleshell_st = SIMPLESHELL_ST_NORM;
            }
            if (simpleshell_cnt == 0) {
                simpleshell_st = SIMPLESHELL_ST_NORM;
            }
            break;
    }
}

char simpleshellParse(char far *strCmd, char lCmd)
{
	static char             	outst[201];
	static char _far            *tk;
    static char _far            *p;
    static char _far            *tk_aux;
    static char                 cmdok,badgetopt,badprm;
    static char                 errmsg;
    static long int             ival;
    static float                fval, i_error;
    static int                  i,array_size,ch;
    static float                nullmeter;
    static unsigned char        addr;
    static unsigned char        data;

    p11_0 = 1;
    //sprintf((const char far *)simpleshell_outstr,"got here %s",strCmd);
  //strCmd = strupr(StrCmd);                                                                //this makes the parser case insensitive
    tk = strtok(strCmd, " ");                                                               //extracts first segment of string
    if (tk)
    {                                                                                       //must not be empty
        cmdok = 0;
        badgetopt = 0;
        badprm = 0;
        errmsg = 0;

        simpleshell_outstr[0] = '\0';

//----------------------------------------------------------------------------

        if (!usbdcm_sm)
        {                                                                                   	//verification of CDC or SM

//----------------------------CDC commands------------------------------------


            if(!strcmp(tk, "CALSYS.CDC.OUTPUT"))
            {                                                                           		// the argument is an enumeration
                cmdok = 1;																		//representing the secondary current range
                if (set_cmd)
                {
                    if (tk = strtok(NULL, " "))
                    {
                        usbdcm_csec = INVALID_RANGE;
                        for(i = OFF; i <= RANGE_10A; i++)										//check if the argument is a valid range
                        {
                            if (!strcmp(tk, cdc_out_range[i]))
                            {
                                usbdcm_csec = i;												//valid range found
                            }
                        }
                        ival = usbdcmReadStatus(simpleshell_strtmp);
                        if (vb_mode && (usbdcm_csec == RANGE_10A))								//VB only allowed in <5A mode
                        {
                        	usbdcm_csec = INVALID_RANGE;
                        }
                        if (!(tk = strtok(NULL, " ")))
                        {
							USBDCM_longop = 1;												//this is a long operation
							if (usbdcm_nsec && (usbdcm_csec != INVALID_RANGE))
							{                                                               //the CDC was already on and the requested range is valid
								errmsg = simpleshellSetCdc(OFF);							//in that case, the CDC is turned OFF first
							}
							switch (usbdcm_csec)											//this will prepared the variables to set the desired range
							{
								case OFF:
										simpleshell_secrelays = 0x00;
										usbdcm_npri  = 0;
										usbdcm_nsec  = 0;
										usbdcm_fs    = 0.0;
										break;
								case RANGE_1A:
										simpleshell_secrelays = 0x01;						//secondary relay setting for this range
										usbdcm_nsec           = 40;                         //secondary turns
										usbdcm_fs             = 1.0;                        //Full scale value
										usbdcm_kp             = 5;                          //Kp and Ki for all ranges were determined experimentally
										usbdcm_ki             = 25;
										break;
								case RANGE_2A5:
										simpleshell_secrelays = 0x02;
										usbdcm_nsec           = 16;
										usbdcm_fs             = 2.5;
										usbdcm_kp             = 2;
										usbdcm_ki             = 10;
										break;
								case RANGE_5A:
										simpleshell_secrelays = 0x04;
										usbdcm_nsec           = 8;
										usbdcm_fs             = 5.0;
										usbdcm_kp             = 1;
										usbdcm_ki             = 5;
										break;
								case RANGE_10A:
										simpleshell_secrelays = 0x08;
										usbdcm_nsec           = 4;
										usbdcm_fs             = 10.0;
										usbdcm_kp             = 0.5;
										usbdcm_ki             = 2.5;
										break;
								default:
										sprintf((const char far *)simpleshell_outstr,
											"%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
										errmsg = 1;
								break;
							}
							if (usbdcm_nsec && !errmsg)
							{                                                              		//check the CDC state, to see if we are allowed to switch the relays
								ival = usbdcmReadStatus(simpleshell_strtmp);               		//it needs to be in one of these states
								if((ival == NO_VB_CLP_ON) || (ival == NO_VB_CLP_ON_TWARN)
									|| (ival == VB_ON_CLP_ON) || (ival == VB_ON_CLP_ON_TWARN))
								{
									errmsg = simpleshellSetCdc(simpleshell_secrelays);			//CDC state is ok, we can set the CDC
									ival = usbdcmReadStatus(simpleshell_strtmp);				//MIGUEL062014 check if this helps in detecting the VB correctly in order to avoid autonull
									if (!vb_mode && !errmsg)
									{                                                   		//autonull
										usbdcmAutonullPid();
										if (!usbdcm_autonull || usbdcm_autonull_sat)
										{
											sprintf((const char far *)simpleshell_outstr,
												"%i %s", FGC_NULL_FAILED, fgc_errmsg[FGC_NULL_FAILED]);
											errmsg = 1;
										}
									}
									else
									{
										//if (!errmsg) usbdcmDacOutV(1, 0);						//In VB mode, ensure the DAc is at zero
										//this line was wrong so I comnented it, because the DAC should not be at zero but rather at the value of the last autonull
										//this value should also be memorized so that the CDC also starts with no offset
										//mcb 29-06-2016
									}
								}
								else															//CDC state is not OFF, we cannot set the relays
								{
									errmsg = simpleshellSetCdc(OFF);							//let's try to put it in a known state and give an error
									simpleshell_secrelays = 0x00;
									usbdcm_npri  = 0;
									usbdcm_nsec  = 0;
									usbdcm_fs    = 0.0;
									sprintf((const char far *)simpleshell_outstr,
										"%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
									errmsg = 1;
								}
							}
							USBDCM_longop = 0;
                        }
                        else
                        {
                            badprm = 1;                                                     	//too many parameters
                        }
                    }
                    else
                    {
                        //if no parameters, accept but do nothing
                    }
                }
                else                                                                        	//it is a GET
                {
                	sprintf((const char far *)simpleshell_outstr,
                	   "%s",cdc_out_range[usbdcm_csec]);
                	if ((tk = strtok(NULL, " ")))
                    {
                		strcpy(outst,(const char far *)simpleshell_outstr);
                		tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.OUTPUT:","STRING:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.I_REF"))
            {
                cmdok = 1;
                if (set_cmd)
                {
                    if (tk = strtok(NULL, " "))
                    {
                        errno = 0;
                        fval = strtof(tk,&p);																					//current value to set
                        tk_aux = tk;
                        if (!(tk = strtok(NULL, " ")))
                        {
                            if (usbdcm_nsec)																					//check if CDC is ON
                            {
                                ival = usbdcmReadStatus(simpleshell_strtmp);
                                if((ival == NO_VB_CLP_OFF) || (ival == NO_VB_CLP_OFF_TWARN)										//check the CDC state, to see if we are allowed to switch the relays
                                    || (ival == VB_ON_CLP_OFF) || (ival == VB_ON_CLP_OFF_TWARN))								//needs to be one of these states
                                {
                                    if (errno || (*p == *tk_aux) || (*p != NULL) || (fval > usbdcm_fs) || (fval < -usbdcm_fs))	//if syntax is wrong or requested current out of range
                                    {
                                        sprintf((const char far *)simpleshell_outstr,
                                            "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                        errmsg = 1;
                                    }
                                    else
                                    {
                                        USBDCM_longop = 1;																		//it is a long operation
                                        usbdcmSetCurrent(fval);                            										//set the requested current
                                        simpleshell_cnt = 10000;                            									//wait 1s
                                        while (simpleshell_cnt);
                                        usbdcmAdcCfg(USBDCM_DIFF, 2);															//configure the ADC multiplexer
                                        usbdcm_wcnt = usbdcm_adcdelay;
                                        while (usbdcm_wcnt);
                                        i_error = ((usbdcmAdcInV() * USBCDC_V2I) / i_meas1a) - fval;      							//read the ADC channel for the current, it should be close to the requested value
                                        if ((i_error < (-0.2 * usbdcm_fs)) || (i_error > (0.2 * usbdcm_fs)))
                                        {
                                            sprintf((const char far *)simpleshell_outstr,
                                                "%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
                                            errmsg = 1;
                                        }
                                        else
                                        {
                                            if (vb_mode)
                                            {
                                                usbdcmAdcCfg(USBDCM_DIFF, 0);													//in VB mode the null ADC channel must be zero
                                                usbdcm_wcnt = usbdcm_adcdelay;
                                                while (usbdcm_wcnt);
                                                //fval = 0;
                                                //for (i = 0; i < 20; i++)
                                                //{
                                                //	fval += usbdcmAdcInV();														//some filtering of the ADC reading is done here, noise was causing problems
                                                //}																				//I think this is aliasing as the filters on these channels are not properly set and modulation noise is high !
                                                //fval = fval/20;																	//we know 50mV is 1uA. On the 5A scale that is 0.2 ppm. The voltage for +-0.5ppm in each range is +-FS * 0.025 A/V.
                                                fval = usbdcmAdcInV(); //MIGUEL062014
                                                if ((fval < -0.025 * usbdcm_fs) || (fval > 0.025 * usbdcm_fs))                  //check the nulling meter is centered within +-0.5ppm
                                                {
                                                    sprintf((const char far *)simpleshell_outstr,
                                                        "%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
                                                    errmsg = 1;
                                                }
                                            }
                                        }
                                        USBDCM_longop = 0;
                                    }
                                }
                                else
                                {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                    errmsg = 1;
                                }
                            }else{
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                errmsg = 1;
                            }
                        }
                        else
                        {
                            badprm = 1;                                                     									//too many parameters
                        }
                    }
                    else
                    {
                        //if no parameters, accept but do nothing
                    }
                }
                else
                {
                	sprintf((const char far *)simpleshell_outstr,
                		"%.7e", usbdcm_current);
                	if ((tk = strtok(NULL, " ")))
                    {
                		strcpy(outst,(const char far *)simpleshell_outstr);
                		tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.I_REF:","FLOAT:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.DAC1TW.CAL"))										//1 turn DAC automatic calibration, assumes CDC ON, on the 5A range.
            {
                cmdok = 1;
                if (set_cmd)
                {
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (!vb_mode)														//in VB mode we cannot calibrate !!
                        {
                            if (usbdcm_fs == 5.0)											//the CDC has to be ON and on the 5A range
                            {
                                USBDCM_longop = 1;
                                usbdcm_dac_cal = usbdcmCalDac();                           	//usbdcm_dac calibration
                                USBDCM_longop = 0;
                                if (!usbdcm_dac_cal)
                                {                                                           //check if successfull (if we could null then success)
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_NULL_FAILED, fgc_errmsg[FGC_NULL_FAILED]);
                                    errmsg = 1;
                                }
                            }
                            else
                            {
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                errmsg = 1;
                            }
                        }
                        else
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_BAD_REG_MODE, fgc_errmsg[FGC_BAD_REG_MODE]);
                            errmsg = 1;
                        }
                    }
                    else
                    {
                        badprm = 1;                                                     	//too many parameters
                    }
                }
                else
                {
                	if (usbdcm_dac_cal)
					{
						sprintf((const char far *)simpleshell_outstr,
							"CALIBRATED");
					}
					else
					{
						sprintf((const char far *)simpleshell_outstr,
							"UNCALIBRATED");
					}

                	if ((tk = strtok(NULL, " ")))
                    {
                		strcpy(outst,(const char far *)simpleshell_outstr);
                		tk_aux = strtok(NULL, " ");
                		badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.DAC1TW.CAL:","STRING:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.DAC1TW.ZERO"))										//this command returns the zero cal constant for the 1t DAC
            {
                cmdok = 1;
                if (!set_cmd)
                {
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcm_dac0_0mat);
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.DAC1TW.ZERO:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.DAC1TW.FS"))										//this command returns the FS cal constant for the 1t DAC
            {
                cmdok =1;
                if (!set_cmd)
                {
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcm_dac0_10mat);
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.DAC1TW.FS:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }


            if (!strcmp(tk, "CALSYS.CDC.STATUS.RELAYS"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmReadStatus(simpleshell_strtmp);								//reads the status of the CDC relays
					for (i = 0; i < CDC_RELAY_MAP_LEN; i++)
					{
						if (cdc_relay[i])
						{
							strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " " : "" );
							strcat((const char far *)simpleshell_outstr, cdc_relay_status[i]);
						}
					}
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.STATUS.RELAYS:","STRING:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.NULL.KI"))											//expert property which allows us to change the PI controller parameters
            {
                cmdok = 1;
                if (set_cmd)
                {
                    if (tk = strtok(NULL, " "))
                    {
                        errno = 0;
                        fval = strtof(tk,&p);												//get the value to be set
                        tk_aux = tk;
                        if (!(tk = strtok(NULL, " ")))
                        {
                            if (errno || (*p == *tk_aux) || (*p != NULL) || (fval < 0) || (fval > 100))
                            {																//verify the value to be set
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                errmsg = 1;
                            }
                            else
                            {																//store the new value in flash
                                usbdcm_ki = fval;
                                usbdcmWriteEnableFlash();
                                usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_KI, usbdcm_ki);
                                usbdcmWriteDisableFlash();
                            }
                        }
                        else
                        {
                            badprm = 1;                                                     //too many parameters
                        }
                    }
                    else
                    {
                        //if no parameters, accept but do nothing
                    }
                }
                else
                {
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcm_ki);
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.NULL.KI:","FLOAT:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.NULL.KP"))											//expert property which allows us to change the PI controller parameters
            {
                cmdok = 1;
                if (set_cmd)
                {
                    if (tk = strtok(NULL, " "))
                    {
                        errno = 0;
                        fval = strtof(tk,&p);
                        tk_aux = tk;
                        if (!(tk = strtok(NULL, " ")))
                        {
                            if (errno || (p == tk_aux) || (*p != NULL) || (fval < 0) || (fval > 100))
                            {
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                errmsg = 1;
                            }
                            else
                            {
                                usbdcm_kp = fval;
                                usbdcmWriteEnableFlash();
                                usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_KP, usbdcm_kp);
                                usbdcmWriteDisableFlash();
                            }
                        }
                        else
                        {
                            badprm = 1;                                                     //too many parameters
                        }
                    }
                    else
                    {
                        //if no parameters, accpet but do nothing
                    }
                }
                else
                {
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcm_kp);
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.NULL.KP:","FLOAT:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.NULL.NR"))
            {                                                                               //expert propery that defines number of nulling attempts
                cmdok = 1;																	//in a auto null operation
                if (set_cmd)																//this is not a critical parameter so it is not stored in memory, each time it is initialised
                {
                    if (tk = strtok(NULL, " "))
                    {
                        errno = 0;
                        ival = strtol(tk,&p,10);
                        tk_aux = tk;
                        if (!(tk = strtok(NULL, " ")))
                        {
                            if (errno || (p == tk_aux) || (*p != NULL) || (ival < 0) || (ival > 100))
                            {																//verify the value to be set
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                errmsg = 1;
                            }
                            else
                            {
                                usbdcm_nautonull = ival;
                            }
                        }
                        else
                        {
                            badprm = 1;                                                     //too many parameters
                        }
                    }
                    else
                    {
                        //if no parameters, accpet but do nothing
                    }
                }
                else
                {
					sprintf((const char far *)simpleshell_outstr,
						"%u", usbdcm_nautonull);;
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.NULL.NR:","INT16U:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.NULL.AUTO"))
            {                                                                               //launches an auto null
                cmdok = 1;
                if (set_cmd)
                {
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (!vb_mode)														//cannot be used in VB mode
                        {
                            if (usbdcm_nsec)
                            {
                                USBDCM_longop = 1;
                                usbdcmAutonullPid();                                      	// autonull
                                if (!usbdcm_autonull || usbdcm_autonull_sat)
                                {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_NULL_FAILED, fgc_errmsg[FGC_NULL_FAILED]);
                                    errmsg = 1;
                                }
                            }
                            else
                            {
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                errmsg = 1;
                            }
                        }
                        else
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_BAD_REG_MODE, fgc_errmsg[FGC_BAD_REG_MODE]);
                                errmsg = 1;
                        }
                        USBDCM_longop = 0;
                    }
                    else
                    {
                        badprm = 1;                                                     	//too many parameters
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_GET_NOT_PERM, fgc_errmsg[FGC_GET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.VB_MODE"))							//this property allows us to know if the CDC is in VB mode
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmReadStatus(simpleshell_strtmp);				//check if VB is connected and update vb_mode
					sprintf((const char far *)simpleshell_outstr,
						"%u", vb_mode);
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.VB_MODE:","INT16U:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.ADC.GAIN_1A"))                          //calibration value, made at 1A
            {                                                                   //as the value is referred to 1A the gain value
                cmdok = 1;
                if (set_cmd)                                                    //coincides with the measured current
                {
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (usbdcm_current  && !vb_mode)						//don't allow calibration in VB mode as it is noisier
                        {
                            usbdcmAdcCfg(USBDCM_DIFF, 2);						//set the multiplexer
                            usbdcm_wcnt = usbdcm_adcdelay;
                            while (usbdcm_wcnt);
                            usbdcmAdcInV();										//read ADC in Volt
                            fval = usbdcmAdcInV() * USBCDC_V2I;					//calculate the current
                            if ((fval < 0.8) || (fval > 1.2))					//uncalibrated values should nevertheless be reasonable
                            {
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                errmsg = 1;
                            }
                            else
                            {
                                i_meas1a = fval;								//store the value in flash
                                usbdcmWriteEnableFlash();
                                usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_IMEAS1A, i_meas1a);
                                usbdcmWriteDisableFlash();
                            }
                        }
                        else
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                            errmsg = 1;
                        }
                    }
                    else
                    {
                        badprm = 1;                                           	//too many parameters
                    }
                }
                else
                {
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", i_meas1a);
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.ADC.GAIN_1A:","FLOAT:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.ADC.I_MEAS"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmAdcCfg(USBDCM_DIFF, 2);								//set the multiplexer
					usbdcm_wcnt = usbdcm_adcdelay;
					while (usbdcm_wcnt);
					usbdcmAdcInV();												//read ADC in Volt
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", ((usbdcmAdcInV() * USBCDC_V2I) / i_meas1a));	//return the value scaled by the cal factor
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.ADC.I_MEAS:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.ADC.V_MEAS"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmAdcCfg(USBDCM_DIFF, 1);								//set the multiplexer
					usbdcm_wcnt = usbdcm_adcdelay;
					while (usbdcm_wcnt);
					usbdcmAdcInV();												//read ADC in Volt
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcmAdcInV() * USBCDC_V2V);					//return the scaled value
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.ADC.V_MEAS:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.ADC.I_NULL"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmAdcCfg(USBDCM_DIFF, 0);								//set the multiplexer
					usbdcm_wcnt = usbdcm_adcdelay;
					while (usbdcm_wcnt);
					usbdcmAdcInV();												//read ADC in Volt
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcmAdcInV() * USBCDC_V2N);					//return the scaled value
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.ADC.I_NULL:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.CDC.ADC.PBC_TEMP"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmAdcCfg(USBDCM_DIFF, 3);								//set the multiplexer
					usbdcm_wcnt = usbdcm_adcdelay;
					while (usbdcm_wcnt);
					usbdcmAdcInV();												//read ADC in Volt
					sprintf((const char far *)simpleshell_outstr,
						"%.2e", usbdcmAdcInV() * USBCDC_V2T);					//return the scaled value
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.ADC.PBC_TEMP:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strncmp(tk, "CALSYS.CDC.DAC.RAW",18))
            {
                cmdok = 1;
                array_size = 2;
                tk_aux = tk + 18; //was 20 12-2014
                //tk = strtok(NULL, " ");        //I think this should not be here, MIGUEL 082013
                errmsg = simpleshellCheckArrayIdx(tk_aux,array_size);								//checks the requested array index is valid
                if (errmsg || (begin_array < 0) || (end_array < 0) || (end_array < begin_array))
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_BAD_ARRAY_IDX, fgc_errmsg[FGC_BAD_ARRAY_IDX]);
                    errmsg = 1;
                }
                else
                {
                    if (!set_cmd)
                    {                                                                                         		//search for the channel
						for (i = begin_array; i <= end_array; i++)
						{
							if ((i == begin_array))
							{
								sprintf((const char far *)simpleshell_outstr,					//return the raw value of DAC
									"%i", usbdcm_dac_raw[i]);
							}
							else
							{
								sprintf((const char far *)simpleshell_strtmp,					//return the raw value of DAC
									",%i", usbdcm_dac_raw[i]);
								strcat((const char far *)simpleshell_outstr,
									simpleshell_strtmp);
							}
						}
						if ((tk = strtok(NULL, " ")))
						{
							strcpy(outst,(const char far *)simpleshell_outstr);
							tk_aux = strtok(NULL, " ");
                        	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.DAC.RAW:","INT16S:",outst);
                        }
                    }
                    else
                    {
                        sprintf((const char far *)simpleshell_outstr,
                            "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                        errmsg = 1;
                    }
                }
            }

            if (!strncmp(tk, "CALSYS.CDC.DAC.VOLT",19))
            {
                cmdok = 1;
                array_size = 2;
                tk_aux = tk + 19;	//was 20 12-2014																//point to array index
                errmsg = simpleshellCheckArrayIdx(tk_aux,array_size);								//checks the requested array index is valid
                if (errmsg || (begin_array < 0) || (end_array < 0) || (end_array < begin_array))
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_BAD_ARRAY_IDX, fgc_errmsg[FGC_BAD_ARRAY_IDX]);
                    errmsg = 1;
                }
                else
                {
                    if (set_cmd)
                    {
                        if (tk = strtok(NULL, " "))													//gets the value to be set
                        {
                            tk_aux = tk;
                            if (!(tk = strtok(NULL, " ")))											//checks there are no more arguments
                            {
                                errmsg = simpleshellCheckArrayFlt(tk_aux);							//check the requested array of values are valid floats
                                if (!errmsg)
                                {
                                    for (i = begin_array; i <= end_array; i++)
                                    {
                                        if ((fval_array[i] < -10) || (fval_array[i] > 10))			//check the value is within range
                                        {
                                            sprintf((const char far *)simpleshell_outstr,
                                                "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                            errmsg = 1;
                                        }
                                        else
                                        {
                                            usbdcmDacOutV(i, fval_array[i]);						//set the dac
                                        }
                                    }
                                }
                                else
                                {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                }
                            }
                            else
                            {
                                badprm = 1;
                            }
                        }
                        else
                        {
                            //if no parameters, accpet but do nothing
                        }
                    }
                    else
                    {                                                                   		// search for the idx
						for (i = begin_array; i <= end_array; i++)
						{
							if ((i == begin_array))
							{
								sprintf((const char far *)simpleshell_outstr,
									"%.5e", usbdcm_dac[i]);
							}
							else
							{
								sprintf((const char far *)simpleshell_strtmp,
									",%.5e", usbdcm_dac[i]);
								strcat((const char far *)simpleshell_outstr,
									simpleshell_strtmp);
							}
						}
						if ((tk = strtok(NULL, " ")))
						{
							strcpy(outst,(const char far *)simpleshell_outstr);
							tk_aux = strtok(NULL, " ");
                        	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.CDC.DAC.VOLT:","FLOAT:",outst);
                        }
                    }
                }
            }

        //----------------------------end of CDC commands-----------------------------

        }
        else
        {     // verification of CDC or SM, in this case it is SM

        //----------------------------SM commands-------------------------------------


            if (!strcmp(tk, "CALSYS.SM.STATUS.RELAYS"))                                         //returns 4 nibbles with the state of the SM relays
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmSmReadStatus();													//get status of SM relays from relay driver card
					for (i = 0; i < SM_RELAY_MAP_LEN; i++)
					{
						if (sm_relay[i])
						{
							strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " " : "" );
							strcat((const char far *)simpleshell_outstr, sm_relay_status[i]);
						}
					}
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.STATUS.RELAYS:","STRING:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.SM.STATE"))                                 		//returns 4 nibbles with the state of the SM relays
			{
				cmdok = 1;
				if (set_cmd)
				{
					if (tk = strtok(NULL, " "))
					{
						tk_aux = tk;
						if (!(tk = strtok(NULL, " ")))
						{

							if (!strcmp(tk_aux, "RESET"))
							{

								usbdcmAdcCfg(USBDCM_SE, 9);										//configure the multiplexer to the current measurement channel
								usbdcm_wcnt = usbdcm_adcdelay;
								while (usbdcm_wcnt);
								if(usbdcmAdcInV() < 0.5)										//check that there is no current in the circuit
								{                                                       		//0.5V/100 (gain) = 5mV -> 5uA over 1kOhm -> 1ppm
									for (i = 0; i <= 6; i++)
									{
										if (sm_relay[sm_idx[2*i]] && sm_relay[sm_idx[2*i+1]])
										{														//relays for a specific channel are always set in pairs (bypass + select)
											errmsg = simpleshellUnsetSmChannel(i);				//unsets the relays
										}
									}
									USBDCM_longop = 1;
									usbdcm_smbits = usbdcm_smbits & 0x7FFF;                		//sets general bypass closed
									usbdcmSmWriteStatus(usbdcm_smbits);
									simpleshell_cnt = 5000;                                		//wait
									while (simpleshell_cnt);
									usbdcmSmReadStatus();										//read status of all relays
									if(usbdcm_sm_relays != usbdcm_smbits)						//if status is different from command, then error
									{
										sprintf((const char far *)simpleshell_outstr,
											"%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
										errmsg = 1;
									}
									USBDCM_longop = 0;
								}
								else
								{
									sprintf((const char far *)simpleshell_outstr,
										"%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
									errmsg = 1;
								}
							}
							else
							{
								sprintf((const char far *)simpleshell_outstr,
									"%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
								errmsg = 1;
							}
						}
						else
						{
							badprm = 1;
						}
					}
					else
					{
						 //if no parameters, accept but do nothing
					}
				}
				else
				{
					sprintf((const char far *)simpleshell_outstr,
						"%i %s", FGC_GET_NOT_PERM, fgc_errmsg[FGC_GET_NOT_PERM]);
					errmsg = 1;
				}
			}

            if (!strncmp(tk, "CALSYS.SM.DCCT",14))												//controls the state of a given DCCT channel (1 bypass + 1 select relay)
            {
                cmdok = 1;
                array_size = 7;
                tk_aux = tk + 14;   //was 15 12-2014
                errmsg = simpleshellCheckArrayIdx(tk_aux,array_size);							//check the array index is valid
                if (errmsg || (begin_array < 0) || (end_array < 0) || (end_array < begin_array))
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_BAD_ARRAY_IDX, fgc_errmsg[FGC_BAD_ARRAY_IDX]);
                    errmsg = 1;
                }
                else
                {
                    if (set_cmd)
                    {
                        if (tk = strtok(NULL, " "))												//gets the value to be set
                        {
                            tk_aux = tk;
                            if (!(tk = strtok(NULL, " ")))
                            {
                                errmsg = simpleshellCheckArrayEnb(tk_aux);						//checks that the requested array of values are either ENABLED or DISABLED
                                if (!errmsg)
                                {
                                   for (i = begin_array; i <= end_array; i++)
                                   {
                                       usbdcmAdcCfg(USBDCM_SE, 9);                            	//let us check if there is any current
                                       usbdcm_wcnt = usbdcm_adcdelay;                          	//in this case we don't want to be changing channels
                                       while (usbdcm_wcnt);                                    	//since we might need to switch the VB relay
                                       usbdcmAdcInV();											//0.5V/100 (gain) = 5mV -> 5uA over 1kOhm -> 1ppm
                                       if((usbdcmAdcInV() < 0.5) && !sm_relay[BP_ALL_OPEN])     //no current and bypass closed
                                       {
                                           if (ival_array[i] == ENABLED)
                                           {
                                               if (!sm_relay[sm_idx[2*i]] && !sm_relay[sm_idx[2*i+1]])
                                               {															//relays for a specific channel are always set in pairs (bypass + select)
                                                   if (!usbdcm_smch || ((usbdcm_smch < 2) && sm_relay[VB_DUAL]))
                                                   {                                           				//cannot have more than two channels ON and mode must be correct
														errmsg = simpleshellSetSmChannel(i);				//sets the relays
                                                   }
                                                   else
                                                   {
													   sprintf((const char far *)simpleshell_outstr,
															   "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
													   errmsg = 1;
                                                   }
                                               }
                                           }
                                           else
                                           {
                                               if (ival_array[i] == DISABLED)
                                               {
                                                   if (sm_relay[sm_idx[2*i]] && sm_relay[sm_idx[2*i+1]])
                                                   {														//relays for a specific channel are always set in pairs (bypass + select)
														errmsg = simpleshellUnsetSmChannel(i);				//unsets the relays
                                                   }
                                               }
                                               else
                                               {
                                                   sprintf((const char far *)simpleshell_outstr,
                                                       "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                                   errmsg = 1;
                                               }
                                           }
                                       }
                                       else
                                       {
                                           sprintf((const char far *)simpleshell_outstr,
                                               "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                           errmsg = 1;
                                       }
                                       if (errmsg) break;

                                   }
                                }
                                else
                                {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                }
                            }
                            else
                            {
                                badprm = 1;
                            }
                        }
                        else
                        {
                        	//if no parameters, accept but do nothing
                        }
                    }
                    else
                    {                                                                                         	//returns state of requested channel
						for (i = begin_array; i <= end_array; i++)
						{
							if ((i == begin_array))
							{
								sprintf((const char far *)simpleshell_outstr,
									"%s", ((sm_relay[sm_idx[2*i]] & sm_relay[sm_idx[2*i+1]]) ? "ENABLED" : "DISABLED"));
							}
							else
							{
								sprintf((const char far *)simpleshell_strtmp,
									",%s", ((sm_relay[sm_idx[2*i]] & sm_relay[sm_idx[2*i+1]]) ? "ENABLED" : "DISABLED"));
								strcat((const char far *)simpleshell_outstr,
									simpleshell_strtmp);
							}
						}
						if ((tk = strtok(NULL, " ")))
						{
							strcpy(outst,(const char far *)simpleshell_outstr);
							tk_aux = strtok(NULL, " ");
                        	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.DCCT:","STRING:",outst);
                        }
                    }
                }
            }


            if (!strcmp(tk, "CALSYS.SM.MODE"))													//if only 1 DCCT is being calibrated, a resistor present in the VB must be in series with the DCCT cal winding
            {																					//this command allows the user to put that resistor in series
                cmdok = 1;
                if (set_cmd)
                {
                    if (tk = strtok(NULL, " "))
                    {
                        errno = 0;
                        ival = strtol(tk,&p,10);
                        tk_aux = tk;
                        if (!(tk = strtok(NULL, " ")))
                        {
                            if (errno || (p == tk_aux) || (*p != NULL) || (ival < 0) || (ival > 100))
                            {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                    errmsg = 1;
                            }
                            else
                            {
                                if (ival == 1)
                                {
                                    if (sm_relay[VB_DUAL])													//relay in mode 2 DCCTs, i.e. no resistor in series
                                    {
                                        usbdcmAdcCfg(USBDCM_SE, 9);											//configure the multiplexer to the current measurement channel
                                        usbdcm_wcnt = usbdcm_adcdelay;
                                        while (usbdcm_wcnt);
                                        if(usbdcmAdcInV() < 0.5 && !usbdcm_smch && !sm_relay[BP_ALL_OPEN])	//check there is no current, no channels active, bypass closed
                                        {                                                       			//0.5V/100 (gain) = 5mV -> 5uA over 1kOhm -> 1ppm
                                            usbdcm_smbits = usbdcm_smbits & 0xEFFF;             			//set vb_dual relay to single
                                            usbdcmSmWriteStatus(usbdcm_smbits);
                                            simpleshell_cnt = 5000;                             			//wait
                                            while (simpleshell_cnt);
                                            usbdcmSmReadStatus();											//check status
                                            if(usbdcm_sm_relays != usbdcm_smbits)							//if status different from command, then error
                                            {
                                                sprintf((const char far *)simpleshell_outstr,
                                                    "%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
                                                errmsg = 1;
                                            }
                                        }
                                        else
                                        {
                                            sprintf((const char far *)simpleshell_outstr,
                                                "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                            errmsg = 1;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ival == 2)
                                    {
                                        if (!sm_relay[VB_DUAL])														//relay in mode 1 DCCTs, i.e. resistor in series
                                        {
                                            usbdcmAdcCfg(USBDCM_SE, 9);												//configure the multiplexer to the current measurement channel
                                            usbdcm_wcnt = usbdcm_adcdelay;
                                            while (usbdcm_wcnt);
                                            if((usbdcmAdcInV() < 0.5) && !usbdcm_smch && !sm_relay[BP_ALL_OPEN])	//check there is no current, no channels active, bypass closed
                                            {                                                           			//check that there is no current in the circuit
                                                usbdcm_smbits = usbdcm_smbits | 0x1000;                 			//0.5V/100 (gain) = 5mV -> 5uA over 1kOhm -> 1ppm
                                                usbdcmSmWriteStatus(usbdcm_smbits);             					//set vb_dual relay to double
                                                simpleshell_cnt = 5000;                                 			//wait
                                                while (simpleshell_cnt);
                                                usbdcmSmReadStatus();												//check status
                                                if(usbdcm_sm_relays != usbdcm_smbits)								//if status different from command, then error
                                                {
                                                    sprintf((const char far *)simpleshell_outstr,
                                                        "%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
                                                    errmsg = 1;
                                                }
                                            }
                                            else
                                            {
                                                sprintf((const char far *)simpleshell_outstr,
                                                    "%i %s", FGC_BAD_STATE, fgc_errmsg[FGC_BAD_STATE]);
                                                errmsg = 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        sprintf((const char far *)simpleshell_outstr,
                                            "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                        errmsg = 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            badprm = 1;
                        }
                    }
                    else
                    {
                        //if no parameters, accept but do nothing
                    }
                }
                else
                {
					sprintf((const char far *)simpleshell_outstr,
						"%u", (sm_relay[VB_DUAL] + 1));
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.MODE:","INT16U:",outst);
                    }
                }
            }

            if (!strcmp(tk, "CALSYS.SM.OUTPUT"))											//control of the bypass relays
            {
                cmdok = 1;
                if (set_cmd)
                {
                    if (tk = strtok(NULL, " "))
                    {
                        tk_aux = tk;
                        if (!(tk = strtok(NULL, " ")))
                        {
                            if (!strcmp(tk_aux, "ENABLED"))
                            {
                                usbdcm_smbits = usbdcm_smbits | 0x8000;                     //set general bypass open
                                usbdcmSmWriteStatus(usbdcm_smbits);
                                simpleshell_cnt = 5000;                                		//wait
                                while (simpleshell_cnt);

                            }
                            else
                            {
                                if (!strcmp(tk_aux, "DISABLED"))
                                {
                                    usbdcm_smbits = usbdcm_smbits & 0x7FFF;                 //sets general bypass closed
                                    usbdcmSmWriteStatus(usbdcm_smbits);
                                    simpleshell_cnt = 5000;                                	//wait
                                    while (simpleshell_cnt);
                                }
                                else
                                {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                    errmsg = 1;
                                }
                            }
                            if (!errmsg)
                            {
                                simpleshell_cnt = 5000;                                		//wait
                                while (simpleshell_cnt);
                                usbdcmSmReadStatus();										//check status
                                if(usbdcm_sm_relays == usbdcm_smbits)						//if status different from command, then error
                                {
                                    //usbdcm_smch = 0;
                                }
                                else
                                {
                                    sprintf((const char far *)simpleshell_outstr,
                                        "%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
                                    errmsg = 1;
                                }
                            }
                        }
                        else
                        {
                            badprm = 1;
                        }
                    }
                    else
                    {
                        //if no parameters, accpet but do nothing
                    }
                }
                else
                {
					if (sm_relay[BP_ALL_OPEN])
					{
						sprintf((const char far *)simpleshell_outstr,
							"ENABLED");
					}else{
						sprintf((const char far *)simpleshell_outstr,
							"DISABLED");
					}
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.OUTPUT:","STRING:",outst);
                    }
                }
            }

            if (!strncmp(tk, "CALSYS.SM.ADC.TEMP",18))										//reading of the temperature channels
            {
                cmdok = 1;
                array_size = 8;
                tk_aux = tk + 18;	//was 19 12-2014														//get array index
                errmsg = simpleshellCheckArrayIdx(tk_aux,array_size);						//check array index is valid
                if (errmsg || (begin_array < 0) || (end_array < 0) || (end_array < begin_array))
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_BAD_ARRAY_IDX, fgc_errmsg[FGC_BAD_ARRAY_IDX]);
                    errmsg = 1;
                }
                else
                {
                    if (set_cmd)
                    {
                        sprintf((const char far *)simpleshell_outstr,
                            "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                        errmsg = 1;
                    }
                    else
                    {                                                                                           //ensure there are no other parameters
						for (i = begin_array; i <= end_array; i++)
						{
							simpleshell_cnt = 1000;
							while (simpleshell_cnt);
							usbdcmAdcCfg(USBDCM_SE, i);									//configure the multiplexer for the respective channel
							usbdcm_wcnt = usbdcm_adcdelay;
							while (usbdcm_wcnt);
							usbdcmAdcInV();												//get the channel value in volt
							if ((i == begin_array))
							{
								sprintf((const char far *)simpleshell_outstr,
									"%.5e", usbdcmAdcInV() * USBSM_V2T);				//scale the value to degrees and return reading
							}
							else
							{
								sprintf((const char far *)simpleshell_strtmp,
									",%.5e", usbdcmAdcInV() * USBSM_V2T);				//scale the value to degrees and return reading
								strcat((const char far *)simpleshell_outstr,
									simpleshell_strtmp);
							}
						}
						if ((tk = strtok(NULL, " ")))
						{
							strcpy(outst,(const char far *)simpleshell_outstr);
							tk_aux = strtok(NULL, " ");
                        	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.ADC.TEMP:","FLOAT:",outst);
                        }
                    }
                }
            }


            if (!strcmp(tk, "CALSYS.SM.ADC.V_MEAS"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmAdcCfg(USBDCM_SE, 8);											//configure the multiplexer
					usbdcm_wcnt = usbdcm_adcdelay;
					while (usbdcm_wcnt);
					usbdcmAdcInV();														//get the value in volt
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcmAdcInV() * USBSM_V2V);							//scale the value and return reading
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.ADC.V_MEAS:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

            if (!strcmp(tk, "CALSYS.SM.ADC.I_NULL"))
            {
                cmdok = 1;
                if (!set_cmd)
                {
					usbdcmAdcCfg(USBDCM_SE, 9);											//configure the multiplexer
					usbdcm_wcnt = usbdcm_adcdelay;
					while (usbdcm_wcnt);
					usbdcmAdcInV();														//get the value in volt
					sprintf((const char far *)simpleshell_outstr,
						"%.5e", usbdcmAdcInV() * USBSM_V2N);							//scale the value and return reading
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.SM.ADC.I_NULL:","FLOAT:",outst);
                    }
                }
                else
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
            }

    //----------------------------end of SM commands------------------------------

        }        //end of CDC SM verification

    //----------------------------generic commands--------------------------------

        if (!strcmp(tk, "DEVICE.VERSION.MAIN_PROG"))
        {                                                                                   //returns software version
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%u", mp_version);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"DEVICE.VERSION.MAIN_PROG:","INT16U:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "STATUS.FAULTS"))
        {                                                                                   //returns list of faults
            cmdok = 1;
            if (!set_cmd)
            {
				if (!usbdcm_sm)
				{
					usbdcmReadStatus(simpleshell_strtmp);
					simpleshellTermCheckCdcStatus();
				}
				else
				{
					usbdcmSmReadStatus();
					simpleshellTermCheckSmStatus();
				}
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"STATUS.FAULTS:","STRING:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "STATUS.WARNINGS"))
        {                                           										//returns list of warnings
            cmdok = 1;
            if (!set_cmd)
            {
				if (!usbdcm_sm)
				{
					usbdcmReadStatus(simpleshell_strtmp);
					simpleshellTermCheckCdcWarn();
				}
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"STATUS.WARNINGS:","STRING:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.SELECT"))												//device selection, CDC or SM
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (tk = strtok(NULL, " "))
                {
                    tk_aux = tk;
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (!strcmp(tk_aux, "CDC"))
                        {
                            usbdcm_sm = 0;
                        }
                        else
                        {
                            if (!strcmp(tk_aux, "SM"))
                            {
                                usbdcm_sm = 1;
                            }
                            else
                            {
                                sprintf((const char far *)simpleshell_outstr,
                                    "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                                errmsg = 1;
                            }
                        }
                        if (!errmsg)
                        {
                            usbdcmWriteEnableFlash();										//the type of device is stored in flash
                            usbdcmWriteFlash8(USBDCM_ADDR_SM, usbdcm_sm);
                            usbdcmWriteDisableFlash();
                        }
                    }
                    else
                    {
                        badprm = 1;
                    }
                }
                else
                {
                    //if no parameters, accept but do nothing
                }
            }
            else
            {
				if (usbdcm_sm)
				{
					sprintf((const char far *)simpleshell_outstr,
						"SM");
				}
				else
				{
					sprintf((const char far *)simpleshell_outstr,
						"CDC");
				}
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.SELECT:","STRING:",outst);
                }
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.INIT"))													//expert property that initialises all cal constants and writes them in the flash
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (!(tk = strtok(NULL, " ")))
                {
                    usbdcmInitFlashStorage();
                    usbdcmGetFlash();
                }
                else
                {
                    badprm = 1;
                }
            }
            else
            {
				sprintf((const char far *)simpleshell_outstr,
					"%i %s", FGC_GET_NOT_PERM, fgc_errmsg[FGC_GET_NOT_PERM]);
            }
        }


        if (!strncmp(tk, "CALSYS.DCM.ADC.RAW",18))											//reading of raw values from the ADCs, mostly for troubleshooting
        {
            cmdok = 1;
            array_size = usbdcm_sm ? 16 : 8;
            tk_aux = tk + 18; //was 19 12-2014
            errmsg = simpleshellCheckArrayIdx(tk_aux,array_size);							//check the array index is valid
            if (errmsg || (begin_array < 0) || (end_array < 0) || (end_array < begin_array))
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_BAD_ARRAY_IDX, fgc_errmsg[FGC_BAD_ARRAY_IDX]);
                errmsg = 1;
            }
            else
            {
                if (set_cmd)
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
                else
                {                                                                                        		//ensure there are no more parameters
					for (i = begin_array; i <= end_array; i++)
					{
						simpleshell_cnt = 1000;                                         		//wait
						while (simpleshell_cnt);
						usbdcm_sm ? usbdcmAdcCfg(USBDCM_SE, i): usbdcmAdcCfg(USBDCM_DIFF, i);	//set the multiplexer to the desired channel
						usbdcm_wcnt = usbdcm_adcdelay;
						while (usbdcm_wcnt);
						if ((i == begin_array))
						{
							sprintf((const char far *)simpleshell_outstr,
								"%i", usbdcmAdcIn());											//read the raw value from the ADC
						}
						else
						{
							sprintf((const char far *)simpleshell_strtmp,
								",%i", usbdcmAdcIn());											//read the raw value from the ADC
							strcat((const char far *)simpleshell_outstr,
								simpleshell_strtmp);
						}
					}
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.ADC.RAW:","INT16S:",outst);
                    }
                }
            }
        }

        if (!strncmp(tk, "CALSYS.DCM.ADC.VOLT",19))											//reading of the ADCs in volt
        {
            cmdok = 1;
            array_size = usbdcm_sm ? 16 : 8;
            tk_aux = tk + 19;  //was 20 12-2014
            errmsg = simpleshellCheckArrayIdx(tk_aux,array_size);									//check the array index is valid
            if (errmsg || (begin_array < 0) || (end_array < 0) || (end_array < begin_array))
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_BAD_ARRAY_IDX, fgc_errmsg[FGC_BAD_ARRAY_IDX]);
                errmsg = 1;
            }
            else
            {
                if (set_cmd)
                {
                    sprintf((const char far *)simpleshell_outstr,
                        "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                    errmsg = 1;
                }
                else
                {                                                                                           		//ensure there are no more parametersel
					for (i = begin_array; i <= end_array; i++)
					{
						simpleshell_cnt = 1000;                                         		//wait
						while (simpleshell_cnt);
						usbdcm_sm ? usbdcmAdcCfg(USBDCM_SE, i): usbdcmAdcCfg(USBDCM_DIFF, i);	//set the multiplexer to the desired channel
						usbdcm_wcnt = usbdcm_adcdelay;
						while (usbdcm_wcnt);
						usbdcmAdcInV();
						if ((i == begin_array))
						{
							sprintf((const char far *)simpleshell_outstr,
								"%.5e", usbdcmAdcInV());										//read the value from the ADC in volt
						}
						else
						{
							sprintf((const char far *)simpleshell_strtmp,
								",%.5e", usbdcmAdcInV());										//read the value from the ADC in volt
							strcat((const char far *)simpleshell_outstr,
								simpleshell_strtmp);
						}
					}
					if ((tk = strtok(NULL, " ")))
					{
						strcpy(outst,(const char far *)simpleshell_outstr);
						tk_aux = strtok(NULL, " ");
                    	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.ADC.VOLT:","INT16S:",outst);
                    }
                }
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.ADC.CAL"))														//calibrates the ADC system using the internal VREF
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (!(tk = strtok(NULL, " ")))
                {
                    usbdcm_adc_cal = usbdcmAdcCal();                                           		//launch ADC calibration - returns 1 if ok
                    if (!usbdcm_adc_cal)															//inside usbdcmAdcCal(), a flag is written in the flash to signal
                    {
                        sprintf((const char far *)simpleshell_outstr,
                            "%i %s", FGC_NULL_FAILED, fgc_errmsg[FGC_NULL_FAILED]);
                        errmsg = 1;
                    }
                }
                else
                {
                    badprm = 1;
                }
            }
            else
            {
				if (usbdcm_adc_cal)
				{
					sprintf((const char far *)simpleshell_outstr,
						"CALIBRATED");
				}
				else
				{
					sprintf((const char far *)simpleshell_outstr,
						"UNCALIBRATED");
				}
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.ADC.CAL:","STRING:",outst);
                }
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.ADC.V_REF"))													//expert property that sets the value voltage reference used for calibration
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (tk = strtok(NULL, " "))
                {
                    errno = 0;
                    fval = strtof(tk,&p);															//gets the value of the vref which was measured with a DVM by the user
                    tk_aux = tk;
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (errno || (*p == *tk_aux) || (*p != NULL) || (fval < 9) || (fval > 11))
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                            errmsg = 1;
                        }
                        else
                        {
                            usbdcm_adc_vref = fval;													//stores the value of the vref in the flash
                            usbdcmWriteEnableFlash();
                            usbdcmWriteFlashFloat(USBDCM_ADDR_ADC_VREF, usbdcm_adc_vref);
                            usbdcmWriteDisableFlash();
                        }
                    }
                    else
                    {
                        badprm = 1;
                    }
                }
                else
                {
                    //if no parameters, accept but do nothing
                }
            }
            else
            {

				sprintf((const char far *)simpleshell_outstr,
					"%.6e", usbdcm_adc_vref);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.ADC.V_REF:","FLOAT:",outst);
                }
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.ADC.RAW_ZERO"))
        {                                                                                       //ADC offset, used to calculate the ADC calibrated reading
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%i", (int)usbdcm_adc_d0);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.RAW_ZERO:","INT16S:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.ADC.RAW_REF"))                                              //raw value of VREF read by ADC
        {                                                                                       //used to calculate the ADC calibration gin constant
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%i", (int)usbdcm_adc_dref);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.RAW_REF:","INT16S:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS05.CAL"))													//calibration of power supply 5V reading
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (tk = strtok(NULL, " "))
                {
                    errno = 0;
                    fval = strtof(tk,&p);														//get the 5V value introduced by the user
                    tk_aux = tk;
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (errno || (*p == *tk_aux) || (*p != NULL) || (fval < 4) || (fval > 6))
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                            errmsg = 1;
                        }
                        else
                        {
                            calp05 = fval/usbdcm_volts_p05;										//save the cal value in flash
                            usbdcmWriteEnableFlash();
                            usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_P05, calp05);
                            usbdcmWriteDisableFlash();
                        }
                    }
                    else
                    {
                        badprm = 1;
                    }
                }
                else
                {
                    //if no parameters, accept but do nothing
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_GET_NOT_PERM, fgc_errmsg[FGC_GET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS15.CALPOS"))												//calibration of power supply +15V reading
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (tk = strtok(NULL, " "))
                {
                    errno = 0;
                    fval = strtof(tk,&p);														//get the 15V value introduced by the user
                    tk_aux = tk;
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (errno || (*p == *tk_aux) || (*p != NULL) || (fval < 14) || (fval > 16))
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                            errmsg = 1;
                        }
                        else
                        {
                            calp15 = fval/usbdcm_volts_p15;										//save the cal value in flash
                            usbdcmWriteEnableFlash();
                            usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_P15, calp15);
                            usbdcmWriteDisableFlash();
                        }
                    }
                    else
                    {
                        badprm = 1;
                    }
                }
                else
                {
                    //if no parameters, accept but do nothing
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_GET_NOT_PERM, fgc_errmsg[FGC_GET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS15.CALNEG"))												//calibration of power supply -15V reading
        {
            cmdok = 1;
            if (set_cmd)
            {
                if (tk = strtok(NULL, " "))
                {
                    errno = 0;
                    fval = strtof(tk,&p);														//get the -15V value introduced by the user
                    tk_aux = tk;
                    if (!(tk = strtok(NULL, " ")))
                    {
                        if (errno || (*p == *tk_aux) || (*p != NULL) || (fval < -16) || (fval > -14))
                        {
                            sprintf((const char far *)simpleshell_outstr,
                                "%i %s", FGC_OUT_OF_LIMITS, fgc_errmsg[FGC_OUT_OF_LIMITS]);
                            errmsg = 1;
                        }
                        else
                        {
                            caln15 = fval/usbdcm_volts_m15;										//save the cal value in flash
                            usbdcmWriteEnableFlash();
                            usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_N15, caln15);
                            usbdcmWriteDisableFlash();
                        }
                    }
                    else
                    {
                        badprm = 1;
                    }
                }
                else
                {
                    //if no parameters, accept but do nothing
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_GET_NOT_PERM, fgc_errmsg[FGC_GET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS05.GAIN"))												//returns the cal constant for the ps voltage reading
        {
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%.5e", calp05);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.PS05.GAIN:","FLOAT:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS15.GAINPOS"))												//returns the cal constant for the ps voltage reading
        {
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%.5e", calp15);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.PS15.GAINPOS:","FLOAT:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS15.GAINNEG"))												//returns the cal constant for the ps voltage reading
        {
            cmdok =1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%.5e", caln15);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.PS15.GAINNEG:","FLOAT:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS05.MEAS"))												//returns the value of the 5V ps, which is periodically measured in the mains
        {
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%.5e", usbdcm_volts_p05 * calp05);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.PS05.MEAS:","FLOAT:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS15.MEASPOS"))												//returns the value of the +15V ps, which is periodically measured in the mains
        {
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%.5e", usbdcm_volts_p15 * calp15);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.PS15.MEASPOS:","FLOAT:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!strcmp(tk, "CALSYS.DCM.PS15.MEASNEG"))												//returns the value of the -15V ps, which is periodically measured in the mains
        {
            cmdok = 1;
            if (!set_cmd)
            {
				sprintf((const char far *)simpleshell_outstr,
					"%.5e", usbdcm_volts_m15 * caln15);
				if ((tk = strtok(NULL, " ")))
				{
					strcpy(outst,(const char far *)simpleshell_outstr);
					tk_aux = strtok(NULL, " ");
                	badgetopt = simpleshellCheckOptions(tk,tk_aux,"CALSYS.DCM.PS15.MEASNEG:","FLOAT:",outst);
                }
            }
            else
            {
                sprintf((const char far *)simpleshell_outstr,
                    "%i %s", FGC_SET_NOT_PERM, fgc_errmsg[FGC_SET_NOT_PERM]);
                errmsg = 1;
            }
        }

        if (!cmdok)
        {
          sprintf((const char far *)simpleshell_outstr,
            "%i %s", FGC_UNKNOWN_SYM, fgc_errmsg[FGC_UNKNOWN_SYM]);
        }

        if (badgetopt)
        {
            sprintf((const char far *)simpleshell_outstr,
                "%i %s", FGC_BAD_GET_OPT, fgc_errmsg[FGC_BAD_GET_OPT]);
        }

        if (badprm)
        {
            sprintf((const char far *)simpleshell_outstr,
                "%i %s", FGC_BAD_PARAMETER, fgc_errmsg[FGC_BAD_PARAMETER]);
        }

	}
	else
	{
	   sprintf((const char far *)simpleshell_outstr,
				   "%i %s", FGC_UNKNOWN_CMD, fgc_errmsg[FGC_UNKNOWN_CMD]);
	}

    p11_0 = 0;
    return (cmdok && !errmsg && !badgetopt && !badprm);
}

void simpleshellTimer(void)
{
    if (simpleshell_cnt > 0)
    {
        simpleshell_cnt--;
    }
}

void simpleshellCleanLine(void)
{
    if (simpleshell_echo)
    {
    																							// clear line
        simpleshell_str[0] = SIMPLESHELL_ESC;
        simpleshell_str[1] = '[';
        simpleshell_str[2] = '2';
        simpleshell_str[3] = 'K';
        serialTx0((const char far *)simpleshell_str, 4);
        																						// up one line
        simpleshell_str[0] = SIMPLESHELL_ESC;
        simpleshell_str[1] = '[';
        simpleshell_str[2] = 'A';
        serialTx0((const char far *)simpleshell_str, 3);
        if (simpleshell_echo)
        {
            serialTx0((const char far *)SIMPLESHELL_PROMPT, -1);
        }
    }
}

void simpleshellCursorLeft(void)
{
    if (simpleshell_echo)
    {
        simpleshell_str[0] = SIMPLESHELL_ESC;
        simpleshell_str[1] = '[';
        simpleshell_str[2] = 'D';
        serialTx0((const char far *)simpleshell_str, 3);
    }
}

void simpleshellCursorRight(void)
{
    if (simpleshell_echo)
    {
        simpleshell_str[0] = SIMPLESHELL_ESC;
        simpleshell_str[1] = '[';
        simpleshell_str[2] = 'C';
        serialTx0((const char far *)simpleshell_str, 3);
    }
}

void simpleshellCharIns(char c)
{
    static int i;

    for (i=simpleshell_lCmd; i>simpleshell_cursor; i--)
    {
        simpleshell_strCmd[i] = simpleshell_strCmd[i - 1];
    }
    simpleshell_strCmd[simpleshell_lCmd + 1] = 0;
    simpleshell_strCmd[simpleshell_cursor] = c;
    simpleshell_lCmd++;
    simpleshell_cursor++;
}

void simpleshellCharDel(void)
{
    static int i;

    for (i=simpleshell_cursor; i<simpleshell_lCmd; i++)
    {
        simpleshell_strCmd[i] = simpleshell_strCmd[i + 1];
    }
    simpleshell_lCmd--;
}

void simpleshellDirectCharIns(char c)
{
    static int i;

    for (i=direct_lCmd; i>direct_cursor; i--)
    {
        direct_strCmd[i] = direct_strCmd[i - 1];
    }
    direct_strCmd[direct_lCmd + 1] = 0;
    direct_strCmd[direct_cursor] = c;
    direct_lCmd++;
    direct_cursor++;
}

int simpleshellCheckOptions(char far *tk1, char far *tk2, char far *rsp1, char far *rsp2, char far *outstr)
{
	static int opt, badget;
	//char *outstr;
	badget = 0;
   	opt = 3;

   	if (!strcmp(tk1, "LBL"))
		if(!tk2)	opt = 0;
		else	if (!strcmp(tk2, "TYPE"))	opt = 2;

	if (!strcmp(tk1, "TYPE"))
		if(!tk2)	opt = 1;
		else	if (!strcmp(tk2, "LBL"))	opt = 2;

	switch (opt)
	{
		case 0:
			sprintf((const char far *)simpleshell_outstr,"%s%s",rsp1,outstr);
			break;
		case 1:
			sprintf((const char far *)simpleshell_outstr,"%s%s",rsp2,outstr);
			break;
		case 2:
			sprintf((const char far *)simpleshell_outstr,"%s%s%s",rsp1,rsp2,outstr);
			break;
		default:
			badget = 1;
			break;
	}
	return badget;
}

void simpleshellTermCheckCdcStatus(void)														//prepare the cdc faults return array based on the alarms
{																								//the alarms are set in: usbdcmReadStatus
    if (!cdc_pwr_ok)
    {
        strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " CDC_PWR" : "CDC_PWR" );
    }
    else
    {
        if (!pbc_temp_warn && pbc_temp_fault)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " PBC_TMP": "PBC_TMP");
        }
        if (!pbc_cal_ok)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " PBC_CAL" : "PBC_CAL");
        }
        if (!pbc_cmpl_ok)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " PBC_CMPL" : "PBC_CMPL");
        }
        if (!pbc_chr_ok)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " PBC_CHR" : "PBC_CHR");
        }
        if (!vb_pwr_ok)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " VB_PWR" : "VB_PWR");
        }
        if (!vb_temp_ok)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " VB_TMP" : "VB_TMP");
        }
        if (!vb_con_ok)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " VB_CN" : "VB_CN");
        }
    }
}

void simpleshellTermCheckSmStatus(void)															//prepare the cdc faults return array based on the alarms
{																								//the alarms are set in: usbdcmReadStatus
    if (!usbdcm_sm_5v_present)
    {
        strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " SM_PWR": "SM_PWR");
    }
}


void simpleshellTermCheckCdcWarn(void)															//prepare the cdc warnings return array based on the alarms
{																								//the alarms are set in: usbdcmReadStatus
    if (cdc_pwr_ok)
    {
        if (pbc_temp_warn && !pbc_temp_fault)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " PBC_TMP" : "PBC_TMP");
        }
        if (usbdcm_nsec && clamp_on)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " CLP_ON" : "CLP_ON");
        }
        if (!usbdcm_nsec && !clamp_on)
        {
            strcat((const char far *)simpleshell_outstr, *simpleshell_outstr ? " CLP_OFF" : "CLP_OFF");
        }
    }
}

void simpleshellDiagCheckCdcStatus(void)														//this is used in DIAG mode only for the preparation of the published data
{
    if (!cdc_pwr_ok)
    {
        cdc_faults_msk = cdc_faults_msk | 0x0001;
        cdc_faults_msk = cdc_faults_msk & 0x0001;                                           	//all other faults should be zero as they cannot be read because the CDC is unpowered
    }
    else
    {
        cdc_faults_msk = cdc_faults_msk & 0xFFFE;
        cdc_faults_msk = (!pbc_temp_warn && pbc_temp_fault) ? (cdc_faults_msk | 0x0002) : (cdc_faults_msk & 0xFFFD);
        cdc_faults_msk = (!pbc_cal_ok) ? (cdc_faults_msk | 0x0004) : (cdc_faults_msk & 0xFFFB);
        cdc_faults_msk = (!pbc_cmpl_ok) ? (cdc_faults_msk | 0x0008) : (cdc_faults_msk & 0xFFF7);
        cdc_faults_msk = (!pbc_chr_ok) ? (cdc_faults_msk | 0x0010) : (cdc_faults_msk & 0xFFEF);
        cdc_faults_msk = (!vb_pwr_ok) ? (cdc_faults_msk | 0x0020) : (cdc_faults_msk & 0xFFDF);
        cdc_faults_msk = (!vb_temp_ok) ? (cdc_faults_msk | 0x0040) : (cdc_faults_msk & 0xFFBF);
        cdc_faults_msk = (!vb_con_ok) ? (cdc_faults_msk | 0x0080) : (cdc_faults_msk & 0xFF7F);
    }
}

void simpleshellDiagCheckSmStatus(void)															//this is used in DIAG mode only for the preparation of the published data
{
    sm_faults_msk = (!usbdcm_sm_5v_present) ? (sm_faults_msk | 0x0001) : (sm_faults_msk & 0xFFFE);
}


void simpleshellDiagCheckCdcWarn(void)															//this is used in DIAG mode only for the preparation of the published data
{
    if (cdc_pwr_ok)
    {
        cdc_warns_msk = (pbc_temp_warn && !pbc_temp_fault) ? (cdc_warns_msk | 0x0001) : (cdc_warns_msk & 0xFFFE);
        cdc_warns_msk = (usbdcm_nsec && clamp_on) ? (cdc_warns_msk | 0x0002) : (cdc_warns_msk & 0xFFFD);
        cdc_warns_msk = (!usbdcm_nsec && !clamp_on) ? (cdc_warns_msk | 0x0004) : (cdc_warns_msk & 0xFFFB);
    }
    else
    {
        cdc_warns_msk = cdc_warns_msk & 0x0000;
    }
}

void simpleshellReadAllStatus(void)
{
    if (usbdcm_sm)
    {
        usbdcmSmReadStatus();
        simpleshellDiagCheckSmStatus();
    }
    else
    {
        usbdcmReadStatus(simpleshell_strtmp);
        simpleshellDiagCheckCdcStatus();
        simpleshellDiagCheckCdcWarn();
    }
}

void simpleshellReadAllAdcs(void)
{
    static int i;

    if (usbdcm_sm)
    {
        usbdcmAdcCfg(USBDCM_SE, 8);
        usbdcm_wcnt = usbdcm_adcdelay;
        while (usbdcm_wcnt);
        sm_vout = usbdcmAdcInV() * USBSM_V2V;

        usbdcmAdcCfg(USBDCM_SE, 9);
        usbdcm_wcnt = usbdcm_adcdelay;
        while (usbdcm_wcnt);
        sm_inull = usbdcmAdcInV() * USBSM_V2N;

        for (i=0; i>7; i++)
        {
            usbdcmAdcCfg(USBDCM_SE, i);
            usbdcm_wcnt = usbdcm_adcdelay;
            while (usbdcm_wcnt);
            sm_temp[i] = usbdcmAdcInV() * USBSM_V2T;
        }
    }
    else
    {

        usbdcmAdcCfg(USBDCM_DIFF, 1);
        usbdcm_wcnt = usbdcm_adcdelay;
        while (usbdcm_wcnt);
        cdc_vout = usbdcmAdcInV() * USBCDC_V2V;

        usbdcmAdcCfg(USBDCM_DIFF, 3);
        usbdcm_wcnt = usbdcm_adcdelay;
        while (usbdcm_wcnt);
        pbc_temp = usbdcmAdcInV() * USBCDC_V2T;

        usbdcmAdcCfg(USBDCM_DIFF, 2);
        usbdcm_wcnt = usbdcm_adcdelay;
        while (usbdcm_wcnt);
        cdc_iout = ((usbdcmAdcInV() * USBCDC_V2I) / i_meas1a);
    }
}


/* this function takes a pointer to a string corresponding to the indexes
of an array property and it takes an integer corresponding to the expected
size of the array
The indexes may be in the following formats
   a]                      //single element a
   a,b]                    //all elements from a to b
   a,]                     //all elements from a to the end
   ]                       //all elements
The function will verify the index syntax and it will set the variables:
   begin_array
   end_array
The return value indicates if there was an error
*/

char simpleshellCheckArrayIdx(char far *array_idx, int array_sz)
{
    long int  i_val;
    char _far  *p, *array_p;
    char err;

    begin_array = -1;
    end_array = -1;
    array_p = array_idx;
    array_idx++; //miguel 12-2014
    do
    {
        err = 1;
        errno = 0;
        if(array_p[0] == '[')
        //if (!strncmp(array_idx,"[",1)) //miguel 12-2014
        {
        	i_val = strtol(array_idx,&p,10);
        	if ((errno || (p == array_idx)) && !i_val)                                          //found no integer
			{
				if (*p == ']')
				{                                                               //but found end of array index symbol
					if (begin_array == -1)                                                      //as we had not found a begin index yet
					{
						begin_array = 0;                                                        //assign begin index
						err = 0;
					}
					if (end_array == -1)                                                        //no end index assigned yet
					{
						end_array = array_sz -1;                                                //assign end index
						err = 0;
					}
				}
			}
			else
			{
				if ((i_val >= 0) && i_val < (array_sz))                                         //an integer was found
				{
					switch(*p)       //was only *p, changed to *(p+1) in 12-2014                                                           //what is the character after that integer?
					{
						case ',':                                                               //it is the begin index
							if ((begin_array == -1))                                            //check there wasn't one already, if there was, brak
							{
								begin_array = i_val;
								err = 0;
							}
							break;

						case ']':                                                               //end of array list symbol
							if (begin_array == -1 && end_array == -1)
							{
								begin_array = i_val;
								err = 0;
							}
							if (begin_array != -1 && end_array == -1)
							{
								end_array = i_val;
								err = 0;
							}
							break;
						default:
							break;
					}
				}
			}
        }
        array_idx = p + 1;
        	//miguel 12-2014
		if (array_p[0] == '\0') //added //miguel 12-2014
		{
			array_idx = array_p;
			if (begin_array == -1)                                                      //as we had not found a begin index yet
			{
				begin_array = 0;                                                        //assign begin index
				err = 0;
			}
			if (end_array == -1)                                                        //no end index assigned yet
			{
				end_array = array_sz -1;                                                //assign end index
				err = 0;
			}
		}

    }
    while(*array_idx && !err);
    return err;
}

/* this function takes a string corresponding to the values of an array property
according to the present begin_array and end_array variables
The values may be in the following formats
   x                       //setting single element
   x,y,z                   //setting multiple elements

The function will verify the syntax and it will set the variable:
   aux_array
The return value indicates if there was an error
*/

char simpleshellCheckArrayEnb(char far *array_val)		//checks the values in the array are either ENABLED or DISABLED
{
    int  i_val,len,k;
    char state[70];
    char *s,*t,err;
    err = 0;
    k = 0;
    len = 0;

    for(k = begin_array; k <= end_array; k++)
    {
        err = 1;
        strcpy(state,array_val);                        //save the original string as we are going to work on the copy
        s = strchr(array_val,',');                      //find the first comma and save the string from there on
        t = array_val;                                  //the three next lines of code pick up the part of the string
        len = s - t;                                    //before the comma to form a new string
        state[len] = NULL;

        if (!strcmp(state, "ENABLED"))					//check if array eleemnt is "enabled" or "disabled"
        {
            i_val = ENABLED;
        }
        else
        {
            if (!strncmp(state, "DISABLED", 8))
            {
               i_val = DISABLED;
            }
            else
            {
               i_val = INVALID_STATE;
            }
        }

        if (i_val == INVALID_STATE)
        {
            k = end_array;
        }
        else
        {
            switch(*s)
            {
                case ',':
                    if (k != end_array)
                    {
                        ival_array[k] = i_val;
                        err = 0;
                    }
                    break;
                case NULL:
                    if (k == end_array)
                    {
                        ival_array[k] = i_val;
                        err = 0;
                    }
                    break;
                default:
                    break;
            }
        }
        array_val = s + 1;
    }
    return err;
}

char simpleshellCheckArrayFlt(char far *array_val)
{
    float  f_val;
    char _far  *p;
    char err;
    int k;
    err = 0;
    k = 0;
    f_val = 0;

    for(k = begin_array; k <= end_array; k++)
    {
        err = 1;
        errno = 0;
        f_val = strtof(array_val,&p);
        if ((errno || (p == array_val)) && !f_val)
        {
            k = end_array;                                     //leave loop
        }
        else
        {
            switch(*p)
            {
                case ',':
                    if (k != end_array)
                    {
                        fval_array[k] = f_val;
                        err = 0;
                    }
                    break;
                case NULL:
                    if (k == end_array)
                    {
                        fval_array[k] = f_val;
                        err = 0;
                    }
                    break;
                default:
                    break;
            }
        }
        array_val = p + 1;
    }
    return err;
}


char simpleshellSetCdc(int cdc_on)
{
    int k;

    usbdcmSetCurrent(0);                                                                //go to 0A
    simpleshell_cnt = 5000;                                                             //wait
    while (simpleshell_cnt);
    simpleshell_matrixstring[8] = 0x00;                                                 //switch the clamp on (0)
    usbdcmWriteStatus(simpleshell_matrixstring);
    simpleshell_cnt = 5000;                                                             //wait
    while (simpleshell_cnt);
    simpleshell_matrixstring[7] = cdc_on ? cdc_on : 0x00;                               //switch secondary relays
    usbdcmWriteStatus(simpleshell_matrixstring);
    simpleshell_cnt = 5000;                                                             //wait
    while (simpleshell_cnt);
    if (cdc_on)
    {
        simpleshell_matrixstring[8] = 0x01;
        usbdcmWriteStatus(simpleshell_matrixstring);
        simpleshell_cnt = 5000;                                                         //wait
        while (simpleshell_cnt);
    }
    usbdcmReadStatus(simpleshell_strtmp);
    for(k = 0; k < USBDCM_READRELAYS; k++)                                              //take the first 8 characters of the string sent
    {                                                                                   //and compare with the relay status
        if (simpleshell_strtmp[k] != simpleshell_matrixstring[k])
        {
            sprintf((const char far *)simpleshell_outstr,
                "%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
            return 1;
        }
    }
    return 0;
}

char simpleshellSetSmChannel(int k)
{
    USBDCM_longop = 1;
    simpleshell_cnt = 5000;
    while (simpleshell_cnt);                                                                //wait
    usbdcm_smbits = usbdcm_smbits | (0x0001 << sm_idx[2*k]);                                //set the dcct select relay on
    usbdcmSmWriteStatus(usbdcm_smbits);                                               		//change the status of the relay
    simpleshell_cnt = 5000;                                                            		//wait
    while (simpleshell_cnt);
    usbdcm_smbits = usbdcm_smbits | (0x0001 << sm_idx[2*k+1]);                              //open the bypass relay
    usbdcmSmWriteStatus(usbdcm_smbits);
    simpleshell_cnt = 5000;                                                                	//wait
    while (simpleshell_cnt);
    usbdcmSmReadStatus();                                                                 	//check if status corresponds to requested
	if(usbdcm_sm_relays == usbdcm_smbits)
	{
		usbdcm_smch += 1;																	//update number of active channels
	}
	else
	{
		sprintf((const char far *)simpleshell_outstr,
			"%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
		USBDCM_longop = 0;
		return 1;
	}
    USBDCM_longop = 0;
    return 0;
}

char simpleshellUnsetSmChannel(int k)
{
    USBDCM_longop = 1;
    simpleshell_cnt = 5000;                                                            	//wait
    while (simpleshell_cnt);
    usbdcm_smbits = usbdcm_smbits & ~(0x0001 << sm_idx[2*k+1]);                         //set the dcct bypass on
    usbdcmSmWriteStatus(usbdcm_smbits);
    simpleshell_cnt = 5000;                                                        		//wait
    while (simpleshell_cnt);
    if (usbdcm_smch == 1)
    {
    	usbdcm_smbits = usbdcm_smbits & (0x7FFF); 										//close the general bypass relay if it is the last DCCT

    }
    usbdcm_smbits = usbdcm_smbits & ~(0x0001 << sm_idx[2*k]);                           //open the dcct select relay
    usbdcmSmWriteStatus(usbdcm_smbits);
    simpleshell_cnt = 5000;                                                            	//wait
    while (simpleshell_cnt);
	usbdcmSmReadStatus();																//check that status corresponds to what was requested
	if(usbdcm_sm_relays == usbdcm_smbits)
	{
		usbdcm_smch -= 1;																//updates number of active channels
	}
	else
	{
		sprintf((const char far *)simpleshell_outstr,
			"%i %s", FGC_IO_ERROR, fgc_errmsg[FGC_IO_ERROR]);
		USBDCM_longop = 0;
		return 1;
	}
    USBDCM_longop = 0;
    return 0;
}
