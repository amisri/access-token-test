#define __USBDCM_c__

#include <main.h>

void usbdcmInit(void)
{
    float t, nullmeter;
    int i, j, delay = 100;

    mp_version = VERSION;
    usbdcm_cfgp = 0xFF;
    USBDCM_longop = 0;																		//for long operations - it rejects other commands if this flag is on
    usbdcm_npri  = 0;
    usbdcm_csec = 0;
    usbdcm_nsec  = 0;
    usbdcm_fs    = 0.0;
    usbdcm_wcnt  = 0;
    usbdcm_limStepSize         = USBDCM_DEFAULT_LIMSTEPSIZE;
    usbdcm_relaydelay    	   = USBDCM_DEFAULT_RELAYDELAY;
    usbdcm_autonulldelay       = USBDCM_DEFAULT_AUTONULLDELAY;
    usbdcm_adcdelay            = USBDCM_DEFAULT_ADCDELAY;
    usbdcm_nautonull = 15;																	//nr of autonull attempts
    usbdcm_adc_v0 = 0;
    vb_mode = 0;                                                                            //initialise the value for the ADC zero calibration

    usbdcm_readyness = usbdcmReadFlash8(USBDCM_ADDR_READYNESS);                            	//check the flash to see if it has been used before

    switch (usbdcm_readyness)
    {
        case USBDCM_JUSTMANUFACTURED:                                                       //first use - save values in flash
            usbdcmInitFlashStorage();
            break;

        case USBDCM_READYFORUSE:                                                            //not first use - get values from flash
            usbdcmGetFlash();
            break;
    }

    usbdcm_sm_relays    = 0;
    usbdcm_sm_5v_present = 0;
    usbdcm_smch = 0;
    if (usbdcm_sm)
    {                                                                                       //set SM to idle
        usbdcmSmWriteStatus(USBDCM_SM_IDLE);
        for (i=0; i<16; i++) sm_relay[i] = 0;                                               //set relay status to idle = all bypass on, all dcct off, vb dcct on single
    }
    else
    {
        usbdcmResetCpld();
        usbdcmReadStatus(simpleshell_strtmp);                                              	//if CDC, then read status
        usbdcmDacOutV(1, 0);                                                               	//initialise DACs to zero
        usbdcmDacOutV(0, 0);                                                               	//initialise DACs to zero
        usbdcm_dac[0] = 0;
        usbdcm_dac[1] = 0;
        usbdcm_dac_raw[0] = 0;
        usbdcm_dac_raw[1] = 0;
    }
}

void usbdcmTimer(void)
{
    if (usbdcm_wcnt > 0) {
        usbdcm_wcnt--;
    }
    if (status_cnt > 0) {
        status_cnt--;
    }
}

void usbdcmInitFlashStorage(void)															//First time the flash is written
{
    int i, delay = 10000;

    usbdcm_dac0_0mat  = 0.0;                                                                //initialise usbdcm_dac values that correspond to 0mA and 10mA
    usbdcm_dac0_10mat = 10.0;                                                               //these are the calibration constants for the 10mA current
    usbdcm_dac0_slope_mat = (usbdcm_dac0_10mat - usbdcm_dac0_0mat) * 100.0;                 //initialise conversion factor: current to voltage -> 1000 (0.01A=10V)
    usbdcm_adc_d0 = 0;                                                                      //initialise the zero value
    usbdcm_adc_vref = 10.0;                                                                 //initialise the values for the FS ADC calibration - vref voltage
    usbdcm_adc_dref = 32768;                                                                //initialise FS value
    usbdcm_adc_slope  = (usbdcm_adc_vref - usbdcm_adc_v0) /
                           (usbdcm_adc_dref - usbdcm_adc_d0);
    usbdcm_kp = USBDCM_KP_DEFAULT;                                                          //initialise autonull loop parameters
    usbdcm_ki = USBDCM_KI_DEFAULT;
    usbdcm_sm = 0;																			//number of channels selected on SM
    i_meas1a = 1;																			//calibration constant for current measurement
    calp05 = 1;																				//calibration constants for power supply voltage readings
    calp15 = 1;
    caln15 = 1;

    usbdcmWriteEnableFlash();																//write all values in the flash for the first time

    usbdcmWriteFlashFloat(USBDCM_ADDR_DAC0_0mAT, usbdcm_dac0_0mat);
    usbdcmWriteFlashFloat(USBDCM_ADDR_DAC0_10mAT, usbdcm_dac0_10mat);
    usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_KP, usbdcm_kp);
    usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_KI, usbdcm_ki);
    usbdcmWriteFlashFloat(USBDCM_ADDR_ADC_D0, usbdcm_adc_d0);
    usbdcmWriteFlashFloat(USBDCM_ADDR_ADC_VREF, usbdcm_adc_vref);
    usbdcmWriteFlashFloat(USBDCM_ADDR_ADC_DREF, usbdcm_adc_dref);

    usbdcmWriteFlash8(USBDCM_ADDR_DAC_CAL, USBDCM_UNCAL);                                  	//USBDCM DAC and ADC systems calibration status = 0
    for (i=0; i<delay; i++);
    usbdcmWriteFlash8(USBDCM_ADDR_ADC_CAL, USBDCM_UNCAL);
    for (i=0; i<delay; i++);
    usbdcmWriteFlash8(USBDCM_ADDR_SM, usbdcm_sm);
    for (i=0; i<delay; i++);
    usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_IMEAS1A, i_meas1a);
    for (i=0; i<delay; i++);
    usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_P05, calp05);
    for (i=0; i<delay; i++);
    usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_P15, calp15);
    for (i=0; i<delay; i++);
    usbdcmWriteFlashFloat(USBDCM_ADDR_CAL_N15, caln15);
    for (i=0; i<delay; i++);
    usbdcmWriteFlash8(USBDCM_ADDR_READYNESS, USBDCM_READYFORUSE);                          	//flash is now initialised, ready for use
    for (i=0; i<delay; i++);
    usbdcmWriteFlashFloat(USBDCM_ADDR_DAC_NULL, last_autonull);								//last value for CDC offset correction, mcb 29-06-2016
    for (i=0; i<delay; i++);

    usbdcmWriteDisableFlash();
}

void usbdcmGetFlash(void){																	//get stored values from the flash

    usbdcm_dac0_0mat      = usbdcmReadFlashFloat(USBDCM_ADDR_DAC0_0mAT);
    usbdcm_dac0_10mat     = usbdcmReadFlashFloat(USBDCM_ADDR_DAC0_10mAT);
    usbdcm_dac0_slope_mat = (usbdcm_dac0_10mat - usbdcm_dac0_0mat) * 100.0;
    usbdcm_kp             = usbdcmReadFlashFloat(USBDCM_ADDR_CAL_KP);
    usbdcm_ki             = usbdcmReadFlashFloat(USBDCM_ADDR_CAL_KI);

    i_meas1a              = usbdcmReadFlashFloat(USBDCM_ADDR_CAL_IMEAS1A);
    calp05                = usbdcmReadFlashFloat(USBDCM_ADDR_CAL_P05);
    calp15                = usbdcmReadFlashFloat(USBDCM_ADDR_CAL_P15);
    caln15                = usbdcmReadFlashFloat(USBDCM_ADDR_CAL_N15);

    usbdcm_adc_d0         = usbdcmReadFlashFloat(USBDCM_ADDR_ADC_D0);
    usbdcm_adc_vref       = usbdcmReadFlashFloat(USBDCM_ADDR_ADC_VREF);
    usbdcm_adc_dref       = usbdcmReadFlashFloat(USBDCM_ADDR_ADC_DREF);
    usbdcm_adc_slope      = (usbdcm_adc_vref - usbdcm_adc_v0) /
                              (usbdcm_adc_dref - usbdcm_adc_d0);
    last_autonull         = usbdcmReadFlashFloat(USBDCM_ADDR_DAC_NULL);						//last value for CDC offset correction, mcb 29-06-2016

    usbdcm_dac_cal        = usbdcmReadFlash8(USBDCM_ADDR_DAC_CAL);
    usbdcm_adc_cal        = usbdcmReadFlash8(USBDCM_ADDR_ADC_CAL);
    usbdcm_sm             = usbdcmReadFlash8(USBDCM_ADDR_SM);

}

void usbdcmDacOut(unsigned char ch, unsigned val)
{
    static unsigned char i;
    static unsigned v;

    usbdcm_dac_raw[ch] = (int)val - 32768;
    v = val;
    if (!ch)
    {
        USBDCM_DAC1SCLK = 0;                                                                //data will be shifted-in at the rising CLK edge
        USBDCM_DAC1CS   = 0;                                                                //data is read from DIN when CS is active (low)
        for (i=0; i<16; i++)
        {                                                                                   //the usbdcm_dac is a 16 bit
            USBDCM_DAC1SCLK = 0;
            if (v & 0x8000)
            {                                                                               //check bits of the word one by one, start on MSB
                USBDCM_DAC1DIN = 1;                                                         //it is a 1 so a 1 is sent
            }
            else
            {
                USBDCM_DAC1DIN = 0;                                                         //it is a 0 so a 0 is sent
            }
            USBDCM_DAC1SCLK = 1;                                                            //shift this bit into the usbdcm_dac register - rising CLK edge
            v = v << 1;                                                                     //shift the bits of the word so that the MSB-i bit is checked
        }
        USBDCM_DAC1CS   = 1;                                                                //all bits of the word have been sent,
    }
    else
    {                                                                                       //at the CS rising edge the word is tranferred to the usbdcm_dac latch
        USBDCM_DAC2SCLK = 0;
        USBDCM_DAC2CS   = 0;
        for (i=0; i<16; i++)
        {
            USBDCM_DAC2SCLK = 0;
            if (v & 0x8000)
            {
                USBDCM_DAC2DIN = 1;
            }
            else
            {
                USBDCM_DAC2DIN = 0;
            }
            USBDCM_DAC2SCLK = 1;
            v = v << 1;
        }
        USBDCM_DAC2CS   = 1;
    }
}

void usbdcmDacOutV(unsigned char ch, float val)                                            	//calculates raw from voltage and sends to usbdcm_dac
{                                                                                           //the voltage value
    static long ival,d0,dpfs,dnfs;
    static float v0,vpfs,vnfs,prslope,nrslope;

    switch (ch)
    {
        case 0:
            v0      = USBDCM_DAC_0_V0_NOM;                                                  //zero voltage
            vpfs    = USBDCM_DAC_0_VPFS_NOM;                                                //positive FS voltage
            vnfs    = USBDCM_DAC_0_VNFS_NOM;                                                //negative FS voltage
            d0      = USBDCM_DAC_0_D0_NOM;                                                  //zero raw value
            dpfs    = USBDCM_DAC_0_DPFS_NOM;                                                //positive FS raw value
            dnfs    = USBDCM_DAC_0_DNFS_NOM;                                                //negative FS raw value
            break;
        case 1:
            v0      = USBDCM_DAC_1_V0_NOM;
            vpfs    = USBDCM_DAC_1_VPFS_NOM;
            vnfs    = USBDCM_DAC_1_VNFS_NOM;
            d0      = USBDCM_DAC_1_D0_NOM;
            dpfs    = USBDCM_DAC_1_DPFS_NOM;
            dnfs    = USBDCM_DAC_1_DNFS_NOM;
            break;
    }
    prslope = (float) (dpfs - d0)/(vpfs - v0);
    nrslope = (float) (d0 - dnfs)/(v0 - vnfs);
    if (val < vnfs)
    {
        ival = dnfs;
    } else {
        if (val > vpfs) {
            ival = dpfs;
        }
        else
        {
            if (val > v0)
            {
                ival = d0 + (val - v0) * prslope;                                           //conversion from voltage to raw
            }
            else
            {                                                                               //using the cal constants
                ival = d0 + (val - v0) * nrslope;
            }
        }
    }
    usbdcmDacOut(ch, (unsigned)ival);                                                       //send to usbdcm_dac
    usbdcm_dac[ch] = val;
}


char usbdcmCalDac(void)                                                                    	//calibrates the 0mA and 10mA values
{                                                                                           //this function does the same as the potentiometer
    static int i;
    static char c;                                                                          //adjustment on the 10mA1T card so it should not be used
                                                                                            //unless that adjustment cannot be done
    //usbdcmSetCurrentStep(0.0);                                                          	// 1. set current to 0
    //strcpy(USBDCM_matrixstring, "0000000000");                                            //set the matrix 4x10 that controls the relays
    for(i = 0; i < 10; i++) USBDCM_matrixstring[i] = 0x00;
    usbdcm_dac0_0mat = -5;                                                                  //be sure that the usbdcm_dac is at 0
    usbdcmDacOutV(0, usbdcm_dac0_0mat);                                                    	//passing a negative value will send 0L to the usbdcm_dac
    USBDCM_matrixstring[6] = 0x01;                                                          //1T usbdcm_dac winding ON
    USBDCM_matrixstring[7] = simpleshell_secrelays;                                         //setting the secondary turns according to the range
    USBDCM_matrixstring[8] = 0x01;                                                          //clamp always off
    usbdcmWriteStatus(USBDCM_matrixstring);                                                	//send to the primary relays card
    usbdcm_wcnt = 20000;                                                                    //wait 2s
    while (usbdcm_wcnt);
    usbdcm_dac0_0mat = usbdcmAutonullDac1t(1);                                            	//find the value for 0AT
    usbdcm_wcnt = 10000;                                                                    //wait 1s
    while (usbdcm_wcnt);
    USBDCM_matrixstring[6] = 0x0D;                                                          //set cal winding on inverted, usbdcm_dac on non inverted
    USBDCM_matrixstring[7] = simpleshell_secrelays;
    USBDCM_matrixstring[8] = 0x01;                                                          // clamp always off
    usbdcmWriteStatus(USBDCM_matrixstring);
    usbdcm_wcnt = 20000;                                                                    //wait 2s
    while (usbdcm_wcnt);                                                                    //find the value for 10mAT
    usbdcm_dac0_10mat = usbdcmAutonullDac1t(0);                                           	//null and store the usbdcm_dac value that nulls the 1T CAL winding                                                                                     //set cal winding off not inverted, usbdcm_dac on non inverted
    usbdcm_wcnt = 10000;                                                                    //wait 1s
    while (usbdcm_wcnt);                                                                   	//now put everything back as it should be
    usbdcmDacOutV(0, usbdcm_dac0_0mat);                                                    	//usbdcm_dac on and at 0
    USBDCM_matrixstring[6] = 0x01;
    USBDCM_matrixstring[7] = simpleshell_secrelays;
    USBDCM_matrixstring[8] = 0x01;                                                          // clamp always off
    usbdcmWriteStatus(USBDCM_matrixstring);
    usbdcm_wcnt = 20000;                                                                    //wait 2s
    while (usbdcm_wcnt);
    usbdcm_dac0_slope_mat = (usbdcm_dac0_10mat - usbdcm_dac0_0mat) * 100.0;                 //now calculate the conversion factor using the                                                                                            //measured values
    if (usbdcm_autonull && !usbdcm_autonull_sat)
    {                                                                                       //only successful if we could null
        usbdcmWriteEnableFlash();
        usbdcmWriteFlashFloat(USBDCM_ADDR_DAC0_0mAT, usbdcm_dac0_0mat);                    	//store the calibration factors in the flash
        usbdcmWriteFlashFloat(USBDCM_ADDR_DAC0_10mAT, usbdcm_dac0_10mat);
        usbdcmWriteFlash8(USBDCM_ADDR_DAC_CAL, USBDCM_CALOK);
        usbdcmWriteDisableFlash();
        c = 1;
    }
    else
    {
        c = 0;
    }
                                                                                            //report results
    for (i=0; i<20; i++)
    {
        SERIAL_FLUSH0;
    }
    return c;                                                                               //return successful or not
}

                                                                                            //the ADC is a differential input ADC with vin=+-5V
char usbdcmAdcCal(void)                                                                    	//the input signal 0..10V is attenuated x2 in the acq circuit
{                                                                                           //since attenuation is not exactly x2 calibration is essential
    static int i;                                                                           //Calibration is done using a local zero and 10V reference
    static char c;
                                                                                            //which can be selected through the MUX
    usbdcmAdcCfg(USBDCM_SE, 14);                                                           	//channel corresponding to 0V ref - see long note below
    usbdcm_wcnt = usbdcm_adcdelay;
    while (usbdcm_wcnt);
    usbdcm_adc_d0 = 0;
    for (i=0; i<USBDCM_ADC_CALCYCLES; i++)
    {
        usbdcm_adc_d0 += (long)usbdcmAdcIn();
    }
    usbdcm_adc_d0 /= (long)USBDCM_ADC_CALCYCLES;
    usbdcmAdcCfg(USBDCM_SE, 15);                                                           //channel corresponding to 10V ref - see long note below
    usbdcm_wcnt = usbdcm_adcdelay;
    while (usbdcm_wcnt);
    usbdcm_adc_dref = 0;
    for (i=0; i<USBDCM_ADC_CALCYCLES; i++)                                                 //please notice that the usbdcmAdcIn returns a INT16
    {                                                                                      //b15 is the sign, so FS = 7FFFh, -FS = 8000h
        usbdcm_adc_dref += (long)usbdcmAdcIn();                                            //but for the calibration calculation we work with INT32
    }                                                                                      //and we get a float: usbdcm_adc_slope,
    usbdcm_adc_dref   /= (long)USBDCM_ADC_CALCYCLES;                                       //which we use for correction when calculating the ADC reading
    usbdcm_adc_slope  = (usbdcm_adc_vref - usbdcm_adc_v0) /
        (usbdcm_adc_dref - usbdcm_adc_d0);
    if ((-5 < usbdcm_adc_d0) && (usbdcm_adc_d0 < 5) && (usbdcm_adc_dref < 32000))
    {                                                                                       //check if calibration is reasonable
        usbdcmWriteEnableFlash();                                                          	//write in the flash
        usbdcmWriteFlashFloat(USBDCM_ADDR_ADC_D0, usbdcm_adc_d0);
        usbdcmWriteFlashFloat(USBDCM_ADDR_ADC_DREF, usbdcm_adc_dref);
        usbdcmWriteFlash8(USBDCM_ADDR_ADC_CAL, USBDCM_CALOK);
        usbdcmWriteDisableFlash();
        c = 1;                                                                              //calibration successful
    }
    else
    {
        c = 0;
    }
    return c;
}

void usbdcmAdcCfg(unsigned char val, unsigned char addr)
{
    unsigned char taddr;                                                                    //taddr is the ADC channel and addr is the mux channel
    int i;                                                                                  //see explanation below
    taddr = 0;
    usbdcm_cfgp = usbdcm_cfg;
    if (val == USBDCM_DIFF)
    {                                                                                       //differential channels?
        usbdcm_cfg = (addr << USBDCM_ADDRSH) & USBDCM_ADDRMSK;                              //prepare usbdcm_cfg word: put addr in upper nibble
        usbdcm_cfg |= 0x05;                                                                 //set the control bits in lower nibble as differential
    }
    else
    {                                                                                       //single ended channels?
        switch (addr)
        {
            case 0:                                                                         //this tables matches the address on the
                taddr = 9;                                                                  //multiplexer, i.e. the multiplexer channel (taddr)
                break;                                                                      //with the "virtual" ADC channel on the CDC (addr)
            case 1:                                                                         //the reason is that the ADC channel 1 is not
                taddr = 10;                                                                 //connected to the multiplexer channel 1 but
                break;                                                                      //to the channel 10 instead, probably to
            case 2:                                                                         //make wiring easier on the USB card
                taddr = 11;                                                                 //remember that the pinout of the back connector
                break;                                                                      //was pre-defined so the "virtual" ADC channels
            case 3:                                                                         //could not be modified
                taddr = 12;
                break;                                                                      //WARNING: the addr variable is from 0..15
            case 4:                                                                         //as well as taddr which controls the MUX
                taddr = 13;                                                                 //but in the USBDCM card the channels are
                break;                                                                      //numbered from 1..16 as well as the inputs
            case 5:                                                                         //of the MUX. This means that addr=0, taddr=9
                taddr = 14;                                                                 //is corresponds to CH1 and NO10 in the drawings
                break;
            case 6:
                taddr = 15;
                break;
            case 7:
                taddr = 7;
                break;
            case 8:
                taddr = 8;
                break;
            case 9:
                taddr = 0;
                break;
            case 10:
                taddr = 1;
            break;
            case 11:
                taddr = 2;
                break;
            case 12:
                taddr = 3;
                break;
            case 13:
                taddr = 4;
                break;
            case 14:
                taddr = 5;
                break;
            case 15:
                taddr = 6;
                break;
        }
        usbdcm_cfg = (taddr << USBDCM_ADDRSH) & USBDCM_ADDRMSK;                             //prepare usbdcm_cfg word: put taddr in upper nibble
        usbdcm_cfg |= 0x0A;    // bit 1 and 3 = 1 -> single ended                           //set the control bits in lower nibble as single ended
    }
    USBDCM_ADCCFG = usbdcm_cfg;                                                             //send to port1 (USBDCM_ADCCFG)
}

int usbdcmAdcIn(void)
{
    static int i;
    static long res;

    res = 0;
    USBDCM_ADCCS  = 0;                                                                      //force the CLK to zero after CS
    USBDCM_ADCCLK = 0;
    for (i=0; i<5; i++)
    {                                                                                       //shouldn't ADCCLK variable be initialised ? miguel
        USBDCM_ADCCLK = 1;                                                                  //toggle the clock 5 times and DOUT goes to zero
        USBDCM_ADCCLK = 0;
    }
    USBDCM_ADCCLK = 1;                                                                      //toggle one more time to have DOUT = MSB
    USBDCM_ADCCLK = 0;
    for (i=0; i<16; i++)
    {                                                                                       //now read 16 bits
        res |= USBDCM_ADCDOUT;                                                              //read MSB -> b0 of res
        res <<= 1;                                                                          //shift that bit up to read MSB-1
        USBDCM_ADCCLK = 1;                                                                  //toggle the clock to get MSB-1
        USBDCM_ADCCLK = 0;
    }
    USBDCM_ADCCS  = 1;                                                                      //acquisition finished
    res = (res >> 1) & 0x0000FFFF;                                                          //there was one shift too much, go back
    return (int)res;
}

float usbdcmAdcInV(void)
{
    static long ival;
    static float fval;
    static float afval;
    static int d;
    afval = 0.0;
    for (d=0; d<5; d++)
    {
    	ival = usbdcmAdcIn();                                                             	//convert raw value to voltage
    	fval = usbdcm_adc_v0 + ((long)ival - usbdcm_adc_d0) * usbdcm_adc_slope;           	//we use INT32 as we obtain a float
    	afval += fval;																		//average 5 readings
    }
    fval = afval / 5;
    return fval;
}

void usbdcmWriteStatus(char far *st)
{
    int i, j, delay = 100;
    unsigned char out;
    out = 0;
    for (i = 1; i <= 23; i++)
    {
        switch (i)
        {
            case 0:
                out = 23;
                break;
            case 1:
                out = st[0];
                break;
            case 2:
                out |= USBDCM_WRITECLOCK;
                break;
            case 3:
                out = st[1];
                break;
            case 4:
                out |= USBDCM_WRITECLOCK;
                break;
            case 5:
                out = st[2];
                break;
            case 6:
                out |= USBDCM_WRITECLOCK;
                break;
            case 7:
                out = st[3];
                break;
            case 8:
                out |= USBDCM_WRITECLOCK;
                break;
            case 9:
                out = st[4];
                break;
            case 10:
                out |= USBDCM_WRITECLOCK;
                break;
            case 11:
                out = st[5];
                break;
            case 12:
                out |= USBDCM_WRITECLOCK;
                break;
            case 13:
                out = st[6];
                break;
            case 14:
                out |= USBDCM_WRITECLOCK;
                break;
            case 15:
                out = st[7];
                break;
            case 16:
                out |= USBDCM_WRITECLOCK;
                break;
            case 17:
                out = st[8];
                break;
            case 18:
                out |= USBDCM_WRITECLOCK;
                break;
            case 19:
                out = st[9];
                break;
            case 20:
                out |= USBDCM_WRITECLOCK;
                break;
            case 21:
                out = 0;
                break;
            case 22:
                out = USBDCM_WRITELATCH;
                break;
            case 23:
                out = 0;
                break;
        }
        for (j=0; j<delay; j++);
        USBDCM_DIGOUT(out);
    }
}

void  usbdcmResetCpld(void)
{
    int j, delay = 100;

    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_WRITECLOCK | USBDCM_READCLOCK);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(0);
    usbdcm_npri  = 0;
    usbdcm_csec = 0;
    usbdcm_nsec  = 0;
    usbdcm_fs    = 0.0;
    usbdcm_sm_relays = 0;
    usbdcm_smch = 0;                                                                        //set SM to idle
    for (j=0; j<16; j++) sm_relay[j] = 0;                                                   //set relay status to idle = all bypass on, all dcct off, vb dcct on single
}

int usbdcmReadStatus(char far *st)
{
    int i, j, k, bin[10], delay = 100;
    int sbits;
    char st_rcv[10];
    char vbtempok, vbpwrok;
    char byte9, byte10, ext_alrm;
    sbits = 0;
    vbtempok = 0;
    vbpwrok = 0;

    for (j=0; j<delay; j++);                                                                //start of sequence to read the
    USBDCM_DIGOUT(USBDCM_READLATCH);                                                        //input table from the relay card
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_READLATCH | USBDCM_READCLOCK);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_READLATCH);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(0);
    for (j=0; j<delay; j++);
    for (i=0; i<12; i++)
    {                                                                                       //reading the input I/O port
        if (i > 1)                                                                          //we toggle the CLK twice before reading the first byte
        {                                                                                   //lower nibble connected to relay card b0..b3
            k = i - 2;                                                                      //and b4, b5 connected to 5V and VB_active alarms
            bin[k]  = USBDCM_DIGIN;                                                         //let's update all the alarms and relay
            if(k >= 0 && k < 8)
            {
                cdc_relay[k*4 + 3] = (bin[k] & 0x0001) ? 1 : 0;
                cdc_relay[k*4 + 2] = (bin[k] & 0x0002) ? 1 : 0;
                cdc_relay[k*4 + 1] = (bin[k] & 0x0004) ? 1 : 0;
                cdc_relay[k*4 + 0] = (bin[k] & 0x0008) ? 1 : 0;
            }
            if (k == 8)
            {
                clamp_on = (bin[k] & 0x0001) ? 0 : 1;                                       //when this bit is 1 the clamp is off
                vbtempok = (bin[k] & 0x0002) ? 1 : 0;                                       //needs analysing in conjunction with other bits, see further down
                pbc_cal_ok = (bin[k] & 0x0004) ? 1 : 0;                                     //when this bit is 1 the PBC cal is ok
                pbc_cmpl_ok = (bin[k] & 0x0008) ? 0 : 1;                                    //when this bit is 1 the PBC voltage is OK
            }
            if (k == 9)
            {
                pbc_pwr_ok = (bin[k] & 0x0001) ? 1 : 0;                                     //when this bit is 1 the PBC power is ok
                pbc_chr_ok = (bin[k] & 0x0002) ? 1 : 0;                                     //when this bit is 1 the PBC charge is ok
                if ((bin[k] & 0x0004) && (bin[k] & 0x0008))
                {                                                                           //when both are high, 7<temp<12
                    pbc_temp_warn = 1;                                                      //ATTENTION: the fact that the warning is when both are high
                    pbc_temp_fault = 0;                                                     //is not clear when reading the CDC manual - refer to the PBC manual for this
                }
                else
                {                                                                           //temp>12
                    if (!(bin[k] & 0x0004) && (bin[k] & 0x0008))
                    {
                        pbc_temp_warn = 0;
                        pbc_temp_fault = 1;
                    }
                    else
                    {
                        pbc_temp_warn = 0;                                                  //we don't distinguish if it is too high
                        pbc_temp_fault = 0;
                    }
                }                                                                           //or too low, also if both are high we generate   //a fault because it is an invalid readin anyway
            }
            cdc_pwr_ok = (bin[k] & 0x0010) ? 1 : 0;
            vbpwrok = (bin[k] & 0x0020) ? 0 : 1;                                            //needs analysing in conjunction with other bits, see further down
                                                                                            //From vbpwrpok and vbtempok we extract 3 alarms: vb_pwr_ok, vb_con_ok, vb_temp_ok
            if (!vbpwrok & vbtempok)                                                        //power is OFF and cable ON
            {
                vb_mode = 1;
                vb_con_ok = 1;
                vb_pwr_ok = 0;
                vb_temp_ok = 1;
            }
            else
            {
                if (!vbpwrok & !vbtempok)                                                   //cable off so other alarms don't care (= 1)
                {
                    vb_mode = 0;
                    vb_con_ok = 1;
                    vb_pwr_ok = 1;
                    vb_temp_ok = 1;
                }
                else
                {
                    vb_mode = 1;
                    if (vbpwrok & !vbtempok)
                    {                                                                       //power is on, cable on and temp is high
                        vb_temp_ok = 0;
                        vb_pwr_ok = 1;
                        vb_con_ok = 1;
                    }
                    else
                    {
                        vb_temp_ok = 1;                                                     //power is on, cable on and temp is ok
                        vb_con_ok = 1;
                        vb_pwr_ok = 1;
                    }
                }
            }
            sbits   = bin[k];                                                               //store the last reading: byte 10 -> this is the complete byte
            bin[k] &= 0x000F;                                                               //so it includes the two external alarms on b4, b5
            st_rcv[k] = bin[k];                                                             //in bin[k] keep only lower nibble b0..b3 (4x10 matrix)
        }
        for (j=0; j<delay; j++);
        USBDCM_DIGOUT(USBDCM_READCLOCK);                                                    //toggle the readclock
        for (j=0; j<delay; j++);
        USBDCM_DIGOUT(0);
        for (j=0; j<delay; j++);
    }                                                                                       //represent the 5V and VB_active alarms
                                                                                            //let's build the status word
    sbits &= 0x0000003F;                                                                    //we are interested in b0..b5 from byte 10
    sbits = sbits << 4;                                                                     //let's shift left and add b0..b3 from byte 9
    sbits = sbits | bin[8];                                                                 //sbits is now composed by the two nibbles
    sbits = sbits & 0x000003FF;
    byte9 = sbits & 0x000F;                                                                 //let's add 3 bytes with this info
    byte10 = (sbits >> 4) & 0x000F;                                                         //to our input table
    ext_alrm = (sbits >> 8) & 0x0003;

    st[7] = st_rcv[7];                                                                      //the response string has 12 chars
    st[6] = st_rcv[6];                                                                      //the first is the 10 bytes of the 4x10 matrix
    st[5] = st_rcv[5];                                                                      //then comes  external alarms
    st[4] = st_rcv[4];                                                                      //and the 12th byte is the NULL
    st[3] = st_rcv[3];
    st[2] = st_rcv[2];
    st[1] = st_rcv[1];
    st[0] = st_rcv[0];

  return sbits;
}

void usbdcmSetCurrent(double current)
{
    static double diff;
    static double limstep;
    static int    nsteps;
    static int    i;

    if (current != usbdcm_current)
    {
        usbdcm_prevcurrent = usbdcm_current;
        usbdcm_current     = current;
        diff    = usbdcm_current - usbdcm_prevcurrent;
        limstep = 4096 * 0.01 / usbdcm_nsec * usbdcm_limStepSize / 100;
        nsteps  = (int)floor(fabs(diff / limstep));
        if (usbdcm_current < usbdcm_prevcurrent)
        {
            limstep = -limstep;
        }
        current = usbdcm_prevcurrent;
        for (i=0; i<nsteps; i++)
        {
            current +=  limstep;
            usbdcmSetCurrentStep(current);
            usbdcm_wcnt = usbdcm_relaydelay;
            while (usbdcm_wcnt);
        }
        if (current != usbdcm_current)                                                      // remaining current
        {
            current = usbdcm_current;
            usbdcmSetCurrentStep(current);
        }
    }
}

void usbdcmSetCurrentStep(double pcurrent)
{
    static int  inv      = 0;
    static int  i,j,nptmp,ctmp;

    if (pcurrent < 0)
    {                                                                                       //calculate the primary relay configuration
        inv = 1;
        pcurrent = - pcurrent;
    }
    else
    {
        inv = 0;
    }                                                                                       //calculate the matrix corresponding to the primary relays
    usbdcm_pat = pcurrent * (float)usbdcm_nsec;                                             //primary Amp Turns (pat) = current * secondary turns
    usbdcm_npri = (int)floor(usbdcm_pat * 100);                                             //so nprim = integer part of (pat/10mA)
    usbdcm_at = usbdcm_pat - (float)usbdcm_npri * 0.01;                                     //calculate the remaining At for the usbdcm_dac
    for(i = 0; i < 10; i++) USBDCM_matrixstring[i] = 0x00;
    USBDCM_matrixstring[7] = simpleshell_secrelays;                                         //set the range (row 8 of output matrix)
    USBDCM_matrixstring[8] = 0x01;                                                          //clamp stays off (row 9 of output matrix)
    j = 0;                                                                                  //the 10th row is only alarms so no need to set it
    if (inv)
    {
        ctmp = 0x0A;                                                                        //set polarity inverted (bits 3 and 1 of each row)
    }
    else
    {
        ctmp = 0x00;
    }
                                                                                            //fill the command string
    for (i=11; i>=0; i--)
    {                                                                                       //nprim is a 12 bit word
        nptmp = usbdcm_npri >> i;                                                           //each bit is a winding, let's check it one by one
        if (nptmp & 0x0001)
        {                                                                                   //starting on the MSB
            if (i & 0x0001)
            {                                                                               //check if the bit is a 1
                ctmp |= 0x0004;                                                             //check if it is an odd or even winding:
            }
            else
            {                                                                               //the numbers of the windings go from 11 to 0
                ctmp |= 0x0001;                                                             //winding 11 is the 2048T and winding 10 the 1024T
            }                                                                               //to put winding 11 on, send a 0100
        }                                                                                   //to put winding 10 on send a 0001
                                                                                            //and repeat it for all rows
    //if (i & 0x0001 != 0x0001) {          why this doesn't work?
        if (i % 2 == 0)
        {                                                                                   //there are two windings per row so after two windings
            USBDCM_matrixstring[j] = ctmp;                                                  //save the value of this row
            if (inv)
            {                                                                               //set polarity inverted (bits 3 and 1 of each row)
                ctmp = 0x0A;
            }
            else
            {
                ctmp = 0x00;
            }
            j++;
        }
    }                                                                                       //cal and dac winding
  //USBDCM_matrixstring[j] = '0';   // no cal, no dac (to be modified)
    if (inv)                                                                                // no cal, dac negative
    {
        USBDCM_matrixstring[j] = 0x03;
    }
    else
    {                                                                                       // no cal, dac positive
        USBDCM_matrixstring[j] = 0x01;
    }
    usbdcmWriteStatus(USBDCM_matrixstring);                                                	//send the 4x10 matrix
    usbdcm_vdac_cal = usbdcm_dac0_0mat + usbdcm_dac0_slope_mat * usbdcm_at;                 //update usbdcm_dac (considering the 10mAT calibration)
    usbdcmDacOutV(0, usbdcm_vdac_cal);                                                     	//set the 1T dac value

}

void usbdcmAutonullPid(void)
{
    static int   i,j,n;
    static float meas,ref,err,ierr,ctrl,kp,ki,maxabserr;
    static char  sat = 0;

    usbdcmAdcCfg(USBDCM_DIFF, 0);                                                          	//just be sure we are using a differential measurement
    usbdcm_wcnt = usbdcm_adcdelay;
    while (usbdcm_wcnt);
    usbdcmAdcInV();                                                                        	//read the nulling channel
    usbdcm_wcnt = usbdcm_autonulldelay;                                                    	//250ms pause before starting nulling
    while (usbdcm_wcnt);
    usbdcm_autonull  = 0;
    ref       = 0.0;
    ierr      = 0.0;
    n         = 20;
    kp        = usbdcm_kp;
    ki        = usbdcm_ki;
    maxabserr = 0.0025 * usbdcm_fs;                                                         //corresponds to 0.05 ppm
    usbdcmDacOutV(1, last_autonull);                                                       	//apply the last nulling value known
    usbdcm_wcnt = usbdcm_autonulldelay;                                                     //wait 250ms
    while (usbdcm_wcnt);
    for (i=0; i<usbdcm_nautonull; i++)
    {                                                                                       //let's try to null
        meas = 0;
        for (j=0; j<n; j++)
        {                                                                                   //here we measure the nulling meter
            usbdcm_wcnt = 100;
            while (usbdcm_wcnt);
            usbdcmAdcCfg(USBDCM_DIFF, 0);
            usbdcm_wcnt = usbdcm_adcdelay;
            while (usbdcm_wcnt);
            meas += usbdcmAdcInV();
        }
        meas /= n;                                                                          //calculate the average of 250 measurements
        if ((meas > maxabserr) || (meas < -maxabserr))
        {                                                                                   //the nulling error is too big
            err  = ref - meas;                                                              //calculate the loop error
            ierr = ierr + err;                                                              //calculate the integral of the error
            ctrl = kp * err + ki * ierr;                                                    //calculate the loop output
            usbdcm_autonull_sat = 0;
            if (ctrl > 10.0)
            {
                ctrl = 10.0;
                usbdcm_autonull_sat = 1;
            }
            else
            {
                if (ctrl < -10.0)
                {
                    ctrl = -10.0;
                    usbdcm_autonull_sat = 1;
                }
            }
            usbdcmDacOutV(1, ctrl);                                                        	//apply the loop output
            usbdcm_wcnt = usbdcm_autonulldelay;                                             //wait 250ms
            while (usbdcm_wcnt);
        }
        else
        {
            usbdcm_autonull = 1;                                                            //null is done
            last_autonull = ctrl;
            break;
        }
    }
}


float usbdcmAutonullDac1t(int offset)
{
    static int   i,j,n;
    static float meas,ref,err,ierr,ctrl,ctrl0,kp,ki,maxabserr;
    static char  sat = 0;

    usbdcmAdcCfg(USBDCM_DIFF, 0);                                                          	//just be sure we are using a differential measurement
    usbdcm_wcnt = usbdcm_adcdelay;
    while (usbdcm_wcnt);
    usbdcmAdcInV();                                                                        	//read the nulling channel
    usbdcm_wcnt = usbdcm_autonulldelay;                                                     //250ms pause before starting nulling
    while (usbdcm_wcnt);
    usbdcm_autonull  = 0;
    ref       = 0.0;
    ierr      = 0.0;
    n         = 20;
    kp        = usbdcm_kp * 0.02;
    ki        = usbdcm_ki * 0.02;
    maxabserr = 0.0025 * usbdcm_fs;

    if (offset)
    {                                                                                       //offset or gain?
        ctrl0 = 0;
    }
    else
    {
        ctrl0 = 9.99;                                                                       //feedforward because we know the output
    }
    usbdcmDacOutV(0, ctrl0);
    for (i=0; i<usbdcm_nautonull; i++)
    {                                                                                       //let's try to null
        meas = 0;
        for (j=0; j<n; j++)
        {                                                                                   //here we measure the nulling meter
            usbdcm_wcnt = 100;
            while (usbdcm_wcnt);
            usbdcmAdcCfg(USBDCM_DIFF, 0);
            usbdcm_wcnt = usbdcm_adcdelay;
            while (usbdcm_wcnt);
            meas += usbdcmAdcInV();
        }
        meas /= n;                                                                          //calculate the average of 250 measurements
        if ((meas > maxabserr) || (meas < -maxabserr))
        {                                                                                   //the nulling error is too big
            err  = ref - meas;                                                              //calculate the loop error
            ierr = ierr + err;                                                              //calculate the integral of the error
            ctrl = kp * err + ki * ierr + ctrl0;                                          	//calculate the loop output including the feed-forward

            usbdcm_autonull_sat = 0;
            if (ctrl > 10.5)
            {
                ctrl = 10.5;
                usbdcm_autonull_sat = 1;
            }
            else
            {
                if (ctrl < -10.5)
                {
                    ctrl = -10.5;
                    usbdcm_autonull_sat = 1;
                }
            }
            usbdcmDacOutV(0, ctrl);                                                        	//apply the loop output
            usbdcm_wcnt = usbdcm_autonulldelay;                                             //wait 250ms
            while (usbdcm_wcnt);
        }
        else
        {
            usbdcm_autonull = 1;                                                            //null is done
            break;
        }
    }
    return ctrl;                                                                            //return the calibration value
}



void usbdcmSmWriteStatus(unsigned int st)													//sends the relay command data to the SM relay driver card CPLD
{
    unsigned int i, a, j, delay = 100;														//the relay command data are two bytes

    for (i=0; i<16; i++)
    {
        a    = st & 0x0001;
        st >>= 1;
        for (j=0; j<delay; j++);
        USBDCM_DIGOUT(a);
        for (j=0; j<delay; j++);
        a |= USBDCM_SM_WRITECLOCK;
        USBDCM_DIGOUT(a);
        for (j=0; j<delay; j++);
    }
    USBDCM_DIGOUT(0);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_SM_WRITELATCH);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(0);
    for (j=0; j<delay; j++);
}

unsigned int usbdcmSmReadStatus(void)														//reads the relay status data from the SM relay driver card CPLD
{
    int i, j, k, bin[10], delay = 100;														//this corresponds to two bytes
    int sbits;
    unsigned int resp,test;

    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_SM_READLATCH);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_SM_READLATCH | USBDCM_SM_READCLOCK);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(USBDCM_SM_READLATCH);
    for (j=0; j<delay; j++);
    USBDCM_DIGOUT(0);
    for (j=0; j<delay; j++);

    resp = 0;
    for (i=0; i<16; i++)
    {                                                                                       //toggle the clock twice

        resp >>= 1;
        test = USBDCM_DIGIN; //debug
        resp  |= (USBDCM_DIGIN & USBDCM_SM_2UC) ? 0x0000 : 0x8000;                          //just DIN0 is used to read the status in serial mode
        test = USBDCM_DIGIN; //debug                                                        //the status is a 16 bit array
        usbdcm_sm_5v_present = (USBDCM_DIGIN & USBDCM_SM_5V) ? 1 : 0;						//DIN1 is used as a power alarm
        for (j=0; j<delay; j++);
        USBDCM_DIGOUT(USBDCM_SM_READCLOCK);
        for (j=0; j<delay; j++);
        USBDCM_DIGOUT(0);
        for (j=0; j<delay; j++);
    }

    sm_relay[SEL_CH1]     = (resp & 0x0001) ? 1 : 0;										//a relay is ON when a zero is read
    sm_relay[BP_CH1_OPEN] = (resp & 0x0002) ? 1 : 0;										//update the status of all relays
    sm_relay[SEL_CH2]     = (resp & 0x0004) ? 1 : 0;
    sm_relay[BP_CH2_OPEN] = (resp & 0x0008) ? 1 : 0;
    sm_relay[SEL_CH3]     = (resp & 0x0010) ? 1 : 0;
    sm_relay[BP_CH3_OPEN] = (resp & 0x0020) ? 1 : 0;
    sm_relay[SEL_CH4]     = (resp & 0x0040) ? 1 : 0;
    sm_relay[BP_CH4_OPEN] = (resp & 0x0080) ? 1 : 0;
    sm_relay[SEL_CH5]     = (resp & 0x0100) ? 1 : 0;
    sm_relay[BP_CH5_OPEN] = (resp & 0x0200) ? 1 : 0;
    sm_relay[SEL_CH6]     = (resp & 0x0400) ? 1 : 0;
    sm_relay[BP_CH6_OPEN] = (resp & 0x0800) ? 1 : 0;
    sm_relay[VB_DUAL]     = (resp & 0x1000) ? 1 : 0;
    sm_relay[SEL_CH7]     = (resp & 0x2000) ? 1 : 0;
    sm_relay[BP_CH7_OPEN] = (resp & 0x4000) ? 1 : 0;
    sm_relay[BP_ALL_OPEN] = (resp & 0x8000) ? 1 : 0;

    usbdcm_sm_relays = resp;
    return resp;
}

#define CKINV 0
#if CKINV
  #define CKOFF 1
  #define CKON  0
#else
  #define CKOFF 0
  #define CKON  1
#endif

void  usbdcmInternalAdcInit(void)															//initialisation of internal ADCs
{
    ad0con0 = USBDCM_AD0CON0;																//set internal AD control registers
    ad0con1 = USBDCM_AD0CON1;
    ad0con2 = USBDCM_AD0CON2;
    ad0con3 = USBDCM_AD0CON3;
    ad0con4 = USBDCM_AD0CON4;
    usbdcm_raw_p05 = 0;
    usbdcm_raw_p15 = 0;
    usbdcm_raw_m15 = 0;
    usbdcm_volts_p05 = 5.0;
    usbdcm_volts_p15 = 15.0;
    usbdcm_volts_m15 = -15.0;
    usbdcm_cnt_iadc = USBDCM_CNT_IADC_RELOAD;
    usbdcm_flag_iadc = 0;
}

void usbdcmInitFlash(void)
{
    USBDCM_FLASH_CS   = 0;
    USBDCM_FLASH_DIN  = 0;
    USBDCM_FLASH_SCLK = CKOFF;
}

void usbdcmWriteEnableFlash(void)
{
    USBDCM_FLASH_CS   = 1;

    USBDCM_FLASH_DIN  = 1;  // start bit
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_DIN  = 1;  // 1
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 1;  // 1
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_CS   = 0;
}

void usbdcmWriteDisableFlash(void)
{
    USBDCM_FLASH_CS   = 1;

    USBDCM_FLASH_DIN  = 1;  // start bit
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_DIN  = 1;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 1;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    USBDCM_FLASH_CS   = 0;
}

void usbdcmWriteFlash8(unsigned char addr, unsigned char b)									//writes a byte in the flash
{
    unsigned int  i;
    unsigned char t;
    unsigned long tb;

    USBDCM_FLASH_CS   = 1;

    USBDCM_FLASH_DIN  = 1;  // start bit
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
                                                                                            //WRITE operation code is 01
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 1;  // 1
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
                                                                                            //prepare the word with the address (7 bits)
    tb = (((unsigned)addr & 0x0000007F) << 8) | ((unsigned)b & 0x000000FF);                 //and the data (8 bits)

    for (i=0; i<15; i++)
    {                                                                                       //send the word bit by bit
        t = ((tb & 0x00004000) != 0) ? 1 : 0;                                               //start by the MSB of the 15 bit word
        USBDCM_FLASH_DIN  = t;                                                              //write a bit
        USBDCM_FLASH_SCLK = CKON;                                                           //toggle the clock
        tb <<= 1;                                                                           //and continue with MSB-1
        USBDCM_FLASH_SCLK = CKOFF;
    }

    USBDCM_FLASH_DIN  = 0;
    USBDCM_FLASH_CS   = 0;
}

unsigned char usbdcmReadFlash8(unsigned char addr)											//reads a byte from the flash
{
    unsigned int  i;
    unsigned char t;
    unsigned int  tb;
    unsigned char resp;

    USBDCM_FLASH_CS   = 1;

    USBDCM_FLASH_DIN  = 1;  // start bit
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
                                                                                            //READ operation code is 01
    USBDCM_FLASH_DIN  = 1;  // 1
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;
    USBDCM_FLASH_DIN  = 0;  // 0
    USBDCM_FLASH_SCLK = CKON;
    USBDCM_FLASH_SCLK = CKOFF;

    tb = (unsigned)addr & 0x007F;                                                           //prepare the word with the address (7 bits)

    for (i=0; i<8; i++) {                                                                   //send the word bit by bit
        t = ((tb & 0x40) != 0) ? 1 : 0;                                                     //start by the MSB of the 7 bit word
        USBDCM_FLASH_DIN  = t;
        USBDCM_FLASH_SCLK = 1;
        tb <<= 1;
        USBDCM_FLASH_SCLK = 0;
    }

    resp = 0;
    for (i=0; i<8; i++)
    {                                                                                       //now to read the value
        resp <<= 1;                                                                         //make space for next bit
        resp |= USBDCM_FLASH_DOUT;                                                          //read the bit
        USBDCM_FLASH_SCLK = 1;                                                              //toggle the clock
        USBDCM_FLASH_SCLK = 0;
    }

  USBDCM_FLASH_CS   = 0;

  return resp;
}

void usbdcmWriteFlashFloat(unsigned char addr, float f)										//writes a float in the flash
{
    int i, delay = 10000;
    usbdcm_flb t;

    t.f = f;
    //usbdcmWriteEnableFlash();
    usbdcmWriteFlash8(addr++, t.bytes.b0);
    for (i=0; i<delay; i++);
    usbdcmWriteFlash8(addr++, t.bytes.b1);
    for (i=0; i<delay; i++);
    usbdcmWriteFlash8(addr++, t.bytes.b2);
    for (i=0; i<delay; i++);
    usbdcmWriteFlash8(addr,   t.bytes.b3);
    for (i=0; i<delay; i++);
}

float usbdcmReadFlashFloat(unsigned char addr)												//reads a float from the flash
{
    usbdcm_flb resp;
    resp.bytes.b0 = (unsigned long)usbdcmReadFlash8(addr++);
    resp.bytes.b1 = (unsigned long)usbdcmReadFlash8(addr++);
    resp.bytes.b2 = (unsigned long)usbdcmReadFlash8(addr++);
    resp.bytes.b3 = (unsigned long)usbdcmReadFlash8(addr);
    return resp.f;
}

void usbdcmSwapbytes(void *object, size_t size)
{
    unsigned char *start, *end;
    for (start = object, end = start + size - 1; start < end; ++start, --end)
    {
        unsigned char swap = *start;
        *start = *end;
        *end = swap;
    }
}
