/*---------------------------------------------------------------------------------------------------------*\
  File:     err.c                                                                       Copyright CERN 2011

  License:  This file is part of libreg.

            libreg is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Purpose:  Regulation error functions

            These functions can be used for any sort of regulation (current, field, voltage).  They
            maintain a history of the reference so that the measurement can be compared against the
            reference taking into account the tracking delay.
\*---------------------------------------------------------------------------------------------------------*/

#include "libreg1.h"

/*---------------------------------------------------------------------------------------------------------*/
void regErrInitLimits(struct reg_err *err, float err_warning_limit, float err_fault_limit)
/*---------------------------------------------------------------------------------------------------------*\
  This function can be called to initialise the warning and fault limits of the reg_err structure.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Set the new fault and warning limits

    err->warning_limit = err_warning_limit;
    err->fault_limit   = err_fault_limit;

    // If the fault or warning is disabled (set to zero), then reset the corresponding flag and counter.

    if(err->warning_limit == 0.0)
    {
        err->flags.warning          = 0;
        err->warning_filter_counter = 0;
    }

    if(err->fault_limit   == 0.0)
    {
        err->flags.fault            = 0;
        err->fault_filter_counter   = 0;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void regErrInitDelay(struct reg_err *err, float *buf, float delay_in_iters, uint32_t undersampled_flag)
/*---------------------------------------------------------------------------------------------------------*\
  This function can be called to initialise the reg_err delay structure. The buf pointer should be to a
  float array of length (int)delay_in_iters.  If the function is called again then buf can be NULL and the
  old pointer to buf will be preserved.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Initialise delay parameters

    regDelayInitPars(&err->delay, buf, delay_in_iters, undersampled_flag);

    // Keep filter period and re-initialise counter limit and reset error variables

    regErrInitFilterPeriod(err, err->filter_period_iters);
}
/*---------------------------------------------------------------------------------------------------------*/
void regErrInitFilterPeriod(struct reg_err *err, uint32_t filter_period_iters)
/*---------------------------------------------------------------------------------------------------------*\
  If the regulation period is a multiple of the iteration period then voltage regulation error will need
  to be filtered to avoid aliasing.  This is enabled by passing the regulation period in iterations in the
  parameter filter_period_iters. This function sets the filter period for the error calculation and
  re-initialises the error calculation. For current/field regulation error the filter_period_iters can
  be set to 1 since the error is calculated every regulation period.
\*---------------------------------------------------------------------------------------------------------*/
{
    err->filter_period_iters = (filter_period_iters == 0 ? 1 : filter_period_iters);
    err->counter_limit       = (3 * err->delay.delay_int) / err->filter_period_iters;

    regErrInitVars(err);
}
/*---------------------------------------------------------------------------------------------------------*/
void regErrInitVars(struct reg_err *err)
/*---------------------------------------------------------------------------------------------------------*\
  This function can be called to initialise the reg_err structure variables.
\*---------------------------------------------------------------------------------------------------------*/
{
    regDelayInitVars(&err->delay, 0.0);

    err->err                    = 0.0;
    err->err_accumulator        = 0.0;
    err->max_abs_err            = 0.0;
    err->flags.warning          = 0;
    err->flags.fault            = 0;
    err->warning_filter_counter = 0;
    err->fault_filter_counter   = 0;
    err->iteration_counter      = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
static void regErrLimit(uint32_t limit_exceeded_f, uint32_t counter_limit, uint32_t *counter, uint32_t *flag)
/*---------------------------------------------------------------------------------------------------------*\
  This function manages the limits by applying a filter time in both directions.  The error must cross the
  limit for a minimum number of iterations before the flag can change state.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(*flag)                           // If limit flag is currently set
    {
        if(!limit_exceeded_f)
        {
            (*counter)++;
        }
        else
        {
            if(*counter)
            {
                (*counter)--;
            }
        }

        if(*counter > counter_limit)
        {
            *flag    = 0;
            *counter = 0;
        }
    }
    else                                // else limit flag is currently NOT set
    {
        if(limit_exceeded_f)
        {
            (*counter)++;
        }
        else
        {
            if(*counter)
            {
                (*counter)--;
            }
        }

        if(*counter > counter_limit)
        {
            *flag    = 1;
            *counter = 0;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
float regErrCalc(struct reg_err *err, uint32_t enable_max_abs_err, float ref, float meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function can be called to calculate the regulation error and to check the error limits (if supplied).
  The calculation isn't started until the history buffer is full (since the last time regErrInitVars() was
  called).  The calculation of the max_abs_err can be enabled by setting enable_max_abs_err to be non-zero,
  otherwise max_abs_err is zeroed.
\*---------------------------------------------------------------------------------------------------------*/
{
    float       abs_error;
    float       delayed_ref;

    // If reg_err structure not initialised then return immediately

    if(err->filter_period_iters == 0)
    {
        return(0.0);
    }

    // Calculate delayed reference and return if it's not yet valid (circular buffer not yet full)

    if(regDelayCalc(&err->delay, ref, &delayed_ref) == 0)
    {
        return(0.0);
    }

    // Calculate and filter regulation error

    err->err_accumulator += delayed_ref - meas;

    if(++err->iteration_counter >= err->filter_period_iters)
    {
        err->err = err->err_accumulator / (float)err->filter_period_iters;

        abs_error = fabs(err->err);

        // Calculate or reset max abs err

        if(enable_max_abs_err)
        {
            if(abs_error > err->max_abs_err)
            {
                err->max_abs_err = abs_error;
            }
        }
        else
        {
            err->max_abs_err = 0.0;
        }

        // Check error limits if they are non-zero

        if(err->warning_limit > 0.0)
        {
            regErrLimit((abs_error > err->warning_limit), err->counter_limit,
                         &err->warning_filter_counter, &err->flags.warning);
        }

        if(err->fault_limit > 0.0)
        {
            regErrLimit((abs_error > err->fault_limit), err->counter_limit,
                         &err->fault_filter_counter, &err->flags.fault);
        }

        // Reset accumulator

        err->err_accumulator   = 0.0;
        err->iteration_counter = 0;
    }

    return(err->err);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: err.c
\*---------------------------------------------------------------------------------------------------------*/
