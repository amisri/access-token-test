/*---------------------------------------------------------------------------------------------------------*\
  File:     rst.c                                                                       Copyright CERN 2011

  License:  This file is part of libreg.

            libreg is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Purpose:  RST regulation algorithm functions
\*---------------------------------------------------------------------------------------------------------*/

#include "libreg1.h"

#define TWO_PI          6.28318530717958647693

/*---------------------------------------------------------------------------------------------------------*/
static void regRstInitPII(struct reg_rst_pars  *pars,
                          float                 period,
                          struct reg_load_pars *load,
                          float                 clbw,
                          float                 clbw2,
                          float                 z,
                          float                 pure_delay)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares coefficients for the RST regulation algorithm to implement a dead-beat PII
  controller provided the voltage source bandwidth is more than ten times faster than the desired closed
  loop bandwidth of the controlled quantity (normally current or field).

  It can be used with slow inductive circuits as well as fast or even resistive loads.  For fast loads,
  clbw is set to 1.0E+5 and z to 0.8.  Normally clbw should equal clbw2 and z should be 0.5.

  The pure_delay parameter informs the algorithm of the pure delay around the loop.  This can be
  a dynamic behaviour (i.e. of the voltage source or FIR filter) crudely modelled as a pure delay for low
  frequencies, or it can be real pure delay due to computation or communications delays.  The pure delay
  shouldn't be more than about 0.4 of the period to maintain stability.
\*---------------------------------------------------------------------------------------------------------*/
{
    float t1;                   // -period / load_Tc
    float a1;                   // -exp(t1)
    float a2;                   // a2 = 1 + a1 = 1 - exp(t1) - use Maclaurin expansion for small t1

    float b0;
    float b1;
    float c1;
    float c2;
    float d1;
    float d2;
    float df;

    t1 = -period / load->tc;
    a1 = -exp(t1);

    if(a1 > -0.99)              // if t1 > 0.01
    {
        a2 = 1.0 + a1;                  // it is okay to use 1 - exp(t1)
    }
    else                        // else use Maclaurin series for exp(t1) = 1 + t1 + t1^2/2! + ...
    {
        a2 = -(t1 * (1.0 + 0.5 * t1));  // This is more precise for small t1
    }

    // Calculate RST coefficients

    df =  pure_delay / period;                            // pure delay fraction
    b0 =  load->gain0 + load->gain1 * a2 * (1.0 - df);
    b1 =  load->gain0 * a1 + load->gain1 * a2 * df;
    c1 = -exp(-period * TWO_PI * clbw);
    c2 =  exp(-period * TWO_PI * clbw2 * z);
    d1 = -2.0 * c2 * cos(period * TWO_PI * clbw2 * sqrt(1.0 - z * z));
    d2 =  c2 * c2;

    pars->rst.r[0] = (c1 + d1 - a1 + 2.0);
    pars->rst.r[1] = (c1 * d1 + d2 + 2.0 * a1 - 1.0);
    pars->rst.r[2] = (c1 * d2 - a1);

    pars->rst.s[0] =  b0;
    pars->rst.s[1] = (b1 - 2.0 * b0);
    pars->rst.s[2] = (b0 - 2.0 * b1);
    pars->rst.s[3] =  b1;

    pars->rst.t[0] =  1.0;
    pars->rst.t[1] = (c1 + d1);
    pars->rst.t[2] = (c1 * d1 + d2);
    pars->rst.t[3] =  c1 * d2;
}
/*---------------------------------------------------------------------------------------------------------*/
static void regRstInitPI(struct reg_rst_pars  *pars,
                         float                 period,
                         struct reg_load_pars *load,
                         float                 clbw)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares coefficients for the RST regulation algorithm to implement a proportional-integral
  controller.  It can be used with fast slightly inductive circuits.
\*---------------------------------------------------------------------------------------------------------*/
{
    float a1 = -exp(-period * (load->ohms_ser + load->ohms_mag) * load->inv_henrys);
    float b1 = (1.0 + a1) / (load->ohms_ser + load->ohms_mag);
    float c1 = -exp(-period * TWO_PI * clbw);

    pars->rst.r[0] = 1.0 + c1;
    pars->rst.r[1] = a1 * pars->rst.r[0];

    pars->rst.s[0] =  b1;
    pars->rst.s[1] = -b1;

    pars->rst.t[0] = pars->rst.r[0];
    pars->rst.t[1] = pars->rst.r[1];
}
/*---------------------------------------------------------------------------------------------------------*/
static void regRstInitI(struct reg_rst_pars  *pars,
                        float                 period,
                        struct reg_load_pars *load,
                        float                 clbw)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares coefficients for the RST regulation algorithm to implement an Integrator only.
  This can be used with resistive circuits.
\*---------------------------------------------------------------------------------------------------------*/
{
    float b1 = 1.0 / (load->ohms_ser + load->ohms_mag);
    float c1 = -exp(-TWO_PI * period * clbw);

    pars->rst.r[0] = 1.0 + c1;

    pars->rst.s[0] =  b1;
    pars->rst.s[1] = -b1;

    pars->rst.t[0] = 1.0 + c1;
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t regRstInit(struct reg_rst_pars  *pars,
                    float                 iter_period,
                    uint32_t              period_iters,
                    struct reg_load_pars *load,
                    float                 clbw,
                    float                 clbw2,
                    float                 z,
                    float                 pure_delay,
                    struct reg_rst       *manual)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares coefficients for the RST regulation algorithm based on the paper EDMS 686163 by
  Hugues Thiesen with extensions from Martin Veenstra to allow a pure loop delay to be accommodated.
  The function returns REG_OK on success and REG_FAULT if s[0] is too small (<1E-10).

  Notes:	The algorithms can calculate the RST coefficients from the clbw/clbw2/z/pure_delay/load
                parameters. This only work well if the voltage source bandwidth is much faster (>100x)
                than the closed loop bandwidth.  This is true for all LHC circuits.  Three controllers
                are selectable:  I, PI and PII.  For the PII regulator (which is normally used operationally),
                the regulation will have a latency (track delay) of exactly 1 period.

		If the voltage source bandwidth is less than a factor 100 above the closed loop bandwidth
		then the algorithms will not produce good results and the RST coefficients will need
		to be calculated manually using Matlab.

  Implementation notes:
                Computing the T0 correction requires a better precision than 32-bit floating point for
                the intermediate results. This is achieved by using the type double for the local variable
                t0_correction. On TI C32 DSP, 'double' is simply an alias for 'float' i.e. 32-bit FP.
                However that DSP can take advantage of the extended 40-bit FP precision of its ALU,
                providing the code is written in such a way that the C32 compiler produces the most efficient
                assembly code. That usually implies to write formulas on a single line, not relying on loops.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t        i;
    double	    t0_correction;

#ifdef LIBREG_NORMALISE_IN_S0
    float           inv_s0;
#endif

    pars->period_iters = period_iters;
    pars->period       = iter_period * period_iters;
    pars->freq         = 1.0 / pars->period;
    pars->status       = REG_OK;

    if(clbw <= 0.0)                          // if CLBW = 0.0 -> MANUAL RST coefficients
    {
        pars->rst = *manual;
    }
    else                                     // else calculate RST coefficients
    {
        pars->rst.track_delay = pars->period;   // Set track delay to 1 period (only true for PII regulator)

        for(i=0 ; i < REG_N_RST_COEFFS ; i++)
        {
            pars->rst.r[i] = pars->rst.s[i] = pars->rst.t[i] = 0.0;
        }

        if(clbw2 > 0.0)                         // If CLBW2 > 0               -> PII regulator (slow inductive load)
        {
            regRstInitPII(pars, pars->period, load, clbw, clbw2, z, pure_delay);
        }
        else if(load->henrys >= 1.0E-10)        // If CLBW2 == 0, HENRYS > 0  ->  PI regulator (fast inductive load)
        {
            regRstInitPI(pars, pars->period, load, clbw);
        }
        else                                    // If CLBW2 == 0, HENRYS == 0 ->   I regulator (resistive load)
        {
            regRstInitI(pars, pars->period, load, clbw);
        }
    }

    // Check that S[0] isn't too small

    if(fabs(pars->rst.s[0]) < 1.0E-10)
    {
        pars->status = REG_FAULT;

        pars->inv_s0           = 0.0;
        pars->t0_correction    = 0.0;
        pars->inv_corrected_t0 = 0.0;
    }
    else
    {
#ifdef LIBREG_NORMALISE_IN_S0
        // Normalise all RST coefficients so that S0 is 1.0

        inv_s0 = 1.0 / pars->rst.s[0];

        for(i=0 ; i < REG_N_RST_COEFFS ; i++)
        {
            pars->rst.r[i] *= inv_s0;
            pars->rst.s[i] *= inv_s0;
            pars->rst.t[i] *= inv_s0;
        }

        pars->inv_s0 = 1.0;
#else
        pars->inv_s0 = 1.0 / pars->rst.s[0];
#endif

        // Calculate floating point math correction for T coefficients to ensure that Sum(T) == Sum(R)

        t0_correction =  ((double)pars->rst.r [0] -
                                  pars->rst.t [0] +
                                  pars->rst.r [1] -
                                  pars->rst.t [1] +
                                  pars->rst.r [2] -
                                  pars->rst.t [2] +
                                  pars->rst.r [3] -
                                  pars->rst.t [3] +
                                  pars->rst.r [4] -
                                  pars->rst.t [4] +
                                  pars->rst.r [5] -
                                  pars->rst.t [5] +
                                  pars->rst.r [6] -
                                  pars->rst.t [6] +
                                  pars->rst.r [7] -
                                  pars->rst.t [7] +
                                  pars->rst.r [8] -
                                  pars->rst.t [8] +
                                  pars->rst.r [9] -
                                  pars->rst.t [9] +
                                  pars->rst.r[10] -
                                  pars->rst.t[10] +
                                  pars->rst.r[11] -
                                  pars->rst.t[11] +
                                  pars->rst.r[12] -
                                  pars->rst.t[12] +
                                  pars->rst.r[13] -
                                  pars->rst.t[13] +
                                  pars->rst.r[14] -
                                  pars->rst.t[14] +
                                  pars->rst.r[15] -
                                  pars->rst.t[15]);

	pars->t0_correction    = t0_correction;
        pars->inv_corrected_t0 = 1.0 / (pars->rst.t[0] + t0_correction);
    }

    // Return the status

    return(pars->status);
}
/*---------------------------------------------------------------------------------------------------------*/
float regRstCalcAct(struct reg_rst_pars *pars, struct reg_rst_vars *vars, float ref, float meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function returns the actuation based on the supplied reference and measurement using the RST
  parameters.  The function regRstHistory() must be called at the end of the iteration to shift the
  RST history.  If the actuation is clipped then regRstCalcRef must be called to re-calculate the
  reference to put in the history.

  Implementation notes:
        Computing the actuation requires a better precision than 32-bit floating point for
        the intermediate results. This is achieved by using the type double for the local variable act.
        On TI C32 DSP, 'double' is simply an alias for 'float' i.e. 32-bit FP. However that DSP can take
        advantage of the extended 40-bit FP precision of its ALU, providing the code is written in such
        a way that the C32 compiler produces the most efficient assembly code. That usually implies to
        write formulas on a single line, not relying on loops.
\*---------------------------------------------------------------------------------------------------------*/
{
    double       act;

    vars->ref [0] = ref;
    vars->meas[0] = meas;

    /*--- Calculate actuation using RST algorithm ---*/

    if(pars->status == REG_OK)
    {
        act =  ((double)pars->t0_correction * ref      +
                (double)pars->rst.t [0] * vars->ref  [0] +
                (double)pars->rst.t [1] * vars->ref  [1] +
                (double)pars->rst.t [2] * vars->ref  [2] +
                (double)pars->rst.t [3] * vars->ref  [3] +
                (double)pars->rst.t [4] * vars->ref  [4] +
                (double)pars->rst.t [5] * vars->ref  [5] +
                (double)pars->rst.t [6] * vars->ref  [6] +
                (double)pars->rst.t [7] * vars->ref  [7] +
                (double)pars->rst.t [8] * vars->ref  [8] +
                (double)pars->rst.t [9] * vars->ref  [9] +
                (double)pars->rst.t[10] * vars->ref [10] +
                (double)pars->rst.t[11] * vars->ref [11] +
                (double)pars->rst.t[12] * vars->ref [12] +
                (double)pars->rst.t[13] * vars->ref [13] +
                (double)pars->rst.t[14] * vars->ref [14] +
                (double)pars->rst.t[15] * vars->ref [15] -
                (double)pars->rst.r [0] * vars->meas [0] -
                (double)pars->rst.r [1] * vars->meas [1] -
                (double)pars->rst.r [2] * vars->meas [2] -
                (double)pars->rst.r [3] * vars->meas [3] -
                (double)pars->rst.r [4] * vars->meas [4] -
                (double)pars->rst.r [5] * vars->meas [5] -
                (double)pars->rst.r [6] * vars->meas [6] -
                (double)pars->rst.r [7] * vars->meas [7] -
                (double)pars->rst.r [8] * vars->meas [8] -
                (double)pars->rst.r [9] * vars->meas [9] -
                (double)pars->rst.r[10] * vars->meas[10] -
                (double)pars->rst.r[11] * vars->meas[11] -
                (double)pars->rst.r[12] * vars->meas[12] -
                (double)pars->rst.r[13] * vars->meas[13] -
                (double)pars->rst.r[14] * vars->meas[14] -
                (double)pars->rst.r[15] * vars->meas[15] -
                (double)pars->rst.s [1] * vars->act  [1] -                 // Skip index 0 for S[] coefficients
                (double)pars->rst.s [2] * vars->act  [2] -
                (double)pars->rst.s [3] * vars->act  [3] -
                (double)pars->rst.s [4] * vars->act  [4] -
                (double)pars->rst.s [5] * vars->act  [5] -
                (double)pars->rst.s [6] * vars->act  [6] -
                (double)pars->rst.s [7] * vars->act  [7] -
                (double)pars->rst.s [8] * vars->act  [8] -
                (double)pars->rst.s [9] * vars->act  [9] -
                (double)pars->rst.s[10] * vars->act [10] -
                (double)pars->rst.s[11] * vars->act [11] -
                (double)pars->rst.s[12] * vars->act [12] -
                (double)pars->rst.s[13] * vars->act [13] -
                (double)pars->rst.s[14] * vars->act [14] -
                (double)pars->rst.s[15] * vars->act [15]) * pars->inv_s0;
    }
    else
    {
        act = 0.0;
    }

    return(vars->act[0] = act);
}
/*---------------------------------------------------------------------------------------------------------*/
float regRstCalcRef(struct reg_rst_pars *pars, struct reg_rst_vars *vars, float act, float meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function must be called in two situations:

    1. After calling regRstCalcAct() if the actuation has been clipped due to limits in the actuator.

    2. If the system is running with an open loop actuation

  The function saves the new actuation in the RST history and re-calculates the reference which is returned.

  Implementation notes:
        Computing the reference requires a better precision than 32-bit floating point for
        the intermediate results. This is achieved by using the type double for the local variable ref.
        On TI C32 DSP, 'double' is simply an alias for 'float' i.e. 32-bit FP. However that DSP can take
        advantage of the extended 40-bit FP precision of its ALU, providing the code is written in such
        a way that the C32 compiler produces the most efficient assembly code. That usually implies to
        write formulas on a single line, not relying on loops.
\*---------------------------------------------------------------------------------------------------------*/
{
    double       ref;

    vars->act [0] = act;
    vars->meas[0] = meas;

    /*--- Recalculate the reference for the new actuation ---*/

    if(pars->status == REG_OK)
    {
        ref =  ((double)pars->rst.s [0] * vars->act  [0] +
                (double)pars->rst.s [1] * vars->act  [1] +
                (double)pars->rst.s [2] * vars->act  [2] +
                (double)pars->rst.s [3] * vars->act  [3] +
                (double)pars->rst.s [4] * vars->act  [4] +
                (double)pars->rst.s [5] * vars->act  [5] +
                (double)pars->rst.s [6] * vars->act  [6] +
                (double)pars->rst.s [7] * vars->act  [7] +
                (double)pars->rst.s [8] * vars->act  [8] +
                (double)pars->rst.s [9] * vars->act  [9] +
                (double)pars->rst.s[10] * vars->act [10] +
                (double)pars->rst.s[11] * vars->act [11] +
                (double)pars->rst.s[12] * vars->act [12] +
                (double)pars->rst.s[13] * vars->act [13] +
                (double)pars->rst.s[14] * vars->act [14] +
                (double)pars->rst.s[15] * vars->act [15] +
                (double)pars->rst.r [0] * vars->meas [0] +
                (double)pars->rst.r [1] * vars->meas [1] +
                (double)pars->rst.r [2] * vars->meas [2] +
                (double)pars->rst.r [3] * vars->meas [3] +
                (double)pars->rst.r [4] * vars->meas [4] +
                (double)pars->rst.r [5] * vars->meas [5] +
                (double)pars->rst.r [6] * vars->meas [6] +
                (double)pars->rst.r [7] * vars->meas [7] +
                (double)pars->rst.r [8] * vars->meas [8] +
                (double)pars->rst.r [9] * vars->meas [9] +
                (double)pars->rst.r[10] * vars->meas[10] +
                (double)pars->rst.r[11] * vars->meas[11] +
                (double)pars->rst.r[12] * vars->meas[12] +
                (double)pars->rst.r[13] * vars->meas[13] +
                (double)pars->rst.r[14] * vars->meas[14] +
                (double)pars->rst.r[15] * vars->meas[15] -
                (double)pars->rst.t [1] * vars->ref  [1] -             // Skip index 0 for T[] coefficients
                (double)pars->rst.t [2] * vars->ref  [2] -
                (double)pars->rst.t [3] * vars->ref  [3] -
                (double)pars->rst.t [4] * vars->ref  [4] -
                (double)pars->rst.t [5] * vars->ref  [5] -
                (double)pars->rst.t [6] * vars->ref  [6] -
                (double)pars->rst.t [7] * vars->ref  [7] -
                (double)pars->rst.t [8] * vars->ref  [8] -
                (double)pars->rst.t [9] * vars->ref  [9] -
                (double)pars->rst.t[10] * vars->ref [10] -
                (double)pars->rst.t[11] * vars->ref [11] -
                (double)pars->rst.t[12] * vars->ref [12] -
                (double)pars->rst.t[13] * vars->ref [13] -
                (double)pars->rst.t[14] * vars->ref [14] -
                (double)pars->rst.t[15] * vars->ref [15]) * pars->inv_corrected_t0;
    }
    else
    {
        ref = 0.0;
    }

    return(vars->ref[0] = ref);
}
/*---------------------------------------------------------------------------------------------------------*/
float regRstHistory(struct reg_rst_vars *vars)
/*---------------------------------------------------------------------------------------------------------*\
  This function must be called after calling regRstCalcAct() to shift the RST history.  It returns the
  average actuation value over the length of the RST history.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t	i;
    uint32_t	j;
    float       sum_act = vars->act[REG_N_RST_COEFFS-1];

    for(i = REG_N_RST_COEFFS-2, j = REG_N_RST_COEFFS-1 ; j ; i--,j--)
    {
	vars->ref [j] = vars->ref [i];
	vars->meas[j] = vars->meas[i];
	vars->act [j] = vars->act [i];

        sum_act += vars->act [j];
    }

    return(sum_act * (1.0 / REG_N_RST_COEFFS));
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rst.c
\*---------------------------------------------------------------------------------------------------------*/
