/*---------------------------------------------------------------------------------------------------------*\
  File:     sim.c                                                                       Copyright CERN 2011

  License:  This file is part of libreg.

            libreg is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Purpose:  Voltage source and load simulation functions

  Authors:  Quentin King
            Martin Veenstra
            Hugues Thiesen
            Pierre Dejoue
\*---------------------------------------------------------------------------------------------------------*/

#include "libreg1.h"

#include <stdio.h>

/*---------------------------------------------------------------------------------------------------------*/
void regSimLoadInit(struct reg_load_pars *load, struct reg_sim_load_vars *sim_load_vars, float sim_period)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the load simulation.  The load structure must first be initialised with
  regLoadInit().
\*---------------------------------------------------------------------------------------------------------*/
{
    // Derive ratio of the simulation period and the time constant of the load's primary pole.
    // If this is greater than 3.0 then the load is considered to be undersampled and ohms law will be used
    // when simulating the load.

    load->sim.period_tc_ratio        = sim_period / load->tc;
    load->sim.load_undersampled_flag = (load->sim.period_tc_ratio > 3.0);

    // Reset simulation

    sim_load_vars->voltage      = 0.0;
    sim_load_vars->current      = 0.0;
    sim_load_vars->field        = 0.0;
    sim_load_vars->mag_current  = 0.0;
    sim_load_vars->integrator   = 0.0;
    sim_load_vars->compensation = 0.0;
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimLoadSetField(struct reg_load_pars *load, struct reg_sim_load_vars *vars, float b_init)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the load simulation with the field b_init.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(regSimLoadSetCurrent(load, vars, regLoadFieldToCurrent(load, b_init)));
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimLoadSetCurrent(struct reg_load_pars *load, struct reg_sim_load_vars *vars, float i_init)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the load simulation with the current i_init.
\*---------------------------------------------------------------------------------------------------------*/
{
    vars->voltage = i_init / load->gain3;

    if(load->sim.load_undersampled_flag == 0)
    {
        vars->integrator = vars->voltage * load->gain1;
    }

    regSimLoad(load, vars, vars->voltage);

    return(vars->voltage);
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimLoadSetVoltage(struct reg_load_pars *load, struct reg_sim_load_vars *vars, float v_init)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the load simulation with the voltage v_init provided the load is not resistive.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(load->sim.load_undersampled_flag == 0)
    {
        vars->integrator = v_init * load->gain1;
	vars->voltage    = v_init;
    }

    regSimLoad(load, vars, v_init);

    return(v_init);
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimLoad(struct reg_load_pars *load, struct reg_sim_load_vars *vars, float voltage)
/*---------------------------------------------------------------------------------------------------------*\
  This function simulates the current in the load in response to the specified voltage.  The algorithm
  depends upon whether the voltage source simulation and the load are undersampled.
  
  The computation of the integrator (vars->integrator) makes use of the Kahan Summation Algorithm which
  largely improves the precision on the sum, especially in that specific function where the increment is
  often very small compared to the integrated sum.

  IMPLEMENTATION NOTES: On C32 DSP, where there is no native support for 64-bit floating point arithmetic,
  the use of a compensated summation (like Kahan) is necessary for the accuracy of the load simulation with
  32-bit floating-point. However, the C32 has an internal ALU with extended 40-bit floating point arithmetic,
  and in some conditions the compiler will use the extended precision for all intermediate results. In the
  case of a Kahan summation, that extended precision would actually break the algorithm and result in a
  precision no better than a naive summation. The reason for that is if the new integrator value is stored
  in a register with 40-bit precision, then the compensation:

      compensation = (integrator - previous_integrator) - increment ~ 0

  Will be flawed and result in a negligible compensation value, different from the value obtained if the
  integrator is stored with 32-bit precision. The implementation below stores the new value of the integrator
  in the global variable vars->integrator BEFORE calculating the compensation, and that is sufficient for the
  C32 compiler to lower the precision of the integrator to 32 bits. Furthermore, vars->integrator is defined
  as a volatile variable to prevent compiler optimisations on all platforms.
\*---------------------------------------------------------------------------------------------------------*/
{
    float int_gain;
    float increment;
    float prev_integrator;

    // When load is not undersampled the inductive transients are modelled with an integrator

    if(load->sim.load_undersampled_flag == 0)
    {
        int_gain = load->sim.period_tc_ratio / regLoadCalcSatFactor(load,vars->mag_current);

        // If voltage source simulation is not undersampled use first-order interpolation of voltage

        if(load->sim.vs_undersampled_flag == 0)
        {
            increment = int_gain * (load->gain1 * 0.5 * (voltage + vars->voltage) - vars->integrator);
        }
        else // else when voltage source simulation is undersampled use final voltage for complete sample
        {
            increment = int_gain * (load->gain1 * voltage - vars->integrator);
        }

        // Computation of the integrator using Kahan Summation

        increment         -= vars->compensation;
        prev_integrator    = vars->integrator;
        vars->integrator   = prev_integrator + increment;
        vars->compensation = (vars->integrator - prev_integrator) - increment;  // Algebraically 0, in fact holds the
                                                                                // floating-point error compensation

        vars->current      = vars->integrator + load->gain0 * voltage;
        vars->mag_current  = vars->integrator * load->gain2;
    }
    else // else when load is undersampled the inductive transients are ignored and ohms law is used
    {
        vars->mag_current = vars->current = voltage * load->gain3;
    }

    // Remember voltage for next iteration

    vars->voltage = voltage;

    // Simulate magnet field based on magnet current

    vars->field   = regLoadCurrentToField(load, vars->mag_current);

    return(vars->current);
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimVsInitGain(struct reg_sim_vs_pars *pars, struct reg_load_pars *sim_load)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the gain and the bandwidth of the simulated voltage source.  If the simulation
  is undersampled it sets a flag in the sim_load structure so that the load simulation can be more accurate.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t        i;
    float           sum_num  = 0.0;         // Sum(b)
    float           sum_den  = 0.0;         // Sum(a)

    // Calculate gain of voltage source model

    for(i = 0 ; i < REG_N_VS_SIM_COEFFS ; i++)
    {
        sum_num += pars->num[i];
        sum_den += pars->den[i];
    }
    // Protect gain against NaN if the denominator is zero

    if(sum_den != 0.0)
    {
       pars->gain = sum_num / sum_den;

        // Mark voltage source simulation as undersampled if step response reaches 95% in 1 period

       sim_load->sim.vs_undersampled_flag = (pars->num[0] / pars->den[0]) >= 0.95;
    }
    else
    {
        pars->gain = 1.0;
    }

    return(pars->gain);
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimVsInitHistory(struct reg_sim_vs_pars *pars, struct reg_sim_vs_vars *vars, float v_load)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the voltage source simulation history to be in steady-state with the given
  v_load value.  The function returns the corresponding steady state v_ref.  Note that the gain must
  be calculated before calling this function using regSimVsInitGain().
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t        idx;
    float           v_ref;

    // Initialise history arrays for v_ref and v_load

    v_ref = v_load / pars->gain;

    for(idx = 0 ; idx < REG_N_VS_SIM_COEFFS ; idx++)
    {
        vars->v_ref [idx] = v_ref;
        vars->v_load[idx] = v_load;
    }

    return(v_ref);
}
/*---------------------------------------------------------------------------------------------------------*/
float regSimVs(struct reg_sim_vs_pars *pars, struct reg_sim_vs_vars *vars, float v_ref)
/*---------------------------------------------------------------------------------------------------------*\
  This function simulates the voltage source in response to the specified voltage reference.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t	i;
    uint32_t	j;
    float       v_load;

    // Shift history of input and output

    for(i = REG_N_VS_SIM_COEFFS-2, j = REG_N_VS_SIM_COEFFS-1 ; j ; i--,j--)
    {
	vars->v_ref [j] = vars->v_ref [i];
	vars->v_load[j] = vars->v_load[i];
    }

    vars->v_ref[0] = v_ref;

    v_load = pars->num[0] * v_ref;

    for(i = 1 ; i < REG_N_VS_SIM_COEFFS ; i++)
    {
        v_load += pars->num[i] * vars->v_ref[i] - pars->den[i] * vars->v_load[i];
    }

    if(pars->den[0] != 0.0)     // Protect against divide by zero
    {
        v_load /= pars->den[0];
    }

    vars->v_load[0] = v_load;

    return(v_load);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sim.c
\*---------------------------------------------------------------------------------------------------------*/

