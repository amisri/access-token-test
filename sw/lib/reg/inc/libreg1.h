/*---------------------------------------------------------------------------------------------------------*\
  File:     libreg1.h                                                                    Copyright CERN 2011

  License:  This file is part of libreg.

            libreg is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Purpose:  Converter Control Regulation library header file

  Contact:  cclibs-devs@cern.ch

  Notes:    The regulation library provides support for:

                1. Field, Current and voltage limits
                2. RST based regulation (Landau notation)
                3. Regulation error calculation
                4. Voltage source simulation
                5. Magnet load definition and simulation
                6. Signal delay

            The limits support includes three types of limits relevant to
            power converter controls:

                1. Field/Current measurement limits (trip)
                2. Field/Current reference limits (clip)
                3. Voltage reference limits (clip)

            The RST algorithm is implemented based on Landau notation:

                ACTUATION x S = REFERENCE x T - MEASUREMENT x R

            The other common notation swaps the R and S polynomials.

\*---------------------------------------------------------------------------------------------------------*/

#ifndef LIBREG_H
#define LIBREG_H

#include <stdint.h>
#include <math.h>

// Constants

// REG_N_RST_COEFFS: Maximum number of RST coefficients. That number is hardcoded in several functions, which
// must be reworked when changing the number of RST coefficients: regRstInit(), regRstCalcAct(), regRstCalcRef().

#define REG_N_RST_COEFFS        16

#define REG_N_VS_SIM_COEFFS     4                       // Number of Voltage Source simulation coefficients
#define REG_LIM_CLIP            0.001                   // Clip limit shift factor
#define REG_LIM_TRIP            0.01                    // Trip limit shift factor
#define REG_LIM_HYSTERESIS      0.1                     // Low/Zero limit hysteresis factor
#define REG_LIM_V_DIODE         -0.6                    // Diode voltage for unipolar flag
#define REG_LIM_FP32_MARGIN     2.0E-07                 // Margin on the relative precision of 32-bit floats

// Signal delay structure

struct reg_delay
{
    uint32_t            undersampled_flag;              // Signal is undersampled flag
    int32_t             iteration_counter;              // Iteration counter
    int32_t             buf_index;                      // Index into circular buffer
    int32_t             delay_int;                      // Integer delay in iteration periods
    float               delay_frac;                     // Fractional delay in iteration periods
    float               prev_signal;                    // Signal from previous iteration
    float               *buf;                           // Pointer to circular buffer for signal
};

// Regulation error structure (Error between unclipped reference and measurement)

struct reg_err
{
    struct reg_delay    delay;                          // Signal delay structure
    uint32_t            warning_filter_counter;         // Warning level filter counter
    uint32_t            fault_filter_counter;           // Fault level filter counter
    uint32_t            counter_limit;                  // Warning/fault counter limit
    uint32_t            iteration_counter;              // Iteration counter for filtering
    uint32_t            filter_period_iters;            // Filter accumulation period
    float               warning_limit;                  // Absolute error warning limit
    float               fault_limit;                    // Absolute error fault limit
    float               err;                            // Regulation error
    float               err_accumulator;                // Regulation error accumulator
    float               max_abs_err;                    // Max absolute error

    struct
    {
        uint32_t        warning;                        // Warning level flag
        uint32_t        fault;                          // Fault level flag
    } flags;
};

// Limits structures

struct reg_lim_meas                                             // Measurement limits
{
    uint32_t                    invert_limits;                  // Invert the limits before use flag (e.g. polarity switch is negative)
    float                       pos_trip;                       // Positive measurement trip limit
    float                       neg_trip;                       // Negative measurement trip limit
    float                       low;                            // Low measurement threshold
    float                       zero;                           // Zero measurement threshold
    float                       low_hysteresis;                 // Low measurement threshold with hysteresis
    float                       zero_hysteresis;                // Zero measurement threshold with hysteresis

    struct                                                      // Measurement limit flags
    {
        uint32_t                trip;                           // Measurement exceeds pos or neg trip limits
        uint32_t                low;                            // Absolute measurement is below low threshold
        uint32_t                zero;                           // Absolute measurement is below zero threshold
    } flags;
};

struct reg_lim_ref                                              // Reference limits
{
    uint32_t                    invert_limits;                  // Invert the limits before use flag (e.g. polarity switch is negative)
    float                       max_clip;                       // Max ref clip limit from max_clip_user or Q41 limit
    float                       min_clip;                       // Min ref clip limit from min_clip_user or Q41 limit
    float                       rate_clip;                      // Abs ref rate clip limit

    float                       max_clip_user;                  // Maximum reference clip limit from user
    float                       min_clip_user;                  // Minimum reference clip limit from user

    float                       i_quadrants41_max;              // i_quadrants41[1] or -1.0E10 if no Q41 limits
    float                       v0;                             // Voltage limit for i_meas = 0
    float                       dvdi;                           // Voltage limit slope with i_meas

    struct                                                      // Reference limit flags
    {
        uint32_t                unipolar;                       // Unipolar flag
        uint32_t                clip;                           // Ref is being clipped to [max_clip,min_clip] range
        uint32_t                rate;                           // Ref rate is being clipped to +/-rate_clip range
    } flags;
};

// Load structure

struct reg_load_pars
{
    float               ohms_ser;                       // Load series resistance
    float               ohms_par;                       // Load parallel resistance
    float               ohms_mag;                       // Load magnet resistance
    float               henrys;                         // Load inductance
    float               henrys_sat;                     // Load inductance value accounting for saturation
    float               inv_henrys;                     // 1.0 / henrys clipped to 1.0E+20 to avoid infinities
    float               gauss_per_amp;                  // Field to current ratio for the magnet
    float               ohms;                           // Composite resistance (based on ohms_ser & ohms_par)
    float               tc;                             // Time constant for primary pole
    float               gain0;                          // Load gain 0
    float               gain1;                          // Load gain 1
    float               gain2;                          // Load gain 2
    float               gain3;                          // Load gain 3 (steady state gain)
    float               gain10;                         // Load gain 1/Load gain 0 - Rp significance factor
                                                        // if gain10 > ~10 then Rp is not significant
    struct reg_load_sat
    {
        float           i_start;                        // Current measurement at start of saturation
        float           i_end;                          // Current measurement at end of saturation
        float           i_delta;                        // i_sat_end - i_sat_start
        float           b_end;                          // Field at i_sat_end
        float           b_factor;                       // Parabolic factor for i_sat_start < i < i_sat_end
        float           l_rate;                         // Inductance droop rate factor (/A)
        float           l_clip;                         // Clip limit for saturation factor
    } sat;

    struct reg_load_sim
    {
        float           period_tc_ratio;                // Simulation period / load time constant
        uint32_t        load_undersampled_flag;         // Simulated load is undersampled flag
        uint32_t        vs_undersampled_flag;           // Simulated voltage source is undersampled flag
    } sim;
};

// Regulation RST algorithm structures

enum reg_status                                         // Regulation status
{
    REG_OK,
    REG_WARNING,
    REG_FAULT
};

struct reg_rst                                          // RST polynomial arrays and track delay
{
    float               track_delay;                    // Track delay
    float               r   [REG_N_RST_COEFFS];	        // R polynomial coefficients (meas)
    float               s   [REG_N_RST_COEFFS];	        // S polynomial coefficients (act)
    float               t   [REG_N_RST_COEFFS];	        // T polynomial coefficients (ref)
};

struct reg_rst_pars                                     // RST algorithm parameters
{
    enum reg_status     status;                         // Regulation parameters status
    uint32_t            period_iters;                   // Regulation period (in iterations)
    float               period;                         // Regulation period
    float               freq;                           // Regulation frequency
    float               inv_s0;                         // Store 1/S[0]
    float               t0_correction;                  // Correction to t[0] for rounding errors
    float               inv_corrected_t0;               // Store 1/(T[0]+ t0_correction)
    struct reg_rst      rst;                            // RST polynomials
};

struct reg_rst_vars                                     // RST algorithm variables
{
    float               ref [REG_N_RST_COEFFS];         // RST calculated reference history
    float               meas[REG_N_RST_COEFFS];         // RST measurement history
    float               act [REG_N_RST_COEFFS];         // RST actuation history
};

// Simulation structures

struct reg_sim_load_vars                                // Load simulation variables
{
    float               voltage;                        // Load voltage from voltage source simulation
    float               field;                          // Simulated magnet field
    float               current;                        // Simulated circuit current
    float               mag_current;                    // Simulated magnet current
    float volatile      integrator;                     // Integrator for simulated current (see comments in regSimLoad)
    float               compensation;                   // Compensation for Kahan Summation
};

struct reg_sim_vs_pars                                  // Voltage source simulation parameters
{                                                       // Note: the order of fields is significant in fgtest
    float               num  [REG_N_VS_SIM_COEFFS];     // Numerator coefficients b0, b1, b2, ...
    float               den  [REG_N_VS_SIM_COEFFS];     // Denominator coefficients a0, a2, a2, ...
    float               gain;                           // Gain = Sum(den)/Sum(num)
};

struct reg_sim_vs_vars                                  // Voltage source simulation variables
{
    float               v_ref [REG_N_VS_SIM_COEFFS];    // Voltage reference history
    float               v_load[REG_N_VS_SIM_COEFFS];    // Simulated load voltage history
};

// Global power converter regulation structure

enum reg_mode                                           // Converter regulation mode
{
    REG_NONE,                                           // No regulation mode set
    REG_VOLTAGE,                                        // Open loop (voltage reference)
    REG_CURRENT,                                        // Closed loop on current
    REG_FIELD                                           // Closed loop on field
};

struct reg_converter                                    // Global converter regulation structure
{
    enum reg_mode               mode;                   // Field, current or voltage regulation

    float                       iter_period;            // Iteration period
    float                       cl_period;              // Closed loop regulation period
    uint32_t                    cl_period_iters;        // Closed loop regulation period (in iterations)
    uint32_t                    iteration_counter;      // Iteration counter

    // Measurement values - real or simulated

    float                       v_meas;                 // Voltage measurement
    float                       i_meas;                 // Current measurement
    float                       b_meas;                 // Field measurement

    // Reference and regulation variables

    float                       ref;                    // Field or current reference
    float                       ref_limited;            // Field or current reference after limits
    float                       v_ref;                  // Voltage reference before saturation or limits
    float                       v_ref_sat;              // Voltage reference after saturation compensation
    float                       v_ref_limited;          // Voltage reference after saturation and limits
    float                       f_ref;                  // Firing reference
    float                       f_ref_limited;          // Firing reference after limits
    float                       err;                    // Regulation error (field or current)
    float                       max_abs_err;            // Max absolute regulation error (field or current)

    // Measurement and reference limits

    struct reg_lim_meas         lim_i_meas;             // Current measurement limits
    struct reg_lim_meas         lim_b_meas;             // Field measurement limits

    struct reg_lim_ref          lim_v_ref;              // Voltage reference limits
    struct reg_lim_ref          lim_i_ref;              // Current reference limits
    struct reg_lim_ref          lim_b_ref;              // Field reference limits

    struct                                              // Reference (field, current or voltage) limit flags
    {
        uint32_t        ref_clip;                       // Ref is being clipped
        uint32_t        ref_rate;                       // Ref rate is being clipped
    } flags;

    // Regulation variables structures

    struct reg_rst_vars         rst_vars;               // Field or current regulation RST variables
    struct reg_rst_vars         v_rst_vars;             // Voltage regulation RST variables

    struct reg_err              v_err;                  // Voltage regulation error
    struct reg_err              i_err;                  // Current regulation error
    struct reg_err              b_err;                  // Field regulation error

    // Simulation variables structures

    struct reg_sim_vs_vars      sim_vs_vars;            // Voltage source simulation variables
    struct reg_sim_load_vars    sim_load_vars;          // Load simulation variables
    float                       sim_v_load;             // Simulated load voltage

    struct reg_delay            v_meas_delay;           // Voltage measurement delay parameters
    struct reg_delay            i_meas_delay;           // Current measurement delay parameters
    struct reg_delay            b_meas_delay;           // Field measurement delay parameters
};

// The parameter structures that are time consuming to calculate are included in the following structure
// which can be double buffered if required to avoid race conditions when regulating/simulating in real-time

struct reg_converter_pars                               // Global converter regulation parameters structure
{
    struct reg_rst_pars         v_rst_pars;             // Voltage regulation RST parameters
    struct reg_rst_pars         i_rst_pars;             // Current regulation RST parameters
    struct reg_rst_pars         b_rst_pars;             // Field regulation RST parameters
    struct reg_sim_vs_pars      sim_vs_pars;            // Voltage source simulation parameters
    struct reg_load_pars        load;                   // Circuit load model for regulation
    struct reg_load_pars        sim_load;               // Circuit load model for simulation
};

#ifdef __cplusplus
extern "C" {
#endif

// Signal delay functions

void     regDelayInitPars       (struct reg_delay *delay, float *buf,
                                 float delay_in_iters, uint32_t undersampled_flag);
void     regDelayInitVars       (struct reg_delay *delay, float initial_signal);
uint32_t regDelayCalc           (struct reg_delay *delay, float signal, float *delayed_signal);

// Regulation error functions

void     regErrInitLimits       (struct reg_err *err, float err_warning_limit, float err_fault_limit);
void     regErrInitDelay        (struct reg_err *err, float *buf, float delay_in_iters,
                                 uint32_t undersampled_flag);
void     regErrInitFilterPeriod (struct reg_err *err, uint32_t filter_period_iters);
void     regErrInitVars         (struct reg_err *err);
float    regErrCalc             (struct reg_err *err, uint32_t enable_max_abs_err, float ref, float meas);

// Limits functions

void     regLimMeasInit         (struct reg_lim_meas *lim_meas, float pos_lim, float neg_lim,
                                 float low_lim, float zero_lim, uint32_t invert_limits);
void     regLimMeas             (struct reg_lim_meas *lim_meas, float meas);
void     regLimRefInit          (struct reg_lim_ref *lim_ref, float pos_lim, float neg_lim, float rate_lim,
                                 uint32_t invert_limits);
void     regLimVrefInit         (struct reg_lim_ref *lim_v_ref, float pos_lim, float neg_lim, float rate_lim,
                                 float i_quadrants41[2], float v_quadrants41[2], uint32_t invert_limits);
void     regLimVrefCalc         (struct reg_lim_ref *lim_v_ref, float i_meas);
float    regLimRef              (struct reg_lim_ref *lim_ref, float period, float ref, float prev_ref);

// Load functions

void     regLoadInit            (struct reg_load_pars *load, float ohms_ser, float ohms_par, float ohms_mag,
                                 float henrys, float gauss_per_amp);
float    regLoadCurrentToField  (struct reg_load_pars *load, float i_meas);
float    regLoadFieldToCurrent  (struct reg_load_pars *load, float b_meas);
void     regLoadInitSat         (struct reg_load_pars *load, float henrys_sat, float i_sat_start, float i_sat_end);
float    regLoadVrefSat         (struct reg_load_pars *load, float i_meas, float v_ref);
float    regLoadInverseVrefSat  (struct reg_load_pars *load, float i_meas, float v_ref_sat);
float    regLoadCalcSatFactor   (struct reg_load_pars *load, float i_meas);

// RST regulation functions

uint32_t regRstInit             (struct reg_rst_pars *pars, float iter_period, uint32_t period_iters,
                                 struct reg_load_pars *load, float clbw, float clbw2, float z,
                                 float pure_delay, struct reg_rst *manual);
float    regRstCalcAct          (struct reg_rst_pars *pars, struct reg_rst_vars *vars, float ref, float meas);
float    regRstCalcRef          (struct reg_rst_pars *pars, struct reg_rst_vars *vars, float act, float meas);
float    regRstHistory          (struct reg_rst_vars *vars);

// Simulation functions

void     regSimLoadInit         (struct reg_load_pars *load, struct reg_sim_load_vars *sim_load_vars,
                                 float sim_period);
float    regSimLoadSetField     (struct reg_load_pars *load, struct reg_sim_load_vars *vars, float b_init);
float    regSimLoadSetCurrent   (struct reg_load_pars *load, struct reg_sim_load_vars *vars, float i_init);
float    regSimLoadSetVoltage   (struct reg_load_pars *load, struct reg_sim_load_vars *vars, float v_init);
float    regSimLoad             (struct reg_load_pars *load, struct reg_sim_load_vars *vars, float voltage);
float    regSimVsInitGain       (struct reg_sim_vs_pars *pars, struct reg_load_pars   *sim_load);
float    regSimVsInitHistory    (struct reg_sim_vs_pars *pars, struct reg_sim_vs_vars *vars, float v_ref);
float    regSimVs               (struct reg_sim_vs_pars *pars, struct reg_sim_vs_vars *vars, float v_ref);

// Converter regulation functions

void     regSetMeas             (struct reg_converter *reg,  struct reg_converter_pars *reg_pars,
                                 float v_meas, float i_meas, float b_meas, uint32_t sim_meas_control);
void     regSetVoltageMode      (struct reg_converter *reg, struct reg_converter_pars *reg_pars);
void     regSetMode             (struct reg_converter *reg, struct reg_converter_pars *reg_pars,
                                 enum reg_mode mode, float meas, float rate);
uint32_t regConverter           (struct reg_converter *reg, struct reg_converter_pars *reg_pars, float ref,
                                 float feedforward_v_ref, uint32_t feedforward_control,
                                 uint32_t max_abs_err_control);
void     regSimulate            (struct reg_converter *reg, struct reg_converter_pars *reg_pars,
                                 float v_perturbation);

#ifdef __cplusplus
}
#endif

// inline function definitions

static inline void regLimRefSetInvertLimits(struct reg_lim_ref *lim_ref, uint32_t invert_limits)
{
    lim_ref->invert_limits = invert_limits;
}

#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: libreg1.h
\*---------------------------------------------------------------------------------------------------------*/
