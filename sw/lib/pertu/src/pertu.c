/*
 *  Filename: pertu.c
 *
 *  Purpose:  Functions for reading Converteam pertu files
 *
 *  Author:   Stephen Page
 */

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define PERTU

#include "pertu.h"
#include "pertu/def_per.h"

////////////////////////////////////////////////////////////////////////////////

// Read a pertu file

uint32_t pertu_read_file(char *filename, struct pertu_data *data)
{
    FILE        *file;
    S_DEF_PER   file_head;
    uint32_t    i;

    // Open pertu file

    if(!(file = fopen(filename, "r")))
    {
        fprintf(stderr, "Failed to open %s: ", filename);
        perror(NULL);
        return(1);
    }

    // Read pertu file header

    if(!fread(&file_head, sizeof(file_head), 1, file))
    {
        fclose(file);
        fprintf(stderr, "Failed to read header of file %s\n", filename);
        return(1);
    }

    // Set meta data

    data->num_samples   = file_head.nbrec;
    data->num_signals   = file_head.nbvar;
    data->period_ms     = file_head.per / 100000;

    // Allocate memory for signal names

    if(!(data->signal_names = malloc(PERTU_SIG_NAME_SIZE * data->num_signals)))
    {
        fclose(file);
        fprintf(stderr, "Failed to allocate memory for signal names in file %s\n", filename);
        return(1);
    }

    // Allocate memory for signal types

    if(!(data->signal_types = malloc(sizeof(*data->signal_types) * data->num_signals)))
    {
        free(data->signal_names);
        fclose(file);
        fprintf(stderr, "Failed to allocate memory for signal types in file %s\n", filename);
        return(1);
    }

    // Allocate memory for signal data

    if(!(data->signal_data = malloc(PERTU_SAMPLE_SIZE * data->num_signals * data->num_samples)))
    {
        free(data->signal_names);
        free(data->signal_types);
        fclose(file);
        fprintf(stderr, "Failed to allocate memory for signals in file %s\n", filename);
        return(1);
    }

    // Zero start time

    data->start_time.tv_sec  = 0;
    data->start_time.tv_usec = 0;

    // Read each signal

    for(i = 0 ; i < data->num_signals ; i++)
    {
        if(pertu_read_signal(filename, file, data, i))
        {
            free(data->signal_names);
            free(data->signal_types);
            free(data->signal_data);
            fclose(file);
            return(1);
        }
    }
    fclose(file);

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Free pertu data

void pertu_free_data(struct pertu_data *data)
{
    free(data->signal_names);
    free(data->signal_types);
    free(data->signal_data);
}

////////////////////////////////////////////////////////////////////////////////

// Read a signal from a pertu file

static uint32_t pertu_read_signal(char *filename, FILE *file, struct pertu_data *data, uint32_t signal_idx)
{
    uint32_t        i;
    uint8_t         *sample;
    uint8_t         *signal_buf = &data->signal_data[PERTU_SAMPLE_SIZE * signal_idx * data->num_samples];
    S_DEF_PER_REC   signal_head;

    // Read signal header

    if(!fread(&signal_head, sizeof(signal_head), 1, file))
    {
        fprintf(stderr, "Failed to read signal header in file %s\n", filename);
        return(1);
    }

    // Store signal type

    data->signal_types[signal_idx] = signal_head.type;

    // Store signal name

    snprintf(data->signal_names[signal_idx], PERTU_SIG_NAME_SIZE, "%s.%s.%s",
             signal_head.mnemoK, signal_head.mnemoKp, signal_head.mnemo);

    // Read samples

    for(i = 0 ; i < data->num_samples ; i++)
    {
        sample = &signal_buf[PERTU_SAMPLE_SIZE * i];

        // Read sample from file

        if(!fread(sample, PERTU_SAMPLE_SIZE, 1, file))
        {
            fprintf(stderr, "Failed to read sample from file %s\n", filename);
            return(1);
        }

        // If this is the first sample, check whether signal is a timestamp

        if(i == 0)
        {
            if(!strcmp(data->signal_names[signal_idx], "Ps.ValCtrl.Time"))
            {
                data->start_time.tv_sec = *(int32_t *)sample;
            }
            else if(!strcmp(data->signal_names[signal_idx], "Ps.ValCtrl.Time_ms"))
            {
                data->start_time.tv_usec = 1000 * *(int32_t *)sample;
            }
        }
    }
    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// EOF
