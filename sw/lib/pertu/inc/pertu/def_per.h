/* --------------------------------------------------------------- */
/* Fichier def_per.h          Structure d�finition d'une pertu     */
/* M.BEGEY                             2 novembre 1998             */
/* --------------------------------------------------------------- */
#ifndef	def_per_h
#define	def_per_h

#include <time.h>

typedef int BOOL;

/* Definition du type de cible ----------------------------------- */
/*#define	MT80	1		                                       */
#define	HPC0	1 

#define	DEF_PER_MAX_COLOR	12
extern	long def_per_color[] ;

/* Structures pour la navigation dans les fichiers historiques --  */
typedef struct
	{
	time_t	time_to_search ;
	time_t	file_duration ;
	BOOL	must_adjust_position ;
	int		status ;
	} S_NAVIGATE ;

/* Structure pour les connexions 'lentes'  ----------------------  */
typedef	struct
{
	char	node[16] ;
	int		cpu ;
	char	appli[16] ;
	int		sd ;
} S_CHANNEL_ACQ ;

typedef	struct		/* Structure remarque (MBY le 1/11/2000)       */
	{
	char	remarque[40] ;	/* Remarque positionnable sur �cran    */
	float	rem_x ;			/* Position remarque                   */
	float	rem_y ;			/* Position remarque                   */
	long	rem_coul ;		/* Couleur remarque                    */
	} S_PERTU_REM ;
#define	PERTU_MAX_REMARQUE	64	/* Nombre maxi de remarques */

typedef	struct		/* Structure configuration (MBY le 26/2/2001)  */
	{
	char	remarque[64] ;	/* Libell� de la configuration    */
	} S_CONFIG ;
#define	PERTU_MAX_CONFIG	32	/* Nombre maxi de config */

typedef	struct
	{
	long	rem_x ;			/* Position remarque                   */
	long	rem_y ;			/* Position remarque                   */
	long	rem_coul ;		/* Couleur remarque                    */
	} OLD_REM ;
typedef	struct
	{
	float	mini ;
	float	maxi ;
	long	reserve ;
	} SCALE_LIMIT ;
typedef	struct
	{
	long	mini ;
	long	maxi ;
	long	reserve ;
	} SCALE_LIMIT_L ;

typedef	struct			/* Definition d'une courbe (version 1 et 2)*/
{
	char	node[16] ;	/* Nom de la cible                         */
	long	cpu ;		/* Num�ro CPU (pour 80MT)                  */
	char	appli[16] ;	/* Nom de l'application                    */
	char	mnemo[16] ;	/* Mn�monique de la variable               */
	long	addr ;		/* Adresse de la variable                  */
	long	couleur ;	/* Couleur affichage de la courbe          */
	long	enreg ;		/* Valeurs correctement enregistr�es       */
	long	valide ;	/* Courbe visible                          */
	long	type ;		/* Type de la variable                     */
	long	index ;		/* Index de l'�chantillon le plus ancien   */
						/* (buffer tournant)                       */
	char	remarque[40] ;	/* Remarque positionnable sur �cran    */
	union
		{
		OLD_REM		old_rem ;
		SCALE_LIMIT	scale_limit ;
		} u1 ;
	long	num_rec ;		/* Num�ro d'enregistrement (cible)     */
	long	scale_auto ;	/* Mise � l'�chelle automatique        */
	float	offset ;		/* Offset individuel pour trac�        */
	float	ratio ;			/* Grossissement individuel            */
	char	mnemoK[12] ;		/* MBY le 3/1/2000	*/
	char	mnemoKp[12] ;		/* MBY le 3/1/2000	*/
	short	num_pert ;		/* indice pertu (MBY le 11/10/2000)    */
	short	num_task ;		/* Num�ro de la t�che (MBY le 11/10/00)*/
	long	periode ;		/* Periode de tache (MBY le 11/10/00)  */
	short	mother_index ;	/* Index dans pertu m�re               */
	short	old_index ;		/* Index prec jointure                 */
	short	daughter    ;	/* Num�ro de pertu 'fille'             */
	short	reserve ;
	float	first_rec ;	/* Table des valeurs                       */
}S_DEF_PER_REC_12 ;

#pragma pack(push, 1)
typedef	struct
{
	char	node[16] ;	/* Nom de la cible                         */
	long	order ;		/* Ancien num�ro CPU = index (ordre)       */
	char	appli[16] ;	/* Nom de l'application                    */
/*3*/char	mnemo[64] ;	/* Mn�monique KKS de la variable               */
	long	addr ;		/* Adresse de la variable                  */
	long	couleur ;	/* Couleur affichage de la courbe          */
	long	enreg ;		/* Valeurs correctement enregistr�es       */
	long	valide ;	/* Courbe visible                          */
	long	type ;		/* Type de la variable                     */
	long	index ;		/* Index de l'�chantillon le plus ancien   */
						/* (buffer tournant)                       */
	char	remarque[40] ;	/* Remarque positionnable sur �cran    */
	union
		{
		OLD_REM		old_rem ;
		SCALE_LIMIT	scale_limit ;
		SCALE_LIMIT_L	scale_limit_long ;
		} u1 ;
	long	num_rec ;		/* Num�ro d'enregistrement (cible)     */
	long	scale_auto ;	/* Mise � l'�chelle automatique        */
	float	offset ;		/* Offset individuel pour trac�        */
	float	ratio ;			/* Grossissement individuel            */
	char	mnemoK[12] ;		/* MBY le 3/1/2000	*/
	char	mnemoKp[12] ;		/* MBY le 3/1/2000	*/
	short	num_pert ;		/* indice pertu (MBY le 11/10/2000)    */
	short	num_task ;		/* Num�ro de la t�che (MBY le 11/10/00)*/
	long	periode ;		/* Periode de tache (MBY le 11/10/00)  */
	short	mother_index ;	/* Index dans pertu m�re               */
	short	old_index ;		/* Index prec jointure                 */
	short	daughter    ;	/* Num�ro de pertu 'fille'             */
/*3*/short   ud_ident[10] ; /* Chemin acc�s User Def               */
/*3*/short   slot ;             /* Num�ro de slot (si var interface)	*/
/*3*/char	unit[16] ;		/* Affichage des unit�s	*/
/*3*/long	recording_trigger ;	/* 1 : Recording snapshot  2 : Recording stop  4:Snapshot txt*/
/*3*/long	flag_PWM ;		/* MBY 12/6/2008 : For a special 'square' display oon LINT */
/*3*/long	reserve2[8] ;

}S_DEF_PER_REC ;    /* Cette structure est	*/
                    /* 64 - 16 = 48 octets	*/
                    /* + 20	*/
                    /* + 40 = 108 octets > S_DEF_PER_REC_12	*/
#pragma pack(pop)


#define PERTU_VIRTUAL_CHANNEL_ADDR -1	/* MBY le 6/11/2001 - Canaux virtuels
// Syntaxe de l'�quation (dans comment) :
// Une succession de champs 16 bits / 8 bits pouvant contenir les codes suivants :
// (La partie 'code' est sur 4 bits)
// <TAG_CHANNEL>xxx : Valeur d'un canal enregistr� (xxx va de 0 � 4095)
// <TAG_VALUE>xxx : Valeur 'immediate' 12 bits enti�re (xxx va de 0 � 4095)
// <TAG_SHORT_VALUE>x : Valeur 'immediate' 4 bits enti�re (x va de 0 � 15)
// <TAG_OPERATOR>y : Code 'op�rateur' (voir types support�s correspondants � y)
// <TAG_OPEN_LEVEL>y : Ouverture de parenth�se (y = niveau - de 0 � 0xf)
// <TAG_CLOSE_LEVEL>y : Fermeture de parenth�se (y = niveau - de 0 � 0xf)
// ----------------------------------------------------------------------
// Op�rateurs 8 bits */
#define TAG_SHORT_VALUE	0		/* suivi de 4 bits (valeur de 0 � 15) */
#define TAG_OPEN_LEVEL	1		/* Ouverture parenth�se (suivi de 4 bits pour niveau - de 0 � 15) */
#define TAG_CLOSE_LEVEL	2		/* Fermeture parenth�se (suivi de 4 bits pour niveau - de 0 � 15) */
#define	TAG_OPERATOR_B	3		/* Op�rateurs binaires bool�ens (entre 2 canaux) suivi de 4 bits pour pr�ciser l'op�rateur */
#define	TAG_OPERATOR_R	4		/* Op�rateurs binaires r�els (entre 2 canaux) suivi de 4 bits pour pr�ciser l'op�rateur */
/* Sous famille des op�rateurs binaires bool�ens*/
#define	OPERATOR_B_AND	0
#define	OPERATOR_B_OR	1
/* Sous famille des op�rateurs binaires r�els */
#define	OPERATOR_R_PLUS		0
#define	OPERATOR_R_MOINS	1
#define	OPERATOR_R_MUL		2
#define	OPERATOR_R_DIV		3
#define	TAG_END				5		/* Fin de l'�quation */

#define	TAG_LIMIT8		8		/* Limite (incluse) des op�rateurs 8 bits p/r aux 16 bits */

/* Op�rateurs 16 bits */
#define	TAG_DERIV		9		// Derivation added 24/8/2005
#define	TAG_INTEG		10		// Integral   added 24/8/2005

#define TAG_LIMIT		11		/* suivi de 12 bits (val limite de -2048 � +2047) */
#define TAG_MSQ_BIT		12		/* suivi de 12 bits (rang du bits - de 0 � 31) + 16 bits TAG_CHANNEL */
#define TAG_CHANNEL		13		/* suivi de 12 bits (num�ro de canal) */
#define TAG_CHANNEL_NOT	14		/* Inversion bool suivi de 12 bits (num�ro de canal) */
#define TAG_VALUE		15		/* suivi de 12 bits (valeur de 0 � 4095) */



typedef	struct			/* D�finition d'une pertu                  */
{
	long	magic ;		/* Ident structure                         */
	char	node[16] ;	/* Nom de la cible                         */
	char	appli[16] ;	/* Nom de l'application                    */
	char	filename[80] ;	/* Nom du fichier de sauvegarde        */
	char	mapname[80] ;	/* Nom du fichier DONP.map             */
	short	num_pert ;	/* indice pertu                            */
	short	num_task ;	/* Num�ro de la t�che                      */
	int		modif ;		/* Flag pertu modifi�e                     */
	long	size ;		/* Taille totale de la structure           */
	long	nbvar ;		/* Nombre de variable pertu                */
	long	nbrec ;		/* Nombre total d'enregistrement           */
	long	nbpost ;	/* Nombre voulu d'enreg. apres incident    */
	short	mode ;		/* Mode declenchement pertu                */
	short	m_auto ;	/* Rearmement manuel/automatique           */
	long	rearm_auto ;/* Scrutation ->upload + rearmement auto   */
	long	per ;		/* Periode enregistrement THL(�S)          */
	long	periode ;	/* Periode d'acquisition                   */
	long	startrec ;	/* Indice permiere valeur valide           */
	short	stat ;		/* Code erreur communication ou etat pertu */
	char	automode ;	/* Commande automatique en cours           */
	char	progress ;	/* Evolution commande en cours             */

	char	mnemo[16] ;	/* Mn�monique de la variable d�clencheur   */
	long	trigger ;	/* Adresse declencheur                     */
	long	timedec[2] ;	/* Datation d�clenchement              */
	long	record_mode ;	/* Mode d'enregistrement :             */
							/* 0->Pertu 1->Fast 2->Slow            */
	long	CheckSum ;	/* CheckSum sur table de variables         */
	int				sd ;	/* Ident de connexions        */
	S_CHANNEL_ACQ	*pt_channel ;	/* Zone dynamique              */
	long	old_index ;	/* Ancien index pour acq par fen�tre       */
	long	pid ;		/* Identifier pertu                        */
	int		maxi ;		/* Nombre maxi de fichier dans historique  */
	short	ScaleManu ;	/* Calcul de l'echelle auto(0) / manu (1)  */
	short	GradManu ;	/* Calcul graduations  auto(0) / manu (1)  */
	long	scroll ;	/* Nombre de pas � scroller � gauche       */

/*	Sauvegarde des param�tres d'affichage                          */	
	double	Echelle_x ;	/* Sauvegarde param�tres d'echelle         */
	double	Echelle_y ;
	double	Min_y ;		/* M�morisation Mini Y                     */
	double	Max_y ;		/* M�morisation Maxi Y                     */
	double	Grad_x ;	/* M�morisation graduations x              */
	double	Grad_y ;	/* M�morisation graduations y              */
	int		Origine_x ;	/* M�morisation origine x scrolling        */
	int		Origine_y ;	/* M�morisation origine y scrolling        */
	int		ModeTexte ;		/* Affichage Texte                     */
	int		AvecGraduation ;	/* Visualisation des graduations   */
	int		AfficheTemps ;	/* Affichage axe t au lieu de x        */
	int		TileLog ;	/* R�partition des bool�ens verticalement  */
	char	wp[44] ;	/* Structure WINDOWPLACEMENT */

	/*	Zone ajout�e le 22 f�vrier 1999		*/
	char	Titre[80] ;	/* Titre pertu                             */
	int		Titre_x ;			/* Position titre                  */
	int	Titre_y ;			/* Position titre                  */
	long	Titre_coul ;		/* Couleur titre                   */
	long	reserve[100] ;
	
	S_DEF_PER_REC_12	first_rec ;	 /*  Table de Rec                  */


} S_DEF_PER1 ;	/* Associ� � l'ancien format "!PER" */

typedef	struct			/* D�finition d'une pertu                  */
{
	long	magic ;		/* Ident structure "PER2"                  */
/*2*/long	old_magic ;	/* Pour conserver l'ancien format          */
	char	node[16] ;	/* Nom de la cible                         */
	char	appli[16] ;	/* Nom de l'application                    */
/*2*/char	filename[254] ;	/* Nom du fichier de sauvegarde        */
/*2*/char	mapname[254] ;	/* Nom du fichier DONP.map             */
	short	num_pert ;	/* indice pertu                            */
	short	num_task ;	/* Num�ro de la t�che                      */
	int		modif ;		/* Flag pertu modifi�e                     */
	long	size ;		/* Taille totale de la structure           */
	long	nbvar ;		/* Nombre de variable pertu                */
	long	nbrec ;		/* Nombre total d'enregistrement           */
	long	nbpost ;	/* Nombre voulu d'enreg. apres incident    */
	short	mode ;		/* Mode declenchement pertu                */
	short	m_auto ;	/* Rearmement manuel/automatique           */
	long	rearm_auto ;/* Scrutation ->upload + rearmement auto   */
	long	per ;		/* Periode enregistrement THL(�S)          */
	long	periode ;	/* Periode d'acquisition                   */
	long	startrec ;	/* Indice permiere valeur valide           */
	short	stat ;		/* Code erreur communication ou etat pertu */
	char	automode ;	/* Commande automatique en cours           */
	char	progress ;	/* Evolution commande en cours             */

	char	mnemo[16] ;	/* Mn�monique de la variable d�clencheur   */
	long	trigger ;	/* Adresse declencheur                     */
	long	timedec[2] ;	/* Datation d�clenchement              */
	long	record_mode ;	/* Mode d'enregistrement :             */
							/* 0->Pertu 1->Fast 2->Slow            */
	long	CheckSum ;	/* CheckSum sur table de variables         */
	int				sd ;	/* Ident de connexions        */
	S_CHANNEL_ACQ	*pt_channel ;	/* Zone dynamique              */
	long	old_index ;	/* Ancien index pour acq par fen�tre       */
	long	pid ;		/* Identifier pertu                        */
	int		maxi ;		/* Nombre maxi de fichier dans historique  */
	short	ScaleManu ;	/* Calcul de l'echelle auto(0) / manu (1)  */
	short	GradManu ;	/* Calcul graduations  auto(0) / manu (1)  */
	long	scroll ;	/* Nombre de pas � scroller � gauche       */

	long	reserv8 ;	/* POur alignement 8 (MBY le 25/11/00)     */
/*	Sauvegarde des param�tres d'affichage                          */	
	double	Echelle_x ;	/* Sauvegarde param�tres d'echelle         */
	double	Echelle_y ;
	double	Min_y ;		/* M�morisation Mini Y                     */
	double	Max_y ;		/* M�morisation Maxi Y                     */
	double	Grad_x ;	/* M�morisation graduations x              */
	double	Grad_y ;	/* M�morisation graduations y              */
	int		Origine_x ;	/* M�morisation origine x scrolling        */
	int		Origine_y ;	/* M�morisation origine y scrolling        */
	int		ModeTexte ;		/* Affichage Texte                     */
	int		AvecGraduation ;	/* Visualisation des graduations   */
	int		AfficheTemps ;	/* Affichage axe t au lieu de x        */
	int		TileLog ;	/* R�partition des bool�ens verticalement  */
	char	wp[44] ;	/* Structure WINDOWPLACEMENT */

	/*	Zone ajout�e le 22 f�vrier 1999		*/
	char	Titre[80] ;	/* Titre pertu                             */
	float	Titre_x ;			/* Position titre                  */
	float	Titre_y ;			/* Position titre                  */
	long	Titre_coul ;		/* Couleur titre                   */

	char	mnemoK[12] ;		/* MBY le 3/1/2000	*/
	char	mnemoKp[12] ;		/* MBY le 3/1/2000	*/
	float	ratio ;		/* Ajout� le 11/10/2000 - ratio sous-pertu */
	int		nb_rem ;	/* Nombre de remarques (MBY le 1/11/2000)  */
S_PERTU_REM *pt_rem ;	/* Pointeur sur table remarque             */
	char	m_VarInfo_Addr;
	char	m_VarInfo_Comment;
	char	m_VarInfo_Mnemo;
	char	m_VarInfo_Value;

	long	synchro ;	/* 11/11/2000 : NBre cycle de retard p/r � la pertu m�re */

	int		NbDaughter ;		/* MBY le 12/11/2000	*/
	long	DaughterPert[16] ;	/* MBY le 12/11/2000	*/
	long	compressed ;

	long	nbrec_file ;		/* MBY le 6/2/2001 : Nombre maxi de record dans le grand fichier	*/
	long	curec_file ;		/* MBY le 6/2/2001 : Index courant dans le grand fichier	*/
	long	index ;				/* Index remplissage	*/
	long	total_index ;		/* Nombre total de record	*/
	long	min_index ;			/* Pour pertu m�re : Le nombre de record compl�tements remplis	*/
	long	ref_time ;			/* index record ref time	*/

	long	CWnd ;				/* ident view (pour envoi WM_USER depuis status bar)	*/

	long	isagraf ;			/* Communication de type isagraf	*/

	long	configuration ;		/* Num�ro de config (0 � 32)	*/
	S_CONFIG *pt_config ;		/* Num�ro de config (0 � 32)	*/

	long	BkColor ;			/* Couleur du fond	*/
	long	AxeColor ;			/* Couleur des axes	*/
	long	GridColor ;			/* Couleur de la grille	*/
	long	TextColor ;			/* Couleur mode texte	*/
	long	Text2Color ;		/* Couleur mode texte (apres incident)	*/

	long	OverView ;			/* Vision condens�e	*/
	long	memo_nbrec_file ;	/* Pour sauvegarde uniquement config	*/
	long	compress ;			/* Rapport compression horizontal	*/
	long	can_be_scrolled ;	/* Verrouillage si en cours d'acquisition	*/
	long	ExitProtection ;    /* Protection de la sortie (par mot de passe = ALSTOM)	*/
	long	reserve[50] ;
	int				nb_channel ;	/* Nombre de connexions        */
	
	S_DEF_PER_REC_12	first_rec ;	 /*  Table de Rec                  */


} S_DEF_PER2 ;

#pragma pack(push, 1)
typedef	struct	
{
	long	magic ;		/* Ident structure "PER3"                  */
    long	old_magic ;	/* Pour conserver l'ancien format          */
	char	node[16] ;	/* Nom de la cible                         */
	char	appli[16] ;	/* Nom de l'application                    */
    char	filename[254] ;	/* Nom du fichier de sauvegarde        */
    char	mapname[254] ;	/* Nom du fichier DONP.map             */
	short	num_pert ;	/* indice pertu                            */
	short	num_task ;	/* Num�ro de la t�che                      */
	int		modif ;		/* Flag pertu modifi�e                     */
	long	size ;		/* Taille totale de la structure           */
	long	nbvar ;		/* Nombre de variable pertu                */
	long	nbrec ;		/* Nombre total d'enregistrement           */
	long	nbpost ;	/* Nombre voulu d'enreg. apres incident    */
	short	mode ;		/* Mode declenchement pertu                */
	short	m_auto ;	/* Rearmement manuel/automatique           */
	long	rearm_auto ;/* Scrutation ->upload + rearmement auto   */
	long	per ;		/* Periode enregistrement THL(�S)          */
	long	periode ;	/* Periode d'acquisition                   */
	long	startrec ;	/* Indice permiere valeur valide           */
	short	stat ;		/* Code erreur communication ou etat pertu */
	char	automode ;	/* Commande automatique en cours           */
	char	progress ;	/* Evolution commande en cours             */

	char	res_mnemo[16] ;	/* Ancien mn�monique de la variable d�clencheur   */
	long	trigger ;	/* Adresse declencheur                     */
	long	timedec[2] ;	/* Datation d�clenchement              */
	long	record_mode ;	/* Mode d'enregistrement :             */
							/* 0->Pertu 1->Fast 2->Slow            */
	long	CheckSum ;	/* CheckSum sur table de variables         */
	int				sd ;	/* Ident de connexions        */
	S_CHANNEL_ACQ	*pt_channel ;	/* Zone dynamique              */
	long	old_index ;	/* Ancien index pour acq par fen�tre       */
	long	pid ;		/* Identifier pertu                        */
	int		maxi ;		/* Nombre maxi de fichier dans historique  */
	short	ScaleManu ;	/* Calcul de l'echelle auto(0) / manu (1)  */
	short	GradManu ;	/* Calcul graduations  auto(0) / manu (1)  */
	long	scroll ;	/* Nombre de pas � scroller � gauche       */

	long	reserv8 ;	/* POur alignement 8 (MBY le 25/11/00)     */
/*	Sauvegarde des param�tres d'affichage                          */	
	double	Echelle_x ;	/* Sauvegarde param�tres d'echelle         */
	double	Echelle_y ;
	double	Min_y ;		/* M�morisation Mini Y                     */
	double	Max_y ;		/* M�morisation Maxi Y                     */
	double	Grad_x ;	/* M�morisation graduations x              */
	double	Grad_y ;	/* M�morisation graduations y              */
	int		Origine_x ;	/* M�morisation origine x scrolling        */
	int		Origine_y ;	/* M�morisation origine y scrolling        */
	int		ModeTexte ;		/* Affichage Texte                     */
	int		AvecGraduation ;	/* Visualisation des graduations   */
	int		AfficheTemps ;	/* Affichage axe t au lieu de x        */
	int		TileLog ;	/* R�partition des bool�ens verticalement  */
	char	wp[44] ;	/* Structure WINDOWPLACEMENT */

	/*	Zone ajout�e le 22 f�vrier 1999		*/
	char	Titre[80] ;	/* Titre pertu                             */
	float	Titre_x ;			/* Position titre                  */
	float	Titre_y ;			/* Position titre                  */
	long	Titre_coul ;		/* Couleur titre                   */

	char	mnemoK[12] ;		/* MBY le 3/1/2000	*/
	char	mnemoKp[12] ;		/* MBY le 3/1/2000	*/
	float	ratio ;		/* Ajout� le 11/10/2000 - ratio sous-pertu */
	int		nb_rem ;	/* Nombre de remarques (MBY le 1/11/2000)  */
S_PERTU_REM *pt_rem ;	/* Pointeur sur table remarque             */
	char	m_VarInfo_Addr;
	char	m_VarInfo_Comment;
	char	m_VarInfo_Mnemo;
	char	m_VarInfo_Value;

	long	synchro ;	/* 11/11/2000 : NBre cycle de retard p/r � la pertu m�re */

	int		NbDaughter ;		/* MBY le 12/11/2000	*/
	long	DaughterPert[16] ;	/* MBY le 12/11/2000	*/
	long	compressed ;

	long	nbrec_file ;		/* MBY le 6/2/2001 : Nombre maxi de record dans le grand fichier	*/
	long	curec_file ;		/* MBY le 6/2/2001 : Index courant dans le grand fichier	*/
	long	index ;				/* Index remplissage	*/
	long	total_index ;		/* Nombre total de record	*/
	long	min_index ;			/* Pour pertu m�re : Le nombre de record compl�tements remplis	*/
	long	ref_time ;			/* index record ref time	*/

	long	CWnd ;				/* ident view (pour envoi WM_USER depuis status bar)	*/

	long	isagraf ;			/* Communication de type isagraf	*/

	long	configuration ;		/* Num�ro de config (0 � 32)	*/
	S_CONFIG *pt_config ;		/* Num�ro de config (0 � 32)	*/

	long	old_coul[5] ;		/* Les couleurs sont maintenant dans les pref utilisateur	*/
/*	long	BkColor ;			// Couleur du fond	*/
/*	long	AxeColor ;			// Couleur des axes	*/
/*	long	GridColor ;			// Couleur de la grille	*/
/*	long	TextColor ;			// Couleur mode texte	*/
/*	long	Text2Color ;		// Couleur mode texte (apres incident)	*/

	long	OverView ;			/* Vision condens�e	*/
	long	memo_nbrec_file ;	/* Pour sauvegarde uniquement config	*/
	long	compress ;			/* Rapport compression horizontal	*/
	long	can_be_scrolled ;	/* Verrouillage si en cours d'acquisition	*/
	long	ExitProtection ;    /* Protection de la sortie (par mot de passe = ALSTOM)	*/
/*3*/char   mnemo[64] ;        /* Extension taille mnemonique	*/
/*3*/short  ud_ident[10] ;     /* Chemin User Def (trigger)	*/
/*3*/short  slot ;             /* Num�ro de slot (si var interface)	*/
	long	height_screen ;			/* Hauteur utile de la vue	*/
	int		nb_channel ;	/* Nombre de connexions        */
	int		ReticuleVertical ;	/* Navigation reticul vertical	*/
	int		ModeJLS ;		/* Affichage des variables dans des zones horizontales s�par�es	*/
	int		Delay ;		/* Inter frame delay	*/
	short	FFT ;		/* View in FFT Mode	*/
	short	FFT0 ;		/* View freq 0 in FFT	*/
	int		EcartReticule ;	/* 7/9/2002: Ecart r�ticule		*/
	long	DefaultRec ;	/* 14/02/2003 : Default record - for vertical scale	*/
	short	res2 ;
	short	DontCheckPeriod ;
	float	float_scroll ;	/* MBY 12/6/2003 : If scrolling less than  1 pixel */
	int		ShowOnly ;		/* MBY 16/9/2003 : Monipert downloaded file can only be show by Pertu (Not started) */

   long  VF_mode ;               /* MBY 7/7/2006 : Validate variable frequency data */
   long  VF_time_resolution ;    /* MBY 7/7/2006 : Time scale resolution in �S for variable frequency data time stamp */
   long  VF_starting_point ;     /* MBY 7/7/2006 : Variable frequency scrolling starting point */
   long	 VF_decode_pulse ;		/* MBY 10/6/2009 : Flag for pulse decoding feature */

   long	reserve[132] ;	

} S_DEF_PER ;
#pragma pack(pop)

/* ----------------------------------------------------------------------------------- */
/* Important note about variable frequency mode :                                      */
/* For compatibility purpose, data order is not changed, and starts like in other mode */
/* at first_rec in the normal order ... timestamp values are stored just after this    */
/* area                                                                                */
/* ----------------------------------------------------------------------------------- */

#define	MAGIC_OLD_PERTU1	(*(long *)"!PER") 
#define	MAGIC_OLD_PERTU2	(*(long *)"PER2") 
#define	MAGIC_PERTU		(*(long *)"PER3") 

#define	DIF_SIZE1		((254 - 80) * 2 + 4)	/* Diff�rence taille header entre !PER er PER2	*/

#define	AUTOMODE_REARM	1	/* R�armement en cours	*/
#define	AUTOMODE_WAIT	2	/* Attente trigger	*/
#define	AUTOMODE_UPLOAD	3	/* Upload en cours	*/

typedef	struct		/* Memorisation description d'une variable */
{
	long	addr ;
	char	node[16] ;
	int		cpu ;
	char	appli[16] ;
	short	num_pert ;		/* indice pertu (MBY le 12/11/2000)    */
	short	num_task ;		/* Num�ro de la t�che (MBY le 12/11/00)*/
	char	mnemo[128] ;
	char	mnemoK[12] ;		/* MBY le 3/1/2000	*/
	char	mnemoKp[12] ;		/* MBY le 3/1/2000	*/
	int		type ;
	float	ratio ;
	float	offset ;
	char	remarque[40] ;

	long	lParam ;
}S_VAR_DEF ;

#define	PERTU_NBVAR_DEFAULT		1
#define	PERTU_NBREC_DEFAULT		200
#define	PERTU_NBPOST_DEFAULT	100
#define	PERTU_MAX_TARGET	16	/* Nombre maxi de 'sous-pertu'	*/

#endif
