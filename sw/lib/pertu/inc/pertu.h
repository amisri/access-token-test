/*
 *  Filename: pertu.h
 *
 *  Purpose:  Declarations for pertu
 *
 *  Author:   Stephen Page
 */

#ifndef PERTU_H
#define PERTU_H

#ifdef PERTU
#define PERTU_EXT
#else
#define PERTU_EXT extern
#endif

#include <stdint.h>
#include <sys/time.h>

#include "pertu/def_per.h"

// Constants

#define PERTU_SAMPLE_SIZE       4
#define PERTU_SIG_NAME_SIZE     49

// Types

enum pertu_data_type
{
    PERTU_BOOL  = 66,
    PERTU_LONG  = 76,
    PERTU_FLOAT = 82,
};

struct pertu_data
{
    struct timeval          start_time;
    uint32_t                period_ms;
    uint32_t                num_signals;
    uint32_t                num_samples;
    char                    (*signal_names)[PERTU_SIG_NAME_SIZE];
    enum pertu_data_type    *signal_types;
    uint8_t                 *signal_data;
};

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef PERTU
static uint32_t pertu_read_signal(char *filename, FILE *file,
                                  struct pertu_data *data, uint32_t signal_idx);
#endif

// External functions

PERTU_EXT void     pertu_free_data(struct pertu_data *data);
PERTU_EXT uint32_t pertu_read_file(char *filename, struct pertu_data *data);

#ifdef __cplusplus
}
#endif

#endif

// EOF
