#!/usr/bin/perl
use File::Basename;
use File::Spec;
use Getopt::Std;
use strict;

#This script performs the following checks:
# * Datbase files exist
# * There are more than 10 strings
# * All the strings are shorter than 100 characters
# * All the strings contain either alphanumeric characters or punctuation
# * There are more than 10 gain and offset pairs
# * gain and offset can not be both 0 at the same time
# * |gain| <= 10
# * |offset| <=  4096

my ($VOL, $EXEC_DIR, $EXEC_FILE) = File::Spec->splitpath(__FILE__);
my $VERBOSE                      = 0;  

sub test_string_length 
{
	for my $str (@_)
	{
		if (length($str) > 100) 
		{
			printf("TEST FAILED, $EXEC_FILE: 1+ labels are too long (e.g. \'$str\')\n");
			return 1;
		}
		
		if (length($str) == 0)
		{
			printf("TEST FAILED, $EXEC_FILE: 1+ labels have length 0\n");
			return 1;
		}
	}
	printf("TEST OK, $EXEC_FILE: All the labels have a normal length\n");
	return 0;
	
}

sub test_number_of_strings 
{

	if (scalar(@_) < 10)
	{
		my $l = scalar(@_);
		printf("TEST FAILED, $EXEC_FILE: The number of parsed labels is too smal (n=$l)\n");
		return 1;
	} else 
	{
		printf("TEST OK, $EXEC_FILE: The number of parsed labels is normal\n");
		return 0;
	}	
}

sub test_string_content {
	for my $str (@_)
	{
		if ($str !~ /^[a-zA-Z0-9\s!?\/\<>.,'"@()[\]+-_#:]*$/) 
		{
			printf("TEST FAILED, $EXEC_FILE: 1+ labels contain non standard characters (e.g. \'$str\')\n");
			return 1;
		}
		
	}
	printf("TEST OK, $EXEC_FILE: All labels contain standard characters\n");
	return 0;
}

sub test_number_of_gains 
{
	if (scalar(@_) < 5)
	{
		my $l = scalar(@_)/2;
		printf("TEST FAILED, $EXEC_FILE: The number of parsed gain-offset pairs is too smal (n=$l)\n");
		return 1;
	} else 
	{
		printf("TEST OK, $EXEC_FILE: The number of parsed gain-offset pairs is normal\n");
		return 0;
	}	
	
}

sub test_zero_gains 
{
	my @all = @_;
	while(@all)
	{
		my $gain = shift @all;
		my $offset = shift @all;

		if ($gain == 0 && $offset == 0)
		{
			printf("TEST FAILED, $EXEC_FILE: 1+ pairs of gain/offset contain two 0s\n");
			return 1;
		}
	}	
	printf("TEST OK, $EXEC_FILE: All pairs of gain/offset are different than 0\n");
	return 0;
	
}

sub test_max_gains 
{
	my @all = @_;
	while(@all)
	{
		my $gain = shift @all;
		my $offset = shift @all;
		if (abs($gain) > 10)
		{
			printf("TEST FAILED, $EXEC_FILE: 1+ gain is too big (e.g. gain=$gain)\n");
			return 1;
		}
		if (abs($offset) > 4096)
		{
			printf("TEST FAILED, $EXEC_FILE: 1+ offset is too big (e.g. offset=$offset)\n");
			return 1;
		}
	}	
	printf("TEST OK, $EXEC_FILE: All pairs of gain/offset look normal\n");
	return 0;
}

sub test_file_exists 
{
	my $fn = shift @_;
	
	if (-e $fn) 
	{
		printf("TEST OK, $EXEC_FILE: File $fn exists\n");
		return 0; 
	} else 
	{
		printf("TEST FAILED, $EXEC_FILE: File $fn does not exist\n");
		return 1;
	}
	
}

sub usage() 
{
	printf("Usage: $EXEC_FILE [-h | -v] <path>\n\n");
	printf("Test if the content of the SysDb, CompDb, and DimDb is correct.\n\n");	
	printf("Where:\n");	
	printf("    -h        This help\n\n");	
	printf("    -v        Verbose. Outputs the content of DB in the stdout\n");	
	
	exit(2);
}

# Parse Options and arguments

my %opt;
getopts( "hv", \%opt ) or usage();

if ($opt{"h"}) 
{
	usage();
}

if ($opt{"v"}) 
{
	$VERBOSE=1 ;
}

if (scalar(@ARGV) < 1) 
{
	printf("ERROR: Missing path to database filenames\n\n");
	
	usage();
}

my ($dbpath) = @ARGV;

# Test file before continuing

my $errors  = 0;

my $sysdb  = glob "$dbpath/sysdb.bin"; 
my $compdb = glob "$dbpath/compdb.bin";
my $dimdb  = glob "$dbpath/dimdb.bin";

$errors += test_file_exists($sysdb);
$errors += test_file_exists($compdb);
$errors += test_file_exists($dimdb);

if ($errors) 
{
    exit(1);
}

# execute FGC DB dump program
my $host_os = `uname -s`;
$host_os    =~ s/\s+$//;
my $host_cpu= `uname -m`;
$host_cpu    =~ s/\s+$//;
my $script  = "$EXEC_DIR/../dump/$host_os/$host_cpu/dump_fgc_db.exe $sysdb $compdb $dimdb";
$script     =~ s/\s+$//;
my $output = `$script` or die "Can not execute $script: $!\n";

if ($VERBOSE) 
{
	printf $output;
}

# find all strings separated by single quotes

my @all_strings = ($output =~ /\'(.*?)\'/g);

#find all pairs of gain and offset: "x*0.293945 + 0.000000"

my @all_gain_offset = ($output =~ /x\*(\d+\.\d+)\s\+\s([-]?\d+\.\d+)/g);

# Run tests
	
$errors += test_number_of_strings(@all_strings);
$errors += test_string_length(@all_strings);
$errors += test_string_content(@all_strings);
	
$errors += test_number_of_gains(@all_gain_offset);
$errors += test_zero_gains(@all_gain_offset);
$errors += test_max_gains(@all_gain_offset);

exit($errors);



