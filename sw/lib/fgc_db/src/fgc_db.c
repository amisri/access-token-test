/*!
 * @file   fgcddb.c
 * @brief  FGC DB Parser implementation
 * @author Marc Magrans de Abril
 * @author Michael Davis
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <fgc_consts_gen.h>
#include <fgc/fgc_db.h>



/********************************************************************
 * Protocol version numbers should be moved to parser
 *******************************************************************/

#define FGC_SYSDB_VERSION                    0
#define FGC_MAX_SYS                         60
#define FGC_SYSDB_SYS_LEN                    5
#define FGC_MAX_LOG_MENUS                   20
#define FGC_MAX_LOG_MENU_NAMES              50
#define FGC_SYSDB_MENU_NAME_LEN             10
#define FGC_SYSDB_MENU_PROP_LEN             20
#define FGC_SYSDB_SYS_LBL_LEN               80

#define FGC_COMPDB_VERSION                   0
#define FGC_N_COMP_GROUPS                   30
#define FGC_COMPDB_COMP_LBL_LEN             80
#define FGC_COMPDB_COMP_LEN                 10
#define FGC_COMPDB_N_COMPS                 166
#define FGC_COMPDB_PROP_LEN                 20

#define FGC_DIMDB_VERSION                    0
#define FGC_MAX_DIMS                        20
#define FGC_N_DIM_DIG_INPUTS                12
#define FGC_N_DIM_ANA_CHANS                  4
#define FGC_N_DIM_DIG_BANKS                  2
#define FGC_DIMDB_MAX_DIM_NAME_LEN          13

//to-do//#define FGC_IDDB_SIZE                   196608                                        // C62 uses 3x64 Kb storage banks for IDDB = 192 Kb total
#define FGC_PTDB_SIZE                     8192                                                 // C62 has 8 Kb available in bank 4 for PTDB
#define FGC_PTDB_N_RECS (FGC_PTDB_SIZE / sizeof(struct FGC_pt_rec_v0))


/***********************************************************
 * SYSDB version 0 constants and structures originally at:
 *  fgc/sw/fgc/mcu/inc/fgc_sysdb.h
 *  fgc/sw/inc/classes/50/defconst.h
 *  fgc/sw/inc/classes/60/defconst.h
 **********************************************************/

// DIM module element within a System version 0

struct FGC_sysdb_dim_v0
{
    uint8_t                   logical_dim;
    uint8_t                   flat_qspi_board_number;
    uint16_t                   name_idx;                                                        // Address in fgc_dimdb names[][] array
    uint16_t                   dim_type_idx;                                                    // Address in struct FGC_dimdb_type array
};

// Element of SysDb version 0

struct FGC_sysdb_sys_v0
{
    char                      type[FGC_SYSDB_SYS_LEN + 1];                                     // System type, e.g. "RPMBB"
    char                      label[FGC_SYSDB_SYS_LBL_LEN + 1];                                // System label
    char                      pad;                                                             // Word align
    uint8_t                   required[FGC_N_COMP_GROUPS];                                     // Required per group
    uint16_t                  n_log_menus;                                                     // Number of log menus
    uint8_t                   menu_name_idx[FGC_MAX_LOG_MENUS];                                // Log menu names (index into menu_name[])
    uint8_t                   menu_prop_idx[FGC_MAX_LOG_MENUS];                                // Log menu properties (idx into menu_prop[])

    struct FGC_sysdb_dim_v0   dims[FGC_MAX_DIMS];
};

// SysDb version 0
typedef char sys_menu_name_t[FGC_SYSDB_MENU_NAME_LEN];                                          // Sys menu_name type
typedef char sys_menu_prop_t[FGC_SYSDB_MENU_PROP_LEN];                                          // Sys menu_prop type

struct FGC_sysdb_v0
{
    uint16_t                  def_version;                                                     // Definition version (must match DimDB)
    uint16_t                  n_sys;                                                           // Number of systems
    sys_menu_name_t          * FAR menu_name;                                                       // Log menu names [FGC_MAX_LOG_MENUS_NAMES]
    sys_menu_prop_t          * FAR menu_prop;                                                       // Log menu properties [FGC_MAX_LOG_MENUS_NAMES]

    struct FGC_sysdb_sys_v0  * FAR sys;
};



/***********************************************************
 * DIMDB version 0 constants and structures originally at:
 * fgc/sw/fgc/mcu/inc/fgc_sysdb.h
 * fgc/sw/inc/classes/50/defconst.h
 * fgc/sw/inc/classes/60/defconst.h
 **********************************************************/

// Analogue channel definitions

struct FGC_dimdb_ana_v0
{
    FP32                      gain;                                                            // This is included in fgc_log_sig_header
    FP32                      offset;                                                          // Sig = gain * value + offset
    uint16_t                  label_offset;                                                    // Offset in heap to analog signal label string
    uint16_t                  units_offset;                                                    // Offset in heap to analog signal units string
};

struct FGC_dimdb_dig_v0
{
    uint16_t                  chan_lbl_offset;                                                 // Offset in heap to digital channel label
    uint16_t                  one_lbl_offset;                                                  // Offset in heap to level one label
    uint16_t                  zero_lbl_offset;                                                 // Offset in heap to level zero label
};

struct FGC_dimdb_bank_v0
{
    struct FGC_dimdb_dig_v0   dig[FGC_N_DIM_DIG_INPUTS];
    uint16_t                  faults_mask;                                                     // Input faults mask
};

struct FGC_dim_type_v0
{
    struct FGC_dimdb_ana_v0   ana[FGC_N_DIM_ANA_CHANS];
    struct FGC_dimdb_bank_v0  bank[FGC_N_DIM_DIG_BANKS];
};

typedef char dim_name_t[FGC_DIMDB_MAX_DIM_NAME_LEN + 1];                                       // DIM name type

struct FGC_dimdb_info_v0
{
    uint16_t                  n_dim_names;                                                     // Number of DIM names
    uint16_t                  n_dim_types;                                                     // Number of DIM types
    dim_name_t               * FAR names;                                                           // DIM names array (from dim_names.xml)
    struct FGC_dim_type_v0   * FAR dim_type;                                                        // DIM types array
};

struct FGC_dimdb_v0
{
    uint16_t                  def_version;                                                     // Definition version (must match SysDB), should always be on top
    uint16_t                  heap_size;                                                       // Heap size in words
    char                     * FAR heap;                                                            // Heap with strings
    struct FGC_dimdb_info_v0  info;                                                            // Names and types
};



/***********************************************************
 * COMPDB version 0 constants and structures originally at:
 * fgc/sw/fgc/mcu/inc/fgc_compdb.h
 * fgc/sw/inc/classes/50/defconst.h
 * fgc/sw/inc/classes/60/defconst.h
 **********************************************************/

// Component data

struct FGC_compdb_comp_v0
{
    char                      type[FGC_COMPDB_COMP_LEN + 1];                                   // Component type, e.g. "HCRFBCA___"
    char                      label[FGC_COMPDB_COMP_LBL_LEN + 1];                              // Component label, e.g. "FGC CPU-200"
    char                      prop_temp[FGC_COMPDB_PROP_LEN];                                  // e.g. "TEMP.FGC.IN", "TEMP.A.DCCT.HEAD",...
    char                      prop_barcode[FGC_COMPDB_PROP_LEN];                               // e.g. "BARCODE.FGC.",...
    uint16_t                  group_size;                                                      // Number of group that the group belong to
    uint16_t                  group_offset;                                                    // Offset to the component group data
};

struct FGC_compdb_v0
{
    uint16_t                  def_version;                                                     // Definition version (must match SysDB)
    uint16_t                  n_comps;                                                         // Number of components
    uint16_t                  comps_size;                                                      // Size of components in memory (n_comps * sizeof(fgc_compdb_comp)/sizeof(WORD))

    struct FGC_compdb_comp_v0 * FAR comps;                                                          // Info of each components
    uint16_t                  * FAR groups;                                                         // Group membership for each system                                              
};

/********************************************************************
 *
 * IDDB and PTDB version 0 constants and structures originally at:
 *
 * fgc/sw/fgc/31/inc/db.h
 * fgc/sw/fgc/31/src/db.c
 *
 *******************************************************************/

// Structure of an IDDB directory record

struct ATTPACK FGC_dir_rec_v0
{
    uint16_t                  first_idx;                                                       // Index of first record in table
    uint16_t                  last_idx;                                                        // Index of last record in table
    uint8_t                   tbl_byte;                                                        // Table byte
};

// Structure of an IDDB ID record

struct ATTPACK FGC_id_rec_v0
{
    uint16_t                  part_no;                                                         // Part number = serial_num + pt_base - serial_base
    uint8_t                   id123[3];                                                        // Dallas ID bytes, ordered with least significant byte first
};

// Each entry in IDDB is either a directory or an ID record. They are the same size.

union FGC_iddb_rec_v0
{
    struct FGC_dir_rec_v0     dir;
    struct FGC_id_rec_v0      id;
};

// Structure of a PTDB record
struct ATTPACK FGC_pt_rec_v0
{
    uint16_t                  pt_base;                                                         // Lowest index for part type
    uint32_t                  serial_base;                                                     // Base serial number
    char                      type_code[FGC_PTDB_TYPE_CODE_LEN];                               // Type code, e.g. "HCRFBMA___-GL\0"
};

/********************************************************************
 * Global variables and macros
 *******************************************************************/

static uint16_t * FAR           sysdb_ptr   = NULL;             // location of the Sysdb
static struct   FGC_sysdb_v0    g_sysdb     = {0, 0, NULL, NULL, NULL};       // SysDb structure
static uint16_t * FAR           compdb_ptr  = NULL;             // location of the DimDb
static struct FGC_compdb_v0     g_compdb    = {0, 0, 0, NULL, NULL};       // CompDb structure
static uint16_t * FAR           dimdb_ptr   = NULL;              // location of the DimDb
static struct FGC_dimdb_v0      g_dimdb     = {0, 0, NULL};      // DimdDb structure
static union  FGC_iddb_rec_v0 * FAR g_iddb  = NULL;              // location of the IDDB
static struct FGC_pt_rec_v0   * FAR g_ptdb  = NULL;              // location of the PTDB

/********************************************************************
 * Static functions
 *******************************************************************/

/*
 * Utilities for big endian encoding
 */

// This compile time check only works if uint8_t occupies 8 bits, and uint16_t occupies 16 bits

#ifndef __BYTE_ORDER__
#error __BYTE_ORDER__ is not defined
#endif

// Define ntohs and ntohf depending on target endianess

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__

static uint16_t ntohs(uint16_t x)
{
    return x;
}

static FP32 ntohf(FP32 x)
{
    return x;
}

#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__

union endianess32_t
{
    uint32_t n;
    uint8_t  c[4];
    FP32     f;
};

union endianess16_t
{
    uint16_t n;
    uint8_t  c[2];
};

static uint16_t ntohs(uint16_t x)
{
    union endianess16_t to,from;

    from.n = x;
    to.c[0] = from.c[1];
    to.c[1] = from.c[0];

    return to.n;
}

static FP32 ntohf(FP32 f)
{
    union endianess32_t to,from;

    from.f = f;

    to.c[0] = from.c[3];
    to.c[1] = from.c[2];
    to.c[2] = from.c[1];
    to.c[3] = from.c[0];

    return to.f;
}

#else
#error Non of the endianess flags is defined: LITTLE_ENDIAN, BIG_ENDIAN
#endif

static const char * FAR DimDbName(fgc_db_t idx)
{
    return g_dimdb.info.names[idx];
}

static const char * FAR DimDbHeap(fgc_db_t offset)
{
    return (char * FAR) &(g_dimdb.heap[offset]);
}

/*
 * Calculate Dallas 1-wire 8-bit CRC
 *
 * As described in Maxim Application Note 27: "Understanding and Using Cyclic Redundancy Checks with Maxim iButton Products"
 */

static uint8_t dallas_crc8(const struct Dallas_id *id)
{
    const uint8_t *byte = (const uint8_t*)(id) + 7;
    uint8_t        crc  = 0;

    uint8_t i,j;

    for (i = 0; i < 8; ++i) 
    {
        uint8_t inbyte = *byte--;

        for (j = 0; j < 8; ++j) 
        {
            uint8_t mix = (crc ^ inbyte) & 0x01;

            crc >>= 1;

            if (mix) crc ^= 0x8C;

            inbyte >>= 1;
        }
    }

    return crc;
}

/*
 * Perform a sequential search of the IDDB
 *
 * @param[in]        index    Index byte. Value is matched to FGC_dir_rec_v0::tbl_byte.
 * @param[in,out]    first    IDDB index to start search
 * @param[in,out]    last     IDDB index to end search
 */

static bool IdDbSequentialSearch(uint8_t index, uint16_t *first, uint16_t *last)
{
    uint16_t i;

    for(i = *first; i <= *last; ++i)
    {
        if(index == g_iddb[i].dir.tbl_byte)
        {
            *first = g_iddb[i].dir.first_idx;
            *last  = g_iddb[i].dir.last_idx;

            return true;
        }
    }

    return false;
}

/*
 * Compare two 24-bit values
 *
 * @param[in]     code1      24-bit essential ID to compare
 * @param[in]     code2      24-bit DB index to compare
 *
 * The values to compare are three bytes, with LSB first.
 *
 * @retval        -1         code1 <  code2
 * @retval         0         code1 == code2
 * @retval         1         code1  > code2
 */

static int8_t IdDbCmp(const uint8_t *code1, const uint8_t *code2)
{
    int32_t i;

    for(i = 0; i <= 2; ++i)
    {
        if(code1[i] < code2[2-i]) return -1;
        if(code1[i] > code2[2-i]) return  1;
    }

    return 0;
}

/*
 * Perform a binary search of the IDDB
 *
 * @param[in]     code       24-bit essential ID
 * @param[in]     first      IDDB index to start search
 * @param[in]     last       IDDB index to end search
 * @param[out]    part_no    Part number to look up in PTDB
 */

static bool IdDbBinarySearch(const uint8_t *code, uint16_t first, uint16_t last, uint16_t *part_no)
{
    uint16_t current = first;

    // Validate start and end points of the search

    if(first > last) return false;

    // Perform a binary search of the search space

    while(first != last)
    {
        int8_t cmp;

        current = first + ((last-first) / 2);
        cmp     = IdDbCmp(code, g_iddb[current].id.id123);

        if(cmp < 0)
        {
            last = current;
        }
        else if (cmp > 0)
        {
            if(last == first + 1) current = last;

            first = current;
        }
        else break;
    }

    // Check if we found a matching ID

    if(IdDbCmp(code, g_iddb[current].id.id123) == 0)
    {
        *part_no = g_iddb[current].id.part_no;

        return true;
    }

    return false;
}



/********************************************************************
 * SysDb implementation
 *******************************************************************/

int32_t SysDbInit(void* FAR sysdb)
{
    sysdb_ptr = (uint16_t * FAR)sysdb;

    g_sysdb.def_version =  ntohs(*sysdb_ptr++);

    g_sysdb.n_sys = ntohs(*sysdb_ptr++);

    g_sysdb.menu_name = (sys_menu_name_t * FAR)(sysdb_ptr);
    sysdb_ptr += (FGC_MAX_LOG_MENU_NAMES * FGC_SYSDB_MENU_NAME_LEN) / sizeof(uint16_t);

    g_sysdb.menu_prop = (sys_menu_prop_t * FAR)(sysdb_ptr);
    sysdb_ptr += (FGC_MAX_LOG_MENU_NAMES * FGC_SYSDB_MENU_PROP_LEN) / sizeof(uint16_t);

    g_sysdb.sys = (struct FGC_sysdb_sys_v0 * FAR)(sysdb_ptr);
    
    // Version number check

    if (SysDbVersion() == FGC_SYSDB_VERSION)
    {
        return SysDbVersion();
    } else
    {
        return -1;
    }
}

fgc_db_t SysDbVersion(void)
{
    // Version is always at the top

    return g_sysdb.def_version;
}

fgc_db_t SysDbLength(void)
{
    return g_sysdb.n_sys;
}

const char * FAR SysDbType(fgc_db_t sysdb_idx)
{
    return g_sysdb.sys[sysdb_idx].type;
}

const char * FAR SysDbLabel(fgc_db_t sysdb_idx)
{
    return g_sysdb.sys[sysdb_idx].label;
}

fgc_db_t SysDbLogMenuLength(fgc_db_t sysdb_idx)
{
    uint16_t n_log_menus;

    n_log_menus = g_sysdb.sys[sysdb_idx].n_log_menus;
    n_log_menus = ntohs(n_log_menus);

    return n_log_menus;
}

const char * FAR SysDbLogMenuName(fgc_db_t sysdb_idx, fgc_db_t log_idx)
{
    uint8_t idx;

    idx = g_sysdb.sys[sysdb_idx].menu_name_idx[log_idx];

    return g_sysdb.menu_name[idx];
}

const char * FAR SysDbLogMenuProperty(fgc_db_t sysdb_idx, fgc_db_t log_idx)
{
    uint8_t idx;

    idx = g_sysdb.sys[sysdb_idx].menu_prop_idx[log_idx];

    return g_sysdb.menu_prop[idx];
}

fgc_db_t SysDbRequiredGroupsLength(fgc_db_t sysdb_idx)
{
    return FGC_N_COMP_GROUPS;
}

fgc_db_t SysDbRequiredGroups(fgc_db_t sysdb_idx, fgc_db_t group_idx)
{
    return g_sysdb.sys[sysdb_idx].required[group_idx];
}

fgc_db_t SysDbDimsLength(fgc_db_t sysdb_idx)
{
    return FGC_MAX_DIMS;
}

fgc_db_t SysDbDimLogicalAddress(fgc_db_t sysdb_idx, fgc_db_t dim_idx)
{
    return g_sysdb.sys[sysdb_idx].dims[dim_idx].logical_dim;
}

fgc_db_t SysDbDimBusAddress(fgc_db_t sysdb_idx, fgc_db_t dim_idx)
{
    return g_sysdb.sys[sysdb_idx].dims[dim_idx].flat_qspi_board_number;
}

const char * FAR SysDbDimName(fgc_db_t sysdb_idx, fgc_db_t dim_idx)
{
    uint16_t x;

    x = ntohs(g_sysdb.sys[sysdb_idx].dims[dim_idx].name_idx);

    return DimDbName(x);
}

fgc_db_t SysDbDimDbType(fgc_db_t sysdb_idx, fgc_db_t dim_idx)
{
    uint16_t dim_type_idx;

    dim_type_idx = g_sysdb.sys[sysdb_idx].dims[dim_idx].dim_type_idx;
    dim_type_idx = ntohs(dim_type_idx);

    return dim_type_idx;
}

bool SysDbDimEnabled(fgc_db_t sysdb_idx, fgc_db_t dim_idx)
{
    return (SysDbDimLogicalAddress(sysdb_idx,dim_idx)==0xFF)?false:true;
}

/********************************************************************
 * CompDb implementation
 *******************************************************************/

int32_t CompDbInit(void* FAR compdb)
{
    compdb_ptr = (uint16_t * FAR)compdb;
    
    // Initialize CompDb structure members

    g_compdb.def_version =  ntohs(*compdb_ptr++);

    g_compdb.n_comps = ntohs(*compdb_ptr++);

    g_compdb.comps_size = (g_compdb.n_comps * sizeof(struct FGC_compdb_comp_v0)/sizeof(uint32_t));

    g_compdb.comps = (struct FGC_compdb_comp_v0 * FAR)(compdb_ptr);

    compdb_ptr += (CompDbCompsSize()* sizeof(uint16_t));

    g_compdb.groups = (uint16_t * FAR)(compdb_ptr);

    if (CompDbVersion() == FGC_COMPDB_VERSION)
    {
        return CompDbVersion();
    } else
    {
        return -1;
    }
}

fgc_db_t CompDbVersion(void)
{
    // Version is always at the top

    return g_compdb.def_version;
}

fgc_db_t CompDbLength(void)
{
    return g_compdb.n_comps;
}

fgc_db_t CompDbCompsSize(void)
{
    return g_compdb.comps_size;
}

const char * FAR CompDbType(fgc_db_t compdb_idx)
{
    return g_compdb.comps[compdb_idx].type;
}

const char * FAR CompDbLabel(fgc_db_t compdb_idx)
{
    return g_compdb.comps[compdb_idx].label;
}

const char * FAR CompDbTempProperty(fgc_db_t compdb_idx)
{
    return g_compdb.comps[compdb_idx].prop_temp;
}

const char * FAR CompDbBarcodeProperty(fgc_db_t compdb_idx)
{
    return g_compdb.comps[compdb_idx].prop_barcode;
}

fgc_db_t CompDbGroupIndex(fgc_db_t compdb_idx, fgc_db_t sysdb_idx)
{
    uint8_t        * FAR group_ptr  = ((uint8_t * FAR)g_compdb.groups) + CompDbGroupOffset(compdb_idx);    
    uint16_t const   group_size = CompDbGroupSize(compdb_idx);
    uint16_t         i;

    for (i = 0; i < group_size; i++)
    {
        if (*group_ptr++ == sysdb_idx)
        {
            return (*group_ptr);
        }
        group_ptr++;
    }
    return(0);
}

fgc_db_t CompDbGroupSize(fgc_db_t compdb_idx)
{
    return ntohs(g_compdb.comps[compdb_idx].group_size);
}

fgc_db_t CompDbGroupOffset(fgc_db_t compdb_idx)
{
    return ntohs(g_compdb.comps[compdb_idx].group_offset)*2;
}

fgc_db_t CompDbLookup(const char *type_code)
{
    int32_t i;

    // Perform a sequential search of CompDb

    for(i = 0; i < g_compdb.n_comps; ++i)
    {
        uint32_t cmp = strncmp(type_code, g_compdb.comps[i].type, FGC_COMPDB_COMP_LEN);

        if(cmp == 0) return i;

        if(cmp < 0) break;
    }

    return 0;
}



/******************************************************************************
 *  DimDB functions
 ******************************************************************************/

int32_t DimDbInit(void* FAR dimdb)
{
    // Get location of the DimDb binary

    dimdb_ptr = (uint16_t * FAR)dimdb;

    // Initialize DimDb structure members

    g_dimdb.def_version =  ntohs(*dimdb_ptr++);

    // Heap size is in words

    g_dimdb.heap_size = ntohs(*dimdb_ptr++);

    g_dimdb.heap = (char * FAR)(dimdb_ptr);

    // Info structure starts after heap

    dimdb_ptr +=  (DimDbHeapSize() * sizeof(uint16_t));

    g_dimdb.info.n_dim_names = ntohs(*dimdb_ptr++);

    g_dimdb.info.n_dim_types = ntohs(*dimdb_ptr++);

    g_dimdb.info.names = (dim_name_t * FAR)(dimdb_ptr);

    // DIM types array starts after the DIM names array

    dimdb_ptr += (DimDbNumNames() * (FGC_DIMDB_MAX_DIM_NAME_LEN +1)) / sizeof(uint16_t);

    g_dimdb.info.dim_type = (struct FGC_dim_type_v0 * FAR)(dimdb_ptr);


    if (DimDbVersion() == FGC_DIMDB_VERSION)
    {
        return DimDbVersion();
    } else
    {
        return -1;
    }
}

fgc_db_t DimDbVersion(void)
{
    // in host byte order

    return g_dimdb.def_version;
}

fgc_db_t DimDbHeapSize(void)
{
    // size is in words, host byte order

    return g_dimdb.heap_size;
}

fgc_db_t DimDbNumTypes(void)
{
    // number of dim types, host byte order

    return g_dimdb.info.n_dim_types;
}

fgc_db_t DimDbNumNames(void)
{
    // number of dim names, host byte order

    return g_dimdb.info.n_dim_names;
}

fgc_db_t DimDbNumberOfChannels(fgc_db_t dimdb_type)
{
    // 4 analog channels and 2 digital banks with several inputs each

    return 6;
}

bool DimDbAnalogEnabled(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return (g_dimdb.info.dim_type[dimdb_type].ana[ch_idx].label_offset == 0)?false:true;
}

bool DimDbIsAnalogChannel(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return (ch_idx < 4) && DimDbAnalogEnabled(dimdb_type, ch_idx);
}

FP32 DimDbAnalogGain(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return ntohf(g_dimdb.info.dim_type[dimdb_type].ana[ch_idx].gain);
}

FP32 DimDbAnalogOffset(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return ntohf(g_dimdb.info.dim_type[dimdb_type].ana[ch_idx].offset);

}

const char * FAR DimDbAnalogUnits(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    uint16_t offset;

    offset = ntohs(g_dimdb.info.dim_type[dimdb_type].ana[ch_idx].units_offset);

    return DimDbHeap(offset);
}

const char * FAR DimDbAnalogLabel(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    uint16_t offset;

    offset = ntohs(g_dimdb.info.dim_type[dimdb_type].ana[ch_idx].label_offset);

    return DimDbHeap(offset);
}

bool DimDbIsDigitalChannel(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return (ch_idx >= 4);
}

fgc_db_t DimDbDigitalInputsLength(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return FGC_N_DIM_DIG_INPUTS;
}

bool DimDbIsDigitalInput(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx)
{
    return DimDbIsDigitalChannel(dimdb_type, ch_idx) && (g_dimdb.info.dim_type[dimdb_type].bank[ch_idx-4].dig[input_idx].chan_lbl_offset != 0);
}

const char * FAR DimDbDigitalLabel(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx)
{
    uint16_t offset;

    // channel 4 is bank 0, and chanel 5 is bank 1

    offset = ntohs(g_dimdb.info.dim_type[dimdb_type].bank[ch_idx-4].dig[input_idx].chan_lbl_offset);

    return DimDbHeap(offset);
}

const char * FAR DimDbDigitalLabelOne(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx)
{
    uint16_t offset;

    // channel 4 is bank 0, and chanel 5 is bank 1

    offset = ntohs(g_dimdb.info.dim_type[dimdb_type].bank[ch_idx-4].dig[input_idx].one_lbl_offset);

    return DimDbHeap(offset);
}

const char * FAR DimDbDigitalLabelZero(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx)
{
    uint16_t offset;

    // channel 4 is bank 0, and chanel 5 is bank 1

    offset = ntohs(g_dimdb.info.dim_type[dimdb_type].bank[ch_idx-4].dig[input_idx].zero_lbl_offset);

    return DimDbHeap(offset);
}

bool DimDbDigitalIsFault(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx)
{
    uint16_t faults_mask;

    faults_mask = ntohs(g_dimdb.info.dim_type[dimdb_type].bank[ch_idx-4].faults_mask);

    return faults_mask & (0x0001 << input_idx);
}

int16_t DimDbDigitalFaults(fgc_db_t dimdb_type, fgc_db_t ch_idx)
{
    return ntohs(g_dimdb.info.dim_type[dimdb_type].bank[ch_idx-4].faults_mask);
}



/********************************************************************
 * IDDB implementation
 *******************************************************************/

int32_t IdDbInit(void *iddb)
{
    g_iddb = iddb;

    return 0;
}

void IdDbDallasIDString(char *buffer, const struct Dallas_id *id)
{   
    sprintf(buffer, "%02X%02X%02X%02X%02X%02X%02X%02X",
                    id->crc,     id->zeros[0], id->zeros[1], id->tbl_index,
                    id->code[0], id->code [1], id->code [2], id->type);
}

enum Dallas_id_error IdDbValidate(const struct Dallas_id *id)
{
    // Check CRC

    if(dallas_crc8(id) != 0) return DLS_ID_ERR_CRC;

    // Check zero bytes are really zero

    if(id->zeros[0] != 0 || id->zeros[1] != 0) return DLS_ID_ERR_ZEROS;

    // Dallas ID is in a valid format

    return DLS_ID_OK;
}

enum Dallas_id_error IdDbLookup(const struct Dallas_id *id, uint16_t *part_no)
{
    uint16_t first = g_iddb[0].dir.first_idx;
    uint16_t last  = g_iddb[0].dir.last_idx;

    // Scan the IDDB for the Dallas ID type

    if(!IdDbSequentialSearch(id->type, &first, &last)) return DLS_ID_ERR_TYPE;

    // Scan the IDDB for the Table byte

    if(!IdDbSequentialSearch(id->tbl_index, &first, &last)) return DLS_ID_ERR_TBL;

    // Search for the three essential bytes

    if(!IdDbBinarySearch(id->code, first, last, part_no)) return DLS_ID_ERR_NOT_FOUND;

    return DLS_ID_OK;
}



/********************************************************************
 * PTDB implementation
 *******************************************************************/

int32_t PtDbInit(void *ptdb)
{
    g_ptdb = ptdb;

    return 0;
}

int32_t PtDbLookup(uint16_t part_no)
{
    uint16_t i;

    // Perform a sequential search of PTDB

    for(i = 0; i < FGC_PTDB_N_RECS - 1; ++i)
    {
        if(part_no < g_ptdb[i+1].pt_base)
        {
            return i;
        }
    }

    return -1;
}

const char *PtDbTypeCode(uint32_t ptdb_idx)
{
    return g_ptdb[ptdb_idx].type_code;
}

void PtDbBarcodeString(char *buffer, uint16_t part_no, uint32_t ptdb_idx)
{
    int32_t len = snprintf(buffer, FGC_PTDB_TYPE_CODE_LEN, "%s", g_ptdb[ptdb_idx].type_code);

    if(len > FGC_PTDB_TYPE_CODE_LEN - 1) len = FGC_PTDB_TYPE_CODE_LEN - 1;

    sprintf(buffer + len, "%06u", (unsigned int)(g_ptdb[ptdb_idx].serial_base + part_no - g_ptdb[ptdb_idx].pt_base));
}

// EOF
