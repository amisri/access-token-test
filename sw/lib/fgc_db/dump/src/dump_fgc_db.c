/*!
 * 	@file test_sysdb.c
 *
 * 	@author Marc Magrans de Abril
 *
 * 	@brief Test cases for the SysDb parser
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "fgc/fgc_db.h"

void dump_dim(size_t sysdb_idx, size_t dim_idx)
{
	size_t k, l, dim_type;

	printf("  DIM #%zu '%s': dim_type=%d, logical_addr=%d, bus_addr=%d\n"
			, dim_idx
			, SysDbDimName(sysdb_idx, dim_idx)
			, SysDbDimDbType(sysdb_idx, dim_idx)
			, SysDbDimLogicalAddress(sysdb_idx, dim_idx)
			, SysDbDimBusAddress(sysdb_idx, dim_idx));

	dim_type = SysDbDimDbType(sysdb_idx, dim_idx);

	for(k=0; k < DimDbNumberOfChannels(dim_type); ++k)
	{
		if (DimDbIsAnalogChannel(dim_type,k))
		{
			printf("    - A[%zu]: '%s' [%s] = x*%f + %f\n"
			        , k
					, DimDbAnalogLabel(dim_type,k)
					, DimDbAnalogUnits(dim_type,k)
					, DimDbAnalogGain(dim_type,k)
					, DimDbAnalogOffset(dim_type,k)
			);
		} else if (DimDbIsDigitalChannel(dim_type,k))
		{
			for(l=0; l != DimDbDigitalInputsLength(dim_type,k); ++l )
			{
				if (DimDbIsDigitalInput(dim_type,k,l))
				{
					printf("    - D[%zu][%2zu]: %s'%s' = %s/%s\n"
					        , k
					        , l
							, DimDbDigitalIsFault(dim_type,k,l)?"(FAULT) ":""
							, DimDbDigitalLabel(dim_type,k,l)
							, DimDbDigitalLabelOne(dim_type,k,l)
							, DimDbDigitalLabelZero(dim_type,k,l)
							);
				}
			}
		}
	}
}

void dump_comp(size_t sysdb_idx)
{
	size_t j,k;
	for(j=0; j < SysDbRequiredGroupsLength(sysdb_idx); ++j)
	{
		if (!SysDbRequiredGroups(sysdb_idx,j))
		{
			continue;
		}

		printf("    - %d Required: ", SysDbRequiredGroups(sysdb_idx,j));
		for(k=0; k < CompDbLength(); ++k)
		{
			if (CompDbGroupIndex(k,sysdb_idx) == j)
			{
				printf("'%s', ", CompDbType(k));
			}
		}
		printf("\n");

	}

}

//dumps the content of the sysdb buffer to stdout
void dumpdb(void* sysdb, void* compdb, void* dimdb)
{
	size_t i,j;

	SysDbInit(sysdb);
	DimDbInit(dimdb);
	CompDbInit(compdb);

	printf("DimDb Heap size: %d words\n", DimDbHeapSize());
	printf("DimDb DIM names: %d\n", DimDbNumNames());
	printf("DimDb DIM types: %d\n", DimDbNumTypes());

	printf("\nSysDb Encoding v. %d (%d elements)\n", SysDbVersion(), SysDbLength());
	printf("CompDb Encoding v. %d (%d elements)\n", CompDbVersion(), CompDbLength());

	for(i=0; i < SysDbLength(); ++i)
	{
		printf("\nSystem #%zu '%s' '%s'\n",i, SysDbType(i), SysDbLabel(i));

		printf("  Required components:\n");
		dump_comp(i);

		printf("  %d Spy properties\n", SysDbLogMenuLength(i));
		for(j=0; j < SysDbLogMenuLength(i); ++j)
		{
			printf("    - '%s' '%s' \n",SysDbLogMenuName(i,j), SysDbLogMenuProperty(i,j));
		}

		for(j=0; j < SysDbDimsLength(i); ++j)
		{
			if (SysDbDimEnabled(i,j))
			{
				dump_dim(i,j);

			}
		}

		//			printf("    - #%lu %s: logical = %d, bus = %d, type = %d"
		//					, j
		//					, SysDbDimName(i,j)
		//					, SysDbDimLogicalAddress(i,j)
		//					, SysDbDimBusAddress(i,j)
		//					);

	}
}

//Reads a file, allocates a buffer of the size of the file contents, and copies the contents to the buffer.
size_t read_file(const char* fn, void** buffer)
{
	FILE* f;                           //file handler
	size_t file_size, read_size;       //size of the file, and the amount of data read

	//open file
	f = fopen(fn,"rb");
	if (!f)
	{
		printf("ERROR: Unable to open '%s'\n",fn);
		return 0;
	}

	//size of the sysdb
	fseek(f, 0L, SEEK_END);
	file_size = ftell(f);
	fseek(f, 0L, SEEK_SET);

	//allocate sysdb memory buffer
	*buffer = malloc(file_size);

	//read file
	read_size = fread(*buffer,1,file_size,f);
	fclose(f);

	if (file_size != read_size)
	{
		printf("ERROR: Read error, file size != amount read (%zu != %zu)", file_size, read_size);
		free(*buffer);
		return 0;
	}

	return read_size;
}

int main(int argc, char *argv[])
{
	void* sysdb;          //pointer to the sysdb buffer
	void* compdb;         //pointer to the dimdb buffer
	void* dimdb;          //pointer to the dimdb buffer
	size_t read_size;     //amount read

	//check arguments
	if (argc != 4)
	{
		printf("ERROR: Missing database file names in arguments\n");
		printf("\nUsage: %s <sysdb file name> <compdb filename> <dimdb filename>\n", argv[0]);
		exit(1);
	}

	read_size = read_file(argv[1], &sysdb);
	printf("\nSysDb  size: %zu bytes\n",read_size);

	if (!read_size)
	{
		exit(1);
	}

	read_size = read_file(argv[2], &compdb);
	printf("CompDb size: %zu bytes\n",read_size);

	if (!read_size)
	{
		exit(1);
	}

	read_size = read_file(argv[3], &dimdb);
	printf("DimDb  size: %zu bytes\n\n",read_size);

	if (!read_size)
	{
		exit(1);
	}


	dumpdb(sysdb,compdb,dimdb);

	return 0;
}

