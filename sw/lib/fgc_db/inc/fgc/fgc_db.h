/*!
 * @file   fgc_db.h
 * @brief  FGC DB Parser
 * @author Marc Magrans
 * @author Michael Davis
 *
 * This file contains the declarations of the functions used to
 * access to the contents of the SysDb, CompDb, and DimDb DB.
 * Scope: FGC, FGC2, FGC3, FGCD
 */

#ifndef FGC_FGC_DB_H_
#define FGC_FGC_DB_H_

#include <cc_types.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


#define FGC_PTDB_TYPE_CODE_LEN              14                                                 // Length of type code in Part Type database, e.g. "HCRFBMA___-GL\0"

// Types

/*!
 * Recognised values for the Dallas_id::type field.
 *
 * Most devices are type 0x01 (Dallas ID only). Devices with an on-board temperature sensor are type
 * 0x028. The other types are used only in a few exceptional cases.
 */
typedef uint16_t fgc_db_t;

enum Dallas_device_type
{
    DLS_NONE                  = 0x00,          //!< No device present
    DLS_SERIAL_NUMBER         = 0x01,          //!< Silicon serial number
    DLS_HIGH_PREC_TEMP_SENSOR = 0x10,          //!< High-precision digital thermometer
    DLS_QUAD_AD_CONVERTER     = 0x20,          //!< Quad A/D converter
    DLS_TEMP_SENSOR           = 0x28           //!< Programmable resolution digital thermometer
};



/*!
 * Dallas ID error codes.
 *
 * Errors can be caused by an invalid Dallas ID (e.g. failed CRC check) or if the Dallas ID is not
 * found in the IDDB database.
 */

enum Dallas_id_error
{
    DLS_ID_OK,                                 //!< Dallas ID is valid/present in database
    DLS_ID_ERR_CRC,                            //!< CRC error (ID7)
    DLS_ID_ERR_ZEROS,                          //!< Zero bytes (ID5/ID6) contain a non-zero value
    DLS_ID_ERR_TYPE,                           //!< Type (ID0) is not in the database
    DLS_ID_ERR_TBL,                            //!< Table (ID4) is not in the database
    DLS_ID_ERR_NOT_FOUND                       //!< 24-bit ID (ID1/ID2/ID3) not found
};



/*!
 * Dallas ID fields.
 *
 * A Dallas ID is always 64 bits long.
 *
 * ID0 determines if the device has a digital thermometer or not. Devices with an unrecognised ID
 * will be treated in the same way as 0x01 (ID only).
 *
 * ID5-6 are assumed to be zero, so they are not encoded in IDDB. However, there are a few devices
 * which have non-zero values here. This is not a problem so long as the device can still be uniquely
 * identified from the essential bits in ID1-3.
 */

#if defined(__GNUC__)
#define ATTPACK __attribute__((__packed__))
#else
#define ATTPACK
#endif

struct ATTPACK Dallas_id
{
    uint8_t    crc;              //!< ID7
    uint8_t    zeros[2];         //!< ID5-6. 16 bits assumed to be zero and not encoded in IDDB.
    uint8_t    tbl_index;        //!< ID4. Table index byte.
    uint8_t    code[3];          //!< ID1-3. Essential 24 bits which are encoded in the IDDB to uniquely identify this device.
    uint8_t    type;             //!< ID0. See ::Dallas_device_type for valid values
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

// SysDb functions

/*!
 * Initializes the internal data structures for the System DB (SysDB).
 *
 * @param sysdb address of the buffer containing SysDb encoded data
 *
 * @returns the encoding version number of the SysDb.
 * If the encoding version is not compatible, then -1 is returned.
 */

int32_t SysDbInit(void* FAR sysdb);

/*!
 * SysDB encoding version.
 *
 * @returns the version number.
 */

fgc_db_t SysDbVersion(void);

/*!
 * Number of SysDb elements.
 *
 * @returns the number of elements of the SysDb.
 *
 */

fgc_db_t SysDbLength(void);

/*!
 * Type of a given SysDb element.
 *
 * @param sysdb_idx Element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @returns a pointer to the type c string.
 *
 */

const char * FAR SysDbType(fgc_db_t sysdb_idx);

/*!
 * Human readable description of a given SysDb element.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @returns a pointer to the label c string.
 *
 */

const char * FAR SysDbLabel(fgc_db_t sysdb_idx);

/*!
 * Number of elements of the logging menu of a SysDB System. The logging menu refers to properties that can be requested in chunks.
 * A typical user of these logging properties is the Labview Spy application.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @returns the size of the logging menu for a given System.
 *
 */

fgc_db_t SysDbLogMenuLength(fgc_db_t sysdb_idx);

/*!
 * Human readable description of the ith log menu item.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param log_idx   Logging menu element number of a given System (i.e. between 0 and size-1 of the logging menu
 * for a given SysDB System)
 *
 * @returns a pointer to a c string with the logging menu name.
 *
 */

const char * FAR SysDbLogMenuName(fgc_db_t sysdb_idx, fgc_db_t log_idx);

/*!
 * Property name of the ith log menu item.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param log_idx   Logging menu element number of a given System (i.e. between 0 and size-1 of the logging menu
 * for a given SysDB System)
 *
 * @returns a pointer to a c string with the logging menu name.
 *
 */

const char * FAR SysDbLogMenuProperty(fgc_db_t sysdb_idx, fgc_db_t log_idx);

/*!
 * Number of different required component groups of a given System.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @returns the size of the logging menu for a given System.
 *
 */

fgc_db_t SysDbRequiredGroupsLength(fgc_db_t sysdb_idx);

/*!
 * Number of components of group type group_idx required for a given SysDb System.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param group_idx Required group element number (i.e. between 0 and size-1 of the required group size)
 *
 * @returns the number of components of the same group required for a given system.
 *
 */

fgc_db_t SysDbRequiredGroups(fgc_db_t sysdb_idx, fgc_db_t group_idx);


/*!
 * Number of Diagnostic Modules (DIMs) for a given System.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @returns the number of DIMs for a given System.
 *
 */

fgc_db_t SysDbDimsLength(fgc_db_t sysdb_idx);

/*!
 * Logical address of the dim_idx-th Diagnostic Modules (DIMs).
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param dim_idx   DIM index
 *
 * @returns the logical address for a given DIM.
 *
 */

fgc_db_t SysDbDimLogicalAddress(fgc_db_t sysdb_idx, fgc_db_t dim_idx);

/*!
 * Bus address of the dim_idx-th Diagnostic Modules (DIMs).
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param dim_idx   DIM index
 *
 * @returns the number of DIMs for a given System.
 * If this DIM is not used 0xFF is returned.
 *
 */

fgc_db_t SysDbDimBusAddress(fgc_db_t sysdb_idx, fgc_db_t dim_idx);

/*!
 * Name of the dim_idx-th Diagnostic Modules (DIMs).
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param dim_idx   DIM index
 *
 * @returns a c string with the name of the DIM module.
 * If the DIM is not used, then an unknown string is returned.
 *
 */

const char* FAR SysDbDimName(fgc_db_t sysdb_idx, fgc_db_t dim_idx);

/*!
 * DimDB type index of the dim_idx-th Diagnostic Modules (DIMs) on the sysDb.
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param dim_idx   DIM index in a given SysDb entry
 *
 * @returns the DimDb type index for a given System.
 * If the DIM is not used, then 0 is returned.
 *
 */

fgc_db_t SysDbDimDbType(fgc_db_t sysdb_idx, fgc_db_t dim_idx);

/*!
 * Is the DIM enabled?
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @param dim_idx   DIM index in a given SysDb entry
 *
 * @returns true if the DIM is enabled (i.e. logical address != 0xFF).
 *
 */

bool SysDbDimEnabled(fgc_db_t sysdb_idx, fgc_db_t dim_idx);



// CompDB functions

/*!
 * Initializes the internal data structures for the Component DB (CompDB).
 *
 * @param compdb address of the buffer containing CompDb encoded data
 *
 * @returns the encoding version number of the CompDb.
 * If the encoding version is not compatible, then -1 is returned.
 */

int32_t CompDbInit(void* FAR compdb);

/*!
 * CompDB encoding version.
 *
 * @returns the encoding version number.
 */

fgc_db_t CompDbVersion(void);

/*!
 * Size of the CompDb.
 *
 * @returns the number of elements of the CompDb.
 *
 */

fgc_db_t CompDbLength(void);

/*!
 * Size of the CompDb.
 *
 * @returns the size of the components data in the CompsDB.
 *
 */

fgc_db_t CompDbCompsSize(void);

/*!
 * Number of groups that the component belong to.
 *
 * @param compdb_idx CompDb element number (i.e. between 0 and size-1 of the CompDb)
 *
 * @returns the number of groups that the component belong to.
 *
 */

fgc_db_t CompDbGroupSize(fgc_db_t compdb_idx);

/*!
 * Offset of a given components for the groups that belong to.
 *
 * @param compdb_idx CompDb element number (i.e. between 0 and size-1 of the CompDb)
 *
 * @returns the offset needed to be add to the pointer of groups in order to access to the group info of a given components.
 *
 */

fgc_db_t CompDbGroupOffset(fgc_db_t compdb_idx);

/*!
 * Type of a given CompDb element.
 *
 * @param compdb_idx Element number (i.e. between 0 and size-1 of the CompDb)
 *
 * @returns a pointer to the type c string.
 */

const char * FAR CompDbType(fgc_db_t compdb_idx);

/*!
 * Human readable description of a given CompDb element.
 *
 * @param compdb_idx CompDb element number (i.e. between 0 and size-1 of the CompDB)
 *
 * @returns a pointer to the label c string.
 */

const char * FAR CompDbLabel(fgc_db_t compdb_idx);

/*!
 * Temperature property name for a given component of the CompDb.
 *
 * @param compdb_idx CompDb element number (i.e. between 0 and size-1 of the CompDB)
 *
 * @returns a pointer to the label c string.
 */

const char * FAR CompDbTempProperty(fgc_db_t compdb_idx);

/*!
 * Barcode property name for a given component of the CompDb.
 *
 * @param compdb_idx CompDb element number (i.e. between 0 and size-1 of the CompDB)
 *
 * @returns a pointer to the label c string.
 */

const char * FAR CompDbBarcodeProperty(fgc_db_t compdb_idx);

/*!
 * Group Membership Index for a given component on a given system. If the component belong to one of the system
 * component groups, then the function return a group_idx which can be found SysDbRequiredGroups(sysdb_idx).
 * The returned group index can be used in SysDbRequiredGroups to retrieve number of required components of
 * this group.
 *
 * @param compdb_idx CompDb element number (i.e. between 0 and size-1 of the CompDB)
 *
 * @param sysdb_idx SysDb element number (i.e. between 0 and size-1 of the SysDB)
 *
 * @returns an integer with the group index.
 * If the component is not required by system sysdb_idx, then 0 is returned (i.e. group_id==0
 * means that this component is not required for system sysdb_idx).
 *
 */

fgc_db_t CompDbGroupIndex(fgc_db_t compdb_idx, fgc_db_t sysdb_idx);

/*!
 * Lookup a barcode in CompDb
 *
 * @param[in]    type_code    Type code of the barcode to look up (13 characters)
 *
 * @returns      index to matching record in CompDb, or 0 if not found
 */

fgc_db_t CompDbLookup(const char *type_code);

// DimDB functions

/*!
 * Initializes the internal data structures for the Diagnosis Module DB (DimDB).
 *
 * @param dimdb address of the buffer containing DimDb encoded data
 *
 * @returns the encoding version number of the DimDb.
 * If the encoding version is not compatible, then -1 is returned.
 */

int32_t DimDbInit(void* FAR dimdb);

/*!
 * DimDB encoding version.
 *
 * @returns the encoding version number.
 */

fgc_db_t DimDbVersion(void);

/*!
 * Heap size of the DimDb.
 *
 * @returns the size of the DimDb Heap.
 */

fgc_db_t DimDbHeapSize(void);

/*!
 * Size of the DimDb.
 *
 * @returns the number of dim types in the DimDb.
 * If there is an error (e.g. not initialized), then -1 is returned.
 *
 */

fgc_db_t DimDbNumTypes(void);

/*!
 * Number of dim names in the DimDb
 *
 * @returns the number of dim names in the DimDb.
 *
 */

fgc_db_t DimDbNumNames(void);

/*!
 * Number of channels for a given DIM Type.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @returns an integer with the number of DIM channels.
 *
 */

fgc_db_t DimDbNumberOfChannels(fgc_db_t dimdb_type);

/*!
 * Is the channel Analog
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel index for a given DIM
 *
 * @return If the channel type is analog, then the function returns true.
 */

bool DimDbIsAnalogChannel(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Gain for a given analog channel of a DimDb Type. A DIM analog channel has a dynamic range between 0 and 5V.
 * Therefore the conversion between the input analog value and the digitized value is:
 * Analog Value = Gain*(DIM Channel) + Offset   [Units]
 * If the function is used on a digital channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @returns the gain to transform the DIM digitized value to its analog input.
 * If the channel is not used, then the default value 0 is returned.
 */

FP32 DimDbAnalogGain(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Offset for a given analog channel of a DimDb Type. A DIM analog channel has a dynamic range between 0 and 5V.
 * Therefore the conversion between the input analog value and the digitized value is:
 * Analog Value = Gain*(DIM Channel) + Offset   [Units]
 * If the function is used on a digital channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @returns the offset to transform the DIM digitized value to its analog input.
 * If the channel is not used, then the default value 0 is returned.
 */

FP32 DimDbAnalogOffset(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Units for a given analog channel of a DimDb Type. A DIM analog channel has a dynamic range between 0 and 5V.
 * Therefore the conversion between the input analog value and the digitized value is:
 * Analog Value = Gain*(DIM Channel) + Offset   [Units]
 * If the function is used on a digital channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @returns the c string with the units of the DIM analog input.
 * If the channel is not used, then the empty strign is returned.
 */

const char* FAR DimDbAnalogUnits(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Human readable label for a given Analog channel of a DimDb Type.
 * If the function is used on a digital channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @returns the c string with the label of the DIM analog input.
 * If the channel is not used, then the empty string is returned.
 */

const char * FAR DimDbAnalogLabel(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Is the channel Digital
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel index for a given DIM
 *
 * @return If the channel type is digital, then the function returns true.
 */

bool DimDbIsDigitalChannel(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Number of digital inputs for a given Digital channel input of a DimDb Type.
 * If the function is used on a analog channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @returns the number of inputs of the DIM digital channel (aka bank).
 */

fgc_db_t DimDbDigitalInputsLength(fgc_db_t dimdb_type, fgc_db_t ch_idx);

/*!
 * Is the digital input enabled?
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @param input_idx Input number (i.e. between 0 and number of inputs)
 *
 * @returns true if the digital input is enabled (i.e. label offset == 0).
 */

bool DimDbIsDigitalInput(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx);

/*!
 * Human readable label for a given Digital channel input of a DimDb Type.
 * If the function is used on a analog channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @param input_idx Input number (i.e. between 0 and number of inputs)
 *
 * @returns the c string with the label of the DIM analog input.
 * If the input is not used, then the empty string is returned.
 */

const char * FAR DimDbDigitalLabel(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx);

/*!
 * Human readable label for a the one value of the Digital channel input of a DimDb Type.
 * If the function is used on a analog channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @param input_idx Input number (i.e. between 0 and number of inputs)
 *
 * @returns the c string with the label of the one value for the DIM analog input.
 * If the input is not used, then the empty string is returned.
 */

const char * FAR DimDbDigitalLabelOne(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx);

/*!
 * Human readable label for a the zero value of the Digital channel input of a DimDb Type.
 * If the function is used on a analog channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @param input_idx Input number (i.e. between 0 and number of inputs)
 *
 * @returns the c string with the label of the zero value for the DIM analog input.
 * If the input is not used, then the empty string is returned.
 */

const char * FAR DimDbDigitalLabelZero(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx);

/*!
 * The function tells if the digital input is associated with a fault.
 * If the function is used on a analog channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @param input_idx Input number (i.e. between 0 and number of inputs)
 *
 * @returns true if the digital input for this channel represents a fault.
 */

bool DimDbDigitalIsFault(fgc_db_t dimdb_type, fgc_db_t ch_idx, fgc_db_t input_idx);


/*!
 * The function tells if the digital inputs are associated with any fault.
 * If the function is used on a analog channel then the behavior is undefined.
 *
 * @param dimdb_type DimDb element number (i.e. between 0 and size-1 of the DimDB)
 *
 * @param ch_idx Channel number (i.e. between 0 and number of channels for this DIM type)
 *
 * @returns the int representation of the bit mask for the all the faults in the digital channel.
 */
int16_t DimDbDigitalFaults(fgc_db_t dimdb_type, fgc_db_t ch_idx);



// IDDB functions

/*!
 * Initializes the internal data structures for the ID Database (IDDB).
 *
 * @param[in]     iddb       Address of the buffer containing IDDB encoded data
 *
 * @returns       0 on success, -1 on error
 */

int32_t IdDbInit(void *iddb);

/*!
 * Write a Dallas ID as a hexadecimal string
 *
 * @param[out]    buffer        Pointer to char buffer (minimum size 17 bytes)
 * @param[in]     id            Pointer to a Dallas ID
 */

void IdDbDallasIDString(char *buffer, const struct Dallas_id *id);

/*!
 * Check validity of a Dallas ID code.
 *
 * @param[in]     id         Pointer to a Dallas ID
 *
 * @returns       DLS_ID_OK if the ID is valid, otherwise an error code
 */

enum Dallas_id_error IdDbValidate(const struct Dallas_id *id);

/*!
 * Search for a Dallas ID code in the IDDB.
 *
 * @param[in]     id         Pointer to a Dallas ID
 * @param[out]    part_no    Part number to look up this Dallas ID in PTDB
 *
 * @returns       DLS_ID_OK if the ID is valid, otherwise an error code
 */

enum Dallas_id_error IdDbLookup(const struct Dallas_id *id, uint16_t *part_no);



// PTDB functions

/*!
 * Initializes the internal data structures for the Part Type Database (PTDB).
 *
 * @param[in]     ptdb    address of the buffer containing PTDB encoded data
 *
 * @returns       0 on success, -1 on error
 */

int32_t PtDbInit(void *ptdb);

/*!
 * Search for a part number in the PTDB.
 *
 * @param[out]    part_no    Part number to look up
 *
 * @returns       index into the PTDB, or -1 if not found
 */

int32_t PtDbLookup(uint16_t part_no);

/*!
 * Return the barcode for a part in PTDB
 *
 * @param[in]     ptdb_idx      Index of PTDB record
 *
 * @returns       Pointer to type_code field for the selected record
 */

const char *PtDbTypeCode(uint32_t ptdb_idx);

/*!
 * Return the barcode for a part in PTDB
 *
 * @param[out]    buffer        Pointer to char buffer (minimum size 20 bytes)
 * @param[in]     part_no       Part number
 * @param[in]     ptdb_idx      Index of PTDB record for part_no
 */

void PtDbBarcodeString(char *buffer, uint16_t part_no, uint32_t ptdb_idx);

#ifdef __cplusplus
}
#endif

#endif

// EOF
