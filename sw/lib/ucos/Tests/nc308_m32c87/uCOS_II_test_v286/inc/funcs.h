/*---------------------------------------------------------------------------------------------------------*\
  File:		funcs.h

  Contents:	this file declares all the functions contained in the program

  Notes:	Function declarations are grouped by source file.

  History:

    20 apr 03	qak	Created
    25 Jan 05	doc	update for uCOS II v2.86

\*---------------------------------------------------------------------------------------------------------*/

/*---- main.c functions -----*/


/*---- task0.c functions ----*/

void	StartSystemTick		(void);
void	TaskStartFunc		(void* data);

/*---- task1.c functions ----*/

void	Task1Func		(void* data);

/*---- task2.c functions ----*/

void	Task2Func		(void* data);

/*---- task3.c functions ----*/

void	Task3Func		(void* data);


/*---- uart.c functions -----*/

void 	Uart_Init		(void);
void	UartWaitAndOutChar	(INT8U byte);
void	UartOutString		(PCHAR c);
INT8U	UartWaitForChar		(void);

/*---- flash.c functions -----*/

void	FlashCopiedFunc		(void);
void	FlashFunc		(void);
void	FlashPutBurningCodeIntoRam(void);
void	FlashAdaptCpuSpeed	(void);
void	FlashCpuRestore		(void);
INT16U	FlashErase		(INT32U erase_address);
INT16U	FlashWrite		(INT32U target_address, INT32U data_buf, INT16U buf_size);
INT16U	FlashVariables		(INT32U target_address, INT32U data_buf, INT16U buf_size);
void	RecallFlashedVariables	(INT32U source_address, INT32U data_buf, INT16U buf_size);

/*---- Isr.c functions -----*/

void	IsrDummy1		(void);
void	IsrDummy2		(void);
void	IsrOverFlow		(void);
void	IsrForWatchDog		(void);
void	IsrTickT1		(void);
void	IsrTickT2		(void);


/*---- hdw_c87.c functions -----*/

void    InitHdwM32C87		(void);
void 	M32C87_EnableInts	(void);

/*---------------------------------------------------------------------------------------------------------*\
  End of file: funcs.h
\*---------------------------------------------------------------------------------------------------------*/

