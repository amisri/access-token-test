-silent
-o test
-dir lst
-g
-O1
-v
-M82
-Wall
-Wno_stop
-Wstdout	
-as308 "-LDIMS"
-ln308 "-MSL -NOSTOP -T"

-Iinc
-Ic:\mtool\Tools\Renesas\nc308wa\v541r01\inc308
-Isrc_os
-Isrc_os_m32c87

-lc:\mtool\Tools\Renesas\nc308wa\v541r01\lib308\nc308lib

src\\start.a30
src\\main.c
src\\hdw_c87.c
src\\isr.c
src\\task0.c
src\\task1.c
src\\task2.c
src\\task3.c
src\\uart.c

src_os_m32c87\\os_cpu_a.a30
src_os_m32c87\\os_cpu_c.c
src_os_m32c87\\os_dbg.c

src_os\\ucos_ii.c
