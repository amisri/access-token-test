/*---------------------------------------------------------------------------------------------------------*\
  File:		vars.h

  Contents:	this file declares/defines all the global structures and variables used
		by the program.

  History:

    20/04/03	qak	Created
\*---------------------------------------------------------------------------------------------------------*/

// EXT dependes on the GLOBAL definition

/*
**************************************************************************************************************
*                                                GLOBALS
**************************************************************************************************************
*/

EXT OS_STK          gTaskStartStk[APP_TASK_START_STK_SIZE];            /* Tasks stacks */


struct TTask1Vars
{
    OS_STK	stk[APP_TASK_T1_STK_SIZE];
    OS_EVENT *	sem_trig;
    INT8U	xx;
};

EXT struct TTask1Vars	gTask1
#ifdef GLOBALS
= {
    {0},	// stk
    0,		// sem_trig
    2,		// xx
}
#endif
;


EXT OS_STK          gTask2Stk[APP_TASK_T2_STK_SIZE];
EXT OS_STK          gTask3Stk[APP_TASK_T3_STK_SIZE];

/************************************************************************/
/* Semaphores for reception and transmission                            */
/************************************************************************/

EXT OS_EVENT*	gRxSem
#ifdef GLOBALS
= NULL
#endif
;

EXT OS_EVENT*	gTxSem
#ifdef GLOBALS
= NULL
#endif
;

/*---------------------------------------------------------------------------------------------------------*\
  End of file: vars.h
\*---------------------------------------------------------------------------------------------------------*/

