/*---------------------------------------------------------------------------------------------------------*\
  File:		main.h

  Contents:	FGC tester program - this is the principle header file for a tester program

  Notes:	The constant GLOBALS should only be declared in main.c

  History:

    20/04/03	qak	Created
\*---------------------------------------------------------------------------------------------------------*/
#define DEBUG_VERSION	// to disable FLASH writing and other things that hangs the debugger

#define NOT_USE_WATCHDOG
/* the watchdog is not yet operational, I need to measure the times involved and put the watchdog reset
   in the proper places */

#ifdef DEBUG_VERSION
#define NOT_STORE_DATA_IN_FLASH
/* when debugging, the debugger hangs when try to store information in the FLASH ROM. This because the
   procedure frezzes all the FLASH also the code block used by the debugger
*/
#else
#endif


/*--- Prepare EXT to define global variables only once ---*/

#ifdef GLOBALS
#define EXT
#else
#define EXT extern
#endif

/*----- Include all other header files -----*/

#include <stdarg.h>
#include <stdio.h>		// ANSI C header files
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sfr32c87.h>		// M32C/87 PQ144 register declarations

#include <consts.h>		// Global constants

#include <ucos_ii.h>		// uCOS II

#include <funcs.h>		// Functions
#include <vars.h>		// Global Variables

/*----- DAC special definitions to prevent errors -----*/

#ifdef __DAC__
#define	__BUILTIN_strcpy(s1,s2)
#define	__BUILTIN_memcpy(s1,s2,n)
#define	__BUILTIN_strcmp(s1,s2)		(0)
#define __BUILTIN_memset(s,c,n)
#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
