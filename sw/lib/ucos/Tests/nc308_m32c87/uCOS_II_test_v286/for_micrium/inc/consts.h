/*---------------------------------------------------------------------------------------------------------*\
  File:		consts.h

  Contents:	this file defines the global constants

  History:

    20 apr 03	qak	Created
\*---------------------------------------------------------------------------------------------------------*/

// FALSE = 0, TRUE = 1

#define	TAB			0x09
#define	LF			0x0A
#define	CR			0x0D

// the code is stored from USER_ADR = 0fc0000h onwards as defined at start.a30
// this is Chunk 6
#define ROM_ADDR_STORED_VARS	0x00FF8000
// we store the flashable variables in Chunk 2 (8Kb)
// WARNING !!!! check that the code don't overlap the Chunk 2

#define DATA_IN_FLASH_SIGNATURE	0x7557

#define	POINTER_ANY_MEM		far *		// this pointer can acces all the memory, is the default
#define	POINTER_PAGE0		near *		// this pointer can acces only the first 64Kb of memory

#define	VAR_ANY_MEM		far 		// this variable goes in any place of the memory, not restricted to the first 64Kb
#define	VAR_PAGE0		near		// this variable goes only in the first 64Kb of memory, is the default

// * Variables ............................... near attribute, resides in the first 64Kb of memory, our RAM
// * const-qualified constants ............... far attribute
// * Functions ................................far attribute

/*------------------------------------*/

#define	SET_B0	0x01	// 0000 0001
#define	SET_B1	0x02	// 0000 0010
#define	SET_B2	0x04	// 0000 0100
#define	SET_B3	0x08	// 0000 1000
#define	SET_B4	0x10	// 0001 0000
#define	SET_B5	0x20	// 0010 0000
#define	SET_B6	0x40	// 0100 0000
#define	SET_B7	0x80	// 1000 0000

#define	CLR_B0	0xFE	// 1111 1110
#define	CLR_B1	0xFD	// 1111 1101
#define	CLR_B2	0xFB	// 1111 1011
#define	CLR_B3	0xF7	// 1111 0111
#define	CLR_B4	0xEF	// 1110 1111
#define	CLR_B5	0xDF	// 1101 1111
#define	CLR_B6	0xBF	// 1011 1111
#define	CLR_B7	0x7F	// 0111 1111

#define	SET_B8	0x0100	// 0000 0001  0000 0000
#define	SET_B9	0x0200	// 0000 0010  0000 0000
#define	SET_B10	0x0400	// 0000 0100  0000 0000
#define	SET_B11	0x0800	// 0000 1000  0000 0000
#define	SET_B12	0x1000	// 0001 0000  0000 0000
#define	SET_B13	0x2000	// 0010 0000  0000 0000
#define	SET_B14	0x4000	// 0100 0000  0000 0000
#define	SET_B15	0x8000	// 1000 0000  0000 0000

#define	CLR_B8	0xFEFF	// 1111 1110  1111 1111
#define	CLR_B9	0xFDFF	// 1111 1101  1111 1111
#define	CLR_B10	0xFBFF	// 1111 1011  1111 1111
#define	CLR_B11	0xF7FF	// 1111 0111  1111 1111
#define	CLR_B12	0xEFFF	// 1110 1111  1111 1111
#define	CLR_B13	0xDFFF	// 1101 1111  1111 1111
#define	CLR_B14	0xBFFF	// 1011 1111  1111 1111
#define	CLR_B15	0x7FFF	// 0111 1111  1111 1111

/*---------------------------------------------------------------------------------------------------------*\
  End of file: consts.h
\*---------------------------------------------------------------------------------------------------------*/

