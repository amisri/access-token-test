REM -----------------------------------------------------------------------------------------------------------
REM
REM usage: make.bat
REM
REM -----------------------------------------------------------------------------------------------------------

ECHO (V1.0) Making M32C87 test
ECHO

SET BIN308=C:\mtool\Tools\Renesas\nc308wa\v541r01\BIN
SET INC308=C:\mtool\Tools\Renesas\nc308wa\v541r01\INC308
SET LIB308=C:\mtool\Tools\Renesas\nc308wa\v541r01\LIB308
SET TMP308=C:\mtool\Tools\Renesas\nc308wa\v541r01\TMP

SET SCRIPT_FILE=nc308.cmd

SET OUT_FILE=TEST

REM ----- Delete old files -----

ECHO Deleting old files...

del %OUT_FILE%.out
del *.x30
del *.map
del *.mot
del .\LST\*.lst

REM ----- Compile and link -----

ECHO Compiling and linking

c:\mtool\Tools\Renesas\nc308wa\v541r01\BIN\nc308 @%SCRIPT_FILE% >%OUT_FILE%.out

REM ----- Generate motorola S-records file -----

ECHO Generating motorola S-records file

c:\mtool\Tools\Renesas\nc308wa\v541r01\BIN\lmc308 -. -ID#0000000 %OUT_FILE%

REM -- Move listing files to lst directory --

copy .\SRC\*.lst .\LST\*.*
copy .\SRC_OS_M32C87\*.lst .\LST\*.*

del .\SRC\*.lst 
del .\SRC_OS_M32C87\*.lst 

ECHO End.....................................
