/*---------------------------------------------------------------------------------------------------------*\
 File:		task2.c

 Purpose:	task 2

 Author:	Hubert Kronenberg (HK)

 History:
    16 sep 03   hk      Created
    25 Jan 05	doc	update for uCOS II v2.86

\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void Task2Func(void *data)
/*---------------------------------------------------------------------------------------------------------*\
    Send '#' every 5.0 sec
\*---------------------------------------------------------------------------------------------------------*/
{

  for (;;) // loop for ever
  {

    OSTimeDly(5000);
    UartWaitAndOutChar('#');

  }

}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: task2.c
\*---------------------------------------------------------------------------------------------------------*/

