/*---------------------------------------------------------------------------------------------------------*\
 File:		isr.c

 Purpose:	Interrupt Service Routines

 Author:	Daniel Calcoen

 History:
    25 Jan 05	doc	Created, Calcoen Daniel, AB/PO/CC

\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*

   with "#pragma INTERRUPT [/B] IsrName" signal to the compiler that the functions is an interrupt so
   put and REIT instead of a RET at its end

   the compiler automatically generates the push of the "used" register in the ISR function so are not
   always all the registers
   the solution I found for this is usign /B parameter

   /B directs the compiler to switch to bank 1 registers
   so it adds ath the beginig the XXXX instruction instead of the automatic PUSHM generated to save
   the used registers by the compiler
   I force back to bank 0 and then I can add my PUSHM of ALL_REGISTERS

   Optional

   /E Multiple interrupts are enabled immediately after entering the interrupt.
      (if you allow interrupt interrupting ISRs)

*/


/*---------------------------------------------------------------------------------------------------------*/
void  SoftwareReset(void)
/*---------------------------------------------------------------------------------------------------------*\
  software reset
\*---------------------------------------------------------------------------------------------------------*/
{
    prc1 = 1; // enable write to protected register, bit1 unlock PM0, PM1, INVC0, INVC1
    pm03 = 1; // Setting Processor mode register (PM0), Software reset bit
}
/*---------------------------------------------------------------------------------------------------------*/

#pragma INTERRUPT /B IsrDummy1

/*---------------------------------------------------------------------------------------------------------*/
void IsrDummy1(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for not used interrupts
\*---------------------------------------------------------------------------------------------------------*/
{
//  dbg.int_dummy1_counter++;
//  SoftwareReset();
}
/*---------------------------------------------------------------------------------------------------------*/

#pragma INTERRUPT /B IsrDummy2

/*---------------------------------------------------------------------------------------------------------*/
void IsrDummy2(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for not used interrupts

    Undefined instruction
    Overflow
    BRK (can be redirected to int 0)
    Address Match
    Single Step, reserved
    DBC, reserved
    NMI

\*---------------------------------------------------------------------------------------------------------*/
{
//  dbg.int_dummy2_counter++;
//  SoftwareReset();
}
/*---------------------------------------------------------------------------------------------------------*/

#pragma INTERRUPT /B IsrForWatchDog

/*---------------------------------------------------------------------------------------------------------*/
void IsrForWatchDog(void)
/*---------------------------------------------------------------------------------------------------------*\
    writing to -> wdts_addr sets the watchdog timer to 7FFF, independant of the written value
\*---------------------------------------------------------------------------------------------------------*/
{
//  dbg.int_dummy2_counter++;
//  SoftwareReset();
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma INTERRUPT /B Isr_UART0_rx	// (vect=18) declare the vector needs to service for UART0 Reception, ACK
/*---------------------------------------------------------------------------------------------------------*/
void Isr_UART0_rx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for UART0 Reception.
  is mapped to int 18 of the M32C87 in the start.a30 file
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_START_ISR(Isr_UART0_rx);		// save all registers and executes OSIntEnter()

    /*
	u0rb : UART Receive buffer Register, default value after reset = xxxx xxxx, xxxx xxxx
		u0rbh	036Fh
			    b7 sum_u0rb, Error Sum Flag
			    b6 per_u0rb, Parity Error Flag
			    b5 fer_u0rb, Framing Error Flag
			    b4 oer_u0rb, Overrun Error Flag
			    b3 abt_u0rb, Arbitration Lost Detect Flag
			    b2 - 0
			    b1 - 0
			    b0 - received data D8
		u0rbl	036Eh
			    b7 - b0, received data D0-D7
    */

//  u0rbl is the received character

    OSSemPost(gRxSem);

    OS_END_ISR();		// restore previously saved registers and call OSIntExit()
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma INTERRUPT /B Isr_UART0_tx	// (vect=17) declare the vector needs to service for UART0 Transmission, NACK
/*---------------------------------------------------------------------------------------------------------*/
void Isr_UART0_tx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for UART0 Transmission.
  is mapped to int 17 of the M32C87 in the start.a30 file
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_START_ISR(Isr_UART0_tx);		// save all registers and executes OSIntEnter()

//  te_u0c1 = 0;			// disable transmission

    /*
	u0tb : UART Transmit buffer Register, default value after reset = xxxx xxxx, xxxx xxxx
	       Use "MOV" instruction when writing to this register.
		u0tbh	036Bh
			    b7 - 0
			    b6 - 0
			    b5 - 0
			    b4 - 0
			    b3 - 0
			    b2 - 0
			    b1 - 0
			    b0 - D8 to transmit
		u0tbl	036Ah
			    b7 - b0, D0-D7 data to transmit
    */

//  u0tbl = char to transmit;

    OSSemPost(gTxSem);

    OS_END_ISR();		// restore previously saved registers and call OSIntExit()
}
/*---------------------------------------------------------------------------------------------------------*/
#ifdef DEBUG_VERSION
// not use this interrupts
#else

#pragma INTERRUPT /B Isr_UART1_rx	// (vect=20) declare the vector needs to service for UART1 Reception, ACK
/*---------------------------------------------------------------------------------------------------------*/
void Isr_UART1_rx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for UART1 Reception.
  is mapped to int 20 of the M32C87 in the start.a30 file
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_START_ISR(Isr_UART1_rx);		// save all registers and executes OSIntEnter()

    /*
	u1rb : UART1 Receive buffer Register, default value after reset = xxxx xxxx, xxxx xxxx
		u1rbh	02EFh
			    b7 sum_u1rb, Error Sum Flag
			    b6 per_u1rb, Parity Error Flag
			    b5 fer_u1rb, Framing Error Flag
			    b4 oer_u1rb, Overrun Error Flag
			    b3 abt_u1rb, Arbitration Lost Detect Flag
			    b2 - 0
			    b1 - 0
			    b0 - received data D8
		u1rbl	02EEh
			    b7 - b0, received data D0-D7
    */

//  u1rbl is the received character

    OSSemPost(gRxSem);

    OS_END_ISR();		// restore previously saved registers and call OSIntExit()
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma INTERRUPT /B Isr_UART1_tx	// (vect=19) declare the vector needs to service for UART1 Transmission, NACK
/*---------------------------------------------------------------------------------------------------------*/
void Isr_UART1_tx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for UART1 Transmission.
  is mapped to int 19 of the M32C87 in the start.a30 file
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_START_ISR(Isr_UART1_tx);		// save all registers and executes OSIntEnter()

//   te_u1c1 = 0;	// disable UART1 transmission, u1c1 02EDh UART1 transmit/receive control register 1

    /*
	u1tb : UART1 Transmit buffer Register 16 bit, default value after reset = xxxx xxxx, xxxx xxxx
	       Use "MOV" instruction when writing to this register.
		u1tbh	02EBh - UART1 Transmit buffer register high 8 bit
			    b7 - 0
			    b6 - 0
			    b5 - 0
			    b4 - 0
			    b3 - 0
			    b2 - 0
			    b1 - 0
			    b0 - D8 to transmit
		u1tbl	02EAh - UART1 Transmit buffer register low  8 bit
			    b7 - b0, D0-D7 data to transmit
    */

//  u1tbl = char to transmit;

    OSSemPost(gTxSem);

    OS_END_ISR();		// restore previously saved registers and call OSIntExit()
}
/*---------------------------------------------------------------------------------------------------------*/
#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/

