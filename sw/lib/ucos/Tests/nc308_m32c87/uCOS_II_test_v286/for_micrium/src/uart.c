/*---------------------------------------------------------------------------------------------------------*\
 File:		uart.c

 Purpose:	Uart functions

 Author:	Hubert Kronenberg (HK)

 History:
    16 sep 03   hk      Created
    25 Jan 05	doc	update for uCOS II v2.86

\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void Uart_Init(void)
/*---------------------------------------------------------------------------------------------------------*\
    initialize UART
\*---------------------------------------------------------------------------------------------------------*/
{
    re_u0c1 = 1; // Receive Enable Bit, 1 = Receive Enable
    te_u0c1 = 1; // Transmit Enable Bit, 1 = Transmite enabled

//  re_u1c1 = 1; // Receive Enable Bit, 1 = Receive Enable
//  te_u1c1 = 1; // Transmit Enable Bit, 1 = Transmite enabled

    re_u2c1 = 1; // Receive Enable Bit, 1 = Receive enabled
    te_u2c1 = 1; // Transmit Enable Bit, 1 = Transmite enabled

    gRxSem = OSSemCreate(0);
    gTxSem = OSSemCreate(1);
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U UartWaitForChar(void)
/*---------------------------------------------------------------------------------------------------------*\
    receive character
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err;

    OSSemPend(gRxSem, 0, &err);

    /*
	u0rb : UART Receive buffer Register, default value after reset = xxxx xxxx, xxxx xxxx
		u0rbh	036Fh
			    b7 sum_u0rb, Error Sum Flag
			    b6 per_u0rb, Parity Error Flag
			    b5 fer_u0rb, Framing Error Flag
			    b4 oer_u0rb, Overrun Error Flag
			    b3 abt_u0rb, Arbitration Lost Detect Flag
			    b2 - 0
			    b1 - 0
			    b0 - received data D8
		u0rbl	036Eh
			    b7 - b0, received data D0-D7
    */

//  u0rbl is the received character

    return(u0rbl);
//  return(u1rbl);
}
/*---------------------------------------------------------------------------------------------------------*/
void  UartOutString(PCHAR c)
/*---------------------------------------------------------------------------------------------------------*\
    send string
\*---------------------------------------------------------------------------------------------------------*/
{
    while (*c)
    {
	UartWaitAndOutChar(*c++);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void UartWaitAndOutChar(INT8U byte)
/*---------------------------------------------------------------------------------------------------------*\
    send character
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err;

    OSSemPend(gTxSem, 0, &err);

    /*
	u1tb : UART1 Transmit buffer Register 16 bit, default value after reset = xxxx xxxx, xxxx xxxx
	       Use "MOV" instruction when writing to this register.
		u1tbh	02EBh - UART1 Transmit buffer register high 8 bit
			    b7 - 0
			    b6 - 0
			    b5 - 0
			    b4 - 0
			    b3 - 0
			    b2 - 0
			    b1 - 0
			    b0 - D8 to transmit
		u1tbl	02EAh - UART1 Transmit buffer register low  8 bit
			    b7 - b0, D0-D7 data to transmit
    */

//  u1tbl = char to transmit;

    u0tbl = byte;
//  u1tbl = byte;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: uart.c
\*---------------------------------------------------------------------------------------------------------*/

