/*---------------------------------------------------------------------------------------------------------*\
 File:		task1.c

 Purpose:	task 1

 Author:	Hubert Kronenberg (HK)

 History:
    16 sep 03   hk      Created
    25 Jan 05	doc	update for uCOS II v2.86

\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void Task1Func(void *data)
/*---------------------------------------------------------------------------------------------------------*\
    Initialize UART
    Create Task2 and Task3
    Send '%' all 0.5 sec
\*---------------------------------------------------------------------------------------------------------*/
{
  Uart_Init();

  OSTaskCreate(Task2Func,NULL,&gTask2Stk[APP_TASK_T2_STK_SIZE - 1],APP_TASK_T2_PRIO);
  OSTaskCreate(Task3Func,NULL,&gTask3Stk[APP_TASK_T3_STK_SIZE - 1],APP_TASK_T3_PRIO);

  UartOutString("\r\nCPU: M32C\r\nCompiler: NC308\r\nMicroC/OS-II V");
  UartWaitAndOutChar((OS_VERSION/100)%10+'0');
  UartWaitAndOutChar('.');
  UartWaitAndOutChar((OS_VERSION/10)%10+'0');
  UartWaitAndOutChar((OS_VERSION)%10+'0');
  UartOutString(" -- Critical Method ");
  UartWaitAndOutChar((OS_CRITICAL_METHOD)%10+'0');
  UartOutString("\r\n");

  for (;;) // loop for ever
  {
    OSTimeDly(500);
    UartWaitAndOutChar('%');
  }

}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: task1.c
\*---------------------------------------------------------------------------------------------------------*/

