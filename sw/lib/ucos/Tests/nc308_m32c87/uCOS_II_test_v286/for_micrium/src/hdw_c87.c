/*---------------------------------------------------------------------------------------------------------*\
 File:		hdw_c87.c

 Purpose:	M32C87 hardware related stuff

 Author:	Daniel Calcoen

 History:
    11 Dec 07	doc	Created
\*---------------------------------------------------------------------------------------------------------*/

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void InitHdwM32C87(void)
/*---------------------------------------------------------------------------------------------------------*\
  set the internal peripherals of M32C87
\*---------------------------------------------------------------------------------------------------------*/
{

/*
    prcr 000Ah Protect Register, default value after reset = xxxx 0000

    b7,
    b6,
    b5,
    b4,
    b3, prc3    lock/unlock PLV, VDC0
    b2, prc2 	lock/unlock PD9, PS3 (this bit has automatic lock after write)
    b1, prc1    lock/unlock PM0, PM1, INVC0, INVC1
    b0, prc0    lock/unlock CM0, CM1, CM2, MCD, PLC0, PLC1

    mcd 000Ch Main Clock Decode Register, default value after reset = xxx0 1000

    mcd = 0x00,   CLK divided by 16
    mcd = 0x0E,   CLK divided by 14
    mcd = 0x0C,   CLK divided by 12
    mcd = 0x0A,   CLK divided by 10
    mcd = 0x08,   CLK divided by  8
    mcd = 0x06,   CLK divided by  6
    mcd = 0x04,   CLK divided by  4
    mcd = 0x03,   CLK divided by  3
    mcd = 0x02,   CLK divided by  2
    mcd = 0x12,   CLK divided by  1 (no division)

    cm0 0006h System Clock Control Register 0, default value after reset = 0000 x000

    b7, cm07 System clock select bit (Xin-Xout or Xcin-Xcout)
	    	0 : selects Xin - Xout
	    	1 : selects XCin - XCout
    b6, cm06 WatchDog Timer function select bit
    		Note : Once the CM06 bit is set to "1", it cannot be set "0" by program
	    	0 : only generates Watchdog timer interrupt (not resets the CPU)
	    	1 : Reset CPU when watchdog counter overflows
    b5, cm05 Main clock stop bit
	    	0 : Main clock oscillates
	    	1 : Main clock stops
    b4, cm04 Port Xc select bit
	    	0 : I/O port function
	    	1 : XCin - XCout oscillation function
    b3, cm03 reserved
	    	1
    b2, cm02 WAIT peripheral function clock stop bit
	    	0 : peripheral clock does not stop in wait mode
	    	1 : peripheral clock stop in wait mode
    b1 b0, cm01 cm00 Clock output function select bit
	    	00 :   I/O port P5
	    	01 :   outputs fc
	    	10 :   outputs f8
	    	11 :   outputs f32

*/

    /*--- Clock Divider ---*/

    prc0 = 1;	// unlock CM0, CM1, CM2, MCD, PLC0, PLC1 registers
//  mcd	= 0x12;	// set main clock division to NO DIVISION, done in the start.a30 file

#ifdef NOT_USE_WATCHDOG
    cm06 = 0x00;	// only generates Watchdog timer interrupt (not resets the CPU), default value after reset
#else
    cm06 = 0x01;	// watchdog resets the CPU
#endif

    prc0 = 0;	// lock again

/*
    the M32C87 clock is 24MHz

    Watchdog Timer counts up to 7FFF

    24MHz clock, with prescaler at 16  : cycle about  21.854 ms
		 with prescaler at 128 : cycle about 174.762 ms

    pm22 of pm2 (Process Mode Register 2) selects the clock source for the watchdog counter

    wdts 0000Eh Watchdog Timer Register, default value after reset = xxxx xxxx

             writing to wdts sets the watchdog timer to 7FFF, independant of the written value


    wdc 000Fh Watchdog Timer Control Register, default value after reset = 00xx xxxx

    b7, wdc7 Prescaler select bit (for the WatchDog Timer)
	    	0 : prescaler divide-by 16
	    	1 : prescaler divide-by 128
    b6, reserved set to 0
    b5, wdc5, 0 : do a cold restart, 1 : do a warm restart
    b4, read only, watchdog counter b4
    b3, read only, watchdog counter b3
    b2, read only, watchdog counter b2
    b1, read only, watchdog counter b1
    b0, read only, watchdog counter b0
*/

    wdc7 = 1;       // prescaler divide-by 128
    // -------------------------------------------------------------------------------------------------------
    /*--- External memory ---*/

/*
    ewcr0 0048h External space wait control register 0, default value after reset = x0x0 0011
    area of #CS1

    b7,         Nothing is assigned
    b6, ewcr006 Recovery cycle addition select bit
    b5,         Nothing is assigned
    b4, ewcr004 Bus cycle select bit
    b3, ewcr003 Bus cycle select bit
    b2, ewcr002 Bus cycle select bit
    b1, ewcr001 Bus cycle select bit
    b0, ewcr000 Bus cycle select bit

    ewcr1 0049h External space wait control register 1, default value after reset = x0x0 0011
    area of #CS2

    b7,         Nothing is assigned
    b6, ewcr106 Recovery cycle addition select bit
    b5,         Nothing is assigned
    b4, ewcr104 Bus cycle select bit
    b3, ewcr103 Bus cycle select bit
    b2, ewcr102 Bus cycle select bit
    b1, ewcr101 Bus cycle select bit
    b0, ewcr100 Bus cycle select bit

    ewcr2 004Ah External space wait control register 2, default value after reset = x0x0 0011
    area of #CS3

    b7,         Nothing is assigned
    b6, ewcr206 Recovery cycle addition select bit
    b5,         Nothing is assigned
    b4, ewcr204 Bus cycle select bit
    b3, ewcr203 Bus cycle select bit
    b2, ewcr202 Bus cycle select bit
    b1, ewcr201 Bus cycle select bit
    b0, ewcr200 Bus cycle select bit

    ewcr3 004Bh External space wait control register 3, default value after reset = x0x0 0011
    area of #CS0, this is the one we use

    b7,         Nothing is assigned
    b6, ewcr306 Recovery cycle addition select bit, 0 = Insert no recovery cycle when accessing external space
    b5,         Nothing is assigned
    b4, ewcr304 Bus cycle select bit
    b3, ewcr303 Bus cycle select bit
    b2, ewcr302 Bus cycle select bit
    b1, ewcr301 Bus cycle select bit
    b0, ewcr300 Bus cycle select bit

                b4 b3 b2 b1 b0                  separate bus
		 0  0  0  0  0     --    --
		 0  0  0  0  1     1 a + 1 b    2 BClk cycles
		 0  0  0  1  0     1 a + 2 b    3 BClk cycles  <- we use this
		 0  0  0  1  1     1 a + 3 b    4 BClk cycles  <- default
		 0  0  1  0  0     1 a + 4 b    5 BClk cycles
		 0  0  1  0  1     1 a + 5 b    6 BClk cycles
		 0  0  1  1  0     1 a + 6 b    7 BClk cycles
		 0  0  1  1  1     --    --
		 0  1  0  0  0     --    --
		 0  1  0  0  1     --    --
		 0  1  0  1  0     2 a + 2 b    4 BClk cycles
		 0  1  0  1  1     2 a + 3 b    5 BClk cycles
		 0  1  1  0  0     2 a + 4 b    6 BClk cycles
		 0  1  1  0  1     2 a + 5 b
		 0  1  1  1  0     --    --
		 0  1  1  1  1     --    --
		 1  0  0  0  0     --    --
		 1  0  0  0  1     --    --
		 1  0  0  1  0     --    --
		 1  0  0  1  1     3 a + 3 b    6 BClk cycles
		 1  0  1  0  0     3 a + 4 b    7 BClk cycles
		 1  0  1  0  1     3 a + 5 b
		 1  0  1  1  0     3 a + 6 b    9 BClk cycles
		 1  0  1  1  1     --    --
		 1  1  0  0  0     --    --
		 1  1  0  0  1     --    --
		 1  1  0  1  0     --    --
		 1  1  0  1  1     --    --
		 1  1  1  0  0     --    --
		 1  1  1  0  1     --    --
		 1  1  1  1  0     --    --
		 1  1  1  1  1     --    --
*/
    /* Bus timing for external memory space */

    ewcr0 = 0x01; // SRAM   - 1+1 cycles, no recovery cycle
    ewcr1 = 0x01; // MRAM   - 1+1 cycles, no recovery cycle
    ewcr2 = 0x02; // PERIPH - 1+2 cycles, no recovery cycle
    ewcr3 = 0x0D; // uFIP   - 2+5 cycles, no recovery cycle

/*
    ds 000Bh Outer Data-bus Width Register, default value after reset = xxxx ?000

    b7
    b6
    b5
    b4
    b3, ds3 1 = External space 3 is 16 bit wide, 0 = 8 bit wide
    b2, ds2 1 = External space 2 is 16 bit wide, 0 = 8 bit wide
    b1, ds1 1 = External space 1 is 16 bit wide, 0 = 8 bit wide
    b0, ds0 1 = External space 0 is 16 bit wide, 0 = 8 bit wide

    The DATA BUS in the external space 3, after reset,
    becomes 16 bits wide when an "L" signal is applied to the BYTE pin
             8 bits wide when an "H" signal is applied.

    bits PM05 and PM04 selects separete or multiplexed bus

    bits PM11 and PM10 selects number of chip select lines
*/
    ds = 0x07; // Bus 16bit wide for external spaces 0-2, 8bit for ext space 3
    // -------------------------------------------------------------------------------------------------------

/*  DAC 0 - analogue board heater
    DAC 1 - external heater

    DACON 039Ch D/A Control Register, default value after reset = xxxx xx00

    b7,
    b6,
    b5,
    b4,
    b3,
    b2,
    b1, da1e = 1 (enable DAC output and disable pull-up of the output)
    b0, da0e = 1 (enable DAC output and disable pull-up of the output)

    DA0 0398h D/A Register 0, default value after reset = 0xXXXX
    DA1 039Ah D/A Register 1, default value after reset = 0xXXXX

    the output voltage is given by Vout = (Vref * Reg) / 256


    ps3 03B5h Port 9 Output Function Select Register A3, default value after reset = 0000 0000 (to write it needs enabling the protection bit prc2)

    b7, ps3_7 -> Pin P97, 0 = I/O port/peripheral function input, 1 = (specified by psl3_7)
    b6, ps3_6 -> Pin P96, 0 = I/O port/peripheral function input, 1 = (specified by psl3_6)
    b5, ps3_5 -> Pin P95, 0 = I/O port/peripheral function input, 1 = CLK4 (out)
    b4, ps3_4 -> Pin P94, 0 = I/O port/peripheral function input, 1 = #RTS4
    b3, ps3_3 -> Pin P93, 0 = I/O port/peripheral function input, 1 = #RTS3
    b2, ps3_2 -> Pin P92, 0 = I/O port/peripheral function input, 1 = (specified by psl3_2)
    b1, ps3_1 -> Pin P91, 0 = I/O port/peripheral function input, 1 = (specified by psl3_1)
    b0, ps3_0 -> Pin P90, 0 = I/O port/peripheral function input, 1 = (specified by psl3_0)


    pd9 03C7h Port P9 Direction Register, default value after reset = 0000 0000 (to write it needs enabling the protection bit prc2)

    b7, pd9_7 -> Pin P97, 0 : Input          1 : Output
    b6, pd9_6 -> Pin P96, 0 : Input          1 : Output
    b5, pd9_5 -> Pin P95, 0 : Input          1 : Output
    b4, pd9_4 -> Pin P94, 0 : Input          1 : Output (use 0 = input to use the DAC (!))
    b3, pd9_3 -> Pin P93, 0 : Input          1 : Output (use 0 = input to use the DAC (!))
    b2, pd9_2 -> Pin P92, 0 : Input          1 : Output
    b1, pd9_1 -> Pin P91, 0 : Input          1 : Output
    b0, pd9_0 -> Pin P90, 0 : Input          1 : Output

    psl3 03B7h Port 9 Output Function Select Register B3, default value after reset = 0000 0000

    b7, psl3_7, Port P97, 0 : SCL4 (out), 1 : STxD4
    b6, psl3_6, Port P96, 0 : peripheral function input except ANEX1, 1 : ANEX1
    b5, psl3_5, Port P95, 0 : peripheral function input except ANEX0, 1 : ANEX0
    b4, psl3_4, Port P94, 0 : peripheral function input, 1 : DA1
    b3, psl3_3, Port P93, 0 : peripheral function input, 1 : DA0
    b2, psl3_2, Port P92, 0 : TxD3/SDA3 (out), 1 : OUTC2_0/ISTxD2/IEout
    b1, psl3_1, Port P91, 0 : SCL3 (out), 1 : STxD3
    b0, psl3_0, Port P90, 0 : CLK3 (out), 1 : Do not set to this value

    To use DAC you need to set

    for DAC_0     da0e = 1, pd9_3 = 0, ps3_3 = 0, psl3_3 = 1
    for DAC_1     da1e = 1, pd9_4 = 0, ps3_4 = 0, psl3_4 = 1

    If you dont use DAC set

    for DAC_0     da0e = 0 and DA0 = 0x00
    for DAC_1     da1e = 0 and DA1 = 0x00

*/
    // as this bit has automatic lock after write :
    // Set the PD9 and PS3 registers immediately after set PRCR.PRC2 = "1" (write enable).
    // Do not generate an interrupt or a DMA transfer between the instruction to set the PRCR.PRC2 bit to "1"
    // and the instruction to set the PD9 and PS3 registers.

//  prc2 = 1;		// unlock PD9, PS3
//  pd9_3 = 0; 		// Pin P93, 0 : Input          1 : Output (use 0 = input to use the DAC (!))
//  pd9_4 = 0; 		// Pin P94, 0 : Input          1 : Output (use 0 = input to use the DAC (!))
//  prc2 = 0;		// lock again, done automaticaly, no need for this instruction



//  prc2 = 1;		// unlock PD9, PS3
//  ps3_4 = 0;		// Pin P94, 0 = I/O port/peripheral function input, 1 = #RTS4
//  ps3_3 = 0;		// Pin P93, 0 = I/O port/peripheral function input, 1 = #RTS3
//  prc2 = 0;		// lock again, done automaticaly, no need for this instruction



    psl3_4 = 1;         // Port P94, 0 : peripheral function input, 1 : DA1
    psl3_3 = 1;         // Port P93, 0 : peripheral function input, 1 : DA0

    da0 = 0x0000;	// DAC value = 0x0000
    da1 = 0x0000;	// DAC value = 0x0000

    da1e = 1;           // enable DAC output
    da0e = 1;		// enable DAC output
    // -------------------------------------------------------------------------------------------------------
/*
    A/D related registers

    0 : analogue board temp reading
    1 : ext temp reading

    fad = f(Xin) -> frequency must be under 10 MHz when VCC=3.3V

    The IR bit in the ad0ic, ad1ic register indicates whether A-D conversion is completed or not.

    ad0ic 0073h A/D0 Interrupt Control Register, default value after reset = xxxx ?000

    b7,
    b6,
    b5,
    b4,
    b3, ir_ad0ic     (Read Only) Interrupt Request, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
    b2, ilvl2_ad0ic  Interrupt Priority Level Select, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_ad0ic
    b0, ilvl0_ad0ic


    ad1ic 0086h A/D1 Interrupt Control Register, default value after reset = xxxx ?000

    b7,
    b6,
    b5,
    b4,
    b3, ir_ad1ic     (Read Only) Interrupt Request, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
    b2, ilvl2_ad1ic  Interrupt Priority Level Select, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_ad1ic
    b0, ilvl0_ad1ic

    kupic 0093h Key Input Interrupt Control Register, default value after reset = 0000 0000

    b7,
    b6,
    b5,
    b4,
    b3, ir_kupic
    b2, ilvl2_kupic
    b1, ilvl1_kupic
    b0, ilvl0_kupic


    ad0con0 0396h A/D0 control register 0, default value after reset = 0000 0000

    b7, cks0_ad0con0, f0 : Frequency select
    b6, adst_ad0con0,      conversion start flag, 0 = stop conversion, 1 = start conversion
    b5, trg_ad0con0,       Trigger source, 0 = software trigger, 1 = External or hardware trigger
    b4, md1_ad0con0,  m1 : operation mode
    b3, md0_ad0con0,  m0 : operation mode
    b2, ch2_ad0con0,  s2 : Analog input pin select
    b1, ch1_ad0con0,  s1 : Analog input pin select
    b0, ch0_ad0con0,  s0 : Analog input pin select

    s2 s1 s0
     0  0  0 AN0
     .  .  .
     1  1  1 AN7

    ad0con1 0397h A/D0 control register 1, default value after reset = 0000 0000

    b7, opa1_ad0con1,  a1 : External op-amp connection mode
    b6, opa0_ad0con1,  a0 : External op-amp connection mode
    b5, vcut_ad0con1,       Vref connect, 0 = No Vref connection, 1 = Vref connection
    b4, cks1_ad0con1,  f1 : Frequency select
    b3, bits_ad0con1,       8/10-bit mode, 0 = 8 bit, 1 = 10 bit
    b2, md2_ad0con1,   m2 : operation mode
    b1, scan1_ad0con1, w1 : sweep select
    b0, scan0_ad0con1, w0 : sweep select

       f1 f0
	0  0 fad / 4
	0  1 fad / 2
	1  0 fad / 3
	1  1 fad

    m2 m1 m0
     0  0  0  One shot mode
     0  0  1  Repeat mode
     0  1  0  Single Sweep mode
     0  1  1  Repeat Sweep mode 0
     1  x  x  Repeat Sweep mode 1

       w1 w0
	0  0 AN0, AN1 (AN0)
	0  1 AN0..AN5 (AN0..AN2)
	1  0 AN0..AN3 (AN0,AN1)
	1  1 AN0..AN7 (AN0..AN3)

       a1 a0
	0  0 ANEX0 and ANEX1 are not used          additionally set 0 to b6,b5 (psl3_6,psl3_5) of Function Select Register B3
	0  1 signal into ANEX1 is A/D converted
	1  0 signal into ANEX0 is A/D converted
	1  1 External op-amp connection mode


    ad0con2 0394h A/D0 control register 2, default value after reset = xx0x x000

*   b7, 0 		       reserved
*   b6, 0 		       reserved
*   b5, trg0_ad0con2,      external trigger request cause select bit, 0=nADtrg, 1=timer B2 interrupt request of the three-phase motor control timer functions (after ICTB2 counter completes counting)
*   b4,                    nothing is assigned
    b3,  		       nothing is assigned
*   b2, aps1_ad0con2, s1   analog input port select bit
*   b1, aps0_ad0con2, s0   analog input port select bit
    b0, smp_ad0con2,       sampling method, 0 = without the sample and hold, 1 = with the sample and hold function

       s1 s0
	0  0 AN0 to AN_7, Anex0, Anex1
	0  1 AN15_0 to AN15_7
	1  0 AN0_0 to AN0_7
	1  1 AN2_0 to AN2_7

	valid when bit MSS = 0 at AD0Con3 register
	if MSS = 1 set s1s0 to 01

    ------------------------------------------------------------------------------------------------------
    ad00_addr   0380h A/D0 register 0   16bits, default value after reset = 0x00XX
    ad01_addr   0382h A/D1 register 1   16bits, default value after reset = 0x00XX
    ad02_addr   0384h A/D2 register 2   16bits, default value after reset = 0x00XX
    ad03_addr   0386h A/D3 register 3   16bits, default value after reset = 0x00XX
    ad04_addr   0388h A/D4 register 4   16bits, default value after reset = 0x00XX
    ad05_addr   038ah A/D5 register 5   16bits, default value after reset = 0x00XX
    ad06_addr   038ch A/D6 register 6   16bits, default value after reset = 0x00XX
    ad07_addr   038eh A/D7 register 7   16bits, default value after reset = 0x00XX

*/
    // -------------------------------------------------------------------------------------------------------
    //  Repeat sweep mode 0 on channels 0 & 1 of port AN (port 10)
    //  Ana conversion cycle = Fad cycle / 4

    ad0con0 = 0x58;	// Repeat sweep mode 0, start conversion, Clk=Fad/4
    ad0con1 = 0x28;	// 10 bits, CKS1=0, VREF connexion, channels 0 & 1 selected
    ad0con2 = 0x01;	// sample&hold, AN-7 selected

    // -------------------------------------------------------------------------------------------------------
    /*--- External Interrupt ---*/

/*
    ifsr 031Fh Interrupt cause select Register, default value after reset = 0000 0000

	    	UART1 and UART4 Interrupt Cause Select Bit, 0 = UART4 bus conflict, start condition detect, stop condition detect, fault error detect
    b7, ifsr7 = 0;
	    	UART0 and UART3 Interrupt Cause Select Bit, 1 = UART0 bus conflict, start condition detect, stop condition detect, fault error detect
    b6, ifsr6 = 1;
	    	INT5 Interrupt Polarity Select Bit, 0 = One Edge, 1 = Both Edges
    b5, ifsr5 = 0;
	    	INT4 Interrupt Polarity Select Bit, 0 = One Edge, 1 = Both Edges
    b4, ifsr4 = 0;
	    	INT3 Interrupt Polarity Select Bit, 0 = One Edge, 1 = Both Edges
    b3, ifsr3 = 0;
	    	INT2 Interrupt Polarity Select Bit, 0 = One Edge, 1 = Both Edges
    b2, ifsr2 = 0;
	    	INT1 Interrupt Polarity Select Bit, 0 = One Edge, 1 = Both Edges
    b1, ifsr1 = 0;
	    	INT0 Interrupt Polarity Select Bit, 0 = One Edge, 1 = Both Edges
                Note : when (lvs_int0ic == 1 Level Sense) this bit must be 0 (One Edge).
                       when this bit is 1 = Both Edges then set pol_int0ic = 0 (Falling Edge or "L")
    b0, ifsr0 = 0;
*/
    ifsr6 = 1;  // UART0, UART3 wakes up

    // -------------------------------------------------------------------------------------------------------
    /*--- Timers ---*/
/*
    trgsr 0343h Trigger Select Register, default value after reset = 0000 0000

    b7, ta4tgh Timer A4 event/trigger select bit
    b6, ta4tgl Timer A4 event/trigger select bit
    b5, ta3tgh Timer A3 event/trigger select bit
    b4, ta3tgl Timer A3 event/trigger select bit
    b3, ta2tgh Timer A2 event/trigger select bit
    b2, ta2tgl Timer A2 event/trigger select bit
    b1, ta1tgh Timer A1 event/trigger select bit
    b0, ta1tgl Timer A1 event/trigger select bit

    udf 0344h Timer Up/Down Select Register, default value after reset = 0000 0000

    b7, ta4p, 0 = Timer A4 Two-Phase disabled
    b6, ta3p, 0 = Timer A3 Two-Phase disabled
    b5, ta2p, 0 = Timer A2 Two-Phase disabled
    b4, ta4ud, 0 = Decrement counter for Timer A4, 1 = Increment counter for Timer A4
    b3, ta3ud, 0 = Decrement counter for Timer A3
    b2, ta2ud, 0 = Decrement counter for Timer A2
    b1, ta1ud, 0 = Decrement counter for Timer A1, 1 = Increment counter for Timer A1
    b0, ta0ud, 0 = Decrement counter for Timer A0

    tabsr 0340h Timer Count Start Flag, default value after reset = 0000 0000

    b7, tb2s
    b6, tb1s
    b5, tb0s
    b4, ta4s, 1 = Start Timer A4
    b3, ta3s, 1 = Start Timer A3
    b2, ta2s, 1 = Start Timer A2
    b1, ta1s, 1 = Start Timer A1
    b0, ta0s, 1 = Start Timer A0

    tcspr 035Fh Count Source Prescaler Register

    b7 cst, Operation Enable Bit, 0 : Stop, 1 : Running
    b6
    b5
    b4
    b3,b2,b1,b0 cnt3, division rate select bit 1 to 15

    0000 no division
    0001 division by 2*1
    0010 division by 2*2
    ....
    1111 divison by 2*15

*/
    // -------------------------------------------------------------------------------------------------------
    tcspr	= 0x82;			// for setting f2n with n=2 with a 24MHz clock
    // -------------------------------------------------------------------------------------------------------
    /*--- Timer A0 ---*/
/*
	Timer A0 must generates the tick every 1ms (the timer is 16bits)

    24MHz clock
    using f1 as source 24MHz -> T = 41.6ns
    the Timer must decrement from 60'000

    and alternative can be
    using f8 as source 24MHz/8 = 3MHz -> T = 333.3ns
    the Timer must decrement from 30'000

    ta0mr 0356h Timer A0 Mode Register, default value after reset = 0000 0000

    changes when using 3-phase motor control Timer Function
    changes when using Event Counter Mode

    b7 tck1_ta0mr, t1		Count source select
    b6 tck0_ta0mr, t0		Count source select
    b5 mr3_ta0mr, g3		0 : Not used in Timer mode
    b4 mr2_ta0mr, g2		gate function select
    b3 mr1_ta0mr, g1		gate function select
    b2 mr0_ta0mr, g0		0 : Not used
    b1, tmod1_ta0mr, m1 	Operation Mode Select
    b0, tmod0_ta0mr, m0 	Operation Mode Select

	t1 t0 - Count Source Select Bit
	00 : f1
	01 : f8
	10 : f2n
	11 : fc32

	g2 g1 - Gate Function Select Bit
	00 : Gate function disabled
	01 : Gate function disabled
	10 : Timer counts only while the TA0in pin is held "L"
	11 : Timer counts only while the TA0in pin is held "H"

	m1 m0
***	00 : Timer Mode
	01 : Event counter mode
	10 : One-shot timer mode
	11 : PWM mode
*/

//  ta0ud = 0; // Decrement counter for Timer A0
    ta0mr = 0x00;	// for 24MHz selecting f1 as source
    ta0	= 200 - 1;	// The calculated value less 1 used to load the counter
    ta0s = 1;	// Start Timer A0

    // -------------------------------------------------------------------------------------------------------
    /* Timer A1 - 1ms tick

	Timer A1 must generates a tick every 1ms (the timer is 16bits)

    24MHz clock
    using f1 as source 24MHz -> T = 41.6ns
    the Timer must decrement from 24000


    ta1mr 0357h Timer A1 Mode Register, default value after reset = 0000 0000

    changes when using 3-phase motor control Timer Function
    changes when using Event Counter Mode

    b7 tck1_ta1mr, t1		Count source select
    b6 tck0_ta1mr, t0		Count source select
    b5 mr3_ta1mr, g3		0 : Not used in Timer mode
    b4 mr2_ta1mr, g2		gate function select
    b3 mr1_ta1mr, g1		gate function select
    b2 mr0_ta1mr, g0		0 : Not used
    b1, tmod1_ta1mr, m1 	Operation Mode Select
    b0, tmod0_ta1mr, m0 	Operation Mode Select

	t1 t0 - Count Source Select Bit
	00 : f1
	01 : f8
	10 : f2n
	11 : fc32

	g2 g1 - Gate Function Select Bit
	00 : Gate function disabled
	01 : Gate function disabled
	10 : Timer counts only while the TA0in pin is held "L"
	11 : Timer counts only while the TA0in pin is held "H"

	m1 m0
***	00 : Timer Mode
	01 : Event counter mode
	10 : One-shot timer mode
	11 : PWM mode
*/

//    b1, ta1tgh Timer A1 event/trigger select bit
//    b0, ta1tgl Timer A1 event/trigger select bit

//  ta1ud = 0; // Decrement counter for Timer A1
//  ta1mr = 0x00; // for 24MHz selecting f1 as source

    ta1	= 24000 - 1;	// The calculated value less 1 used to load the counter
    ta1s = 1;	// Start Timer A1
    // -------------------------------------------------------------------------------------------------------
    /* Timer A2 - increments on Timer A1

    ta2mr 0358h Timer A2 Mode Register, default value after reset = 0000 0000

    changes when using Timer Mode
    changes when using 3-phase motor control Timer Function

    b7 tck1_ta2mr, 2-Phase Pulse Signal Processing Operation Select, 0 : Normal, 1 : Multiply by 4
    b6 tck0_ta2mr, Count operation Type Select, 0 : Reloading, 1 : Free running
    b5 mr3_ta2mr,  0 : Not used for Event counter mode
    b4 mr2_ta2mr, Increment/Decrement Switching Cause Select, 0 : Use setting of the UDF register, 1 : Input signal to TA2out pin
                  (UDF is 0344h Timer Up Down Flag Register)
    b3 mr1_ta2mr, Count polarity Select, 0 : Counts falling edges of an external signal, 1 : Counts rising edges of an external signal
    b2 mr0_ta2mr, 0 : Not used
    b1, tmod1_ta2mr, m1 	Operation Mode Select
    b0, tmod0_ta2mr, m0 	Operation Mode Select

	m1 m0
	00 : Timer Mode
***	01 : Event counter mode
	10 : One-shot timer mode
	11 : PWM mode

*/
    ta2tgh = 1;	// Timer A2 uses TA1 overflow or underflow
    ta2ud = 1; // Increment counter for Timer A2

//  tck1_ta2mr = 0; // Not used
    tck0_ta2mr = 1; // 1 : Free running
//  mr3_ta2mr = 0; // Not used for Event counter mode
//  mr2_ta2mr = 0; // Use setting of the UDF register

//  mr1_ta2mr = 0; // external trigger select, 0 : 3-phase motor control timer
//  mr0_ta2mr = 0; // Not used
//  tmod1_ta2mr = 0;
    tmod0_ta2mr = 1; // 01 : Event counter mode

//  ta2 = ??;	// count for
    ta2s = 1;	// Start timer A2
    // -------------------------------------------------------------------------------------------------------
    /*  Timer A3 - 1us tick

	Timer A3 must generates a tick every 1us (the timer is 16bits)

    24MHz clock
    using f1 as source 24MHz -> T = 41.6ns
    the Timer must decrement from 24

    ta3mr 0359h Timer A3 Mode Register, default value after reset = 0000 0000

    changes when using 3-phase motor control Timer Function
    changes when using Event Counter Mode

    b7 tck1_ta3mr, t1		Count source select
    b6 tck0_ta3mr, t0		Count source select
    b5 mr3_ta3mr, g3		0 : Not used in Timer mode
    b4 mr2_ta3mr, g2		gate function select
    b3 mr1_ta3mr, g1		gate function select
    b2 mr0_ta3mr, g0		0 : Not used
    b1, tmod1_ta3mr, m1 	Operation Mode Select
    b0, tmod0_ta3mr, m0 	Operation Mode Select

	t1 t0 - Count Source Select Bit
	00 : f1
	01 : f8
	10 : f2n
	11 : fc32

	g2 g1 - Gate Function Select Bit
	00 : Gate function disabled
	01 : Gate function disabled
	10 : Timer counts only while the TA0in pin is held "L"
	11 : Timer counts only while the TA0in pin is held "H"

	m1 m0
***	00 : Timer Mode
	01 : Event counter mode
	10 : One-shot timer mode
	11 : PWM mode
*/

//  ta3ud = 0; // Decrement counter for Timer A3
//  ta3mr = 0x00; // for 24MHz selecting f1 as source

    ta3	= 24 - 1;	// The calculated value less 1 used to load the counter
    ta3s = 1;	// Start timer A3
    // -------------------------------------------------------------------------------------------------------
    /*--- Timer A4 - increments on Timer A3 - used by usleep() ---*/

/*
    ta4mr 035Ah Timer A4 Mode Register, default value after reset = 0000 0000

    changes when using Timer Mode
    changes when using 3-phase motor control Timer Function

    b7 tck1_ta4mr, 2-Phase Pulse Signal Processing Operation Select, 0 : Normal, 1 : Multiply by 4
    b6 tck0_ta4mr, Count operation Type Select, 0 : Reloading, 1 : Free running
    b5 mr3_ta4mr,  0 : Not used for Event counter mode
    b4 mr2_ta4mr, Increment/Decrement Switching Cause Select, 0 : Use setting of the UDF register, 1 : Input signal to TA4out pin
                  (UDF is 0344h Timer Up Down Flag Register)
    b3 mr1_ta4mr, Count polarity Select, 0 : Counts falling edges of an external signal, 1 : Counts rising edges of an external signal
    b2 mr0_ta4mr, 0 : Not used
    b1, tmod1_ta4mr, m1 	Operation Mode Select
    b0, tmod0_ta4mr, m0 	Operation Mode Select

	m1 m0
	00 : Timer Mode
***	01 : Event counter mode
	10 : One-shot timer mode
	11 : PWM mode

*/
    ta4tgh = 1;	// Timer A4 uses TA3 overflow or underflow
    ta4ud = 1; // Increment counter for Timer A4

//  tck1_ta4mr = 0; // Not used
    tck0_ta4mr = 1; // 1 : Free running
//  mr3_ta4mr = 0; // Not used for Event counter mode
//  mr2_ta4mr = 0; // Use setting of the UDF register

//  mr1_ta4mr = 0; // external trigger select, 0 : 3-phase motor control timer
//  mr0_ta4mr = 0; // Not used
//  tmod1_ta4mr = 0;
    tmod0_ta4mr = 1; // 01 : Event counter mode

//  ta4 = ??;	// count for
    ta4s = 1;	// Start timer A4
    // -------------------------------------------------------------------------------------------------------
/*

    dm0sl 0378h DMA0 Request (cause) Source Select Register, default value after reset = 0x00 0000

    b7, drq_dm0sl  DMA request bit, 0 : Not requested, 1 : Requested
    b6, reserved
    b5, dsr_dm0sl  Software DMA request bit, When a software trigger is selected, a DMA request is generated by setting this bit to 1 (Read as 0)
    b4, dsel4_dm0sl   DMA request cause select bit
    b3, dsel3_dm0sl   DMA request cause select bit
    b2, dsel2_dm0sl   DMA request cause select bit
    b1, dsel1_dm0sl   DMA request cause select bit
    b0, dsel0_dm0sl   DMA request cause select bit

         0000 Software trigger

	 0011 Timer A0 interrupt request
*/

//  dm0sl = 0x83;		// cf p146 of hw manual, TA0 interrupt

    // -------------------------------------------------------------------------------------------------------
/*
    UARTx related registers, needed to config the I/O pins

    psl0 03B2h Port Output Function Select Register B0, default value after reset = 0000 0000

    b7, psl0_7, Port P67, 0 : TxD1/SDA1(out), 1 : Do not set to this value
    b6, psl0_6, Port P66, 0 : SCL1(in/out), 1 : STxD1(out)
    b5, psl0_5, Port P65, 0 : CLK1(out), 1 : Do not set to this value
    b4, psl0_4, Port P64, 0 : #RTS1(out), 1 : OUTC2_1(out)/ISCLK2(intelligent I/O)
    b3, psl0_3, Port P63, 0 : TxD0/SDA0/IrDA(out), 1 : Do not set to this value
    b2, psl0_2, Port P62, 0 : SCL0(in/out), 1 : STxD0(out)
    b1, psl0_1, Port P61, 0 : CLK0(out),  1 : RTP0_1
    b0, psl0_0, Port P60, 0 : #RTS0(out), 1 : RTP0_0

    psl1 03B3h Port 7 Output Function Select Register B1, default value after reset = 0000 0000

    b7, psl1_7, Port P77, 0 : ISCLK0 (out), 1 : (specified by psc_7)
    b6, psl1_6, Port P76, 0 : (specified by psc_6), 1 : TA3out (out)
    b5, psl1_5, Port P75, 0 : #W, 1 : (specified by psc_5)
    b4, psl1_4, Port P74, 0 : (specified by psc_4), 1 : W
    b3, psl1_3, Port P73, 0 : (specified by psc_3), 1 : #V
    b2, psl1_2, Port P72, 0 : (specified by psc_2), 1 : TA1out
    b1, psl1_1, Port P71, 0 : (specified by psc_1), 1 : STXD2
    b0, psl1_0, Port P70, 0 : (specified by psc_0), 1 : TA0out

    psl3 03B7h Port 9 Output Function Select Register B3, default value after reset = 0000 0000
    shared with DAC config

    ps0 03B0h Port 6 Output Function Select Register A0, default value after reset = 0000 0000

    b7, ps0_7 -> Pin P67, 0 = I/O port, 1 = TxD1(out)/SDA1(in/out)/SRxD1(in)
    b6, ps0_6 -> Pin P66, 0 = I/O port/RxD1(in), 1 = (specified by psl0_6)
    b5, ps0_5 -> Pin P65, 0 = I/O port/Clk1(in), 1 =
    b4, ps0_4 -> Pin P64, 0 = I/O port/#CTS0(in), 1 = (specified by psl0_4)
    b3, ps0_3 -> Pin P63, 0 = I/O port, 1 = TxD0(out)/SDA0(in/out)/SRxD0(in)
    b2, ps0_2 -> Pin P62, 0 = I/O port/RxD0(in), 1 = (specified by psl0_2)
    b1, ps0_1 -> Pin P61, 0 = I/O port/Clk0(in), 1 =
    b0, ps0_0 -> Pin P60, 0 = I/O port/#CTS0(in), 1 = #RTS0(out)/#SS0(in)

    ps1 03B1h Port 7 Output Function Select Register A1, default value after reset = 0000 0000

    b7, ps1_7 -> Pin P77, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_7)
    b6, ps1_6 -> Pin P76, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_6)
    b5, ps1_5 -> Pin P75, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_5)
    b4, ps1_4 -> Pin P74, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_4)
    b3, ps1_3 -> Pin P73, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_3)
    b2, ps1_2 -> Pin P72, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_2)
    b1, ps1_1 -> Pin P71, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_1)
    b0, ps1_0 -> Pin P70, 0 = I/O port/peripheral function input, 1 = (specified by PSL1_0)

    ps3 03B5h Port 9 Output Function Select Register A3, default value after reset = 0000 0000 (to write it needs enabling the protection bit prc2)
    shared with DAC config



    Ports 0,1,2,3,4 & 5 are used for C87 bus

    pd1 03E3h Port P1 Direction Register, default value after reset = 0000 0000

    b7, pd1_7 -> Pin P17, 0 : Input, 1 : Output
    b6, pd1_6 -> Pin P16, 0 : Input, 1 : Output
    b5, pd1_5 -> Pin P15, 0 : Input, 1 : Output
    b4, pd1_4 -> Pin P14, 0 : Input, 1 : Output
    b3, pd1_3 -> Pin P13, 0 : Input, 1 : Output
    b2, pd1_2 -> Pin P12, 0 : Input, 1 : Output
    b1, pd1_1 -> Pin P11, 0 : Input, 1 : Output
    b0, pd1_0 -> Pin P10, 0 : Input, 1 : Output

    pd1 = 0; // Pin P1x as Input

    read or write the pins P10..P17 via Port P1 Register

    p1 03E1h Port P1 Register, default value after reset = xxxx xxxx

    b7, p1_7
    b6, p1_6
    b5, p1_5
    b4, p1_4
    b3, p1_3
    b2, p1_2
    b1, p1_1
    b0, p1_0

    pcr 03FFh Port P1 Control Register, default value after reset = xxxx xxx0

    b7..b1, 0 not used
    b0, pcr0, 0 : CMOS output, 1 : N-channel open drain output

    pcr0 = 0;



    pur0 03F0h Pull-Up Control Register 0  for 144-pin package, default value after reset = 0000 0000

    b7, pu07, 0 : P34 - P37 NOT pulled up, 1 : pulled up
    b6, pu06, 0 : P30 - P33 NOT pulled up, 1 : pulled up
    b5, pu05, 0 : P24 - P27 NOT pulled up, 1 : pulled up
    b4, pu04, 0 : P20 - P23 NOT pulled up, 1 : pulled up
    b3, pu03, 0 : P14 - P17 NOT pulled up, 1 : pulled up
    b2, pu02, 0 : P10 - P13 NOT pulled up, 1 : pulled up
    b1, pu01, 0 : P04 - P07 NOT pulled up, 1 : pulled up
    b0, pu00, 0 : P00 - P03 NOT pulled up, 1 : pulled up

    in memory expansion mode and microprocessor mode set each bit in PUR0 to 0 since P0 to P5
    are used as bus control pins


    pur2 03DAh Pull-Up Control Register 2  for 144-pin package, default value after reset = 0000 0000

    b7, pu27, 0 : P94 - P97 NOT pulled up, 1 : pulled up
    b6, pu26, 0 : P90 - P93 NOT pulled up, 1 : pulled up
    b5, pu25, 0 : P84 - P87 NOT pulled up, 1 : pulled up  (P85 can't be pulled-up internally)
    b4, pu24, 0 : P80 - P83 NOT pulled up, 1 : pulled up
    b3, pu23, 0 : P74 - P77 NOT pulled up, 1 : pulled up
    b2, pu22, 0 : P72 - P73 NOT pulled up, 1 : pulled up  (P70,P71 can't be pulled-up)
    b1, pu21, 0 : P64 - P67 NOT pulled up, 1 : pulled up
    b0, pu20, 0 : P60 - P63 NOT pulled up, 1 : pulled up

    pur3 03DBh Pull-Up Control Register 3, default value after reset = 0000 0000

    b7, pu37, 0 : P134 - P137 NOT pulled up, 1 : pulled up
    b6, pu36, 0 : P130 - P133 NOT pulled up, 1 : pulled up
    b5, pu35, 0 : P124 - P127 NOT pulled up, 1 : pulled up
    b4, pu34, 0 : P120 - P123 NOT pulled up, 1 : pulled up
    b3, pu33, 0 : P114        NOT pulled up, 1 : pulled up
    b2, pu32, 0 : P110 - P113 NOT pulled up, 1 : pulled up
    b1, pu31, 0 : P104 - P107 NOT pulled up, 1 : pulled up
    b0, pu30, 0 : P100 - P103 NOT pulled up, 1 : pulled up


    pd5 03EBh Port P5 Direction Register, default value after reset = 0000 0000

    b7, pd5_7 -> Pin P57, 0 : Input          1 : Output
    b6, pd5_6 -> Pin P56, 0 : Input          1 : Output
    b5, pd5_5 -> Pin P55, 0 : Input          1 : Output
    b4, pd5_4 -> Pin P54, 0 : Input          1 : Output
    b3, pd5_3 -> Pin P53, 0 : Input          1 : Output
    b2, pd5_2 -> Pin P52, 0 : Input          1 : Output
    b1, pd5_1 -> Pin P51, 0 : Input          1 : Output
    b0, pd5_0 -> Pin P50, 0 : Input          1 : Output

    pd6 03C2h Port P6 Direction Register, default value after reset = 0000 0000

    b7, pd6_7 -> Pin P67, 0 : Input,         1 : Output (TxD1)
    b6, pd6_6 -> Pin P66, 0 : Input (RxD1),  1 : Output
    b5, pd6_5 -> Pin P65, 0 : Input (Clk1),  1 : Output
    b4, pd6_4 -> Pin P64, 0 : Input (#CTS1), 1 : Output
    b3, pd6_3 -> Pin P63, 0 : Input,         1 : Output (TxD0)
    b2, pd6_2 -> Pin P62, 0 : Input (RxD0),  1 : Output
    b1, pd6_1 -> Pin P61, 0 : Input (Clk0),  1 : Output
    b0, pd6_0 -> Pin P60, 0 : Input (#CTS0), 1 : Output

    pd7 03C3h Port P7 Direction Register, default value after reset = 0000 0000

    b7, pd7_7 -> Pin P77, 0 : Input          1 : Output     related to TA3in, CLK5, ISCLK0, CAN0in
    b6, pd7_6 -> Pin P76, 0 : Input          1 : Output     related to TA3out, TXD5, ISTXD0, CAN0out
    b5, pd7_5 -> Pin P75, 0 : Input          1 : Output     related to TA2in, ISRXD1
    b4, pd7_4 -> Pin P74, 0 : Input          1 : Output     related to TA2out, ISCLK1
    b3, pd7_3 -> Pin P73, 0 : Input          1 : Output     related to TA1in, ISTXD1
    b2, pd7_2 -> Pin P72, 0 : Input          1 : Output     related to TA1out
    b1, pd7_1 -> Pin P71, 0 : Input          1 : Output     related to TA0in, Rx2
    b0, pd7_0 -> Pin P70, 0 : Input          1 : Output     related to TA0out

    pd8 03C6h Port P8 Direction Register, default value after reset = 00x0 0000

    b7, pd8_7 -> Pin P87, 0 : Input          1 : Output     related to CM04
    b6, pd8_6 -> Pin P86, 0 : Input          1 : Output     related to CM04
    b5, pd8_5 -> Pin P85, 0 : Input          1 : Output
    b4, pd8_4 -> Pin P84, 0 : Input          1 : Output
    b3, pd8_3 -> Pin P83, 0 : Input          1 : Output     related to CAN0in, CAN1in
    b2, pd8_2 -> Pin P82, 0 : Input          1 : Output     related to CAN0out, CAN1out
    b1, pd8_1 -> Pin P81, 0 : Input          1 : Output     related to TA4in, CTS5, RTS5
    b0, pd8_0 -> Pin P80, 0 : Input          1 : Output     related to TA4out, RXD5, ISRXD0

    pd9 03C7h Port P9 Direction Register, default value after reset = 0000 0000 (to write it needs enabling the protection bit prc2)
    shared with DAC config


    pd10 03CAh Port P10 Direction Register, default value after reset = 0000 0000

    b7, pd10_7 -> Pin P107, 0 : Input          1 : Output
    b6, pd10_6 -> Pin P106, 0 : Input          1 : Output
    b5, pd10_5 -> Pin P105, 0 : Input          1 : Output
    b4, pd10_4 -> Pin P104, 0 : Input          1 : Output
    b3, pd10_3 -> Pin P103, 0 : Input          1 : Output
    b2, pd10_2 -> Pin P102, 0 : Input          1 : Output
    b1, pd10_1 -> Pin P101, 0 : Input          1 : Output    related to AN1
    b0, pd10_0 -> Pin P100, 0 : Input          1 : Output    related to AN0

    pd11 03CBh Port P11 Direction Register, default value after reset = xxx0 0000

    b7, pd11_7 -> Pin P117, 0 : Input          1 : Output
    b6, pd11_6 -> Pin P116, 0 : Input          1 : Output
    b5, pd11_5 -> Pin P115, 0 : Input          1 : Output
    b4, pd11_4 -> Pin P114, 0 : Input          1 : Output
    b3, pd11_3 -> Pin P113, 0 : Input          1 : Output
    b2, pd11_2 -> Pin P112, 0 : Input          1 : Output
    b1, pd11_1 -> Pin P111, 0 : Input          1 : Output
    b0, pd11_0 -> Pin P110, 0 : Input          1 : Output

    pd12 03CEh Port P12 Direction Register, default value after reset = 0000 0000

    b7, pd12_7 -> Pin P127, 0 : Input          1 : Output
    b6, pd12_6 -> Pin P126, 0 : Input          1 : Output
    b5, pd12_5 -> Pin P125, 0 : Input          1 : Output
    b4, pd12_4 -> Pin P124, 0 : Input          1 : Output
    b3, pd12_3 -> Pin P123, 0 : Input          1 : Output
    b2, pd12_2 -> Pin P122, 0 : Input          1 : Output
    b1, pd12_1 -> Pin P121, 0 : Input          1 : Output
    b0, pd12_0 -> Pin P120, 0 : Input          1 : Output

    pd13 03CFh Port P13 Direction Register, default value after reset = 0000 0000

    b7, pd13_7 -> Pin P137, 0 : Input          1 : Output
    b6, pd13_6 -> Pin P136, 0 : Input          1 : Output
    b5, pd13_5 -> Pin P135, 0 : Input          1 : Output
    b4, pd13_4 -> Pin P134, 0 : Input          1 : Output
    b3, pd13_3 -> Pin P133, 0 : Input          1 : Output
    b2, pd13_2 -> Pin P132, 0 : Input          1 : Output
    b1, pd13_1 -> Pin P131, 0 : Input          1 : Output
    b0, pd13_0 -> Pin P130, 0 : Input          1 : Output

    pd14 03D2h Port P14 Direction Register, default value after reset = x000 0000

    b7, pd14_7 -> Pin P147, 0 : Input          1 : Output
    b6, pd14_6 -> Pin P146, 0 : Input          1 : Output
    b5, pd14_5 -> Pin P145, 0 : Input          1 : Output
    b4, pd14_4 -> Pin P144, 0 : Input          1 : Output
    b3, pd14_3 -> Pin P143, 0 : Input          1 : Output
    b2, pd14_2 -> Pin P142, 0 : Input          1 : Output
    b1, pd14_1 -> Pin P141, 0 : Input          1 : Output
    b0, pd14_0 -> Pin P140, 0 : Input          1 : Output

    pd15 03D3h Port P15 Direction Register, default value after reset = 0000 0000

    b7, pd15_7 -> Pin P157, 0 : Input          1 : Output
    b6, pd15_6 -> Pin P156, 0 : Input          1 : Output
    b5, pd15_5 -> Pin P155, 0 : Input          1 : Output
    b4, pd15_4 -> Pin P154, 0 : Input          1 : Output
    b3, pd15_3 -> Pin P153, 0 : Input          1 : Output
    b2, pd15_2 -> Pin P152, 0 : Input          1 : Output
    b1, pd15_1 -> Pin P151, 0 : Input          1 : Output
    b0, pd15_0 -> Pin P150, 0 : Input          1 : Output

    psc 03AFh Port 7 Output Function Select Register C, default value after reset = 0000 0000
    related with ADC config
    related with UART2 config

    b7, psc_7 -> Pin P77, 0 : P10_4 to P10_7 or #KI0 to #KI3, 1 : AN_4 to AN_7
    b6, psc_6 -> Pin P76, 0 : (specified by PSD1_6), 1 : CAN0out
    b5, psc_5 -> Pin P75, 0 : OUTC1_2, 1 : RTP2_1
    b4, psc_4 -> Pin P74, 0 : TA2out (out), 1 : (specified by PSD1_4)
    b3, psc_3 -> Pin P73, 0 : #RTS2, 1 : OUTC1_0/ISTXD1
    b2, psc_2 -> Pin P72, 0 : CLK2 (out), 1 : V
    b1, psc_1 -> Pin P71, 0 : SCL2 (out), 1 : (specified by PSD1_1)
    b0, psc_0 -> Pin P70, 0 : TXD2/SDA2 (out), 1 : (specified by PSD1_0)

*/
    // -------------------------------------------------------------------------------------------------------
/*
    CTS 0 (input)     pd6_0 = 0,  ps0_0 = 0
    RTS 0 (output)    psl0_0 = 0, ps0_0 = 1
    CLK 0 (input)     pd6_1 = 0,  ps0_1 = 0
    Rx  0 (input)     pd6_2 = 0,  ps0_2 = 0
    Tx  0 (output)    psl0_3 = 0, ps0_3 = 1

    CTS 1 (input)     pd6_4 = 0,  ps0_4 = 0
    RTS 1 (output)    psl0_4 = 0, ps0_4 = 1
    CLK 1 (input)     pd6_5 = 0,  ps0_5 = 0
    Rx  1 (input)     pd6_6 = 0,  ps0_6 = 0
    Tx  1 (output)    psl0_7 = 0, ps0_7 = 1

    CTS 2 (input)     pd7_3 = 0, ps1_3 = 0
    RTS 2 (output)    psc_3 = 0, psl1_3 = 0, ps1_3 = 1
    CLK 2 (input)     pd7_2 = 0, ps1_2 = 0
    Rx  2 (input)     pd7_1 = 0, ps1_1 = 0
    Tx  2 (output)    psc_0 = 0, psl1_0 = 0, ps1_0 = 1

    CTS 3 (input)     pd9_3 = 0,  psl3_3 = 0, ps3_3 = 0
    RTS 3 (output)    ps3_3 = 1
    CLK 3 (input)     pd9_0 = 0,  ps3_0 = 0
    Rx  3 (input)     pd9_1 = 0,  ps3_1 = 0
    Tx  3 (output)    psl3_2 = 0, ps3_2 = 1

    CTS 4 (input)     pd9_4 = 0, psl3_4 = 0, ps3_4 = 0
    RTS 4 (output)    ps3_4 = 1
    CLK 4 (input)     pd9_5 = 0, psl3_5 = 0, ps3_5 = 0
    Rx  4 (input)     pd9_7 = 0, ps3_7 = 0
    Tx  4 (output)    psc3_6 = 0, ps3_6 = 1


*/

    pu20 = 1;	// Pull-up p6_0 to p6_3

//  psl1_2 = 0; // Port P72, 0 : (specified by psc_2), 1 : TA1out

    pd5_4 = 1;	// output used for Slow WD trig
//  pd5_7 = 1;	// output used for MCU_~RDY (goes to NET board)

//  pd6_6 = 0;
//  pd6_5 = 0;
//  pd6_4 = 0;
//  pd6_3 = 1; // ?? needed ??
//  pd6_2 = 0; // Pin P62 with RxD0 as input, default value after reset
//  pd6_1 = 0;
//  pd6_0 = 0; // JTAG ready input
               // (goes to zero when JTAG interface is powered, otherwise high-Z with pullup)

//  pd7_3 = 0; // TERM/SPY ready signal from USB
//  pd7_1 = 0; // Pin P71, 0 : Input Rx2


//  pd8 = 0x00; // default value after reset = 00x0 0000

//  pd10_4 = 1;	// TP0 output
//  pd10_5 = 1;	// TP1 output
//  pd10_6 = 0;	// TP2 input
//  pd10_7 = 1;	// TP3 input (used as JTAG REQUEST acknowledge for front panel)

    pd10 = 0xFE; // 1111 1110

//  p10_4 = 1;	// TP0 set to 1
//  p10_5 = 1;	// TP1 set to 1
//  p10_7 = 1;	// DEV_JTAG_RQST_ACK_NOT active low

    p10	= 0x00;

//  pd11_0 = 0; // JTAG REQUEST from front panel
    pd12_5 = 1; // MRAM protect output

    /* JTAG port to external board (voltage source, etc.) */

    pd13_0 = 1;	// FPGA_PROG_B output (reset FPGA)
//  pd13_1 = 0;	// FPGA_INIT_B input
//  pd13_2 = 0;	// FPGA_DONE input
//  pd13_4 = 0; // JTAG TDI input
    pd13_5 = 1; // JTAG TDO output
    pd13_6 = 1; // JTAG TMS output
    pd13_7 = 1; // JTAG TCK output

    p13_0  = 1; // release FPGA_PROG_B (When PROG_B is low, it initiates a programming of the FPGA)


    pd14_0 = 1; // FIP reset output
    pd14_1 = 1; // Fast WD trig ouput
    pd14_2 = 1; // DIG_CMD_ENA_not output
    pd14_6 = 1; // POWERCYCLE output

    p14_6 = 1; // No powercycle by default


    /*--- Port 15 - JTAG bus to program the FPGA ---*/

    pd15_5 = 0; // JTAG TCK
    pd15_4 = 0; // JTAG TMS
    pd15_6 = 0; // JTAG TDI
    pd15_7 = 0; // JTAG TDO


//  ps0_7 = 0;
//  ps0_6 = 0;
//  ps0_5 = 0;
//  ps0_4 = 0;
    ps0_3 = 1; // Pin P63 as TxD0/SDA0
//  ps0_2 = 0; // Pin P62 as RxD0, default value after reset
//  ps0_1 = 0;
//  ps0_0 = 0;


//  ps1_3 = 1; // Pin P73, 1 = (specified by PSL1_3) to use RTS 2
    ps1_2 = 1; // Pin P72, 1 = (specified by PSL1_2)
//  ps1_1 = 0; // Pin P71, 0 = I/O port/peripheral function input
    ps1_0 = 1; // Pin P70, 1 = (specified by PSL1_0)


//  psl0_7 = 0;
//  psl0_3 = 0;
//  psl0_2 = 0; // P62 -> RxD0, default value after reset

//  psc_2 = 0; // Pin P72, 0 : CLK2 (out) (but not connected)

    // -------------------------------------------------------------------------------------------------------
/*
    UART0 related registers

    u0mr 0368h UART0 Transmit/Receive Mode Register, default value after reset = 0000 0000

    b7, iopol_u0mr      TxD, Rxd Input/Output Polarity Switch Bit, 0 = Not inverted
    b6, prye_u0mr       Parity Enabled Bit, 0 = parity disabled
    b5, pry_u0mr        Odd/Even Parity Select Bit, 0 = Odd parity (only when parity enabled)
    b4, stps_u0mr       Stop bit Length Select Bit, 0 = 1 stop bit
    b3, ckdir_u0mr      Internal/External Clock Select Bit, 0 = Internal clock
    b2, smd2_u0mr  s2 : Serial I/O Mode Select
    b1, smd1_u0mr  s1 : Serial I/O Mode Select
    b0, smd0_u0mr  s0 : Serial I/O Mode Select

        s2 s1 s0
	 0  0  0  Serial interface disabled
	 0  0  1  Clock Synchronous mode
	 0  1  0  I2C mode
	 0  1  1
	 1  0  0  UART mode (asynchronous), 7-bit transfer data
	 1  0  1  UART mode (asynchronous), 8-bit transfer data
	 1  1  0  UART mode (asynchronous), 9-bit transfer data
	 1  1  1


    u0smr 0367h UART0 Special Mode Register, default value after reset = 0000 0000

    b7, sclkdiv_u0smr	Clock Divide Synchronous Bit, related to su1him_u0smr2
    b6, sss_u0smr 	Transmit Start Condition Select Bit, 0 = Not Related to RxD0
    b5, acse_u0smr 	Auto Clear Function Select Bit for Transmit Enable Bit, 0 = No autoclear function
    b4, abscs_u0smr 	Bus Conflict Detect Sampling Clock Select Bit, 0 = Rising edge of transfer clock
    b3, lsyn_u0smr 	SCLL Sync Output Enable Bit, 0 = Disabled
    b2, bbs_u0smr	Bus Busy Flag, 0 = Stop Condition Detected
    b1, abc_u0smr	Arbitration Lost Detect Flag Control Bit, 0 = Update per bit
    b0, iicm_u0smr	I2C Mode Select Bit, 0 = Except I2C mode

    u0smr2 0366h UART0 Special Mode Register 2, default value after reset = 0000 0000

    b7, su1him_u0smr2 	External clock Synchronous Enable Bit, related to sclkdiv_u0smr, 00 = No synchronization
    b6, sdhi_u0smr2 	SDA Output Inhibit Bit, 0 = Output
    b5, swc2_u0smr2 	SCL Wait Output Bit 2, 0 = Transfer clock
    b4, stc_u0smr2 	UART0 Initialize Bit, 0 = Disabled
    b3, als_u0smr2 	SDA Output Stop Bit, 0 = Disabled
    b2, swc_u0smr2 	SCL Wait Output Bit, 0 = Disabled
    b1, csc_u0smr2 	Clock Synchronous Bit, 0 = Disabled
    b0, iicm2_u0smr2 	I2C Mode Select Bit 2,

    u0smr3 0365h UART0 Special Mode Register 3, default value after reset = 0000 0000

		SDAI0 : Digital Delay Time Set Bit
		000 = No delay
    b7, dl2_u0smr3 = 0
    b6, dl1_u0smr3 = 0
    b5, dl0_u0smr3 = 0
    b4, err_u0smr3 Fault Error Flag, 0 = No Error
    b3, nodc_u0smr3 Clock Output Select Bit, 0 = CMOS ouptut
    b2, dinc_u0smr3 Serial Input Port Set Bit, 0 = select the TxD0 and RxD0 pins (master mode)
    b1, ckph_u0smr3 Clock Phase Set Bit, 0 = No clock delay
    b0, sse_u0smr3 #SS Pin Function Enable Bit, 0 = Disables #SS pin function
    			Note : set this pin AFTER set crd_u0c0 = 1 (Disables #CTS/#RTS function)

    u0smr4 0364h UART0 Special Mode Register 4, default value after reset = 0000 0000

    b7, swc9_u0smr4     SCL Wait Output Bit 3, 0 = SCL "L" hold disabled
    b6, sclhi_u0smr4    SCL Output Stop Enable Bit, 0 = Disabled
    b5, ackc_u0smr4     ACK Data Output Enable Bit, 0 = Serial I/O data output
    b4, ackd_u0smr4     ACK Data Bit, 0 = ACK
    b3, stspsel_u0smr4  SCL, SDA Output Select Bit, 0 = Selects the serial I/O circuit
    b2, stpreq_u0smr4   Stop Condition Generate Bit, 0 = Clear
    b1, rstareq_u0smr4  Restart Condition Generate Bit, 0 = Clear
    b0, stareq_u0smr4   Start Condition Generate Bit, 0 = Clear

    u0c0 036Ch UART0 Transmit/Receive Control Register 0, default value after reset = 0000 1000

    b7, uform_u0c0     Trasfer Format Select, 0 = LSB first
    b6, ckpol_u0c0     Clock Polarity Select, 0 = Data is transmitted on the falling edge of the transfer clock and data is received on the rising edge
    b5, nch_u0c0       Data output Select, 0 = TxD0/SDA0 and SCL0 are ports for the CMOS output
    b4, crd_u0c0       #CTS/#RTS Disabled, 1 = Disables #CTS/#RTS function
    b3, txept_u0c0     Transmit Register Empty Flag, 0 = Data in transmit register (during transmission)
    b2, crs_u0c0       #CTS/#RTS Function Select, 0 = Selects #CTS function (only when crd_u0c0 == 0)
    b1, clk1_u0c0 c1 : u0brg Count Source Select
    b0, clk0_u0c0 c0 : u0brg Count Source Select

	    c1 c0
	     0  0 f1
	     0  1 f8
	     1  0 f2n
	     1  1 forbidden

    u0c1 036Dh UART0 Transmit/Receive Control Register 1, default value after reset = 0000 0010

		      Note : set this bit AFTER setting the bits  [smd2_u0mr,smd1_u0mr,smd0_u0mr] at u0mr
    b7, sclkstpb_u0c1 Clock-Divided Synchronous Stop Bit
    b7, u0ere_u0c1    Error Signal output enable Bit, 0 = Not output
    b6, u0lch_u0c1    Data Logic Select Bit, 0 = Not inverted
    b5, u0rrm_u0c1    UART0 Contiuous Receive Mode Enable Bit, 0 = disables Contiuous Receive Mode to be entered
    b4, u0irs_u0c1    UART0 Transmit Interrupt Cause Select Bit,
			0 = No data in the u0tb	(when ti_u0c1 == 1, no data in the u0tb register)
			    The interrupt is generated when data is transferred from the u0tb register to
			    the UART0 transmit register
			1 = Transmission is completed (when txept_u0c0 == 1, no data in transmit register [transmission is completed])
			    The interrupt is generated when data transmission from the UART0 transfert register is completed
    b3, ri_u0c1       Receive Complet Flag, 0 = Data in the u0rb register
    b2, re_u0c1       Receive Enable Bit, 1 = Receive Enable
    b1, ti_u0c1       Transmit Buffer Empty Flag, 0 = Data in the u0tb register
    b0, te_u0c1       Transmit Enable Bit, 1 = Transmite enabled

    u0brg  0369h UART0 bit rate generator, default value after reset = xxxx xxxx

    set the baud rate [bps]: Clck[Hz]/[16*(m+1)]

     Clk = 24MHz
       9600 bps -> m = 155,	bit dif 0.17us
      19200 bps -> m =  77,	bit dif 0.08us
      38400 bps -> m =  38,	bit dif 0.04us
      57600 bps -> m =  25,	bit dif 0.03us
     115200 bps -> m =  12,	bit dif 0.01us

*/
    // -------------------------------------------------------------------------------------------------------

//  Configuration of UART0 for Terminal Communication

//  set 8N1 / Internal Clock

//  iopol_u0mr = 0; // TxD, Rxd Input/Output Polarity Switch, 0 = Not inverted, default value after reset
//  prye_u0mr = 0; // Parity Enabled, 0 = parity disabled, default value after reset
//  pry_u0mr = 0; // Odd/Even Parity Select, 0 = Odd parity (only when parity enabled), default value after reset
//  stps_u0mr = 0; // Stop bit Length Select, 0 = 1 stop bit, default value after reset
//  ckdir_u0mr = 0; // Internal/External Clock Select, 0 = Internal clock, default value after reset
    // Serial I/O Mode Select,  s2 s1 s0 = 1  0  1 = UART mode (asynchronous), 8-bit transfer data
    smd2_u0mr = 1;
//  smd1_u0mr = 0; // default value after reset
    smd0_u0mr = 1;


//  sclkdiv_u0smr = 0; // Clock Divide Synchronous Bit, related to su1him_u0smr2, default value after reset
//  sss_u0smr = 0; // Transmit Start Condition Select Bit, 0 = Not Related to RxD0, default value after reset
//  acse_u0smr = 0; // Auto Clear Function Select Bit for Transmit Enable Bit, 0 = No autoclear function, default value after reset
//  abscs_u0smr = 0; // Bus Conflict Detect Sampling Clock Select Bit, 0 = Rising edge of transfer clock, default value after reset
//  lsyn_u0smr = 0; // SCLL Sync Output Enable Bit, 0 = Disabled, default value after reset
//  bbs_u0smr = 0; // Bus Busy Flag, 0 = Stop Condition Detected, default value after reset
//  abc_u0smr = 0; // Arbitration Lost Detect Flag Control Bit, 0 = Update per bit, default value after reset
//  iicm_u0smr = 0; // I2C Mode Select Bit, 0 = Except I2C mode, default value after reset

//  su1him_u0smr2 = 0; // External clock Synchronous Enable Bit, related to sclkdiv_u0smr, 00 = No synchronization, default value after reset
//  sdhi_u0smr2 = 0; // SDA Output Inhibit Bit, 0 = Output, default value after reset
//  swc2_u0smr2 = 0; // SCL Wait Output Bit 2, 0 = Transfer clock, default value after reset
//  stc_u0smr2 = 0; // UART0 Initialize Bit, 0 = Disabled, default value after reset
//  als_u0smr2 = 0; // SDA Output Stop Bit, 0 = Disabled, default value after reset
//  swc_u0smr2 = 0; // SCL Wait Output Bit, 0 = Disabled, default value after reset
//  csc_u0smr2 = 0; // Clock Synchronous Bit, 0 = Disabled, default value after reset
//  iicm2_u0smr2 = 0; // I2C Mode Select Bit 2,, default value after reset

//	SDAI0 Digital Delay Time Set
//	000 = No delay
//  dl2_u0smr3 = 0; // default value after reset
//  dl1_u0smr3 = 0; // default value after reset
//  dl0_u0smr3 = 0; // default value after reset
//  err_u0smr3 = 0; // Fault Error Flag, 0 = No Error, default value after reset
//  nodc_u0smr3 = 0; // Clock Output Select Bit, 0 = CMOS ouptut, default value after reset
//  dinc_u0smr3 = 0; // Serial Input Port Set Bit, 0 = select the TxD0 and RxD0 pins (master mode), default value after reset
//  ckph_u0smr3 = 0; // Clock Phase Set Bit, 0 = No clock delay, default value after reset
//  sse_u0smr3 = 0; // #SS Pin Function Enable Bit, 0 = Disables #SS pin function, default value after reset

//  swc9_u0smr4 = 0; // SCL Wait Output Bit 3, 0 = SCL "L" hold disabled, default value after reset
//  sclhi_u0smr4 = 0; // SCL Output Stop Enable Bit, 0 = Disabled, default value after reset
//  ackc_u0smr4 = 0; // ACK Data Output Enable Bit, 0 = Serial I/O data output, default value after reset
//  ackd_u0smr4 = 0; // ACK Data Bit, 0 = ACK, default value after reset
//  stspsel_u0smr4 = 0; // SCL, SDA Output Select Bit, 0 = Selects the serial I/O circuit, default value after reset
//  stpreq_u0smr4 = 0; // Stop Condition Generate Bit, 0 = Clear, default value after reset
//  rstareq_u0smr4 = 0; // Restart Condition Generate Bit, 0 = Clear, default value after reset
//  stareq_u0smr4 = 0; // Start Condition Generate Bit, 0 = Clear, default value after reset

//  uform_u0c0 = 0; // Trasfer Format Select, 0 = LSB first, default value after reset
//  ckpol_u0c0 = 0;  // Clock Polarity Select, 0 = Data is transmitted on the falling edge of the transfer clock and data is received on the rising edge, default value after reset
//  nch_u0c0 = 0; // Data output Select, 0 = TxD0/SDA0 and SCL0 are ports for the CMOS output, default value after reset

    crd_u0c0 = 1; // #CTS/#RTS Disabled, 1 = Disables #CTS/#RTS function
    txept_u0c0 = 0; // Transmit Register Empty Flag, 0 = Data in transmit register (during transmission)

//  crs_u0c0 = 0; // #CTS/#RTS Function Select, 0 = Selects #CTS function (only when crd_u0c0 == 0), default value after reset

// u0brg Count Source Select, c1 c0 = 0 0 = f1
//  clk1_u0c0 = 0; // default value after reset
//  clk0_u0c0 = 0; // default value after reset

    u0brg = 155; // Clk = 24MHz, 38400 bps -> m =  38,	bit dif 0.04us

    // Rx disabled and Tx disabled

//  sclkstpb_u0c1 = 0; // default value after reset
//  u0ere_u0c1 = 0; // default value after reset
//  u0lch_u0c1 = 0; // default value after reset
//  u0rrm_u0c1 = 0; // default value after reset
//  u0irs_u0c1 = 0; // default value after reset
//  ri_u0c1 = 0; // default value after reset
//  re_u0c1 = 0; // Receive Enable Bit, 0 = Receive disabled, default value after reset
    ti_u0c1 = 0; // Transmit Buffer Empty Flag, 0 = Data in the u0tb register
//  te_u0c1 = 0; // Transmit Enable Bit, 0 = Transmite disabled, default value after reset
    // -------------------------------------------------------------------------------------------------------
/*
    UART1 related registers

    u1mr 02E8h UART1 Transmit/Receive Mode Register, default value after reset = 0000 0000

    b7, iopol_u1mr      TxD, Rxd Input/Output Polarity Switch Bit, 0 = Not inverted
    b6, prye_u1mr       Parity Enabled Bit, 0 = parity disabled
    b5, pry_u1mr        Odd/Even Parity Select Bit, 0 = Odd parity (only when parity enabled)
    b4, stps_u1mr       Stop bit Length Select Bit, 0 = 1 stop bit
    b3, ckdir_u1mr      Internal/External Clock Select Bit, 0 = Internal clock
    b2, smd2_u1mr  s2 : Serial I/O Mode Select
    b1, smd1_u1mr  s1 : Serial I/O Mode Select
    b0, smd0_u1mr  s0 : Serial I/O Mode Select

        s2 s1 s0
	 0  0  0  Serial interface disabled
	 0  0  1  Clock Synchronous mode
	 0  1  0  I2C mode
	 0  1  1
	 1  0  0  UART mode (asynchronous), 7-bit transfer data
	 1  0  1  UART mode (asynchronous), 8-bit transfer data
	 1  1  0  UART mode (asynchronous), 9-bit transfer data
	 1  1  1

    u1smr 02E7h UART1 Special Mode Register, default value after reset = 0000 0000

    b7, sclkdiv_u1smr	Clock Divide Synchronous Bit, related to su1him_u1smr2
    b6, sss_u1smr 	Transmit Start Condition Select Bit, 0 = Not Related to RxD1
    b5, acse_u1smr 	Auto Clear Function Select Bit for Transmit Enable Bit, 0 = No autoclear function
    b4, abscs_u1smr 	Bus Conflict Detect Sampling Clock Select Bit, 0 = Rising edge of transfer clock
    b3, lsyn_u1smr 	SCLL Sync Output Enable Bit, 0 = Disabled
    b2, bbs_u1smr	Bus Busy Flag, 0 = Stop Condition Detected
    b1, abc_u1smr	Arbitration Lost Detect Flag Control Bit, 0 = Update per bit
    b0, iicm_u1smr	I2C Mode Select Bit, 0 = Except I2C mode

    u1smr2 02E6h UART1 Special Mode Register 2, default value after reset = 0000 0000

    b7, su1him_u1smr2 	External clock Synchronous Enable Bit, related to sclkdiv_u1smr, 00 = No synchronization
    b6, sdhi_u1smr2 	SDA Output Inhibit Bit, 0 = Output
    b5, swc2_u1smr2 	SCL Wait Output Bit 2, 0 = Transfer clock
    b4, stc_u1smr2 	UART1 Initialize Bit, 0 = Disabled
    b3, als_u1smr2 	SDA Output Stop Bit, 0 = Disabled
    b2, swc_u1smr2 	SCL Wait Output Bit, 0 = Disabled
    b1, csc_u1smr2 	Clock Synchronous Bit, 0 = Disabled
    b0, iicm2_u1smr2 	I2C Mode Select Bit 2,

    u1smr3 02E5h UART1 Special Mode Register 3, default value after reset = 0000 0000

		SDAI1 : Digital Delay Time Set Bit
		000 = No delay
    b7, dl2_u1smr3 = 0
    b6, dl1_u1smr3 = 0
    b5, dl0_u1smr3 = 0
    b4, err_u1smr3 Fault Error Flag, 0 = No Error
    b3, nodc_u1smr3 Clock Output Select Bit, 0 = CMOS ouptut
    b2, dinc_u1smr3 Serial Input Port Set Bit, 0 = select the TxD1 and RxD1 pins (master mode)
    b1, ckph_u1smr3 Clock Phase Set Bit, 0 = No clock delay
    b0, sse_u1smr3 #SS Pin Function Enable Bit, 0 = Disables #SS pin function
    			Note : set this pin AFTER set crd_u1c0 = 1 (Disables #CTS/#RTS function)

    u1smr4 02E4h UART1 special mode register 4, default value after reset = 0000 0000

    b7, swc9_u1smr4     SCL Wait Output Bit 3, 0 = SCL "L" hold disabled
    b6, sclhi_u1smr4    SCL Output Stop Enable Bit, 0 = Disabled
    b5, ackc_u1smr4     ACK Data Output Enable Bit, 0 = Serial I/O data output
    b4, ackd_u1smr4     ACK Data Bit, 0 = ACK
    b3, stspsel_u1smr4  SCL, SDA Output Select Bit, 0 = Selects the serial I/O circuit
    b2, stpreq_u1smr4   Stop Condition Generate Bit, 0 = Clear
    b1, rstareq_u1smr4  Restart Condition Generate Bit, 0 = Clear
    b0, stareq_u1smr4   Start Condition Generate Bit, 0 = Clear

    u1c0 02ECh UART1 Transmit/Receive Control Register 0, default value after reset = 0000 1000

    b7, uform_u1c0     Trasfer Format Select, 0 = LSB first
    b6, ckpol_u1c0     Clock Polarity Select, 0 = Data is transmitted on the falling edge of the transfer clock and data is received on the rising edge
    b5, nch_u1c0       Data output Select, 0 = TxD1/SDA1 and SCL1 are ports for the CMOS output
    b4, crd_u1c0       #CTS/#RTS Disabled, 1 = Disables #CTS/#RTS function
    b3, txept_u1c0     Transmit Register Empty Flag, 0 = Data in transmit register (during transmission)
    b2, crs_u1c0       #CTS/#RTS Function Select, 0 = Selects #CTS function (only when crd_u1c0 == 0)
    b1, clk1_u1c0 c1 : u1brg Count Source Select
    b0, clk0_u1c0 c0 : u1brg Count Source Select

	    c1 c0
	     0  0 f1
	     0  1 f8
	     1  0 f2n
	     1  1 forbidden

    u1c1 02EDh UART1 transmit/receive control register 1, default value after reset = 0000 0010

		      Note : set this bit AFTER setting the bits  [smd2_u1mr,smd1_u1mr,smd0_u1mr] at u1mr
    b7, sclkstpb_u1c1 Clock-Divided Synchronous Stop Bit
    b7, u0ere_u1c1    Error Signal output enable Bit, 0 = Not output
    b6, u0lch_u1c1    Data Logic Select Bit, 0 = Not inverted
    b5, u0rrm_u1c1    UART1 Contiuous Receive Mode Enable Bit, 0 = disables Contiuous Receive Mode to be entered
    b4, u0irs_u1c1    UART1 Transmit Interrupt Cause Select Bit,
			0 = No data in the u1tb	(when ti_u1c1 == 1, no data in the u1tb register)
			    The interrupt is generated when data is transferred from the u1tb register to
			    the UART1 transmit register
			1 = Transmission is completed (when txept_u1c0 == 1, no data in transmit register [transmission is completed])
			    The interrupt is generated when data transmission from the UART1 transfert register is completed
    b3, ri_u1c1       Receive Complet Flag, 0 = Data in the u1rb register
    b2, re_u1c1       Receive Enable Bit, 1 = Receive Enable
    b1, ti_u1c1       Transmit Buffer Empty Flag, 0 = Data in the u1tb register
    b0, te_u1c1       Transmit Enable Bit, 1 = Transmite enabled

    u1brg  02E9h UART1 bit rate generator, default value after reset = xxxx xxxx

    set the baud rate [bps]: Clck[Hz]/[16*(m+1)]

     Clk = 24MHz
       9600 bps -> m = 155,	bit dif 0.17us
      19200 bps -> m =  77,	bit dif 0.08us
      38400 bps -> m =  38,	bit dif 0.04us
      57600 bps -> m =  25,	bit dif 0.03us
     115200 bps -> m =  12,	bit dif 0.01us

*/
    // -------------------------------------------------------------------------------------------------------

#ifdef DEBUG_VERSION
    // not use this interrupts
    // UART1 Receive Interrupt
    // UART1 Transmit Interrupt
#else

//  Configuration of UART1 for Terminal Communication

//  set 8N1 / Internal Clock

//  iopol_u1mr = 0; // TxD, Rxd Input/Output Polarity Switch, 0 = Not inverted, default value after reset
//  prye_u1mr = 0; // Parity Enabled, 0 = parity disabled, default value after reset
//  pry_u1mr = 0; // Odd/Even Parity Select, 0 = Odd parity (only when parity enabled), default value after reset
//  stps_u1mr = 0; // Stop bit Length Select, 0 = 1 stop bit, default value after reset
//  ckdir_u1mr = 0; // Internal/External Clock Select, 0 = Internal clock, default value after reset
    // Serial I/O Mode Select,  s2 s1 s0 = 1  0  1 = UART mode (asynchronous), 8-bit transfer data
    smd2_u1mr = 1;
//  smd1_u1mr = 0; // default value after reset
    smd0_u1mr = 1;

//  sclkdiv_u1smr = 0; // Clock Divide Synchronous Bit, related to su1him_u1smr2, default value after reset
//  sss_u1smr = 0; // Transmit Start Condition Select Bit, 0 = Not Related to RxD1, default value after reset
//  acse_u1smr = 0; // Auto Clear Function Select Bit for Transmit Enable Bit, 0 = No autoclear function, default value after reset
//  abscs_u1smr = 0; // Bus Conflict Detect Sampling Clock Select Bit, 0 = Rising edge of transfer clock, default value after reset
//  lsyn_u1smr = 0; // SCLL Sync Output Enable Bit, 0 = Disabled, default value after reset
//  bbs_u1smr = 0; // Bus Busy Flag, 0 = Stop Condition Detected, default value after reset
//  abc_u1smr = 0; // Arbitration Lost Detect Flag Control Bit, 0 = Update per bit, default value after reset
//  iicm_u1smr = 0; // I2C Mode Select Bit, 0 = Except I2C mode, default value after reset

//  su1him_u1smr2 = 0; // External clock Synchronous Enable Bit, related to sclkdiv_u0smr, 00 = No synchronization, default value after reset
//  sdhi_u1smr2 = 0; // SDA Output Inhibit Bit, 0 = Output, default value after reset
//  swc2_u1smr2 = 0; // SCL Wait Output Bit 2, 0 = Transfer clock, default value after reset
//  stc_u1smr2 = 0; // UART1 Initialize Bit, 0 = Disabled, default value after reset
//  als_u1smr2 = 0; // SDA Output Stop Bit, 0 = Disabled, default value after reset
//  swc_u1smr2 = 0; // SCL Wait Output Bit, 0 = Disabled, default value after reset
//  csc_u1smr2 = 0; // Clock Synchronous Bit, 0 = Disabled, default value after reset
//  iicm2_u1smr2 = 0; // I2C Mode Select Bit 2,, default value after reset

//	SDAI1 Digital Delay Time Set
//	000 = No delay
//  dl2_u1smr3 = 0; // default value after reset
//  dl1_u1smr3 = 0; // default value after reset
//  dl0_u1smr3 = 0; // default value after reset
//  err_u1smr3 = 0; // Fault Error Flag, 0 = No Error, default value after reset
//  nodc_u1smr3 = 0; // Clock Output Select Bit, 0 = CMOS ouptut, default value after reset
//  dinc_u1smr3 = 0; // Serial Input Port Set Bit, 0 = select the TxD1 and RxD1 pins (master mode), default value after reset
//  ckph_u1smr3 = 0; // Clock Phase Set Bit, 0 = No clock delay, default value after reset
//  sse_u1smr3 = 0; // #SS Pin Function Enable Bit, 0 = Disables #SS pin function, default value after reset

//  swc9_u1smr4 = 0; // SCL Wait Output Bit 3, 0 = SCL "L" hold disabled, default value after reset
//  sclhi_u1smr4 = 0; // SCL Output Stop Enable Bit, 0 = Disabled, default value after reset
//  ackc_u1smr4 = 0; // ACK Data Output Enable Bit, 0 = Serial I/O data output, default value after reset
//  ackd_u1smr4 = 0; // ACK Data Bit, 0 = ACK, default value after reset
//  stspsel_u1smr4 = 0; // SCL, SDA Output Select Bit, 0 = Selects the serial I/O circuit, default value after reset
//  stpreq_u1smr4 = 0; // Stop Condition Generate Bit, 0 = Clear, default value after reset
//  rstareq_u1smr4 = 0; // Restart Condition Generate Bit, 0 = Clear, default value after reset
//  stareq_u1smr4 = 0; // Start Condition Generate Bit, 0 = Clear, default value after reset

//  uform_u1c0 = 0; // Trasfer Format Select, 0 = LSB first, default value after reset
//  ckpol_u1c0 = 0;  // Clock Polarity Select, 0 = Data is transmitted on the falling edge of the transfer clock and data is received on the rising edge, default value after reset
//  nch_u1c0 = 0; // Data output Select, 0 = TxD1/SDA1 and SCL1 are ports for the CMOS output, default value after reset

    crd_u1c0 = 1; // #CTS/#RTS Disabled, 1 = Disables #CTS/#RTS function
    txept_u1c0 = 0; // Transmit Register Empty Flag, 0 = Data in transmit register (during transmission)

//  crs_u1c0 = 0; // #CTS/#RTS Function Select, 0 = Selects #CTS function (only when crd_u1c0 == 0), default value after reset

// u0brg Count Source Select, c1 c0 = 0 0 = f1
//  clk1_u1c0 = 0; // default value after reset
//  clk0_u1c0 = 0; // default value after reset

    u1brg = 38; // Clk = 24MHz, 38400 bps -> m =  38,	bit dif 0.04us

    // Rx disabled and Tx disabled

//  sclkstpb_u1c1 = 0; // default value after reset
//  u0ere_u1c1 = 0; // default value after reset
//  u0lch_u1c1 = 0; // default value after reset
//  u0rrm_u1c1 = 0; // default value after reset
//  u0irs_u1c1 = 0; // default value after reset
//  ri_u1c1 = 0; // default value after reset
//  re_u1c1 = 0; // Receive Enable Bit, 0 = Receive disabled, default value after reset
    ti_u1c1 = 0; // Transmit Buffer Empty Flag, 0 = Data in the u1tb register
//  te_u1c1 = 0; // Transmit Enable Bit, 0 = Transmite disabled, default value after reset

#endif
    // -------------------------------------------------------------------------------------------------------
/*
    UART2 related registers

    u2mr 0338h UART2 Transmit/Receive Mode Register, default value after reset = 0000 0000

    b7, iopol_u2mr      TxD, Rxd Input/Output Polarity Switch Bit, 0 = Not inverted
    b6, prye_u2mr       Parity Enabled Bit, 0 = parity disabled
    b5, pry_u2mr        Odd/Even Parity Select Bit, 0 = Odd parity (only when parity enabled)
    b4, stps_u2mr       Stop bit Length Select Bit, 0 = 1 stop bit
    b3, ckdir_u2mr      Internal/External Clock Select Bit, 0 = Internal clock
    b2, smd2_u2mr  s2 : Serial I/O Mode Select
    b1, smd1_u2mr  s1 : Serial I/O Mode Select
    b0, smd0_u2mr  s0 : Serial I/O Mode Select

        s2 s1 s0
	 0  0  0  Serial interface disabled
	 0  0  1  Clock Synchronous mode
	 0  1  0  I2C mode
	 0  1  1
	 1  0  0  UART mode (asynchronous), 7-bit transfer data
	 1  0  1  UART mode (asynchronous), 8-bit transfer data
	 1  1  0  UART mode (asynchronous), 9-bit transfer data
	 1  1  1

    u2smr 0337h UART2 Special Mode Register, default value after reset = 0000 0000

    b7, sclkdiv_u2smr	Clock Divide Synchronous Bit, related to su1him_u2smr2
    b6, sss_u2smr 	Transmit Start Condition Select Bit, 0 = Not Related to RxD2
    b5, acse_u2smr 	Auto Clear Function Select Bit for Transmit Enable Bit, 0 = No autoclear function
    b4, abscs_u2smr 	Bus Conflict Detect Sampling Clock Select Bit, 0 = Rising edge of transfer clock
    b3, lsyn_u2smr 	SCLL Sync Output Enable Bit, 0 = Disabled
    b2, bbs_u2smr	Bus Busy Flag, 0 = Stop Condition Detected
    b1, abc_u2smr	Arbitration Lost Detect Flag Control Bit, 0 = Update per bit
    b0, iicm_u2smr	I2C Mode Select Bit, 0 = Except I2C mode

    u2smr2 0336h UART2 Special Mode Register 2, default value after reset = 0000 0000

    b7, su1him_u2smr2 	External clock Synchronous Enable Bit, related to sclkdiv_u2smr, 00 = No synchronization
    b6, sdhi_u2smr2 	SDA Output Inhibit Bit, 0 = Output
    b5, swc2_u2smr2 	SCL Wait Output Bit 2, 0 = Transfer clock
    b4, stc_u2smr2 	UART0 Initialize Bit, 0 = Disabled
    b3, als_u2smr2 	SDA Output Stop Bit, 0 = Disabled
    b2, swc_u2smr2 	SCL Wait Output Bit, 0 = Disabled
    b1, csc_u2smr2 	Clock Synchronous Bit, 0 = Disabled
    b0, iicm2_u2smr2 	I2C Mode Select Bit 2,

    u2smr3 0335h UART2 Special Mode Register 3, default value after reset = 0000 0000

		SDAI0 : Digital Delay Time Set Bit
		000 = No delay
    b7, dl2_u2smr3 = 0
    b6, dl1_u2smr3 = 0
    b5, dl0_u2smr3 = 0
    b4, err_u2smr3 Fault Error Flag, 0 = No Error
    b3, nodc_u2smr3 Clock Output Select Bit, 0 = CMOS ouptut
    b2, dinc_u2smr3 Serial Input Port Set Bit, 0 = select the TxD2 and RxD2 pins (master mode)
    b1, ckph_u2smr3 Clock Phase Set Bit, 0 = No clock delay
    b0, sse_u2smr3 #SS Pin Function Enable Bit, 0 = Disables #SS pin function
    			Note : set this pin AFTER set crd_u2c0 = 1 (Disables #CTS/#RTS function)

    u2smr4 0334h UART2 special mode register 4, default value after reset = 0000 0000

    b7, swc9_u2smr4     SCL Wait Output Bit 3, 0 = SCL "L" hold disabled
    b6, sclhi_u2smr4    SCL Output Stop Enable Bit, 0 = Disabled
    b5, ackc_u2smr4     ACK Data Output Enable Bit, 0 = Serial I/O data output
    b4, ackd_u2smr4     ACK Data Bit, 0 = ACK
    b3, stspsel_u2smr4  SCL, SDA Output Select Bit, 0 = Selects the serial I/O circuit
    b2, stpreq_u2smr4   Stop Condition Generate Bit, 0 = Clear
    b1, rstareq_u2smr4  Restart Condition Generate Bit, 0 = Clear
    b0, stareq_u2smr4   Start Condition Generate Bit, 0 = Clear

    u2c0 033Ch UART2 Transmit/Receive Control Register 0, default value after reset = 0000 1000

    b7, uform_u2c0     Trasfer Format Select, 0 = LSB first
    b6, ckpol_u2c0     Clock Polarity Select, 0 = Data is transmitted on the falling edge of the transfer clock and data is received on the rising edge
    b5, nch_u2c0       Data output Select, 0 = TxD2/SDA2 and SCL2 are ports for the CMOS output
    b4, crd_u2c0       #CTS/#RTS Disabled, 1 = Disables #CTS/#RTS function
    b3, txept_u2c0     Transmit Register Empty Flag, 0 = Data in transmit register (during transmission)
    b2, crs_u2c0       #CTS/#RTS Function Select, 0 = Selects #CTS function (only when crd_u2c0 == 0)
    b1, clk1_u2c0 c1 : u1brg Count Source Select
    b0, clk0_u2c0 c0 : u1brg Count Source Select

	    c1 c0
	     0  0 f1
	     0  1 f8
	     1  0 f2n
	     1  1 forbidden

    u2c1 033Dh UART2 transmit/receive control register 1, default value after reset = 0000 0010

		      Note : set this bit AFTER setting the bits  [smd2_u2mr,smd1_u2mr,smd0_u2mr] at u2mr
    b7, sclkstpb_u2c1 Clock-Divided Synchronous Stop Bit
    b7, u0ere_u2c1    Error Signal output enable Bit, 0 = Not output
    b6, u0lch_u2c1    Data Logic Select Bit, 0 = Not inverted
    b5, u0rrm_u2c1    UART2 Contiuous Receive Mode Enable Bit, 0 = disables Contiuous Receive Mode to be entered
    b4, u0irs_u2c1    UART2 Transmit Interrupt Cause Select Bit,
			0 = No data in the u2tb	(when ti_u2c1 == 1, no data in the u2tb register)
			    The interrupt is generated when data is transferred from the u2tb register to
			    the UART2 transmit register
			1 = Transmission is completed (when txept_u2c0 == 1, no data in transmit register [transmission is completed])
			    The interrupt is generated when data transmission from the UART2 transfert register is completed
    b3, ri_u2c1       Receive Complet Flag, 0 = Data in the u2rb register
    b2, re_u2c1       Receive Enable Bit, 1 = Receive Enable
    b1, ti_u2c1       Transmit Buffer Empty Flag, 0 = Data in the u2tb register
    b0, te_u2c1       Transmit Enable Bit, 1 = Transmite enabled

    u2brg  0339h UART2 bit rate generator, default value after reset = xxxx xxxx

    set the baud rate [bps]: Clck[Hz]/[16*(m+1)]

     Clk = 24MHz
       9600 bps -> m = 155,	bit dif 0.17us
      19200 bps -> m =  77,	bit dif 0.08us
      38400 bps -> m =  38,	bit dif 0.04us
      57600 bps -> m =  25,	bit dif 0.03us
     115200 bps -> m =  12,	bit dif 0.01us

*/
    // -------------------------------------------------------------------------------------------------------

//  Configuration of UART2 for SPY (synchronous serial link to reach higher baud rate)

//  set Synchronous, Int clk, 1 stop bit, no parity

//  iopol_u2mr = 0; // TxD, Rxd Input/Output Polarity Switch, 0 = Not inverted, default value after reset
//  prye_u2mr = 0; // Parity Enabled, 0 = parity disabled, default value after reset
//  pry_u2mr = 0; // Odd/Even Parity Select, 0 = Odd parity (only when parity enabled), default value after reset
//  stps_u2mr = 0; // Stop bit Length Select, 0 = 1 stop bit, default value after reset
//  ckdir_u2mr = 0; // Internal/External Clock Select, 0 = Internal clock, default value after reset
    // Serial I/O Mode Select,  s2 s1 s0 = 0  0  1  Clock Synchronous mode
//  smd2_u2mr = 0;
//  smd1_u2mr = 0;
    smd0_u2mr = 1;


//  sclkdiv_u2smr = 0; // Clock Divide Synchronous Bit, related to su1him_u2smr2, default value after reset
//  sss_u2smr = 0; // Transmit Start Condition Select Bit, 0 = Not Related to RxD2, default value after reset
//  acse_u2smr = 0; // Auto Clear Function Select Bit for Transmit Enable Bit, 0 = No autoclear function, default value after reset
//  abscs_u2smr = 0; // Bus Conflict Detect Sampling Clock Select Bit, 0 = Rising edge of transfer clock, default value after reset
//  lsyn_u2smr = 0; // SCLL Sync Output Enable Bit, 0 = Disabled, default value after reset
//  bbs_u2smr = 0; // Bus Busy Flag, 0 = Stop Condition Detected, default value after reset
//  abc_u2smr = 0; // Arbitration Lost Detect Flag Control Bit, 0 = Update per bit, default value after reset
//  iicm_u2smr = 0; // I2C Mode Select Bit, 0 = Except I2C mode, default value after reset

//  su1him_u2smr2 = 0; // External clock Synchronous Enable Bit, related to sclkdiv_u2smr, 00 = No synchronization, default value after reset
//  sdhi_u2smr2 = 0; // SDA Output Inhibit Bit, 0 = Output, default value after reset
//  swc2_u2smr2 = 0; // SCL Wait Output Bit 2, 0 = Transfer clock, default value after reset
//  stc_u2smr2 = 0; // UART1 Initialize Bit, 0 = Disabled, default value after reset
//  als_u2smr2 = 0; // SDA Output Stop Bit, 0 = Disabled, default value after reset
//  swc_u2smr2 = 0; // SCL Wait Output Bit, 0 = Disabled, default value after reset
//  csc_u2smr2 = 0; // Clock Synchronous Bit, 0 = Disabled, default value after reset
//  iicm2_u2smr2 = 0; // I2C Mode Select Bit 2, default value after reset

//	SDAI1 Digital Delay Time Set
//	000 = No delay
//  dl2_u2smr3 = 0; // default value after reset
//  dl1_u2smr3 = 0; // default value after reset
//  dl0_u2smr3 = 0; // default value after reset
//  err_u2smr3 = 0; // Fault Error Flag, 0 = No Error, default value after reset
//  nodc_u2smr3 = 0; // Clock Output Select Bit, 0 = CMOS ouptut, default value after reset
//  dinc_u2smr3 = 0; // Serial Input Port Set Bit, 0 = select the TxD2 and RxD2 pins (master mode), default value after reset
//  ckph_u2smr3 = 0; // Clock Phase Set Bit, 0 = No clock delay, default value after reset
//  sse_u2smr3 = 0; // #SS Pin Function Enable Bit, 0 = Disables #SS pin function, default value after reset

//  swc9_u2smr4 = 0; // SCL Wait Output Bit 3, 0 = SCL "L" hold disabled, default value after reset
//  sclhi_u2smr4 = 0; // SCL Output Stop Enable Bit, 0 = Disabled, default value after reset
//  ackc_u2smr4 = 0; // ACK Data Output Enable Bit, 0 = Serial I/O data output, default value after reset
//  ackd_u2smr4 = 0; // ACK Data Bit, 0 = ACK, default value after reset
//  stspsel_u2smr4 = 0; // SCL, SDA Output Select Bit, 0 = Selects the serial I/O circuit, default value after reset
//  stpreq_u2smr4 = 0; // Stop Condition Generate Bit, 0 = Clear, default value after reset
//  rstareq_u2smr4 = 0; // Restart Condition Generate Bit, 0 = Clear, default value after reset
//  stareq_u2smr4 = 0; // Start Condition Generate Bit, 0 = Clear, default value after reset

    uform_u2c0 = 1; // Trasfer Format Select, 1 = MSB first
//  ckpol_u2c0 = 0;  // Clock Polarity Select, 0 = Data is transmitted on the falling edge of the transfer clock and data is received on the rising edge, default value after reset
//  nch_u2c0 = 0; // Data output Select, 0 = TxD2/SDA2 and SCL2 are ports for the CMOS output, default value after reset
//  crd_u2c0 = 0; // #CTS/#RTS enabled
    txept_u2c0 = 0; // Transmit Register Empty Flag, 0 = Data in transmit register (during transmission)
//  crs_u2c0 = 0; // #CTS/#RTS Function Select, 0 = Selects #CTS function (only when crd_u2c0 == 0), default value after reset
// u2brg Count Source Select, c1 c0 = 0 0 = f1
//  clk1_u2c0 = 0; // default value after reset
//  clk0_u2c0 = 0; // default value after reset

    u2brg = 5; // Clk = 24MHz, 38400 bps -> m = 38,	bit dif 0.04us

    // Rx disabled and Tx disabled

//  sclkstpb_u2c1 = 0; // default value after reset
//  u0ere_u2c1 = 0; // default value after reset
//  u0lch_u2c1 = 0; // default value after reset
//  u0rrm_u2c1 = 0; // default value after reset
//  u0irs_u2c1 = 0; // default value after reset
//  ri_u2c1 = 0; // default value after reset
//  re_u2c1 = 0; // Receive Enable Bit, 0 = Receive disabled, default value after reset
//  ti_u2c1 = 0; // Transmit Buffer Empty Flag, 0 = Data in the u1tb register
//  te_u2c1 = 0; // Transmit Enable Bit, 0 = Transmite disabled, default value after reset

    // -------------------------------------------------------------------------------------------------------
}
/*---------------------------------------------------------------------------------------------------------*/
void M32C87_EnableInts(void)
/*---------------------------------------------------------------------------------------------------------*\

    Maskable interrupts are enabled and disabled by using
    a) the interrupt enable flag (I flag),
    b) interrupt priority level select bit
    c) processor interrupt priority level (IPL).

\*---------------------------------------------------------------------------------------------------------*/
{
/*
	changing priority level from 000 (on power up) to != 0 enables the interrupts

	Interrupt related registers

	ta0ic 006Ch Timer A0 Interrupt Control Register, default value after reset = xxxx x000

	b7,
	b6,
	b5,
	b4,
	b3, ir_ta0ic         (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_ta0ic = 1; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_ta0ic = 0;
	b0, ilvl0_ta0ic = 1;


	int0ic 009Eh Interrupt Control Register, default value after reset = xx00 x000

	b7, 0
	b6, 0
	b5, lvs_int0ic = 0; Level Sense/Edge Sense Switch Bit, 0 = Edge Sense, 1 = Level Sense
			    Note : when this bit is 1 (Level Sense) then ifsr1 must be 0 (One Edge)
	b4, pol_int0ic = 0; Polarity Switch Bit, 0 = Selects Falling Edge or "L", 1 = Selects Rising Edge or "H"
			    Note : when ifsr0 == 1 (Both Edges) then this this bit must be 0 (Falling Edge or "L")
	b3, ir_int0ic = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_int0ic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_int0ic = 0;
	b0, ilvl0_int0ic = 1;


	s0ric 0072h UART0 Receive Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s0ric = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s0ric = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s0ric = 0;
	b0, ilvl0_s0ric = 1;

	s0tic 0090h UART0 Transmit Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s0tic = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s0tic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s0tic = 0;
	b0, ilvl0_s0tic = 1;

	s1ric 0074h UART1 Receive Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s1ric = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s1ric = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s1ric = 0;
	b0, ilvl0_s1ric = 1;

	s1tic 0092h UART1 Transmit Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s1tic = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s1tic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s1tic = 0;
	b0, ilvl0_s1tic = 1;

	s2ric 006B UART2 Receive Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s2ric = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s2ric = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s2ric = 0;
	b0, ilvl0_s2ric = 1;

	s2tic 0089h UART2 Transmit Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s2tic = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s2tic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s2tic = 0;
	b0, ilvl0_s2tic = 1;

	s3ric 006D UART3 Receive Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s3ric = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s3ric = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s3ric = 0;
	b0, ilvl0_s3ric = 1;

	s3tic 008Bh UART3 Transmit Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s3tic = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s3tic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s3tic = 0;
	b0, ilvl0_s3tic = 1;

	s4ric 006F UART4 Receive Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s4ric = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s4ric = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s4ric = 0;
	b0, ilvl0_s4ric = 1;

	s4tic 008Dh UART4 Transmit Interrupt Control Register, default value after reset = xxxx x000

	b7, 0
	b6, 0
	b5, 0
	b4, 0
	b3, ir_s4tic = 0; (Read Only) Interrupt Request Bit, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_s4tic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_s4tic = 0;
	b0, ilvl0_s4tic = 1;


	ad0ic 0073h A/D0 Interrupt Control Register, default value after reset = xxxx ?000

	b7,
	b6,
	b5,
	b4,
	b3, ir_ad0ic     (Read Only) Interrupt Request, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_ad0ic  Interrupt Priority Level Select, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_ad0ic
	b0, ilvl0_ad0ic


	ad1ic 0086h A/D1 Interrupt Control Register, default value after reset = xxxx ?000

	b7,
	b6,
	b5,
	b4,
	b3, ir_ad1ic     (Read Only) Interrupt Request, 0 = no interrupt requested, 1 = interrupt requested (can be written with 0)
	b2, ilvl2_ad1ic  Interrupt Priority Level Select, 000 = Level 0, Interrupt Disabled
	b1, ilvl1_ad1ic
	b0, ilvl0_ad1ic
*/
    // -------------------------------------------------------------------------------------------------------
    // Enable all interrupt sources
    // -------------------------------------------------------------------------------------------------------
    // Timers
    ta0ic  = 0x05;				// Timer A0 source for OS tick interrupt, Level 5
    ta1ic  = 0x02;				// Timer A1 generates Level 2 interrupts

//  ta3ic  = 0x0?;				// Timer A3 is not generating interrupts
//  ta4ic  = 0x0?;				// Timer A4 is not generating interrupts

    // -------------------------------------------------------------------------------------------------------
    // Enable Interrupts from G64

    // edege sense, FALLING edge, Level 1

//  lvs_int0ic = 0; // Level Sense/Edge Sense Switch Bit, 0 = Edge Sense, default value after reset
//  pol_int0ic = 0; // Polarity Switch Bit, 0 = Selects Falling Edge or "L", default value after reset
//  ir_int0ic = 0;  // (Read Only) Interrupt Request Bit, default value after reset
// Set Level 001
//  ilvl2_int0ic = 0; // , default value after reset
//  ilvl1_int0ic = 0; // , default value after reset
    ilvl0_int0ic = 1;
    // -------------------------------------------------------------------------------------------------------
    // A/D0 Interrupt
    // Interrupt Priority Level Select, 000 = Level 0, Interrupt Disabled
//  ilvl2_ad0ic = 0; // default value after reset
//  ilvl1_ad0ic = 0; // default value after reset
//  ilvl0_ad0ic = 0; // default value after reset
    // -------------------------------------------------------------------------------------------------------
    // UART0 Receive Interrupt
    // set Level 1 interrupt

    ir_s0ric = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s0ric = 0; // default value after reset
//  ilvl1_s0ric = 0; // default value after reset
    ilvl0_s0ric = 1;
    // -------------------------------------------------------------------------------------------------------
    // UART0 Transmit Interrupt
    // set Level 1 interrupt

    ir_s0tic = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s0tic = 0; // default value after reset
//  ilvl1_s0tic = 0; // default value after reset
    ilvl0_s0tic = 1;
    // -------------------------------------------------------------------------------------------------------
#ifdef DEBUG_VERSION
    // not use this interrupts
    // UART1 Receive Interrupt
    // UART1 Transmit Interrupt
#else
    // UART1 Receive Interrupt
    // set Level 1 interrupt

    ir_s1ric = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s1ric = 0; // default value after reset
//  ilvl1_s1ric = 0; // default value after reset
    ilvl0_s1ric = 1;
    // -------------------------------------------------------------------------------------------------------
    // UART1 Transmit Interrupt
    // set Level 1 interrupt

    ir_s1tic = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s1tic = 0; // default value after reset
//  ilvl1_s1tic = 0; // default value after reset
    ilvl0_s1tic = 1;
#endif
    // -------------------------------------------------------------------------------------------------------
    // UART2 Receive Interrupt
    // set Level 1 interrupt

    ir_s2ric = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s2ric = 0; // default value after reset
//  ilvl1_s2ric = 0; // default value after reset
    ilvl0_s2ric = 1;
    // -------------------------------------------------------------------------------------------------------
    // UART2 Transmit Interrupt
    // set Level 1 interrupt

    ir_s2tic = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s2tic = 0; // default value after reset
//  ilvl1_s2tic = 0; // default value after reset
    ilvl0_s2tic = 1;
    // -------------------------------------------------------------------------------------------------------
    // UART3 Receive Interrupt
    // set Level 1 interrupt

    ir_s3ric = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s3ric = 0; // default value after reset
//  ilvl1_s3ric = 0; // default value after reset
    ilvl0_s3ric = 1;
    // -------------------------------------------------------------------------------------------------------
    // UART3 Transmit Interrupt
    // set Level 1 interrupt

    ir_s3tic = 0; // acknowledge interrupt, to not have any pending interrupt

//  ilvl2_s3tic = 0; // default value after reset
//  ilvl1_s3tic = 0; // default value after reset
    ilvl0_s3tic = 1;
    // -------------------------------------------------------------------------------------------------------
    // put watchdog timer to work
    // writing to wdts sets the watchdog timer to 7FFF, independant of the written value

    // wdts 0000Eh Watchdog Timer Register, default value after reset = xxxx xxxx
#ifdef NOT_USE_WATCHDOG
#else
    wdts = 0;				// reset watchdog timer, cycle about 262.144 ms
#endif
    // -------------------------------------------------------------------------------------------------------
    // start A/D0 conversion
    adst_ad0con0 = 1;
    // stop conversion
//  adst_ad0con0 = 0;
    // -------------------------------------------------------------------------------------------------------
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: hdw_c87.c
\*---------------------------------------------------------------------------------------------------------*/
/*

-----------------------------------------------------------
int 40, Bus conflict detect, Start condition detect, Stop condition detect, (UART3/UART0), Fault Error

the bit ifsr6 in ifsr (031Fh Interrupt cause select Register) selects between UART3 and UART0

bcn0ic 0071h Bus collision (UART0/UART3) interrupt control register, default value after reset = xxxx x000

    b7, 0
    b6, 0
    b5, 0
    b4, 0
    b3, ir_bcn0ic = 0; (Read Only) Interrupt Request Bit, 0 = Request no interrupt, 1 = requests and interrupt (can be written with 0)
    b2, ilvl2_bcn0ic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_bcn0ic = 0;
    b0, ilvl0_bcn0ic = 1;

bcn3ic SAME ADDRESS as previous (0071h)

    b7, 0
    b6, 0
    b5, 0
    b4, 0
    b3, ir_bcn3ic = 0; (Read Only) Interrupt Request Bit, 0 = Request no interrupt, 1 = requests and interrupt (can be written with 0)
    b2, ilvl2_bcn3ic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_bcn3ic = 0;
    b0, ilvl0_bcn3ic = 1;
-----------------------------------------------------------
int 41, Bus conflict detect, Start condition detect, Stop condition detect, (UART4/UART1), Fault Error

the bit ifsr7 in ifsr (031Fh Interrupt cause select Register) selects between UART4 and UART1

bcn1ic 0091h Bus collision (UART1,UART4) interrupt control register, default value after reset = xxxx x000

    b7, 0
    b6, 0
    b5, 0
    b4, 0
    b3, ir_bcn1ic = 0; (Read Only) Interrupt Request Bit, 0 = Request no interrupt, 1 = requests and interrupt (can be written with 0)
    b2, ilvl2_bcn1ic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_bcn1ic = 0;
    b0, ilvl0_bcn1ic = 1;

bcn4ic SAME ADDRESS as previous (0091h)

    b7, 0
    b6, 0
    b5, 0
    b4, 0
    b3, ir_bcn4ic = 0; (Read Only) Interrupt Request Bit, 0 = Request no interrupt, 1 = requests and interrupt (can be written with 0)
    b2, ilvl2_bcn4ic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_bcn4ic = 0;
    b0, ilvl0_bcn4ic = 1;
-----------------------------------------------------------
int 39, Bus conflict detect, Start condition detect, Stop condition detect, (UART2), Fault Error

managed by

bcn2ic 008Fh Bus collision (UART2) interrupt control register, default value after reset = xxxx x000

    b7, 0
    b6, 0
    b5, 0
    b4, 0
    b3, ir_bcn2ic = 0; (Read Only) Interrupt Request Bit, 0 = Request no interrupt, 1 = requests and interrupt (can be written with 0)
    b2, ilvl2_bcn2ic = 0; Interrupt Priority Level Select Bit, 000 = Level 0, Interrupt Disabled
    b1, ilvl1_bcn2ic = 0;
    b0, ilvl0_bcn2ic = 1;
-----------------------------------------------------------
bcn3ic_addr
bcn0ic_addr 0071h Bus collision (UART0/UART3) interrupt control register, default value after reset = xxxx x000
*/

