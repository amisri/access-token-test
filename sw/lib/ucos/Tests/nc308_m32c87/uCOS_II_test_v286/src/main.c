/*---------------------------------------------------------------------------------------------------------*\
 File:		main.c

 Purpose:	test application for MicroC/OS-II (interrupt version)
                three tasks sending and receiving via UART

 Author:	Hubert Kronenberg (HK)

 History:
    16 sep 03   hk      Created
    25 Jan 05	doc	update for uCOS II v2.86

\*---------------------------------------------------------------------------------------------------------*/

#define	GLOBALS

#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void main (void)
/*---------------------------------------------------------------------------------------------------------*\
    MAIN
\*---------------------------------------------------------------------------------------------------------*/
{
    InitHdwM32C87();	// init the hardware but all maskable ints are disabled

    OSInit(); // uCOS II internal variables initializations

    // create only 1 task
    OSTaskCreate(TaskStartFunc, (void*)0x5aa5, &gTaskStartStk[APP_TASK_START_STK_SIZE - 1], APP_TASK_START_PRIO);

    // start uCOS II
    OSStart();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/

