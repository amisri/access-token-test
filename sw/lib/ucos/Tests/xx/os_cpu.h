/*
***********************************************************************************************
*                                         uC/OS-II
*                                  The Real-Time Kernel
*
*                  (c) Copyright 1992-2002, Jean J. Labrosse, WESTON, FL
*                                    All Rights Reserved
*
*                          Mitsubishi M32C/80 Family Specific code
*
*
* File : OS_CPU.H
* By   : Jean J. Labrosse
*
***********************************************************************************************
*/

/* Typedefs */

#define PCHAR		char far *		// String pointer
#define PINT8SFAR	INT8S far *

/* GNU only accept far as an "attribute" for variables */
/* for GNU-GCC
#define PCHAR		char *		// String pointer
#define PINT8SFAR	INT8S *
*/


// char is char, nor signed nor unsigned

typedef unsigned char		INT8U;		// Unsigned  8-bit quantity [0:255]
typedef signed   char		INT8S;		// Signed    8-bit quantity [-128:127]

// int = signed short int = INT16S  for this compiler

typedef unsigned short		INT16U;		// Unsigned 16-bit quantity [0:65535]           64Kb
typedef signed   short		INT16S;		// Signed   16-bit quantity [-32768:32767]      32Kb

typedef unsigned long		INT32U;		// Unsigned 32-bit quantity [0:4294967295]              4Gb
typedef signed   long		INT32S;		// Signed   32-bit quantity [-2147483648:2147483647]    2Gb

typedef unsigned long long	INT64U;		// Unsigned 64-bit quantity [0:18446744073709551615]
typedef signed   long long	INT64S;		// Signed   64-bit quantity [-9223372036854775808:9223372036854775807]

// typedef INT8U		_Bool;	// NC308 _Bool is 8bits unsigned [0,1]

typedef INT8U			BOOLEAN;	// Flag value (TRUE/FALSE)
// Note: Q.K. uses in the __HC16__ code BOOLEAN as INT16U

typedef float			FP32;		// Single precision floating point
//    float is 4 bytes (32 bits) [1.17549435e-38:3.40282347e+38]
typedef double			FP64;		// Double precision floating point
//    double is 8 bytes (64 bits) [2.2250738585072014e-308:1.7976931348623157e+308]

// this is not valid!!!, long double = double, it is FP64
typedef long double		FP96;	// 12 bytes floating point

typedef volatile unsigned char		REG8U;		// Unsigned  8-bit register
typedef volatile signed   short		REG16S;		// Signed   16-bit register
typedef volatile unsigned short		REG16U;		// Unsigned 16-bit register
typedef volatile unsigned long		REG32U;		// Unsigned 32-bit register
typedef volatile unsigned long long	REG64U;
typedef volatile signed long long	REG64S;


typedef unsigned short OS_STK;                   /* Each stack entry is 16-bit wide          */

/* Macros */

#define  OS_ENABLE_INTS()						/* Enable interrupts  */
#define  OS_ENTER_CRITICAL()  asm("PUSHC FLG"); asm("FCLR I")		/* Disable interrupts */
#define  OS_EXIT_CRITICAL()   asm("POPC FLG")				/* Enable  interrupts */

#define  OS_STK_GROWTH        1                  /* Stack grows from HIGH to LOW memory      */

#define  OS_TASK_SW()         asm("INT #1")      /* mapped to the software interrupt 1       */

/* save all register and do the same as if OSIntEnter() is called  */
#define	OS_START_ISR(lbl) \
asm("PUSHM R0,R1,R2,R3,A0,A1,FB");\
asm("INC.B _OSIntNesting:16");\
asm("CMP.B #1,_OSIntNesting:16");\
asm("JNE _L" ## #lbl );\
asm("MOV.L _OSTCBCur:16,A0");\
asm("STC ISP,[A0]");\
asm("_L" ## #lbl ":")

/* restore all saved register and call OSIntExit */
#define	OS_END_ISR() \
asm("JSR _OSIntExit");\
asm("POPM R0,R1,R2,R3,A0,A1,FB")

/* End of os_cpu.h */
