/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2003-2006; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                       APPLICATION CONFIGURATION
*
*                                             Renesas M32C
*
* Filename      : app_cfg.h
* Version       : V1.00
* Programmer(s) : Brian Nagel
*********************************************************************************************************
*/

#ifndef  __APP_CFG_H__
#define  __APP_CFG_H__

/*
*********************************************************************************************************
*                                       ADDITIONAL uC/MODULE ENABLES
*********************************************************************************************************
*/
#define	OS_VIEW_MODULE			0	// not used

/*
*********************************************************************************************************
*                                            TASK PRIORITIES
* also used as Task Identifiers
*********************************************************************************************************
*/

//                                      0 // highest priority task
#define	APP_TASK_START_PRIO		0
#define	APP_TASK_T1_PRIO		1
#define	APP_TASK_T2_PRIO		2
#define	OS_TASK_TMR_PRIO		3
#define	OS_VIEW_TASK_PRIO		7
#define	OS_VIEW_TASK_ID		7

#define	APP_TASK_T3_PRIO		OS_LOWEST_PRIO - 2

// 	IDLE TASK			15=OS_LOWEST_PRIO	// lowest priority task

// Warning Note !!! don't forget to check against OS_MAX_TASKS

/*
*********************************************************************************************************
*                                            Queues sizes
* size in words
*********************************************************************************************************
*/

// Warning Note !!! don't forget to check against OS_MAX_QS

/*
*********************************************************************************************************
*                                            TASK STACK SIZES
* size in words
*********************************************************************************************************
*/

#define	OS_VIEW_TASK_STK_SIZE		200
#define	OS_TASK_DEF_STK_SIZE		512


#define	APP_TASK_START_STK_SIZE		512


#define	APP_TASK_T1_STK_SIZE		512
#define	APP_TASK_T2_STK_SIZE		512
#define	APP_TASK_T3_STK_SIZE		513


#define TOS(s) ( (s) + sizeof(s) - sizeof(OS_STK) )

//  (void*)&gTaskStartStk[APP_TASK_START_STK_SIZE - 1]

/*
*********************************************************************************************************
*                                          uC/LCD CONFIGURATION
*********************************************************************************************************
*/

#define  DISP_BUS_WIDTH               4        /* LCD controller is accessed with a 4 bit bus          */
#endif
