/*
*********************************************************************************************************
*                                               uC/OS-II
*                                        The Real-Time Kernel
*
*                        (c) Copyright 1992-1998, Jean J. Labrosse, Plantation, FL
*                                          All Rights Reserved
*
*                                          68HC16 Specific code
*                                       Hiware - Medium Memory Model
*
* File         : OS_CPU.H
* By           : Jean J. Labrosse & Quentin King
* Port Version : V1.00
*********************************************************************************************************
*/

/* Typedefs */

typedef unsigned char  BOOLEAN;
typedef unsigned char  INT8U;                    /* Unsigned  8 bit quantity                           */
typedef signed   char  INT8S;                    /* Signed    8 bit quantity                           */
typedef unsigned int   INT16U;                   /* Unsigned 16 bit quantity                           */
typedef signed   int   INT16S;                   /* Signed   16 bit quantity                           */
typedef unsigned long  INT32U;                   /* Unsigned 32 bit quantity                           */
typedef signed   long  INT32S;                   /* Signed   32 bit quantity                           */
typedef float          FP32;                     /* Single precision floating point                    */
typedef double         FP64;                     /* Double precision floating point                    */
typedef unsigned short OS_STK;                   /* Each stack entry is 16-bit wide                    */

/* Macros */

#ifndef  FALSE
#define  FALSE    0
#endif

#ifndef  TRUE
#define  TRUE     1
#endif

#define  OS_ENTER_CRITICAL() 	{asm PSHM CCR; asm ORP #0x00E0;}
#define  OS_EXIT_CRITICAL()   	asm PULM CCR

#define  OS_TASK_SW()         	asm SWI
#define	 OS_ENABLE_INTS()     	asm ANDP #0xFF1F
#define	 OS_DISABLE_INTS()    	asm ORP #0x00E0
#define  OS_INT_ENTER()         {asm INC OSIntNesting; asm PSHM D,E,X,Y,Z,K;}
#define  OS_INT_EXIT()		{asm JSR OSIntExit; asm PULM D,E,X,Y,Z,K;}

#define  OS_STK_GROWTH        	1                  /* Define stack growth: 1 = Down, 0 = Up */

/* End of os_cpu.h */
