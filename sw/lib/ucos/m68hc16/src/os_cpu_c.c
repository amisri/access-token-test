/*
*********************************************************************************************************
*                                               uC/OS-II
*                                         The Real-Time Kernel
*
*                         (c) Copyright 1998, Jean J. Labrosse, Plantation, FL
*                                          All Rights Reserved
*
*
*                                          68HC16 Specific code
*                                           Hiware Environment
*                                          (Medium Memory Model)
*
* File        	: OS_CPU_C.C
* By          	: Jean J. Labrosse & qak & MC
* Port Version	: V1.00
*
* History	: qak  Mar 99	Ported to Hiware environment from Cosmic version.
*                 qak  May 99   Ported to Y_BASED segment environment - it will only work with -My option
*                 qak  May 99   Reverted to non Y-based environment.
*                 qak  Jun 99   Removed all hook functions.
*                 qak  Jun 99   Added indirect vector jump table with Inc OSIntNesting included
*                 qak  Jun 99   Set CCR:IP to 7 when creating task stack in OSTaskStkInit()
*                 MC   oct 00	OSTickISR adapted for running with RTC V3025
*********************************************************************************************************
*/

#define  OS_GLOBALS                           /* Declare GLOBAL variables */

#include <os.h>

/*
*********************************************************************************************************
*                                        INITIALIZE A TASK'S STACK
*
* Description: This function is called by either OSTaskCreate() or OSTaskCreateExt() to initialize the
*              stack frame of the task being created.  This function is highly processor and compiler
*              specific.
*
* Arguments  : task     20 bit pointer to the task code.  The value stacked must be this value
*                       incremented by six because of the 3 instruction pipeline of the HC16.
*                       When RTI or RTS is executed, it will pull the PC off the stack and will
*                       subtract six before loading the PC register.
*
*              pdata    is a pointer to a user supplied data area that will be passed to the task
*                       when the task first executes.  The Hiware compiler will expect to receive
*                       this parameter in the IY register (this depends upon compiler options).
*
*              ptos     is a pointer to the top of stack.  It is assumed that 'ptos' points to the
*                       first 'free' entry on the task stack.  The HC16 stack grows downwards.
*
*              opt      specifies options that can be used to alter the behavior of OSTaskStkInit().
*                       (see uCOS_II.H for OS_TASK_OPT_???).  This isn't used at the moment.
*
* Returns    : Always returns the location of the new top-of-stack once the processor registers have
*              been placed on the stack in the proper order.
*
* Note(s)    : 1) The STOP instruction is enabled when your task starts executing.  You can change this
*                 by setting BIT15 in the CCR.
*              2) All interrupts are enabled when a task starts executing.
*              3) Stack frame (16-bit wide stack):
*                        +  0 --->
*                        +  2        K   = page reg \
*                        +  4        Z   = unchanged |
*                        +  6        Y   = pdata     | CPU registers
*                        +  8        X   = unchanged |
*                        + 10        E   = unchanged |
*                        + 12        D   = unchanged/
*                        + 14        CCR            \  SWI or INT stacking
*                        + 16        task           /
*              4) 6 is added to the task address because the CPU subtracts 6 when it executes the RTI
*                 instruction (due to pipelining).
*              5) NO_EXIT pragma prevents exit code being added by the compiler.  The return value
*                 (void * to top of stack) is returned in IY.
*	       6) All tasks will start with interrupts enabled
*********************************************************************************************************
*/

#pragma NO_EXIT
OS_STK *OSTaskStkInit (void (*task)(void *pd), void *pdata, OS_STK *ptos, INT16U opt)
{
    asm {
        LDY ptos,Z     // IY = ptos (Top of stack)
        AIY #-16       // Move IY to just below new stack

        LDE task,Z     // D = low 16 bits of task address
        LDD 14,Z       // E = high 16 bits of task address (only 4 bits are significant)
        ADDD #6        // Add six to address to compensate for 6 byte prefetch pipeline
        ADCE #0x0000   // Add carry bit to E to allow for change of 64KB page and set
		       // Interrupt priority IP is 0, to allow all interrupts
        STD 16,Y       // Store task address at the of top of the new stack
        STE 14,Y       // Store CCR = PK for task address

        LDD pdata,Z    // Get pdata into D
        STD 6,Y        // Prepare Y = pdata

        PSHM K         // Use stack to transfer K...
        PULM D         // ... into D
        STD 2,Y        // Put K on the new stack

        LDZ 0,Z        // Restore stack frame for calling function
        AIS #4         // Adjust SP to jump over Z and D from entry code PSHM D,Z
        RTS            // Return(IY)
    }
}
/*
;********************************************************************************************************
;                               START HIGHEST PRIORITY TASK READY-TO-RUN
;
; Description : This function is called by OSStart() to start the highest priority task that was created
;               by your application before calling OSStart().
;
; Arguments   : none
;
; Note(s)     : 1) The stack frame is assumed to look as follows:
;
;                  OSTCBHighRdy->OSTCBStkPtr +  0  -->
;                                            +  2       K             \
;                                            +  4       Z              |
;                                            +  6       Y              |
;                                            +  8       X              |
;                                            + 10       E              | CPU registers
;                                            + 12       D              |
;                                            + 14       CCR            |
;                                            + 16       PC            /
;
;               2) OSStartHighRdy() MUST:
;                      a) Call OSTaskSwHook() then,
;                      b) Set OSRunning to TRUE,
;                      c) Switch to the highest priority task.
;********************************************************************************************************
*/

#pragma TRAP_PROC
#pragma NO_FRAME
void OSStartHighRdy(void)
{
    asm {
        INC    OSRunning		// Indicate that we are multitasking
        LDX    OSTCBHighRdy		// Point to TCB of highest priority task ready to run
        LDS    0,X			// Load SP into 68HC16
        PULM   D,E,X,Y,Z,K		// Restore CPU registers
    }
}

/*
*********************************************************************************************************
*                                       TASK LEVEL CONTEXT SWITCH
*
* Description : This function is called when a task makes a higher priority task ready-to-run.
*
* Arguments   : none
*
* Note(s)     : 1) Upon entry,
*                  OSTCBCur     points to the OS_TCB of the task to suspend
*                  OSTCBHighRdy points to the OS_TCB of the task to resume
*
*               2) The stack frame of the task to suspend looks as follows:
*
*                  SP +  0  -->
*                     +  2       CCR
*                     +  4       PC
*
*               3) The stack frame of the task to resume looks as follows:
*
*                  OSTCBHighRdy->OSTCBStkPtr +  0  -->
*                                            +  2       K             \
*                                            +  4       Z              |
*                                            +  6       Y              |
*                                            +  8       X              |
*                                            + 10       E              | CPU registers
*                                            + 12       D              |
*                                            + 14       CCR            |
*                                            + 16       PC            /
*
*		!! The HC16 interrupt vector have only 16 bit address range consequently only the first page
*		can be reach. For this reason the code for all the IRQ must reside in page 0 (medium memory
*		model). To do this a pragma must be used in the .prm file for the memory allocation. This
*		paragma is called : ISR_PLACE.
*
*********************************************************************************************************
*/

#pragma CODE_SEG ISR_PLACE
#pragma TRAP_PROC
#pragma NO_FRAME
void OSCtxSw(void)
{
    asm {
        PSHM   D,E,X,Y,Z,K		// Save CPU registers
        LDX    OSTCBCur
        STS    0,X			// Save stack pointer of preempted task in TCB
	LDY    #0x0000
        LDX    OSTCBHighRdy		// OSTCBCur  = OSTCBHighRdy
        STX    OSTCBCur
        LDS    0,X			// Load stack pointer of new task from TCB
	LDAA   OSPrioHighRdy		// Take priority of task being start
	STAA   OSPrioCur		// Store in current priority variable
	PULM   D,E,X,Y,Z,K		// Restore CPU registers
    }
}
#pragma CODE_SEG DEFAULT

/*
*********************************************************************************************************
;                                    INTERRUPT LEVEL CONTEXT SWITCH
;
; Description : This function is called by OSIntExit() to perform a context switch to a task that has
;               been made ready-to-run by an ISR.
;
; Arguments   : none
;
; Note(s)     : 1) The constant to add (i.e. 'AIS #14') depends on compiler & options.
;
;               2) Stack frame upon entry:
;
;                  SP +  0  -->
;                     +  2       CCR           OSIntCtxSw()
;                     +  4       PC
;                     +  6       CCR           OS_ENTER_CRITICAL() provided OS_CRITICAL_METHOD = 2!!!!!
;                     +  8       Z             Stack frame from OSIntExit()
;                     + 10       locvar        Space for OSIntExit() local variable
;  SP is moved to     + 12       CCR           OSIntExit()
;  this location  ->  + 14       PC
;                     + 16       K             \
;                     + 18       Z              |
;                     + 20       Y              |
;                     + 22       X              |
;                     + 24       E              | CPU registers
;                     + 26       D              |
;   ^                 + 28       CCR            |
;   | Stack growth    + 30       PC            /
*********************************************************************************************************
*/
#pragma TRAP_PROC
#pragma NO_FRAME
void OSIntCtxSw(void)
{
    asm {
        AIS    #14			// Adjust stack pointer
        LDX    OSTCBCur
        STS    0,X			// Save stack pointer of preempted task in TCB
        LDX    OSTCBHighRdy		// OSTCBCur  = OSTCBHighRdy
        STX    OSTCBCur
        LDS    0,X			// Load stack pointer of new task from TCB
	LDAA   OSPrioHighRdy		// Take priority of task being start
	STAA   OSPrioCur		// Store in current priority variable
        PULM   D,E,X,Y,Z,K		// Restore CPU registers
    }
}
/*
*********************************************************************************************************
*                                           SYSTEM TICK ISR
*
* Description : This function is the ISR used to notify uC/OS-II that a system tick has occurred.
*
* Arguments   : none
*
* Notes       :  1) The 68HC16 ensures that the FIRST instruction is executed BEFORE the 68HC16 recognizes
*                   any other interrupts.
*
*		!! The HC16 interrupt vector have only 16 bit address range consequently only the first page
*		can be reach. For this reason the code for all the IRQ must reside in page 0 (medium memory
*		model). To do this a pragma must be used in the .prm file for the memory allocation. This
*		paragma is called : ISR_PLACE.
*********************************************************************************************************
*/
#pragma CODE_SEG ISR_PLACE
#pragma TRAP_PROC
#pragma NO_FRAME
void OSTickISR(void)
{
    OS_INT_ENTER();

    asm {
         LDAA #0x01;          // efface le flag dans le RTC afin de liberer l'irq
	 STAA 0xE000;

	 BCLR 0XE001,#0x10;
	}

    OSTimeTick();

    OS_INT_EXIT();
}

#pragma CODE_SEG DEFAULT

/* End of file: os_cpu_c.c */
