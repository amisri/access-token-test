# -----------------------------------------------------------------------------------------------------------
# File:		lib.mak
#
# Purpose:	uC/OS-II HC16 Tester CPU card (Data page F) library make file
# -----------------------------------------------------------------------------------------------------------

# Define Compiler Options:
#
#   Hiware object format		-Fh
#   IEEE Floating point formats		-Fi
#   Medium Memory Model			-Mm
#   Optimise register usage		-Or
#   Optimise for time			-Ot
#   No information messages		-W1
#   Leave assembler list files		-lasm

CCFLAGS = -Fh -Fi -Mm -Or -Ot -W1 -lasm

# Specify default data page

PAGE = F

# Lists of C source files and object files

SRCS   = os_cpu_c.c os_core.c os_mbox.c os_mem.c os_q.c os_sem.c os_task.c os_time.c
OBJS   = os_cpu_c.o os_core.o os_mbox.o os_mem.o os_q.o os_sem.o os_task.o os_time.o

# Default target: Compile all files and build library

Default:
 -del lst\rtosF.lst lib\rtosF.lib
 $(COMP) $(CCFLAGS) -Cp$(PAGE) $(SRCS)
 cd lib
 $(LIBM) -NoPath -Cmd"$(OBJS) = rtosF.lib"
 del edout $(OBJS)
 ren rtosF.lst ..\lst\rtosF.lst

# -----------------------------------------------------------------------------------------------------------
# End of file: lib.mak
# -----------------------------------------------------------------------------------------------------------

