/*
***********************************************************************************************
*                                         uC/OS-II
*                                  The Real-Time Kernel
*
*                  (c) Copyright 1992-2002, Jean J. Labrosse, WESTON, FL
*                                    All Rights Reserved
*
*                          Mitsubishi M16C/80 Family Specific code
*
*
* File : OS_CPU.H
* By   : Jean J. Labrosse
***********************************************************************************************
*/

/* Typedefs */

typedef unsigned char  BOOLEAN;
typedef unsigned char  INT8U;                    /* Unsigned  8 bit quantity                 */
typedef char           INT8S;                    /* Signed    8 bit quantity                 */
typedef unsigned short INT16U;                   /* Unsigned 16 bit quantity                 */
typedef short          INT16S;                   /* Signed   16 bit quantity                 */
typedef unsigned long  INT32U;                   /* Unsigned 32 bit quantity                 */
typedef long           INT32S;                   /* Signed   32 bit quantity                 */
typedef float          FP32;                     /* Single precision floating point          */
typedef double         FP64;                     /* Double precision floating point          */

typedef unsigned short OS_STK;                   /* Each stack entry is 16-bit wide          */

/* Macros */

#define  OS_ENABLE_INTS()						/* Enable interrupts  */
#define  OS_ENTER_CRITICAL()  asm("PUSHC FLG"); asm("FCLR I")		/* Disable interrupts */
#define  OS_EXIT_CRITICAL()   asm("POPC FLG")				/* Enable  interrupts */

#define  OS_STK_GROWTH        1                  /* Stack grows from HIGH to LOW memory      */

#define  OS_TASK_SW()         asm("INT #1")      /* mapped to the software interrupt 0       */

#define	OS_START_ISR(lbl) \
asm("PUSHM R0,R1,R2,R3,A0,A1,FB");\
asm("INC.B _OSIntNesting:16");\
asm("CMP.B #1,_OSIntNesting:16");\
asm("JNE _L" ## #lbl );\
asm("MOV.L _OSTCBCur:16,A0");\
asm("STC ISP,[A0]");\
asm("_L" ## #lbl ":")

#define	OS_END_ISR() \
asm("JSR _OSIntExit");\
asm("POPM R0,R1,R2,R3,A0,A1,FB")

/* End of os_cpu.h */
