REM --- Compile and build uC-OS library ---

REM Note: mbox and mem functions are not included at the moment.

del lst\*     < yes.txt
del lib\*     < yes.txt

sh make.cmd

move src\*.r30 lib
move src\*.lst lst
move ..\common\src\*.r30 lib
move ..\common\src\*.lst lst

cd lib

lb308 @..\lb308.cmd > ..\lbout

lb308 -L ucos.lib

move ucos.lls ..\lst

REM --- EOF ---
