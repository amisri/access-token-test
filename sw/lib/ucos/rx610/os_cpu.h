/*
*********************************************************************************************************
*                                               uC/OS-II
*                                        The Real-Time Kernel
*
*                          (c) Copyright 1992-2010, Micrium, Inc., Weston, FL
*                                          All Rights Reserved
*
*                                      Renesas RX Specific code
*
* File         : os_cpu.h
* By           : HMS
* Compiler     : Renesas HEW IDE with the RX compiler
*********************************************************************************************************
*/

#ifndef _OS_CPU_H
#define _OS_CPU_H

#ifdef  OS_CPU_GLOBALS
#define OS_CPU_EXT
#else
#define OS_CPU_EXT  extern
#endif

#include  <machine.h>

/*
*********************************************************************************************************
*                                              DATA TYPES
*                                         (Compiler Specific)
*********************************************************************************************************
*/

typedef  unsigned  char         BOOLEAN;
typedef  unsigned  char         INT8U;                                  /* Unsigned  8 bit quantity                         */
typedef  signed    char         INT8S;                                  /* Signed    8 bit quantity                         */
typedef  unsigned  short        INT16U;                                 /* Unsigned 16 bit quantity                         */
typedef  signed    short        INT16S;                                 /* Signed   16 bit quantity                         */
typedef  unsigned  long         INT32U;                                 /* Unsigned 32 bit quantity                         */
typedef  signed    long         INT32S;                                 /* Signed   32 bit quantity                         */
typedef  unsigned  long   long  INT64U;                                 /* Unsigned 64 bit quantity                         */
typedef  signed    long   long  INT64S;                                 /* Signed   64 bit quantity                         */
typedef  float                  FP32;                                   /* Single precision floating point                  */
typedef  double                 FP64;                                   /* Double precision floating point                  */

typedef  unsigned  long         OS_STK;                                 /* Each stack entry is 32-bit wide                  */
typedef  unsigned  char         OS_CPU_SR;                              /* The status register (SR) is 8-bits wide          */

/*
*********************************************************************************************************
*                                               Renesas RX
*
*
* Method #1:  Disable/Enable interrupts using simple instructions.  After critical section, interrupts
*             will be enabled even if they were disabled before entering the critical section.
*
* Method #2:  Disable/Enable interrupts by preserving the state of interrupts.  In other words, if 
*             interrupts were disabled before entering the critical section, they will be disabled when
*             leaving the critical section.
*
* Method #3:  Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking you
*             would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
*             disable interrupts.  'cpu_sr' is allocated in all of uC/OS-II's functions that need to 
*             disable interrupts.  You would restore the interrupt disable state by copying back 'cpu_sr'
*             into the CPU's status register.
*
*********************************************************************************************************
*/

#define  OS_CRITICAL_METHOD    3

#if      OS_CRITICAL_METHOD == 1
                                                                        /* Disable interrupts                               */
#define  OS_ENTER_CRITICAL()    set_ipl(12)
                                                                        /* Enable interrupts                                */
#define  OS_EXIT_CRITICAL()     set_ipl(0)
#endif

#if      OS_CRITICAL_METHOD == 2
                                                                        /* Save and disable interrupts                      */
#define  OS_ENTER_CRITICAL()    asm("PUSHC PSW"); set_ipl(12)
                                                                        /* Restore interrupt status                         */
#define  OS_EXIT_CRITICAL()     asm("POPC PSW")
#endif

#if      OS_CRITICAL_METHOD == 3
                                                                        /* Save and disable interrupts                      */
#define  OS_ENTER_CRITICAL()    cpu_sr = get_ipl(); set_ipl(12)

                                                                        /* Restore interrupt status                         */
#define  OS_EXIT_CRITICAL()     set_ipl(cpu_sr)
#endif

/*
*********************************************************************************************************
*                                           Renesas SH-2A-FPU Miscellaneous
*********************************************************************************************************
*/

#define  OS_STK_GROWTH      1                                           /* Stack grows from HIGH to LOW on the Renesas RX   */

#define  OS_TASK_SW()       int_exception(1)                            /* SW interrupt instruction                         */

/*
*********************************************************************************************************
*                                            GLOBAL VARIABLES
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                            PROTOTYPES
*********************************************************************************************************
*/

void  OSCtxSw       (void);
void  OSIntCtxSw    (void);

void  OSStartHighRdy(void);


#endif
