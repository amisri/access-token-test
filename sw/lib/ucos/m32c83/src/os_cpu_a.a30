;**********************************************************************************************
;                                               uC/OS-II
;                                         The Real-Time Kernel
;
;                         (c) Copyright 1992-2002, Jean J. Labrosse, Weston, FL
;                                          All Rights Reserved
;
;                                Mitsubishi M16C/80 Family Specific code
;                                            Assembler: AS308
;
; File : OS_CPU_A.ASM
; By   : Jean J. Labrosse
;	 Quentin King		(Port to Mitsubishi tools)
;
;**********************************************************************************************

    .GLB	__SB__
    .SB		__SB__
    .FB		0

    .GLB	_OSTCBCur              ; declared as OS_TCB *,24-bit long
    .GLB	_OSTCBHighRdy          ; declared as OS_TCB *,24-bit long
    .GLB	_OSPrioCur             ; declared as INT8U,8-bit long
    .GLB	_OSPrioHighRdy         ; declared as INT8U,8-bit long
    .GLB	_OSIntNesting	       ; declared as INT8U,8-bit long
    .GLB	_OSRunning             ; declared as Boolean(unsigned char),8-bit long
    .GLB	_OSIntExit             ; external functions written in C
    .GLB	_OSTimeTick

    .SECTION	program,CODE,ALIGN

;**********************************************************************************************
;*                                   Start High Ready Task
;*                                 void OSStartHighRdy(void)
;*
;* Description: This function is called from OSStart()
;*
;* Arguments  : none
;*
;* Note(s)    :
;**********************************************************************************************

    .align
    .glb	_OSStartHighRdy

_OSStartHighRdy:

    FCLR        U

    MOV.L       _OSTCBHighRdy:16, A0           ; ISP = OSTCBHighRdy->OSTCBStkPtr
    LDC         [A0], ISP

    MOV.B       #01H, _OSRunning:16            ; OSRunning = TRUE

    POPM        R0,R1,R2,R3,A0,A1,FB

    REIT

;**********************************************************************************************
;*                                       Context Switch
;*                                     void OSCtxSW(void)
;*
;* Description: This function is called from OSSched()
;*
;* Arguments  : none
;*
;* Note(s)    : 1) You must map the address of OSCtxSw() to interrupt #1 in the vector table
;**********************************************************************************************

    .align
    .glb	_OSCtxSw

_OSCtxSw:
    PUSHM       R0,R1,R2,R3,A0,A1,FB

    MOV.L       _OSTCBCur:16, A0                ; OSTCBCur->OSTCBStkPtr = SP
    STC         ISP, [A0]

    MOV.L       _OSTCBHighRdy:16,_OSTCBCur:16   ; OSTCBCur  = OSTCBHighRdy

    MOV.B       _OSPrioHighRdy:16,_OSPrioCur:16 ; OSPrioCur = OSPrioHighRdy

    MOV.L       _OSTCBHighRdy:16, A0            ; SP        = OSTCBHighRdy->OSTCBStkPtr
    LDC         [A0], ISP

    POPM        R0,R1,R2,R3,A0,A1,FB         ; Restore all processor registers from the new task's stack

    REIT

;**********************************************************************************************
;*                                  Context Switch from ISR
;*                   void OSIntCtxSw(void)
;* Description: This function is called from OSIntExit()
;*
;* Arguments  : none
;**********************************************************************************************

    .align
    .glb	_OSIntCtxSw

_OSIntCtxSw:
    MOV.L       _OSTCBHighRdy:16, _OSTCBCur:16  ; OSTCBCur  = OSTCBHighRdy

    MOV.B       _OSPrioHighRdy:16,_OSPrioCur:16 ; OSPrioCur = OSPrioHighRdy

    MOV.L       _OSTCBHighRdy:16, A0            ; SP        = OSTCBHighRdy->OSTCBStkPtr
    LDC         [A0], ISP

    POPM        R0,R1,R2,R3,A0,A1,FB         ; Restore all processor registers from the new task's stack

    REIT

;**********************************************************************************************
;*                                    Time ticker ISR
;*                                  void OSTickISR(void)
;*
;* Description: This function is invoked by a hardware timer
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*
;*                 Save all registers
;*                 OSIntNesting++
;*                 if (OSIntNesting == 1) {
;*                     OSTCBCur->OSTCBStkPtr = SP
;*                 }
;*                 OSTimeTick();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************

    .align
    .glb	_OSTickISR

_OSTickISR:

    PUSHM       R0,R1,R2,R3,A0,A1,FB            ; Save current task's registers

    INC.B       _OSIntNesting:16                ; OSIntNesting++
    CMP.B       #1,_OSIntNesting:16             ; if (OSIntNesting == 1) {
    JNE         _OSTickISR1

    MOV.L       _OSTCBCur:16, A0                ;     OSTCBCur->OSTCBStkPtr = SP
    STC         ISP, [A0]                       ; }

_OSTickISR1:
    JSR         _OSTimeTick                      ; OSTimeTick()

    JSR         _OSIntExit                       ; OSIntExit()

    POPM        R0,R1,R2,R3,A0,A1,FB            ; Restore registers from the new task's stack

    REIT

    .END

; * End of file *
