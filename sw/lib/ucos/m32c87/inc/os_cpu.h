/*
*********************************************************************************************************
*                                               uC/OS-II
*                                         The Real-Time Kernel
*
*
*                             (c) Copyright 1992-2007, Micrium, Weston, FL
*                                          All Rights Reserved
*
*                                           Renesas M32C Port
*
* File      : os_cpu.h
* Version   : V1.00
* By        : Jean J. Labrosse
* Ported          : Farshad Nourozi
* Ported to V2.52 : Hubert Kronenberg
* Ported to V2.86 : Daniel O. Calcoen - CERN
*
* For       : Renesas M32C
* Toolchain : Renesas HEW IDE with the NC308WA compiler
*********************************************************************************************************
*/

#ifdef  OS_CPU_GLOBALS
#define OS_CPU_EXT
#else
#define OS_CPU_EXT  extern
#endif

/*
*********************************************************************************************************
*                                             DATA TYPES
*                                         (Compiler Specific)
*********************************************************************************************************
*/

// use z:\projects\inc\renesas\m32c83\cc_types.h
#include <cc_types.h>
/* Typedefs */

/*
#define PCHAR		char far *		// String pointer
#define PINT8SFAR	INT8S far *

// GNU only accept far as an "attribute" for variables
// for GNU-GCC
#define PCHAR		char *		// String pointer
#define PINT8SFAR	INT8S *
//


// char is char, nor signed nor unsigned

typedef unsigned char		INT8U;		// Unsigned  8-bit quantity [0:255]
typedef signed   char		INT8S;		// Signed    8-bit quantity [-128:127]

// int = signed short int = INT16S  for this compiler

typedef unsigned short		INT16U;		// Unsigned 16-bit quantity [0:65535]           64Kb
typedef signed   short		INT16S;		// Signed   16-bit quantity [-32768:32767]      32Kb

typedef unsigned long		INT32U;		// Unsigned 32-bit quantity [0:4294967295]              4Gb
typedef signed   long		INT32S;		// Signed   32-bit quantity [-2147483648:2147483647]    2Gb

typedef unsigned long long	INT64U;		// Unsigned 64-bit quantity [0:18446744073709551615]
typedef signed   long long	INT64S;		// Signed   64-bit quantity [-9223372036854775808:9223372036854775807]

// typedef INT8U		_Bool;	// NC308 _Bool is 8bits unsigned [0,1]

typedef INT8U			BOOLEAN;	// Flag value (TRUE/FALSE)
// Note: Q.K. uses in the __HC16__ code BOOLEAN as INT16U

typedef float			FP32;		// Single precision floating point
//    float is 4 bytes (32 bits) [1.17549435e-38:3.40282347e+38]
typedef double			FP64;		// Double precision floating point
//    double is 8 bytes (64 bits) [2.2250738585072014e-308:1.7976931348623157e+308]

// this is not valid!!!, long double = double, it is FP64
typedef long double		FP96;	// 12 bytes floating point

typedef volatile unsigned char		REG8U;		// Unsigned  8-bit register
typedef volatile signed   short		REG16S;		// Signed   16-bit register
typedef volatile unsigned short		REG16U;		// Unsigned 16-bit register
typedef volatile unsigned long		REG32U;		// Unsigned 32-bit register
typedef volatile unsigned long long	REG64U;
typedef volatile signed long long	REG64S;
*/




// previous version was
// typedef unsigned char  OS_STK;                 /* Each stack entry is 8 bit wide      */

typedef INT16U		OS_STK;                   /* Each stack entry is 16-bit wide     */
typedef INT16U		OS_CPU_SR;                /* Type of CPU status register         */


/*

when a value is PUSHed into the stack the stack pointer is decremented

so as you PUSH more values the stack "grows" toward 0000h

when and interrupt occurs the M32C87 first PUSHes the flags register (FLG) then return address

so the stack will look like this


high mem    FFFFFFFh

            FLG         <--- old SP, previous to interrupt call
	    RET ADD(H)
	    RET ADD(L)
                        <--- SP (actual pointer at interrupt entry)
low  mem    0000000h


to do the context switching uCOS will mimic an interrupt call plus the save of all the register
like an "INT 1" call followed by "PUSHM R0,R1,R2,R3,A0,A1,SB,FB"


so the stack will look like this

high mem    	FFFFFFFh

            	/ FLG       <--- old SP, previous to interrupt call
Interrupt frame	| RET ADD(H)
	    	\ RET ADD(L)
		/ FB(H)     <--- old SP at interrupt entry
		| FB(L)
		| SB(H)
		| SB(L)
CPU registers	| A1(H)
		| A1(L)
		| A0(H)
		| A0(L)
		| R3
		| R2
		| R1
		\ R0
			    <--- SP (actual pointer at interrupt entry)
low  mem        0000000h


adding the extra info needed by uCOS the stack frame structure is


high mem    	FFFFFFFh

		pdata(H)       <-- C variable + size of stack frame
		pdata(L)
		task_func(H)
		task_func(L)
	       	FLG
		RET ADD(H)
	    	RET ADD(L)
		FB(H)
		FB(L)
		SB(H)
		SB(L)
		A1(H)
		A1(L)
		A0(H)
		A0(L)
		R3
		R2
		R1
		R0             <-- C variable used for store stack frame points here
		                   uCOS control block needs to point here to mimic the return from interrupt

low  mem        0000000h

*/

/*
*********************************************************************************************************

   with "#pragma INTERRUPT [/B] IsrName" signal to the compiler that the functions is an interrupt so
   put and REIT instead of a RET at its end

   the compiler automatically generates the push of the "used" register in the ISR function so are not
   always all the registers
   the solution I found for this is usign /B parameter

   /B directs the compiler to switch to bank 1 registers
   so it adds at the beginig the FSET B instruction instead of the automatic PUSHM generated to save
   the used registers by the compiler
   I force back to bank 0 and then I can add my PUSHM of ALL_REGISTERS


   The pseudo code for an ISR must be
       a) Save all registers
       b) OSIntEnter()
       c) If a task was interrupted (OSIntNesting == 1), then save stack pointer into the task's TCB


       d) you ISR code


       e) OSIntExit();
       f) Restore all registers
       g) Return from interrupt



   macro OS_START_ISR() consist of a)b)c)
   macro OS_END_ISR() consist of e)f)g)

   so your pseudo code will be

           OS_START_ISR(any_label);

           you ISR code

           OS_END_ISR();


*********************************************************************************************************
*/

// save all registers and executes OSIntEnter()

// Becarefull - search globally this label "ALL_REGISTERS" to check that you push and pop always
// the same registers and in the same order

// ALL_REGISTERS R0,R1,R2,R3,A0,A1,SB,FB

#define	OS_START_ISR(lbl) \
asm("FCLR B");\
asm("PUSHM R0,R1,R2,R3,A0,A1,SB,FB");\
asm("JSR _OSIntEnter");\
asm("CMP.B #1,_OSIntNesting");\
asm("JNE _L" ## #lbl );\
asm("MOV.L _OSTCBCur,A0");\
asm("STC ISP,[A0]");\
asm("_L" ## #lbl ":");


// call OSIntExit() and restore previously saved registers

#define	OS_END_ISR() \
asm("JSR _OSIntExit");\
asm("POPM R0,R1,R2,R3,A0,A1,SB,FB");

// ALL_REGISTERS R0,R1,R2,R3,A0,A1,SB,FB

/*
*********************************************************************************************************
*                                        RENESAS M32C FAMILY
*
* Method #1:  Disable/Enable interrupts using simple instructions.  After critical section, interrupts
*             will be enabled even if they were disabled before entering the critical section.
*
* Method #2:  Disable/Enable interrupts by preserving the state of interrupts.  In other words, if
*             interrupts were disabled before entering the critical section, they will be disabled when
*             leaving the critical section.
*
*             the state is preserved in the active stack (USP or ISP), if the stack is not respected
*             between OS_ENTER_CRITICAL() and OS_EXIT_CRITICAL() dont't use this method!
*
* Method #3:  Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking you
*             would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
*             disable interrupts.  'cpu_sr' is allocated in all of uC/OS-II's functions that need to
*             disable interrupts.  You would restore the interrupt disable state by copying back 'cpu_sr'
*             into the CPU's status register.
*
*             the state is preserved in a local var (the stack is not used in this method)
*
*
* clear I flag of FLG register to mask interrupts is only valid for peripheral interrupts
* int 8 to int 54 and int 57
*
* the software INT instruction is Non Maskable, if you use the instruction it will call the routine
* independant of the I bit state or the IPL level.
*
* also the following interrupts are Non Maskable
*
* UND or UDI         (Undefined instruction)
* INTO or OVER_FLOW  (Overflow, is active after set O in FLG register)
* BRK or BRKI        (Break, can redirect to int 0, BRK2)
* ADDRESS_MATCH      (Address Match)
* SINGLE_STEP        (Single Step)
* WDT                (Watchdog timer, shared with Oscillation Stop Detect and Vdet4)
* DBC                (debugging)
* NMI                (Non maskable)
* RESET              (Reset)
* DMACII request interrupt (generated by any peripheral (hw) with ILVL=7, after seting b5 of RLVL)
* DMACII transfer complete
*
*
* hardware (peripheral) interrupts always clears U flag (selecting ISP) on entry
* so always saving FLG register + PC register at ISP stack
*
* for INT instruction applied from int 32 to int 63 the U flag is unchanged so the stack is not forced to ISP
* for INT instruction applied from int 0 to int 31 the U flag is cleared forcing use of ISP
*
*********************************************************************************************************
*/

#define  OS_CRITICAL_METHOD    2

#if      OS_CRITICAL_METHOD == 1
#define  OS_ENTER_CRITICAL()  asm("FCLR I")                     /* Disable interrupts                  */
#define  OS_EXIT_CRITICAL()   asm("FSET I")                     /* Enable  interrupts                  */
#endif

#if      OS_CRITICAL_METHOD == 2
#define  OS_ENTER_CRITICAL()  asm("PUSHC FLG"); asm("FCLR I")   /* Disable interrupts                  */
#define  OS_EXIT_CRITICAL()   asm("POPC FLG")                   /* Enable  interrupts                  */
#endif

#if      OS_CRITICAL_METHOD == 3
#define  OS_ENTER_CRITICAL()  asm("STC FLG, $@", cpu_sr);asm("FCLR I")   /* Disable interrupts         */
#define  OS_EXIT_CRITICAL()   asm("LDC $@, FLG", cpu_sr)                 /* Enable  interrupts         */
#endif

/*
*********************************************************************************************************
*                                  RENESAS M32C FAMILY MISCELLANEOUS
*********************************************************************************************************
*/

#define  OS_STK_GROWTH        1                                 /* Stack grows from HIGH to LOW memory */

/* leave the int0 for the BRK interrupt to run nicely the debugger */
/* search for "OS_TASK_SW_vector" through the project files to know where to replace if you change this value */
#define  OS_TASK_SW()         asm("INT #1")                     /* Mapped to the software interrupt 1  */

/*
*********************************************************************************************************
*                                              PROTOTYPES
*********************************************************************************************************
*/

void     OSCtxSw        (void);
void     OSIntCtxSw     (void);
void     OSStartHighRdy (void);
void     OSTickISR      (void);
