#pragma once  
  
//! @file   powerspy.h
//! @brief  Provides interface to output data for PowerSpy serial port.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <string>


namespace fgc
{
    namespace hal
    {
        // ---------- Classes declarations --------------------------
        class Powerspy_output
        {
            public:
                virtual void send(std::string data) = 0;
        };

        using Powerspy_output_ptr = std::shared_ptr<Powerspy_output>;
    }
}


// EOF