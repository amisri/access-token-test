#pragma once  
  
//! @file   gpio.h
//! @brief  Provides interface to debug GPIOs.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>

#include <v32/inc/ftm_timer.h>


namespace fgc
{
    namespace hal
    {
        // ---------- Misc declarations -----------------------------
        using TimerConfig = hal_v32::ftm::TimerConfig;

        // ---------- Classes declarations --------------------------
        template<uint8_t module_index>
        using Timer = hal_v32::ftm::Timer<module_index>;
    }
}

// EOF