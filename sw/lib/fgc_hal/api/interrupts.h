#pragma once  
  
//! @file   interrupts.h
//! @brief  Provides interface to register ISR.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>

#include <cpp_utils/callbacks.h>


namespace fgc
{
    namespace hal
    {
        // ---------- Classes declarations --------------------------
        class Isr_controller
        {
            public:
                virtual void registerRegulationIsr(utils::callback::Simple callback) = 0;
        };

        using Isr_controller_ptr = std::shared_ptr<Isr_controller>;
    }
}


// EOF