#pragma once  
  
//! @file   fpga_programming.h
//! @brief  Provides interface to reprogram the FPGA.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <vector>

namespace hal_v32::mem
{
    class Map;
}

namespace fgc
{
    namespace hal
    {
        // ---------- Classes declarations --------------------------
        class Fpga
        {
            public:
                virtual bool programAndVerify(const std::vector<uint8_t>& bitstream) = 0;
                virtual bool checkWatchdog() = 0;

                //! Gets the pointer to the IFC region
                virtual hal_v32::mem::Map &getIfcRegion() = 0;
        };

        using Fpga_ptr = std::shared_ptr<Fpga>;
    }
}


// EOF