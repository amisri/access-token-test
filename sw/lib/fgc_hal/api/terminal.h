#pragma once  
  
//! @file   terminal.h
//! @brief  Provides interface to communicate with serial terminal.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <string>


namespace fgc
{
    namespace hal
    {
        // ---------- Classes declarations --------------------------
        class Serial_terminal
        {

        };

        using Serial_terminal_ptr = std::shared_ptr<Serial_terminal>;
    }
}


// EOF