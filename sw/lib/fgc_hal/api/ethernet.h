#pragma once  
  
//! @file   ethernet.h
//! @brief  Provides interface to control hardware low-level (MDIO/PHY) Ethernet interfaces.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <string>


namespace fgc
{
    namespace hal
    {
        // ---------- Misc declarations -----------------------------
        enum class Eth_led : uint8_t
        {
            rx_error                 = 0b1110,
            rx_tx_error              = 0b1101,
            link_and_blink_for_tx_rx = 0b1011,
            full_duplex              = 0b1010,
            link_100_1000            = 0b1001,
            link_10_100              = 0b1000,
            link_10                  = 0b0111,
            link_100                 = 0b0110,
            link_1000                = 0b0101,
            collision_detected       = 0b0100,
            rx_activity              = 0b0011,
            tx_activity              = 0b0010,
            rx_tx_activity           = 0b0001,
            link_established         = 0b0000,
        };

        // ---------- Classes declarations --------------------------

        class Eth_port
        {
            public:
                virtual void setAsFgcEther() = 0;
                virtual bool linkStatus() = 0;
                virtual void setGreenLed(Eth_led trigger) = 0;
                virtual void setYellowLed(Eth_led trigger) = 0;
        };

        using Eth_port_ptr = std::shared_ptr<Eth_port>;

        //************************************************************

        class Eth_controller
        {
            public:
                virtual Eth_port_ptr getEth1() = 0;
                virtual Eth_port_ptr getEth2() = 0;
        };

        using Eth_controller_ptr = std::shared_ptr<Eth_controller>;
    }
}


// EOF