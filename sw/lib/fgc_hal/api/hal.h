#pragma once  
  
//! @file   hal.h
//! @brief  Include this file in a project that uses fgc HAL library to include all API interfaces.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <api/dongle.h>
#include <api/ethernet.h>
#include <api/fpga.h>
#include <api/gpio.h>
#include <api/interrupts.h>
#include <api/leds.h>
#include <api/power.h>
#include <api/powerspy.h>
#include <api/terminal.h>


namespace fgc
{
    namespace hal
    {
        //! Available versions of HAL library
        enum class Version
        {
            v32,
            v32_mock
        };

        //! This is a base class providing an access to HAL modules and controllers
        //! It is meant to be inherited by the particular versions of the library
        //! and the correct pointer is selected in the create() function.
        class Hal
        {
            public:
                //! Creates and returns the pointer to the Hal object for a given version.
                //! This function can be called only once - further calls will throw an exception.
                static std::shared_ptr<Hal> create(const Version& version);

                virtual ~Hal() = default;

                virtual Version              getVersion() const = 0;

                virtual Dongle_ptr           getDongle() const;
                virtual Eth_controller_ptr   getEthController() const;
                virtual Fpga_ptr             getFpgaController() const;
                virtual Gpio_controller_ptr  getGpioController() const;
                virtual Isr_controller_ptr   getIsrController() const;
                virtual Led_controller_ptr   getLedController() const;
                virtual Power_controller_ptr getPowerController() const;
                virtual Powerspy_output_ptr  getPowerspyOutput() const;
                virtual Serial_terminal_ptr  getSerialTerminal() const;

            protected:
                template<class T>
                T unsupportedModule() const
                {
                    // TODO throw
                    throw 0;
                }
        };

        using Hal_ptr = std::shared_ptr<Hal>;
    }
}


// EOF