#pragma once  
  
//! @file   gpio.h
//! @brief  Provides interface to debug GPIOs.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>


namespace fgc
{
    namespace hal
    {
        // ---------- Misc declarations -----------------------------
        enum class State
        {
            low,            //!< Pin has voltage equal to GND
            high,           //!< Pin has voltage equal to VCC
            active,         //!< Active-low pin has low value, active-high pin has high value
            inactive,       //!< Active-low pin has high value, active-high pin has low value
        };

        // ---------- Classes declarations --------------------------
        class Gpio_pin
        {
            public:
                virtual State get() = 0;
                virtual void set(const State& state) = 0;
                virtual bool isActive() = 0;
                virtual bool isInactive() = 0;
        };

        using Gpio_pin_ptr = std::shared_ptr<Gpio_pin>;

        class Gpio_controller
        {
            public:
                virtual Gpio_pin_ptr channel1() = 0;
                virtual Gpio_pin_ptr channel2() = 0;
        };

        using Gpio_controller_ptr = std::shared_ptr<Gpio_controller>;
    }
}

// EOF