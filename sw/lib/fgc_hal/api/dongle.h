#pragma once

//! @file   dongle.h
//! @brief  Interface to read device ID from the dongle.
//! @author Dariusz Zielinski

// ---------- Includes
#include <memory>


namespace fgc
{
    namespace hal
    {
        // ---------- Classes declarations --------------------------
        class Dongle
        {
            public:
                virtual uint8_t getId() = 0;
        };

        using Dongle_ptr = std::shared_ptr<Dongle>;
    }
}


// EOF