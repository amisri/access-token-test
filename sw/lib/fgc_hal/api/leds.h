#pragma once  
  
//! @file   leds.h
//! @brief  Provides interface to control LEDs present on the FGC front panel.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>


namespace fgc
{
    namespace hal
    {
        // ---------- Misc declarations -----------------------------
        enum class Led_mode
        {
            off,
            red,
            red_blink,
            orange,
            orange_blink,
            green,
            green_blink,
            blue,
            blue_blink,
        };

        // ---------- Classes declarations --------------------------
        class Led_controller
        {
            public:
                virtual void blink() = 0;

                virtual void net(const Led_mode& led_mode) = 0;
                virtual void fgc(const Led_mode& led_mode) = 0;
                virtual void psu(const Led_mode& led_mode) = 0;
                virtual void vs(const Led_mode& led_mode) = 0;
                virtual void dcct(const Led_mode& led_mode) = 0;
                virtual void pic(const Led_mode& led_mode) = 0;
                virtual void boot(const Led_mode& led_mode) = 0;
        };

        using Led_controller_ptr = std::shared_ptr<Led_controller>;
    }
}


// EOF