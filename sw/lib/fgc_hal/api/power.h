#pragma once  
  
//! @file   power.h
//! @brief  Interface to control powering of the FGC board.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>


namespace fgc
{
    namespace hal
    {
        // ---------- Classes declarations --------------------------
        class Power_controller
        {
            public:
                virtual void powerCycle() = 0;
        };

        using Power_controller_ptr = std::shared_ptr<Power_controller>;
    }
}


// EOF