//! @file   hal.cpp
//! @brief  Default implementations of the main base class Hal.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <api/hal.h>
#include <fgc32/inc/hal.32.h>


using namespace fgc::hal;

std::shared_ptr<Hal> Hal::create(const Version& version)
{
    static bool initialized = false;

    if (initialized)
    {
        // TODO throw
    }

    Hal_ptr hal_ptr;

    switch (version)
    {
        case Version::v32:
            hal_ptr = std::make_shared<hal_v32::Hal>();
            break;

        case Version::v32_mock:
            hal_ptr = std::make_shared<hal_v32::Hal>();
            break;

        default:
            // TODO throw
            throw 0;
    }

    return hal_ptr;
}

//************************************************************

Dongle_ptr Hal::getDongle() const
{
    return unsupportedModule<Dongle_ptr>();
}

//************************************************************

Eth_controller_ptr Hal::getEthController() const
{
    return unsupportedModule<Eth_controller_ptr>();
}

//************************************************************

Fpga_ptr Hal::getFpgaController() const
{
    return unsupportedModule<Fpga_ptr>();
}

//************************************************************

Gpio_controller_ptr Hal::getGpioController() const
{
    return unsupportedModule<Gpio_controller_ptr>();
}

//************************************************************

Isr_controller_ptr Hal::getIsrController() const
{
    return unsupportedModule<Isr_controller_ptr>();
}

//************************************************************

Led_controller_ptr Hal::getLedController() const
{
    return unsupportedModule<Led_controller_ptr>();
}

//************************************************************

Power_controller_ptr Hal::getPowerController() const
{
    return unsupportedModule<Power_controller_ptr>();
}

//************************************************************

Powerspy_output_ptr Hal::getPowerspyOutput() const
{
    return unsupportedModule<Powerspy_output_ptr>();
}

//************************************************************

Serial_terminal_ptr Hal::getSerialTerminal() const
{
    return unsupportedModule<Serial_terminal_ptr>();
}

//************************************************************





// EOF