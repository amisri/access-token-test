//! @file   leds.cpp
//! @brief  Implementation of LED handling.
//! @author Dariusz Zielinski

// ---------- Includes
#include <iostream>
#include <thread>
#include <chrono>

#include <api/hal.h>
#include <fgc32/inc/hw_map.32.h>
#include <fgc32/inc/leds.32.h>


using namespace hal_v32::leds;

// **********************************************************
// * Led_hw
// **********************************************************
void Led_hw::setMode(const Mode &state)
{
    // Save the state
    m_state = state;

    // If the LED is set to be OFF, then disable it, otherwise (On, Blink) enable it.
    setState(m_state != Mode::off);
}

//************************************************************

void Led_hw::blink(const bool &blink_state)
{
    if (m_state == Mode::blink)
    {
        setState(blink_state);
    }
}

// **********************************************************
// * Led_rgb
// **********************************************************
void Led_rgb::setMode(const fgc::hal::Led_mode &led_mode)
{
    switch (led_mode)
    {
        case fgc::hal::Led_mode::red          : m_r->setMode(Mode::on);    m_g->setMode(Mode::off);   m_b->setMode(Mode::off);   break;
        case fgc::hal::Led_mode::red_blink    : m_r->setMode(Mode::blink); m_g->setMode(Mode::off);   m_b->setMode(Mode::off);   break;
        case fgc::hal::Led_mode::orange       : m_r->setMode(Mode::on);    m_g->setMode(Mode::on);    m_b->setMode(Mode::off);   break;
        case fgc::hal::Led_mode::orange_blink : m_r->setMode(Mode::blink); m_g->setMode(Mode::blink); m_b->setMode(Mode::off);   break;
        case fgc::hal::Led_mode::green        : m_r->setMode(Mode::off);   m_g->setMode(Mode::on);    m_b->setMode(Mode::off);   break;
        case fgc::hal::Led_mode::green_blink  : m_r->setMode(Mode::off);   m_g->setMode(Mode::blink); m_b->setMode(Mode::off);   break;
        case fgc::hal::Led_mode::blue         : m_r->setMode(Mode::off);   m_g->setMode(Mode::off);   m_b->setMode(Mode::on);    break;
        case fgc::hal::Led_mode::blue_blink   : m_r->setMode(Mode::off);   m_g->setMode(Mode::off);   m_b->setMode(Mode::blink); break;

        case fgc::hal::Led_mode::off          : // [[fall-through]]
        default                               : m_r->setMode(Mode::off); m_g->setMode(Mode::off); m_b->setMode(Mode::off);       break;
    }
}

// **********************************************************
// * Controller
// **********************************************************
Controller::Controller(const gpio::Controller_ptr& gpio_ctrl, const fpga::Controller_ptr& fpga_ctrl)
{
    // Initialize LEDs
     m_net.init<map::led::Net> (m_leds, gpio_ctrl, fpga_ctrl);
     m_fgc.init<map::led::Fgc> (m_leds, gpio_ctrl, fpga_ctrl);
     m_psu.init<map::led::Psu> (m_leds, gpio_ctrl, fpga_ctrl);
      m_vs.init<map::led::Vs>  (m_leds, gpio_ctrl, fpga_ctrl);
    m_dcct.init<map::led::Dcct>(m_leds, gpio_ctrl, fpga_ctrl);
     m_pic.init<map::led::Pic> (m_leds, gpio_ctrl, fpga_ctrl);
    m_boot.init<map::led::Boot>(m_leds, gpio_ctrl, fpga_ctrl);
}

//************************************************************

void Controller::blink()
{
    // Invert blink state
    m_blink_state = !m_blink_state;

    // Iterate over all HW LEDs and tell them to blink (they know if they are supposed to blink)
    for (auto const &led : m_leds)
    {
        led->blink(m_blink_state);
    }
}

//************************************************************

void Controller::net(const fgc::hal::Led_mode &led_mode)
{
    m_net.setMode(led_mode);
}

//************************************************************

void Controller::fgc(const fgc::hal::Led_mode &led_mode)
{
    m_fgc.setMode(led_mode);
}

//************************************************************

void Controller::psu(const fgc::hal::Led_mode &led_mode)
{
    m_psu.setMode(led_mode);
}

//************************************************************

void Controller::vs(const fgc::hal::Led_mode &led_mode)
{
    m_vs.setMode(led_mode);
}

//************************************************************

void Controller::dcct(const fgc::hal::Led_mode &led_mode)
{
    m_dcct.setMode(led_mode);
}

//************************************************************

void Controller::pic(const fgc::hal::Led_mode &led_mode)
{
    m_pic.setMode(led_mode);
}

//************************************************************

void Controller::boot(const fgc::hal::Led_mode &led_mode)
{
    m_boot.setMode(led_mode);
}


// EOF