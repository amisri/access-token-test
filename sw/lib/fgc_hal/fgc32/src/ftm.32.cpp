//! @file   ftm.cpp
//! @brief  Methods to control NXP hardware timer module (FTM - Flexible Timer Module)
//! @author Dariusz Zielinski

// ---------- Includes
#include <fgc32/inc/ftm.32.h>
#include <fgc32/inc/ftm_timer.32.h>
#include <fgc32/inc/ftm_reg.32.h>


using namespace hal_v32::ftm;

// **********************************************************
// * FTM
// **********************************************************
hal_v32::mem::Map FTM::base = {};

// **********************************************************
// * Hw_timer
// **********************************************************
void Hw_timer::init(const uint8_t& index)
{
    // Memory-map module registers
    m_base = mem::Map(map::gpio::base[index]);

    // Disable clocking source
    FTM::SC::CLKS::set(FTM::SC::CLKSv::no_clock);

    // Reset timer counter value
    FTM::CNT::write(0);

    // Set MOD (final) value to max value of 16-bit timer
    FTM::MOD::write(0xFFFF);

    // Set CNTIN (initial) value to 0
    FTM::CNTIN::write(0);
}

//************************************************************

void Hw_timer::start(const TimerConfig& config) noexcept
{
    m_config  = config;
    m_tick_ns = (1.0 / ((map::system::platform_frequency/2.0) / static_cast<uint8_t>(m_config))) * 1000000000.0;

    // Set correct prescaler
    switch (m_config)
    {
        case TimerConfig::stopped:                                                               break;
        case TimerConfig::tick_2_86_ns:   FTM::SC::PS::set(FTM::SC::PSv::divide_by_1,   m_base); break;
        case TimerConfig::tick_5_71_ns:   FTM::SC::PS::set(FTM::SC::PSv::divide_by_2,   m_base); break;
        case TimerConfig::tick_11_43_ns:  FTM::SC::PS::set(FTM::SC::PSv::divide_by_4,   m_base); break;
        case TimerConfig::tick_22_86_ns:  FTM::SC::PS::set(FTM::SC::PSv::divide_by_8,   m_base); break;
        case TimerConfig::tick_45_71_ns:  FTM::SC::PS::set(FTM::SC::PSv::divide_by_16,  m_base); break;
        case TimerConfig::tick_91_43_ns:  FTM::SC::PS::set(FTM::SC::PSv::divide_by_32,  m_base); break;
        case TimerConfig::tick_182_86_ns: FTM::SC::PS::set(FTM::SC::PSv::divide_by_64,  m_base); break;
        case TimerConfig::tick_365_71_ns: FTM::SC::PS::set(FTM::SC::PSv::divide_by_128, m_base); break;
    };

    if (m_config == TimerConfig::stopped)
    {
        // Disable clock for the module
        FTM::SC::CLKS::set(FTM::SC::CLKSv::no_clock, m_base);
    }
    else
    {
        // Enable the platform (system) clock as a module clock
        FTM::SC::CLKS::set(FTM::SC::CLKSv::system_clock, m_base);

        // Reset counter
        FTM::CNT::write(0, m_base);
    }
}

//************************************************************

void Hw_timer::reset() noexcept
{
    FTM::CNT::write(0, m_base);
}

//************************************************************

std::chrono::nanoseconds Hw_timer::now() noexcept
{
    auto counter_value = FTM::CNT::read();
    return std::chrono::nanoseconds(static_cast<uint32_t>(m_tick_ns * counter_value));
}

//************************************************************

std::chrono::nanoseconds Hw_timer::getCapacity() noexcept
{
    // If the clock is stopped, then m_tick_ns is 0
    return std::chrono::nanoseconds(static_cast<uint32_t>(256 * 256 * m_tick_ns));
}

// **********************************************************
// * Controller
// **********************************************************
Controller::Controller()
{
    Timer<2>::init();
    Timer<3>::init();
    Timer<4>::init();
    Timer<5>::init();
    Timer<6>::init();
    Timer<7>::init();
    Timer<8>::init();
}


// EOF