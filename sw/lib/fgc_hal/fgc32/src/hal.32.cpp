//! @file   hal.cpp
//! @brief  Implementation of the main file for the fgc HAL library.
//! @author Dariusz Zielinski

// ---------- Includes
#include <fgc32/inc/hal.32.h>

using namespace hal_v32;

hal_v32::Hal::Hal()
{
    m_gpio = std::make_shared<gpio::Controller>();
    m_ftm  = std::make_shared<ftm::Controller>();
    m_ifc  = std::make_shared<ifc::Controller>();
    m_isr  = std::make_shared<isr::Controller>();
    m_phy  = std::make_shared<phy::Controller>();
    m_pll  = std::make_shared<pll::Pll>();
    m_fpga = std::make_shared<fpga::Controller>(m_gpio, m_ifc);
    m_leds = std::make_shared<leds::Controller>(m_gpio, m_fpga);
    m_misc = std::make_shared<Misc>(m_gpio);
}

fgc::hal::Version Hal::getVersion() const
{
    return fgc::hal::Version::v32;
}

fgc::hal::Dongle_ptr Hal::getDongle() const
{
    return m_misc;
}

fgc::hal::Eth_controller_ptr Hal::getEthController() const
{
    return m_phy;
}

fgc::hal::Fpga_ptr Hal::getFpgaController() const
{
    return m_fpga;
}

fgc::hal::Gpio_controller_ptr Hal::getGpioController() const
{
    return m_gpio;
}

fgc::hal::Isr_controller_ptr Hal::getIsrController() const
{
    return m_isr;
}

fgc::hal::Led_controller_ptr Hal::getLedController() const
{
    return m_leds;
}

fgc::hal::Power_controller_ptr Hal::getPowerController() const
{
    return m_misc;
}

fgc::hal::Powerspy_output_ptr Hal::getPowerspyOutput() const
{
    return nullptr; // TODO return proper module
}

fgc::hal::Serial_terminal_ptr Hal::getSerialTerminal() const
{
    return nullptr; // TODO return proper module
}


// EOF