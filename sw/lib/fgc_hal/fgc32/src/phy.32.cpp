//! @file   phy.cpp
//! @brief  Provides access to internal registers of the Ethernet PHY chips.
//! @author Dariusz Zielinski

// ---------- Includes
#include <iostream>

#include <cpp_utils/inc/bits.h>
#include <fgc32/inc/mem_reg.32.h>
#include <fgc32/inc/phy.32.h>
#include <fgc32/inc/phy_reg.32.h>


using namespace hal_v32::phy;

// ---------- Methods definitions  ---------------------------
PhyByte::PhyByte(uint8_t byteValue, bool isEnabled) : value(byteValue), enabled(isEnabled)
{
}

//************************************************************

Phy::Phy(std::string interfaceName, uint16_t phyId) : m_mdio(interfaceName, phyId)
{
}

//************************************************************

void Phy::reset()
{
    PHY::BMCR::RESET::set(PHY::BMCR::RESETv::reset, m_mdio);
}

//************************************************************

bool Phy::linkStatus()
{
    return (PHY::BMSR::LINK_STATUS::get(m_mdio) == PHY::BMSR::LINK_STATUSv::valid_link);
}

//************************************************************

void Phy::configurePatternMatching(std::vector<PhyByte> pattern)
{
    // Save pattern length and limit its size if it contains more than 64 elements (the rest is ignored)
    auto length = (pattern.size() < 64 ? pattern.size() : 64);

    // For setting the actual pattern we don't use Mdio_reg class, but access memory directly,
    // as there are 32 of those registers and it's pointless to define them all.

    // Set all 32 register to values taken from the vector, or to 0 for the vector length < 64
    for (uint8_t i = 0; i < 32; ++i)
    {
        // Extract bytes, for odd lengths of the vector we set a second byte to 0
        uint8_t byte1 = (((i*2)      < length) ? pattern[i*2].value     : 0);
        uint8_t byte2 = (((i*2 + 1U) < length) ? pattern[i*2 + 1].value : 0);

        // Write those two bytes into proper register
        m_mdio.write(PHY::receive_pattern_base + i, (byte2 << 8) | byte1);
    }

    // Configure masking of those bytes in the following 4 registers.
    // Each bit in those 4 registers corresponds to a byte in pattern.
    // Setting a bit to '1' disables a corresponding byte (it's not compared with the packet).

    // First calculate values of those 4 registers
    uint16_t regs[4] = {0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};

    for (uint8_t i = 0; i < length; ++i)
    {
        // If the current byte is enabled, set corresponding bit to 0
        if (pattern[i].enabled)
        {
            regs[i / 16] &= ~utils::bit::mask<uint16_t>(i % 16);
        }
    }

    // And then set them
    PHY::RXFPBM1::write(regs[0], m_mdio);
    PHY::RXFPBM2::write(regs[1], m_mdio);
    PHY::RXFPBM3::write(regs[2], m_mdio);
    PHY::RXFPBM4::write(regs[3], m_mdio);

    // Set starting point of matching to 0 (i.e. in the first byte after Ethernet SFD)
    PHY::RXFPATC::PATTERN_START_POINT::set(0, m_mdio);

    // Enable enhanced MAC features need for Wake-On-LAN
    PHY::RXFCFG::ENHANCED_MAC_SUPPORT::set(PHY::RXFCFG::Status::enable, m_mdio);

    // Disable the Wake-On-Lan for broadcast packets (it's enabled by default)
    PHY::RXFCFG::WAKE_ON_BCAST::set(PHY::RXFCFG::Status::disable, m_mdio);

    // Enable issuing a Wake-On-Lan interrupt on pattern match
    PHY::RXFCFG::WAKE_ON_PATTERN::set(PHY::RXFCFG::Status::enable, m_mdio);

    // Enable issuing an interrupt for Wake-On-Lan in general
    PHY::MICR::WOL::set(PHY::MICR::Status::enable, m_mdio);

    // Configure INTN/PWDNN pin as interrupt
    PHY::CFG3::INT_OE::set(PHY::CFG3::INT_OEv::interrupt, m_mdio);


    // Set Wake-On-Lan in pulse mode
    PHY::RXFCFG::WOL_OUT_MODE::set(PHY::RXFCFG::WOL_OUT_MODEv::pulse_mode, m_mdio);

    // Set pulse width to 64 x 125 MHz clock cycles
    PHY::RXFCFG::WOL_OUT_STRETCH::set(PHY::RXFCFG::WOL_OUT_STRETCHv::cycles_64, m_mdio);


    // Set GPIO1 to output Wake-On-Lan signal
    PHY::GPIO_MUX_CTRL::GPIO_1_CTRL::set(PHY::GPIO_MUX_CTRL::GpioMode::wol, m_mdio);
}

//************************************************************

void Phy::clearInterrupts()
{
    PHY::ISR::read(m_mdio);
}

//************************************************************

void Phy::setGreenLed(fgc::hal::Eth_led trigger)
{
    PHY::LEDCR1::LED_0::set(trigger, m_mdio);
}

//************************************************************

void Phy::setYellowLed(fgc::hal::Eth_led trigger)
{
    PHY::LEDCR1::LED_2::set(trigger, m_mdio);
}

//************************************************************

uint16_t Phy::readReceptionStatus()
{
    return PHY::RXFSTS::read(m_mdio);
}

//************************************************************

fgc::hal::Eth_port_ptr Controller::getEth1()
{
    return fgc::hal::Eth_port_ptr();
}

//************************************************************

fgc::hal::Eth_port_ptr Controller::getEth2()
{
    return fgc::hal::Eth_port_ptr();
}


// EOF