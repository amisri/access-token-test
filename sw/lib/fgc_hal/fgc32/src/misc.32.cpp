//! @file   misc.cpp
//! @brief  Implementation for a few different, small methods.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <fgc32/inc/misc.32.h>


using namespace hal_v32;

Misc::Misc(const gpio::Controller_ptr& gpio_ctrl)
{
    m_pin_powercycle_req = gpio_ctrl->getPin<map::gpio::Mcu_pwrcycle_req>();
}

//************************************************************

uint8_t Misc::getId()
{
    return 0;
}

//************************************************************

void Misc::powerCycle()
{
    // Set the pin and leave it enabled, as this is powercycle request,
    // so the CPU will be put into reset anyway.
    m_pin_powercycle_req->set(fgc::hal::State::active);
}



// EOF
