//! @file   gpio.cpp
//! @brief  Accessing GPIO using NXP kernel driver.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <fgc32/inc/gpio.32.h>
#include <fgc32/inc/hw_map.32.h>


using namespace hal_v32::gpio;

// **********************************************************
// * Controller
// **********************************************************
Controller::Controller()
{
    GPIO<1>::init();
    GPIO<2>::init();
    GPIO<3>::init();
    GPIO<4>::init();

    m_mcu_gpio_0 = getPin<map::gpio::Mcu_gpio_0>();
    m_mcu_gpio_1 = getPin<map::gpio::Mcu_gpio_1>();
}

fgc::hal::Gpio_pin_ptr Controller::channel1()
{
    return m_mcu_gpio_0;
}

fgc::hal::Gpio_pin_ptr Controller::channel2()
{
    return m_mcu_gpio_1;
}

//************************************************************




// EOF