//! @file   mem_map.cpp
//! @brief  Implementation of Linux memory mapping with /dev/mem.
//! @author Dariusz Zielinski

// ---------- Includes
#include <fcntl.h>
#include <stdexcept>
#include <sys/mman.h>
#include <unistd.h>

#include <fgc32/inc/mem_map.32.h>


using namespace hal_v32::mem;

Map::Map(uint32_t hw_address, size_t map_size)
{
    // Get this only once - this doesn't change for a given OS
    static auto page_size = static_cast<size_t>(getpagesize());

    // Calculate memory size and mask
    m_map_size = map_size * page_size;
    m_map_mask = m_map_size - 1;

    // Open /dev/mem
    int mem = open("/dev/mem", O_RDWR);

    if (mem == -1)
    {
        throw std::runtime_error("failed to open /dev/mem");
    }

    // Memory map
    // FIXME: there should be a sanity check that (static_cast<off_t>(hw_address) & m_map_mask) == 0
    m_pointer = reinterpret_cast<uint8_t*>(mmap(nullptr, m_map_size, PROT_READ|PROT_WRITE, MAP_SHARED,
                                                mem, static_cast<off_t>(hw_address) & ~m_map_mask));

    if (m_pointer == nullptr)
    {
        throw std::runtime_error("failed to mmap /dev/mem");
    }
}

//************************************************************

uint8_t* Map::pointer() noexcept
{
    return m_pointer;
}

//************************************************************

uint8_t* Map::offset(uint64_t address)
{
    return m_pointer + (address & m_map_mask);
}


// EOF