//! @file   mdio.cpp
//! @brief  Provides access to PHY chips via MDIO interface.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cstring>
#include <linux/sockios.h>
#include <cstdio>
#include <sys/ioctl.h>
#include <unistd.h>

#include <fgc32/inc/mdio.32.h>


using namespace hal_v32::phy;

Mdio::Mdio(const std::string interfaceName, const uint16_t phyId)
{
    // Initialize ifreq structure
    memset(&m_ifr, 0, sizeof(m_ifr));
    strcpy(m_ifr.ifr_name, interfaceName.c_str());

    // Initialize mii_ioctl_data structure
    m_mii = (struct mii_ioctl_data*)(&m_ifr.ifr_data);
    m_mii->phy_id  = phyId;
    m_mii->reg_num = 0;
    m_mii->val_in  = 0;
    m_mii->val_out = 0;

    // Open socket
    m_fd = socket(AF_INET, SOCK_DGRAM, 0);

    if (m_fd < 0)
    {
        // TODO error
    }
}

//************************************************************

Mdio::~Mdio()
{
    if (m_fd >= 0)
    {
        close(m_fd);
    }
}

//************************************************************

uint16_t Mdio::read(uint16_t regNumber)
{
    // Addresses from 0 to 31 can be read normally, using clause 22 access
    if (regNumber < 32)
    {
        return c22Read(regNumber);
    }
    else
    {
        // Otherwise, we need to use clause 45 type of access
        c22Write(0xD, 0x1F);
        c22Write(0xE, regNumber);
        c22Write(0xD, 0x401F);
        return c22Read(0xE);
    }
}

//************************************************************

void Mdio::write(uint16_t regNumber, uint16_t value)
{
    // Addresses from 0 to 31 can be written normally, using clause 22 access
    if (regNumber < 32)
    {
        c22Write(regNumber, value);
    }
    else
    {
        // Otherwise, we need to use clause 45 type of access
        c22Write(0xD, 0x1F);
        c22Write(0xE, regNumber);
        c22Write(0xD, 0x401F);
        c22Write(0xE, value);
    }
}

//************************************************************

uint16_t Mdio::c22Read(const uint16_t regNumber)
{
    // Clause 22 read - can read registers with the address < 32
    if (m_fd != -1)
    {
        m_mii->reg_num = regNumber;

        if (ioctl(m_fd, SIOCGMIIREG, &m_ifr) != -1)
        {
            return m_mii->val_out;
        }
    }

    return 0;
}

//************************************************************

void Mdio::c22Write(const uint16_t regNumber, const uint16_t value)
{
    // Clause 22 write - can write to registers with the address < 32
    if (m_fd != -1)
    {
        m_mii->reg_num = regNumber;
        m_mii->val_in  = value;

        ioctl(m_fd, SIOCSMIIREG, &m_ifr);
    }
}


// EOF