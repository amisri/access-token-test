//! @file   pll.cpp
//! @brief  DAC control for PLL functionality.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <fgc32/inc/pll.32.h>


using namespace hal_v32::pll;


// EOF