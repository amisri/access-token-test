//! @file   isr.cpp
//! @brief  Provides functionality to register ISRs, including fast-ISR for regulation thread.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <fgc32/inc/isr.32.h>


using namespace hal_v32::isr;

void Controller::registerRegulationIsr(utils::callback::Simple callback)
{

}


// EOF