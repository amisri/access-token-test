//! @file   ifc.cpp
//! @brief  Hardware driver for IFC (Integrated Flash Controller) module used to communicate with the FPGA.
//! @author Dariusz Zielinski

// ---------- Includes
#include <sstream>

#include <cpp_utils/bits.h>
#include <fgc32/inc/hw_map.32.h>
#include <fgc32/inc/ifc.32.h>
#include <fgc32/inc/ifc_reg.32.h>
#include <fgc32/inc/mem_reg.32.h>


using namespace hal_v32::ifc;

// ---------- Definitions and internal declarations ----------
//! Instantiate Map variable that holds the memory-mapped for the IFC module
hal_v32::mem::Map IFC::base;

// ---------- Methods definitions  ---------------------------
Controller::Controller()
{
    // Map physical memory of the IFC module to a virtual memory of this program
    IFC::base = mem::Map(map::ifc::base);

    // Map physical memory of the IFC region to a virtual memory of this program
    // TODO this probably should also pass a seconds argument to the mem::Map, to define propert amount of page sizes
    m_region_ptr = mem::Map(map::ifc::region_address, 16);

#if 0
    // ******* Setting address and range

    // Set region address for the selected IFC bank. The address is 24 bits.
    // First, put most significant bits (16-23) into BA_EXT
    IFC::CSPR_EXT::BA_EXT::set(0);

    // Then put the rest of the address (bit 0-15) into BA field of IFC_CSPR register
    //IFC::CSPR::BA::set(0x7FB00000 >> 16);
    IFC::CSPR::BA::set(map::ifc::region_address >> 16);

    // Then set the mask, which effectively sets the size of the memory region (here we use 1MB)
    // For more details refer to reference manual, chapter 22.3.4.
    //IFC::IFC_AMASK::AM::set(0b1111'1111'1111'1111);
    IFC::IFC_AMASK::AM::set(0xFFFF);

    // ******* IFC general settings

    // Set 16 bit port size
    IFC::CSPR::PS::set(IFC::CSPR::PSv::bit_16);

    // Select GPCM as mode for this IFC bank
    IFC::CSPR::MSEL::set(IFC::CSPR::MSELv::gpcm);

    // Check that content of CSPR register is valid
    if (IFC::CSPR::V::get() == IFC::CSPR::Vv::invald)
    {
        // std::cout << "CSPR content not valid" << '\n';
    }

    if (IFC::CSPR::V::get() == IFC::CSPR::Vv::valid)
    {
        // std::cout << "CSPR content is valid" << '\n';
    }


    // ******* GPCM general settings
/*
    // Set GPCM mode of operation to ASIC
    IFC::CSOR_GPCM::GPMODE::set(IFC::CSOR_GPCM::GPMODEv::gpcm);

    // Disable parity checks
    IFC::CSOR_GPCM::PAR_EN::set(IFC::CSOR_GPCM::PAR_ENv::disabled);

    // Set timeout of the module to 262144 cycles (arbitrary decision)
    IFC::CSOR_GPCM::GPTO::set(IFC::CSOR_GPCM::GPTOv::cycles_256);

    // Delay of parity error indication
    IFC::CSOR_GPCM::GAPERRD::set(IFC::CSOR_GPCM::GAPERRDv::delay1);

    // Time for read enable high to output high impedance
    IFC::CSOR_GPCM::TRHZ::set(IFC::CSOR_GPCM::TRHZv::wait20);

    // Set that there is no external buffer present
    IFC::CSOR_GPCM::BCTLD::set(IFC::CSOR_GPCM::BCTLDv::bctlDriven);
*/
    IFC::CSOR_GPCM::write(131072);


    // ******* GPCM timings

    // See reference manual for details. Those settings have to match FPGA implementation of the protocol.
/*  IFC::FTIM0_CS_GPCM::TACSE::set(15);
    IFC::FTIM0_CS_GPCM::TEADC::set(1);
    IFC::FTIM0_CS_GPCM::TEAHC::set(1);

    IFC::FTIM1_CS_GPCM::TACO::set(5);
    IFC::FTIM1_CS_GPCM::TRAD::set(1);

    IFC::FTIM2_CS_GPCM::TCS::set(1);
    IFC::FTIM2_CS_GPCM::TCH::set(1);
    IFC::FTIM2_CS_GPCM::TWP::set(1);

    IFC::FTIM3_CS_GPCM::TAAD::set(1);
*/

    IFC::FTIM0_CS_GPCM::write(3759013902);
    IFC::FTIM1_CS_GPCM::write(4278206208);
    IFC::FTIM2_CS_GPCM::write(255590462);
    IFC::FTIM3_CS_GPCM::write(0);

    IFC::GPCM_EVTER_EN::write(88080384);

    // ******* Clocking

    // Set clock prescaler to 4 - meaning IFC external clock should be 150 MHz
    IFC::CCR::CLKDIV::set(IFC::CCR::CLKDIVv::div_4);

    // Don't delay the clock
    IFC::CCR::CLK_DLY::set(0);

    // Check that content of CSPR register is valid
    if (IFC::CSR::CLK_STAT::get() == IFC::CSR::CLK_STATv::stable)
    {
        // std::cout << "Clock is stable" << '\n';
    }

    if (IFC::CSR::CLK_STAT::get() == IFC::CSR::CLK_STATv::unstable)
    {
        // std::cout << "Clock is unstable" << '\n';
    }

    // ************************************* Other
    IFC::GPCM_EVTER_EN::TOEREN::set(IFC::GPCM_EVTER_EN::Status::disable);
    IFC::GPCM_EVTER_EN::PEREN::set(IFC::GPCM_EVTER_EN::Status::disable);
    IFC::GPCM_EVTER_EN::ABEREN::set(IFC::GPCM_EVTER_EN::Status::disable);
#endif
}

//************************************************************

hal_v32::mem::Map Controller::getRegionPointer()
{
    return m_region_ptr;
}

//************************************************************

std::string Controller::dumpIfcRegisters()
{
    std::stringstream ss;

    ss << "map::ifc::bank      = " << static_cast<int>(map::ifc::bank)                       << "\n\n";

    ss << "REV                 = " << static_cast<uint32_t>(IFC::REV::read())                << '\n';
    ss << "CSPR_EXT            = " << static_cast<uint32_t>(IFC::CSPR_EXT::read())           << '\n';
    ss << "CSPR                = " << static_cast<uint32_t>(IFC::CSPR::read())               << '\n';
    ss << "IFC_AMASK           = " << static_cast<uint32_t>(IFC::IFC_AMASK::read())          << '\n';
    ss << "CSOR_GPCM           = " << static_cast<uint32_t>(IFC::CSOR_GPCM::read())          << '\n';
    ss << "FTIM0_CS_GPCM       = " << static_cast<uint32_t>(IFC::FTIM0_CS_GPCM::read())      << '\n';
    ss << "FTIM1_CS_GPCM       = " << static_cast<uint32_t>(IFC::FTIM1_CS_GPCM::read())      << '\n';
    ss << "FTIM2_CS_GPCM       = " << static_cast<uint32_t>(IFC::FTIM2_CS_GPCM::read())      << '\n';
    ss << "FTIM3_CS_GPCM       = " << static_cast<uint32_t>(IFC::FTIM3_CS_GPCM::read())      << '\n';
    ss << "RB_STAT             = " << static_cast<uint32_t>(IFC::RB_STAT::read())            << '\n';
    ss << "GCR                 = " << static_cast<uint32_t>(IFC::GCR::read())                << '\n';
    ss << "CM_EVTER_STAT       = " << static_cast<uint32_t>(IFC::CM_EVTER_STAT::read())      << '\n';
    ss << "CM_EVTER_EN         = " << static_cast<uint32_t>(IFC::CM_EVTER_EN::read())        << '\n';
    ss << "CM_EVTER_INTR_EN    = " << static_cast<uint32_t>(IFC::CM_EVTER_INTR_EN::read())   << '\n';
    ss << "CM_ERATTR0          = " << static_cast<uint32_t>(IFC::CM_ERATTR0::read())         << '\n';
    ss << "CM_ERATTR1          = " << static_cast<uint32_t>(IFC::CM_ERATTR1::read())         << '\n';
    ss << "CCR                 = " << static_cast<uint32_t>(IFC::CCR::read())                << '\n';
    ss << "CSR                 = " << static_cast<uint32_t>(IFC::CSR::read())                << '\n';
    ss << "GPCM_EVTER_STAT     = " << static_cast<uint32_t>(IFC::GPCM_EVTER_STAT::read())    << '\n';
    ss << "GPCM_EVTER_EN       = " << static_cast<uint32_t>(IFC::GPCM_EVTER_EN::read())      << '\n';
    ss << "GPCM_EVTER_INTR_EN  = " << static_cast<uint32_t>(IFC::GPCM_EVTER_INTR_EN::read()) << '\n';
    ss << "GPCM_ERATTR0        = " << static_cast<uint32_t>(IFC::GPCM_ERATTR0::read())       << '\n';
    ss << "GPCM_ERATTR1        = " << static_cast<uint32_t>(IFC::GPCM_ERATTR1::read())       << '\n';
    ss << "GPCM_ERATTR2        = " << static_cast<uint32_t>(IFC::GPCM_ERATTR2::read())       << '\n';
    ss << "GPCM_STAT           = " << static_cast<uint32_t>(IFC::GPCM_STAT::read())          << '\n';

    return ss.str();
}


// EOF