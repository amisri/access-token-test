//! @file   fpga.cpp
//! @brief  Programming of the FPGA and its state.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <algorithm>
#include <fstream>
#include <thread>

#include <cpp_utils/files.h>
#include <cpp_utils/misc.h>
#include <cpp_utils/timing.h>
#include <fgc32/inc/fpga.32.h>
#include <fgc32/inc/fpga_reg.32.h>
#include <fgc32/inc/mem_map.32.h>
#include <fgc32/inc/hal.32.h>


using namespace hal_v32::fpga;

// **********************************************************
// * Controller
// **********************************************************
Controller::Controller(const gpio::Controller_ptr& gpio_ctrl, const ifc::Controller_ptr& ifc_ctrl)
{
    // Assign the IFC region pointer as a base memory for the FPGA memory map registers
    m_ifc_region = ifc_ctrl->getRegionPointer();

    // Initialize GPIOs
    m_pin_fpga_reset       = gpio_ctrl->getPin<map::gpio::Fpga_prog_rst>();
    m_pin_fpga_conf_done   = gpio_ctrl->getPin<map::gpio::Fpga_conf_done>();
    m_pin_fpga_init        = gpio_ctrl->getPin<map::gpio::Fpga_init>();
    m_pin_conf_mem_prog_en = gpio_ctrl->getPin<map::gpio::Conf_mem_prog_en>();

    // Reset state of the FPGA-related GPIO
    m_pin_fpga_reset->set(fgc::hal::State::inactive);
    m_pin_conf_mem_prog_en->set(fgc::hal::State::inactive);

    // Wait until the FPGA is ready
    waitForFpgaProperState();
}

//************************************************************

bool Controller::programAndVerify(const std::vector<uint8_t> &bitstream)
{
    // Just make sure that bitstream makes sense
    if (bitstream.empty() || bitstream.size() > map::fpga::flash_size)
    {
        return false;
    }

    // First try to open file that represents the flash memory, just to see if it's there and working
    {
        std::ifstream flash_file;
        flash_file.open(map::fpga::flash_path);
        if (flash_file.good() == false)
        {
            return false;
        }
    }

    // Disconnect the flash from FPGA and connect it to the MCU QSPI (refer to schematics for details)
    m_pin_conf_mem_prog_en->set(fgc::hal::State::active);
    {
        utils::Finally reconnect_flash([&]()
        {
            // Connect flash memory back to the FPGA
            m_pin_conf_mem_prog_en->set(fgc::hal::State::inactive);
        });

        try
        {
            // Program the flash memory
            utils::writeBinaryToFile(map::fpga::flash_path, bitstream, true);

            // Read out the whole flash memory
            auto memory_content = utils::readFromBinaryFile(map::fpga::flash_path);

            // Check if the read memory is not shorter than bitstream, as this is an error for sure
            if (memory_content.size() < bitstream.size())
            {
                return false;
            }

            // Now compare bitstream and memory content
            if (std::equal(bitstream.begin(), bitstream.end(), memory_content.begin()) == false)
            {
                return false;
            }
        }
        catch (...)
        {
            return false;
        }
    }

    // At this stage the flash memory is verified to be the same as the bitstream.
    // Disable FPGA watchdog, checking if it's alive
    stopWatchdog();
    utils::Finally enable_watchdog([&]()
    {
        startWatchdog();
    });

    // The FPGA programming procedure is described in "7 Series FPGAs Configuration User Guide" (UG470).
    // As mention there, in 'table 2-4' / page 25: falling edge (-->active) will clear FPGA configuration.
    // On the rising edge (-->inactive) the FPGA will begin to configure (program) itself from the flash.
    m_pin_fpga_reset->set(fgc::hal::State::inactive);
    std::this_thread::sleep_for(std::chrono::microseconds(100));
    m_pin_fpga_reset->set(fgc::hal::State::active);
    std::this_thread::sleep_for(std::chrono::microseconds(100));
    m_pin_fpga_reset->set(fgc::hal::State::inactive);

    // Last thing to check is whether the FPGA successfully reprogrammed itself, by checking if DONE pin is asserted.
    return waitForFpgaProperState();

    // FPGA watchdog will be enabled after exiting this function.
}

//************************************************************

bool Controller::checkWatchdog()
{
    if (m_watchdog_enabled)
    {
        // TODO Check and reset heartbeat flag, set by kernel module
        // TODO In case we don't use heartbeat in the end, then ask FPGA for something constant
    }

    return true;
}

//************************************************************

void Controller::startWatchdog()
{
    m_watchdog_enabled = true;
}

//************************************************************

void Controller::stopWatchdog()
{
    m_watchdog_enabled = false;
}

//************************************************************

hal_v32::mem::Map &Controller::getIfcRegion()
{
    return m_ifc_region;
}

//************************************************************

utils::PriorityMutex &Controller::getPriorityMutex()
{
    return m_priority_mutex;
}

//************************************************************

bool Controller::waitForFpgaProperState()
{
    return utils::setTimeout([&]()
    {
        return m_pin_fpga_conf_done->isActive() && m_pin_fpga_init->isInactive();
    }, map::fpga::programming_timeout);
}


// EOF