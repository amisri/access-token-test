#pragma once  
  
//! @file   gpio.h
//! @brief  Accessing GPIO using NXP kernel driver.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <vector>
#include <mutex>

#include <api/gpio.h>
#include <fgc32/inc/gpio_reg.32.h>


namespace hal_v32::gpio
{
    // ---------- Classes declarations --------------------------
    class Controller;

    //! This class represents a GPIO pin and is used to read/write it. It also implements
    //! the `hal::Gpio_pin` public interface.
    //! @tparam T should be of map::Led_map which holds parameters (port, pin and type) of the pin. See hw_map.h.
    template<class T>
    class Pin : public fgc::hal::Gpio_pin
    {
            friend Controller;

            static_assert(T::port >= 1 && T::port <= 4, "GPIO port index must have value of 1-4");
            static_assert(T::index >= 0 && T::index <= 31, "GPIO pin index must have value of 0-31");

            using Dir_pin   = typename GPIO<T::port>::GPDIR::template PIN<T::index>; //!< Alias for the pin's direction register
            using Dir_value = typename GPIO<T::port>::GPDIR::Value;                  //!< Alias for the direction enum
            using Dat_pin   = typename GPIO<T::port>::GPDAT::template PIN<T::index>; //!< Alias for the pin's data register

        public:
            //! Reads the state of the GPIO, by returning `low` of `high`. It will also check if the
            //! pin is currently configured as input and if not, it will configure it as input first.
            //! This is part of `hal::Gpio_pin` public interface.
            //! @return `hal::State::low` if the pin is in a low state, `hal::State::high` otherwise.
            fgc::hal::State get() override
            {
                setDirection(Dir_value::input);
                return (Dat_pin::get() == 0 ? fgc::hal::State::low : fgc::hal::State::high);
            }

            //************************************************************

            //! Sets pin to a given type. First locks the mutex for the pin's port. It has to lock this, because
            //! in order to change direction or data registers it has to first read them, set/clear relevant bit
            //! and then write the value back to register. If 2 different threads try to do the same, it would
            //! lead to a data race. This function will also set pin as output, if it's not already.
            //! @param state state to set for the pin.
            void set(const fgc::hal::State &state) override
            {
                // Acquire a lock
                std::lock_guard<std::mutex> lock(GPIO<T::port>::mutex);

                setDirection(Dir_value::output, true);
                switch (state)
                {
                    case fgc::hal::State::low:      Dat_pin::set(0);                break;
                    case fgc::hal::State::high:     Dat_pin::set(1);                break;
                    case fgc::hal::State::active:   Dat_pin::set(m_active_value);   break;
                    case fgc::hal::State::inactive: Dat_pin::set(m_inactive_value); break;
                }
            }

            //************************************************************

            //! Checks pin's state, setting it to input if it's not already.
            //! @return true if the pin is currently in an active state.
            bool isActive() override
            {
                setDirection(Dir_value::input);
                return (Dat_pin::get() == m_active_value);
            }

            //************************************************************

            //! Checks pin's state, setting it to input if it's not already.
            //! @return true if the pin is currently in an inactive state.
            bool isInactive() override
            {
                return !isActive();
            }

        private:
            //! A default constructor that is private - so the users of this class cannot create it directly.
            //! This class requires that the GPIO memory-map pointer are initialized first, before this class is used.
            //! To ensure that, the constructor is private and the class can be only created using Controller::getPin.
            Pin() = default;

            //! This a helper function that sets direction of the pin, only if the current direction is different
            //! then the one requested. It can lock the mutex or assume that the mutex is locked.
            //! As it can change common port register it has to lock, for the same reason as the set function (see it).
            //! @param direction new direction to set for the pin.
            //! @param locked if false, the mutex is locked inside the function. If true, it's assumed it has already been locked.
            void setDirection(Dir_value direction, bool locked = false)
            {
                if (m_direction != direction)
                {
                    // Acquire a lock, only if it hasn't been acquired before
                    if (locked == false)
                    {
                        std::lock_guard<std::mutex> lock(GPIO<T::port>::mutex);
                    }

                    // Save current direction
                    m_direction = direction;

                    // Update hardware to match direction
                    Dir_pin::set(m_direction);
                }
            }

            //! Simple const to tell what state (low or high) is "active".
            static constexpr uint8_t m_active_value   = (T::type == map::Pin_type::active_high ? 1 : 0);
            //! Simple const to tell what state (low or high) is "inactive".
            static constexpr uint8_t m_inactive_value = (T::type == map::Pin_type::active_high ? 0 : 1);
            //! A static variable that holds current direction of the pin. It's static to be the same across all instances of the pin.
            static Dir_value m_direction;
    };

    //! Definition of the direction variable.
    template<class T>
    typename Pin<T>::Dir_value Pin<T>::m_direction;

    //! Type alias for std::shared_ptr to the Pin class.
    template<class T>
    using Pin_ptr = std::shared_ptr<Pin<T>>;

    //************************************************************

    //! Main controller of the GPIO. It implements `hal::Gpio_controller` public interface.
    class Controller : public fgc::hal::Gpio_controller
    {
        public:
            //! A default constructor that performs memory-mapping of the GPIO module address space.
            Controller();

            //! Creates a Pin<T> class. It is the only way to create Pin<T> class, as this class
            //! requires the controller to be initialized before usage of the Pin<T> class.
            //!
            //! @tparam T Pin_map class that holds low-level information about GPIO pin.
            //! @return std::shared_ptr to the newly created Pin<T> object.
            template<class T>
            fgc::hal::Gpio_pin_ptr getPin()
            {
                // This trick with a struct below is to enable usage of std::make_shared.
                // std::make_shared won't work if class has a private constructor.
                struct Pin_shared_ptr : public Pin<T>
                {
                };

                return std::make_shared<Pin_shared_ptr>();
            }

            fgc::hal::Gpio_pin_ptr channel1() override;  //!< See api/gpio.h
            fgc::hal::Gpio_pin_ptr channel2() override;  //!< See api/gpio.h

        private:
            fgc::hal::Gpio_pin_ptr m_mcu_gpio_0;  //!< Pin pointer used to implement `hal::Gpio_controller` interface.
            fgc::hal::Gpio_pin_ptr m_mcu_gpio_1;  //!< Pin pointer used to implement `hal::Gpio_controller` interface.
    };

    //! Type alias for std::shared_ptr to the Controller class.
    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF