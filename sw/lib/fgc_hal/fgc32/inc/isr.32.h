#pragma once  
  
//! @file   isr.h
//! @brief  Provides functionality to register ISRs, including fast-ISR for regulation thread.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <vector>

#include <api/interrupts.h>


namespace hal_v32::isr
{
    // ---------- Classes declarations --------------------------
    class Controller : public fgc::hal::Isr_controller
    {
        public:
            void registerRegulationIsr(utils::callback::Simple callback) override;
    };

    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF