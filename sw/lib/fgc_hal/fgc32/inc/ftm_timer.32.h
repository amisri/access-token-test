#pragma once  
  
//! @file   ftm_timer.h
//! @brief  Provide a class to control an FTM hardware timer and a wrapper that satisfies C++ TrivialClock requirements.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <chrono>


namespace hal_v32::ftm
{
    // ---------- Misc declarations -----------------------------
    //! Enum used to configure resolution of a timer.
    enum class TimerConfig : uint8_t
    {
            stopped        = 0b00000000,  //!< Timer disabled (stopped)
            tick_2_86_ns   = 0b00000001,  //!< Timer tick 2,8571428571   ns --> Timer size 0,1872457143  ms
            tick_5_71_ns   = 0b00000010,  //!< Timer tick 5,7142857143   ns --> Timer size 0,3744914286  ms
            tick_11_43_ns  = 0b00000100,  //!< Timer tick 11,4285714286  ns --> Timer size 0,7489828571  ms
            tick_22_86_ns  = 0b00001000,  //!< Timer tick 22,8571428571  ns --> Timer size 1,4979657143  ms
            tick_45_71_ns  = 0b00010000,  //!< Timer tick 45,7142857143  ns --> Timer size 2,9959314286  ms
            tick_91_43_ns  = 0b00100000,  //!< Timer tick 91,4285714286  ns --> Timer size 5,9918628571  ms
            tick_182_86_ns = 0b01000000,  //!< Timer tick 182,8571428571 ns --> Timer size 11,9837257143 ms
            tick_365_71_ns = 0b10000000,  //!< Timer tick 365,7142857143 ns --> Timer size 23,9674514286 ms
    };

    // ---------- Classes declarations --------------------------
    // Forward declarations
    class Controller;

    template<uint8_t module_index>
    class Timer;

    //************************************************************

    //! This a regular class representing FTM timer. It has a private constructor and thus only the Timer class
    //! can instantiate this class. Generally, the Hw_timer class is used to hide implementation,
    //! as the Timer class is part of the public API.
    class Hw_timer
    {
            //! Declare Timer class
            template<uint8_t> friend
            class Timer;

        private:
            //! Default constructor that only the Timer class can invoke.
            Hw_timer() = default;

            //! Initializes the timer by performing memory-map to the FTM address space. It also stops the clock
            //! and reset the counter and configuration registers.
            //! @param index a value 2-8 that corresponds to a hardware module. 2-8 restriction is already asserted by the Timer class.
            void init(const uint8_t &index);

            //! Start the timer with a given resolution (which also influences the timer's capacity - i.e. maximum time
            //! it can hold before overflowing and resetting the counter). This function can also be used to stop the timer
            //! if the passed parameter says so.
            //! @param config defines the timer's resolution or make it stop. See `TimerConfig` enum for details.
            void start(const TimerConfig &config) noexcept;

            //! Resets the timer counter to 0.
            void reset() noexcept;

            //! @return current value of the timer's counter in nanoseconds.
            std::chrono::nanoseconds now() noexcept;

            //! @return the timer's capacity, i.e. maximum time it can hold before overflowing and resetting the counter.
            std::chrono::nanoseconds getCapacity() noexcept;

            TimerConfig m_config  = TimerConfig::stopped;   //!< Holds the current timer's config.
            double      m_tick_ns = 0;                      //!< Number of nanoseconds in single tick of timer.
            mem::Map    m_base;                             //!< Pointer to the memory-mapped FTM address space.
    };

    //************************************************************

    //! Wrapper around Hw_Timer that satisfies C++ TrivialClock requirements.
    //!
    //! @tparam module_index index of hardware timer. Valid values are 2-8.
    template<uint8_t module_index>
    class Timer
    {
            static_assert(module_index >= 2 && module_index <= 8, "Timer module index can only have value 2-8");

            //! The FTM main controller is a friend, so it can execute init() function.
            friend Controller;

        public:
            typedef std::chrono::nanoseconds duration;      //!< C++ Clock requirements: duration type of the clock.
            typedef duration::rep rep;           //!< C++ Clock requirements: type used by duration.
            typedef duration::period period;        //!< C++ Clock requirements: tick period of the clock in sec.
            typedef std::chrono::time_point<Timer> time_point;    //!< C++ Clock requirements: type of a time point object.

            //! Clears the hardware FlexTimer's counter.
            static void start(const TimerConfig &config = TimerConfig::tick_91_43_ns) noexcept
            {
                m_hw_timer.start(config);
            }

            //! Clears the hardware FlexTimer's counter.
            static void reset() noexcept
            {
                m_hw_timer.reset();
            }

            //! Returns current value of the counter. Fulfills a requirement of C++ Clock requirements
            //!
            //! @return time point representing current counter value.
            static time_point now() noexcept
            {
                return time_point(m_hw_timer.now());
            }

            //! Returns current capacity (overflow) of the timer in nanoseconds
            //!
            //! @return time point representing current counter value.
            static std::chrono::nanoseconds getCapacity() noexcept
            {
                return m_hw_timer.getCapacity();
            }

            static constexpr bool is_steady = false;    //!< C++ Clock requirements: this clock is not steady

        private:
            //! Initialize the hardware FlexTimer module.
            static void init()
            {
                m_hw_timer.init(module_index);
            }

            static Hw_timer m_hw_timer;   //!< Holds object of the Hw_timer, different for each FTM module.
    };

    //! Definition of the static member that holds the Ht_timer object inside of the Timer class.
    template<uint8_t index>
    Hw_timer Timer<index>::m_hw_timer;
}


// EOF