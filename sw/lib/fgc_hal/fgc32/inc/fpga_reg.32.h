#pragma once  
  
//! @file   fpga_reg.h
//! @brief  Classes providing high-level access to FPGA memory map via the IFC module.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <array>
#include <cstdint>
#include <type_traits>

#include <cpp_utils/priorityMutex.h>
#include <fgc32/inc/mem_map.32.h>
#include <fgc32/inc/fpga.32.h>


namespace hal_v32::fpga
{
    // ---------- Misc declarations -----------------------------
    using Priority = utils::MutexPriority;   //!< Type alias to keep it short

    // ---------- Classes declarations --------------------------
    //! This class represents a field in the memory map used for communication between the SoC and the FPGA.
    //! This particular implementation (see below for other specialization) is used to represent simple types,
    //! that is not arrays.
    //! The Field class is used in the auto-generated file `fpga_mem_map.h` that defines the memory map.
    //! @tparam base_address address of the field within the memory map
    //! @tparam Access type of a variable used to read/write under this address. Can be any simple type, enum or std::array.
    //! @tparam priority can be high or regular. High priority access is used for regulation-related fields (like ADC/DAC).
    template<uint64_t base_address, class Access, Priority priority = Priority::regular>
    class Field
    {
        public:
            //! The function locks the priority mutex with a given priority and reads from a specified memory location
            //! inside the IFC region address space.
            //! @return value under the given address read from the FPGA via IFC.
            static Access get(const Controller_ptr &fpga_ctrl)
            {
                utils::PriorityLockGuard lock(fpga_ctrl->getPriorityMutex(), priority);

                return fpga_ctrl->getIfcRegion().read<Access>(base_address);
            }

            //************************************************************

            //! The function locks the priority mutex with a given priority and writes to a specified memory location
            //! inside the IFC region address space.
            //! @param value data to be written to the FPGA via IFC under the given address.
            static void set(const Access &value, const Controller_ptr &fpga_ctrl)
            {
                utils::PriorityLockGuard lock(fpga_ctrl->getPriorityMutex(), priority);

                fpga_ctrl->getIfcRegion().write<Access>(base_address, value);
            }
    };

    //************************************************************

    //! This class is a specialization of the generic Field class. It is specialized for std::array and a regular priority.
    //! @tparam base_address address of the field within the memory map
    //! @tparam Array_type type to pass to the std::array
    //! @tparam array_size size of the array
    template<uint64_t base_address, class Array_type, std::size_t array_size>
    class Field<base_address, std::array<Array_type, array_size>, Priority::regular>
    {
            using Array = std::array<Array_type, array_size>;   //!< Type alias for the array used withing the class

        public:
            //! This function will perform read from the IFC region for each element of the array.
            //! The priority mutex will be locked and unlock with every access. This may reduce performance,
            //! but should allow high priority accesses to preempt this loop.
            //! @param array a reference to the array that holds values read from the FPGA via the IFC.
            static void get(Array &array, const Controller_ptr &fpga_ctrl)
            {
                uint64_t address = base_address;
                for (auto &element : array)
                {
                    utils::PriorityLockGuard lock(fpga_ctrl->getPriorityMutex());

                    element = fpga_ctrl->getIfcRegion().read<Array_type>(address);
                    address += sizeof(Array_type);
                }
            }

            //************************************************************

            //! It's a simple wrapper for the get() function above, that returns the array.
            //! @return array that holds values read from the FPGA via IFC.
            static Array get(const Controller_ptr &fpga_ctrl)
            {
                Array array;
                get(array);

                return array;
            }

            //************************************************************

            //! See the description of the get() function. It works analogously, but writes the values instead of reading them.
            //! @param array a reference to the array holding values to be written to the FPGA via the IFC.
            static void set(const Array &array, const Controller_ptr &fpga_ctrl)
            {
                uint64_t address = base_address;
                for (const auto &element : array)
                {
                    utils::PriorityLockGuard lock(fpga_ctrl->getPriorityMutex());

                    fpga_ctrl->getIfcRegion().write<Array_type>(address, element);
                    address += sizeof(Array_type);
                }
            }
    };

    //************************************************************

    //! This class is a specialization of the generic Field class. It is specialized for std::array and a high priority.
    //! @tparam base_address address of the field within the memory map
    //! @tparam Array_type type to pass to the std::array
    //! @tparam array_size size of the array
    template<uint64_t base_address, class Array_type, std::size_t array_size>
    class Field<base_address, std::array<Array_type, array_size>, Priority::high>
    {
            using Array = std::array<Array_type, array_size>;   //!< Type alias for the array used withing the class

        public:
            //! This function will perform read from the IFC region for each element of the array.
            //! The priority mutex will be locked at the beginning so that this high priority access is not
            //! preempted by other accesses.
            //! @param array a reference to the array that holds values read from the FPGA via the IFC.
            static void get(Array &array, const Controller_ptr &fpga_ctrl)
            {
                utils::PriorityLockGuard lock(fpga_ctrl->getPriorityMutex(), Priority::high);

                uint64_t address = base_address;
                for (auto &element : array)
                {
                    element = fpga_ctrl->getIfcRegion().read<Array_type>(address);
                    address += sizeof(Array_type);
                }
            }

            //************************************************************

            //! It's a simple wrapper for the get() function above, that returns the array.
            //! @return array that holds values read from the FPGA via IFC.
            static Array get(const Controller_ptr &fpga_ctrl)
            {
                Array array;
                get(array);

                return array;
            }

            //************************************************************

            //! See the description of the get() function. It works analogously, but writes the values instead of reading them.
            //! @param array a reference to the array holding values to be written to the FPGA via the IFC.
            static void set(const Array &array, const Controller_ptr &fpga_ctrl)
            {
                utils::PriorityLockGuard lock(fpga_ctrl->getPriorityMutex(), Priority::high);

                uint64_t address = base_address;
                for (const auto &element : array)
                {
                    fpga_ctrl->getIfcRegion().write<Array_type>(address, element);
                    address += sizeof(Array_type);
                }
            }
    };
}


// EOF