#pragma once  
  
//! @file   gpio_reg.h
//! @brief  Declaration of hardware registers for the GPIO modules.
//! @author Dariusz Zielinski

// ---------- Includes
#include <mutex>

#include <cpp_utils/bits.h>
#include <fgc32/inc/hw_map.32.h>
#include <fgc32/inc/mem_map.32.h>
#include <fgc32/inc/mem_reg.32.h>


namespace hal_v32::gpio
{
    using utils::nxp::nxp;

    // ---------- Classes declarations --------------------------
    template<uint8_t port_index>
    class GPIO : public Mem_module<uint32_t, Endian::big>
    {
            static_assert(port_index >= 1 && port_index <= 4, "GPIO port index must have value of 1-4");

        public:
            static mem::Map base;
            static std::mutex mutex;

            static void init()
            {
                base = mem::Map(map::gpio::base[port_index]);
            }

            //************************************************************

            //!< GPIO direction register (GPDIR)
            class GPDIR : public Mem_reg<GPIO<port_index>, 0x00>
            {
                public:
                    enum class Value
                    {
                            input = 0,
                            output = 1,
                    };

                    template<uint8_t index>
                    using PIN = Mem_field<GPDIR, nxp(index), nxp(index), Value>;
            };

            //************************************************************

            //!< GPIO open drain register (GPODR)
            class GPODR : public Mem_reg<GPIO, 0x04>
            {
                public:
                    enum class Value
                    {
                            push_pull = 0,
                            open_drain = 1,
                    };

                    template<uint8_t index>
                    using PIN = Mem_field<GPODR, nxp(index), nxp(index), Value>;
            };

            //************************************************************

            //!< GPIO data register
            class GPDAT : public Mem_reg<GPIO, 0x08>
            {
                public:
                    template<uint8_t index>
                    using PIN = Mem_field<GPDAT, nxp(index), nxp(index), uint8_t>;
            };

            //************************************************************

            //!< GPIO interrupt event register:
            class GPIER : public Mem_reg<GPIO, 0x0C>
            {
                public:
                    template<uint8_t index>
                    using PIN = Mem_field<GPIER, nxp(index), nxp(index), uint8_t>;
            };

            //************************************************************

            //!< GPIO interrupt mask register
            class GPIMR : public Mem_reg<GPIO, 0x10>
            {
                public:
                    enum class Value
                    {
                            masked = 0,
                            not_masked = 1,
                    };

                    template<uint8_t index>
                    using PIN = Mem_field<GPIMR, nxp(index), nxp(index), Value>;
            };

            //************************************************************

            //!< GPIO interrupt control register
            class GPICR : public Mem_reg<GPIO, 0x14>
            {
                public:
                    enum class Value
                    {
                            any_change = 0,
                            high_to_low = 1,
                    };

                    template<uint8_t index>
                    using PIN = Mem_field<GPICR, nxp(index), nxp(index), Value>;
            };
    };

    template<uint8_t index> std::mutex    GPIO<index>::mutex = {};
    template<uint8_t index> hal_v32::mem::Map GPIO<index>::base = {};
}


// EOF