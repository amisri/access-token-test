#pragma once

//! @file   phy.h
//! @brief  Provides access to internal registers of the Ethernet PHY chips.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cstdint>
#include <string>
#include <vector>

#include <api/ethernet.h>
#include <fgc32/inc/mdio.32.h>


namespace hal_v32::phy
{
    // ---------- Classes declarations --------------------------
    class PhyByte
    {
        public:
            PhyByte() = default;

            PhyByte(uint8_t byteValue, bool isEnabled);

            uint8_t value   = 0;
            bool    enabled = false;
    };

    //************************************************************

    class Phy
    {
        public:
            Phy(std::string interfaceName, uint16_t phyId);

            void reset();

            bool linkStatus();

            void configurePatternMatching(std::vector<PhyByte> pattern);

            void clearInterrupts();

            void setGreenLed(fgc::hal::Eth_led trigger);

            void setYellowLed(fgc::hal::Eth_led trigger);

            uint16_t readReceptionStatus();

        private:
            Mdio m_mdio;
    };

    using Phy_ptr = std::shared_ptr<Phy>;

    //************************************************************

    class Controller : public fgc::hal::Eth_controller
    {
        public:
            fgc::hal::Eth_port_ptr getEth1() override;

            fgc::hal::Eth_port_ptr getEth2() override;
    };

    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF