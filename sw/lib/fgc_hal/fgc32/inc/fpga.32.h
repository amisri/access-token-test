#pragma once  
  
//! @file   fpga.h
//! @brief  FPGA state monitoring and reprogramming.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <atomic>
#include <memory>
#include <type_traits>
#include <vector>

#include <api/fpga.h>
#include <fgc32/inc/gpio.32.h>
#include <fgc32/inc/ifc.32.h>


namespace hal_v32::fpga
{
    // ---------- Classes declarations --------------------------
    //! Provides FPGA watchdog functionality to periodically check on the FPGA.
    //! It also implements `hal::Fpga` interface that is used to reprogram the FPGA
    //! with provided bitstream.
    class Controller : public fgc::hal::Fpga
    {
        public:
            //! Enables the watchdog and performs memory mapping for the IFC region.
            Controller(const gpio::Controller_ptr &gpio_ctrl, const ifc::Controller_ptr &ifc_ctrl);

            //! Puts the FPGA into a reset state, then switches the analog switch to connect
            //! SoC's QSPI into the flash memory, then reprograms the flash memory with the provided
            //! bitstream. Then it reads the memory and performs verification. Finally it turns the
            //! FPGA back online. FPGA Watchdog is disabled for the time of this function.
            //! @param bitstream vector of bytes representing bitstream for the FPGA
            //! @return currently true on success (TODO)
            bool programAndVerify(const std::vector<uint8_t> &bitstream) override;

            //! This method should be executed from the periodic thread.
            //!
            //! @return true if the working state of the FPGA was confirmed
            bool checkWatchdog() override;

            //! Enables the FPGA watchdog.
            void startWatchdog();

            //! Disables the FPGA watchdog.
            void stopWatchdog();

            //! Gets the pointer to the IFC region
            mem::Map &getIfcRegion();

            //! Gets the priority mutex for accessing IFC
            utils::PriorityMutex &getPriorityMutex();


        private:
            //! Waits for the FPGA's 'done' pin to become active and for the 'init' pin to become inactive.
            //! This combination indicates that the FPGA has properly finished reprogramming and is operational.
            //! This is described in "7 Series FPGAs Configuration User Guide" (UG470).
            //! The waiting will be carried out until timeout.
            //! @return Returns true if the correct state is reached before timeout, false otherwise.
            bool waitForFpgaProperState();

            fgc::hal::Gpio_pin_ptr m_pin_fpga_reset;          //!< GPIO pin. See description in the hw_map.h file.
            fgc::hal::Gpio_pin_ptr m_pin_fpga_conf_done;      //!< GPIO pin. See description in the hw_map.h file.
            fgc::hal::Gpio_pin_ptr m_pin_fpga_init;           //!< GPIO pin. See description in the hw_map.h file.
            fgc::hal::Gpio_pin_ptr m_pin_conf_mem_prog_en;    //!< GPIO pin. See description in the hw_map.h file.
            std::atomic_bool       m_watchdog_enabled{false}; //!< This flag is used by the watchdog thread to know if it's enabled.
            mem::Map               m_ifc_region;              //!< Pointer for memory-mapping of IFC region
            utils::PriorityMutex   m_priority_mutex;          //!< Instantiation of the priority mutex for accessing IFC
    };

    //! Type alias for std::shared_ptr to FPGA controller.
    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF