#pragma once  
  
//! @file   ftm.h
//! @brief  Header to control NXP hardware timer module (FTM - Flexible Timer Module)
//! @author Dariusz Zielinski

// ---------- Includes  
#include <chrono>

#include <api/hal.h>
#include <fgc32/inc/ftm_reg.32.h>


namespace hal_v32::ftm
{
    // ---------- Classes declarations --------------------------
    //! Main class for FTM timers
    class Controller
    {
        public:
            //! This constructor will memory-map regions used by all FTM timers.
            Controller();
    };

    //! Type alias for std::shared_ptr to the Timer controller.
    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF