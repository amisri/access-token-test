#pragma once  
  
//! @file   ftm_reg.h
//! @brief  Declaration of hardware registers for the FTM (Flex Timer Module).
//! @author Dariusz Zielinski

// ---------- Includes  
#include <cpp_utils/bits.h>
#include <fgc32/inc/hw_map.32.h>
#include <fgc32/inc/mem_map.32.h>
#include <fgc32/inc/mem_reg.32.h>


namespace hal_v32::ftm
{
    using utils::nxp::operator""_nxp;

    // ---------- Classes declarations --------------------------
    class FTM : public Mem_module<uint32_t, Endian::big>
    {
        public:
            static mem::Map base;

            //************************************************************

            //!< Status And Control
            class SC: public Mem_reg<FTM, 0x00>
            {
                public:
                    enum class TOFv
                    {
                        not_overflowed = 0,
                        overflowed     = 1,
                    };

                    enum class TOIEv
                    {
                        interrupt_disabled = 0,
                        interrupt_enabled  = 1,
                    };

                    enum class CPWMSv
                    {
                        up_counting       = 0,
                        up_down_counting  = 1,
                    };

                    enum class CLKSv
                    {
                        no_clock              = 0b00,
                        system_clock          = 0b01,
                        fixed_frequency_clock = 0b10,
                        external_clock        = 0b11,
                    };

                    enum class PSv
                    {
                        divide_by_1   = 0b000,
                        divide_by_2   = 0b001,
                        divide_by_4   = 0b010,
                        divide_by_8   = 0b011,
                        divide_by_16  = 0b100,
                        divide_by_32  = 0b101,
                        divide_by_64  = 0b110,
                        divide_by_128 = 0b111,
                    };

                    using TOF   = Mem_field<SC, 24_nxp, 24_nxp, TOFv>;
                    using TOIE  = Mem_field<SC, 25_nxp, 25_nxp, TOIEv>;
                    using CPWMS = Mem_field<SC, 26_nxp, 26_nxp, CPWMSv>;
                    using CLKS  = Mem_field<SC, 27_nxp, 28_nxp, CLKSv>;
                    using PS    = Mem_field<SC, 29_nxp, 31_nxp, PSv>;
            };

            //************************************************************

            //!< Counter
            class CNT: public Mem_reg<FTM, 0x04>
            {
                public:
                    using COUNT = Mem_field<CNT, 16_nxp, 31_nxp, uint16_t>;
            };

            //************************************************************

            //!< Modulo
            class MOD: public Mem_reg<FTM, 0x08>
            {
                public:
                    using MODULO = Mem_field<MOD, 16_nxp, 31_nxp, uint16_t>;
            };

            //************************************************************

            //!< Channel 0 Status And Control
            class C0SC: public Mem_reg<FTM, 0x0C>
            {};

            //************************************************************

            //!< Channel 0 Value
            class C0V: public Mem_reg<FTM, 0x10>
            {};

            //************************************************************

            //!< Channel 1 Status And Control
            class C1SC: public Mem_reg<FTM, 0x14>
            {};

            //************************************************************

            //!< Channel 1 Value
            class C1V: public Mem_reg<FTM, 0x18>
            {};

            //************************************************************

            //!< Channel 2 Status And Control
            class C2SC: public Mem_reg<FTM, 0x1C>
            {};

            //************************************************************

            //!< Channel 2 Value
            class C2V: public Mem_reg<FTM, 0x20>
            {};

            //************************************************************

            //!< Channel 3 Status And Control
            class C3SC: public Mem_reg<FTM, 0x24>
            {};

            //************************************************************

            //!< Channel 3 Value
            class C3V: public Mem_reg<FTM, 0x28>
            {};

            //************************************************************

            //!< Channel 4 Status And Control
            class C4SC: public Mem_reg<FTM, 0x2C>
            {};

            //************************************************************

            //!< Channel 4 Value
            class C4V: public Mem_reg<FTM, 0x30>
            {};

            //************************************************************

            //!< Channel 5 Status And Control
            class C5SC: public Mem_reg<FTM, 0x34>
            {};

            //************************************************************

            //!< Channel 5 Value
            class C5V: public Mem_reg<FTM, 0x38>
            {};

            //************************************************************

            //!< Channel 6 Status And Control
            class C6SC: public Mem_reg<FTM, 0x3C>
            {};

            //************************************************************

            //!< Channel 6 Value
            class C6V: public Mem_reg<FTM, 0x40>
            {};

            //************************************************************

            //!< Channel 7 Status And Control
            class C7SC: public Mem_reg<FTM, 0x44>
            {};

            //************************************************************

            //!< Channel 7 Value
            class C7V: public Mem_reg<FTM, 0x48>
            {};

            //************************************************************

            //!< Counter Initial Value
            class CNTIN: public Mem_reg<FTM, 0x4C>
            {
                public:
                    using INIT = Mem_field<CNTIN, 16_nxp, 31_nxp, uint16_t>;
            };

            //************************************************************

            //!< Capture And Compare Status
            class STATUS: public Mem_reg<FTM, 0x50>
            {};

            //************************************************************

            //!< Features Mode Selection
            class MODE: public Mem_reg<FTM, 0x54>
            {
                public:
                    enum WPDISv
                    {
                        protection_enabled  = 0,
                        protection_disabled = 1,
                    };

                    using WPDIS = Mem_field<MODE, 29_nxp, 29_nxp, WPDISv>;
            };

            //************************************************************

            //!< Synchronization
            class SYNC: public Mem_reg<FTM, 0x58>
            {};

            //************************************************************

            //!< Initial State For Channels Output
            class OUTINIT: public Mem_reg<FTM, 0x5C>
            {};

            //************************************************************

            //!< Output Mask
            class OUTMASK: public Mem_reg<FTM, 0x60>
            {};

            //************************************************************

            //!< Function For Linked Channels
            class COMBINE: public Mem_reg<FTM, 0x64>
            {};

            //************************************************************

            //!< Deadtime Insertion Control
            class DEADTIME: public Mem_reg<FTM, 0x68>
            {};

            //************************************************************

            //!< FTM External Trigger
            class EXTTRIG: public Mem_reg<FTM, 0x6C>
            {};

            //************************************************************

            //!< Channels Polarity
            class POL: public Mem_reg<FTM, 0x70>
            {};

            //************************************************************

            //!< Fault Mode Status
            class FMS: public Mem_reg<FTM, 0x74>
            {
                public:
                    enum WPENv
                    {
                        protection_disabled = 0,
                        protection_enabled  = 1,
                    };

                    using WPEN = Mem_field<FMS, 25_nxp, 25_nxp, WPENv>;
            };

            //************************************************************

            //!< Input Capture Filter Control
            class FILTER: public Mem_reg<FTM, 0x78>
            {};

            //************************************************************

            //!< Fault Control
            class FLTCTRL: public Mem_reg<FTM, 0x7C>
            {};

            //************************************************************

            //!< Quadrature Decoder Control And Status
            class QDCTRL: public Mem_reg<FTM, 0x80>
            {};

            //************************************************************

            //!< Configuration
            class CONF: public Mem_reg<FTM, 0x84>
            {};

            //************************************************************

            //!< FTM Fault Input Polarity
            class FLTPOL: public Mem_reg<FTM, 0x88>
            {};

            //************************************************************

            //!< Synchronization Configuration
            class SYNCONF: public Mem_reg<FTM, 0x8C>
            {};

            //************************************************************

            //!< FTM Inverting Control
            class INVCTRL: public Mem_reg<FTM, 0x90>
            {};

            //************************************************************

            //!< FTM Software Output Control
            class SWOCTRL: public Mem_reg<FTM, 0x94>
            {};

            //************************************************************

            //!< FTM PWM Load
            class PWMLOAD: public Mem_reg<FTM, 0x98>
            {};
    };
}


// EOF