#pragma once

//! @file   mem_reg.h
//! @brief  Classes providing high-level access to memory registers and bitfields.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cstdint>
#include <type_traits>

#include <cpp_utils/bits.h>


namespace hal_v32
{
    namespace bit = utils::bit;

    // ---------- Misc declarations -----------------------------
    enum class Endian
    {
        little,
        big,
    };

    // ---------- Classes declarations --------------------------
    template<class Access_width, Endian endianness = Endian::big>
    class Mem_module
    {
        public:
            using Width  = Access_width;
            static constexpr Endian endian = endianness;
    };

    //************************************************************

    template<class Module, uint64_t base_address>
    class Mem_reg
    {
        public:
            using Width       = typename Module::Width;
            using Base_module = Module;
            using Base        = decltype(Module::base);

            static Width read(Base& base = Module::base)
            {
                if constexpr (Module::endian == Endian::little)
                {
                    // In case of LE just return the read value
                    return base.template read<Width>(base_address);
                }
                else
                {
                    // In case of BE swap bytes first and then return
                    return bit::bs<Width>(base.template read<Width>(base_address));
                }
            }

            //************************************************************

            static void write(Width value, Base& base = Module::base)
            {
                if constexpr (Module::endian == Endian::little)
                {
                    // In case of LE just write the value
                    base.template write<Width>(base_address, value);
                }
                else
                {
                    // In case of BE swap bytes first and then write
                    base.template write<Width>(base_address, bit::bs<Width>(value));
                }
            };
    };

    //************************************************************

    template<class Module, uint64_t base_address>
    class Mdio_reg
    {
        public:
            using Width       = typename Module::Width;
            using Base_module = Module;
            using Base        = typename std::remove_pointer<decltype(Module::base)>::type;

            static Width read(Base& mdio)
            {
                return mdio.read(base_address);
            }

            //************************************************************

            static void write(Width value, Base& mdio)
            {
                mdio.write(base_address, value);
            };
    };

    //************************************************************

    // A class to access register's field
    template<class Reg, uint8_t startBit, uint8_t stopBit, class Value>
    class Mem_field
    {
        public:
            static Value get(typename Reg::Base& base = Reg::Base_module::base)
            {
                // Read register value, apply m_mask to leave only bits of this field
                // and then shift them right to get value of the field alone.
                return static_cast<Value>((Reg::read(base) & m_mask) >> m_start );
            }

            //************************************************************

            static void set(const Value value, typename Reg::Base& base = Reg::Base_module::base)
            {
                // Set value of the register to the value ==
                // Read the current value of the register, clear all the bits of the field using the m_mask
                // and set bits that are one in value  (static casted to appropriate int type and
                // then shifted by startBit and ANDed with the m_mask, to leave only bits relevant for this field).
                Reg::write((Reg::read(base) & (~m_mask)) | ((static_cast<typename Reg::Width>(value) << m_start) & m_mask), base);
            }

        private:
            // Save startBit and stopBit in a way that startBit < stopBit is always true
            static constexpr uint8_t m_start = (startBit < stopBit ? startBit : stopBit);
            static constexpr uint8_t m_stop  = (stopBit > startBit ? stopBit : startBit);
            static constexpr auto    m_mask  = bit::mask<typename Reg::Width>(m_start, m_stop);
    };
}


// EOF