#pragma once

//! @file   ifc_reg.h
//! @brief  Declaration of hardware registers for the IFC module.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cpp_utils/bits.h>
#include <fgc32/inc/hw_map.32.h>
#include <fgc32/inc/mem_map.32.h>
#include <fgc32/inc/mem_reg.32.h>


namespace hal_v32::ifc
{
    using utils::nxp::operator""_nxp;

    // ---------- Classes declarations --------------------------
    class IFC : public Mem_module<uint32_t, Endian::big>
    {
        public:
            static mem::Map base;

            //************************************************************

            class REV : public Mem_reg<IFC, 0x0000>
            {
                public:
                    using REV_MAJ = Mem_field<REV, 4_nxp, 7_nxp, uint8_t>;
                    using REV_MIN = Mem_field<REV, 12_nxp, 15_nxp, uint8_t>;
            };

            //************************************************************

            class CSPR_EXT : public Mem_reg<IFC, 0x000C + (map::ifc::bank * 12)>
            {
                public:
                    using BA_EXT = Mem_field<CSPR_EXT, 24_nxp, 31_nxp, uint8_t>;
            };

            //************************************************************

            class CSPR : public Mem_reg<IFC, 0x0010 + (map::ifc::bank * 12)>
            {
                public:
                    enum class PSv
                    {
                        bit_8  = 0b01,
                        bit_16 = 0b10,
                    };

                    enum class WPv
                    {
                        rw = 0,
                        ro = 1,
                    };

                    enum class TEv
                    {
                        logic_low  = 0,
                        logic_high = 1,
                    };

                    enum class MSELv
                    {
                        nor  = 0b00,
                        nand = 0b01,
                        gpcm = 0b10,
                    };

                    enum class Vv
                    {
                        valid  = 1,
                        invald = 0,
                    };

                    using BA   = Mem_field<CSPR, 0_nxp,  15_nxp, uint16_t>;
                    using PS   = Mem_field<CSPR, 23_nxp, 24_nxp, PSv>;
                    using WP   = Mem_field<CSPR, 25_nxp, 25_nxp, WPv>;
                    using TE   = Mem_field<CSPR, 27_nxp, 27_nxp, TEv>;
                    using MSEL = Mem_field<CSPR, 29_nxp, 30_nxp, MSELv>;
                    using V    = Mem_field<CSPR, 31_nxp, 31_nxp, Vv>;
            };

            //************************************************************

            class IFC_AMASK : public Mem_reg<IFC, 0x00A0 + (map::ifc::bank * 12)>
            {
                public:
                    using AM = Mem_field<IFC_AMASK, 0_nxp, 15_nxp, uint16_t>;
            };

            //************************************************************

            class CSOR_GPCM : public Mem_reg<IFC, 0x0130 + (map::ifc::bank * 12)>
            {
                public:
                    enum class GPMODEv
                    {
                        gpcm = 0,
                        asic = 1,
                    };

                    enum class PARv
                    {
                        odd  = 0,
                        even = 1,
                    };

                    enum class PAR_ENv
                    {
                        disabled = 0,
                        enabled  = 1,
                    };

                    enum class GPTOv
                    {
                        cycles_256     = 0b0000,
                        cycles_512     = 0b0001,
                        cycles_1024    = 0b0010,
                        cycles_2048    = 0b0011,
                        cycles_4096    = 0b0100,
                        cycles_8192    = 0b0101,
                        cycles_16384   = 0b0110,
                        cycles_32768   = 0b0111,
                        cycles_65536   = 0b1000,
                        cycles_131072  = 0b1001,
                        cycles_262144  = 0b1010,
                        cycles_524288  = 0b1011,
                        cycles_1048576 = 0b1100,
                        cycles_2097152 = 0b1101,
                        cycles_4194304 = 0b1110,
                        cycles_8388608 = 0b1111,
                    };

                    enum class ABRT_RSP_ENv
                    {
                        no_error = 0,
                        error    = 1,
                    };

                    enum class RGETAv
                    {
                        abort_mode = 0,
                        ack_mode   = 1,
                    };

                    enum class WGETAv
                    {
                        abort_mode = 0,
                        ack_mode   = 1,
                    };

                    enum class ADM_SHFTv
                    {
                        no_shift    = 0b00000,
                        shift_by_1  = 0b00001,
                        shift_by_2  = 0b00010,
                        shift_by_3  = 0b00011,
                        shift_by_4  = 0b00100,
                        shift_by_5  = 0b00101,
                        shift_by_6  = 0b00110,
                        shift_by_7  = 0b00111,
                        shift_by_8  = 0b01000,
                        shift_by_9  = 0b01001,
                        shift_by_10 = 0b01010,
                        shift_by_11 = 0b01011,
                        shift_by_12 = 0b01100,
                        shift_by_13 = 0b01101,
                        shift_by_14 = 0b01110,
                        shift_by_15 = 0b01111,
                        shift_by_16 = 0b10000,
                        shift_by_17 = 0b10001,
                        shift_by_18 = 0b10010,
                        shift_by_19 = 0b10011,
                        shift_by_20 = 0b10100,
                    };

                    enum class BURST_LENv
                    {
                        no_burst  = 0b000,
                        burst_2   = 0b001,
                        burst_4   = 0b010,
                        burst_8   = 0b011,
                        burst_16  = 0b100,
                        burst_32  = 0b101,
                        burst_64  = 0b110,
                        burst_128 = 0b111,
                    };

                    enum class GAPERRDv
                    {
                        delay_1 = 0b00,
                        delay_2 = 0b01,
                        delay_3 = 0b10,
                        delay_4 = 0b11,
                    };

                    enum class TRHZv
                    {
                        wait_20  = 0b000,
                        wait_40  = 0b001,
                        wait_60  = 0b010,
                        wait_80  = 0b011,
                        wait_100 = 0b100,
                    };

                    enum class BCTLDv
                    {
                        bctl_driven  = 0,
                        bctl_default = 1,
                    };

                    using GPMODE      = Mem_field<CSOR_GPCM, 0_nxp,  0_nxp,  GPMODEv>;
                    using PAR         = Mem_field<CSOR_GPCM, 1_nxp,  1_nxp,  PARv>;
                    using PAR_EN      = Mem_field<CSOR_GPCM, 2_nxp,  2_nxp,  PAR_ENv>;
                    using GPTO        = Mem_field<CSOR_GPCM, 4_nxp,  7_nxp,  GPTOv>;
                    using ABRT_RSP_EN = Mem_field<CSOR_GPCM, 11_nxp, 11_nxp, ABRT_RSP_ENv>;
                    using RGETA       = Mem_field<CSOR_GPCM, 12_nxp, 12_nxp, RGETAv>;
                    using WGETA       = Mem_field<CSOR_GPCM, 13_nxp, 13_nxp, WGETAv>;
                    using ADM_SHFT    = Mem_field<CSOR_GPCM, 14_nxp, 18_nxp, ADM_SHFTv>;
                    using BURST_LEN   = Mem_field<CSOR_GPCM, 20_nxp, 22_nxp, BURST_LENv>;
                    using GAPERRD     = Mem_field<CSOR_GPCM, 23_nxp, 24_nxp, GAPERRDv>;
                    using TRHZ        = Mem_field<CSOR_GPCM, 27_nxp, 29_nxp, TRHZv>;
                    using BCTLD       = Mem_field<CSOR_GPCM, 31_nxp, 31_nxp, BCTLDv>;
            };

            //************************************************************

            class FTIM0_CS_GPCM : public Mem_reg<IFC, 0x01C0 + (map::ifc::bank * 48)>
            {
                public:
                    using TACSE = Mem_field<FTIM0_CS_GPCM, 0_nxp,  3_nxp,  uint8_t>;
                    using TEADC = Mem_field<FTIM0_CS_GPCM, 10_nxp, 15_nxp, uint8_t>;
                    using TEAHC = Mem_field<FTIM0_CS_GPCM, 26_nxp, 31_nxp, uint8_t>;
            };

            //************************************************************

            class FTIM1_CS_GPCM : public Mem_reg<IFC, 0x01C4 + (map::ifc::bank * 48)>
            {
                public:
                    using TACO = Mem_field<FTIM1_CS_GPCM, 0_nxp,  7_nxp,  uint8_t>;
                    using TRAD = Mem_field<FTIM1_CS_GPCM, 18_nxp, 23_nxp, uint8_t>;
            };

            //************************************************************

            class FTIM2_CS_GPCM : public Mem_reg<IFC, 0x01C8 + (map::ifc::bank * 48)>
            {
                public:
                    using TCS = Mem_field<FTIM2_CS_GPCM, 4_nxp,  7_nxp,  uint8_t>;
                    using TCH = Mem_field<FTIM2_CS_GPCM, 10_nxp, 13_nxp, uint8_t>;
                    using TWP = Mem_field<FTIM2_CS_GPCM, 24_nxp, 31_nxp, uint8_t>;
            };

            //************************************************************

            class FTIM3_CS_GPCM : public Mem_reg<IFC, 0x01CC + (map::ifc::bank * 48)>
            {
                public:
                    using TAAD = Mem_field<FTIM3_CS_GPCM, 0_nxp,  5_nxp, uint8_t>;
            };

            //************************************************************

            class RB_STAT : public Mem_reg<IFC, 0x0400>
            {
                public:
                    enum class RBv
                    {
                        busy  = 0,
                        ready = 1,
                    };

                    using RB0 = Mem_field<RB_STAT, 0_nxp,  0_nxp, RBv>;
                    using RB1 = Mem_field<RB_STAT, 1_nxp,  1_nxp, RBv>;
                    using RB2 = Mem_field<RB_STAT, 2_nxp,  2_nxp, RBv>;
                    using RB3 = Mem_field<RB_STAT, 3_nxp,  3_nxp, RBv>;
                    using RB4 = Mem_field<RB_STAT, 4_nxp,  4_nxp, RBv>;
                    using RB5 = Mem_field<RB_STAT, 5_nxp,  5_nxp, RBv>;
                    using RB6 = Mem_field<RB_STAT, 6_nxp,  6_nxp, RBv>;
            };

            //************************************************************

            class GCR : public Mem_reg<IFC, 0x040C>
            {
                public:
                    enum class SOFT_RST_ALLv
                    {
                        no_reset     = 0,
                        assert_reset = 1,
                    };

                    using SOFT_RST_ALL   = Mem_field<GCR, 0_nxp,  0_nxp,  SOFT_RST_ALLv>;
                    using TBCTL_TRN_TIME = Mem_field<GCR, 16_nxp, 20_nxp, uint8_t>;
            };

            //************************************************************

            class CM_EVTER_STAT : public Mem_reg<IFC, 0x0418>
            {
                public:
                    enum class CSERv
                    {
                        no_error = 0,
                        error    = 1,
                    };

                    using CSER = Mem_field<CM_EVTER_STAT, 0_nxp, 0_nxp, CSERv>;
            };

            //************************************************************

            class CM_EVTER_EN : public Mem_reg<IFC, 0x0424>
            {
                public:
                    enum class CSERENv
                    {
                        disabled = 0,
                        enabled  = 1,
                    };

                    using CSEREN = Mem_field<CM_EVTER_EN, 0_nxp, 0_nxp, CSERENv>;
            };

            //************************************************************

            class CM_EVTER_INTR_EN : public Mem_reg<IFC, 0x0430>
            {
                public:
                    enum class CSERIRENv
                    {
                        disabled = 0,
                        enabled  = 1,
                    };

                    using CSERIREN = Mem_field<CM_EVTER_INTR_EN, 0_nxp, 0_nxp, CSERIRENv>;
            };

            //************************************************************

            class CM_ERATTR0 : public Mem_reg<IFC, 0x043C>
            {
                public:
                    enum class ERTYPv
                    {
                        write = 0,
                        read  = 1,
                    };

                    using ERTYP   = Mem_field<CM_ERATTR0, 0_nxp,  0_nxp,  ERTYPv>;
                    using ERAID   = Mem_field<CM_ERATTR0, 4_nxp,  11_nxp, uint8_t>;
                    using ERSRCID = Mem_field<CM_ERATTR0, 16_nxp, 23_nxp, uint8_t>;
            };

            //************************************************************

            class CM_ERATTR1 : public Mem_reg<IFC, 0x0440>
            {
                public:
                    using ERADDR = Mem_field<CM_ERATTR1, 0_nxp, 31_nxp, uint32_t>;
            };

            //************************************************************

            class CCR : public Mem_reg<IFC, 0x044C>
            {
                public:
                    enum class CLKDIVv
                    {
                        div_2 = 0b0001,
                        div_4 = 0b0011,
                        div_8 = 0b0111,
                    };

                    enum class INV_CLK_ENv
                    {
                        not_inverted = 0,
                        inverted     = 1,
                    };

                    using CLKDIV     = Mem_field<CCR, 4_nxp,  7_nxp,  CLKDIVv>;
                    using CLK_DLY    = Mem_field<CCR, 12_nxp, 15_nxp, uint8_t>;
                    using INV_CLK_EN = Mem_field<CCR, 16_nxp, 16_nxp, INV_CLK_ENv>;
            };

            //************************************************************

            class CSR : public Mem_reg<IFC, 0x0450>
            {
                public:
                    enum class CLK_STATv
                    {
                        unstable = 0,
                        stable   = 1,
                    };

                    using CLK_STAT = Mem_field<CSR, 0_nxp,  0_nxp, CLK_STATv>;
            };

            //************************************************************

            class GPCM_EVTER_STAT : public Mem_reg<IFC, 0x1800>
            {
                public:
                    enum class Status
                    {
                        no_error = 0,
                        error    = 1,
                    };

                    using TOER = Mem_field<GPCM_EVTER_STAT, 5_nxp, 5_nxp, Status>;
                    using PER  = Mem_field<GPCM_EVTER_STAT, 7_nxp, 7_nxp, Status>;
                    using ABER = Mem_field<GPCM_EVTER_STAT, 9_nxp, 9_nxp, Status>;
            };

            //************************************************************

            class GPCM_EVTER_EN : public Mem_reg<IFC, 0x180C>
            {
                public:
                    enum class Status
                    {
                        disable = 0,
                        enable  = 1,
                    };

                    using TOEREN = Mem_field<GPCM_EVTER_EN, 5_nxp, 5_nxp, Status>;
                    using PEREN  = Mem_field<GPCM_EVTER_EN, 7_nxp, 7_nxp, Status>;
                    using ABEREN = Mem_field<GPCM_EVTER_EN, 9_nxp, 9_nxp, Status>;
            };

            //************************************************************

            class GPCM_EVTER_INTR_EN : public Mem_reg<IFC, 0x1818>
            {
                public:
                    enum class Status
                    {
                        disable = 0,
                        enable  = 1,
                    };

                    using TOERIREN = Mem_field<GPCM_EVTER_INTR_EN, 5_nxp, 5_nxp, Status>;
                    using PERIREN  = Mem_field<GPCM_EVTER_INTR_EN, 7_nxp, 7_nxp, Status>;
                    using ABERIREN = Mem_field<GPCM_EVTER_INTR_EN, 9_nxp, 9_nxp, Status>;
            };

            //************************************************************

            class GPCM_ERATTR0 : public Mem_reg<IFC, 0x1824>
            {
                public:
                    enum class ERCSv
                    {
                        bank_0 = 0b000,
                        bank_1 = 0b001,
                        bank_2 = 0b010,
                        bank_3 = 0b011,
                        bank_4 = 0b100,
                        bank_5 = 0b101,
                        bank_6 = 0b110,
                    };

                    enum class ERTYPEv
                    {
                        write = 0,
                        read  = 1,
                    };

                    using ERSRCID = Mem_field<GPCM_ERATTR0, 0_nxp,  7_nxp,  uint8_t>;
                    using ERAID   = Mem_field<GPCM_ERATTR0, 12_nxp, 19_nxp, uint8_t>;
                    using ERCS    = Mem_field<GPCM_ERATTR0, 23_nxp, 25_nxp, ERCSv>;
                    using ERTYPE  = Mem_field<GPCM_ERATTR0, 31_nxp, 31_nxp, ERTYPEv>;
            };

            //************************************************************

            class GPCM_ERATTR1 : public Mem_reg<IFC, 0x1828>
            {
                public:
                    using ERADDR = Mem_field<GPCM_ERATTR1, 0_nxp, 31_nxp, uint32_t>;
            };

            //************************************************************

            class GPCM_ERATTR2 : public Mem_reg<IFC, 0x182C>
            {
                public:
                    enum class PERR_ADv
                    {
                        address_phase = 0,
                        data_phase    = 1,
                    };

                    using PERR_BEAT = Mem_field<GPCM_ERATTR2, 20_nxp, 21_nxp, uint8_t>;
                    using PERR_BYTE = Mem_field<GPCM_ERATTR2, 24_nxp, 27_nxp, uint8_t>;
                    using PERR_AD   = Mem_field<GPCM_ERATTR2, 31_nxp, 31_nxp, PERR_ADv>;
            };

            //************************************************************

            class GPCM_STAT : public Mem_reg<IFC, 0x1830>
            {
                public:
                    enum class GPCM_BSYv
                    {
                        not_busy = 0,
                        busy     = 1,
                    };

                    using GPCM_BSY = Mem_field<GPCM_STAT, 0_nxp, 0_nxp, GPCM_BSYv>;
            };
    };
}


// EOF