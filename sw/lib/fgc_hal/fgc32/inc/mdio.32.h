#pragma once

//! @file   mdio.h
//! @brief  Provides access to PHY chips via MDIO interface.
//! @author Dariusz Zielinski

// ---------- Includes
#include <string>

#include <linux/mii.h>
#include <net/if.h>

#include <cpp_utils/misc.h>


namespace hal_v32::phy
{
    // ---------- Classes declarations --------------------------
    class Mdio : utils::NoncopyableNonmovable
    {
        public:
            Mdio(std::string interfaceName, uint16_t phyId);

            ~Mdio();

            uint16_t read(uint16_t regNumber);

            void write(uint16_t regNumber, uint16_t value);

        private:
            uint16_t c22Read(uint16_t regNumber);

            void c22Write(uint16_t regNumber, uint16_t value);

            ifreq           m_ifr;
            mii_ioctl_data* m_mii;
            int             m_fd = -1;
    };
}


// EOF