#pragma once  
  
//! @file   pll.h
//! @brief  DAC control for PLL functionality.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <vector>

#include <api/hal.h>


namespace hal_v32::pll
{
    // ---------- Classes declarations --------------------------
    class Pll
    {

    };

    using Pll_ptr = std::shared_ptr<Pll>;
}


// EOF