#pragma once  
  
//! @file   hal.h
//! @brief  Main header file with functions to get controllers instances, to be used internally inside the library.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <api/hal.h>
#include <fgc32/inc/fpga.32.h>
#include <fgc32/inc/ftm.32.h>
#include <fgc32/inc/gpio.32.h>
#include <fgc32/inc/ifc.32.h>
#include <fgc32/inc/isr.32.h>
#include <fgc32/inc/leds.32.h>
#include <fgc32/inc/misc.32.h>
#include <fgc32/inc/phy.32.h>
#include <fgc32/inc/pll.32.h>


namespace hal_v32
{
    class Hal : public fgc::hal::Hal
    {
        public:
            Hal() {
                m_gpio = std::make_shared<hal_v32::gpio::Controller>();
                m_ifc = std::make_shared<hal_v32::ifc::Controller>();
                m_fpga = std::make_shared<hal_v32::fpga::Controller>(m_gpio, m_ifc);
            }

            virtual ~Hal() = default;

            fgc::hal::Version              getVersion()         const override { return fgc::hal::Version::v32; }

            // fgc::hal::Dongle_ptr           getDongle()          const override;
            // fgc::hal::Eth_controller_ptr   getEthController()   const override;
            fgc::hal::Fpga_ptr             getFpgaController()  const override { return m_fpga; }
            fgc::hal::Gpio_controller_ptr  getGpioController()  const override { return m_gpio; }
            fgc::hal::Isr_controller_ptr   getIsrController()   const override { return m_isr; }
            fgc::hal::Led_controller_ptr   getLedController()   const override { return m_leds; }
            // fgc::hal::Power_controller_ptr getPowerController() const override;
            // fgc::hal::Powerspy_output_ptr  getPowerspyOutput()  const override;
            // fgc::hal::Serial_terminal_ptr  getSerialTerminal()  const override;

        protected:
            fpga::Controller_ptr    m_fpga;
            ftm::Controller_ptr     m_ftm;
            gpio::Controller_ptr    m_gpio;
            ifc::Controller_ptr     m_ifc;
            isr::Controller_ptr     m_isr;
            leds::Controller_ptr    m_leds;
            phy::Controller_ptr     m_phy;
            pll::Pll_ptr            m_pll;
            Misc_ptr                m_misc;
    };
}


// EOF