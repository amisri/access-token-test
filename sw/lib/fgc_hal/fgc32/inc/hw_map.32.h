#pragma once  
  
//! @file   hw_map.h
//! @brief  This file contains mappings of hardware addresses, constants and parameters into semantic names.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <chrono>

#include <cpp_utils/bits.h>
#include <cpp_utils/callbacks.h>
#include <cpp_utils/circularBuffer.h>
#include <cpp_utils/files.h>
#include <cpp_utils/priorityMutex.h>
#include <cpp_utils/timing.h>
#include <cpp_utils/misc.h>

namespace hal_v32
{
    // ---------- Forward declarations ------------------------------
    // Hardware LED types (defined in leds.h)
    namespace leds
    {
        class Led_hw_none;
        template<class T> class Led_hw_gpio;
        template<class T> class Led_hw_fpga;
    }

    namespace map
    {
        // ---------- Misc declarations -----------------------------
        template<class R, class G, class B>
        class Led_map
        {
            public:
                using Type_r = R;
                using Type_g = G;
                using Type_b = B;
        };

        // **********************************************************

        enum class Pin_type
        {
            active_low,
            active_high
        };

        template<uint8_t port_value, uint8_t index_value, Pin_type type_value>
        class Pin_map
        {
            public:
                static constexpr  uint8_t  port  = port_value;
                static constexpr  uint8_t  index = index_value;
                static constexpr  Pin_type type  = type_value;
        };


        // ---------- Hardware mapping ------------------------------
        // **********************************************************
        // * System
        // **********************************************************
        namespace system
        {
            constexpr uint32_t cpu_frequency      = 1800000000;  //!< CPU base frequency in Hz
            constexpr uint32_t platform_frequency = 700000000;   //!< Platform clock frequency in Hz
        }

        // **********************************************************
        // * IFC mapping
        // **********************************************************
        namespace ifc
        {
            //! Base address of the IFC module
            constexpr uint32_t base = 0x1530000;

            //! Base address of the memory region used by the IFC bank to communicate
            constexpr uint32_t region_address = 0x7fb00000;

            //! Size of the memory region used by the IFC bank. Expressed in number of bits
            constexpr uint32_t region_size = 16;    // 16 bit == 64 KB

            //! Memory bank index (chip-select) to be used by this class (valid 0-6)
            constexpr uint8_t bank = 0;
        }

        // **********************************************************
        // * FTM mapping
        // **********************************************************
        namespace ftm
        {
            //! FTM modules base addresses.
            //! First index has value of 0, because modules are numerated from 1, not 0.
            //! Second index has value of 0, because module 1 is already used by the OS, so it should not be used here.
            constexpr uint32_t base[] = {0, 0, 0x29E0000, 0x29F0000, 0x2A00000, 0x2A10000, 0x2A20000, 0x2A30000, 0x2A40000};
        }

        // **********************************************************
        // * GPIO mapping
        // **********************************************************
        namespace gpio
        {
            //! GPIO modules (ports) base addresses.
            //! First index has value of 0, because ports are numerated from 1, not 0.
            constexpr uint32_t base[] = {0, 0x2300000, 0x2310000, 0x2320000, 0x2330000};

            // Mappings of pins used based on the electrical schematic
            using Td1_alert         = Pin_map<1, 23, Pin_type::active_low>;   //!< ?
            using Td1_fault         = Pin_map<1, 24, Pin_type::active_low>;   //!< ?
            using Conf_mem_prog_en  = Pin_map<1, 25, Pin_type::active_low>;   //!< Enables programming of FPGA flash.
            using Mcu_cmd_enable    = Pin_map<1, 26, Pin_type::active_low>;   //!< ?
            using Fpga_conf_done    = Pin_map<1, 27, Pin_type::active_high>;  //!< FPGA "DONE" pin. It is set high by the FPGA, when it finishes the configuration.
            using Fpga_init         = Pin_map<1, 28, Pin_type::active_low>;   //!< FPGA "INIT_B" pin. It's low during configuration or stays low on error. So it should be high.
            using Fpga_prog_rst     = Pin_map<1, 29, Pin_type::active_low>;   //!< FPGA "PROGRAM_B" pin. Falling edge resets the configuration, rising edge starts reprogramming.
            using Fpga_heartbeat    = Pin_map<1, 30, Pin_type::active_low>;   //!< Pulse from the FPGA that start the regulation iteration and is also used for FPGA watchdog.
            using Ddr4_event        = Pin_map<1, 31, Pin_type::active_low>;   //!<

            using Mcu_gpio_0        = Pin_map<4, 2,  Pin_type::active_high>;   //!<
            using Mcu_gpio_1        = Pin_map<4, 3,  Pin_type::active_high>;   //!<
            using Pf_mcu_irq        = Pin_map<1, 21, Pin_type::active_high>;   //!<
            using Mcu_pf_irq        = Pin_map<1, 19, Pin_type::active_high>;   //!<
            using Fpga_irq0         = Pin_map<1, 18, Pin_type::active_high>;   //!<
            using Fpga_irq1         = Pin_map<1, 16, Pin_type::active_high>;   //!<

            using Led_dcct_blue     = Pin_map<4, 10, Pin_type::active_low>;    //!< Pin controlling blue color of the DCCT LED.
            using Led_pic_blue      = Pin_map<4, 11, Pin_type::active_low>;    //!< Pin controlling blue color of the PIC LED.
            using Led_boot_green    = Pin_map<1, 22, Pin_type::active_low>;    //!< Pin controlling green color of the BOOT LED.
            using Led_boot_red      = Pin_map<1, 20, Pin_type::active_high>;   //!< Pin controlling red color of the BOOT LED.

            using Mcu_1wire_off_req = Pin_map<4, 12, Pin_type::active_high>;   //!<
            using Mcu_pwrcycle_req  = Pin_map<4, 13, Pin_type::active_low>;    //!<
            using Mcu_asleep        = Pin_map<1, 13, Pin_type::active_low>;    //!<
        }

        // **********************************************************
        // * FPGA / flash memory
        // **********************************************************
        namespace fpga
        {
            //! Path to a file that provides access to the flash memory via QSPI
            constexpr auto flash_path = "/dev/mtdblock0";

            //! Size of the flash memory in bytes
            constexpr uint32_t flash_size = 16 * 1024 * 1024;

            //! Maximum time (timeout) for reprogramming the FPGA
            constexpr auto programming_timeout = std::chrono::seconds(5);   // TODO that's probably too much

            //! Interval at which FPGA Watchdog is being checked.
            //! It should not be shorter than interval for the lowest regulation frequency possible.
            constexpr auto watchdog_interval = std::chrono::milliseconds(10); // 100 Hz
        }

        // **********************************************************
        // * LED mapping
        // **********************************************************
        namespace led
        {
            // Typedefs for LED types
                              using None = hal_v32::leds::Led_hw_none;
            template<class T> using Gpio = hal_v32::leds::Led_hw_gpio<T>;
            template<class T> using Fpga = hal_v32::leds::Led_hw_fpga<T>;

            //! Interval at which LEDs blink
            constexpr auto blinking_interval = std::chrono::milliseconds(500);

            // LEDs declarations
            using Net  = Led_map<None,                     None,                       None>;
            using Fgc  = Led_map<None,                     None,                       None>;
            using Psu  = Led_map<None,                     None,                       None>;
            using Vs   = Led_map<None,                     None,                       None>;
            using Dcct = Led_map<None,                     None,                       Gpio<gpio::Led_dcct_blue>>;
            using Pic  = Led_map<None,                     None,                       Gpio<gpio::Led_pic_blue>>;
            using Boot = Led_map<Gpio<gpio::Led_boot_red>, Gpio<gpio::Led_boot_green>, None>;
        }
    }
}





// EOF