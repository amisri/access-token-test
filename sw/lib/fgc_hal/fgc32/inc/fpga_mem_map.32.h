#pragma once  
  
//! @file   fpga_mem_map.h
//! @brief  Auto-generated FPGA memory map.

// *************************************************************************
// ***                        AUTO-GENERATED FILE                        ***
// ***  This file was auto-generated and must not be modified manually.  ***
// *************************************************************************

// ---------- Includes  
#include <fgc32/inc/fpga_reg.32.h>

namespace hal_v32::FPGA
{
    namespace MID
    {
        using FGCTYPE   = fpga::Field<0x00000000, uint32_t>;
        using FGCVER    = fpga::Field<0x00000004, uint32_t>;
        using PLDTYPE   = fpga::Field<0x00000008, uint32_t>;
        using PLDVER    = fpga::Field<0x0000000C, uint32_t>;
        using CRATETYPE = fpga::Field<0x00000010, uint32_t>;
        using NETTYPE   = fpga::Field<0x00000014, uint32_t>;
        using EOMID     = fpga::Field<0x0000001C, uint32_t>;
    }

    namespace ANA
    {
        // (... stuff before DAC)
        namespace DAC
        {
            using A = fpga::Field<0x00000240, uint32_t, fpga::Priority::high>;
            using B = fpga::Field<0x00000244, uint32_t, fpga::Priority::high>;
        }
        // (stuff after DAC ...)
    }

    namespace WR
    {
        namespace TX
        {
            namespace STATUS
            {
                using FAULT      = fpga::Field<0x00000100, bool>;
                using SIMULATION = fpga::Field<0x00000102, bool>;
            }

            using I_MEAS = fpga::Field<0x00000104, uint32_t>;
            using V_MEAS = fpga::Field<0x00000108, uint32_t>;

            // (... rest of the WR that I omitted)
        }
    }

    namespace SCIVS
    {
        // (...)
        using MCU_BUF = fpga::Field<0x00001100, std::array<uint8_t, 64>>;
        using SCI_BUF = fpga::Field<0x00001180, std::array<uint8_t, 64>>;
    }

    namespace FIR
    {
        namespace A
        {
            // I don't know: Z and P are supposed to be arrays??
            using Z     = fpga::Field<0x00000108, std::array<uint16_t, 4>>;  // 8 bytes?
            using P     = fpga::Field<0x00000108, std::array<uint16_t, 3>>;  // 6 bytes???
            using SUM_Z = fpga::Field<0x00000108, uint16_t>;                 // 2 bytes
        }

        // (B, C, D ...)
    }
}


// EOF