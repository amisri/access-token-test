#pragma once  
  
//! @file   leds.h
//! @brief  Header for classes implementing LED interface.
//! @author Dariusz Zielinski

// ---------- Includes  
#include <memory>
#include <vector>

#include <api/hal.h>
#include <fgc32/inc/gpio.32.h>
#include <fgc32/inc/fpga.32.h>


namespace hal_v32::leds
{
    // ---------- Misc declarations -----------------------------
    enum class Mode
    {
        off,
        on,
        blink,
    };

    // ---------- Classes declarations --------------------------
    class Led_hw
    {
        public:
            void setMode(const Mode &state);

            void blink(const bool &blink_state);

        protected:
            virtual void setState(const bool &state) = 0;

        private:
            Mode m_state = Mode::off;
    };

    using Led_hw_ptr = std::shared_ptr<Led_hw>;

    //************************************************************

    class Led_hw_none : public Led_hw
    {
        public:
            Led_hw_none(const gpio::Controller_ptr &, const fpga::Controller_ptr &)
            {}

        protected:
            void setState(const bool &state) override
            {
                // None type means there's not LED so implementation does nothing
            }
    };

    //************************************************************

    template<class T>
    class Led_hw_gpio : public Led_hw
    {
        public:
            Led_hw_gpio(const gpio::Controller_ptr &gpio_ctrl, const fpga::Controller_ptr &)
            {
                m_pin = gpio_ctrl->getPin<T>();
            }

        protected:
            void setState(const bool &state) override
            {
                m_pin->set(state ? fgc::hal::State::active : fgc::hal::State::inactive);
            }

        private:
            fgc::hal::Gpio_pin_ptr m_pin;
    };

    //************************************************************

    template<class T>
    class Led_hw_fpga : public Led_hw
    {
        public:
            Led_hw_fpga(const gpio::Controller_ptr &, fpga::Controller_ptr fpga_ctrl)
                    : m_fgpa(std::move(fpga_ctrl))
            {
            }

        protected:
            void setState(const bool &state) override
            {
                // TODO when I have the FPGA mmap
                // T::set(state);
            }

        private:
            fpga::Controller_ptr m_fgpa;
    };

    //************************************************************

    class Led_rgb
    {
        public:
            void setMode(const fgc::hal::Led_mode &led_mode);

            template<class Led_map>
            void init(std::vector<Led_hw_ptr> &led_list, const gpio::Controller_ptr &gpio_ctrl,
                      const fpga::Controller_ptr &fpga_ctrl)
            {
                // Instantiate individuals LEDs
                m_r = std::make_shared<typename Led_map::Type_r>(gpio_ctrl, fpga_ctrl);
                m_g = std::make_shared<typename Led_map::Type_g>(gpio_ctrl, fpga_ctrl);
                m_b = std::make_shared<typename Led_map::Type_b>(gpio_ctrl, fpga_ctrl);

                // Add individual LEDs to the vector
                led_list.push_back(m_r);
                led_list.push_back(m_g);
                led_list.push_back(m_b);

                // Turn off the LED (as default)
                setMode(fgc::hal::Led_mode::off);
            }

        private:
            Led_hw_ptr m_r;
            Led_hw_ptr m_g;
            Led_hw_ptr m_b;
    };

    //************************************************************

    class Controller : public fgc::hal::Led_controller
    {
        public:
            Controller(const gpio::Controller_ptr &gpio_ctrl, const fpga::Controller_ptr &fpga_ctrl);

            void blink() override;

            void net(const fgc::hal::Led_mode &led_mode) override;

            void fgc(const fgc::hal::Led_mode &led_mode) override;

            void psu(const fgc::hal::Led_mode &led_mode) override;

            void vs(const fgc::hal::Led_mode &led_mode) override;

            void dcct(const fgc::hal::Led_mode &led_mode) override;

            void pic(const fgc::hal::Led_mode &led_mode) override;

            void boot(const fgc::hal::Led_mode &led_mode) override;

        protected:
            std::vector<Led_hw_ptr> m_leds;

        private:
            bool    m_blink_state = false;
            Led_rgb m_net;
            Led_rgb m_fgc;
            Led_rgb m_psu;
            Led_rgb m_vs;
            Led_rgb m_dcct;
            Led_rgb m_pic;
            Led_rgb m_boot;
    };

    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF