#pragma once

//! @file   phy_reg.h
//! @brief  Declaration of the internal hardware registers for the Ethernet PHY chips.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cpp_utils/inc/bits.h>
#include <fgc32/inc/mdio.32.h>
#include <fgc32/inc/mem_map.32.h>
#include <fgc32/inc/mem_reg.32.h>
#include <fgc32/inc/phy.32.h>


namespace hal_v32::phy
{
    // ---------- Classes declarations --------------------------
    //! PHY register mapping (Based on DP83867E/IS/CS datasheet)
    class PHY : public Mem_module<uint16_t, Endian::little>
    {
        public:
            static Mdio*              base;
            constexpr static uint16_t receive_pattern_base = 0x013C;

            //************************************************************

            class BMCR : public Mdio_reg<PHY, 0x0000>
            {
                public:
                    enum class RESETv
                    {
                        normal = 0,
                        reset  = 1,
                    };

                    using RESET = Mem_field<BMCR, 15, 15, RESETv>;
            };

            //************************************************************

            class BMSR : public Mdio_reg<PHY, 0x0001>
            {
                public:
                    enum class LINK_STATUSv
                    {
                        no_link     = 0,
                        valid_link  = 1,
                    };

                    using LINK_STATUS = Mem_field<BMSR, 2, 2, LINK_STATUSv>;
            };

            //************************************************************

            class CFG3 : public Mdio_reg<PHY, 0x001E>
            {
                public:
                    enum class INT_OEv
                    {
                        power_down = 0,
                        interrupt  = 1,
                    };

                    using INT_OE = Mem_field<CFG3, 7, 7, INT_OEv>;
            };

            //************************************************************

            class LEDCR1 : public Mdio_reg<PHY, 0x0018>
            {
                public:
                    using LED_GPIO = Mem_field<LEDCR1, 12, 15, fgc::hal::Eth_led>;
                    using LED_2    = Mem_field<LEDCR1, 8, 11, fgc::hal::Eth_led>;
                    using LED_1    = Mem_field<LEDCR1, 4, 7, fgc::hal::Eth_led>;
                    using LED_0    = Mem_field<LEDCR1, 0, 3, fgc::hal::Eth_led>;
            };

            //************************************************************

            class MICR : public Mdio_reg<PHY, 0x0012>
            {
                public:
                    enum class Status
                    {
                        disable = 0,
                        enable  = 1,
                    };

                    using AUTONEG_ERR        = Mem_field<MICR, 15, 15, Status>;
                    using SPEED_CHNG         = Mem_field<MICR, 14, 14, Status>;
                    using DUPLEX_MODE_CHNG   = Mem_field<MICR, 13, 13, Status>;
                    using PAGE_RECEIVED      = Mem_field<MICR, 12, 12, Status>;
                    using AUTONEG_COMP       = Mem_field<MICR, 11, 11, Status>;
                    using LINK_STATUS_CHNG   = Mem_field<MICR, 10, 10, Status>;
                    using FALSE_CARRIER      = Mem_field<MICR,  8,  8, Status>;
                    using MDI_CROSSOVER_CHNG = Mem_field<MICR,  6,  6, Status>;
                    using SPEED_OPT_EVENT    = Mem_field<MICR,  5,  5, Status>;
                    using SLEEP_MODE_CHNG    = Mem_field<MICR,  4,  4, Status>;
                    using WOL                = Mem_field<MICR,  3,  3, Status>;
                    using XGMII_ERR          = Mem_field<MICR,  2,  2, Status>;
                    using POLARITY_CHNG      = Mem_field<MICR,  1,  1, Status>;
                    using JABBER             = Mem_field<MICR,  0,  0, Status>;
            };

            //************************************************************

            class ISR : public Mdio_reg<PHY, 0x0013>
            {
            };

            //************************************************************

            class RXFCFG : public Mdio_reg<PHY, 0x0134>
            {
                public:
                    enum class Status
                    {
                        disable = 0,
                        enable  = 1,
                    };

                    enum class WOL_OUT_STRETCHv
                    {
                        cycles_64 = 0b11,
                        cycles_32 = 0b10,
                        cycles_16 = 0b01,
                        cycles_8  = 0b00,
                    };

                    enum class WOL_OUT_MODEv
                    {
                        level_mode = 0,
                        pulse_mode = 1,
                    };

                    using WOL_OUT_STRETCH      = Mem_field<RXFCFG, 9, 10, WOL_OUT_STRETCHv>;
                    using WOL_OUT_MODE         = Mem_field<RXFCFG, 8,  8, WOL_OUT_MODEv>;
                    using ENHANCED_MAC_SUPPORT = Mem_field<RXFCFG, 7,  7, Status>;
                    using WAKE_ON_UCAST        = Mem_field<RXFCFG, 4,  4, Status>;
                    using WAKE_ON_BCAST        = Mem_field<RXFCFG, 2,  2, Status>;
                    using WAKE_ON_PATTERN      = Mem_field<RXFCFG, 1,  1, Status>;
                    using WAKE_ON_MAGIC        = Mem_field<RXFCFG, 0,  0, Status>;
            };

            //************************************************************

            class RXFSTS : public Mdio_reg<PHY, 0x0135>
            {
                public:
                    enum class Status
                    {
                        not_received = 0,
                        received     = 1,
                    };

                    using UCAST_RCVD   = Mem_field<RXFCFG, 4,  4, Status>;
                    using BCAST_RCVD   = Mem_field<RXFCFG, 2,  2, Status>;
                    using PATTERN_RCVD = Mem_field<RXFCFG, 1,  1, Status>;
                    using MAGIC_RCVD   = Mem_field<RXFCFG, 0,  0, Status>;
            };

            //************************************************************

            class RXFPBM1: public Mdio_reg<PHY, 0x015C>  { };

            //************************************************************

            class RXFPBM2: public Mdio_reg<PHY, 0x015D>  { };

            //************************************************************

            class RXFPBM3: public Mdio_reg<PHY, 0x015E>  { };

            //************************************************************

            class RXFPBM4: public Mdio_reg<PHY, 0x015F>  { };

            //************************************************************

            class RXFPATC: public Mdio_reg<PHY, 0x0161>
            {
                public:
                    using PATTERN_START_POINT = Mem_field<RXFPATC, 0, 5, uint8_t>;
            };

            //************************************************************

            class GPIO_MUX_CTRL : public Mdio_reg<PHY, 0x0172>
            {
                public:
                    enum class GpioMode
                    {
                        constant_1      = 0b1001,
                        constant_0      = 0b1000,
                        prbs_errors     = 0b0111,
                        led_4           = 0b0110,
                        lpi_on          = 0b0101,
                        energy_detect   = 0b0100,
                        wol             = 0b0011,
                        rx1588          = 0b0010,
                        tx1588          = 0b0001,
                        col_or_rx_err   = 0b0000,
                    };

                    using GPIO_1_CTRL = Mem_field<GPIO_MUX_CTRL, 4, 7, GpioMode>;
                    using GPIO_0_CTRL = Mem_field<GPIO_MUX_CTRL, 0, 3, GpioMode>;
            };
    };
}


// EOF