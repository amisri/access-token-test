#pragma once  
  
//! @file   mem_map.h
//! @brief  The class in this file is used to map hardware (physical) memory into the process virtual memory.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cstdint>

#include <cpp_utils/bits.h>


namespace hal_v32::mem
{
    // ---------- Classes declarations --------------------------
    class Map
    {
        public:
            //! Default constructor. After its execution, the internal pointer is set to nullptr,
            Map() = default;

            //! The constructor maps a hardware memory (pointed via hw_address) to the process virtual memory using /dev/mem.
            //! This way a userland program (this app) can access a hardware (physical) memory via returned pointer.
            //! In case of failure nullptr is returned.
            //!
            //! @param hw_address Address of a hardware (physical) memory to map.
            explicit Map(uint32_t hw_address, size_t map_size = 1);

            uint8_t *pointer() noexcept;

            uint8_t *offset(uint64_t address);

            template<class Width>
            Width read(uint64_t address)
            {
                return utils::bit::read<Width>(offset(address));
            }

            template<class Width>
            void write(uint64_t address, Width value)
            {
                utils::bit::write<Width>(offset(address), value);
            }

        private:
            uint8_t* m_pointer  = nullptr;   //!< Pointer pointing to a virtual memory address that maps to a physical one.
            size_t   m_map_size = 0;         //!< Size of the mapped region (multiple of memory page size).
            size_t   m_map_mask = 0;         //!< Memory mask used in accessing mapped memory.
    };
}


// EOF