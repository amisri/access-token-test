#pragma once

//! @file   ifc.h
//! @brief  Hardware driver for IFC (Integrated Flash Controller) module used to communicate with the FPGA.
//! @author Dariusz Zielinski

// ---------- Includes
#include <cstdint>
#include <memory>
#include <string>

#include <fgc32/inc/mem_map.32.h>


namespace hal_v32::ifc
{
    // ---------- Classes declarations --------------------------
    class Controller
    {
        public:
            Controller();

            mem::Map getRegionPointer();

            std::string dumpIfcRegisters();

        private:
            mem::Map m_region_ptr;
    };

    using Controller_ptr = std::shared_ptr<Controller>;
}


// EOF