#pragma once

//! @file   misc.h
//! @brief  A few different, small methods in a single class.
//! @author Dariusz Zielinski

// ---------- Includes
#include <memory>

#include <api/gpio.h>
#include <api/dongle.h>
#include <api/power.h>
#include <fgc32/inc/gpio.32.h>


namespace hal_v32
{
    // ---------- Classes declarations --------------------------
    class Misc : public fgc::hal::Dongle, public fgc::hal::Power_controller
    {
        public:
            Misc(const gpio::Controller_ptr& gpio_ctrl);

            uint8_t getId() override;

            void powerCycle() override;

        private:
            fgc::hal::Gpio_pin_ptr m_pin_powercycle_req;
    };

    using Misc_ptr = std::shared_ptr<Misc>;
}


// EOF