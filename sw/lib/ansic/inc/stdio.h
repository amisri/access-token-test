/*---------------------------------------------------------------------------------------------------------*\
  File:		stdio.h

  Contents:	This file contains the constants, structures and function prototypes for the printf.c
		DICO version of the ANSI library functions.

  Notes:	Only a very limited subset of STDIO functions are supported.

  History:

    15/06/99	qak	Modified to match Dico Version of printf.c
    19/09/00	qak	idx field in struct chnl is now unsigned short instead of unsigned char
\*---------------------------------------------------------------------------------------------------------*/

#pragma ONCE

#ifndef _H_STDIO_
#define _H_STDIO_

#include <stdarg.h>

/*----- Constants -----*/

#define EOF		(-1)
#define NULL		((void*)0)
#define	NL		0x01		// Newline flag
#define	INHIBIT		0x02		// Inhibit stream flag
#define	NO_CR		0x04		// Don't add CR after NL

/*----- Callback structure and function definition -----*/

typedef void (*fputc_cb)(char ch, struct chnl *f);
typedef void (*fputc_store_cb)(char ch);

typedef struct chnl {
    unsigned short	_offset;	// Next free space in buffer
    char		*_p;	   	// Start of output buffer
    fputc_cb		cb;		// Callback for character output
    unsigned char	_flags;		// Stream flags
} FILE;

/*----- Printf.c functions and macros -----*/

#define fputc(ch,f)	(f)->cb(ch,f)

extern int  fputs	(char * far s, FILE *f);
extern int  fprintf	(FILE *f, char * far format, ...);
extern int  vfprintf	(FILE *f, char * far format, va_list args);
extern int  sprintf	(char *s, char * far format, ...);
extern int  vsprintf	(char *s, char * far format, va_list args);

#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: stdio.h
\*---------------------------------------------------------------------------------------------------------*/

