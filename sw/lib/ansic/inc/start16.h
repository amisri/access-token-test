/*****************************************************
  start16.h : Startup data structure for MC68HC16
  ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#ifndef START16_H
#define START16_H

#include <definfo.h>

typedef unsigned char *far	POINTER;
typedef unsigned short		Word;
typedef unsigned char		Byte;
typedef void 			(*PROC)(void);

/**** The following is a description of a memory range starting at address
      'base' and 'size' bytes long. (I.e. [base .. (base+size)[) */
typedef struct _Range {
  POINTER  beg;
  Word     size;
} _Range;

/* Note: The structure above does not exactly describe the actual range
         descriptors generate by the linker. To save memory, the linker
         simply allocates a POINTER, followed by a WORD, i.e. 5 bytes.
         Due to a padding byte added by the compiler to make the struct's
         size even, one cannot use this description to access the memory
         containing the ranges to clear. */

/**** The copy-down descriptor. */
typedef struct _Copy {
  Word     size;
  Byte     filler;
  POINTER  dest;
/*
  Byte     data[(size & 1) ? size + 1 : size];
*/
} _Copy;


/* After the data of a copy-down descriptor, the next one follows, i.e. the
   whole copy-down section can be described as

      CopyDesc copyDownSection[];

   An entry with (size == 0) terminates the array.
 */

/* HIWARE object file format */

typedef POINTER _Init;

extern struct _tagStartup {
  Word       flags;
  PROC       main;             /* Main function of user's application       */
  Byte       filler0;
  Byte       dataPage;         /* Always equals the stack page!             */
  Byte       filler1;
  POINTER    stackOffset;      /* Initial value of the stack pointer        */
  Word       nofZeroOuts;      /* #blocks to clear in memory                */
  _Range *far pZeroOut;        /* Pointer to array of '_Range', terminated
                                 by an entry with size 0                   */
  Byte       filler3;
  _Copy *far toCopyDownBeg;    /* First copy descriptor                     */
  PROC *far  mInits;           /* Pointer to array of function pointers,
                                  terminated by a 0 entry                   */
  _Init *far libInits;         /* Pointer to array of pointers to startup
                                  descriptors of ROM libraries, terminated
                                  by 0x0000FFFF                             */
} _startupData;

#if (FGC_CLASS_ID == 50)
#pragma CODE_SEG START_UP
#endif
extern void _Startup (void);     /* Execution begins in this procedure        */
#if (FGC_CLASS_ID == 50)
#pragma CODE_SEG DEFAULT
#endif

#endif


