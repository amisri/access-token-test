/*****************************************************
libdefs.h - ANSI-C library: library configuration definition
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
         Do not modify the Copyright above
 *****************************************************/

#pragma ONCE

#ifndef _H_LIBDEFS_
#define _H_LIBDEFS_

#ifdef __cplusplus
  extern "C" {
#endif

/*
  This file contains all defines to configure the library build.
  Normally they can be set on the compiler command line during library build,
  e.g. the compiler option
    -DLIBDEF_FAR_HEAP=1
  would enable that the library is build for a 'far' memory used by malloc, free,...
  NOTE: you have to set the define to zero or one, just -DLIBDEF_FAR_HEAP is wrong!

  If the following defines are NOT set on the compiler command line, a default is assumed.
  Note that not all configuration defines may be available for a target, e.g. if the
  target does not support the 'far' keyword.

  Please be careful:
  If you build your library using special setting on the command line, use them to build
  your application too, else your application uses not the correct interface to the library!
*/

#include <hidef.h>

/*---------------------------------------------------------------------------------
  GENERAL TYPES
  Header Files: stdlib.h
                string.h
                assert.h
                stdio.h
  Functions   : vprintf, sprintf, vsprintf, vsscanf, sscanf
                strtod, strol, strtoul,
                getenv, system
                mblen, mbtowc, mbstowcs,
                strcat, strncat, strcpy, strncpy, strcmp, strncmp,
                strchr, strrchr, strspn, strcspn, strpbrk, strstr,
                strtok, strcoll, strxfrm, strlen,
                _assert
                bsearch, qsort
  Defines     : LIBDEF_FAR_CONST_STRINGS  (0 (default) or 1)
                LIBDEF_FAR_STRINGS        (0 (default) or 1)
                LIBDEF_FAR_CONST_VOID_PTR (0 (default) or 1)
                LIBDEF_FAR_VOID_PTR       (0 (default) or 1)
  Data Segment: none
  Code Segment: none
  ---------------------------------------------------------------------------------*/
/* some processor allocate strings (e.g. "abc") in the far area */
#if defined(__HC05__) || defined(__ST7__) || defined(__XA__)
  #define LIBDEF_FAR_CONST_STRINGS  1
  #define LIBDEF_FAR_STRINGS        1
  #define LIBDEF_FAR_CONST_VOID_PTR 1
  #define LIBDEF_FAR_VOID_PTR       1
  #define LIBDEF_FAR_CONSTANTS      1
#endif

#ifndef LIBDEF_FAR_CONST_STRINGS
  #define LIBDEF_FAR_CONST_STRINGS  0   /* define this to 1 if const strings (const char*) are placed in 'far' heap memory */
#endif

#ifndef LIBDEF_FAR_STRINGS
  #define LIBDEF_FAR_STRINGS        0   /* define this to 1 if strings (char *) are placed in 'far' heap memory */
#endif

#ifndef LIBDEF_FAR_CONST_VOID_PTR
  #define LIBDEF_FAR_CONST_VOID_PTR 0   /* define this to 1 if const void pointers (const void *) have to point into 'far' heap memory */
#endif

#ifndef LIBDEF_FAR_VOID_PTR
  #define LIBDEF_FAR_VOID_PTR       0   /* define this to 1 if void pointers (void *) have to point into 'far' heap memory */
#endif

#ifndef LIBDEF_FAR_CONSTANTS
  #define LIBDEF_FAR_CONSTANTS      0   /* define this to 1 if pointers to constants (const *) have to point into 'far' heap memory */
#endif


/* const char Data PTR Qualifier CCHAR_DPTRQ */
#if LIBDEF_FAR_CONST_STRINGS
  #define LIBDEF_CCHAR_DPTRQ _FAR  /* see hidef.h */
#else /* default */
  #define LIBDEF_CCHAR_DPTRQ  /* empty */
#endif

/* char Data PTR Qualifier CHAR_DPTRQ */
#if LIBDEF_FAR_STRINGS
  #define LIBDEF_CHAR_DPTRQ  _FAR
#else /* default */
  #define LIBDEF_CHAR_DPTRQ  /* empty */
#endif

/* const void Data PTR Qualifier CVOID_DPTRQ */
#if LIBDEF_FAR_CONST_VOID_PTR
  #define LIBDEF_CVOID_DPTRQ  _FAR
#else /* default */
  #define LIBDEF_CVOID_DPTRQ  /* empty */
#endif

/* void Data PTR Qualifier VOID_DPTRQ */
#if LIBDEF_FAR_VOID_PTR
  #define LIBDEF_VOID_DPTRQ  _FAR
#else /* default */
  #define LIBDEF_VOID_DPTRQ  /* empty */
#endif

/* const Data PTR Qualifier CONST_DPTRQ */
#if LIBDEF_FAR_CONSTANTS
  #define LIBDEF_CONST_DPTRQ  _FAR
#else /* default */
  #define LIBDEF_CONST_DPTRQ  /* empty */
#endif

typedef const char *LIBDEF_CCHAR_DPTRQ ConstString;
typedef const char *LIBDEF_CCHAR_DPTRQ LIBDEF_ConstStringPtr;
typedef       char *LIBDEF_CHAR_DPTRQ  LIBDEF_StringPtr;

typedef const void *LIBDEF_CVOID_DPTRQ    ConstMem;
typedef const void *LIBDEF_CVOID_DPTRQ    LIBDEF_ConstMemPtr;
typedef       void *LIBDEF_VOID_DPTRQ     LIBDEF_MemPtr;
typedef       char *LIBDEF_VOID_DPTRQ  LIBDEF_MemBytePtr; /* pointer to a byte in memory */
typedef const char *LIBDEF_VOID_DPTRQ  LIBDEF_ConstMemBytePtr; /* pointer to a constant byte in memory */

/*---------------------------------------------------------------------------------
  MEMORY MANAGEMENT:
  Header Files: stdlib.h
                heap.h
  Functions   : calloc
                free
                malloc
                realloc
                _heapcrash_
  Defines     : LIBDEF_FAR_HEAP_DATA  (0 (default) or 1)
                LIBDEF_HEAPSIZE       (default 2000)
  Data Segment: HEAP_SEGMENT          far if LIBDEF_FAR_HEAP_DATA is 1
  Code Segment: DEFAULT
  ---------------------------------------------------------------------------------*/
#ifndef LIBDEF_FAR_HEAP_DATA
  #define LIBDEF_FAR_HEAP_DATA  0   /* define this to 1 if you want to use 'far' heap memory */
#endif

/* Heap Data PTR Qualifier HEAP_DPTRQ */
#if LIBDEF_FAR_HEAP_DATA
  #define LIBDEF_HEAP_DPTRQ  far
#else
  #define LIBDEF_HEAP_DPTRQ  /* empty */
#endif

#define LIBDEF_HEAPSIZE 2000  /* Adjust to your requirements, nofBytes = 4*LIBDEF_HEAPSIZE,
                                 must be multipe of 4!  */
/*---------------------------------------------------------------------------------
  NUMBER CONVERSION FUNCTIONS:
  Header Files: stdlib.h
  Functions   :
  Defines     : LIBDEF_FAR_NUMCONV_DATA  (0 (default) or 1)
  Data Segment: none
  Code Segment: DEFAULT
  ---------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------*/
/* stdlib.c */
/*---------------------------------------------------------------------------------*/
/* The following defines are used to tailor the ANSI-C library code overhead.
   To get better code (improved code density and speed), disable them */
#define LIBDEF_ENABLE_OVERFLOW_CHECK  1   /* ANSI standard overflow checking. Switch off for less code. */
#define LIBDEF_STRTOD_EXTENDED_PREC   1   /* Extended precision for strtod. Disable it for less code */

/* printf, scanf */
#define LIBDEF_PRINTF_FLOATING        1   /* if %e,%E,%f,%F (floating point) in printf/scanf is needed */
#define LIBDEF_PRINTF_LONG            (1 || LIBDEF_PRINTF_FLOATING)  /* if %l,%L, is needed */
#define LIBDEF_PRINTF_ALLOW_OCTINT    0   /* if %o     in printf is needed */
#define LIBDEF_PRINTF_ALLOW_HEXINT    1   /* if %x, %X is printf needed */
#define LIBDEF_PRINTF_FAR_PTR         0  /* (1 && (defined(__HC12__) || defined(__HC16__)))  */
#define LIBDEF_SCANF_BUF_SIZE         255 /* buffer size needed in scanf: may be reduced to save RAM */
#define LIBDEF_MULTI_CHAR_SELECTION   1   /* if the "[" scanf format is needed */
#define LIBDEF_REENTRANT_PRINTF       1   /* if a reentrant version of printf and sprintf is needed */
/*---------------------------------------------------------------------------------*/

#ifdef __cplusplus
 }
#endif

#endif /* _H_LIBDEFS_ */

/*****************************************************/

