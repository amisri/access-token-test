/*****************************************************
        errno.h - ANSI-C library: error codes
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#pragma ONCE

#ifndef _H_ERRNO_
#define _H_ERRNO_

#define ERESET      0  /* Reset                      */
#define EDOM        1  /* ANSI Required              */
#define ERANGE      2  /* ANSI Required              */
#define ESIG_ERR    3  /* Signal number out of range */
#define EOVERFLOW   4  /* Overflow                   */
#define EUNDERFLOW  5  /* Underflow                  */

extern int errno;

#endif

/*****************************************************/
/* end errno.h */
