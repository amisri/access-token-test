#ifndef _STDBOOL_H_
#define _STDBOOL_H_

// Make bool a 16 bit integer to keep structures aligned to word boundaries

typedef unsigned short bool;

#define true  1
#define false 0

#endif // _STDBOOL_H_
