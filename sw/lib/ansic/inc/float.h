/*****************************************************
    float.h - ANSI-C library: floating point format
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#pragma ONCE

#ifndef _H_FLOAT_
#define _H_FLOAT_

/**** Rounding Mode */

#define FLT_ROUNDS         -1
#define FLT_RADIX           2

#define FLT_MANT_DIG       24
#define FLT_DIG             6
#define FLT_MIN_EXP      -126
#define FLT_MIN_10_EXP    -37
#define FLT_MAX_EXP       127
#define FLT_MAX_10_EXP     38
#define FLT_MAX             3.402823466E+38F
#define FLT_EPSILON         1.19209290E-07F
#define FLT_MIN             1.17549435E-38F

#define DBL_MANT_DIG       53
#define DBL_DIG            15
#define DBL_MIN_EXP     -1022
#define DBL_MIN_10_EXP   -307
#define DBL_MAX_EXP      1023
#define DBL_MAX_10_EXP    308
#define DBL_MAX             1.7976931348623157E+308
#define DBL_EPSILON         2.2204460492503131E-16
#define DBL_MIN             2.2259738585972014E-308

#endif

/*****************************************************/
/* end float.h */
