/*****************************************************
    stdarg.h - ANSI-C library: standard arguments
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#pragma ONCE

#ifndef _H_STDARG_
#define _H_STDARG_

typedef char *va_list;

#define va_start(ap,v)          ((ap) = (char *)&v + sizeof(v))
#define __va(ap, type)     (((type *)((ap) += sizeof (type)))[-1])

#define va_arg(ap, type)   ((sizeof (type) & 1) ?       \
                              ((ap)++, __va (ap, type)) : \
                              __va (ap, type))
#define va_end(ap)
#endif

/* end stdarg.h */
