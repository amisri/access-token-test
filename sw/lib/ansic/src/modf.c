/*****************************************************
   modf.c - Low-level ANSI library routines
             For DSP Format MC68HC16
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#define FLT_BIAS      127

/******************************************************************************/

typedef struct {
  unsigned int hi, lo;
} FLOAT;

/******************************************************************************/


void modff (FLOAT val)
  /* Calculate integral and fractional part of a floating point number by searching
     for the decimal point in the mantissa. The fraction is returned, the integral
     part is stored directly into variable 'int_part'. The mask used below contains
     1-bits to the left of the decimal point and 0-bits to the right, i.e. the inte-
     gral part becomes v & mask (normalized), the mantissa of the fraction becomes
     v & ~mask (denormalized). The fraction then has to be normalized and packed.
       Note: register Y contains the address of the integral part, the fractional
     part is returned in E:D. */
{
  unsigned long mask;

  asm {
                    LDD   val.hi
                    LSLD
                    SUBA  #FLT_BIAS
                    BPL   Large
                    CLRW  0,Y           ; Exp < 0
                    CLRW  2,Y           ; int_p := 0.0
                    LDE   val.hi        ; fract := val
                    LDD   val.lo
                    BRA   End
    Large:          CMPA  #22
                    BLE   Ok            ; Exp >= 23
                    LDD   val.hi        ; int_p := val
                    STD   0,Y
                    LDD   val.lo
                    STD   2,Y
    FractZero:      CLRD                ; fract := 0.0
                    TDE
                    BRA   End
    Ok:             CLRB
                    XGAB
                    XGDX                ; X is exponent
                    LDD   #0xFF80       ; Initial mask
                    CLRE
                    CPX   #0
                    BEQ   EndLoop
    Loop:           ASRD                ; Build mask
                    RORE
                    AIX   #-1
                    BNE   Loop
    EndLoop:        STD   mask          ; Save mask
                    STE   mask:2
                    ANDD  val.hi        ; Compute integral part
                    ANDE  val.lo
                    STD   0,Y           ; Store integral part
                    STE   2,Y
                    LDD   mask          ; Compute mask for fraction
                    LDE   mask:2
                    COMD
                    ORD   #0xFF80
                    COME
                    ANDD  val.hi        ; Compute fraction
                    ANDE  val.lo
                    LSLD
                    LSRB
                    BNE   NotZero
                    TSTE
                    BEQ   FractZero
    NotZero:        DECA
                    LSLE
                    ROLB
                    BCC   NotZero
                    INCA
                    LSRD
                    RORE
                    BRCLR val.hi, #0x80, Pos
                    ORAA  #0x80
    Pos:            XGDE
    End:
  } /* end asm */;
} /* end modff */

/******************************************************************************/

void frexpf (FLOAT val)
  /* Split a REAL number into exponent and mantissa such that val = 2^exp * mant,
     0.5 > ABS (mant) <= 1.0. Returns both exp = 0 and mantissa = 0.0 if val = 0.0,
     if val = +/- infinity, exp = 128 and mantissa = +/- 1.0  Mantissa returned as
     float in E:D.
     __Y_BASED__: exponent stored at @X
     else       : exponent stored at @Y. */
{
  asm {
                    LDD   val.hi
                    BNE   NotZero
                    LDE   val.lo
                    BNE   NotZero
                    STD   0,Y
                    BRA   End
    NotZero:        LSLD
                    XGAB
                    CMPB  #0xFF
                    BNE   NotInf
                    LDD   #128
                    STD   0,Y
                    LDE   #0x3F80
                    CLRD
                    BRCLR val.hi, #0x80, PosInf
                    ORE   #0x8000
    PosInf:         BRA   End
    NotInf:         SUBB  #(FLT_BIAS - 1)
                    SXT
                    STD   0,Y
                    LDE   val.hi
                    ANDE  #0x807F
                    ORE   #0x3F00
                    LDD   val.lo
    End:
  } /* end asm */;
} /* end frexpf */

/******************************************************************************/

void ldexpf (FLOAT val, int exp)
  /* Returns val * 2^exp. If an underflow occures, 0.0 is returned, in case of an
     overflow, +/- infinity is returned. The result is returned in E:D. */
{
  asm {
                    LDD   val.hi
                    BNE   NotZero
                    TSTW  val.lo
                    BEQ   RetZero
    NotZero:        LSLD
                    CLRB
                    XGAB
                    ADDD  exp
                    BVS   RetInf
                    BPL   NotSmall
    RetZero:        CLRD                      ; Return 0
                    TDE
                    BRA   End
    NotSmall:       CPD   #0x00FF
                    BLS   NotLarge
    RetInf:         CLRD                      ; Return infinity
                    LDE   #0x7F80
                    BRCLR val.hi, #0x80, End
                    ORE   #0x8000
                    BRA   End
    NotLarge:       TBA
                    LDAB  val.hi:1
                    LSLB
                    LSRD
                    BRCLR val.hi, #0x80, Pos
                    ORAA  #0x80
    Pos:            LDE   val.lo
                    XGDE
    End:
  } /* end asm */;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void modf (void)
{
  asm   JMP   modff;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void frexp (void)
{
  asm   JMP   frexpf;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void ldexp (void)
{
  asm   JMP   ldexpf;
}

/******************************************************************************
    FAR routines, do not depend on the float format chosen!!
 ******************************************************************************/

void far_modff (FLOAT val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  modff (val);
  asm {
       PULM  K
  } /* end asm */;
} /* end far_modff */

void far_frexpf (FLOAT val)
{
  asm {
       PSHM  K
       TBYK
  } /* end asm */;
  frexpf (val);
  asm {
       PULM  K
  } /* end asm */;
} /* end far_frexpf */

#pragma NO_ENTRY
#pragma NO_EXIT

void far_modf (void)
{
  asm JMP far_modff;
}

#pragma NO_ENTRY
#pragma NO_EXIT

void far_frexp (void)
{
  asm JMP far_frexpf;
}

/******************************************************************************/

