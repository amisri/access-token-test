/*****************************************************
  FILE        : RTSHC16.c - Runtime support for M68HC16.
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
 *****************************************************/

int errno;           /* implemented here because Modula-2 uses 'Math.c'.  */

/******************************************************************************
                      CALLING PROCEDURE VARIABLES

  For all routines except _CALLS, the procedure variable is in registers. We
  always have to increment the low word by 2 because the RTS will subtract 2!
    The method is quite tricky: we set up a stack layout as follows:

                  RETURN ADDRESS LOW
                  RETURN ADDRESS HIGH
                  Parameter Low + 2
                  Parameter High

    The return address is the runtime routine's return address, i.e. it points
  behind the JSR that brought us here. The parameter is the address of the
  function we want to call.
    When we have set up above stack frame, we execute a RTS. This pops the
  parameter from the stack and instead of returning jumps to the beginning of
  the procedure to be called. When this procedure finally executes its own RTS,
  it'll use the original return address and thereby finally return to the point
  where the function pointer call was.

  Function _CALLE is used in SMALL memory model only (if both X and Y are used
  by parameters). It uses 0 as the high word of the parameter. This implies that
  in SMALL memory model, the code page be page zero.
    This however is also required by the fact that the vector table has only
  16bit entries and also because the compiler doesn't patch the code page in
  a JSR 0,X!!
 ******************************************************************************/

#pragma NO_FRAME

void _CALLYX (void)         /* parameters are passed in Y:X (4:16)  */
{
   asm {
          AIX   #2        ; adjust address
          PSHM  X,Y       ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLED(void)         /* parameters are passed in E:D (4:16)  */
{
   asm {
           ADDD  #2       ; adjust address
           PSHM  D,E      ; push function address
   }                      /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLE (void)
{
  asm {
          ADDE  #2
          PSHM  E
          CLRE            ; zero as code page!!
          PSHM  E
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLXE (void)
{
  asm {
          ADDE  #2
          PSHM  E,X
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLYE (void)
{
  asm {
          ADDE #2
          PSHM E,Y
  }                       /* this return is the procedure call !  */
}

#pragma NO_FRAME

void _CALLS (void)
  /**** Here, the destination address is on the stack,
        above the return address! Therefore, we gotta swap them! */
{
  asm {
          PSHM  D,X,Z     ; Save all registers used!
          TSZ             ; Set up access pointer
          LDD   12,Z      ; Load dest addr (LO)
          LDX   8,Z       ; Load return addr (LO)
          ADDD  #2        ; Correct dest addr
          STD   8,Z       ; Swap the two!
          STX   12,Z
          LDD   10,Z      ; Load dest addr (HI)
          LDX   6,Z       ; Load return addr (HI)
          STD   6,Z       ; Swap the two!
          STX   10,Z
          PULM  D,X,Z     ; Restore all registers!
  }                       /* this return is the procedure call !  */
} /* end _CALLS */

#pragma NO_FRAME

void _CALLSS (void)
{
  asm {
          AIS   #-2       ; Create space for duplication of code page
          PSHM  D,X,K     ; Save all registers used
          TSX             ; Set up access pointer
          LDD   8,X       ; Load code page...
          STD   6,X       ; ... and duplicate
          LDD   12,X      ; Load dest addr...
          ADDD  #2        ; ... correct it...
          STD   8,X       ; ... and set up as our return address
          LDD   10,X      ; Load former return address...
          STD   12,X      ; ... and set up as return address of called function
          LDD   6,X       ; Now set code page for the latter
          STD   10,X
          PULM  D,X,K     ; Restore all registers used!
  }                       /* this return is the procedure call! */
} /* end _CALLSS */

/******************************************************************************
                                BLOCK MOVES

  If we may use only one address register, block moves must be done using
  runtime routines. There are four routines, mirroring all the possible moves
  between I/O space and normal memory. To keep the number of routines that
  small, all pointers (not constant I/O pointers) are considered far pointers.
  If necessary, a near pointer is extended by the default data page.
 ******************************************************************************/

#pragma NO_FRAME

static void _block_move (void)
  /****
    Arguments:
          YK:Y  source address
          XK:X  dest. address
          E     counter, initially block size in bytes - 2
    Precondition:
          E >= 0
   */
{
  asm {
    Loop: LDD   E,Y      ;   Copy words
          STD   E,X
          SUBE  #2       ;   counter -= 2
          BPL   Loop     ; WHILE (counter >= 0)
          PULM  Y,K      ; Restore registers
  } /* end asm */;
} /* end _block_move */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_MEM (void)
  /****
    Arguments:
          Source address : 16 bit I/O address on stack
          Dest. address  : 20 bit far pointer in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TBXK           ; Load page register
          TSY            ; Set up access pointer
          LDY   8,Y      ; Load source address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBYK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _IO_TO_MEM */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_IO (void)
  /****
    Arguments:
          Source address : 20 bit far pointer in B/X
          Dest. address  : 16 bit I/O address on stack
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM  Y,K      ; Save registers
          TXY            ; Move source pointer to Y
          TBYK           ; Load page register
          TSX            ; Set up access pointer
          LDX   8,X      ; Load dest address
          TSKB
          DECB           ; I/O page is defined as the next lower page...
          TBXK           ; Implicitly does the % 16
          JMP   _block_move
  } /* end asm */;
} /* end _MEM_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _IO_TO_IO (void)
  /****
    Arguments:
          Source address : 16 bit I/O address in D
          Dest. address  : 16 bit I/O address in X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K
          XGDY
          TSKB
          DECB
          TBXK
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _IO_TO_IO */

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _MEM_TO_MEM (void)
  /****
    Arguments:
          Source address : 20 bit address on stack
          Dest. address  : 20 bit address in B/X
          Counter        : E
    Precondition:
          E != 0
   */
{
  asm {
          PSHM Y,K
          TBXK
          TSY
          LDAB 9,Y
          LDY  10,Y
          TBYK
          JMP  _block_move
  } /* end asm */;
} /* end _MEM_TO_MEM */

/******************************************************************************
                          MISCELLANEOUS PROCEDURES
 ******************************************************************************/

#pragma NO_FRAME

void _YK (void)
  /**** Used to reset YK after runtime call. */
{
  asm {
           PSHM   D, CCR
           TSKB
           TBYK
           PULM   D, CCR
  };
} /* end _YK */

#pragma NO_FRAME

void _XK (void)
{
  asm {
           PSHM   D, CCR
           TSKB
           TBXK
           PULM   D, CCR
  };
} /* end _XK */

#pragma NO_FRAME

void _IDIV(void)          /* parameters are passed in D / X       */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
           XGDX
   }
}

#pragma NO_FRAME

void _IMOD(void)          /* parameters are passed in D MOD X     */
{                         /* result in D                          */
   asm {
           CLRE
           TSTD
           BGE   L1
           COME
     L1:   EDIVS
   }
}

/******************************************************************************
  32 bit integral arithmetic
 ******************************************************************************/

/*--------------------------------------------------------------- _LMULU ----*
 * UNSIGNED LONG MULTIPLICATION
 *------------------------------
 * Multiplies the unsigned value in registers E:D with the unsigned at the
 * address 0,Y:2,Y. The result is stored back in registers E:D.
 *
 * The general results of this operation is
 *
 *        +-----------+
 *        |  E  * 0,Y |                 High part
 *        +-----+-----+-----+
 *    +         |  E  * 2,Y |           Middle 1 part
 *              +-----------+
 *    +         |  D  * 0,Y |           Middle 2 part
 *              +-----+-----+-----+
 *    +               |  D  * 2,Y |     Low part
 *                    +-----------+
 *
 *                    \_ result __/
 *
 * The result of the multiplication is only le lowest longword of the 64 bits
 * results. In order to have a correct multiplication (without overflow) the
 * highest longword of the result must be 0.
 * The highest longword equal to 0 means that either E = 0 or 0,Y = 0. We can
 * use this fact to make the algorithm faster :
 *
 * if (e == 0) {
 *     return ((D * 0,Y) >> 8) + (D * 2,Y);
 * } else {
 *     return ((E * 2,Y) >> 8) + (D * 2,y);
 * }
 *
 * This algorithm is quite fast because we have only 2 multiplications.
 *
 * WRONG: for ANSI-C we have to use 32 bit even in case of an
 * overflow! So we have the same code as for the signed multiplication.
 * (bh, vc; 1-Mar-93)
 *
 *---------------------------------------------------------------------------*/
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
long _LMULU (void)
{
    int dd, t0;

    asm {
                STD dd     ; D will be changed after the EMUL, and we need
                           ; it later for calculation the low part. So we
                           ; have to store it.

                LDD 2,Y    ; Compute mid1 part
                EMUL       ; E * 2,Y
                STD t0

                LDD dd     ; Compute mid2 part
                LDE 0,Y
                EMUL       ; D * 0,Y
                ADDD t0
                STD t0

                LDD dd     ; Compute low part
                LDE 2,Y
                EMUL       ; D * 2,Y
                ADDE t0
    }
}


/**************************************************************** _LMULS *****
 * SIGNED LONG MULTIPLICATION
 *----------------------------
 * Multiplies the signed value in registers E:D with the signed at the
 * address 0,Y:2,Y. The result is stored back in registers E:D.
 *
 * Here we can't use the fact that either E = 0 or 0,Y = 0, so we have to
 * implement the standard algorithm with 3 multiplictions.
 *
 *---------------------------------------------------------------------------*/

long _LMULS (void)

{
    int dd, t0;

    asm {
                STD dd     ; D will be changed after the EMUL, and we need
                           ; it later for calculation the low part. So we
                           ; have to store it.

                LDD 2,Y    ; Compute mid1 part
                EMUL       ; E * 2,Y
                STD t0

                LDD dd     ; Compute mid2 part
                LDE 0,Y
                EMUL       ; D * 0,Y
                ADDD t0
                STD t0

                LDD dd     ; Compute low part
                LDE 2,Y
                EMUL       ; D * 2,Y
                ADDE t0
    }
}
#pragma MESSAGE DEFAULT C1404

/************************************************************** _LDIVG ******
 * GENERIC LONG DIVISION
 *-----------------------
 * See D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LDIVG ()

{
    unsigned int t0,t1;
    unsigned int ax, ah, al;
    unsigned int bh, bl;
    unsigned int tx, th, tl;
    unsigned int rl;
    unsigned int dd;

    asm {
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1

        Alg0:               ; 0,Y and E are = 0. Then we can use
                            ; the (fast) IDIV instruction.
                LDX 2,Y
                IDIV
                XGDX
                CLRE
                BRA end

        Alg1:               ; only 0,Y is 0, we can use the algorithm
                            ; explained at exercice 16 of chapter 4.3.1 in
                            ; D. KNUTH

                STD t0      ; Store D in t0
                TED
                LDX 2,Y
                IDIV        ; E / 2,Y
                STX t1

                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; rest : D / 2,X
                XGDX        ; result in E:D
                LDE t1
                BRA end

        Alg2:               ; 0,Y is not equal to 0, so we have to use here
                            ; the complete long division.

                STD al      ; Firste store E:D in ah:al
                LDD 0,Y     ; and 0,Y:2,Y in bh:bl
                STD bh
                LDD 2,Y
                STD bl
                STE ah
                CLRW ax
                BMI calcQ   ; if ah:al >= 2^16, no need to normalize

        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register
                BNE Divide
                LDX #1
                BRA Approx
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1

                LDE al      ; Compute ax:ah:al = ah:al * dd
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax

                LDE bl      ; Compute bh:bl = bh:bl * d
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh

        calcQ:  LDX #0xFE   ; First estimation of the quotient
                LDE bh
                CPE ax
                BEQ l1      ; if bl = ax then register X = 0xFE
                LDE ax      ; else compute register X = ax:ah / bh
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl      ; store X in rl (q)

                            ; Check if the result is correct
                LDE bl      ; Compute tx:th:tl = rl * bh:bl
                LDD rl
                EMUL
                STD tl
                STE th

                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx

                LDD al      ; Compute tx:th:tl = ax:ah:al - tx:th:tl
                SUBD tl
                STD tl

                LDD ah
                SBCD th
                STD th

                LDD ax
                SBCD tx
                STD tx
                            ; Here tx:th:tl represents the rest of the
                            ; division (T = A - R * B)

                BMI loop    ; if T < 0 then result is too big, goto loop

                CLRE        ; Else, result is already correct, clear E and
                LDD rl      ; load the result in D

                BRA end

        loop:   DECW rl     ; Result is too big, so decrement it (R = R-1)

                            ; If the result is of 1 smaller, then the rest
                            ; (in T) is of B bigger
                            ; T0 = A - R*B
                            ; T1 = A - (R-1)*B
                            ; T1 = A - R*B + B = T0 + B
                            ; --                 ------

                LDD tl
                ADDD bl
                STD tl

                LDD th
                ADCD bh
                STD th

                LDD tx
                ADCD #0
                STD tx

                BMI loop    ; if T is still < 0, loop again
                            ; this branch is very very unlikely to be taken

                CLRE        ; The Result is now correct, clear E and
                LDD rl      ; load the result in D

        end:
    }
}

/************************************************************** _LMODG ******
 * GENERIC LONG MODULO
 *---------------------
 * See generic long division and D. KNUTH for detailed informations
 *---------------------------------------------------------------------------*/

void _LDIVU(void); /* << ES 12/09/96 */

void _LMODG (void) {
    unsigned int t0;
#if 0 /* << ES 12/09/96 */
    unsigned int ax, ah, al;
    unsigned int bh, bl;
    unsigned int tx, th, tl;
    unsigned int rh, rl;
    unsigned int dd;
#else
    unsigned long tmp;
#endif
    asm {
                TSTW 0,Y    ; is 0,Y = 0 ?
                BNE Alg2    ; if not use complex algorithm Alg2
                TSTE        ; if yes, check E.
                BNE Alg1    ; if E is not = 0, use Alg1

        Alg0:   LDX 2,Y     ; See Generic Long Division
                IDIV
                CLRE        ; The result is already in D, just clear E
                BRA exit

        Alg1:   STD t0      ; See Generic Long Division
                TED
                LDX 2,Y
                IDIV

                TDE
                LDD t0
                LDX 2,Y
                EDIV        ; The result is in D, clear E
                CLRE
                BRA exit
#if 1   /* << ES 12/09/96: somehow, the other way it does not work! */
        /* use a%b == 'return (a-((a/b)*b));' */
        Alg2:
                PSHM D,E       ; spill a
                JSR _LDIVU     ; Y points to b, a/b => result in E:D, Y is NOT destroyed!!!!!
                JSR _LMULU     ; Y points to b, (a/b) is in E:D, result of ((a/b)*a in E:D
                STD tmp
                STE tmp:2
                PULM D,E
                SUBD tmp
                SBCE tmp:2
                /* result in E:D, return */
#else
        Alg2:   STD al
                LDD 0,Y
                STD bh
                LDD 2,Y
                STD bl
                STE ah

                CLR dd      ; dd = 0 means has not been normalized
                CLRW ax
                BMI calcQ

        Norm:   LDD #0xFFFF ; Normalization
                LDX bh
                CPX #0xFFFF ; Avoid overflow into page register
                BNE Divide
                LDX #1
                BRA Approx
        Divide: AIX #1
                IDIV
        Approx: STX dd      ; dd = 0xFFFF / bh+1

                LDE al
                LDD dd
                EMUL
                STD al
                LDD ah
                STE ah
                LDE dd
                EMUL
                ADDD ah
                STD ah
                ADCE #0
                STE ax

                LDE bl
                LDD dd
                EMUL
                STD bl
                LDD bh
                STE bh
                LDE dd
                EMUL
                ADDD bh
                STD bh

        calcQ:  LDX #0xFE
                LDE bh
                CPE ax
                BEQ l1
                LDE ax
                LDD ah
                LDX bh
                EDIV
        l1:     STX rl

                LDE bl
                LDD rl
                EMUL
                STD tl
                STE th

                LDE bh
                LDD rl
                EMUL
                ADDD th
                STD th
                ADCE #0
                STE tx

                LDD al
                SUBD tl
                STD tl

                LDD ah
                SBCD th
                STD th

                LDD ax
                SBCD tx
                STD tx

                BMI loop
                BRA end

        loop:   DECW rl

                LDD tl
                ADDD bl
                STD tl

                LDD th
                ADCD bh
                STD th

                LDD tx
                ADCD #0
                STD tx

                BMI loop

        end:    TST dd      ; Check if the operation has been normalized
                BNE deno    ; if yes, denormalize

                LDE th      ; If the operatinon has not been normalized
                            ; the result is in th:tl
                LDD tl
                BRA exit

        deno:   LDE tx      ; Else, we have to divide tx:th:tl by dd
                LDD th
                LDX dd
                EDIV
                STX rh
                TDE
                LDD tl
                LDX dd
                EDIV
                XGDX
                LDE rh
#endif
        exit:
    }
}


/************************************************************** _LDIVS ******/

void _LDIVS ()

{
    int sign, ySign;

    asm {
                PSHM X

                STE sign

                TSTE        ; test E
                BPL ePos    ; if e > 0 then ok
                COME        ; else negate E:D
                NEGD
                SBCE #-1

        ePos:   CLR ySign   ; Clear ySign
                TST 0,Y     ; test 0,Y
                BPL yPos    ; if 0,Y > 0 then ok
                COM sign    ; Complement sign
                COM ySign   ; Complement sign of y
                XGDX        ; else negate 0,Y:2,Y
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX

        yPos:   JSR _LDIVG

        end:    TST sign
                BPL corrY   ; is sign is +, then keep E:D and check if a
                            ; correction of Y is nessessary.
                COME        ; else negate E:D
                NEGD
                SBCE #-1

        corrY:  TST ySign
                BEQ exit
                XGDX        ; y has been negated, reverse operation.
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX
        exit:   PULM X
    }
}

/************************************************************** _LDIVU ******/


#pragma NO_FRAME

void _LDIVU (void)

{
    asm {
                PSHM X
                JSR _LDIVG
                PULM X
    }
}

/************************************************************** _LMODS ******/

void _LMODS (void)
{
    int sign, ySign;

    asm {
                PSHM X      ; See _LDIVS for comments

                STE sign    ; the difference with _LDIVS is that here the
                            ; sign of the result is the same as the sign
                            ; of the first argument.

                TSTE
                BPL ePos
                COME
                NEGD
                SBCE #-1

        ePos:   CLR ySign
                TST 0,Y
                BPL yPos
                COM ySign
                XGDX
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX

        yPos:   JSR _LMODG

        end:    TST sign
                BPL corrY
                COME
                NEGD
                SBCE #-1

        corrY:  TST ySign
                BEQ exit
                XGDX
                LDD 0,Y
                COMD
                NEGW 2,Y
                SBCD #-1
                STD 0,Y
                XGDX
        exit:   PULM X
    }
}

/************************************************************** _LMODU ******/

#pragma NO_FRAME

void _LMODU (void)
{
    asm {
                PSHM X
                JSR _LMODG
                PULM X
    }
}

/****************************************************************************/

#pragma NO_FRAME

void _LINCPRE (void)
  /**** ++long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                INCW  2,X
                BNE   LoadIt
                INCW  0,X
    LoadIt:     LDD   2,X
                LDE   0,X
  }
}

#pragma NO_FRAME

void _LINCPST (void)
  /**** long++. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD   2,X
                LDE   0,X
                INCW  2,X
                BNE   End
                INCW  0,X
    End:
  }
}

#pragma NO_FRAME

void _LDECPRE (void)
  /**** --long. IN: XK/X = ptr to operand, OUT: E/D = result */
{
  asm {
                LDD  2,X
                BNE  DecLo
                DECW 0,X
    DecLo:      ADDD #-1
                STD  2,X
                LDE  0,X
  }
}

#pragma NO_FRAME

void _LDECPST (void)
{
  asm {
                LDE   0,X
                LDD   2,X
                BNE   DecLo
                DECW  0,X
    DecLo:      DECW  2,X
  }
}

/****************************************************************************/

#pragma NO_FRAME

void _LCMP (void)
  /**** Signed or unsigned comparison. Called in SMALL and MEDIUM memory models
        if one of the operands is far. (Generating the normal inline comparison
        gives troubles with the PUSH/PULL optimization!
          Arguments: E/D    a
                     Stack  b
          Result:    CCR according to a - b
   */
{
  asm {
            TSX
            SUBD    6,X  /* low part */
            SBCE    4,X  /* high part */
            BNE     End
            /* here, E == 0 */
            TSTD
            BEQ     End /* << ES 10/05/95: if low part is zero, all is ok */
            CLRA        /* but if there is the N flag set, we must clear it! */
            INCA        /* here, N=0, Z=0, V=0, C=0 */
    End:
  } /* end asm */;
} /* end _LCMP */

#pragma NO_FRAME

void _PCMP (void)
  /**** Far pointer comparison. Stack: b, B/E: a, CCR set according to a - b. */
{
  asm {
            TSX
            SUBE    6,X
            SBCB    5,X
            BNE     End
            TSTE
    End:
  } /* end asm */;
} /* end _PCMP */

#pragma NO_FRAME

void  _LSHL (void)
  /**** Long <<. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASLD
            ROLE
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHL */

#pragma NO_FRAME

void  _LSHR (void)
  /**** Long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   ASRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHR */

#pragma NO_FRAME

void  _LSHRU (void)
  /**** Unsigned long >>. E/D: value to be shifted, X: shift count. */
{
  asm {
            CPX     #0
            BEQ     End
    Loop:   LSRE
            RORD
            AIX     #-1
            BNE     Loop
    End:
  } /* end asm */;
} /* end _LSHRU */

/******************************************************************************
  CASE PROCESSOR ROUTINES CALLED IMPLICITLY BY THE COMPILER
 ******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_CHECKED (void)
{
   asm {
             TDE
             PSHM   K       ; Save old XK and YK registers
             TSY
             LDX    4,Y     ; load table address
             LDAB   3,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             CPE   -2,X     ; compare with table size bound
             BCC    Default ; too big
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD    2,X     ; load offset
    Return:  ADDD   4,Y     ; add return address
             STD    4,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    3,Y
    ThatsIt: PULM   K       ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
    Default: LDD    0,X
             BRA    Return;
   }
}

/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_DIRECT (void)
{
   asm {
             TDE
             PSHM   K       ; Save old XK,YK registers
             TSY
             LDX    4,Y     ; load table address
             LDAB   3,Y     ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0 ; Disable interrupts
#endif
             TBXK
             ASLE           ; adjust to entry size
             AEX            ; point to entry in table
             LDD   -2,X     ; load offset
    Return:  ADDD   4,Y     ; add return address
             STD    4,Y     ; overwrite return address by destination address
             BCC    ThatsIt
             INC    3,Y
    ThatsIt: PULM   K       ; Restore XK register
             RTI            ; jumps to case label and restores interrupt mask
   }
}


/******************************************************************************/

#pragma NO_FRAME
#pragma NO_EXIT

void _CASE_SEARCH(void)
{
   asm {
             TDE             ; Move value to look for into E
             PSHM   D,K      ; Save old XK register and reserve space for temp
             TSY
             LDX    6,Y      ; load table address
             LDAB   5,Y      ; Set XK register
#ifndef __LARGE__
             ORP    #0x00E0  ; Disable interrupts
#endif
             TBXK
             LDD    -2,X     ; load table size -> D
             XGDE            ; E = table size, D = value to look for
             AIX    #2
    Loop:    ASRE            ; E = (E / 2) & 0xFFFC
             STE    2,Y      ; Save into temp: we will need it below!!
             ANDE   #0xFFFC  ;
             BEQ    Check    ; if last possible entry -> Check
             CPD    E,X      ; is entry found
             BEQ    Found    ; yes, -> Found
             BLS    Loop     ; no and lower -> Loop
             AEX             ; X = X + E + 4
             AIX    #4       ; +4 because this entry already has been tested
             BRSET  3,Y, #0x02, Loop
             SUBE   #4       ; Correct E, but only if we generated
                             ; an odd size sub-table
             BRA    Loop

    Check:   CPD    E,X      ; entry found?
             BNE    Default  ; yes,
    Found:   AEX             ; point to entry in table
             LDD    2,X      ; load offset
    Return:  ADDD   6,Y      ; add return address
             STD    6,Y      ; overwrite return address by destination address
             BCC    ThatsIt
             INC    5,Y
    ThatsIt: PULM   K,D      ; Restore XK and YK registers and remove temp space
             RTI             ; jumps to case label and restores interrupt mask

    Default: LDX    6,Y      ; load table address
             LDD    0,X      ; load default offset
             BRA    Return
   }
}

/******************************************************************************
  IEEE 32 bit arithmetic
 ******************************************************************************/

/**** IEEE 32 bit multiplication. Arguments:

        x   in E:D
        &y  in Y

        result x * y (E:D * @Y) is returned in E:D
 */

void _FMUL (void)
{
    unsigned long par1;
    int           newExp, carry;
    int           result[4];

    asm {

                STE par1:0          ; Save x
                STD par1:2

        ;----- Compute the exponent of the result

                ANDE #0x7F80        ; Extract first exponent
                BEQ zero

                LDD 0,Y             ; Extract second exponent
                ANDD #0x7F80
                BEQ zero

                ADE                 ; Add the 2 exponents
                SUBE #0x3F80        ; Substract Bias

        ;----- Compute the sign of the result

                LDD par1
                EORD 0,Y
                ANDD #0x8000        ; Mask the sign bit

                ADE
                STE newExp          ; newExp is now the exponent of the result

        ;----- Multiply the 2 mantissa and store the result in <result>

                LDE par1:2          ; par1:2 * 2,Y
                LDD 2,Y
                EMUL
                STD result:6
                STE carry

                LDE par1:2          ; par1:2 * 0,Y
                LDD 0,Y
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                STD result:4
                STE result:2

                LDE par1:0          ; par1:0 * 2,Y
                ANDE #0x007F
                ORE  #0x0080
                LDD 2,Y
                EMUL
                ADDD result:4
                ADCE #0
                STD result:4
                STE carry

                LDE par1:0          ; par1:0 * 0,Y
                ANDE #0x007F
                ORE  #0x0080
                LDD 0,Y
                ANDD #0x007F
                ORD  #0x0080
                EMUL
                ADDD carry
                ADCE #0
                ADDD result:2
                ADCE #0
                STD result:2
                STE result:0

        ;----- Normalise the mantissa

                BRSET result:2, #0x80, noDen
                LSLW result:4
               ; LSLW result:2     bug 1177  md 22.5.95
               ; LSLW result:0
                ROLW result:2   ;md 22.5.95
                ROLW result:0   ;md 22.5.95
                LDD result:3
                LDE result:1
                ANDE #0x007F
                ADDE newExp
                BRA exit

        noDen:  LDD result:3
                LDE result:1
                ANDE #0x007F
                ADDE newExp
                ADDE #0x0080
                BRA exit


        zero:   CLRD
                CLRE

        exit:
    }
}

/******************************************************************************
  IEEE 32 bit division. Arguments:

    x   in E:D
    &y  in Y

    result x / y (E:D / @Y) is returned in E:D
 */

void _FDIV (void)
{

    unsigned long par1;
    int           newExp;

    unsigned int  u[5];
    unsigned int  v[2];
    unsigned int  r[3];
    int           q, i, j;
    int           borrow, rest;

    asm {
                STE par1:0
                STD par1:2

        ; Compute the new exponent

                LDD 0,Y             ; load first word of second parameter
                ANDD #0x7F80        ; mask exponent
                BEQ div0            ; if zero, then division by zero error

                                    ; E is first word of first parameter
                ANDE #0x7F80        ; mask exponent
                BEQ zero            ; if zero, then result is zero

                SDE                 ; substract exponents
                ADDE #0x3F80        ; add bias

                LDD par1            ; load first word of first parameter
                EORD 0,Y            ; xor with the 1 st word of the
                                    ; 2 nd param.
                ANDD #0x8000        ; mask the sign bit

                ADE                 ; add the sign to the new exponent
                STE newExp          ; newExp is now the exponent of
                                    ; the result

        ; Compute the normalized fractional part of the first parameter
        ; and store it in u

                CLRW u:8
                CLRW u:6

                LDAA par1:3
                CLRB
                STD u:4

                LDE par1:1
                ORE #0x8000
                STE u:2

                CLRW u:0


        ; Compute the normalized fractional part of the second parameter
        ; and store it in v

                LDAA 3,Y
                CLRB
                STD v:2

                LDE 1,Y
                ORE #0x8000
                STE v:0

        ; Compute the quotient of the mantissa.

                CLRW j              ; counter = 0
                TZY
                AIY @u              ; Y points to u[j = 0]
                BRA loop

        mainLoop:
                TZY
                AIY @u              ; Y points to u[0]
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to u[j]

        ; Calculate the first estimation of q

        loop:   LDE 0,Y             ; E = u[j]
                CPE v:0             ; IF u[j] == v[1] THEN
                BNE l1              ;
                LDX #0xFFFF         ;   X := b-1
                STX q

                LDD 0,Y             ;   rest = u[j] + u[j+1]
                ADDD 2,Y

                BCS substr
                STD rest
                BRA fastCheck

        l1:     LDD 2,Y             ; ELSE  D = u[j+1]
                LDX v:0
                EDIV                ;   X = u[j]:u[j+1] / v[1]

        l2:     STX q
                STD rest            ;   rest

        ; Fast Check to see if q is too big

        fastCheck:
                LDE v:2             ; E = v[2]
                LDD q               ; D = q
                EMUL

                CPE rest
                BHI decq
                BNE substr

                CPD 4,Y
                BLS substr

        decq:   DECW q              ; Decrement q
                LDD rest            ; Adjust rest
                ADDD v:0
                BCS substr
                STD rest
                BRA fastCheck

        ; substract q * v from u

        substr: CLRW borrow         ; Start with no borrow

                TZX
                AIX @v:2            ; X points to v[n]
                TZY
                AIY @u:4
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to u[j+n]

                LDE #1
                STE i               ; i = 1..0

        sl1:    LDE 0,X
                LDD q
                EMUL                ; E:D = q * v[n]

                ADDD borrow
                ADCE #0             ; E:D = q * v[n] + borrow

                STE borrow
                LDE 0,Y             ; E = u[j+n]
                SDE                 ; E = E - D
                STE 0,Y             ; Store new u[j+n]

                BCC noB
                INCW borrow         ; correct borrow if necessary

        noB:    AIX #-2
                AIY #-2
                DECW i
                BPL sl1

                LDE 0,Y
                SUBE borrow
                STE 0,Y

                BCC ok

        ; q was too big, we have to add v back to u

                TZX
                AIX @v              ; X points to v[1]

                TZY
                AIY @u
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to u[j]

                LDD 2,X
                ADDD 2,Y
                STD 2,Y

                LDD 0,X
                ADCD 0,Y
                STD 0,Y

                DECW q

        ok:     TZY
                AIY @r
                XGEY
                LDD j
                ASLD
                ADE
                XGEY                ; Y points to r[j]

                LDE q
                STE 0,Y             ; store q in r[j]

                INCW j
                LDE j
                CPE #2
                BLE mainLoop

        ; denormalize and move the result in par1. if necessary, correct
        ; the exponent of the result.

                TSTW r
                BNE dnl1
                LDE r:1
                ANDE #0x007F
                ADDE newExp
                SUBE #0x0080
                LDD r:3
                BRA end

        dnl1:
                LDE r:1
                LDD r:3
                LSRE
                RORD
                ANDE #0x007F
                ADDE newExp
                BRA end

        div0:   LDX #0             ; Simulate a division by 0
                EDIV

        zero:   CLRE               ; result is zero
                CLRD

        end:

    }
}

/******************************************************************************
  IEEE 32 bit addition. Arguments:

    x   in E:D
    &y  in Y

    result x + y (E:D + @Y) is returned in E:D
 */

void _FADD (void)
{
    unsigned long par1,  par2;
    int           newExp;
    char          sign1, sign2, resSign;

    asm {
                CLR   resSign
                STE   par1:0        ; Save par1
                STD   par1:2
                ANDE  #0x7F80       ; Mask Exponent
                BEQ   uvfl          ; x == 0, return par2 (y)
                LDD   2,Y
                STD   par2:2
                LDD   0,Y
                STD   par2:0
                ANDD  #0x7F80
                BNE   not0          ; y == 0, return par1 (x)
                LDE   par1:0
                LDD   par1:2
                BRA   end
        not0:   SDE                 ; compute exp1 - exp2
                ASLE                ; carry set means D negativ
                TED
                XGAB
                TDE                 ; E = # of bits to shift
                TZX                 ; initialise X and Y pointers
                TZY
                BCC   shP2
        shP1:   AIX   @par1         ; exp1 < exp2 => shift par1
                AIY   @par2         ; and compute par2 + par1
                NEGE
                ANDE  #0x00FF
                BRA   cont
        shP2:   AIX   @par2         ; exp1 > exp2 => shift par2
                AIY   @par1         ; and compute par1 + par2
        cont:   CPE   #24
                BLT   noUVFL
        uvfl:   LDE   0,Y           ; We would shift @X completely away
                LDD   2,Y
                BRA   end
        noUVFL: LDAA  0,X
                STAA  sign2
                LDD   0,Y
                STAA  sign1
                ANDD  #0x7F80
                STD   newExp
                CLR   par1
                BSET  par1:1,#0x80  ; Hidden bit
                CLR   par2
                BSET  par2:1,#0x80  ; Hidden bit

        ; shift the mantissa pointed to by X right by E bits

        chk16:  CPE   #16           ; Try to shift a bloc of 16 bits
                BLT   chk8
        sh16:   LDD   0,X
                STD   2,X
                CLRW  0,X
                SUBE  #16
        chk8:   CPE   #8            ; Try to shift a bloc of 8 bits
                BLT   chk1
        sh8:    LDD   1,X
                STD   2,X
                CLR   1,X
                SUBE  #8
                BRA   chk1
        sh1:    LSR   1,X
                RORW  2,X
        chk1:   SUBE  #1
                BGE   sh1
        madd:   BRCLR sign2,#0x80,xPos
                LDD   #0             ; if @X is negative...
                SUBD  2,X            ; ...negate the mantissa
                STD   2,X
                LDD   #0
                SBCD  0,X
                STD   0,X
        xPos:   BRCLR sign1,#0x80,yPos
                LDD   #0             ; if @Y is negative...
                SUBD  2,Y            ; ...negate the mantissa
                STD   2,Y
                LDD   #0
                SBCD  0,Y
                STD   0,Y
        yPos:   LDD   2,X            ; Add them!
                ADDD  2,Y
                STD   par1:2
                LDD   0,X
                ADCD  0,Y
                STD   par1:0
                BPL   normal
                LDD   #0             ; Resulting mantissa is negative
                SUBD  par1:2
                STD   par1:2
                LDD   #0
                SBCD  par1:0
                STD   par1:0
                COM   resSign        ; Result is negative!!
        normal: LDE   newExp
                BRCLR par1,#0x01,subN
                LSRW  par1:0         ; Overflow: shift right by 1...
                RORW  par1:2
                ADDE  #0x80          ; ... and increment exponent
        subN:   BRSET par1:1, #0x80, pack
                LDD   par1           ; De-normalized: test for zero
                BNE   try1
                LDD   par1:2
                BNE   mov16
                TDE                  ; Result is zero
                BRA   end
        mov16:  LDAA  par1:2
                BNE   mov8
                LDD   par1:2         ; hi 3 bytes are 0
                STD   par1:0
                CLRW  par1:2
                SUBE  #0x800
                BRA   try1
        mov8:   LDD   par1:2         ; only hi 2 bytes are 0
                STD   par1:1
                CLR   par1:3
                SUBE  #0x400
                BRA   try1
        mov1:   LSLW  par1:2         ; Shift until normalized
                ROL   par1:1
                SUBE  #0x80
        try1:   BRCLR par1:1,#0x80,mov1
        pack:   BCLR  par1:1,#0x80
                LDD   par1:2
                ORE   par1
                BRCLR resSign,#0x80, end
                ORE   #0x8000
        end:
    }
}


/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in Y

    result x - y (E:D - @Y) is returned in E:D

    Note: @Y mustn't be changed, therefore y is copied (and the sign bit
          inverted).
 */

void _FSUB (void)
{
    unsigned long par1;

    asm {
                XGDX
                LDD 0,Y
                EORD #0x8000
                STD par1
                LDD 2,Y
                STD par1:2
                TZY
                AIY @par1
                XGDX
                JSR _FADD
    }
}

/******************************************************************************
  IEEE 32 bit subtraction. Arguments:

    x   in E:D
    &y  in Y

    result x - y (E:D - @Y) is returned in E:D

    Note: @Y may be changed, therefore the sign bit is inverted in place.
 */

#pragma NO_EXIT

void _FSUB0 (void)
{
  asm {
                XGDX
                LDD  0,Y
                EORA #0x80
                STD  0,Y
                XGDX
                PULM Z
                JMP  _FADD
  }
}

/******************************************************************************
  Auxiliary routine: right shift (B, E, #0:8) by 31-A bits and return result
  in E:D
 */

#define FLT_BIAS      127

void _frshift (void)
{
  unsigned long l;

  asm {
                NEGA                    ;   A := 31 - A
                ADDA   #31
                CMPA   #24
                BCS    Smaller24        ;   IF (A >= 24) THEN
                STAB   l:3              ;     l := (mantissa >> 24);
                CLRW   l
                CLR    l:2
                SUBA   #24              ;     DEC (A, 24);
                BRA    Smaller8         ;   ELSE
    Smaller24:  STAB   l                ;     l := left adjusted mantissa
                STE    l:1
                CLR    l:3
                CMPA   #16
                BCS    Smaller16        ;     IF (A >= 16) THEN
                LDE    l                ;       l.lo := l.hi
                STE    l:2
                CLRW   l                ;       l.hi := 0;
                SUBA   #16              ;       DEC (A, 16);
                BRA    Smaller8
    Smaller16:  CMPA   #8
                BCS    Smaller8         ;     ELSIF (A >= 8) THEN
                LDAB   l:2              ;       Move l right 8 bits by swapping bytes
                STAB   l:3
                LDE    l
                STE    l:1
                CLR    l
                SUBA   #8               ;       DEC (A, 8);
                                        ;     END;
    Smaller8:   LDE    l                ;   END;
                STAA   l
                LDD    l:2
                TST    l
                BEQ    EndLoop          ;   IF (count > 0) THEN
                                        ;     /* Note: {count < 8} */
    Loop:       LSRE                    ;     REPEAT
                RORD                    ;       result := result >> 1
                DEC    l                ;       DEC (count);
                BNE    Loop             ;     UNTIL (count = 0);
                                        ;   END;
    EndLoop:
  } /* end asm */
} /* end _frshift */

/******************************************************************************
  IEEE-32 to LONG conversion                                   TW, 03/01/93
    E:D    float
    E:D    result
 */

void _FSTRUNC (void)
{
  unsigned char  i;

  asm {
                XGDE                    ; Move hi word to D (easier to unpack exp)
                STAA   i                ; Save sign bit
                LSLD                    ; Exp in A
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
                CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #31              ; ELSIF (exp >= 31) THEN
                BCS    Ok
                LDE    #0x8000          ;   IF (f < 0.0) THEN
                CLRD                    ;     RETURN MIN (LONGINT);
                BRSET  i, #0x80, End    ;   ELSE
                COME                    ;     RETURN MAX (LONGINT);
                COMD                    ;   END;
                BRA    End              ; ELSE
    Ok:         JSR    _frshift
                BRCLR  i, #0x80, End    ;   IF (f < 0.0) THEN
                NEGE                    ;     result := -result;
                NEGD
                SBCE   #0               ;   END;
    End:                                ; END
  } /* end asm */;
} /* end FSTRUNC */

/******************************************************************************
  IEEE-32 to UNSIGNED LONG conversion                          TW, 03/01/93
    E:D    float
    E:D    result
 */

void _FUTRUNC (void)
{
  asm {
                XGDE                    ; Move hi word to D (easier to unpack exp)
                LSLD                    ; Exp in A
                BCS    RetZero          ; IF (f < 0.0) THEN RETURN 0
                LSRB
                ORAB   #0x80            ; Hidden 1 in mantissa: B:E is mantissa now
                SUBA   #FLT_BIAS
                BCC    NonNeg           ; IF (exp < 0) THEN
    RetZero:    CLRD
                CLRE                    ;   RETURN 0
                BRA    End
    NonNeg:     CMPA   #32              ; ELSIF (exp >= 32) THEN
                BCS    Ok
                LDD    #0xFFFF          ;   RETURN MAX (LONGCARD);
                TDE
                BRA    End              ; END
    Ok:         JSR    _frshift
    End:
  } /* end asm */;
} /* end FUTRUNC */


/******************************************************************************
  Auxiliary routine: shift (E, D) until bit #15 of E is 1. Also decrement X
  for each bit shifted. Then, pack it together and return result in D/E!
 */

#define F_EXP_MAX     158

void _fnormant (void)
{
  unsigned long l;

  asm {
                LDX   #F_EXP_MAX
                TSTE
                BMI   Pack
                BNE   Shift
                AIX   #-16              ; Optimize for shift counts > 16
                TDE
                CLRD
                TSTE
                BMI   Pack
    Shift:      AIX   #-1               ; Shift until negative (normalized)
                LSLD
                ROLE
                BPL   Shift
    Pack:       STE   l
                STAA  l:2
                BCLR  l, #0x80          ; Clear hidden bit
                XGDX                    ; A = undef,   B = exp
                XGAB                    ; A = exp,     B = undef
                CLRB                    ; A = exp,     B = 0
                LSRD                    ; A = 0/exp:7, B = exp:1/0:7
                ORAB  l                 ; A = 0/exp:7, B = exp:1/mant:7 ==> D = res.hi
                LDE   l:1               ; E = res.lo
  } /* end asm */;
} /* end _fnormant */

/******************************************************************************
  LONG to IEEE-32                                              TW, 03/01/93
    E:D    long
    E:D    result
 */

void _FSFLOAT (void)
{
  unsigned int  i;

  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    STE   i                 ; Set negative flag
                BPL   Pos
                NEGE                    ; Negate mantissa
                NEGD
                SBCE  #0
    Pos:        JSR   _fnormant
                BRCLR i, #0x80, Swap    ; IF (long < 0) THEN
                ORAA  #0x80             ;   Set sign bit
    Swap:       XGDE                    ; END
    End:
  } /* end asm */;
} /* end _FSFLOAT */


/******************************************************************************
  UNSIGNED LONG to IEEE-32                                     TW, 03/01/93
    E:D    long
    E:D    result
 */

void _FUFLOAT (void)
{
  asm {
                TSTE
                BNE   NotZero
                TSTD
                BEQ   End               ; Zero remains zero...
    NotZero:    JSR   _fnormant
                XGDE
    End:
  } /* end asm */;
} /* end _FUFLOAT */

/******************************************************************************
    E:D   x
    @Y    y

    Set flags according to x-y
 */

void _FCMP (void)
{
  asm {
                    XGDX
                    TED
                    EORD  0, Y
                    BMI   InverseComp     /*  different signs */
    SameSign:       TSTE
                    BPL   Pos
                    /* Both are negative: compare the other way 'round */
                    /* Or numbers have different signs */
    InverseComp:    ;
                    LDD   0, Y
                    XGDE
                    SDE
                    BNE   End
                    XGDX
                    LDE   2, Y
                    SDE
                    BRA   End
                    /* Both are positive */
    Pos:            CPE   0, Y
                    BNE   End
                    CPX   2, Y
    End:
  } /* end asm */;
} /* end _FCMP */

/******************************************************************************/
/* end rtshc16.c */
