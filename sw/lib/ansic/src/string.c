/*****************************************************
    string.c - ANSI-C library: string handling
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#include <hidef.h>
#include <string.h>
#include <libdefs.h>

typedef unsigned char uchar;

/*****************************************************/

#pragma MESSAGE DISABLE  C5703 /* errnum: parameter declared in function strerror but not referenced */

LIBDEF_StringPtr strerror(int errnum) {
  return "";  /* no specific errors implemented */  
}
/*****************************************************/
LIBDEF_MemPtr memchr(LIBDEF_ConstMemPtr buffer, int chr, size_t count)
{
  LIBDEF_ConstStringPtr ptr = buffer;
  
  while (count--) {
    if ( *ptr == (uchar) chr ) 
      return (LIBDEF_MemPtr)(ptr);
    ++ptr;
  }
  return NULL;
}
/*****************************************************/
int memcmp(LIBDEF_ConstMemPtr buf1, LIBDEF_ConstMemPtr buf2, size_t count)
{
  LIBDEF_ConstStringPtr ptr1 = buf1;
  LIBDEF_ConstStringPtr ptr2 = buf2;

  if (!count) return 0;
  while (--count) {
    if (*ptr1 != *ptr2) 
      break;
    ++ptr1; 
    ++ptr2;
  };
  return ( *ptr1 - *ptr2 );
}
/*****************************************************/
void memcpy2(LIBDEF_MemPtr dest, LIBDEF_ConstMemPtr source, size_t count) {
  /* this function does not return the dest and assumes count > 0 */
  do {
    *((LIBDEF_MemBytePtr)dest)++ = *((LIBDEF_ConstMemBytePtr)source)++;
  } while(count--);
}

LIBDEF_MemPtr memcpy(LIBDEF_MemPtr dest, LIBDEF_ConstMemPtr source, size_t count)
{ 
  LIBDEF_MemBytePtr sd = dest;
  LIBDEF_ConstMemBytePtr ss = source;

  while (count--)
     *sd++ = *ss++; 
  return (dest);
}
/*****************************************************/
LIBDEF_MemPtr memmove(LIBDEF_MemPtr dest, LIBDEF_ConstMemPtr source, size_t count)
{ 
  LIBDEF_MemBytePtr sd = dest;
  LIBDEF_ConstMemBytePtr ss = source;

  if ( ss > sd) {  
    while (count--)
      *sd++ = *ss++;
  } else if (ss < sd) {
    sd += count - 1;
    ss += count - 1;
    while (count--)
      *sd-- = *ss--;
  }
  return (dest);
}
/*****************************************************/
void *memset(LIBDEF_MemPtr buffer, int chr, size_t count)
{
  LIBDEF_MemBytePtr ptr = buffer;

  while(count--) 
    *ptr++ = (uchar) chr;
  return (buffer);
}
/*****************************************************/
size_t  strlen(LIBDEF_ConstStringPtr str)
{
  LIBDEF_ConstMemBytePtr s = str;

  while(*str++);
  return (str - s - 1);
}
/*****************************************************/
LIBDEF_StringPtr strset(LIBDEF_StringPtr str, int chr)
{
  LIBDEF_StringPtr s = str;
    
  while (*str)
      *str++ = (char) chr;
  return (s);
}
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*****************************************************/
LIBDEF_StringPtr strcat(LIBDEF_StringPtr str_d, LIBDEF_ConstStringPtr str_s)
{
  LIBDEF_StringPtr sd = str_d;

  str_d += strlen(str_d);
  while(*str_d++ = *str_s++) 
    ;
  return (sd);
}
/*****************************************************/
LIBDEF_StringPtr strncat(LIBDEF_StringPtr str_d, LIBDEF_ConstStringPtr str_s, size_t count)
{
  LIBDEF_StringPtr sd = str_d;

  while (*str_d++)
    ;
  str_d--;
  while (count--) {
    if (!(*str_d++ = *str_s++)) {
      return sd;
    }
  }
  *str_d = '\0';
  return sd;
}
/*****************************************************/
LIBDEF_StringPtr strcpy(LIBDEF_StringPtr str_d, LIBDEF_ConstStringPtr str_s)
{ 
  LIBDEF_StringPtr sd = str_d;

  while(*str_d++ = *str_s++) 
    ; 
  return (sd);
}
/*****************************************************/
LIBDEF_StringPtr strncpy(LIBDEF_StringPtr str_d, LIBDEF_ConstStringPtr str_s, size_t count)
{ 
  LIBDEF_StringPtr sd = str_d;

  while(count--) 
    {
      if(*str_s) 
        *str_d++ = *str_s++; 
      else 
        *str_d++ = '\0';
    }
  return (sd);
}
/*****************************************************/
int strcmp(LIBDEF_ConstStringPtr str1, LIBDEF_ConstStringPtr str2)
{
  while ( *str1 && *str2 )
    {
      if (*str1 != *str2) 
        break;
      ++str1; 
      ++str2;
    }
  return (*str1 - *str2);
}
/*****************************************************/
int strncmp(LIBDEF_ConstStringPtr str1, LIBDEF_ConstStringPtr str2, size_t count)
{
  if (!count) return 0;
  while( --count && *str1 && *str2 )
    {
      if (*str1 != *str2) 
        break; 
      ++str1; 
      ++str2;
    }
  return (*str1 - *str2);
}
/*****************************************************/
LIBDEF_StringPtr strchr(LIBDEF_ConstStringPtr str, int chr)
{
  while (*str)
    {
      if(*str == chr) 
        return (str);
      ++str;
    }
  if(*str == chr) return str;
  return (NULL);
}
/*****************************************************/
LIBDEF_StringPtr strrchr(LIBDEF_ConstStringPtr str, int chr)
{
  LIBDEF_ConstStringPtr sr = NULL;
  
  while (*str)
    {
      if (*str == chr)
        sr = str;
      ++str;
    }
  if(*str == chr) return str;
  return (sr);
}
/*****************************************************/
size_t strspn(LIBDEF_ConstStringPtr str1, LIBDEF_ConstStringPtr str2)
{
  size_t len = 0;

  while(*str1)
    {
      if (strchr(str2, *str1) == NULL)
        return (len);
      ++str1; 
      ++len;
    }
  return (len);
}
/*****************************************************/
size_t strcspn(LIBDEF_ConstStringPtr str1, LIBDEF_ConstStringPtr str2)
{
  register LIBDEF_ConstStringPtr b2;
  size_t len = 0;

  while( *str1)
    {
      b2 = str2;
      while(*b2)
        {
          if (*str1 == *b2++) 
            return (len);
        }
      ++str1; 
      ++len;
    }
  return (len);
}
/*****************************************************/
LIBDEF_StringPtr strpbrk(LIBDEF_ConstStringPtr str1, LIBDEF_ConstStringPtr str2)
{
  register LIBDEF_ConstStringPtr b2;

  while (*str1)
    {
      b2 = str2;
      while(*b2) 
        {
          if(*str1 == *b2++) 
            return (str1);
        }
      ++str1;
    }
  return (NULL);
}
/*****************************************************/
LIBDEF_StringPtr strstr(LIBDEF_ConstStringPtr str1, LIBDEF_ConstStringPtr str2)
{
  register int    count;
  register size_t len;
  
  if ( (len = strlen(str2)) == 0)
    return (str1);
  if ((count = (strlen(str1) + 1) - len) < 1)
    return (NULL);
  while (count--)
    {
      if (*str1 == *str2)
        {
          if (strncmp(str1, str2, len) == 0)
          return (str1);
        }
      ++str1;
    }
  return (NULL);
}
#pragma MESSAGE DEFAULT C5909
/*****************************************************/
LIBDEF_StringPtr strtok(LIBDEF_StringPtr str1, LIBDEF_ConstStringPtr str2)
{
  static LIBDEF_StringPtr next = NULL;
  register LIBDEF_ConstStringPtr s2;
  
  if (str1 != NULL)
    next = str1;                /* start new sequence */
  else
    str1 = next;                /* set *s to start of token */
  
  while (*str1 )
    {
      s2 = str2;
      while (*s2 )
        {
          if (*str1 == *s2++)
            {
              /* found */
              if (str1 == next)
                {
                  next++;         /* remove delimiters in front of token */
                  break;
                }
              *str1  = '\0';      /* change delimiter to null */
              s2   = next;        /* save token start         */
              next = ++str1;      /* set start of next token  */
              return (s2);
            }
        }
      str1++; /* test next character        */
    }
  /* end of string found */
  if (*next)
    {
      s2   = next; /* save token start         */
      next = str1; /* set start of next token  */
      return (s2);
    }
  return (NULL);   /* if end_of_string, return NULL */
}
/*****************************************************/
int strcoll(LIBDEF_ConstStringPtr string1, LIBDEF_ConstStringPtr string2) {
  return strcmp(string1, string2);
}
/*****************************************************/
size_t strxfrm(LIBDEF_StringPtr strDest, LIBDEF_ConstStringPtr strSource, size_t count) {
  (void)strncpy(strDest, strSource, count);
  return(strlen(strSource));
}
/*****************************************************/
/* end string.c */
