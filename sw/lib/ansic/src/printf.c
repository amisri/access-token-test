/*----------------------------------------------------------------------------*\
 File:		printf.c

 Purpose:	Provide buffered IO

 Author:	Hiware & Quentin.King@cern.ch

 Notes:		This file started life as part of the Hiware ANSI library,
		but it has been extensively hacked by QAK to make it work
		for the FGC project.  The main difference was to force all
		floats to 32-bit and to add far string support using %s.

 History:

   10/06/99     qak     Changed #define out(c) to call desc->outc directly
   15/06/99	qak	Complete tidy up
   11/05/01	qak	Converted to single precision floating point only
   11/05/01	qak	Floats are clipped at 1.0E+/-37
   25/07/08	qak	Format strings are now far
\*----------------------------------------------------------------------------*/

#include <stdio.h>
#include <math.h>

/* Suppress compiler warnings */

#pragma MESSAGE DISABLE C5919
#pragma MESSAGE DISABLE C4001
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
#pragma MESSAGE DISABLE C2402
#pragma MESSAGE DISABLE C2706

/* Constants & Macros */

#define out(c)		{file->cb((c),file);n_ch++;}
#define abs(v)  	((v)<0?-(v):(v))
#define LEN(array)	(sizeof(array)/sizeof(array[0]))
#define F_DIGITS	(LEN(pow10)-3)
#define FLOAT_MAX	1.0E+37
#define FLOAT_MIN	1.0E-37

#define dig		c
#define farstr		_local._farstr
#define val		_local._num._val
#define pwr		_local._num._pow
#define fval		_local._num._flt
#define expon		_local._num._exp
#define exp0		_local._int

#define HEXINT		0x2
#define NOT_DEC		(HEXINT | OCTINT)
#define ETYPE		0x1
#define FTYPE		0x2
#define GTYPE		(ETYPE | FTYPE)
#define BLANK		0x4
#define SIGNED		0x8
#define NEG		(SIGNED | BLANK)

#define UPPER     	0x10
#define LEFT      	0x20
#define ZEROPAD   	0x40
#define ALT       	0x80
#define LONGVAL  	0x100
#define SHORTVAL 	0x200
#define PREC     	0x400

/*----------------------------------------------------------------------------*/
static float Ten(int e)
/*----------------------------------------------------------------------------*/
{
    static float fpow10[]  = {1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e16, 1e24, 1e32};
    static float fpow_10[] = {1e0, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-16, 1e-24, 1e-32};
    unsigned int	i;
    float *		arr = fpow10;

    if (e < 0)
    {
        arr = fpow_10;
        e   = -e;
    }

    if (e <= 8)
    {
        return(arr[e]);
    }
    else
    {
        i = e >> 3;

        if (i > 4)
        {
            i = 4;
        }

        e  -= i << 3;

        return(arr[i+7]*arr[e]);
    }
}
#pragma MESSAGE DISABLE C4000
#pragma MESSAGE DISABLE C5910
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*----------------------------------------------------------------------------*/
int vfprintf(FILE *file, char * far format, va_list args)
/*----------------------------------------------------------------------------*/
{
    static unsigned long pow10[] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,0};
    static unsigned long pow16[] = {1,0x10,0x100,0x1000,0x10000,0x100000,0x1000000,0x10000000,0x0};
    int width;
    int n_ch = 0;
    char c;
    unsigned int flags;
    unsigned short prec, len;
    static   char nullmsg[] = "<NULL>";

    union
    {
	char *far _farstr;
        int _int;
        struct
        {
            unsigned long _val;
            unsigned long _pow;
            float _flt;
            int _exp;
        } _num;
    } _local;

    /* Do nothing if FILE *file is NULL */

    if(!file)
    {
        return(0);
    }

    /* Main Loop: Work through each character of format string */

    while (c=*(format++))
    {
	if (c != '%') /* Non format character */
	{
	    out(c);
	    continue;
	}

	/* Scan for leading flags */

	flags = 0;
	prec  = 0;
	width = 0;

	for(;;)
	{
	    switch(c=*(format++))
	    {
	    case '-':
		flags |= LEFT;
		continue;
	    case '+':
		flags |= SIGNED;
		continue;
	    case ' ':
		flags |= BLANK;
		continue;
	    case '0':
		flags |= ZEROPAD;
		continue;
	    case '#':
		flags |= ALT;
		continue;
	    }
	    break;
	}

        /* Apply flag exclusions */

        if(flags & SIGNED)
        {
            flags &= ~BLANK;
        }

        if(flags & LEFT)
        {
            flags &= ~ZEROPAD;
        }

        /* Determine field width */

        if(c == '*')
        {
            width = va_arg(args,int);
            c = *(format++);
        }
        else
        {
            while(c >= '0' && c <= '9')
            {
                width = 10 * width + (c - '0');
		c = *(format++);
            }
        }

        /* Determine precision */

        if(c == '.')
        {
            flags |= PREC;
	    c = *(format++);

            if (c == '*')
            {
                prec = va_arg(args,unsigned int);
		c = *(format++);
            }
            else
            {
                while(c >= '0' && c <= '9')
                {
                    prec = 10 * prec + (c - '0');
		    c = *(format++);
                }
            }
        }

        /* long/short argument */

        if(c == 'l')
        {
            flags |= LONGVAL;
	    c = *(format++);
        }
        else
        {
            if(c == 'h')
            {
		c = *(format++);
            }
        }

        /* Main Switch for all Conversions types */

        switch (c)
        {
        case 'c':				/* char */
            c = va_arg(args, int);
            farstr = (char *far)&c;
            len = 1;
            goto string;

        case 's':				/* far string */

            farstr = va_arg(args, char * far);

	    farstring:

            len = 0;

            if(farstr)
            {
                while(farstr[len]) { ++len; }
            }
	    else
	    {
	        farstr = (char *far)&nullmsg;
		len    = 6;
	    }

	    string:

            /* adjust length to precision */

	    if(prec && (prec < len))
            {
                len = prec;
            }

	    if(width > len)
            {
                width -=len;
            }
	    else
            {
                width = 0;
            }

	    /* pad left */

	    if(!(flags & LEFT))
            {
                while(width)
                {
                    out(' ');
		    width--;
                }
            }

            /* output string */

            while(len--)
            {
                out(*(farstr++));
            }

            /* pad right */

            while(width--)
            {
                out(' ');
            }
            continue;

        case 'X': 				/* Hex unsigned int (upper case) */
            flags |= UPPER;

        case 'x':				/* Hex unsigned int (lower case) */
            flags |= HEXINT;

	case 'u':				/* Decimal unsigned int */
	    flags &= ~NEG;

            if(flags & LONGVAL)
            {
                val = va_arg(args,unsigned long);
            }
            else
            {
                val = va_arg(args,unsigned int);
            }
            goto integer;

        case 'i':				/* Decimal signed int */
        case 'd':
	    if (flags & LONGVAL)
            {
                val = va_arg(args, long);
            }
            else
            {
                val = va_arg(args, int);
	    }

            if((long)val < 0)
            {
                val = (-(long)val);
                flags |= NEG;
            }

            integer:

	    /* Determine length of integer */

            len = 1;

	    if (flags & HEXINT)
            {
	        unsigned long pw;

                while((pw = pow16[len]) && val >= pw) { ++len; }
            }
	    else
            {
	        unsigned long pw;

		while((pw = pow10[len]) && val >= pw) { ++len; }
            }

            /* Width of integer */

	    if (prec > len)
            {
                prec -= len;
            }
	    else
            {
                prec = 0;
            }

            width -= (prec + len);

            if ((flags & (ALT|HEXINT))==(ALT|HEXINT))
            {
                width -= 2;
            }

	    if (flags & NEG)
            {
                --width;
            }

            if (width < 0)
            {
                width = 0;
            }

            /* pad left */

            if (width && !(flags & LEFT))
            {
                if (!(flags & ZEROPAD)) /* pad with spaces */
		{
                    do
                    {
                        out(' ');
			--width;
                    } while (width);
                }
                else /* pad with zeros */
                {
                    prec += width;
		    width = 0;
                }
            }

            /* alternate form of integers for hex numbers */

	    if ((flags & (ALT|HEXINT)) == (ALT|HEXINT))
            {
                out('0');

                if (flags & UPPER)
                {
                    out('X');
                }
		else
                {
                    out('x');
                }
            }

            /* sign */

	    if (flags & SIGNED)
            {
                if (flags & BLANK)
                {
                    out('-');
                }
		else
                {
                    out('+');
                }
            }
	    else
	    {
	        if (flags & BLANK)
                {
                    out(' ');
                }
	    }

            /* pad with zeros to precision */

	    while (prec)
            {
                out('0');
		--prec;
            }

            /* output conversion. par: val, len; local: dig, pwr */

	    do
            {
                dig = 0;
                --len;

		if (flags & HEXINT)
                {
                    pwr = pow16[len];
                }
		else
                {
                    pwr = pow10[len];
                }

                while (val >= pwr)
                {
                    val -= pwr;
                    ++dig;
                }

		if (dig < 10)
                {
                    out(dig + '0');
                }
		else
		{
		    if (flags & UPPER)
                    {
                        out(dig + ('A' - 10));
                    }
		    else
                    {
                        out(dig + ('a' - 10));
                    }
		}
            } while (len);

            /* append spaces to field width */

            while (width)
            {
                out(' ');
		--width;
            }
            continue;

        case 'E':				/* float sX.XXXEsYY (upper case E) */
            flags |= UPPER;

	case 'e':				/* float sX.XXXesYY (lower case e) */
            flags |= ETYPE;
            goto real;

        case 'f':				/* float sX.XXXXX */
            flags |= FTYPE;
            goto real;

        case 'G':				/* float %G (upper case E) */
            flags |= UPPER;

        case 'g':				/* float %g (lower case e) */
            flags |= GTYPE;

            real:

            if (!(flags & PREC))
            {
                prec = 6;       /* default precision */
            }

            fval = va_arg(args, float);

            if (fval < 0.0)
            {
                fval = (-fval);
                flags |= NEG;
            }

            expon = 0;

	    if(fval < FLOAT_MIN)	// Clip at min limit
	    {
		fval = 0.0;
	    }
	    else if(fval > FLOAT_MAX)	// Clip at max limit
	    {
		fval = FLOAT_MAX;
	    }

            if (fval != 0.0)
            {
                (void)frexpf(fval, &exp0);

		expon = (exp0 * 3) / 10;    /*ln(2)/ln(10) = 0.30102999..*/
            }

            if (flags & ETYPE && (!(flags & FTYPE) || (expon < -4) || (expon >= (int) prec)))
            {
                /* base conversion */

                if (fval != 0.0)
                {
                    fval *= Ten(-expon);

                    if (fval != 0.0)
                    {
                        while (fval >= 10.0)
                        {
                            fval *= 1e-1;
			    ++expon;
                        }
                        while (fval < 1.0)
                        {
                            fval *= 10.0;
			    --expon;
                        }
                    }
                }

                /* x = fval * 10 ^ expon; 1 <= fval < 10 */

                if (prec && flags & FTYPE) /* for g type: prec = nof significant digits */
		{
                    --prec;
                }

                if (prec < F_DIGITS)
                {
                    fval += 0.5 * Ten(-(int)prec);
                }

                /* if rounding causes overflow */

		if (fval >= 10.0)
                {
                    fval *= 1e-1;
		    ++expon;
                }

                /*adjust precision for g type conversion (remove trailing zeros)*/

		if(prec && flags & FTYPE && !(flags & ALT))
                {
                    if (prec > F_DIGITS)
                    {
                        prec = F_DIGITS;
                    }

                    val = fval * Ten(prec);

                    if (val)
                    {
                        while (prec && !(val % 10))
                        {
                            val /= 10;
			    --prec;
                        }
                    }
		    else
                    {
                        prec = 0;
                    }
                }
                flags &= ~FTYPE;

                /* width of e type conversion */

		width -= (5 + prec); /*minimum: dE+dd*/

		if(prec || flags & ALT) /*decimal point*/
		{
		    --width;
                }
                if(flags & NEG) /*sign or blank*/
		{
		    --width;
                }

                val   = fval;
                fval -= (float) val;
                len   = 1;
            }
	    else /* f type conversion */
	    {
                /*float = fval * 10 ^ expon; fval is not restricted to 1 <= fval < 10 !!!*/

                /*adjust precision for g type conversion (trailing zeros)*/

		if (prec && flags & ETYPE && !(flags & ALT))
                {
                    prec -= (expon+1); /*prec >= expon*/

                    if (expon < 0 || fval < 1.0) /*leading 0 is not significant*/
		    {
		        ++prec;
                    }

                    if (prec > F_DIGITS)
                    {
                        prec = F_DIGITS;
                    }

                    val = fval * Ten(prec) + 0.5;

                    while (prec && !(val % 10))
                    {
                        val /= 10;
			--prec;
                    }
                }

                flags &= ~ETYPE;

                if ((int)(expon - F_DIGITS) >= -(int)prec)
                {
                    fval += 0.5 * Ten(expon - F_DIGITS);
                }
		else
                {
                    fval += 0.5 * Ten(-(int)prec);
                }

                /* width of type f conversion */

                if (expon >= 0) /* (expon + 1) digits before dec pt */
		{
		    if (expon > F_DIGITS)
                    {
                        expon -= F_DIGITS;
                        val    = fval * Ten(-expon);
                        fval   = 0.0;
                    }
		    else
                    {
                        val   = fval;
                        fval -= (float) val;
                        expon = 0;
                    }
                }
		else
                {
                    val   = 0;
                    expon = 0;
                }

		{
		    unsigned long pw;

                    len = 1;
                    while ((pw = pow10[len]) && val >= pw) { ++len; }
                }

                width -= (prec + len + expon);

                if (prec || flags & ALT) /*decimal point*/
		{
		    --width;
                }

                if (flags & NEG) /*sign or blank*/
		{
                    --width;
                }

            }

            if (width < 0)
	    {
	        width = 0;
	    }

            /* pad left */

            if (!(flags & LEFT))
            {
                if (!(flags & ZEROPAD)) /* pad with spaces before sign */
		{
                    while (width)
                    {
                        out(' ');
			--width;
                    }
                }
            }

            /* !(flags & LEFT) && !(flags & ZEROPAD) => (width == 0) */

            /* sign */

            if (flags & SIGNED)
            {
                if (flags & BLANK)
                {
                    out('-');
                } else
                {
                    out('+');
                }
            }
	    else
	    {
	        if (flags & BLANK)
                {
                    out(' ');
                }
	    }

            if (!(flags & LEFT)) /*pad with zeros after sign (if width still > 0)*/
	    {
                while (width)
                {
                    out('0');
		    --width;
                }
            }

            /* float conversion */

            /* print all digits before decimal point */

            do
            {
                --len;
                dig = '0';
                pwr = pow10[len];

                while (val >= pwr)
                {
                    val -= pwr; ++dig;
                }

                out(dig);

            } while (len);

            if (flags & FTYPE) /* add zeros before decimal point */
	    {
                while (expon > 0)
                {
                    out('0'); --expon;
                }
            }

            if (prec || flags & ALT)
            {
                out('.');

		if(prec > 0)
                {
                    if (prec > F_DIGITS)
                    {
                        len = F_DIGITS;
                    }
		    else
                    {
                        len = prec;
                    }

                    prec -= len;

                    if (flags & FTYPE)
                    {
                        val = fval * Ten(len - expon); /*10^(-expon) --> 1 <= fval < 10 */
                    }
		    else
                    {
                        val = fval * Ten(len);
                    }

                    /*out int val */

                    do
                    {
                        --len;
                        dig = '0';
                        pwr = pow10[len];

                        while (val >= pwr)
                        {
                            val -= pwr; ++dig;
                        }
                        out(dig);

                    } while (len);

                    while (prec)
                    {
                        out('0');
			--prec;
                    }
                } /* if prec > 0 */
            }

            if (flags & ETYPE) /* exponent */
	    {
                if (flags & UPPER)
                {
                    out('E');
                }
		else
                {
                    out('e');
                }

                if (expon < 0)
                {
                    expon = (-expon);
                    out('-');
                }
		else
                {
                    out('+');
                }

                out(expon / 10 + '0');
                out(expon % 10 + '0');
            }

            /* pad right */

            while (width)
            {
                out(' ');
		--width;
            }
            continue;

	case '%':				/* Percent */
            out('%');
            continue;

        default: 				/* Unknown format */
            continue;

        } /* switch (*format++) */
    } /* while (*format) */

    /* Return number of characters output */

    return(n_ch);
}
/*----------------------------------------------------------------------------*/
int fputs(char * far s, FILE *file)
/*----------------------------------------------------------------------------*/
{
    int  n_ch = 0;
    char ch;

    if(file)
    {
	while((ch = *(s++)))
	{
	    out(ch);
	}
    }

    return(n_ch);
}
#pragma MESSAGE DEFAULT C4000
#pragma MESSAGE DEFAULT C5910
#pragma MESSAGE DEFAULT C5909
/*----------------------------------------------------------------------------*/
int fprintf(FILE *f, char * far format, ...)
/*----------------------------------------------------------------------------*/
{
    int n_ch = 0;
    va_list args;

    /* Call vfprintf() using local stream */

    if(f)
    {
	va_start(args, format);
	n_ch = vfprintf(f,format,args);
	va_end(args);
    }
    return(n_ch);
}
/*----------------------------------------------------------------------------*/
static void _outc( char c, FILE *f)
/*----------------------------------------------------------------------------*/
{
    f->_p[f->_offset++] = c;
}
/*----------------------------------------------------------------------------*/
int sprintf(char *s, char * far format, ...)
/*----------------------------------------------------------------------------*/
{
    FILE f;
    va_list args;

    /* Configure local stream structure */

    f._offset = 0;
    f._p      = s;
    f.cb      = _outc;

    /* Call vfprintf() using local stream */

    va_start(args, format);
    s[vfprintf(&f,format,args)] = '\0';
    va_end(args);

    return(f._offset);
}
/*----------------------------------------------------------------------------*/
int vsprintf(char *s, char * far format, va_list args)
/*----------------------------------------------------------------------------*/
{
    FILE f;

    /* Configure local stream structure */

    f._offset = 0;
    f._p      = s;
    f.cb      = _outc;

    /* Call vfprintf() using local stream */

    s[vfprintf(&f,format,args)] = '\0';

    return(f._offset);
}

/* End of file: printf.c */
