/*****************************************************
     scanf.c - ANSI-C library: sscanf, vsscanf
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>
#include <hidef.h>
#include <libdefs.h>

#ifdef __HC12__
  #pragma MESSAGE DISABLE C12002 /* WARNING C12002: Use DPAGE, PPAGE, EPAGE to specify the page register. Assumed DPAGE */
#endif

#pragma MESSAGE DISABLE C4001

#undef isspace
#undef toupper

/*****************************************************/
static int _getNum(LIBDEF_StringPtr *format)
{
   int n = 0, ch = **format;
   while (ch >= '0' && ch <= '9')
   {
      n = 10*n + (ch - '0');
      ch = *(++(*format));
   }
   return n;
} /* end _getNum */

/*****************************************************/

#if LIBDEF_MULTI_CHAR_SELECTION
#if LIBDEF_PRINTF_FAR_PTR
static char *_copyFarList(char *FAR far_ptr, char *s, char **nsp, char *format, int *tasg, int fwi, int ma)
{
   static char mtchbuf[LIBDEF_SCANF_BUF_SIZE]; char *m;
   int found, sz = 0;

   *tasg = 1;
   if(*(++format) == '^')   /* Set not eql. flag */
   {
      sz = 1; format++;
   }
   m = mtchbuf;
   if (*format == ']')
   {
      *m++ = ']'; format++;                   /* Special case */
   }
   while (*format && (*format != ']'))        /* Set mtchbuf */
   {
      *m++ = *format++;
   }
   *m = '\0';                                 /* Terminate array */

   while (*s && fwi--)
   {
      m = mtchbuf;                            /* Reset m to top */
      while (*m && *s != *m) m++;

      found = (*m != 0);
      if (sz == found) { *tasg = 0; break; }  /* failed */
      if (ma) *far_ptr++ = *s;                     /* Passed test, next s */
      s++;
   }
   if (ma) *far_ptr = '\0';                        /* Terminate array */

   *nsp = s;
   return format;
} /* end _copyFarList */
#endif

static char *_copyList(char *ss, LIBDEF_StringPtr s, char **nsp, LIBDEF_StringPtr format, int *tasg, int fwi, int ma)
{
   static char mtchbuf[LIBDEF_SCANF_BUF_SIZE]; char *m;
   int found, sz = 0;

   *tasg = 1;
   if(*(++format) == '^')   /* Set not eql. flag */
   {
      sz = 1; format++;
   }
   m = mtchbuf;
   if (*format == ']')
   {
      *m++ = ']'; format++;                   /* Special case */
   }
   while (*format && (*format != ']'))        /* Set mtchbuf */
   {
      *m++ = *format++;
   }
   *m = '\0';                                 /* Terminate array */

   while (*s && fwi--)
   {
      m = mtchbuf;                            /* Reset m to top */
      while (*m && *s != *m) m++;

      found = (*m != 0);
      if (sz == found) { *tasg = 0; break; }  /* failed */
      if (ma) *ss++ = *s;                     /* Passed test, next s */
      s++;
   }
   if (ma) *ss = '\0';                        /* Terminate array */

   *nsp = s;
   return format;
} /* end _copyList */
#endif /* LIBDEF_MULTI_CHAR_SELECTION */

/*****************************************************/
/* only internaly used functions defined in stdlib.c */
double _strtod (LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, unsigned int len);
long int _strtol(LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, int base, unsigned int len);
unsigned long int _strtoul(LIBDEF_ConstStringPtr nptr, LIBDEF_StringPtr *endptr, int base, unsigned int len);

/*****************************************************/

int vsscanf(LIBDEF_ConstStringPtr ps, LIBDEF_ConstStringPtr format, va_list args)
{
   LIBDEF_ConstStringPtr s=ps;
   char sz, ma, base;           /* Size and no-assign flags  */
   LIBDEF_ConstStringPtr nsp;             /* New s position            */
   char *ss= NULL;
   int ok;
   int fwi;                     /* Field width size          */
   LIBDEF_ConstStringPtr sbase;           /* base addr of string s     */
   int tasg = 0;                /* Total assignments made    */

#if LIBDEF_PRINTF_FAR_PTR
   Bool is_far_ptr = FALSE;     /* Is it a FAR pointer */
   char *FAR far_ptr = NULL;    /* FAR pointer */
#endif

   sbase = s;

   while (*format)
   {
      sz = fwi = base = 0;
      ma = 1;
      if (isspace(*format))   /* White space flag */
      {
         while (isspace(*format)) ++format;    /* Eat white space */
         if (isspace(*s))
         {
            while(isspace(*s))
            {
               ++s;
            }   /* Is white space and was eaten */
         }
         else if (*s) goto error; /* Is no white space, is error */
      }
      if (*format !='%') /* Read input string */
      {
         if (*s)
         {
            if (*s++ == *format++)
               ; /* Character match */
            else
               goto error;
         }
         else
            goto endscan;
      }
      else     /* Conversion specification */
      {
         format++; /* Drop %  */
         if (*format == '*')
         {  /* Assignment suppressed */
            ma = 0;
            format++;
#if LIBDEF_PRINTF_FAR_PTR
         } else if (*format == '+') {
            is_far_ptr = TRUE;
            format++;
#endif
         }

         fwi = _getNum(&format);  /* Get max field width if any */

         switch (*format++)
         {
            case 'h': sz = 1; break;
            case 'l': sz = 2; break;
            case 'L': sz = 3; break;
            default: format--;
         }

         switch(*format)
         {
            case 'd': base = 10;
            case 'i': {
                      long l = _strtol(s, &nsp, base, fwi);
                      if (ma)
                      {
#if LIBDEF_PRINTF_FAR_PTR
                         if (is_far_ptr)
                         {
                            switch(sz)
                            {
                               case 1:
                                  *va_arg(args, short int *FAR) = l;
                                  break;
                               case 2:
                               case 3:
                                  *va_arg(args, long int *FAR) = l;
                                  break;
                               default:
                                  *va_arg(args, int *FAR) = l;
                            }
                         } else {
#endif
                            switch(sz)
                            {
                               case 1:
                                  *va_arg(args, short int*) = l;
                                  break;
                               case 2:
                               case 3:
                                  *va_arg(args, long int*) = l;
                                  break;
                               default:
                                  *va_arg(args, int*) = l;
                            }
#if LIBDEF_PRINTF_FAR_PTR
                         }
#endif
                      }
                      s = nsp; tasg++;
                      break;
                      }
            case 'o': base = 8;  goto xx;
#if LIBDEF_PRINTF_FAR_PTR
            case 'P': sz = 5;
                      base = 16; goto xx;
#endif
            case 'p': sz = 4;
            case 'X':
            case 'x': base = 16; goto xx;
            case 'u': {
                      unsigned long l;
                      base = 10;
xx:
                      if (*s == '-') {
                        ++s;  /* Strip sign if any */
                        fwi--;
                      }
                      l = _strtol(s, &nsp, base, fwi);
                      if (ma)
                      {
#if LIBDEF_PRINTF_FAR_PTR
                         if (is_far_ptr)
                         {
                            switch(sz)
                            {
                               case 1:
                                  *va_arg(args, unsigned short int *FAR) = l;
                                  break;
                               case 2:
                               case 3:
                                  *va_arg(args, unsigned long int *FAR) = l;
                                  break;
                               case 4:
                                  *va_arg(args, char * *FAR) = (char *) l;
                                  break;
                               case 5:
                                  *va_arg(args, char *FAR *FAR) = (char *FAR) l;
                                  break;
                               default:
                                  *va_arg(args, unsigned int *FAR) = l;
                            }
                         } else {
#endif
                            switch(sz)
                            {
                               case 1:
                                  *va_arg(args, unsigned short int*) = l;
                                  break;
                               case 2:
                               case 3:
                                  *va_arg(args, unsigned long int*) = l;
                                  break;
                               case 4:
                                  *va_arg(args, char**) = (char*)l;
                                  break;
#if LIBDEF_PRINTF_FAR_PTR
                               case 5:
                                  *va_arg(args, char *FAR *) = (char *FAR) l;
                                  break;
#endif
                               default:
                                  *va_arg(args, unsigned int*) = l;
                            }
#if LIBDEF_PRINTF_FAR_PTR
                         }
#endif
                      }
                      s = nsp; tasg++;

                      }
                      break;
#if LIBDEF_PRINTF_FLOATING
            case 'e':
            case 'f':
            case 'g': {
                      double d = _strtod(s, &nsp, fwi);
                      if (ma)
                      {
#if LIBDEF_PRINTF_FAR_PTR
                         if (is_far_ptr)
                         {
                            if (sz >= 2)
                               *va_arg(args, double *FAR) = d;
                            else
                               *va_arg(args, float *FAR) = d;
                         } else {
#endif
                            if (sz >= 2)
                               *va_arg(args, double*) = d;
                            else
                               *va_arg(args, float*) = d;
#if LIBDEF_PRINTF_FAR_PTR
                         }
#endif
                       }
                       s = nsp; tasg++;

                       }
                       break;
#endif
#if LIBDEF_PRINTF_FAR_PTR
             case 'S':
#endif
             case 's': while (isspace(*s)) ++s;               /* Eat spaces */
                       if (!fwi) fwi = LIBDEF_SCANF_BUF_SIZE; /* Set field width */
                       if (ma)
                       {
#if LIBDEF_PRINTF_FAR_PTR
                          if ((*format == 'S') || is_far_ptr)
                          {
                             far_ptr = va_arg(args, char *FAR);   /* Set char pointer */
                             while (!(isspace(*s)) && (fwi-- != 0) &&
                                    (*s != '\0'))                 /* Transfer array */
                             {
                                *far_ptr++ = *s++;
                             }
                             *far_ptr = '\0';                     /* Terminate array */
                             while (!isspace(*s) && (*s != '\0'))
                               s++;
                          } else {
#endif
                             ss = va_arg(args, char*);   /* Set char pointer */
                             while (!(isspace(*s)) && (fwi-- != 0) &&
                                    (*s != '\0'))        /* Transfer array */
                             {
                                *ss++ = *s++;
                             }
                             *ss = '\0';                 /* Terminate array */
                             while (!isspace(*s) && (*s != '\0'))
                               s++;
#if LIBDEF_PRINTF_FAR_PTR
                          }
#endif
                       }
                       else
                       {
                          while (!(isspace(*s)) && (*s != '\0'))
                            s++;  /* Skip array */
                       }
                       tasg++;
                       break;

             case '%':  /* << ES 11/15/95 */
                       if (*s == '%') {
                        s++; /* tasg++; */ /* eat '%', do not assign */
                       }
                       break;

             case 'c': if (!fwi) fwi = 1;  /* Set field width */
                       if (ma)
                       {
#if LIBDEF_PRINTF_FAR_PTR
                          if (is_far_ptr)
                          {
                             far_ptr = va_arg(args, char *FAR);   /* Set char pointer */
                             while (*s && fwi--)              /* Transfer array */
                             {
                                *far_ptr++ = *s++;
                             }
                          } else {
#endif
                             ss = va_arg(args, char*);   /* Set char pointer */
                             while (*s && fwi--)         /* Transfer array */
                             {
                                *ss++ = *s++;
                             }
#if LIBDEF_PRINTF_FAR_PTR
                          }
#endif
                       }
                       else
                       {
                          while (*s && fwi--) s++;  /* Skip array */
                       }
                       tasg++;
                       break;

#if LIBDEF_PRINTF_FAR_PTR
             case 'N':
#endif
             case 'n': if (ma)
                       {
#if LIBDEF_PRINTF_FAR_PTR
                          if ((*format == 'N') || is_far_ptr)
                          {
                             switch(sz)
                             {
                                case 1:  *va_arg(args, long int *FAR) = s - sbase;
                                         break;
                                case 2:  *va_arg(args, short int *FAR) = s - sbase;
                                         break;
                                default: *va_arg(args, int *FAR) = s - sbase;
                             }
                          } else {
#endif
                             switch(sz)
                             {
                                case 1:  *va_arg(args, long int*) = s - sbase;
                                         break;
                                case 2:  *va_arg(args, short int*) = s - sbase;
                                         break;
                                default: *va_arg(args, int*) = s - sbase;
                             }
#if LIBDEF_PRINTF_FAR_PTR
                          }
#endif
                       }
                       /* ++tasg; */
                       break;
#if LIBDEF_MULTI_CHAR_SELECTION
                 case '[':
                       if (ma)
                       {
#if LIBDEF_PRINTF_FAR_PTR
                          if (is_far_ptr)
                             far_ptr = va_arg(args, char *FAR);   /* Set char pointer */
                          else
#endif
                             ss = va_arg(args, char*);            /* Set char pointer */
                       }
                       if (!fwi) fwi = LIBDEF_SCANF_BUF_SIZE;     /* Set field width */
#if LIBDEF_PRINTF_FAR_PTR
                       if (is_far_ptr)
                          format = _copyFarList(far_ptr, s, &nsp, format, &ok, fwi, ma);
                       else
#endif
                          format = _copyList(ss, s, &nsp, format, &ok, fwi, ma);
                       s = nsp; tasg += ok;
#endif /* LIBDEF_MULTI_CHAR_SELECTION */

         }
         format++;
      }  /* end if */
   }  /* end while */

endscan:
   return tasg;

error:
   if (!tasg) return EOF;
   return tasg;
} /* end vsscanf */


/*****************************************************/

int sscanf(LIBDEF_ConstStringPtr s, LIBDEF_ConstStringPtr format, ...)
{
   int i;
   va_list args;

   va_start(args, format);
   i = vsscanf(s, format, args);
   va_end(args);
   return i;
} /* end sscanf */

/*****************************************************/

#pragma MESSAGE DEFAULT C4001
