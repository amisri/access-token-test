/*
 *  Filename: mpc.c
 *
 *  Purpose:  Functions for the MPC VME card
 *
 *  Author:   Stephen Page
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#ifdef __Lynx__
extern int snprintf(char * str, size_t size, const char * format, ...);
#endif

#define MUGEFHW_MPC

#include <mugefhw/mpc.h>
#include <mugefhw/vme.h>

////////////////////////////////////////////////////////////////////////////////

// Map an MPC card

int mpcMap(struct mpc_card * card)
{
    // Set addresses

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address       = MPC_VME_ADDR_BASE;
    card->reg_map.am            = MPC_VME_ADDR_MOD;
    card->reg_map.data_width    = MPC_DATA_WIDTH;
    card->reg_map.size          = MPC_SIZE;

    // Attempt to map registers

    if(!(card->regs = vmeMap(&card->reg_map, 1)))
    {
        return(1);
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap an MPC card

void mpcUnmap(struct mpc_card * card)
{
    // Do nothing if the card is not mapped

    if(!card->regs) return;

    // Unmap card

    vmeUnmap(&card->reg_map);

    // Zero card structure

    memset(card, 0, sizeof(*card));
}

////////////////////////////////////////////////////////////////////////////////

// Map an MPC card programming registers

int mpcProgMap(struct mpc_prog_card * card)
{
    // Set addresses

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address       = MPC_PROG_VME_ADDR_BASE;
    card->reg_map.am            = MPC_VME_ADDR_MOD;
    card->reg_map.data_width    = MPC_DATA_WIDTH;
    card->reg_map.size          = MPC_PROG_SIZE;

    // Attempt to map registers

    if(!(card->regs = vmeMap(&card->reg_map, 1)))
    {
        return(1);
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap an MPC card programming registers

void mpcProgUnmap(struct mpc_prog_card * card)
{
    // Do nothing if the card is not mapped

    if(!card->regs) return;

    // Unmap card

    vmeUnmap(&card->reg_map);

    // Zero card structure

    memset(card, 0, sizeof(*card));
}

////////////////////////////////////////////////////////////////////////////////

// Initialise MPC board

int mpcInit(struct mpc_card * card)
{
    unsigned int channel;
    unsigned int parm;

    // Do nothing if the card is not mapped

    if(!card->regs) return(1);

    // Check that PLD version matches expected

    if(ntohs(card->regs->version_pld) != MPC_EXPECTED_VER_PLD)
    {
        fprintf(stderr, "MPC PLD version (%hu) does not match expected (%u).\n",
                ntohs(card->regs->version_pld), MPC_EXPECTED_VER_PLD);
    }

    // Check that firmware version matches expected

    if(ntohs(card->regs->version_firmware) != MPC_EXPECTED_VER_FIRMWARE)
    {
        fprintf(stderr, "MPC firmware version (%hu) does not match expected (%u).\n",
                ntohs(card->regs->version_firmware), MPC_EXPECTED_VER_FIRMWARE);
    }

    // Zero all command parameter registers

    for(channel = 0 ; channel < MPC_NUM_CHANNELS ; channel++)
    {
        for(parm = 0 ; parm < MPC_NUM_PARMS ; parm++)
        {
            card->regs->parm[channel][parm] = 0;
        }
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Get MPC time

void mpcGetTime(struct mpc_card * card, struct timeval * time)
{
    // Do nothing if the card is not mapped

    if(!card->regs) return;

    // Note that the following operation is unsafe since the time may roll over between VME reads

    time->tv_sec    = ntohs(card->regs->time_s_h) << 16;
    time->tv_sec   |= ntohs(card->regs->time_s_l);
    time->tv_usec   = 0;
}

////////////////////////////////////////////////////////////////////////////////

// Set MPC time

void mpcSetTime(struct mpc_card * card, struct timeval * time)
{
    // Do nothing if the card is not mapped

    if(!card->regs) return;

    card->regs->time_s_h = htons((time->tv_sec & 0xFFFF0000) >> 16);
    card->regs->time_s_l = htons(time->tv_sec & 0x0000FFFF);
}

////////////////////////////////////////////////////////////////////////////////

// Read status_ind mask

uint64_t mpcReadStatusInd(struct mpc_card * card)
{
    uint64_t status_ind;

    // Do nothing if the card is not mapped

    if(!card->regs) return(0);

    if(htonl(1) == 1) // Big endian
    {
        status_ind = (uint64_t)card->regs->status_ind[0]        |
                     (uint64_t)card->regs->status_ind[1] << 16  |
                     (uint64_t)card->regs->status_ind[2] << 32  |
                     (uint64_t)card->regs->status_ind[3] << 48;
    }
    else // Little endian
    {
        status_ind = (uint64_t)ntohs(card->regs->status_ind[3])         |
                     (uint64_t)ntohs(card->regs->status_ind[2]) << 16   |
                     (uint64_t)ntohs(card->regs->status_ind[1]) << 32   |
                     (uint64_t)ntohs(card->regs->status_ind[0]) << 48;
    }

    return(status_ind);
}

////////////////////////////////////////////////////////////////////////////////

// Send a command to a channel

int mpcSendCommand(struct mpc_card  *card,
                   int              channel,
                   uint16_t         command,
                   uint16_t         num_parms,
                   uint16_t         *parms,
                   int              wait_ack)
{
    unsigned int    i;
    struct timespec sleep_time = { 0, 100000 }; // 100us

    // Do nothing if the card is not mapped

    if(!card->regs) return(-1);

    // Check that channel is ready to receive a command

    if(ntohs(card->regs->cmd[channel])       != MPC_CMD_READY ||
       ntohs(card->regs->cmd_state[channel]) == MPC_CMD_STATE_PROC)
    {
        return(-1);
    }

    // Set parameters

    card->regs->parm[channel][0] = htons(num_parms);

    for(i = 0 ; i < num_parms ; i++)
    {
        card->regs->parm[channel][1 + i] = htons(parms[i]);
    }

    // Send command

    card->regs->cmd[channel] = htons(command);

    // Wait for acknowledgement if requested

    if(wait_ack)
    {
        while(ntohs(card->regs->cmd[channel]) != MPC_CMD_READY)
        {
            // Sleep for time defined in sleep_time

            while(nanosleep(&sleep_time, &sleep_time))
            {
                if(errno != EINTR)
                {
                    perror("nanosleep");
                    break;
                }
            }
        }
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Read command error from a channel

int mpcError(struct mpc_card * card, int chan, uint32_t buffer_size, char * buffer)
{
    uint16_t    data;
    int         i = 0;

    // Do nothing if the card is not mapped

    if(!card->regs) return(1);

    // Check that channel is in error state

    if(ntohs(card->regs->cmd_state[chan]) != MPC_CMD_STATE_ERROR)
    {
        return(1);
    }

    // Copy error message to buffer

    if(buffer)
    {
        do
        {
            data        = ntohs(card->regs->data[chan][i]);
            buffer[i++] = data;
        }
        while(data && i < MPC_DATA_SIZE);
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Send a card to a channel, wait for it to complete and store response
//
// This function will return the number of 16 bit words stored in data_buf
// or -1 on error.  In the case of an error, data_buf will contain a NULL
// terminated error string.

int mpcSendCommandWait(struct mpc_card * card,
                       int              channel,
                       uint16_t         command,
                       uint16_t         num_parms,
                       uint16_t     *    parms,
                       uint32_t         data_buf_size,
                       uint16_t     *    data_buf)
{
    uint32_t        data_buf_els    = data_buf_size / sizeof(data_buf[0]);
    unsigned int    i;
    uint16_t        response_length;
    struct timespec sleep_time      = { 0, 100000 }; // 100us

    // Do nothing if the card is not mapped

    if(!card->regs) return(-1);

    // Ensure that data buffer is null-terminated by default

    if(data_buf)
    {
        data_buf[0] = 0;
    }

    // Send command

    if(mpcSendCommand(card, channel, command, num_parms, parms, 1))
    {
        if(data_buf)
        {
            snprintf((char *)data_buf, data_buf_size, "Failed to send command to MPC board");
        }

        return(-1);
    }

    // Wait for command processing

    while(ntohs(card->regs->cmd_state[channel]) == MPC_CMD_STATE_PROC)
    {
        // Sleep for time defined in sleep_time

        while(nanosleep(&sleep_time, &sleep_time))
        {
            if(errno != EINTR)
            {
                perror("nanosleep");
                break;
            }
        }
    }

    // Check whether an error occurred

    if(ntohs(card->regs->cmd_state[channel]) != MPC_CMD_STATE_READY &&
       !mpcError(card, channel, data_buf_size, (char *)data_buf))
    {
        return(-1);
    }

    // Copy response data into buffer

    if(data_buf)
    {
        response_length = ntohs(card->regs->data[channel][0]);

        // Check that response length is within accepted range

        if(response_length > data_buf_els)
        {
            snprintf((char *)data_buf, data_buf_size, "Buffer is too small for response: %d->%d", response_length , data_buf_els);
            return(-1);
        }

        // Copy response to data buffer

        for(i = 0 ; i < response_length ; i++)
        {
            data_buf[i] = ntohs(card->regs->data[channel][1 + i]);
        }

        return(response_length);
    }

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// EOF
