/*
 *  Filename: vme.c
 *
 *  Purpose:  Functions for accessing the VME bus
 *
 *  Author:   Stephen Page
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef __Lynx__
#include <stdint.h>
#include <sys/types.h>
#include <ces/vmelib.h>

extern uint32_t find_controller(uint32_t               vmeaddr,
                                uint32_t               len,
                                uint32_t               am,
                                uint32_t               offset,
                                uint32_t               size,
                                struct pdparam_master  *param);

extern void return_controller(uint32_t vmeaddr,
                              uint32_t len);
#else
#include <libvmebus.h>
#endif

#define MUGEFHW_VME

#include <mugefhw/vme.h>

////////////////////////////////////////////////////////////////////////////////

// Map a VME device

void *vmeMap(struct vme_map *map, int32_t berror_check)
{
#ifdef __Lynx__
    struct pdparam_master param;

    // Attempt to map VME device

    memset(&param, 0, sizeof(param));
    param.iack = 1;
    param.swap = SINGLE_AUTO_SWAP;

    if((map->p = (void *)find_controller(map->address,
                                         map->size,
                                         map->am,
                                         0,
                                         berror_check ? map->data_width / 8 : 0,
                                         &param)) == (void *)-1)
    {
        return(NULL);
    }
#else
    uint64_t address;
    uint32_t i;
    uint32_t value;

    // Prevent gcc warning about value being unused

    (void)value;

    map->libvmebus_map.am           = map->am;
    map->libvmebus_map.data_width   = map->data_width;
    map->libvmebus_map.sizel        = map->size;
    map->libvmebus_map.vme_addrl    = map->address;

    // Attempt to map VME device

    if((map->p = vme_map(&map->libvmebus_map, 1)) == NULL)
    {
        return(NULL);
    }

    // Perform bus error check if requested

    if(berror_check)
    {
        // Bus error check request number of locations

        address = map->address;
        for(i = 0 ; i < berror_check ; i++)
        {
            // Attempt a read of the appropriate data width

            switch(map->data_width)
            {
                case 16:
                    address  = map->address + (i * sizeof(uint16_t));
                    value    = *((volatile uint16_t *)map->p + i);
                    break;
                case 32:
                    address  = map->address + (i * sizeof(uint32_t));
                    value    = *((volatile uint32_t *)map->p + i);
                    break;
            }
            usleep(100000);

            // Check whether a bus error occurred

            if(vme_bus_error_check_clear(&map->libvmebus_map, address))
            {
                vmeUnmap(map);
                return(NULL);
            }
        }
    }
#endif

    return(map->p);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap a VME device

void vmeUnmap(struct vme_map *map)
{
#ifdef __Lynx__
    return_controller((uint32_t)map->p, map->size);
#else
    vme_unmap(&map->libvmebus_map, 0);
#endif

    map->p = NULL;
}

////////////////////////////////////////////////////////////////////////////////

// EOF
