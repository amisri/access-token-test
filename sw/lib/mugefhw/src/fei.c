/*
 *  Filename: fei.c
 *
 *  Purpose:  Functions for the Fast Extraction Interlock (FEI) VME card
 *
 *  Author:   Stephen Page
 */

#include <string.h>

#define MUGEFHW_FEI

#include <mugefhw/fei.h>
#include <mugefhw/vme.h>

////////////////////////////////////////////////////////////////////////////////

// Map an FEI card

int feiMap(struct fei_card *card)
{
    // Set addresses

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address       = FEI_VME_ADDR_BASE;
    card->reg_map.am            = FEI_VME_ADDR_MOD;
    card->reg_map.data_width    = FEI_DATA_WIDTH;
    card->reg_map.size          = FEI_SIZE;

    // Attempt to map registers

    if(!(card->regs = vmeMap(&card->reg_map, 1)))
    {
        return(1);
    }
    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap an FEI card

void feiUnmap(struct fei_card *card)
{   
    // Unmap card

    vmeUnmap(&card->reg_map);

    // Zero card structure

    memset(card, 0, sizeof(*card));
}

////////////////////////////////////////////////////////////////////////////////

// EOF
