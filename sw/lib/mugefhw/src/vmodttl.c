/*
 *  Filename: mpc.c
 *
 *  Purpose:  Functions for the MPC VME card
 *
 *  Author:   Stephen Page
 */

#include <arpa/inet.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include <vmod/libvmodttl.h>

#define MUGEFHW_VMODTTL

#include <mugefhw/vmodttl.h>

int vmodttlInit(struct vmodttl_card * card)
{
    int i;
	struct vmodttl_config vmodttl_device_config;

    // Set the channel direction

	vmodttl_device_config.dir_a = CHAN_IN;
    vmodttl_device_config.dir_b = CHAN_OUT;

    // Set the channel mode

    vmodttl_device_config.mode_a = TTL;
    vmodttl_device_config.mode_b = TTL;

    // Set logic for all channels

    vmodttl_device_config.inverting_logic = INVERT;

    // Disable interrupts

    for (i = 0; i < NUM_BITS; i++)
    {
        vmodttl_device_config.conf_pattern_a[i] = OFF;
        vmodttl_device_config.conf_pattern_b[i] = OFF;
    }

    // to be removed after test
    // as irqs are disabled, the values of 'pattern_mode' are not important

    vmodttl_device_config.pattern_mode_a = OR;
    vmodttl_device_config.pattern_mode_b = OR;

    // tobe be removed after test
    // as channel c is not used, the value of the length of a strobe pulse
    // that is generated in channel c is not important (in us)

    vmodttl_device_config.us_pulse = 1;

    if (vmodttl_io_config(*(card->device), vmodttl_device_config) < 0)
    {
        return errno;
    }

    return 0;
}

int vmodttlMap(struct vmodttl_card * card)
{
	if (! (card->device = (int*)malloc(sizeof(int))) )
	{
		return 1;
	}

	// VMOD TTL is connected to the modulbus number 0

	*(card->device) = 0;

	// open the device device

	if (vmodttl_open( *(card->device) ) < 0)
	{
		card->device = NULL;

		return 1;
	}

	return 0;
}

int vmodttlUnmap(struct vmodttl_card * card)
{
	if (vmodttl_close( *(card->device) ) < 0)
	{
		card->device = NULL;

		return 1;
	}

	free(card->device);

	card->device = NULL;

	return 0;
}

// Find the index of the mapping given a mugef channel number

static int find_vmod_signal(const struct vmodttl_mapping vmod_map[],int mugef_ch, int* index)
{
	int i;

	for (i=0; i != VMODTTL_MAP_LEN; ++i)
	{
		if (vmod_map[i].mugef_ch == mugef_ch)
		{
			*index = i;

			return 0;
		}
	}

	return -1;
}

int vmodttlGetNoFault(struct vmodttl_card * card, int channel, int * no_fault)
{
	int value;
	int i;

	// Card not mapped, it fails silently

	if (!card->device)
	{
		return -1;
	}

	// Find the VMOD fault signal definition

	if ( find_vmod_signal(vmodttl_sps_mains_faults,channel,&i) )
	{
		return -1;
	}

	// Read

	if (vmodttl_read( *(card->device), VMOD_TTL_CHANNEL_A, &value))
	{
		return errno;
	}

	*no_fault = value & vmodttl_sps_mains_faults[i].vmod_mask;

	return 0;
}

int vmodttlGetState(struct vmodttl_card * card,int channel, int * state)
{
	int value;
	int i;

	// Card not mapped, it fails silently

	if (!card->device)
	{
		return -1;
	}

	// Find the VMOD state signal definition

	if ( find_vmod_signal(vmodttl_sps_mains_status,channel,&i) )
	{
		return -1;
	}

	// Read

	if (vmodttl_read( *(card->device), VMOD_TTL_CHANNEL_A, &value))
	{
		return errno;
	}

	*state = value & vmodttl_sps_mains_status[i].vmod_mask;

	return 0;
}

int vmodttlSetState(struct vmodttl_card * card,int channel, int state)
{
	int value;
	int i;

	// Card not mapped, it fails silently

	if (!card->device)
	{
		return -1;
	}

	// Find the VMOD VRUN signal defintion

	if ( find_vmod_signal(vmodttl_sps_mains_cmds,channel,&i) )
	{
		return -1;
	}

	// Read

	if (vmodttl_read( *(card->device), VMOD_TTL_CHANNEL_B, &value))
	{
		return errno;
	}

	// Apply the mask

	if (state)
	{
		value |= vmodttl_sps_mains_cmds[i].vmod_mask;
		value &= 0xFF;
	}
	else
	{
		value &= ~(vmodttl_sps_mains_cmds[i].vmod_mask);
		value &= 0xFF;

	}

	// Write

	if (vmodttl_write( *(card->device), VMOD_TTL_CHANNEL_B, value))
	{
		return errno;
	}

	return 0;
}

// EOF
