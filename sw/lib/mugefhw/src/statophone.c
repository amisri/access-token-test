/*
 *  Filename: statophone.c
 *
 *  Purpose:  Functions for the statophone VME card
 *
 *  Author:   Stephen Page
 */

#include <stdint.h>
#include <string.h>

#define MUGEFHW_STATOPHONE

#include <mugefhw/statophone.h>
#include <mugefhw/vme.h>

////////////////////////////////////////////////////////////////////////////////

// Map a statophone card

int statophoneMap(struct statophone_card *card)
{
    // Set addresses

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address       = STATOPHONE_VME_ADDR_BASE;
    card->reg_map.am            = STATOPHONE_VME_ADDR_MOD;
    card->reg_map.data_width    = STATOPHONE_DATA_WIDTH;
    card->reg_map.size          = STATOPHONE_SIZE;

    // Attempt to map registers

    if(!(card->regs = vmeMap(&card->reg_map, 1)))
    {
        return(1);
    }
    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap a statophone card

void statophoneUnmap(struct statophone_card *card)
{   
    // Unmap card

    vmeUnmap(&card->reg_map);

    // Zero card structure

    memset(card, 0, sizeof(*card));
}

////////////////////////////////////////////////////////////////////////////////

// EOF
