/*
 *  Filename: mpv915-21.c
 *
 *  Purpose:  Functions for the MPV915-21 VME card
 *
 *  Author:   Stephen Page
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#endif

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define MUGEFHW_MPV915

#include "mugefhw/mpv915-21.h"
#include "mugefhw/vme.h"

////////////////////////////////////////////////////////////////////////////////

// Map an MPV915-21 card

int mpv915Map(struct mpv915_card *card, int card_num)
{
    // Set addresses

    memset(&card->ram_map, 0, sizeof(card->ram_map));
    card->ram_map.address       = MPV915_RAM_VME_ADDR_BASE +
                                  (MPV915_RAM_VME_ADDR_INC * card_num);
    card->ram_map.am            = MPV915_RAM_VME_ADDR_MOD;
    card->ram_map.data_width    = MPV915_RAM_DATA_WIDTH;
    card->ram_map.size          = MPV915_RAM_SIZE;

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address      = MPV915_REG_VME_ADDR_BASE + 
                                 (MPV915_REG_VME_ADDR_INC * card_num);
    card->reg_map.am           = MPV915_REG_VME_ADDR_MOD;
    card->reg_map.data_width   = MPV915_REG_DATA_WIDTH;
    card->reg_map.size         = MPV915_REG_SIZE;

    // Attempt to map registers

    if(!(card->regs = vmeMap(&card->reg_map, 1)))
    {
        return(1);
    }

    // Configure RAM offset following procedure from card manual section 4.4.1

    mpv915SetReg(&card->regs->a24_offset, (uint16_t)(card->ram_map.address >> 17));
    mpv915SetReg(&card->regs->window,     0x0003);

    // Attempt to map RAM

    if(!(card->ram = vmeMap(&card->ram_map, 0)))
    {
        fprintf(stderr, "Failed to map RAM at 0x%08x\n", card->ram_map.address);
        card->regs = NULL;
        vmeUnmap(&card->reg_map);
        return(1);
    }

    // Write magic address from card manual section 4.4.1

    *(uint16_t *)((void *)card->ram + 0x10C0C) = 0;

    // Sleep to allow DRAM warm-up

    mpv915DelayNs(100000000); // 100ms

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap an MPV915-21 card

void mpv915Unmap(struct mpv915_card *card)
{
    // Unmap card

    vmeUnmap(&card->ram_map);
    vmeUnmap(&card->reg_map);

    // Zero card structure

    memset(card, 0, sizeof(*card));
}

////////////////////////////////////////////////////////////////////////////////

// Many actions on the MPV915-21 require delays
// This function performs a high resolution delay

void mpv915DelayNs(uint64_t delay_ns)
{
#ifdef __Lynx__
    usleep(delay_ns / 1000);
#else
    uint64_t        elapsed_ns;
    struct timespec end;
    struct timespec start;

    // Delay using high resolution timer

    clock_gettime(CLOCK_MONOTONIC, &start);
    do
    {
        clock_gettime(CLOCK_MONOTONIC, &end);

        elapsed_ns = (end.tv_sec  - start.tv_sec) * 1000000000 +
                     (end.tv_nsec - start.tv_nsec);
    } while(elapsed_ns < delay_ns);
#endif
}

////////////////////////////////////////////////////////////////////////////////

// Set a register on an MPV915-21 card
// A delay is necessary for the card to take the write into account

static inline void mpv915SetReg(volatile uint16_t *reg, const uint16_t value)
{
    *reg = htons(value);

    // Delay to allow the card to take the write into account

    mpv915DelayNs(MPV915_DELAY_REG_WRITE_NS);
}

////////////////////////////////////////////////////////////////////////////////

// Set a ADC gains and wait for them to settle

static void mpv915SetGains(struct mpv915_card *card, const uint16_t adc0_gain,
                                                     const uint16_t adc1_gain)
{
    mpv915SetReg(&card->regs->adc0_gain, adc0_gain);
    mpv915DelayNs(MPV915_DELAY_REG_WRITE_NS);
    mpv915SetReg(&card->regs->adc1_gain, adc1_gain);

    mpv915DelayNs(300000000); // 300ms
}

////////////////////////////////////////////////////////////////////////////////

// Set ADC offsets and wait for them to settle

static void mpv915SetOffsets(struct mpv915_card *card, const uint16_t adc0_offset,
                                                       const uint16_t adc1_offset)
{
    mpv915SetReg(&card->regs->adc0_offset, adc0_offset);
    mpv915DelayNs(MPV915_DELAY_REG_WRITE_NS);
    mpv915SetReg(&card->regs->adc1_offset, adc1_offset);

    mpv915DelayNs(2000000); // 2ms
}

////////////////////////////////////////////////////////////////////////////////

// Perform soft triggers, read the results and store the averages a specified number of samples

static void mpv915TriggerRead(struct mpv915_card *card, uint32_t num_samples, float *adc0, float *adc1)
{
    uint32_t i = num_samples;

    *adc0 = 0;
    *adc1 = 0;

    // Read ADCs num_samples times and store averages

    while(i--)
    {
        // Trigger and read ADC 0 value

        mpv915SetReg(&card->regs->soft_trigger, 0);
        while(ntohs(card->regs->config) & MPV915_CONF_CONV_IN_PROG)
        {
            mpv915DelayNs(10000); // 10us
        }
        *adc0 += (float)ntohs(card->ram[0]) / num_samples;

        // Trigger and read ADC 1 value

        mpv915SetReg(&card->regs->soft_trigger, 0);
        while(ntohs(card->regs->config) & MPV915_CONF_CONV_IN_PROG)
        {
            mpv915DelayNs(10000); // 10us
        }
        *adc1 += (float)ntohs(card->ram[0]) / num_samples;
    }
}

////////////////////////////////////////////////////////////////////////////////

// Select an input
// This function is used to switch to reference inputs during calibration

static void mpv915SetInput(struct mpv915_card *card, uint32_t input)
{
    float dummy;

    // Set analogue input multiplexors to defined state
    // (input channel 0, input source normal)

    mpv915SetReg(&card->regs->config, ntohs(card->regs->config) |  MPV915_CONF_RESET_INPUT);
    mpv915SetReg(&card->regs->config, ntohs(card->regs->config) & ~MPV915_CONF_RESET_INPUT);

    // Advance to requested input

    while(input--)
    {
        // Advance input source

        mpv915SetReg(&card->regs->config, ntohs(card->regs->config) & ~MPV915_CONF_ADV_INPUT);
        mpv915SetReg(&card->regs->config, ntohs(card->regs->config) |  MPV915_CONF_ADV_INPUT);
        mpv915SetReg(&card->regs->config, ntohs(card->regs->config) & ~MPV915_CONF_ADV_INPUT);
    }

    // Trigger twice to discard conversions in the pipeline

    mpv915TriggerRead(card, 2, &dummy, &dummy);
}

////////////////////////////////////////////////////////////////////////////////

// Calibrate ADC offsets using the 0V reference

static void mpv915CalibrateOffsets(struct mpv915_card *card, struct mpv915_cal_results *cal_results)
{
    float       adc0;
    float       adc1;
    uint16_t    adc0_offset = 0;
    uint16_t    adc1_offset = 0;
    uint16_t    bit;

    // Start with an offset of zero

    mpv915SetOffsets(card, adc0_offset, adc1_offset);
    mpv915SetInput(card, MPV915_INPUT_ZERO);

    // Test each bit of offsets to try to move closer to MPV915_CAL_ZERO reference

    for(bit = 0x8000 ; bit & 0xFFF0 ; bit >>= 1)
    {
        mpv915SetOffsets(card, adc0_offset | bit, adc1_offset | bit);
        mpv915TriggerRead(card, MPV915_CAL_NUM_AVGS, &adc0, &adc1);

        if(adc0 <= MPV915_CAL_ZERO)
        {
            adc0_offset |= bit;
        }

        if(adc1 <= MPV915_CAL_ZERO)
        {
            adc1_offset |= bit;
        }
    }
    mpv915SetOffsets(card, adc0_offset, adc1_offset);

    // Store offsets in calibration results

    cal_results->adc_offset[0] = adc0_offset;
    cal_results->adc_offset[1] = adc1_offset;
}

////////////////////////////////////////////////////////////////////////////////

// Calibrate ADC gains

static void mpv915CalibrateGains(struct mpv915_card *card, struct mpv915_cal_results *cal_results)
{
    float       adc0;
    float       adc1;
    uint16_t    adc0_gain = 0xFFF0;
    uint16_t    adc1_gain = 0xFFF0;
    uint16_t    bit;

    // Start with minimum gain

    mpv915SetGains(card, adc0_gain, adc1_gain);

    // Test each bit of gains to try to move closer to MPV915_CAL_RANGE reference

    for(bit = 0x8000 ; bit & 0xFFF0 ; bit >>= 1)
    {
        float adc0_diff;
        float adc1_diff;

        mpv915SetGains(card, adc0_gain & ~bit, adc1_gain & ~bit);

        mpv915SetInput(card, MPV915_INPUT_POS);
        mpv915TriggerRead(card, MPV915_CAL_NUM_AVGS, &adc0, &adc1);
        adc0_diff = adc0;
        adc1_diff = adc1;

        mpv915SetInput(card, MPV915_INPUT_NEG);
        mpv915TriggerRead(card, MPV915_CAL_NUM_AVGS, &adc0, &adc1);
        adc0_diff -= adc0;
        adc1_diff -= adc1;

        if(adc0_diff <= MPV915_CAL_RANGE)
        {
            adc0_gain &= ~bit;
        }

        if(adc1_diff <= MPV915_CAL_RANGE)
        {
            adc1_gain &= ~bit;
        }
    }
    mpv915SetGains(card, adc0_gain, adc1_gain);

    // Store gains in calibration results

    cal_results->adc_gain[0] = adc0_gain;
    cal_results->adc_gain[1] = adc1_gain;
}

////////////////////////////////////////////////////////////////////////////////

// Calibrate ADC offsets and gains

int mpv915Calibrate(struct mpv915_card *card, struct mpv915_cal_results *cal_results)
{
    int                         i;
    struct mpv915_cal_results   results;

    // Switch to soft triggering

    mpv915SetReg(&card->regs->config, ntohs(card->regs->config) & ~MPV915_CONF_NORMAL_TRIG);

    // Set RAM window to page 0, location 0

    mpv915SetReg(&card->regs->window, 0x0000);

    // Start with gains at mid-point

    mpv915SetGains(card, 0x7FF0, 0x7FF0);

    // Perform calibration
    // This is repeated multiple times as the offsets and gains affect each other

    for(i = 0 ; i < MPV915_CAL_NUM_ITERATIONS ; i++)
    {
        // Calibrate offsets

        mpv915CalibrateOffsets(card, &results);

        // Calibrate gains

        mpv915CalibrateGains(card, &results);
    }

    // Read results of calibration

    mpv915SetInput(card, MPV915_INPUT_ZERO);
    mpv915TriggerRead(card, MPV915_CAL_NUM_AVGS, &results.zero[0], &results.zero[1]);

    mpv915SetInput(card, MPV915_INPUT_NEG);
    mpv915TriggerRead(card, MPV915_CAL_NUM_AVGS, &results.neg[0], &results.neg[1]);

    mpv915SetInput(card, MPV915_INPUT_POS);
    mpv915TriggerRead(card, MPV915_CAL_NUM_AVGS, &results.pos[0], &results.pos[1]);

    mpv915SetInput(card, MPV915_INPUT_NORMAL);

    // Switch to normal triggering

    mpv915SetReg(&card->regs->config, ntohs(card->regs->config) | MPV915_CONF_NORMAL_TRIG);

    // Copy results if a non-null cal_results pointer was supplied

    if(cal_results != NULL)
    {
        *cal_results = results;
    }

    // Check whether results exceeded thresholds

    if(results.neg[0]  < MPV915_CAL_NEG_MIN  || results.neg[1]  <  MPV915_CAL_NEG_MIN  ||
       results.neg[0]  > MPV915_CAL_NEG_MAX  || results.neg[1]  >  MPV915_CAL_NEG_MAX  ||
       results.zero[0] < MPV915_CAL_ZERO_MIN || results.zero[1] <  MPV915_CAL_ZERO_MIN ||
       results.zero[0] > MPV915_CAL_ZERO_MAX || results.zero[1] >  MPV915_CAL_ZERO_MAX ||
       results.pos[0]  < MPV915_CAL_POS_MIN  || results.pos[1]  <  MPV915_CAL_POS_MIN  ||
       results.pos[0]  > MPV915_CAL_POS_MAX  || results.pos[1]  >  MPV915_CAL_POS_MAX)
    {
        // Thresholds were exceeded
        // Set default values for gains and offsets

        mpv915SetGains(card,   MPV915_CAL_DEFAULT_GAIN,   MPV915_CAL_DEFAULT_GAIN);
        mpv915SetOffsets(card, MPV915_CAL_DEFAULT_OFFSET, MPV915_CAL_DEFAULT_OFFSET);

        return(1);
    }
    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// EOF
