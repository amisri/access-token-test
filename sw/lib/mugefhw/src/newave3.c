/*
 *  Filename: newave3.c
 *
 *  Purpose:  Functions for accessing the Newave3 card
 *
 *  Author:   Stephen Page
 */

#ifdef __Lynx__
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#define MUGEFHW_NEWAVE3

#include <mugefhw/newave3.h>
#include <mugefhw/vme.h>

////////////////////////////////////////////////////////////////////////////////

// Map a Newave3 card

int newave3Map(struct newave3_card *card, int card_num)
{
    // Set addresses

    memset(&card->ram_map, 0, sizeof(card->ram_map));
    card->ram_map.address       = NEWAVE3_RAM_VME_ADDR_BASE +
                                  (NEWAVE3_RAM_VME_ADDR_INC * card_num);
    card->ram_map.am            = NEWAVE3_RAM_VME_ADDR_MOD;
    card->ram_map.data_width    = NEWAVE3_RAM_DATA_WIDTH;
    card->ram_map.size          = NEWAVE3_RAM_SIZE;

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address       = NEWAVE3_REG_VME_ADDR_BASE +
                                  (NEWAVE3_REG_VME_ADDR_INC * card_num);
    card->reg_map.am            = NEWAVE3_REG_VME_ADDR_MOD;
    card->reg_map.data_width    = NEWAVE3_REG_DATA_WIDTH;
    card->reg_map.size          = NEWAVE3_REG_SIZE;

    // Attempt to map registers

    if(!(card->regs = vmeMap(&card->reg_map, 1)))
    {
        return(1);
    }

    // Attempt to map RAM

    card->regs->ram_base = htons(card->ram_map.address >> 20);
    card->regs->ram_page = htons(ntohs(card->regs->mid_high) >> 4);

    if(!(card->ram = vmeMap(&card->ram_map, 0)))
    {
        fprintf(stderr, "Failed to map RAM at 0x%08x\n", card->ram_map.address);
        card->regs = NULL;
        vmeUnmap(&card->reg_map);
        return(1);
    }

    // Get reference pointers

    newave3GetRef(card);

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// Unmap a Newave3 card

void newave3Unmap(struct newave3_card *card)
{
    // Unmap card

    vmeUnmap(&card->ram_map);
    vmeUnmap(&card->reg_map);

    // Zero card structure

    memset(card, 0, sizeof(*card));
}

////////////////////////////////////////////////////////////////////////////////

// Get main MID

struct newave3_mid_main *newave3GetMIDMain(struct newave3_card *card)
{
    struct newave3_mid_addrs    *mid_addrs;
    struct newave3_mid_main     *mid_main;

    mid_addrs   = (struct newave3_mid_addrs *)(card->ram + ((ntohs(card->regs->mid_high) & 0x000F) << 16) + ntohs(card->regs->mid_low));
    mid_main    = (struct newave3_mid_main  *)(card->ram + (ntohl(mid_addrs->main_start) & 0xFFFFF));

    return(mid_main);
}

////////////////////////////////////////////////////////////////////////////////

// Get main code version

uint16_t newave3GetVersion(struct newave3_card *card)
{
    struct newave3_mid_main *mid_main = newave3GetMIDMain(card);
    union
    {
        char        c[NEWAVE3_CODE_VER_LENGTH + 1];
        uint32_t    l;
    } version;

    version.l = mid_main->version;
    version.c[sizeof(version.c) - 1] = '\0';

    return(strtol(version.c, NULL, 16));
}

////////////////////////////////////////////////////////////////////////////////

// Get reference pointers

static void newave3GetRef(struct newave3_card *card)
{
    struct newave3_mid_main *mid_main = newave3GetMIDMain(card);

    card->ref.update_mask   = (volatile uint16_t *)(card->ram + (ntohl(mid_main->update_mask_addr)   & 0xFFFFF));
    card->ref.status        = (volatile uint16_t *)(card->ram + (ntohl(mid_main->status_addr)        & 0xFFFFF));
    card->ref.value         = (volatile int32_t  *)(card->ram + (ntohl(mid_main->ref_addr)           & 0xFFFFF));
}

////////////////////////////////////////////////////////////////////////////////

// Program Newave3

int newave3Program(struct newave3_card *card, char *filename, int force)
{
    unsigned int    address = 0;
    unsigned char   buffer[64];
    int             file;
    uint16_t        file_version;
    unsigned int    i;
    ssize_t         length;
    char            version[NEWAVE3_CODE_VER_LENGTH + 1];

    // Open code file

    if((file = open(filename, O_RDONLY)) < 0)
    {
        perror("open");
        return(1);
    }

    // Read file code version

    lseek(file, NEWAVE3_CODE_VER_OFFSET, SEEK_SET);

    if((length = read(file, version, NEWAVE3_CODE_VER_LENGTH)) != NEWAVE3_CODE_VER_LENGTH)
    {
        perror("read");
        close(file);
        return(1);
    }
    version[sizeof(version) - 1] = '\0';
    file_version = strtol(version, NULL, 16);
    lseek(file, 0, SEEK_SET);

    // Check whether file version matches card version

    if(!force && file_version == newave3GetVersion(card))
    {
        close(file);
        return(0);
    }

    printf("Newave version is 0x%04X, file %s version is 0x%04X.  Reprogramming...\n",
            newave3GetVersion(card), filename, file_version);

    // Put card in halt

    card->regs->cmd_stat = htons(NEWAVE3_CMD_STAT_HALT);

    // Set card RAM page to 0

    card->regs->ram_page = 0;

    // Read file and write data to card RAM

    while((length = read(file, &buffer, sizeof(buffer))) > 0)
    {
        for(i = 0 ; i < length ; i++)
        {
            card->ram[address++] = buffer[i];
        }
    }

    // Check whether an error occurred while reading the file

    if(length < 0)
    {
        perror("read");
        close(file);
        return(1);
    }

    close(file);

    // Reset card

    card->regs->action = htons(NEWAVE3_ACTION_RESET);

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

int newave3WaitBoot(struct newave3_card *card)
{
    unsigned int timeout;

    // Wait for card to boot

    timeout = NEWAVE3_BOOT_TIMEOUT;
    while(ntohs(card->regs->cmd_stat) != NEWAVE3_CMD_STAT_START && timeout--)
    {
        sleep(1);
    }

    // Check whether wait timed-out

    if(!timeout)
    {
        printf("Time-out waiting for card to boot\n");
        return(1);
    }

    // Set RAM base address

    card->regs->ram_base = htons(card->ram_map.address >> 20);
    card->regs->ram_page = htons(ntohs(card->regs->mid_high) >> 4);

    return(0);
}

////////////////////////////////////////////////////////////////////////////////

// EOF
