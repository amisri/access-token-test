/*
 *  @file:    svec.c
 *
 *  @brief:   Definitions for the access to the SPS Mains SVEC card
 */

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <time.h>

#include <mugefhw/svec.h>

int svecMap(struct svec_card * card)
{
    // Attempt to map filter coeficients

    memset(&card->ram_map, 0, sizeof(card->ram_map));
    card->ram_map.address       = SVEC_BASE_ADDR + SVEC_RAM_OFFSET;
    card->ram_map.am            = SVEC_RAM_VME_ADDR_MOD;
    card->ram_map.data_width    = SVEC_RAM_DATA_WIDTH;
    card->ram_map.size          = SVEC_RAM_DATA_SIZE;

    if(!(card->filter_coef = vmeMap(&card->ram_map, 1)))
    {
        fprintf(stderr, "ERROR: Failed to map SVEC filter coeficients at 0x%08x\n", card->ram_map.address);

        return 1;
    }

    // Attempt to map ram

    memset(&card->reg_map, 0, sizeof(card->reg_map));
    card->reg_map.address      = SVEC_BASE_ADDR + SVEC_REG_OFFSET;
    card->reg_map.am           = SVEC_REG_VME_ADDR_MOD;
    card->reg_map.data_width   = SVEC_REG_DATA_WIDTH;
    card->reg_map.size         = SVEC_REG_DATA_SIZE;

    // Attempt to map RAM

    if(!(card->regs = vmeMap(&card->reg_map,1)))
    {
        fprintf(stderr, "ERROR: Failed to map SVEC Registers at 0x%08x\n", card->reg_map.address);

        card->filter_coef = NULL;
        vmeUnmap(&card->ram_map);
        return 1;
    }

    // Enable all channels
    card->regs->channel_enable = htonl(0x3F);

	return 0;
}

int svecUnmap(struct svec_card * card)
{

    card->filter_coef = NULL;
    vmeUnmap(&card->ram_map);

    card->regs        = NULL;
    vmeUnmap(&card->reg_map);

	return 0;
}


// EOF
