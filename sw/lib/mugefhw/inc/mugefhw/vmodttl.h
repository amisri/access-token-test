/*
 *  @file: mpc.h
 *
 *  @brief:  Declarations for accessing the VMOD TTL Card. The card provides basic control and monitoring
 *  		 of the SPS Mains CIS PLC System.
 *
 */

#ifndef MUGEFHW_VMODTTL_H
#define MUGEFHW_VMODTTL_H

#include <stdint.h>
#include <sys/time.h>

#include <mugef/consts.h>

#define VMODTTL_MAP_LEN 6

struct vmodttl_card
{
    int*                      device;
};

struct vmodttl_mapping
{
	char            name[32];
	int             mugef_ch;
	uint8_t         vmod_mask;

};

// Mapping between VMOD TTL signals and SPS Mains Status

static const struct vmodttl_mapping vmodttl_sps_mains_status[] = {
		{"VS_POWER_ON_SMD" , 49, 0x01},
		{"VS_POWER_ON_SMQF", 17, 0x04},
		{"VS_POWER_ON_SMQD",  1, 0x10},
		{"VS_POWER_ON_SMD" , 41, 0x01},
		{"VS_POWER_ON_SMQF", 42, 0x04},
		{"VS_POWER_ON_SMQD", 43, 0x10}
};

// Mapping between VMOD TTL signals and SPS Main State

static const struct vmodttl_mapping vmodttl_sps_mains_faults[] = {
		{"VS_NO_FAULT_SMD" , 49, 0x40},
		{"VS_NO_FAULT_SMQF", 17, 0x02},
		{"VS_NO_FAULT_SMQD",  1, 0x08},
		{"VS_NO_FAULT_SMD" , 41, 0x40},
		{"VS_NO_FAULT_SMQF", 42, 0x02},
		{"VS_NO_FAULT_SMQD", 43, 0x08}
};

static const struct vmodttl_mapping vmodttl_sps_mains_cmds[] = {
		{"VS_RUN_SMD" , 49, 0x01},
		{"VS_RUN_SMQF", 17, 0x04},
		{"VS_RUN_SMQD",  1, 0x10},
		{"VS_RUN_SMD" , 41, 0x01},
		{"VS_RUN_SMQF", 42, 0x04},
		{"VS_RUN_SMQD", 43, 0x10}
};


#ifdef MUGEFHW_VMODTTL
#define MUGEFHW_VMODTTL_EXT
#else
#define MUGEFHW_VMODTTL_EXT extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

// External functions

/*!
 * Initializes the card. It has to be called once every time the VME Bus is reset.
 * The card handler has to be opened before calling this function.
 *
 * @param card Pointer to an already opened VMOD TTL Card handler
 *
 * @retval Returns 0 on success, otherwise there was an error.
 */
MUGEFHW_VMODTTL_EXT int         vmodttlInit(struct vmodttl_card * card);

/*!
 * Creates a VMOD TTL card handler.
 *
 * @param card Pointer to a VMOD TTL Card handler
 *
 * @retval Returns 0 on success, -1 if the error comes from the VMOD Mugef library, >0 if the error comes from the libvmodttl driver.
 */
MUGEFHW_VMODTTL_EXT int         vmodttlMap(struct vmodttl_card * card);

/*!
 * Destroys the resources associated with the VMOD TTL card handler.
 *
 * @param card Pointer to a VMOD TTL Card handler
 *
 * @retval Returns 0 on success, -1 if the error comes from the VMOD Mugef library, >0 if the error comes from the libvmodttl driver.
 */
MUGEFHW_VMODTTL_EXT int         vmodttlUnmap(struct vmodttl_card * card);

/*!
 * Get the fault status of one of the SPS Main converters set (QD, QS, MB)
 *
 * @param card      Pointer to a VMOD TTL Card handler
 * @param channel 	Channel number from the Mugef point of view. Where 41 corresponds to SPS Main Quadrupole Defocusing Magnets (QD),
 * 					42 corresponds to the SPS Main Quadrupole Focusing Magnets (QF), and 43 corresponds to the SPS Main Bending Magnets (MB).
 * @param fault     The value is 0 if there is a fault, otherwise there is no fault.
 *
 * @retval Returns 0 on success, -1 if the error comes from the VMOD Mugef library, >0 if the error comes from the libvmodttl driver.
 */
MUGEFHW_VMODTTL_EXT int         vmodttlGetNoFault(struct vmodttl_card * card,int channel, int * fault);

/*!
 * Get the state of one of the SPS Main converters set (QD, QS, MB)
 *
 * @param card      Pointer to a VMOD TTL Card handler
 * @param channel 	Channel number from the Mugef point of view. Where 41 corresponds to SPS Main Quadrupole Defocusing Magnets (QD),
 * 					42 corresponds to the SPS Main Quadrupole Focusing Magnets (QF), and 43 corresponds to the SPS Main Bending Magnets (MB).
 * @param state     The value is 0 if any of the converters are OFF. The value is 1 if all the converters are ON.
 *
 * @retval Returns 0 on success, -1 if the error comes from the VMOD Mugef library, >0 if the error comes from the libvmodttl driver.
 */
MUGEFHW_VMODTTL_EXT int         vmodttlGetState(struct vmodttl_card * card,int channel, int * state);

/*!
 * Set the state of one of the SPS Main converters set (QD, QS, MB). This function is not thread-safe because setting a signle bit implies two operations:
 * 1) retrieving the current values, 2) Applying the mask to the retrieved value, and 3) setting the bit.
 *
 * @param card      Pointer to a VMOD TTL Card handler
 * @param channel 	Channel number from the Mugef point of view. Where 41 corresponds to SPS Main Quadrupole Defocusing Magnets (QD),
 * 					42 corresponds to the SPS Main Quadrupole Focusing Magnets (QF), and 43 corresponds to the SPS Main Bending Magnets (MB)
 * @param state     The value 0 request to the SPS Main CIS system to change the state of the converters to OFF.
 * 					The value 1 request to the SPS Main CIS system to change the state of the converters to ON.
 *
 * @retval Returns 0 on success, -1 if the error comes from the VMOD Mugef library, >0 if the error comes from the libvmodttl driver.
 */
MUGEFHW_VMODTTL_EXT int         vmodttlSetState(struct vmodttl_card * card,int channel, int state);

#ifdef __cplusplus
}
#endif

#endif

// EOF
