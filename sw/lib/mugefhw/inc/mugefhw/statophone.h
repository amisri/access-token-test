/*
 *  Filename: statophone.h
 *
 *  Purpose:  Declarations for accessing the statophone card
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEFHW_STATOPHONE_H
#define MUGEFHW_STATOPHONE_H

#include <stdint.h>

#include <mugefhw/vme.h>

// Constants

#define STATOPHONE_DATA_WIDTH       16                      // VME data width
#define STATOPHONE_SIZE             0x0200                  // Size of VME mapped memory
#define STATOPHONE_VME_ADDR_BASE    0xF000                  // VME base address
#define STATOPHONE_VME_ADDR_MOD     VME_ADDR_MODIFIER_16    // VME address modifier

#define STATOPHONE_MID_LEN          32                      // Length of MID

// Card status register constants

#define STATOPHONE_CARD_STATUS_BUSY 0x0001                  // Card is busy

// Card registers

struct statophone_regs
{
    volatile const uint8_t  mid[STATOPHONE_MID_LEN * sizeof(uint16_t)];
    const uint16_t          padding_1[96];
    volatile const uint16_t card_status;
    const uint16_t          padding_2[127];
    volatile uint16_t       command;
    volatile uint16_t       enable[4];
    const uint16_t          padding_3[123];
    volatile const uint16_t status[64];
    volatile const uint16_t special_status[64];
};

struct statophone_card
{
    struct vme_map          reg_map;
    struct statophone_regs  *regs;
};

#ifdef MUGEFHW_STATOPHONE
#define MUGEFHW_STATOPHONE_EXT
#else
#define MUGEFHW_STATOPHONE_EXT extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef MUGEFHW_STATOPHONE
#endif

// External functions

MUGEFHW_STATOPHONE_EXT int  statophoneMap(struct statophone_card *card);
MUGEFHW_STATOPHONE_EXT void statophoneUnmap(struct statophone_card *card);

#ifdef __cplusplus
}
#endif

#endif

// EOF
