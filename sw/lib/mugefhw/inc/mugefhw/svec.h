/*
 *  @file: svec.h
 *
 *  @brief:  Declarations for accessing the SVEC Card. The card provides high accuracy measurements for the SPS Mains
 *
 */

#ifndef MUGEFHW_SVEC_H
#define MUGEFHW_SVEC_H

#include <mugefhw/vme.h>

//! See SVEC board documetation in https://wikis.cern.ch/pages/viewpage.action?pageId=73731636&src=contextnavpagetreemode

#define SVEC_BASE_ADDR             0x18D00000           // Base Address

#define SVEC_REG_OFFSET            0x0                  // Offset of the SVEC registers
#define SVEC_REG_VME_ADDR_MOD      VME_A32_USER_DATA_SCT
#define SVEC_REG_DATA_SIZE         0x21
#define SVEC_REG_DATA_WIDTH        32

#define SVEC_RAM_OFFSET            0x8000               // Offset of the memory containing the filter coeficients
#define SVEC_RAM_NUM_ELEMENTS      1024                 // Number of 32bit elements in the RAM
#define SVEC_RAM_VME_ADDR_MOD      VME_A32_USER_DATA_SCT
#define SVEC_RAM_DATA_SIZE         SVEC_REG_DATA_SIZE
#define SVEC_RAM_DATA_WIDTH        32

#define SVEC_NUM_CHANNELS          6

struct svec_regs
{
    volatile const uint32_t modulator_error;            // 0x00: (R) Indicates if the modulator signal is in error (b5-b0: per-channel bitmask)
    volatile const uint32_t not_used_1[2];              //
    volatile const uint32_t timing_error;               // 0x0C: Indicates if there's a timing error (b0 = 1 : timing signal incorrect)
    volatile const uint32_t ch[SVEC_NUM_CHANNELS];      // 0x10: Channel 1 to 6
    volatile const uint32_t ch5_modulator_error_count;  // 0x28: ch5 counter of errors in modulator singal 
    volatile const uint32_t ch6_modulator_error_count;  // 0x2C: ch6 counter of errors in modulator singal
    volatile       uint32_t channel_enable;             // 0x30: Channel b5-b0 enables the corresponding [Default is all enabled: 0x3F]
    volatile       uint32_t lemo_cfg;                   // 0x34: Manages internal config (see EDMS-1729111)
    volatile const uint32_t not_used_2[2];              //
    volatile       uint32_t filter_version;             // 0x40: The filter coeficient version
    volatile       uint32_t filter_program_revision;    // 0x44: The filter coeficient revistion
    volatile const uint32_t not_used_3[2];              //
    volatile const uint32_t ch1_modulator_error_count;  // 0x50: ch1 counter of errors in modulator singal
    volatile const uint32_t ch2_modulator_error_count;  // 0x54: ch2 counter of errors in modulator singal
    volatile const uint32_t ch3_modulator_error_count;  // 0x58: ch3 counter of errors in modulator singal
    volatile const uint32_t ch4_modulator_error_count;  // 0x5C: ch4 counter of errors in modulator singal
    volatile const uint32_t timing_error_counter;       // 0x60: Number of errors in timing signal
    volatile const uint32_t soft_err_cnt;               // 0x60: Rising edge on soft error counter
    volatile const uint32_t not_used_4[2];              //
    volatile const uint32_t fpga_revision;              // 0x6C: Revision of the fpga program
} __attribute__((packed));

struct svec_card
{
    struct vme_map          reg_map;
    struct vme_map          ram_map;
    struct svec_regs        *regs;
    volatile uint32_t       *filter_coef;
};


#ifdef __cplusplus
extern "C" {
#endif

// External functions

/*!
 * Creates an SVEC card handler.
 *
 * @param card Pointer to a SVEC Card handler
 *
 * @retval Returns 0 on success, otherwise there was an error
 */
int         svecMap(struct svec_card * card);

/*!
 * Destroys the resources associated with the SVEC card handler.
 *
 * @param card Pointer to a SVEC Card handler
 *
 * @retval Returns 0 on success, otherwise there was an error
 */
int         svecUnmap(struct svec_card * card);

#ifdef __cplusplus
}
#endif

#endif

// EOF
