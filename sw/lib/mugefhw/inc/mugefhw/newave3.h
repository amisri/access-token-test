/*
 *  Filename: newave3.h
 *
 *  Purpose:  Declarations for accessing the Newave3 card
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEFHW_NEWAVE3_H
#define MUGEFHW_NEWAVE3_H

#include <stdint.h>

#include <mugefhw/vme.h>

// Constants

#define NEWAVE3_NUM_ADDRS           8
#define NEWAVE3_NUM_CHANS           8                       // Maximum number of channels per card

#define NEWAVE3_CODE_VER_LENGTH     4                       // Version length in bytes
#define NEWAVE3_CODE_VER_OFFSET     0x10010                 // Version byte offset within code

#define NEWAVE3_RAM_DATA_WIDTH      16                      // VME data width of RAM
#define NEWAVE3_RAM_SIZE            0x100000                // Size of VME mapped memory
#define NEWAVE3_RAM_VME_ADDR_BASE   0x100000                // VME base address of RAM
#define NEWAVE3_RAM_VME_ADDR_INC    0x100000                // Increment between possible RAM addresses
#define NEWAVE3_RAM_VME_ADDR_MOD    VME_ADDR_MODIFIER_24    // VME address modifier of RAM

#define NEWAVE3_REG_DATA_WIDTH      16                      // VME data width of registers
#define NEWAVE3_REG_SIZE            0x0020                  // Size of VME mapped memory
#define NEWAVE3_REG_VME_ADDR_BASE   0xD000                  // VME base address of registers
#define NEWAVE3_REG_VME_ADDR_INC    0x0020                  // Increment between possible register addresses
#define NEWAVE3_REG_VME_ADDR_MOD    VME_ADDR_MODIFIER_16    // VME address modifier of registers

#define NEWAVE3_BOOT_TIMEOUT        20                      // Time-out in seconds to wait for card to boot

// Action register

#define NEWAVE3_ACTION_RESET        0x8000                  // Reset card

// Command / status register

#define NEWAVE3_CMD_STAT_HALT       0xFFFC                  // Halt card
#define NEWAVE3_CMD_STAT_CPU_RUN    0x0001                  // Card run (not in halt)
#define NEWAVE3_CMD_STAT_VME_RUN    0x0002                  // VME run (no halt request)
#define NEWAVE3_CMD_STAT_INT_OK     0x0004                  // Internal state okay
#define NEWAVE3_CMD_STAT_EXT_OK     0x0008                  // External state okay
#define NEWAVE3_CMD_STAT_GLOB_OK    0x0010                  // Global state okay
#define NEWAVE3_CMD_STAT_TST_RSLT   0x4000                  // Result of internal test
#define NEWAVE3_CMD_STAT_INVALID    0x8000                  // Status invalid

// Code status

#define NEWAVE3_CMD_STAT_CODE_MASK  0x3000                  // Mask of code status
#define NEWAVE3_CMD_STAT_APP        0x0000                  // Application code running
#define NEWAVE3_CMD_STAT_FIRM       0x1000                  // Firmware running
#define NEWAVE3_CMD_STAT_BOOT       0x2000                  // Boot running
#define NEWAVE3_CMD_STAT_NONE       0x3000                  // No test running

// 0x0020 = APPLIWARE RESULTS: APPLIWARE MID DONE (SAME AS FIRMWARE)

#define NEWAVE3_CMD_STAT_START     (NEWAVE3_CMD_STAT_TST_RSLT   | \
                                    NEWAVE3_CMD_STAT_GLOB_OK    | \
                                    0x0020                      | \
                                    NEWAVE3_CMD_STAT_EXT_OK     | \
                                    NEWAVE3_CMD_STAT_INT_OK     | \
                                    NEWAVE3_CMD_STAT_VME_RUN    | \
                                    NEWAVE3_CMD_STAT_CPU_RUN)

// Main

#define NEWAVE3_STATUS_REF_TRIG     0x0001                  // References have been triggered
#define NEWAVE3_STATUS_REF_READ     0x0002                  // References have been read
#define NEWAVE3_STATUS_REF_WRITE    0x0004                  // References have been written

// Card registers

struct newave3_regs
{
    volatile uint16_t       cmd_stat;       // Command / status register
    volatile uint16_t       ram_base;       // RAM base address
    volatile uint16_t       ram_page;       // RAM page selector
    volatile uint16_t       mid_high;       // MID address high word
    volatile uint16_t       mid_low;        // MID address low word
    volatile uint16_t       action;         // Action for firmware (and reset bit)
    volatile const uint16_t addon_stat;     // Add-on status
    volatile uint16_t       irqv;           // Interrupt enable
    volatile uint16_t       ivec[8];        // Interrupt vectors
};

struct newave3_mid_addrs
{
    volatile const uint32_t boot_start;     // Start of boot MID
    volatile const uint32_t boot_end;       // End of boot MID
    volatile const uint32_t main_start;     // Start of main MID
    volatile const uint32_t main_end;       // End of main MID
};

struct newave3_mid_main
{
    volatile const uint32_t size;                   // MID size
    volatile const uint32_t version;                // Main code version
    volatile const uint32_t fw_start;               // Start address of firmware
    volatile const uint32_t fw_end;                 // End address of firmware
    volatile const uint32_t fw_size;                // Size of firmware
    volatile const uint32_t reserved0[4];
    volatile const uint32_t addon_type;             // Type of add-on card
    volatile const uint32_t addon_sub;              // Sub-type of add-on card
    volatile const uint32_t addon_trig;             // Type of trigger inputs used by add-on card
    volatile const uint32_t addon_chan_phys_mask;   // Physically connectable channel mask
    volatile const uint32_t addon_chan_phys_num;    // Number of physically connectable channels
    volatile const uint32_t addon_chan_log_mask;    // Logically connectable channel mask
    volatile const uint32_t addon_chan_log_num;     // Number of logically connectable channels
    volatile const uint32_t reserved1;
    volatile const uint32_t addon_test_aa;          // Add-on byte constant 0xAA
    volatile const uint32_t addon_test_55;          // Add-on byte constant 0x55
    volatile const uint32_t addon_chan_char[8];     // Add-on channel characteristics
    volatile const uint32_t ref_addr;               // Reference base address
    volatile const uint32_t update_mask_addr;       // Update mask address
    volatile const uint32_t status_addr;            // Status address
};

struct newave3_ref
{
    volatile uint16_t       *update_mask;   // Mask of channels with updated references
    volatile uint16_t       *status;        // Reference status
    volatile int32_t        *value;         // Reference values
};

struct newave3_card
{
    struct vme_map          ram_map;
    struct vme_map          reg_map;
    struct newave3_regs     *regs;
    volatile uint8_t        *ram;
    struct newave3_ref      ref;
};

#ifdef MUGEFHW_NEWAVE3
#define MUGEFHW_NEWAVE3_EXT
#else
#define MUGEFHW_NEWAVE3_EXT extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef MUGEFHW_NEWAVE3
static void newave3GetRef(struct newave3_card *card);
#endif

// External functions

MUGEFHW_NEWAVE3_EXT struct newave3_mid_main *newave3GetMIDMain(struct newave3_card *card);
MUGEFHW_NEWAVE3_EXT uint16_t                newave3GetVersion(struct newave3_card *card);
MUGEFHW_NEWAVE3_EXT int                     newave3Map(struct newave3_card *card, int card_num);
MUGEFHW_NEWAVE3_EXT int                     newave3Program(struct newave3_card *card, char *filename, int force);
MUGEFHW_NEWAVE3_EXT void                    newave3Unmap(struct newave3_card *card);
MUGEFHW_NEWAVE3_EXT int                     newave3WaitBoot(struct newave3_card *card);

#ifdef __cplusplus
}
#endif

#endif

// EOF
