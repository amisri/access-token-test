/*
 *  Filename: vme.h
 *
 *  Purpose:  Declarations for accessing the VME bus
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEFHW_VME_H
#define MUGEFHW_VME_H

#include <stdint.h>

#ifndef __Lynx__
#include <libvmebus.h>
#endif

// Constants

#define VME_ADDR_BASE_16                0xDFFE0000              // VME 16-bit address range base
#define VME_ADDR_BASE_24                0xDE000000              // VME 24-bit address range base
#define VME_ADDR_BASE_32                0xD0000000              // VME 32-bit address range base

#ifdef __Lynx__
#define VME_ADDR_MODIFIER_16            0x29                    // VME 16-bit address modifier
#define VME_ADDR_MODIFIER_24            0x39                    // VME 24-bit address modifier
#define VME_ADDR_MODIFIER_32            0x09                    // VME 32-bit address modifier
#else
#define VME_ADDR_MODIFIER_16            VME_A16_USER            // VME 16-bit address modifier
#define VME_ADDR_MODIFIER_24            VME_A24_USER_DATA_SCT   // VME 24-bit address modifier
#define VME_ADDR_MODIFIER_32            VME_A32_USER_DATA_SCT   // VME 32-bit address modifier
#endif

// Macros for VME bus access

#define VME_INT16S_RO(base, offset)     *(const volatile  int16_t *)((base) + (offset))
#define VME_INT16S_RW(base, offset)     *(      volatile  int16_t *)((base) + (offset))
#define VME_INT16U_RO(base, offset)     *(const volatile uint16_t *)((base) + (offset))
#define VME_INT16U_RW(base, offset)     *(      volatile uint16_t *)((base) + (offset))
#define VME_INT32S_RO(base, offset)     *(const volatile  int32_t *)((base) + (offset))
#define VME_INT32S_RW(base, offset)     *(      volatile  int32_t *)((base) + (offset))
#define VME_INT32U_RO(base, offset)     *(const volatile uint32_t *)((base) + (offset))
#define VME_INT32U_RW(base, offset)     *(      volatile uint32_t *)((base) + (offset))

#ifdef MUGEFHW_VME
#define MUGEFHW_VME_EXT
#else
#define MUGEFHW_VME_EXT extern
#endif

// Types

struct vme_map
{
    uint32_t            address;        // Address
    int32_t             am;             // Address modifier
    int32_t             data_width;     // Data width
    uint32_t            size;           // Mapping size
    void                *p;             // Pointer to mapped data

#ifndef __Lynx__
    struct vme_mapping  libvmebus_map;  // Mapping for libvmebus
#endif
};

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef MUGEFHW_VME
#endif

// External functions

MUGEFHW_VME_EXT void *vmeMap(struct vme_map *map, int berror_check);
MUGEFHW_VME_EXT void vmeUnmap(struct vme_map *map);

#ifdef __cplusplus
}
#endif

#endif

// EOF
