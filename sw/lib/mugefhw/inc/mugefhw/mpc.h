/*
 *  Filename: mpc.h
 *
 *  Purpose:  Declarations for accessing the MPC card
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEFHW_MPC_H
#define MUGEFHW_MPC_H

#include <stdint.h>
#include <sys/time.h>

#include <mugef/consts.h>
#include <mugef/mpc_cmd.h>
#include <mugefhw/vme.h>

// Constants

#define MPC_EXPECTED_VER_FIRMWARE   4                                               // Version of the card's firmware described by this file
#define MPC_EXPECTED_VER_PLD        3                                               // Version of the card's PLD programming described by this file

#define MPC_VME_ADDR_BASE           0xA00000                                        // VME base address of MPC card
#define MPC_DATA_WIDTH              16                                              // VME data width
#define MPC_PROG_SIZE               0x0000A                                         // Size of VME mapped memory for programming
#define MPC_PROG_VME_ADDR_BASE      0xE00000                                        // VME base address of MPC card for programming
#define MPC_SIZE                    0x80000                                         // Size of VME mapped memory
#define MPC_VME_ADDR_MOD            VME_ADDR_MODIFIER_24                            // VME address modifier of MPC card

#define MPC_ADDR                    (VME_ADDR_BASE_24 | MPC_VME_ADDR_BASE)          // Direct address of MPC card
#define MPC_PROG_ADDR               (VME_ADDR_BASE_24 | MPC_PROG_VME_ADDR_BASE)     // Direct address of MPC card for programming



// MPC command register (cmd) constants

#define MPC_CMD_NO_ACK              0x0000
#define MPC_CMD_READY               0x0001

// Register access

#define MPC_VERSION_PLD(base)       VME_INT16U_RO(base, 0x000000)
#define MPC_VERSION_FIRMWARE(base)  VME_INT16U_RO(base, 0x000002)
#define MPC_CMD(base, chan)         VME_INT16U_RW(base, 0x000004 + ((chan) * sizeof(short)))
#define MPC_STATUS_IND_48_63(base)  VME_INT16U_RO(base, 0x00008A)
#define MPC_STATUS_IND_32_47(base)  VME_INT16U_RO(base, 0x00008C)
#define MPC_STATUS_IND_16_31(base)  VME_INT16U_RO(base, 0x00008E)
#define MPC_STATUS_IND_00_15(base)  VME_INT16U_RO(base, 0x000090)
#define MPC_TIME_S_H(base)          VME_INT16U_RW(base, 0x000092)
#define MPC_TIME_S_L(base)          VME_INT16U_RW(base, 0x000094)
#define MPC_LINK(base, chan)        VME_INT16U_RW(base, 0x000100 + ((chan) * sizeof(short)))
#define MPC_PARM(base, chan, parm)  VME_INT16U_RW(base, 0x000200 + (((chan * MPC_NUM_PARMS) + parm) * sizeof(short)))
#define MPC_STATUS(base, chan)      VME_INT16U_RO(base, 0x020000 + ((chan) * sizeof(short)))
#define MPC_CMD_STATE(base, chan)   VME_INT16U_RO(base, 0x020200 + ((chan) * sizeof(short)))
#define MPC_DATA(base, chan)        VME_INT16U_RO(base, 0x020400 + ((chan) * sizeof(short)))

// CPU programming register commands

#define MPC_CPU_PROG_MODE_RUN       0x0001  // Put the CPU into normal operation mode
#define MPC_CPU_PROG_RESET          0x0002  // Reset the CPU
#define MPC_CPU_PROG_MODE_PROG      0x0003  // Put the CPU into programming mode

// Card memory structure

struct mpc_regs
{
    volatile const uint16_t version_pld;                            // PLD version
    volatile const uint16_t version_firmware;                       // Firmware version
    volatile uint16_t       cmd[MPC_NUM_CHANNELS];                  // Command channels

    const uint16_t          padding_1;

    volatile const uint16_t status_ind[4];                          // Status changed indicators
    volatile uint16_t       time_s_h;                               // Unix time seconds (high word)
    volatile uint16_t       time_s_l;                               // Unix time seconds (low word)

    const uint16_t          padding_2[53];

    volatile uint16_t       link[MUGEF_MAX_CHANNELS];

    const uint16_t          padding_3[64];

    volatile uint16_t       parm[MPC_NUM_CHANNELS][MPC_NUM_PARMS];  // Command parameters

    const uint16_t          padding_4[32182];
    volatile uint16_t       vme_buffer[32768];

    volatile const uint16_t status[MUGEF_MAX_CHANNELS];             // Channel status

    const uint16_t          padding_5[192];

    volatile const uint16_t cmd_state[MPC_NUM_CHANNELS];            // Channel command states

    const uint16_t          padding_6[190];

    volatile const uint16_t data[MPC_NUM_CHANNELS][MPC_DATA_SIZE];  // Channel command response data
};

// MPC card programming interface

struct mpc_prog_regs
{
    volatile uint16_t jtag_prog;       // JTAG access for programming
    volatile uint16_t cpu_prog;        // CPU access for programming
    volatile uint16_t vme_control;     // VME control
    volatile uint16_t cpu_bootloader_tx;  // CPU boad loader Tx
    volatile uint16_t cpu_bootloader_rx;  // CPU boad loader Rx
    volatile uint16_t cpu_baudrate;    // CPU baud rate
};

struct mpc_card
{
    struct vme_map  reg_map;
    struct mpc_regs * regs;
};

struct mpc_prog_card
{
    struct vme_map          reg_map;
    struct mpc_prog_regs  *  regs;
};

#ifdef MUGEFHW_MPC
#define MUGEFHW_MPC_EXT
#else
#define MUGEFHW_MPC_EXT extern
#endif

// Global variables

// Link names

MUGEFHW_MPC_EXT char * mpc_link_name[]
#ifdef MUGEFHW_MPC
    = {"unused", "serial *deprecated*", "daisy"}
#endif
      ;

// Command acknowledgement names

MUGEFHW_MPC_EXT char * mpc_cmd_ack_name[]
#ifdef MUGEFHW_MPC
    = {"no_ack", "ready"}
#endif
      ;

// Command state names

MUGEFHW_MPC_EXT char * mpc_cmd_state_name[]
#ifdef MUGEFHW_MPC
    = {"ready", "processing", "error", "timeout"}
#endif
      ;

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef MUGEFHW_MPC
#endif

// External functions

MUGEFHW_MPC_EXT int         mpcInit(struct mpc_card * card);
MUGEFHW_MPC_EXT int         mpcError(struct mpc_card * card, int chan, uint32_t buffer_size, char * buffer);
MUGEFHW_MPC_EXT void        mpcGetTime(struct mpc_card * card, struct timeval * time);
MUGEFHW_MPC_EXT int         mpcMap(struct mpc_card * card);
MUGEFHW_MPC_EXT int         mpcProgMap(struct mpc_prog_card * card);
MUGEFHW_MPC_EXT void        mpcProgUnmap(struct mpc_prog_card * card);
MUGEFHW_MPC_EXT uint64_t    mpcReadStatusInd(struct mpc_card * card);
MUGEFHW_MPC_EXT int         mpcSendCommand(struct mpc_card * card, int channel, uint16_t command,
                                           uint16_t num_parms, uint16_t * parms, int wait_ack);
MUGEFHW_MPC_EXT int         mpcSendCommandWait(struct mpc_card * card, int channel, uint16_t command,
                                               uint16_t num_parms, uint16_t * parms, uint32_t data_buf_size, uint16_t * data_buf);
MUGEFHW_MPC_EXT void        mpcSetTime(struct mpc_card * card, struct timeval * time);
MUGEFHW_MPC_EXT void        mpcUnmap(struct mpc_card * card);

#ifdef __cplusplus
}
#endif

#endif

// EOF
