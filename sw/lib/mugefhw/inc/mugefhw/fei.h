/*
 *  Filename: fei.h
 *
 *  Purpose:  Declarations for accessing the Fast Extraction Interlock (FEI) card
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEFHW_FEI_H
#define MUGEFHW_FEI_H

#include <stdint.h>

#include <mugefhw/vme.h>

// Constants

#define FEI_EXPECTED_VERSION            2                                                           // Version of the card's firmware described by this file

#define FEI_VME_ADDR_BASE               0xF70000                                                    // VME base address of FEI card
#define FEI_VME_ADDR_MOD                VME_ADDR_MODIFIER_24                                        // VME address modifier of FEI card
#define FEI_ADDR                        (VME_ADDR_BASE_24 | FEI_VME_ADDR_BASE)                      // Direct address of FEI card
#define FEI_DATA_WIDTH                  16                                                          // VME data width
#define FEI_NUM_BD_OUTPUTS              2                                                           // Number of beam dump outputs
#define FEI_NUM_FEI_OUTPUTS             4                                                           // Number of FEI outputs
#define FEI_SIZE                        0x10000                                                     // Size of VME mapped memory
#define FEI_TIMER_UNIT_US               100                                                         // Length of one timer unit in us

// Register access

#ifdef FEI_NEW // New FEI programming with corrected addressing
#define FEI_RESET(base)                 VME_INT16U_RW(base, 0x000000) = 0                           // Reset FEI counters
#define FEI_VERSION(base)               VME_INT16U_RO(base, 0x000002)                               // Card firmware version
#define FEI_FEI_TIMER(base, channel)    VME_INT16U_RW(base, 0x000004 + (channel * sizeof(short)))   // FEI timer
#define FEI_BD_TIMER(base, channel)     VME_INT16U_RW(base, 0x00000C + (channel * sizeof(short)))   // Beam-dump timer
#else

// Note that the multiplication by 4 (rather than sizeof(short)) of the offsets compensates for a bug in the FEI card's address decoding

#define FEI_FEI_TIMER(base, channel)    VME_INT16U_RW(base, (1 + (channel)) * 4)                    // FEI timer
#define FEI_BD_TIMER(base, channel)     VME_INT16U_RW(base, (6 + (channel)) * 4)                    // Beam-dump timer
#endif

// Card register structure

struct fei_regs
{
    volatile uint16_t       id_reset;                       // Card ID (read) and reset (write) register
    volatile const uint16_t version;                        // Card firmware version
    volatile uint16_t       fei[FEI_NUM_FEI_OUTPUTS];       // FEI outputs
    volatile uint16_t       beam_dump[FEI_NUM_BD_OUTPUTS];  // Beam dump outputs
};

struct fei_card
{
    struct vme_map  reg_map;
    struct fei_regs *regs;
};

#ifdef MUGEFHW_FEI
#define MUGEFHW_FEI_EXT
#else
#define MUGEFHW_FEI_EXT extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef MUGEFHW_FEI
#endif

// External functions

MUGEFHW_FEI_EXT int     feiMap(struct fei_card *card);
MUGEFHW_FEI_EXT void    feiUnmap(struct fei_card *card);

#ifdef __cplusplus
}
#endif

#endif

// EOF
