/*
 *  Filename: mpv915-21.h
 *
 *  Purpose:  Declarations for accessing the MPV915-21 card
 *
 *  Author:   Stephen Page
 */

#ifndef MUGEFHW_MPV915_H
#define MUGEFHW_MPV915_H

#include <stdint.h>

#include <mugefhw/vme.h>

// Constants

#define MPV915_MEAS_DELAY_MS        2                       // Delay in milliseconds before a measurement is available to be read
#define MPV915_NUM_ADDRS            4                       // Number of possible addresses for MPV915-21 cards

#define MPV915_RAM_DATA_WIDTH       16                      // VME data width of RAM
#define MPV915_RAM_SIZE             0x20000                 // Size of VME mapped memory
#define MPV915_RAM_VME_ADDR_BASE    0x900000                // VME base address of RAM
#define MPV915_RAM_VME_ADDR_INC     0x020000                // Increment between possible RAM addresses
#define MPV915_RAM_VME_ADDR_MOD     VME_ADDR_MODIFIER_24    // VME address modifier of RAM

#define MPV915_REG_DATA_WIDTH       16                      // VME data width of registers
#define MPV915_REG_SIZE             0x200                   // Size of VME mapped memory
#define MPV915_REG_VME_ADDR_BASE    0xE000                  // VME base address of registers
#define MPV915_REG_VME_ADDR_INC     0x0200                  // Increment between possible register addresses
#define MPV915_REG_VME_ADDR_MOD     VME_ADDR_MODIFIER_16    // VME address modifier of registers

#define MPV915_NUM_ADCS             2                       // Number of ADCs on MPV915-21 card
#define MPV915_NUM_CHANS            32                      // Number of channels supported by MPV915-21 card

// Config/watchdog register constants

#define MPV915_CONF_NORMAL_TRIG     0x8000                  // Normal trigger mode (clear for soft trigger mode)
#define MPV915_CONF_ADV_INPUT       0x4000                  // Advance input source
#define MPV915_CONF_RESET_INPUT     0x2000                  // Reset input source
#define MPV915_CONF_MS_DIV_MASK     0x1800                  // MSC division mask
#define MPV915_CONF_CONV_IN_PROG    0x0004                  // Conversion in progress
#define MPV915_CONF_SCC_SEEN        0x0002                  // Supercycle clock seen since normal trigger mode enabled
#define MPV915_CONF_SLOW_CONV       0x0001                  // Slow conversion detected

// Input numbers

#define MPV915_INPUT_NORMAL         0                       // Normal input
#define MPV915_INPUT_ZERO           1                       // Zero reference input
#define MPV915_INPUT_POS            2                       // Positive reference input
#define MPV915_INPUT_NEG            3                       // Negative reference input

// Calibration constants

#define MPV915_CAL_ZERO             32768.25                            // Zero reference calibration target
#define MPV915_CAL_POS              65527.75                            // Positive reference calibration target
#define MPV915_CAL_NEG              8.75                                // Negative reference calibration target
#define MPV915_CAL_RANGE            (MPV915_CAL_POS - MPV915_CAL_NEG)   // Calibration target range
#define MPV915_CAL_NUM_AVGS         10                                  // Number of points to average over for calibrations
#define MPV915_CAL_NUM_ITERATIONS   2                                   // Number of calibration iterations to perform
#define MPV915_CAL_DEFAULT_GAIN     33004                               // Default gain in case of calibration failure
#define MPV915_CAL_DEFAULT_OFFSET   48166                               // Default offset in case of calibration failure

// Calibration will fail if these thresholds are exceeded

#define MPV915_CAL_NEG_MAX          20                      // Maximum value for negative reference after calibration
#define MPV915_CAL_NEG_MIN          1                       // Minimum value for negative reference after calibration
#define MPV915_CAL_ZERO_MAX         32800                   // Maximum value for zero reference after calibration
#define MPV915_CAL_ZERO_MIN         32740                   // Minimum value for zero reference after calibration
#define MPV915_CAL_POS_MAX          65534                   // Maximum value for positive reference after calibration
#define MPV915_CAL_POS_MIN          65500                   // Minimum value for positive reference after calibration

// Status register constants

#define MPV915_STATUS_IDX_MASK      0xFFFE                  // Acquisition index in buffer
#define MPV915_STATUS_PAGE_MASK     0x0001                  // Acquisition page

// Window register constants

#define MPV915_WINDOW_CHAN_MASK     0x001F                  // Channel number
#define MPV915_WINDOW_PAGE_SHIFT    5                       // Shift for page selection

#define MPV915_DELAY_REG_WRITE_NS   2000                    // Delay to apply after writing a register

// Card registers

struct mpv915_regs
{
    volatile uint16_t       a24_offset;     // Control of offset of data mapped into A24 space
    volatile uint16_t       status;         // Acquisition status
    volatile uint16_t       window;         // Data buffer and channel selection
    volatile uint16_t       config;         // Input and trigger configuration
    volatile uint16_t       adc0_gain;      // ADC 0 gain
    volatile uint16_t       adc0_offset;    // ADC 0 offset
    volatile uint16_t       adc1_gain;      // ADC 1 gain
    volatile uint16_t       adc1_offset;    // ADC 1 offset
    volatile uint16_t       soft_trigger;   // Soft trigger
    volatile const uint16_t reference;      // Low bytes of +REF and GND references
    volatile const uint16_t reserved[236];
};

struct mpv915_card
{
    struct vme_map          ram_map;
    struct vme_map          reg_map;
    struct mpv915_regs      *regs;
    volatile const uint16_t *ram;
};

// Structure representing results of a calibration

struct mpv915_cal_results
{
    uint16_t    adc_gain[MPV915_NUM_ADCS];      // Calibrated gain
    uint16_t    adc_offset[MPV915_NUM_ADCS];    // Calibrated offset

    float       neg[MPV915_NUM_ADCS];           // Measurement of negative reference
    float       pos[MPV915_NUM_ADCS];           // Measurement of positive reference
    float       zero[MPV915_NUM_ADCS];          // Measurement of zero reference
};

#ifdef MUGEFHW_MPV915
#define MUGEFHW_MPV915_EXT
#else
#define MUGEFHW_MPV915_EXT extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

// Static functions

#ifdef MUGEFHW_MPV915
static void         mpv915CalibrateGains(struct mpv915_card *card, struct mpv915_cal_results *cal_results);
static void         mpv915CalibrateOffsets(struct mpv915_card *card, struct mpv915_cal_results *cal_results);
static void         mpv915SetGains(struct mpv915_card *card, const uint16_t adc0_gain, const uint16_t adc1_gain);
static void         mpv915SetInput(struct mpv915_card *card, uint32_t input);
static void         mpv915SetOffsets(struct mpv915_card *card, const uint16_t adc0_offset, const uint16_t adc1_offset);
static inline void  mpv915SetReg(volatile uint16_t *reg, const uint16_t value);
static void         mpv915TriggerRead(struct mpv915_card *card, uint32_t num_samples, float *adc0, float *adc1);
#endif

// External functions

MUGEFHW_MPV915_EXT int  mpv915Calibrate(struct mpv915_card *card, struct mpv915_cal_results *cal_results);
MUGEFHW_MPV915_EXT void mpv915DelayNs(uint64_t delay_ns);
MUGEFHW_MPV915_EXT int  mpv915Map(struct mpv915_card *card, int card_num);
MUGEFHW_MPV915_EXT void mpv915Unmap(struct mpv915_card *card);

#ifdef __cplusplus
}
#endif

#endif

// EOF
