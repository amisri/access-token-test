#ifndef TEST_SIMPLE_H
#define TEST_SIMPLE_H

#include <stdio.h>

unsigned long test_simple_num_tests = 0;
unsigned long test_simple_failed_tests = 0;

//!Checks if the condition is fullfilled
#define TEST_OK(cond) \
    do  \
    {  \
        test_simple_num_tests++;  \
        if (!(cond))  \
        {  \
            printf("not ok %lu - " #cond,test_simple_num_tests);  \
            printf("\n");  \
            printf(" at %s line %d\n",__FILE__,__LINE__);  \
            test_simple_failed_tests++;  \
        }  \
        fflush(stdout);  \
    } while(0)

#define TEST_RESULT  (test_simple_failed_tests != 0)

#endif
