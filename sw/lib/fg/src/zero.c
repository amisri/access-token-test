/*---------------------------------------------------------------------------------------------------------*\
  File:     table.c                                                                     Copyright CERN 2014

  License:  This file is part of libfg.

            libfg is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Purpose:  Generate linearly interpolated table functions
\*---------------------------------------------------------------------------------------------------------*/


#include "libfg/zero.h"



enum fg_error fgZeroArm(struct 	fg_meta *meta)
{
    enum    fg_error  fg_error = FG_OK;                     // Reference limits status
    struct  fg_meta   local_meta;                           // Local meta data in case user meta is NULL
    float   init_ref  = 0.0;

    fgResetMeta(meta, &local_meta, init_ref);             // Reset meta structure - uses local_meta if meta is NULL

	return fg_error;
}



uint32_t fgZeroGen(float *ref)
{
	
	*ref = 0.0;

	return 1; 
}

