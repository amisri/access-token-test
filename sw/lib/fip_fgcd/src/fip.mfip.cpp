/*!
 * @file   fip_mfip.cpp
 * @brief  FIP Device Manager layer
 * @author Michael Davis
 *
 * The functions in this file assume that there is a single WorldFIP segment, and all devices connected to it
 * (except this one) have nanoFIP controllers. This means that:
 *
 * - this device will be bus master
 * - periodic variables are implemented
 * - aperiodic variables, periodic messages and aperiodic messages are not implemented
 *
 * To develop this into library code, it should be extended to include messages and aperiodic variables (not
 * required for FGClite).
 */

#include <cstdarg>
#include <sys/time.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

// FIP includes

#include <fip_errno.h>
#include <fip.mfip.h>

// Types

/*!
 * FIP macrocycle definition.
 * See "FIP DEVICE MANAGER Software Version 4 User Reference Manual", p.3-59 to 3-63 for details.
 */

struct FIP_BA_Cycle
{
    bool                      is_initialised;       // Flag to indicate whether the FIP macrocycle configuration has been initialised
    bool                      is_running;
    struct mstrfip_macrocycle *mcycle;              // macro cycle object
    struct mstrfip_data       **per_varlist;        // Sorted list of all periodic variables
    uint32_t                  n_per_var;            // No. of variables to be created
    struct mstrfip_data	      **cons_per_varlist;   // Sorted list of consumed variables
    uint32_t                  n_cons_per_var;       // No. of variables to be created
    struct mstrfip_data	      **prod_per_varlist;   // Sorted list of produced variables
    uint32_t                  n_prod_per_var;       // No. of variables to be created
};

/*!
 * @brief FIP Device Manager structure
 */

struct FIP_MFIP
{
    bool                       is_initialised;        // Flag to indicate whether the FDM configuration has been initialised
    bool                       is_running;            // Flag to indicate that the interface is running

    pthread_mutex_t            fip_mutex;             // Mutex to protect starting and stopping FIP interface
    pthread_mutex_t            ba_mutex;              // Mutex to protect starting and stopping BA program

    struct mstrfip_dev         *dev;                  // MasterFip device
    struct FIP_config          config;                // FIP configuration

    struct FIP_BA_Cycle        ba;                    // FIP macrocycle definition
    uint32_t                   cycle_count;           // FIP cycle count

    // Device statuses

    uint8_t                    device_configured[32]; // One bit per WorldFIP address. 0 is not configured, 1 is configured. 32x8 bits = 256 addresses, to match the addressing model of Diamon.
    uint8_t                    device_present[32];    // Presence bits, one bit per WorldFIP address, as above.

    // Statistics

    struct FIP_fieldbus_stats  stats_default;         // Default data storage for fieldbus statistics
};

// Constants

const uint32_t                LOG_MESSAGE_MAX_LEN          = 512;                       // Maximum length of a log message

// Variables for FDM_Configuration_Soft - these can be changed in required using fipFdmSetConfigSoft()

static enum mstrfip_bitrate   FIP_BUS_SPEED                = MSTRFIP_BITRATE_2500;       //!< FGC app expects 2.5Mb/s FIP bus

static uint32_t               FIP_TURNAROUND_USTIME        = 13;                     // Minimum turnaround time -


// Constants for FDM_Configuration_Soft

// Variables for FDM_Configuration_Hard - these can be changed in required using fipFdmSetConfigHard()

static uint16_t               FIP_MEZZANINE_NR             = 0;                         // FIP logical unit card number, e.g. 1 for fip_u01



// Global variables

struct FIP_MFIP fip_mfip = {};    // initialise all values to zero



// FDM error and warning callback functions

static void fipMfipInterfaceFatalError(struct mstrfip_dev *dev,
				   enum mstrfip_error_list error);


// Inline functions

/*
 * FDM Software configuration
 */

static void fipMfipInitSoft(void)
{
    fip_mfip.config.sw_config.mstrfip_error_handler   = fipMfipInterfaceFatalError;
    fip_mfip.config.sw_config.irq_thread_prio = FIP_DEFAULT_THREAD_PRIORITY;
}



/*
 * FDM Hardware configuration
 */

static void fipMfipInitHard(void)
{
    fip_mfip.config.hw_config.enable_ext_trig = 1;      /* Enable external trigger */
    fip_mfip.config.hw_config.enable_int_trig = 0;      /* Disable internal trigger */
    fip_mfip.config.hw_config.enable_ext_trig_term = 1; /* No 50ohms term on external trigger */
    fip_mfip.config.hw_config.turn_around_ustime = FIP_TURNAROUND_USTIME;
}



// Static functions



/*
 * Callback on interface fatal error.
 *
 * When the FDM detects a FATAL_ERROR, this callback function is called. Error codes are defined in enum Code_Error
 * in fdm.h. See "FIP DEVICE MANAGER Software Version 4 User Reference Manual", Chapter 5 for a detailed description
 * of error codes.
 */

static void fipMfipInterfaceFatalError(struct mstrfip_dev *dev,
				   enum mstrfip_error_list error)
{
     
    if(fip_mfip.config.fieldbus_stats->last_interface_error != static_cast<uint32_t>(error))
    {
        fipMfipLogError("fipInterfaceFatalError: %s (error = %u)\n", mstrfip_strerror(error), error);
    }

    fip_mfip.config.fieldbus_stats->error_flag           = 1;
    fip_mfip.config.fieldbus_stats->last_interface_error = error;
    fip_mfip.config.fieldbus_stats->interface_error_count++;
}



/*
 * Initialise MPS command and status variables.
 * See "FIP DEVICE MANAGER Software Version 4 User Reference Manual", p.3-32 to 3-35
 */

static int32_t fipMfipCreatePerVars(const struct fgc_fip_mcycle_desc *mcycle_desc,
			        uint32_t nentries)
{
    struct mstrfip_data *per_var;
    struct mstrfip_data_cfg var_cfg;
    int idx_var = 0, idx_cons_var = 0, idx_prod_var = 0;

    for (uint32_t i = 0; i < nentries; ++i) {
    	if (mcycle_desc[i].type == FGC_FIP_PER_VAR_WIND) {
	    const struct fgc_fip_per_var_wind_desc *pwind = &(mcycle_desc[i].per_var_wind);
	    for (int j = 0; j < pwind->nvar; ++j) {
	    	var_cfg.id = pwind->varlist[j].id;
		var_cfg.flags = (pwind->varlist[j].dir == FGC_FIP_PER_VAR_CONS) ?
			      MSTRFIP_DATA_FLAGS_CONS : MSTRFIP_DATA_FLAGS_PROD;
	        var_cfg.max_bsz = pwind->varlist[j].bsz;
		var_cfg.mstrfip_data_handler = pwind->varlist[j].cb;
		per_var = mstrfip_var_create(fip_mfip.ba.mcycle, &var_cfg);
		if (per_var == NULL) {
		    fipMfipLogError("%s() creating periodic var id 0x%x failed: %s\n",
				__func__, var_cfg.id, strerror(errno));
		    return -1;
		}
		// Set the data_ptr pointing to the fip var buffer
		*pwind->varlist[j].data_ptr = per_var->buffer;

		// register the pointer to the new created var
		fip_mfip.ba.per_varlist[idx_var] = per_var;
		++idx_var;
		if (var_cfg.flags == MSTRFIP_DATA_FLAGS_CONS) {
		    fip_mfip.ba.cons_per_varlist[idx_cons_var] = per_var;
		    ++idx_cons_var;
		}
		else {
		    fip_mfip.ba.prod_per_varlist[idx_prod_var] = per_var;
		    ++idx_prod_var;
		}
	    }
	}
    }

    return 0;
}


static int32_t fipMfipBuildMacrocycle(const struct fgc_fip_mcycle_desc *mcycle_desc,
                                  uint32_t nentries)
{
    struct mstrfip_per_var_wind_cfg pwind_cfg;
    int res;

    for (uint32_t i = 0, idx_var = 0; i < nentries; ++i) {
    	switch (mcycle_desc[i].type) {
	case FGC_FIP_PER_VAR_WIND:
    	    /*
	     * per_varlist contains all periodic variables to be played during
	     * a macrocycle, sorted in the right order.
	     * According to the macro cycle description, we build the required
	     * periodic window giving the right entry in the per_varlist and of
	     * course the number of periodic var for this window.
	     */
	    pwind_cfg.varlist = &fip_mfip.ba.per_varlist[idx_var];
    	    pwind_cfg.var_count = mcycle_desc[i].per_var_wind.nvar;
	    idx_var += mcycle_desc[i].per_var_wind.nvar; // increment idx for next window
            res = mstrfip_per_var_wind_append(fip_mfip.ba.mcycle, &pwind_cfg);
	    if (res < 0) {
   	        fipMfipLogError("%s() appending periodic window failed: %s\n",
			    __func__, strerror(errno));
		return -1;
	    }
	    break;

	case FGC_FIP_APER_MSG_WIND:
	    // Not implemented
	    break;

	case FGC_FIP_WAIT_WIND :
	    res = mstrfip_wait_wind_append(fip_mfip.ba.mcycle,
	    				   mcycle_desc[i].wait_wind.silent,
					   mcycle_desc[i].wait_wind.end_ustime);
	    if (res < 0) {
   	        fipMfipLogError("%s() appending wait window failed: %s\n",
			    __func__, strerror(errno));
		return -1;
	    }
	    break;
	}
    }
    return 0;
}


/*
 * One-time FIP FDM initialisation
 */

static int32_t fipMfipDefaultInit(void)
{
    pthread_mutexattr_t mutex_attr;

    // Statically-initialised mutex for one-time initialisation

    static pthread_mutex_t init_lock = PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&init_lock);

    if(fip_mfip.is_initialised)
    {
        errno = INIT_ALREADY_INITIALISED;
        pthread_mutex_unlock(&init_lock);
        return 0;
    }

    // Initialise FIP FDM mutex

    if(pthread_mutexattr_init(&mutex_attr) != 0)
    {
        errno = INIT_MUTEX_FAILED;
        pthread_mutex_unlock(&init_lock);
        return -1;
    }

    if(pthread_mutexattr_setprotocol(&mutex_attr, PTHREAD_PRIO_INHERIT) != 0  ||
       pthread_mutex_init(&fip_mfip.fip_mutex, &mutex_attr)              != 0  ||
       pthread_mutex_init(&fip_mfip.ba_mutex,  &mutex_attr)              != 0)
    {
        pthread_mutexattr_destroy(&mutex_attr);
        errno = INIT_MUTEX_FAILED;
        pthread_mutex_unlock(&init_lock);
        return -1;
    }
    pthread_mutexattr_destroy(&mutex_attr);

    // Initialise FIP FDM configuration

    fipMfipInitSoft();
    fipMfipInitHard();

    fip_mfip.config.thread_priority = FIP_DEFAULT_THREAD_PRIORITY;
    fip_mfip.config.fieldbus_stats  = &fip_mfip.stats_default;
    fip_mfip.is_initialised         = true;

    // Register the front-end and diagnostic device as configured

    fipMfipConfigureDevice(0);
    fipMfipConfigureDevice(FIP_DIAG_DEV_ADDR);

    errno = INIT_SUCCESS;

    pthread_mutex_unlock(&init_lock);

    return 0;
}

static int32_t fipMfipOpenDevice()
{
    int res;

    res = mstrfip_init();
    if (res)
    {
        fprintf(stderr, "ERROR: Cannot init fip library: %s\n",
		mstrfip_strerror(errno));
	return res;
    }

    fip_mfip.dev = mstrfip_open_by_lun(FIP_MEZZANINE_NR);
    if(fip_mfip.dev == NULL)
    {
        fprintf(stderr, "ERROR: Cannot open masterfip dev: %s\n",
	        mstrfip_strerror(errno));
        return res;
    }

    /* reset mockturtle RT app */
    if (mstrfip_rtapp_reset(fip_mfip.dev) < 0)
    {
        fprintf(stderr, "ERROR: Cannot reset rt app: %s\n",
		mstrfip_strerror(errno));
	return res;
    }
    return 0;
}


static int32_t fipMfipReadBusSpeed()
{
    int res;
    enum mstrfip_bitrate speed;
    char *speed_str;

    res = mstrfip_hw_speed_get(fip_mfip.dev, &speed);
    if (res)
    {
	fprintf(stderr, "ERROR: Can't get FIP speed: %s. Exit\n",
		mstrfip_strerror(errno));
	return res;
    }

    if (speed != FIP_BUS_SPEED)
    {
        switch (speed)
        {
	case MSTRFIP_BITRATE_31:
	   speed_str = (char *)"31.25 kb/s";
	   break;
	case MSTRFIP_BITRATE_1000:
	   speed_str = (char *)"1 Mb/s";
	   break;
	case MSTRFIP_BITRATE_2500:
	   speed_str = (char *)"2.5 Mb/s";
	   break;
	case MSTRFIP_BITRATE_UNDEFINED:
	   speed_str = (char *)"Undefined speed";
	   break;
        default: // This case should never occur
	   speed_str = (char *)"Unexpected speed returned by mstrfip_hw_speed_get()";
	   break;
	}

        fprintf(stderr, "ERROR: Expected FIP bus speed 2.5 Mb/s and got %s\n",
				speed_str);
	return 1;
    }
    return 0;
}

// External functions

void fipMfipSetConfigHard(const uint16_t fip_mezzanine_nr,
		      const uint32_t fip_turnaround_ustime)
{
    FIP_MEZZANINE_NR = fip_mezzanine_nr;

    if(fip_turnaround_ustime > 0)
    {
        FIP_TURNAROUND_USTIME = fip_turnaround_ustime;
    }
}



void fipMfipResetFieldbusStats(struct FIP_fieldbus_stats *stats)
{
    stats->interface_error_count = 0;
}



void fipMfipResetDeviceStats(struct FIP_device_stats *stats)
{
    stats->var_recv_count              = 0;
    stats->var_miss_count              = 0;
    stats->var_late_count              = 0;
    stats->nonsignificance_fault_count = 0;
    stats->promptness_fail_count       = 0;
    stats->significance_fault_count    = 0;
    stats->user_error_count            = 0;
}



int32_t fipMfipGetTime(struct timeval *timeNow)
{
    if(fip_mfip.config.read_time_func == NULL)
    {
        gettimeofday(timeNow, NULL);
    }
    else
    {
        fip_mfip.config.read_time_func(timeNow);
    }

    return timeNow->tv_sec;
}



int32_t fipMfipLogError(const char *format, ...)
{
    va_list     args;
    char buffer[LOG_MESSAGE_MAX_LEN];
    int32_t     length;

    // Write to stderr by default

    if(!fip_mfip.is_initialised || fip_mfip.config.log_error_func == NULL)
    {
        va_start(args, format);
        length = vfprintf(stderr, format, args);
        va_end(args);

        return length;
    }

    // Write variable arguments into the buffer

    va_start(args, format);
    length = vsnprintf(buffer, LOG_MESSAGE_MAX_LEN - 4, format, args);
    va_end(args);

    if(length == LOG_MESSAGE_MAX_LEN - 4)
    {
        // Replace final characters of message with ellipsis to indicate that the message is incomplete

        strcpy(buffer + LOG_MESSAGE_MAX_LEN - 5, "...\n");
    }

    if(length > 0)
    {
        length = fip_mfip.config.log_error_func(buffer);
    }

    return length;
}



int32_t fipMfipGetConfig(struct FIP_config *config)
{
    if(fipMfipDefaultInit() != 0) return -1;

    pthread_mutex_lock(&fip_mfip.fip_mutex);
    memcpy(config, &fip_mfip.config, sizeof(struct FIP_config));
    pthread_mutex_unlock(&fip_mfip.fip_mutex);

    return 0;
}



int32_t fipMfipSetConfig(struct FIP_config *config)
{
    if(fipMfipDefaultInit() != 0) return -1;

    pthread_mutex_lock(&fip_mfip.fip_mutex);

    // It is not possible to reconfigure a running interface

    if(fip_mfip.is_running)
    {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
        errno = INIT_ALREADY_RUNNING;
        return -1;
    }

    memcpy(&fip_mfip.config, config, sizeof(struct FIP_config));

    pthread_mutex_unlock(&fip_mfip.fip_mutex);

    return 0;
}



int32_t fipMfipStartInterface(void)
{
    struct timeval timeNow;

    // One-time initialisation

    if(!fip_mfip.is_initialised)
    {
        if(fipMfipDefaultInit() != 0) return -1;
    }

    // Protect starting and stopping

    pthread_mutex_lock(&fip_mfip.fip_mutex);

    // Check whether interface should already be running

    if(fip_mfip.is_running)
    {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
        errno = INIT_ALREADY_RUNNING;
        return -1;
    }

    // Increment start count, set start time and reset error flag

    fip_mfip.config.fieldbus_stats->start_count++;
    fip_mfip.config.fieldbus_stats->start_time = fipMfipGetTime(&timeNow);

    fip_mfip.config.fieldbus_stats->error_flag = 0;
    fip_mfip.config.fieldbus_stats->last_interface_error = 0;

    // Open masterFIP device. Returns 0 on success. Returns -1 and sets errno on failure.
    if (fipMfipOpenDevice()) {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
    	return -1;
    }

    // Check the FIP bus speed: 2.5MB/s is expected
    if (fipMfipReadBusSpeed()) {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
    	return -1;
    }

    // Send Sw config to the masterFIP device
    if (mstrfip_sw_cfg_set(fip_mfip.dev, &fip_mfip.config.sw_config) < 0) {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
    	return -1;
    }

    // Send HW config to the masterFIP device
    if (mstrfip_hw_cfg_set(fip_mfip.dev, &fip_mfip.config.hw_config) < 0) {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
    	return -1;
    }


    // Set run flag

    fip_mfip.is_running = true;

    errno = INIT_SUCCESS;

    pthread_mutex_unlock(&fip_mfip.fip_mutex);

    return 0;
}


void fipMfipStopInterface(void)
{
    if(!fip_mfip.is_initialised) return;

    // Protect starting and stopping

    pthread_mutex_lock(&fip_mfip.fip_mutex);

    // Check whether the interface is already stopped

    if(!fip_mfip.is_running)
    {
        pthread_mutex_unlock(&fip_mfip.fip_mutex);
        return;
    }

    fip_mfip.is_running = false;

    // Stop BA macrocycle program

    fipMfipStopProtocol();

    pthread_mutex_unlock(&fip_mfip.fip_mutex);
}



int32_t fipMfipRestartInterface(void)
{
    int32_t result;
    int32_t cancel_state_orig;

    static pthread_mutex_t restart_mutex = PTHREAD_MUTEX_INITIALIZER;

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&restart_mutex);

    fipMfipStopInterface();

    result = fipMfipStartInterface();
    if(result == 0)
    {
        result = fipMfipStartProtocol();
    }

    pthread_mutex_unlock(&restart_mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return result;
}



int32_t fipMfipInitProtocol(const struct fgc_fip_mcycle_desc *mcycle_desc,
			uint32_t nentries)
{
    int res;

    // clean up if this is a redefinition

    if(fip_mfip.ba.is_initialised)
    {
        delete[] fip_mfip.ba.per_varlist;        // Sorted list of all periodic variables
        delete[] fip_mfip.ba.cons_per_varlist;   // Sorted list of consumed variables
        delete[] fip_mfip.ba.prod_per_varlist;   // Sorted list of produced variables
    	fip_mfip.ba.n_per_var = 0;
    	fip_mfip.ba.n_cons_per_var = 0;
    	fip_mfip.ba.n_prod_per_var = 0;
    }

    // Create macro cycle object
    fip_mfip.ba.mcycle = mstrfip_macrocycle_create(fip_mfip.dev);
    if (fip_mfip.ba.mcycle == NULL) 
    {
        return -1;
    }

    // Count no. of periodic var consumed and produced
    for (uint32_t i = 0; i < nentries; ++i) {
    	if (mcycle_desc[i].type == FGC_FIP_PER_VAR_WIND) {
	    for (int j = 0; j < mcycle_desc[i].per_var_wind.nvar; ++j) {
	        (mcycle_desc[i].per_var_wind.varlist[j].dir == FGC_FIP_PER_VAR_CONS) ?
		++fip_mfip.ba.n_cons_per_var : ++fip_mfip.ba.n_prod_per_var;
	    }
	}
    }
    // total number of periodic variables is the sum
    fip_mfip.ba.n_per_var = fip_mfip.ba.n_cons_per_var + fip_mfip.ba.n_prod_per_var;

    // Allocate arrays for macrocycle variables
    fip_mfip.ba.per_varlist = new mstrfip_data*[fip_mfip.ba.n_per_var];
    fip_mfip.ba.cons_per_varlist = new mstrfip_data*[fip_mfip.ba.n_cons_per_var];
    fip_mfip.ba.prod_per_varlist = new mstrfip_data*[fip_mfip.ba.n_prod_per_var];

    // initialise periodic variables
    res = fipMfipCreatePerVars(mcycle_desc, nentries);
    if (res == -1)
    	return res;

    // Build instruction lists in FIP FDM format
    res = fipMfipBuildMacrocycle(mcycle_desc, nentries);
    if (res == -1)
    	return res;

    fip_mfip.ba.is_initialised = true;
    return 0;
}



int32_t fipMfipStartProtocol(void)
{
    // Check that initialisation has been performed

    if(!fip_mfip.is_initialised || !fip_mfip.ba.is_initialised)
    {
        errno = INIT_NOT_INITIALISED;
        return -1;
    }

    // Protect starting and stopping

    pthread_mutex_lock(&fip_mfip.ba_mutex);

    if(!fip_mfip.is_running)
    {
        pthread_mutex_unlock(&fip_mfip.ba_mutex);
        errno = INIT_NOT_RUNNING;
        return -1;
    }

    if(fip_mfip.ba.is_running)
    {
        pthread_mutex_unlock(&fip_mfip.ba_mutex);
        errno = INIT_ALREADY_RUNNING;
        return -1;
    }

    if (mstrfip_ba_load(fip_mfip.dev, fip_mfip.ba.mcycle) < 0)
    {
        pthread_mutex_unlock(&fip_mfip.ba_mutex);
        fipMfipStopProtocol();
        errno = INIT_FDM_BA_FAILED;
	return -1;
    }

    if (mstrfip_ba_start(fip_mfip.dev) < 0)
    {
        pthread_mutex_unlock(&fip_mfip.ba_mutex);
        fipMfipStopProtocol();
        errno = INIT_FDM_BA_FAILED;
	return -1;
    }
    fip_mfip.ba.is_running = true;

    // Reset the cycle count

    fip_mfip.cycle_count = 0;

    // Callback to indicate BA macrocycle program is running

    if(fip_mfip.config.ba_up_func != NULL)
    {
        fip_mfip.config.ba_up_func();
    }

    // Set the status of the front-end and the diagnostic device to present on the WorldFIP bus

    fipMfipSetDevicePresent(0);

    errno = INIT_SUCCESS;

    pthread_mutex_unlock(&fip_mfip.ba_mutex);

    return 0;
}



void fipMfipStopProtocol()
{
    if(!fip_mfip.is_initialised) return;

    // Protect starting and stopping

    pthread_mutex_lock(&fip_mfip.ba_mutex);

    // Stop the bus arbitrator
    mstrfip_ba_reset(fip_mfip.dev);
    fip_mfip.ba.is_running = false;

    // Callback to indicate BA macrocycle program has stopped running

    if(fip_mfip.config.ba_down_func != NULL)
    {
        fip_mfip.config.ba_down_func();
    }

    // Set the status of the front-end and the diagnostic device to not present on the WorldFIP bus

    fipMfipSetDeviceNotPresent(0);

    pthread_mutex_unlock(&fip_mfip.ba_mutex);
}



int32_t fipMfipRestartProtocol(void)
{
    int32_t result;
    int32_t cancel_state_orig;

    static pthread_mutex_t restart_mutex = PTHREAD_MUTEX_INITIALIZER;

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_orig);
    pthread_mutex_lock(&restart_mutex);

    fipMfipStopProtocol();
    result = fipMfipStartProtocol();

    pthread_mutex_unlock(&restart_mutex);
    pthread_setcancelstate(cancel_state_orig, &cancel_state_orig);

    return result;
}



void fipMfipWriteVar(uint32_t id_dat)
{
    uint32_t idx;

    if(!fip_mfip.ba.is_running)
    {
        fipMfipLogError("Attempt to write FIP variable while BA program is not running.\n");
        return;
    }

    // Find the variable with this ID

    for(idx = 0; idx < fip_mfip.ba.n_prod_per_var; ++idx)
    {
        if(fip_mfip.ba.prod_per_varlist[idx]->id == (int)id_dat) break;
    }

    // Validate

    if(fip_mfip.ba.prod_per_varlist[idx]->id != (int)id_dat)
    {
        fipMfipLogError("Variable not found with ID_DAT = 0x%x.\n", id_dat);
        return;
    }

    // Write the variable

    if ( (fip_mfip.config.fieldbus_stats->error_flag == 0) &&
	 (mstrfip_var_write(fip_mfip.dev, fip_mfip.ba.prod_per_varlist[idx]) != 0) )
    {
        fipMfipLogError("FIP Failed to write producer variable %u\n", idx);
        fip_mfip.config.fieldbus_stats->error_flag = 1;
    }
}



void fipMfipWriteVars(void)
{
    if(!fip_mfip.ba.is_running)
    {
        fipMfipLogError("Attempt to write FIP variables while BA program is not running.\n");
        return;
    }

    for(uint32_t idx = 0; idx < fip_mfip.ba.n_prod_per_var; ++idx)
    {
        if ( (fip_mfip.config.fieldbus_stats->error_flag == 0) &&
	     (mstrfip_var_write(fip_mfip.dev, fip_mfip.ba.prod_per_varlist[idx]) != 0) )
        {
            fipMfipLogError("FIP Failed to write producer variable %u\n", idx);
            fip_mfip.config.fieldbus_stats->error_flag = 1;
        }
    }
}



bool fipMfipReadVar(struct mstrfip_data *data, FIP_device_stats *error_stats)
{
    if(!fip_mfip.ba.is_running)
    {
        fipMfipLogError("Attempt to read FIP variables while BA program is not running.\n");
        return false;
    }

    mstrfip_var_update(fip_mfip.dev, data); // update the periodic status variable

    switch (data->status) {
        case MSTRFIP_DATA_OK:
            error_stats->var_recv_count++;
            fip_mfip.config.fieldbus_stats->errors.var_recv_count++;
            return true;

        case MSTRFIP_DATA_PAYLOAD_ERROR:
            if(data->payload_error & MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT) {
                error_stats->nonsignificance_fault_count++;
                fip_mfip.config.fieldbus_stats->errors.nonsignificance_fault_count++;
                error_stats->significance_fault_count++;
                fip_mfip.config.fieldbus_stats->errors.significance_fault_count++;
    	    }
	    if(data->payload_error & MSTRFIP_FRAME_PAYLOAD_NOT_REFRESH) {
                error_stats->promptness_fail_count++;
                fip_mfip.config.fieldbus_stats->errors.promptness_fail_count++;
            }
            break;

        case MSTRFIP_DATA_FRAME_ERROR:
    	    if (data->frame_error == MSTRFIP_FRAME_TMO) {
                // A timeout is ignored and handled at applicaiton level
    	    }
	       else {
                error_stats->user_error_count++;
                fip_mfip.config.fieldbus_stats->errors.user_error_count++;
            }
	    break;
	default:
	    break;
    }

    return false;
}



bool fipMfipIsInterfaceUp(void)
{
    return fip_mfip.is_running;
}



uint32_t fipMfipGetStartTime(void)
{
    return fip_mfip.config.fieldbus_stats->start_time;
}



uint32_t fipMfipCycleTick(void)
{
    return ++fip_mfip.cycle_count;
}



uint32_t fipMfipGetCycleCount(void)
{
    return fip_mfip.cycle_count;
}



void fipMfipConfigureDevice(uint8_t address)
{
    fip_mfip.device_configured[address/8] |= 1 << (address % 8);
}

void fipMfipUnconfigureDevice(uint8_t address)
{
    fip_mfip.device_configured[address/8] &= ~(1 << (address % 8));
}


void fipMfipGetAllConfigured(uint8_t *ptr)
{
    memcpy(ptr, fip_mfip.device_configured, 32);
}



bool fipMfipIsDeviceConfigured(uint8_t address)
{
    return fip_mfip.device_configured[address/8] & (1 << (address % 8));
}


void fipMfipSetDevicePresent(uint8_t address)
{
    fip_mfip.device_present[address/8] |= 1 << (address % 8);
}



void fipMfipSetDeviceNotPresent(uint8_t address)
{
    fip_mfip.device_present[address/8] &= ~(1 << (address % 8));
}



bool fipMfipIsDevicePresent(uint8_t address)
{
    return fip_mfip.device_present[address/8] & (1 << (address % 8));
}



void fipMfipGetAllPresent(uint8_t *ptr)
{
    memcpy(ptr, fip_mfip.device_present, 32);
}

// EOF
