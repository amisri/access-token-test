/*!
 * @file   fip_watchdog.cpp
 * @brief  Functions for the %watchdog thread
 * @author Stephen Page
 * @author Michael Davis
 */

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

// FIP FDM includes

#include <fip.mfip.h>
#include <fip_diamon.h>
#include <fip_watchdog.h>



// Constants

const uint16_t WATCHDOG_PERIOD_MS = 5000;    //!< Period between watchdog checks in milliseconds



// Types

/*!
 * Type for casting pthread start functions
 */

typedef void *(*ThreadRunFunctionPtr)(void *);



/*!
 * Struct containing global variables
 */

struct Watchdog
{
    bool      is_running;         //!< Flag to indicate that the thread should run
    pthread_t thread;             //!< Thread handle
};



// Global variables

struct Watchdog watchdog = {};    // initialise all values to zero



// Static function definitions

/*
 * Watchdog thread
 */

static void watchdogRun(void)
{
    uint32_t cycle_count;

    watchdog.is_running = true;

    while(watchdog.is_running)
    {
        cycle_count = fipMfipGetCycleCount();

        // Wait

        msSleep(WATCHDOG_PERIOD_MS);

        // Check if WorldFIP cycle count has incremented

        if(watchdog.is_running && cycle_count == fipMfipGetCycleCount())
        {
            // Restart WorldFIP interface

            fipMfipLogError("watchdogRun(): Restarting WorldFIP interface\n");
            fipMfipRestartInterface();
        } 

        // Update Diamon WorldFIP data

        fipDiamonUpdate();
    }
}



// External function definitions

int32_t watchdogStart(void)
{
    pthread_attr_t      thread_attr;    // Thread attributes
    struct sched_param  thread_param;   // Thread scheduling parameters

    // Statically-initialised mutex for one-time initialisation

    static pthread_mutex_t init_lock = PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&init_lock);

    // Check whether thread should already be running

    if(watchdog.is_running)
    {
        pthread_mutex_unlock(&init_lock);
        return -1;
    }

    // Initialise Diamon WorldFIP data

    fipDiamonInit();

    // Configure thread attributes

    if(pthread_attr_init(&thread_attr) == -1)
    {
        fipMfipLogError("watchdogStart(): pthread_attr_init\n");
        pthread_mutex_unlock(&init_lock);
        return -1;
    }

    pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&thread_attr, SCHED_FIFO);
    pthread_attr_setstacksize   (&thread_attr, FIP_THREAD_STACK_SIZE);

    sched_getparam(getpid(), &thread_param);
    thread_param.sched_priority = FIP_WATCHDOG_THREAD_PRIORITY;
    pthread_attr_setschedparam(&thread_attr, &thread_param);

    // Start watchdog thread

    if(pthread_create(&watchdog.thread, &thread_attr, reinterpret_cast<ThreadRunFunctionPtr>(watchdogRun), NULL) != 0)
    {
        fipMfipLogError("watchdogStart(): pthread_create\n");
        pthread_attr_destroy(&thread_attr);
        pthread_mutex_unlock(&init_lock);
        return -1;
    }
    pthread_attr_destroy(&thread_attr);

    pthread_mutex_unlock(&init_lock);

    return 0;
}



void watchdogStop(void)
{
    watchdog.is_running = false;

    // Clean-up Diamon WorldFIP data

    fipDiamonCleanUp();
}



// EOF
