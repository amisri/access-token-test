/*!
 * @file   fip_diamon.cpp
 * @brief  Functions for reporting WorldFIP status to Diamon
 * @author Stephen Page
 * @author Michael Davis
 */

#include <stdio.h>
#include <cerrno>
#include <stdint.h>
#include <sys/shm.h>
#include <pthread.h>

// FIP includes

#include <fip.mfip.h>
#include <fip_diamon.h>



// Constants

const key_t FIPDIAG_SHM_KEY = 0x46444730;    //!< Key for WorldFIP Diamon shared memory



/*!
 * Struct containing global variables
 */

struct FIPdiamon
{
    struct mstrfip_diag_shm *data;
};



// Global variables

struct FIPdiamon fipdiamon = {};    // initialised to zero



// External functions

int32_t fipDiamonInit(void)
{
    int32_t  shm_id;

    // Statically-initialised mutex for one-time initialisation

    static pthread_mutex_t init_lock = PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&init_lock);

    // Get shared memory ID

    shm_id = shmget(FIPDIAG_SHM_KEY, sizeof(*fipdiamon.data), 0666 | IPC_CREAT);

    if(shm_id == -1)
    {
        fipMfipLogError("fipDiamonInit(): shmget failed with error %d.\n", errno);
        pthread_mutex_unlock(&init_lock);
        return -1;
    }

    // Attach to shared memory

    fipdiamon.data = reinterpret_cast<mstrfip_diag_shm*>(shmat(shm_id, NULL, 0));

    if(fipdiamon.data == reinterpret_cast<mstrfip_diag_shm*>(-1))
    {
        fipMfipLogError("fipDiamonStop(): shmat failed with error %d.\n", errno);
        pthread_mutex_unlock(&init_lock);
        return -1;
    }

    // Set constant values within shared memory

    fipdiamon.data->start_time         = fipMfipGetStartTime();

    pthread_mutex_unlock(&init_lock);

    return 0;
}



void fipDiamonCleanUp(void)
{
    // Do nothing if shared memory not mapped

    if(fipdiamon.data == NULL) return;

    // Update shared memory

    fipDiamonUpdate();

    // Detach from shared memory

    shmdt(fipdiamon.data);
    fipdiamon.data = NULL;
}

void fipDiamonUpdate(void)
{
    uint16_t       status;
    struct timeval time;

    // Do nothing if shared memory not mapped

    if(!fipdiamon.data) return;

    // Set cycle time

    fipdiamon.data->last_cycle_date = fipMfipGetTime(&time);

    // Get current device statuses

    status = MSTRFIP_STATUS_DIAG_OK;

    // Copy configured and present statuses into shared memory

    fipMfipGetAllConfigured(fipdiamon.data->present_theoric_list);
    fipMfipGetAllPresent   (fipdiamon.data->present_list);

    // Copy absent status bits into FIP Diamon status, one byte at-a-time

    uint8_t *device_absent = fipdiamon.data->absent_list;

    for(uint8_t i = 0; i < 32; ++i)
    {
        // According to fipdiag.h, List_absent is the difference (XOR) of the other two

        device_absent[i] = fipdiamon.data->present_theoric_list[i] ^ fipdiamon.data->present_list[i];

        // If any absent bit (except the diagnostic device) is set, set status to AGENTMISSING

        if(device_absent[i] != 0)
        {
            if(i != FIP_DIAG_DEV_ADDR / 8 || device_absent[i] != (1 << (FIP_DIAG_DEV_ADDR % 8)))
                status = MSTRFIP_STATUS_AGT_MISS;
        }
    }

    // Check diagnostic device

    if((fipdiamon.data->present_list[FIP_DIAG_DEV_ADDR / 8] & (1 << (FIP_DIAG_DEV_ADDR % 8))) == 0)
    {
        if(status == MSTRFIP_STATUS_AGT_MISS)
            status = MSTRFIP_STATUS_DIAG_FAULT1;
        else
            status = MSTRFIP_STATUS_DIAG_HW_ERR;
    }

    // Check whether cycles have occurred since last update

    if(fipdiamon.data->cycle_count == fipMfipGetCycleCount())
    {
        // No cycles have occurred

        status = MSTRFIP_STATUS_DIAG_FAULT2;
    }
    fipdiamon.data->cycle_count = fipMfipGetCycleCount();

    // Set status summary

    fipdiamon.data->com_status = status;
}

// EOF
