/*!
 * @file   watchdog.h
 * @brief  Declarations for the FIP %watchdog thread
 * @author Stephen Page
 * @author Michael Davis
 *
 * While the watchdog is running, fipFdmCycleTick() must be called during each FIP cycle.
 * If the cycle counter is not updated for 5 seconds, the watchdog will restart the FIP
 * interface and BA macrocycle program.
 */

#ifndef __LIBFIPFDM_WATCHDOG_H
#define __LIBFIPFDM_WATCHDOG_H

#include <stdint.h>

// External function declarations

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Start the watchdog thread.
 *
 * @retval  0   if the watchdog thread starts successfully
 * @retval -1   if there is an initialisation error or the watchdog should already have been running
 */

int32_t watchdogStart(void);



/*!
 * Stop the watchdog thread.
 */

void watchdogStop(void);

#ifdef __cplusplus
}
#endif

#endif

// EOF
