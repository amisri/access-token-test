/*
 * @brief FIP library user function declarations.
 * @author Michael Davis
 *
 * The IRQ handlers activate the FIP Device Manager, which calls the user callback
 * functions declared below.
 *
 * This header file replaces the library-provided user_fct.h. It adds header guards,
 * C linkage declaration, complete function prototypes and documentation.
 */

#ifndef __USER_FCT_H
#define __USER_FCT_H

#include <fdm.h>

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @brief Called when the event corresponds to the reception or the transmission of a universal-type variable.
 */

void User_Signal_Mps_Aper(struct _FDM_REF *fdm_ref);



/*!
 * @brief Called when the event corresponds to the reception of one of the network management variables, following a request to read this variable.
 */

void User_Signal_Smmps(struct _FDM_REF *fdm_ref);



/*!
 * @brief Called when the event corresponds to the reception of a message.
 */

void User_Signal_Rec_Msg(struct _FDM_REF *fdm_ref);



/*!
 * @brief Called when the event corresponds to the reception of acknowledgement of transmission of a message or to a request for user transmission.
 */

void User_Signal_Send_Msg(struct _FDM_REF *fdm_ref);



#if 0

// These functions are defined in the original user_fct.h but are not used in the FIP FDM library

/*!
 * @brief Called when the event corresponds to the transmission of a produced variable with associated event (A_Sent).
 */

void User_Signal_Asent(struct _FDM_REF *fdm_ref);



/*!
 * @brief Called when the event corresponds to the reception of a consumed variable with associated event (A_Received).
 */

void User_Signal_Areceived(struct _FDM_REF *fdm_ref);



/*!
 * @brief Called when the event corresponds to the transmission of a produced variable of the universal type or at the time-out of the request.
 */

void User_Signal_Var_Prod(struct _FDM_REF *fdm_ref);



void User_Signal_Fatal_Error ();

void User_Signal_Warning ();

void User_Reset_Component ();

unsigned short User_Presence_Prog ();

void User_Present_List_Prog();

unsigned short User_Identification_Prog();

unsigned short User_Report_Prog();
#endif

#ifdef __cplusplus
}
#endif

#endif
