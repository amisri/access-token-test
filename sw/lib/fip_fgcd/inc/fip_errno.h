/*!
 * @file   fip_errno.h
 * @brief  Error codes for FIP FDM library functions
 * @author Michael Davis
 */

#ifndef __LIBFIPFDM_ERRNO_H
#define __LIBFIPFDM_ERRNO_H

/*!
 * Error codes for FIP FDM initialisation and startup functions
 */

enum FIP_errno
{
    INIT_SUCCESS,                        //!< The function completed successfully

    INIT_NOT_INITIALISED        = 1000,  //!< Initialisation relies on a prior initialisation step which has not been performed
    INIT_ALREADY_INITIALISED,            //!< Tried to initialise a data structure that has already been initialised
    INIT_NOT_RUNNING,                    //!< Tried to start a function that relies on a service which is not running
    INIT_ALREADY_RUNNING,                //!< Tried to start a function that is already running
    INIT_SEM_FAILED,                     //!< Failed to initialise a semaphore
    INIT_MUTEX_FAILED,                   //!< Failed to initialise a mutex
    INIT_PTHREAD_FAILED,                 //!< Failed to initialise pthreads
    INIT_QUEUE_FAILED,                   //!< Failed to initialise a queue data structure
    INIT_FDM_BA_FAILED,                  //!< Failed to initialise FIP FDM Bus Arbitrator (BA)
    INIT_FDM_AE_LE_FAILED,               //!< Failed to initialise FIP FDM Application Entity/Link Entity (AE/LE) layer
    INIT_FDM_MPS_FAILED                  //!< Failed to initialise FIP FDM MPS Variables
};

#endif

// EOF
