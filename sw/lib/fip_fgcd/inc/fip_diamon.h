/*!
 * @file   fip_diamon.h
 * @brief  Declarations for reporting WorldFIP status to Diamon
 * @author Stephen Page
 * @author Michael Davis
 */

#ifndef __LIBFIPFDM_FIPDIAMON_H
#define __LIBFIPFDM_FIPDIAMON_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Initialise shared memory for Diamon client.
 *
 * @retval  0    Initialisation successful
 * @retval -1    Shared memory error
 */

int32_t fipDiamonInit(void);



/*!
 * Clean up shared memory
 */

void fipDiamonCleanUp(void);



/*!
 * Update shared memory with device statuses
 */

void fipDiamonUpdate(void);

#ifdef __cplusplus
}
#endif

#endif

// EOF
