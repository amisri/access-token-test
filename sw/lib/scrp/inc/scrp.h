/*!
 * @filename scrp.h
 * @author   Stephen Page
 *
 * Declarations for the FGC Serial Command Response Protocol (SCRP)
 */

#ifndef SCRP_H
#define SCRP_H

#include <stdio.h>
#include <termios.h>

// Constants

#define SCRP_BAUD_RATE          B9600
#define SCRP_CMD_DIAG_MODE      "\031"
#define SCRP_CMD_DIRECT_MODE    "\032"
#define SCRP_CMD_EDITOR_MODE    "\033"

#include <fgc_stat.h>

#ifdef __cplusplus
extern "C" {
#endif

// External functions

/*!
 * Open a device for SCRP communication
 */

FILE *scrpOpen(char *device);



/*!
 * Close a device for SCRP communication
 */

void scrpClose(FILE *port);



/*!
 * Send a command
 *
 *
 * @retval  0    Success
 * @retval  1    The device returned an error
 * @retval -1    Communication failure
 */

int scrpCmd(FILE *port, char type, char *property, char *value, unsigned int value_size, unsigned int *value_length);



/*!
 * Receive diagnostic data
 *
 * @retval  0    Success
 * @retval -1    Communication failure
 */

int scrpDiag(FILE *port, struct fgc_diag *diag_data);

#ifdef __cplusplus
}
#endif

#endif

// EOF
