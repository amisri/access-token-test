/*!
 * @filename scrp.c
 * @author   Stephen Page
 *
 * Communicate using the FGC Serial Command Response Protocol (SCRP)
 * (see https://wikis.cern.ch/pages/viewpage.action?pageId=70682032)
 */

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "fgc_consts_gen.h"
#include "scrp.h"



FILE *scrpOpen(char *device)
{
    struct termios  options;
    FILE           *port;

    // Open the serial port

    if(!(port = fopen(device, "r+")))
    {
        // Failed to open serial port

        return NULL;
    }

    // Configure port

    memset(&options, 0, sizeof(options));
    options.c_cflag     = SCRP_BAUD_RATE | CS8 | CLOCAL | CREAD;
    options.c_iflag     = IGNPAR;
    options.c_oflag     = 0;
    options.c_lflag     = 0;
    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 200; // 20 second read time-out

    tcflush(fileno(port), TCIFLUSH);
    if(tcsetattr(fileno(port), TCSANOW, &options))
    {
        scrpClose(port);
        return NULL;
    }

    // Switch to diagnostic mode

    tcflush(fileno(port), TCIFLUSH);
    fwrite(SCRP_CMD_DIAG_MODE, 1, 1, port);
    tcdrain(fileno(port));

    return port;
}



void scrpClose(FILE *port)
{
    fclose(port);
}



int scrpCmd(FILE *port, char type, char *property, char *value, unsigned int value_size, unsigned int *value_length)
{
    char            c;
    int             error   = 0;
    unsigned int    i       = 0;

    // Switch to direct mode

    fwrite(SCRP_CMD_DIRECT_MODE, 1, 1, port);
    tcdrain(fileno(port));
    tcflush(fileno(port), TCIFLUSH);

    // Send command to device

    switch(type)
    {
        case 'g':   // Get command
        case 'G':
            fprintf(port, "!g %s\n", property);
            break;

        case 's':   // Set command
        case 'S':
            fprintf(port, "!s %s %s\n", property, value);
            break;

        default:    // Unknown command type
            return -1;
    }

    // Read response

    while(fread(&c, 1, 1, port) == 1)
    {
        switch(c)
        {
            case '$': // Start of response
                i = 0;
                break;

            case '!': // End of error response
                error = 1;

            case ';': // End of successful response
                if(i < value_size)
                {
                    // The SCRP protocol specifies that all successful responses are terminated with '\n;'
                    // and all error responses are terminated with '\n!'. Strip off the final \n before returning.

                    if(i > 0 && value[i-1] == '\n') --i;

                    value[i] = '\0';
                }

                if(value_length)
                {
                    *value_length = i;
                }

                // Switch to diagnostic mode

                fwrite(SCRP_CMD_DIAG_MODE, 1, 1, port);

                return error;

            default:
                if(i < value_size)
                {
                    value[i++] = c;
                }
                break;

        }
    }

    // Switch to diagnostic mode

    fwrite(SCRP_CMD_DIAG_MODE, 1, 1, port);

    // Read failed

    return -1;
}



int scrpDiag(FILE *port, struct fgc_diag *diag_data)
{
    unsigned char   c;
    unsigned int    sync_bytes = 0;

    // Read synchronisation bytes

    do
    {
        // Read a byte from the port

        if(fread(&c, 1, 1, port) != 1)
        {
            // Read failed

            return -1;
        }

        // Check whether byte is a synchronisation byte

        if(c == FGC_DIAG_SYNC)
        {
            sync_bytes++;
        }
        else // Byte is not a synchronisation byte
        {
            sync_bytes = 0;
        }
    } while(sync_bytes < FGC_DIAG_N_SYNC_BYTES);

    // Read diagnostic data

    if(fread(diag_data, sizeof(*diag_data), 1, port) != 1)
    {
        // Read failed

        return -1;
    }
    return 0;
}

// EOF
