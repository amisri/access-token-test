//! @file   os_tsk.c
//! @brief  Contains NanOS Task related functions for FGC3
//!
//! This file contains the NanOS functions which are related to task management
//! specific to the FGC3 hardware (RX610) and GCC M32C Compiler.
//! It is a port of the NanOS for FGC2 done by Quenting King


// ---------- Includes

#include <stdint.h>
#include <stdlib.h>
#include <sys/reent.h>
#include <syscalls.h>

#include <os.h>
#include <os_hardware.h>

#include <dpcom.h>
#include <memmap_mcu.h>
#include <mcuDependent.h>
#include <sharedMemory.h>
#include <structs_bits_big.h>



// ---------- Internal variable definitions

struct os_tsk * os_tsk_id_isr;



// ---------- Internal function definitions

//! Priority must never be 0x0000. This function must be used as follows:
//! os.next_tid = OSTskGetHighPrioHardware(os.ready_mask);

__inline__ uint16_t OSTskGetHighPrioHardware(uint16_t const priority) // __attribute__ ((always_inline))
{
    uint16_t tid;

    MCU_FF1(priority, tid);     // Find first bit set to 1. Result stored in tid.

    return(tid);
}



// ---------- External function definitions

//! This function is used to create a task.
//!
//! OSTskInit()  must always be called first to set up teh task control blocks.
//! OSTskStart() must be called last after all tasks have been created.
//! This function must not be called more than n_tsk times. The behaviour is
//! otherwise undefined.
//!
//! All interrupts are enabled when a task starts executing.
//!
//! The stack is 16 bits wide and is initialized to the value 0xCAF0 + tid.
//! It follows this layout:
//!
//!    psw            <-- C variable + size of tasks's stack
//!    RET ADD(H)
//!    RET ADD(L)
//!    RET ADD(H)
//!    RET ADD(L)
//!    r3
//!    r2
//!    r1             <-- r0 is not included as it is the stack pointer
//!    acc            <-- nanOS control block needs to point here to mimic the return from interrupt
//!    ..........
//!    task stack data
//!    .........
//!    .........      <-- stk C variable used for store task's stack points here
//!    .........
//!
//! During the preparation of the task control block, the variables
//! os.curr_tsk, os.curr_tid and os.next_tid are used as the new task's
//! TCB pointer, recognisable stack pattern and bit mask respectively.
//! These are initialised by the macro OSTskInit()
//!
//! REMEMBER stack is stored an calculated based in OS_STK items (uint16_t)
//! so any structure inside must be multiple of this. uint32_t is 2 times
//! OS_STK and uint16_t is OS_STK - no problem
//!
//! @param[in]   func      Pointer to the task function
//! @param[in]   data      Pointer to the task data structure
//! @param[in]   stk       Pointer to the bottom (end) of the task stack
//! @param[in]   stk_size  Stack size

void OSTskCreate(void (*func)(void*), void *data, OS_STK *stk, uint16_t stk_size)
{
    // Fill task stack with a recognisable pattern (0xCAF0 + TID) to help debugging

    while (stk_size--)
    {
        *(stk++) = os.curr_tid;
    }

    // Set up the task's stack frame

    // stk points to the high (word) address of the task's stack (the beginning of the next stack)
    // Fill in the stack in same manner as it grows (towards 0000h)

    // First store the fix part of data
    // Fill to mimic an ISR entry (all registers PUSH) is the struct os_REIT_data
    // Tasks should start with interrupts enabled and in Supervisor mode

    // ALL_REGISTERS ACC,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,FPSW,PC,PSW

    *(--stk)  = 0x0000;                       // PSW  register low:  IPL=0, ISP selected, interrupts enabled
    *(--stk)  = 0x0001;                       // PSW  register high
    *(--stk)  = (uint32_t)func & 0x0000ffff;  // PC   register low:  task function
    *(--stk)  = (uint32_t)func >> 16;         // PC   register high: task function
    *(--stk) = 0x0100;                        // FPSW register low:  floating-point status word
    *(--stk) = 0x0000;                        // FPSW register high: floating-point status word
    *(--stk) = 0xFF00;                        // r15  register low
    *(--stk) = 0xFFFF;                        // r15  register high
    *(--stk) = 0xEE00;                        // r14  register low
    *(--stk) = 0xEEEE;                        // r14  register high
    *(--stk) = 0xDD00;                        // r13  register low
    *(--stk) = 0xDDDD;                        // r13  register high
    *(--stk) = 0xCC00;                        // r12  register low
    *(--stk) = 0xCCCC;                        // r12  register high
    *(--stk) = 0xBB00;                        // r11  register low
    *(--stk) = 0xBBBB;                        // r11  register high
    *(--stk) = 0xAA00;                        // r10  register low
    *(--stk) = 0xAAAA;                        // r10  register high
    *(--stk) = 0x9900;                        //  r9  register low
    *(--stk) = 0x9999;                        //  r9  register high
    *(--stk) = 0x8800;                        //  r8  register low
    *(--stk) = 0x8888;                        //  r8  register high
    *(--stk) = 0x7700;                        //  r7  register low
    *(--stk) = 0x7777;                        //  r7  register high
    *(--stk) = 0x6600;                        //  r6  register low
    *(--stk) = 0x6666;                        //  r6  register high
    *(--stk) = 0x5500;                        //  r5  register low
    *(--stk) = 0x5555;                        //  r5  register high
    *(--stk) = 0x4400;                        //  r4  register low
    *(--stk) = 0x4444;                        //  r4  register high
    *(--stk) = 0x3300;                        //  r3  register low
    *(--stk) = 0x3333;                        //  r3  register high
    *(--stk) = 0x2200;                        //  r2  register low
    *(--stk) = 0x2222;                        //  r2  register high

    // gcc for Rx610 uses R1 to obtain the address of parameters like in "void CmdTsk(struct cmd *c)"
    // so pass the data pointer in R1, and pray for no compiler changes in the future

    *(--stk) = (uint32_t)data & 0x0000ffff;   //  r1  register low
    *(--stk) = (uint32_t)data >> 16;          //  r1  register high

    // Register r0 is not included as it is the stack pointer

    // This mimics saving r1, r2 of the accumulator but
    // ACC b47-b32 (r2 b31-b16) will be lost (already duplicated in r1)
    // ACC b31-b16 (r2 b15-b0) are shifted left to its place
    // ACC b15-b0 was not saved so takes 0s from r2
    // nanOS control block needs to point here to mimic the return from interrupt

    *(--stk) = 0x7654;                        // ACC low-low
    *(--stk) = 0x0000;                        // ACC low-high
    *(--stk) = 0xBA98;                        // ACC high-low
    *(--stk) = 0xFEDC;                        // ACC high-high

    // Save task's stack frame pointer and mask into the global variable that holds all task control blocks

    os.curr_tsk->stack = stk;
    os.curr_tsk->mask  = os.next_tid;

    // Set task mask bit to make the new task ready to run

    os.ready_mask = os.next_tid | os.ready_mask;

    // Prepare global values for next call to OSTskCreate()

    os.next_tid = os.next_tid << 1;
    os.curr_tid++;
    os.curr_tsk++;
}



//! Starts all the tasks and runs the task with ID zero (the highest priority)
//!
//! This function is like a normal function but returns with REIT forcing the 1st nanOS context switch
//! All tasks, semaphores, message queues and memory partitions must be created before calling it.
//! It never returns to the calling function.

void OSTskStart(void)
{
    // Local variables cannot be used to prevent local stack frame adjustments

    // Get address of TCB for task 0 (the highest priority) and make it the current task

    os.curr_tid = 0;
    OS_TID_CODE = 0;
    os.curr_tsk = os.tsk;

    OS_ISR_MASK = 0;
    OS_2US_CLK  = 0;

    // Convert after reset conditions to normal ones
    // 0x000C -> 4=EQUALS after reset 8=NOT EQUALS after reset

    if ((shared_mem.watch_point.control & 0x000C) != 0)
    {
        shared_mem.watch_point.control <<= 2;
    }

    // Mimic return from interrupt

    __asm__ volatile
    (
        // Load stack pointer for new task from its TCB
        //
        // os.tsk->stack ----> isp
        //
        // Check the correct offset os.tsk = os + 16
        // Verify with structures struct os and struct os_tsk
        // Use r5 as it is no used later

        "mov.l   #_os + 16, r5      \n\t"  // r5 = os.tsk
        "mov.l   [r5], r5           \n\t"  // r5 = os.tsk->
        "mov.l   [r5], r0           \n\t"  // r0 = stack pointer = os.tsk->stack

        // Pop all registers. This must be consistent with the other entry/exit
        // parts of nanOS/uCOS-II to be able to context switch.

        "popm    r1-r2              \n\t"  // Restore accumulator register

        // ACC b47-b32 (r2 b31-b16) will be lost (already duplicated in r1)
        // ACC b31-b16 (r2 b15-b0) are shifted left to its place
        // ACC b15-b0 was not saved so takes 0s from r2

        "shll    #16, r2            \n\t"

        "mvtaclo r2                 \n\t"  // Restore accumulator ACC bits b31-b0 from r2
        "mvtachi r1                 \n\t"  // Restore accumulator ACC bits b63-b32 from r1
        "popm    r1-r15             \n\t"  // Restore general-purpose registers
        "popc    fpsw               \n\t"  // Restore floating-point status word
        "rte                            "
    );
}



//! Resume a task which suspended itself using OSTskSuspend()
//!
//! This function can be called by a task or ISR
//!
//! @param[in]   tid       Task ID to resume

void OSTskResume(uint16_t const tid)
{
    // Allocate storage for CPU status register

    OS_CPU_SR  cpu_sr;

    os.next_tsk = &os.tsk[tid];

    // Protect against interrupts, except timers that are level 7

    OS_ENTER_CRITICAL();

    // Check if the task is not ready to run or pending on a sem/msg/mem

    if (    ((os.ready_mask & os.next_tsk->mask) == 0)
         && (os.next_tsk->pending == 0))
    {
        os.ready_mask |= os.next_tsk->mask;

        // Resume task if it is the highest and not called from ISR

        if (!(os.int_nesting > 0) && (tid < os.curr_tid))
        {
            os.next_tid = tid;
            OS_TASK_SW();
        }
    }

    OS_EXIT_CRITICAL();
}



//! Pend the current task
//!
//! This function switches the context to next highest priority task ready to run.
//! It first makes the current task not ready to run.
//!
//! Interrupts must be masked before this function is called.
//!
//! @param[in]   pending   Points to the sem/msg/mem pending tasks mask.

void * OSTskPend(uint16_t * pending)
{
    // Make current task not ready to run

    os.ready_mask &= ~os.curr_tsk->mask;

    // Link task to the pending mask if not NULL

    if (pending != NULL)
    {
        os.curr_tsk->pending = pending;
        *pending |= os.curr_tsk->mask;
    }

    // Pass ready mask to lssb logic and get address of TCB for next task to run

    os.next_tid = OSTskGetHighPrioHardware(os.ready_mask);
    os.next_tsk = &(os.tsk[os.next_tid]);

    OS_TASK_SW();

    return os.curr_tsk->tmp_ptr;
}



//! This function prepares the highest priority pending task for execution.
//!
//! Interrupts are masked before this function is called.  For OSMsgPost() & OSMemPost()
//! the message/block address is passed to the pending task via the stack, so that it can be returned from
//! OSTskPend() to OSMsgPend() or OSMemPend()
//!
//! @param[in]   pending   Points to the sem/msg/mem pending tasks mask.
//! @param[in]   addr

void OSTskReady(uint16_t * pending, void * addr)
{
    // Move *pending to lssb logic

    os.next_tid = OSTskGetHighPrioHardware(*pending);

    // Make next pending task ready to run

    os.next_tsk    = &os.tsk[os.next_tid];
    *pending      &= ~os.next_tsk->mask;
    os.ready_mask |=  os.next_tsk->mask;

    os.next_tsk->pending = 0;
    os.next_tsk->tmp_ptr = addr;

    // Run task if not called from ISR and priority higher than current task

    if (   !(os.int_nesting > 0)
        && (os.next_tid < os.curr_tid))
    {
        OS_TASK_SW();
    }
}



//! Force a context switch
//!
//! This function is called following a software interrupt (OS_TASK_SW()). It is used
//! to trigger a context switch from task level operation. It saves the registers of
//! the running task on its stack, and switches to the stack of the next task to be run.
//! The identity of this task has been pre-set in os.next_tid and os.next_tsk by the
//! function which includes the OS_TASK_SW().

void OSTskCtxSwitch(void)
{
    // Don't use local variables to prevent local stack frame adjustments

    // Push ALL_REGISTERS

    __asm__ volatile
    (
        // Save floating-point status word
        "pushc   fpsw           \n\t"
        // Save general-purpose registers
        // r0 is not included as it is the stack pointer
        "pushm   r1-r15         \n\t"
        // save accumulator ACC bits b63-b32 to r1
        "mvfachi r1             \n\t"
        // save accumulator ACC bits b47-b16 to r2
        "mvfacmi r2             \n\t"
        "pushm   r1-r2          "
    );

    __asm__ volatile
    (
        // Save stack pointer of current task in its TCB
        //
        // isp ----> os.curr_tsk->stack
        //
        // Check the correct offset os.curr_tsk = os + 8
        //
        // This destroys r15, since it is not used before it not a problem

        "mov.l   #_os + 8, r15      \n\t"   // r15 = os.curr_tsk
        "mov.l   [r15], r15         \n\t"   // r15 = os.curr_tsk->
        "mov.l   r0, [r15]          \n\t"   // r0  = stack pointer = os.curr_tsk->stack
    );

    OSTskWatch();

    // Add the used time

    os.curr_tsk->task_run_time += OS_2US_CLK;

    // Get the address of TCB for the new task and make it the current TCB

    os.curr_tsk = os.next_tsk;

    // Get task ID of new task and make it the current task ID

    os.curr_tid      = os.next_tid;
    OS_TID_CODE      = os.curr_tid;
    dpcom.dbg.tsk_id = os.curr_tid;

    // Point to the task's specific reentrant structure (newlib requirement)

    _impure_ptr = newlib_reent_ptr[os.curr_tid];

#ifdef FGC_DEBUG_OS_HISTORY
    OSCtxSnapshot(0);
#endif

    // Executes RTI to resume operation of the new task

    __asm__ volatile
    (
        //  Load stack pointer for new task from its TCB
        //
        //  os.next_tsk->stack ----> isp
        //
        // Check the correct offset os.curr_tsk = os + 12
        // This destroys r5, since it is not used before it not a problem

        "mov.l   #_os + 12, r5      \n\t"  // r5 = os.next_tsk
        "mov.l   [r5], r5           \n\t"  // r5 = os.next_tsk->
        "mov.l   [r5], r0           \n\t"  // r0 = stack pointer = os.next_tsk->stack

        // The stack can be pointing to a higher priority task so this forces a context switch
        // Pop ALL_REGISTERS

        "popm    r1-r2              \n\t"  // Restore accumulator register
        // ACC b47-b32 (r2 b31-b16) will be lost (already duplicated in r1)
        // ACC b31-b16 (r2 b15-b0) are shifted left to its place
        // ACC b15-b0 was not saved so takes 0s from r2
        "shll    #16, r2            \n\t"
        "mvtaclo r2                 \n\t"  // Restore accumulator ACC bits b31-b0 from r2
        "mvtachi r1                 \n\t"  // Restore accumulator ACC bits b63-b32 from r1
        "popm    r1-r15             \n\t"  // Restore general-purpose registers
        "popc    fpsw               \n\t"  // Restore floating-point status word
        "rte                            "
    );
}



//! This function is used to notify NanOS that an ISR has completed
//!
//! All user ISRs must exit via this function.

void OSTskIntExit(void)
{
    // Allocate storage for CPU status register

    OS_CPU_SR  cpu_sr;

    // Disable interrupts, except timers that are level 7

    OS_ENTER_CRITICAL();

    // Prevent os.int_nesting from wrapping

    if (os.int_nesting > 0)
    {
        os.int_nesting--;
    }

    OSTskWatch();

    // Reschedule only if all ISRs complete

    if (os.int_nesting == 0)
    {
        // Add the interrupt time to the task the interrupt is associated to

        os_tsk_id_isr->isr_run_time += OS_2US_CLK;

        // Clear task bit in ISR code word

        OS_ISR_MASK = 0;

        // Don't do switch context if this is the current task

        if (OSTskGetHighPrioHardware(os.ready_mask) != os.curr_tid)
        {
            // Get task ID of new task and make it the current task ID

            os.curr_tid = OSTskGetHighPrioHardware(os.ready_mask);
            OS_TID_CODE = os.curr_tid;

            // Point to the task's specific reentrant structure (newlib requirement)

            _impure_ptr = newlib_reent_ptr[os.curr_tid];

            // Get address of TCB for new task, and make it the current TCB

            os.curr_tsk = &os.tsk[os.curr_tid];

    #ifdef FGC_DEBUG_OS_HISTORY
            OSCtxSnapshot(3);
    #endif

            // OSIntCtxSw() Perform interrupt level ctx switch

            __asm__ volatile
            (
                // Load stack pointer for new task from its TCB
                //
                // os.curr_tsk->stack ----> isp
                //
                // Check the correct offset os.curr_tsk = os + 8
                // This destroys r5, since it is not used before it not a problem

                "mov.l   #_os + 8, r5       \n\t"   // r5 = os.curr_tsk
                "mov.l   [r5], r5           \n\t"   // r5 = os.curr_tsk->
                "mov.l   [r5], r0           \n\t"   // r0 = stack pointer = os.curr_tsk->stack

                // The stack can be pointing to a higher priority task so this forces a context switch

                "popm    r1-r2              \n\t"  // Pop ALL_REGISTERS
                // ACC b47-b32 (r2 b31-b16) will be lost (already duplicated in r1)
                // ACC b31-b16 (r2 b15-b0) are shifted left to its place
                // ACC b15-b0 was not saved so takes 0s from r2
                "shll    #16, r2            \n\t"
                "mvtaclo r2                 \n\t"   // Restore accumulator ACC bits b31-b0 from r2
                "mvtachi r1                 \n\t"   // Restore accumulator ACC bits b63-b32 from r1
                "popm    r1-r15             \n\t"   // Restore general-purpose registers
                "popc    fpsw               \n\t"   // Restore floating-point status word
                "rte                        \n\t"
                "nop                        \n\t"
                "nop                            "
            );
        }
    }

#ifdef FGC_DEBUG_OS_HISTORY
    OSCtxSnapshot(3);
#endif

    OS_EXIT_CRITICAL();
}



__inline__ void OSTskSetIsrTaskId(uint32_t const tid)
{
    os_tsk_id_isr = &os.tsk[tid];
    OS_ISR_MASK   = os_tsk_id_isr->mask;
}



//!  Take a snapshot of the current context at each context switch (os tasks or interrupts).
//!  If int_nesting > 0, we are in an interrupt and the corresponding interrupt number can be seen
//!  in the NEXT snapshot. If int_nesting == 0 we are in the task with current TID number.
//!  This function can be called in 4 situation:
//!      0) Task enter                      2) Int enter (last isr is not valid)
//!      1) Task exit (not used)            3) Int exit  (last isr is valid)
//!  pc contains the address where the task has been suspended.
//!  In case the task suspended itself, pc (the return address) is the end of OSTskPend.
//!  In this case pc_indirect contains the return address in user space (the OSTskPend caller).

#ifdef FGC_DEBUG_OS_HISTORY
__inline__ void OSCtxSnapshot(uint16_t event)
{

    os_history.tsk_switch[os_history.index].tid         = os.curr_tid;
    os_history.tsk_switch[os_history.index].time        = TIME_US_P;
    os_history.tsk_switch[os_history.index].next_stack  = os.next_tsk->stack;
    // PSW is the last word of the context switch, which is 80 byte long.
    os_history.tsk_switch[os_history.index].psw         = ((uint32_t*)os.next_tsk->stack)[19];
    os_history.tsk_switch[os_history.index].pc          = ((uint32_t*)os.next_tsk->stack)[18];
    // In case we come from OSTskPend, we are not interested in PC but PC after return (RTSD #10H, R7-R10)
    // So the interesting return address is end of OSTskCtxSwitch stack size (20) + 5, that is 25th uint32_t
    os_history.tsk_switch[os_history.index].pc_indirect = ((uint32_t*)os.next_tsk->stack)[24];
    os_history.tsk_switch[os_history.index].ipl         = (os_history.tsk_switch[os_history.index].psw & 0x07000000) >> 24;
    os_history.tsk_switch[os_history.index].int_enable  = (os_history.tsk_switch[os_history.index].psw & 0x00010000) >> 16;
    os_history.tsk_switch[os_history.index].int_nesting = os.int_nesting;
    os_history.tsk_switch[os_history.index].event       = event;

    os_history.index++;
    os_history.index &= (OS_HISTORY_SIZE-1);
}
#endif


// EOF
