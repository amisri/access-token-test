//! @file   os_hardware.h
//! @brief  Contains the NanOS hardware dependent part
//!
//! NanOS was evolved from the larger and more sophisticated RTOS MicroC/OS-II
//! written by Jean Labrosse. See os_tsk.c for more details.
//!
//! Disable/Enable interrupts by preserving the state of interrupts. Generally speaking you
//! would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
//! disable interrupts. 'cpu_sr' is allocated in all of uC/OS-II's functions that need to
//! disable interrupts. You would restore the interrupt disable state by copying back 'cpu_sr'
//! into the CPU's status register. This also restores the other flags (apart from I) from the
//! point they were saved

#pragma once


// ---------- Includes

#include <stdint.h>

#include <sharedMemory.h>
#include <time_fgc.h>



// ---------- Constants

//! Adds tmp pointer for returning data from the queues to struct os_tsk
//! and Interrupt nesting level to struct os
#define  EXTRA_DATA

#define  OS_CRITICAL_METHOD    3

#define OS_LSSB                 CPU_LSSB_32     //!< Least significant set bit register
#define OS_2US_CLK              CPU_TIMER_P     //!< 500kHz timer

#define OS_TID_CODE             shared_mem.task_id
#define OS_ISR_MASK             shared_mem.isr_mask

// Control OS history by calling OSCtxSnapshot() to take a snapshot at each context switch
//#define FGC_DEBUG_OS_HISTORY 1


// ---------- External structures, unions and enumerations

//! Saved registers for level 3 OS_ENTER_CRITICAL
typedef uint32_t OS_CPU_SR;



// ---------- External function declarations

void OSTskSetIsrTaskId(uint32_t const tid);



// ---------- External function definitions

//! This function provides a crude watchpoint system.  It can check to see if a specified location is
//! equal or not equal to a given value.  If the condition is true, a hit counter is incremented and the
//! task ID and ISR mask are recorded.

static __inline__ void OSTskWatch(void)
{
    uint32_t value = *((uint32_t *)shared_mem.watch_point.address);

    if (   (   (shared_mem.watch_point.control & 0x0001) != 0
            && (value & ~shared_mem.watch_point.mask) == shared_mem.watch_point.value)
        || (   (shared_mem.watch_point.control & 0x0002) != 0
            && (value & ~shared_mem.watch_point.mask) != shared_mem.watch_point.value))
    {
        shared_mem.watch_point.hit_count++;
        shared_mem.watch_point.control      = 0;
        shared_mem.watch_point.task_id      = shared_mem.task_id;
        shared_mem.watch_point.isr_mask     = shared_mem.isr_mask;
        shared_mem.watch_point.timestamp_s  = timeGetUtcTime();
        shared_mem.watch_point.timestamp_us = timeGetUtcTimeUs();
    }
}



//   Note: the MVTIPL instruction is buggy in products of the RX610 Group (but can be used in the Rx62N)
//   Use the MVTC instruction to write interrupt priority levels to the processor interrupt-priority
//   level (IPL[2:0]) bits in the Processor Status Word (PSW)
//
//   movtc xx,PSW    ; move to Processor Status Word (PSW)
//           ; In user mode, writing to IPL[3:0], PM, U, and I bits in the PSW is ignored
//   movtc xx,BPSW   ; move to Backup Processor Status Word (PSW)
//
//   mvfc PSW, Rd
//
//   clrpsw I    // disable interrupts
//   setpsw I    // Re-enable interrupts
//       PSW bit 16 is I (Interrupt Enable)
//       0: Interrupt disabled
//       1: Interrupt enabled

#define  DISABLE_INTERRUPTS()  __asm__ volatile ( "clrpsw I" )
#define  ENABLE_INTERRUPTS()   __asm__ volatile ( "setpsw I" )


// The operands for the extended __asm__ must be added at the end of __asm__()
//
// Leave level 6 active as it is used for the timer, this way we can continue to
// use it for USLEEP, MSLEEP, etc.

// The macros OS_ENTER_CRITICAL and OS_EXIT_CRITICAL require a variable 'cpu_sr'
// local to the calling function to be defined (see the example below)
//
// Example of a critical section:
//
// void foo()
// {
//     OS_CPU_SR           cpu_sr;
//
//     OS_ENTER_CRITICAL();
//
//     // Code executed in critical section
//
//     OS_EXIT_CRITICAL();
// }
//
// CAUTION: NEVER NEST TWO CRITICAL SECTIONS IN THE SAME FUNCTION!!!
//          If there is really a need, then define two locals  type OS_CPU_SR and use the
//          GENERIC macros instead of the simpler OS_ENTER_CRITICAL/OS_EXIT_CRITICAL ones.
//
// void foo()
// {
//     OS_CPU_SR           cpu_sr1;
//     OS_CPU_SR           cpu_sr2;
//
//     OS_ENTER_CRITICAL_GENERIC(cpu_sr1);
//
//         OS_ENTER_CRITICAL_GENERIC(cpu_sr2);
//
//         // Code executed in a nested critical section
//
//         OS_EXIT_CRITICAL_GENERIC(cpu_sr2);
//
//     OS_EXIT_CRITICAL_GENERIC(cpu_sr1);
// }


#define  OS_ENTER_CRITICAL_GENERIC(saved_psw) /* Disable interrupts                        */ \
__asm__ volatile                                                                              \
(                                                                                             \
    "push   r5               \n\t"  /* Save r5                                             */ \
    "mvfc   psw, r5          \n\t"  /* Copy the Processor Status Word (PSW) into r5        */ \
    "clrpsw I                \n\t"  /* Manipulation of the stack pointer (= R0) must       */ \
                                    /* be protected against ALL interrupts                 */ \
    "add    #04h, r0         \n\t"  /* The +4 is because the push introduced is not seen   */ \
                                    /* by the compile and saved_psw is referred to the     */ \
                                    /* stack known by the compiler                         */ \
    "mov.l  r5, %0":"=m"(saved_psw):/* no input operands */:/* clobbered registers         */ \
);                                                                                            \
__asm__ volatile                                                                              \
(                                                                                             \
    "sub    #04h, r0         \n\t"  /* Restore stack                                       */ \
    "setpsw I                \n\t"                                                            \
    "and    #0f8ffffffh, r5  \n\t"  /* Clear the IPL bits (to OR later)                    */ \
    "or     #006000000h, r5  \n\t"  /* Set IPL bits (b26,b25,b24) to 011                   */ \
    "mvtc   r5, psw          \n\t"  /* Write back the modified Processor Status Word (PSW) */ \
                                    /* This is ignored if the processor is in user mode    */ \
    "pop    r5               \n\t"  /* Restore original r5                                 */ \
)

// cpu_sr must be defined local to the calling function

#define  OS_ENTER_CRITICAL()  OS_ENTER_CRITICAL_GENERIC(cpu_sr)


// cpu_sr = GetIPL() +  SetIPL(7)

#define  OS_ENTER_CRITICAL_7()      /* Disable interrupts                                  */ \
__asm__ volatile                                                                              \
(                                                                                             \
    "push   r5               \n\t"  /* Save r5                                             */ \
    "mvfc   psw, r5          \n\t"  /* Copy the Processor Status Word (PSW) into r5        */ \
    "clrpsw I                \n\t"  /* Manipulation of the stack pointer (= R0) must       */ \
                                    /* be protected against ALL interrupts                 */ \
    "add    #04h, r0         \n\t"  /* The +4 is because the push introduced is not seen   */ \
                                    /* by the compile and saved_psw is referred to the     */ \
                                    /* stack known by the compiler                         */ \
    "mov.l  r5, %0":"=m"(cpu_sr):/* no input operands */:/* clobbered registers            */ \
);                                                                                            \
__asm__ volatile                                                                              \
(                                                                                             \
    "sub    #04h, r0         \n\t"  /* Restore stack                                       */ \
    "setpsw I                \n\t"                                                            \
    "and    #0f8ffffffh, r5  \n\t"  /* Clear the IPL bits (to OR later)                    */ \
    "or     #007000000h, r5  \n\t"  /* Set IPL bits (b26,b25,b24) to 011                   */ \
    "mvtc   r5, psw          \n\t"  /* Write back the modified Processor Status Word (PSW) */ \
                                    /* This is ignored if the processor is in user mode    */ \
    "pop    r5               \n\t"  /* Restore original r5                                 */ \
)


// SetIPL(cpu_sr)

#define OS_EXIT_CRITICAL_GENERIC(saved_psw) /* Enable interrupts                           */ \
__asm__ volatile                                                                              \
(                                                                                             \
    "push   r4               \n\t"  /* Save r4                                             */ \
    "push   r5               \n\t"  /* Save r5                                             */ \
    "clrpsw I                \n\t"  /* Manipulation of the stack pointer (= R0) must       */ \
                                    /* be protected against ALL interrupts                 */ \
    "add    #08h, r0         \n\t"  /* The +8 is because the 2 push introduced is not seen */ \
                                    /* by the compile and saved_psw is referred to the     */ \
                                    /* stack known by the compiler                         */ \
    "mov.l  %0, r4":/* no output operands */:"m"(saved_psw):/* clobbered registers         */ \
);                                                                                            \
__asm__ volatile                                                                              \
(                                                                                             \
    "sub    #08h, r0         \n\t"  /* Restore stack                                       */ \
    "setpsw I                \n\t"                                                            \
    "and    #007000000h, r4  \n\t"  /* Clear the all except IPL bits (b26,b25,b24) (to OR later) */ \
    "mvfc   psw, r5          \n\t"  /* Copy the Processor Status Word (PSW) into r5        */ \
    "and    #0f8ffffffh, r5  \n\t"  /* Clear the IPL bits (b26,b25,b24)                    */ \
    "or     r4, r5           \n\t"  /* Update the actual PSW                               */ \
    "mvtc   r5, psw          \n\t"  /* Write back the modified Processor Status Word (PSW) */ \
                                    /* This is ignored if the processor is in user mode    */ \
    "pop    r5               \n\t"  /* Restore original r5                                 */ \
    "pop    r4               \n\t"  /* Restore original r4                                 */ \
)

// cpu_sr must be defined local to the calling function

#define OS_EXIT_CRITICAL() OS_EXIT_CRITICAL_GENERIC(cpu_sr)



// Search for "OS_TASK_SW_vector" through the project files to know where to replace
// if this value is changed
// This is a fast call to _OSTskCtxSwitch
// Mapped to the software interrupt 1

#define OS_TASK_SW()  __asm__ volatile ("int #1")



// Search globally this label "ALL_REGISTERS" to check that the same registers
// are always pushed and popped in the same order
// ALL_REGISTERS ACC,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,FPSW,PC,PSW

#define OS_INT_ENT_ASM(lbl)                                                                   \
__asm__ volatile                                                                              \
(                                                                                             \
    "pushc   fpsw            \n\t"  /* Save floating-point status word                     */ \
    "pushm   r1-r15          \n\t"  /* Save general-purpose registers. r0 is not included  */ \
                                    /* as it is the stack pointer                          */ \
    "mvfachi r1              \n\t"  /* save accumulator ACC bits b63-b32 to r1             */ \
    "mvfacmi r2              \n\t"  /* Save accumulator ACC bits b47-b16 to r2             */ \
    "pushm   r1-r2           \n\t"                                                            \
                                                                                              \
    /* In previous versions I was testing for nesting limit       */                          \
    /* Not anymore as the overflow will arise after 65535 nesting */                          \
    /* if (os.int_nesting < 255)   { os.int_nesting++; }          */                          \
    /* if (os.int_nesting < 65535) { os.int_nesting++; }          */                          \
                                                                                              \
    "mov.l  #_os + 32, r5    \n\t"  /* Get os.int_nesting address = os + 32                */ \
    "mov.w  [r5], r3         \n\t"  /* r3 = os.int_nesting                                 */ \
    "add    #1, r3           \n\t"  /* Increment os.int_nesting                            */ \
    "mov.w  r3, [r5]         \n\t"  /* Save the new value                                  */ \
    /* if (os.int_nesting == 1)          */                                                   \
    /* {                                 */                                                   \
    /*    os.curr_tsk[0] = stack pointer */                                                   \
    /* }                                 */                                                   \
                                                                                              \
    "cmp    #1, r3           \n\t"                                                            \
    "bne _" #lbl  "          \n\t"                                                            \
                                                                                              \
    "mov.l  #_os + 8, r5     \n\t"  /* Save current task's SP into its TCB (os.curr_tsk = os + 8) */  \
    "mov.l  [r5], r5         \n\t"  /* r5 = os.curr_tsk->                                  */ \
    "mov.l  r0, [r5]         \n\t"  /* Stack pointer to os.curr_tsk->[0]                   */ \
     "_" #lbl ":             \n\t"                                                            \
);



#ifdef FGC_DEBUG_OS_HISTORY
#define OS_INT_ENT(tsk_id, lbl)                                                               \
    OS_INT_ENT_ASM(lbl);                                                                      \
    OSTskWatch();                                                                             \
    os.curr_tsk->task_run_time = OS_2US_CLK + os.curr_tsk->task_run_time;                     \
    OSTskSetIsrTaskId(tsk_id);                                                                \
    OSCtxSnapshot(2);                                                                         \
    __builtin_rx_setpsw('I');       /* Re-enable interrupts. */
#else
#define OS_INT_ENT(tsk_id, lbl)                                                               \
    OS_INT_ENT_ASM(lbl);                                                                      \
    OSTskWatch();                                                                             \
    os.curr_tsk->task_run_time = OS_2US_CLK + os.curr_tsk->task_run_time;                     \
    OSTskSetIsrTaskId(tsk_id);                                                                \
    __builtin_rx_setpsw('I');       /* Re-enable interrupts. */
#endif // FGC_DEBUG_OS_HISTORY


// ALL_REGISTERS ACC,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,FPSW,PC,PSW

#define OS_INT_EXIT()                                                                         \
__asm__ volatile                                                                              \
(                                                                                             \
    "mov.l  #_OSTskIntExit, r5 \n\t" /* call OSTskIntExit() and restore previously saved registers */ \
    "jsr    r5               \n\t"                                                            \
    "popm   r1-r2            \n\t"  /* Restore accumulator register                        */ \
    /* ACC b47-b32 (r2 b31-b16) will be lost (already duplicated in r1) */                    \
    /* ACC b31-b16 (r2 b15-b0) are shifted left to its place            */                    \
    /* ACC b15-b0 was not saved so takes 0s from r2                     */                    \
    "shll   #16, r2          \n\t"                                                            \
    "mvtaclo r2              \n\t"  /* Restore accumulator ACC bits b31-b0 from r2         */ \
    "mvtachi r1              \n\t"  /* Restore accumulator ACC bits b63-b32 from r1        */ \
    "popm    r1-r15          \n\t"  /* Restore general-purpose registers. r0 is not        */ \
                                    /* included as it is the stack pointer                 */ \
    "popc   fpsw             \n\t"  /* Restore floating-point status word                  */ \
    "rte                     \n\t"                                                            \
    "nop                     \n\t"  /* Are these 2 nops necesary ? (to test)               */ \
    "nop                         "                                                            \
)

#define OSTskSuspend()  {                                              \
                            OS_CPU_SR cpu_sr_suspend;                  \
                            OS_ENTER_CRITICAL_GENERIC(cpu_sr_suspend); \
                            OSTskPend(0);                              \
                            OS_EXIT_CRITICAL_GENERIC(cpu_sr_suspend);  \
                        }


void OSTskStart      (void)  __attribute__ ((interrupt, naked));
void OSTskCtxSwitch  (void)  __attribute__ ((interrupt, naked));


// EOF
