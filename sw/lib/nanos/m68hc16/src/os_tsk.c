/*---------------------------------------------------------------------------------------------------------*\
  File:     os_tsk.c

  Purpose:  Contains NanOS Task related functions for MC68HC16Z1 + Hiware compiler.

  Author:   Quentin.King@cern.ch

  Notes:    This file contains the NanOS functions which are related to task management.  They are
            specific to the CERN FGC2 hardware and Hiware HC16 Compiler.

        NanOS is an evolution of MicroC/OS-II by Jean Labrosse.  It has been reduced and
        simplified to match the needs of the CERN AB-PO-CC FGC Project.
        NanOS provides the following facilities:

            o Pre-emptive task scheduling (without priority inheritance) for up to 16 tasks.
            o Single interrupt level with no more than one interrupt associated with each task.
            o Task Suspend, Resume.
            o Counting semaphores.
            o FIFO message queues.
            o Blocking memory partitions.
            o Task and ISR run time measurement (2 us units, minimum 10Hz update to avoid overflow)
            o Watchpoint on context switch

        The main restrictions compared to standard MicroC/OS-II are:

            o All OS resources (tasks, semaphores, message queues, memory partitions) must be
              created before task scheduling starts and may not be deleted or changed.
            o The maximum number of tasks is 16 instead of 64.
            o Interrupts cannot be nested.
            o Tasks cannot change priority.
            o Task self-suspend only is supported (i.e. one task cannot suspend another).
            o No mailbox support.
            o No tick function or timeouts support.
            o No task function hooks.
            o No idle or statistics task support.
            o No error checking or reporting.

        The main extensions compared to standard MicroC/OS-II are:

            o Memory partitions include a counting semaphore.
                    o Task and ISR run time is measured.

        In terms of source file lines, NanOS at ~1000 is 25% of the size of MicroC/OS-II.

        The single NanOS global variable (os) is declared in os.h and is defined in this file.

        Code size on the MC68HC16Z1 is ~1060 bytes and the data size (excluding message queue and
        memory partition buffers) is given by:

            18 + (10 x n_tsks) + (4 x n_sems) + (14 x n_msgs) + (6 x n_mems)

        Task priority is related to task ID (0-15).  The lower the ID, the higher the priority.
        Task IDs always start from zero and are contiguous due to the way that the TCB is initialised
        in the application.

        These functions were evolved from the task functions of MicroC/OS-II by Jean Labrosse.
        They were simplified to remove the timeout support, error reporting, and dynamic creation
        and deletion of tasks.  Task suspend is for "SELF" only.  Space for the Task Control
        Blocks is now defined in the application, with the task stack, data and function address
        initialised.

        The V2 FGC hardware includes logic that performs the essential OS function of
        finding the index of the least significant set bit (lssb).  On V1 hardware, this required
        the function OSBitIdx() which took 3-5us to run.  The new hardware provides the
        register OS_LSSB into which a 16 bit mask can be written.  The register can
        then be read immediately to get the index of the least significant set bit.

        NanOS writes the TID of the running task into a memory location (OS_TID_CODE) and the
        mask of the task associated with a running ISR into a different location (OS_ISR_CODE).

        Only one ISR can run at a time - there is no ISR nesting.
  History:

    29/02/00    qak Adapted from MicroC/OS-II RTOS by Jean Labrosse
    20/03/00    qak OSTaskRestart() added
    24/03/00    qak OSTaskRestart(), OSTaskEventReady() & OSTaskResume() context switch directly
    02/05/00    qak Updated to use new structures
    04/05/00    qak state removed from TCB and message returned via stack
    17/05/01    qak Ported to V1.1 and merged with os_hc16.c
    10/07/01    qak OSTskSuspend() converted into a Macro
    23/07/02    qak Task run timing support added
    03/08/02    qak Global variable is now defined in this file
    05/02/04    qak Registers moved to FC00
    02/06/04    qak FGC memmap header now included to get register constants
    06/02/06    qak Add task data to TCB
    21/03/06    qak OSTskRestart() removed.  OSTskCreate() added and merged with OSTskStackInit()
    09/07/07    qak TBEK added to OSTskIntEnter() to handle EK being changed when using far pointers
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <os.h>
#include <memmap_mcu.h>         // Include FGC memory map constants

/*--- Constants and macros ---*/

// These depends on the FGC2 hardware
#define OS_LSSB             CPU_LSSB_16
#define OS_2US_CLK          CPU_TIMER_16
#define OS_TID_CODE         SM_TSKID_16
#define OS_ISR_MASK         SM_ISRMASK_16
#define OS_ISR_RUNNING()    *((INT16U*)(OS_ISR_MASK))



#define OS_TASK_SW()        asm SWI
#define OS_SAVE_ADDR(addr)  os.next_tsk->stack[3]=(OS_STK)addr

/*---------------------------------------------------------------------------------------------------------*/

/*--- Global variable definition ---*/

struct os os;

/*---------------------------------------------------------------------------------------------------------*/

void Crash(INT16U reg_e);

/*---------------------------------------------------------------------------------------------------------*/



#pragma MESSAGE DISABLE C5703 // Disable parameter declared but not referenced (for stk_size)
/*---------------------------------------------------------------------------------------------------------*/
void OSTskCreate(void (*func)(void*), void *data, OS_STK *stk, INT16U stk_size)     // main
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to create a task.  All calls to OSTskCreate() must be after "calling" the macro
  function OSTskInit(n_tsks) to set up the task control blocks.  All calls to OSTskCreate() must be
  complete before calling OSTskStart().  The function must not be called more than n_tsk times, as no
  check is made to see if there are remaining task control blocks.

  Arguments:
        func        20 bit pointer to the task function.  The value stacked will be this value
                    plus six because of the 3 instruction pipeline of the MC68HC16Z1.
                    i.e. The RTI instruction will subract six from the stacked return address
                    before setting the PC register.

        data        is a pointer the task data structure.

        stk         is a pointer to the bottom of the task stack.  The MC68HC16Z1 stack
                    grows downwards so the stack_size is needed to find the start of the stack.

        stk_size    Size of stack in elements (words)

  Notes:        1) All interrupts are enabled when a task starts executing.

                2) Stack frame (16-bit wide stack on MC68HC16Z1) created by this function:

                      + 18   Location just beyond top of stack
                      + 16   task func addr \  Interrupt frame
                      + 14   CCR:PK         /
                      + 12   D = unchanged  \
                      + 10   E = unchanged   |
                      +  8   X = unchanged   | CPU registers
                      +  6   Y = task data   |
                      +  4   Z = unchanged   |
                      +  2   K = zero       /
                   SP +  0   Stack pointer = bottom of new task's stack frame

                3) The page register K on the new stack is zeroed.  This means that the
                   default data page is 0.  The PULM will set IY to the task data as
                   required by the Hiware compiler register passing model.

                4) The stack is initialised to 0xCAFE + TID to aid debugging

                5) During the preparation of the task control blocks, the variables
                   os.curr_tsk, os.curr_tid and os.next_tid are used as the new task's
                   TCB pointer, recognisable stack pattern and bit mask respectively.
                   These are initialised by the macro OSTskInit()
\*---------------------------------------------------------------------------------------------------------*/
{
    asm                 // D = stack_size on entry (Hiware compiler convention)
    {
    /* Set task task to contain recognisable pattern to help debugging */

        LDY stk             // IY = bottom of stack
        ADDD    #-9         // Deduct 9 words from stack size to allow for task's stack frame
        LDE os.curr_tid     // E = recognisable pattern to help debugging (0xCAF0 + TID)
        INCW    os.curr_tid // Increment pattern for next call to OSTskCreate()

        loop:               // Loop to fill task stack with this recognisable pattern
        STE 0,Y
        AIY #2
        ADDD    #-1
        BNE loop            // On exit, IY points to bottom of new task's stack frame

        /* Set up new task's stack frame */

        LDE func:0          // E:D = pointer to task function (E=high word)
        LDD func:2          // E:D = pointer to task function (D=low word)
        ADDD    #0x0006     // Add six to address to compensate for 6 byte prefetch pipeline
        ADCE    #0x0000     // Add carry bit to E to allow for change of 64KB page and clear
                        // interrupt priority IP to enable all interrupts
        STD 16,Y            // Store task function address at the top of the new stack frame
        STE 14,Y            // Store CCR = IP:PK
        LDD data            // Get task data pointer
        STD 6,Y             // Store so that it will be in IY when the task starts
        CLRW    2,Y         // Clear stacked K to force default data page to be zero

        /* Set up new task control block */

        LDX os.curr_tsk     // IX is address of the new task control block (see OSTskInit() macro)
        STY OS_STACK,X      // Save task's stack pointer in task control block
        LDD os.next_tid     // D = task mask (os.next_tid is initialised by OSTskInit() macro)
        TDE                 // E = task mask
        STD OS_MASK,X       // Set task mask in TCB
        ORD os.ready_mask   // Set task mask bit in os.read_mask...
        STD os.ready_mask   // to make new task ready to run
        LSLE                // Shift task mask bit
        STE os.next_tid     // Save new bit mask in os.next_tid for next call to OSTskCreate()
        AIX #OS_TCB_SIZE    // Increment IX to point to next TCB
        STX os.curr_tsk     // Save IX back in os.curr_tsk for next call to OSTskCreate()
    }
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void OSTskStart(void)                                   // main
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to start multitasking.  All tasks, semaphores, message queues and memory partitions
  must be created before calling OSTskStart().  This function never returns to the calling function.  It
  starts the task with ID zero (the highest priority).
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDD SM_WATCH_CTRL_16    // If WATCH_CTRL has bits 2 or 3 set
        ANDD    #0x0C           // the shift right by 2 bits and store
        BEQ start
        LSRD
        LSRD
        STD SM_WATCH_CTRL_16
        start:
        LDX os.tsk          // Get address of TCB for task 0 (the highest priority)
        STX os.curr_tsk     // Make this the current task
        LDS OS_STACK,X      // Load task stack address into HC16 SP register
        CLRW    OS_TID_CODE     // Set zero (task ID) in TID code location
        CLRW    OS_ISR_MASK     // Set zero in ISR code location
        CLRW    OS_2US_CLK      // Reset 2us timer and (this also retriggers slow watchdog time out)
        PULM    D,E,X,Y,Z,K     // Restore CPU registers
        RTI                 // Restore CCR and return
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void OSTskResume(INT16U tid)                                // task, ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function can be called by a task or ISR to resume another task which suspended itself using
  OSTskSuspend() macro.
\*---------------------------------------------------------------------------------------------------------*/
{
    os.next_tsk = &os.tsk[tid];         // Get address of specified task's TCB

    OS_ENTER_CRITICAL();                // Protect against interrupts

    if(!(os.ready_mask & os.next_tsk->mask) &&  // If the task is not ready to run and
       !os.next_tsk->pending)                   // is not pending on a sem/msg/mem
    {
        os.ready_mask |= os.next_tsk->mask;     // Make task ready to run

        if(!OS_ISR_RUNNING() &&                 // If not called from ISR and
            tid < os.curr_tid)                  // resumed task is highest priority
        {
            os.next_tid = tid;                  // Prepare for context switch
            OS_TASK_SW();                       // Trigger context switch
        }
    }

    OS_EXIT_CRITICAL();                 // Allow interrupts
}
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
void * OSTskPend(INT16U *pending)                           // RTOS (task)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by the OSTskSuspend() macro, OSSemPend(), OSMsgPend() and OSMemPend to switch
  contexts to the next highest priority task ready to run.  It first makes the current task not ready to run.
  Interrupts are masked before this function is called.  pending will be set by OSSemPend(), OSMsgPend()
  or OSMemPend() to point to the sem/msg/mem pending tasks mask.  For OSTskSuspend() it is NULL.  For
  sem/msg/mem, the function links the task to the sem/msg/mem by setting the relevent bit in the pending
  task mask.

  If pending on a message or memory partition, OSTskReady() will prepare the stack so that message/memory
  block address is in the IY register which passes the return value (void *) to the calling function
  (OSMsgPend() or OSMemPend())
\*---------------------------------------------------------------------------------------------------------*/
{
    os.ready_mask &= ~os.curr_tsk->mask;        // Make current task not ready to run

    if(pending)                     // If pending on sem/msg/mem
    {
        os.curr_tsk->pending = pending;         // Link task to sem/msg/mem pending task mask
        *pending |= os.curr_tsk->mask;          // Link sem/msg/mem to pending task
    }

    asm
    {
    MOVW    os.ready_mask,OS_LSSB       // Pass ready mask to lssb logic
    MOVW    OS_LSSB,os.next_tid         // Store result in next_tid
    }

    os.next_tsk = &os.tsk[os.next_tid];         // Get address of TCB for next task to run

    OS_TASK_SW();                   // Trigger context switch
                                    // Return msg/mem address as void * in register
}
#pragma MESSAGE DEFAULT C1404
/*---------------------------------------------------------------------------------------------------------*/
void OSTskReady(INT16U *pending, void *addr)                        // RTOS (task, ISR)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by OSSemPost(), OSMsgPost(), OSMemPost() to prepare the highest priority pending
  task for execution.  Interrupts are masked before this function is called.  For OSMsgPost() & OSMemPost()
  the message/block address is passed to the pending task via the stack, so that it can be returned from
  OSTskPend() to OSMsgPend() or OSMemPend().
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDX pending                     // X=pending
        MOVW    0,X,OS_LSSB             // Move *pending to lssb logic
        MOVW    OS_LSSB,os.next_tid     // Store result in next_tid
    }

    os.next_tsk      = &os.tsk[os.next_tid];    // Get address of TCB for next task to run
    *pending        &= ~os.next_tsk->mask;      // Clear bit in pending task mask
    os.ready_mask   |=  os.next_tsk->mask;      // Mark task as ready to run
    os.next_tsk->pending = 0;                   // Clear link from task to sem/msg/mem pending mask

    OS_SAVE_ADDR(addr);                 // Pass address via stack (returned from OSTskPend())

    if(!OS_ISR_RUNNING() &&             // If not called from ISR
    os.next_tid < os.curr_tid)          // and pending task is higher priority than curr task
    {
        OS_TASK_SW();                   // Trigger context switch
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void OSTskWatch(void)                               // RTOS (SWI)
/*---------------------------------------------------------------------------------------------------------*\
  This function provides a crude watchpoint system.  It can check to see if a specified location is
  equal or not equal to a given value.  If the condition is true, a hit counter is incremented and the
  task ID and ISR mask are recorded.

  IMPORTANT: It is essential that this doesn't use the E register - if changes are made, check os_tsk.lst
  to be sure.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(((SM_WATCH_CTRL_P & 0x01) && *((INT16U*)SM_WATCH_ADDR_P) == SM_WATCH_VALUE_P) ||
       ((SM_WATCH_CTRL_P & 0x02) && *((INT16U*)SM_WATCH_ADDR_P) != SM_WATCH_VALUE_P))
    {
        SM_WATCH_HITCOUNT_P++;              // Increment hit counter
        SM_WATCH_CTRL_P    = 0;             // Disarm watch
        SM_WATCH_TSKID_P   = SM_TSKID_P;    // Record
        SM_WATCH_ISRMASK_P = SM_ISRMASK_P;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT                 // RTI is explicit in this function
/*---------------------------------------------------------------------------------------------------------*/
void OSTskCtxSwitch(void)                               // RTOS (SWI)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called following an SWI software interrupt (OS_TASK_SW()).  It is used to trigger a
  context switch from task level operation by the functions: OSTskRestart(), OSTskResume(), OSTskPend()
  and OSTskReady().  It saves the registers of the running task on its stack, and switches to the stack
  of the next task to be run.  The identity of this task has been pre-set in os.next_tid and os.next_tsk by
  the function which includes the OS_TASK_SW().  It then executes RTI to resume operation of the new task.
  This function takes 116 cycles or ~11us.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    D,E,X,Y,Z,K         // [16] Save CPU registers for currently running task

        JSR OSTskWatch              // [??] Check Watch Point if active

        LDX os.curr_tsk             // [ 6] Get address of TCB for currently running task
        LDD OS_2US_CLK              // [ 6] D=2us elapsed timer
        CLRW OS_2US_CLK             // [ 6] Reset timer and retrigger watchdog time out
        ADDD OS_TSK_RUN_TIME,X      // [ 6] Add elapsed time to run_time for current task
        STD OS_TSK_RUN_TIME,X       // [ 6] and store back in run_time variable

        STS OS_STACK,X              // [ 4] Save stack pointer of current task in its TCB
        LDX os.next_tsk             // [ 6] Get address of TCB for new task
        STX os.curr_tsk             // [ 6] And make it the current TCB
        LDS OS_STACK,X              // [ 6] Load stack pointer for new task from its TCB

        LDD os.next_tid             // [ 6] Get task ID of new task
        STD os.curr_tid             // [ 6] and make it the current task ID
        STD OS_TID_CODE             // [ 6] and store task ID in the TID code location
        PULM    D,E,X,Y,Z,K         // [18] Restore CPU registers for new task
        RTI                         // [12] Restore CCR and return to task level
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT                 // RTI is explicit in this function
/*---------------------------------------------------------------------------------------------------------*/
void OSTskIntEnter(void)                                // ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to inform NanOS that an interrupt has been received.  NanOS supports only one
  interrupt level, so ISRs are never nested.  This function is JMPed to by the user interrupt intermediate
  vector function after it has:

    - pushed all registers on the stack
    - set E to the TID for the task associated with the interrupt
    - set Y to the 16-bit address of the ISR function (+2)
    - set Z to the page address of the ISR function

  This function will calculate the run time for the current task and will prepare for the timing
  of the ISR.  It will also set the relevant bit in the ISR code word to indicate that this interrupt is
  running.  Every ISR must be associated with a task, so that the ISR run time can be stored in a TCB.
  This limitation makes the code much simpler than having a separate ISR table.  It means that each task
  can have no more than one associated interrupt.

  This function takes 88 cycles (~9us) and the intermediate vector function takes 34 cycles, so the time
  to arrive in the ISR is slightly more than 15us (not including watchpoint).
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        TSKB                    // [ 2] Transfer SK to B
        TBEK                    // [ 2] Transfer B to EK
        TBXK                    // [ 2] Transfer B to XK
        TBYK                    // [ 2] Transfer B to YK

        JSR OSTskWatch          // [??] Check Watch Point if active - this must not corrupt E!

        LDX os.tsk              // [ 6] X = address of TCB table, E = task ID for task associated with the interrupt (0-15)
        LDD #OS_TCB_SIZE        // [ 2] D = sizeof(struct os_tsk)), E = TID
        EMUL                    // [10] E:D = E * D = offset in TCB
        ADX                     // [ 2] X = &os.tsk[isr_task_id]
        LDD OS_MASK,X           // [ 4] D=mask for task
        STD OS_ISR_MASK         // [ 6] Save mask in ISR mask word (for debugging)
        PSHM    X,Y,Z           // [10] Push ISR TCB, ISR Func, PK

        LDY os.curr_tsk         // [ 6] Get address of current task's TCB
        LDD OS_2US_CLK          // [ 6] D=2us elapsed timer
        CLRW OS_2US_CLK         // [ 6] Reset timer and retrigger 6s watchdog time out
        ADDD OS_TSK_RUN_TIME,Y  // [ 6] Add elapsed time to run time for current task
        STD OS_TSK_RUN_TIME,Y   // [ 6] and store back in TCB

        RTS                     // [12] Use RTS to jump to user's ISR
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT                 // RTI is explicit in this function
/*---------------------------------------------------------------------------------------------------------*/
void OSTskIntExit(void)                                 // ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to notify NanOS that an ISR has completed.  All user ISRs must exit via this
  function.  This function is NOT called but jumped to using the macro OS_INT_EXIT().  This is VERY
  important.  Calling OSTskIntExit() will cause the program to crash immediately by misaligning the stack.

  The function will take 96 cycles (9us) if it is returning to the same task, and 140 cycles (14us)
  if switching to a different task (not including watchpoint).

  Notes:        Stack frame upon entry:

                  + 18       PC         \  Call to ISR
                  + 16       CCR:PK     /
                  + 14       D          \
                  + 12       E           |
                  + 10       X           | PSHM D,E,X,Y,Z,K at start of intermediate vector func
                  +  8       Y           |
                  +  6       Z           |
                  +  4       K          /
                  +  2   isr_tsk    ]  Pushed by OSTskIntEnter
               SP +  0
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PULM    X                   // [ 8] Recover TCB for task associated with ISR
        LDD OS_2US_CLK              // [ 6] D=2us elapsed timer
        CLRW    OS_2US_CLK          // [ 6] Reset timer and retrigger slow watchdog time out
        ADDD    OS_ISR_RUN_TIME,X   // [ 6] Add elapsed time to ISR's run time
        STD OS_ISR_RUN_TIME,X       // [ 6] and store back in variable

        MOVW os.ready_mask,OS_LSSB  // [10] Pass os.ready_mask to hardware lssb detector
        LDD OS_LSSB             // [ 6] Read back least sig. set bit index in D (0x0-0xF)
        CPD os.curr_tid         // [ 6] If this is the current task...
        BEQ Continue            // [6,2] continue running the same task

        STD os.curr_tid         // [ 6] Store ID of new task as the current task ID
        STD OS_TID_CODE         // [ 6] and store new task ID in tid code location

        LDX os.curr_tsk         // [ 6] Get address of TCB for current task
        STS OS_STACK,X          // [ 4] Save stack pointer of current task in its TCB
        LDX os.tsk              // [ 6] X = address of TCB table, D = A:B = new task ID (0-15)
        LDAA    #OS_TCB_SIZE    // [ 2] A = sizeof(struct os_tsk))
        MUL                     // [10] D = A * B = address of new task in TCB
        ADX                     // [ 2] X = &os.tsk[os.curr_tid] = address of TCB for new task
        STX os.curr_tsk         // [ 6] Make it the current TCB
        LDS OS_STACK,X          // [ 6] Load stack pointer for new task from its TCB

        Continue:
        JSR OSTskWatch          // [??] Check Watch Point if active

        CLRW    OS_ISR_MASK     // [ 6] Clear task bit in ISR mask word
        PULM    D,E,X,Y,Z,K     // [18] Restore CPU registers
        RTI                     // [12] Restore CCR and return to task or lower level ISR
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: os_tsk.c
\*---------------------------------------------------------------------------------------------------------*/

