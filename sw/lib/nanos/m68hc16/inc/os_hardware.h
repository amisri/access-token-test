/*---------------------------------------------------------------------------------------------------------*\
  File:		os_hardware.h

  Purpose:	Contains NanOS hardware dependant part.
		Constants and macros for MC68HC16Z1 and Hiware's Hicross HC16 compiler

  Author:	Quentin.King@cern.ch

		NanOS was evolved from the larger and more sophisticated RTOS MicroC/OS-II written by
		Jean Labrosse.  See os_tsk.c for more details.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef OS_HARDWARE_H		// header encapsulation
#define OS_HARDWARE_H

#include <cc_types.h>

#define  OS_CRITICAL_METHOD    2

#pragma NO_STRING_CONSTR	// Stop #xxx in macro from making a string literal "xxx" (needed for asm)

#define OS_STACK		0
#define OS_MASK			4
#define OS_TSK_RUN_TIME		6
#define OS_ISR_RUN_TIME		8
#define OS_TCB_SIZE		10
#define OS_ENABLE_INTS()	asm ANDP #0xFF1F
#define OS_DISABLE_INTS()	asm ORP  #0x00E0
#define OS_ENTER_CRITICAL()    {asm PSHM CCR;asm ORP #0x00E0;}
#define OS_EXIT_CRITICAL()	asm PULM CCR
#define OS_INT_EXIT()		asm JMP OSTskIntExit
#define OS_SWI_VEC(vec)		void  os_vec_##vec(void){asm JMP OSTskCtxSwitch;}
#define OS_USR_VEC(vec,tid,isr)	void usr_vec_##vec(void){  \
				asm PSHM D,E,X,Y,Z,K;      \
				asm LDE # tid;             \
				asm LDY @isr:2;            \
				asm LDZ @isr:PAGE;         \
				asm JMP OSTskIntEnter;}

void		OSTskStart	(void);							// main
void		OSTskCtxSwitch	(void);							// RTOS (SWI)

#define     OSTskSuspend()  {                                                                       \
                                OS_ENTER_CRITICAL_GENERIC(cpu_sr_suspend);                          \
                                OSTskPend(0);                                                       \
                                OS_EXIT_CRITICAL_GENERIC(cpu_sr_suspend);                           \
                            }

// For compatibility with FGC3 (FGC2 does not require a store variable).

typedef INT16U                                  OS_CPU_SR;
#define OS_ENTER_CRITICAL_GENERIC(cpu_sr)       OS_ENTER_CRITICAL()
#define OS_EXIT_CRITICAL_GENERIC(cpu_sr)        OS_EXIT_CRITICAL()

#endif	// OS_HARDWARE_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: os_hardware.h
\*---------------------------------------------------------------------------------------------------------*/
