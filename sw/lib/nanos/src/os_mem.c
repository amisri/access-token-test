/*---------------------------------------------------------------------------------------------------------*\
  File:     os_mem.c

  Purpose:  Contains NanOS Memory Partition functions.

  Author:   Quentin.King@cern.ch

  Notes:    This is a very simple implementation of fixed-block size memory partitions.  Each
        partition incorporates a counting semaphore.

        These functions were evolved from the memory partition and message queue functions of
        MicroC/OS-II by Jean Labrosse.  They were simplified to remove the error reporting and
        dynamic creation and deletion of partitions.

        The macro OSMemInit(n_mems) must be included in main() before calls to OSMemCreate()
        to allocate space for the n_mems memory partition control blocks.

        This file is common to the FGC2 and FGC3
\*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>

#include <os.h>

/*---------------------------------------------------------------------------------------------------------*/
struct os_mem * OSMemCreate(void *mem_buf, INT16U n_blks, INT16U blk_size)          // main
/*---------------------------------------------------------------------------------------------------------*\
  This function creates a fixed-size memory partition.  All calls to OSMemCreate() must be after "calling"
  the macro function OSMemInit(n_mems) to set up the memory partition control blocks.  All calls to
  OSMemCreate() must be complete before calling OSTskStart().  The function must not be called more than
  n_mems times, as no check is made to see if there are remaining memory partition control blocks.  The
  function returns a handle for the partition to be used with the other OSMem functions.  blk_size must be
  at least two bytes or four bytes or ... depending on the sizeof(POINTER).
\*---------------------------------------------------------------------------------------------------------*/
{
    void   **   lnk;
    INT8U  *    blk;

    os.mem->free   = mem_buf;           // Link partition with memory buffer
    os.mem->blocks = n_blks;            // Save number of free blocks

    lnk = (void **)mem_buf;             // Create linked list of free memory blocks
    blk = (INT8U *)mem_buf + blk_size;

    while (--n_blks)            // Loop for all but the last block
    {
       *lnk  = blk;             // Link with next block
       lnk  = (void **)blk;
       blk += blk_size;
    }

    return (os.mem++);                  // Return pointer to memory partition control block
    // the late increment is only valid in the case you do OSMemInit(x) with x > 1
}
/*---------------------------------------------------------------------------------------------------------*/
void * OSMemPend(struct os_mem *mem)                                // task
/*---------------------------------------------------------------------------------------------------------*\
  This function gets a free block from the specified memory partition.  If no blocks are available, the
  current task is put to sleep, pending on the partition.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                     // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    void *  blk;

    OS_ENTER_CRITICAL();        // Protect against interrupts

    if (mem->blocks)            // If block is available
    {
        mem->blocks--;                  // Decrement free block counter
        blk = mem->free;                // Get address of next free memory block
        mem->free = *((void **)blk);    // Adjust memory partition control block
    }
    else                        // else message queue is empty
    {
        blk = OSTskPend(&mem->pending); // Pend task on the memory partition
    }

    OS_EXIT_CRITICAL();         // Permit interrupts

    return (blk);               // Return pointer to memory block
}
/*---------------------------------------------------------------------------------------------------------*/
void * OSMemAccept(struct os_mem *mem)                              // task, ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function gets a free block from the specified memory partition.  If no blocks are available, it
  returns NULL.  It never blocks.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                     // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    void *  blk = 0;

    OS_ENTER_CRITICAL();        // Protect against interrupts

    if (mem->blocks)
    {
        mem->blocks--;                  // Decrement free block counter
        blk = mem->free;                // Get address of next free memory block
        mem->free = *((void **)blk);    // Adjust memory partition control block
    }

    OS_EXIT_CRITICAL();         // Permit interrupts

    return(blk);                // Return pointer to memory block
}
/*---------------------------------------------------------------------------------------------------------*/
void OSMemPost(struct os_mem *mem, void *blk)                           // task, ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function returns a block to a memory partition.  If a task or tasks are pending on the partition,
  the highest priority task is given the block and is made ready to run.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                     // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    OS_ENTER_CRITICAL();            // Protect against interrupts

    if (mem->pending)     // If there are tasks pending on this memory partition
    {
        OSTskReady(&mem->pending, blk);     // Ready highest priority pending task and pass block
    }
    else                    // else no tasks are pending
    {
        mem->blocks++;              // Increment blocks counter
        *((void **)blk) = mem->free;        // Insert block at start of linked list of free blocks
        mem->free = blk;            // Adjust memory partition control block
    }

    OS_EXIT_CRITICAL();             // Permit interrupts
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: os_mem.c
\*---------------------------------------------------------------------------------------------------------*/

