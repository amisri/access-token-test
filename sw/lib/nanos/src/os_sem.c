/*---------------------------------------------------------------------------------------------------------*\
  File:     os_sem.c

  Purpose:  Contains NanOS Semaphore functions.

  Author:   Quentin.King@cern.ch

  Notes:    This is a very simple implementation of counting semaphores.  They can be used for resource
        management when initialised to the number of resources, or for signaling between tasks
        when initialised to zero.

        These functions were evolved from the semaphore functions of MicroC/OS-II by Jean Labrosse.
        They were simplified to remove the timeout support, error reporting, and dynamic creation
        and deletion of semaphores.

        The macro OSSemInit(n_sems) must be included in main() before calls to OSSemCreate()
        to allocate space for the n_sems semaphore control blocks.

        This file is common to the FGC2 and FGC3
\*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>

#include <os.h>

/*---------------------------------------------------------------------------------------------------------*/
struct os_sem * OSSemCreate(INT16U sem_count)                           // main
/*---------------------------------------------------------------------------------------------------------*\
  This function creates a counting semaphore.  All calls to OSSemCreate() must be after "calling" the
  macro function OSSemInit(n_sems) to set up the semaphore control blocks.  All calls to OSSemCreate()
  must be complete before calling OSTskStart().  This function must not be called more than n_sems times, as
  no check is made to see if there are remaining semaphore control blocks.  The argument defines the
  initial value for the semaphore.  The function returns a handle for the semaphore to be used with the
  other OSSem functions.
\*---------------------------------------------------------------------------------------------------------*/
{
    os.sem->counter = sem_count;        // Set initial semaphore counter value
    return (os.sem++);                  // Return pointer to semaphore control block
}
/*---------------------------------------------------------------------------------------------------------*/
void OSSemPend(struct os_sem *sem)                              // task
/*---------------------------------------------------------------------------------------------------------*\
  This function will pend the running task on the specified semaphore.  If the semaphore counter is not
  zero, the function will decrement the counter and will return immediately.  If the counter is zero, the
  function will pend the task and will context switch to the highest priority task able to run.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    OS_ENTER_CRITICAL();            // Protect against interrupts

    if(sem->counter)                // If semaphore is available
    {
        sem->counter--;                 // Decrement semaphore counter
    }
    else                            // else semaphore is not available
    {
        OSTskPend(&sem->pending);       // Pend current task on semaphore
    }

    OS_EXIT_CRITICAL();             // Allow interrupts
}
/*---------------------------------------------------------------------------------------------------------*/
void OSSemPost(struct os_sem *sem)                              // task, ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function posts a semaphore.  If there are tasks pending on the semaphore, the highest priority
  pending task is made ready to run.  If no tasks are pending, the semaphore count is incremented.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3         // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    OS_ENTER_CRITICAL();            // Protect against interrupts

    if (sem->pending)               // If there are tasks are pending on this semaphore
    {
        OSTskReady(&sem->pending, 0);   // Make highest priority pending task ready to run
    }
    else                            // else no task are pending
    {
        sem->counter++;                 // Increment semaphore counter
    }

    OS_EXIT_CRITICAL();             // Allow interrupts
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: os_sem.c
\*---------------------------------------------------------------------------------------------------------*/

