/*---------------------------------------------------------------------------------------------------------*\
  File:     os_msg.c

  Purpose:  Contains NanOS Message queue functions.

  Author:   Quentin.King@cern.ch

  Notes:    This is a very simple implementation of FIFO message queues.  Messages are a fixed size,
        in this case, a two byte pointer to void.  They can be used for any two byte value (including
        zero), or as a pointer to any arbitrary structure.

        These functions were evolved from the message queue functions of MicroC/OS-II by Jean
        Labrosse.  They were simplified to remove the timeout support, error reporting, and dynamic
        creation and deletion of message queues.

        The macro OSMsgInit(n_msgs) must be included in main() before calls to OSMsgCreate()
        to allocate space for n_msgs message control blocks.

        Space for the message queue buffers must be defined in the application and passed to
        OSMsgCreate().

        This file is common to the FGC2 and FGC3
\*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>

#include <os.h>

/*---------------------------------------------------------------------------------------------------------*/
struct os_msg * OSMsgCreate(void *buf, INT16U max_entries)                  // main
/*---------------------------------------------------------------------------------------------------------*\
  This function creates a message queue for interprocess communication.  All calls to OSMsgCreate()
  must be after "calling" the macro function OSMsgInit(n_msgs) once to set up the message queue control
  blocks.   All calls to OSMsgCreate() must be complete before calling OSTskStart().  This function must not
  be called more than n_msgs times, as no check is made to see if there are remaining message queue control
  blocks.  The arguments define the address and length of the message queue buffer.
\*---------------------------------------------------------------------------------------------------------*/
{
    os.msg->max_entries = max_entries;  // Set message queue size (number of elements)
    os.msg->in      = buf;              // Set message queue input pointer
    os.msg->out     = buf;              // Set message queue output pointer
    os.msg->start   = buf;              // Set message queue start
    os.msg->end     = buf;              // Set message queue end...
    os.msg->end     += max_entries;     // to first element after queue buffer

    return (os.msg++);                  // Return pointer to message queue control block
}
/*---------------------------------------------------------------------------------------------------------*/
void * OSMsgPend(struct os_msg *msg)                                // task
/*---------------------------------------------------------------------------------------------------------*\
  This function can be used to pend a calling task on a message queue. If the queue is empty, the task
  will be put to sleep, otherwise it will receive the next entry in the queue.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                     // Allocate storage for CPU status register
    OS_CPU_SR   cpu_sr = 0;
#endif
    void  *     message;


    OS_ENTER_CRITICAL();            // Prevent interrupts

    if (msg->entries)               // If message queue contains entries
    {
        msg->entries--;             // Decrement number of messages waiting in queue
        message = *msg->out;        // Extract the next message from queue

        if(++msg->out == msg->end)  // If out pointer is at the end of the buffer
        {
            msg->out = msg->start;  // Wrap out pointer to start of the buffer
        }
    }
    else                    // else message queue is empty
    {
        message = OSTskPend(&msg->pending); // Pend task on the message queue and sleep
    }

    OS_EXIT_CRITICAL();             // Allow interrupts

    return (message);               // Return message
}
/*---------------------------------------------------------------------------------------------------------*/
void OSMsgPost(struct os_msg * msg, void * message)                     // task, ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function checks if any tasks are pending on the queue.  If there are, it passes the message directly
  to the highest priority pending task which is made ready to run, otherwise it puts the message in the
  the queue.  If the queue is already full, the new message is simply discarded.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                     // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    OS_ENTER_CRITICAL();            // Prevent interrupts

    if(msg->pending)                // If there are tasks pending on this message queue
    {
        OSTskReady(&msg->pending, message); // Ready highest priority pending task and pass msg
    }
    else                    // else no tasks are pending
    {
        if(msg->entries < msg->max_entries) // If queue isn't full
        {
            msg->entries++;         // Increment entries counter

            *msg->in = message;     // Insert message at the end of queue

            if(++msg->in == msg->end)       // If in-pointer is at the end
            {
                msg->in = msg->start;       // Wrap in-pointer to start
            }
        }
    }

    OS_EXIT_CRITICAL();             // Allow interrupts
}
/*---------------------------------------------------------------------------------------------------------*/
void OSMsgFlush(struct os_msg *msg)                             // task, ISR
/*---------------------------------------------------------------------------------------------------------*\
  This function flushes the contents of the message queue.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                     // Allocate storage for CPU status register
    OS_CPU_SR  cpu_sr = 0;
#endif

    OS_ENTER_CRITICAL();            // Prevent interrupts

    msg->entries = 0;               // Reset queue entry counter
    msg->out     = msg->in;         // Reset queue pointers

    OS_EXIT_CRITICAL();             // Allow interrupts
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: os_msg.c
\*---------------------------------------------------------------------------------------------------------*/

