/*---------------------------------------------------------------------------------------------------------*\
  File:     os.h

  Purpose:  Contains NanOS constants, macros, typedefs, structures, variables and function prototypes.

  Author:   Quentin.King@cern.ch

  Notes:    This is the header file used by NanOS. It is also used by applications based on NanOS.

        NanOS was evolved from the larger and more sophisticated RTOS MicroC/OS-II written by
        Jean Labrosse.  See os_tsk.c for more details.

        This file is common to the FGC2 and FGC3
\*---------------------------------------------------------------------------------------------------------*/

#ifndef OS_H        // header encapsulation
#define OS_H

#ifdef OS_GLOBALS
    #define OS_VARS_EXT
#else
    #define OS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stddef.h>

#include <os_hardware.h>


//-----------------------------------------------------------------------------------------------------------

typedef uint16_t            OS_STK;     // Stack entry
typedef struct os_tsk       OS_TSK;     // Task control block
typedef struct os_sem       OS_SEM;     // Semaphore control block
typedef struct os_msg       OS_MSG;     // Message queue control block
typedef struct os_mem       OS_MEM;     // Memory partition control block

// Operating System Structures

/*
    REMEMBER stack is stored and calculated based in OS_STK items (uint16_t)
    so any structure inside it must be multiple of uint16_t


    (uint16_t) is 1 time  OS_STK - no problem
    (void *) is 2 times OS_STK - no problem

    for Rx610 the pointer is INT32U so it aligns to INT32U (not uint16_t)

    WARNING !!!
   for M32C87, assembler code at os_cpu.asm (and at OS_INT_ENT) relies on the offsets of os_tsk structure
   for Rx610, assembler code relies on the offsets of this os structure
*/

struct os_tsk                       // Task Control Block (TCB) [10 bytes] [16 bytes]
{                                   // HC16 M32C87 Rx610
    OS_STK *        stack;          // [ 0]  [ 0]  [ 0] Pointer to task stack position
    uint16_t *      pending;        // [ 2]  [ 4]  [ 4] Pointer to Sem/Msg/Mem pending task mask
    uint16_t        mask;           // [ 4]  [ 8]  [ 8] Bit mask for task's priority (0x0001 -> 0x8000)
    uint16_t        task_run_time;  // [ 6]  [10]  [10] Task run time accumulator (units: 2 us)
    uint16_t        isr_run_time;   // [ 8]  [12]  [12] ISR run time accumulator  (units: 2 us)
#ifdef EXTRA_DATA
    uint16_t *      tmp_ptr;        // [ x]  [14]  [16] tmp pointer for returning data from the queues
#endif
};

struct os_sem                   // Semaphore control block [4 bytes]
{
    uint16_t        pending;    // [ 0] Mask for tasks pending on semaphore
    uint16_t        counter;    // [ 2] Semaphore counter
};

struct os_msg                   // Message queue control block [14 bytes] [22 bytes]
{                               // HC16 M32C87 Rx610
    uint16_t        pending;    // [ 0]  [ 0]  [ 0]  Mask for tasks pending on message queue
    uint16_t        max_entries;// [ 2]  [ 2]  [ 2]  Max number of entries the queue can hold
    uint16_t        entries;    // [ 4]  [ 4]  [ 4]  Number of entries now in the queue
    void **         start;      // [ 6]  [ 6]  [ 8]  Pointer to start of message queue buffer
    void **         end;        // [ 8]  [10]  [12]  Pointer to end of message queue buffer
    void **         in;         // [10]  [14]  [16]  Pointer to where next msg will be inserted in the Q
    void **         out;        // [12]  [18]  [20]  Pointer to where next msg will be extracted from the Q
};

struct os_mem                   // Memory partition control block [6 bytes]
{
    uint16_t        pending;    // [ 0] Mask for tasks pending on memory partition
    uint16_t        blocks;     // [ 2] Number of free blocks in the partition
    void *          free;       // [ 4] Pointer to first free memory block
};

/*
   WARNING !!!
   for M32C87, assembler code at os_cpu.asm (and at OS_INT_ENT) relies on the offsets of this os structure
   for Rx610, assembler code relies on the offsets of this os structure
*/

struct os                           // NanOS variables structure [18 bytes] [32 bytes]
{                                   // HC16 M32C87 Rx610
    uint16_t            ready_mask; // [ 0]  [ 0]  [ 0] Mask for tasks ready to run
    uint16_t            curr_tid;   // [ 2]  [ 2]  [ 2] ID of currently running task
    uint16_t            next_tid;   // [ 4]  [ 4]  [ 4] ID of next task to run
    struct os_tsk *     curr_tsk;   // [ 6]  [ 6]  [ 8] Pointer to TCB of currently running task
    struct os_tsk *     next_tsk;   // [ 8]  [10]  [12] Pointer to TCB of next task to run
    struct os_tsk *     tsk;        // [10]  [14]  [16] Pointer to table of task control blocks
    struct os_sem *     sem;        // [12]  [18]  [20] Pointer to table of semaphore control blocks
    struct os_msg *     msg;        // [14]  [22]  [24] Pointer to table of message queue control blocks
    struct os_mem *     mem;        // [16]  [26]  [28] Pointer to table of memory partition control blocks
#ifdef EXTRA_DATA
    uint16_t            int_nesting;// [ x]  [30]  [32] Interrupt nesting level
#endif
};

#ifdef FGC_DEBUG_OS_HISTORY
    // DEBUG: OS HISTORY
    #include <mcu_dependent.h>

    // OS_HISTORY_SIZE must be a power of 2.
    #define OS_HISTORY_SIZE 128

    struct os_history
    {
        uint16_t index;
        struct os_tsk_switch
        {
            uint16_t    tid;
            uint16_t    time;
            uint16_t    event;
            uint16_t    int_nesting;
            uint16_t    last_isr;
            OS_STK*     next_stack;     // next task's stack pointer
            INT32U      psw;            // of the next task
            uint16_t    ipl;            // of the next task
            uint16_t    int_enable;     // of the next task
            INT32U      pc;             // of the next task
            INT32U      pc_indirect;    // of the next task

        } tsk_switch[OS_HISTORY_SIZE];
    };

    struct os_history os_history;

    void OSCtxSnapshot(uint16_t event);
#endif


// Function Macros & Prototypes                                                // Can be called from:


#define     OSTskInit(n)    {static OS_TSK tsk[n];\
                 os.tsk = os.curr_tsk = tsk;\
                 os.curr_tid = 0xCAF0;\
                 os.next_tid = 1; }

#define     OSTskResumeLP(TID) if(!(os.ready_mask&(1<<TID))&&!os.tsk[TID].pending)os.ready_mask|=(1<<TID)

/*
 OSTskResumeLP() is an efficient macro that can resume a lower priority task.  Interrupts must be
 disabled before using the macro.  On the M68HC16 the macro takes 4us, instead of 15us for the function
 OSTskResume().
*/

//-----------------------------------------------------------------------------------------------------------

void        OSTskCreate (void (*func)(void*),void *data,OS_STK *stk,uint16_t stk_size);

// this prototype is in os_hardware.h
//void      OSTskStart  (void);

// ToDo: is necessary to extend also to level 7?
//    OS_ENTER_CRITICAL_7();                // Protect against ALL interrupts

void            OSTskResume (uint16_t tid);
void *          OSTskPend   (uint16_t *pending);
void            OSTskReady  (uint16_t *pending, void *addr);

// this prototype is in os_hardware.h
//void      OSTskCtxSwitch  (void);

void            OSTskIntEnter   (void);
void            OSTskIntExit    (void);

// os_sem.c

#define         OSSemInit(n) { static struct os_sem sem[n]; os.sem=sem; }
struct os_sem * OSSemCreate (uint16_t sem_count);
void            OSSemPend   (struct os_sem *sem);
void            OSSemPost   (struct os_sem *sem);

// os_msg.c

#define         OSMsgInit(n) { static struct os_msg msg[n]; os.msg=msg; }
struct os_msg * OSMsgCreate (void *buf, uint16_t max_entries);
void *          OSMsgPend   (struct os_msg *msg);
void            OSMsgPost   (struct os_msg *msg, void *message);
void            OSMsgFlush  (struct os_msg *msg);

// os_mem.c

#define         OSMemInit(n) { static struct os_mem mem[n]; os.mem=mem; }
struct os_mem * OSMemCreate (void *mem_buf, uint16_t n_blks, uint16_t blk_size);
void *          OSMemPend   (struct os_mem *mem);
void *          OSMemAccept (struct os_mem *mem);
void            OSMemPost   (struct os_mem *mem, void *blk);

//-----------------------------------------------------------------------------------------------------------

// Global variable declaration

OS_VARS_EXT  struct os  os;

//-----------------------------------------------------------------------------------------------------------

#endif  // OS_H : end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: os.h
\*---------------------------------------------------------------------------------------------------------*/
