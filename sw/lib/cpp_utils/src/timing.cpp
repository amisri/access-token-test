//! @file   timing.h
//! @brief  Timing-related utilities.
//! @author Dariusz Zielinski

#include <../include/cpp_utils/timing.h>

using namespace utils;

std::string utils::formatDuration(std::chrono::nanoseconds duration, TimingFormat from, TimingFormat to) noexcept
{
    // Declare const array with ratios.
    static constexpr double ratios[] = { 3600.0 * 1000000000, 60.0 * 1000000000, 1000000000, 1000000, 1000, 1 };

    // Convert duration to nanoseconds.
    auto nano = static_cast<uint64_t>(duration.count());

    // Check that 'from' and 'to' are ok.
    if (from < TimingFormat::hours || from > TimingFormat::nano)
    {
        from = TimingFormat::hours;
    }

    if (to < TimingFormat::hours || to > TimingFormat::nano)
    {
        to = TimingFormat::nano;
    }

    std::stringstream ss;

    // Break-down nanoseconds into components.
    for (auto i = static_cast<uint8_t>(from); i <= static_cast<uint8_t>(to); ++i)
    {
        // Calculate next value.
        auto value = static_cast<uint64_t>(nano / ratios[i]);
        nano -= static_cast<uint64_t>(value * ratios[i]);

        // Print value.
        ss << std::setfill('0') << std::setw( i > static_cast<uint8_t>(TimingFormat::seconds) ? 3 : 2 ) << value;

        // Print delimiter.
        if (i < static_cast<uint8_t>(to))
        {
            ss << (i >= static_cast<uint8_t>(TimingFormat::seconds) ? '.' : ':');
        }
    }

    // Return prepared string.
    return ss.str();
}

// EOF