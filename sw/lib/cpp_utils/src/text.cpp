//! @file   text.h
//! @brief  Functions related to text/string processing
//! @author Dariusz Zielinski

#include <algorithm>
#include <locale>
#include <map>

#include <../include/cpp_utils/text.h>

using namespace utils;

// **********************************************************

void utils::toUpper(std::string& str)
{
    std::for_each(str.begin(), str.end(),
                  [](char &c){ c = static_cast<char>(::toupper(c)); });
}

// **********************************************************

std::string utils::toUpperCopy(std::string_view str)
{
    std::string copy(str);
    toUpper(copy);
    return copy;
}

// **********************************************************

// Convert to lower case (in place)
void utils::toLower(std::string& str)
{
    std::for_each(str.begin(), str.end(),
                  [](char &c){ c = static_cast<char>(::tolower(c)); });
}

// **********************************************************

std::string utils::toLowerCopy(std::string_view str)
{
    std::string copy(str);
    toLower(copy);
    return copy;
}

// **********************************************************

// Trim whitespaces from start (in place)
void utils::trimLeft(std::string& str)
{
    str.erase(str.begin(), std::find_if(str.begin(), str.end(),
                                        [](unsigned char ch) { return !std::isspace(ch); }));
}

// **********************************************************

std::string utils::trimLeftCopy(std::string_view str)
{
    std::string copy(str);
    trimLeft(copy);
    return copy;
}

// **********************************************************

// Trim whitespaces from end (in place)
void utils::trimRight(std::string& str)
{
    str.erase(std::find_if(str.rbegin(), str.rend(),
                           [](unsigned char c) { return !std::isspace(c); }).base(), str.end());
}

// **********************************************************

std::string utils::trimRightCopy(std::string_view str)
{
    std::string copy(str);
    trimRight(copy);
    return copy;
}

// **********************************************************

// Trim whitespaces from both ends (in place)
void utils::trim(std::string& str)
{
    trimRight(str);
    trimLeft(str);
}

// **********************************************************

std::string utils::trimCopy(std::string_view str)
{
    std::string copy(str);
    trimRight(copy);
    trimLeft(copy);
    return copy;
}

// **********************************************************

std::optional<bool> utils::parseBool(std::string_view value)
{
    // Map of what is considered to be true or false
    static std::map<std::string_view, bool> map
    {
        {"0",        false},  {"1",       true},
        {"false",    false},  {"true",    true},
        {"disabled", false},  {"enabled", true},
        {"off",      false},  {"on",      true},
        {"no",       false},  {"yes",     true}
    };

    // Process input value to get trim it and make it lower case
    auto value_processed = trimCopy(value);
    toLower(value_processed);

    // Try to find the value in the map
    auto it = map.find(value);

    // If found, return it (otherwise return nullopt)
    return it != map.end() ? std::optional<bool>(it->second) : std::nullopt;
}

// **********************************************************

std::pair<std::string_view, std::string_view> utils::splitFirst(std::string_view string, char delimiter)
{
    auto split_position = string.find(delimiter);

    auto lhs = string.substr(0, split_position);
    auto rhs = std::string_view();

    if (split_position != std::string::npos)
    {
        rhs = string.substr(split_position + 1);
    }

    return {lhs, rhs};
}

// **********************************************************

std::string_view utils::asStringView(std::span<const std::byte> bytes)
{
    auto* ptr = reinterpret_cast<const char*>(bytes.data());
    return {ptr, bytes.size()};
}

// EOF
