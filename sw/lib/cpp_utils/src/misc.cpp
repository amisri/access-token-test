//! @file   misc.h
//! @brief  Set of different functions without common category.
//! @author Dariusz Zielinski

#include "../include/cpp_utils/misc.h"

using namespace utils;

// **********************************************************

bool utils::setTimeout(const callback::Predicate& predicate, const std::chrono::nanoseconds& timeout,
                const std::chrono::nanoseconds& sleep_time)
{
    auto start_point = std::chrono::high_resolution_clock::now();

    while (!predicate())
    {
        if (std::chrono::high_resolution_clock::now() - start_point > timeout)
        {
            // Timeout occurred
            return false;
        }

        std::this_thread::sleep_for(sleep_time);
    }

    // If we are here (out of loop) then the predicate() has returned true before timeout
    return true;
}

// EOF