file(GLOB_RECURSE source CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_library(cpp_utils STATIC ${source})

target_include_directories(cpp_utils PUBLIC "${PROJECT_ROOT}/include")
target_compile_features(cpp_utils PUBLIC cxx_std_17)