//! @file   priority_mutex.h
//! @brief  Provides a mutex and lock guard with priority settings.
//! @author Dariusz Zielinski

#include "../include/cpp_utils/priorityMutex.h"

using namespace utils;

// **********************************************************

void PriorityMutex::lock(const MutexPriority& mutex_priority)
{
    // Lock the mutex
    std::unique_lock<std::mutex> lock(m_mutex);

    // If this is a high priority thread, then increase number of such threads waiting
    if (mutex_priority == MutexPriority::high)
    {
        m_high_threads_count++;
    }

    // Wait until the lock is not locked and this is high priority thread or there is no such
    m_cond_var.wait(lock, [&]
    {
        return (m_is_locked == false) && (mutex_priority == MutexPriority::high || m_high_threads_count == 0);
    });

    // Now, inside a critical section, mark that the resource is locked
    // and decrease number of high priority threads waiting for the lock
    m_is_locked = true;

    if (mutex_priority == MutexPriority::high)
    {
        m_high_threads_count--;
    }
}

// **********************************************************

void PriorityMutex::unlock()
{
    // Acquire lock
    std::lock_guard<std::mutex> lock(m_mutex);

    // Mark and notify all waiting thread that the resources is no longer locked
    m_is_locked = false;
    m_cond_var.notify_all();
}

// **********************************************************

PriorityLockGuard::PriorityLockGuard(PriorityMutex& mutex, const MutexPriority& mutex_priority)
    : m_mutex(mutex)
{
    m_mutex.lock(mutex_priority);
}

// **********************************************************

PriorityLockGuard::~PriorityLockGuard()
{
    m_mutex.unlock();
}

// EOF