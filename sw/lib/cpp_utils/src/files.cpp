//! @file   files.h
//! @brief  Functions to read and write to files.
//! @author Dariusz Zielinski

#include "../include/cpp_utils/files.h"

using namespace utils;

// **********************************************************

std::string utils::readFromFile(const std::string& file_name)
{
    std::ifstream input_stream(file_name);
    return { std::istreambuf_iterator<char>(input_stream), std::istreambuf_iterator<char>() };
}

// **********************************************************

void utils::writeBinaryToFile(const std::string& file_name, const std::vector<uint8_t>& data, bool request_fsync)
{
    std::ofstream output_stream(file_name, std::ios::out | std::ios::binary);
    output_stream.write(reinterpret_cast<const char*>(data.data()), data.size());

    if (request_fsync)
    {
        output_stream.flush();
        output_stream.rdbuf()->pubsync();
    }
}

// **********************************************************

void utils::writeLinesToFile(const std::string& file_name, const std::vector<std::string>& data)
{
    std::ofstream output_stream(file_name);
    std::copy(data.begin(), data.end(), std::ostream_iterator<std::string>(output_stream, "\n"));
}

// **********************************************************

void utils::writeToFile(const std::string& file_name, const std::string& data)
{
    std::ofstream output_stream(file_name);
    output_stream << data;
}

// **********************************************************

std::vector<uint8_t> utils::readFromBinaryFile(const std::string& file_name)
{
    std::ifstream input_stream(file_name, std::ios::in | std::ios::binary);
    return { std::istreambuf_iterator<char>(input_stream), std::istreambuf_iterator<char>() };
}

// **********************************************************

std::vector<std::string> utils::readLinesFromFile(const std::string& file_name)
{
    std::vector<std::string> result;

    std::ifstream input_stream(file_name);
    for (std::string line; std::getline(input_stream, line);)
    {
        result.push_back(line);
    }

    return result;
}

// EOF