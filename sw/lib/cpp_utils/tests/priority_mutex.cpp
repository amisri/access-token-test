#include <vector>
#include <gtest/gtest.h>

#include <cpp_utils/priorityMutex.h>

using namespace utils;

// Create priority mutex
PriorityMutex mutex;

// Output text from threads
std::string output;

void regularFunc()
{
    PriorityLockGuard lock(mutex);
    output += "Regular";
}

void highFunc()
{
    PriorityLockGuard lock(mutex, MutexPriority::high);
    output += "High";
}

void testPriorityLockingRun()
{
    output = "";

    // Create some thread that will block the mutex for 1 second with regular priority
    std::thread some_thread([&]()
    {
        PriorityLockGuard lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    });
    some_thread.detach();

    // Wait a bit to ensure that the mutex in the some_thread is locked
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    // Now create few threads with regular and high priority.
    // They will add text to this string and the High should always be first
    std::vector<std::thread> threads;
    threads.emplace_back(std::thread(regularFunc));
    threads.emplace_back(std::thread(regularFunc));
    threads.emplace_back(std::thread(highFunc));
    threads.emplace_back(std::thread(highFunc));
    threads.emplace_back(std::thread(highFunc));
    threads.emplace_back(std::thread(regularFunc));
    threads.emplace_back(std::thread(regularFunc));
    threads.emplace_back(std::thread(highFunc));

    // Wait for all threads to finish
    for (auto& thread : threads)
    {
        thread.join();
    }

    // Check that the output string is like that, meaning that high priority threads have always run first.
    EXPECT_EQ("HighHighHighHighRegularRegularRegularRegular", output);
}

TEST(priorityMutexTest, priorityLocking)
{
    // Run the above function few times, just to be better sure
    for (int i = 0; i < 50; ++i)
    {
        testPriorityLockingRun();
    }
}


// EOF