#include <gtest/gtest.h>

#include <cpp_utils/enumStr.h>

using namespace utils;

enum class Direction
{
    left    = 0,
    right   = 1,
    top     = 2,
    bottom  = 3
};

MAKE_ENUM_STR(DirectionStr, Direction,
    { Direction::left,   "LEFT",   "l"},
    { Direction::right,  "RIGHT",  "r"},
    { Direction::top,    "TOP",    "t"},
    { Direction::bottom, "BOTTOM", "b"}
);

enum class Fraction
{
    warrior    = 0,
    elf        = 1,
    sorcer     = 2,
    demon      = 3
};

MAKE_ENUM_STR(FractionStr, Fraction,
    ENUM_STR(Fraction, warrior),
    ENUM_STR(Fraction, elf),
    ENUM_STR(Fraction, sorcer),
    ENUM_STR(Fraction, demon),
);

TEST(enumStrGeneralTest, parseWrongString)
{
    EXPECT_FALSE(DirectionStr::fromString("RIGHT").has_value());
    EXPECT_FALSE(DirectionStr::fromString("right").has_value());
    EXPECT_FALSE(DirectionStr::fromString("r ").has_value());
    EXPECT_FALSE(DirectionStr::fromString("").has_value());
    EXPECT_FALSE(DirectionStr::fromString("RIGHT1").has_value());
    EXPECT_FALSE(DirectionStr::fromString("\nr").has_value());
}

TEST(enumStrGeneralTest, parseWrongStringEnumStrMacro)
{
    EXPECT_FALSE(FractionStr::fromString("warior").has_value());
    EXPECT_FALSE(FractionStr::fromString("warior123").has_value());
    EXPECT_FALSE(FractionStr::fromString("").has_value());
    EXPECT_FALSE(FractionStr::fromString("\t").has_value());
    EXPECT_FALSE(FractionStr::fromString(" warior").has_value());
}

TEST(enumStrGeneralTest, serialize)
{
    EXPECT_EQ("LEFT",   DirectionStr::toString(Direction::left));
    EXPECT_EQ("RIGHT",  DirectionStr::toString(Direction::right));
    EXPECT_EQ("TOP",    DirectionStr::toString(Direction::top));
    EXPECT_EQ("BOTTOM", DirectionStr::toString(Direction::bottom));
    EXPECT_EQ("",       DirectionStr::toString(static_cast<Direction>(10)));
}

TEST(enumStrGeneralTest, serializeEnumStrMacro)
{
    EXPECT_EQ("Fraction::warrior",   FractionStr::toString(Fraction::warrior));
    EXPECT_EQ("Fraction::elf",       FractionStr::toString(Fraction::elf));
    EXPECT_EQ("Fraction::sorcer",    FractionStr::toString(Fraction::sorcer));
    EXPECT_EQ("Fraction::demon",     FractionStr::toString(Fraction::demon));
    EXPECT_EQ("",                    FractionStr::toString(static_cast<Fraction>(10)));
}

// EOF