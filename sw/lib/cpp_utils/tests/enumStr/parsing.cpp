#include <gtest/gtest.h>

#include <cpp_utils/enumStr.h>

using namespace utils;


// *********************************************************************************************
//! Test MAKE_ENUM_STR macro

enum class Direction
{
    left    = 0,
    right   = 1,
    top     = 2,
    bottom  = 3
};

MAKE_ENUM_STR(DirectionStr, Direction,
    { Direction::left,   "LEFT",   "l"},
    { Direction::right,  "RIGHT",  "r"},
    { Direction::top,    "TOP",    "t"},
    { Direction::bottom, "BOTTOM", "b"}
);

class DirectionParamFixture : public testing::TestWithParam<std::tuple<const std::string, Direction>> {};

TEST_P(DirectionParamFixture, parseMakeEnumStr) {
    auto [input, result] = GetParam();

    EXPECT_TRUE(DirectionStr::fromString(input).has_value());
    EXPECT_EQ(result, *DirectionStr::fromString(input));
}

INSTANTIATE_TEST_SUITE_P(makeEnumStrMacro,
                         DirectionParamFixture,
                         testing::Values(
                             std::make_tuple("l", Direction::left),
                             std::make_tuple("L", Direction::left),
                             std::make_tuple("r", Direction::right),
                             std::make_tuple("R", Direction::right),
                             std::make_tuple("T", Direction::top),
                             std::make_tuple("t", Direction::top),
                             std::make_tuple("B", Direction::bottom),
                             std::make_tuple("b", Direction::bottom)
                        ));

// *********************************************************************************************
//! Test ENUM_STR macro

enum class Fraction
{
    warrior    = 0,
    elf        = 1,
    sorcer     = 2,
    demon      = 3
};

MAKE_ENUM_STR(FractionStr, Fraction,
    ENUM_STR(Fraction, warrior),
    ENUM_STR(Fraction, elf),
    ENUM_STR(Fraction, sorcer),
    ENUM_STR(Fraction, demon),
);

class FractionParamFixture  : public testing::TestWithParam<std::tuple<const std::string, Fraction>> {};

TEST_P(FractionParamFixture, parseEnumStr) {
    auto [input, result] = GetParam();


    EXPECT_TRUE(FractionStr::fromString(input).has_value());
    EXPECT_EQ(result, *FractionStr::fromString(input));
}

INSTANTIATE_TEST_SUITE_P(enumStrMacro,
                         FractionParamFixture,
                         testing::Values(
                             std::make_tuple("warrior", Fraction::warrior),
                             std::make_tuple("WArRIoR", Fraction::warrior),
                             std::make_tuple("sorcer",  Fraction::sorcer),
                             std::make_tuple("SORCER",  Fraction::sorcer),
                             std::make_tuple("ELF",     Fraction::elf),
                             std::make_tuple("elF",     Fraction::elf),
                             std::make_tuple("demon",   Fraction::demon),
                             std::make_tuple("DemoN",   Fraction::demon)
                        ));

// EOF