#include <optional>
#include <variant>
#include <vector>
#include <sstream>
#include <gtest/gtest.h>

#include <cpp_utils/serializable.h>
#include <cpp_utils/StreamSerializer.h>

using namespace utils;

struct ExampleNumber
{
    SERIALIZABLE
    (
        Item("age",   age),
        Item("param",  param)
    )

    double                        param  = 2.5;
    int                           age    = 42;
};

TEST(serializableTest, serializeNumber)
{
    std::stringstream ss;
    ExampleNumber obj;

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({age = 42, param = 2.5})", ss.str());
}

struct ExampleString
{
    SERIALIZABLE
    (
        Item("name",   name)
    )

    std::string                   name   = "Example";
};

TEST(serializableTest, serializeString)
{
    std::stringstream ss;
    ExampleString obj;

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({name = "Example"})", ss.str());
}

struct ExampleTuple
{
    SERIALIZABLE
    (
        Item("rec",    record)
    )

    std::tuple<int, std::string>  record = {10, "Mark"};
};

TEST(serializableTest, serializeTuple)
{
    std::stringstream ss;
    ExampleTuple obj;

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({rec = {10, "Mark"}})", ss.str());
}

struct ExampleVector
{
    SERIALIZABLE
    (
        Item("grades", grades)
    )

    std::vector<int>            grades = { 5, 6, 2, 1 };
};

TEST(serializableTest, serializeVector)
{
    std::stringstream ss;
    ExampleVector obj;

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({grades = [5, 6, 2, 1]})", ss.str());
}

struct Point
{
    SERIALIZABLE
    (
        Item("X", x),
        Item("Y", y)
    );

    int x;
    int y;
};

struct ExampleNested
{
    SERIALIZABLE
    (
        Item("points", points)
    )

    std::vector<Point>            points = { {1, 2}, {5, 6} };
};

TEST(serializableTest, serializeNested)
{
    std::stringstream ss;
    ExampleNested obj;

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({points = [{X = 1, Y = 2}, {X = 5, Y = 6}]})", ss.str());
}

struct ExampleCombination
{
    SERIALIZABLE
    (
        Item("points", points),
        Item("name",   name),
        Item("param",  param),
        Item("rec",    record)
    )

    std::vector<Point>            points = { {1, 2}, {5, 6} };
    std::tuple<int, std::string>  record = {10, "Mark"};
    std::string                   name   = "Example";
    double                        param  = 2.5;
    int                           age    = 42;
};

TEST(serializableTest, serializeCombination)
{
    std::stringstream ss;
    ExampleCombination obj;

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({points = [{X = 1, Y = 2}, {X = 5, Y = 6}], name = "Example", param = 2.5, rec = {10, "Mark"}})", ss.str());
}

// **********************************************************

struct Optional
{
    SERIALIZABLE
    (
        Item("value", value)
    )

    std::optional<int> value = std::nullopt;
};

TEST(serializableTest, emptyOptional)
{
    std::stringstream ss;
    Optional obj{};

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({value = null})", ss.str());
}

TEST(serializableTest, setOptional)
{
    std::stringstream ss;
    Optional obj{10};

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({value = 10})", ss.str());
}

// **********************************************************

struct Variant
{
    SERIALIZABLE
    (
        Item("value", value)
    )

    std::variant<std::monostate, int, char> value = {};
};

TEST(serializableTest, variantMonostate)
{
    std::stringstream ss;
    Variant obj{};

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({value = null})", ss.str());
}

TEST(serializableTest, vairantValue)
{
    std::stringstream ss;
    Variant obj{100};

    obj.serialize<utils::StdSerializer>(ss);

    EXPECT_EQ(R"({value = 100})", ss.str());
}

// EOF
