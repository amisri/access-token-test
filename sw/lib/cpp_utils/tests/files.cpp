#include <cpp_utils/files.h>
#include <filesystem>
#include <fstream>
#include <gtest/gtest.h>

using namespace utils;
namespace fs = std::filesystem;

constexpr auto test_file_name = "test_file.txt";

class FileFixture : public ::testing::Test {
    protected:
        void TearDown() override {
            if (fs::exists(test_file_name))
            {
                fs::remove(test_file_name);
            }
        }
};

TEST_F(FileFixture, readStringNotExistingFile)
{
    ASSERT_FALSE(fs::exists(test_file_name));
    auto content = readFromFile(test_file_name);

    EXPECT_EQ("", content);
}

TEST_F(FileFixture, readLinesNotExistingFile)
{
    ASSERT_FALSE(fs::exists(test_file_name));
    auto content = readLinesFromFile(test_file_name);

    EXPECT_EQ(std::vector<std::string>(), content);
}

TEST_F(FileFixture, readBinaryNotExistingFile)
{
    ASSERT_FALSE(fs::exists(test_file_name));
    auto content = readFromBinaryFile(test_file_name);

    EXPECT_EQ(std::vector<uint8_t>(), content);
}

TEST_F(FileFixture, readStringEmptyFile)
{
    std::ofstream test_file(test_file_name);
    ASSERT_TRUE(fs::exists(test_file_name));

    auto content = readFromFile(test_file_name);

    EXPECT_EQ("", content);
}

TEST_F(FileFixture, readLinesEmptyFile)
{
    std::ofstream test_file(test_file_name);
    ASSERT_TRUE(fs::exists(test_file_name));

    auto content = readLinesFromFile(test_file_name);

    EXPECT_EQ(std::vector<std::string>(), content);
}

TEST_F(FileFixture, readBinaryEmptyFile)
{
    std::ofstream test_file(test_file_name);
    ASSERT_TRUE(fs::exists(test_file_name));

    auto content = readFromBinaryFile(test_file_name);

    EXPECT_EQ(std::vector<uint8_t>(), content);
}

TEST_F(FileFixture, stringFileRead)
{
    std::ofstream test_file(test_file_name);
    std::string input_text = "A B C D E This is just a \ttest \n with a new line";

    test_file << input_text;
    test_file.flush();
    auto content = readFromFile(test_file_name);

    EXPECT_EQ(input_text, content);
}

TEST_F(FileFixture, stringFileWrite)
{
    std::string random_text = "A B C D E This is just a test \n with a new line";

    writeToFile(test_file_name, random_text);
    auto content = readFromFile(test_file_name);

    EXPECT_EQ(random_text, content);
}

TEST_F(FileFixture, linesFileRead)
{
    std::ofstream file(test_file_name);
    std::vector<std::string> random_lines =
    {
        "A B C D E This is just a test",
        "with a few new lines",
        "like this one",
        "and this one"
    };

    for(auto line : random_lines)
        file << line << "\n";
    file.flush();
    auto content = readLinesFromFile(test_file_name);

    EXPECT_EQ(random_lines, content);
}

TEST_F(FileFixture, linesFileWrite)
{
    std::vector<std::string> random_lines =
    {
        "A B C D E This is just a test",
        "with a few new lines",
        "like this one",
        "and this one"
    };

    writeLinesToFile(test_file_name, random_lines);
    auto content = readLinesFromFile(test_file_name);

    EXPECT_EQ(random_lines, content);
}

TEST_F(FileFixture, binaryFileRead)
{
    std::ofstream file(test_file_name);
    std::vector<uint8_t> random_bytes =
    {
        0xFF, 0xAA, 0x99, 0xFF, 0xAA, 0x99, 0xFF, 0xAA, 0x99, 0xFF, 0xAA, 0x99
    };

    for(auto byte : random_bytes)
        file << byte;
    file.flush();
    auto content = readFromBinaryFile(test_file_name);

    EXPECT_EQ(random_bytes, content);
}

TEST_F(FileFixture, binaryFileWrite)
{
    std::vector<uint8_t> random_bytes =
    {
        0xFF, 0xAA, 0x99, 0xFF, 0xAA, 0x99, 0xFF, 0xAA, 0x99, 0xFF, 0xAA, 0x99
    };

    writeBinaryToFile(test_file_name, random_bytes);
    auto content = readFromBinaryFile(test_file_name);

    EXPECT_EQ(random_bytes, content);
}

// EOF