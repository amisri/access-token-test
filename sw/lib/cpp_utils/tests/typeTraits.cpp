#include <tuple>
#include <iostream>
#include <sstream>
#include <string_view>
#include <vector>
#include <gtest/gtest.h>

#include <cpp_utils/typeTraits.h>

using namespace utils;

struct Foo {};

TEST(typeTraitsTest, isPair)
{
    EXPECT_TRUE((is_pair_v<std::pair<int, int>>));
    EXPECT_TRUE((is_pair_v<std::pair<int, bool>>));
    EXPECT_TRUE((is_pair_v<std::pair<std::string, int>>));
    EXPECT_TRUE((is_pair_v<std::pair<std::string, Foo>>));

    EXPECT_FALSE(is_pair_v<int>);
    EXPECT_FALSE(is_pair_v<std::string>);
    EXPECT_FALSE(is_pair_v<Foo>);
}

TEST(typeTraitsTest, isTuple)
{
    EXPECT_TRUE((is_tuple_v<std::tuple<int, bool>>));
    EXPECT_TRUE((is_tuple_v<std::tuple<int>>));
    EXPECT_TRUE((is_tuple_v<std::tuple<std::string, int, Foo>>));
    EXPECT_TRUE((is_tuple_v<std::tuple<std::string, Foo>>));

    EXPECT_FALSE(is_tuple_v<int>);
    EXPECT_FALSE(is_tuple_v<std::string>);
    EXPECT_FALSE(is_tuple_v<Foo>);

    EXPECT_FALSE((is_tuple_v<std::pair<int, int>>));
    EXPECT_FALSE((is_tuple_v<std::pair<int, bool>>));
    EXPECT_FALSE((is_tuple_v<std::pair<std::string, int>>));
    EXPECT_FALSE((is_tuple_v<std::pair<std::string, Foo>>));
}

TEST(typeTraitsTest, isStringLike)
{
    EXPECT_FALSE((is_string_like_v<std::tuple<std::string, int, Foo>>));
    EXPECT_FALSE((is_string_like_v<std::tuple<std::string>>));
    EXPECT_FALSE((is_string_like_v<std::tuple<char>>));
    EXPECT_FALSE((is_string_like_v<std::pair<std::string, std::string>>));

    EXPECT_TRUE(is_string_like_v<std::string>);
    EXPECT_TRUE(is_string_like_v<char*>);
    EXPECT_TRUE(is_string_like_v<const char*>);
    EXPECT_TRUE(is_string_like_v<std::string_view>);
}

TEST(typeTraitsTest, isIterable)
{
    EXPECT_FALSE((is_iterable_v<std::tuple<std::string, int, Foo>>));
    EXPECT_FALSE((is_iterable_v<std::tuple<std::string>>));
    EXPECT_FALSE((is_iterable_v<std::tuple<char>>));
    EXPECT_FALSE((is_iterable_v<std::pair<std::string, std::string>>));
    EXPECT_FALSE(is_iterable_v<char*>);
    EXPECT_FALSE(is_iterable_v<const char*>);
    EXPECT_FALSE(is_iterable_v<Foo>);

    EXPECT_TRUE(is_iterable_v<std::string_view>);
    EXPECT_TRUE(is_iterable_v<std::vector<int>>);
    EXPECT_TRUE(is_iterable_v<std::vector<Foo>>);
    EXPECT_TRUE((is_iterable_v<std::vector<Foo>>));
}

// EOF