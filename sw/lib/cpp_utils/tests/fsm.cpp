#include <chrono>
#include <future>
#include <gtest/gtest.h>
#include <thread>
#include <vector>

#include <cpp_utils/enumStr.h>
#include <cpp_utils/fsm.h>

using namespace utils;
using namespace std::chrono_literals;

enum class EtherState
{
    idle,
    first_packet,
    last_packet
};

// **********************************************************
//! Utilities to test fsm  

// classes to log order of function calls in fsm
enum class CallType
{
    state,
    transition
};

struct Calltrace
{
    EtherState state;
    CallType type;

    bool operator==(const Calltrace& other) const
    {
        return state == other.state && type == other.type;
    }
};

// utilities to provide pretty printing for error messages
MAKE_ENUM_STR(StateStr, EtherState,
    ENUM_STR(EtherState, idle),
    ENUM_STR(EtherState, first_packet),
    ENUM_STR(EtherState, last_packet),
);

MAKE_ENUM_STR(CallTypeStr, CallType,
    ENUM_STR(CallType, state),
    ENUM_STR(CallType, transition),
);

std::ostream& operator<<(std::ostream& os, const Calltrace& object) {
    return os << StateStr::toString(object.state) << ", " << CallTypeStr::toString(object.type);
}

// **********************************************************
//! Implementing fsm using cpp_utils/fsm.h library

class FgcEtherResponse;
using TransRes = utils::FsmTransitionResult<EtherState>;
using StateFunc = void (FgcEtherResponse::*)();
using TransitionFunc = FsmTransitionResult<EtherState> (FgcEtherResponse::*)();
class FgcEtherResponse
{
    public:

        explicit FgcEtherResponse(EtherState initstate) : m_fsm(*this, initstate) {};

        const std::vector<Calltrace>& getCallstack() const {
            return callstack;
        }

        void update()
        {
            m_fsm.update();
        }

        void addState(const EtherState& state, const StateFunc& state_func, const std::vector<TransitionFunc>& transitions)
        {
            m_fsm.addState(state, state_func, transitions);
        }

        EtherState getState() const
        {
            return m_fsm.getState();
        }

        void packet_flag(bool flag)
        {
            _packet_flag = flag;
        }

        void idleState();
        void firstPacketState();
        void lastPacketState();
        TransRes idleToFirstPacket();
        TransRes xxToLastPacket();
        TransRes lastPacketToIdle();
        TransRes wrongPacket();

    private:
        bool _packet_flag;
        utils::Fsm<EtherState, FgcEtherResponse> m_fsm;
        std::vector<Calltrace> callstack;
};

TransRes FgcEtherResponse::idleToFirstPacket()
{
    callstack.push_back({EtherState::idle, CallType::transition}); 
    if (m_fsm.getState() == EtherState::idle)
    {
        return TransRes(EtherState::first_packet);
    }

    return {};
}

TransRes FgcEtherResponse::xxToLastPacket()
{
    callstack.push_back({EtherState::first_packet, CallType::transition}); 
    if (m_fsm.getState() == EtherState::first_packet)
    {
        return TransRes(EtherState::last_packet, FsmCascade);
    }

    return {};
}

TransRes FgcEtherResponse::wrongPacket()
{
    callstack.push_back({EtherState::first_packet, CallType::transition}); 
    if (m_fsm.getState() == EtherState::first_packet && _packet_flag)
    {
        return TransRes(EtherState::idle);
    }

    return {};
}

TransRes FgcEtherResponse::lastPacketToIdle()
{
    callstack.push_back({EtherState::last_packet, CallType::transition}); 
    if (m_fsm.getState() == EtherState::last_packet)
    {
        return TransRes(EtherState::idle);
    }

    return {};
}

void FgcEtherResponse::idleState()       { callstack.push_back({EtherState::idle,         CallType::state}); }
void FgcEtherResponse::firstPacketState(){ callstack.push_back({EtherState::first_packet, CallType::state}); }
void FgcEtherResponse::lastPacketState() { callstack.push_back({EtherState::last_packet,  CallType::state}); }

TEST(fsmTest, transitionResult)
{
    EXPECT_EQ(EtherState::idle,         TransRes(EtherState::idle).state());
    EXPECT_EQ(EtherState::first_packet, TransRes(EtherState::first_packet).state());
    EXPECT_TRUE(                        TransRes(EtherState::first_packet).stateChanged());
    EXPECT_TRUE(                        TransRes(EtherState::last_packet).stateChanged());
    EXPECT_TRUE(                        TransRes(EtherState::first_packet, FsmCascade).cascade());
    EXPECT_FALSE(                       TransRes(EtherState::last_packet).cascade());
}


TEST(fsmTest, emptyStates)
{
    std::vector<Calltrace> callstack;
    FgcEtherResponse state(EtherState::idle);

    state.update();

    callstack = state.getCallstack();
    EXPECT_EQ(0, callstack.size());
    EXPECT_EQ(EtherState::idle, state.getState());
}

TEST(fsmTest, initStateWithNoTransition)
{
    std::vector<Calltrace> callstack;
    FgcEtherResponse state(EtherState::last_packet);

    state.addState(EtherState::idle,         &FgcEtherResponse::idleState,        { &FgcEtherResponse::idleToFirstPacket });
    state.addState(EtherState::first_packet, &FgcEtherResponse::firstPacketState, { &FgcEtherResponse::xxToLastPacket    });
    

    EXPECT_EQ(EtherState::last_packet, state.getState());
    state.update();
    EXPECT_EQ(EtherState::last_packet, state.getState());
    state.update();
    EXPECT_EQ(EtherState::last_packet, state.getState());

    callstack = state.getCallstack();
    EXPECT_EQ(0, callstack.size());
}

// *********************************************************************************************
//! Graph of finite state machine used in next test
//!                                                      cascade transition
//!                               |>>>>>>>>>>>>>>>>>>>>>> xxToLastPacket >>>>>>>>>>>>>>>>>>>>>>|
//!                               |                                                            |
//!     |> idleToFirstPacket >|   |                                                            |
//!     |                     |   |                                                            |
//!  ********              ****************                                             ***************
//!  * IDLE *              * FIRST PACKET *                                             * LAST PACKET *
//!  ********              ****************                                             ***************
//!     |                                                                                      |
//!     |<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< lastPacketToIdle <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<|
//!
TEST(fsmTest, normalOperation)
{
    std::vector<Calltrace> callstack;
    FgcEtherResponse state(EtherState::idle);

    state.addState(EtherState::idle,         &FgcEtherResponse::idleState,        { &FgcEtherResponse::idleToFirstPacket });
    state.addState(EtherState::first_packet, &FgcEtherResponse::firstPacketState, { &FgcEtherResponse::xxToLastPacket    });
    state.addState(EtherState::last_packet,  &FgcEtherResponse::lastPacketState,  { &FgcEtherResponse::lastPacketToIdle  });
    

    EXPECT_EQ(EtherState::idle,         state.getState());
    state.update();
    EXPECT_EQ(EtherState::first_packet, state.getState());
    state.update();
    EXPECT_EQ(EtherState::idle,         state.getState());

    callstack = state.getCallstack();

    ASSERT_EQ(6, callstack.size());
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::state}),          callstack[0]);
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::transition}),     callstack[1]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::state}),          callstack[2]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::transition}),     callstack[3]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::state}),          callstack[4]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::transition}),     callstack[5]);
}

TEST(fsmTest, nullStateFunction)
{
    std::vector<Calltrace> callstack;
    FgcEtherResponse state(EtherState::idle);

    state.addState(EtherState::idle,         &FgcEtherResponse::idleState,        { &FgcEtherResponse::idleToFirstPacket });
    state.addState(EtherState::first_packet, nullptr,                             { &FgcEtherResponse::xxToLastPacket    });
    state.addState(EtherState::last_packet,  &FgcEtherResponse::lastPacketState,  { &FgcEtherResponse::lastPacketToIdle  });
    

    EXPECT_EQ(EtherState::idle,         state.getState());
    state.update();
    EXPECT_EQ(EtherState::first_packet, state.getState());
    state.update();
    EXPECT_EQ(EtherState::idle,         state.getState());

    callstack = state.getCallstack();

    ASSERT_EQ(5, callstack.size());
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::state}),          callstack[0]);
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::transition}),     callstack[1]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::transition}),     callstack[2]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::state}),          callstack[3]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::transition}),     callstack[4]);
}

TEST(fsmTest, emptyStateTransition)
{
    std::vector<Calltrace> callstack;
    FgcEtherResponse state(EtherState::idle);

    state.addState(EtherState::idle,         &FgcEtherResponse::idleState,        { });
    state.addState(EtherState::first_packet, &FgcEtherResponse::firstPacketState, { &FgcEtherResponse::xxToLastPacket    });
    state.addState(EtherState::last_packet,  &FgcEtherResponse::lastPacketState,  { &FgcEtherResponse::lastPacketToIdle  });
    

    EXPECT_EQ(EtherState::idle, state.getState());
    state.update();
    EXPECT_EQ(EtherState::idle, state.getState());
    state.update();
    EXPECT_EQ(EtherState::idle, state.getState());

    callstack = state.getCallstack();

    ASSERT_EQ(2, callstack.size());
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::state}),          callstack[0]);
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::state}),          callstack[1]);
}

// *********************************************************************************************
//! Graph of finite state machine used in next test
//!                                                         cascade transition
//!                                       |>>>>>>>>>>>>>>>>>> xxToLastPacket >>>>>>>>>>>>>>>>>>|
//!                                       |                                                    |
//!     |>>>>> idleToFirstPacket >>>>>|   |                                                    |
//!     |                             |   |                                                    |
//!  ********      flag == 1      ****************                                     ***************
//!  * IDLE * <<< wrongPacket <<< * FIRST PACKET *                                     * LAST PACKET *
//!  ********                     ****************                                     ***************
//!     |                                                                                      |
//!     |<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< lastPacketToIdle <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<|
//!

TEST(fsmTest, manyTranslations)
{
    std::vector<Calltrace> callstack;
    FgcEtherResponse state(EtherState::idle);

    state.addState(EtherState::idle,         &FgcEtherResponse::idleState,        { &FgcEtherResponse::idleToFirstPacket });
    state.addState(EtherState::first_packet, &FgcEtherResponse::firstPacketState, { &FgcEtherResponse::wrongPacket, &FgcEtherResponse::xxToLastPacket });
    state.addState(EtherState::last_packet,  &FgcEtherResponse::lastPacketState,  { &FgcEtherResponse::lastPacketToIdle  });
    

    EXPECT_EQ(EtherState::idle,         state.getState());
    state.update();
    EXPECT_EQ(EtherState::first_packet, state.getState());
    state.packet_flag(false);
    state.update();
    EXPECT_EQ(EtherState::idle,         state.getState());
    state.update();
    EXPECT_EQ(EtherState::first_packet, state.getState());
    state.packet_flag(true);
    state.update();
    EXPECT_EQ(EtherState::idle,         state.getState());

    callstack = state.getCallstack();

    ASSERT_EQ(11, callstack.size());
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::state}),          callstack[0]);
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::transition}),     callstack[1]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::state}),          callstack[2]);
    // First check wrongPacket transtion
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::transition}),     callstack[3]);
    // Then check xxToLastPacket transtion
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::transition}),     callstack[4]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::state}),          callstack[5]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::transition}),     callstack[6]);
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::state}),          callstack[7]);
    EXPECT_EQ((Calltrace{EtherState::idle,         CallType::transition}),     callstack[8]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::state}),          callstack[9]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::transition}),     callstack[10]);
}

// **********************************************************

//! Graph of finite state machine used in next test
//!
//!                              cascade transition
//!            |>>>>>>>>>>>>>>>>>> xxToLastPacket >>>>>>>>>>>>>>>>>>|
//!            |                                                    |
//!            |                                                    |
//!            |                                                    |
//!    ****************                                     ***************
//!    * FIRST PACKET *                                     * LAST PACKET *
//!    ****************                                     ***************
//!

TEST(fsmTest, noTransitionAfterCascade)
{
    // **********************************************************
    // Preparation
    std::promise<bool> completed;
    auto completed_future = completed.get_future();
    auto callstack = std::vector<Calltrace>();
    auto state     = FgcEtherResponse(EtherState::first_packet);
    state.addState(EtherState::first_packet, &FgcEtherResponse::firstPacketState, { &FgcEtherResponse::xxToLastPacket });
    state.addState(EtherState::last_packet,  &FgcEtherResponse::lastPacketState,  {                                   });

    // **********************************************************
    // Execution
    std::thread([&state, &callstack, &completed]()
        {
            state.update();
            callstack = state.getCallstack();
            completed.set_value(true);
        }).detach();
    auto result = completed_future.wait_for(5s);

    // **********************************************************
    // Verification
    ASSERT_EQ(result, std::future_status::ready);
    ASSERT_EQ(3, callstack.size());
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::state      }), callstack[0]);
    EXPECT_EQ((Calltrace{EtherState::first_packet, CallType::transition }), callstack[1]);
    EXPECT_EQ((Calltrace{EtherState::last_packet,  CallType::state      }), callstack[2]);
}

// EOF
