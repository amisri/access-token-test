#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <thread>

#include <cpp_utils/Result.h>

using namespace utils;

TEST(ResultTests, defaultConstructor)
{
    Result<int, std::string> result{};

    ASSERT_EQ(result.hasValue(), true);
    ASSERT_EQ(result.hasError(), false);
    EXPECT_EQ(*result, int{});
    EXPECT_EQ(result.value(), int{});
}

// **********************************************************

TEST(ResultTests, valueConstructor)
{
    Result<int, std::string> result{10};

    ASSERT_EQ(result.hasValue(), true);
    ASSERT_EQ(result.hasError(), false);
    EXPECT_EQ(*result, 10);
    EXPECT_EQ(result.value(), 10);
}

// **********************************************************

TEST(ResultTests, errorConstructor)
{
    Result<int, std::string> result{Failure{"Error"}};

    ASSERT_EQ(result.hasValue(), false);
    ASSERT_EQ(result.hasError(), true);
    EXPECT_EQ(result.error(), "Error");
}

// **********************************************************

struct TestType
{
    int         fiz;
    std::string bar;

    bool operator==(const TestType&) const = default;
};

TEST(ResultFailureTests, variadicConstructor)
{

    Result<int, TestType> result   = Failure<TestType>{10, "TEST"};
    TestType              expected = TestType{10, "TEST"};

    ASSERT_EQ(result.hasValue(), false);
    ASSERT_EQ(result.hasError(), true);
    EXPECT_EQ(result.error(), expected);
}

// **********************************************************

TEST(ResultTests, equalValueOperator)
{
    {
        Result<int, std::string> result{10};
        int                      other{10};

        EXPECT_EQ(result == other, true);
    }

    {
        Result<int, std::string> result{10};
        int                      other{11};

        EXPECT_EQ(result == other, false);
    }

    {
        Result<int, std::string> result{10};
        Failure<std::string>     other{"Error"};

        EXPECT_EQ(result == other, false);
    }
}

// **********************************************************

TEST(ResultTests, equalFailureOperator)
{
    {
        Result<int, std::string> result{Failure{"Error"}};
        Failure<std::string>     other{"Error"};

        EXPECT_EQ(result == other, true);
    }

    {
        Result<int, std::string> result{Failure{"Error"}};
        Failure<std::string>     other{"Other"};

        EXPECT_EQ(result == other, false);
    }

    {
        Result<int, std::string> result{Failure{"Error"}};
        int                      other{10};

        EXPECT_EQ(result == other, false);
    }
}

// **********************************************************

TEST(ResultTests, equalResultOperator)
{
    // **********************************************************
    // Result with value
    // **********************************************************

    {
        Result<int, std::string> result{10};
        Result<int, std::string> other{10};

        EXPECT_EQ(result == other, true);
    }

    {
        Result<int, std::string> result{10};
        Result<int, std::string> other{11};

        EXPECT_EQ(result == other, false);
    }

    {
        Result<int, std::string> result{10};
        Result<int, std::string> other{Failure{"Error"}};

        EXPECT_EQ(result == other, false);
    }

    // **********************************************************
    // Result with error
    // **********************************************************

    {
        Result<int, std::string> result{Failure{"Error"}};
        Result<int, std::string> other{Failure{"Error"}};

        EXPECT_EQ(result == other, true);
    }

    {
        Result<int, std::string> result{Failure{"Error"}};
        Result<int, std::string> other{Failure{"Other"}};

        EXPECT_EQ(result == other, false);
    }

    {
        Result<int, std::string> result{Failure{"Error"}};
        Result<int, std::string> other{10};

        EXPECT_EQ(result == other, false);
    }
}

// **********************************************************

TEST(ResultTests, mapMethodLvalue)
{
    {
        Result<int, std::string> result{Failure{"Error"}};

        auto mapped = result.map(
            [](int w)
            {
                return w + 1;
            }
        );

        EXPECT_EQ(result, Failure<std::string>{"Error"});
    }

    {
        Result<int, std::string> result{10};

        auto mapped = result.map(
            [](int w)
            {
                return w + 1;
            }
        );

        EXPECT_EQ(mapped, 11);
    }
}

// **********************************************************

TEST(ResultTests, mapMethodRValue)
{
    {
        auto result = Result<int, std::string>{Failure{"Error"}}.map(
            [](int w)
            {
                return w + 1;
            }
        );

        EXPECT_EQ(result, Failure<std::string>{"Error"});
    }

    {
        auto result = Result<int, std::string>{10}.map(
            [](int w)
            {
                return w + 1;
            }
        );

        EXPECT_EQ(result, 11);
    }
}

// **********************************************************

TEST(ResultTests, pointerOperator)
{
    {
        Result<std::string, std::string> result{"Value"};

        EXPECT_EQ(*result, "Value");
        EXPECT_EQ(result->size(), 5);
    }

    {
        const Result<std::string, std::string> result{"Value"};

        EXPECT_EQ(*result, "Value");
        EXPECT_EQ(result->size(), 5);
    }
}

// **********************************************************

TEST(ResultTests, pointerOperatorConst)
{
    {
        Result<std::string, std::string> result{"Value"};

        EXPECT_EQ(*result, "Value");
        EXPECT_EQ(result->size(), 5);
    }

    {
        const Result<std::string, std::string> result{"Value"};

        EXPECT_EQ(*result, "Value");
        EXPECT_EQ(result->size(), 5);
    }
}

// **********************************************************

TEST(ResultTests, boolConversion)
{
    {
        Result<int, std::string> result{10};

        EXPECT_EQ(static_cast<bool>(result), true);
    }

    {
        Result<int, std::string> result{Failure{"Error"}};

        EXPECT_EQ(static_cast<bool>(result), false);
    }
}

// **********************************************************
// Result<void, std::string>
// **********************************************************

TEST(ResultVoidTests, defaultConstructor)
{
    Result<void, std::string> result{};

    ASSERT_EQ(result.hasValue(), true);
    ASSERT_EQ(result.hasError(), false);
}

// **********************************************************

TEST(ResultVoidTests, errorConstructor)
{
    Result<void, std::string> result{Failure{"Error"}};

    ASSERT_EQ(result.hasValue(), false);
    ASSERT_EQ(result.hasError(), true);
    EXPECT_EQ(result.error(), "Error");
}

// **********************************************************

TEST(ResultVoidTests, equalOperator)
{
    {
        Result<void, std::string> result{Failure{"Error"}};
        Failure<std::string>      other{"Error"};

        EXPECT_EQ(result == other, true);
    }

    {
        Result<void, std::string> result{Failure{"Error"}};
        Failure<std::string>      other{"Other"};

        EXPECT_EQ(result == other, false);
    }
}

// **********************************************************

TEST(ResultVoidTests, boolConversion)
{
    {
        Result<void, std::string> result{};

        EXPECT_EQ(static_cast<bool>(result), true);
    }
    {
        Result<void, std::string> result{Failure{"Error"}};

        EXPECT_EQ(static_cast<bool>(result), false);
    }
}

// EOF
