#include <thread>
#include <gtest/gtest.h>

#include <cpp_utils/timing.h>

using namespace utils;

TEST(timingTest, formatDurationNormal)
{
    EXPECT_EQ("01",                   formatDuration(std::chrono::seconds(3661), TimingFormat::hours, TimingFormat::hours));
    EXPECT_EQ("01:01",                formatDuration(std::chrono::seconds(3661), TimingFormat::hours, TimingFormat::minutes));
    EXPECT_EQ("01:01:01",             formatDuration(std::chrono::seconds(3661), TimingFormat::hours, TimingFormat::seconds));
    EXPECT_EQ("01:01:01.000",         formatDuration(std::chrono::seconds(3661), TimingFormat::hours, TimingFormat::milli));
    EXPECT_EQ("01:01:01.000.000",     formatDuration(std::chrono::seconds(3661), TimingFormat::hours, TimingFormat::micro));
    EXPECT_EQ("01:01:01.000.000.000", formatDuration(std::chrono::seconds(3661), TimingFormat::hours, TimingFormat::nano));

    EXPECT_EQ("61",                   formatDuration(std::chrono::seconds(3661), TimingFormat::minutes, TimingFormat::minutes));
    EXPECT_EQ("61:01",                formatDuration(std::chrono::seconds(3661), TimingFormat::minutes, TimingFormat::seconds));
    EXPECT_EQ("61:01.000",            formatDuration(std::chrono::seconds(3661), TimingFormat::minutes, TimingFormat::milli));

    EXPECT_EQ("3661",                 formatDuration(std::chrono::seconds(3661), TimingFormat::seconds, TimingFormat::seconds));
    EXPECT_EQ("3661.000",             formatDuration(std::chrono::seconds(3661), TimingFormat::seconds, TimingFormat::milli));

    EXPECT_EQ("01.005.501",           formatDuration(std::chrono::microseconds(1005501), TimingFormat::seconds, TimingFormat::micro));
    EXPECT_EQ("1005",                 formatDuration(std::chrono::microseconds(1005501), TimingFormat::milli, TimingFormat::milli));
}


TEST(timingTest, formatDurationWrongFromTo)
{
    auto invalidFormat = static_cast<TimingFormat>(10);

    // From is smaller than to - empty string should be returned
    EXPECT_EQ("", formatDuration(std::chrono::seconds(3661), TimingFormat::seconds, TimingFormat::hours));

    // Non-existing From (hours should be used)
    EXPECT_EQ("01:01:01", formatDuration(std::chrono::seconds(3661), invalidFormat, TimingFormat::seconds));

    // Non-existing To (nanoseconds should be used)
    EXPECT_EQ("5000.000", formatDuration(std::chrono::microseconds(5000), TimingFormat::micro, invalidFormat));

    // Non-existing From and To (hours should be used)
    EXPECT_EQ("01:01:01.000.000.000", formatDuration(std::chrono::seconds(3661), invalidFormat, invalidFormat));
}

class MockClock
{
    public:
        typedef std::chrono::nanoseconds           duration;
        typedef duration::rep                      rep;
        typedef duration::period                   period;
        typedef std::chrono::time_point<MockClock> time_point;
        static  constexpr bool                     is_steady = true;

        static void set(duration time_to_set)
        {
            m_time = time_to_set;
        }

        static time_point now() noexcept
        {
            return time_point(m_time);
        }

    private:
        static duration m_time;
};

MockClock::duration MockClock::m_time(0);


TEST(timingTest, timerNormalOperation)
{
    Timer<MockClock> timer;

    timer.start();
    MockClock::set(std::chrono::seconds(10));
    timer.stop();
    MockClock::set(std::chrono::seconds(20));

    EXPECT_TRUE(std::chrono::seconds(10) == timer.get());
}


TEST(timingTest, timerNotStarted)
{
    Timer<MockClock> timer;

    MockClock::set(std::chrono::seconds(10));
    EXPECT_TRUE(std::chrono::seconds(0) == timer.get());
}


TEST(timingTest, timerNotStopped)
{
    Timer<MockClock> timer;
    timer.start();

    MockClock::set(std::chrono::seconds(10));
    EXPECT_TRUE(std::chrono::seconds(10) == timer.get());

    MockClock::set(std::chrono::seconds(20));
    EXPECT_TRUE(std::chrono::seconds(20) == timer.get());

    MockClock::set(std::chrono::seconds(30));
    EXPECT_TRUE(std::chrono::seconds(30) == timer.get());

    timer.stop();
    MockClock::set(std::chrono::seconds(40));

    EXPECT_TRUE(std::chrono::seconds(30) == timer.get());
}

TEST(timingTest, timerStreamPrinting)
{
    Timer<MockClock> timer("SomeName");
    std::stringstream output;

    timer.start();
    MockClock::set(std::chrono::seconds(10));
    timer.stop();

    timer.print(output);
    EXPECT_EQ("SomeName time = 10000.000", output.str());
}

TEST(timingTest, timerPrinting)
{
    Timer<MockClock> timer("SomeName");

    timer.start();
    MockClock::set(std::chrono::seconds(10));
    timer.stop();

    EXPECT_EQ("SomeName time = 10000.000", timer.print());
}


TEST(timingTest, scopedTimer)
{
    std::stringstream ss;

    {
        ScopedTimer<MockClock> timer(ss, "SomeName");
        MockClock::set(std::chrono::seconds(10));
    }

    EXPECT_EQ("SomeName time = 10000.000", ss.str());
}


TEST(timingTest, chainedTimer)
{
    ChainedTimer<MockClock> timer("SomeName");
    timer.start();

    MockClock::set(std::chrono::seconds(11));
    timer.addPoint("1");

    MockClock::set(std::chrono::seconds(12));
    timer.addPoint("2");

    MockClock::set(std::chrono::seconds(13));
    timer.addPoint("3");

    auto expected = "SomeName time point 1 = 11.000.000 (11.000.000)\n"
                    "SomeName time point 2 = 12.000.000 (01.000.000)\n"
                    "SomeName time point 3 = 13.000.000 (01.000.000)";
    EXPECT_EQ(expected, timer.print());
}

// EOF