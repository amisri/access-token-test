#include <tuple>
#include <stdexcept>
#include <gtest/gtest.h>

#include <cpp_utils/misc.h>

using namespace utils;

class Foo : public Noncopyable
{
};

class Bar : public NoncopyableNonmovable
{
};

TEST(miscTest, nonCopyable)
{
    EXPECT_FALSE(std::is_copy_constructible_v<Foo>);
    EXPECT_FALSE(std::is_copy_assignable_v<Foo>);
    EXPECT_TRUE(std::is_move_constructible_v<Foo>);
    EXPECT_TRUE(std::is_move_assignable_v<Foo>);
}

TEST(miscTest, nonCopyableNonMovable)
{
    EXPECT_FALSE(std::is_copy_constructible_v<Bar>);
    EXPECT_FALSE(std::is_copy_assignable_v<Bar>);
    EXPECT_FALSE(std::is_move_constructible_v<Bar>);
    EXPECT_FALSE(std::is_move_assignable_v<Bar>);
}

TEST(miscTest, finally)
{
    bool called = false;

    {
        Finally setXTo1([&]()
        {
            called = true;
        });
    }

    EXPECT_TRUE(called);
}

class TestException : std::exception
{
};

TEST(miscTest, finallyException)
{
    bool called = false;
    
    ASSERT_ANY_THROW(
        Finally setXTo1([&]()
        {
            called = true;
        });
        throw TestException();
    );

    EXPECT_TRUE(called);
}

TEST(miscTest, zeroTimeout)
{
    int counter = 0;

    EXPECT_FALSE(setTimeout([&](){ 
        ++counter;
        return false;
    }, std::chrono::milliseconds(0)));

    EXPECT_EQ(1, counter);
}

TEST(miscTest, functionCalledBeforeCheckingTime)
{
    int counter = 0;

    EXPECT_FALSE(setTimeout([&](){ 
        ++counter;
        return false;
    }, std::chrono::milliseconds(10), std::chrono::seconds(1)));

    EXPECT_EQ(2, counter);
}

TEST(miscTest, timeOutNotTimingout)
{
    bool flag = false;

    std::thread th([&]()
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        flag = true;
    });

    EXPECT_TRUE(setTimeout([&](){ return flag == true;}, std::chrono::seconds(1))) << "Info: `setTimeOut` timed out when it shouldn't.";

    th.join();
}

TEST(miscTest, timeOutTimingout)
{
    bool flag = false;

    std::thread th([&]()
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        flag = true;
    });

    EXPECT_FALSE(setTimeout([&](){ return flag == true;}, std::chrono::milliseconds(1))) << "Info: `setTimeOut` didn't time out when it should.";

    th.join();
}

// **********************************************************

TEST(CyclicIntTests, withoutWrapping)
{
    // **********************************************************
    // Preparation
    CyclicInt<4> counter = {};


    // **********************************************************
    // Execution
    ++counter;
    ++counter;


    // **********************************************************
    // Verification
    EXPECT_EQ(counter, 2);
}

// **********************************************************

TEST(CyclicIntTests, wrapping)
{
    // **********************************************************
    // Preparation
    CyclicInt<4> counter = {};


    // **********************************************************
    // Execution
    ++counter;
    ++counter;
    ++counter;
    ++counter;


    // **********************************************************
    // Verification
    EXPECT_EQ(counter, 0);
}

// **********************************************************

TEST(CyclicIntTests, conversionConstructor)
{
    // **********************************************************
    // Preparation
    CyclicInt<6> counter = 3;


    // **********************************************************
    // Execution
    ++counter;
    ++counter;


    // **********************************************************
    // Verification
    EXPECT_EQ(counter, 5);
}

// **********************************************************

TEST(CyclicIntTests, conversionAssigmnet)
{
    // **********************************************************
    // Preparation
    CyclicInt<6> counter = {};
    counter = 3;


    // **********************************************************
    // Execution
    ++counter;
    ++counter;


    // **********************************************************
    // Verification
    EXPECT_EQ(counter, 5);
}


// EOF
