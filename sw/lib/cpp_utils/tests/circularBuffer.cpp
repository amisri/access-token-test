#include <thread>
#include <gtest/gtest.h>

#include <cpp_utils/circularBuffer.h>

using namespace utils;


TEST(circularBufferTest, emptyOnInit)
{
    CircularBuffer<int, 4> buffer;

    
    ASSERT_TRUE(buffer.isEmpty());
}

TEST(circularBufferTest, popEmptyBuffer)
{
    CircularBuffer<int, 4> buffer;

    
    auto element = buffer.pop();


    EXPECT_FALSE(element.has_value());
}

TEST(circularBufferTest, normalOperation)
{
    CircularBuffer<int, 4> buffer;


    buffer.push(1);
    buffer.push(2);
    buffer.push(3);


    EXPECT_FALSE(buffer.isEmpty());

    EXPECT_EQ(1, *buffer.pop());
    EXPECT_EQ(2, *buffer.popBlock());
    EXPECT_EQ(3, *buffer.popBlock());

    EXPECT_TRUE(buffer.isEmpty());
}

TEST(circularBufferTest, fullBuffer)
{
    CircularBuffer<int, 4> buffer;


    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    buffer.push(4);


    EXPECT_FALSE(buffer.isEmpty());
}

TEST(circularBufferTest, overflowBuffer)
{
    CircularBuffer<int, 4> buffer;


    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    buffer.push(4);


    EXPECT_FALSE(buffer.isEmpty());

    EXPECT_EQ(1, *buffer.pop());
    EXPECT_EQ(2, *buffer.pop());
    EXPECT_EQ(3, *buffer.pop());

    EXPECT_TRUE(buffer.isEmpty());
}

TEST(circularBufferTest, overflowBufferTwice)
{
    CircularBuffer<int, 4> buffer;


    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    buffer.push(4);
    buffer.push(5);
    buffer.push(6);
    buffer.push(7);
    buffer.push(8);
    buffer.push(9);


    EXPECT_FALSE(buffer.isEmpty());

    EXPECT_EQ(1, *buffer.pop());
    EXPECT_EQ(2, *buffer.pop());
    EXPECT_EQ(3, *buffer.pop());

    EXPECT_TRUE(buffer.isEmpty());
}

TEST(circularBufferTest, pushFromThreads)
{
    CircularBuffer<int, 4> buffer;


    std::thread th1([&buffer]()
    {
        buffer.push(1);
        buffer.push(2);
    });

    std::thread th2([&buffer]()
    {
        buffer.push(3);
        buffer.push(4);
    });

    th1.join();
    th2.join();


    EXPECT_FALSE(buffer.isEmpty());
}

TEST(circularBufferTest, popBlock)
{
    CircularBuffer<int, 4> buffer;
    int number = 0;


    std::thread th1([&]()
    {
        number = *buffer.popBlock();
    });

    std::thread th2([&]()
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        buffer.push(1);
    });

    th1.join();
    th2.join();


    EXPECT_EQ(1, number);
}

TEST(circularBufferTest, pushBlock)
{
    CircularBuffer<int, 4> buffer;


    buffer.push(1);
    buffer.push(2);
    buffer.push(3);

    std::thread th1([&]()
    {
        buffer.pushBlock(4);
    });

    // thread not finished, still waits to push an element
    EXPECT_TRUE(th1.joinable());
    EXPECT_EQ(1, *buffer.pop());

    th1.join();
    EXPECT_EQ(2, *buffer.pop());
    EXPECT_EQ(3, *buffer.pop());
    EXPECT_EQ(4, *buffer.pop());
    EXPECT_TRUE(buffer.isEmpty());
}

TEST(circularBufferTest, emptyBufferDisable)
{
    CircularBuffer<int, 4> buffer;
    bool valid;


    std::thread th1([&]()
    {
        valid = buffer.popBlock().has_value();
    });

    std::thread th2([&]()
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        buffer.unblockAndDisable();
    });

    th1.join();
    th2.join();


    EXPECT_FALSE(valid);
    EXPECT_FALSE(buffer.push(1));
    EXPECT_TRUE(buffer.isEmpty());
    buffer.pushBlock(1);
    EXPECT_TRUE(buffer.isEmpty());
}

TEST(circularBufferTest, filledBufferDisable)
{
    CircularBuffer<int, 4> buffer;
    ASSERT_TRUE(buffer.push(1));
    ASSERT_TRUE(buffer.push(2));
    ASSERT_TRUE(buffer.push(3));

    std::thread th1([&]()
    {
        buffer.pushBlock(4);
    });

    std::thread th2([&]()
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        buffer.unblockAndDisable();
    });

    th1.join();
    th2.join();
    EXPECT_FALSE(buffer.pop().has_value());
    EXPECT_FALSE(buffer.popBlock().has_value());
}

TEST(circularBufferTest, bufferLongUsage)
{
    CircularBuffer<int, 4> buffer;


    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    // 1 2 3 

    buffer.pop();
    buffer.push(4);
    // 2 3 4

    buffer.pop();
    buffer.pop();
    // 4

    buffer.push(5);
    buffer.push(6);
    // 4 5 6


    EXPECT_FALSE(buffer.isEmpty());
    EXPECT_EQ(4, *buffer.pop());
    EXPECT_EQ(5, *buffer.pop());
    EXPECT_EQ(6, *buffer.pop());
}

// EOF