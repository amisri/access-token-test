#include <vector>
#include <sstream>
#include <gtest/gtest.h>

#include <cpp_utils/flagSet.h>

using namespace utils;

enum class EnumMock {
    b0,
    b1,
    b2,
    b3,
    b4,
    COUNT
};

TEST(flagSetGeneralSuit, testNoneFlagSet)
{
    FlagSet<EnumMock> fs;

    EXPECT_TRUE(fs.none());
}

TEST(flagSetGeneralSuit, testClear)
{
    FlagSet<EnumMock> fs;

    fs = EnumMock::b0;
    fs.clear();

    EXPECT_TRUE(fs.none());
}

TEST(flagSetGeneralSuit, testReassign)
{
    FlagSet<EnumMock> fs;

    fs = EnumMock::b0;

    EXPECT_TRUE(fs.has(EnumMock::b0));

    fs = EnumMock::b1;

    EXPECT_TRUE (fs.has(EnumMock::b1));
    EXPECT_FALSE(fs.has(EnumMock::b0));
}

TEST(flagSetGeneralSuit, testComparison)
{
    FlagSet<EnumMock> fs1, fs2, fs3;

    fs1.add(                EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               );
    fs2.add(                EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               );
    fs3.add(                EnumMock::b0, EnumMock::b1, EnumMock::b2              , EnumMock::b4 );

    EXPECT_TRUE (fs1 == fs1);
    EXPECT_TRUE (fs1 == fs2);
    EXPECT_TRUE (fs2 == fs1);
    EXPECT_FALSE(fs1 == fs3);
}

TEST(flagSetGeneralSuit, testConversionConstructor)
{
    FlagSet<EnumMock> fs(  EnumMock::b0 |              EnumMock::b2 |             EnumMock::b4  );

    EXPECT_TRUE( fs.hasAll(EnumMock::b0,               EnumMock::b2,              EnumMock::b4  ));
    EXPECT_FALSE(fs.hasAny(              EnumMock::b1,               EnumMock::b3               ));
}

TEST(flagSetGeneralSuit, testCopyConstructor)
{
    FlagSet<EnumMock> f1(  EnumMock::b0 |              EnumMock::b2 |             EnumMock::b4  );
    FlagSet<EnumMock> f2 = f1;

    EXPECT_TRUE( f2.hasAll(EnumMock::b0,               EnumMock::b2,              EnumMock::b4  ));
    EXPECT_FALSE(f2.hasAny(              EnumMock::b1,               EnumMock::b3               ));
}

TEST(flagSetGeneralSuit, testAdd)
{
    FlagSet<EnumMock> fs;

    fs.add(EnumMock::b0);

    EXPECT_TRUE( fs.has(   EnumMock::b0                                                         ));
    EXPECT_FALSE(fs.hasAny(              EnumMock::b1, EnumMock::b2, EnumMock::b3, EnumMock::b4 ));

    fs.add(                EnumMock::b0, EnumMock::b1,               EnumMock::b3               );

    EXPECT_TRUE( fs.hasAll(EnumMock::b0, EnumMock::b1,               EnumMock::b3               ));
    EXPECT_FALSE(fs.hasAny(                            EnumMock::b2,               EnumMock::b4 ));
}

TEST(flagSetGeneralSuit, testRemove)
{
    FlagSet<EnumMock> fs;

    fs.add(                EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               );
    fs.remove(             EnumMock::b0,                             EnumMock::b3, EnumMock::b4 );

    EXPECT_FALSE(fs.hasAny(EnumMock::b0,                             EnumMock::b3, EnumMock::b4 ));
    EXPECT_TRUE( fs.hasAll(              EnumMock::b1, EnumMock::b2                             ));
}

TEST(flagSetGeneralSuit, testSubscript)
{
    FlagSet<EnumMock> fs;

    fs.add(EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3);

    EXPECT_TRUE( static_cast<bool>(fs[EnumMock::b0]));
    EXPECT_TRUE( static_cast<bool>(fs[EnumMock::b0]));
    EXPECT_TRUE( static_cast<bool>(fs[EnumMock::b1]));
    EXPECT_TRUE( static_cast<bool>(fs[EnumMock::b2]));
    EXPECT_TRUE( static_cast<bool>(fs[EnumMock::b3]));
    EXPECT_FALSE(static_cast<bool>(fs[EnumMock::b4]));

    fs[EnumMock::b0] = false;
    EXPECT_FALSE( static_cast<bool>(fs[EnumMock::b0]));
}

TEST(flagSetGeneralSuit, testSubstraction)
{
    FlagSet<EnumMock> fs1, fs2, result;

    fs1.add(                    EnumMock::b0, EnumMock::b1,               EnumMock::b3, EnumMock::b4 );
    fs2.add(                    EnumMock::b0,                             EnumMock::b3               );

    result = fs1 - fs2;

    EXPECT_TRUE( result.hasAll(              EnumMock::b1,                             EnumMock::b4 ));
    EXPECT_FALSE(result.hasAny(EnumMock::b0,               EnumMock::b2, EnumMock::b3               ));

    ASSERT_TRUE( fs1.hasAll(   EnumMock::b0, EnumMock::b1,               EnumMock::b3, EnumMock::b4 ));
    ASSERT_FALSE(fs1.hasAny(                               EnumMock::b2                             ));

    fs1 -= fs2;

    EXPECT_TRUE( fs1.hasAll(                 EnumMock::b1,                             EnumMock::b4 ));
    EXPECT_FALSE(fs1.hasAny(   EnumMock::b0,               EnumMock::b2, EnumMock::b3               ));
}

TEST(flagSetGeneralSuit, testBitwiseOr)
{
    FlagSet<EnumMock> fs1, fs2, result;

    fs1.add(                                  EnumMock::b1,               EnumMock::b3              );
    fs2.add(                    EnumMock::b0,               EnumMock::b2, EnumMock::b3              );

    result = fs1 | fs2;

    EXPECT_TRUE( result.hasAll(EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               ));
    EXPECT_FALSE(result.hasAny(                                                        EnumMock::b4 ));

    ASSERT_TRUE( fs1.hasAll(                 EnumMock::b1,               EnumMock::b3               ));
    ASSERT_FALSE(fs1.hasAny(   EnumMock::b0,               EnumMock::b2,               EnumMock::b4 ));

    fs1 |= fs2;

    EXPECT_TRUE( fs1.hasAll(   EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               ));
    EXPECT_FALSE(fs1.hasAny(                                                           EnumMock::b4 ));
}

TEST(flagSetGeneralSuit, testBitwiseOrEnum)
{
    EnumMock e1, e2;
    FlagSet<EnumMock> fs;

    e1 = EnumMock::b0;
    e2 = EnumMock::b4;

    fs = e1 | e2;

    EXPECT_TRUE( fs.hasAll(    EnumMock::b0,                                           EnumMock::b4 ));
    EXPECT_FALSE(fs.hasAny(                  EnumMock::b1, EnumMock::b2, EnumMock::b3               ));
}

TEST(flagSetGeneralSuit, testAddition)
{
    FlagSet<EnumMock> fs1, fs2, result;

    fs1.add(                                 EnumMock::b1,               EnumMock::b3              );
    fs2.add(                   EnumMock::b0,               EnumMock::b2, EnumMock::b3              );

    result = fs1 + fs2;

    EXPECT_TRUE( result.hasAll(EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               ));
    EXPECT_FALSE(result.hasAny(                                                        EnumMock::b4 ));

    ASSERT_TRUE( fs1.hasAll(                 EnumMock::b1,               EnumMock::b3               ));
    ASSERT_FALSE(fs1.hasAny(   EnumMock::b0,               EnumMock::b2,               EnumMock::b4 ));

    fs1 += fs2;

    EXPECT_TRUE( fs1.hasAll(   EnumMock::b0, EnumMock::b1, EnumMock::b2, EnumMock::b3               ));
    EXPECT_FALSE(fs1.hasAny(                                                           EnumMock::b4 ));
}

TEST(flagSetGeneralSuit, testPrint)
{
    std::stringstream ss;
    FlagSet<EnumMock> fs;

    fs.add(EnumMock::b0, EnumMock::b1, EnumMock::b4);

    ss << fs;
    //         43210
    EXPECT_EQ("10011", ss.str());
}

// EOF