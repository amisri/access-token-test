#include <cpp_utils/flagSet.h>
#include <gtest/gtest.h>

using namespace utils;

enum class EnumMock {
    b0,
    b1,
    b2,
    b3,
    b4,
    COUNT
};

TEST(flagSetConstexprText, subscriptEmpty)
{
    constexpr FlagSet<EnumMock> fs;

    EXPECT_EQ(false, fs[EnumMock::b0]);
    EXPECT_EQ(false, fs[EnumMock::b1]);
    EXPECT_EQ(false, fs[EnumMock::b2]);
    EXPECT_EQ(false, fs[EnumMock::b3]);
    EXPECT_EQ(false, fs[EnumMock::b4]);
}

TEST(flagSetConstexprText, subscriptInitialized)
{
    constexpr FlagSet<EnumMock> fs (EnumMock::b2);

    EXPECT_EQ(false, fs[EnumMock::b0]);
    EXPECT_EQ(false, fs[EnumMock::b1]);
    EXPECT_EQ(true,  fs[EnumMock::b2]);
    EXPECT_EQ(false, fs[EnumMock::b3]);
    EXPECT_EQ(false, fs[EnumMock::b4]);
}

TEST(flagSetConstexprText, subscriptOverflow)
{
    constexpr FlagSet<EnumMock> fs (static_cast<EnumMock>(10));

    EXPECT_EQ(false, fs[EnumMock::b0]);
    EXPECT_EQ(false, fs[EnumMock::b1]);
    EXPECT_EQ(false, fs[EnumMock::b2]);
    EXPECT_EQ(false, fs[EnumMock::b3]);
    EXPECT_EQ(false, fs[EnumMock::b4]);
}

TEST(flagSetConstexprText, hasEmpty)
{
    constexpr FlagSet<EnumMock> fs;

    EXPECT_EQ(false, fs.has(EnumMock::b0));
    EXPECT_EQ(false, fs.has(EnumMock::b1));
    EXPECT_EQ(false, fs.has(EnumMock::b2));
    EXPECT_EQ(false, fs.has(EnumMock::b3));
    EXPECT_EQ(false, fs.has(EnumMock::b4));
}

TEST(flagSetConstexprText, hasInitialized)
{
    constexpr FlagSet<EnumMock> fs (EnumMock::b2);

    EXPECT_EQ(false, fs.has(EnumMock::b0));
    EXPECT_EQ(false, fs.has(EnumMock::b1));
    EXPECT_EQ(true,  fs.has(EnumMock::b2));
    EXPECT_EQ(false, fs.has(EnumMock::b3));
    EXPECT_EQ(false, fs.has(EnumMock::b4));
}

TEST(flagSetConstexprText, hasOverflow)
{
    constexpr FlagSet<EnumMock> fs (static_cast<EnumMock>(10));

    EXPECT_EQ(false, fs.has(EnumMock::b0));
    EXPECT_EQ(false, fs.has(EnumMock::b1));
    EXPECT_EQ(false, fs.has(EnumMock::b2));
    EXPECT_EQ(false, fs.has(EnumMock::b3));
    EXPECT_EQ(false, fs.has(EnumMock::b4));
}

// EOF