#include <cpp_utils/text.h>
#include <gtest/gtest.h>

using namespace utils;

class TextParseTrueTest  : public testing::TestWithParam<const char*> {};
class TextParseFalseTest : public testing::TestWithParam<const char*> {};
class TextParseWrongTest : public testing::TestWithParam<const char*> {};

TEST_P(TextParseTrueTest, parseTrue) {
    auto result = parseBool(GetParam());

    ASSERT_TRUE(result.has_value());
    EXPECT_TRUE(result.value());
}

INSTANTIATE_TEST_SUITE_P(testTruePattern,
                         TextParseTrueTest,
                         testing::Values("yes", "1", "true", "enabled"));

TEST_P(TextParseFalseTest, parseFalse) {
    auto result = parseBool(GetParam());

    ASSERT_TRUE(result.has_value());
    EXPECT_FALSE(result.value());
}

INSTANTIATE_TEST_SUITE_P(testFalsePattern,
                         TextParseFalseTest,
                         testing::Values("no", "0", "false", "disabled"));

TEST_P(TextParseWrongTest, parseWrong) {
    auto result = parseBool(GetParam());

    EXPECT_FALSE(result.has_value());
}

INSTANTIATE_TEST_SUITE_P(testWrongPattern,
                         TextParseWrongTest,
                         testing::Values("false1", "FALSE", "yes\t", "", " no"));
// EOF