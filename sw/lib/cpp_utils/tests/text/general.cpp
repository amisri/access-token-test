#include <cpp_utils/text.h>
#include <gtest/gtest.h>

using namespace utils;

TEST(textGeneralTest, textUpperCase)
{
    std::string s1, s2;
    s1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper.";

    s2 = toUpperCopy(s1);
    toUpper(s1);
    
    EXPECT_EQ("LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. INTEGER SEMPER.", s1);
    EXPECT_EQ("LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. INTEGER SEMPER.", s2);
}

TEST(textGeneralTest, textLowerCase)
{
    std::string s1, s2;
    s1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper.";

    s2 = toLowerCopy(s1);
    toLower(s1);

    EXPECT_EQ("lorem ipsum dolor sit amet, consectetur adipiscing elit. integer semper.", s1);
    EXPECT_EQ("lorem ipsum dolor sit amet, consectetur adipiscing elit. integer semper.", s2);
}

TEST(textGeneralTest, textTrimLeft)
{
    std::string s1, s2;
    s1 = " \f\n\r\t\vLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper. \f\n\r\t\v";

    s2 = trimLeftCopy(s1);
    trimLeft(s1);

    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper. \f\n\r\t\v",  s1);
    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper. \f\n\r\t\v",  s2);
}

TEST(textGeneralTest, textTrim)
{
    std::string s1, s2;
    s1 = " \f\n\r\t\vLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper. \f\n\r\t\v";

    s2 = trimCopy(s1);
    trim(s1);

    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper.",  s1);
    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper.",  s2);
}

// EOF