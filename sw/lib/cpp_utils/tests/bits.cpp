#include <limits>
#include <gtest/gtest.h>

#include <cpp_utils/bits.h>

using namespace utils;

template <typename T>
struct BitTypesTest : public testing::Test {
    using Int_type = T;
    static T msb;
};

using Int_types = ::testing::Types<uint8_t, uint16_t, uint32_t, uint64_t>;
TYPED_TEST_SUITE(BitTypesTest, Int_types);

template<> uint8_t  BitTypesTest<uint8_t >::msb = 128;
template<> uint16_t BitTypesTest<uint16_t>::msb = 32768;
template<> uint32_t BitTypesTest<uint32_t>::msb = 2147483648;
template<> uint64_t BitTypesTest<uint64_t>::msb = 9223372036854775808ull;

TYPED_TEST(BitTypesTest, singleMask) {
    using Int_type  = typename TestFixture::Int_type;
    auto bit_count = sizeof(Int_type) * 8;

    // Test min index
    //          76543210
    EXPECT_EQ(0b00000001, bit::mask<Int_type>(0));

    // Test some random index
    //          76543210
    EXPECT_EQ(0b00001000, bit::mask<Int_type>(3));
    EXPECT_EQ(0b01000000, bit::mask<Int_type>(6));

    // Test max index
    EXPECT_EQ(TestFixture::msb, bit::mask<Int_type>(bit_count-1));
}

TEST(bitsTest, rangedMask)
{
    // Test some random indexes
    //          76543210
    EXPECT_EQ(0b00000011, bit::mask<uint8_t>(0, 1));
    EXPECT_EQ(0b11111111, bit::mask<uint8_t>(0, 7));
    EXPECT_EQ(0b00011100, bit::mask<uint8_t>(2, 4));
    EXPECT_EQ(0b11111110, bit::mask<uint8_t>(1, 7));

    // Test when indexes are equal
    //          76543210
    EXPECT_EQ(0b00000010, bit::mask<uint8_t>(1, 1));
    EXPECT_EQ(0b01000000, bit::mask<uint8_t>(6, 6));

    // Test when start index is bigger than stop
    //          76543210
    EXPECT_EQ(0b00000000, bit::mask<uint8_t>(7, 1));
    EXPECT_EQ(0b00000000, bit::mask<uint8_t>(4, 3));
}

TEST(bitsTest, bitSetting)
{
    uint8_t x;

    x = 0;
    bit::set(&x, 0b00101010);
    EXPECT_EQ(   0b00101010, x);

    x = 0;
    bit::set(&x, 0b00000100);
    bit::set(&x, 0b00001000);
    bit::set(&x, 0b00010000);
    EXPECT_EQ(   0b00011100, x);
}

TEST(bitsTest, bitClearing)
{
    uint8_t x;

    x =            0b00101010;
    bit::clear(&x, 0b00101010);
    EXPECT_EQ(0, x);

    x = 255;
    bit::clear(&x, 0b00000100);
    bit::clear(&x, 0b00001000);
    bit::clear(&x, 0b00010000);
    EXPECT_EQ(     0b11100011, x);
}

TEST(bitsTest, bitOneChecking)
{
    uint8_t x;

    x =                         0b00000010;
    EXPECT_TRUE (bit::isOne(&x, 0b00000010));
    EXPECT_FALSE(bit::isOne(&x, 0b00000001));
}

TEST(bitsTest, regReadingWriting)
{
    uint8_t x;

    x = 0;
    bit::write(&x, 132);
    EXPECT_EQ(132, bit::read<uint8_t>(&x));
}


// EOF