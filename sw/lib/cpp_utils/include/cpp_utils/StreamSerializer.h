//! @file
//! @brief  Serializer to serialize to the standard streams.
//! @author Dariusz Zielinski

#pragma once

#include <string>

#include "GenericSerializer.h"

namespace utils
{
    //! This class provides a specialization of the serializer that prints to the standard output streams.
    class StdSerializer : GenericSerializer<StdSerializer>
    {
        public:
            //! This is a main method required for the serializer, it uses serializeHelper from GenericSerializer.
            //! This method will be used by SERIALIZABLE macro.
            //! @param stream serializing output will go this output stream.
            //! @param args any number of objects that will be serialized.
            //! @return stream passed as an argument.
            template<typename... Args>
            static std::ostream& serialize(std::ostream& stream, Args&&... args)
            {
                size_t index = 0;

                stream << "{";
                (( stream << args.name << " = ", serializeHelper(stream, args.obj, (++index) == sizeof...(args)) ), ...);
                stream << "}";

                return stream;
            }

            //! Performs actual printing to the stream.
            //! @param stream output stream.
            //! @param object object to print.
            template<typename T>
            static void print(std::ostream& stream, const T& object)
            {
                stream << object;
            }
    };

    // TODO add as a comment (or part of the docs) how to define global stream << operator for serializable classes
}

// EOF