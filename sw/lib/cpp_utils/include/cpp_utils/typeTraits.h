//! @file
//! @brief  Some additional type traits.
//! @author Dariusz Zielinski

#pragma once

#include <array>
#include <concepts>
#include <optional>
#include <string>
#include <type_traits>
#include <variant>
#include <vector>

namespace utils
{
    namespace detail
    {
        //! Defined for non-iterable types.
        template <typename T, typename = void>
        struct IsIterable : std::false_type {};

        //! Checks if the type can be iterated (i.e. std::begin() and std::end() can be applied on the type).
        template <typename T>
        struct IsIterable<T, std::void_t<decltype(std::begin(std::declval<T>())),
                                         decltype(std::end(std::declval<T>()))>> : std::true_type {};

        //! Defined for non-tuple types.
        template <typename>
        struct IsTuple: std::false_type {};

        //! Checks if the type is a tuple.
        template <typename ...T>
        struct IsTuple<std::tuple<T...>>: std::true_type {};

        //! Defined for non-pair types.
        template <typename>
        struct IsPair: std::false_type {};

        //! Checks if the type is a pair.
        template <typename ...T>
        struct IsPair<std::pair<T...>>: std::true_type {};

        //! Defined for non-optional types.
        template <typename T>
        struct IsOptional : std::false_type {};

        //! Checks if the type is an optional.
        template <typename T>
        struct IsOptional<std::optional<T>> : std::true_type {};

        //! Defined for non-variant types.
        template <typename... Args>
        struct IsVariant : std::false_type {};

        //! Checks if the type is an optional.
        template <typename... Args>
        struct IsVariant<std::variant<Args...>> : std::true_type {};

        //! Defined for non-vector types.
        template <typename... Args>
        struct IsVector : std::false_type {};

        //! Checks if the type is a vector.
        template <typename T>
        struct IsVector<std::vector<T>> : std::true_type {};
    }

    //! Convenience type for IsIterable
    template <typename T>
    constexpr bool is_iterable_v = ::utils::detail::IsIterable<T>::value;


    //! Convenience type for IsTuple
    template <typename T>
    constexpr bool is_tuple_v = ::utils::detail::IsTuple<T>::value;


    //! Convenience type for IsPair
    template <typename T>
    constexpr bool is_pair_v = ::utils::detail::IsPair<T>::value;


    //! Checks if the type represents a collection of chars.
    template <typename T>
    constexpr bool is_string_like_v = std::is_same_v<char *,  std::decay_t<T>>       ||
                                      std::is_same_v<const char *,  std::decay_t<T>> ||
                                      std::is_same_v<std::string, T>                 ||
                                      std::is_same_v<std::string_view, T>;


    //! Convenience type for IsOptional
    template <typename T>
    constexpr bool is_optional_v = ::utils::detail::IsOptional<T>::value;


    //! Convenience type for IsVariant
    template <typename... Args>
    constexpr bool is_variant_v = ::utils::detail::IsVariant<Args...>::value;


    //! Convenience type for IsVector
    template <typename T>
    constexpr bool is_vector_v = ::utils::detail::IsVector<T>::value;


    //! Trait that resolve to false 
    template<typename>
    inline constexpr bool dependent_false_v = false;

    //! Concept checks if type is enum
    template <typename T>
    concept enum_type = std::is_enum_v<T>;

    //! Concept checks if type is bounded enum
    template <typename T>
    concept bounded_enum_type = std::is_enum_v<T> && //
                                requires(T t) { T::count; };

    //! Concept for numeric types
    template <typename T>
    concept numeric = std::integral<T> || std::floating_point<T>;

    //! Concept for range of type Range_
    template<typename Range_, typename Value_>
    concept IsInputRangeOf = std::ranges::input_range<Range_> && std::same_as<std::ranges::range_value_t<Range_>, Value_>;

    //! Concept for pair like types
    template<typename T>
    concept pair_type = requires(T t)
    {
        t.first;
        t.second;
    };

    //! Concept for string types
    template<typename T>
    concept string_type = std::same_as<T, std::string_view> || std::same_as<T, std::string>;

    //! Concept for POD types
    template <typename T>
    concept CStandardLayout = std::is_standard_layout_v<T>;

    template<typename T>
    struct IsStdArray : std::false_type {};

    template<typename T, std::size_t N>
    struct IsStdArray<std::array<T, N>> : std::true_type {};

    //! Concept for std::array
    template<typename T>
    concept CStdArray = IsStdArray<T>::value;

    template<class T>
    concept CAggregate = std::is_aggregate_v<T>;
}

// EOF
