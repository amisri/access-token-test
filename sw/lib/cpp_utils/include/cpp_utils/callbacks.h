//! @file
//! @brief  Contains generic callbacks types.
//! @author Dariusz Zielinski

#pragma once

#include <functional>

namespace utils::callback
{
    using Simple    = std::function<void()>;
    using Predicate = std::function<bool()>;
}

// EOF