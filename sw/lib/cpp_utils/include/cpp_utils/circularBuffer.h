//! @file
//! @brief  Auxiliary class providing thread-safe, template-based circular buffer.
//! @author Dariusz Zielinski

#pragma once

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <optional>
#include <iostream>
#include <utility>

namespace utils
{
    //! This class provides thread-safe, template-based circular buffer.
    //!
    //! @tparam T Type of elements stored in the buffer. Has to provide default constructor.
    //! @tparam buffer_size maximum number of elements stored in the buffer. Defaults to 500.
    template<class T, size_t buffer_size = 500>
    class CircularBuffer
    {
        public:
            //! Tries to pushes an item into the circular buffer, but it does not block.
            //! If an item was pushed it returns true, otherwise false (if the buffer was full).
            //! If the buffer is disabled, the item is ignored and not pushed.
            //!
            //! @param item element to be added to the buffer.
            //! @return true if item was pushed, false otherwise.
            template <typename U>
            bool push(U&& item)
            {
                // Enter critical section
                std::scoped_lock<std::mutex> lock(m_mutex);

                if (full() || m_disabled)
                {
                    return false;
                }
                else
                {
                    pushCommon(std::forward<U>(item));
                    return true;
                }
            }

            //! Pushes an item into the circular buffer, blocking if the buffer is full.
            //! If the buffer is disabled, the item is ignored and not pushed.
            //!
            //! @param item element to be added to the buffer.
            template <typename U>
            void pushBlock(U&& item)
            {
                // Enter critical section
                std::unique_lock<std::mutex> lock(m_mutex);

                // Wait, if the buffer is full
                m_cond_not_full.wait(lock, [this]
                {
                    return ((!full()) || m_disabled);
                });

                if (!m_disabled)
                {
                    pushCommon(std::forward<U>(item));
                }
            }

            // **********************************************************

            //! Removes the oldest element from the buffer and returns it by value, using std::optional.
            //! This is a non-blocking variant of the pop method.
            //!
            //! @return The oldest element in the buffer or std::nullopt if the buffer was empty.
            std::optional<T> pop()
            {
                // Enter critical section
                std::scoped_lock<std::mutex> lock(m_mutex);

                // TODO behave differently than popBlock after unblockAndDisable

                // Return an item or empty optional if the queue is empty
                return empty() ? std::nullopt : std::optional<T>{popCommon()};
            }

            // **********************************************************

            //! Removes the oldest element from the buffer and returns it by value, using std::optional.
            //! The method blocks if the buffer is empty.
            //! It can return an std::nullopt, if the buffer was disabled during waiting.
            //!
            //! @return The oldest element in the buffer or std::nullopt if the buffer was disabled.
            std::optional<T> popBlock()
            {
                std::unique_lock<std::mutex> lock(m_mutex);

                m_cond_not_empty.wait(lock, [this]
                {
                    return ( (!empty()) || m_disabled);
                });

                return m_disabled ? std::nullopt : std::optional<T>{popCommon()};

            }

            // **********************************************************

            //! @return true if the buffer if empty, false otherwise
            bool isEmpty() const
            {
                return m_is_empty;              // This flag is atomic
            }

            // **********************************************************

            //! It releases blocking methods. For pushBlock() it means that the item to be added will be ignored and
            //! not added. For popBlock() it will cause the function to return an empty std::optional.
            //! This method is useful during program termination for clean-up. After this method is invoked the
            //! buffer becomes permanently disabled.
            void unblockAndDisable()
            {
                m_disabled = true;              // This flag is atomic
                m_cond_not_empty.notify_all();
                m_cond_not_full.notify_all();
            }

        private:
            //! @return Return true if the buffer is empty.
            inline bool empty() const
            {
                return m_back == m_front;
            }

            // **********************************************************

            //! @return Return true if the buffer is full.
            inline bool full() const
            {
                return (m_front + 1) % buffer_size == m_back;
            }

            // **********************************************************

            //! Common part of push methods.
            template <typename U>
            inline void pushCommon(U&& item)
            {
                bool buffer_was_empty = empty();

                // Add item and advance index
                m_buffer[m_front] = std::forward<U>(item);
                m_front           = (m_front + 1) % buffer_size;

                // Notify waiting threads
                if (buffer_was_empty)
                {
                    m_is_empty = false;
                    m_cond_not_empty.notify_all();
                }
            }

            // **********************************************************

            //! Common part of pop methods.
            //!
            //! @return The oldest element in the buffer.
            inline T popCommon()
            {
                bool buffer_was_full = full();
                auto old_back        = m_back;

                m_back = (m_back + 1) % buffer_size;

                // Notify waiting threads
                if (buffer_was_full)
                {
                    m_cond_not_full.notify_all();
                }

                // If the buffer is empty now, change state of the flag. This atomic flag is used
                // so that other thread can check if the buffer is empty without acquiring the lock.
                m_is_empty = empty();

                return std::move(m_buffer[old_back]);
            }

            // **********************************************************

            std::mutex              m_mutex;                  //!< A mutex object used to synchronize.
            std::condition_variable m_cond_not_empty;         //!< A condition variable indicating that the buffer has content.
            std::condition_variable m_cond_not_full;          //!< A condition variable indicating that the buffer if not full.
            T                       m_buffer[buffer_size];    //!< An array of elements of type T - the buffer.
            size_t                  m_front    = 0;           //!< An index of the newest element in the buffer.
            size_t                  m_back     = 0;           //!< An index of the oldest element in the buffer.
            std::atomic_bool        m_disabled = false;       //!< A flag used to release blocking functions.
            std::atomic_bool        m_is_empty = true;        //!< An atomic flag indicating the buffer is empty for the sake of API.
    };
}

// EOF
