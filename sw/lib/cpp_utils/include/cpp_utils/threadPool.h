//! @file
//! @brief  Thread pool implementation with templated task type.
//! @author Dariusz Zielinski

#pragma once

#include <atomic>
#include <functional>
#include <thread>
#include <type_traits>
#include <vector>

#include "callbacks.h"
#include "circular_buffer.h"

namespace utils
{
    // TODO This is currently not tested.

    //! This class provides thread pool utility. It can work with a standard std::thread (by default)
    //! or with a custom thread object, for which a custom thread-creating callback has to be provides
    //! (so that this class knows how to create this custom thread class).
    //!
    //! @tparam task_queue_size Maximum size of the queue holding tasks before they are assigned to a worker thread.
    //! @tparam ThreadType class that is used for thread. By default std::thread.
    template<size_t task_queue_size, class ThreadType = std::thread>
    class ThreadPool
    {
        public:
            //! A constructor that will be present only if ThreadType is std::thread.
            //! The constructor will create required number of threads (defined by thread_count).
            //!
            //! @tparam U Dummy parameter, ignore.
            //! @param thread_count Number of threads to create for this pool.
            template<typename U = ThreadType>
            typename std::enable_if<std::is_same<U, std::thread>::value, void>::type
            createThreads(const size_t& thread_count)
            {
                for (size_t i = 0; i < thread_count; ++i)
                {
                    m_threads.emplace_back(std::thread(&ThreadPool::threadFunc, this));
                }
            }

            //************************************************************

            //! A constructor that will be present only if ThreadType is different than std::thread (custom thread class)
            //! The constructor will create required number of threads (defined by \p thread_count),
            //! using a callback function provided by the \p create_thread argument, that should return a newly created
            //! thread object, that has entry function set to the parameter of the callback.
            //!
            //! @tparam U Dummy parameter, ignore.
            //! @param thread_count Number of threads to create for this pool.
            //! @param create_thread Callback function that creates a thread object.
            template<typename U = ThreadType>
            typename std::enable_if<!std::is_same<U, std::thread>::value, void>::type
            createThreads(const size_t& thread_count, const std::function<ThreadType(callback::Simple)>& create_thread)
            {
                for (size_t i = 0; i < thread_count; ++i)
                {
                    m_threads.emplace_back(create_thread([this](){ ThreadPool::threadFunc(); }));
                }
            }

            // **********************************************************

            //! Adds task to the task queue, from which it will be assigned to next available worker thread or
            //! wait there until some thread is available.
            //!
            //! @param task A task to add to the task queue.
            void pushTask(const callback::Simple& task)
            {
                m_task_queue.push(task);
            }

            // **********************************************************

            //! This function will terminate the thread pool. All still unassigned tasks will be discarded,
            //! while tasks already being executed will finish executing. After this every thread is terminated
            //! and joined.
            void terminate()
            {
                m_terminate = true;
                m_task_queue.release();

                for (auto& thread : m_threads)
                {
                    thread.join();
                }

                m_threads.clear();
            }

        private:
            //! This is an entry point for threads. It will wait for a new task on the queue and if there is
            //! such a task, it will consume it and execute it. It will terminate after m_terminate is set to
            //! true and the currently executed task is finished.
            void threadFunc()
            {
                while (!m_terminate)
                {
                    // Get the new task
                    auto task = m_task_queue.popBlock();

                    // If the terminate become true during popBlock, then popBlock will return empty task
                    if (m_terminate)
                    {
                        return;
                    }

                    // Execute popped task
                    if (task)
                    {
                        task();
                    }
                }
            }

            // **********************************************************

            std::atomic_bool                                  m_terminate{false};
            std::vector<ThreadType>                           m_threads;
            CircularBuffer<callback::Simple, task_queue_size> m_task_queue;
    };
}

// EOF