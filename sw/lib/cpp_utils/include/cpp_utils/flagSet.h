//! @file
//! @brief  Utility class for using enums as flags.
//! @author Dariusz Zielinski

#pragma once

#include <bitset>
#include <type_traits>

namespace utils
{
    //! This class allows to have
    //! @tparam T Enum type for which to use flag set class.
    template<typename T>
    class FlagSet
    {
        static_assert(T::COUNT == T::COUNT, "Enum type has to declare COUNT field.");
        static_assert(std::is_enum_v<T>,    "FlagSet supports only enum types.");

        using EnumType = std::underlying_type_t<T>;
        using Bitset   = std::bitset<static_cast<EnumType>(T::COUNT)>;

        public:
            //! Default constructor representing an empty flag set.
            FlagSet() = default;

            // **********************************************************

            //! Copy constructor.
            FlagSet(const FlagSet& other) : m_bitset(other.m_bitset)
            {}

            // **********************************************************

            //! Conversion constructor from underlying enum into the flagSet object.
            FlagSet(const T& flag)  // Implicit
            {
                m_bitset.set(static_cast<EnumType>(flag));
            }

            // ! Constexpr conversion constructor from underlying enum into the flagSet object.
            constexpr FlagSet(T&& flag) : m_bitset(1 << static_cast<EnumType>(flag))
            {}

            // Assignment / access operations
            // **********************************************************

            //! Assignment operator for a single flag. It makes the FlagSet to hold only the assigned flag.
            FlagSet& operator=(const T& flag)
            {
                m_bitset.reset();
                m_bitset[static_cast<EnumType>(flag)] = 1;
                return *this;
            }

            // **********************************************************

            //! @return a reference to the field holding a value for a given flag.
            typename Bitset::reference operator[](const T flag)
            {
                return m_bitset[static_cast<EnumType>(flag)];
            }

            // Flag presence testing
            // **********************************************************

            //! @return true if the flag is present in the FlagSet.
            constexpr bool has(const T flag) const
            {
                return m_bitset[static_cast<EnumType>(flag)];
            }

            // **********************************************************

            //! @return true if any of the specified flags is present in the FlagSet
            template<class... Ts>
            bool hasAny(Ts&& ... flags) const
            {
                static_assert((std::is_same_v<T, Ts> && ...), "Flags have to be fields of the enum");

                return (m_bitset.test(static_cast<EnumType>(flags)) || ...);
            }

            // **********************************************************

            //! @return true if all of the specified flags are present in the FlagSet
            template<class... Ts>
            bool hasAll(Ts&& ... flags) const
            {
                static_assert((std::is_same_v<T, Ts> && ...), "Flags have to be fields of the enum");

                return (m_bitset.test(static_cast<EnumType>(flags)) && ...);
            }

            // **********************************************************

            //! @return true if the flag is present in the FlagSet.
            constexpr bool operator[](const T flag) const
            {
                return has(flag);
            }

            // **********************************************************

            //! @return true if any flag is present in the FlagSet
            bool any() const
            {
                return m_bitset.any();
            }

            // **********************************************************

            //! @return true if no flag is present in the FlagSet.
            bool none() const
            {
                return m_bitset.none();
            }

            // **********************************************************

            //! Comparison operator with another FlagSet
            bool operator==(const FlagSet& other) const
            {
                return m_bitset == other.m_bitset;
            }

            // Flag adding operations
            // **********************************************************

            //! Adds the flag(s) to the FlagSet.
            template <class... Ts>
            FlagSet& add(const Ts&&... flags)
            {
                static_assert((std::is_same_v<T, Ts> && ...), "Flags have to be fields of the enum");

                (m_bitset.set(static_cast<EnumType>(flags)), ...);
                return *this;
            }

            // **********************************************************

            //! Adds the flag to the FlagSet.
            FlagSet& operator|=(const T& flag)
            {
                m_bitset.set(static_cast<EnumType>(flag));
                return *this;
            }

            // **********************************************************

            //! Adds the flag to the FlagSet.
            FlagSet& operator+=(const T& flag)
            {
                m_bitset.set(static_cast<EnumType>(flag));
                return *this;
            }

            // **********************************************************

            //! @return a copy of this FlagSet containing at least the flag.
            FlagSet operator|(const T& flag) const
            {
                return FlagSet(*this) |= flag;
            }

            // **********************************************************

            //! @return a copy of this FlagSet containing at least the flag.
            FlagSet operator+(const T& flag) const
            {
                return (*this) | flag;
            }

            // **********************************************************

            //! Adds all the flags in flag_set to the FlagSet.
            FlagSet& operator|=(const FlagSet& flag_set)
            {
                m_bitset |= flag_set.m_bitset;
                return *this;
            }

            // **********************************************************

            //! Adds all the flags in flag_set to the FlagSet.
            FlagSet& operator+=(const FlagSet& flag_set)
            {
                m_bitset |= flag_set.m_bitset;
                return *this;
            }

            // **********************************************************

            //! Returns a new flag set that has flags of the current flagSet and the one passed as argument.
            FlagSet operator|(const FlagSet& flag_set) const
            {
                FlagSet copy(*this);
                copy.m_bitset |= flag_set.m_bitset;
                return copy;
            }

            // **********************************************************

            //! Returns a new flag set that has flags of the current flagSet and the one passed as argument.
            FlagSet operator+(const FlagSet& flag_set) const
            {
                return (*this) | flag_set;
            }

            // Flag removing operations
            // **********************************************************

            //! Removes the flag from the FlagSet.
            template <class... Ts>
            FlagSet& remove(Ts&&... flags)
            {
                static_assert((std::is_same_v<T, Ts> && ...), "Flags have to be fields of the enum");

                (m_bitset.reset(static_cast<EnumType>(flags)), ...);
                return *this;
            }

            // **********************************************************

            //! Removes the flag from the FlagSet.
            FlagSet& operator-=(const T& flag)
            {
                m_bitset.reset(static_cast<EnumType>(flag));
                return *this;
            }

            // **********************************************************

            //! Removes the flag from the FlagSet.
            FlagSet operator-(const T& flag)
            {
                return FlagSet(*this) -= flag;
            }

            // **********************************************************

            //! Removes all the flags in flag_set from the FlagSet.
            FlagSet& operator-=(const FlagSet& flag_set)
            {
                m_bitset &= (~flag_set.m_bitset);
                return *this;
            }

            // **********************************************************

            //! Removes all the flags in flag_set from the FlagSet.
            FlagSet operator-(const FlagSet& flag_set) const
            {
                return FlagSet(*this) -= flag_set;
            }

            // Other operations
            // **********************************************************

            //! Removes all the flags (clears the FlagSet)
            FlagSet& clear()
            {
                m_bitset.reset();
                return *this;
            }

            // **********************************************************

            // Prints the flag set to the stream.
            friend std::ostream& operator<<(std::ostream& stream, const FlagSet& me)
            {
                return stream << me.m_bitset;
            }

        private:
            Bitset m_bitset;
    };

    //! Provide a free operator allowing to combine two enumeration member into a FlagSet.
    //!
    //! @tparam T enum type for which to use flag set class.
    //! @param lhs left side operand (enum).
    //! @param rhs right side operand (enum).
    //! @return a new FlagSet that has these two flags (lhs, rhs) set.
    template<typename T>
    std::enable_if_t<std::is_enum<T>::value, FlagSet<T>>
    operator|(const T& lhs, const T& rhs)
    {
        FlagSet<T> fs;
        fs |= lhs;
        fs |= rhs;

        return fs;
    }
}

// EOF