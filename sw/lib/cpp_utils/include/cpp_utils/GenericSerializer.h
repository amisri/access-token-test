//! @file
//! @brief  Class of the generic serializer that can be useful in defining other serializers.
//! @author Dariusz Zielinski

#pragma once

#include <string>
#include <tuple>
#include <type_traits>
#include <variant>

#include "serializable.h"
#include "typeTraits.h"

namespace utils
{
    //! This is a class for a generic serializer, that provides serializeHelper method used by the specialized serialiers.
    //! The method is responsible for appropriate printing, depending on a type.
    //!
    //! @tparam Serializer class that does the actual printing, using method Serializer::print.
    template<typename Serializer>
    class GenericSerializer
    {
        protected:
            //! This method recognize the type of what is printed and acts respectively. If the type itself is serializable
            //! then serializer is used to print it (which will call this function again). The strings are printed between
            //! " and ". If the type represents an iterable type or a tuple, the method iterates over them to print each element.
            //!
            //! @param context an object of an arbitrary type (to be used by specialized Serializer), passed to serializing methods. For example, a stream or a JSON objects to print to.
            //! @param object ab object to serialize.
            //! @param last_element flag to indicate whether the printed object is the last element in a container (for iterable objects or tuples).
            template<typename Context, typename T>
            static void serializeHelper(Context& context, const T& object, bool last_element)
            {
                if constexpr(is_serializable_v<T>)
                {
                    object.template serialize<Serializer>(context);
                }
                else if constexpr(is_string_like_v<T>)
                {
                    Serializer::print(context, '"');
                    Serializer::print(context, object);
                    Serializer::print(context, '"');
                }
                else if constexpr(is_iterable_v<T>)
                {
                    Serializer::print(context, '[');
                    for (auto it = std::begin(object); it != std::end(object); ++it)
                    {
                        serializeHelper(context, *it, it + 1 == std::end(object));
                    }
                    Serializer::print(context, ']');
                }
                else if constexpr(is_tuple_v<T> || is_pair_v<T>)
                {
                    Serializer::print(context, '{');
                    std::apply( [&](const auto&... tuple_objects)
                    {
                        size_t index = 0;
                        (( serializeHelper(context, tuple_objects, (++index) == sizeof...(tuple_objects)) ), ...);
                    }, object);
                    Serializer::print(context, '}');
                }
                else if constexpr(is_optional_v<T>)
                {
                    if(object.has_value())
                    {
                        Serializer::print(context, *object);
                    }
                    else
                    {
                        Serializer::print(context, "null");
                    }
                }
                else if constexpr(is_variant_v<T>)
                {
                    std::visit(
                        [&context](const auto& value)
                        {
                            using ValueType = std::remove_cvref_t<decltype(value)>;
                            if constexpr(std::is_same_v<ValueType, std::monostate>)
                            {
                                Serializer::print(context, "null");
                            }
                            else
                            {
                                Serializer::print(context, value);
                            }
                        }, object);
                }
                else
                {
                    Serializer::print(context, object);
                }

                if (!last_element)
                {
                    Serializer::print(context, ", ");
                }
            }
    };

}

// EOF
