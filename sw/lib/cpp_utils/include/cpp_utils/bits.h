//! @file
//! @brief  Functions for bit-wise operations.
//! @author Dariusz Zielinski

#pragma once

#include <byteswap.h>
#include <cstdint>
#include <type_traits>

namespace utils::bit
{
    //! Generates a bit mask with one bit on \p bit_index position set to 1.
    //! Note: overflowing (index >= width of the type) is an undefined behavior in C++.
    //!
    //! @tparam Width integer type used to store the mask.
    //! @param bit_index bit on this index will be set to 1.
    //! @return a specified bit mask.
    template<class Width>
    constexpr Width mask(const uint8_t& bit_index) noexcept
    {
        return static_cast<Width>(1) << bit_index;
    }

    // **********************************************************

    //! Generates a bit mask with bits on positions between \p start_index and \p stop_index set to 1.
    //!
    //! @tparam Width integer type used to store the mask.
    //! @param start_index starting index. Must be lesser than \p stop_index
    //! @param stop_index finishing index.
    //! @return a specified bit mask.
    template<class Width>
    constexpr Width mask(const uint8_t& start_index, const uint8_t& stop_index) noexcept
    {
         Width bit_mask = 0;

         for (auto i = start_index; i <= stop_index; ++i)
         {
             bit_mask |= (1U << i);
         }

         return bit_mask;
    }

    // **********************************************************

    //! Generic-width byte swap function, which is actually deleted. Specialisation will provide implementation.
    //!
    //! @tparam Width integer type.
    //! @param value input value.
    //! @return byte-swapped \p value.
    template<class Width>
    inline Width bs(const Width& value) noexcept = delete;

    // **********************************************************

    //! Byte-swap specialization for 8 bits - no swapping by definition.
    //!
    //! @param value input value.
    //! @return byte-swapped \p value.
    template<>
    inline uint8_t bs(const uint8_t& value) noexcept
    {
        return value;
    }

    // **********************************************************

    //! Byte-swap specialization for 16 bits.
    //!
    //! @param value input value.
    //! @return byte-swapped \p value.
    template<>
    inline uint16_t bs(const uint16_t& value) noexcept
    {
        return bswap_16(value);
    }

    // **********************************************************

    //! Byte-swap specialization for 32 bits.
    //!
    //! @param value input value.
    //! @return byte-swapped \p value.
    template<>
    inline uint32_t bs(const uint32_t& value) noexcept
    {
        return bswap_32(value);
    }

    // **********************************************************

    //! Byte-swap specialization for 64 bits.
    //!
    //! @param value input value.
    //! @return byte-swapped \p value.
    template<>
    inline uint64_t bs(const uint64_t& value) noexcept
    {
        return bswap_64(value);
    }

    // **********************************************************

    //! Generic-width function to set bits in a variable.
    template<class Var, class Width>
    inline void vSet(Var& var, const Width& mask)
    {
        var |= mask;
    }

    // **********************************************************

    //! Generic-width function to clear bits in a variable.
    template<class Var, class Width>
    inline void vClear(Var& var, const Width& mask)
    {
        var &= ~mask;
    }

    // **********************************************************

    //! Generic-width function to test if bits are equal to 1 in a variable.
    template<class Var, class Width>
    inline bool vIsOne(const Var& var, const Width& mask)
    {
        return (var & mask) == static_cast<Var>(mask);
    }

    // **********************************************************

    //! Sets bit to 1 if value is true, or clears to 0 otherwise.
    template<class Var, class Width>
    inline void vWrite(Var& var, const Width& mask, const bool& value)
    {
        if (value)
        {
            vSet(var, mask);
        }
        else
        {
            vClear(var, mask);
        }
    }

    // **********************************************************

    //! Generic-width function to set bits in a register.
    //!
    //! @tparam Width integer type used for a value.
    //! @tparam AddressType type used for a register.
    //! @param reg register address or pointer.
    //! @param mask a bit mask used to set bits.
    template<class Var, class Width>
    inline void set(Var reg, const Width& mask)
    {
        (*reinterpret_cast<volatile Width *>(reg)) |= mask;
    }

    // **********************************************************

    //! Generic-width function to clear bits in a register.
    //!
    //! @tparam Width integer type used for a value.
    //! @tparam AddressType type used for a register.
    //! @param reg register address or pointer.
    //! @param mask a bit mask used to clear bits.
    template<class Width, class AddressType>
    inline void clear(AddressType reg, const Width& mask)
    {
        (*reinterpret_cast<volatile Width *>(reg)) &= (~mask);
    }

    // **********************************************************

    //! Generic-width function to test if bits are equal to 1.
    //!
    //! @tparam Width integer type used for a value.
    //! @tparam AddressType type used for a register.
    //! @param reg register address or pointer.
    //! @param mask a bit mask used to select bits to test.
    //! @return returns true if bits enabled by the mask are equal to 1.
    template<class Width, class AddressType>
    inline bool isOne(AddressType reg, const Width& mask)
    {
        return ((*reinterpret_cast<volatile Width *>(reg)) & (mask)) == mask;
    }

    // **********************************************************

    //! Generic-width function to read a value of a register.
    //!
    //! @tparam Width integer type used for a value.
    //! @tparam AddressType type used for a register.
    //! @param reg register address or pointer.
    //! @return returns a value of the register.
    template<class Width, class AddressType>
    inline Width read(AddressType reg)
    {
        return (*reinterpret_cast<volatile Width *>(reg));
    }

    // **********************************************************

    //! Generic-width function to write a value to a register.
    //!
    //! @tparam Width integer type used for a value.
    //! @tparam AddressType type used for a register.
    //! @param reg register address or pointer.
    //! @param value value to write to the register.
    template<class Width, class AddressType>
    inline void write(AddressType reg, Width value)
    {
        (*reinterpret_cast<volatile Width *>(reg)) = value;
    }
}

// **********************************************************

namespace utils::nxp
{
    //! A trivial user-defined literal to convert bit indexing used in the NXP documentations
    //! to the one that makes mathematical sense.
    //! @param nxp_bit bit index to convert.
    //! @return returns converted bit index, that is (31 - \p nxp_bit).
    constexpr unsigned long long int operator""_nxp(unsigned long long int nxp_bit)
    {
        // This throw will actually produce compile time error if nxp_bit is invalid
        return nxp_bit <= 31 ? (31 - nxp_bit) : throw "Bit index must not be greater than 31.";
    }

    //! A trivial function to convert bit indexing used in the NXP documentations
    //! to the one that makes mathematical sense.
    //! @param nxp_bit bit index to convert.
    //! @return returns converted bit index, that is (31 - \p nxp_bit).
    constexpr uint8_t nxp(uint8_t nxp_bit)
    {
        // This throw will actually produce compile time error if nxp_bit is invalid
        return nxp_bit <= 31 ? (31 - nxp_bit) : throw "Bit index must not be greater than 31.";
    }
}

// EOF
