//! @file
//! @brief  Timing-related utilities.
//! @author Dariusz Zielinski

#pragma once

#include <chrono>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "callbacks.h"

namespace utils
{
    //! Possible time-units to print in formatted way.
    enum class TimingFormat : uint8_t
    {
        hours   = 0,
        minutes = 1,
        seconds = 2,
        milli   = 3,
        micro   = 4,
        nano    = 5,
    };

    // **********************************************************

    //! Formats passed duration in nanoseconds as string. String format is defined by 'from' and 'to' variables,
    //! which define, respectively, the biggest time unit and the smallest one to print. For example, printing
    //! from minutes to milliseconds will produce a following format: "mm:ss.mmm". If 'from' is a smaller time unit
    //! than 'to' an empty string will be returned. If invalid 'from' or 'to' is passed, hours and nanoseconds will
    //! be used, respectively.
    //!
    //! @param duration duration in nanoseconds to format as a string.
    //! @param from the biggest time unit to print.
    //! @param to the smallest time unit to print.
    //! @return A string representation of a passed duration.
    //!
    //! @remarks Thread-safe.
    std::string formatDuration(std::chrono::nanoseconds duration, TimingFormat from, TimingFormat to) noexcept;

    // **********************************************************

    //! Basic timer class used to measure time since start() to stop() and print formatted time to the timer logger.
    //!
    //! @tparam T type of a clock used to get time. T should satisfy the C++ TrivialClock requirements.
    template <class T = std::chrono::high_resolution_clock>
    class Timer
    {
        public:
            //! The name is later printed along with the measured time.
            //!
            //! @param timer_name name of the timer.
            explicit Timer(std::string timer_name = "") noexcept : m_name(std::move(timer_name))
            {
            }

            // **********************************************************

            //! Starts time measurement.
            //!
            //! @remarks Not thread-safe.
            void start()
            {
                m_start_point = T::now();
                m_is_started  = true;
            }

            // **********************************************************

            //! Stops time measurement.
            //!
            //! @remarks Not thread-safe.
            void stop()
            {
                if (m_is_started)
                {
                    m_stop_point = T::now();
                    m_is_started = false;
                }
            }

            // **********************************************************

            //! Returns measured duration as nanoseconds. If the timer has been started and stopped the duration will
            //! represent time between start() and stop(). If the timer has been started and not yet stopped the duration
            //! will represent time between start() and the current moment. If the timer has never been started, the duration
            //! will be equal to 0.
            //!
            //! @return Duration measured by the timer.
            //!
            //! @remarks Thread-safe.
            std::chrono::nanoseconds get()
            {
                if (m_is_started)
                {
                    return T::now() - m_start_point;
                }
                else
                {
                    return m_stop_point - m_start_point;
                }
            }

            // **********************************************************

            //! Prints name of the timer and duration measured by the timer into the timer logger, displaying time from
            //! seconds to microseconds. The duration value is the one returned from the get() method.
            //!
            //! @param stream reference to an object that provides stream-like operator <<
            //! @param from the biggest time unit to print.
            //! @param to the smallest time unit to print.
            //! @remarks Thread-safe.
            template<class Q>
            void print(Q& stream = std::cout, TimingFormat from = TimingFormat::milli, TimingFormat to = TimingFormat::micro)
            {
                stream << m_name << " time = " << formatDuration(get(), from, to);
            }

            // **********************************************************

            std::string print(TimingFormat from = TimingFormat::milli, TimingFormat to = TimingFormat::micro)
            {
                std::stringstream ss;
                print(ss, from, to);
                return ss.str();
            }

        protected:
            bool                   m_is_started = false;           //!< Flag that marks whether the timer is started but not stopped.
            std::string            m_name;                         //!< Name of the timer. Empty by default.
            class T::time_point m_start_point;                  //!< Time point representing start() moment.
            class T::time_point m_stop_point;                   //!< Time point representing stop() moment.
    };

    // **********************************************************

    //! Extension of the basic timer. This timer measures time its own instance lives, i.e. it does start()
    //! in constructor and stop() and print() in destructor.
    //!
    //! @tparam T type of a clock used to get time. T should satisfy the C++ TrivialClock requirements.
    template <class T = std::chrono::high_resolution_clock, class Q = decltype(std::cout)>
    class ScopedTimer : public Timer<T>
    {
        public:
            //! Default constructor. Leaves name of the timer empty and starts the timer.
            //!
            //! @param from the biggest time unit to print.
            //! @param to the smallest time unit to print.
            explicit ScopedTimer(const std::string& name = "", TimingFormat from = TimingFormat::milli, TimingFormat to = TimingFormat::micro)
                : ScopedTimer(std::cout, name, from, to)
            {}

            // **********************************************************

            explicit ScopedTimer(Q& stream, const std::string& name = "", TimingFormat from = TimingFormat::milli, TimingFormat to = TimingFormat::micro)
                : m_stream(stream), Timer<T>(name), m_from(from), m_to(to)
            {
                Timer<T>::start();
            }

            // **********************************************************

            //! Destructor of the tmer. Stops it and print the measured time.
            //!
            //! @remarks Not thread-safe.
            ~ScopedTimer()
            {
                Timer<T>::stop();
                Timer<T>::print(m_stream, m_from, m_to);
            }

        private:
            Q&           m_stream;
            TimingFormat m_from;
            TimingFormat m_to;
    };

    // **********************************************************

    //! This timer should be especially useful to profile a functions, as it supports multiple points of measurement
    //! from the single point of start.
    //! This timer doesn't have a stop() method. Instead, a user simply adds points and a print() method prints them.
    //!
    //! @tparam T type of a clock used to get time. T should satisfy the C++ TrivialClock requirements.
    template <class T = std::chrono::high_resolution_clock>
    class ChainedTimer
    {
        public:
            //! A default constructor that leaves name of the timer empty.
            ChainedTimer() = default;

            // **********************************************************

            //! A constructor that sets name of the timer. Name is later printed along with the measured time.
            //!
            //! @param timer_name name of the timer.
            explicit ChainedTimer(std::string timer_name) : m_name(std::move(timer_name))
            {
            }

            // **********************************************************

            //! Resets list of time points and starts the timer (i.e. adds the first point in time).
            //!
            //! @remarks Not thread-safe.
            void start()
            {
                m_points.clear();
                m_first_point = T::now();
            }

            // **********************************************************

            //! Adds time point to the list of such. User may provide custom name for this point in name.
            //!
            //! @param point_name name of the added time point, defaults to empty string.
            //!
            //! @remarks Not thread-safe.
            void addPoint(std::string point_name = "")
            {
                m_points.push_back(std::make_pair(point_name, T::now()));
            }

            // **********************************************************

            //! Prints all added time points and their name to the timer logger.
            //! If only one point has been added nothing will be printed.
            //! Both absolute times (from the first point) and relative times (from the last point) are printed.
            //!
            //! @param stream reference to an object that provides stream-like operator <<
            //! @param from the biggest time unit to print.
            //! @param to the smallest time unit to print.
            //!
            //! @remarks Not thread-safe.
            template<class Q>
            void print(Q& stream = std::cout, TimingFormat from = TimingFormat::milli, TimingFormat to = TimingFormat::micro)
            {
                auto last = m_first_point;

                for (auto it = m_points.begin(); it != m_points.end(); ++it)
                {
                    stream << m_name << " time point " << it->first << " = "
                           << formatDuration(it->second - m_first_point, TimingFormat::seconds, TimingFormat::micro) << " ("
                           << formatDuration(it->second - last, TimingFormat::seconds, TimingFormat::micro) << ")";

                    if (it + 1 != m_points.end())
                    {
                        stream << "\n";
                    }

                    last = it->second;
                }
            }

            // **********************************************************

            //! Same as the print above, but it returns the result as a string.
            //!
            //! @param from the biggest time unit to print.
            //! @param to the smallest time unit to print.
            //! @return String representation of the duration.
            //!
            //! @remarks Not thread-safe.
            std::string print(TimingFormat from = TimingFormat::milli, TimingFormat to = TimingFormat::micro)
            {
                std::stringstream ss;
                print(ss, from, to);
                return ss.str();
            }

        private:
            std::string                                              m_name;         //!< Name of the timer.
            class T::time_point                                      m_first_point;  //!< First time point after starting the timer.
            std::vector<std::pair<std::string, class T::time_point>> m_points;       //!< List of the time points and their names.
    };
}

// EOF