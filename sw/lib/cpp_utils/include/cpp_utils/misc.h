//! @file
//! @brief  Set of different functions without common category.
//! @author Dariusz Zielinski

#pragma once

#include <arpa/inet.h>
#include <array>
#include <bit>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <span>
#include <thread>
#include <tuple>
#include <type_traits>
#include <vector>

#include "callbacks.h"
#include "typeTraits.h"

namespace utils
{
    //! Gets size of the static array (helper function for template arguments)
    template <typename T, size_t n>
    constexpr size_t getArraySize(const T (&)[n])
    {
        return n;
    }

    // **********************************************************

    //! Inheriting from this class will make the derived class non-copyable.
    class Noncopyable
    {
        public:
            Noncopyable() = default;

            // Disable copy
            Noncopyable(const Noncopyable&) = delete;
            Noncopyable& operator = (const Noncopyable&) = delete;

            // Keep move enabled
            Noncopyable(Noncopyable&&) = default;
            Noncopyable& operator = (Noncopyable&&)  = default;
    };

    // **********************************************************

    //! Inheriting from this class will make the derived class non-copyable and non-movable.
    class NoncopyableNonmovable
    {
        public:
            NoncopyableNonmovable() = default;

            // Disable copy and move
            NoncopyableNonmovable(const NoncopyableNonmovable&) = delete;
            NoncopyableNonmovable& operator = (const NoncopyableNonmovable&) = delete;
    };

    // **********************************************************

    //! This function will keep on executing \p predicate in a busy-waiting manner, until it returns true
    //! or a time given by \p timeout passes. The function will sleep in the loop (between each check of predicate) for
    //! the time given by \p sleep_time.
    //!
    //! @param predicate std::function taking no parameters and returning a bool.
    //! @param timeout maximum waiting time for the predicate to return true.
    //! @param sleep_time time to sleep between checkin of the predicate.
    //! @return The function will return true if the predicate returns true before timing out. Otherwise false is returned.
    bool setTimeout(const callback::Predicate& predicate, const std::chrono::nanoseconds& timeout,
                    const std::chrono::nanoseconds& sleep_time = std::chrono::microseconds(10));

    // **********************************************************

    //! Finally will execute given action in its destructor (so when it goes out of scope).
    class Finally
    {
        public:
            //! Constructs the object.
            //! @param final_action a simple function, with no arguments and no return, to be executed in the destructor.
            explicit Finally(callback::Simple final_action) : m_final_action{std::move(final_action)}
            {}

            //! Executes the callback passed in the constructor.
            ~Finally()
            {
                if (m_final_action)
                {
                    m_final_action();
                }
            }

        private:
            callback::Simple m_final_action;  //!< Callback to be called during the class destructor.
    };

    // **********************************************************

    // helper function for std::visit
    template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
    template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

    // **********************************************************

    template<typename T>
    requires requires(T t, int a) { t.fill(a); }
    constexpr T createArray(typename T::value_type init_value)
    {
        T array;
        array.fill(init_value);

        return array;
    }

    // **********************************************************

    template<unsigned int max_value, std::integral T = unsigned int>
    class CyclicInt
    {
      public:
        CyclicInt() = default;

        template<std::integral V>
        CyclicInt(V value)
        {
            m_value = static_cast<T>(value % max_value);
        }

        template<std::integral V>
        CyclicInt& operator=(V value)
        {
            m_value = static_cast<T>(value % max_value);
            return *this;
        }

        CyclicInt& operator++()
        {
            m_value = (m_value + 1) % max_value;
            return *this;
        }

        CyclicInt operator++(int)
        {
            CyclicInt temp(*this);
            operator++();
            return temp;
        }

        CyclicInt& operator--()
        {
            m_value = (m_value - 1 + max_value) % max_value;
            return *this;
        }

        CyclicInt operator--(int)
        {
            CyclicInt temp(*this);
            operator--();
            return temp;
        }

        operator T() const
        {
            return m_value;
        }

      private:
        T m_value;
    };

    // **********************************************************

    template<numeric T>
    T hton(T value)
    {
        if constexpr(std::same_as<T, std::uint16_t>)
        {
            return htons(value);
        }
        else if constexpr(std::same_as<T, std::uint32_t>)
        {
            return htonl(value);
        }
        else if constexpr(std::same_as<T, float>)
        {
            auto integer = std::bit_cast<std::uint32_t>(value);
            auto converted_integer = htonl(integer);
            return std::bit_cast<float>(converted_integer);
        }
        else
        {
            static_assert(dependent_false_v<T>, "Unimplemented");
        }
    }

    // **********************************************************

    template<numeric T>
    void htonInPlace(T& value)
    {
        value = hton(value);
    }

    template<numeric T>
    T ceil(T a, T b)
    {
        T quotinet = a / b;
        T reminder = a % b;
        return quotinet + (reminder == 0 ? 0 : 1);
    }
}

// EOF
