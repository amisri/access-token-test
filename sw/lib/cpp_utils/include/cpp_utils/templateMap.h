//! @file
//! @brief  A generic class that works like a map between any compile-time type and object.
//! @author Dariusz Zielinski

#pragma once

#include <cstdint>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

namespace utils
{
    template<typename T1, typename T2>
    class TemplateMap
    {
        public:
            explicit TemplateMap(T2 default_value = T2()) : m_default_value(default_value)
            {
            }

            // **********************************************************

            template<T1 T>
            inline T2& value() const
            {
                static T2 value = m_default_value;
                return value;
            }

        private:
            const T2 m_default_value;
    };
}

// EOF