//! @file
//! @brief
//! @author Adam Solawa

#pragma once

#include <string_view>
#include <ranges>

#include "typeTraits.h"

namespace utils
{
    //! Converts a range of contiguous characters to a std::basic_string_view.
    //! TODO: Remove this in C++23; std::views::split will return contiguous ranges and std::basic_string_view will have a range constructor.
    //!
    //! std::views::split returns a range of ranges.
    //! The ranges unfortunately are not std::ranges::contiguous_range
    //! even when the base type is contiguous, so we can't use that constraint.
    //! This will be fixed
    //! http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2021/p2210r2.html
    inline constexpr auto range_to_string_view = []<IsInputRangeOf<char> Range_>(Range_&& range)
    {
        return std::string_view{&*std::ranges::begin(range), static_cast<std::string_view::size_type>(std::ranges::distance(range))};
    };

    // **********************************************************

    inline constexpr auto split_string(char delimiter)
    {
        return std::views::split(delimiter) | std::views::transform(range_to_string_view);
    }

    // **********************************************************

    inline constexpr auto filter_true = [](auto&& item)
    {
        return static_cast<bool>(item);
    };

    // **********************************************************

    inline constexpr auto deference = [](auto&& item)
    {
        return *item;
    };
}

// EOF
