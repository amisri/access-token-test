//! @file
//! @brief  Functions to read and write to files.
//! @author Dariusz Zielinski

#pragma once

#include <cstdint>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

namespace utils
{
    // TODO These functions' implementation lacks of error handling.

    //! Read content of a file into an std::string. Currently no error handling.
    //!
    //! @param file_name path to a file to read.
    //! @return returns an std::string with the content of the file.
    std::string readFromFile(const std::string& file_name);

    // **********************************************************

    //! Read content of a file into a vector of std::strings. Currently no error handling.
    //!
    //! @param file_name path to a file to read.
    //! @return returns a vector of std::strings where each element corresponds to a line in the file.
    std::vector<std::string> readLinesFromFile(const std::string& file_name);

    // **********************************************************

    //! Read content of a binary file into array of bytes (unsigned chars). Currently no error handling.
    //!
    //! @param file_name path to a file to read.
    //! @return returns a vector of bytes read from the file.
    std::vector<uint8_t> readFromBinaryFile(const std::string& file_name);

    // **********************************************************

    //! Write data given in an std::string into a file. Currently no error handling.
    //!
    //! @param file_name path to a file to write.
    //! @param data data to write to the file.
    void writeToFile(const std::string& file_name, const std::string& data);

    // **********************************************************

    //! Write data given in a vector of std::strings into a file. Currently no error handling.
    //!
    //! @param file_name path to a file to write.
    //! @param data a vector of std::strings where each element represents a line in the file.
    void writeLinesToFile(const std::string& file_name, const std::vector<std::string>& data);

    // **********************************************************

    //! Write data given in a vector of bytes into a binary file. Currently no error handling.
    //!
    //! @param file_name path to a file to write.
    //! @param data a vector of bytes to be written to the file.
    void writeBinaryToFile(const std::string& file_name, const std::vector<uint8_t>& data, bool request_fsync = false);
}

// EOF