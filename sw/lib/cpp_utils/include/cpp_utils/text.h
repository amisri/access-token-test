//! @file
//! @brief  Functions related to text/string processing
//! @author Dariusz Zielinski

#pragma once

#include <charconv>
#include <cstddef>
#include <optional>
#include <span>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include "Result.h"
#include "misc.h"
#include "typeTraits.h"

namespace utils
{
    //! Convert to upper case (in place version).
    //!
    //! @param str A string to convert to upper case.
    void toUpper(std::string& str);

    // **********************************************************

    //! Convert to upper case (copying version)
    //!
    //! @param str a string to convert to upper case.
    //! @return a version of string in upper case.
    std::string toUpperCopy(std::string_view str);

    // **********************************************************

    //! Convert to lower case (in place version)
    //!
    //! @param str
    void toLower(std::string& str);

    // **********************************************************

    //! Convert to lower case (copying version)
    //!
    //! @param str a string to convert to lower case.
    //! @return a version of string in lower case.
    std::string toLowerCopy(std::string_view str);

    // **********************************************************

    //! Trim whitespaces from start (in place version)
    //!
    //! @param str a string to trim.
    void trimLeft(std::string& str);

    // **********************************************************

    //! Trim whitespaces from start (copying version)
    //!
    //! @param str a string to trim.
    //! @return left-trimmed string.
    std::string trimLeftCopy(std::string_view str);

    // **********************************************************

    //! Trim whitespaces from end (in place version)
    //!
    //! @param str a string to trim.
    void trimRight(std::string& str);

    // **********************************************************

    //! Trim whitespaces from end (copying version)
    //!
    //! @param str a string to trim.
    //! @return right-trimmed string.
    std::string trimRightCopy(std::string_view str);

    // **********************************************************

    //! Trim whitespaces from both ends (in place version)
    //!
    //! @param str a string to trim.
    void trim(std::string& str);

    // **********************************************************

    //! Trim whitespaces from both ends (copying version)
    //!
    //! @param str a string to trim.
    //! @return trimmed string.
    std::string trimCopy(std::string_view str);

    // **********************************************************

    //! @return parsed string as a bool or std::nullopt if no conversion was possible.
    //!
    //! @param value
    //! @return boolean value represented by a string or std::nullopt if no conversion was possible.
    std::optional<bool> parseBool(std::string_view value);

    // **********************************************************

    std::pair<std::string_view, std::string_view> splitFirst(std::string_view string, char delimiter);

    // **********************************************************

    std::string_view asStringView(std::span<const std::byte> bytes);

    // **********************************************************

    template<typename T>
    Result<T, std::string> parseNumber(std::string_view string)
    {
        T value;
        auto [_, error_code] = std::from_chars(string.data(), string.data() + string.size(), value);

        if(error_code != std::errc())
        {
            return Failure{"Cannot parse number"};
        }

        return value;
    }

    // **********************************************************

    template<bounded_enum_type T>
    std::optional<T> parseEnum(std::string_view string)
    {
        using UnderlingType  = std::underlying_type_t<T>;
        auto underling_value = parseNumber<UnderlingType>(string);

        if(underling_value.hasError())
        {
            return std::nullopt;
        }

        if (*underling_value >= static_cast<UnderlingType>(T::count))
        {
            return std::nullopt;
        }

        return static_cast<T>(*underling_value);
    }

}

// EOF
