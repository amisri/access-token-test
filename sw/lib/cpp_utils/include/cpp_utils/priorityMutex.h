//! @file
//! @brief  Provides a mutex and lock guard with priority settings.
//! @author Dariusz Zielinski

#pragma once

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "misc.h"

namespace utils
{
    //! Level of priority for PriorityMutex class.
    enum class MutexPriority
    {
        high,           //! Locks mutex using high priority.
        regular,        //! Locks mutex using regular priority.
    };

    // **********************************************************

    //! This class provides functionality analogous to std::mutex but with selectable priority.
    //! That means that if more than one thread is waiting for lock, the one that locks with high priority will lock first.
    //! It satisfies C++ named requirement BasicLockable, so it can be used with std::lock_guard, std::unique_lock, etc.
    //! Similarly to std::mutex, it is non-copyable and non-movable.
    class PriorityMutex : NoncopyableNonmovable
    {
        public:
            //! Locks the mutex. Locking with mutex_priority set to high will take priority over locking.
            //! with regular priority.
            //!
            //! @param mutex_priority level of locking priority.
            void lock(const MutexPriority& mutex_priority = MutexPriority::regular);

            //! Unlocks the mutex.
            void unlock();

        private:
            std::mutex              m_mutex;                  //!< Standard mutex.
            std::condition_variable m_cond_var;               //!< Conditional variable.
            bool                    m_is_locked = false;      //!< Used to check if the mutex is currently locked.
            size_t                  m_high_threads_count = 0; //!< Number of high priority threads waiting for the lock

    };

    // **********************************************************

    //! A class analogous to std::lock_guard, but it provides selectable lock priority.
    class PriorityLockGuard : NoncopyableNonmovable
    {
        public:
            explicit PriorityLockGuard(PriorityMutex& mutex, const MutexPriority& mutex_priority = MutexPriority::regular);

            ~PriorityLockGuard();

        private:
            PriorityMutex& m_mutex;
    };
}

// EOF