//! @file
//! @brief  A for-each algorithm to iterate over parent-children structures.
//! @author Dariusz Zielinski

#pragma once

#include <functional>
#include <tuple>

#include "flagSet.h"

namespace utils
{
    //! Options for the forEach functions
    enum class ForEach
    {
        parent_after,
        reverse,
        COUNT
    };

    //! Typedef that declare flagSet for forEch options.
    using ForEachFlags = FlagSet<ForEach>;

    // ************************************************************

    //! It's a function that recursively iterates over children of a parent object. A list of the children is fetched
    //! using provided get_children function. For each child (and parent), the provided function is executed on that item.
    //! Flags control a direction of the iteration (forward (default) or reverse) and whether to execute function first
    //! on a parent (default) or only after executing it on the children.
    //! This version uses function that doesn't accept any parameters.
    //!
    //! @tparam T (implicitly deduced) type of the object.
    //! @tparam GetChildrenFunction (implicitly deduced) type of a function to get all children of a parent.
    //! @tparam Function (implicitly deduced) type of a function to execute on items (children and parents).
    //! @param parent parent object.
    //! @param flags additional options, enum class ForEach for the list.
    //! @param get_children  function to get all children of a parent.
    //! @param function function to execute on items (children and parents).
    template<typename T, typename GetChildrenFunction, typename Function>
    void forEach(T& parent, const ForEachFlags flags,
                 GetChildrenFunction&& get_children, Function&& function)
    {
        if (!flags.has(ForEach::parent_after))
        {
            std::invoke(function, &parent);
        }

        if (flags.has(ForEach::reverse))
        {
            auto& children = std::invoke(get_children, &parent);
            for (auto child = children.rbegin(); child != children.rend(); child++)
            {
                forEach(**child, flags, get_children, function);
            }
        }
        else
        {
            for (const auto& child : std::invoke(get_children, &parent))
            {
                forEach(*child, flags, get_children, function);
            }
        }

        if (flags.has(ForEach::parent_after))
        {
            std::invoke(function, &parent);
        }
    }

    // ************************************************************

    //! See the description of the function above.
    //! This version supports passing parameters to the function executed on the children.
    template<typename T, typename GetChildrenFunction, typename Function, typename... Args>
    void forEach(T& parent, const ForEachFlags flags,
                 GetChildrenFunction&& get_children, Function&& function, Args... args)
    {
        auto new_args = std::make_tuple(args...);

        if (!flags.has(ForEach::parent_after))
        {
            new_args = std::invoke(function, &parent, args...);
        }

        if (flags.has(ForEach::reverse))
        {
            auto& children = std::invoke(get_children, &parent);
            for (auto child = children.rbegin(); child != children.rend(); child++)
            {
                std::apply([&](auto&... a){ forEach(**child, flags, get_children, function, a...); }, new_args);
            }
        }
        else
        {
            for (const auto& child : std::invoke(get_children, &parent))
            {
                std::apply([&](auto&... a){ forEach(*child, flags, get_children, function, a...); }, new_args);
            }
        }

        if (flags.has(ForEach::parent_after))
        {
            // TODO for parent_after - the return is ignored. Should it be like that?
            std::invoke(function, &parent, args...);
        }
    }
}

// EOF