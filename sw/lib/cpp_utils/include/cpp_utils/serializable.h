//! @file
//! @brief  Utility to define how to serialize any class.
//! @author Dariusz Zielinski

#pragma once

#include <iostream>
#include <string>

namespace utils
{
    namespace detail
    {
        //! This class is used to stores objects to serialize and their name.
        template<typename T>
        struct Item
        {
            const char* name;
            const T&    obj;

            //! @param name a string used to print the object.
            //! @param obj a reference to the object that needs to be serialized.
            Item(const char* name, const T& obj): name(name), obj(obj)
            {};
        };

        // **********************************************************

        //! It's a minimal serializer made just for IsSerializable type trait.
        class DummySerializer
        {
            public:
                template<typename... Args>
                static std::ostream& serialize(std::ostream& data, Args&&... args)
                {
                    return data;
                }
        };

        // **********************************************************

        //! Default type trait, defined for non-serializable types.
        template<typename T, typename = void>
        struct IsSerializable : std::false_type
        {};

        //! Type trait to check if the type can be serialized (i.e. does it contain serialize method).
        template<typename T>
        struct IsSerializable<T, decltype(std::declval<T>().template serialize<DummySerializer>(std::cout), void())> : std::true_type
        {};
    }

    //! Convenience value for IsSerializable
    template <typename T>
    constexpr bool is_serializable_v = ::utils::detail::IsSerializable<T>::value;


    // **********************************************************

    //! Convenience type for IsSerializable
    template<typename T, typename = void>
    struct IsSerializable : std::false_type
    {};

    //! Convenience type for IsSerializable
    template<typename T>
    struct IsSerializable<T, decltype(std::declval<T>().isSerializable, void())> : std::true_type
    {};

    // **********************************************************

    //! This macro defines a list of serializable members of the class, along with their names used to print them.

    #define SERIALIZABLE(...)                                            \
        template<typename Serializer, typename T>                        \
        auto& serialize(T& context) const                                \
        {                                                                \
            using namespace ::utils::detail;                             \
            return Serializer::serialize(context, __VA_ARGS__ );         \
        }
}

// EOF