//! @file
//! @brief  Provides a way to convert from enum to string and from to string to enum.
//! @author Dariusz Zielinski

#pragma once

#include <iostream>
#include <map>
#include <optional>
#include <ostream>
#include <istream>
#include <type_traits>

#include "misc.h"
#include "text.h"

namespace utils
{
    //! A tuple-like struct that stores a way to parse enum from and to string.
    template<typename T>
    struct EnumStrField
    {
        const T           field;        //!< Enum field.
        const char* const out_str;      //!< String representation of enum field for printing.
        const char* const in_str;       //!< String representation of enum field for parsing.

        //! Creates this object with out_str equal to in_str.
        constexpr EnumStrField(T field, const char* out_str)
            : field(field), out_str(out_str), in_str(out_str)
        {}

        //! Creates this object with a possibility to specify different in_str and out_str.
        constexpr EnumStrField(T field, const char* out_str, const char* in_str)
            : field(field), out_str(out_str), in_str(in_str)
        {}
    };

    // **********************************************************

    template<typename T, size_t N, const EnumStrField<T> (&array)[N]>
    class EnumStr
    {
        static_assert(std::is_enum_v<T>);

        public:
            //! @param enum_value enum field to convert to string.
            //! @return string representation on the enum_value, or "" if enum_value was not registered.
            static constexpr const char* toString(T enum_value)
            {
                for (const auto& e : array)
                {
                    if (e.field == enum_value)
                    {
                        return e.out_str;
                    }
                }

                return "";
            }

            // **********************************************************

            //! @param str_value string that represents enum field.
            //! @return enum filed represented by str_value, or nullopt if no valid conversion was found.
            static std::optional<T> fromString(std::string_view str_value)
            {
                // Create the map (only the first time this function is executed)
                static auto map = arrayToMap();

                // Change the string value to lower case
                auto normalized_value = toLowerCopy(str_value);

                // Find the element in the map
                auto it = map.find(normalized_value.c_str());

                // If found, return it
                return it != map.end() ? std::optional<T>(it->second) : std::nullopt;
            }

        private:
            //! Converts template parameter array into map used to parse string
            static auto arrayToMap()
            {
                std::map<std::string, T> map;

                for (const auto& e : array)
                {
                    map.emplace(toLowerCopy(e.in_str), e.field);
                }

                return map;
            }
    };

    // **********************************************************

    #define JOIN(a,b)  a ## b

    //! Convenience macro to define EnumStrField, that will define "<Enum>::field" for printing and "field" for parsing
    #define ENUM_STR(ENUM_CLASS, ENUM_FIELD) { ENUM_CLASS::ENUM_FIELD, "" #ENUM_CLASS "::" #ENUM_FIELD , #ENUM_FIELD }

    //! Defines ENUM_STR_CLASS class that can be used to print and parse Enum class (ENUM_CLASS).
    #define MAKE_ENUM_STR(ENUM_STR_CLASS, ENUM_CLASS, ...)                                                             \
    constexpr utils::EnumStrField<ENUM_CLASS> JOIN(ENUM_STR_CLASS, _array)[] = { __VA_ARGS__ };                        \
                                                                                                                       \
    using ENUM_STR_CLASS = utils::EnumStr<ENUM_CLASS,                                                                  \
                           utils::getArraySize(JOIN(ENUM_STR_CLASS, _array)), JOIN(ENUM_STR_CLASS, _array)>;           \
                                                                                                                       \
    inline std::ostream& operator<<(std::ostream& stream, const ENUM_CLASS& enum_value)                                \
    {                                                                                                                  \
        auto str = ENUM_STR_CLASS::toString(enum_value);                                                               \
        return str[0] != '\0' ? stream << str :                                                                        \
                                stream << ("<" #ENUM_CLASS ">")                                                        \
                                       << '(' << static_cast<std::underlying_type_t<ENUM_CLASS>>(enum_value) << ')';   \
    }                                                                                                                  \
                                                                                                                       \
    inline std::istream& operator>>(std::istream& stream, ENUM_CLASS& enum_value)                                      \
    {                                                                                                                  \
        if (std::string str; std::getline(stream, str))                                                                \
        {                                                                                                              \
            auto value = ENUM_STR_CLASS::fromString(str);                                                              \
            if (value)                                                                                                 \
            {                                                                                                          \
                enum_value = value.value();                                                                            \
                return stream;                                                                                         \
            }                                                                                                          \
        }                                                                                                              \
        stream.setstate(std::ios_base::failbit);                                                                       \
        return stream;                                                                                                 \
    }
}

// EOF
